#!/bin/bash
TXMINSTALLDIR="/usr/lib/TXM"
ARCHIVEPATH=`readlink -f "$1"`

USEZENITY=true
# X11 tests
if [ ${DISPLAY:-foobartxtm} == foobartxm ] ; then 
	echo "use shell" ; 
	USEZENITY=false;
else 
	echo "try to use display '$DISPLAY'" ; 
	xprop -root >/dev/null &>/dev/null # pour lancer la commande sans afficher de sortie 
	if [ $? != 0 ];then
		echo "use shell"
		USEZENITY=false;
	else
		echo "use zenity"
	fi
fi 

function question() {
# $1: message
# $2: ok message
# $3: cancel message
if ( $USEZENITY ) ; then
	echo "** TXM install : Question: $1"
	zenity --question --text="$1" --ok-label="$2" --cancel-label="$3";
	return $?
else
	echo "** TXM install : Question: $1"
	echo "$2 OR $3 = Y OR n ?"
	read LINE
	if [ "$LINE" = "Y" ];then
		return 0;
	else
		return 1;
	fi
fi
} 

function error() {
# $1: message
echo "** TXM install : Error: $1";
if ( $USEZENITY ) ; then
	zenity --error --text="$1";
fi
} 

echo "use zenity : $USEZENITY"

# Test usage
USAGE="** TXMinstall: usage: install.sh <Install source directory>"
if [ $# != 1 ]; then
	echo "$USAGE"
	exit 1;
fi

# Test if java exists
java -version
if [ $? != 0 ]; then
	echo "** TXM install: Could not find java. Aborting."
	exit 1;
fi

# Test if TestArchi.jar exists
if [ ! -f "$ARCHIVEPATH/TestPreInstall.jar" ]; then
	echo "** TXM install: TestArchi.jar is missing $ARCHIVEPATH/TestPreInstall.jar. Can't continue the installation."
	exit 1;
fi

# Test is javaversion os and arch match to the setup
java -jar "$ARCHIVEPATH/TestPreInstall.jar" 1.6.0 i386 Linux
if [ $? != 0 ]; then
	echo "** TXM install: It seems that TXM setup is not appropriate for your system. Still continue ?"
	echo "Type Y to continue and n to stop installation. Y/n ?"
	read LINE
	if [ "$LINE" = "n" ];then
		exit 1
	fi
fi

# Test if ARCHIVEPATH exists
if [ ! -d "$ARCHIVEPATH" ]; then
	echo "** TXM install: archive $ARCHIVEPATH does not exist or is not a directory. Can't continue the installation."
	exit 1;
fi



# License agreement
question "`cat $TXMINSTALLDIR/license.txt`" "I agree" "Cancel"
if [ $? != 0 ];then
	echo "** TXM install: abort"
	exit 1;
fi

echo "** TXM install: This script will now delete and create the 'TXM' directory in the directory ($TXMINSTALLDIR), create a link to the TXM binary and install R if it is not already installed."
echo "** TXM install: Continue ? (Y/n)"

read LINE
if [ "$LINE" = "Y" ];then
	echo ""
else
	echo "** TXM install: abort"
	exit;
fi

# install R & packages
echo "** TXM install: Install R packages"
if [ `which R` ];then 
	echo ""
else
	aptitude install r-base
fi 

sudo R --no-save < "$TXMINSTALLDIR/R/rlibs.R"

if [ $? != 0 ]; then
	error "** TXM install: R packages installation failed. Aborting."
	exit 1;
fi

echo "** TXM install: Removing $TXMINSTALLDIR..."
		rm -rf "$TXMINSTALLDIR"
		if [ $? != 0 ]; then
			echo "** TXM install: error in 'rm -rf $TXMINSTALLDIR', aborting."
			exit 1 ;
		fi

# create TXM install dir
mkdir "$TXMINSTALLDIR"
if [ $? != 0 ]; then
	echo "** TXM install: creation of directory TXMINSTALLDIR '$TXMINSTALLDIR' failed. Aborting."
	exit 1;
fi

# copy archive
echo "** TXM install: Copy files into TXM install directory"
cp -rf "$ARCHIVEPATH"/* "$TXMINSTALLDIR"
if [ $? != 0 ]; then
	echo "** TXM install: TXMinstallation files copy failed. Aborting."
	exit 1;
fi

# create launcher
echo "** TXM install: create the TXM launcher"
rm -fv "/usr/local/bin/TXM"
rm -fv "/usr/bin/TXM" && \
cp "$ARCHIVEPATH/TXMexec" "/usr/bin" && \
mv "/usr/bin/TXMexec" "/usr/bin/TXM"

if [ $? != 0 ]; then
	echo "** TXM install: Failed to create TXM launcher. Aborting."
	exit 1;
fi

echo "** TXM install: Create preferences file"
optExec="cqi_server_path_to_executable"
pathToCqp="$TXMINSTALLDIR/cwb/bin/cqpserver"
optInit="cqi_server_path_to_init_file"
pathToInit="$TXMINSTALLDIR/cwb/cqpserver.init"
optPathToR="r_path_to_executable"
pathToR=`which R`
optPathToTT="TREETAGGER_INSTALL_PATH"
pathToTT="$TXMINSTALLDIR/treetagger"
optPathToTTMod="TREETAGGER_MODELS_PATH"
pathToTTMod="$TXMINSTALLDIR/treetagger/models"

# create STAMP
touch "$TXMINSTALLDIR/STAMP"

# set executable files
chmod +x "$TXMINSTALLDIR/TXM"
chmod +x "$TXMINSTALLDIR/cwb/bin"/*

# set preferences
PREF_FILE="$TXMINSTALLDIR/install.prefs"
touch "$PREF_FILE"
echo $optExec"="$pathToCqp >> "$PREF_FILE"
echo $optInit"="$pathToInit >> "$PREF_FILE"
echo $optPathToR"="$pathToR >> "$PREF_FILE"
echo $optPathToTT"="$pathToTT >> "$PREF_FILE"
echo $optPathToTTMod"="$pathToTTMod >> "$PREF_FILE"
# set tools' default options
echo "ca_show_individuals=true" >> "$PREF_FILE"
echo "ca_show_variables=false" >> "$PREF_FILE"
echo "ca_quality_display=.00" >> "$PREF_FILE"
echo "ca_mass_display=###.00" >> "$PREF_FILE"
echo "ca_dist_display=#.00" >> "$PREF_FILE"
echo "ca_contrib_display=###.00" >> "$PREF_FILE"
echo "ca_cos2_display=.00" >> "$PREF_FILE"
echo "ca_coord_display=#.00" >> "$PREF_FILE"
echo "cooc_scoreformat=E00" >> "$PREF_FILE"
echo "cooc_minfreq=2" >> "$PREF_FILE"
echo "cooc_mincount=2" >> "$PREF_FILE"
echo "diag_max_value=20" >> "$PREF_FILE"
echo "lt_fmin=2" >> "$PREF_FILE"
echo "r_file_transfert=false" >> "$PREF_FILE"

touch "$TXMINSTALLDIR/STAMP"

if [ $? != 0 ]; then
	error "** TXM install: Failed to create install TIMESTAMP file : $TXMINSTALLDIR/STAMP"
	exit 1;
fi

echo "** TXM install: done, type TXM& in a terminal to launch the application"

exit 0
