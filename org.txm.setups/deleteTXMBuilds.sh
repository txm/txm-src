#!/bin/bash
ROOT=exportRCP
VERSION=`cat VERSION`

rm -rf $ROOT/linux.gtk.x86_64
rm -rf $ROOT/macosx.cocoa.x86_64
rm -rf $ROOT/win32.win32.x86_64
rm -rf $ROOT/repository

rm -rf "TXM_${VERSION}_Win64_installer"
rm -rf "TXM_${VERSION}_Linux64_installer"
rm -rf "TXM_${VERSION}_MacOSX_installer"

