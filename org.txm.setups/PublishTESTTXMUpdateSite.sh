#./updateTESTTXMUpdateSite.sh dev && ./updateTESTTXMUpdateSite.sh alpha && ./updateTESTTXMUpdateSite.sh beta && ./updateTESTTXMUpdateSite.sh

echo "Sending RCP export to TEST update site..."

UPDATESITE="/home/mdecorde/TEMP/updates"

if [ "$1" = "" ]; then
	echo "uploading to default test repository"
elif [[ $1 =~ alpha|beta|dev ]]; then
	echo "uploading to the '$1' test repository"
else
	echo "$1: invalid parameter"
	exit 1;
fi

#moving to repository
cd "exportRCP/repository"
if [ $? != 0 ]; then
	echo "** updateTESTTXMUpdateSites.sh : failed to find exportRCP/repository"
	exit 1;
fi

mkdir "$UPDATESITE"
rm -rf "$UPDATESITE/TXM$1"

mkdir "$UPDATESITE/TXM$1" && 
mkdir "$UPDATESITE/TXM$1/plugins" && 
mkdir "$UPDATESITE/TXM$1/binary" && 
mkdir "$UPDATESITE/TXM$1/features" && 
cp -r * "$UPDATESITE/TXM$1"

if [ $? != 0 ]; then
	echo "** updateTESTTXMUpdateSites.sh : failed to update"
	exit 1;
fi

