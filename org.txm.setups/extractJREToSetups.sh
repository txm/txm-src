JREDIR=~/workspace079/JREPOURTXM

echo "extracting Linux 64bit JRE..."
tar -xzf $JREDIR/jre\ linux\ 64bit.tar.gz
rm -rf archs/debian/64bit/usr/lib/TXM/jre/*
mv -f jre archs/debian/64bit/usr/lib/TXM

echo "extracting Windows 64bit JRE..."
unzip -q $JREDIR/jre\ win\ 64bit.zip
rm -rf archs/win/64bit/TXM/jre/*
mv -f jre archs/win/64bit/TXM/

echo "extracting MacOSX JRE... Oh no, it must be done on a Mac!"
