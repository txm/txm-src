DIRNAME=`dirname $0`
VERSION=`cat $DIRNAME/../bundles/org.txm.rcp/VERSION`
SUBVERSION=`cat $DIRNAME/../bundles/org.txm.rcp/SUBVERSION`
FULLVERSION=${VERSION}${SUBVERSION}
STEP=$1
TIMESTAMP=`date +"%Y-%m-%d"`
APP="$DIRNAME/TXM_${FULLVERSION}_${TIMESTAMP}_MacOSX"
SHAREDMAC="$DIRNAME/shared/mac"
SHAREDALL="$DIRNAME/shared/all"

rm -f "$APP.tar.gz"
rm -rf "$APP"
mkdir "$APP"

# ensure INSTALL CLEAN file rights
chmod 775 $SHAREDMAC/INSTALL $SHAREDMAC/CLEAN
if [ $? != 0 ]; then
	echo "** $APP: failed to set rights of INSTALL CLEAN files "
	exit 1;
fi

echo " copy mac shared files: R"
rsync -r  --exclude '.svn' $SHAREDMAC/* "$APP"
if [ $? != 0 ]; then
	echo "** MacOSXIntel.sh : failed to copy Mac shared files"
	exit 1;
fi

echo " copy shared files: css, xsl, scripts, samples..."
rsync -r  --exclude '.svn' $SHAREDALL/* "$APP/Applications/TXM.app/Contents/Eclipse"
if [ $? != 0 ]; then
	echo "** MacOSXIntel.sh : failed to copy shared files"
	exit 1;
fi

echo " convert licence files encoding to MacRoman"
iconv -f utf8 -t MAC $SHAREDALL/license_agreement_fr.txt > "$APP/Applications/TXM.app/Contents/Eclipse/license_agreement_fr.txt"
iconv -f utf8 -t MAC $SHAREDALL/license_agreement.txt > "$APP/Applications/TXM.app/Contents/Eclipse/license_agreement.txt"
iconv -f utf8 -t MAC $SHAREDALL/LICENSE_FR.TXT > "$APP/Applications/TXM.app/Contents/Eclipse/LICENSE_FR.TXT"
iconv -f utf8 -t MAC $SHAREDALL/LICENSE.TXT > "$APP/Applications/TXM.app/Contents/Eclipse/LICENSE.TXT"

echo " remove parasite settings"
rm -f "$DIRNAME/../products/org.txm.rcp.product/target/products/org.txm.rcp.app/macosx/cocoa/x86_64/TXM.app/Contents/Eclipse/p2/org.eclipse.equinox.p2.engine/.settings/*" &&
rm -f "$DIRNAME/../products/org.txm.rcp.product/target/products/org.txm.rcp.app/macosx/cocoa/x86_64/TXM.app/Contents/Eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings/*"
if [ $? != 0 ]; then
	echo "** $APP: failed to remove p2 preferences"
	exit 1;
fi

echo " copy eclipse arch dependent build"
rsync -r  --exclude '.svn' "$DIRNAME/../products/org.txm.rcp.product/target/products/org.txm.rcp.app/macosx/cocoa/x86_64/"* "$APP/Applications/"
if [ $? != 0 ]; then
	echo "** MacOSXIntel.sh : failed to binaries files"
	exit 1;
fi

chmod +x "$APP/INSTALL" &&
chmod +x "$APP/CLEAN"
if [ $? != 0 ]; then
	echo "** MacOSXIntel.sh : failed to chmod +x INSTALL & CLEAN files"
	exit 1;
fi

#echo "set LSUIElement to 1"
#find ./$APP/Applications/TXM.app/.main.app/Contents/Info.plist -type f -exec sed -i 's/<dict>/<dict><key>LSUIElement<\/key><string>1<\/string>/g' {} \;
#if [ $? != 0 ]; then
#	echo "** MacOSXIntel.sh : failed to fix Info.plist"
#	exit 1;
#fi

echo " clean"
bash $DIRNAME/directoryCleaner.sh "$APP"
if [ $? != 0 ]; then
	echo "** $APP: failed to clean mac setup $APP"
	exit 1;
fi

echo " search&replace in files TXMVERSION"
find "$APP/CLEAN" -type f -exec sed -i "s/TXMVERSION/$FULLVERSION/g" {} \;
find "$APP/finish_en.txt" -type f -exec sed -i "s/TXMVERSION/$FULLVERSION/g" {} \;
find "$APP/INSTALL" -type f -exec sed -i "s/TXMVERSION/$FULLVERSION/g" {} \;
find "$APP/BUILDPKG.pkgproj" -type f -exec sed -i "s/TXM_TXMVERSION_MacOSX_installer/TXM_${VERSION}_${TIMESTAMP}_MacOSX_installer/g" {} \;
find "$APP/BUILDPKG.pkgproj" -type f -exec sed -i "s/TXMVERSION/$FULLVERSION/g" {} \;
find "$APP/BUILDPKG.pkgproj" -type f -exec sed -i "s/TXMMAJORVERSION/$VERSION/g" {} \;
find "$APP/Applications/TXM.app/Contents/MacOS/TXM" -type f -exec sed -i "s/TXMMAJORVERSION/$VERSION/g" {} \;
find "$APP/Applications/TXM.app/Contents/MacOS/TXM" -type f -exec sed -i "s/TXMVERSION/$FULLVERSION/g" {} \;
find "$APP/Applications/TXM.app/Contents/Info.plist" -type f -exec sed -i "s/launcher/TXM/gi" {} \;

# +x the executable rights
chmod +x "$APP/Applications/TXM.app/Contents/MacOS/"*

echo " rename files to add VERSION=$VERSION"
mv "$APP/Applications/TXM.app" "$APP/Applications/TXM-${FULLVERSION}.app"

echo "Tar gz $APP"
tar -zcf "$APP.tar.gz" "$APP"
if [ $? != 0 ]; then
	echo "** MacOSXIntel.sh : failed to tar gz $APP"
	exit 1;
fi


