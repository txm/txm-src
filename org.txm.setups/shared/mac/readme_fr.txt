
TXM vous est fourni gracieusement. En contre-partie, dans l'esprit du logiciel libre, vous �tes invit� � participer � son am�lioration. Pour cela, vous n'�tes pas oblig� d'�tre d�veloppeur informatique, vous pouvez :
 * transmettre vos publications ou supports de cours ;
 * proposer des corrections ou am�liorations ;
 * traduire l'interface ou la documentation ;
 * monter un projet pour adapter TXM � vos besoins ;
 * etc.

 Voir la rubrique 'Contribuer' du wiki des utilisateurs de TXM : https://groupes.renater.fr/wiki/txm-users/public/contribuer 

 N'h�sitez pas � nous contacter pour de plus amples informations : 'textometrie AT groupes.renater DOT fr'.
