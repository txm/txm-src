
TXM is provided to you free of charge. In return, in the spirit of open-source software development, you are invited to participate in its improvement. To do this, you don't have to be a software developer, you can:
 * send us your publications or course materials related to your use of TXM;
 * suggest corrections or improvements;
 * translate in your language the interface or documentation;
 * Setup a research project involving TXM adaptation and use;
 * etc.

 See the 'Contribute' page on the TXM users' wiki: https://groupes.renater.fr/wiki/txm-users/public/contribuer 

 Don't hesitate to contact us for more information at 'textometrie AT groupes.renater DOT fr'.
