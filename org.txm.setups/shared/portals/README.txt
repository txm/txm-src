TXM portal v0.6.1

This archive contains:
 * J2EE portal WAR file: WAR to deploy with Tomcat
 * LICENSE.txt: property and usage declaration file
 * INSTALL_Linux_EN.txt: Linux installation instructions
 * INSTALL_Windows_FR.txt: Windows installation instructions
 * makePassword.jar: MD5 password encryption utility
 * doc directory:
   * Manuel de reference TXMWEB Admin X.X_FR.pdf: portal administrator manual
   * Manuel de reference TXMWEB Dev X.X_FR.pdf: portal ..~~**(Developper)**~~.. manual
 * ANNOUNCE.txt: release details
 * README.txt: this file
 