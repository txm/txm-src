EN EN EN EN EN

Une demande d'accès à la Base de Français Médiéval a été formulée sur le portail
portail (%1$s) avec l'adresse mail %2$s

Les informations associées à cette demande sont les suivantes :

 - Nom : %3$s
 - Prénom : %4$s
 - Institution : %5$s
 - Tél : %6$s
 - Statut : %7$s
 - Identifiant : %8$s
 - Mail : %2$s

Si vous souhaitez modifier certaines de ces informations, merci de contacter 
l'administrateur du portail (%9$s) pour lui indiquer les corrections à effectuer.
Par la suite, vous pourrez également corriger certaines informations dans votre espace 
'informations personnelles' du portail un fois connecté.

Cette demande d'accès est associée à la création du compte utilisateur '%8$s'.
Pour pouvoir valider la création de ce compte, merci de cliquer sur le lien suivant %10$s
(ce lien est valable 24h) pour associer définitivement cette adresse mail avec ce compte.
Une fois cette association établie, vous pourrez vous connecter et l'administrateur pourra alors valider le compte.
L'accès à la base vous sera alors signifié par un mail de l'administrateur.

Si vous n'êtes pas à l'origine de cette demande d'accès à la BFM, merci d'avertir 
l'administrateur du portail %11$s (%9$s) pour lui indiquer un abus de l'usage de votre mail 
de sorte à ce qu'il puisse annuler cette inscription. Dans ce cas, merci de ne pas 
cliquer sur le lien de confirmation de la demande de création de compte.

Cordialement, 
l'équipe %11$s
