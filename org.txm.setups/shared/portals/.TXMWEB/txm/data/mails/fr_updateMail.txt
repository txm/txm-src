TXM - Mise à jour de l'email de votre compte
Vous ou quelqu'un d'autre a demandé la mise à jour de votre email
de votre compte %1$s sur le portail %2$s. Si cela vous convient, 
veuillez cliquer sur le lien suivant pour continuer : %3$s
Sinon veuillez simplement ignorer ce mail. Vous pouvez aussi signaler
un abus auprès de l'administrateur : %4$s
