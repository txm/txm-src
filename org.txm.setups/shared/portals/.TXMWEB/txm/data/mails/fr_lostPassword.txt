Portail BFM - Récupération de votre mot de passe
Madame, Monsieur,

Une demande d'envoi d'un mail de récupération de mot de passe de
votre compte '%1$s' a été formulée sur le portail %2$s.

Si cela vous convient, veuillez cliquer sur le lien suivant pour 
continuer : %3$s

Sinon veuillez simplement ignorer ce mail. Vous pouvez aussi signaler
un abus auprès de l'administrateur : %4$s

Cordialement, 
l'équipe BFM