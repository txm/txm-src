TXM - Update your account mail
You, or someone else, has requested to update the email for 
account %1$s of the %2$s portal. If you agree with that, 
please click on the following link to confirm the sending 
of those informations.

To continue, follow this link: %3$s
Otherwise please ignore this email or signal an abuse to %4$s.
