set installdir=%~dp0
set ARGS=%*
set DEBUG=0
set VERSION=0.8.4
set USERHOME=%USERPROFILE%\

IF NOT EXIST "%installdir%/ARCHIVEINSTALL" (GOTO NORMALSTARTTXM)

START %installdir%launcher.exe -test
GOTO WAITDEBUG

:NORMALSTARTTXM

IF NOT EXIST ROAMINGINSTALL GOTO ROAMINGINSTALLDONE
echo "The 'roaming' install option is set."
set USERHOME=%HOMEDRIVE%%HOMEPATH%\
echo "USERHOME set to %USERHOME%"

:ROAMINGINSTALLDONE

IF NOT EXIST ASKUSERDIRECTORY GOTO ASKUSERDIRECTORYDONE
echo "The 'ask user directory' install option is set."
for /f "tokens=*" %%a in ('jre\bin\java -jar StartDialog.jar') do set USERHOME=%%a\
echo "USERHOME set to %USERHOME%"

:ASKUSERDIRECTORYDONE

IF EXIST "%USERHOME%" GOTO USERHOMEOK
echo "%USERHOME% does not exist. Aborting"
msg * "TXM could not start using the USERHOME='%USERHOME%' directory."
GOTO:EOF

:USERHOMEOK

set TXMHOME=%USERHOME%TXM-%VERSION%\
set DOTTXM=%USERHOME%.TXM-%VERSION%\

mkdir "%TXMHOME%" 

mkdir "%DOTTXM%" 
attrib +h "%DOTTXM%" /s /d

echo "TXM: getting install date from %installdir%STAMP and %DOTTXM%STAMP files" 
Fc "%installdir%STAMP" "%DOTTXM%STAMP" 

echo "TXM: Preparing redirections in %USERHOME%/TXMPostInstallOutputLogs.txt and %USERHOME%/TXMPostInstallErrorLogs.txt files."


IF errorlevel 1 (goto :install) else (goto :run)

:install

(
echo "New TXM install: it's the first launch after the installation." 
del "%temp%\org.txm.rcpapplication.prefs" 
copy "%DOTTXM%configuration\.settings\org.txm.rcpapplication.prefs" "%temp%\org.txm.rcpapplication.prefs" 
copy "%DOTTXM%.metadata\.plugins\org.eclipse.core.runtime\.settings\org.txm.rcpapplication.prefs" "%temp%\org.txm.rcpapplication.prefs" 
copy "%DOTTXM%data\.metadata\.plugins\org.eclipse.core.runtime\.settings\org.txm.rcpapplication.prefs" "%temp%\org.txm.rcpapplication.prefs" 

rmdir /s /q "%DOTTXM%"
IF errorlevel 1 (echo "TXM: failed to delete the %DOTTXM% directory.")

mkdir "%DOTTXM%" 
IF errorlevel 1 (echo "TXM: failed to create %DOTTXM% directory.")

copy "%installdir%launcher.ini" "%DOTTXM%launcher.ini" 
IF errorlevel 1 (echo "TXM: failed to copy %DOTTXM%launcher.ini file")

copy "%temp%\org.txm.rcpapplication.prefs" "%DOTTXM%org.txm.rcpapplication.prefs" 

mkdir "%DOTTXM%user" 
IF errorlevel 1 (echo "TXM: failed to create %DOTTXM%user directory.")

mkdir "%DOTTXM%configuration" 
IF errorlevel 1 (echo "TXM: failed to create %DOTTXM%user directory.")

rmdir /s /q "%TXMHOME%\.metadata"
IF errorlevel 1 (echo "TXM: failed to delete the %TXMHOME%.metadata directory.")

copy "%installdir%STAMP" "%DOTTXM%STAMP" 
IF errorlevel 1 (echo "TXM: failed to copy %DOTTXM%STAMP file")
) >"%USERHOME%\TXMPostInstallOutputLogs.txt" 2>"%USERHOME%\TXMPostInstallErrorLogs.txt"

:run
(
set _JAVA_OPTIONS=-Duser.home="%USERHOME%"
echo running "%installdir%launcher.exe" -run --launcher.ini "%DOTTXM%launcher.ini" %ARGS% -data "%DOTTXM%data" -user "%DOTTXM%user" -install "%installdir%"
REM "%installdir%launcher.exe" -run --launcher.ini "%DOTTXM%launcher.ini" %ARGS% -user "%DOTTXM%user" -install "%installdir%"
) >"%USERHOME%\TXMPostInstallOutputLogs.txt" 2>"%USERHOME%\TXMPostInstallErrorLogs.txt"


cd "%installdir%" 
START launcher.exe -run --launcher.ini "%DOTTXM%launcher.ini" %ARGS% -configuration "%DOTTXM%configuration" -user "%DOTTXM%user" -install "%installdir%\"


:WAITDEBUG
IF "%~1"=="-debug" (
	echo "TXM launched in debug mode."
	pause
)
