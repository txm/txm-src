Unicode True

!include MUI2.nsh
!include nsDialogs.nsh
!include LogicLib.nsh

!include txmfunctions.nsi

;general

Name "TXM TXMVERSION"

!define Version "-TXMVERSION"

OutFile "../TXM_TXMDATEVERSION_Win64_installer.exe"

InstallDir "$LOCALAPPDATA\TXM${Version}"

;RequestExecutionLevel admin
RequestExecutionLevel user

;compression options set in the build command line

;interface configuration

!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_RIGHT
!define MUI_HEADERIMAGE_BITMAP "header.bmp"
!define MUI_HEADERIMAGE_UNBITMAP "header.bmp"
!define MUI_ICON "install.ico"
!define MUI_UNICON "uninstall.ico"
!define MUI_WELCOMEFINISHPAGE_BITMAP "welcome.bmp" ; optional


;Welcome conf

;!define MUI_WELCOMEFINISHPAGE_BITMAP "welcome.bmp" ; optional
!define MUI_WELCOMEPAGE_TITLE $(WelcomeTitleMessage)
!define MUI_WELCOMEPAGE_TEXT $(WelcomeTextMessage)


;Finish conf

!define MUI_FINISHPAGE_TITLE $(FinishTitleMessage)
!define MUI_FINISHPAGE_TEXT $(FinishTextMessage)

;pages

!insertmacro MUI_PAGE_WELCOME

;!insertmacro MUI_PAGE_LICENSE "license_agreement.txt"  
!insertmacro MUI_PAGE_DIRECTORY

Var Dialog
Var Checkbox1
Var Checkbox2
Var Checkbox3
Var CheckValue1
Var CheckValue2
Var CheckValue3

Page Custom SpecialOptionsPage SpecialOptionsLeavePage

Function SpecialOptionsPage
	!insertmacro MUI_HEADER_TEXT $(AdvancedOptionsTitle) $(AdvancedOptionsSubTitle)

	nsDialogs::Create 1018
	Pop $Dialog

	${If} $Dialog == error
		Abort
	${EndIf}

	${NSD_CreateCheckBox} 0 0 100% 30% $(LocalInstallMessage)
	Pop $Checkbox1
	
	${NSD_CreateCheckBox} 0 30% 100% 30% $(AskUserDirMessage)
	Pop $Checkbox2
	
	${NSD_CreateCheckBox} 0 60% 100% 30% $(PortableInstall)
	Pop $Checkbox3

	nsDialogs::Show
FunctionEnd

Function SpecialOptionsLeavePage

SetOutPath $INSTDIR

  ${NSD_GetState} $Checkbox1 $CheckValue1
  ${If} $CheckValue1 == '1'
    File ROAMINGINSTALL
  ${EndIf}

  ${NSD_GetState} $Checkbox2 $CheckValue2
  ${If} $CheckValue2 == '1'
    File ASKUSERDIRECTORY
  ${EndIf}

  ${NSD_GetState} $Checkbox3 $CheckValue3
  ${If} $CheckValue3 == '1'
    File ARCHIVEINSTALL
  ${EndIf}

FunctionEnd

!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_UNPAGE_CONFIRM

!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_PAGE_FINISH

;Languages (must be set after calling the MUI_(UN)PAGE)

!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "French"
!insertmacro MUI_LANGUAGE "Russian"

!include messages.nsi

Function .onInit

	;!insertmacro MUI_LANGDLL_DISPLAY
	
		setOutPath $INSTDIR
	
		ReadRegStr $R0 HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TXM${Version}" "UninstallString"
		StrCmp $R0 "" done
	
		MessageBox MB_OKCANCEL|MB_ICONEXCLAMATION $(UninstallWindowsMessage) IDOK uninst
		Abort

	;Run the uninstaller
	
	uninst:
	
		ClearErrors
		
		ExecWait '$R0 _?=$INSTDIR' ;Do not copy the uninstaller to a temp file
		
		IfErrors no_remove_uninstaller done
		
		no_remove_uninstaller:
		
	done:
	
FunctionEnd

Section "Copy files" SecFiles
	
	#we install txm in INSTDIR
	
	SetOutPath $INSTDIR
	
File /r configuration
File /r doc
File /r features
File /r jre
File /r p2
File /r plugins
File /r readme
;File /r redist
File /r samples
File artifacts.xml
File cqpjni.dll
File header.bmp
File IEShims.dll
File install.ico
File libcharset-1.dll
File libglib-2.0-0.dll
File libgnurx-0.dll
File libiconv-2.dll
File libintl-8.dll
File libpcre-1.dll
File LICENSE.TXT
File license_agreement.txt
File license_agreement_fr.txt
File LICENSE_FR.TXT
File OpenAppDataDirectory.bat
File pcre3.dll
File TXM.bat
File launcher.exe
File TXM.ico
File launcher.ini
File "TXM debug.bat"
File uninstall.ico
	
	# hide TXM.exe
	SetFileAttributes "$INSTDIR/launcher.exe" HIDDEN
	
	# the users right on install dir to enable updates
	# AccessControl::GrantOnFile "$INSTDIR\TXM" "(S-1-5-32-545)" "FullAccess"
	
	# we set the shortcuts
	SetShellVarContext current
	
	# use ? "AppData\Roaming\Microsoft\Windows\Start Menu\Programs"
	
	CreateDirectory "$SMPROGRAMS\TXM${Version}"
	
	CreateShortCut "$SMPROGRAMS\TXM${Version}\TXM${Version}.lnk" "$INSTDIR\TXM.bat" "" "$INSTDIR\TXM.ico" 0
	CreateShortCut "$SMPROGRAMS\TXM${Version}\TXM${Version} debug.lnk" "$INSTDIR\TXM debug.bat" "" "$INSTDIR\TXM.ico" 0
	CreateShortCut "$SMPROGRAMS\TXM${Version}\uninstall TXM${Version}.lnk" "$INSTDIR\uninstall.exe"
	#CreateShortCut "$SMPROGRAMS\TXM${Version}\Manuel utilisateur FR.pdf.lnk" "$INSTDIR\doc\Manuel de TXM 0.7 FR.pdf"
	#CreateShortCut "$SMPROGRAMS\TXM${Version}\User Manual EN.pdf.lnk" "$INSTDIR\doc\TXM 0.7 Manual EN.pdf"
	
	; Store installation folder
	
	WriteRegStr HKCU "Software\TXM${Version}" "" $INSTDIR
	
	#register the TXM uninstall to windows programs
	
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TXM${Version}" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TXM${Version}" "DisplayName" "TXM${Version}  - The Textometrie project builds the new generation of modular and open-source textometry platform and application. The scientific project web site is http://textometrie.ens-lsh.fr/?lang=en. "
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TXM${Version}" "QuietUninstallString" "$\"$INSTDIR\uninstall.exe$\" /S"


	#we create the uninstaller
	WriteUninstaller "$INSTDIR\uninstall.exe"
	
SectionEnd


Section "WriteInstallStamp" secStamp

	${TimeStamp} $0
	
	Push "$0"
	
	Push "$INSTDIR\STAMP" ;file to write to 
	
	Call WriteToFile
	
SectionEnd


Section "WriteInstallPrefs" secPrefs

	#we set the default preferences
	
	;!define pathToCqp "$INSTDIR\cwb\bin\win32\cqpserver.exe"
	;!define pathToCqpLib "$INSTDIR\cwb\bin\"
	;!define pathToInit "$INSTDIR\cwb\cqpserver.init"
	;!define pathToR "$INSTDIR\R\bin\x64\Rterm.exe"
	;!define pathToTT "$PROGRAMFILES\treetagger"
	;!define pathToTTMod "$PROGRAMFILES\treetagger\models"
	

	;FileOpen $4 "$INSTDIR\install.prefs" a
	
	;FileSeek $4 0 END
	
	;FileWrite $4 "$\r$\ncqi_server_path_to_cqplib=${pathToCqpLib}"
	;FileWrite $4 "$\r$\ncqi_server_path_to_executable=${pathToCqp}"
	;FileWrite $4 "$\r$\ncqi_server_path_to_init_file=${pathToInit}"
	;FileWrite $4 "$\r$\nr_path_to_executable=${pathToR}"
	;FileWrite $4 "$\r$\ntreetagger_install_path=${pathToTT}"
	;FileWrite $4 "$\r$\ntreetagger_models_path=${pathToTTMod}"
	;FileClose $4 ; and close the file
	
SectionEnd


;UninstallText "This will uninstall TXM"



;uninstaller section start

Section "Uninstall"

	SetShellVarContext current
	
	# Delete installation key
	DeleteRegKey /ifempty HKCU "Software\TXM${Version}"
	
	# Delete uninstaller key
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TXM${Version}"
	
	# we remove the software
	RMDir /r $INSTDIR
	RMDir /r "$SMPROGRAMS\TXM${Version}"
	
SectionEnd

