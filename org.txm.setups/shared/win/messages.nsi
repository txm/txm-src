
;Lang Strings ENGLISH
LangString AdvancedOptionsTitle ${LANG_ENGLISH} "Advanced options"
LangString AdvancedOptionsSubTitle ${LANG_ENGLISH} "Check these options only for specific Windows configurations"
LangString LocalInstallMessage ${LANG_ENGLISH} "Do not use local user directory (check this option to use a roaming Windows account home directory)."
LangString PortableInstall ${LANG_ENGLISH} "Portable install of TXM"

LangString AskUserDirMessage ${LANG_ENGLISH} "Do not use the user directory to store the TXM working directory (check this option to store corpora on a USB stick or on a remote directory. In this case, the working directory path will be asked at each TXM launch)."

LangString InfosTitleMessage ${LANG_ENGLISH} "For your information"

LangString InfosTextMessage ${LANG_ENGLISH} "If TXM is already installed on this machine, it will first be removed by this installer (TXM corpora will NOT be removed).$\nClose the window to cancel installation.$\n$\nMinimum requirements: Windows 7 64bit"

LangString WelcomeTitleMessage ${LANG_ENGLISH} "Welcome to TXM setup"

LangString WelcomeTextMessage ${LANG_ENGLISH} "TXM is provided to you free of charge. In return, in the spirit of open-source software development, you are invited to participate in its improvement. To do this, you don't have to be a software developer, you can:$\n * send us your publications or course materials related to your use of TXM;$\n * suggest corrections or improvements;$\n * translate in your language the interface or documentation;$\n * Setup a research project involving TXM adaptation and use;$\n * etc.$\n$\n See the 'Contribute' page on the TXM users' wiki: https://groupes.renater.fr/wiki/txm-users/public/contribuer $\n$\n Don't hesitate to contact us for more information at 'textometrie AT groupes.renater DOT fr'."

LangString FinishTitleMessage ${LANG_ENGLISH} "Installation done"

LangString FinishTextMessage ${LANG_ENGLISH} "The installation is done. A 'TXM' entry has been added to the 'Start' menu.$\nIf you want to automatically tag and lemmatize your corpora with the TreeTagger software during import, you need to follow additional installation instructions given in the 'Help > Install TreeTagger' menu."

LangString ErrorArch32Message ${LANG_ENGLISH} "This setup is for 32bit systems but your system seems to be 64bit. Please download and install the 64bit version of TXM for this machine. Continue to install anyway?"

LangString ErrorArch64Message ${LANG_ENGLISH} "This setup is for 64bit systems but your system seems to be 32bit. Please download and install the 32bit version of TXM for this machine. Continue to install anyway?"

LangString UninstallWindowsMessage ${LANG_ENGLISH} "TXM TXMVERSION is already installed on this machine.$\n$\nClick `OK` to uninstall the previous version first (TXM TXMVERSION corpora will NOT be removed).$\nOr click `Cancel` to abort this installation.$\nPlease ensure that no TXM TXMVERSION is running during the installation process."


;Lang Strings FRENCH
LangString AdvancedOptionsTitle ${LANG_FRENCH} "Options avancées"
LangString AdvancedOptionsSubTitle ${LANG_FRENCH} "Ne cocher ces options que pour des configurations Windows particulières"
LangString LocalInstallMessage ${LANG_FRENCH} "Ne pas utiliser le répertoire utilisateur local (cocher cette option si vous souhaitez utiliser un répertoire de connexion de compte itinérant)."
LangString PortableInstall ${LANG_FRENCH} "Installation portable de TXM"

LangString AskUserDirMessage ${LANG_FRENCH} "Ne pas utiliser le répertoire utilisateur comme répertoire de travail de TXM (cocher cette option pour stocker les corpus sur une clé USB ou sur un disque distant. Dans ce cas, le chemin du répertoire de travail sera demandé chaque lancement de TXM)."

LangString InfosTitleMessage ${LANG_FRENCH} "Pour votre information"

LangString InfosTextMessage ${LANG_FRENCH} "Si TXM est déjà installé sur cette machine, cet installeur le supprimera pour commencer (les corpus existants ne seront pas supprimés). Pour annuler l'installation, vous pouvez fermer cette fenêtre.$\n$\nConfiguration minimum: Windows Vista 64bit"

LangString WelcomeTitleMessage ${LANG_FRENCH} "Bienvenue dans l'installeur de TXM"

LangString WelcomeTextMessage ${LANG_FRENCH} "TXM vous est fourni gracieusement. En contre-partie, dans l'esprit du logiciel libre, vous êtes invité à participer à son amélioration. Pour cela, vous n'êtes pas obligé d'être développeur informatique, vous pouvez :$\n * transmettre vos publications ou supports de cours ;$\n * proposer des corrections ou améliorations ;$\n *  traduire l'interface ou la documentation ;$\n * monter un projet pour adapter TXM à vos besoins ;$\n * etc.$\n$\n Voir la rubrique 'Contribuer' du wiki des utilisateurs de TXM : https://groupes.renater.fr/wiki/txm-users/public/contribuer $\n$\n N'hésitez pas à nous contacter pour de plus amples informations : 'textometrie AT groupes.renater DOT fr'."

LangString FinishTitleMessage ${LANG_FRENCH} "Installation terminée."

LangString FinishTextMessage ${LANG_FRENCH} "L'installation s'est correctement terminée. Une entrée 'TXM' a été ajoutée au menu Démarrer.$\n$\nSi vous souhaitez étiqueter et lemmatiser automatiquement vos corpus avec le logiciel TreeTagger lors de l'import, vous devez suivre les instructions d'installation supplémentaires fournies dans le menu 'Aide > Installer TreeTagger'."

LangString ErrorArch32Message ${LANG_FRENCH} "La version de TXM que vous allez installer est pour un système 32 bits, or votre système semble être 64 bits. Il faudrait télécharger et installer la version 64 bits de TXM. Voulez-vous malgré tout continuer cette installation ?"

LangString ErrorArch64Message ${LANG_FRENCH} "La version de TXM que vous allez installer est pour un système 64 bits, or votre système semble être 32 bits. Il faudrait télécharger et installer la version 32 bits de TXM. Voulez-vous malgré tout continuer cette installation ?"

LangString UninstallWindowsMessage ${LANG_FRENCH} "TXM TXMVERSION est déjà installé sur cette machine. Veuillez cliquer sur 'OK' pour désinstaller cette version au préalable (les corpus seront conservés). Ou cliquez sur 'Annuler' pour interrompre l'installation. Merci de vous assurer que TXM TXMVERSION n'est pas en cours d'exécution pendant l'installation."

;END OF LANG MESSAGES