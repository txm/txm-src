#!/bin/bash
VERSION=`cat VERSION`
root="annonces"

function toHTML {
	source=$1
	output=$2
	title=$3

	rm $output
	echo '<?xml version="1.0" encoding="UTF-8"?>' >> "$output"
	echo "<!DOCTYPE html>" >> "$output"
	echo "<html>" >> "$output"
	echo " <head>" >> "$output"
	echo '  <meta charset="utf-8" />' >> "$output"
	echo "  <title>title</title>" >> "$output"
	echo " </head>" >> "$output"
	echo " <body>" >> "$output"
	echo "  <h1>$title</h1>" >> "$output"
	markdown $source >> "$output"
	echo " </body>" >> "$output"
	echo "</html>" >> "$output"
}

source="${root}/mail.md/${VERSION}_fr.md"
output="${root}/welcome.html/welcome_${VERSION}_fr.html"
toHTML $source $output "Bienvenue dans TXM $VERSION"

source="${root}/readme.md/${VERSION}beta_fr.md"
output="${root}/welcome.html/welcome_${VERSION}beta_fr.html"
toHTML $source $output "Bienvenue dans TXM $VERSION beta"
rm $output

source="${root}/readme.md/${VERSION}_en.md"
output="${root}/welcome.html/welcome_${VERSION}_en.html"
toHTML $source $output "Welcome in TXM $VERSION"
     
source="${root}/readme.md/${VERSION}beta_en.md"
output="${root}/welcome.html/welcome_${VERSION}beta_en.html"
toHTML $source $output "Welcome in TXM $VERSION beta"

source="${root}/readme.md/${VERSION}_ru.md"
output="${root}/welcome.html/welcome_${VERSION}_ru.html"
toHTML $source $output "Welcome in TXM $VERSION"

groovy MD2Mail.groovy
