#!/bin/bash

if [ $# -ne 2 ]
then
  echo "Usage: ./extractDeb.sh *.deb dir"
  exit 1
fi

if [ -d "$2" ]; then
	echo "** extractDeb: outdir already exits: $2."
	exit 1;
fi

if [ ! -f "$1" ]; then
	echo "** extractDeb: deb file does not exits: $1."
	exit 1;
fi

mkdir $2 &&
dpkg-deb -x "$1" "$2" &&
dpkg-deb -e "$1" "$2/DEBIAN"

if [ $? != 0 ]; then
	echo "** extractDeb: failed to extract deb file"
	exit 1;
fi
