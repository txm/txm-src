#echo "get scripts"
DIRNAME=`dirname $0`

echo "LINUX !!"
sh $DIRNAME/BuildLinux64bit.sh
if [ $? != 0 ]; then
	echo "** Failed to get build Linux64"
fi

echo "MacOSX :("
sh $DIRNAME/BuildMacOSXIntel.sh
if [ $? != 0 ]; then
	echo "** Failed to get build Mac OS X"
fi

echo "WINDOWS !!"
sh $DIRNAME/BuildWin64bit.sh
if [ $? != 0 ]; then
	echo "** Failed to get build Win64"
fi

#play end.wav
echo "DONE !!"
