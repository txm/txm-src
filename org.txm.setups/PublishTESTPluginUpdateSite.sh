# this script needs that you create a public key for SF
echo "Sending RCP plugins to update site..."
SF_USER=mdecorde
UPDATESITE="/home/mdecorde/TEMP/updates/plugins"

if [ "$1" = "" ]; then
	echo "uploading to default repository"
elif [[ $1 =~ alpha|beta ]]; then
	echo "uploading to the '$1' repository"
else
	echo "$1: invalid parameter"
	exit 1;
fi

# renaming repository
cd "../../TXM plugins.site"
if [ $? != 0 ]; then
	echo "** updateTESTPluginsUpdateSites.sh : failed to find ../../TXM plugins.site"
	exit 1;
fi

mkdir "$UPDATESITE$1"
rm -rf "$UPDATESITE$1"

mkdir "${UPDATESITE}$1" && 
mkdir "${UPDATESITE}$1/plugins" && 
mkdir "${UPDATESITE}$1/features" && 
cp -r * "${UPDATESITE}$1"

if [ $? != 0 ]; then
	echo "** updateTESTPluginsUpdateSites.sh : failed to update"
	exit 1;
fi

