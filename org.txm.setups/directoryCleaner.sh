#!/bin/bash

if [ $1 ]
then
 find $1 -name ".svn" -exec rm -rfv {} + &&
 find $1 -name "*~" -exec rm -rfv {} + 
else
 echo "need 1 arg : directory to clean"
 exit 1
fi
