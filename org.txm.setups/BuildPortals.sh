version="0.6.1"
STEP=$1

outdir="TXM_Portal_${version}"
rm -rf "$outdir"
mkdir "$outdir"
if [ $? != 0 ]; then
	echo "** Portals.sh : failed to create dir $outdir"
	exit 1;
fi

# copy debian shared files: INSTALL, LICENCE, ...
cp -r shared/portals/* "$outdir"
if [ $? != 0 ]; then
	echo "** Portals.sh : failed to copy shared files in dir $outdir"
	exit 1;
fi

# get manuals from SVN
svn update "$HOME/SVN/TXMSVN/trunk/doc" &&
cp "$HOME/SVN/TXMSVN/trunk/doc/admin manuals//Manuel du portail TXM 0.6.1 - admin.pdf" "$outdir/doc" &&
cp "$HOME/SVN/TXMSVN/trunk/doc/dev manuals/Manuel de reference TXMWEB Dev 0.3_FR.pdf" "$outdir/doc"
if [ $? != 0 ]; then
	echo "** Portals.sh : failed to copy documentation"
	exit 1;
fi

# build txm.war
pwd
currentdir=`pwd`
WARDIR="$HOME/workspace079/org.txm.web/war"
cd $WARDIR &&
rm -f txm.war &&
./buildWar.sh "txm" &&
cd $currentdir
if [ $? != 0 ]; then
	echo "** Portals.sh : failed to build txm.war file"
	exit 1;
fi

pwd
# copy war build
cp -r "$WARDIR/txm.war" "$outdir"
if [ $? != 0 ]; then
	echo "** Portals.sh : failed to copy war file in dir $outdir"
	exit 1;
fi

# remove temporary and SVN files
echo " cleaning TXM_Portal_${version} directory"
bash directoryCleaner.sh "TXM_Portal_${version}"

# copy .TXMWEB configuration directory (must be done after temp file cleaning)
# remove test files and personnal path
echo "copying '.TXMWEB/war' directories"
mkdir "$outdir/.TXMWEB"
cp -r "$HOME/.TXMWEB/war" "$outdir/.TXMWEB/txm"
if [ $? != 0 ]; then
	echo "** Portals.sh : failed to copy war file in dir $outdir"
	exit 1;
fi

# remove all references to $HOME
echo "remove all references to $HOME"
find $outdir/.TXMWEB/* -type f -exec sed -i "s#$HOME#<user_home>#g" {} \;
if [ $? != 0 ]; then
	echo "** Portals.sh : failed to replace $HOME references in $outdir/.TXMWEB/*"
	exit 1;
fi

# erase SMTP setup in txmweb.conf
echo "erase SMTP setup in $outdir/.TXMWEB/txm/txmweb.conf"
find $outdir/.TXMWEB/txm/txmweb.conf -type f -exec sed -i "s#mail.host=.*#mail.host=#g" {} \; && 
find $outdir/.TXMWEB/txm/txmweb.conf -type f -exec sed -i "s#mail.user=.*#mail.user=#g" {} \; && 
find $outdir/.TXMWEB/txm/txmweb.conf -type f -exec sed -i "s#mail.default.from=.*#mail.default.from=#g" {} \; && 
find $outdir/.TXMWEB/txm/txmweb.conf -type f -exec sed -i "s#mail.default.reply=.*#mail.default.reply=#g" {} \; && 
find $outdir/.TXMWEB/txm/txmweb.conf -type f -exec sed -i "s#mail.password=.*#mail.password=#g" {} \; &&
find $outdir/.TXMWEB/txm/txmweb.conf -type f -exec sed -i "s#mail.contact=.*#mail.contact=#g" {} \; &&  

if [ $? != 0 ]; then
	echo "** Portals.sh : failed to remove all references to the SMTP setup in $outdir/.TXMWEB/txm/txmweb.conf"
	exit 1;
fi

# build
echo "building 'TXM_Portal_${version}.zip' file"
zip -9 -r -q "TXM_Portal_${version}.zip" "TXM_Portal_${version}"
if [ $? != 0 ]; then
	echo "** Portals.sh : failed to zip $outdir"
	exit 1;
fi

# send to shared files
echo "Sending to ensldfs.ens-lyon.fr/services//Laboratoires/labo_ana_corpus/Projets/Textométrie/Logiciel/TXM"
smbclient -A auth.txt "//ensldfs.ens-lyon.fr/services" -c "cd \"/Laboratoires/labo_ana_corpus/Projets/Textométrie/Logiciel/TXM\" ; put TXM_Portal_${version}.zip ; exit"
if [ $? != 0 ]; then
	echo "** Portals.sh : failed to send TXM_Portal_${version}.zip to labo_ana_corpus"
	exit 1;
fi
