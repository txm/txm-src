#!/bin/bash

if [ ! $# -eq 2 ]
  then
    echo "** Error: called with $# args. usage: 'pushTXMExtUpdateToGIT.sh VERSION LEVEL'"
    echo "** eg ./pushTXMExtUpdateToGIT.sh 0.8.3 alpha"
    exit 1
fi

CURRENTPATH="$(dirname $(realpath $0))"
echo "PATH='$PATH'"
VERSION=$1

LEVEL=$2
LEVEL="${LEVEL,,}"
TXMGITROOT=~/GIT/txm-software
TXMGITLOCAL="dist/$VERSION/ext/$LEVEL"
TXMGIT=$TXMGITROOT/$TXMGITLOCAL

mkdir -p $TXMGIT

echo "TXM GIT directory: $TXMGIT"

if [ ! -d "$TXMGIT" ]; then
	echo "** Error the GIT directory does not exists: $TXMGIT"
	exit 1;
fi

DIRECTORYTOCOPY=$CURRENTPATH/../sites/org.txm.rcp.updatesite/target/repository

if [ ! -d "$DIRECTORYTOCOPY" ]; then
	echo "** Error: no TXM repository build found: $DIRECTORYTOCOPY"
	exit 1;
fi

echo "Directory to copy: $DIRECTORYTOCOPY"

echo "Preparing GIT push..."
pushd $TXMGIT

if [ -z "$(git status --porcelain $TXMGIT)" ]; then 
	echo "GIT OK"
else 
	echo "Aborting GIT push: $TXMGIT repository is not clean."
	popd
	exit 1
fi

echo "rm previous version in `pwd`..."
git pull
git rm -r binary/*
git rm -r features/*
git rm -r plugins/*

pwd
echo "cp new build..."
cp -rf $DIRECTORYTOCOPY/* .

echo "git add commit push..."
git add --all .
git commit -m "push $LEVEL update of TXM-$VERSION"
git push

popd

echo "Done."
