DIRNAME=`dirname $0`
VERSIONWITHOUTSUB=`cat $DIRNAME/../bundles/org.txm.rcp/VERSION`
SUBVERSION=`cat $DIRNAME/../bundles/org.txm.rcp/SUBVERSION`
VERSION=${VERSIONWITHOUTSUB}${SUBVERSION}

STEP=$1
ARCH="64"
ARCHCODE="x86_64"
TIMESTAMP=`date +"%Y-%m-%d"`
APP="$DIRNAME/TXM_${VERSION}_${TIMESTAMP}_Linux${ARCH}"
BUILDDIR="$DIRNAME/../products/org.txm.rcp.product/target/products/org.txm.rcp.app/linux/gtk/$ARCHCODE"

rm -rf "$APP"
mkdir "$APP"

echo purge | sudo debconf-communicate txm
echo purge | sudo debconf-communicate TXM

# ensure postinst preinst ... rights
chmod 644 $DIRNAME/shared/debian/DEBIAN/templates &&
chmod 755 $DIRNAME/shared/debian/DEBIAN/postinst &&
chmod 755 $DIRNAME/shared/debian/DEBIAN/postrm &&
chmod 755 $DIRNAME/shared/debian/DEBIAN/preinst &&
chmod 755 $DIRNAME/shared/debian/usr/bin/TXM*
if [ $? != 0 ]; then
	echo "** $APP: failed to set rights of postinst preinst ... files "
	exit 1;
fi

# remove old profile that can contains unwanted repositories
rm -f "$BUILDDIR/p2/org.eclipse.equinox.p2.engine/.settings/*" &&
rm -f "$BUILDDIR/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings/*"
if [ $? != 0 ]; then
	echo "** $APP: failed to remove p2 preferences"
	exit 1;
fi

mkdir -p "$APP/usr/lib/TXM"

# copy debian shared files: INSTALL, R libs...
rsync -r  --exclude '.svn' $DIRNAME/shared/debian/* "$APP"
if [ $? != 0 ]; then
	echo "** $APP: failed to get 'debian' shared files"
	exit 1;
fi

# copy shared files: css, xsl, scripts, samples...
rsync -r  --exclude '.svn' $DIRNAME/shared/all/* "$APP/usr/lib/TXM"
if [ $? != 0 ]; then
	echo "** $APP: failed to get shared files"
	exit 1;
fi

# copy eclise arch dependent build
rsync -r  --exclude '.svn' "$BUILDDIR" "$APP/usr/lib/TXM"
cp -rf "$APP/usr/lib/TXM/$ARCHCODE/"* "$APP/usr/lib/TXM"
rm -rf "$APP/usr/lib/TXM/$ARCHCODE"
if [ $? != 0 ]; then
	echo "** $APP: failed to get ${ARCH}bit binaries Linux $ARCHCODE files"
	exit 1;
fi

echo " clean"
bash $DIRNAME/directoryCleaner.sh "$APP"
if [ $? != 0 ]; then
	echo "** $APP: failed to clean Debian package $APP"
	exit 1;
fi

# for multi installation: set the package version to txm-$VERSION
find "$APP/DEBIAN/preinst" -type f -exec sed -i "s/TXMVERSION/$VERSION/g" {} \;
find "$APP/DEBIAN/postinst" -type f -exec sed -i "s/TXMVERSION/$VERSION/g" {} \;
find "$APP/DEBIAN/postrm" -type f -exec sed -i "s/TXMVERSION/$VERSION/g" {} \;
find "$APP/usr/bin/TXM" -type f -exec sed -i "s/TXMVERSIONWITHOUTSUB/${VERSIONWITHOUTSUB}/g" {} \;
find "$APP/usr/bin/TXM" -type f -exec sed -i "s/TXMVERSION/$VERSION/g" {} \;
#find "$APP/DEBIAN/control" -type f -exec sed -i "s/Version: TXMVERSION/Version: TXMVERSION.$DATEQUALIFIER/g" {} \;
find "$APP/DEBIAN/control" -type f -exec sed -i "s/TXMVERSION/${VERSION}/g" {} \;

mv "$APP/usr/bin/TXM" "$APP/usr/bin/TXM-$VERSION"
mv "$APP/usr/lib/TXM" "$APP/usr/lib/TXM-$VERSION"

find "$APP/usr/share/applications/TXM.desktop" -type f -exec sed -i "s/TXMVERSION/$VERSION/g" {} \;
find "$APP/usr/share/applications/TXM.desktop" -type f -exec sed -i "s/TXMVERSION/$VERSION/g" {} \;
find "$APP/usr/share/applications/TXM debug.desktop" -type f -exec sed -i "s/TXMVERSION/$VERSION/g" {} \;
find "$APP/usr/share/applications/TXM debug.desktop" -type f -exec sed -i "s/TXMVERSION/$VERSION/g" {} \;
mv "$APP/usr/share/applications/TXM.desktop" "$APP/usr/share/applications/TXM-$VERSION.desktop"
mv "$APP/usr/share/applications/TXM debug.desktop" "$APP/usr/share/applications/TXM-$VERSION debug.desktop"

mv "$APP/usr/share/doc/txm" "$APP/usr/share/doc/txm-$VERSION"

find "$APP/usr/share/lintian/overrides/txm" -type f -exec sed -i "s/TXMVERSION/$VERSION/g" {} \;
mv "$APP/usr/share/lintian/overrides/txm" "$APP/usr/share/lintian/overrides/txm-$VERSION"
mv "$APP/usr/share/TXM" "$APP/usr/share/TXM-$VERSION"

# build
sudo fakeroot dpkg-deb --build "$APP"
#add "--faked faked-tcp" when called from windows WSL
if [ $? != 0 ]; then
	echo "** $APP: failed to build debian package $APP"
	exit 1;
fi

rm -f "${APP}_installer.deb"
mv $APP.deb ${APP}_installer.deb
