#!/bin/bash

if [ ! $# -eq 2 ]
  then
    echo "** Error: called with $# args. usage: 'pushTXMUpdateToGIT.sh VERSION LEVEL'"
    echo "** eg ./pushTXMUpdateToGIT.sh 0.8.3 alpha"
    exit 1
fi

CURRENTPATH="$(dirname $(realpath $0))"
echo "PATH='$PATH'"
VERSION=$1

LEVEL=$2
LEVEL="${LEVEL,,}"
TXMGITROOT=~/GIT/txm-software
TXMGITLOCAL="dist/$VERSION/main/$LEVEL"
TXMGIT=$TXMGITROOT/$TXMGITLOCAL

mkdir -p $TXMGIT

if [ ! -d "$TXMGIT" ]; then
	echo "** Error the GIT directory does not exists: $TXMGIT"
	exit 1;
fi
#../products/org.txm.rcp.product/target/repository
if [ ! -d "$CURRENTPATH/../products/org.txm.rcp.product/target/repository" ]; then
	echo "** Error: no TXM repository build found: $CURRENTPATH/products/org.txm.rcp.product/target/repository"
	exit 1;
fi

#echo "Preparing update files..."

#MACOSXBUILD=`ls $CURRENTPATH/TXM_*_MacOSX/Applications/TXM-*.app/Contents/MacOS/TXM|tail -1`
#if [ "$MACOSXBUILD" = "" ]; then
#	echo "Error: no MAC OS X build. Call BuildMacOSX before"
#	exit 1
#fi

#echo "Fixing Mac OS X binary archive..."
#cp -r `ls -d $CURRENTPATH/TXM_*_MacOSX/Applications/TXM-*.app/Contents/MacOS|tail -1` . && 
#cp -f `ls $CURRENTPATH/TXM_*_MacOSX/Applications/TXM-*.app/Contents/Info.plist|tail -1` Info.plist && 
#zip `ls $CURRENTPATH/../products/org.txm.rcp.product/target/repository/binary/org.txm.rcp.app.executable.cocoa.macosx.x86_64_*|tail -1` -d Info.plist &&
##zip `ls $CURRENTPATH/../products/org.txm.rcp.product/target/repository/binary/org.txm.rcp.app.executable.cocoa.macosx.x86_64_*|tail -1` -ur Info.plist &&
#zip `ls $CURRENTPATH/../products/org.txm.rcp.product/target/repository/binary/org.txm.rcp.app.executable.cocoa.macosx.x86_64_*|tail -1` -d MacOS &&
##zip `ls $CURRENTPATH/../products/org.txm.rcp.product/target/repository/binary/org.txm.rcp.app.executable.cocoa.macosx.x86_64_*|tail -1` -ur MacOS &&
#rm -r MacOS &&
#rm Info.plist

if [ $? != 0 ]; then
	echo "** $APP: failed to prepare MacOS files"
	rm -r MacOS
	rm Info.plist
	exit 1;
fi

echo "Preparing GIT push..."
pushd $TXMGIT

if [ -z "$(git status --porcelain $TXMGIT)" ]; then 
	echo "GIT OK"
else 
	echo "Aborting GIT push: $TXMGIT repository is not clean."
	popd
	exit 1
fi

echo "rm previous version in `pwd`..."
git pull
git rm -r binary/*
git rm -r features/*
git rm -r plugins/*

pwd
echo "cp new build..."
cp -rf $CURRENTPATH/../products/org.txm.rcp.product/target/repository/* .

echo "git add commit push..."
git add --all .
git commit -m "push $LEVEL update of TXM-$VERSION"
git push

popd

echo "Done."
