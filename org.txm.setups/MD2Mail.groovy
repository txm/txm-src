File mdFile = new File("annonces/mail.md/0.7.6_fr.md")
File mailFile = new File("annonces/mail.txt/0.7.6_fr.txt")
File readmeFile = new File("annonces/readme.md/0.7.6_fr.md")

String text = mdFile.getText("UTF-8")
mailFile.withWriter("UTF-8") { writer ->
	text = text.replaceAll("\t", '  ')
	text = text.replaceAll("\\* (.+\n)", ' - $1')
	
	text = text.replaceAll("\\[([^\\]]+)\\]\\([^\\)]+\\)", '$1')
	text = text.replaceAll("([^\n]+)\n===\n", 'Bonjour à tous,\n')
	text = text.replaceAll("([^\n]+)\n---\n", '*$1*\n')
	writer.print text
}

text = mdFile.getText("UTF-8")
readmeFile.withWriter("UTF-8") { writer ->
	text = text.replaceAll("\n===\n", '')
	text = text.replaceAll("Nous avons le plai.+\n",'')
	writer.print text
}

println 'fin'
System.exit()