#!/bin/bash

echo "Sending RCP export to update site..."

UPDATESITE="$HOME/workspace079/org.txm.setups/exportRCP/repository"
# UPDATE THIS MOUNT PATH TO YOURS
SERVERPATH="/run/user/1001/gvfs/sftp:host=vm-web-preprod.pun.ens-lyon.fr,user=mdecorde"
DISTANTPATH="$SERVERPATH/textometrie/dist/main/"

if [ "$1" = "" ] ; then
	echo "You need to specify the update site level to update"
	exit 1
elif [[ $1 =~ alpha|beta|dev ]] ; then
	echo "Uploading to the '$1' dist/main repository"
	DISTANTPATH="$SERVERPATH/textometrie/dist/main/"
else
	echo "Uploading to the '$1' project update site repository"
	DISTANTPATH="$SERVERPATH/textometrie/dist/"
fi

if [ ! -d "$UPDATESITE" ]; then
	echo "** $UPDATESITE is missing."
	exit 1;
fi

# DO IT !
echo "ls" > pushFilesToPreprod.ftp
echo "exit" > pushFilesToPreprod.ftp

echo "Remove old files"
rm -rf "$DISTANTPATH$1/"*
if [ $? != 0 ]; then
    echo "Could not delete files of $DISTANTPATH$1";
    exit 1 ;
fi

echo "Copy new files"
mkdir "$DISTANTPATH"
mkdir "$DISTANTPATH$1"
cp -rf "$UPDATESITE/"* "$DISTANTPATH$1"
if [ $? != 0 ]; then
    echo "Could not copy files of $UPDATESITE to $DISTANTPATH$1";
    exit 1 ;
fi

echo "Rename commandes-succes.txt file"
mv "$SERVERPATH/commandes-succes.txt" "$SERVERPATH/commandes.txt"
if [ $? != 0 ]; then
    echo "Could not rename commandes-succes.txt file";
    exit 1 ;
fi

echo "$1 update site updated"
