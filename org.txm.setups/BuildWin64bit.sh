DIRNAME=`dirname $0`
VERSION=`cat $DIRNAME/../bundles/org.txm.rcp/VERSION`
SUBVERSION=`cat $DIRNAME/../bundles/org.txm.rcp/SUBVERSION`
FULLVERSION=${VERSION}${SUBVERSION}
STEP=$1
ARCH="64"
ARCHCODE="x86_64"
TIMESTAMP=`date +"%Y-%m-%d"`
APP="$DIRNAME/TXM_${FULLVERSION}_${TIMESTAMP}_Win${ARCH}_installer"
BUILDDIR="$DIRNAME/../products/org.txm.rcp.product/target/products/org.txm.rcp.app/win32/win32/$ARCHCODE"


echo "BUILDING $APP..."
echo "Make dir"
rm -rf "${APP}"
mkdir "${APP}"

echo "copy shared files: css, xsl, scripts, samples..."
rsync -r  --exclude '.svn' "$DIRNAME/shared/all"/* "${APP}"

if [ $? != 0 ]; then
	echo "** ${APP}: copy Win shared files failed"
	exit 1;
fi

echo "convert licence files encoding to Windows-1252"
iconv -f utf8 -t WINDOWS-1252 $DIRNAME/shared/all/license_agreement_fr.txt > "$APP/license_agreement_fr.txt"
iconv -f utf8 -t WINDOWS-1252 $DIRNAME/shared/all/license_agreement.txt > "$APP/license_agreement.txt"
iconv -f utf8 -t WINDOWS-1252 $DIRNAME/shared/all/LICENSE_FR.TXT > "$APP/LICENSE_FR.TXT"
iconv -f utf8 -t WINDOWS-1252 $DIRNAME/shared/all/LICENSE.TXT > "$APP/LICENSE.TXT"

echo "copy win shared files: *.ico, TestPreInstall.jar..."
rsync -r  --exclude '.svn' "$DIRNAME/shared/win"/* "${APP}"

if [ $? != 0 ]; then
	echo "** ${APP}: copy Win arch files failed"
	exit 1;
fi


rm -f "$BUILDDIR/p2/org.eclipse.equinox.p2.engine/.settings/*" &&
rm -f "$BUILDDIR/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings/*"
if [ $? != 0 ]; then
	echo "** $APP: failed to remove p2 preferences"
	exit 1;
fi

echo "copy eclipse arch dependent build"
rsync -r --exclude '.svn' "$BUILDDIR/"* "${APP}"

if [ $? != 0 ]; then
	echo "** ${APP}: copy Win build files failed"
	exit 1;
fi

#echo "copy cqp JNI libs near TXM executable"
echo "copy CQP binary files"
cp "$DIRNAME/../bundles/org.txm.libs.cqp.win32/res/win64"/*.dll "${APP}/" &&
cp "$DIRNAME/../bundles/org.txm.libs.cqp.win32/res/win64"/*.dll "${APP}/TXM"

if [ $? != 0 ]; then
	echo "** ${APP}: copy Win lib files failed"
	exit 1;
fi

echo "clean"
bash $DIRNAME/directoryCleaner.sh "$APP"
if [ $? != 0 ]; then
	echo "** $APP: failed to clean win64 setup $APP"
	exit 1;
fi

echo "set TXMVERSION and TIMESTAMP in build files"
find "$APP/txm.nsi" -type f -exec sed -i "s/TXMDATEVERSION/${FULLVERSION}_${TIMESTAMP}/g" {} \;
find "$APP/txm.nsi" -type f -exec sed -i "s/TXMVERSION/${FULLVERSION}/g" {} \;
find "$APP/txm_admin.nsi" -type f -exec sed -i "s/TXMDATEVERSION/${FULLVERSION}_${TIMESTAMP}/g" {} \;
find "$APP/txm_admin.nsi" -type f -exec sed -i "s/TXMVERSION/${FULLVERSION}/g" {} \;
find "$APP/messages.nsi" -type f -exec sed -i "s/TXMVERSION/${FULLVERSION}/g" {} \;
find "$APP/TXM.bat" -type f -exec sed -i "s/TXMVERSION/${VERSION}/g" {} \;

echo "build executable"
makensis -V1 ${APP}/txm.nsi

if [ $? != 0 ]; then
	echo "** $APP: failed to build win64 setup $APP"
	exit 1;
fi

echo "build ADMIN executable"
makensis -V1 ${APP}/txm_admin.nsi

if [ $? != 0 ]; then
	echo "** $APP: failed to build win64 setup $APP"
	exit 1;
fi

if [ ! -e "${APP}.exe" ]; then
	echo "** ${APP}.exe : not builded"
	exit 1;
fi

chmod +x "${APP}.exe"
chmod +x "${APP}_admin.exe"
