TOOLBOXDIRECTORY="$HOME/workspace079/org.txm.toolbox"
SVNDIR="$HOME/SVN/TXMSVN/trunk"
SVNDOCTRUNK="$SVNDIR/doc"
SHARED="./shared"
SHAREDALL="$SHARED/all"


svn update "${SVNDIR}"

# copy TXM sample corpora files files
mkdir  "$SHAREDALL/samples"
cp -rf "${SVNDIR}/corpora/tdm80j" "$SHAREDALL/samples/tdm80j"
if [ $? != 0 ]; then
	echo "** TXM sample corpora copy failed"
	exit 1;
fi

# copy TXM documentation files files
mkdir "$SHAREDALL/doc"
cp -f "${SVNDOCTRUNK}/user manuals/txm-manual-fr.pdf" "$SHAREDALL/doc/Manuel de TXM 0.7 FR.pdf"
if [ $? != 0 ]; then
	echo "** TXM REFMAN copy failed"
	exit 1;
fi

echo "** Shared files copy: success !"
