package org.txm.chartsengine.core.styling.base;

import org.txm.chartsengine.core.results.ChartResult;

public interface Stylable<C extends ChartResult> {

	public StylerCatalog<C> getCatalogs();
}

