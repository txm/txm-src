/**
 *
 */
package org.txm.chartsengine.core.styling.definitions;

/**
 * An element definition representing an instruction definition : what to do
 *
 * @author s, mdecorde
 *
 */
public abstract class InstructionDefinition<InputType extends Object> extends ElementDefinition {


	/**
	 *
	 * @param id
	 * @param label
	 */
	public InstructionDefinition(String id, String label) {
		super(id, label);
	}



	//	/**
	//	 * Adds a value definition to available definitions list.
	//	 *
	//	 * @param valueDef
	//	 * @param valueDef
	//	 */
	//	public void addAvailableAssociation(FunctionDefinition<?, ?> function, ParameterDefinition<?> parameter) {
	//		this.availableAssociations.add(new Association(function, parameter));
	//	}



	//
	//	/**
	//	 * Gets a value definition from the available definitions according to its id.
	//	 *
	//	 * @param id
	//	 * @return
	//	 */
	//	public ValueDefinition getValueDefinition(String id) {
	//		ValueDefinition value = null;
	//		for (int i = 0; i < this.availableParameterDefinitions.size(); i++) {
	//			if (this.availableParameterDefinitions.get(i).getID().equals(id)) {
	//				value = this.availableParameterDefinitions.get(i);
	//			}
	//		}
	//		return value;
	//	}



	//	/**
	//	 * @return the availableDefinitions
	//	 */
	//	public ArrayList<Association> getAvailableAssociations() {
	//		return this.availableAssociations;
	//	}


	//	/**
	//	 * Gets the dump of this definition data.
	//	 *
	//	 * @param indentation the indentation level
	//	 * @return
	//	 */
	//	public String getDump(int indentation) {
	//
	//		return super.getDump(indentation)+ " inputType: "+getInputType();
	////		StringBuffer dump = new StringBuffer(super.getDump(indentation));
	////		StringBuffer indentationStr = new StringBuffer();
	////
	////		for (int i = 0; i < indentation; i++) {
	////			indentationStr.append("     ");
	////		}
	////
	////		dump.append(indentationStr);
	////
	////		dump.append(System.lineSeparator()).append("       Value definitions:").append(System.lineSeparator());
	////		for (int i = 0; i < this.availableAssociations.size(); i++) {
	////			dump.append(this.availableAssociations.get(i).function.getDump(2)).append(System.lineSeparator());
	////			dump.append(this.availableAssociations.get(i).parameter.getDump(2)).append(System.lineSeparator());
	////		}
	////		return dump.toString();
	//	}

	// FIXME: SJ: tests
	public static void main(String[] args) {
		//		StylingInstructionDefinition<?> instructionDef = StylingInstructionsCatalog.getInstance().getInstructionDefinition(StylingInstruction.ID_SIZE);
		//		System.out.println(instructionDef.getDump());
	}

}
