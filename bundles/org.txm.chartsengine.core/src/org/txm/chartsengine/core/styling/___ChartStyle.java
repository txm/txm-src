/**
 * 
 */
package org.txm.chartsengine.core.styling;

import java.awt.Shape;

/**
 * @author s
 *
 */
public class ___ChartStyle {


	Shape shape;

	float labelSize;


	/**
	 * 
	 */
	public ___ChartStyle() {
		// TODO Auto-generated constructor stub
	}



	public Shape getShape() {
		return this.shape;
	}



	public float getLabelSize() {
		return labelSize;
	}



	public void setLabelSize(float labelSize) {
		this.labelSize = labelSize;
	}



	public void setShape(Shape shape) {
		this.shape = shape;
	}



}
