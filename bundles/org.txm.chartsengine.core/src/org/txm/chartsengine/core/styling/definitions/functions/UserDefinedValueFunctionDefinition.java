package org.txm.chartsengine.core.styling.definitions.functions;

/**
 * A function returning the value selected by a user. Useful later to set the return value from an user interface of values, the interface will know a value is asked
 * 
 * Warning the interface must set the "settings" of the Instruction to avoid NPE later
 * 
 * @author mdecorde
 *
 */
public class UserDefinedValueFunctionDefinition extends TransformationFunctionDefinition {

	/**
	 * 
	 * @param id
	 * @param label
	 * @param data the return is fixed value
	 */
	public UserDefinedValueFunctionDefinition(String id, String label) {

		super(id, label);
	}

	@Override
	public Object apply(Object settings, Object n) {
		return settings;
	}
}
