package org.txm.chartsengine.core.styling.definitions.functions;

/**
 * A function to use for Selection instruction. They always return a boolean value
 * 
 * @author mdecorde
 *
 */
public abstract class TestFunctionDefinition extends FunctionDefinition {

	private static final long serialVersionUID = 792917124957005389L;

	public TestFunctionDefinition(String id, String label) {

		super(id, label);
	}
}
