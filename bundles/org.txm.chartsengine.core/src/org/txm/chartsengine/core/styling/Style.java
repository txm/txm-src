/**
 *
 */
package org.txm.chartsengine.core.styling;

import java.awt.Color;
import java.awt.Shape;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.txm.chartsengine.core.styling.catalogs.StylingInstructionsCatalog;
import org.txm.chartsengine.core.styling.instructions.StylingInstruction;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;

/**
 * Represent a style instruction to apply.
 * 
 * The Style can be a compilation of several style instructions to apply -> it is then a pre-defined style
 * 
 * @author s, mdecorde
 *
 */
public class Style implements Serializable {

	private static final long serialVersionUID = 177772422209272705L;

	protected String name;

	protected ArrayList<StylingInstruction<?>> stylingInstructions = new ArrayList<>();

	/**
	 *
	 */
	public Style() {
		this(TXMCoreMessages.common_unnamed);
	}

	@Override
	public Style clone() {

		Style tmp = new Style(this.name);
		for (StylingInstruction<?> si : this.stylingInstructions) {
			tmp.stylingInstructions.add(si.clone());
		}
		
		return tmp;
	}

	/**
	 *
	 */
	public Style(String name) {
		this.name = name;
		this.stylingInstructions = new ArrayList<>();
	}

	public boolean isPredefined() {
		return this.stylingInstructions.size() > 1;
	}

	/**
	 *
	 * @param instruction
	 */
	public void addInstruction(StylingInstruction<?> instruction) {
		this.stylingInstructions.add(instruction);
	}

	/**
	 *
	 * @param instruction
	 * @return
	 */
	public boolean removeInstruction(StylingInstruction<?> instruction) {
		return this.stylingInstructions.remove(instruction);
	}

	/**
	 *
	 * @param instruction
	 */
	public void addInstructions(StylingInstruction<?>... instructions) {
		for (StylingInstruction<?> instruction : instructions) {
			this.stylingInstructions.add(instruction);
		}
	}

	public void addInstructions(ArrayList<StylingInstruction<?>> instructions) {

		for (StylingInstruction<?> instruction : instructions) {
			this.stylingInstructions.add(instruction);
		}
	}

	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		if (TXMCoreMessages.common_unnamed.equals(name)) {
			return stylingInstructions.toString();
		} else {
			return name;
		}
	}

	public ArrayList<StylingInstruction<?>> getStylingInstructions() {
		return this.stylingInstructions;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (o instanceof Style s) {
			return this.getName().equals(s.getName());
		}
		else {
			return false;
		}
	}

	/**
	 *
	 * @param indentation the indentation level
	 * @return
	 */
	public String getDump(int indentation) {

		StringBuffer dump = new StringBuffer();
		StringBuffer indentationStr = new StringBuffer();

		for (int i = 0; i < indentation; i++) {
			indentationStr.append("	"); //$NON-NLS-1$
		}

		dump.append(indentationStr);
		dump.append(TXMCoreMessages.bind("	* {0} - \"{1}\", numbers of instructions: {2}", this.getClass().getSimpleName(), this.name, this.stylingInstructions.size())); //$NON-NLS-1$

		indentation++;
		for (int i = 0; i < this.stylingInstructions.size(); i++) {
			dump.append(System.lineSeparator()).append("		"+this.stylingInstructions.get(i).getDump(indentation)); //$NON-NLS-1$
		}


		return dump.toString();
	}

	/**
	 *
	 * @return
	 */
	public String getDump() {
		return this.getDump(0);
	}



	// FIXME: SJ: tests
	public static void main(String[] args) {


		StylingInstructionsCatalog catalog = new StylingInstructionsCatalog();
		// Rows
		Style style = new Style("Use case #1 - substyle 1"); //$NON-NLS-1$
		style.addInstruction(new StylingInstruction(catalog, StylingInstruction.ID_SHAPE_REPLACEMENT, StylingInstruction.VALUE_ID_SHAPE_DISK));


		Style style2 = new Style("Use case #1 - substyle 2"); //$NON-NLS-1$
		style2.addInstruction(new StylingInstruction(catalog, StylingInstruction.ID_COLOR, StylingInstruction.VALUE_ID_COLOR_GREEN));


		Style style3 = new Style("Use case #1 - substyle 3, transparency on Q12 and size on Cont1"); //$NON-NLS-1$
		style3.addInstruction(new StylingInstruction(catalog, StylingInstruction.ID_TRANSPARENCY, StylingInstruction.VALUE_ID_PROPERTY_Q12));
		style3.addInstruction(new StylingInstruction(catalog, StylingInstruction.ID_SIZE, StylingInstruction.VALUE_ID_PROPERTY_CONT1));


		// Cols
		// style 1 + style 3
		Style style4 = new Style("Use case #1 - substyle 4, politique de gauche en rouge"); //$NON-NLS-1$
		style4.addInstruction(new StylingInstruction(catalog, StylingInstruction.ID_COLOR, StylingInstruction.VALUE_ID_COLOR_RED));

		Style style5 = new Style("Use case #1 - substyle 5, politique de droite en bleu"); //$NON-NLS-1$
		style5.addInstruction(new StylingInstruction(catalog, StylingInstruction.ID_COLOR, StylingInstruction.VALUE_ID_COLOR_BLUE));
		// Obligé de faire deux styles différents car le sélecteur est défini sur le style style entier (repasser le sélectleur dans la StyleInstruction ?)

		System.out.println(style.getDump());
		System.out.println(style2.getDump());
		System.out.println(style3.getDump());
		System.out.println(style5.getDump());

		System.out.println();
		StylingInstructionsCatalog.getInstance().dump();

	}

	public void updateChartStyle(TXMResult result, int serie, int item, HashMap<Integer, String> rowLabelStyle, HashSet<Integer> rowHiddenStyle, HashMap<Integer, Double> rowPointSizeStyle,
			HashMap<Integer, Color> rowPointColorStyle, HashMap<Integer, Double> rowPointAlphaStyle, HashMap<Integer, Shape> rowPointShapeStyle, HashMap<Integer, Double> rowFontSizeStyle,
			HashMap<Integer, Color> rowFontColorStyle, HashMap<Integer, Double> rowFontAlphaStyle, HashMap<Integer, String> rowFontFamilyStyle, HashMap<Integer, Integer> rowFontStyleStyle) {
		// TODO Auto-generated method stub
		for (StylingInstruction<?> style : getStylingInstructions()) {
			style.updateChartStyle(result, serie, item,
					rowLabelStyle, rowHiddenStyle, rowPointSizeStyle, rowPointColorStyle, rowPointAlphaStyle, rowPointShapeStyle, rowFontSizeStyle, rowFontColorStyle, rowFontAlphaStyle,
					rowFontFamilyStyle, rowFontStyleStyle);
		}
	}

}
