package org.txm.chartsengine.core.styling.catalogs;

import java.util.LinkedHashMap;

import org.txm.chartsengine.core.styling.definitions.functions.FunctionDefinition;

/**
 * Container of FunctionDefinition of a certain type
 * 
 * @author mdecorde
 *
 * @param <FunctDef>
 */
public class FunctionsCatalog<FunctDef extends FunctionDefinition> extends LinkedHashMap<String, FunctDef> {

	private static final long serialVersionUID = 3782776743040910997L;

	/**
	 * Convenience method to put(function.getID(), function)
	 * 
	 * @param function function definition to add
	 * 
	 * @return the function added
	 */
	public FunctDef add(FunctDef function) {
		this.put(function.getID(), function);
		return function;
	}
}
