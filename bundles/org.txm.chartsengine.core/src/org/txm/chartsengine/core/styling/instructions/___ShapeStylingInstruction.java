/**
 *
 */
package org.txm.chartsengine.core.styling.instructions;

/**
 * @author s
 *
 */
public class ___ShapeStylingInstruction extends StylingInstruction {



	//	static {
	//		// Shape
	//		// dimensions scaling factor (the same used in default theme for the replaced shapes have same dimension than non-replaced)
	//		float itemShapesScalingFactor = 1.2f;
	//
	//		float circleSize = 6.4f * itemShapesScalingFactor;
	//		GeneralPath shape;
	//		float scalingFactor;
	//
	//
	//		// Diamond shape
	//		scalingFactor = (itemShapesScalingFactor * 3.4f);
	//		shape = new GeneralPath();
	//		shape.moveTo(0.0f, -scalingFactor);
	//		shape.lineTo(scalingFactor, 0.0f);
	//		shape.lineTo(0.0f, scalingFactor);
	//		shape.lineTo(-scalingFactor, 0.0f);
	//		shape.closePath();
	//		InstructionsCatalog.addAvailableInstructionValue(StylingInstruction.INTERNAL_TYPE_SHAPE_REPLACEMENT, new ValueDefinition("Diamant", StylingInstruction.INTERNAL_VALUE_SHAPE_DIAMOND, shape));
	//
	//
	//		// Square
	//		InstructionsCatalog.addAvailableInstructionValue(StylingInstruction.INTERNAL_TYPE_SHAPE_REPLACEMENT, new ValueDefinition("Carré", StylingInstruction.INTERNAL_VALUE_SHAPE_SQUARE, new Rectangle2D.Double(-3.0, -3.0, 6.0, 6.0)));
	//
	//
	//
	//		// FIXME: SJ: should be "Circle" when we'll add the "filled" property
	//		// Disk
	//		InstructionsCatalog.addAvailableInstructionValue(StylingInstruction.INTERNAL_TYPE_SHAPE_REPLACEMENT, new ValueDefinition("Disque", StylingInstruction.INTERNAL_VALUE_SHAPE_DISK, new Ellipse2D.Float((float) (-circleSize / 2),
	//				(float) (-circleSize / 2), circleSize, circleSize)));
	//
	//
	//
	//		// Triangle
	//		scalingFactor = (itemShapesScalingFactor * 3.2f);
	//		shape = new GeneralPath();
	//		shape.moveTo(0.0f, -scalingFactor);
	//		shape.lineTo(scalingFactor, scalingFactor);
	//		shape.lineTo(-scalingFactor, scalingFactor);
	//		shape.closePath();
	//		InstructionsCatalog.addAvailableInstructionValue(StylingInstruction.INTERNAL_TYPE_SHAPE_REPLACEMENT, new ValueDefinition("Triangle vers le haut", StylingInstruction.INTERNAL_VALUE_SHAPE_UP_TRIANGLE, shape));
	//	}


	/**
	 * @param type
	 * @param value
	 */
	public ___ShapeStylingInstruction() {
		super(null, "Symbole", StylingInstruction.ID_SHAPE_REPLACEMENT); //$NON-NLS-1$
	}

}
