package org.txm.chartsengine.core.styling;

import java.util.ArrayList;

import org.txm.chartsengine.core.styling.instructions.SelectionInstruction;
import org.txm.core.results.TXMResult;

/**
 * Manages a list of Selection instructions
 * 
 * This first implementation will ensure all instructions are validated
 * 
 * @author mdecorde
 *
 */
public class Selector extends ArrayList<SelectionInstruction> {

	private static final long serialVersionUID = 2623525104135801593L;

	/**
	 * Test all instructions. Stop if one fails.
	 * 
	 * @param serie
	 * @param item
	 * @return
	 */
	public boolean doIt(TXMResult result, int serie, int item) {
		
		if (this.size() == 0) return true; // no test to pass
		
		int deactivated = 0;
		for (SelectionInstruction s : this) {
			if (s.getFunction() == null || s.getFunction().getID().length() == 0) continue; // ignore selection instruction
			
			if (!s.isActivated()) {
				deactivated++; // skip this selection instruction
				continue;
			}
			
			if (!s.test(result, serie, item)) {
				return false;
			}
		}
		if (deactivated == this.size()) {
			return false; // true if all instructions are deactivated == rule.isActivated()
		}
		
		return true;
	}

	public SelectionInstruction getInstructionForType(String c) {
		for (SelectionInstruction s : this) {
			if (c.equals(s.getInstruction().getID())) {
				return s;
			}
		}
		return null;
	}

}
