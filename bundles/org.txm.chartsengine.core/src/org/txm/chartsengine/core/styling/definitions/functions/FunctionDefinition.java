/**
 *
 */
package org.txm.chartsengine.core.styling.definitions.functions;

import org.txm.chartsengine.core.styling.definitions.ElementDefinition;

/**
 * A function returns a value of ReturnType class and defines the transformations done (can be identity or a linear gradient). It can be parameterized with a "settings" of SettingsType class.
 * 
 * The instruction will get the value using the apply method and optionally an argument of ArgumentType class
 * 
 * @author s, mdecorde
 *
 */
public abstract class FunctionDefinition extends ElementDefinition {

	private static final long serialVersionUID = -7712261763425674856L;

	/**
	 *
	 * @param name
	 * @param id
	 * @param value
	 */
	public FunctionDefinition(String id, String label) {
		super(id, label);
	}

	@Override
	public String getDump(int indentation) {

		StringBuffer dump = new StringBuffer(super.getDump(indentation));
		return dump.toString();
	}

	public abstract Object apply(Object settings, Object arguments);

}
