/**
 *
 */
package org.txm.chartsengine.core.styling.definitions;

import java.io.Serializable;

import org.txm.core.results.TXMResult;

/**
 * A ParameterDefinition allow to chose what will be given the the FunctionDefinition when the apply method is called.
 * 
 * the class contains subclasses of commons Data types to be used in the implementations of Parameters
 * 
 * @author s, mdecorde
 *
 */
public abstract class ParameterDefinition extends ElementDefinition {

	private static final long serialVersionUID = 7602030545776260940L;

	// FIXME: SJ: useless?
	public record Any() implements Serializable {
	};

	/**
	 *
	 * @param name
	 * @param id
	 */
	public ParameterDefinition(String id, String label) {
		super(id, label);
	}

	public abstract Object getValue(TXMResult result, int serie, int item); // TODO trop lié à JFreechart
}
