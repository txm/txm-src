package org.txm.chartsengine.core.styling.definitions.functions;

import java.awt.Color;

import org.txm.utils.serializable.Color1Color2;
import org.txm.utils.serializable.ValueMinMax;

/**
 * A function to be used for StylingInstructions. Useful later to fill interface values with instanceof tests
 * 
 * @author mdecorde
 *
 */
public class GradientFunctionDefinition extends TransformationFunctionDefinition {

	public GradientFunctionDefinition(String id, String label) {

		super(id, label);
	}

	@Override
	public Color apply(Object settings, Object arg) {

		Color1Color2 c = null;
		if (settings instanceof Color1Color2) {
			c = (Color1Color2) settings;
		}
		ValueMinMax d = null;
		if (arg instanceof ValueMinMax) {
			d = (ValueMinMax) arg;
		}
		
		if (d == null) {
			return null;
		}
		
		float l = (float) ((d.value().doubleValue() - d.min().doubleValue()) / (d.max().doubleValue() - d.min().doubleValue()));

		int r = (int) (Math.min(c.color1().getRed(), c.color2().getRed()) + (Math.abs(c.color1().getRed() - c.color2().getRed()) * l));
		int g = (int) (Math.min(c.color1().getGreen(), c.color2().getGreen()) + (Math.abs(c.color1().getGreen() - c.color2().getGreen()) * l));
		int b = (int) (Math.min(c.color1().getBlue(), c.color2().getBlue()) + (Math.abs(c.color1().getBlue() - c.color2().getBlue()) * l));
		int a = (int) (Math.min(c.color1().getAlpha(), c.color2().getAlpha()) + (Math.abs(c.color1().getAlpha() - c.color2().getAlpha()) * l));
		return new Color(
				r,
				g,
				b,
				a);
	}
}
