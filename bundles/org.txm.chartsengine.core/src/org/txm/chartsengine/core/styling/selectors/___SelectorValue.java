/**
 *
 */
package org.txm.chartsengine.core.styling.selectors;


/**
 * @author s
 *
 */
public class ___SelectorValue {



	protected String name;


	protected String internalName;


	protected Object value;


	/**
	 *
	 */
	public ___SelectorValue(String name, String internalName, Object value) {
		this.name = name;
		this.internalName = internalName;
		this.value = value;
	}



	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}



	public String getName() {
		return this.name;
	}



	public String getInternalName() {
		return this.internalName;
	}



	public Object getValue() {
		return this.value;
	}


	public String getDump() {
		return this.name + (" (") + this.internalName + "), " + this.value;
	}

}
