package org.txm.chartsengine.core.styling;

import java.awt.Paint;
import java.awt.Shape;
import java.util.List;
import java.util.Map;

/**
 * Le styler prendrait une feuille de style, se chargerait de la sélection et donc transformerait la sélection index : series/items et ou pointeur sur l'ITEmEntity ?
 * puis ensuite retournerait les Shape, Font, Color, etc. adaptée ?
 * Et le Renderer utiliserait ce styler ?
 *
 *
 * @author s
 *
 */
public class ___ChartStyler {


	List<___ChartStyle> styles;

	Map<Integer, Map<Integer, ___ChartStyle>> seriesAndItemsStyles;



	/**
	 *
	 */
	public ___ChartStyler() {

		// TODO Auto-generated constructor stub
	}


	public Shape getItemShape(int series, int item) {

		Shape shape = null;

		try {
			Map<Integer, ___ChartStyle> m = this.seriesAndItemsStyles.get(series);
			___ChartStyle style = m.get(item);
			shape = style.getShape();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return shape;

	}



	public Paint getItemColor(int series, int item) {
		return null;

		//ici comment retourner directement une interpolation de couleur ou bien un mapping ou bien une unterpolation de dégradé de gris ?
		// le problème n'existe pas si c'est fait à l'initialisation plutôt qu'en templs réel par interpolation dynamique


		// du coup gros doute ici  pour les dégradés, est-ce q'on pré-rempli une map comme pour CACompileStyle ? ou bien on interpole directement ? Est-ce qu'il y a besoin que ce ne soit pas figé ?
		// par ailleurs est-ce que par exemple l'interpolation sur un dégrédé de couleur sur la Mass doivent gérer vraiement la valeur de la masse ? ou bien seulement le nombre de ligne ?
		// bref, les step doivent-ils être différents en fonction de la valeur ?

	}


}
