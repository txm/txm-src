package org.txm.chartsengine.core.styling.definitions.functions;

/**
 * A function returning always the same value. It extends TransformationFunctionDefinition because it has no use for the SelectionInstructionDefinitions
 * 
 * @author mdecorde
 *
 */
public class ConstantFunctionDefinition extends TransformationFunctionDefinition {

	Object value;

	/**
	 * 
	 * @param id
	 * @param label
	 * @param data the return is fixed value
	 */
	public ConstantFunctionDefinition(String id, String label, Object value) {

		super(id, label);
		this.value = value; // stores the value in the settings objet to avoid unnecessary storage
	}

	@Override
	public Object apply(Object settings, Object arguments) {

		return value;
	}
}
