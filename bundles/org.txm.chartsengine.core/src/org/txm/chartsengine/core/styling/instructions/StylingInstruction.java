/**
 *
 */
package org.txm.chartsengine.core.styling.instructions;

import java.awt.Color;
import java.awt.Font;
import java.awt.Shape;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Pattern;

import org.txm.chartsengine.core.styling.catalogs.InstructionsCatalog;
import org.txm.chartsengine.core.styling.catalogs.StylingInstructionsCatalog;
import org.txm.chartsengine.core.styling.definitions.StylingInstructionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.TransformationFunctionDefinition;
import org.txm.core.results.TXMResult;
import org.txm.utils.logger.Log;

/**
 * StylingInstruction represent a choice made to style something with a function and a parameter
 * 
 * It can be related to the shapes or text (labels) of charts
 * 
 * @author s, mdecorde
 *
 */
@SuppressWarnings("rawtypes")
public class StylingInstruction<InputType extends Object> extends Instruction<StylingInstructionDefinition<InputType>, TransformationFunctionDefinition> {

	public StylingInstruction(InstructionsCatalog catalog, String instructionID, String functionId) {

		super(catalog, instructionID, functionId);
	}

	/**
	 * Warning, function definition must be set to avoid NPE later
	 * 
	 * @param catalog
	 * @param instructionID
	 */
	public StylingInstruction(InstructionsCatalog catalog, String instructionID) {

		this(catalog, instructionID, null);
	}
	
	@Override
	public StylingInstruction clone() {
		StylingInstruction copy = new StylingInstruction(this.instructionsCatalog, this.instructionDefinition != null ? this.instructionDefinition.getID(): "");
		
		copy.setMustAppledToShape(this.mustBeAppliedToShape());
		copy.setMustAppliedToText(this.mustBeAppliedToText());
		
		if (this.functionDefinition != null) {
			copy.setFunctionDefinition(functionDefinition.getID());
		}
		
		if (this.parameterDefinition != null) {
			copy.setParameterDefinition(parameterDefinition.getID());
		}
		
		if (this.settings != null) { // TODO Settings must be clonable ? 
			copy.setSettings(settings);
		}
		
		copy.setActivated(this.isActivated());
		
		return copy;
	}

	/**
	 *
	 */
	public static final String ID_SHAPE_REPLACEMENT = "shape-replacement", ID_COLOR = "color", //$NON-NLS-1$
			ID_VARIABLE_COLOR = "variable color", ID_TRANSPARENCY = "transparency", ID_SIZE = "size", //$NON-NLS-1$
			ID_LABEL_REPLACEMENT = "label-replacement", ID_HIDDEN = "hidden", ID_FONT = "font"; //$NON-NLS-1$

	//shapes
	public static final String VALUE_ID_SHAPE_DIAMOND = "diamond",  //$NON-NLS-1$
			VALUE_ID_SHAPE_VERTICAL_RHOMBUS = "losange",  //$NON-NLS-1$
			VALUE_ID_SHAPE_HORIZONTAL_RHOMBUS = "losange2", //$NON-NLS-1$
			VALUE_ID_SHAPE_DISK = "disk", //$NON-NLS-1$
			VALUE_ID_SHAPE_HORIZONTAL_ELLIPSE = "horizontal_ellipse", //$NON-NLS-1$
			VALUE_ID_SHAPE_VERTICAL_ELLIPSE = "vertical_ellipse", //$NON-NLS-1$
			VALUE_ID_SHAPE_SQUARE = "square", //$NON-NLS-1$
			VALUE_ID_SHAPE_VERTICAL_RECTANGLE = "vertical_rectangle", //$NON-NLS-1$
			VALUE_ID_SHAPE_HORIZONTAL_RECTANGLE = "horizontal_rectangle", //$NON-NLS-1$
			VALUE_ID_SHAPE_UP_TRIANGLE = "triangle_up", //$NON-NLS-1$
			VALUE_ID_SHAPE_DOWN_TRIANGLE = "triangle_down", //$NON-NLS-1$
			VALUE_ID_SHAPE_CROSS = "cross", //$NON-NLS-1$
			VALUE_ID_SHAPE_HORIZONTAL_CROSS = "horizontal_cross"; //$NON-NLS-1$
	
	public static final String ALWAYS_TRUE = "always_true", //$NON-NLS-1$
			ALWAYS_FALSE = "always_false"; //$NON-NLS-1$
	
	// colors
	public static final String VALUE_ID_COLOR_RED = "red", VALUE_ID_COLOR_BLUE = "blue", VALUE_ID_COLOR_GREEN = "green", VALUE_ID_COLOR_RGB = "rgb", //$NON-NLS-1$
			VALUE_ID_PROPERTY_Q12 = "property-q12", VALUE_ID_PROPERTY_CONT1 = "property-cont1", VALUE_ID_PROPERTY_CONT2 = "property-cont2", //$NON-NLS-1$
			VALUE_ID_TRANSPARENCY_PERCENT = "transparency-percent", //$NON-NLS-1$
			VALUE_ID_SCALING_FACTOR = "scaling-factor", // FIXME: SJ: useless? //$NON-NLS-1$
			VALUE_ID_REGEX = "regex-replacement-pattern"  // FIXME: SJ: useless? //$NON-NLS-1$
	;

// NOTE MD au début l'interntion de ces 2 variables étaient de gérer la compatbilité d'une instruction de stylage peut etre appliquée a un TEXT ou une SHAPE 
//	// FIXME: SJ: useless? was dedicated to know if an instruction can be applied to label, shape or both, 
//	protected boolean relatedToText = true;
//	// FIXME: SJ: useless? was dedicated to know if an instruction can be applied to label, shape or both
//	protected boolean relatedToShape = true;
	
	/*
	 * if true the style will modify the TEXT 
	 * TODO move this in the StylingRule ?
	 */
	protected boolean applyToText = true; // by default a style will attempt to modify both text and shape
	/*
	 * if true the style will modify the SHAPE
	 * TODO move this in the StylingRule ?
	 */
	protected boolean applyToShape = true; // by default a style will attempt to modify both text and shape

	/**
	 * 
	 * @param serie
	 * @param item
	 * @return the transformation result, might be null if no function is set
	 */
	public InputType transform(TXMResult result,int serie, int item) {
		
		if (functionDefinition == null) return null; // 

		if (parameterDefinition == null) {
			return (InputType) functionDefinition.apply(settings, null);
		}
		else {
			Object v = parameterDefinition.getValue(result, serie, item);
			return (InputType) functionDefinition.apply(settings, v);
		}
	}

	//
	//
	//
	//	/**
	//	 *
	//	 * @param type
	//	 * @param typeInternalName
	//	 */
	//	public StylingInstruction(String id) {
	//		this(id, null);
	//	}
	//
	//
	//	/**
	//	 *
	//	 * @param type
	//	 * @param typeInternalName
	//	 */
	//	public StylingInstruction(String id, String currentValueID) {
	//		this(id, currentValueID, null);
	//	}
	//
	//
	//	/**
	//	 *
	//	 * @param type
	//	 * @param typeInternalName
	//	 * @param currentValueInternalName
	//	 */
	//	public StylingInstruction(String id, String currentValueID, Object value) {
	//		super(StylingInstructionsCatalog.getInstance(),id, currentValueID, value);
	//		this.relatedToText = true;
	//		this.relatedToShape = true;
	//	}


	/**
	 *
	 * @return
	 */
	public boolean mustBeAppliedToText() {
		return applyToText;
	}


	/**
	 *
	 * @return
	 */
	public boolean mustBeAppliedToShape() {
		return applyToShape;
	}


	// FIXME: SJ: tests
	public static void main(String[] args) {

		StylingInstructionsCatalog catalog = new StylingInstructionsCatalog();
		// Example of instruction creation
		StylingInstruction s = new StylingInstruction(catalog, ID_LABEL_REPLACEMENT);
		//		System.out.println(s.getDump());
		//		s.setInstructionDefinition(StylingInstruction.ID_LABEL_REPLACEMENT);
		s.setFunctionDefinition("STRINGREPLACE"); //$NON-NLS-1$
		s.setParameterDefinition(VALUE_ID_REGEX);
		s.setSettings(Pattern.compile("lolok")); //$NON-NLS-1$
		System.out.println(s.getDump());

		//		s.setInstructionDefinition(ID_COLOR);
		System.out.println(s.getDump());


		System.out.println();
		s.getInstructionsCatalog().dump();

	}

	//	@Override
	//	public void setValue(Object settings) {
	//		
	//		//functionDefinition.settings = settings;
	//	}

	public void setMustAppliedToText(boolean applyToText) {
		this.applyToText = applyToText;
	}

	public void setMustAppledToShape(boolean applyToShape) {
		this.applyToShape = applyToShape;
	}

	// Old Keys of the CAStyle macro code copied in the CAItemStyleRenderer. TODO implement the definitive CAItemStyleRenderer + move/merge/delete keys between StylingSheets and Chart core etc.
	public static String LABEL_PATTERN = "label-pattern"; //$NON-NLS-1$
	public static String LABEL_REPLACEMENT = "label-replacement"; //$NON-NLS-1$
	public static String HIDDEN = "hidden"; //$NON-NLS-1$
	public static String SHAPE_SIZE = "shape-size"; //$NON-NLS-1$
	public static String SHAPE_COLOR = "shape-color"; //$NON-NLS-1$
	public static String SHAPE_ALPHA = "shape-alpha"; //$NON-NLS-1$
	public static String SHAPE_REPLACEMENT = "shape-replacement"; //$NON-NLS-1$
	public static String LABEL_SIZE = "label-size"; //$NON-NLS-1$
	public static String LABEL_COLOR = "label-color"; //$NON-NLS-1$
	public static String LABEL_ALPHA = "label-alpha"; //$NON-NLS-1$
	public static String LABEL_FONT_FAMILY = "label-font-family"; //$NON-NLS-1$
	public static String LABEL_STYLE = "label-style"; //$NON-NLS-1$
	
	/**
	 * Temporary function to set the chart internal styles (association of serie&item to a style modification
	 * 
	 * @param serie
	 * @param item
	 * @param labelReplacementStyle
	 * @param hiddenStyle
	 * @param shapeSizeStyle
	 * @param shapeColorStyle
	 * @param shapeAlphaStyle
	 * @param shapeReplacementStyle
	 * @param textSizeStyle
	 * @param textFontColorStyle
	 * @param textFontAlphaStyle
	 * @param textFontFamilyStyle
	 * @param textFontStyleStyle
	 */
	public void updateChartStyle(TXMResult result, int serie, int item, HashMap<Integer, String> labelReplacementStyle, HashSet<Integer> hiddenStyle, HashMap<Integer, Double> shapeSizeStyle,
			HashMap<Integer, Color> shapeColorStyle, HashMap<Integer, Double> shapeAlphaStyle, HashMap<Integer, Shape> shapeReplacementStyle, HashMap<Integer, Double> textSizeStyle,
			HashMap<Integer, Color> textFontColorStyle, HashMap<Integer, Double> textFontAlphaStyle, HashMap<Integer, String> textFontFamilyStyle, HashMap<Integer, Integer> textFontStyleStyle) {

		if (this.getInstruction() == null) {
			return; // nothing to do
		}
		
		if (!this.isActivated()) { // skip this instruction
			return;
		}
		
		String style_id = this.getInstruction().getID();

		// TODO move this code in a StylingInstruction.toChartStyles() method defined in each StylingInstructionDefinition
		if (style_id.equals(ID_LABEL_REPLACEMENT)) {
			labelReplacementStyle.put(item, (String) this.transform(result, serie, item));
		}
		else if (style_id.equals(ID_HIDDEN)) {
			if (Boolean.TRUE.equals(this.transform(result, serie, item))) {
				hiddenStyle.add(item);
			}
		} else if (style_id.equals(ID_FONT)) {
			Object o = this.transform(result, serie, item);
			if (o != null && o instanceof Font f) {
				textFontFamilyStyle.put(item, f.getFamily());
				textFontStyleStyle.put(item, f.getStyle());
			}
		}
		else if (style_id.equals(ID_SIZE)) {
			if (mustBeAppliedToShape()) {
				shapeSizeStyle.put(item, (Double) this.transform(result, serie, item));
			}
			if (mustBeAppliedToText()) {
				textSizeStyle.put(item, (Double) this.transform(result, serie, item));
			}
		}
		else if (style_id.equals(ID_COLOR)) {
			if (mustBeAppliedToShape()) {
				shapeColorStyle.put(item, (Color) this.transform(result, serie, item));
			}

			if (mustBeAppliedToText()) {
				textFontColorStyle.put(item, (Color) this.transform(result, serie, item));
			}
		}
		else if (style_id.equals(ID_TRANSPARENCY)) {
			if (mustBeAppliedToShape()) {
				shapeAlphaStyle.put(item, (Double) this.transform(result, serie, item));
			}
			if (mustBeAppliedToText()) {
				textFontAlphaStyle.put(item, (Double) this.transform(result, serie, item));
			}
		}
		else if (style_id.equals(SHAPE_REPLACEMENT)) {
			shapeReplacementStyle.put(item, (Shape) this.transform(result, serie, item));
		}
		else if (style_id.equals(LABEL_FONT_FAMILY)) {
			textFontFamilyStyle.put(item, (String) this.transform(result, serie, item));
		}
		else if (style_id.equals(LABEL_STYLE)) {
			textFontStyleStyle.put(item, (Integer) this.transform(result, serie, item));
		}
		else {
			Log.warning("Unknown style: " + style_id);
		}
	}

}
