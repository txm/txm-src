package org.txm.chartsengine.core.styling.base;

import java.io.File;
import java.util.LinkedHashSet;

import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.core.styling.StylesRegistry;
import org.txm.chartsengine.core.styling.StylingRule;
import org.txm.chartsengine.core.styling.StylingSheet;
import org.txm.chartsengine.core.styling.catalogs.SelectionInstructionsCatalog;
import org.txm.chartsengine.core.styling.catalogs.StylingInstructionsCatalog;

/**
 * Gives for a type of ChartResult the available selection and style instructions + the default style sheet + the available styles sheets.
 * 
 * @author mdecorde
 *
 * @param <CR>
 */
public abstract class StylerCatalog<CR extends ChartResult> {

	/**
	 * 
	 * @return the available selection instructions.
	 */
	public abstract SelectionInstructionsCatalog getSelectionInstructionsCatalog();

	/**
	 * 
	 * @return the available style instructions.
	 */
	public abstract StylingInstructionsCatalog getStyleInstructionsCatalog();

	/**
	 * 
	 * @return the pre-defined Style (compilation of style instructions)
	 */
	public abstract StylesRegistry getStylesRegistry();

	/**
	 * 
	 * @return the StyleSheets to set to the new ChartResult of the CR type
	 */
	public abstract LinkedHashSet<StylingSheet> getStyleSheets();
	
	/**
	 * 
	 * @return the default StylingSheet to set to a chart result
	 */
	public abstract StylingSheet getDefaultStyleSheet();

	/**
	 * Creates a default style rule
	 * 
	 * @return a default StyleRule for the CR objets
	 */
	public abstract StylingRule newDefaultRule();
	
	/**
	 * Import a styling sheet from a file. The styling sheet name is built with the filename without its extension.
	 * 
	 * @param file the file to read
	 * @return a new StylingSheet
	 * @throws Exception 
	 */
	public abstract StylingSheet importStylingSheet(File file) throws Exception;
	
	/**
	 * Export the style sheet in a semi binary format. The exported file can be imported later.
	 * 
	 * @param file the file to write
	 * @param sheet the StylingSheet to export
	 * @return true if the style sheet has been exported correctly
	 */
	public abstract boolean exportStylingSheet(File file, StylingSheet sheet) throws Exception;
	
	/**
	 * 
	 * @param sheet the sheet to test
	 * @return true if the catalog contains the sheet
	 */
	public abstract boolean contains(StylingSheet sheet);
	
	/**
	 * 
	 * @param sheet the sheet to add
	 * @return true if the sheet has been added. A sheet is not added if NULL or already in the catalog (=same name)
	 */
	public abstract boolean add(StylingSheet sheet);
	
	/**
	 * 
	 * @param sheet the sheet to be removed
	 * @return FALSE if the catalog does not contain the sheet
	 */
	public abstract boolean remove(StylingSheet sheet);
	
	/**
	 * 
	 * @param sheet name the sheet to get
	 * @return the Sheet OR NULL if the catalog does not contain the sheet
	 */
	public abstract StylingSheet get(String sheetName);

	/**
	 * Fill the style sheets collection with the default styling sheets 
	 */
	protected abstract void populateDefaultStyleSheets();
	
	/**
	 * Fill the style sheets collection with the USER saved styling sheets 
	 */
	protected abstract void populateUserStyleSheets();

	/**
	 * 
	 * @param stylingsheet the styling sheet to save
	 */
	protected abstract void persistUserStyleSheet(StylingSheet stylingsheet);

	/**
	 * 
	 * @param name the persisted styling sheet name to remove
	 */
	protected abstract void removePersistedUserStyleSheet(String name);
}
