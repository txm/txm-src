/**
 *
 */
package org.txm.chartsengine.core.styling;

import java.awt.Color;
import java.awt.Shape;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;

import org.txm.chartsengine.core.styling.base.StylerCatalog;
import org.txm.chartsengine.core.styling.catalogs.SelectionInstructionsCatalog;
import org.txm.chartsengine.core.styling.catalogs.StylingInstructionsCatalog;
import org.txm.chartsengine.core.styling.instructions.SelectionInstruction;
import org.txm.chartsengine.core.styling.instructions.StylingInstruction;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;

/**
 * A rule of a StylingSheet.
 * 
 * A rule is composed of 2 parts : selectors (to select the element to style) and a style (the transformations to apply to the selected element)
 * 
 * @author s, mdecorde
 *
 */
public class StylingRule implements Serializable {

	private static final long serialVersionUID = -2205100341310319981L;

	protected String name;

	protected Selector selectors; // MD replace the array with an actual class to manage the SelectionInstructions : test isntructions and get/set/edit instructions ?

	protected Style style;
	
	protected boolean activated = true;

	/**
	 *
	 */
	public StylingRule() {
		this(TXMCoreMessages.common_unnamed, null);
	}

	/**
	 *
	 */
	public StylingRule(String name, Style style) {
		this.name = name;
		this.selectors = new Selector();
		this.style = style;
	}

	@Override
	public StylingRule clone() {

		StylingRule tmp = new StylingRule();
		tmp.setActivated(this.isActivated());
		if (this.style != null) {
			tmp.style = this.style.clone();
		}
		for (SelectionInstruction si : this.selectors) {
			tmp.selectors.add(si.clone());
		}
		return tmp;
	}

	/**
	 *
	 * @param selector
	 */
	public void addSelector(SelectionInstruction selector) {
		this.selectors.add(selector);
	}

	public Selector getSelectors() {
		return this.selectors;
	}

	public Style getStyle() {
		return style;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isActivated() {
		return activated;
	}
	
	public boolean setActivated(boolean newState) {
		this.activated = newState;
		return activated;
	}
	
	public void activate() {
		this.activated = true;
	}
	
	public void deactivate() {
		this.activated = false;
	}

	/**
	 *
	 * @param level the indentation level
	 * @return
	 */
	public String getDump(int indentation) {

		StringBuffer dump = new StringBuffer();
		StringBuffer indentationStr = new StringBuffer();

		for (int i = 0; i < indentation; i++) {
			indentationStr.append("	"); //$NON-NLS-1$
		}

		dump.append(indentationStr);
		dump.append(TXMCoreMessages.bind("* {0} - \"{1}\", numbers of selectors: {2}", this.getClass().getSimpleName(), this.name, this.selectors.size())); //$NON-NLS-1$
		dump.append(System.lineSeparator()).append(indentationStr).append("	Selectors:"); //$NON-NLS-1$

		indentation++;
		for (int i = 0; i < this.selectors.size(); i++) {
			dump.append(System.lineSeparator()).append("	"+ this.selectors.get(i).getDump(indentation)); //$NON-NLS-1$
		}

		dump.append(System.lineSeparator()).append(indentationStr).append("	Style:\n"); //$NON-NLS-1$
		dump.append(this.style.getDump(indentation));

		return dump.toString();
	}

	/**
	 *
	 * @return
	 */
	public String getDump() {
		return this.getDump(0);
	}

	//FIXME: SJ: tests
	public static void main(String[] args) {

		StylerCatalog caCatalog = null;//new CAStylerCatalog(null);

		StylingInstructionsCatalog stylesCatalog = caCatalog.getStyleInstructionsCatalog();
		SelectionInstructionsCatalog selectionsCatalog = caCatalog.getSelectionInstructionsCatalog();
		// Rows
		Style style = new Style("Use case #1 - substyle 1"); //$NON-NLS-1$
		style.addInstruction(new StylingInstruction(stylesCatalog, StylingInstruction.ID_SHAPE_REPLACEMENT, StylingInstruction.VALUE_ID_SHAPE_DISK));

		StylingRule rule = new StylingRule("Fait des cercles sur les points-lignes", style); //$NON-NLS-1$

		rule.addSelector(new SelectionInstruction(selectionsCatalog, "label-selection", SelectionInstruction.VALUE_ID_REGEX)); //$NON-NLS-1$
		System.out.println(rule.getDump());

		Style style2 = new Style("Use case #1 - substyle 2"); //$NON-NLS-1$
		style2.addInstruction(new StylingInstruction(stylesCatalog, StylingInstruction.ID_COLOR, StylingInstruction.VALUE_ID_COLOR_GREEN));

		Style style3 = new Style("Use case #1 - substyle 3, transparency on Q12 and size on Cont1"); //$NON-NLS-1$
		style3.addInstruction(new StylingInstruction(stylesCatalog, StylingInstruction.ID_TRANSPARENCY, StylingInstruction.VALUE_ID_PROPERTY_Q12));
		style3.addInstruction(new StylingInstruction(stylesCatalog, StylingInstruction.ID_SIZE, StylingInstruction.VALUE_ID_PROPERTY_CONT1));

		// Cols
		// style 1 + style 3
		Style style4 = new Style("Use case #1 - substyle 4, politique de gauche en rouge"); //$NON-NLS-1$
		style4.addInstruction(new StylingInstruction(stylesCatalog, StylingInstruction.ID_COLOR, StylingInstruction.VALUE_ID_COLOR_RED));

		Style style5 = new Style("Use case #1 - substyle 5, politique de droite en bleu"); //$NON-NLS-1$
		style5.addInstruction(new StylingInstruction(stylesCatalog, StylingInstruction.ID_COLOR, StylingInstruction.VALUE_ID_COLOR_BLUE));
		// Obligé de faire deux styles différents car le sélecteur est défini sur le style style entier (repasser le sélectleur dans la StyleInstruction ?)

		//		// catalogs
		//		System.out.println();
		//		SelectionInstructionsCatalog.getInstance().dump();
		//		StylingInstructionsCatalog.getInstance().dump();

	}

	public void setStyle(Style style2) {

		this.style = style2;
	}
	
	@Override
	public String toString() {
		return selectors.toString()+ " " + style; //$NON-NLS-1$
	}

	public void updateChartStyle(TXMResult result, int serie, int item, HashMap<Integer, String> labelStyle, HashSet<Integer> hiddenStyle, HashMap<Integer, Double> pointSizeStyle,
			HashMap<Integer, Color> pointColorStyle, HashMap<Integer, Double> pointAlphaStyle, HashMap<Integer, Shape> pointShapeStyle, HashMap<Integer, Double> fontSizeStyle,
			HashMap<Integer, Color> fontColorStyle, HashMap<Integer, Double> fontAlphaStyle, HashMap<Integer, String> fontFamilyStyle, HashMap<Integer, Integer> fontStyleStyle) {

		if (!activated) return; // rule is deactivated, nothing to do :-)
		
		boolean styleIt = getSelectors().doIt(result, serie, item);

		if (styleIt) {

			Style styles = getStyle();
			styles.updateChartStyle(result, serie, item,
					labelStyle, hiddenStyle, pointSizeStyle, pointColorStyle, pointAlphaStyle, pointShapeStyle, fontSizeStyle, fontColorStyle, fontAlphaStyle,
					fontFamilyStyle, fontStyleStyle);
		}
	}
}
