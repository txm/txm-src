package org.txm.chartsengine.core.styling.definitions.functions;

/**
 * A transformation with no settings. The returned value depends only on the given argument
 * 
 * @author mdecorde
 *
 */
public abstract class NoSettingsTransformationFunctionDefinition extends TransformationFunctionDefinition {

	public NoSettingsTransformationFunctionDefinition(String id, String label) {

		super(id, label);
	}

	@Override
	public final Object apply(Object settings, Object arg) {
		return apply(null, arg);
	}

	public abstract Object apply(Object arg);
}
