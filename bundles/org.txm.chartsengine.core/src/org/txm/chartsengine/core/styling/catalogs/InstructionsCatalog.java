/**
 *
 */
package org.txm.chartsengine.core.styling.catalogs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;

import org.txm.chartsengine.core.styling.definitions.InstructionDefinition;
import org.txm.chartsengine.core.styling.definitions.ParameterDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.FunctionDefinition;


/**
 * Instructions catalog dedicated to get available instruction value definition for a specified instruction type.
 * 
 * Use the associate methods to set new associations of Instruction x Functions x Parameters
 *
 * @author s, mdecorde
 *
 */
public abstract class InstructionsCatalog<InstDef extends InstructionDefinition<?>, FoncDef extends FunctionDefinition> implements Serializable {

	private static final long serialVersionUID = -7299956646413252532L;

	protected FunctionsCatalog<FoncDef> functions = new FunctionsCatalog<>();

	protected ParametersCatalog parameters = new ParametersCatalog();

	
	
	
	
	/**
	 * Available instruction definitions.
	 */
	protected LinkedHashMap<InstDef, LinkedHashMap<FoncDef, ArrayList<ParameterDefinition>>> instructionsToFunctionsToParameters;

	public InstructionsCatalog() {
		super();
		this.instructionsToFunctionsToParameters = new LinkedHashMap<>();
		this.initDefinitions();
	}

	public void addInstruction(InstDef I) {

		this.instructionsToFunctionsToParameters.put(I, new LinkedHashMap<>());
	}

	@SuppressWarnings("unchecked")
	public void addInstruction(InstDef... instructionstoAdd) {

		for (InstDef I : instructionstoAdd) {
			this.instructionsToFunctionsToParameters.put(I, new LinkedHashMap<>());
		}
	}

	public void associate(InstDef instruction, FoncDef function) {

		instructionsToFunctionsToParameters.get(instruction).put(function, new ArrayList<>());
		if (!functions.containsKey(function.getID())) {
			functions.add(function);
		}
	}

	public void associate(InstDef instruction, String functionid) {
		associate(instruction, this.getFunction(functionid));

	}

	public void associate(String instructionid, String functionid) {
		associate(this.getInstructionDefinition(instructionid), this.getFunction(functionid));

	}

	@SuppressWarnings("unchecked")
	public void associate(InstDef instruction, FoncDef... functionsToAdd) {

		for (FoncDef function : functionsToAdd) {
			instructionsToFunctionsToParameters.get(instruction).put(function, new ArrayList<>());
			if (!functions.containsKey(function.getID())) {
				functions.add(function);
			}
		}
	}

	public void associate(InstDef instruction, String functionid, String parameterid) {

		associate(instruction, this.getFunction(functionid), this.getParameters(parameterid));
	}

	public void associate(InstDef instruction, FoncDef function, String parameterid) {

		associate(instruction, function, this.getParameters(parameterid));
	}

	public void associate(String instructionid, String functionid, String parameterid) {

		associate(this.getInstructionDefinition(instructionid), this.getFunction(functionid), this.getParameters(parameterid));
	}

	public void associate(InstDef instruction, FoncDef fonction, ParameterDefinition parameter) {

		LinkedHashMap<FoncDef, ArrayList<ParameterDefinition>> instructionTofunctions = instructionsToFunctionsToParameters.get(instruction);
		if (instructionTofunctions == null) {
			addInstruction(instruction);
			instructionTofunctions = instructionsToFunctionsToParameters.get(instruction);
		}
		if (!functions.containsKey(fonction.getID())) {
			functions.add(fonction);
		}

		if (!parameters.containsKey(parameter.getID())) {
			parameters.add(parameter);
		}
		if (!instructionTofunctions.containsKey(fonction)) {
			instructionTofunctions.put(fonction, new ArrayList<>());
		}
		instructionTofunctions.get(fonction).add(parameter);
	}

	public void associate(InstDef instruction, FoncDef fonction, ParameterDefinition... parameterstoAdd) {

		LinkedHashMap<FoncDef, ArrayList<ParameterDefinition>> instructionTofunctions = instructionsToFunctionsToParameters.get(instruction);
		if (instructionTofunctions == null) {
			addInstruction(instruction);
			instructionTofunctions = instructionsToFunctionsToParameters.get(instruction);
		}
		if (!functions.containsKey(fonction.getID())) {
			functions.add(fonction);
		}
		for (ParameterDefinition parameter : parameterstoAdd) {

			if (!parameters.containsKey(parameter.getID())) {
				parameters.add(parameter);
			}
			if (!instructionTofunctions.containsKey(fonction)) {
				instructionTofunctions.put(fonction, new ArrayList<>());
			}
			instructionTofunctions.get(fonction).add(parameter);
		}
	}

	/**
	 * Initializes the default available value definitions grouped by instruction types.
	 */
	public abstract void initDefinitions();

	/**
	 * Gets the instruction definition according to its id.
	 * 
	 * @param id
	 * @return
	 */
	public InstDef getInstructionDefinition(String id) {

		for (InstDef instructionDef : this.instructionsToFunctionsToParameters.keySet()) {
			if (instructionDef.getID().equals(id)) {
				return instructionDef;
			}
		}
		return null;
	}

	public FunctionsCatalog<FoncDef> getFunctionsCatalog() {

		return functions;
	}

	public ParametersCatalog getParametersCatalog() {

		return parameters;
	}

	public FoncDef getFunction(String id) {

		return functions.get(id);
	}

	public ParameterDefinition getParameters(String id) {

		return parameters.get(id);
	}

	public Set<InstDef> getInstructions() {
		return instructionsToFunctionsToParameters.keySet();
	}

	public ArrayList<FoncDef> getFunctionsFor(InstructionDefinition<?> instructionDefinition) {

		ArrayList<FoncDef> sub = new ArrayList<>();
		sub.addAll(instructionsToFunctionsToParameters.get(instructionDefinition).keySet());
		return sub;
	}

	public ArrayList<ParameterDefinition> getParametersFor(InstDef instructionDef, FoncDef functionDef) {
		ArrayList<ParameterDefinition> sub = new ArrayList<>();
		LinkedHashMap<FoncDef, ArrayList<ParameterDefinition>> functions = instructionsToFunctionsToParameters.get(instructionDef);
		if (functions != null) {
			if (functions.containsKey(functionDef)) {
				sub.addAll(functions.get(functionDef));
			}
		}
		return sub;
	}

	public FunctionsCatalog<FoncDef> getFunctions() {
		return functions;
	}

	public ParametersCatalog getParameters() {
		return parameters;
	}

	/**
	 * Dumps the data.
	 */
	public void dump() {
		System.out.println("**** " + this.getClass().getSimpleName() + " ****"); //$NON-NLS-1$
		for (InstructionDefinition<?> instr : instructionsToFunctionsToParameters.keySet()) {
			System.out.println("     * Instruction definition: " + instr.getDump()); //$NON-NLS-1$
		}
		for (ParameterDefinition p : this.getParameters().values()) {
			System.out.println("     * Parameter definition: " + p.getDump()); //$NON-NLS-1$
		}
		for (FunctionDefinition f : this.getFunctions().values()) {
			System.out.println("     * Function definition: " + f.getDump()); //$NON-NLS-1$
		}
	}

}
