package org.txm.chartsengine.core.styling.definitions.functions;

/**
 * A function to be used for StylingInstructions.
 * 
 * @author mdecorde
 *
 */
public abstract class TransformationFunctionDefinition extends FunctionDefinition {

	private static final long serialVersionUID = 5717138836748653683L;

	public TransformationFunctionDefinition(String id, String label) {

		super(id, label);
	}

	//	public ReturnType applyWithParameter(Object settings, ParameterDefinition<?> parameterDefinition, int serie, int item) {
	//		
	//		if (parameterDefinition == null) {
	//			return apply((SettingsType)settings, (ArgumentType) null);
	//		} else {
	//			return apply((SettingsType)settings, (ArgumentType) parameterDefinition.getValue(serie, item));
	//		}
	//	}
}
