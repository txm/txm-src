package org.txm.chartsengine.core.styling.definitions;

/**
 * A Style instruction changes a style property (Color, Label, Shape, Shape color, etc..)
 * 
 * @author mdecorde
 *
 * @param <F>
 */
public class StylingInstructionDefinition<InputType extends Object> extends InstructionDefinition<InputType> {

	private static final long serialVersionUID = -3773526753001489441L;

	public StylingInstructionDefinition(String id, String label) {

		super(id, label);
	}
}
