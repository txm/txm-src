/**
 *
 */
package org.txm.chartsengine.core.styling.definitions;

import java.io.Serializable;

import org.txm.core.messages.TXMCoreMessages;

/**
 * @author s, mdecorde
 *
 */
public class ElementDefinition implements Serializable {

	private static final long serialVersionUID = -5528670630854608274L;

	/**
	 * The internal ID.
	 */
	protected String id;

	/**
	 * The label.
	 */
	protected String label;

	/**
	 *
	 * @param id
	 * @param label
	 */
	public ElementDefinition(String id, String label) {
		assert(id != null);
		assert(label != null);
		
		this.id = id;
		this.label = label;
	}

	/**
	 *
	 * @return
	 */
	public String getID() {
		return this.id;
	}

	/**
	 *
	 * @return
	 */
	public String getLabel() {
		return this.label;
	}

	/**
	 * Gets the dump of this definition data.
	 *
	 * @param indentation the indentation level
	 * @return
	 */
	public String getDump(int indentation) {

		StringBuffer dump = new StringBuffer();
		StringBuffer indentationStr = new StringBuffer();

		for (int i = 0; i < indentation; i++) {
			indentationStr.append("	"); //$NON-NLS-1$
		}

		dump.append(indentationStr);
		dump.append(TXMCoreMessages.bind("* {0} ({1})", this.id, this.label)); //$NON-NLS-1$

		return dump.toString();
	}

	/**
	 *
	 * @return
	 */
	public String getDump() {
		return this.getDump(0);
	}

	@Override
	public String toString() {
		//return this.getClass().getSimpleName()+"[id="+id+", label="+label+"]";
		return label;
	}

	@Override
	public int hashCode() {
		return this.id.hashCode() + this.label.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (!(o instanceof ElementDefinition e)) return false;
		if (e.id == null) return false;
		if (e.label == null) return false;
		
		return this.id.equals(e.id) && this.label.equals(e.label);
	}

}
