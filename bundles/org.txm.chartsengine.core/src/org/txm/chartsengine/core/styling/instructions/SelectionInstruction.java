package org.txm.chartsengine.core.styling.instructions;

import org.txm.chartsengine.core.messages.ChartsEngineCoreMessages;
import org.txm.chartsengine.core.styling.catalogs.InstructionsCatalog;
import org.txm.chartsengine.core.styling.catalogs.SelectionInstructionsCatalog;
import org.txm.chartsengine.core.styling.definitions.SelectionInstructionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.TestFunctionDefinition;
import org.txm.core.results.TXMResult;

/**
 * An instruction specialized to the selection of element to style
 *
 * @author s, mdecorde
 *
 */
public class SelectionInstruction extends Instruction<SelectionInstructionDefinition, TestFunctionDefinition> {

	public static final String VALUE_ID_TARGET_LABELS = ChartsEngineCoreMessages.labels 
							, VALUE_ID_TARGET_SHAPES = ChartsEngineCoreMessages.symbols  //$NON-NLS-1$
							, VALUE_ID_TARGET_LABELS_AND_SHAPES = ChartsEngineCoreMessages.labelsAndSymbols //$NON-NLS-1$
							, VALUE_ID_REGEX = "regex-search-pattern"; //$NON-NLS-1$

	public SelectionInstruction(InstructionsCatalog catalog, String instructionid, String functionid) {

		super(catalog, instructionid, functionid);
	}
	
	public SelectionInstruction(InstructionsCatalog catalog, String instructionid, String functionid, String parameterid) {

		super(catalog, instructionid, functionid, parameterid);
	}

	@Override
	public SelectionInstruction clone() {

		SelectionInstruction si = new SelectionInstruction(this.instructionsCatalog, this.instructionDefinition.getID(), null);
		
		if (this.functionDefinition != null) {
			si.setFunctionDefinition(this.functionDefinition.getID());
		}
		
		if (this.parameterDefinition != null) {
			si.setParameterDefinition(this.parameterDefinition.getID());
		}
		
		if (this.settings != null) {
			si.setSettings(settings);
		}
		
		si.setActivated(this.isActivated());
		
		return si;
	}

	public boolean test(TXMResult result, int serie, int item) {

		Object v = null;
		if (parameterDefinition != null) {
			v = parameterDefinition.getValue(result, serie, item);
		}

		return (boolean) functionDefinition.apply(settings, v);
	}

	//
	//	/**
	//	 *
	//	 * @param type
	//	 * @param id
	//	 */
	//	public SelectionInstruction(String instructionID) {
	//		this(instructionID, null);
	//	}
	//
	//	/**
	//	 *
	//	 * @param type
	//	 * @param typeInternalName
	//	 */
	//	public SelectionInstruction(String instructionID, String functionID) {
	//		this(instructionID, functionID, null);
	//	}
	//
	//	/**
	//	 *
	//	 * @param variable
	//	 * @param variableInternalName
	//	 * @param currentValueID
	//	 */
	//	public SelectionInstruction(String instructionID, String functionID, Object settings) {
	//		super(SelectionInstructionsCatalog.getInstance(), instructionID, functionID, null);
	//		//functionDefinition.settings = settings;
	//	}

	// FIXME: SJ: tests
	public static void main(String[] args) {

		SelectionInstructionsCatalog catalog = new SelectionInstructionsCatalog();
		catalog.addInstruction(new SelectionInstructionDefinition("UNESELECTION", "Ma petite selection qui va bien sur mon résult")); //$NON-NLS-1$

		System.out.println();
		catalog.dump();

		// Example of instruction creation
		SelectionInstruction s = new SelectionInstruction(catalog, "UNESELECTION", "REGEX"); //$NON-NLS-1$
		//		System.out.println(s.getDump());

		//		s.setFunctionDefinition("REGEX");
		//		s.setParameterDefinition();
		s.setSettings("(ab.+)"); //$NON-NLS-1$
		System.out.println(s.getDump());

		//		System.out.println(s.getFunction().apply("abbbbbb"));
		//		System.out.println(s.getFunction().apply("accccc"));
	}

	//	@Override
	//	public void setValue(Object value) {
	//		
	//		this.value = value;
	//	}


}
