package org.txm.chartsengine.core.styling.definitions;

/**
 * A Selection instruction tests a parameter given a test function
 * 
 * @author mdecorde
 */
public class SelectionInstructionDefinition extends InstructionDefinition<Boolean> {

	private static final long serialVersionUID = -4915769080341495810L;

	/**
	 * 
	 * @param id
	 * @param label
	 */
	public SelectionInstructionDefinition(String id, String label) {
		super(id, label);
	}
}
