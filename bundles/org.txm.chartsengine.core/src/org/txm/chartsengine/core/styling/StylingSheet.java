/**
 *
 */
package org.txm.chartsengine.core.styling;

import java.util.ArrayList;

import org.eclipse.osgi.util.NLS;
import org.txm.chartsengine.core.messages.ChartsEngineCoreMessages;
import org.txm.chartsengine.core.styling.instructions.SelectionInstruction;
import org.txm.chartsengine.core.styling.instructions.StylingInstruction;
import org.txm.core.messages.TXMCoreMessages;

/**
 * @author s
 *
 */
public class StylingSheet {

	private String name;

	protected ArrayList<StylingRule> rules = new ArrayList<>();

	protected boolean editable = true;

	/**
	 *
	 */
	public StylingSheet() {
		this(TXMCoreMessages.common_unnamed);
	}

	public StylingSheet(String name) {
		this.setName(name);
	}

	/**
	 * 
	 * @return true if rules can be added/removed from the styling sheet
	 */
	public boolean isEditable() {
		return editable;
	}

	/**
	 * if the to true, no rules can be added or removed
	 */
	public void setEditable(boolean editable2) {
		this.editable = editable2;
	}

	@Override
	public StylingSheet clone() {

		StylingSheet clone = new StylingSheet();
		clone.editable = true;
		clone.name = this.name;

		for (StylingRule rule : this.getRules()) {
			clone.add(rule.clone());
		}
		return clone;
	}

	final static String[] columns = {"selectors", "styles"}; //$NON-NLS-1$

	//	/**
	//	 * FIXME does not because of the attached Ca to the CAParameterDefinition :s
	//	 * Creates a styling sheet from a table file
	//	 * @param file
	//	 * @param catalog
	//	 * @throws Exception 
	//	 */
	//	public StylingSheet(File file, StylerCatalog<?> catalog) throws Exception {
	//		this.setName(FileUtils.stripExtension(file));
	//
	//		Properties props = new Properties();
	//		props.load(IOUtils.getReader(file));
	//		
	//		for (Object k : props.keySet()) {
	//			StylingRule r = StylingRule.deSerialize(props.get(k).toString());
	//			if (r != null) {
	//				rules.add(r);
	//			}
	//		}
	//	}

	//	public boolean export(File file) throws Exception {
	//		
	//		Properties props = new Properties();
	//		int n = 1;
	//		for (StylingRule rule : rules) {
	//			props.put("rule"+(n++), rule.serialize());
	//		}
	//		props.store(IOUtils.getWriter(file), name);
	//		return true;
	//	}

	public StylingRule add(StylingRule rule) {

		if (!editable) return null;

		this.rules.add(rule);
		return rule;
	}

	public StylingRule addAt(int i, StylingRule rule) {

		if (!editable) return null;

		this.rules.add(i, rule);
		return rule;
	}

	public StylingRule remove(StylingRule rule) {

		if (!editable) return null;

		this.rules.remove(rule);
		return rule;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<StylingRule> getRules() {
		return rules;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof StylingSheet s) {
			return name.equals(s.getName());
		}
		return false;
	}

	public String getDump() {
		
		StringBuilder b = new StringBuilder();
		b.append("Name: "+this.name+" Number of rules: "+rules.size()+"\n"); //$NON-NLS-1$
		for (StylingRule rule : getRules()) {
			b.append(rule.getDump());
			b.append("\n"); //$NON-NLS-1$ //$NON-NLS-1$
		}
		return b.toString();
	}

	public String getHumanDump() {
		
		StringBuilder b = new StringBuilder();
		b.append(this.name+"\n"); //$NON-NLS-1$
		b.append(ChartsEngineCoreMessages.bind(ChartsEngineCoreMessages.rulesP0, rules.size()));
		for (StylingRule rule : getRules()) {
			b.append("\n\t"+ (rule.isActivated()?"":"(")); //$NON-NLS-1$

			b.append(ChartsEngineCoreMessages.forThe);
			for (SelectionInstruction si : rule.getSelectors()) {
				if ("datatest".equals(si.getFunction().getID())) b.append(" "+si.getSettings()); //$NON-NLS-1$
				else b.append(ChartsEngineCoreMessages.bind(ChartsEngineCoreMessages.andIfP0, si.getParameter()+" "+si.getFunction()+" "+si.getSettings())); //$NON-NLS-1$
			}


			b.append("\n\t"); //$NON-NLS-1$
			b.append(ChartsEngineCoreMessages.appliedTo);
			for (StylingInstruction<?> si : rule.getStyle().getStylingInstructions()) {

				if (si.mustBeAppliedToText() && si.mustBeAppliedToShape()) b.append("\t"+ ChartsEngineCoreMessages.labelsAndSymbols); //$NON-NLS-1$
				else if (si.mustBeAppliedToText()) b.append("\t"+ ChartsEngineCoreMessages.labels); //$NON-NLS-1$
				else if (si.mustBeAppliedToShape()) b.append("\t"+ ChartsEngineCoreMessages.symbols); //$NON-NLS-1$

				if (si.getInstruction() == null) {
					b.append("\t<not defined>"); //$NON-NLS-1$
				} else {
					b.append("\t"+si.getInstruction()+": "); //$NON-NLS-1$
					if (si.getFunction() != null) {
						if (si.getFunction().getLabel().contains("{0}")) { //$NON-NLS-1$
							b.append(NLS.bind(si.getFunction().getLabel(), si.getParameter()));
						} else {
							b.append(si.getFunction().getLabel());
						}
					}

					if (si.getSettings() != null) {
						b.append(" "+si.getSettings()); //$NON-NLS-1$
					}

					b.append(""+ (rule.isActivated()?"":")")); //$NON-NLS-1$
				}
			}
		}
		return b.toString();
	}
	
	public String getTableDump() {
		
		StringBuilder b = new StringBuilder();
		b.append(this.name+"\n"); //$NON-NLS-1$
		
		b.append("Active	Points	Target	Style	Value	Parameter	Settings	Info	Threshold	Label\n"); //$NON-NLS-1$
		
		for (StylingRule rule : getRules()) {
			b.append(""+ (rule.isActivated()?"Yes":"No")); //$NON-NLS-1$
			
			for (SelectionInstruction si : rule.getSelectors()) { //P=OBJECT F=datatest 
				if ("OBJECT".equals(si.getParameter().getID()) && "datatest".equals(si.getFunction().getID())) {
					b.append("\t"+si.getSettings()); //$NON-NLS-1$
					break;
				}
			}

			for (StylingInstruction<?> si : rule.getStyle().getStylingInstructions()) {

				if (si.mustBeAppliedToText() && si.mustBeAppliedToShape()) b.append("\t"+ ChartsEngineCoreMessages.labelsAndSymbols); //$NON-NLS-1$
				else if (si.mustBeAppliedToText()) b.append("\t"+ ChartsEngineCoreMessages.labels); //$NON-NLS-1$
				else if (si.mustBeAppliedToShape()) b.append("\t"+ ChartsEngineCoreMessages.symbols); //$NON-NLS-1$

				if (si.getInstruction() == null) {
					b.append("\t"); //$NON-NLS-1$
				} else {
					b.append("\t"+si.getInstruction().getLabel()); //$NON-NLS-1$
				}
				
				if (si.getFunction() != null) {
						b.append("\t"+si.getFunction().getLabel());
				} else {
					b.append("\t"); //$NON-NLS-1$
				}
				
				if (si.getParameter() != null) {
					b.append("\t"+si.getParameter().getLabel());
				} else {
					b.append("\t"); //$NON-NLS-1$
				}
				
				if (si.getSettings() != null) {
					b.append("\t"+si.getSettings()); //$NON-NLS-1$
				} else {
					b.append("\t"); //$NON-NLS-1$
				}
			}
			
			//TODO this is speciq to the CA
			String infos = "\t";
			for (SelectionInstruction si : rule.getSelectors()) { // the infos and threshold columns are filled with the selection which is not the dataset or the label selection
				
				if ("Label".equals(si.getParameter().getID()) && "REGEX".equals(si.getFunction().getID())) b.append(""); //$NON-NLS-1$
				else if ("OBJECT".equals(si.getParameter().getID()) && "datatest".equals(si.getFunction().getID())) b.append(""); //$NON-NLS-1$
				else {
					if (infos.length() > 1) infos += " & ";
					// "activated: "+si.isActivated()+" "+
					infos += si.getParameter().getID()+"\t"+si.getFunction().getID()+" "+si.getSettings();
				}
			}
			b.append(infos);
			
			//TODO this is speciq to the CA
			for (SelectionInstruction si : rule.getSelectors()) { // P=Label F=REGEX
				if ("Label".equals(si.getParameter().getID()) && "REGEX".equals(si.getFunction().getID())) {
					b.append("\t"+si.getSettings()); //$NON-NLS-1$
					break;
				}
			}
			
			b.append("\n"); //$NON-NLS-1$
		}
		return b.toString();
	}
}
