/**
 *
 */
package org.txm.chartsengine.core.styling.catalogs;

import java.util.regex.Pattern;

import org.txm.chartsengine.core.messages.ChartsEngineCoreMessages;
import org.txm.chartsengine.core.styling.definitions.SelectionInstructionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.TestFunctionDefinition;
import org.txm.utils.serializable.ValueMinMax;

/**
 * Selectors catalog dedicated to get available selector functions for a specified selection instruction and get available parameters for a given selection instruction+function.
 *
 * @author s, mdecorde
 *
 */
public class SelectionInstructionsCatalog extends InstructionsCatalog<SelectionInstructionDefinition, TestFunctionDefinition> {

	public static String REGEX = "REGEX", EQUALS = "EQUALS", INF = "<", SUP = ">", INFEQUAL = "=<", SUPEQUAL = ">="; //$NON-NLS-1$
	{
		functions.add(new TestFunctionDefinition(REGEX, ChartsEngineCoreMessages.match) {

			@Override
			public Object apply(Object settings, Object d) {
				
				if (settings == null) {
					return false;
				}
				if (settings instanceof Pattern p) {
					return p.matcher(d.toString()).matches();
				}
				return false;
			}
		});
		
		functions.add(new TestFunctionDefinition(EQUALS, "=") { //$NON-NLS-1$

			@Override
			public Boolean apply(Object settings, Object d) {
				
				return settings.equals(d);
			}
		});

		functions.add(new TestFunctionDefinition(INF, "<") { //$NON-NLS-1$

			@Override
			public Object apply(Object settings, Object d) {
				
				if (settings == null) return false; // if not completely set, return true to skip the test
				
				Double value = 0.0d;
				if (d instanceof Number n) {
					value = n.doubleValue();
				} else if (d instanceof ValueMinMax vmm && vmm.value() != null) {
					value = vmm.value().doubleValue();
				}
				
				if (settings instanceof Number set) {
					return value < set.doubleValue();
				} else if (settings instanceof ValueMinMax vmm && vmm.value() != null) {
					return value < vmm.value().doubleValue();
				} else {
					return false;
				}
			}
		});

		functions.add(new TestFunctionDefinition(SUP, ">") { //$NON-NLS-1$

			@Override
			public Object apply(Object settings, Object d) {

				if (settings == null) return false; // if not completely set, return false to skip the test
				
				Double value = 0.0d;
				if (d instanceof Number n) {
					value = n.doubleValue();
				} else if (d instanceof ValueMinMax vmm && vmm.value() != null) {
					value = vmm.value().doubleValue();
				}
				
				if (settings instanceof Number set) {
					return value > set.doubleValue();
				} else if (settings instanceof ValueMinMax vmm && vmm.value() != null) {
					return value > vmm.value().doubleValue();
				} else {
					return false;
				}
			}
		});
		
		functions.add(new TestFunctionDefinition(INFEQUAL, "<=") { //$NON-NLS-1$

			@Override
			public Object apply(Object settings, Object d) {
				
				if (settings == null) return false; // if not completely set, return true to skip the test
				
				Double value = 0.0d;
				if (d instanceof Number n) {
					value = n.doubleValue();
				} else if (d instanceof ValueMinMax vmm && vmm.value() != null) {
					value = vmm.value().doubleValue();
				}
				
				if (settings instanceof Number set) {
					return value <= set.doubleValue();
				} else if (settings instanceof ValueMinMax vmm && vmm.value() != null) {
					return value <= vmm.value().doubleValue();
				} else {
					return false;
				}
			}
		});

		functions.add(new TestFunctionDefinition(SUPEQUAL, ">=") { //$NON-NLS-1$

			@Override
			public Object apply(Object settings, Object d) {

				if (settings == null) return false; // if not completely set, return false to skip the test
				
				Double value = 0.0d;
				if (d instanceof Number n) {
					value = n.doubleValue();
				} else if (d instanceof ValueMinMax vmm && vmm.value() != null) {
					value = vmm.value().doubleValue();
				}
				
				if (settings instanceof Number set) {
					return value >= set.doubleValue();
				} else if (settings instanceof ValueMinMax vmm && vmm.value() != null) {
					return value >= vmm.value().doubleValue();
				} else {
					return false;
				}
			}
		});

	}

	/**
	 *
	 */
	public SelectionInstructionsCatalog() {
		super();
	}

	@Override
	public void initDefinitions() {

	}
}
