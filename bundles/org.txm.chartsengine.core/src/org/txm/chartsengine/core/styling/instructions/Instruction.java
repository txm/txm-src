/**
 *
 */
package org.txm.chartsengine.core.styling.instructions;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import org.txm.chartsengine.core.styling.catalogs.InstructionsCatalog;
import org.txm.chartsengine.core.styling.definitions.InstructionDefinition;
import org.txm.chartsengine.core.styling.definitions.ParameterDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.FunctionDefinition;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.serializable.None;

/**
 * A generic instruction that contains an instruction definition (what to do), a function definition (how), a parameter definition (with what) and a value (MD I'm not sure about this one)
 * 
 * @author s
 *
 */
public abstract class Instruction<InstDef extends InstructionDefinition<?>, FunctDef extends FunctionDefinition> {

	/**
	 * defines what to do
	 */
	protected InstDef instructionDefinition;

	/**
	 * defines what manipulation to do
	 */
	protected FunctDef functionDefinition;

	/**
	 * the settings to use by the function, this value does not depends on the ParameterDefinition. ex, the size limit of the SIZE instruction AND the LINEAR function is different from the ALPHA instruction and LINEAR function
	 */
	protected Serializable settings;

	/**
	 * defines how the function argument are fetch
	 */
	protected ParameterDefinition parameterDefinition;

	/**
	 * Catalog of possible value definitions according to the instruction type.
	 */
	protected InstructionsCatalog<InstDef, FunctDef> instructionsCatalog;
	
	protected boolean activated = true;

	/**
	 *
	 * @param name
	 * @param id
	 */
	public Instruction(InstructionsCatalog<InstDef, FunctDef> instructionsCatalog, String instructionID) {
		this(instructionsCatalog, instructionID, null);
	}

	/**
	 *
	 * @param type
	 * @param typeInternalName
	 */
	public Instruction(InstructionsCatalog<InstDef, FunctDef> instructionsCatalog, String instructionID, String functionID) {
		this(instructionsCatalog, instructionID, functionID, null);
	}
	
	public Serializable getSettings() {
		return settings;
	}


	public void setSettings(Serializable newSettings) {
		this.settings = newSettings;
	}

	public static ArrayList<String> toArrayListOfIDs(Instruction inst) {
		ArrayList<String> line = new ArrayList<>();
		
		if (inst == null) {
			line.add(""); //$NON-NLS-1$
			line.add(""); //$NON-NLS-1$
			line.add(""); //$NON-NLS-1$
			line.add(""); // settings //$NON-NLS-1$
			line.add(""); // human settings //$NON-NLS-1$
			return line; //$NON-NLS-1$
		}
		
		line.add(""+inst.getInstruction().getID()); //$NON-NLS-1$
		
		if (inst.getFunction() != null) {
			line.add(""+inst.getFunction().getID()); //$NON-NLS-1$
		} else {
			line.add(""); //$NON-NLS-1$
		}
		
		if (inst.getParameter() != null) {
			line.add(""+inst.getParameter().getID()); //$NON-NLS-1$
		} else {
			line.add(""); //$NON-NLS-1$
		}
		
		if (inst.getSettings() != null) {
			
			try {
				// To String
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				ObjectOutputStream os = new ObjectOutputStream(bos);
				os.writeObject(inst.getSettings());
				byte[] bytes = java.util.Base64.getEncoder().encode(bos.toByteArray());
				os.close();
				line.add(new String(bytes));
				
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			
			line.add(inst.getSettings().toString());
		} else {
			line.add(""); //$NON-NLS-1$
		}
		
		return line;
	}
	
	/**
	 *
	 * @param type
	 * @param id
	 * @param currentValueID
	 */
	public Instruction(InstructionsCatalog<InstDef, FunctDef> instructionsCatalog, String instructionID, String functionID, String parameterID) {
		this.instructionsCatalog = instructionsCatalog;
		this.setInstructionDefinition(instructionID);
		this.setFunctionDefinition(functionID);
		this.setParameterDefinition(parameterID);
	}

	/**
	 * Sets the instruction definition
	 * 
	 * @param id
	 */
	public void setInstructionDefinition(String instructionID) {
		if (instructionID == null) {
			this.instructionDefinition = null;
		}
		else {
			this.instructionDefinition = this.instructionsCatalog.getInstructionDefinition(instructionID);
		}
	}

	public void setFunctionDefinition(String functionID) {
		if (functionID == null) {
			this.functionDefinition = null;
		}
		else {
			this.functionDefinition = this.instructionsCatalog.getFunction(functionID);
		}
	}

	public void setParameterDefinition(String parameterID) {
		if (parameterID == null) {
			this.parameterDefinition = null;
		}
		else {
			this.parameterDefinition = this.instructionsCatalog.getParameters(parameterID);
		}
	}

	public ParameterDefinition getParameter() {
		return parameterDefinition;
	}

	public FunctDef getFunction() {
		return functionDefinition;
	}

	public FunctDef setFunction(String id) {

		functionDefinition = instructionsCatalog.getFunction(id);
		return functionDefinition;
	}
	
	public void setActivated(boolean b) {

		this.activated = b;
	}
	
	public boolean isActivated() {

		return this.activated;
	}

	public InstructionDefinition<?> getInstruction() {
		return instructionDefinition;
	}

	//
	//	/**
	//	 * Sets the instruction value from the available instruction values by its ID.
	//	 * Also sets a user defined dynamic value.
	//	 *
	//	 * @param currentValueID
	//	 */
	//	public void setValueDefinition(String currentValueID, Object value) {
	//		ValueDefinition valueDefinition = this.instructionDefinition.getValueDefinition(currentValueID);
	//		if (valueDefinition != null) {
	//			this.setValueDefinition(valueDefinition);
	//			if (value != null) {
	//				this.setValue(value);
	//			}
	////			else {
	////				this.setValue(currentValueInternalName);
	////			}
	//		}
	//	}
	//
	//	/**
	//	 * Sets the instruction value.
	//	 * Looks in the catalog for the default value.
	//	 *
	//	 * @param value
	//	 */
	//	protected void setValueDefinition(ValueDefinition valueDefinition) {
	//		if (this.instructionDefinition.getAvailableValueDefinitions().contains(valueDefinition)) {
	//			this.valueDefinition = valueDefinition;
	//			if(this.valueDefinition.getValue() != null) {
	//				this.value = this.valueDefinition.getValue();
	//			}
	//			else {
	//				this.value = null;
	//			}
	//		}
	//		else {
	//			System.err.println(TXMCoreMessages.bind("Trying to set an instruction value that is not available for the instruction internal type \"{0}\". Value = {1}", this.instructionDefinition.getID(), valueDefinition)); //$NON-NLS-1$
	//
	//		}
	//	}
	//
	//
	//	/**
	//	 * Sets the instruction value from the available instruction values by its type internal name.
	//	 *
	//	 * @param currentValueID
	//	 */
	//	public void setValueDefinition(String currentValueID) {
	//		this.setValueDefinition(currentValueID, null);
	//	}


	//	/**
	//	 * Sets the instruction value from the available instruction values by its type internal name.
	//	 * Also sets a user defined dynamic value.
	//	 *
	//	 * @param currentValueInternalName
	//	 */
	//	public void setValue(Object value) {
	//		this.value = value;
	//	}
	//	
	//	public Object getValue() {
	//		return this.value;
	//	}

	//	/**
	//	 * Sets the new type.
	//	 * Also sets the first value definition available as the current value definition.
	//	 *
	//	 * @param type
	//	 * @param typeInternalName
	//	 */
	//	public void setType(String type, String typeInternalName) {
	//		this.name = type;
	//		this.id = typeInternalName;
	//		this.setValueDefinition(this.getAvailableValueDefinitions().get(0));
	//	}
	//
	//
	//	/**
	//	 *
	//	 * @return the type
	//	 */
	//	public String getType() {
	//		return this.instructionDefinition.getLabel();
	//	}

	/**
	 * @return the value
	 */
	public Object getSettingsType() {
		if (settings == null) return None.class;
		return settings.getClass();
	}


	//
	//	/**
	//	 *
	//	 * @return
	//	 */
	//	public ArrayList<ValueDefinition> getAvailableValueDefinitions() {
	//		return this.instructionsCatalog.getInstructionValues(this.instructionDefinition);
	//	}
	//



	/**
	 *
	 * @param indentation the indentation level
	 * @return
	 */
	public String getDump(int indentation) {

		StringBuffer dump = new StringBuffer();
		StringBuffer indentationStr = new StringBuffer();

		for (int i = 0; i < indentation; i++) {
			indentationStr.append("	"); //$NON-NLS-1$
		}

		dump.append(indentationStr);
		dump.append(TXMCoreMessages.bind("* {0} - Instruction: \"{1}\" Function: \"{2}\" Parameter: \"{3}\" Settings: \"{4}\" Activated: {5}", this.getClass().getSimpleName(), this.instructionDefinition, this.functionDefinition, this.parameterDefinition, this.settings, this.activated));
		//				System.lineSeparator() + indentationStr, System.lineSeparator() + this.valueDefinition.getDump(++indentation)));

		//FIXME: SJ: tests
		//		dump.append(System.lineSeparator()).append(indentation).append("Available instruction values for this instruction:");
		//		level++;
		//		for (int i = 0; i < this.getAvailableValueDefinitions().size(); i++) {
		//			dump.append(System.lineSeparator()).append(this.getAvailableValueDefinitions().get(i).getDump(level));
		//		}

		return dump.toString();
	}

	/**
	 *
	 * @return
	 */
	public String getDump() {
		return this.getDump(0);
	}
	
	@Override
	public String toString() {

		return this.instructionDefinition + " = "  //$NON-NLS-1$
				+ (this.parameterDefinition != null ? this.parameterDefinition.getLabel() : "") //$NON-NLS-1$
				+ (this.functionDefinition != null ? " " +this.functionDefinition.getLabel()+" " : "") //$NON-NLS-1$
				+ (this.settings != null ? this.settings.toString() : ""); //$NON-NLS-1$
	}

	/**
	 * @return the instructionsCatalog where the functions and parameters used can be found
	 */
	public InstructionsCatalog<InstDef, FunctDef> getInstructionsCatalog() {
		return instructionsCatalog;
	}



}
