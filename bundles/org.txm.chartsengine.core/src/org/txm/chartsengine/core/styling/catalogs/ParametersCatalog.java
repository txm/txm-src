package org.txm.chartsengine.core.styling.catalogs;

import java.util.LinkedHashMap;

import org.txm.chartsengine.core.styling.definitions.ParameterDefinition;

/**
 * Container of ParameterDefinitions. No type needed.
 * 
 * Parameters will be defined later in the core plugins of the ChartResult of what ever must be styled
 * 
 * @author mdecorde
 *
 */
public class ParametersCatalog extends LinkedHashMap<String, ParameterDefinition> {

	private static final long serialVersionUID = 3782776743040910997L;

	/**
	 * Convenience method to put(parameter.getID(), parameter)
	 * 
	 * @param parameterDefinition the definition to add
	 */
	public void add(ParameterDefinition parameterDefinition) {
		this.put(parameterDefinition.getID(), parameterDefinition);
	}
}
