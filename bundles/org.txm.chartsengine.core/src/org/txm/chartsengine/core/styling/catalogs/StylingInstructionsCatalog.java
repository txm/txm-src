/**
 *
 */
package org.txm.chartsengine.core.styling.catalogs;

import java.awt.Color;
import java.awt.Font;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.ColorMessages;
import org.txm.chartsengine.core.Theme;
import org.txm.chartsengine.core.messages.ChartsEngineCoreMessages;
import org.txm.chartsengine.core.styling.definitions.StylingInstructionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.ConstantFunctionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.GradientFunctionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.NoSettingsTransformationFunctionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.TransformationFunctionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.UserDefinedValueFunctionDefinition;
import org.txm.chartsengine.core.styling.instructions.StylingInstruction;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.utils.serializable.Color1Color2;
import org.txm.utils.serializable.MinMax;
import org.txm.utils.serializable.PatternReplace;
import org.txm.utils.serializable.TestReplace;
import org.txm.utils.serializable.ValueMinMax;

/**
 * Instructions catalog dedicated to get available instruction functions for a specified instruction type and get available parameters for a given instrution+function
 *
 * @author s, mdecorde
 *
 */
public class StylingInstructionsCatalog extends InstructionsCatalog<StylingInstructionDefinition<?>, TransformationFunctionDefinition> {

	private static final long serialVersionUID = -2498273979855604211L;
	private static final String VALUE_ID_USER_ALPHA = "user_alpha"; //$NON-NLS-1$

	// REGEXREPLACE STRINGREPLACE LINEAR LINEAR GRADIANT
	// RAINBOW PALETTE1 COLORPICKER ROUGE GREEN BLUE YELLOW
	{

	}

	/**
	 * The instance of the catalog.
	 */
	protected static StylingInstructionsCatalog instance = null;


	/**
	 * Returns the instance of the catalog.
	 * Creates the instance if it doesn't exist?
	 * 
	 * @return
	 */
	public static StylingInstructionsCatalog getInstance() {
		if (instance == null) {
			instance = new StylingInstructionsCatalog();
		}
		return instance;
	}

	/**
	 *
	 */
	public StylingInstructionsCatalog() {
		super();
	}

	public static String REGEXREPLACE = "REGEXREPLACE", STRINGREPLACE = "STRINGREPLACE", //$NON-NLS-1$
			LINEAR = "LINEAR", LINEAR_GRADIANT = "LINEAR_GRADIANT", //$NON-NLS-1$
			INVERSE_LINEAR = "INVERSE_LINEAR", INVERSE_LINEAR_GRADIANT = "INVERSE_LINEAR_GRADIANT", //$NON-NLS-1$
			USER_FONT = "user_font", ARIAL_FONT = "arial_font", COURRIER_FONT = "courrier_font", TIMES_FONT = "times_font", //$NON-NLS-1$
			RAINBOW = "RAINBOW", PALETTE1 = "PALETTE1", //COLORPICKER="COLORPICKER", //$NON-NLS-1$
			USER_COLOR = "user_color", RED = "red", BLUE = "blue", GREEN = "green", YELLOW = "yellow", CYAN = "cyan", MAGENTA = "magenta", ORANGE = "orange", //$NON-NLS-1$
			X2 = "x2", X3 = "x3", X4 = "x4", X5 = "x5"; //$NON-NLS-1$


	public static double linear_min(double x, double max_input, double min_output, double max_output) {

		double y = x * (max_output - min_output) / max_input + min_output;

		if (y < 1) {
			return min_output;
		}
		else {
			return y;
		}
	}

	@Override
	public void initDefinitions() {

		functions.add(new TransformationFunctionDefinition(REGEXREPLACE, ChartsEngineCoreMessages.regexP0) {

			private static final long serialVersionUID = -8862250380882516960L;

			@Override
			public Object apply(Object settings, Object d) {

				if( settings instanceof PatternReplace p) {

					return p.pattern().matcher(d.toString()).replaceAll(p.replace());
				}
				return null;
			}
		});

		functions.add(new TransformationFunctionDefinition(STRINGREPLACE, ChartsEngineCoreMessages.replaceWithP0) {

			private static final long serialVersionUID = 8182558622472212880L;

			@Override
			public String apply(Object settings, Object d) {
				// <TestReplace, String, String>
				if (settings instanceof TestReplace tr) {
					return tr.pattern().replace(d.toString(), tr.replace());
				}
				return null;
			}
		});

		functions.add(new TransformationFunctionDefinition(LINEAR, ChartsEngineCoreMessages.proportionalToP0) {

			private static final long serialVersionUID = 694075802330342948L;

			@Override
			public Double apply(Object settings, Object d) {

				// MinMax settings, ValueMinMax d
				if (settings instanceof MinMax output && d instanceof ValueMinMax input) {
					
					
					///FIXME: SJ: debug
//					System.err.println("***StylingInstructionsCatalog.initDefinitions().new TransformationFunctionDefinition() {...}.apply() value min/max = " + vmm.min() + "/" + vmm.max());
//					System.err.println("***StylingInstructionsCatalog.initDefinitions().new TransformationFunctionDefinition() {...}.apply() settings min/max = " + minmax.min() + "/" + minmax.max());
					
					double rez;
					
					//FIXME: SJ, doesn't seem to work as expected:  solution 1
//					rez = (vmm.value().doubleValue() - vmm.min().doubleValue()) / (vmm.max().doubleValue() - vmm.min().doubleValue()); // relative position
//					rez = minmax.min().doubleValue() + ((minmax.max().doubleValue() - minmax.min().doubleValue()) * rez); // translated to asked min&max value
//					System.err.println("StylingInstructionsCatalog.initDefinitions().new TransformationFunctionDefinition() {...}.apply() REZ relative + translation = " + rez);
					
					// solution 2: linear interpolation
					double x1 = input.min().doubleValue();
					double x2 = input.max().doubleValue();
					double y1 = output.min().doubleValue();
					double y2 = output.max().doubleValue();
					double y = y1 + (y2 - y1) * (input.value().doubleValue() - x1) / (x2 - x1);
//					System.err.println("StylingInstructionsCatalog.initDefinitions().new TransformationFunctionDefinition() {...}.apply() REZ interpolation = " + y);
					rez = y;

					if (rez - output.min().doubleValue() < -0.00000000000004) { // ex : 0.9999999 - 1 = -0.0000001 OK ; 0.5 - 1 = -0.5 KO
						System.out.println(TXMCoreMessages.bind("Warning, exceeding the MIN limits: value={0} -> output={1} ; value.min={2} value.max={3} output.min={4} output.max={5}",input.value(), rez, input.min(), input.max(), output.min(), output.max())); //$NON-NLS-1$
						rez = output.min().doubleValue();
					} else if (rez - output.max().doubleValue() > 0.00000000000004) { // ex : 255.000002 - 255 = 0.00002 OK ; 256 - 255 = 1 KO
						System.out.println(TXMCoreMessages.bind("Warning, exceeding the MAX limits: value={0} -> output={1} ; value.min={2} value.max={3} output.min={4} output.max={5}",input.value(), rez, input.min(), input.max(), output.min(), output.max())); //$NON-NLS-1$
						rez = output.max().doubleValue();
					}
					
					
					///FIXME: SJ: debug					
//					if((int)rez > 255) {
//						System.err.println("***StylingInstructionsCatalog.initDefinitions().new TransformationFunctionDefinition() {...}.apply() value min/max (x1/x2) = " + x1 + "/" + x2);
//						System.err.println("StylingInstructionsCatalog.initDefinitions().new TransformationFunctionDefinition() {...}.apply() settings min/max (y1/y2) = " + y1 + "/" + y2);
//						System.err.println("StylingInstructionsCatalog.initDefinitions().new TransformationFunctionDefinition() {...}.apply() value (x) = " + input.value().doubleValue());
//						System.err.println("StylingInstructionsCatalog.initDefinitions().new TransformationFunctionDefinition() {...}.apply() REZ interpolation (y) = " + y);
//					}
					
					
					return rez;
				}

				return 0.0d;
			}
		});

		functions.add(new TransformationFunctionDefinition(INVERSE_LINEAR, ChartsEngineCoreMessages.inverseProportionalToP0) {

			private static final long serialVersionUID = -7021003825656686060L;

			@Override
			public Double apply(Object settings, Object d) {

				// MinMax settings, ValueMinMax d
				if (settings instanceof MinMax minmax && d instanceof ValueMinMax vmm) {
					double rez = -(vmm.value().doubleValue() - vmm.min().doubleValue()) / (vmm.max().doubleValue() - vmm.min().doubleValue());
					return minmax.min().doubleValue() + ((minmax.max().doubleValue() - minmax.min().doubleValue()) * rez);
				}

				return 0.0d;
			}
		});

		Color1Color2 grayBlack = new Color1Color2(Color.black, Color.lightGray);
		GradientFunctionDefinition gradientFunction = new GradientFunctionDefinition(LINEAR_GRADIANT, ChartsEngineCoreMessages.gradientOfP0) {

			private static final long serialVersionUID = -7143231607741929491L;

			@Override
			public Color apply(Object settings, Object d) {
				// Color1Color2 settings, ValueMinMax d
				if (settings == null) settings = grayBlack; // ensure no fail
				if (settings instanceof Color c) settings = new Color1Color2(c, Color.white); // Fix
				if (settings instanceof Color1Color2 cc && d instanceof ValueMinMax vmm) {
					return super.apply(cc, d);
				}
				return null;
			}
		};
		functions.add(gradientFunction);

		functions.add(new NoSettingsTransformationFunctionDefinition(RAINBOW, ChartsEngineCoreMessages.rainbow) {

			private static final long serialVersionUID = -3033470317725598283L;
			Color[] rainbow = { Color.pink, Color.blue, Color.green, Color.yellow, Color.orange, Color.red };

			@Override
			public Color apply(Object d) {
				// <Integer, Color>
				if (d instanceof Integer i) {
					return rainbow[i % rainbow.length];
				}

				return null;
			}
		});

		functions.add(new NoSettingsTransformationFunctionDefinition(PALETTE1, ChartsEngineCoreMessages.fourColorPaletteP0) {

			private static final long serialVersionUID = 594220573124161816L;
			Color[] rainbow = { Color.pink, Color.blue, Color.green, Color.yellow, Color.orange, Color.red };

			@Override
			public Color apply(Object d) {

				if (d instanceof Integer i) {
					return rainbow[i % rainbow.length];
				}
				return null;
			}
		});

		functions.add(new UserDefinedValueFunctionDefinition(USER_COLOR, ChartsEngineCoreMessages.colorToSelect) {

			private static final long serialVersionUID = 1L;

			@Override
			public Color apply(Object settings, Object arguments) {
				if (settings == null) return Color.BLACK;
				return (Color) settings;
			}
		});

		functions.add(new UserDefinedValueFunctionDefinition(USER_FONT, ChartsEngineCoreMessages.fontToSelect) {

			private static final long serialVersionUID = 1L;

			@Override
			public Font apply(Object settings, Object arguments) {
				if (settings != null && settings instanceof Font f) return f;
				return null;
			}
		});

		for (int i = 0 ; i < ChartsEngine.FONTS_LOGICAL_NAMES.length ; i++) {
			functions.add(new ConstantFunctionDefinition(ChartsEngine.FONTS_LOGICAL_NAMES[i], ChartsEngine.FONTS_USER_FRIENDLY_NAMES[i], new Font(ChartsEngine.FONTS_LOGICAL_NAMES[i], 0, 14)));
		}

		Theme theme = new Theme();
		ArrayList<Color> colors = theme.getPalette17();
		List<String> names = theme.getColorNamesForPalette(colors);
		for (int i = 0 ; i < colors.size() ; i++) {
			functions.add(new ConstantFunctionDefinition(names.get(i), names.get(i), colors.get(i)));
		}

		ArrayList<Color> greyColors = theme.getGrayScalePaletteFor(10);
		for (int i = 0 ; i < greyColors.size() ; i++) {
			if (i == 0) functions.add(new ConstantFunctionDefinition("BLACK", ColorMessages.black, greyColors.get(i))); //$NON-NLS-1$
			else functions.add(new ConstantFunctionDefinition("GREY"+(10-i)*10, NLS.bind(ColorMessages.greyP0Percent,(10-i)*10), greyColors.get(i))); //$NON-NLS-1$
		}


		// Shape

		// dimensions scaling factor (the same used in default theme for the replaced shapes have same dimension than non-replaced)
		float itemShapesScalingFactor = 1.2f;

		float circleSize = 6.4f * itemShapesScalingFactor;
		GeneralPath shape;
		float scalingFactor;

		// Diamond shape
		scalingFactor = (itemShapesScalingFactor * 3.4f);
		shape = new GeneralPath();
		shape.moveTo(0.0f, -scalingFactor);
		shape.lineTo(scalingFactor, 0.0f);
		shape.lineTo(0.0f, scalingFactor);
		shape.lineTo(-scalingFactor, 0.0f);
		shape.closePath();
		functions.add(new ConstantFunctionDefinition(StylingInstruction.VALUE_ID_SHAPE_DIAMOND, ShapeMessages.diamond, shape));

		//		// Diamond shape
		//				scalingFactor = (itemShapesScalingFactor * 3.4f);
		//				shape = new GeneralPath();
		////				shape.moveTo(-scalingFactor, -scalingFactor/3);
		////				shape.moveTo(-scalingFactor/3, scalingFactor/3);
		////				shape.moveTo(-scalingFactor/3, scalingFactor);
		////				shape.moveTo(scalingFactor/3, scalingFactor);
		////				shape.moveTo(scalingFactor/3, scalingFactor/3);
		////				shape.moveTo(scalingFactor, scalingFactor/3);
		////				shape.moveTo(scalingFactor, -scalingFactor/3);
		////				shape.moveTo(scalingFactor/3, -scalingFactor/3);
		////				shape.moveTo(scalingFactor/3, -scalingFactor);
		////				shape.moveTo(-scalingFactor/3, -scalingFactor);
		////				shape.moveTo(-scalingFactor/3, scalingFactor/3);
		//				
		//				shape.moveTo(0, scalingFactor);
		//				shape.moveTo(scalingFactor, -scalingFactor);
		//				shape.moveTo(-scalingFactor, -scalingFactor);
		//				
		//				shape.closePath();
		//				functions.add(new ConstantFunctionDefinition("test", "test", shape));

		// Vertical Losange shape
		scalingFactor = (itemShapesScalingFactor * 3.4f);
		shape = new GeneralPath();
		shape.moveTo(0.0f, -scalingFactor);
		shape.lineTo(scalingFactor/2f, 0.0f);
		shape.lineTo(0.0f, scalingFactor);
		shape.lineTo(-scalingFactor/2f, 0.0f);
		shape.closePath();
		functions.add(new ConstantFunctionDefinition(StylingInstruction.VALUE_ID_SHAPE_VERTICAL_RHOMBUS, ShapeMessages.rhombus, shape));

		// Horizontal Losange shape
		scalingFactor = (itemShapesScalingFactor * 3.4f);
		shape = new GeneralPath();
		shape.moveTo(0.0f, -scalingFactor/2f);
		shape.lineTo(scalingFactor, 0.0f);
		shape.lineTo(0.0f, scalingFactor/2f);
		shape.lineTo(-scalingFactor, 0.0f);
		shape.closePath();
		functions.add(new ConstantFunctionDefinition(StylingInstruction.VALUE_ID_SHAPE_HORIZONTAL_RHOMBUS, ShapeMessages.rhombusHorizontal, shape));

		// Square
		functions.add(new ConstantFunctionDefinition(StylingInstruction.VALUE_ID_SHAPE_SQUARE, ShapeMessages.square, new Rectangle2D.Double(-3.0, -3.0, 6.0, 6.0)));
		// Rectangles
		functions.add(new ConstantFunctionDefinition(StylingInstruction.VALUE_ID_SHAPE_VERTICAL_RECTANGLE, ShapeMessages.rectangleVertical, new Rectangle2D.Double(-1.5, -3.0, 3.0, 6.0)));
		functions.add(new ConstantFunctionDefinition(StylingInstruction.VALUE_ID_SHAPE_HORIZONTAL_RECTANGLE, ShapeMessages.rectangleHorizontal, new Rectangle2D.Double(-3.0, -1.5, 6.0, 3.0)));

		// FIXME: SJ: should be "Circle" when we'll add the "filled" property
		// Disk
		functions.add(new ConstantFunctionDefinition(StylingInstruction.VALUE_ID_SHAPE_DISK, ShapeMessages.disk, new Ellipse2D.Float(-circleSize / 2, -circleSize / 2, circleSize, circleSize)));
		// Ellipsis
		functions.add(new ConstantFunctionDefinition(StylingInstruction.VALUE_ID_SHAPE_VERTICAL_ELLIPSE, ShapeMessages.ellispe, new Ellipse2D.Double(-circleSize / 4, -circleSize / 2, circleSize / 2, circleSize)));
		functions.add(new ConstantFunctionDefinition(StylingInstruction.VALUE_ID_SHAPE_HORIZONTAL_ELLIPSE, ShapeMessages.ellispeHorizontal, new Ellipse2D.Double(-circleSize / 2, -circleSize / 4, circleSize, circleSize / 2)));

		// Triangle
		scalingFactor = (itemShapesScalingFactor * 3.2f);
		shape = new GeneralPath();
		shape.moveTo(0.0f, -scalingFactor);
		shape.lineTo(scalingFactor, scalingFactor);
		shape.lineTo(-scalingFactor, scalingFactor);
		shape.closePath();
		functions.add(new ConstantFunctionDefinition(StylingInstruction.VALUE_ID_SHAPE_UP_TRIANGLE, ShapeMessages.triangle, BasicShapes.createUpTriangle(scalingFactor)));
		functions.add(new ConstantFunctionDefinition(StylingInstruction.VALUE_ID_SHAPE_DOWN_TRIANGLE, ShapeMessages.triangleDown, BasicShapes.createDownTriangle(scalingFactor)));
		functions.add(new ConstantFunctionDefinition(StylingInstruction.VALUE_ID_SHAPE_CROSS, ShapeMessages.cross, BasicShapes.createDiagonalCross(scalingFactor,scalingFactor)));
		functions.add(new ConstantFunctionDefinition(StylingInstruction.VALUE_ID_SHAPE_HORIZONTAL_CROSS, ShapeMessages.crossHorizontal, BasicShapes.createRegularCross(scalingFactor,scalingFactor)));

		functions.add(new UserDefinedValueFunctionDefinition(VALUE_ID_USER_ALPHA, ChartsEngineCoreMessages.transparency) {

			private static final long serialVersionUID = 7104902197299616773L;

			@Override
			public Integer apply(Object settings, Object arguments) {
				if (settings instanceof Integer i) {
					return i;
				}
				return null;
			}
		});


		//Transparency values from 0% to 100%
		for (int i = 0 ; i <= 10 ; i++) {
			String v = NLS.bind("{0}%", 100-(i*10)); //$NON-NLS-1$
			functions.add(new ConstantFunctionDefinition(v, v, 255d * (i/ 10.0d)));
		}

		// Hidden functions
		functions.add(new ConstantFunctionDefinition(StylingInstruction.ALWAYS_TRUE, ChartsEngineCoreMessages.yes, true));
		functions.add(new ConstantFunctionDefinition(StylingInstruction.ALWAYS_FALSE, ChartsEngineCoreMessages.no, false));

		StylingInstructionDefinition<Shape> hiddenStyle = new StylingInstructionDefinition<Shape>(StylingInstruction.ID_HIDDEN, ChartsEngineCoreMessages.dontDisplay);
		this.addInstruction(hiddenStyle);
		this.associate(hiddenStyle, this.getFunction(StylingInstruction.ALWAYS_TRUE));
		this.associate(hiddenStyle, this.getFunction(StylingInstruction.ALWAYS_FALSE));

		StylingInstructionDefinition<Shape> shapeStyle = new StylingInstructionDefinition<Shape>(StylingInstruction.ID_SHAPE_REPLACEMENT, ChartsEngineCoreMessages.symbol);
		this.addInstruction(shapeStyle);//, STYLE.SHAPE));
		//		this.associate(shapeStyle, this.getFunction("test"));
		this.associate(shapeStyle, this.getFunction(StylingInstruction.VALUE_ID_SHAPE_SQUARE));
		this.associate(shapeStyle, this.getFunction(StylingInstruction.VALUE_ID_SHAPE_DISK));
		this.associate(shapeStyle, this.getFunction(StylingInstruction.VALUE_ID_SHAPE_UP_TRIANGLE));
		this.associate(shapeStyle, this.getFunction(StylingInstruction.VALUE_ID_SHAPE_DOWN_TRIANGLE));
		//		this.associate(shapeStyle, this.getFunction(StylingInstruction.VALUE_ID_SHAPE_CROSS));
		//		this.associate(shapeStyle, this.getFunction(StylingInstruction.VALUE_ID_SHAPE_HORIZONTAL_CROSS));
		this.associate(shapeStyle, this.getFunction(StylingInstruction.VALUE_ID_SHAPE_DIAMOND));
		this.associate(shapeStyle, this.getFunction(StylingInstruction.VALUE_ID_SHAPE_VERTICAL_RECTANGLE));
		this.associate(shapeStyle, this.getFunction(StylingInstruction.VALUE_ID_SHAPE_HORIZONTAL_RECTANGLE));
		this.associate(shapeStyle, this.getFunction(StylingInstruction.VALUE_ID_SHAPE_VERTICAL_ELLIPSE));
		this.associate(shapeStyle, this.getFunction(StylingInstruction.VALUE_ID_SHAPE_HORIZONTAL_ELLIPSE));
		this.associate(shapeStyle, this.getFunction(StylingInstruction.VALUE_ID_SHAPE_VERTICAL_RHOMBUS));
		this.associate(shapeStyle, this.getFunction(StylingInstruction.VALUE_ID_SHAPE_HORIZONTAL_RHOMBUS));
		
		
		

		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
			StylingInstructionDefinition<Color> pStyle = new StylingInstructionDefinition<Color>(StylingInstruction.ID_FONT, ChartsEngineCoreMessages.font);
			this.addInstruction(pStyle);//, STYLE.COLOR));
			for (String fontFamilies : ChartsEngine.FONTS_LOGICAL_NAMES) {
				this.associate(pStyle, this.getFunction(fontFamilies)); // TODO add the commons fonts of charts
			}
			this.associate(pStyle, this.getFunction(USER_FONT));
		}
		StylingInstructionDefinition<Color> cStyle = new StylingInstructionDefinition<Color>(StylingInstruction.ID_COLOR, ChartsEngineCoreMessages.color);
		this.addInstruction(cStyle);//, STYLE.COLOR));
		this.associate(cStyle, this.getFunction(LINEAR_GRADIANT));
		

		for (int i = 0 ; i < colors.size() ; i++) {
			this.associate(cStyle, this.getFunction(names.get(i)));
		}
		
		this.associate(cStyle, this.getFunction(USER_COLOR));
		
		for (int i = 0 ; i < greyColors.size() ; i++) {
			if (i == 0) this.associate(cStyle, this.getFunction("BLACK")); //$NON-NLS-1$
			else this.associate(cStyle, this.getFunction("GREY"+(10-i)*10)); //$NON-NLS-1$
		}
		//this.addInstruction(new StylingInstructionDefinition<Color>(StylingInstruction.ID_VARIABLE_COLOR, "Couleur variable", Color.class));//, STYLE.COLOR));
		StylingInstructionDefinition<Double> tStyle = new StylingInstructionDefinition<Double>(StylingInstruction.ID_TRANSPARENCY, ChartsEngineCoreMessages.transparency);
		this.addInstruction(tStyle);//, STYLE.TRANSPARENCY));
		this.associate(tStyle, this.getFunction(LINEAR));
		this.associate(tStyle, this.getFunction(INVERSE_LINEAR));

		//Associate Transparency values from 0% to 100%
		for (int i = 0 ; i <= 10 ; i++) {
			String v = ""+(i*10)+"%"; //$NON-NLS-1$
			this.associate(tStyle, this.getFunction(v));
		}

		StylingInstructionDefinition<Double> sStyle = new StylingInstructionDefinition<Double>(StylingInstruction.ID_SIZE, ChartsEngineCoreMessages.size);
		this.addInstruction(sStyle);//, STYLE.SIZE));
		this.associate(sStyle, this.getFunction(LINEAR));
		this.associate(sStyle, this.getFunction(INVERSE_LINEAR));

		ConstantFunctionDefinition f = null;
		
		f = new ConstantFunctionDefinition("x 1", "x 1", 1d); //$NON-NLS-1$
		functions.add(f);
		this.associate(sStyle, f);

		for (int i = 1 ; i <= 5 ; i++) {
			if (i == 1) {
//				f = new ConstantFunctionDefinition("x "+i+",10", "x "+i+",10", (i) + 0.10d); //$NON-NLS-1$
//				functions.add(f);
//				this.associate(sStyle, f);
				f = new ConstantFunctionDefinition("x "+i+",2", "x "+i+",2", (i) + 0.20d); //$NON-NLS-1$
				functions.add(f);
				this.associate(sStyle, f);
				f = (new ConstantFunctionDefinition("x "+i+",4", "x "+i+",4", (i) + 0.40d)); //$NON-NLS-1$
				functions.add(f);
				this.associate(sStyle, f);
				f = (new ConstantFunctionDefinition("x "+i+",7", "x "+i+",7", (i) + 0.70d)); //$NON-NLS-1$
				functions.add(f);
				this.associate(sStyle, f);
			} else {
				f = (new ConstantFunctionDefinition("x "+i, "x "+i, ((double)i))); //$NON-NLS-1$
				functions.add(f);
				this.associate(sStyle, f);

				if (i < 1) {
					f = (new ConstantFunctionDefinition("x "+i+",25", "x "+i+",25", (i) + 0.25d)); //$NON-NLS-1$
					functions.add(f);
					this.associate(sStyle, f);
					f = (new ConstantFunctionDefinition("x "+i+",5", "x "+i+",5", (i) + 0.5d)); //$NON-NLS-1$
					functions.add(f);
					this.associate(sStyle, f);
					f = (new ConstantFunctionDefinition("x "+i+",75", "x "+i+",75", (i) + 0.75d)); //$NON-NLS-1$
					functions.add(f);
					this.associate(sStyle, f);
				}
			}
		}

		//		this.associate(sStyle, this.getFunction(X2));
		//		this.associate(sStyle, this.getFunction(X3));
		//		this.associate(sStyle, this.getFunction(X4));

		//		StylingInstructionDefinition<String> rStyle = new StylingInstructionDefinition<String>(StylingInstruction.ID_LABEL_REPLACEMENT, "Remplacement d'étiquette");
		//		this.addInstruction(rStyle);
		//		this.associate(rStyle, this.getFunction(REGEXREPLACE));
		//		this.associate(rStyle, this.getFunction(STRINGREPLACE));
	}

	/*
	 * @Override
	 * public void initDefinitions_previous() {
	 * // Shape
	 * InstructionDefinition instructionDefinition = new InstructionDefinition(StylingInstruction.ID_SHAPE_REPLACEMENT, "Symbole");
	 * this.availableDefinitions.add(instructionDefinition);
	 * // dimensions scaling factor (the same used in default theme for the replaced shapes have same dimension than non-replaced)
	 * float itemShapesScalingFactor = 1.2f;
	 * float circleSize = 6.4f * itemShapesScalingFactor;
	 * GeneralPath shape;
	 * float scalingFactor;
	 * // Diamond shape
	 * scalingFactor = (itemShapesScalingFactor * 3.4f);
	 * shape = new GeneralPath();
	 * shape.moveTo(0.0f, -scalingFactor);
	 * shape.lineTo(scalingFactor, 0.0f);
	 * shape.lineTo(0.0f, scalingFactor);
	 * shape.lineTo(-scalingFactor, 0.0f);
	 * shape.closePath();
	 * instructionDefinition.addAvailableValueDefinition(new ValueDefinition(StylingInstruction.VALUE_ID_SHAPE_DIAMOND, "Diamant", shape));
	 * // Square
	 * instructionDefinition.addAvailableValueDefinition(new ValueDefinition(StylingInstruction.VALUE_ID_SHAPE_SQUARE, "Carré", new Rectangle2D.Double(-3.0, -3.0, 6.0, 6.0)));
	 * // FIXME: SJ: should be "Circle" when we'll add the "filled" property
	 * // Disk
	 * instructionDefinition.addAvailableValueDefinition(new ValueDefinition(StylingInstruction.VALUE_ID_SHAPE_DISK, "Disque", new Ellipse2D.Float((float) (-circleSize / 2),
	 * (float) (-circleSize / 2), circleSize, circleSize)));
	 * // Triangle
	 * scalingFactor = (itemShapesScalingFactor * 3.2f);
	 * shape = new GeneralPath();
	 * shape.moveTo(0.0f, -scalingFactor);
	 * shape.lineTo(scalingFactor, scalingFactor);
	 * shape.lineTo(-scalingFactor, scalingFactor);
	 * shape.closePath();
	 * instructionDefinition.addAvailableValueDefinition(new ValueDefinition(StylingInstruction.VALUE_ID_SHAPE_UP_TRIANGLE, "Triangle vers le haut", shape));
	 * // Color
	 * instructionDefinition = new InstructionDefinition(StylingInstruction.ID_COLOR, "Couleur");
	 * this.availableDefinitions.add(instructionDefinition);
	 * instructionDefinition.addAvailableValueDefinition(new ValueDefinition(StylingInstruction.VALUE_ID_COLOR_RED, "Rouge", Color.red));
	 * instructionDefinition.addAvailableValueDefinition(new ValueDefinition(StylingInstruction.VALUE_ID_COLOR_BLUE, "Bleu", Color.blue));
	 * instructionDefinition.addAvailableValueDefinition(new ValueDefinition(StylingInstruction.VALUE_ID_COLOR_GREEN, "Green", Color.green));
	 * instructionDefinition.addAvailableValueDefinition(new ValueDefinition(StylingInstruction.VALUE_ID_COLOR_RGB, "Color Picker"));
	 * instructionDefinition.addAvailableValueDefinition(new ValueDefinition(StylingInstruction.VALUE_ID_PROPERTY_Q12, "Dégradé de gris sur Q12", "?????")); // valeur inutile ou bien mettre les marges ?
	 * instructionDefinition.addAvailableValueDefinition(new ValueDefinition(StylingInstruction.VALUE_ID_PROPERTY_Q12, "Dégradé de rouge sur Q12", "??????")); // valeur inutile ou bien mettre les marges ?
	 * instructionDefinition.addAvailableValueDefinition(new ValueDefinition(StylingInstruction.VALUE_ID_PROPERTY_Q12, "Palette 1", new Color[] { Color.red, Color.green, Color.blue, Color.cyan }));
	 * // Transparency
	 * instructionDefinition = new InstructionDefinition(StylingInstruction.ID_TRANSPARENCY, "Transparence");
	 * this.availableDefinitions.add(instructionDefinition);
	 * instructionDefinition.addAvailableValueDefinition(new ValueDefinition(StylingInstruction.VALUE_ID_PROPERTY_Q12, "Proportionnel à Q12", "????")); // valeur inutile ou bien mettre les marges ?
	 * instructionDefinition.addAvailableValueDefinition(new ValueDefinition(StylingInstruction.VALUE_ID_TRANSPARENCY_PERCENT, "25%", 63.75));
	 * instructionDefinition.addAvailableValueDefinition( new ValueDefinition(StylingInstruction.VALUE_ID_TRANSPARENCY_PERCENT, "50%", 127.5));
	 * instructionDefinition.addAvailableValueDefinition(new ValueDefinition(StylingInstruction.VALUE_ID_TRANSPARENCY_PERCENT, "75%", 191.25));
	 * // Size
	 * instructionDefinition = new InstructionDefinition(StylingInstruction.ID_SIZE, "Taille");
	 * this.availableDefinitions.add(instructionDefinition);
	 * instructionDefinition.addAvailableValueDefinition(new ValueDefinition(StylingInstruction.VALUE_ID_PROPERTY_Q12, "Proportionnel à Q12", "????")); // valeur inutile ou bien mettre les marges ?
	 * // Label replacement
	 * instructionDefinition = new InstructionDefinition(StylingInstruction.ID_LABEL_REPLACEMENT, "Remplacement d'étiquee");
	 * this.availableDefinitions.add(instructionDefinition);
	 * instructionDefinition.addAvailableValueDefinition(new ValueDefinition(StylingInstruction.VALUE_ID_REGEX, "Remplacement par")); // valeur dynamique / user value // le problème est qu'il faudrait plusieurs instances ici
	 * }
	 */



}
