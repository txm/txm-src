/**
 *
 */
package org.txm.chartsengine.core.styling;

import java.util.LinkedHashSet;

/**
 * Contains pre-defined Style (compilation of style instructions)
 * 
 * @author mdecorde
 *
 */
//FIXME: SJ: seems useless.
public class StylesRegistry extends LinkedHashSet<Style> {

	private static final long serialVersionUID = 7797501183181962757L;

}
