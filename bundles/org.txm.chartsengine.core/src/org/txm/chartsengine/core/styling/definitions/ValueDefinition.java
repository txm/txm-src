/**
 *
 */
package org.txm.chartsengine.core.styling.definitions;

import org.txm.core.messages.TXMCoreMessages;

/**
 * @author s
 *
 */
public class ValueDefinition extends ElementDefinition {

	private static final long serialVersionUID = -3304320931580759417L;
	/**
	 * A default value for the definition.
	 */
	protected Object value;


	/**
	 *
	 * @param name
	 * @param id
	 * @param value
	 */
	public ValueDefinition(String id, String label, Object value) {
		super(id, label);
		this.value = value;
	}


	/**
	 *
	 * @param name
	 * @param id
	 */
	public ValueDefinition(String id, String label) {
		this(id, label, null);
	}


	/**
	 * Gets the default value.
	 *
	 * @return
	 */
	public Object getValue() {
		return this.value;
	}


	@Override
	public String getDump(int indentation) {
		StringBuffer dump = new StringBuffer(super.getDump(indentation));
		dump.append(TXMCoreMessages.bind(", value: {0}", this.value)); //$NON-NLS-1$
		return dump.toString();
	}

}
