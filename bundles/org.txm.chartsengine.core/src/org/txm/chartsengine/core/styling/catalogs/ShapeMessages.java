package org.txm.chartsengine.core.styling.catalogs;

import org.eclipse.osgi.util.NLS;

public class ShapeMessages extends NLS {

	private static final String BUNDLE_NAME = ShapeMessages.class.getPackageName() + ".messages"; //$NON-NLS-1$

	public static String diamond;
	public static String cross;
	public static String triangle;
	public static String disk;
	public static String ellispe;
	public static String ellispeHorizontal;
	public static String rectangleHorizontal;
	public static String rectangleVertical;
	public static String rhombus;
	public static String rhombusHorizontal;
	public static String square;
	public static String triangleDown;
	public static String crossHorizontal;
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, ShapeMessages.class);
	}

	private ShapeMessages() {
	}
}
