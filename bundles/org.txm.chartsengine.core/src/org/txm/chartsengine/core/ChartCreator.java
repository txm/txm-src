package org.txm.chartsengine.core;

import java.awt.Color;
import java.util.ArrayList;

import org.txm.chartsengine.core.results.ChartResult;
import org.txm.utils.logger.Log;


/**
 * Abstract base for chart creation in command plug-ins from a result data.
 * 
 * @author sjacquot
 *
 */
public abstract class ChartCreator<T extends ChartsEngine, R extends ChartResult> {

	public final static String extensionPointId = "org.txm.chartsengine.chartcreator"; //$NON-NLS-1$

	/**
	 * Basic commands constants for chart updating.
	 */
	public static final int SHOW_TITLE = 0, SHOW_LEGEND = 1;

	/**
	 * The linked charts engine.
	 */
	protected T chartsEngine;

	/**
	 * The prefix that will be added to the generated file name.
	 */
	protected String fileNamePrefix;

	/**
	 * A string representing the chart type created by this creator.
	 */
	protected String chartType;

	/**
	 * Default constructor.
	 */
	public ChartCreator() {
	}


	/**
	 * Sets the linked charts engine.
	 * 
	 * @param chartsEngine
	 */
	public void setChartsEngine(T chartsEngine) {
		this.chartsEngine = chartsEngine;
	}

	/**
	 * Creates a chart object according to the specified result data.
	 * 
	 * @param result
	 * @return
	 */
	public abstract Object createChart(R result);


	/**
	 * Updates an existing chart.
	 * By default this method does nothing.
	 * Subclasses should override this method to update a chart without recreating one.
	 * This method is dedicated for example, to hide a title, to change a rendering style, etc.
	 * This method is called by the SWT charts components provider when just after a chart is created.
	 * 
	 * @param result
	 */
	//FIXME: SJ: should be abstract? MD: I guess yes, we'll be sure this method well managed in subclasses 
	public void updateChart(R result) {
		Log.severe("ChartCreator.updateChart(): not implemented. This method is intended to be overridden in subclasses."); //$NON-NLS-1$
	}

	/**
	 * Gets the type of the result data linked with this chart creator.
	 * 
	 * @return
	 */
	public abstract Class<? extends R> getResultDataClass();

	public abstract Class<? extends T> getChartsEngineClass();

	/**
	 * Convenience method.
	 * 
	 * @return the casted charts engine.
	 */
	public T getChartsEngine() {
		return this.chartsEngine;
	}

	/**
	 * @param fileNamePrefix the fileNamePrefix to set
	 */
	public void setFileNamePrefix(String fileNamePrefix) {
		this.fileNamePrefix = fileNamePrefix;
	}

	/**
	 * Gets the color used to draw the shapes of the series.
	 * 
	 * @param chart
	 * @return
	 */
	public abstract ArrayList<Color> getSeriesShapesColors(Object chart);

	/**
	 * @return the fileNamePrefix
	 */
	public String getFileNamePrefix() {
		return fileNamePrefix;
	}

	/**
	 * @return the chartType
	 */
	public String getChartType() {
		return chartType;
	}

	/**
	 * @param chartType the chartType to set
	 */
	public void setChartType(String chartType) {
		this.chartType = chartType;
	}



	//TODO to be cleaned later
	// /**
	// * Creates a chart file according to the specified result data and parameters.
	// *
	// * @param result
	// * @param file
	// * @return
	// */
	// public File createChartFile(ChartResult result, File file) {
	// Object chart = this.createChart(result);
	// // FIXME: SJ: became useless?
	// // Synchronizes the chart with the shared preferences as show title, show legend, rendering colors mode, etc.
	// this.updateChart(result);
	// return this.chartsEngine.createChartFile(chart, file);
	// }
	//
	// /**
	// * Creates a chart file according to the specified result data using the specified prefix for the file name.
	// *
	// * @param result
	// * @param fileNamePrefix
	// * @return
	// */
	// public File createChartFile(ChartResult result, String fileNamePrefix) {
	// return this.createChartFile(result, this.chartsEngine.createTmpFile(fileNamePrefix));
	// }
	//
	//
	// /**
	// * Creates a chart file according to the specified result data.
	// *
	// * @param result
	// * @return
	// */
	// public File createChartFile(ChartResult result) {
	// return this.createChartFile(result, this.fileNamePrefix);
	// }
	//
}
