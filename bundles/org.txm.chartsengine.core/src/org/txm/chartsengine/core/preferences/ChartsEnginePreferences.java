package org.txm.chartsengine.core.preferences;


import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import org.osgi.service.prefs.Preferences;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.core.preferences.TXMPreferences;
import org.txm.utils.logger.Log;

/**
 * Preferences initializer and manager.
 *
 * @author sjacquot
 *
 */
public class ChartsEnginePreferences extends TXMPreferences {


	// Engine preferences
	/**
	 * The name of the current charts engine.
	 */
	public final static String CURRENT_NAME = "current_name"; //$NON-NLS-1$


	/**
	 * The order list used when look for the first charts engine that can manage a chart type.
	 */
	public final static String CHARTS_ENGINES_ORDER = "charts_engines_order"; //$NON-NLS-1$


	/**
	 * Constant for the default export format.
	 */
	public static final String DEFAULT_EXPORT_FORMAT = "default_export_format"; //$NON-NLS-1$

	/**
	 * Constant for the width in centimeters to use for exporting charts.
	 */
	public static final String EXPORT_WIDTH_IN_CM = "export_width_in_centimeters"; //$NON-NLS-1$

	/**
	 * Constant for the resolution in DPI to use for exporting charts.
	 */
	public static final String EXPORT_DPI_RESOLUTION = "export_dpi_resolution"; //$NON-NLS-1$

	/**
	 * Constant for the width in pixels to use for exporting charts.
	 */
	public static final String EXPORT_WIDTH_IN_PIXELS = "export_width_in_pixels"; //$NON-NLS-1$



	// Result preferences
	/**
	 * Constant for showing title or not in the charts.
	 */
	public final static String SHOW_TITLE = "show_title"; //$NON-NLS-1$


	/**
	 * Constant for showing legend or not in the charts.
	 */
	public final static String SHOW_LEGEND = "show_legend"; //$NON-NLS-1$


	/**
	 * Constant for showing grid/lines or not in the charts.
	 */
	public final static String SHOW_GRID = "show_grid"; //$NON-NLS-1$


	/**
	 * Constant for the color mode (colors, grayscale, monochrome, Black and White).
	 */
	public final static String RENDERING_COLORS_MODE = "colors_mode"; //$NON-NLS-1$


	/**
	 * Constant for the color to use when in monochrome mode.
	 */
	public final static String MONOCHROME_COLOR = "monochrome_color"; //$NON-NLS-1$

	/**
	 * Constant for the font.
	 */
	public final static String FONT = "chart_font"; //$NON-NLS-1$

	/**
	 * Same style for all lines.
	 */
	public static final String MULTIPLE_LINE_STROKES = "multiple_line_strokes"; //$NON-NLS-1$

	/**
	 * Chart type.
	 */
	public final static String CHART_TYPE = "chart_type"; //$NON-NLS-1$

	/**
	 * Default chart type name.
	 */
	public final static String DEFAULT_CHART_TYPE = "[Default]"; //$NON-NLS-1$



	/**
	 * Gets the instance.
	 *
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(ChartsEnginePreferences.class)) {
			new ChartsEnginePreferences();
		}
		return TXMPreferences.instances.get(ChartsEnginePreferences.class);
	}

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.put(CURRENT_NAME, "jfreechart_charts_engine"); //$NON-NLS-1$
		preferences.put(DEFAULT_EXPORT_FORMAT, "svg"); //$NON-NLS-1$

		initializeChartsEngineSharedPreferences(preferences);

		//	this.computeExportWidthInPixels();
	}

	/**
	 * Initializes the charts engine default preferences.
	 * This method is also used to define the charts preference for each command using charts.
	 *
	 * @param preferences
	 */
	public static void initializeChartsEngineSharedPreferences(Preferences preferences) {

		preferences.put(CHART_TYPE, DEFAULT_CHART_TYPE);
		preferences.putBoolean(SHOW_TITLE, true);
		preferences.putBoolean(SHOW_LEGEND, true);
		preferences.putBoolean(SHOW_GRID, true);
		preferences.putInt(RENDERING_COLORS_MODE, ChartsEngine.RENDERING_COLORS_MODE);
		preferences.put(MONOCHROME_COLOR, "0,220,20"); //$NON-NLS-1$
		preferences.putBoolean(MULTIPLE_LINE_STROKES, false);

		// Default export configuration
		preferences.putDouble(EXPORT_WIDTH_IN_CM, 11);
		preferences.putInt(EXPORT_DPI_RESOLUTION, 300);
		preferences.putInt(EXPORT_WIDTH_IN_PIXELS, 1299);


		//FIXME: SJ: old version for Font
		// Check the target OS and set the default font using encoded String in JFace StringConverter format (?|Font name|size|style|?|?|...)
		//		String defaultFont;
		//		if (OSDetector.isFamilyUnix()) {
		//			defaultFont = "1|Lucida Sans|11.0|1|GTK|1|"; //$NON-NLS-1$
		//		}
		//		else {
		//			defaultFont = "1|Lucida Sans Unicode|11.0|1|WINDOWS|1|-16|0|0|0|400|0|0|0|0|3|2|1|34|Lucida Sans Unicode;"; //$NON-NLS-1$
		
		//		}
		// style [0 = regular, 1 = italic, 2 = bold, 3 = bold italic]
		//
		// Internal simplification in our UI:
		// Arial = SansSerif
		// Courier = Monospaced
		// Times = Serif


		// FIXME: SJ: new version, but actually it should not be based on JFace format. Most of the font usage is a at core/non-ui level and use AWT Font object.
		// May need to use e.g. 3 variables font_size, font_type, font_name. Then if we need to use a SWT/JFace Font object, make a function to convert it, 2 ways.
		String defaultFont = "1|SansSerif|14|0";  //$NON-NLS-1$
		//String defaultFont = "1|Monospaced|18|3";  //$NON-NLS-1$
		
		preferences.put(FONT, defaultFont);
	}


	/**
	 * Computes and returns the export width in pixels according to the DPI and width in centimeters preferences.
	 * 
	 * @return
	 */
	//FIXME: SJ: should be moved?
	public int getExportWidthInPixels(int dpi, double centimeters) {
		return (int) (dpi * centimeters / 2.54); // 2.54 = 1 inch
	}

	
	/**
	 * Gets the engines order from the specified string with | separator.
	 *
	 * @param enginesOrder
	 * @return
	 */
	//FIXME: SJ: not fully tested and unfinished feature
	public static String[] getEnginesOrder(String enginesOrder) {

		String[] orderedEnginesNames = enginesOrder.split("\\|"); //$NON-NLS-1$

		if (orderedEnginesNames.length == 1 && orderedEnginesNames[0].isEmpty()) {
			return null;
		}
		else {
			return orderedEnginesNames;
		}
	}


	/**
	 * Creates the engines order string with | separator from the specified engines list.
	 *
	 * @param enginesNames
	 * @return
	 */
	//FIXME: SJ: not fully tested and unfinished feature
	public static String createEnginesOrder(String[] enginesNames) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < enginesNames.length; i++) {
			if (i > 0) {
				sb.append("|"); //$NON-NLS-1$
			}
			sb.append(enginesNames[i]);
		}

		Log.finest("ChartsEnginePreferences.createChartsEnginesOrder():" + sb); //$NON-NLS-1$

		return sb.toString();
	}

	/**
	 * Gets the name of the first engine stored in the engines order preference.
	 *
	 * @return
	 */
	//FIXME: SJ: not fully tested and unfinished feature
	public static String getFirstEngineName() {
		return getEnginesOrder(getInstance().getString(CHARTS_ENGINES_ORDER))[0];
	}


	/**
	 * Gets the engines order from stored in the preferences.
	 *
	 * @return
	 */
	//FIXME: SJ: not fully tested and unfinished feature
	public String[] getEnginesOrder() {
		return getEnginesOrder(this.getString(ChartsEnginePreferences.CHARTS_ENGINES_ORDER));
	}


}

