package org.txm.chartsengine.core;

import org.eclipse.osgi.util.NLS;

public class ColorMessages extends NLS {

	private static final String BUNDLE_NAME = ColorMessages.class.getPackageName() + ".messages"; //$NON-NLS-1$

	public static String blue;

	public static String blue_dark;

	public static String blue_fluo;

	public static String blue_light;

	public static String green;

	public static String green_blue_fluo;

	public static String green_dark;

	public static String green_fluo;

	public static String green_light;

	public static String pink;

	public static String pink_fuchsia;

	public static String red;

	public static String red_bright;

	public static String red_dark;

	public static String violet;

	public static String violet_fluo;

	public static String voilet_fluo;

	public static String warningPaletteNamesSizeDoNotMatchP0;

	public static String yellow;

	public static String yellow_fluo;

	public static String yellow_ocher;

	public static String black;
	public static String white;
	public static String grey;
	public static String greyP0Percent;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, ColorMessages.class);
	}

	private ColorMessages() {
	}
}
