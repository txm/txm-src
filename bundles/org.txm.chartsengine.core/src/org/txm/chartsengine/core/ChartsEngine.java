package org.txm.chartsengine.core;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.osgi.util.NLS;
import org.txm.Toolbox;
import org.txm.chartsengine.core.messages.ChartsEngineCoreMessages;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.core.engines.Engine;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.TXMResult;
import org.txm.utils.logger.Log;


/**
 * Common charts engine. This is the superclass of all chart engine implementations.
 * 
 * This class provides methods to create charts from variable chart creators stored by type of result data such as CA, CAH, Singular values, etc.
 * 
 * It also stores the available chart creators
 * 
 * Provides methods to output the chart created by the ChartCreators
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public abstract class ChartsEngine implements Engine {

	public final static String EXTENSION_POINT_ID = "org.txm.chartsengine.chartsengine"; //$NON-NLS-1$

	/**
	 * Constants for output formats and file extensions.
	 */
	public final static String OUTPUT_FORMAT_NONE = "NONE", OUTPUT_FORMAT_JPEG = "jpeg", OUTPUT_FORMAT_PDF = "pdf", OUTPUT_FORMAT_PNG = "png", OUTPUT_FORMAT_PS = "ps", OUTPUT_FORMAT_SVG = "svg", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
			OUTPUT_FORMAT_BMP = "bmp", OUTPUT_FORMAT_GIF = "gif", OUTPUT_FORMAT_TIFF = "tiff"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

	/**
	 * Constants for the colors rendering modes.
	 */
	public final static int RENDERING_COLORS_MODE = 0, RENDERING_GRAYSCALE_MODE = 1, RENDERING_BLACK_AND_WHITE_MODE = 2, RENDERING_MONOCHROME_MODE = 3;


	/**
	 * Font styles definition.
	 */
	// FIXME: SJ: see if we move this in an other class to share it
	public final static String[] FONT_STYLES = { "Regular", "Bold", "Italic", "Bold italic" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	/**
	 * Logical font names definition.
	 */
	// FIXME: SJ: see if we move this in an other class to share it
	public final static String[] FONTS_LOGICAL_NAMES = { "SansSerif", "Monospaced", "Serif" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

	/**
	 * User-friendly font names definition.
	 */
	// FIXME: SJ: see if we move this in an other class to share it
	public final static String[] FONTS_USER_FRIENDLY_NAMES = { "Arial", "Courier", "Times" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$



	/**
	 * The engine description.
	 */
	protected String description;

	/**
	 * The current output format.
	 */
	protected String outputFormat;

	/**
	 * Chart creators extension contributions.
	 */
	protected HashMap<Class<? extends ChartResult>, HashMap<String, ChartCreator<ChartsEngine, ChartResult>>> chartCreators;

	/**
	 * Current theme.
	 */
	protected Theme theme;

	/**
	 * UI related, this preference can be used to tell if the chart must be updated directly after editing a widget value or only after pressed a "Compute" button.
	 */
	// FIXME: SJ: this preference is not used at this time, see if it's useless or not.
	protected boolean directComputing;



	// add the preference node qualifier to the preferences so the commands use some charts engine level preferences if they do not redefine themselves
	static {
		TXMPreferences.additionalNodesQualifiers.add(ChartsEnginePreferences.getInstance().getPreferencesNodeQualifier());
	}

	/**
	 * Creates a charts engine with the specified name, description and output format.
	 * 
	 * @param description
	 * @param outputFormat
	 */
	protected ChartsEngine(String description, String outputFormat) {

		this.chartCreators = new HashMap<>();
		this.description = description;
		this.outputFormat = outputFormat;
		this.theme = new Theme();
		// FIXME: SJ: this preference is not used at this time, see if it's useless or not.
		this.directComputing = true;
	}

	/**
	 * Gets a chart creator for the specified result and chart type.
	 * 
	 * @param result
	 * @param chartType
	 * @param showLogWarning
	 * @return A chart creator if it exists otherwise <code>null</code>
	 */
	@SuppressWarnings("unchecked")
	public <CR extends ChartResult> ChartCreator<ChartsEngine, CR> getChartCreator(CR result, String chartType, boolean showLogWarning) {

		ChartCreator<ChartsEngine, CR> chartCreator = null;
		HashMap<String, ChartCreator<ChartsEngine, ChartResult>> map = this.chartCreators.get(result.getClass());
		if (map != null) {
			ChartCreator<ChartsEngine, ChartResult> chartCreator2 = map.get(chartType);
			if (chartCreator2 != null && chartCreator2.getResultDataClass().isAssignableFrom(result.getClass())) { // ensure the ChartCreator actually manages the CR class
				chartCreator = (ChartCreator<ChartsEngine, CR>) chartCreator2;
			}
		}

		if (chartCreator == null && showLogWarning) {
			Log.fine(this.getName() + ": No chart creator can be found for result: " + result.getClass() + " and chart type: " + chartType + "."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		return chartCreator;
	}

	/**
	 * Gets a chart creator for the specified result and default chart type.
	 * 
	 * @param result
	 * @param showLogWarning
	 * @return
	 */
	public ChartCreator<ChartsEngine, ChartResult> getChartCreator(ChartResult result, boolean showLogWarning) {
		return this.getChartCreator(result, result.getChartType(), showLogWarning);
	}

	/**
	 * Gets a chart creator for the specified result and chart type.
	 * 
	 * @param result
	 * @param chartType
	 * @return
	 */
	public <CR extends ChartResult> ChartCreator<ChartsEngine, CR> getChartCreator(CR result, String chartType) {
		return this.getChartCreator(result, chartType, true);
	}


	/**
	 * Gets a chart creator for the specified result and default chart type.
	 * 
	 * @param result
	 * @return
	 */
	public <CR extends ChartResult> ChartCreator<ChartsEngine, CR> getChartCreator(CR result) {
		return this.getChartCreator(result, result.getChartType());
	}


	/**
	 * Gets a list of all installed chart creators for the specified result.
	 * 
	 * @param result
	 * @return
	 */
	public ArrayList<ChartCreator<ChartsEngine, ChartResult>> getChartCreators(ChartResult result) {
		ArrayList<ChartCreator<ChartsEngine, ChartResult>> chartCreators = new ArrayList<>();
		HashMap<String, ChartCreator<ChartsEngine, ChartResult>> map = this.chartCreators.get(result.getClass());
		if (map != null) {
			chartCreators.addAll(map.values());
		}
		return chartCreators;
	}


	/**
	 * Checks if the charts engine can create a chart according to the specified result and chart type.
	 * 
	 * @param chartType
	 * @return <code>True</code> if a chart creator can be found, otherwise <code>false</code>
	 */
	public boolean canCreateChart(ChartResult result, String chartType) {
		return this.getChartCreator(result, chartType, false) != null;
	}

	/**
	 * Checks if the charts engine can create a chart according to the specified result data type.
	 * 
	 * @return <code>True</code> if a chart creator can be found, otherwise <code>false</code>
	 */
	public boolean canCreateChart(ChartResult result) {
		return this.getChartCreator(result, false) != null;
	}


	/**
	 * Adds a chart creator for the specified result data type and chart type.
	 * 
	 * @param resultDataType
	 * @param chartCreator
	 */
	public void addChartCreator(Class<? extends ChartResult> resultDataType, ChartCreator<ChartsEngine, ChartResult> chartCreator) {

		String chartType = chartCreator.getChartType();
		if (chartType == null) {
			chartType = ChartsEnginePreferences.DEFAULT_CHART_TYPE;
		}

		if (!this.chartCreators.containsKey(resultDataType)) {
			HashMap<String, ChartCreator<ChartsEngine, ChartResult>> m = new HashMap<>();
			m.put(chartType, chartCreator);
			this.chartCreators.put(resultDataType, m);
		}
		else {
			this.chartCreators.get(resultDataType).put(chartType, chartCreator);
		}


		chartCreator.setChartsEngine(this);

		String log = this.getName() + ": Chart creator " + chartCreator.getClass().getSimpleName() + " added for result data type: " + resultDataType; //$NON-NLS-1$ //$NON-NLS-2$

		if (chartType != null) {
			log += " (chart type: " + chartType + ")"; //$NON-NLS-1$ //$NON-NLS-2$
		}

		log += "."; //$NON-NLS-1$

		Log.finest(log);

	}

	/**
	 * Gets the current charts engine.
	 * Convenience method to return the current chart engine of the charts engines manager.
	 * 
	 * @return the current charts engine
	 */
	public static ChartsEngine getCurrent() {
		return ChartsEnginesManager.getInstance().getCurrentEngine();
	}

	/**
	 * Gets a charts engine by name.
	 * Convenience method to return a chart engine of the charts engines manager.
	 * 
	 * @return the current charts engine
	 */
	public static ChartsEngine getEngine(String name) {
		return ChartsEnginesManager.getInstance().getEngine(name);
	}

	// FIXME: SJ: became useless?
	// /**
	// * Gets an installed charts engine according to its class type.
	// * @param type
	// * @return the installed charts engine according to its class type if it exists otherwise null
	// */
	// @Deprecated
	// public static ChartsEngine getChartsEngine(Class type) {
	// ChartsEngine chartsEngine = null;
	// for(int i = 0; i < ChartsEngine.chartsEngines.size(); i++) {
	// if(ChartsEngine.chartsEngines.get(i).getClass().equals(type)) {
	// chartsEngine = ChartsEngine.chartsEngines.get(i);
	// break;
	// }
	// }
	// return chartsEngine;
	// }
	// /**
	// * Gets an installed charts engine according to its name.
	// * @param type
	// * @return the installed charts engine according to its name if it exists otherwise null
	// */
	// @Deprecated
	// public static ChartsEngine getChartsEngineByName(String name) {
	// ChartsEngine chartsEngine = null;
	// for(int i = 0; i < ChartsEngine.chartsEngines.size(); i++) {
	// if(chartsEngines.get(i).getName().equals(name)) {
	// chartsEngine = ChartsEngine.chartsEngines.get(i);
	// break;
	// }
	// }
	// return chartsEngine;
	// }
	//
	// /**
	// * Gets the first charts engine that can render the specified result.
	// * @param result
	// * @return
	// */
	// @Deprecated
	// public static ChartsEngine getChartsEngine(ChartResult result) {
	// ChartsEngine chartsEngine = null;
	// for (int i = 0; i < ChartsEngine.chartsEngines.size(); i++) {
	// if(ChartsEngine.chartsEngines.get(i).canCreateChart(result)) {
	// chartsEngine = ChartsEngine.chartsEngines.get(i);
	// break;
	// }
	// }
	// return chartsEngine;
	// }

	@Override
	public boolean initialize() throws Exception {
		this.registerChartCreatorExtensions();
		return true;
	}

	
	/**
	 * Scales a Graphics2D according to input and desired output dimensions.
	 * 
	 * @param g the Graphics2D to scale
	 * @param outputWidth the output width
	 * @param outputHeight the output height
	 * @param inputWidth
	 * @param inputHeight
	 */
	public static void scaleGraphics2D(Graphics2D g, int outputWidth, int outputHeight, int inputWidth, int inputHeight) {
		double scaleX = outputWidth / (double) inputWidth;
		double scaleY = outputHeight / (double) inputHeight;
		AffineTransform st = AffineTransform.getScaleInstance(scaleX, scaleY);
		g.transform(st);
		
//		System.err.println("ChartsEngine.scaleGraphics2D() outputWidth = " + outputWidth + ", outputHeight = " + outputHeight);
//		System.err.println("ChartsEngine.scaleGraphics2D() inputWidth = " + inputWidth + ", inputHeight = " + inputHeight);
//		System.err.println("ChartsEngine.scaleGraphics2D() scale x = " + scaleX + ", scale y = " + scaleY);
	}
	

	/**
	 * Populates the chart creators from loaded extensions according to the specified chars engine name.
	 * 
	 * @param chartsEngineClass
	 */
	protected void registerChartCreatorExtensions(Class<? extends ChartsEngine> chartsEngineClass) {

		IConfigurationElement[] contributions = Platform.getExtensionRegistry().getConfigurationElementsFor(ChartCreator.extensionPointId);

		for (int i = 0; i < contributions.length; i++) {

			try {
				@SuppressWarnings("unchecked")
				ChartCreator<ChartsEngine, ChartResult> chartCreator = (ChartCreator<ChartsEngine, ChartResult>) contributions[i].createExecutableExtension("class"); //$NON-NLS-1$

				if (chartsEngineClass.equals(chartCreator.getChartsEngineClass())) {
					chartCreator.setFileNamePrefix(contributions[i].getAttribute("fileNamePrefix")); //$NON-NLS-1$
					chartCreator.setChartType(contributions[i].getAttribute("chartType")); //$NON-NLS-1$
					this.addChartCreator(chartCreator.getResultDataClass(), chartCreator);
				}
			}
			catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Populates the chart creators from loaded extensions.
	 */
	public void registerChartCreatorExtensions() {
		this.registerChartCreatorExtensions(this.getClass());
	}

	/**
	 * Creates a AWT font from the specified encoded String (in JFace StringConverter format, e.g.: "1|Lucida Sans Unicode|12.0|0|WINDOWS|1|-16|0|0|0|400|0|0|0|0|3|2|1|34|Lucida Sans Unicode;").
	 * 
	 * @param encodedStr
	 * @return the decoded font
	 */
	public static Font createFont(String encodedStr) {
		String[] data = encodedStr.split("\\|");///$NON-NLS-1$
		Font font = new Font(data[1], Integer.valueOf(data[3]), Integer.parseInt(data[2].split("\\.")[0])); // font name, font style, font size //$NON-NLS-1$
		return font;
	}

	/**
	 * Creates a AWT font from the current global charts engine font preference encoded String (in JFace StringConverter format, e.g.: "1|Lucida Sans Unicode|12.0|0|WINDOWS|1|-16|0|0|0|400|0|0|0|0|3|2|1|34|Lucida Sans Unicode;").
	 * 
	 * @return
	 */
	public static Font createFont() {
		return createFont(ChartsEnginePreferences.getInstance().getString(ChartsEnginePreferences.FONT));
	}

	//	/**
	//	 * Creates a temporary file in USER_TXM_HOME\results with the specified prefix for writing the chart.
	//	 * The suffix is computed according to the current output format of the charts engine.
	//	 * 
	//	 * @return
	//	 */
	//	public File createTmpFile(String prefix) {
	//		File file = null;
	//		try {
	//			File resultsDir = new File(Toolbox.getTxmHomePath(), "results"); //$NON-NLS-1$
	//			resultsDir.mkdirs();
	//			
	//			// right padding to 3 characters long
	//			if (prefix.length() < 3) {
	//				prefix = String.format("%1$-3s", prefix).replace(' ', '_'); //$NON-NLS-1$
	//			}
	//			
	//			file = File.createTempFile(prefix, "." + this.outputFormat, resultsDir); //$NON-NLS-1$
	//		}
	//		catch (IOException e) {
	//			Log.severe(NLS.bind(ChartsEngineCoreMessages.error_cantCreateTmpChartFileP0, e));
	//			Log.printStackTrace(e);
	//		}
	//		
	//		// TODO : old code, useful ? and/or return null instead of the file if canWrite() return false ?
	//		if (!file.canWrite()) {
	//			Log.severe(NLS.bind(ChartsEngineCoreMessages.error_cantWriteInFileP0, file));
	//		}
	//		return file;
	//	}

	/**
	 * Get a Java File in USER_TXM_HOME\results with a specified prefix given by the ChartCreator of the result for writing the chart.
	 * The suffix is computed according to the current output format of the charts engine.
	 * 
	 * @return the file
	 */
	public File createFileForChart(ChartResult result) {
		return createChartFile(result, result.getChartCreator().getFileNamePrefix());
	}

	/**
	 * Get a Java File in USER_TXM_HOME\results with the specified prefix for writing the chart.
	 * 
	 * The suffix is computed according to the current output format of the charts engine.
	 * 
	 * @return the file
	 */
	public File createChartFile(ChartResult result, String prefix) {
		File file = null;
		File resultsDir = new File(Toolbox.getTxmHomePath(), "results"); //$NON-NLS-1$
		resultsDir.mkdirs();

		// right padding to 3 characters long
		if (prefix.length() < 3) {
			prefix = String.format("%1$-3s", prefix).replace(' ', '_'); //$NON-NLS-1$
		}

		file = new File(resultsDir, prefix + result.getParametersNodePath().replaceAll("/", "_") + "." + this.outputFormat); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		// TODO : old code, useful ? and/or return null instead of the file if canWrite() return false ?
		if (!resultsDir.canWrite()) {
			Log.severe(NLS.bind(ChartsEngineCoreMessages.error_cantWriteInFileP0, file));
		}
		return file;
	}

	// public File createChartFile(ChartResult result);

	// /**
	// * Creates a file form the specified chart object and output format.
	// *
	// * @param chart
	// * @param file
	// * @return
	// */
	// public abstract File createChartFile(Object chart, File file, String outputFormat);
	//
	// /**
	// * Creates a chart according to the specified result and output format and writes it in the specified file.
	// *
	// * @param result
	// * @param file
	// * @param outputFormat
	// * @return
	// */
	// public File createChartFile(ChartResult result, File file, String outputFormat) {
	// // FIXME : SJ: bad ? Switch the current charts engine output format, export the chart, and then roll back the output format
	// FIXME: this old code don't work with asynchronous calls
	// String oldOutputFormat = this.outputFormat;
	// this.setOutputFormat(outputFormat);
	// file = this.createChartFile(result.getChart(), file);
	// this.outputFormat = oldOutputFormat;
	// return file;
	// }

	// /**
	// * Creates a chart file according to the specified result data, parameters and default outputFormat.
	// *
	// * @param result
	// * @param file
	// * @return
	// */
	// public File createChartFile(ChartResult result, File file) {
	// Object chart = this.createChart(result);
	// // FIXME: SJ: became useless?
	// // Synchronizes the chart with the shared preferences as show title, show legend, rendering colors mode, etc.
	// // this.updateChart(result);
	// return this.exportChartToFile(chart, file, this.outputFormat, 0, 0);
	// }


	// /**
	// * Creates a chart file according to the specified result data using the specified prefix for the file name.
	// *
	// * @param result
	// * @param fileNamePrefix
	// * @return
	// */
	// public File createChartFile(ChartResult result, String fileNamePrefix) {
	// return this.createChartFile(result, this.createTmpFile(fileNamePrefix));
	// }


	/**
	 * Exports the chart to the specified file. Also crops the image according to the specified drawing area values.
	 * 
	 * @param chart
	 * @param file
	 * @param outputFormat
	 * @param imageWidth
	 * @param imageHeight
	 * @param drawingAreaX
	 * @param drawingAreaY
	 * @param drawingAreaWidth
	 * @param drawingAreaHeight
	 * @return
	 */
	protected abstract File exportChartToFile(Object chart, File file, String outputFormat, int imageWidth, int imageHeight, int drawingAreaX, int drawingAreaY, int drawingAreaWidth,
			int drawingAreaHeight);

	/**
	 * Exports the chart to the specified file.
	 * 
	 * @param chart
	 * @param file
	 * @param outputFormat
	 * @param imageWidth
	 * @param imageHeight
	 * @return
	 */
	protected File exportChartToFile(Object chart, File file, String outputFormat, int imageWidth, int imageHeight) {
		return this.exportChartToFile(chart, file, outputFormat, imageWidth, imageHeight, 0, 0, imageWidth, imageHeight);
	}

	/**
	 * Export a chart in a result file. The file path is generated and uniq for a result
	 * 
	 * image size is set to 1024x576
	 * 
	 * @param result
	 * @return
	 */
	public File exportChartResultToFile(ChartResult result, String outputFormat) {
		return exportChartResultToFile(result, 1024, 576, outputFormat);
	}

	/**
	 * Export a chart in a result file. The file path is generated and uniq for a result
	 * 
	 * @param result
	 * @param imageWidth image width
	 * @param imageHeight image height
	 * @return
	 */
	public File exportChartResultToFile(ChartResult result, int imageWidth, int imageHeight, String outputFormat) {
		return exportChartResultToFile(result, createFileForChart(result), imageWidth, imageHeight, outputFormat);
	}

	/**
	 * Export a chart in a file
	 * 
	 * @param result
	 * @return
	 */
	public abstract File exportChartResultToFile(ChartResult result, File file, int imageWidth, int imageHeight, String outputFormat);

	/**
	 * Gets the current output format.
	 * 
	 * @return
	 */
	public String getOutputFormat() {
		return this.outputFormat;
	}

	/**
	 * Returns the output display formats supported by the implementation.
	 * This method is intended to be used in the purpose of knowing all the possible display outputs offered by the charts engine implementation and dedicated to the UI visualization (ex. Java2D,
	 * iplots, etc.).
	 * 
	 * @return
	 */
	public abstract ArrayList<String> getSupportedOutputDisplayFormats();

	/**
	 * Returns the output file formats supported by the implementation.
	 * This method is intended to be used in the purpose of knowing all the possible file outputs offered by the charts engine implementation.
	 * 
	 * @return
	 */
	public abstract ArrayList<String> getSupportedOutputFileFormats();

	/**
	 * Sets the output format for the generated charts.
	 * 
	 * @param outputFormat the output format to set
	 */
	public void setOutputFormat(String outputFormat) {
		this.outputFormat = outputFormat;
	}

	/**
	 * Auto-populate supported output file raster formats list from available system image writers.
	 * 
	 * @param supportedOutputFileFormats
	 * @return
	 */
	// FIXME: SJ: this method is useful but can lead to a deadlock when call during the SWT initialization maybe because of a multi-thread problem
	public static ArrayList<String> populateSupportedOutputRasterFileFormats(ArrayList<String> supportedOutputFileFormats) {

		String[] writerFormatNames = javax.imageio.ImageIO.getWriterFormatNames();

		for (int i = 0; i < writerFormatNames.length; i++) {
			if (!supportedOutputFileFormats.contains(writerFormatNames[i]) && !supportedOutputFileFormats.contains(writerFormatNames[i].toLowerCase())) {
				supportedOutputFileFormats.add(writerFormatNames[i].toLowerCase());
			}
		}
		return supportedOutputFileFormats;
	}

	@Override
	public String toString() {

		String str = this.getName() + " (description: " + this.description + ", current output format: " + this.outputFormat; //$NON-NLS-1$ //$NON-NLS-2$

		str += ", supported output display formats: " + this.getSupportedOutputDisplayFormats().toString().replaceAll("\\[|\\]", "");  //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
		str += ", supported output file formats: " + this.getSupportedOutputFileFormats().toString().replaceAll("\\[|\\]", "");  //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$

		str += ")"; //$NON-NLS-1$

		return str;
	}

	/**
	 * @return the theme
	 */
	public Theme getTheme() {

		return theme;
	}

	/**
	 * @param theme the theme to set
	 */
	public void setTheme(Theme theme) {
		this.theme = theme;
	}

	/**
	 * UI related, this preference can be used to tell if the chart must be updated directly after editing a widget value or only after the "Compute" button is pressed.
	 * 
	 * @return the directComputing
	 */
	public boolean isDirectComputing() {
		return directComputing;
	}

	// FIXME: MD: not used ? deprecated ?
	// /**
	// *
	// * @param result
	// * @param chartType
	// * @return
	// */
	// public Object createChart(ChartResult result, String chartType) {
	// Object chart = null;
	// ChartCreator chartCreator = this.getChartCreator(result, chartType);
	// if (chartCreator != null) {
	// chart = chartCreator.createChart(result);
	// }
	// return chart;
	// }
	//
	// /**
	// *
	// * @param result
	// * @return
	// */
	// public Object createChart(ChartResult result) {
	// return this.createChart(result, result.getChartType());
	// }
	//
	// /**
	// *
	// * @param chart
	// * @param result
	// */
	// public void updateChart(Object chart, ChartResult result) {
	// ChartCreator chartCreator = this.getChartCreator(result, result.getChartType());
	// if (chartCreator != null) {
	// chartCreator.updateChart(result);
	// }
	// }

	@Override
	public void notify(TXMResult r, String state) {
		// nothing to do
	}

	@Override
	public String getDetails() {
		return this.description;
	}

	@Override
	public String hasAdditionalDetailsForResult(TXMResult result) {
		return null;
	}

	@Override
	public String getAdditionalDetailsForResult(TXMResult result) {
		return null;
	}
	
}
