/**
 *
 */
package org.txm.chartsengine.core;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;

/**
 * Base theme for charts.
 *
 * TODO re-implement Them with TXMColor
 * TODO replace all paletteN with the Palette17
 *
 * @author sjacquot
 * @author mdecorde add color names, replace palette1 with palette17
 *
 */
public class Theme {


	/**
	 * Palette to use for chart with 1 item.
	 */
	protected ArrayList<Color> palette1;

	/**
	 * Palette to use for chart with 5 items.
	 */
	protected ArrayList<Color> palette5;

	/**
	 * Palette to use for chart with 10 items.
	 */
	protected ArrayList<Color> palette10;

	/**
	 * Palette to use for chart with 17 items.
	 */
	protected ArrayList<Color> palette17;

	/**
	 * Color to use when drawing in monochrome mode.
	 */
	protected Color monochromeColor;

	protected HashMap<ArrayList<Color>, List<String>> paletteToNames;

	/**
	 *
	 */
	public Theme() {

		this(ChartsEnginePreferences.getInstance().getString(ChartsEnginePreferences.MONOCHROME_COLOR));
	}
	/**
	 *
	 */
	public Theme(String monochrome) {

		this.setMonochromeColor(monochrome);

		this.palette1 = new ArrayList<Color>(1);
		this.palette5 = new ArrayList<Color>(5);
		this.palette10 = new ArrayList<Color>(10);
		this.palette17 = new ArrayList<Color>(17);
		this.paletteToNames = new HashMap<>();

		// palette 1
		this.palette1.add(Color.decode("#2F7ED8")); // Blue //$NON-NLS-1$
		paletteToNames.put(palette1, Arrays.asList("")); //$NON-NLS-1$


		//FIXME: SJ, 2024-11-21: commented for now, not used anymore
//		// palette 5
//		this.palette5.add(new Color(255, 0, 0));
//		this.palette5.add(new Color(0, 0, 255));
//		this.palette5.add(new Color(0, 139, 0));
//		this.palette5.add(new Color(255, 215, 0));
//		this.palette5.add(new Color(255, 20, 147));
//		paletteToNames.put(palette5, Arrays.asList(
//				ColorMessages.blue,
//				ColorMessages.red_bright,
//				ColorMessages.green_dark,
//				ColorMessages.yellow,
//				ColorMessages.pink_fuchsia));
//
//		// palette 10
//		this.palette10.add(new Color(255, 0, 0));
//		this.palette10.add(new Color(0, 0, 255));
//		this.palette10.add(new Color(0, 139, 0));
//		this.palette10.add(new Color(255, 215, 0));
//		this.palette10.add(new Color(255, 20, 147));
//		this.palette10.add(new Color(178, 34, 34));
//		this.palette10.add(new Color(0, 191, 255));
//		this.palette10.add(new Color(173, 255, 47));
//		this.palette10.add(new Color(255, 185, 15));
//		this.palette10.add(new Color(255, 0, 255));
//		paletteToNames.put(palette10, Arrays.asList(ColorMessages.blue,
//				ColorMessages.red_bright,
//				ColorMessages.green_dark,
//				ColorMessages.yellow,
//				ColorMessages.pink_fuchsia,
//				ColorMessages.red_dark,
//				ColorMessages.blue_light,
//				ColorMessages.green_fluo,
//				ColorMessages.yellow_ocher,
//				ColorMessages.violet_fluo));

		// palette 17
		this.palette17.add(new Color(255, 0, 0)); // Rouge
		this.palette17.add(new Color(0, 0, 255)); // Bleu
		this.palette17.add(new Color(0, 205, 0)); // Vert
		this.palette17.add(new Color(255, 215, 0)); // Jaune
		this.palette17.add(new Color(255, 20, 147)); //Rose
		this.palette17.add(new Color(178, 34, 34)); // Rouge foncé
		this.palette17.add(new Color(0, 0, 128)); // Bleu foncé
		this.palette17.add(new Color(0, 139, 0)); // Vert foncé
		this.palette17.add(new Color(255, 185, 15)); // Jaune ocre
		this.palette17.add(new Color(128, 0, 128)); // Violet
		this.palette17.add(new Color(0, 191, 255)); // Bleu clair
		this.palette17.add(new Color(0, 255, 0)); // Vert clair
		this.palette17.add(new Color(255, 255, 0)); // Jaune fluo
		this.palette17.add(new Color(255, 0, 255)); // Violet fluo
		this.palette17.add(new Color(0, 255, 255)); // Bleu fluo
		this.palette17.add(new Color(173, 255, 47)); // Vert fluo
		this.palette17.add(new Color(84, 255, 159)); // Vert-Bleu fluo
		paletteToNames.put(palette17, Arrays.asList(
				ColorMessages.red,
				ColorMessages.blue,
				ColorMessages.green,
				ColorMessages.yellow,
				ColorMessages.pink,
				ColorMessages.red_dark,
				ColorMessages.blue_dark,
				ColorMessages.green_dark,
				ColorMessages.yellow_ocher,
				ColorMessages.violet,
				ColorMessages.blue_light,
				ColorMessages.green_light,
				ColorMessages.yellow_fluo,
				ColorMessages.voilet_fluo,
				ColorMessages.blue_fluo,
				ColorMessages.green_fluo,
				ColorMessages.green_blue_fluo));

		for (ArrayList<Color> p : paletteToNames.keySet()) {
			if (p.size() != paletteToNames.get(p).size()) {
				System.out.println(NLS.bind(ColorMessages.warningPaletteNamesSizeDoNotMatchP0, paletteToNames.get(p)));
			}
		}
	}

	public List<String> getColorNamesForPalette(int itemsCount) {
		return paletteToNames.get(getColorPaletteFor(itemsCount));
	}

	public List<String> getColorNamesForPalette(ArrayList<Color> palette) {
		List<String> names =  paletteToNames.get(palette);
		if (names == null) { // not a TXM palette !!!
			names = new ArrayList<>();
			for (Color c : palette) {
				names.add(c.toString());
			}
		}
		return names;
	}

	/**
	 * Gets the palette matching the number of specified items.
	 *
	 * @param itemsCount
	 * @return the created palette
	 */
	public ArrayList<Color> getColorPaletteFor(int itemsCount) {

		ArrayList<Color> palette = new ArrayList<Color>();

		if (itemsCount <= 1) {
			palette.addAll(this.palette1);
		}
		//FIXME: SJ, 2024-11-21: commented for now, not used anymore
//		else if (itemsCount <= 5) {
//			palette.addAll(this.palette5);
//		}
//		else if (itemsCount <= 10) {
//			palette.addAll(this.palette10);
//		}
		else {
			palette.addAll(this.palette17);
		}

		// fill to match items count with brighter colors based on same palette
		if (itemsCount > 17) {
			for (int j = 0, k = 17; k < itemsCount; k++, j++) {
				if (j == 17) {
					j = 0;
				}
				palette.add(this.palette17.get(j).brighter());
			}
		}
		return palette;
	}

	/**
	 * Converts the specified palette to hexadecimal strings.
	 *
	 * @param palette
	 * @return the created palette
	 */
	public String[] getPaletteAsHex(ArrayList<Color> palette) {
		String[] paletteAsString = new String[palette.size()];
		for (int i = 0; i < palette.size(); i++) {
			paletteAsString[i] = String.format("#%06x", palette.get(i).getRGB() & 0x00FFFFFF); //$NON-NLS-1$
		}
		return paletteAsString;
	}

	/**
	 * @return the monochromeColor
	 */
	public Color getMonochromeColor() {
		return monochromeColor;
	}

	/**
	 * Gets a monochrome palette according to the specified items count and using the monochrome color defined in the theme.
	 *
	 * @param itemsCount
	 * @return the created palette
	 */
	public ArrayList<Color> getMonochromePaletteFor(int itemsCount) {
		return this.getMonochromePaletteFor(itemsCount, this.monochromeColor);
	}

	/**
	 * Gets a monochrome palette according to the specified items count and using the specified color.
	 *
	 * @param itemsCount
	 * @param color
	 * @return
	 */
	public ArrayList<Color> getMonochromePaletteFor(int itemsCount, Color color) {
		ArrayList<Color> palette = new ArrayList<Color>(itemsCount);
		for (int i = 0; i < itemsCount; i++) {
			palette.add(color);
		}
		return palette;
	}

	/**
	 * Gets a B and W palette according to the specified items count.
	 *
	 * @param itemsCount
	 * @return the created palette
	 */
	public ArrayList<Color> getBlackPaletteFor(int itemsCount) {
		return this.getMonochromePaletteFor(itemsCount, Color.BLACK);
	}


	/**
	 * Gets a gray scale palette according to the specified items count.
	 *
	 * @param itemsCount
	 * @return the created palette
	 */
	public ArrayList<Color> getGrayScalePaletteFor(int itemsCount) {
		ArrayList<Color> palette = new ArrayList<Color>(itemsCount);

		// special case of 1 items count palette, better to use gray rather than black
		if (itemsCount == 1) {
			palette.add(new Color(128, 128, 128));
		}
		else {
			for (int i = 0; i < itemsCount; i++) {
				int c;
				if (i == 0) {
					c = 1;
				}
				else {
					c = (int) ((double) 255 / itemsCount * i);
				}
				palette.add(new Color(c, c, c));
			}
		}

		return palette;
	}

	/**
	 * Gets a color, gray scale or monochrome palette according to the specified rendering mode and items count.
	 *
	 * @param renderingColorMode
	 * @param itemsCount
	 * @return the created palette
	 */
	public ArrayList<Color> getPaletteFor(int renderingColorMode, int itemsCount) {

		ArrayList<Color> palette = new ArrayList<Color>(itemsCount);

		if (renderingColorMode == ChartsEngine.RENDERING_COLORS_MODE) {
			palette = this.getColorPaletteFor(itemsCount);
		}
		// Items rendering grayscale mode
		else if (renderingColorMode == ChartsEngine.RENDERING_GRAYSCALE_MODE) {
			palette = this.getGrayScalePaletteFor(itemsCount);
		}
		// Items rendering B&W
		else if (renderingColorMode == ChartsEngine.RENDERING_BLACK_AND_WHITE_MODE) {
			palette = this.getBlackPaletteFor(itemsCount);
		}
		// Items rendering monochrome mode
		else if (renderingColorMode == ChartsEngine.RENDERING_MONOCHROME_MODE) {
			palette = this.getMonochromePaletteFor(itemsCount);
		}

		return palette;
	}

	/**
	 * Gets a color, gray scale or monochrome palette as hexadecimal strings according to the specified rendering mode and items count.
	 *
	 * @param itemsCount
	 * @return the created palette
	 */
	public String[] getPaletteAsHexFor(int renderingColorMode, int itemsCount) {
		return this.getPaletteAsHex(this.getPaletteFor(renderingColorMode, itemsCount));
	}



	/**
	 * Sets the monochrome color of the theme using a string as "r,g,b".
	 *
	 * @param color the monochromeColor to set
	 */
	public void setMonochromeColor(String color) {
		String[] rgb = color.split(","); //$NON-NLS-1$
		this.monochromeColor = new Color(Integer.valueOf(rgb[0]), Integer.valueOf(rgb[1]), Integer.valueOf(rgb[2]));
	}

	public static Color stringToColor(String color) {
		String[] rgb = color.split(","); //$NON-NLS-1$
		return new Color(Integer.valueOf(rgb[0]), Integer.valueOf(rgb[1]), Integer.valueOf(rgb[2]));
	}

	/**
	 * @return the palette1
	 */
	public ArrayList<Color> getPalette1() {
		return palette1;
	}



	/**
	 * @param palette1 the palette1 to set
	 */
	public void setPalette1(ArrayList<Color> palette1) {
		this.palette1 = palette1;
	}



	/**
	 * @return the palette5
	 */
	public ArrayList<Color> getPalette5() {
		return palette5;
	}



	/**
	 * @param palette5 the palette5 to set
	 */
	public void setPalette5(ArrayList<Color> palette5) {
		this.palette5 = palette5;
	}



	/**
	 * @return the palette10
	 */
	public ArrayList<Color> getPalette10() {
		return palette10;
	}



	/**
	 * @param palette10 the palette10 to set
	 */
	public void setPalette10(ArrayList<Color> palette10) {
		this.palette10 = palette10;
	}



	/**
	 * @return the palette17
	 */
	public ArrayList<Color> getPalette17() {
		return palette17;
	}



	/**
	 * @param newPalette17 the new palette17 to set
	 */
	public void setPalette17(ArrayList<Color> newPalette17) {
		this.palette17 = newPalette17;
	}

}
