package org.txm.chartsengine.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Charts engine core messages.
 * 
 * @author sjacquot
 *
 */
public class ChartsEngineCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.chartsengine.core.messages.messages"; //$NON-NLS-1$


	public static String error_cantCreateTmpChartFileP0;

	public static String error_cantWriteInFileP0;

	public static String EMPTY;

	public static String labels;

	public static String symbols;

	public static String labelsAndSymbols;

	public static String match;


	public static String rulesP0;
	public static String andIfP0;
	public static Object forThe;
	public static Object appliedTo;


	public static String colorToSelect;
	public static String fontToSelect;
	public static String fourColorPaletteP0;
	public static String gradientOfP0;
	public static String inverseProportionalToP0;
	public static String rainbow;
	public static String proportionalToP0;
	public static String regexP0;
	public static String replaceWithP0;


	public static String color;
	public static String dontDisplay;
	public static String font;
	public static String no;
	public static String size;
	public static String symbol;
	public static String label;
	public static String transparency;
	public static String yes;


	static {
		Utf8NLS.initializeMessages(BUNDLE_NAME, ChartsEngineCoreMessages.class);
	}

}
