/**
 * 
 */
package org.txm.chartsengine.core.results;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.eclipse.osgi.util.NLS;
import org.txm.chartsengine.core.ChartCreator;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.ChartsEnginesManager;
import org.txm.chartsengine.core.messages.ChartsEngineCoreMessages;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * Chart result.
 * 
 * @author sjacquot
 *
 */
public abstract class ChartResult extends TXMResult {


	/**
	 * Linked charts engine.
	 */
	protected ChartsEngine chartsEngine;

	/**
	 * The chart object.
	 * TODO: MD: this is problematic, the chart object should allow us to edit the chart. In the case of the R charts, the File has not the same value as the JFC Chart (which can be edited)
	 * TODO: replace this with at least a ChartResult parameter such as "O extends Object" but a abstract "C extends org.txm.Chart" would be better even if is only an empty class. avoid compilation
	 * error and upgrade the readability of the code and avoid unnecessary casts
	 */
	protected Object chart;


	/**
	 * Chart creator used when the chart has been created.
	 */
	protected ChartCreator<ChartsEngine, ChartResult> currentChartCreator;
	
	/**
	 * The chart dirty state.
	 */
	protected boolean chartDirty;

	/**
	 * If true, the view (zoom, pan, etc.) will be reset when the chart will be updated.
	 */
	protected boolean needsToResetView;

	/**
	 * If true, the items selection will cleared when the chart will be updated.
	 */
	protected boolean needsToClearItemsSelection;

	/**
	 * If true, the items selection will cleared when the chart will be updated.
	 */
	protected boolean needsToSquareOff;


	/**
	 * To show/hide domain grid lines.
	 */
	protected boolean domainGridLinesVisible;


	/**
	 * The chart type;
	 * TODO MD: finalize the chart type parameter. The chart type is directly linked to the ChartCreator classes and should not be a simple String. this will allow us to manage this pref correctly
	 * in the autosave and load methods
	 */
	@Parameter(key = ChartsEnginePreferences.CHART_TYPE, type = Parameter.RENDERING)
	private String chartType;


	/**
	 * To show/hide title.
	 */
	@Parameter(key = ChartsEnginePreferences.SHOW_TITLE, type = Parameter.RENDERING)
	protected boolean titleVisible;

	/**
	 * To show/hide legend.
	 */
	@Parameter(key = ChartsEnginePreferences.SHOW_LEGEND, type = Parameter.RENDERING)
	protected boolean legendVisible;

	/**
	 * To show/hide grid.
	 */
	@Parameter(key = ChartsEnginePreferences.SHOW_GRID, type = Parameter.RENDERING)
	protected boolean gridVisible;

	/**
	 * Rendering colors mode
	 */
	@Parameter(key = ChartsEnginePreferences.RENDERING_COLORS_MODE, type = Parameter.RENDERING)
	protected int renderingColorsMode;

	/**
	 * Font.
	 */
	@Parameter(key = ChartsEnginePreferences.FONT, type = Parameter.RENDERING)
	protected String font;


	/**
	 * Multiple lines styles/strokes.
	 */
	@Parameter(key = ChartsEnginePreferences.MULTIPLE_LINE_STROKES, type = Parameter.RENDERING)
	protected boolean multipleLineStrokes;



	/**
	 * Creates a new ChartResult child of the specified parent.
	 * 
	 * @param parent
	 */
	public ChartResult(TXMResult parent) {
		this(null, parent);
	}


	/**
	 * Creates a new ChartResult with no parent.
	 * If a local node exist with the parent_uuid, the parent will be retrieved and this result will be added to it.
	 * 
	 * @param parametersNodePath
	 */
	public ChartResult(String parametersNodePath) {
		this(parametersNodePath, null);
	}

	/**
	 * Creates a new ChartResult child of the specified parent.
	 * 
	 * @param parametersNodePath
	 * @param parent
	 */
	public ChartResult(String parametersNodePath, TXMResult parent) {
		super(parametersNodePath, parent);
		this.chartsEngine = ChartsEngine.getCurrent();
		this.chartDirty = true;
		this.needsToResetView = false;
		this.needsToClearItemsSelection = false;
		this.needsToSquareOff = false;
		this.domainGridLinesVisible = true;
	}

	@Override
	public boolean autoLoadParametersFromAnnotations() throws Exception {

		return (super.autoLoadParametersFromAnnotations()
				&& super.autoLoadParametersFromAnnotations(Parameter.RENDERING));
	}


	@Override
	public void setDirty() {

		super.setDirty();
		// this.chart = null; // FIXME: SJ: became useless?
		this.setChartDirty();
	}

	/**
	 * Marks the chart as needing a new rendering.
	 */
	public void setChartDirty() {

		this.chartDirty = true;
	}

	/**
	 * This method is dedicated to be implemented by the inherited result classes to run the computing steps.
	 * Subclasses can monitor progress using the dedicated methods.
	 * At the start of this method call, the internal monitor have 100 ticks of remaining work to consume.
	 * 
	 * 
	 * warning: at this step no chart rendering is done: only the data is computed
	 * 
	 * @return should return true if the result was computed, otherwise false
	 * 
	 * @throws Exception
	 */
	protected abstract boolean __compute(TXMProgressMonitor monitor) throws Exception;

	@Override
	protected final boolean _compute(TXMProgressMonitor monitor) {

		boolean computingState = true;

		try {
			Log.finest("*** ChartResult._compute(): " + this.getClass().getSimpleName() + ": starting computing process..."); //$NON-NLS-1$ //$NON-NLS-2$

			boolean computingParametersChanged = super.haveParametersChanged();
			boolean renderingParametersChanged = this.hasRenderingParameterChanged();

			if (this.isDirty()) {

				// Debug
				Log.finest("+++ ChartResult._compute(): " + this.getClass().getSimpleName() + ": computing result of type " + this.getClass() + "..."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

				computingState = this.__compute(monitor.createNewMonitor(90)) && this.computeChart(monitor.createNewMonitor(10));
			}
			else {
				if (computingParametersChanged) { // no parameter changed but result is dirty

					// Debug
					Log.finest("+++ ChartResult._compute(): " + this.getClass().getSimpleName() + ": computing result of type " + this.getClass() + "..."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

					computingState = this.__compute(monitor.createNewMonitor(90)) && this.computeChart(monitor.createNewMonitor(10));
				}
				else if (renderingParametersChanged) {

					// Debug
					Log.finest("--- ChartResult._compute(): " + this.getClass().getSimpleName() + ": result parameters have not changed since last computing, computing skipped."); //$NON-NLS-1$ //$NON-NLS-2$

					computingState = this.computeChart(monitor.createNewMonitor(100));
				}
				else {
					// FIXME: Debug
					Log.finest("--- ChartResult.renderChart(): chart rendering parameters have not changed since last rendering, rendering skipped."); //$NON-NLS-1$
					// Log.warning("ChartResult _compute has been called without parameter changed and not dirty?");
				}
			}

			// if (changed // dirty because parameters changed
			// || (!changed && isDirty())) { // dirty because the result has been marked dirty
			//
			// // Debug
			// Log.finest("+++ ChartResult._compute(): " + this.getClass().getSimpleName() + ": computing result of type " + this.getClass() + "...");
			//
			// computingState = this.__compute(monitor.createNewMonitor(90));
			// }
			// else {
			// // Debug
			// Log.finest("--- ChartResult._compute(): " + this.getClass().getSimpleName() + ": result parameters have not changed since last computing, computing skipped.");
			// }
			//
			// // rendering the chart
			// if (computingState && (this.hasRenderingParameterChanged() || changed || this.isDirty())) {
			// computingState = this.renderChart(monitor.createNewMonitor(10));
			// }
			// else {
			// // FIXME: Debug
			// Log.finest("--- ChartResult.renderChart(): chart rendering parameters have not changed since last rendering, rendering skipped."); //$NON-NLS-1$
			// }
			// }
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.severe("ChartResult._compute(): Exception occurred during computing or rendering.");
			computingState = false;
		}

		return computingState;
	}

	/**
	 * 1 - Creates the chart using the right chart creator.
	 * 2 - The chart is created if needed then it is updated.
	 * 
	 * note: no dirty state test is done here
	 * 
	 * @return true if the chart has been computed
	 * @throws Exception
	 */
	//FIXME: SJ: this method needs a full review and clean
	private boolean computeChart(TXMProgressMonitor monitor) throws Exception {

		monitor.setTask(ChartsEngineCoreMessages.bind("Computing chart for result {0} and chart type {1}...", this.getClass(), this.getChartType()));

		Log.finest("*** ChartResult.computeChart(): computing chart for result " + this.getClass() + " and chart type " + this.getChartType() + "..."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		ChartCreator<ChartsEngine, ChartResult> newChartCreator = this.findSuitableChartCreator(); // get a chart creator for the current chart engine and current chart_type asked
		if (newChartCreator == null) { // try finding another one
			Log.finest("ChartResult.computeChart(): no suitable chart creator has been found in charts engine: " + this.chartsEngine + "."); //$NON-NLS-1$ //$NON-NLS-2$
			for (ChartsEngine alternativeEngine : ChartsEnginesManager.getChartsEngines()) {
				if (alternativeEngine == this.chartsEngine) {
					continue; // not an alternative
				}
				newChartCreator = alternativeEngine.getChartCreator(this);
				if (newChartCreator != null) {
					Log.finest("ChartResult.computeChart(): another suitable chart creator has been found in charts engine: " + alternativeEngine + "."); //$NON-NLS-1$ //$NON-NLS-2$

					break;
				}
			}
		}

		if (newChartCreator == null) { // no luck ?
			Log.severe("ChartResult.computeChart(): can not find any new suitable chart creator for result: " + this.getClass() + "."); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}
		if (currentChartCreator == null // no current chart, we take the new one
				|| !currentChartCreator.equals(newChartCreator)) { // the current chart creator is not  the same, change and reset chart !
			Log.finest("ChartResult.computeChart(): set the new chart creator: " + newChartCreator + "."); //$NON-NLS-1$ //$NON-NLS-2$
			this.currentChartCreator = newChartCreator; // a new chart creator !!
			this.chartsEngine = currentChartCreator.getChartsEngine();
			setChart(null); // the chart must be redrawn from scratch since the chart creator changed
		}
		if (currentChartCreator == null) { // should not happen
			Log.severe("ChartResult.computeChart(): can not find any suitable chart creator for result: " + this.getClass() + "."); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}


		// FIXME: fix #1
		// FIXME: SJ: without this, this.hasParameterChanged(ChartsEnginePreferences.CHART_TYPE) returns true for results that added their computing parameters in the parameter history.
		// Since the stack is shared by computing and rendering parameters, this.hasParameterChanged(ChartsEnginePreferences.CHART_TYPE) returns true because the parameter doesn't exist in the
		// last stack entry filled only with the computing parameter
		// may need to fix this in another way:
		// Solution 1: store two stacks, one for computing parameters and another for rendering parameters
		// Solution 2: stop to dissociate rendering parameters and computing parameters. Maybe the best way but need to check if this dissociation is very useless
		// clear the last computing parameters
		// if(this.parametersHistory.size() > 2
		//
		// //|| this.getLastParametersFromHistory().isEmpty()
		// ) {
		// this.clearLastComputingParameters();
		// }

		// Creating, if needed.
		// The change of the chart type parameter can occur if:
		// - the chart has never been created
		// - the chart type has been changed, e.g. to dynamically change the type of chart or the current charts engine
		if (this.chart == null
		// MD: the following tests are no more necessary since chartResult dirty state has been already resolved
		// || this.getLastParametersFromHistory() == null ||
		// (this.getLastParametersFromHistory().get(ChartsEnginePreferences.CHART_TYPE) != null &&
		// this.hasParameterChanged(ChartsEnginePreferences.CHART_TYPE))
		// )

		) {
			Log.finest("+++ ChartResult.computeChart(): creating chart (charts engine = " + this.chartsEngine + ", output format = " + this.chartsEngine.getOutputFormat() + ")..."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

			this.chart = currentChartCreator.createChart(this);
		}

		// Updating
		Log.finest("ChartResult.computeChart(): updating chart..."); //$NON-NLS-1$

		// the update must be done here (BEFORE the call of this.updateLastRenderingParameters()) rather than in the SWTChartComponentsProvider => the problem is that for File based Engine, the
		// file may be created twice, need to check this
		// also before the call of this.updateLastParameters() to be able to check if a computing parameter has changed in the chart creators
		// FIXME: SJ: this call doesn't work well with File based charts engines, the file is created one time in createChartFile() and a second time in updateChart()
		this.currentChartCreator.updateChart(this);

		// FIXME: needed by the above fix #1
		// reupdate the last computing parameters
		// this.updateLastParameters();

		// updates last rendering parameters
		// ArrayList<Integer> parametersTypes = new ArrayList<>();
		// parametersTypes.add(Parameter.RENDERING);
		// HashMap<String, Object> lastParameters = this.getLastParametersFromHistory();
		// if (lastParameters == null) {
		// lastParameters = new HashMap<>();
		// }
		// this.updateLastParameters(parametersTypes, lastParameters);


		//			this.needsToClearItemsSelection = false;
		//			this.needsToResetView = false;

		this.chartDirty = false;

		// Debug
		Log.finest(NLS.bind("ChartResult.computeChart(): chart computing done ({0}).", this.chart)); //$NON-NLS-1$

		return true;
	}

	
	/**
	 * Updates the chart using its current chart creator.
	 */
	public void updateChart() {
		this.currentChartCreator.updateChart(this);
	}
	

	/**
	 * Stores the last parameters used for computing and rendering.
	 * 
	 * @throws Exception
	 */
	@Override
	protected void updateLastParameters() throws Exception {

		// FIXME: SJ: linked to the bug of computing dirty state and rendering dirty state.
		// the rendering parameters should be stored here, but if they are stored there is an unidentified bug linked to the dirty state and the _compute() is skipped in TXMREsult.compute()
		// need to investigate further
		ArrayList<Integer> parametersTypes = new ArrayList<>();
		parametersTypes.add(Parameter.COMPUTING);
		parametersTypes.add(Parameter.RENDERING); // when adding this, it breaks the rendering of the Progression due of the call of this method too early in the process, also breaks the AHC
		// then hasRenderingParameterChanged() always returns false
		this.updateLastParameters(parametersTypes);
	}



	@Override
	public String dumpParameters() {

		return super.dumpParameters() + "\n" + this.dumpParameters(Parameter.RENDERING); //$NON-NLS-1$
	}

	/**
	 * Clears the parameters used for last rendering.
	 * Dedicated to force a chart recreation, e.g. for cloning the chart result or for dynamically changing the chart type .
	 * 
	 */
	public void clearLastRenderingParameters() {

		this.clearLastParameters(Parameter.RENDERING);
	}

	/**
	 * Checks if at least one rendering parameter value has changed since last computing.
	 * 
	 * @return
	 * @throws Exception
	 */
	protected boolean hasRenderingParameterChanged() throws Exception {

		HashMap<String, Object> p = this.getLastParametersFromHistory();
		if (p == null) return false;
		return this.hasParametersChanged(p, Parameter.RENDERING);
	}

	@Override
	public boolean haveParametersChanged() throws Exception {

		return super.haveParametersChanged() || this.hasRenderingParameterChanged();
	}

	// @Override
	// public void updateDirtyFromHistory() throws Exception {
	//
	// super.updateDirtyFromHistory();
	//
	// if (!this.isDirty() && this.hasRenderingParameterChanged()) {
	// this.setDirty(false); // FIXME: SJ: temporary but breaks the dirty computing state and rendering computing state management
	// // problem here is that if the object is not dirty, TXMResult.compute() doesn't call ChartResult._compute() so the rendering is not done
	// }
	// }


	/**
	 * Sets the chart dirty state to true if a rendering parameter has changed since last computing.
	 * Also sets the chart dirty to true if the computing dirty state is equals to true.
	 * 
	 * @throws Exception
	 */
	public void updateChartDirtyFromHistory() throws Exception {

		if (this.isDirty() || this.hasRenderingParameterChanged()) {
			this.chartDirty = true;
		}
	}

	@Override
	public ChartResult clone() {

		ChartResult clone = null;
		clone = (ChartResult) super.clone();
		clone.chart = null;
		// FIXME: SJ: would be nice to clone the chart if possible instead of clearing the last rendering parameters?
		// clone.chart = this.chart.clone();
		clone.clearLastRenderingParameters(); // to force recreation of the chart at next computing // FIXME: SJ: need to see if it's necessary, the chartDirty state may be sufficient
		clone.chartDirty = true;
		return clone;
	}

	/**
	 * Gets the chart object.
	 * 
	 * @return the chart object
	 */
	public Object getChart() {
		return this.chart;
	}


	/**
	 * Gets the chart type.
	 * 
	 * @return the chartType
	 */
	public String getChartType() {
		return this.chartType;
	}

	/**
	 * Gets the chart creator used to compute this result.
	 * 
	 * @return the chart creator or null if the result has never been computed
	 */
	public ChartCreator<ChartsEngine, ChartResult> getChartCreator() {
		return this.currentChartCreator;
	}

	/**
	 * Gets a chart creator for this chart result according to its current type and current charts engine.
	 * 
	 * @return a suitable chart creator if exists, otherwise null
	 */
	public ChartCreator<ChartsEngine, ChartResult> findSuitableChartCreator() {
		return this.chartsEngine.getChartCreator(this);
	}

	/**
	 * Sets the chart type.
	 * The chart is especially used to find a suitable chart creator in the current charts engine. 
	 * 
	 * @param chartType the chartType to set
	 */
	public void setChartType(String chartType) {

		setChart(null); // the chart must be redrawn from scratch
		this.chartType = chartType;
		this.setDirty();
		this.chartDirty = true;


		Log.finest("ChartResult.setChartType(): chart type set to " + chartType + "."); //$NON-NLS-1$

		
		//FIXME: SJ: became useless?
		// this.getLastParametersFromHistory().put(ChartsEnginePreferences.CHART_TYPE, "[Default]");

		// update the chart type of last cycle parameters so hasRenderingParameterChanged() returns true
		// HashMap<String, Object> last = this.getLastParametersFromHistory();
		// if (last != null) {
		// this.getLastParametersFromHistory().put(ChartsEnginePreferences.CHART_TYPE, "__undef___"); //$NON-NLS-1$
		// }

		// clear all last rendering parameters to force the chart full recreation
		this.clearLastRenderingParameters();

	}

	/**
	 * Sets the chart type as default.
	 */
	public void setDefaultChartType() {
		this.setChartType(ChartsEnginePreferences.DEFAULT_CHART_TYPE);
	}

	/**
	 * Checks if the current chart type is the default one.
	 * 
	 * @return
	 */
	public boolean isDefaultChartType() {
		return ChartsEnginePreferences.DEFAULT_CHART_TYPE.equals(this.getChartType());
	}


	/**
	 * @return the chartDirty
	 */
	public boolean isChartDirty() {
		return chartDirty;
	}

	/**
	 * @return the needsToResetView
	 */
	public boolean needsToResetView() {
		return needsToResetView;
	}


	/**
	 * @param needsToResetView the needsToResetView to set
	 */
	public void setNeedsToResetView(boolean needsToResetView) {
		this.needsToResetView = needsToResetView;
	}


	/**
	 * @return the needsClearItemsSelection
	 */
	public boolean needsToClearItemsSelection() {
		return needsToClearItemsSelection;
	}


	/**
	 * @param needsToClearItemsSelection the needsClearItemsSelection to set
	 */
	public void setNeedsToClearItemsSelection(boolean needsToClearItemsSelection) {
		this.needsToClearItemsSelection = needsToClearItemsSelection;
	}

	/**
	 * Gets the square off constraint state.
	 * 
	 * @return the square off constraint state
	 */
	public boolean needsToSquareOff() {
		return needsToSquareOff;
	}


	/**
	 * Sets the square off constraint state.
	 * 
	 * @param needsToSquareOff the square off constraint state to set
	 */
	public void setNeedsToSquareOff(boolean needsToSquareOff) {
		this.needsToSquareOff = needsToSquareOff;
	}


	/**
	 * Gets the title visibility state.
	 * 
	 * @return the titleVisible
	 */
	public boolean isTitleVisible() {
		return titleVisible;
	}

	/**
	 * Sets the title visibility state.
	 * 
	 * @param titleVisible the titleVisible to set
	 */
	public void setTitleVisible(boolean titleVisible) {
		this.titleVisible = titleVisible;
	}

	/**
	 * Gets the legend visibility state.
	 * 
	 * @return the legendVisible
	 */
	public boolean isLegendVisible() {
		return legendVisible;
	}

	/**
	 * Sets the legend visibility state.
	 * 
	 * @param legendVisible the legendVisible to set
	 */
	public void setLegendVisible(boolean legendVisible) {
		this.legendVisible = legendVisible;
	}

	/**
	 * Gets the grid visibility state.
	 * 
	 * @return the gridVisible
	 */
	public boolean isGridVisible() {
		return gridVisible;
	}

	/**
	 * Gets the domain grid lines visibility state.
	 * 
	 * @return the domainGridLinesVisible
	 */
	public boolean isDomainGridLinesVisible() {
		return domainGridLinesVisible;
	}


	/**
	 * Sets the visiiblity visibility state.
	 * 
	 * @param gridVisible the gridVisible to set
	 */
	public void setGridVisible(boolean gridVisible) {
		this.gridVisible = gridVisible;
	}


	/**
	 * Gets the rendering color mode as integer.
	 * 
	 * @return the renderingColorsMode
	 */
	public int getRenderingColorsMode() {
		return renderingColorsMode;
	}


	/**
	 * Gets the rendering color mode.
	 * 
	 * @param renderingColorsMode the renderingColorsMode to set
	 */
	public void setRenderingColorsMode(int renderingColorsMode) {
		this.renderingColorsMode = renderingColorsMode;
	}


	/**
	 * Gets the font.
	 * 
	 * @return the font
	 */
	public String getFont() {
		return font;
	}


	/**
	 * Sets the font using encoded String in JFace StringConverter format (e.g.: "1|Lucida Sans Unicode|12.0|0|WINDOWS|1|-16|0|0|0|400|0|0|0|0|3|2|1|34|Lucida Sans Unicode;").
	 * 
	 * @param font the font to set
	 */
	public void setFont(String font) {
		this.font = font;
	}


	/**
	 * Sets the font.
	 * Internally build an encoded String in JFace StringConverter format (e.g.: "1|Lucida Sans Unicode|12.0|0|WINDOWS|1|-16|0|0|0|400|0|0|0|0|3|2|1|34|Lucida Sans Unicode;") from the specified
	 * parameters.
	 * Also, is convertUserFriendlyName is set to true, then the font name will be converted to it's real logical font name.
	 * 
	 * @param fontName
	 * @param size
	 * @param style
	 */
	public void setFont(String fontName, String size, String style, boolean convertUserFriendlyName) {

		int styleAsInt = Arrays.asList(ChartsEngine.FONT_STYLES).indexOf(style);

		// force regular if style is not found
		if (styleAsInt < 0) {
			styleAsInt = 0;
		}

		// convert user-friendly name to real logical font name
		if (convertUserFriendlyName) {
			int fontNameIndex = Arrays.asList(ChartsEngine.FONTS_USER_FRIENDLY_NAMES).indexOf(fontName);
			// force "SansSerif" if name is not found
			if (fontNameIndex < 0) {
				fontNameIndex = 0;
			}
			fontName = ChartsEngine.FONTS_LOGICAL_NAMES[fontNameIndex];
		}

		this.setFont("1|" + fontName + "|" + size + "|" + String.valueOf(styleAsInt)); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}


	/**
	 * 
	 * @param fontName
	 * @param size
	 * @param style
	 */
	public void setFont(String fontName, String size, String style) {
		this.setFont(fontName, size, style, true);
	}


	/**
	 * Gets the multiple line strokes state.
	 * 
	 * @return the multipleLineStrokes
	 */
	public boolean isMultipleLineStrokes() {
		return multipleLineStrokes;
	}



	/**
	 * Sets the multiple line strokes state.
	 * 
	 * @param multipleStrokes the multiple line strokes state to set
	 */
	public void setMultipleLineStrokes(boolean multipleStrokes) {
		this.multipleLineStrokes = multipleStrokes;
	}


	/**
	 * Gets the current charts engine used for the rendering.
	 * 
	 * @return the chartsEngine
	 */
	public ChartsEngine getChartsEngine() {
		return chartsEngine;
	}


	/**
	 * Sets the charts engine to use for the rendering.
	 * 
	 * @param chartsEngine the chartsEngine to set
	 */
	public void setChartsEngine(ChartsEngine chartsEngine) {
		this.chartsEngine = chartsEngine;
	}


	/**
	 * Sets the chart object.
	 * The chart raw data object type depends of the charts engine implementation.
	 * It can be some AWT or SWT component, a file, etc.
	 * 
	 * @param chart the chart to set
	 */
	public void setChart(Object chart) {
		this.chart = chart;
	}



}
