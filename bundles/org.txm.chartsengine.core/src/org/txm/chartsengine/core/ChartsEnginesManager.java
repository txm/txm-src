package org.txm.chartsengine.core;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeSet;

import org.eclipse.core.runtime.IProgressMonitor;
import org.txm.Toolbox;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.core.engines.EngineType;
import org.txm.core.engines.EnginesManager;

/**
 * Charts engines manager.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ChartsEnginesManager extends EnginesManager<ChartsEngine> {

	private static final long serialVersionUID = 2369327408978181063L;

	/**
	 * Installed charts engines.
	 * TODO remove this static member and use the ChartsEnginesManager
	 */
	protected static ArrayList<ChartsEngine> chartsEngines;

	@Override
	public boolean fetchEngines() {
		return this.fetchEngines(ChartsEngine.EXTENSION_POINT_ID);
	}

	@Override
	public boolean startEngines(IProgressMonitor monitor) {

		// FIXME: temporary solution to not break the charts engines but later need to stop to use
		// ChartsEngine.chartsEngines, ChartsEngine.currentChartsEngineIndex, getCurrent() etc.
		// and directly manage that in the Manager
		chartsEngines = new ArrayList<>();
		// ChartsEngine.currentChartsEngineIndex = 0;
		//

		for (ChartsEngine engine : this.values()) {
			chartsEngines.add(engine);
		}

		// add the engines sorted by order defined in preferences
		this.sortEngines();


		// String[] orderedEnginesNames = ((ChartsEnginePreferences) ChartsEnginePreferences.getInstance()).getEnginesOrder();
		// ChartsEngine[] engines = new ChartsEngine[this.size()];
		//
		// for (ChartsEngine engine : this.values()) {
		// //ChartsEngine.chartsEngines.add(engine);
		// engines[ArrayUtils.indexOf(orderedEnginesNames, engine.getName())] = engine;
		//
		// }
		// for (int i = 0; i < engines.length; i++) {
		// ChartsEngine.chartsEngines.add(engines[i]);
		// }


		// ChartsEngine.setCurrrentChartsEngine(this.getEngine(ChartsEnginePreferences.getInstance().getString(ChartsEnginePreferences.CURRENT_NAME)));

		this.setCurrentEngine(ChartsEnginePreferences.getInstance().getString(ChartsEnginePreferences.CURRENT_NAME));



		// TXMPreferences.alternativeNodesQualifiers.add(ChartsEnginePreferences.getInstance().getPreferencesNodeQualifier());


		return super.startEngines(monitor);
	}


	/**
	 * Sorts the engines according to the engines order preference.
	 */
	public void sortEngines() {

		// FIXME: SJ: need to manage ordering
		// String[] orderedEnginesNames = ((ChartsEnginePreferences) ChartsEnginePreferences.getInstance()).getEnginesOrder();

		// existing preference
		// if (orderedEnginesNames != null) {
		// ChartsEngine[] engines = new ChartsEngine[this.size()];
		// ChartsEngine.chartsEngines.clear();
		//
		// for (ChartsEngine engine : this.values()) {
		// // ChartsEngine.chartsEngines.add(engine);
		// int index = ArrayUtils.indexOf(orderedEnginesNames, engine.getName());
		// if (index >= 0) {
		// engines[index] = engine;
		// }
		// else {
		// Log.warning(NLS.bind("Chart engine {0} not found in engines ordered list: {1}.", engine.getName(), Arrays.toString(orderedEnginesNames)));
		// }
		// }
		// for (int i = 0; i < engines.length; i++) {
		// ChartsEngine.chartsEngines.add(engines[i]);
		// }
		// }
		// // non existing preference
		// else {
		ChartsEnginePreferences.getInstance().put(ChartsEnginePreferences.CHARTS_ENGINES_ORDER, this.createEnginesOrder());
		// }
	}


	@Override
	public ChartsEngine getCurrentEngine() {

		// set the currentEngine if not already done
		if (currentEngine == null) {
			if (getEnginesOrder() != null && getEnginesOrder().length > 0) {
				currentEngine = this.getEngines().get(getEnginesOrder()[0]);
			}
			else {
				for (ChartsEngine e : this.getEngines().values()) {
					currentEngine = e;
					break;
				}
			}
		}

		return this.currentEngine;
		// FIXME: SJ: test to retrieve the first engine of the map
		// return ChartsEngine.chartsEngines.get(0);
	}

	@Override
	public void setCurrentEngine(ChartsEngine engine) {
		super.setCurrentEngine(engine);

		// this.sortEngines();
		// ChartsEngine.chartsEngines.

		// String[] enginesNames = this.getEnginesOrder();
	}

	/**
	 * Gets an installed charts engine that supports the specified output format.
	 * Looking for display formats and file formats support.
	 * 
	 * @param outputFormat
	 * @return the first suitable ChartsEngine if exists otherwise null
	 */
	public ChartsEngine getChartsEngine(String outputFormat) {

		ChartsEngine chartsEngine = null;
		for (int i = 0; i < chartsEngines.size(); i++) {

			ChartsEngine tmpChartsEngine = chartsEngines.get(i);

			// checking display formats
			ArrayList<String> displayFormats = tmpChartsEngine.getSupportedOutputDisplayFormats();
			if (displayFormats != null && displayFormats.contains(outputFormat)) {
				chartsEngine = tmpChartsEngine;
				break;
			}

			// checking file formats
			ArrayList<String> fileFormats = tmpChartsEngine.getSupportedOutputFileFormats();
			if (fileFormats != null && fileFormats.contains(outputFormat)) {
				chartsEngine = tmpChartsEngine;
				break;
			}
		}
		return chartsEngine;
	}

	/**
	 * Creates the charts engine order string with | separator from the specified engines list.
	 * 
	 * @return
	 */
	public String createEnginesOrder() {
		return ChartsEnginePreferences.createEnginesOrder(this.getEnginesOrder());
	}

	/**
	 * 
	 * @return
	 */
	public String[] getEnginesOrder() {
		String[] engineNames = new String[this.size()];
		int i = 0;
		for (Map.Entry<String, ChartsEngine> entry : this.entrySet()) {
			engineNames[i] = entry.getValue().getName();
			i++;
		}
		return engineNames;
	}

	/**
	 * Gets the instance stored in the Toolbox.
	 * 
	 * @return the instance stored in the Toolbox
	 */
	public static ChartsEnginesManager getInstance() {
		return (ChartsEnginesManager) Toolbox.getEngineManager(EngineType.CHARTS);
	}


	/**
	 * Gets the entry names and values of the supported export formats of the specified charts engine.
	 * 
	 * @param engine
	 * @return the entry names and values of the supported export formats of the specified charts engine
	 */
	public static String[][] getExportFormatsEntryNamesAndValues(ChartsEngine engine) {
		ArrayList<String> supportedExportFormats = new ArrayList<>();
		supportedExportFormats.addAll(engine.getSupportedOutputFileFormats());

		// Sort the set
		TreeSet<String> formatsSet = new TreeSet<>(supportedExportFormats);
		String[] formats = formatsSet.toArray(new String[formatsSet.size()]);

		String[][] exportFormats = new String[formats.length][2];
		for (int j = 0; j < formats.length; j++) {
			exportFormats[j][0] = formats[j];
			exportFormats[j][1] = formats[j];
		}
		return exportFormats;
	}


	@Override
	public EngineType getEnginesType() {
		return EngineType.CHARTS;
	}

	/**
	 * Returns the actual list of the installed charts engine contributions.
	 * 
	 * @return
	 */
	public static ArrayList<ChartsEngine> getChartsEngines() {
		return chartsEngines;
	}
}
