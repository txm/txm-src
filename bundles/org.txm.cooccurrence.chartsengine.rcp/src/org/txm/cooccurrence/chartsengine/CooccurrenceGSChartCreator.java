package org.txm.cooccurrence.chartsengine;

/**
 * 
 */


import java.awt.Color;
import java.util.ArrayList;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.txm.chartsengine.graphstream.core.GSChartCreator;
import org.txm.cooccurrence.core.functions.Cooccurrence;
import org.txm.cooccurrence.functions.CooccurrenceGraph;

/**
 * Cooccurrence graph chart creator.
 * 
 * @author sjacquot
 *
 */
// FIXME: simulation tests for cooccurrence chart
public class CooccurrenceGSChartCreator extends GSChartCreator<CooccurrenceGraph> {

	/**
	 * 
	 */
	public CooccurrenceGSChartCreator() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Graph createChart(CooccurrenceGraph result) {
		//
		// String styleSheet =
		// "node {" +
		// " fill-mode: dyn-plain;" +
		// " fill-color: grey;" +
		// " size-mode: dyn-size;" +
		// " size: 5px;" +
		//// " size: 30px, 30px;" +
		//// " text-size: 12px;" +
		//// " shape: freeplane;" +
		//// // " size-mode: fit;" +
		//// " fill-mode: none;" +
		//// " stroke-mode: plain;" +
		// "}" +
		// "edge {" +
		// " shape: cubic-curve;" +
		//
		//
		//
		//// " stroke-mode: plain;" +
		//
		// "}" +
		// /*"node.marked {" +
		// " fill-color: red;" +
		// "}" +*/
		//
		// "node.titre {" +
		// " size: 55px, 45px;" +
		// " fill-color: blue;" +
		// " text-color: red;" +
		// " shape: diamond;" +
		// "}" +
		//
		// "node.axe {" +
		// " fill-color: blue;" +
		// " text-color: blue;" +
		// " shape: box;" +
		// "}" +
		// /*
		// "node.mot {" +
		// " fill-color: green;" +
		// " text-color: green;" +
		// "}" +*/
		// "node.auteur {" +
		// " fill-color: red;" +
		// " text-color: gray;" +
		// " shape: diamond;" +
		// "}";
		//

		Cooccurrence cooccurrence = result.getParent();
		// Graph graph = (Graph) result.getChart();

		Graph graph = new SingleGraph(cooccurrence.getSimpleName());
		// graph.addAttribute("ui.stylesheet", styleSheet);


		// graph.clear();
		// graph.clearSinks();
		//
		// Generator gen = new DorogovtsevMendesGenerator();
		// gen.addSink(graph);
		// gen.begin();
		// for(int i=0; i<500; i++) {
		// gen.nextEvents();
		// }
		// gen.end();

		// graph.addAttribute("ui.stylesheet", styleSheet);
		// System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer"); // Pour les fonctionnalités avancées des CSS

		// int maxSize = 20;
		//
		// // add query node
		// Node rootNode = graph.addNode(cooccurrence.getQuery().getQueryString());
		// rootNode.addAttribute("ui.size", 5);
		// rootNode.addAttribute("ui.color", Color.RED);
		//
		// // add cooccurrents nodes
		// for (int i = 0; i < cooccurrence.getLines().size(); i++) {
		//
		// Node n2 = graph.addNode(cooccurrence.getLines().get(i).occ);
		// n2.addAttribute("ui.size", cooccurrence.getLines().get(i).freq / 5);
		//
		// graph.addEdge(String.valueOf(i), rootNode, n2);
		//
		// }
		//
		//// graph.addNode("A");
		//// graph.addNode("B");
		//// graph.addNode("C");
		//// graph.addEdge("AB", "A", "B");
		//// graph.addEdge("BC", "B", "C");
		//// graph.addEdge("CA", "C", "A");
		//
		// // set text labels
		// for (Node n : graph) {
		// n.setAttribute("ui.label", n.getId());
		// }


		// FIXME: tests (opening a Frame with the graph but be careful, closing the Frame closes the entire RCP app)
		// graph.display();

		return graph;
	}

	@Override
	public void updateChart(CooccurrenceGraph result) {

		Cooccurrence cooccurrence = result.getParent();
		Graph graph = (Graph) result.getChart();

		graph.clear();

		String styleSheet = "node {" + //$NON-NLS-1$
				"	fill-mode: dyn-plain;" + //$NON-NLS-1$
				"   fill-color: grey;" + //$NON-NLS-1$
				"	size-mode: dyn-size;" + //$NON-NLS-1$
				"	size: 5px;" + //$NON-NLS-1$
				// " size: 30px, 30px;" +
				// " text-size: 12px;" +
				// " shape: freeplane;" +
				// // " size-mode: fit;" +
				// " fill-mode: none;" +
				// " stroke-mode: plain;" +
				"}" + //$NON-NLS-1$
				"edge {" + //$NON-NLS-1$
				"   shape: cubic-curve;" + //$NON-NLS-1$



				// " stroke-mode: plain;" +

				"}" + //$NON-NLS-1$
				/*
				 * "node.marked {" +
				 * "   fill-color: red;" +
				 * "}" +
				 */

				"node.titre {" + //$NON-NLS-1$
				"   size: 55px, 45px;" + //$NON-NLS-1$
				"   fill-color: blue;" + //$NON-NLS-1$
				"   text-color: red;" + //$NON-NLS-1$
				"   shape: diamond;" + //$NON-NLS-1$
				"}" + //$NON-NLS-1$

				"node.axe {" + //$NON-NLS-1$
				"   fill-color: blue;" + //$NON-NLS-1$
				"   text-color: blue;" + //$NON-NLS-1$
				"   shape: box;" + //$NON-NLS-1$
				"}" + //$NON-NLS-1$
				/*
				 * "node.mot {" +
				 * "   fill-color: green;" +
				 * "   text-color: green;" +
				 * "}" +
				 */
				"node.auteur {" + //$NON-NLS-1$
				"   fill-color: red;" + //$NON-NLS-1$
				"   text-color: gray;" + //$NON-NLS-1$
				"   shape: diamond;" + //$NON-NLS-1$
				"}"; //$NON-NLS-1$


		graph.addAttribute("ui.stylesheet", styleSheet); //$NON-NLS-1$

		int maxSize = 20;

		// add query node
		Node rootNode = graph.addNode(cooccurrence.getQuery().getQueryString());
		rootNode.addAttribute("ui.size", 5); //$NON-NLS-1$
		rootNode.addAttribute("ui.color", Color.RED); //$NON-NLS-1$

		// add cooccurrents nodes
		for (int i = 0; i < cooccurrence.getLines().size(); i++) {

			Node n2 = graph.addNode(cooccurrence.getLines().get(i).occ);
			n2.addAttribute("ui.size", cooccurrence.getLines().get(i).freq / 5); //$NON-NLS-1$

			graph.addEdge(String.valueOf(i), rootNode, n2);

		}

		// graph.addNode("A");
		// graph.addNode("B");
		// graph.addNode("C");
		// graph.addEdge("AB", "A", "B");
		// graph.addEdge("BC", "B", "C");
		// graph.addEdge("CA", "C", "A");

		// set text labels
		for (Node n : graph) {
			n.setAttribute("ui.label", n.getId()); //$NON-NLS-1$
		}


		super.updateChart(result);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.chartsengine.core.ChartCreator#getResultDataClass()
	 */
	@Override
	public Class<CooccurrenceGraph> getResultDataClass() {
		return CooccurrenceGraph.class;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.chartsengine.core.ChartCreator#getSeriesShapesColors(java.lang.Object)
	 */
	@Override
	public ArrayList<Color> getSeriesShapesColors(Object chart) {
		// TODO Auto-generated method stub
		return null;
	}


}
