package org.txm.cooccurrence.functions;

/**
 * 
 */


import java.io.File;

import org.txm.chartsengine.core.results.ChartResult;
import org.txm.cooccurrence.core.functions.Cooccurrence;
import org.txm.utils.TXMProgressMonitor;

/**
 * @author sjacquot
 *
 */
public class CooccurrenceGraph extends ChartResult {

	/**
	 * @param parent
	 */
	public CooccurrenceGraph(Cooccurrence parent) {
		super(parent);

		// FIXME: conception problem
		// Since the command has no preferences, the default charts engine preferences are not loaded in TXMResult constructor, so the code above is needed to reload the parameters
		// It also problematic for persistence to pass "org.txm.chartsengine.core" because it is saved as bundle ID and the deserialisation can not be done since "org.txm.chartsengine.core" does not
		// found the class CooccurrenceGraph
		this.commandPreferencesNodePath = "org.txm.chartsengine.core"; //$NON-NLS-1$
		try {
			this.autoLoadParametersFromAnnotations();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Cooccurrence getParent() {
		return (Cooccurrence) parent;
	}



	/**
	 * @param parametersNodePath
	 */
	public CooccurrenceGraph(String parametersNodePath) {
		super(parametersNodePath);

		// FIXME: conception problem
		// Since the command has no preferences, the default charts engine preferences are not loaded in TXMResult constructor, so the code above is needed to reload the parameters
		// It also problematic for persistence to pass "org.txm.chartsengine.core" because it is saved as bundle ID and the deserialisation can not be done since "org.txm.chartsengine.core" does not
		// found the class CooccurrenceGraph
		this.commandPreferencesNodePath = "org.txm.chartsengine.core"; //$NON-NLS-1$
		try {
			this.autoLoadParametersFromAnnotations();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	/*
	 * (non-Javadoc)
	 * @see org.txm.core.results.TXMResult#saveParameters()
	 */
	@Override
	public boolean saveParameters() throws Exception {
		// TODO Auto-generated method stub
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.core.results.TXMResult#loadParameters()
	 */
	@Override
	public boolean loadParameters() throws Exception {
		// TODO Auto-generated method stub
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.core.results.TXMResult#getName()
	 */
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "graph"; //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.core.results.TXMResult#getSimpleName()
	 */
	@Override
	public String getSimpleName() {
		// TODO Auto-generated method stub
		return "graph"; //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.core.results.TXMResult#getDetails()
	 */
	@Override
	public String getDetails() {
		// TODO Auto-generated method stub
		return "graph"; //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.core.results.TXMResult#clean()
	 */
	@Override
	public void clean() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.core.results.TXMResult#canCompute()
	 */
	@Override
	public boolean canCompute() throws Exception {
		// TODO Auto-generated method stub
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.core.results.TXMResult#_compute()
	 */
	@Override
	protected boolean __compute(TXMProgressMonitor monitor) throws Exception {
		// TODO Auto-generated method stub
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.core.results.TXMResult#toTxt(java.io.File, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getResultType() {
		return "Cooccurrence graph"; //$NON-NLS-1$
	}

}
