// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.cooccurrence.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.cooccurrence.core.functions.Cooccurrence;
import org.txm.cooccurrence.functions.CooccurrenceGraph;
import org.txm.rcp.handlers.BaseAbstractHandler;


/**
 * Opens a cooccurrence graph editor.
 * Computes the result if it contains all required parameters.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class ComputeCooccurrenceGraph extends BaseAbstractHandler {


	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkCorpusEngine()) {
			return false;
		}

		if (!this.checkStatsEngine()) {
			return false;
		}


		Object selection = this.getCorporaViewSelectedObject(event);
		CooccurrenceGraph coocGraph = null;

		// Creating from Cooccurrence
		if (selection instanceof Cooccurrence) {
			coocGraph = new CooccurrenceGraph((Cooccurrence) selection);

		}
		// Reopening from existing result
		else if (selection instanceof CooccurrenceGraph) {
			coocGraph = (CooccurrenceGraph) selection;
		}
		// Error
		else {
			return super.logCanNotExecuteCommand(selection);
		}

		ChartEditor.openEditor(coocGraph);

		return null;
	}

}
