package org.txm.cooccurrence.editors;

import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.cooccurrence.functions.CooccurrenceGraph;

/**
 * Cooccurrence graph chart editor.
 * 
 * @author sjacquot
 *
 */
public class CooccurrenceGraphEditor extends ChartEditor<CooccurrenceGraph> {


	@Override
	public void updateEditorFromChart(boolean update) {
		// nothing to do
	}

	@Override
	public void __createPartControl() {
		// TODO Auto-generated method stub

	}


}
