package org.txm.treetagger.preferences;

import org.eclipse.core.runtime.preferences.DefaultScope;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * 
 * @author mdecorde
 *
 */
public class TreeTaggerInstallerPreferences extends TXMPreferences {

	public static final String PREFERENCES_NODE = FrameworkUtil.getBundle(TreeTaggerInstallerPreferences.class).getSymbolicName();

	public static final String VERSION = "treetaggerinstaller.version"; //$NON-NLS-1$

	@Override
	public void initializeDefaultPreferences() {
		Preferences preferences = DefaultScope.INSTANCE.getNode(PREFERENCES_NODE);
		preferences.put(VERSION, ""); //$NON-NLS-1$
	}
}
