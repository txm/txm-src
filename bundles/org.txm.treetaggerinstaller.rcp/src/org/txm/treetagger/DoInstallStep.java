package org.txm.treetagger;

import java.io.File;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.util.Util;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.osgi.framework.Version;
import org.txm.PostInstallationStep;
import org.txm.core.preferences.TXMPreferences;
import org.txm.rcp.TXMWindows;
import org.txm.treetagger.core.preferences.TreeTaggerPreferences;
import org.txm.treetagger.preferences.TreeTaggerInstallerPreferences;
import org.txm.utils.BundleUtils;
import org.txm.utils.logger.Log;


public class DoInstallStep extends PostInstallationStep {

	protected String name = "TreeTaggerInstaller"; //$NON-NLS-1$

	@Override
	public void preInstall() {

	}

	public DoInstallStep() {
		name = "TreeTagger"; //$NON-NLS-1$
	}

	@Override
	public void install() {
		Log.info("TreeTaggerInstaller.DoInstallStep.install()");
		String saved = TXMPreferences.getString(TreeTaggerInstallerPreferences.VERSION, TreeTaggerInstallerPreferences.PREFERENCES_NODE);
		Version currentVersion = BundleUtils.getBundleVersion("TreeTaggerInstaller");

		if (saved != null && saved.length() > 0) {
			Version savedVersion = new Version(saved);
			if (currentVersion.compareTo(savedVersion) <= 0) {
				Log.info("No post-installation of TreeTaggerInstaller to do");
				return; // nothing to do
			}
		}
		Log.info(NLS.bind("Updating TreeTagger preferences for TreeTaggerInstaller version {0}.", currentVersion));

		String os = "win"; //$NON-NLS-1$
		if (Util.isWindows()) { // $NON-NLS-1$
			os = "win"; //$NON-NLS-1$
		}
		else if (Util.isMac()) { // $NON-NLS-1$
			os = "macosx"; //$NON-NLS-1$
		}
		else {
			os = "linux32"; //$NON-NLS-1$
			if (System.getProperty("os.arch").contains("64")) //$NON-NLS-1$
				os = "linux64"; //$NON-NLS-1$
		}

		File bundleDir = BundleUtils.getBundleFile("TreeTaggerInstaller"); //$NON-NLS-1$
		if (bundleDir == null) {
			Log.severe("Error while retrieving TreeTaggerInstaller bundle directory.");
			return;
		}
		File treetaggerDir = new File(bundleDir, "TreeTagger"); //$NON-NLS-1$
		File OSDir = new File(treetaggerDir, os);
		final File treetaggerModelsDir = new File(treetaggerDir, "models"); //$NON-NLS-1$

		TreeTaggerPreferences.getInstance().put(TreeTaggerPreferences.INSTALL_PATH, OSDir.getAbsolutePath());

		final String previousModelDirectoryPath = TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.MODELS_PATH);
		if (previousModelDirectoryPath == null) {
			Log.info(NLS.bind("Set TreeTagger model directory to \"{0}\".", treetaggerModelsDir));
			TreeTaggerPreferences.getInstance().put(TreeTaggerPreferences.MODELS_PATH, treetaggerModelsDir.getAbsolutePath());
		}
		else if (previousModelDirectoryPath.equals(treetaggerModelsDir.getAbsolutePath())) {
			// nothing to do
			Log.info("No change to TreeTagger model directory.");
		}
		else {
			final File previousModelDirectoryDirectory = new File(previousModelDirectoryPath);
			final File[] files = previousModelDirectoryDirectory.listFiles();
			if (previousModelDirectoryDirectory.exists() && previousModelDirectoryDirectory.isDirectory()
					&& files != null && files.length > 0) {

				Log.info(NLS.bind("TreeTagger model directory already set to \"{0}\".", previousModelDirectoryDirectory));
				// final ;
				// if (Display.getCurrent() == null) display = Display.getDefault();
				// else display = Display.getCurrent();
				//
				// if (display == null) return;
				final Display display = TXMWindows.getDisplay();
				display.syncExec(new Runnable() {

					@Override
					public void run() {
						Shell shell = display.getActiveShell(); // ask user if he's sure
						String[] filenames = new String[files.length];
						
						for (int i = 0; i < files.length; i++) {
							filenames[i] = files[i].getName();
						}
						
						boolean ok = MessageDialog.openConfirm(shell, "TreeTagger model directory", "The TreeTagger model directory is already set in TXM preferences and is not empty: \n  - "
								+ StringUtils.join(filenames, "\n  - ") + "\n\nKeep using this one ?\n(" + previousModelDirectoryPath + ")");
						
						if (!ok) {
							Log.info(NLS.bind("Set TreeTagger model directory to \"{0}\".", treetaggerModelsDir));
							TreeTaggerPreferences.getInstance().put(TreeTaggerPreferences.MODELS_PATH, treetaggerModelsDir.getAbsolutePath());
						}
						else {
							Log.info("No change to TreeTagger model directory.");
						}
					}
				});
			}
			else {
				TreeTaggerPreferences.getInstance().put(TreeTaggerPreferences.MODELS_PATH, treetaggerModelsDir.getAbsolutePath());
			}
		}

		String ttdir = TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.INSTALL_PATH);
		String ttmodeldir = TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.MODELS_PATH);
		Log.warning("TreeTagger preferences set to: " + ttdir + " and " + ttmodeldir);
		TXMPreferences.put(TreeTaggerInstallerPreferences.PREFERENCES_NODE, TreeTaggerInstallerPreferences.VERSION, currentVersion.toString());

		// FIXME: to be removed when 0.8.0 preferences are ready
		TreeTaggerPreferences.getInstance().put(TreeTaggerPreferences.INSTALL_PATH, ttdir);
		TreeTaggerPreferences.getInstance().put(TreeTaggerPreferences.MODELS_PATH, ttmodeldir);
	}
}
