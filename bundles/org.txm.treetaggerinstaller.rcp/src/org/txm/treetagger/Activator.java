package org.txm.treetagger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {

		// System.out.println("Activating TreeTagger plugin");
		// String os = "win";
		// if (Util.isWindows()) { //$NON-NLS-1$
		// os = "win";
		// } else if (Util.isMac()) { //$NON-NLS-1$
		// os = "macosx";
		// } else {
		// os = "linux32";
		// if (System.getProperty("os.arch").contains("64"))
		// os = "linux64";
		// }
		//
		// URL url = FileLocator.find(Platform.getBundle("TreeTaggerInstaller"), new Path("TreeTagger"), null);
		//
		// String treetaggerDir = url.getFile();
		// File treetaggerOSDir = new File(treetaggerDir, os);
		// File treetaggerModelsDir = new File(treetaggerDir,"models");
		//
		// System.out.println("Set TreeTagger preferences to: "+treetaggerDir);
		//
		// TxmPreferences.set(TreeTaggerPreferencePage.TREETAGGER_INSTALL_PATH, treetaggerOSDir.getAbsolutePath());
		// TxmPreferences.set(TreeTaggerPreferencePage.TREETAGGER_MODELS_PATH, treetaggerModelsDir.getAbsolutePath());
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		// TODO Auto-generated method stub

	}

}
