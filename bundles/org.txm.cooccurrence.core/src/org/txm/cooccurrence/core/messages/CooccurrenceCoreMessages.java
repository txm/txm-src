package org.txm.cooccurrence.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Cooccurrence core message.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class CooccurrenceCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.cooccurrence.core.messages.messages"; //$NON-NLS-1$

	public static String RESULT_TYPE;


	public static String P0CooccurentsForP1Occurrences;

	public static String propertiesColon;

	public static String errorColonNegativeArgumentInFactlnFunction;

	public static String errorColonNegativeArgumentInRbicolnFunction;

	public static String occ;

	public static String freq;

	public static String coFreq;

	public static String score;

	public static String meanDist;

	public static String errorColonNoCooccurrents;

	public static String fColon;

	public static String occFreqCoFreqScoreMeanDistMode;

	public static String occColon;

	public static String scoreColon;

	public static String meanDistColon;

	public static String cooccurrentsOfP0PropertieP1InTheP2Corpus;


	public static String ErrorWhileComputingIndexForTheCooccurrenceAborting;

	public static String ErrorWhileComputingIndexForTheCooccurrenceP0;

	public static String ErrorWhileSettingCooccurrenceParameters;

	public static String info_buildingQueries;

	public static String info_retreivingMatches;

	public static String info_buildingLineSignatures;

	public static String info_counting;

	public static String info_buildingLexicalTable;

	public static String info_computingSpecificitiesScores;

	public static String info_details;

	public static String NoQuery;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, CooccurrenceCoreMessages.class);
	}
}
