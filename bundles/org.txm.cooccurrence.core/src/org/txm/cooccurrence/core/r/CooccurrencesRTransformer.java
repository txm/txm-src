package org.txm.cooccurrence.core.r;

import org.txm.cooccurrence.core.functions.Cooccurrence;
import org.txm.cooccurrence.core.functions.Cooccurrence.CLine;
import org.txm.statsengine.r.core.RTransformer;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

public class CooccurrencesRTransformer extends RTransformer<Cooccurrence> {

	/** The noconc. */
	protected static int nocooc = 1;

	/** The prefix r. */
	protected static String prefixR = "Cooccurrences"; //$NON-NLS-1$

	@Override
	public Cooccurrence fromRtoTXM(String symbol) throws RWorkspaceException {
		return null;
	}

	@Override
	public String _fromTXMtoR(Cooccurrence o, String symbol) throws RWorkspaceException {

		if (!(o instanceof Cooccurrence)) return null;

		Cooccurrence cooc = (Cooccurrence) o;

		if (symbol == null || symbol.length() == 0) {
			symbol = prefixR + nocooc;
			nocooc++;
		}

		String[] occ = new String[cooc.getLines().size()];
		int[] freq = new int[cooc.getLines().size()];
		int[] cofreq = new int[cooc.getLines().size()];
		double[] score = new double[cooc.getLines().size()];
		double[] dist = new double[cooc.getLines().size()];

		int i = 0;
		for (CLine line : cooc.getLines()) {
			occ[i] = line.occ;
			freq[i] = line.freq;
			cofreq[i] = line.nbocc;
			score[i] = line.score;
			dist[i] = line.distmoyenne;
			i++;
		}

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		rw.addVectorToWorkspace("coococc", occ); //$NON-NLS-1$
		rw.addVectorToWorkspace("coocfreq", freq); //$NON-NLS-1$
		rw.addVectorToWorkspace("cooccofreq", cofreq); //$NON-NLS-1$
		rw.addVectorToWorkspace("coocscore", score); //$NON-NLS-1$
		rw.addVectorToWorkspace("coocmeandist", dist); //$NON-NLS-1$

		rw.eval(symbol + "<- matrix(data = c(coocfreq, cooccofreq, coocscore, coocmeandist), nrow = " + cooc.getLines().size() + ", ncol = 4)"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval("rownames(" + symbol + " ) <- coococc"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval("colnames(" + symbol + " ) <- c('freq', 'cofreq', 'score', 'dist')"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval(symbol + "<- list(data=" + symbol //$NON-NLS-1$
		//+ ", leftcontext="+this.leftContextSize
		//+ ", rightcontext="+this.rightContextSize
		//+ ", query=\""+this.query.getQueryString()+"\""
				+ ")"); //$NON-NLS-1$

		rw.eval("rm(coocfreq, cooccofreq, coocscore, coocmeandist)"); //$NON-NLS-1$

		return symbol;
	}
}
