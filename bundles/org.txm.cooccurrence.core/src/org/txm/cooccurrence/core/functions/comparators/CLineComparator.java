// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.cooccurrence.core.functions.comparators;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

import org.txm.cooccurrence.core.functions.Cooccurrence;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

/**
 * comparator of cooc line
 * 
 * @author mdecorde.
 */
public abstract class CLineComparator implements Comparator<Cooccurrence.CLine> {

	/** The Constant NAME. */
	protected static final String NAME = "None"; //$NON-NLS-1$

	/** The locale. */
	private Locale locale = new Locale("en", "US"); //$NON-NLS-1$ //$NON-NLS-2$

	/** The collator. */
	protected Collator collator;

	protected boolean ascending = true;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return CLineComparator.NAME;
	}

	/**
	 * Gets the locale.
	 *
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * Sets the locale.
	 *
	 * @param locale the new locale
	 */
	public void setLocale(Locale locale) {

		this.locale = locale;
		this.collator = Collator.getInstance(locale);
		this.collator.setStrength(Collator.TERTIARY);
	}

	/**
	 * Initialize.
	 *
	 * @param corpus the corpus
	 */
	public void initialize(CQPCorpus corpus) {
		setLocale(new Locale(corpus.getLang()));
	}

	/**
	 * Instantiates a new c line comparator.
	 */
	public CLineComparator() {
		setLocale(locale);
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public final int compare(Cooccurrence.CLine l1, Cooccurrence.CLine l2) {
		int c = _compare(l1, l2);
		if (ascending) {
			return c;
		}
		else {
			return -c;
		}
	}

	public abstract int _compare(Cooccurrence.CLine l1, Cooccurrence.CLine l2);

	public void setAscending(boolean b) {
		this.ascending = b;
	}
}
