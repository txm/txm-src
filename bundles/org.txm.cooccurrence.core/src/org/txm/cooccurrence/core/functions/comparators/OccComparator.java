// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.cooccurrence.core.functions.comparators;

import org.txm.cooccurrence.core.functions.Cooccurrence;

/**
 * compare the occurrence property value of lines
 * 
 * @author mdecorde.
 */
public class OccComparator extends CLineComparator {

	/** The Constant NAME. */
	private static final String NAME = "Occ"; //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.txm.functions.cooccurrences.comparators.CLineComparator#getName()
	 */
	@Override
	public String getName() {
		return NAME;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.functions.cooccurrences.comparators.CLineComparator#compare(org.txm.functions.cooccurrences.Cooccurrence.CLine, org.txm.functions.cooccurrences.Cooccurrence.CLine)
	 */
	@Override
	public int _compare(Cooccurrence.CLine l1, Cooccurrence.CLine l2) {
		return collator.compare(l1.occ, l2.occ);
	}
}
