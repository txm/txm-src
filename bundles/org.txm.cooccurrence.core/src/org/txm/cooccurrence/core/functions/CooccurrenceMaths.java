package org.txm.cooccurrence.core.functions;

import org.txm.cooccurrence.core.messages.CooccurrenceCoreMessages;

/**
 * static methods to compute cooccurrences scores
 * 
 * @author mdecorde
 *
 */
public class CooccurrenceMaths {

	/** The array of xyz values. */
	static double[] a = new double[101];

	/** The arbicoln. */
	static double[][] arbicoln = new double[101][101];

	/** The cof. */
	static double[] cof = { 76.18009172947146, -86.50532032941677,
			24.01409824083091, -1.231739572450155, 0.1208650973866179e-2,
			-0.5395239384953e-5 };

	/** The maxprec. */
	static double maxprec = 0.0;


	static {// void init_rbicoln()
		int i, j;

		for (i = 0; i < 101; i++)
			for (j = 0; j < 101; j++)
				arbicoln[i][j] = -1;
	}

	/**
	 * Factln.
	 *
	 * @param n the n
	 * @return the double
	 */
	static double factln(int n) {
		if (n < 0)
			System.err.println(CooccurrenceCoreMessages.errorColonNegativeArgumentInFactlnFunction);
		if (n <= 1)
			return 0.0;
		if (n <= 100)
			return (a[n] > 0) ? a[n] : (a[n] = gammln(n + 1.0));
		else
			return gammln(n + 1.0);
	}

	/**
	 * Gammln.
	 *
	 * @param xx the xx
	 * @return the double
	 */
	static double gammln(double xx) {
		double x, y, tmp, ser;

		int j;

		y = x = xx;
		tmp = x + 5.5;
		tmp -= (x + 0.5) * Math.log(tmp);
		ser = 1.000000000190015;
		for (j = 0; j <= 5; j++)
			ser += cof[j] / ++y;
		return -tmp + Math.log(2.5066282746310005 * ser / x);
	}

	// Maths
	/**
	 * Proba binom.
	 *
	 * @param inf the inf
	 * @param ing the ing
	 * @param s the s
	 * @param cf the cf
	 * @return the double
	 */
	public static double ProbaBinom(long inf, long ing, long s, long cf) {
		long mode = (long) Math.floor((double) ((inf + 1) * (ing + 1))
				/ (inf + ing + s + 2));
		long f, g, k;
		double P = 0.0, p, lp, dnm;
		int lpflag = 1;

		if (cf <= mode)
			return (1.0);

		if (inf < ing) {
			f = inf;
			g = ing;
		}
		else {
			f = ing;
			g = inf;
		}
		;

		dnm = rbicoln((int) (f + g + s), (int) g);
		for (k = cf; k <= f; k++) {
			p = Math.exp(rbicoln((int) f, (int) k)
					+ rbicoln((int) (s + g), (int) (g - k)) - dnm);
			// if (lpflag > 0) { // rentre dedans de toute facon
			lp = p;
			lpflag = 0;
			// };
			if ((lp / p) < maxprec)
				break;
			lp = p;
			P += p;
		}
		;

		return (P);

		/*
		 * for (k = 0; k < cf; k++) { p = exp(rbicoln(f,
		 * k)+rbicoln(s+g,g-k)-rbicoln(f+g+s, g)); P += p; }; return(fabs(1-P));
		 */
	}

	/**
	 * Rbicoln.
	 *
	 * @param n the n
	 * @param k the k
	 * @return the double
	 */
	static double rbicoln(int n, int k) {
		if (n < 0)
			System.err.println(CooccurrenceCoreMessages.errorColonNegativeArgumentInRbicolnFunction);
		if (k < 0)
			System.err.println(CooccurrenceCoreMessages.errorColonNegativeArgumentInRbicolnFunction);
		if (n <= 100 && k <= 100)
			return (arbicoln[n][k] >= 0) ? arbicoln[n][k]
					: (arbicoln[n][k] = factln(n) - factln(k) - factln(n - k));
		else
			return factln(n) - factln(k) - factln(n - k);
	}

	/**
	 * Calcmode.
	 *
	 * @param inf the inf
	 * @param ing the ing
	 * @param s the s
	 * @param cf the cf
	 * @return the long
	 */
	long calcmode(long inf, long ing, long s, long cf) {
		return (long) Math.floor((double) ((inf + 1) * (ing + 1))
				/ (inf + ing + s + 2));
	}


	//	/**
	//	 * Compute.
	//	 *
	//	 * @return true, if successful
	//	 */
	//	//FIXME: not used? or may in portal? in any case, need to use other method compute() now
	//	public boolean compute() {
	//		if (conclines == null || concordance == null || P == -1 || FA == -1
	//				|| properties == null || corpus == null) {
	//			System.err.println(CooccurrenceCoreMessages.Cooccurrence_5 + concordance);
	//			return false;
	//		}
	//		// System.out.println("seuils: freq="+seuil_freq+", count="+seuil_count+", score="+seuil_score);
	//		String queryfreq = "["; //$NON-NLS-1$
	//		for (Property p : properties) {
	//			queryfreq += " " + p.getName() + "=\".*\" |"; //$NON-NLS-1$ //$NON-NLS-2$
	//		}
	//		queryfreq = queryfreq.substring(0, queryfreq.length() - 1) + "]"; //$NON-NLS-1$
	//		try {
	//			index = new Index(corpus, new Query(queryfreq), properties);
	//		} catch (Exception e){
	//			Log.severe("Error while computing Index for the cooccurrence: "+e.getLocalizedMessage());
	//			return false;
	//		}
	//		// voc.getCorpus().storeResult(voc);
	//
	//		count = new HashMap<String, Integer>(index.getV());
	//		dist = new HashMap<String, Float>(index.getV());
	//		freq = new HashMap<String, Integer>(index.getV());
	//		scores = new HashMap<String, Double>(index.getV());
	//		occproperties = new HashMap<String, List<String>>(index.getV());
	//
	//		// System.out.println("Lignes");
	//		// System.out.println("leftC\tKeyword\trightC");
	//		for (Line concline : conclines) {
	//			// System.out.println(concline.leftContextToString()+"\t"+concline.keywordToString()+"\t"+concline.rightContextToString());
	//			Map<Property, List<String>> propsvalue = concline
	//					.getLeftCtxViewProperties();
	//			countOcc(propsvalue, false);
	//			propsvalue = concline.getRightCtxViewProperties();
	//			countOcc(propsvalue, true);
	//		}
	//
	//		for (String cooc : count.keySet()) {
	//			if (count.get(cooc) >= seuil_count && freq.get(cooc) >= seuil_freq) {
	//				// System.out.println("compute dist et score de : "+cooc);
	//				// calcul dist moyenne avant de rectif ier count
	//				dist.put(cooc, dist.get(cooc) / count.get(cooc));
	//
	//				if (count.get(cooc) > freq.get(cooc))
	//					count.put(cooc, freq.get(cooc));
	//
	//				// calcul score du cooc
	//				double score = ProbaBinom(FA, freq.get(cooc), P,
	//						count.get(cooc));
	//				if (score >= seuil_score) {
	//					long mode = calcmode(FA, freq.get(cooc), P, count.get(cooc));
	//					CLine line = new CLine(this, cooc, occproperties.get(cooc),
	//							count.get(cooc), freq.get(cooc), score, dist
	//							.get(cooc), mode);
	//					lines.add(line);
	//				}
	//			}
	//		}
	//
	//		// clear memory
	//		count = null;
	//		dist = null;
	//		freq = null;
	//		scores = null;
	//		occproperties = null;
	//
	//		return true;
	//	}
}
