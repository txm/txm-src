// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (Tue, 24 Jan 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.cooccurrence.core.functions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.osgi.util.NLS;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.core.functions.Line;
import org.txm.concordance.core.preferences.ConcordancePreferences;
import org.txm.cooccurrence.core.functions.comparators.CLineComparator;
import org.txm.cooccurrence.core.messages.CooccurrenceCoreMessages;
import org.txm.cooccurrence.core.preferences.CooccurrencePreferences;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMParameters;
import org.txm.core.results.TXMResult;
import org.txm.index.core.functions.Index;
import org.txm.lexicaltable.core.statsengine.r.data.LexicalTableImpl;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.QueryBasedTXMResult;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.specificities.core.statsengine.r.function.SpecificitiesR;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.LogMonitor;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * Compute a cooccurrence from a pivot query.
 *
 * @author mdecorde
 * 
 */
public class Cooccurrence extends QueryBasedTXMResult {

	boolean debug = false;

	/**
	 * The Class CLine.
	 */
	public class CLine {

		/** The cooc. */
		Cooccurrence cooc;

		/** The distmoyenne. */
		public float distmoyenne;

		/** The freq. */
		public int freq;

		/** The id. */
		public int id;

		/** The mode. */
		public long mode;

		/** The nbocc. */
		public int nbocc;

		/** The occ. */
		public String occ;

		/** The props. */
		public List<String> props;

		/** The score. */
		public double score;

		/** The score. */
		public String debug;

		/**
		 * Instantiates a new c line.
		 *
		 * @param cooc the cooc
		 * @param occ the occ
		 * @param props the props
		 * @param nbocc the nbocc
		 * @param freq the freq
		 * @param score the score
		 * @param distmoyenne the distmoyenne
		 * @param mode the mode
		 */
		public CLine(Cooccurrence cooc, String occ, List<String> props,
				int nbocc, int freq, double score, Float distmoyenne, long mode) {
			this.occ = occ;
			this.props = props;
			this.freq = freq;
			this.nbocc = nbocc;
			this.score = score;
			this.distmoyenne = distmoyenne;
			this.mode = mode;
			this.cooc = cooc;
		}

		/**
		 * Adds the txt sep.
		 *
		 * @param str the str
		 * @param sep the sep
		 * @return the string
		 */
		private String addTxtSep(String str, String sep) {
			return sep + str.replace(sep, sep + sep) + sep;
		}

		/**
		 * Gets the cooc.
		 *
		 * @return the cooc
		 */
		public Cooccurrence getCooc() {
			return cooc;
		}

		/**
		 * Resume.
		 *
		 * @param colseparator the colseparator
		 * @param txtseparator the txtseparator
		 * @return the string
		 */
		public String resume(String colseparator, String txtseparator) {
			return addTxtSep("" + occ, txtseparator) //$NON-NLS-1$
					+ colseparator + freq + colseparator + nbocc + colseparator + score + colseparator + distmoyenne;
		}

		/**
		 * Sets the count and dist.
		 *
		 * @param count the count
		 * @param dist the dist
		 */
		public void setCountAndDist(int count, int dist) {
			this.nbocc = count;
			this.distmoyenne = dist;
		}

		/**
		 * Sets the freq.
		 *
		 * @param freq the new freq
		 */
		public void setFreq(int freq) {
			this.freq = freq;
		}

		/**
		 * Sets the score.
		 */
		public void setScore() {// FB == freq, R = nbocc
			this.score = (this.nbocc + this.freq + this.distmoyenne);
		}

		@Override
		public String toString() {
			return occ + CooccurrenceCoreMessages.fColon + freq + CooccurrenceCoreMessages.occColon + nbocc + CooccurrenceCoreMessages.scoreColon + score + CooccurrenceCoreMessages.meanDistColon
					+ distmoyenne + CooccurrenceCoreMessages.propertiesColon + props + debug;
		}
	}

	/** The nocooc. */
	protected static int nocooc = 1;

	/** The prefix r. */
	protected static String prefixR = "Cooccurrences"; //$NON-NLS-1$

	/** The allsignaturesstr. */
	private HashMap<Integer, String> allsignaturesstr;

	/** The anti-context query used to trim the nearest contexts cooccurrents. */
	private CQLQuery anticontextquery;

	private boolean buildLexicalTableWithCooccurrents;

	/** The conclines. */
	List<org.txm.concordance.core.functions.Line> conclines;

	/** The conc. */
	Concordance concordance;

	/** The contextquery. */
	private CQLQuery contextQuery;

	/** The count. */
	HashMap<String, Integer> count;

	// contains the sum of distances
	/** The counts. */
	HashMap<String, Integer> counts = new HashMap<>();

	/** The dist. */
	HashMap<String, Float> dist;

	// System.out.println("Matches: focus: "+m1.size()+" full: "+m2.size()+" anti: "+m3.size());
	// System.out.println("T matches : "+(System.currentTimeMillis()- time)); //$NON-NLS-1$
	/** The distances. */
	HashMap<String, Double> distances = new HashMap<>();

	/** The FA. */
	int FA = -1;

	/** The freq. */
	HashMap<String, Integer> freq;

	/** The index. */
	Index index;

	/** The indexfreqs. */
	HashMap<String, Integer> indexfreqs = new HashMap<>();

	/** The keys to string. */
	private HashMap<String, Object> keysToString;

	/** The lines. */
	List<CLine> lines = new ArrayList<>();

	/** The lt. */
	private LexicalTableImpl lt;

	/** The m1. */
	private List<Match> m1;

	/** The m2. */
	private List<Match> m2;

	/** The m3. */
	private List<Match> m3;

	int numberOfCooccurrents = -1;

	/** The number of keyword. */
	int numberOfKeyword = 0;

	/** The occproperties. */
	HashMap<String, List<String>> occproperties;

	/** The P. */
	int P = -1;

	/** The reference corpus to use = the R symbol that point to a matrix WordxFreqs. */
	String referenceCorpus;

	/** The scores. */
	HashMap<String, Double> scores;

	/** The symbol. */
	private String symbol;

	/** The writer. */
	private BufferedWriter writer;

	@Parameter(key = CooccurrencePreferences.QUERY_FILTER)
	protected String pCooccurentQueryFilter = "[]"; //$NON-NLS-1$

	/** The mincof. */
	@Parameter(key = CooccurrencePreferences.MIN_COUNT)
	protected Integer pFCoocFilter;

	/** The minf. */
	@Parameter(key = TXMPreferences.F_MIN)
	protected Integer pFminFilter;

	/** The include xpivot. */
	@Parameter(key = CooccurrencePreferences.INCLUDE_X_PIVOT)
	protected Boolean pIncludeXpivot;

	/** The maxleft. */
	@Parameter(key = CooccurrencePreferences.MAX_LEFT)
	protected Integer pMaxLeftContextSize;

	/** The maxright. */
	@Parameter(key = CooccurrencePreferences.MAX_RIGHT)
	protected Integer pMaxRightContextSize;

	/** The minleft. */
	@Parameter(key = CooccurrencePreferences.MIN_LEFT)
	protected Integer pMinLeftContextSize;

	/** The minright. */
	@Parameter(key = CooccurrencePreferences.MIN_RIGHT)
	protected Integer pMinRightContextSize;

	/** The cooccurrents properties to display. */
	@Parameter(key = CooccurrencePreferences.UNIT_PROPERTIES)
	protected List<WordProperty> pProperties;

	/** The keyword query. */
	@Parameter(key = CooccurrencePreferences.QUERY)
	protected CQLQuery pQuery;

	/** The minscore. */
	@Parameter(key = CooccurrencePreferences.MIN_SCORE)
	protected Float pScoreMinFilter;

	/**
	 * The structural unit context limit.
	 * In null then the unit property is used.
	 */
	@Parameter(key = CooccurrencePreferences.STRUCTURAL_UNIT_LIMIT, electric = false)
	protected StructuralUnit pStructuralUnitLimit;

	private CQLQuery fixedQuery;

	/**
	 * Creates a not computed cooccurrence from the specified corpus.
	 * 
	 * @param parent
	 */
	public Cooccurrence(CQPCorpus parent) {
		super(parent);
	}

	/**
	 * Creates a not computed cooccurrence from a parameters node.
	 * 
	 * @param parametersNodePath
	 */
	public Cooccurrence(String parametersNodePath) {
		super(parametersNodePath);
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws CqiClientException, IOException, CqiServerError, StatException {
		// System.out.println("cooc: "+corpus+" "+query+" "+properties+" "+limit+" "+maxLeft+" "+minLeft+" "+minRight+" "+maxRight+" "+minFreq+" "+minCof+" "+minScore+" "+includeXpivot);

		monitor.setTask(CooccurrenceCoreMessages.info_buildingQueries);

		// clear data
		try {
			this.numberOfCooccurrents = -1;
			this.lines.clear();
			this.allsignaturesstr.clear();
			this.conclines.clear();
			this.count.clear();
			this.counts.clear();
			this.dist.clear();
			this.distances.clear();
			this.indexfreqs.clear();
			this.keysToString.clear();
			this.occproperties.clear();
		}
		catch (Exception e) {
		}

		if (!this.stepQueryLimits()) {
			return false;
		}

		monitor.setTask(CooccurrenceCoreMessages.info_retreivingMatches);
		if (!this.stepGetMatches()) {
			return false;
		}
		monitor.worked(20);

		monitor.setTask(CooccurrenceCoreMessages.info_buildingLineSignatures);
		if (!this.stepBuildSignatures()) {
			return false;
		}
		monitor.worked(20);

		monitor.setTask(CooccurrenceCoreMessages.info_counting);
		if (!this.stepCount()) {
			return false;
		}
		monitor.worked(20);

		monitor.setTask(CooccurrenceCoreMessages.info_buildingLexicalTable);
		if (!this.stepBuildLexicalTable(monitor)) {
			return false;
		}
		monitor.worked(10);

		monitor.setTask(CooccurrenceCoreMessages.info_computingSpecificitiesScores);
		if (!this.stepGetScores()) {
			return false;
		}

		this.clearMemory();
		monitor.done();

		return true;
	}

	@Override
	public String[] getExportTXTExtensions() {
		return new String[] { "*.csv" }; //$NON-NLS-1$
	}

	/**
	 * To txt.
	 *
	 * @param outfile the outfile
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @return true, if successful
	 */
	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) {
		try {
			// NK: writer declared as class attribute to perform a clean if the operation is interrupted
			this.writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(outfile), encoding));
			// if ("UTF-8".equals(encoding)) writer.write('\ufeff'); // UTF-8 BOM
			toTxt(writer, colseparator, txtseparator);
		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.bind(TXMCoreMessages.error_errorP0, e));
			return false;
		}
		return true;
	}


	/**
	 * As r matrix.
	 *
	 * @return the string
	 * @throws RWorkspaceException the r workspace exception
	 */
	public String asRMatrix() throws RWorkspaceException {
		symbol = prefixR + nocooc;

		String[] occ = new String[this.lines.size()];
		int[] freq = new int[this.lines.size()];
		int[] cofreq = new int[this.lines.size()];
		double[] score = new double[this.lines.size()];
		double[] dist = new double[this.lines.size()];

		int i = 0;
		for (CLine line : this.lines) {
			occ[i] = line.occ;
			freq[i] = line.freq;
			cofreq[i] = line.nbocc;
			score[i] = line.score;
			dist[i] = line.distmoyenne;
			i++;
		}

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		rw.addVectorToWorkspace("coococc", occ); //$NON-NLS-1$
		rw.addVectorToWorkspace("coocfreq", freq); //$NON-NLS-1$
		rw.addVectorToWorkspace("cooccofreq", cofreq); //$NON-NLS-1$
		rw.addVectorToWorkspace("coocscore", score); //$NON-NLS-1$
		rw.addVectorToWorkspace("coocmeandist", dist); //$NON-NLS-1$

		rw.eval(symbol + "<- matrix(data = c(coocfreq, cooccofreq, coocscore, coocmeandist), nrow = " + this.lines.size() + ", ncol = 4)"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval("rownames(" + symbol + " ) <- coococc"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval("colnames(" + symbol + " ) <- c('freq', 'cofreq', 'score', 'dist')"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval(symbol + "<- list(data=" + symbol //$NON-NLS-1$
		// + ", leftcontext="+this.leftContextSize
		// + ", rightcontext="+this.rightContextSize
		// + ", query=\""+this.query.getQueryString()+"\""
				+ ")"); //$NON-NLS-1$

		nocooc++;
		return symbol;
	}

	@Override
	public boolean canCompute() {

		if (pQuery == null || pQuery.isEmpty()) {
			Log.fine("No query set."); //$NON-NLS-1$
			return false;
		}

		if (pProperties == null) {
			Log.fine("No properties set."); //$NON-NLS-1$
			return false;
		}

		if (getCorpus() == null) {
			Log.fine("No corpus set."); //$NON-NLS-1$
			return false;
		}

		if (pProperties.size() == 0) {
			Log.fine("No properties filled."); //$NON-NLS-1$
			return false;
		}

		return true;
	}

	@Override
	public void clean() {
		try {
			if (writer != null) {
				this.writer.flush();
				this.writer.close();
			}
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	// FIXME: useless?
	public void clearMemory() {
		if (distances != null) distances.clear();
		if (counts != null) counts.clear();
		if (indexfreqs != null) indexfreqs.clear();
		if (m1 != null) m1.clear();
		if (m2 != null) m2.clear();
		if (m3 != null) m3.clear();
		if (allsignaturesstr != null) allsignaturesstr.clear();
		if (keysToString != null) keysToString.clear();
		if (occproperties != null) occproperties.clear();
		if (count != null) count.clear();
		if (dist != null) dist.clear();
		if (freq != null) freq.clear();
		if (scores != null) scores.clear();

		lt = null;
	}

	// /**
	// * Count occ.
	// *
	// * @param propsvalue the propsvalue
	// * @param rightcontext the rightcontext
	// */
	// public void countOcc(Map<Property, List<String>> propsvalue, boolean rightcontext) {
	// // System.out.println("countOcc (R="+rightcontext+") "+propsvalue);
	// Property key = pProperties.get(0);
	// List<String> iterationlist = propsvalue.get(key);
	// // pr chq mot
	// for (int i = 0; i < iterationlist.size(); i++) {
	// // build occ
	// String occ = ""; //$NON-NLS-1$
	// for (Property p : pProperties)
	// occ += propsvalue.get(p).get(i) + "_"; //$NON-NLS-1$
	// occ = occ.substring(0, occ.length() - 1);
	//
	// if (occproperties.get(occ) == null) {
	// ArrayList<String> values = new ArrayList<String>();
	// for (Property p : pProperties)
	// values.add(propsvalue.get(p).get(i));
	// occproperties.put(occ, values);
	// }
	//
	// // System.out.println("occ '"+occ+"'");
	// // update nbocc
	// if (count.get(occ) == null) {
	// count.put(occ, 0);
	// }
	// count.put(occ, count.get(occ) + 1);
	//
	// // update dist
	// if (dist.get(occ) == null) {
	// dist.put(occ, 0.0f);
	// }
	// if (rightcontext) {
	// dist.put(occ, dist.get(occ) + i + 1);
	// } else {
	// dist.put(occ, dist.get(occ) + iterationlist.size() - i);
	// }
	//
	// // update freq
	// if (freq.get(occ) == null) {
	// // System.out.println("compute freq of "+occ);
	// int occfreq = -1; // calcul avec l'index
	// for (org.txm.index.core.functions.Line l : index.getLines(0, index.getV())) {
	// if (l.toString().equals(occ)) {
	// // System.out.println("FOUND "+occ);
	// occfreq = l.getFrequency();
	// break;
	// }
	// }
	// freq.put(occ, occfreq);
	// }
	// }
	// }

	/**
	 * Creates a CQL query string from the specified lines.
	 *
	 * @param lines
	 * @return the query
	 */
	public Query createQuery(List<CLine> lines) {

		if (this.getQuery().isEmpty()) {
			return new CQLQuery(""); //$NON-NLS-1$
		}

		int nbProps = this.getProperties().size();
		List<WordProperty> props = this.getProperties();

		String query = "@["; //$NON-NLS-1$
		for (int p = 0; p < nbProps; p++) {

			String test = null;

			if (props.get(p) instanceof WordProperty) {
				ArrayList<String> values = new ArrayList<String>();
				for (int l = 0; l < lines.size(); l++) {
					CLine line = lines.get(l);
					String s = line.props.get(p);
					values.add(s);
				}
				test = props.get(p).getCQLTest(values);
			}

			if (test != null) {

				if (p > 0) {
					query += " & "; //$NON-NLS-1$
				}

				query += test;
			}
		}
		query += "] "; //$NON-NLS-1$

		int maxempan = Math.max(this.getMaxLeft(), this.getMaxRight());
		if (this.getIncludeXPivot() && maxempan == 0) maxempan = 1;

		String maxempanstr = "within " + maxempan + " "; //$NON-NLS-1$ //$NON-NLS-2$
		if (this.getStructuralUnitLimit() != null) maxempanstr += this.getStructuralUnitLimit().getName();


		String pquery = CQLQuery.fixQuery(this.getQuery().getQueryString(), this.getCorpus().getLang());
		if (this.getMaxLeft() == 0 && !this.pIncludeXpivot) {
			query = "" + pquery + " []* " + query + " " + maxempanstr; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		else if (this.getMaxRight() == 0 && !this.pIncludeXpivot) {
			query = "" + query + " []* " + pquery + " " + maxempanstr; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		else {
			query = "(" + pquery + " []* " + query + ") | (" + query + " []* " + pquery + ") " + maxempanstr; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		}

		return new CQLQuery(query);
	}

	@Override
	public String getComputingDoneMessage() {
		if (this.lines.isEmpty()) {
			return TXMCoreMessages.common_noResults;
		}
		else {
			return TXMCoreMessages.bind(CooccurrenceCoreMessages.P0CooccurentsForP1Occurrences, TXMCoreMessages.formatNumber(this.lines.size()), TXMCoreMessages.formatNumber(this.numberOfKeyword));
		}
	}

	@Override
	public String getComputingStartMessage() {
		return TXMCoreMessages.bind(CooccurrenceCoreMessages.cooccurrentsOfP0PropertieP1InTheP2Corpus, (this.pQuery != null ? this.pQuery.asString() : CooccurrenceCoreMessages.NoQuery),
				WordProperty.asString(this.pProperties),
				(this.pMaxLeftContextSize - 1), (this.pMaxRightContextSize - 1), this.pFminFilter, this.pFCoocFilter, this.pScoreMinFilter, this.getCorpus().getName());
	}


	/**
	 * Gets the corpus.
	 *
	 * @return the corpus
	 */
	public CQPCorpus getCorpus() {
		return (CQPCorpus) this.getParent();
	}


	@Override
	public String getDetails() {
		Object[] params = new Object[] { this.getParent(), this.pQuery, this.pProperties, this.pStructuralUnitLimit, (this.pMinLeftContextSize - 1), (this.pMaxLeftContextSize - 1),
				(this.pMinRightContextSize - 1),
				(this.pMaxRightContextSize - 1), this.pFminFilter, this.pFCoocFilter, this.pScoreMinFilter };
		return NLS.bind(CooccurrenceCoreMessages.info_details, params);
	}


	/**
	 * Gets the fA.
	 *
	 * @return the fA
	 */
	public int getFA() {
		return this.FA;
	}

	public boolean getIncludeXPivot() {
		return pIncludeXpivot;
	}

	/**
	 * Gets the lines.
	 *
	 * @return the lines
	 */
	public List<CLine> getLines() {
		return lines;
	}

	/**
	 * Gets the max left.
	 *
	 * @return the max left
	 */
	public int getMaxLeft() {
		return pMaxLeftContextSize;
	}

	/**
	 * Gets the max right.
	 *
	 * @return the max right
	 */
	public int getMaxRight() {
		return pMaxRightContextSize;
	}

	/**
	 * Gets the min left.
	 *
	 * @return the min left
	 */
	public int getMinLeft() {
		return pMinLeftContextSize;
	}

	/**
	 * Gets the min right.
	 *
	 * @return the min right
	 */
	public int getMinRight() {
		return pMinRightContextSize;
	}

	@Override
	public String getName() {
		try {
			return this.getParent().getName() + TXMPreferences.PARENT_NAME_SEPARATOR + this.getSimpleName();
		}
		catch (Exception e) {
			return this.getSimpleName();
		}
	}

	/**
	 * Gets the lines.
	 *
	 * @return the lines
	 */
	public int getNumberOfCooccurrents() {
		if (numberOfCooccurrents == -1) {
			numberOfCooccurrents = 0;
			for (CLine line : lines) {
				numberOfCooccurrents += line.nbocc;
			}
		}
		return numberOfCooccurrents;
	}

	/**
	 * Gets the lines.
	 *
	 * @return the lines
	 */
	public int getNumberOfDifferentCooccurrents() {
		if (lines != null) {
			return lines.size();
		}
		return 0;
	}

	/**
	 * Gets the number of keyword.
	 *
	 * @return the number of keyword
	 */
	public int getNumberOfKeyword() {
		return numberOfKeyword;
	}

	/**
	 * Gets the p.
	 *
	 * @return the p
	 */
	public int getP() {
		return P;
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public List<WordProperty> getProperties() {
		return pProperties;
	}

	/**
	 * Gets the query.
	 *
	 * @return the query
	 */
	@Override
	public CQLQuery getQuery() {
		return pQuery;
	}


	@Override
	public String getResultType() {
		return CooccurrenceCoreMessages.RESULT_TYPE;
	}

	@Override
	public String getSimpleName() {
		if (this.pQuery != null && !this.pQuery.isEmpty()) {
			StringBuffer output = new StringBuffer();
			output.append(this.pQuery.asString());
			output.append(WordProperty.asString(this.pProperties));

			if (this.pMaxLeftContextSize > 0 && this.pMaxRightContextSize > 0) {
				output.append(" " + (this.pMaxLeftContextSize - 1) + " " + (this.pMaxRightContextSize - 1)); //$NON-NLS-1$ //$NON-NLS-2$
			}
			output.append(TXMCoreMessages.formatMinFilter(this.pFminFilter));
			output.append(TXMCoreMessages.formatMinFilter(this.pFCoocFilter));
			output.append(TXMCoreMessages.formatMinFilter(this.pScoreMinFilter));

			// TODO: SJ: improve the hiding or display of value according to the default preferences values
			// output.append(TXMCoreMessages.formatMinFilter(this.pFminFilter, CooccurrencePreferences.getInstance().getInt(CooccurrencePreferences.F_MIN) + 1));
			// output.append(TXMCoreMessages.formatMinFilter(this.pFCoocFilter, CooccurrencePreferences.getInstance().getInt(CooccurrencePreferences.MIN_COUNT) + 1));
			// output.append(TXMCoreMessages.formatMinFilter(this.pScoreMinFilter, CooccurrencePreferences.getInstance().getDouble(CooccurrencePreferences.MIN_SCORE) + 1));


			return output.toString();
		}
		else {
			return this.getEmptyName();
		}
	}

	/**
	 * Gets the structural unit limit.
	 *
	 * @return the structure
	 */
	public StructuralUnit getStructuralUnitLimit() {
		return this.pStructuralUnitLimit;
	}

	/**
	 * Gets the symbol.
	 *
	 * @return the symbol
	 */
	public String getSymbol() {
		return this.symbol;
	}

	/**
	 * Inits the conc infos.
	 *
	 * @param conc the conc
	 * @return true, if successful
	 */
	private boolean initConcInfos(Concordance conc) {

		int from = 0;
		int to = conc.getNLines() - 1;
		return initConcInfos(conc, from, to);
	}

	/**
	 * Inits the conc infos.
	 *
	 * @param conc the conc
	 * @param from the from
	 * @param to the to
	 * @return true, if successful
	 */
	private boolean initConcInfos(Concordance conc, int from, int to) {

		try {
			this.concordance = conc;
			conclines = conc.getLines(from, to);
			return initConcInfos(conc, conclines);
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return false;
	}

	/**
	 * Inits the conc infos.
	 *
	 * @param conc the conc
	 * @param conclines the conclines
	 * @return true, if successful
	 */
	private boolean initConcInfos(Concordance conc, List<Line> conclines) {

		try {
			CQPCorpus corpus = this.getCorpus();
			this.concordance = conc;
			corpus = conc.getCorpus();
			this.P = corpus.getSize() / (conc.getLeftContextSize() + conc.getRightContextSize());
			this.FA = conc.getNLines();
			corpus = conc.getCorpus();
			pQuery = new CQLQuery(conc.getQuery().getQueryString()); // TODO manage all kind of query
			// System.out.println("CONC PROPS :"+conc.getViewProperties());
			pProperties = conc.getKeywordViewProperties();
			pStructuralUnitLimit = null;
			pMaxLeftContextSize = conc.getLeftContextSize();
			pMaxRightContextSize = conc.getRightContextSize();
			this.conclines = conclines;
			return true;
		}
		catch (CqiClientException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return false;
	}

	@Override
	public boolean loadParameters() throws CqiClientException {
		pProperties = (List<WordProperty>) Property.stringToProperties(getCorpus(), this.getStringParameterValue(TXMPreferences.UNIT_PROPERTIES));
		//pQuery = new CQLQuery(this.getStringParameterValue(TXMPreferences.QUERY));
		this.pQuery = (CQLQuery) Query.stringToQuery(this.getStringParameterValue(TXMPreferences.QUERY));
		pStructuralUnitLimit = this.getCorpus().getStructuralUnit(this.getStringParameterValue(CooccurrencePreferences.STRUCTURAL_UNIT_LIMIT));
		return true;
	}

	/**
	 * Prints the.
	 */
	public void print() {
		// System.out.println("Lines "+lines.size());
		System.out.println("FA : " + FA); //$NON-NLS-1$
		System.out.println("P : " + P); //$NON-NLS-1$
		System.out.println(CooccurrenceCoreMessages.occFreqCoFreqScoreMeanDistMode);
		for (CLine line : lines)
			System.out.println(line.resume("\t", "")); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Override
	public boolean saveParameters() {
		this.saveParameter(TXMPreferences.UNIT_PROPERTIES, Property.propertiesToString(this.pProperties));

		if (pQuery != null) {
			this.saveParameter(TXMPreferences.QUERY, pQuery.getQueryString());
		}

		if (pStructuralUnitLimit != null) {
			this.saveParameter(CooccurrencePreferences.STRUCTURAL_UNIT_LIMIT, this.pStructuralUnitLimit.getName());
		}

		return true;
	}

	public void setCoocQuery(String q) {
		pCooccurentQueryFilter = q;
	}

	public void setIncludeXpivot(boolean b) {
		pIncludeXpivot = b;
	}

	/**
	 * Sets the max left.
	 *
	 * @param maxleft the new max left
	 */
	public void setMaxLeft(int maxleft) {
		this.pMaxLeftContextSize = maxleft;
	}

	/**
	 * Sets the max right.
	 *
	 * @param maxright the new max right
	 */
	public void setMaxRight(int maxright) {
		this.pMaxRightContextSize = maxright;
	}

	/**
	 * Sets the min left.
	 *
	 * @param minleft the new min left
	 */
	public void setMinLeft(int minleft) {
		this.pMinLeftContextSize = minleft;
	}

	/**
	 * Sets the min right.
	 *
	 * @param minright the new min right
	 */
	public void setMinRight(int minright) {
		this.pMinRightContextSize = minright;
	}

	public void setParameters(CQLQuery query, List<WordProperty> properties, StructuralUnit limit, int maxLeft, int minLeft, int minRight,
			int maxRight, int minFreq, float minScore, int minCof, boolean includeXpivot, boolean buildLexicalTableWithCooccurrents) {

		this.pQuery = query;
		this.pProperties = properties;
		this.pStructuralUnitLimit = limit;
		this.pMinLeftContextSize = minLeft;
		this.pMaxLeftContextSize = maxLeft;
		this.pMinRightContextSize = minRight;
		this.pMaxRightContextSize = maxRight;
		this.pFminFilter = minFreq;
		this.pScoreMinFilter = minScore;
		this.pFCoocFilter = minCof;
		this.pIncludeXpivot = includeXpivot;
		this.buildLexicalTableWithCooccurrents = buildLexicalTableWithCooccurrents;
	}

	public boolean setParametersFromWidgets(TXMParameters parameters) {
		try {
			CQPCorpus corpus = this.getCorpus();
			boolean includeXpivot = parameters.getBoolean(CooccurrencePreferences.INCLUDE_X_PIVOT);

			String queryString = ""; //$NON-NLS-1$
			if (parameters.get(TXMPreferences.QUERY) != null) {
				queryString = parameters.get(TXMPreferences.QUERY).toString();
			}
			CQLQuery query = new CQLQuery(queryString);

			StructuralUnit limit = (StructuralUnit) parameters.get(CooccurrencePreferences.STRUCTURAL_UNIT_LIMIT);

			Object propsParam = parameters.get(CooccurrencePreferences.UNIT_PROPERTIES);
			List<WordProperty> properties = null;
			if (propsParam instanceof List<?>) {
				properties = (List<WordProperty>) propsParam;
			}
			else if (propsParam instanceof String) {
				properties = (List<WordProperty>) Property.stringToProperties(corpus, propsParam.toString());
			}

			int maxLeft = parameters.getInteger(CooccurrencePreferences.MAX_LEFT);
			int minLeft = parameters.getInteger(CooccurrencePreferences.MIN_LEFT);
			int maxRight = parameters.getInteger(CooccurrencePreferences.MAX_RIGHT);
			int minRight = parameters.getInteger(CooccurrencePreferences.MIN_RIGHT);
			float minScore = parameters.getFloat(CooccurrencePreferences.MIN_SCORE);
			int minCof = parameters.getInteger(CooccurrencePreferences.MIN_COUNT);
			int minFreq = parameters.getInteger(TXMPreferences.F_MIN);
			boolean buildLexicalTableWithCooccurrents = parameters.getBoolean(CooccurrencePreferences.PARTIAL_LEXICAL_TABLE);

			this.setParameters(query, properties, limit, maxLeft, minLeft, minRight, maxRight, minFreq, minScore, minCof, includeXpivot, buildLexicalTableWithCooccurrents);

		}
		catch (Exception e) {
			System.out.println(CooccurrenceCoreMessages.ErrorWhileSettingCooccurrenceParameters + e.getLocalizedMessage());
			Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	/**
	 * Sets the query.
	 * 
	 * @param query
	 */
	public void setQuery(CQLQuery query) {
		this.pQuery = query;
	}

	public void setReferenceCorpus(String symbol) {
		referenceCorpus = symbol;
	}

	/**
	 * Sets the structural unit limit.
	 *
	 * @param su the new structure
	 */
	public void setStructuralUnitLimt(StructuralUnit su) {
		pStructuralUnitLimit = su;
	}

	/**
	 * Sets the thresfold.
	 *
	 * @param freq the freq
	 * @param count the count
	 * @param score the score
	 */
	public void setThresfold(int freq, int count, float score) {

		pFminFilter = freq;
		pFCoocFilter = count;
		pScoreMinFilter = score;
	}

	/**
	 * Sort.
	 *
	 * @param comparator the comparator
	 * @throws CqiClientException the cqi client exception
	 */
	public void sort(CLineComparator comparator) throws CqiClientException {
		CQPCorpus corpus = this.getCorpus();
		comparator.initialize(corpus);
		if (lines.size() > 0) {
			Collections.sort(lines, comparator);
		}
	}

	/**
	 * Step build lexical table.
	 *
	 * @return true, if successful
	 * @throws RWorkspaceException the r workspace exception
	 */
	public boolean stepBuildLexicalTable(TXMProgressMonitor monitor) throws RWorkspaceException {

		CQPCorpus corpus = this.getCorpus();
		String[] colnames = { corpus.getName() + "-" + pQuery.getQueryString(), pQuery.getQueryString() }; //$NON-NLS-1$
		keysToString = new HashMap<>();

		// time = System.currentTimeMillis();
		for (TXMResult rez : corpus.getChildren(Index.class)) { // TODO: fix usages of index for cooc
			Index rezvoc = (Index) rez;

			if (rezvoc.getProperties().equals(pProperties)) {
				if (rezvoc.getQuery() != null && rezvoc.getQuery().equals(new CQLQuery(pCooccurentQueryFilter))) {
					if (rezvoc.getFilterFmax() == null) {
						index = rezvoc;
						break;
					}
				}
			}
		}

		// TODO: MD: implement a sibling dependency system
		if (index == null) {
			try {
				index = new Index(corpus);
				index.setVisible(false);
				index.setParameters(new CQLQuery(pCooccurentQueryFilter), pProperties, null, null, null, null);
				if (!index.compute(new LogMonitor())) {
					Log.severe(CooccurrenceCoreMessages.ErrorWhileComputingIndexForTheCooccurrenceAborting);
					return false;
				}
			}
			catch (Exception e) {
				Log.severe(NLS.bind(CooccurrenceCoreMessages.ErrorWhileComputingIndexForTheCooccurrenceP0, e.getLocalizedMessage()));
				return false;
			}
		}

		// ALTER THE INDEX IF A REFERENCE CORPUS IS SET -> this change the base frequencies
		if (referenceCorpus != null && referenceCorpus.length() > 0) {
			// voc.toTxt(new File("/home/mdecorde/TEMP/before.tsv"), "UTF-8", "\t", "");
			try {
				index.alterFrequencies(referenceCorpus);
			}
			catch (Exception e) {
				org.txm.utils.logger.Log.printStackTrace(e);
				return false;
			}
		}

		List<org.txm.index.core.functions.Line> vocLines = index.getAllLines();
		int[][] freqs;
		String[] rownames;
		if (buildLexicalTableWithCooccurrents) {
			freqs = new int[counts.keySet().size()][2];
			rownames = new String[counts.keySet().size()];
		}
		else { // all words
			freqs = new int[vocLines.size()][2];
			rownames = new String[vocLines.size()];
		}

		int i = 0;
		// System.out.println("T voc : "+(System.currentTimeMillis()- time)); //$NON-NLS-1$
		// System.out.println("nb lines voc "+voclines.size());
		// System.out.println("counts keys: "+counts.keySet());
		for (org.txm.index.core.functions.Line l : vocLines) {
			// System.out.println("L sign '"+l.getSignature()+"'");
			if (counts.keySet().contains(l.getSignature())) {
				keysToString.put(l.toString(), l.getSignature());
				rownames[i] = l.toString();
				// System.out.println("set rowname: "+l.toString());
				// System.out.println("("+l.getSignature()+", "+l.toString()+") : "+l.getFrequency()+" - "+counts.get(l.getSignature()));
				int count = counts.get(l.getSignature());
				int tot = l.getFrequency();
				indexfreqs.put(l.toString(), tot);

				freqs[i][0] = tot - count;
				freqs[i][1] = count;
				i++;
			}
			else if (!buildLexicalTableWithCooccurrents) {
				keysToString.put(l.toString(), l.getSignature());
				rownames[i] = l.toString();
				// System.out.println("set rowname: "+l.toString());
				// System.out.println("("+l.getSignature()+", "+l.toString()+") : "+l.getFrequency()+" - "+counts.get(l.getSignature()));

				int tot = l.getFrequency();
				indexfreqs.put(l.toString(), tot);

				freqs[i][0] = tot;
				freqs[i][1] = 0;
				i++;
			}
		}
		index.delete(); // no more needed
		index = null;

		// time = System.currentTimeMillis();
		if (freqs.length == 0) {
			System.out.println(CooccurrenceCoreMessages.errorColonNoCooccurrents);
		}
		// try {
		// PrintWriter writer = IOUtils.getWriter("/home/mdecorde/test_cooc.txt");
		// //writer.println("Build LT: ");
		// //writer.println("freqs: "+Arrays.toString(freqs));
		// //writer.println("Rows: "+);
		// //String rows = Arrays.toString();
		// int nrow = rownames.length;
		// int ncol = colnames.length;
		// for (int ii = 0 ; ii < nrow ; ii++) {
		// writer.write(rownames[ii]);
		// for (int j = 0 ; j < ncol ; j++) {
		// writer.write("\t"+freqs[ii][j]);
		// }
		// writer.write("\n");
		// }
		// writer.close();
		// //writer.println("Cols: "+Arrays.toString(colnames));
		// } catch(Exception e) {e.printStackTrace();}

		lt = new LexicalTableImpl(freqs, rownames, colnames);


		// if(referenceCorpus != null && referenceCorpus.length() > 0) {
		// //lt.removeCol(0, false);
		// lt.setReference(referenceCorpus);
		// lt.exchangeColumns(1,2);
		// }
		return true;
	}

	/**
	 * Step build signatures.
	 *
	 * @return true, if successful
	 * @throws UnexpectedAnswerException the unexpected answer exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	public boolean stepBuildSignatures() throws UnexpectedAnswerException, IOException, CqiServerError {
		allsignaturesstr = new HashMap<>();
		Set<Integer> allpositions = new HashSet<>(); // no duplicates
		for (Match n : m2) {
			for (int i = n.getStart(); i <= n.getEnd(); i++) {
				allpositions.add(i);
			}
		}
		// System.out.println("Position set: "+allpositions.size());

		int[] allpositionsarray = new int[allpositions.size()];
		int pcount = 0;
		for (int p : allpositions) {
			allpositionsarray[pcount++] = p;
		}

		HashMap<Property, int[]> propsId = new HashMap<>();
		// HashMap<Property, String[]> propsValues = new HashMap<Property, String[]>();
		for (WordProperty property : pProperties) {
			int[] indices = property.cpos2Id(allpositionsarray);
			//String[] values = CorpusManager.getCorpusManager().getCqiClient().cpos2Str(property.getQualifiedName(), allpositionsarray);
			propsId.put(property, indices);
		}

		// System.out.println("T values + ids: "+(System.currentTimeMillis()- time)); //$NON-NLS-1$

		pcount = 0;
		for (int position : allpositionsarray) {
			// String sign = ""; //$NON-NLS-1$
			String signstr = ""; //$NON-NLS-1$
			for (Property property : pProperties) {
				signstr += "[" + propsId.get(property)[pcount] + "]"; //$NON-NLS-1$ //$NON-NLS-2$
				// signstr+="["+propsValues.get(property)[pcount]+"]"; //$NON-NLS-1$ //$NON-NLS-2$
			}
			// allsignatures.put(position, sign);
			allsignaturesstr.put(position, signstr);
			pcount++;

		}
		if (debug) System.out.println(allsignaturesstr);
		return true;
	}

	/**
	 * Step count.
	 *
	 * @return true, if successful
	 */
	public boolean stepCount() {
		// ArrayList<Integer> keepedPosition = new ArrayList<>();
		//PrintWriter debugWriter = new PrintWriter(new File("/home/mdecorde/debugcooc.txt"));
		int startsearchM2 = 0; // optimisation: m2 is ordered
		int startsearchM3 = 0; // optimisation: m3 is ordered
		// time = System.currentTimeMillis();

		HashMap<Integer, Integer> positionsDistances = new HashMap<>();
		//int[] positionsDistances = new ArrayList()
		for (Match m : m1) { // for each match = for each focus

			if (debug) System.out.println("m=" + m); //$NON-NLS-1$
			if (m.getTarget() >= 0) { // if target is set focus on target position
				m.setStart(m.getTarget());
				m.setEnd(m.getTarget());
			}
			// System.out.println("for match m: "+m);
			Match n = null; // the match which contains the context
			Match o = null; // the match which does not contain the context
			boolean matchFound = false;
			for (int i = startsearchM2; i < m2.size(); i++) { // find n
				n = m2.get(i);
				//if (n.getTarget() == m.getStart()) {
				if (n.getStart() <= m.getStart() && m.getEnd() <= n.getEnd()) {
					startsearchM2 = i;
					matchFound = true;
					break;
				}
			}
			if (Thread.interrupted()) return false; // stop if interrupted by user
			// System.out.println("found n: "+n);

			for (int i = startsearchM3; i < m3.size(); i++) { // find next match m3 contained by m2

				o = m3.get(i);
				//if (o.getTarget() == m.getStart()) {
				if (o.getStart() <= m.getStart() && m.getEnd() <= o.getEnd()) {
					startsearchM3 = i;
					matchFound = matchFound && true;
					break;
				}
			}
			// System.out.println("found o: "+o);

			if (!matchFound) {
				continue;
			}

			//			if (debug) System.out.println("n="+n);
			//			if (debug) System.out.println("o="+o);
			int start = n.getStart();
			int size = n.getEnd() - start + 1;
			// if (size > 0)
			// size++;
			// System.out.println("Process focus:"+m+" with maxcontext:"+n+" and anticontext:"+o);
			// System.out.println("NbOccs "+(size));
			//int[] positions = new int[size];
			int noOcc = 0;

			// System.out.println("positions");
			// System.out.println("start: "+(start)+" end:"+n.getEnd());
			for (int position = start; position <= n.getEnd(); position++) {

				if (o.getStart() <= position && position <= o.getEnd()) {
					// ignore positions in the anticontext positions
					continue;
				}

				//				if (allsignaturesstr.get(position).equals("[1304]") || position == 13313) {
				//					int a = 1+1;
				//				}

				int dist;
				if (position < m.getStart()) {
					dist = m.getStart() - position - 1;
				}
				else if (m.getEnd() < position) {
					dist = position - m.getEnd() - 1;
				}
				else { // the n match is in the m match !?
					Log.warning("Warning: the n match is in the m match ? " + n + " " + m); //$NON-NLS-1$ //$NON-NLS-2$
					dist = 0;
				}

				if (debug) System.out.println(" p=" + position + " sign=" + allsignaturesstr.get(position) + " dist=" + dist); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				if (positionsDistances.containsKey(position) && positionsDistances.get(position) > dist) {
					positionsDistances.put(position, dist);
					//					if (debug) System.out.println(" using the higher distance");
				}
				else if (!positionsDistances.containsKey(position)) {
					positionsDistances.put(position, dist);
					//					if (debug) System.out.println(" add");
				}

			}

			// System.out.println("nb Occ ignored: "+ignore);
			// System.out.println("nb Occ chevauche: "+chevauche);
		}

		// store and count distances for each position signature
		int noOcc = 0;
		for (int position : positionsDistances.keySet()) { // cooccurrent words positions
			// String signature = allsignatures.get(position);
			String signaturestr = allsignaturesstr.get(position);

			Integer dist = positionsDistances.get(position);
			if (distances.containsKey(signaturestr)) {
				distances.put(signaturestr, (distances.get(signaturestr)) + dist);
			}
			else {
				distances.put(signaturestr, (double) dist);
			}

			Integer count = (counts.get(signaturestr));
			if (counts.containsKey(signaturestr)) {
				counts.put(signaturestr, count + 1);
			}
			else {
				counts.put(signaturestr, 1);
			}

			//			if (allsignaturesstr.get(position).equals("[1304]")) {
			//				System.out.println("dist= "+dist+" distances="+distances.get(signaturestr)+" counts="+counts.get(signaturestr));
			//			}

			// if ("[1599]".equals(signaturestr)) {
			//	System.out.println("sign="+allsignaturesstr.get(position)+" p=" + position + " c=" + counts.get(signaturestr) + " d=" + dist + " total(d)=" + distances.get(signaturestr)); 
			// }
			// }

			noOcc++;
		}
		// System.out.println("T counts : "+(System.currentTimeMillis()- time)); //$NON-NLS-1$

		allsignaturesstr = null; // no more need
		// counted = null;
		return true;
	}

	/**
	 * Step get matches.
	 *
	 * @return true, if successful
	 * @throws CqiClientException
	 */
	public boolean stepGetMatches() throws CqiClientException {

		CQPCorpus corpus = this.getCorpus();
		QueryResult r1 = corpus.query(fixedQuery, "CoocFocusQuery", false); // keywords positions //$NON-NLS-1$
		QueryResult r2 = null;
		if (contextQuery.equals(fixedQuery)) {
			r2 = r1;
		}
		else {
			r2 = corpus.query(contextQuery, "CoocContextFocusQuery", false); // max context //$NON-NLS-1$
		}
		QueryResult r3 = null;
		if (anticontextquery.equals(fixedQuery)) {
			r3 = r1;
		}
		else if (contextQuery.equals(anticontextquery)) {
			r3 = r2;
		}
		else {
			r3 = corpus.query(anticontextquery, "CoocAntiContextFocusQuery", false); // no context //$NON-NLS-1$
		}

		m1 = r1.getMatches();
		numberOfKeyword = m1.size();
		m2 = r2.getMatches();
		m3 = r3.getMatches();

		if (debug) System.out.println(fixedQuery);
		if (debug) System.out.println(m1);
		if (debug) System.out.println(contextQuery);
		if (debug) System.out.println(m2);
		if (debug) System.out.println(anticontextquery);
		if (debug) System.out.println(m3);

		Log.finest("R1 size=" + m1.size()); //$NON-NLS-1$
		Log.finest("R2 size=" + m2.size()); //$NON-NLS-1$
		Log.finest("R3 size=" + m3.size()); //$NON-NLS-1$

		// System.out.println(query+" M1 size: "+m1.size());
		// System.out.println(contextquery+" M2 size: "+m2.size());
		// System.out.println(anticontextquery+" M3 size: "+m3.size());
		r1.drop();

		if (r2 != r1) {
			r2.drop();
		}
		if (r3 != r1 && r2 != r3) {
			r3.drop();
		}
		return true;
	}

	/**
	 * Step get scores.
	 *
	 * @return true, if successful
	 * @throws CqiClientException the cqi client exception
	 * @throws StatException the stat exception
	 */
	public boolean stepGetScores() throws CqiClientException, StatException {

		SpecificitiesR specif = new SpecificitiesR(lt);
		// System.out.println("Specif N part: "+specif.getNbrPart()); //$NON-NLS-1$
		// System.out.println("Specif N lines number: "+specif.getSpecificitesIndices().length); //$NON-NLS-1$
		// System.out.println("T specif e: "+(System.currentTimeMillis()- time)); //$NON-NLS-1$
		// specif.toTxt(new File("~/Bureau/coocresults/specif Cooc")); //$NON-NLS-1$
		String[] specifrownames = specif.getRowNames().asStringsArray();
		double[][] scores = specif.getScores();
		// System.out.println("Nb specif result: "+specif.getSpecificitesIndices().length);

		int iimax = Math.min(specifrownames.length, scores.length);
		for (int ii = 0; ii < iimax; ii++) { // counts.keySet())
			Object signaturestr = keysToString.get(specifrownames[ii]);

			ArrayList<String> props = new ArrayList<>();
			if (pProperties.size() > 1) {
				String[] splited = specifrownames[ii].split("_", pProperties.size()); //$NON-NLS-1$

				for (int p = 0; p < pProperties.size(); p++) {
					props.add(splited[p]);
				}
			}
			else {
				props.add(specifrownames[ii]);
			}
			// if(specifrownames[ii].equals("(") || specifrownames[ii].equals(")"))
			// {
			// System.out.println("rowname: "+specifrownames[ii]);
			// System.out.println("props: "+props);
			// System.out.println("counts: "+counts.get(signaturestr));
			// System.out.println("speciffreq: "+indexfreqs.get(specifrownames[ii]));
			// System.out.println("specif score: "+scores[ii][1]);
			// System.out.println("distance: "+distances.get(signaturestr));
			// System.out.println("distance count: "+distancescounts.get(signaturestr));
			// }
			if (counts.containsKey(signaturestr)) {
				CLine cline = new CLine(this, specifrownames[ii], props,
						counts.get(signaturestr), // cofreq
						indexfreqs.get(specifrownames[ii]), scores[ii][1], // freq
						((float) (distances.get(signaturestr) / counts.get(signaturestr))), // mean distance
						-1);
				cline.debug = " " + signaturestr; //$NON-NLS-1$
				// System.out.println("Line: "+specifrownames[ii]+" dists="+distances.get(signaturestr)+" counts="+counts.get(signaturestr)+" mean="+((float) (distances.get(signaturestr) / counts.get(signaturestr))));
				// select the line
				if (cline.freq >= this.pFminFilter && cline.nbocc >= this.pFCoocFilter && cline.score >= this.pScoreMinFilter) {
					if (cline.score >= Integer.MAX_VALUE - 5) {
						cline.score = Float.MAX_EXPONENT;
					}
					lines.add(cline);
				}
			}
			// System.out.println(signaturestr+"\t"+voclines.get(ii).getFrequency()+"\t"+counts.get(signaturestr)+"\t"+(distances.get(signaturestr)/distancescounts.get(signaturestr))+"\t"+scores[ii][1]);
			// //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		}
		return true;
	}

	/**
	 * Step query limits build the context, anticontext and keyword queries using the context size and xincludepivot and structuralLimit parameters
	 *
	 * @return true, if successful
	 */
	public boolean stepQueryLimits() {
		// structural context

		String sfixedQuery = CQLQuery.fixQuery(pQuery.getQueryString(), this.getCorpus().getLang()); // a query starting with " or [ or (
		if (sfixedQuery.contains("@[") || sfixedQuery.contains("@\"")) { //$NON-NLS-1$ //$NON-NLS-2$

		}
		else if (sfixedQuery.startsWith("(")) { //$NON-NLS-1$
			sfixedQuery = "(@" + sfixedQuery.substring(1); //$NON-NLS-1$
		}
		else if (sfixedQuery.startsWith("((")) { //$NON-NLS-1$
			sfixedQuery = "((@" + sfixedQuery.substring(2); //$NON-NLS-1$
		}
		else {
			sfixedQuery = "@" + sfixedQuery; //$NON-NLS-1$
		}

		if (pStructuralUnitLimit != null) {
			String tempquery = ""; //$NON-NLS-1$
			String lname = pStructuralUnitLimit.getName();

			// test if there is a left context
			if (pMinLeftContextSize > 0) {
				tempquery += "(<" + lname + ">[]* </" + lname + ">){" + (pMaxLeftContextSize) + "," + (pMaxLeftContextSize) + "}"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			}
			// (<p>[]*</p>){0, 50} "je" (<p>[]*</p>){0, 50}
			tempquery += " <" + lname + ">[]* " + sfixedQuery + " []* </" + lname + "> "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

			// test if there is a right context
			if (pMinRightContextSize > 0) {
				tempquery += "(<" + lname + ">[]* </" + lname + ">){" + (pMaxRightContextSize) + "," + (pMaxRightContextSize) + "}";  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			}
			this.contextQuery = new CQLQuery(tempquery);

			if (pIncludeXpivot) {
				String anticontextquerystring = ""; //$NON-NLS-1$
				// minleft = 2..N
				if (pMinLeftContextSize > 1) {
					anticontextquerystring += "(<" + lname + ">[]* </" + lname + ">){" + (pMinLeftContextSize - 1) + "," + (pMinLeftContextSize - 1) + "} <" + lname + ">[]* "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
				}
				anticontextquerystring += sfixedQuery;
				// minright = 2..N
				if (pMinRightContextSize > 1) {
					anticontextquerystring += " []* </" + lname + "> (<" + lname + ">[]* </" + lname + ">){" + (pMinRightContextSize - 1) + "," + (pMinRightContextSize - 1) + "}"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
				}
				this.anticontextquery = new CQLQuery(anticontextquerystring);
			}
			else {
				String anticontextquerystring = ""; //$NON-NLS-1$
				// minleft = 2..N
				if (pMinLeftContextSize > 0) {
					anticontextquerystring += "(<" + lname + ">[]* </" + lname + ">){" + (pMinLeftContextSize - 1) + "," + (pMinLeftContextSize - 1) + "} <" + lname + ">[]* "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
				}
				anticontextquerystring += sfixedQuery;
				// minright = 2..N
				if (pMinRightContextSize > 0) {
					anticontextquerystring += " []* </" + lname + "> (<" + lname + ">[]* </" + lname + ">){" + (pMinRightContextSize - 1) + "," + (pMinRightContextSize - 1) + "}"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
				}
				this.anticontextquery = new CQLQuery(anticontextquerystring);
			}
		}
		else { // word context

			String tempquery = ""; //$NON-NLS-1$
			if (pMinLeftContextSize > 0) { // test if there is a left context
				tempquery += "[]{" + pMaxLeftContextSize + "," + pMaxLeftContextSize + "} "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			tempquery += sfixedQuery;

			if (pMinRightContextSize > 0) { // test if there is a right context
				tempquery += " []{" + pMaxRightContextSize + "," + pMaxRightContextSize + "} "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			this.contextQuery = new CQLQuery(tempquery);
			String anticontextquerystring = ""; //$NON-NLS-1$
			if (pMinLeftContextSize > 1) {
				anticontextquerystring += "[]{" + (pMinLeftContextSize - 1) + ", " + (pMinLeftContextSize - 1) + "}"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			anticontextquerystring += sfixedQuery;
			if (pMinRightContextSize > 1) {
				anticontextquerystring += "[]{" + (pMinRightContextSize - 1) + "," + (pMinRightContextSize - 1) + "} "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			this.anticontextquery = new CQLQuery(anticontextquerystring);
		}

		fixedQuery = new CQLQuery(sfixedQuery);
		//fixedQuery = pQuery;
		return true;
	}

	/**
	 * To txt.
	 *
	 * @param writer the writer
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @return true, if successful
	 */
	public boolean toTxt(Writer writer, String colseparator, String txtseparator) {
		try {
			// Occ Freq CoFreq Score MeanDist
			writer.write(
					txtseparator + CooccurrenceCoreMessages.occ + txtseparator + colseparator +
							txtseparator + CooccurrenceCoreMessages.freq + txtseparator + colseparator +
							txtseparator + CooccurrenceCoreMessages.coFreq + txtseparator + colseparator +
							txtseparator + CooccurrenceCoreMessages.score + txtseparator + colseparator +
							txtseparator + CooccurrenceCoreMessages.meanDist + txtseparator + "\n");// colseparator+ //$NON-NLS-1$
			for (CLine line : lines)
				writer.write(line.resume(colseparator, txtseparator) + "\n"); //$NON-NLS-1$
			writer.close();
		}
		catch (IOException e) {
			Log.severe(TXMCoreMessages.bind(TXMCoreMessages.error_errorP0, e));
			return false;
		}

		return true;
	}


}
