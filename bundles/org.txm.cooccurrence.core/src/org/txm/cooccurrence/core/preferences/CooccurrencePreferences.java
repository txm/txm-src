package org.txm.cooccurrence.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Default preferences initializer.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class CooccurrencePreferences extends TXMPreferences {

	/** The Constant SCOREFORMAT. */
	public static final String SCORE_FORMAT = "score_format"; //$NON-NLS-1$

	public static final String STRUCTURAL_UNIT_LIMIT = "structural_unit_limit"; //$NON-NLS-1$

	/** The Constant MINCOUNT. */
	public static final String MIN_COUNT = "min_count"; //$NON-NLS-1$

	/** The Constant MINSCORE. */
	public static final String MIN_SCORE = "min_score"; //$NON-NLS-1$

	public static final String MAX_LEFT = "max_left"; //$NON-NLS-1$

	public static final String MIN_LEFT = "min_left"; //$NON-NLS-1$

	public static final String MIN_RIGHT = "min_right"; //$NON-NLS-1$

	public static final String MAX_RIGHT = "max_right"; //$NON-NLS-1$

	public static final String INCLUDE_X_PIVOT = "include_x_pivot"; //$NON-NLS-1$

	public static final String PARTIAL_LEXICAL_TABLE = "partial_lexical_table"; //$NON-NLS-1$

	public static final String STRUCTURES_TO_IGNORE = "milestone_structures_to_ignore"; //$NON-NLS-1$


	public static final String QUERY_FILTER = "query_filter"; //$NON-NLS-1$


	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {

		if (!TXMPreferences.instances.containsKey(CooccurrencePreferences.class)) {

			new CooccurrencePreferences();
		}
		return TXMPreferences.instances.get(CooccurrencePreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {

		super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.put(SCORE_FORMAT, "E00"); //$NON-NLS-1$
		preferences.putInt(F_MIN, 2);
		preferences.putInt(MIN_COUNT, 2);
		preferences.putDouble(MIN_SCORE, 2.0);

		preferences.putInt(MAX_LEFT, 10);
		preferences.putInt(MIN_LEFT, 1);
		preferences.putInt(MIN_RIGHT, 1);
		preferences.putInt(MAX_RIGHT, 10);

		preferences.put(STRUCTURES_TO_IGNORE, "text,anchor,cb,fw,gb,lb,milestone,pb,txmcorpus"); //$NON-NLS-1$
		preferences.put(UNIT_PROPERTIES, TXMPreferences.DEFAULT_UNIT_PROPERTY);
	}

}
