package org.txm.ca.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * CA UI messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class CAUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.ca.rcp.messages.messages"; //$NON-NLS-1$

	public static String info;
	public static String axesColon;
	public static String axes;
	public static String canNotComputeCAOnLexicalTableWithLessThan4Columns;
	public static String canNotComputeCAWithLessThan4RowsInTheTable;
	public static String canNotComputeFactorialAnalysisWithOnlyP0PartsTheMinimumIs4Parts;
	public static String chi;
	public static String colsInfos;
	public static String contributionColumnFormat;
	public static String coordColumnFormat;
	public static String CorrespondanceAnalysisEditorInput_11;
	public static String CorrespondanceAnalysisEditorInput_4;
	public static String cosColumnFormat;
	public static String distanceColumnFormat;
	public static String eigenvalue;
	public static String eigenvalues;
	public static String eigenvaluesBarChat;
	public static String error_cannot_compute_withP0;
	public static String error_openingP0;
	public static String errorWhileOpeningCAEditorColonP0;
	public static String factPlan;
	public static String infosColonP0;
	public static String lexicalTableCreatedWhenCalledFromAPartition;
	public static String massColumnFormat;
	public static String openingTheCorrespondenceAnalysisResults;
	public static String q;
	public static String qualityColumnFormat;
	public static String rowsInfos;
	public static String showhidePointLabels;
	public static String showhidePointShapes;
	public static String showhideColumns;
	public static String showhideRows;
	public static String showPointLabels;
	public static String showPointShapes;
	public static String showColumns;
	public static String showRows;
	public static String SupplementaryColNames;
	public static String SupplementaryRowNames;

	public static String mustBeOpenedWithACASelectionAborting;

	public static String structuralUnitUsedToBuildTheParentLexicalTable;

	public static String currentStylingSheet;

	public static String showTheSingularValueInformations;

	public static String showTheRowInformations;

	public static String showTheColumnInformations;

	public static String filtering;

	public static String filterThePointsOfYourChart;

	public static String stylingSheet;

	public static String editTheStyleOfYourChart;

	public static String forPresentationAndSharePurposeSelectTheAxesToBeInverted;

	public static String invertedAxes;

	public static String copiedTextP0;

	public static String axesP0P1RowsP2P3ContXP4ContYP5ColumnsP6P7ContXP8ContYP9;

	public static String firstAxisDisplayed;
	public static String secondAxisDisplayed;

	public static String showAllAxisInformations;
	public static String displayTheCAPlanWindow;
	public static String showCoords;
	public static String groupInformations;

	public static String showTheSingularValuesChart;

	public static String activateDeactivateFiltersOfP0;
	public static String resetContributionThreasholdsOfP0To80;
	public static String resetContributionThreasholdsOfP0TotheMeanValue;

	public static String settingsOfP0P1;

	public static String thresholdOfP0P1;

	public static String deletingColumnsP0OfP1;
	public static String deletingRowsP0OfP1;
	public static String errorCannotEmptyTheLexicalTable;
	public static String nothingToDeleteAborting;
	public static String errorAtLeast3rowsAreNecessarytoComputeCA;

	public static String mergingColumnsP0OfP1;
	public static String mergingRowsP0OfP1;
	public static String nothingToMergeAborting;

	public static String numberOfFactors;

	
	public static String availableStylingSheets;
	public static String neww;
	public static String clone;
	public static String chevLess;
	public static String cloneCurrentStylingSheetTheNewOneWillBeEditableByDefault;
	public static String copyP0OfP1;
	public static String createANewStylingSheetCurrentOneWillBelostIfNotSaved;
	public static String delete;
	public static String deleteTheCurrentStylingSheet;
	public static String displayMoreStyleOptionsAndSelection;
	public static String elementOftheChartToStyleShapesOrLabelsOrBoth;
	public static String enableDisableAStylingRule;
	public static String errorStylingSheetNameAlreadyUsed;
	public static String errorStylingSheetNameIsEmpty;
	public static String export;
	public static String hideTheSupplmentaryOptionsAndSelection;
	public static String import3Dot;
	public static String importAStylingSheetFromTheJSONExportString;
	public static String jsonStringToSaveInAFile;
	public static String label;
	public static String moreChev;
	public static String NoP0SelectionInstructionInP1;
	public static String points;
	public static String printTheStylingSheetRulesDescriptionAndJSONStringCanBeUsedToReImportTheStylingSheet;
	public static String property;
	public static String putHereTheJSONStringOfTheExportedStylingSheet;
	public static String save;
	public static String saveCurrentStylingsheetModificationsChangeItsNameIfNecessary;
	public static String saveStylingSheet;
	public static String settings;
	public static String style;
	public static String stylingSheetName;
	public static String target;
	public static String threshold;
	public static String value;
	public static String creatingARuleWithSelectionP0;


	public static String invertTheP0Axis;


	public static String setTheP0SupplementaryColumnsOfP1;
	public static String setTheP0SupplementaryRowsOfP1;


	public static String showTheInterpretationHelpTools;
	public static String showTheContributionSumInTheFilterPanel;
	public static String showTheNumberOfFilteredItemsInTheFilterPanel;
	public static String showClassesInTheFilterPanel;
	public static String showTheInformationPanelsWhenOpeningTheCAEditor;


	public static String tools;
	public static String numberOfSavedCoordinates;

	public static String maximumNumberOfAxesToBeUsedInTheInterfaceAndCalculations;

	public static String defaultStylingSheetName;

	public static String minimumFrequencyThresholdCommaAllValuesBelowFminAreIgnored; // filtrage des lignes par fréquence minimale lors de l’appel sur une partition (10 par défaut)
	public static String listTruncationThresholdCommaOnlyTheMostFrequentFirstVmaxLinesAreRetained; // nombre de lignes maximum lors de l’appel sur une partition
	public static String numbeOfFactorsDisplayedEtc; // nombre de facteurs dont on pourra visualiser les aides à l'interprétation et qui seront utilisables pour une CAH
	public static String qualityColumnFormatEtc; // format d'affichage des valeurs de qualité de représentation dans le plan
	public static String contributionColumnFormatEtc; // format d'affichage des valeurs de contribution aux axes
	public static String massColumnFormatEtc; // format d'affichage des valeurs de masses
	public static String distanceColumnFormatEtc; // format d'affichage des valeurs de distance
	public static String cosColumnFormatEtc; // format d'affichage des valeurs de cos²
	public static String coordColumnFormatEtc; // format d'affichage des valeurs de coordonnées
	public static String columnFormats; // Format des colonnes
	

	static {
		Utf8NLS.initializeMessages(BUNDLE_NAME, CAUIMessages.class);
	}

}
