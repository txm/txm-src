package org.txm.ca.rcp.chartsengine.events;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Table;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.entity.XYItemEntity;
import org.txm.ca.core.chartsengine.base.CAChartCreator;
import org.txm.ca.rcp.editors.CAFactorialMapChartEditor;
import org.txm.ca.rcp.editors.ColsRowsInfosEditor;
import org.txm.chartsengine.rcp.events.EventCallBack;

/**
 * CA chart point selection call back shared by mouse and keyboard events.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class CAPointSelectionCallBack extends EventCallBack<CAFactorialMapChartEditor> {


	String rowFocus = null;

	String colFocus = null;

	@Override
	public void processEvent(Object event, int eventArea, Object o) {

		rowFocus = null;
		colFocus = null;

		// TODO MD: maybe the  getCAFactorialMapChartSelectedPoints() methods last selected point is the right one ? create abstract ChartMouseEvent, Entity to be compatible with the other ChartsEngines
		if (event instanceof MouseEvent mouseEvent && o instanceof ChartMouseEvent chartMouseEvent) {

			if (mouseEvent.getClickCount() == 2) {

				if (chartMouseEvent.getEntity() instanceof XYItemEntity xyEntity) {

					int series = xyEntity.getSeriesIndex();
					int item = xyEntity.getItem();
					if (series == 0) { // row
						try {
							rowFocus = chartEditor.getResult().getRowNames()[item];
							//							System.out.println("FOCUS ON ROW "+rowFocus);
						}
						catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					else { // col
						try {
							colFocus = chartEditor.getResult().getColNames()[item];
							//							System.out.println("FOCUS ON COL "+colFocus);
						}
						catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}

		if (eventArea == EventCallBack.AREA_ITEM || event instanceof KeyEvent) {

			final ColsRowsInfosEditor rowsInfoEditor = chartEditor.getRowsInfosEditor();
			final ColsRowsInfosEditor columnsInfoEditor = chartEditor.getColsInfosEditor();

			// Need to run this in the SWT UI thread because it's called from the AWT UI thread in TBX charts engine layer
			chartEditor.getComposite().getDisplay().asyncExec(new Runnable() {

				@Override
				public void run() {

					updateColsRowsInfoTableSelections(chartEditor, rowsInfoEditor, columnsInfoEditor, rowFocus, colFocus, null, null);
				}
			});
		}
	}

	/**
	 * Can be used to update or set the editors selection
	 * 
	 * @param rowsInfoEditor
	 * @param columnsInfoEditor
	 */
	public static void updateColsRowsInfoTableSelections(CAFactorialMapChartEditor chartEditor, 
			ColsRowsInfosEditor rowsInfoEditor, ColsRowsInfosEditor columnsInfoEditor, 
			String rowFocus, String colFocus,
			ArrayList<String> selectedRowLabels, ArrayList<String> selectedColumnLabels) {

		if (chartEditor == null || chartEditor.isDisposed()) return;

		if (selectedRowLabels == null) {
			selectedRowLabels = ((CAChartCreator) chartEditor.getResult().getChartCreator()).getCAFactorialMapChartSelectedPoints(chartEditor.getChart(), 0);
		}
		if (selectedColumnLabels == null) {
			selectedColumnLabels = ((CAChartCreator) chartEditor.getResult().getChartCreator()).getCAFactorialMapChartSelectedPoints(chartEditor.getChart(), 1);
		}

		if (rowFocus != null) {
			rowsInfoEditor = chartEditor.openVariablesEditor();
		} 

		if (colFocus != null) {
			columnsInfoEditor = chartEditor.openIndividualsEditor();
//			if (columnsInfoEditor == null) {
//				chartEditor.openIndividualsEditor();
//			else
//				columnsInfoEditor.getSite().getPage().activate(columnsInfoEditor);
		}

		// Update the selected lines in the row and column information tables
		if (rowsInfoEditor != null) {

			TableViewer rowsInfoViewer = rowsInfoEditor.getViewer();

			boolean updated = false;
			for (int i = 0; i < rowsInfoViewer.getTable().getItemCount(); i++) {
				if (selectedRowLabels.contains(rowsInfoViewer.getTable().getItem(i).getText())) {

					if (rowsInfoViewer.getTable().isSelected(i)) continue; // nothing to do

					rowsInfoViewer.getTable().select(i);
					// Scroll to the last selected point
					if (rowsInfoViewer.getTable().getItem(i).getText().equals(selectedRowLabels.get(selectedRowLabels.size() - 1))) {
						rowsInfoViewer.getTable().setTopIndex(i);
					}
					updated = true;
				}
				else {

					if (!rowsInfoViewer.getTable().isSelected(i)) continue; // nothing to do

					rowsInfoViewer.getTable().deselect(i);
					updated = true;
				}
			}

			if (updated && !rowsInfoEditor.getSite().equals(chartEditor.getSite())) {
				rowsInfoEditor.getSite().getPage().bringToTop(chartEditor);
			}
		}

		if (columnsInfoEditor != null) {

			TableViewer columnsInfoViewer = columnsInfoEditor.getViewer();

			boolean updated = false;
			Table table = columnsInfoViewer.getTable();
			for (int i = 0; i < table.getItemCount(); i++) {
				String item = columnsInfoViewer.getTable().getItem(i).getText();
				if (selectedColumnLabels.contains(item)) {

					if (columnsInfoViewer.getTable().isSelected(i)) continue; // nothing to do

					columnsInfoViewer.getTable().select(i);
					// Scroll to the last selected point
					if (item.equals(selectedColumnLabels.get(selectedColumnLabels.size() - 1))) {
						columnsInfoViewer.getTable().setTopIndex(i);
					}
					updated = true;
				}
				else {

					if (!table.isSelected(i)) continue; // nothing to do

					table.deselect(i);
					updated = true;
				}
			}

			if (updated && !columnsInfoEditor.getSite().equals(chartEditor.getSite())) {
				columnsInfoEditor.getSite().getPage().bringToTop(columnsInfoEditor);
			}
		}
	}
}
