package org.txm.ca.rcp.editors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.txm.ca.core.functions.CA;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.rcp.swt.GLComposite;

/*
 * Allwo to select 2 dimensions of a CA plane
 */
public class CAPlaneSelectionWidget extends GLComposite {

	CA ca;

	Combo firstDimensionCombo, secondDimensionCombo;

	String[] selectedPlane;

//	Color a;
	
	public CAPlaneSelectionWidget(Composite parent, CA ca) {

		super(parent, SWT.NONE);
		this.getLayout().numColumns = 4;
		this.getLayout().horizontalSpacing = 4;
		this.ca = ca;
		
//		a = new Color(this.getDisplay(), 255, 0, 0, 30);

		Label l = new Label(this, SWT.NONE);
		l.setText("X"); //$NON-NLS-1$
		l.setToolTipText(CAUIMessages.firstAxisDisplayed);

		firstDimensionCombo = new Combo(this, SWT.READ_ONLY | SWT.SINGLE);
		firstDimensionCombo.setToolTipText(CAUIMessages.firstAxisDisplayed);

		l = new Label(this, SWT.NONE);
		l.setText("Y"); //$NON-NLS-1$
		l.setToolTipText(CAUIMessages.secondAxisDisplayed);

		secondDimensionCombo = new Combo(this, SWT.READ_ONLY | SWT.SINGLE);
		secondDimensionCombo.setToolTipText(CAUIMessages.secondAxisDisplayed);

		SelectionListener listener = new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {

				if (firstDimensionCombo.getText().equals(secondDimensionCombo.getText())) {
					// not allowed for now
					selectedPlane = null;
//					firstDimensionCombo.setBackground(a);
//					secondDimensionCombo.setBackground(a);
//				} else if (Integer.parseInt(firstDimensionCombo.getText()) > Integer.parseInt(secondDimensionCombo.getText())) {
//					// X > Y !!!
//					selectedPlane = null;
//					firstDimensionCombo.setBackground(a);
//					secondDimensionCombo.setBackground(a);
				} else {
					selectedPlane = new String[] { firstDimensionCombo.getText(), secondDimensionCombo.getText() };
					firstDimensionCombo.setBackground(firstDimensionCombo.getParent().getBackground());
					secondDimensionCombo.setBackground(firstDimensionCombo.getParent().getBackground());
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				
			}
		};
		firstDimensionCombo.addSelectionListener(listener);
		secondDimensionCombo.addSelectionListener(listener);

		updateDefaultValues();
	}

	public void updateDefaultValues() {

		firstDimensionCombo.setItems(new String[0]);
		secondDimensionCombo.setItems(new String[0]);

		try {
			int min = Math.min(ca.getNumberOfFactorsMemorized(), ca.getSingularValues().length);
			for (int i = 0; i < min; i++) {
				firstDimensionCombo.add(Integer.toString(i + 1));
				secondDimensionCombo.add(Integer.toString(i + 1));
			}

			firstDimensionCombo.setText(Integer.toString(ca.getFirstDimension()));
			secondDimensionCombo.setText(Integer.toString(ca.getSecondDimension()));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String[] getSelectedPlane() {
		return selectedPlane;
	}

	public void setSelectedPlane(String d1, String d2) {

		firstDimensionCombo.setText(d1);
		secondDimensionCombo.setText(d2);
		selectedPlane = new String[] { firstDimensionCombo.getText(), secondDimensionCombo.getText() };
	}

	public void addSelectionListener(SelectionListener listener) {

		firstDimensionCombo.addSelectionListener(listener);
		secondDimensionCombo.addSelectionListener(listener);
	}

	public void setSelectedPlane(int firstDimension, int secondDimension) {

		setSelectedPlane(Integer.toString(firstDimension), Integer.toString(secondDimension));
	}

	public int[] getSelectedPlaneAsInts() {
		
		if (selectedPlane == null) return null;
		return new int[] { Integer.parseInt(selectedPlane[0]), Integer.parseInt(selectedPlane[1]) };
	}
}
