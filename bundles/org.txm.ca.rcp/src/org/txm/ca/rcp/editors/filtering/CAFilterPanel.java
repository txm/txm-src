package org.txm.ca.rcp.editors.filtering;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Text;
import org.txm.ca.core.chartsengine.styling.CAFilterStylingSheet;
import org.txm.ca.core.chartsengine.styling.CAStylerCatalog;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.ca.core.preferences.CAPreferences;
import org.txm.ca.rcp.editors.CAFactorialMapChartEditor;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.chartsengine.core.styling.StylingRule;
import org.txm.chartsengine.core.styling.StylingSheet;
import org.txm.chartsengine.core.styling.instructions.SelectionInstruction;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.swt.GLComposite;
import org.txm.statsengine.core.StatException;

/**
 * Configure the rows and cols filtering options.
 * 
 * 
 * @author mdecorde
 *
 */
public class CAFilterPanel extends GLComposite {

	LinkedHashMap<String, LinkedHashMap<String, Double>> thresholds = new LinkedHashMap<>();

	CAFactorialMapChartEditor editor;

	//	TableViewer viewer;

	String[] infos = {CAStylerCatalog.ID_CONTX, CAStylerCatalog.ID_CONTY, CAStylerCatalog.ID_QUALITY};
	String[] objects = {CAStylerCatalog.VALUE_ID_DIMENSION_ROWS, CAStylerCatalog.VALUE_ID_DIMENSION_COLS};

	private GroupFilter rowsFilterGroup;
	private GroupFilter colsFilterGroup;


	/**
	 * 
	 * @param parent
	 * @param editor
	 */
	public CAFilterPanel(Composite parent, CAFactorialMapChartEditor editor) {

		super(parent, SWT.NONE);

		this.editor = editor;

		this.getLayout().numColumns = 2;
		this.getLayout().horizontalSpacing = 5;

		rowsFilterGroup = new GroupFilter(this, CAStylerCatalog.VALUE_ID_DIMENSION_ROWS, CACoreMessages.rows);
		colsFilterGroup = new GroupFilter(this, CAStylerCatalog.VALUE_ID_DIMENSION_COLS, CACoreMessages.columns);

		initThresholdsAndUIFromFilterStylingSheet();
		
		updateWidgetsFromThresholds();

		this.updateChart();
	}


	/**
	 * Initializes the thresholds with the values of the filter styling sheet.
	 */
	protected void initThresholdsAndUIFromFilterStylingSheet() {

		CA ca = editor.getResult();
		StylingSheet sheet = ca.getFilterStylingSheet();

		// Fill with default values of the CAFilter macro
		for (String object : objects) {
			thresholds.put(object, new LinkedHashMap<String, Double>());
			for (String info : infos) {
				//				if (info.startsWith("Cos") || info.startsWith(CAStylerCatalog.ID_QUALITY)) {
				//					thresholds.get(object).put(info, 0.01d);
				//				}
				//				else if (info.startsWith("ΣCont")) {
				//					thresholds.get(object).put(info, 80d);
				//				}
				//				else if (info.startsWith(CAStylerCatalog.ID_CONTX) || info.startsWith(CAStylerCatalog.ID_CONTY)) {
				//					thresholds.get(object).put(info, 0.01d);
				//				}
				//				else {
				//					thresholds.get(object).put(info, 1d);
				//				}
				thresholds.get(object).put(info, 0d);
			}

			thresholds.get(object).put(CAStylerCatalog.ID_QUALITY, 1d);
		}

		//sumCont80(false, true, true);

		if (sheet != null) { // update values from the filter sheet
			//			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS).put(CAStylerCatalog.ID_QUALITY, 0d);
			//			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS).put(CAStylerCatalog.ID_CONTX, 0d);
			//			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS).put(CAStylerCatalog.ID_CONTY, 0d);
			//			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_COLS).put(CAStylerCatalog.ID_QUALITY, 0d);
			//			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_COLS).put(CAStylerCatalog.ID_CONTX, 0d);
			//			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_COLS).put(CAStylerCatalog.ID_CONTY, 0d);

			for (StylingRule rule : sheet.getRules()) {

				String rowOrColumn = null; // row or column rule ?
				for (SelectionInstruction si : rule.getSelectors()) {
					if (si.getInstruction().getID().equals(CAStylerCatalog.ID_OBJECT)) {
						rowOrColumn = si.getSettings().toString();
						break;
					}
				}

				for (SelectionInstruction si : rule.getSelectors()) { // find the property selection instruction
					if (si.getInstruction().getID().equals(CAStylerCatalog.ID_RESULT_INFOS)) {
//						if (si.getSettings() != null) {
//							String info = si.getParameter().getID();
//							Double threshold = (Double) si.getSettings();
//							thresholds.get(rowOrColumn).put(info, threshold);
//						}
						
						if (rule.getName().equals("Rows filter")) {
							
							if (si.getSettings() != null) {
								String info = si.getParameter().getID();
								Double threshold = (Double) si.getSettings();
								thresholds.get(rowOrColumn).put(info, threshold);
							}
							
							if (si.getParameter().getID().equals(CAStylerCatalog.ID_CONTX)) {
								rowsFilterGroup.contxFilter.doSubFilter = si.isActivated();
							} else if (si.getParameter().getID().equals(CAStylerCatalog.ID_CONTY)) {
								rowsFilterGroup.contyFilter.doSubFilter = si.isActivated();
							} else if (si.getParameter().getID().equals(CAStylerCatalog.ID_QUALITY)) {
								rowsFilterGroup.qxyFilter.doSubFilter = si.isActivated();
							}
						} else if (rule.getName().equals("Cols filter")) {
							
							if (si.getSettings() != null) {
								String info = si.getParameter().getID();
								Double threshold = (Double) si.getSettings();
								thresholds.get(rowOrColumn).put(info, threshold);
							}
							
							if (si.getParameter().getID().equals(CAStylerCatalog.ID_CONTX)) {
								colsFilterGroup.contxFilter.doSubFilter = si.isActivated();
							} else if (si.getParameter().getID().equals(CAStylerCatalog.ID_CONTY)) {
								colsFilterGroup.contyFilter.doSubFilter = si.isActivated();
							} else if (si.getParameter().getID().equals(CAStylerCatalog.ID_QUALITY)) {
								colsFilterGroup.qxyFilter.doSubFilter = si.isActivated();
							}
						}
					}
				}

				if (rule.getName().equals("Rows filter")) {
					rowsFilterGroup.doFilter = rule.isActivated();
				} else if (rule.getName().equals("Cols filter")) {
					colsFilterGroup.doFilter = rule.isActivated();
				} else if (rule.getName().startsWith("rows ") && rule.getName().endsWith(" color")) {
					rowsFilterGroup.doColors = rule.isActivated();
				} else if (rule.getName().startsWith("cols ") && rule.getName().endsWith(" color")) {
					colsFilterGroup.doColors = rule.isActivated();
				}
			}
		}
	}


	/**
	 * 
	 * @return the actual threshold values map
	 */
	public LinkedHashMap<String, LinkedHashMap<String, Double>> getThresholds() {
		return thresholds;
	}

	public GroupFilter getRowsContXGroup() {
		return rowsFilterGroup;
	}

	public GroupFilter getColumnsContXGroup() {
		return colsFilterGroup;
	}



	/**
	 * Sets CONT to the mean values
	 * 
	 *  + update viewer + filter styling sheet
	 */
	protected void applyThresholdsMeanContribs(boolean filterNow, boolean rows, boolean cols) {
		CA ca = editor.getResult();

		double[][] contribs;
		try {
			contribs = ca.getRowContrib();
		}
		catch (StatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		int l = contribs.length;
		if (rows) {
			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS).put(CAStylerCatalog.ID_CONTX, 100d / l); // sum of contribs is always 100
			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS).put(CAStylerCatalog.ID_CONTY, 100d / l);
		}
		try {
			contribs = ca.getColContrib();
		}
		catch (StatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		l = contribs.length;
		if (cols) {
			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_COLS).put(CAStylerCatalog.ID_CONTX, 100d / l);
			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_COLS).put(CAStylerCatalog.ID_CONTY, 100d / l);
		}

		if (filterNow && (rows || cols)) {
			updateFilterStylingSheetFromThresholdAndUI();
		}
	}

	/**
	 * Sets CONT to the mean values
	 * 
	 *  + update viewer + filter styling sheet
	 */
	protected void applyThresholdsMedContribs(boolean filterNow, boolean rows, boolean cols) {
		CA ca = editor.getResult();

		double[][] contribs;
		try {
			contribs = ca.getRowContrib();
		}
		catch (StatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		if (rows) {
			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS).put(CAStylerCatalog.ID_CONTX, CAFilterStylingSheet.getMedContribs(contribs, ca.getFirstDimension() - 1));
			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS).put(CAStylerCatalog.ID_CONTY, CAFilterStylingSheet.getMedContribs(contribs, ca.getSecondDimension() - 1));
		}

		try {
			contribs = ca.getColContrib();
		}
		catch (StatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		if (cols) {
			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_COLS).put(CAStylerCatalog.ID_CONTX, CAFilterStylingSheet.getMedContribs(contribs, ca.getFirstDimension() - 1));
			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_COLS).put(CAStylerCatalog.ID_CONTY, CAFilterStylingSheet.getMedContribs(contribs, ca.getSecondDimension() - 1));
		}
		if (filterNow && (rows || cols)) {
			updateFilterStylingSheetFromThresholdAndUI();
		}
	}

	/**
	 * Sets CONT to 80% of the total contribution of rows and columns
	 * 
	 *  + update viewer + filter styling sheet
	 */
	protected void applyThresholdsSumCont80(boolean filterNow, boolean rows, boolean cols) {

		CA ca = editor.getResult();

		double[][] contribs;
		try {
			contribs = ca.getRowContrib();
		}
		catch (StatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		if (rows) {
			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS).put(CAStylerCatalog.ID_CONTX, CAFilterStylingSheet.get80PercentOfSumContribs(contribs, ca.getFirstDimension() - 1));
			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS).put(CAStylerCatalog.ID_CONTY, CAFilterStylingSheet.get80PercentOfSumContribs(contribs, ca.getSecondDimension() - 1));
		}

		try {
			contribs = ca.getColContrib();
		}
		catch (StatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		if (cols) {
			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_COLS).put(CAStylerCatalog.ID_CONTX, CAFilterStylingSheet.get80PercentOfSumContribs(contribs, ca.getFirstDimension() - 1));
			thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_COLS).put(CAStylerCatalog.ID_CONTY, CAFilterStylingSheet.get80PercentOfSumContribs(contribs, ca.getSecondDimension() - 1));
		}
		if (filterNow && (rows || cols)) {
			//			viewer.refresh();
			updateFilterStylingSheetFromThresholdAndUI();
		}
	}

	/**
	 * Sets all values to ZERO + update viewer + filter styling sheet
	 */
	protected void applyThresholdsZero() {

		for (String object : objects) {
			for (String info : infos) {
				thresholds.get(object).put(info, 0d);
			}
			thresholds.get(object).put(CAStylerCatalog.ID_QUALITY, 1d);
		}

		//		viewer.refresh();
		updateFilterStylingSheetFromThresholdAndUI();
	}

	/**
	 * Sets all values to the minimum values
	 * 
	 *  + update viewer + filter styling sheet
	 */
	protected void applyThresholdsMins() {

		CA ca = editor.getResult();

		double[][] cos2;
		try {
			cos2 = ca.getRowCos2();
		}
		catch (StatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}
		double[] qualities = new double[cos2.length];
		for (int i = 0 ; i < cos2.length ; i++) {
			qualities[i] = Math.sqrt(cos2[i][ca.getFirstDimension() - 1]) + Math.sqrt(cos2[i][ca.getSecondDimension() - 1]);
		}
		double[][] contribs = ca.getRowsInfo().getAllContribs();
		thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS).put(CAStylerCatalog.ID_QUALITY, CAFilterStylingSheet.getMin(qualities));
		thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS).put(CAStylerCatalog.ID_CONTX, CAFilterStylingSheet.getMin2(contribs, ca.getFirstDimension() - 1));
		thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS).put(CAStylerCatalog.ID_CONTY, CAFilterStylingSheet.getMin2(contribs, ca.getSecondDimension() - 1));

		try {
			cos2 = ca.getColCos2();
		}
		catch (StatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		for (int i = 0 ; i < cos2.length ; i++) {
			qualities[i] = Math.sqrt(cos2[i][ca.getFirstDimension() - 1]) + Math.sqrt(cos2[i][ca.getSecondDimension() - 1]);
		}
		try {
			contribs = ca.getColContrib();
		}
		catch (StatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_COLS).put(CAStylerCatalog.ID_QUALITY, CAFilterStylingSheet.getMin(qualities));
		thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_COLS).put(CAStylerCatalog.ID_CONTX, CAFilterStylingSheet.getMin2(contribs, ca.getFirstDimension() - 1));
		thresholds.get(CAStylerCatalog.VALUE_ID_DIMENSION_COLS).put(CAStylerCatalog.ID_CONTY, CAFilterStylingSheet.getMin2(contribs, ca.getSecondDimension() - 1));
		//		viewer.refresh();
		updateFilterStylingSheetFromThresholdAndUI();
	}

	/**
	 * 
	 * @param threshold2 the new thresholds. must contains the VALUE_ID_DIMENSION_ROWS and VALUE_ID_DIMENSION_COLS key with values for keys CAStylerCatalog.ID_QUALITY, CAStylerCatalog.ID_CONTX, CAStylerCatalog.ID_CONTY
	 * @return false if thresholds map is not replaced 
	 */
	public boolean updateThresholds(LinkedHashMap<String, LinkedHashMap<String, Double>> threshold2) {

		if (threshold2 == null) return true;
		if (!threshold2.containsKey(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS)) return false;
		if (!threshold2.containsKey(CAStylerCatalog.VALUE_ID_DIMENSION_COLS)) return false;

		thresholds = threshold2;
		updateFilterStylingSheetFromThresholdAndUI();
		return true;
	}

	/**
	 * Updates the filter styling sheet from the values of the thresholds map.
	 */
	public synchronized void updateFilterStylingSheetFromThresholdAndUI() {

		// System.out.println("UPDATE FILTER STYLING SHEET");
		CA ca = this.editor.getResult();
		StylingSheet sheet = ca.getFilterStylingSheet();
		//		System.out.println("UPDATE FILTER STYLING SHEET: "+thresholds);
		for (StylingRule rule : sheet.getRules()) {

			String rowOrColumn = null; // row or coclumn rule ?
			for (SelectionInstruction si : rule.getSelectors()) {
				if (si.getInstruction().getID().equals(CAStylerCatalog.ID_OBJECT)) {
					rowOrColumn = si.getSettings().toString();
					break;
				}
			}

			for (SelectionInstruction si : rule.getSelectors()) { 
				
				
				if (rule.getName().equals("Rows filter")) { // set activations
					
					if (si.getInstruction().getID().equals(CAStylerCatalog.ID_RESULT_INFOS)) { // find the property selection instruction
						String info = si.getParameter().getID();
						Double threshold = thresholds.get(rowOrColumn).get(info);
						si.setSettings(threshold); // update the threshold
					}
					
					if (si.getParameter().getID().equals(CAStylerCatalog.ID_CONTX)) {
						si.setActivated(rowsFilterGroup.contxFilter.doSubFilter);
					} else if (si.getParameter().getID().equals(CAStylerCatalog.ID_CONTY)) {
						si.setActivated(rowsFilterGroup.contyFilter.doSubFilter);
					} else if (si.getParameter().getID().equals(CAStylerCatalog.ID_QUALITY)) {
						si.setActivated(rowsFilterGroup.qxyFilter.doSubFilter);
					} else if (si.getParameter().getID().equals(CAStylerCatalog.ID_OBJECT)) {
						si.setActivated(rowsFilterGroup.contxFilter.doSubFilter || rowsFilterGroup.contyFilter.doSubFilter || rowsFilterGroup.qxyFilter.doSubFilter);
					}
				} else if (rule.getName().equals("Cols filter")) {
					
					if (si.getInstruction().getID().equals(CAStylerCatalog.ID_RESULT_INFOS)) { // find the property selection instruction
						String info = si.getParameter().getID();
						Double threshold = thresholds.get(rowOrColumn).get(info);
						si.setSettings(threshold); // update the threshold
					}
					
					if (si.getParameter().getID().equals(CAStylerCatalog.ID_CONTX)) {
						si.setActivated(colsFilterGroup.contxFilter.doSubFilter);
					} else if (si.getParameter().getID().equals(CAStylerCatalog.ID_CONTY)) {
						si.setActivated(colsFilterGroup.contyFilter.doSubFilter);
					} else if (si.getParameter().getID().equals(CAStylerCatalog.ID_QUALITY)) {
						si.setActivated(colsFilterGroup.qxyFilter.doSubFilter);
					} else if (si.getParameter().getID().equals(CAStylerCatalog.ID_OBJECT)) {
						si.setActivated(colsFilterGroup.contxFilter.doSubFilter || colsFilterGroup.contyFilter.doSubFilter || colsFilterGroup.qxyFilter.doSubFilter);
					}
				}
			}

			if (rule.getName().equals("Rows filter")) {
				rule.setActivated(rowsFilterGroup.doFilter);
			} else if (rule.getName().equals("Cols filter")) {
				rule.setActivated(colsFilterGroup.doFilter);
			} else if (rule.getName().endsWith(" color") && rule.getName().startsWith("rows")) {
				rule.setActivated(rowsFilterGroup.doColors);
			} else if (rule.getName().endsWith(" color") && rule.getName().startsWith("cols")) {
				rule.setActivated(colsFilterGroup.doColors);
			}
		}

		updateChart();
	}

	public void updateChart() {

		//System.out.println(sheet.getRules());
		this.editor.getResult().getChartCreator().updateChart(editor.getResult());
		//	this.editor.updateEditorFromChart(true);
		this.editor.updateBottomInfoLine();
	}


	public class GroupFilter extends Group {

		private SubFilterPanel qxyFilter;
		private SubFilterPanel contyFilter;
		private SubFilterPanel contxFilter;
		private Button activateButton;
		private Button activateQualityButton;
		private Button activateContXButton;
		private Button activateContYButton;

		protected String info;
		//		private Button columns80SumCont;
		//		private Button columnsMeanCont;
		//		private Button columnsMedCont;
		private Label sum80contButton;
		private Label meanButton;
		private Button colorModeButton;

		boolean doFilter = true;
		boolean doColors = true;


		public GroupFilter(Composite parent, String info, String title) {
			super(parent, SWT.NONE);
			this.info = info;
			this.setText(" "+title+" "); //$NON-NLS-1$
			//this.setLayout(new GridLayout(7, false));

			RowLayout l = new RowLayout();
			l.center = true;
			this.setLayout(l);
			this.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

			GLComposite buttons = new GLComposite(this, SWT.NONE);
			buttons.getLayout().verticalSpacing = 4;
			buttons.getLayout().makeColumnsEqualWidth = false;

			activateButton = new Button(buttons, SWT.CHECK);
			activateButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			activateButton.setToolTipText(CAUIMessages.bind(CAUIMessages.activateDeactivateFiltersOfP0,  this.getText().trim()));
			activateButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {

					doFilter = activateButton.getSelection();

					updateFilterStylingSheetFromThresholdAndUI();
					
					updateWidgetsFromThresholds();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {}
			});

			if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
				colorModeButton = new Button(buttons, SWT.TOGGLE);
				colorModeButton.setSelection(false);
				colorModeButton.setAlignment(SWT.CENTER);
				colorModeButton.setImage(IImageKeys.getImage("platform:/plugin/org.txm.ca.rcp/icons/colors.png"));
				colorModeButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
				//			sum80contButton.setText("Σ%"); //$NON-NLS-1$
				colorModeButton.setToolTipText("Red=filtered on contX, Blue=filtered on contY and Yellow=quality ; Orange = filtered on contX and contY, Violet = filtered on contY and quality, Green = filtered on ContX and Quality");
				colorModeButton.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {

						doColors = colorModeButton.getSelection();
						
						updateFilterStylingSheetFromThresholdAndUI();
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) { }
				});
			} else {
				new Label(buttons, SWT.NONE);
			}

			sum80contButton = new Label(buttons, SWT.BORDER);
			sum80contButton.setAlignment(SWT.CENTER);
			//resetbutton.setImage(IImageKeys.getImage(IImageKeys.REFRESH));
			sum80contButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			sum80contButton.setText("Σ%"); //$NON-NLS-1$
			sum80contButton.setToolTipText(CAUIMessages.bind(CAUIMessages.resetContributionThreasholdsOfP0To80, this.getText().trim()));
			sum80contButton.addMouseListener(new MouseListener() {

				@Override
				public void mouseUp(MouseEvent e) {

					if (CAStylerCatalog.VALUE_ID_DIMENSION_ROWS.equals(info)) {
						applyThresholdsSumCont80(true, true, false);

					} else {
						applyThresholdsSumCont80(true, false, true);
					}

					contxFilter.updateWidgetsFromThresholds();
					contyFilter.updateWidgetsFromThresholds();
				}

				@Override
				public void mouseDown(MouseEvent e) { }

				@Override
				public void mouseDoubleClick(MouseEvent e) { }

			});

			meanButton = new Label(buttons, SWT.BORDER);
			//meanbutton.setImage(IImageKeys.getImage(IImageKeys.REFRESH));
			meanButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			meanButton.setText("⋶"); //$NON-NLS-1$
			meanButton.setAlignment(SWT.CENTER);
			meanButton.setToolTipText(CAUIMessages.bind(CAUIMessages.resetContributionThreasholdsOfP0TotheMeanValue, this.getText().trim()));
			meanButton.addMouseListener(new MouseListener() {

				@Override
				public void mouseUp(MouseEvent e) {

					if (CAStylerCatalog.VALUE_ID_DIMENSION_ROWS.equals(info)) {
						applyThresholdsMeanContribs(true, true, false);
					} else {
						applyThresholdsMeanContribs(true, false, true);
					}

					contxFilter.updateWidgetsFromThresholds();
					contyFilter.updateWidgetsFromThresholds();
				}

				@Override
				public void mouseDown(MouseEvent e) { }

				@Override
				public void mouseDoubleClick(MouseEvent e) { }

			});

			buttons = new GLComposite(this, SWT.NONE);
			buttons.getLayout().verticalSpacing = 4;
			buttons.getLayout().makeColumnsEqualWidth = false;

			activateContXButton = new Button(buttons, SWT.CHECK);
			activateContXButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			activateContXButton.setToolTipText(CAUIMessages.bind(CAUIMessages.activateDeactivateFiltersOfP0,  CAStylerCatalog.ID_CONTX+" "+this.getText().trim()));
			activateContXButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					contxFilter.doSubFilter = activateContXButton.getSelection();
					contxFilter.setEnabled(activateContXButton.getSelection());
					
					updateFilterStylingSheetFromThresholdAndUI();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {}
			});
			new Label(buttons, SWT.NONE);
			new Label(buttons, SWT.NONE);

			//			int nvalues = 0;
			//			try {
			//				nvalues = CAStylerCatalog.VALUE_ID_DIMENSION_ROWS.equals(info) ? editor.getResult().getRowsCount():editor.getResult().getColumnsCount();
			//			}
			//			catch (Exception e1) {
			//				// TODO Auto-generated catch block
			//				e1.printStackTrace();
			//			}

			contxFilter = new SubFilterPanel(this, CAStylerCatalog.ID_CONTX, CAStylerCatalog.ID_CONTX) { // , 0, 100000, 3, 100000/nvalues

				@Override
				public String getLabel() {
					return decimalFormat.format(thresholds.get(group.info).get(info));
				}
			};

			buttons = new GLComposite(this, SWT.NONE);
			buttons.getLayout().verticalSpacing = 4;
			buttons.getLayout().makeColumnsEqualWidth = false;

			activateContYButton = new Button(buttons, SWT.CHECK);
			activateContYButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			activateContYButton.setToolTipText(CAUIMessages.bind(CAUIMessages.activateDeactivateFiltersOfP0,  CAStylerCatalog.ID_CONTY+" "+this.getText().trim()));
			activateContYButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					contyFilter.doSubFilter = activateContYButton.getSelection();
					contyFilter.setEnabled(activateContYButton.getSelection());

					updateFilterStylingSheetFromThresholdAndUI();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {}
			});
			new Label(buttons, SWT.NONE);
			new Label(buttons, SWT.NONE);

			contyFilter = new SubFilterPanel(this, CAStylerCatalog.ID_CONTY, CAStylerCatalog.ID_CONTY) { // , 0, 100000, 3, 100000/nvalues
				@Override
				public String getLabel() {
					return decimalFormat.format(thresholds.get(group.info).get(info));
				}
			};

			buttons = new GLComposite(this, SWT.NONE);
			buttons.getLayout().verticalSpacing = 4;
			buttons.getLayout().makeColumnsEqualWidth = false;

			activateQualityButton = new Button(buttons, SWT.CHECK);
			activateQualityButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			activateQualityButton.setToolTipText(CAUIMessages.bind(CAUIMessages.activateDeactivateFiltersOfP0,  CAStylerCatalog.ID_QUALITY+" "+this.getText().trim()));
			activateQualityButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					qxyFilter.doSubFilter = activateQualityButton.getSelection();
					qxyFilter.setEnabled(activateQualityButton.getSelection());
					updateFilterStylingSheetFromThresholdAndUI();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {}
			});
			new Label(buttons, SWT.NONE);
			new Label(buttons, SWT.NONE);

			qxyFilter = new SubFilterPanel(this, CAStylerCatalog.ID_QUALITY, CAStylerCatalog.ID_QUALITY) { // , 0, 1000, 3, 1000/nvalues
				@Override
				public String getLabel() {
					return decimalFormat.format(thresholds.get(group.info).get(info));
				}
			};

			// initialize
			

			//			Label helpTooltip = new Label(this, SWT.NONE);
			//			helpTooltip.setText("(????)");
			//			helpTooltip.setToolTipText("BLAH BLAH filter points contx < XXX...");
		}

		@Override
		protected void checkSubclass() {} // OK

		/**
		 * 
		 */
		public void updateWidgetsFromThresholds() {
			
			activateButton.setSelection(doFilter);
			
			activateContXButton.setSelection(contxFilter.doSubFilter);
			activateContXButton.setEnabled(doFilter);
			contxFilter.setEnabled(contxFilter.doSubFilter && doFilter);
			
			activateContYButton.setSelection(contyFilter.doSubFilter);
			activateContYButton.setEnabled(doFilter);
			contyFilter.setEnabled(contyFilter.doSubFilter && doFilter);
			
			activateQualityButton.setSelection(qxyFilter.doSubFilter);
			activateQualityButton.setEnabled(doFilter);
			qxyFilter.setEnabled(qxyFilter.doSubFilter && doFilter);
			
			if (sum80contButton != null) sum80contButton.setEnabled(doFilter);
			if (meanButton != null) meanButton.setEnabled(doFilter);
			
			contxFilter.updateWidgetsFromThresholds();
			contyFilter.updateWidgetsFromThresholds();
			qxyFilter.updateWidgetsFromThresholds();
		}


		//		public void updateAvailableThresholdsValues() {
		//			contxFilter.updateAvailableThresholdsValues();
		//			contyFilter.updateAvailableThresholdsValues();
		//			qxyFilter.updateAvailableThresholdsValues();
		//		}
	}



	public abstract class SubFilterPanel extends Group {

		protected DecimalFormat decimalFormat;

		String info;
		double min = 0d;
		double max = 100d;
		//		int digit = 0;
		//		double digit10 = Math.pow(10, digit);Group
		int increment = 1;

		GroupFilter group;
		private Button previousValueButton;
		private Scale slider;
		private Text valueText;
		private Button nextValueButton;

		private Label infosLabel;

		boolean doSubFilter = true;

		/**
		 * 
		 * @param parent
		 * @param info
		 * @param title
		 */
		//FIXME: SJ: the panel should store its own double[][] values list at creation
		public SubFilterPanel(GroupFilter parent, String info, String title) { // , int min, int max, int digit, int increment

			super(parent, SWT.NONE);
			this.group = parent;
			this.info = info;

			decimalFormat = editor.getResult().getFormatter(info);
			//			decimalFormat.setMaximumFractionDigits(3);

			this.setText(" "+title + " ⩽ "); //$NON-NLS-1$
			this.setLayout(new GridLayout(3, false));

			SelectionListener applyListener = new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {


					// SJ: old code that needs an external update of the values 
					//					if (e.getSource() == previousValueButton) {
					//						indexValues--;
					//						if (indexValues < 0) {
					//							indexValues = 0;
					//						}
					//
					//					}
					//					else if (e.getSource() == nextValueButton) {
					//						indexValues++;
					//						if (indexValues >= values.length) {
					//							indexValues = values.length - 1;
					//						}
					//					}
					//					double d = values[indexValues];
					//					thresholds.get(group.info).put(info, d);
					//					valueText.setText(decimalFormat.format(d));
					//					updateStatsLabel();
					//					updateFilterStylingSheet();



					// SJ: FIXME: test to dynamically retrieve the next/previous value directly from the CA
					// SJ: the code is not really efficient, to improve if we keep dynamic way of values retrieving
					// especially, store a double[][] values array at the panel creation

					try {
						// set axis x, y, or quality 
						int axisIndex = 0;
						switch (info) {
							default:
							case CAStylerCatalog.ID_CONTX:
								axisIndex = editor.getResult().getFirstDimension() - 1;
								break;
							case CAStylerCatalog.ID_CONTY:
								axisIndex = editor.getResult().getSecondDimension() - 1;
								break;
							case CAStylerCatalog.ID_QUALITY:
								axisIndex = -1; // means we need both axis
								break;
						}


						// rows or cols contrib and quality
						double[][] contribValues = null;
						double[][] qualityValues = null;
						if (CAStylerCatalog.VALUE_ID_DIMENSION_ROWS.equals(group.info)) {

							contribValues = editor.getResult().getRowContrib();			
							qualityValues = editor.getResult().getRowCos2();
						} else { //if (CAStylerCatalog.VALUE_ID_DIMENSION_COLS.equals(group.info)) {
							contribValues = editor.getResult().getColContrib();
							qualityValues = editor.getResult().getColCos2();
						}

						double currentValue = thresholds.get(group.info).get(info); 
						double newValue = 0;

						// get the next nearest lower value than than the current value
						if (e.getSource() == previousValueButton) {
							double tmpValue = 0;
							newValue = 0;
							// Cont x or y
							if(axisIndex != -1) {
								for (int i = 0; i < contribValues.length; i++) {
									tmpValue = contribValues[i][axisIndex];
									// if tmp value < currentValue and tmp value > last candidate value then it's a new candidate 
									if (tmpValue < currentValue && tmpValue > newValue) {
										newValue = tmpValue; 
									}
								}
							}
							// Q(x,y)
							else {
								for (int i = 0; i < qualityValues.length; i++) {
									tmpValue = qualityValues[i][editor.getResult().getFirstDimension() - 1] + qualityValues[i][editor.getResult().getSecondDimension() - 1];
									//if tmp value < currentValue and tmp value > last candidate value then it's a new candidate
									if(tmpValue < currentValue && tmpValue > newValue) {
										newValue = tmpValue; 
									}
								}
							}
						}
						// get the next nearest greater value than the current value
						else if (e.getSource() == nextValueButton) {
							double tmpValue = 0;
							newValue = Double.MAX_VALUE;
							// Cont x or y
							if(axisIndex != -1) {
								for (int i = 0; i < contribValues.length; i++) {
									tmpValue = contribValues[i][axisIndex];
									//if tmp value > currentValue and tmp value < last candidate value then it's a new candidate
									if(tmpValue > currentValue && tmpValue < newValue) {
										newValue = tmpValue; 
									}
								}
							}
							// Q(x,y)
							else {
								for (int i = 0; i < qualityValues.length; i++) {
									tmpValue = qualityValues[i][editor.getResult().getFirstDimension() - 1] + qualityValues[i][editor.getResult().getSecondDimension() - 1];
									//if tmp value > currentValue and tmp value < last candidate value then it's a new candidate
									if(tmpValue > currentValue && tmpValue < newValue) {
										newValue = tmpValue; 
									}
								}
							}

							if(newValue == Double.MAX_VALUE) {
								newValue = currentValue;
							}
						}


						thresholds.get(group.info).put(info, newValue);
						valueText.setText(decimalFormat.format(newValue));

						updateSubFilterInfosLabel();
						updateFilterStylingSheetFromThresholdAndUI();

					}
					catch (StatException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}


				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) { }
			};

			// Previous button
			previousValueButton = new Button(this, SWT.PUSH);
			previousValueButton.setImage(IImageKeys.getImage(IImageKeys.CTRLREVERSE));
			previousValueButton.addSelectionListener(applyListener);


			// Manual input text
			valueText = new Text(this, SWT.BORDER | SWT.CENTER);
			GridData gdata = new GridData(GridData.FILL, GridData.FILL, true, true);
			gdata.widthHint = gdata.minimumWidth = 35;
			valueText.setLayoutData(gdata);
			valueText.setToolTipText("The points which contx < X and also conty < Y and also Q < q will be removed");
			valueText.addKeyListener(new KeyListener() {

				@Override
				public void keyReleased(KeyEvent e) {

					if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
						try {
							String s = valueText.getText();
							s = s.replaceAll("\\.", ","); // manage the decimal point character separator //$NON-NLS-1$
							double currentValue = decimalFormat.parse(s).doubleValue();
							thresholds.get(group.info).put(info, currentValue);
							valueText.setText(decimalFormat.format(currentValue));
							updateSubFilterInfosLabel();
							updateFilterStylingSheetFromThresholdAndUI();
						}
						catch (ParseException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}

				@Override
				public void keyPressed(KeyEvent e) { }
			});

			// listener just ensure the inputed text makes a number
			valueText.addVerifyListener(new VerifyListener() {
				Pattern p = Pattern.compile("[0-9]*([,.][0-9]*)?"); //$NON-NLS-1$
				@Override
				public void verifyText(VerifyEvent e) {
					// allows cut (CTRL + x)
					if (e.keyCode == SWT.ARROW_LEFT ||
							e.keyCode == SWT.ARROW_RIGHT ||
							e.keyCode == SWT.BS ||
							e.keyCode == SWT.DEL ||
							e.keyCode == SWT.CTRL ||
							e.keyCode == SWT.SHIFT) {
						e.doit = true;
					} else if (e.text.isEmpty()) {
						e.doit = true;
					} else {
						boolean allow = false;
						//						System.out.println(e);
						if (e.keyCode == 0) { // Text is not the source, verify the new input
							//							try {
							//								double d = decimalFormat.parse(e.text).doubleValue();
							//								if (d > max) e.doit= false;
							//								else if (d < min) e.doit = false;
							//								else e.doit = true;
							//							} catch(Exception ex) {
							//								e.doit = false;
							//							}
							e.doit = true;
						} else {
							e.doit = true;
							for (int i = 0; i < e.text.length(); i++) {

								char c = e.text.charAt(i);
								allow = Character.isDigit(c) || Character.isWhitespace(c) || ',' == c || '.' == c; 
								if (! allow ) {
									e.doit = false;
									break;
								}
							}
							if (!e.doit) return; // don't need to do more tests

							String newValue = valueText.getText().substring(0, e.start)+e.text+valueText.getText().substring(e.start);
							e.doit = p.matcher(newValue).matches(); //  test if the new value is a number (not multiple comma, etc.)
						}
					}
				}
			});

			// Next button
			nextValueButton = new Button(this, SWT.PUSH);
			nextValueButton.setImage(IImageKeys.getImage(IImageKeys.CTRLPLAY));
			nextValueButton.addSelectionListener(applyListener);

			infosLabel = new Label(this, SWT.NONE);
			infosLabel.setLayoutData(new GridData(GridData.CENTER, GridData.FILL, true, false, 3, 1));

			// Scale and slider tests
			//			if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
			//				slider = new Scale(this, SWT.HORIZONTAL);
			//				//slider.setThumb(increment/10);
			//				gdata = new GridData(GridData.FILL, GridData.CENTER, true, false,3 ,1);
			//				gdata.widthHint = gdata.minimumWidth = 70;
			//				slider.setLayoutData(gdata);
			//				slider.addMouseListener(new MouseAdapter() {
			//					@Override
			//					public void mouseUp(MouseEvent e) {
			//						indexValues = slider.getSelection();
			//						double d = values[slider.getSelection()];//(float) slider.getSelection() / digit10;
			//						thresholds.get(group.info).put(info, d);
			//						valueText.setText(decimalFormat.format(d));
			//						updateStatsLabel();
			//						updateFilterStylingSheet();
			//					}
			//				});
			//				slider.addMouseWheelListener(new MouseWheelListener() {
			//
			//					@Override
			//					public void mouseScrolled(MouseEvent e) {
			//						indexValues = slider.getSelection();
			//						double d = values[slider.getSelection()]; // (float) slider.getSelection() / digit10;
			//						thresholds.get(group.info).put(info, d);
			//						valueText.setText(decimalFormat.format(d));
			//						updateStatsLabel();
			//						updateFilterStylingSheet();
			//					}
			//				});
			//				slider.addSelectionListener(new SelectionListener() {
			//
			//					@Override
			//					public void widgetSelected(SelectionEvent e) {
			//						indexValues = slider.getSelection();
			//						double d = values[slider.getSelection()]; // (float) slider.getSelection() / digit10;
			//						thresholds.get(group.info).put(info, d);
			//						updateStatsLabel();
			//						valueText.setText(decimalFormat.format(d));
			//					}
			//
			//					@Override
			//					public void widgetDefaultSelected(SelectionEvent e) { }
			//				});
			//			}


			//updateWidgetsFromThresholds();
		}


		@Override
		public void setEnabled(boolean enable) {
			super.setEnabled(enable);
			if (slider != null) slider.setEnabled(enable);
			valueText.setEnabled(enable);
			previousValueButton.setEnabled(enable);
			nextValueButton.setEnabled(enable);
		}

		//public double[] values = new double[] {0d, 0.1d, 0.2d, 0.3d, 0.4d, 0.5d};
		//int indexValues = 0; 





		public abstract String getLabel();




		//		public int quickFind(double[] values, double d) {
		//
		//			//			double d = decimalFormat.parse(s).doubleValue();
		//			int i = 0;
		//			int min = 0;
		//			int max = values.length;
		//			while (max - min > 1) {
		//				i = (min + max)/2;
		//				if (values[i] > d) {
		//					max = i;
		//				} else if (values[i] < d) {
		//					min = i;
		//				} else {
		//					break; // found it
		//				}
		//			}
		//
		//			if (max == i) i--; // to get the previous hit
		//
		//			return i;
		//		}

		/**
		 * gets values to navigate into
		 */
		//		public void updateAvailableThresholdsValues() {
		//			try {
		//				values = (double[]) editor.getResult().getData(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS.equals(group.info), info);
		//
		//				Arrays.sort(values);
		//				this.min = values[0];
		//				this.max = values[values.length - 1];
		//				this.increment = 1; // (int) (((this.max - this.min)/(10*values.length)));
		//				this.max += this.increment; // this will allow to filter more
		//				
		//				if (slider != null) {
		//					slider.setMaximum(0); // this.max);
		//					slider.setMaximum(values.length - 1); // this.max);
		//					slider.setIncrement(1); //increment);
		//					slider.setPageIncrement(values.length / 15);
		//				}
		//			}
		//			catch (Exception e1) {
		//				// TODO Auto-generated catch block
		//				e1.printStackTrace();
		//				return;
		//			}
		//		}
		//
		//		public void updateThresholdsAndFilterFromTextField() {
		//			try {
		//				String s = valueText.getText();
		//				double d = decimalFormat.parse(s).doubleValue();
		//
		//				indexValues = quickFind(values, d);
		//				d = values[indexValues];
		//				
		//				thresholds.get(group.info).put(info, d);
		//				updateGroupLabel();
		//				valueText.setText(decimalFormat.format(d));
		//			}
		//			catch (ParseException e1) {
		//				// TODO Auto-generated catch block
		//				e1.printStackTrace();
		//			}
		//
		//			updateFilterStylingSheet();
		//		}

		/**
		 * Updates all the widgets according to the current value in the thresholds map.
		 */
		public void updateWidgetsFromThresholds() {

			double d = thresholds.get(group.info).get(info);

			//			indexValues = quickFind(values, d);
			//			if (slider != null) slider.setSelection(indexValues);

			valueText.setText(decimalFormat.format(d));
			valueText.setToolTipText(TXMCoreMessages.bind("If activated, the {0} points with {1} greater than {2} will be visible", this.group.info, this.info , valueText.getText()));
			
			updateSubFilterInfosLabel();
		}

		/**
		 * Computes infos and updates the SubFilterPanel title.
		 */
		public void updateSubFilterInfosLabel() {

			CA ca = editor.getResult();
			double currentValue = thresholds.get(group.info).get(info);

			try {

				int axisIndex = 0;
				switch (info) {
					default:
					case CAStylerCatalog.ID_CONTX:
						axisIndex = editor.getResult().getFirstDimension() - 1;
						break;
					case CAStylerCatalog.ID_CONTY:
						axisIndex = editor.getResult().getSecondDimension() - 1;
						break;
					case CAStylerCatalog.ID_QUALITY:
						axisIndex = -1; // means we need both axis
						break;
				}

				// rows or cols contrib and quality
				double[][] contribValues;
				double[][] qualityValues;
				if (CAStylerCatalog.VALUE_ID_DIMENSION_ROWS.equals(group.info)) {
					contribValues = editor.getResult().getRowContrib();			
					qualityValues = editor.getResult().getRowCos2();
				} else {// CAStylerCatalog.VALUE_ID_DIMENSION_COLS:
					contribValues = editor.getResult().getColContrib();
					qualityValues = editor.getResult().getColCos2();
				}

				StringBuilder buffer = new StringBuilder();
				//buffer.append(info + "	"); //$NON-NLS-1$
				buffer.append(" "); //$NON-NLS-1$

				// Displays the number of kept points according to the local filtering rule
				if (CAPreferences.getInstance().getBoolean(CAPreferences.FILTER_INFOS_N_FILTERED)) {

					// SJ: old version based on cached values
					//					int n = 0;
					//					int N = 0;
					//					if (CAStylerCatalog.VALUE_ID_DIMENSION_ROWS.equals(group.info)) {
					//						N = ca.getRowsCount();
					//					}
					//					else {
					//						N = ca.getColumnsCount();
					//					}
					//					for (n = 0 ; n < values.length; n++) {
					//						if (values[n] > d) {
					//							n--;
					//							break;
					//						}
					//					}
					//
					//					buffer.append(" " + (N-n) + "/" + N);

					// SJ: new version
					int keptPointsCount = 0;
					int totalPointsCount = 0;

					if (CAStylerCatalog.VALUE_ID_DIMENSION_ROWS.equals(group.info)) {
						totalPointsCount = ca.getRowsCount();
					}
					else {
						totalPointsCount = ca.getColumnsCount();
					}

					// Cont x or y
					if(axisIndex != -1) {
						for (int i = 0; i < contribValues.length; i++) {
							if(contribValues[i][axisIndex] > currentValue) {
								keptPointsCount++;
							}
						}
					}
					// Q(x,y)
					else {
						for (int i = 0; i < qualityValues.length; i++) {
							if((qualityValues[i][editor.getResult().getFirstDimension() - 1] + qualityValues[i][editor.getResult().getSecondDimension() - 1]) > currentValue) {
								keptPointsCount++;
							}
						}
					}	

					buffer.append(" " + keptPointsCount + "/" + totalPointsCount); //$NON-NLS-1$
				}

				if (CAPreferences.getInstance().getBoolean(CAPreferences.FILTER_INFOS_SUM)) {

					double contSum = -1;
					if (!info.equals(CAStylerCatalog.ID_QUALITY)) {
						contSum = ca.getSumContForValue(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS.equals(group.info),
								CAStylerCatalog.ID_CONTX.equals(info)? ca.getFirstDimension() - 1:ca.getSecondDimension() - 1,
										currentValue).doubleValue();

						buffer.append(", Σ=" + String.valueOf(CAFactorialMapChartEditor.noDecimalFormatter.format(contSum))+"%"); //$NON-NLS-1$
					}
				} 

				if (CAPreferences.getInstance().getBoolean(CAPreferences.FILTER_INFOS_AHC_CLASSES)) {
					int classNumber  = ca.getAHCClassForValue(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS.equals(group.info), info, currentValue);
					buffer.append(", C="+classNumber); //$NON-NLS-1$
				}
				infosLabel.setText(buffer.toString());
				getParent().layout(true);
				getParent().getParent().layout(true);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		protected void checkSubclass() {} // OK
	}

	public void updateWidgetsFromThresholds() {
		this.rowsFilterGroup.updateWidgetsFromThresholds();
		this.colsFilterGroup.updateWidgetsFromThresholds();
	}
}
