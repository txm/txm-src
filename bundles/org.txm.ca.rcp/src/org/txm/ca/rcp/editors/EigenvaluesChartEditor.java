/**
 * 
 */
package org.txm.ca.rcp.editors;

import org.txm.ca.core.functions.Eigenvalues;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.editors.ChartEditorInput;

/**
 * 
 * Eigenvalues chart editor.
 * 
 * @author sjacquot
 *
 */
public class EigenvaluesChartEditor extends ChartEditor<Eigenvalues> {

	/**
	 * 
	 */
	public EigenvaluesChartEditor() {
		super();
	}

	/**
	 * 
	 * @param chartEditorInput
	 */
	public EigenvaluesChartEditor(ChartEditorInput<Eigenvalues> chartEditorInput) {
		super(chartEditorInput);
	}

	@Override
	public void __createPartControl() {
		// remove the compute button
		this.removeComputeButton();
	}

	@Override
	public void updateEditorFromChart(boolean update) {
		// nothing to do
	}
}
