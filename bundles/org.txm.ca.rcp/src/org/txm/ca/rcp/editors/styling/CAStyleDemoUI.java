package org.txm.ca.rcp.editors.styling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.TreeItem;
import org.txm.ca.core.chartsengine.styling.CAStylerCatalog;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.provider.SimpleLabelProvider;


/*
 * 
	Feuille Style :
		Selector	------ Traduction ---- >	No serie + No item ---- projection dans chart ----> ChartItems
		Style		------ Traduction ----->	Instructions de rendu de chart
			Type : Font, Color, Shape
			Valeur : 
 * 
 */
/**
 * First demo of styling interface
 * @deprecated
 * 
 *
 */
@Deprecated
public class CAStyleDemoUI extends GLComposite {

	private static final String[] dummyObjects = new String[] { "points&labels", "points", "labels" }; //$NON-NLS-1$

	private static final String[] dummyProperties = new String[] { "Quality", CAStylerCatalog.ID_COSX, CAStylerCatalog.ID_CONTX, CAStylerCatalog.ID_COSY, CAStylerCatalog.ID_CONTY }; //$NON-NLS-1$

	private static final String[] dummyThresholds = new String[] { "< 0.5", "< 0.4", "< 0.2", "< 0.1", "> 0.5", "> 0.4", "> 0.2", "> 0.1" }; //$NON-NLS-1$

	private static final String[] dummyStyleTypes = new String[] { "Style pré-défini", "Transparence", "Couleur", "Forme", "Taille", "Police" }; //$NON-NLS-1$

	private static final String[] dummyStyleValues = new String[] { "Mise en évidence1", "Cercle", "Disque", "Mauve", "Rouge", "Up Triangle", "Proportionel à Cont1", "Proportionel à Q12", "x 3", //$NON-NLS-1$
			"Vert pomme d'api", "Arial", "Carré" }; //$NON-NLS-1$

	static HashMap<String, ArrayList<Styling>> demos = new HashMap<>();

	static ArrayList<Styling> demoData = new ArrayList<>();

	ArrayList<StructuredViewer> viewers = new ArrayList<StructuredViewer>();

	Object currentSelection;

	private ISelectionChangedListener tmpListener;

	static {
		demoData = new ArrayList<>();
		Styling styleVOEUXEx11 = new Styling(null);
		styleVOEUXEx11.selection = new SelectionDefinition("points", null, null, null); //$NON-NLS-1$
		styleVOEUXEx11.style = new StyleDefinition("Forme", "Disque", null); //$NON-NLS-1$

		Styling styleVOEUXEx12 = new Styling(null);
		styleVOEUXEx12.selection = new SelectionDefinition("points&labels", null, null, null); //$NON-NLS-1$
		styleVOEUXEx12.style = new StyleDefinition("Couleur", "Vert foncé", null); //$NON-NLS-1$

		Styling styleVOEUXEx13 = new Styling(null);
		styleVOEUXEx13.selection = new SelectionDefinition("points&labels", null, null, null); //$NON-NLS-1$
		styleVOEUXEx13.style = new StyleDefinition("Transparence", "Proportionel à Q12", null); //$NON-NLS-1$

		Styling styleVOEUXEx14 = new Styling(null);
		styleVOEUXEx14.selection = new SelectionDefinition("points&labels", null, null, null); //$NON-NLS-1$
		styleVOEUXEx14.style = new StyleDefinition("Taille", "Proportionel à Cont1", null); //$NON-NLS-1$

		demoData.add(styleVOEUXEx11);
		demoData.add(styleVOEUXEx12);
		demoData.add(styleVOEUXEx13);
		demoData.add(styleVOEUXEx14);
		demos.put("Exemple 1", demoData); //$NON-NLS-1$

		demoData = new ArrayList<>();
		Styling styleVOEUX1 = new Styling(null);
		styleVOEUX1.selection = new SelectionDefinition("points&labels", null, null, "centre-ville | agglomération | année"); //$NON-NLS-1$
		styleVOEUX1.style = new StyleDefinition("Couleur", "Mauve", null); //$NON-NLS-1$

		Styling styleVOEUX2 = new Styling(null);
		styleVOEUX2.selection = new SelectionDefinition("points", null, null, "centre-ville | agglomération | année"); //$NON-NLS-1$
		styleVOEUX2.style = new StyleDefinition("Forme", "Up Triangle", null); //$NON-NLS-1$

		Styling styleVOEUX3 = new Styling(null);
		styleVOEUX3.selection = new SelectionDefinition("points", null, null, "centre-ville | agglomération | année"); //$NON-NLS-1$
		styleVOEUX3.style = new StyleDefinition("Remplissage", "Oui", null); //$NON-NLS-1$

		Styling styleVOEUX4 = new Styling(null);
		styleVOEUX4.selection = new SelectionDefinition("points&labels", null, null, "organiser | améliorer | renforcer"); //$NON-NLS-1$
		styleVOEUX4.style = new StyleDefinition("Couleur", "Rouge", null); //$NON-NLS-1$

		Styling styleVOEUX5 = new Styling(null);
		styleVOEUX5.selection = new SelectionDefinition("points", null, null, "organiser | améliorer | renforcer"); //$NON-NLS-1$
		styleVOEUX5.style = new StyleDefinition("Forme", "Cercle", null); //$NON-NLS-1$

		demoData.add(styleVOEUX1);
		demoData.add(styleVOEUX2);
		demoData.add(styleVOEUX3);
		demoData.add(styleVOEUX4);
		demoData.add(styleVOEUX5);

		demos.put("Exemple 2", demoData); //$NON-NLS-1$

		demoData = new ArrayList<>();
		Styling style1 = new Styling(null);
		style1.selection = new SelectionDefinition("points", null, null, null); //$NON-NLS-1$
		style1.style = new StyleDefinition("Transparence", "Proportionel à Q12", null); //$NON-NLS-1$

		Styling style2 = new Styling(null);
		style2.selection = new SelectionDefinition("points&labels", null, null, "av.*"); //$NON-NLS-1$
		style2.style = new StyleDefinition("Style pré-défini", null, null); //$NON-NLS-1$
		style2.style.name = "Mise en évidence1"; //$NON-NLS-1$
		style2.style.sub.add(new StyleDefinition("Forme", "Carré", style2.style)); //$NON-NLS-1$
		style2.style.sub.add(new StyleDefinition("Couleur", "Vert pomme d'api", style2.style)); //$NON-NLS-1$
		style2.style.sub.add(new StyleDefinition("Taille", "x 3", style2.style)); //$NON-NLS-1$
		style2.style.sub.add(new StyleDefinition("Police", "Arial", style2.style)); //$NON-NLS-1$
		demoData.add(style1);
		demoData.add(style2);

		demos.put("Demo", demoData); //$NON-NLS-1$

		demoData = new ArrayList<>();
	}

	public static String EMPTY = ""; //$NON-NLS-1$

	public CAStyleDemoUI(Composite parent) {

		super(parent, SWT.BORDER, "demo"); //$NON-NLS-1$

		GLComposite buttons = new GLComposite(this, SWT.NONE); //$NON-NLS-1$
		buttons.getLayout().numColumns = 20;

		new Label(buttons, SWT.NONE).setText("Feuilles de style "); //$NON-NLS-1$

		Combo demoscombo = new Combo(buttons, SWT.MULTI | SWT.READ_ONLY);
		demoscombo.setText("Demo"); //$NON-NLS-1$
		demoscombo.setItems(demos.keySet().toArray(new String[demos.size()]));
		demoscombo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				demoData.clear();
				for (String choice : demoscombo.getText().split(",")) { //$NON-NLS-1$
					ArrayList<Styling> d = demos.get(choice);
					if (d != null) {
						demoData.addAll(d);
					}
				}

				refreshViewers();
			}
		});

		new Label(buttons, SWT.NONE).setText("Ajouter/Retirer "); //$NON-NLS-1$

		Button addStyle = new Button(buttons, SWT.PUSH);
		addStyle.setText("+ Règle +"); //$NON-NLS-1$
		addStyle.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				demoData.add(new Styling(EMPTY));
				refreshViewers();
			}
		});

		Button addStyleDefinition = new Button(buttons, SWT.PUSH);
		addStyleDefinition.setText("+ Style +"); //$NON-NLS-1$
		addStyleDefinition.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				if (currentSelection instanceof Styling s) {
					s.style.sub.add(new StyleDefinition(EMPTY, EMPTY, s.style));
					refreshViewers();
				}
			}
		});

		Button removeStyle = new Button(buttons, SWT.PUSH);
		removeStyle.setText("- * -"); //$NON-NLS-1$
		removeStyle.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {


				if (currentSelection != null && currentSelection instanceof Styling s) {
					demoData.remove(s);
				}
				else if (currentSelection != null && currentSelection instanceof StyleDefinition sd && sd.parent != null) {
					sd.parent.sub.remove(sd);
				}

				refreshViewers();
			}
		});

		buildTree(parent);
		buildList(parent);
	}

	protected void refreshViewers() {

		for (Viewer viewer : viewers) {
			viewer.refresh();
		}
		if (tmpListener != null) tmpListener.selectionChanged(null);
	}

	protected void refreshSelection(Object sel, Viewer... origins) {

		if (sel == null) return;

		HashSet<Viewer> set = new HashSet(Arrays.asList(origins));
		for (Viewer viewer : viewers) {
			if (!set.contains(viewer)) {
				viewer.setSelection(new StructuredSelection(sel));
			}
		}
	}

	private void buildList(Composite parent) {

		GLComposite panels = new GLComposite(parent, SWT.NONE, "panels"); //$NON-NLS-1$
		panels.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		panels.getLayout().numColumns = 4;

		GLComposite rulesComposite = new GLComposite(panels, SWT.NONE, "rules"); //$NON-NLS-1$
		rulesComposite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		new Label(rulesComposite, SWT.NONE).setText("Règles"); //$NON-NLS-1$
		ListViewer listViewer = new ListViewer(rulesComposite, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		viewers.add(listViewer);
		listViewer.getList().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		listViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {

				currentSelection = listViewer.getStructuredSelection().getFirstElement();
				refreshSelection(currentSelection, listViewer);
			}
		});

		listViewer.setLabelProvider(new SimpleLabelProvider());

		listViewer.setContentProvider(new ITreeContentProvider() {

			@Override
			public boolean hasChildren(Object element) {

				if (element instanceof Styling s) {
					return s.style.sub.size() > 0;
				}
				return false;
			}

			@Override
			public Object getParent(Object element) {

				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object[] getElements(Object inputElement) {

				if (inputElement instanceof ArrayList list) {
					return list.toArray();
				}

				return null;
			}

			@Override
			public Object[] getChildren(Object parentElement) {

				if (parentElement instanceof Styling s) {
					return s.style.sub.toArray();
				}
				return new Object[0];
			}
		});

		listViewer.setInput(demoData);

		GLComposite parametersPanel = new GLComposite(panels, SWT.NONE, "parameters"); //$NON-NLS-1$
		parametersPanel.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		parametersPanel.getLayout().numColumns = 2;

		new Label(parametersPanel, SWT.NONE).setText("Selecteur"); //$NON-NLS-1$
		new Label(parametersPanel, SWT.NONE).setText(""); //$NON-NLS-1$

		new Label(parametersPanel, SWT.NONE).setText("Nom"); //$NON-NLS-1$

		Text nameCombo = new Text(parametersPanel, SWT.BORDER);
		nameCombo.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {

				Object o = listViewer.getStructuredSelection().getFirstElement();
				if (o != null && o instanceof Styling s) {
					s.selection.name = nameCombo.getText();
				}
				refreshViewers();
			}
		});

		new Label(parametersPanel, SWT.NONE).setText("Variable - Cible"); //$NON-NLS-1$
		Combo objectCombo = new Combo(parametersPanel, SWT.SINGLE | SWT.READ_ONLY | SWT.BORDER);
		objectCombo.setItems(dummyObjects);
		objectCombo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				Object o = listViewer.getStructuredSelection().getFirstElement();
				if (o != null && o instanceof Styling s) {
					s.selection.object = objectCombo.getText();
				}
				refreshViewers();
			}
		});
		new Label(parametersPanel, SWT.NONE).setText("Variable - Propriété"); //$NON-NLS-1$
		Text propertiesCombo = new Text(parametersPanel, SWT.BORDER);
		propertiesCombo.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {

				Object o = listViewer.getStructuredSelection().getFirstElement();
				if (o != null && o instanceof Styling s) {
					s.selection.property = propertiesCombo.getText();
				}
				refreshViewers();
			}
		});

		new Label(parametersPanel, SWT.NONE).setText("Opérateur+Valeur - Seuil"); //$NON-NLS-1$
		Text thresholdCombo = new Text(parametersPanel, SWT.BORDER);
		thresholdCombo.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {

				Object o = listViewer.getStructuredSelection().getFirstElement();
				if (o != null && o instanceof Styling s) {
					s.selection.threshold = thresholdCombo.getText();
				}
				refreshViewers();
			}
		});

		new Label(parametersPanel, SWT.NONE).setText("Variable - Etiquette"); //$NON-NLS-1$
		Text labelCombo = new Text(parametersPanel, SWT.BORDER);
		labelCombo.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {

				Object o = listViewer.getStructuredSelection().getFirstElement();
				if (o != null && o instanceof Styling s) {
					s.selection.label = labelCombo.getText();
				}
				refreshViewers();
			}
		});

		GLComposite styleComposite = new GLComposite(panels, SWT.NONE, "style"); //$NON-NLS-1$
		styleComposite.getLayout().numColumns = 2;
		styleComposite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		new Label(styleComposite, SWT.NONE).setText("Style"); //$NON-NLS-1$
		new Label(styleComposite, SWT.NONE).setText(""); //$NON-NLS-1$

		new Label(styleComposite, SWT.NONE).setText("Nom"); //$NON-NLS-1$

		Text nameStyleCombo = new Text(styleComposite, SWT.BORDER);
		nameStyleCombo.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {

				Object o = listViewer.getStructuredSelection().getFirstElement();
				if (o != null && o instanceof StyleDefinition sd) {
					sd.name = nameStyleCombo.getText();
				}
				refreshViewers();
			}
		});

		new Label(styleComposite, SWT.NONE).setText("Type"); //$NON-NLS-1$
		Combo styleCombo = new Combo(styleComposite, SWT.SINGLE | SWT.READ_ONLY | SWT.BORDER);
		styleCombo.setItems(dummyStyleTypes);


		new Label(styleComposite, SWT.NONE).setText("Valeur"); //$NON-NLS-1$
		Combo valueCombo = new Combo(styleComposite, SWT.SINGLE | SWT.READ_ONLY | SWT.BORDER);
		valueCombo.setItems(dummyStyleValues);
		valueCombo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				Object o = listViewer.getStructuredSelection().getFirstElement();
				if (o != null && o instanceof Styling s) {
					s.style.value = valueCombo.getText();
				}
				refreshViewers();
			}
		});



		TableViewer tableViewer = new TableViewer(styleComposite, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		styleCombo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				Object o = listViewer.getStructuredSelection().getFirstElement();
				if (o != null && o instanceof Styling s) {
					s.style.type = styleCombo.getText();
				}
				refreshViewers();
			}
		});
		viewers.add(tableViewer);
		tableViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {

				currentSelection = tableViewer.getStructuredSelection().getFirstElement();
				refreshSelection(currentSelection, tableViewer);
			}
		});
		tableViewer.getTable().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 2, 1));
		tableViewer.getTable().setHeaderVisible(true);
		tableViewer.getTable().setLinesVisible(true);

		TableViewerColumn stylecolumn = new TableViewerColumn(tableViewer, SWT.NONE);
		stylecolumn.getColumn().setText("Type"); //$NON-NLS-1$
		stylecolumn.getColumn().setWidth(300);
		stylecolumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof Styling s) {
					return s.style.type;
				}
				else if (element instanceof StyleDefinition sd) {
					return sd.type;
				}
				return EMPTY;
			}
		});

		TableViewerColumn styleValuecolumn = new TableViewerColumn(tableViewer, SWT.NONE);
		styleValuecolumn.getColumn().setText("Valeur"); //$NON-NLS-1$
		styleValuecolumn.getColumn().setWidth(300);
		styleValuecolumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof Styling s) {
					return s.style.value;
				}
				else if (element instanceof StyleDefinition sd) {
					return sd.value;
				}
				return EMPTY;
			}
		});

		tableViewer.setContentProvider(new ArrayContentProvider());

		tableViewer.getTable().pack();

		tmpListener = new ISelectionChangedListener() {

			private void setOrEmpty(Text t, String value) {
				Listener m = null;
				if (t.getListeners(SWT.Modify).length > 0) {
					m = t.getListeners(SWT.Modify)[0];
					t.removeListener(SWT.Modify, m);
				}
				if (value == null) t.setText(EMPTY);
				else t.setText(value);
				if (m != null) t.addListener(SWT.Modify, m);
			}

			private void setOrEmpty(Combo t, String value) {
				Listener m = null;
				if (t.getListeners(SWT.Modify).length > 0) {
					m = t.getListeners(SWT.Modify)[0];
					t.removeListener(SWT.Modify, m);
				}
				if (value == null) t.setText(EMPTY);
				else t.setText(value);
				if (m != null) t.addListener(SWT.Modify, m);
			}

			@Override
			public void selectionChanged(SelectionChangedEvent event) {

				Object o = listViewer.getStructuredSelection().getFirstElement();

				if (o instanceof Styling s) {
					setOrEmpty(nameCombo, s.selection.name);
					setOrEmpty(objectCombo, s.selection.object);
					setOrEmpty(propertiesCombo, s.selection.property);
					setOrEmpty(thresholdCombo, s.selection.threshold);
					setOrEmpty(labelCombo, s.selection.label);
					setOrEmpty(styleCombo, s.style.type);
					setOrEmpty(valueCombo, s.style.value);
					setOrEmpty(nameStyleCombo, s.style.name);
					if (event != null) tableViewer.setInput(s.style.sub);
					if ("Style pré-défini".equals(s.style.type)) { //$NON-NLS-1$
						tableViewer.getTable().setVisible(true);
					}
					else {
						tableViewer.getTable().setVisible(false);
					}
				}
				currentSelection = o;
				refreshSelection(currentSelection, tableViewer, listViewer);
			}
		};
		listViewer.addSelectionChangedListener(tmpListener);

		listViewer.getList().pack();
	}

	public class StylingEditSupport extends EditingSupport {

		String[] svalues, sdvalues;

		ComboBoxCellEditor cellEditor, cellEditor2;

		public StylingEditSupport(TreeViewer treeViewer, String[] svalues, String[] sdvalues) {

			super(treeViewer);

			this.svalues = svalues;
			this.sdvalues = sdvalues;
			// TODO use ComboBoxViewerCellEditor instead, seams simplier
			cellEditor = new ComboBoxCellEditor(treeViewer.getTree(), svalues, SWT.READ_ONLY | SWT.SINGLE);
			cellEditor2 = new ComboBoxCellEditor(treeViewer.getTree(), sdvalues, SWT.READ_ONLY | SWT.SINGLE);

		}

		@Override
		protected void setValue(Object element, Object value) {

			if (element instanceof Styling s) {
				if (value instanceof Integer i) {
					if (i == -1) return;
					saveObjectValue(s, svalues[i]);

					getViewer().update(element, null);
					refreshViewers();
				}
			}
			else if (element instanceof StyleDefinition sd) {
				if (value instanceof Integer i) {
					if (i == -1) return;
					saveObjectValue(sd, sdvalues[i]);

					getViewer().update(element, null);
					refreshViewers();
				}
			}
		}

		protected void saveObjectValue(Styling s, String v) {
		};

		protected String getObjectValue(Styling s) {
			return EMPTY;
		};

		protected void saveObjectValue(StyleDefinition sd, String v) {
		};

		protected String getObjectValue(StyleDefinition sd) {
			return EMPTY;
		};

		@Override
		protected Object getValue(Object element) {

			if (element instanceof Styling s) {
				if (getObjectValue(s) == null) return -1;
				return Arrays.binarySearch(svalues, getObjectValue(s));
			}
			else if (element instanceof StyleDefinition sd) {
				if (getObjectValue(sd) == null) return -1;
				return Arrays.binarySearch(sdvalues, getObjectValue(sd));
			}
			return -1;
		}

		@Override
		protected CellEditor getCellEditor(Object element) {

			if (element instanceof Styling s) {
				return cellEditor;
			}
			else if (element instanceof StyleDefinition sd) {
				return cellEditor2;
			}
			return null;
		}

		@Override
		protected boolean canEdit(Object element) {

			if (element instanceof Styling s) {
				return true;
			}
			else if (element instanceof StyleDefinition sd) {
				return true;
			}
			return false;
		}
	}


	private void buildTree(Composite parent) {

		TreeViewer treeViewer = new TreeViewer(this, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		viewers.add(treeViewer);
		treeViewer.getTree().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		treeViewer.getTree().setHeaderVisible(true);
		treeViewer.getTree().setLinesVisible(true);

		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {

				currentSelection = treeViewer.getStructuredSelection().getFirstElement();
			}
		});

		// To draw the bar plot column
		treeViewer.getTree().addListener(SWT.EraseItem, new Listener() {

			@Override
			public void handleEvent(Event event) {


				TreeItem it = (TreeItem) event.item;
				//int itemIndex = Arrays.binarySearch(treeViewer.getTree().getItems(), it);
				Object o = it.getData();
				if (o instanceof Styling) {
					Rectangle rect = event.getBounds();
					event.gc.setBackground(treeViewer.getTree().getDisplay().getSystemColor(SWT.COLOR_GRAY));
					event.gc.fillRectangle(rect);
				}
			}
		});

		TreeViewerColumn objectSelectionColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		objectSelectionColumn.getColumn().setText("Cible"); //$NON-NLS-1$
		objectSelectionColumn.getColumn().setWidth(150);
		objectSelectionColumn.getColumn().setResizable(true);
		objectSelectionColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof Styling s) {
					if (s.selection.object == null) {
						return "<select object>"; //$NON-NLS-1$
					}
					else {
						return s.selection.object != null ? s.selection.object : EMPTY;
					}
				}
				return EMPTY;
			}
		});
		objectSelectionColumn.setEditingSupport(new StylingEditSupport(treeViewer, dummyObjects, new String[0]) {

			@Override
			protected void saveObjectValue(Styling s, String v) {

				s.selection.object = v;
			}

			@Override
			protected String getObjectValue(Styling s) {

				return s.selection.object;
			}
		});

		TreeViewerColumn propertySelectionColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		propertySelectionColumn.getColumn().setText("Propriété"); //$NON-NLS-1$
		propertySelectionColumn.getColumn().setWidth(100);
		propertySelectionColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof Styling s) {
					return s.selection.property != null ? s.selection.property : EMPTY;
				}
				return EMPTY;
			}
		});
		propertySelectionColumn.setEditingSupport(new StylingEditSupport(treeViewer, dummyProperties, new String[0]) {

			@Override
			protected void saveObjectValue(Styling s, String v) {

				s.selection.property = v;
			}

			@Override
			protected String getObjectValue(Styling s) {

				return s.selection.property;
			}
		});

		TreeViewerColumn thresholdSelectionColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		thresholdSelectionColumn.getColumn().setText("Seuil"); //$NON-NLS-1$
		thresholdSelectionColumn.getColumn().setWidth(100);
		thresholdSelectionColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof Styling s) {
					return s.selection.threshold != null ? s.selection.threshold : EMPTY;
				}
				return EMPTY;
			}
		});
		thresholdSelectionColumn.setEditingSupport(new StylingEditSupport(treeViewer, dummyThresholds, new String[0]) {

			@Override
			protected void saveObjectValue(Styling s, String v) {

				s.selection.threshold = v;
			}

			@Override
			protected String getObjectValue(Styling s) {

				return s.selection.threshold;
			}
		});
		TreeViewerColumn labelSelectionColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		labelSelectionColumn.getColumn().setText("Etiquette"); //$NON-NLS-1$
		labelSelectionColumn.getColumn().setWidth(100);
		labelSelectionColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof Styling s) {
					return s.selection.label != null ? s.selection.label : EMPTY;
				}
				return EMPTY;
			}
		});
		labelSelectionColumn.setEditingSupport(new EditingSupport(treeViewer) {

			TextCellEditor cellEditor = new TextCellEditor(treeViewer.getTree());

			@Override
			protected void setValue(Object element, Object value) {

				if (element instanceof Styling s) {
					s.selection.label = value.toString();
					getViewer().update(element, null);
					refreshViewers();
				}
			}

			@Override
			protected Object getValue(Object element) {

				if (element instanceof Styling s && s.selection.label != null) {
					return s.selection.label;
				}
				return EMPTY;
			}

			@Override
			protected CellEditor getCellEditor(Object element) {

				return cellEditor;
			}

			@Override
			protected boolean canEdit(Object element) {

				return element instanceof Styling;
			}
		});
		TreeViewerColumn stylecolumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		stylecolumn.getColumn().setText("Type"); //$NON-NLS-1$
		stylecolumn.getColumn().setWidth(300);
		stylecolumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof Styling s) {
					return s.style.type;
				}
				else if (element instanceof StyleDefinition sd) {
					return sd.type;
				}
				return EMPTY;
			}
		});
		stylecolumn.setEditingSupport(new StylingEditSupport(treeViewer, dummyStyleTypes, dummyStyleTypes) {

			@Override
			protected void saveObjectValue(Styling s, String v) {

				s.style.type = v;
			}

			@Override
			protected String getObjectValue(Styling s) {

				return s.style.type;
			}

			@Override
			protected void saveObjectValue(StyleDefinition sd, String v) {

				sd.type = v;
			}

			@Override
			protected String getObjectValue(StyleDefinition sd) {

				return sd.type;
			}
		});

		TreeViewerColumn styleValuecolumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		styleValuecolumn.getColumn().setText("Valeur"); //$NON-NLS-1$
		styleValuecolumn.getColumn().setWidth(300);
		styleValuecolumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof Styling s) {
					return s.style.value;
				}
				else if (element instanceof StyleDefinition sd) {
					return sd.value;
				}
				return EMPTY;
			}
		});
		styleValuecolumn.setEditingSupport(new StylingEditSupport(treeViewer, dummyStyleValues, dummyStyleValues) {

			@Override
			protected void saveObjectValue(Styling s, String v) {

				s.style.value = v;
			}

			@Override
			protected String getObjectValue(Styling s) {

				return s.style.value;
			}

			@Override
			protected void saveObjectValue(StyleDefinition sd, String v) {

				sd.value = v;
			}

			@Override
			protected String getObjectValue(StyleDefinition sd) {

				return sd.value;
			}
		});
		treeViewer.setContentProvider(new ITreeContentProvider() {

			@Override
			public boolean hasChildren(Object element) {

				if (element instanceof Styling s) {
					return s.style.sub.size() > 0;
				}
				return false;
			}

			@Override
			public Object getParent(Object element) {

				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object[] getElements(Object inputElement) {

				if (inputElement instanceof ArrayList list) {
					return list.toArray();
				}
				return null;
			}

			@Override
			public Object[] getChildren(Object parentElement) {

				if (parentElement instanceof Styling s) {
					return s.style.sub.toArray();
				}
				return new Object[0];
			}
		});

		treeViewer.setInput(demoData);

		treeViewer.getTree().pack();
	}

	public static void main(String[] args) {
		Display display = new Display();

		Shell shell = new Shell(display);
		shell.setText("DEMO CA STYLES"); //$NON-NLS-1$
		shell.setMinimumSize(400, 300);
		shell.setLayout(new GridLayout());

		CAStyleDemoUI c = new CAStyleDemoUI(shell);
		c.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	private static class Styling {

		public Styling(String name) {
			this.name = name;
		}

		public String name;

		public SelectionDefinition selection = new SelectionDefinition("points&labels", "", "", ""); //$NON-NLS-1$

		public StyleDefinition style = new StyleDefinition(null, null, null);

		@Override
		public String toString() {

			if (name != null && name.length() > 0) return name;

			StringBuffer buffer = new StringBuffer();
			buffer.append("Les " + selection.toString() + " -> " + style.toString()); //$NON-NLS-1$
			return buffer.toString();
		}
	}

	private static class SelectionDefinition {

		public SelectionDefinition(String object, String property, String threshold, String label) {
			this.object = object;
			this.property = property;
			this.threshold = threshold;
			this.label = label;
		}

		public String name;

		public String object;

		public String property;

		public String threshold;

		public String label;

		@Override
		public String toString() {
			if (name != null && name.length() > 0) {
				return name;
			}
			else {
				return "object=" + object + " property=" + property + " threshold=" + threshold + " label=" + label; //$NON-NLS-1$
			}
		}
	}

	private static class StyleDefinition {

		public StyleDefinition(String type, String value, StyleDefinition parent) {
			this.type = type;
			this.value = value;
			this.parent = parent;
		}

		public String name;

		public StyleDefinition parent;

		public String type;

		public String value;

		public ArrayList<StyleDefinition> sub = new ArrayList<StyleDefinition>();

		@Override
		public String toString() {
			if (name != null && name.length() > 0) {
				return name;
			}
			else {
				return "type=" + type + " value=" + value; //$NON-NLS-1$
			}
		}
	}
}
