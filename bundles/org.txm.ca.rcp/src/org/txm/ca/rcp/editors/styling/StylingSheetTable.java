package org.txm.ca.rcp.editors.styling;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TreeItem;
import org.txm.ca.core.chartsengine.styling.CAStylerCatalog;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.ca.core.preferences.CAPreferences;
import org.txm.ca.rcp.editors.CAFactorialMapChartEditor;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.styling.Style;
import org.txm.chartsengine.core.styling.StylingRule;
import org.txm.chartsengine.core.styling.StylingSheet;
import org.txm.chartsengine.core.styling.catalogs.SelectionInstructionsCatalog;
import org.txm.chartsengine.core.styling.catalogs.StylingInstructionsCatalog;
import org.txm.chartsengine.core.styling.definitions.InstructionDefinition;
import org.txm.chartsengine.core.styling.definitions.ParameterDefinition;
import org.txm.chartsengine.core.styling.definitions.SelectionInstructionDefinition;
import org.txm.chartsengine.core.styling.definitions.StylingInstructionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.ConstantFunctionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.FunctionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.TestFunctionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.TransformationFunctionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.UserDefinedValueFunctionDefinition;
import org.txm.chartsengine.core.styling.instructions.SelectionInstruction;
import org.txm.chartsengine.core.styling.instructions.StylingInstruction;
import org.txm.core.results.TXMResult;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.jface.TreeViewerSeparatorColumn;
import org.txm.rcp.swt.provider.SimpleLabelProvider;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;
import org.txm.utils.serializable.Color1Color2;
import org.txm.utils.serializable.MinMax;
import org.txm.utils.serializable.PatternReplace;

/**
 * A table filled with a StylingSheet rules for CA results
 * 
 * Rules can be edited, added or deleted
 * 
 * @author mdecorde
 *
 */
public class StylingSheetTable extends GLComposite {

	public static final ArrayList<String> AVAILABLE_DIMENSIONS = new ArrayList<>();
	public static final ArrayList<String> AVAILABLE_DIMENSIONS_LABELS = new ArrayList<>(); 
	static { // this is necessary to make TXM workds with diff language
		AVAILABLE_DIMENSIONS.add(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS);
		AVAILABLE_DIMENSIONS.add(CAStylerCatalog.VALUE_ID_DIMENSION_COLS);
		AVAILABLE_DIMENSIONS.add(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS_COLS);
		AVAILABLE_DIMENSIONS_LABELS.add(CACoreMessages.rows);
		AVAILABLE_DIMENSIONS_LABELS.add(CACoreMessages.columns);
		AVAILABLE_DIMENSIONS_LABELS.add(CACoreMessages.rowsAndColumns);
	}

	public static final String[] AVAILABLE_TARGETS = new String[] { // no need to translate this ones in the current implementation
			SelectionInstruction.VALUE_ID_TARGET_LABELS,
			SelectionInstruction.VALUE_ID_TARGET_SHAPES,
			SelectionInstruction.VALUE_ID_TARGET_LABELS_AND_SHAPES};

	//	LinkedHashSet<StylingSheet> demos = new LinkedHashSet<>();

	StylingSheet selectedStylingData;

	private TreeViewer treeViewer;

	private Object currentSelection;

	public static String EMPTY = ""; //$NON-NLS-N$

	CAFactorialMapChartEditor editor = null;

	private ComboViewer demoscombo;

	CAStylerCatalog catalog = null;

	private TreeViewerColumn dimensionSelectionColumn;

	private TreeViewerColumn targetSelectionColumn;

	private TreeViewerColumn styleColumn;

	private TreeViewerColumn styleValueColumn;

	private TreeViewerColumn styleSettingsColumn;

	private TreeViewerColumn informationSelectionColumn;

	private TreeViewerColumn thresholdSelectionColumn;

	private TreeViewerColumn labelSelectionColumn;

	private Button moreParameters;

	private TreeViewerColumn activationColumn;

	private Button newStyleSheet;

	private Button cloneStyleSheet;

	private Button saveStyleSheetAs;

	private Button deleteStyleSheet;

	private Button importStyleSheet;

	private Button displayAndExportStyleSheet;
	private TreeViewerColumn sep2;
	private TreeViewerColumn sep3;

	public StylingSheetTable(Composite parent, ComboViewer demoscombo2, CAFactorialMapChartEditor editor) {

		super(parent, SWT.NONE);

		this.catalog = editor.getResult().getCatalogs();

		this.demoscombo = demoscombo2;

		this.editor = editor;
		buildControls(); // build the toolbar buttons
		buildTree(); // build the table and columns

		// fill styling sheets combo with available StylingSheets
		demoscombo.setInput(catalog.getStyleSheets());

		// get the default StylingSheet if none is set in the CA
		selectedStylingData = editor.getResult().getStylingSheet();
		if (selectedStylingData == null) { // if no StylingSheet set to the result, set a default one
			
			String defaultStylingsheetName = CAPreferences.getInstance().getString(CAPreferences.DEFAULT_STYLINGSHEET);
			if (defaultStylingsheetName != null && defaultStylingsheetName.length() > 0) {
				selectedStylingData = catalog.getStylingSheetByName(defaultStylingsheetName);
			}
			if (selectedStylingData == null) {
				selectedStylingData = catalog.getDefaultStyleSheet();
			}
		}

		// updates the table content
		if (selectedStylingData == null) { // if the catalog has no default stylingsheet, use the first one
			for (StylingSheet firstStylingSheet : catalog.getStyleSheets()) {
				selectedStylingData = firstStylingSheet;
				break;
			}
		}

		demoscombo.setSelection(new StructuredSelection(selectedStylingData)); 
	}

	public StylingSheet getStylingSheet() {
		return selectedStylingData;
	}

	private void buildControls() {

		GLComposite buttons = new GLComposite(this, SWT.NONE);
		buttons.getLayout().numColumns = 20;

		if (demoscombo == null) { 
			demoscombo = new ComboViewer(buttons, SWT.MULTI | SWT.READ_ONLY);
			demoscombo.setLabelProvider(new SimpleLabelProvider());
			demoscombo.setContentProvider(new ArrayContentProvider());
		}
		GridData gdata = new GridData(GridData.FILL, GridData.FILL, false, true);
		gdata.minimumWidth = gdata.widthHint = 150;
		demoscombo.getCombo().setLayoutData(gdata);
		demoscombo.getCombo().setText(CAUIMessages.stylingSheet);
		demoscombo.getCombo().setToolTipText(CAUIMessages.availableStylingSheets);
		demoscombo.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {

				if (event.getStructuredSelection().isEmpty()) return;

				selectedStylingData = (StylingSheet) event.getStructuredSelection().getFirstElement();

				editor.getResult().setStylingSheet(selectedStylingData);
				editor.getResult().getChartCreator().updateChart(editor.getResult());
				treeViewer.setInput(selectedStylingData);


				updateInterface();
			}
		});

		newStyleSheet = new Button(buttons, SWT.PUSH);
		newStyleSheet.setText(CAUIMessages.neww);
		newStyleSheet.setToolTipText(CAUIMessages.createANewStylingSheetCurrentOneWillBelostIfNotSaved);
		newStyleSheet.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				selectedStylingData = new StylingSheet();
				selectedStylingData.add(catalog.newDefaultRule());
				int n = 2;
				String basename = selectedStylingData.getName();
				while (catalog.contains(selectedStylingData)) {
					selectedStylingData.setName(basename+" "+(n++)); //$NON-NLS-1$
				}
				editor.getResult().setStylingSheet(selectedStylingData);
				catalog.add(selectedStylingData);
				demoscombo.refresh();
				demoscombo.setSelection(new StructuredSelection(selectedStylingData));
			}
		});

		cloneStyleSheet = new Button(buttons, SWT.PUSH);
		cloneStyleSheet.setText(CAUIMessages.clone);
		cloneStyleSheet.setToolTipText(CAUIMessages.cloneCurrentStylingSheetTheNewOneWillBeEditableByDefault);
		cloneStyleSheet.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				if (selectedStylingData == null) return;

				selectedStylingData = selectedStylingData.clone();
				String basename = selectedStylingData.getName();
				int n = 2;
				selectedStylingData.setName(CAUIMessages.bind(CAUIMessages.copyP0OfP1,"", basename));
				while (catalog.contains(selectedStylingData)) {
					selectedStylingData.setName(CAUIMessages.bind(CAUIMessages.copyP0OfP1,n++, basename));
				}
				selectedStylingData.setEditable(true);

				catalog.add(selectedStylingData);
				demoscombo.refresh();

				editor.getResult().setStylingSheet(selectedStylingData);
				demoscombo.setSelection(new StructuredSelection(selectedStylingData));
			}
		});

		saveStyleSheetAs = new Button(buttons, SWT.PUSH);
		saveStyleSheetAs.setText(CAUIMessages.save);
		saveStyleSheetAs.setToolTipText(CAUIMessages.saveCurrentStylingsheetModificationsChangeItsNameIfNecessary);
		saveStyleSheetAs.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				if (selectedStylingData == null) return; // error
				if (!selectedStylingData.isEditable()) return; // error

				InputDialog dialog = new InputDialog(e.display.getActiveShell(), CAUIMessages.saveStylingSheet, CAUIMessages.stylingSheetName, selectedStylingData.getName(), new IInputValidator() {

					@Override
					public String isValid(String newText) {
						for (StylingSheet sheet : catalog.getStyleSheets()) {
							if (!selectedStylingData.getName().equals(newText)
									&& sheet.getName().equals(newText)) {
								return CAUIMessages.errorStylingSheetNameAlreadyUsed;
							}
						}
						if (newText.trim().length() == 0) {
							return CAUIMessages.errorStylingSheetNameIsEmpty;
						}
						return null;
					}
				});
				if (dialog.open() == Window.OK && dialog.getValue().length() > 0) {

					String oldName = null;
					if (!selectedStylingData.getName().equals(dialog.getValue())) {
						oldName = selectedStylingData.getName();
					}
					selectedStylingData.setName(dialog.getValue());
					demoscombo.refresh();
					try {
						catalog.persistUserStyleSheet(selectedStylingData);
						if (oldName != null) {
							catalog.removePersistedUserStyleSheet(oldName);
						}
					}
					catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});

		deleteStyleSheet = new Button(buttons, SWT.PUSH);
		deleteStyleSheet.setText(CAUIMessages.delete);
		deleteStyleSheet.setToolTipText(CAUIMessages.deleteTheCurrentStylingSheet);
		deleteStyleSheet.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				if (selectedStylingData == null) return;
				if (!selectedStylingData.isEditable()) return;

				catalog.remove(selectedStylingData);

				for (StylingSheet s : catalog.getStyleSheets()) {
					selectedStylingData = s;
					break;
				}

				demoscombo.refresh();
				editor.getResult().setStylingSheet(selectedStylingData);
				demoscombo.setSelection(new StructuredSelection(selectedStylingData));
				return;
			}
		});

		importStyleSheet = new Button(buttons, SWT.PUSH);
		importStyleSheet.setText(CAUIMessages.import3Dot);
		importStyleSheet.setToolTipText(CAUIMessages.importAStylingSheetFromTheJSONExportString);
		importStyleSheet.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				FileDialog dialog = new FileDialog(e.display.getActiveShell(), SWT.OPEN);
				dialog.setFilterExtensions(new String[] {"*.json"});
				String path = dialog.open();
				if (path == null) {
					Log.info("Aborted");
					return;
				}
				try {
					String json = IOUtils.getText(new File(path));

					StylingSheet sheet = catalog.JSONToStylingSheet(json.trim());
					Log.info("StylingSheet imported to "+path);

					selectedStylingData = sheet;
					String basename = selectedStylingData.getName();
					int n = 1;
					while (catalog.contains(selectedStylingData)) {
						if (n == 1) {
							selectedStylingData.setName(CAUIMessages.bind(CAUIMessages.copyP0OfP1, "", basename));
						} else {
							selectedStylingData.setName(CAUIMessages.bind(CAUIMessages.copyP0OfP1, n++, basename));
						}
					}
					selectedStylingData.setEditable(true);
					catalog.add(selectedStylingData);
					demoscombo.refresh();
					editor.getResult().setStylingSheet(selectedStylingData);
					demoscombo.setSelection(new StructuredSelection(selectedStylingData));
				} catch(Exception e2) {
					e2.printStackTrace();
				}

			}
		});

		displayAndExportStyleSheet = new Button(buttons, SWT.PUSH);
		displayAndExportStyleSheet.setText(CAUIMessages.export+"..."); //$NON-NLS-1$
		displayAndExportStyleSheet.setToolTipText(CAUIMessages.printTheStylingSheetRulesDescriptionAndJSONStringCanBeUsedToReImportTheStylingSheet);
		displayAndExportStyleSheet.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				System.out.println(selectedStylingData.getTableDump());
				try {
					FileDialog dialog = new FileDialog(e.display.getActiveShell(), SWT.SAVE);
					dialog.setFilterExtensions(new String[] {"*.json"});
					String path = dialog.open();
					if (path != null) {
						IOUtils.write(new File(path), catalog.StylingSheetToJSON(selectedStylingData));
						Log.info("StylingSheet exported to "+path);
					}
				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		new Label(buttons, SWT.NONE).setText(" | "); //$NON-NLS-1$

		moreParameters = new Button(buttons, SWT.TOGGLE);
		moreParameters.setText(CAUIMessages.moreChev);
		moreParameters.setToolTipText(CAUIMessages.displayMoreStyleOptionsAndSelection);
		moreParameters.setSelection(false);
		moreParameters.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				updateHideShowMoreParameters();
			}
		});

		//		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
		//			CACategorizeParameterPanel categorizePanel = new CACategorizeParameterPanel(buttons, editor.getResult());
		//		}
	}

	/**
	 * Updates enable states of the components + pack columns
	 */
	protected void updateInterface() {

		treeViewer.getTree().setEnabled(selectedStylingData.isEditable());

		TableUtils.packColumns(treeViewer);

		updateButtonStates();

		updateHideShowMoreParameters();
	}

	/**
	 * Update the button states
	 */
	protected void updateButtonStates() {
		newStyleSheet.setEnabled(true);
		cloneStyleSheet.setEnabled(true);
		importStyleSheet.setEnabled(true);
		displayAndExportStyleSheet.setEnabled(true);
		saveStyleSheetAs.setEnabled(selectedStylingData.isEditable());
		deleteStyleSheet.setEnabled(selectedStylingData.isEditable());
	}

	/**
	 * Shows advanced settings dependign on the moreParameters button selection
	 */
	protected void updateHideShowMoreParameters() {

		if (!moreParameters.getSelection()) {
			styleSettingsColumn.getColumn().setWidth(0);
			informationSelectionColumn.getColumn().setWidth(0);
			sep2.getColumn().setWidth(0);
			thresholdSelectionColumn.getColumn().setWidth(0);
			sep3.getColumn().setWidth(0);
			labelSelectionColumn.getColumn().setWidth(0);

			styleSettingsColumn.getColumn().setResizable(false);
			informationSelectionColumn.getColumn().setResizable(false);
			thresholdSelectionColumn.getColumn().setResizable(false);
			labelSelectionColumn.getColumn().setResizable(false);

			moreParameters.setText(CAUIMessages.moreChev);
			moreParameters.setToolTipText(CAUIMessages.displayMoreStyleOptionsAndSelection);
		}
		else {
			styleSettingsColumn.getColumn().pack();
			sep2.getColumn().pack();
			informationSelectionColumn.getColumn().pack();
			thresholdSelectionColumn.getColumn().pack();
			sep3.getColumn().pack();
			labelSelectionColumn.getColumn().pack();

			styleSettingsColumn.getColumn().setResizable(true);
			informationSelectionColumn.getColumn().setResizable(true);
			thresholdSelectionColumn.getColumn().setResizable(true);
			labelSelectionColumn.getColumn().setResizable(true);

			moreParameters.setText(CAUIMessages.chevLess);
			moreParameters.setToolTipText(CAUIMessages.hideTheSupplmentaryOptionsAndSelection);
			moreParameters.getParent().layout(true);
		}
	}

	/**
	 * Add a new rule depending on the table selection:
	 * if a rule is selected: partially duplicate it
	 * if no rule is selected get a default rule from the result catalog
	 */
	private void addNewRule() {

		if (selectedStylingData == null) return;
		if (!selectedStylingData.isEditable()) return;

		int idx = -1; // the selected rule index in table. -1 if no selection
		if (treeViewer.getStructuredSelection().getFirstElement() != null) {
			idx = selectedStylingData.getRules().indexOf(treeViewer.getStructuredSelection().getFirstElement());
		}

		StylingRule s = null;
		if (idx >= 0) {
			StylingRule selectedRule = (StylingRule) treeViewer.getStructuredSelection().getFirstElement();
			s = selectedRule.clone();
			s.getStyle().getStylingInstructions().get(0).setFunctionDefinition(null); // unset the function (propertionel à)
			s.getStyle().getStylingInstructions().get(0).setParameterDefinition(null); // unset the parameter (cos2)
			selectedStylingData.addAt(idx+1, s); // insert AFTER
		} else {
			s = catalog.newDefaultRule();
			selectedStylingData.add(s); // add at the end
		}
		treeViewer.refresh();
		treeViewer.setSelection(new StructuredSelection(s));
	}

	/**
	 * Build the TreeViewer + its columns
	 * install Events, the LabelProviders and EditSupports
	 */
	private void buildTree() {

		treeViewer = new TreeViewer(this, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
		GridData gdata = new GridData(GridData.FILL, GridData.FILL, true, true);
		gdata.minimumHeight = gdata.heightHint = 200;
		treeViewer.getTree().setLayoutData(gdata);
		treeViewer.getTree().setHeaderVisible(true);
		treeViewer.getTree().setLinesVisible(true);

		// To highligh Styles content -> not used yet
		treeViewer.getTree().addListener(SWT.EraseItem, new Listener() {

			@Override
			public void handleEvent(Event event) {

				TreeItem it = (TreeItem) event.item;
				//int itemIndex = Arrays.binarySearch(treeViewer.getTree().getItems(), it);
				Object o = it.getData();
				if (o instanceof Style) {
					Rectangle rect = event.getBounds();
					event.gc.setBackground(treeViewer.getTree().getDisplay().getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
					event.gc.fillRectangle(rect);
				}
			}
		});

		TreeViewerColumn sepColumn = new TreeViewerColumn(treeViewer, SWT.NONE); // presentation purpose
		sepColumn.getColumn().setText(" "); //$NON-NLS-1$
		sepColumn.getColumn().setWidth(1);
		sepColumn.getColumn().setResizable(false);
		sepColumn.setLabelProvider(new CellLabelProvider() {

			@Override
			public void update(ViewerCell cell) { }
		});

		activationColumn = new TreeViewerColumn(treeViewer, SWT.NONE); // display a a checkbox and is clickable to activate|deactivate a Stylingrule
		activationColumn.getColumn().setText(" "); //$NON-NLS-1$
		activationColumn.getColumn().setToolTipText(CAUIMessages.enableDisableAStylingRule);
		activationColumn.getColumn().setWidth(20);
		activationColumn.getColumn().setResizable(false);
		activationColumn.setLabelProvider(new ColumnLabelProvider() {

			Image selectedImage = IImageKeys.getImage(IImageKeys.CHECKEDBOX_SELECTED);
			Image unselectedImage = IImageKeys.getImage(IImageKeys.CHECKEDBOX_CLEARED);

			@Override
			public String getText(Object element) {
				return EMPTY;
			}

			@Override
			public Image getImage(Object element) {
				if (element instanceof StylingRule s) {
					if (s.isActivated()) {
						return selectedImage;
					} else {
						return unselectedImage;
					}
				}
				return null;
			}
		});

		// install mouse listener to manage click on the checkbox images
		TableUtils.addLineListener(treeViewer, activationColumn.getColumn(), new org.eclipse.swt.events.MouseAdapter() {

			@Override
			public void mouseUp(MouseEvent e) {
				if (treeViewer.getStructuredSelection().getFirstElement() instanceof StylingRule rule) {
					rule.setActivated(!rule.isActivated());
					treeViewer.refresh(rule);
					editor.getResult().getChartCreator().updateChart(editor.getResult());
				}
			}
		});

		dimensionSelectionColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		dimensionSelectionColumn.getColumn().setText(CAUIMessages.points);
		dimensionSelectionColumn.getColumn().setWidth(150);
		dimensionSelectionColumn.getColumn().setResizable(true);
		dimensionSelectionColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof StylingRule s) {
					for (SelectionInstruction selector : s.getSelectors()) {
						if (selector.getInstruction().getID().equals(CAStylerCatalog.ID_OBJECT)) {
							if (selector.getSettings() == null) return ""; //$NON-NLS-1$
							//							System.out.println("settings="+selector.getSettings());
							return AVAILABLE_DIMENSIONS_LABELS.get(AVAILABLE_DIMENSIONS.indexOf(selector.getSettings().toString()));
						}
					}
				}

				return "";
			}
		});
		dimensionSelectionColumn.setEditingSupport(new StylingRuleEditSupport(treeViewer, AVAILABLE_DIMENSIONS_LABELS) {

			@Override
			protected void savetheNewValueOfTheRule(StylingRule s, Object v) {

				SelectionInstruction objectSelector = null;
				for (SelectionInstruction selector : s.getSelectors()) {
					if (selector.getInstruction().getID().equals(CAStylerCatalog.ID_OBJECT)) {
						objectSelector = selector;
					}
				}
				if (objectSelector == null) {
					catalog.getSelectionInstructionsCatalog();
					objectSelector = new SelectionInstruction(catalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_OBJECT, "datatest"); //$NON-NLS-1$
					objectSelector.setParameterDefinition(CAStylerCatalog.ID_OBJECT);
					s.getSelectors().add(objectSelector);
				}

				objectSelector.setSettings(AVAILABLE_DIMENSIONS.get(AVAILABLE_DIMENSIONS_LABELS.indexOf(v)));
				editor.getResult().getChartCreator().updateChart(editor.getResult());
			}

			@Override
			protected Object getValue(StylingRule s) {

				for (SelectionInstruction selector : s.getSelectors()) {
					if (selector.getInstruction().getID().equals(CAStylerCatalog.ID_OBJECT)) {
						return AVAILABLE_DIMENSIONS_LABELS.get(AVAILABLE_DIMENSIONS.indexOf(selector.getSettings()));
					}
				}

				return EMPTY;
			}
		});
		
		targetSelectionColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		targetSelectionColumn.getColumn().setText(CAUIMessages.target);
		targetSelectionColumn.getColumn().setToolTipText(CAUIMessages.elementOftheChartToStyleShapesOrLabelsOrBoth);
		targetSelectionColumn.getColumn().setWidth(150);
		targetSelectionColumn.getColumn().setResizable(true);
		targetSelectionColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof StylingRule s) {

					for (StylingInstruction<?> inst : s.getStyle().getStylingInstructions()) {
						if (inst.mustBeAppliedToText() && inst.mustBeAppliedToShape()) {
							return SelectionInstruction.VALUE_ID_TARGET_LABELS_AND_SHAPES;
						}
						else if (inst.mustBeAppliedToText()) {
							return SelectionInstruction.VALUE_ID_TARGET_LABELS;
						}
						else {
							return SelectionInstruction.VALUE_ID_TARGET_SHAPES;
						}
					}
				}

				return ""; //$NON-NLS-1$
			}
		});

		targetSelectionColumn.setEditingSupport(new StylingRuleEditSupport(treeViewer, AVAILABLE_TARGETS) {

			@Override
			protected void savetheNewValueOfTheRule(StylingRule s, Object v) {

				for (StylingInstruction<?> inst : s.getStyle().getStylingInstructions()) {
					if (v.equals(SelectionInstruction.VALUE_ID_TARGET_LABELS_AND_SHAPES)) {
						inst.setMustAppliedToText(true);
						inst.setMustAppledToShape(true);
					}
					else if (v.equals(SelectionInstruction.VALUE_ID_TARGET_SHAPES)) {
						inst.setMustAppledToShape(true);
						inst.setMustAppliedToText(false);
					}
					else {
						inst.setMustAppledToShape(false);
						inst.setMustAppliedToText(true);
					}
				}
				editor.getResult().getChartCreator().updateChart(editor.getResult());
			}

			@Override
			protected Object getValue(StylingRule s) {

				for (StylingInstruction<?> inst : s.getStyle().getStylingInstructions()) {
					if (inst.mustBeAppliedToText() && inst.mustBeAppliedToShape()) {
						return SelectionInstruction.VALUE_ID_TARGET_LABELS_AND_SHAPES;
					}
					else if (inst.mustBeAppliedToText()) {
						return SelectionInstruction.VALUE_ID_TARGET_LABELS;
					}
					else {
						return SelectionInstruction.VALUE_ID_TARGET_SHAPES;
					}
				}

				return EMPTY;
			}
		});

		TreeViewerColumn sep = TreeViewerSeparatorColumn.newSeparator(treeViewer);
		
		styleColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		styleColumn.getColumn().setText(CAUIMessages.style);
		styleColumn.getColumn().setWidth(300);
		styleColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof StylingRule s) {
					if (s.getStyle() == null) return EMPTY;
					if (s.getStyle().getStylingInstructions().size() == 0) return EMPTY;
					if (s.getStyle().getStylingInstructions().get(0).getInstruction() == null) return EMPTY;

					return s.getStyle().getStylingInstructions().get(0).getInstruction().getLabel();
				}
				return element.toString();
			}
		});
		ArrayList<StylingInstructionDefinition<?>> styles = new ArrayList<>(catalog.getStyleInstructionsCatalog().getInstructions());
		styles.add(new StylingInstructionDefinition<Object>(EMPTY, EMPTY)); // to unset the style
		Collections.sort(styles, new Comparator<StylingInstructionDefinition<?>>() {

			@Override
			public int compare(StylingInstructionDefinition o1, StylingInstructionDefinition o2) {
				return o1.getLabel().compareTo(o2.getLabel());
			}
		});

		styleColumn.setEditingSupport(new StylingRuleEditSupport(treeViewer, styles) {

			@Override
			protected void savetheNewValueOfTheRule(StylingRule s, Object v) {

				if (v instanceof StylingInstructionDefinition<?> sdValue) {

					if (s.getStyle() == null) {
						s.setStyle(new Style());
					}
					// MD omg I need to get the previous relatedToX values from the previous style.
					boolean applyToShapes = true;
					boolean applyToText = true;
					ParameterDefinition param = null;
					Serializable settings = null;
					TransformationFunctionDefinition func = null;
					for (StylingInstruction<?> inst : s.getStyle().getStylingInstructions()) { // MD we should simplify the Style class to store one StylingInstruction instead of a list of StylingInstructions
						applyToShapes = inst.mustBeAppliedToShape();
						applyToText = inst.mustBeAppliedToText();
						if (inst.getInstruction() != null && inst.getInstruction().equals(sdValue)) { // same style, keep its parameter + settings
							param = inst.getParameter();
							settings = inst.getSettings();
							func = inst.getFunction();
						}
					}

					StylingInstruction<Object> inst = new StylingInstruction<Object>(catalog.getStyleInstructionsCatalog(), sdValue.getID(), null);
					inst.setMustAppledToShape(applyToShapes);
					inst.setMustAppliedToText(applyToText);
					if (param != null) {
						inst.setParameterDefinition(param.getID());
					}
					if (settings != null) {
						inst.setSettings(settings);
					}
					if (func != null) {
						inst.setFunction(func.getID());
					}

					s.getStyle().getStylingInstructions().clear();
					s.getStyle().getStylingInstructions().add(inst);

					editor.getResult().getChartCreator().updateChart(editor.getResult());
				}
			}

			@Override
			protected String getCellEditorTextValue(Object o) {

				if (o instanceof InstructionDefinition<?> id) {
					return id.getLabel();
				}

				return null;
			}

			@Override
			protected Object getValue(StylingRule s) {

				return s.getStyle().getStylingInstructions().get(0).getInstruction();
			}
		});

		styleValueColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		styleValueColumn.getColumn().setText(CAUIMessages.value);
		styleValueColumn.getColumn().setWidth(300);
		styleValueColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof StylingRule s) {
					if (s.getStyle() == null) return EMPTY;
					if (s.getStyle().getStylingInstructions().size() == 0) return EMPTY;
					if (s.getStyle().getStylingInstructions().get(0).getFunction() == null) return EMPTY;

					StylingInstruction<?> inst = s.getStyle().getStylingInstructions().get(0);
					FunctionDefinition func = inst.getFunction();
					if (func instanceof ConstantFunctionDefinition) {
						return func.getLabel();
					} else if (func instanceof UserDefinedValueFunctionDefinition) {
						return func.getLabel();
					} else if (inst.getSettings() != null && inst.getParameter().getID().equals(CAStylerCatalog.ID_LABEL)) {
						return NLS.bind(func.getLabel(), inst.getSettings().toString());
					}
					else {
						return NLS.bind(func.getLabel(), inst.getParameter());
					}
				}
				return element.toString();
			}
		});
		styleValueColumn.setEditingSupport(new StylingRuleEditSupport(treeViewer, null) {

			@Override
			protected void savetheNewValueOfTheRule(StylingRule s, Object v) {

				StylingInstruction<?> inst = s.getStyle().getStylingInstructions().get(0);

				if (v instanceof FunctionDefinition sdValue) {

					inst.setFunction(sdValue.getID());

					if (sdValue.getID().equals(StylingInstructionsCatalog.LINEAR) || sdValue.getID().equals(StylingInstructionsCatalog.INVERSE_LINEAR)) {
						if (inst.getInstruction().getID().equals(StylingInstruction.ID_SIZE)) {
							if (inst.mustBeAppliedToText()) {
								inst.setSettings(new MinMax(1, 2));
							} else {
								inst.setSettings(new MinMax(1, 5));
							}
						}
						else if (inst.getInstruction().getID().equals(StylingInstruction.ID_TRANSPARENCY)) {
							inst.setSettings(new MinMax(50, 255));
						}
						else {
							inst.setSettings(null);
						}
					} else if (sdValue.getID().equals(StylingInstructionsCatalog.USER_COLOR) && inst.getInstruction().getID().equals(StylingInstruction.ID_COLOR)) {
						inst.setSettings(Color.BLACK);
					} else if (sdValue.getID().equals(StylingInstructionsCatalog.USER_FONT) && inst.getInstruction().getID().equals(StylingInstruction.ID_FONT)) {
						inst.setSettings(ChartsEngine.createFont(editor.getResult().getFont()));
					} else if (sdValue.getID().equals(StylingInstructionsCatalog.LINEAR_GRADIANT) && inst.getInstruction().getID().equals(StylingInstruction.ID_COLOR)) {
						inst.setSettings(new Color1Color2(Color.BLACK, Color.LIGHT_GRAY));
					}
					else {
						inst.setSettings(null);
					}
				} else if (v instanceof String stringValue) { // LABEL_REGEX STYLE INSTRUCTION

					if (inst.getInstruction().getID().equals(StylingInstruction.ID_LABEL_REPLACEMENT)) {

						if (stringValue.startsWith("/") && stringValue.endsWith("/") //$NON-NLS-1$
								&& stringValue.indexOf("/", 1) < stringValue.length() - 1) { //$NON-NLS-1$

							String pattern = stringValue.substring(1, stringValue.indexOf("/", 1)); //$NON-NLS-1$
							String replace = stringValue.substring(1 + stringValue.indexOf("/", 1), stringValue.length() - 1); //$NON-NLS-1$
							inst.setSettings(new PatternReplace(Pattern.compile(pattern), replace));

							inst.setFunction(StylingInstructionsCatalog.REGEXREPLACE);
							inst.setParameterDefinition(CAStylerCatalog.ID_LABEL);
						} else {
							inst.setSettings(null);
							inst.setFunction(EMPTY);
							inst.setParameterDefinition(EMPTY);
						}
					}
				}
				else if (v instanceof ArrayList<?> values) {

					FunctionDefinition sdValue = (FunctionDefinition) values.get(0);

					inst.setFunction(sdValue.getID());
					if (values.get(1) != null) {
						ParameterDefinition param = (ParameterDefinition) values.get(1);
						inst.setParameterDefinition(param.getID());
					}
					else {
						inst.setParameterDefinition(null);
					}

					if (sdValue.getID().equals(StylingInstructionsCatalog.LINEAR) || sdValue.getID().equals(StylingInstructionsCatalog.INVERSE_LINEAR)) {
						if (inst.getInstruction().getID().equals(StylingInstruction.ID_SIZE)) {

							if (inst.mustBeAppliedToText()) {
								inst.setSettings(new MinMax(1, 2));
							} else {
								inst.setSettings(new MinMax(1, 5));
							}
						}
						else if (inst.getInstruction().getID().equals(StylingInstruction.ID_TRANSPARENCY)) {
							inst.setSettings(new MinMax(50, 255));
						}
						else {
							inst.setSettings(null);
						}
					} else if (sdValue.getID().equals(StylingInstructionsCatalog.USER_COLOR) && inst.getInstruction().getID().equals(StylingInstruction.ID_COLOR)) {
						inst.setSettings(Color.BLACK);
					} else if (sdValue.getID().equals(StylingInstructionsCatalog.USER_FONT) && inst.getInstruction().getID().equals(StylingInstruction.ID_FONT)) {
						inst.setSettings(ChartsEngine.createFont(editor.getResult().getFont()));
					} else if (sdValue.getID().equals(StylingInstructionsCatalog.LINEAR_GRADIANT) && inst.getInstruction().getID().equals(StylingInstruction.ID_COLOR)) {
						inst.setSettings(new Color1Color2(Color.BLACK, Color.LIGHT_GRAY));
					}
					else if (values.get(2) != null){
						inst.setSettings((Serializable) values.get(2));
					} else {
						inst.setSettings(null);
					}
				}

				editor.getResult().getChartCreator().updateChart(editor.getResult());
			}

			@Override
			protected String getCellEditorTextValue(Object o) {

				if (o instanceof ArrayList values) {
					if (values.get(1) == null) {
						return values.get(0).toString();
					}
					else {
						return NLS.bind(values.get(0).toString(), values.get(1).toString());
					}
				}
				return o.toString();
			}

			@Override
			protected CellEditor getCellEditor(Object element) {

				if (element instanceof StylingRule s && s.getStyle() != null) {
					InstructionDefinition<?> inst = s.getStyle().getStylingInstructions().get(0).getInstruction();

					if (inst == null) return null;

					if (inst.getID().equals(StylingInstruction.ID_LABEL_REPLACEMENT)) { // text input celleditor

						TextCellEditor replaceLabelCellEditor = new TextCellEditor(treeViewer.getTree()) {

							@Override
							protected void doSetValue(Object value) {
								if (value instanceof List list) {
									if (list.get(2) != null && list.get(2) instanceof PatternReplace pr) {
										super.doSetValue("/"+pr.pattern().pattern()+"/"+pr.replace()+"/"); //$NON-NLS-1$
									} else {
										super.doSetValue("///"); //$NON-NLS-1$
									}
								}
							}
						};

						return replaceLabelCellEditor;
					} else { // use a Comboviewer with the associations of functions and parameters

						StylingInstructionDefinition<?> sinst = (StylingInstructionDefinition<?>) inst;
						ArrayList<ArrayList> associations = new ArrayList<>();
						for (TransformationFunctionDefinition func : catalog.getStyleInstructionsCatalog().getFunctionsFor(sinst)) {
							ArrayList<ParameterDefinition> params = catalog.getStyleInstructionsCatalog().getParametersFor(sinst, func);
							for (ParameterDefinition param : params) {
								associations.add(new ArrayList(Arrays.asList(func, param, null)));
							}

							if (func instanceof ConstantFunctionDefinition) {
								associations.add(new ArrayList(Arrays.asList(func, null, null)));
							}

							if (func instanceof UserDefinedValueFunctionDefinition) {
								associations.add(new ArrayList(Arrays.asList(func, null, null)));
							}
						}
						cellEditor.setInput(associations.toArray()); // update the available values with the possible associations
						return cellEditor;
					}
				}
				return null;
			}

			@Override
			protected Object getValue(StylingRule s) {

				StylingInstruction<?> inst = s.getStyle().getStylingInstructions().get(0);
				return Arrays.asList(inst.getFunction(), inst.getParameter(), inst.getSettings());
			}
		});

		styleSettingsColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		styleSettingsColumn.getColumn().setText(CAUIMessages.settings);
		styleSettingsColumn.getColumn().setWidth(0);
		styleSettingsColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof StylingRule s) {
					for (StylingInstruction<?> si : s.getStyle().getStylingInstructions()) {
						if (si.getSettings() != null) {
							if (si.getSettings() instanceof MinMax mm) {
								return ""+mm.min() + " -> " + mm.max(); //$NON-NLS-1$
							} else if (si.getSettings() instanceof Font f) {
								return f.getFamily()+" h:"+f.getSize()+" style:"+f.getStyle(); //$NON-NLS-1$
							} else if (si.getSettings() instanceof Color c) {
								return "Color("+c.getRed()+", "+c.getGreen()+", "+c.getBlue()+")"; //$NON-NLS-1$
							} else if (si.getSettings() instanceof Color1Color2 cc) {
								return "Color("+cc.color1().getRed()+", "+cc.color1().getGreen()+", "+cc.color1().getBlue()+" -> "+cc.color2().getRed()+", "+cc.color2().getGreen()+", "+cc.color2().getBlue()+")"; //$NON-NLS-1$
							} else {
								return si.getSettings().toString();
							}
						} else {
							return ""; //$NON-NLS-1$
						}
					}
				}
				return ""+element; //$NON-NLS-1$
			}
		});

		styleSettingsColumn.setEditingSupport(new TestEditingSupport(treeViewer) {

			@Override
			protected void setValue(Object element, Object value) {

				if (value == null) {
					return; // nothing to do
				}

				if (element instanceof StylingRule s) {

					// get the property SelectionInstruction
					for (StylingInstruction<?> si : s.getStyle().getStylingInstructions()) {

						// set the new values of the property SelectionInstruction
						if (value instanceof Object[] values 
								&& values[0] instanceof Double min 
								&& values[1] instanceof Double max) {
							si.setSettings(new MinMax(min, max));

							treeViewer.refresh();
							editor.getResult().getChartCreator().updateChart(editor.getResult());
						} else if (value instanceof Object[] values && values.length == 1 && values[0] instanceof Color c) {
							si.setSettings(c);
						} else if (value instanceof Object[] values && values.length == 1 && values[0] instanceof Font f) {
							si.setSettings(f);
						} else if (value instanceof Object[] values && values.length == 2 && values[0] instanceof Color c1 && values[1] instanceof Color c2) {
							si.setSettings(new Color1Color2(c1, c2));
						}

						editor.getResult().getChartCreator().updateChart(editor.getResult());
						getViewer().update(element, null);
						treeViewer.refresh();

						break;
					}
				}
			}

			@Override
			protected Object getValue(Object element) {

				//System.out.println("get value: e=" + element);
				if (element instanceof StylingRule s) {
					for (StylingInstruction<?> si : s.getStyle().getStylingInstructions()) {
						return si;
					}
				}

				return null; // propertySelector
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				DialogCellEditor c = new StyleDialogCellEditor(this, element, treeViewer.getTree());
				return c;
			}

			@Override
			protected boolean canEdit(Object element) {

				if (element instanceof StylingRule s) {
					for (StylingInstruction<?> si : s.getStyle().getStylingInstructions()) {
						return si.getSettings() != null;
					}
				}
				return false; 
			}
		});
		
		sep2 = TreeViewerSeparatorColumn.newSeparator(treeViewer);

		informationSelectionColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		informationSelectionColumn.getColumn().setText(CAUIMessages.info);
		informationSelectionColumn.getColumn().setWidth(0);
		informationSelectionColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof StylingRule s) {
					for (SelectionInstruction selector : s.getSelectors()) {
						if (selector.getInstruction().getID().equals(CAStylerCatalog.ID_RESULT_INFOS)) {
							if (selector.getParameter() == null) return ""; //$NON-NLS-1$

							return selector.getParameter().getLabel();
						}
					}
				}
				return ""; //$NON-NLS-1$
			}
		});

		SelectionInstructionDefinition propertyInst = catalog.getSelectionInstructionsCatalog().getInstructionDefinition(CAStylerCatalog.ID_RESULT_INFOS);
		TestFunctionDefinition funcDef = catalog.getSelectionInstructionsCatalog().getFunction(SelectionInstructionsCatalog.SUP);
		ArrayList<ParameterDefinition> properties = new ArrayList<>();
		properties.add(new ParameterDefinition("", "") { //$NON-NLS-1$

			private static final long serialVersionUID = 1L;

			@Override
			public Object getValue(TXMResult result, int serie, int item) {
				return null;
			}
		});
		properties.addAll(catalog.getSelectionInstructionsCatalog().getParametersFor(propertyInst, funcDef));

		informationSelectionColumn.setEditingSupport(new StylingRuleEditSupport(treeViewer, properties) {

			@Override
			protected void savetheNewValueOfTheRule(StylingRule s, Object v) {

				if (v instanceof ParameterDefinition p) {
					SelectionInstruction propertySelector = null;
					for (SelectionInstruction selector : s.getSelectors()) {
						if (selector.getInstruction().getID().equals(CAStylerCatalog.ID_RESULT_INFOS)) {
							propertySelector = selector;
						}
					}
					if (propertySelector == null) {
						propertySelector = new SelectionInstruction(catalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_RESULT_INFOS, funcDef.getID());
						propertySelector.setSettings(0.0d);
						s.getSelectors().add(propertySelector);
					}

					if (v == null || p.getID().equals("")) { //$NON-NLS-1$
						s.getSelectors().remove(propertySelector);
					}
					else {
						propertySelector.setParameterDefinition(p.getID());
					}
					editor.getResult().getChartCreator().updateChart(editor.getResult());
				}
			}

			@Override
			protected String getCellEditorTextValue(Object o) {
				if (o instanceof ParameterDefinition pd) {
					return pd.getLabel();
				}
				return null;
			}

			@Override
			protected Object getValue(StylingRule s) {

				for (SelectionInstruction selector : s.getSelectors()) {
					if (selector.getInstruction().getID().equals(CAStylerCatalog.ID_RESULT_INFOS)) {
						return selector.getParameter();
					}
				}
				return null;
			}
		});

		thresholdSelectionColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		thresholdSelectionColumn.getColumn().setText(CAUIMessages.threshold);
		thresholdSelectionColumn.getColumn().setWidth(0);
		thresholdSelectionColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof StylingRule s) {
					for (SelectionInstruction si : s.getSelectors()) {
						if (si.getInstruction().getID().equals(CAStylerCatalog.ID_RESULT_INFOS)) {
							if (si.getFunction() == null) {
								return ""; //$NON-NLS-1$
							}

							if (si.getSettings() == null) {
								return si.getFunction().getLabel() + " ..."; //$NON-NLS-1$
							} else {
								return si.getFunction().getLabel() + " " + si.getSettings(); //$NON-NLS-1$
							}
						}
					}
				}
				return ""; //$NON-NLS-1$
			}
		});
		ArrayList<TestFunctionDefinition> thresholds = new ArrayList<>();
		thresholds.add(new TestFunctionDefinition(EMPTY, EMPTY) {

			@Override
			public Boolean apply(Object settings, Object arg) {

				return null;
			}
		});
		thresholds.addAll(catalog.getSelectionInstructionsCatalog().getFunctionsFor(propertyInst));
		thresholdSelectionColumn.setEditingSupport(new TestEditingSupport(treeViewer) {

			@Override
			protected void setValue(Object element, Object value) {

				if (value == null) return; // nothing to do

				SelectionInstruction propertySelector = null;
				if (element instanceof StylingRule s) {

					// get the property SelectionInstruction
					for (SelectionInstruction selector : s.getSelectors()) {
						if (selector.getInstruction().getID().equals(CAStylerCatalog.ID_RESULT_INFOS)) {
							propertySelector = selector;
							break;
						}
					}

					if (propertySelector == null) { // should not happen
						Log.warning(NLS.bind(CAUIMessages.NoP0SelectionInstructionInP1, CAStylerCatalog.ID_RESULT_INFOS, s));
						return;
					}

					// set the new values of the property SelectionInstruction
					if (value instanceof Object[] values 
							&& values[0] instanceof String functionId 
							&& values[1] instanceof Serializable settingsValue) {
						propertySelector.setFunction(functionId);
						propertySelector.setSettings(settingsValue);

						treeViewer.refresh();
						editor.getResult().getChartCreator().updateChart(editor.getResult());
					}
				}
			}

			@Override
			protected Object getValue(Object element) {

				SelectionInstruction propertySelector = null;
				//System.out.println("get value: e=" + element);
				if (element instanceof StylingRule s) {
					for (SelectionInstruction selector : s.getSelectors()) {
						if (selector.getInstruction().getID().equals(CAStylerCatalog.ID_RESULT_INFOS)) {
							propertySelector = selector;
							break;
						}
					}
				}

				if (propertySelector == null) { // should not happen
					Log.warning(NLS.bind(CAUIMessages.NoP0SelectionInstructionInP1, CAStylerCatalog.ID_RESULT_INFOS, element));
					return null;
				}

				return propertySelector;
			}

			@Override
			protected CellEditor getCellEditor(Object element) {

				DialogCellEditor c = new StyleDialogCellEditor(this, element, treeViewer.getTree());
				return c;
			}

			@Override
			protected boolean canEdit(Object element) {

				if (element instanceof StylingRule s) {
					for (SelectionInstruction selector : s.getSelectors()) {
						if (selector.getInstruction().getID().equals(CAStylerCatalog.ID_RESULT_INFOS) && selector.getParameter() != null) {
							return true;
						}
					}
				}
				return false; 
			}
		});

		sep3 = TreeViewerSeparatorColumn.newSeparator(treeViewer);
		
		labelSelectionColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		labelSelectionColumn.getColumn().setText(CAUIMessages.label);
		labelSelectionColumn.getColumn().setWidth(0);
		labelSelectionColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof StylingRule s) {
					for (SelectionInstruction selector : s.getSelectors()) {
						if (selector.getInstruction().getID().equals(CAStylerCatalog.ID_LABEL_REGEX)) {
							if (selector.getSettings() == null) return ""; //$NON-NLS-1$
							return selector.getSettings().toString();
						}
					}
				}
				return "";
			}
		});
		labelSelectionColumn.setEditingSupport(new EditingSupport(treeViewer) {

			TextCellEditor cellEditor = new TextCellEditor(treeViewer.getTree(), SWT.BORDER);

			@Override
			protected void setValue(Object element, Object value) {
				if (value == null) return;
				if (value instanceof Serializable svalue) {

					if (element instanceof StylingRule s) {
						SelectionInstruction labelSelector = null;
						for (SelectionInstruction selector : s.getSelectors()) {
							if (selector.getInstruction().getID().equals(CAStylerCatalog.ID_LABEL_REGEX)) {
								labelSelector = selector;
								break;
							}
						}
						if (labelSelector == null) {
							if (value == null || value.toString().length() == 0) {
								// nothing to do
							}
							else {
								catalog.getSelectionInstructionsCatalog();
								labelSelector = new SelectionInstruction(catalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_LABEL_REGEX, SelectionInstructionsCatalog.REGEX);
								labelSelector.setParameterDefinition(CAStylerCatalog.ID_LABEL);
								labelSelector.setSettings(Pattern.compile(value.toString()));
								s.addSelector(labelSelector);
							}
						}

						if (value == null || value.toString().length() == 0) {
							s.getSelectors().remove(labelSelector); // remove the selector since to value is set
						}
						else {
							try {
								labelSelector.setSettings(Pattern.compile(svalue.toString()));
							} catch(Exception e) {
								labelSelector.setSettings(null);
							}
							labelSelector.setParameterDefinition(CAStylerCatalog.ID_LABEL);
						}

						editor.getResult().getChartCreator().updateChart(editor.getResult());
						getViewer().update(element, null);
						treeViewer.refresh();
					}
				}
			}

			@Override
			protected Object getValue(Object element) {

				if (element instanceof StylingRule s) {
					SelectionInstruction labelSelector = null;
					for (SelectionInstruction selector : s.getSelectors()) {
						if (selector.getInstruction().getID().equals(CAStylerCatalog.ID_LABEL_REGEX)) {
							if (selector.getFunction() == null) return EMPTY;
							if (selector.getSettings() == null) return selector.getFunction();
							return selector.getSettings().toString();
						}
					}
				}
				return EMPTY;
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				if (element instanceof StylingRule s) {
					return cellEditor;
				}
				return null;
			}

			@Override
			protected boolean canEdit(Object element) {

				return element instanceof StylingRule;
			}
		});

		TreeViewerColumn upRuleColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		upRuleColumn.getColumn().setText(" "); //$NON-NLS-1$
		upRuleColumn.getColumn().setData("id", "down"); //$NON-NLS-1$
		upRuleColumn.getColumn().setWidth(20);
		upRuleColumn.getColumn().setResizable(false);
		upRuleColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				return null;
			}

			@Override
			public Image getImage(Object element) {
				return IImageKeys.getImage("platform:/plugin/org.txm.ca.rcp/icons/up.png"); //$NON-NLS-1$
			}
		});

		TreeViewerColumn downRuleColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		downRuleColumn.getColumn().setText(" "); //$NON-NLS-1$
		downRuleColumn.getColumn().setData("id", "down"); //$NON-NLS-1$
		downRuleColumn.getColumn().setWidth(20);
		downRuleColumn.getColumn().setResizable(false);
		downRuleColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				return null;
			}

			@Override
			public Image getImage(Object element) {
				return IImageKeys.getImage("platform:/plugin/org.txm.ca.rcp/icons/down.png"); //$NON-NLS-1$
			}
		});

		TreeViewerColumn deleteRuleColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		deleteRuleColumn.getColumn().setText(" "); //$NON-NLS-1$
		deleteRuleColumn.getColumn().setWidth(20);
		deleteRuleColumn.getColumn().setResizable(false);
		deleteRuleColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				return null;
			}

			@Override
			public Image getImage(Object element) {
				return IImageKeys.getImage(IImageKeys.ACTION_DELETE);
			}
		});
		TreeViewerColumn addRuleColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		addRuleColumn.getColumn().setText(" "); //$NON-NLS-1$
		addRuleColumn.getColumn().setWidth(20);
		addRuleColumn.getColumn().setResizable(false);
		addRuleColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				return null;
			}

			@Override
			public Image getImage(Object element) {
				return IImageKeys.getImage(IImageKeys.ACTION_ADD);
			}
		});

		//EVENTS
		TableUtils.addLineListener(treeViewer, deleteRuleColumn.getColumn(), new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {

				if (selectedStylingData == null) return;
				if (!selectedStylingData.isEditable()) return;

				currentSelection = treeViewer.getStructuredSelection().getFirstElement();
				removeRule();
			}
		});
		TableUtils.addLineListener(treeViewer, addRuleColumn.getColumn(), new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				currentSelection = treeViewer.getStructuredSelection().getFirstElement();
				addNewRule();
			}
		});
		TableUtils.addLineListener(treeViewer, upRuleColumn.getColumn(), new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {

				currentSelection = treeViewer.getStructuredSelection().getFirstElement();
				moveUpRule();

				//System.out.println("MOVE UP: "+treeViewer.getStructuredSelection().getFirstElement());
			}
		});
		TableUtils.addLineListener(treeViewer, downRuleColumn.getColumn(), new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				currentSelection = treeViewer.getStructuredSelection().getFirstElement();
				moveDownRule();
				//System.out.println("MOVE DOWN: "+treeViewer.getStructuredSelection().getFirstElement());
			}
		});
		treeViewer.setContentProvider(new ITreeContentProvider() {

			@Override
			public boolean hasChildren(Object element) {

				if (element instanceof Style s) {
					return s.getStylingInstructions().size() > 0;
				}
				return false;
			}

			@Override
			public Object getParent(Object element) {

				return null;
			}

			@Override
			public Object[] getElements(Object inputElement) {

				if (inputElement instanceof StylingSheet sheet) {
					return sheet.getRules().toArray();
				}
				return null;
			}

			@Override
			public Object[] getChildren(Object parentElement) {

				if (parentElement instanceof Style s) {
					return s.getStylingInstructions().toArray();
				}
				return new Object[0];
			}
		});
	}

	/**
	 * If StylingSheet rules size > 1 removes the selected rule of the Viewer.
	 */
	protected void removeRule() {

		if (selectedStylingData.getRules().size() <= 1) {
			return; // abort
		}

		if (currentSelection != null && currentSelection instanceof StylingRule s) {
			selectedStylingData.remove(s);
		}

		editor.getResult().getChartCreator().updateChart(editor.getResult());
		treeViewer.refresh();
	}

	protected void moveUpRule() {

		if (selectedStylingData == null) return;
		if (!selectedStylingData.isEditable()) return;

		if (currentSelection != null && currentSelection instanceof StylingRule s) {
			int i = selectedStylingData.getRules().indexOf(s);
			if (i > 0) {
				selectedStylingData.remove(s);
				selectedStylingData.getRules().add(i - 1, s);
			}
		}

		editor.getResult().getChartCreator().updateChart(editor.getResult());
		treeViewer.refresh();
	}

	protected void moveDownRule() {

		if (selectedStylingData == null) return;
		if (!selectedStylingData.isEditable()) return;

		if (currentSelection != null && currentSelection instanceof StylingRule s) {
			int i = selectedStylingData.getRules().indexOf(s);
			if (i < selectedStylingData.getRules().size() - 1) {
				selectedStylingData.remove(s);
				selectedStylingData.getRules().add(i + 1, s);
			}
		}

		editor.getResult().getChartCreator().updateChart(editor.getResult());
		treeViewer.refresh();
	}

	/**
	 * Add the styling sheet in the catalog and reload the interface with it
	 * 
	 * @param stylingSheet the new styling sheet
	 */
	public void setStylingSheet(StylingSheet stylingSheet) {

		if (selectedStylingData == stylingSheet) return;
		if (stylingSheet == null) {
			demoscombo.setSelection(new StructuredSelection());
			return;
		}
		if (stylingSheet.equals(selectedStylingData)) return;

		selectedStylingData = stylingSheet;
		if (!catalog.contains(selectedStylingData)) {
			catalog.add(selectedStylingData);
			demoscombo.refresh();
		}
		demoscombo.setSelection(new StructuredSelection(selectedStylingData));

	}

	public void addStyleSlectionHandler(SelectionListener selectionListener) {

		demoscombo.getCombo().addSelectionListener(selectionListener);
	}



	abstract class TestEditingSupport extends EditingSupport {

		public TestEditingSupport(ColumnViewer viewer) {
			super(viewer);
		}

		public void setValue2(StylingRule rule, Object[] values) {
			this.setValue(rule, values);
		}
	}

	/**
	 * Refresh the table viewer if the sheet has been updated
	 */
	public void refresh() {
		this.treeViewer.refresh();
	}

}
