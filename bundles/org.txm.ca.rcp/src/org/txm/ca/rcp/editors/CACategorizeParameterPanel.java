package org.txm.ca.rcp.editors;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.jfree.chart.JFreeChart;
import org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.renderers.CAItemSelectionRenderer;
import org.txm.ca.core.chartsengine.styling.CAStyler;
import org.txm.ca.core.functions.CA;
import org.txm.chartsengine.core.Theme;
import org.txm.chartsengine.core.styling.instructions.StylingInstruction;
import org.txm.core.results.ResultStyler;
import org.txm.core.results.TXMResult;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.rcp.swt.dialog.ComboDialog;
import org.txm.rcp.swt.dialog.ListSelectionDialog;
import org.txm.rcp.swt.dialog.ObjectSelectionDialog;
import org.txm.rcp.swt.dialog.SinglePropertySelectionDialog;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.StructuredPartition;
import org.txm.utils.PatternUtils;
import org.txm.utils.TableReader;
import org.txm.utils.logger.Log;

/**
 * Simple experiment of CA rows and columns lines color categorization
 * 
 * @author mdecorde
 *
 */
public class CACategorizeParameterPanel {

	private CA result;

	public CACategorizeParameterPanel(Composite categoriesComposite, CA result) {
		// CATEGORIES
		
		this.result = result;

		categoriesComposite.setLayout(new GridLayout(4, false));

		Button AHCCategoryButton = new Button(categoriesComposite, SWT.RADIO);
		AHCCategoryButton.setText("AHC");
		AHCCategoryButton.setToolTipText("Categorize using a children AHC");
		AHCCategoryButton.setSelection(true);

		Label lc = new Label(categoriesComposite, SWT.NONE);

		lc = new Label(categoriesComposite, SWT.NONE);

		Button applyCategories = new Button(categoriesComposite, SWT.PUSH);
		applyCategories.setText("Apply categories");
		applyCategories.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false, 1, 4));

		Button specificitiesCategoryButton = new Button(categoriesComposite, SWT.RADIO);
		specificitiesCategoryButton.setText("Specificities");
		specificitiesCategoryButton.setToolTipText("Categorize using a sibling Specificities");

		lc = new Label(categoriesComposite, SWT.NONE);
		lc = new Label(categoriesComposite, SWT.NONE);

		Button metadataCategoryButton = new Button(categoriesComposite, SWT.RADIO);
		metadataCategoryButton.setText("Metadata");
		metadataCategoryButton.setToolTipText("Categorize using a corpus metadata");
		metadataCategoryButton.setEnabled(false);
		if (result.getParent().getParent() instanceof StructuredPartition p) {
			if (p.getStructuralUnitProperty().getFullName().equals("text_id")) { //$NON-NLS-1$
				metadataCategoryButton.setEnabled(true);
			}
		}

		Button externalCategoryButton = new Button(categoriesComposite, SWT.RADIO);
		externalCategoryButton.setText("External data");
		externalCategoryButton.setToolTipText("Categorize using external metadata");

		applyCategories.addSelectionListener(new SelectionListener() {

			private String selectedTextProperty;

			@Override
			public void widgetSelected(SelectionEvent e) {

				// TO BE MOVED IN A StyleChartRenderer or something
				if (result.getChart() instanceof JFreeChart chart) {
					if (chart.getXYPlot().getRenderer() instanceof CAItemSelectionRenderer renderer) {
						try {
							List<ResultStyler> stylers = new ArrayList<>();

							if (AHCCategoryButton.getSelection()) {

								for (TXMResult c : result.getChildren()) {
									if (c instanceof CAStyler s && c.getResultType().equals("AHC")) {
										stylers.add(s);
									}
								}
							}
							else if (specificitiesCategoryButton.getSelection()) {

								LexicalTable parent = result.getParent();
								for (TXMResult c : parent.getChildren()) {
									if (c instanceof ResultStyler s && c.getResultType().equals("Specificities")) {
										stylers.add(s);
									}
								}
							}
							else if (metadataCategoryButton.getSelection()) {

								selectedTextProperty = null;
								MainCorpus corpus = result.getFirstParent(MainCorpus.class);
								try {
									SinglePropertySelectionDialog<StructuralUnitProperty> dialog = new SinglePropertySelectionDialog<StructuralUnitProperty>(categoriesComposite.getShell(),
											new ArrayList<StructuralUnitProperty>(corpus.getTextStructuralUnit().getOrderedProperties()), new ArrayList<StructuralUnitProperty>());
									if (dialog.open() == ComboDialog.OK) {
										selectedTextProperty = dialog.getSelectedProperties().get(0).getName();
									}
								}
								catch (CqiClientException e1) {
									e1.printStackTrace();
									return;
								}

								if (selectedTextProperty == null) return;

								CAStyler styler = new CAStyler() {

									@Override
									public HashMap<Pattern, HashMap<String, String>> getRowStyles() {
										return null;
									}

									@Override
									public String getName() {
										return "Metadata";
									}

									@Override
									public HashMap<Pattern, HashMap<String, String>> getColStyles() {
										HashMap<Pattern, HashMap<String, String>> styles = new HashMap<Pattern, HashMap<String, String>>();

										try {
											int[] starts = corpus.getTextStartLimits();
											int[] idx = new int[starts.length];
											for (int i = 0; i < starts.length; i++) {
												idx[i] = i;
											}

											String[] values = CQPSearchEngine.getCqiClient().struc2Str(corpus.getTextStructuralUnit().getProperty(selectedTextProperty).getQualifiedName(), idx); // idx
											String[] ids = CQPSearchEngine.getCqiClient().struc2Str(corpus.getTextIdStructuralUnitProperty().getQualifiedName(), idx); // idx

											LinkedHashSet<String> uniqValues = new LinkedHashSet<>();
											uniqValues.addAll(Arrays.asList(values));
											Theme theme = new Theme();
											ArrayList<Color> colors = theme.getColorPaletteFor(uniqValues.size());

											HashMap<String, String> valuesToColor = new HashMap<String, String>();
											int icolor = 0;
											ArrayList<String> tmp = new ArrayList<>(uniqValues);
											Collections.sort(tmp);
											for (String v : tmp) {
												Color c = colors.get((icolor++) % colors.size());
												String color = "" + c.getRed() + " " + c.getGreen() + " " + c.getBlue(); //$NON-NLS-1$
												valuesToColor.put(v, color);
											}

											for (int i = 0; i < values.length; i++) {
												values[i] = valuesToColor.get(values[i]);
											}

											for (int itext = 0; itext < ids.length; itext++) {
												HashMap<String, String> style = new HashMap<String, String>();
												//													style.put("shape-color", values[itext]);
												style.put(StylingInstruction.LABEL_COLOR, values[itext]);
												styles.put(Pattern.compile(PatternUtils.quote(ids[itext])), style);
											}
										}
										catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										return styles;
									}
								};
								stylers.add(styler);
							}
							else if (externalCategoryButton.getSelection()) {

								FileDialog dialog = new FileDialog(externalCategoryButton.getDisplay().getActiveShell(), SWT.OPEN);
								dialog.setFilterExtensions(new String[] { "*.csv", "*.tsv", "*.ods", "*.xlsx" }); //$NON-NLS-1$
								String path = dialog.open();
								if (path == null) return;

								CAStyler styler = new CAStyler() {

									protected HashMap<Pattern, HashMap<String, String>> getStyles(String sheet) {

										try {
											String[][] table = TableReader.readAsTable(new File(path), sheet);

											HashMap<Pattern, HashMap<String, String>> styles = new HashMap<Pattern, HashMap<String, String>>();
											for (String[] line : table) {
												if (line.length == 2) {
													HashMap<String, String> style = new HashMap<String, String>();
													//														style.put("shape-color", line[1]);
													style.put(StylingInstruction.LABEL_COLOR, line[1]);
													styles.put(Pattern.compile(PatternUtils.quote(line[0])), style);
												}
											}
											return styles;
										}
										catch (Exception e) {
											e.printStackTrace();
											return null;
										}

									}

									@Override
									public HashMap<Pattern, HashMap<String, String>> getRowStyles() {
										return getStyles("rows"); //$NON-NLS-1$
									}

									@Override
									public String getName() {
										return "External data";
									}

									@Override
									public HashMap<Pattern, HashMap<String, String>> getColStyles() {
										return getStyles("cols"); //$NON-NLS-1$
									}
								};
								stylers.add(styler);
							}

							if (stylers.size() == 0) {
								Log.warning("No styler found for the categorization");
								return;
							}

							ResultStyler styler = null;
							if (stylers.size() > 1) {
								ListSelectionDialog<ResultStyler> dialog = new ListSelectionDialog<>(categoriesComposite.getShell(), "Available stylers", "Select one of the folowwingg stylers", stylers, null);
								if (dialog.open() == ObjectSelectionDialog.OK) {
									styler = dialog.getSelectedValue();
								}
								else {
									Log.warning("Aborting.");
								}
							}
							else {
								for (ResultStyler s : stylers) {
									styler = s;
									break;
								}
							}

							Log.info("Styling using " + styler.getName());
							Log.info("Colors legend:");
							renderer.resetStyles();
							HashMap<String, HashMap<Pattern, HashMap<String, String>>> styles = styler.getStyles();
							for (String k : styles.keySet()) {
								if (styles.get(k) == null) continue;
								System.out.println("	" + k);

								for (Pattern s : styles.get(k).keySet()) {
									System.out.println("		" + s + ": " + styles.get(k).get(s)); //$NON-NLS-1$
								}
							}
							renderer.updateRulesFromMaps(styles.get("rows"), styles.get("cols"), result); //$NON-NLS-1$

//							CAFilterStylingSheet stylingsheet = result.getFilterStylingSheet();
//							HashMap<String, StylingRule> newRules = new HashMap<>();
//							for (String k : styles.keySet()) {
//								if (styles.get(k) == null) continue;
//								System.out.println("	" + k);
//
//								for (Pattern s : styles.get(k).keySet()) {
//									HashMap<String, String> rule = styles.get(k).get(s);
//									String rulename = rule.toString();
//									if (!newRules.containsKey(rulename)) {
//										newRules.put(rulename, new StylingRule(rulename, new Style()));
//									}
//									StylingRule srule = newRules.get(rule.toString());
//								}
//							}
						}
						catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}
}
