package org.txm.ca.rcp.editors.styling;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ComboBoxViewerCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TreeViewer;
import org.txm.chartsengine.core.styling.StylingRule;

/**
 * 
 * Editing support specialized for StylingRules
 * 
 * implements getCellEditorTextValue the value to show from the StylingRule is different from a String
 * 
 * getValue should return the current value to edit
 * setValue(style, v) should implement the StylingRule update procedure
 * 
 * @author s
 *
 */
public abstract class StylingRuleEditSupport extends EditingSupport {

	ComboBoxViewerCellEditor cellEditor;

	static String EMPTY = "";
	
	public StylingRuleEditSupport(TreeViewer treeViewer, Object input) {

		super(treeViewer);

		cellEditor = new AutoDropComboBoxViewerCellEditor(treeViewer.getTree());
		
		cellEditor.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof String s) {
					return s;
				} else {
					return getCellEditorTextValue(element);
				}
			}
		});
		cellEditor.setContentProvider(new ArrayContentProvider());
		cellEditor.setInput(input);
	}

	@Override
	protected final void setValue(Object element, Object value) {
		if (value == null) return;

		savetheNewValueOfTheRule((StylingRule) element, value);

		getViewer().update(element, null);
		getViewer().refresh();
	}

	/**
	 * What to do when the new value has been validated by the user
	 * @param rule
	 * @param value
	 */
	protected abstract void savetheNewValueOfTheRule(StylingRule rule, Object newValue);

	/**
	 * How to display in the StylingRule part to edit (the value) **IF the value is not a String**
	 * 
	 * @param elementToDisplayInTheCellEditor
	 * @return the String to be displayed in the ComboBoxViewer
	 */
	protected String getCellEditorTextValue(Object elementToDisplayInTheCellEditor) {
		return ""+elementToDisplayInTheCellEditor;
	}

	/**
	 * What part of the StylingRule to edit in the celleditor
	 */
	protected abstract Object getValue(StylingRule s);

	@Override
	protected final Object getValue(Object element) {
		
		if (element instanceof StylingRule s)
			return getValue(s);
		
		return null;
	}
	
	@Override
	protected CellEditor getCellEditor(Object element) {

		if (element instanceof StylingRule s) {
			return cellEditor;
		}
		return null;
	}

	@Override
	protected boolean canEdit(Object element) {

		if (element instanceof StylingRule) {
			return true;
		}
		return false;
	}
}
