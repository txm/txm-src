package org.txm.ca.rcp.editors;

import java.awt.Component;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Set;

import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.ToolItem;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.functions.CAColsInfo;
import org.txm.ca.core.functions.CARowsInfo;
import org.txm.ca.core.functions.Eigenvalues;
import org.txm.ca.core.preferences.CAPreferences;
import org.txm.ca.rcp.editors.filtering.CAFilterPanel;
import org.txm.ca.rcp.editors.styling.StylingSheetTable;
import org.txm.ca.rcp.handlers.ComputeCAColInfos;
import org.txm.ca.rcp.handlers.ComputeCARowInfos;
import org.txm.ca.rcp.handlers.ComputeCASingularValues;
import org.txm.ca.rcp.handlers.ComputeCASingularValuesTable;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.chartsengine.core.styling.StylingSheet;
import org.txm.chartsengine.rcp.IChartComponent;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.editors.ChartEditorInput;
import org.txm.chartsengine.rcp.editors.listeners.UpdateChartSelectionListener;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.index.core.functions.PartitionIndex;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.ITXMResultEditor;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.provider.SimpleLabelProvider;
import org.txm.rcp.swt.widget.ListText;
import org.txm.rcp.swt.widget.ThresholdsGroup;
import org.txm.rcp.swt.widget.structures.PropertiesComboViewer;
import org.txm.rcp.utils.IOClipboard;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

/**
 * Correspondence analysis factorial map chart editor.
 *
 * @author sjacquot, mdecorde
 *
 */
public class CAFactorialMapChartEditor extends ChartEditor<CA> {

	// FIXME: try to automate that
	// a solution is to stop using two int in CA but a members int[] dimensions, then use a ComboViewer in this editor
	// /**
	// * First dimension.
	// */
	// @Parameter(key=CAPreferences.FIRST_DIMENSION)
	// protected int firstDimension = 1;
	//
	// /**
	// * Second dimension.
	// */
	// @Parameter(key=CAPreferences.SECOND_DIMENSION)
	// protected int secondDimension = 2;

	/**
	 * To show/hide individuals.
	 */
	@Parameter(key = CAPreferences.SHOW_INDIVIDUALS)
	protected ToolItem showIndividuals;

	/**
	 * To show/hide variables.
	 */
	@Parameter(key = CAPreferences.SHOW_VARIABLES)
	protected ToolItem showVariables;

	/**
	 * To invert some axes.
	 */
	@Parameter(key = CAPreferences.MIRRORED_AXES)
	protected ArrayList<Integer> mirroredDimensions;

	/**
	 * To show/hide the point shapes.
	 */
	@Parameter(key = CAPreferences.SHOW_POINT_SHAPES)
	protected ToolItem showPointShapes;

	/**
	 * To show/hide the point labels.
	 */
	@Parameter(key = CAPreferences.SHOW_POINT_LABELS)
	protected ToolItem showPointLabels;

	/**
	 * Unit property.
	 */
	@Parameter(key = TXMPreferences.UNIT_PROPERTY)
	protected PropertiesComboViewer unitPropertyComboViewer;

	/**
	 * Minimum frequency filtering spinner.
	 */
	@Parameter(key = TXMPreferences.F_MIN)
	protected Spinner fMinSpinner;

	/**
	 * Maximum frequency filtering spinner.
	 */
	@Parameter(key = TXMPreferences.F_MAX)
	protected Spinner fMaxSpinner;

	/**
	 * Maximum number of lines filtering.
	 */
	@Parameter(key = TXMPreferences.V_MAX)
	protected Spinner vMaxSpinner;

	@Parameter(key = CAPreferences.ROW_SUP_NAMES)
	private ListText supRowNamesText;

	@Parameter(key = CAPreferences.COL_SUP_NAMES)
	private ListText supColNamesText;

	/**
	 * Maximum number of lines filtering.
	 */
	@Parameter(key = CAPreferences.N_FACTORS_MEMORIZED)
	protected Spinner numberOfFactors;

	private CAPlaneSelectionWidget plansSelectionCombo;

	private ListViewer mirroredDimensionsListViewer;

	@Parameter(key = CAPreferences.STYLINGSHEET)
	protected StylingSheet stylingSheet;

	private StylingSheetTable stylingSheetEditorTable;

	private ToolItem infoLine;

	private CAFilterPanel caFilterPanel;

	/**
	 *
	 */
	public static NumberFormat noDecimalFormatter = DecimalFormat.getInstance();
	{
		noDecimalFormatter.setMaximumFractionDigits(0);
	}

	/**
	 *
	 */
	ColsRowsInfosEditor colsInfosEditor, rowsInfosEditor;

	/**
	 * Eigenvalues bar chart.
	 */
	EigenvaluesChartEditor singularValuesEditor;

	/**
	 * Eigenvalues data table.
	 */
	EigenvaluesTableEditor singularValuesTableEditor;


	/**
	 *
	 * @param chartEditorInput
	 */
	public CAFactorialMapChartEditor() {
	}

	/**
	 *
	 * @param chartEditorInput
	 */
	public CAFactorialMapChartEditor(ChartEditorInput<CA> chartEditorInput) {
		super(chartEditorInput);
	}


	@Override
	public void __createPartControl() throws CqiClientException {

		//FIXME: SJ: needed?
		// Auto-compute selection listener
		//ComputeSelectionListener computeSelectionListener = new ComputeSelectionListener(this, true);

		// Auto-update chart selection listener
		UpdateChartSelectionListener updateChartSelectionListener = new UpdateChartSelectionListener(this);


		// Main parameters
		GLComposite mainParametersArea = this.getMainParametersComposite();

		try {
			// display lexical table parameters if it is not visible
			if (!this.getResult().getParent().isVisible()  // the LT parent is not visible
					&& !(this.getResult().getParent().getParent() instanceof PartitionIndex)) { // and its parent is not a PartitionIndex
				mainParametersArea.getLayout().numColumns = 2;

				// unit property
				Label l = new Label(mainParametersArea, SWT.NONE);
				l.setText(TXMCoreMessages.common_property);
				l.setToolTipText(CAUIMessages.structuralUnitUsedToBuildTheParentLexicalTable);
				
				this.unitPropertyComboViewer = new PropertiesComboViewer(
						mainParametersArea,
						this,
						false,
						CQPCorpus.getFirstParentCorpus(this.getResult()).getOrderedProperties(),
						this.getResult().getUnitProperty(),
						false);

				// reset view and clear selection on property change
				this.unitPropertyComboViewer.addSelectionChangedListener(new ISelectionChangedListener() {
					@Override
					public void selectionChanged(SelectionChangedEvent event) {
						getResult().setNeedsToResetView(true);
						getResult().setNeedsToClearItemsSelection(true);

						try {
							getColsInfosEditor().getViewer().getTable().deselectAll();
							getRowsInfosEditor().getViewer().getTable().deselectAll();
						}
						catch (NullPointerException e) {
							//nothing to do
						}

					}
				});

				unitPropertyComboViewer.getCombo().setToolTipText(CAUIMessages.structuralUnitUsedToBuildTheParentLexicalTable);
			}

		}
		catch (CqiClientException e) {
			Log.printStackTrace(e);
		}


		mainParametersArea.getLayout().numColumns++;
		plansSelectionCombo = new CAPlaneSelectionWidget(mainParametersArea, this.getResult());
		plansSelectionCombo.setLayoutData(new GridData(GridData.FILL));
		updateAvailablePlans();

		plansSelectionCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				boolean resetView = false;

				int[] plan = plansSelectionCombo.getSelectedPlaneAsInts();
				if (plan == null) {
					plansSelectionCombo.setSelectedPlane(getResult().getFirstDimension(), getResult().getSecondDimension());
					return;
				}
//				else if (plan[0] >= plan[1]) {
//					return;
//				}

				if (getResult().getFirstDimension() == plan[0] && getResult().getSecondDimension() == plan[1]) {
					return;
				}

				getResult().setFirstDimension(plan[0]);
				getResult().setSecondDimension(plan[1]);

				//SJ: FIXME: we should not recompute here. Need to find why we do that, maybe for styling? then update the code to not recompute.
				resetView = true;
				getResult().setDirty(); // force updating the CA when plan changes
				getResult().setNeedsToResetView(resetView);
				compute(true);

				caFilterPanel.updateWidgetsFromThresholds();
				//caFilterPanel.updateFilterStylingSheet();

				// forceFocus();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});




		// Extends the top tool bar
		//SJ: nothing at this moment. Will see when we'll validated the styling button and show columns, etc. positions




		// Extend the chart editor tool bar
		new ToolItem(this.chartToolBar, SWT.SEPARATOR);

		// Show/hide columns
		showIndividuals = new ToolItem(this.chartToolBar, SWT.CHECK);
		// FIXME: SJ: keep this for propose an option "large icons" with text in buttons?
		//showIndividuals.setText(CAUIMessages.showTheColumns);
		showIndividuals.setToolTipText(CAUIMessages.showhideColumns);
		showIndividuals.setImage(IImageKeys.getImage(CAFactorialMapChartEditor.class, "icons/show_columns.png")); //$NON-NLS-1$
		//showIndividuals.addSelectionListener(computeSelectionListener);
		showIndividuals.addSelectionListener(updateChartSelectionListener);

		// Show/hide rows
		showVariables = new ToolItem(this.chartToolBar, SWT.CHECK);
		// FIXME: SJ: keep this for propose an option "large icons" with text in buttons?
		//showVariables.setText(CAUIMessages.showTheRows);
		showVariables.setToolTipText(CAUIMessages.showhideRows);
		showVariables.setImage(IImageKeys.getImage(CAFactorialMapChartEditor.class, "icons/show_rows.png")); //$NON-NLS-1$
		//showVariables.addSelectionListener(computeSelectionListener);
		showVariables.addSelectionListener(updateChartSelectionListener);

		// Show individuals table editor
		ToolItem showIndividualsTableEditor = new ToolItem(this.chartToolBar, SWT.PUSH);
		showIndividualsTableEditor.setImage(IImageKeys.getImage(CAFactorialMapChartEditor.class, "icons/show_columns_information.png")); //$NON-NLS-1$
		showIndividualsTableEditor.setToolTipText(CAUIMessages.showTheColumnInformations);
		showIndividualsTableEditor.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				openIndividualsEditor();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});

		// Show variables table editor
		ToolItem showVariablesTableEditor = new ToolItem(this.chartToolBar, SWT.PUSH);
		showVariablesTableEditor.setImage(IImageKeys.getImage(CAFactorialMapChartEditor.class, "icons/show_rows_information.png")); //$NON-NLS-1$
		showVariablesTableEditor.setToolTipText(CAUIMessages.showTheRowInformations);
		showVariablesTableEditor.addSelectionListener(new SelectionListener() {


			@Override
			public void widgetSelected(SelectionEvent e) {

				openVariablesEditor(); // default
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});

		ToolItem showSingularValuesTableEditor = new ToolItem(this.chartToolBar, SWT.PUSH);
		showSingularValuesTableEditor.setImage(IImageKeys.getImage(CAFactorialMapChartEditor.class, "icons/eigenvalues.png")); //$NON-NLS-1$
		showSingularValuesTableEditor.setToolTipText(CAUIMessages.showTheSingularValueInformations);
		showSingularValuesTableEditor.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				CAFactorialMapChartEditor.this.openSingularValuesTableEditor();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});

		// Show/hide point shapes
		showPointShapes = new ToolItem(this.chartToolBar, SWT.CHECK);
		// FIXME: SJ: keep this for propose an option "large icons" with text in buttons?
		//showPointShapes.setText(CAUIMessages.showPointShapes);
		showPointShapes.setToolTipText(CAUIMessages.showhidePointShapes);
		showPointShapes.setImage(IImageKeys.getImage(CAFactorialMapChartEditor.class, "icons/show_point_shapes.png")); //$NON-NLS-1$
		showPointShapes.addSelectionListener(updateChartSelectionListener);

		// Show/hide point labels
		showPointLabels = new ToolItem(this.chartToolBar, SWT.CHECK);
		showPointLabels.setToolTipText(CAUIMessages.showhidePointLabels);
		showPointLabels.setImage(IImageKeys.getImage(CAFactorialMapChartEditor.class, "icons/show_point_labels.png")); //$NON-NLS-1$
		showPointLabels.addSelectionListener(updateChartSelectionListener);


		// Styling section
		new ToolItem(this.chartToolBar, SWT.SEPARATOR);

		// Style sheet combo box selection
		this.getFirstLineComposite().getLayout().numColumns++;
		ComboViewer demoscombo = new ComboViewer(this.getFirstLineComposite());
		demoscombo.getCombo().setToolTipText(CAUIMessages.currentStylingSheet);
		demoscombo.setLabelProvider(new SimpleLabelProvider());
		demoscombo.setContentProvider(new ArrayContentProvider());

		// Filtering panel
		Composite filterComposite = this.chartToolBar.installGroup(CAUIMessages.filtering, CAUIMessages.filterThePointsOfYourChart
						, "platform:/plugin/org.txm.ca.rcp/icons/filter_down.png", //$NON-NLS-1$
						"platform:/plugin/org.txm.ca.rcp/icons/filter_up.png", false, true); //$NON-NLS-1$
		filterComposite.setLayout(new GridLayout());
		caFilterPanel = new CAFilterPanel(filterComposite, this);
		caFilterPanel.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));


		// Styling sheet panel
		Composite styleComposite = this.chartToolBar.installGroup(CAUIMessages.stylingSheet, CAUIMessages.editTheStyleOfYourChart,
				"platform:/plugin/org.txm.ca.rcp/icons/style_down.png", //$NON-NLS-1$
				"platform:/plugin/org.txm.ca.rcp/icons/style_up.png", false, true); //$NON-NLS-1$
		styleComposite.setLayout(new GridLayout());
		stylingSheetEditorTable = new StylingSheetTable(styleComposite, demoscombo, this);
		stylingSheetEditorTable.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		stylingSheetEditorTable.addStyleSlectionHandler(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				stylingSheet = stylingSheetEditorTable.getStylingSheet();
				getResult().setStylingSheet(stylingSheet);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

		});
		if (getResult().getStylingSheet() != null) {
			stylingSheetEditorTable.setStylingSheet(getResult().getStylingSheet());
		}

		// Add some parameters to the extended parameters area

		// parent parameters
		// thresholds
		if (!this.getResult().getParent().isVisible()  // the Lexical Table parent is not visible
				&& !(this.getResult().getParent().getParent() instanceof PartitionIndex)) { // and its parent is not a PartitionIndex
			ThresholdsGroup thresholdsGroup = new ThresholdsGroup(this.getExtendedParametersGroup(), SWT.NONE, this, false, true);
			this.fMinSpinner = thresholdsGroup.getFMinSpinner();
			this.fMaxSpinner = thresholdsGroup.getFMaxSpinner();
			this.vMaxSpinner = thresholdsGroup.getVMaxSpinner();
			this.vMaxSpinner.setMinimum(2);
			CQPCorpus corpus = getResult().getFirstParent(MainCorpus.class);
			if (corpus != null) {
				this.vMaxSpinner.setMaximum(corpus.getSize());
				this.fMaxSpinner.setMaximum(corpus.getSize());
				this.fMinSpinner.setMaximum(corpus.getSize());
			}
		}

		// Axis
		Group axesGroup = new Group(this.getExtendedParametersGroup(), SWT.NONE);
		axesGroup.setText(" "+CAUIMessages.axes);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 4;
		gridLayout.makeColumnsEqualWidth = false;
		axesGroup.setLayout(gridLayout);
		
//		axesGroup.setLayout(new GridLayout(2, false));
//		RowLayout rlayout = new RowLayout();
//		rlayout.center = true;
//		axesGroup.setLayout(rlayout);

		
		// Number of factors
		Label l = new Label(axesGroup, SWT.NONE);
		l.setText(CAUIMessages.numberOfFactors);
		l.setToolTipText(CAUIMessages.maximumNumberOfAxesToBeUsedInTheInterfaceAndCalculations);
		numberOfFactors = new Spinner(axesGroup, SWT.BORDER);
		numberOfFactors.setToolTipText(CAUIMessages.maximumNumberOfAxesToBeUsedInTheInterfaceAndCalculations);
		numberOfFactors.setMinimum(1);
		try {
			if (!this.getResult().getLexicalTable().hasBeenComputedOnce()) this.getResult().getLexicalTable().compute(false);
			numberOfFactors.setMaximum(Math.min(this.getResult().getLexicalTable().getNRows() -1, this.getResult().getLexicalTable().getNColumns() -1));
		}
		catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}


		// Axis mirroring
		l = new Label(axesGroup, SWT.NONE);
		l.setText(CAUIMessages.invertedAxes);
		l.setToolTipText(CAUIMessages.forPresentationAndSharePurposeSelectTheAxesToBeInverted);
		
		mirroredDimensionsListViewer = new ListViewer(axesGroup, SWT.MULTI | SWT.V_SCROLL | SWT.BORDER);
		mirroredDimensionsListViewer.getList().setToolTipText(CAUIMessages.forPresentationAndSharePurposeSelectTheAxesToBeInverted);
		mirroredDimensionsListViewer.setLabelProvider(new SimpleLabelProvider());
		mirroredDimensionsListViewer.setContentProvider(new ArrayContentProvider());
		mirroredDimensionsListViewer.getList().setSize(50, SWT.DEFAULT);
		//		GridData gdata = new GridData(GridData.FILL, GridData.FILL, false, true);
		//		gdata.heightHint = 100;
		GridData rdata = new GridData();
		rdata.heightHint = 40;
		rdata.widthHint = 30;
		mirroredDimensionsListViewer.getList().setLayoutData(rdata);
		//mirroredDimensionsListViewer.getList().pack();
		

		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {

			// SUPPLEMENTARY ROWS AND COLS
			l = new Label(this.getExtendedParametersGroup(), SWT.NONE);
			l.setText(CAUIMessages.SupplementaryRowNames);

			supRowNamesText = new ListText(this.getExtendedParametersGroup(), SWT.BORDER);

			l = new Label(this.getExtendedParametersGroup(), SWT.NONE);
			l.setText(CAUIMessages.SupplementaryColNames);

			supColNamesText = new ListText(this.getExtendedParametersGroup(), SWT.BORDER);

			//			// CATEGORIES
			//			Composite categoriesComposite = this.getTopToolbar().installGroup("Categorize with color", "Categorize points&&labels", "platform:/plugin/org.txm.ca.rcp/icons/categories.png",
			//					"platform:/plugin/org.txm.ca.rcp/icons/categories.png", false, true);
			//			categoriesComposite.setLayout(new GridLayout(4, false));
			//
			//			new CACategorizeParameterPanel(categoriesComposite, getResult());

			//new CAAnnotationPanel(styleComposite, getResult());

		}
		this.chartToolBar.pack();

		infoLine = new ToolItem(this.bottomToolBar, SWT.PUSH);
		//infoLine.setText("No infos yet, compute your CA");
		infoLine.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				IOClipboard.write(infoLine.getText());
				Log.info(CAUIMessages.bind(CAUIMessages.copiedTextP0, infoLine.getText()));
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});


		updateBottomInfoLine();

	}

	@Override
	public void updateResultFromEditor() {

		ArrayList newList = new ArrayList(mirroredDimensionsListViewer.getStructuredSelection().toList());
		if (!newList.equals(mirroredDimensions)) {
			mirroredDimensions = newList;
		}
		stylingSheet = stylingSheetEditorTable.getStylingSheet();
		//FIXME: SJ, 2024-12-05: useless?
//		if (result.getChart() instanceof JFreeChart chart) {
//			if (chart.getXYPlot().getRenderer() instanceof CAItemSelectionRenderer renderer) {
//				try {
//					renderer.updateRulesFromStylingsheet(stylingSheet, getResult());
//				}
//				catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}
	}


	/**
	 * Open the Variables editor and move it next to the CA Editor
	 *
	 * @return
	 */
	//TODO: MD adapt link editors to be able to manage this kind of link
	public ColsRowsInfosEditor openVariablesEditor() {
		if (getRowsInfosEditor() != null) {
			getRowsInfosEditor().getSite().getPage().activate(getRowsInfosEditor());
			return getRowsInfosEditor();
		}

		Object o = ComputeCARowInfos.open(getResult());
		if (o != null && o instanceof ColsRowsInfosEditor editor) {
			SWTEditorsUtils.moveEditor(CAFactorialMapChartEditor.this, editor, EModelService.RIGHT_OF, 0.3f);
			return editor;
		}
		return null;
	}

	/**
	 *
	 */
	public void closeVariablesEditor() {
		if (getRowsInfosEditor() == null) {
			return;
		}
		getRowsInfosEditor().close();
	}

	/**
	 *
	 * @return
	 */
	public ColsRowsInfosEditor openIndividualsEditor() {
		if (getColsInfosEditor() != null) {
			getColsInfosEditor().getSite().getPage().activate(getColsInfosEditor());
			return getColsInfosEditor();
		}

		Object o = ComputeCAColInfos.open(getResult());
		if (o != null && o instanceof ColsRowsInfosEditor editor) {
			SWTEditorsUtils.moveEditor(CAFactorialMapChartEditor.this, editor, EModelService.RIGHT_OF, 0.3f);
			return editor;
		}
		return null;
	}

	/**
	 *
	 */
	public void closeIndividualsEditor() {
		if (getColsInfosEditor() == null) {
			return;
		}
		getColsInfosEditor().close();
	}

	/**
	 *
	 * @return
	 */
	public EigenvaluesChartEditor openSingularValuesEditor() {
		if (getSingularValuesEditor() != null) {
			getSingularValuesEditor().getSite().getPage().activate(getSingularValuesEditor());
			return getSingularValuesEditor();
		}

		Object o = ComputeCASingularValues.open(getResult());
		if (o != null && o instanceof EigenvaluesChartEditor editor) {
			SWTEditorsUtils.moveEditor(CAFactorialMapChartEditor.this, editor, EModelService.RIGHT_OF, 0.3f);
			return editor;
		}
		return null;
	}

	/**
	 *
	 * @return
	 */
	public EigenvaluesTableEditor openSingularValuesTableEditor() {
		if (getSingularValuesTableEditor() != null) {
			getSingularValuesTableEditor().getSite().getPage().activate(getSingularValuesTableEditor());
			return getSingularValuesTableEditor();
		}

		Object o = ComputeCASingularValuesTable.open(getResult());
		if (o != null && o instanceof EigenvaluesTableEditor editor) {
			SWTEditorsUtils.moveEditor(CAFactorialMapChartEditor.this, editor, EModelService.RIGHT_OF, 0.3f);
			return editor;
		}
		return null;
	}


	@Override
	public void updateEditorFromChart(boolean update) {

		updateAvailablePlans();

//		if (the data part have been recomputed ??) {
//			caFilterPanel.updateAvailableThresholdsValues(); // thresholds available values might have changed
//		}

		if (getResult().getStylingSheet() != null && !getResult().getStylingSheet().equals(stylingSheet)) {
			stylingSheet = getResult().getStylingSheet();
			stylingSheetEditorTable.setStylingSheet(stylingSheet);
		}
		ArrayList<Integer> dims = new ArrayList<Integer>();
		try {
			for (int i = 1; i <= this.getResult().getNumberOfFactorsMemorized(); i++) {
				dims.add(i);
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mirroredDimensionsListViewer.setInput(dims);
		//mirroredDimensionsListViewer.getList().pack();
		if (this.getResult().getMirroredDimensions() != null) {
			mirroredDimensionsListViewer.setSelection(new StructuredSelection(this.getResult().getMirroredDimensions()));
		}

		updateBottomInfoLine();

		// update the parent multipart editor partname
		//		if (parentMultiPagesEditor != null) {
		//			this.parentMultiPagesEditor.setPartName(this.getResult().getName());
		//		}
		try {
			if (getSingularValuesEditor() != null) {
				getSingularValuesEditor().refresh(update);
			}

			if (getColsInfosEditor() != null) {
				getColsInfosEditor().refresh(update);
			}

			if (getRowsInfosEditor() != null) {
				getRowsInfosEditor().refresh(update);
			}
			//			// Updating complementary editors
			//
			//				// cols and rows tables
			//				((ColsRowsInfosEditor) this.parentMultiPagesEditor.getEditors().get(1)).refresh(update);
			//				((ColsRowsInfosEditor) this.parentMultiPagesEditor.getEditors().get(2)).refresh(update);
			//				// eigenvalues chart
			//				// ((EigenvaluesChartEditor)this.parentMultiPagesEditor.getEditors().get(4)).refresh(update);
			//				// eigenvalues table
			//				((TXMEditor) this.parentMultiPagesEditor.getEditors().get(4)).refresh(update);
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}

	}


	/**
	 * Updates the info line of bottom tool bar.
	 */
	public void updateBottomInfoLine() {

		try {
			double[] sumcont = getResult().getSumContXOfVisiblePoints();
			int[] filtered = getResult().getNumberOfHiddenPoints();

			infoLine.setText(TXMCoreMessages.bind(CAUIMessages.axesP0P1RowsP2P3ContXP4ContYP5ColumnsP6P7ContXP8ContYP9
					, getResult().getFirstDimension()
					, getResult().getSecondDimension()
					, noDecimalFormatter.format(getResult().getRowsCount()-filtered[0])
					, noDecimalFormatter.format(getResult().getRowsCount())
					, noDecimalFormatter.format(sumcont[0])
					, noDecimalFormatter.format(sumcont[1])
					, noDecimalFormatter.format(getResult().getColumnsCount()-filtered[1])
					, noDecimalFormatter.format(getResult().getColumnsCount())
					, noDecimalFormatter.format(sumcont[2])
					, noDecimalFormatter.format(sumcont[3])));

			infoLine.getControl().pack(true);
			bottomToolBar.layout(true);
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void updateAvailablePlans() {
		plansSelectionCombo.updateDefaultValues();
	}


	public EigenvaluesChartEditor getSingularValuesEditor() {

		if (singularValuesEditor != null && !singularValuesEditor.isDisposed()) {
			return singularValuesEditor;
		}

		singularValuesEditor = null;
		//		if (parentMultiPagesEditor != null) {
		//			singularValuesEditor = ((CAEditor)parentMultiPagesEditor).getEigenvaluesBarChartEditor();
		//		} else {
		Eigenvalues infos = getResult().getFirstChild(Eigenvalues.class);
		if (infos == null) return null;

		Set<ITXMResultEditor<TXMResult>> editors = SWTEditorsUtils.getEditors(infos);
		if (editors != null) {
			for (ITXMResultEditor<TXMResult> editor : editors) {
				if (editor instanceof EigenvaluesChartEditor eEditor) {
					singularValuesEditor = eEditor;
					break;
				}
			}
		}
		//		}

		return singularValuesEditor;
	}

	public EigenvaluesTableEditor getSingularValuesTableEditor() {

		if (singularValuesTableEditor != null && !singularValuesTableEditor.isDisposed()) {
			return singularValuesTableEditor;
		}

		singularValuesTableEditor = null;
		//		if (parentMultiPagesEditor != null) {
		//			singularValuesTableEditor = ((CAEditor)parentMultiPagesEditor).getEigenvaluesTableEditor();
		//		} else {
		Eigenvalues infos = getResult().getFirstChild(Eigenvalues.class);
		if (infos == null) return null;

		Set<ITXMResultEditor<TXMResult>> editors = SWTEditorsUtils.getEditors(infos);
		if (editors != null) {
			for (ITXMResultEditor<TXMResult> editor : editors) {
				if (editor instanceof EigenvaluesTableEditor eEditor) {
					singularValuesTableEditor = eEditor;
					break;
				}
			}
		}
		//		}

		return singularValuesTableEditor;
	}

	public ColsRowsInfosEditor getColsInfosEditor() {

		if (colsInfosEditor != null && !colsInfosEditor.isDisposed()) {
			return colsInfosEditor;
		}

		colsInfosEditor = null;
		//		if (parentMultiPagesEditor != null) {
		//			colsInfosEditor = ((CAEditor)parentMultiPagesEditor).getColsInfosEditor();
		//		} else {
		CAColsInfo infos = getResult().getFirstChild(CAColsInfo.class);
		if (infos == null) return null;

		Set<ITXMResultEditor<TXMResult>> editors = SWTEditorsUtils.getEditors(infos);
		if (editors != null) {
			for (ITXMResultEditor<TXMResult> editor : editors) {
				if (editor instanceof ColsRowsInfosEditor criEditor) {
					colsInfosEditor = criEditor;
					break;
				}
			}
		}
		//}

		return colsInfosEditor;
	}

	public ColsRowsInfosEditor getRowsInfosEditor() {

		if (rowsInfosEditor != null && !rowsInfosEditor.isDisposed()) {
			return rowsInfosEditor;
		}
		rowsInfosEditor = null;
		//		if (parentMultiPagesEditor != null) {
		//			rowsInfosEditor = ((CAEditor)parentMultiPagesEditor).getRowsInfosEditor();
		//		} else {
		CARowsInfo infos = getResult().getFirstChild(CARowsInfo.class);
		if (infos == null) return null;

		Set<ITXMResultEditor<TXMResult>> editors = SWTEditorsUtils.getEditors(infos);
		if (editors != null) {
			for (ITXMResultEditor<TXMResult> editor : editors) {
				if (editor instanceof ColsRowsInfosEditor criEditor) {
					rowsInfosEditor = criEditor;
					break;
				}
			}
		}
		//		}

		return rowsInfosEditor;
	}

	@Override
	public void onInit() {

		IChartComponent chartComponent = this.getComposite().getChartComponent();

		// Axis unit square ratio constraint
		// chartComponent.setSquareOffEnabled(true);
		chartComponent.squareOff();

		// FIXME: this code is too much AWT related
		// Axis unit square ratio constraint on resize
		((Component) chartComponent).addComponentListener(new ComponentListener() {

			@Override
			public void componentShown(ComponentEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void componentResized(ComponentEvent e) {
				getComposite().getChartComponent().squareOff();
			}

			@Override
			public void componentMoved(ComponentEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void componentHidden(ComponentEvent arg0) {
				// TODO Auto-generated method stub
			}
		});
	}

	@Override
	public void onClosed() {
		if (getSingularValuesEditor() != null) getSingularValuesEditor().close();
		if (getSingularValuesTableEditor() != null) getSingularValuesTableEditor().close();
		if (getRowsInfosEditor() != null) getRowsInfosEditor().close();
		if (getColsInfosEditor() != null) getColsInfosEditor().close();
	}

	/**
	 *
	 * @return
	 */
	public StylingSheetTable getStylingSheetTable() {
		return stylingSheetEditorTable;
	}

	/**
	 *
	 * @return
	 */
	public CAFilterPanel getCAFilterPanel() {
		return this.caFilterPanel;
	}

	/**
	 *
	 * @param mirrored
	 */
	public void setMirroredAxis(ArrayList<Integer> mirrored) {
		mirroredDimensionsListViewer.setSelection(new StructuredSelection(mirrored));
		this.mirroredDimensions = mirrored;
	}
}
