package org.txm.ca.rcp.editors.styling;

import org.eclipse.jface.viewers.ComboBoxViewerCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * To fix https://bugs.eclipse.org/bugs/show_bug.cgi?id=230398 with workaround adding simple click selection and validation on focus lost features.
 * @author sjacquot
 *
 */
public class AutoDropComboBoxViewerCellEditor extends ComboBoxViewerCellEditor {

	/**
	 * 
	 * @param parent
	 */
	protected AutoDropComboBoxViewerCellEditor(Composite parent) {
		super(parent, SWT.READ_ONLY | SWT.SINGLE);
		this.setActivationStyle(DROP_DOWN_ON_MOUSE_ACTIVATION);
	}

	@Override
	protected Control createControl(Composite parent) {
		final Control control = super.createControl(parent);
		
		// trigger the validation if the list is not visible after a selection change
		//SJ: it doesn't work on Linux. Maybe because of the event order:
		// - Windows: trigger the selection change event then hide the combo box
		// - Linux: behavior may be inverted, therefore the list is visible 
		//SJ: need to check on Mac OS
		this.getViewer().getCCombo().addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (e.stateMask != 0) { // Ubuntu, mask = 0 if no mouse or keyboard input is done. TODO test if this code works on Windows and Mac OS X. Note: e.source and e.widget are always the CCombo.
					focusLost();
				}
				else if (!getViewer().getCCombo().getListVisible()) { // Windows
					//AutoDropComboBoxViewerCellEditor.this.fireApplyEditorValue();
					//FIXME: SJ: other way. To remove when the above method will be validated on the 3 OS
					focusLost();
				}
			}
		});
		return control;
	}
}
