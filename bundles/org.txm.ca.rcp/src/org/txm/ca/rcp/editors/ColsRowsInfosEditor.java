// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.ca.rcp.editors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.eclipse.jface.util.Util;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.ToolItem;
import org.txm.Toolbox;
import org.txm.ca.core.chartsengine.base.CAChartCreator;
import org.txm.ca.core.functions.CAColsInfo;
import org.txm.ca.core.functions.CAInfos;
import org.txm.ca.core.functions.CARowsInfo;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.ca.core.preferences.CAPreferences;
import org.txm.ca.rcp.chartsengine.events.CAPointSelectionCallBack;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TXMResultEditorInput;
import org.txm.rcp.editors.TableKeyListener;
import org.txm.rcp.editors.TableLinesViewerComparator;
import org.txm.rcp.editors.TableResultEditor;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.jface.TableViewerSeparatorColumn;
import org.txm.rcp.swt.widget.TXMParameterSpinner;

/**
 * Used to display rows and columns information such as contribution, inertia, mass, coordinates, etc.
 *
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ColsRowsInfosEditor extends CALinkedEditor<CAInfos> implements TableResultEditor {

	/**
	 * The viewer.
	 */
	private TableViewer viewer;

	private TableLinesViewerComparator viewerComparator;

	@Parameter(key = CAPreferences.EXPAND_DISPLAYED_AXIS)
	ToolItem expandAxis;

	@Parameter(key = CAPreferences.GROUP_BY_INFORMATIONS_COLUMN)
	ToolItem groupInformations;

	@Parameter(key = CAPreferences.SHOW_COORDINATES)
	ToolItem showCoordinates;

	@Parameter(key = CAPreferences.N_FACTORS_MEMORIZED)
	TXMParameterSpinner numberOfAvailableDimensions;

	private TableKeyListener tableKeyListener;

	public ColsRowsInfosEditor() {
		super();
	}

	/**
	 *
	 * @param result
	 */
	public ColsRowsInfosEditor(CAInfos result, final String tabTooltip) {
		super(new TXMResultEditorInput<TXMResult>(result) {

			@Override
			public String getToolTipText() {
				return tabTooltip;
			}
		});
	}

	@Override
	public void _init() {

		if (getResult() instanceof CAColsInfo) {
			this.setTitleImage(IImageKeys.getImage(ColsRowsInfosEditor.class, "icons/show_columns_information.png")); //$NON-NLS-1$
		}
		else {
			this.setTitleImage(IImageKeys.getImage(ColsRowsInfosEditor.class, "icons/show_rows_information.png")); //$NON-NLS-1$
		}
	}

	@Override
	public void _createPartControl() {

		// Some parameters

		this.getComputeButton().dispose();

		SelectionListener listener = new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				compute(true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};

		ToolItem showCAButton = new ToolItem(this.getTopToolbar(), SWT.PUSH);
		showCAButton.setToolTipText(CAUIMessages.displayTheCAPlanWindow);
		showCAButton.setImage(IImageKeys.getImage("platform:/plugin/org.txm.ca.rcp/icons/ca.png")); //$NON-NLS-1$
		showCAButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				openCAFactorialMapChartEditor();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});

		expandAxis = new ToolItem(this.getTopToolbar(), SWT.CHECK);
		expandAxis.setToolTipText(CAUIMessages.showAllAxisInformations);
		expandAxis.setImage(IImageKeys.getImage(this.getClass(), "icons/extend_axes_infos.png")); //$NON-NLS-1$
		expandAxis.addSelectionListener(listener);

		groupInformations = new ToolItem(this.getTopToolbar(), SWT.CHECK);
		groupInformations.setToolTipText(CAUIMessages.groupInformations);
		groupInformations.setImage(IImageKeys.getImage(this.getClass(), "icons/groupby_axes_infos.png")); //$NON-NLS-1$
		groupInformations.addSelectionListener(listener);

		showCoordinates = new ToolItem(this.getTopToolbar(), SWT.CHECK);
		showCoordinates.setToolTipText(CAUIMessages.showCoords);
		showCoordinates.setImage(IImageKeys.getImage(this.getClass(), "icons/show_coords_infos.png")); //$NON-NLS-1$
		showCoordinates.addSelectionListener(listener);

		//numberOfAvailableDimensions = new TXMParameterSpinner(this.getTopToolbar(), this, (String) null, "The maximum number of axis displayed");

		this.getTopToolbar().layout();

		// Result area
		GLComposite resultArea = this.getResultArea();

		// FIXME: SJ, 2025-01-15: SWT.VIRTUAL has been removed to fix a "null pointer" exception when using "Group informations", "Show all axes", "Display coordinates" on Windows system,
		// see: https://gitlab.huma-num.fr/txm/txm-src/-/issues/4085.
		// But since it also seems to fix "index out of bounds" exception of the "delete rows line" in lexical table command: see https://gitlab.huma-num.fr/txm/txm-src/-/issues/3966#note_124029, 
		// then I removed it for all 3 OS.
		// Original code:
		// viewer = new TableViewer(resultArea, SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI | SWT.FULL_SELECTION | SWT.VIRTUAL);
		// FIXME: SJ, 2025-01-16: need to try to fix the issues in another way and restore SWT.VIRTUAL, see: https://gitlab.huma-num.fr/txm/txm-src/-/issues/4127.
		viewer = new TableViewer(resultArea, SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI | SWT.FULL_SELECTION);
		
		
		
		viewer.getTable().setLayoutData(new GridData(GridData.FILL_BOTH));

		tableKeyListener = TableUtils.installCopyAndSearchKeyEvents(viewer);
		tableKeyListener.installSearchGroup(this);
		tableKeyListener.installMenuContributions(this);
		TableUtils.installColumnResizeMouseWheelEvents(viewer);
		TableUtils.installTooltipsOnSelectionChangedEvent(viewer);

		Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		String[] titles = this.getResult().getTitles();

		// creates the viewer comparator
		viewerComparator = new TableLinesViewerComparator(Toolbox.getCollator(this.getResult())) {

			@Override
			public int compare(Viewer viewer, Object e1, Object e2) {
				//				if (lastColumnIndex == 0) {
				//					return super.compare(viewer, e1, e2);
				//				}
				int icol = this.lastColumnIndex;
				try {
					e1 = getResult().getData(table.getColumns()[icol].getText(), (Integer) e1);
					e2 = getResult().getData(table.getColumns()[icol].getText(), (Integer) e2);

					int i = super.compare(viewer, e1, e2);
//					if (lastColumnIndex == 0 // name
//							|| getResult().getTitles()[lastColumnIndex].startsWith("c(")) { // coords //$NON-NLS-1$
//						return i;
//					}
					return i; // invert sort except for column name and positions
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return 0;
				}
			}

			@Override
			public int getNewSelectedColumnInitialDirection() {

				if (lastColumnIndex == 0 // name
						|| getResult().getTitles()[lastColumnIndex].startsWith("c(")) { // coords //$NON-NLS-1$
					return SWT.UP; // ASC sort
				}
				return SWT.DOWN; // DESC BY DEFAULT for the other columns
			}
		};

		createColumns(titles);

		viewer.setContentProvider(new IStructuredContentProvider() {

			@Override
			public Object[] getElements(Object inputElement) {
				return (Object[]) inputElement;
			}
		});

		//this.setContentDescription(NLS.bind(CAUIMessages.infosColonP0, titles[0]));

		// Selection changed listener to highlight CA chart points from table lines selection
		ISelectionChangedListener selectionChangedListener = new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent e) {

				CAFactorialMapChartEditor chartEditor = getCAFactorialMapChartEditor();
				if (chartEditor != null && !chartEditor.isDisposed()) {

					// Highlight points from table lines
					Table table = ((TableViewer) e.getSource()).getTable();
					String[] selectedLabels = new String[table.getSelectionCount()];
					TableItem[] selection = table.getSelection();

					for (int i = 0; i < selection.length; i++) {
						selectedLabels[i] = selection[i].getText();
					}

					boolean rows = getResult() instanceof CARowsInfo;

					((CAChartCreator) chartEditor.getResult().getChartCreator()).updateChartCAFactorialMapHighlightPoints(chartEditor.getChart(), rows, selectedLabels);
				}
			}
		};

		getViewer().addSelectionChangedListener(selectionChangedListener);

		// Selection listeners to change chart items selection order from table sorting
		SelectionListener selectionListener = new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				// Change chart items selection order
				CAFactorialMapChartEditor chartEditor = getCAFactorialMapChartEditor();
				if (chartEditor != null && !chartEditor.isDisposed()) {

					String[] rows = new String[0];
					String[] cols = new String[0];

					if (chartEditor.getColsInfosEditor() != null) {
						cols = chartEditor.getColsInfosEditor().getOrdererLabels();
					}
					if (chartEditor.getRowsInfosEditor() != null) {
						rows = chartEditor.getRowsInfosEditor().getOrdererLabels();
					}

					((CAChartCreator) chartEditor.getResult().getChartCreator()).updateChartCAFactorialMapSetLabelItemsSelectionOrder(chartEditor.getChart(), rows, cols);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};
		// Add listener to column to link chart items selection order with table sorting order
		for (int i = 0; i < getViewer().getTable().getColumnCount(); i++) {
			getViewer().getTable().getColumn(i).addSelectionListener(selectionListener);
		}


		// Register the context menu
		TXMEditor.initContextMenu(this.viewer.getTable(), this.getSite(), this.viewer);

	}



	@Override
	public void updateResultFromEditor() {
		// TODO Auto-generated method stub

	}


	@Override
	public void updateEditorFromResult(boolean update) throws Exception {

//		this.viewer.getControl().setRedraw(false);
//		this.viewer.refresh();

		if (this.viewer.getTable().isDisposed()) return; // abort
		Integer[] range = new Integer[this.getResult().getNames().length];
		for (int i = 0; i < this.getResult().getNames().length; i++) {
			range[i] = i;
		}

		// store the current selection before reloading the table, it is use later to restore the selection
		ArrayList<String> selectedRowLabels = null;
		ArrayList<String> selectedColumnLabels = null;
		if (this.getResult() instanceof CARowsInfo) {
			selectedRowLabels = ((CAChartCreator) getResult().getParent().getChartCreator()).getCAFactorialMapChartSelectedPoints(getResult().getParent().getChart(), 0);
		} else {
			selectedColumnLabels = ((CAChartCreator) getResult().getParent().getChartCreator()).getCAFactorialMapChartSelectedPoints(getResult().getParent().getChart(), 1);
		}

		this.viewer.setInput(range);

		String newNames = Arrays.toString(this.getResult().updateInfosTitles());
		String currentNames = "["; //$NON-NLS-1$
		for (TableColumn col : this.viewer.getTable().getColumns()) {
			if (currentNames.length() > 1) currentNames += ", "; //$NON-NLS-1$
			currentNames += col.getText();
		}
		currentNames += "]"; //$NON-NLS-1$

		if (!newNames.equals(currentNames)) { // must rebuild columns
			for (TableColumn col : this.viewer.getTable().getColumns()) {
				col.dispose();
			}
			createColumns(this.getResult().getTitles());
		}

		// Pack the columns
		TableUtils.packColumns(this.viewer);

		if (this.getResult() instanceof CARowsInfo) {
			CAPointSelectionCallBack.updateColsRowsInfoTableSelections(getCAFactorialMapChartEditor(), this, null, null, null, selectedRowLabels, selectedColumnLabels);
		}
		else {
			CAPointSelectionCallBack.updateColsRowsInfoTableSelections(getCAFactorialMapChartEditor(), null, this, null, null, selectedRowLabels, selectedColumnLabels);
		}
	}


	private void createColumns(String[] titles) {

		for (int i = 0; i < titles.length; i++) {
			final int index = i;
			final TableViewerColumn column;
			if (index == 0) { // names
				column = new TableViewerColumn(viewer, SWT.LEFT);
			}
			else { // numeric values
				column = new TableViewerColumn(viewer, SWT.RIGHT);
			}
			column.getColumn().setText(titles[i]);

			String name = titles[i];
			if (name.startsWith(CACoreMessages.common_mass)) { // create a label provider of each column type
				column.setLabelProvider(new ColRowInfosLabelProvider(getResult(), name) {

					@Override
					public String getText(Object element) {
						int line = (int) element;
						return infoFormatter.format(getResult().getMass()[line]);
					}
				});
			}
			else if (name.startsWith(CAUIMessages.chi)) {
				column.setLabelProvider(new ColRowInfosLabelProvider(getResult(), name) {

					@Override
					public String getText(Object element) {
						int line = (int) element;
						return infoFormatter.format(getResult().getAllDists()[line]);
					}
				});
			}
			else if (name.startsWith(CACoreMessages.common_dist)) {
				column.setLabelProvider(new ColRowInfosLabelProvider(getResult(), name) {

					@Override
					public String getText(Object element) {
						int line = (int) element;
						return infoFormatter.format(getResult().getAllDists()[line]);
					}
				});
			}
			else if (name.startsWith(CACoreMessages.common_cos2)) {
				column.setLabelProvider(new ColRowInfosLabelProvider(getResult(), name) {

					@Override
					public String getText(Object element) {
						int line = (int) element;
						int i = Integer.parseInt(name.substring(CACoreMessages.common_cos2.length() + 1, name.length() -1 ));
						return infoFormatter.format(getResult().getAllCos2s()[line][i - 1]);
					}
				});

			}
			else if (name.startsWith(CACoreMessages.common_cont)) {
				column.setLabelProvider(new ColRowInfosLabelProvider(getResult(), name) {

					@Override
					public String getText(Object element) {
						int line = (int) element;
						int i = Integer.parseInt(name.substring(CACoreMessages.common_cont.length() + 1, name.length() -1));
						return infoFormatter.format(getResult().getAllContribs()[line][i - 1]);
					}
				});
			}
			else if (name.equals(getResult().getTitles()[0])) { // Rows/Columns
				column.setLabelProvider(new ColRowInfosLabelProvider(getResult(), name) {

					@Override
					public String getText(Object element) {
						int line = (int) element;
						return getResult().getNames()[line];
					}
				});
			}
			else if (name.startsWith("Q")) { //$NON-NLS-1$
				column.setLabelProvider(new ColRowInfosLabelProvider(getResult(), name) {

					@Override
					public String getText(Object element) {
						int line = (int) element;
						double[] cos2 = getResult().getAllCos2s()[line];
						return infoFormatter.format(cos2[getResult().getParent().getFirstDimension() - 1] + cos2[getResult().getParent().getSecondDimension() - 1]);
					}
				});
			}
			else if (name.startsWith("c(")) { //$NON-NLS-1$
				column.setLabelProvider(new ColRowInfosLabelProvider(getResult(), name) {

					@Override
					public String getText(Object element) {
						int line = (int) element;
						int i = Integer.parseInt(name.substring(2, name.length() -1)); // c ( number )
						return infoFormatter.format(getResult().getAllCoords()[line][i - 1]);
					}
				});
			}
			else {
				column.setLabelProvider(new ColumnLabelProvider() {

					@Override
					public String getText(Object element) {
						return TableViewerSeparatorColumn.EMPTY;
					}
				});
				column.getColumn().setData("separator", true); //$NON-NLS-1$
			}
		}

		viewer.setComparator(viewerComparator);

		viewerComparator.addSelectionAdapters(viewer);
	}


	/**
	 *
	 * @return
	 */

	public String[] getTableTitles() {

		return this.getResult().getTitles();
	}


	/**
	 * Gets the ordered labels according to the current table sort.
	 *
	 * @return
	 */
	public String[] getOrdererLabels() {

		TableItem[] items = this.viewer.getTable().getItems();
		String[] labels = new String[items.length];
		for (int i = 0; i < items.length; i++) {
			labels[i] = items[i].getText();
		}

		return labels;
	}


	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void _setFocus() {
		viewer.getControl().setFocus();
	}

	/**
	 * @return the viewer
	 */
	public TableViewer getViewer() {
		return viewer;
	}


	@Override
	public TableViewer[] getTableViewers() {

		return new TableViewer[] { viewer };
	}

	@Override
	public TableKeyListener getTableKeyListener() {

		// TODO Auto-generated method stub
		return tableKeyListener;
	}
}
