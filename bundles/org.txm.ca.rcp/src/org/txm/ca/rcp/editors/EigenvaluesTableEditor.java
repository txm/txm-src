// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.ca.rcp.editors;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.ToolItem;
import org.txm.Toolbox;
import org.txm.ca.core.functions.Eigenvalues;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.core.results.TXMResult;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.ITXMResultEditor;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TableKeyListener;
import org.txm.rcp.editors.TableLinesViewerComparator;
import org.txm.rcp.editors.TableResultEditor;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.utils.logger.Log;


/**
 * Displays the singular values and computes some stats.
 *
 * @author mdecorde
 * @author sjacquot
 */
public class EigenvaluesTableEditor extends CALinkedEditor<Eigenvalues> implements TableResultEditor {

	/** The viewer. */
	private TableViewer viewer;

	private TableKeyListener tableKeyListener;

	public EigenvaluesTableEditor() {
		super();
	}
	
	/**
	 * 
	 * @param result
	 */
	public EigenvaluesTableEditor(TXMResult result) {
		super(result);
	}

	@Override
	public void _createPartControl() throws Exception {

		// remove the compute button
		this.removeComputeButton();
		
		ToolItem showCAButton = new ToolItem(this.getTopToolbar(), SWT.PUSH);
		showCAButton.setToolTipText(CAUIMessages.displayTheCAPlanWindow);
		showCAButton.setImage(IImageKeys.getImage("platform:/plugin/org.txm.ca.rcp/icons/ca.png")); //$NON-NLS-1$
		showCAButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				openCAFactorialMapChartEditor();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});

		ToolItem showSingularValuesTableEditor = new ToolItem(this.getTopToolbar(), SWT.PUSH);
		showSingularValuesTableEditor.setImage(IImageKeys.getImage(CAFactorialMapChartEditor.class, "icons/eigenvalues.png")); //$NON-NLS-1$
		showSingularValuesTableEditor.setToolTipText(CAUIMessages.showTheSingularValuesChart);
		showSingularValuesTableEditor.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				ITXMResultEditor<TXMResult> editor = SWTEditorsUtils.getEditor(getResult().getParent());
				if (editor != null && editor instanceof CAFactorialMapChartEditor caEditor) {
					caEditor.openSingularValuesEditor();
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});
		
		GLComposite resultArea = this.getResultArea();

		viewer = new TableViewer(resultArea, SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI | SWT.FULL_SELECTION | SWT.VIRTUAL);

		viewer.getTable().setLayoutData(new GridData(GridData.FILL_BOTH));

		tableKeyListener = TableUtils.installCopyAndSearchKeyEvents(viewer);
		tableKeyListener.installSearchGroup(this);
		tableKeyListener.installMenuContributions(this);
		TableUtils.installColumnResizeMouseWheelEvents(viewer);
		TableUtils.installTooltipsOnSelectionChangedEvent(viewer);

		viewer.getTable().setHeaderVisible(true);
		viewer.getTable().setLinesVisible(true);

		String[] titles = new String[] {
				CAUIMessages.CorrespondanceAnalysisEditorInput_4,
				CAUIMessages.eigenvalue,
				"%", CAUIMessages.CorrespondanceAnalysisEditorInput_11, "", "" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$;

		int sizes[] = { 50, 120, 70, 70, 100, 100 };
		for (int i = 0; i < titles.length; i++) {
			final TableViewerColumn column;

			int alignment = SWT.RIGHT;
			// Align the bar plot to the left
			if (i == 4) {
				alignment = SWT.LEFT;
			}

			column = new TableViewerColumn(viewer, alignment);

			column.getColumn().setText(titles[i]);
			column.getColumn().setWidth(sizes[i]);
			column.getColumn().setResizable(true);
			column.getColumn().setMoveable(true);
		}


		// To draw the bar plot column
		viewer.getTable().addListener(SWT.EraseItem, new Listener() {

			@Override
			public void handleEvent(Event event) {

				if (event.index == 4) {
					int itemIndex = viewer.getTable().indexOf((TableItem) event.item);
					String s = ((ArrayList<?>) viewer.getElementAt(itemIndex)).get(2).toString().replaceAll(",", "."); //$NON-NLS-1$
					double percent = Double.parseDouble(s); // comma replacement according to the locale //$NON-NLS-1$ //$NON-NLS-2$

					int maxPercentItemIndex = 0;
					double maxPercent = Double.parseDouble(viewer.getTable().getItem(0).getText(2).replaceAll(",", ".")); //$NON-NLS-1$ //$NON-NLS-2$
					double tmpMaxPercent;

					// Get the max percent despite of the current row sorting
					for (int i = 0; i < viewer.getTable().getItemCount(); i++) {
						tmpMaxPercent = Double.parseDouble(viewer.getTable().getItem(i).getText(2).replaceAll(",", ".")); //$NON-NLS-1$ //$NON-NLS-2$
						if (tmpMaxPercent > maxPercent) {
							maxPercent = tmpMaxPercent;
							maxPercentItemIndex = i;
						}
					}

					event.gc.setBackground(viewer.getTable().getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));
					event.gc.fillRectangle(event.x, event.y + 1, (int) (event.width * percent / Double.parseDouble(viewer.getTable().getItem(maxPercentItemIndex).getText(2).replaceAll(",", "."))), event.height - 1); //$NON-NLS-1$//$NON-NLS-2$  
				}
			}
		});

		viewer.setContentProvider(ArrayContentProvider.getInstance());
		viewer.setLabelProvider(new CASingularValueLabelProvider());

		// creates the viewer comparator
		TableLinesViewerComparator viewerComparator = new TableLinesViewerComparator(Toolbox.getCollator(this.getResult()), false);
		viewer.setComparator(viewerComparator);
		viewerComparator.addSelectionAdapters(viewer);

		// Register the context menu
		TXMEditor.initContextMenu(this.viewer.getTable(), this.getSite(), this.viewer);
	}


	@Override
	public void updateEditorFromResult(boolean update) throws Exception {

		try {
			this.viewer.setInput(this.getResult().getSingularValuesInfos().toArray());
		}
		catch (Exception e) {
			Log.printStackTrace(e);
			this.viewer.setInput(null);
		}

		////		// Pack the columns except the bar plot column
		////		for(int i = 0; i < viewer.getTable().getColumnCount(); i++)	{
		////			if(i != 4)	{
		////				this.viewer.getTable().getColumn(i).pack();
		////			}
		////		}
		//		
		//		
		//		float W = 6.5f ;
		//		
		//		int n = 0;
		//		TableColumn[] columns = viewer.getTable().getColumns();
		//		for (int i = 0; i < columns.length; i++) {
		//			TableColumn column = columns[i];
		//			if (column.getWidth() == 0) continue; // skip closed columns
		//			
		//			int max = column.getText().length();
		//			if (max == 0) continue; // skip no-title columns
		//			
		//			n = 0;
		//			for (TableItem item : viewer.getTable().getItems()) {
		//				if (max < item.getText(i).length()) max = item.getText(i).length();
		//				if (n++ > 100) break; // stop testing after 100 lines
		//			}
		//			
		//			column.setWidth(15 + (int) (max * W));
		//		}

		viewer.getTable().layout(true, true);
		this.viewer.refresh();

	}



	@Override
	public void _setFocus() {
		viewer.getControl().setFocus();
	}

	@Override
	public void updateResultFromEditor() {
		// nothing to do
	}

	/**
	 * The Class CASingularValueLabelProvider.
	 */
	private class CASingularValueLabelProvider extends LabelProvider implements ITableLabelProvider {

		/**
		 * Percent values number format.
		 */
		protected DecimalFormat percentValuesNumberFormat = new DecimalFormat(new String("0.00")); //$NON-NLS-1$

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
		 */
		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			// TODO Auto-generated method stub
			return null;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
		 */
		@Override
		@SuppressWarnings("unchecked") //$NON-NLS-1$
		public String getColumnText(Object element, int columnIndex) {
			DecimalFormat f;
			Object o = ((List<Object>) element).get(columnIndex);
			switch (columnIndex) {
				case 0: // factor
					return o.toString();
				case 1: // eigenvalue
					if ((Double) o < 1) {
						if ((Double) o < 0.0001)
							f = new DecimalFormat("0.0000E00"); //$NON-NLS-1$
						else
							f = new DecimalFormat("0.0000"); //$NON-NLS-1$
					}
					else {
						f = new DecimalFormat("##########0.0000"); //$NON-NLS-1$
					}
					// FIXME: printf syntax tests
					//return String.format("%.4f", o);
					return f.format(o);
				case 2: // percent
				case 3: // cumul
					return percentValuesNumberFormat.format(o);
				default:
					return ""; //$NON-NLS-1$
			}
		}
	}

	@Override
	public TableViewer[] getTableViewers() {

		return new TableViewer[] { viewer };
	}

	@Override
	public TableKeyListener getTableKeyListener() {

		return tableKeyListener;
	}

}
