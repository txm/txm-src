package org.txm.ca.rcp.editors.styling;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.chartsengine.core.styling.StylingRule;
import org.txm.chartsengine.core.styling.definitions.functions.TestFunctionDefinition;
import org.txm.chartsengine.core.styling.instructions.SelectionInstruction;
import org.txm.core.preferences.TBXPreferences;
import org.txm.utils.OSDetector;

public class SelectionThresholdDialog extends Dialog {
	
	StyleDialogCellEditor cellEditor;
	
	SelectionInstruction instruction;
	String funcId = null;
	Double d = 0.0d;
	
	private Combo testCombo;

	private Spinner valueSpinner;
	private Control openPosition;
	
	protected SelectionThresholdDialog(StyleDialogCellEditor styleSettingsDialogCellEditor, Shell shell, SelectionInstruction instruction, Control openPosition) {
		super(shell);
		this.cellEditor = styleSettingsDialogCellEditor;
		this.instruction = instruction;
		this.openPosition = openPosition;
		
		if (openPosition != null) {
			// NOTE SJ: the modal option is not compatible with the validate or close on click outside feature (when using SWT.Deactivate event)
			//FIXME: SJ: see if we decide to force real time update, then remove the test
			if(TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
				this.setShellStyle(SWT.NONE);
			}
			else {
				this.setShellStyle(SWT.NONE | SWT.APPLICATION_MODAL);
			}
		}
	}
	
	@Override
	protected Point getInitialLocation(Point initialSize) {
		
		if (openPosition != null) {
			return openPosition.getDisplay().map(openPosition, null, 0, openPosition.getSize().y);
		}
		return super.getInitialLocation(initialSize);
	}
	
	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		Composite composite = (Composite) super.createDialogArea(parent);
		
		GridLayout layout = (GridLayout) composite.getLayout();
		layout.numColumns = 5;
		layout.makeColumnsEqualWidth = false;

		Label l = new Label(composite, SWT.NONE);
		l.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		l.setText(instruction.getInstruction().getLabel() + " " + instruction.getParameter().getLabel()); //$NON-NLS-1$
		
		testCombo = new Combo(composite, SWT.SINGLE | SWT.READ_ONLY);
		GridData gdata = new GridData(GridData.FILL, GridData.FILL, true, false);
		gdata.minimumWidth = 100;
		testCombo.setLayoutData(gdata);
		testCombo.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				funcId = testCombo.getText();
				
				// force the chart update
				//FIXME: SJ: see if we decide to force real time update, then remove the test
				if(TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
					cellEditor.getEs().setValue2((StylingRule) cellEditor.getElement(), getSelectedValues());
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});
		
		// get the available function IDS
		ArrayList<TestFunctionDefinition> values = instruction.getInstructionsCatalog().getFunctionsFor(instruction.getInstruction());
		String[] ids = new String[values.size()];
		for (int i = 0 ; i < values.size() ; i++) {
			ids[i] = values.get(i).getID();
		}
		testCombo.setItems(ids);
		if (instruction.getFunction() != null) {
			testCombo.setText(instruction.getFunction().getID());
			funcId = instruction.getFunction().getID();
		}
		
		valueSpinner = new Spinner(composite, SWT.NONE);
		valueSpinner.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		valueSpinner.setDigits(2);
		valueSpinner.setMaximum(Integer.MAX_VALUE);
		valueSpinner.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				d = valueSpinner.getSelection() / 100d;
				
				// force the chart update
				//FIXME: SJ: see if we decide to force real time update, then remove the test
				if(TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
					cellEditor.getEs().setValue2((StylingRule) cellEditor.getElement(), getSelectedValues());
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});

		if (instruction.getSettings() != null && instruction.getSettings() instanceof Number n) {
			d = n.doubleValue();
			valueSpinner.setSelection((int) (100 * d));
		}
		
		return composite;
	}

	@Override
	protected void configureShell(Shell shell) {
		
		super.configureShell(shell);
		
		shell.setText(CAUIMessages.bind(CAUIMessages.thresholdOfP0P1, instruction.getInstruction().getLabel(), instruction.getParameter().getLabel()));
		//shell.setSize(shell.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));

		
		if (openPosition != null) {
			shell.setLocation(shell.getDisplay().map(openPosition, null, 0, openPosition.getSize().y));

			// to validate values and close the dialog box when clicking outside
			//FIXME: SJ: see if we decide to force real time update, then remove the test
//			if(TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
//				if (!OSDetector.isFamilyUnix()) {
					shell.addListener(SWT.Deactivate, new Listener() {

						@Override
						public void handleEvent(Event event) {
							if (!testCombo.getListVisible()) {
								cancelPressed();
							}
						}
					});
//				}
//			}
		}
	}

	public Object[] getSelectedValues() {

		return new Object[] {funcId, d};
	}



}
