package org.txm.ca.rcp.editors.styling;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ComboBoxViewerCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.txm.chartsengine.core.styling.instructions.StylingInstruction;

public class StyleEditSupport extends EditingSupport {

	ComboBoxViewerCellEditor cellEditor;
	
	static String EMPTY = "";

	public StyleEditSupport(TreeViewer treeViewer, Object input) {

		super(treeViewer);

		cellEditor = new ComboBoxViewerCellEditor(treeViewer.getTree(), SWT.READ_ONLY | SWT.SINGLE);
		cellEditor.setContentProvider(new ArrayContentProvider());
		cellEditor.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				return getObjectValue(element);
			}
		});
		cellEditor.setInput(input);
	}

	@Override
	protected void setValue(Object element, Object value) {
		if (value == null) return;

		if (element instanceof StylingInstruction<?> s) {

			saveObjectValue(s, value);
			getViewer().update(element, null);
			getViewer().refresh();
		}
	}

	protected void saveObjectValue(Object s, Object v) {
	};

	protected String getObjectValue(Object s) {
		return EMPTY;
	};

	@Override
	protected Object getValue(Object element) {

		if (element instanceof StylingInstruction<?> s) {
			return getObjectValue(s);
		}
		return element.toString();
	}

	@Override
	protected CellEditor getCellEditor(Object element) {

		if (element instanceof StylingInstruction<?> s) {
			return cellEditor;
		}
		return null;
	}

	@Override
	protected boolean canEdit(Object element) {

		if (element instanceof StylingInstruction<?> s) {
			return true;
		}
		return false;
	}
}