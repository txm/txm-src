// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.ca.rcp.editors;

import java.text.DecimalFormat;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.graphics.Image;
import org.txm.ca.core.functions.CAInfos;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.ca.rcp.messages.CAUIMessages;

/**
 * The Class ColRowInfosLabelProvider.
 * TODO simplify this class, one LabelProvider must be instanciated per column -> only one formatter to initialize
 */
public class ColRowInfosLabelProvider extends ColumnLabelProvider {

	/** The qualitypattern. */
	public DecimalFormat infoFormatter = new DecimalFormat("0.00"); //$NON-NLS-1$

	private CAInfos infos;
	private String infoName;

	/**
	 * Instantiates a new col row infos label provider.
	 *
	 * @param names the names
	 */
	public ColRowInfosLabelProvider(CAInfos infos, String infoName) {
		super();
		this.infos = infos;
		this.infoName = infoName;
		infoFormatter = infos.getCA().getFormatter(infoName);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
	 */
	@Override
	public Image getImage(Object element) {
		return null;
	}

	public static final String EMPTY = "";

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
	 */
	@Override
	public String getText(Object element) {
		// names[columnIndex] = Rows,Q12,Q13,Q23,Mass,Chi²,Contrib,Cos²,common_c1,common_c2,common_c3

		Integer line = (Integer) element;
		String name = infoName;
		if (name.startsWith(CACoreMessages.common_mass)) {
			return infoFormatter.format(infos.getMass()[line]);
		}
		else if (name.startsWith(CAUIMessages.chi)) {
			return infoFormatter.format(infos.getAllDists()[line]);
		}
		else if (name.startsWith(CACoreMessages.common_dist)) {
			return infoFormatter.format(infos.getAllDists()[line]);
		}
		else if (name.startsWith(CACoreMessages.common_cos2)) {
			int i = Integer.parseInt(name.substring(CACoreMessages.common_cos2.length()));
			return infoFormatter.format(infos.getAllCos2s()[line][i - 1]);
		}
		else if (name.startsWith(CACoreMessages.common_cont)) {
			int i = Integer.parseInt(name.substring(CACoreMessages.common_cont.length()));
			return infoFormatter.format(infos.getAllContribs()[line][i - 1]);
		}
		else if (name.startsWith("Q(")) { //$NON-NLS-1$
			double[] cos2 = infos.getAllCos2s()[line];
			return infoFormatter.format(cos2[infos.getParent().getFirstDimension() - 1] + cos2[infos.getParent().getSecondDimension() - 1]);
		}
		else if (name.startsWith("c(")) { //$NON-NLS-1$
			int i = Integer.parseInt(name.substring(2, name.length() -1)); // c ( number )
			return infoFormatter.format(infos.getAllCoords()[line][i - 1]);
		}
		else if (name.equals(infos.getTitles()[0])) {
			return infos.getNames()[line];
		}

		return EMPTY;
	}
}
