package org.txm.ca.rcp.editors.styling;

import java.awt.Color;
import java.awt.Font;

import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FontDialog;
import org.txm.chartsengine.core.styling.instructions.SelectionInstruction;
import org.txm.chartsengine.core.styling.instructions.StylingInstruction;
import org.txm.rcp.swt.TXMFontRegistry;
import org.txm.utils.serializable.Color1Color2;
import org.txm.utils.serializable.MinMax;

public class StyleDialogCellEditor extends DialogCellEditor {
	
	StylingSheetTable.TestEditingSupport es;
	
	Object element;
	
	public StyleDialogCellEditor(StylingSheetTable.TestEditingSupport es, Object element, Composite parent) {
		super(parent);
		this.es = es;
		this.element = element;
	}


//	Button opendialogbutton;
//	
//	@Override
//	public Button createButton(Composite editor) {
//		opendialogbutton = super.createButton(editor);
//		return opendialogbutton;
//	}
//	
//	@Override
//	public void activate(ColumnViewerEditorActivationEvent activationEvent) {
//		super.activate(activationEvent);
////		for (Listener  l : opendialogbutton.getListeners(SWT.Selection)) {
////			if (l instanceof TypedListener se) {
//////				SelectionListener se = (SelectionListener) opendialogbutton.getListeners(SWT.SELEC)[0];
////				((SelectionListener)se.getEventListener()).widgetSelected(new SelectionEvent(activationEvent.eve));
////			}
////		}
//		
//		opendialogbutton.act
//	}
	
	
	
	@Override
	protected Object openDialogBox(Control cellEditorWindow) {
		
		if (getValue() instanceof StylingInstruction<?> inst) {
			
			if (inst.getSettings() instanceof MinMax) {
				MinMaxStyleSettingsDialog dialog = new MinMaxStyleSettingsDialog(this, cellEditorWindow.getShell(), inst, cellEditorWindow);
				if (dialog.open() == MinMaxStyleSettingsDialog.OK) {
					return dialog.getSelectedValues();
				} else {
					System.out.println("Canceled.");
					return null;
				}
			} else if (inst.getSettings() instanceof Color c) {

				ColorDialog dialog = new ColorDialog(cellEditorWindow.getShell());
				RGB rgb = dialog.open();
				if (rgb != null) {
					return new Object[] { new Color(rgb.red, rgb.green, rgb.blue)};
				}
			} else if (inst.getSettings() instanceof Color1Color2 cc) {

				Color color1, color2;
				
				ColorDialog dialog = new ColorDialog(cellEditorWindow.getShell());
				dialog.setRGB(new RGB(cc.color1().getRed(), cc.color1().getGreen(), cc.color1().getBlue()));
				RGB rgb = dialog.open();
				if (rgb != null) {
					color1 = new Color(rgb.red, rgb.green, rgb.blue);
				} else {
					return null;
				}
				
				dialog = new ColorDialog(cellEditorWindow.getShell());
				dialog.setRGB(new RGB(cc.color2().getRed(), cc.color2().getGreen(), cc.color2().getBlue()));
				rgb = dialog.open();
				if (rgb != null) {
					color2 = new Color(rgb.red, rgb.green, rgb.blue);
				} else {
					return null;
				}
				
				return new Object[] {color1, color2};
			} else if (inst.getSettings() instanceof Font f) {

				FontDialog dialog = new FontDialog(cellEditorWindow.getShell());
				org.eclipse.swt.graphics.Font fswt = TXMFontRegistry.getFontFromAWTFont(f);
				dialog.setFontList(fswt.getFontData());
				FontData fdata = dialog.open();
				if (fdata != null) {
					return new Object[] { TXMFontRegistry.getAWTFontFromSWTFont(fdata)};
				}
			}
		} else if (getValue() instanceof SelectionInstruction inst) {
			SelectionThresholdDialog dialog = new SelectionThresholdDialog(this, cellEditorWindow.getShell(), inst, cellEditorWindow);
			if (dialog.open() == SelectionThresholdDialog.OK) {
				return dialog.getSelectedValues();
			} else {
//				System.out.println("Canceled.");
			}
		}
		return null;
	}

	@Override
	protected void updateContents(Object value) {
		
		if (value instanceof StylingInstruction<?> si) {

			if (si.getSettings() instanceof MinMax mm) {
				getDefaultLabel().setText(""+mm.min()+" -> "+mm.max()); //$NON-NLS-1$
			} else if (si.getSettings() instanceof Color c) {
				getDefaultLabel().setText("Color("+c.getRed()+", "+c.getGreen()+", "+c.getBlue()+")"); //$NON-NLS-1$
			} else if (si.getSettings() instanceof Font f) {
				getDefaultLabel().setText(f.getFamily()+" h:"+f.getSize()+" style:"+f.getStyle()); //$NON-NLS-1$
			} else if (si.getSettings() instanceof Color1Color2 cc) {
				getDefaultLabel().setText("Color("+cc.color1().getRed()+", "+cc.color1().getGreen()+", "+cc.color1().getBlue()+" -> "+cc.color2().getRed()+", "+cc.color2().getGreen()+", "+cc.color2().getBlue()+")"); //$NON-NLS-1$
			} else if (si.getSettings() instanceof MinMax mm) {
				getDefaultLabel().setText(""+mm.min() + " -> " + mm.max()); //$NON-NLS-1$
			} else if (si.getSettings() != null) {
				getDefaultLabel().setText(si.getSettings().toString());
			} 
			else {
				getDefaultLabel().setText(""); //$NON-NLS-1$
			}
		} else if (value instanceof SelectionInstruction si && si.getSettings() != null) {
			getDefaultLabel().setText(si.getFunction().getLabel() + " " + si.getSettings()); //$NON-NLS-1$
		} else {
			super.updateContents(value);
		}
	}
	
	/**
	 * @return the es
	 */
	public StylingSheetTable.TestEditingSupport getEs() {
		return es;
	}


	
	/**
	 * @return the element
	 */
	public Object getElement() {
		return element;
	}

	
	
	
}
