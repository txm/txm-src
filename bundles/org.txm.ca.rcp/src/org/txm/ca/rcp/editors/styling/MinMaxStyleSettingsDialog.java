package org.txm.ca.rcp.editors.styling;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.chartsengine.core.styling.StylingRule;
import org.txm.chartsengine.core.styling.instructions.StylingInstruction;
import org.txm.core.preferences.TBXPreferences;
import org.txm.utils.serializable.MinMax;

public class MinMaxStyleSettingsDialog extends Dialog {
	
	StyleDialogCellEditor cellEditor;
	
	StylingInstruction<?> instruction;
	String funcId = null;
	Double dMin = 0.0d;
	Double dMax = 0.0d;
	
	private Spinner minValueSpinner;
	private Spinner maxValueSpinner;
	private Control openPosition;

	protected MinMaxStyleSettingsDialog(StyleDialogCellEditor cellEditor, Shell shell, StylingInstruction<?> instruction, Control openPosition) {
		super(shell);
		this.cellEditor = cellEditor;
		this.instruction = instruction;
		this.openPosition = openPosition;
		
		if (openPosition != null) {
			// NOTE SJ: the modal option is not compatible with the validate or close on click outside feature (when using SWT.Deactivate event)
			//FIXME: SJ: see if we decide to force real time update, then remove the test
			if(TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
				this.setShellStyle(SWT.NONE);
			}
			else {
				this.setShellStyle(SWT.NONE | SWT.APPLICATION_MODAL);
			}
		}
	}
	
	@Override
	protected Point getInitialLocation(Point initialSize) {
		if (openPosition != null) {
			return openPosition.getDisplay().map(openPosition, null, 0, openPosition.getSize().y);
		}
		return super.getInitialLocation(initialSize);
	}
	
	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		Composite composite = (Composite) super.createDialogArea(parent);
		
		GridLayout layout = (GridLayout) composite.getLayout();
		layout.numColumns = 5;
		layout.makeColumnsEqualWidth = false;

		Label l = new Label(composite, SWT.NONE);
		l.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		l.setText(instruction.getSettings().getClass().getSimpleName());
		
		
		minValueSpinner = new Spinner(composite, SWT.BORDER);
		minValueSpinner.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		minValueSpinner.setDigits(2);
		minValueSpinner.setIncrement(100);
		minValueSpinner.setMaximum(Integer.MAX_VALUE);
		
		minValueSpinner.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				dMin = minValueSpinner.getSelection() / 100d;
				maxValueSpinner.setMinimum(minValueSpinner.getSelection());
				// force the chart update
				//FIXME: SJ: see if we decide to force real time update, then remove the test
				if(TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
					cellEditor.getEs().setValue2((StylingRule) cellEditor.getElement(), getSelectedValues());
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});
		
		maxValueSpinner = new Spinner(composite, SWT.BORDER);
		maxValueSpinner.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		maxValueSpinner.setDigits(2);
		maxValueSpinner.setIncrement(100);
		maxValueSpinner.setMaximum(Integer.MAX_VALUE);
		maxValueSpinner.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				dMax = maxValueSpinner.getSelection() / 100d;
				minValueSpinner.setMaximum(maxValueSpinner.getSelection());
				// force the chart update
				//FIXME: SJ: see if we decide to force real time update, then remove the test
				if(TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
					cellEditor.getEs().setValue2((StylingRule) cellEditor.getElement(), getSelectedValues());
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});

		if (instruction.getSettings() != null && instruction.getSettings() instanceof MinMax minMax) {
			minValueSpinner.setMaximum((int) (100 * minMax.max().doubleValue()));
			maxValueSpinner.setMinimum((int) (100 * minMax.min().doubleValue()));
			minValueSpinner.setSelection((int) (100 * minMax.min().doubleValue()));
			maxValueSpinner.setSelection((int) (100 * minMax.max().doubleValue()));
			
			dMin = minMax.min().doubleValue();
			dMax = minMax.max().doubleValue();
		}
		
		return composite;
	}

	@Override
	protected void configureShell(Shell shell) {
		
		super.configureShell(shell);
		
		shell.setText(CAUIMessages.bind(CAUIMessages.settingsOfP0P1, instruction.getInstruction().getLabel(), instruction.getParameter().getLabel()));
		//shell.setSize(shell.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));

		
		if (openPosition != null) {
			shell.setLocation(shell.getDisplay().map(openPosition, null, 0, openPosition.getSize().y));

			// to validate values and close the dialog box when clicking outside
			//FIXME: SJ: see if we decide to force real time update, then remove the test
//			if(TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
//				if (!OSDetector.isFamilyUnix()) shell.addListener(SWT.Deactivate, event -> okPressed());
//			}
			
//			shell.addListener(SWT.Deactivate, new Listener() {
//
//				@Override
//				public void handleEvent(Event event) {
//					if (!min.getCombo().getListVisible()) {
//						cancelPressed();
//					}
//				}
//			});
			shell.addListener(SWT.Deactivate, event -> cancelPressed());
		}
	}

	public Object[] getSelectedValues() {
		return new Object[] {dMin, dMax};
	}
}
