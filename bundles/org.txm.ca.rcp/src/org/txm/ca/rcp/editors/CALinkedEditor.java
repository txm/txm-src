package org.txm.ca.rcp.editors;

import java.util.Set;

import org.txm.ca.core.functions.CA;
import org.txm.ca.rcp.handlers.ComputeCA;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.ITXMResultEditor;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TXMResultEditorInput;
import org.txm.rcp.utils.SWTEditorsUtils;

/**
 * 
 * @author mdecorde
 *
 * @param <R> the sub result of CA
 */
public abstract class CALinkedEditor<R extends TXMResult> extends TXMEditor<R> {

	CAFactorialMapChartEditor caFactorialMapEditor;

	public CALinkedEditor() {
		super();
	}
	
	public CALinkedEditor(TXMResultEditorInput<TXMResult> txmResultEditorInput) {
		super(txmResultEditorInput);
	}

	public CALinkedEditor(TXMResult result) {
		super(result);
	}

	/**
	 * *
	 * @return the associated CA Editor
	 */
	public final CAFactorialMapChartEditor openCAFactorialMapChartEditor() {
		CAFactorialMapChartEditor editor = getCAFactorialMapChartEditor();
		if (editor != null) {
			editor.getSite().getPage().activate(editor);
			return editor;
		}
		Object o = ComputeCA.open((CA) getResult().getParent(), false);
		if (o != null && o instanceof CAFactorialMapChartEditor caEditor) {
			return caEditor;
		}
		
		return editor; //  no luck ?
	}
	
	/**
	 * *
	 * @return the associated CA Editor
	 */
	public final CAFactorialMapChartEditor getCAFactorialMapChartEditor() {

		if (caFactorialMapEditor != null && !caFactorialMapEditor.isDisposed()) {
			return caFactorialMapEditor;
		}

		caFactorialMapEditor = null;
		CA ca = getResult().getFirstParent(CA.class);
		if (ca == null) return null;

		Set<ITXMResultEditor<TXMResult>> editors = SWTEditorsUtils.getEditors(ca);
		if (editors != null) {
			for (ITXMResultEditor<TXMResult> editor : editors) {
				if (editor instanceof CAFactorialMapChartEditor eEditor) {
					caFactorialMapEditor = eEditor;
					break;
				}
				else if (editor instanceof CAEditor eEditor) {
					caFactorialMapEditor = (CAFactorialMapChartEditor) eEditor.getChartEditor();
					break;
				}
			}
		}

		return caFactorialMapEditor;
	}
}
