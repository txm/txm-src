package org.txm.ca.rcp.editors;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.util.Arrays;
import java.util.LinkedHashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYBoxAnnotation;
import org.jfree.chart.annotations.XYLineAnnotation;
import org.jfree.chart.annotations.XYShapeAnnotation;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.plot.XYPlot;
import org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.renderers.CAItemSelectionRenderer;
import org.txm.ca.core.functions.CA;
import org.txm.utils.TableReader;
import org.txm.utils.logger.Log;

/**
 * Simple experiment of CA annotation lines
 * 
 * @author mdecorde
 *
 */
public class CAAnnotationPanel {
	
	public CAAnnotationPanel(Composite styleComposite, CA result) {
			
			Button resetAnnotationsButton = new Button(styleComposite, SWT.PUSH);
			resetAnnotationsButton.setText("Reset annotations");
			resetAnnotationsButton.setToolTipText("Remove all annotations");
			resetAnnotationsButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					
			// TO BE MOVED IN A AnnotationChartRenderer or something
					if (result.getChart() instanceof JFreeChart chart) {
						chart.getXYPlot().clearAnnotations();
					}
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) { }
			});
			
			Button importXYAnnotationsButton = new Button(styleComposite, SWT.PUSH);
			importXYAnnotationsButton.setText("Import XY annotations from an ODS file...");
			importXYAnnotationsButton.setToolTipText("Columns: what x1 y1 x2 y2");
			importXYAnnotationsButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					
			// TO BE MOVED IN A AnnotationChartRenderer or something.
					if (result.getChart() instanceof JFreeChart chart) {
						if (chart.getXYPlot().getRenderer() instanceof CAItemSelectionRenderer renderer) { 
							FileDialog dialog = new FileDialog(e.display.getActiveShell(), SWT.OPEN);
							dialog.setFilterExtensions(new String[] {"*.csv", "*.tsv", "*.ods", "*.xlsx"}); //$NON-NLS-1$
							dialog.setText("Select the annotations ODS file");
							String path = dialog.open();
							if (path == null) return;
							try {
								TableReader reader = new TableReader(new File(path));
								reader.readHeaders();
								String[] headers = reader.getHeaders();
								if (!Arrays.toString(headers).startsWith("what, x1, y1, x2, y2")) { //$NON-NLS-1$
									Log.severe("Wrong XY annotation table header. Waiting for: what, x1, y1, x2, y2");
									return;
								}
								
								while (reader.readRecord()) {
									LinkedHashMap<String, String> line = reader.getRecord();
									if (chart.getPlot() instanceof XYPlot xyPlot) {
										
										if ("line".equals(line.get("what"))) { //$NON-NLS-1$
											xyPlot.addAnnotation(new XYLineAnnotation(Float.parseFloat(line.get("x1")), Float.parseFloat(line.get("y1")), Float.parseFloat(line.get("x2")), Float.parseFloat(line.get("y2")))); //$NON-NLS-1$
										} else if ("arrow".equals(line.get("what"))) { //$NON-NLS-1$
											xyPlot.addAnnotation(new XYLineAnnotation(Float.parseFloat(line.get("x1")), Float.parseFloat(line.get("y1")), Float.parseFloat(line.get("x2")), Float.parseFloat(line.get("y2")))); //$NON-NLS-1$
										} else if ("box".equals(line.get("what"))) { //$NON-NLS-1$
											xyPlot.addAnnotation(new XYBoxAnnotation(Float.parseFloat(line.get("x1")), Float.parseFloat(line.get("y1")), Float.parseFloat(line.get("x2")), Float.parseFloat(line.get("y2")))); //$NON-NLS-1$
										} else if ("ellipse".equals(line.get("what"))) { //$NON-NLS-1$
											Ellipse2D ellipse = new Ellipse2D.Double(Float.parseFloat(line.get("x1")), Float.parseFloat(line.get("y1")), Float.parseFloat(line.get("x2")), Float.parseFloat(line.get("y2"))); //$NON-NLS-1$
											xyPlot.addAnnotation(new XYShapeAnnotation(ellipse, new BasicStroke(1.0f), Color.red));
										} else {
											xyPlot.addAnnotation(new XYTextAnnotation(line.get("what"), Float.parseFloat(line.get("x1")), Float.parseFloat(line.get("y1")))); //$NON-NLS-1$
										}
									}
								}
							}
							catch (Exception e2) {
								e2.printStackTrace();
							}
							try {
								System.out.println("NOT IMPLEMENTED"); //$NON-NLS-1$
							} catch (Exception e1) {
								e1.printStackTrace();
							}
						}
					}
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) { }
			});

			Button importLineAnnotationsButton = new Button(styleComposite, SWT.PUSH);
			importLineAnnotationsButton.setText("Import named line annotations from an ODS file...");
			importLineAnnotationsButton.setToolTipText("Columns: from, to, width, color");
			importLineAnnotationsButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					
					if (result.getChart() instanceof JFreeChart chart) {
						if (chart.getXYPlot().getRenderer() instanceof CAItemSelectionRenderer renderer) { 
							FileDialog dialog = new FileDialog(e.display.getActiveShell(), SWT.OPEN);
							dialog.setFilterExtensions(new String[] {"*.csv", "*.tsv", "*.ods", "*.xlsx"}); //$NON-NLS-1$
							dialog.setText("Select the annotations ODS file");
							String path = dialog.open();
							if (path == null) return;
							try {
								TableReader reader = new TableReader(new File(path));
								reader.readHeaders();
								String[] headers = reader.getHeaders();
								if (!Arrays.toString(headers).startsWith("from, to, width, color")) { //$NON-NLS-1$
									Log.severe("Wrong XY annotations table header waiting for: from, to, width, color");
									return;
								}
								
								BasicStroke stroke = new BasicStroke(1);
								Color color = Color.BLACK;
								while (reader.readRecord()) {
									LinkedHashMap<String, String> line = reader.getRecord();
									if (chart.getPlot() instanceof XYPlot xyPlot) {
										
										xyPlot.addAnnotation(new XYLineAnnotation(Float.parseFloat(line.get("x1")), Float.parseFloat(line.get("y1")), Float.parseFloat(line.get("x2")), Float.parseFloat(line.get("y2")), stroke, color)); //$NON-NLS-1$
									} else {
										
									}
								}
							}
							catch (Exception e2) {
								e2.printStackTrace();
							}
						}
					}
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) { }
			});
	}
}
