// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.ca.rcp.editors;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IPropertyListener;
import org.txm.ca.core.chartsengine.base.CAChartCreator;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.functions.CAColsInfo;
import org.txm.ca.core.functions.CARowsInfo;
import org.txm.ca.core.functions.Eigenvalues;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.editors.ChartEditorInput;
import org.txm.rcp.editors.TXMMultiPageEditor;
import org.txm.rcp.editors.TXMResultEditorInput;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.utils.logger.Log;

/**
 * CA editor with a factorial map chart in the left area and a tabbed area on the right side with eigenvalues, rows and columns information.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class CAEditor extends TXMMultiPageEditor<CA> {


	/**
	 * The CA.
	 */
	private CA ca;


	@Override
	protected void createPages() {

		// Tabs titles
		this.names = Arrays.asList(new String[] { CAUIMessages.factPlan, CACoreMessages.rows, CACoreMessages.columns, CAUIMessages.eigenvaluesBarChat, CAUIMessages.eigenvalues });

		Composite container = this.getContainer();
		final Composite parentContainer = container.getParent();

		parentContainer.setLayout(new FormLayout());// change layout
		final Sash sash = new Sash(parentContainer, SWT.VERTICAL);

		Composite mainEditorComposite = new Composite(parentContainer, SWT.NONE);

		FormData containerData = new FormData();
		containerData.left = new FormAttachment(sash, 0);
		containerData.right = new FormAttachment(100, 0);
		containerData.top = new FormAttachment(0, 0);
		containerData.bottom = new FormAttachment(100, 0);
		container.setLayoutData(containerData);// set new layout data

		final FormData sashData = new FormData();
		sashData.left = new FormAttachment(70, 0);
		sashData.top = new FormAttachment(0, 0);
		sashData.bottom = new FormAttachment(100, 0);
		sash.setLayoutData(sashData);
		sash.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event e) {
				Rectangle sashRect = sash.getBounds();
				Rectangle shellRect = parentContainer.getClientArea();
				int right = shellRect.width - sashRect.width;
				e.x = Math.max(Math.min(e.x, right), 0);
				if (e.x != sashRect.x) {
					sashData.left = new FormAttachment(0, e.x);
					parentContainer.layout();
				}
			}
		});

		FormData CAFactorialMapChartFormData = new FormData();
		CAFactorialMapChartFormData.left = new FormAttachment(0, 0);
		CAFactorialMapChartFormData.right = new FormAttachment(sash, 0);
		CAFactorialMapChartFormData.top = new FormAttachment(0, 0);
		CAFactorialMapChartFormData.bottom = new FormAttachment(100, 0);
		mainEditorComposite.setLayoutData(CAFactorialMapChartFormData);// set new layout data
		mainEditorComposite.setLayout(new FillLayout());


		editors = new ArrayList<>();
		inputs = new ArrayList<>();

		this.ca = ((TXMResultEditorInput<CA>) this.getEditorInput()).getResult();

		// creating the Eigenvalues bar chart if not exists
		Eigenvalues eigenvalues = (Eigenvalues) this.ca.getFirstChild(Eigenvalues.class);
		if (eigenvalues == null) {
			eigenvalues = new Eigenvalues(this.ca);
		}



		// Initialize the editor parts and editor inputs
		this.initCAFactorialMapEditor();
		this.initRowsTableEditor();
		this.initColumnsTableEditor();
		this.initCAEigenvaluesBarChartEditor();
		this.initCAEigenvaluesTableEditor();


		if (editors.size() != inputs.size()) {
			throw new IllegalArgumentException(NLS.bind(TXMUIMessages.theNumberOfEditorsP0AndNumberOfInputsP1MustBeEqual, editors.size(), inputs.size()));
		}

		// put first editor in the left panel and set it as "main" editor
		try {

			final CAFactorialMapChartEditor caFactorialMapEditorPart = (CAFactorialMapChartEditor) editors.get(0);
			this.mainEditor = caFactorialMapEditorPart;

			IEditorSite site = this.createSite(caFactorialMapEditorPart);
			// call init first so that if an exception is thrown, we have created no new widgets
			caFactorialMapEditorPart.init(site, caFactorialMapEditorPart.getEditorInput());

			// Set the parent multi pages editor of the CA factorial map editor to activate it from the child editor
			//			caFactorialMapEditorPart.setParentMultiPagesEditor(this);

			caFactorialMapEditorPart.createPartControl(mainEditorComposite);

			caFactorialMapEditorPart.addPropertyListener(new IPropertyListener() {

				@Override
				public void propertyChanged(Object source, int propertyId) {
					handlePropertyChange(propertyId);
				}
			});

			// add the other editors
			for (int i = 1; i < editors.size(); i++) {
				int editorIndex = this.addPage(editors.get(i), inputs.get(i)); // NOTE: the method MultiPageEditorPart.addPage() internally calls the createPartControl() method of the editors
				this.setPageText(editorIndex, this.names.get(i));
			}

			// caFactorialMapEditorPart.compute(false);
			// caFactorialMapEditorPart.refresh(false);
			eigenvalues.compute();

		}
		catch (Exception e1) {
			Log.printStackTrace(e1);
			Log.severe(NLS.bind(CAUIMessages.errorWhileOpeningCAEditorColonP0, e1));
			return;
		}

		// Selection changed listener to highlight CA chart points from table lines selection
		ISelectionChangedListener selectionChangedListener = new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent e) {
				// Highlight points from table lines
				Table table = ((TableViewer) e.getSource()).getTable();
				String[] selectedLabels = new String[table.getSelectionCount()];
				TableItem[] selection = table.getSelection();

				for (int i = 0; i < selection.length; i++) {
					selectedLabels[i] = selection[i].getText();
				}

				CAFactorialMapChartEditor chartEditor = (CAFactorialMapChartEditor) editors.get(0);
				((CAChartCreator) chartEditor.getResult().getChartCreator()).updateChartCAFactorialMapHighlightPoints(chartEditor.getChart(), (e.getSource() == getRowsInfosEditor().getViewer()),
						selectedLabels);

			}
		};

		// Add selection listener to rows table to link editor with factorial map chart
		this.getRowsInfosEditor().getViewer().addSelectionChangedListener(selectionChangedListener);
		// Add selection listener to cols table to link editor with factorial map chart
		this.getColsInfosEditor().getViewer().addSelectionChangedListener(selectionChangedListener);

		// Selection listeners to change chart items selection order from table sorting
		SelectionListener selectionListener = new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				// Change chart items selection order
				CAFactorialMapChartEditor chartEditor = (CAFactorialMapChartEditor) editors.get(0);
				((CAChartCreator) chartEditor.getResult().getChartCreator()).updateChartCAFactorialMapSetLabelItemsSelectionOrder(chartEditor.getChart(), getRowsInfosEditor().getOrdererLabels(),
						getColsInfosEditor().getOrdererLabels());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		};
		// Add listener to column to link chart items selection order with table sorting order
		for (int i = 0; i < this.getRowsInfosEditor().getViewer().getTable().getColumnCount(); i++) {
			this.getRowsInfosEditor().getViewer().getTable().getColumn(i).addSelectionListener(selectionListener);
		}
		// Add listener to column to link chart items selection order with table sorting order
		for (int i = 0; i < this.getColsInfosEditor().getViewer().getTable().getColumnCount(); i++) {
			this.getColsInfosEditor().getViewer().getTable().getColumn(i).addSelectionListener(selectionListener);
		}



		// Sets the parent multi pages editor of the singular values barplot to activate it from the child editor
		//		((ChartEditor) editors.get(3)).setParentMultiPagesEditor(this);


		// set the eigenvalues chart visible by default
		if (LAST_EDITOR_USED == 0) {
			LAST_EDITOR_USED = 3;
		}
		this.setActivePage(LAST_EDITOR_USED);

		this.setPartName(this.getEditorInput().getName());
	}

	/**
	 * Initializes CA factorial map editor.
	 */
	public void initCAFactorialMapEditor() {
		ChartEditor caFactorialMapEditorPart = new CAFactorialMapChartEditor(new ChartEditorInput<CA>(this.ca));
		editors.add(caFactorialMapEditorPart);
		inputs.add(caFactorialMapEditorPart.getEditorInput());
	}

	/**
	 * Initializes CA Eigenvalues bar chart editor.
	 */
	public void initCAEigenvaluesBarChartEditor() {
		Eigenvalues eigenvalues = (Eigenvalues) ca.getFirstChild(Eigenvalues.class);
		EigenvaluesChartEditor editor = new EigenvaluesChartEditor(new ChartEditorInput<Eigenvalues>(eigenvalues));
		editors.add(editor);
		inputs.add(editor.getEditorInput());
	}

	/**
	 * Initializes CA Eigenvalues table editor.
	 */
	public void initCAEigenvaluesTableEditor() {
		EigenvaluesTableEditor editor = new EigenvaluesTableEditor(ca);
		editors.add(editor);
		inputs.add(editor.getEditorInput());
	}

	/**
	 * Initializes rows data table editor.
	 */
	public void initRowsTableEditor() {

		CARowsInfo infos = ca.getFirstChild(CARowsInfo.class);
		if (infos == null) {
			infos = new CARowsInfo(ca);
		}

		ColsRowsInfosEditor editor = new ColsRowsInfosEditor(infos, CAUIMessages.rowsInfos);

		editors.add(editor);
		inputs.add(editor.getEditorInput());

	}

	/**
	 * Initializes columns data table editor.
	 */
	public void initColumnsTableEditor() {

		CAColsInfo infos = ca.getFirstChild(CAColsInfo.class);
		if (infos == null) {
			infos = new CAColsInfo(ca);
		}

		ColsRowsInfosEditor editor = new ColsRowsInfosEditor(infos, CAUIMessages.colsInfos);

		editors.add(editor);
		inputs.add(editor.getEditorInput());
	}



	/**
	 * Gets the CA.
	 *
	 * @return the CA
	 */
	public CA getCA() {
		return this.ca;
	}

	/**
	 * Gets the factorial map chart editor.
	 * 
	 * @return the factorial map chart editor
	 */
	public ChartEditor getChartEditor() {
		return (ChartEditor) this.editors.get(0);
	}


	/**
	 * Gets the rows information editor.
	 * 
	 * @return the rows information editor
	 */
	public ColsRowsInfosEditor getRowsInfosEditor() {
		return (ColsRowsInfosEditor) this.editors.get(1);
	}

	/**
	 * Gets the columns information editor.
	 * 
	 * @return the columns information editor
	 */
	public ColsRowsInfosEditor getColsInfosEditor() {
		return (ColsRowsInfosEditor) this.editors.get(2);
	}

	/**
	 * Gets the Eigenvalues chart editor.
	 * 
	 * @return the Eigenvalues chart editor
	 */
	public EigenvaluesChartEditor getEigenvaluesBarChartEditor() {
		return (EigenvaluesChartEditor) this.editors.get(3);
	}

	/**
	 * Gets the Eigenvalues chart editor.
	 * 
	 * @return the Eigenvalues chart editor
	 */
	public EigenvaluesTableEditor getEigenvaluesTableEditor() {
		return (EigenvaluesTableEditor) this.editors.get(4);
	}

	@Override
	public void setFocus() {
		super.setFocus();
		// Give the focus to the CA factorial map ChartEditor instead of the parent MultiPageEditorPart
		// editors.get(0).setFocus();
		// FIXME: the problem here is that we need to give the focus to the singular values chart editor when clicking in it
		// System.err.println("CaEditor.setFocus(): " + this.getActivePage());
		// editors.get(4).setFocus();
	}

	// @Override
	public CA getResult() {
		return ca;
	}

	//
	//
	// @Override
	// public void close() {
	// close(false);
	// }
	//
	// @Override
	// public void close(final boolean deleteResult) {
	// final EditorPart self = this;
	// this.getSite().getShell().getDisplay().syncExec(new Runnable() {
	// @Override
	// public void run() {
	// getSite().getPage().closeEditor(self, false);
	// if(deleteResult) {
	// try {
	// getResult().delete();
	// }
	// catch(Exception e) {
	//
	// }
	// }
	// }
	// });
	// }
	//
	// @Override
	// public void setLocked(boolean locked) {
	// }
	//
	// @Override
	// public JobHandler compute(boolean update) {
	// // TODO Auto-generated method stub
	// return null;
	// }
	//
	// @Override
	// public void refresh(boolean update) throws Exception {
	// // TODO Auto-generated method stub
	//
	// }
}
