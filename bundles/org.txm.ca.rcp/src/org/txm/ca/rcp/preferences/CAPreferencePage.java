// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.ca.rcp.preferences;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbench;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.ca.core.preferences.CAPreferences;
import org.txm.ca.rcp.adapters.CAAdapterFactory;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.rcp.swt.widget.preferences.ComboFieldEditor;
import org.txm.rcp.swt.widget.preferences.IntegerFieldEditor;
import org.txm.rcp.swt.widget.preferences.StringFieldEditor;

/**
 * CA preferences page.
 *
 * @author mdecorde
 * @author sjacquot
 *
 */
public class CAPreferencePage extends TXMPreferencePage {

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		Group ltGroup = new Group(getFieldEditorParent(), SWT.NONE);
		ltGroup.setText(CAUIMessages.lexicalTableCreatedWhenCalledFromAPartition);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData.horizontalSpan = 2;
		gridData.verticalIndent = 10;
		ltGroup.setLayoutData(gridData);

		IntegerFieldEditor fMin = new IntegerFieldEditor(CAPreferences.F_MIN, TXMCoreMessages.common_fMin, ltGroup);
		fMin.setToolTipText(CAUIMessages.minimumFrequencyThresholdCommaAllValuesBelowFminAreIgnored);
		this.addField(fMin);
		
		IntegerFieldEditor vMax = new IntegerFieldEditor(CAPreferences.V_MAX, TXMCoreMessages.common_vMax, ltGroup);
		vMax.setToolTipText(CAUIMessages.listTruncationThresholdCommaOnlyTheMostFrequentFirstVmaxLinesAreRetained);
		this.addField(vMax);
		
		((GridLayout)ltGroup.getLayout()).marginWidth = 3;
		
		StringFieldEditor field = new StringFieldEditor(CAPreferences.N_FACTORS_MEMORIZED, CAUIMessages.numberOfSavedCoordinates, this.getFieldEditorParent());
		field.setToolTipText(CAUIMessages.numbeOfFactorsDisplayedEtc);
		this.addField(field);
		
		Group formatsGroup = new Group(getFieldEditorParent(), SWT.NONE);
		formatsGroup.setText(CAUIMessages.columnFormats);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData.horizontalSpan = 2;
		gridData.verticalIndent = 10;
		formatsGroup.setLayoutData(gridData);
		
		field = new StringFieldEditor(CAPreferences.QUALITY_DISPLAY_FORMAT, CACoreMessages.quality, formatsGroup);
		field.setToolTipText(CAUIMessages.qualityColumnFormatEtc);
		this.addField(field);
		
		field = new StringFieldEditor(CAPreferences.CONTRIB_DISPLAY_FORMAT, CACoreMessages.contribution, formatsGroup);
		field.setToolTipText(CAUIMessages.contributionColumnFormatEtc);
		this.addField(field);
		
		field = new StringFieldEditor(CAPreferences.MASS_DISPLAY_FORMAT, CACoreMessages.mass, formatsGroup);
		field.setToolTipText(CAUIMessages.massColumnFormatEtc);
		this.addField(field);
		
		field = new StringFieldEditor(CAPreferences.DIST_DISPLAY_FORMAT, CACoreMessages.distance, formatsGroup);
		field.setToolTipText(CAUIMessages.distanceColumnFormatEtc);
		this.addField(field);
		
		field = new StringFieldEditor(CAPreferences.COS2_DISPLAY_FORMAT, CACoreMessages.cos2, formatsGroup);
		field.setToolTipText(CAUIMessages.cosColumnFormatEtc);
		this.addField(field);
		
		field = new StringFieldEditor(CAPreferences.COORD_DISPLAY_FORMAT, CACoreMessages.coordinates, formatsGroup);
		field.setToolTipText(CAUIMessages.coordColumnFormatEtc);
		this.addField(field);

		((GridLayout)formatsGroup.getLayout()).marginWidth = 3;
		
		// enable interpretation help
		Group interpretationsGroup = new Group(getFieldEditorParent(), SWT.NONE);
		interpretationsGroup.setText(CAUIMessages.tools);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData.horizontalSpan = 2;
		gridData.verticalIndent = 10;
		interpretationsGroup.setLayoutData(gridData);
		this.addField(new BooleanFieldEditor(CAPreferences.USER_INTERPRETATION_HELP, CAUIMessages.showTheInterpretationHelpTools, BooleanFieldEditor.SEPARATE_LABEL, interpretationsGroup));
		this.addField(new StringFieldEditor(CAPreferences.DEFAULT_STYLINGSHEET, CAUIMessages.defaultStylingSheetName, interpretationsGroup));
		this.addField(new BooleanFieldEditor(CAPreferences.FILTER_INFOS_SUM, CAUIMessages.showTheContributionSumInTheFilterPanel, BooleanFieldEditor.SEPARATE_LABEL, interpretationsGroup));
		this.addField(new BooleanFieldEditor(CAPreferences.FILTER_INFOS_N_FILTERED, CAUIMessages.showTheNumberOfFilteredItemsInTheFilterPanel, BooleanFieldEditor.SEPARATE_LABEL, interpretationsGroup));
		this.addField(new BooleanFieldEditor(CAPreferences.FILTER_INFOS_AHC_CLASSES, CAUIMessages.showClassesInTheFilterPanel, BooleanFieldEditor.SEPARATE_LABEL, interpretationsGroup));
		
		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
			
			String[][] values = {
					{"Q=1 -> deactivate quality filter", "1"},
					{"Q=0.5", "0.5"},
					{"80% of qualities", "80%"},
					{"20% of qualities", "20%"},
					{"The minimum quality of the 80% contributions", "minOfCont"},
					{"The minimum quality of the points", "min"},
					{"The mean value of the qualities", "mean"},
					{"The maximum quality of the points", "max"},
				};
			this.addField(new ComboFieldEditor(CAPreferences.FILTER_DEFAULT_QUALITY_MODE, "Default filter quality value mode", values, interpretationsGroup));
		}
		this.addField(new BooleanFieldEditor(CAPreferences.OPEN_INFOS, CAUIMessages.showTheInformationPanelsWhenOpeningTheCAEditor, BooleanFieldEditor.SEPARATE_LABEL, interpretationsGroup));
		//this.addField(new BooleanFieldEditor(CAPreferences.OPEN_INFOS_IN_BOX, "(Experimental) Open the CA result in a box area along side its informations tables", BooleanFieldEditor.SEPARATE_LABEL, interpretationsGroup));
		
		((GridLayout)interpretationsGroup.getLayout()).marginWidth = 3;
		
		// Charts rendering
		Composite chartsTab = SWTChartsComponentsProvider.createChartsRenderingPreferencesTabFolderComposite(this.getFieldEditorParent());

		Group displayGroup = new Group(chartsTab, SWT.NONE);
		displayGroup.setText(TXMUIMessages.display);
		GridData booleansGroupGridData = new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1);
		booleansGroupGridData.horizontalSpan = 2;
		displayGroup.setLayoutData(booleansGroupGridData);
		
		this.addField(new BooleanFieldEditor(CAPreferences.SHOW_INDIVIDUALS, CACoreMessages.columnPoints, displayGroup));
		this.addField(new BooleanFieldEditor(CAPreferences.SHOW_VARIABLES, CACoreMessages.rowsPoints, displayGroup));
		this.addField(new BooleanFieldEditor(CAPreferences.SHOW_POINT_SHAPES, CACoreMessages.pointShapes, displayGroup));
		this.addField(new BooleanFieldEditor(CAPreferences.SHOW_POINT_LABELS, CACoreMessages.pointLabels, displayGroup));

		// other shared preferences
		SWTChartsComponentsProvider.createChartsRenderingPreferencesFields(this, chartsTab);
	}

	//FIXME: SJ: tabbed preference tests
	//	@Override
	//	public void createFieldEditors() {
	//
	//
	//		// FIXME: SJ: tabbed preferences page test
	//
	//		TabFolder tabFolder = new TabFolder(this.getFieldEditorParent(), SWT.TOP);
	//		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
	//
	//		TabItem tabItem = new TabItem(tabFolder, SWT.NULL);
	//		tabItem.setText("Table lexicale");
	//		Composite composite1 = new Composite(tabFolder, SWT.NULL);
	//		tabItem.setControl(composite1);
	//		fmin = new IntegerFieldEditor(FMIN, Messages.LexicalTablePreferencePage_1, composite1);
	//		addField(fmin);
	//		vmax = new IntegerFieldEditor(VMAX, Messages.LexicalTableEditor_8, composite1);
	//		addField(vmax);
	//
	//
	//		TabItem tabItem2 = new TabItem(tabFolder, SWT.NULL);
	//		tabItem2.setText("Calcul");
	//		Composite composite2 = new Composite(tabFolder, SWT.NULL);
	//		tabItem2.setControl(composite2);
	//		qualityDisplay = new StringFieldEditor(QUALITYDISPLAY,
	//				Messages.CAPreferencePage_0, composite2);
	//		addField(qualityDisplay);
	//
	//		contribDisplay = new StringFieldEditor(CONTRIBDISPLAY,
	//				Messages.CAPreferencePage_1, composite2);
	//		addField(contribDisplay);
	//
	//		massDisplay = new StringFieldEditor(MASSDISPLAY,
	//				Messages.CAPreferencePage_5, composite2);
	//		addField(massDisplay);
	//
	//		distDisplay = new StringFieldEditor(DISTDISPLAY,
	//				Messages.CAPreferencePage_6, composite2);
	//		addField(distDisplay);
	//
	//		cos2Display = new StringFieldEditor(COS2DISPLAY,
	//				Messages.CAPreferencePage_7, composite2);
	//		addField(cos2Display);
	//
	//		coordDisplay = new StringFieldEditor(COORDDISPLAY,
	//				Messages.CAPreferencePage_8, composite2);
	//		addField(coordDisplay);
	//
	//
	//		// Charts rendering
	//		TabItem tabItem3 = new TabItem(tabFolder, SWT.NULL);
	//		tabItem3.setText("Graphiques");
	//		Composite composite3 = new Composite(tabFolder, SWT.NULL);
	//		tabItem3.setControl(composite3);
	//
	//		// FIXME : these 2 preferences should be renamed and moved to charts engine preference level (associated messages too)
	//		showIndividuals = new BooleanFieldEditor(SHOWINDIVIDUALS, Messages.CAPreferencePage_3, composite3);
	//		addField(showIndividuals);
	//
	//		showVariables = new BooleanFieldEditor(SHOWVARIABLES, Messages.CAPreferencePage_4, composite3);
	//		addField(showVariables);
	//
	//	}



	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(CAPreferences.getInstance().getPreferencesNodeQualifier()));
		//this.setDescription(Messages.CorrespondanceAnalysisEditorInput_10);
		//this.setTitle(CAUIMessages.CorrespondanceAnalysisEditorInput_10);
		this.setImageDescriptor(CAAdapterFactory.ICON);
	}


}
