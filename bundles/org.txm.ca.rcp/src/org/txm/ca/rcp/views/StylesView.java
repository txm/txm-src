package org.txm.ca.rcp.views;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ComboBoxViewerCellEditor;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.txm.ca.rcp.editors.styling.StyleEditSupport;
import org.txm.ca.rcp.editors.styling.StylingSheetTable;
import org.txm.chartsengine.core.styling.Style;
import org.txm.chartsengine.core.styling.catalogs.StylingInstructionsCatalog;
import org.txm.chartsengine.core.styling.definitions.StylingInstructionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.ConstantFunctionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.FunctionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.TransformationFunctionDefinition;
import org.txm.chartsengine.core.styling.instructions.StylingInstruction;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.handlers.results.DeleteObject;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.provider.SimpleLabelProvider;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Subcorpus;

public class StylesView extends ViewPart {

	static public String ID = StylesView.class.getName();

	public static HashSet<Style> styles = new HashSet<>();

	//	public static final String[] dummyStyleTypes = new String[]{"Style pré-défini", "Transparence", "Couleur", "Forme", "Taille", "Police"};
	//	public static final String[] dummyStyleValues = new String[]{"Mise en évidence1", "Cercle", "Disque", "Mauve", "Rouge", "Up Triangle", "Proportionel à Cont1", "Proportionel à Q12", "x 3", "Vert pomme d'api", "Arial", "Carré"};
	static String EMPTY = "";

	public static StylingInstructionsCatalog styleCatalog = new StylingInstructionsCatalog();
	static {
		Style me1 = new Style("Mise en évidence1");
		me1.addInstruction(new StylingInstruction<Object>(styleCatalog, StylingInstruction.ID_COLOR, StylingInstructionsCatalog.BLUE));
		me1.addInstruction(new StylingInstruction<Object>(styleCatalog, StylingInstruction.ID_SIZE, StylingInstructionsCatalog.X2));
		styles.add(me1);
	}

	/** The tree viewer. */
	public TreeViewer treeViewer;

	public ComboViewer combo;

	public static StylesView getInstance() {

		if (!PlatformUI.isWorkbenchRunning()) return null;

		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window == null) return null;
		IWorkbenchPage page = window.getActivePage();
		if (page == null) return null;

		return (StylesView) page.findView(StylesView.ID);
	}

	public TreeViewer getTreeViewer() {

		return treeViewer;
	}


	/**
	 * Reload.
	 */
	public static void reload() {
		// System.out.println("Reload corpora view");
		StylesView corporaView = getInstance();
		if (corporaView != null) {
			corporaView._reload();
		}
	}

	/**
	 * _reload.
	 */
	public void _reload() {

		if (treeViewer.getContentProvider() == null) {

			treeViewer.setContentProvider(new ITreeContentProvider() {

				@Override
				public Object[] getElements(Object inputElement) {
					if (currentStyle == null) return new Object[0];
					return currentStyle.getStylingInstructions().toArray();
				}

				@Override
				public Object[] getChildren(Object parentElement) {

					if (parentElement instanceof Style sd) {
						return sd.getStylingInstructions().toArray();
					}
					return new Object[0];
				}

				@Override
				public Object getParent(Object element) {

					return null;
				}

				@Override
				public boolean hasChildren(Object element) {

					if (element instanceof Style sd) {
						return sd.getStylingInstructions().size() > 0;
					}
					return false;
				}

			});
			ColumnViewerToolTipSupport.enableFor(treeViewer);
		}

		currentStyle = null;

		combo.setInput(styles);
		combo.refresh();
		combo.getCombo().layout();
		for (Style style : styles) {
			combo.setSelection(new StructuredSelection(style));
			//combo.getCombo().setText(style.getName());
			currentNameText.setText(style.getName());
			break;
		}
	}

	private Style currentStyle = null;

	public void refreshStyle() {
		if (treeViewer.getInput() == null || !(treeViewer.getInput().equals(currentStyle))) {


			if (currentStyle != null) {
				currentNameText.setEnabled(true);
				currentNameText.setText(currentStyle.getName());
				treeViewer.getTree().setEnabled(true);
			}
			else {
				currentNameText.setText(EMPTY);
				currentNameText.setEnabled(false);
				treeViewer.getTree().setEnabled(false);
			}
			treeViewer.setInput(currentStyle);
			treeViewer.refresh();
		}
	}

	private Text currentNameText;

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 *
	 * @param parent the parent
	 */
	@Override
	public void createPartControl(Composite parent) {

		// new delete copy
		parent.setLayout(new GridLayout(1, false));

		Composite buttons = new Composite(parent, SWT.BORDER);
		RowLayout rlayout = new RowLayout();
		rlayout.center = true;
		rlayout.spacing = 1;
		buttons.setLayout(rlayout);
		//		buttons.getLayout().numColumns = 20;
		buttons.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));

		new Label(buttons, SWT.NONE).setText("Styles");

		combo = new ComboViewer(buttons, SWT.SINGLE | SWT.READ_ONLY);
		RowLayout gdata = new RowLayout();
		//combo.getCombo().setLayoutData(gdata);
		combo.setContentProvider(new ArrayContentProvider() {

			/**
			 * Returns the elements in the input, which must be either an array or a
			 * <code>Collection</code>.
			 */
			@Override
			public Object[] getElements(Object inputElement) {
				return styles.toArray();
			}
		});
		combo.setLabelProvider(new SimpleLabelProvider());
		combo.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {

				currentStyle = (Style) combo.getStructuredSelection().getFirstElement();
				refreshStyle();
			}
		});

		Button newButton = new Button(buttons, SWT.PUSH);
		newButton.setText("New");
		newButton.setToolTipText("Create a new style");
		newButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				String name = "New style ";
				int i = 1;

				Style sd = new Style(name + i);
				while (styles.contains(sd)) {
					i++;
					sd.setName(name + i);
				}

				styles.add(sd);
				combo.refresh(true);
				combo.getCombo().layout();
				combo.setSelection(new StructuredSelection(sd));
				refreshStyle();
			}
		});

		Button copyStyleButton = new Button(buttons, SWT.PUSH);
		copyStyleButton.setText("Copy");
		copyStyleButton.setToolTipText("Create a copy of the selected style");
		copyStyleButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (currentStyle != null) {

					int i = 1;
					Style sd = currentStyle.clone();
					sd.setName("Copy " + i + " of " + currentStyle.getName());
					while (styles.contains(sd)) {
						i++;
						sd.setName("Copy " + i + " of " + currentStyle.getName());
					}

					styles.add(sd);
					combo.refresh(true);
					combo.getCombo().layout();
					combo.setSelection(new StructuredSelection(sd));
					refreshStyle();
				}
			}
		});

		Button deleteButton = new Button(buttons, SWT.PUSH);
		deleteButton.setText("Delete");
		deleteButton.setToolTipText("Delete selected style");
		deleteButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (currentStyle != null) {

					if (MessageDialog.openQuestion(e.display.getActiveShell(), "Delete " + currentStyle.getName() + "?", "Delete " + currentStyle.getName() + "?")) {
						styles.remove(currentStyle);
						combo.refresh(true);
						combo.getCombo().layout();
						combo.setSelection(new StructuredSelection());
						for (Style style : styles) {
							currentStyle = style;
							combo.setSelection(new StructuredSelection(currentStyle));
							break;
						}
					}
				}
			}
		});

		new Label(buttons, SWT.NONE).setText("Current style name");

		currentNameText = new Text(buttons, SWT.BORDER);
		//		gdata = new GridData(GridData.FILL, GridData.FILL, false, false);
		//		gdata.widthHint = gdata.minimumWidth = 200;
		//		currentNameText.setLayoutData(gdata);
		currentNameText.setToolTipText("Name of the current style");
		currentNameText.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {

				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					if (currentStyle == null) return;
					String newName = currentNameText.getText().trim();
					if (newName.length() == 0) return;
					for (Style sd : styles) {
						if (newName.equals(sd.getName()) && sd != currentStyle) {
							currentNameText.setBackground(new org.eclipse.swt.graphics.Color(255, 0, 0));
							return;
						}
					}
					currentNameText.setBackground(currentNameText.getParent().getBackground());
					currentStyle.setName(newName);
					combo.refresh();
					combo.getCombo().layout();
				}
				else {
					currentNameText.setBackground(new org.eclipse.swt.graphics.Color(200, 200, 200));
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		new Label(buttons, SWT.NONE).setText("Instructions");

		Button newInstructionButton = new Button(buttons, SWT.PUSH);
		newInstructionButton.setText("New");
		newInstructionButton.setToolTipText("Add an instruction to the current style");
		newInstructionButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				if (currentStyle != null) {
					StylingInstruction<?> newSd = new StylingInstruction(styleCatalog, StylingInstruction.ID_COLOR, StylingInstructionsCatalog.BLUE);
					currentStyle.addInstruction(newSd);
					treeViewer.refresh(true);
				}
			}
		});

		Button removeButton = new Button(buttons, SWT.PUSH);
		removeButton.setText("Delete");
		removeButton.setToolTipText("Delete the selected instruction");
		removeButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				Object o = treeViewer.getStructuredSelection().getFirstElement();
				if (o instanceof StylingInstruction<?> sd) {
					currentStyle.removeInstruction(sd);

					treeViewer.refresh();
				}
			}
		});

		treeViewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
		treeViewer.getTree().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		treeViewer.getTree().setHeaderVisible(true);
		treeViewer.getTree().setLinesVisible(true);

		PlatformUI.getWorkbench().getHelpSystem().setHelp(treeViewer.getControl(), TXMUIMessages.corpus);
		getSite().setSelectionProvider(treeViewer);

		_reload();

		TreeViewerColumn stylecolumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		stylecolumn.getColumn().setText("Type");
		stylecolumn.getColumn().setWidth(300);
		stylecolumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof StylingInstruction<?> sd) {
					return sd.getInstruction().getLabel();
				}
				return EMPTY;
			}
		});

		stylecolumn.setEditingSupport(new StyleEditSupport(treeViewer, styleCatalog.getInstructions().toArray()) {

			@Override
			protected void saveObjectValue(Object o, Object v) {

				if (o instanceof StylingInstruction<?> sd) {
					if (v instanceof StylingInstructionDefinition<?> s) {
						sd.setInstructionDefinition(s.getID());
						sd.setFunction(null);
					}
				}
			}

			@Override
			protected String getObjectValue(Object o) {
				if (o instanceof StylingInstruction<?> sd) {
					return sd.getInstruction().getLabel();
				}
				else {
					return o.toString();
				}
			}

			@Override
			protected boolean canEdit(Object element) {
				if (element instanceof StylingInstruction<?> sd) {
					return true;
				}
				return false;
			}
		});

		TreeViewerColumn styleValuecolumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		styleValuecolumn.getColumn().setText("Valeur");
		styleValuecolumn.getColumn().setWidth(300);
		styleValuecolumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof StylingInstruction<?> sd) {
					if (sd.getFunction() == null) return "<no value set>";
					return sd.getFunction().getLabel();
				}
				return element.toString();
			}
		});
		styleValuecolumn.setEditingSupport(new StyleEditSupport(treeViewer, styleCatalog.getFunctions().values().toArray()) {

			@Override
			protected void saveObjectValue(Object o, Object v) {
				if (o instanceof StylingInstruction<?> sd) {
					if (v instanceof FunctionDefinition f) {
						sd.setFunction(f.getID());
					}
				}
			}

			@Override
			protected String getObjectValue(Object o) {

				//				if (o instanceof StyleDefinition sd) {
				//					if (sd.parent == null) return EMPTY;
				//					return sd.value;
				//				}
				return o.toString();
			}

			@Override
			protected boolean canEdit(Object element) {

				if (element instanceof StylingInstruction<?> sd) {
					return true;
				}
				return false;
			}

			@Override
			protected CellEditor getCellEditor(Object element) {

				CellEditor ce = super.getCellEditor(element);
				if (ce != null && ce instanceof ComboBoxViewerCellEditor cbvce) {
					if (element instanceof StylingInstruction<?> s) {
						ArrayList<FunctionDefinition> constants = new ArrayList<>();
						for (TransformationFunctionDefinition f : styleCatalog.getFunctionsFor((StylingInstructionDefinition<?>) s.getInstruction())) {
							if (f instanceof ConstantFunctionDefinition) {
								constants.add(f);
							}
						}
						cbvce.setInput(constants);
					}
				}
				return ce;
			}
		});

		TreeViewerColumn separatorColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		separatorColumn.getColumn().setText("");
		separatorColumn.getColumn().setWidth(1);
		separatorColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				return EMPTY;
			}
		});
		treeViewer.setColumnProperties(new String[] { "name", "type", "value", "separator" }); // mandatory to enable edit mode -> used to identify columns //$NON-NLS-1$

		treeViewer.setSorter(new ViewerSorter() {

			@Override
			public int category(Object element) {
				if (element instanceof Subcorpus) return 2; // must do it before the Corpus test
				else if (element instanceof CQPCorpus) return 0;
				else if (element instanceof Partition) return 1;
				else return 3;
			}

			@Override
			public int compare(Viewer viewer, Object e1, Object e2) {


				return super.compare(viewer, e1, e2);

			}
		});

		treeViewer.getTree().addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {

				boolean expert = TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER);

				if (expert && (char) e.keyCode == 'c' & ((e.stateMask & SWT.CTRL) != 0)) {
					Object o = treeViewer.getStructuredSelection().getFirstElement();
					System.out.println("copy " + o);
				}
				else if (expert && (char) e.keyCode == 'x' & ((e.stateMask & SWT.CTRL) != 0)) {
					Object o = treeViewer.getStructuredSelection().getFirstElement();
					System.out.println("Cut " + o);
				}
				else if (expert && (char) e.keyCode == 'v' & ((e.stateMask & SWT.CTRL) != 0)) {
					Object o = treeViewer.getStructuredSelection().getFirstElement();
					System.out.println("Paste " + o);
				}
				else if (e.keyCode == SWT.DEL) {
					final ISelection sel = treeViewer.getSelection();
					if (sel instanceof IStructuredSelection) {

						if (!DeleteObject.askContinueToDelete((IStructuredSelection) sel)) return;

						JobHandler job = new JobHandler(TXMUIMessages.deleting, true) {

							@Override
							protected IStatus _run(SubMonitor monitor) {
								DeleteObject.delete(sel);
								this.syncExec(new Runnable() {

									@Override
									public void run() {
										System.out.println("refresh");
									}
								});
								return Status.OK_STATUS;
							}
						};
						job.schedule();
						try {
							job.join();
						}
						catch (InterruptedException ex) {
							ex.printStackTrace();
						}
					}
				}
			}
		});

		MenuManager menuManager = new MenuManager();
		Menu menu = menuManager.createContextMenu(treeViewer.getTree());

		// Set the MenuManager
		treeViewer.getTree().setMenu(menu);
		getSite().registerContextMenu(menuManager, treeViewer);
		getSite().setSelectionProvider(treeViewer);

//		IContextService contextService = getSite().getService(IContextService.class);
//		contextService.activateContext(ID);

		treeViewer.refresh();
		treeViewer.getTree().pack();

		combo.setInput(styles);
	}

	@Override
	public void setFocus() {

		treeViewer.getControl().setFocus();
	}

	/**
	 * Gets the first selected object in the tree.
	 * 
	 * @return
	 */
	public static Object getFirstSelectedObject() {

		StylesView corporaView = getInstance();
		if (corporaView != null) {
			try {
				ISelection selection = corporaView.treeViewer.getSelection();
				if (selection != null & selection instanceof IStructuredSelection) {
					return ((IStructuredSelection) selection).getFirstElement();
				}
			}
			catch (SWTException e) {
			}
		}
		return null;
	}

	/**
	 * Gets the selected objects in the tree.
	 * 
	 * @return
	 */
	public static List<?> getSelectedObjects() {

		StylesView corporaView = getInstance();
		if (corporaView != null) {
			try {
				ISelection selection = corporaView.treeViewer.getSelection();
				if (selection != null & selection instanceof IStructuredSelection) {
					return ((IStructuredSelection) selection).toList();
				}
			}
			catch (SWTException e) {
			}
		}
		return new ArrayList<Object>();
	}

	public static IStructuredSelection getSelection() {
		StylesView corporaView = getInstance();
		if (corporaView != null) {
			return corporaView.treeViewer.getStructuredSelection();
		}
		return new StructuredSelection();
	}

	/**
	 * Selects some items.
	 * 
	 * @param arrayList
	 */
	public static void select(List<?> arrayList) {

		if (!PlatformUI.isWorkbenchRunning()) return;

		StylesView corporaView = openView();
		if (corporaView != null) {
			StructuredSelection selection = new StructuredSelection(arrayList);
			corporaView.getTreeViewer().setSelection(selection, true);
			//corporaView.getTreeViewer().reveal(arrayList); // to be sure
		}
	}

	/**
	 * Selects some items.
	 * 
	 * @param arrayList
	 */
	public static void select(Object o) {

		select(Arrays.asList(o));
	}

	/**
	 * Opens the Corpora view if not already opened.
	 * 
	 * @return the corpora view if opened
	 */
	public static StylesView openView() {
		// open Unit View if not opened
		try {
			if (!PlatformUI.isWorkbenchRunning()) return null;

			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			StylesView view = (StylesView) page.findView(StylesView.ID);
			if (view == null) {
				view = (StylesView) page.showView(StylesView.ID);
			}
			if (view == null) {
				System.out.println("Errorview not found: " + StylesView.ID); //$NON-NLS-1$
			}
			else {
				view.getSite().getPage().activate(view);
			}
			return view;
		}
		catch (PartInitException e) {
			e.printStackTrace();
		}
		return null;
	}


	/**
	 * Refreshes the specified result node in the tree view.
	 * 
	 * @param result
	 */
	public static void refresh() {

		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {

				StylesView corporaView = getInstance();
				if (corporaView == null) return;
				corporaView.combo.refresh();
				corporaView.combo.getCombo().layout();
			}
		});
	}

	/**
	 * Refreshes the specified result node in the tree view.
	 * 
	 * @param result
	 */
	public static void refreshObject(final Object result) {

		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {

				StylesView corporaView = getInstance();
				if (corporaView == null) return;

				corporaView.treeViewer.refresh();

				try {
					StylesView.expand(result);
				}
				catch (NullPointerException e) {
				}
			}
		});
	}

	public static void expand(Object obj) {

		final StylesView corporaView = getInstance();
		if (corporaView == null) return;
		if (obj != null) {
			corporaView.treeViewer.expandToLevel(obj, 1);
		}
	}

	/**
	 * 
	 * @param obj
	 */
	public static void reveal(Object obj) {

		final StylesView corporaView = openView();
		if (corporaView == null) return;

		if (obj != null && corporaView != null) {
			corporaView.treeViewer.reveal(obj);
		}
	}

	public static void selectStyle(Style newStyle) {
		final StylesView corporaView = openView();
		if (corporaView == null) return;
		corporaView.combo.setSelection(new StructuredSelection(newStyle));
	}
}
