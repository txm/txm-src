// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.ca.rcp.handlers;

import java.util.ArrayList;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.ca.core.functions.CA;
import org.txm.ca.rcp.editors.CAFactorialMapChartEditor;
import org.txm.ca.rcp.editors.CALinkedEditor;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.utils.JobHandler;
import org.txm.utils.logger.Log;

/**
 * Mirror the current X Axis
 * 
 * @author mdecorde
 * 
 */
public class MirrorXAxis extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object editor = HandlerUtil.getActiveEditor(event);
		
		if (editor instanceof CALinkedEditor<?> e) {
			editor = e.getCAFactorialMapChartEditor();
		}

		if (editor != null && editor instanceof CAFactorialMapChartEditor crEditor) {
			return mirrorAxis(crEditor.getResult().getFirstDimension(), crEditor);
		}
		return null;
	}
	
	public static JobHandler mirrorAxis(int dim, CAFactorialMapChartEditor crEditor) {
		CA ca = crEditor.getResult();
		ArrayList<Integer> mirrored = new ArrayList<>(ca.getMirroredDimensions());
		Log.info(NLS.bind(CAUIMessages.invertTheP0Axis, dim));
		if (mirrored.contains(dim)) {
			mirrored.remove(mirrored.indexOf(dim));
		} else {
			mirrored.add(dim);
		}
		crEditor.setMirroredAxis(mirrored);
		return crEditor.compute(true);
	}
}
