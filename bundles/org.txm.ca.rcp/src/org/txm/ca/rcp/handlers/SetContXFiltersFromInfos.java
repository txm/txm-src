// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.ca.rcp.handlers;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.ca.core.chartsengine.styling.CAStylerCatalog;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.functions.CAInfos;
import org.txm.ca.core.functions.CARowsInfo;
import org.txm.ca.rcp.editors.ColsRowsInfosEditor;
import org.txm.ca.rcp.editors.filtering.CAFilterPanel;
import org.txm.rcp.handlers.BaseAbstractHandler;

/**
 * Set a row or column as supplmentary
 * 
 * @author mdecorde
 * 
 */
public class SetContXFiltersFromInfos extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		final ISelection selection = HandlerUtil.getCurrentSelection(event);
		final IEditorPart editor = HandlerUtil.getActiveEditor(event);

		if (editor instanceof ColsRowsInfosEditor crEditor && selection instanceof StructuredSelection sSelection && !sSelection.isEmpty()) {
			return setContFromSelection(crEditor, sSelection, CAStylerCatalog.ID_CONTX);
		}
		return null;
	}

	public static StructuredSelection setContFromSelection(ColsRowsInfosEditor crEditor, StructuredSelection sSelection, String info) {
		
		CAInfos infos = crEditor.getResult();
		CA ca = infos.getParent();

		ArrayList<String> toMerge = new ArrayList<String>();
		String[] names = infos.getNames();

		for (Object o : sSelection.toArray()) {
			if (o instanceof Integer i) {
				toMerge.add(names[i]);
				break;
			}
		}

		String rowOrCol = CAStylerCatalog.VALUE_ID_DIMENSION_COLS;
		if (infos instanceof CARowsInfo) {
			rowOrCol = CAStylerCatalog.VALUE_ID_DIMENSION_ROWS;
		}

		double newValue = Double.MAX_VALUE;
		for (Object o : sSelection.toList()) { // get the min value of the selection
			if (o instanceof Integer lineIndex) {
				Object infoValue;
				try {
					infoValue = infos.getData(info, lineIndex);

					if (infoValue instanceof Number n && n.doubleValue() < newValue) {
						newValue = n.doubleValue();
					}
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		CAFilterPanel panel = crEditor.getCAFactorialMapChartEditor().getCAFilterPanel();
		LinkedHashMap<String, LinkedHashMap<String, Double>> thresholds = panel.getThresholds();

		try {
			thresholds.get(rowOrCol).put(info, newValue);
			panel.updateWidgetsFromThresholds();
			panel.updateFilterStylingSheetFromThresholdAndUI();
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		panel.updateFilterStylingSheetFromThresholdAndUI();

		return sSelection;
	}

}
