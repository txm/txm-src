// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.ca.rcp.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.functions.CAInfos;
import org.txm.ca.rcp.editors.CAFactorialMapChartEditor;
import org.txm.ca.rcp.editors.ColsRowsInfosEditor;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.rcp.editors.ITXMResultEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.utils.logger.Log;

/**
 * Set a row or column as supplmentary
 * 
 * @author mdecorde
 * 
 */
public class SupplementaryColRowFromInfos extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		final Object selection = HandlerUtil.getCurrentSelection(event);
		final Object editor = HandlerUtil.getActiveEditor(event);

		if (editor instanceof ColsRowsInfosEditor crEditor && selection instanceof StructuredSelection sSelection && !sSelection.isEmpty()) {
			CAInfos infos = crEditor.getResult();
			CA ca = infos.getParent();
			LexicalTable lt = ca.getParent();

			ArrayList<String> toMerge = new ArrayList<String>();
			String[] names = infos.getNames();

			for (Object o : sSelection.toArray()) {
				if (o instanceof Integer i) {
					toMerge.add(names[i]);
				}
			}


			if (infos.getInfoName().equals(TXMCoreMessages.common_rows)) {
				Log.info(CAUIMessages.bind(CAUIMessages.setTheP0SupplementaryRowsOfP1, toMerge, crEditor.getResult()));

				try {
					
					List<String> rows = ca.getSupplmentaryRows();
					List<String> cols = ca.getSupplmentaryCols();
					ArrayList<String> newrows = new ArrayList<String>(rows);
					newrows.addAll(toMerge);
					ca.setSupplmentaryRowsCols(newrows, cols);
					ca.setDirty();
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				
				Log.info(CAUIMessages.bind(CAUIMessages.setTheP0SupplementaryColumnsOfP1, toMerge, crEditor.getResult()));

				try {
					List<String> rows = ca.getSupplmentaryRows();
					List<String> cols = ca.getSupplmentaryCols();
					ArrayList<String> newcols = new ArrayList<String>(cols);
					newcols.addAll(toMerge);
					ca.setSupplmentaryRowsCols(rows, newcols);
					ca.setDirty();
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			crEditor.getViewer().setSelection(null);

			ITXMResultEditor<TXMResult> ltEditor = SWTEditorsUtils.getEditor(lt);
			if (ltEditor != null) {
				try {
					ltEditor.close(false);
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			ITXMResultEditor<TXMResult> caEditor = SWTEditorsUtils.getEditor(ca);
			if (caEditor != null && caEditor instanceof CAFactorialMapChartEditor cafamacaEditor) {
				cafamacaEditor.autoUpdateEditorFieldsFromResult(false);
				caEditor.compute(true);
			}
			CorporaView.refreshObject(lt);

			crEditor.compute(true);

			return selection;
		}
		return null;
	}


}
