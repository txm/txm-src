// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.ca.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.functions.CARowsInfo;
import org.txm.ca.rcp.editors.ColsRowsInfosEditor;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.utils.logger.Log;

/**
 * Opens a CA row Infos editor.
 * 
 * @author mdecorde
 * 
 */
public class ComputeCARowInfos extends BaseAbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkStatsEngine()) {
			return null;
		}

		final Object selection = this.getCorporaViewSelectedObject(event);

		if (selection == null) return null;

		ColsRowsInfosEditor editor = open(selection);
		ComputeCA.moveEditorNextToCAEditor(editor);
		return editor;
	}

	/**
	 * 
	 * @param selection a CAInfos or a CA
	 * @return
	 */
	public static ColsRowsInfosEditor open(Object selection) {

		try {

			CARowsInfo infos = null;

			// Reopening an existing result
			if (selection instanceof CA ca) {
				infos = ca.getFirstChild(CARowsInfo.class);
				if (infos == null) {
					infos = new CARowsInfo(ca);
				}
			}
			else if (selection instanceof CARowsInfo e) {
				infos = e;
			}
			else {
				Log.warning(CAUIMessages.mustBeOpenedWithACASelectionAborting);
				return null;
			}

			return ComputeCAColInfos.open(infos);
		}
		catch (Throwable e) {
			Log.severe(CAUIMessages.bind(CAUIMessages.error_cannot_compute_withP0, selection));
			Log.printStackTrace(e);
		}
		return null;
	}

}
