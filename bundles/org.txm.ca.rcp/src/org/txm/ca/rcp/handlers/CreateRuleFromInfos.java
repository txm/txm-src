// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.ca.rcp.handlers;

import java.util.ArrayList;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.ca.core.chartsengine.styling.CAStylerCatalog;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.functions.CAInfos;
import org.txm.ca.rcp.editors.ColsRowsInfosEditor;
import org.txm.ca.rcp.editors.styling.StylingSheetTable;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.chartsengine.core.styling.StylingRule;
import org.txm.chartsengine.core.styling.StylingSheet;
import org.txm.chartsengine.core.styling.catalogs.SelectionInstructionsCatalog;
import org.txm.chartsengine.core.styling.instructions.SelectionInstruction;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.utils.logger.Log;

/**
 * Set a row or column as supplmentary
 * 
 * @author mdecorde
 * 
 */
public class CreateRuleFromInfos extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		final Object selection = HandlerUtil.getCurrentSelection(event);
		final Object editor = HandlerUtil.getActiveEditor(event);

		if (editor instanceof ColsRowsInfosEditor crEditor && selection instanceof StructuredSelection sSelection && !sSelection.isEmpty()) {
			CAInfos infos = crEditor.getResult();
			CA ca = infos.getParent();
			LexicalTable lt = ca.getParent();

			ArrayList<String> toMerge = new ArrayList<String>();
			String[] names = infos.getNames();

			for (Object o : sSelection.toArray()) {
				if (o instanceof Integer i) {
					toMerge.add(names[i]);
				}
			}

			StylingSheetTable table = crEditor.getCAFactorialMapChartEditor().getStylingSheetTable();
			StylingSheet sheet = table.getStylingSheet();
			StylingRule rule = ca.getCatalogs().newDefaultRule();
			sheet.getRules().add(rule);
			Log.info(CAUIMessages.bind(CAUIMessages.creatingARuleWithSelectionP0, toMerge));
			String object = CAStylerCatalog.VALUE_ID_DIMENSION_ROWS;
			if (infos.getInfoName().equals(TXMCoreMessages.common_cols)) {
				for (SelectionInstruction si : rule.getSelectors()) {
					if (si.getInstruction().getID().equals(CAStylerCatalog.ID_OBJECT)) {
						object = CAStylerCatalog.VALUE_ID_DIMENSION_COLS;
					}
				}
			}
			// set the right object in the rule
			for (SelectionInstruction si : rule.getSelectors()) {
				if (si.getInstruction().getID().equals(CAStylerCatalog.ID_OBJECT)) {
					si.setSettings(object);
				}
			}
			
			SelectionInstruction labelSelector = new SelectionInstruction(ca.getCatalogs().getSelectionInstructionsCatalog(),
					CAStylerCatalog.ID_LABEL_REGEX,
					SelectionInstructionsCatalog.REGEX); // test with a REGEX
			labelSelector.setParameterDefinition(CAStylerCatalog.ID_LABEL); // working with CA row|col Labels
			labelSelector.setSettings(Pattern.compile(StringUtils.join(toMerge, "|"))); // the REGEX to use //$NON-NLS-1$
			rule.getSelectors().add(labelSelector);
			
			table.refresh();

			return selection;
		}
		return null;
	}


}
