// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.ca.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.functions.Eigenvalues;
import org.txm.ca.rcp.editors.EigenvaluesTableEditor;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.chartsengine.rcp.editors.ChartEditorInput;
import org.txm.rcp.StatusLine;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.utils.logger.Log;

/**
 * Opens a CA singular values (table) editor.
 * 
 * @author mdecorde
 * 
 */
public class ComputeCASingularValuesTable extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkStatsEngine()) {
			return null;
		}

		final Object selection = this.getCorporaViewSelectedObject(event);

		EigenvaluesTableEditor editor = open(selection);
		ComputeCA.moveEditorNextToCAEditor(editor);
		return editor;
	}

	public static EigenvaluesTableEditor open(Object selection) {

		try {
			Eigenvalues eig = null;

			// Reopening an existing result
			if (selection instanceof CA ca) {
				eig = ca.getFirstChild(Eigenvalues.class);
				if (eig == null) {
					eig = new Eigenvalues(ca);
				}
			}
			else if (selection instanceof Eigenvalues e) {
				eig = e;
			}
			else {
				Log.warning(CAUIMessages.mustBeOpenedWithACASelectionAborting);
				return null;
			}

			return open(eig);
		}
		catch (Throwable e) {
			Log.severe(CAUIMessages.bind(CAUIMessages.error_cannot_compute_withP0, selection));
			Log.printStackTrace(e);
		}
		return null;
	}

	public static EigenvaluesTableEditor open(Eigenvalues eig) {
		try {
			ChartEditorInput<Eigenvalues> editorInput = new ChartEditorInput<>(eig);
			// IWorkbenchPage page = TXMWindows.getActivePage();
			StatusLine.setMessage(CAUIMessages.openingTheCorrespondenceAnalysisResults);
			TXMEditor<Eigenvalues> editor = TXMEditor.openEditor(editorInput, EigenvaluesTableEditor.class.getName());
			if (editor != null && editor instanceof EigenvaluesTableEditor eteditor) {
				return eteditor;
			}

		}
		catch (Throwable ex) {
			Log.severe(CAUIMessages.bind(CAUIMessages.error_openingP0, eig));
			Log.printStackTrace(ex);
		}
		return null;
	}
}
