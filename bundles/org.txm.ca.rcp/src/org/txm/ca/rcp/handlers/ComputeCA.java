// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.ca.rcp.handlers;

import java.util.Set;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.functions.ICAComputable;
import org.txm.ca.core.preferences.CAPreferences;
import org.txm.ca.rcp.editors.CAEditor;
import org.txm.ca.rcp.editors.CAFactorialMapChartEditor;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.chartsengine.rcp.editors.ChartEditorInput;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;
import org.txm.index.core.functions.PartitionIndex;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.rcp.StatusLine;
import org.txm.rcp.editors.ITXMResultEditor;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.utils.logger.Log;

/**
 * Opens a CA editor.
 * Computes the result if it contains all required parameters.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class ComputeCA extends BaseAbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkStatsEngine()) {
			return null;
		}

		final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
		final Object selection = this.getCorporaViewSelectedObject(event);

		try {

			LexicalTable lexicalTable = null;
			CA ca = null;

			// Reopening an existing result
			if (selection instanceof CA) {
				ca = (CA) selection;
			}
			// Creating
			else {
				// Creating from Partition
				if (selection instanceof Partition) {

					final Partition partition = (Partition) selection;

					// not enough parts error
					if (partition.getPartsCount() < 3) {
						Display.getDefault().syncExec(new Runnable() {

							@Override
							public void run() {

								MessageDialog d = new MessageDialog(window.getShell(),
										TXMCoreMessages.error_error,
										null,
										NLS.bind(CAUIMessages.canNotComputeFactorialAnalysisWithOnlyP0PartsTheMinimumIs4Parts, partition.getPartsCount()), 0,
										new String[] { TXMCoreMessages.common_ok }, 0);
								d.open();
							}
						});
						return null;
					}

					lexicalTable = new LexicalTable(partition);
					lexicalTable.setFMinFilter(CAPreferences.getInstance().getInt(CAPreferences.F_MIN));
					lexicalTable.setVMaxFilter(CAPreferences.getInstance().getInt(CAPreferences.V_MAX));
					ca = new CA(lexicalTable);
				}
				else if (selection instanceof PartitionIndex) {
					PartitionIndex index = (PartitionIndex) selection;
					lexicalTable = new LexicalTable(index);

					if (index.getPartition().getPartsCount() < 3) {
						MessageDialog.openError(window.getShell(), TXMCoreMessages.error_error, CAUIMessages.canNotComputeCAOnLexicalTableWithLessThan4Columns);
						return null;
					}
					// FIXME: SJ: problem here because we need the index has been computed to make this check
					if (index.hasBeenComputedOnce() && index.getAllLines().size() < 3) {
						MessageDialog.openError(window.getShell(), TXMCoreMessages.error_error, CAUIMessages.canNotComputeCAWithLessThan4RowsInTheTable);
						return null;
					}

					ca = new CA(lexicalTable);
				}
				// Creating from Lexical Table
				else if (selection instanceof LexicalTable) {
					lexicalTable = (LexicalTable) selection;

					// FIXME: SJ: problem here because we need the LT has been computed to make this check
					if (lexicalTable.hasBeenComputedOnce() && lexicalTable.getNColumns() < 3) {
						MessageDialog.openError(window.getShell(), TXMCoreMessages.error_error, CAUIMessages.canNotComputeCAOnLexicalTableWithLessThan4Columns);
						return null;
					}
					// FIXME: SJ: problem here because we need the LT has been computed to make this check
					if (lexicalTable.hasBeenComputedOnce() && lexicalTable.getNRows() < 3) {
						MessageDialog.openError(window.getShell(), TXMCoreMessages.error_error, CAUIMessages.canNotComputeCAWithLessThan4RowsInTheTable);
						return null;
					}

					ca = new CA(lexicalTable);
				}
				else if (selection instanceof ICAComputable c) {
					((TXMResult) c).compute(false);
					ca = c.toCA();
					lexicalTable = ca.getLexicalTable();
				}

				// show the lexical table only if the command was called from a lexical table
				if (!(selection instanceof LexicalTable)) {
					if (lexicalTable != null) lexicalTable.setVisible(false);
				}

			}

			try {
				//ChartEditorInput<CA> editorInput = new ChartEditorInput<>(ca);
				StatusLine.setMessage(CAUIMessages.openingTheCorrespondenceAnalysisResults);
				boolean groupedMode = false;
				if (groupedMode) {
					TXMEditor.openEditor(ca, CAEditor.class.getName());
				}
				else {
					open(ca, true);
				}
			}
			catch (Throwable e) {
				Log.severe(CAUIMessages.bind(CAUIMessages.error_openingP0, ca));
				Log.printStackTrace(e);
			}
		}
		catch (Throwable e) {
			Log.severe(CAUIMessages.bind(CAUIMessages.error_cannot_compute_withP0, selection));
			Log.printStackTrace(e);
		}
		return null;
	}

	public static TXMEditor<CA> open(CA ca, boolean openInfos) {
		ChartEditorInput<CA> editorInput = new ChartEditorInput<>(ca);
		boolean wasComputed = ca.hasBeenComputedOnce();
		
		boolean wasOpened = SWTEditorsUtils.isOpenEditor(editorInput, CAFactorialMapChartEditor.class.getName());
		TXMEditor<CA> editor = TXMEditor.openEditor(editorInput, CAFactorialMapChartEditor.class.getName());
		if (CAPreferences.getInstance().getBoolean(CAPreferences.OPEN_INFOS)) {
			if (openInfos && !wasOpened && editor != null && editor instanceof CAFactorialMapChartEditor caFactorEditor) {

				if (CAPreferences.getInstance().getBoolean(CAPreferences.OPEN_INFOS_IN_BOX)) {
					SWTEditorsUtils.boxEditor(caFactorEditor, null, false); // not working yet
				}

				caFactorEditor.openVariablesEditor();
				caFactorEditor.openIndividualsEditor();
				caFactorEditor.openSingularValuesTableEditor();
				caFactorEditor.setFocus();
				if (!wasComputed) {
					caFactorEditor.resetView();
				}
				
				return caFactorEditor;
			}
		}
		
		return editor;
	}
	
	/**
	 * Move any subresult of CA editor next to the CA editor
	 * 
	 * @param eEditor
	 */
	public static void moveEditorNextToCAEditor(TXMEditor<?> eEditor) {

		if (eEditor == null) return;

		TXMResult parent = eEditor.getResult().getParent();
		if (!(parent instanceof CA ca)) {
			return;
		}

		Set<ITXMResultEditor<TXMResult>> caEditors = SWTEditorsUtils.getEditors(ca);
		for (ITXMResultEditor<TXMResult> editor : caEditors) {
			if (editor instanceof TXMEditor<?> txmEditor) {
				SWTEditorsUtils.moveEditor(txmEditor, eEditor, EModelService.RIGHT_OF, 0.3f);
			}
			break;
		}
	}
}
