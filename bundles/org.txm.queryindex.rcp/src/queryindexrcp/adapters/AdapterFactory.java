// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package queryindexrcp.adapters;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.txm.queryindex.core.functions.QueryIndex;
import org.txm.rcp.Application;
import org.txm.rcp.IImageKeys;

/**
 * A factory for creating Adapter objects.
 * 
 * @author mdecorde
 */
public class AdapterFactory implements IAdapterFactory {

	/** The Index adapter. */
	private IWorkbenchAdapter QueryIndexAdapter = new IWorkbenchAdapter() {

		@Override
		public Object[] getChildren(Object o) {
			return new Object[0];
		}

		@Override
		public ImageDescriptor getImageDescriptor(Object object) {
			return AbstractUIPlugin.imageDescriptorFromPlugin(
					Application.PLUGIN_ID, IImageKeys.QUERYINDEX);
		}

		@Override
		public String getLabel(Object o) {
			return ((QueryIndex) o).getName();
		}

		@Override
		public Object getParent(Object o) {
			return ((QueryIndex) o).getCorpus();
		}
	};

	/**
	 * Gets the adapter.
	 *
	 * @param adaptableObject the adaptable object
	 * @param adapterType the adapter type
	 * @return the adapter
	 * @see org.eclipse.core.runtime.IAdapterFactory#getAdapter(java.lang.Object,
	 *      java.lang.Class)
	 */
	@Override
	public <T> T getAdapter(Object adaptableObject, Class<T> adapterType) {
		if (adapterType == IWorkbenchAdapter.class
				&& adaptableObject instanceof QueryIndex)
			return adapterType.cast(QueryIndexAdapter);
		return null;
	}

	/**
	 * Gets the adapter list.
	 *
	 * @return the adapter list
	 * @see org.eclipse.core.runtime.IAdapterFactory#getAdapterList()
	 */
	@Override
	public Class<?>[] getAdapterList() {
		return new Class[] { IWorkbenchAdapter.class };
	}

}
