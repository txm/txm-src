// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.queryindex.rcp.editors;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.PartInitException;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.index.rcp.messages.IndexUIMessages;
import org.txm.queryindex.core.functions.Messages;
import org.txm.queryindex.core.functions.QueryIndex;
import org.txm.queryindex.core.functions.QueryIndexLine;
import org.txm.rcp.StatusLine;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TXMResultEditorInput;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.TXMFontRegistry;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.swt.widget.NamedAssistedQueryWidget;
import org.txm.rcp.swt.widget.NavigationWidget;
import org.txm.rcp.views.QueriesView;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.statsengine.r.rcp.views.RVariablesView;
import org.txm.utils.logger.Log;

/**
 * Display the query index parameters and result.
 * 
 * @author mdecorde
 */
public class QueryIndexEditor extends TXMEditor<QueryIndex> {

	/** The Constant ID. */
	public static final String ID = "org.txm.rcp.editors.queryindex.QueryIndexEditor"; //$NON-NLS-1$

	/** The corpus. */
	protected CQPCorpus corpus;

	protected Partition partition; // can be null

	/** The index. */
	protected QueryIndex queryindex;

	boolean stored = false; // start storing at first line added
	// protected boolean saveQueryIndex = true;

	// params
	/** The query label. */
	protected Label queryLabel;

	/** The query widget. */
	protected NamedAssistedQueryWidget queryWidget;

	/** The N ligne p page spinner. */
	protected Spinner NLignePPageSpinner;

	// infos
	/** The navigation area. */
	protected NavigationWidget navigationArea;

	/** The l fmin info. */
	protected Label lFminInfo;

	/** The l fmax info. */
	protected Label lFmaxInfo;

	/** The l v info. */
	protected Label lVInfo;

	/** The l t info. */
	protected Label lTInfo;

	// result
	/** The line table viewer. */
	protected TableViewer viewer;

	/** The n column. */
	protected TableColumn nColumn;

	/** The unit column. */
	protected TableColumn unitColumn;

	/** The freq column. */
	protected TableColumn freqColumn;

	/** The separator column. */
	protected TableColumn separatorColumn;

	/** The top line. */
	protected int topLine;

	/** The bottom line. */
	protected int bottomLine;

	/** The nblinesperpage. */
	protected int nblinesperpage;

	/** The nblinesmax. */
	protected int nblinesmax;

	// /** The scroll composite. */
	// private ScrolledComposite scrollComposite;
	//
	// /** The head composite. */
	// private Composite headComposite;

	/** The title. */
	String title;

	/**
	 * Instantiates a new index editor.
	 */
	public QueryIndexEditor() {
		super();
	}

	/**
	 * Do save.
	 * 
	 * @param arg0
	 *            the arg0
	 * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor arg0) {
		// TODO Auto-generated method stub
	}

	/**
	 * Do save as.
	 * 
	 * @see "org.eclipse.lyon gournd zeroui.part.EditorPart#doSaveAs()"
	 */
	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub
	}

	/**
	 * Inits the.
	 * 
	 * @param site the site
	 * @param input the input
	 * @throws PartInitException the part init exception
	 * @see org.eclipse.ui.part.EditorPart#init(org.eclipse.ui.IEditorSite, org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void _init() throws PartInitException {

		TXMResultEditorInput<QueryIndex> tre = getEditorInput();
		this.corpus = tre.getResult().getCorpus();
		this.partition = tre.getResult().getPartition();
		this.queryindex = tre.getResult();
	}

	/**
	 * Checks if is dirty.
	 * 
	 * @return true, if is dirty
	 * @see org.eclipse.ui.part.EditorPart#isDirty()
	 */
	@Override
	public boolean isDirty() {
		return false;
	}

	/**
	 * Checks if is save as allowed.
	 * 
	 * @return true, if is save as allowed
	 * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void updateResultFromEditor() {

		System.out.println("QueryIndexEditor.updateResultFromEditor(): not implemented."); //$NON-NLS-1$
	}

	/**
	 * Compute index.
	 */
	@Override
	public void updateEditorFromResult(boolean update) {

		try {
			fillDisplayArea(0, NLignePPageSpinner.getSelection());
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Creates the part control.
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void _createPartControl() {

		final Group paramArea = getExtendedParametersGroup();

		GLComposite resultArea = getResultArea();

		// on créé une Query, ici le pivot de la concordance est "[]"
		// Query Area: query itself + view properties
		Composite queryArea = new Composite(paramArea, SWT.NONE);
		RowData rdata = new RowData();
		queryArea.setLayoutData(rdata);
		queryArea.setLayout(new GridLayout(4, false));

		// | Query: [ (v)] [Search] |
		// Query:
		// queryLabel = new Label(queryArea, SWT.NONE);
		// queryLabel.setText(RCPMessages.IndexEditor_3);
		// queryLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
		// true));

		// [ (v)]
		queryWidget = new NamedAssistedQueryWidget(queryArea, SWT.DROP_DOWN, this.corpus);
		GridData layoutData = new GridData(GridData.FILL);
		layoutData.horizontalAlignment = GridData.FILL;
		layoutData.grabExcessHorizontalSpace = true;
		layoutData.minimumWidth = 400;
		layoutData.widthHint = 900;
		queryWidget.setLayoutData(layoutData);
		queryWidget.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// System.out.println("key pressed : "+e.keyCode);
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					try {
						addQuery();
					}
					catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}
		});

		Button loadFromFile = new Button(queryArea, SWT.BOLD);
		loadFromFile.setToolTipText("Load queries from a properties file.");
		loadFromFile.setEnabled(true);
		loadFromFile.setText(Messages.QueryIndexEditor_4);
		loadFromFile.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_CENTER));
		loadFromFile.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				File propFile = null;
				if (LastOpened.getFile(ID) != null) {
					propFile = new File(LastOpened.getFolder(ID));
				}
				FileDialog dialog = new FileDialog(getShell(), SWT.OPEN);
				String[] exts = { "*.properties" }; // $NON-NLS //$NON-NLS-1$
				dialog.setFilterExtensions(exts);
				if (propFile != null) {
					dialog.setFilterPath(propFile.getParent());
					dialog.setFileName(propFile.getName());
				}
				String path = dialog.open();
				if (path != null) {
					propFile = new File(path);
					// System.out.println("TODO: load from file "+propFile);
					try {
						try {
							queryindex.addLinesFromFile(propFile);
						}
						catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						queryindex.sortLines(0, true);

						stored = true;

						fillDisplayArea(0, NLignePPageSpinner.getSelection());

						CorporaView.refresh();
						CorporaView.expand(queryindex.getParent());
						QueriesView.refresh();
						RVariablesView.refresh();

						setPartName(title);

						queryWidget.focus();
					}
					catch (Exception e1) {
						Log.severe(TXMCoreMessages.bind(Messages.error_QueryIndexEditor_6, propFile));
						Log.printStackTrace(e1);
					}
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Button addQueryButton = new Button(queryArea, SWT.BOLD);
		addQueryButton.setText(Messages.QueryIndexEditor_12);
		addQueryButton.setToolTipText("Add a new named query");
		//		Font f = addQueryButton.getFont();
		//		FontData defaultFont = f.getFontData()[0];
		//		defaultFont.setStyle(SWT.BOLD);
		//		Font newf = new Font(Display.getCurrent(), defaultFont);
		//		addQueryButton.setFont(newf);

		layoutData = new GridData(GridData.VERTICAL_ALIGN_CENTER);
		layoutData.horizontalAlignment = GridData.FILL;
		layoutData.grabExcessHorizontalSpace = false;
		addQueryButton.setLayoutData(layoutData);

		addQueryButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					addQuery();
				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// Filters
		Composite filtercontrols = new Composite(paramArea, SWT.NONE);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 9;
		gridLayout.makeColumnsEqualWidth = false;
		filtercontrols.setLayout(gridLayout);

		Label lNLigneppage = new Label(filtercontrols, SWT.NONE);
		lNLigneppage.setText(TXMUIMessages.common_elementsPerPage);
		lNLigneppage.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));

		NLignePPageSpinner = new Spinner(filtercontrols, SWT.BORDER);
		NLignePPageSpinner.setMinimum(0);
		NLignePPageSpinner.setMaximum(99999);
		NLignePPageSpinner.setSelection(100);
		NLignePPageSpinner.setIncrement(1);
		NLignePPageSpinner.setPageIncrement(100);
		NLignePPageSpinner.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_END));
		NLignePPageSpinner.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				fillDisplayArea(0, NLignePPageSpinner.getSelection());
				viewer.getTable().select(0);
				viewer.getTable().showSelection();
			}
		});

		// compose infosArea
		// infosArea.setLayout(new GridLayout(6, false));

		navigationArea = new NavigationWidget(filtercontrols, SWT.NONE);
		navigationArea.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false));

		navigationArea.addFirstListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				fillDisplayArea(0, NLignePPageSpinner.getSelection());
				viewer.getTable().select(0);
				viewer.getTable().showSelection();
			}
		});
		navigationArea.addLastListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				int top = (queryindex.getV() / NLignePPageSpinner.getSelection()) * NLignePPageSpinner.getSelection();
				int bottom = top + NLignePPageSpinner.getSelection();
				fillDisplayArea(top, bottom);
				viewer.getTable().select(0);
				viewer.getTable().showSelection();
			}
		});
		navigationArea.addNextListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				int top = topLine + NLignePPageSpinner.getSelection();
				int bottom = top + NLignePPageSpinner.getSelection();
				fillDisplayArea(top, bottom);
				viewer.getTable().select(0);
				viewer.getTable().showSelection();
			}
		});
		navigationArea.addPreviousListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				int top = topLine - NLignePPageSpinner.getSelection();
				if (top < 0) {
					top = 0;
				}
				int bottom = top + NLignePPageSpinner.getSelection();
				fillDisplayArea(top, bottom);
				viewer.getTable().select(0);
				viewer.getTable().showSelection();
			}
		});

		// Labels
		lTInfo = new Label(filtercontrols, SWT.NONE);
		lTInfo.setText(""); //$NON-NLS-1$
		lTInfo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

		lVInfo = new Label(filtercontrols, SWT.NONE);
		lVInfo.setText(""); //$NON-NLS-1$
		lVInfo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

		lFminInfo = new Label(filtercontrols, SWT.NONE);
		lFminInfo.setText(""); //$NON-NLS-1$
		lFminInfo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

		lFmaxInfo = new Label(filtercontrols, SWT.NONE);
		lFmaxInfo.setText(""); //$NON-NLS-1$
		lFmaxInfo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

		// results
		viewer = new TableViewer(resultArea, SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER | SWT.VIRTUAL);
		viewer.getTable().setLinesVisible(true);
		viewer.getTable().setHeaderVisible(true);
		TableUtils.installCopyAndSearchKeyEvents(viewer);
		TableUtils.installColumnResizeMouseWheelEvents(viewer);
		TableUtils.installTooltipsOnSelectionChangedEvent(viewer);
		viewer.getTable().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		if (corpus != null && corpus.getProject().getFont() != null && corpus.getProject().getFont().length() > 0) {
			Font old = viewer.getTable().getFont();
			FontData fD = old.getFontData()[0];
			Font font = TXMFontRegistry.getFont(Display.getCurrent(), corpus.getProject().getFont(), fD.getHeight(), fD.getStyle());
			viewer.getTable().setFont(font);
		}

		viewer.setLabelProvider(new LineLabelProvider());
		viewer.setContentProvider(new LineContentProvider());
		viewer.setInput(new ArrayList<Property>());

		viewer.getTable().addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				Point mouseposition = new Point(e.x + viewer.getTable().getHorizontalBar().getSelection(), e.y);
				int col = getPointedColumn(mouseposition);
				if (col == 1) {
					// IndexToConcordance.link(self,
					// (IStructuredSelection) viewer
					// .getSelection());
					Log.warning(Messages.QueryIndexEditor_7);
				}
				else if (col == 2) {
					Log.warning(Messages.QueryIndexEditor_8);
				}
			}
		});

		viewer.getTable().addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.DEL) {
					Log.info(Messages.QueryIndexEditor_9);
					ISelection sel = viewer.getSelection();
					if (sel instanceof StructuredSelection) {
						List<?> lines = ((StructuredSelection) sel).toList();
						for (Object obj : lines) {
							if (obj instanceof QueryIndexLine) {
								QueryIndexLine line = (QueryIndexLine) obj;
								queryindex.removeLine(line.getName());
							}
						}
						fillDisplayArea(topLine, bottomLine);
					}
				}
			}
		});

		nColumn = new TableColumn(viewer.getTable(), SWT.LEFT);
		nColumn.setText(""); //$NON-NLS-1$
		nColumn.pack();

		unitColumn = new TableColumn(viewer.getTable(), SWT.LEFT);
		unitColumn.setText(TXMCoreMessages.common_units);
		unitColumn.setToolTipText(TXMCoreMessages.common_units);
		unitColumn.setWidth(200);
		unitColumn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				StatusLine.setMessage(IndexUIMessages.tP0vP1fminP2fmaxP3);
				if (viewer.getTable().getSortColumn() != unitColumn) {
					viewer.getTable().setSortColumn(unitColumn);
					viewer.getTable().setSortDirection(SWT.UP);
					queryindex.sortLines(-1, false);
				}
				else {
					if (viewer.getTable().getSortDirection() == SWT.UP) {
						viewer.getTable().setSortDirection(SWT.DOWN);
						queryindex.sortLines(-1, true);
					}
					else {
						viewer.getTable().setSortDirection(SWT.UP);
						queryindex.sortLines(-1, false);
					}
				}

				StatusLine.setMessage(IndexUIMessages.tP0vP1fminP2fmaxP3);
				fillDisplayArea(topLine, bottomLine);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		freqColumn = new TableColumn(viewer.getTable(), SWT.RIGHT);
		try {
			if (partition != null)
				freqColumn.setText(TXMCoreMessages.common_frequency + " T=" + partition.getTotalSize()); //$NON-NLS-1$
			else
				freqColumn.setText(TXMCoreMessages.common_frequency + " T=" + corpus.getSize()); //$NON-NLS-1$
		}
		catch (CqiClientException e2) {
			Log.printStackTrace(e2);
			return;
		}
		freqColumn.setToolTipText(TXMCoreMessages.common_frequency);
		freqColumn.setWidth(100);
		freqColumn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (viewer.getTable().getSortColumn() != freqColumn) {
					viewer.getTable().setSortColumn(freqColumn);
					viewer.getTable().setSortDirection(SWT.UP);
					queryindex.sortLines(0, false);
				}
				else {
					if (viewer.getTable().getSortDirection() == SWT.UP) {
						viewer.getTable().setSortDirection(SWT.DOWN);
						queryindex.sortLines(0, true);
					}
					else {
						viewer.getTable().setSortDirection(SWT.UP);
						queryindex.sortLines(0, false);
					}
				}
				StatusLine.setMessage(IndexUIMessages.tP0vP1fminP2fmaxP3);
				fillDisplayArea(topLine, bottomLine);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// rmv/creates parts columns if needed
		if (this.partition != null) {
			List<String> partnames = this.partition.getPartNames();
			if (partnames.size() > 1) {
				if (viewer.getTable().getColumnCount() < partnames.size() + 3) {
					for (int i = 0; i < partnames.size(); i++) {
						final TableColumn partColumn = new TableColumn(viewer.getTable(), SWT.RIGHT);
						try {
							partColumn.setText(partnames.get(i) + " t=" + partition.getParts().get(i).getSize()); //$NON-NLS-1$
						}
						catch (CqiClientException e1) {
							Log.printStackTrace(e1);
							return;
						}
						partColumn.setToolTipText(partnames.get(i));
						partColumn.setWidth(100);
						partColumn.addSelectionListener(new SelectionListener() {

							@Override
							public void widgetSelected(SelectionEvent e) {
								if (viewer.getTable().getSortColumn() != partColumn) {
									viewer.getTable().setSortColumn(partColumn);
									viewer.getTable().setSortDirection(SWT.UP);
									queryindex.sortLines(0, false);
								}
								else {
									if (viewer.getTable().getSortDirection() == SWT.UP) {
										viewer.getTable().setSortDirection(SWT.DOWN);
										queryindex.sortLines(0, true);
									}
									else {
										viewer.getTable().setSortDirection(SWT.UP);
										queryindex.sortLines(0, false);
									}
								}
								fillDisplayArea(topLine, bottomLine);
							}

							@Override
							public void widgetDefaultSelected(SelectionEvent e) {
							}
						});
					}
				}
			}
		}
		separatorColumn = new TableColumn(viewer.getTable(), SWT.LEFT);
		separatorColumn.setText(""); //$NON-NLS-1$
		separatorColumn.pack();

		initializeFields();
		createContextMenu(viewer);
		viewer.getTable().pack();
	}

	protected boolean addQuery() throws Exception {

		final CQLQuery query; // if the query is empty then we put "[]" instead
		if (queryWidget.getQueryString().length() > 0) {//$NON-NLS-1$
			query = new CQLQuery(queryWidget.getQueryString());
		}
		else {
			Log.warning(IndexUIMessages.indexColonQueryIsEmpty);
			return false;
		}

		final String name;
		if (queryWidget.getQueryName().length() == 0) {
			name = query.toString();
		}
		else {
			name = queryWidget.getQueryName();
		}

		queryWidget.memorize();
		nblinesperpage = NLignePPageSpinner.getSelection();

		try {
			if (queryindex.hasLine(name)) {
				Log.info(TXMCoreMessages.bind(Messages.QueryIndexEditor_1, this));
				queryindex.removeLine(name);
			}
			QueryIndexLine line = queryindex.addLine(name, query);

			if (line != null) {
				queryWidget.setText(""); //$NON-NLS-1$
				queryWidget.setName(""); //$NON-NLS-1$
				queryWidget.focus();
			}
			else {
				Log.warning(TXMCoreMessages.bind(Messages.error_QueryIndexEditor_2, this));
				return false;
			}

			updateEditorFromResult(false);
		}
		catch (CqiClientException e2) {
			Log.severe(TXMCoreMessages.bind(Messages.error_QueryIndexEditor_2, this));
			Log.printStackTrace(e2);
			return false;
		}
		return false;
	}

	/**
	 * Gets the pointed column.
	 * 
	 * @param mouseposition the mouseposition
	 * 
	 * @return the pointed column
	 */
	public int getPointedColumn(Point mouseposition) {

		int x = mouseposition.x; // + lineTableViewer.getTable().get;
		int sumWidthColumn = this.nColumn.getWidth();
		if (x < sumWidthColumn)
			return 0;

		sumWidthColumn += this.unitColumn.getWidth();
		if (x < sumWidthColumn)
			return 1;

		sumWidthColumn += this.freqColumn.getWidth();
		if (x < sumWidthColumn)
			return 2;

		return 2;
	}

	/**
	 * Creates the context menu.
	 * 
	 * @param tableViewer the table viewer
	 */
	private void createContextMenu(TableViewer tableViewer) {

		MenuManager menuManager = new MenuManager();
		Menu menu = menuManager.createContextMenu(tableViewer.getTable());

		// Set the MenuManager
		tableViewer.getTable().setMenu(menu);
		getSite().registerContextMenu(menuManager, tableViewer);
		// Make the selection available
		getSite().setSelectionProvider(tableViewer);
	}

	/**
	 * Initialize fields.
	 */
	public void initializeFields() {

		if (this.queryindex != null) {

			if (queryindex.getPartition() != null)
				setPartName(queryindex.getPartition().getName() + ": " + queryindex.getName()); //$NON-NLS-1$
			else
				setPartName(queryindex.getCorpus().getName() + ": " + queryindex.getName()); //$NON-NLS-1$

			queryWidget.setFocus();

			queryindex.sortLines(0, true);

			fillDisplayArea(0, NLignePPageSpinner.getSelection());
		}
		else if (this.corpus != null) {
			// no query
			queryWidget.setText(""); //$NON-NLS-1$

			if (partition != null) {
				setPartName(partition.getName());
			}
			else {
				setPartName(corpus.getName());
			}
			queryWidget.setFocus();
		}
		else {
			Log.severe(IndexUIMessages.couldntInitializeIndexTable);
		}
	}

	/**
	 * Fill display area.
	 * 
	 * @param from the from
	 * @param to the to
	 */
	public void fillDisplayArea(int from, int to) {
		// System.out.println("call fill from "+from+" to "+to);
		from = Math.max(from, 0);
		to = Math.min(to, queryindex.getV());
		ArrayList<QueryIndexLine> lines = null;
		if (queryindex.getV() > 0) {
			lines = queryindex.getLines(from, to);
		}
		else {
			lines = new ArrayList<>(); // no result
		}
		navigationArea.setInfoLineText("" + (from + 1), "-" + (to) + " / " + queryindex.getV()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		navigationArea.setPreviousEnabled(from > 0);
		navigationArea.setFirstEnabled(from > 0);
		navigationArea.setNextEnabled(to < queryindex.getV() - 1);
		navigationArea.setLastEnabled(to < queryindex.getV() - 1);

		try {
			lFminInfo.setText(String.valueOf(queryindex.getFmin()));
			lFmaxInfo.setText(String.valueOf(queryindex.getFmax()));
			lTInfo.setText(String.valueOf(queryindex.getT()));
			lVInfo.setText(String.valueOf(queryindex.getV()));
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		lVInfo.getParent().layout();

		unitColumn.setText(Messages.QueryIndexEditor_10);

		viewer.setInput(lines);
		viewer.refresh();
		// System.out.println("table refreshed from "+from+" to "+to+" with : "+lines);
		topLine = from;
		bottomLine = to;
		for (TableColumn col : viewer.getTable().getColumns()) {
			col.pack();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#getTitleToolTip()
	 */
	@Override
	public String getTitleToolTip() {

		if (queryindex != null) {

			String str = Messages.QueryIndexEditor_11;
			return str;
		}
		return ""; //$NON-NLS-1$
	}

	/**
	 * Sets the focus.
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void _setFocus() {

		this.queryWidget.setFocus();
		StatusLine.setMessage(IndexUIMessages.openingTheIndexResults);
	}

	/**
	 * Gets the corpus.
	 * 
	 * @return the corpus
	 */
	public CQPCorpus getCorpus() {

		return this.corpus;
	}

	/**
	 * Gets the partition.
	 * 
	 * @return the partition
	 */
	public Partition getPartition() {

		return this.partition;
	}

	/**
	 * Sets the focus.
	 * 
	 * @param query the new focus
	 */
	public void setFocus(String query) {

		this.queryWidget.setText(query);
	}

	/**
	 * Gets the index.
	 * 
	 * @return the index
	 */
	public Object getIndex() {

		return this.queryindex;
	}
}
