// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.queryindex.rcp.commands;

import java.util.ArrayList;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.dialogs.ListSelectionDialog;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.lexicaltable.rcp.messages.LexicalTableUIMessages;
import org.txm.queryindex.core.functions.QueryIndex;
import org.txm.rcp.StatusLine;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class IndexToLexicalTable.
 */
public class QueryIndexToLexicalTable extends AbstractHandler {

	/** The window. */
	private IWorkbenchWindow window;

	/** The selection. */
	private IStructuredSelection selection;

	private static Shell shell;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// TODO Auto-generated method stub
		window = HandlerUtil.getActiveWorkbenchWindow(event);
		selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		shell = HandlerUtil.getActiveWorkbenchWindowChecked(event)
				.getShell();
		return null;
	}

	/**
	 * Builds the lexical table.
	 *
	 * @param qindex the voc
	 * @param from the from
	 * @param to the to
	 * @return true, if successful
	 */
	public boolean buildLexicalTable(QueryIndex qindex, int from, int to) {
		LexicalTable table;
		Partition partition;
		Property property;

		if (!qindex.isComputedWithPartition()) {

			System.out.println(TXMUIMessages.cannotCreateALexicalTableFromAnIndexCreatedWithACorpusMoreThanOneColumnIsNeeded);
			StatusLine.setMessage(TXMUIMessages.cannotCreateALexicalTableFromAnIndexCreatedWithACorpusMoreThanOneColumnIsNeeded);
			return false;
		}

		Log.info(LexicalTableUIMessages.openingMargeConfigurationDialog);
		ArrayList<String> choices = new ArrayList<String>();
		choices.add(LexicalTableUIMessages.useAllOccurrences);
		choices.add(LexicalTableUIMessages.userIndexOccurrences);
		ListSelectionDialog dialog = new ListSelectionDialog(shell,
				choices, new ArrayContentProvider(), new LabelProvider(),
				LexicalTableUIMessages.selectWhichMarginsYouWantToUse);

		int ret = dialog.open();
		//System.out.println("ret= "+ret);
		if (ret != dialog.OK) {
			return false;
		}

		// TODO: fix line 119
		//		try {
		//			table = LexicalTableImpl.createLexicalTableImpl(voc, QuantitativeDataStructureImpl
		//					.createSymbole(LexicalTableImpl.class));
		//			partition = voc.getPartition();
		//			property = voc.getCorpus().getProperties().get(0);
		//		} catch (Exception e) {
		//			org.txm.utils.logger.Log.printStackTrace(e);
		//			return false;
		//		}
		//
		//		try {
		//			IWorkbenchWindow window = PlatformUI.getWorkbench()
		//					.getActiveWorkbenchWindow();
		//			IWorkbenchPage page = window.getActivePage();
		//			LexicalTableEditorInput editorInput = new LexicalTableEditorInput(
		//					table, partition, property);
		//			StatusLine.setMessage(RCPMessages.ComputeLexicalTable_10);
		//			page
		//					.openEditor(editorInput,
		//							"org.txm.rcp.editors.lexicaltable.LexicalTableEditor"); //$NON-NLS-1$
		//		} catch (PartInitException e) {
		//			org.txm.utils.logger.Log.printStackTrace(e);
		//		} 

		return false;
	}

}
