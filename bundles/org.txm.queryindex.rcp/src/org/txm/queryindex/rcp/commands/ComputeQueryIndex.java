// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.queryindex.rcp.commands;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.txm.index.rcp.messages.IndexUIMessages;
import org.txm.queryindex.core.functions.QueryIndex;
import org.txm.queryindex.rcp.editors.QueryIndexEditor;
import org.txm.rcp.StatusLine;
import org.txm.rcp.TXMWindows;
import org.txm.rcp.editors.TXMResultEditorInput;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.utils.logger.Log;

/**
 * Open the Query Index Editor
 * 
 * @author mdecorde.
 */
public class ComputeQueryIndex extends BaseAbstractHandler {

	/** The ID. */
	public static String ID = "org.txm.rcp.commands.function.ComputeQueryIndex"; //$NON-NLS-1$

	/** The selection. */
	private IStructuredSelection selection;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Log.info("Compute QueryIndex: start");
		selection = this.getCorporaViewSelection(event);

		Object s = selection.getFirstElement();
		if (s instanceof CQPCorpus) {
			final CQPCorpus corpus = (CQPCorpus) s;
			openEditor(corpus);
		}
		else if (s instanceof Partition) {
			final Partition partition = (Partition) s;
			openEditor(partition);
		}
		else if (s instanceof QueryIndex) {
			final QueryIndex qi = (QueryIndex) s;
			openEditor(qi);
		}
		else {
			Log.severe("Can not compute QueryIndex with: " + s);
		}

		return null;
	}

	public static void openEditor(QueryIndex qi) {
		IWorkbenchPage page = TXMWindows.getActiveWindow().getActivePage();
		TXMResultEditorInput<QueryIndex> editorInput = new TXMResultEditorInput<QueryIndex>(qi);
		try {
			StatusLine.setMessage(IndexUIMessages.openingIndexResults);
			IEditorPart editor = SWTEditorsUtils.openEditor(editorInput, QueryIndexEditor.ID);
			if (editor instanceof QueryIndexEditor) {
				QueryIndexEditor voceditor = (QueryIndexEditor) editor;
				voceditor.initializeFields();
			}
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
	}

	public static void openEditor(CQPCorpus corpus) {
		TXMResultEditorInput<QueryIndex> editorInput = new TXMResultEditorInput<QueryIndex>(new QueryIndex(corpus));
		try {
			StatusLine.setMessage(IndexUIMessages.openingIndexResults);
			IEditorPart editor = SWTEditorsUtils.openEditor(editorInput, QueryIndexEditor.ID);
			if (editor instanceof QueryIndexEditor) {
				QueryIndexEditor voceditor = (QueryIndexEditor) editor;
				voceditor.initializeFields();
			}
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
	}

	public static void openEditor(Partition partition) {
		IWorkbenchPage page = TXMWindows.getActiveWindow().getActivePage();
		TXMResultEditorInput<QueryIndex> editorInput = new TXMResultEditorInput<QueryIndex>(new QueryIndex(partition));
		try {
			StatusLine.setMessage(IndexUIMessages.openingIndexResults);
			QueryIndexEditor voceditor = (QueryIndexEditor) page
					.openEditor(editorInput, QueryIndexEditor.ID);
			voceditor.initializeFields();
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
	}
}
