package org.txm.queryindex.core.functions;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;


/**
 * QueryIndex messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class Messages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.queryindex.core.functions.messages"; //$NON-NLS-1$

	public static String QueryIndexEditor_0;

	public static String QueryIndexEditor_1;

	public static String QueryIndexEditor_10;

	public static String QueryIndexEditor_11;

	public static String QueryIndexEditor_12;

	public static String error_QueryIndexEditor_2;

	public static String QueryIndexEditor_3;

	public static String QueryIndexEditor_4;

	public static String error_QueryIndexEditor_6;

	public static String QueryIndexEditor_7;

	public static String QueryIndexEditor_8;

	public static String QueryIndexEditor_9;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
