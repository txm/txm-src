package org.txm.queryindex.core.functions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.index.core.messages.IndexCoreMessages;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.lexicaltable.core.statsengine.r.data.LexicalTableImpl;
import org.txm.rcp.IImageKeys;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.Selection;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.core.messages.CQPSearchEngineCoreMessages;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Part;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

public class QueryIndex extends TXMResult implements IAdaptable {

	@Parameter(key = TXMPreferences.QUERIES)
	LinkedHashMap<String, IQuery> queries;

	// stores result
	LinkedHashMap<String, QueryIndexLine> lines = new LinkedHashMap<>();

	public int getT() throws Exception {

		int t = 0;
		for (QueryIndexLine line : lines.values()) {
			t += line.getFrequency();
		}
		return t;
	}

	public int getFmin() throws Exception {

		int t = Integer.MAX_VALUE;
		for (QueryIndexLine line : lines.values()) {
			int f = line.getFrequency();
			if (f < t)
				t = f;
		}
		return t;
	}

	public int getFmax() throws Exception {

		int t = 0;
		for (QueryIndexLine line : lines.values()) {
			int f = line.getFrequency();
			if (f > t)
				t = f;
		}
		return t;
	}

	public int getV() {
		return lines.values().size();
	}

	public QueryIndex(String id) {

		super(id, null);
	}

	public QueryIndex(String id, CQPCorpus corpus) {

		super(id, corpus);
	}

	public QueryIndex(CQPCorpus corpus) {

		super(corpus);
	}

	@Override
	public String getName() {

		return getParent().getName() + (queries != null ? queries.toString() : "");
	}

	public QueryIndex(Partition partition) {

		super(partition);
	}

	public CQPCorpus getCorpus() {

		if (getParent() instanceof Partition) {
			return ((Partition) getParent()).getCorpus();
		}
		return (CQPCorpus) getParent();
	}

	public Partition getPartition() {

		if (getParent() instanceof Partition) {
			return ((Partition) getParent());
		}
		return null;
	}

	public Collection<QueryIndexLine> getLines() {

		return lines.values();
	}

	public LinkedHashMap<String, QueryIndexLine> getLinesHash() {
		return lines;
	}

	// public void filterLines(int fmin, int fmax) {
	// System.out.println("filter lines fmin="+fmin +" fmax="+fmax );
	// this.FilterFmin = fmin;
	// this.FilterFmax = fmax;
	// }
	int multi = 1;

	public void sortLines(int ncol, boolean revert) {

		multi = 1;
		if (revert)
			multi = -1;
		List<Map.Entry<String, QueryIndexLine>> entries = new ArrayList<>(lines.entrySet());

		if (ncol != -1) {
			Collections.sort(entries, new Comparator<Map.Entry<String, QueryIndexLine>>() {

				@Override
				public int compare(Map.Entry<String, QueryIndexLine> a, Map.Entry<String, QueryIndexLine> b) {
					int ret = 0;
					try {
						ret = multi * (a.getValue().getFrequency() - b.getValue().getFrequency());
					}
					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (ret == 0) {
						return multi * a.getValue().getName().compareTo(b.getValue().getName());
					}
					return ret;
				}
			});
		}
		else if (ncol == -1) {
			Collections.sort(entries, new Comparator<Map.Entry<String, QueryIndexLine>>() {

				@Override
				public int compare(Map.Entry<String, QueryIndexLine> a, Map.Entry<String, QueryIndexLine> b) {
					int ret = multi * a.getValue().getName().compareTo(b.getValue().getName());
					if (ret == 0) {
						try {
							return multi * (a.getValue().getFrequency() - b.getValue().getFrequency());
						}
						catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					return ret;
				}
			});
		}

		LinkedHashMap<String, QueryIndexLine> sortedMap = new LinkedHashMap<>();
		for (Map.Entry<String, QueryIndexLine> entry : entries) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		lines = sortedMap;
	}

	public boolean isComputedWithPartition() {

		return getParent() instanceof Partition;
	}

	public void addLinesFromFile(File propFile) throws Exception {

		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(propFile), "UTF-8")); //$NON-NLS-1$

		String line = reader.readLine();
		while (line != null) {
			String[] split = line.split("=", 2); //$NON-NLS-1$
			if (split.length == 2) {
				if (hasLine(split[0])) {
					Log.finest(TXMCoreMessages.bind(TXMCoreMessages.warningColonDuplicateQueryEntryColonP0, line));
				}
				else {
					if (addLine(split[0], new CQLQuery(split[1])) == null) {
						Log.finest(TXMCoreMessages.bind(TXMCoreMessages.warningColonQueryFailedColonP0, line));
					}
				}
			}
			line = reader.readLine();
		}
		reader.close();
	}

	/**
	 * Write all the lines on a writer.
	 *
	 * @param outfile the outfile
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @return true, if successful
	 */
	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) {

		try {
			toTxt(outfile, 0, lines.size(), encoding, colseparator, txtseparator);
		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.bind(IndexCoreMessages.error_failedToExportLexiconColonP0, Log.toString(e)));
			return false;
		}
		return true;
	}

	/**
	 * Write the lines between from and to on a writer.
	 *
	 * @param outfile the outfile
	 * @param from The first line to be written
	 * @param to The last line to be writen
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @throws Exception
	 */
	public void toTxt(File outfile, int from, int to, String encoding, String colseparator, String txtseparator)
			throws Exception {

		// NK: writer declared as class attribute to perform a clean if the operation is
		// interrupted
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(outfile), encoding);
		String header = "Queries"; //$NON-NLS-1$
		header = txtseparator + header.substring(0, header.length() - 1) + txtseparator;
		header += colseparator + txtseparator + "F" + txtseparator; //$NON-NLS-1$

		List<String> partnames = getPartnames();
		if (partnames.size() > 1)
			for (int j = 0; j < partnames.size(); j++)
			header += colseparator + txtseparator
					+ partnames.get(j).replace(txtseparator, txtseparator + txtseparator) + txtseparator;
		header += "\n"; //$NON-NLS-1$
		writer.write(header);

		// for(Line line: lines)
		for (String name : lines.keySet()) {
			QueryIndexLine ligne = lines.get(name);
			writer.write(txtseparator + ligne.getName().replace(txtseparator, txtseparator + txtseparator)
					+ txtseparator + colseparator + ligne.getFrequency());
			if (partnames.size() > 1)
				for (int j = 0; j < partnames.size(); j++)
				writer.write(colseparator + ligne.getFrequency(j));
			writer.write("\n"); //$NON-NLS-1$
		}
		writer.flush();
		writer.close();
	}

	public List<String> getPartnames() {

		List<String> partnames = null;
		if (getParent() instanceof Partition) {
			partnames = ((Partition) getParent()).getPartNames();
		}
		else {
			partnames = Arrays.asList(getParent().getName());
		}
		return partnames;
	}

	public QueryIndexLine addLine(String name, IQuery query) throws Exception {

		if (lines.containsKey(name)) {
			return lines.get(name);
		}

		Selection[] qresults;
		if (getParent() instanceof Partition) {
			Partition partition = (Partition) getParent();
			qresults = new Selection[partition.getPartsCount()];
			List<Part> parts = partition.getParts();
			for (int i = 0; i < parts.size(); i++) {

				qresults[i] = query.getSearchEngine().query(getCorpus(), query, "tmp", false);

				//qresults[i] = parts.get(i).query(query, "tmp", true); //$NON-NLS-1$
				// qresults[i].drop(); // free mem done later
			}
		}
		else {
			qresults = new Selection[1];
			qresults[0] = query.getSearchEngine().query(getCorpus(), query, "tmp", false);
			//qresults[0] = getCorpus().query(query, "tmp", true); //$NON-NLS-1$
			// qresults[0].drop(); // free mem done later
		}

		QueryIndexLine line = new QueryIndexLine(name, query, qresults);
		lines.put(name, line);
		queries.put(name, query);
		return line;
	}

	public LexicalTable toLexicalTable() throws Exception {

		if (!(getParent() instanceof Partition)) {
			return null;
		}

		Partition partition = (Partition) getParent();
		int npart = partition.getPartsCount();
		int[][] freqs = new int[lines.size()][npart];
		String[] rownames = new String[lines.size()];
		String[] colnames = new String[npart];

		int i = 0;
		for (String key : lines.keySet()) {

			QueryIndexLine line = lines.get(key);
			int[] linefreqs = line.getFreqs();
			rownames[i] = line.getName();
			for (int j = 0; j < npart; j++) {
				freqs[i][j] = linefreqs[j];
			}
			i++;
		}
		List<Part> parts = partition.getParts();
		for (int j = 0; j < npart; j++) {
			colnames[j] = parts.get(j).getName();
		}

		try {
			LexicalTableImpl lt = new LexicalTableImpl(freqs, rownames, colnames);
			LexicalTable table = new LexicalTable(partition);
			table.setParameters(partition.getCorpus().getDefaultProperty(), 1, Integer.MAX_VALUE, this.getV(), null);
			table.setData(lt);
			return table;
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
		return null;
	}

	public boolean removeLine(String name) {

		if (lines.containsKey(name)) {
			lines.remove(name);
			queries.remove(name);
			return true;
		}
		else {
			return false;
		}
	}

	public boolean hasLine(String name) {

		return queries.containsKey(name);
	}

	public ArrayList<QueryIndexLine> getLines(int from, int to) {

		if (lines.size() == 0) {
			return new ArrayList<>();
		}

		if (from < 0)
			from = 0;
		if (to < 0)
			to = 0;
		if (to > lines.size())
			to = lines.size();
		if (from > to)
			from = to - 1;
		ArrayList<QueryIndexLine> tmp = new ArrayList<>();
		int i = 0;
		for (QueryIndexLine line : lines.values()) {
			if (i >= from && i < to) {
				tmp.add(line);
			}
			i++;
		}

		return tmp;
	}

	@Override
	public void clean() {

	}

	@Override
	public Object getAdapter(Class adapterType) {

		if (adapterType == IWorkbenchAdapter.class)
			return queryindexAdapter;
		return null;
	}

	/** The QueryIndex adapter. */
	private static IWorkbenchAdapter queryindexAdapter = new IWorkbenchAdapter() {

		@Override
		public Object[] getChildren(Object o) {
			return new Object[0];
		}

		@Override
		public ImageDescriptor getImageDescriptor(Object object) {
			return IImageKeys.getImageDescriptor(IImageKeys.QUERYINDEX);
			// return AbstractUIPlugin.imageDescriptorFromPlugin("QueryIndexRCP",
			// "icons/functions/WordCloud.png"); //$NON-NLS //$NON-NLS-1$
			// return null;
		}

		@Override
		public String getLabel(Object o) {
			return ((QueryIndex) o).getName();
		}

		@Override
		public Object getParent(Object o) {
			if (((QueryIndex) o).getPartition() != null) {
				return ((QueryIndex) o).getPartition();
			}
			else {
				return ((QueryIndex) o).getCorpus();
			}
		}
	};

	@Override
	public String getSimpleName() {

		return ""; //$NON-NLS-1$
	}

	@Override
	public String getDetails() {

		return ""; //$NON-NLS-1$
	}

	@Override
	public boolean canCompute() {

		return getParent() != null && this.queries != null && this.queries.size() > 0;
	}

	@Override
	public boolean _compute(TXMProgressMonitor monitor) throws Exception {

		try {
			for (String q : queries.keySet()) {
				addLine(q, queries.get(q));
			}

			try {

				if (monitor.isCanceled()) {
					return false;
				}
				this.sortLines(0, true);

				if (monitor.isCanceled()) {
					return false;
				}
				monitor.worked(95);
			}
			catch (Exception e) {
				Log.warning(e.getLocalizedMessage());
				try {
					Log.severe(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.lastCQPErrorP0, CQPSearchEngine.getCqiClient().getLastCQPError()));
				}
				catch (Exception e1) {
					Log.severe(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.cQPErrorColonP0, e1));
					Log.printStackTrace(e1);
				}
				Log.severe(TXMCoreMessages.bind("Error while computing QueryIndex: {0}.", e.getLocalizedMessage()));
				return false;
			}
			return true;

		}
		catch (Exception e1) {
			Log.printStackTrace(e1);
			return false;
		}
	}

	@Override
	public boolean saveParameters() throws Exception {

		try {
			if (queries == null) return true;

			//			Gson gson = new Gson();
			//			HashMap<String, Object> values = new HashMap<>();
			//			LinkedTreeMap<String, String> serialized = new LinkedTreeMap<String, String>();
			//			for (String k : queries.keySet()) {
			//				serialized.put(k, Query.lol(queries.get(k)));
			//			}
			//			values.put("queries", serialized);
			this.saveParameter(TXMPreferences.QUERIES, Query.queriesToString(queries.values()));

		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean loadParameters() throws Exception {

		try {
			String s = this.getStringParameterValue(TXMPreferences.QUERIES);
			queries = new LinkedHashMap<String, IQuery>();

			if (s != null && s.length() > 0) {
				Gson gson = new Gson();
				HashMap<?, ?> objects = gson.fromJson(s, HashMap.class);
				LinkedTreeMap<?, ?> serialized = (LinkedTreeMap<?, ?>) objects.get("queries");

				for (Object k : serialized.keySet()) {
					queries.put(k.toString(), Query.stringToQuery(serialized.get(k).toString()));
				}
			}
		}
		catch (Exception e) {
			Log.warning(e);
			return false;
		}
		return true;
	}

	@Override
	public String getResultType() {

		return "Query Index"; //$NON-NLS-1$
	}

	public Collection<IQuery> getQueries() {
		
		return queries.values();
	}
}
