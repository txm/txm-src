// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.treetagger.rcp.preferences;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.commands.OpenBrowser;
import org.txm.rcp.jface.DoubleFieldEditor;
import org.txm.rcp.preferences.RCPPreferencesPage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.rcp.swt.widget.preferences.DirectoryFieldEditor;
import org.txm.rcp.swt.widget.preferences.FileFieldEditor;
import org.txm.rcp.swt.widget.preferences.IntegerFieldEditor;
import org.txm.rcp.utils.JobHandler;
import org.txm.treetagger.core.preferences.TreeTaggerPreferences;
import org.txm.treetagger.rcp.messages.TreeTaggerUIMessages;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

/**
 * TreeTagger preferences page.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class TreeTaggerPreferencePage extends RCPPreferencesPage {

	private TableViewer modelsTable;

	private TableViewerColumn documentationColumn;

	private TableViewerColumn nameColumn;

	private TableViewerColumn checkColumn;

	private TableViewerColumn langColumn;

	private String rawList;

	private HashMap<File, String[]> modelsInfo;

	private LinkedHashMap<File, Boolean> installedModels;

	public final String MODELRESSOURCES = "https://gitlab.huma-num.fr/txm/txm-ressources/-/raw/master/treetagger/"; //$NON-NLS-1$

	@Override
	public void createFieldEditors() {

		this.addField(new DirectoryFieldEditor(TreeTaggerPreferences.INSTALL_PATH, TreeTaggerUIMessages.pathToInstallDirectory, this.getFieldEditorParent()));
		this.addField(new DirectoryFieldEditor(TreeTaggerPreferences.MODELS_PATH, TreeTaggerUIMessages.pathToModelsDirectory, this.getFieldEditorParent()));

		Label l = new Label(this.getFieldEditorParent(), SWT.SEPARATOR | SWT.HORIZONTAL);
		l.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 3, 1));

		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {

			l = new Label(this.getFieldEditorParent(), SWT.NONE);
			l.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 3, 1));
			l.setText(TreeTaggerUIMessages.preferencePage_treeTaggerModelsManagement);
			
			modelsTable = new TableViewer(this.getFieldEditorParent(), SWT.CHECK | SWT.H_SCROLL);

			GridData gdata = new GridData(GridData.FILL, GridData.CENTER, true, false, 3, 1);
			gdata.minimumHeight = 100;
			gdata.heightHint = 200;
			modelsTable.getTable().setLayoutData(gdata);
			modelsTable.setContentProvider(new ArrayContentProvider());
			modelsTable.getTable().setLinesVisible(true);
			modelsTable.getTable().setHeaderVisible(true);

			modelsTable.getTable().addMouseListener(new MouseListener() {

				@Override
				public void mouseUp(MouseEvent e) {
				}

				@Override
				public void mouseDown(MouseEvent e) {
				}

				@Override
				public void mouseDoubleClick(MouseEvent e) {
					int sel = modelsTable.getTable().getSelectionIndex();

					String doc = modelsTable.getTable().getItems()[sel].getText(4);
					if (doc != null && doc.length() > 0) {
						OpenBrowser.openExternalBrowser(doc);
					}
				}
			});
			modelsTable.getTable().setToolTipText("Install or update TreeTagger models. Check/Uncheck model lines to install or uninstall then apply changes.");

			TableViewerColumn firstColumn = new TableViewerColumn(modelsTable, SWT.NONE);
			firstColumn.setLabelProvider(new ColumnLabelProvider() {

				@Override
				public String getText(Object element) {
					return "";
				}
			});
			checkColumn = new TableViewerColumn(modelsTable, SWT.CHECK);
			checkColumn.getColumn().setText("I/U");
			checkColumn.setLabelProvider(new ColumnLabelProvider() {

				@Override
				public String getText(Object element) {
					return null;
				}

				@Override
				public Image getImage(Object element) {
					if (installedModels.get(element)) {
						if (((File) element).exists()) {
							return IImageKeys.getImage(IImageKeys.CHECKEDBOX_SELECTED);
						}
						else {
							return IImageKeys.getImage(IImageKeys.CHECKEDBOX_EXPAND);
						}
					}
					else {
						if (((File) element).exists()) {
							return IImageKeys.getImage(IImageKeys.CHECKEDBOX_UNSELECTED);
						}
						else {
							return IImageKeys.getImage(IImageKeys.CHECKEDBOX_CLEARED);
						}
					}
				}
			});
			checkColumn.setEditingSupport(new InstallEditingSupport(modelsTable));

			langColumn = new TableViewerColumn(modelsTable, SWT.NONE);
			langColumn.getColumn().setText("Language code");
			langColumn.setLabelProvider(new ColumnLabelProvider() {

				@Override
				public String getText(Object element) {
					return modelsInfo.get(element)[0];
				}
			});

			nameColumn = new TableViewerColumn(modelsTable, SWT.NONE);
			nameColumn.getColumn().setText("Model name");
			nameColumn.setLabelProvider(new ColumnLabelProvider() {

				@Override
				public String getText(Object element) {
					return modelsInfo.get(element)[2];
				}
			});

			documentationColumn = new TableViewerColumn(modelsTable, SWT.NONE);
			documentationColumn.getColumn().setText("Documentation");
			documentationColumn.setLabelProvider(new ColumnLabelProvider() {

				@Override
				public String getText(Object element) {
					return modelsInfo.get(element)[3];
				}
			});

			TableViewerColumn lastColumn = new TableViewerColumn(modelsTable, SWT.NONE);
			lastColumn.setLabelProvider(new ColumnLabelProvider() {

				@Override
				public String getText(Object element) {
					return "";
				}
			});
			firstColumn.getColumn().setWidth(0);
			checkColumn.getColumn().setWidth(50);
			langColumn.getColumn().setWidth(120);
			nameColumn.getColumn().setWidth(100);
			documentationColumn.getColumn().setWidth(100);
			lastColumn.getColumn().setWidth(0);

			GLComposite buttonsComposite = new GLComposite(getFieldEditorParent(), SWT.NONE, "buttons");
			buttonsComposite.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 3, 1));
			buttonsComposite.getLayout().numColumns = 10;

			Button openModelsListButton = new Button(buttonsComposite, SWT.PUSH);
			openModelsListButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, true, false));
			openModelsListButton.setText("Open models directory");
			openModelsListButton.setToolTipText("Open the models directory in the system default file navigator.");
			openModelsListButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {

					File modelsDir = new File(TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.MODELS_PATH));
					if (!modelsDir.exists()) {
						Log.warning("Model directory not found: " + modelsDir);
						Log.warning("	Set the model directory preference and try again");
						modelsTable.setInput(null);
						return;
					}

					Program.launch(modelsDir.getAbsolutePath());
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			Button refreshModelsListButton = new Button(buttonsComposite, SWT.PUSH);
			refreshModelsListButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			refreshModelsListButton.setText("1- Fetch models list");
			refreshModelsListButton.setToolTipText("Load models from TXM catalogue.");
			refreshModelsListButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {

					Log.info("Fetch models list...");
					fetchModelsList();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			l = new Label(buttonsComposite, SWT.NONE);
			l.setText("2- change the models selection.");

			Button applyChanges = new Button(buttonsComposite, SWT.PUSH);
			applyChanges.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			applyChanges.setText("3- Apply changes");
			applyChanges.setToolTipText("Select the models to install or uninstall, then press this button to make the changes.");
			applyChanges.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {

					File modelsDir = new File(TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.MODELS_PATH));
					if (!modelsDir.exists()) {
						Log.warning("Model directory not found: " + modelsDir);
						Log.warning("	Set the model directory preference and try again");
						modelsTable.setInput(null);
						return;
					}

					Log.info("Install&Uninstall models to " + modelsDir + "...");
					ArrayList<File> todo = new ArrayList<File>();
					LinkedHashMap<File, Exception> errors = new LinkedHashMap<File, Exception>();
					String todoString = "";
					for (File m : installedModels.keySet()) {

						if (installedModels.get(m)) { // INSTALL
							if (m.exists()) {
								// nothing to do
							}
							else {
								Log.info("	+ " + modelsInfo.get(m)[2]);
								todoString += "\n	+ " + modelsInfo.get(m)[2];
								todo.add(m);
							}
						}
						else {  // UNINSTALL
							if (m.exists()) {
								Log.info("	- " + modelsInfo.get(m)[2]);
								todoString += "\n	- " + modelsInfo.get(m)[2];
								todo.add(m);
							}
							else {
								// nothing to do
							}
						}
					}


					if (todo.size() == 0) {
						Log.info("Nothing to do.");
					}

					if (!MessageDialog.openQuestion(getShell(), "Updating model files", "Install and remove the following models? \n" + todoString
							+ "\n\nClick on 'YES' to accept the TreeTagger licence and update files (https://cis.uni-muenchen.de/~schmid/tools/TreeTagger/Tagger-Licence)..")) {
						Log.info("Cancel update.");
						return;
					}

					JobHandler job = new JobHandler("Updating model files") {

						@Override
						protected IStatus _run(SubMonitor monitor) {

							ConsoleProgressBar cpb = new ConsoleProgressBar(todo.size());
							for (File m : todo) {
								cpb.tick();
								if (m.exists()) {
									if (!m.delete()) { // \o/
										errors.put(m, new IllegalStateException("Could not delete file: " + m));
									}
								}
								else {
									try {
										IOUtils.download(new URL(MODELRESSOURCES + "/" + modelsInfo.get(m)[1]), new File(modelsDir, m.getName())); //$NON-NLS-1$
									}
									catch (Exception e1) {
										errors.put(m, e1);
									}
								}
							}
							cpb.done();

							if (errors.size() > 0) {
								Log.warning("Errors: ");
								for (File m : errors.keySet()) {
									Log.warning("	" + m.getName() + ": " + errors.get(m));
								}
								return Status.CANCEL_STATUS;
							}

							syncExec(new Runnable() {

								@Override
								public void run() {
									fetchModelsList();
								}
							});

							return Status.OK_STATUS;
						}
					};
					job.setUser(true);
					job.schedule();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			l = new Label(this.getFieldEditorParent(), SWT.SEPARATOR | SWT.HORIZONTAL);
			l.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 10, 1));
		}
		this.addField(new BooleanFieldEditor(TreeTaggerPreferences.OPTIONS_DEBUG, TreeTaggerUIMessages.activateTreeTaggerDebugMessages, this.getFieldEditorParent()));

		// TREETAGGER TAGGER OPTIONS
		Group runOptions = new Group(this.getFieldEditorParent(), SWT.BORDER);
		runOptions.setText(TreeTaggerUIMessages.treeTaggerOptions);
		GridData gdata2 = new GridData(GridData.FILL, GridData.FILL, true, false, 3, 1);
		runOptions.setLayoutData(gdata2);
		runOptions.setLayout(new GridLayout(3, false));

		BooleanFieldEditor field = new BooleanFieldEditor(TreeTaggerPreferences.OPTIONS_UNKNOWN, TreeTaggerUIMessages.wordFormAsLemmaForUnknownLemma, runOptions);
		field.setToolTipText(TreeTaggerUIMessages.useTheWordFormAsLemmaRatherThanEtc);
		this.addField(field);
		
		field = new BooleanFieldEditor(TreeTaggerPreferences.OPTIONS_HYPHENHEURISTIC, TreeTaggerUIMessages.unknownHyphenatedWordsPartOfSpeechHeuristic, runOptions);
		field.setToolTipText(TreeTaggerUIMessages.useAHeuristicToDetermineThePartOfSpeechOfUnknownHyphenatedWords);
		this.addField(field);
		
		field = new BooleanFieldEditor(TreeTaggerPreferences.OPTIONS_PROB, TreeTaggerUIMessages.partOfSpeechProbability, runOptions);
		field.setToolTipText(TreeTaggerUIMessages.addTheProbabilityOfThePartOfSpeechAsAProperty);
		this.addField(field);
		
		FileFieldEditor fField = new FileFieldEditor(TreeTaggerPreferences.OPTIONS_LEX, TreeTaggerUIMessages.externalLexiconFile, runOptions);
		fField.setToolTipText(TreeTaggerUIMessages.externalLexiconFileEtc);
		this.addField(fField);
		fField = new FileFieldEditor(TreeTaggerPreferences.OPTIONS_WC, TreeTaggerUIMessages.wordClassAutomatonFile, runOptions);
		fField.setToolTipText(TreeTaggerUIMessages.grammaticalWordClassAutomatonFile);
		this.addField(fField);

		runOptions.setLayout(new GridLayout(3, false));

		// TREETAGGER TRAINING OPTIONS
		Group runTrainOptions = new Group(this.getFieldEditorParent(), SWT.BORDER);
		runTrainOptions.setText(TreeTaggerUIMessages.treeTaggerTrainingOptions);
		gdata2 = new GridData(GridData.FILL, GridData.FILL, true, false, 3, 1);
		runTrainOptions.setLayoutData(gdata2);
		runTrainOptions.setLayout(new GridLayout(3, false));

		IntegerFieldEditor iField = new IntegerFieldEditor(TreeTaggerPreferences.OPTIONS_CL, TreeTaggerUIMessages.leftContextSize, runTrainOptions);
		iField.setToolTipText(TreeTaggerUIMessages.useAContextOfWords);
		this.addField(iField);
		
		DoubleFieldEditor dField = new DoubleFieldEditor(TreeTaggerPreferences.OPTIONS_DTG, TreeTaggerUIMessages.minimumDescisionTreeGain, runTrainOptions);
		this.addField(dField);
		
		dField = new DoubleFieldEditor(TreeTaggerPreferences.OPTIONS_SW, TreeTaggerUIMessages.smoothingWeight, runTrainOptions);
		this.addField(dField);
		
		dField = new DoubleFieldEditor(TreeTaggerPreferences.OPTIONS_ATG, TreeTaggerUIMessages.minimumAffixTreeGain, runTrainOptions);
		this.addField(dField);
		
		dField = new DoubleFieldEditor(TreeTaggerPreferences.OPTIONS_ECW, TreeTaggerUIMessages.equivalenceClassWeight, runTrainOptions);
		this.addField(dField);
		
		dField = new DoubleFieldEditor(TreeTaggerPreferences.OPTIONS_LT, TreeTaggerUIMessages.thresholdProbabiblityForLexicalEntries, runTrainOptions);
		dField.setToolTipText(TreeTaggerUIMessages.thresholdProbabiblityForLexicalEntriesEtc);
		this.addField(dField);

		runTrainOptions.setLayout(new GridLayout(3, false));
	}

	protected void fetchModelsList() {

		File modelsDir = new File(TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.MODELS_PATH));
		if (!modelsDir.exists()) {
			Log.warning("Model directory not found: " + modelsDir);
			Log.warning("	Set the model directory preference and try again");
			modelsTable.setInput(null);
			return;
		}

		ArrayList<File> files = new ArrayList<File>();
		modelsInfo = new HashMap<File, String[]>();
		installedModels = new LinkedHashMap<File, Boolean>();

		// https://gitlab.huma-num.fr/txm/txm-ressources/-/raw/master/treetagger/models
		if (rawList == null) {

		}
		try {
			rawList = IOUtils.getText(new URL(MODELRESSOURCES + "models"), "UTF-8"); //$NON-NLS-1$
			
			String[] lines = rawList.split("\n"); //$NON-NLS-1$
			for (String line : lines) {
				line = line.trim();
				if (line.length() == 0) continue;
				if (line.startsWith("#")) continue; //$NON-NLS-1$

				String[] split = line.split("\t", 4); //$NON-NLS-1$
				if (split.length != 4) continue;
				if (split[0] == null) continue;
				if (split[1] == null) continue;
				if (split[2] == null) continue;

				File m = new File(modelsDir, split[0] + ".par"); //$NON-NLS-1$
				files.add(m);
				modelsInfo.put(m, split);
				installedModels.put(m, m.exists());
			}
			modelsTable.setInput(files.toArray());
		}
		catch (Exception e) {
			e.printStackTrace();
			modelsTable.setInput(null);
		}
	}

	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(TreeTaggerPreferences.getInstance().getPreferencesNodeQualifier()));
	}

	public class InstallEditingSupport extends EditingSupport {

		private final TableViewer viewer;

		public InstallEditingSupport(TableViewer viewer) {
			super(viewer);
			this.viewer = viewer;
		}

		@Override
		protected CellEditor getCellEditor(Object element) {
			return new CheckboxCellEditor(null, SWT.CHECK | SWT.READ_ONLY);

		}

		@Override
		protected boolean canEdit(Object element) {
			return true;
		}

		@Override
		protected Object getValue(Object element) {
			return installedModels.get(element);
		}

		@Override
		protected void setValue(Object element, Object value) {
			installedModels.put((File) element, (Boolean) value);
			viewer.update(element, null);
		}
	}

}
