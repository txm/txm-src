package org.txm.treetagger.rcp.handlers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.kohsuke.args4j.Option;
import org.txm.Toolbox;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.DeleteDir;
import org.txm.utils.Tuple;
import org.txm.utils.io.FileCopy;
import org.txm.utils.io.IOUtils;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class LemmaProjection extends AbstractHandler {

	protected static final String EXTRA = "extra";

	@Option(name = "dictionary", usage = "TSV Dictionary file with form, msd, lemma, source columns", widget = "File", required = true, def = "frolex.tsv")
	public File dictionary = null;

	@Option(name = "extrarules", usage = "form+pos rules files", widget = "File", required = false, def = "extrarules.tsv")
	public File extrarules = null;

	@Option(name = "posproperty", usage = "The lexicon property to read", widget = "String", required = true, def = "frpos")
	public String posproperty = null;

	@Option(name = "lemmaproperty", usage = "The property to create/update in the corpus", widget = "String", required = true, def = "plemma")
	public String lemmaproperty = null;

	@Option(name = "formAsLemmaPosList", usage = "Pos values lemma exceptions", widget = "String", required = false, def = "NOMPro")
	public String formAsLemmaPosList = null;

	@Option(name = "sourcePriorityList", usage = "The property to create/update in the corpus", widget = "String", required = true, def = "TL")
	public String sourcePriorityList = null;

	/**
	 * 
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		CQPCorpus corpus = null;
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);

		ISelection isel = window.getActivePage().getSelection();
		if (isel instanceof IStructuredSelection) {
			IStructuredSelection sel = (IStructuredSelection) isel;
			Object first = sel.getFirstElement();
			if (first instanceof CQPCorpus) {
				corpus = (CQPCorpus) first;
				if (ParametersDialog.open(this)) {
					LinkedHashSet<String> formAsLemmaPosSet = new LinkedHashSet<String>();
					formAsLemmaPosSet.addAll(Arrays.asList(formAsLemmaPosList.split(",")));
					LinkedHashSet<String> sourcePrioritySet = new LinkedHashSet<String>();
					if (extrarules != null && extrarules.exists()) sourcePrioritySet.add(EXTRA); // extra must be the first source
					sourcePrioritySet.addAll(Arrays.asList(sourcePriorityList.split(",")));

					System.out.println("formAsLemmaPosSet=" + formAsLemmaPosSet);
					System.out.println("sourcePrioritySet=" + sourcePrioritySet);
					apply(corpus, dictionary, extrarules, posproperty, lemmaproperty, formAsLemmaPosSet, sourcePrioritySet);
					return corpus;
				}
			}
		}

		System.out.println("Wrong selection.");
		return null;
	}

	public static void apply(final CQPCorpus corpus, final File dictionary, final File extrarules, final String posproperty,
			final String targetproperty, final LinkedHashSet<String> formAsLemmaPosList, final LinkedHashSet<String> sourceprioritylist) {
		final MainCorpus mainCorpus = corpus.getMainCorpus();
		final File corpusBinaryDirectory = mainCorpus.getProjectDirectory();
		final File txmDirectory = new File(corpusBinaryDirectory, "txm/" + mainCorpus.getID());

		if (!txmDirectory.exists()) {
			System.out.println("Can't process a corpus with no XML-TXM files directory: " + txmDirectory);
			return;
		}

		final File[] files = txmDirectory.listFiles(new FileFilter() {

			@Override
			public boolean accept(File file) {
				return file.isFile() && file.canWrite() && file.getName().endsWith(".xml"); //$NON-NLS-1$
			}
		});

		Property pos = null;
		try {
			pos = mainCorpus.getProperty(posproperty);
		}
		catch (CqiClientException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (pos == null) {
			System.out.println("No pos property found with name=" + posproperty);
			return;
		}

		if (files == null || files.length == 0) {
			System.out.println("Can't process a corpus with no XML-TXM files in " + txmDirectory);
			return;
		}

		System.out.println("APPLYING : " + dictionary + " to " + mainCorpus + ": creating/updating " + targetproperty + " property with lexicon " + dictionary);
		JobHandler job = new JobHandler("Creating/Updating " + targetproperty + " property.") {

			@Override
			protected IStatus _run(SubMonitor monitor) throws Exception {

				Tuple t;
				LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>> rules = new LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>>();
				HashSet<String> formAsLemmaPosSet = new HashSet<String>();

				// load rules
				BufferedReader reader = IOUtils.getReader(dictionary);
				String line = reader.readLine();
				while (line != null) {
					String[] splitTab = line.split("\t");
					if (splitTab.length != 5) {
						System.out.println("Error in dictionary files with line='" + line + "': length is not 5. Found: " + Arrays.toString(splitTab));
						line = reader.readLine();
						reader.close();
						return Status.CANCEL_STATUS;
					}
					String form = splitTab[0];
					String pos = splitTab[1];// .replace("<no_pos>|", "").replace("|<no_pos>|", "").replace("|<no_pos>", "");
					String lemma = splitTab[2];// .replace("<no_lemma>|", "").replace("|<no_lemma>|", "").replace("|<no_lemma>", "");
					String source = splitTab[3];

					if (!rules.containsKey(form)) rules.put(form, new LinkedHashMap<String, LinkedHashMap<String, String>>());
					LinkedHashMap<String, LinkedHashMap<String, String>> posHash = rules.get(form);

					if (!lemma.equals("<no_lemma>")) {
						if (!posHash.containsKey(pos)) posHash.put(pos, new LinkedHashMap<String, String>());
						LinkedHashMap<String, String> sourceHash = posHash.get(pos);

						sourceHash.put(source, lemma);
					}
					line = reader.readLine();
				}
				reader.close();
				System.out.println("Dictionary rules loaded: " + rules.size());

				if (extrarules.exists()) {
					reader = IOUtils.getReader(extrarules);
					line = reader.readLine();
					while (line != null) {
						String[] splitTab = line.split("\t");
						if (splitTab.length != 3) {
							System.out.println("Error in extra rule files with line='" + line + "': length is not 3.");
							line = reader.readLine();
							continue;
						}
						String form = splitTab[0];
						String pos = splitTab[1];
						String lemma = splitTab[2];
						if (!rules.containsKey(form)) rules.put(form, new LinkedHashMap<String, LinkedHashMap<String, String>>());
						LinkedHashMap<String, LinkedHashMap<String, String>> posHash = rules.get(form);

						if (!posHash.containsKey(pos)) posHash.put(pos, new LinkedHashMap<String, String>());
						LinkedHashMap<String, String> sourceHash = posHash.get(pos);
						sourceHash.put(EXTRA, lemma);

					}
					reader.close();
					System.out.println("Dictionary extra rules loaded: " + rules.size());
				}
				else {
					System.out.println("No extra rule loaded.");
				}

				PrintWriter writer = IOUtils.getWriter("/tmp/rules.txt"); //$NON-NLS-1$
				for (String k : rules.keySet()) {
					writer.println("FORM=" + k); //$NON-NLS-1$
					LinkedHashMap<String, LinkedHashMap<String, String>> rules2 = rules.get(k);
					for (String k2 : rules2.keySet()) {
						writer.println(" POS=" + k2); //$NON-NLS-1$
						LinkedHashMap<String, String> rules3 = rules2.get(k2);
						for (String k3 : rules3.keySet()) {
							writer.println("  SOURCE=" + k3); //$NON-NLS-1$
							String ls2 = rules3.get(k3);
							writer.println("   LEMMA=" + ls2); //$NON-NLS-1$
						}
					}
				}
				writer.close();
				System.out.println("RULE DUMP: /tmp/rules.txt");

				// load rules
				for (String s : formAsLemmaPosList) {
					formAsLemmaPosSet.add(s);
				}
				System.out.println("POS exception rules loaded: " + formAsLemmaPosSet.size());

				// save previous version of XML-TXM files
				File previousXMLTXMDirectory = new File(txmDirectory.getAbsolutePath() + "_previous");
				DeleteDir.deleteDirectory(previousXMLTXMDirectory);
				FileCopy.copyFiles(txmDirectory, previousXMLTXMDirectory);

				// work
				File noMatchsFile = new File(Toolbox.getTxmHomePath(), "results/nomatch.txt");
				HashSet<String> noMatchsSet = new HashSet<String>();
				ConsoleProgressBar cpb = new ConsoleProgressBar(files.length);
				for (File xmlFile : files) {
					cpb.tick();
					XMLLemmaProjection p = new XMLLemmaProjection(xmlFile, rules, formAsLemmaPosSet, sourceprioritylist, posproperty, targetproperty);
					File tmpFile = new File(xmlFile.getParentFile(), "tmp_" + xmlFile.getName());
					if (p.process(tmpFile)) {
						if (xmlFile.delete() && tmpFile.renameTo(xmlFile)) {
							// ok
						}
						else {
							System.out.println("Error during lemma projection: can't replace XML-TXM file: " + xmlFile);
							return Status.CANCEL_STATUS;
						}
					}
					else {
						System.out.println("Error during lemma projection. Aborting.");
						return Status.CANCEL_STATUS;
					}
					if (p.getNoMatchValues().size() > 0) {
						System.out.println("No matchs found with file " + xmlFile.getName() + ": " + p.getNoMatchValues());
						noMatchsSet.addAll(p.getNoMatchValues());
					}
				}

				if (noMatchsSet.size() > 0) {
					System.out.println("Missing lemma values report saved in: " + noMatchsFile);
					IOUtils.write(noMatchsFile, StringUtils.join(noMatchsSet, "\n"));
				}

				cpb.done();
				monitor.worked(50);

				System.out.println("Done.");
				return Status.OK_STATUS;
			}
		};
		job.schedule();
	}
}
