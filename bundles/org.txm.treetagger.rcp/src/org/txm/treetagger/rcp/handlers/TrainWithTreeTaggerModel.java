package org.txm.treetagger.rcp.handlers;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.kohsuke.args4j.Option;
import org.txm.index.core.functions.Index;
import org.txm.index.core.functions.Line;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.treetagger.core.preferences.TreeTaggerPreferences;
import org.txm.utils.DeleteDir;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;
import org.txm.utils.treetagger.TreeTagger;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class TrainWithTreeTaggerModel extends AbstractHandler {

	public CQPCorpus corpus = null;

	@Option(name = "pos_property", usage = "The pos property", widget = "String", required = true, def = "frpos")
	public String pos_property = null;

	@Option(name = "pos_property_sentence_value", usage = "The pos property value coding the end fo sentence", widget = "String", required = true, def = "SENT")
	public String pos_property_sentence_value = null;

	@Option(name = "lemma_property", usage = "The lemma property", widget = "String", required = true, def = "frlemma")
	public String lemma_property = null;

	@Option(name = "lexicon_file", usage = "Optional Lexicon file.", widget = "File", required = false, def = "")
	public File lexicon_file = null;

	@Option(name = "open_class_file", usage = "openclassfile file", widget = "File", required = false, def = "")
	public File open_class_file = null;

	@Option(name = "options", usage = "TreeTagger supplementary options", widget = "String", required = true, def = "")
	public String options = null;
	
	@Option(name = "model_file", usage = "The model file to create name format is <2 letters code of the lang>+ '.par'", widget = "CreateFile", required = true, def = "")
	public File model_file = null;

	/**
	 * 
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {


		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);

		ISelection isel = window.getActivePage().getSelection();
		if (isel instanceof IStructuredSelection) {
			IStructuredSelection sel = (IStructuredSelection) isel;
			Object first = sel.getFirstElement();
			if (first instanceof CQPCorpus) {
				corpus = (CQPCorpus) first;
				if (ParametersDialog.open(this)) {

					train(corpus, model_file, lexicon_file, open_class_file, new String[] { pos_property, lemma_property }, pos_property_sentence_value, options.split("  ")); //$NON-NLS-1$

					return corpus;
				}
			}
		}

		System.out.println("Wrong selection.");
		return null;
	}

	public static void train(final CQPCorpus corpus, final File model, final File lexique, final File openclassfile, final String[] properties, final String posSentenceTagValue, final String[] options) {

		JobHandler job = new JobHandler("Applying TreeTagger to " + corpus + " corpus.") {

			@Override
			protected IStatus _run(SubMonitor monitor) throws Exception {

				try {
					File lexique2 = lexique;
					MainCorpus mainCorpus = corpus.getMainCorpus();
					File corpusBinaryDirectory = mainCorpus.getProjectDirectory();

					System.out.println("Training TreeTagger on " + corpus +  (lexique2 == null? "<no lexicon file>":" with " +lexique2) + " to create " + model + " with the following properties: " + Arrays.toString(properties)); 

					if (properties == null || properties.length != 2) {
						System.out.println("Error can't continue with selected word properties: " + Arrays.toString(properties));
						return Status.CANCEL_STATUS;
					}

					for (String p : properties) {
						Property prop = corpus.getProperty(p);
						if (prop == null) {
							System.out.println("Missing property in corpus: " + p);
							return Status.CANCEL_STATUS;
						}
					}

					Property pos = corpus.getProperty(properties[0]);
					Property lemma = corpus.getProperty(properties[1]);

					// Prepare temporary directory
					File treetaggerSrcDirectory = new File(mainCorpus.getProjectDirectory(), "treetagger"); //$NON-NLS-1$
					DeleteDir.deleteDirectory(treetaggerSrcDirectory);
					treetaggerSrcDirectory.mkdirs();

					HashMap<String, HashSet<String>> simplified_lexicon = null;
					HashMap<String, HashSet<String>> simplified_lexicon_errors = null;
					int error_counter = 0;
					// Create Lexicon file from an Index
					if (lexique2 == null || !lexique2.exists()) {
						File lexiconfile = new File(treetaggerSrcDirectory, "lexicon.txt"); //$NON-NLS-1$
						System.out.println("No lexicon file provided (" + (lexique2 == null? "<no lexicon file>":lexique2) + "). Using the corpus Index to write one: "+lexiconfile.getAbsolutePath());
						
						List<WordProperty> corpusProperties = new ArrayList<WordProperty>();
						corpusProperties.add(mainCorpus.getProperty("word")); //$NON-NLS-1$
						for (String p : properties) {
							WordProperty prop = mainCorpus.getProperty(p);
							if (prop == null) {
								System.out.println("Error, a property is missing: " + p);
								return Status.CANCEL_STATUS;
							}
							corpusProperties.add(prop);
						}
						Index index = new Index(mainCorpus);
						index.setParameters(new CQLQuery("[]"), corpusProperties, null, null, null, null); //$NON-NLS-1$
						index.compute(monitor);
						List<Line> lines = index.getAllLines();
						LinkedHashMap<String, ArrayList<String>> lex = new LinkedHashMap<String, ArrayList<String>>();
						HashMap<String, HashSet<String>> allPosValues = new HashMap<String, HashSet<String>>();
						for (Line l : lines) {
							List<List<String>> values = l.getUnitsProperties();
							String form = values.get(0).get(0);
							if (!lex.containsKey(form)) {
								ArrayList<String> pairs = new ArrayList<String>();
								HashSet<String> posValues = new HashSet<String>();

								allPosValues.put(form, posValues);
								lex.put(form, pairs);
							}
							ArrayList<String> pairs = lex.get(form);
							HashSet<String> posValues = allPosValues.get(form);
							String posValue = values.get(1).get(0);
							String lemmaValue = values.get(2).get(0);
							if (posValues.contains(posValue)) {

							}
							else {
								posValues.add(posValue);
								pairs.add(posValue);
								pairs.add(lemmaValue);
							}
						}

						BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(lexiconfile), "UTF-8")); //$NON-NLS-1$
						for (String form : lex.keySet()) {

							writer.write(form);
							boolean tab = true;
							for (String v : lex.get(form)) {
								if (tab) writer.write("\t" + v); //$NON-NLS-1$
								else writer.write(" " + v); //$NON-NLS-1$

								tab = !tab;
							}
							writer.write("\n"); //$NON-NLS-1$
						}
						writer.close();
						lexique2 = lexiconfile;
					}
					else { // diagnose lexicon content
						simplified_lexicon = new HashMap<String, HashSet<String>>();
						simplified_lexicon_errors = new HashMap<String, HashSet<String>>();
						BufferedReader reader = IOUtils.getReader(lexique2);
						String line = reader.readLine();
						while (line != null) {
							String[] split = line.split("\t", 2); //$NON-NLS-1$
							HashSet<String> posValues = new HashSet<String>();
							simplified_lexicon.put(split[0], posValues);
							for (String poslemme : split[1].split("\t")) { //$NON-NLS-1$
								String[] split2 = poslemme.split(" ", 2); //$NON-NLS-1$
								posValues.add(split2[0]);
							}
							line = reader.readLine();
						}
						reader.close();
					}


					// create TT SRC file from CWB indexes

					File ttSrcFile = new File(treetaggerSrcDirectory, mainCorpus.getID() + ".tt"); //$NON-NLS-1$
					System.out.println("TT SRC file: " + ttSrcFile.getAbsolutePath());
					BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(ttSrcFile));
					PrintStream ps = new PrintStream(fos);
					LinkedHashSet<Integer> positions = new LinkedHashSet<Integer>();
					Property word = corpus.getProperty("word"); //$NON-NLS-1$
					AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();
					for (org.txm.objects.Match m : corpus.getMatches()) {
						for (int i = m.getStart(); i <= m.getEnd(); i++) { // end match must be included
							positions.add(i);

							if (positions.size() >= 1000) { // avoid too big array
								int[] positions_array = new int[positions.size()];
								int ip = 0;
								for (int p : positions)
									positions_array[ip++] = p;
								String[] words = CQI.cpos2Str(word.getQualifiedName(), positions_array);
								String[] values = CQI.cpos2Str(pos.getQualifiedName(), positions_array);

								for (int iW = 0; iW < words.length; iW++) {
									String w = words[iW];
									if (w != null) {
										String s = w + "\t" + values[iW]; //$NON-NLS-1$
										ps.println(s);

										if (simplified_lexicon != null) { // check given lexicon
											if (simplified_lexicon.containsKey(w)) {
												if (!simplified_lexicon.get(w).contains(values[iW])) {
													// System.out.println("Lexicon error: cannot find pos="+values[iW]+" for form="+w);
													if (!simplified_lexicon_errors.containsKey(w)) simplified_lexicon_errors.put(w, new HashSet<String>());
													HashSet<String> error_values = simplified_lexicon_errors.get(w);
													error_values.add(values[iW]);
													error_counter++;
												}
											}
											else {
												// System.out.println("Lexicon error: cannot find form="+w);
												if (!simplified_lexicon_errors.containsKey(w)) simplified_lexicon_errors.put(w, new HashSet<String>());
												HashSet<String> error_values = simplified_lexicon_errors.get(w);
												error_values.add("#" + values[iW]); //$NON-NLS-1$
												error_counter++;
											}
										}
									}
								}
								positions.clear();
							}
						}
					}
					if (positions.size() > 0) { // write last words
						int[] positions_array = new int[positions.size()];
						int ip = 0;
						for (int p : positions)
							positions_array[ip++] = p;
						String[] words = CQI.cpos2Str(word.getQualifiedName(), positions_array);
						String[] values = CQI.cpos2Str(pos.getQualifiedName(), positions_array);

						for (int iW = 0; iW < words.length; iW++) {
							String w = words[iW];
							if (w != null) {
								String s = w + "\t" + values[iW]; //$NON-NLS-1$
								ps.println(s);
							}
						}
						positions.clear();
					}
					ps.close();

					if (simplified_lexicon_errors != null && simplified_lexicon_errors.size() > 0) {
						File error_file = new File(treetaggerSrcDirectory, "errors.txt"); //$NON-NLS-1$
						PrintWriter errorwriter = IOUtils.getWriter(error_file);
						int c = 0;
						System.out.println("Warning, lexicon errors (" + error_counter + ") found with words:");
						for (String w : simplified_lexicon_errors.keySet()) {
							errorwriter.println(w + "=" + simplified_lexicon_errors.get(w));
							if (c < 10) {
								System.out.println(w + "=" + simplified_lexicon_errors.get(w));
								c++;
								if (c == 10) System.out.println("... errors display is trucated, see " + error_file.getAbsolutePath());
							}
						}
						errorwriter.close();
						// System.out.println("Cannot apply train-treetagger if lexicon is missing words and pos.");
						// return Status.CANCEL_STATUS;
						File lexique3 = new File(lexique2.getParentFile(), lexique2.getName() + ".fix"); //$NON-NLS-1$
						BufferedReader reader = IOUtils.getReader(lexique2);
						PrintWriter writer = IOUtils.getWriter(lexique3);
						String line = reader.readLine();
						while (line != null) {
							String w = line.split("\t", 2)[0]; //$NON-NLS-1$

							if (simplified_lexicon_errors.containsKey(w)) {
								for (String p : simplified_lexicon_errors.get(w)) {
									if (!p.startsWith("#")) //$NON-NLS-1$
										line += ("\t" + p + " <no_lemma>"); // append missing value //$NON-NLS-1$
								}
								simplified_lexicon_errors.remove(w);
							}

							writer.println(line);
							line = reader.readLine();
						}

						// write missing words
						for (String w2 : simplified_lexicon_errors.keySet()) {
							writer.print(w2);
							for (String p : simplified_lexicon_errors.get(w2)) {
								writer.print("\t" + p + " <no_lemma>"); //$NON-NLS-1$
							}
							writer.println("");
						}

						reader.close();
						writer.close();
						System.out.println("Adding words to a temporary lexicon: " + lexique3);
						lexique2 = lexique3;
					}

					// Create open class file : contains all pos values
					File tmpopenclassfile = openclassfile;
					if (tmpopenclassfile == null || tmpopenclassfile.getName().length() == 0 || !tmpopenclassfile.exists()) {
						
						tmpopenclassfile = new File(treetaggerSrcDirectory, "openclasses.txt");
						System.out.println("No open class file provided (" + (openclassfile == null?"<none>":openclassfile)+"). Creating an empty one: "+tmpopenclassfile.getAbsolutePath());
						PrintWriter openClassFileWriter = IOUtils.getWriter(tmpopenclassfile);

						// Lexicon poslexicon = corpus.getLexicon(pos);
						// String[] posValues = poslexicon.getForms();
						// for (int iV = 0 ; iV < posValues.length ; iV++) {
						// if (iV == 0) openClassFileWriter.print(posValues[iV]);
						// else openClassFileWriter.print(" "+posValues[iV]);
						// }
						openClassFileWriter.close();
					}

					// Call treetagger-train
					if (ttSrcFile.exists() && lexique2.exists() && tmpopenclassfile.exists()) {
						System.out.println("Running treetagger-train...");
						String treetaggerBinDirectory = new File(TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.INSTALL_PATH), "bin").getAbsolutePath();
						if (!treetaggerBinDirectory.endsWith("/")) treetaggerBinDirectory += "/";

						
						if (!new File(treetaggerBinDirectory).exists()) {
							Log.warning("TreeTagger binaries not installed ("+treetaggerBinDirectory+"). Cancel.");
							return Status.CANCEL_STATUS;
						}
						
						TreeTagger tt = new TreeTagger(treetaggerBinDirectory, options);
						tt.settoken();

						// tt.setlemma();
						tt.setsgml();
						tt.setst(posSentenceTagValue);
						// tt.setproto();
						tt.setutf8();
						int cl = TreeTaggerPreferences.getInstance().getInt(TreeTaggerPreferences.OPTIONS_CL);
						if (cl > 0) {
							tt.setcl(cl);
						}

						float dtg = TreeTaggerPreferences.getInstance().getFloat(TreeTaggerPreferences.OPTIONS_DTG);
						if (dtg > 0) {
							tt.setdtg(dtg);
						}

						float sw = TreeTaggerPreferences.getInstance().getFloat(TreeTaggerPreferences.OPTIONS_SW);
						if (sw > 0) {
							tt.setsw(sw);
						}

						float atg = TreeTaggerPreferences.getInstance().getFloat(TreeTaggerPreferences.OPTIONS_ATG);
						if (atg > 0) {
							tt.setatg(atg);
						}

						float ecw = TreeTaggerPreferences.getInstance().getFloat(TreeTaggerPreferences.OPTIONS_ECW);
						if (ecw > 0) {
							tt.setecw(ecw);
						}

						float lt = TreeTaggerPreferences.getInstance().getFloat(TreeTaggerPreferences.OPTIONS_LT);
						if (lt > 0) {
							tt.setlt(lt);
						}

						if (TreeTaggerPreferences.getInstance().getBoolean(TreeTaggerPreferences.OPTIONS_DEBUG)) {
							tt.debug(true);
						}
						else {
							tt.setquiet();
						}

						tt.traintreetagger(lexique2.getAbsolutePath(), tmpopenclassfile.getAbsolutePath(), ttSrcFile.getAbsolutePath(), model.getAbsolutePath());
						System.out.println("Done: " + model.getAbsolutePath());
					}
					else {
						System.out.println("Aborting.");
					}

					return Status.OK_STATUS;
				}
				catch (Exception e) {
					System.out.println("Error while training TT: " + e);
					throw e;
				}
			}
		};
		job.schedule();
	}
}
