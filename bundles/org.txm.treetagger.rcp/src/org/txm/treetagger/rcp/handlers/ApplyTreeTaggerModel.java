package org.txm.treetagger.rcp.handlers;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.HashMap;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.kohsuke.args4j.Option;
import org.txm.importer.xmltxm.Annotate;
import org.txm.rcp.commands.workspace.UpdateCorpus;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.FileUtils;
import org.txm.utils.logger.Log;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ApplyTreeTaggerModel extends AbstractHandler {

	@Option(name = "model_file", usage = "Model file", widget = "File", required = true, def = "model.par")
	public File model_file = null;

	@Option(name = "pos_property", usage = "The pos property to add", widget = "String", required = true, def = "frpos")
	public String pos_property = null;

	@Option(name = "lemma_property", usage = "The lemma property to add", widget = "String", required = true, def = "frlemma")
	public String lemma_property = null;

	@Option(name = "options", usage = "TreeTagger supplementary options", widget = "String", required = false, def = "")
	public String options = null;

	/**
	 * 
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		CQPCorpus corpus = null;
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);

		ISelection isel = window.getActivePage().getSelection();
		if (isel instanceof IStructuredSelection) {
			IStructuredSelection sel = (IStructuredSelection) isel;
			Object first = sel.getFirstElement();
			if (first instanceof CQPCorpus) {
				corpus = (CQPCorpus) first;
				if (ParametersDialog.open(this)) {
					apply(corpus, model_file, new String[] { pos_property, lemma_property }, options.split("  "));
					return corpus;
				}
			}
		}

		System.out.println("Wrong selection.");
		return null;
	}

	public static void apply(CQPCorpus corpus, final File model, final String[] properties, final String[] options) {
		final MainCorpus mainCorpus = corpus.getMainCorpus();
		final File corpusBinaryDirectory = mainCorpus.getProjectDirectory();
		final File txmDirectory = new File(corpusBinaryDirectory, "txm/" + mainCorpus.getID());

		if (!txmDirectory.exists()) {
			System.out.println("Can't apply TreeTagger to a corpus with no XML-TXM files.");
		}

		final File[] files = txmDirectory.listFiles(new FileFilter() {

			@Override
			public boolean accept(File file) {
				return file.isFile() && file.canWrite() && file.getName().endsWith(".xml");
			}
		});

		if (files == null || files.length == 0) {
			System.out.println("Can't apply TreeTagger to a corpus with no XML-TXM files in " + txmDirectory);
		}

		if (!FileUtils.isExtension(model, "par")) {
			System.out.println("Model file name must ends with the '.par' extension");
			return;
		}

		String lang = FileUtils.stripExtension(model);

		final HashMap<String, String> hash = new HashMap<>();
		for (File txmFile : files) {
			hash.put(txmFile.getName(), lang);
		}

		for (int i = 0; i < properties.length; i++)
			properties[i] = properties[i].trim();

		System.out.println("APPLY : " + model + " to " + corpus + " updating " + Arrays.toString(properties) + " with options " + Arrays.toString(options));
		JobHandler job = new JobHandler("Applying TreeTagger to " + corpus + " corpus.") {

			@Override
			protected IStatus _run(SubMonitor monitor) {

				Annotate annotator = new Annotate();
				annotator.setModelsDirectory(model.getParentFile());
				annotator.setDebug();
				if (!annotator.run(corpusBinaryDirectory, txmDirectory, hash, true, properties, options)) {
					Log.warning("Fail to apply TreeTagger with " + txmDirectory + " files.");
					return Status.CANCEL_STATUS;
				}

				JobHandler subjob = UpdateCorpus.update(mainCorpus);
				if (subjob == null) {
					System.out.println("Fail to update corpus indexes and editions.");
				} else {
					//subjob.acquireSemaphore();
				}
				
				return Status.OK_STATUS;// frppos
			}
		};
		job.schedule();
	}
}
