package org.txm.treetagger.rcp.handlers;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamException;

import org.txm.importer.StaxIdentityParser;

/**
 * Remove XML-TXM file ana elements which 'type' attribute value is in a set
 * 
 * @author mdecorde
 *
 */
public class XMLRemoveProperties extends StaxIdentityParser {

	// form -> pos -> source -> lemma
	protected HashSet<String> propertiesSet = null;

	/**
	 * 
	 * @param infile the XML-TXM file to process
	 * @param propertiesSet the set of ana@type attributes to remove
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	public XMLRemoveProperties(File infile, HashSet<String> propertiesSet) throws IOException, XMLStreamException {
		super(infile);
		this.propertiesSet = new HashSet<String>();
		for (String property : propertiesSet) {
			// the XML-TXM files word properties name starts with # (they are references)
			if (!property.startsWith("#")) property = "#" + property; //$NON-NLS-1$
			this.propertiesSet.add(property);
		}
	}

	boolean inW = false, inAna = false;

	String typeName = null;

	@Override
	public void processStartElement() throws XMLStreamException, IOException {

		if (localname.equals("w")) { //$NON-NLS-1$
			inW = true;
		}
		else if (inW && localname.equals("ana")) { //$NON-NLS-1$
			inAna = true;
			typeName = parser.getAttributeValue(null, "type"); //$NON-NLS-1$
			if (propertiesSet.contains(typeName)) return; // don't write this element start tag
		}

		super.processStartElement();
	}

	@Override
	public void processCharacters() throws XMLStreamException {

		if (inW && typeName != null && propertiesSet.contains(typeName)) {
			return;
		} // don't write the element content
		super.processCharacters();
	}

	@Override
	public void processEndElement() throws XMLStreamException {
		if (localname.equals("w")) { //$NON-NLS-1$
			inW = false;
		}
		else if (inW && localname.equals("ana")) { //$NON-NLS-1$
			inAna = false;
			if (propertiesSet.contains(typeName)) {
				typeName = null;
				return;
			} // don't write the element end tag
			typeName = null;
		}

		super.processEndElement(); // don't write W content now
	}

	public static void main(String args[]) {
		File xmlFile = new File("TXM/corpora/XTZTEXTUALPLANS/txm/XTZTEXTUALPLANS/test.xml"); //$NON-NLS-1$
		File tmpFile = new File("TXM/corpora/XTZTEXTUALPLANS/txm/XTZTEXTUALPLANS/test-o.xml"); //$NON-NLS-1$
		String posProperty = "type"; //$NON-NLS-1$
		String newType = "lemma"; //$NON-NLS-1$
		LinkedHashMap<Pattern[], String> rules = new LinkedHashMap<Pattern[], String>();
		rules.put(new Pattern[] { Pattern.compile("w"), Pattern.compile("w") }, "WORD"); //$NON-NLS-1$
		rules.put(new Pattern[] { Pattern.compile("x.+"), Pattern.compile("w") }, "XWORD"); //$NON-NLS-1$
		rules.put(new Pattern[] { Pattern.compile("y"), Pattern.compile("w") }, "YWORD"); //$NON-NLS-1$
		rules.put(new Pattern[] { Pattern.compile("y.*"), Pattern.compile("w") }, "YMULTIWORD"); //$NON-NLS-1$
		// XMLPropertyProjection converter = new XMLPropertyProjection(xmlFile, rules, posProperty, newType);
		// System.out.println(converter.process(tmpFile));
	}
}
