package org.txm.treetagger.rcp.handlers;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.kohsuke.args4j.Option;
import org.txm.Toolbox;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.DeleteDir;
import org.txm.utils.io.FileCopy;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class RemoveProperties extends AbstractHandler {

	@Option(name = "propertiesList", usage = "The properties to remove", widget = "String", required = true, def = "plemma")
	public String propertiesList = null;

	/**
	 * 
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		CQPCorpus corpus = null;
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);

		ISelection isel = window.getActivePage().getSelection();
		if (isel instanceof IStructuredSelection) {
			IStructuredSelection sel = (IStructuredSelection) isel;
			Object first = sel.getFirstElement();
			if (first instanceof CQPCorpus) {
				corpus = (CQPCorpus) first;
				if (ParametersDialog.open(this)) {
					LinkedHashSet<String> propertiesSet = new LinkedHashSet<String>();
					propertiesSet.addAll(Arrays.asList(propertiesList.split(","))); //$NON-NLS-1$

					apply(corpus, propertiesSet);
					return corpus;
				}
			}
		}

		System.out.println("Wrong selection.");
		return null;
	}

	public static void apply(final CQPCorpus corpus, final HashSet<String> propertiesSet) {
		final MainCorpus mainCorpus = corpus.getMainCorpus();
		final File corpusBinaryDirectory = mainCorpus.getProjectDirectory();
		final File txmDirectory = new File(corpusBinaryDirectory, "txm/" + mainCorpus.getID()); //$NON-NLS-1$

		if (!txmDirectory.exists()) {
			System.out.println("Can't process a corpus with no XML-TXM files directory: " + txmDirectory);
			return;
		}

		final File[] files = txmDirectory.listFiles(new FileFilter() {

			@Override
			public boolean accept(File file) {
				return file.isFile() && file.canWrite() && file.getName().endsWith(".xml"); //$NON-NLS-1$
			}
		});

		if (files == null || files.length == 0) {
			System.out.println("Can't process a corpus with no XML-TXM files in " + txmDirectory);
			return;
		}

		System.out.println("Removing " + propertiesSet + " to " + mainCorpus + " XML-TXM files...");
		JobHandler job = new JobHandler("Removing " + propertiesSet + " to " + mainCorpus + " XML-TXM files.") {

			@Override
			protected IStatus _run(SubMonitor monitor) throws Exception {

				LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>> rules = new LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>>();
				HashSet<String> no_change_rules = new HashSet<String>();

				// save previous version of XML-TXM files
				File previousXMLTXMDirectory = new File(txmDirectory.getAbsolutePath() + "_previous"); //$NON-NLS-1$
				DeleteDir.deleteDirectory(previousXMLTXMDirectory);
				FileCopy.copyFiles(txmDirectory, previousXMLTXMDirectory);

				// work
				File noMatchsFile = new File(Toolbox.getTxmHomePath(), "results/nomatch.txt"); //$NON-NLS-1$
				HashSet<String> noMatchsSet = new HashSet<String>();
				ConsoleProgressBar cpb = new ConsoleProgressBar(files.length);
				for (File xmlFile : files) {
					cpb.tick();
					XMLRemoveProperties p = new XMLRemoveProperties(xmlFile, propertiesSet);
					File tmpFile = new File(xmlFile.getParentFile(), "tmp_" + xmlFile.getName()); //$NON-NLS-1$
					if (p.process(tmpFile)) {
						if (xmlFile.delete() && tmpFile.renameTo(xmlFile)) {
							// ok
						}
						else {
							System.out.println("Error during properties removal: can't replace XML-TXM file: " + xmlFile);
							return Status.CANCEL_STATUS;
						}
					}
					else {
						System.out.println("Error during properties removal. Aborting.");
						return Status.CANCEL_STATUS;
					}
				}

				cpb.done();
				monitor.worked(50);

				System.out.println("Done.");
				return Status.OK_STATUS;
			}
		};
		job.schedule();
	}
}
