package org.txm.treetagger.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;


/**
 * TreeTagger UI Messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class TreeTaggerUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.treetagger.rcp.messages.messages"; //$NON-NLS-1$

	public static String pathToInstallDirectory;

	public static String pathToModelsDirectory;

	public static String preferencePage_treeTaggerModelsManagement;

	public static String activateTreeTaggerDebugMessages; // Activate TreeTagger debug messages

	public static String treeTaggerOptions; // TreeTagger tagging options / Options d'annotation de TreeTagger

	public static String wordFormAsLemmaForUnknownLemma; // Forme du mot comme lemme pour lemme inconnu / Word form as lemma for unknown lemma

	public static String useTheWordFormAsLemmaRatherThanEtc; //Use the word form as lemma rather than  for unknown lemma / Utiliser la forme du mot comme lemme plutôt que  pour un lemme inconnu

	public static String unknownHyphenatedWordsPartOfSpeechHeuristic; // unknown hyphenated words part of speech heuristic / heuristique de partie du discours des mots inconnus césurés
	
	public static String useAHeuristicToDetermineThePartOfSpeechOfUnknownHyphenatedWords; // use a heuristic to determine the part of speech of unknown hyphenated words / utiliser une heuristique pour déterminer la partie du discours des mots inconnus césurés

	public static String partOfSpeechProbability; // part of speech probability / probabilité de la partie du discours
	
	public static String addTheProbabilityOfThePartOfSpeechAsAProperty; // add the probability of the part of speech as a property / ajouter la probabilité de la partie du discours comme propriété

	public static String externalLexiconFile; // Fichier de lexique externe / External lexicon file
	
	public static String externalLexiconFileEtc; // external lexicon file (form TAB pos TAB lemma) /	fichier de lexique externe (forme TAB catégorie TAB lemme)
	
	public static String wordClassAutomatonFile; // word-class automaton file / fichier d'automate de catégorie
	
	public static String grammaticalWordClassAutomatonFile; // grammatical word-class automaton file /fichier d'automate de catégorie grammaticale de mot

	public static String treeTaggerTrainingOptions; // TreeTagger Training options / Options d'apprentissage de TreeTagger
	
	public static String leftContextSize; // left context size / taille contexte gauche
	
	public static String useAContextOfWords; // use a context of  words / utiliser un contexte de  mots
	
	public static String minimumDescisionTreeGain; // minimum decision tree gain / gain de l'arbre de décision minimum
	
	public static String smoothingWeight; // smoothing weight / poids de lissage
	
	public static String minimumAffixTreeGain; // minimum affix tree gain / gain de l'arbre des affixes minimum
	
	public static String equivalenceClassWeight; // equivalence class weight / poids de la classe d'équivalence
	
	public static String thresholdProbabiblityForLexicalEntries; // threshold probability for lexical entries /	probabilité seuil pour les entrées lexicales
	
	public static String thresholdProbabiblityForLexicalEntriesEtc; // threshold probability for lexical entries (default 0.001) / probabilité seuil pour les entrées lexicales (0.001 par défaut)
	
	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, TreeTaggerUIMessages.class);
	}

	private TreeTaggerUIMessages() {
	}

}
