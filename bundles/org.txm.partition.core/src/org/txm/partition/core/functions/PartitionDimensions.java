/**
 * 
 */
package org.txm.partition.core.functions;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.txm.chartsengine.core.results.ChartResult;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.Parameter;
import org.txm.partition.core.messages.PartitionCoreMessages;
import org.txm.partition.core.preferences.PartitionDimensionsPreferences;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Part;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

/**
 * Partition dimensions.
 * 
 * @author sjacquot
 *
 */
public class PartitionDimensions extends ChartResult {

	/**
	 * Parent partition parts caching.
	 */
	protected List<Part> parts = null;

	/**
	 * Parent partition parts sorted by size caching.
	 */
	// FIXME: SJ: became useless? since the parent Parts are now real TXMResult children
	@Deprecated
	protected List<Part> partsSortedBySize = null;

	/**
	 * To display or not the parts count in chart title.
	 */
	@Parameter(key = PartitionDimensionsPreferences.CHART_DIMENSIONS_DISPLAY_PARTS_COUNT_IN_TITLE, type = Parameter.RENDERING)
	protected boolean pDisplayPartsCountInTitle;

	/**
	 * To sort or not the parts by size.
	 */
	@Parameter(key = PartitionDimensionsPreferences.CHART_DIMENSIONS_SORT_BY_SIZE, type = Parameter.RENDERING)
	protected boolean pSortBySize;



	/**
	 * Creates a not computed partition dimensions.
	 * 
	 * @param partition
	 */
	public PartitionDimensions(Partition partition) {
		this(null, partition);
	}

	/**
	 * Creates a not computed partition dimensions.
	 * 
	 * @param parametersNodePath
	 */
	public PartitionDimensions(String parametersNodePath) {
		this(parametersNodePath, null);
	}

	/**
	 * Creates a not computed partition dimensions.
	 * 
	 * @param parametersNodePath
	 * @param partition
	 */
	public PartitionDimensions(String parametersNodePath, Partition partition) {
		super(parametersNodePath, partition);

		this.domainGridLinesVisible = false;
	}


	@Override
	public String getComputingStartMessage() {
		return TXMCoreMessages.bind(PartitionCoreMessages.info_dimensionsOfTheP0Partition, this.getParent().getName());
	}



	@Override
	public boolean loadParameters() {
		// nothing to do, all parameters are auto-loaded
		return true;
	}

	@Override
	public boolean saveParameters() {
		// nothing to do, all parameters are auto-saved
		return true;
	}

	/**
	 * Gets a string representing the result that can be used as a file name (eg. for exporting in file).
	 * 
	 * @return a valid filename without unexpected characters
	 */
	// FIXME: SJ: to discuss and/or to move in export layer
	@Override
	public String getValidFileName() {
		try {
			// return FILE_NAME_PATTERN.matcher(this.getCurrentName()).replaceAll(UNDERSCORE);
			return (this.parent.getResultType() + " " + this.getResultType() + " " + this.parent.getUserName()).replaceAll("[¤€§µ£°().,;:/?§%\"'*+\\-}\\]\\[{#~&@<>]", "").trim(); // avoid blanks in name
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
		return ""; //$NON-NLS-1$

	}

	@Override
	protected boolean __compute(TXMProgressMonitor monitor) throws Exception {
		// retrieve the parts
		this.parts = this.getPartition().getParts();

		// store a copy of parts sorted by size
		List<Part> sortedParts = new ArrayList<>(this.parts);
		Collections.sort(sortedParts, new Comparator<Part>() {

			@Override
			public int compare(Part arg0, Part arg1) {
				try {
					return arg1.getSize() - arg0.getSize();
				}
				catch (CqiClientException e) {
					return 0;
				}
			}
		});
		this.partsSortedBySize = sortedParts;

		return true;
	}

	@Override
	public void clean() {
		// does nothing
	}

	@Override
	public boolean canCompute() {
		return this.getPartition() != null;
	}

	//	@Override
	//	public boolean setParameters(TXMParameters parameters) {
	//		try {
	//			this.setSortBySize((boolean) parameters.get(PartitionDimensionsPreferences.CHART_DIMENSIONS_SORT_BY_SIZE));
	//			return true;
	//		}
	//		catch (Exception e) {
	//			// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		}
	//		return false;
	//	}
	//	


	/**
	 * Convenience method.
	 */
	public Partition getPartition() {
		try {
			return (Partition) this.parent;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}



	/**
	 * Gets the number of parts in the linked partition.
	 * 
	 * @return
	 */
	public int getPartsCount() {
		return this.parts.size();
	}

	/**
	 * Gets the parts of the linked partition sorted by size or not.
	 * 
	 * @param sortedBySize
	 * @return
	 */
	public List<Part> getParts(boolean sortedBySize) {
		if (sortedBySize) {
			return this.partsSortedBySize;
		}
		else {
			return this.parts;
		}
	}


	/**
	 * Gets the corpus of the linked partition.
	 * 
	 * @return
	 */
	public CQPCorpus getCorpus() {
		return this.getPartition().getParent();
	}


	@Override
	public String getSimpleName() {
		return PartitionCoreMessages.RESULT_TYPE;
	}


	@Override
	public String getName() {
		try {
			return this.getParent().getName();
		}
		catch (Exception e) {
		}
		return ""; //$NON-NLS-1$
	}


	@Override
	public String getDetails() {
		return this.getName();
	}


	@Override
	public boolean _toTxt(File file, String encoding, String colseparator, String txtseparator) throws Exception {
		PrintWriter writer = IOUtils.getWriter(file, encoding);

		writer.println("Parts" + colseparator + "Size");

		List<Part> partOrderToUse = this.parts;
		if (pSortBySize) {
			partOrderToUse = this.partsSortedBySize;
		}
		
		for (Part b : partOrderToUse) {
			String name = b.getName();
			if (name.contains(" ∩ ")) {

				String[] split = name.split(" ∩ ");
				name = "";
				for (int i = 0; i < split.length; i++) {
					String s = split[i];
					//System.out.println("s=" + s);
					if (i > 0) name += colseparator;
					name += txtseparator + s.replaceAll(txtseparator, txtseparator + txtseparator) + txtseparator;
				}
			}

			writer.println(name + colseparator + txtseparator + b.getSize() + txtseparator);
		}

		writer.close();
		return true;
	}



	/**
	 * @return the sortBySize
	 */
	public boolean isSortingBySize() {
		return pSortBySize;
	}



	/**
	 * @param sortBySize the sortBySize to set
	 */
	public void setSortBySize(boolean sortBySize) {
		this.pSortBySize = sortBySize;
	}



	/**
	 * @return the displayPartCountInTitle
	 */
	public boolean isDisplayingPartCountInTitle() {
		return pDisplayPartsCountInTitle;
	}

	@Override
	public String getResultType() {
		return PartitionCoreMessages.RESULT_TYPE;
	}

}
