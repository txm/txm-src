package org.txm.partition.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;


/**
 * Partition core messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class PartitionCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.partition.core.messages.messages"; //$NON-NLS-1$

	public static String RESULT_TYPE;

	public static String chart_title_dimensionsOfTheP0PartitionInTheP1Corpus;
	public static String dimensionsColonP0;
	public static String error_cantCreateDimensionsPartitionChartFileChartsEngineColonP0;
	public static String info_dimensionsOfTheP0Partition;
	public static String numberOfWords;
	public static String part;
	public static String parts;
	public static String sortedBySize;
	public static String t;


	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, PartitionCoreMessages.class);
	}


}
