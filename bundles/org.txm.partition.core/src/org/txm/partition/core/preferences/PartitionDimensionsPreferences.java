package org.txm.partition.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Default preferences initializer.
 * 
 * @author sjacquot
 *
 */
public class PartitionDimensionsPreferences extends ChartsEnginePreferences {

	public static final String CHART_DIMENSIONS_DISPLAY_PARTS_COUNT_IN_TITLE = "chart_dimensions_display_parts_count_in_title"; //$NON-NLS-1$

	public static final String CHART_DIMENSIONS_SORT_BY_SIZE = "chart_dimensions_sort_by_parts_size"; //$NON-NLS-1$

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(PartitionDimensionsPreferences.class)) {
			new PartitionDimensionsPreferences();
		}
		return TXMPreferences.instances.get(PartitionDimensionsPreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.putBoolean(CHART_DIMENSIONS_DISPLAY_PARTS_COUNT_IN_TITLE, true);
		preferences.putBoolean(CHART_DIMENSIONS_SORT_BY_SIZE, false);

		// disable unavailable functionality
		TXMPreferences.setEmpty(this.getPreferencesNodeQualifier(), ChartsEnginePreferences.SHOW_LEGEND);
	}
}
