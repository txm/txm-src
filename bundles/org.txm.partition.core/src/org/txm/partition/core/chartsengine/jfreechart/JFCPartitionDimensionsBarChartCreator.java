package org.txm.partition.core.chartsengine.jfreechart;

import java.util.List;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.title.TextTitle;
import org.jfree.chart.title.Title;
import org.jfree.data.general.DatasetUtils;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.chartsengine.jfreechart.core.JFCChartCreator;
import org.txm.chartsengine.jfreechart.core.themes.base.ExtendedNumberAxis;
import org.txm.chartsengine.jfreechart.core.themes.base.SymbolAxisBetweenTicks;
import org.txm.partition.core.chartsengine.base.Utils;
import org.txm.partition.core.functions.PartitionDimensions;
import org.txm.partition.core.messages.PartitionCoreMessages;
import org.txm.partition.core.preferences.PartitionDimensionsPreferences;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Part;

/**
 * Creates a partition dimensions bar chart from the specified <code>PartitionDimensions</code> result.
 * 
 * @author sjacquot
 *
 */
public class JFCPartitionDimensionsBarChartCreator extends JFCChartCreator<PartitionDimensions> {


	@Override
	public JFreeChart createChart(PartitionDimensions partitionDimensions) {

		// Creating an empty data set
		XYSeriesCollection dataset = new XYSeriesCollection();

		// Create the chart
		JFreeChart chart = this.getChartsEngine().createXYBarChart(dataset, null, PartitionCoreMessages.part, PartitionCoreMessages.numberOfWords, false, true);
		// Custom range axis for ticks drawing options
		chart.getXYPlot().setRangeAxis(new ExtendedNumberAxis((NumberAxis) chart.getXYPlot().getRangeAxis(), false, true, 0, DatasetUtils.findMaximumRangeValue(chart.getXYPlot().getDataset())
				.doubleValue()));

		return chart;

	}


	@Override
	public void updateChart(PartitionDimensions partitionDimensions) {

		JFreeChart chart = (JFreeChart) partitionDimensions.getChart();

		// freeze rendering while computing
		chart.setNotify(false);


		// parameters
		boolean sortPartsBySize = partitionDimensions.isSortingBySize();
		boolean displayPartsCountInTitle = partitionDimensions.isDisplayingPartCountInTitle();


		if (partitionDimensions.hasParameterChanged(ChartsEnginePreferences.CHART_TYPE)
				|| partitionDimensions.hasParameterChanged(PartitionDimensionsPreferences.CHART_DIMENSIONS_SORT_BY_SIZE)) {

			// Fill the data set from the result
			XYSeriesCollection dataset = (XYSeriesCollection) chart.getXYPlot().getDataset();
			dataset.removeAllSeries();

			XYSeries series = new XYSeries(PartitionCoreMessages.t);
			dataset.addSeries(series);

			List<Part> parts = partitionDimensions.getParts(sortPartsBySize);
			String[] xAxisSymbols = new String[parts.size()];


			try {
				for (int i = 0; i < parts.size(); i++) {
					series.add(i, parts.get(i).getSize());

					// Add X axis symbol
					xAxisSymbols[i] = parts.get(i).getSimpleName();
				}
			}
			catch (CqiClientException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			chart.getXYPlot().setDomainAxis(new SymbolAxisBetweenTicks(chart.getXYPlot().getDomainAxis().getLabel(), xAxisSymbols));
		}

		// Create chart title
		chart.setTitle(Utils.createPartitionDimensionsChartTitle(partitionDimensions));

		// Subtitle
		if (chart.getSubtitleCount() > 0) {
			chart.removeSubtitle(chart.getSubtitle(0));
		}
		chart.addSubtitle(new TextTitle(Utils.createPartitionDimensionsChartSubtitle(partitionDimensions, sortPartsBySize, displayPartsCountInTitle), this.getChartsEngine().getJFCTheme()
				.getSmallFont(),
				(this.getChartsEngine().getJFCTheme().getAxisLabelPaint()), Title.DEFAULT_POSITION,
				Title.DEFAULT_HORIZONTAL_ALIGNMENT,
				Title.DEFAULT_VERTICAL_ALIGNMENT, Title.DEFAULT_PADDING));


		super.updateChart(partitionDimensions);

	}

	@Override
	public Class<PartitionDimensions> getResultDataClass() {
		return PartitionDimensions.class;
	}
}
