package org.txm.partition.core.chartsengine.r;

import java.io.File;
import java.util.List;

import org.txm.chartsengine.r.core.RChartCreator;
import org.txm.chartsengine.r.core.RChartsEngine;
import org.txm.partition.core.chartsengine.base.Utils;
import org.txm.partition.core.functions.PartitionDimensions;
import org.txm.partition.core.messages.PartitionCoreMessages;
import org.txm.searchengine.cqp.corpus.Part;
import org.txm.stat.engine.r.RDevice;
import org.txm.utils.logger.Log;

/**
 * Creates a partition dimensions pie chart from the specified <code>PartitionDimensions</code> result.
 * 
 * @author sjacquot
 *
 */
public class RPartitionDimensionsPieChartCreator extends RChartCreator<PartitionDimensions> {


	@Override
	public File createChart(PartitionDimensions result, File file) {

		try {

			PartitionDimensions partitionDimensions = result;


			// parameters
			boolean sortPartsBySize = partitionDimensions.isSortingBySize();
			boolean displayPartsCountInTitle = partitionDimensions.isDisplayingPartCountInTitle();

			List<Part> parts = partitionDimensions.getParts(sortPartsBySize);

			// Create parts names command string
			String snames = "c("; //$NON-NLS-1$
			for (int i = 0; i < parts.size(); i++) {
				String name = parts.get(i).getSimpleName();
				if (name.trim().length() == 0)
					name = "N/A"; //$NON-NLS-1$
				snames += "\"" + name + "\""; //$NON-NLS-1$ //$NON-NLS-2$
				if (i != parts.size() - 1)
					snames += ","; //$NON-NLS-1$
			}
			snames += ")"; //$NON-NLS-1$

			// Create parts sizes command string
			String ssizes = "c("; //$NON-NLS-1$

			for (int i = 0; i < parts.size(); i++) {
				ssizes += "" + parts.get(i).getSize(); //$NON-NLS-1$
				if (i != parts.size() - 1)
					ssizes += ","; //$NON-NLS-1$
			}
			ssizes += ")"; //$NON-NLS-1$

			// colors
			// this.getChartsEngine().setColors(result.getRenderingColorsMode(), 1);
			// FIXME: one color by part
			this.getChartsEngine().setColors(result.getRenderingColorsMode(), parts.size());


			String ylab = PartitionCoreMessages.numberOfWords;

			// String cmd = "pie(" + ssizes + ", col=colors, labels=" + snames + ", horiz=F, las=2, ylab=\"" + ylab + "\");\n"; //$NON-NLS-1$

			String cmd = String.format("pie(%s, col=colors, labels=%s, horiz=F, las=2, ylab=\"%s\");\n", ssizes, snames, ylab); //$NON-NLS-1$

			// plot the chart
			this.getChartsEngine().plot(file, cmd, result, Utils.createPartitionDimensionsChartTitle(partitionDimensions), Utils.createPartitionDimensionsChartSubtitle(partitionDimensions,
					sortPartsBySize, displayPartsCountInTitle), RDevice.getDeviceForFile(file));

		}
		catch (Exception e) {
			Log.severe(PartitionCoreMessages.bind(PartitionCoreMessages.error_cantCreateDimensionsPartitionChartFileChartsEngineColonP0, RChartsEngine.DESCRIPTION) + e);
		}

		return file;
	}


	@Override
	public Class<PartitionDimensions> getResultDataClass() {
		return PartitionDimensions.class;
	}
}
