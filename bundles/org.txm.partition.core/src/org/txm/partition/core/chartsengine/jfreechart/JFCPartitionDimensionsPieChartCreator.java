package org.txm.partition.core.chartsengine.jfreechart;

import java.util.List;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.title.TextTitle;
import org.jfree.chart.title.Title;
import org.jfree.chart.util.SortOrder;
import org.jfree.data.general.DefaultPieDataset;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.chartsengine.jfreechart.core.JFCChartCreator;
import org.txm.partition.core.chartsengine.base.Utils;
import org.txm.partition.core.functions.PartitionDimensions;
import org.txm.partition.core.preferences.PartitionDimensionsPreferences;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Part;

/**
 * Creates a partition dimensions pie chart from the specified <code>PartitionDimensions</code> result.
 * 
 * @author sjacquot
 *
 */
public class JFCPartitionDimensionsPieChartCreator extends JFCChartCreator<PartitionDimensions> {


	@Override
	public JFreeChart createChart(PartitionDimensions partitionDimensions) {

		// Creating an empty data set
		DefaultPieDataset dataset = new DefaultPieDataset();

		// Create the chart
		JFreeChart chart = this.getChartsEngine().createPieChart(dataset, null, true);

		return chart;

	}



	@Override
	public void updateChart(PartitionDimensions partitionDimensions) {

		JFreeChart chart = (JFreeChart) partitionDimensions.getChart();

		// freeze rendering while computing
		chart.setNotify(false);


		// parameters
		boolean sortPartsBySize = partitionDimensions.isSortingBySize();
		boolean displayPartsCountInTitle = partitionDimensions.isDisplayingPartCountInTitle();


		if (partitionDimensions.hasParameterChanged(ChartsEnginePreferences.CHART_TYPE)
				|| partitionDimensions.hasParameterChanged(PartitionDimensionsPreferences.CHART_DIMENSIONS_SORT_BY_SIZE)) {

			// Fill the data set from the result
			DefaultPieDataset dataset = (DefaultPieDataset) ((PiePlot) chart.getPlot()).getDataset();
			dataset.clear();

			try {

				List<Part> parts = partitionDimensions.getParts(sortPartsBySize);

				for (int i = 0; i < parts.size(); i++) {
					dataset.setValue(parts.get(i).getSimpleName(), parts.get(i).getSize());
				}

				if (sortPartsBySize) {
					dataset.sortByValues(SortOrder.ASCENDING);
				}
				else {
					dataset.sortByKeys(SortOrder.ASCENDING);
				}
			}
			catch (CqiClientException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// Create chart title
		chart.setTitle(Utils.createPartitionDimensionsChartTitle(partitionDimensions));

		// Subtitle
		if (chart.getSubtitleCount() > 0) {
			chart.removeSubtitle(chart.getSubtitle(0));
		}
		chart.addSubtitle(new TextTitle(Utils.createPartitionDimensionsChartSubtitle(partitionDimensions, sortPartsBySize, displayPartsCountInTitle), this.getChartsEngine().getJFCTheme()
				.getSmallFont(),
				(this.getChartsEngine().getJFCTheme().getAxisLabelPaint()), Title.DEFAULT_POSITION,
				Title.DEFAULT_HORIZONTAL_ALIGNMENT,
				Title.DEFAULT_VERTICAL_ALIGNMENT, Title.DEFAULT_PADDING));


		super.updateChart(partitionDimensions);

	}

	@Override
	public Class<PartitionDimensions> getResultDataClass() {
		return PartitionDimensions.class;
	}
}
