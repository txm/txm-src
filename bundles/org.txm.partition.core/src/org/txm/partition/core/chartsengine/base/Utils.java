package org.txm.partition.core.chartsengine.base;

import org.txm.partition.core.functions.PartitionDimensions;
import org.txm.partition.core.messages.PartitionCoreMessages;

/**
 * Utility class for partition dimensions charts.
 * 
 * @author sjacquot
 *
 */
public abstract class Utils {


	/**
	 * Creates a string title, shared by charts engine implementations, for the partition dimensions chart from the specified result.
	 * 
	 * @param partitionDimensions the dimension to display
	 * @return the title string
	 */
	public static String createPartitionDimensionsChartTitle(PartitionDimensions partitionDimensions) {
		return PartitionCoreMessages.bind(PartitionCoreMessages.chart_title_dimensionsOfTheP0PartitionInTheP1Corpus, partitionDimensions.getParent().getSimpleName(), partitionDimensions.getCorpus()
				.getSimpleName());
	}

	/**
	 * Creates a string subtitle, shared by charts engine implementations, for the partition dimensions chart from the specified result.
	 * 
	 * @param partitionDimensions the partition result
	 * @param sortPartsBySize the sort parts by size state
	 * @param displayPartsCountInTitle displayPartsCountInTitle display or not the parts count in chart title
	 * @return the subtitle string
	 */
	public static String createPartitionDimensionsChartSubtitle(PartitionDimensions partitionDimensions, boolean sortPartsBySize, boolean displayPartsCountInTitle) {

		String subtitle = ""; //$NON-NLS-1$

		if (displayPartsCountInTitle) {
			subtitle += partitionDimensions.getPartsCount() + " " + PartitionCoreMessages.parts; //$NON-NLS-1$
		}

		if (displayPartsCountInTitle && sortPartsBySize) {
			subtitle += " "; //$NON-NLS-1$
		}

		if (sortPartsBySize) {
			subtitle += PartitionCoreMessages.sortedBySize;
		}

		return subtitle;

	}
}
