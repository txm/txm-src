package org.txm.partition.core.chartsengine.r;

import java.io.File;
import java.util.List;

import org.txm.chartsengine.r.core.RChartCreator;
import org.txm.chartsengine.r.core.RChartsEngine;
import org.txm.partition.core.chartsengine.base.Utils;
import org.txm.partition.core.functions.PartitionDimensions;
import org.txm.partition.core.messages.PartitionCoreMessages;
import org.txm.searchengine.cqp.corpus.Part;
import org.txm.stat.engine.r.RDevice;
import org.txm.utils.logger.Log;

/**
 * Creates a partition dimensions bar chart from the specified <code>PartitionDimensions</code> result.
 * 
 * @author sjacquot
 *
 */
public class RPartitionDimensionsBarChartCreator extends RChartCreator<PartitionDimensions> {

	@Override
	public File createChart(PartitionDimensions result, File file) {

		try {

			PartitionDimensions partitionDimensions = (PartitionDimensions) result;


			// parameters
			boolean sortPartsBySize = partitionDimensions.isSortingBySize();
			boolean displayPartsCountInTitle = partitionDimensions.isDisplayingPartCountInTitle();

			List<Part> parts = partitionDimensions.getParts(sortPartsBySize);

			// Create parts names command string
			String snames = "c("; //$NON-NLS-1$
			for (int i = 0; i < parts.size(); i++) {
				String name = parts.get(i).getSimpleName();
				if (name.trim().length() == 0)
					name = "N/A"; //$NON-NLS-1$
				snames += "\"" + name + "\""; //$NON-NLS-1$ //$NON-NLS-2$
				if (i != parts.size() - 1)
					snames += ","; //$NON-NLS-1$
			}
			snames += ")"; //$NON-NLS-1$

			// Create parts sizes command string
			String ssizes = "c("; //$NON-NLS-1$

			for (int i = 0; i < parts.size(); i++) {
				ssizes += parts.get(i).getSize();
				if (i != parts.size() - 1)
					ssizes += ","; //$NON-NLS-1$
			}
			ssizes += ")"; //$NON-NLS-1$

			// Create chart title
			//			String title = "";
			//			if(result.getBooleanParameterValue(ChartsEnginePreferences.SHOW_TITLE))	{
			//				title = Utils.createPartitionDimensionsChartTitle(partitionDimensions, sortPartsBySize, displayPartsCountInTitle);
			//			}

			// colors
			this.getChartsEngine().setColors(result.getRenderingColorsMode(), 1);
			// FIXME: one color by part
			//this.getChartsEngine().setColors(TXMPreferences.getInt(preferencesNode, result, ChartsEnginePreferences.RENDERING_COLORS_MODE),  parts.size());

			String cmd = String.format("barplot(%s, col=colors, names.arg=%s, horiz=F, las=2, ylab=\"%s\", xlab=\"%s\");\n", ssizes, snames, PartitionCoreMessages.numberOfWords, //$NON-NLS-1$
					PartitionCoreMessages.part);

			// plot the chart
			this.getChartsEngine().plot(file, cmd, result, Utils.createPartitionDimensionsChartTitle(partitionDimensions),
					Utils.createPartitionDimensionsChartSubtitle(partitionDimensions, sortPartsBySize, displayPartsCountInTitle), RDevice.getDeviceForFile(file));

		}
		catch (Exception e) {
			Log.severe(PartitionCoreMessages.bind(PartitionCoreMessages.error_cantCreateDimensionsPartitionChartFileChartsEngineColonP0, RChartsEngine.DESCRIPTION) + e);
			Log.printStackTrace(e);
		}

		return file;
	}



	@Override
	public Class<PartitionDimensions> getResultDataClass() {
		return PartitionDimensions.class;
	}

}
