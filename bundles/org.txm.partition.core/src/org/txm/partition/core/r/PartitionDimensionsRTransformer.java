package org.txm.partition.core.r;

import java.util.List;

import org.txm.partition.core.functions.PartitionDimensions;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Part;
import org.txm.statsengine.r.core.RTransformer;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;


public class PartitionDimensionsRTransformer extends RTransformer<PartitionDimensions> {

	public static int nrec = 1;

	public PartitionDimensionsRTransformer() {

		// TODO Auto-generated constructor stub
	}

	@Override
	public PartitionDimensions fromRtoTXM(String symbol) throws RWorkspaceException {

		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String _fromTXMtoR(PartitionDimensions o, String symbol) throws RWorkspaceException {

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		//		System.out.println("rw="+rw);
		List<Part> parts = o.getParts(false);
		//		System.out.println("parts="+parts);
		int[] vector = new int[parts.size()];
		String[] svector = new String[parts.size()];
		for (int i = 0; i < parts.size(); i++) {
			try {
				vector[i] = parts.get(i).getSize();
				svector[i] = parts.get(i).getName();
			}
			catch (CqiClientException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//		System.out.println("vector="+Arrays.toString(vector));
		//		System.out.println("svector="+Arrays.toString(svector));
		if (symbol == null || symbol.length() == 0) {
			symbol = "PartitionDimensions" + nrec;
			nrec++;
		}
		rw.addVectorToWorkspace(symbol, vector);
		rw.addVectorToWorkspace("names(" + symbol + ")", svector);


		return symbol;
	}

}
