// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.searchengine.cqp.rcp.preferences;


import java.util.Arrays;

import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbench;
import org.osgi.service.prefs.Preferences;
import org.txm.libs.cqp.CQPLibPreferences;
import org.txm.rcp.Application;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.rcp.swt.widget.preferences.ComboFieldEditor;
import org.txm.rcp.swt.widget.preferences.DirectoryFieldEditor;
import org.txm.rcp.swt.widget.preferences.FileFieldEditor;
import org.txm.rcp.swt.widget.preferences.IntegerFieldEditor;
import org.txm.rcp.swt.widget.preferences.StringFieldEditor;
import org.txm.searchengine.core.messages.SearchEngineCoreMessages;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.rcp.messages.CQPUIMessages;
import org.txm.utils.OSDetector;
import org.txm.utils.logger.Log;

/**
 * CQP search engine preferences page.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class CQPPreferencePage extends TXMPreferencePage {

	/** CQL fix query strategy: tokenize string or not */
	private BooleanFieldEditor cql_fix_query_tokenize;

	/** The cqi_server_is_remote. */
	private BooleanFieldEditor cqi_use_network;

	/** The cqi_server_is_remote. */
	private BooleanFieldEditor cqi_server_is_remote;

	/** The cqi_server_host. */
	private StringFieldEditor cqi_server_host;

	/** The cqi_server_path_to_executable. */
	private FileFieldEditor cqi_server_path_to_executable;

	private DirectoryFieldEditor cqi_server_path_to_cqplib;

	//	/** The cqi_server_path_to_registry. */
	//	private StringFieldEditor cqi_server_path_to_registry;

	/** The cqi_server_path_to_init_file. */
	private FileFieldEditor cqi_server_path_to_init_file;

	/** The cqi_server_additional_options. */
	private StringFieldEditor cqi_server_additional_options;

	/** The cqi_server_path_to_registry. */
	private IntegerFieldEditor cqi_server_port;

	/** The cqi_server_path_to_init_file. */
	private StringFieldEditor cqi_server_user;

	/** The cqi_server_additional_options. */
	private StringFieldEditor cqi_server_password;

	/**
	 * Instantiates a new cQP preference page.
	 */
	public CQPPreferencePage() {
		super();
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		Group localGroup = new Group(getFieldEditorParent(), SWT.NONE);
		localGroup.setText(CQPUIMessages.libraryMode);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData.horizontalSpan = 3;
		gridData.verticalIndent = 10;
		localGroup.setLayoutData(gridData);
		
		cqi_server_path_to_cqplib = new DirectoryFieldEditor(CQPLibPreferences.CQI_SERVER_PATH_TO_CQPLIB, CQPUIMessages.cQPlibPath, localGroup);
		addField(cqi_server_path_to_cqplib);
		
		cqi_server_path_to_executable = new FileFieldEditor(CQPLibPreferences.CQI_SERVER_PATH_TO_EXECUTABLE, CQPUIMessages.pathToCWBServerBinary, localGroup);
		addField(cqi_server_path_to_executable);
		//		cqi_server_path_to_registry = new DirectoryFieldEditor(CQI_SERVER_PATH_TO_REGISTRY, CQPUIMessages.CQPPreferencePage_16, getFieldEditorParent());

		cqi_server_path_to_init_file = new FileFieldEditor(CQPLibPreferences.CQI_SERVER_PATH_TO_INIT_FILE, CQPUIMessages.pathToCWBServerInitializationFile, localGroup);
		addField(cqi_server_path_to_init_file);
		
		cqi_server_additional_options = new StringFieldEditor(CQPLibPreferences.CQI_SERVER_ADDITIONAL_OPTIONS, CQPUIMessages.additionalAmpoptionsForTheCWBServerColon, localGroup);
		addField(cqi_server_additional_options);
		
		cqi_use_network = new BooleanFieldEditor(CQPLibPreferences.CQI_NETWORK_MODE, CQPUIMessages.useNetworkProtocol, BooleanFieldEditor.DEFAULT, getFieldEditorParent());
		addField(cqi_use_network);
		
		Group serverGroup = new Group(getFieldEditorParent(), SWT.NONE);
		serverGroup.setText(CQPUIMessages.serverMode);
		GridData gridData2 = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData2.horizontalSpan = 3;
		gridData2.verticalIndent = 10;
		gridData2.horizontalIndent = 25;
		serverGroup.setLayoutData(gridData2);
		
		cqi_server_is_remote = new BooleanFieldEditor(CQPLibPreferences.CQI_SERVER_IS_REMOTE, CQPUIMessages.ampRemoteCWBServerAddressColon, BooleanFieldEditor.DEFAULT, serverGroup);
		addField(cqi_server_is_remote);
		
		cqi_server_host = new StringFieldEditor(CQPLibPreferences.CQI_SERVER_HOST, CQPUIMessages.cWBServerColonAmphostColon, serverGroup);
		addField(cqi_server_host);
		
		cqi_server_port = new IntegerFieldEditor(CQPLibPreferences.CQI_SERVER_PORT, CQPUIMessages.cWBServerColonAmpportColon, serverGroup);
		addField(cqi_server_port);
		
		cqi_server_user = new StringFieldEditor(CQPLibPreferences.CQI_SERVER_LOGIN, CQPUIMessages.cWBServerColonAmploginColon, serverGroup);
		addField(cqi_server_user);
		
		cqi_server_password = new StringFieldEditor(CQPLibPreferences.CQI_SERVER_PASSWORD, CQPUIMessages.cWBServerColonAmppasswordColon, serverGroup);
		addField(cqi_server_password);
		
		Group cqlGroup = new Group(getFieldEditorParent(), SWT.NONE);
		cqlGroup.setText("CQL");
		GridData gridData3 = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData2.horizontalSpan = 3;
		gridData2.verticalIndent = 10;
		cqlGroup.setLayoutData(gridData3);
		
		if (!OSDetector.isFamilyWindows()) {
			// Log level
			String[][] choices = new String[4][2];
			choices[0][0] = choices[0][1] = "shortest"; //$NON-NLS-1$
			choices[1][0] = choices[1][1] = "standard"; //$NON-NLS-1$
			choices[2][0] = choices[2][1] = "longest"; //$NON-NLS-1$
			choices[3][0] = choices[3][1] = "traditional"; //$NON-NLS-1$
			addField(new ComboFieldEditor(CQPLibPreferences.MATCHINGSTRATEGY, SearchEngineCoreMessages.matchingStrategy, choices, cqlGroup));
		}
		
		cql_fix_query_tokenize = new BooleanFieldEditor(CQPLibPreferences.QUERY_FIX_TOKENIZE, CQPUIMessages.tokenizeWhenFixingCQLQueries, BooleanFieldEditor.DEFAULT, cqlGroup);
		addField(cql_fix_query_tokenize);
		
		//		addField(new StringFieldEditor(CQPLibPreferences.DEFAULTNONBRACKATTR, "Default non-brackets property", getFieldEditorParent()));
		//
		//		addField(new StringFieldEditor(CQPLibPreferences.STRICTREGIONS, "Default strict regions mode", getFieldEditorParent()));

		Preferences preferences = org.eclipse.core.runtime.preferences.InstanceScope.INSTANCE.getNode(Application.PLUGIN_ID);
		boolean useNetwork = preferences.getBoolean(CQPLibPreferences.CQI_NETWORK_MODE, false);
		boolean serverIsRemote = preferences.getBoolean(CQPLibPreferences.CQI_SERVER_IS_REMOTE, false);
		updateFieldsState(useNetwork, serverIsRemote);
		
		((GridLayout)serverGroup.getLayout()).marginWidth = 5;
		((GridLayout)serverGroup.getLayout()).numColumns = 3;
		
		((GridLayout)localGroup.getLayout()).marginWidth = 5;
		((GridLayout)localGroup.getLayout()).numColumns = 3;
		
		((GridLayout)cqlGroup.getLayout()).marginWidth = 5;
		((GridLayout)cqlGroup.getLayout()).numColumns = 3;
	}

	public void performApply() {

		super.performApply();

		updateCQPOptions();
	}

	@Override
	public boolean performOk() {

		boolean ret = super.performOk();
		if (ret) {
			updateCQPOptions();
		}
		return ret;
	}

	private void updateCQPOptions() {

		if (!OSDetector.isFamilyWindows()) {

			for (String pref : Arrays.asList(CQPLibPreferences.MATCHINGSTRATEGY, CQPLibPreferences.DEFAULTNONBRACKATTR, CQPLibPreferences.STRICTREGIONS)) {

				try {
					String newValue = CQPLibPreferences.getInstance().getString(pref);
					String currentValue = CQPSearchEngine.getCqiClient().getOption(pref);
					if (!newValue.equals(currentValue)) {
						Log.info("Updating '" + pref + "' to '" + newValue + "'");
						CQPSearchEngine.getCqiClient().setOption(pref, newValue);
					}
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public void updateFieldsState(boolean useNetwork, boolean serverIsRemote) {

		if (useNetwork) {
			if (serverIsRemote) {
				(cqi_server_path_to_cqplib).setEnabled(false);
				(cqi_server_is_remote).setEnabled(true);
				(cqi_server_host).setEnabled(true);
				(cqi_server_port).setEnabled(true);
				(cqi_server_user).setEnabled(true);
				(cqi_server_password).setEnabled(true);
				(cqi_server_path_to_executable).setEnabled(false);
				//				(cqi_server_path_to_registry).setEnabled(false);
				(cqi_server_path_to_init_file).setEnabled(false);
				(cqi_server_additional_options).setEnabled(false);
			}
			else {
				//cqi_server_host.loadDefault();
				(cqi_server_path_to_cqplib).setEnabled(false);
				(cqi_server_is_remote).setEnabled(true);
				(cqi_server_host).setEnabled(false);
				(cqi_server_port).setEnabled(true);
				(cqi_server_user).setEnabled(true);
				(cqi_server_password).setEnabled(true);
				(cqi_server_path_to_executable).setEnabled(true);
				//				(cqi_server_path_to_registry).setEnabled(true);
				(cqi_server_path_to_init_file).setEnabled(true);
				(cqi_server_additional_options).setEnabled(true);
			}
		}
		else {
			(cqi_server_path_to_cqplib).setEnabled(true);
			(cqi_server_is_remote).setEnabled(false);
			(cqi_server_host).setEnabled(false);
			(cqi_server_port).setEnabled(false);
			(cqi_server_user).setEnabled(false);
			(cqi_server_password).setEnabled(false);
			(cqi_server_path_to_executable).setEnabled(false);
			//			(cqi_server_path_to_registry).setEnabled(true);
			(cqi_server_path_to_init_file).setEnabled(true);
			(cqi_server_additional_options).setEnabled(true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(new CQPLibPreferences().getPreferencesNodeQualifier()));
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#propertyChange(org.eclipse.jface.util.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		boolean useNetwork = false;
		boolean serverIsRemote = false;
		boolean update = false;
		if (((FieldEditor) event.getSource()).getPreferenceName().equals(CQPLibPreferences.CQI_SERVER_IS_REMOTE)) {
			useNetwork = cqi_use_network.getBooleanValue();
			serverIsRemote = (Boolean) event.getNewValue();
			update = true;
		}
		else if (((FieldEditor) event.getSource()).getPreferenceName().equals(CQPLibPreferences.CQI_NETWORK_MODE)) {
			useNetwork = (Boolean) event.getNewValue();
			serverIsRemote = cqi_server_is_remote.getBooleanValue();
			update = true;
		}
		if (update) {
			updateFieldsState(useNetwork, serverIsRemote);
		}
	}
}
