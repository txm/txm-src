// Copyright © 2010-2023 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.searchengine.cqp.rcp.handlers.base;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Partition;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.utils.logger.Log;

/**
 * Command that toggle the visibility of a Partition parts
 * 
 * @author mdecorde
 */
public class ToggleShowParts extends BaseAbstractHandler {


	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands
	 * .ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object selection = this.getCorporaViewSelectedObject(event);

		if (selection instanceof Partition) {
			return togglePartsDisplay((Partition) selection);
		}
		return null;
	}

	/**
	 * 
	 * @param partition the partition parts to display or not
	 * @return
	 * @throws ExecutionException
	 */
	public JobHandler togglePartsDisplay(Partition partition) throws ExecutionException {

		try {
			JobHandler jobhandler = new JobHandler(NLS.bind("Toggle visibility of {0} parts", partition.getName())) {

				@Override
				protected IStatus _run(SubMonitor monitor) {

					monitor.subTask("Toggle parts...");
					for (CorpusBuild part : partition.getParts()) {
						part.setVisible(!part.isVisible());
					}

					monitor.subTask(TXMUIMessages.refreshingCorpora);
					syncExec(new Runnable() {

						@Override
						public void run() {
							CorporaView.refresh();
							CorporaView.expand(partition);
						}
					});
					return Status.OK_STATUS;
				}
			};

			jobhandler.startJob();
			return jobhandler;

		}
		catch (Exception e) {
			Log.severe(NLS.bind(TXMUIMessages.errorWhileCreatingAPartitionColonP0, e));
		}
		return null;
	}
}
