package org.txm.searchengine.cqp.rcp.editor;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPart;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.libs.cqp.CQPLibPreferences;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.listeners.ComputeKeyListener;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.widget.AssistedChoiceQueryWidget;
import org.txm.rcp.swt.widget.structures.ComplexSubcorpusPanel;
import org.txm.rcp.swt.widget.structures.ContextsSubcorpusPanel;
import org.txm.rcp.swt.widget.structures.SimpleSubcorpusPanel;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.Subcorpus;

public class SubcorpusEditor extends TXMEditor<Subcorpus> {

	protected static final String ASSISTANTS_GROUPNAME = "Assistants";

	private Label nameLabel;

	@Parameter(key = TXMPreferences.USER_NAME)

	private Text nameText;

	private Label queryLabel;

	@Parameter(key = TXMPreferences.QUERY)
	private AssistedChoiceQueryWidget queryWidget;

	@Parameter(key = CQPLibPreferences.MATCHINGSTRATEGY)
	private Text matchingStrategy;

	Label matchesInfo;

	Text matchStartsEndsInfo;

	Composite assistantPanel;

	private Group modeGroups;

	private ToolItem refreshQueryButton;

	private Button updateQueryFromAssistance;

	@Override
	public void _createPartControl() throws Exception {

		getSite().getPage().addPartListener(new IPartListener() {

			@Override
			public void partOpened(IWorkbenchPart part) {
			}

			@Override
			public void partDeactivated(IWorkbenchPart part) {
			}

			@Override
			public void partClosed(IWorkbenchPart part) {
				TXMResult r = getResult();
				if (!r.hasBeenComputedOnce()) {
					r.delete();
					CorporaView.refresh();
				}
			}

			@Override
			public void partBroughtToTop(IWorkbenchPart part) {
			}

			@Override
			public void partActivated(IWorkbenchPart part) {
			}
		});

		// Modified computing listeners to manage multi lines query widgets
		ComputeKeyListener computeKeyListener = new ComputeKeyListener(this) {

			@Override
			public void keyPressed(KeyEvent e) {

				if (queryWidget.getQuery() == null) return;
				if (!queryWidget.getQuery().getSearchEngine().hasMultiLineQueries()) super.keyPressed(e);
			}
		};

		getMainParametersComposite().getLayout().numColumns = 6;
		getMainParametersComposite().getLayout().horizontalSpacing = 3;

		nameLabel = new Label(getMainParametersComposite(), SWT.NONE);
		nameLabel.setText(TXMUIMessages.name);
		nameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

		nameText = new Text(this.getMainParametersComposite(), SWT.BORDER);
		GridData gdata = new GridData(SWT.FILL, SWT.CENTER, true, false);
		nameText.setLayoutData(gdata);
		gdata.widthHint = 150;
		nameText.addModifyListener(computeKeyListener);

		queryLabel = new Label(getMainParametersComposite(), SWT.NONE);
		queryLabel.setText(TXMUIMessages.ampQuery);
		queryLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

		queryWidget = new AssistedChoiceQueryWidget(this.getMainParametersComposite(), SWT.BORDER | SWT.DROP_DOWN | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL, getResult());
		gdata = new GridData(SWT.FILL, SWT.FILL, true, true);
		gdata.minimumWidth = 400;
		queryWidget.setLayoutData(gdata);
		queryWidget.addKeyListener(computeKeyListener);
		queryWidget.getQueryWidget().addModifyListener(computeKeyListener);

		queryLabel = new Label(getMainParametersComposite(), SWT.NONE);
		queryLabel.setText("Matching strategy");
		queryLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

		matchingStrategy = new Text(this.getMainParametersComposite(), SWT.BORDER);
		gdata = new GridData(SWT.FILL, SWT.FILL, true, true);
		gdata.minimumWidth = 200;
		matchingStrategy.setLayoutData(gdata);
		matchingStrategy.addKeyListener(computeKeyListener);
		matchingStrategy.addModifyListener(computeKeyListener);

		ArrayList<String> modes = new ArrayList<String>();
		final ArrayList<SelectionListener> listeners = new ArrayList<SelectionListener>();

		// MODE TABS
		SelectionListener openCloseSelectionListener = new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				if (e.getSource() == updateQueryFromAssistance) { // copy query
					Control c = modeGroups.getChildren()[0];
					if (c instanceof SimpleSubcorpusPanel) {
						SimpleSubcorpusPanel ssp = (SimpleSubcorpusPanel) c;
						String q = ssp.getQueryString();
						if (q.length() > 0) {
							queryWidget.setText(q);
							nameText.setText(ssp.getGeneratedName());
						}
					}
					else if (c instanceof ComplexSubcorpusPanel) {
						ComplexSubcorpusPanel ssp = (ComplexSubcorpusPanel) c;
						String q = ssp.getQueryString();
						if (q.length() > 0) {
							queryWidget.setText(q);
							nameText.setText(ssp.getGeneratedName());
						}
					}
				}
				else {

					for (Control c2 : modeGroups.getChildren())
						c2.dispose();

					boolean v = getTopToolbar().isVisible(ASSISTANTS_GROUPNAME);
					if (v) { // not visible remove all composites

						updateQueryFromAssistance.setEnabled(false);
						getTopToolbar().setVisible(ASSISTANTS_GROUPNAME, false);
					}
					else {

						updateQueryFromAssistance.setEnabled(true);
						if (modeGroups.getChildren().length == 0) { // select first mode by default
							listeners.get(0).widgetSelected(e);
						}
					}

					modeGroups.layout(true);
					getResultArea().getParent().layout(true);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};

		modes.add("Simple");
		modes.add("Assisted");
		modes.add("Contexts");

		listeners.add(new SelectionListener() { // create alistener to instantiate a new AnnotationArea

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					for (Control c : modeGroups.getChildren())
						c.dispose(); // clean

					getTopToolbar().setVisible(ASSISTANTS_GROUPNAME, true);

					assistantPanel = new SimpleSubcorpusPanel(modeGroups, SWT.BORDER, null, getResult().getCorpusParent());
					assistantPanel.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

					modeGroups.layout(true);
					getResultArea().getParent().layout(true);
					updateQueryFromAssistance.setEnabled(true);

				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		listeners.add(new SelectionListener() { // create alistener to instantiate a new AnnotationArea

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					for (Control c : modeGroups.getChildren())
						c.dispose(); // clean

					getTopToolbar().setVisible(ASSISTANTS_GROUPNAME, true);

					assistantPanel = new ComplexSubcorpusPanel(modeGroups, SWT.BORDER, getResult().getCorpusParent());
					assistantPanel.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

					modeGroups.layout(true);
					getResultArea().getParent().layout(true);
					updateQueryFromAssistance.setEnabled(true);

				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		listeners.add(new SelectionListener() { // create alistener to instantiate a new AnnotationArea

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					for (Control c : modeGroups.getChildren())
						c.dispose(); // clean

					getTopToolbar().setVisible(ASSISTANTS_GROUPNAME, true);

					assistantPanel = new ContextsSubcorpusPanel(modeGroups, SWT.BORDER, getResult().getCorpusParent());
					assistantPanel.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

					modeGroups.layout(true);
					getResultArea().getParent().layout(true);
					updateQueryFromAssistance.setEnabled(true);

				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		modeGroups = this.getTopToolbar().installAlternativesGroup(ASSISTANTS_GROUPNAME, ASSISTANTS_GROUPNAME, "platform:/plugin/org.txm.searchengine.cqp.rcp/icons/functions/subcorpus_assistant.png",
				null, false, openCloseSelectionListener, modes, listeners);
		modeGroups.setLayout(new GridLayout(1, true));

		// RESULT AREA

		updateQueryFromAssistance = new Button(this.getResultArea(), SWT.PUSH);
		updateQueryFromAssistance.setText("Update query from query assitant");
		updateQueryFromAssistance.addSelectionListener(openCloseSelectionListener);
		updateQueryFromAssistance.setEnabled(false);

		matchesInfo = new Label(this.getResultArea(), SWT.NONE);
		matchesInfo.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));

		matchStartsEndsInfo = new Text(this.getResultArea(), SWT.BORDER | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		matchStartsEndsInfo.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, true));
	}

	protected void refreshQuery() {
		System.out.println("Refresh query...");
	}

	@Override
	public void updateResultFromEditor() {
		// nothing to do
	}

	@Override
	public void updateEditorFromResult(boolean update) throws Exception {

		List<? extends org.txm.objects.Match> matches = getResult().getMatches();
		if (matches == null || matches.size() == 0) {

			if (getResult().hasBeenComputedOnce()) {
				matchesInfo.setText(NLS.bind("{0} match and {1} token", 0, 0));
			}
			else {
				matchesInfo.setText("(not computed yet)");
			}
			matchStartsEndsInfo.setText("");
		}
		else {
			matchesInfo.setText(NLS.bind("{0} match(s) for {1} tokens", this.getResult().getNMatch(), this.getResult().getSize()));
			matchStartsEndsInfo.setText(StringUtils.join(getResult().getMatches(), "\n"));
		}
		matchesInfo.redraw();
		matchStartsEndsInfo.redraw();
		matchStartsEndsInfo.getParent().layout(true);
	}
}
