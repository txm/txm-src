package org.txm.searchengine.cqp.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * CQP UI messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class CQPUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.searchengine.cqp.rcp.messages.messages"; //$NON-NLS-1$

	public static String cQPlibPath;

	public static String useNetworkProtocol;

	public static String ampRemoteCWBServerAddressColon;

	public static String cWBServerColonAmphostColon;

	public static String cWBServerColonAmpportColon;

	public static String cWBServerColonAmploginColon;

	public static String cWBServerColonAmppasswordColon;

	public static String pathToCWBServerBinary;

	public static String pathToCWBServerInitializationFile;

	public static String additionalAmpoptionsForTheCWBServerColon;

	public static String AddExtraFiles;

	public static String AvailablesSubcorpus;

	public static String ChangeThisIdentifierToRenameTheCorpus;

	public static String ClearSelection;

	public static String CorpusIndentifier;

	public static String CorpusVersion;

	public static String DirectoriesNotManagedByTXM;

	public static String ErrorAllPartsAreEmpty;

	public static String ErrorSelectedSubcorpusHaveDifferentMainCorpusP0;

	public static String ErrorSelectionSize2;



	public static String ExportingP0;

	public static String Name;


	public static String OutputFile;


	public static String SelectAll;

	public static String SelectASubcorpusInTheList;

	public static String WarningSomePartsAreEmptyP0;

	public static String optimizedSubcorpusQueriesWontWorkOnFragmentedCorpus;
	public static String optimizeSubcorpusQueries;
	public static String createSubcorpusUsingAnEditor;
	public static String editorAllowToCheckResultingMatches;
	public static String createPartitionsUsingAnEditor;
	public static String editorAllowToCheckResultingParts;

	public static String tokenizeWhenFixingCQLQueries;

	public static String libraryMode;
	public static String serverMode;
	
	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, CQPUIMessages.class);
	}
}
