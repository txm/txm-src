// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.searchengine.cqp.rcp.handlers.base;

import java.io.File;
import java.util.HashSet;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.rcp.dialogs.ExportCorpusDialog;
import org.txm.utils.logger.Log;

/**
 * Export a corpus to the .txm binary format (Zip)
 */
public class ExportCorpus extends AbstractHandler {

	public final static String ID = "org.txm.rcp.commands.base.ExportCorpus"; //$NON-NLS-1$

	/** The selection. */
	private IStructuredSelection selection;

	/** The lastopenedfile. */
	private static String lastopenedfile;

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands
	 * .ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);

		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event)
				.getShell();

		if (!(selection.getFirstElement() instanceof MainCorpus))
			return null;

		final MainCorpus corpus = (MainCorpus) selection.getFirstElement();

		try {
			corpus.compute(false);
		}
		catch (InterruptedException e) {
			Log.printStackTrace(e);
			return null;
		}

		if (corpus.isModified()) {
			boolean ret = MessageDialog.openConfirm(shell, "Export - " + corpus.getName() + " annotations not saved",
					"Do you wish to export without the current annotations?\n\n(to include the current annotations in the exported corpus, you need to save them before calling the \"Export\" command)");
			if (!ret) return null;
		}

		ExportCorpusDialog dialog2 = new ExportCorpusDialog(shell, corpus);

		if (dialog2.open() == Window.OK) {

			final File outfile = dialog2.getZipFile();
			final String version = dialog2.getCorpusVersion();
			if ("0.7.9".equals(version) && dialog2.getRename().length() > 20) { //$NON-NLS-1$
				
				dialog2.setRename(dialog2.getRename().substring(0, 20));
				Log.warning(NLS.bind("The new corpus name has to be reduced to ''{0}'' due to limitations of the cqpserver of old TXM version < 0.8.0.", dialog2.getRename()));
			}
			final String rename = dialog2.getRename();
			final HashSet<String> ignored = dialog2.getIgnoreNames();
			
			

			if (outfile.exists()) {
				boolean ret = MessageDialog.openConfirm(shell, "Export - " + corpus.getName() + " warning",
						TXMUIMessages.bind(TXMUIMessages.theP0FileAlreadyExistsWouldYouLikeToReplaceIt, outfile.getAbsoluteFile()));
				if (!ret) {
					Log.info(TXMUIMessages.abort);
					return null;
				}
			}

			Log.info(NLS.bind(TXMUIMessages.exportingP0, corpus.getName()));
			JobHandler jobhandler = new JobHandler(NLS.bind(TXMUIMessages.exportingP0, corpus.getName())) {

				@Override
				protected IStatus _run(SubMonitor monitor) {

					monitor.beginTask(NLS.bind(TXMUIMessages.exportingP0ThisMayTakeAWhile, corpus.getName()), 100);

					if (!corpus.export(outfile, this, ignored, version, rename)) {
						Log.warning(NLS.bind(TXMUIMessages.failedToExportCorpusP0, corpus.getName()));
					}

					Log.info(TXMUIMessages.bind(TXMUIMessages.doneTheCreatedBinaryFileIsP0, outfile.getAbsolutePath()));
					return Status.OK_STATUS;
				}
			};
			jobhandler.startJob();
		}

		return null;
	}
}
