// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.searchengine.cqp.rcp.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.TXMResult;
import org.txm.rcp.Application;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.adapters.TXMResultAdapter;
import org.txm.rcp.adapters.TXMResultAdapterFactory;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Partition;


/**
 * A factory for creating Adapter objects.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class CorpusAdapterFactory extends TXMResultAdapterFactory {



	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		
		if (adapterType == IWorkbenchAdapter.class && adaptableObject instanceof CQPCorpus) {
			return new TXMResultAdapter() {

				@Override
				public Object[] getChildren(Object corpus) {

					// List<TXMResult> allChildren = ((CQPCorpus) corpus).getChildren();
					List<TXMResult> allChildren = ((TXMResult) corpus).getChildren(!TBXPreferences.getInstance().getBoolean(TBXPreferences.SHOW_ALL_RESULT_NODES));

					ArrayList<Partition> partitions = new ArrayList<>();
					ArrayList<CQPCorpus> subcorporas = new ArrayList<>();
					ArrayList<TXMResult> resultsChildren = new ArrayList<>();
					ArrayList<Object> hiddentResultsChildren = new ArrayList<>();
					ArrayList<Object> children = new ArrayList<>();

					// FIXME: remove Text and SavedQuery from the corpora view
					// + sort and group the partitions and subcorpus
					for (int i = 0; i < allChildren.size(); i++) {
						Object element = allChildren.get(i);
						// partitions
						if (element instanceof Partition) {
							partitions.add((Partition) element);
						}
						// corpus
						else if (element instanceof CQPCorpus) {
							subcorporas.add((CQPCorpus) element);
						}
						// results
						else if (((TXMResult) element).isVisible()) {
							resultsChildren.add((TXMResult) element);
						}
						// hidden results
						else if (TBXPreferences.getInstance().getBoolean(TBXPreferences.SHOW_ALL_RESULT_NODES)) {
							hiddentResultsChildren.add(element);
						}

					}

					Collections.sort(partitions);
					Collections.sort(subcorporas);

					children.addAll(partitions);
					children.addAll(subcorporas);
					children.addAll(resultsChildren);
					children.addAll(hiddentResultsChildren);

					return children.toArray();
				}

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					// if (object instanceof MainCorpus
					// && !((MainCorpus) object).hasBeenComputed()) { // not working because of actual management of the CorpusManager (all Corpus are loaded and there is not 'not ready state')
					// return AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, IImageKeys.CORPUS_BROKEN);
					// }
					// else {
					return AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, IImageKeys.CORPUS);
					// }
				}
			};
		}
		return null;
	}



}
