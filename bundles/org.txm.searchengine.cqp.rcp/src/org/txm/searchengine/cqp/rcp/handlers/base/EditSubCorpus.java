// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.searchengine.cqp.rcp.handlers.base;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.rcp.editor.SubcorpusEditor;

/**
 * Command which creates a subcorpus from a Corpus
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class EditSubCorpus extends BaseAbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkCorpusEngine()) {
			return false;
		}

		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		if (selection == null) {
			System.out.println(TXMUIMessages.noSelectionForColon + HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActiveEditor());
			return null;
		}

		if (selection.getFirstElement() instanceof Subcorpus) {
			return edit((Subcorpus) selection.getFirstElement());
		}
		else {
			return null;
		}
	}

	public static Object edit(Subcorpus sub) throws ExecutionException {

		return TXMEditor.openEditor(sub, SubcorpusEditor.class.getName());
	}
}
