package org.txm.searchengine.cqp.rcp.dialogs;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.rcp.messages.CQPUIMessages;

/**
 * 
 * 
 *
 */
public class SetOperationsDialog extends Dialog {

	private Text textField;

	String operation;

	CQPCorpus corpus[] = new CQPCorpus[2];

	String name;

	//	private Label corpus1Label;
	//	private Label corpus2Label;
	private String previousDefaultName;

	private TreeViewer treeViewer;

	//	private Label questionLabel;
	private TreeViewerColumn nameColumn;

	public static HashMap<String, String> operations = new HashMap<String, String>();
	static {
		operations.put("UNION", "''{0}'' subcorpus union with..."); //$NON-NLS-1$ //$NON-NLS-2$
		operations.put("INTER", "''{0}'' subcorpus intersection with..."); //$NON-NLS-1$ //$NON-NLS-2$
		operations.put("MINUS", "''{0}'' subcorpus minus"); //$NON-NLS-1$ //$NON-NLS-2$
		operations.put("COMPL", "''{0}'' subcorpus complement..."); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public static HashMap<String, String> symbols = new HashMap<String, String>();
	static {
		symbols.put("UNION", "{0} ∪ {1}"); //$NON-NLS-1$ //$NON-NLS-2$
		symbols.put("INTER", "{0} ∩ {1}"); //$NON-NLS-1$ //$NON-NLS-2$
		symbols.put("MINUS", "{0} - {1}"); //$NON-NLS-1$ //$NON-NLS-2$
		symbols.put("COMPL", "-{0}"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * 
	 * @param parentShell
	 */
	public SetOperationsDialog(Shell parentShell, String operation, CQPCorpus corpus1) {
		super(parentShell);
		this.operation = operation;
		this.corpus[0] = corpus1;
		if (this.operation.equals("COMPL")) { //$NON-NLS-1$
			this.corpus[1] = corpus1.getCorpusParent();
		}
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(NLS.bind(operations.get(operation), corpus[0].getName()));
		newShell.setMinimumSize(400, 200);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite comp = (Composite) super.createDialogArea(parent);

		GridLayout layout = (GridLayout) comp.getLayout();
		layout.numColumns = 2;

		Label l = new Label(comp, SWT.LEFT);
		l.setText(CQPUIMessages.Name);
		l.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false));

		textField = new Text(comp, SWT.SINGLE | SWT.BORDER);
		textField.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));

		treeViewer = new TreeViewer(comp, SWT.SINGLE | SWT.BORDER);
		if (this.operation.equals("COMPL")) { //$NON-NLS-1$
			treeViewer.getTree().setEnabled(false);
		}
		treeViewer.setContentProvider(new ITreeContentProvider() {

			@Override
			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			}

			@Override
			public void dispose() {
			}

			@Override
			public boolean hasChildren(Object element) {
				if (element instanceof CQPCorpus) {
					return ((CQPCorpus) element).getSubcorpora().size() > 0;
				}
				return false;
			}

			@Override
			public Object getParent(Object element) {
				if (element instanceof CQPCorpus) {
					return ((CQPCorpus) element).getParent();
				}
				return null;
			}

			@Override
			public Object[] getElements(Object inputElement) {
				if (inputElement == null) return new Object[0];

				if (inputElement instanceof CQPCorpus) {
					List<Subcorpus> c = ((CQPCorpus) inputElement).getSubcorpora();
					ArrayList<Subcorpus> corpora = new ArrayList<Subcorpus>(c);
					corpora.remove(corpus[0]);
					return corpora.toArray();
				}
				return null;
			}

			@Override
			public Object[] getChildren(Object element) {
				if (element instanceof CQPCorpus) {
					return ((CQPCorpus) element).getSubcorpora().toArray();
				}
				return new Object[0];
			}
		});

		treeViewer.getTree().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 2, 1));
		treeViewer.getTree().setHeaderVisible(true);
		treeViewer.getTree().setLinesVisible(true);
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection sel = event.getStructuredSelection();
				Object o = sel.getFirstElement();
				corpus[1] = (CQPCorpus) o;
				initializeWidgets();
			}
		});

		nameColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		nameColumn.getColumn().setText(CQPUIMessages.AvailablesSubcorpus);
		nameColumn.getColumn().setWidth(150);
		nameColumn.setLabelProvider(new CellLabelProvider() {

			@Override
			public void update(ViewerCell cell) {
				Object element = cell.getElement();

				if (element instanceof CQPCorpus) {
					cell.setText(((CQPCorpus) element).getName());
				}
				else {
					cell.setText(""); //$NON-NLS-1$
				}
			}
		});

		treeViewer.setInput(corpus[0].getMainCorpus());
		initializeWidgets();

		return comp;
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	public void initializeWidgets() {
		//		corpus1Label.setText(corpus[0].getName());
		//		corpus2Label.setText(corpus[1].getName());
		//		questionLabel.setText(NLS.bind(symbols.get(operation), corpus[0], corpus[1]));
		//textField.getShell().setText(NLS.bind(symbols.get(operation), corpus[0], corpus[1]));
		String current = textField.getText();
		if (current.length() == 0 || current.equals(previousDefaultName)) {
			textField.setText(getDefaultName());
		}
	}

	public String getDefaultName() {
		String s = CQPUIMessages.SelectASubcorpusInTheList;
		if (corpus[1] != null) {
			s = corpus[1].getSimpleName();
		}
		previousDefaultName = NLS.bind(symbols.get(operation), corpus[0].getSimpleName(), s);
		return previousDefaultName;
	}

	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == Dialog.OK) {
			name = textField.getText();
		}
		super.buttonPressed(buttonId);
	}

	public String getName() {
		return name;
	}

	public CQPCorpus[] getCorpusOrder() {
		return corpus;
	}
}
