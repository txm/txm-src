package org.txm.searchengine.cqp.rcp.editor;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.editors.listeners.ComputeKeyListener;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.widget.AssistedQueryWidget;
import org.txm.rcp.swt.widget.PartitionComposer;
import org.txm.rcp.swt.widget.structures.CrossedPartitionPanel;
import org.txm.rcp.swt.widget.structures.SimplePartitionPanel;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Part;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.utils.logger.Log;

public class PartitionEditor extends TXMEditor<Partition> {

	protected static final String ASSISTANTS_GROUPNAME = "Assistants";

	TableViewer partsViewer;

	TableViewerColumn nameColumn;

	TableViewerColumn queryColumn;

	TableViewerColumn sizeColumn;

	private Label miniInfoLabel;

	@Parameter(key = TXMPreferences.USER_NAME)
	private Text nameWidget;

	private Text partNameWidget;

	private AssistedQueryWidget queryWidget;

	private Group modeGroups;

	private Button updateQueryFromAssistance;

	private Composite assistantPanel;

	@SuppressWarnings("deprecation")
	@Override
	public void _createPartControl() throws Exception {

		getResult().compute(false); // ensure Parts are ready

		ComputeKeyListener computeKeyListener = new ComputeKeyListener(this);

		getMainParametersComposite().getLayout().numColumns = 7;
		getMainParametersComposite().getLayout().horizontalSpacing = 2;

		Label l = new Label(getMainParametersComposite(), SWT.NONE);
		l.setText(TXMUIMessages.name);
		l.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

		nameWidget = new Text(getMainParametersComposite(), SWT.BORDER);
		GridData gdata = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		gdata.minimumWidth = 300;
		nameWidget.setLayoutData(gdata);
		nameWidget.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				getResult().setUserName(nameWidget.getText());
				getResult().saveParameter(TXMPreferences.USER_NAME, nameWidget.getText());

				CorporaView.refreshObject(getResult());
			}
		});

		l = new Label(getMainParametersComposite(), SWT.SEPARATOR | SWT.VERTICAL);
		l.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

		miniInfoLabel = new Label(getMainParametersComposite(), SWT.NONE);
		miniInfoLabel.setText("Add a part");
		miniInfoLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

		partNameWidget = new Text(getMainParametersComposite(), SWT.BORDER);
		gdata = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		gdata.minimumWidth = 300;
		partNameWidget.setLayoutData(gdata);

		// [ (v)]
		// queryWidget = new QueryWidget(queryArea, SWT.DROP_DOWN);
		queryWidget = new AssistedQueryWidget(getMainParametersComposite(), SWT.DROP_DOWN, getResult().getCorpus());
		gdata = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		gdata.minimumWidth = 400;
		queryWidget.setLayoutData(gdata);
		queryWidget.addKeyListener(new ComputeKeyListener(this) {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					try {
						if (queryWidget.getQueryString().length() > 0 && onPlusButtonPressed(null, queryWidget.getQueryString())) {
							super.keyPressed(e); // recompute only if the query has been added
							queryWidget.clearQuery();
						}
						else {
							queryWidget.setText(queryWidget.getQueryString());
						}
					}
					catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		queryWidget.getQueryWidget().addModifyListener(computeKeyListener);

		Button addNewQueryButton = new Button(getMainParametersComposite(), SWT.PUSH);
		addNewQueryButton.setText("+");
		addNewQueryButton.setToolTipText("Add a new part given a query");
		addNewQueryButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
		addNewQueryButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					if (queryWidget.getQueryString().length() > 0 && onPlusButtonPressed(null, queryWidget.getQueryString())) {
						compute(true);
						queryWidget.clearQuery();
					}
					else {
						queryWidget.setText(queryWidget.getQueryString());
					}
				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		ArrayList<String> modes = new ArrayList<String>();
		final ArrayList<SelectionListener> listeners = new ArrayList<SelectionListener>();

		// MODE TABS
		SelectionListener openCloseSelectionListener = new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				if (e.getSource() == updateQueryFromAssistance) { // copy query
					Control c = modeGroups.getChildren()[0];

					if (c instanceof SimplePartitionPanel) {

						SimplePartitionPanel ssp = (SimplePartitionPanel) c;
						ssp.createPartition("", getResult());
					}
					else if (c instanceof CrossedPartitionPanel) {

						CrossedPartitionPanel ssp = (CrossedPartitionPanel) c;
						ssp.createPartition("", getResult());
					}
					else if (c instanceof PartitionComposer) {

						PartitionComposer ssp = (PartitionComposer) c;
						ssp.createPartition(nameWidget.getText(), getResult());
					}

					compute(false);

					try {
						updateEditorFromResult(false);
					}
					catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
				else {

					for (Control c2 : modeGroups.getChildren())
						c2.dispose();

					boolean v = getTopToolbar().isVisible(ASSISTANTS_GROUPNAME);
					if (v) { // not visible remove all composites
						updateQueryFromAssistance.setEnabled(false);
						getTopToolbar().setVisible(ASSISTANTS_GROUPNAME, false);
					}
					else {
						updateQueryFromAssistance.setEnabled(true);
						if (modeGroups.getChildren().length == 0) { // select first mode by default
							listeners.get(0).widgetSelected(e);
						}
					}

					modeGroups.layout(true);
					getResultArea().getParent().layout(true);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};

		modes.add("Simple");
		modes.add("Assisted");
		modes.add("Crossed");

		listeners.add(new SelectionListener() { // create alistener to instantiate a new AnnotationArea

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					for (Control c : modeGroups.getChildren())
						c.dispose(); // clean

					getTopToolbar().setVisible(ASSISTANTS_GROUPNAME, true);

					assistantPanel = new SimplePartitionPanel(modeGroups, SWT.BORDER, getResult().getCorpus());
					assistantPanel.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

					modeGroups.layout(true);
					getResultArea().getParent().layout(true);
					updateQueryFromAssistance.setEnabled(true);

				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		listeners.add(new SelectionListener() { // create alistener to instantiate a new AnnotationArea

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					for (Control c : modeGroups.getChildren())
						c.dispose(); // clean

					getTopToolbar().setVisible(ASSISTANTS_GROUPNAME, true);

					assistantPanel = new PartitionComposer(modeGroups, SWT.BORDER, getResult().getCorpus());
					assistantPanel.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

					modeGroups.layout(true);
					getResultArea().getParent().layout(true);
					updateQueryFromAssistance.setEnabled(true);

				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		listeners.add(new SelectionListener() { // create alistener to instantiate a new AnnotationArea

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					for (Control c : modeGroups.getChildren())
						c.dispose(); // clean

					getTopToolbar().setVisible(ASSISTANTS_GROUPNAME, true);

					assistantPanel = new CrossedPartitionPanel(modeGroups, SWT.BORDER, getResult().getCorpus());
					assistantPanel.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

					modeGroups.layout(true);
					getResultArea().getParent().layout(true);
					updateQueryFromAssistance.setEnabled(true);

				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		modeGroups = this.getTopToolbar().installAlternativesGroup(ASSISTANTS_GROUPNAME, ASSISTANTS_GROUPNAME, IImageKeys.ACTION_ASSISTQUERY, null, false, openCloseSelectionListener, modes,
				listeners);
		modeGroups.setLayout(new GridLayout(1, true));

		// RESULT AREA

		this.getResultArea().getLayout().verticalSpacing = 2;

		GLComposite buttons = new GLComposite(this.getResultArea(), SWT.NONE);
		buttons.getLayout().numColumns = 10;
		buttons.getLayout().horizontalSpacing = 2;

		Button deleteAllButton = new Button(buttons, SWT.PUSH);
		deleteAllButton.setText("Clear");
		deleteAllButton.setToolTipText("Remove all parts");
		deleteAllButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				for (Part p : getResult().getParts()) {
					p.delete();
				}

				try {
					updateEditorFromResult(false);
				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		updateQueryFromAssistance = new Button(buttons, SWT.PUSH);
		updateQueryFromAssistance.setText("Add parts from from assitant");
		updateQueryFromAssistance.addSelectionListener(openCloseSelectionListener);
		updateQueryFromAssistance.setEnabled(false);

		partsViewer = new TableViewer(this.getResultArea(), SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
		partsViewer.getTable().setLayoutData(GridDataFactory.fillDefaults().align(GridData.FILL, GridData.FILL).grab(true, true).create());
		partsViewer.getTable().setHeaderVisible(true);
		partsViewer.getTable().setLinesVisible(true);

		partsViewer.setSorter(new ViewerSorter() {

			@Override
			public int compare(Viewer viewer, Object e1, Object e2) {
				Part p1 = (Part) e1;
				Part p2 = (Part) e2;
				int s = 0;
				if (partsViewer.getTable().getSortColumn() == nameColumn.getColumn()) {
					s = p1.getName().compareTo(p2.getName());
				}
				else if (partsViewer.getTable().getSortColumn() == queryColumn.getColumn()) {
					s = p1.getQuery().getQueryString().compareTo(p2.getQuery().getQueryString());
				}
				else if (partsViewer.getTable().getSortColumn() == sizeColumn.getColumn()) {
					try {
						s = p1.getSize() - p2.getSize();
					}
					catch (CqiClientException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				if (partsViewer.getTable().getSortDirection() == SWT.DOWN) s = -s;

				return s;
			}
		});

		partsViewer.getTable().addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.DEL) {
					ISelection isel = partsViewer.getSelection();
					IStructuredSelection sel = (IStructuredSelection) isel;
					for (Object o : sel.toList()) {
						if (o != null && o instanceof Part) {
							Part p = (Part) o;
							p.delete();
						}
					}

					try {
						updateEditorFromResult(false);
					}
					catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		partsViewer.setContentProvider(new ITreeContentProvider() {

			@Override
			public Object[] getElements(Object inputElement) {
				if (inputElement instanceof Partition) {
					return ((Partition) inputElement).getParts().toArray();
				}

				return new Object[0];
			}

			@Override
			public Object[] getChildren(Object parentElement) {
				return new Object[0];
			}

			@Override
			public Object getParent(Object element) {
				return getResult();
			}

			@Override
			public boolean hasChildren(Object element) {
				return false;
			}
		});

		nameColumn = new TableViewerColumn(partsViewer, SWT.NONE);
		nameColumn.getColumn().setText(TXMCoreMessages.common_name);
		nameColumn.getColumn().pack();
		nameColumn.setLabelProvider(new CellLabelProvider() {

			@Override
			public void update(ViewerCell cell) {
				Object element = cell.getElement();

				if (element instanceof Part) {
					Part part = (Part) element;
					cell.setText(part.getName());
				}
				else {
					cell.setText("not part");
				}
			}
		});
		nameColumn.setEditingSupport(new EditingSupport(partsViewer) {
			
			@Override
			protected void setValue(Object element, Object value) {
				
				if (value == null) return;
				
				if (element instanceof Part part) {
					part.setName(value.toString());
					partsViewer.refresh(part);
				}
			}
			
			@Override
			protected Object getValue(Object element) {
				
				if (element instanceof Part part) {
					return part.getName();
				}
				
				return null;
			}
			
			@Override
			protected CellEditor getCellEditor(Object element) {
				
				if (element instanceof Part part) {
					return new DialogCellEditor(partsViewer.getTable()) {
						
						@Override
						protected Object openDialogBox(Control cellEditorWindow) {
							InputDialog dialog = new InputDialog(cellEditorWindow.getShell(), "Name", "Set the new name", part.getName(), null);
							if (dialog.open() == InputDialog.OK) {
								return dialog.getValue();
							} else {
								return null;
							}
						}
					};
				}
				
				return null;
			}
			
			@Override
			protected boolean canEdit(Object element) {
				
				return true;
			}
		});

		SelectionListener colSelection = new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				TableColumn col = (TableColumn) e.widget;
				Table table = partsViewer.getTable();
				if (table.getSortColumn() == col) {
					if (table.getSortDirection() == SWT.DOWN) {
						table.setSortDirection(SWT.UP);
					}
					else {
						table.setSortDirection(SWT.DOWN);
					}
				}
				else {
					table.setSortDirection(SWT.DOWN);
				}

				table.setSortColumn(col);
				partsViewer.refresh();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		};

		queryColumn = new TableViewerColumn(partsViewer, SWT.NONE);
		queryColumn.getColumn().setText(TXMCoreMessages.common_query);
		queryColumn.getColumn().pack();
		queryColumn.setLabelProvider(new CellLabelProvider() {

			@Override
			public void update(ViewerCell cell) {
				
				Object element = cell.getElement();

				if (element instanceof Part part) {
					cell.setText(part.getQuery().toString());
				}
				else {
					cell.setText("not part");
				}
			}
		});
		
		queryColumn.setEditingSupport(new EditingSupport(partsViewer) {
			
			@Override
			protected void setValue(Object element, Object value) {
				if (value == null) return;
				
				if (element instanceof Part part) {
					part.getQuery().setQuery(value.toString());
					try {
						part.setDirty();
						part.compute();
						partsViewer.refresh(part);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
			@Override
			protected Object getValue(Object element) {
				if (element instanceof Part part) {
					return part.getQuery().getQueryString();
				}
				
				return null;
			}
			
			@Override
			protected CellEditor getCellEditor(Object element) {
				if (element instanceof Part part) {
					return new DialogCellEditor(partsViewer.getTable()) {
						
						@Override
						protected Object openDialogBox(Control cellEditorWindow) {
							InputDialog dialog = new InputDialog(cellEditorWindow.getShell(), "Query", "Set the new query", part.getQuery().getQueryString(), null);
							if (dialog.open() == InputDialog.OK) {
								return dialog.getValue();
							} else {
								return null;
							}
						}
					};
				}
				
				return null;
			}
			
			@Override
			protected boolean canEdit(Object element) {
				return true;
			}
		});

		sizeColumn = new TableViewerColumn(partsViewer, SWT.NONE);
		sizeColumn.getColumn().setText(TXMCoreMessages.common_size);
		sizeColumn.getColumn().pack();
		sizeColumn.setLabelProvider(new CellLabelProvider() {

			@Override
			public void update(ViewerCell cell) {
				Object element = cell.getElement();

				if (element instanceof Part) {
					Part part = (Part) element;
					try {
						cell.setText(String.valueOf(part.getSize()));
					}
					catch (CqiClientException e) {
						Log.printStackTrace(e);
					}
				}
				else {
					cell.setText("not part");
				}
			}
		});

		TableViewerColumn tmpColumn = new TableViewerColumn(partsViewer, SWT.NONE);
		tmpColumn.getColumn().setText("");
		tmpColumn.getColumn().pack();
		tmpColumn.setLabelProvider(new CellLabelProvider() {

			private String EMPTY = ""; //$NON-NLS-1$

			@Override
			public void update(ViewerCell cell) {
				cell.setText(EMPTY);
			}
		});

		nameColumn.getColumn().addSelectionListener(colSelection);
		queryColumn.getColumn().addSelectionListener(colSelection);
		sizeColumn.getColumn().addSelectionListener(colSelection);

		partsViewer.setInput(getResult());

		TableUtils.packColumns(this.partsViewer);
	}

	/**
	 * 
	 * @param event
	 * @param query
	 * @return
	 * @throws Exception
	 */
	public boolean onPlusButtonPressed(Event event, String query) throws Exception {
		
		Log.info(NLS.bind("Add parts: {0}.", query));
		
		String pName = partNameWidget.getText().trim();
		if (pName.length() == 0) {
			Log.warning("No part name set.");
			return false;
		}
		if (query.length() == 0) {
			Log.warning("No part query set.");
			return false;
		}
		if (getResult().getPartForName(pName) != null) {
			Log.warning(NLS.bind("A part with the ''{0}'' name already exists.", pName));
			return false;
		}

		Part p = new Part(getResult(), pName, queryWidget.getQueryString());
		if (p.compute()) { // the part is OK, update the partition parameters
			getResult().getQueriesParameter().add(query);
			getResult().getPartNamesParameter().add(pName);
			compute(false);

			updateEditorFromResult(false);
		}
		else {
			Log.warning(NLS.bind("Can not compute the part with query: {0}.", query));
			p.delete();
		}

		return false;
	}

	/**
	 * 
	 * @return success state
	 */
	public boolean addMultiParts(ArrayList<String> queries, ArrayList<String> names) {
		
		Log.info(NLS.bind("Add parts: {0} {1}.", queries, names));

		if (queries.size() == 0) {
			Log.warning("Error: no queries.");
			return false;
		}
		if (names.size() == 0) {
			Log.warning("Error: no names.");
			return false;
		}
		if (names.size() != queries.size()) {
			Log.warning("Error: names and queries sizes differ.");
			return false;
		}
		try {
			for (int i = 0; i < names.size(); i++) {
				Part p = new Part(getResult(), names.get(i), queries.get(i));

				if (p.compute() && p.getSize() > 0) { // the part is OK, update the partition parameters
					getResult().getQueriesParameter().add(queries.get(i));
					getResult().getPartNamesParameter().add(names.get(i));
				}
				else {
					p.delete(); // remove bad results
				}
			}

			compute(false);

			updateEditorFromResult(false);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public void updateResultFromEditor() {
		// nothing to do
	}

	@Override
	public void updateEditorFromResult(boolean update) throws Exception {
		partsViewer.setInput(getResult());
		TableUtils.packColumns(this.partsViewer);
	}
}
