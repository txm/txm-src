// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.searchengine.cqp.rcp.preferences;


import org.eclipse.ui.IWorkbench;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.searchengine.cqp.core.preferences.PartitionPreferences;
import org.txm.searchengine.cqp.rcp.messages.CQPUIMessages;

/**
 * CQP search engine preferences page.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class PartitionPreferencePage extends TXMPreferencePage {

	/** The cqi_server_is_remote. */
	private BooleanFieldEditor editor_mode;

	/**
	 * Instantiates a new cQP preference page.
	 */
	public PartitionPreferencePage() {
		super();
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
			editor_mode = new BooleanFieldEditor(PartitionPreferences.EDITOR_MODE, CQPUIMessages.createPartitionsUsingAnEditor, BooleanFieldEditor.DEFAULT, getFieldEditorParent());
			editor_mode.setToolTipText(CQPUIMessages.editorAllowToCheckResultingParts);
			addField(editor_mode);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(new PartitionPreferences().getPreferencesNodeQualifier()));
	}
}
