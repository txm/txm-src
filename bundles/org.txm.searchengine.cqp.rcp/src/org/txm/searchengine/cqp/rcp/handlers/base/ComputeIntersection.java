// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.searchengine.cqp.rcp.handlers.base;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.core.messages.CQPSearchEngineCoreMessages;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.rcp.dialogs.SetOperationsDialog;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * Command which creates a subcorpus from a Corpus @ author mdecorde.
 */
public class ComputeIntersection extends AbstractHandler {

	/** The selection. */
	private IStructuredSelection selection;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		if (selection == null) {
			Log.warning(TXMUIMessages.noSelectionForColon + HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActiveEditor());
			return null;
		}
		List<?> list = selection.toList();
		if (list.size() == 0) {
			Log.info("No corpus selected");
			return null;
		}
		else if (list.size() != 1) {
			Log.info("Intersection need a selection of 1 corpus");
			return null;
		}
		Object firstElement = list.get(0);
		// Object secondElement = list.get(1);

		if (!(firstElement instanceof CQPCorpus)) {
			Log.warning(TXMUIMessages.selectionIsNotACorpusColon + firstElement);
			return null;
		}

		// if (!(secondElement instanceof CQPCorpus)) {
		// System.out.println(TXMUIMessages.selectionIsNotACorpusColon+secondElement);
		// return null;
		// }

		final CQPCorpus corpus = (CQPCorpus) firstElement;
		try {
			corpus.compute(false);
		}
		catch (InterruptedException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			return null;
		}
		// final CQPCorpus corpus2 = (CQPCorpus) secondElement;
		// corpus2.compute(false);

		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		final SetOperationsDialog d = new SetOperationsDialog(shell, "INTER", corpus);
		int code = d.open();
		if (code == Window.OK) {
			JobHandler jobhandler = new JobHandler(NLS.bind(TXMUIMessages.creatingASubcorpusOnP0, corpus.getName())) {

				@Override
				protected IStatus _run(SubMonitor monitor) {

					try {
						monitor.beginTask(TXMUIMessages.creatingASubcorpusInTheSearchEngine, 100);
						String name = d.getName();
						if (name.length() == 0) {
							name = d.getDefaultName();
						}

						CQPCorpus[] order = d.getCorpusOrder();

						Subcorpus subcorpus = Subcorpus.intersect(corpus, name, (Subcorpus) order[0], (Subcorpus) order[1]);

						if (subcorpus != null && subcorpus.getSize() == 0) {
							Log.info("The created subcorpus was empty. Aborting.");
							subcorpus.delete();
							return Status.CANCEL_STATUS;
						}

						if (subcorpus == null) {
							monitor.done();
							Log.warning(TXMUIMessages.errorColonSubcorpusWasNotCreated);
							return Status.CANCEL_STATUS;
						}

						System.out.println(NLS.bind(TXMUIMessages.doneColonP0Created, subcorpus.getName()));
						monitor.worked(50);

						monitor.subTask(TXMUIMessages.refreshingCorpora);
						final Subcorpus subcorpus2 = subcorpus;
						syncExec(new Runnable() {

							@Override
							public void run() {
								CorporaView.refresh();
								CorporaView.expand(subcorpus2.getParent());
							}
						});

						monitor.worked(100);
					}
					catch (ThreadDeath td) {
						return Status.CANCEL_STATUS;
					}
					catch (Exception e) {
						org.txm.utils.logger.Log.printStackTrace(e);
						Log.severe(e.toString());

						try {
							System.out.println(NLS.bind(CQPSearchEngineCoreMessages.lastCQPErrorP0, CQPSearchEngine.getCqiClient().getLastCQPError()));
						}
						catch (Exception e1) {
							System.out.println(TXMUIMessages.failedToGetLastCQPErrorColon + e1);
							org.txm.utils.logger.Log.printStackTrace(e1);
						}
						return Status.CANCEL_STATUS;
					}
					return Status.OK_STATUS;
				}
			};
			jobhandler.startJob();
		}
		return null;
	}
}
