// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.searchengine.cqp.rcp.handlers.base;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.rcp.JobsTimer;
import org.txm.rcp.actions.CreatePartitionDialog;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.ComputeProgressMonitorDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.core.preferences.PartitionPreferences;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Part;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Partitionable;
import org.txm.searchengine.cqp.rcp.editor.PartitionEditor;
import org.txm.searchengine.cqp.rcp.messages.CQPUIMessages;
import org.txm.utils.logger.Log;

/**
 * Command that creates a Partition from a Corpus.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class ComputePartition extends BaseAbstractHandler {

	/**
	 * Partition.
	 */
	protected Partition partition;

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands
	 * .ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkCorpusEngine()) {
			return false;
		}

		// FIXME: SJ: tests of MD for using a TXMEditor rather than a dialog box to manage partitions creation and edition
		if (PartitionPreferences.getInstance().getBoolean(PartitionPreferences.EDITOR_MODE)) {
			return execute_editor(event);
		}
		// original code that uses a dialog box
		else {
			// since some widgets fields need some values of parents branch,
			// ensure the parents branch is ready
			// show modal blocking cancelable progression dialog
			try {

				Object selection = this.getCorporaViewSelectedObject(event);

				if (selection instanceof CQPCorpus) {
					CQPCorpus corpus = (CQPCorpus) selection;
					if (!corpus.hasBeenComputedOnce()) {

						ComputeProgressMonitorDialog dialog = new ComputeProgressMonitorDialog(HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell(), corpus);
						dialog.runComputingProcess(true);

					}
				}
				else if (selection instanceof Partitionable partitionable) {

					JobHandler job = new JobHandler(NLS.bind("Creating partition from {0} ", partitionable.getSimpleName())) {

						@Override
						protected IStatus _run(SubMonitor monitor) throws Exception {
							Partition p = null;
							try {
								partitionable.compute();

								String[] queries = partitionable.getQueryStringsforPartition();
								String[] names = partitionable.getNamesForPartition();
								String name = partitionable.getNameForPartition();
								
								if (queries == null || queries.length == 0) {
									Log.warning(NLS.bind("Error: can not create partition from {0}: no queries.", partitionable.getSimpleName()));
									return Status.CANCEL_STATUS;
								}
								if (names == null || names.length == 0) {
									Log.warning(NLS.bind("Error: can not create partition from {0}: no names.", partitionable.getSimpleName()));
									return null;
								}
								if (name == null || name.length() == 0) {
									Log.warning(NLS.bind("Error: can not create partition from {0}: no name.", partitionable.getSimpleName()));
									return Status.CANCEL_STATUS;
								}
								
								p = new Partition(partitionable.getCorpus());
								p.setParameters(name, Arrays.asList(queries), Arrays.asList(names));

								if (!p.compute(monitor, true)) {
									Log.warning(NLS.bind("Error: cannot create partition from {0}.", partitionable.getSimpleName()));
									return Status.CANCEL_STATUS;
								}
							}
							catch (Exception e) {
								Log.warning(NLS.bind("Error: cannot create partition from {0}: {1}.", partitionable.getSimpleName(), e));
								if (p != null) p.delete();
								throw e;
							}

							this.syncExec(new Runnable() {
								@Override
								public void run() {
									CorporaView.refresh();
								}
							});

							return Status.OK_STATUS;
						}
					};
					job.schedule();
					return job;
				}

			}
			// Canceling or error
			catch (InterruptedException | InvocationTargetException e) {
				CorporaView.refresh();
				return Status.CANCEL_STATUS;
			}

			return execute_dialog(event);
		}
	}

	/**
	 * 
	 * @param event
	 * @return
	 * @throws ExecutionException
	 */
	public Object execute_editor(ExecutionEvent event) throws ExecutionException {
		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		if (selection == null) {
			Log.warning(TXMUIMessages.noSelectionForColon + HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActiveEditor());
			return null;
		}
		Object firstElement = selection.getFirstElement();
		Partition part = null;
		if (firstElement instanceof CQPCorpus) {
			part = new Partition((CQPCorpus) firstElement);
			part.setParameters("", new ArrayList<String>(), new ArrayList<String>()); //$NON-NLS-1$
		}
		else if (firstElement instanceof Partition) {
			part = (Partition) firstElement;
		}
		return TXMEditor.openEditor(part, PartitionEditor.class.getName());
	}

	/**
	 * 
	 * @param event
	 * @return
	 * @throws ExecutionException
	 */
	public Object execute_dialog(ExecutionEvent event) throws ExecutionException {
		ISelection isel = HandlerUtil.getCurrentSelection(event);
		if (!(isel instanceof IStructuredSelection)) {
			Log.warning(NLS.bind("Can not compute partition using current selection: {0}.", isel));
			return Status.CANCEL_STATUS;
		}
		IStructuredSelection selection = (IStructuredSelection) isel;

		final CQPCorpus corpus = (CQPCorpus) selection.getFirstElement();
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		final CreatePartitionDialog d = new CreatePartitionDialog(shell, corpus);
		int code = d.open();
		if (code == Window.OK) {
			try {
				JobHandler jobhandler = new JobHandler(NLS.bind(TXMUIMessages.creatingANewPartitionWithP0, corpus.getName())) {

					@Override
					protected IStatus _run(SubMonitor monitor) {

						try {
							JobsTimer.start();

							partition = d.getPartition();
							if (partition == null) {
								Log.warning(TXMUIMessages.errorColonThePartitionWasNotCreated);
								return Status.CANCEL_STATUS;
							}

							// compute the partition and children parts
							partition.compute(monitor.split(10));

							// ensure the partition viability
							if (partition.getPartsCount() == 0 || partition.getTotalSize() == 0) {
								Log.warning(TXMUIMessages.errorColonThePartitionCreatedHasNoPart);
								partition.delete();
								return Status.CANCEL_STATUS;
							}
							ArrayList<Part> emptyParts = new ArrayList<>();
							for (Part p : partition.getParts()) {
								if (p.getNMatch() == 0) {
									emptyParts.add(p);
								}
							}
							if (emptyParts.size() == partition.getPartsCount()) {
								Log.warning(NLS.bind(CQPUIMessages.ErrorAllPartsAreEmpty, partition.getPartsCount()));

								// clean broken partition
								partition.delete();

								return Status.CANCEL_STATUS;
							}
							else if (emptyParts.size() != 0) {
								Log.warning(NLS.bind(CQPUIMessages.WarningSomePartsAreEmptyP0, StringUtils.join(emptyParts, ", "))); //$NON-NLS-1$
							}

							monitor.subTask(TXMUIMessages.refreshingCorpora);

							syncExec(new Runnable() {

								@Override
								public void run() {
									CorporaView.refresh();
									CorporaView.expand(partition.getParent());
								}
							});

						}
						// for user direct canceling
						catch (ThreadDeath | InterruptedException e) {
							partition.delete();
							return Status.CANCEL_STATUS;
						}
						catch (Exception e) {
							partition.delete();
							Log.printStackTrace(e);
							return Status.CANCEL_STATUS;
						}
						return Status.OK_STATUS;
					}
				};
				jobhandler.startJob();

			}
			catch (Exception e) {
				Log.severe(NLS.bind(TXMUIMessages.errorWhileCreatingAPartitionColonP0, e));
			}
		}
		return Status.CANCEL_STATUS;
	}
}
