package org.txm.searchengine.cqp.rcp.dialogs;


import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.txm.Toolbox;
import org.txm.core.preferences.TXMPreferences;
import org.txm.objects.Project;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.rcp.messages.CQPUIMessages;

/**
 * Allow to select the file to create and select elements to include in the binary
 * 
 *
 */
public class ExportCorpusDialog extends Dialog {

	/**
	 * list of the default files to include in the corpus binary
	 */
	private List<String> defaultFiles = Arrays.asList("HTML", "txm", "data", "registry", "analec", "temporary_annotations", "xsl", "css", ".settings", ".project", "doc"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$

	private File zipFile;

	private LinkedHashMap<File, Boolean> selectedFiles;

	private CQPCorpus corpus;

	private TableViewer treeViewer;

	private TableViewerColumn nameColumn;

	private Text textField;

	private Combo versionField;

	private Button selectButton;

	private String version;

	private Text renameField;

	private String rename;

	public final static String ID = ExportCorpusDialog.class.getName();

	/**
	 * 
	 * @param parentShell
	 */
	public ExportCorpusDialog(Shell parentShell, CQPCorpus corpus) {
		super(parentShell);
		this.corpus = corpus;
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(NLS.bind(CQPUIMessages.ExportingP0, corpus.getProject().getName().toUpperCase()));
		newShell.setMinimumSize(400, 300);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite comp = (Composite) super.createDialogArea(parent);

		GridLayout layout = (GridLayout) comp.getLayout();
		layout.numColumns = 3;

		Label l = new Label(comp, SWT.LEFT);
		l.setText(CQPUIMessages.OutputFile);
		l.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false));

		textField = new Text(comp, SWT.SINGLE | SWT.BORDER);
		textField.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		String previousPath = TXMPreferences.getString("path", ExportCorpusDialog.class.getName()); //$NON-NLS-1$
		if (previousPath == null || previousPath.length() == 0) {
			previousPath = System.getProperty("user.home"); // //$NON-NLS-1$
		}
		textField.setText(previousPath + "/" + corpus.getProject().getName().toUpperCase() + "-" + Toolbox.dateformat.format(Calendar.getInstance().getTime()) + ".txm"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		selectButton = new Button(comp, SWT.PUSH);
		selectButton.setText("..."); //$NON-NLS-1$
		selectButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(selectButton.getShell(), SWT.SAVE);

				String[] exts = { "*.txm", "*.zip" }; //$NON-NLS-1$ //$NON-NLS-2$
				dialog.setFilterExtensions(exts);
				String path = textField.getText();
				if (path != null && path.length() > 0) {
					File p = new File(path);
					if (p.isDirectory()) {
						dialog.setFilterPath(textField.getText());
					}
					else {
						dialog.setFilterPath(p.getParent());
						dialog.setFileName(p.getName());
					}
				}
				//

				path = dialog.open();
				if (path != null) {
					textField.setText(path);
					File dir = new File(path).getParentFile();
					getButton(IDialogConstants.OK_ID).setEnabled(dir.exists());
					TXMPreferences.put(ExportCorpusDialog.class.getName(), "path", dir.getAbsolutePath()); //$NON-NLS-1$
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		new Label(comp, SWT.NONE).setText(CQPUIMessages.CorpusIndentifier);
		renameField = new Text(comp, SWT.BORDER);
		GridData gdata2 = GridDataFactory.fillDefaults().create();
		gdata2.widthHint = 50;
		gdata2.horizontalSpan = 2;
		renameField.setLayoutData(gdata2);
		renameField.setText(corpus.getProject().getName());
		renameField.setToolTipText(CQPUIMessages.ChangeThisIdentifierToRenameTheCorpus);

		new Label(comp, SWT.NONE).setText(CQPUIMessages.CorpusVersion);
		versionField = new Combo(comp, SWT.SINGLE | SWT.READ_ONLY);
		versionField.setItems("0.8.0", "0.7.9"); //$NON-NLS-1$ //$NON-NLS-2$
		versionField.select(0);
		GridData gdata = GridDataFactory.fillDefaults().create();
		gdata.widthHint = 50;
		gdata.horizontalSpan = 2;
		versionField.setLayoutData(gdata);

		// File selection section
		selectedFiles = new LinkedHashMap<>();
		Project project = corpus.getProject();
		File directory = project.getProjectDirectory();
		if (directory.exists()) {

			File[] files = directory.listFiles(new FileFilter() {

				@Override
				public boolean accept(File f) {
					return !f.getName().startsWith(".") && !f.isHidden() && f.isDirectory() && !defaultFiles.contains(f.getName()); //$NON-NLS-1$
				}
			});
			Arrays.sort(files, new Comparator<File>() {

				@Override
				public int compare(File arg0, File arg1) {
					if (arg0.isDirectory() && arg1.isDirectory()) {
						return arg0.getName().compareTo(arg1.getName());
					}
					else if (arg0.isFile() && arg1.isFile()) {
						return arg0.getName().compareTo(arg1.getName());
					}
					else {
						if (arg0.isDirectory()) {
							return -1;
						}
						else {
							return 1;
						}
					}
				}
			});
			for (File f : files) {
				if (!f.isHidden() && f.isDirectory()) {

					if (defaultFiles.contains(f.getName())) {
						selectedFiles.put(f, true);
					}
					else {
						selectedFiles.put(f, false);
					}
				}
			}

			if (files.length > 0) {
				treeViewer = new TableViewer(comp, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
				treeViewer.getTable().setToolTipText(CQPUIMessages.DirectoriesNotManagedByTXM);
				treeViewer.setContentProvider(new ArrayContentProvider());

				treeViewer.getTable().addListener(SWT.Selection, event -> {

					if (event.detail == SWT.CHECK) {
						TableItem item = (TableItem) event.item;
						selectedFiles.put((File) event.item.getData(), item.getChecked());
					}
				});

				treeViewer.getTable().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 3, 1));
				treeViewer.getTable().setHeaderVisible(true);
				treeViewer.getTable().setLinesVisible(true);

				nameColumn = new TableViewerColumn(treeViewer, SWT.NONE);
				nameColumn.getColumn().setText(CQPUIMessages.AddExtraFiles);
				nameColumn.getColumn().pack();
				nameColumn.setLabelProvider(new CellLabelProvider() {

					@Override
					public void update(ViewerCell cell) {
						Object element = cell.getElement();

						if (element instanceof File) {
							File f = (File) element;
							cell.setText(f.getName());
						}
						else {
							cell.setText("" + element.getClass()); //$NON-NLS-1$
						}
					}
				});

				treeViewer.setInput(files);
				for (int i = 0; i < files.length; i++) {
					treeViewer.getTable().getItem(i).setChecked(selectedFiles.get(files[i]));
				}

				Button selectAllButton = new Button(comp, SWT.PUSH);
				selectAllButton.setText(CQPUIMessages.SelectAll);
				selectAllButton.setToolTipText("Select all files to be exported");
				selectAllButton.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						massSelect(true);
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});

				Button clearAllButton = new Button(comp, SWT.PUSH);
				clearAllButton.setText(CQPUIMessages.ClearSelection);
				clearAllButton.setToolTipText("Deselect all files");
				clearAllButton.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						massSelect(false);
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});
			}
		}

		return comp;
	}

	protected void massSelect(boolean b) {
		for (int i = 0; i < treeViewer.getTable().getItems().length; i++) {
			treeViewer.getTable().getItem(i).setChecked(b);
		}
		for (File f : selectedFiles.keySet()) {
			selectedFiles.put(f, b);
		}
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == Dialog.OK) {
			zipFile = new File(textField.getText());
			version = versionField.getText();
			rename = renameField.getText();

//			if (renameField.getText().length() > 0) {
//				String zipname = zipFile.getName();
//				String oldname = corpus.getProject().getName();
//				zipname = zipname.replace(oldname, rename);
//				zipFile = new File(zipFile.getParentFile(), zipname);
//			}

		}
		super.buttonPressed(buttonId);
	}

	/**
	 * 
	 * @return the ZIP file to create
	 */
	public File getZipFile() {
		return zipFile;
	}

	/**
	 * 
	 * @return The corpus version to produce
	 */
	public String getCorpusVersion() {
		return version;
	}

	/**
	 * 
	 * @return The corpus version to produce
	 */
	public String getRename() {
		return rename;
	}

	/**
	 * @return The corpus version to produce
	 */
	public LinkedHashMap<File, Boolean> getFileSelection() {
		return selectedFiles;
	}

	public HashSet<String> getIgnoreNames() {
		HashSet<String> ignore = new HashSet<>();
		for (File f : selectedFiles.keySet()) {
			if (!selectedFiles.get(f)) {
				ignore.add(f.getName());
			}
		}
		return ignore;
	}

	public void setRename(String rename2) {
		this.rename = rename2;
	}
}
