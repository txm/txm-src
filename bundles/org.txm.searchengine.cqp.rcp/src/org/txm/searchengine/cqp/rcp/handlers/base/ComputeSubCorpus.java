// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.searchengine.cqp.rcp.handlers.base;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.rcp.actions.CreateSubcorpusDialog;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.ComputeProgressMonitorDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.core.messages.CQPSearchEngineCoreMessages;
import org.txm.searchengine.cqp.core.preferences.SubcorpusPreferences;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.rcp.editor.SubcorpusEditor;
import org.txm.utils.logger.Log;

/**
 * Command which creates a subcorpus from a Corpus
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class ComputeSubCorpus extends BaseAbstractHandler {


	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkCorpusEngine()) {
			return false;
		}

		// FIXME: SJ: tests of MD for using a TXMEditor rather than a dialog box to manage sub-corpus creation and edition
		if (SubcorpusPreferences.getInstance().getBoolean(SubcorpusPreferences.EDITORMODE)) {
			return execute_editor(event);
		}
		// original code that uses a dialog box
		else {

			// since some widgets fields need some values of parents branch,
			// ensure the parents branch is ready
			// show modal blocking cancelable progression dialog
			try {

				Object selection = this.getCorporaViewSelectedObject(event);

				if (selection instanceof CQPCorpus) {
					CQPCorpus corpus = (CQPCorpus) selection;
					if (!corpus.hasBeenComputedOnce()) {

						ComputeProgressMonitorDialog dialog = new ComputeProgressMonitorDialog(HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell(), corpus);
						dialog.runComputingProcess(true);

					}
				}
			}
			// Canceling or error
			catch (InterruptedException | InvocationTargetException e) {
				CorporaView.refresh();
				return null;
			}

			return execute_dialog(event);
		}
	}


	public Object execute_editor(ExecutionEvent event) throws ExecutionException {

		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		if (selection == null) {
			System.out.println(TXMUIMessages.noSelectionForColon + HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActiveEditor());
			return null;
		}

		String edit = event.getParameter("edit"); //$NON-NLS-1$

		Object firstElement = selection.getFirstElement();
		Subcorpus sub = null;
		if (firstElement instanceof CQPCorpus) {
			if (firstElement instanceof Subcorpus && "true".equals(edit)) {
				sub = (Subcorpus) firstElement;
			}
			else {
				sub = new Subcorpus((CQPCorpus) firstElement);
			}
		}
		return TXMEditor.openEditor(sub, SubcorpusEditor.class.getName());
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute_dialog(ExecutionEvent event) throws ExecutionException {

		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		if (selection == null) {
			System.out.println(TXMUIMessages.noSelectionForColon + HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActiveEditor());
			return null;
		}
		Object firstElement = selection.getFirstElement();

		if (!(firstElement instanceof CQPCorpus)) {
			System.out.println(TXMUIMessages.selectionIsNotACorpusColon + firstElement);
			return null;
		}

		final CQPCorpus corpus = (CQPCorpus) firstElement;
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		final CreateSubcorpusDialog d = new CreateSubcorpusDialog(shell, corpus);
		int code = d.open();
		if (code == Window.OK) {
			JobHandler jobhandler = new JobHandler(NLS.bind(TXMUIMessages.creatingASubcorpusOnP0, corpus.getName())) {

				@Override
				protected IStatus _run(SubMonitor monitor) throws CqiClientException {

					try {
						monitor.beginTask(TXMUIMessages.creatingASubcorpusInTheSearchEngine, 100);
						final Subcorpus subcorpus;

						if (d.isQueryBased()) {

							// test if a query has been set
							if (d.getQuery().length() == 0) {
								Log.warning(TXMUIMessages.queryCannotBeLeftEmpty);
								return Status.CANCEL_STATUS;
							}

							CQLQuery q = new CQLQuery(d.getQuery());

							// System.out.println(NLS.bind(TXMUIMessages.computeSubCorpusOnP0QueryColonP1, corpus.getName(), q.getQueryString()));
							subcorpus = corpus.createSubcorpus(q, d.getName());
						}
						else {
							// System.out.println(NLS.bind(TXMUIMessages.computeSubCorpusOnP0StructsColonP1AmpPropsP2ColonP3,
							// new Object[] { corpus.getName(), d.getStructuralUnit(),
							// d.getStructuralUnitProperty(), d.getValues() }));

							// Escape regex char of prop values
							// List<String> values = d.getValues();
							// for(int i = 0 ; i < values.size() ; i++)
							// values.set(i, Query.addBackSlash(values.get(i)));


							List<String> values = d.getValues();
							subcorpus = corpus.createSubcorpus(
									d.getStructuralUnit(), d.getStructuralUnitProperty(),
									values, d.getName());
						}

						if (subcorpus == null) {
							monitor.done();
							Log.warning(TXMUIMessages.errorColonSubcorpusWasNotCreated);
							return Status.CANCEL_STATUS;
						}
						if (subcorpus.getSize() == 0) {
							Log.warning("The created subcorpus is empty. Aborting creation.");
							// clean broken partition
							subcorpus.delete();
						}
						// System.out.println(NLS.bind(TXMUIMessages.doneColonP0Created, subcorpus.getName()));
						monitor.worked(50);

						monitor.subTask(TXMUIMessages.refreshingCorpora);
						syncExec(new Runnable() {

							@Override
							public void run() {
								CorporaView.refresh();
								// System.out.println("expand");
								CorporaView.expand(subcorpus.getParent());
							}
						});

						monitor.worked(100);
					}
					catch (CqiClientException e) {
						try {
							Log.severe(NLS.bind(CQPSearchEngineCoreMessages.lastCQPErrorP0, CQPSearchEngine.getCqiClient().getLastCQPError()));
						}
						catch (Exception e1) {
							Log.severe(TXMUIMessages.failedToGetLastCQPErrorColon + e1);
							org.txm.utils.logger.Log.printStackTrace(e1);
						}
						throw e;
					}
					return Status.OK_STATUS;
				}
			};
			jobhandler.startJob();
		}
		return null;
	}

}
