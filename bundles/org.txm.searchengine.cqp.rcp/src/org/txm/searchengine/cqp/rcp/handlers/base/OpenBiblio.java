// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.searchengine.cqp.rcp.handlers.base;

import java.util.ArrayList;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.objects.Text;
import org.txm.rcp.commands.OpenBrowser;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.ComboDialog;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

/**
 * Open a text edition from: a main corpus : the first text of the maincorpus a
 * sub-corpus : the first text of the subcorpus a partition : the user must
 * choose one of the text used in the partition
 * 
 * note: unplugged for now
 * 
 * @author mdecorde.
 */
public class OpenBiblio extends AbstractHandler {

	/** The Constant ID. */
	public final static String ID = "org.txm.rcp.commands.base.OpenBiblio"; //$NON-NLS-1$

	/** The lastopenedfile. */
	public static String lastopenedfile;

	/** The textids. */
	private ArrayList<String> textids;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);

		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

		if (selection.getFirstElement() instanceof CQPCorpus) {
			CQPCorpus corpus = (CQPCorpus) selection.getFirstElement();

			textids = new ArrayList<>(corpus.getProject().getTextsID());
			if (textids.size() == 0) {
				System.out.println(NLS.bind(TXMUIMessages.noTextInCorpusColonP0, corpus.getName()));
				return null;
			}

			ComboDialog dialog = new ComboDialog(shell, TXMUIMessages.selectTextId, textids, textids.get(0));
			if (dialog.open() == Window.OK) {
				String textid = dialog.getSelectedValue();
				openBiblio(corpus, textid);
			}
		}
		else if (selection.getFirstElement() instanceof Text) {
			Text text = (Text) selection.getFirstElement();
			openBiblio(text);
		}
		return null;
	}

	/**
	 * Open biblio.
	 *
	 * @param corpus the corpus
	 * @param textid the textid
	 * @return the object
	 */
	public Object openBiblio(CQPCorpus corpus, String textid) {

		Text text = corpus.getProject().getText(textid);
		if (text == null) {
			System.out.println(NLS.bind(TXMUIMessages.couldNotFindTextP0InCorpusP1, textid, corpus.getName()));
			return null;
		}

		return openBiblio(text);
	}

	/**
	 * Open biblio.
	 *
	 * @param text the text
	 * @return the object
	 */
	public Object openBiblio(Text text) {

		if (text == null) {
			return null;
		}
		if (text.getBiblioPath() == null) {
			System.out.println(NLS.bind(TXMUIMessages.textP0HasNoBibliographicRecord, text.getName()));
			return null;
		}
		String url = text.getBiblioPath().toString();
		if (url.trim().length() == 0) {
			System.out.println(NLS.bind(TXMUIMessages.textP0HasNoBibliographicRecord, text.getName()));
			return null;
		}
		// attachedBrowserEditor.setCorpus(text.getCorpus());
		return OpenBrowser.openEdition(url);
	}

}
