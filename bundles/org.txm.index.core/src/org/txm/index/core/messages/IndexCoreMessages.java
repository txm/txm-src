package org.txm.index.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * 
 * @author sjacquot
 *
 */
public class IndexCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.index.core.messages.messages"; //$NON-NLS-1$

	public static String indexColonPartitionP0queryP1propertiesP2FminP3FmaxP4;

	public static String indexColonCorpusP0queryP1propertiesP2FminP3FmaxP4;

	public static String RESULT_TYPE;


	public static String lexicon;

	public static String consoleColonP0;

	public static String error_failedToExportLexiconColonP0;


	public static String formsAndFrequenciesTablesMustHaveTheSameSize;

	public static String lexiconColonP0;

	public static String computingTheLexiconOfSubcorpusP0;

	public static String lexiconOfP0InTheP1Corpus;

	public static String indexOfP0PropertiesP1InTheP2Corpus;

	public static String indexOfP0PropertiesP1OnP2Partition;


	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, IndexCoreMessages.class);
	}

	private IndexCoreMessages() {
	}
}
