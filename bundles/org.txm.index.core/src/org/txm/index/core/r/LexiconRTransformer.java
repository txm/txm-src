package org.txm.index.core.r;

import java.util.ArrayList;
import java.util.List;

import org.txm.index.core.functions.Lexicon;
import org.txm.index.core.functions.Line;
import org.txm.statsengine.r.core.RTransformer;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

public class LexiconRTransformer extends RTransformer<Lexicon> {

	/** The novoc. */
	protected static int novoc = 1;

	/** The prefix r. */
	protected static String prefixR = "Lexicon"; //$NON-NLS-1$


	public LexiconRTransformer() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Lexicon fromRtoTXM(String symbol) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * As r matrix.
	 *
	 * @return the string
	 * @throws RWorkspaceException the r workspace exception
	 */
	@Override
	public String _fromTXMtoR(Lexicon o, String symbol) throws RWorkspaceException {

		Lexicon index = (Lexicon) o;

		if (symbol == null || symbol.length() == 0) {
			symbol = prefixR + novoc;
			novoc++;
		}

		ArrayList<String> colnames = new ArrayList<String>();

		colnames.add("F"); //$NON-NLS-1$

		// System.out.println("cols: "+colnames);
		List<Line> lines = index.getAllLines();
		String[] keywords = new String[lines.size()];

		int[] freq = new int[lines.size()];

		for (int i = 0; i < lines.size(); i++) {
			Line line = lines.get(i);
			freq[i] = line.getFrequency();
			keywords[i] = line.toString();
		}

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		rw.addVectorToWorkspace("vocfreq", freq); //$NON-NLS-1$
		rw.addVectorToWorkspace("vockeywords", keywords); //$NON-NLS-1$
		rw.addVectorToWorkspace("voccolnames", colnames.toArray(new String[colnames.size()])); //$NON-NLS-1$

		int ncol = 1;

		int nrow = lines.size();
		String partscmd = ""; //$NON-NLS-1$

		rw.eval(symbol + "<- matrix(data = c(vocfreq" + partscmd + "), nrow = " + nrow + ", ncol = " + ncol + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		rw.eval("colnames(" + symbol + " ) <- voccolnames"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval("rownames(" + symbol + " ) <- vockeywords"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval(symbol + "<- list(data=" + symbol + ", filters=c(fmin="+index.getFilterFmin()+", fmax="+index.getFilterFmax()+", vmax="+index.getFilterVmax()+"))"); //$NON-NLS-1$ //$NON-NLS-2$

		rw.eval("rm(vocfreq, vockeywords, voccolnames)");


		return symbol;
	}
}
