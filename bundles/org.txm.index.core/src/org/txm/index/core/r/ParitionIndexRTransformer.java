package org.txm.index.core.r;

import java.util.ArrayList;
import java.util.List;

import org.txm.index.core.functions.Line;
import org.txm.index.core.functions.PartitionIndex;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.statsengine.r.core.RTransformer;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

public class ParitionIndexRTransformer extends RTransformer<PartitionIndex> {

	/** The novoc. */
	protected static int novoc = 1;

	/** The prefix r. */
	protected static String prefixR = "PartitionIndex"; //$NON-NLS-1$


	public ParitionIndexRTransformer() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public PartitionIndex fromRtoTXM(String symbol) {
		return null;
	}

	/**
	 * As r matrix.
	 *
	 * @return the string
	 * @throws RWorkspaceException the r workspace exception
	 */
	@Override
	public String _fromTXMtoR(PartitionIndex o, String symbol) throws RWorkspaceException {
		if (!(o instanceof PartitionIndex)) return null;

		PartitionIndex index = (PartitionIndex) o;

		if (symbol == null || symbol.length() == 0) {
			symbol = prefixR + novoc;
			novoc++;
		}

		ArrayList<String> colnames = new ArrayList<String>();
		List<String> partnames = index.getPartNames();

		colnames.add("F"); //$NON-NLS-1$
		colnames.addAll(partnames);

		// System.out.println("cols: "+colnames);
		List<Line> lines = index.getAllLines();
		String[] keywords = new String[lines.size()];

		int[] freq = new int[lines.size()];
		ArrayList<int[]> partfreqs = new ArrayList<int[]>(partnames.size());
		for (int j = 0; j < partnames.size(); j++) {
			partfreqs.add(new int[lines.size()]);
		}

		for (int i = 0; i < lines.size(); i++) {
			Line line = lines.get(i);
			freq[i] = line.getFrequency();
			keywords[i] = line.toString();
			for (int j = 0; j < partnames.size(); j++) {
				partfreqs.get(j)[i] = line.getFrequency(j);
			}
		}

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		for (int j = 0; j < partnames.size(); j++) {
			rw.addVectorToWorkspace("vocpartfreqs" + j, partfreqs.get(j)); //$NON-NLS-1$
		}
		rw.addVectorToWorkspace("vocfreq", freq); //$NON-NLS-1$
		rw.addVectorToWorkspace("vockeywords", keywords); //$NON-NLS-1$
		rw.addVectorToWorkspace("voccolnames", colnames.toArray(new String[colnames.size()])); //$NON-NLS-1$

		int ncol = 1 + partnames.size();

		int nrow = lines.size();
		String partscmd = ""; //$NON-NLS-1$
		for (int j = 0; j < partnames.size(); j++) {
			partscmd += ", vocpartfreqs" + j; //$NON-NLS-1$
		}
		rw.eval(symbol + "<- matrix(data = c(vocfreq" + partscmd + "), nrow = " + nrow + ", ncol = " + ncol + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		rw.eval("colnames(" + symbol + " ) <- voccolnames"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval("rownames(" + symbol + " ) <- vockeywords"); //$NON-NLS-1$ //$NON-NLS-2$
		
		List<WordProperty> props = index.getProperties();
		String[] s = new String[props.size()];
		for (int i = 0 ; i < props.size() ; i++) s[i] = props.get(i).getName();
		rw.addVectorToWorkspace("tmp", s);
		rw.eval(symbol + "<- list(data=" + symbol + ", properties=tmp, filters=c(fmin="+index.getFilterFmin()+", fmax="+index.getFilterFmax()+", vmax="+index.getFilterVmax()+"))"); //$NON-NLS-1$ //$NON-NLS-2$

		return symbol;
	}
}
