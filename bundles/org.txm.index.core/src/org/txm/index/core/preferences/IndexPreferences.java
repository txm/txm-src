package org.txm.index.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Preferences initializer and manager.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class IndexPreferences extends TXMPreferences {

	/**
	 * 
	 */
	public static final String PROPERTIES_SEPARATOR = "properties_separator"; //$NON-NLS-1$

	/**
	 * Number of the line of the current page.
	 */
	public static final String N_TOP_INDEX = "n_top_index"; //$NON-NLS-1$

	/**
	 * Number of the line of the current page. -1=names, 0=totals, 1 and more = additional freq columns
	 */
	public static final String SORTED_COLUMN = "sorted_column"; //$NON-NLS-1$

	/**
	 * Number of the line of the current page SWT.DOWN and SWT.UP values.
	 */
	public static final String REVERSED_SORT = "reversed_sort"; //$NON-NLS-1$
	
	/**
	 * If true the link from index to concordances creates a child concordance of the index in the other case the concordance is a brother result of the index
	 */
	public static final String LINKED_CONCORDANCES = "linked_concordances"; //$NON-NLS-1$

	//	/**
	//	 * if true the links from index will create a subcorpus of the index occurrences
	//	 */
	//	public static final String LINK_USING_SUBCORPUS = "link_using_subcorpus";

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(IndexPreferences.class)) {
			new IndexPreferences();
		}
		return TXMPreferences.instances.get(IndexPreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.put(UNIT_PROPERTIES, DEFAULT_UNIT_PROPERTY); // $NON-NLS-1$
		preferences.put(UNIT_PROPERTY, DEFAULT_UNIT_PROPERTY); // $NON-NLS-1$
		preferences.put(PROPERTIES_SEPARATOR, "_"); //$NON-NLS-1$
		preferences.put(QUERY, ""); //$NON-NLS-1$
		preferences.putInt(F_MIN, 1);
		preferences.putInt(F_MAX, Integer.MAX_VALUE);
		preferences.putInt(V_MAX, Integer.MAX_VALUE);
		preferences.putInt(NB_OF_ELEMENTS_PER_PAGE, 100);
		preferences.putInt(SORTED_COLUMN, 0); // sort using the total
		preferences.putBoolean(REVERSED_SORT, true); // sort using the total
		preferences.putBoolean(LINKED_CONCORDANCES, true);
		//		preferences.putBoolean(LINK_USING_SUBCORPUS, false);
	}
}
