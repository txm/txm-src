// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-11-19 14:29:08 +0100 (Thu, 19 Nov 2015) $
// $LastChangedRevision: 3056 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.index.core.functions;

import java.util.ArrayList;
import java.util.List;

import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.query.Occurrence;

/**
 * Contains the signature of the line all properties value and counts
 * 
 * @author mdecorde
 * 
 */
public class Line {

	/** The Units property. */
	public List<List<String>> UnitsProperty; // the values, for each property there is a list of property values

	/** The Units ids. */
	public List<int[]> UnitsIds; // the values, for each property there is a list of property values

	/** The properties. */
	public List<Property> properties = new ArrayList<>(); // the properties name UnitsProperty and properties have the same order

	/** The signature. */
	private Object signature;

	/** The counts. */
	private int[] counts;

	/** The total. */
	private int total;

	/** The frelatif. */
	private float frelatif = 0;

	/** The str. */
	private String str;

	public int orderNumber = 0;

	/**
	 * Instantiates a new line.
	 */
	public Line() {
		this.UnitsProperty = new ArrayList<>();
		UnitsIds = new ArrayList<>();
	}

	/**
	 * add line's values for the given property.
	 *
	 * @param p the p
	 * @param values the values
	 */
	public void put(Property p, List<String> values) {
		UnitsProperty.add(values);
		// properties.add(p);
	}

	/**
	 * add line's values for the given property.
	 *
	 * @param p the p
	 * @param ids the ids
	 */
	public void putIds(Property p, int[] ids) {
		UnitsIds.add(ids);
		properties.add(p);
	}

	/**
	 * return the signature of a Line, an id to compare them quickly if L1 ==
	 * L2, L1.signature = L2.signature else L1.signature != L2.signature
	 *
	 * @return the signature
	 */
	public Object getSignature() {

		if (signature == null) {
			// System.out.println("getSignature() props size "+properties.size());
			signature = ""; //$NON-NLS-1$
			for (int i = 0; i < properties.size(); i++) {
				for (int s : UnitsIds.get(i)) {
					signature += "[" + s + "]"; //$NON-NLS-1$ //$NON-NLS-2$
				}
			}
		}
		return signature;
	}

	/**
	 * Sets the counts.
	 *
	 * @param c the c
	 * @param f the f
	 */
	public void setCounts(int[] c, float f) {
		counts = c;
		total = 0;
		for (int i = 0; i < c.length; i++) {
			total += c[i];
		}
		frelatif = f;
	}

	/**
	 * Gets the frequency.
	 *
	 * @return the frequency
	 */
	public int getFrequency() {
		return total;
	}

	/**
	 * Gets the frequency.
	 *
	 * @param i the i
	 * @return the frequency
	 */
	public int getFrequency(int i) {
		if (i >= counts.length || i < 0) {
			return total;
		}
		return counts[i];
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public List<Property> getProperties() {
		return this.properties;
	}

	/**
	 * Gets the units properties.
	 *
	 * @return the units properties
	 */
	public List<List<String>> getUnitsProperties() {
		return this.UnitsProperty;
	}

	/**
	 * Gets the relative frequency.
	 *
	 * @return the relative frequency
	 */
	public float getRelativeFrenquency() {
		return frelatif;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (str == null) {
			str = this.toString("_"); //$NON-NLS-1$
		}
		return str;
	}

	/**
	 * To string.
	 *
	 * @param separator the separator
	 * @return the string
	 */
	public String toString(String separator) {

		if (UnitsProperty != null && UnitsProperty.size() > 0) {

			String rez = ""; //$NON-NLS-1$
			int len = 0;
			for (int i = 0; i < properties.size();) {
				len = UnitsProperty.get(i).size();
				break;
			}

			for (int i = 0; i < len; i++) { // for each token
				for (int j = 0; j < properties.size(); j++) {
					rez += UnitsProperty.get(j).get(i);
					if (j < properties.size() - 1) {
						rez += separator;
					}
				}

				if (i < len - 1) {
					rez += " "; //$NON-NLS-1$
				}
			}
			return rez;
		}
		else {
			return getSignature().toString();
		}
	}

	/**
	 * compare to lines with their signature.
	 *
	 * @param l the l
	 * @return true, if successful
	 */
	public boolean equals(Line l) {
		return l.getSignature().equals(this.getSignature());
	}

	/**
	 * Sets the signature.
	 *
	 * @param signature the new signature not null
	 */
	public void setSignature(Object signature) {
		this.signature = signature;
	}

	/**
	 * Gets the frequencies.
	 *
	 * @return the frequencies
	 */
	public int[] getFrequencies() {
		return counts;
	}

	/**
	 * The Class Signature. MD: test to replace String[] signatures with a tuple class
	 */
	public static class Signature {

		/** The sign. */
		Occurrence[] sign;// [token][prop]

		/**
		 * Equals.
		 *
		 * @param s the s
		 * @return true, if successful
		 */
		public boolean equals(Signature s) {

			if (sign.length != s.sign.length) {
				return false;
			}

			for (int i = 0; i < sign.length; i++) {
				if (!sign[i].equals(s.sign[i])) {
					return false;
				}
			}
			return true;
		}
	}

	/**
	 * Gets the units ids.
	 *
	 * @return the units ids
	 */
	public List<int[]> getUnitsIds() {
		return UnitsIds;
	}

	public int getOrderNumber() {

		return orderNumber;
	}
}
