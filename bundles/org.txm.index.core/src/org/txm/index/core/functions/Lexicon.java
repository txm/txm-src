// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-09-19 10:31:00 +0200 (Mon, 19 Sep 2016) $
// $LastChangedRevision: 3298 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.index.core.functions;

import java.util.ArrayList;
import java.util.List;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;
import org.txm.index.core.messages.IndexCoreMessages;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CQPLexicon;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * Represent a frequency list according to a {@link CQPCorpus} (or a {@link Subcorpus}) and a {@link Property}.
 * 
 * @author sjacquot
 * @author mdecorde
 */
public class Lexicon extends Index {


	/**
	 * 
	 * @param parent
	 */
	public Lexicon(CQPCorpus parent) {
		super(parent);
	}

	/**
	 * 
	 * @param parametersNodePath
	 */
	public Lexicon(String parametersNodePath) {
		super(parametersNodePath);
	}

	/**
	 * 
	 * @param parametersNodePath
	 */
	public Lexicon(String parametersNodePath, TXMResult parent) {
		super(parametersNodePath, parent);
	}


	@Override
	protected boolean _computeLines(TXMProgressMonitor monitor) {
		try {

			// this.query = new Query("[]"); //$NON-NLS-1$
			// this.props = new ArrayList<Property>();
			// this.props.add(property);
			WordProperty property = this.pProperties.get(0);

			CQPLexicon lexicon = CQPLexicon.getLexicon(this.getCorpus(), property, monitor.createNewMonitor(20));
			//lexicon.setProperty(property);
			//lexicon._compute();

			this.nTotalTokens = lexicon.nbrOfToken();
			String[] forms = lexicon.getForms();
			int[] freqs = lexicon.getFreq();
			int[] ids = lexicon.getIds();
			this.lines = new ArrayList<>();
			int nbOfToken = lexicon.nbrOfToken();
			if (nbOfToken > 0) { // no lines
				for (int i = 0; i < forms.length && i < freqs.length; i++) {
					Line l = new Line();
					List<String> values = new ArrayList<>(1);
					values.add(forms[i]);
					l.put(property, values);
					int[] c = new int[1];
					c[0] = freqs[i];
					l.setCounts(c, freqs[i] / lexicon.nbrOfToken());
					l.putIds(property, new int[] { ids[i] });
					l.orderNumber = i + 1;
					lines.add(l);

					if (Fmin > freqs[i]) {
						Fmin = freqs[i];
					}
					if (Fmax < freqs[i]) {
						Fmax = freqs[i];
					}
				}
			}

		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.bind(TXMCoreMessages.errorColonP0, e.getLocalizedMessage()));
			Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	/**
	 * The Lexicon can be computed without a query but needs at least one property.
	 */
	@Override
	public boolean canCompute() {
		return pProperties != null && pProperties.size() > 0;
	}

	@Override
	public String getComputingStartMessage() {
		return TXMCoreMessages.bind(IndexCoreMessages.lexiconOfP0InTheP1Corpus, WordProperty.asString(this.pProperties), this.getCorpus().getName());
	}


	@Override
	public String getSimpleName() {
		String name = "";
		if (this.pProperties != null && this.pProperties.size() > 0 && this.pProperties.get(0) != null) {
			name += WordProperty.asString(this.pProperties);
			name += TXMCoreMessages.formatMinFilter(this.pFminFilter);
			name += TXMCoreMessages.formatMaxFilter(this.pFmaxFilter);
			name += TXMCoreMessages.formatVMaxFilter(this.pVmaxFilter);
		}
		else {
			name = this.getParent().getSimpleName();
		}
		return name;
	}

}


