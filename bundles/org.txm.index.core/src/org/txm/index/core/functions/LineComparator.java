// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-11-19 14:29:08 +0100 (Thu, 19 Nov 2015) $
// $LastChangedRevision: 3056 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.index.core.functions;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

import org.txm.searchengine.cqp.corpus.CQPCorpus;

// TODO: Auto-generated Javadoc
/**
 * The Class LineComparator.
 */
public class LineComparator implements Comparator<Line> {

	/** The collator. */
	protected Collator collator;

	/** The reverse. */
	boolean reverse;

	int columnToSort = -1;

	/**
	 * Instantiates a new line comparator.
	 *
	 * @param mode the mode
	 * @param reverse the reverse
	 */
	public LineComparator(int focusedSubcorpus, boolean reverse) {
		this.reverse = reverse;
		this.columnToSort = focusedSubcorpus;
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Line arg0, Line arg1) {
		int sort = 0;

		if (columnToSort == -1) { // names
			sort = collator.compare(arg0.toString(), arg1.toString());
			if (sort == 0) {
				sort = arg0.getFrequency() - arg1.getFrequency();
			}
		}
		else if (columnToSort == 0) { // frequencies
			sort = arg0.getFrequency() - arg1.getFrequency();

			if (sort == 0) {
				sort = -collator.compare(arg0.toString(), arg1.toString());
			}
		}
		else { // frequencies
			sort = arg0.getFrequency(columnToSort - 1) - arg1.getFrequency(columnToSort - 1);

			if (sort == 0) {
				sort = -collator.compare(arg0.toString(), arg1.toString());
			}
		}

		if (reverse) {
			sort = -sort;
		}
		return sort;
	}

	/**
	 * Initialize.
	 *
	 * @param corpus the corpus
	 */
	public void initialize(CQPCorpus corpus) {
		collator = Collator.getInstance(new Locale(corpus.getLang()));
		collator.setStrength(Collator.TERTIARY);
	}
}
