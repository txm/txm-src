// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-10-21 11:09:43 +0200 (Fri, 21 Oct 2016) $
// $LastChangedRevision: 3323 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.index.core.functions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.rosuda.REngine.REXPMismatchException;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.index.core.messages.IndexCoreMessages;
import org.txm.index.core.preferences.IndexPreferences;
import org.txm.objects.Match;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.QueryBasedTXMResult;
import org.txm.searchengine.core.Selection;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.VirtualProperty;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * Computes an index of a corpus or subcorpus
 * 
 * @author mdecorde
 */
public class Index extends QueryBasedTXMResult {

	/** The counts. */
	protected LinkedHashMap<Object, ArrayList<Integer>> counts = new LinkedHashMap<>();

	/** The currentpartid. */
	protected int currentpartid = 0;

	/** The current Fmax value. */
	protected int Fmax = 0;

	/** The current Fmin value. */
	protected int Fmin = Integer.MAX_VALUE;

	/** The current lines. */
	protected List<Line> lines = new ArrayList<>();

	/** The current number of lines. */
	protected int nTotalTokens = 0;

	/** The writer. */
	// FIXME: should be in an exporter extension
	@Deprecated
	protected BufferedWriter writer;

	/**
	 * Maximum frequency filter value.
	 */
	@Parameter(key = TXMPreferences.F_MAX)
	protected Integer pFmaxFilter;

	/**
	 * Minimum frequency filter value.
	 */
	@Parameter(key = TXMPreferences.F_MIN)
	protected Integer pFminFilter;

	/**
	 * Minimum frequency filter value.
	 */
	@Parameter(key = IndexPreferences.SORTED_COLUMN, type = Parameter.RENDERING)
	protected Integer pSortedColumn;

	@Parameter(key = IndexPreferences.REVERSED_SORT, type = Parameter.RENDERING)
	protected Boolean pReversedSort;

	/**
	 * Number of lines to display per page.
	 */
	@Parameter(key = TXMPreferences.NB_OF_ELEMENTS_PER_PAGE)
	protected Integer pNLinesPerPage;

	/**
	 * The word properties to display.
	 */
	@Parameter(key = TXMPreferences.UNIT_PROPERTIES)
	protected List<WordProperty> pProperties;

	/**
	 * The string used to separated property values.
	 */
	@Parameter(key = IndexPreferences.PROPERTIES_SEPARATOR)
	protected String pPropertiesSeparator;

	/**
	 * The CQP query.
	 */
	@Parameter(key = TXMPreferences.QUERIES)
	protected ArrayList<IQuery> pQueries;
	
	/**
	 * The CQP queries.
	 */
	@Parameter(key = TXMPreferences.QUERY)
	protected ArrayList<IQuery> pQuery; // not used but necessary to avoid the QueryViewget@QUERY to be filled with a subcorpus@QUERY parameter

	/**
	 * The line index of the current index page.
	 */
	@Parameter(key = IndexPreferences.N_TOP_INDEX)
	protected Integer pTopIndex;

	/**
	 * The vmax filter value parameter.
	 */
	@Parameter(key = TXMPreferences.V_MAX)
	protected Integer pVmaxFilter;

	/**
	 * Do projection on properties
	 */
	@Parameter(key = "projection")
	protected Boolean pProjection = true;

	/**
	 * 
	 * @param parent
	 */
	public Index(CQPCorpus parent) {
		super(parent);
	}

	/**
	 * 
	 * @param parametersNodePath
	 */
	public Index(String parametersNodePath) {
		super(parametersNodePath);
	}

	/**
	 * 
	 * @param parametersNodePath
	 */
	public Index(String parametersNodePath, TXMResult parent) {
		super(parametersNodePath, parent);
	}


	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		// reset all stored data
		if (lines != null) {
			lines.clear();
			counts.clear();
			currentpartid = 0;
			nTotalTokens = 0;
		}

		// FIXME: tests to not recompute all on fmax, vmax, etc. changes
		// It works but the problem is the cut() method that really cut the lines rather than doing getLines() that could filtering and returns only the lines between fmin/fmax
		// if(this.hasParameterChanged(TXMPreferences.QUERY) || this.hasParameterChanged(TXMPreferences.UNIT_PROPERTIES)) {

		if (!_computeLines(monitor)) {
			Log.severe("Error while computing lines. Aborting.");
			return false;
		}
		// }
		this.filterLines();
		monitor.worked(30);

		monitor.setTask("Sorting...");
		this.sortLines();
		monitor.worked(30);

		this.cut();

		this.pTopIndex = 0;

		monitor.setTask("Index done.");

		return true;
	}


	/**
	 * Extends this method to change the way the line are computed. the lines are sorted and filterd afterward
	 * 
	 * @return true if the index lines hae been computed
	 * @throws Exception
	 */
	protected boolean _computeLines(TXMProgressMonitor monitor) throws Exception {
		if (parent instanceof CQPCorpus) {

			if (!scanCorpus((CQPCorpus) parent)) {
				lines = new ArrayList<>();
				return false;
			}

			setLineCounts();

			getAllLines();
			return true;
		}
		else {
			Log.severe("Error: Index parent is not a corpus.");
			return false;
		}
	}

	/**
	 * Creates a CQL query string from the specified Index lines.
	 *
	 * @param lines
	 * @return the query
	 */
	public static Query createQuery(List<Line> lines) {
		String query = ""; //$NON-NLS-1$
		if (lines.size() == 0) {
			return new CQLQuery(query);
		}

		Line line = lines.get(0);
		if (line.getProperties().size() == 0) {
			if (line.getSignature() instanceof Query q) {
				return q;
			}
			return new CQLQuery(line.getSignature().toString()); // C PT QUANDLE NOM EST CHANGéééééé, ca fait quoi si je met 2 fois le meme nomm de requete ????
		}
		else {
			int nbToken = line.getUnitsProperties().get(0).size();
			int nbProps = line.getProperties().size();
			int nbLines = lines.size();
			List<Property> props = line.getProperties();
			for (int t = 0; t < nbToken; t++) {
				query += "["; //$NON-NLS-1$
				for (int p = 0; p < nbProps; p++) {

					String test = null;

					if (props.get(p) instanceof StructuralUnitProperty) {
						test = "_." + ((StructuralUnitProperty) props.get(p)).getFullName() + "=\""; //$NON-NLS-1$ //$NON-NLS-2$

						for (int l = 0; l < nbLines; l++) {
							line = lines.get(l);
							List<List<String>> values = line.getUnitsProperties();
							String s = values.get(p).get(t);
							s = CQLQuery.addBackSlash(s);
							query += s + "|"; //$NON-NLS-1$
						}
						test = test.substring(0, query.length() - 1);

						test += "\""; //$NON-NLS-1$
					}
					else if (props.get(p) instanceof VirtualProperty) {
						ArrayList<String> values = new ArrayList<>();
						for (int l = 0; l < nbLines; l++) {
							line = lines.get(l);
							String s = line.getUnitsProperties().get(p).get(t);
							values.add(s);
						}

						test = ((VirtualProperty) props.get(p)).getCQLTest(values);
					}
					else if (props.get(p) instanceof WordProperty) {
						ArrayList<String> values = new ArrayList<>();
						for (int l = 0; l < nbLines; l++) {
							line = lines.get(l);
							String s = line.getUnitsProperties().get(p).get(t);
							values.add(s);
						}

						test = ((WordProperty) props.get(p)).getCQLTest(values);
					}


					if (test != null) {
						if (p > 0) {
							query += " & "; //$NON-NLS-1$
						}
						query += test;
					}
				}

				query += "] "; //$NON-NLS-1$
			}
			query = query.substring(0, query.length() - 1);
			return new CQLQuery(query);
		}
	}


	/**
	 * Creates a Query list from the specified Index lines.
	 * 
	 * @param lines
	 * @return
	 */
	public static List<CQLQuery> createQueries(List<Line> lines) {
		List<CQLQuery> queries = new ArrayList<>();

		if (lines.size() == 0) return queries;

		Line firstLine = lines.get(0);
		if (firstLine.getProperties().size() == 0) {
			for (Line line : lines) {
				queries.add(new CQLQuery(line.getSignature().toString()));
			}
		}
		else {


			for (Line line : lines) {
				String query = ""; //$NON-NLS-1$
				int nbToken = line.getUnitsProperties().get(0).size();
				int nbProps = line.getProperties().size();
				List<List<String>> values = line.getUnitsProperties();
				List<Property> props = line.getProperties();
				for (int t = 0; t < nbToken; t++) {
					query += "["; //$NON-NLS-1$
					for (int p = 0; p < nbProps; p++) {

						if (props.get(p) instanceof WordProperty) {
							String s = line.getUnitsProperties().get(p).get(t);

							String test = ((WordProperty) props.get(p)).getCQLTest(s);
							if (test != null) {
								query += test;
							}
						}

						if (p < nbProps - 1) {
							query += " & "; //$NON-NLS-1$
						}
					}
					query += "] "; //$NON-NLS-1$
				}
				queries.add(new CQLQuery(query));
			}
		}
		return queries;
	}



	/**
	 * This method alter the index first column frequencies using a table stored in the R workspace
	 * 
	 * @param referenceCorpus the R table variable name
	 * @return true if frequencies have been altered
	 * 
	 * @throws RWorkspaceException
	 * @throws REXPMismatchException
	 */
	public boolean alterFrequencies(String referenceCorpus) throws RWorkspaceException, REXPMismatchException {
		String[] ref_forms = RWorkspace.getRWorkspaceInstance().eval("rownames(" + referenceCorpus + ")").asStrings(); //$NON-NLS-1$ //$NON-NLS-2$
		int[] ref_freqs = RWorkspace.getRWorkspaceInstance().eval(referenceCorpus + "[,1]").asIntegers(); //$NON-NLS-1$
		if (ref_forms.length != ref_freqs.length) {
			System.out.println("Cannot alter index frequencies with the '" + referenceCorpus + "' empty table.");
			return false;
		}
		HashMap<String, Integer> ref_counts = new HashMap<>();
		for (int i = 0; i < ref_forms.length; i++) {
			ref_counts.put(ref_forms[i], ref_freqs[i]);
		}

		for (org.txm.index.core.functions.Line l : this.getAllLines()) {
			String key = l.toString();
			if (ref_counts.containsKey(key)) {
				int[] f = { ref_counts.get(key) };
				l.setCounts(f, 0);
			}
		}
		this.updateFminFmax();
		return true;
		// voc.toTxt(new File("/home/mdecorde/TEMP/after.tsv"), "UTF-8", "\t", "");
	}

	@Override
	public boolean canCompute() {
		if (this.getCorpus() == null) {
			Log.fine("Corpus not set."); //$NON-NLS-1$
			return false;
		}

		if (pProperties == null || pProperties.size() == 0) {
			Log.fine("No property set."); //$NON-NLS-1$
			return false;
		}

		if (pQueries == null || pQueries.size() == 0) {
			Log.fine("No query set."); //$NON-NLS-1$
			return false;
		}

		return true;
	}

	@Override
	public boolean saveParameters() {

		this.saveParameter(TXMPreferences.UNIT_PROPERTIES, WordProperty.propertiesToString(pProperties));

		if (pQueries != null) {
			this.saveParameter(TXMPreferences.QUERY, Query.queriesToJSON(pQueries));
		}

		return true;
	}

	@Override
	public boolean loadParameters() {
		this.pProperties = (List<WordProperty>) Property.stringToProperties(getCorpus(), this.getStringParameterValue(TXMPreferences.UNIT_PROPERTIES));
		try {
			this.pQueries = new ArrayList<>(Query.stringToQueries(this.getStringParameterValue(TXMPreferences.QUERY)));
		} catch (Exception e) {
			this.pQueries = new ArrayList<>();
			this.pQueries.add(Query.stringToQuery(this.getStringParameterValue(TXMPreferences.QUERY)));
		}
		return true;
	}

	@Override
	public void clean() {
		try {
			if (this.writer != null) {
				this.writer.flush();
				this.writer.close();
			}
		}
		catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	/**
	 * keep the vmax lines more frequents.
	 *
	 */
	public void cut() {
		if (pVmaxFilter == null) {
			return;
		}

		this.acquireSemaphore();

		Log.fine("Cutting Tmax=" + pVmaxFilter);
		// assume the lines are sorted
		// int before = lines.size();
		this.lines = this.lines.subList(0, Math.min(lines.size(), pVmaxFilter));
		this.updateFminFmax();

		this.releaseSemaphore();
	}

	/**
	 * Equals.
	 *
	 * @param index the index
	 * @return true, if successful
	 */
	public boolean equals(Index index) {
		try {
			return this.pQueries.equals(index.getQueries())
					&& this.pProperties.equals(index.getProperties())
					&& this.pFminFilter == index.getFilterFmin()
					&& this.pFmaxFilter == index.getFilterFmax();
		}
		catch (Exception e) {
		}
		return false;
	}


	/**
	 * Removes lines with frequency not in [Fmin,Fmax] range.
	 *
	 **/
	public void filterLines() {

		if (!(pFminFilter > 0 && pFmaxFilter > 0 && pFminFilter <= pFmaxFilter)) {
			return;
		}

		Log.fine("Filtering Fmin = " + pFminFilter + " and Fmax = " + pFmaxFilter); //$NON-NLS-1$ //$NON-NLS-2$

		for (int i = 0; i < lines.size(); i++) { // for each line

			Line line = lines.get(i);
			int f = line.getFrequency();
			if (f < pFminFilter) { // if its frequency is not in the interval, remove it

				nTotalTokens -= line.getFrequency();
				lines.remove(i);
				i--;
				continue; // no need to go further the line is removed
			}
			if (f > pFmaxFilter) { // if its frequency is not in the interval, remove it

				nTotalTokens -= line.getFrequency();
				lines.remove(i);
				i--;
			}
		}

		this.updateFminFmax();
	}

	/**
	 * Gets the corpus.
	 *
	 * @return the corpus
	 */
	public CQPCorpus getCorpus() {
		return (CQPCorpus) this.parent;
	}

	@Override
	public String getDetails() {

		try {
			Object[] params = new Object[] { this.parent.getSimpleName(), this.getQuery().getQueryString(), this.getProperties(), this.getFmin(), this.getFmax() };
			return NLS.bind(IndexCoreMessages.indexColonCorpusP0queryP1propertiesP2FminP3FmaxP4, params);
		}
		catch (Exception e) {
			return getName();
		}
	}

	@Override
	public String getSimpleDetails() {

		try {
			if (this.hasBeenComputedOnce()) {
				return TXMCoreMessages.bind(TXMCoreMessages.fminP0fmaxP1tP2vP3, getFmin(), getFmax(), getT(), getV());
			}
			else {
				return getName();
			}
		}
		catch (Exception e) {
			return getName();
		}
	}

	/**
	 * Gets the filter fmax.
	 *
	 * @return the filter fmax
	 */
	public Integer getFilterFmax() {
		return pFmaxFilter;
	}

	/**
	 * Gets the filter fmin.
	 *
	 * @return the filter fmin
	 */
	public Integer getFilterFmin() {
		return pFminFilter;
	}

	/**
	 * Gets the filter vmax.
	 *
	 * @return the filter vmax
	 */
	public Integer getFilterVmax() {
		return pVmaxFilter;
	}

	/**
	 * Gets the fmax.
	 *
	 * @return the fmax
	 */
	public int getFmax() {
		return Fmax;
	}

	/**
	 * Gets the fmin.
	 *
	 * @return the fmin
	 */
	public int getFmin() {
		return Fmin;
	}

	/**
	 * return the lines from le "start"th one to the "end"th one.
	 *
	 * @param start the start
	 * @param end the end
	 * @return the lines
	 */
	public List<Line> getLines(int start, int end) {
		// long time = System.currentTimeMillis();
		List<Line> selectedLines = new ArrayList<>();
		if (lines.size() > 0) {
			start = Math.max(0, start);
			end = Math.min(end, lines.size());
			selectedLines = lines.subList(start, end);

			if (pProjection) {
				int p = 0;
				// for each property get the string values of the tokens
				for (Property property : pProperties) {

					int len = 0;
					for (Line l : selectedLines) {
						len += l.UnitsIds.get(p).length;
					}

					int[] indices = new int[len]; // build the array of indices
					len = 0;
					for (Line l : selectedLines) {
						int[] ids = l.UnitsIds.get(p);
						System.arraycopy(ids, 0, indices, len, ids.length);
						len += ids.length;
					}
					String[] strs = null;
					try {
						if (property instanceof StructuralUnitProperty) {
							strs = CorpusManager.getCorpusManager().getCqiClient().struc2Str(property.getQualifiedName(), indices);
						}
						else if (property instanceof WordProperty) {
							strs = ((WordProperty) property).id2Str(indices);
						}
					}
					catch (Exception e) {
						org.txm.utils.logger.Log.printStackTrace(e);
						return null;
					}
					len = 0;
					for (Line l : selectedLines) {
						int[] ids = l.UnitsIds.get(p);
						String[] lstr = new String[ids.length];
						System.arraycopy(strs, len, lstr, 0, ids.length);
						if (l.UnitsProperty.size() == pProperties.size()) continue; // the line is already initialized
						l.put(property, Arrays.asList(lstr));
						len += ids.length;
					}
					p++;
				}
			}
			else {
				// nothing to prepare
			}


		}
		// System.out.println("Time get lines "+(System.currentTimeMillis()-time));
		return selectedLines;
	}

	/**
	 * return all the lines of the index.
	 *
	 * @return the all lines
	 */
	public List<Line> getAllLines() {
		return getLines(0, lines.size());
	}

	public int size() {
		return lines.size();
	}

	/**
	 * update the lines counts.
	 */
	protected void setLineCounts() {
		for (Line line : lines) {// for each Line set its count
			int[] c = new int[1];
			for (int i = 0; i < 1; i++) {
				if (counts.get(line.getSignature()).size() <= i) {
					c[i] = 0;
				}
				else {
					c[i] = counts.get(line.getSignature()).get(i);
				}
			}
			line.setCounts(c, -1);
		}
	}

	/**
	 * Gets the number of lines per page.
	 * 
	 * @return the number of lines per page
	 */
	public Integer getNLinesPerPage() {
		return pNLinesPerPage;
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public List<WordProperty> getProperties() {
		return this.pProperties;
	}


	/**
	 * 
	 * @return
	 */
	public String getPropertySeparator() {
		return pPropertiesSeparator;
	}

	/**
	 * Gets the query.
	 *
	 * @return the query used
	 */
	public ArrayList<IQuery> getQueries() {
		return pQueries;
	}


	@Override
	public String getSimpleName() {
		String name = "";
		if (this.pQueries != null && !this.pQueries.isEmpty() && this.pProperties != null && this.pProperties.size() > 0 && this.pProperties.get(0) != null) {
			name += this.pQueries.toString();
			name += WordProperty.asString(this.pProperties);
			name += TXMCoreMessages.formatMinFilter(this.pFminFilter);
			name += TXMCoreMessages.formatMaxFilter(this.pFmaxFilter);
			name += TXMCoreMessages.formatVMaxFilter(this.pVmaxFilter);
		}
		else {
			name = this.getEmptyName();
		}
		return name;
	}


	@Override
	public String getName() {
		try {
			return this.getParent().getName() + TXMPreferences.PARENT_NAME_SEPARATOR + this.getSimpleName();
		}
		catch (NullPointerException e) {
			return this.getEmptyName();
		}
	}


	@Override
	public String getComputingStartMessage() {
		return TXMCoreMessages.bind(IndexCoreMessages.indexOfP0PropertiesP1InTheP2Corpus, (this.pQueries != null ? this.pQueries.toString() : "<no query>"), WordProperty.asString(this.pProperties),
				this
						.getCorpus().getName());
	}


	@Override
	public String getComputingDoneMessage() {
		if (this.lines.isEmpty()) {
			return TXMCoreMessages.common_noResults;
		}
		else {
			return TXMCoreMessages.bind(TXMCoreMessages.common_P0ItemsForP1Occurrences, TXMCoreMessages.formatNumber(this.lines.size()), TXMCoreMessages.formatNumber(this.nTotalTokens));
		}
	}


	/**
	 * Gets the number of tokens found.
	 *
	 * @return the number of tokens returned by the selection
	 */
	public int getT() {
		return nTotalTokens;
	}

	/**
	 * 
	 * @return
	 */
	public int getTopIndex() {
		return pTopIndex;
	}

	/**
	 * Gets the v.
	 *
	 * @return the number of entries in the index
	 */
	public int getV() {
		return lines.size();
	}

	/**
	 * count tokens.
	 *
	 * @param corpus the corpus to scan
	 * @return true, if successful
	 * @throws CqiClientException
	 * @throws CqiServerError
	 * @throws IOException
	 */
	protected boolean scanCorpus(CQPCorpus corpus) throws Exception {
		// get the cqp result of the query

		// long time = System.currentTimeMillis();
		Selection result = null;
		//		if (pQuery instanceof CQLQuery) {
		//			result = corpus.query((CQLQuery) pQuery, "index", true); //$NON-NLS-1$
		//		}
		//		else {
		//			
		//		}
		//		QueryResult unionizedResult = null;
		//		for (IQuery pQuery : pQueries) { // TODO UNION the all the query result into one query result, difficult if the queries are not CQL queries :s
		//			if (pQuery instanceof CQLQuery cqlQuery) {
		//				QueryResult tmpResult = (QueryResult) cqlQuery.getSearchEngine().query(corpus, pQuery, "index", true); //$NON-NLS-1$
		//				if (unionizedResult == null) {
		//					unionizedResult = tmpResult; // easy
		//				} else {
		//					unionizedResult = Subcorpus.union(corpus, unionizedResult.getQualifiedCqpId(), tmpResult.getQualifiedCqpId());
		//				}
		//			}
		//		}

		for (IQuery pQuery : pQueries) { // TODO UNION the all the query result into one query result, difficult if the queries are not CQL queries :s
			result = pQuery.getSearchEngine().query(corpus, pQuery, "index", true); //$NON-NLS-1$
			int nbresults = result.getNMatch();
			this.nTotalTokens += nbresults; // get number of tokens

			if (pProjection) {
				boolean isTargetUsed = result.isTargetUsed();

				// System.out.println("nLines : "+nLines);
				List<? extends Match> matches = null;
				if (nbresults > 0) {
					matches = result.getMatches(0, nbresults - 1); // get the indexes sequences of result's tokens
				}
				else {
					matches = new ArrayList<>();
				}
				// count matches
				// time = System.currentTimeMillis();
				List<Integer> allpositions = new ArrayList<>();
				for (int j = 0; j < nbresults; j++) {
					Match match = matches.get(j);
					// beginingOfKeywordsPositions.add(match.getStart()); // get the
					// first index
					// lengthOfKeywords.add(match.size());// get the last index
					if (isTargetUsed) {
						allpositions.add(match.getTarget());
					}
					else {
						for (int i = match.getStart(); i <= match.getEnd(); i++) {
							allpositions.add(i);
						}
					}
				}

				int[] allpositionsarray = new int[allpositions.size()];
				int pcount = 0;
				for (int p : allpositions) {
					allpositionsarray[pcount++] = p;
				}

				// time = System.currentTimeMillis();
				HashMap<Property, int[]> propsId = new HashMap<>();
				for (Property property : pProperties) {
					try {
						if (property instanceof StructuralUnitProperty) {
							int[] structs = CorpusManager.getCorpusManager().getCqiClient().cpos2Struc(property.getQualifiedName(), allpositionsarray);
							propsId.put(property, structs);
						}
						else if (property instanceof WordProperty) {
							int[] indices = ((WordProperty) property).cpos2Id(allpositionsarray);
							propsId.put(property, indices);
						}
					}
					catch (Exception e) {
						org.txm.utils.logger.Log.printStackTrace(e);
						result.drop();
						return false;
					}
				}
				// System.out.println("Time recup indices "+(System.currentTimeMillis()-time));
				int currentIndex = 0;
				// time = System.currentTimeMillis();
				for (int i = 0; i < nbresults; i++) {
					Line line = new Line();
					line.orderNumber = i + 1;
					Match match = matches.get(i);
					int size = match.size();
					if (isTargetUsed) {
						size = 1;
					}
					for (int p = 0; p < pProperties.size(); p++) {
						Property property = pProperties.get(p);
						int[] allprosids = propsId.get(property);
						int[] ids = new int[size];
						System.arraycopy(allprosids, currentIndex, ids, 0, size);
						line.putIds(property, ids);
					}
					currentIndex += size;

					Object signature = line.getSignature();

					// if the counts contains the signature, increment its corresponding value
					if (counts.containsKey(signature)) {
						while (counts.get(signature).size() <= currentpartid) {
							counts.get(signature).add(0);
						}
						int c = counts.get(signature).get(currentpartid) + 1;
						counts.get(signature).set(currentpartid, c);
					}
					// else initialize count of the signature to 1
					else {
						// System.out.println("add new sign "+signature+" of line "+line.toString());
						ArrayList<Integer> tmp = new ArrayList<>();
						for (int j = 0; j < currentpartid + 1; j++) {
							tmp.add(0);
						}
						counts.put(signature, tmp);
						counts.get(signature).set(currentpartid, 1);

						lines.add(line);
					}
				}
			}
			else { // no projection

				if (counts.containsKey(pQuery)) {
					while (counts.get(pQuery).size() <= currentpartid) {
						counts.get(pQuery).add(0);
					}
					int c = counts.get(pQuery).get(currentpartid) + nbresults;
					counts.get(pQuery).set(currentpartid, c);
				}
				// else initialize count of the signature to 1
				else {

					ArrayList<Integer> tmp = new ArrayList<>();
					for (int j = 0; j < currentpartid + 1; j++) {
						tmp.add(0);
					}
					counts.put(pQuery, tmp);
					counts.get(pQuery).set(currentpartid, nbresults);
					Line line = new Line();
					line.setSignature(pQuery);
					lines.add(line);
				}
			}
			result.drop();
		}

		// System.out.println("Time count lines "+(System.currentTimeMillis()-time));
		// System.out.println("took "+(System.currentTimeMillis()-time));
		return true;

	}

	/**
	 * 
	 * @param nLinesPerPage
	 */
	public void setNLinesPerPage(int nLinesPerPage) {
		this.pNLinesPerPage = Math.max(nLinesPerPage, 1);
	}

	/**
	 * 
	 * @param props
	 */
	public void setParameters(List<WordProperty> props) {
		this.pProperties = props;
	}

	/**
	 * Sets the query.
	 * 
	 * @param query
	 */
	public void setQuery(CQLQuery query) {
		this.pQueries = new ArrayList<>();
		this.pQueries.add(query);
	}

	/**
	 * Sets the query.
	 * 
	 * @param query
	 */
	public void setQueries(List<IQuery> queries) {
		if (queries != null) {
			this.pQueries = new ArrayList<>(queries);
		}
		else {
			this.pQueries = new ArrayList<>();
		}
	}

	/**
	 * Sets the properties.
	 * 
	 * @param properties
	 */
	public void setProperties(List<WordProperty> properties) {
		this.pProperties = properties;
	}

	/**
	 * Sets the property.
	 * Clears all existing properties.
	 * 
	 * @param property
	 */
	public void setProperty(WordProperty property) {
		List<WordProperty> properties = new ArrayList<>();
		properties.add(property);
		this.setProperties(properties);
	}

	public void setParameters(List<IQuery> queries, List<WordProperty> props, Integer filterFmin, Integer filterFmax, Integer filterVmax, Integer nLinesPerPage) {
		this.pQueries = new ArrayList<>(queries);
		this.pProperties = new ArrayList<>(props);
		if (filterFmax != null) this.pFmaxFilter = filterFmax;
		if (filterFmin != null) this.pFminFilter = filterFmin;
		if (filterVmax != null) this.pVmaxFilter = filterVmax;
		if (nLinesPerPage != null) this.pNLinesPerPage = nLinesPerPage;
	}

	public void setParameters(CQLQuery cqlQuery, List<WordProperty> props, Integer filterFmin, Integer filterFmax, Integer filterVmax, Integer nLinesPerPage) {

		this.pQueries = new ArrayList<>();
		this.pQueries.add(cqlQuery);
		this.pProperties = new ArrayList<>(props);
		if (filterFmax != null) this.pFmaxFilter = filterFmax;
		if (filterFmin != null) this.pFminFilter = filterFmin;
		if (filterVmax != null) this.pVmaxFilter = filterVmax;
		if (nLinesPerPage != null) this.pNLinesPerPage = nLinesPerPage;
	}

	public void setTopLine(int i) {
		pTopIndex = Math.max(i, 0);
	}

	public void setVMax(int maxFilter) {
		this.pVmaxFilter = maxFilter;
	}

	/**
	 * Sort lines.
	 *
	 * @param mode the mode
	 * @param reverse the reverse
	 */
	public void sortLines() {

		LineComparator lc = new LineComparator(pSortedColumn, pReversedSort);
		lc.initialize(this.getCorpus());

		Collections.sort(lines, lc);

		this.pTopIndex = 0; // return to the first page
	}

	/**
	 * dump the index in the console.
	 *
	 * @throws CqiClientException the cqi client exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void toConsole() throws CqiClientException, IOException {
		System.out.println(NLS.bind(IndexCoreMessages.consoleColonP0, (lines.size())));
		toConsole(0, lines.size());
	}

	/**
	 * dump a part of the index.
	 *
	 * @param from the from
	 * @param to the to
	 * @throws CqiClientException the cqi client exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void toConsole(int from, int to) throws CqiClientException, IOException {

		String header = ""; //$NON-NLS-1$
		for (Property p : pProperties) {
			header += (p + pPropertiesSeparator);
		}
		header = header.substring(0, header.length() - 1);
		header += "\tF"; //$NON-NLS-1$

		System.out.println(header);
		for (int i = from; i < to; i++) {
			Line ligne = lines.get(i);
			System.out.print(ligne + "\t" + ligne.getFrequency()); //$NON-NLS-1$
			System.out.print("\n"); //$NON-NLS-1$
		}
		System.out.flush();
	}

	/**
	 * Write the lines between from and to on a writer.
	 *
	 * @param outfile the outfile
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @throws CqiClientException the cqi client exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	// FIXME: should be in an exporter extension
	@Deprecated
	public void toTSVDictionnary(File outfile, String colseparator, String encoding) throws CqiClientException, IOException {

		// String colseparator = "\t";

		this.writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outfile), encoding));

		String header = ""; //$NON-NLS-1$
		for (Property p : pProperties) {
			header += (p + colseparator);
		}

		// header = header.substring(0, header.length() - 1);
		header += "F"; //$NON-NLS-1$
		header += "\n"; //$NON-NLS-1$
		writer.write(header);

		for (Line ligne : lines) {

			for (int i = 0; i < pProperties.size(); i++) {
				writer.write(StringUtils.join(ligne.getUnitsProperties().get(i), " ") + colseparator);
			}

			writer.write(Integer.toString(ligne.getFrequency()));

			writer.write("\n"); //$NON-NLS-1$
		}
		writer.flush();
		writer.close();
	}

	/**
	 * Write the lines between from and to on a writer.
	 *
	 * @param outfile the outfile
	 * @param from The first line to be written
	 * @param to The last line to be writen
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @throws CqiClientException the cqi client exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	// FIXME: should be in an exporter extension
	@Deprecated
	public void toTxt(File outfile, int from, int to, String encoding, String colseparator, String txtseparator)
			throws CqiClientException, IOException {
		// NK: writer declared as class attribute to perform a clean if the operation is interrupted
		this.writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outfile), encoding));
		// if ("UTF-8".equals(encoding)) writer.write('\ufeff'); // UTF-8 BOM
		String header = ""; //$NON-NLS-1$
		for (Property p : pProperties) {
			header += (p + pPropertiesSeparator);
		}
		header = txtseparator + header.substring(0, header.length() - 1) + txtseparator;
		header += colseparator + txtseparator + "F" + txtseparator; //$NON-NLS-1$
		header += "\n"; //$NON-NLS-1$
		writer.write(header);

		// for(Line ligne: lines)
		for (int i = from; i < to; i++) {
			Line ligne = lines.get(i);
			writer.write(txtseparator + ligne.toString().replace(txtseparator, txtseparator + txtseparator) + txtseparator + colseparator + ligne.getFrequency());
			writer.write("\n"); //$NON-NLS-1$
		}
		writer.flush();
		writer.close();
	}

	/**
	 * Write all the lines on a writer.
	 *
	 * @param outfile the outfile
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @return true, if successful
	 */
	@Override
	// FIXME: should be in an exporter extension
	@Deprecated
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) {
		try {
			toTxt(outfile, 0, lines.size(), encoding, colseparator, txtseparator);
		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.bind(IndexCoreMessages.error_failedToExportLexiconColonP0, Log.toString(e)));
			return false;
		}
		return true;
	}

	/**
	 * checks all lines to update Fmin and Fmax.
	 */
	protected void updateFminFmax() {
		Fmin = Integer.MAX_VALUE;
		Fmax = Integer.MIN_VALUE;
		for (int i = 0; i < lines.size(); i++) {
			Line line = lines.get(i);
			int f = line.getFrequency();
			if (f < Fmin) {
				Fmin = f;
			}
			if (f > Fmax) {
				Fmax = f;
			}
		}
	}

	@Override
	public String getResultType() {
		return IndexCoreMessages.RESULT_TYPE;
	}

	/** The novoc. */
	protected static int novoc = 1;

	/** The prefix r. */
	protected static String prefixR = "Index"; //$NON-NLS-1$

	/**
	 * As r matrix.
	 *
	 * TODO MD: restore the asRMatrix method used by the execR macros as demonstration
	 * TODO MD: finalize a export to R mechanism (maybe linked to the export engines to be implemented
	 *
	 * @deprecated
	 * @return the string
	 * @throws RWorkspaceException the r workspace exception
	 */
	@Deprecated
	public String asRMatrix() throws RWorkspaceException {
		String symbol = prefixR + novoc;

		ArrayList<String> colnames = new ArrayList<>();
		ArrayList<String> partnames = new ArrayList<>();
		partnames.add(this.getCorpus().getName());

		colnames.add("F"); //$NON-NLS-1$
		if (partnames.size() > 1)
			for (int j = 0; j < partnames.size(); j++)
			colnames.add(partnames.get(j));

		// System.out.println("cols: "+colnames);

		String[] keywords = new String[this.lines.size()];

		int[] freq = new int[this.lines.size()];
		ArrayList<int[]> partfreqs = new ArrayList<>(partnames.size());
		if (partnames.size() > 1)
			for (int j = 0; j < partnames.size(); j++)
			partfreqs.add(new int[this.lines.size()]);

		for (int i = 0; i < lines.size(); i++) {
			Line ligne = lines.get(i);
			freq[i] = ligne.getFrequency();
			keywords[i] = ligne.toString();
			if (partnames.size() > 1)
				for (int j = 0; j < partnames.size(); j++) {
					partfreqs.get(j)[i] = ligne.getFrequency(j);
				}

		}

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		if (partnames.size() > 1)
			for (int j = 0; j < partnames.size(); j++)
			rw.addVectorToWorkspace("vocpartfreqs" + j, partfreqs.get(j)); //$NON-NLS-1$
		rw.addVectorToWorkspace("vocfreq", freq); //$NON-NLS-1$
		rw.addVectorToWorkspace("vockeywords", keywords); //$NON-NLS-1$
		rw.addVectorToWorkspace("voccolnames", colnames.toArray(new String[colnames.size()])); //$NON-NLS-1$

		int ncol = 1;
		if (partnames.size() > 1)
			ncol += partnames.size();

		int nrow = lines.size();
		String partscmd = ""; //$NON-NLS-1$
		if (partnames.size() > 1)
			for (int j = 0; j < partnames.size(); j++)
			partscmd += ", vocpartfreqs" + j; //$NON-NLS-1$

		rw.eval(symbol + "<- matrix(data = c(vocfreq" + partscmd + "), nrow = " + nrow + ", ncol = " + ncol + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		rw.eval("colnames(" + symbol + " ) <- voccolnames"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval("rownames(" + symbol + " ) <- vockeywords"); //$NON-NLS-1$ //$NON-NLS-2$
		String[] s = new String[getProperties().size()];
		for (int i = 0 ; i < getProperties().size() ; i++) s[i] = getProperties().get(i).getName();
		rw.addVectorToWorkspace("tmp", s);
		rw.eval(symbol + "<- list(data=" + symbol + ", properties=tmp, filters=c(fmin="+pFminFilter+", fmax="+pFmaxFilter+", vmax="+pVmaxFilter+"))"); //$NON-NLS-1$ //$NON-NLS-2$

		novoc++;
		return symbol;
	}

	/**
	 * 
	 * @param i the column number to sort with. Use "-1" to sort using totals
	 */
	public void setSortColumn(int i) {

		this.pSortedColumn = i;
	}

	public void setSortDirection(boolean reverseSort) {

		this.pReversedSort = reverseSort;
	}

	public int getSortColumn() {
		return pSortedColumn;
	}

	public boolean isReversedSort() {
		return pReversedSort;
	}

	@Override
	public IQuery getQuery() {

		if (pQueries.size() > 0) {
			return pQueries.get(0);
		}
		else {
			return null;
		}
	}

	public boolean getDoProjection() {

		return pProjection;
	}
}
