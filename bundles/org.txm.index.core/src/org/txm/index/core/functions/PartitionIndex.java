// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-10-21 11:09:43 +0200 (Fri, 21 Oct 2016) $
// $LastChangedRevision: 3323 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.index.core.functions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.rosuda.REngine.REXPMismatchException;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.index.core.messages.IndexCoreMessages;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Part;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * Computes an index of a partition.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class PartitionIndex extends Index {

	/**
	 * 
	 * @param parent
	 */
	public PartitionIndex(Partition parent) {
		super(null, parent);
	}

	/**
	 * 
	 * @param parametersNodePath
	 */
	public PartitionIndex(String parametersNodePath) {
		this(parametersNodePath, null);
	}

	/**
	 * 
	 * @param parent
	 */
	public PartitionIndex(String parametersNodePath, Partition parent) {
		super(parametersNodePath, parent);
	}

	@Override
	public Partition getParent() {
		return (Partition) parent;
	}


	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {
		lines.clear();
		counts.clear();
		currentpartid = 0;
		nTotalTokens = 0;

		Partition partition = getParent();
		for (Part part : partition.getParts()) {
			scanCorpus(part);
			currentpartid++;
		}

		monitor.worked(30);

		setLineCounts();

		getAllLines();

		this.filterLines();


		monitor.worked(30);

		monitor.setTask("Sorting...");

		this.sortLines();

		this.cut();

		monitor.worked(30);

		this.pTopIndex = 0;

		monitor.setTask("Index done.");

		return true;
	}



	/**
	 * This method alter the index first column frequencies using a table stored in the R workspace
	 * 
	 * @param referenceCorpus the R table variable name
	 * @return true if frequencies have been altered
	 * 
	 * @throws RWorkspaceException
	 * @throws REXPMismatchException
	 */
	@Override
	public boolean alterFrequencies(String referenceCorpus) throws RWorkspaceException, REXPMismatchException {
		String[] ref_forms = RWorkspace.getRWorkspaceInstance().eval("rownames(" + referenceCorpus + ")").asStrings(); //$NON-NLS-1$ //$NON-NLS-2$
		int[] ref_freqs = RWorkspace.getRWorkspaceInstance().eval(referenceCorpus + "[,1]").asIntegers(); //$NON-NLS-1$
		if (ref_forms.length != ref_freqs.length) {
			System.out.println("Cannot alter index frequencies with the '" + referenceCorpus + "' empty table.");
			return false;
		}
		HashMap<String, Integer> ref_counts = new HashMap<>();
		for (int i = 0; i < ref_forms.length; i++) {
			ref_counts.put(ref_forms[i], ref_freqs[i]);
		}

		for (org.txm.index.core.functions.Line l : this.getAllLines()) {
			String key = l.toString();
			if (ref_counts.containsKey(key)) {
				int[] f = { ref_counts.get(key) };
				l.setCounts(f, 0);
			}
		}
		this.updateFminFmax();
		return true;
		// voc.toTxt(new File("/home/mdecorde/TEMP/after.tsv"), "UTF-8", "\t", "");
	}

	@Override
	public boolean canCompute() {
		if (this.getPartition() == null) {
			Log.fine("Partition not set."); //$NON-NLS-1$
			return false;
		}
		return super.canCompute();
	}


	@Override
	public CQPCorpus getCorpus() {
		try {
			return ((Partition) this.parent).getParent();
		}
		catch (Exception e) {
		}
		return null;
	}

	/**
	 * Gets the part names.
	 *
	 * @return the part names
	 */
	public List<String> getPartNames() {
		return this.getPartition().getPartNames();
	}

	@Override
	public String getDetails() {
		try {
			Object[] params = new Object[] { this.parent.getSimpleName(), this.getQuery().getQueryString(), this.getProperties(), this.getFmin(), this.getFmax() };
			String str;
			if (this.parent instanceof Partition) {
				str = NLS.bind(IndexCoreMessages.indexColonPartitionP0queryP1propertiesP2FminP3FmaxP4, params);
			}
			else {
				str = NLS.bind(IndexCoreMessages.indexColonCorpusP0queryP1propertiesP2FminP3FmaxP4, params);
			}
			return str;
		}
		catch (Exception e) {
			return getName();
		}
	}

	@Override
	public String getComputingStartMessage() {
		return TXMCoreMessages.bind(IndexCoreMessages.indexOfP0PropertiesP1OnP2Partition, this.pQueries.toString(), WordProperty.asString(this.pProperties), this.getPartition().getName());
	}

	/**
	 * update the lines counts.
	 */
	@Override
	protected void setLineCounts() {
		// for each Line set its count
		for (Line line : lines) {
			int[] c = new int[this.getPartition().getPartsCount()];
			for (int i = 0; i < this.getPartition().getPartsCount(); i++) {
				if (counts.get(line.getSignature()).size() <= i) {
					c[i] = 0;
				}
				else {
					c[i] = counts.get(line.getSignature()).get(i);
				}
			}
			line.setCounts(c, -1);
		}
	}


	/**
	 * Gets the number of lines per page.
	 * 
	 * @return the number of lines per page
	 */
	@Override
	public Integer getNLinesPerPage() {
		return pNLinesPerPage;
	}


	/**
	 * Gets the partition.
	 *
	 * @return the partition if exists otherwise null
	 */
	public Partition getPartition() {
		try {
			return (Partition) this.parent;
		}
		catch (ClassCastException e) {
		}
		return null;
	}



	/**
	 * Tell if the index has been computed with a partition or not.
	 *
	 * @return true, if is computed with partition
	 */
	public boolean isComputedWithPartition() {
		return (this.getParent() instanceof Partition);
	}

	/**
	 * 
	 * 
	 * /**
	 * 
	 * @param props
	 */
	@Override
	public void setParameters(List<WordProperty> props) {
		this.pProperties = props;
	}



	/**
	 * dump a part of the index.
	 *
	 * @param from the from
	 * @param to the to
	 * @throws CqiClientException the cqi client exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void toConsole(int from, int to) throws CqiClientException, IOException {

		List<Part> parts = this.getPartition().getParts();

		String header = ""; //$NON-NLS-1$
		for (Property p : pProperties) {
			header += (p + pPropertiesSeparator);
		}
		header = header.substring(0, header.length() - 1);
		header += "\tF"; //$NON-NLS-1$
		if (parts.size() > 1) {
			for (int j = 0; j < parts.size(); j++) {
				header += "\t" + parts.get(j).getName(); //$NON-NLS-1$
			}
		}

		System.out.println(header);
		for (int i = from; i < to; i++) {
			Line ligne = lines.get(i);
			System.out.print(ligne + "\t" + ligne.getFrequency()); //$NON-NLS-1$
			if (parts.size() > 1) {
				for (int j = 0; j < parts.size(); j++) {
					System.out.print("\t" + ligne.getFrequency(j)); //$NON-NLS-1$
				}
			}
			System.out.print("\n"); //$NON-NLS-1$
		}
		System.out.flush();
	}


	/**
	 * Write the lines between from and to on a writer.
	 *
	 * @param outfile the outfile
	 * @param from The first line to be written
	 * @param to The last line to be writen
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @throws CqiClientException the cqi client exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	// FIXME: should be in an exporter extension
	@Deprecated
	public void toTxt(File outfile, int from, int to, String encoding, String colseparator, String txtseparator)
			throws CqiClientException, IOException {


		List<Part> parts = this.getPartition().getParts();

		// NK: writer declared as class attribute to perform a clean if the operation is interrupted
		this.writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outfile), encoding));
		// if ("UTF-8".equals(encoding)) writer.write('\ufeff'); // UTF-8 BOM
		String header = ""; //$NON-NLS-1$
		for (Property p : pProperties) {
			header += (p + pPropertiesSeparator);
		}
		header = txtseparator + header.substring(0, header.length() - 1) + txtseparator;
		header += colseparator + txtseparator + "F" + txtseparator; //$NON-NLS-1$
		if (parts.size() > 1) {
			for (int j = 0; j < parts.size(); j++) {
				header += colseparator + txtseparator + parts.get(j).getName().replace(txtseparator, txtseparator + txtseparator) + txtseparator;
			}
		}
		header += "\n"; //$NON-NLS-1$
		writer.write(header);

		// for(Line ligne: lines)
		for (int i = from; i < to; i++) {
			Line ligne = lines.get(i);
			writer.write(txtseparator + ligne.toString().replace(txtseparator, txtseparator + txtseparator) + txtseparator + colseparator + ligne.getFrequency());
			if (parts.size() > 1) {
				for (int j = 0; j < parts.size(); j++) {
					writer.write(colseparator + ligne.getFrequency(j));
				}
			}
			writer.write("\n"); //$NON-NLS-1$
		}
		writer.flush();
		writer.close();
	}



	@Override
	public String getResultType() {
		return PartitionIndex.class.getSimpleName();
	}

	/** The novoc. */
	protected static int novoc = 1;

	/** The prefix r. */
	protected static String prefixR = "PartitionIndex"; //$NON-NLS-1$

	/**
	 * As r matrix.
	 *
	 * TODO MD: restore the asRMatrix method used by the execR macros as demonstration
	 * TODO MD: finalize a export to R mechanism (maybe linked to the export engines to be implemented
	 *
	 * @return the string
	 * @throws RWorkspaceException the r workspace exception
	 */
	public String asRMatrix() throws RWorkspaceException {
		String symbol = prefixR + novoc;

		ArrayList<String> colnames = new ArrayList<String>();
		List<String> partnames = getPartNames();
		partnames.add(this.getCorpus().getName());

		colnames.add("F"); //$NON-NLS-1$
		if (partnames.size() > 1)
			for (int j = 0; j < partnames.size(); j++)
			colnames.add(partnames.get(j));

		//System.out.println("cols: "+colnames);

		String[] keywords = new String[this.lines.size()];

		int[] freq = new int[this.lines.size()];
		ArrayList<int[]> partfreqs = new ArrayList<int[]>(partnames.size());
		if (partnames.size() > 1)
			for (int j = 0; j < partnames.size(); j++)
			partfreqs.add(new int[this.lines.size()]);

		for (int i = 0; i < lines.size(); i++) {
			Line ligne = lines.get(i);
			freq[i] = ligne.getFrequency();
			keywords[i] = ligne.toString();
			if (partnames.size() > 1)
				for (int j = 0; j < partnames.size(); j++) {
					partfreqs.get(j)[i] = ligne.getFrequency(j);
				}

		}

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		if (partnames.size() > 1)
			for (int j = 0; j < partnames.size(); j++)
			rw.addVectorToWorkspace("vocpartfreqs" + j, partfreqs.get(j)); //$NON-NLS-1$
		rw.addVectorToWorkspace("vocfreq", freq); //$NON-NLS-1$
		rw.addVectorToWorkspace("vockeywords", keywords); //$NON-NLS-1$
		rw.addVectorToWorkspace("voccolnames", colnames.toArray(new String[colnames.size()])); //$NON-NLS-1$

		int ncol = 1;
		if (partnames.size() > 1)
			ncol += partnames.size();

		int nrow = lines.size();
		String partscmd = ""; //$NON-NLS-1$
		if (partnames.size() > 1)
			for (int j = 0; j < partnames.size(); j++)
			partscmd += ", vocpartfreqs" + j; //$NON-NLS-1$

		rw.eval(symbol + "<- matrix(data = c(vocfreq" + partscmd + "), nrow = " + nrow + ", ncol = " + ncol + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		rw.eval("colnames(" + symbol + " ) <- voccolnames"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval("rownames(" + symbol + " ) <- vockeywords"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval(symbol + "<- list(data=" + symbol + ")"); //$NON-NLS-1$ //$NON-NLS-2$

		novoc++;
		return symbol;
	}
}
