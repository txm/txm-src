// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.para.rcp.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.para.rcp.editors.ParaBrowserEditorInput;
import org.txm.para.rcp.messages.ParaBrowserUIMessages;
import org.txm.rcp.StatusLine;
import org.txm.searchengine.cqp.corpus.MainCorpus;

// TODO: Auto-generated Javadoc
/**
 * Display and browse text of structural unit of parallel corpora
 * 
 * @author mdecorde
 */
public class OpenParaBrowser extends AbstractHandler {

	/** The ID. */
	public static String ID = OpenParaBrowser.class.getName();

	/** The window. */
	private IWorkbenchWindow window;

	/** The selection. */
	private IStructuredSelection selection;

	/** The editor input. */
	private ParaBrowserEditorInput editorInput;

	// org.txm.rcp.commands.function.OpenParaBrowser
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		window = HandlerUtil.getActiveWorkbenchWindow(event);
		selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		/*
		 * Object s = selection.getFirstElement(); if (s == null) return null;
		 * if (!(s instanceof MainCorpus)) return null; final MainCorpus corpus =
		 * (MainCorpus) s;
		 */
		List<Object> list = selection.toList();
		List<MainCorpus> corpusList = new ArrayList<MainCorpus>();
		for (Object co : list)
			corpusList.add((MainCorpus) co);

		editorInput = new ParaBrowserEditorInput(corpusList);// Put a list of
																// corpus

		IWorkbenchPage page = window.getActivePage();
		try {
			StatusLine
					.setMessage(ParaBrowserUIMessages.openingTheParabrowser);
			page
					.openEditor(editorInput,
							"org.txm.rcp.editors.parabrowser.ParaBrowserEditor"); //$NON-NLS-1$
		}
		catch (PartInitException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}

		return null;
	}

}
