package org.txm.para.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;


/**
 * Parallel browser UI messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ParaBrowserUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.para.rcp.messages.messages"; //$NON-NLS-1$

	public static String paraBrowserColonGetNextP0StructP1ParaidP2ValueP3;

	public static String ParaBrowserEditor_21;

	public static String openParabrowser;

	public static String refColonP0;

	public static String references;

	public static String refColonP0ColonP1;

	public static String openingTheParabrowser;


	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, ParaBrowserUIMessages.class);
	}

	private ParaBrowserUIMessages() {
	}
}
