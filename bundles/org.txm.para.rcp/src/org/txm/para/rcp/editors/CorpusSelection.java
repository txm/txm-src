// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.para.rcp.editors;

import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.txm.searchengine.cqp.corpus.MainCorpus;

// TODO: Auto-generated Javadoc
/**
 * The Class CorpusSelection.
 */
public class CorpusSelection extends Action implements IWorkbenchAction {

	/** The Constant ID. */
	private static final String ID = "org.txm.rcp.editors.parabrowser.corpusselection"; //$NON-NLS-1$

	/** The window. */
	private IWorkbenchWindow window;

	/** The para browser editor. */
	private ParaBrowserEditor paraBrowserEditor;

	/**
	 * Instantiates a new corpus selection.
	 *
	 * @param window the window
	 * @param paraBrowserEditor the para browser editor
	 */
	public CorpusSelection(IWorkbenchWindow window,
			ParaBrowserEditor paraBrowserEditor) {
		this.window = window;
		this.paraBrowserEditor = paraBrowserEditor;
		setId(ID);
		setText("Définir les corpus à l'affichage"); //$NON-NLS-1$
		setToolTipText("Corpus à l'affichage"); //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	@Override
	public void dispose() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		if (paraBrowserEditor != null) {
			List<MainCorpus> selectedCorpus = paraBrowserEditor
					.getSelectedCorpus();
			List<MainCorpus> availableCorpus = paraBrowserEditor
					.getAvailableCorpus();

			CorpusSelectionDialog d = new CorpusSelectionDialog(window
					.getShell(), availableCorpus, selectedCorpus);
			if (d.open() == Window.OK) {
				if (paraBrowserEditor == null) {
					paraBrowserEditor.setSelectedCorpus(d.getSelectedCorpus(),
							d.getAvailableCorpus());
				}
			}
		}
	}

	/**
	 * The Class CorpusSelectionDialog.
	 */
	private class CorpusSelectionDialog extends Dialog {

		/** The available corpus. */
		final private List<MainCorpus> availableCorpus;

		/** The selected corpus. */
		final private List<MainCorpus> selectedCorpus;

		/** The available corpus view. */
		org.eclipse.swt.widgets.List availableCorpusView;

		/** The selected corpus view. */
		org.eclipse.swt.widgets.List selectedCorpusView;

		/**
		 * Instantiates a new corpus selection dialog.
		 *
		 * @param parentShell the parent shell
		 * @param availableCorpus the available corpus
		 * @param selectedCorpus the selected corpus
		 */
		protected CorpusSelectionDialog(Shell parentShell,
				List<MainCorpus> availableCorpus,
				List<MainCorpus> selectedCorpus) {
			super(parentShell);
			this.availableCorpus = availableCorpus;
			this.selectedCorpus = selectedCorpus;
			// TODO Auto-generated constructor stub
		}

		/**
		 * Gets the selected corpus.
		 *
		 * @return the selected corpus
		 */
		public List<MainCorpus> getSelectedCorpus() {
			return selectedCorpus;
		}

		/**
		 * Gets the available corpus.
		 *
		 * @return the available corpus
		 */
		public List<MainCorpus> getAvailableCorpus() {
			return availableCorpus;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
		 */
		@Override
		protected Control createDialogArea(Composite parent) {
			// parent.setLayout(new FormLayout());
			Composite mainArea = new Composite(parent, SWT.NONE);
			mainArea
					.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			// 4 columns
			GridLayout layout = new GridLayout(4, false);
			mainArea.setLayout(layout);

			availableCorpusView = new org.eclipse.swt.widgets.List(mainArea,
					SWT.BORDER | SWT.V_SCROLL);
			GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			availableCorpusView.setLayoutData(gridData);

			Composite selectionButtons = new Composite(mainArea, SWT.NONE);
			selectionButtons.setLayout(new GridLayout(1, false));
			Button select = new Button(selectionButtons, SWT.ARROW | SWT.RIGHT);
			Button unselect = new Button(selectionButtons, SWT.ARROW | SWT.LEFT);

			selectedCorpusView = new org.eclipse.swt.widgets.List(mainArea,
					SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
			gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			selectedCorpusView.setLayoutData(gridData);

			Composite orderButtons = new Composite(mainArea, SWT.NONE);
			orderButtons.setLayout(new GridLayout(1, false));
			Button up = new Button(orderButtons, SWT.ARROW | SWT.UP);
			Button down = new Button(orderButtons, SWT.ARROW | SWT.DOWN);

			select.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					int index = availableCorpusView.getSelectionIndex();

					selectedCorpus.add(availableCorpus.get(index));
					availableCorpus.remove(index);

					reload();
					availableCorpusView.setSelection(index);
				}
			});

			unselect.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					if (selectedCorpus.size() > 1) {
						int index = selectedCorpusView.getSelectionIndex();

						availableCorpus.add(selectedCorpus.get(index));
						selectedCorpus.remove(index);

						reload();
						selectedCorpusView.setSelection(index);
					}
				}
			});

			up.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					int index = selectedCorpusView.getSelectionIndex();
					if (index > 0) {
						MainCorpus selectedC = selectedCorpus.get(index);
						MainCorpus upperC = selectedCorpus.get(index - 1);

						selectedCorpus.set(index, upperC);
						selectedCorpus.set(index - 1, selectedC);

						reload();
						selectedCorpusView.setSelection(index - 1);
					}
				}
			});

			down.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					int index = selectedCorpusView.getSelectionIndex();
					if (index < selectedCorpus.size() - 1) {
						MainCorpus selectedC = selectedCorpus.get(index);
						MainCorpus bellowC = selectedCorpus.get(index + 1);

						selectedCorpus.set(index, bellowC);
						selectedCorpus.set(index + 1, selectedC);

						reload();
						selectedCorpusView.setSelection(index + 1);
					}
				}
			});

			availableCorpusView.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseDoubleClick(MouseEvent e) {
					int index = selectedCorpusView.getSelectionIndex();

					selectedCorpus.add(availableCorpus.get(index));
					availableCorpus.remove(index);

					reload();
					availableCorpusView.setSelection(index);
				}
			});

			selectedCorpusView.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseDoubleClick(MouseEvent e) {
					if (selectedCorpus.size() > 1) {
						int index = selectedCorpusView.getSelectionIndex();

						availableCorpus.add(selectedCorpus.get(index));
						selectedCorpus.remove(index);

						reload();
						selectedCorpusView.setSelection(index);
					}
				}
			});

			reload();
			return mainArea;
		}

		/**
		 * Reload.
		 */
		public void reload() {
			System.out
					.println("CorpusSelectionDialog.reload() => availableCorpus[" //$NON-NLS-1$
							+ availableCorpus.size()
							+ "] / selectedCorpus[" //$NON-NLS-1$
							+ selectedCorpus.size() + "]"); //$NON-NLS-1$
			availableCorpusView.removeAll();
			for (MainCorpus corpus : availableCorpus) {
				availableCorpusView.add(corpus.getID());
			}
			selectedCorpusView.removeAll();
			for (MainCorpus corpus : selectedCorpus) {
				selectedCorpusView.add(corpus.getID());
			}

			availableCorpusView.getParent().layout();
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
		 */
		@Override
		protected void okPressed() {
			if (selectedCorpus.size() != 0)
				super.okPressed();
		}
	}
}
