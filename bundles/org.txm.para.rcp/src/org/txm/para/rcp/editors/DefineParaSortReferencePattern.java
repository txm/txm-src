// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.para.rcp.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.ReferencePattern;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;

// TODO: Auto-generated Javadoc
/**
 * Action to build the reference pattern.
 *
 * @author mdecorde
 */
public class DefineParaSortReferencePattern extends Action implements IWorkbenchAction {

	/** The Constant ID. */
	private static final String ID = "org.txm.rcp.editors.concordances.definereferencepattern"; //$NON-NLS-1$

	/** The window. */
	private IWorkbenchWindow window;

	/** The concordance editor. */
	private ConcordanceEditor concordanceEditor;

	/** The para browser editor. */
	private ParaBrowserEditor paraBrowserEditor;

	/**
	 * Instantiates a new define reference pattern.
	 *
	 * @param window the window
	 * @param concordanceEditor the concordance editor
	 */
	public DefineParaSortReferencePattern(IWorkbenchWindow window,
			ConcordanceEditor concordanceEditor) {
		this.window = window;
		this.concordanceEditor = concordanceEditor;
		setId(ID);
		setText(TXMUIMessages.referencesAmpsortOptions);
		setToolTipText(TXMUIMessages.referencesSortOptions);
	}

	/**
	 * Instantiates a new define reference pattern.
	 *
	 * @param window the window
	 * @param paraBrowserEditor the para browser editor
	 */
	public DefineParaSortReferencePattern(IWorkbenchWindow window,
			ParaBrowserEditor paraBrowserEditor) {
		this.window = window;
		this.paraBrowserEditor = paraBrowserEditor;
		setId(ID);
		setText(TXMUIMessages.referencesAmpsortOptions);
		setToolTipText(TXMUIMessages.referencesSortOptions);
	}


	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	@Override
	public void dispose() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		List<StructuralUnit> structuralUnits = null;
		List<Property> availableReferenceItems = new ArrayList<Property>();
		try {
			if (paraBrowserEditor != null) {
				structuralUnits = paraBrowserEditor.getCorpus()
						.getOrderedStructuralUnits();
				availableReferenceItems.addAll(paraBrowserEditor.getCorpus()
						.getOrderedProperties());

			}
			else if (concordanceEditor != null) {
				structuralUnits = concordanceEditor.getCorpus()
						.getOrderedStructuralUnits();
				availableReferenceItems.addAll(concordanceEditor.getCorpus()
						.getOrderedProperties());

			}

		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}

		for (StructuralUnit unit : structuralUnits) {
			availableReferenceItems.addAll(unit.getProperties());
		}

		// System.out.println("selected "+concordanceEditor.getSelectedReferences());
		// System.out.println("before avail"+availableReferenceItems);
		if (paraBrowserEditor != null) {
			availableReferenceItems.removeAll(paraBrowserEditor
					.getSelectedReferences());
		}
		else {
			availableReferenceItems.removeAll(concordanceEditor
					.getSelectedSortReferences());
		}

		// System.out.println("after avail"+availableReferenceItems);
		DefineReferencePatternDialog d = null;
		if (paraBrowserEditor != null) {
			d = new DefineReferencePatternDialog(window.getShell(),
					availableReferenceItems, paraBrowserEditor
							.getSelectedReferences());
		}
		else if (concordanceEditor != null) {
			d = new DefineReferencePatternDialog(window.getShell(),
					availableReferenceItems, concordanceEditor
							.getSelectedSortReferences());
		}

		if (d.open() == Window.OK) {
			if (paraBrowserEditor != null) {
				paraBrowserEditor.setReferencePattern(new ReferencePattern(d
						.getSelectesdReferenceItems()));
			}
			else if (concordanceEditor != null) {
				concordanceEditor.setRefSortPattern(new ReferencePattern(d
						.getSelectesdReferenceItems()));
			}
			//System.out.println(concordanceEditor.getSortReferencePattern());
		}
	}

	/**
	 * The Class DefineReferencePatternDialog.
	 */
	private class DefineReferencePatternDialog extends Dialog {

		/** The available reference items. */
		private List<Property> availableReferenceItems;

		/** The selected reference items. */
		private List<Property> selectedReferenceItems;

		/**
		 * Instantiates a new define reference pattern dialog.
		 *
		 * @param parentShell the parent shell
		 * @param availableReferenceItems the available reference items
		 * @param selectedReferences the selected references
		 */
		protected DefineReferencePatternDialog(Shell parentShell,
				List<Property> availableReferenceItems,
				List<Property> selectedReferences) {
			super(parentShell);

			this.availableReferenceItems = availableReferenceItems;
			this.selectedReferenceItems = selectedReferences;
			this.availableReferenceItems.removeAll(selectedReferences);
			this.setShellStyle(this.getShellStyle() | SWT.RESIZE);
		}

		/**
		 * Gets the selectesd reference items.
		 *
		 * @return the selectesd reference items
		 */
		public List<Property> getSelectesdReferenceItems() {
			return selectedReferenceItems;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
		 */
		@Override
		protected void configureShell(Shell shell) {
			super.configureShell(shell);
			shell.setText(TXMUIMessages.referencesSortOptions);

			shell.setSize(400, 500);
			shell.setLocation(shell.getParent().getLocation());
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
		 */
		@Override
		protected Control createDialogArea(Composite parent) {

			Composite mainArea = new Composite(parent, SWT.NONE);
			mainArea.setLayoutData(new GridData(GridData.FILL, GridData.FILL,
					true, true));
			mainArea.setLayout(new GridLayout(4, false));

			final org.eclipse.swt.widgets.List availbleItems = new org.eclipse.swt.widgets.List(
					mainArea, SWT.BORDER | SWT.V_SCROLL);
			for (Property property : availableReferenceItems) {
				if (property instanceof StructuralUnitProperty) {
					StructuralUnitProperty sProperty = (StructuralUnitProperty) property;
					availbleItems.add(sProperty.getStructuralUnit().getName()
							+ ": " + property.getName()); //$NON-NLS-1$
				}
				else {
					availbleItems.add(property.getName());
				}
			}

			GridData gridData = new GridData(GridData.FILL, GridData.FILL,
					true, true);
			//Point size = availbleItems.computeSize(SWT.DEFAULT, SWT.DEFAULT);
			//gridData.widthHint = size.x;
			availbleItems.setLayoutData(gridData);

			Composite selectionButtons = new Composite(mainArea, SWT.NONE);
			selectionButtons.setLayout(new GridLayout(1, false));
			Button select = new Button(selectionButtons, SWT.ARROW | SWT.RIGHT);
			Button unselect = new Button(selectionButtons, SWT.ARROW | SWT.LEFT);

			final org.eclipse.swt.widgets.List selectedItems = new org.eclipse.swt.widgets.List(
					mainArea, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
			selectedItems.setLayoutData(gridData);
			for (Property property : selectedReferenceItems) {
				if (property instanceof StructuralUnitProperty) {
					StructuralUnitProperty sProperty = (StructuralUnitProperty) property;
					selectedItems.add(sProperty.getStructuralUnit().getName()
							+ ": " + property.getName()); //$NON-NLS-1$
				}
				else {
					selectedItems.add(property.getName());
				}
			}

			//DBL CLICK Listeners
			selectedItems.addMouseListener(new MouseListener() {

				@Override
				public void mouseUp(MouseEvent e) {
				}

				@Override
				public void mouseDown(MouseEvent e) {
				}

				@Override
				public void mouseDoubleClick(MouseEvent e) {
					int ind = selectedItems.getSelectionIndex();
					availbleItems.add(selectedItems.getItem(ind));
					selectedItems.remove(ind);
					availableReferenceItems
							.add(selectedReferenceItems.get(ind));
					selectedReferenceItems.remove(ind);
					selectedItems.setSelection(ind);
				}
			});
			availbleItems.addMouseListener(new MouseListener() {

				@Override
				public void mouseUp(MouseEvent e) {
				}

				@Override
				public void mouseDown(MouseEvent e) {
				}

				@Override
				public void mouseDoubleClick(MouseEvent e) {
					int ind = availbleItems.getSelectionIndex();
					selectedItems.add(availbleItems.getItem(ind));
					availbleItems.remove(ind);
					selectedReferenceItems
							.add(availableReferenceItems.get(ind));
					availableReferenceItems.remove(ind);
					availbleItems.setSelection(ind);
				}
			});

			Composite orderButtons = new Composite(mainArea, SWT.NONE);
			orderButtons.setLayout(new GridLayout(1, false));
			Button up = new Button(orderButtons, SWT.ARROW | SWT.UP);
			Button down = new Button(orderButtons, SWT.ARROW | SWT.DOWN);

			select.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					int ind = availbleItems.getSelectionIndex();
					selectedItems.add(availbleItems.getItem(ind));
					availbleItems.remove(ind);
					selectedReferenceItems
							.add(availableReferenceItems.get(ind));
					availableReferenceItems.remove(ind);
					availbleItems.setSelection(ind);
				}
			});

			unselect.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					int ind = selectedItems.getSelectionIndex();
					availbleItems.add(selectedItems.getItem(ind));
					selectedItems.remove(ind);
					availableReferenceItems
							.add(selectedReferenceItems.get(ind));
					selectedReferenceItems.remove(ind);
					selectedItems.setSelection(ind);
				}
			});

			up.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					int ind = selectedItems.getSelectionIndex();
					if (ind > 0) {
						String value = selectedItems.getItem(ind);
						selectedItems.remove(ind);
						selectedItems.add(value, ind - 1);

						Property prop = selectedReferenceItems.get(ind);
						selectedReferenceItems.remove(ind);
						selectedReferenceItems.add(ind - 1, prop);

						selectedItems.setSelection(ind - 1);
					}
				}
			});

			down.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					int ind = selectedItems.getSelectionIndex();
					if (ind < selectedItems.getItemCount() - 1) {
						String value = selectedItems.getItem(ind);
						selectedItems.remove(ind);
						selectedItems.add(value, ind + 1);

						Property prop = selectedReferenceItems.get(ind);
						selectedReferenceItems.remove(ind);
						selectedReferenceItems.add(ind + 1, prop);

						selectedItems.setSelection(ind + 1);
					}
				}
			});

			return mainArea;
		}
	}
}
