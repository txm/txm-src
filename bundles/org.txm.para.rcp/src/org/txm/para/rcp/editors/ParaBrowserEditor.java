// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.para.rcp.editors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
import org.txm.concordance.rcp.messages.ConcordanceUIMessages;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.para.core.functions.ParaBrowser;
import org.txm.para.rcp.messages.ParaBrowserUIMessages;
import org.txm.rcp.StatusLine;
import org.txm.searchengine.cqp.ReferencePattern;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.utils.Pair;

// TODO: Auto-generated Javadoc
/**
 * The Class ParaBrowserEditor.
 */
public class ParaBrowserEditor extends EditorPart {

	/** The Constant ID. */
	public static final String ID = "org.txm.rcp.editors.parabrowser.ParaBrowserEditor"; //$NON-NLS-1$

	/** The display area. */
	private Composite displayArea;

	/** The define reference pattern action. */
	protected Action defineReferencePatternAction;

	/** The define order corpus action. */
	protected Action defineOrderCorpusAction;

	/** The corpus text areas. */
	private Map<MainCorpus, StyledText> corpusTextAreas;

	/** The corpus ref areas. */
	private Map<MainCorpus, Label> corpusRefAreas;

	/** The corpus query result areas. */
	private Map<MainCorpus, QueryResult> corpusQueryResultAreas;

	/** The selected structural unit. */
	private StructuralUnit selectedStructuralUnit;

	/** The selected structural unit para id. */
	private StructuralUnitProperty selectedStructuralUnitParaId;

	/** The selected value. */
	private String selectedValue;

	/** The structural units combo. */
	private Combo structuralUnitsCombo;

	/** The structural units final. */
	private List<StructuralUnit> structuralUnitsFinal;

	/** The para id combo. */
	private Combo paraIdCombo;

	/** The reference pattern. */
	private ReferencePattern referencePattern = new ReferencePattern();

	/** The selected reference. */
	private String selectedReference = ""; //$NON-NLS-1$

	/** The selected view corpus. */
	private List<MainCorpus> selectedViewCorpus;

	/** The available view corpus. */
	private List<MainCorpus> availableViewCorpus;

	/** The available view properties. */
	private List<StructuralUnit> availableViewProperties;

	/** The selected view properties. */
	private List<StructuralUnit> selectedViewProperties;

	private ParaBrowser paraBrowser;

	/** The next. */
	private Button next;

	/** The prev. */
	private Button prev;

	/**
	 * Instantiates a new para browser editor.
	 */
	public ParaBrowserEditor() {
	}

	/**
	 * Do save.
	 *
	 * @param arg0 the arg0
	 * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor arg0) {
		// TODO Auto-generated method stub
	}

	/**
	 * Do save as.
	 *
	 * @see "org.eclipse.lyon gournd zeroui.part.EditorPart#doSaveAs()"
	 */
	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub
	}

	/**
	 * Inits the.
	 *
	 * @param site the site
	 * @param input the input
	 * @throws PartInitException the part init exception
	 * @see org.eclipse.ui.part.EditorPart#init(org.eclipse.ui.IEditorSite,
	 *      org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setSite(site);
		setInput(input);

		this.selectedViewCorpus = ((ParaBrowserEditorInput) input)
				.getCorpusList();
		List<MainCorpus> tempView = new ArrayList<MainCorpus>();
		availableViewCorpus = new ArrayList<MainCorpus>();
		if (selectedViewCorpus.size() > 3) {
			int i = 0;
			for (MainCorpus corpus : selectedViewCorpus) {
				if (i < 4) {
					tempView.add(corpus);
				}
				else {
					availableViewCorpus.add(corpus);
				}
				++i;
			}
			selectedViewCorpus = tempView;
		}
	}

	/**
	 * Checks if is dirty.
	 *
	 * @return true, if is dirty
	 * @see org.eclipse.ui.part.EditorPart#isDirty()
	 */
	@Override
	public boolean isDirty() {
		return false;
	}

	/**
	 * Checks if is save as allowed.
	 *
	 * @return true, if is save as allowed
	 * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/**
	 * Creates the part control.
	 *
	 * @param parent the parent
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(final Composite parent) {
		corpusTextAreas = new HashMap<MainCorpus, StyledText>();
		corpusRefAreas = new HashMap<MainCorpus, Label>();
		corpusQueryResultAreas = new HashMap<MainCorpus, QueryResult>();

		parent.setLayout(new GridLayout(1, false));

		Composite controlArea = new Composite(parent, SWT.BORDER);
		controlArea
				.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));// FILL,
		composeControlArea(controlArea);

		displayArea = new Composite(parent, SWT.NONE);
		displayArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));// 2ème
																				// arg
																				// :
																				// true
		composeDisplayArea(displayArea);
		reloadTexts();
	}

	/**
	 * Compose display area.
	 *
	 * @param displayArea the display area
	 */
	private void composeDisplayArea(Composite displayArea) {

		if (selectedViewCorpus != null) {
			displayArea.setLayout(new GridLayout(selectedViewCorpus.size(),
					true));

			for (int i = 0; i < selectedViewCorpus.size(); ++i) {
				composeSingleTextDisplayArea(displayArea, selectedViewCorpus
						.get(i), selectedViewCorpus.size());
			}
		}
		else {
			displayArea.setLayout(new GridLayout(1, false));

			selectedViewCorpus = new ArrayList<MainCorpus>();
			selectedViewCorpus.add(selectedViewCorpus.get(0));
			composeSingleTextDisplayArea(displayArea,
					selectedViewCorpus.get(0), 1);
		}

	}

	/**
	 * Compose single text display area.
	 *
	 * @param displayArea the display area
	 * @param displayedCorpus the displayed corpus
	 * @param nbtextDisplayAreas the nbtext display areas
	 */
	private void composeSingleTextDisplayArea(Composite displayArea,
			MainCorpus displayedCorpus, int nbtextDisplayAreas) {
		Composite singleCorpusArea = new Composite(displayArea, SWT.NONE);
		singleCorpusArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
				true));
		GridLayout layout = new GridLayout(1, false);
		singleCorpusArea.setLayout(layout);

		// Corpus name Label
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		Label nameCorpusLabel = new Label(singleCorpusArea, SWT.NONE);
		nameCorpusLabel.setText(displayedCorpus.getName());
		nameCorpusLabel.setLayoutData(new GridData(GridData.FILL,
				GridData.FILL, true, false));

		// Text
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		StyledText corpusTextArea = new StyledText(singleCorpusArea, SWT.WRAP
				| SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER); // SWT.BORDER
		corpusTextArea.setLayoutData(new GridData(GridData.FILL, GridData.FILL,
				true, true));
		// String text = displayedCorpus.getTextRegion(selectedStructuralUnit,
		// selectedStructuralUnitParaId, selectedValue);
		corpusTextArea.setText(""); //$NON-NLS-1$
		corpusTextAreas.put(displayedCorpus, corpusTextArea);

		// Reference Label
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		Label referenceLabel = new Label(singleCorpusArea, SWT.NONE);
		referenceLabel.setLayoutData(new GridData(GridData.FILL, GridData.FILL,
				true, false));
		if (referencePattern != null) {
			// selectedStructuralUnit.getName()
			List<Property> references = referencePattern.getProperties();
			for (Property property : references) {
				selectedReference += property.getName() + "/"; //$NON-NLS-1$
			}
		}
		referenceLabel.setText(NLS.bind(ParaBrowserUIMessages.refColonP0, selectedReference));
		corpusRefAreas.put(displayedCorpus, referenceLabel);
	}

	/**
	 * Compose control area.
	 *
	 * @param controlArea the control area
	 */
	private void composeControlArea(Composite controlArea) {
		controlArea.setLayout(new GridLayout(6, false));
		// composeCorpusDisplay(controlArea);
		composeStructuralNavigation(controlArea);
		composeNavigation(controlArea);

		createActions();

		MenuManager menuManager = new MenuManager("#PopupMenu"); //$NON-NLS-1$
		menuManager.setRemoveAllWhenShown(true);
		menuManager.addMenuListener(new IMenuListener() {

			@Override
			public void menuAboutToShow(IMenuManager manager) {
				manager.add(defineReferencePatternAction);
				manager.add(defineOrderCorpusAction);
			}
		});
		controlArea.setMenu(menuManager.createContextMenu(controlArea));
		getSite().registerContextMenu(menuManager,
				this.getSite().getSelectionProvider());
	}

	/*
	 * private void composeCorpusDisplay(Composite controlArea){ //Manager for
	 * corpus display Button buttonManager = new Button(controlArea, SWT.PUSH);
	 * buttonManager.setText("manage view"); buttonManager.addMouseListener(new
	 * MouseListener() {
	 * @Override public void mouseUp(MouseEvent e) {
	 * launchCorpusSelectionManager(); }
	 * @Override public void mouseDown(MouseEvent e) {
	 * launchCorpusSelectionManager(); }
	 * @Override public void mouseDoubleClick(MouseEvent e) { // TODO
	 * Auto-generated method stub
	 * } });
	 * //Manager to build reference Button buttonReference = new
	 * Button(controlArea, SWT.PUSH); buttonReference.setText("reference");
	 * buttonReference.addMouseListener(new MouseListener() {
	 * @Override public void mouseUp(MouseEvent e) {
	 * launchCorpusReferenceManager(); }
	 * @Override public void mouseDown(MouseEvent e) {
	 * launchCorpusReferenceManager(); }
	 * @Override public void mouseDoubleClick(MouseEvent e) { // TODO
	 * Auto-generated method stub
	 * } }); }
	 */

	/**
	 * Compose navigation.
	 *
	 * @param controlArea the control area
	 */
	private void composeNavigation(Composite controlArea) {
		Composite navigationArea = new Composite(controlArea, SWT.NONE);
		navigationArea.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
				false));// false, false
		navigationArea.setLayout(new GridLayout(2, false));
		// [<] [>]

		// [<]
		prev = new Button(navigationArea, SWT.PUSH);
		prev.setText(ConcordanceUIMessages.inf);
		prev.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				nextorpreviousText(false);
			}
		});
		prev.setEnabled(true);

		// [>]
		next = new Button(navigationArea, SWT.PUSH);
		next.setText(ConcordanceUIMessages.sup);
		next.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				nextorpreviousText(true);
			}
		});
		next.setEnabled(true);

	}

	/**
	 * Compose structural navigation.
	 *
	 * @param controlArea the control area
	 */
	private void composeStructuralNavigation(Composite controlArea) {
		// StructuralUnit
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		Button refButton = new Button(controlArea, SWT.PUSH);
		refButton.setText(ParaBrowserUIMessages.references);
		refButton
				.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, true));
		refButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				defineReferencePatternAction.run();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Label structureLabel = new Label(controlArea, SWT.NONE);
		structureLabel.setText(TXMCoreMessages.common_structure); // LEFT
		structureLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				true));

		structuralUnitsCombo = new Combo(controlArea, SWT.READ_ONLY);

		List<StructuralUnit> structuralUnits = null;
		if (selectedViewCorpus.size() > 0) {
			try {
				// same structure in all corpus, therefore use the first in the
				// list
				structuralUnits = selectedViewCorpus.get(0)
						.getOrderedStructuralUnits();
			}
			catch (CqiClientException e) {
				System.out.println("Error: " + e);
				e.printStackTrace();
				return;
			}
		}
		structuralUnitsFinal = new ArrayList<StructuralUnit>();
		if (structuralUnits != null) {
			for (StructuralUnit unit : structuralUnits) {
				if (unit.getOrderedProperties() != null
						&& unit.getOrderedProperties().size() != 0) {
					structuralUnitsCombo.add(unit.getName());
					structuralUnitsFinal.add(unit);
				}
			}
		}

		if (structuralUnitsCombo.getItemCount() == 0) {
			// this.getButton(IDialogConstants.OK_ID).setEnabled(false);
		}
		else {
			structuralUnitsCombo.select(0);
			selectedStructuralUnit = structuralUnitsFinal.get(0);
		}

		// StructuralUnitParaId
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		Label paraIdLabel = new Label(controlArea, SWT.NONE);
		paraIdLabel.setText("paraid"); //$NON-NLS-1$
		paraIdLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				true));

		paraIdCombo = new Combo(controlArea, SWT.READ_ONLY);
		paraIdCombo
				.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));

		structuralUnitsCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent event) {
				reloadIds();
			}
		});

		for (StructuralUnitProperty property : structuralUnitsFinal.get(0)
				.getOrderedProperties()) {
			if (property.getName().equals("id")) { //$NON-NLS-1$
				selectedStructuralUnitParaId = property;
				try {
					for (String value : property.getValues()) {
						paraIdCombo.add(value);
					}
				}
				catch (CqiClientException e1) {
					// TODO Auto-generated catch block
					org.txm.utils.logger.Log.printStackTrace(e1);
				}
			}
		}
		paraIdCombo.select(0);
		try {
			if (selectedStructuralUnitParaId != null)
				selectedValue = selectedStructuralUnitParaId.getValues().get(0);
		}
		catch (CqiClientException e1) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e1);
		}

		paraIdCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent event) {
				// selectedStructuralUnitParaId =
				// selectedStructuralUnit.getProperties().get(paraIdCombo.getSelectionIndex());
				if (selectedStructuralUnitParaId != null) {
					try {
						List<String> values = selectedStructuralUnitParaId
								.getValues();
						Collections.sort(values);
						selectedValue = values.get(paraIdCombo
								.getSelectionIndex());
						// System.out.println("Selection of id = "+selectedValue);
					}
					catch (CqiClientException e) {
						// TODO Auto-generated catch block
						org.txm.utils.logger.Log.printStackTrace(e);
					}
				}
				reloadTexts();
				reloadReferences();
			}
		});
	}

	/**
	 * Creates the actions.
	 */
	private void createActions() {
		defineReferencePatternAction = new DefineParaViewReferencePattern(getSite()
				.getWorkbenchWindow(), this);
		defineOrderCorpusAction = new CorpusSelection(getSite()
				.getWorkbenchWindow(), this);
	}

	/**
	 * Reload display.
	 */
	private void reloadDisplay() {
		if (selectedViewCorpus != null) {
			displayArea.setLayout(new GridLayout(selectedViewCorpus.size(),
					true));

			for (int i = 0; i < selectedViewCorpus.size(); ++i) {
				composeSingleTextDisplayArea(displayArea, selectedViewCorpus
						.get(i), selectedViewCorpus.size());
			}
		}
		else {
			displayArea.setLayout(new GridLayout(1, false));

			selectedViewCorpus = new ArrayList<MainCorpus>();
			selectedViewCorpus.add(selectedViewCorpus.get(0));
			composeSingleTextDisplayArea(displayArea,
					selectedViewCorpus.get(0), 1);
		}
		displayArea.layout();
		displayArea.pack(true);
	}

	/**
	 * Reload texts.
	 */
	private void reloadTexts() {
		Set keys = corpusTextAreas.keySet();
		Iterator<MainCorpus> it = keys.iterator();
		while (it.hasNext()) {
			MainCorpus corpus = it.next();
			StyledText textArea = corpusTextAreas.get(corpus);
			Pair<String, QueryResult> pair = paraBrowser.getTextRegion(corpus,
					selectedStructuralUnit, selectedStructuralUnitParaId,
					selectedValue);
			// corpusQueryResultAreas.put(corpus, pair.getSecond());
			textArea.setText(pair.getFirst());
			// Label ref = corpusRefAreas.get(corpus);
			// ref.setText("Ref: ...");
		}
	}

	/**
	 * Reload ids.
	 */
	private void reloadIds() {
		selectedStructuralUnit = structuralUnitsFinal.get(structuralUnitsCombo
				.getSelectionIndex());
		paraIdCombo.removeAll();

		for (StructuralUnitProperty property : structuralUnitsFinal.get(
				structuralUnitsCombo.getSelectionIndex()).getOrderedProperties()) {
			if (property.getName().equals("id")) { //$NON-NLS-1$
				selectedStructuralUnitParaId = property;
				try {
					List<String> values = property.getValues();
					Collections.sort(values);
					for (String value : values) {
						paraIdCombo.add(value);
					}
				}
				catch (CqiClientException e1) {
					// TODO Auto-generated catch block
					org.txm.utils.logger.Log.printStackTrace(e1);
				}
			}
		}

		if (paraIdCombo.getItemCount() > 0) {
			paraIdCombo.select(0);
			try {
				if (selectedStructuralUnitParaId != null) {
					List<String> values = selectedStructuralUnitParaId
							.getValues();
					Collections.sort(values);
					selectedValue = values.get(0);
					reloadTexts();
					reloadReferences();
				}
			}
			catch (CqiClientException e1) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e1);
			}
			// selectedStructuralUnitParaId =
			// selectedStructuralUnit.getProperties().get(0);
		}
	}

	/**
	 * Reload references.
	 */
	private void reloadReferences() {
		Set<MainCorpus> keys = corpusRefAreas.keySet();
		Iterator<MainCorpus> it = keys.iterator();
		while (it.hasNext()) {
			MainCorpus corpus = it.next();

			// get all reference values of all lines
			/*
			 * List<Integer> beginingOfKeywordsPositions = new
			 * ArrayList<Integer>(j-i); Map<Property, List<List<String>>>
			 * refValues = new HashMap<Property, List<List<String>>>(); for
			 * (Property property : referencePattern){
			 * refValues.put(property,cache
			 * .get(property).getData(beginingOfKeywordsPositions,
			 * Collections.nCopies(beginingOfKeywordsPositions.size(), 1))); }
			 */

			Label refArea = corpusRefAreas.get(corpus);
			refArea.setText(NLS.bind(ParaBrowserUIMessages.refColonP0ColonP1, corpus.getName(), selectedReference));
		}

	}

	/**
	 * Nextorprevious text.
	 *
	 * @param findNext the find next
	 */
	public void nextorpreviousText(boolean findNext) {
		// get Property
		System.out.println(NLS.bind(ParaBrowserUIMessages.paraBrowserColonGetNextP0StructP1ParaidP2ValueP3,
				new Object[] { findNext, selectedStructuralUnit, selectedStructuralUnitParaId, selectedValue }));
		if (selectedStructuralUnit != null) {
			// Multiple Query, as many as there are corpuses displayed
			Set keys = corpusTextAreas.keySet();
			Iterator<MainCorpus> it = keys.iterator();
			String newValue = ""; //$NON-NLS-1$
			for (int i = 0; it.hasNext(); ++i) {
				// if (i==0){
				MainCorpus corpus = it.next();
				System.out.println("CORPUS ################### " //$NON-NLS-1$
						+ corpus.getID());
				newValue = paraBrowser.getNextOrPrevious(corpus, selectedStructuralUnit,
						selectedStructuralUnitParaId, selectedValue, findNext);
				// }
			}
			if (!newValue.equals("")) { //$NON-NLS-1$
				int index = paraIdCombo.indexOf(newValue);
				if (index != -1) {
					selectedValue = newValue;
					paraIdCombo.select(index);
				}
			}
			else {
			}
			reloadTexts();
			reloadReferences();
		}
	}

	/**
	 * Sets the reference pattern.
	 *
	 * @param referencePattern the new reference pattern
	 */
	public void setReferencePattern(ReferencePattern referencePattern) {
		this.referencePattern = referencePattern;
		selectedReference = ""; ////$NON-NLS-0$
		if (referencePattern != null) {
			// selectedStructuralUnit.getName()
			List<Property> references = referencePattern.getProperties();
			for (Property property : references) {
				selectedReference += property.getName() + ParaBrowserUIMessages.ParaBrowserEditor_21;
			}
		}
		reloadReferences();
	}

	/**
	 * Sets the focus.
	 *
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		StatusLine.setMessage(ParaBrowserUIMessages.openParabrowser);
	}

	/**
	 * Gets the corpus.
	 *
	 * @return the corpus
	 */
	public MainCorpus getCorpus() {
		if (selectedViewCorpus.size() > 0) {
			return selectedViewCorpus.get(0);
		}
		else
			return null;
	}

	/**
	 * Gets the selected corpus.
	 *
	 * @return the selected corpus
	 */
	public List<MainCorpus> getSelectedCorpus() {
		return selectedViewCorpus;
	}

	/**
	 * Sets the selected corpus.
	 *
	 * @param corpus the corpus
	 * @param corpusAvailable the corpus available
	 */
	public void setSelectedCorpus(List<MainCorpus> corpus,
			List<MainCorpus> corpusAvailable) {
		selectedViewCorpus = corpus;
		availableViewCorpus = corpusAvailable;
		reloadDisplay();
	}

	/**
	 * Gets the available corpus.
	 *
	 * @return the available corpus
	 */
	public List<MainCorpus> getAvailableCorpus() {
		if (availableViewCorpus == null) {
			availableViewCorpus = new ArrayList<MainCorpus>();
		}
		return availableViewCorpus;
	}

	/**
	 * Gets the selected references.
	 *
	 * @return the selected references
	 */
	public List<Property> getSelectedReferences() {
		return referencePattern.getProperties();
	}
}
