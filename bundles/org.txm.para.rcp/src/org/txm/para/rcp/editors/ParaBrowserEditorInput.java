// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.para.rcp.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.txm.rcp.Application;
import org.txm.rcp.IImageKeys;
import org.txm.searchengine.cqp.corpus.MainCorpus;

// TODO: Auto-generated Javadoc
/**
 * The Class ParaBrowserEditorInput.
 */
public class ParaBrowserEditorInput implements IEditorInput {

	/** The corpus. */
	private List<MainCorpus> corpus;

	/**
	 * Instantiates a new para browser editor input.
	 *
	 * @param corpus the corpus
	 */
	public ParaBrowserEditorInput(List<MainCorpus> corpus) {
		this.corpus = corpus;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getImageDescriptor()
	 */
	@Override
	public ImageDescriptor getImageDescriptor() {
		return AbstractUIPlugin.imageDescriptorFromPlugin(
				Application.PLUGIN_ID, IImageKeys.ACTION_CONCORDANCES);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getName()
	 */
	@Override
	public String getName() {
		if (corpus.size() > 0) {
			return corpus.get(0).getName();
		}
		else
			return ""; //$NON-NLS-1$
	}

	/**
	 * Gets the names.
	 *
	 * @return the names
	 */
	public List<String> getNames() {
		List<String> names = new ArrayList<String>();
		for (MainCorpus corp : corpus) {
			names.add(corp.getName());
		}
		return names; // corpus.getName();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getPersistable()
	 */
	@Override
	public IPersistableElement getPersistable() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getToolTipText()
	 */
	@Override
	public String getToolTipText() {
		return getName();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@Override
	public Object getAdapter(Class adapter) {
		return null;
	}

	/**
	 * Gets the corpus.
	 *
	 * @return the corpus
	 */
	public MainCorpus getCorpus() {
		if (corpus.size() > 0) {
			return corpus.get(0);
		}
		else
			return null;
	}

	/**
	 * Gets the corpus list.
	 *
	 * @return the corpus list
	 */
	public List<MainCorpus> getCorpusList() {
		return corpus;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#exists()
	 */
	@Override
	public boolean exists() {
		return false;
	}
}
