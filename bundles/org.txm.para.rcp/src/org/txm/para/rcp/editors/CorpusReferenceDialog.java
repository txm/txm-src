// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.para.rcp.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbenchPartSite;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;

// TODO: Auto-generated Javadoc
/**
 * The Class CorpusReferenceDialog.
 */
public class CorpusReferenceDialog extends Dialog {

	/** The available reference items. */
	private List<Property> availableReferenceItems;

	/** The selected reference items. */
	private List<Property> selectedReferenceItems;

	/**
	 * Instantiates a new corpus reference dialog.
	 *
	 * @param iWorkbenchPartSite the i workbench part site
	 * @param availableReferenceItems the available reference items
	 */
	public CorpusReferenceDialog(IWorkbenchPartSite iWorkbenchPartSite,
			List<Property> availableReferenceItems) {
		super(iWorkbenchPartSite);
		this.availableReferenceItems = availableReferenceItems;
		this.selectedReferenceItems = new ArrayList<Property>();
	}

	/**
	 * Gets the selectesd reference items.
	 *
	 * @return the selectesd reference items
	 */
	public List<Property> getSelectesdReferenceItems() {
		return selectedReferenceItems;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		Composite mainArea = new Composite(parent, SWT.NONE);
		mainArea.setLayout(new GridLayout(4, false));

		final org.eclipse.swt.widgets.List availbleItems = new org.eclipse.swt.widgets.List(
				mainArea, SWT.BORDER | SWT.V_SCROLL);
		for (Property property : availableReferenceItems) {
			if (property instanceof StructuralUnitProperty) {
				StructuralUnitProperty sProperty = (StructuralUnitProperty) property;
				availbleItems.add(sProperty.getStructuralUnit().getName()
						+ ": " + property.getName()); //$NON-NLS-1$
			}
			else {
				availbleItems.add(property.getName());
			}
		}
		GridData gridData = new GridData(GridData.FILL, GridData.FILL, true,
				false);
		Point size = availbleItems.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		gridData.widthHint = size.x;
		availbleItems.setLayoutData(gridData);

		Composite selectionButtons = new Composite(mainArea, SWT.NONE);
		selectionButtons.setLayout(new GridLayout(1, false));
		Button select = new Button(selectionButtons, SWT.ARROW | SWT.RIGHT);
		Button unselect = new Button(selectionButtons, SWT.ARROW | SWT.LEFT);

		final org.eclipse.swt.widgets.List selectedItems = new org.eclipse.swt.widgets.List(
				mainArea, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		selectedItems.setLayoutData(gridData);

		Composite orderButtons = new Composite(mainArea, SWT.NONE);
		orderButtons.setLayout(new GridLayout(1, false));
		Button up = new Button(orderButtons, SWT.ARROW | SWT.UP);
		Button down = new Button(orderButtons, SWT.ARROW | SWT.DOWN);

		select.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int ind = availbleItems.getSelectionIndex();
				selectedItems.add(availbleItems.getItem(ind));
				availbleItems.remove(ind);
				selectedReferenceItems.add(availableReferenceItems.get(ind));
				availableReferenceItems.remove(ind);
				availbleItems.setSelection(ind);
			}
		});

		unselect.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int ind = selectedItems.getSelectionIndex();
				availbleItems.add(selectedItems.getItem(ind));
				selectedItems.remove(ind);
				availableReferenceItems.add(selectedReferenceItems.get(ind));
				selectedReferenceItems.remove(ind);
				selectedItems.setSelection(ind);
			}
		});

		up.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int ind = selectedItems.getSelectionIndex();
				if (ind > 0) {
					selectedItems.add(selectedItems.getItem(ind), ind - 1);
					selectedItems.remove(ind + 1);
					selectedReferenceItems.add(selectedReferenceItems
							.get(ind - 1));
					selectedReferenceItems.remove(ind + 1);
					selectedItems.setSelection(ind - 1);
				}
			}
		});

		down.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int ind = selectedItems.getSelectionIndex();
				if (ind < selectedItems.getItemCount() - 1) {
					selectedItems.add(selectedItems.getItem(ind), ind + 2);
					selectedItems.remove(ind);
					selectedReferenceItems.add(selectedReferenceItems
							.get(ind + 2));
					selectedReferenceItems.remove(ind);
					selectedItems.setSelection(ind + 2);
				}
			}
		});

		return mainArea;
	}
}
