package org.txm.querycooccurrences.rcp.editor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.txm.core.results.Parameter;
import org.txm.querycooccurrences.rcp.functions.QueryCooccurrences;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.widget.MultipleQueriesComposite;
import org.txm.rcp.swt.widget.StructureSelector;
import org.txm.rcp.swt.widget.TXMParameterCombo;
import org.txm.rcp.swt.widget.TXMParameterSpinner;
import org.txm.searchengine.core.IQuery;

public class QueryCooccurrencesEditor extends TXMEditor<QueryCooccurrences>{

	@Parameter(key = "structure", type = Parameter.COMPUTING)
	StructureSelector propsCombo;

	@Parameter(key = "dist", type = Parameter.COMPUTING)
	TXMParameterSpinner distSpinner;

	@Parameter(key = "minCooc", type = Parameter.COMPUTING)
	TXMParameterSpinner minSpinner;

	@Parameter(key = "oriented", type = Parameter.COMPUTING)
	private TXMParameterCombo orientedButton;

	private Label nQueriesXLabel, nQueriesYLabel;

	private MultipleQueriesComposite queriesXFocusComposite;

	private MultipleQueriesComposite queriesYFocusComposite;

	@Parameter(key = "queries1", type = Parameter.COMPUTING)
	private ArrayList<IQuery> queryXList;

	@Parameter(key = "queries2", type = Parameter.COMPUTING)
	private ArrayList<IQuery> queryYList;

	private org.eclipse.swt.widgets.Text resultText;

	@Override
	public void _createPartControl() throws Exception {

		this.getMainParametersComposite().getLayout().numColumns = 3;
		
		nQueriesXLabel = new Label(this.getMainParametersComposite(), SWT.NONE);
		nQueriesXLabel.setToolTipText("The number of queries crossed");
		nQueriesXLabel.setText("<no X query yet>");
		nQueriesXLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));

		new Label(this.getMainParametersComposite(), SWT.NONE).setText(" x ");

		nQueriesYLabel = new Label(this.getMainParametersComposite(), SWT.NONE);
		nQueriesYLabel.setToolTipText("The number of queries crossed");
		nQueriesYLabel.setText("<no Y query yet>");
		nQueriesYLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));

		Composite composite = this.getExtendedParametersGroup(); //new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout(6, false);
		composite.setLayout(layout);

		propsCombo = new StructureSelector(composite, SWT.READ_ONLY);
		GridData gdata = new GridData(GridData.FILL, GridData.FILL, false, false);
		propsCombo.setLayoutData(gdata);
		propsCombo.setCorpus(this.getResult().getCorpus());

		distSpinner = new TXMParameterSpinner(composite, this, new Spinner(composite, SWT.BORDER), TXMUIMessages.distance, "Number of words between the queries", false);
		distSpinner.setMinimum(0);
		distSpinner.setMaximum(1000);
		distSpinner.setSelection(10);

		minSpinner = new TXMParameterSpinner(composite, this, new Spinner(composite, SWT.BORDER), TXMUIMessages.minimalCoFrequence, "Mininal cofrequence of the queries", false);
		minSpinner.setMinimum(1);
		minSpinner.setMaximum(1000);
		minSpinner.setSelection(20);

		orientedButton = new TXMParameterCombo(composite, this, new Combo(composite, SWT.BORDER|SWT.SINGLE|SWT.READ_ONLY),
				"Orientation", new String[] {QueryCooccurrences.BOTH, QueryCooccurrences.LEFT, QueryCooccurrences.RIGHT}, "...", false);
		orientedButton.setText(QueryCooccurrences.BOTH);

		queriesXFocusComposite = new MultipleQueriesComposite(composite, SWT.NONE, this, 100, nQueriesXLabel, true);
		queriesXFocusComposite.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, true, 3,1));
		
		queriesYFocusComposite = new MultipleQueriesComposite(composite, SWT.NONE, this, 100, nQueriesYLabel, true);
		queriesYFocusComposite.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, true,3,1));
		
		
		// RESULT AREA
		
		resultText = new org.eclipse.swt.widgets.Text(getResultArea(), SWT.MULTI|SWT.WRAP|SWT.H_SCROLL);
		resultText.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
	}

	@Override
	public void updateResultFromEditor() {
		// TODO Auto-generated method stub
		queryXList = queriesXFocusComposite.getQueries();

		queryYList = queriesYFocusComposite.getQueries();
	}

	@Override
	public void updateEditorFromResult(boolean update) throws Exception {

		if (queryXList != null) {
			queriesXFocusComposite.clearQueries();
			for (IQuery q : queryXList) {
				queriesXFocusComposite.addFocusQueryField(q);
			}
		}

		if (queryYList != null) {
			queriesYFocusComposite.clearQueries();

			for (IQuery q : queryYList) {
				queriesYFocusComposite.addFocusQueryField(q);
			}
		}
		
		if (this.getResult().hasBeenComputedOnce()) {
			StringWriter sw = new StringWriter();
			this.getResult().printCoocs(new PrintWriter(sw));
			resultText.setText(sw.toString());
			sw.close();
		}
	}
}
