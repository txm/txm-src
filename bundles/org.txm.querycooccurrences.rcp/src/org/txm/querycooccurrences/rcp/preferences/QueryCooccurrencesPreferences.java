package org.txm.querycooccurrences.rcp.preferences;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

public class QueryCooccurrencesPreferences extends TXMPreferences {

	public static final String dist = "dist";
	public static final String cofreq = "cofreq";
	public static final String structure = "structure";
	public static final String orientation = "orientation";
	
	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(QueryCooccurrencesPreferences.class)) {
			new QueryCooccurrencesPreferences();
		}
		return TXMPreferences.instances.get(QueryCooccurrencesPreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.putInt(dist, 50);
		preferences.putInt(cofreq, 1);
		preferences.put(structure, "");
		preferences.put(orientation, "both");
	}
}
