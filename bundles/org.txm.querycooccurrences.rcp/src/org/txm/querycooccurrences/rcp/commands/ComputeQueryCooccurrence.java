// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.querycooccurrences.rcp.commands;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.index.core.functions.Index;
import org.txm.index.core.functions.Lexicon;
import org.txm.querycooccurrences.rcp.editor.QueryCooccurrencesEditor;
import org.txm.querycooccurrences.rcp.functions.QueryCooccurrences;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;

public class ComputeQueryCooccurrence extends BaseAbstractHandler {

	float reqtime = 0.5f;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IStructuredSelection selection = this.getCorporaViewSelection(event);
		final Object element = selection.getFirstElement();

		final Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

		QueryCooccurrences cooc = null;
		if (element instanceof CQPCorpus corpus) {
			cooc = new QueryCooccurrences(corpus);
		} else if (element instanceof QueryCooccurrences c) {
			cooc = c;
		} else if (element instanceof Index index) {
			cooc = new QueryCooccurrences(index.getCorpus());
			try {
				index.compute(false);
				List<CQLQuery> queries = Index.createQueries(index.getAllLines());
				cooc.setQueries1(queries);
				cooc.setQueries2(queries);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (element instanceof Lexicon lex) {
			cooc = new QueryCooccurrences(lex.getCorpus());
			try {
				lex.compute(false);
				List<CQLQuery> queries = Index.createQueries(lex.getAllLines());
				cooc.setQueries1(queries);
				cooc.setQueries2(queries);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		TXMEditor.openEditor(cooc, QueryCooccurrencesEditor.class.getCanonicalName());
		
		return cooc;
	}
}
