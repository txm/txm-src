// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.querycooccurrences.rcp;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.FrameworkUtil;
import org.txm.querycooccurrences.rcp.functions.QueryCooccurrences;
import org.txm.rcp.adapters.TXMResultAdapter;
import org.txm.rcp.adapters.TXMResultAdapterFactory;

/**
 * A factory for creating Adapter objects.
 * 
 * @author mdecorde
 */
public class QueryCoocAdapterFactory extends TXMResultAdapterFactory {


	public static final ImageDescriptor ICON = AbstractUIPlugin.imageDescriptorFromPlugin(FrameworkUtil.getBundle(QueryCoocAdapterFactory.class).getSymbolicName(),
			"platform:/plugin/" + FrameworkUtil.getBundle(QueryCoocAdapterFactory.class).getSymbolicName() + "/icons/functions/CooccurrencesP.png"); //$NON-NLS-1$ //$NON-NLS-2$

	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {

		if (this.canAdapt(adapterType) && adaptableObject instanceof QueryCooccurrences) {
			return new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {

					return ICON;
				}
			};
		}
		return null;
	}


}
