package org.txm.querycooccurrences.rcp.functions;

import java.io.File;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.functions.ICAComputable;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.lexicaltable.core.statsengine.r.data.LexicalTableImpl;
import org.txm.pca.core.functions.IPCAComputable;
import org.txm.pca.core.functions.PCA;
import org.txm.rcp.IImageKeys;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.statsengine.r.core.RResult;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

public class QueryCooccurrences extends TXMResult implements ICAComputable, IPCAComputable, IAdaptable, RResult {

	/** The WordCloud adapter. */
	private static IWorkbenchAdapter coocMatrixAdapter = new IWorkbenchAdapter() {

		@Override
		public Object[] getChildren(Object o) {
			return new Object[0];
		}

		@Override
		public ImageDescriptor getImageDescriptor(Object object) {
			return IImageKeys.getImageDescriptor(IImageKeys.COOCMATRIX);
		}

		@Override
		public String getLabel(Object o) {
			return ((QueryCooccurrences) o).getName();
		}

		@Override
		public Object getParent(Object o) {
			if (((QueryCooccurrences) o).getPartition() != null) {
				return ((QueryCooccurrences) o).getPartition();
			}
			else {
				return ((QueryCooccurrences) o).getCorpus();
			}
		}
	};

	protected static int noCoocMatrix = 1;

	protected static String prefixR = "CoocMatrix";

	protected int[][] coocs;

	protected CQPCorpus corpus;

	@Parameter(key = "dist", type = Parameter.COMPUTING)
	protected Integer pDistanceMax;

	@Parameter(key = "minCooc", type = Parameter.COMPUTING)
	protected Integer pMinNCooc;

	protected int nEdges = 0;

	protected int nNodes = 0;

	public static final String LEFT = "left";
	public static final String RIGHT = "right";
	public static final String BOTH = "both";
	/**
	 * left, right or both
	 */
	@Parameter(key = "oriented", type = Parameter.COMPUTING)
	protected String pOriented;

	@Parameter(key = "queries1", type = Parameter.COMPUTING)
	protected List<CQLQuery> pQueries1;

	@Parameter(key = "queries2", type = Parameter.COMPUTING)
	protected List<CQLQuery> pQueries2;

	@Parameter(key = "structure", type = Parameter.COMPUTING)
	protected StructuralUnit pStructureLimit;
	
	protected String symbol;

	public QueryCooccurrences(CQPCorpus corpus) {
		super(corpus);
		this.corpus = corpus;
		this.setVisible(true);
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		this.symbol = null;
		monitor.setTask("Querying cooccurrences...");
		monitor.setWorkRemaining(nNodes);
		
		nNodes = pQueries1.size() * pQueries2.size();
		nEdges = 0;
		int todo = nNodes;
		
		ConsoleProgressBar cpb = new ConsoleProgressBar(todo);
		

		coocs = new int[pQueries1.size()][pQueries2.size()];
		for (int i = 0; i < pQueries1.size(); i++) {

			String fixed1 = pQueries1.get(i).fixQuery(null).getQueryString();
			
			// if (oriented) j = 0; // if the graph is not oriented, we don't need to evaluate all queries
			for (int j = 0; j < pQueries2.size(); j++) {
				
				String fixed2 = pQueries2.get(j).fixQuery(null).getQueryString();
				
				String query = null;
				if (LEFT.equals(pOriented)) {
					query = "(" + fixed2 + "[]{0," + pDistanceMax + "}" + fixed1 + ")"+ (pStructureLimit == null?"":" within " + this.pStructureLimit.getName());
				} else if (RIGHT.equals(pOriented)) {
					query = "(" + fixed1 + "[]{0," + pDistanceMax + "}" + fixed2 + ")"+ (pStructureLimit == null?"":" within " + this.pStructureLimit.getName());
				}
				else {
					query =  "(" + fixed1 + "[]{0," + pDistanceMax + "}" + fixed2 + ")|(" + fixed2 + "[]{0," + pDistanceMax + "}" + fixed1 + ")"+ (pStructureLimit == null?"":" within " + this.pStructureLimit.getName());
				}

				Log.finer("ListCooc: query " + query);
				QueryResult result = corpus.query(new CQLQuery(query), "TMP", false);
				coocs[i][j] = result.getNMatch();
				if (coocs[i][j] >= pMinNCooc) {
					nEdges++;
				}
				else {
					coocs[i][j] = 0;
				}
				
				result.drop();
				
				cpb.tick();
				monitor.worked(1);
			}
		}

		
//		if (!pOriented) { // if the graph is not oriented, copy upper triangle values to lower triangle values
//			for (int i = 0; i < pQueries2.size(); i++) {
//				for (int j = 0; j < pQueries1.size(); j++) {
//					coocs[i][j] = coocs[i][j];
//				}
//			}
//		} else { // oriented, we need to compute the reverse queries
//			for (int i = 0; i < pQueries1.size(); i++) {
//
//				String fixed1 = pQueries1.get(i).fixQuery(null).getQueryString();
//				// if (oriented) j = 0; // if the graph is not oriented, we don't need to evaluate all queries
//				for (int j = 0; j < pQueries2.size(); j++) {
//					
//					String fixed2 = pQueries2.get(j).fixQuery(null).getQueryString();
//					
//					String query = "(" + fixed2 + "[]{0," + pDistanceMax + "}" + fixed1 + ")" + (pStructureLimit == null?"":" within " + this.pStructureLimit.getName());
//
//					Log.finer("ListCooc: query " + query);
//					QueryResult result = corpus.query(new CQLQuery(query), "TMP", false);
//					coocs[j][i] = result.getNMatch();
//					if (coocs[i][j] >= pMinNCooc) {
//						nEdges++;
//					}
//					else {
//						coocs[j][i] = 0;
//					}
//					
//					result.drop();
//					
//					cpb.tick();
//					monitor.worked(1);
//				}
//			}
//		}
		
		cpb.done();
		
		return true;
	}

	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		
		if (outfile.getName().endsWith(".graphml")) {
			return toGraphml(outfile);
		} else {
			PrintStream stream = new PrintStream(outfile);
			printCoocs(stream);
			stream.close();
			return true;
		}
	}

	@Override
	public boolean canCompute() {
		return corpus != null 
				&& pQueries1 != null && pQueries1.size() > 0 
				&& pQueries2 != null && pQueries2.size() > 0 ;
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub
	}

	@Override
	public Object getAdapter(Class adapterType) {
		if (adapterType == IWorkbenchAdapter.class)
			return coocMatrixAdapter;
		return null;
	}
	
	public int[][] getCoocs() {
		return coocs;
	}

	@Override
	public CQPCorpus getCorpus() {
		return corpus;
	}

	@Override
	public String getDetails() {
		return getResultType() + " d=" + pDistanceMax + " min=" + pMinNCooc + "\nQ1=\n\t" + StringUtils.join(pQueries1, "\n\t") + "\nQ2=\n\t" + StringUtils.join(pQueries2, "\n\t");
	}

	public int getDist() {
		return pDistanceMax;
	}
	
	@Override
	public String[] getExportTXTExtensions() {
		return new String[] { "*.graphml", "*.tsv" };
	}

	@Override
	public String getName() {
		return "d=" + pDistanceMax + " min=" + pMinNCooc;
	}

	public int getNEdges() {
		return nEdges;
	}
	
	public int getNNodes() {
		return nNodes;
	}

	@Override
	public Partition getPartition() {
		return null;
	}


	public List<CQLQuery> getQueries() {
		return pQueries1;
	}

	public List<CQLQuery> getQueries2() {
		return pQueries2;
	}

	@Override
	public String getResultType() {
		return "Query cooccurrence";
	}
	
	@Override
	public String getSimpleName() {
		return "d=" + pDistanceMax + " min=" + pMinNCooc;
	}
	
	public String getRSymbol() {
		return symbol;
	}

	@Override
	public boolean loadParameters() throws Exception {
		// FIXME: not yet implemented.
		// System.err.println("QueryCooccurrence.loadParameters(): not yet implemented.");
		return true;
	}


	public void printCoocs() {
		printCoocs(System.out);
	}

	public void printCoocs(PrintStream writer) {
		for (int i = 0; i < pQueries1.size(); i++) {
			writer.print(pQueries1.get(i).getName());
			for (int j = 0; j < pQueries2.size(); j++) {
				writer.print("\t" + pQueries2.get(j).getName()+ "\t"+ coocs[i][j]);
			}
			writer.println();
		}
	}
	
	public void printCoocs(PrintWriter writer) {
		for (int i = 0; i < pQueries1.size(); i++) {
			for (int j = 0; j < pQueries2.size(); j++) {
				writer.print(pQueries1.get(i).getName()+"\t" + pQueries2.get(j).getName()+ "\t"+ coocs[i][j]); writer.println();
			}
		}
	}

	@Override
	public boolean saveParameters() throws Exception {
		// FIXME: not yet implemented.
		// System.err.println("QueryCooccurrence.saveParameters(): not yet implemented.");
		return true;
	}

	private void sendToR() throws RWorkspaceException {
		if (symbol != null) return;

		this.symbol = prefixR + (QueryCooccurrences.noCoocMatrix++);
		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		rw.eval("rm(" + symbol + ")");
		rw.addMatrixToWorkspace(symbol, coocs);
		String[] queries = new String[pQueries1.size()];
		int i = 0;
		for (CQLQuery query : pQueries1) {
			queries[i++] = query.getName();
		}
		rw.addVectorToWorkspace("cooclabels", queries);
		queries = new String[pQueries2.size()];
		i = 0;
		for (CQLQuery query : pQueries2) {
			queries[i++] = query.getName();
		}
		rw.addVectorToWorkspace("cooclabels2", queries);
		rw.eval("rownames(" + symbol + ") <- cooclabels");
		rw.eval("colnames(" + symbol + ") <- cooclabels2");
	}

	public void setParameters(
			ArrayList<CQLQuery> queries1, 
			ArrayList<CQLQuery> queries2, 
			int dist, int minCooc, String struct, String oriented) {

		this.pQueries1 = queries1;
		this.pQueries2 = queries2;
		this.pDistanceMax = dist;
		this.pMinNCooc = minCooc;
		this.pOriented = oriented;
		if (struct == null) {
			try {
				this.pStructureLimit = corpus.getTextStructuralUnit();
			} catch (CqiClientException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public CA toCA() throws Exception {
		
		this.compute(false);

		LexicalTable table = this.toLexicalTable();

		CA ca = new CA(table);
		ca.setDoPCA(false);
		return ca;
	}

	public boolean toGraphml(File outfile) throws RWorkspaceException {

//		RWorkspace rw = null;
//		try {
//			rw = RWorkspace.getRWorkspaceInstance();
//			rw.eval("library(\"igraph\")");
//			Log.info("Package 'igraph' installed. Now running CoocMatrix");
//		}
//		catch (Exception e) {
//			Log.printStackTrace(e);
//			Log.warning("The 'igraph' package is not installed: " + e);
//			boolean install = true;
//			if (install) {
//				Log.warning("Installing 'igraph'");
//				try {
//					rw.eval("install.packages(\"igraph\", dependencies=TRUE, repos=\"http://cran.rstudio.com\");");
//				}
//				catch (Exception e2) {
//					Log.severe("Could not install the 'igraph' package");
//					Log.printStackTrace(e2);
//					return false;
//				}
//			}
//			else {
//				System.out.println("Operation canceled by user");
//				return false;
//			}
//		}
//
//		sendToR();
//
//		outfile.delete();
//
//		String orientationMode = "undirected";
//		// if (oriented) orientationMode = "directed";
//
//		rw.eval("library(igraph)");
//		rw.eval("g <- graph.adjacency(" + symbol + ", weighted=T, mode = \"" + orientationMode + "\", diag=FALSE)"); // create graph from matrix
//		rw.eval("V(g)$label <- V(g)$name"); // copy names in labels
//		// rw.eval("g <- set.vertex.attribute(g, \"label\", colnames("+symbol+"))");
//		// rw.eval("g <- set.vertex.attribute(g, \"color\", value=colors)");
//
//		String path = outfile.getAbsolutePath();// .replaceAll("\\\\", "\\\\")
//		path = path.replace('\\', '/');
//		rw.eval("write.graph(g, \"" + path + "\", format = 'graphml')");
//
//		
		
		PrintWriter writer;
		try {
			writer = IOUtils.getWriter(outfile);
			writer.println("""
<?xml version="1.0" encoding="UTF-8"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns"  
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
					""");
			
			writer.println("  <key id=\"f\" for=\"edge\" attr.name=\"frequency\" attr.type=\"integer\"/>");
			writer.println("  <key id=\"s\" for=\"edge\" attr.name=\"score\" attr.type=\"double\"/>");
			writer.println("  <key id=\"query\" for=\"node\" attr.name=\"query\" attr.type=\"string\"/>");
			writer.println("  <key id=\"label\" for=\"node\" attr.name=\"name\" attr.type=\"string\"/>");
			
			writer.println("  <graph id=\"G\" edgedefault=\"undirected\">");
			
			for (int i = 0 ; i < coocs.length ; i++) {
				writer.println("    <node id=\""+pQueries1.get(i).getName().replace('"', '\'')+"\">");// <node id="n0"/>
				writer.println("      <data key=\"query\">"+pQueries1.get(i).getQueryString()+"</data>");// <node id="n0"/>
				writer.println("      <data key=\"label\">"+pQueries1.get(i).getName()+"</data>");// <node id="n0"/>
				writer.println("    </node>");// <node id="n0"/>
			}
			
			for (int j = 0 ; j <coocs[0].length ; j++) {
				writer.println("    <node id=\""+pQueries2.get(j).getName().replace('"', '\'')+"\">");// <node id="n0"/>
				writer.println("      <data key=\"query\">"+pQueries2.get(j).getQueryString()+"</data>");// <node id="n0"/>
				writer.println("      <data key=\"label\">"+pQueries2.get(j).getName()+"</data>");// <node id="n0"/>
				writer.println("    </node>");// <node id="n0"/>
			}
			
			for (int i = 0 ; i < coocs.length ; i++) {
				for (int j = 0 ; j <coocs[i].length ; j++) {
					if (coocs[i][j] > 0) {
						writer.println("    <edge id=\"e"+i+"_"+j+"\" source=\""+pQueries1.get(i).getName().replace('"', '\'')+"\" target=\""+pQueries2.get(j).getName().replace('"', '\'')+"\">");// <node id="n0"/>
						writer.println("      <data key=\"f\">"+coocs[i][j]+"</data>");// 
						writer.println("      <data key=\"s\">0.0</data>");// 
						writer.println("    </edge>");
					}
				}
			}
			
			writer.println("""
  </graph>
</graphml>					
					""");
			writer.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return outfile.exists();
		
	}

	public LexicalTable toLexicalTable() throws Exception {
		sendToR();
		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		rw.eval("LT<-" + this.getRSymbol());
		rw.eval("LT<-LT[rowSums(LT) != 0, ]");
		rw.eval("LT<-LT[, colSums(LT) != 0]");
		rw.eval(this.getRSymbol() + "LT<-LT");
		rw.eval("rm(LT)");
		// TODO: use LexicalTablePreferences fmin
		LexicalTable table = new LexicalTable(corpus);
		table.setUnitProperty(corpus.getProperty("word"));
		table.setData(new LexicalTableImpl(this.getRSymbol() + "LT"));
		return table;
	}
	
	@Override
	public PCA toPCA() throws Exception {
		
		this.compute(false);

		LexicalTable table = this.toLexicalTable();

		PCA ca = new PCA(table);
		return ca;
	}

	@Override
	public String toString() {
		return getName();
	}

	public void setQueries1(List<CQLQuery> queries) {
		
		pQueries1 = queries;
	}

	public void setQueries2(List<CQLQuery> queries) {
		
		pQueries2 = queries;
	}
}
