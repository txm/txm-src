package org.txm.libs.jodconverter;

import java.io.File;

import org.jodconverter.core.office.OfficeException;
import org.jodconverter.core.office.OfficeManager;
import org.jodconverter.core.office.OfficeUtils;
import org.jodconverter.local.JodConverter;
import org.jodconverter.local.office.LocalOfficeManager;
import org.jodconverter.local.office.LocalOfficeManager.Builder;

public class ConvertDocument {
	
	public static void convert(File inputFile, File outputFile) throws OfficeException {
		convert(inputFile, outputFile, null);
	}
	
	public static void convert(File inputFile, File outputFile, String officePath) throws OfficeException {
		
		Builder builder = LocalOfficeManager.builder().install();
		if (officePath != null && officePath.length() > 0) {
			builder.officeHome(officePath);
		}
		OfficeManager officeManager = builder.build();
		try {
			// Start an office process and connect to the started instance (on port 2002).
			officeManager.start();
			// Convert
			JodConverter
			.convert(inputFile)
			.to(outputFile)
			.execute();
		} finally {
			// Stop the office process
			OfficeUtils.stopQuietly(officeManager);
		}
	}
	
	public static void setOfficeHome(String officeHome) {
		
		if (officeHome == null) {
			System.setProperty("office.home", "");
		} else {
			System.setProperty("office.home", officeHome);
			
			Builder builder = LocalOfficeManager.builder().install();
			builder.officeHome(officeHome);
		}
	}
	
	public static void main(String[] args) throws OfficeException {
		
		File inputFile = new File("/home/mdecorde/xml/doc/Formulaire_SFT 2022 - Decorde.docx");
		File outputFile = new File("/home/mdecorde/xml/doc/Formulaire_SFT 2022 - Decorde.odt");
		
		convert(inputFile, outputFile);
	}
}
