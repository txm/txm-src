// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.index.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.core.preferences.TXMPreferences;
import org.txm.index.core.functions.Index;
import org.txm.index.rcp.editors.IndexEditor;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

/**
 * Opens a Index editor.
 * Computes the result if it contains all required parameters.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class ComputeIndex extends BaseAbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkCorpusEngine()) {
			return false;
		}

		Index index = null;

		// From link/"send to command": creating from a prebuilt parameter node path
		//FIXME: SJ: all "Send to" commands have a different node path schema than normal result creation since we do not know the target class in the source handler (the class string is replaced with "_link")  
		String parametersNodePath = event.getParameter(TXMPreferences.RESULT_PARAMETERS_NODE_PATH);
		if (parametersNodePath != null && !parametersNodePath.isEmpty()) {
			index = new Index(parametersNodePath);
		}
		// From view result node
		else {
			Object selection = this.getCorporaViewSelectedObject(event);

			// New editor from corpus
			if (selection instanceof CQPCorpus) {
				index = new Index((CQPCorpus) selection);
			}
			// Reopen an existing result
			else if (selection instanceof Index) {
				index = (Index) selection;
			}
			else {
				return super.logCanNotExecuteCommand(selection);
			}
		}

		return TXMEditor.openEditor(index, IndexEditor.class.getName());
	}

}
