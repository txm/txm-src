// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.index.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.rcp.handlers.BaseAbstractHandler;

// TODO: Auto-generated Javadoc
/**
 * The Class IndexToLexicalTable.
 */
// FIXME: not used?
public class PartitionIndexToLexicalTable extends BaseAbstractHandler {

	/** The window. */
	private IWorkbenchWindow window;

	/** The selection. */
	private IStructuredSelection selection;

	private static Shell shell;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkStatsEngine()) {
			return null;
		}


		// TODO Auto-generated method stub
		window = HandlerUtil.getActiveWorkbenchWindow(event);
		selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		return null;
	}

	/**
	 * Builds the lexical table.
	 *
	 * @param voc the voc
	 * @param from the from
	 * @param to the to
	 * @return true, if successful
	 */
	// FIXME: SJ: temporary commented to avoid cyclic dependencies
	// public boolean buildLexicalTable(Index voc, int from, int to) {
	// LexicalTable table;
	//
	// if (!voc.isComputedWithPartition()) {
	//
	// System.out.println(TXMUIMessages.cannotCreateALexicalTableFromAnIndexCreatedWithACorpusMoreThanOneColumnIsNeeded);
	// StatusLine.setMessage(TXMUIMessages.cannotCreateALexicalTableFromAnIndexCreatedWithACorpusMoreThanOneColumnIsNeeded);
	// return false;
	// }
	//
	// System.out.println(LexicalTableUIMessages.openingMargeConfigurationDialog);
	// ArrayList<String> choices = new ArrayList<String>();
	// choices.add(LexicalTableUIMessages.useAllOccurrences);
	// choices.add(LexicalTableUIMessages.userIndexOccurrences);
	// ListSelectionDialog dialog = new ListSelectionDialog(shell,
	// choices, new ArrayContentProvider(), new LabelProvider(),
	// LexicalTableUIMessages.selectWhichMarginsYouWantToUse);
	//
	// int ret = dialog.open();
	// //System.out.println("ret= "+ret);
	// if (ret == Dialog.OK_OPTION) {
	//
	// } else {
	// return false;
	// }
	//
	// try {
	// table = new LexicalTable(voc);
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// org.txm.utils.logger.Log.printStackTrace(e);
	// return false;
	// }
	//
	// try {
	// IWorkbenchWindow window = PlatformUI.getWorkbench()
	// .getActiveWorkbenchWindow();
	// IWorkbenchPage page = window.getActivePage();
	// ___LexicalTableEditorInput editorInput = new ___LexicalTableEditorInput(table);
	// StatusLine.setMessage(LexicalTableUIMessages.ComputeLexicalTable_10);
	// SWTEditorsUtils.openEditor(editorInput, "org.txm.rcp.editors.lexicaltable.LexicalTableEditor"); //$NON-NLS-1$
	// } catch (PartInitException e) {
	// org.txm.utils.logger.Log.printStackTrace(e);
	// }
	//
	// return true;
	// }

}
