// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.index.rcp.handlers;

import java.util.Arrays;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.index.core.functions.Index;
import org.txm.index.core.functions.Lexicon;
import org.txm.index.rcp.editors.LexiconEditor;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.utils.logger.Log;


/**
 * Opens a Lexicon editor.
 * Computes the result if it contains all required parameters.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class ComputeLexicon extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkCorpusEngine()) {
			return false;
		}

		Object selection = this.getCorporaViewSelectedObject(event);

		Lexicon index = null;

		// New Lexicon from Corpus
		if (selection instanceof CQPCorpus) {
			CQPCorpus corpus = (CQPCorpus) selection;
			index = new Lexicon(corpus);
			try {
				index.setProperties(Arrays.asList(corpus.getWordProperty()));
			}
			catch (CqiClientException e) {
				Log.printStackTrace(e);
			}
		}
		// Reopening an existing Lexicon
		else if (selection instanceof Lexicon) {
			index = (Lexicon) selection;
		}
		else {
			super.logCanNotExecuteCommand(selection);
		}

		open(index);

		return null;
	}

	public static void open(Index lexicon) {
		TXMEditor.openEditor(lexicon, LexiconEditor.class.getName());
	}
}
