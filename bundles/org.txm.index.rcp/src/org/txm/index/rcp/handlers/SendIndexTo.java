// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.index.rcp.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.results.TXMResult;
import org.txm.index.core.functions.Index;
import org.txm.index.core.preferences.IndexPreferences;
import org.txm.links.rcp.handlers.SendSelectionToQueryable;
import org.txm.rcp.editors.TXMEditor;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.utils.logger.Log;

/**
 * Sends the selected lines of an index or lexicon to compute an other command.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class SendIndexTo extends SendSelectionToQueryable {

	@Override
	public Query createQuery(ExecutionEvent event, ISelection selection) {
		if (selection instanceof StructuredSelection) {
			StructuredSelection sselection = (StructuredSelection) selection;
			return Index.createQuery(sselection.toList());
		}
		return null;
	}

	@Override
	public List<Query> createQueries(ExecutionEvent event, ISelection selection) {
		if (selection instanceof StructuredSelection) {
			StructuredSelection sselection = (StructuredSelection) selection;
			return Index.createQueries(sselection.toList());
		}
		return null;
	}

	@Override
	public TXMResult getResultParent(ExecutionEvent event, String id) {
		@SuppressWarnings("unchecked")
		TXMEditor<Index> editor = (TXMEditor<Index>) HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();

		TXMResult result = editor.getResult();
		Index index = (Index) result;
		CQPCorpus corpus = CQPCorpus.getFirstParentCorpus(index);

		// TODO enable subcorpus from index query when the hidden subcorpus deletion will be managed or another system implemented (eg new SubCorpus(TXMResult result)
		if (id.contains("Concordance") && index.getDoProjection()) { //$NON-NLS-1$

			ArrayList<IQuery> cqlQueries = index.getQueries();
			if (cqlQueries == null // no query
					|| cqlQueries.size() == 0 // no query
					|| (cqlQueries.size() == 1
							&& (cqlQueries.get(0).getQueryString().equals("[]") || cqlQueries.get(0).getQueryString().equals("\".*\"") || cqlQueries.get(0).getQueryString().equals("\".+\"")))) { // don't bother creating a subcorpus use the parent corpus //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				return corpus;
			}

			if (cqlQueries.size() > 1) {
				//CQPCorpus unionedSubcorpus = null;

				return corpus; // good enough for now but we should union the CQP query results into one subcorpus
			}
			else if (!IndexPreferences.getInstance().getBoolean(IndexPreferences.LINKED_CONCORDANCES)) {
				return corpus;
			}
			else {

				IQuery cqlQuery = cqlQueries.get(0);

				CQPCorpus subcorpus = null;
				try {
					for (Subcorpus c : index.getChildren(Subcorpus.class)) {
						if (c.getQuery().equals(cqlQuery) && !c.isAltered()) {
							subcorpus = c;
							break;
						}
					}
					if (subcorpus == null) {
						//subcorpus = corpus.createSubcorpus(cqlQuery, cqlQuery.getQueryString());
						subcorpus = new Subcorpus(index) {

							@Override
							public boolean isInternalPersistable() { // change the corpus behavior
								return false;
							}
						};
						subcorpus.compute(false); // make it ready
						subcorpus.setUserPersistable(false);
						subcorpus.setVisible(false);
						//index.setUserPersistable(true); // make the index persistable to avoid bugs later
					}
				}
				catch (Exception e) {
					Log.printStackTrace(e);
					return null;
				}

				return subcorpus;
			}
		}

		return corpus;
	}

	@Override
	public boolean isUniqLink() {
		return true;
	}
}
