package org.txm.index.rcp.preferences;

import org.eclipse.ui.IWorkbench;
import org.txm.core.preferences.TXMPreferences;
import org.txm.index.core.preferences.IndexPreferences;
import org.txm.index.rcp.adapters.IndexAdapterFactory;
import org.txm.index.rcp.messages.IndexUIMessages;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.rcp.swt.widget.preferences.IntegerFieldEditor;

/**
 * Index preferences page.
 * 
 * @author sjacquot, mdecorde
 *
 */
public class IndexPreferencesPage extends TXMPreferencePage {

	@Override
	public void init(IWorkbench workbench) {

		this.setPreferenceStore(new TXMPreferenceStore(IndexPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setTitle(IndexUIMessages.index);
		this.setImageDescriptor(IndexAdapterFactory.ICON);
	}

	@Override
	protected void createFieldEditors() {
		
		IntegerFieldEditor elementsPerPage = new IntegerFieldEditor(TXMPreferences.NB_OF_ELEMENTS_PER_PAGE, TXMUIMessages.common_elementsPerPage, this.getFieldEditorParent());
		elementsPerPage.setToolTipText(TXMUIMessages.common_numberOfElementsToDisplayPerPage);
		this.addField(elementsPerPage);
		
//		BooleanFieldEditor linkfield = new BooleanFieldEditor(IndexPreferences.LINKED_CONCORDANCES, IndexUIMessages.linkToChildrenConcordances, this.getFieldEditorParent());
//		linkfield.setToolTipText(IndexUIMessages.linksToConcordancesCreateAConcordanceChild);
//		this.addField(linkfield);
	}
}
