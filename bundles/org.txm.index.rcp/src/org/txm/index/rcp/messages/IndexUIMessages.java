package org.txm.index.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Index UI messages.
 * 
 * @author sjacquot
 *
 */
public class IndexUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.index.rcp.messages.messages"; //$NON-NLS-1$

	public static String infos;

	public static String numberOfLinesPerResultPage;
	public static String linksToConcordancesCreateAConcordanceChild;
	public static String linkToChildrenConcordances;

	public static String index;

	public static String indexColonQueryIsEmpty;

	public static String failedToComputeIndex;

	public static String indexOfInfP0SupWithPropertyColonP1InTheCorpusColonP2;

	public static String indexOfInfP0SupWithPropertyColonP1InThePartitionColonP2;

	public static String couldntInitializeIndexTable;

	public static String openingTheIndexResults;



	public static String computingIndex;


	public static String openingIndexResults;



	public static String tP0vP1fminP2fmaxP3;

	public static String WarningTheNumberOfColumnsExceedsP0P1TheTableWasCut;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, IndexUIMessages.class);
	}

	private IndexUIMessages() {
	}
}
