// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.index.rcp.editors;

import java.util.List;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableColumn;
import org.txm.index.core.functions.Line;
import org.txm.index.core.functions.PartitionIndex;
import org.txm.index.rcp.messages.IndexUIMessages;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.preferences.RCPPreferences;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Part;
import org.txm.utils.logger.Log;

/**
 * Partition index editor.
 * 
 * 
 * @author mdecorde
 */
public class PartitionIndexEditor extends IndexEditor<PartitionIndex> {

	/**
	 * Computes index.
	 */
	@Override
	public void updateEditorFromResult(final boolean update) {

		// rmv/creates parts columns if needed
		List<Part> parts = index.getPartition().getParts();
		List<String> partnames = index.getPartition().getPartNames();
		if (partnames.size() > 1) {
			int c = rightViewer.getTable().getColumnCount();
			if (c < partnames.size() + 1) {

				int MAX_NUMBER_OF_COLUMNS = RCPPreferences.getInstance().getInt(RCPPreferences.MAX_NUMBER_OF_COLUMNS);

				for (TableColumn col : rightViewer.getTable().getColumns()) {
					col.dispose();
				}
				
				for (int i = 0; i < partnames.size(); i++) {

					if (i >= MAX_NUMBER_OF_COLUMNS) {
						Log.warning(NLS.bind(IndexUIMessages.WarningTheNumberOfColumnsExceedsP0P1TheTableWasCut, MAX_NUMBER_OF_COLUMNS, partnames.size()));
						break;
					}

					TableViewerColumn partColumn = new TableViewerColumn(rightViewer, SWT.RIGHT);
					try {
						partColumn.getColumn().setText("t=" + parts.get(i).getSize() + TableUtils.headersNewLine + partnames.get(i)); //$NON-NLS-1$
					}
					catch (CqiClientException e1) {
						org.txm.utils.logger.Log.printStackTrace(e1);
					}
					partColumn.getColumn().setToolTipText(partnames.get(i));
					partColumn.getColumn().setWidth(100);
					partColumn.getColumn().addSelectionListener(sortSelectionListener);
					final int idx = i;
					partColumn.setLabelProvider(new ColumnLabelProvider() {

						public String getText(Object elem) {
							return Integer.toString(((Line) elem).getFrequency(idx));
						}
					});
				}
			}
		}

		super.updateEditorFromResult(update);
	}

}
