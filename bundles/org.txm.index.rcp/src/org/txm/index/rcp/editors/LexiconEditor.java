// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.index.rcp.editors;

import org.eclipse.swt.layout.GridData;
import org.txm.index.core.functions.Lexicon;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;

/**
 * Extends the index editor to:
 * - set the number of property to 1
 * - hide the query field
 * 
 * @author mdecorde
 */
public class LexiconEditor extends IndexEditor<Lexicon> {

	@Override
	public void _createPartControl() throws CqiClientException {

		super._createPartControl();

		this.propertiesSelector.setMaxPropertyNumber(1);

		this.queryWidget.setText("[]"); //$NON-NLS-1$
		this.queryWidget.setEnabled(false);
		this.queryWidget.setVisible(false);
		this.queryWidget.setSize(0, 0);
		this.queryWidget.setLayoutData(new GridData(0, 0));
		this.queryLabel.setVisible(false);
		this.queryLabel.dispose();
		
		if (this.queriesFocusComposite != null) {
			queriesFocusComposite.dispose();
		}

		// no query field -> no need to expand the zone
		this.getMainParametersComposite().setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
	}
}
