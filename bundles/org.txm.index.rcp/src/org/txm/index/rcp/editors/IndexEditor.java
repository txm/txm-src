// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.index.rcp.editors;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TableColumn;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.index.core.functions.Index;
import org.txm.index.core.functions.Lexicon;
import org.txm.index.core.functions.Line;
import org.txm.index.rcp.messages.IndexUIMessages;
import org.txm.rcp.StatusLine;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TableKeyListener;
import org.txm.rcp.editors.TableResultEditor;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.editors.listeners.ComputeKeyListener;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.jface.TableViewerSeparatorColumn;
import org.txm.rcp.swt.widget.AssistedChoiceQueryWidget;
import org.txm.rcp.swt.widget.MultipleQueriesComposite;
import org.txm.rcp.swt.widget.NavigationWidget;
import org.txm.rcp.swt.widget.PropertiesSelector;
import org.txm.rcp.swt.widget.TXMParameterSpinner;
import org.txm.rcp.swt.widget.ThresholdsGroup;
import org.txm.rcp.utils.IOClipboard;
import org.txm.rcp.views.QueriesView;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.statsengine.r.rcp.views.RVariablesView;
import org.txm.utils.logger.Log;

/**
 * Index editor.
 *
 * @author mdecorde
 */
public class IndexEditor<I extends Index> extends TXMEditor<I> implements TableResultEditor {

	/** The index. */
	protected I index;

	// params
	/** The query label. */
	protected Label queryLabel;

	// infos
	/** The navigation area. */
	protected NavigationWidget navigationArea;

	/** The l fmin info. */
	protected Button infoLine;

	/** The line table viewer. */
	protected TableViewer leftViewer;

	/** The line table viewer. */
	protected TableViewer rightViewer;

	/** The n column. */
	protected TableViewerColumn nColumn;

	/** The unit column. */
	protected TableViewerColumn unitColumn;

	/** The freq column. */
	protected TableViewerColumn freqColumn;

	/** The title. */
	String title;

	/** The query widget. */
	//@Parameter(key = TXMPreferences.QUERY)
	protected AssistedChoiceQueryWidget queryWidget;

	/**
	 * Queries alimented with the querywidget.
	 */
	@Parameter(key = TXMPreferences.QUERIES)
	protected List<IQuery> queryList;

	/**
	 * Minimum frequency filtering spinner.
	 */
	@Parameter(key = TXMPreferences.F_MIN)
	protected Spinner fMinSpinner;

	/**
	 * Maximum frequency filtering spinner.
	 */
	@Parameter(key = TXMPreferences.F_MAX)
	protected Spinner fMaxSpinner;

	/**
	 * Maximum number of lines filtering.
	 */
	@Parameter(key = TXMPreferences.V_MAX)
	protected Spinner vMaxSpinner;


	/** The N ligne p page spinner. */
	@Parameter(key = TXMPreferences.NB_OF_ELEMENTS_PER_PAGE)
	protected Spinner nLinesPerPageSpinner;

	/**
	 * If selected the query result selection is projected on the properties
	 */
	@Parameter(key = "projection")
	protected Button doProjectionButton;

	/**
	 * Word properties selector.
	 */
	@Parameter(key = TXMPreferences.UNIT_PROPERTIES)
	protected PropertiesSelector<WordProperty> propertiesSelector;

	protected SelectionListener sortSelectionListener;

	MultipleQueriesComposite queriesFocusComposite;

	private TableKeyListener tableKeyListener;

	/**
	 * Creates the part control.
	 *
	 * @throws CqiClientException
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void _createPartControl() throws CqiClientException {

		this.index = this.getResult();

		// Computing listeners
		ComputeSelectionListener computeSelectionListener = new ComputeSelectionListener(this);
		ComputeKeyListener computeKeyListener = new ComputeKeyListener(this);

		Composite extendedParametersComposite = this.getExtendedParametersGroup();
		// extendedParametersComposite.setLayout(new FormLayout());

		// info&navigation panel
		final GLComposite infosArea = this.getBottomToolbar().installGLComposite(IndexUIMessages.infos, 1, false);
		infosArea.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		infosArea.getLayout().numColumns = 5; // nav panel + labels

		// on créé une Query, ici le pivot de la concordance est "[]"
		// Query Area: query itself + view properties
		// Composite queryArea = new Composite(extendedParametersComposite, SWT.NONE);
		//
		// FormData queryLayoutData = new FormData();
		// queryLayoutData.top = new FormAttachment(0);
		// queryLayoutData.left = new FormAttachment(0);
		// queryLayoutData.right = new FormAttachment(100);
		// queryArea.setLayoutData(queryLayoutData);
		//
		// queryArea.setLayout(new GridLayout(4, false));

		// | Query: [ (v)] [Search] |

		// Main parameters
		// Query
		this.getMainParametersComposite().getLayout().numColumns = 5;
		// make the zone expandable to display the longest query field
		this.getMainParametersComposite().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));

		getComputeButton().addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				if (queriesFocusComposite != null && !queriesFocusComposite.isDisposed() && queryWidget != null && !queryWidget.isDisposed()) {
					if (queryWidget.getQueryString().length() > 0 && queriesFocusComposite.onPlusButtonPressed(null, queryWidget.getQuery())) {
						queryWidget.clearQuery();
					}
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// Query
		queryLabel = new Label(this.getMainParametersComposite(), SWT.NONE);
		queryLabel.setText(TXMCoreMessages.common_query);
		queryLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true));

		// [(v)]
		queryWidget = new AssistedChoiceQueryWidget(this.getMainParametersComposite(), SWT.DROP_DOWN, index.getCorpus());
		GridData layoutData = new GridData(GridData.VERTICAL_ALIGN_CENTER);
		layoutData.horizontalAlignment = GridData.FILL;
		layoutData.grabExcessHorizontalSpace = true;
		queryWidget.setLayoutData(layoutData);
		queryWidget.addKeyListener(computeKeyListener);
		queryWidget.getQueryWidget().addModifyListener(computeKeyListener);
		queryWidget.addKeyListener(new ComputeKeyListener(this) {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {

					if (queriesFocusComposite != null && !queriesFocusComposite.isDisposed()) {

						if (queryWidget.getQueryString().length() > 0 && queriesFocusComposite.onPlusButtonPressed(null, queryWidget.getQuery())) {
							super.keyPressed(e); // recompute only if the query has been added
							queryWidget.clearQuery();
						}
						else {
							queryWidget.setText(queryWidget.getQueryString());
						}
					}
					else {
						super.keyPressed(e);
					}
				}
			}
		});

		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER) && !(this.getResult() instanceof Lexicon)) { // ) {

			doProjectionButton = new Button(this.getMainParametersComposite(), SWT.CHECK);
			doProjectionButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));
			doProjectionButton.setText("Projection");
			doProjectionButton.setToolTipText("if checked, the query selection is projected into the selected property values");
			doProjectionButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					propertiesSelector.setEnabled(doProjectionButton.getSelection());
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		}

		// Word properties selector
		propertiesSelector = new PropertiesSelector<>(this.getMainParametersComposite(), SWT.NONE);
		propertiesSelector.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		propertiesSelector.setLayout(new GridLayout(3, false));
		try {
			ArrayList<WordProperty> props = new ArrayList<>(index.getCorpus().getOrderedProperties());
			props.addAll(index.getCorpus().getVirtualProperties());
			propertiesSelector.setProperties(props);
		}
		catch (CqiClientException e) {
			Log.printStackTrace(e);
		}
		// Listener
		propertiesSelector.addSelectionListener(computeSelectionListener);

		// Extended parameters
		this.getExtendedParametersGroup().setLayout(new GridLayout(3, false));
		// Queries
		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER) && getResult() instanceof Index) {
			queriesFocusComposite = new MultipleQueriesComposite(this.getExtendedParametersGroup(), SWT.NONE, this, 100, queryLabel);
			GridData multiQueriesGData = new GridData(GridData.FILL, GridData.FILL, true, true, 3, 1);
			multiQueriesGData.heightHint = multiQueriesGData.minimumHeight = 120;
			queriesFocusComposite.setLayoutData(multiQueriesGData);
		}
		// Filters
		//CustomThresholdsGroup thresholdsGroup = new CustomThresholdsGroup(this.getExtendedParametersGroup(), this, new CustomThresholdsGroup.Configuration("F", 0, 100, 0, true, true), new CustomThresholdsGroup.Configuration("V", 0, 100, 0, false, true));
		ThresholdsGroup thresholdsGroup = new ThresholdsGroup(this.getExtendedParametersGroup(), this, true);
		thresholdsGroup.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));

		this.fMinSpinner = thresholdsGroup.getFMinSpinner();
		this.fMaxSpinner = thresholdsGroup.getFMaxSpinner();
		this.vMaxSpinner = thresholdsGroup.getVMaxSpinner();

		CQPCorpus corpus = getResult().getFirstParent(MainCorpus.class);
		if (corpus != null) {
			this.vMaxSpinner.setMaximum(corpus.getSize());
			this.fMaxSpinner.setMaximum(corpus.getSize());
			this.fMinSpinner.setMaximum(corpus.getSize());
		}

		// FIXME: SJ: we should create a widget class for the number of elements per page
		// Number of elements per page
		TXMParameterSpinner numberOfResultsPerPage = new TXMParameterSpinner(this.getExtendedParametersGroup(), this, TXMUIMessages.common_elementsPerPage, TXMUIMessages.common_numberOfElementsToDisplayPerPage);
		numberOfResultsPerPage.getControl().setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false));
		this.nLinesPerPageSpinner = numberOfResultsPerPage.getControl();
		this.nLinesPerPageSpinner.setMinimum(0);
		this.nLinesPerPageSpinner.setMaximum(99999);
		this.nLinesPerPageSpinner.setIncrement(1);
		this.nLinesPerPageSpinner.setPageIncrement(100);
		
		// separator
		new Label(infosArea, SWT.NONE).setText("\t"); //$NON-NLS-1$

		infoLine = new Button(infosArea, SWT.PUSH|SWT.FLAT);
		infoLine.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				IOClipboard.write(infoLine.getText());
				Log.info("Copied text: "+infoLine.getText());
			}
		});
		infoLine.setText(""); //$NON-NLS-1$

		new Label(infosArea, SWT.NONE).setText("\t");
		
		navigationArea = new NavigationWidget(infosArea, SWT.NONE);
		navigationArea.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, false));

		navigationArea.addFirstListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				index.setTopLine(0);
				index.setNLinesPerPage(nLinesPerPageSpinner.getSelection());
				fillResultArea();
				leftViewer.getTable().select(0);
				leftViewer.getTable().showSelection();
			}
		});
		navigationArea.addLastListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				int i = (index.getV() / nLinesPerPageSpinner.getSelection());
				int top = i * nLinesPerPageSpinner.getSelection();

				index.setTopLine(top);
				fillResultArea();

				leftViewer.getTable().select(0);
				leftViewer.getTable().showSelection();
			}
		});
		navigationArea.addNextListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				index.setTopLine(index.getTopIndex() + index.getNLinesPerPage());
				fillResultArea();
				leftViewer.getTable().select(0);
				leftViewer.getTable().showSelection();
			}
		});
		navigationArea.addPreviousListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				int top = index.getTopIndex() - index.getNLinesPerPage();
				if (top < 0) {
					top = 0;
				}
				index.setTopLine(top);
				fillResultArea();
				leftViewer.getTable().select(0);
				leftViewer.getTable().showSelection();
			}
		});


		// Result area
		GLComposite resultArea = this.getResultArea();
		resultArea.getLayout().numColumns = 2;
		//		Composite formArea = new Composite(resultArea, SWT.NONE);
		//		formArea.setLayout(new FormLayout());
		//		formArea.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		//		Sash sash = new Sash(formArea, SWT.VERTICAL);
		//		sash.addSelectionListener(new SelectionAdapter() {
		//			
		//			@Override
		//			public void widgetSelected(SelectionEvent event) {
		//				
		//				((FormData) sash.getLayoutData()).left = new FormAttachment(0, event.x);
		//				sash.getParent().layout();
		//			}
		//		});
		//
		//		//SashForm sash = new SashForm(formArea, SWT.HORIZONTAL);
		//		sash.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, true));

		leftViewer = new TableViewer(resultArea, SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER);
		rightViewer = new TableViewer(resultArea, SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER);

		leftViewer.getTable().setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, true));
		rightViewer.getTable().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		// set Layout datas
		//				FormData data = new FormData();
		//				data.top = new FormAttachment(0);
		//				data.bottom = new FormAttachment(100);
		//				data.left = new FormAttachment(0);
		//				data.right = new FormAttachment(sash, 0);
		//				leftViewer.getTable().setLayoutData(data);
		//				
		//				FormData data2 = new FormData();
		//				data2.top = new FormAttachment(0, 0);
		//				data2.bottom = new FormAttachment(100, 0);
		//				data2.left = new FormAttachment(10);
		//				sash.setLayoutData(data2);
		//				
		//				FormData data3 = new FormData();
		//				data3.top = new FormAttachment(0);
		//				data3.bottom = new FormAttachment(100);
		//				data3.left = new FormAttachment(sash, 0);
		//				data3.right = new FormAttachment(100);
		//				rightViewer.getTable().setLayoutData(data3);

		//	sash.setWeights(1,0);

		leftViewer.getTable().setLinesVisible(true);
		leftViewer.getTable().setHeaderVisible(true);
		rightViewer.getTable().setLinesVisible(true);
		rightViewer.getTable().setHeaderVisible(true);

		tableKeyListener = TableUtils.installCopyAndSearchKeyEvents(leftViewer, rightViewer);
		tableKeyListener.installSearchGroup(this);
		tableKeyListener.installMenuContributions(this);
		TableUtils.installColumnResizeMouseWheelEvents(leftViewer, true, false);
		TableUtils.installColumnResizeMouseWheelEvents(rightViewer, true, false);
		TableUtils.installTooltipsOnSelectionChangedEvent(leftViewer, rightViewer);
		TableUtils.installVerticalSynchronisation(leftViewer, rightViewer);

		leftViewer.setContentProvider(new LineContentProvider());
		leftViewer.setInput(new ArrayList<Property>());
		rightViewer.setContentProvider(new LineContentProvider());
		rightViewer.setInput(new ArrayList<Property>());

		nColumn = new TableViewerColumn(leftViewer, SWT.LEFT);
		nColumn.getColumn().setText(""); //$NON-NLS-1$
		nColumn.getColumn().pack();
		nColumn.setLabelProvider(new CellLabelProvider() {

			@Override
			public void update(ViewerCell cell) {
			}
		});

		sortSelectionListener = new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				if (e.widget == null) return;
				if (!(e.widget instanceof TableColumn selectedCol)) return;

				TableViewer sortedTableViewer = null;
				TableColumn sortedCol = null;
				for (TableColumn col : leftViewer.getTable().getColumns()) {
					if (selectedCol == col) {
						sortedTableViewer = leftViewer;
						break;
					}
				}
				if (!rightViewer.getTable().isDisposed()) {
					for (TableColumn col : rightViewer.getTable().getColumns()) {
						if (selectedCol == col) {
							sortedTableViewer = rightViewer;
							break;
						}
					}
				}
				sortedCol = sortedTableViewer.getTable().getSortColumn();

				// store current selection
				List<Line> sel = leftViewer.getStructuredSelection().toList();
				HashSet<Object> lineNames = new HashSet<>();
				for (Line l : sel) {
					lineNames.add(l.getSignature());
				}

				if (selectedCol.equals(sortedCol)) { // reverse sort
					index.setSortDirection(!index.isReversedSort());
				}
				else {
					index.setSortDirection(selectedCol.equals(freqColumn.getColumn()));
				}

				if (selectedCol.equals(unitColumn.getColumn())) { // change the sort direction
					index.setSortColumn(-1); // sort using line names
				}
				else if (selectedCol.equals(freqColumn.getColumn())) {
					index.setSortColumn(0);
				}
				else {
					index.setSortColumn(sortedTableViewer.getTable().indexOf(selectedCol) + 1);
				}

				index.sortLines();
				index.saveParameters();

				sortedTableViewer.getTable().setSortColumn(selectedCol);
				if (index.isReversedSort()) {
					sortedTableViewer.getTable().setSortDirection(SWT.DOWN);
				}
				else {
					sortedTableViewer.getTable().setSortDirection(SWT.UP);
				}

				if (leftViewer != sortedTableViewer) {
					leftViewer.getTable().setSortColumn(null);
					leftViewer.getTable().setSortDirection(SWT.NONE);
				}
				if (!rightViewer.getTable().isDisposed() && rightViewer != sortedTableViewer) {
					rightViewer.getTable().setSortColumn(null);
					rightViewer.getTable().setSortDirection(SWT.NONE);
				}

				StatusLine.setMessage(TXMUIMessages.sortDone);
				fillResultArea();

				//restore selection
				if (lineNames.size() > 0) {
					List<Line> newLines = (List<Line>) leftViewer.getInput();
					List<Line> newSelection = new ArrayList<Line>();
					for (Line l : newLines) {
						if (lineNames.contains(l.getSignature())) newSelection.add(l);
						if (lineNames.size() == 0) break;
					}
					leftViewer.setSelection(new StructuredSelection(newSelection));
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};

		unitColumn = new TableViewerColumn(leftViewer, SWT.LEFT);

		unitColumn.getColumn().setText(TableUtils.headersNewLine + TXMCoreMessages.common_units);
		unitColumn.getColumn().setToolTipText(TXMCoreMessages.common_units);
		unitColumn.getColumn().setWidth(300);
		unitColumn.getColumn().addSelectionListener(sortSelectionListener);
		unitColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object elem) {
				return ((Line) elem).toString();
			}
		});


		freqColumn = new TableViewerColumn(leftViewer, SWT.RIGHT);
		freqColumn.getColumn().setText(TXMCoreMessages.common_frequency);
		freqColumn.getColumn().setToolTipText(TXMCoreMessages.common_frequency);
		freqColumn.getColumn().setWidth(100);
		freqColumn.getColumn().addSelectionListener(sortSelectionListener);
		freqColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object elem) {
				return Integer.toString(((Line) elem).getFrequency());
			}
		});

		TableViewerSeparatorColumn.newSeparator(leftViewer);

		extendedParametersComposite.pack();

		// Add double click, "Send to" commands
		TableUtils.addDoubleClickCommandListener(leftViewer.getTable(), 1, "org.txm.concordance.rcp.handlers.ComputeConcordance"); //$NON-NLS-1$
		TableUtils.addDoubleClickCommandListener(leftViewer.getTable(), 2, Integer.MAX_VALUE, "org.txm.progression.rcp.handlers.ComputeProgression"); //$NON-NLS-1$
		TableUtils.addDoubleClickCommandListener(rightViewer.getTable(), 1, Integer.MAX_VALUE, "org.txm.progression.rcp.handlers.ComputeProgression"); //$NON-NLS-1$

		// Register the context menu
		TXMEditor.initContextMenu(this.leftViewer.getTable(), this.getSite(), this.leftViewer);
		TXMEditor.initContextMenu(this.rightViewer.getTable(), this.getSite(), this.rightViewer);
		
		TableUtils.packColumns(this.leftViewer);
		TableUtils.packColumns(this.rightViewer);
		setFocus();
	}

	@Override
	public void updateResultFromEditor() {
		// create query list from widget
		if (queriesFocusComposite != null && !queriesFocusComposite.isDisposed()) {
			queryList = queriesFocusComposite.getQueries();
		}
		else {
			queryList = new ArrayList<IQuery>();
			queryList.add(queryWidget.getQuery());
		}
	}

	/**
	 * Computes index.
	 */
	@Override
	public void updateEditorFromResult(final boolean update) {
		if (!rightViewer.getTable().isDisposed() && rightViewer.getTable().getColumns().length > 0) {
			String s = rightViewer.getTable().getColumns()[rightViewer.getTable().getColumnCount() - 1].getText();
			if (s.length() > 0) { // add a last separator column for presentation if not already done
				TableViewerSeparatorColumn.newSeparator(rightViewer);
			}
		}
		else {
			rightViewer.getTable().dispose();
			((GridData) leftViewer.getTable().getLayoutData()).grabExcessHorizontalSpace = true;
		}

		// initialize query fields
		if (queryList != null) {

			if (queriesFocusComposite != null && !queriesFocusComposite.isDisposed()) {
				queriesFocusComposite.clearQueries();

				for (IQuery q : queryList) {
					queriesFocusComposite.addFocusQueryField(q);
				}
			}
			else {
				if (queryList != null && queryList.size() > 0) {
					IQuery q = queryList.get(0);

					queryWidget.setSearchEngine(q.getSearchEngine());
					queryWidget.setQueryName(q.getName());
					queryWidget.setText(q.getQueryString());
				}
			}
		}
		QueriesView.refresh();
		RVariablesView.refresh();

		if (!queryWidget.isDisposed()) {
			queryWidget.memorize();
		}


		if (getResult().getSortColumn() == -1) {
			leftViewer.getTable().setSortColumn(unitColumn.getColumn());
			leftViewer.getTable().setSortDirection(getResult().isReversedSort() ? SWT.DOWN : SWT.UP);
			if (!rightViewer.getTable().isDisposed()) {
				rightViewer.getTable().setSortColumn(null);
				rightViewer.getTable().setSortDirection(SWT.NONE);
			}
		}
		else if (getResult().getSortColumn() == 0) {
			leftViewer.getTable().setSortColumn(freqColumn.getColumn());
			leftViewer.getTable().setSortDirection(getResult().isReversedSort() ? SWT.DOWN : SWT.UP);
			if (!rightViewer.getTable().isDisposed()) {
				rightViewer.getTable().setSortColumn(null);
				rightViewer.getTable().setSortDirection(SWT.NONE);
			}
		}
		else {
			if (!rightViewer.getTable().isDisposed()) {
				rightViewer.getTable().setSortColumn(rightViewer.getTable().getColumn(getResult().getSortColumn() + 2));
				rightViewer.getTable().setSortDirection(getResult().isReversedSort() ? SWT.DOWN : SWT.UP);
			}
			leftViewer.getTable().setSortColumn(null);
			leftViewer.getTable().setSortDirection(SWT.NONE);
		}

		fillResultArea(); // refresh input and columns widths

		leftViewer.getTable().setFocus();
	}

	/**
	 * Fill result area using the index top and index number of results per page parameters.
	 *
	 */
	public void fillResultArea() {
		// System.out.println("call fill from "+from+" to "+to);
		int from = index.getTopIndex();

		int to = from + index.getNLinesPerPage();
		to = Math.min(to, index.getV());

		List<Line> lines = null;
		if (index.getV() > 0) {
			lines = index.getLines(from, to);
		}
		else {
			lines = new ArrayList<>();
		}

		navigationArea.setInfoLineText(Integer.toString(from + 1), NLS.bind("{0} / {1}", to, index.getV())); //$NON-NLS-1$
		navigationArea.setPreviousEnabled(from > 0);
		navigationArea.setFirstEnabled(from > 0);
		navigationArea.setNextEnabled(to < index.getV());
		navigationArea.setLastEnabled(to < index.getV());

		infoLine.setText(TXMCoreMessages.bind(IndexUIMessages.tP0vP1fminP2fmaxP3, index.getT(), index.getV(), index.getFmin(), index.getFmax()));
		infoLine.getParent().layout();

		String unitColumnHeader = ""; //$NON-NLS-1$
		for (Property p : index.getProperties()) {
			unitColumnHeader += p.getName() + index.getPropertySeparator();
		}
		if (unitColumnHeader.length() > 0) {
			unitColumnHeader = unitColumnHeader.substring(0, unitColumnHeader.length() - 1);
		}
		if (getResult().getParent() instanceof CQPCorpus) {
			unitColumn.getColumn().setText(unitColumnHeader);
			freqColumn.getColumn().setText("F"); //$NON-NLS-1$
		} else {
			unitColumn.getColumn().setText(TableUtils.headersNewLine + unitColumnHeader);
			freqColumn.getColumn().setText("t=" + index.getT() + TableUtils.headersNewLine + "F"); //$NON-NLS-1$
		}
		leftViewer.setInput(lines);

		if (from == 0) {
			leftViewer.getTable().setTopIndex(0);
		}

		// Refresh and pack the columns
		TableUtils.packColumns(leftViewer, true, true);

		if (!rightViewer.getTable().isDisposed()) {
			rightViewer.setInput(lines);
			TableUtils.packColumns(rightViewer, true, true);
		}

		leftViewer.getTable().getParent().layout();

		//		if (rightViewer.getTable().getColumnCount() > 0) {
		//			SashForm sash = ((SashForm)leftViewer.getTable().getParent());
		//			// total width = 400 ;
		//			int X = this.getResultArea().computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
		//			if (X > 0) {
		//				int lx = leftViewer.getTable().getSize().x;
		//				int rx = rightViewer.getTable().getSize().x;
		//				System.out.println("X="+X);
		//				System.out.println("lx="+lx);
		//				System.out.println("rx="+rx);
		//				sash.setWeights(100 * lx / X, 100 * (X-rx) / X);
		//			} else {
		//				sash.setWeights(leftViewer.getTable().getColumnCount(), rightViewer.getTable().getColumnCount());
		//			}
		//		}
	}

	/**
	 * Sets the focus.
	 *
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void _setFocus() {
		if (this.queryWidget != null && !this.queryWidget.isDisposed()) {
			this.queryWidget.setFocus();
		}
		StatusLine.setMessage(IndexUIMessages.openingTheIndexResults);
	}

	/**
	 * Gets the corpus of the index.
	 *
	 * @return the corpus
	 */
	public CQPCorpus getCorpus() {
		return index.getCorpus();
	}

	/**
	 * Sets the focus.
	 *
	 * @param query the new focus
	 */
	public void setFocus(String query) {
		this.queryWidget.setText(query);
	}

	/**
	 * Gets the query of the query widget.
	 * 
	 * @return
	 */
	public IQuery getQuery() {
		return queryWidget.getQuery();
	}

	public TableViewer getLeftTableViewer() {
		return leftViewer;
	}

	public void setQuery(String query) {
		this.queryWidget.setText(query);
	}

	@Override
	public TableViewer[] getTableViewers() {
		return new TableViewer[] { leftViewer, rightViewer };
	}

	@Override
	public TableKeyListener getTableKeyListener() {
		return tableKeyListener;
	}
}
