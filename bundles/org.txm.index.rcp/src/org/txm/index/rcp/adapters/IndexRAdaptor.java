package org.txm.index.rcp.adapters;

import java.util.ArrayList;
import java.util.List;

import org.txm.index.core.functions.Index;
import org.txm.index.core.functions.Line;
import org.txm.index.core.functions.PartitionIndex;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.statsengine.r.rcp.handlers.RAdaptor;

/**
 * Create a R object from an Index
 * 
 * The class stores the symbols and sended results in 2 static objects : symbols and sended
 * 
 * @author mdecorde
 *
 */
public class IndexRAdaptor extends RAdaptor {


	/** The novoc. */
	protected static int no_index = 1;

	/** The prefix r. */
	protected static String prefixR = "Index"; //$NON-NLS-1$

	/**
	 * @return the R symbol
	 * 
	 * @throws RWorkspaceException the r workspace exception
	 */
	public String sendToR(PartitionIndex index) throws RWorkspaceException {

		if (sended.containsKey(index)) {
			return sended.get(index);
		}

		String symbol = prefixR + no_index;

		ArrayList<String> colnames = new ArrayList<String>();

		colnames.add("F"); //$NON-NLS-1$
		List<String> partnames = index.getPartNames();
		if (partnames.size() > 1)
			for (int j = 0; j < partnames.size(); j++)
			colnames.add(partnames.get(j));

		// System.out.println("cols: "+colnames);

		List<Line> lines = index.getAllLines();
		String[] keywords = new String[lines.size()];

		int[] freq = new int[lines.size()];
		ArrayList<int[]> partfreqs = new ArrayList<int[]>(partnames.size());
		if (partnames.size() > 1)
			for (int j = 0; j < partnames.size(); j++)
			partfreqs.add(new int[lines.size()]);

		for (int i = 0; i < lines.size(); i++) {
			Line ligne = lines.get(i);
			freq[i] = ligne.getFrequency();
			keywords[i] = ligne.toString();
			if (partnames.size() > 1)
				for (int j = 0; j < partnames.size(); j++) {
					partfreqs.get(j)[i] = ligne.getFrequency(j);
				}

		}

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		if (partnames.size() > 1)
			for (int j = 0; j < partnames.size(); j++)
			rw.addVectorToWorkspace("vocpartfreqs" + j, partfreqs.get(j)); //$NON-NLS-1$
		rw.addVectorToWorkspace("vocfreq", freq); //$NON-NLS-1$
		rw.addVectorToWorkspace("vockeywords", keywords); //$NON-NLS-1$
		rw.addVectorToWorkspace("voccolnames", colnames.toArray(new String[colnames.size()])); //$NON-NLS-1$

		int ncol = 1;
		if (partnames.size() > 1)
			ncol += partnames.size();

		int nrow = lines.size();
		String partscmd = ""; //$NON-NLS-1$
		if (partnames.size() > 1)
			for (int j = 0; j < partnames.size(); j++)
			partscmd += ", vocpartfreqs" + j; //$NON-NLS-1$

		rw.eval(symbol + "<- matrix(data = c(vocfreq" + partscmd + "), nrow = " + nrow + ", ncol = " + ncol + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		rw.eval("colnames(" + symbol + " ) <- voccolnames"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval("rownames(" + symbol + " ) <- vockeywords"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval(symbol + "<- list(data=" + symbol + ")"); //$NON-NLS-1$ //$NON-NLS-2$

		no_index++;

		sended.put(index, symbol);
		symbols.put(symbol, index);

		return symbol;
	}

	/**
	 * @return the R symbol
	 * 
	 * @throws RWorkspaceException the r workspace exception
	 */
	public String sendToR(Index index) throws RWorkspaceException {

		if (sended.containsKey(index)) {
			return sended.get(index);
		}

		String symbol = prefixR + no_index;

		ArrayList<String> colnames = new ArrayList<String>();

		colnames.add("F"); //$NON-NLS-1$

		// System.out.println("cols: "+colnames);

		List<Line> lines = index.getAllLines();
		String[] keywords = new String[lines.size()];

		int[] freq = new int[lines.size()];

		for (int i = 0; i < lines.size(); i++) {
			Line ligne = lines.get(i);
			freq[i] = ligne.getFrequency();
			keywords[i] = ligne.toString();

		}

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		rw.addVectorToWorkspace("vocfreq", freq); //$NON-NLS-1$
		rw.addVectorToWorkspace("vockeywords", keywords); //$NON-NLS-1$
		rw.addVectorToWorkspace("voccolnames", colnames.toArray(new String[colnames.size()])); //$NON-NLS-1$

		int ncol = 1;

		int nrow = lines.size();
		String partscmd = ""; //$NON-NLS-1$

		rw.eval(symbol + "<- matrix(data = c(vocfreq" + partscmd + "), nrow = " + nrow + ", ncol = " + ncol + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		rw.eval("colnames(" + symbol + " ) <- voccolnames"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval("rownames(" + symbol + " ) <- vockeywords"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval(symbol + "<- list(data=" + symbol + ")"); //$NON-NLS-1$ //$NON-NLS-2$

		no_index++;

		sended.put(index, symbol);
		symbols.put(symbol, index);

		return symbol;
	}
}
