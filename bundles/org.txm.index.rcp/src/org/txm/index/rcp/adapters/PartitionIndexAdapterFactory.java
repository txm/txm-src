// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.index.rcp.adapters;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.FrameworkUtil;
import org.txm.index.core.functions.PartitionIndex;
import org.txm.rcp.adapters.TXMResultAdapter;
import org.txm.rcp.adapters.TXMResultAdapterFactory;


/**
 * Display an Index or Lexicon icon dependaing on the Index used a lexicon to compute
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class PartitionIndexAdapterFactory extends TXMResultAdapterFactory {


	public static final ImageDescriptor ICON = AbstractUIPlugin.imageDescriptorFromPlugin(FrameworkUtil.getBundle(PartitionIndexAdapterFactory.class).getSymbolicName(),
			"platform:/plugin/" + FrameworkUtil.getBundle(PartitionIndexAdapterFactory.class).getSymbolicName() + "/icons/partitionindex.png"); //$NON-NLS-1$ //$NON-NLS-2$

	@Override
	public <T> T getAdapter(Object adaptableObject, Class<T> adapterType) {
		if (this.canAdapt(adapterType) && adaptableObject instanceof PartitionIndex) {
			return adapterType.cast(new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return ICON;
				}
			});
		}
		return null;
	}


}
