                              TXM 0.7.1

                  Copyright © 2010-2013 ENS de Lyon.
     Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
       Lyon 2, University of Franche-Comté, University of Nice
               Sophia Antipolis, University of Paris 3.

The TXM platform is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation,
either version 2 of the License, or (at your option) any
later version.

The TXM platform is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General
Public License along with the TXM platform in a file
named "GNU General Public License.TXT". If not, see
http://www.gnu.org/licenses.

TXM is made of free and open-source software and libraries
having each their own licence :
- Eclipse RCP : http://www.eclipse.org/home/categories/rcp.php
- The IMS Open Corpus Workbench (CWB) : http://cwb.sourceforge.net
- The R Project for Statistical Computing : http://www.r-project.org
