package org.txm;

import java.io.File;

import org.junit.Test;

public class StartToolbox {

	@Test
	public void test() throws Exception {
		String home = System.getProperty("user.home");
		String product_configuration_path = "runtime-rcp.product/.metadata/.plugins/org.eclipse.core.runtime/.settings/org.txm.rcp.prefs";
		File properties = new File(home, product_configuration_path);
		Toolbox.initialize(properties);
	}

}
