package org.txm.xml;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

/**
 * Simple "XPath without attribute selector" Activator activated when the current path matches the xpath
 * 
 * supported operators : / , // , * , node()
 * only the last element attributes can be tested with simple equals test
 * 
 * exemple: //p[name='value']
 * exemple: /text/node()/p[name='value']
 * 
 * @author mdecorde
 *
 */
public class XPathHookActivator<T extends XMLParser> extends HookActivator<T> {
	
	String xpath;
	
	Pattern p;
	
	LinkedHashMap<String, String> attributesTest = null;
	
	
	protected boolean debug = false;
	
	/**
	 */
	public XPathHookActivator(Hook<T> hook, String xpath) {
		super(hook);
		this.xpath = xpath;
		String regex = xpath;
		
		int attrTest = xpath.indexOf("["); // [some tests]
		int lastNodeTest = xpath.lastIndexOf("/");
		
		if (attrTest > 0 && attrTest > lastNodeTest) {
			attributesTest = new LinkedHashMap<>();
			regex = xpath.substring(0, attrTest);
			String tail = xpath.substring(attrTest);
			tail = tail.substring(1, tail.length() - 1);
			if (debug) System.out.println("TAIL=" + tail);
			String tests[] = tail.split("@");
			if (debug) System.out.println("TESTS=" + Arrays.toString(tests));
			for (String test : tests) {
				if (test.length() == 0) continue;
				
				String split2[] = test.split("=", 2); // 0 name 1 "value"
				if (debug) System.out.println("TEST=" + Arrays.toString(split2));
				attributesTest.put(split2[0], split2[1].substring(1, split2[1].length() - 1));
			}
		}
		
		regex = regex.replaceAll("([^.])[*]", "$1[^/]+");
		if (regex.startsWith("//")) regex = "(/[^/]+)*/" + regex.substring(2);
		regex = regex.replace("//", "(/[^/]+)*/");
		regex = regex.replace("node()", "[^/]+");
		
		this.p = Pattern.compile(regex);
		// System.out.println(p);
		// System.out.println(attributesTest);
	}
	
	@Override
	public boolean mustActivate() {
		
		if (xpath == null) return false;
		if (p == null) return false;
		
		if (debug) System.out.println(hook.getCurrentPath() + hook.parentParser.getCurrentAttributes());
		if (attributesTest != null) {
			return p.matcher(hook.getCurrentPath()).matches() && hook.parentParser.getCurrentAttributes().equals(attributesTest);
		}
		else {
			String path = hook.getCurrentPath();
			return p.matcher(path).matches();
		}
	}
	
	@Override
	public void hookWasActivated() {
		// nothing to do
	}
	
	public static void main(String[] args) {
		new XPathHookActivator(null, "//*");
		new XPathHookActivator(null, "//p[@name='value']");
		new XPathHookActivator(null, "//text/node()/p[@name='value']");
		new XPathHookActivator(null, "/TEI/text/node()/p[@name='value']");
		new XPathHookActivator(null, "//text//p[@name='value']");
	}
}
