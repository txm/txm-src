package org.txm.xml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Stack;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.txm.importer.PersonalNamespaceContext;

/**
 * parse a XML files using Stax and allow to set somes hooks activated with their HookActivator
 * 
 * @author mdecorde
 *
 */
public class XMLParser {
	
	/** The input */
	protected URL inputurl;
	
	protected InputStream inputData;
	
	protected XMLInputFactory factory;
	
	protected XMLStreamReader parser;
	
	public static String TXMNS = "http://textometrie.org/1.0";
	
	public static String TXM = "txm";
	
	public static String TEINS = "http://www.tei-c.org/ns/1.0";
	
	public static String TEI = "tei";
	
	protected PersonalNamespaceContext Nscontext = new PersonalNamespaceContext();
	
	protected StringBuilder currentXPath = new StringBuilder("");
	
	protected String localname;
	
	int processingXInclude = 0;
	
	LinkedHashMap<String, Hook<?>> hooks = new LinkedHashMap<>();
	
	private Hook<?> activeHook = null;
	
	public XMLParser(File infile) throws IOException, XMLStreamException {
		this(infile.toURI().toURL());
	}
	
	public XMLParser(URL inputurl) throws IOException, XMLStreamException {
		this.inputurl = inputurl;
		this.inputData = inputurl.openStream();
		this.factory = XMLInputFactory.newInstance();
		this.parser = factory.createXMLStreamReader(inputData);
	}
	
	/**
	 * Helper method to get an attribute value by its name
	 * 
	 * @param name the attribute name
	 * @return the value if any
	 */
	public String getParserAttributeValue(String name) {
		if (name == null) return null;
		
		int c = parser.getAttributeCount();
		for (int i = 0; i < c; i++) {
			if (name.equals(parser.getAttributeLocalName(i))) {
				return parser.getAttributeValue(i);
			}
		}
		
		return null;
	}
	
	protected void before() {
		
	}
	
	/**
	 * 
	 * @param allWentWell true if the process ends well
	 * 
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	protected void after(boolean allWentWell) throws XMLStreamException, IOException {
		if (factory != null) {
			factory = null;
			
			if (parser != null) {
				parser.close();
			}
			if (inputData != null) {
				inputData.close();
			}
			parser = null;
			
			if (parser != null) {
				try {
					parser.close();
				}
				catch (Exception e) {
					System.out.println("parser excep: " + e);
				}
			}
			
			if (inputData != null) {
				try {
					inputData.close();
				}
				catch (Exception e) {
					System.out.println("inputData excep: " + e);
				}
			}
		}
	}
	
	public final static String SLASH = "/";
	
	/**
	 * true if the first start element was parsed
	 */
	protected boolean firstElementParsed = false;
	
	public final boolean process() throws XMLStreamException, IOException {
		
		// if (processingXInclude == 0) {
		before(); // if you need to do something before reading the xml
		// }
		
		firstElementParsed = false;
		
		try {
			for (int event = parser.getEventType(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				
				// preparing process
				if (event == XMLStreamConstants.START_ELEMENT) {
					firstElementParsed = true;
					localname = parser.getLocalName();
					currentXPath.append(SLASH);
					currentXPath.append(localname);
					buildCurrentAttributes(); // for easier access later
				}
				else if (event == XMLStreamConstants.END_ELEMENT) {
					localname = parser.getLocalName();
				}
				
				if (activeHook != null) {
					if (activeHook.mustDeactivate()) {
						activeHook.deactivate();
						activeHook = null;
					}
					else {
						activeHook.processParserEvent(event);
					}
				}
				
				if (activeHook == null && event == XMLStreamConstants.START_ELEMENT) { // only activate when starting an element (for now, thus we cannot process only text nodes)
					for (Hook hook : hooks.values()) {
						if (hook.mustActivate()) {
							if (hook.activate()) {
								activeHook = hook;
								
								hook.processParserEvent(event);
								break;
							}
						}
					}
				}
				
				if (activeHook == null) {
					this.processParserEvent(event);
				}
				
				// post-preparing process
				if (event == XMLStreamConstants.END_ELEMENT) {
					currentXPath.setLength(currentXPath.length() - localname.length() - 1);
					popCurrentAttributes();
				}
			}
		}
		catch (Exception e) {
			System.out.println("Unexpected error while parsing at " + getLocation() + " : " + e);
			org.txm.utils.logger.Log.printStackTrace(e);
			// e.printStackTrace();
			after(false); // if you need to do something before closing the parser();
			return false;
		}
		finally {
			after(true); // if you need to do something before closing the parser();
		}
		
		return true;
	}
	
	protected final void processParserEvent(int event) throws XMLStreamException, IOException {
		switch (event) {
			case XMLStreamConstants.START_DOCUMENT:
				processStartDocument();
				break;
			case XMLStreamConstants.SPACE:
				processSpace();
				break;
			case XMLStreamConstants.NOTATION_DECLARATION:
				processNotationDeclaration();
				break;
			case XMLStreamConstants.NAMESPACE:
				processNamespace();
				break;
			case XMLStreamConstants.START_ELEMENT:
				processStartElement();
				break;
			case XMLStreamConstants.CHARACTERS:
				processCharacters();
				break;
			case XMLStreamConstants.END_ELEMENT:
				processEndElement();
				break;
			case XMLStreamConstants.PROCESSING_INSTRUCTION:
				processProcessingInstruction();
				break;
			case XMLStreamConstants.DTD:
				processDTD();
				break;
			case XMLStreamConstants.CDATA:
				processCDATA();
				break;
			case XMLStreamConstants.COMMENT:
				processComment();
				break;
			case XMLStreamConstants.END_DOCUMENT:
				processEndDocument();
				break;
			case XMLStreamConstants.ENTITY_REFERENCE:
				processEntityReference();
				break;
		}
	}
	
	/**
	 * 
	 * @param line show the line number
	 * @param col show the column number
	 * @param fullPath show the file full path
	 * 
	 * @return
	 */
	public String getLocation(boolean line, boolean col, boolean fullPath) {
		StringBuilder b = new StringBuilder();
		if (parser != null) {
			
			if (line) b.append("Line: " + parser.getLocation().getLineNumber());
			
			if (col) {
				if (line) b.append(" ");
				b.append("Col: " + parser.getLocation().getLineNumber());
			}
			
			if (line || col) b.append(" ");
			
			if (fullPath) {
				b.append("in File:" + this.inputurl.toString());
			}
			else {
				b.append("in File:" + new File(this.inputurl.getFile()).getName());
			}
			
			if (b.length() == 0) return parser.getLocation().toString();
		}
		return b.toString();
	}
	
	public String getLocation() {
		return getLocation(true, true, true);
	}
	
	private Stack<LinkedHashMap<String, String>> attributesStack = new Stack<>();
	
	private final HashMap<String, String> popCurrentAttributes() {
		return attributesStack.pop();
	}
	
	protected final void buildCurrentAttributes() throws XMLStreamException {
		LinkedHashMap<String, String> currentAttributes = new LinkedHashMap<>(); // keep order
		for (int i = 0; i < parser.getAttributeCount(); i++) {
			currentAttributes.put(parser.getAttributeLocalName(i), parser.getAttributeValue(i));
		}
		attributesStack.push(currentAttributes);
	}
	
	public final LinkedHashMap<String, String> getCurrentAttributes() {
		return attributesStack.lastElement();
	}
	
	protected void processStartDocument() throws XMLStreamException {}
	
	protected void processSpace() throws XMLStreamException {}
	
	protected void processNotationDeclaration() throws XMLStreamException {}
	
	protected void processNamespace() throws XMLStreamException {}
	
	protected void processStartElement() throws XMLStreamException, IOException {}
	
	protected void processCharacters() throws XMLStreamException {}
	
	protected void processProcessingInstruction() throws XMLStreamException {}
	
	protected void processDTD() throws XMLStreamException {}
	
	protected void processCDATA() throws XMLStreamException {}
	
	protected void processComment() throws XMLStreamException {}
	
	protected void processEndElement() throws XMLStreamException {}
	
	protected void processEndDocument() throws XMLStreamException {}
	
	protected void processEntityReference() throws XMLStreamException {}
	
	public String getCurrentPath() {
		return currentXPath.toString();
	}
	
	public static void main(String[] args) {
		try {
			File input = new File(System.getProperty("user.home"), "xml/tdm80j/tdm80j.xml");
			if (!(input.exists() && input.canRead())) {
				System.out.println("cannot found $input");
				return;
			}
			XMLParser builder;
			
			builder = new XMLParser(input.toURI().toURL());
			
			// IdentityHook hook1 = new IdentityHook("hook1", new LocalNameHookActivator(null, "p"), builder) {
			//
			// @Override
			// public boolean _activate() {
			// System.out.println("ACTIVATING " + name + " AT " + getLocation());
			// return true;
			// }
			//
			// @Override
			// public boolean deactivate() {
			// System.out.println("DEACTIVATING " + name + " AT " + getLocation());
			// return true;
			// }
			//
			// @Override
			// public void processStartElement() throws XMLStreamException, IOException {
			// super.processStartElement();
			// parentParser.writer.writeAttribute("hook", name);
			// }
			// };
			XPathHookActivator xpathActivator = new XPathHookActivator(null, "//p");
			XPathsHookActivator xpathsActivator = new XPathsHookActivator(null, Arrays.asList("//p", "//p"));
			// IdentityHook hook2 = new IdentityHook("hook2", xpathsActivator, builder) {
			//
			// @Override
			// public boolean _activate() {
			// System.out.println("ACTIVATING " + name + " AT " + getLocation());
			// return true;
			// }
			//
			// @Override
			// public boolean deactivate() {
			// System.out.println("DEACTIVATING " + name + " AT " + getLocation());
			// return true;
			// }
			//
			// @Override
			// public void processStartElement() throws XMLStreamException, IOException {
			// super.processStartElement();
			// parentParser.writer.writeAttribute("hook", name);
			// }
			// };
			
			long time = System.currentTimeMillis();
			if (builder.process()) {
				System.out.println("Time=" + (System.currentTimeMillis() - time));
				System.out.println("XPaths activator has done working ? " + xpathsActivator.hasAllXpathsBeenProcessed());
			}
			else {
				System.out.println("failure !");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @return the actual hooks hash
	 */
	public LinkedHashMap<String, Hook<? extends XMLParser>> getHooks() {
		return hooks;
	}
	
	public void addHook(String name, Hook<? extends XMLParser> hook) {
		hooks.put(name, hook);
	}
}
