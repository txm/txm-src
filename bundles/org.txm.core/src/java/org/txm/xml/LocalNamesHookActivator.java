package org.txm.xml;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Simple Activator activated when the localname matches the localname constructor parameter
 * 
 * @author mdecorde
 *
 */
public class LocalNamesHookActivator<T extends XMLParser> extends HookActivator<T> {
	
	HashSet<String> localnames;
	
	
	/**
	 * @param localnames the element localnames to match
	 */
	public LocalNamesHookActivator(String... localnames) {
		this(null, Arrays.asList(localnames));
	}
	
	/**
	 * @param localnames the element localnames to match
	 */
	public LocalNamesHookActivator(List<String> localnames) {
		this(null, localnames);
	}
	
	/**
	 * @param localnames the element localnames to match
	 */
	public LocalNamesHookActivator(Hook<T> hook, List<String> localnames) {
		super(hook);
		this.localnames = new HashSet<>();
		this.localnames.addAll(localnames);
	}
	
	/**
	 * @param localnames the element localnames to match
	 */
	public LocalNamesHookActivator(Hook<T> hook, String... localnames) {
		this(hook, Arrays.asList(localnames));
	}
	
	@Override
	public boolean mustActivate() {
		if (localnames == null) return false;
		
		return localnames.contains(hook.getLocalName());
	}
	
	@Override
	public void hookWasActivated() {
		// nothing to do
	}
}
