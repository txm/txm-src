package org.txm.xml;

import java.io.IOException;

import javax.xml.stream.XMLStreamException;

/**
 * Simple wrapper to process only the start element, end element and characters Stax events
 * 
 * @author mdecorde
 *
 */
public abstract class SimpleHook<T extends XMLParser> extends Hook<T> {
	
	public SimpleHook(String name, HookActivator<T> activator, T parentParser) throws IOException, XMLStreamException {
		super(name, activator, parentParser);
	}
	
	@Override
	protected void processNamespace() throws XMLStreamException {}
	
	@Override
	protected void processProcessingInstruction() throws XMLStreamException {}
	
	@Override
	protected void processDTD() throws XMLStreamException {}
	
	@Override
	protected void processCDATA() throws XMLStreamException {}
	
	@Override
	protected void processComment() throws XMLStreamException {}
	
	@Override
	protected void processEndDocument() throws XMLStreamException {}
	
	@Override
	protected void processEntityReference() throws XMLStreamException {}
}
