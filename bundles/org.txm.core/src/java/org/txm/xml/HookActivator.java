package org.txm.xml;

/**
 * This class is used by Hook to do the activation test. If no activator is provided no activation are done.
 * 
 * @author mdecorde
 *
 */
public abstract class HookActivator<T extends XMLParser> {
	
	/**
	 * to access to current parsing informations
	 */
	Hook<T> hook;
	
	/**
	 * 
	 * The Hook will be associated in the Hook constructor
	 * 
	 * @param hook if the hook is already defined. you can associated it that way (or call hook.setActivator() and activator.setHook(
	 */
	public HookActivator(Hook<T> hook) {
		this.hook = hook;
		if (hook != null) {
			hook.setActivator(this);
		}
	}
	
	/**
	 * 
	 * @return true if the hook must be activated
	 */
	public abstract boolean mustActivate();
	
	/**
	 * Use this method if you need the activator to be updated when the hook was activated once
	 */
	protected abstract void hookWasActivated();
	
	/**
	 * Set afterward the hook
	 * 
	 * @param hook this activators hook
	 */
	public void setHook(Hook<T> hook) {
		this.hook = hook;
		if (hook.getActivator() != this) {
			hook.setActivator(this);
		}
	}
	
	public Hook<T> getHook() {
		return hook;
	}
}
