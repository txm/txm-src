package org.txm.xml;

import java.io.IOException;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public abstract class Hook<T extends XMLParser> {
	
	protected T parentParser = null;
	
	protected HookActivator<T> activator = null;
	
	protected String name;
	
	private int elements = 0;
	
	protected XMLStreamReader parser;
	
	/**
	 * create a hook parser. When a hook parser is activated, the processXYZ methods are used instead of the parent processXTZ methods.
	 * 
	 * The hook can be activated / deactivated by the parent using the mustActivated mustDeactivated methods
	 * 
	 * register the hok to the XMLProcessor
	 * 
	 * @param parentParser
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	public Hook(String name, HookActivator<T> activator, T parentParser) throws IOException, XMLStreamException {
		this.parentParser = parentParser;
		this.name = name;
		setActivator(activator); // set the activator
		parentParser.addHook(name, this); // register the hook to the XMLProcessor
		this.parser = parentParser.parser;
	}
	
	public void setActivator(HookActivator<T> activator) {
		this.activator = activator;
		if (activator.getHook() != this) {
			activator.setHook(this);
		}
	}
	
	/**
	 * 
	 * @return the hook's name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * called when the parser is activated by its parent
	 * 
	 * @return true if correctly activated
	 */
	public final boolean activate() {
		elements = 0;
		if (activator != null) {
			activator.hookWasActivated();
		}
		return _activate();
	}
	
	public abstract boolean _activate();
	
	/**
	 * called when the parser is deactivated by its parent
	 * 
	 * @return true if correctly activated
	 */
	public abstract boolean deactivate();
	
	/**
	 * 
	 * @return true if this hook parser must be activated
	 */
	public final boolean mustActivate() {
		if (activator == null) return false;
		
		return activator.mustActivate();
	}
	
	public boolean mustDeactivate() {
		return elements == 0;
	}
	
	/**
	 * if something needs to be done BEFORE XMLProcessor starts parsing
	 */
	protected void before() {}
	
	/**
	 * if something needs to be done AFTER XMLProcessor starts parsing
	 */
	protected void after() {}
	
	/**
	 * if something needs to be done when an error occurs
	 */
	protected void closeForError() {}
	
	protected void processParserEvent(int event) throws XMLStreamException, IOException {
		switch (event) {
			case XMLStreamConstants.NAMESPACE:
				processNamespace();
				break;
			case XMLStreamConstants.START_ELEMENT:
				elements++;
				processStartElement();
				break;
			case XMLStreamConstants.CHARACTERS:
				processCharacters();
				break;
			case XMLStreamConstants.END_ELEMENT:
				processEndElement();
				elements--;
//				if (elements < 0) {
//					//Log.severe("ERROR: Hook was not interupted on time AT " + getLocation());
//				}
				break;
			case XMLStreamConstants.PROCESSING_INSTRUCTION:
				processProcessingInstruction();
				break;
			case XMLStreamConstants.DTD:
				processDTD();
				break;
			case XMLStreamConstants.CDATA:
				processCDATA();
				break;
			case XMLStreamConstants.COMMENT:
				processComment();
				break;
			case XMLStreamConstants.END_DOCUMENT:
				processEndDocument();
				break;
			case XMLStreamConstants.ENTITY_REFERENCE:
				processEntityReference();
				break;
		}
	}
	
	public final String getLocation() {
		return parentParser.getLocation();
	}
	
	protected abstract void processNamespace() throws XMLStreamException;
	
	protected abstract void processStartElement() throws XMLStreamException, IOException;
	
	protected abstract void processCharacters() throws XMLStreamException;
	
	protected abstract void processProcessingInstruction() throws XMLStreamException;
	
	protected abstract void processDTD() throws XMLStreamException;
	
	protected abstract void processCDATA() throws XMLStreamException;
	
	protected abstract void processComment() throws XMLStreamException;
	
	protected abstract void processEndElement() throws XMLStreamException;
	
	protected abstract void processEndDocument() throws XMLStreamException;
	
	protected abstract void processEntityReference() throws XMLStreamException;
	
	public String getCurrentPath() {
		return parentParser.getCurrentPath();
	}
	
	public String getLocalName() {
		return parentParser.localname;
	}
	
	public HookActivator<T> getActivator() {
		return activator;
	}
}
