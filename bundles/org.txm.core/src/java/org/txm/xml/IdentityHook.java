package org.txm.xml;

import java.io.IOException;

import javax.xml.stream.XMLStreamException;

public abstract class IdentityHook extends Hook<XMLProcessor> {
	
	public IdentityHook(String name, HookActivator activator, XMLProcessor parentParser) throws IOException, XMLStreamException {
		super(name, activator, parentParser);
	}
	
	@Override
	protected void processNamespace() throws XMLStreamException {
		parentParser.processNamespace();
	}
	
	@Override
	protected void processStartElement() throws XMLStreamException, IOException {
		parentParser.processStartElement();
	}
	
	@Override
	protected void processCharacters() throws XMLStreamException {
		parentParser.processCharacters();
	}
	
	@Override
	protected void processProcessingInstruction() throws XMLStreamException {
		parentParser.processProcessingInstruction();
	}
	
	@Override
	protected void processDTD() throws XMLStreamException {
		parentParser.processDTD();
	}
	
	@Override
	protected void processCDATA() throws XMLStreamException {
		parentParser.processCDATA();
	}
	
	@Override
	protected void processComment() throws XMLStreamException {
		parentParser.processComment();
	}
	
	@Override
	protected void processEndElement() throws XMLStreamException {
		parentParser.processEndElement();
	}
	
	@Override
	protected void processEndDocument() throws XMLStreamException {
		parentParser.processEndDocument();
	}
	
	@Override
	protected void processEntityReference() throws XMLStreamException {
		parentParser.processEntityReference();
	}
	
}
