// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2016-03-02 14:17:06 +0100 (mer., 02 mars 2016) $
// $LastChangedRevision: 3134 $
// $LastChangedBy: mdecorde $
//
package org.txm.xml.xmltxm;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import javax.xml.stream.XMLStreamException;

import org.txm.utils.logger.Log;
import org.txm.xml.IdentityHook;
import org.txm.xml.XMLProcessor;
import org.txm.xml.XPathHookActivator;

/**
 * The Class XMLTXMWordPropertiesInjection.
 *
 * @author mdecorde
 * 
 *         inject annotation from a stand-off file into a xml-tei-txm file
 */

public class XMLTXMWordPropertiesInjection extends XMLProcessor {
	
	HashMap<String, HashMap<String, String>> rules;
	
	HashSet<String> nonActivatedRules = new HashSet<>();
	
	XPathHookActivator activator;
	
	IdentityHook hook;
	
	int nInsertions = 0;
	
	public XMLTXMWordPropertiesInjection(File infile) throws IOException, XMLStreamException {
		super(infile);
	}
	
	public HashMap<String, HashMap<String, String>> getRules() {
		return rules;
	}
	
	/**
	 * @param id the word identifier (id) to process.
	 * @param properties hashmap of ana@type+ana@value to injected. Warning ana@type must be prefixed with "#"
	 * 
	 */
	public void addProperty(String id, HashMap<String, String> properties) throws IOException, XMLStreamException {
		if (rules == null) {
			setProperties(new HashMap<String, HashMap<String, String>>());
		}
		
		rules.put(id,  properties);
	}
	
	public HashSet<String> getNonActivatedRules() {
		return nonActivatedRules;
	}
	
	/**
	 * @param rules the keys are the word identifiers (id) to process. the values are hashmap of ana@type+ana@value to injected. Warning ana@type must be prefixed with "#"
	 * 
	 */
	public void setProperties(HashMap<String, HashMap<String, String>> rules) throws IOException, XMLStreamException {
		
		this.rules = rules;
		nonActivatedRules.addAll(rules.keySet());
		
		activator = new XPathHookActivator<>(hook, "//w");
		
		hook = new IdentityHook("word_hook", activator, this) {
			
			String id;
			
			boolean inAna = false;
			
			boolean inForm = false;
			
			boolean inW = false;
			
			ArrayList<String[]> anaValues = new ArrayList<>();
			
			ArrayList<String[]> formValues = new ArrayList<>();
			
			StringBuilder value = new StringBuilder();
			
			String resp = "";
			
			String type = "";
			
			@Override
			public boolean deactivate() {
				return true;
			}
			
			@Override
			public boolean _activate() {
				id = parser.getAttributeValue(null, "id");
				
				if (id != null && rules.containsKey(id)) {
					Log.finest("Activating hook on "+id);
					nonActivatedRules.remove(id); // the rule has been activated once
					anaValues.clear(); // empty ana values
					formValues.clear(); // empty form values
					nInsertions++;
					return true;
				}
				else {
					return false;
				}
			}
			
			@Override
			protected void processStartElement() throws XMLStreamException, IOException {
				if (localname.equals("ana")) {
					// store values
					inAna = true;
					
					resp = parser.getAttributeValue(null, "resp");
					
					type = parser.getAttributeValue(null, "type");
					
					value.setLength(0);
					return; // write ana later
				}
				else if (localname.equals("form")) {
					// store values
					inForm = true;
					
					
					if (parser.getAttributeValue(null, "type") != null) {
						type = parser.getAttributeValue(null, "type");
					}
					else {
						type = "";
					}
					
					value.setLength(0);
					return; // write form later
				}
				else {
					if (localname.equals("w")) inW = true;
					
					super.processStartElement();
				}
			}
			
			@Override
			protected void processCharacters() throws XMLStreamException {
				if (inAna || inForm) {
					value.append(parser.getText());
				}
				else if (inW) {
					
				}
				else {
					super.processCharacters();
				}
			}
			
			@Override
			protected void processEndElement() throws XMLStreamException {
				if (localname.equals("w")) {
					// update ana values
					HashMap<String, String> values = rules.get(id);
					
					for (String[] l : anaValues) { // update existing txm:ana and remove the updated value from 'values'
						if (values.containsKey(l[0])) {
							l[2] = values.get(l[0]);
							values.remove(l[0]);
						}
					}
					
					for (Entry<String, String> e : values.entrySet()) { // create new values (for remaining values of 'values'
						anaValues.add(new String[] { e.getKey(), "#txm", e.getValue() }); // the ana type is already prefixed with #
					}
					
					// write forms
					for (String[] l : formValues) {
						writer.writeStartElement("txm:form");
						if (l[0].length() > 0) {
							writer.writeAttribute("type", l[0]);
						}
						writer.writeCharacters(l[1]);
						writer.writeEndElement();
					}
					writer.writeCharacters("\n");
					
					// write anas
					for (String[] l : anaValues) {
						writer.writeStartElement("txm:ana");
						writer.writeAttribute("resp", l[1]);
						writer.writeAttribute("type", l[0]);
						writer.writeCharacters(l[2]);
						writer.writeEndElement();
					}
					writer.writeCharacters("\n");
					
					// let parent writer the end of the w element
					inW = false;
					super.processEndElement();
				}
				else if (localname.equals("ana")) {
					anaValues.add(new String[] { type.toString(), resp.toString(), value.toString() });
					inAna = false;
					return; // write ana later
				}
				else if (localname.equals("form")) {
					formValues.add(new String[] { type.toString(), value.toString() });
					inForm = false;
					return; // write form later
				}
				else {
					super.processEndElement();
				}
			}
		};
	}
	
	public int getNInsertions() {
		return nInsertions;
	}
	
	public static void main(String[] args) {
		
		File infile = new File(System.getProperty("user.home"), "xml/srcmfmadridxmltxm/alexis-txm.xml");
		File outfile = new File(System.getProperty("user.home"), "xml/srcmfmadridxmltxm/alexis-txm-out.xml");
		
		try {
			XMLTXMWordPropertiesInjection processor = new XMLTXMWordPropertiesInjection(infile);
			HashMap<String, HashMap<String, String>> rules = new HashMap<>();
			
			HashMap<String, String> props = new HashMap<>();
			props.put("lol", "lolvalue");
			rules.put("w19_1303", props);
			processor.setProperties(rules);
			
			processor.process(outfile);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
