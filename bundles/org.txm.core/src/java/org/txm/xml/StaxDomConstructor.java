package org.txm.xml;

import java.io.Reader;
import java.io.StringReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.txm.utils.xml.DomUtils;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.EntityReference;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;

public class StaxDomConstructor {
	
	XMLStreamReader parser = null;
	
	public StaxDomConstructor(XMLStreamReader parser) {
		this.parser = parser;
	}
	
	/**
	 * consumes the parser events to create a DOM. start consuming after the current Stax event
	 * 
	 * stop consuming when the current parser element is closed
	 * 
	 * @return
	 * @throws ParserConfigurationException
	 * @throws XMLStreamException
	 * @throws IllegalAccessException
	 */
	public Element getDom() throws ParserConfigurationException, XMLStreamException, IllegalAccessException {
		
		if (parser.getEventType() != XMLStreamConstants.START_ELEMENT) {
			throw new IllegalAccessException("The Dom constructor must be called from the XMLStreamConstants.START_ELEMENT event");
		}
		
		Document doc = DomUtils.emptyDocument();
		Element currentElement = null;
		
		int elements = 0;
		String localname = null;
		
		for (int event = parser.getEventType(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					
					String ns = parser.getNamespaceURI();
					if (ns == null) ns = "";
					Element e = doc.createElementNS(ns, localname);
					
					for (int i = 0; i < parser.getAttributeCount(); i++) {
						ns = parser.getAttributeNamespace(i);
						if (ns != null) {
							e.setAttributeNS(ns, parser.getAttributeLocalName(i), parser.getAttributeValue(i));
						}
						else {
							e.setAttribute(parser.getAttributeLocalName(i), parser.getAttributeValue(i));
						}
					}
					
					if (currentElement == null) {
						doc.appendChild(e);
					}
					else {
						currentElement.appendChild(e);
					}
					currentElement = e;
					elements++;
					break;
				case XMLStreamConstants.CHARACTERS:
					Text textNode = doc.createTextNode(parser.getText());
					currentElement.appendChild(textNode);
					break;
				case XMLStreamConstants.END_ELEMENT:
					
					elements--;
					if (elements == 0) return doc.getDocumentElement(); // stop parsing elements now : it was the last element
					
					currentElement = (Element) currentElement.getParentNode();
					
					break;
				case XMLStreamConstants.PROCESSING_INSTRUCTION:
					ProcessingInstruction piNode = doc.createProcessingInstruction(parser.getPITarget(), parser.getPIData());
					currentElement.appendChild(piNode);
					break;
				case XMLStreamConstants.CDATA:
					CDATASection cDataNode = doc.createCDATASection(parser.getText());
					currentElement.appendChild(cDataNode);
					break;
				case XMLStreamConstants.COMMENT:
					Comment commentNode = doc.createComment(parser.getText());
					currentElement.appendChild(commentNode);
					break;
				case XMLStreamConstants.END_DOCUMENT:
					return doc.getDocumentElement();
				case XMLStreamConstants.ENTITY_REFERENCE:
					EntityReference entityNode = doc.createEntityReference(parser.getText());
					currentElement.appendChild(entityNode);
					break;
			}
		}
		return null;
	}
	
	public static void main(String[] args) throws Exception {
		Reader inputData = new StringReader("<test><ceci><est><un></un></est></ceci></test>");
		XMLStreamReader parser = XMLInputFactory.newInstance().createXMLStreamReader(inputData);
		parser.next();
		System.out.println("E=" + parser.getLocalName());
		parser.next();
		System.out.println("E=" + parser.getLocalName());
		System.out.println(new StaxDomConstructor(parser).getDom());
		parser.close();
		inputData.close();
	}
}
