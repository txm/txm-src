package org.txm.xml;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.LinkedHashMap;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 * Parse and write a XML file. If no hook specified the file is not transformed (mimic the implementation of the old StaxIdentityParser)
 * 
 * See the available Hook and Activators classes to do more things
 * 
 * @author mdecorde
 *
 */
public class XMLProcessor extends XMLParser {
	
	/** The output. */
	protected XMLOutputFactory outfactory = XMLOutputFactory.newInstance();
	
	protected BufferedOutputStream output;
	
	protected XMLStreamWriter writer;
	
	/**
	 * list of XML elements that are always milestones elements
	 */
	protected HashSet<String> milestoneElements = new HashSet<>();
	
	/**
	 * 
	 * @param infile the file to read
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	public XMLProcessor(File infile) throws IOException, XMLStreamException {
		this(infile.toURI().toURL());
	}
	
	/**
	 * 
	 * @param inputurl the input URL to read
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	public XMLProcessor(URL inputurl) throws IOException, XMLStreamException {
		super(inputurl);
	}
	
	/**
	 * Helper method to get a Stax element attribute value using its name
	 * 
	 * @param name the attribute name
	 * @return the value if any
	 */
	@Override
	public String getParserAttributeValue(String name) {
		if (name == null) return null;
		
		// try the no namespace attribute first
		if (parser.getAttributeValue(null, name) != null) {
			return parser.getAttributeValue(null, name);
		}
		
		// no luck try all attributes
		int c = parser.getAttributeCount();
		for (int i = 0; i < c; i++) {
			if (name.equals(parser.getAttributeLocalName(i))) {
				return parser.getAttributeValue(i);
			}
		}
		
		return null;
	}
	
	@Override
	protected void before() {
		
		super.before();
	}
	
	/**
	 * closing parser and writer
	 */
	@Override
	protected void after(boolean allWentWell) throws XMLStreamException, IOException {
		
		super.after(allWentWell); // close parser, inputData and factory
		
		if (writer != null) writer.close();
		
		if (output != null) output.close();
	}
	
	/**
	 * Creates the output.
	 *
	 * @return true, if successful
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	private boolean createOutput(File f) throws XMLStreamException, IOException {
		
		if (writer != null) { // process from a file
			writer.close();
		}
		if (output != null) { // process from a file
			output.close();
		}
		
		output = new BufferedOutputStream(new FileOutputStream(f), 16 * 1024);
		
		// create writer later
		return true;
	}
	
	public boolean process(File outFile) throws XMLStreamException, IOException {
		
		if (!createOutput(outFile)) {
			return false;
		}
		
		
		boolean ret = process(writer);
		
		return ret;
	}
	
	public boolean process(XMLStreamWriter awriter) throws XMLStreamException, IOException {
		
		if (factory == null) {
			System.out.println("Error: this parser is a Hook parser to be used with another StaxIdentityParser as a Hook");
			return false;
		}
		
		this.writer = awriter;
		
		return process();
		
	}
	
	/**
	 * Go to the end of an element :
	 * - The start element has already been written
	 * - the content is skipped and written
	 * 
	 * @return true if all went well
	 * @param tagname
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	public boolean goToEnd(String tagname) throws XMLStreamException, IOException {
		int elements = 1;
		try {
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				// System.out.println("event "+event
				switch (event) {
					case XMLStreamConstants.START_DOCUMENT:
						processStartDocument();
						break;
					case XMLStreamConstants.SPACE:
						processSpace();
						break;
					case XMLStreamConstants.NOTATION_DECLARATION:
						processNotationDeclaration();
						break;
					case XMLStreamConstants.NAMESPACE:
						processNamespace();
						break;
					case XMLStreamConstants.START_ELEMENT:
						elements++;
						localname = parser.getLocalName();
						currentXPath.append(SLASH);
						currentXPath.append(localname);
						
						// register namespaces
						for (int i = 0; i < parser.getNamespaceCount(); i++) {
							Nscontext.addNamespace(parser.getNamespacePrefix(i), parser.getNamespaceURI(i));
						}
						
						buildCurrentAttributes();
						
						processStartElement();
						break;
					case XMLStreamConstants.CHARACTERS:
						processCharacters();
						break;
					case XMLStreamConstants.PROCESSING_INSTRUCTION:
						processProcessingInstruction();
						break;
					case XMLStreamConstants.DTD:
						processDTD();
						break;
					case XMLStreamConstants.CDATA:
						processCDATA();
						break;
					case XMLStreamConstants.COMMENT:
						processComment();
						break;
					case XMLStreamConstants.END_ELEMENT:
						elements--;
						localname = parser.getLocalName();
						
						writer.writeEndElement();
						
						currentXPath.setLength(currentXPath.length() - localname.length() - 1);
						
						if (elements == 0 && localname == tagname) {
							return false;
						}
						break;
					case XMLStreamConstants.END_DOCUMENT:
						processEndDocument();
						break;
					case XMLStreamConstants.ENTITY_REFERENCE:
						processEntityReference();
						break;
				}
			}
		}
		catch (Exception e) {
			System.out.println("Location " + getLocation());
			org.txm.utils.logger.Log.printStackTrace(e);
			after(false);
			return false;
		}
		return true;
	}
	
	@Override
	protected void processStartDocument() throws XMLStreamException {
		
		String encoding = parser.getCharacterEncodingScheme();
		if (encoding == null) {
			encoding = "UTF-8";
		}
		writer = outfactory.createXMLStreamWriter(output, encoding);// create a new file
		writer.setNamespaceContext(Nscontext);
		
		writer.writeStartDocument(encoding, parser.getVersion());
		writer.writeCharacters("\n");
	}
	
	@Override
	protected void processNamespace() throws XMLStreamException {
		this.Nscontext.addNamespace(parser.getPrefix(), parser.getNamespaceURI());
		writer.writeNamespace(parser.getPrefix(), parser.getNamespaceURI());
	}
	
	@Override
	protected void processStartElement() throws XMLStreamException, IOException {
		
		String prefix = parser.getPrefix();
		if (prefix != null && prefix.length() > 0) {
			if (milestoneElements.contains(localname)) {
				writer.writeEmptyElement(Nscontext.getNamespaceURI(prefix), localname);
			}
			else {
				writer.writeStartElement(Nscontext.getNamespaceURI(prefix), localname);
			}
		}
		else {
			if (milestoneElements.contains(localname)) {
				writer.writeEmptyElement(localname);
			}
			else {
				writer.writeStartElement(localname);
			}
		}
		
		for (int i = 0; i < parser.getNamespaceCount(); i++) {
			writer.writeNamespace(parser.getNamespacePrefix(i), parser.getNamespaceURI(i));
		}
		
		writeAttributes();
	}
	
	protected void writeAttributes() throws XMLStreamException {
		for (int i = 0; i < parser.getAttributeCount(); i++) {
			writeAttribute(parser.getAttributePrefix(i), parser.getAttributeLocalName(i), parser.getAttributeValue(i));
		}
	}
	
	protected void writeAttribute(String prefix, String name, String value) throws XMLStreamException {
		if (prefix != null && prefix.length() > 0) {
			writer.writeAttribute(prefix + ":" + name, value);
		}
		else {
			writer.writeAttribute(name, value);
		}
	}
	
	@Override
	protected void processCharacters() throws XMLStreamException {
		if (parser.getText() != null && parser.getText().length() > 0) {
			writer.writeCharacters(parser.getText());
		}
	}
	
	@Override
	protected void processProcessingInstruction() throws XMLStreamException {
		writer.writeProcessingInstruction(parser.getPITarget(), parser.getPIData());
		if (!firstElementParsed) writer.writeCharacters("\n");
	}
	
	@Override
	protected void processDTD() throws XMLStreamException {
		writer.writeDTD(parser.getText());
	}
	
	@Override
	protected void processCDATA() throws XMLStreamException {
		writer.writeCData(parser.getText());
	}
	
	@Override
	protected void processComment() throws XMLStreamException {
		writer.writeComment(parser.getText());
	}
	
	@Override
	protected void processEndElement() throws XMLStreamException {
		if (!milestoneElements.contains(localname)) {
			writer.writeEndElement();
		}
	}
	
	@Override
	protected void processEndDocument() throws XMLStreamException {
		writer.writeEndDocument();
	}
	
	@Override
	protected void processEntityReference() throws XMLStreamException {
		writer.writeEntityRef(parser.getLocalName());
	}
	
	@Override
	public String getCurrentPath() {
		return currentXPath.toString();
	}
	
	public static void main(String[] args) {
		try {
			File input = new File(System.getProperty("user.home"), "xml/identity/test.xml");
			File output = new File(System.getProperty("user.home"), "xml/identity/test-copy.xml");
			if (!(input.exists() && input.canRead())) {
				System.out.println("cannot found $input");
				return;
			}
			XMLProcessor builder;
			
			builder = new XMLProcessor(input.toURI().toURL());
			builder.addMilestoneElements("milestone");
			// IdentityHook hook1 = new IdentityHook("hook1", new LocalNameHookActivator(null, "p"), builder) {
			//
			// @Override
			// public boolean _activate() {
			// System.out.println("ACTIVATING " + name + " AT " + getLocation());
			// return true;
			// }
			//
			// @Override
			// public boolean deactivate() {
			// System.out.println("DEACTIVATING " + name + " AT " + getLocation());
			// return true;
			// }
			//
			// @Override
			// public void processStartElement() throws XMLStreamException, IOException {
			// super.processStartElement();
			// parentParser.writer.writeAttribute("hook", name);
			// }
			// };
			// XPathHookActivator xpathActivator = new XPathHookActivator(null, "//p");
			// XPathsHookActivator xpathsActivator = new XPathsHookActivator(null, Arrays.asList("//p", "//p"));
			// IdentityHook hook2 = new IdentityHook("hook2", xpathsActivator, builder) {
			//
			// @Override
			// public boolean _activate() {
			// System.out.println("ACTIVATING " + name + " AT " + getLocation());
			// return true;
			// }
			//
			// @Override
			// public boolean deactivate() {
			// System.out.println("DEACTIVATING " + name + " AT " + getLocation());
			// return true;
			// }
			//
			// @Override
			// public void processStartElement() throws XMLStreamException, IOException {
			// super.processStartElement();
			// parentParser.writer.writeAttribute("hook", name);
			// }
			// };
			//
			// // DOMIdentityHook hook3 = new DOMIdentityHook("hook3", xpathsActivator, builder) {
			//
			// };
			
			long time = System.currentTimeMillis();
			if (builder.process(output)) {
				System.out.println("Time=" + (System.currentTimeMillis() - time));
				// System.out.println("XPaths activator has done working ? " + xpathsActivator.hasAllXpathsBeenProcessed());
			}
			else {
				System.out.println("failure !");
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void addMilestoneElements(String... elements) {
		for (String e : elements) {
			milestoneElements.add(e);
		}
	}
	
	public void removeMilestoneElements(String... elements) {
		for (String e : elements) {
			milestoneElements.remove(e);
		}
	}
	
	/**
	 * 
	 * @return the actual hooks hash
	 */
	@Override
	public LinkedHashMap<String, Hook<?>> getHooks() {
		return hooks;
	}
	
	@Override
	public void addHook(String name, Hook<?> hook) {
		hooks.put(name, hook);
	}
}
