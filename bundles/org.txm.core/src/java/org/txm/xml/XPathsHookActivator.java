package org.txm.xml;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.logger.Log;

/**
 * Simple "XPath without attribute selector" Activator activated when the current path matches the xpath
 * 
 * supported operators : / , // , * , node()
 * only the last element attributes can be tested with simple equals test
 * 
 * exemple: //p[name='value']
 * exemple: /text/node()/p[name='value']
 * 
 * @author mdecorde
 *
 */
public class XPathsHookActivator<T extends XMLParser> extends XPathHookActivator<T> {
	
	int currentXPath = 0;
	
	List<String> xpaths;
	
	/**
	 */
	public XPathsHookActivator(Hook<T> hook, List<String> xpaths) {
		super(hook, xpaths.get(0));
		this.xpaths = xpaths;
		
		loadNextXPath();
	}
	
	/**
	 * the xpaths used to build the patterns
	 */
	public List<String> getXPaths() {
		return xpaths;
	}
	
	/**
	 * 
	 */
	public int getCurrentXPathIndex() {
		return currentXPath;
	}
	
	/**
	 * the xpaths used to build the patterns
	 */
	public String getCurrentXPath() {
		if (xpaths.size() <= currentXPath) return null;
		
		return xpaths.get(currentXPath);
	}
	
	protected boolean loadNextXPath() {
		if (xpaths == null || xpaths.size() <= currentXPath) {
			p = null;
			return true; // nothing to load
		}
		
		String xpath = xpaths.get(currentXPath);
		String regex = xpath;
		
		int attrTest = xpath.indexOf("["); // [some tests]
		int lastNodeTest = xpath.lastIndexOf("/");
		
		if (attrTest > 0 && attrTest > lastNodeTest) {
			attributesTest = new LinkedHashMap<>();
			regex = xpath.substring(0, attrTest);
			String tail = xpath.substring(attrTest);
			tail = tail.substring(1, tail.length() - 1);
			if (debug) System.out.println("TAIL=" + tail);
			String tests[] = tail.split("@");
			if (debug) System.out.println("TESTS=" + Arrays.toString(tests));
			for (String test : tests) {
				if (test.length() == 0) continue;
				
				String split2[] = test.split("=", 2); // 0 name 1 "value"
				if (debug) System.out.println("TEST=" + Arrays.toString(split2));
				attributesTest.put(split2[0], split2[1].substring(1, split2[1].length() - 1));
			}
		}
		
		regex = regex.replaceAll("([^.])[*]", "$1[^/]+");
		if (regex.startsWith("//")) regex = "(/[^/]+)*/" + regex.substring(2);
		regex = regex.replace("//", "(/[^/]+)*/");
		regex = regex.replace("node()", "[^/]+");
		
		try {
			this.p = Pattern.compile(regex);
			if (debug) System.out.println("P=" + p);
		}
		catch (PatternSyntaxException e) {
			String m = NLS.bind("Pattern not compiled correctly with: {0} at {1}.", xpath + " (" + currentXPath + ")", hook.getLocation());
			Log.severe(m);
			Log.severe("Pattern error: " + e);
			Log.printStackTrace(e);
			p = null;
			return false;
		}
		
		return true;
	}
	
	public boolean hasAllXpathsBeenProcessed() {
		return xpaths != null && xpaths.size() == currentXPath;
	}
	
	@Override
	public boolean mustActivate() {
		boolean b = super.mustActivate();
		if (b) {
			currentXPath++;
		}
		return b;
	}
	
	@Override
	public void hookWasActivated() {
		loadNextXPath();
	}
	
	public static void main(String[] args) {
		new XPathsHookActivator(null,
				Arrays.asList("//text//p[@name='value']", "//text//p[@name='value']"));
	}
}
