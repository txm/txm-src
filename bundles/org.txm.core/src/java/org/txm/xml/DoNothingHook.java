package org.txm.xml;

import java.io.IOException;

import javax.xml.stream.XMLStreamException;

public class DoNothingHook<T extends XMLParser> extends SimpleHook<T> {
	
	public DoNothingHook(String name, HookActivator<T> activator, T parentParser) throws IOException, XMLStreamException {
		super(name, activator, parentParser);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public boolean _activate() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean deactivate() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	protected void processStartElement() throws XMLStreamException, IOException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected void processCharacters() throws XMLStreamException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected void processEndElement() throws XMLStreamException {
		// TODO Auto-generated method stub
		
	}
	
}
