package org.txm.xml;

import java.io.IOException;

import javax.xml.stream.XMLStreamException;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMIdentityHook extends Hook<XMLProcessor> {
	
	/**
	 * The constructed DOM element
	 */
	protected Element dom;
	
	public DOMIdentityHook(String name, HookActivator<XMLProcessor> activator, XMLProcessor processor) throws IOException, XMLStreamException {
		super(name, activator, processor);
	}
	
	@Override
	public final boolean _activate() {
		StaxDomConstructor domConstructor = new StaxDomConstructor(parentParser.parser);
		try {
			dom = domConstructor.getDom();
			
			return dom != null;
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	@Override
	public final boolean mustDeactivate() {
		boolean b = dom != null; // the DOM has been constructed and the XMLProcess can continue parsing
		if (b) {
			try {
				processDom();
				writeDOM();
				dom = null;
			}
			catch (XMLStreamException e) {
				e.printStackTrace();
			}
		}
		return b;
	}
	
	/**
	 * extends this method to process the DOM before it is written
	 */
	public void processDom() {
		// nothing since its the DomIdentityHook
	}
	
	@Override
	public final boolean deactivate() {
		return dom == null; // dom should be null
	}
	
	private void writeDOM() throws XMLStreamException {
		writeDOM(dom);
	}
	
	private void writeDOM(Node node) throws XMLStreamException {
		if (node == null) return; // nothing to write
		
		if (node.getNodeType() == Node.ELEMENT_NODE) {
			parentParser.writer.writeCharacters("\n");
			Element e = (Element) node;
			NodeList children = e.getChildNodes();
			String ns = e.getNamespaceURI();
			String ln = e.getLocalName();
			if (children.getLength() > 0) {
				if (ns == null) {
					parentParser.writer.writeStartElement(ln);
				} else {
					parentParser.writer.writeStartElement(ns, ln);
				}
			}
			else {
				if (ns == null) {
					parentParser.writer.writeEmptyElement(ln);
				} else {
					parentParser.writer.writeEmptyElement(ns, ln);
				}
			}
			
			for (int i = 0; i < e.getAttributes().getLength(); i++) {
				Node att = e.getAttributes().item(i);
				if (att.getNamespaceURI() != null) {
					parentParser.writer.writeAttribute(att.getNamespaceURI(), att.getNodeName(), att.getNodeValue());
				}
				else {
					parentParser.writer.writeAttribute(att.getNodeName(), att.getNodeValue());
				}
			}
			//parentParser.writer.writeCharacters("\n");
			
			for (int c = 0; c < children.getLength(); c++) {
				writeDOM(children.item(c));
			}
			if (children.getLength() > 0) {
				parentParser.writer.writeEndElement();
				//parentParser.writer.writeCharacters("\n");
			}
		}
		else if (node.getNodeType() == Node.TEXT_NODE) {
			parentParser.writer.writeCharacters(node.getTextContent());
		} else if (node.getNodeType() == Node.PROCESSING_INSTRUCTION_NODE) {
			parentParser.writer.writeProcessingInstruction(node.getNodeName(), node.getNodeValue());
		} else if (node.getNodeType() == Node.CDATA_SECTION_NODE) {
			parentParser.writer.writeCData(node.getNodeValue());
		} else if (node.getNodeType() == Node.COMMENT_NODE) {
			parentParser.writer.writeComment(node.getNodeValue());
		}
	}
	
	@Override
	protected void processNamespace() throws XMLStreamException {}
	
	@Override
	protected void processStartElement() throws XMLStreamException, IOException {}
	
	@Override
	protected void processCharacters() throws XMLStreamException {}
	
	@Override
	protected void processProcessingInstruction() throws XMLStreamException {}
	
	@Override
	protected void processDTD() throws XMLStreamException {}
	
	@Override
	protected void processCDATA() throws XMLStreamException {}
	
	@Override
	protected void processComment() throws XMLStreamException {}
	
	@Override
	protected void processEndElement() throws XMLStreamException {}
	
	@Override
	protected void processEndDocument() throws XMLStreamException {}
	
	@Override
	protected void processEntityReference() throws XMLStreamException {}
}