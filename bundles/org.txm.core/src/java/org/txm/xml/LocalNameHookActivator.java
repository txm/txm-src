package org.txm.xml;

/**
 * Simple Activator activated when the localname matches the localname constructor parameter
 * 
 * @author mdecorde
 *
 */
public class LocalNameHookActivator<T extends XMLParser> extends HookActivator<T> {
	
	String localname;
	
	/**
	 * @param localname the element localname to match
	 */
	public LocalNameHookActivator(String localname) {
		this(null, localname);
	}
	
	
	/**
	 * @param localname the element localname to match
	 */
	public LocalNameHookActivator(Hook<T> hook, String localname) {
		super(hook);
		this.localname = localname;
	}
	
	@Override
	public boolean mustActivate() {
		if (localname == null) return false;
		
		return localname.equals(hook.getLocalName());
	}
	
	@Override
	public void hookWasActivated() {
		// nothing to do
	}
}
