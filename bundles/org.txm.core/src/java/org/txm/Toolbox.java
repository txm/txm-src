// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (Tue, 24 Jan 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//

package org.txm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.Collator;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.internal.registry.osgi.OSGIUtils;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.RegistryFactory;
import org.eclipse.core.runtime.SafeRunner;
import org.eclipse.osgi.util.NLS;
import org.osgi.framework.Version;
import org.txm.core.engines.Engine;
import org.txm.core.engines.EngineType;
import org.txm.core.engines.EnginesManager;
import org.txm.core.engines.ImportEngines;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.TXMResult;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Workspace;
import org.txm.utils.BundleUtils;
import org.txm.utils.DeleteDir;
import org.txm.utils.LogMonitor;
import org.txm.utils.OSDetector;
import org.txm.utils.io.FileCopy;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

/**
 * Initialize TXM engine services using the installed @see org.txm.Engine extension points.
 * 
 * Use the initialize() static method to start the Toolbox without special configuration.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class Toolbox {
	
	public static final DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd"); //$NON-NLS-1$
	// public static final DateFormat shortUKDateformat = new SimpleDateFormat("dd-MM-yy");
	
	private static LinkedHashMap<EngineType, EnginesManager<?>> enginesManagers = new LinkedHashMap<>();
	
	private static boolean initializing = false;
	
	/** The state. */
	private static boolean state = false;
	
	/** Root of all TXMResults and manages the corpus */
	public static Workspace workspace;
	
	/**
	 * 
	 * @param enginesManager
	 */
	private static void addEngineManager(EnginesManager<?> enginesManager) {
		enginesManagers.put(enginesManager.getEnginesType(), enginesManager);
	}
	
	/**
	 * If search engine or stats engine or workspace is not ready then the toolbox is not ready.
	 *
	 * @return true, if successful
	 */
	private static boolean checkState() {
		return state;
	}
	
	private static boolean fetchEnginesManagers() {
		Log.finest("Fetching engines managers..."); //$NON-NLS-1$
		enginesManagers.clear();
		// System.out.println("Contributions of "+EnginesManager.ENGINEMANAGER_EXTENSION_POINT_ID);
		// Platform.getExtensionRegistry().getExtensionPoints();
		IConfigurationElement[] contributions = RegistryFactory.getRegistry().getConfigurationElementsFor(EnginesManager.ENGINES_MANAGER_EXTENSION_POINT_ID);
		// System.out.println("contributions: "+Arrays.toString(contributions));
		
		Log.fine(contributions.length + " engines managers found."); //$NON-NLS-1$
		
		for (int i = 0; i < contributions.length; i++) {
			try {
				EnginesManager<?> enginesManager = (EnginesManager<?>) contributions[i].createExecutableExtension("class"); //$NON-NLS-1$
				
				Log.fine("Registering " + enginesManager.getEnginesDescription() + " engines manager..."); //$NON-NLS-1$ //$NON-NLS-2$
				addEngineManager(enginesManager);
				if (enginesManager.fetchEngines()) {
					Log.fine(NLS.bind("{0} {1} engine(s) loaded.", enginesManager.size(), EngineType.NLP)); //$NON-NLS-1$
				}
				else {
					Log.fine("No engine found for " + enginesManager + ", installation failed."); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}
			catch (Throwable e) {
				e.printStackTrace();
			}
		}
		
		return true;
	}
	
	/**
	 * Gets a preference from the Toolbox Preferences node.
	 * 
	 * @param key
	 * @return
	 */
	public static String getPreference(String key) {
		return TBXPreferences.getInstance().getString(key);
	}
	
	/**
	 * Sets a preference of the Toolbox Preferences node.
	 * 
	 * @param key
	 * @param value
	 */
	public static void setPreference(String key, Object value) {
		TBXPreferences.getInstance().put(key, value);
	}
	
	/**
	 * Gets the manager of the specified engine type.
	 * 
	 * @param engineType
	 * @return
	 */
	public static EnginesManager<? extends Engine> getEngineManager(EngineType engineType) {
		
		if (enginesManagers == null) {
			return null;
		}
		return enginesManagers.get(engineType);
	}
	
	/**
	 * Gets the TXM working directory.
	 * 
	 * @return the TXM working directory set with the USER_TXM_HOME preference key
	 */
	public static String getTxmHomePath() {
		return getPreference(TBXPreferences.USER_TXM_HOME);
	}
	
	/**
	 * Initializes the Toolbox with the parameters read in the file
	 * textometrie.properties found in the user home directory
	 *
	 * @return true, if successful
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static boolean initialize() throws Exception {
		initializing = true;
		File home = new File(System.getProperty("user.home")); //$NON-NLS-1$
		File txmDirectory = new File(home, ".txm"); //$NON-NLS-1$
		if (!txmDirectory.exists()) {
			txmDirectory.mkdirs();
		}
		File configFile = new File(txmDirectory, "textometrie.properties"); //$NON-NLS-1$
		if (!configFile.exists()) {
			//			initializing = false;
			//			throw new IllegalArgumentException(TXMCoreMessages.file
			//					+ configFile.getAbsolutePath()
			//					+ TXMCoreMessages.doesNotExists);
		}
		
		boolean ret = initialize(configFile);
		initializing = false;
		return ret;
	}
	
	/**
	 * 
	 * @param preferencesStore
	 * @return
	 * @throws Exception
	 */
	public static boolean initialize(Class<?> preferencesStore) throws Exception {
		return initialize(preferencesStore, new LogMonitor(TXMCoreMessages.InitializingToolbox));
	}
	
	/**
	 * Initializes the Toolbox using the INSTANCE SCOPE preferences of TXMPreferences
	 *
	 * @param monitor the optional monitor
	 * @return true, if successful
	 * @throws Exception
	 */
	public static boolean initialize(Class<?> preferencesStore2, IProgressMonitor monitor) throws Exception {
		initializing = true;
		
		if (TBXPreferences.getInstance().getString(TBXPreferences.USER_TXM_HOME).length() == 0) {
			TBXPreferences.getInstance().put(TBXPreferences.USER_TXM_HOME, "TXM"); //$NON-NLS-1$
		}
		
		File homedir = new File(TBXPreferences.getInstance().getString(TBXPreferences.USER_TXM_HOME));
		homedir.mkdirs();
		
		try {
			Log.fine("Initializing logger..."); //$NON-NLS-1$
			Log.setLevel(Level.parse(getPreference(TBXPreferences.LOG_LEVEL)));
			Log.setPrintInConsole(TBXPreferences.getInstance().getBoolean(TBXPreferences.ADD_TECH_LOGS));
			Log.log_stacktrace_boolean = TBXPreferences.getInstance().getBoolean(TBXPreferences.LOG_STACKTRACE);
			
			if (!getPreference(TBXPreferences.LOG_DIR).isEmpty()) {
				Log.setPrintInFile(TBXPreferences.getInstance().getBoolean(TBXPreferences.LOG_IN_FILE), new File(getPreference(TBXPreferences.LOG_DIR)));
			}
			else {
				Log.setPrintInFile(TBXPreferences.getInstance().getBoolean(TBXPreferences.LOG_IN_FILE));
			}
		}
		catch (Exception e) {
			System.out.println("Error while initializing logger: " + e); //$NON-NLS-1$
			e.printStackTrace();
		}
		
		if (monitor != null) {
			monitor.setTaskName(TXMCoreMessages.initializingToolbox);
		}
		try {
			if (monitor != null) {
				monitor.subTask(TXMCoreMessages.loadingExtensionPreInstallation);
			}
			pluginsPreinstallation(monitor);
		}
		catch (Exception e) {
			Log.warning(NLS.bind(TXMCoreMessages.errorWhilePreInstallingPluginsP0, e.getLocalizedMessage()));
		}
		
		if (monitor != null && monitor.isCanceled()) return false;
		
		try {
			// initialize workspace : list of corpus loaded in TXM
			// copy shared files if needed
			state = startWorkspace(monitor);
			if (monitor != null && monitor.isCanceled()) return false;
			
		}
		catch (Exception e) {
			initializing = false;
			throw e;
		}
		
		try {
			startEnginesManagers(monitor);
			if (monitor != null && monitor.isCanceled()) return false;
		}
		catch (Exception e) {
			Log.warning(TXMCoreMessages.ErrorWhileStartingEngines);
			e.printStackTrace();
			initializing = false;
			throw e;
		}
		
		Log.finest(TXMCoreMessages.bind(TXMCoreMessages.ToolboxStatesWorkspaceColonP0, state));
		
		try {
			if (monitor != null) {
				monitor.subTask(TXMCoreMessages.loadingExtensionPostInstallation);
			}
			pluginsPostinstallation(monitor);
			if (monitor != null && monitor.isCanceled()) return false;
		}
		catch (Exception e) {
			Log.warning(NLS.bind(TXMCoreMessages.errorWhilePostInstallingPluginsP0, e.getLocalizedMessage()));
		}
		
		// loads and recreate the existing projects, loadParameters() needs engines to be ready
		if (workspace != null) {
			workspace.loadProjectsFromProjectScopes(monitor);
			monitor.subTask(""); //$NON-NLS-1$
		}
		else {
			System.out.println(TXMCoreMessages.ErrorToolboxIsNotCorrectlyInitializedAborting);
		}
		
		initializing = false;
		if (monitor != null) {
			monitor.setTaskName(""); //$NON-NLS-1$
		}
		return checkState();
	}
	
	/**
	 * initialize the toolbox from a UTF-8 file
	 * 
	 * key1=value1 key2=value2.
	 *
	 * @param file the file
	 * @return true, if successful
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static boolean initialize(File file) throws Exception {
		initializing = true;
		String encoding = "UTF-8"; //$NON-NLS-1$
		
		// Windows OS
		if (OSDetector.isFamilyWindows()) {
			encoding = "ISO-8859-1"; //$NON-NLS-1$
		}
		
		boolean ret = initialize(file, encoding);
		initializing = false;
		return ret;
	}
	
	/**
	 * Initializes the toolbox with the parameters read in the properties file
	 * passed in argument.
	 *
	 * @param file the file
	 * @param encoding the encoding
	 * @return true, if successful
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static boolean initialize(File file, String encoding) throws Exception {
		initializing = true;
		
		if (file != null && file.exists() && file.isFile()) {
			try {
				BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));
				String line = input.readLine();
				while (line != null) {
					String[] split = line.split("="); //$NON-NLS-1$
					if (split.length == 2) {
						setPreference(split[0], split[1]);
					}
					else if (split.length > 2) {
						String[] subsplit = new String[split.length - 1];
						System.arraycopy(split, 1, subsplit, 0, split.length - 1);
						setPreference(split[0], StringUtils.join(subsplit, "=")); //$NON-NLS-1$
					}
					line = input.readLine();
				}
				input.close();
			}
			catch (Exception e) {
				initializing = false;
				System.err.println(e);
				throw e;
			}
		}
		return initialize(TBXPreferences.class);
	}
	
	/**
	 * test if searchEngine and statEngine and Workspace are initialized.
	 *
	 * @return true, if is initialized
	 */
	public static boolean isInitialized() {
		return checkState();
	}
	
	public static boolean isInitializing() {
		return initializing;
	}
	
	/**
	 * Allow to install a TXM extension AFTER the Toolbox is initialized
	 * 
	 * @param monitor
	 */
	private static void pluginsPostinstallation(IProgressMonitor monitor) {
		
		IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(PostInstallationStep.EXTENSION_ID);
		
		Log.fine(TXMCoreMessages.loadingExtensions);
		Log.fine(Arrays.toString(config));
		
		for (IConfigurationElement e : config) {
			if (monitor != null && monitor.isCanceled()) return;
			try {
				Log.fine(NLS.bind(TXMCoreMessages.evaluatingExtensionColonP0FromP1, e.getName(), e.getContributor().getName()));
				
				final Object o = e.createExecutableExtension("class"); //$NON-NLS-1$
				if (o instanceof PostInstallationStep) {
					ISafeRunnable runnable = new ISafeRunnable() {
						
						@Override
						public void handleException(Throwable exception) {
							Log.severe(NLS.bind(TXMCoreMessages.exceptionColonP0, exception));
						}
						
						@Override
						public void run() throws Exception {
							((PostInstallationStep) o).install();
						}
					};
					SafeRunner.run(runnable);
				}
			}
			catch (Exception ex) {
				Log.severe(NLS.bind(TXMCoreMessages.ExceptionWhileInstallingExtensionIsP0, e.getName())); //$NON-NLS-2$
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * Allow to install a TXM extension BEFORE the Toolbox is initialized
	 */
	private static void pluginsPreinstallation(IProgressMonitor monitor) {
		
		IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(PostInstallationStep.EXTENSION_ID);
		Log.fine(TXMCoreMessages.loadingExtensions);
		Log.fine(Arrays.toString(config));
		
		for (IConfigurationElement e : config) {
			if (monitor != null && monitor.isCanceled()) return;
			try {
				Log.fine(NLS.bind(TXMCoreMessages.evaluatingExtensionColonP0FromP1, e.getName(), e.getContributor().getName()));
				
				final Object o = e.createExecutableExtension("class"); //$NON-NLS-1$
				if (o instanceof PostInstallationStep) {
					ISafeRunnable runnable = new ISafeRunnable() {
						
						@Override
						public void handleException(Throwable exception) {
							Log.severe(NLS.bind(TXMCoreMessages.exceptionColonP0, exception));
						}
						
						@Override
						public void run() throws Exception {
							((PostInstallationStep) o).preInstall();
						}
					};
					SafeRunner.run(runnable);
				}
			}
			catch (Exception ex) {
				Log.severe(NLS.bind(TXMCoreMessages.ExceptionWhileInstallingExtensionIsP0, e.getName()));
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * Removes an engines manager.
	 * 
	 * @param enginesManager
	 */
	private static void removeEngineManager(EnginesManager<?> enginesManager) {
		enginesManagers.remove(enginesManager.getEnginesType());
		Log.finest("Engines manager removed: " + enginesManager); //$NON-NLS-1$
	}
	
	/**
	 * Restarts the toolbox.
	 *
	 * @return true, if successful
	 * @throws Exception
	 */
	public static boolean restart() throws Exception {
		Toolbox.shutdown();
		Toolbox.initialize(TBXPreferences.class);
		return checkState();
	}
	
	// /**
	// * Restart the toolbox's workspace.
	// *
	// * @return true, if successful
	// */
	// public static boolean restartWorkspace(IProgressMonitor monitor) {
	// try {
	// shutdownWorkspace();
	// startWorkspace(monitor);
	// } catch (Exception e) {
	// Log.severe("Error while restarting workspace.");
	// e.printStackTrace();
	// }
	// return checkState();
	// }
	
	/**
	 * Shut the toolbox down.
	 */
	public static void shutdown() {
		
		
		
		try {
			// FIXME: tmp solution before IProject: remove all not persistable result nodes
			// TXMPreferences.deleteAllNotPersistableResultsNodes();
			// FIXME: tmp solution before IProject: remove result local nodes if persistence preference is disabled
			// if (!TBXPreferences.getInstance().getBoolean(TBXPreferences.AUTO_PERSISTENCE_ENABLED)) {
			// TXMPreferences.deleteAllResultsNodes();
			// }
			// FIXME: other tmp solution
			// TODO iterates over opened projects to save them (not close); add this code to project.close
			
			
			// save projects parameters
			if (Toolbox.workspace != null) {
				
				TXMResult.deleteAllNonPersistentResults(Toolbox.workspace);
				
				Toolbox.workspace.onWorkspaceClose();
				
				Toolbox.workspace.saveParameters(true);
			}
			
			stopEnginesManagers();
			
			state = false;
			
			// save global preferences
			TXMPreferences.saveAll();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// /**
	// * Shut the toolbox workspace down.
	// */
	// public static void shutdownWorkspace() {
	// try {
	// Toolbox.workspace.saveParameters();
	// Toolbox.workspace = null;
	// state = false;
	// } catch (Exception e) {
	// Log.severe("Error while closing workspace.");
	// e.printStackTrace();
	// }
	// }
	
	public static boolean startEnginesManagers(IProgressMonitor monitor) {
		
		fetchEnginesManagers();
		
		Log.finest("Starting engines of the {0}  engines managers..." + enginesManagers.keySet().size() + " engines managers..."); //$NON-NLS-1$ //$NON-NLS-2$
		
		boolean result = true;
		for (EngineType et : enginesManagers.keySet()) {
			EnginesManager<?> enginesManager = enginesManagers.get(et);
			try {
				Log.finest(enginesManager.getClass().getSimpleName() + "{0} : starting {1} engine(s)..." + enginesManager.getEngines().size() + " " + enginesManager.getEnginesDescription() + " engine(s)..."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				result = enginesManager.startEngines(monitor) && result; // order is important
			}
			catch (Exception e) {
				Log.severe(NLS.bind(TXMCoreMessages.ErrorWhileStartingEnginesOfTheP0EnginesManager, enginesManager.getEnginesDescription() ));
				e.printStackTrace();
			}
		}
		return result;
	}
	
	public static boolean stopEnginesManagers() {
		
		Log.finest("Stopping engines of the {0}  engines managers..." + enginesManagers.keySet().size() + " engines managers..."); //$NON-NLS-1$ //$NON-NLS-2$
		
		if (enginesManagers == null) return true;
		if (enginesManagers.size() == 0) return true;
		
		boolean result = true;
		HashSet<EngineType> ets = new HashSet<>();
		ets.addAll(enginesManagers.keySet());
		for (EngineType et : ets) {
			EnginesManager<?> enginesManager = enginesManagers.get(et);
			Log.finest(enginesManager.getClass().getSimpleName() + "{0} : stopping {1}  engine(s)..." + enginesManager.getEngines().size() + " " + enginesManager.getEnginesDescription() + " engine(s)..."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			result = result && enginesManager.stopEngines();
			removeEngineManager(enginesManager); // remove the engine manager anyway
		}
		
		return true;
	}
	
	private static boolean startWorkspace(IProgressMonitor monitor) {
		
		String txmhomedir = getPreference(TBXPreferences.USER_TXM_HOME);
		if (txmhomedir == null || txmhomedir.length() == 0) {
			state = false;
			Log.warning("TXM home directory is not set in preferences: "+TBXPreferences.USER_TXM_HOME);
			return false;
		}
		
		Version currentVersion = BundleUtils.getBundleVersion("org.txm.core"); //$NON-NLS-1$
		Log.fine(TXMCoreMessages.bind("Current toolbox version: {0}.", currentVersion)); //$NON-NLS-1$
		String installedVersionString = getPreference(TBXPreferences.VERSION);
		if (installedVersionString.isEmpty()) {
			installedVersionString = "0.0.0"; //$NON-NLS-1$
		}
		Version installedVersion = new Version(installedVersionString);
		Log.fine(TXMCoreMessages.bind("Installed toolbox version: {0}.", installedVersion)); //$NON-NLS-1$
		try {
			// System.out.println("Toolbox.startWorkspace(): workspace location = " + location);
			
			boolean doUpdateworkspace = currentVersion.compareTo(installedVersion) > 0;
			
			if (doUpdateworkspace) { // newer install
				Log.fine("Updating Toolbox workspace..."); //$NON-NLS-1$
				
				// create or update the workspace & create TXM default.xml workspace file
				Toolbox.workspace = createOrUpdate(new File(txmhomedir));
				
				// test
				if (Toolbox.workspace == null) {
					Log.severe(TXMCoreMessages.ErrorWorkspaceNotUpdatedIsNull);
					return false;
				}
				setPreference(TBXPreferences.VERSION, currentVersion.toString());
			}
			else { // already existing workspace
				workspace = Workspace.getInstance();
			}
			
			Log.fine(NLS.bind(TXMCoreMessages.loadingWorkspaceFromFileColonP0, txmhomedir));
			state = true;
			
			if (workspace == null) {
				Log.severe(NLS.bind(TXMCoreMessages.errorLoadingWorkspaceFromFileColonP0, txmhomedir));
				state = false;
				// System.out.println(TXMCoreMessages.FAILED);
				return false;
			}
			else {
				workspace.getChildren().clear();
				// System.out.println(TXMCoreMessages.DONE);
				// updating extension files
				IConfigurationElement[] contributions = Platform.getExtensionRegistry().getConfigurationElementsFor(PostTXMHOMEInstallationStep.EXTENSION_ID);
				// System.out.println("contributions: "+Arrays.toString(contributions));
				Log.fine("Installing extensions ({0})" + contributions.length + ")"); //$NON-NLS-1$ //$NON-NLS-2$
				for (int i = 0; i < contributions.length; i++) {
					try {
						Log.fine(TXMCoreMessages.bind("Installing extentions: {0}...", contributions[i].getName())); //$NON-NLS-1$
						PostTXMHOMEInstallationStep extp = (PostTXMHOMEInstallationStep) contributions[i].createExecutableExtension("class"); //$NON-NLS-1$
						
						if (!extp.install(workspace)) {
							Log.severe(NLS.bind(TXMCoreMessages.FailToInstallExtensionP0FilesInP1, extp.getName(), workspace.getLocation()));
						}
					}
					catch (CoreException e) {
						e.printStackTrace();
					}
				}
			}
		}
		catch (Exception e) {
			Log.warning(NLS.bind(TXMCoreMessages.errorDuringWorkspaceInitializationColonP0, txmhomedir));
			Log.warning(NLS.bind(TXMCoreMessages.errorColonP0, Log.toString(e)));
			Log.printStackTrace(e);
			state = false;
		}
		return workspace != null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		try {
			Toolbox.shutdown();
		}
		finally {
			super.finalize();
		}
	}
	
	/**
	 * Gets a Collator for the specified result.
	 * The collator is created from the parent of the result if exists and if its a TXMObject (from "lang" attribute) otherwise it is created from the default locale.
	 * 
	 * @param result
	 * @return
	 */
	public static Collator getCollator(TXMResult result) {
		Locale locale = Locale.getDefault();
		String lang = result.getStringParameterValue(TBXPreferences.LANG);
		if (lang != null) {
			locale = new Locale(lang);
		}
		Collator collator = Collator.getInstance(locale);
		collator.setStrength(Collator.TERTIARY);
		return collator;
	}
	
	public static String getMetadataEncoding() {
		return getPreference(TBXPreferences.METADATA_ENCODING);
	}
	
	public static String getMetadataColumnSeparator() {
		return getPreference(TBXPreferences.METADATA_COLSEPARATOR);
	}
	
	public static String getMetadataTextSeparator() {
		return getPreference(TBXPreferences.METADATA_TXTSEPARATOR);
	}
	
	public static String getInstallDirectory() {
		//return getPreference(TBXPreferences.INSTALL_DIR);
		return Platform.getInstallLocation().getURL().getFile();
	}
	
	/**
	 * Creates and Update workspace directory.
	 * 
	 * The update part consist of copying files from the install directory
	 *
	 * @param txmhomedir
	 * @return the workspace
	 */
	public static Workspace createOrUpdate(File txmhomedir) {
		try {
			txmhomedir.mkdirs();
			
			if (!txmhomedir.exists()) {
				System.out.println(NLS.bind(TXMCoreMessages.couldNotCreateTXMHOMEDirectoryColonP0, txmhomedir));
				return null;
			}
			
			// create folders
			String createfolders[] = {
					"corpora", "clipboard", //$NON-NLS-1$ //$NON-NLS-2$
					"css", "xsl", "schema", "scripts" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			String deletefolders[] = { "clipboard" }; //$NON-NLS-1$
			
			for (String folder : deletefolders) {
				DeleteDir.deleteDirectory(new File(txmhomedir, folder));
			}
			
			for (String folder : createfolders) {
				new File(txmhomedir, folder).mkdirs();
			}
			
			// if (!xmlfile.exists()) {
			// if (!writeEmptyWorkspaceFile(xmlfile)) {
			// System.out.println("Error while creating workspace definition file: "+xmlfile);
			// }
			// }
			
			String path = getInstallDirectory();
			File installDirectory = new File(path);
			if (!installDirectory.exists()) {
				Log.warning("Error: Workspace cannot found install directory: " + installDirectory.getAbsolutePath()); //$NON-NLS-1$
				return null;
			}
			
			File redistDirectory = new File(installDirectory, "redist"); //$NON-NLS-1$
			if (!redistDirectory.exists()) {
//				Log.warning(NLS.bind(TXMCoreMessages.WarningWorkspaceCannotFoundTheRedistDirectoryP0, redistDirectory.getAbsolutePath()));
			}
			else {
				FileCopy.copyFiles(redistDirectory, txmhomedir);
			}
			
			// // the folders to copy from TXM install dir
			BundleUtils.copyFiles("org.txm.core", "res", "org/txm/xml", "xsl", txmhomedir); //$NON-NLS-1$
			BundleUtils.copyFiles("org.txm.core", "res", "org/txm/xml", "schema", txmhomedir); //$NON-NLS-1$
			BundleUtils.copyFiles("org.txm.core", "res", "org/txm", "css", txmhomedir); //$NON-NLS-1$
			
			workspace = Workspace.getInstance();
			
			return workspace;
		}
		catch (Exception e) {
			Log.warning(NLS.bind(TXMCoreMessages.failedToCopyTXMFilesFromTXMINSTALLDIRToTXMHOMEColonP0, e.getLocalizedMessage()));
			e.printStackTrace();
			return null;
		}
	}
	
	public static ImportEngines getImportEngines() {
		return (ImportEngines) getEngineManager(EngineType.IMPORT);
	}
	
	public static LinkedHashMap<EngineType, EnginesManager<?>> getEngineManagers() {
		return enginesManagers;
	}
	
	/**
	 * Notify all engines about a result state
	 * 
	 * @param r
	 * @param state
	 */
	public static void notifyEngines(TXMResult r, String state) {
		Log.fine("Toolbox.notifyEngines " + r + " -> " + state); //$NON-NLS-1$ //$NON-NLS-2$
		for (EnginesManager<?> em : enginesManagers.values()) {
			if (em == null) continue;
			for (Engine e : em.getEngines().values()) {
				if (e == null) continue;
				e.notify(r, state);
			}
		}
	}
	
	/**
	 * @return the Eclipse RCP configuration directory (osgi.configuration.area)
	 */
	public static String getConfigurationDirectory() {
		return OSGIUtils.getDefault().getConfigurationLocation().getURL().toString().substring(5);
	}
	
	public static void writeStartupCorporaDiagnostics(File checkupResultFile) {
		
		StringBuilder builder = new StringBuilder();
		
		IWorkspace rcpWorkspace = ResourcesPlugin.getWorkspace();
		IProject corporaDirectory = rcpWorkspace.getRoot().getProject("corpora"); //$NON-NLS-1$
		
		if (!corporaDirectory.exists()) {
			builder.append("No 'corpora' not found in workspace."); //$NON-NLS-1$
			return;
		}
		IProject projects[] = rcpWorkspace.getRoot().getProjects();
		
		for (IProject rcpProject : projects) {
			
			if (rcpProject.equals(corporaDirectory)) continue;
			
			String s1 = rcpProject.getLocationURI().toString();
			String s2 = corporaDirectory.getLocationURI().toString();
			
			if (!s1.startsWith(s2)) {
				builder.append(NLS.bind("RCP Project: {0} not in the corpora.\n\n{0} not in the corpora.\n\n", rcpProject.getName())); //$NON-NLS-1$ //$NON-NLS-2$
				continue;
			}
			
			org.txm.objects.Project project = Toolbox.workspace.getProject(rcpProject.getName().toUpperCase());
			
			if (project == null) {
				builder.append(NLS.bind("Corpus Project: {0} not found.\n\n{0} not found.\n\n", rcpProject.getName())); //$NON-NLS-1$ //$NON-NLS-2$
				continue;
			}
			
			CorpusBuild corpus = project.getCorpusBuild(project.getName().toUpperCase());
			if (corpus == null) {
				builder.append(NLS.bind(" no CQP corpus with name: {0}.", project.getName().toUpperCase())); //$NON-NLS-1$
				builder.append("\n  the import was not done or failed"); //$NON-NLS-1$
				builder.append("\n"); //$NON-NLS-1$
				continue;
			}
			
			List<?> errors = corpus.isBuildValid();
			if (errors != null && errors.size() > 0) {
				builder.append(NLS.bind(" corpus built failed with file errors: {0}", errors)); //$NON-NLS-1$
				builder.append("\n  the import or update failed"); //$NON-NLS-1$
				builder.append("\n"); //$NON-NLS-1$
			}
			
			String error = corpus.isReady();
			if (error != null && error.length() > 0) {
				builder.append(NLS.bind(" corpus is not ready: {0}", error)); //$NON-NLS-1$
				builder.append("\n  the import or update failed"); //$NON-NLS-1$
				builder.append("\n"); //$NON-NLS-1$
			}
			
			if (Log.getLevel().intValue() < Level.INFO.intValue()) { // print more details if log level is lower than INFO
				
				builder.append("Project: "+project.getName()+" at "+project.getProjectDirectory()); //$NON-NLS-1$ //$NON-NLS-2$
				builder.append("\n"); //$NON-NLS-1$
				builder.append(" creation: "+project.getCreationDate()); //$NON-NLS-1$
				builder.append("\n"); //$NON-NLS-1$
				builder.append(" update: "+project.getLastComputingDate()); //$NON-NLS-1$
				builder.append("\n"); //$NON-NLS-1$
				builder.append(" 'default' edition: "+project.getEditionDefinition("default")); //$NON-NLS-1$ //$NON-NLS-2$
				builder.append("\n"); //$NON-NLS-1$
				builder.append(" 'facs' edition: "+project.getEditionDefinition("facs")); //$NON-NLS-1$ //$NON-NLS-2$
				builder.append("\n"); //$NON-NLS-1$
				builder.append(" 'preferences': "+TXMPreferences.dumpToString(project.getParametersNodePath())); //$NON-NLS-1$
				builder.append("\n"); //$NON-NLS-1$
				
				builder.append(" Results: "); //$NON-NLS-1$
				builder.append("\n"); //$NON-NLS-1$
				for (TXMResult result : project.getDeepChildren()) {
					builder.append("  "+result.getName()+" ("+result.getResultType()+"): "+result.getSimpleDetails()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					builder.append("\n"); //$NON-NLS-1$
					builder.append("  'preferences': "+TXMPreferences.dumpToString(result.getParametersNodePath())); //$NON-NLS-1$
					builder.append("\n"); //$NON-NLS-1$
				}
				builder.append("\n"); //$NON-NLS-1$
			}
		}
		
		if (builder.length() > 0) {
			IOUtils.write(checkupResultFile, builder.toString());
			Log.warning(NLS.bind(TXMCoreMessages.TXMWasNotCorrectlyClosedSeeTheP0ReportFile, checkupResultFile.getAbsolutePath()));
		}
	}

	public static String getVersion() {
		
		return BundleUtils.getBundleVersion("org.txm.core").toString();
	}
}
