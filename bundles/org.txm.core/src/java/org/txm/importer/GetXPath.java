package org.txm.importer;


import java.io.File;

import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathEvaluationResult;
import javax.xml.xpath.XPathExpressionException;

import net.sf.saxon.Configuration;
import net.sf.saxon.xpath.XPathFactoryImpl;

/**
 * XPath wrapper
 * 
 * Managed types: 
    Boolean -- Boolean.class
    Number -- Number.class
    String -- String.class
    Nodeset -- XPathNodes.class
    Node -- Node.class

 * 
 * @author mdecorde
 *
 */
public class GetXPath {
	
	public static XPath build() {
		
		XPathFactoryImpl xfactory = new XPathFactoryImpl();
		Configuration config = new Configuration();
		config.setLineNumbering(true);
		xfactory.setConfiguration(config);
		
		XPath xpath = xfactory.newXPath();
		xpath.setNamespaceContext(new PersonalNamespaceContext());
		return xpath;
	}
		
	public static XPathEvaluationResult<?> evaluateExpression(XPath xpath, File xmlFile, String xpathstring) throws XPathExpressionException {
		
		return xpath.evaluateExpression(xpathstring, new StreamSource(xmlFile));
	}
	
	public static <T extends Object> T evaluateExpressionToObject(XPath xpath, File xmlFile, String xpathstring, Class<T> type) throws XPathExpressionException {
		
		return xpath.evaluateExpression(xpathstring, new StreamSource(xmlFile), type);
	}
	
	public static Object evaluateToObject(XPath xpath, File xmlFile, String xpathstring, QName qname) throws XPathExpressionException {
		
		return xpath.evaluate(xpathstring, new StreamSource(xmlFile), qname);
	}
	
	public static String evaluateToString(XPath xpath, File xmlFile, String xpathstring) throws XPathExpressionException {
		
		return xpath.evaluate(xpathstring, new StreamSource(xmlFile));
	}
	
	
	public static XPathEvaluationResult<?> evaluateExpression(File xmlFile, String xpathstring) throws XPathExpressionException {
		
		XPath xpath = build();
		
		return xpath.evaluateExpression(xpathstring, new StreamSource(xmlFile));
	}
	
	public static <T extends Object> T evaluateExpressionToObject(File xmlFile, String xpathstring, Class<T> type) throws XPathExpressionException {
		
		XPath xpath = build();

		return xpath.evaluateExpression(xpathstring, new StreamSource(xmlFile), type);
	}
	
	public static Object evaluateToObject(File xmlFile, String xpathstring, QName qname) throws XPathExpressionException {
		
		XPath xpath = build();
		
		return xpath.evaluate(xpathstring, new StreamSource(xmlFile), qname);
	}
	
	public static String evaluateToString(File xmlFile, String xpathstring) throws XPathExpressionException {
		
		XPath xpath = build();
		
		return xpath.evaluate(xpathstring, new StreamSource(xmlFile));
	}
}
