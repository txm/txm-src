// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-08-29 14:23:55 +0200 (jeu., 29 août 2013) $
// $LastChangedRevision: 2519 $
// $LastChangedBy: nilskredens $ 
//
package org.txm.importer.scripts.filters;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.util.LinkedList;
import java.util.List;

// TODO: Auto-generated Javadoc
//private OutputStreamWriter output;
/**
 * Manage a sequence of filter to be applied one after the other.
 */
public class Sequence extends LinkedList<Filter> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1071883268500776713L;

	/** The source. */
	public File source;

	/**
	 * Instantiates a new sequence.
	 */
	public Sequence() { }

	/**
	 * Instantiates a new sequence.
	 *
	 * @param filters the filters
	 */
	public Sequence(List<Filter> filters) {
		for (Filter f : filters) this.push(f);
	}

	/**
	 * set the input file of the sequence and the output file of the sequence
	 * then link all pipe between filters.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 */
	public void SetInFileAndOutFile(String infile, String outfile) {
		for (int i = 1; i < this.size(); i++) {
			// System.out.println(this.get(i)+"1st connect only : "+this.get(i).source);
			this.get(i - 1).Poutput = new PipedWriter();
			this.get(i - 1).output = new BufferedWriter(this.get(i - 1).Poutput);
			this.get(i).Pinput = new PipedReader();
			this.get(i).input = new BufferedReader(this.get(i).Pinput);

			this.get(i).setSource(this.get(i - 1));
		}

		this.getFirst().Pinput = new PipedReader();
		this.getFirst().input = new BufferedReader(this.getFirst().Pinput);
		this.getFirst().SetInSource(new File(infile));

		File f = new File(outfile);
		this.getLast().Poutput = new PipedWriter();
		this.getLast().output = new BufferedWriter(this.getLast().Poutput);
		this.getLast().SetOutSource(f);
	}

	/**
	 * start the sequence and wait for the last filter to finish its job.
	 *
	 * @return true, if successful
	 */
	public boolean proceed() {
		Thread s = null;
		int i = 0;
		for (i = 0; i < this.size(); i++) {
			s = new Thread(this.get(i));
			s.start();
		}
		
		try {
			s.join();
		} catch (InterruptedException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
	
		return true;
	}

	/**
	 * set the encodings for all filters.
	 *
	 * @param entree the entree
	 * @param sortie the sortie
	 */
	public void setEncodages(String entree, String sortie) {
		for (Filter f : this) f.SetEncodages(entree, sortie);
	}

	/**
	 * Clean.
	 */
	public void clean() {
		for (Filter f : this) {
			try {
				f.Disconnect();
			} catch (Exception e) { }
		}
		this.clear();
		System.gc();
	}

	/**
	 * set the lineseparator for all filters.
	 *
	 * @param lineSeparator the new line separators
	 */
	public void setLineSeparators(String lineSeparator) {
		for (Filter f : this) f.lineSeparator = lineSeparator;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		// Get filter Manager
		String ActionHome = System.getProperty("user.home") + "/.txm/import/actions/"; //$NON-NLS-1$ //$NON-NLS-2$
		Manager<Filter> filterManager = new FilterManager(ActionHome);

		// Create a sequence
		Sequence S = new Sequence();

		// Create filters
		Filter F1 = filterManager.get("MinimalFilter"); //$NON-NLS-1$
		Filter F2 = filterManager.get("MinimalFilter"); //$NON-NLS-1$
		Filter F3 = filterManager.get("MinimalFilter"); //$NON-NLS-1$

		// Set Filter's order
		S.add(F1);
		S.add(F2);
		S.add(F3);

		// Create Catalog Manager
		String CatalogHome = System.getProperty("user.home") + "/.txm/import/catalogs/"; //$NON-NLS-1$ //$NON-NLS-2$
		CatalogManager cm = new CatalogManager(CatalogHome);

		// Get Catalog "BFM.clg"
		Catalog c = cm.get("BFM.clg"); //$NON-NLS-1$

		for (File f : c) {
			S.SetInFileAndOutFile(f.getPath(), f.getPath() + "RESULT"); //$NON-NLS-1$

			Object[] arguments1 = {};
			F1.SetUsedParam(arguments1);
			F3.SetUsedParam(arguments1);

			Object[] arguments2 = {};
			F2.SetUsedParam(arguments2);

			S.proceed();
		}

		System.out.println("seq : " + S); //$NON-NLS-1$
		System.out.println(S.getLast().getStringChain());
	}
}
