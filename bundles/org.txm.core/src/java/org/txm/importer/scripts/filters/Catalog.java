// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (Tue, 24 Jan 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.importer.scripts.filters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;

// TODO: Auto-generated Javadoc
/**
 * 
 * aims at handling a Catalog of text, has to be more defined...
 * 
 * @author mdecorde
 * 
 */
public class Catalog extends LinkedList<File> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8648432882133436521L;
	
	/** The source. */
	File source;
	
	/**
	 * Instantiates a new catalog.
	 *
	 * @param path path to the catalog file catalog.clg
	 */
	public Catalog(String path) {
		source = new File(path);
		if (!source.exists()) {
			System.err.println(NLS.bind(TXMCoreMessages.catalogFileNotFoundP0, path)); 
			System.exit(-1);
		}
		
		init();
	}
	
	/**
	 * initialize the file List.
	 */
	private void init() {
		if (source.exists()) {
			BufferedReader input = null;
			try {
				input = new BufferedReader(new FileReader(source));
				
				String filepath;
				File file;
				
				while ((filepath = input.readLine()) != null) {
					file = new File(filepath);
					if (file.exists())
						this.add(file);
				}
				
			} catch (IOException e) {
				System.out.println(e.getLocalizedMessage());
			}
			if (input != null) {
				try {
					input.close();
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Catalog C = new Catalog(
				System.getProperty("user.home") + "/.txm/import/catalogs/test.clg"); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println(C);
		
	}
}