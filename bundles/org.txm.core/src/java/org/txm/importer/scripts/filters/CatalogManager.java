// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.importer.scripts.filters;

import java.io.File;
import java.util.Hashtable;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.io.IOUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class CatalogManager.
 */
public class CatalogManager extends Hashtable<String, Catalog> implements
		Manager<Catalog> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5076240727042878267L;

	/** The home. */
	File home;

	/**
	 * Instantiates a new catalog manager.
	 *
	 * @param path the path
	 */
	public CatalogManager(String path) {
		File home = new File(path);
		if (!home.exists())
			System.out
					.println(NLS.bind(TXMCoreMessages.managerErrorDuringInitializationColonP0NotFound, path)); 
		else {
			File[] list = home.listFiles(IOUtils.HIDDENFILE_FILTER);
			for (File f : list)
				this.put(f.getName(), new Catalog(f.getPath()));
		}
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Manager#GetManageDirectory()
	 */
	@Override
	public String GetManageDirectory() {
		return home.toString();
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Manager#get(java.lang.String)
	 */
	@Override
	public Catalog get(String key) {
		return super.get(key);
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Manager#getIDs()
	 */
	@Override
	public Iterable<String> getIDs() {
		return this.keySet();
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Manager#containsKey(java.lang.String)
	 */
	@Override
	public boolean containsKey(String key) {
		return super.containsKey(key);
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String args[]) {
		String CatalogHome = System.getProperty("user.home") + "/.txm/import/catalogs/"; //$NON-NLS-1$ //$NON-NLS-2$
		CatalogManager m = new CatalogManager(CatalogHome);

		System.out.println(m);
	}

}