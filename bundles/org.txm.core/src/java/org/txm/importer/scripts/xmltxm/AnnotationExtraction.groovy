// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $
//
package org.txm.importer.scripts.xmltxm

import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.stream.*;
import java.net.URL;
import org.txm.importer.filters.*;
// TODO: Auto-generated Javadoc

/**
 * The Class AnnotationExtraction.
 *
 * @author mdecorde
 * 
 * Extract ana tags from a xml-tei-txm file
 * saved into a stand-off file
 */
public class AnnotationExtraction
{
	/** The input data. */
	private def inputData;

	/** The factory. */
	private def factory;

	/** The parser. */
	private XMLStreamReader parser;

	/** The dir. */
	private def dir;

	/** The writer. */
	private Writer writer;

	/** The url. */
	private def url;

	/** The posfile. */
	private def posfile;

	/** The anafile. */
	private def anafile;

	/** The encoding. */
	private def encoding;

	/** The extract types. */
	private List<String> extractTypes;

	/** The resp stmt. */
	private String respStmt;

	/** The solotags. */
	ArrayList<String> solotags;

	/**
	 * initialize.
	 *
	 * @param anafile the path to the xml-tei-txm file
	 * @param posfile the output stand-off file
	 * @param encoding the encoding of the xml-tei-txm file
	 * @param milestones milestones tags (temporary)
	 */
	public AnnotationExtraction(File anafile, File posfile,String encoding,milestones) {
		this.url = anafile.toURI().toURL();
		this.posfile = posfile;
		this.encoding = encoding;
		this.solotags = milestones;

		inputData = url.openStream();
		factory = XMLInputFactory.newInstance();
	}

	/**
	 * process !!!.
	 */
	public void process() {
		assert(extractTypes != null);
		if (this.createOutput(posfile,encoding)) {
			this.writeHead();
			this.writeBody( posfile, encoding);
			this.writeTail();
		}
		writer.close();
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @param encoding the encoding
	 * @return true, if successful
	 */
	private boolean createOutput(File outfile, String encoding){
		try {
			writer = new OutputStreamWriter(new FileOutputStream(outfile) , "UTF-8");
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * set the respStmt tag of the stand-off file by reading the respstmt of the xml-tei-txm file.
	 */
	private void setResp()
	{
		this.respStmt = "";
		boolean begincapture = false;
		String lastopenlocalname= "";

		inputData = url.openStream();
		parser = factory.createXMLStreamReader(inputData);

		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
		{
			String prefix = parser.getPrefix();
			if (prefix == null || prefix == "")
				prefix = "";
			else
				prefix +=":";
			if (event == XMLStreamConstants.START_ELEMENT) {
				if (parser.getLocalName().matches("resp")) {
					String id = parser.getAttributeValue("xml","id")
					if (id == null)
						id = parser.getAttributeValue(null,"id")
					if (extractTypes.contains(id)) {
						this.respStmt = "<respStmt>\n";
						begincapture= true;
					}
				}
				if (begincapture) {
					lastopenlocalname = parser.getLocalName();
					respStmt += ("\n<"+prefix+parser.getLocalName());
					for (int i= 0 ; i < parser.getAttributeCount() ;i++ )
						respStmt += (" "+parser.getAttributeLocalName(i)+"=\""+parser.getAttributeValue(i)+"\"" );
					if (solotags.contains(lastopenlocalname))
						respStmt += ("/>");
					else
						respStmt += (">");
				}
			}
			else if (event == XMLStreamConstants.END_ELEMENT) {
				if (parser.getLocalName().matches("respStmt")) {
					begincapture= false;
					this.respStmt += "\n</respStmt>"
					System.out.println(this.respStmt);
					parser.close();
					inputData.close()
					return;
				} else {
					if (!solotags.contains(parser.getLocalName()))
						if (lastopenlocalname.equals(parser.getLocalName()))
							respStmt += ("</"+prefix+parser.getLocalName()+">");
						else
							respStmt += ("\n</"+prefix+parser.getLocalName()+">");
				}
			} else if (event == XMLStreamConstants.CHARACTERS) {
				String txt = parser.getText().trim();
				respStmt += txt;
			}
		}
		parser.close();
		inputData.close()
		return;
	}

	/**
	 * write the header of the stand-off ile.
	 */
	private void writeHead() {
		this.setResp();

		writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		writer.write("<!DOCTYPE TEI SYSTEM \"tei_bfm_v1.dtd\">\n");
		writer.write("<TEI xmlns:txm=\"http://textometrie.ens-lyon.fr/1.0\">\n");
		writer.write("<teiHeader xml:lang=\"eng\">\n");
		writer.write("<fileDesc>\n");
		writer.write("<titleStmt>\n");
		for (int i=0;i < extractTypes.size(); i++)
			writer.write("<title>"+extractTypes[i]+"</title>\n");
		writer.write(this.respStmt+"\n");
		writer.write("</titleStmt>\n");
		writer.write("<publicationStmt>\n");
		writer.write("<distributor>BFM project - http://bfm.ens-lsh.fr</distributor>\n");
		writer.write("<availability>\n");
		writer.write("<p>(c) 2010 Projet BFM - CNRS/ENS-LSH.\n");
		writer.write("<hi>Conditions d'utilisation</hi> : \n");
		writer.write("Sous licence <ref target=\"http://creativecommons.org/licenses/by-sa/2.0/fr/\">Creative Commons</ref>.\n");
		writer.write("</p>\n");
		writer.write("</availability>\n");
		writer.write("</publicationStmt>\n");
		writer.write("<sourceDesc>\n");
		writer.write("<p>born digital : TXM project - http://textometrie.org</p>\n");
		writer.write("</sourceDesc>\n");
		writer.write("</fileDesc>\n");
		writer.write("</teiHeader>\n");
		writer.write("<text xml:lang=\"fr\" type=\"standoff\">\n");
		writer.write("<body>\n");
		writer.write("<div>\n");
	}

	/**
	 * write the group of ana tags with the attribute type = type.
	 *
	 * @param type the type
	 */
	private void writeGroup(String type)
	{
		System.out.println("write group : "+type);
		inputData = url.openStream();
		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);

		boolean flagAna = false;
		boolean flagForm = false;
		boolean flagW = false;
		String wordid="";
		String vAna="";
		String currentType;

		writer.write("<linkGrp type=\""+type+"\">\n")

		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
		{
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					switch (parser.getLocalName()) {
						case "w":
						wordid = parser.getAttributeValue(null,"id");
						flagW = true;
						break;
						case "ana":
						if (flagW) {
							currentType = (parser.getAttributeValue(null,"type"));
							if (currentType.matches(type)) {
								flagAna = true;
								vAna ="";
							}
						}
						break;
					}
					break;

				case XMLStreamConstants.END_ELEMENT:
					switch (parser.getLocalName()) {
						case "w":
						flagW = false;
						break;

						case "ana":
						if (flagW) {
							writer.write("<link targets=\"#"+wordid+" #"+vAna+"\"/>\n")
							flagAna = false;
						}
						break;
					}
					break;

				case XMLStreamConstants.CHARACTERS:
					if (flagAna)
						vAna += parser.getText().trim();//catch interp text
					break;
			}
		}
		writer.write("</linkGrp>\n");
		parser.close();
		inputData.close();
	}

	/**
	 * write a group per ana type to extract from the anafile.
	 *
	 * @param TTrez the t trez
	 * @param encoding the encoding
	 * @return the java.lang. object
	 */
	private writeBody(File TTrez, String encoding)
	{
		for (String type : extractTypes) {
			writeGroup(type);
		}
	}

	/**
	 * write the tail of the stand-off file = close body, text and TEI tags.
	 */
	private void writeTail()
	{
		writer.write("</div>\n");
		writer.write("</body>\n");
		writer.write("</text>\n");
		writer.write("</TEI>\n");
	}

	/**
	 * define the types to extract.
	 *
	 * @param types the new types to extract
	 */
	public void setTypesToExtract(List<String> types)
	{
		extractTypes = types;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		String rootDir = "~/xml/rgaqcj/"
		new File(rootDir+"/extract/").mkdir()

		File anafile = new File(rootDir+"/injection/","roland-ana.xml")
		File posfile =  new File(rootDir+"/extract/","roland-pos.xml")
		String encoding = "UTF-8"
		//extrait les txm:ana dont la ref est dans cette liste
		List<String> types = ["#t1","#POS"]

		ArrayList<String> milestones = new ArrayList<String>()
		//the tags you want to keep as milestones
		milestones.add("tagUsage")
		milestones.add("pb")
		milestones.add("lb")
		milestones.add("catRef")

		AnnotationExtraction builder = new AnnotationExtraction(anafile,posfile,encoding,milestones)
		builder.setTypesToExtract(types)
		builder.process()
		return
	}
}
