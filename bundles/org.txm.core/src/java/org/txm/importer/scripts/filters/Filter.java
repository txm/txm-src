// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-08-29 14:23:55 +0200 (jeu., 29 août 2013) $
// $LastChangedRevision: 2519 $
// $LastChangedBy: nilskredens $ 
//
package org.txm.importer.scripts.filters;

import groovy.lang.GroovyClassLoader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PipedReader;
import java.io.PipedWriter;

import org.txm.core.messages.TXMCoreMessages;

// TODO: Auto-generated Javadoc
/**
 * The base class to all the filters used to build an import chain. A filter
 * reads from its input, apply some transformation and writes the result to its
 * output.
 * 
 * @author jmague
 * 
 */
public abstract class Filter extends Thread {
	
	/** The classname. */
	String classname;
	
	/** The Constant gcl. */
	public static final GroovyClassLoader gcl = new GroovyClassLoader();
	
	/** The source. */
	public Filter source = null;
	
	/** The Pinput. */
	public PipedReader Pinput = new PipedReader();
	
	/** The Poutput. */
	public PipedWriter Poutput = new PipedWriter();

	/** The input. */
	public BufferedReader input = new BufferedReader(Pinput);
	
	/** The output. */
	public BufferedWriter output = new BufferedWriter(this.Poutput);
	
	/** The line. */
	protected String line = ""; //$NON-NLS-1$
	
	/** The read input. */
	public boolean readInput = true;

	/** The line separator. */
	public String lineSeparator = "\r\n"; //$NON-NLS-1$
	
	/** The encodage. */
	protected String encodage = "UTF-8"; //$NON-NLS-1$
	
	/** The encodage sortie. */
	protected String encodageSortie = "UTF-8"; //$NON-NLS-1$
	
	/** The Thread counter. */
	public Integer ThreadCounter;

	/** The linecounter. */
	public int linecounter = 0;

	/**
	 * Sets the counter.
	 *
	 * @param c the new counter
	 */
	public void setCounter(Integer c) {
		this.ThreadCounter = c;
	}

	/**
	 * Creates the filter from groovy.
	 *
	 * @param path the path
	 * @param classname the classname
	 * @return the filter
	 */
	@SuppressWarnings("unchecked") //$NON-NLS-1$
	public static Filter CreateFilterFromGroovy(String path, String classname) {
		// System.out.println("Creating Filter : "+path+"/"+classname+".groovy");
		try {
			gcl.addClasspath(path + "/"); //$NON-NLS-1$
			Class<Filter> clazz = gcl.parseClass(new File(path
					+ "/" + classname + ".groovy")); //$NON-NLS-1$ //$NON-NLS-2$
			Object aScript = clazz.newInstance();
			Filter myObject = (Filter) aScript;
			myObject.classname = classname;
			return myObject;
		} catch (Exception e) {
			System.err.println(e);
			return null;
		}
	}

	/**
	 * Links the input of this filter to the output of another source filter.
	 * This method builds chains of filters.
	 *
	 * @param source the new source
	 */
	public void setSource(Filter source) {
		// System.out.println("connect "+this.toString()+" to "+source.toString());
		try {
			this.source = source;
			// this.source.Poutput = new PipedWriter();
			// this.Pinput = new PipedReader();
			this.Pinput.connect(source.Poutput);
			this.input = new BufferedReader(this.Pinput);

		} catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			System.exit(-1);
		}
	}

	/**
	 * Must be defined by all Filters. to set their args before launching them
	 *
	 * @param args the args
	 */
	public abstract void SetUsedParam(Object args);

	/**
	 * break all connected pipe.
	 */
	public void Disconnect() {
		try {
			this.input.close();
			this.output.close();
			this.Pinput.close();
			this.Poutput.close();
			this.output = null;
			this.input = null;
			this.Pinput = null;
			this.Poutput = null;
		} catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	/**
	 * set the source file of the filter.
	 *
	 * @param f the f
	 */
	public void SetInSource(File f) {
		try {
			this.source = null;
			this.Pinput = null;
			this.input = new BufferedReader(new InputStreamReader(
					new FileInputStream(f), encodage));

		} catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			System.exit(-1);
		}
	}

	/**
	 * set the out file of the filter.
	 *
	 * @param f the f
	 */
	public void SetOutSource(File f) {
		try {
			this.Poutput = null;
			this.output = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(f), encodageSortie));
		} catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			System.exit(-1);
		}
	}

	/**
	 * set In and Out encodings.
	 *
	 * @param entree the entree
	 * @param sortie the sortie
	 */
	public void SetEncodages(String entree, String sortie) {
		if (entree.length() > 0)
			this.encodage = entree;
		if (sortie.length() > 0)
			this.encodageSortie = sortie;
	}

	/**
	 * execute the filter on the input and write to the output.
	 */
	@Override
	final public void run() {
		before();
		if (readInput) {
			while (this.getLine()) {
				linecounter++;
				filter();
			}
		}
		// System.out.println(this.classname + " : after");
		after();

		// System.out.println("  "+this.classname+" : processed line number : "+linecounter);
		try {
			this.output.close();
		} catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}

		// decrementCounter();
	}

	/**
	 * the method used to get the next line.
	 *
	 * @return the line
	 */
	private boolean getLine() {
		try {
			this.line = this.input.readLine();
			// System.out.println("getted line: "+this.line);
			if (this.line != null)
				return true;
		} catch (IOException e) {
			System.err.println(TXMCoreMessages.readError); 
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
		return false;
	}

	/**
	 * Debug purpose : get String format of the chain from this filter backward
	 * to the ultimate source(s).
	 *
	 * @return the string chain
	 */
	public String getStringChain() {
		return printChain(""); //$NON-NLS-1$
	}

	/**
	 * Prints the chain.
	 *
	 * @param prefix the prefix
	 * @return the string
	 */
	protected String printChain(String prefix) {
		String res = prefix + this.getClass().getName() + "\n"; //$NON-NLS-1$
		if (source != null)
			res += source.printChain(prefix + " "); //$NON-NLS-1$
		return res;
	}

	/**
	 * The method that actually realizes the transformation for each line.
	 *
	 */
	protected void filter() {
		try {
			output.write(line);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
			System.exit(-1);
		}
	}

	/**
	 * Called before {@link #filter()}.
	 *
	 * @return true, if successful
	 */
	protected abstract boolean before();

	/**
	 * After.
	 */
	protected abstract void after();
}