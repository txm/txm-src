// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.importer.scripts.filters;

import java.io.File;
import java.util.Hashtable;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.io.IOUtils;

// TODO: Auto-generated Javadoc
/**
 * Load groovy filters from a directory with the method get("ScriptName") during
 * the initialization, the given path is scanned : each directory found must
 * contains a groovy file with the same name.
 *
 * @author mdecorde
 */
public class FilterManager implements Manager<Filter> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 923334159432679936L;
	
	/** The hashtable. */
	private Hashtable<String, String> hashtable = new Hashtable<String, String>();

	/** The home. */
	File home;

	/**
	 * Instantiates a new filter manager.
	 *
	 * @param path the path
	 */
	public FilterManager(String path) {
		File home = new File(path);
		if (!home.exists())
			System.out
					.println(NLS.bind(TXMCoreMessages.filterManagerErrorColonInitializationP0NotFound, path)); 
		else {
			File[] list = home.listFiles(IOUtils.HIDDENFILE_FILTER);
			for (File f : list)
				if (f.isDirectory()) {
					this.hashtable.put(f.getName(), f.getPath());
				}
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.hashtable.toString();
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Manager#getIDs()
	 */
	@Override
	public Iterable<String> getIDs() {
		return this.hashtable.keySet();
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Manager#get(java.lang.String)
	 */
	@Override
	public Filter get(String key) {
		Filter f = Filter.CreateFilterFromGroovy(this.hashtable.get(key), key);
		return f;
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Manager#GetManageDirectory()
	 */
	@Override
	public String GetManageDirectory() {

		return home.toString();
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Manager#containsKey(java.lang.String)
	 */
	@Override
	public boolean containsKey(String key) {
		return this.hashtable.containsKey(key);
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String args[]) {
		String ActionHome = System.getProperty("user.home") + "/.txm/import/actions/"; //$NON-NLS-1$ //$NON-NLS-2$
		Manager<Filter> m = new FilterManager(ActionHome);

		System.out.println(m);
	}
}