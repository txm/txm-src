// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $
//
package org.txm.importer.scripts.xmltxm

import java.util.ArrayList;
import javax.xml.stream.*;
import java.net.URL;
import org.txm.importer.filters.*;

// TODO: Auto-generated Javadoc
/**
 * The Class AnnotationInliner.
 *
 * @author mdecorde build a xml-tei-txm from a weblex xml file
 * @deprecated
 */

public class WLX2TXMTEI {

	/** The input data. */
	private def inputData;

	/** The factory. */
	private def factory;

	/** The parser. */
	private def parser;

	/** The dir. */
	private def dir;

	/** The output. */
	private def output;

	/** The url. */
	private def url;

	/** The solotags. */
	ArrayList<String> solotags;

	/**
	 * initialize.
	 *
	 * @param url the url to the file to transform
	 * @param solotags temporary list of milestones tags
	 */
	public WLX2TXMTEI(URL url, ArrayList<String> solotags) {
		try {
			this.url = url;
			this.solotags = solotags;
			inputData = url.openStream();
			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		} catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}

	/**
	 * create output FileWriter.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	private boolean createOutput(String dirPathName, String fileName){
		try {
			dir = new File(dirPathName)
			File f = new File(dir, fileName);
			output = new java.io.FileWriter(f)

			return true;
		} catch (Exception e) {
			System.out.println("la?"+e);
			return false;
		}
	}

	/**
	 * process !!!.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	public boolean transfomFile(String dirPathName, String fileName){
		if(createOutput(dirPathName, fileName)){

			int idPb = 0;
			def idLb = "";
			String idLinesuiv;
			int charcount=0;
			String lastopenlocalname= "";

			boolean flagWord = false;
			boolean flagNorm = false;
			String vWordNorm = "";
			String anabalises = "";
			String vP2 = "";
			int idcount=1;
			String targets= "w_fro_"

			try {
				File inputfile = new File(this.url.getFile());
				for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
					switch (event) {
						case XMLStreamConstants.START_ELEMENT:
							switch (parser.getLocalName()) {
								case "w":
								output.write("\n<w xml:id=\""+targets+idcount+"\"");
								idcount++;
								anabalises = "";
								for(int i= 0 ; i < parser.getAttributeCount() ;i++ )
								{
									switch(parser.getAttributeLocalName(i))
									{
										case "p2":
											anabalises += "<txm:ana ref=\"#TT1\" type=\"#t1\">" + parser.getAttributeValue(i)+"</txm:ana>\n"
											break
										case "p3":
											anabalises += "<txm:ana ref=\"#TT1\" type=\"#lemma\">" + parser.getAttributeValue(i)+"</txm:ana>\n"
											break
										case "p4":
											anabalises += "<txm:ana ref=\"#TT1\" type=\"#lasla\">" + parser.getAttributeValue(i)+"</txm:ana>\n"
											break
										case "p5":
											anabalises += "<txm:ana ref=\"#TT1\" type=\"#grace\">" + parser.getAttributeValue(i)+"</txm:ana>\n"
											break
										case "p6":
											anabalises += "<txm:ana ref=\"#TT1\" type=\"#t5\">" + parser.getAttributeValue(i)+"</txm:ana>\n"
											break
										case "p7":
											anabalises += "<txm:ana ref=\"#TT1\" type=\"#tt\">" + parser.getAttributeValue(i)+"</txm:ana>\n"
											break
										case "p8":
											anabalises += "<txm:ana ref=\"#TT1\" type=\"#tt\">" + parser.getAttributeValue(i)+"</txm:ana>\n"
											break
										case "p9":
											anabalises += "<txm:ana ref=\"#TT1\" type=\"#tt\">" + parser.getAttributeValue(i)+"</txm:ana>\n"
											break
										case "p10":
											anabalises += "<txm:ana ref=\"#TT1\" type=\"#tt\">" + parser.getAttributeValue(i)+"</txm:ana>\n"
											break
										case "p11":
											anabalises += "<txm:ana ref=\"#TT1\" type=\"#tt\">" + parser.getAttributeValue(i)+"</txm:ana>\n"
											break
										case "p12":
											anabalises += "<txm:ana ref=\"#TT1\" type=\"#tt\">" + parser.getAttributeValue(i)+"</txm:ana>\n"
											break
										case "p13":
											anabalises += "<txm:ana ref=\"#TT1\" type=\"#tt\">" + parser.getAttributeValue(i)+"</txm:ana>\n"
											break
										default:
											output.write(" "+parser.getAttributeLocalName(i)+"=\""+parser.getAttributeValue(i)+"\"" );
									}
								}

								output.write(">\n");
								flagWord = true;
								break;

								case "TEI":
								output.write("\n<TEI");
								for(int i= 0 ; i < parser.getAttributeCount() ;i++ )
									output.write(" "+parser.getAttributeLocalName(i)+"=\""+parser.getAttributeValue(i)+"\"" );
								output.write(" xmlns:txm=\"http://textometrie.ens-lyon.fr/1.0\"" );
								output.write(">\n");
								break;
								default:
								lastopenlocalname = parser.getLocalName();
								output.write("\n<"+parser.getLocalName());
								for(int i= 0 ; i < parser.getAttributeCount() ;i++ )
									output.write(" "+parser.getAttributeLocalName(i)+"=\""+parser.getAttributeValue(i)+"\"" );
								if(solotags.contains(lastopenlocalname))
									output.write("/>");
								else
									output.write(">");
								charcount=0;
							}
							break;

						case XMLStreamConstants.END_ELEMENT:
							switch (parser.getLocalName()) {
								case "w":
								output.write("<txm:form>"+vWordNorm+"</txm:form>")
								output.write("\n"+anabalises);
								output.write("</w>");
								vWordNorm = "";
								anabalises ="";
								flagWord = false;
								break;
								default:
								//if(charcount > 0)
								//output.write("\n");
								if(!solotags.contains(parser.getLocalName()))
									if(lastopenlocalname.equals(parser.getLocalName()))
										output.write("</"+parser.getLocalName()+">");
									else
										output.write("\n</"+parser.getLocalName()+">");
							}
							break;

						case XMLStreamConstants.CHARACTERS:
							if (flagWord) {

								vWordNorm += parser.getText().trim();

							} else
							{
								String txt = parser.getText().trim();
								output.write(txt);
								charcount += txt.length();
							}
							break;
					}
				}

				output.close();
				parser.close();
				inputData.close();
			}
			catch (XMLStreamException ex) {
				System.out.println(ex);
			}
			catch (IOException ex) {
				System.out.println("IOException while parsing " + inputData);
			}
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		String rootDir = "~/xml/rgaqcj/";
		new File(rootDir+"/anainline/").mkdir();

		def files = ["roland.xml","qgraal_cm.xml","artu.xml","qjm.xml","commyn1.xml","jehpar.xml"];
		def anafiles = ["roland-ana.xml","qgraal_cm-ana.xml","artu-ana.xml","qjm-ana.xml","commyn1-ana.xml","jehpar-ana.xml"];
		ArrayList<String> solotags = new ArrayList<String>();
		solotags.add("tagUsage");
		solotags.add("pb");
		solotags.add("lb");
		solotags.add("catRef");

		for(int i=0; i < files.size();i++)
		{
			String file = files[i];
			String anafile = anafiles[i];
			def builder = new WLX2TXMTEI(new File(rootDir+"/src/",file).toURL(),solotags);
			builder.transfomFile(rootDir+"/anainline/",anafile);
		}


		return
	}
}
