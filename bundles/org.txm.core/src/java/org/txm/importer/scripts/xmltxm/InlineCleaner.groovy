// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-03-29 09:51:35 +0200 (mar. 29 mars 2016) $
// $LastChangedRevision: 3185 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.importer.scripts.xmltxm

import java.text.DateFormat;
import java.util.Date;
import java.util.ArrayList;
import javax.xml.stream.*;
import java.net.URL;
import org.txm.importer.filters.*;

// TODO: Auto-generated Javadoc
/**
 * The Class InlineCleaner.
 *
 * @author mdecorde
 * remove all ana tags of a xml-tei-txm file it supose you have
 * the stand-off version
 */

public class InlineCleaner {
	
	/** The url. */
	private def url;
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private XMLStreamReader parser;

	/** The output. */
	private def output;
	
	/** The solotags. */
	ArrayList<String> solotags;

	/**
	 * Instantiates a new inline cleaner.
	 *
	 * @param url the url
	 * @param outfile the outfile
	 * @param solotags the solotags
	 */
	public InlineCleaner(URL url, File outfile, ArrayList<String> solotags) {
		try {
			this.url = url;
			this.solotags = solotags;
			inputData = url.openStream();
			factory = XMLInputFactory.newInstance();

			parser = factory.createXMLStreamReader(inputData);

			this.transformFile(outfile);

		} catch (XMLStreamException ex) {
			System.out.println(ex);
		} catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput(File outfile) {
		output = new OutputStreamWriter(new FileOutputStream(outfile), "UTF-8");
		return true;
	}

	/**
	 * Transform file.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	public boolean transformFile(File outfile) {
		boolean shouldwrite = true;
		boolean isW = false;
		String lastopenlocalname = "";

		if (!createOutput(outfile))
			return false;

		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser
				.next()) {
			String prefix = parser.getPrefix();
			if (prefix == null)
				prefix = "";
			else
				prefix += ":";

			switch (event) {
			case XMLStreamConstants.START_ELEMENT:
				
				if (parser.getLocalName().equals("w")) {
					isW = true;
				} 
				
				if (parser.getLocalName().equals("ana") && isW) {
					shouldwrite = false;
				} else {
					lastopenlocalname = parser.getLocalName();
					output.write("\n<" + prefix + parser.getLocalName());
					if (parser.getLocalName().equals("TEI"))
						output
								.write(" xmlns:txm=\"http://textometrie.ens-lyon.fr/1.0\"");
					for (int i = 0; i < parser.getAttributeCount(); i++) {
						String attname = parser.getAttributeLocalName(i);
						output.write(" " + attname + "=\""
								+ parser.getAttributeValue(i) + "\"");
					}
					if (solotags.contains(lastopenlocalname))
						output.write("/>");
					else
						output.write(">");
				}
				break;
			case XMLStreamConstants.END_ELEMENT:
				switch (parser.getLocalName()) {
				case "ana":
					if(isW)
						shouldwrite = true;
					break;
				case "w":
					isW = false;
					//do default
				default:
					if (!solotags.contains(parser.getLocalName()))
						if (lastopenlocalname.equals(parser.getLocalName()))
							output.write("</" + prefix + parser.getLocalName()+ ">");
						else
							output.write("\n</" + prefix
									+ parser.getLocalName() + ">");
				}
				break;

			case XMLStreamConstants.CHARACTERS:
				if (shouldwrite) {
					String txt = parser.getText().trim();
					output.write(txt);
				}
				break;
			}
		}
		output.close();
		parser.close();
		inputData.close();
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		String rootDir = "~/xml/rgaqcj/";
		new File(rootDir + "/cleaner/").mkdir();

		ArrayList<String> milestones = new ArrayList<String>();// the tags who stay milestones
		milestones.add("tagUsage");
		milestones.add("pb");
		milestones.add("lb");
		milestones.add("catRef");

		File srcfile = new File(rootDir + "/anainline/", "roland-ana.xml");
		File cleanfile = new File(rootDir + "/cleaner/", "roland-off.xml");

		System.out.println("clean file " + srcfile);
		def builder = new InlineCleaner(srcfile.toURL(), cleanfile, milestones);
		return;
	}

}
