package org.txm.importer.xtz;

import java.io.File;
import java.util.List;

import org.txm.utils.DeleteDir;

/**
 * Takes the XML-TXM files and build an edition
 * 
 * @author mdecorde
 *
 */
public class Pager extends ImportStep {

	protected File htmlDirectory;
	protected String corpusname;
	protected List<String> orderedTextIDs;

	public Pager(ImportModule module, String editionName) {
		super(module);

		corpusname = module.getCorpusName();

		inputDirectory = new File(module.getBinaryDirectory(), "txm/"+module.getCorpusName()); //$NON-NLS-1$
		htmlDirectory = new File(module.getBinaryDirectory(), "HTML/"+corpusname); //$NON-NLS-1$
		outputDirectory  = new File(htmlDirectory, editionName);

		if (!module.isUpdatingCorpus()) {
			DeleteDir.deleteDirectory(outputDirectory);
			outputDirectory.mkdirs();
		}
	}

	@Override
	public void cancel() {
		// TODO Auto-generated method stub

	}

	@Override
	public void process() {
		process(null); // no default files order set
	}
	
	public void process(List<String> orderedTextIDs) {
		this.orderedTextIDs = orderedTextIDs;
	}
}
