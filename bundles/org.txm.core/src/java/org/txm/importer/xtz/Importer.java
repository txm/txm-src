package org.txm.importer.xtz;

import java.io.File;

/**
 * Takes any form of source files
 * 
 * After this step, the XML-TXM files are created.
 * 
 * they are validated before continuing 
 * @author mdecorde
 *
 */
public abstract class Importer extends ImportStep {

	public Importer(ImportModule module) {
		super(module);
		inputDirectory = module.getSourceDirectory();
		outputDirectory = new File(module.getBinaryDirectory(), "txm/"+module.getCorpusName()); //$NON-NLS-1$
		outputDirectory.mkdirs();
	}

	public abstract void checkFiles();
}
