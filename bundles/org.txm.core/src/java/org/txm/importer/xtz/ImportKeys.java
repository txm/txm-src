package org.txm.importer.xtz;

public class ImportKeys {

	public static final String CLEAN = "clean.directories"; //$NON-NLS-1$
	public static final String TTMODEL = "annotate.model"; //$NON-NLS-1$
	public static final String TTANNOTATE = "annotate.run"; //$NON-NLS-1$
	public static final String LANG = "lang"; //$NON-NLS-1$
	
	public static final String MULTITHREAD = "multithread"; //$NON-NLS-1$
	public static final String DEBUG = "debug"; //$NON-NLS-1$
	public static final String UPDATECORPUS = "corpus.update"; //$NON-NLS-1$
	public static final String UPDATECORPUSEDITION = "corpus.update.edition"; //$NON-NLS-1$
	
	public static final String NORMALISEANAVALUES = "normalize.ana.values"; //$NON-NLS-1$
	public static final String NORMALISEATTRIBUTEVALUES = "normalize.attribute.values"; //$NON-NLS-1$
}