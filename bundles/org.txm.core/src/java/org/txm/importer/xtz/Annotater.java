package org.txm.importer.xtz;

import java.io.File;


/**
 * 
 * Takes the XML-TXM files and wrap a TAL Tool to update the XML-TXM files
 * 
 * @author mdecorde
 *
 */
public abstract class Annotater extends ImportStep {

	public Annotater(ImportModule module) {
		super(module);
		
		inputDirectory = new File(module.getBinaryDirectory(), "txm/"+module.corpusName); //$NON-NLS-1$
		outputDirectory = new File(module.getBinaryDirectory(), "txm"); //$NON-NLS-1$
	}
}
