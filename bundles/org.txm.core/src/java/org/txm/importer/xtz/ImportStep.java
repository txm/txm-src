package org.txm.importer.xtz;

import java.io.File;
import java.util.HashMap;

import org.txm.objects.Project;

/**
 * One of the step of an import module
 * 
 * @author mdecorde
 *
 */
public abstract class ImportStep {
	
	protected File inputDirectory, outputDirectory;
	
	protected ImportModule module;
	
	protected Project project;
	
	protected HashMap<String, Object> stepProperties = new HashMap<>();
	
	protected boolean isSuccessFul = false;
	
	protected String reason = "not set."; //$NON-NLS-1$
	
	protected boolean stopAtFirstError = true;
	
	protected boolean debug = true;

	public ImportStep(ImportModule module) {
		this.module = module;
		this.project = module.getProject();
		this.debug = module.debug;
		//TODO split a SubMonitor like in the TXMResult.compute() but only when the ImportStep is starting
	}
	
	public File getInputDirectory() {
		return inputDirectory;
	}
	
	public File getOutputDirectory() {
		return outputDirectory;
	}
	
	public ImportModule getImportModule() {
		return module;
	}
	
	public boolean isSuccessFul() {
		return isSuccessFul;
	}
	
	public String getReason() {
		return reason;
	}
	
	/**
	 * Called when a step is interrupted to clean streams and stuff
	 */
	public abstract void cancel();
	
	public abstract void process();
}
