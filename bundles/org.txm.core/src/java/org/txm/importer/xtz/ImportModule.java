package org.txm.importer.xtz;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.objects.Project;
import org.txm.objects.Text;
import org.txm.utils.DeleteDir;
import org.txm.utils.LogMonitor;
import org.txm.utils.logger.Log;

public class ImportModule {

	public Project project;

	public String corpusVersionProduced;

	public File sourceDirectory;

	public File binaryDirectory;

	public Importer importer;

	public Annotater annotater;

	public Compiler compiler;

	public Pager pager;

	/**
	 * set the variable to false to stop the import process at next step
	 */
	public boolean isSuccessful = true;

	public String reason = "none";

	public boolean debug = false;

	public boolean multithread = false;

	public boolean updateCorpus = false;

	public boolean updateEdition = true;

	public String corpusName;

	IProgressMonitor monitor;

	public void setMonitor(IProgressMonitor monitor) {
		this.monitor = monitor;
	}


	public boolean isMultiThread() {
		return multithread;
	}

	public boolean isDebugging() {
		return debug;
	}

	public ImportModule(Project p) {
		init(p);
	}

	public boolean isUpdatingCorpus() {
		return updateCorpus;
	}

	public boolean isUpdatingEdition() {
		return updateEdition;
	}

	public void init(Project p) {
		this.project = p;

		corpusName = project.getName();
		// this.debug = "true".equals(project.getKeyValueParameters().get(ImportKeys.DEBUG));

		if (Log.getLevel().intValue() < Level.INFO.intValue()) {
			debug = true;
		}
		this.multithread = project.getDoMultiThread();
		this.updateCorpus = project.getDoUpdate();
		this.updateEdition = project.getDoUpdateEdition();


		this.sourceDirectory = project.getSrcdir();
		this.binaryDirectory = project.getProjectDirectory();

		if (!updateCorpus) { // clean directories only if it's a new import
			File txmDir = new File(binaryDirectory, "txm");
			// DeleteDir.deleteDirectory(binaryDirectory);
			try {
				p.getRCPProject().getFolder("HTML").delete(true, new LogMonitor("XTZ delete project content"));
				p.getRCPProject().getFolder("cqp").delete(true, new LogMonitor("XTZ delete project content"));
				p.getRCPProject().getFolder("data").delete(true, new LogMonitor("XTZ delete project content"));
				p.getRCPProject().getFolder("registry").delete(true, new LogMonitor("XTZ delete project content"));
				p.getRCPProject().getFolder("tokenized").delete(true, new LogMonitor("XTZ delete project content"));
				p.getRCPProject().getFolder("txm").delete(true, new LogMonitor("XTZ delete project content"));
				DeleteDir.deleteDirectory(new File(binaryDirectory, "HTML")); //$NON-NLS-1$
				DeleteDir.deleteDirectory(new File(binaryDirectory, "cqp")); //$NON-NLS-1$
				DeleteDir.deleteDirectory(new File(binaryDirectory, "data")); //$NON-NLS-1$
				DeleteDir.deleteDirectory(new File(binaryDirectory, "registry")); //$NON-NLS-1$
				DeleteDir.deleteDirectory(new File(binaryDirectory, "tokenized")); //$NON-NLS-1$
				DeleteDir.deleteDirectory(new File(binaryDirectory, "txm")); //$NON-NLS-1$
			}
			catch (CoreException e) {
				e.printStackTrace();
			}
			txmDir.mkdirs();
		}
	}

	public void start() throws InterruptedException {

		binaryDirectory.mkdirs(); // ensure output exists
		// System.out.println("ImportModule.start");
		if (!updateCorpus) { // create XML-TXM files and annotate
			System.out.println(TXMCoreMessages.creatingCorpus);
			if (importer != null) {
				// System.out.println("ImportModule.start: importer: "+importer);
				if (monitor != null) monitor.subTask("-- IMPORTER - Reading source files");

				System.out.println("-- IMPORTER - Reading source files");
				importer.process();
				// importer.checkFiles();
				isSuccessful = isSuccessful & importer.isSuccessFul();
				if (!isSuccessful) {
					System.out.println("Error while importing corpus during 'importer' step, reason=" + importer.getReason());
					return;
				}
			}
			else {
				System.out.println("XML-TXM files already produced in " + new File(binaryDirectory, "txm/" + corpusName));
			}
			if (monitor != null)  { monitor.worked(25); }
		}

		// update existing Text file pointers
		for (Text text : project.getTexts()) {
			File f = new File(project.getProjectDirectory(), "txm/" + project.getName() + "/" + text.getName() + ".xml"); //$NON-NLS-1$
			text.setTXMFile(f);
		}

		// declare in the right order the new texts produced in the "txm" directory
		for (File build : new File(binaryDirectory, "txm").listFiles()) { //$NON-NLS-1$
			if (!build.isDirectory()) continue;

			for (File xmltxmFile : build.listFiles()) {
				// File xmltxmFile = new File(build, name + ".xml");
				if (xmltxmFile.isDirectory()) continue;
				if (xmltxmFile.isHidden()) continue;
				if (!xmltxmFile.getName().endsWith(".xml")) continue; //$NON-NLS-1$

				String name = xmltxmFile.getName();
				name = name.substring(0, name.length() - 4);

				if (project.getText(name) == null) { // if text does not exists create it
					Text t = new Text(project);
					t.setName(name);
					t.setTXMFile(xmltxmFile);
					t.setDirty();
				}
			}
		}

		// remove Texts without xml-txm files
		for (Text text : project.getTexts()) {
			if (!text.getXMLTXMFile().exists()) {
				text.delete();
			}
		}

		// XML-TXM files are ready to be compiled for the right order
		final List<String> orderedTextIDs = getTXMFilesOrder();

		// set Texts order for further uses in TXM
		for (Text text : project.getTexts()) {
			String name = text.getName();
		}


		if (!updateCorpus) { // create XML-TXM files and annotate
			boolean annotate = project.getAnnotate();
			if (annotate && annotater != null) {
				if (monitor != null) monitor.subTask("-- ANNOTATE - Running NLP tools");
				System.out.println("-- ANNOTATE - Running NLP tools");
				annotater.process();
				isSuccessful = isSuccessful & annotater.isSuccessFul();
				if (!isSuccessful) {
					System.out.println("Error while importing corpus during 'annotate' step, reason=" + annotater.getReason());
					return;
				}
			}
			else {
				// System.out.println("XML-TXM files already annotated.");
			}
		}
		if (monitor != null)  { monitor.worked(25); }


		//		Thread Tcompiler = new Thread("XTZ Compiler - " + project.getSrcdir().getName()) {
		//			
		//			@Override
		//			public void run() {
		if (compiler != null) {
			if (monitor != null) monitor.subTask("-- COMPILING - Building Search Engine indexes");

			System.out.println("-- COMPILING - Building Search Engine indexes");
			compiler.process(orderedTextIDs);
			isSuccessful = isSuccessful & compiler.isSuccessFul();
			if (!isSuccessful) {
				System.out.println("Error while importing corpus during 'compiler' step, reason=" + compiler.getReason());
				return;
			}
		}
		else {
			System.out.println("No CQP index created.");
		}
		if (monitor != null)  { monitor.worked(25); }
		//			}
		//		};

		//		Thread Tpager = new Thread("XTZ Pager - " + project.getSrcdir().getName()) {
		//			
		//			@Override
		//			public void run() {
		if (!updateCorpus || updateEdition) {
			if (pager != null) {
				if (monitor != null) monitor.subTask("-- EDITION - Building editions");

				System.out.println("-- EDITION - Building editions");
				pager.process(orderedTextIDs);
				isSuccessful = isSuccessful & pager.isSuccessFul();
				if (!isSuccessful) {
					System.out.println("Error while importing corpus during 'pager' step, reason=" + pager.getReason());
					return;
				}
			}
			else {
				System.out.println("No edition produced.");
			}
		}
		if (monitor != null) { monitor.worked(25); }
		//			}
		//		};
		//		
		//		Tcompiler.start();
		//		Tcompiler.join();
		//		if (!isSuccessful) { // don't call pager is compiler step failed
		//			return;
		//		}
		//		Tpager.start();
		//		Tpager.join();
		//		

		if (isSuccessful) { // all done TODO remove this code when Text._compute() will be implemented
			for (Text t : project.getTexts()) {
				t.compute(false);
			}

			project.setDoUpdate(false, false);
		}
	}

	protected List<String> getTXMFilesOrder() {
		// //System.out.println("DEFAULT FILES ORDER");
		File txmDirectory = new File(binaryDirectory, "txm/" + corpusName); //$NON-NLS-1$
		ArrayList<File> files = new ArrayList<>(Arrays.asList(txmDirectory.listFiles(new FileFilter() {

			@Override
			public boolean accept(File file) {
				return file.isFile() && file.getName().endsWith(".xml"); //$NON-NLS-1$
			}
		})));

		Collections.sort(files);
		ArrayList<String> ids = new ArrayList<>();
		for (File f : files) {
			String name = f.getName();
			ids.add(name.substring(0, name.length() - 4));
		}

		return ids;
		// return project.getTextsID();
		// return files;
	}

	public void end() {
		File paramFile = new File(binaryDirectory, "import.xml"); //$NON-NLS-1$
		try {
			// DomUtils.save(project.root.getOwnerDocument(), paramFile);
			project.saveParameters(true);
			isSuccessful = true;
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			isSuccessful = false;
		}
	}

	public String getCorpusName() {
		return corpusName;
	}

	public String getReason() {
		return reason;
	}

	public boolean isSuccessFul() {
		return isSuccessful;
	}

	public Project getProject() {
		return project;
	}

	public File getSourceDirectory() {
		return sourceDirectory;
	}

	public File getBinaryDirectory() {
		return binaryDirectory;
	}

	public void process() throws InterruptedException {
		start();
		if (isSuccessful)
			end();
	}

	public static void main(String[] args) {
		// File projectFile = new File("/home/mdecorde/xml/brown/import.xml");
		//
		// ImportModule module = new ImportModule(projectFile);
		// System.out.println("Parameters: "+module.getParameters());
		// try {
		// module.start();
		//
		// if (module.isSuccessful) {
		// System.out.println("Import sucessful. reloading corpora...");
		// } else {
		// System.out.println("Import failed, reason = "+module.getReason());
		// }
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
	}
}
