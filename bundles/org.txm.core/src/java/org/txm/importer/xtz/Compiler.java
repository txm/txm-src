package org.txm.importer.xtz;

import java.io.File;
import java.util.List;

import org.txm.utils.DeleteDir;

/**
 * Takes XML-TXM files, build the CQP files and call cwb utils
 * 
 * @author mdecorde
 *
 */
public abstract class Compiler extends ImportStep {

	protected File cqpDirectory, registryDirectory, dataDirectory;
	protected List<String> orderedTextIDs;

	/**
	 * Creates the output directories
	 * 
	 * @param module
	 */
	public Compiler(ImportModule module) {
		super(module);

		inputDirectory = new File(module.getBinaryDirectory(), "txm/"+module.getCorpusName()); //$NON-NLS-1$
		cqpDirectory = new File(module.getBinaryDirectory(), "cqp"); //$NON-NLS-1$
		outputDirectory = new File(module.getBinaryDirectory(), "data"); //$NON-NLS-1$
		dataDirectory = new File(outputDirectory, module.getCorpusName());
		registryDirectory = new File(module.getBinaryDirectory(), "registry"); //$NON-NLS-1$

		if (!module.isUpdatingCorpus()) {
			DeleteDir.deleteDirectory(cqpDirectory);
		} 
		cqpDirectory.mkdir();
	}

	@Override
	public void cancel() {
		// TODO Auto-generated method stub
	}
	
	/**
	 * implement this method to build CQP files, CQP data and registry file
	 */
	protected abstract void _process() ;

	/**
	 * called by ImportModule
	 * 
	 */
	@Override
	public void process() {
		process(null);
	}
	
	/**
	 * called by ImportModule
	 * 
	 * @param orderedTextIDs
	 */
	public void process(List<String> orderedTextIDs) {
		this.orderedTextIDs = orderedTextIDs;
		_process();
	}
}
