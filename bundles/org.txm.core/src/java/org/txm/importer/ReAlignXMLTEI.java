package org.txm.importer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * Re align a XML TEI file with a TAL tool result. The TAL result might change: words boundaries, words properties, an element.
 * 
 * 
 * 
 * @author mdecorde
 *
 */
public class ReAlignXMLTEI extends StaxIdentityParser {
	
	String elementToPatch;
	
	protected URL patchUrl;
	
	protected InputStream patchInputData;
	
	protected XMLStreamReader patchParser;
	
	public ReAlignXMLTEI(File origFile, File patchFile, String elementToPatch) throws XMLStreamException, IOException {
		super(origFile);
		this.elementToPatch = elementToPatch;
		this.patchUrl = patchFile.toURI().toURL();
		
		this.factory = XMLInputFactory.newInstance();
		
		this.patchInputData = patchUrl.openStream();
		this.patchParser = factory.createXMLStreamReader(patchInputData);
	}
	
	@Override
	public boolean process(File outfile) throws XMLStreamException, IOException {
		boolean ret = super.process(outfile);
		
		if (patchParser != null) {
			try {
				patchParser.close();
			}
			catch (Exception e) {
				System.out.println("patchParser excep: " + e);
			}
		}
		
		if (patchInputData != null) {
			try {
				patchInputData.close();
			}
			catch (Exception e) {
				System.out.println("patchInputData excep: " + e);
			}
		}
		
		return ret;
	}
}
