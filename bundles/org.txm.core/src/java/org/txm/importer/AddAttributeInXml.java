package org.txm.importer;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.txm.metadatas.Entry;
import org.txm.utils.Pair;

/**
 * Add a attribute value map in a XML file
 * Warning: if an attribute already exists its value won't be changed
 */
public class AddAttributeInXml extends StaxIdentityParser {
	
	File xmlfile;
	String tag;
	List<Entry> attributes;
	HashMap<String, String> attributesMap;
	boolean result;

//	public AddAttributeInXml(File xmlfile, String tag, List<Pair<String, String>> attributes) throws MalformedURLException, IOException, XMLStreamException
//	{
//		super(xmlfile.toURI().toURL());
//		this.xmlfile = xmlfile;
//		this.tag = tag;
//		this.attributes = attributes;
//	}

	public AddAttributeInXml(File xmlfile, String tag, HashMap<String, String> attributesMap) throws MalformedURLException, IOException, XMLStreamException {
		super(xmlfile.toURI().toURL());
		this.xmlfile = xmlfile;
		this.tag = tag;
		this.attributesMap = attributesMap;
	}

	public AddAttributeInXml(File xmlfile, String tag,	ArrayList<org.txm.metadatas.Entry> attributes) throws MalformedURLException, IOException, XMLStreamException {
		super(xmlfile.toURI().toURL());
		this.xmlfile = xmlfile;
		this.tag = tag;
		this.attributes = attributes;
	}

	public boolean process(File outfile) throws XMLStreamException, IOException {
		this.result = false;
		boolean ret = super.process(outfile);
		return this.result & ret;
	}

	/**
	 * Rewrite the processStartElement() to update/add attributes
	 * @throws XMLStreamException 
	 */
	public void processStartElement() throws XMLStreamException
	{
		String prefix = parser.getPrefix();
		//TODO: uncomment for TXM 0.7.6 and later
		//			if (INCLUDE == localname && XI == prefix) {
		//				processingXInclude();
		//				return;
		//			}

		if (prefix.length() > 0)
			writer.writeStartElement(Nscontext.getNamespaceURI(prefix), localname);
		else
			writer.writeStartElement(localname);

		for (int i = 0 ; i < parser.getNamespaceCount() ; i++) {
			writer.writeNamespace(parser.getNamespacePrefix(i), parser.getNamespaceURI(i));
		}

		HashSet<String> exists = new HashSet<String>();
		for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
			exists.add(parser.getAttributeLocalName(i));
			writer.writeAttribute(parser.getAttributeLocalName(i), parser.getAttributeValue(i));
		}
		if (localname == tag && !stopInsertion) { // insert here
			result = true;;
			if (attributes != null) {
				for (Pair<String, String> meta : attributes) {
					if (stopAtFirst)
						stopInsertion = true;
					if (!exists.contains(meta.getFirst())) {// I can add the new attribute
						writer.writeAttribute(meta.getFirst(), meta.getSecond());
					}
				}
			} else if (attributesMap != null) {
				for (String name : attributesMap.keySet()) {
					if (stopAtFirst)
						stopInsertion = true;
					if (!exists.contains(name)) {// I can add the new attribute
						writer.writeAttribute(name, attributesMap.get(name));
					}
				}
			}
		}
	}

	boolean stopAtFirst = false;
	boolean stopInsertion = false;
	public void onlyOneElement() {
		stopAtFirst = true;
	}

	public static void main(String[] args)
	{
		try {
			File xmlfile = new File("xml/xmlçàé/essai.xml"); //$NON-NLS-1$
			File temp = new File(xmlfile.getAbsolutePath()+"-rez.xml"); //$NON-NLS-1$
			String tag = "titre"; //$NON-NLS-1$
			HashMap<String, String> metadatas = new HashMap<String, String>();
			metadatas.put("name1", "tricky value\""); //$NON-NLS-1$
			metadatas.put("name2", "value&2"); //$NON-NLS-1$
			metadatas.put("name3", "value<3"); //$NON-NLS-1$
			metadatas.put("name4", "value>"); //$NON-NLS-1$

			AddAttributeInXml builder = new AddAttributeInXml(xmlfile, tag, metadatas);
			builder.onlyOneElement();
			if (builder.process(temp)) {
				System.out.println("Done: ok."); //$NON-NLS-1$
			} else {
				System.out.println("Error during process"); //$NON-NLS-1$
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
