// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2011-06-03 12:37:57 +0200 (ven., 03 juin 2011) $
// $LastChangedRevision: 1867 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.importer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.logger.Log;
import org.xml.sax.SAXException;

import groovy.util.Node;
import groovy.xml.XmlParser;

/**
 * Validate a XML file with a Stax parser.
 *
 * @author mdecorde
 */
public class ValidateXml {

	/**
	 * @throws XMLStreamException test a String, not implemented.
	 *
	 * @return true, if successful
	 */
	public static boolean testAndThrow(File infile) throws XMLStreamException {
		if (infile.isDirectory()) {
			System.out.println(NLS.bind(TXMCoreMessages.xmlValidationColonP0IsADirectory, infile));
			return false;
		}
		if (!infile.exists()) {
			System.out.println(NLS.bind(TXMCoreMessages.xmlValidationColonP0DoesNotExists, infile));
			return false;
		}
		if (!infile.canRead()) {
			System.out.println(NLS.bind(TXMCoreMessages.xmlValidationColonP0IsNotReadable, infile));
			return false;
		}
		try {
			URL url = infile.toURI().toURL();
			InputStream inputData = url.openStream();
			XMLInputFactory factory = XMLInputFactory.newInstance();
			XMLStreamReader parser = factory.createXMLStreamReader(inputData);
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {

			}
			parser.close();
			inputData.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	/**
	 * test a content stored in a String, not implemented.
	 *
	 * @param xmldata the xmldata
	 * @return true, if successful
	 */
	public static boolean test(String xmldata) {
		return testAndGetError(xmldata).length() == 0;
	}

	/**
	 * test a content stored in a String, not implemented.
	 *
	 * @param xmldata the xmldata
	 * @return true, if successful
	 */
	public static String testAndGetError(String xmldata) {
		try {
			XmlParser parser = new XmlParser();
			Node doc = parser.parseText(xmldata);
			return ""; //$NON-NLS-1$
		}
		catch (IOException | ParserConfigurationException e) {
			return e.getMessage();
		}
		catch (SAXException ex) {
			return getXMLError(null, ex);
		}
	}

	/**
	 * Parse an exception 
	 * @param file
	 * @param e
	 * @return
	 */
	private static String getXMLError(String file, Exception e) {
		String message = e.getMessage();

		Matcher msgParse = null;
		if (message.startsWith("ParseError at")) { //$NON-NLS-1$
			Pattern p = Pattern.compile("(ParseError at \\[row,col\\]:\\[)([0-9]+),([0-9]+)\\]\n" + "Message: (.+)."); //$NON-NLS-1$
			msgParse = p.matcher(message); // getMessageAndLocation
		} else {
			Pattern p = Pattern.compile("^(.*) lineNumber: ([0-9]+); columnNumber: ([0-9]+); (.*).$"); //$NON-NLS-1$
			msgParse = p.matcher(message); // getMessageAndLocation
		}

		if (msgParse.matches() && msgParse.groupCount() == 4) {
			String lineNumber = msgParse.group(2);
			String colNumber = msgParse.group(3);
			String errMsg = msgParse.group(4);

			return TXMCoreMessages.bind("** {0} [line {1}, character {2}]: {3}.", file, lineNumber, colNumber, errMsg);
		} else {
			return message;
		}
	}

	/**
	 * test a file.
	 *
	 * @param infile
	 *            the infile
	 * @return false if the file is not valid
	 */
	public static boolean test(File infile) {
		if (infile.isDirectory()) {
			Log.warning(NLS.bind(TXMCoreMessages.xmlValidationColonP0IsADirectory, infile));
			return false;
		}
		if (!infile.exists()) {
			Log.warning(NLS.bind(TXMCoreMessages.xmlValidationColonP0DoesNotExists, infile));
			return false;
		}
		if (!infile.canRead()) {
			Log.warning(NLS.bind(TXMCoreMessages.xmlValidationColonP0IsNotReadable, infile));
			return false;
		}
		
		try {
			URL url = infile.toURI().toURL();
			InputStream inputData = url.openStream();
			XMLInputFactory factory = XMLInputFactory.newInstance();
			XMLStreamReader parser = factory.createXMLStreamReader(inputData);
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {

			}
			parser.close();
			inputData.close();
			return true;
		}
		catch (Exception e) {
			Log.warning(getXMLError(infile.toString(), e));
			return false;
		}
	}

	/**
	 * small test to verify a file is a XML-TEI file.
	 *
	 * @param infile the file to test
	 * @return false if the file is not valid
	 */
	public static boolean teiTest(File infile) {
		if (infile.isDirectory()) {
			System.out.println(NLS.bind(TXMCoreMessages.xmlValidationColonP0IsADirectory, infile));
			return false;
		}
		if (!infile.exists()) {
			System.out.println(NLS.bind(TXMCoreMessages.xmlValidationColonP0DoesNotExists, infile));
			return false;
		}
		if (!infile.canRead()) {
			System.out.println(NLS.bind(TXMCoreMessages.xmlValidationColonP0IsNotReadable, infile));
			return false;
		}
		try {
			URL url = infile.toURI().toURL();
			InputStream inputData = url.openStream();
			XMLInputFactory factory = XMLInputFactory.newInstance();
			XMLStreamReader parser = factory.createXMLStreamReader(inputData);
			int n = 0;
			int tei = -1;
			int text = -1;
			int teiHeader = -1;
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				if (event == XMLStreamConstants.START_ELEMENT) {
					String name = parser.getLocalName();
					if ("TEI".equals(name)) { //$NON-NLS-1$
						tei = n++;
					}
					else if ("text".equals(name)) { //$NON-NLS-1$
						text = n++;
					}
					else if ("teiHeader".equals(name)) { //$NON-NLS-1$
						teiHeader = n++;
					}
				}
			}
			parser.close();
			inputData.close();

			boolean test = n == 3 && tei < teiHeader && teiHeader < text;
			if (test) {
				return true;
			}
			else {
				if (n != 3) {
					if (tei == -1) {
						System.out.println(infile.getName() + ": No 'TEI' tag found");
					}
					if (teiHeader == -1) {
						System.out.println(infile.getName() + ": No 'teiHeader' tag found");
					}
					if (text == -1) {
						System.out.println(infile.getName() + ": No 'text' tag found");
					}
				}
				else {
					System.out.println(infile.getName() + ": Malformed TEI XML");
				}
				return false;
			}
		}
		catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		
		for (File file : new File(System.getProperty("user.home"), "xml/teierrors").listFiles()) { //$NON-NLS-1$
			if (ValidateXml.test(file)) {
				System.out.println("OK: " + file); //$NON-NLS-1$
			}
			else {
				System.out.println("ERROR: " + file); //$NON-NLS-1$
			}
		}
	}
}
