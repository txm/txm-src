package org.txm.importer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.txm.utils.io.IOUtils;

/**
 * Read an XML file and find out : XML elements, their attributes and recursivity level
 * names are lowercases
 * 
 * @author mdecorde
 *
 */
public class SAttributesListener {
	
	public HashMap<String, HashSet<String>> structs = new HashMap<>();
	
	public HashSet<String> anatypes = new HashSet<>();
	
	public HashMap<String, Integer> structsCountProf = new HashMap<>();
	
	public HashMap<String, Integer> structsMaxProf = new HashMap<>();
	
	private String structPath = "/"; //$NON-NLS-1$
	
	private XMLStreamReader parser;
	
	SAttributesListener() {
		structs = new HashMap<>();
		structsCountProf = new HashMap<>();
		structsMaxProf = new HashMap<>();
		structPath = "/";
		anatypes = new HashSet<>(); // store scanned word attributes
	}
	
	SAttributesListener(XMLStreamReader parser) {
		this();
		this.parser = parser;
	}
	
	public void appendResultsTo(SAttributesListener another) {
		structs = another.structs;
		structsCountProf = another.structsCountProf;
		structsMaxProf = another.structsMaxProf;
		anatypes = another.anatypes; // store scanned word attributes
	}
	
	public void start(XMLStreamReader parser) {
		this.parser = parser;
	}
	
	String W = "w"; //$NON-NLS-1$
	String ANA = "ana"; //$NON-NLS-1$
	String FORM = "form"; //$NON-NLS-1$
	String TEXT = "text"; //$NON-NLS-1$
	String TYPE = "type"; //$NON-NLS-1$
	
	/**
	 * Call this method for each START_ELEMENT stax event
	 * 
	 * @param localname the element localname
	 */
	public void startElement(String localname) {
		localname = localname.toLowerCase();
		
		// String localname = parser.getLocalName();
		if (localname.equals(W)) return;
		if (localname.equals(ANA)) return;
		if (localname.equals(FORM)) return;
		
		structPath += localname + "/"; //$NON-NLS-1$
		// println "add: "+structPath
		HashSet<String> attrs = structs.get(localname);
		if (!structs.containsKey(localname)) {
			attrs = new HashSet<>();
			structs.put(localname, attrs);
			structsCountProf.put(localname, 0);
			structsMaxProf.put(localname, 0);
		} // else {
		
		// get structure recursion
		int prof = structsCountProf.get(localname) + 1;
		structsCountProf.put(localname, prof);
		if (structsMaxProf.get(localname) < prof) {
			structsMaxProf.put(localname, prof);
		}
		
		// get the structure attributes
		for (int i = 0; i < parser.getAttributeCount(); i++) {
			attrs.add(parser.getAttributeLocalName(i).toLowerCase());
		}
	}
	
	/**
	 * Call this method for each END_ELEMENT stax event
	 * 
	 * @param localname the element localname
	 */
	public void endElement(String localname) {
		localname = localname.toLowerCase();
		// String localname = parser.getLocalName();
		if (localname.equals(W)) return;
		if (localname.equals(ANA)) return;
		if (localname.equals(FORM)) return;
		
		if (structPath.length() > 1) {
			int idx = structPath.lastIndexOf("/"); //$NON-NLS-1$
			if (idx > 0) {
				structPath = structPath.substring(0, idx);
				// println "end of $localname "+(structsCountProf.get(localname))
				// if (structsCountProf.get(localname) != null)
				structsCountProf.put(localname, structsCountProf.get(localname) - 1);
			}
			// println "pop: "+structPath
		}
	}
	
	// boolean firstGetStructs = true;
	public HashMap<String, HashSet<String>> getStructs() {
		if (structsMaxProf.containsKey("div")) { //$NON-NLS-1$
			if (structsMaxProf.get("div") > 0) //$NON-NLS-1$
				structs.remove("div1"); //$NON-NLS-1$
			if (structsMaxProf.get("div") > 1) //$NON-NLS-1$
				structs.remove("div2"); //$NON-NLS-1$
			if (structsMaxProf.get("div") > 2) //$NON-NLS-1$
				structs.remove("div3"); //$NON-NLS-1$
			if (structsMaxProf.get("div") > 3) //$NON-NLS-1$
				structs.remove("div4"); //$NON-NLS-1$
			if (structsMaxProf.get("div") > 4) //$NON-NLS-1$
				structs.remove("div5"); //$NON-NLS-1$
			if (structsMaxProf.get("div") > 5) //$NON-NLS-1$
				structs.remove("div6"); //$NON-NLS-1$
		}
		// if (firstGetStructs) {
		// firstGetStructs = false;
		// fix min&maj names for CQP
		ArrayList<String> keys = new ArrayList<>();
		keys.addAll(structs.keySet());
		for (String key : keys) {
			HashSet<String> value = structs.get(key);
			structs.remove(key);
			structs.put(key.toLowerCase(), value);
		}
		// }
		
		return structs;
	}
	
	boolean firstGetstructsCountProf = true;
	
	public HashMap<String, Integer> getProfs() {
		
		// if (firstGetstructsCountProf) {
		// firstGetstructsCountProf = false;
		// def keys = []
		// keys.addAll(structsCountProf.keySet());
		// for( String key : keys) {
		// def value = structsCountProf.get(key);
		// structsCountProf.remove(key)
		// structsCountProf.put(key.toLowerCase(), value);
		// }
		// }
		HashMap<String, Integer> clone = new HashMap<>();
		for (String key : structsMaxProf.keySet()) {
			if (structsMaxProf.get(key) > 0)
				clone.put(key, structsMaxProf.get(key) - 1);
			else
				clone.put(key, 0);
		}
		return clone;
	}
	
	public void initialize(ArrayList<String> pattributes, HashMap<String, HashSet<String>> sAttributesMap, HashMap<String, Integer> sAttributesProfs) {
		this.anatypes.addAll(pattributes);
		for (String s : sAttributesMap.keySet()) {
			this.structsMaxProf.put(s, sAttributesProfs.get(s));
			this.structsCountProf.put(s, 0);
			this.structs.put(s, sAttributesMap.get(s));
		}
	}
	
	public HashSet<String> getAnatypes() {
		return anatypes;
	}
	
	// public SAttributesListener scanFile(File xmlFile) throws MalformedURLException, IOException, XMLStreamException {
	// return scanFile(xmlFile, this);
	// }
	
	public void setParser(XMLStreamReader parser) {
		this.parser = parser;
	}
	
	/**
	 * Merge results in the parentListener
	 * 
	 * @param xmlFile
	 * @return
	 * @throws IOException
	 * @throws MalformedURLException
	 * @throws XMLStreamException
	 */
	public SAttributesListener scanFile(File xmlFile) throws MalformedURLException, IOException, XMLStreamException {
		
		boolean startText = false;
		boolean startWord = false;
		InputStream inputData = xmlFile.toURI().toURL().openStream();
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLStreamReader parser = factory.createXMLStreamReader(inputData);
		
		// SAttributesListener listener;
		// if (parentListener != null) {
		// listener = parentListener;
		// listener.setParser(parser);
		// } else {
		// listener = new SAttributesListener(parser);
		// }

		// HashSet<String> types = new HashSet<String>();
		this.setParser(parser);
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			if (event == XMLStreamConstants.START_ELEMENT) { // start elem
				if (TEXT.equals(parser.getLocalName())) startText = true;
				
				if (startText) this.startElement(parser.getLocalName());
				
				if (this.W.equals(parser.getLocalName())) {
					startWord = true;
				}
				else if (startWord && ANA.equals(parser.getLocalName())) { // ana elem
					for (int i = 0; i < parser.getAttributeCount(); i++) { // find @type
						if (TYPE.equals(parser.getAttributeLocalName(i))) { // @type
							this.anatypes.add(parser.getAttributeValue(i).substring(1)); // remove the #
							break;
						}
					}
				}
			}
			else if (event == XMLStreamConstants.END_ELEMENT) { // end elem
				if (startText) this.endElement(parser.getLocalName());
				if (TEXT.equals(parser.getLocalName())) startText = false;
				
				if (this.W.equals(parser.getLocalName())) {
					startWord = false;
				}
			}
		}
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
		
		return this;
	}
	
	/**
	 * scan the XML files of a directory to list the structures with their properties and levels. Also list the word properties
	 * 
	 * @param xmlDirectory
	 * @param wordTag
	 * @return
	 * @throws XMLStreamException
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public static SAttributesListener scanFiles(File xmlDirectory, String wordTag) throws MalformedURLException, IOException, XMLStreamException {
		SAttributesListener listener = new SAttributesListener();
		listener.W = wordTag;
		for (File xmlFile : xmlDirectory.listFiles(IOUtils.HIDDENFILE_FILTER)) {
			if (xmlFile.isFile() && !xmlFile.isHidden() && xmlFile.getName().toLowerCase().endsWith(".xml")) { //$NON-NLS-1$
				listener.scanFile(xmlFile); // results saved in 'listener' data
				// println "LISTENER RESULT with ${xmlFile.getName()}: "+listener
				// println " prof: "+listener.getStructs()
				// println " prof: "+listener.getProfs()
				// println " path: "+listener.structPath
			}
		}
		
		return listener;
	}
}
