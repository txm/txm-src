// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.importer;

import java.io.File;
import java.io.FileOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

/**
 * The Class ResolveXInclude.
 */
class ResolveXInclude {
	
	/**
	 * Resolve.
	 *
	 * @param xmlfile the xmlfile
	 * @param outfile the outfile
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean resolve(File xmlfile, File outfile) throws Exception {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		//factory.setXIncludeAware(true);
		factory.setNamespaceAware(true);
		DocumentBuilder docBuilder = factory.newDocumentBuilder();
//		if (!docBuilder.isXIncludeAware()) {
//			throw new IllegalStateException();
//		}
		/*docBuilder.setEntityResolver(new EntityResolver() {
					@Override
					public InputSource resolveEntity(String publicId, String systemId)
					throws SAXException, IOException {
						if (systemId.endsWith("include.txt")) {
							return new InputSource(includeStream);
						}
						return null;
					}
				});*/
		Document doc = docBuilder.parse(xmlfile);
		// print result
		Source source = new DOMSource(doc);
		Result result = new StreamResult(new FileOutputStream(outfile));
		TransformerFactory transformerFactory = new net.sf.saxon.TransformerFactoryImpl();
		Transformer transformer;

		transformer = transformerFactory.newTransformer();
		transformer.transform(source, result);		
		return true;
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		
		String userdir= System.getProperty("user.home"); //$NON-NLS-1$
		File xmlfile = new File(userdir, "xml/queen/queen/Texts/driver.xml"); //$NON-NLS-1$
		File outfile = new File(userdir, "xml/queen/queen.xml"); //$NON-NLS-1$
		try {
			ResolveXInclude.resolve(xmlfile, outfile);
		} catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			System.out.println("Fail to resolve xinclude"); //$NON-NLS-1$
		}
	}
}
