package org.txm.importer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.txm.objects.Project;
import org.txm.utils.logger.Log;
import org.txm.utils.xml.DomUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Read and save a import parameter file
 * 
 * The file is read to fill the ImportFormEditor > CorpusPage widgets
 * 
 * @author mdecorde
 *
 */
public class ImportFileManager {
	
	Document doc;
	
	File importFile;
	
	/**
	 * one map per import section
	 */
	LinkedHashMap<String, LinkedHashMap<String, String>> sections = new LinkedHashMap<>();
	
	public ImportFileManager(File importFile) {
		this.importFile = importFile;
	}
	
	public LinkedHashMap<String, LinkedHashMap<String, String>> getSections() {
		return sections;
	}
	
	public LinkedHashMap<String, String> getSection(String name) {
		if (name == null) return null;
		return sections.get(name);
	}
	
	/**
	 * Load values from the import.xml file set
	 */
	public String load() {
		try {
			doc = DomUtils.load(importFile);
			
			String name = doc.getDocumentElement().getLocalName();
			if (!"import".equals(name)) { //$NON-NLS-1$
				return "Not a import parameter file: " + importFile;
			}
			String version = doc.getDocumentElement().getAttribute("version"); //$NON-NLS-1$
			if ("0.7.9".equals(version)) { //$NON-NLS-1$
				loadFrom079();
			}
			else {
				genericLoad();
			}
//			else {
//				return "Wrong import parameter file version: " + importFile + ": " + version;
//			}
			return ""; //$NON-NLS-1$
		}
		catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	/**
	 * simple reading of all section parameters are stored in a section element with parameter elements (@key + text)
	 */
	private void genericLoad() {
		sections.clear();
		
		NodeList sectionList = doc.getElementsByTagName("section"); //$NON-NLS-1$
		for (int i = 0; i < sectionList.getLength(); i++) {
			Element section = (Element) sectionList.item(i);
			String name = section.getAttribute("name"); //$NON-NLS-1$
			if (name == null || name.length() == 0) {
				Log.warning("Load import parameters: section with no name");
				continue;
			}
			if (sections.containsKey(name)) {
				Log.warning("Load import parameters: duplicated section with name:" + name);
				continue;
			}
			
			LinkedHashMap<String, String> sectionParameters = new LinkedHashMap<>();
			sections.put(name, sectionParameters);
			NodeList parameterList = section.getElementsByTagName("parameter"); //$NON-NLS-1$
			for (int j = 0; j < parameterList.getLength(); j++) {
				Element parameter = (Element) parameterList.item(j);
				String name2 = parameter.getAttribute("name"); //$NON-NLS-1$
				if (name2 == null || name2.length() == 0) {
					Log.warning("Load import parameters: parameter with no name in section: " + name);
					continue;
				}
				if (sectionParameters.containsKey(name2)) {
					Log.warning("Load import parameters: duplicated parameter with name:" + name2 + " in section: " + name);
					continue;
				}
				
				sectionParameters.put(name2, parameter.getTextContent());
			}
		}
	}
	
	private void loadFrom079() {
		sections.clear();
		
		Element corporaElement = (Element) doc.getElementsByTagName("corpora").item(0); //$NON-NLS-1$
		Element corpusElement = (Element) corporaElement.getElementsByTagName("corpus").item(0); //$NON-NLS-1$
		Element tokenizerElement = (Element) corpusElement.getElementsByTagName("tokenizer").item(0); //$NON-NLS-1$
		Element pattributesElement = (Element) corpusElement.getElementsByTagName("pattributes").item(0); //$NON-NLS-1$
		Element sattributesElement = (Element) corpusElement.getElementsByTagName("sattributes").item(0); //$NON-NLS-1$
		Element uisElement = (Element) corpusElement.getElementsByTagName("uis").item(0); //$NON-NLS-1$
		Element editionsElement = (Element) corpusElement.getElementsByTagName("editions").item(0); //$NON-NLS-1$
		Element queriesElement = (Element) corpusElement.getElementsByTagName("queries").item(0); //$NON-NLS-1$
		Element preBuildElement = (Element) corpusElement.getElementsByTagName("preBuild").item(0); //$NON-NLS-1$
		Element xsltElement = (Element) corpusElement.getElementsByTagName("xslt").item(0); //$NON-NLS-1$
		
		LinkedHashMap<String, String> section = new LinkedHashMap<>();
		section.put("src", corporaElement.getAttribute("rootDir")); //$NON-NLS-1$
		section.put("desc", corporaElement.getAttribute("desc")); //$NON-NLS-1$
		section.put("script", corporaElement.getAttribute("script")); //$NON-NLS-1$
		section.put("date", corporaElement.getAttribute("date")); //$NON-NLS-1$
		section.put("author", corporaElement.getAttribute("author")); //$NON-NLS-1$
		section.put("author", corpusElement.getAttribute("name")); //$NON-NLS-1$
		sections.put("src", section);
		
		section = new LinkedHashMap<>();
		section.put("annotate", corpusElement.getAttribute("annotate")); //$NON-NLS-1$
		section.put("selected_lang", corpusElement.getAttribute("lang")); //$NON-NLS-1$
		sections.put("lang", section); //$NON-NLS-1$
		
		section = new LinkedHashMap<>();
		section.put("word_element", corpusElement.getAttribute("annotate")); //$NON-NLS-1$
		section.put("tokenize", "" + (tokenizerElement.getAttribute("skip") != "true")); //$NON-NLS-1$
		NodeList paramList = tokenizerElement.getElementsByTagName("param"); //$NON-NLS-1$
		for (int p = 0; p < paramList.getLength(); p++) {
			String pn = paramList.item(p).getAttributes().getNamedItem("name").getNodeValue(); //$NON-NLS-1$
			if (pn.equals("whitespaces")) { //$NON-NLS-1$
				section.put("whitespaces", paramList.item(p).getTextContent()); //$NON-NLS-1$
			}
			else if (pn.equals("regPunct")) { //$NON-NLS-1$
				section.put("regPunct", paramList.item(p).getTextContent()); //$NON-NLS-1$
			}
			else if (pn.equals("punct_strong")) { //$NON-NLS-1$
				section.put("punct_strong", paramList.item(p).getTextContent()); //$NON-NLS-1$
			}
			else if (pn.equals("regElision")) { //$NON-NLS-1$
				section.put("regElision", paramList.item(p).getTextContent()); //$NON-NLS-1$
			}
		}
		sections.put("tokenizer", section); //$NON-NLS-1$
		
		section = new LinkedHashMap<>();
		section.put("font", corpusElement.getAttribute("font")); //$NON-NLS-1$
		sections.put("font", section); //$NON-NLS-1$
		
		section = new LinkedHashMap<>();
		NodeList editionList = editionsElement.getElementsByTagName("edition"); //$NON-NLS-1$
		for (int e = 0; e < editionList.getLength(); e++) {
			Element editionElement = (Element) editionList.item(e);
			if ("default".equals(editionElement.getAttribute("name"))) { //$NON-NLS-1$
				section.put("build_default", editionElement.getAttribute("build_edition")); //$NON-NLS-1$
				section.put("page_break_tag", editionElement.getAttribute("page_break_tag")); //$NON-NLS-1$
				section.put("words_per_page", editionElement.getAttribute("words_per_page")); //$NON-NLS-1$
			}
			else if ("facs".equals(editionElement.getAttribute("name"))) { //$NON-NLS-1$
				section.put("build_facs", editionElement.getAttribute("build_edition")); //$NON-NLS-1$
			}
		}
		section.put("default", editionsElement.getAttribute("default")); //$NON-NLS-1$
		sections.put("editions", section); //$NON-NLS-1$
		
		section = new LinkedHashMap<>();
		sections.put("commands", section); //$NON-NLS-1$
		
		section = new LinkedHashMap<>();
		sections.put("commands", section); //$NON-NLS-1$
		
		section = new LinkedHashMap<>();
		sections.put("textualplans", section); //$NON-NLS-1$
		
		section = new LinkedHashMap<>();
		sections.put("options", section); //$NON-NLS-1$
	}
	
	public String save() {
		try {
			if (doc == null) {
				return "Import parameter not loaded";
			}
			DomUtils.save(doc, importFile);
			return ""; //$NON-NLS-1$
		}
		catch (UnsupportedEncodingException | FileNotFoundException e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}

	public ImportFileManager load(Project project) {
		// TODO Auto-generated method stub
		return null;
	}
}
