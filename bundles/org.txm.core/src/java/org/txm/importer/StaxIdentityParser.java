package org.txm.importer;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.logger.Log;

/**
 * XML Identity transformation. extends the processXYZ methods to do something
 * 
 * NOTE: use the org.txm.xml.XMLProcessor instead. The XMLProcessor implements Hooks to manage XML Event or XML DOM while parsing
 * @author mdecorde
 *
 */
public class StaxIdentityParser {
	
	protected URL inputurl;
	
	protected InputStream inputData;
	
	protected XMLInputFactory factory;
	
	protected XMLStreamReader parser;
	
	/** The output. */
	protected XMLOutputFactory outfactory = XMLOutputFactory.newInstance();
	
	protected BufferedOutputStream output;
	
	protected XMLStreamWriter writer;
	
	public static String TXMNS = "http://textometrie.org/1.0"; //$NON-NLS-1$
	
	public static String TXM = "txm"; //$NON-NLS-1$
	
	public static String TEINS = "http://www.tei-c.org/ns/1.0"; //$NON-NLS-1$
	
	public static String TEI = "tei"; //$NON-NLS-1$
	
	protected PersonalNamespaceContext Nscontext = new PersonalNamespaceContext();
	
	// protected StringBuilder currentXPath = new StringBuilder("/")
	protected String localname;
	
	int processingXInclude = 0;
	
	public StaxIdentityParser(File infile) throws IOException, XMLStreamException {
		this(infile.toURI().toURL());
	}
	
	public StaxIdentityParser(URL inputurl) throws IOException, XMLStreamException {
		this.inputurl = inputurl;
		this.inputData = inputurl.openStream();
		this.factory = XMLInputFactory.newInstance();
		this.parser = factory.createXMLStreamReader(inputData);
		
	}
	
	/**
	 * Helper method to get an attribute value
	 * 
	 * @param name the attribute name
	 * @return the value if any
	 */
	public String getParserAttributeValue(String name) {
		if (name == null) return null;
		
		int c = parser.getAttributeCount();
		for (int i = 0; i < c; i++) {
			if (name.equals(parser.getAttributeLocalName(i))) {
				return parser.getAttributeValue(i);
			}
		}
		
		return null;
	}
	
	protected void before() {
		
	}
	
	protected void after() throws XMLStreamException, IOException {
		factory = null;
		if (parser != null) parser.close();
		writer.flush();
		if (writer != null) writer.close();
		if (inputData != null) inputData.close();
		writer = null;
		parser = null;
	}
	
	protected void closeForError() throws XMLStreamException, IOException {
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
	}
	
	/**
	 * Creates the output.
	 *
	 * @return true, if successful
	 */
	private boolean createOutput(File f, String encoding) {
		try {
			if (writer != null) // process from a file
				writer.close();
			if (output != null) // process from a file
				output.close();
			
			output = new BufferedOutputStream(new FileOutputStream(f), 16 * 1024);
			
			writer = outfactory.createXMLStreamWriter(output, encoding);// create a new file
			writer.setNamespaceContext(Nscontext);
			return true;
		}
		catch (Exception e) {
			System.out.println("Error: create output of " + f + ": " + e);
			return false;
		}
	}
	
	/**
	 * Default output encoding is UTF-8
	 * 
	 * @param outfile
	 * @return
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	public boolean process(File outfile) throws XMLStreamException, IOException {
		return process(outfile, "UTF-8"); //$NON-NLS-1$
	}
	
	public boolean process(File outfile, String encoding) throws XMLStreamException, IOException {
		// //writer.writeStartDocument("UTF-8", "1.0");
		// writer.writeStartDocument();
		// writer.writeCharacters("\n");
		boolean ret = process(null, outfile, encoding);
		if (writer != null) {
			writer.close();
		}
		if (output != null) {
			try {
				output.close();
			}
			catch (Exception e) {
				System.out.println("output excep: " + e);
			}
		}
		
		if (parser != null) {
			try {
				parser.close();
			}
			catch (Exception e) {
				System.out.println("parser excep: " + e);
			}
		}
		
		if (inputData != null) {
			try {
				inputData.close();
			}
			catch (Exception e) {
				System.out.println("inputData excep: " + e);
			}
		}
		
		return ret;
	}
	
	public final static String SLASH = "/"; //$NON-NLS-1$
	
	public boolean firstElementStarted = false;
	
	
	/**
	 * If the writer is given, the expected encoding is UTF-8
	 * 
	 * @param awriter
	 * @return
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	public boolean process(XMLStreamWriter awriter) throws XMLStreamException, IOException {
		if (awriter == null) {
			Log.warning("Error: called without XMLStreamWriter.");
			return false;
		}
		return process(awriter, null, "UTF-8"); //$NON-NLS-1$
	}
	
	
	private boolean process(XMLStreamWriter awriter, File outfile, String encoding) throws XMLStreamException, IOException {
		firstElementStarted = false;
		this.writer = awriter;
		
		// if (processingXInclude == 0) {
		before(); // if you need to do something before parsing the XML
		// }
		try {
			for (int event = parser.getEventType(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				switch (event) {
					
					case XMLStreamConstants.START_DOCUMENT:
						if (encoding == null) { // if encoding is not specified,
							encoding = parser.getCharacterEncodingScheme();
						}
						if (awriter == null) {
							if (!createOutput(outfile, encoding)) {
								return false;
							}
						}
						else {
							encoding = "UTF-8"; // if awriter is set encoding must be UTF-8 //$NON-NLS-1$
						}
						
						writer.writeStartDocument(encoding, parser.getVersion());
						writer.writeCharacters("\n"); //$NON-NLS-1$
						break;
					case XMLStreamConstants.NAMESPACE:
						this.Nscontext.addNamespace(parser.getPrefix(), parser.getNamespaceURI());
						processNamespace();
						break;
					case XMLStreamConstants.START_ELEMENT:
						firstElementStarted = true;
						localname = parser.getLocalName();
						
						for (int i = 0; i < parser.getNamespaceCount(); i++) {
							Nscontext.addNamespace(parser.getNamespacePrefix(i), parser.getNamespaceURI(i));
						}
						// currentXPath.append(SLASH)
						processStartElement();
						break;
					case XMLStreamConstants.NOTATION_DECLARATION:
						break;
					case XMLStreamConstants.SPACE:
						break;
					case XMLStreamConstants.CHARACTERS:
						processCharacters();
						break;
					case XMLStreamConstants.END_ELEMENT:
						localname = parser.getLocalName();
						processEndElement();
						// currentXPath.substring(0, currentXPath.length() - localname.length() -1)
						break;
					case XMLStreamConstants.PROCESSING_INSTRUCTION:
						processProcessingInstruction();
						break;
					case XMLStreamConstants.DTD:
						processDTD();
						break;
					case XMLStreamConstants.CDATA:
						processCDATA();
						break;
					case XMLStreamConstants.COMMENT:
						processComment();
						break;
					case XMLStreamConstants.END_DOCUMENT:
						processEndDocument();
						break;
					case XMLStreamConstants.ENTITY_REFERENCE:
						processEntityReference();
						break;
				}
			}
		}
		catch (Exception e) {
			System.out.println("Unexpected error while parsing file " + inputurl + " : " + e);
			System.out.println("Location line: " + parser.getLocation().getLineNumber() + " character: " + parser.getLocation().getColumnNumber());
			org.txm.utils.logger.Log.printStackTrace(e);
			
			// e.printStackTrace();
			if (writer != null) writer.close();
			if (output != null) output.close();
			parser.close();
			inputData.close();
			return false;
		}
		// if (processingXInclude == 0) {
		after(); // if you need to do something before closing the parser();
		// }
		return true;
	}
	
	/**
	 * The start element has already been written
	 * 
	 * @param tagname
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	public void goToEnd(String tagname) throws XMLStreamException, IOException {
		// System.out.println("start gotoend $tagname") //$NON-NLS-1$
		int elements = 1;
		try {
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				
				switch (event) {
					case XMLStreamConstants.NAMESPACE:
						processNamespace();
						break;
					case XMLStreamConstants.START_ELEMENT:
						elements++;
						localname = parser.getLocalName();
						// currentXPath.append(SLASH)
						_processStartElement();
						break;
					case XMLStreamConstants.CHARACTERS:
						processCharacters();
						break;
					case XMLStreamConstants.PROCESSING_INSTRUCTION:
						processProcessingInstruction();
						break;
					case XMLStreamConstants.DTD:
						processDTD();
						break;
					case XMLStreamConstants.CDATA:
						processCDATA();
						break;
					case XMLStreamConstants.COMMENT:
						processComment();
						break;
					case XMLStreamConstants.END_ELEMENT:
						elements--;
						localname = parser.getLocalName();
						// currentXPath.substring(0, currentXPath.length() - localname.length() -1)
						writer.writeEndElement();
						if (elements == 0 && localname == tagname)
							return;
						break;
					case XMLStreamConstants.END_DOCUMENT:
						processEndDocument();
						break;
					case XMLStreamConstants.ENTITY_REFERENCE:
						processEntityReference();
						break;
				}
			}
		}
		catch (Exception e) {
			System.out.println("Error while parsing file " + inputurl);
			System.out.println("Location " + parser.getLocation());
			org.txm.utils.logger.Log.printStackTrace(e);
			output.close();
			parser.close();
			return;
		}
	}
	
	/**
	 * 
	 * @param line show the line number
	 * @param col show the column number
	 * @param fullPath show the file full path
	 * 
	 * @return
	 */
	public String getLocation(boolean line, boolean col, boolean fullPath) {
		StringBuilder b = new StringBuilder();
		if (parser != null) {
			if (line) b.append("Line: " + parser.getLocation().getLineNumber());
			
			if (col) {
				if (line) b.append(" ");
				b.append("Col: " + parser.getLocation().getLineNumber());
			}
			
			if (line || col) b.append(" ");
			if (fullPath) {
				b.append("in File:" + this.inputurl.toString());
			}
			else {
				b.append("in File:" + this.inputurl.getFile());
			}
			
			if (b.length() == 0) return parser.getLocation().toString();
		}
		return b.toString();
	}
	
	public String getLocation() {
		return getLocation(true, true, true);
	}
	
	protected void processNamespace() throws XMLStreamException {
		writer.writeNamespace(parser.getPrefix(), parser.getNamespaceURI());
	}
	
	public static final String INCLUDE = "include"; //$NON-NLS-1$
	
	public static final String XI = "xi"; //$NON-NLS-1$
	
	protected void processStartElement() throws XMLStreamException, IOException {
		String prefix = parser.getPrefix();
		
		if (INCLUDE == localname && XI == prefix) {
			processXInclude();
		}
		else {
			if (prefix != null && prefix.length() > 0) {
				writer.writeStartElement(Nscontext.getNamespaceURI(prefix), localname);
			}
			else {
				writer.writeStartElement(localname);
			}
			
			for (int i = 0; i < parser.getNamespaceCount(); i++) {
				writer.writeNamespace(parser.getNamespacePrefix(i), parser.getNamespaceURI(i));
			}
			
			writeAttributes();
		}
	}
	
	private void _processStartElement() throws XMLStreamException, IOException {
		String prefix = parser.getPrefix();
		if (INCLUDE == localname && XI == prefix) {
			processXInclude();
		}
		else {
			if (prefix != null && prefix.length() > 0)
				writer.writeStartElement(Nscontext.getNamespaceURI(prefix), localname);
			else
				writer.writeStartElement(localname);
			
			for (int i = 0; i < parser.getNamespaceCount(); i++) {
				writer.writeNamespace(parser.getNamespacePrefix(i), parser.getNamespaceURI(i));
			}
			
			writeAttributes();
		}
	}
	
	protected void writeAttributes() throws XMLStreamException {
		for (int i = 0; i < parser.getAttributeCount(); i++) {
			writeAttribute(parser.getAttributePrefix(i), parser.getAttributeLocalName(i), parser.getAttributeValue(i));
		}
	}
	
	protected void writeAttribute(String prefix, String name, String value) throws XMLStreamException {
		if (prefix != null && prefix.length() > 0)
			writer.writeAttribute(prefix + ":" + name, value); //$NON-NLS-1$
		else
			writer.writeAttribute(name, value);
	}
	
	protected void processCharacters() throws XMLStreamException {
		writer.writeCharacters(parser.getText());
	}
	
	protected void processProcessingInstruction() throws XMLStreamException {
		writer.writeProcessingInstruction(parser.getPITarget(), parser.getPIData());
		if (!firstElementStarted) writer.writeCharacters("\n"); // new lines are not reported before the first element is parsed $NON-NLS-1$
	}
	
	protected void processDTD() throws XMLStreamException {
		writer.writeDTD(parser.getText());
	}
	
	protected void processCDATA() throws XMLStreamException {
		writer.writeCData(parser.getText());
	}
	
	protected void processComment() throws XMLStreamException {
		writer.writeComment(parser.getText().replace("--", "&#x2212;&#x2212;")); //$NON-NLS-1$ $NON-NLS-2$
	}
	
	protected void processEndElement() throws XMLStreamException {
		if (localname == INCLUDE && parser.getPrefix() == XI) {
			// nothing !!
		}
		else {
			writer.writeEndElement();
		}
	}
	
	protected void processEndDocument() throws XMLStreamException {
		writer.writeEndDocument();
	}
	
	protected void processEntityReference() throws XMLStreamException {
		writer.writeEntityRef(parser.getLocalName());
	}
	
	/**
	 * Process the XInclude elements
	 * 
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	protected void processXInclude() throws XMLStreamException, IOException {
		String url = parser.getAttributeValue(null, "href"); // relative only //$NON-NLS-1$
		File driver = new File(inputurl.getFile());
		File ref = new File(driver.getParent(), url);
		if (ref.exists()) {
			URL includeurl = ref.toURI().toURL();
			// save variables before killing them
//			System.out.println("process xi include: " + ref);
			XMLStreamReader parserSave = this.parser; // back up current parser
			InputStream xiIncludeInputData = includeurl.openStream();
			XMLStreamReader xiIncludeParser = factory.createXMLStreamReader(xiIncludeInputData);
			parser = xiIncludeParser;
			processingXInclude++; // to avoid recalling before & after methods
			this.process(writer);
			processingXInclude--; // end of XInclude processing
			this.parser = parserSave; // restore parser
		}
		else {
			System.out.println(NLS.bind("Warning referenced file: {0} does not exists", ref));
		}
	}
	
	public static void main(String[] args) {
		try {
			File input = new File("runtime-rcpapplication.product/corpora/TXT/txm/TXT/0002 o2.xml"); //$NON-NLS-1$
			File outputFile = new File("runtime-rcpapplication.product/corpora/TXT/txm/TXT/0002 o2-out.xml"); //$NON-NLS-1$
			if (!(input.exists() && input.canRead())) {
				System.out.println("cannot found $input"); //$NON-NLS-1$
				return;
			}
			StaxIdentityParser builder;
			
			builder = new StaxIdentityParser(input.toURI().toURL());
			
			if (builder.process(outputFile)) {
				System.out.println("success ? " + ValidateXml.test(outputFile)); //$NON-NLS-1$
			}
			else {
				System.out.println("failure !"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
