package org.txm.importer;

import java.util.LinkedHashMap;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.txm.objects.Project;
import org.txm.utils.logger.Log;

/**
 * Read and save a import parameter file
 * 
 * The file is read to fill the ImportFormEditor > CorpusPage widgets
 * 
 * @author mdecorde
 *
 */
public class ImportPreferencesManager {
	
	/**
	 * one map per import section
	 */
	LinkedHashMap<String, LinkedHashMap<String, String>> sections = new LinkedHashMap<>();
	
	private Project project;
	
	public ImportPreferencesManager(Project project) {
		this.project = project;
	}
	
	public LinkedHashMap<String, LinkedHashMap<String, String>> getSections() {
		return sections;
	}
	
	public LinkedHashMap<String, String> getSection(String name) {
		if (name == null) return null;
		return sections.get(name);
	}
	
	public String load() throws BackingStoreException {
		String version = project.getCorpusVersion();
		if ("0.8.0".equals(version)) { //$NON-NLS-1$
			loadFrom080();
		}
		else if ("0.8.4".equals(version)) { //$NON-NLS-1$
			genericLoad();
		}
		else {
			return "Wrong import parameter file version: " + project + ": " + version;
		}
		return ""; //$NON-NLS-1$
	}
	
	private void loadFrom080() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * simple reading of all section parameters are stored in a section element with parameter elements (@key + text)
	 * 
	 * @throws BackingStoreException
	 */
	private void genericLoad() throws BackingStoreException {
		sections.clear();
		
		Preferences sectionList = project.getPreferencesScope().getNode("import").node("sections");
		String[] sectionNames = sectionList.childrenNames();
		for (int i = 0; i < sectionNames.length; i++) {
			Preferences section = sectionList.node(sectionNames[i]);
			String name = section.name();
			if (name == null || name.length() == 0) {
				Log.warning("Load import parameters: section with no name");
				continue;
			}
			if (sections.containsKey(name)) {
				Log.warning("Load import parameters: duplicated section with name:" + name);
				continue;
			}
			
			LinkedHashMap<String, String> sectionParameters = new LinkedHashMap<>();
			sections.put(name, sectionParameters);
			for (String name2 : section.keys()) {
				String parameter = section.get(name2, ""); //$NON-NLS-1$
				
				if (sectionParameters.containsKey(name2)) {
					Log.warning("Load import parameters: duplicated parameter with name:" + name2 + " in section: " + name);
					continue;
				}
				
				sectionParameters.put(name2, parameter);
			}
		}
	}
	
	public String save() throws BackingStoreException {
		String version = project.getCorpusVersion();
		if ("0.8.0".equals(version)) { //$NON-NLS-1$
			saveFrom080();
		}
		else if ("0.8.4".equals(version)) { //$NON-NLS-1$
			genericSave();
		}
		else {
			return "Wrong import parameter file version: " + project + ": " + version;
		}
		return ""; //$NON-NLS-1$
	}
	
	private void genericSave() throws BackingStoreException {
		IEclipsePreferences importNode = project.getPreferencesScope().getNode("import"); //$NON-NLS-1$
		Preferences sectionList = importNode.node("sections"); //$NON-NLS-1$
		
		// store previous values
		if (importNode.nodeExists("sections-previous")) { //$NON-NLS-1$
			importNode.node("sections-previous").removeNode(); // clear previous values //$NON-NLS-1$
		}
		Preferences oldSectionList = importNode.node("sections-previous"); // create/get the node //$NON-NLS-1$
		oldSectionList.removeNode();
		for (String section : sectionList.childrenNames()) {
			Preferences sectionPref = sectionList.node(section);
			Preferences sectionPref2 = oldSectionList.node(section);
			for (String key : sectionPref.keys()) {
				sectionPref2.put(key, sectionPref.get(key, "")); //$NON-NLS-1$
			}
		}
		
		// clear the sections list
		sectionList.clear();
		for (String section : sectionList.childrenNames()) {
			sectionList.node(section).removeNode();
		}
		
		// recreate sections
		for (String section : this.sections.keySet()) {
			Preferences n = sectionList.node(section);
			LinkedHashMap<String, String> prefs = this.sections.get(section);
			for (String key : prefs.keySet()) {
				n.put(key, prefs.get(key));
			}
		}
	}
	
	private void saveFrom080() {
		// TODO Auto-generated method stub
		
	}
}
