// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2011-11-03 17:59:23 +0100 (jeu., 03 nov. 2011) $
// $LastChangedRevision: 2051 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.importer;

/**
 * @author mdecorde
 */

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.io.FileCopy;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

import net.sf.saxon.TransformerFactoryImpl;

// TODO: Auto-generated Javadoc
/**
 * apply a xsl file to a file : use process(File infile, File outfile, String[]
 * args) to apply the xslt file loaded by the constructor args =
 * ["name1","value1","name2",value2, ...]
 * 
 * @author mdecorde
 */

public class ApplyXsl2 {
	
	// /** The proc. */
	// private Processor proc;
	//
	// /** The comp. */
	// private XsltCompiler comp;
	//
	// /** The exp. */
	// private XsltExecutable exp;
	//
	// /** The source. */
	// private XdmNode source;
	//
	// /** The out. */
	// private Serializer out;
	//
	// /** The trans. */
	// private XsltTransformer trans;
	
	net.sf.saxon.TransformerFactoryImpl tFactory;
	
	Transformer transformer;
	
	private HashMap<String, Object> params = new HashMap<>();
	
	private File xsltfile;
	
	boolean debug = false;
	
	/**
	 * initialize with the xslt file.
	 *
	 * @param xsltfile the xsltfile
	 * @throws TransformerConfigurationException
	 */
	public ApplyXsl2(String xsltfile) throws TransformerConfigurationException {
		
		System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl"); //$NON-NLS-1$
		
		this.xsltfile = new File(xsltfile);
		tFactory = new TransformerFactoryImpl();
		transformer = tFactory.newTransformer(new StreamSource(this.xsltfile));
		
		
		// Log.info("new tFactory: "+tFactory);
		// Log.info("new transformer: "+transformer);
	}
	
	/**
	 * initialize with the xslt file.
	 *
	 * @param xsltfile the xsltfile
	 * @throws TransformerConfigurationException
	 */
	public ApplyXsl2(File xsltfile) throws TransformerConfigurationException {
		this.xsltfile = xsltfile;
		
		System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl"); //$NON-NLS-1$
		
		tFactory = new net.sf.saxon.TransformerFactoryImpl();
		// Log.info("new tFactory: "+tFactory);
		//tFactory.getConfiguration().registerExtensionFunction(new SaxonNodeSet());
		Log.fine(NLS.bind("ApplyXsl2 with the {0} stylesheet.", xsltfile)); //$NON-NLS-1$
		reload();
		// Log.info("new transformer: "+transformer);
	}
	
	/**
	 * Set a xslt param.
	 *
	 * @return true, if successful
	 */
	public boolean setParams(HashMap<String, String> params) {
		boolean b = true;
		for (String k : params.keySet())
			b = b & setParam(k, params.get(k));
		return b;
	}
	
	public void setDebug(boolean d) {
		this.debug = d;
	}
	
	/**
	 * Set a xslt param.
	 *
	 * @param name the name
	 * @param value the value
	 * @return true, if successful
	 */
	public boolean setParam(String name, Object value) {
		if (transformer != null) {
			transformer.setParameter(name, value);
			params.put(name, value);
			return true;
		}
		else
			return false;
	}
	
	/**
	 * Set a xslt param.
	 *
	 * @param name the name
	 * @param value the value
	 * @return true, if successful
	 */
	@Deprecated
	public boolean SetParam(String name, Object value) {
		if (transformer != null) {
			transformer.setParameter(name, value);
			params.put(name, value);
			return true;
		}
		else
			return false;
	}
	
	public boolean resetParams() {
		if (transformer != null) {
			transformer.reset();
			params.clear();
		}
		else
			return false;
		return true;
	}
	
	/**
	 * Force java to clean memory after processing a xslt. If not we might catch
	 * a JavaHeapspace Exception
	 * 
	 * @throws TransformerConfigurationException
	 * 
	 */
	private void cleanMemory() throws TransformerConfigurationException {
		reload();
		for (String name : params.keySet()) { // reload parameters
			transformer.setParameter(name, params.get(name));
		}
	}
	
	private boolean reload() throws TransformerConfigurationException {
		// try {
		transformer = null;
		transformer = tFactory.newTransformer(new StreamSource(xsltfile));
		// } catch (TransformerConfigurationException e) {
		// org.txm.utils.logger.Log.printStackTrace(e);
		// Log.severe("Error while reloading transformer: "+e);
		// System.out.println("Error while reloading transformer: "+e);
		// return false;
		// }
		return true;
	}
	
	/**
	 * Process files with xslt args.
	 *
	 * @param xmlinfile file to be processed
	 * @param xmloutfile output file
	 * @param args xslt args ["arg1","arg1value","arg2","arg2value"...]
	 * @return true, if successful
	 * @throws TransformerException
	 * @throws FileNotFoundException
	 */
	
	public boolean process(String xmlinfile, String xmloutfile, String[] args) throws Exception {
		File infile = new File(xmlinfile);
		File outfile = new File(xmloutfile);
		
		// System.out.println(xmlinfile+" Params: "+Arrays.toString(args));
		for (int i = 0; i < args.length; i = i + 2) {
			if (!this.setParam(args[i], args[i + 1]))
				return false;
		}
		
		transformer.transform(new StreamSource(xmlinfile), new StreamResult(new BufferedOutputStream(new FileOutputStream(xmloutfile))));
		cleanMemory(); // save memory
		// System.out.println("Done");
		return true;
	}
	
	/**
	 * Process files without xslt args.
	 *
	 * @param xmlinfile the xmlinfile
	 * @param xmloutfile the xmloutfile. If not set, xmlinfile content is replaced
	 * @return true, if successful
	 * @throws TransformerException
	 * @throws IOException
	 */
	public boolean process(File xmlinfile, File xmloutfile) throws TransformerException, IOException {
		
		// bufferize result in a String
		StringWriter strWriter = new StringWriter();
		StreamResult out = new StreamResult(strWriter);
		
		File tmp_dir = null;
		File inputfile_copy = null;
		
		if (debug) {
			// FIXME: old version
			// File inputfile_copy = new File("/tmp", xmlinfile.getName());
			// FIXME: new version
			tmp_dir = new File(xmlinfile.getParentFile().getAbsolutePath() + "/tmp"); //$NON-NLS-1$
			tmp_dir.mkdirs();
			tmp_dir.deleteOnExit();
			
			inputfile_copy = new File(tmp_dir, xmlinfile.getName());
			
			// System.out.println("copying inputfile to: "+inputfile_copy.getAbsolutePath());
			// FIXME: old version
			// inputfile_copy.delete();
			// FIXME: new version
			inputfile_copy.deleteOnExit();
			FileCopy.copy(xmlinfile, inputfile_copy);
			
		}
		
		transformer.transform(new StreamSource(xmlinfile), out);
		
		// then write the result in the result file
		if (xmloutfile == null) {
			xmloutfile = xmlinfile;
		}
		
		BufferedWriter w = new BufferedWriter(new FileWriter(xmloutfile));
		w.write(strWriter.toString());
		// System.out.println("DEBUG: "+xmloutfile);
		// System.out.println(" : "+strWriter.toString().substring(0, 60));
		w.close();
		
		// if (debug && inputfile_copy != null) {
		// //FIXME: old version
		// //File inputfile_copy = new File("/tmp", xmlinfile.getName());
		//
		// String s = Diff.diffFile(inputfile_copy, xmloutfile);
		// if (s.length() > 0) { // for ultra debug?
		// System.out.println("Result "+xmlinfile.getName()+" VS "+ xmloutfile.getName()+" diff="+s);
		// } else {
		// System.out.println("Warning: no diff between "+inputfile_copy+" and "+ xmloutfile);
		// }
		// }
		
		
		cleanMemory();
		return true;
	}
	
	/**
	 * Transform files and select files : nor hidden and nor technical (DTD, XSL, etc.)
	 * 
	 * @param xslFile
	 * @param srcdir
	 * @param outdir
	 * @return
	 * @throws TransformerException
	 * @throws IOException
	 */
	public static boolean processImportSources(File xslFile, File srcdir, File outdir) throws TransformerException, IOException {
		HashMap<String, Object> params = new HashMap<>();
		return processImportSources(xslFile, srcdir, outdir, params);
	}
	
	/**
	 * Transform files and select files : nor hidden and nor technical (DTD, XSL, etc.)
	 * 
	 * @param xslFile
	 * @param srcdir
	 * @param outdir
	 * @param params XSL parameters
	 * @return
	 * @throws TransformerException
	 * @throws IOException
	 */
	public static boolean processImportSources(File xslFile, File srcdir, File outdir, HashMap<String, Object> params) throws TransformerException, IOException {
		return processImportSources(xslFile, srcdir, outdir, params, false);
	}
	
	/**
	 * 
	 * @param srcdir directory
	 * @return the source files of a directory
	 */
	public static File[] listFiles(File srcdir) {
		
		File[] files = srcdir.listFiles(new FileFilter() {
			
			@Override
			public boolean accept(File f) {
				String name = f.getName();
				if (f.isHidden() || f.isDirectory()) return false;
				if (name.endsWith(".properties")) return false; //$NON-NLS-1$
				if (name.equals("import.xml")) return false; //$NON-NLS-1$
				if (name.endsWith(".csv")) return false; //$NON-NLS-1$
				if (name.endsWith(".ods")) return false; //$NON-NLS-1$
				if (name.endsWith(".xlsx")) return false; //$NON-NLS-1$
				if (name.endsWith(".dtd")) return false; //$NON-NLS-1$
				if (name.endsWith(".xsl")) return false; //$NON-NLS-1$
				if (name.endsWith("~")) return false; //$NON-NLS-1$
				if (name.endsWith(".bak")) return false; //$NON-NLS-1$
				if (name.startsWith(".")) return false; //$NON-NLS-1$
				return true;
			}
		});
		
		return files;
	}
	
	/**
	 * Transform files and select files : nor hidden and nor technical (DTD, XSL, etc.)
	 * 
	 * @param xslFile
	 * @param srcdir
	 * @param outdir
	 * @param params XSL parameters
	 * @return
	 * @throws TransformerException
	 * @throws IOException
	 */
	public static boolean processImportSources(File xslFile, File srcdir, File outdir, HashMap<String, Object> params, boolean deleteOutput) throws TransformerException, IOException {
		outdir.mkdir();
		if (xslFile.exists()) {
			ApplyXsl2 builder = new ApplyXsl2(xslFile);
			boolean debug = Log.getLevel().intValue() < Level.INFO.intValue();
			if (debug) {
				builder.setDebug(true);
				System.out.println("Debug mode enabled.");
			}
			if (!params.containsKey("output-directory")) //$NON-NLS-1$
				params.put("output-directory", outdir.getAbsoluteFile().toURI().toString()); //$NON-NLS-1$
			
			for (String name : params.keySet())
				builder.setParam(name, params.get(name));
			
			File[] files = listFiles(srcdir); // get files XSL compatible
			if (files == null || files.length == 0) {
				System.out.println("No XML files to process found in: "+srcdir);
				return false;
			}
			System.out.println("-- Applying '" + xslFile + "' XSL to " + files.length + " files from  directory '" + srcdir + "' with parameters: " + params + " result written in '" + outdir + "'");
			
			ConsoleProgressBar cpb = new ConsoleProgressBar(files.length);
			for (File f : files) {
				String name = f.getName();
				
				cpb.tick();
				File outfile = new File(outdir, name);
				
//				if (!f.getAbsolutePath().equals(outfile.getAbsolutePath()) && f.lastModified() <= outfile.lastModified()) {
//					if (debug) System.out.println("Skipping: " + f);
//					continue; // input file is older than output file
//				}
				
				if (deleteOutput) outfile = null; // if outfile is null builder.process won't create the output file adn will replace f
				
				if (!builder.process(f, outfile)) {
					System.out.println("Failed to process file " + f);
					if (outfile != null) outfile.delete();
				}
			}
			System.out.println(""); //$NON-NLS-1$
			return true;
		}
		else {
			System.out.println("XSL file does not exists: " + xslFile);
		}
		return false;
	}
	
	/**
	 * Transform files and select files : nor hidden and nor technical (DTD, XSL, etc.)
	 * 
	 * Apply one XSL, XML files are replaced
	 * 
	 * @param xslFile
	 * @param params XSL parameters
	 * @return
	 * @throws TransformerException
	 * @throws IOException
	 */
	public static boolean processImportSources(File xslFile, List<File> files, HashMap<String, Object> params) throws TransformerException, IOException {
		return processImportSources(xslFile, files.toArray(new File[files.size()]), params);
	}
	
	/**
	 * Transform files and select files : nor hidden and nor technical (DTD, XSL, etc.)
	 * 
	 * Apply one XSL, XML files are replaced
	 * 
	 * @param xslFile
	 * @param params XSL parameters
	 * @return
	 * @throws TransformerException
	 * @throws IOException
	 */
	public static boolean processImportSources(File xslFile, File[] files, HashMap<String, Object> params) throws TransformerException, IOException {
		if (xslFile.exists()) {
			ApplyXsl2 builder = new ApplyXsl2(xslFile);
			for (String name : params.keySet())
				builder.setParam(name, params.get(name));
			
			if (files == null || files.length == 0) {
				System.out.println("No XML files to process.");
				return false;
			}
			
			System.out.println("-- Applying " + xslFile + " XSL to " + files.length + " files with parameters: " + params);
			
			ConsoleProgressBar cpb = new ConsoleProgressBar(files.length);
			
			for (File f : files) {
				String name = f.getName();
				if (f.isHidden() || f.isDirectory()) continue;
				if (name.endsWith(".properties")) continue; //$NON-NLS-1$
				if (name.equals("import.xml")) continue; //$NON-NLS-1$
				if (name.endsWith(".csv")) continue; //$NON-NLS-1$
				if (name.endsWith(".ods")) continue; //$NON-NLS-1$
				if (name.endsWith(".xslx")) continue; //$NON-NLS-1$
				if (name.endsWith(".dtd")) continue; //$NON-NLS-1$
				if (name.endsWith(".xsl")) continue; //$NON-NLS-1$
				if (name.endsWith("~")) continue; //$NON-NLS-1$
				if (name.startsWith(".")) continue; //$NON-NLS-1$
				
				if (!ValidateXml.test(f)) {
					System.out.println("src file " + f + ", is not XML valid");
					return false;
				}
				cpb.tick();
				
				if (!builder.process(f, null)) {
					System.out.println("Failed with file " + f);
				}
			}
			System.out.println(""); //$NON-NLS-1$
			return true;
		}
		else {
			System.out.println("XSL file does not exists: " + xslFile);
		}
		return false;
	}
	
	/**
	 * Apply the XSL listed in the xslDirectory. Files are replaced
	 * 
	 * @param filesToProcess
	 * @param xslDirectory
	 * @param xslParams
	 * @return
	 * @throws TransformerException
	 * @throws IOException
	 */
	public static boolean processWithMultipleXSL(List<File> filesToProcess, File xslDirectory, HashMap<String, Object> xslParams) throws TransformerException, IOException {
		if (xslDirectory.exists()) {
			File[] xslFiles = xslDirectory.listFiles(IOUtils.HIDDENFILE_FILTER);
			Arrays.sort(xslFiles);
			return processWithMultipleXSL(filesToProcess, xslFiles, xslParams);
		}
		else {
			System.out.println("Nothing to do.");
		}
		return true;
	}
	
	/**
	 * Apply the XSL listed in the xslDirectory. Files are replaced
	 * 
	 * @param filesToProcess
	 * @param xslDirectory
	 * @param xslParams
	 * @return
	 * @throws TransformerException
	 * @throws IOException
	 */
	public static boolean processWithMultipleXSL(File[] filesToProcess, File xslDirectory, HashMap<String, Object> xslParams) throws TransformerException, IOException {
		if (xslDirectory.exists()) {
			File[] xslFiles = xslDirectory.listFiles(IOUtils.HIDDENFILE_FILTER);
			Arrays.sort(xslFiles);
			return processWithMultipleXSL(filesToProcess, xslFiles, xslParams);
		}
		else {
			System.out.println("Nothing to do.");
		}
		return true;
	}
	
	/**
	 * Apply the XSL list. Files are replaced
	 * 
	 * @param filesToProcess
	 * @param xslFiles
	 * @param xslParams
	 * @return
	 * @throws TransformerException
	 * @throws IOException
	 */
	public static boolean processWithMultipleXSL(List<File> filesToProcess, File[] xslFiles, HashMap<String, Object> xslParams) throws TransformerException, IOException {
		
		Arrays.sort(xslFiles);
		for (File xslFile : xslFiles) {
			if (xslFile.isDirectory() || xslFile.isHidden() || !xslFile.getName().endsWith(".xsl")) continue;
			
			if (processImportSources(xslFile, filesToProcess, xslParams)) {
				System.out.println(""); //$NON-NLS-1$
			}
			else {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Apply the XSL list. Files are replaced
	 * 
	 * @param filesToProcess
	 * @param xslFiles
	 * @param xslParams
	 * @return
	 * @throws TransformerException
	 * @throws IOException
	 */
	public static boolean processWithMultipleXSL(File[] filesToProcess, File[] xslFiles, HashMap<String, Object> xslParams) throws TransformerException, IOException {
		
		Arrays.sort(xslFiles);
		for (File xslFile : xslFiles) {
			if (xslFile.isDirectory() || xslFile.isHidden() || !xslFile.getName().endsWith(".xsl")) continue;
			
			if (processImportSources(xslFile, filesToProcess, xslParams)) {
				System.out.println(""); //$NON-NLS-1$
			}
			else {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws TransformerException
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) throws Exception {
		File xslfile = new File(System.getProperty("user.home"), "xml/xsltest/test.xsl"); //$NON-NLS-1$
		assert(xslfile.exists());
		ApplyXsl2 a = new ApplyXsl2(xslfile);
		
		File srcdirectory = new File(System.getProperty("user.home"), "xml/xsltest"); //$NON-NLS-1$
		assert(srcdirectory.exists());
		assert(srcdirectory.isDirectory());
		assert(srcdirectory.canRead());
		assert(srcdirectory.canExecute());
		
		File[] files = srcdirectory.listFiles(IOUtils.HIDDENFILE_FILTER);
		
		//				files = files.findAll{item-> item.getName().endsWith(".xml")};
		
		long bigstart = System.currentTimeMillis();
		for (File infile : files) {
			if (!(infile.getName().toLowerCase().endsWith(".xml"))) continue; //$NON-NLS-1$
			
			File outfile = new File(infile.getAbsolutePath()+".out"); //$NON-NLS-1$
			if (infile.canRead()) {
				long start = System.currentTimeMillis();
				System.out.println("Process : "+infile.getName()); //$NON-NLS-1$
				// String[] params = ["pbval1",1,"pbval2",2];
				a.process(infile, outfile); // no parameters
				// a.process( infile.getAbsolutePath(),outfile,params);
				System.out.println("Done : "+(System.currentTimeMillis()-start)+"ms"); //$NON-NLS-1$
			}
		}
		System.out.println("Total time : "+(System.currentTimeMillis()-bigstart)+"ms");
	}
}
