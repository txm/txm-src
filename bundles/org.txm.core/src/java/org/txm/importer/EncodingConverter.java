// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2014-09-15 10:51:15 +0200 (Mon, 15 Sep 2014) $
// $LastChangedRevision: 2836 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.importer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Node;

// TODO: Auto-generated Javadoc
/**
 * change the encoding of a file.
 *
 * @author mdecorde
 */
public class EncodingConverter {

	/**
	 * Instantiates a new encoding converter.
	 *
	 * @param in the in
	 * @param out the out
	 * @param in_encoding the in_encoding
	 * @param out_encoding the out_encoding
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public EncodingConverter(File in, File out, String in_encoding,
			String out_encoding) throws IOException {
		int c;
		InputStreamReader ist = new InputStreamReader(new FileInputStream(in),
				in_encoding);
		OutputStreamWriter ost = new OutputStreamWriter(new FileOutputStream(
				out), out_encoding);

		while ((c = ist.read()) != -1)
			ost.write(c);

		ist.close();
		ost.close();
	}

	/**
	 * out encoding is UTF-8.
	 *
	 * @param in the in
	 * @param in_encoding the in_encoding
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public EncodingConverter(File in, String in_encoding) throws IOException {
		String out_encoding = "UTF-8"; //$NON-NLS-1$

		File out = new File("tempencodingconverter"); //$NON-NLS-1$
		int c;
		InputStreamReader ist = new InputStreamReader(new FileInputStream(in),
				in_encoding);
		OutputStreamWriter ost = new OutputStreamWriter(new FileOutputStream(
				out), out_encoding);
		while ((c = ist.read()) != -1)
			ost.write(c);

		ist.close();
		ost.close();

		in.delete();
		out.renameTo(in);
	}

	/**
	 * Instantiates a new encoding converter.
	 *
	 * @param in : will be replaced
	 * @param in_encoding the in_encoding
	 * @param out_encoding the out_encoding
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public EncodingConverter(File in, String in_encoding, String out_encoding)
			throws IOException {
		File out = new File("tempencodingconverter"); //$NON-NLS-1$
		int c;
		InputStreamReader ist = new InputStreamReader(new FileInputStream(in),
				in_encoding);
		OutputStreamWriter ost = new OutputStreamWriter(new FileOutputStream(
				out), out_encoding);
		while ((c = ist.read()) != -1)
			ost.write(c);

		ist.close();
		ost.close();

		in.delete();
		out.renameTo(in);
	}
	
	/**
	 * Convert xml.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 * @param encoding the encoding
	 * @return true, if successful
	 */
	public static boolean convertXML(File infile, File outfile, String encoding)
	{
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Node doc = builder.parse(infile);
			// Création de la source DOM
			Source source = new DOMSource(doc);
			
			// Création du fichier de sortie
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outfile), "UTF-8"));  //$NON-NLS-1$
			Result resultat = new StreamResult(writer);
			
			// Configuration du transformer
			TransformerFactory fabrique = new net.sf.saxon.TransformerFactoryImpl();
			
			Transformer transformer = fabrique.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");  //$NON-NLS-1$
			transformer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");  //$NON-NLS-1$
			
			// Transformation
			transformer.transform(source, resultat);
			writer.close();
			doc = null;
			return true;
		} catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		File infile = new File("mdecorde\\xml\\minicedre\\int1.trs"); //$NON-NLS-1$
		File outfile = new File("mdecorde\\xml\\minicedre\\int1utf8.trs"); //$NON-NLS-1$
		EncodingConverter.convertXML(infile, outfile, "UTF-8"); //$NON-NLS-1$
		/*List<File> files = new ArrayList<File>();
		LinkedList<File> filefiles = new LinkedList<File>();

		File racine = new File(
				"~/workspace/org.txm.rcp");
		String extension = ".properties";
		String prefix = "messages";

		while (racine != null) {
			if (racine.exists())
				for (File f : racine.listFiles(IOUtils.HIDDENFILE_FILTER)) {
					// System.out.println("scan file "+f);
					if (f.isDirectory()) {
						if (!f.getName().equals(".svn"))
							filefiles.add(f);
					} else if (f.isFile()) {
						if (f.getName().endsWith(extension)
								&& f.getName().startsWith(prefix))
							files.add(f);
					}
				}

			if (filefiles.size() > 0)
				racine = filefiles.pop();
			else
				racine = null;
		}

		System.out.println(files);

		for (File f : files) {
			System.out.println("convert " + f);
			try {
				new EncodingConverter(f, "iso-8859-1", "utf-8");
			} catch (IOException e) {
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}*/
	}

}
