package org.txm.importer;

import java.io.File;
import java.util.HashMap;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Element;

/**
 * Process a ResultDom after applying a XSL to XML files of a directory
 * 
 * Java conversion of ExelXSLDom macro
 * 
 * @author mdecorde
 *
 */
public abstract class ApplyXSLDOM {
	// STANDARD DECLARATIONS
	
	public boolean debug = true;
		
	public abstract void processDOMResultToFile(File inputXMLFile, Element resultnode);
	
	// END USER MANIPULATIONS
	
	public void processFile(File xslFile, File inputDirectory, File outputDirectory, HashMap<String, String> args) throws TransformerConfigurationException {
		System.out.println("Processing directory: $inputDirectory");
		
		ApplyXsl2 a = new ApplyXsl2(xslFile.getAbsolutePath());
		File[] toProcess = inputDirectory.listFiles();
		for(File XMLFile : toProcess) {
			
			if (!XMLFile.isFile()) continue;
			if (!XMLFile.canRead()) continue;
			if (XMLFile.isHidden()) continue;
			if (!XMLFile.getName().toLowerCase().endsWith(".xml")) continue; //$NON-NLS-1$
			
			String name = XMLFile.getName();
			try {
				DOMResult result = _process(a, XMLFile, null);
				
				processDOMResultToFile(XMLFile, (Element)result.getNode());
				File resultFile = new File(outputDirectory, XMLFile.getName());
			} catch (Exception e) {
				System.out.println("Warning: XSL transformation of '$name' failed with error=$e with ");
				if (debug) e.printStackTrace(); 
			}
		}
	}
	
	private DOMResult _process(ApplyXsl2 a, File inputXMLFile, HashMap<String, String> args) throws Exception {
		
		if (args != null) {
			for (String k : args.keySet()) {
				if (!a.setParam(k, args.get(k)))
					return null;
			}
		}
		
		DOMResult result = new DOMResult();
		
		a.transformer.transform(new StreamSource(inputXMLFile), result);
		a.resetParams();
		return result;
	}
}
