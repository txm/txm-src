package org.txm.importer;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.objects.BaseOldParameters;
import org.txm.objects.Page;
import org.txm.utils.io.FileCopy;
import org.txm.utils.io.IOUtils;
import org.txm.utils.xml.DomUtils;
import org.txm.utils.zip.Zip;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Convert5To6 {

	File HTMLs, HTMLmulti, TXMs, DATAs, REGISTRY, BINDIR, IMPORT;
	File OUTDIR;
	File INDIR;

	public Convert5To6(File directory, File outdir)
	{
		OUTDIR = directory;
		OUTDIR = outdir;
		INDIR = directory;
	}
	
	public static boolean is6(File corpusdir) {
		File param = new File(corpusdir, "import.xml"); //$NON-NLS-1$
		if (!param.exists()) { 
			//System.out.println("\n");
			System.out.println(TXMCoreMessages.bind(TXMCoreMessages.P0isMissing, param)); //$NON-NLS-1$
			return false; 
		}

		try {
			Document doc = DomUtils.load(param);
			Element root = doc.getDocumentElement();
			return "0.6".compareTo(root.getAttribute("version").toString()) <= 0; //$NON-NLS-1$ //$NON-NLS-2$
		} catch (Exception e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return false;
	}

	public boolean is5(boolean silent) {
		File HTMLs = new File(INDIR, "HTML"); //$NON-NLS-1$
		File HTMLmulti = new File(HTMLs, "multi"); //$NON-NLS-1$
		File TXMs = new File(INDIR, "txm"); //$NON-NLS-1$
		File DATAs = new File(INDIR, "data"); //$NON-NLS-1$
		File REGISTRY = new File(INDIR, "registry"); //$NON-NLS-1$
		File IMPORT = new File(INDIR, "import.xml"); //$NON-NLS-1$
		
		if (!silent) System.out.println(TXMCoreMessages.checkingBinaryFormat);
		if (!TXMs.exists()) {
			if (!silent) System.out.println(NLS.bind(TXMCoreMessages.txmDirectoryIsMissingColonP0, TXMs));
			return false;
		}

		if (!HTMLs.exists()) {
			if (!silent) System.out.println(NLS.bind(TXMCoreMessages.htmlDirectoryIsMissingColonP0, HTMLs));
			return false;
		}

		if (!DATAs.exists()) {
			if (!silent) System.out.println(NLS.bind(TXMCoreMessages.dataDirectoryIsMissingColonP0, DATAs));
			return false;
		}

		if (!REGISTRY.exists()) {
			if (!silent) System.out.println(NLS.bind(TXMCoreMessages.registryFileIsMissingColonP0, REGISTRY));
			return false;
		}

		if (!HTMLmulti.exists()) {
			if (!silent) System.out.println(NLS.bind(TXMCoreMessages.htmlDirectoryIsMissingColonP0, HTMLmulti));
			return false;
		}

		return REGISTRY.listFiles(IOUtils.HIDDENFILE_FILTER).length > 0;
	}

	private boolean process() throws Exception {
		System.out.println(TXMCoreMessages.processing);
		String corpusname = REGISTRY.listFiles(IOUtils.HIDDENFILE_FILTER)[0].getName().toUpperCase();

		// create the corpus directories
		File HTMLc = new File(HTMLs, corpusname); 
		File HTMLcmulti = new File(HTMLc, "default"); //$NON-NLS-1$
		File HTMLconepage = new File(HTMLc, "onepage"); //$NON-NLS-1$
		File TXMc = new File(TXMs, corpusname); 
		File DATAc = new File(DATAs, corpusname); 

		// mkdirs
		HTMLc.mkdir();
		HTMLcmulti.mkdir();
		File[] files = HTMLs.listFiles(IOUtils.HIDDENFILE_FILTER);
		if (files != null && files.length > 1)
			HTMLconepage.mkdir();
		TXMc.mkdir();
		DATAc.mkdir();
		
		System.out.println(NLS.bind(TXMCoreMessages.reorganizingFilesOfP0, OUTDIR));
		// move files
		for (File f : HTMLmulti.listFiles(IOUtils.HIDDENFILE_FILTER))
			f.renameTo(new File(HTMLcmulti, f.getName()));
		if (HTMLconepage.exists()) {
			for (File f : HTMLs.listFiles(IOUtils.HIDDENFILE_FILTER))
				if (f.isFile())
					f.renameTo(new File(HTMLconepage, f.getName()));
		}
		for (File f : DATAs.listFiles(IOUtils.HIDDENFILE_FILTER))
			f.renameTo(new File(DATAc, f.getName()));
		for (File f : TXMs.listFiles(IOUtils.HIDDENFILE_FILTER))
			f.renameTo(new File(TXMc, f.getName()));

		// remove unused dir
		HTMLmulti.delete();

		// create import.xml
		String baseDesc = ""; //$NON-NLS-1$
		String baseName = corpusname.toUpperCase();

		String corpusDesc = ""; //$NON-NLS-1$
		String author = System.getProperty("user.name"); //$NON-NLS-1$
		String rootDir = ""; //$NON-NLS-1$
		String script = ""; //$NON-NLS-1$
		String xslt = ""; //$NON-NLS-1$
		String lang = "fr"; //$NON-NLS-1$
		String encoding = "UTF-8"; //$NON-NLS-1$
		String date = BaseOldParameters.dateformat.format(new Date(IMPORT.lastModified()));

		if (IMPORT.exists()) {
			//System.out.println(NLS.bind(TXMCoreMessages.readCorpusInfosFromOldImportxmlFileColonP0, IMPORT));
			Document doc = DomUtils.load(IMPORT);
			Element root = doc.getDocumentElement();
			lang = root.getAttribute("language"); //$NON-NLS-1$
			baseDesc = root.getAttribute("description"); //$NON-NLS-1$
			baseName = root.getAttribute("basename"); //$NON-NLS-1$
			rootDir = root.getAttribute("rootDir"); //$NON-NLS-1$
			script = root.getAttribute("script"); //$NON-NLS-1$
			xslt = root.getAttribute("xslt"); //$NON-NLS-1$
			encoding = root.getAttribute("encoding"); //$NON-NLS-1$
			author = root.getAttribute("author"); //$NON-NLS-1$

			IMPORT.delete();
		}
		
		File PARAMS = new File(BINDIR, "import.xml"); //$NON-NLS-1$
		System.out.println("writing "+PARAMS); //$NON-NLS-1$
		BaseOldParameters.createEmptyParams(PARAMS, corpusname.toUpperCase());
		System.out.println(TXMCoreMessages.fillImportxmlWithColon);
		BaseOldParameters params = new BaseOldParameters(PARAMS);
		params.load();

		System.out.println( "corpusname: $baseName, corpusdesc: $baseDesc, rootDir: $rootDir, script: $script"+ //$NON-NLS-1$
			"\nCQPID: $corpusname, lang: $lang, xslt: $xslt, encoding: $encoding"); //$NON-NLS-1$
		Element corpusElem = params.corpora.get(corpusname);
		params.root.setAttribute("name", baseName); //$NON-NLS-1$
		params.root.setAttribute("author", author); //$NON-NLS-1$
		params.root.setAttribute("date", ""+date); //$NON-NLS-1$ //$NON-NLS-2$
		params.root.setAttribute("desc", baseDesc); //$NON-NLS-1$
		params.root.setAttribute("rootDir", rootDir); //$NON-NLS-1$
		params.root.setAttribute("script", script); //$NON-NLS-1$

		corpusElem.setAttribute("name", corpusname); //$NON-NLS-1$
		corpusElem.setAttribute("lang", lang); //$NON-NLS-1$
		corpusElem.setAttribute("encoding", encoding); //$NON-NLS-1$
		corpusElem.setAttribute("desc", baseDesc); //$NON-NLS-1$
	
		params.getXsltElement(corpusElem).setAttribute("xsl", xslt); //$NON-NLS-1$
		
		//process xml-txm files
		//params.addEditionDefinition(corpusElem, "default", "groovy", "");
		params.addEditionDefinition(corpusElem, "onepage", "groovy", ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		for (File txmFile : TXMc.listFiles(IOUtils.HIDDENFILE_FILTER)) {
			System.out.println(NLS.bind(TXMCoreMessages.processingTextColonP0, txmFile));
			String textname = txmFile.getName();
			textname = textname.substring(0, textname.lastIndexOf(".")); //$NON-NLS-1$

			Element textElem = params.addText(corpusElem, textname, txmFile);
			Element editionElem = params.addEdition(textElem, "default", HTMLc.getAbsolutePath(), "html"); //$NON-NLS-1$ //$NON-NLS-2$
			
			File[] htmlfiles = HTMLcmulti.listFiles(IOUtils.HIDDENFILE_FILTER);
			Arrays.sort(htmlfiles, new Comparator<File>() {
				@Override
				public int compare(File arg0, File arg1) {
					return getInt(arg0.getName()).compareTo(getInt(arg1.getName()));
				}
			});//.sort { a -> getInt(a.getName()); }
			
			int pagecounter = 1;
			for (File page : htmlfiles) {
				if (page.getName().startsWith(textname)) {
					String firstwordid  = Page.findFirstWordIdInFile(page);
					params.addPage(editionElem, ""+pagecounter++, firstwordid); //$NON-NLS-1$
				}
			}

			// add default Edition
			if (HTMLconepage.exists()) {
				editionElem = params.addEdition(textElem, "onepage", HTMLc.getAbsolutePath(), "html"); //$NON-NLS-1$ //$NON-NLS-2$
				htmlfiles = HTMLconepage.listFiles(IOUtils.HIDDENFILE_FILTER);
				pagecounter = 1;
				for (File page : htmlfiles) {
					if (page.getName().startsWith(textname)) {
						String firstwordid  = Page.findFirstWordIdInFile(page);
						params.addPage(editionElem, ""+pagecounter++, firstwordid); //$NON-NLS-1$
					}
				}
			}
		}

		return DomUtils.save(params.root.getOwnerDocument(), PARAMS);
	}

	private Integer getInt(String string) {
		int r = 0;
		try {
			Pattern p = Pattern.compile("([0-9]+)"); //$NON-NLS-1$
			Matcher m = p.matcher(string);
			while (m.find()) {
				string = m.group(1);
				r = Integer.parseInt(string);
			}
		} catch (PatternSyntaxException pse) { }
		return r;
	}
	
	public boolean start() throws Exception {
		if (is5(false)) {
			FileCopy.copyFiles(INDIR, OUTDIR);
			//OUTDIR.deleteDir();
			//println "copying binary into "+OUTDIR
			
			BINDIR = OUTDIR;
			HTMLs = new File(BINDIR, "HTML"); //$NON-NLS-1$
			HTMLmulti = new File(HTMLs, "multi"); //$NON-NLS-1$
			TXMs = new File(BINDIR, "txm"); //$NON-NLS-1$
			DATAs = new File(BINDIR, "data"); //$NON-NLS-1$
			REGISTRY = new File(BINDIR, "registry"); //$NON-NLS-1$
	
			IMPORT = new File(BINDIR, "import.xml"); //$NON-NLS-1$
			
			return process();
		} else
			return false;
	}

	public boolean zip(File zipFile) {
		try {
			Zip.compress(BINDIR, zipFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
//		if (args.length < 2 || args.length > 3) {
//			println "Usage: groovy Convert5to6 binaryDirV5 binaryDirV6 [zipFile]"
//			System.exit 1;
//		}
//		}
		File bindir = new File("/home/mdecorde/.TXMWEB/corporademo/ramses-bin"); //$NON-NLS-1$
		File outDir = new File("/home/mdecorde/.TXMWEB/corporademo", bindir.getName()+"6"); //$NON-NLS-1$ //$NON-NLS-2$
		//File zipFile = new File("/home/mdecorde/TXMCORPORA05/srcmf2-bin6.zip");
		
//		File bindir = new File(args[0]);
//		File outDir = new File(args[1]);
//		File zipFile = new File(outDir.getAbsolutePath()+".zip");
//		if( args.length == 3)
//			zipFile = new File(args[2]);
		
		Convert5To6 converter = new Convert5To6(bindir, outDir);
		try {
			converter.start();
			System.out.println("done"); //$NON-NLS-1$
		} catch (Exception e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		//converter.zip(zipFile);
		//	println Convert5To6.is6(new File("/home/mdecorde/TXM-corpus-backup/bfm1"))
	}
}