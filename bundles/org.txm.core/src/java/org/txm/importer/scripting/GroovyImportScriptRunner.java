// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-08-29 14:23:55 +0200 (jeu., 29 août 2013) $
// $LastChangedRevision: 2519 $
// $LastChangedBy: nilskredens $ 
//
package org.txm.importer.scripting;

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.codehaus.groovy.control.CompilationFailedException;
import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;

// TODO: Auto-generated Javadoc
/**
 * Do all the initialization job of the groovy script runner.
 *
 * @author mdecorde
 */
public class GroovyImportScriptRunner {

	/** The script. */
	File script = null;
	
	/** The classpath. */
	List<File> classpath;

	/**
	 * Instantiates a new groovy import script runner.
	 *
	 */

	public GroovyImportScriptRunner() {

	}

	/**
	 * Instantiates a new groovy import script runner.
	 *
	 * @param script the script
	 * @param classpath the classpath
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public GroovyImportScriptRunner(File script, List<File> classpath)
			throws IOException {
		this.script = script;
		this.classpath = classpath;
	}

	/**
	 * Instantiates a new groovy import script runner.
	 *
	 * @param scriptfile the scriptfile
	 * @param args the args
	 */
	public GroovyImportScriptRunner(File scriptfile, Object[] args) {
		System.out.println(TXMCoreMessages.initializationOfGroovyImportScriptRunner);
		this.executeScript(scriptfile.getAbsolutePath(), args);
	}

	/**
	 * Execute script.
	 *
	 * @param scriptfile the scriptfile
	 * @param args the args
	 */
	public void executeScript(String scriptfile, Object args) {
		System.out.println(NLS.bind(TXMCoreMessages.supBeginningExecutionOfP0, scriptfile));
		ClassLoader parent = getClass().getClassLoader();
		GroovyClassLoader loader = new GroovyClassLoader(parent);
		Class groovyClass = null;
		try {
			groovyClass = loader.parseClass(new File(scriptfile));
		} catch (CompilationFailedException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}

		// let's call some method on an instance
		GroovyObject groovyObject = null;
		try {
			groovyObject = (GroovyObject) groovyClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}

		try {
			groovyObject.invokeMethod("run", args); //$NON-NLS-1$
		} catch (Exception e) {
			System.err.println(e);
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		System.out.println(TXMCoreMessages.supEndOfExecution);
	}

	/**
	 * Run.
	 *
	 * @param groovyloader the groovyloader
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void run(GroovyClassLoader groovyloader) throws IOException {

		// gse

	}
}