// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2017-04-11 15:30:35 +0200 (mar. 11 avril 2017) $
// $LastChangedRevision: 3426 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer;

import org.txm.utils.i18n.DetectBOM;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import javax.xml.stream.*;
import java.net.URL;

// TODO: Auto-generated Javadoc
/**
 * The Class FortranColumns2XML.
 */
class FortranColumns2XML {
	
	/** The infile. */
	
	File infile;
	
	/** The outfile. */
	File outfile;
	
	/** The encoding. */
	String encoding;
	
	/** The column names. */
	List<String> columnNames
	
	/** The word tag. */
	String wordTag;
	
	/** The wordcolumn. */
	int wordcolumn;
	
	/** The linecounter. */
	int linecounter = 0;
	
	/** The check line number. */
	int checkLineNumber;
	
	/** The check line number tag. */
	String checkLineNumberTag;
	
	/** The columnindex. */
	private List<Integer> columnindex;
	
	/** The endcolumnindex. */
	private List<Integer> endcolumnindex;
	
	/** The line. */
	private String line;
	
	/** The writer. */
	private XMLStreamWriter writer;
	
	/** The reader. */
	private Reader reader;
	
	/** The output. */
	private FileOutputStream output;
	
	String textname;
	
	/**
	 * Instantiates a new fortran columns2 xml.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 * @param encoding the encoding
	 */
	FortranColumns2XML(File infile, File outfile, String encoding)
	{
		this.infile = infile;
		this.outfile = outfile;
		
		textname = infile.getName();
		int idx = -1;
		idx = textname.lastIndexOf(".");
		if (idx > 0)
			textname = textname.substring(0, idx);

		def input = new FileInputStream(infile) 		
		reader = new InputStreamReader(input , encoding);
		DetectBOM bomdetector = new DetectBOM(infile);
		for (int ibom = 0 ; ibom < bomdetector.getBOMSize() ; ibom++) input.read()
		
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		output = new FileOutputStream(outfile)
		writer = factory.createXMLStreamWriter(output, "UTF-8");
	}
	
	/**
	 * Process.
	 *
	 * @return true, if successful
	 */
	public boolean process()
	{
		line = reader.readLine(); // use the first line to find columns 
		
		if(columnindex == null || endcolumnindex == null)
		{
			searchNumberOfColumns();
		}
		
		if(columnindex.size() != columnNames.size())
		{
			System.err.println("Number of column("+columnindex.size()+") different of number of column name("+columnNames.size()+")");
			return false;
		}
		
		//write start document
		writer.writeStartDocument("UTF-8", "1.0");
		writer.writeStartElement("text")
		writer.writeAttribute("id",infile.getName())
		
		boolean gogogo = true;
		while(gogogo)
		{	
			//println "line : "+line
			processLine();//split line accordinly to columnindexes
			linecounter++;
			
			//continue to read the document
			line = reader.readLine();
			if(line == null)
				gogogo= false;
		}
		
		writer.writeEndElement();
		writer.writeEndDocument();
		writer.close();
		output.close();
		
		return true;
	}
	
	/**
	 * Process line.
	 */
	int wordcount = 1;
	private void processLine()
	{
		String pcdata;
		if(line.length() > 0)
		{
			writer.writeStartElement(wordTag)
			if(setIds)
				writer.writeAttribute("id", "w_"+textname+"_"+(wordcount++));
			for(int i = 0 ; i < columnindex.size(); i++)
			{
				if(line.length() > endcolumnindex.get(i))
				{
					String substring = line.substring(columnindex.get(i), endcolumnindex.get(i))
					if(i != wordcolumn)
					{
						writer.writeAttribute(columnNames.get(i), substring.trim())
					}
					else
					{
						pcdata = substring.trim()
					}
				}
				else
				{
					println "Error: line $linecounter: len="+line.length()+" maxidx="+endcolumnindex.get(i)
					println line;
				}
			}
			writer.writeCharacters(pcdata);
			writer.writeEndElement();
		}
	}
	
	/**
	 * Sets the column indexes.
	 *
	 * @param starts the starts
	 * @param ends the ends
	 */
	private void setColumnIndexes(List<Integer> starts, List<Integer> ends)
	{
		columnindex = starts;
		endcolumnindex = ends;
	}
	
	boolean setIds = false;
	public void setAddIds(boolean setIds)
	{
		this.setIds = setIds;
	}
	
	/**
	 * Search number of columns.
	 */
	private void searchNumberOfColumns()
	{
		columnindex = new ArrayList<Integer>();
		endcolumnindex = new ArrayList<Integer>();
		columnindex.add(0);
		int ic = 0
		char c;
		char[] cline = line.toCharArray();
		int state = 0; // 0 lettre, 1 espace
		
		for(ic = 0 ; ic < cline.length ; ic++)
		{
			c = cline[ic];
			if(state == 0)// zone lettre
			{
				if(c == " ")
				{
					state = 1;
					
				}
			}
			else // zone espace
			{
				if(c != " ")
				{
					columnindex.add(ic);
					endcolumnindex.add(ic-1);
					state = 0;
				}
			}
		}
		endcolumnindex.add(cline.length);
		println columnindex;
		println endcolumnindex;
	}
	
	/**
	 * Sets the columns lines names.
	 *
	 * @param columnNames the column names
	 * @param unitTag the unit tag
	 * @param wordcolumn the wordcolumn
	 */
	public void setColumnsLinesNames(List<String> columnNames, String unitTag, int wordcolumn)
	{
		this.columnNames = columnNames;
		this.wordTag = unitTag;
		this.wordcolumn = wordcolumn;
	}
	
	/**
	 * Sets the line check.
	 *
	 * @param colindex the colindex
	 * @param tagname the tagname
	 */
	public void setLineCheck(int colindex, String tagname)
	{
		this.checkLineNumber = colindex;
		this.checkLineNumberTag = tagname;
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File fortranfile = new File(System.getProperty("user.home"),"\\xml\\lasla\\lasla\\L01PlaAmphi.APN");
		File outfile = new File(System.getProperty("user.home"),"\\xml\\lasla\\out.xml");
		FortranColumns2XML builder = new FortranColumns2XML(fortranfile, outfile, "iso-8859-1");
		builder.setColumnIndexes([0, 3, 4, 8, 29, 55, 67],	[3, 4, 8, 28, 54, 66, 79]);
		builder.setColumnsLinesNames(["ref","s","sent","word","lemme","line","pos"], "w", 3);
		builder.setLineCheck(3, "l");
		
		builder.process();
	}
}


