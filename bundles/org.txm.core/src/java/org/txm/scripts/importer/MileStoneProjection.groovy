package org.txm.scripts.importer

import javax.xml.stream.*;
import org.txm.importer.StaxIdentityParser

/**
 * add 2 attributes per element to encode the distance to the previous milestone and next milestone
 * TODO: only the first attribute is implemented
 * 
 * @author mdecorde
 *
 */
class MileStoneProjection extends StaxIdentityParser {
	String wordTag
	String mileStoneTag
	String startTag
	boolean start = false
	
	int mileStoneDistance = 0
	
	String mileStoneID = ""
	def milestonesLength = []
	int milestonesCounter = 0
	boolean secondPass = false
	
	String msIdAttributeName
	String msStartAttributeName
	String msEndAttributeName
	
	public MileStoneProjection(File inputFile, String startTag, String wordTag, String mileStoneTag) {
		super(inputFile)
		
		this.wordTag = wordTag
		this.mileStoneTag = mileStoneTag
		mileStoneID = mileStoneTag+"_0"
		this.startTag = startTag
		this.start = (startTag == null);
		
		msIdAttributeName = mileStoneTag+"id";
		msStartAttributeName = mileStoneTag+"start";
		msEndAttributeName = mileStoneTag+"end";
		
		fetchMilestoneLengths();
		this.start = (startTag == null);
	}
	
	public void fetchMilestoneLengths() {
		def inputData = inputurl.openStream();
		def factory = XMLInputFactory.newInstance();
		def parser = factory.createXMLStreamReader(inputData);
		
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName()
					if (start && localname.equals(mileStoneTag)) {
						milestonesLength << mileStoneDistance;
						mileStoneDistance = 0
						milestonesCounter++;
					} else if (start && localname.equals(wordTag)) {
						mileStoneDistance++
					} else if (localname.equals(startTag)) {
						start = true
					}
					break;
			}
		}
		milestonesLength[milestonesCounter] = mileStoneDistance;
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
		milestonesCounter = 0;
		//println milestonesLength
	}
	
	
	
	public void processStartElement() {
		super.processStartElement();
		
		if (start && localname.equals(mileStoneTag)) {
			//println "START MILESTONE"
			mileStoneDistance = 0
			//println parser
			mileStoneID = parser.getAttributeValue(null, "id")
			if (mileStoneID == null) mileStoneID = parser.getAttributeValue("xml", "id");
			if (mileStoneID == null) mileStoneID = "0";
			
			milestonesCounter++;
		} else if (start && localname.equals(wordTag)) {
			//println "START W"
			// println "end of $milestonesCounter len="+milestonesLength[milestonesCounter]+" dist="+mileStoneDistance
			writer.writeAttribute(msEndAttributeName, Integer.toString((milestonesLength[milestonesCounter] - mileStoneDistance - 1)))
			writer.writeAttribute(msStartAttributeName, Integer.toString(mileStoneDistance))
			writer.writeAttribute(msIdAttributeName, mileStoneID)
			
			mileStoneDistance++
		} else if (localname.equals(startTag)) {
			start = true
		}
	}
	
	public static void main(String[] args) {
		File inputFile = new File(System.getProperty("user.home"), "runtime-rcpapplication.product/corpora/XTZMILESTONES2/tokenized/test.xml")
		File outputFile = new File(System.getProperty("user.home"), "runtime-rcpapplication.product/corpora/XTZMILESTONES2/tokenized/result.xml")
		
		MileStoneProjection msp = new MileStoneProjection(inputFile, null, "w", "lb");
		println "Sucess: "+msp.process(outputFile)
	}
}
