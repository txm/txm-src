package org.txm.scripts.importer

import org.txm.metadatas.Metadatas
import org.txm.utils.io.IOUtils

File dir = new File("/home/mdecorde/xml/voeux/split_xml")
File outdir = new File("/home/mdecorde/xml/voeux/split_txtcsv")
println "1) xml -> txt + write metadata.csv"
outdir.deleteDir()
outdir.mkdir()
File metadatafile = new File(outdir, "metadata.csv")
String csvString = ""

def files = dir.listFiles(IOUtils.HIDDENFILE_FILTER)
files.sort()
for(File f : files) {
	File outfile = new File(outdir, f.getName()+".txt");
	String text = f.getText("UTF-8");
	String texttag = text.find("<text id.*>")
//	println texttag
//	texttag = texttag.replaceAll('<text id="([^"]+)"', '<text id="$1.txt"')
//	println "> "+texttag
	text = text.replaceAll("<text.*>", "").replace("</text>", "");
	outfile.withWriter("UTF-8"){writer -> writer.write(text) }
	csvString += texttag.replace("<text id=","").replace(" loc=", ",").replace(" annee=", ",").replace("\">", "\"")+"\n"
}

println "2) write metadata.csv"
metadatafile.withWriter("UTF-8"){csvwriter -> 
	csvwriter.write("\"id\",\"loc\",\"annee\"\n");
	csvwriter.write(csvString)}

println "3) rename Voeux_*"
outdir.eachFileMatch(~/Voeux_.*/) {file-> file.renameTo(new File(outdir, file.getName().substring(6, 10)+".txt")) }
