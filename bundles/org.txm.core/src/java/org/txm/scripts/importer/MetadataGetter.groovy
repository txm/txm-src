package org.txm.scripts.importer

import javax.xml.stream.*;

class MetadataGetter {
	public static String get(File xmlFile, String struct, String prop) {

		def inputData = xmlFile.toURI().toURL().openStream();
		def factory = XMLInputFactory.newInstance();
		XMLStreamReader parser = factory.createXMLStreamReader(inputData);

		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			if (event == XMLStreamConstants.START_ELEMENT) {
				if ("text".equals(parser.getLocalName())) {
					for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
						if(prop.equals(parser.getAttributeLocalName(i))) {
							String tmp = parser.getAttributeValue(i)
							if (parser != null) parser.close();
							if (inputData != null) inputData.close();
							return tmp
						}
					}
				}
			}
		}
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
		return null;
	}

	public static void main(String[] args) {
		File xmlFile = new File("/home/mdecorde/TXM/corpora/superphenix/txm/SUPERPHENIX/CreysSuper_04_0175.xml")
		String struct = "text"
		String prop = "date"
		println MetadataGetter.get(xmlFile, struct, prop)
	}
}
