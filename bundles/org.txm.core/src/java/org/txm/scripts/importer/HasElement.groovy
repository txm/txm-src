package org.txm.scripts.importer

import javax.xml.stream.*;
import java.net.URL;

/**
 * Check if a file contains a tag
 * @author mdecorde
 *
 */
class HasElement {

	HashSet<String> localnames = new HashSet<String>();
	File xmlfile;
	def inputData
	XMLStreamReader parser;
	HashMap<String, Boolean> elements = [:];

	public HasElement(File xmlfile, String localname) {
		localnames << localname;
		this.xmlfile = xmlfile;
		elements[localname] = false;

		init();
	}

	private void init() {
		try {
			
//			println "xmlfile: "+xmlfile
//			println "xmlfile exists: "+xmlfile.exists()
//			println "uri: "+ xmlfile.toURI()
//			println "url: "+ xmlfile.toURI().toURL()
//			
			inputData = xmlfile.toURI().toURL().openStream();
			def inputFactory = XMLInputFactory.newInstance();
			parser = inputFactory.createXMLStreamReader(inputData);
		} catch (Exception ex) {
			System.out.println("IOException while parsing: "+ex);
			ex.printStackTrace();
		}
	}

	public boolean process() {
		if (inputData == null) return false;
		if (parser == null) {
			inputData.close()
			return false;
		}
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			if (event == XMLStreamConstants.START_ELEMENT) {
				String localname = parser.getLocalName();
				if (localnames.contains(localname)) {
					parser.close();
					inputData.close();
					return true;
				}
			}
		}
		parser.close();
		inputData.close();
		return false;
	}
	
	public static void main(String[] args) {
		File dir = new File("/home/mdecorde/xml/lamoplat")
		println dir.listFiles()
		for (File xmlfile : dir.listFiles()) {
			if (!xmlfile.getName().endsWith(".xml")) continue
			//println xmlfile
			println "$xmlfile: "+ new HasElement(xmlfile, "text").process();
		}
	}
}
