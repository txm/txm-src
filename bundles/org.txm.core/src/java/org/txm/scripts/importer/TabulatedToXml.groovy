package org.txm.scripts.importer 
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

import org.txm.utils.io.IOUtils

// TODO: Auto-generated Javadoc
/**
 * Tool to convert Tabulated file to xml file. Such as CWB files or TreeTagger ouput
 */
public class TabulatedToXml {
	String[] colnames;
	
	/** The texttag. */
	String texttag;
	
	/** The tag. */
	boolean tag;
	
	/** The no col form. */
	int noColForm;
	
	/**
	 * Instantiates a new tabulated to xml.
	 *
	 * @param colnames the word properties name
	 * @param texttag the element that represents a text unit
	 * @param tag if true, split the file by the texttag, if false wrap the file with an element texttag
	 */
	public TabulatedToXml(String[] colnames, String texttag, boolean tag) throws Exception
	{
		this.colnames = colnames;
		this.texttag = texttag;
		this.tag = tag;
		this.noColForm = 0;
	}
	
	/**
	 * Instantiates a new tabulated to xml.
	 *
	 * @param colnames the colnames
	 * @param texttag the texttag
	 * @param tag the tag
	 * @param noColForm the no col form
	 */
	public TabulatedToXml(String[] colnames, String texttag, boolean tag, int noColForm) throws Exception
	{
		this.colnames = colnames;
		this.texttag = texttag;
		this.tag = tag;
		this.noColForm = noColForm;
	}
	
	/**
	 * Process.
	 *
	 * @param tabulatedfile the tabulatedfile
	 * @param xmlfile the xmlfile
	 * @param encoding the encoding
	 * @return true, if successful
	 */
	public boolean process(File tabulatedfile, File xmlfile, String encoding)
	{
		BufferedReader reader = new BufferedReader(new FileReader(tabulatedfile));
		FileOutputStream output = new FileOutputStream(xmlfile)
		
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = factory.createXMLStreamWriter(output, "UTF-8"); 
		
		writer.writeStartDocument("UTF-8", "1.0")
		
		if(!tag) // there is no tag, we must wrap the content in an element
		{
			if(texttag.length() == 0)
			{
				println "no text element given"
				return false;
			}
			writer.writeStartElement(texttag);
			String filename = tabulatedfile.getName()
			filename = filename.substring(0, filename.lastIndexOf("."));
			writer.writeAttribute("id", filename);
		}
		
		String line = reader.readLine();
		int noline = 0;
		while(line != null)
		{
			if(tag && line.startsWith("</")) // closing xml tag
			{
				writer.writeEndElement();
			}
			else if(tag && line.startsWith("<")) // opening xml tag
			{
				int firstblank = line.indexOf(" ");
				String tagname;
				String[] attrnamevalues = null;
				if(firstblank > 0) // contains attributes
				{
					tagname = line.substring(1, firstblank);
					line = line.substring(firstblank+1, line.length() -1);
					attrnamevalues = line.split(" ");
				}
				else // no attr
				{
					tagname = line.substring(1, line.length() -1);
				}
				
				writer.writeStartElement(tagname);
				if(attrnamevalues != null)
				{
					for(String namevalue : attrnamevalues)
					{
						int egalidx = namevalue.indexOf("=");
						String name = namevalue.substring(0, egalidx);
						String value = namevalue.substring(egalidx+1, namevalue.length());
						
						writer.writeAttribute(name, value.replace("\"",""));
					}
				}
			}
			else // it's a word
			{
				String[] split = line.split("\t");
				if(split.length != colnames.length)
				{
					System.out.println("Error line "+noline+" : missing columns :"+split.length+" instead of "+ colnames.length);
					println "line: "+line
				}
				String form = "";
				writer.writeStartElement("w");
				for(int i = 0 ; i < colnames.length ; i++)
				{
					if(i == noColForm)
					{
						form = split[0];
					}
					else
					{
						if(split.length <= i)
						writer.writeAttribute(colnames[i], "N/A");
						else
						writer.writeAttribute(colnames[i], split[i]);
					}
				}
				writer.writeCharacters(form);
				writer.writeEndElement();
				
			}
			noline++;
			line = reader.readLine();
		}
		
		if(!tag) // there is no tag, we close the wrapping element
		{
			writer.writeEndElement();
		}
		
		writer.close();
		output.close();
		reader.close();
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		String home = System.getProperty("user.home");
		// sample_TXM_LA.vrt
		/*
		 File tabulatedfile = new File(home, "xml/tabulated/sample_TXM_LA.vrt");
		 File xmlfile = new File(home, "xml/tabulated/sample_TXM_LA.xml");
		 String[] colnames = ["form", "pos", "lem", "func"]; // TreeTagger
		 boolean tag = true;
		 String encoding = "UTF-8";
		 try {
		 TabulatedToXml ttx = new TabulatedToXml(colnames, "", tag);
		 ttx.process(tabulatedfile, xmlfile, encoding)
		 } catch (Exception e) {
		 // TODO Auto-generated catch block
		 org.txm.utils.logger.Log.printStackTrace(e);
		 }*/
		File srcdir = new File(home, "xml/Bendinelli");
		File outdir = new File(home, "xml/Bendinelli/xml");
		println "srcdir: "+srcdir
		outdir.deleteDir();
		outdir.mkdir();
		
		String[] colnames = ["form", "pos", "lem"]; // TreeTagger
		boolean tag = false;
		String encoding = "UTF-8";
		
		TabulatedToXml ttx = new TabulatedToXml(colnames, "debat", tag);
		for(File tabulatedfile : srcdir.listFiles(IOUtils.HIDDENFILE_FILTER))
		{
			if(tabulatedfile.getName().endsWith(".txt"))
			{
				String filename = tabulatedfile.getName()
				filename = filename.substring(0, filename.lastIndexOf("."));
				File xmlfile = new File(outdir, filename+".xml")
				println "process: "+tabulatedfile
				ttx.process(tabulatedfile, xmlfile, encoding)
			}
		}
		
		println "Done"
	}
}