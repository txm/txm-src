package org.txm.scripts.importer;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

@SuppressWarnings("rawtypes")
public class StaxStackWriter implements XMLStreamWriter {
	
	XMLStreamWriter writer;
	OutputStream output;
	List<List<List>> events = new ArrayList<>();
	boolean debug = false;
	
	public StaxStackWriter(File file) throws FileNotFoundException, XMLStreamException {
		
		this(new BufferedOutputStream(new FileOutputStream(file)));
	}
	
	public StaxStackWriter(File file, String encoding) throws FileNotFoundException, XMLStreamException {
		
		this(new BufferedOutputStream(new FileOutputStream(file)), encoding);
	}
	
	public StaxStackWriter(OutputStream output) throws XMLStreamException {
		
		this(output, "UTF-8");//create a new file
	}
	
	public StaxStackWriter(OutputStream output, String encoding) throws XMLStreamException {
		
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		this.output = output;
		this.writer = factory.createXMLStreamWriter(output, encoding);//create a new file
	}
	
	Charset UTF8 = Charset.forName("UTF-8");
	public void write(String txt) throws IOException {
		
		output.write(txt.getBytes(UTF8));
	}
	
	public void writeEndElements() throws XMLStreamException {
		
		int size = events.size();
		for (int i = 0 ; i < size ; i++) {
			writeEndElement();
			this.writer.writeCharacters("\n");
		}
		
		if (events.size() > 0) {
			System.out.println("Warning: XML events should be empty and its not: "+events);
		}
		
		events.clear();
	}
	
	/**
	 * tagsToWrite: {@code List<String> or List<[String, [String, String]]>}
	 * @throws XMLStreamException 
	 */
	public void writeStartElements(List<Object> tagsToWrite) throws XMLStreamException {
		
		for (Object tag : tagsToWrite) {
			if (tag instanceof String) {
				writeStartElement((String)tag);
			} else {
				List l = (List)tag;
				writeStartElement((String)l.get(0));
				for (Object att : (List)l.get(1)) {
					List l2 = (List)att;
					writeAttribute(l2.get(0).toString(), l2.get(1).toString());
				}
			}
		}
		//System.out.println("reopened: "+events);
	}
	
	public List<List<List>> getTagStack() {
		
		return events;
	}
	
	@Override
	public void close () throws XMLStreamException {
		
		writer.close();
		try {
			output.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void flush () throws XMLStreamException {
		
		writer.flush();
	}
	
	@Override
	public NamespaceContext getNamespaceContext() {
		
		return writer.getNamespaceContext();
	}
	
	@Override
	public String getPrefix (String uri) throws XMLStreamException {
		
		return writer.getPrefix(uri);
	}
	
	@Override
	public Object getProperty (String name) throws IllegalArgumentException {
		
		return writer.getProperty(name);
	}
	
	@Override
	public void setDefaultNamespace (String uri) throws XMLStreamException {
		
		writer.setDefaultNamespace(uri);
	}
	
	@Override
	public void setNamespaceContext(NamespaceContext context) throws XMLStreamException {
		
		writer.setNamespaceContext(context);
	}
	
	@Override
	public void setPrefix (String prefix, String uri) throws XMLStreamException {
		
		writer.setPrefix(prefix, uri);
	}
	
	@Override
	public void writeAttribute (String localName, String value) throws XMLStreamException {
		
		if (value == null) return; // skip if value is null
		if (localName == null) return; // skip if localName is null
		writer.writeAttribute(localName, value);
		
		if (events.size() > 0) events.get(events.size()-1).get(1).add(Arrays.asList(localName, value));
	}
	
	@Override
	public void writeAttribute (String namespaceURI, String localName, String value) throws XMLStreamException {
		
		if (value == null) return; // skip if value is null
		if (localName == null) return; // skip if localName is null
		writer.writeAttribute(namespaceURI, localName, value);
		
		if (events.size() > 0) events.get(events.size()-1).get(1).add(Arrays.asList(localName, value));
	}
	
	@Override
	public void writeAttribute (String prefix, String namespaceURI, String localName, String value) throws XMLStreamException {
		
		if (value == null) return; // skip if value is null
		if (localName == null) return; // skip if localName is null
		writer.writeAttribute(prefix, namespaceURI, localName, value);
		
		if (events.size() > 0) events.get(events.size()-1).get(1).add(Arrays.asList(localName, value));
	}
	
	@Override
	public void writeCData (String data) throws XMLStreamException {
		
		if (data == null) return;
		writer.writeCData(data);
	}
	
	@Override
	public void writeCharacters (String text) throws XMLStreamException {
		
		if (text == null) return;
		writer.writeCharacters(text);
	}
	
	@Override
	public void writeCharacters (char[] text, int start, int len) throws XMLStreamException {
		
		if (text == null) return;
		writer.writeCharacters(text, start, len);
	}
	
	@Override
	public void writeComment (String data) throws XMLStreamException {
		
		if (data == null) return;
		writer.writeComment(data);
	}
	
	@Override
	public void writeDTD (String dtd) throws XMLStreamException {
		
		if (dtd == null) return;
		writer.writeDTD(dtd);
	}
	
	@Override
	public void writeDefaultNamespace(String namespaceURI) throws XMLStreamException {
		
		writer.writeDefaultNamespace(namespaceURI);
	}
	
	/**
	 * WARNING THE ELEMENT IS CLOSED
	 * @param localName
	 * @param text
	 * @throws XMLStreamException
	 */
	public void writeElement(String localName, String text) throws XMLStreamException {
		
		if (text != null && text.length() > 0) {
			writer.writeStartElement(localName);
			writer.writeCharacters(text);
			writer.writeEndElement();
		} else {
			writer.writeEmptyElement(localName);
		}
	}
	
	/**
	 * write an entire XML Element (start-attributes-text content-end)
	 * @param localName
	 * @param map attributes map
	 * @param text
	 * @throws XMLStreamException
	 */
	public void writeElement(String localName, HashMap map, String text) throws XMLStreamException {
		
		if (text != null && text.length() > 0) {
			writer.writeStartElement(localName);
			events.add(new ArrayList(Arrays.asList(localName, new ArrayList()))); // must be done or else writeAttribute will insert the attribute in the previous element
			
			for (Object key : map.keySet()) {
				if (map.get(key) != null) { // avoid NPE
					writeAttribute(key.toString(), map.get(key).toString());
				}
			}
			if (text != null) writer.writeCharacters(text);
			writer.writeEndElement();
			
			events.remove(events.size()-1);
		} else {
			writeEmptyElement(localName, map);
		}
	}
	
	@Override
	public void writeEmptyElement (String localName) throws XMLStreamException {
		
		writer.writeEmptyElement(localName);
	}
	
	public void writeEmptyElement (String localName, HashMap map) throws XMLStreamException {
		
		writer.writeEmptyElement(localName);
		events.add(new ArrayList(Arrays.asList(localName, new ArrayList()))); // must be done or else writeAttribute will insert the attribute in the previous element
		
		for (Object key : map.keySet()) {
			if (map.get(key) != null) { // avoid NPE
				writeAttribute(key.toString(), map.get(key).toString());
			}
		}
		events.remove(events.size()-1);
	}
	
	@Override
	public void writeEmptyElement(String namespaceURI, String localName) throws XMLStreamException {
		
		writer.writeEmptyElement(namespaceURI, localName);
	}
	
	@Override
	public void writeEmptyElement (String prefix, String localName, String namespaceURI) throws XMLStreamException {
		
		writer.writeEmptyElement(prefix, localName, namespaceURI);
	}
	
	@Override
	public void writeEndDocument () throws XMLStreamException {
		
		writer.writeEndDocument();
	}
	
	@Override
	public void writeEndElement () throws XMLStreamException {
		
		writer.writeEndElement();
		events.remove(events.size()-1);
		if (debug) System.out.println("END: "+events);
	}
	
	@Override
	public void writeEntityRef (String name) throws XMLStreamException {
		
		writer.writeEntityRef(name);
	}
	
	@Override
	public void writeNamespace(String prefix, String namespaceURI) throws XMLStreamException {
		
		writer.writeNamespace(prefix, namespaceURI);
	}
	
	@Override
	public void writeProcessingInstruction(String target) throws XMLStreamException {
		
		writer.writeProcessingInstruction(target);
	}
	
	@Override
	public void writeProcessingInstruction(String target, String data) throws XMLStreamException {
		
		writer.writeProcessingInstruction(target, data);
	}
	
	@Override
	public void writeStartDocument () throws XMLStreamException {
		
		writer.writeStartDocument();
	}
	
	@Override
	public void writeStartDocument (String version) throws XMLStreamException {
		
		writer.writeStartDocument(version);
	}
	
	@Override
	public void writeStartDocument(String encoding, String version) throws XMLStreamException {
		
		writer.writeStartDocument(encoding, version);
	}
	
	@Override
	public void writeStartElement (String localName) throws XMLStreamException {
		
		writer.writeStartElement(localName);
		events.add(new ArrayList(Arrays.asList(localName, new ArrayList())));
		if (debug) System.out.println("START "+localName+" "+events);
	}

	public void writeStartElement (String localName, HashMap map) throws XMLStreamException {
		
		writeStartElement(localName);
		
		for (Object key : map.keySet()) {
			
			if (map.get(key) != null) { // avoid NPE
				writeAttribute(key.toString(), map.get(key).toString());
			}
		}
	}
	
	@Override
	public void writeStartElement(String namespaceURI, String localName) throws XMLStreamException {
		
		writer.writeStartElement(namespaceURI, localName);
		events.add(new ArrayList(Arrays.asList(localName, new ArrayList())));
		
		if (debug) System.out.println("START "+localName+" "+events);
	}
	
	@Override
	public void writeStartElement(String prefix, String localName, String namespaceURI) throws XMLStreamException {
		
		writer.writeStartElement(prefix, localName, namespaceURI);
		events.add(new ArrayList(Arrays.asList(localName, new ArrayList())));
		if (debug) System.out.println("START "+prefix+" "+localName+" "+events);
	}
}
