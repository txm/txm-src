package org.txm.scripts.importer

import javax.xml.stream.*;

import org.txm.importer.*

import groovy.transform.CompileStatic
import org.txm.utils.logger.Log

@CompileStatic
class StaxParser {

	protected URL inputurl;
	protected InputStream inputData;
	protected XMLInputFactory factory;
	protected XMLStreamReader parser;
	
	
	protected static PersonalNamespaceContext Nscontext = new PersonalNamespaceContext();
	
	protected String localname;
	
	public StaxParser(File infile)
	{
		this(infile.toURI().toURL());
	}
	
	public StaxParser(URL inputurl)
	{
		this.inputurl = inputurl;
		inputData = inputurl.openStream();
		factory = XMLInputFactory.newInstance();
		
		parser = factory.createXMLStreamReader(inputData);
	}
	
	protected void before()
	{
		
	}
	
	protected void after()
	{
		factory = null;
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
		parser = null;
	}
	
	protected void closeForError() {
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
	}

	public boolean process()
	{
		try {
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				switch (event) {
					case XMLStreamConstants.NAMESPACE:
						processNamespace();
						break;
					case XMLStreamConstants.START_ELEMENT:
						localname = parser.getLocalName();
						processStartElement();
						break;		
					case XMLStreamConstants.CHARACTERS:
						processCharacters();
						break;
					case XMLStreamConstants.PROCESSING_INSTRUCTION:
						processProcessingInstruction();
						break;
					case XMLStreamConstants.DTD:
						processDTD();
						break;
					case XMLStreamConstants.CDATA:
						processCDATA();
						break;
					case XMLStreamConstants.COMMENT:
						processComment();
						break;
					case XMLStreamConstants.END_ELEMENT:
						localname = parser.getLocalName();
						processEndElement();	
						break;
					case XMLStreamConstants.END_DOCUMENT:
						processEndDocument();
						break;
					case XMLStreamConstants.ENTITY_REFERENCE:
						processEntityReference();
						break;
				}
			}
		} catch(Exception e) {
			println("Unexpected error while parsing file "+inputurl+" : "+e);
			println("Location line: "+parser.getLocation().getLineNumber()+" character: "+parser.getLocation().getColumnNumber());
			Log.printStackTrace(e);
			if (parser != null) parser.close();
		if (inputData != null) inputData.close();
			return false;
		}
		
		after(); // if you need to do something before closing the parser();
		return true;
	}

	public def getLocation() {
		if (parser != null)
			return "Line: "+parser.getLocation().getLineNumber()+" Col: "+parser.getLocation().	getColumnNumber()
		return null;
	}
	
	protected void processNamespace() {
	}
	
	protected void processStartElement()
	{
	}

	protected void processCharacters()
	{
	}
	
	protected void processProcessingInstruction()
	{
	}
	
	protected void processDTD()
	{
	}
	
	protected void processCDATA()
	{
	}
	
	protected void processComment()
	{
	}
	
	protected void processEndElement()
	{
	}
	
	protected void processEndDocument() {
	}
	
	protected void processEntityReference() {
	}
	
	public static void main(String[] args)
	{
		File input = new File("/home/mdecorde/xml/xiinclude/master.xml")
		File output = new File("/home/mdecorde/xml/xiinclude/merged.xml")
		if (!(input.exists() && input.canRead())) {
			println "cannot found $input";
			return;
		}
		def builder = new StaxParser(input.toURI().toURL());
		if (builder.process()) {
			println "success ? "+ValidateXml.test(output);
		} else {
			println "failure !"
		}
	}
}