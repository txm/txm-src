// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.scripts.importer

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.*;
import javax.xml.xpath.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.txm.importer.PersonalNamespaceContext;
import org.txm.importer.StaxIdentityParser

// TODO: Auto-generated Javadoc
/**
 * The Class RenameTag : using Stax or DOM depending on the file size
 */
class RenameTag extends StaxIdentityParser{

	public static int DOMMAXSIZE = 1000000;
	String newname, oldname;
	File xmlfile;

	public RenameTag(File xmlfile, String oldname, String newname)
	{
		super(xmlfile.toURI().toURL());
		this.oldname = oldname;
		this.newname = newname;
		this.xmlfile = xmlfile;
	}
	/**
	 * Rename an element - DOM method
	 *
	 * @param xmlfile the xmlfile
	 * @param oldname the oldname
	 * @param newname the newname
	 * @return true, if successful
	 */
	public static boolean rename(File xmlfile, String oldname, String newname)
	{
		if (!xmlfile.exists()) {
			println "Error: $xmlfile does not exists"
		}

		if (!(xmlfile.canRead() && xmlfile.canWrite())) {
			println "Error: $xmlfile is not readable or writable"
		}

		if (xmlfile.length() <= DOMMAXSIZE) {
			String xpath = "//"+oldname;
			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
			domFactory.setXIncludeAware(true);
			domFactory.setNamespaceAware(true); // never forget this!
			DocumentBuilder builder = domFactory.newDocumentBuilder();
			Document doc = builder.parse(xmlfile);

			XPathFactory xpathfactory = XPathFactory.newInstance();
			def _xpath = xpathfactory.newXPath();
			_xpath.setNamespaceContext(new PersonalNamespaceContext());
			def expr = _xpath.compile(xpath);
			def nodes = expr.evaluate(doc, XPathConstants.NODESET);

			for(def node : nodes) {
				doc.renameNode(node, null, newname)
			}
			return org.txm.utils.xml.DomUtils.save(doc, xmlfile)
		}
		else {
			RenameTag builder = new RenameTag(xmlfile, oldname, newname);
			File temp = File.createTempFile("temp", ".xml", xmlfile.getParentFile());
			if(builder.process(temp)) {
				if (!(xmlfile.delete() && temp.renameTo(xmlfile))) println "Warning can't rename file "+temp+" to "+xmlfile
				return true;
			}
			else
				return false;
		}
	}

	/**
	 * Rewrite processStartElement to update the localname if localname == oldname
	 */
	public void processStartElement()
	{
		String prefix = parser.getPrefix();
		if (INCLUDE == localname && XI == prefix) {
			processingXInclude();
			return;
		}

		if (localname == oldname) {
			localname = newname;

			if(prefix.length() > 0)
				writer.writeStartElement(Nscontext.getNamespaceURI(prefix), localname)
			else
				writer.writeStartElement(localname);

			for(int i = 0 ; i < parser.getNamespaceCount() ; i++)
			{
				writer.writeNamespace(parser.getNamespacePrefix(i), parser.getNamespaceURI(i));
			}

			for(int i = 0 ; i < parser.getAttributeCount() ; i++)
				writer.writeAttribute(parser.getAttributeLocalName(i), parser.getAttributeValue(i));
		} else {
			super.processStartElement();
		}
	}

	public static void main(String[] args)
	{
		File xmlfile = new File("/home/mdecorde/xml/xmlçàé/essai.xml");
		File temp = new File("/home/mdecorde/xml/xmlçàé/renamed.xml");
		String oldname = "machin"
		String newname = "titre"
		long time = System.currentTimeMillis();
		RenameTag.rename(xmlfile, oldname, newname);
		//		RenameTag builder = new RenameTag(xmlfile, oldname, newname);
		//		builder.process(temp);
		println "elapsed: "+(System.currentTimeMillis()-time);
	}
}
