package org.txm.scripts.importer

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.*;
import javax.xml.xpath.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.txm.importer.PersonalNamespaceContext;

class SetNewXmlDocumentRoot {
	public static boolean process(File xmlfile, String xpath) {
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true); // never forget this!
		domFactory.setXIncludeAware(true);
		DocumentBuilder builder = domFactory.newDocumentBuilder();
		
		Document doc = builder.parse(xmlfile);
		Element root = doc.getDocumentElement();
		
		XPathFactory xpathfactory = XPathFactory.newInstance();
		def _xpath = xpathfactory.newXPath();
		_xpath.setNamespaceContext(new PersonalNamespaceContext());
		def expr = _xpath.compile(xpath);
		def nodes = expr.evaluate(doc, XPathConstants.NODESET);

		for(Node node : nodes) {

			Document document = builder.newDocument();
			document.setXmlVersion("1.0");
			document.setXmlStandalone(true);
			Node adopted = document.adoptNode(node)
			document.appendChild(adopted)
			return org.txm.utils.xml.DomUtils.save(document, xmlfile);
		}
		return false;
	}
	
	//		File xmlfile = new File("/home/mdecorde/xml/txmrefman/TEI/refman.xml")
	//		File outfile = new File("/home/mdecorde/xml/txmrefman/TEI/refman-o.xml")
	//		String xpath = "//body"
	
}
