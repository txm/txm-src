// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-12-17 12:11:39 +0100 (jeu. 17 déc. 2015) $
// $LastChangedRevision: 3087 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer;

import java.io.BufferedReader;
import java.util.ArrayList;
import javax.xml.parsers.SAXParserFactory
import org.xml.sax.helpers.DefaultHandler
import org.xml.sax.*
import javax.xml.stream.*;
import java.net.URL;

// TODO: Auto-generated Javadoc
/** Count the focused tag and add id to it you must specify  : which tag are milestones the tag to count. */
class WordCounter extends DefaultHandler {
    String txt ;
	
	/** The focus. */
	String focus;
    
    /** The counter. */
    int counter = 0;
    
    /** The solotags. */
    ArrayList<String> solotags;
    
    /** The writer. */
    XMLStreamWriter writer;
	
	/**
	 * The initializer do the work
	 * The file infile is replaced by the result.
	 *
	 * @param infile the infile
	 * @param focus the focus
	 * @param txt the txt
	 */
    WordCounter(File infile, String focus, String txt)
    {
    	File outfile = new File(infile.getName()+"counter");
		this.focus=focus;
		this.txt = txt;
		
		FileInputStream input = new FileInputStream(infile);
		
        def handler = this;
        def reader = SAXParserFactory.newInstance().newSAXParser();
		
        def output = new FileOutputStream(outfile);
        XMLOutputFactory factory = XMLOutputFactory.newInstance();
		writer = factory.createXMLStreamWriter(output, "UTF-8");
		writer.writeStartDocument("UTF-8", "1.0");
		output.write("\n")
		
		//Reader inputStream = new InputStreamReader(new FileInputStream(infile) , "UTF-8");
		//BufferedReader r= new BufferedReader(inputStream);
		
        reader.parse(input, handler);// process !!
		
        input.close();
        writer.close();
		output.close();
        //r.close();
        if (!(infile.delete() && outfile.renameTo(infile))) println "Warning can't rename file "+outfile+" to "+infile
    }
	
	/**
	 * process start tag.
	 *
	 * @param ns the ns
	 * @param localName the localname of the current tag
	 * @param qName the q name
	 * @param atts the attributes of the current tag
	 */
    void startElement(String ns, String localName, String qName, Attributes atts) {
    	
    	writer.writeStartElement(qName)
    	
    	//writeAttrs
        if(qName.matches(focus))
        {
        	counter++;
        	writer.writeAttribute("n",""+counter)
            writer.writeAttribute("xml:id",focus+txt+"_"+counter)
            for(int i=0; i < atts.getLength();i++)
            	if(atts.getQName(i) != "n" && atts.getQName(i) != "xml:id" && atts.getQName(i) != "id")
                writer.writeAttribute(atts.getQName(i), atts.getValue(i));
        }
        else
        {
        	for(int i=0; i < atts.getLength();i++)
                writer.writeAttribute(atts.getQName(i), atts.getValue(i));
        }
    }
	
	/**
	 * write the CDATA, not finished.
	 *
	 * @param chars all the chars
	 * @param offset where the text begin
	 * @param length the length
	 */
    void characters(char[] chars, int offset, int length)
    {
    	writer.writeCharacters(chars, offset, length)
    }
	
	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
    void endElement(String ns, String localName, String qName)
    {
       writer.writeEndElement();
    }
    
    /**
     * Find text id.
     *
     * @param infile the infile
     * @param focus the focus
     * @return the string
     */
    public static String findTextId(File infile, String focus)
    {
    	def inputData = infile.toURI().toURL().openStream();
		def inputfactory = XMLInputFactory.newInstance();
		XMLStreamReader parser = inputfactory.createXMLStreamReader(inputData);
				
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) 
		{
			switch (event) 
			{
				case XMLStreamConstants.START_ELEMENT:
					if(parser.getLocalName() == focus)
					{
						String sid = ""
						for(int i = 0 ; i < parser.getAttributeCount() ; i++)
							if(parser.getAttributeLocalName(i) == "id")
							{
								sid = parser.getAttributeValue(i); //xxx_000
								sid = sid.substring(focus.length()); // on retire le nom du focus
								sid = sid.split("_")[0];
								break;
							}
						if (parser != null) parser.close();
						if (inputData != null) inputData.close();
						return sid;
					}
			}
		}
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
    }
    
    /**
     * The main method.
     *
     * @param args the arguments
     */
    static void main(String[] args)
    {
    	File infile = new File("~/xml/quote/processme-q.xml")
        new WordCounter(infile, "s","01");
    	new WordCounter(infile, "q","01");
    }
}