package org.txm.scripts.importer

import java.util.HashMap;
import java.util.ArrayList;
import java.io.File;
import javax.xml.stream.*;
import java.net.URL;

class GetAttributeValue {
	public static String process(File xmlFile, String path, String attribute) {
		String value = "N/A";
		String currentpath = "";
		boolean done = false;
		
		def inputData = null;
		def factory = null;
		XMLStreamReader parser = null;
		try {
			URL url = xmlFile.toURI().toURL();
			inputData = url.openStream();
			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);
			for (int event = parser.next(); !done && event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
			{
				switch (event) {
					case XMLStreamConstants.START_ELEMENT:
						currentpath += "/"+parser.getLocalName();// append the current tag to the current path
						if (path == currentpath) {
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
								if (parser.getAttributeName(i) == attribute) {
									value = parser.getAttributeValue(i)
									done = true;
									break;
								}
							}
						}
						
						break;
					case XMLStreamConstants.END_ELEMENT:
						currentpath = currentpath.substring(0,currentpath.length() -1 - parser.getLocalName().length()) // remove tag from the path
						break;
					
				}
			}
		}
		catch(Exception e){println("File "+xmlFile+"\n"+e); 
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
			return "N/A";}
		
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
		return value;
	}
}
