// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.importer

import java.io.File;

// TODO: Auto-generated Javadoc
/**
 * The Class ReunitShortLines.
 */
class ReunitShortLines {
	
	/**
	 * Instantiates a new reunit short lines.
	 *
	 * @param file the file
	 * @param maxsize the maxsize
	 * @param encoding the encoding
	 */
	ReunitShortLines(File file, int maxsize, String encoding)
	{
		File temp = new File( file.getParentFile(), "reu.txt");
		Reader input = new InputStreamReader(new FileInputStream(file) , encoding);
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(temp) , encoding);
		
		String line = input.readLine();
		boolean oklength = true;
		boolean previousok = false;
		while(line != null)
		{
			if(line.length() == 0)
			{
				writer.write("\n");
				oklength = true;
			}
			else if(line.startsWith("**** "))
			{
				writer.write("\n"+line);
				oklength = true;
			}
			else
			{
			previousok = oklength;
			oklength = line.length() > maxsize;
			
			if(previousok && oklength)
				writer.write("\n");
			
			writer.write(line);
			
			if(previousok && ! oklength)
				writer.write("\n");
			
			}
			line = input.readLine();
		}
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		new ReunitShortLines(new File("/home/mdecorde/xml/voeux/Voeux.txt"), 30, "cp1252")
	}
}
