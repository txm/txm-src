package org.txm.scripts.importer

import javax.xml.stream.*;
import java.net.URL;
/**
 * Read the import.xml file
 * @author mdecorde
 */
class ReadImportParameters {
	HashMap<String, ArrayList<String>> links = new HashMap<String, ArrayList<String>>();;
	HashMap<String, String> structs = new HashMap<String, String>();
	HashMap<String, Integer> levels = new HashMap<String, Integer>();
	
	HashMap<String, HashMap<String, String>> metadatas = [:];
	HashMap<String, HashMap<String, String>> pattributes = [:];
	HashMap<String, HashMap<String, String>> sattributes = [:];
	
	HashMap<String, String> biblios = [:];
	
	HashMap<String, String> uis = [:];
	
	HashMap<String, ArrayList<HashMap<String, String>>> editions = [:];
	
	String basename;
	String encoding;
	String language;
	String rootDir;
	String script;
	String xslt;
	String author;
	String description;
	
	boolean debug;
	
	public ReadImportParameters() {
	}
	
	public boolean read(File infile) {
		try {
			def url = infile.toURI().toURL();
			def inputData = url.openStream();
			def factory = XMLInputFactory.newInstance();
			def parser = factory.createXMLStreamReader(inputData);
			boolean ret = read(parser);
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
			return ret;
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		}catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}
	
	public boolean read(def parser) {
		boolean startAlignInfos = false;
		boolean startEditions = false;
		boolean startMetadatas = false;
		boolean startPAttributes = false;
		boolean startSAttributes = false;
		boolean startTokenizer = false;
		
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			switch (event) {
				
				case XMLStreamConstants.END_ELEMENT:
					String localname = parser.getLocalName();
					if(localname == "linkGrp") {
						startAlignInfos = false;
					}
					else if(localname == "metadatas") {
						startMetadatas = false;
					}
					else if(localname == "editions") {
						startEditions = false;
					}
					else if(localname == "pattributes") {
						startPAttributes = false;
					}
					else if(localname == "sattributes") {
						startSAttributes = false;
					}
					else if(localname == "tokenizer") {
						startSAttributes = false;
					}
					break;
				
				case XMLStreamConstants.START_ELEMENT:
					String localname = parser.getLocalName();
					if (localname == "import") {
						basename = parser.getAttributeValue(null, "name");
						encoding = parser.getAttributeValue(null, "encoding");
						language = parser.getAttributeValue(null, "lang");
						rootDir = parser.getAttributeValue(null, "rootDir");
						script = parser.getAttributeValue(null, "script");
						xslt = parser.getAttributeValue(null, "xslt");
						author = parser.getAttributeValue(null, "author")
						description = parser.getAttributeValue(null, "desc");
						
						boolean debug;
					}
					else if(localname == "editions") {
						startEditions = true;
					}
					else if(localname == "edition") {
						String corpus = parser.getAttributeValue(null, "corpus")
						if(!editions.containsKey(corpus))
							editions.put(corpus, []);
						def list = editions.get(corpus);
						def map = [:]
						list.add(map)
						
						map.put("name", parser.getAttributeValue(null, "name"))
						map.put("mode", parser.getAttributeValue(null, "mode"))
						map.put("script", parser.getAttributeValue(null, "script"))
					}
					else if (localname == "metadatas") {
						startMetadatas = true;
					}
					else if (localname == "metadata" && startMetadatas) {
						metadatas.put(parser.getAttributeValue(null, "id"),
								["id":parser.getAttributeValue(null, "id"),
									"shortname":parser.getAttributeValue(null, "shortname"),
									"longname":parser.getAttributeValue(null, "longname"),
									"type":parser.getAttributeValue(null, "type"),
									"selection":parser.getAttributeValue(null, "selection"),
									"partition":parser.getAttributeValue(null, "partition"),
									"display":parser.getAttributeValue(null, "display"),])
					}
					else if (localname == "pattributes") {
						startPAttributes = true;
					} else if(localname == "pattribute" && startPAttributes) {
						pattributes.put(parser.getAttributeValue(null, "id"),
								["id":parser.getAttributeValue(null, "id"),
									"shortname":parser.getAttributeValue(null, "shortname"),
									"longname":parser.getAttributeValue(null, "longname"),
									"type":parser.getAttributeValue(null, "type"),
									"renderer":parser.getAttributeValue(null, "renderer"),
									"tooltip":parser.getAttributeValue(null, "tooltip"),
									"display":parser.getAttributeValue(null, "display"),
									"min":parser.getAttributeValue(null, "min"),
									"max":parser.getAttributeValue(null, "max"),
									"inputFormat":parser.getAttributeValue(null, "inputFormat"),
									"outputFormat":parser.getAttributeValue(null, "outputFormat"),
									"position":parser.getAttributeValue(null, "position"),
									"sortBy":parser.getAttributeValue(null, "sortBy")])
					}
					else if (localname == "sattributes") {
						startSAttributes = true;
					}
					else if (localname == "sattribute" && startSAttributes) {
						sattributes.put(parser.getAttributeValue(null, "id"),
								["id":parser.getAttributeValue(null, "id"),
									"shortname":parser.getAttributeValue(null, "shortname"),
									"longname":parser.getAttributeValue(null, "longname"),
									"type":parser.getAttributeValue(null, "type"),
									"renderer":parser.getAttributeValue(null, "renderer"),
									"tooltip":parser.getAttributeValue(null, "tooltip"),
									"pattern":parser.getAttributeValue(null, "renderer"),
									"import":parser.getAttributeValue(null, "import"),
									"min":parser.getAttributeValue(null, "min"),
									"max":parser.getAttributeValue(null, "max"),
									"mandatory":parser.getAttributeValue(null, "mandatory"),
									"order":parser.getAttributeValue(null, "order"),
									"inputFormat":parser.getAttributeValue(null, "inputFormat"),
									"outputFormat":parser.getAttributeValue(null, "outputFormat"),])
					} else if (localname == "link" && startAlignInfos) {
						String target = parser.getAttributeValue(null, "target");
						String struct = parser.getAttributeValue(null, "alignElement");
						String level = parser.getAttributeValue(null, "alignLevel")
						ArrayList<String> corpora = target.split("#");
						for (int i = 0 ; i < corpora.size() ; i++) {
							corpora[i] = corpora[i].trim()
							if (corpora[i].length() == 0) {
								corpora.remove(i)
								i--
							}
						}
						
						if (level == null)
							level = "1";
						
						if (corpora.size() == 0) {
							println "no corpus aligned: "+parser.getLocation()
							return false;
						}
						if (struct == null) {
							println "a sturcture is missing: "+parser.getLocation()
							return false;
						}
						links.put(target, corpora)
						structs.put(target, struct)
						levels.put(target, level.toInteger())
					} else if(localname == "linkGrp" && parser.getAttributeValue(null, "type") == "align") {
						startAlignInfos = true;
					} else if(localname == "tokenizer") {
						startSAttributes = true;
					}
			}
		}
	}
	
	public Set<String> getAlignements() {
		return links.keySet();
	}
	
	public ArrayList<String> getTargets(String alignement) {
		return links.get(alignement);
	}
	
	public String getStructure(String alignement) {
		return structs.get(alignement);
	}
	
	public int getLevel(String alignement) {
		return levels.get(alignement);
	}
	
	public void dump() {
		println "LINKS"
		for(String link : links.keySet()) {
			println link
			println " corpus: "+links.get(link)
			println " struct: "+structs.get(link)
			println " level: "+levels.get(link)
		}
		
		println "PROPERTIES"
		println "metadata $metadatas"
		println " pattributes $pattributes"
		println " sattributes $sattributes"
		
		println " biblios $biblios"
		
		println "uis $uis"
		
		println " editions $editions"
		
		println "HEADER"
		println " basename $basename"
		println " encoding $encoding"
		println " language $language"
		println " rootDir $rootDir"
		println " script $script"
		println " xslt $xslt"
		println " author $author"
		println " description $description"
		println "DEBUG: $debug"
	}
	
	public static void main(String[] args) {
		String userhome = System.getProperty("user.home");
		File alignfile = new File(userhome, "xml/xmltxmpara/align.xml")
		ReadImportParameters parameters = new ReadImportParameters();
		parameters.read(alignfile);
		parameters.dump();
	}
}

