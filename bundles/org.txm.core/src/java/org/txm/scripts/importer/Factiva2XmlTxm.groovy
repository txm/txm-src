/**
 * Main.
 *
 * @param args the args
 */
// script de recherche de contenus entre balises dans un fichier xml

/**
 *  @author Daniel Marin, Florent Bédécarrats
 *  @author Matthieu Decorde
 *  @copyright 2011 Daniel Marin &  Florent Bédécarrats, for original Perl version
 *  @copyright 2011 ENS de Lyon, for translation in Groovy
 *  This software is licensed under the GNU GPL version 3.0 or later. 
 */
package org.txm.scripts.importer;

import org.txm.importer.ValidateXml

public run(File rootDir, String rep_final ) {

	// A RENSEIGNER #########################################
	
	def fich_xml = rootDir.listFiles().grep(~/.*\.xml/)
	println "$rootDir : $fich_xml"

	def ext = ".xml"
	//def rep_final = rootDir.getAbsolutePath()+"/out" // le nom du dossier dans lequel on va créer les fichiers txm

	List<String> Balises = ["<baseLanguage>","<date>","<sourceName>","<name>","<name>"] // balises XML à détecter
	List<String> FinBalises = ["</baseLanguage>","</date>","</sourceName>","</name>","</name>"]

	List<String> Sections = ["<ppsarticle>","<industry ","<region ","<headline>","<tailParagraphs>"] // Balises indiquant qu'à 'intérieur de la section se trouvent les infos (sur plusieurs lignes contrairement à @Balises)
	List<String> FinSections = ["</ppsarticle>","</industry>","</region>","</headline>","</tailParagraphs>"]

	def Headline = [] // le tableau dans lequel on va stocker le contenu de <headline>
	def TailPara = [] // le tableau dans lequel on va stocker le contenu de <tailParagraphs>

	def NomsMeta = ["language=","date=","source=","subject=","country="] // les noms qui interviennent dans les méta-données du fichier final
	// FIN A RENSEIGNER #####################################

	def TestMeta = [0,0,0,0,0] // Tableau qui indique si les métadonnées sont disponibles dans le fichier
	def MetaData = [] //le tableau des metadonnées
	def metaData = "" // la chaine dans laquelle on va stocker les métadonnées
	String temp = ""
	def fich_out = ">"
	def nom_in = ""

	def new_fich=0 // variable booleenne qui indique si on doit créer un nouveau txm
	def industry=0 // variable booleenne qui indique qu'on est dans la section <industry> pour renseigner "subject"
	def premIndustry=0 // variable booleenne qui indique qu'on a passé le 1er <industry> pour renseigner "subject"
	def region=0 // variable booleenne qui indique qu'on est dans la section <region> pour renseigner "country"
	def premRegion=0 // variable booleenne qui indique qu'on a passé le 1er <region> pour renseigner "country"
	def copie=0 // variable booleenne qui indique si l'on doit copier le contenu de la ligne dans le fichier txm
	def copieSec3=0 // variable qui indique si on est à l'intérieur de la section 3
	def copieSec4=0 // variable qui indique si on est à l'intérieur de la section 4

	def num_fich=0 // le numéro du fichier traité
	def char_num_fich="" // pour numéroter les fichiers fich_001 pour garder l'ordre alphabétique

	def i = 1

	println "Création du dossier dans lequel on va mettre les fichiers finaux txm"
	if (new File(rep_final).exists()) {
		println "Le dossier $rep_final existe"
	}
	else {
		if (!new File(rep_final).mkdir()) {
			println "Echec de la création du dossier: $rep_final"
			return
		}
		if (!new File(rep_final).canWrite()) {
			println "Le dossier '$rep_final' final est read-only"
			return
		}
	}

	FileWriter ECRITURE
	for (File fich_in : fich_xml) {
		if (fich_in.getName() == "import.xml") continue;
		println "Validate $fich_in...";
		if (!ValidateXml.test(fich_in)) {
			println "skip $fich_in"
			continue;
		}
		// open(LECTURE, fich_in) || die("erreur d'ouverture de $fich_in")
		println "on parcourt l'ensemble de $fich_in"
		def reader = new FileReader(fich_in)
		String line = reader.readLine()
		while (line != null)
		{
			// on stocke le contenu de la ligne (et on efface le retour chariot à la fin)
			temp = line
			temp = temp.trim()
			temp = (temp =~ /^\s+/).replaceAll("")
			temp = (temp =~ /^\t/).replaceAll("")
			temp = temp.replaceAll("</paragraph>", "</paragraph><br/>")

			// On indique si on est dans la 1ère section de industry ou region pour prélever <name>
			if ((temp =~ Sections[1]) && (premIndustry==0)){
				industry=1
				premIndustry=1
			}
			else if (temp =~ FinSections[1]){
				industry=0
			}

			if ((temp =~ Sections[2]) && (premRegion==0)){
				region = 1
				premRegion = 1
			}
			else if (temp =~ FinSections[2]){
				region = 0
			}

			// on regarde si la balise de création de nouveau fichier est présente
			if (temp =~ Sections[0]){
				new_fich=1
				num_fich = num_fich+1
				nom_in = fich_in.getName()
				nom_in = (nom_in =~ /$ext/).replaceAll("")
				// Petit test pour numéroter les fichiers en 001, 002 plutôt que 1, 2 (ordre alphabétique)
				if (num_fich < 10){
					char_num_fich = "00"+num_fich
				} else if (num_fich<100) {
					char_num_fich = "0"+num_fich
				} else {
					char_num_fich = num_fich
				}
				fich_out = new File(rep_final, nom_in + "_" + char_num_fich + ext)
				//open(ECRITURE,"> $fich_out") || die("erreur d'ouverture de $fich_out ! $! \n")
				println("new file : "+fich_out)
				if (ECRITURE != null) ECRITURE.close()
				ECRITURE = new FileWriter(fich_out)
			}

			// Remplissage des métadonnées : ####################################################
			// Language
			if (temp =~ Balises[0]){
				temp = (temp =~ Balises[0]).replaceAll("")
				temp = (temp =~ FinBalises[0]).replaceAll("")

				MetaData[0]=temp
				print " language trouve : "+MetaData[0]+" \n"
				TestMeta[0]=1

			}
			// date
			if (temp =~ Balises[1]) {
				temp = (temp =~ Balises[1]).replaceAll("")
				temp = (temp =~ FinBalises[1]).replaceAll("")
				MetaData[1]=temp
				print " date trouvee : "+MetaData[1]+" \n"
				TestMeta[1]=1
			}
			// source
			if (temp =~ Balises[2]){
				temp = (temp =~ Balises[2]).replaceAll("")
				temp = (temp =~ FinBalises[2]).replaceAll("")
				MetaData[2]=temp
				print " source trouvee : "+MetaData[2]+" \n"
				TestMeta[2]=1
			}
			// subject (on doit être dans la 1ère section <industry>)
			if ((temp =~ Balises[3]) && (industry==1)){
				temp = (temp =~ Balises[3]).replaceAll("")
				temp = (temp =~ FinBalises[3]).replaceAll("")
				MetaData[3]=temp
				industry=0
				print " Sujet trouve : "+MetaData[3]+" \n"
				TestMeta[3]=1
			}
			// country
			if ((temp =~ Balises[4]) && (region==1)){
				temp = (temp =~ Balises[4]).replaceAll("")
				temp = (temp =~ FinBalises[4]).replaceAll("")
				MetaData[4]=temp
				region=0
				print " Pays trouve : "+MetaData[4]+" \n"
				TestMeta[4]=1

				// Toutes les méta-données ont été trouvées, on assemble la chaîne
				metaData = "<article "
				for (i=0;i<(NomsMeta.size()+1);i++){
					if (TestMeta[i]==1){
						metaData = metaData+NomsMeta[i]+"\""+MetaData[i]+"\" "
					}
					else if (TestMeta[i]==0){
						metaData = metaData +NomsMeta[i]+"\"\" "
					}
				}
				metaData = "$metaData>\n"
				print " metaData : $metaData \n"
			}
			// FIN Remplissage des métadonnées : #################################################

			// On copie ou pas si on est ou pas dans <headline> ou <tailParagraphs>
			if (temp =~ Sections[3]){
				copieSec3=1
			}
			if (temp =~ FinSections[3]){
				Headline.add(temp)
				copieSec3=0
			}
			if (temp =~ Sections[4]){
				copieSec4=1
			}
			if (temp =~ FinSections[4]){
				TailPara.add(temp)
				copieSec4=0
			}

			if (copieSec3 == 1){
				Headline.add(temp)
			}
			if (copieSec4 == 1){
				TailPara.add(temp)
			}

			if (temp =~ FinSections[0]){
				ECRITURE.write("$metaData\n")
				for (i=0;i<(Headline.size());i++){
					if (i==0 || i==Headline){
						ECRITURE.write(Headline[i].toString()+"\n")
					}
					else{
						ECRITURE.write("\t"+Headline[i]+"\n")
					}
				}
				for (i=0;i<(TailPara.size());i++){
					if (i==0 || i==TailPara){
						ECRITURE.write(TailPara[i].toString()+"\n")
					}
					else{
						ECRITURE.write("\t"+TailPara[i]+"\n")
					}
				}
				ECRITURE.write("</article>")

				new_fich=0
				industry=0
				region=0
				premIndustry=0
				premRegion=0
				MetaData=[]
				TestMeta=[0,0,0,0,0]
				Headline=[]
				TailPara=[]

				ECRITURE.close()
				print "fichier $char_num_fich traite.\n"
			}
			line = reader.readLine()
		}
		ECRITURE.close()
		reader.close()
		num_fich = 0
	}
}

String userDir = System.getProperty("user.home");
File rootDir = new File(userDir, "xml/factiva")
File outdir = new File(rootDir, "out2")
run(rootDir, outdir.getAbsolutePath());