package org.txm.scripts.importer

import org.w3c.tidy.*

def workDir = new File("/home/mdecorde/TEMP")
def outdir = new File("/home/mdecorde/TEMP/out")

def src = "http://textometrie.ens-lyon.fr/html/doc/manual/manual1.xhtml";
def base = src.substring(0, src.lastIndexOf("/"));

def url_xpath = """//*/a[@href]/@href""";
def url_regexp = /.+/;
def duplicates = false; // 'true' not implemented
def filename_regexp = /zer/;
def content_xpath = "//*/div[@id=\"content\"]";

def xhtmlFile = new File(workDir, "src.xhtml")
URL2XHTMLFILE(src, xhtmlFile, workDir);

println "finding urls of $xhtmlFile"
XPathResult xpathResult = new XPathResult(xhtmlFile);
def urls = new LinkedHashSet<String>();

for (String url : xpathResult.getXpathResponses(url_xpath))
	if (urls ==~ url_regexp ) urls << url
println urls

int i = 1;
for (String url : urls) {
	//url = url.replaceAll()
	def subxHtmlFile = new File(workDir, url)
	URL2XHTMLFILE(base+"/"+url, subxHtmlFile, workDir);
	
	def txtFile = new File(outdir, url+".xhtml")
	def writer = txtFile.newWriter("UTF-8");
	for (def node : xpathResult.getNodes(content_xpath)) {
		writer.println(node)
	}
	writer.close()
	i++
}

def URL2XHTMLFILE(String url, File outfile, File workDir) {
	println "getting src: $url"
	def htmlFile = new File(workDir, "base.html")
	def filestream = new FileOutputStream(htmlFile)
	def out = new BufferedOutputStream(filestream)
	out << new URL(url).openStream()
	out.close()
	
	println "xHTMLize HTML $htmlFile"
	//TODO replace JTidy with Jsoup
	//def tidy = new Tidy(); // obtain a new Tidy instance
	tidy.setXHTML(true); // set desired config options using tidy setters
	tidy.setInputEncoding("UTF-8")
	tidy.setOutputEncoding("UTF-8")
	tidy.setShowErrors(0)
	tidy.setTabsize(10)
	tidy.setShowWarnings(false)
	File xhtmlFile = new File(workDir, "base.xhtml")
	xhtmlFile.withWriter("UTF-8") { out2 ->
		tidy.parse(htmlFile.toURI().toURL().newInputStream([readTimeout: 10000]), out2); // run tidy, providing an input and output stream
	}
	
	println "remove entities and doctype"
	String text = xhtmlFile.getText("UTF-8")
	text = text.replaceAll("&nbsp;", " ")
	text = text.replaceAll("xmlns=\"http://www.w3.org/1999/xhtml\"", " ")
	text = text.replaceAll("""<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">""", "")
	outfile.withWriter("UTF-8") { writer ->
		writer.write(text);
	}
}
println "done"