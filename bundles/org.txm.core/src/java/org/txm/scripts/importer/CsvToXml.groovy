// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2015-12-17 12:11:39 +0100 (jeu. 17 déc. 2015) $
// $LastChangedRevision: 3087 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer

import java.io.FileInputStream
import java.io.InputStreamReader
import java.nio.charset.Charset
import org.txm.utils.*

import javax.xml.stream.*
import java.net.URL

/**
 * The Class CsvToXml: allow to convert CSV files to XML files
 *
 * @author mdecorde
 */
class CsvToXml {

	/** The titles. */
	File xmlfile
	List<String> titles = []

	private CsvReader getCSVReader(File csvfile, String fieldSeparator, String encoding) {
		CsvReader csvreader = new CsvReader(csvfile.getAbsolutePath(), fieldSeparator.charAt(0), Charset.forName(encoding))

		//get titles
		csvreader.readHeaders()
		titles = []
		for (String title : csvreader.getHeaders()) {
			titles << title
		}

		if (titles.size() == 0) {
			println "the CSV file as no column"
			return null
		}

		if (titles.contains("text") == 0) {
			println "the CSV file as no 'text' column"
			return null
		}

		if (titles.contains("id") == 0) {
			println "the CSV file as no 'id' column"
			return null
		}
		
		return csvreader
	}
	
	/**
	 * Convert a CSV file to ONE XML file using the first line to declare metadata
	 *
	 * @param csvfile the csvfile
	 * @param outfile the outfile
	 * @param fieldSeparator the field separator
	 * @param textSeparator the text separator
	 * @param encoding the encoding
	 * @return true, if successful
	 */
	public boolean toOneXMLFile(File csvfile, File xmlfile, String fieldSeparator, String textSeparator, String encoding) {
		CsvReader csvreader = getCSVReader(csvfile, fieldSeparator, encoding)
		if (csvreader == null) return false
		
		println "Metadata properties declared: "+titles

		XMLOutputFactory factory = XMLOutputFactory.newInstance()
		FileOutputStream output = new FileOutputStream(xmlfile)
		XMLStreamWriter writer = factory.createXMLStreamWriter(output, "UTF-8")

		writer.writeStartDocument("UTF-8","1.0")
		writer.writeStartElement("text")
		while (csvreader.readRecord()) {
			
			writer.writeStartElement("div")

			for (int i = 0 ; i < titles.size() ; i++) {
				if (titles[i] == "text") continue
				writer.writeAttribute(AsciiUtils.buildId(titles.get(i)), csvreader.get(i))
			}

			writer.writeCharacters(csvreader.get("text")) // get text content
			writer.writeEndElement() // div
		}

		writer.writeEndElement() // text
		writer.close()
		output.close()
		csvreader.close()

		return true;
	}
	
	/**
	* Convert a CSV file to SEVERAL XML file using the first line to declare metadata
	*
	* @param csvfile the csvfile
	* @param outfile the outfile
	* @param fieldSeparator the field separator
	* @param textSeparator the text separator
	* @param encoding the encoding
	* @return true, if successful
	*/
	public boolean toMultipleXMLFiles(File csvfile, File outDir, String fieldSeparator, String textSeparator, String encoding) {
		CsvReader csvreader = getCSVReader(csvfile, fieldSeparator, encoding)
		if (csvreader == null) return false
		
		if (!outDir.exists()) outDir.mkdirs()
		if (!outDir.exists()) {
			println "Out directory does not exist and could not create it"
			return false;
		}
		
		while (csvreader.readRecord()) {
			
			File xmlfile = new File(outDir, csvreader.get("id")+".xml")
			
			XMLOutputFactory factory = XMLOutputFactory.newInstance()
			FileOutputStream output = new FileOutputStream(xmlfile)
			XMLStreamWriter writer = factory.createXMLStreamWriter(output, "UTF-8")
	
			writer.writeStartDocument("UTF-8","1.0")
			writer.writeStartElement("text")
			
			for (int i = 0 ; i < titles.size() ; i++) {
				if (titles[i] == "text") continue
				writer.writeAttribute(AsciiUtils.buildId(titles.get(i)), csvreader.get(i))
			}

			writer.writeCharacters(csvreader.get("text")) // get text content
			
			writer.writeEndElement() //text
			writer.close()
			output.close()
		}
		return true
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	static public void main(String[] args)
	{
		String home = System.getProperty("user.home")
		File csvfile = new File(home, "xml/csv/test.csv")
		File outfile = new File(home, "xml/csv/test.xml")
		File outdir = new File(home, "xml/csv/out")

		String fieldSeparator = "\t"
		String textSeparator = "" //or "'"
		CsvToXml builder = new CsvToXml()
		
		builder.toMultipleXMLFiles(csvfile, outdir, fieldSeparator, textSeparator, "UTF-8")
		
		builder.toOneXMLFile(csvfile, outfile, fieldSeparator, textSeparator, "UTF-8")
	}
}
