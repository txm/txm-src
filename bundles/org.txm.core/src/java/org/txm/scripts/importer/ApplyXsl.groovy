// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2016-03-01 10:10:58 +0100 (mar. 01 mars 2016) $
// $LastChangedRevision: 3133 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer;

/**
 * @author mdecorde
 */

import java.io.File;
import javax.xml.transform.Transformer
import javax.xml.transform.TransformerFactory
import javax.xml.transform.stream.StreamResult
import javax.xml.transform.stream.StreamSource;

import org.txm.utils.io.IOUtils

import net.sf.saxon.*;
import net.sf.saxon.s9api.*;

// TODO: Auto-generated Javadoc
/**
 * apply a xsl file to a file : use process(File infile, File outfile, String[]
 * args) to apply the xslt file loaded by the constructor args =
 * ["name1","value1","name2",value2, ...]
 * 
 * @author mdecorde
 */

public class ApplyXsl {
	
//	/** The proc. */
//	private Processor proc;
//	
//	/** The comp. */
//	private XsltCompiler comp;
//	
//	/** The exp. */
//	private XsltExecutable exp;
//
//	/** The source. */
//	private XdmNode source;
//	
//	/** The out. */
//	private Serializer out;
//	
//	/** The trans. */
//	private XsltTransformer trans;

	TransformerFactory tFactory;
	Transformer transformer;
	
	/**
	 * initialize with the xslt file.
	 *
	 * @param xsltfile the xsltfile
	 */
	public ApplyXsl(String xsltfile) {
//		proc = new Processor(false);
//		comp = proc.newXsltCompiler();
//		exp = comp.compile(new StreamSource(new File(xsltfile)));
		tFactory = new net.sf.saxon.TransformerFactoryImpl();
		transformer = tFactory.newTransformer(new StreamSource(xsltfile));

	}
	
	/**
	 * initialize with the xslt file.
	 *
	 * @param xsltfile the xsltfile
	 */
	public ApplyXsl(File xsltfile) {
//		proc = new Processor(false);
//		comp = proc.newXsltCompiler();
//		exp = comp.compile(new StreamSource(xsltfile));
		tFactory = new net.sf.saxon.TransformerFactoryImpl();
		transformer = tFactory.newTransformer(new StreamSource(xsltfile));
	}

	/**
	 * Set in and out file.
	 *
	 * @param xmlinfile the xmlinfile
	 * @param xmloutfile the xmloutfile
	 * @return true, if successful
	 */
	private boolean SetInOutSource(String xmlinfile, String xmloutfile) {
//		if (proc == null || exp == null || comp == null)
//			return false;
//
//		source = proc.newDocumentBuilder().build(
//				new StreamSource(new File(xmlinfile)));
//		out = new Serializer();
//		out.setOutputFile(new File(xmloutfile));
//
//		trans = exp.load();
//		transformer.set.setInitialContextNode(source);
//		transformer.setDestination(out);
		return true;
	}

	/**
	 * Set a xslt param.
	 *
	 * @param name the name
	 * @param value the value
	 * @return true, if successful
	 */
	public boolean SetParam(String name, Object value) {
//		if (trans != null)
//			trans.setParameter(new QName(name), new XdmAtomicValue("" + value));
//		else
//			return false;
		return true;
	}

	/**
	 * Force java to clean memory after processing a xslt. If not we might catch
	 * a JavaHeapspace Exception
	 * 
	 */
	private void cleanMemory() {
//		source = null;
//		trans = null;
//		out = null;
	}

	/**
	 * Process files with xslt args.
	 *
	 * @param xmlinfile file to be processed
	 * @param xmloutfile output file
	 * @param args xslt args ["arg1","arg1value","arg2","arg2value"...]
	 * @return true, if successful
	 */

	public boolean process(String xmlinfile, String xmloutfile, String[] args) {
		if (!this.SetInOutSource(xmlinfile, xmloutfile))
			return false;
		for (int i = 0; i < args.length; i = i + 2)// set params
		{
			if (!this.SetParam(args[i], args[i + 1]))
				return false;
		}

		transformer.transform(new StreamSource(xmlinfile), new StreamResult(new BufferedOutputStream(new FileOutputStream(xmloutfile))));
		cleanMemory(); // save memory
		// System.out.println("Done");
		return true;
	}

	/**
	 * Process files without xslt args.
	 *
	 * @param xmlinfile the xmlinfile
	 * @param xmloutfile the xmloutfile
	 * @return true, if successful
	 */
	public boolean process(String xmlinfile, String xmloutfile) {
		if (!this.SetInOutSource(xmlinfile, xmloutfile))
			return false;

		transformer.transform(new StreamSource(xmlinfile), new StreamResult(new BufferedOutputStream(new FileOutputStream(xmloutfile))));
		cleanMemory();
		// System.out.println("Done");
		return true;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File xslfile = new File(System.getProperty("user.home"), "xml/xsltest/test.xsl")
		assert(xslfile.exists())
		ApplyXsl a = new ApplyXsl(xslfile);
		
		File srcdirectory = new File(System.getProperty("user.home"), "xml/xsltest")
		assert(srcdirectory.exists())
		assert(srcdirectory.isDirectory())
		assert(srcdirectory.canRead())
		assert(srcdirectory.canExecute())
		
		List<File> files = srcdirectory.listFiles(IOUtils.HIDDENFILE_FILTER);
		
		files = files.findAll{item-> item.getName().endsWith(".xml")}
		
		long bigstart = System.currentTimeMillis()
		for (File infile : files) {
			String outfile = infile.getAbsolutePath()+".out"
			if (infile.canRead()) {
				long start = System.currentTimeMillis()
				println "Process : "+infile.getName()
				// String[] params = ["pbval1",1,"pbval2",2];
				// a.process(infile.getAbsolutePath(),outfile); // no parameters
				// a.process( infile.getAbsolutePath(),outfile,params);
				println "Done : "+(System.currentTimeMillis()-start)+"ms"
			}
		}
		println "Total time : "+(System.currentTimeMillis()-bigstart)+"ms"
	}
}
