package org.txm.scripts.importer;
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//

import groovy.xml.XmlNodePrinter
import groovy.xml.XmlParser
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

// TODO: Auto-generated Javadoc
/**
 * The Class EncodeTEIQuotes.
 */
class EncodeTEIQuotes
{
	
	/** The name. */
	def name;
	
	/** The initsize. */
	def initsize;
	
	/** The quote. */
	String quote ="\"";
	
	/** The quote_type. */
	String quote_type= "pon"
	
		/**
		 * Gets the name.
		 *
		 * @param node the xml node
		 * @return the name of the node
		 */
	String getName(def node)
	{
		def name = node.name();
		try{name = name.getLocalPart()}catch(Exception e){return name;} // might throw an exception depending on the node class
		return name;
	}
	
	/**
	 * method to know if the Node is an element.
	 *
	 * @param n the n
	 * @return true, if is elem
	 */
	boolean isElem(def n)
	{
		try{ n.name(); return true; }
		catch(MissingMethodException e){ return false;}
	}
	
	/**
	 * Contains open quote.
	 *
	 * @param selem the node to process
	 * @return true if the node contains a tag <q> at the beginning
	 */
	boolean containsOpenQuote(Node selem)
	{
		def children = selem.children()
		//println "OTS: "+selem.attributes()
		for(int i = 0 ; i < children.size()-1 ; i++)//on part de la fin jusqu'au 2e w
		{
			def child = children.get(i)
			if(isElem(child))
			{
				name = getName(child)
				if(name == "w" && child.@type != quote_type)
				{
					//println "not opening Q"
					return false;
				}
				else if(name == "w" && child.@type == quote_type && child.text().equals(quote))
				{
					//println "opening Q"
					return true;
				}
			}
		}
		//println "not opening Q"
		return false;
	}
	
	/**
	 * Contains close quote.
	 *
	 * @param selem the node to process
	 * @return true if the node contains a tag <q> at the end
	 */
	boolean containsCloseQuote(Node selem)
	{
		if(selem.w.size() == 0)
			return false
		//println "CTS: "+selem.attributes()
		def children = selem.children()
		for(int i = children.size()-1 ; i > 0 ; i--)//on part de la fin jusqu'au 2e w
		{
			def child = children.get(i)
			if(isElem(child))
			{
				name = getName(child)
				if(name == "w" && child.@type != quote_type)
				{
					//println "not closing Q"
					return false;
				}
				else if(name == "w" && child.@type == quote_type && child.text().equals(quote))
				{
					//println "closing Q"
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Next children are words.
	 *
	 * @param children the children
	 * @param index the index
	 * @return true, if successful
	 */
	def boolean nextChildrenAreWords(def children, def index)
	{
		//println "at "+children.get(index).attributes()+" test("+children.size()+") "+children
		for(int i = index ; i < children.size() ; i++)
		{
			def nextC = children.get(i)
			if(isElem(nextC) && getName(nextC) == "w" && nextC.@type != quote_type)
				return true;
		}
		//println "no more words"
		return false;
	}
	
	/**
	 * Process containers.
	 *
	 * @param root the root
	 * @param containers the containers
	 * @return the java.lang. object
	 */
	def processContainers(Node root, containers)
	{
		int countcontainer = 0;
		int countinits = 0;
		int countnews = 0;
		int countinitquote = 0
		int quotecount = 0;
		boolean openq = false;
		// cut sentences whit a wpon not first nor last
		int initsizecontainer = root.children().size()
		//println "process "+initsizecontainer+" elements"
		for(int k = 0 ; k < initsizecontainer ; k++)
		{
			def node = root.children().get(0);
			root.children().remove(0);
			//println getName(node)
			if(isElem(node) && containers.contains(getName(node)))
			{
				countcontainer++;
				Node pelem = node;
				openq = false;
				//println "PAB================="
				int initsize = pelem.children().size()
				for(int j = 0 ; j < initsize ; j++) // pour chq fils de p
				{
					def subnode = pelem.children().get(0);
					pelem.children().remove(0)
					//println getName(subnode)
					if(isElem(subnode) && getName(subnode) == "s") // si c un S
					{
						//println "\nS: "+subnode.attributes()
						def selem = subnode;
						def newS = pelem.appendNode(selem.name(), selem.attributes())
						boolean isOpenningQuote = false;
						if(containsOpenQuote(selem))
						{
							isOpenningQuote = true
							if(openq)
							{
								//println "ERROR: unclosed quote before "+selem.@id
							}
							openq = false;
							//println "force openq false"
						}
						int tempwponcount = 0
						for(int i = 0 ; i < selem.children().size() ; i++) // append chacun de ses fils au S courant
						{
							def child = selem.children().get(i) // test si == wpon"
							if(isElem(child) && getName(child) == "w" && child.@type == quote_type && child.text().equals(quote))
							{
								countinitquote++
								//println "Q: "+child.attributes()
								//println "openq "+openq
								if(openq)
								{
									newS.children().add(child)
									if(nextChildrenAreWords(selem.children(), i))
									{                                                                             
										//println "NEW S"
										newS = pelem.appendNode(selem.name(), selem.attributes())
									}
									//println "set openq false"
									openq = false;
								}
								else
								{
									if(tempwponcount != 0)// premier wpon"
									{
										//println "pas premier wpon''"
										if(nextChildrenAreWords(selem.children(), i))// si y'a aut' chose deriere, new S
										{
											newS = pelem.appendNode(selem.name(), selem.attributes())
											//println "NEW S"
											//println "set openq true"
											openq = true
										}
									}
									else // tempwponcount == 0
									{
										if(!isOpenningQuote)
										{
											if(nextChildrenAreWords(selem.children(), i))// si y'a aut' chose deriere, new S
											{
												newS = pelem.appendNode(selem.name(), selem.attributes())
											}
										}
										//println "set openq true"
										openq = true
									}
									newS.children().add(child)
								}
								tempwponcount++;
							}
							else
							{
								newS.children().add(child)
							}
						}
					}
					else
						pelem.children().add(subnode)
				}
			}
			root.children().add(node)
		}
		// A partir d'ici on a que des phrases de la forme :
		// s q wwww s
		// s q wwww q s
		// s wwww q s
		//println "****"
		//wrap sentences in q tags
		initsize = root.children().size()
		for(int j = 0 ; j < initsize ; j++)
		{
			def node = root.children().get(0);
			root.children().remove(0)
			if(isElem(node) && containers.contains(getName(node)))
			{
				Node pelem = node
				//println "PPPPPPPP"
				def newp = root.appendNode(getName(pelem), pelem.attributes());
				//newp.attribute("test")
				openq = false;
				def children = pelem.children()
				//println children
				Node qelem;
				for(Node subnode : children)
				{
					if(isElem(subnode) && (getName(subnode) == "s"))
					{
						countnews++;
						//println "ELEM s"
						Node selem = subnode;
						if(containsOpenQuote(selem))
						{
							// println "openning wpon"
							if(openq)
							{
								// println "ERROR: unclosed quote (found swpon)"
								//on ferme, et ouvre un new
								qelem.appendNode("note",[type:"auto"]).setValue("unclosed quote")
								Map attrs = [:];
								attrs.put("xml:id",""+(quotecount+1))
								qelem = newp.appendNode("q", attrs)
								quotecount++;
								qelem.appendNode(getName(pelem), selem.attributes(), selem.value());
								if(containsCloseQuote(selem)) // la phrase étaient bien balisée
									openq = false;
							}
							else
							{
								// println "open q + append current s"
								openq = true;
								Map attrs = [:];
								attrs.put("xml:id",""+(quotecount+1))
								qelem = newp.appendNode("q", attrs)
								quotecount++;
								qelem.appendNode(getName(selem), selem.attributes(), selem.value());
								if(containsCloseQuote(selem)) // la phrase étaient bien balisée
									openq = false;
							}
						}
						else if(containsCloseQuote(selem))
						{
							//println "closing wpon"
							if(openq)
							{
								// println "close q + add dernier s du quote"
								qelem.appendNode(getName(selem), selem.attributes(), selem.value());
								openq = false;
							}
							else
							{
								//println "ERROR: unopened quote"
								Map attrs = [:];
								attrs.put("xml:id",""+(quotecount+1))
								qelem = newp.appendNode("q", attrs)
								qelem.appendNode(getName(selem), selem.attributes(), selem.value());
								quotecount++;
								qelem.appendNode("note",[type:"auto"]).setValue("unopened quote")
							}
						}
						else // no wpon
						{
							// println "normal s"
							if(openq)
							{
								// println "append to q"
								qelem.appendNode(getName(selem), selem.attributes(), selem.value());
							}
							else
							{
								// println "append to p"
								newp.appendNode(getName(selem), selem.attributes(), selem.value());
							}
						}
					}
					else
					{
						if(openq)
						{
							qelem.children().add(subnode)
						}
						else
						{
							newp.children().add(subnode)
						}
					}
				}
				if(openq)// un quote fermé a la fin du P !!
				{
					// println "ERROR: unmatched quote"
					qelem.appendNode("note",[type:"auto"]).setValue("unmatched quote")
				}
				//root.children().add(newp)
			}
			else
				root.children().add(node)
		}
		//count sentences
		println "processed "+countcontainer+" "+containers
		println "initial number of &quot; "+countinitquote;
		println "created "+(countnews )+" init "+ countinits+" s"
		println "create "+(quotecount)+" quotes elements"
	}
	
	/**
	 * Instantiates a new encode tei quotes.
	 *
	 * @param nodesToInspect the nodes to inspect
	 * @param containers the containers
	 * @param quote_value the quote_value
	 * @param quote_type the quote_type
	 */
	public EncodeTEIQuotes(List<Node> nodesToInspect, containers, String quote_value, String quote_type)
	{
		this.quote= quote_value;
		this.quote_type= quote_type;
		println "process "+nodesToInspect.size()+" elements with containers "+containers
		for(Node root : nodesToInspect)//.body)
		{
			println(" root : "+root.children().size())
			processContainers(root, containers)
		}
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		List<String> containers = ["p","ab"]; // elements qui contiennent des <s>
		
		File infile = new File(System.getProperty("user.home"), "xml/quote/gormont.xml")
		File outfile = new File(System.getProperty("user.home"), "xml/quote/gormont-q.xml")
		
		def doc = new XmlParser().parse (infile)
		List<Node> nodesToInspect = doc.text.body
		// nodesToInspect << doc.text.body.div // on en rajoute
		
		/*************************/
		new org.txm.scripts.importer.EncodeTEIQuotes(nodesToInspect, containers, "\"", "pon");
		
		//copy the doc in "outfile" File  
		String encoding = "UTF-8"
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(outfile) , encoding);
		writer.write("<?xml version=\"1.0\" encoding=\""+encoding+"\"?>\n")
		def pwriter = new PrintWriter(writer, true)
		XmlNodePrinter xmlwriter = new XmlNodePrinter(pwriter)
		xmlwriter.setPreserveWhitespace(false)
		xmlwriter.print(doc)
		pwriter.close()
		writer.close()
		xmlwriter = null
		//println "write output file "+outfile
		
		//update counts
		if(outfile.exists())
		{
			String txtid = org.txm.scripts.importer.WordCounter.findTextId(infile, "s"); // retrouve l'id du text qui a été concaténé aux id des S originels (ex : s19_12 >> 19)
			new org.txm.scripts.importer.WordCounter(outfile, "s", txtid);
			new org.txm.scripts.importer.WordCounter(outfile, "q", txtid);
		}
	}
}