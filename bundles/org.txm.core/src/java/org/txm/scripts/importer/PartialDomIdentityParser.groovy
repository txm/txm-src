package org.txm.scripts.importer

import java.util.ArrayList;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

import org.txm.importer.StaxIdentityParser

import java.io.File;
import java.io.Writer;
import java.net.URL;

import javax.xml.stream.*;

class PartialDomIdentityParser extends StaxIdentityParser {
	
	boolean domMode = false;
	def partialDom = [];
	def currentNode;
	
	public PartialDomIdentityParser(File infile) {
		super(infile)
	}
	
	protected void processNamespace() {
		super.processNamespace()
	}
	
	protected void processStartElement()
	{
		if (parser.getLocalName() == "test") {
			println "Build dom..."
			Tag testTag = new Tag(parser);
			println testTag.toString()
			testTag.writeDom(writer)
		} else {
			super.processStartElement()
		}
	}
	
	protected void writeAttributes() {
		super.writeAttributes();
	}
	
	protected void processCharacters()
	{
		super.processCharacters()
	}
	
	protected void processProcessingInstruction()
	{
		super.processProcessingInstruction()
	}
	
	protected void processDTD()
	{
		super.processDTD()
	}
	
	protected void processCDATA()
	{
		super.processCDATA();
	}
	
	protected void processComment()
	{
		super.processComment()
	}
	
	protected void processEndElement()
	{
		super.processEndElement();
	}
	
	protected void processEndDocument() {
		super.processEndDocument()
	}
	
	protected void processEntityReference() {
		super.processEntityReference()
	}
	
	public class Tag {
		public String[] attnames, attvalues, attprefix;
		public String localname, prefix
		int count;
		public def children = [];

		public Tag(def parser) {
			prefix = parser.getPrefix()
			localname = parser.getLocalName();
			count = parser.getAttributeCount()
			attnames =  new String[count]
			attvalues = new String[count]
			attprefix = new String[count]
			for (int i = 0 ; i < count ; i++) {
				attnames[i] = parser.getAttributeLocalName(i)
				attprefix[i] = parser.getAttributePrefix(i).toString()
				attvalues[i] = parser.getAttributeValue(i).toString()
			}
			buildNodeContent(parser)
		}
		
		private void buildNodeContent(def parser) {
			try {
				for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
					//println "event "+event
					switch (event) {
						case XMLStreamConstants.START_ELEMENT:
							children << new Tag(parser);
							break;
						case XMLStreamConstants.CHARACTERS:
							children << parser.getText()
							break;
						case XMLStreamConstants.END_ELEMENT:
							if (localname == parser.getLocalName()) {
								return; // end of stax parsing, the Tag is built
							}
							break;
						case XMLStreamConstants.END_DOCUMENT:
							println "ERROR: END OF DOCUMENT REACHED"
							break;
					}
				}
			} catch(Exception e) {
				println("Error while parsing partial content of file "+inputurl);
				println("Location "+parser.getLocation());
				org.txm.utils.logger.Log.printStackTrace(e);
				return;
			}
		}

		public String toString() {
			def out = new StringWriter()
			XMLStreamWriter strwriter = outfactory.createXMLStreamWriter(out);//create a new file
			println ""+(strwriter != null)
			writeDom(strwriter);
			return out.toString()
		}
		
		public writeDom(def writer) {
			// Open the Tag
			if (prefix != null && prefix.length() > 0)
				writer.writeStartElement(prefix+":"+localname)
			else
				writer.writeStartElement(localname)

			// write Attributes
			for (int i = 0 ; i < count ; i++) {
				if (attprefix[i] != null && attprefix[i].length() > 0) {
					writer.writeAttribute(attprefix[i]+":"+attnames[i], attvalues[i])
				} else {
					writer.writeAttribute(attnames[i], attvalues[i])
				}
			}
			
			// write children recursivelly
			for (def child : children) {
				if (child instanceof String) {
					writer.writeCharacters(child)
				} else if (child instanceof Tag) {
					child.writeDom(writer)
				} else {
					println "Error: can't write $child"
				}
			}
			
			// close the Tag
			writer.writeEndElement() 
		}
	}
	
	public static void main(String[] args) {
		File infile = new File("/home/mdecorde/xml/partialdom/test.xml")
		File outfile = new File("/home/mdecorde/xml/partialdom/test-o.xml")
		PartialDomIdentityParser p = new PartialDomIdentityParser(infile);
		println p.process(outfile)
	}
}
