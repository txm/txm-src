package org.txm.scripts.importer

import org.txm.importer.StaxIdentityParser

class WriteIdAndNAttributes extends StaxIdentityParser {

	String textname

	int nMileStone = 1, nPb = 1, nCb = 1, nLb = 1, nW = 1, nSeg = 1, nRejet = 1;
	String previousMileStone, previousPb, previousCb, previousW;


	String PB = "pb", CB = "cb", LB = "lb", ID = "id", TYPE = "type",
	N = "n", CORRESP = "corresp", FACS="facs", W="w", PC="pc", SEG="seg",
	UNIT="unit", XML="xml", WP="wp", SURFACE="surface", POINT = ".", REJET ="rejet";

	public WriteIdAndNAttributes(File xmlFile, String textname) {
		super(xmlFile);

		this.textname = textname
	}

	protected void writeAttributes() {
		// do nothing
	}

	protected void processStartElement() {
		String id = null
		String n = null
		String type = null
		String corresp = null
		String facs = null

		for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
			if (parser.getAttributeLocalName(i) == ID) {
				id = parser.getAttributeValue(i)
			} else if (parser.getAttributeLocalName(i) == N) {
				n = parser.getAttributeValue(i)
			} else if (parser.getAttributeLocalName(i) == TYPE) {
				type = parser.getAttributeValue(i)
			} else if (parser.getAttributeLocalName(i) == CORRESP) {
				corresp = parser.getAttributeValue(i)
			} else if (parser.getAttributeLocalName(i) == FACS) {
				facs = parser.getAttributeValue(i)
			}
		}

		super.processStartElement(); // attributes are not written because super.writeAttributes() is not called

		if (localname == "milestone" && parser.getAttributeValue(null, UNIT) == SURFACE) {
			if (n == null) {
				n = nMileStone
			} else {
				int tmp = Integer.parseInt(n)
				if (nMileStone < tmp) {
					nMileStone = tmp
					n = tmp
				} else {
					n = nMileStone;
				}
			}

			if (id == null) { // don't rewrite "id"
				if (facs == null) {
					println "Error: found milestone@type=\"surface\" with no @facs at "+parser.getLocation()
					throw new Exception("no facs attribute")
				}
				if (facs.lastIndexOf(POINT) > 0) facs = facs.substring(0, facs.lastIndexOf(POINT))
				previousMileStone = "surf_$textname"+"_"+facs
				id = previousMileStone
			} else {
				previousMileStone = id
			}

			nMileStone++
		} else if (localname == PB) {
			if (n == null) {
				n = nPb
			} else {
				try {
					int tmp = Integer.parseInt(n)
					if (nPb < tmp) {
						nPb = tmp
					} else {
						n = nPb;
					}
				} catch(Exception e) { }
			}

			if (id == null) { // don't rewrite "id"
				id = "page_${textname}_"+nPb
				previousPb = "${textname}_"+nPb
			} else {
				previousPb = id
			}

			nRejet = nLb = nCb = 1
			nPb++
		} else if (localname == CB) {
			if (n == null) {
				n = nCb
			} else {
				try {
					int tmp = Integer.parseInt(n)
					if (nCb < tmp) {
						nCb = tmp
					} else {
						n = nCb;
					}
				} catch(Exception e) { }
			}

			if (id == null) { // don't rewrite "id"
				previousCb = "${previousPb}_"+nCb
				id = "col_${previousPb}_"+nCb
			} else {
				previousCb = id
			}

			nLb = nRejet = 1
			nCb++
		} else if (localname == LB) {
			if (n == null) {
				if (REJET == type) {
					println "Warning: no 'n' attribute provided for 'rejet' line break at "+parser.getLocation()
				}
				
				n = nLb
			} else {
				if (REJET != type) {
					try { // if n is numeric update nLgRelative
						int tmp = Integer.parseInt(n)
						nLb = tmp;
					} catch(Exception e) {
						// n is not numeric use it
					}
				}
			}

			if (id == null) { // don't rewrite "id"
				if (REJET == type) {
					id = "line_${previousCb}_"+n+"_r"+nRejet
				} else {
					id = "line_${previousCb}_"+n
				}
			}

			if (corresp == null && REJET == type) {
				//println "Warning: no 'corresp' attribute provided for 'rejet' line break at "+parser.getLocation()
				corresp = "#line_${previousCb}_"+n; // next word id in the same column
			}

			if (REJET != type) {
				nLb++;
			} else {
				nRejet++;
			}

		} else if (localname == W || localname == PC) {
			if (n == null) {
				n = nW
			} else {
				try {
					int tmp = Integer.parseInt(n)
					if (nW < tmp) {
						nW = tmp
					} else {
						n = nW;
					}
				} catch(Exception e) { }
			}

			if (id == null) { // don't rewrite "id"
				//previousW = "${previousPb}_"+nW
				id = localname+"_${textname}_"+nW
			} 
			previousW = id
			nSeg = 1
			nW++
		} else if (localname == SEG && WP == type) {
			if (n == null) {
				n = nSeg
			} else {
				try {
					int tmp = Integer.parseInt(n)
					if (nSeg < tmp) {
						nSeg = tmp
					} else {
						n = nSeg;
					}
				} catch(Exception e) { }
			}

			if (id == null) { // don't rewrite "id"
				id = "w_p_"+previousW+"_"+nSeg
			}
			nSeg++
		}

		// write attributes except ID, N and CORRESP
		for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
			if (parser.getAttributeLocalName(i) == ID) {

			} else if (parser.getAttributeLocalName(i) == N) {

			}  else if (parser.getAttributeLocalName(i) == CORRESP) {

			} else {
				writeAttribute(parser.getAttributePrefix(i), parser.getAttributeLocalName(i), parser.getAttributeValue(i))
			}
		}
		//if (localname == "lb") println "write id: "+["xml", "id", id]
		if (id != null)
			writeAttribute(XML, ID, id)
		if (n != null)
			writeAttribute(null, N, n)
		if (corresp != null)
			writeAttribute(null, CORRESP, corresp)
	}

	public static void main(String[] args) {
		File xmlFile = new File("/home/mdecorde/xml/bugrejet/Psautier5-or28.xml")
		File outFile = new File("/home/mdecorde/xml/bugrejet/Psautier5-or28-o.xml")

		WriteIdAndNAttributes wiana = new WriteIdAndNAttributes(xmlFile, "qgraal_cm")
		println wiana.process(outFile)

		String text = xmlFile.getText().replaceAll(">", ">\n");
		new File("/home/mdecorde/xml/bugrejet/Psautier5-or28-p.xml").write(text);
		text = outFile.getText().replaceAll(">", ">\n");
		new File("/home/mdecorde/xml/bugrejet/Psautier5-or28-o-p.xml").write(text);
	}
}
