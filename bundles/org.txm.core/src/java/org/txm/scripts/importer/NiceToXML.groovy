package org.txm.scripts.importer

import javax.xml.stream.XMLStreamException
import org.apache.tools.ant.types.resources.selectors.InstanceOf;
import groovy.xml.*

def root = new File("/home/mdecorde/xml/temoignagesnice/corpus Matrice - fichiers xmlisés/")
File srcdir = new File(root, "orig");
File outdir = new File(root, "tmp");
File okdir = new File(root, "ok");
File ok2dir = new File(root, "ok2");
File temoignagedir = new File(root, "temoignages");
ok2dir.deleteDir()
ok2dir.mkdir()

//rename title -> head
for (def file : outdir.listFiles()) {
	if (!file.getName().endsWith(".xml")) continue;
	def doc = new XmlParser().parse(file);
	
	for (def note : doc.body.chapter.title) {
		println note
		note.name = "head"
	}
	
	new File(ok2dir, file.getName()).withWriter("UTF-8") { writer ->
		new XmlNodePrinter(new PrintWriter(writer)).print(doc)
	}
}

/*
// ADD chapter@title
for (def file : outdir.listFiles()) {
	if (!file.getName().endsWith(".xml")) continue;
	def doc = new XmlParser().parse(file);
	
	for (def chapter : doc.body.chapter) {
		for (def title : chapter.title) {
			chapter.@title = title.text()
			break;
		}
	}
	
	new File(ok2dir, file.getName()).withWriter("UTF-8") { writer ->
		new XmlNodePrinter(new PrintWriter(writer)).print(doc)
	}
}
*/
//FIX figure and caption inclusions
/*
for (def file : outdir.listFiles()) {
	if (!file.getName().endsWith(".xml")) continue;
	def doc = new XmlParser().parse(file);
	for (def note : doc.body."**".figure) {
		note.name = "note"
		//println "fig : $note"
		//if ("Image :" == note.text()) {
			def children = note.parent().children()
			int i = children.indexOf(note)
			//println i + " < "+children.size()
			def nextChild = children[i+1]
			if (nextChild != null && nextChild.name().toString() == "caption") {
				println nextChild

				note.value = "Images : "+note.text()// + " "+nextChild.text()
				//println note

				children.remove(i+1)
				note.append(nextChild)
			}
		//}
	}

	new File(ok2dir, file.getName()).withWriter("UTF-8") { writer ->
		new XmlNodePrinter(new PrintWriter(writer)).print(doc)
	}
}
*/
/*
// DOCBOOK -> DOCBOOK TEXT ONLY
for (def file : outdir.listFiles()) {
	def doc = new XmlParser().parse(file);

	def body = null
	def bookinfo = null
	def preface = null
	for (def e : doc.body) body = e

	for (def e : doc.bookinfo) {
		doc.remove(e)
	}
	for (def e : doc.preface) {
		doc.remove(e)
	}
	for (def e : doc.appendix) {
		doc.remove(e)
	}
	for (def e : doc.chapter) {
		doc.remove(e)
	}

	if (body == null) {
		println "error text: "+file
		continue
	}

	new File(ok2dir, file.getName()).withWriter("UTF-8") { writer ->
		new XmlNodePrinter(new PrintWriter(writer)).print(doc)
	}
	//		writer.print XmlUtil.serialize(new StreamingMarkupBuilder().bind {
	//			mkp.yield body
	//		  })
}
*/
//DOCBOOK to TEI
/*
 for (def file : outdir.listFiles()) {
 def doc = new XmlParser().parse(file);
 def body = null
 //def bookinfo = null
 for (def e : doc.body) body = e
 //for (def e : doc.bookinfo) bookinfo = e
 //println body.getClass()
 if (body == null) {
 println "error text: "+file
 continue
 }
 //	bookinfo.name = "teiHeader"
 body.name = "text"
 def teins = new groovy.xml.Namespace("http://www.tei-c.org/ns/1.0",'tei')
 //	for (def node : body."**") {
 //		if (node instanceof String) continue
 //		def name = node.name()
 //		if (name instanceof String)
 //			node.name = teins.get(name)
 //		else 
 //			node.name = teins.get(name.getLocalPart())
 //	}
 for (def figure : body."**".figure) {
 figure.name = "note"
 figure.value = "Image : " + figure.caption.text()
 }
 for (def chapter : body."**".chapter) {
 chapter.name = "div"
 chapter.@type = "chapter"
 }
 for (def caption : body."**".title) {
 caption.name = "head"
 }
 for (def para : body."**".para) {
 para.name = "p"
 }
 def newdoc = new Node(null, "TEI");
 newdoc.@xmlns="http://www.tei-c.org/ns/1.0";
 newdoc.append(new Node(null, "teiHeader"))
 newdoc.append(body)
 new File(okdir, file.getName()).withWriter("UTF-8") { writer ->
 new XmlNodePrinter(new PrintWriter(writer)).print(newdoc)
 }
 //		writer.print XmlUtil.serialize(new StreamingMarkupBuilder().bind {
 //			mkp.yield body
 //		  })
 }
 */

// remove TEI
/*outdir.deleteDir()
 outdir.mkdir()
 def errors = []
 for (def file : srcdir.listFiles()) {
 if (file.isDirectory()) continue;
 //new EncodingConverter(file, "Windows-1252", "UTF-8")
 File outfile = new File(outdir, file.getName());
 outfile.withWriter("UTF-8") { writer ->
 file.eachLine("UTF-8") { line ->
 if (line.trim() == "<TEI>") {
 } else if (line.trim() == "</TEI>") {
 writer.println("</book>")
 } else if (line.trim() == "<book lang=\"fr\"/>") {
 writer.println("<book lang=\"fr\">")
 } else {
 writer.println(line)
 }
 }		
 }
 try {
 ValidateXml.testAndThrow(outfile);
 } catch (XMLStreamException e) {
 println file.getName() + " : "+ e.getMessage()
 errors << file
 if (e.getMessage().contains('Message: The element type "TEI" must be terminated by the matching end-tag "</TEI>"')) {
 println "Delete line : "+e.location.lineNumber
 }
 println ""
 }
 }
 */
println "done"
//if (errors.size() > 0)
//	println ""+errors.size()+" errors : $errors"
//String content = file.getText("Windows-1252")
//println content