package org.txm.scripts.importer

import java.io.Writer;
import java.io.File;
import javax.xml.stream.*;
import java.net.URL;
import org.txm.importer.filters.*;

class SplitBy 
{
	
	XMLOutputFactory factory = XMLOutputFactory.newInstance();
	FileOutputStream output;
	XMLStreamWriter writer;
	def parser, inputData;
	ArrayList<String> newfiles = [];
	
	public SplitBy(File xmlfile)
	{
		try {
			inputData = xmlfile.toURI().toURL().openStream();
			def inputFactory = XMLInputFactory.newInstance();
			parser = inputFactory.createXMLStreamReader(inputData);
			
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		}catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}
	boolean inby = false;
	public boolean process(File outdir, String by, String idAttribute)
	{
		outdir.mkdirs();
		try
		{
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				
				switch(event)
				{
					case XMLStreamConstants.START_ELEMENT:
						if(parser.getLocalName() == by)
						{
							
							inby = true;
							if(writer != null)
							{
								writer.close();
								output.close();
							}
							
							String filename = "";
							for(int i = 0 ; i < parser.getAttributeCount(); i++)
								if(parser.getAttributeLocalName(i) == idAttribute)
								{
									filename = parser.getAttributeValue(i);
									break;
								}
							File newfile = new File(outdir,filename+".xml")
							if(files.contains(newfile))
							{
								println "Item declared twice "+filename+" at location "+parser.getLocation();
								if (parser != null) parser.close();
								if (inputData != null) inputData.close();
								return false;
							}
							files.add(newfile);
							output = new FileOutputStream(newfile)
							writer = factory.createXMLStreamWriter(output, "UTF-8");//create a new file
							
						}
						if(writer != null)
						{
							writeEvent(event);
							if(parser.getLocalName() == "TEI")
							{ // write namespaces
								writer.writeNamespace("tei", "http://www.tei-c.org/ns/1.0")
								writer.writeNamespace("txm", "http://textometrie.org/1.0")
							}
						}
						break;
					case XMLStreamConstants.END_ELEMENT:
						if(inby)
						{
							if(writer != null)
							{
								writer.writeEndElement();
							}
						}
						if(parser.getLocalName() == by)
						{
							inby = false; // write the end element $by before
						}
						break;
					case XMLStreamConstants.CHARACTERS:
						if(writer != null)
						{
							writer.writeCharacters(parser.getText());
						}
						break;
				}
				
			}
			if(writer != null)
				writer.close();
			if(output != null)
				output.close();
				if (parser != null) parser.close();
				if (inputData != null) inputData.close();
			return true;
		}
		catch(Exception e){
			println "XML Error at location: "+parser.getLocation()
			e.printStackTrace()
			if (writer != null)
				writer.close()
				output.close()
				if (parser != null) parser.close();
				if (inputData != null) inputData.close();
				return false
			}
	}
	
	/**
	 * write the current event.
	 *
	 * @param event the stax event
	 */
	private void writeEvent(int event)
	{
		def prefix = parser.getPrefix();
		if (event == XMLStreamConstants.START_ELEMENT ) 
		{
			def localname = parser.getLocalName(); // write element
			if (prefix != null && prefix.length() > 0)
				writer.writeStartElement(prefix+":"+localname);
			else
				writer.writeStartElement(localname);
			
			for(int i = 0 ; i < parser.getNamespaceCount() ; i++) //write namespaces
			{
				writer.writeNamespace(parser.getNamespacePrefix(i), parser.getNamespaceURI(i));
			}
			
			String attrprefix; // write attributes
			for (int i = 0 ; i < parser.getAttributeCount() ; i++)
			{
				attrprefix = parser.getAttributePrefix(i);
				if (attrprefix != null & attrprefix.length() > 0)
					writer.writeAttribute(attrprefix+":"+parser.getAttributeLocalName(i), parser.getAttributeValue(i))
				else
					writer.writeAttribute(parser.getAttributeLocalName(i), parser.getAttributeValue(i))
			}
			//writer.writeCharacters("\n");
		}
		else if (event == XMLStreamConstants.END_ELEMENT)
		{
			writer.writeEndElement();
			//writer.writeCharacters("\n");
		}
		else if (event == XMLStreamConstants.CHARACTERS)
		{
			writer.writeCharacters(parser.getText());
		}
		else if (event == XMLStreamConstants.COMMENT)
		{
			writer.writeComment(parser.getText().replace("--", "&#x2212;&#x2212;"));
		}
	}
	
	public ArrayList<File> getFiles()
	{
		return newfiles;
	}
	
	public static void main(String[] args)
	{
		File xmlfile = new File("C:\\Documents and Settings\\mdecorde\\Bureau\\MISAT2011\\corpus\\tiempo\\ELTIEMPO.2002-2010.MIN.xml");
		File okfile = new File("C:\\Documents and Settings\\mdecorde\\Bureau\\MISAT2011\\corpus\\tiempo\\");
		File outdir = new File("C:\\Documents and Settings\\mdecorde\\Bureau\\MISAT2011\\corpus\\tiempo\\tiempo");
		outdir.deleteDir();
		outdir.mkdir();
		
		//		Reader reader = new InputStreamReader(new FileInputStream(xmlfile) , "ISO-8859-1");
		//		Writer writer = new OutputStreamWriter(new FileOutputStream(okfile) , "UTF-8");
		//		String line = reader.readLine();
		//		while(line != null)
		//		{
		//			line = line.replaceAll(' ([a-zA-Z0-9]+)="(.*)""', ' $1="$2"')
		//			writer.write(line+"\n");
		//			line = reader.readLine();
		//		}
		//		reader.close();
		//		writer.close();
		//		
		//		
		////		if(!ValidateXml.test(okfile))
		////		{
		////			println "XML not valid";
		////			return;
		////		}
		
		def splitter = new SplitBy(xmlfile);
		if(splitter.process(outdir, "texte", "id"))
			println "success";
		else
			println "failed !"
		
	}
}
