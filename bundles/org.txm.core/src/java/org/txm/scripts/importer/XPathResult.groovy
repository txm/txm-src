// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-04-11 15:30:35 +0200 (mar. 11 avril 2017) $
// $LastChangedRevision: 3426 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer;

import org.txm.importer.PersonalNamespaceContext;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import javax.xml.xpath.*;
import javax.xml.stream.*;
import java.net.URL;

// TODO: Auto-generated Javadoc
/**
 * The Class XPathResult.
 *
 * @authors ayepdieu, mdecorde, vchabanis
 * 
 * return the id of a bfm tag <milestone/>
 */
public class XPathResult {
	
	/** The doc. */
	Document doc;
	XPath xpath;
	
	/**
	 * Instantiates a new x path result.
	 *
	 * @param file the file
	 */
	public XPathResult(File xmlfile) {
		this(xmlfile, true)
	}
	
	/**
	 * Instantiates a new x path result.
	 *
	 * @param file the file
	 */
	public XPathResult(File xmlfile, boolean namespaceAware) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setXIncludeAware(true);
		factory.setNamespaceAware(namespaceAware); // never forget this!
		
		DocumentBuilder builder = factory.newDocumentBuilder();
		doc = builder.parse(xmlfile);
		
		XPathFactory xfactory = XPathFactory.newInstance();
		xpath = xfactory.newXPath();
		xpath.setNamespaceContext(new PersonalNamespaceContext());
	}
	
	public Document getDocument() {
		return doc;
	}
	
	public def getNodes(String query) {
		def rnodes = [];
		
		XPathExpression expr = xpath.compile(query);
		Object result = expr.evaluate(doc.getDocumentElement(), XPathConstants.NODESET);
		if (result instanceof NodeList) {
			NodeList nodes = (NodeList) result;
			for (int i = 0; i < nodes.getLength(); i++) {
				rnodes.add(nodes.item(i));
			}
		} else {
			result.getClass()
			rnodes << result
		}
		return rnodes;
	}
	
	public String getXpathResponse(String query) {
		XPathExpression expr = xpath.compile(query);
		Object result = expr.evaluate(doc.getDocumentElement(), XPathConstants.NODESET);
		
		NodeList nodes = (NodeList) result;
		for (int i = 0; i < nodes.getLength(); i++) {
			//println nodes.item(i)
			return (nodes.item(i).getNodeValue());
		}
	}
	
	public ArrayList<String> getXpathResponses(String query) {
		ArrayList<String> xresult = new ArrayList<String>();
		XPathExpression expr = xpath.compile(query);
		Object result = expr.evaluate(doc.getDocumentElement(), XPathConstants.NODESET);
		
		NodeList nodes = (NodeList) result;
		for (int i = 0; i < nodes.getLength(); i++) {
			//println nodes.item(i)
			xresult.add((nodes.item(i).getNodeValue()));
		}
		return xresult
	}
	
	public String getXpathResponse(String query, String devaultValue) {
		String rez = getXpathResponse(query);
		if (rez == null)
			return devaultValue;
		return rez;
	}
	
	public void close() {
		xpath = null;
		doc = null;
	}
	
	/**
	 * OBSOLETE VERSION FOR TXM return the node text content given a XPath
	 * "//path.../.../@attr"
	 * 
	 * @param path
	 * @param attr
	 *            null or "" return the text of the tag targeted
	 * @return
	 */
	/*
	 * public String getXpathResponse(String path, String attr) throws
	 * ParserConfigurationException, SAXException, IOException,
	 * XPathExpressionException { String Query = path;
	 * 
	 * //println "XPath : "+Query; XPathFactory factory =
	 * XPathFactory.newInstance(); XPath xpath = factory.newXPath();
	 * xpath.setNamespaceContext(new PersonalNamespaceContext());
	 * XPathExpression expr = xpath.compile(Query);
	 * 
	 * Object result = expr.evaluate(doc, XPathConstants.NODESET); // don't work
	 * on windows NodeList nodes = (NodeList) result; //println
	 * "result xpath : "+nodes.getLength(); for(int i = 0 ; i <
	 * nodes.getLength() ; i++) { //println
	 * "item : "+nodes.item(i).getLocalName(); if(attr != null &&
	 * !attr.equals("")) return
	 * nodes.item(i).getAttributes().getNamedItem(attr).getNodeValue(); else
	 * return nodes.item(i).getTextContent();
	 * 
	 * } return ""; }
	 */
	
	static public String getXpathResponse(File xmlfile, String query, String devaultValue) {
		String rez = getXpathResponse(xmlfile, query);
		if (rez == null)
			return devaultValue;
		return rez;
	}
	
	static public String getXpathResponse(File xmlfile, String query, String devaultValue, boolean namespaceAware) {
		String rez = getXpathResponse(xmlfile, query, namespaceAware);
		if (rez == null)
			return devaultValue;
		return rez;
	}
	
	/**
	 * Gets the xpath response.
	 *
	 * @param xmlfile the xmlfile
	 * @param query the query
	 * @return the xpath response
	 */
	static public String getXpathResponse(File xmlfile, String query) {
		return getXpathResponse(xmlfile, query, true);
	}
	
	/**
	 * Gets the xpath response.
	 *
	 * @param xmlfile the xmlfile
	 * @param query the query
	 * @return the xpath response
	 */
	static public String getXpathResponse(File xmlfile, String query, boolean namespaceAware) {
		XPathResult result = new XPathResult(xmlfile);
		return result.getXpathResponse(query);
		//		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		//		factory.setNamespaceAware(namespaceAware); // never forget this!
		//
		//		DocumentBuilder builder = factory.newDocumentBuilder();
		//		Document doc = builder.parse(xmlfile);
		//
		//		XPathFactory xfactory = XPathFactory.newInstance();
		//		XPath xpath = xfactory.newXPath();
		//		xpath.setNamespaceContext(new PersonalNamespaceContext());
		//
		//		XPathExpression expr = xpath.compile(query);
		//		Object result = expr.evaluate(doc, XPathConstants.NODESET);
		//
		//		NodeList nodes = (NodeList) result;
		//		println "size: "+result.getLength()
		//		for (int i = 0; i < nodes.getLength(); i++) {
		//			return (nodes.item(i).getNodeValue());
		//		}
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		println("main of XPathResult")
		File xmlFile = new File("/home/mdecorde/runtime-rcpapplication.product/corpora/DEMOCRATLYON3/txm/DEMOCRATLYON3/DiderotEssais.xml")
		XPathResult processor = new XPathResult(xmlFile)
		for (def node : processor.getNodes(xmlFile, "TEI/text/@date")) {
			println "node: $node"
		}
		//value = XPathResult.getXpathResponse(new File("/home/mdecorde/xml/bfm/strasb.xml"), "tei:TEI/tei:teiHeader/tei:revisionDesc/tei:change[contains(.,'étiquetage morpho')]");
		//println "value: $value"
	}
}