package org.txm.scripts.importer

import org.txm.importer.StaxIdentityParser

class WriteIdAndNAttributesUltraGeneric extends StaxIdentityParser {
	
	String baseID = ""
	def rules = [];
	
	def idCounters = [:]
	def nCounters = [:]
	def parentIds = [:]
	
	def elements;
	def elementShortNames
	def elementParents
	def elementResets
	def elementSelection
	public WriteIdAndNAttributesUltraGeneric(File xmlFile, def elements, def elementSelection, def elementShortNames, def elementParents, def elementResets) {
		super(xmlFile)
		
		this.elements = elements;
		this.elementSelection = elementSelection
		this.elementShortNames = elementShortNames;
		this.elementParents = elementParents
		this.elementResets = elementResets
		
		this.baseID = xmlFile.getName();
		if (baseID.indexOf(".") > 0) {
			baseID = baseID.substring(0, baseID.indexOf("."))
		}
		
		for (String element : elements) {
			idCounters[element] = 1;
			nCounters[element] = 1;
			parentIds[element] = null;
		}
	}
	
	/**
	 * Special rule to add @corresp=#ref to its reference element to element which @attribute=@value
	 */
	public setReferenceRule(String element, String attribute, String value) {
		this.rules << [element, attribute, value]
	}
	
	protected void writeAttributes() {
		if (!idCounters.containsKey(localname) ) {
			super.writeAttributes();
			return;
		}
		println "write attributes $localname"
		boolean idFound = false
		boolean nFound = false
		
		
		for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
			String name = parser.getAttributeLocalName(i);
			String value = parser.getAttributeValue(i);
			
			println " at $name=$value"
			
			//String element_sign = [localname, name, value]
			
			if ("id" == name) {
				if (value == null) {
					value = getNextID()
				}
				
				parentIds[localname] = value
			} else if ("n" == name) {
				if (value == null) {
					value = getNextN()
				} else {
					nCounters[localname] = Integer.parseInt(value)
				}
			}
			
			String attrPrefix = parser.getAttributePrefix(i);
			if (attrPrefix != null && attrPrefix.length() > 0)
				writer.writeAttribute(attrPrefix+":"+name, value);
			else
				writer.writeAttribute(name, value);
		}
		
		if (!idFound) {
			String value = getNextID()
			writer.writeAttribute("id", value);
			parentIds[localname] = value
		}
		if (!nFound) {
			String value = getNextN()
			writer.writeAttribute("n", value);
		}
		
		
		idCounters[localname]++
	}
	
	protected void processStartElement() {
		
		if (idCounters.containsKey(localname)) {
			println "Element: "+localname
			for (String resetElement : elementResets[localname]) {
				println "reset N: "+resetElement
				nCounters[resetElement] = 1
			}
		}
		
		super.processStartElement();
	}
	
	protected String getNextID() {
		String value = elementShortNames[localname]
		
		String parent = elementParents[localname]
		if (parent != null)
			value += "_"+parentIds[parent]
		else
			value += "_"+baseID
		
		value += "-"+idCounters[localname]
		
		return value
	}
	
	protected String getNextN() {
		String value = Integer.toString(nCounters[localname])
		nCounters[localname]++
		return value
	}
	
	public static void main(String[] args) {
		File xmlFile = new File("/home/mdecorde/TEMP/idnmissing.xml")
		File outFile = new File("/home/mdecorde/TEMP/idnmissing-fix.xml")
		
		WriteIdAndNAttributesUltraGeneric wiana = new WriteIdAndNAttributesUltraGeneric(xmlFile,
				[
					"milestone",
					"pb",
					"cb",
					"lb"
				],
				["milestone":"unit=surface", "pb":null, "cb":null, "lb":null],
				["milestone":"surf", "pb":"page", "cb":"col", "lb":"line"],
				["milestone":null, "pb":null, "cb":"pb", "lb":"cb"],
				["milestone":[], "pb":["cb", "lb"], "cb":["lb"], "lb":[]])
		println wiana.process(outFile)
	}
}
