package org.txm.scripts.importer

import org.txm.utils.CsvReader;
import java.nio.charset.Charset;

File csvFile = new File("/home/mdecorde/xml/notices/MARTICE_20130304_TF1_IMAGO_1994-12-31.csv")
CsvReader reader = new CsvReader(csvFile.getAbsolutePath(), ";".charAt(0), Charset.forName("UTF-8"))

reader.readHeaders()
def headers = reader.getHeaders()
int nheader = headers.size()
int nlines = 0;
def counts = new int[nheader]
def values = [:]
for (String key : headers) values[key] = new HashSet()

while (reader.readRecord()) {
	nlines++;
	for (int i = 0 ; i < nheader ; i++) {
		String key = headers[i]
		String str = " "+reader.get(key);
		//print str
		if (str.trim().length() > 0) {
			counts[i] = counts[i] + 1
			values[key] << str.trim()
		}
	}
	//println ""
}

println "N lines: "+nlines
println "Empty cols: N <= 20"
for (int i = 0 ; i < nheader ; i++) {
	
	if (counts[i] < 30) {
		println ( headers[i]+ " : "+counts[i])
		println values[headers[i]]
	}
}