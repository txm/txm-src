package org.txm.scripts.importer

import javax.xml.stream.*
import java.net.URL

class Doc2Transcription {

	String encoding
	File txtfile
	File outfile
	Reader reader
	XMLOutputFactory factory
	FileOutputStream output
	XMLStreamWriter writer

	int noline = 1
	boolean question = false
	int idquestion = 1
	int idresponse = 1
	boolean opened = false

	public Doc2Transcription(File txtfile, File outfile, String encoding)
	{
		this.txtfile = txtfile
		this.outfile = outfile
		this.encoding = encoding

		reader = new InputStreamReader(new FileInputStream(txtfile) , encoding)
		factory = XMLOutputFactory.newInstance()
		output = new FileOutputStream(outfile)
		writer = factory.createXMLStreamWriter(output, "UTF-8")//create a new file
	}

	public boolean process()
	{
		if(reader == null || writer == null)
		{
			println "I/O error"
			return false
		}
		
		// build text id
		String filename = txtfile.getName()
		int idx = filename.lastIndexOf(".")
		if(idx > 0)
			filename = filename.substring(0, idx)

		//write Start doc
		writer.writeStartDocument("UTF-8", "1.0")
		writer.writeStartElement("text") // we create a tag <text>
		writer.writeAttribute("id",filename)// and set its id

		//write first block
		writeStartQuestionResponse()
		
		String line = reader.readLine()
		while(line != null)
		{
			if(line.length() == 0) // saut de ligne
			{
				question = !question
				writer.writeEmptyElement("br")
				writeStartQuestionResponse()
			}
			else
			{
				writer.writeCharacters(line+" \n")
				writer.writeEmptyElement("br")
			}

			noline++
			line = reader.readLine()
		}

		//close xml doc and question/response if needed
		if(opened)
			writer.writeEndElement()// question/response
		writer.writeEndElement()// text

		reader.close()
		writer.close()
		output.close()
		return true
	}

	public void writeStartQuestionResponse()
	{
		if(opened)
		{
			writer.writeEndElement()
			opened = false
		}
		opened = true
		if(question)
		{
			writer.writeStartElement("question") // we create a tag <text>
			writer.writeAttribute("id","q"+idquestion++)// and set its id
		}
		else
		{
			writer.writeStartElement("response") // we create a tag <text>
			writer.writeAttribute("id","r"+idresponse++)// and set its id
		}
	}

	public void setInitialeState(boolean state)
	{
		question = state
	}

	static main(args) {
		File infile = new File("/home/matt/xml/txttranscription/test.txt")
		File outfile = new File("/home/matt/xml/txttranscription/test.xml")
		String encoding = "UTF-8"

		def builder = new Doc2Transcription(infile, outfile, encoding)
		if(builder.process())
			println "ok"
		else
			println "fail"
	}
}