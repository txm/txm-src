package org.txm.scripts.importer

import org.txm.utils.CharsetDetector;
import org.txm.importer.ValidateXml;

File infile = new File("/home/mdecorde/Bureau/matrice/témoignages/CONVERSIONS/jod/odt.html")
File outfile = new File("/home/mdecorde/Bureau/matrice/témoignages/CONVERSIONS/jod/odt.xml")

String encoding = new CharsetDetector(infile).getEncoding();
println "Encoding: $encoding"
String text = infile.getText(encoding);

//lower case tags
text = text.replaceAll(/(<[^!][^>]*>)/, 
	{ full, word -> 
		//fix attributes TRUC=sdf234
		word = word.replaceAll("([A-Z]+=)([^\" >]+)([ >])",'$1"$2"$3' )
		word.toLowerCase() // bourrin
	} )

//lower case <.> tags
text = text.replaceAll(/(<.>)/,
	{ full, word ->
		word.toLowerCase()
	} )

//resolve entities
text = text.replaceAll(/&nbsp;/," ")

//close tags
text = text.replaceAll(/<br>/,"<br/>")
text = text.replaceAll(/<meta([^>]*)>/,'<meta$1/>')
text = text.replaceAll(/<img([^>]*)>/,'<img$1/>')

//remove doctype declaration
text = text.replace('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">', '')

//write&Validate
outfile.withWriter(encoding) { writer -> writer.write(text) }
if (!ValidateXml.test(outfile)) {
	println "FILE: $outfile"
}