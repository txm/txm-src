// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-06-26 16:53:47 +0200 (lun. 26 juin 2017) $
// $LastChangedRevision: 3451 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer

import org.txm.utils.CharsetDetector
import org.txm.utils.i18n.DetectBOM;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import javax.xml.stream.*;
import java.net.URL;

// TODO: Auto-generated Javadoc
/**
 * Create a xml file per text declared in a Hyperbase file (old format).
 *
 * @author mdecorde
 */
class Hyperbase2Xml {
	
	/**
	 * Run.
	 *
	 * @param infile the hyperbase file (old format)
	 * @param outdir where the xml files are created
	 * @param encoding the encoding of the hyperbase file
	 * @return true, if successful
	 */
	public boolean run(File infile, File outdir, String encoding)
	{
		if (encoding == "??") {
			encoding = new CharsetDetector(infile).getEncoding();
			println "Guessing encoding of $infile : $encoding"
		}
		outdir.mkdir();
		
		String textname = null
		String partname = null;
		int textcount = 1;
		def input = new FileInputStream(infile)
		Reader reader = new InputStreamReader(input , encoding);
		DetectBOM bomdetector = new DetectBOM(infile);
		for (int ibom = 0 ; ibom < bomdetector.getBOMSize() ; ibom++) input.read()
		
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		BufferedOutputStream output = null;
		XMLStreamWriter writer = null;
		
		boolean firsttext=true;
		
		String line = reader.readLine();
		while(line != null) {
			line = CleanFile.clean(line);
			if ((line.startsWith("&&&"))) // text declaration &&& longtitle, title, shorttitle &&&
			{
				if (partname != null) // close previous part
				{
					//println("close last part")
					writer.writeEndElement();
					partname= null;
				}
				if (textname != null) // close previous text
				{
					//println("close text "+textname)
					writer.writeEndElement();
					writer.close();
					output.close();
				}
				textname = line.substring(3, line.length()-3);
				if (textname.contains(","))
				{
					String[] textnames = textname.split(",");
					if(textnames.length > 1)
						textname = textnames[1].trim();
					
				}
				// create a new text
				output = new BufferedOutputStream(new FileOutputStream(new File(outdir,textname+".xml")))
				writer = factory.createXMLStreamWriter(output, "UTF-8");
				writer.writeStartElement("text");
				writer.writeAttribute("id",textname);
				println "text : "+new File(outdir,textname+".xml");
			}
			else if((line.startsWith("%%%%"))) // an other type of text declaration
			{
				if(partname != null)
				{
					//println("close last part")
					writer.writeEndElement();
					partname= null;
				}
				if(textname != null)
				{
					//println("close text "+textname)
					writer.writeEndElement();
					writer.close();
					output.close();
				}
				textname = infile.getName().substring(0, infile.getName().length()-4)+(textcount++);
				output =  new BufferedOutputStream(new FileOutputStream(new File(outdir,textname+".xml")))
				writer = factory.createXMLStreamWriter(output, "UTF-8");
				writer.writeStartElement("text");
				writer.writeAttribute("id",textname);
				println "text : "+new File(outdir,textname+".xml");
			}
			else if((line.startsWith("\$"))) // paragraph declaration
			{
				if(partname != null)
				{
					//println("close part "+partname)
					writer.writeEndElement();
				}
				partname = line.substring(1);
				
				writer.writeStartElement("p");
				writer.writeAttribute("id",partname);
			}
			else // brut text > write chars
			{
				if(writer != null)
					writer.writeCharacters(line+"\n");
			}
			line = reader.readLine();
		}
		
		if(partname != null)
		{
			//println("close last part")
			writer.writeEndElement();
		}
		if(writer != null)
			writer.writeEndElement();
		
		reader.close();
		if(output != null)
			output.close();
		
		return true;
	}
	
	/**
	 * Main.
	 *
	 * @param args the args
	 */
	static main(args) 
	{
		File infile = new File(System.getProperty("user.home")+"/xml/presdiscfra/src/PresDiscFranc");
		File outfile = new File(System.getProperty("user.home")+"/xml/presdiscfra/split/");
		new Hyperbase2Xml().run(infile, outfile,"ISO-8859-1");
		println "done"
	}
}
