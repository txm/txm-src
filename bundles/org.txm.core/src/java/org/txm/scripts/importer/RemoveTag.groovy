// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.importer;

import javax.xml.parsers.*
import javax.xml.transform.*
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult
import javax.xml.xpath.*
import org.txm.importer.PersonalNamespaceContext
import org.txm.metadatas.*
import org.txm.utils.*
import org.txm.utils.xml.*
import org.w3c.dom.Document
import org.w3c.dom.Element

/**
 * Removes tags of XML file given a XPath. 
 * @author mdecorde
 *
 */
public class RemoveTag {
	File outfile;
	
	/** The doc. */
	Document doc;
	
	/**
	 *
	 * @param xmlfile the xmlfile
	 * @param outfile the outfile
	 * @param xpath the XPath
	 */
	public RemoveTag(File xmlfile, File outfile, String xpath)
	{
		this.outfile = outfile;
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true); // never forget this!
		domFactory.setXIncludeAware(true);
		DocumentBuilder builder = domFactory.newDocumentBuilder();
		doc = builder.parse(xmlfile);
		
		def expr = XPathFactory.newInstance().newXPath().compile(xpath);
		def nodes = expr.evaluate(doc, XPathConstants.NODESET);
		
		if (nodes != null)
		for(def node : nodes)
		{
			//println "Remove node "+node
			Element elem = (Element)node;
			elem.getParentNode().removeChild(node);
		}
		save()
		doc = null
	}
	
	/**
	 * Save.
	 *
	 * @return true, if successful
	 */
	private boolean save()
	{
		try {
			// Création de la source DOM
			Source source = new DOMSource(doc);
			
			// Création du fichier de sortie
				Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outfile), "UTF-8")); 
			Result resultat = new StreamResult(writer);
			
			// Configuration du transformer
			TransformerFactory fabrique = new net.sf.saxon.TransformerFactoryImpl();
			Transformer transformer = fabrique.newTransformer();
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8"); 
			
			// Transformation
			transformer.transform(source, resultat);
			writer.close();
			return true;
		} catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
	}
	
	public static boolean xpath(File xmlfile, String xpath)
	{
		if (!xmlfile.exists()) {
			println "Error: $xmlfile does not exists"
		}

		if (!(xmlfile.canRead() && xmlfile.canWrite())) {
			println "Error: $xmlfile is not readable or writable"
		}

		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true); // never forget this!
		domFactory.setXIncludeAware(true);
		DocumentBuilder builder = domFactory.newDocumentBuilder();
		Document doc = builder.parse(xmlfile);

		XPathFactory xpathfactory = XPathFactory.newInstance();
		def _xpath = xpathfactory.newXPath();
		_xpath.setNamespaceContext(new PersonalNamespaceContext());
		def expr = _xpath.compile(xpath);
		def nodes = expr.evaluate(doc, XPathConstants.NODESET);

		for(Element node : nodes) {
			def parent = node.getParentNode();
			if (parent != null)
				parent.removeChild(node)
		}
		return DomUtils.save(doc, xmlfile)
	}

	public static boolean xpath(File xmlfile, List<String> xpaths)
	{
		if (!xmlfile.exists()) {
			println "Error: $xmlfile does not exists"
		}

		if (!(xmlfile.canRead() && xmlfile.canWrite())) {
			println "Error: $xmlfile is not readable or writable"
		}

		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true); // never forget this!
		domFactory.setXIncludeAware(true);
		DocumentBuilder builder = domFactory.newDocumentBuilder();
		Document doc = builder.parse(xmlfile);
		XPathFactory xpathfactory = XPathFactory.newInstance();

		for (String xpath : xpaths) {
			def _xpath = xpathfactory.newXPath();
			_xpath.setNamespaceContext(new PersonalNamespaceContext());
			def expr = _xpath.compile(xpath);
			def nodes = expr.evaluate(doc, XPathConstants.NODESET);

			for (Element node : nodes) {
				def parent = node.getParentNode();
				if (parent != null)
					parent.removeChild(node)
			}
		}
		return DomUtils.save(doc, xmlfile)

	}
	
	public static void main(String[] args) {
		RemoveTag rt = new RemoveTag(
			new File("/home/mdecorde/TXM/corpora/graal/import.xml"),
			new File("/home/mdecorde/TXM/corpora/graal/import-o.xml"),
			"//edition[@name='courante']"
			)
	}
}
