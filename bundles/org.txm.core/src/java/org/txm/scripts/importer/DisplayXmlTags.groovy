// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer;

import java.util.HashMap;
import java.util.ArrayList;
import java.io.File;
import javax.xml.stream.*;

import org.txm.utils.io.IOUtils

import java.net.URL;

// TODO: Auto-generated Javadoc
/** count and display the tags of an xml file. @author mdecorde */
class DisplayXmlTags {
	ArrayList<String> paths = new ArrayList<String>(); // contains the xpath of
	// the tags
	
	/** The counts. */
	HashMap<String, Integer> counts = new HashMap<String, Integer>(); // contains the counts per tag
	
	/** The chars. */
	HashMap<String, Integer> chars = new HashMap<String, Integer>(); // contains the char counts per tag

	/** The currentpath. */
	String currentpath = "";
	
	/** The sum. */
	public int sum = 0;

	/**
	 * Instantiates a new display xml tags.
	 *
	 * @param infile : the file to parse
	 */
	public DisplayXmlTags(File infile) {
		if (infile.isDirectory()) {
			for (File f : infile.listFiles(IOUtils.HIDDENFILE_FILTER)) {
				processxmlFile(f);
			}
		} else
			processxmlFile(infile);
	}

	/**
	 * run the script.
	 *
	 * @param xmlfile the xmlfile
	 * @return true, if successful
	 */
	private boolean processxmlFile(File xmlfile)
	{
		def inputData = null;
		def factory = null;
		try
		{
			URL url = xmlfile.toURI().toURL();
			inputData = url.openStream();
			 factory = XMLInputFactory.newInstance();
			XMLStreamReader parser = factory.createXMLStreamReader(inputData);
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) 
			{
				switch (event) 
				{
					case XMLStreamConstants.START_ELEMENT:
					
						currentpath += "/"+parser.getLocalName();// append the current tag to the current path
						if(!paths.contains(currentpath))
						{
							paths.add(currentpath);
							counts.put(currentpath, 0);
							chars.put(currentpath, 0);
						}
						counts.put(currentpath, counts.get(currentpath)+1); // increment path count
					
						break;
					case XMLStreamConstants.END_ELEMENT:
						currentpath = currentpath.substring(0,currentpath.length() -1 - parser.getLocalName().length()) // remove tag from the path
						break;
					case XMLStreamConstants.CHARACTERS:
						
						chars.put(currentpath, chars.get(currentpath)+parser.getText().trim().length());
						sum += parser.getText().trim().length();
				}
			}
			parser.close();
			inputData.close();
		}
		catch(Exception e){
			println("File "+xmlfile+"\n"+e);
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
			return false;
		}
		
		return true;
	}

	/**
	 * Gets the tag hierarchy.
	 *
	 * @return the hierarchy of the tags
	 */
	public ArrayList<String> getTagHierarchy() {
		return paths;
	}

	/**
	 * return the counts of a tag.
	 *
	 * @param path : the tag path (ex : /TEI/text/p")
	 * @return the count
	 */
	public int getCount(String path) {
		return counts.get(path);
	}

	/**
	 * Gets the counts.
	 *
	 * @return all the tags counts
	 */
	public int getCounts() {
		return counts;
	}
	
	/**
	 * return the counts of chars of  a tag.
	 *
	 * @param path : the tag path (ex : /TEI/text/p")
	 * @return the char
	 */
	public int getChar(String path) {
		return chars.get(path);
	}

	/**
	 * Gets the chars.
	 *
	 * @return all the tags counts
	 */
	public int getChars() {
		return chars;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		String userhome = System.getProperty("user.home"); 
		DisplayXmlTags diag = new DisplayXmlTags(new File(userhome, "xml/manuelTXM/Manuel_TEI_FR_0_5.xml"));
		ArrayList<String> paths = diag.getPaths();
		Collections.sort(paths);
		if(paths != null)
			for(String s : paths)
			{
				print s+" : "+diag.getCount(s);
				if(diag.getChar(s) > 0)
					print " (chars "+diag.getChar(s)+")";
					
				println ""
			}
		
		println "total chars : "+diag.sum;
	}
}
