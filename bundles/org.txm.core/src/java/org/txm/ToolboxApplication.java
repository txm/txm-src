package org.txm;

import org.eclipse.core.runtime.adaptor.EclipseStarter;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;

public class ToolboxApplication implements IApplication {
	
	public static void main(String[] args) {
		
		try {
			
//			//Load the framwork factory
//	        ServiceLoader loader = ServiceLoader.load( FrameworkFactory.class );
//	        System.out.println("ServiceLoader= "+loader);
//	        
//	        FrameworkFactory factory = ( FrameworkFactory ) loader.iterator( ).next( );
//	        System.out.println("FrameworkFactory= "+loader);
//	        
//	        // Create a new instance of the framework
//	        Framework framework = factory.newFramework( null );
//	        System.out.println("Framework="+framework);
//	        framework.init( );
//	        framework.start( );
//	        
//	        BundleContext bc = framework.getBundleContext();
//	        System.out.println("BundleContext: "+bc);
//	        
//	        Filter filter = bc.createFilter("(objectClass=*ServiceTasks)");
//	        System.out.println("Filter: "+filter);
//	        
//	        ServiceTracker serviceTaskTracker = new ServiceTracker(bc, filter, null);
//	        System.out.println("ServiceTracker: "+serviceTaskTracker);
//	        
//	        InternalPlatform.getDefault().start(bc);
//	        System.out.println("context started");
//	      
			
			
			
			String[] pargs = {"-application", "org.txm.core.Toolbox", "-data", "/home/mdecorde/workspace047/../runtime-rcpapplication.product", "-configuration", "file:/home/mdecorde/workspace047/.metadata/.plugins/org.eclipse.pde.core/rcpapplication.product/", "-dev", "file:/home/mdecorde/workspace047/.metadata/.plugins/org.eclipse.pde.core/rcpapplication.product/dev.properties", "-os", "linux", "-ws", "gtk", "-arch", "x86_64", "-nl", "fr_FR", "-consoleLog", "-run"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$
			EclipseStarter.main(pargs);
//			System.out.println("plugins: "+Arrays.toString(Platform.getPluginRegistry().getPluginDescriptors()));
//			System.out.println("ext pts: "+Arrays.toString(Platform.getPluginRegistry().getExtensionPoints()));

			
			//Toolbox.initialize(TBXPreferences.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static boolean started = false;
	private boolean goOn = true;
	private int exitCode = IApplication.EXIT_OK;
	
	public void stopToolbox() {
		goOn = false;
	}
	
	public void setExitCode(int exitCode) {
		this.exitCode = exitCode;
	}

	@Override
	public Object start(IApplicationContext context) throws Exception {
		//System.out.println("Starting Toolbox...");
		//Toolbox.initialize(TBXPreferences.class);
		started = true;
		while (goOn) {
			Thread.sleep(1000);
			System.out.println("T"+started); //$NON-NLS-1$
		}
		started = false;
		return IApplicationContext.EXIT_ASYNC_RESULT;
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
	}
}
