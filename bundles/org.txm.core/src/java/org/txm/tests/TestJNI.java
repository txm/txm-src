package org.txm.tests;

class TestJNI {
	public native void afficherBonjour();

	static {
		System.out.println(System.getProperty("java.library.path"));  //$NON-NLS-1$
		System.loadLibrary("mabibjni"); //$NON-NLS-1$
	}

	public static void main(String[] args) {
		new TestJNI().afficherBonjour();
	}
}
