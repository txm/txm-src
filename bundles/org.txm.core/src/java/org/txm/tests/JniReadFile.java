package org.txm.tests;

import java.io.File;

class JniReadFile {
	//Native method declaration
	native byte[] loadFile(String name);
	
	//Load the library
	static {
		System.out.println(System.getProperty("java.library.path"));  //$NON-NLS-1$
		System.loadLibrary("nativelib"); //$NON-NLS-1$
	}

	public static void main(String args[]) {
		byte buf[];
		
		//Create class instance
		JniReadFile mappedFile = new JniReadFile();
		
		//Call native method to load ReadFile.java
		File aFile = new File("/home/mdecorde/Bureau/modifiedCWBSrc/cwb/trunk/COPYING"); //$NON-NLS-1$
		buf = mappedFile.loadFile(aFile.getAbsolutePath());
		
		//Print contents of ReadFile.java
		for (int i=0 ; i<buf.length ; i++) {
			System.out.print((char)buf[i]);
		}
	}
}
