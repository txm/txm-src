package org.txm.objects;

import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Wrapper for getting and setting corpus command default parameters
 * //TODO move the logic to TXMPreferences 
 * 
 * @author mdecorde
 *
 */
public class CorpusCommandPreferences {

	Preferences node;
	
	public CorpusCommandPreferences(Project project, String name) {
		this.node = project.getPreferencesScope().getNode(CorpusCommandPreferences.class.getSimpleName()).node(name);
		setName(name);
	}
	
	public Preferences getNode() {
		return this.node;
	}

	public String getName() {
		return node.get(TBXPreferences.NAME, null);
	}

	public void set(String key, String value) {
		node.put(key, value);
	}
	
	public String get(String key) {
		return node.get(key, null);
	}
	
	public String get(String key, TXMPreferences alternative) {
		String value = node.get(key, null);
		if (value == null) {
			return alternative.getString(key);
		} else {
			return value;
		}
	}
	
	public Integer getInt(String key, TXMPreferences alternative) {
		String v = get(key, alternative);
		try {
			return Integer.parseInt(v);
		} catch (Exception e) { return 0; }
	}

	public void setName(String name) {
		node.put(TBXPreferences.NAME, name);
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		try {
			for (String k : node.keys()) {
				buffer.append(k+"="+node.get(k, "")+"\t");
			}
		} catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return buffer.toString();
	}
}
