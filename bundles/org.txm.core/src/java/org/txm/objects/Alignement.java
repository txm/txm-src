package org.txm.objects;



/**
 * not used ?
 * deprecated ?
 * 
 * @author mdecorde
 *
 */
public class Alignement {

	public String from, to , struct;
	public int level;

	private Project base;
	
	public Alignement(Project base, String from2, String to2, String struct2, int level2) {
		this.base = base;
		from = from2;
		to = to2;
		struct = struct2;
		level= level2;
	}
}
