// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (Tue, 24 Jan 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.objects;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.osgi.util.NLS;
import org.txm.Toolbox;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;
import org.txm.utils.LogMonitor;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;
import org.txm.utils.zip.Zip;
import org.w3c.dom.Element;

/**
 * The Class Workspace: manages the TXM corpora in the $TXMHOME/corpora directory
 */
public class Workspace extends TXMResult {

	private static final String NAME = "name";

	IWorkspace rcpWorkspace;

	IWorkspaceRoot root;

	IProject corporaProject;
	// /** The xmlfile. */
	// File xmlDefinitionFile;
	//
	// /** The doc. */
	// Document doc;
	//
	// /** The projs. */
	// Element projs;


	// /**
	// * Creates the empty workspace.
	// *
	// * @param xmlfile the xmlfile
	// * @return the workspace
	// */
	// public static Workspace createEmptyWorkspaceDefinition(File xmlfile) {
	// writeEmptyWorkspaceFile(xmlfile);
	// return new Workspace(xmlfile);
	// }
	//
	// /**
	// * Write empty workspace file.
	// *
	// * @param xmlfile the xmlfile
	// * @return true, if successful
	// */
	// protected static boolean writeEmptyWorkspaceFile(File xmlfile) {
	// xmlfile.getParentFile().mkdirs();
	// try {
	// OutputStreamWriter writer = new OutputStreamWriter(
	// new FileOutputStream(xmlfile), TXMPreferences.DEFAULT_ENCODING);
	// writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"); //$NON-NLS-1$
	// writer.write("<workspace>\n"); //$NON-NLS-1$
	// writer.write("<projects>\n"); //$NON-NLS-1$
	// writer.write("<project name=\"default\">\n"); //$NON-NLS-1$
	// writer.write("<bases/>\n"); //$NON-NLS-1$
	// writer.write("</project>\n"); //$NON-NLS-1$
	// writer.write("</projects>\n"); //$NON-NLS-1$
	// writer.write("</workspace>\n"); //$NON-NLS-1$
	// writer.close();
	// } catch (IOException e) {
	// System.out.println(TXMCoreMessages.Workspace_6+xmlfile.getAbsolutePath());
	// Log.severe(TXMCoreMessages.Workspace_6+xmlfile.getAbsolutePath()+" : "+e.getMessage()); //$NON-NLS-1$
	// return false;
	// }
	// return true;
	// }

	/**
	 * Instantiates a new workspace.
	 *
	 * @throws CoreException 
	 */
	private Workspace() throws CoreException {
		super("ROOT");

		rcpWorkspace = ResourcesPlugin.getWorkspace();
		rcpWorkspace.getRoot().refreshLocal(org.eclipse.core.resources.IResource.DEPTH_INFINITE, null);
		
		// Deactive autobuild to limit lags and errors
		IWorkspaceDescription desc = rcpWorkspace.getDescription();
		desc.setAutoBuilding(false);
		rcpWorkspace.setDescription(desc);
		
		root = rcpWorkspace.getRoot();
		corporaProject = root.getProject("corpora");

		if (!corporaProject.exists()) {
			try {
				corporaProject.create(null);
			}
			catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		this.setUserName(corporaProject.getName()); // corpora
		// this.setPath("/"+pName);
		// this.xmlDefinitionFile = xmlfile;
	}

	@Override
	public boolean isInternalPersistable() {
		return true;
	}

	public static Workspace getInstance() {
		if (Toolbox.workspace == null) {
			try {
				Toolbox.workspace = new Workspace();
			}
			catch (CoreException e) {
				e.printStackTrace();
				return null;
			}
		}

		return Toolbox.workspace;
	}

	/**
	 * Gets the projects.
	 *
	 * @return the projects
	 */
	@SuppressWarnings("unchecked")
	public List<Project> getProjects() {
		return getChildren(Project.class);
	}

	/**
	 * Gets the project.
	 *
	 * @param name the name
	 * @return the project
	 */
	public Project getProject(String name) {
		for (Project p : getProjects()) {
			if (p.getName().equals(name)) {
				return p;
			}
		}
		return null;
	}

	@Override
	public String getSimpleName() {
		return getUserName();
	}

	@Override
	public String getDetails() {
		return getUserName();
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub
	}

	@Override
	@Deprecated
	public boolean _toTxt(File arg0, String arg1, String arg2, String arg3) throws Exception {
		return false;
	}

	public File getLocation() {
		return corporaProject.getLocation().toFile().getAbsoluteFile();
	}

	@Override
	public void onWorkspaceClose() {
		
		List<TXMResult> todo = new ArrayList<TXMResult>(this.getChildren());
		while (todo.size() > 0) {
			TXMResult r = null;
			r = todo.remove(0);
			todo.addAll(0, r.getChildren());
			
			r.onWorkspaceClose();
		}
	}
	
	/**
	 * 
	 * @param binCorpusDir the directory to scan
	 * @return true if the directory contains the mandatory files of a TXM 0.8.0+ corpus
	 */
	public static boolean isCorpusBinarydirectoryValid(File binCorpusDir) {

		// System.out.println(Messages.Project_5);
		File HTMLs = new File(binCorpusDir, "HTML"); //$NON-NLS-1$
		// File TXMs = new File(binCorpusDir, "txm"); //$NON-NLS-1$
		File DATAs = new File(binCorpusDir, "data"); //$NON-NLS-1$
		File REGISTRY = new File(binCorpusDir, "registry"); //$NON-NLS-1$
		File PARAMS = new File(binCorpusDir, ".settings"); //$NON-NLS-1$
		File PROJECT = new File(binCorpusDir, ".project"); //$NON-NLS-1$

		return binCorpusDir.exists() 
				&& HTMLs.exists() 
				&& DATAs.exists()
				&& DATAs.listFiles().length > 0
				&& REGISTRY.exists() 
				&& REGISTRY.listFiles().length > 0
				&& PARAMS.exists() 
				&& PROJECT.exists();// && correspfile.exists())
	}

	/**
	 * Install a 0.8.0 corpus from its TXM binary file and call scanDirectory
	 *
	 * @param txmFile the corpus file
	 * @return the corpus project
	 * @throws Exception
	 */
	public Project installCorpus(File txmFile) throws Exception {
		String basedirname = Zip.getRoot(txmFile);

		Zip.decompress(txmFile, Toolbox.workspace.getLocation(), false, new LogMonitor("Extracting "+txmFile));

		File binCorpusDirectory = new File(Toolbox.workspace.getLocation(), basedirname);
		if (!binCorpusDirectory.exists()) {
			Log.warning("*** Error no binary corpus found at: "+binCorpusDirectory);
			return null;
		}
		return Toolbox.workspace.scanDirectory(binCorpusDirectory);
	}
	
	/**
	 * Scan directory:
	 * 1- check the binary corpus directory format 
	 * 2- register in the workspace
	 *
	 * @param binCorpusDir the corpus project directory
	 * @return the corpus project
	 * @throws Exception
	 */
	public Project scanDirectory(File binCorpusDir) throws Exception {

		// System.out.println(Messages.Project_5);
		File HTMLs = new File(binCorpusDir, "HTML"); //$NON-NLS-1$
		// File TXMs = new File(binCorpusDir, "txm"); //$NON-NLS-1$
		File DATAs = new File(binCorpusDir, "data"); //$NON-NLS-1$
		File REGISTRY = new File(binCorpusDir, "registry"); //$NON-NLS-1$
		File PARAMS = new File(binCorpusDir, ".settings"); //$NON-NLS-1$
		File PROJECT = new File(binCorpusDir, ".project"); //$NON-NLS-1$
		binCorpusDir = new File(this.getLocation(), binCorpusDir.getName()); // this is to ensure the project is in the workspace files

		if (binCorpusDir.exists() && HTMLs.exists() && DATAs.exists()
				&& REGISTRY.exists() && PARAMS.exists() && PROJECT.exists())// && correspfile.exists())
		{// minimal base data

			File[] maincorpusNames = DATAs.listFiles();
			String maincorpusName = "";
			if (maincorpusNames.length == 0) { // no CQP corpus
				System.out.println(NLS.bind("Error: no CQP corpus data found in {0} directory.", DATAs));
				return null;
			}
			else {
				maincorpusName = maincorpusNames[0].getName();
			}

			Project p = getProject(binCorpusDir.getName());
			if (p != null) {
				p.delete(); // a project with the same name already exists
			}
			p = getProject(maincorpusName);
			if (p != null) {
				p.delete(); // a project with the same name already exists
			}

			// System.out.println(parameters.toString());

			// load the project from the project .settings directory
			IWorkspace rcpWorkspace = ResourcesPlugin.getWorkspace();
			// rcpWorkspace.getRoot().refreshLocal(1, new LogMonitor());

			IProjectDescription description = rcpWorkspace.loadProjectDescription(new Path(binCorpusDir.getAbsolutePath() + "/.project")); //$NON-NLS-1$
			description.setLocation(new Path(binCorpusDir.getAbsolutePath()));
			IProject rcpProject = rcpWorkspace.getRoot().getProject(description.getName());

			if (!rcpProject.exists()) {
				rcpProject.create(description, new LogMonitor());
			}
			if (!rcpProject.isOpen()) {
				rcpProject.open(new LogMonitor());
			}

			Project b = Project.loadProjectFromProjectScope(rcpProject);
			if (b != null) {
				return b;
			}
			else {
				Log.warning(NLS.bind("No TXM project found in {0}", binCorpusDir));
				return null;
			}
		}
		else {
			System.out.println(TXMCoreMessages.bind(TXMCoreMessages.errorColonP0CorpusDirectoryIsNotConformantToTXMCorpusBinaryFormatColonCorpusSkipped, binCorpusDir));
			System.out.println(TXMCoreMessages.bind(TXMCoreMessages.tXMNeedsFoldersColonP0P1P2AndP3, HTMLs, DATAs, REGISTRY, PARAMS));
		}
		return null;
	}

	/**
	 * 
	 * @param binCorpusDir the directory to scan
	 * @return true if the directory contains the mandatory files of a TXM 0.7.X corpus
	 */
	public static boolean isCorpus079DirectoryValid(File binCorpusDir) {
		// System.out.println(Messages.Project_5);
		File HTMLs = new File(binCorpusDir, "HTML"); //$NON-NLS-1$
		// File TXMs = new File(binCorpusDir, "txm"); //$NON-NLS-1$
		File DATAs = new File(binCorpusDir, "data"); //$NON-NLS-1$
		File REGISTRY = new File(binCorpusDir, "registry"); //$NON-NLS-1$
		File PARAMS = new File(binCorpusDir, "import.xml"); //$NON-NLS-1$

		return binCorpusDir.exists()
				&& HTMLs.exists() 
				&& DATAs.exists()
				&& DATAs.listFiles().length > 0
				&& REGISTRY.exists() 
				&& REGISTRY.listFiles().length > 0
				&& PARAMS.exists();// && correspfile.exists())
	}

	/**
	 * Scan directory:
	 * 1- check the binary corpus format
	 * 2- register in the workspace
	 *
	 * @param binCorpusDir the base
	 * @return the base
	 * @throws Exception
	 */
	public Project scanDirectory079(File binCorpusDir) throws Exception {

		// System.out.println(Messages.Project_5);
		File HTMLs = new File(binCorpusDir, "HTML"); //$NON-NLS-1$
		// File TXMs = new File(binCorpusDir, "txm"); //$NON-NLS-1$
		File DATAs = new File(binCorpusDir, "data"); //$NON-NLS-1$
		File REGISTRY = new File(binCorpusDir, "registry"); //$NON-NLS-1$
		File PARAMS = new File(binCorpusDir, "import.xml"); //$NON-NLS-1$

		if (isCorpus079DirectoryValid(binCorpusDir))// && correspfile.exists())
		{// minimal base data
			BaseOldParameters parameters = new BaseOldParameters(PARAMS);
			if (!parameters.load()) {
				System.out.println(NLS.bind(TXMCoreMessages.failedToLoadBaseParametersFromTheP0File, PARAMS));
				Log.severe(NLS.bind(TXMCoreMessages.failedToLoadBaseParametersFromTheP0File, PARAMS));
				return null;
			}

			// if (parameters.version == null || parameters.version.length() == 0) {
			// System.out.println(TXMCoreMessages.Project_14);
			// return null;
			// } else if (CURRENTCORPUSVERSION.compareTo(parameters.version) > 0) {
			// System.out.println(TXMCoreMessages.Project_15);
			// System.out.println(CURRENTCORPUSVERSION+" > "+parameters.version); //$NON-NLS-1$
			// return null;
			// }

			// System.out.println(parameters.toString());

			// Creates a new project that doesn't need to build

			// FIXME: SJ: old version where there is a bug if the name or path are not in same case
			// Project project = new Project(this, parameters.name, false);
			// FIXME: SJ: new version that fix the bug but whe may want to not modify the case of the name?
			Project project = new Project(this, binCorpusDir.getName(), false);
			// if (!parameters.name.equals(project.getRCPProject().getName())) {
			// IPath destination = new Path(project.getRCPProject().getParent().getLocation(), parameters.name);
			// project.getRCPProject().move(destination, true, project);
			// }

			project._load(parameters);
			project.pCorpusVersion = "0.7.9 -> 0.8.0";
			// b._compute();
			// System.out.println("CREATE CORPORA? "+b.corpora.values());
			for (Element c : parameters.corpora.values()) {
				String corpusname = c.getAttribute(NAME);
				// File TXMc = new File(TXMs, corpusname);
				File HTMLc = new File(HTMLs, corpusname);
				File REGISTRYc = new File(REGISTRY, corpusname.toLowerCase());
				File DATAc = new File(DATAs, corpusname);

				if (!(PARAMS.exists() && HTMLc.exists() && REGISTRYc.exists() && DATAc.exists())) {
					System.out.println(NLS.bind(TXMCoreMessages.errorColonSkippingTheLoadingOfTheP0Corpus, corpusname));
					System.out.println(NLS.bind(TXMCoreMessages.pARAMSColonP0, PARAMS));
					System.out.println("HTMLc: " + HTMLc + " : " + HTMLc.exists()); //$NON-NLS-1$ //$NON-NLS-2$
					System.out.println("REGISTRYc: " + REGISTRYc + " : " + REGISTRYc.exists()); //$NON-NLS-1$ //$NON-NLS-2$
					System.out.println("DATAc: " + DATAc + " : " + DATAc.exists()); //$NON-NLS-1$ //$NON-NLS-2$
					continue;
				}
			}

			return project;
		}
		else {
			System.out.println(TXMCoreMessages.bind(TXMCoreMessages.errorColonP0CorpusDirectoryIsNotConformantToTXM079CorpusBinaryFormatColonCorpusSkipped, binCorpusDir));
			System.out.println(TXMCoreMessages.bind(TXMCoreMessages.tXMNeedsFoldersColonP0P1P2AndP3, HTMLs, DATAs, REGISTRY, PARAMS));
		}
		return null;
	}


	@Override
	public boolean saveParameters() throws Exception {
		return true;
	}

	@Override
	public boolean loadParameters() throws Exception {
		return true;
	}

	@Override
	public String getName() {
		return getUserName();
	}

	@Override
	public boolean canCompute() throws Exception {
		return true;
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {
		// TODO should ensure its files are up to date
		return true;
	}

	@Override
	public String getResultType() {
		return "Workspace";
	}

	/**
	 * Loads and recreates the existing projects.
	 * 
	 * @throws CoreException
	 */
	public void loadProjectsFromProjectScopes(IProgressMonitor monitor) throws CoreException {

		Log.fine("Toolbox.initialize(): reloading projects...");

		IWorkspace rcpWorkspace = ResourcesPlugin.getWorkspace();
		rcpWorkspace.getRoot().refreshLocal(org.eclipse.core.resources.IResource.DEPTH_INFINITE, null);
		IProject corporaDirectory = rcpWorkspace.getRoot().getProject("corpora");

		if (!corporaDirectory.exists()) {
			Log.severe("Can't load corpora projects: 'corpora' not found in workspace.");
			return;
		}
		IProject projects[] = rcpWorkspace.getRoot().getProjects();

		Log.fine("Toolbox.initialize(): " + projects.length + " project(s) found in workspace.");
		if (monitor != null) monitor.setTaskName(NLS.bind(TXMCoreMessages.initializingCorporaP0P1, 0, projects.length));
		int c = 0;
		for (IProject project : projects) {
			c++;
			if (monitor != null) monitor.setTaskName(NLS.bind(TXMCoreMessages.initializingCorporaP0P1, c, projects.length));
			if (monitor != null) monitor.subTask(project.getName());
			if (project.getLocation() == null || !project.getLocation().toFile().exists()) {
				project.delete(true, null);
				continue;
			}

			// project.open(monitor);
			// String[] natures = project.getDescription().getNatureIds();
			// if (Arrays.binarySearch(natures, Project.TXMCORPUSNATURE) < 0) continue;
			if (project.isOpen()) {
				Project.loadProjectFromProjectScope(project); // !!!
			}
		}

		// force the initialization of CorpusBuilds
		for (TXMResult r : this.getChildren(Project.class)) {
			Project project = (Project) r;
			if (project.isOpen()) {
				for (CorpusBuild cb : project.getCorpora()) {
					try {
						if (!cb.compute(false)) {
							System.out.println("Deleting broken corpus: " + cb + " of " + cb.getProject());
							cb.delete();
						}
					}
					catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
}
