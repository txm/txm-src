package org.txm.objects;

/**
 * A simple Match implementation with 2 positions: start and end
 * 
 * @author mdecorde
 *
 */
public class Match2P extends Match {
	
	protected int start;
	
	protected int end;
	
	public Match2P(int start, int end) {
		this.start = start;
		this.end = end;
	}
	
	public Match2P(int position) {
		this(position, position);
	}

	@Override
	public int getStart() {
		return start;
	}
	
	@Override
	public int getEnd() {
		return end;
	}
	
	@Override
	public int getTarget() {
		return -1;
	}
	
	@Override
	public void setEnd(int p) {
		end = p;
	}
	
	@Override
	public void setStart(int p) {
		start = p;
	}
	
	@Override
	public void setTarget(int p) {}
}
