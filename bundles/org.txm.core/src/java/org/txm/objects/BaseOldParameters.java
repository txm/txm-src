package org.txm.objects;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.utils.logger.Log;
import org.txm.utils.xml.DomUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class BaseOldParameters implements IParameters {
	
	public static final DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd"); //$NON-NLS-1$
	
	public static final DateFormat shortUKDateformat = new SimpleDateFormat("dd-MM-yy"); //$NON-NLS-1$
	
	HashMap<String, Object> keyValueParameters = new HashMap<>();
	
	public static final String ANNOTATE = "annotate"; //$NON-NLS-1$
	
	public static final String CORPUS = "corpus"; //$NON-NLS-1$
	
	public static final String CURRENTVERSION = "0.8.0"; //$NON-NLS-1$
	
	public static final String DATE = "date"; //$NON-NLS-1$
	
	public static final String DEFAULT = "default"; //$NON-NLS-1$
	
	public static final String DESC = "desc"; //$NON-NLS-1$
	
	public static final String ENCODING = "encoding"; //$NON-NLS-1$
	
	public static final String LANG = "lang"; //$NON-NLS-1$
	
	public static final String LINK = "link"; //$NON-NLS-1$
	
	public static final String NAME = "name"; //$NON-NLS-1$
	
	public static final String SUBCORPUS = "subcorpus"; //$NON-NLS-1$
	
	public static final String PARTITION = "partition"; //$NON-NLS-1$
	
	public static final String PART = "part"; //$NON-NLS-1$
	
	public static final String PARAM = "param"; //$NON-NLS-1$
	
	public static final String QUERY = "query"; //$NON-NLS-1$
	
	public static final String TARGET = "target"; //$NON-NLS-1$
	
	public static final String TYPE = "BASE"; //$NON-NLS-1$
	
	public static final String PARAMETERS = "parameters"; //$NON-NLS-1$
	
	public static final String PARAMETERKEY = "key"; //$NON-NLS-1$
	
	public static final String PARAMETERVALUE = "value"; //$NON-NLS-1$
	
	public static final String TEXT = "text"; //$NON-NLS-1$
	
	public static final String TEXTS = "texts"; //$NON-NLS-1$
	
	public static final String EDITION = "edition"; //$NON-NLS-1$
	
	public static final String EDITIONS = "editions"; //$NON-NLS-1$
	
	public static final String UTF8 = "UTF-8"; //$NON-NLS-1$
	
	public static final String TRUE = "true"; //$NON-NLS-1$
	
	public static final String FALSE = "false"; //$NON-NLS-1$
	
	public static String VALUE = "value"; //$NON-NLS-1$
	
	public static void createEmptyParams(File paramFile, String basename) throws IOException {
		paramFile.getParentFile().mkdirs();
		FileOutputStream fwriter = new FileOutputStream(paramFile);
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fwriter, UTF8)); //$NON-NLS-1$
		
		String AUTHOR = System.getProperty("user.name"); //$NON-NLS-1$
		String DATE = dateformat.format(new Date());
		DATE = DATE.replace("/", "-"); //$NON-NLS-1$ //$NON-NLS-2$
		
		String description = "&lt;pre&gt;&lt;br/&gt;" + "- " + basename + "&lt;br/&gt;- " + AUTHOR + "&lt;br/&gt;- " + DATE + "&lt;/pre&gt;"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		String path = paramFile.getParentFile().getAbsolutePath();
		path = path.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll("\"", "&#34;"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
		writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"); //$NON-NLS-1$
		writer.write("<import version=\"" + CURRENTVERSION + "\" author=\"" + AUTHOR + "\" date=\"" + DATE + "\" desc=\"\" name=\"" + basename + "\" rootDir=\"" + path + "\" script=\"\">\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
		writer.write(" <linkGrp type=\"align\"></linkGrp>\n"); //$NON-NLS-1$
		writer.write(" <corpora>\n"); //$NON-NLS-1$
		writer.write("    <corpus desc=\"" + description + "\" cqpid=\"\" encoding=\"" + System.getProperty("file.encoding") + "\" lang=\"" + Locale.getDefault().getLanguage() //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				+ "\" font=\"\" annotate=\"true\" name=\"" + basename.toUpperCase() + "\">\n");  //$NON-NLS-1$ //$NON-NLS-2$
		writer.write("       <tokenizer onlyThoseTests=\"false\"></tokenizer>\n"); //$NON-NLS-1$
		writer.write("       <xslt xsl=\"\"></xslt>\n"); //$NON-NLS-1$
		writer.write("       <pattributes></pattributes>\n"); //$NON-NLS-1$
		writer.write("       <sattributes></sattributes>\n"); //$NON-NLS-1$
		writer.write("       <biblios></biblios>\n"); //$NON-NLS-1$
		writer.write("       <uis></uis>\n"); //$NON-NLS-1$
		writer.write("       <editions default=\"default\">\n"); //$NON-NLS-1$
		writer.write("       	<edition name=\"default\" mode=\"groovy\" page_break_tag=\"pb\" words_per_page=\"500\" build_edition=\"true\" script=\"pager.groovy\"/>\n"); //$NON-NLS-1$
		writer.write("       </editions>\n"); //$NON-NLS-1$
		writer.write("       <texts></texts>\n"); //$NON-NLS-1$
		writer.write("       <queries></queries>\n"); //$NON-NLS-1$
		writer.write("       <knowledgeRepositories></knowledgeRepositories>\n"); //$NON-NLS-1$
		writer.write("       <preBuild></preBuild>\n"); //$NON-NLS-1$
		writer.write("       <parameters></parameters>\n"); //$NON-NLS-1$
		writer.write("    </corpus>\n"); //$NON-NLS-1$
		writer.write(" </corpora>\n"); //$NON-NLS-1$
		writer.write("</import>\n"); //$NON-NLS-1$
		
		writer.close();
	}
	
	public static String getProjectName(File paramfile) throws ParserConfigurationException, SAXException, IOException {
		Document root = DomUtils.load(paramfile);
		return root.getDocumentElement().getAttribute(NAME); //$NON-NLS-1$
	}
	
	public static void main(String args[]) throws ParserConfigurationException, SAXException, IOException {
		String home = System.getProperty("user.home"); //$NON-NLS-1$
		File paramFile = new File(home, "TXM/corpora/xmltxmpara/param.xml"); //$NON-NLS-1$
		BaseOldParameters p = new BaseOldParameters(paramFile);
		p.load();
		System.out.println(p.toString());
	}
	
	/**
	 * fill the preBuild Element given a Corpus or subcorpus object and its XML Element 
	 * @param corpus
	 * @param corpusElem
	 * @param bp
	 */
	public static void writeSubcorpusandPartitionsInOldBaseParameters(CorpusBuild corpus, Element corpusElem, BaseOldParameters bp) {
		
		for (Partition p : corpus.getChildren(Partition.class)) {
			
			String[] queries = new String[p.getParts().size()];
			String[] names = new String[p.getParts().size()];
			int i = 0;
			for (CorpusBuild part : p.getParts()) {
				names[i] = part.getName();
				queries[i] = part.getStringParameterValue(TBXPreferences.QUERY);
				i++;
			}
			bp.addPartition(corpusElem, p.getSimpleName(), queries, names);
		}
		
		for (CorpusBuild s : corpus.getChildren(CorpusBuild.class)) {
			Element e = bp.addSubcorpus(corpusElem, s.getSimpleName(), s.getStringParameterValue(TBXPreferences.QUERY), "");
			
			writeSubcorpusandPartitionsInOldBaseParameters(s, e, bp);
		}
	}
	
	public HashMap<String, Element> getCorpora() {
		return corpora;
	}
	
	public String author;
	
	String AVOIDXMLSTEPSDEFAULT = FALSE;
	
	String CLEANTEMPDIRSDEFAULT = TRUE;
	
	public HashMap<String, Element> corpora = new HashMap<>();
	
	public Element corporaElement;
	
	public Date date;
	
	String DEFAULT_CQPLIMITS = TEXT; //$NON-NLS-1$
	
	boolean DEFAULT_DOEDITION = true;
	
	String DEFAULT_PAGEELEMENT = "pb"; //$NON-NLS-1$
	
	int DEFAULT_WORDSPERPAGE = 1000;
	
	public String description;
	
	String DOEDITIONSTEPSDEFAULT = TRUE;
	
	public HashMap<String, Element> links = new HashMap<>();
	
	public String name;
	
	public File paramFile;
	
	public Element root;
	
	public String rootDir;
	
	public String scriptFile;
	
	public String version;
	
	public BaseOldParameters(File paramFile) throws ParserConfigurationException, SAXException, IOException {
		this.paramFile = paramFile;
		this.root = DomUtils.load(paramFile).getDocumentElement();
	}
	
	public Element addCorpusNode(String name, String cqpid, String lang, String encoding, boolean annotate) {
		if (corporaElement == null)
			return null;
		Element corpusElem = root.getOwnerDocument().createElement(CORPUS);
		corporaElement.appendChild(corpusElem);
		
		corpusElem.setAttribute(NAME, name);
		corpusElem.setAttribute("cqpid", cqpid); //$NON-NLS-1$
		corpusElem.setAttribute(LANG, lang);
		corpusElem.setAttribute(ENCODING, encoding);
		corpusElem.setAttribute(ANNOTATE, "" + annotate); //$NON-NLS-1$
		
		corpusElem.appendChild(corpusElem.getOwnerDocument().createElement(TEXTS)); //$NON-NLS-1$
		corpusElem.appendChild(corpusElem.getOwnerDocument().createElement("queries")); //$NON-NLS-1$
		corpusElem.appendChild(corpusElem.getOwnerDocument().createElement("xslt")); //$NON-NLS-1$
		corpusElem.appendChild(corpusElem.getOwnerDocument().createElement("uis")); //$NON-NLS-1$
		corpusElem.appendChild(corpusElem.getOwnerDocument().createElement("preBuild")); //$NON-NLS-1$
		corpusElem.appendChild(corpusElem.getOwnerDocument().createElement("parameters")); //$NON-NLS-1$
		Element tokenizerElem = corpusElem.getOwnerDocument().createElement("tokenizer"); //$NON-NLS-1$
		tokenizerElem.setAttribute("onlyThoseTests", FALSE); //$NON-NLS-1$
		corpusElem.appendChild(tokenizerElem);
		corpusElem.appendChild(corpusElem.getOwnerDocument().createElement("biblios")); //$NON-NLS-1$
		corpusElem.appendChild(corpusElem.getOwnerDocument().createElement("pattributes")); //$NON-NLS-1$
		corpusElem.appendChild(corpusElem.getOwnerDocument().createElement("sattributes")); //$NON-NLS-1$
		Element editions = corpusElem.getOwnerDocument().createElement(EDITIONS); //$NON-NLS-1$
		editions.setAttribute(DEFAULT, DEFAULT);
		corpusElem.appendChild(editions);
		
		corpora.put(name, corpusElem);
		return corpusElem;
	}
	
	/**
	 * Add an edition (already declared in the 'editions' element to a text element
	 * 
	 * @param textElem
	 * @param name the edition name to add
	 * @param index the directory containing the edition pages
	 * @param type default is 'html'
	 * @return the created Element
	 */
	public Element addEdition(Element textElem, String name, String index, String type) {
		NodeList editionsList = textElem.getElementsByTagName(EDITIONS);//$NON-NLS-1$
		Element editions = null;
		if (editionsList.getLength() == 0) {
			editions = textElem.getOwnerDocument().createElement(EDITIONS);//$NON-NLS-1$
			textElem.appendChild(editions);
		}
		else {
			editions = (Element) editionsList.item(0);
		}
		
		Element editionElement = editions.getOwnerDocument().createElement(EDITION);//$NON-NLS-1$
		editionElement.setAttribute(NAME, name);
		editionElement.setAttribute("index", index);//$NON-NLS-1$
		editionElement.setAttribute("type", type);//$NON-NLS-1$
		editions.appendChild(editionElement);
		
		return editionElement;
	}
	
	/**
	 * 
	 * @param corpusElem the corpus Element. the new edition definition will be added to corpusElem/editions
	 * @param name
	 * @param mode groovy, xsl, etc.
	 * @param script the script path
	 * @return the created Element
	 */
	public Element addEditionDefinition(Element corpusElem, String name, String mode, String script) {
		Element editionsElement = getEditionsElement(corpusElem);
		Element editionElement = editionsElement.getOwnerDocument().createElement(EDITION);//$NON-NLS-1$
		editionElement.setAttribute(NAME, name);
		editionElement.setAttribute("build_edition", "" + DEFAULT_DOEDITION);//$NON-NLS-1$ //$NON-NLS-2$
		editionElement.setAttribute("page_break_tag", "" + DEFAULT_PAGEELEMENT);//$NON-NLS-1$ //$NON-NLS-2$
		editionElement.setAttribute("words_per_page", "" + DEFAULT_WORDSPERPAGE);//$NON-NLS-1$ //$NON-NLS-2$
		editionElement.setAttribute("mode", mode);//$NON-NLS-1$
		editionElement.setAttribute("script", script);//$NON-NLS-1$
		editionElement.setAttribute("type", "html");//$NON-NLS-1$ //$NON-NLS-2$
		editionElement.setAttribute("index", ".");//$NON-NLS-1$ //$NON-NLS-2$
		editionsElement.appendChild(editionElement);
		
		return editionElement;
	}
	
	public Element addPage(Element edition, String idx, String wordid) {
		Element page = edition.getOwnerDocument().createElement("page");//$NON-NLS-1$
		page.setAttribute("id", idx);//$NON-NLS-1$
		page.setAttribute("wordid", wordid);//$NON-NLS-1$
		edition.appendChild(page);
		return page;
	}
	
	public Element addPartition(Element corpusElem, String name, String[] queries, String[] names) {
		
		Element partitionElem = null;
		
		if (corpusElem.getNodeName().equals(SUBCORPUS)) { // if corpuselement is a subcorpus append the partition element to the subcorpus element children //$NON-NLS-1$
			partitionElem = corpusElem.getOwnerDocument().createElement(PARTITION);//$NON-NLS-1$
			corpusElem.appendChild(partitionElem);
		} else {
			Element preBuildElement = this.getPreBuildElement(corpusElem);
			partitionElem = preBuildElement.getOwnerDocument().createElement(PARTITION);//$NON-NLS-1$
			preBuildElement.appendChild(partitionElem);
		}
		
		
		partitionElem.setAttribute(NAME, name);
		for (int i = 0; i < queries.length; i++) {
			Element partElem = partitionElem.getOwnerDocument().createElement(PART);//$NON-NLS-1$
			partElem.setAttribute(NAME, names[i]);
			partElem.setAttribute(QUERY, queries[i]);
			partitionElem.appendChild(partElem);
		}
		
		
		return partitionElem;
	}
	
	public Element addPAttribute(Element corpusElem, String name, String shortname, String longname, String type, String renderer, String tooltip, String pattern, String importB, String mandatory,
			String inputFormat, String outputFormat) {
		NodeList pattributesList = corpusElem.getElementsByTagName("pattributes");//$NON-NLS-1$
		if (pattributesList.getLength() == 0)
			return null;
		
		Element pattributes = (Element) pattributesList.item(0);
		Element pattribute = pattributes.getOwnerDocument().createElement("pattribute");//$NON-NLS-1$
		pattribute.setAttribute("id", name);//$NON-NLS-1$
		pattribute.setAttribute("shortname", shortname);//$NON-NLS-1$
		pattribute.setAttribute("longname", longname);//$NON-NLS-1$
		pattribute.setAttribute("type", type);//$NON-NLS-1$
		pattribute.setAttribute("renderer", renderer);//$NON-NLS-1$
		pattribute.setAttribute("tooltip", tooltip);//$NON-NLS-1$
		pattribute.setAttribute("pattern", pattern);//$NON-NLS-1$
		pattribute.setAttribute("import", importB);//$NON-NLS-1$
		pattribute.setAttribute("mandatory", mandatory);//$NON-NLS-1$
		pattribute.setAttribute("inputFormat", inputFormat);//$NON-NLS-1$
		pattribute.setAttribute("outputFormat", outputFormat);//$NON-NLS-1$
		pattributes.appendChild(pattribute);
		
		return pattribute;
	}
	
	public Element addPreBuildCorpus(Element corpusElem, String name, String query, String exemple) {
		NodeList prebuildsList = corpusElem.getElementsByTagName("preBuild");//$NON-NLS-1$
		if (prebuildsList.getLength() == 0)
			return null;
		
		Element preBuild = (Element) prebuildsList.item(0);
		Element subcorpus = preBuild.getOwnerDocument().createElement(SUBCORPUS);//$NON-NLS-1$
		subcorpus.setAttribute(NAME, name);
		subcorpus.setAttribute(QUERY, query);
		
		preBuild.appendChild(subcorpus);
		return subcorpus;
	}
	
	public Element addQuery(Element corpusElem, String name, String query, String desc) {
		NodeList queriesList = corpusElem.getElementsByTagName("queries");//$NON-NLS-1$
		if (queriesList.getLength() == 0)
			return null;
		
		Element queries = (Element) queriesList.item(0);
		Element queryElem = queries.getOwnerDocument().createElement(QUERY);
		queryElem.setAttribute(NAME, name);
		queryElem.setAttribute(QUERY, query);
		queryElem.setAttribute(DESC, desc);
		
		queries.appendChild(queryElem);
		return queryElem;
	}
	
	public Element addSAttribute(Element corpusElem, String name, String shortname, String longname, String type, String renderer, String tooltip, String pattern, String importB, String mandatory,
			String inputFormat, String outputFormat) {
		NodeList sattributesList = corpusElem.getElementsByTagName("sattributes");//$NON-NLS-1$
		if (sattributesList.getLength() == 0) {
			return null;
		}
		
		Element sattributes = (Element) sattributesList.item(0);
		Element sattribute = sattributes.getOwnerDocument().createElement("sattribute");//$NON-NLS-1$
		sattribute.setAttribute("id", name);//$NON-NLS-1$
		sattribute.setAttribute("shortname", shortname);//$NON-NLS-1$
		sattribute.setAttribute("longname", longname);//$NON-NLS-1$
		sattribute.setAttribute("type", type);//$NON-NLS-1$
		sattribute.setAttribute("renderer", renderer);//$NON-NLS-1$
		sattribute.setAttribute("tooltip", tooltip);//$NON-NLS-1$
		sattribute.setAttribute("pattern", pattern);//$NON-NLS-1$
		sattribute.setAttribute("import", importB);//$NON-NLS-1$
		sattribute.setAttribute("mandatory", mandatory);//$NON-NLS-1$
		sattribute.setAttribute("inputFormat", inputFormat);//$NON-NLS-1$
		sattribute.setAttribute("outputFormat", outputFormat);//$NON-NLS-1$
		sattributes.appendChild(sattribute);
		
		return sattribute;
	}
	
	public Element addSubcorpus(Element corpusElem, String name, String query, String desc) {
		
		Element subcorpusElem = null;
		
		if (corpusElem.getNodeName().equals(SUBCORPUS)) { // if it's a subcorpus element, create the subcorpus as child //$NON-NLS-1$
			subcorpusElem = corpusElem.getOwnerDocument().createElement(SUBCORPUS);//$NON-NLS-1$
			corpusElem.appendChild(subcorpusElem);
		} else {
			Element preBuildElement = this.getPreBuildElement(corpusElem);
			subcorpusElem = preBuildElement.getOwnerDocument().createElement(SUBCORPUS);//$NON-NLS-1$
			preBuildElement.appendChild(subcorpusElem);
		}
		
		subcorpusElem.setAttribute(NAME, name);
		subcorpusElem.setAttribute(QUERY, query);
		subcorpusElem.setAttribute(DESC, desc);
		
		return subcorpusElem;
	}
	
	/**
	 * 
	 * @param corpusElem
	 * @param name the text ID
	 * @param sourceFile the XML-TXM file
	 * @return
	 */
	public Element addText(Element corpusElem, String name, File sourceFile) {
		NodeList textsList = corpusElem.getElementsByTagName(TEXTS);//$NON-NLS-1$
		Element texts = null;
		if (textsList.getLength() == 0) {
			texts = corpusElem.getOwnerDocument().createElement(TEXTS);//$NON-NLS-1$
			corpusElem.appendChild(texts);
		}
		else {
			texts = (Element) textsList.item(0);
		}
		
		Element text = texts.getOwnerDocument().createElement(TEXT);//$NON-NLS-1$
		text.setAttribute(NAME, name);
		texts.appendChild(text);
		
		Element source = texts.getOwnerDocument().createElement("source");//$NON-NLS-1$
		source.setAttribute("file", "" + sourceFile);//$NON-NLS-1$
		source.setAttribute("type", ".xml");//$NON-NLS-1$ //$NON-NLS-2$
		text.appendChild(source);
		text.appendChild(texts.getOwnerDocument().createElement(EDITIONS));//$NON-NLS-1$
		
		return text;
	}
	
	
	public Element getTextsElement(Element corpusElement) {
		NodeList textsList = corpusElement.getElementsByTagName(TEXTS);//$NON-NLS-1$
		if (textsList.getLength() == 0) {
			Element texts = corpusElement.getOwnerDocument().createElement(TEXTS);//$NON-NLS-1$
			corpusElement.appendChild(texts);
			return texts;
		}
		else {
			Element texts = (Element) textsList.item(0);
			return texts;
		}
	}
	
	public Element getParametersElement(Element corpusElement) {
		NodeList parametersList = corpusElement.getElementsByTagName(PARAMETERS);
		if (parametersList.getLength() == 0) {
			Element parametersElement = corpusElement.getOwnerDocument().createElement(PARAMETERS);
			corpusElement.appendChild(parametersElement);
			return parametersElement;
		}
		else {
			Element parametersElement = (Element) parametersList.item(0);
			return parametersElement;
		}
	}
	
	public Element getTextElement(Element textsElement, String name) {
		NodeList textsList = textsElement.getElementsByTagName(TEXT);//$NON-NLS-1$
		
		for (int i = 0; i < textsList.getLength(); i++) {
			Element text = (Element) textsList.item(i);
			if (name.equals(text.getAttribute(NAME))) return text;//$NON-NLS-1$
		}
		return null;
		
	}
	
	public Element getEditionElement(Element textElement, String name) {
		NodeList editionsList = textElement.getElementsByTagName(EDITION);//$NON-NLS-1$
		
		for (int i = 0; i < editionsList.getLength(); i++) {
			Element editionElement = (Element) editionsList.item(i);
			if (name.equals(editionElement.getAttribute(NAME))) {//$NON-NLS-1$
				return editionElement;
			}
		}
		// System.out.println("Warning no edition found with name="+name);
		return null;
		
	}
	
	public void addTokenizerParameter(Element corpusElem, String name, String value) {
		Element tokenizer = getTokenizerElement(corpusElem);
		// check if param already exists
		NodeList paramList = tokenizer.getElementsByTagName(PARAM);
		for (int i = 0; i < paramList.getLength(); i++) {
			String pname = ((Element) paramList.item(i)).getAttribute(NAME);
			if (name.equals(pname)) {
				((Element) paramList.item(i)).setTextContent(value);
				return;
			}
		}
		Element param = tokenizer.getOwnerDocument().createElement(PARAM);
		param.setAttribute(NAME, name);
		param.setTextContent(value);
		tokenizer.appendChild(param);
	}
	
	public void addTokenizerTest(Element corpusElem, String regex,
			String type, String before, String hit, String after) {
		Element tokenizer = getTokenizerElement(corpusElem);
		Element test = tokenizer.getOwnerDocument().createElement("test");//$NON-NLS-1$
		test.setAttribute(TYPE, type);
		test.setAttribute(VALUE, regex);
		test.setAttribute("before", before);//$NON-NLS-1$
		test.setAttribute("hit", hit);//$NON-NLS-1$
		test.setAttribute("after", after);//$NON-NLS-1$
		tokenizer.appendChild(test);
	}
	
	public void addXsltParameter(Element corpusElem, String name, String value) {
		Element xslt = getXsltElement(corpusElem);
		Element param = xslt.getOwnerDocument().createElement(PARAM);
		param.setAttribute(NAME, name);
		param.setAttribute(VALUE, value);
		xslt.appendChild(param);
	}
	
	public boolean getAvoidXMLSteps(Element corpusElem) {
		String str = corpusElem.getAttribute(CorpusBuild.AVOIDXMLSTEPS);
		if (str == null || str.length() == 0) str = AVOIDXMLSTEPSDEFAULT;
		return TRUE.equals(str);
	}
	
	public Element getBibliosElement(Element corpusElem) {
		return getElement(corpusElem, "biblios");//$NON-NLS-1$
	}
	
	public boolean getCleanTEMPDirectories(Element corpusElem) {
		String str = "" + this.getKeyValueParameters().get("clean.directories"); //$NON-NLS-1$ //$NON-NLS-2$
		if (str == null || str.length() == 0) str = CLEANTEMPDIRSDEFAULT;
		return TRUE.equals(str);
	}
	
	public Element getCorpusElement() {
		return corpora.get(getCorpusName());
	}
	
	/**
	 * 
	 * @return the first found corpus name
	 */
	public String getCorpusName() {
		if (corpora != null) {
			for (String c : corpora.keySet()) {
				return c;
			}
		}
		
		Log.warning(NLS.bind(TXMCoreMessages.warningNoCorpusTagFoundInTheP0FileBlehblehbleh, paramFile));
		return null;
	}
	
	public Element getCQLLimitElement(Element uiElement) {
		NodeList uiList = uiElement.getElementsByTagName("context_limits");//$NON-NLS-1$
		if (uiList.getLength() > 0) {
			return (Element) (uiList.item(0));
		}
		else {
			Element context_limitsElement = uiElement.getOwnerDocument().createElement("context_limits");//$NON-NLS-1$
			context_limitsElement.setAttribute("type", "list");//$NON-NLS-1$ //$NON-NLS-2$ $NON-NLS-2$
			context_limitsElement.setTextContent(DEFAULT_CQPLIMITS);
			uiElement.appendChild(context_limitsElement);
			return context_limitsElement;
		}
	}
	
	public Element getWordElement() {
		Element textualPlanEelement = getTextualPlans();
		NodeList uiList = textualPlanEelement.getElementsByTagName("word");//$NON-NLS-1$
		if (uiList.getLength() > 0) {
			return (Element) (uiList.item(0));
		}
		else {
			Element wordElement = textualPlanEelement.getOwnerDocument().createElement("word");//$NON-NLS-1$
			wordElement.setTextContent("w");//$NON-NLS-1$
			textualPlanEelement.appendChild(wordElement);
			return wordElement;
		}
	}
	
	public Element getMilestonesElement() {
		Element textualPlanEelement = getTextualPlans();
		NodeList uiList = textualPlanEelement.getElementsByTagName("milestones");//$NON-NLS-1$
		if (uiList.getLength() > 0) {
			return (Element) (uiList.item(0));
		}
		else {
			Element wordElement = textualPlanEelement.getOwnerDocument().createElement("milestones");//$NON-NLS-1$
			wordElement.setTextContent(""); //$NON-NLS-1$
			textualPlanEelement.appendChild(wordElement);
			return wordElement;
		}
	}
	
	public Element setWordElement(String tag) {
		if (tag == null) return null;
		tag = tag.trim();
		if (tag.length() == 0) return null;
		
		getWordElement().setTextContent(tag);
		return getWordElement();
	}
	
	public Element getOutSideTextTagsElement() {
		Element textualPlanEelement = getTextualPlans();
		NodeList uiList = textualPlanEelement.getElementsByTagName("outsidetexttags");//$NON-NLS-1$
		if (uiList.getLength() > 0) {
			return (Element) (uiList.item(0));
		}
		else {
			Element context_limitsElement = textualPlanEelement.getOwnerDocument().createElement("outsidetexttags");//$NON-NLS-1$
			context_limitsElement.setAttribute("type", "list");//$NON-NLS-1$ //$NON-NLS-2$ $NON-NLS-2$
			context_limitsElement.setTextContent("");//$NON-NLS-1$
			textualPlanEelement.appendChild(context_limitsElement);
			return context_limitsElement;
		}
	}
	
	public Element getOutSideTextTagsAndKeepContentElement() {
		Element textualPlanEelement = getTextualPlans();
		NodeList uiList = textualPlanEelement.getElementsByTagName("outside_text_tags_and_keep_content");//$NON-NLS-1$
		if (uiList.getLength() > 0) {
			return (Element) (uiList.item(0));
		}
		else {
			Element element = textualPlanEelement.getOwnerDocument().createElement("outside_text_tags_and_keep_content");//$NON-NLS-1$
			element.setAttribute("type", "list");//$NON-NLS-1$ //$NON-NLS-2$ $NON-NLS-2$
			element.setTextContent("");//$NON-NLS-1$
			textualPlanEelement.appendChild(element);
			return element;
		}
	}
	
	public Element getNoteElement() {
		Element textualPlanElement = getTextualPlans();
		NodeList noteList = textualPlanElement.getElementsByTagName("note");//$NON-NLS-1$
		if (noteList.getLength() > 0) {
			return (Element) (noteList.item(0));
		}
		else {
			Element element = textualPlanElement.getOwnerDocument().createElement("note");//$NON-NLS-1$
			element.setAttribute("type", "list");//$NON-NLS-1$ //$NON-NLS-2$
			element.setTextContent("");//$NON-NLS-1$
			textualPlanElement.appendChild(element);
			return element;
		}
	}
	
	public boolean getDoEdition(String edition) {
		Element corpusElement = getCorpusElement();
		if (corpusElement == null) return DEFAULT_DOEDITION;
		Element editionsElement = getEditionsElement(corpusElement);
		if (editionsElement == null) return DEFAULT_DOEDITION;
		Element editionElement = getEditionDefinitionElement(editionsElement, edition);
		if (editionElement == null) return DEFAULT_DOEDITION;
		
		String value = editionElement.getAttribute("build_edition");//$NON-NLS-1$
		if (value == null | value.length() == 0) return DEFAULT_DOEDITION;
		
		return TRUE.equals(value);
	}
	
	/**
	 * 
	 * @param editionsElem
	 * @param name
	 * @return the edition Element corresponding to the asked edition name
	 */
	public Element getEditionDefinitionElement(Element editionsElem, String name) {
		NodeList editionList = editionsElem.getElementsByTagName(EDITION);//$NON-NLS-1$
		for (int i = 0; i < editionList.getLength(); i++) {
			Element elem = (Element) editionList.item(i);
			if (name.equals(elem.getAttribute(NAME)))//$NON-NLS-1$
				return elem;
		}
		//
		// Element editionElement = editionsElem.getOwnerDocument().createElement(EDITION);
		// editionElement.setAttribute(NAME, name);
		// editionElement.setAttribute("build_edition", ""+DEFAULT_DOEDITION);
		// editionElement.setAttribute("page_break_tag", ""+DEFAULT_PAGEELEMENT);
		// editionElement.setAttribute("words_per_page", ""+DEFAULT_WORDSPERPAGE);
		// editionElement.setAttribute("mode", "groovy");
		// editionElement.setAttribute("script", "pager.groovy");
		// editionElement.setAttribute("type", "html");
		// editionElement.setAttribute("index", ".");
		return null;
	}
	
	/**
	 * returns the corpus "editions" element and creates it if missing
	 * 
	 * @param corpusElem
	 * @return the element that list the declared editions in a corpus
	 */
	public Element getEditionsElement(Element corpusElem) {
		Element elem = getElement(corpusElem, EDITIONS);//$NON-NLS-1$
		if (elem == null) {
			elem = corpusElem.getOwnerDocument().createElement(EDITIONS);//$NON-NLS-1$
		}
		if (!elem.hasAttribute(DEFAULT))
			elem.setAttribute(DEFAULT, DEFAULT);
		return elem;
	}
	
	/**
	 * Utility to get the first 'name' element from a node 'elem'
	 * 
	 * @param elem
	 * @param tag
	 * @return
	 */
	public Element getElement(Element elem, String tag) {
		NodeList editionsList = elem.getElementsByTagName(tag);
		if (editionsList.getLength() > 0)
			return (Element) editionsList.item(0);
		
		Element editionsElem = elem.getOwnerDocument().createElement(tag);
		elem.appendChild(editionsElem);
		return editionsElem;
	}
	
	public ArrayList<String> getExcludeXpaths() {
		Element filter = getFilterElement();
		ArrayList<String> xpaths = new ArrayList<>();
		NodeList excludeList = filter.getElementsByTagName("exclude");//$NON-NLS-1$
		for (int i = 0; i < excludeList.getLength(); i++) {
			Element exclude = (Element) excludeList.item(i);
			if ("xpath".equals(exclude.getAttribute("type")))//$NON-NLS-1$ //$NON-NLS-2$ $NON-NLS-2$
				xpaths.add(exclude.getTextContent().trim());
		}
		return xpaths;
	}
	
	public Element getFilterElement() {
		Element corpusElem = corpora.get(getCorpusName());
		return getElement(corpusElem, "xmlfilter");//$NON-NLS-1$
	}
	
	public String getLang(Element corpusElem) {
		String str = corpusElem.getAttribute(LANG);
		if (str == null) str = System.getProperty("user.locale");//$NON-NLS-1$
		return str;
	}
	
	public String getPageElement(String edition) {
		Element corpusElement = getCorpusElement();
		if (corpusElement == null) return DEFAULT_PAGEELEMENT;
		Element editionsElement = getEditionsElement(corpusElement);
		if (editionsElement == null) return DEFAULT_PAGEELEMENT;
		Element editionElement = getEditionDefinitionElement(editionsElement, edition);
		if (editionElement == null) return DEFAULT_PAGEELEMENT;
		
		String value = editionElement.getAttribute("page_break_tag");//$NON-NLS-1$
		if (value == null || value.length() == 0) return DEFAULT_PAGEELEMENT;
		
		return value;
	}
	
	public NodeList getPAttributes(Element corpusElem) {
		NodeList pattributesList = corpusElem.getElementsByTagName("pattributes");//$NON-NLS-1$
		if (pattributesList.getLength() == 0)
			return null;
		return ((Element) pattributesList.item(0)).getElementsByTagName("pattribute");//$NON-NLS-1$
	}
	
	public Element getPAttributesElement(Element corpusElem) {
		return getElement(corpusElem, "pattributes");//$NON-NLS-1$
	}
	
	public Element getPreBuildElement() {
		Element corpusElem = corpora.get(getCorpusName());
		return getElement(corpusElem, "preBuild");//$NON-NLS-1$
	}
	
	public Element getPreBuildElement(Element corpusElem) {
		return getElement(corpusElem, "preBuild");//$NON-NLS-1$
	}
	
	public Element getQueriesElement(Element corpusElem) {
		return getElement(corpusElem, "queries");//$NON-NLS-1$
	}
	
	public Element getSAttributesElement(Element corpusElem) {
		return getElement(corpusElem, "sattributes");//$NON-NLS-1$
	}
	
	public Element getTokenizerElement(Element corpusElem) {
		return getElement(corpusElem, "tokenizer");//$NON-NLS-1$
	}
	
	public Element getUIElement(Element corpusElem, String command) {
		Element uisElement = getUIsElement(corpusElem);
		Element uiElement = null;
		NodeList uiList = uisElement.getElementsByTagName("ui");//$NON-NLS-1$
		if (uiList.getLength() > 0) {
			for (int i = 0; i < uiList.getLength(); i++) {
				Element ui = (Element) (uiList.item(i));
				if (command.equals(ui.getAttribute("command"))) {//$NON-NLS-1$
					return ui;
				}
			}
		}
		if (uiElement == null) {
			uiElement = uisElement.getOwnerDocument().createElement("ui");//$NON-NLS-1$
			uiElement.setAttribute("command", command);//$NON-NLS-1$
			uisElement.appendChild(uiElement);
		}
		return uiElement;
	}
	
	public Element getUIsElement(Element corpusElem) {
		return getElement(corpusElem, "uis"); //$NON-NLS-1$
	}
	
	public int getWordsPerPage(String edition) {
		Element corpusElement = getCorpusElement();
		if (corpusElement == null) return DEFAULT_WORDSPERPAGE;
		Element editionsElement = getEditionsElement(corpusElement);
		if (editionsElement == null) return DEFAULT_WORDSPERPAGE;
		Element editionElement = getEditionDefinitionElement(editionsElement, edition);
		if (editionElement == null) return DEFAULT_WORDSPERPAGE;
		
		String value = editionElement.getAttribute("words_per_page");//$NON-NLS-1$
		if (value == null) return DEFAULT_WORDSPERPAGE;
		
		try {
			return Integer.parseInt(value);
		}
		catch (NumberFormatException e) {
			Log.fine("words_per_page value is not an Integer value: " + value);//$NON-NLS-1$
			return DEFAULT_WORDSPERPAGE;
		}
	}
	
	public Element getXsltElement(Element corpusElem) {
		Element elem = getElement(corpusElem, "xslt");//$NON-NLS-1$
		if (!elem.hasAttribute("xsl"))//$NON-NLS-1$
			elem.setAttribute("xsl", "");//$NON-NLS-1$ //$NON-NLS-2$
		return elem;
	}
	
	public HashMap<String, String> getXsltParams(Element corpusElem) {
		Element elem = getElement(corpusElem, "xslt");//$NON-NLS-1$
		if (!elem.hasAttribute("xsl"))//$NON-NLS-1$
			elem.setAttribute("xsl", "");//$NON-NLS-1$ //$NON-NLS-2$
		HashMap<String, String> hash = new HashMap<>();
		NodeList params = elem.getElementsByTagName("param");//$NON-NLS-1$
		for (int i = 0; i < params.getLength(); i++) {
			Element paramElem = (Element) params.item(i);
			String name = paramElem.getAttribute(NAME);//$NON-NLS-1$
			String value = paramElem.getAttribute("value");//$NON-NLS-1$
			if (name != null && value != null && name.length() > 0) {
				hash.put(name, value);
			}
		}
		return hash;
	}
	
	public void loadKeyValueParameters(Element parametersElem) {
		// HashMap<String, String> hash = new HashMap<String, String>();
		NodeList params = parametersElem.getElementsByTagName("param");//$NON-NLS-1$
		for (int i = 0; i < params.getLength(); i++) {
			Element paramElem = (Element) params.item(i);
			String name = paramElem.getAttribute(PARAMETERKEY);
			String value = paramElem.getAttribute(PARAMETERVALUE);
			if (name != null && value != null && name.length() > 0) {
				keyValueParameters.put(name, value);
			}
		}
	}
	
	public void saveKeyValueParameters(Element parametersElem) {
		// HashMap<String, String> hash = new HashMap<String, String>();
		
		NodeList params = parametersElem.getElementsByTagName("param");//$NON-NLS-1$
		HashSet<String> existingNames = new HashSet<>();
		for (int i = 0; i < params.getLength(); i++) {
			Element paramElem = (Element) params.item(i);
			String name = paramElem.getAttribute(PARAMETERKEY);
			existingNames.add(name);
			if (keyValueParameters.containsKey(name)) { // update existing element
				String value = keyValueParameters.get(name).toString();
				paramElem.setAttribute(PARAMETERVALUE, value);
			}
			else { // the parameter has been removed -> delete element
				parametersElem.removeChild(paramElem);
			}
		}
		
		for (String name : keyValueParameters.keySet()) {
			if (!existingNames.contains(name)) { // create the element
				Element paramElem = parametersElem.getOwnerDocument().createElement("param");//$NON-NLS-1$
				paramElem.setAttribute(PARAMETERKEY, name);
				paramElem.setAttribute(PARAMETERVALUE, keyValueParameters.get(name).toString());
				parametersElem.appendChild(paramElem);
			}
		}
	}
	
	
	public HashMap<String, Object> getKeyValueParameters() {
		return keyValueParameters;
	}
	
	/**
	 * Load import infos.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean load() {
		if (root == null) return false;
		// import general infos
		try {
			if (root.getAttribute(DATE).matches("....-..-..")) {//$NON-NLS-1$
				this.date = dateformat.parse(root.getAttribute(DATE));
			}
			else { // try with DateFormat.getDateInstance(DateFormat.SHORT, Locale.UK)
				this.date = shortUKDateformat.parse(root.getAttribute(DATE));
				// System.out.println("Warning in "+paramFile+": date format '"+root.getAttribute(DATE)+"' does not match '....-..-..', date is set to "+this.date);
			}
		}
		catch (ParseException e) {
			this.date = new Date(0);
			System.out.println("Warning: can't parse date '" + root.getAttribute(DATE) + "' in file " + paramFile + ": " + e);//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		this.version = root.getAttribute("version");//$NON-NLS-1$
		this.name = root.getAttribute(NAME).toUpperCase();
		root.setAttribute(NAME, name);// force UPPERCASED NAME
		this.author = root.getAttribute("author");//$NON-NLS-1$
		this.description = root.getAttribute(DESC);
		this.rootDir = root.getAttribute("rootDir");//$NON-NLS-1$
		this.scriptFile = root.getAttribute("script");//$NON-NLS-1$
		
		// get Alignements
		NodeList alignNodes = root.getElementsByTagName("linkGrp"); //$NON-NLS-1$
		if (alignNodes.getLength() > 0) {
			Element alignNode = (Element) alignNodes.item(0);
			NodeList linkNodes = alignNode.getElementsByTagName(LINK);
			for (int i = 0; i < linkNodes.getLength(); i++) {
				Element link = (Element) linkNodes.item(i);
				String target = link.getAttribute(TARGET);
				String[] split = target.split("#");//$NON-NLS-1$
				if (split.length != 3) {
					System.out.println("Error: linkGrp/link/@target malformed: " + target);//$NON-NLS-1$
					continue;
				}
				String from = split[1].trim();
				String to = split[2].trim();
				
				links.put("#" + to + " #" + from, link);//$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		
		// corpora
		NodeList corporaNodes = root.getElementsByTagName("corpora");//$NON-NLS-1$
		if (corporaNodes.getLength() > 0) {
			corporaElement = (Element) corporaNodes.item(0);
			NodeList corpusNodes = corporaElement.getElementsByTagName(CORPUS);
			for (int i = 0; i < corpusNodes.getLength(); i++) {
				
				Element corpusElement = (Element) corpusNodes.item(i);
				String id = corpusElement.getAttribute(NAME);
				corpora.put(id, corpusElement);
				
				// replace the properties file of some import module
				Element parametersElement = getParametersElement(corpusElement);
				loadKeyValueParameters(parametersElement);
			}
		}
		
		return true;
	}
	
	public boolean save() {
		return save(this.paramFile);
	}
	
	public boolean save(File paramFile) {
		try {
			this.saveToElement(); // save informations
			this.saveKeyValueParameters(getParametersElement(getCorpusElement())); // save additional parameters
			return DomUtils.save(root.getOwnerDocument(), paramFile);
		}
		catch (Exception e) {
			System.out.println("Fail to save parameters in " + paramFile + ". Reason: " + e);//$NON-NLS-1$ //$NON-NLS-2$
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return false;
	}
	
	public void saveToElement() {
		root.setAttribute(NAME, name);
		root.setAttribute(DESC, description);
		root.setAttribute("script", scriptFile);//$NON-NLS-1$
		root.setAttribute("rootDir", rootDir);//$NON-NLS-1$
		root.setAttribute("version", version);//$NON-NLS-1$
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BaseOldParameters [name=" + name + ", date=" + date + ", author=" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ author + ", version=" + version + ", description=" //$NON-NLS-1$ //$NON-NLS-2$
				+ description + ",\n links=" + links + ", corpora=" + corpora //$NON-NLS-1$ //$NON-NLS-2$
				+ ",\n root=" + root + ", corporaElement=" + corporaElement + ", keyValueParameters=" + super.toString() + "]"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	}
	
	public String getCQPLimits() {
		Element concordanceElement = this.getUIElement(getCorpusElement(), "concordance");//$NON-NLS-1$
		if (concordanceElement != null) {
			Element context_limitsElement = this.getCQLLimitElement(concordanceElement);
			if (context_limitsElement != null) {
				String value = context_limitsElement.getTextContent();
				if (value == null || value.length() == 0) return DEFAULT_CQPLIMITS;
				return value;
			}
		}
		return DEFAULT_CQPLIMITS;
	}
	
	public Element getTextualPlans() {
		Element uiElement = this.getUIElement(this.getCorpusElement(), "textualplans");//$NON-NLS-1$
		if (uiElement == null) {
			Element uisElement = getUIsElement(this.getCorpusElement());
			uiElement = uisElement.getOwnerDocument().createElement("ui");//$NON-NLS-1$
			uiElement.setAttribute("command", "textualplans");//$NON-NLS-1$ //$NON-NLS-2$
			uisElement.appendChild(uiElement);
		}
		return uiElement;
	}
	
	/**
	 * 
	 * @return the images directory to use to build a facs Edition
	 */
	public String getFacsEditionImageDirectory() {
		Element corpusElement = getCorpusElement();
		if (corpusElement == null) return "";//$NON-NLS-1$
		Element editionsElement = getEditionsElement(corpusElement);
		if (editionsElement == null) return ""; //$NON-NLS-1$
		Element editionElement = getEditionDefinitionElement(editionsElement, "facs"); //$NON-NLS-1$
		if (editionElement == null) return ""; //$NON-NLS-1$
		
		String value = editionElement.getAttribute("images_directory");//$NON-NLS-1$
		if (value == null || value.length() == 0) return ""; //$NON-NLS-1$
		
		return value;
	}
	
	public boolean getDoFacsEdition() {
		Element corpusElement = getCorpusElement();
		if (corpusElement == null) return false;
		Element editionsElement = getEditionsElement(corpusElement);
		if (editionsElement == null) return false;
		Element editionElement = getEditionDefinitionElement(editionsElement, "facs"); //$NON-NLS-1$
		if (editionElement == null) return false;
		
		String value = editionElement.getAttribute("build_edition"); //$NON-NLS-1$
		if (value == null | value.length() == 0) return DEFAULT_DOEDITION;
		
		return TRUE.equals(value);
	}
	
	public boolean getSkipTokenization() {
		return "true".equals(getTokenizerElement(getCorpusElement()).getAttribute("skip")); // $NON-NLS-1$ $NON-NLS-2$ //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	public void setSkipTokenization(boolean skip) {
		getTokenizerElement(getCorpusElement()).setAttribute("skip", Boolean.toString(skip));//$NON-NLS-1$
	}
	
	public void setDoAnnotation(boolean annotate) {
		getCorpusElement().setAttribute("annotate", Boolean.toString(annotate)); //$NON-NLS-1$
	}
	
	public void setAnnotationLang(String lang) {
		getCorpusElement().setAttribute("lang", lang); //$NON-NLS-1$
	}
	
	public void setTextualPlans(String outsideTextElementsToEdit, String noteElements, String outsideTextElements, String milestoneElements) {
		Element textualplansElement = getTextualPlans();
		if (textualplansElement != null) {
			Element outsideTextElement = getOutSideTextTagsElement();
			outsideTextElement.setAttribute("type", "list"); //$NON-NLS-1$ //$NON-NLS-2$
			outsideTextElement.setTextContent(outsideTextElements);
			
			Element noteElement = getNoteElement();
			noteElement.setAttribute("type", "list"); //$NON-NLS-1$ //$NON-NLS-2$
			noteElement.setTextContent(noteElements);
			
			Element outSideTextTagsAndKeepContentElement = getOutSideTextTagsAndKeepContentElement();
			outSideTextTagsAndKeepContentElement.setAttribute("type", "list"); //$NON-NLS-1$ //$NON-NLS-2$
			outSideTextTagsAndKeepContentElement.setTextContent(outsideTextElementsToEdit);
			
			Element milestonesElement = getMilestonesElement();
			milestonesElement.setTextContent(milestoneElements.trim());
		}
		else {
			System.out.println("Error: Textual plan parameters not saved."); //$NON-NLS-1$
		}
	}
	
	public void setWordsPerPage(int n) {
		Element editionsElement = this.getEditionsElement(this.getCorpusElement());
		if (editionsElement != null) {
			Element defaultEditionElement = this.getEditionDefinitionElement(editionsElement, "default"); //$NON-NLS-1$
			
			if (defaultEditionElement != null) {
				defaultEditionElement.setAttribute("words_per_page", Integer.toString(n)); //$NON-NLS-1$
			}
		}
	}
	
	public void setPageBreakElement(String pageBreak) {
		Element editionsElement = this.getEditionsElement(this.getCorpusElement());
		if (editionsElement != null) {
			Element defaultEditionElement = this.getEditionDefinitionElement(editionsElement, "default"); //$NON-NLS-1$
			
			if (defaultEditionElement != null) {
				defaultEditionElement.setAttribute("page_break_tag", pageBreak); //$NON-NLS-1$
			}
		}
	}
	
	public void initializeProject(Project project) {
		EditionDefinition defaultEditionDef = project.getEditionDefinition("default"); //$NON-NLS-1$
		defaultEditionDef.setBuildEdition(this.getDoEdition("default")); //$NON-NLS-1$
		defaultEditionDef.setWordsPerPage(this.getWordsPerPage("default")); //$NON-NLS-1$
		defaultEditionDef.setPageBreakTag(this.getPageElement("default")); //$NON-NLS-1$
		
		project.setTextualPlan("MileStones", this.getMilestonesElement().getTextContent()); //$NON-NLS-1$
		project.setTextualPlan("OutSideTextTags", this.getOutSideTextTagsElement().getTextContent()); //$NON-NLS-1$
		project.setTextualPlan("OutSideTextTagsAndKeepContent", this.getOutSideTextTagsAndKeepContentElement().getTextContent()); //$NON-NLS-1$
		project.setTextualPlan("Note", this.getNoteElement().getTextContent()); //$NON-NLS-1$
		
		EditionDefinition facsEditionDef = project.getEditionDefinition("facs"); //$NON-NLS-1$
		facsEditionDef.setBuildEdition(this.getDoEdition("facs")); //$NON-NLS-1$
		facsEditionDef.setWordsPerPage(this.getWordsPerPage("facs")); //$NON-NLS-1$
		facsEditionDef.setPageBreakTag(this.getPageElement("facs")); //$NON-NLS-1$
		facsEditionDef.setImagesDirectory(this.getFacsEditionImageDirectory());
		
		if (this.getCorpusElement().getAttribute("font") != null) { //$NON-NLS-1$
			project.setFont(this.getCorpusElement().getAttribute("font")); //$NON-NLS-1$
		}
		if (this.getCorpusElement().getAttribute("lang") != null) { //$NON-NLS-1$
			project.setLang(this.getCorpusElement().getAttribute("lang")); //$NON-NLS-1$
		}
		if (this.getCorpusElement().getAttribute("encoding") != null) { //$NON-NLS-1$
			project.setEncoding(this.getCorpusElement().getAttribute("encoding")); //$NON-NLS-1$
		}
		
		if (this.getCQPLimits() != null) {
			project.getCommandPreferences("concordance").set("context_limits_type", "list"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ $NON-NLS-2$
			project.getCommandPreferences("concordance").set("context_limits", this.getCQPLimits()); //$NON-NLS-1$ //$NON-NLS-2$ $NON-NLS-2$
		}
		
		NodeList nodes = this.getTokenizerElement(this.getCorpusElement()).getElementsByTagName("param"); //$NON-NLS-1$
		for (int i = 0; i < nodes.getLength(); i++) {
			Element paramElement = (Element) nodes.item(i);
			project.addTokenizerParameter(paramElement.getAttribute(NAME), paramElement.getTextContent()); //$NON-NLS-1$
		}
		project.addTokenizerParameter("word_tags", this.getWordElement().getTextContent()); //$NON-NLS-1$
		project.addTokenizerParameter("onlyThoseTests", "false"); //$NON-NLS-1$ //$NON-NLS-2$ $NON-NLS-2$
		project.addTokenizerParameter("doTokenizeStep", Boolean.toString(!this.getSkipTokenization())); //$NON-NLS-1$
		
		project.setXslt(this.getXsltElement(this.getCorpusElement()).getAttribute("xsl")); //$NON-NLS-1$
		project.setAnnotate("true".equals(this.getCorpusElement().getAttribute("annotate"))); //$NON-NLS-1$ //$NON-NLS-2$ $NON-NLS-2$
		project.setCleanAfterBuild(this.getCleanTEMPDirectories(this.getCorpusElement()));
		project.setImportModuleName(scriptFile);
	}
}
