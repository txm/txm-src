// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-10-27 16:30:30 +0200 (Thu, 27 Oct 2016) $
// $LastChangedRevision: 3331 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.objects;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.metadatas.DateMetadata;
import org.txm.metadatas.Metadata;
import org.txm.metadatas.MetadataGroup;
import org.txm.metadatas.MinMaxDateGroup;
import org.txm.utils.logger.Log;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * The abstract Corpus class.
 */
public abstract class CorpusBuild extends TXMResult {
	
	@Parameter(key = TBXPreferences.ID)
	protected String pID;
	
	public static final String TEXT = "text"; //$NON-NLS-1$
	
	public static final String TEXTS = "texts"; //$NON-NLS-1$
	
	public static final String BIBLIO = "biblio"; //$NON-NLS-1$
	
	public static final String PDF = "pdf"; //$NON-NLS-1$
	
	public static final String SOURCE = "source"; //$NON-NLS-1$
	
	public static final String TYPE = "type"; //$NON-NLS-1$
	
	public static final String FILE = "file"; //$NON-NLS-1$
	
	public static final String EDITIONS = "editions"; //$NON-NLS-1$
	
	public static final String EDITION = "edition"; //$NON-NLS-1$
	
	public static final String NAME = "name"; //$NON-NLS-1$
	
	public static final String LANG = "lang"; //$NON-NLS-1$
	
	public static final String WORDSPERPAGE = "wordsperpage"; //$NON-NLS-1$
	
	public static final String DOEDITIONSTEP = "doeditionstep"; //$NON-NLS-1$
	
	public static final String AVOIDXMLSTEPS = "avoidxmlsteps"; //$NON-NLS-1$
	
	public static final String CLEANTEMPDIRS = "cleantempdirectories"; //$NON-NLS-1$
	
	public static final String CORPUS = "corpus"; //$NON-NLS-1$
	
	public static final String CORPORA = "corpora"; //$NON-NLS-1$
	
	public static final String ID = "id"; //$NON-NLS-1$
	
	public static final String DEFAULT = "default"; //$NON-NLS-1$
	
	/** The importmetadatas. */
	public ArrayList<Metadata> importmetadatas = new ArrayList<>();
	
	public ArrayList<MetadataGroup> groupedMetadatas = new ArrayList<>();
	
	/** The p attribute info names. */
	private static String[] attributeInfoNames = { ID, "shortname", "longname", TYPE, "renderer", "tooltip", "pattern", "import", "mandatory", "inputFormat", "outputFormat" }; //$NON-NLS-1$
	
	private HashMap<String, HashMap<String, String>> pPAttributesInformations = new HashMap<>();
	
	private HashMap<String, HashMap<String, String>> pSAttributesInformations = new HashMap<>();
	
	private ArrayList<String> refProperties = new ArrayList<>();
	
	private ArrayList<String> sortProperties = new ArrayList<>();
	
	private ArrayList<String> viewProperties = new ArrayList<>();
	
	private HashMap<String, Alignement> links = new HashMap<>();
	
	@Parameter(key = TBXPreferences.DESCRIPTION)
	private String pDescription;
	
	// The default properties to use
	@Parameter(key = TBXPreferences.REF_PROPERTY)
	private String pDefaultRefProperty;
	
	@Parameter(key = TBXPreferences.SORT_PROPERTY)
	private String pDefaultSortProperty;
	
	@Parameter(key = TBXPreferences.VIEW_PROPERTY)
	private String pDefaultViewProperty;
	
	private int pageSize;
	
	private boolean doEditionStep;
	
	private boolean avoidXMLSteps;
	
	private boolean cleanTempDirs;
	
	protected String cql_limit_query = "<text>[]"; // read only
	
	protected int conc_left_context = 8; // read only
	
	protected int conc_right_context = 12; // read only
	
	/**
	 * Instantiates a new root corpus.
	 *
	 * @param project the parent project
	 */
	public CorpusBuild(Project project) {
		super(project);
		
		// this.name = name;
		// this.persistable = true;
		// this.userPersistable = false;
		// if(base != null && c != null) {
		// this.project = project;
		// this.setPath(base.getPath() + getName());
		// setSelfElement(c);
		// _load();
		// }
	}
	
	@Override
	public boolean isInternalPersistable() {
		return true;
	}
	
	/**
	 * Instantiates a new sub corpus
	 * 
	 * @param corpus2 the parent corpus
	 */
	public CorpusBuild(CorpusBuild corpus2) {
		super(corpus2);
	}
	
	/**
	 * Instantiates a new sub corpus
	 * 
	 * @param partition the parent partition
	 */
	public CorpusBuild(Partition partition) {
		super(partition);
	}
	
	/**
	 * 
	 * @param parametersNodePath
	 */
	public CorpusBuild(String parametersNodePath) {
		super(parametersNodePath);
	}
	
	public CorpusBuild(TXMResult result) {
		
		super(result);
	}

	public String getID() {
		return pID;
	}
	
	/**
	 * Gets the base directory.
	 *
	 * @return the base directory
	 */
	public File getProjectDirectory() {
		if (getProject() != null) {
			return getProject().getProjectDirectory();
		}
		return null;
	}
	
	public String getCQLLimitQuery() {
		return cql_limit_query;
	}
	
	public int getDefaultConcordanceLeftContextSize() {
		return conc_left_context;
	}
	
	public int getDefaultConcordanceRightContextSize() {
		return conc_right_context;
	}
	
	/**
	 * Gets the queries log.
	 *
	 * @return the queries log
	 */
	@SuppressWarnings("unchecked")
	public List<SavedQuery> getQueriesLog() {
		return getChildren(SavedQuery.class);
	}
	
	// /**
	// * Adds Texts from a directory of sources.
	// *
	// * @param directory the directory
	// * @return true, if successful
	// */
	// public boolean addDirectory(File directory) {
	// FileFilter filter = new FileFilter() {
	// @Override
	// public boolean accept(File pathname) {
	// return !pathname.isHidden();
	// }
	// };
	// if (directory.isDirectory() && directory.exists()) {
	// File[] files = directory.listFiles(filter);
	// Arrays.sort(files);
	// for (File f : files) {
	// Text t = new Text(this.getProject());
	// t.setSourceFile(f);
	// }
	// } else {
	// System.out.println(directory + " is not a directory.");
	// return false;
	// }
	// return true;
	// }
	
	/**
	 * 
	 * @return the corpus language. Override this method to set another lang. eg for parallel corpus
	 */
	public String getLang() {
		return getProject().getLang();
	}
	
	// /**
	// * Adds the text.
	// *
	// * @param file the file
	// * @return the text
	// */
	// public Text addText(File file) {
	// if (texts.containsKey(file.getName()))
	// return texts.get(file.getName());
	//
	// String filename = file.getName();
	// int idx = filename.lastIndexOf(".");
	// String txtname = filename;
	// if(idx > 0)
	// txtname = filename.substring(0, idx);
	//
	// Document doc = getSelfElement().getOwnerDocument();
	// Element textRoot = (Element) getSelfElement().getElementsByTagName(TEXTS).item(0); //$NON-NLS-1$
	// Element textElem = doc.createElement(TEXT);
	// textElem.setAttribute(NAME, txtname);
	// textElem.setAttribute(BIBLIO, ""); //$NON-NLS-1$
	// textElem.setAttribute(PDF, ""); //$NON-NLS-1$
	// Element src = doc.createElement(SOURCE);
	// src.setAttribute(TYPE, filename.substring(idx+1));
	// src.setAttribute(FILE, file.getAbsolutePath());
	//
	// textElem.appendChild(src);
	// Element editions = doc.createElement(EDITIONS);
	// textElem.appendChild(editions);
	// textRoot.appendChild(textElem);
	//
	// Text t = new Text(this, textElem);
	// if (texts.keySet().contains(txtname)) {
	// System.out.println("Warning corpus configuration file contains texts with the same ID: "+txtname+" (next ones are ignored).");
	// return null;
	// }
	// texts.put(txtname, t);
	// textnames.add(t.getName());
	// return t;
	// }
	//
	// /**
	// * Removes the text.
	// *
	// * @param name the name
	// * @return the text
	// */
	// public Text removeText(String name) {
	// Text text = texts.get(name);
	//
	// if (text != null) {
	// Element textRoot = (Element) getSelfElement().getElementsByTagName(
	// TEXTS).item(0); //$NON-NLS-1$
	// textRoot.removeChild(text.getSelfElement());
	// texts.remove(name);
	// textnames.remove(name);
	// }
	// return text;
	// }
	
	/*
	 * Load Text from a XML DOM Element -> retro compatibility of import.xml file
	 * Texts&Queries are created in
	 */
	protected boolean _load(Element e) {
		if (e == null) return false;
		
		this.setUserName(e.getAttribute(NAME));
		this.pID = e.getAttribute(NAME);
		// this.pLanguage = e.getAttribute(LANG);
		// System.out.println("LOAD BASE "+this.name);
		loadImportInfos(e);
		
		// get texts
		NodeList textsNodes = e.getElementsByTagName(TEXTS);
		if (textsNodes.getLength() > 0) {
			Element textsElement = (Element) textsNodes.item(0);
			NodeList textsList = textsElement.getElementsByTagName(TEXT);
			for (int i = 0; i < textsList.getLength(); i++) {
				Element textElement = (Element) textsList.item(i);
				Text t = new Text(this.getProject());
				t._load(textElement);
				// if (texts.keySet().contains(t.getName())) {
				// System.out.println("Warning "+this.name+" corpus configuration file contains duplicates with ID="+t.getName()+" (next ones are ignored).");
				// } else {
				// texts.put(t.getName(), t);
				// textnames.add(t.getName());
				// }
				if (this.getID() != null && this.getID().length() > 0) {
					try {
						for (Edition ed : t.getEditions()) {
							ed.saveParameter("corpus", this.getID());
						}
					}
					catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		}
		
		// get saved queries
		if (e.getElementsByTagName("queries").getLength() > 0) { //$NON-NLS-1$
			Element queriesElement = (Element) e.getElementsByTagName("queries").item(0); //$NON-NLS-1$
			NodeList queryList = queriesElement.getElementsByTagName("query"); //$NON-NLS-1$
			for (int i = 0; i < queryList.getLength(); i++) {
				Element qelem = (Element) queryList.item(i);
				SavedQuery q = new SavedQuery(this);
				q.setParameters(qelem);
				// this.queries.put(q.getName(), q);
			}
		}
		
		// loadImportInfos(getSelfElement());
		
		return true;
	}
	
	// /**
	// * Adds the query log.
	// *
	// * @param query the query
	// * @param examples the examples
	// * @return the query log
	// */
	// public SavedQuery addQueryLog(String query, List<String> examples) {
	// if (queries.containsKey(query))
	// return queries.get(query);
	// if (getSelfElement() == null) return null;
	//
	// SavedQuery q = new SavedQuery(this);
	// q.setParameter(query, examples);
	//
	// // q.selfElement = ;
	// Document doc = getSelfElement().getOwnerDocument();
	// Element queriesRoot;
	// if (getSelfElement().getElementsByTagName("queries").getLength() == 0) //$NON-NLS-1$
	// {
	// queriesRoot = doc.createElement("queries"); //$NON-NLS-1$
	// getSelfElement().appendChild(queriesRoot);
	// } else
	// queriesRoot = (Element) getSelfElement()
	// .getElementsByTagName("queries").item(0); //$NON-NLS-1$
	//
	// Element queryElem = doc.createElement("query"); //$NON-NLS-1$
	// queryElem.setAttribute(NAME, q.getName());
	// queryElem.setAttribute("query", q.getQuery()); //$NON-NLS-1$
	// Element examplesElem = doc.createElement("examples"); //$NON-NLS-1$
	// queryElem.appendChild(examplesElem);
	// for (String ex : q.getExamples()) {
	// Element exampleElem = doc.createElement("example"); //$NON-NLS-1$
	// exampleElem.setTextContent(ex);
	// examplesElem.appendChild(exampleElem);
	// }
	//
	// queriesRoot.appendChild(queryElem);
	// queries.put(q.getName(), q);
	// return q;
	// }
	//
	// /**
	// * Removes the query.
	// *
	// * @param name the name
	// * @return the query log
	// */
	// public SavedQuery removeQuery(String name) {
	// SavedQuery query = queries.get(name);
	//
	// if (query != null) {
	// Element queriesRoot = (Element) getSelfElement().getElementsByTagName(
	// "queries").item(0); //$NON-NLS-1$
	// queriesRoot.removeChild(query.getSelfElement());
	// queries.remove(name);
	// }
	// return query;
	// }
	
	// /* (non-Javadoc)
	// * @see org.txm.objects.TxmObject#removeChildren(org.txm.objects.TxmObject)
	// */
	// public TxmObject removeChildren(TxmObject o) {
	// if (o == null)
	// return null;
	// if (o instanceof Text) {
	// textsElement.removeChild(o.getSelfElement());
	// textnames.remove(((Text) o).getName());
	// return texts.remove(((Text) o).getName());
	// } else if (o instanceof SavedQuery) {
	// queriesElement.removeChild(o.getSelfElement());
	// return queries.remove(((SavedQuery) o).getName());
	// }
	// return null;
	// }
	
	
	//
	// /**
	// * Register maincorpus.
	// *
	// * @param corpusName the corpus name
	// */
	// public Element registerMaincorpus(String corpusName) {
	// Document doc = getSelfElement().getOwnerDocument();
	// Element maincorpus = doc.createElement(CORPUS);
	// maincorpus.setAttribute(NAME, corpusName);
	// if (getSelfElement().getElementsByTagName(CORPORA).getLength() == 0) {
	// Log.severe(TXMCoreMessages.Base_22);
	// return null;
	// }
	// Element corporaElem = (Element) getSelfElement().getElementsByTagName(CORPORA).item(0);
	// corporaElem.appendChild(maincorpus); // append maincorpus
	// return maincorpus;
	// }
	
	public boolean export(File exportzip) {
		return export(exportzip, null, null, null, null);
	}
	
	public boolean export(File exportzip, IProgressMonitor monitor, HashSet<String> ignore, String version, String rename) {
		if (this.getProject() != null) {
			try {
				this.getProject().saveParameters(true); // write parameters in preferences
				this.getProject().persist(true); // save preferences
				return this.getProject().export(exportzip, monitor, ignore, version, rename);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}
	
	// /**
	// * Export.
	// *
	// * @param exportzip the exportzip
	// * @return true, if successful
	// */
	// public boolean export(File exportzip, IProgressMonitor monitor) {
	// File binarydir = this.getProjectDirectory();
	// if (binarydir.exists() && exportzip.getParentFile().exists()) {
	// try {
	// Zip.compress(binarydir, exportzip, monitor);
	// }
	// catch (IOException e) {
	// org.txm.utils.logger.Log.printStackTrace(e);
	// return false;
	// }
	// return true;
	// }
	// else {
	// System.out.println(NLS.bind(TXMCoreMessages.binaryDirectoryP0NotFound, binarydir));
	// System.out.println(NLS.bind(TXMCoreMessages.parentDirectoryP0NotFound, exportzip.getParentFile()));
	//
	// }
	// return false;
	// }
	
	/**
	 * 
	 * Gets the import metadata.
	 *
	 * @return the import metadata
	 */
	public ArrayList<Metadata> getImportMetadata() {
		return importmetadatas;
	}
	
	/**
	 * Gets the import metadata.
	 *
	 * @return the import metadata
	 */
	public ArrayList<MetadataGroup> getMetadataGroups() {
		return groupedMetadatas;
	}
	
	/**
	 * Load import infos.
	 *
	 * @param root the root
	 * @return true, if successful
	 */
	private boolean loadImportInfos(Element root) {
		// import general infos
		// System.out.println("load import infos: "+root);
		this.getProject().setEncoding(root.getAttribute("encoding"));
		this.getProject().setLang(root.getAttribute(LANG));
		String str = root.getAttribute(WORDSPERPAGE);
		if (str == null || str.length() == 0) str = "1000";
		this.pageSize = Integer.parseInt(str);
		str = root.getAttribute(DOEDITIONSTEP);
		if (str == null || str.length() == 0) str = "true";
		this.doEditionStep = "true".equals(str);
		str = root.getAttribute(AVOIDXMLSTEPS);
		if (str == null || str.length() == 0) str = "false";
		this.avoidXMLSteps = "true".equals(str);
		str = root.getAttribute(CLEANTEMPDIRS);
		if (str == null || str.length() == 0) str = "false";
		this.cleanTempDirs = "true".equals(str);
		// this.srcdir = root.getAttribute("rootdir");
		// this.script = root.getAttribute("script");
		// this.xslt = root.getAttribute("xslt");
		this.pDescription = root.getAttribute("desc");
		this.getProject().setFont(root.getAttribute("font"));
		
		// tokenizer infos
		/*
		 * NodeList tokenizerNodes = root.getElementsByTagName("tokenizer");
		 * if (tokenizerNodes.getLength() > 0)
		 * {
		 * Element tokenizerNode = (Element) tokenizerNodes.item(0);
		 * NodeList tokenizerparamNodes = tokenizerNode.getElementsByTagName("param");
		 * }
		 */
		
		// Pattributes definitions
		NodeList pAttributesNodes = root.getElementsByTagName("pattributes");
		pPAttributesInformations.clear();
		if (pAttributesNodes.getLength() > 0) { // only one
			Element pattributesNode = (Element) pAttributesNodes.item(0);
			NodeList pattributeNodes = pattributesNode.getElementsByTagName("pattribute");
			for (int i = 0; i < pattributeNodes.getLength(); i++) {
				Element pattribute = (Element) pattributeNodes.item(i);
				HashMap<String, String> values = new HashMap<>();
				pPAttributesInformations.put(pattribute.getAttribute(ID), values);
				
				for (String name : attributeInfoNames) {
					if (pattribute.getAttribute(name) != null) {
						values.put(name, pattribute.getAttribute(name));
					}
					else {
						values.put(name, "");
					}
				}
			}
		}
		
		// Sattributes definitions
		NodeList sAttributesNodes = root.getElementsByTagName("sattributes");
		pSAttributesInformations.clear();
		if (sAttributesNodes.getLength() > 0) {
			Element sattributesNode = (Element) sAttributesNodes.item(0);
			NodeList sattributeNodes = sattributesNode.getElementsByTagName("sattribute");
			for (int i = 0; i < sattributeNodes.getLength(); i++) {
				Element sattribute = (Element) sattributeNodes.item(i);
				HashMap<String, String> values = new HashMap<>();
				pSAttributesInformations.put(sattribute.getAttribute(ID), values);
				
				for (String name : attributeInfoNames) {
					if (sattribute.getAttribute(name) != null) {
						values.put(name, sattribute.getAttribute(name));
					}
					else {
						values.put(name, "");
					}
				}
			}
		}
		// System.out.println("sAttributes Infos : "+sAttributesInfos);
		
		// Metadata definitions
		importmetadatas.clear();
		groupedMetadatas.clear();
		
		// UI: sort, view and ref properties to show
		NodeList uisNodes = root.getElementsByTagName("uis");
		if (uisNodes.getLength() > 0) { // only one
			Element uisElem = (Element) uisNodes.item(0);
			NodeList uiNodes = uisElem.getElementsByTagName("ui");
			for (int c = 0; c < uiNodes.getLength(); c++) {
				Element uiElem = (Element) uiNodes.item(c);
				if ("concordance".equals(uiElem.getAttribute("command").toLowerCase())) {
					NodeList viewsNodes = uiElem.getElementsByTagName("view");
					viewProperties.clear();
					if (viewsNodes.getLength() > 0) { // only one
						Element viewNode = (Element) viewsNodes.item(0);
						this.pDefaultViewProperty = viewNode.getAttribute(DEFAULT);
						for (int i = 0; i < viewNode.getChildNodes().getLength(); i++) {
							try {
								Element element = (Element) viewNode.getChildNodes().item(i);
								viewProperties.add(element.getAttribute(ID));
							}
							catch (Exception e) {
							} // quick and dirty
						}
					}
					
					NodeList sortsNodes = uiElem.getElementsByTagName("sort");
					sortProperties.clear();
					if (sortsNodes.getLength() > 0) { // only one
						Element sortNode = (Element) sortsNodes.item(0);
						this.pDefaultSortProperty = sortNode.getAttribute(DEFAULT);
						for (int i = 0; i < sortNode.getChildNodes().getLength(); i++) {
							try {
								Element element = (Element) sortNode.getChildNodes().item(i);
								sortProperties.add(element.getAttribute(ID));
							}
							catch (Exception e) {
							} // quick and dirty
						}
					}
					
					NodeList refsNodes = uiElem.getElementsByTagName("ref");
					refProperties.clear();
					if (refsNodes.getLength() > 0) { // only one
						Element refNode = (Element) refsNodes.item(0);
						this.pDefaultRefProperty = refNode.getAttribute(DEFAULT);
						for (int i = 0; i < refNode.getChildNodes().getLength(); i++) {
							try {
								Element element = (Element) refNode.getChildNodes().item(i);
								refProperties.add(element.getAttribute(ID));
							}
							catch (Exception e) {
							} // quick and dirty
						}
					}
					
					NodeList cqlLimitNodes = uiElem.getElementsByTagName("context_limits");
					// cql_limit_query = null;
					if (cqlLimitNodes.getLength() > 0) { // only one node
						Element context_limitsNode = (Element) cqlLimitNodes.item(0);
						cql_limit_query = context_limitsNode.getTextContent();
						if ("list".equals(context_limitsNode.getAttribute("type"))) {
							String query = "";
							String[] split = cql_limit_query.split(",");
							if (split.length > 0) {
								for (String s : split) {
									query += "|<" + s.trim() + ">[]";
								}
								cql_limit_query = query.substring(1);
							}
						}
					}
					
					NodeList cqlCtxSizeNodes = uiElem.getElementsByTagName("context_sizes");
					// cql_limit_query = null;
					if (cqlCtxSizeNodes.getLength() > 0) { // only one node
						Element context_limitsNode = (Element) cqlCtxSizeNodes.item(0);
						try {
							conc_left_context = Integer.parseInt(context_limitsNode.getAttribute("left"));
							conc_right_context = Integer.parseInt(context_limitsNode.getAttribute("right"));
						}
						catch (Exception e) {
							System.out.println("In uis/ui@command=concordance/context_sizes: context sizes number malformed: " + e);
						}
					}
				}
				else if ("selection".equals(uiElem.getAttribute("command").toLowerCase())) {
					NodeList sattributeNodes = uiElem.getElementsByTagName("sattribute");
					for (int i = 0; i < sattributeNodes.getLength(); i++) {
						Element sattributeElem = (Element) sattributeNodes.item(i);
						String ref = sattributeElem.getAttribute("ref");
						// String id = ref.substring(5);
						HashMap<String, String> infos = pSAttributesInformations.get(ref);
						if (infos == null) {
							Log.warning("Malformed sattribute tag at uis/command/selection: could not find s-attribute informations for @ref=" + ref);
						}
						// System.out.println("REF: "+ref+" infos: "+infos);
						if (infos != null) {
							if (ref.startsWith("text_")) {
								// System.out.println("TEST: "+infos.get("type")+" =? 'Date'");
								if (infos.get(TYPE).equals("Date")) {
									importmetadatas.add(new DateMetadata(infos, sattributeElem));
								}
								else {
									importmetadatas.add(new Metadata(infos, sattributeElem));
								}
							}
						}
					}
					// System.out.println("Load groups: "+importmetadatas);
					NodeList groupNodes = uiElem.getElementsByTagName("group");
					for (int i = 0; i < groupNodes.getLength(); i++) {
						Element groupElem = (Element) groupNodes.item(i);
						if (groupElem.getAttribute(TYPE).equals("MinMaxDate")) {
							groupedMetadatas.add(new MinMaxDateGroup(this, groupElem));
						}
					}
				}
			}
		}
		
		// get available editions
		NodeList editionsNodes = root.getElementsByTagName(EDITIONS);
		if (editionsNodes.getLength() > 0) { // only one
			Element editionNode = (Element) editionsNodes.item(0);
			
			
			this.getProject().setDefaultEditionName(editionNode.getAttribute(DEFAULT));
			
			NodeList editionNodes = editionNode.getElementsByTagName(EDITION);
			for (int i = 0; i < editionNodes.getLength(); i++) {
				Element edition = (Element) editionNodes.item(i);
				String name = edition.getAttribute(NAME);
				String built = edition.getAttribute("build_edition");
				if (built == null || built.length() == 0) built = "true";
				
				String pbElement = edition.getAttribute("page_break_tag");
				int wpp = 1000;
				try {
					wpp = Integer.parseInt(edition.getAttribute("words_per_page"));
				}
				catch (Exception e) {
				}
				// System.out.println("declare edition: "+name);
				// String corpus = edition.getAttribute("corpus");
				// String mode = edition.getAttribute("mode");
				// String script = edition.getAttribute("script");
				
				EditionDefinition ed = this.getProject().getEditionDefinition(name);
				ed.setBuildEdition("true".equals(built));
				ed.setPageBreakTag(pbElement);
				ed.setWordsPerPage(wpp);
			}
		}
		
		// Text biblio
		NodeList bibliosNodes = root.getElementsByTagName("biblios");
		if (bibliosNodes.getLength() > 0) { // only one
			Element biblioNode = (Element) bibliosNodes.item(0);
			NodeList metadataNodes = biblioNode.getElementsByTagName(BIBLIO);
			for (int i = 0; i < metadataNodes.getLength(); i++) {
				Element biblio = (Element) metadataNodes.item(i);
				String id = biblio.getAttribute(ID);
				String url = biblio.getTextContent();
				Text text = this.getProject().getText(id);
				if (text != null) {
					Edition biblioEdition = new Edition(text);
					try {
						if (this.getID() != null && this.getID().length() > 0) {
							biblioEdition.saveParameter("corpus", this.getID());
						}
					}
					catch (Exception e) {
						e.printStackTrace();
					}
					biblioEdition.setIndex(url);
				}
			}
		}
		
		return true;
	}
	
	public HashMap<String, Alignement> getLinks() {
		return links;
	}
	
	public ArrayList<Alignement> getLinksFrom(String from) {
		ArrayList<Alignement> rez = new ArrayList<>();
		for (String key : links.keySet()) {
			if (key.startsWith("#" + from + " ")) {
				rez.add(links.get(key));
			}
		}
		return rez;
	}
	
	public ArrayList<Alignement> getLinksTo(String to) {
		ArrayList<Alignement> rez = new ArrayList<>();
		for (String key : links.keySet()) {
			if (key.endsWith(" #" + to)) {
				rez.add(links.get(key));
			}
		}
		return rez;
	}
	
	public Alignement getLink(String from, String to) {
		return links.get("#" + from + " #" + to);
	}
	
	/**
	 * Gets the p attributes info names.
	 *
	 * @return the p attributes info names
	 */
	public String[] getPAttributesInfoNames() {
		return attributeInfoNames;
	}
	
	/**
	 * Gets the p attributes infos.
	 *
	 * @return the p attributes infos
	 */
	public HashMap<String, HashMap<String, String>> getPAttributesInfos() {
		return pPAttributesInformations;
	}
	
	/**
	 * Gets the s attributes infos.
	 *
	 * @return the s attributes infos
	 */
	public HashMap<String, HashMap<String, String>> getSAttributesInfos() {
		return pSAttributesInformations;
	}
	
	public String getFont() {
		return getProject().getFont();
	}
	
	// public String getLang( ) {
	// return getProject().getLang();
	// }
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.pDescription;
	}
	
	/**
	 * Sets the description.
	 *
	 */
	public void setDescription(String desc) {
		this.pDescription = desc;
	}
	
	public ArrayList<String> getRefProperties() {
		return refProperties;
	}
	
	public void setRefProperties(ArrayList<String> refProperties) {
		this.refProperties = refProperties;
	}
	
	public ArrayList<String> getSortProperties() {
		return sortProperties;
	}
	
	public void setSortProperties(ArrayList<String> sortProperties) {
		this.sortProperties = sortProperties;
	}
	
	public ArrayList<String> getViewProperties() {
		return viewProperties;
	}
	
	public void setViewProperties(ArrayList<String> viewProperties) {
		this.viewProperties = viewProperties;
	}
	
	/**
	 * @return the defaultRefProperty
	 */
	public String getDefaultRefProperty() {
		return pDefaultRefProperty;
	}
	
	/**
	 * @param defaultRefProperty the defaultRefProperty to set
	 */
	public void setDefaultRefProperty(String defaultRefProperty) {
		this.pDefaultRefProperty = defaultRefProperty;
	}
	
	/**
	 * @return the defaultSortProperty
	 */
	public String getDefaultSortProperty() {
		return pDefaultSortProperty;
	}
	
	/**
	 * @param defaultSortProperty the defaultSortProperty to set
	 */
	public void setDefaultSortProperty(String defaultSortProperty) {
		this.pDefaultSortProperty = defaultSortProperty;
	}
	
	/**
	 * @return the defaultSortProperty
	 */
	public String getDefaultViewProperty() {
		return pDefaultViewProperty;
	}
	
	/**
	 * @param defaultViewProperty the defaultSortProperty to set
	 */
	public void setDefaultViewProperty(String defaultViewProperty) {
		this.pDefaultViewProperty = defaultViewProperty;
	}
	
	public void setName(String name) {
		this.setUserName(name);
	}
	
	protected void setID(String id) {
		this.pID = id;
	}
	
	public abstract void setIsModified(boolean b);
	
	public abstract CorpusBuild getRootCorpusBuild();
	
	public abstract List<? extends Match> getMatches();

	/**
	 * 
	 * @return a list of errors if any, empty means the corpus is build is OK
	 */
	public abstract List<?> isBuildValid();
	
	/**
	 * 
	 * @return true an error message if the corpus is not ready
	 */
	public abstract String isReady();
}
