package org.txm.objects;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstraction of a match. A Match is defined by its start-end positions.
 * 
 * It may have a target position to focus a position
 * 
 * TODO manage multiple target positions
 * TODO manage multiple labeled target positions
 * TODO store match index (hard to update the match indices ?)
 * TODO store sub match index (hard to update the submatch indices ?)
 * 
 * @author mdecorde
 *
 */
public abstract class Match implements Comparable<Match> {
	
	public abstract int getStart();
	
	public abstract int getEnd();
	
	public abstract int getTarget();
	
	public boolean contains(Match m) {
		return (this.getStart() <= m.getStart() && m.getEnd() <= this.getEnd());
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Match m) {
		
		if (m == null)
			return -1;
		if (this.getStart() < m.getStart())
			return -1;
		else if (this.getStart() > m.getStart())
			return 1;
		else if (this.getEnd() < m.getEnd())
			return -1;
		else
			return 1;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "<" + this.getStart() + ", " + this.getEnd() + ">"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}
	
	/**
	 * Size.
	 *
	 * @return the int
	 */
	public int size() {
		return getEnd() - getStart() + 1;
	}
	
	public List<Integer> getRange() {
		List<Integer> ret = new ArrayList<>(size());
		for (int i = getStart(); i <= getEnd(); i++)
			ret.add(i);
		return ret;
	}
	
	public boolean equals(Object o) {
		if (o instanceof Match) {
			Match m = (Match) o;
			return m.getStart() == this.getStart() && m.getEnd() == this.getEnd() && m.getTarget() == this.getTarget();
		}
		return super.equals(o);
	}
	
	public int hashCode() {
		return Integer.hashCode(this.getStart()) + Integer.hashCode(this.getEnd()) + Integer.hashCode(this.getTarget());
	}
	
	public abstract void setEnd(int p);
	
	public abstract void setStart(int p);
	
	public abstract void setTarget(int p);
	
	/**
	 * intersect sorted list of Matches
	 * 
	 * @param list1
	 * @param sublist2
	 * @param strict_inclusion if true the match must be exactly included
	 * @return the matches of sublist2 included in the matches of list1
	 */
	public static List<Match> intersect(List<? extends Match> list1, List<? extends Match> sublist2, boolean strict_inclusion) {
		ArrayList<Match> result = new ArrayList<>();
		int i1 = 0;
		int i2 = 0;
		for (; i2 < sublist2.size() && i1 < list1.size();) {
			Match m = list1.get(i1);
			Match subm = sublist2.get(i2);
			if (m.getEnd() < subm.getStart()) {
				i1++;
			}
			else if (subm.getEnd() < m.getStart()) {
				i2++;
			}
			else {
				if (strict_inclusion) {
					if (m.getStart() <= subm.getStart() && subm.getEnd() <= m.getEnd()) {
						result.add(subm);
					}
				}
				else {
					result.add(subm);
				}
				
				i2++;
			}
		}
		
		return result;
	}

	public int[] getStartEnd() {
		
		return new int[] {getStart(), getEnd()};
	}
}
