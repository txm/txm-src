// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.objects;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.utils.TXMProgressMonitor;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * contains a query and examples of results @ author mdecorde.
 */
public class SavedQuery extends TXMResult {
	
	@Parameter(key = TBXPreferences.VALUE)
	String pValue;
	
	/** The examples. */
	List<String> examples;
	
	/** The base. */
	CorpusBuild corpus;
	
	/**
	 * Instantiates a new query log.
	 *
	 * @param c the corpus
	 */
	public SavedQuery(CorpusBuild c) {
		super(c);
		this.setVisible(false);
	}
	
	/**
	 * Instantiates a new query log.
	 *
	 */
	public SavedQuery(String node) {
		super(node);
		this.setVisible(false);
	}
	
	@Override
	protected boolean _compute(TXMProgressMonitor monitor) {
		return true;
	}
	
	protected boolean setParameters(Element pElement) {
		
		this.setUserName(pElement.getAttribute("name")); //$NON-NLS-1$
		this.pValue = pElement.getAttribute("value"); //$NON-NLS-1$
		this.examples = new ArrayList<>();
		NodeList examplesN = pElement.getElementsByTagName("example"); //$NON-NLS-1$
		for (int i = 0; i < examplesN.getLength(); i++) {
			Element e = (Element) examplesN.item(i);
			examples.add(e.getTextContent());
		}
		if (this.getUserName() == null || this.pValue == null)
			return false;
		return true;
	}
	
	/**
	 * Gets the query.
	 *
	 * @return the query
	 */
	public String getQuery() {
		return pValue;
	}
	
	/**
	 * Gets the examples.
	 *
	 * @return the examples
	 */
	public List<String> getExamples() {
		return examples;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getUserName() + "=" + this.pValue; // + " >> " + this.examples; //$NON-NLS-1$
	}
	
	@Override
	public boolean saveParameters() throws Exception {
		return true;
	}
	
	@Override
	public boolean loadParameters() throws Exception {
		return true;
	}

	
	@Override
	public String getName() {
		return getUserName();
	}
	
	@Override
	public String getSimpleName() {
		return getUserName();
	}
	
	@Override
	public String getDetails() {
		return toString();
	}
	
	@Override
	public void clean() {
		
	}
	
	@Override
	public boolean canCompute() throws Exception {
		return true;
	}
	
	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		return false;
	}
	
	@Override
	public String getResultType() {
		return "SavedQuery";
	}
	
	public void setParameters(String query, ArrayList<String> examples) {
		this.setUserName(query);
		this.pValue = query;
		this.examples = examples;
	}
}
