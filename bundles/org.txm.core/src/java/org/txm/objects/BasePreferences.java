package org.txm.objects;

import org.eclipse.core.internal.preferences.EclipsePreferences;
import org.eclipse.core.internal.preferences.PreferencesService;

/**
 * Represents a node in the Eclipse preference hierarchy which stores preference
 * values for projects.
 * 
 * @since 3.0
 */
public class BasePreferences extends EclipsePreferences {
	
	public BasePreferences(Project base) {
		super(null, "TXMCORPUS-"+base.getName());
		PreferencesService.getDefault();
	}
}
