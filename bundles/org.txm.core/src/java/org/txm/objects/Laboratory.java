package org.txm.objects;

import java.io.File;

import org.txm.core.results.TXMResult;
import org.txm.utils.TXMProgressMonitor;

public class Laboratory extends TXMResult {

	public Laboratory(String parametersNodePath) {
		super(parametersNodePath);
		setVisible(false);
	}
	
	public Laboratory(CorpusBuild corpus) {
		super(corpus);
		setVisible(false);
	}

	@Override
	public boolean saveParameters() throws Exception {
		return true;
	}
	
	@Override
	public boolean isInternalPersistable() {
		return true;
	}

	@Override
	public boolean loadParameters() throws Exception {
		return true;
	}

	@Override
	public String getName() {
		return getParent().getName();
	}

	@Override
	public String getSimpleName() {
		return getParent().getSimpleName();
	}

	@Override
	public String getDetails() {
		return "Contains results not attached to a TXM result";
	}

	@Override
	public void clean() {
		
	}

	@Override
	public boolean canCompute() throws Exception {
		
		return true;
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		return true;
	}

	@Override
	protected boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {

		return false;
	}

	@Override
	public String getResultType() {

		return "Laboratory";
	}

}
