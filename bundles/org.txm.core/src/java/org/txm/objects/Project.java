// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (Tue, 24 Jan 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.objects;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.osgi.util.NLS;
import org.osgi.framework.Bundle;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.txm.Toolbox;
import org.txm.core.engines.ImportEngine;
import org.txm.core.engines.ImportEngines;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.importer.xtz.ImportKeys;
import org.txm.utils.DeleteDir;
import org.txm.utils.LogMonitor;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.io.FileCopy;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;
import org.txm.utils.zip.Zip;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * The Class Project represent a Corpus project with its sources, corpus indexes, texts, editions, pages, etc.
 */
public class Project extends TXMResult {
	
	public static final String TXMCORPUSNATURE = "org.txm.core.CorpusNature";
	
	// private ArrayList<String> pEditionNames = new ArrayList<String>();
	private IProject rcpProject;
	
	private ProjectScope preferenceScope;
	
	/**
	 * If true, the computing will build the main corpus, editions, texts, etc.
	 */
	protected boolean needToBuild;
	
	@Parameter(key = TBXPreferences.CLEAN)
	private boolean pCleanAfterBuild;
	
	/**
	 * Name of import module.
	 */
	@Parameter(key = TBXPreferences.IMPORT_MODULE_NAME)
	String pImportModuleName;
	
	/**
	 * Corpus version when the corpus is imported/updated. Currently 0.8
	 */
	@Parameter(key = TBXPreferences.CORPUS_VERSION, type = Parameter.INTERNAL)
	String pCorpusVersion;
	
	/**
	 * Exact TXM full version when the corpus is imported/updated
	 */
	@Parameter(key = TBXPreferences.TXM_VERSION, type = Parameter.INTERNAL)
	String pTXMVersion;
	
	/**
	 * 
	 */
	@Parameter(key = TBXPreferences.LINKS)
	private HashMap<String, Alignement> links;
	
	/**
	 * Default edition.
	 */
	@Parameter(key = TBXPreferences.DEFAULT_EDITION)
	private String pDefaultEditionName;
	
	/**
	 * Encoding.
	 */
	@Parameter(key = TBXPreferences.ENCODING)
	private String pEncoding;
	
	/**
	 * Language.
	 */
	@Parameter(key = TBXPreferences.LANG)
	private String pLanguage;
	
	/**
	 * Source directory.
	 */
	@Parameter(key = TBXPreferences.SOURCE)
	private File pSrcDirectory;
	
	// /** The front xslt path. */
	// @Parameter(key=TBXPreferences.FRONT_XSL)
	// private String pXsl;
	// /** The front xslt path. */
	// @Parameter(key=TBXPreferences.FRONT_XSL_PARAMETERS)
	// private HashMap<String, String> pXsltParameters;
	
	/**
	 * Author.
	 */
	@Parameter(key = TBXPreferences.AUTHOR)
	private String pAuthor;
	
	/**
	 * Font.
	 */
	@Parameter(key = TBXPreferences.FONT)
	private String pFont;
	
	/**
	 * Description.
	 */
	@Parameter(key = TBXPreferences.DESCRIPTION)
	private String pDescription;
	
	/**
	 * 
	 */
	@Parameter(key = TBXPreferences.ANNOTATE)
	private boolean pAnnotate;
	
	/**
	 * Instantiates a new project and define if it needs to build its main corpus, texts, editions, etc..
	 * 
	 * @param workspace
	 * @param name
	 * @throws Exception
	 */
	public Project(Workspace workspace, String name, boolean needToBuild) throws Exception {
		super("/project/" + name + "/" + createUUID() + "_Project", workspace);
		
		this.needToBuild = needToBuild;
		this.setUserName(name);
		
		this.setVisible(false);
		
		initRCPStuffs(new LogMonitor());
		
		if (isOpen()) {
			this.autoSaveParametersFromAnnotations();
			this.saveParameters();
			TXMPreferences.flush(this);
		}
	}
	
	/**
	 * Instantiates a new existing project from persistence.
	 * This project will not be builded, main corpus, texts, editions, etc. will be restored from persistence.
	 * 
	 * @param parametersNodePath
	 * @throws CoreException
	 */
	public Project(String parametersNodePath) throws CoreException {
		super(parametersNodePath);
		
		this.needToBuild = false;
		this.setVisible(false);
		
		initRCPStuffs(new LogMonitor());
		
		//
		// this.setPath(project.getPath() + getName());
		// params = parameters;
		// setSelfElement(params.root);
		// this.project = project;
		// //load();
		// this.userName = params.name;
		// if (!userName.equals(userName.toUpperCase())) {
		// System.out.println("Warning in Base.init: binary corpus name contains lower case letters: "+this.userName);
		// }
		//
		// this.scope = new BasePreferenceScope(this);
		
		// reloading existing results
		this.loadResults(null);
	}
	
	/**
	 * Instantiates a new project that needs to build its main corpus, texts, editions, etc..
	 * 
	 * @param workspace
	 * @param name
	 * @throws Exception
	 */
	public Project(Workspace workspace, String name) throws Exception {
		this(workspace, name, true);
	}
	
	public boolean isOpen() {
		return rcpProject != null && rcpProject.isOpen();
	}
	
	public boolean open(IProgressMonitor monitor) {
		if (rcpProject != null && !rcpProject.isOpen()) {
			try {
				monitor.beginTask("Openning project", 100);
				
				rcpProject.open(monitor);
				
				
				initRCPStuffs(monitor);
				
				monitor.subTask("Loading results");
				
				this.loadResults(null);
				
				for (TXMResult r : getDeepChildren()) {
					r.onProjectOpen();
				}
				
				for (TXMResult cb : getCorpora()) {
					try {
						if (!cb.compute(false)) {
							Log.warning("Deleting broken corpus: " + cb + " of " + cb.getProject());
							cb.delete();
						}
					}
					catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				monitor.setTaskName("");
			}
			catch (CoreException e) {
				e.printStackTrace();
			}
		}
		return isOpen();
	}
	
	public boolean close(IProgressMonitor monitor) {
		if (rcpProject != null && rcpProject.isOpen()) {
			try {
				for (TXMResult r : getDeepChildren()) {
					r.onProjectClose();
				}
				this.saveParameters(true);
				
				if (preferenceScope != null) {
					preferenceScope.getNode(this.getCommandPreferencesNodePath()).flush();
				}
				
				TXMPreferences.saveAll();
				
				this.getChildren().clear();
				
				rcpProject.close(monitor);
				
				this.preferenceScope = null;
				
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return !isOpen();
	}
	
	/**
	 * The documentation URL is stored in the "documentation" node and the "url" property.
	 * 
	 * If it is not set (empty value) the methods returns a local path to the doc/index.html file
	 * 
	 * if the "doc/index.html" file does not exists the returned path is empty as well
	 * 
	 * @return an URL to the corpus project documentation. empty if not available
	 */
	public String getDocumentationURL() {
		String documentationPath = this.getPreferencesScope().getNode("documentation").get("url", "");
		if (documentationPath.length() == 0) {
			File file = new File(this.getProjectDirectory(), "doc/index.html");
			if (file != null && file.exists()) {
				try {
					documentationPath = file.toURI().toURL().toString();
				}
				catch (MalformedURLException e) {
					e.printStackTrace();
				}
			}
		}
		return documentationPath;
	}
	
	/**
	 * 
	 * @return the corpus architecture version (currently 0.8.0)
	 */
	public String getCorpusVersion() {
		return pCorpusVersion;
	}
	
	public String setCorpusVersion(String version) {
		pCorpusVersion = version;
		return pCorpusVersion;
	}
	
	/**
	 * 
	 * @return the TXM architecture version when the corpus is imported
	 */
	public String getTXMVersion() {
		return pTXMVersion;
	}
	
	public String setTXMVersion(String version) {
		pTXMVersion = version;
		return pTXMVersion;
	}
	
	@Override
	public boolean isInternalPersistable() {
		return true;
	}
	
	@Override
	public boolean canCompute() throws Exception {
		if (getUserName() == null) {
			Log.severe("Project.canCompute(): can not compute with missing name.");
			return false;
		}
		
		if (this.needToBuild == true && (pSrcDirectory == null || pImportModuleName == null)) {
			Log.severe("Project.canCompute(): the project needs to build but source directory or import module name is missing.");
			return false;
		}
		return true;
	}
	
	/**
	 * flag the project to be rebuild with the Import module
	 */
	public void setNeedToBuild() {
		needToBuild = true;
	}
	
	
	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {
		
		// build the main corpus, texts, editions, etc.
		if (this.needToBuild) {
			
			// if (pImportXMLParameters != null) {
			// pImportXMLParameters.load();
			// this._load(pImportXMLParameters.name);
			// // convert to new preferences persistence
			// }
			
			// FIXME if the corpus name change, the TXMResults node path become wrong
			// if (!rcpProject.getName().equals(userName)) { // project directory and project name must stay in sync
			// IPath destination = new Path(this.getName());
			// if (destination.toFile().exists()) {
			// System.out.println("Error: could not rename project since one already existing with the same new name="+this.getName());
			// return false;
			// }
			// rcpProject.move(destination, true, null);
			// }
			
			ImportEngines engines = Toolbox.getImportEngines();
			
			// File scriptfile = new File(Toolbox.getTxmHomePath(), "scripts/groovy/user/org/txm/scripts/importer/"+importName+"/"+importName+"Loader.groovy"); //$NON-NLS-1$
			ImportEngine engine = engines.getEngine(getImportModuleName());
			if (engine == null) {
				//
				// engine = new ScriptedImportEngine(scriptfile) {
				// @Override
				// public boolean build(Project project, IProgressMonitor monitor) {
				// // TODO Auto-generated method stub
				// return false;
				// }
				// };
				// Log.severe("Project._compute(): Import engine not found.");
				
				// no import set
				Log.severe("Error: no import module set. Aborting import.");
				return false;
			}
			
			Log.finest("RUNNING IMPORT MODULE WITH NAME = " + getImportModuleName());
			
			if (this.getDoUpdate()) {
				Log.info(NLS.bind("Updating {0}", this.getName()));
			} else {
				Log.info(NLS.bind("Importing {0}", this.getName()));
			}
			
			boolean state = engine.build(this, monitor.internalGetSubMonitor()) == Status.OK_STATUS;
			
			if (state == true) {
				this.needToBuild = false;
				// This step could be done only if the compiler step if done and only if it modifying/deleting the corpus builds
				for (CorpusBuild corpus : getCorpusBuilds()) {
					if (corpus != null) {
						// corpus.compute(false);
					}
				}
				
				this.setTXMVersion(Toolbox.getVersion());
			}
			else { // import failed
				for (CorpusBuild corpus : getCorpusBuilds()) { // Cleaning eventual broken corpus
					if (corpus != null) {
						corpus.delete();
					}
				}
			}
			return state;
		}
		// Nothing to do if the project does not need to build
		else {
			return true;
		}
	}
	
	/**
	 * Initializes the RCP project and project scope.
	 * 
	 * @throws CoreException
	 */
	private void initRCPStuffs(IProgressMonitor monitor) throws CoreException {
		IWorkspace rcpWorkspace = ResourcesPlugin.getWorkspace();
		if (this.rcpProject == null) {
			String n = this.getName();
			this.rcpProject = rcpWorkspace.getRoot().getProject(n);
		}
		
		if (!rcpProject.exists()) {
			createRCPProject(monitor);
		}
		
		// if (!rcpProject.isOpen()) {
		// try {
		// rcpProject.open(monitor);
		// } catch (Exception e) {
		// System.out.println("Warning: restoring corpus project files: "+e);
		//// rcpProject.delete(true, monitor);
		//// createRCPProject();
		// }
		// }
		if (isOpen()) {
			this.preferenceScope = new ProjectScope(rcpProject);
		}
	}
	
	private void createRCPProject(IProgressMonitor monitor) throws CoreException {
		
		IProjectDescription description = ResourcesPlugin.getWorkspace().newProjectDescription(rcpProject.getName());
		IFolder folder = Workspace.getInstance().corporaProject.getFolder(rcpProject.getName());
		
		description.setLocation(folder.getLocation());
		description.setNatureIds(new String[] { TXMCORPUSNATURE });
		
		rcpProject.create(description, monitor);
		rcpProject.open(monitor);
		// TODO might fail if corpus source directory changed
		IFolder srcFolder = rcpProject.getFolder("src");
		if (pSrcDirectory != null && !pSrcDirectory.getName().isEmpty()) {
			IPath path = new Path(pSrcDirectory.getAbsolutePath());
			if (!srcFolder.exists()) {
				srcFolder.createLink(path, IResource.ALLOW_MISSING_LOCAL, monitor);
			}
		}
	}
	
	/**
	 * Loads and creates the persisted results from the preferences service.
	 */
	public void loadResults(Class<? extends TXMResult> clazz) {
		
		Log.finest("*** Project.loadResults(): reloading project persitent results...");
		
		ArrayList<String> resultNodePaths = TXMPreferences.getAllResultsNodePaths(this.getParametersNodeRootPath());
		
		Log.fine("Loading " + resultNodePaths.size() + " result(s) from project " + this.getName() + "...");
		
		// TXMPreferences.dump();
		
		HashMap<String, TXMResult> loaded = new HashMap<>();
		ArrayList<String> errors = new ArrayList<>();
		for (String resultNodePath : resultNodePaths) {
			
			try {
				//FIXME Move TXMResult initialization in the TXMResult.createResult(Preference node); parent and result node path are set before calling
				Preferences node = TXMPreferences.preferencesRootNode.node(resultNodePath);
				
				String className = node.get(TXMPreferences.CLASS, ""); //$NON-NLS-1$ //$NON-NLS-2$
				
				// Skip Project object
				if (Project.class.getName().equals(className)) { // already loaded in loadProjectFromProjectScope
					continue;
				}
				
				if (className == null || className.length() == 0) {
					errors.add(NLS.bind("Warning: can not restore object with no class name set with path={0}.", resultNodePath)); //$NON-NLS-1$
					node.removeNode();
					continue;
				}
				
				if (clazz != null && !clazz.getName().equals(className)) {
					continue; // only load the result of class=clazz
				}
				
				Log.fine("\nProject.loadResults(): loading from result node qualifier " + resultNodePath);
				// Log.finest("Toolbox.initialize(): class = " + TXMPreferences.getString("class", nodeQualifier));
				// TXMPreferences.dump();
				
				// //TODO see if necessary to prevent errors
				// TXMResult result2 = TXMResult.getResult(this, resultNodePath);
				// if (result2 != null) { // result already restored
				// continue;
				// }
				Log.fine("Project.loadResults(): class = " + className); //$NON-NLS-1$
				// Log.finest("Toolbox.initialize(): parent_uuid = " + TXMPreferences.getString(TXMPreferences.PARENT_UUID, nodeQualifier));
				Log.fine("Project.loadResults(): parent_uuid = " + node.get(TXMPreferences.PARENT_PARAMETERS_NODE_PATH, "")); //$NON-NLS-1$
				String bundleId = node.get(TXMPreferences.BUNDLE_ID, ""); //$NON-NLS-1$
				if (bundleId == null || bundleId.length() == 0) {
					errors.add(NLS.bind("Warning: can not restore {0} object with no bundle id set.", className)); //$NON-NLS-1$
					node.removeNode();
					continue;
				}
				Log.fine("Project.loadResults(): bundle_id = " + bundleId); //$NON-NLS-1$
				Bundle bundle = Platform.getBundle(bundleId);
				if (bundle == null) {
					errors.add(NLS.bind("Warning: can not restore {0} object with bundle id={1}.", className, bundleId)); //$NON-NLS-1$
					node.removeNode();
					continue;
				}
				
				Class<?> cl = bundle.loadClass(className);
				
				// TXMResult parent = loaded.get(node.get(TXMPreferences.PARENT_PARAMETERS_NODE_PATH, null));
				// TXMResult result = null;
				// if (parent == null) {
				Constructor<?> cons = cl.getConstructor(String.class);
				TXMResult result = (TXMResult) cons.newInstance(resultNodePath);
				// } else {
				// Constructor<?> cons = cl.getConstructor(String.class, TXMResult.class);
				// result = (TXMResult) cons.newInstance(resultNodePath, parent);
				// }
				loaded.put(resultNodePath, result);
				
				// TODO TXM 0.8.0 compatibility code: set user persistable to true is the result restored and was not an internal persistable result. See the userPErsitable member
				if (!result.isInternalPersistable() && !result.isUserPersistable()) {
					result.setUserPersistable(true);
				}
			}
			catch (Throwable e) {
				Log.warning("Internal error: could not initialize result: " + resultNodePath + ": " + e);
				Log.printStackTrace(e);
			}
		}
		if (errors.size() > 0) {
			for (int i = 0; i < 20 && i < errors.size(); i++) {
				Log.fine(errors.get(i));
			}
			if (errors.size() > 20) {
				Log.fine("more: " + (errors.size() - 20) + "...");
			}
		}
		
		// // load the corpus builds
		// for (CorpusBuild corpus : getCorpusBuilds()) {
		// if (corpus != null) {
		// corpus.compute(false);
		// }
		// }
	}
	
	// public BasePreferenceScope getPreferenceScope() {
	// return scope;
	// }
	
	// /* (non-Javadoc)
	// * @see org.txm.objects.TxmObject#save()
	// */
	// @Override
	// protected boolean _save() {
	// //System.out.println("SAVE BASE: "+params.root+" "+params.paramFile);
	// try {
	// // Création de la source DOM
	// Source source = new DOMSource(params.root);
	//
	// // Création du fichier de sortie
	// Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(params.paramFile), "UTF-8")); //$NON-NLS-1$
	// Result resultat = new StreamResult(writer);
	//
	// // Configuration du transformer
	// TransformerFactory fabrique = new net.sf.saxon.TransformerFactoryImpl();
	// Transformer transformer = fabrique.newTransformer();
	// transformer.setOutputProperty(OutputKeys.INDENT, "yes"); //$NON-NLS-1$
	// transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8"); //$NON-NLS-1$
	//
	// // Transformation
	// transformer.transform(source, resultat);
	// writer.close();
	// } catch (Exception e) {
	// e.printStackTrace();
	// Log.severe("Error while saving corpus parameters: "+e.getLocalizedMessage());
	// return false;
	// }
	// return true;
	// }
	
	/**
	 * Retro compatibility method for import.xml files.
	 * 
	 * @return
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	public boolean _load(BaseOldParameters params) throws Exception {
		params.load();
		this.setUserName(params.name);
		
		// set the import module name using the Loader script name
		this.pImportModuleName = params.scriptFile;
		if (this.pImportModuleName != null) {
			int idx = this.pImportModuleName.indexOf("Loader.groovy");
			if (idx > 0) {
				this.pImportModuleName = this.pImportModuleName.substring(0, idx);
				idx = this.pImportModuleName.lastIndexOf("/");
				if (idx > 0 && this.pImportModuleName.length() > idx) {
					this.pImportModuleName = this.pImportModuleName.substring(idx + 1);
				}
			}
		}
		else {
			this.pImportModuleName = "xtz";
		}
		
		this.setAnnotate("true".equals(params.getCorpusElement().getAttribute("annotate"))); //$NON-NLS-1$ );
		
		this.pSrcDirectory = params.paramFile.getParentFile();
		if (pSrcDirectory == null) {
			pSrcDirectory = new File(getProjectDirectory(), "src");
		}
		
		File dir = new File(this.getWorkspace().getLocation(), getUserName());
		File paramFile = new File(dir, "import.xml");
		if (!paramFile.exists()) return false;
		
		
		// get Alignements
		// System.out.println("Load alignement from params: "+params.links);
		for (String alignname : params.links.keySet()) {
			Element link = params.links.get(alignname);
			
			String target = link.getAttribute("target"); //$NON-NLS-1$
			String[] split = target.split("#"); //$NON-NLS-1$
			if (split.length != 3) {
				System.out.println(NLS.bind(TXMCoreMessages.errorColonLinkGrplinkAttargetMalformedColonP0, target));
				continue;
			}
			String from = split[1].trim();
			String to = split[2].trim();
			
			Alignement align = new Alignement(this, from, to,
					link.getAttribute("alignElement"), Integer.parseInt(link.getAttribute("alignLevel"))); //$NON-NLS-1$ //$NON-NLS-2$
			links.put("#" + from + " #" + to, align); //$NON-NLS-1$ //$NON-NLS-2$
			
			align = new Alignement(this, to, from,
					link.getAttribute("alignElement"), Integer.parseInt(link.getAttribute("alignLevel"))); //$NON-NLS-1$ //$NON-NLS-2$
			links.put("#" + to + " #" + from, align); //$NON-NLS-1$ //$NON-NLS-2$
		}
		
		return true;
	}
	
	
	/**
	 * Gets the text.
	 *
	 * @param id the id
	 * @return the text
	 */
	public Text getText(String id) {
		for (Text t : getTexts()) {
			if (t.getName().equals(id)) {
				return t;
			}
		}
		return null;
	}
	
	/**
	 * Gets the texts.
	 *
	 * @return the texts
	 */
	@SuppressWarnings("unchecked")
	public List<Text> getTexts() {
		return getChildren(Text.class);
	}
	
	/**
	 * Gets the texts id.
	 *
	 * @return the texts id
	 */
	public List<String> getTextsID() {
		List<String> ids = new ArrayList<>();
		for (Text t : getTexts()) {
			ids.add(t.getName());
		}
		return ids;
	}
	
	/**
	 * Gets the next text.
	 *
	 * @param t the t
	 * @return the next text
	 */
	public Text getNextText(Text t) {
		List<Text> texts = getTexts();
		if (texts.size() > 0) {
			int idx = texts.indexOf(t) + 1;
			if (idx >= 1 && idx < texts.size()) {
				return texts.get(idx);
			}
		}
		
		return t;
	}
	
	/**
	 * Gets the previous text.
	 *
	 * @param t the t
	 * @return the previous text
	 */
	public Text getPreviousText(Text t) {
		List<Text> texts = getTexts();
		if (texts.size() > 0) {
			int idx = texts.indexOf(t) - 1;
			if (idx >= 0) {
				return texts.get(idx);
			}
		}
		
		return t;
	}
	
	/**
	 * Gets the last text.
	 *
	 * @return the last text
	 */
	public Text getLastText() {
		List<Text> texts = getTexts();
		if (texts.size() > 0) return texts.get(texts.size() - 1);
		
		return null;
	}
	
	/**
	 * Gets the first text.
	 *
	 * @return the first text
	 */
	public Text getFirstText() {
		List<Text> texts = getTexts();
		if (texts.size() > 0) return texts.get(0);
		
		return null;
	}
	
	
	public boolean hasEditions() {
		return pDefaultEditionName != null;
	}
	
	public List<String> getEditionNames() {
		IEclipsePreferences node = this.getPreferencesScope().getNode(EditionDefinition.class.getSimpleName());
		try {
			return Arrays.asList(node.childrenNames());
		}
		catch (BackingStoreException e) {
			Log.printStackTrace(e);
		}
		
		return new ArrayList<>();
	}
	
	/**
	 * @return the defaultEditionName
	 */
	public String getDefaultEditionName() {
		return pDefaultEditionName;
	}
	
	/**
	 * @param defaultEditionName the defaultEditionName to set
	 */
	public void setDefaultEditionName(String defaultEditionName) {
		this.pDefaultEditionName = defaultEditionName;
	}
	
	public Workspace getWorkspace() {
		return (Workspace) getParent();
	}
	
	public boolean export(File exportzip) throws IOException {
		return export(exportzip, null, null, null, null);
	}
	
	/**
	 * Export.
	 *
	 * @param exportzip the exportzip
	 * @return true, if successful
	 * @throws IOException 
	 */
	public boolean export(File exportzip, IProgressMonitor monitor, HashSet<String> ignore, String version, String rename) throws IOException {
		File binaryDir = this.getProjectDirectory();
		
		if ("0.7.9".equals(version) || (rename != null && rename.length() > 0)) {
			
			File tmpExportDirectory = new File(binaryDir.getParentFile(), "txm-tmp-export-directory");
			DeleteDir.deleteDirectory(tmpExportDirectory);
			File newBinDirectory = new File(tmpExportDirectory, rename);
			newBinDirectory.mkdirs();
			
			FileCopy.copyFiles(binaryDir, newBinDirectory);
			if (!newBinDirectory.exists()) {
				Log.severe("Fail to copy binary directory $binDirectory to $newBinDirectory");
				return false;
			}
			binaryDir = newBinDirectory; // switch the binaryDirectory to work
		}
		
		if (binaryDir.exists() 
				&& exportzip.getParentFile().exists()) {
			this.save();
			
			if ("0.7.9".equals(version)) {
				
				File importXMLFile = new File(binaryDir, "import.xml");
				try {
					CorpusBuild cb = this.getCorpusBuild(this.getName().toUpperCase());
					
					BaseOldParameters.createEmptyParams(importXMLFile, this.getProjectDirectory().getName());
					BaseOldParameters bp = new BaseOldParameters(importXMLFile);
					bp.load();
					Element corpusElem = bp.getCorpusElement();
					corpusElem.setAttribute("cqpid", cb.getID());
					
					bp.author = this.getAuthor();
					bp.date = this.getCreationDate();
					bp.description = cb.getDescription();
					bp.version = "0.7";
					bp.rootDir = this.getSrcdir().getPath();
					bp.scriptFile = this.getImportModuleName() + "Loader.groovy";
					
					// Element elementCb = bp.addCorpusNode(cb.getName(), cb.getID(), cb.getLang(), this.getEncoding(), this.getAnnotate());
					bp.setAnnotationLang(this.getLang());
					bp.setDoAnnotation(this.getAnnotate());
					bp.setSkipTokenization(!this.getDoTokenizerStep());
					
					Element tokenizerElement = bp.getTokenizerElement(corpusElem);
					Element tmp = bp.root.getOwnerDocument().createElement("param");
					tmp.setAttribute("name", "whitespaces");
					tmp.setTextContent(this.getTokenizerParameter("whitespaces", ""));
					tokenizerElement.appendChild(tmp);
					tmp = bp.root.getOwnerDocument().createElement("param");
					tmp.setAttribute("name", "regPunct");
					tmp.setTextContent(this.getTokenizerParameter("regPunct", ""));
					tokenizerElement.appendChild(tmp);
					tmp = bp.root.getOwnerDocument().createElement("param");
					tmp.setAttribute("name", "punct_strong");
					tmp.setTextContent(this.getTokenizerParameter("punct_strong", ""));
					tokenizerElement.appendChild(tmp);
					tmp = bp.root.getOwnerDocument().createElement("param");
					tmp.setAttribute("name", "regElision");
					tmp.setTextContent(this.getTokenizerParameter("regElision", ""));
					tokenizerElement.appendChild(tmp);
					
					for (EditionDefinition ed2 : this.getEditionDefinitions()) {
						if ("default".equals(ed2.getName())) continue; // "default" is already declared
						bp.addEditionDefinition(corpusElem, ed2.getName(), "html", this.getImportModuleName());
					}
					EditionDefinition ed = this.getEditionDefinition("default");
					bp.setPageBreakElement(ed.getPageElement());
					bp.setWordsPerPage(ed.getWordsPerPage());
					
					bp.setTextualPlans(this.getTextualPlan("OutSideTextTagsAndKeepContent"), this.getTextualPlan("Note"), this.getTextualPlan("OutSideTextTags"), this.getTextualPlan("MileStones"));
					bp.setWordElement(this.getTokenizerWordElement());
					
					
					// write the <text> elements
					
					ArrayList<Text> texts = new ArrayList<Text>(this.getTexts());
					Collections.sort(texts, new Comparator<Text>() {

						@Override
						public int compare(Text o1, Text o2) {
							return o1.getName().compareTo(o2.getName());
						}
					});
					
					for (Text t : texts) {
						Element elementT = bp.addText(corpusElem, t.getName(), t.getSource());
						
						for (Edition e : t.getEditions()) {
							Element elementE = bp.addEdition(elementT, e.getName(), e.getHtmlDir().getPath(), "html");
							for (int i = 0; i < e.getNumPages(); i++) {
								Page p = e.getPage(i);
								bp.addPage(elementE, p.name, p.firstWordId);
							}
						}
					}
					
					// write the <preBuild>, <partition> and <subcorpus> elements
					for (CorpusBuild corpus : this.getChildren(CorpusBuild.class)) {
						corpus.getImportMetadata();
						
						BaseOldParameters.writeSubcorpusandPartitionsInOldBaseParameters(corpus, corpusElem, bp);
					}
					
					bp.getKeyValueParameters().put("lang", this.getLang());
					
					bp.save();
				}
				catch (Exception e) {
					Log.printStackTrace(e);
				}
			}
			
			String name = this.getName();
			if (rename != null && rename.length() > 0 // rename set
					&& !rename.equals(name)) { // and is different from current name
				
				// CQP stuff TODO implement a rename mecanism per engine
				File registryFile = new File(binaryDir, "registry/"+name.toLowerCase());
				File registryFile2 = new File(binaryDir, "registry/"+rename.toLowerCase());
				Log.finer("renaming $registryFile : "+registryFile.renameTo(registryFile2));
				String content = IOUtils.getText(registryFile2);
				content = content.replace(name, rename);
				content = content.replace(name.toLowerCase(), rename.toLowerCase());
				Log.finer("updating $importXMLFile : "+IOUtils.write(registryFile2, content));
				
				File dataFile = new File(binaryDir, "data/"+name);
				File dataFile2 = new File(binaryDir, "data/"+rename);
				Log.finer("renaming $dataFile : "+dataFile.renameTo(dataFile2));
				
				// TIGER stuff TODO implement a rename mecanism per engine
				File tigerDataFile = new File(binaryDir, "tiger/"+name);
				File tigerData2File = new File(binaryDir, "tiger/"+rename);
				Log.finer("renaming $tigerDataFile : "+tigerDataFile.renameTo(tigerData2File));
				
				// URS stuff
				File ecFile = new File(binaryDir, "analec/"+name+".ec");
				File ecvFile = new File(binaryDir, "analec/"+name+".ecv");
				File ecFile2 = new File(binaryDir, "analec/"+rename+".ec");
				File ecvFile2 = new File(binaryDir, "analec/"+rename+".ecv");
				Log.finer("renaming $ecFile : "+ecFile.renameTo(ecFile2));
				Log.finer("renaming $ecvFile : "+ecvFile.renameTo(ecvFile2));
				
				// TXM corpus stuff
				File importXMLFile = new File(binaryDir, "import.xml");
				if (importXMLFile.exists()) {
					content = IOUtils.getText(importXMLFile);
					content = content.replace("\""+name+"\"", "\""+rename+"\"");
					content = content.replace("\""+name.toLowerCase()+"\"", "\""+rename.toLowerCase()+"\"");
					Log.finer("updating $importXMLFile : "+IOUtils.write(importXMLFile, content));
				}
				
				File temporaryAnnotationsFile = new File(binaryDir, "temporary_annotations/"+name);
				File temporaryAnnotations2File = new File(binaryDir, "temporary_annotations/"+rename);
				Log.finer("renaming $temporaryAnnotationsFile : "+temporaryAnnotationsFile.renameTo(temporaryAnnotations2File));
				
				File cssFile = new File(binaryDir, "css/"+name+".css");
				File cssFile2 = new File(binaryDir, "css/"+rename+".css");
				Log.finer("renaming $cssFile : "+cssFile.renameTo(cssFile2));
				
				File htmlFile = new File(binaryDir, "HTML/"+name);
				File htmlFile2 = new File(binaryDir, "HTML/"+rename);
				Log.finer("renaming $htmlFile : "+htmlFile.renameTo(htmlFile2));
				
				File defaultCSSFile = new File(htmlFile, "default/css/"+name+".css");
				File defaultCSSFile2 = new File(htmlFile2, "default/css/"+rename+".css");
				Log.finer("renaming $defaultCSSFile : "+defaultCSSFile.renameTo(defaultCSSFile2));
				
				File txmFile = new File(binaryDir, "txm/"+name);
				File txmFile2 = new File(binaryDir, "txm/"+rename);
				Log.finer("renaming $txmFile : "+txmFile.renameTo(txmFile2));
				
				// patch name in settings
				File settingsDirectory = new File(binaryDir, ".settings");
				for (File prefFile : settingsDirectory.listFiles()) {
					Log.finer("updating $prefFile : "+IOUtils.write(prefFile, IOUtils.getText(prefFile).replace(name, rename)));
				}
				
				File projectSetting = new File(binaryDir, ".project");
				Log.finer("updating $projectSetting : "+IOUtils.write(projectSetting, IOUtils.getText(projectSetting).replace(name, rename)));
			}
			
			try {
				Zip.compress(binaryDir, exportzip, monitor, ignore);
			}
			catch (IOException e) {
				org.txm.utils.logger.Log.printStackTrace(e);
				return false;
			}
			
			if ("0.7.9".equals(version) || (rename != null && rename.length() > 0)) {
				if (!DeleteDir.deleteDirectory(binaryDir)) { // delete the temporary directory
					Log.warning("Warning: the temporary binary corpus could not be deleted: "+binaryDir);
				}
			}
			
			return true;
		}
		else {
			System.out.println(NLS.bind(TXMCoreMessages.binaryDirectoryP0NotFound, binaryDir));
			System.out.println(NLS.bind(TXMCoreMessages.parentDirectoryP0NotFound, exportzip.getParentFile()));
			
		}
		return false;
	}
	
	/**
	 * Gets the base directory.
	 *
	 * @return the base directory
	 */
	public File getProjectDirectory() {
		
		if (this.rcpProject == null || this.rcpProject.getLocation() == null) {
			return new File(Toolbox.workspace.getLocation(), "corpora/" + this.getUserName());
		}
		return this.rcpProject.getLocation().toFile();
	}
	
	public HashMap<String, Alignement> getLinks() {
		return links;
	}
	
	public ArrayList<Alignement> getLinksFrom(String from) {
		
		ArrayList<Alignement> rez = new ArrayList<>();
		for (String key : links.keySet()) {
			if (key.startsWith("#" + from + " ")) {//$NON-NLS-1$ //$NON-NLS-2$
				rez.add(links.get(key));
			}
		}
		return rez;
	}
	
	public ArrayList<Alignement> getLinksTo(String to) {
		ArrayList<Alignement> rez = new ArrayList<>();
		for (String key : links.keySet()) {
			if (key.endsWith(" #" + to)) { //$NON-NLS-1$
				rez.add(links.get(key));
			}
		}
		return rez;
	}
	
	public Alignement getLink(String from, String to) {
		return links.get("#" + from + " #" + to); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	@Override
	public boolean saveParameters() throws Exception {
		StringBuilder str = new StringBuilder();
		if (links != null) {
			for (String l : links.keySet()) {
				Alignement a = links.get(l);
				str.append("\t" + a.from + "#" + a.to + "#" + a.struct + "#" + a.level);
			}
			if (str.length() > 0) {
				this.setParameter(TBXPreferences.LINKS, str.substring(1));
			}
		}
		
		if (this.pSrcDirectory != null) {
			this.saveParameter(TBXPreferences.SOURCE, pSrcDirectory.getAbsolutePath());
		}
		
		return true;
	}
	
	@Override
	public boolean loadParameters() throws Exception {
		
		String path = this.getStringParameterValue(TBXPreferences.SOURCE);
		if (path != null && path.length() > 0) {
			this.pSrcDirectory = new File(path);
		}
		
		String linksString = this.getStringParameterValue(TBXPreferences.LINKS);
		if (linksString != null && !linksString.isEmpty()) {
			String split[] = linksString.split("\t");
			for (String align : split) {
				String split2[] = align.split("#", 4);
				new Alignement(this, split2[0], split2[1], split2[2], Integer.parseInt(split2[3]));
			}
		}
		
		// String xsltParamteresString = this.getStringParameterValue(TBXPreferences.FRONT_XSL_PARAMETERS);
		// if (xsltParamteresString != null && !xsltParamteresString.isEmpty()) {
		// String[] split = xsltParamteresString.split("\t");
		// for (String s: split) {
		// String split2 [] = s.split("=", 2);
		// pXsltParameters.put(split2[0], split2[1]);
		// }
		// }
		
		return true;
	}
		
	@Override
	public String getName() {
		return getUserName();
	}
	
	@Override
	public String getSimpleName() {
		return getUserName();
	}
	
	@Override
	public String getDetails() {
		return getUserName();
	}
	
	@Override
	public void clean() {
		
		Toolbox.notifyEngines(this, "before_clean"); // if an extension needs to free some files before
		
		if (rcpProject != null) {
			try {
				// Toolbox.getEngineManager(EngineType.SEARCH).getEngine("CQP").stop(); //$NON-NLS-1$
				
				rcpProject.close(new LogMonitor("Closing " + this));
				
				rcpProject.delete(true, true, new LogMonitor("Cleaning " + this));
				
				rcpProject.refreshLocal(IResource.DEPTH_INFINITE, new LogMonitor("Refresh workspace " + this));
				
				// Toolbox.getEngineManager(EngineType.SEARCH).getEngine("CQP").start(null); //$NON-NLS-1$
			}
			catch (Exception e) {
				Log.warning("Warning: error while deleting the corpus: "+e);
				Log.printStackTrace(e);
			}
		}
		
		Toolbox.notifyEngines(this, "clean");
		// DeleteDir.deleteDirectory(getProjectDirectory());
	}
	
	public void setName(String name) {
		this.setUserName(name);
	}
	
	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		return false;
	}
	
	@Override
	public String getResultType() {
		return "Project";
	}
	
	public String getImportModuleName() {
		return pImportModuleName;
	}
	
	/**
	 * Gets the encoding.
	 *
	 * @return the encoding
	 */
	public String getEncoding() {
		return pEncoding;
	}
	
	/**
	 * Gets the lang.
	 *
	 * @return the lang
	 */
	public String getLang() {
		return pLanguage;
	}
	
	/**
	 * Gets the srcdir.
	 *
	 * @return the srcdir
	 */
	public File getSrcdir() {
		return pSrcDirectory;
	}
	
	/**
	 * Gets the author.
	 *
	 * @return the author
	 */
	public String getAuthor() {
		return pAuthor;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.pDescription;
	}
	
	/**
	 * Sets the description.
	 *
	 */
	public void setDescription(String desc) {
		this.pDescription = desc;
	}
	
	/**
	 * @return the font
	 */
	public String getFont() {
		return pFont;
	}
	
	/**
	 * @param font the font to set
	 */
	public void setFont(String font) {
		this.pFont = font;
	}
	
	public void setLang(String lang) {
		this.pLanguage = lang;
	}
	
	public void setEncoding(String encoding) {
		this.pEncoding = encoding;
	}
	
	public void setSourceDirectory(String sourcePath) {
		this.pSrcDirectory = new File(sourcePath);
	}
	
	public void setXsltParameter(String name, String value) {
		this.getPreferencesScope().getNode("xsl").node(TBXPreferences.FRONT_XSL_PARAMETERS).put(name, value);
	}
	
	public HashMap<String, String> getXsltParameters() {
		Preferences params = this.getPreferencesScope().getNode("xsl").node(TBXPreferences.FRONT_XSL_PARAMETERS);
		HashMap<String, String> hparams = new HashMap<>();
		try {
			for (String k : params.childrenNames()) {
				hparams.put(k, params.get(k, null));
			}
		}
		catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hparams;
	}
	
	public void removeXsltParameter(String name) {
		this.getPreferencesScope().getNode("xsl").node(TBXPreferences.FRONT_XSL_PARAMETERS).remove(name);
	}
	
	public String getFrontXSL() {
		return this.getPreferencesScope().getNode("xsl").get(TBXPreferences.FRONT_XSL, null);
	}
	
	public void setXslt(String xslpath) {
		this.getPreferencesScope().getNode("xsl").put(TBXPreferences.FRONT_XSL, xslpath);
	}
	
	public void setAnnotate(boolean b) {
		this.pAnnotate = b;
	}
	
	public Boolean getAnnotate() {
		return pAnnotate;
	}
	
	public void setCleanAfterBuild(boolean clean) {
		this.pCleanAfterBuild = clean;
	}
	
	public Boolean getCleanAfterBuild() {
		return this.pCleanAfterBuild;
	}
	
	public void addTokenizerParameter(String name, String value) {
		IEclipsePreferences params = this.getPreferencesScope().getNode("Tokenizer");
		params.put(name, value);
	}
	
	public String getTokenizerParameter(String name, String defaultvalue) {
		IEclipsePreferences params = this.getPreferencesScope().getNode("Tokenizer");
		return params.get(name, defaultvalue);
	}
	
	public String[] getTextualPlans() {
		try {
			return this.getPreferencesScope().getNode("TextualPlans").childrenNames();
		}
		catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new String[0];
	}
	
	/**
	 * 
	 * @param name OutSideTextTags, OutSideTextTagsAndKeepContent, Note, MileStones
	 * @return empty String if the textual plan is not set
	 */
	public String getTextualPlan(String name) {
		return getTextualPlan(name, "");
	}
	
	/**
	 * 
	 * @param name OutSideTextTags, OutSideTextTagsAndKeepContent, Note, MileStones
	 * @param def the default value if not set
	 * @return empty String if the textual plan is not set
	 */
	public String getTextualPlan(String name, String def) {
		return this.getPreferencesScope().getNode("TextualPlans").get(name, def);
	}
	
	public void setTextualPlan(String name, String elements) {
		this.getPreferencesScope().getNode("TextualPlans").put(name, elements);
	}
	
	public String getTokenizerWordElement() {
		IEclipsePreferences params = this.getPreferencesScope().getNode("Tokenizer");
		return params.get("word_tags", "w");
	}
	
	public boolean getDoTokenizerStep() {
		IEclipsePreferences params = this.getPreferencesScope().getNode("Tokenizer");
		return params.getBoolean("doTokenizeStep", true);
	}
	
	public void setDoTokenizerStep(boolean doit) {
		addTokenizerParameter("doTokenizeStep", "" + doit); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	public EditionDefinition getEditionDefinition(String name) {
		return new EditionDefinition(this, name);
	}
	
	public ArrayList<EditionDefinition> getEditionDefinitions() {
		ArrayList<EditionDefinition> editions = new ArrayList<>();
		IEclipsePreferences editionNode = this.getPreferencesScope().getNode(EditionDefinition.class.getSimpleName());
		try {
			for (String editionName : editionNode.childrenNames()) {
				if (this.hasEditionDefinition(editionName)) {
					editions.add(getEditionDefinition(editionName));
				}
			}
		}
		catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return editions;
	}
	
	public CorpusCommandPreferences getCommandPreferences(String name) {
		return new CorpusCommandPreferences(this, name);
	}
	
	public ProjectScope getPreferencesScope() {
		return this.preferenceScope;
	}
	
	public CorpusBuild getCorpusBuild(String id) {
		return getCorpusBuild(id, null);
	}
	
	/**
	 * 
	 * @param id
	 * @param clazz the corpus corpusBuild class must be exactly the same (not inherited)
	 * @return
	 */
	public CorpusBuild getCorpusBuild(String id, Class<? extends CorpusBuild> clazz) {
		// System.out.println("CLASS="+clazz);
		List<CorpusBuild> builds = getCorpusBuilds();
		// System.out.println("BUILDS="+builds);
		for (CorpusBuild build : builds) {
			if (id == null) {
				if (clazz == null) {
					return build;
				}
				else if (build.getClass().equals(clazz)) {
					return build;
				}
			}
			else if (id.equals(build.getID())) {
				if (clazz == null) {
					return build;
				}
				else if (build.getClass().equals(clazz)) {
					return build;
				}
			}
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private List<CorpusBuild> getCorpusBuilds() {
		return getChildren(CorpusBuild.class);
	}
	
	public void setImportModuleName(String name) {
		this.pImportModuleName = name;
	}
	
	/**
	 * Checks if the edition specified by its name exists in the project.
	 * 
	 * @param name the edition to check
	 * 
	 * @return true if the edition exists otherwise false
	 */
	public boolean hasEditionDefinition(String name) {
		try {
			return getPreferencesScope().getNode("EditionDefinition").nodeExists(name);
		}
		catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Gets the project parameters root node path ("/project/[project name]/")
	 * 
	 * @return the project parameters root node path
	 */
	public String getParametersNodeRootPath() {
		return "/project/" + this.rcpProject.getName() + "/"; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	/**
	 * Gets a list of all the root corpus in the project.
	 * 
	 * @return a list of all the root corpus in the project if some exists otherwise an empty list
	 */
	public List<? extends CorpusBuild> getCorpora() {
		ArrayList<CorpusBuild> l = new ArrayList<>();
		for (TXMResult r : this.getChildren()) {
			if (r instanceof CorpusBuild) {
				l.add((CorpusBuild) r);
			}
		}
		return l;
	}
	
	public IEclipsePreferences getImportParameters() {
		return this.getPreferencesScope().getNode("import");
	}
	
	public boolean getDoUpdate() {
		return this.getImportParameters().getBoolean(ImportKeys.UPDATECORPUS, false);
	}
	
	public boolean getDoUpdateEdition() {
		return this.getImportParameters().getBoolean(ImportKeys.UPDATECORPUSEDITION, true);
	}
	
	public void setDoUpdate(boolean update, boolean updateedition) {
		this.getImportParameters().putBoolean(ImportKeys.UPDATECORPUS, update);
		this.getImportParameters().putBoolean(ImportKeys.UPDATECORPUSEDITION, updateedition);
	}
	
	public boolean getDoMultiThread() {
		return this.getImportParameters().getBoolean(ImportKeys.MULTITHREAD, false);
	}
	
	public void setDoMultiThread(boolean multithreaded) {
		this.getImportParameters().putBoolean(ImportKeys.MULTITHREAD, multithreaded);
	}
	
	/**
	 * 
	 * @param rcpProject
	 * @return true if a Project has been found
	 */
	public static Project loadProjectFromProjectScope(IProject rcpProject) {
		
		ArrayList<String> resultNodePaths = TXMPreferences.getAllResultsNodePaths("/project/" + rcpProject.getName() + "/"); //$NON-NLS-1$ //$NON-NLS-2$
		
		Log.fine("Toolbox.initialize(): loading project " + rcpProject.getName() + "..."); //$NON-NLS-1$ //$NON-NLS-2$
		Log.fine("Toolbox.initialize(): " + resultNodePaths.size() + " node(s) found in project " + rcpProject.getName() + "..."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		
		for (String parametersNodePath : resultNodePaths) {
			
			// FIXME: useless?
			// skip non-TXMResult preference node
			// if (TXMPreferences.getString(TXMPreferences.BUNDLE_ID, nodeQualifier).isEmpty()) {
			// continue;
			// }
			
			try {
				
				// Log.finest("Toolbox.initialize(): checking result type linked to node path " + parametersNodePath);
				Preferences node = TXMPreferences.preferencesRootNode.node(parametersNodePath);
				String className = node.get(TXMPreferences.CLASS, ""); //$NON-NLS-1$
				
				if (className == null || className.length() == 0) {
					Log.warning(NLS.bind("Warning: can not restore object with no class name set with path={0}.", parametersNodePath)); //$NON-NLS-1$
					node.removeNode();
					continue;
				}
				
				if (!className.equals(Project.class.getName())) { // only load the Project class
					continue;
				}
				
				Log.fine("Toolbox.initialize(): loading from result node path " + parametersNodePath);
				Log.finest("Toolbox.initialize(): parent_uuid = " + node.get(TXMPreferences.PARENT_PARAMETERS_NODE_PATH, ""));  //$NON-NLS-1$ //$NON-NLS-2$
				
				String bundleId = node.get(TXMPreferences.BUNDLE_ID, ""); //$NON-NLS-1$
				Log.finest("Toolbox.initialize(): bundle_id = " + bundleId); //$NON-NLS-1$
				
				Bundle bundle = Platform.getBundle(bundleId);
				if (bundle == null) {
					Log.finest("Warning: can not restore object with bundle name " + bundleId); //$NON-NLS-1$
					node.removeNode();
					continue;
				}
				
				Log.fine("Toolbox.initialize(): creating TXM corpus project " + rcpProject.getName() + "."); //$NON-NLS-1$ //$NON-NLS-2$
				Class<?> cl = bundle.loadClass(className);
				Constructor<?> cons = cl.getConstructor(String.class);
				TXMResult result = (TXMResult) cons.newInstance(parametersNodePath);
				
				// not an internal persistence (eg. corpus or partition)
				if (!result.isInternalPersistable()) {
					result.setUserPersistable(true);
				}
				
				return (Project) result; // only one Project.class loaded per RCP project
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * Append the line prefixed with the date
	 * 
	 * @param line
	 */
	public void appendToLogs(String line) {
		
		File logFile = new File(this.getProjectDirectory(), "LOGS");
		PrintWriter writer = null;
		try {
			writer = IOUtils.getWriter(logFile, true);
			writer.println(TXMResult.PRETTY_TIME_FORMAT.format(new Date()) + " - " + line);
			writer.close();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if ((writer != null)) writer.close();
		}
	}
	
	public void writeandReplaceLogs(String history) {
		
		File logFile = new File(this.getProjectDirectory(), "LOGS");
		IOUtils.write(logFile, history);
	}
	
	public String getLogs() throws IOException {
		
		File logFile = new File(this.getProjectDirectory(), "LOGS");
		if (logFile.exists()) {
			return IOUtils.getText(logFile);
		} else {
			return "<no logs yet>";
		}
	}
	
	/**
	 * Stores parameters of all the Project and all its results in nodes and saves them in the properties files.
	 * 
	 * @return true if the save has well been done otherwise false
	 */
	public boolean save() {
		try {
			ProjectScope scope = this.getPreferencesScope();
			
			autoSaveParametersFromAnnotations();
			saveParameters();
			
			List<TXMResult> results = this.getDeepChildren();
			for (int i = 0; i < results.size(); i++) {
				TXMResult r = results.get(i);
				if (r.mustBePersisted()) {
					r.autoSaveParametersFromAnnotations();
					r.saveParameters();
					// TXMPreferences.flush(r);
				}
			}
			
			scope.getNode("").flush(); //$NON-NLS-1$
			Toolbox.notifyEngines(this, "save"); // save things before exporting or else
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * @return
	 */
	public IProject getRCPProject() {
		return rcpProject;
	}
	
	@Override
	public String toString() {
		return this.getUserName();// +((this.rcpProject != null && this.rcpProject.getLocation() != null)?" ("+this.rcpProject.getLocation()+")":"");
	}
	
	/**
	 * Gets a list of all the built edition names.
	 * 
	 * @return a list of all the built edition names if some exists otherwise an empty list
	 */
	public List<String> getBuiltEditionNames() {
		ArrayList<String> availableEditions = new ArrayList<>();
		for (String name : this.getEditionNames()) {
			EditionDefinition ed = this.getEditionDefinition(name);
			if (ed.getBuildEdition()) {
				availableEditions.add(name);
			}
		}
		return availableEditions;
	}
}
