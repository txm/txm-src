// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-06-11 16:23:51 +0200 (mar., 11 juin 2013) $
// $LastChangedRevision: 2424 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.objects;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.utils.LogMonitor;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.zip.Zip;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * The Class Text.
 */
public class Text extends TXMResult {
	
	// /** The editions. */
	// HashMap<String, Edition> editions = new HashMap<String, Edition>();
	
	/** The source file. */
	@Parameter(key = TBXPreferences.SOURCE)
	File pSource;
	
	/** The XML-TXM file. */
	@Parameter(key = TBXPreferences.XMLTXM)
	File pXMLTXMFile;
	
	/**
	 * Instantiates a new text.
	 *
	 */
	public Text(Project project) {
		super(project);
		
		this.setVisible(false);
		this.synchronizedWithParent = true;
	}
	
	@Override
	public Project getParent() {
		return (Project) super.getParent();
	}
	
	/**
	 * Instantiates a new text.
	 *
	 */
	public Text(String parametersNodePath) {
		super(parametersNodePath);
		
		this.setVisible(false);
		this.synchronizedWithParent = true;
	}
	
	@Override
	public boolean isInternalPersistable() {
		return true;
	}
	
	@Deprecated
	public String[] getExportTXTExtensions() {
		return new String[] { "*.zip" };
	}
	
	/*
	 * Retro compatiblity to load Editions from a XML Element
	 */
	protected boolean _load(Element e) {
		Element src = (Element) e.getElementsByTagName("source").item(0); //$NON-NLS-1$
		pSource = new File(src.getAttribute("file")); //$NON-NLS-1$
		// sourcetype = src.getAttribute("type"); //$NON-NLS-1$
		try {
			String biblio = e.getAttribute("biblio"); //$NON-NLS-1$
			if (biblio != null && biblio.length() > 0) {
				Edition ed = new Edition(this);
				ed.setName("biblio");
				ed.setIndex(biblio);
			}
		}
		catch (Exception e2) {
		}
		
		try {
			String pdf = e.getAttribute("pdf"); //$NON-NLS-1$
			if (pdf != null && pdf.length() > 0) {
				Edition ed = new Edition(this);
				ed.setName("pdf");
				ed.setIndex(pdf);
			}
		}
		catch (Exception e2) {
		}
		
		this.setUserName(e.getAttribute("name")); //$NON-NLS-1$
		
		Element editionsnode = (Element) e.getElementsByTagName("editions").item(0); //$NON-NLS-1$
		NodeList editionList = editionsnode.getElementsByTagName("edition"); //$NON-NLS-1$
		
		// System.out.println(" build text "+name+" nb editions: "+editionList.getLength());
		for (int i = 0; i < editionList.getLength(); i++) {
			Element edition = (Element) editionList.item(i);
			Edition ed = new Edition(this);
			ed._load(edition);
		}
		
		return true;
	}
	
	/**
	 * Gets the editions.
	 *
	 * @return the editions
	 */
	@SuppressWarnings("unchecked")
	public List<Edition> getEditions() {
		return getChildren(Edition.class);
	}
	
	/**
	 * Gets the edition.
	 *
	 * @param name the name
	 * @return the edition
	 */
	public Edition getEdition(String name) {
		List<Edition> editions = this.getEditions();
		for (Edition ed : editions) {
			if (name.equals(ed.getName())) {
				return ed;
			}
		}
		return null;
	}
	
	// /**
	// * Adds the edition.
	// *
	// * @param name the name
	// * @param type the type
	// * @param index the index
	// * @return the edition
	// */
	// public Edition addEdition(String name, String type, File index) {
	// Document doc = getSelfElement().getOwnerDocument();
	// Element editionRoot = (Element) getSelfElement().getElementsByTagName(
	// "editions").item(0); //$NON-NLS-1$
	// Element editionElem = doc.createElement("edition"); //$NON-NLS-1$
	// editionElem.setAttribute("name", name); //$NON-NLS-1$
	// editionElem.setAttribute("type", type); //$NON-NLS-1$
	// editionElem.setAttribute("index", index.getAbsolutePath()); //$NON-NLS-1$
	// editionRoot.appendChild(editionElem);
	//
	// Edition e = new Edition(this, editionElem);
	// e.setPath(this.getPath() + e.name);
	// editions.put(name, e);
	// return e;
	// }
	//
	// /**
	// * Removes the edition.
	// *
	// * @param name the name
	// * @return the edition
	// */
	// public Edition removeEdition(String name) {
	// Edition e = editions.get(name);
	// if (e != null) {
	// Element editionRoot = (Element) getSelfElement().getElementsByTagName(
	// "editions").item(0); //$NON-NLS-1$
	// editionRoot.removeChild(e.getSelfElement());
	// editions.remove(name);
	// }
	// return e;
	// }
	
	/**
	 * Gets the source file.
	 *
	 * @return the source
	 */
	public File getSource() {
		return pSource;
	}
	
	public File getXMLTXMFile() {
		return pXMLTXMFile;
	}
	
	/**
	 * Gets the PDF edition page path.
	 *
	 * @return the source
	 */
	public String getPDFPath() {
		return getEdition("PDF").getIndex();
	}
	
	/**
	 * Gets the biblio edition page path.
	 *
	 * @return the biblio
	 */
	public String getBiblioPath() {
		return getEdition("biblio").getIndex();
	}
	
	@Override
	public boolean saveParameters() throws Exception {
		return true;
	}
	
	@Override
	public boolean loadParameters() throws Exception {
		
		// we may need to update pXMLTXMFile
		if (pXMLTXMFile == null || !pXMLTXMFile.exists()) { // try setting the parameter using the corpus directory
			File file = new File(this.getParent().getProjectDirectory(), "txm/" + this.getParent().getName() + "/" + this.getName() + ".xml");
			if (file.exists()) {
				pXMLTXMFile = file;
				this.saveParameter("xmltxm", pXMLTXMFile.getAbsolutePath());
			}
		}
		
		return true;
	}
		
	public void setName(String name) {
		this.setUserName(name);
	}
	
	public void setSourceFile(File src) {
		if (pSource == null || !pSource.getAbsolutePath().equals(src.getAbsolutePath())) {
			this.pSource = src;
			this.setDirty();
		}
	}
	
	public void setTXMFile(File txmFile) {
		this.pXMLTXMFile = txmFile;
	}
	
	@Override
	public String getName() {
		return getUserName();
	}
	
	@Override
	public String getSimpleName() {
		return getUserName();
	}
	
	@Override
	public String getDetails() {
		return getUserName();
	}
	
	@Override
	public void clean() {
		if (pXMLTXMFile != null && pXMLTXMFile.exists()) {
			pXMLTXMFile.delete();
		}
	}
	
	@Override
	public boolean canCompute() throws Exception {
		return true;// pSource.exists() || pTXMFile.exists();
	}
	
	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {
		return true;
	}
	
	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		
		List<Edition> editions = this.getEditions();
		File[] htmlFiles = new File[editions.size()];
		for (int i = 0 ; i < editions.size() ; i++) {
			htmlFiles[i] = editions.get(i).getHtmlDir();
		}
		
		// TODO export only the current text and not the whole corpus edition
//		ArrayList<File> htmlFiles = new ArrayList<File>();
//		
//		String[] adds = new String[] {"css", "img", "js"};
//		
//		for (Edition e : this.getEditions()) {
//			
//			File dir = e.getHtmlDir();
//			htmlFiles.add(dir);
//			
//			for (String add : adds) {
//				File f = new File(dir, add);
//				if (f.exists()) {
//					htmlFiles.add(f);
//				}
//			}
//			
//			for (int ipage = 0 ; ipage < e.getNumPages() ; ipage++) {
//				Page page = e.getPage(ipage);
//				if (page.getFile().exists()) {
//					htmlFiles.add(page.getFile());
//				}
//			}
//		}
		
		Zip.compress(htmlFiles, outfile, Zip.DEFAULT_LEVEL_COMPRESSION, new LogMonitor("Text export"), null);
		return false;
	}
	
	@Override
	public String getResultType() {
		return "Text";
	}
	
	@Override
	public String toString() {
		return getUserName() + " (file: " + pXMLTXMFile + ")";
	}
}
