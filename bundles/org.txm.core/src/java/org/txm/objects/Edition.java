// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-07-19 15:36:55 +0200 (Tue, 19 Jul 2016) $
// $LastChangedRevision: 3279 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.objects;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang.StringUtils;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * The Class Edition groups edition pages
 */
public class Edition extends TXMResult {
	
	/** The index page path. */
	@Parameter(key = TBXPreferences.INDEX)
	protected String pIndex;
	
	@Parameter(key = TBXPreferences.NAMES)
	protected ArrayList<String> pPageNames;
	
	@Parameter(key = "pages_wordids")
	protected ArrayList<String> pPageFirstWordIds;
	
	protected File defaultHtmlDir;
	
	/**
	 * Instantiates a new edition.
	 *
	 * @param text the text
	 */
	public Edition(Text text) {
		super(text);
		this.setVisible(false);
		this.synchronizedWithParent = true;
	}
	
	/**
	 * Instantiates a new edition.
	 *
	 * @param parametersNodePath the parametersNodePath String
	 */
	public Edition(String parametersNodePath) {
		super(parametersNodePath);
		this.setVisible(false);
		this.synchronizedWithParent = true;
	}
	
	@Override
	public boolean isInternalPersistable() {
		return true;
	}
	
	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public Text getText() {
		return (Text) getParent();
	}
	
	/**
	 * Gets the num pages.
	 *
	 * @return the num pages
	 */
	public Integer getNumPages() {
		return pPageNames.size();
	}
	
	/*
	 * Retro compatiblity to load Editions from a XML Element
	 */
	protected boolean _load(Element e) {
		// pType = e.getAttribute("type"); //$NON-NLS-1$
		pIndex = e.getAttribute("index"); //$NON-NLS-1$
		this.setUserName(e.getAttribute("name")); //$NON-NLS-1$
		
		// System.out.println(" build edition "+name);
		NodeList pagesList = e.getElementsByTagName("page"); //$NON-NLS-1$
		int numpages = pagesList.getLength();
		if (pPageNames == null) {
			pPageNames = new ArrayList<>();
		}
		else {
			pPageNames.clear();
		}
		
		if (pPageFirstWordIds == null) {
			pPageFirstWordIds = new ArrayList<>();
		}
		else {
			pPageFirstWordIds.clear();
		}
		
		for (int i = 0; i < numpages; i++) {
			Element p = (Element) pagesList.item(i);
			Page page = new Page(this, "", "", i);
			page._load(p);
			
			pPageNames.add(page.getName());
			pPageFirstWordIds.add(page.getWordId());
		}
		// loadMetadata();
		return false;
	}
	
	/**
	 * The Class PageComparator.
	 */
	class PageComparator implements Comparator<Page> {
		
		/*
		 * (non-Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(Page arg0, Page arg1) {
			return getInt(arg0.getWordId()) - getInt(arg1.getWordId());
		}
		
	}
	
	public int get(String id) {
		return pPageNames.indexOf(id);
	}
	
	public static final Pattern findNumbersPattern = Pattern.compile("([0-9]{1,10}+)"); //$NON-NLS-1$
	
	/**
	 * Gets the int.
	 *
	 * @param wordid the wordid
	 * @return the int
	 */
	private int getInt(String wordid) {
		
		int r = 0;
		try {
			Matcher m = findNumbersPattern.matcher(wordid);
			while (m.find()) {
				wordid = m.group(1);
			}
			r = Integer.parseInt(wordid);
		}
		catch (PatternSyntaxException pse) {
		}
		return r;
	}
	
	public void addPage(String id, String wordid) {
		this.pPageNames.add(id);
		this.pPageFirstWordIds.add(wordid);
	}
	
	
	public void resetPages() {
		this.pPageNames.clear();
		this.pPageFirstWordIds.clear();
	}
	
	/**
	 * Gets the page for word id.
	 *
	 * @param idx the idx
	 * @return the page for word id
	 */
	public Page getPageForWordId(String idx) {
		Log.fine("Page.getPageForWordId(): " + idx);
		int lastPage = 0;
		
		int i = 0;
		for (String p : pPageNames) {
			String s = pPageFirstWordIds.get(i);
			Log.fine(p + ": " + s + " >? " + idx);
			// if (getInt(s) > getInt(idx)) {
			if (isFirstGTthanSecond(s, idx) > 0) {
				Log.fine("page found: " + lastPage);
				return getPage(lastPage);
			}
			if (!("w_0".equals(s))) { // ignore the w_0 pages
				lastPage = i;
			}
			i++;
		}
		return getPage(lastPage);
	}
	
	public static int isFirstGTthanSecond(String s, String idx) {
		
		if (s == null) return -1;
		if (idx == null) return 1;
		
		Matcher m1 = findNumbersPattern.matcher(s);
		Matcher m2 = findNumbersPattern.matcher(idx);
		
		boolean m1ok = m1.find();
		boolean m2ok = m2.find();
		
		// boolean c = true;
		while (true) {
			if (m1ok && m2ok) {
				int i1 = Integer.parseInt(m1.group(1));
				int i2 = Integer.parseInt(m2.group(1));
				if (i1 - i2 != 0) return i1 - i2;
				
				m1ok = m1.find();
				m2ok = m2.find();
			}
			else if (m1ok) {
				return 1;
			}
			else if (m2ok) {
				return -1;
			}
			else {
				return 0;
			}
		}
	}
	
	/**
	 * Gets the index.
	 *
	 * @return the index
	 */
	public String getIndex() {
		return pIndex;
	}
	
	// Navigation
	/**
	 * Gets the first page.
	 *
	 * @return the first page
	 */
	public Page getFirstPage() {
		return getPage(0);
	}
	
	/**
	 * Gets the page.
	 *
	 * @param n the no of page to get
	 * @return the page
	 */
	public Page getPage(int n) {
		if (n >= 0 && pPageNames.size() > n) {
			String wId = "w_0"; // default word id
			if (pPageFirstWordIds.size() > n) {
				wId = pPageFirstWordIds.get(n);
			}
			return new Page(this, pPageNames.get(n), wId, n);
		}
		else {
			return null;
		}
	}
	
	/**
	 * Gets the last page.
	 *
	 * @return the last page
	 */
	public Page getLastPage() {
		return getPage(pPageNames.size() - 1);
	}
	
	/**
	 * Gets the next page.
	 *
	 * @param p the p
	 * @return the next page
	 */
	public Page getNextPage(Page p) {
		int idx = p.idx;
		if (idx < pPageNames.size() - 1)
			return getPage(idx + 1);
		return p;
	}
	
	/**
	 * Gets the previous page.
	 *
	 * @param p the p
	 * @return the previous page
	 */
	public Page getPreviousPage(Page p) {
		int idx = p.idx;
		if (idx > 0)
			return getPage(idx - 1);
		return p;
	}
	
	public File getHtmlDir() {
		
		if (defaultHtmlDir == null) {
			File binDir = getText().getProject().getProjectDirectory();
			
			// if the edition is not stored in the default corpus which usually has the same name as the project name
			String corpus = this.getStringParameterValue("corpus");
			if (corpus == null || corpus.length() == 0) {
				corpus = getText().getProject().getName();
			}
			
			defaultHtmlDir = new File(binDir, "HTML/" + corpus + "/" + getName()); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return defaultHtmlDir;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		try {
			return "Edition [name=" + getUserName() + " index=" + pIndex + ", pages=" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					+ pPageNames.size() + "]"; //$NON-NLS-1$
		}
		catch (Exception e) {
			return "Edition";
		}
	}
	
	public static void main(String[] args) {
		String[] strings = { "c_w_P1719_5_3", "c_w_P1719_5_6", "c_w_P1719_16_8", "c_w_P1719_32_3" };
		for (String s1 : strings) {
			for (String s2 : strings) {
				System.out.println(s1 + " - " + s2 + " = " + Edition.isFirstGTthanSecond(s1, s2));
			}
		}
	}
	
	@Override
	public boolean saveParameters() throws Exception {
		if (pPageNames != null) {
			this.saveParameter(TBXPreferences.NAMES, StringUtils.join(pPageNames, "\t"));
		}
		if (pPageFirstWordIds != null) {
			this.saveParameter("pages_wordids", StringUtils.join(pPageFirstWordIds, "\t"));
		}
		return true;
	}
	
	@Override
	public boolean loadParameters() throws Exception {
		
		String names = this.getStringParameterValue(TBXPreferences.NAMES);
		if (names != null && names.length() > 0) {
			pPageNames = new ArrayList<>(Arrays.asList(names.split("\t")));
		}
		else {
			pPageNames = new ArrayList<>();
		}
		
		String wordids = this.getStringParameterValue("pages_wordids");
		if (wordids != null && wordids.length() > 0) {
			pPageFirstWordIds = new ArrayList<>(Arrays.asList(wordids.split("\t")));
		}
		else {
			pPageFirstWordIds = new ArrayList<>();
		}
		
		return true;
	}
	
	@Override
	public String getName() {
		return getUserName();
	}
	
	@Override
	public String getSimpleName() {
		return getUserName();
	}
	
	@Override
	public String getDetails() {
		return getUserName();
	}
	
	@Override
	public void clean() {
		for (String name : this.pPageNames) {
			new File(this.getHtmlDir(), this.getText().getName() + "_" + name + ".html").delete();
		}
	}
	
	@Override
	public boolean canCompute() throws Exception {
		return true;
	}
	
	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {
		// // System.out.println("USE THE PROJECT IMPORTMODULE TO BUILD THE EDITION="+userName);
		// pages.clear();
		//
		// if (pages.size() != pPageNames.size()) {
		// for (int i = 0; i < pPageNames.size() && i < pPageFirstWordIds.size(); i++) {
		// pages.add(new Page(this, pPageNames.get(i), pPageFirstWordIds.get(i)));
		// }
		// }
		//
		return true;
	}
	
	@Deprecated
	public String[] getExportTXTExtensions() {
		return new String[] {};
	}
	
	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		return false;
	}
	
	@Override
	public String getResultType() {
		return "Edition";
	}
	
	public void setIndex(String url) {
		this.pIndex = url;
	}
	
	public void setName(String name) {
		this.setUserName(name);
	}
	
	public ArrayList<String> getPageNames() {
		return pPageNames;
	}
	
	public ArrayList<String> getPageFirstWordIDs() {
		return pPageFirstWordIds;
	}
	
	public ArrayList<String> setPageNames(ArrayList<String> newPageNames) {
		this.pPageNames = newPageNames;
		return pPageNames;
	}
	
	public ArrayList<String> setPageFirstWordIDs(ArrayList<String> newPageWordIDs) {
		this.pPageFirstWordIds = newPageWordIDs;
		return pPageFirstWordIds;
	}
	
	public Page getPageForName(String name) {
		int i = pPageNames.indexOf(name);
		return getPage(i);
	}
}
