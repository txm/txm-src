package org.txm.objects;

import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TBXPreferences;

/**
 * Wrapper for getting&amp;setting edition definition parameters
 * 
 * @author mdecorde
 *
 */
public class EditionDefinition {
	
	Preferences node;
	
	public EditionDefinition(Project project, String name) {
		this.node = project.getPreferencesScope().getNode(EditionDefinition.class.getSimpleName()).node(name);
		setName(name); // TODO use node.name() instead
	}
	
	public boolean delete() {
		try {
			node.removeNode();
			return true;
		}
		catch (BackingStoreException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public String getName() {
		return node.get(TBXPreferences.NAME, null); // should never return null
	}
	
	public int getWordsPerPage() {
		return node.getInt(TBXPreferences.EDITION_DEFINITION_WORDS_PER_PAGE, 1000);
	}
	
	public boolean getEnableCollapsibleMetadata() {
		return node.getBoolean(TBXPreferences.EDITION_DEFINITION_ENABLE_COLLAPSIBLE_METADATA, false);
	}
	
	public String getPageElement() {
		return node.get(TBXPreferences.EDITION_DEFINITION_PAGE_BREAK_ELEMENT, "pb");
	}
	
	public boolean getBuildEdition() {
		return node.getBoolean(TBXPreferences.EDITION_DEFINITION_BUILD, false);
	}
	
	public boolean getPaginateEdition() {
		return node.getBoolean(TBXPreferences.EDITION_DEFINITION_PAGINATE, true);
	}
	
	public String getImagesDirectory() {
		return node.get(TBXPreferences.EDITION_DEFINITION_IMAGES_DIRECTORY, null);
	}
	
	public void set(String key, String value) {
		node.put(key, value);
	}
	
	public String get(String key, String defaultValue) {
		return node.get(key, defaultValue);
	}
	
	public final String get(String key) {
		return get(key, "");
	}
	
	public void copyParametersTo(EditionDefinition anotherEditionDefinition) {
		anotherEditionDefinition.setPageBreakTag(this.getPageElement());
		anotherEditionDefinition.setWordsPerPage(this.getWordsPerPage());
		anotherEditionDefinition.setBuildEdition(this.getBuildEdition());
		anotherEditionDefinition.setImagesDirectory(this.getImagesDirectory());
	}
	
	public void setPageBreakTag(String page_break_tag) {
		if (page_break_tag == null) {
			node.remove(TBXPreferences.EDITION_DEFINITION_PAGE_BREAK_ELEMENT);
		}
		else {
			node.put(TBXPreferences.EDITION_DEFINITION_PAGE_BREAK_ELEMENT, page_break_tag);
		}
	}
	
	public void setEnableCollapsible(boolean enable) {
		node.putBoolean(TBXPreferences.EDITION_DEFINITION_ENABLE_COLLAPSIBLE_METADATA, enable);
	}
	
	public void setWordsPerPage(int words_per_page) {
		node.putInt(TBXPreferences.EDITION_DEFINITION_WORDS_PER_PAGE, words_per_page);
	}
	
	public void setBuildEdition(boolean build) {
		node.putBoolean(TBXPreferences.EDITION_DEFINITION_BUILD, build);
	}
	
	public void setName(String name) {
		if (name == null) {
			node.remove(TBXPreferences.NAME);
		}
		else {
			node.put(TBXPreferences.NAME, name);
		}
	}
	
	public void setImagesDirectory(String images_directory) {
		if (images_directory == null) {
			node.remove(TBXPreferences.EDITION_DEFINITION_IMAGES_DIRECTORY);
		}
		else {
			node.put(TBXPreferences.EDITION_DEFINITION_IMAGES_DIRECTORY, images_directory);
		}
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		try {
			for (String k : node.keys()) {
				buffer.append(k + "=" + node.get(k, "") + "\t");
			}
		}
		catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return buffer.toString();
	}
	
	public void setPaginateEdition(boolean paginate) {
		node.putBoolean(TBXPreferences.EDITION_DEFINITION_PAGINATE, paginate);
	}
}
