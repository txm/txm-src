// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.objects;

import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.w3c.dom.Element;

/**
 * The Class Page a fle to open in the Browser
 */
public class Page {

	/** The htmlfile. */
	protected File htmlfile;

	/** The edition. */
	protected Edition edition;

	/** The wordid. */
	protected String firstWordId;

	protected String name;

	protected int idx;

	/**
	 * Instantiates a new page.
	 *
	 * @param edition the edition
	 * @param name    the page name
	 * @param wordid  the page first word id
	 * @param i       the page position
	 * 
	 */
	public Page(Edition edition, String name, String wordid, int i) {

		this.edition = edition;
		this.name = name;
		this.firstWordId = wordid;
		this.htmlfile = new File(edition.getHtmlDir(), edition.getText().getName() + "_" + name + ".html");
		this.idx = i;
	}

	/**
	 * Retro-compatibility of import.xml file
	 * 
	 * @param e
	 */
	public void _load(Element e) {

		this.name = e.getAttribute("id");
		this.firstWordId = e.getAttribute("wordid");
	}

	/**
	 * Gets the word id.
	 *
	 * @return the word id
	 */
	public String getWordId() {

		return firstWordId;
	}

	/**
	 * Gets the edition.
	 *
	 * @return the edition
	 */
	public Edition getEdition() {
		return edition;
	}

	public boolean equals(Page p) {

		return p.idx == this.idx && p.name.equals(this.name) && p.htmlfile.equals(this.htmlfile);
	}

	/**
	 * Gets the file.
	 *
	 * @return the file
	 */
	public File getFile() {
		return this.htmlfile;
	}

	/**
	 * To url.
	 *
	 * @return the string
	 */
	public String toURL() {
		try {
			return this.htmlfile.toURI().toURL().toString();
		} catch (MalformedURLException e) {
			System.err.println(NLS.bind(TXMCoreMessages.malformedURLFileP0, htmlfile));
			return ""; //$NON-NLS-1$
		}
	}

	public Page getNextPage() {

		return this.edition.getNextPage(this);
	}

	public Page getPreviousPage() {

		return this.edition.getPreviousPage(this);
	}

	public Page getFirstPage() {

		return this.edition.getFirstPage();
	}

	public Page getLastPage() {

		return this.edition.getLastPage();
	}

	/**
	 * 
	 * @return the next page word id or null if this page is the last page
	 */
	public String getLastWordID() {

		Page nextPage = getNextPage();
		if (this.equals(nextPage)) {
			return nextPage.getWordId();
		}
		return null;
	}

	/**
	 * Fint the first word id in a HTML Edition page
	 *
	 * @param page the page
	 * @return the string
	 * @throws Exception the exception
	 */
	public static String findFirstWordIdInFile(File page) throws Exception {

		InputStream inputData;
		inputData = page.toURI().toURL().openStream();

		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLStreamReader parser = factory.createXMLStreamReader(inputData);

		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			if (event == XMLStreamConstants.START_ELEMENT) {
				String localname = parser.getLocalName();
				if (localname.matches("span")) { //$NON-NLS-1$
					String idattr = parser.getAttributeValue(null, "id"); //$NON-NLS-1$
					if (idattr != null) {
						if (idattr.startsWith("w")) { //$NON-NLS-1$
							parser.close();
							inputData.close();
							return idattr;
						}
					}
				}
			}
		}
		parser.close();
		inputData.close();
		return null;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {

		this.name = name;
	}

	/**
	 * Sets the wordid.
	 *
	 * @param wordid the new wordid
	 */
	public void setWordId(String wordid) {

		this.firstWordId = wordid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return "Page [name=" + name + ", htmlfile=" + htmlfile + ", firstWordId=" + firstWordId + "]"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	}

	public String getName() {

		return this.name;
	}
}
