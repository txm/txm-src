package org.txm.core.results;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * An annotation to identify The TXMResult parameters and automate some tasks ad: loading, saving, transfer, etc.
 * 
 * @author mdecorde
 *
 */
@Retention(RUNTIME)
@Target({ FIELD, METHOD, PARAMETER })
public @interface Parameter {
	
	/**
	 * To determine what kind of parameters has been changed between to computing/rendering and to do only computing, rendering or both.
	 */
	public final int COMPUTING = 0, RENDERING = 1, INTERNAL = 3;
	
	/**
	 * String representation of the types for logging purpose.
	 */
	public final String[] types = { "Computing", "Rendering", "Internal" }; //$NON-NLS-1$
	
	/**
	 * The parameter key name. If filled with some preference key, it can be used to automatically load or save parameters from and to the preferences nodes.
	 * Can also automate the copy of parameters from Widgets to result and from result to Widgets.
	 * 
	 * @return
	 */
	public String key() default "";
	
	/**
	 * The parameter electric mode. If true the result is computed as soon the parameter is updated.
	 * 
	 * @return
	 */
	public boolean electric() default true;
	
	/**
	 * To determine what kind of parameters has been changed between two computing/rendering and do only computing, rendering or both.
	 * 
	 * @return
	 */
	public int type() default Parameter.COMPUTING;
}
