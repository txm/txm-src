/**
 * 
 */
package org.txm.core.results;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * Wrapper for semantic purpose.
 * 
 * @author sjacquot, mdecorde
 *
 */
//FIXME: inherit from Properties may be better. but Properties only allow to store Strings
public class TXMParameters extends HashMap<String, Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6004847012085746970L;

	public TXMParameters(HashMap<String, Object> parameters) {
		super(parameters);
	}

	public TXMParameters() {
		super();
	}

	//FIXME: SJ: use super.toString()? -> MD: nope super.toString() don't break lines
	public void dump()	{
		Set<String> keys = this.keySet();
		Iterator<String> it = keys.iterator();
		while (it.hasNext()) {
		   Object key = it.next();
		   Object value = this.get(key);
		   System.out.println("TXMResultParameters.dump(): key = " + key + " / value = " + value);
		}
	}

	public String getString(String key) {
		Object v = get(key);
		if (v == null) {
			return null;
		} else {
			return v.toString();
		}
	}
	
	public Integer getInteger(String key) {
		Object v = get(key);
		if (v == null) {
			return 0;
		} else if (v instanceof Integer) {
			return (Integer)v;
		} else if (v instanceof String) {
			return Integer.parseInt(v.toString());
		} else if (v instanceof Float) {
			float f = (Float)v;
			return (int)f;
		} else if (v instanceof Double) {
			double f = (Double)v;
			return (int)f;
		}
		return 0;
	}
	
	public Float getFloat(String key) {
		Object v = get(key);
		if (v == null) {
			return 0.0f;
		} else if (v instanceof Float) {
			return (Float)v;
		} else if (v instanceof String) {
			return Float.parseFloat(v.toString());
		} else if (v instanceof Double) {
			double f = (Double)v;
			return (float)f;
		}
		return 0.0f;
	}

	public Boolean getBoolean(String key) {
		Object v = get(key);
		if (v == null) {
			return null;
		} else if (v instanceof Boolean) {
			return (Boolean)v;
		} else if (v instanceof String) {
			return Boolean.parseBoolean(v.toString());
		}
		return false;
	}

	public Double getDouble(String key) {
		Object v = get(key);
		if (v == null) {
			return 0.0d;
		} else if (v instanceof Double) {
			return (Double)v;
		} else if (v instanceof Float) {
			float f = (Float)v;
			return (double)f;
		} else if (v instanceof String) {
			return Double.parseDouble(v.toString());
		}
		
		return 0.0d;
	}
}
