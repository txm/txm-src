package org.txm.core.results;

/**
 * If registered the listener while be notified when the result is recomputed
 * 
 * @author mdecorde
 *
 */
public interface TXMResultListener {
	/**
	 * TODO rename to "updateFromResult"
	 * 
	 * @param update true if the result data part changed
	 * @throws Exception
	 */
	public void updateFromResult(boolean update) throws Exception;

	public boolean isDisposed();
}