/**
 * 
 */
package org.txm.core.results.comparators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.txm.core.results.TXMResult;

/**
 * Class to manage different comparators for a sorting type.
 * 
 * @author sjacquot
 *
 */
public class Comparators {

	/**
	 * Alphanumeric sorting on simple name.
	 */
	public final static int SIMPLE_NAME_ALPHANUM = 0;
	
	/**
	 * Node weight sorting.
	 */
	public final static int NODE_WEIGHT = 1;
	
	
	
	/**
	 * 
	 */
	private Comparators() {
		// no instantiation
	}

	/**
	 * Gets a comparator for TXM results according to the specified sorting type.
	 * @param sorting
	 * @return the Comparator
	 */
	public static Comparator<TXMResult> getComparator(int sorting)	{
		//FIXME: to complete and improve to sort on more than one key
		
		switch(sorting) {
		default:			
		// NODE_WEIGHT
		case 0:
			new Comparator<TXMResult>() {
				@Override
				public int compare(TXMResult o1, TXMResult o2) {
					return o1.getWeight() - o2.getWeight();
				}
			};
			break;
		// SIMPLE_NAME_ALPHANUM
		case 1:
			new Comparator<TXMResult>() {
				@Override
				public int compare(TXMResult o1, TXMResult o2) {
					return o1.getSimpleName().compareTo(o2.getSimpleName());
				}
			};
			break;
		}
		
		return null;
	}

	
	/**
	 * Sorts some TXM results according to the specified sorting type and reverse order.
	 * @param results
	 * @param sorting
	 * @param reversed
	 */
	public static void sort(ArrayList<TXMResult> results, boolean reversed, int... sorting)	{
		ArrayList<Comparator<TXMResult>> comparators = new ArrayList<Comparator<TXMResult>>();
		for(int i = 0; i < sorting.length; i++) {
			comparators.add(getComparator(sorting[i]));
		}
		Comparator<TXMResult> comparator = new MultipleComparators(comparators);
		if(reversed)	{
			comparator = Collections.reverseOrder(comparator);
		}
		Collections.sort(results, comparator);
	}
	

}
