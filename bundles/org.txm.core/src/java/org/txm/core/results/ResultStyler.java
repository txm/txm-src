package org.txm.core.results;

import java.util.HashMap;
import java.util.regex.Pattern;

public interface ResultStyler {
	/**
	 * get per category (ex: rows, cols) , per label/id/pattern, per style
	 */
	public HashMap<String, HashMap<Pattern, HashMap<String, String>>> getStyles();
	
	/**
	 * for messages
	 */
	public String getName();
}
