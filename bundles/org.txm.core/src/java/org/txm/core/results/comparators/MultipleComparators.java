/**
 * 
 */
package org.txm.core.results.comparators;

import java.util.ArrayList;
import java.util.Comparator;

import org.txm.core.results.TXMResult;

/**
 * Multiple chained TXM result comparisons management.
 * 
 * @author sjacquot
 *
 */
public class MultipleComparators implements Comparator<TXMResult> {
	
	/**
	 * Comparators to use.
	 */
	protected ArrayList<Comparator<TXMResult>> comparators;
	 
	/**
	 * Creates a comparator dedicated to multiple chained sorting.
	 * The comparison process sequentially calls the specified comparator comparison methods.
	 * 
	 * @param comparators
	 */
    public MultipleComparators(ArrayList<Comparator<TXMResult>> comparators) {
        this.comparators = comparators;
    }
 
	@Override
	public int compare(TXMResult o1, TXMResult o2) {
	   for(int i = 0; i < this.comparators.size(); i++) {
            int result = this.comparators.get(i).compare(o1, o2);
            if (result != 0) {
                return result;
            }
        }
        return 0;
	}
	
}
