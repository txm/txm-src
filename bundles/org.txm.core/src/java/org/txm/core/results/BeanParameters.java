package org.txm.core.results;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Utility class to read/write values to a bean
 * 
 * @author mdecorde
 *
 */
public class BeanParameters {
	
	/**
	 * Gets a member field according to its key.
	 * 
	 * Avoid calling multiple times this method and use getAllFields
	 * 
	 * @param key
	 * @return the field if exists otherwise null
	 */
	public static Field getField(Object bean, String key) {
		if (bean == null) throw new IllegalArgumentException("Object must not be null");
		if (key == null) throw new IllegalArgumentException("Parameter key must not be null");
		
		Field field = null;
		
		List<Field> fields = getAllFields(bean);
		
		for (Field f : fields) {
			Parameter parameter = f.getAnnotation(Parameter.class);
			if (parameter != null && parameter.key().equals(key)) {
				field = f;
				break;
			}
		}
		
		return field;
	}
	
	/**
	 * Returns all the member fields of the class instance (from all inherited classes).
	 * 
	 * @return the list of declared fields with a Parameter annotation
	 */
	public static LinkedHashMap<String, Field> getFieldsPerName(Object bean) {
		LinkedHashMap<String, Field> ret = new LinkedHashMap<>();
		for (Field f : getAllFields(bean)) {
			Parameter p = f.getAnnotation(Parameter.class);
			ret.put(p.key(), f);
		}
		return ret;
	}
	
	/**
	 * Returns all the member fields of the class instance (from all inherited classes).
	 * 
	 * @return the list of declared fields with a Parameter annotation
	 */
	public static List<Field> getAllFields(Object bean) {
		if (bean == null) throw new IllegalArgumentException("Object must not be null");
		
		List<Field> fields = new ArrayList<>();
		Class<?> clazz = bean.getClass();
		
		while (clazz != Object.class) {
			
			for (Field f : Arrays.asList(clazz.getDeclaredFields())) {
				Parameter parameter = f.getAnnotation(Parameter.class);
				if (parameter == null) {
					continue;
				}
				String key = parameter.key();
				if (key.isEmpty()) {
					continue; // no preference key defined
				}
				
				f.setAccessible(true);
				fields.add(f);
			}
			
			clazz = clazz.getSuperclass();
		}
		
		return fields;
	}
}
