package org.txm.core.results;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringBufferInputStream;
import java.io.StringReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Semaphore;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.osgi.util.NLS;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.prefs.BackingStoreException;
import org.txm.Toolbox;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.objects.Project;
import org.txm.objects.Workspace;
import org.txm.utils.LogMonitor;
import org.txm.utils.PatternUtils;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

/**
 * Root class of all TXM results. This class contains methods to get some
 * different versions of the name of the result and some additional computing
 * information. It also contains methods to manage a tree hierarchy of result
 * nodes.
 * 
 * A TXMResult always have a parent (except the root: @see Workspace).
 * 
 * To compute a result simply call the {@link #compute()} method. It contains all the life cycle steps of the result.
 * 
 * To set parameters, you can alternatively call the {@link #setParameter(String, Object)} methods or the setXXX methods. The setParameters methods are usually useful to transfer several parameters at once.
 * 
 * The class contains convenient methods to maintain the same logic between results :
 * - canCompute() : test if all necessary parameters are set
 * - isDirty(): return true if the result needs to be recomputed (if a parameter changed)
 * 
 * Groovy call sample :
 * 
 * <pre>
 * {@code
 * MyTXMResult result = new MyTXMResult(parent);
 * result.setParameters(["param1": 1, "param2":2]);
 * if (result.compute()) {
 * 	println "success"
 * } else {
 *  println "failure"
 * }
 * }
 * </pre>
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public abstract class TXMResult implements Cloneable, Comparable<TXMResult> {

	public static final DateFormat ID_TIME_FORMAT = new SimpleDateFormat("yyyyMMdd_HHmmssSSS"); //$NON-NLS-1$

	public static final DateFormat PRETTY_TIME_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); //$NON-NLS-1$

	public static final DateFormat PRETTY_LOCALIZED_TIME_FORMAT = new SimpleDateFormat("dd MMMM yyyy, HH'h'mm", Locale.getDefault()); //$NON-NLS-1$

	public static final String UUID_PREFIX = "txm_res_"; //$NON-NLS-1$

	public static final Pattern FILE_NAME_PATTERN = Pattern.compile("[^a-zA-Z0-9\\.-_]+"); //$NON-NLS-1$

	public static final String UNDERSCORE = "_"; //$NON-NLS-1$

	/**
	 * Results counter to create unique ids.
	 */
	private static int next = 0;

	/**
	 * The weight, essentially used for sorting purpose.
	 */
	protected int weight;

	/**
	 * the command this command to true when the result needs to be recomputed.
	 * like after updating parameters
	 */
	private boolean dirty = true;

	/**
	 * If a method changed the internal data without **recomputing** the result, the result must be marked "altered"
	 */
	private boolean altered = false;

	/**
	 * The result parent. Should not be null except for roots TXMResult.
	 */
	protected TXMResult parent;

	/**
	 * Children results.
	 */
	protected List<TXMResult> children;

	/**
	 * Command preferences node path.
	 */
	protected String commandPreferencesNodePath;

	/**
	 * Parameters node path. Concatenation of the Project scope path + the result uuid
	 */
	private String parametersNodePath;

	/**
	 * Thread used when calling compute().
	 */
	protected Thread computingThread;

	/**
	 * To manage the computing cancel with TreadDeath.
	 */
	// FIXME: SJ: became useless? 
	private Semaphore progressSemaphore = new Semaphore(1);

	/**
	 * To keep track of the parameters used for the last computing and to determine if the result is dirty.
	 * Also permits to optimize computing by testing if a specific parameter value has changed since last computing and skip or not some computing steps.
	 */
	protected ArrayList<HashMap<String, Object>> parametersHistory = null;

	/**
	 * User persistable state.
	 */
	@Parameter(key = TXMPreferences.PERSITABLE, type = Parameter.INTERNAL)
	private boolean userPersistable;

	/**
	 * The visibility state.
	 */
	@Parameter(key = TXMPreferences.VISIBLE, type = Parameter.INTERNAL)
	private Boolean visible;

	/**
	 * The user name (to rename a result).
	 */
	@Parameter(key = TXMPreferences.USER_NAME, type = Parameter.INTERNAL)
	private String userName;

	/**
	 * To store the simple name for unserialization and lazy loading.
	 * This string can be used, for example, to display some information in UI even if the result has not yet been recomputed.
	 */
	// TODO: SJ: we should do the same with getDetails() method
	@Parameter(key = TXMPreferences.LAZY_NAME, type = Parameter.INTERNAL)
	private String lazyName;

	/**
	 * Last computing date.
	 */
	@Parameter(key = TXMPreferences.LAST_COMPUTING_DATE, type = Parameter.INTERNAL)
	private Date lastComputingDate;

	/**
	 * Creation date.
	 */
	@Parameter(key = TXMPreferences.CREATION_DATE, type = Parameter.INTERNAL)
	private Date creationDate;

	/**
	 * If locked, the result is not updated when computed.
	 */
	@Parameter(key = TXMPreferences.LOCK, type = Parameter.INTERNAL)
	private Boolean locked = false;

	/**
	 * Current computing state.
	 * True if the result is in the process of computing (is in compute(,) method)
	 */
	private boolean computing = false;

	/**
	 * Synchronization state between this result and its parent.
	 * If true, computing the parent will always also computes this child.
	 */
	protected boolean synchronizedWithParent = false;

	protected LinkedHashMap<String, Object> temporaryData = new LinkedHashMap<String, Object>();


	/**
	 * Creates a new TXMResult, child of the specified parent.
	 * 
	 * @param parent
	 */
	public TXMResult(TXMResult parent) {
		this(null, parent);
	}

	/**
	 * Creates a new TXMResult with no parent.
	 * If a local preference node exists with parent_parameters_node_path value, the parent will be retrieved and this result will be added to it.
	 * 
	 * @param parametersNodePath
	 */
	public TXMResult(String parametersNodePath) {
		this(parametersNodePath, null);
	}

	/**
	 * Creates a new TXMResult, child of the specified parent.
	 * If the parameters node path is null, a new path is generated from the project root path ending by a new generated UUID.
	 * 
	 * @param parametersNodePath
	 * @param parent
	 */
	public TXMResult(String parametersNodePath, TXMResult parent) {
		if (parent == this) {
			throw new IllegalArgumentException("A result can not be parent of itself."); //$NON-NLS-1$
		}
		if (parent != null) {
			parent.addChild(this);
		}

		// new result
		if (parametersNodePath == null) {
			if (this.getProject() != null) {
				parametersNodePath = this.getProject().getParametersNodeRootPath();
			}
			else {
				parametersNodePath = ""; //$NON-NLS-1$
			}

			this.parametersNodePath = parametersNodePath + createUUID() + "_" + this.getClass().getSimpleName(); //$NON-NLS-1$ ;
		}
		// lazy result (persistence / import) or linked result ("send to" commands)
		else {
			this.parametersNodePath = parametersNodePath;
		}

		Log.finest("TXMResult.TXMResult(): parameters node path: " + this.parametersNodePath + "."); //$NON-NLS-1$ //$NON-NLS-2$

		this.weight = 0;

		this.children = new ArrayList<>(1);
		// this.children = Collections.synchronizedList(new ArrayList<TXMResult>()); // FIXME: SJ: can fix the concurrent exception if needed when accessing children from multiple threads

		this.commandPreferencesNodePath = FrameworkUtil.getBundle(getClass()).getSymbolicName();
		Log.finest("TXMResult.TXMResult(): command preferences node path: " + this.commandPreferencesNodePath + "."); //$NON-NLS-1$ //$NON-NLS-2$

		//this.visible = true;
		this.dirty = true;

		// set result as persistent if AUTO_PERSISTENCE_ENABLED preference is activated
		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.AUTO_PERSISTENCE_ENABLED)) {
			this.saveParameter(TXMPreferences.PERSITABLE, true);
		}

		// retrieving parent from UUID/node path
		String parentNodePath = this.getStringParameterValue(TXMPreferences.PARENT_PARAMETERS_NODE_PATH);



		// System.out.println("TXMResult.TXMResult(): parent UUID = " + parentUUID);

		// search for parent only if UUID != "ROOT"
		if (!("ROOT".equals(this.parametersNodePath)) && //$NON-NLS-1$
				parent == null &&
				this.parametersNodePath != null &&
				!parentNodePath.isEmpty()) {

			Log.finest("Searching parent with UUID/node path " + parentNodePath + "..."); //$NON-NLS-1$ //$NON-NLS-2$

			TXMResult retrievedParent = null;
			if (this.getProject() != null) {
				retrievedParent = TXMResult.getResult(this.getProject(), parentNodePath); // works only if parent and child are in the same project (which should be).
			}
			if (retrievedParent == null) { // the result parent is not in the same project ??? -> should not happen but maybe usefull later ?
				retrievedParent = TXMResult.getResult(parentNodePath);
			}


			if (retrievedParent != null) {
				Log.finest("Parent retrieved from UUID/node path: " + retrievedParent + "."); //$NON-NLS-1$ //$NON-NLS-2$
				retrievedParent.addChild(this);
			}
			else {
				Log.severe("Warning: no parent was found in project for result parameters UUID/node path=" + parametersNodePath + " and parent UUID/node path=" + parentNodePath + " - current parameters=" + this.dumpParameters()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				try {
					TBXPreferences.delete(this);
				}
				catch (Exception e) {

				}
				return;
			}
		} 

		//		else if (parent != null && !(parent.getParametersNodePath().equals(parentNodePath))) { // WHO IS RIGHT THE parent variable OR the parentnodepath preference ?
		//			Log.warning("This result parents node path ("+parentNodePath+") differs from given parents node path: "+parent);
		//			this.saveParameter(TXMPreferences.PARENT_PARAMETERS_NODE_PATH, parent.getParametersNodePath());
		//		}

		if (this.parent == null && !(this instanceof Workspace)) {
			Log.warning("No parent found for result="+this.getClass()+": "+this.dumpParameters()); //$NON-NLS-1$
			return;
		}

		// loads parameters from local result node, current command preferences or default command preferences
		try {
			this.autoLoadParametersFromAnnotations(); // auto fill from Parameter annotations
			this.loadParameters(); // subclasses manual settings

			try {
				if (this.creationDate == null) {
					this.creationDate = Calendar.getInstance().getTime();
					this.saveParameter(TBXPreferences.CREATION_DATE, ID_TIME_FORMAT.format(this.creationDate));
				}
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		catch (Exception e) {
			Log.severe("Fail to load the " + parametersNodePath + " result: " + e);
			Log.printStackTrace(e);
		}

		// Log
		if (this.parent == null) {
			Log.finest("Warning: the TXMResult of " + this.getClass() + " is attached to no parent. (uuid = " + this.parametersNodePath + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-1$
		}
	}

	/**
	 * Sets the user name.
	 * 
	 * @param name a string dedicated to be defined by the user 
	 */
	public void setUserName(String name) {
		this.userName = name;
	}

	/**
	 * @return true if the internal data has been modified
	 */
	public boolean isAltered() {
		return altered;
	}

	/**
	 * Mark the result as altered -> modifications are lost after a re-compute
	 */
	public void setAltered() {
		this.altered = true;
		// TODO MD if a result is set altered should its children be marked dirty ?
		//for (TXMResult c: children) c.setDirty();
	}


	public Object getTemporaryData(String key) {
		return temporaryData.get(key);
	}

	public Object setTemporaryData(String key, Object value) {
		return temporaryData.put(key, value);
	}





	/**
	 * Do something when project is closed
	 */
	public void onProjectClose() {

	}

	/**
	 * Do something when project is opened
	 */
	public void onProjectOpen() {

	}

	/**
	 * Do something when the workspace is closed = TXM is closing
	 */
	public void onWorkspaceClose() {

	}

	/**
	 * Locks/unlocks the result.
	 */
	public void setLocked(boolean state) {
		this.locked = state;

		if (this.locked && this.parent != null) {
			this.parent.setLocked(true);
		}
		if (!this.locked) {
			for (TXMResult child : getChildren()) {
				child.setUserPersistable(false);
			}
		}
	}

	/**
	 * @return true if the result is locked
	 */
	public boolean isLocked() {
		return this.locked;
	}

	/**
	 * Creates an UUID dedicated to preferences/parameters storing and to persistence of a result.
	 * 
	 * @return
	 */
	public static String createUUID() {
		return UUID_PREFIX + ID_TIME_FORMAT.format(new Date(System.currentTimeMillis())) + "_" + nextInt(); //$NON-NLS-1$
	}

	/**
	 * 
	 * @return the next integer in the current session of TXM
	 */
	private static synchronized String nextInt() {
		return String.format("%04d", next++); //$NON-NLS-1$
	}

	/**
	 * Checks if the result has at least one child.
	 * 
	 * @return
	 */
	public boolean hasChildren() {
		return this.children.size() > 0;
	}


	/**
	 * Gets the preferences node path of the result linked command.
	 * 
	 * @return
	 */
	public String getCommandPreferencesNodePath() {
		return this.commandPreferencesNodePath;
	}


	public String[] getCommandPreferencesKeys() throws BackingStoreException {
		return TXMPreferences.getCommandScopeKeys(this.commandPreferencesNodePath);
	}

	public String[] getDefaultPreferencesKeys() throws BackingStoreException {
		return TXMPreferences.getDefaultScopeKeys(this.commandPreferencesNodePath);
	}

	/**
	 * Gets a current parameter specified by its annotation "key".
	 * 
	 * @param key
	 * @return
	 */
	public Object getParameter(String key) {
		return getParameter(key, false);
	}

	/**
	 * Gets a current parameter specified by its annotation "key".
	 * 
	 * @param key
	 * @param propagateToParent if true ask the parents
	 * @return
	 */
	public Object getParameter(String key, boolean propagateToParent) {
		try {
			Field field = BeanParameters.getField(this, key);
			if (field != null) {
				field.setAccessible(true);
				return field.get(this);
			}
			else if (propagateToParent && this.parent != null) {
				return this.parent.getParameter(key, propagateToParent);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Stores the parameters of the specified types used during last computing in the specified HashMap.
	 * 
	 * @param parametersTypes
	 * @param lastParameters
	 * @throws Exception
	 */
	protected void updateLastParameters(ArrayList<Integer> parametersTypes, HashMap<String, Object> lastParameters) throws Exception {

		List<Field> fields = BeanParameters.getAllFields(this);

		for (Field f : fields) {
			Parameter parameter = f.getAnnotation(Parameter.class);
			if (parameter == null || !parametersTypes.contains(parameter.type())) {
				continue;
			}

			String name;
			if (!parameter.key().isEmpty()) {
				name = parameter.key();
			}
			else {
				name = f.getName();
			}

			f.setAccessible(true);
			lastParameters.put(name, f.get(this));
		}

		// if (lastParameters.isEmpty()) {
		// System.out.println("TXMResult.updateLastParameters() empty");
		// }

		if (this.parametersHistory == null) { // 
			this.parametersHistory = new ArrayList<HashMap<String,Object>>();
		}

		this.parametersHistory.add(lastParameters);

		// truncate the stack to the max count
		// FIXME: SJ: later, store this in a TBX preference
		if (this.parametersHistory.size() > 5) {
			this.parametersHistory.remove(0);
		}
	}

	/**
	 * Stores the last parameters of the specified types used during last computing.
	 * 
	 * @param parametersTypes
	 * @throws Exception
	 */
	protected void updateLastParameters(ArrayList<Integer> parametersTypes) throws Exception {
		this.updateLastParameters(parametersTypes, new HashMap<>());
	}

	/**
	 * Stores the last parameters used during last computing.
	 * 
	 * @throws Exception
	 */
	protected void updateLastParameters() throws Exception {
		ArrayList<Integer> parametersTypes = new ArrayList<>();
		parametersTypes.add(Parameter.COMPUTING);
		this.updateLastParameters(parametersTypes);
	}


	/**
	 * Removes all the parameters of the specified type from the last parameters history.
	 * 
	 * @param parameterType
	 */
	protected void clearLastParameters(int parameterType) {

		if (parametersHistory == null) return; // nothing to do

		List<Field> fields = BeanParameters.getAllFields(this);

		for (Field f : fields) {
			Parameter parameter = f.getAnnotation(Parameter.class);
			if (parameter == null || parameter.type() != parameterType) {
				continue;
			}

			String name;
			if (!parameter.key().isEmpty()) {
				name = parameter.key();
			}
			else {
				name = f.getName();
			}

			f.setAccessible(true);
			try {
				this.getLastParametersFromHistory().remove(name);
			}
			catch (Exception e) {
				// nothing to do if the stack is empty
			}
		}


		try {
			if (this.parametersHistory != null && !this.getLastParametersFromHistory().isEmpty()) {
				this.parametersHistory.remove(this.parametersHistory.size() - 1);
			}
		}
		catch (Exception e) {
			// nothing to do if the stack is empty
		}
	}


	/**
	 * Checks if a parameter value has changed since last computing.
	 * 
	 * @param key
	 * @return
	 */
	public boolean hasParameterChanged(String key) {

		if (parametersHistory == null) return true;

		return this.hasParameterChanged(key, this.getLastParametersFromHistory());
	}

	/**
	 * Checks if a parameter value has changed since last computing according to the specified external map.
	 * 
	 * @param key
	 * @param lastParameters
	 * @return
	 */
	public boolean hasParameterChanged(String key, HashMap<String, Object> lastParameters) {

		if (lastParameters == null) {
			return true;
		}

		if (key.isEmpty()) {
			return false;
		}

		Object lastValue = lastParameters.get(key);
		Object newValue = this.getParameter(key, true);
		if (lastValue == null) {
			if (newValue != null) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return !lastValue.equals(newValue);
		}
	}

	/**
	 * Checks if at least one parameter value of the specified parameter type has changed since last computing according to the specified external map.
	 * 
	 * @param lastParameters
	 * @param parameterType
	 * @return
	 */
	public boolean hasParametersChanged(HashMap<String, Object> lastParameters, int parameterType) throws Exception {

		// result has never been computed
		if (lastParameters == null) {
			return true;
		}

		boolean hasParameterChanged = false;

		List<Field> fields = BeanParameters.getAllFields(this);

		for (Field f : fields) {
			Parameter parameter = f.getAnnotation(Parameter.class);
			if (parameter == null || parameter.type() != parameterType) {
				continue;
			}
			String name;
			if (!parameter.key().isEmpty()) {
				name = parameter.key();
			}
			else {
				name = f.getName();
			}

			f.setAccessible(true); // to be able to test the field values

			hasParameterChanged = this.hasParameterChanged(name, lastParameters);

			if (hasParameterChanged) {
				Log.finest("TXMResult.hasParameterChanged(): " + this.getClass().getSimpleName() + ": parameter " + name + " has changed (type = " + parameterType + ").");
				break;
			}
		}

		return hasParameterChanged;
	}


	/**
	 * Checks if at least one computing parameter value has changed since last computing.
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean haveParametersChanged() throws Exception {
		if (parametersHistory == null) return true;

		return this.hasParametersChanged(this.getLastParametersFromHistory(), Parameter.COMPUTING);
	}


	/**
	 * Dumps the result current parameters, the command user preferences and the command default preferences linked to the result.
	 * 
	 * @return
	 */
	public String dumpPreferences() {

		StringBuilder str = new StringBuilder();

		if (TXMPreferences.resultScopeNodeExists(this)) {
			str.append("\nResult local node preferences\n"); //$NON-NLS-1$
			str.append(TXMPreferences.getKeysAndValues(this.getParametersNodePath()));
		}

		str.append("\nCommand preferences\n"); //$NON-NLS-1$
		str.append(TXMPreferences.getKeysAndValues(this.commandPreferencesNodePath));

		str.append("\nDefault command preferences\n"); //$NON-NLS-1$
		str.append(TXMPreferences.getKeysAndValues(DefaultScope.INSTANCE + "/" + this.commandPreferencesNodePath));

		return str.toString();
	}


	/**
	 * Dumps the result computing parameters (Parameter annotation).
	 * 
	 * @return
	 */
	public String dumpParameters() {
		return this.dumpParameters(Parameter.COMPUTING);
	}


	/**
	 * Dumps the result computing parameters (Parameter annotation).
	 * 
	 * @param parametersType
	 * @return
	 */
	public String dumpParameters(int parametersType) {

		StringBuilder str = new StringBuilder();

		str.append("Parameters (type = " + parametersType + " / " + Parameter.types[parametersType] + ")\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		List<Field> fields = BeanParameters.getAllFields(this);

		for (Field f : fields) {
			Parameter parameter = f.getAnnotation(Parameter.class);
			if (parameter == null || parameter.type() != parametersType) {
				continue;
			}
			f.setAccessible(true);

			String name;
			if (!parameter.key().isEmpty()) {
				name = parameter.key();
			}
			else {
				name = f.getName();
			}

			try {
				Object v = f.get(this);
				if (v != null) {
					str.append(name + " (" + v.getClass().getSimpleName() + ") = " + v + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
				}
				else {
					str.append(name + " = NULL \n"); //$NON-NLS-1$
				}
			}
			catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return str.toString();
	}



	// /**
	// * Updates the dirty state by comparing an old parameter with a new one.
	// *
	// * @param lastValue may be null
	// * @param newValue may be null
	// */
	// protected void updateDirty(Object lastValue, Object newValue) {
	// if(lastValue == null && newValue == null) {
	// return;
	// }
	// if (lastValue == null || !lastValue.equals(newValue)) {
	// Log.info("TXMResult.updateDirty(): " + this.getClass().getSimpleName() + ": setting dirty to true: last = "+ lastValue + " / new = " + newValue);
	// this.setDirty();
	// }
	// }

	/**
	 * Sets the dirty state to true if a parameter has changed since last computing.
	 * 
	 * @throws Exception
	 */
	private void updateDirtyFromHistory() throws Exception {
		//
		//// // result has never been computed
		//// if(this.getLastParametersFromHistory() == null) {
		//// this.dirty = true;
		//// return true;
		//// }
		////
		//// List<Field> fields = this.getAllFields();
		////
		//// for (Field f : fields) {
		//// Parameter parameter = f.getAnnotation(Parameter.class);
		//// if (parameter == null
		//// || parameter.type() == Parameter.INTERNAL
		//// //|| parameter.type() != Parameter.COMPUTING
		//// ) {
		//// continue;
		//// }
		//// String name;
		//// if (!parameter.key().isEmpty()) {
		//// name = parameter.key();
		//// }
		//// else {
		//// name = f.getName();
		//// }
		////
		//// f.setAccessible(true); // to be able to test the field values
		////
		//// Object previousValue = this.getLastParametersFromHistory().get(name);
		//// Object newValue = f.get(this);
		////
		//// // // FIXME: debug
		//// // Log.finest("TXMResult.isDirtyFromHistory(): checking parameter: " + name);
		////
		//// this.updateDirty(previousValue, newValue);
		//// if (this.dirty) {
		//// Log.finest("TXMResult.isDirtyFromHistory(): " + this.getClass().getSimpleName() + ": parameter " + name + " has changed.");
		//// return this.dirty; // no need to go further
		//// }
		//// }
		//// return this.dirty;
		//
		// this.dirty = this.hasParameterChanged();
		//// if (this.chartDirty) {
		//// // FIXME: debug
		//// Log.finest("ChartResult.isChartDirtyFromHistory(): parameter " + name + " has changed.");
		//// return this.chartDirty; // no need to go further
		//// }
		//// return this.dirty;
		// if(this.dirty) {
		// return true;
		// }
		// else {
		// return this.hasParameterChanged();
		// }

		if (this.haveParametersChanged()) {
			this.dirty = true;
		}
	}

	/**
	 * Gets the list of parameters used during last computing or null if the result has not been computed yet.
	 * 
	 * @return
	 */
	public HashMap<String, Object> getLastParametersFromHistory() {

		if (parametersHistory == null) return null;
		if (parametersHistory.size() == 0) return null;

		try {
			return this.parametersHistory.get(this.parametersHistory.size() - 1);
		}
		catch (Exception e) {
			return null;
		}
	}

	/**
	 * Gets the list of parameters used during the previous computing or null if the result has not been re-computed yet.
	 * 
	 * @return the map of parameters previously used or NULL
	 */
	public HashMap<String, Object> getPreviousParametersFromHistory() {

		if (parametersHistory == null) return null;
		if (parametersHistory.size() <= this.parametersHistory.size() - 2) return null;

		try {
			return this.parametersHistory.get(this.parametersHistory.size() - 2);
		}
		catch (Exception e) {
			return null;
		}
	}

	/**
	 * TODO seems to work but the TXMEditor.compute() method re-use the widgets values and the parametersHistory is appended with the 'new' parameters
	 * 
	 * @throws Exception
	 */
	public void revertParametersFromHistory() throws Exception {
		ArrayList<HashMap<String, Object>> history = getParametersHistory();
		if (history == null) return; // nothing to do

		if (history.size() < 2) return; // nothing to do

		history.remove(history.size() - 1);

		HashMap<String, Object> last = history.get(history.size() - 1);

		this.setParameters(last); // auto
	}


	/**
	 * Dumps the parameters history stack to the specified StringBuffer.
	 * 
	 * @param buffer
	 */
	public void dumpParametersHistory(StringBuffer buffer) {
		buffer.append("Parameters history stack:\n"); //$NON-NLS-1$
		if (this.parametersHistory == null) {
			buffer.append("Parameters history stack is undefined\n"); //$NON-NLS-1$
		}
		else {
			for (int i = this.parametersHistory.size() - 1; i >= 0; i--) {
				HashMap<String, Object> lastParameters = this.parametersHistory.get(i);
				buffer.append("Parameters (" + i + ") = " + lastParameters + "\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
		}
	}


	/**
	 * Gets the dirty state.
	 * 
	 * @return
	 */
	public boolean isDirty() {
		return this.dirty;
	}

	/**
	 * Marks the result as dirty so editors or others will know the TXMResult needs to be recomputed.
	 * This method is also recursively called on all the children branch to mark them as dirty.
	 */
	public void setDirty() {
		this.setDirty(true);
	}

	/**
	 * Sets the result dirty state so editors or others will know the TXMResult needs to be recomputed or not.
	 * This method is also recursively called on all the children branch sets the new dirty state.
	 * 
	 * @param propagate if true all children are set dirty as well
	 */
	public void setDirty(boolean propagate) {
		this.dirty = true;
		if (propagate) {
			for (int i = 0; i < this.children.size(); i++) {
				this.children.get(i).setDirty(propagate);
			}
		}
	}

	/**
	 * Gets the value of the specified key in parameters, local result node or
	 * default preferences nodes.
	 * 
	 * @param key
	 * @return value as int
	 */
	public int getIntParameterValue(String key) {
		return TXMPreferences.getInt(key, this, this.commandPreferencesNodePath);
	}

	/**
	 * Gets the value of the specified key in parameters, local result node or
	 * default preferences nodes.
	 * 
	 * @param key
	 * @return value as float
	 */
	public float getFloatParameterValue(String key) {
		return TXMPreferences.getFloat(key, this, this.commandPreferencesNodePath);
	}

	/**
	 * Gets the value of the specified key in parameters, local result node or
	 * default preferences nodes.
	 * 
	 * @param key
	 * @return value as double
	 */
	public double getDoubleParameterValue(String key) {
		return TXMPreferences.getDouble(key, this, this.commandPreferencesNodePath);
	}

	/**
	 * Gets the value of the specified key in parameters, local result node or
	 * default preferences nodes.
	 * 
	 * @param key
	 * @return value as boolean
	 */
	public boolean getBooleanParameterValue(String key) {
		return TXMPreferences.getBoolean(key, this, this.commandPreferencesNodePath);
	}

	/**
	 * Gets the value of the specified key in parameters, local result node or
	 * default preferences nodes.
	 * 
	 * @param key
	 * @return value as long
	 */
	public long getLongParameterValue(String key) {
		return TXMPreferences.getLong(key, this, this.commandPreferencesNodePath);
	}

	/**
	 * Gets the value of the specified key in parameters, local result node or
	 * default preferences nodes.
	 * 
	 * @param key
	 * @return value as String
	 */
	public String getStringParameterValue(String key) {
		return TXMPreferences.getString(key, this, this.commandPreferencesNodePath);
	}

	/**
	 * Gets the value of the specified key in parameters, local result node or
	 * default preferences nodes.
	 * 
	 * @param key
	 * @return value as Date
	 */
	public Date getDateParameterValue(String key) {
		return TXMPreferences.getDate(key, this, this.commandPreferencesNodePath);
	}

	/**
	 * Gets the value of the specified key in parameters, local result node or
	 * default preferences nodes.
	 * 
	 * @param key
	 * @return value as File
	 */
	public File getFileParameterValue(String key) {
		String v = TXMPreferences.getString(key, this, this.commandPreferencesNodePath);
		if (v != null && v.length() > 0) {
			return new File(v);
		}
		else {
			return null;
		}
	}


	/**
	 * Checks if the specified parameter exists.
	 * 
	 * @param key
	 * @return
	 */
	public boolean parameterExists(String key) {
		return TXMPreferences.keyExists(this.parametersNodePath, key);
	}

	/**
	 * Checks if a preference is empty (equals to "") in the command preference node.
	 * An empty preference can be used to disable a functionality for a kind of result
	 * (eg. A partition dimension bar chart does not have a legend so it can be used to not display the hide/show legend button in UI).
	 * 
	 * @param key
	 * @return
	 */
	public boolean isEmptyPreference(String key) {
		return TXMPreferences.isEmpty(this.commandPreferencesNodePath, key);
	}

	/**
	 * Stores the specified parameters pairs of key/value in a local node dedicated to the specified result. The node qualifier is generated by the <code>TXMResult.getUUID</code> method.
	 * 
	 * @param key
	 */
	public void saveParameter(String key, List<?> values) {
		this.saveParameter(key, StringUtils.join(values, UNDERSCORE));
	}


	/**
	 * Stores the specified parameters pairs of key/value in a local node dedicated to the specified result. The node qualifier is generated by the <code>TXMResult.getUUID</code> method.
	 * 
	 * @param key
	 * @param value
	 */
	public void saveParameter(String key, Object value) {
		// FIXME: debug
		// Log.info("TXMResult.saveParameter(): saving parameter " + key + " = " + value + " for " + this.getClass() + " (" + value.getClass() + ") to node.");

		TXMPreferences.putLocal(this, key, value);
	}


	/**
	 * Stores the result parameter fields declared by their @Parameter annotation in the persisted result node preferences.
	 * One have to implement saveParameters() to save parameters that are not of primitive types (e.g. Property, List, HashMap...)
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean autoSaveParametersFromAnnotations() throws Exception {

		Log.finest("TXMResult.autoSaveParametersFromAnnotations(): " + this.getClass().getSimpleName() + ": saving parameters to local node...");

		// internal data to save for unserialization
		if (this.getClass().isAnonymousClass()) {
			this.saveParameter(TXMPreferences.CLASS, this.getClass().getSuperclass().getName()); // avoid bug later if an anonymous class has been used.
		} else {
			this.saveParameter(TXMPreferences.CLASS, this.getClass().getName());
		}
		this.saveParameter(TXMPreferences.RESULT_PARAMETERS_NODE_PATH, this.parametersNodePath);
		this.saveParameter(TXMPreferences.BUNDLE_ID, FrameworkUtil.getBundle(getClass()).getSymbolicName());


		if (this.parent != null) {
			this.saveParameter(TXMPreferences.PARENT_PARAMETERS_NODE_PATH, this.parent.getParametersNodePath());
		}

		if (hasBeenComputedOnce()) {
			this.lazyName = this.getSimpleName();
		}

		List<Field> fields = BeanParameters.getAllFields(this);

		for (Field f : fields) {
			Parameter parameter = f.getAnnotation(Parameter.class);
			if (parameter == null) {
				continue;
			}
			String key = parameter.key();
			if (key.isEmpty()) {
				continue; // no preference key defined
			}

			if (f.getType().isAssignableFrom(int.class) || f.getType().isAssignableFrom(Integer.class) ||
					f.getType().isAssignableFrom(double.class) || f.getType().isAssignableFrom(Double.class) ||
					f.getType().isAssignableFrom(float.class) || f.getType().isAssignableFrom(Float.class) ||
					f.getType().isAssignableFrom(boolean.class) || f.getType().isAssignableFrom(Boolean.class) ||
					f.getType().isAssignableFrom(String.class) || f.getType().isAssignableFrom(File.class)

					// FIXME: do not pass a Serializable for now
					// || !f.getType().isAssignableFrom(Serializable.class)
					// || !Serializable.class.isInstance(f)

					) {


				f.setAccessible(true); // set accessible to test the field values
				try {
					Object value = f.get(this);
					if (value != null) {
						this.saveParameter(key, value);
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if (f.getType().isAssignableFrom(Date.class)) {
				f.setAccessible(true); // set accessible to test the field values
				try {
					Object value = f.get(this);
					if (value != null) {
						this.saveParameter(key, TXMResult.ID_TIME_FORMAT.format((Date) value));
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	/**
	 * Dedicated to stores the result parameters in the persisted result node preferences.
	 * One have to implement this method to save parameters that are not of primitive types (e.g. Property, List, HashMap...)
	 * Parameters of primitive types are automatically saved after a computing.
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract boolean saveParameters() throws Exception;

	/**
	 * calls saveParameters() on this and children if childrenAsWell is set to true
	 * 
	 * only children
	 * 
	 * @param childrenAsWell save the children parameters as well
	 * @return true if all saves were successful
	 * @throws Exception
	 */
	public boolean saveParameters(boolean childrenAsWell) throws Exception {

		boolean ret = true;

		if (this.mustBePersisted()) {
			ret = this.saveParameters();
			if (childrenAsWell) {
				for (TXMResult r : getChildren()) {
					ret = ret && r.saveParameters(childrenAsWell);
				}
			}
		}
		return ret;
	}

	// FIXME: SJ: this method should not call persist on children if childrenAsWell == false?
	public void persist(boolean childrenAsWell) {
		if (this.mustBePersisted()) {
			TXMPreferences.flush(this);
			for (TXMResult r : getChildren()) {
				r.persist(childrenAsWell);
			}
		}
	}

	/**
	 *  load or reload parameters from the preference store node
	 * @return
	 * @throws Exception
	 */
	public boolean autoLoadParametersFromAnnotations() throws Exception {
		return this.autoLoadParametersFromAnnotations(Parameter.COMPUTING)
				&& this.autoLoadParametersFromAnnotations(Parameter.INTERNAL)
				&& this.autoLoadParametersFromAnnotations(Parameter.RENDERING);
	}

	/**
	 * Fills the result parameter fields declared by their @Parameter annotation from the persisted result node or default node of the preferences.
	 * One have to implement loadParameters() to load parameters that are not of primitive types (e.g. Property, List, HashMap...)
	 * 
	 * @return
	 * @throws Exception
	 */
	protected boolean autoLoadParametersFromAnnotations(int parameterType) throws Exception {

		List<Field> fields = BeanParameters.getAllFields(this);

		for (Field f : fields) {

			Parameter parameter = f.getAnnotation(Parameter.class);
			if (parameter == null || parameter.type() != parameterType) {
				continue;
			}

			String key = parameter.key();

			if (key.isEmpty()) {
				continue; // no preference key defined for the parameter so it can't be filled with default value or result persistence value from preferences nodes
			}
			try {
				f.setAccessible(true); // set accessible to test the field values

				Object value = null;

				// only manage primitive types
				if (f.getType().isAssignableFrom(String.class)) {
					value = this.getStringParameterValue(key);
				}
				else if (f.getType().isAssignableFrom(double.class) || f.getType().isAssignableFrom(Double.class)) {
					value = this.getDoubleParameterValue(key);
				}
				else if (f.getType().isAssignableFrom(float.class) || f.getType().isAssignableFrom(Float.class)) {
					value = this.getFloatParameterValue(key);
				}
				else if (f.getType().isAssignableFrom(int.class) || f.getType().isAssignableFrom(Integer.class)) {
					value = this.getIntParameterValue(key);
				}
				else if (f.getType().isAssignableFrom(boolean.class) || f.getType().isAssignableFrom(Boolean.class)) {
					value = this.getBooleanParameterValue(key);
				}
				else if (f.getType().isAssignableFrom(Date.class)) {
					value = this.getDateParameterValue(key);
				}
				else if (f.getType().isAssignableFrom(File.class)) {
					value = this.getFileParameterValue(key);
				}

				// FIXME: test to automate other type creation as Property => problem here is that org.txm.core doesn't know the plug-in org.txm.searchengine.cqp.core
				// could be done using reflection. Get the field runtime class, then get and call the constructor
				// else if (f.getType().isAssignableFrom(Property.class)) {
				// value = new Property(this.getStringParameterValue(key), Corpus.getCorpus(this));
				// }
				// MD: nope should be done via adapter or extension point "org.txm.searchengine.cqp.core" contributes to
				// SJ: or better, when the abstraction of Property, etc. will be done, we could implement some extensions as in ChartCreator model to other engines, eg. SearchEngine, that will return
				// the right class here
				f.set(this, value);

				// FIXME: Debug
				Log.finest("TXMResult.autoLoadParametersFromAnnotations(): setting parameter " + key + " = " + value + "\t[" + this.getClass() + ", parameter type: " + parameterType +"]");

			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	/**
	 * Method dedicated to manually create and load non primitive parameters from strings stored in preference nodes in subclasses.
	 * This method is called by the constructor of the class TXMResult.
	 * The constructor also auto-loads the parameters that have a @Parameter annotation.
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract boolean loadParameters() throws Exception;

	/**
	 * Initializes the @Parameter class members objects using a map of key name + values
	 * 
	 * @param parameters
	 * @return true if all parameters have been set
	 */
	public final boolean setParameters(TXMParameters parameters) throws Exception {

		boolean r = true;

		for (String k : parameters.keySet()) {
			r &= setParameter(k, parameters.get(k), false);
		}
		return r;
	}

	/**
	 * Initializes the @Parameter class members objects
	 * 
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public final boolean setParameters(HashMap<String, Object> parameters) throws Exception {
		return setParameters(new TXMParameters(parameters));
	}

	/**
	 * Sets a parameter from its parameter annotation "key".
	 * 
	 * @param key
	 * @param value
	 * @return true if the parameter have been set
	 * @throws Exception
	 */
	public final boolean setParameter(String key, Object value) throws Exception {
		return setParameter(key, value, false);
	}

	/**
	 * Sets a parameter from its parameter annotation "key".
	 * 
	 * @param key
	 * @param value
	 * @param propagateToParent propagates the key=value to the parents if the result has no field=key
	 * 
	 * @return
	 * @throws Exception
	 */
	public final boolean setParameter(String key, Object value, boolean propagateToParent) throws Exception {

		Field targetField = BeanParameters.getField(this, key);

		if (targetField != null) {
			targetField.setAccessible(true);
			targetField.set(this, value);

			// Log
			String message = "TXMResult.setParameter(): " + this.getClass().getSimpleName() + ": setting parameter " + key + " = " + value; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			Log.finest(message);

		}
		else if (this.parent != null && propagateToParent) {
			this.parent.setParameter(key, value, propagateToParent);
		}

		return true;
	}

	/**
	 * Deletes the object from its parent, also deletes the children. The <code>TXMResult.clean()</code> methods of this result and children results are applied before the deletion.
	 * default behavior is not silent
	 * 
	 * @return
	 */
	public final boolean delete() {
		return delete(false);
	}

	/**
	 * Deletes the object from its parent, also deletes the children. The <code>TXMResult.clean()</code> methods of this result and children results are applied before the deletion.
	 * 
	 * @param silent no info message if silent is true
	 * @return
	 */
	public final boolean delete(boolean silent) {

		try {
			// FIXME: debug
			// System.err.println("TXMResult.delete()");

			// START WITH CHILDREN

			// remove children and clean resources
			while (this.children.size() > 0) {
				TXMResult c = this.children.get(0);
				c.delete(silent); // should call parent.removeResult(child)
				this.children.remove(c); // but should be done already...
			}
			this.children.clear();

			// THEN FINISH WITH THIS
			// delete the local node
			TXMPreferences.delete(this);

			// specific cleaning
			this.clean();

			// log
			if ((this.isVisible() && !silent) || Log.isLoggingFineLevel()) {
				Log.info(TXMCoreMessages.bind(TXMCoreMessages.info_P0P1Deleted, this.getResultType(), this.getSimpleName()));
			}

			// removes the parent only after the log since getSimpleName() can use it
			if (this.parent != null) {
				this.parent.removeChild(this);
			}

			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * Deletes all non persistent results and saves the results to persist.
	 */
	public static void deleteAllNonPersistentResults(TXMResult root) {

		List<TXMResult> todo = new ArrayList<>(root.getChildren());
		while (todo.size() > 0) {
			TXMResult r = null;
			r = todo.remove(0);
			todo.addAll(0, r.getChildren());
			if (r.mustBePersisted()) {
				TXMPreferences.flush(r);
			}
			else {
				Log.finest("TXMResult.deleteAllNonPersistentResults(): Deleting result " + r.getSimpleName() + " of type " + r.getClass().getSimpleName() + ".");
				TXMPreferences.delete(r);
			}
		}
	}


	/**
	 * Gets a child specified by its index in the children list.
	 * 
	 * @param index
	 * @return the child specified by its index if exists otherwise null
	 */
	public TXMResult getChild(int index) {
		try {
			return this.children.get(index);
		}
		catch (Exception e) {
			return null;
		}
	}

	/**
	 * Adds a child result to this result.
	 * If the child already have a parent, the child will be remove from its old parent children.
	 * 
	 * @param child
	 * @return true if the child has been added
	 */
	private boolean addChild(TXMResult child) {
		try {
			// remove from current parent if exists
			child.removeFromParent();

			child.setParent(this);
			if (!this.children.contains(child)) {
				return this.children.add(child);
			}
			// }
		}
		catch (Exception e) {
			// nothing to do
		}
		return false;
	}

	/**
	 * Inserts a child result in this result children list at the specified position.
	 * 
	 * @param index
	 * @param child
	 * @return
	 */
	private boolean addChild(int index, TXMResult child) {
		try {
			if (!this.children.contains(child)) {
				child.setParent(this);
				this.children.add(index, child);
				return true;
			}
		}
		catch (Exception e) {
		}
		return false;
	}

	/**
	 * Removes the specified child result.
	 * 
	 * @param child
	 * @return <code>true if the child has been removed, otherwise <code>false</code>
	 */
	private boolean removeChild(TXMResult child) {
		try {
			child.setParent(null);
			return this.children.remove(child);
		}
		catch (Exception e) {
		}
		return false;
	}

	/**
	 * Removes the child result specified by its index.
	 * 
	 * @param index
	 * @return <code>true if the child has been removed, otherwise <code>false</code>
	 */
	private boolean removeChild(int index) {
		try {
			return this.removeChild(this.children.get(index));
		}
		catch (Exception e) {

		}
		return false;
	}

	/**
	 * Removes the result from its parent: call the parent's removeResult method.
	 * 
	 * @return
	 */
	private boolean removeFromParent() {
		try {
			return this.parent.removeChild(this);
		}
		catch (Exception e) {

		}
		return false;
	}


	/**
	 * Checks if the result has at least one child visible.
	 * 
	 * @return true if the result has at least one child visible
	 */
	public boolean hasVisibleChild() {
		return !this.getChildren(true).isEmpty();
	}

	public boolean hasVisibleChildNotSynhchronized() {

		for (TXMResult c : this.getChildren(true)) {
			if (!c.mustBeSynchronizedWithParent()) return true; // one child visible and NOT synchronized is enough to stop heere
		}
		return false;
	}


	/**
	 * Gets all the children results (WARNING not a clone).
	 * 
	 * @return
	 */
	public List<TXMResult> getChildren() {

		return this.children;
	}

	/**
	 * Gets the children results.
	 * 
	 * @return
	 */
	public final List<TXMResult> getChildren(boolean onlyVisible) {

		return this.getChildren(null, onlyVisible);
	}

	/**
	 * Gets the children resultsby name and class
	 * 
	 * @param name filter using c.getName()
	 * @param clazz filter by c.getClass()
	 * @return list of results
	 */
	public final List<TXMResult> getChildrenByName(String name, Class<? extends TXMResult> clazz) {

		ArrayList<TXMResult> rez = new ArrayList<TXMResult>();
		for (TXMResult c : children) {
			if (c.toString().equals(name) && c.getClass().equals(clazz)) rez.add(c);
		}
		return rez;
	}

	/**
	 * Gets the children results by a path composed of the simple names separated with '/'
	 * 
	 * @param path
	 * @return list of results
	 */
	public final List<TXMResult> getChildrenByPath(String path) {

		return getChildrenByPath(splitString(path, '\\', '/'));
	}

	/**
	 * Gets the children results by a path composed of the simple names separated with '/'
	 * 
	 * @param path
	 * @return list of results
	 */
	public final List<TXMResult> getChildrenByPath(List<String> path) {

		return getChildrenByPath(path.toArray(new String[path.size()]));
	}

	public static String[] splitWithEscapeChars(String toSplit, String delim) {

		ArrayList<String> split = new ArrayList<String>();
		String regex = "(?<!\\\\)" + PatternUtils.quote(delim);

		for (String s : toSplit.split(regex)) {
			split.add(s);
		}

		return split.toArray(new String[split.size()]);
	}

	/**
	 * split by delimiter, espaceCharacter is removed
	 * 
	 * @param str
	 * @param escapeCharacter
	 * @param delimiter
	 * @return
	 */
	private static String[] splitString(String str, char escapeCharacter, char delimiter) {

		ArrayList<String> split = new ArrayList<String>();

		final StringBuilder sb = new StringBuilder();
		boolean isEscaped = false;
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c == escapeCharacter) {
				isEscaped = ! isEscaped;
				//sb.append(c);
			} else if (c == delimiter) {
				if (isEscaped) {
					sb.append(c);
					isEscaped = false;
				} else {
					split.add(sb.toString());
					sb.setLength(0);
				}
			} else {
				isEscaped = false;
				sb.append(c);
			}
		}
		split.add(sb.toString());

		return split.toArray(new String[split.size()]);
	}


	/**
	 * Gets the children results by a path composed of the simple names separated with '/'
	 * 
	 * @param path
	 * @return list of results
	 */
	public final List<TXMResult> getChildrenByPath(String[] path) {

		LinkedHashSet<TXMResult> rez = new LinkedHashSet<TXMResult>();
		if (path.length == 0) return new ArrayList<>(rez);

		// get direct children
		String name = path[0];
		List<TXMResult> tmp = this.getChildrenBySimpleName(name);


		if (path.length == 1) {
			rez.addAll(tmp);
		} else {
			// get deep children
			for (TXMResult sub : tmp) {
				String[] subpath = new String[path.length-1];
				System.arraycopy(path, 1, subpath, 0, path.length - 1);
				rez.addAll(sub.getChildrenByPath(subpath));
			}
		}

		return new ArrayList<>(rez);
	}

	/**
	 * Gets the children results by name
	 * 
	 * @param name filter using c.getName()
	 * @return list of results
	 */
	public final List<TXMResult> getChildrenByName(String name) {

		ArrayList<TXMResult> rez = new ArrayList<TXMResult>();
		for (TXMResult c : children) {
			if (c.getName().equals(name)) rez.add(c);
		}
		return rez;
	}

	/**
	 * Gets the children resultsby name and class
	 * 
	 * @param name filter using c.getSimpleName()
	 * @param clazz filter by c.getClass()
	 * @return list of results
	 */
	public final List<TXMResult> getChildrenBySimpleName(String name, Class<? extends TXMResult> clazz) {

		if (clazz == null) return getChildrenBySimpleName(name); // avoid NPE

		ArrayList<TXMResult> rez = new ArrayList<TXMResult>();
		for (TXMResult c : children) {
			if (c.toString().equals(name) && c.getClass().equals(clazz)) rez.add(c);
		}
		return rez;
	}

	/**
	 * Gets the children results by name
	 * 
	 * @param name filter using c.getSimpleName()
	 * @return list of results
	 */
	public final List<TXMResult> getChildrenBySimpleName(String name) {

		ArrayList<TXMResult> rez = new ArrayList<TXMResult>();
		for (TXMResult c : children) {
			if (c.getCurrentName().matches(name)) rez.add(c);
		}
		return rez;
	}

	/**
	 * Gets the children results specified by their class.
	 * 
	 * @param clazz filter by c.getClass()
	 * @return
	 */
	public final <T extends TXMResult> List<T> getChildren(Class<T> clazz) {

		return TXMResult.getNodes(this.children, clazz, false);
	}

	/**
	 * Gets the children results specified by their class.
	 * 
	 * @param clazz
	 * @return
	 */
	public final <T extends TXMResult> List<T> getChildren(Class<T> clazz, boolean onlyVisible) {
		return TXMResult.getNodes(this.children, clazz, onlyVisible);
	}

	/**
	 * Gets the first child result specified by its class.
	 * 
	 * @param clazz
	 * @return the first child if exists otherwise null
	 */
	public final <T extends TXMResult> T getFirstChild(Class<T> clazz) {
		List<T> children = this.getChildren(clazz, false);
		try {
			return children.get(0);
		}
		catch (Exception e) {
			return null;
		}
	}

	/**
	 * Deletes all the children
	 * 
	 */
	public final void deleteChildren() {
		deleteChildren(null);
	}

	/**
	 * Deletes the children of the specified class.
	 * 
	 * @param clazz
	 */
	public <T extends TXMResult> void deleteChildren(Class<T> clazz) {
		List<T> children = this.getChildren(clazz);
		for (int i = 0; i < children.size(); i++) {
			children.get(i).delete();
		}
	}



	/**
	 * Gets the children of all the branch of the specified node in a flat list according to their specified class.
	 * 
	 * @param parent
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected static <T extends TXMResult> List<T> getDeepChildren(TXMResult parent, Class<T> clazz) {

		List<T> results = new ArrayList<>();
		List<TXMResult> children = parent.getChildren();

		for (int i = 0; i < children.size(); i++) {
			TXMResult child = children.get(i);
			if (clazz == null || child.getClass().isAssignableFrom(clazz)) {
				results.add((T) child);
			}
			results.addAll(child.getDeepChildren(clazz));
		}
		return results;
	}

	/**
	 * Gets the children of all the branch in a flat list.
	 * 
	 * @return
	 */
	public <T extends TXMResult> List<T> getDeepChildren(Class<T> clazz) {
		return TXMResult.getDeepChildren(this, clazz);
	}

	/**
	 * Gets the children of all the branch in a flat list.
	 * 
	 * @return
	 */
	public static List<TXMResult> getDeepChildren(TXMResult result) {
		return TXMResult.getDeepChildren(result, null);
	}

	/**
	 * Gets the children of all the branch in a flat list.
	 * 
	 * @return
	 */
	public List<TXMResult> getDeepChildren() {
		return TXMResult.getDeepChildren(this);
	}

	/**
	 * Gets the TXMResult specified by its node path (UUID) in the children of the specified root result.
	 * Returns the root itself if the specified node path is equal to the root node path.
	 * 
	 * @param root
	 * @param nodePath
	 * @return
	 */
	public static TXMResult getResult(TXMResult root, String nodePath) {

		// returns the root itself if the specified node path is equal to the root node path
		if (root.getParametersNodePath().equals(nodePath)) {
			return root;
		}
		else {

			List<TXMResult> children = root.getChildren();

			// pass 1: to optimize, search in direct children
			for (int i = 0; i < children.size(); i++) {
				if (children.get(i).getParametersNodePath().equals(nodePath)) {
					return children.get(i);
				}
			}

			// pass 2: deep recursively search in all children
			for (int i = 0; i < children.size(); i++) {
				TXMResult result = getResult(children.get(i), nodePath);
				if (result != null) {
					return result;
				}
			}
		}

		return null; // not found
	}

	/**
	 * Gets the TXMResult specified by its node path (UUID) in the children of the Toolbox Workspace (root of the tree).
	 * 
	 * @param nodePath
	 * @return
	 */
	public static TXMResult getResult(String nodePath) {
		return getResult(Toolbox.workspace, nodePath);
	}

	/**
	 * Filters the nodes of the specified source nodes list according to by their class and their visibility state.
	 * 
	 * @param srcNodes
	 * @param clazz
	 * @param onlyVisible
	 * @return a new filtered nodes list
	 */
	@SuppressWarnings("unchecked")
	protected static <T extends TXMResult> List<T> getNodes(List<TXMResult> srcNodes, Class<T> clazz, boolean onlyVisible) {
		List<T> nodes = new ArrayList<>();
		for (TXMResult child : srcNodes) {
			try {
				if ((clazz == null ||
						child.getClass().asSubclass(clazz) != null)
						&& (onlyVisible == false || child.isVisible())) {
					nodes.add((T) child);
				}

				// add children of a non visible result
				else if (!child.isVisible() && child.hasChildren()) {
					List<T> subChidlren = child.getChildren(clazz, onlyVisible);
					nodes.addAll(subChidlren);
				}
			}
			catch (Exception e) {
				// nothing to do
			}
		}
		return nodes;
	}

	/**
	 * Gets the direct children count.
	 * 
	 * @return the direct children count
	 */
	public int getChildrenCount() {
		return this.children.size();
	}


	/**
	 * Gets the root parent of the branch.
	 * Returns the result itself if it's the root.
	 * 
	 * @return
	 */
	public TXMResult getRootParent() {
		TXMResult root;
		TXMResult parent = this;
		do {
			root = parent;
			parent = parent.getParent();
		}
		while (parent != null);

		return root;
	}


	/**
	 * Gets the first parent of the specified class, e.g. this.getFirstParent(Partition.class);
	 * 
	 * @param clazz the class of parent to look for
	 * @return the first parent of the specified class if exist otherwise null
	 */
	@SuppressWarnings("unchecked")
	public <T extends TXMResult> T getFirstParent(Class<T> clazz) {
		T node = null;
		TXMResult parent = this.parent;
		do {
			if (parent != null) {
				if (clazz == null || parent.getClass().equals(clazz) || clazz.isInstance(parent)) {
					node = (T) parent;
				}

				parent = parent.getParent();
			}
		}
		while (parent != null && node == null);

		return node;
	}


	/**
	 * Checks if the specified result is a child of this result.
	 * 
	 * @return true if the specified result is a child of this result otherwise false
	 */
	public boolean isChild(TXMResult child) {
		return this.getDeepChildren().contains(child);
	}

	/**
	 * Gets the sibling nodes of this node result.
	 * Returns an empty list if the node has no siblings.
	 * 
	 * @return a list of the sibling nodes of this result
	 */
	public List<TXMResult> getSiblings() {
		List<TXMResult> sibling = new ArrayList<>();
		if (this.parent != null) {
			sibling.addAll(this.parent.getChildren());
			sibling.remove(this);
		}
		return sibling;
	}

	/**
	 * Gets the sibling nodes of this node result, specified by their class.
	 * Returns an empty list if the node has no siblings of the specified class.
	 * 
	 * @param clazz the class of siblings to look for
	 * @return
	 */
	public <T extends TXMResult> List<T> getSiblings(Class<T> clazz) {
		List<T> sibling = new ArrayList<>();
		if (this.parent != null) {
			sibling.addAll(TXMResult.getNodes(this.parent.getChildren(), clazz, true));
			sibling.remove(this);
		}
		return sibling;
	}

	/**
	 * Checks if the result must be displayed.
	 * 
	 * @return true if the result must be displayed otherwise false
	 */
	public boolean isVisible() {
		return this.visible;
	}

	/**
	 * Checks if the parent of this result must be displayed.
	 * 
	 * @return true if the parent of this result must be displayed otherwise false
	 */
	public boolean isParentVisible() {
		return parent != null && (parent.isVisible() || parent.isInternalPersistable());
	}

	/**
	 * Sets the result visibility state.
	 * Dedicated to UI purposes.
	 * 
	 * @param visible
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * Sets the parent of this result.
	 * 
	 * @param parent the parent to set
	 */
	public void setParent(TXMResult parent) {
		this.parent = parent;
	}

	/**
	 * Gets the parent of this result.
	 * 
	 * @return the parent
	 */
	public TXMResult getParent() {
		return parent;
	}

	/**
	 * Gets the name of the result.
	 * 
	 * @return
	 */
	public abstract String getName();

	/**
	 * Gets the simple name of the result.
	 * Dedicated to be displayed in a small UI area.
	 * 
	 * @return
	 */
	public abstract String getSimpleName();

	/**
	 * Gets some details about how the result has been computed (eg. additional parameters, etc.).
	 * Dedicated to be displayed in a large UI area.
	 * 
	 * @return
	 */
	public abstract String getDetails();


	/**
	 * Gets a message indicating the start of computing.
	 * Dedicated to indicate the start of computing and, for example, the parameters used for the computing.
	 * 
	 * Warning: at this point we don't know if all parameters are set -> you should avoid NPE
	 * 
	 * @return a message indicating the start of computing
	 */
	// FIXME: SJ: this method must become abstract and implemented by subclasses
	public String getComputingStartMessage() {
		return NLS.bind("Computing {0}...", this.getClass().getSimpleName());
	}
	// public abstract String getComputingStartMessage();


	/**
	 * Gets a message indicating the computing is done.
	 * Dedicated to indicate the end of computing and, for example, some informations about the result (eg. : "12 matches found.", "No match found.", etc.).
	 * 
	 * @return a message indicating the computing is done
	 */
	// FIXME: SJ: this method must become abstract and implemented by subclasses
	public String getComputingDoneMessage() {
		return TXMCoreMessages.common_done;
	}
	// public abstract String getComputingDoneMessage();


	/**
	 * Gets the user name if exists otherwise, the simple name if not null, otherwise the lazy name.
	 * 
	 * @return the current name
	 */
	public String getCurrentName() {
		if (!this.userName.isEmpty()) {
			return this.userName;
		}
		// FIXME: SJ: original version
		// else if (this.lazyName != null && !this.lazyName.isEmpty()) {
		// return this.lazyName;
		// }
		// else {
		// return this.getSimpleName();
		// }
		// FIXME: SJ: new version to manage the lazy loading of child of an edited parent (e.g. LT => Specificities where the user modify the LT property)
		// Need to check that all works well then remove the above original version
		else if (this.getSimpleName() != null && !this.getSimpleName().isEmpty()) {
			return this.getSimpleName();
		}
		else {
			return this.lazyName;
		}

	}


	/**
	 * Convenience method to get a shared name when the result is empty (not yet computed).
	 * 
	 * @return the string "..."
	 */
	public String getEmptyName() {
		return "..."; //$NON-NLS-1$
	}


	/**
	 * Gets a string representing the result that can be used as a file name (eg. for exporting in file).
	 * 
	 * @return a valid filename without unexpected characters
	 */
	// FIXME: SJ: to discuss and/or to move in export layer
	// TODO: SJ: restore the matcher and compile the pattern only once
	public String getValidFileName() {
		try {
			// return FILE_NAME_PATTERN.matcher(this.getCurrentName()).replaceAll(UNDERSCORE);
			return this.getResultType() + "_" + this.getCurrentName().replaceAll("[|¤€§µ£°().,;:/?§%\"'*+\\-}\\]\\[{#~&@<>]", "").trim(); // avoid blanks in name
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
		return ""; //$NON-NLS-1$
	}

	/**
	 * Gets a string representing the concatenation of the simple names of the branch of this result.
	 * 
	 * @return the full path
	 */
	public String getFullPathSimpleName() {
		return getFullPathSimpleName(true);
	}

	/**
	 * Gets a string representing the concatenation of the simple names of the branch of this result.
	 * 
	 * @param showTypes display the result type as well
	 * @return the full path
	 */
	public String getFullPathSimpleName(boolean showTypes) {

		// fill the branch
		List<TXMResult> branch = new ArrayList<>();
		TXMResult node = this;
		do {
			branch.add(node);
			node = node.getParent();
			if (node == null || node.toString().equals("corpora")) break; //$NON-NLS-1$
		}
		while (node != null);

		// create the reversed string
		StringBuilder b = new StringBuilder();
		for (int i = branch.size() - 1; i >= 0; i--) {
			if (showTypes) {
				b.append("[\"" + branch.get(i).getClass().getSimpleName() + ": \"" + branch.get(i).getCurrentName() + "\"] / "); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			else {
				if (b.length() > 0) b.append("/"); //$NON-NLS-1$
				b.append(branch.get(i).getCurrentName().replace("/", "\\/")); //$NON-NLS-1$
			}
		}
		// remove last ] and /
		return b.toString();
	}

	/**
	 * Gets the weight of the node. Essentially used for sorting purpose.
	 * 
	 * @return the weight
	 */
	public int getWeight() {
		return weight;
	}

	/**
	 * Cleans and free some resources if needed. This methods is applied when
	 * calling <code>TXMResult.delete()</code>.
	 */
	public abstract void clean();

	@Override
	public TXMResult clone() {
		return clone(null, true);
	}

	public TXMResult clone(boolean all) {
		return clone(null, all);
	}

	/**
	 * recursive method to clone a result
	 * 
	 * @param newParent necessary for cloning children in the right clone parent
	 * @param all
	 * @return
	 */
	public TXMResult clone(TXMResult newParent, boolean all) {

		String newNodePath = this.getProject().getParametersNodeRootPath() + createUUID() + "_" + this.getClass().getSimpleName(); //$NON-NLS-1$
		TXMPreferences.cloneNode(this.getParametersNodePath(), newNodePath);

		if (newParent != null) { // change the cloned node parent preference
			TXMPreferences.put(newNodePath, TXMPreferences.PARENT_PARAMETERS_NODE_PATH, newParent.getParametersNodePath());
		}
		try {
			TXMResult clone = this.getClass().getDeclaredConstructor(String.class).newInstance(newNodePath);

			//System.out.println("CLONE HAS BEEN COMPUTED? " + clone.hasBeenComputedOnce());
			//System.out.println("CLONE IS DIRTY? " + clone.isDirty());

			if (all) {
				for (TXMResult child : this.children) {
					child.clone(clone, all);
				}
			}

			return clone;
		} catch (NoSuchMethodException e) {
			Log.fine("Error: could not clone result "+this+". No constructor TXMResult(String id) available.");
		}  catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public boolean importParameters(File parameters) {
		try {
			Properties props = new Properties();
			BufferedReader r = IOUtils.getReader(parameters);
			props.load(r);
			r.close();

			// some tests before...
			String className = props.getProperty(TXMPreferences.CLASS);
			if (className == null || className.length() == 0) {
				System.out.println("** Error: No class internal parameter set. Can't import the parameters.");
				return false;
			}

			if (this.getClass().getName().equals(className)) {
				for (Object p : props.keySet()) {
					String ps = p.toString();
					if (TXMPreferences.PARENT_PARAMETERS_NODE_PATH.equals(ps)) continue;
					if (TXMPreferences.RESULT_PARAMETERS_NODE_PATH.equals(ps)) continue;
					TXMPreferences.put(this.getParametersNodePath(), ps, props.getProperty(ps, ""));
				}
				this.autoLoadParametersFromAnnotations(); // update java parameters
				this.loadParameters();
			}
			else {
				System.out.println(NLS.bind("Cannot import parameters of {0} in a {1} result.", className, this.getClass().getName()));
				return false;
			}
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}

		return true;
	}


	/**
	 * import a result from a parameter file
	 * 
	 * @param parameters the parameters file
	 * @return the created result
	 */
	public TXMResult importResult(File parameters) {

		Properties props = new Properties();
		try {
			props.load(IOUtils.getReader(parameters));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return importResult(props);
	}

	/**
	 * import a result from a parameter file in Properties XML DTD format
	 * 
	 * @param xmlParameters the parameters file
	 * @return the created result
	 */
	public TXMResult importResultFromXML(File xmlParameters) {

		Properties props = new Properties();
		try {
			props.loadFromXML(IOUtils.getInput(xmlParameters));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return importResult(props);
	}

	/**
	 * import a result from a String in the Properties format
	 * 
	 * @param parameters the parameters as String
	 * @return the created result
	 */
	public TXMResult importResultFromXML(String parameters) {

		Properties props = new Properties();
		try {
			props.loadFromXML(new StringBufferInputStream(parameters));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return importResult(props);
	}

	/**
	 * import a result from a String in the Properties format
	 * 
	 * @param parameters the parameters as String
	 * @return the created result
	 */
	public TXMResult importResult(String parameters) {

		Properties props = new Properties();
		try {
			props.load(new StringReader(parameters));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return importResult(props);
	}

	/**
	 * import a result from a Properties map
	 * 
	 * @param props the parameters as Properties
	 * @return the created result
	 */
	public TXMResult importResult(Properties props) {
		return importResult(new HashMap(props));
	}

	/**
	 * import a result from a Properties map
	 * 
	 * e.g corpus.importResult(concordanceParametersMap)
	 * 
	 * @param map the parameters as Map
	 * @return the created result
	 */
	public TXMResult importResult(Map<String, String> map) {
		try {
			// some tests before...
			String className = map.get(TXMPreferences.CLASS);
			if (className == null || className.length() == 0) {
				System.out.println("** Error: No class internal parameter set.");
				return null;
			}

			String bundleId = map.get(TXMPreferences.BUNDLE_ID);
			if (bundleId == null || bundleId.length() == 0) {
				System.out.println("** Error: No bundle_id internal parameter set.");
				return null;
			}

			Bundle bundle = Platform.getBundle(bundleId);
			if (bundle == null) {
				Log.finest("** Error: can not restore object with bundle name " + bundleId); //$NON-NLS-1$
				return null;
			}

			// copy parameters
			String newNodePath = this.getProject().getParametersNodeRootPath() + createUUID() + "_" + className;
			TXMPreferences.put(newNodePath, TXMPreferences.PARENT_PARAMETERS_NODE_PATH, this.getParametersNodePath());
			for (Object p : map.keySet()) {
				String ps = p.toString();
				if (TXMPreferences.PARENT_PARAMETERS_NODE_PATH.equals(ps)) continue;
				if (TXMPreferences.RESULT_PARAMETERS_NODE_PATH.equals(ps)) continue;
				TXMPreferences.put(newNodePath, ps, map.get(ps));
			}

			// create instance
			Class<?> cl = bundle.loadClass(className);
			Constructor<?> cons = null;
			Class<?> clazz = this.getClass();
			while (cons == null) {
				if (clazz.getSimpleName().equals("TXMResult")) {
					break; // no luck
				}
				try {
					cons = cl.getConstructor(clazz);
				}
				catch (Exception ce) {
					clazz = clazz.getSuperclass();
				}
			}

			if (cons == null) {
				System.out.println(NLS.bind("Cannot import {0} in {1}", map, this));
			}
			else {
				cons = cl.getConstructor(String.class);
				TXMResult result = (TXMResult) cons.newInstance(newNodePath);
				System.out.println(NLS.bind("{0} imported", result));
				return result;
			}

		}
		catch (Exception e) {
			System.out.println(NLS.bind("Fail to import result from {0}: {1}", map, e));
			Log.printStackTrace(e);
		}

		return null;
	}

	/**
	 * Set the TXMResult parameters
	 * 
	 * @param parameters
	 *            the parameters to set
	 * @return true if no parameter read occured
	 * @throws CqiClientException
	 */
	// public abstract boolean setParameters(TXMParameters parameters) throws Exception;

	// this.parameters = parameters;
	// if (this.parameters == null) {
	// this.parameters = new TXMParameters();
	// }
	//
	// if (parameters != null) {
	// for (String k : parameters.keySet()) {
	// Object value = parameters.get(k);
	// System.out.println("PUT "+k+"="+value+" IN "+this.parameters);
	// if (value != null)
	// this.parameters.put(k, value);
	// else
	// this.parameters.remove(k); // because parameters.put(k, null) raise an
	// error
	// }
	// }
	// }

	/**
	 * set the Java object class parameter members and check if the Java object
	 * parameters are all set
	 * 
	 * @return true if parameters are all OK
	 */
	public abstract boolean canCompute() throws Exception;


	// FIXME: SJ: became useless 
	public void acquireSemaphore() {
		try {
			this.progressSemaphore.acquire();
		}
		catch (InterruptedException e) {
			Log.printStackTrace(e);
		}
	}

	/**
	 * Computes the result if:
	 * - it can be computed (checking if all mandatory parameters are set)
	 * - it is dirty (manually or from history, if a parameters value has changed since last computing)
	 * 
	 * @return
	 */
	public boolean compute(boolean deepComputing) throws InterruptedException {
		return this.compute(new LogMonitor("Computing " + this), deepComputing);
	}

	/**
	 * Computes the result if:
	 * - it can be computed (checking if all mandatory parameters are set)
	 * - it is dirty (manually or from history, if a parameters value has changed since last computing)
	 * 
	 * @return
	 */
	public boolean compute() throws InterruptedException {
		return this.compute(true);
	}


	/**
	 * Computes the result if:
	 * - it can be computed (checking if all mandatory parameters are set)
	 * - it is dirty (manually or from history, if a parameters value has changed since last computing)
	 * 
	 * @param monitor
	 * @return
	 */
	public boolean compute(IProgressMonitor monitor) throws InterruptedException {
		return this.compute(monitor, true);
	}


	/**
	 * Computes the result if:
	 * - it can be computed (checking if all mandatory parameters are set)
	 * - it is dirty (manually or from history, if a parameters value has changed since last computing)
	 * 
	 * @param monitor
	 * @param deepComputing
	 * @return
	 */
	public boolean compute(IProgressMonitor monitor, boolean deepComputing) throws InterruptedException {

		// FIXME: SJ: we may not override the current thread member if it already exists?
		this.computingThread = Thread.currentThread();

		// FIXME: SJ: Debug tests
		boolean debugSlowingProcess = false;
		int sleepDuration = 15000;

		// skip the process if the result is already in computing stage
		if (this.computing) {
			Log.finest(NLS.bind("--- TXMResult.compute(): {0}: The result is already computing, computing process aborted.", this.getClass().getSimpleName())); // $NON-NLS-1$
			return true;
		}

		this.computing = true;

		// no changes to do
		// FIXME: SJ: we need to do do some actions even if the object is locked, eg. updating parameters, saving parameters, etc.
		// FIXME: MD: nope the parameters must not move and will be saved in the current state
		if (hasBeenComputedOnce() && this.locked) {
			this.computing = false;
			return true;
		}


		Log.finest("*** TXMResult.compute(): " + this.getClass().getSimpleName() + ": starting computing process...");


		// convert the monitor into a sub-monitor that is dedicated only to the TXMResult.compute() method process
		SubMonitor mainSubMonitor = SubMonitor.convert(monitor, this.getComputingStartMessage(), 3);

		// FIXME: SJ: see if this skipComputing tests is still useful? is it possible to directly returns instead?
		boolean skipComputing = false;

		try {
			// compute the parent if needed
			if (this.parent != null && !(this.parent instanceof Project) && !(this.parent instanceof Workspace)
					&& !this.parent.isComputing()) {

				mainSubMonitor.setTaskName(NLS.bind(TXMCoreMessages.computingParentsOfP0, parent.getName()));

				Log.finest("TXMResult.compute(): " + this.getClass().getSimpleName() + ": starting parent (" + this.parent.getClass().getSimpleName() + ") computing process...");

				if (!this.parent.compute(mainSubMonitor.split(1).setWorkRemaining(100), false)) {
					Log.finest("TXMResult.compute(): " + this.getClass().getSimpleName() + ": failed to compute parent result.");
					this.computing = false;
					return false;
				}
			}
			else {
				mainSubMonitor.setWorkRemaining(2);
			}

			// update the dirty state from history if a parameter has changed since last computing
			// this.updateDirtyFromHistory();

			// check if the result is dirty
			if (!this.isDirty() && !haveParametersChanged()) {
				Log.finest("--- TXMResult.compute(): " + this.getClass().getSimpleName() + ": result parameters have not changed since last computing, computing skipped.");
				skipComputing = true;
			}

			if (!skipComputing) {

				// Debug
				Log.finest("+++ TXMResult.compute(): " + this.getClass().getSimpleName() + ": computing result of type " + this.getClass() + "...");

				// Computing requirements test
				if (!this.canCompute()) {
					Log.finest("TXMResult.compute(): " + this.getClass().getSimpleName() + ": can not compute, missing or wrong parameters or error with parent computing, computing aborted.");
					this.computing = false;
					return false;
				}


				try {
					// console log, only for visible result or if the log level is superior to info level
					if (!this.getComputingStartMessage().isEmpty() && (this.isVisible() || Log.isLoggingFineLevel())) {
						Log.info(this.getComputingStartMessage());
					}


					// monitor log
					// this.setTaskName(this.getComputingStartMessage());
					mainSubMonitor.subTask(this.getComputingStartMessage());

				}
				catch (Exception e) {
					e.printStackTrace();
					Log.severe("Error while retrieving the result computing start message."); //$NON-NLS-1$
				}

				// FIXME: SJ: debug tests
				if (debugSlowingProcess) {
					Thread.sleep(sleepDuration);
				}

				// Computing
				if (!this._compute(new TXMProgressMonitor(mainSubMonitor.split(1)))) {
					Log.finest("TXMResult.compute(): " + this.getClass().getSimpleName() + ": computing failed.");
					this.computing = false;
					return false;
				}
			}
			else {
				mainSubMonitor.setWorkRemaining(1);
			}

			// clear the lazy name, no more needed since the object has been computed and getSimpleName() can now work
			this.lazyName = null;

			// store last used parameters
			this.updateLastParameters(); // we're not skipping so add the parameters



			if (!skipComputing) { // this code block updates the internal variables of the TXMResult (dirtiness, dates, etc.)

				// store last used parameters
				// this.updateLastParameters();
				if (hasBeenComputedOnce() // don't update the computing date if the object has been lazy loaded
						|| this.lastComputingDate == null) { // set the update date if never computed once
					this.lastComputingDate = Calendar.getInstance().getTime(); // update this internal parameter before saving parameters
				}

				if (!this.autoSaveParametersFromAnnotations()) {
					Log.severe("TXMResult.compute(): " + this.getClass().getSimpleName() + ": failed to save parameters from annotations for " + this.getName() + ".");
				}
				if (!this.saveParameters()) {
					Log.severe("TXMResult.compute():" + this.getClass().getSimpleName() + ": failed to save parameters for " + this.getName() + ".");
				}

				// FIXME: SJ: need to find a way to flush file only if it's necessary. For example at this time, at start when all Edition, Text, etc. are recreated, all .prefs file are flushed but it
				// useless and slow
				// file persistence flush
				if (this.mustBePersisted()) {
					TXMPreferences.flush(this);
				}

				this.dirty = false; // the computing was successful, the result is no more dirty

				if (this.altered) {
					Log.info(NLS.bind("Warning {0} manual modifications have been lost.", this.getSimpleName()));
				}
				this.altered = false;
				Log.finest("TXMResult.compute(): " + this.getClass().getSimpleName() + ": computing of result type " + this.getClass() + " done.");

				// console log, only for visible result or if the log level is superior to info level
				if (!this.getComputingDoneMessage().isEmpty() && (this.isVisible() || Log.isLoggingFineLevel())) {
					Log.info(this.getComputingDoneMessage());
				}
			}

			// Children computing
			if ((!skipComputing
					|| this.altered) // FIXME: SJ: fix for computing LexicalTable children even if the LT is not dirty but only altered, need to define if it's a temporary fix or not
					// e.g. can't we use the dirty state rather than a new member "altered"?
					&& this.hasChildren()  // don't bother if there is not child
					&& this.parent != null) { // don't children cascade compute if parent == null (it is the Workspace (Tree root))

				// monitor log
				mainSubMonitor.setTaskName(TXMCoreMessages.bind(TXMCoreMessages.info_computingChildrenOfP0P1, this, this.getClass().getSimpleName()));

				// create a monitor for children computing loop
				SubMonitor loopMonitor = mainSubMonitor.split(1).setWorkRemaining(this.children.size());

				Log.finest("TXMResult.compute(): " + this.getClass().getSimpleName() + ": cascade computing of " + this.children.size() + " child(ren).");

				for (int i = 0; i < this.children.size(); i++) {

					TXMResult child = this.children.get(i);
					child.setDirty(); // set the children as dirty since the result has changed -> may be computed now or later

					// lazy loading test
					// only compute the child if it has already been computed one time ("loaded" one time)
					// or if this result always needs its children synchronized/computed to be consistent (e.g. Partition and children parts)
					if (!child.isComputing() && ((deepComputing && child.hasBeenComputedOnce()) || child.mustBeSynchronizedWithParent())) {
						child.compute(loopMonitor.split(1).setWorkRemaining(100), deepComputing);
					}
					else {
						loopMonitor.worked(1);
					}
				}
			}
		}
		// FIXME: SJ: in case of ThreadDeath we could for example call a method as Engine.interrupt(TXMResult)
		// so the engine can do some clean up, e.g. R destroying some objects, etc.
		// But we also have a TXMResult.cleanAfterComputeFailure() method that we could use
		catch (ThreadDeath | InterruptedException e) {

			// set result and its children as canceled
			this.resetComputingState();
			this.computing = false;

			// console log
			String resultType = this.getResultType();
			if (resultType.isEmpty()) {
				resultType = this.getClass().getSimpleName();
			}
			Log.info(NLS.bind(TXMCoreMessages.info_cancelingComputingOfP0P1, this.getName(), resultType));

			if (e instanceof ThreadDeath) {
				throw new InterruptedException("TXMResult computing thread stopped."); //$NON-NLS-1$
			}
			else {
				throw e;
			}
		}
		catch (Exception e) {
			// set result and its children as canceled
			this.resetComputingState();
			Log.warning(TXMCoreMessages.bind("** {0}.", e.getMessage()));
			Log.printStackTrace(e);
			this.computing = false;
			return false;
		}


		try {
			// TODO see if later we can display the end of compute message here (only if it is the main result computed
			// monitor log
			mainSubMonitor.subTask(this.getComputingDoneMessage());
		}
		catch (Exception e) {
			Log.severe("** Error while retrieving the result computing done message."); //$NON-NLS-1$
			Log.printStackTrace(e);
		}

		this.computing = false;

		mainSubMonitor.done();

		return true;
	}


	/**
	 * Sets the result and all its children as canceled (set them as dirty and set computing state as false).
	 * Resets the computing state.
	 * Dedicated to be called when computing has been killed.
	 * 
	 */
	public void resetComputingState() {
		if (this.computing) {

			// set back to dirty
			this.setDirty();
			this.computing = false;

			Log.finest(NLS.bind("Computing state of {0} ({1}) has been reset.", this.getName(), this.getClass().getSimpleName()));

			// deep reseting
			for (int i = 0; i < this.children.size(); i++) {
				this.children.get(i).resetComputingState();
			}

		}
	}


	/**
	 * This method is dedicated to be implemented by the inherited result classes to run the computing steps.
	 * Subclasses can monitor progress using the dedicated methods.
	 * At the start of this method call, the internal monitor have 100 ticks of remaining work to consume.
	 * 
	 * @return should return true if the result was computed, otherwise false
	 * 
	 * @throws Exception
	 */
	protected abstract boolean _compute(TXMProgressMonitor monitor) throws Exception;

	/***
	 * Checks if the result must be persisted according to the persistence global preference or the user persistable state.
	 * 
	 * @return true if the result as been internally marked as persistable (eg. Corpora, Partition) or if the user has marked the result
	 */
	public boolean mustBePersisted() {
		return this.isInternalPersistable() || this.userPersistable;
	}

	/**
	 * 
	 * extensions form is: '*.ext'
	 * 
	 * @return the array of extensions to show in the FileDialog SWT widget
	 */
	// FIXME: SJ: should be moved in an exporter extension
	@Deprecated
	public String[] getExportTXTExtensions() {
		return new String[] { "*.csv" }; //$NON-NLS-1$
	}

	/**
	 * 
	 * @return the array of extensions to show in the FileDialog SWT widget when data is imported
	 */
	// FIXME: SJ: should be moved in an importer/exporter extension
	@Deprecated
	public String[] getImportTXTExtensions() {
		return new String[] { "*.csv" }; //$NON-NLS-1$
	}


	// FIXME: SJ: became useless?
	public boolean tryAcquireSemaphore() {
		return this.progressSemaphore.tryAcquire();
	}

	// FIXME: SJ: became useless?
	public void releaseSemaphore() {
		this.progressSemaphore.release();
	}

	@Deprecated
	public boolean toTxt(File outfile, String encoding) throws Exception {
		return toTxt(outfile, encoding, "\t", ""); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * 
	 * 			
	 * 
	 *             wrapper to ensure the result is ready to be computed (not computing, computed)
	 * 
	 * @param outfile
	 * @param encoding
	 * @param colseparator
	 * @param txtseparator
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	//FIXME: SJ: should be moved in an exporter extension
	public final boolean toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		if (isComputing()) {
			Log.warning("The result is currently being computed. Please wait before exporting.");
			return false;
		}
		if (!hasBeenComputedOnce()) {
			Log.warning("The result was not computed. Please compute the result before exporting.");
			return false;
		}
		return _toTxt(outfile, encoding, colseparator, txtseparator);
	}

	/**
	 * 
	 *             TXMResults must implement this method to be exported.
	 * 
	 * @param outfile
	 * @param encoding
	 * @param colseparator
	 * @param txtseparator
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	//FIXME: SJ: should be moved in an exporter extension
	protected abstract boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception;



	// /**
	// * Increments the progress of the specified amount of work and displays a message.
	// *
	// * @param amount
	// * @param message
	// */
	// public void worked(int amount, String message) {
	// if (monitor != null) {
	// monitor.worked(amount);
	// this.subTask(message);
	// }
	// Log.fine(message);
	// }


	// /**
	// * Checks if the monitor is canceled.
	// *
	// * @return true if the monitor has been canceled by the user
	// */
	// @Override
	// public boolean isCanceled() {
	// if (monitor != null) {
	// return monitor.isCanceled();
	// }
	// return false;
	// }
	//
	//
	// @Override
	// public void beginTask(String name, int totalWork) {
	// if (monitor != null) {
	// monitor.beginTask(name, totalWork);
	// }
	// Log.fine(name);
	// }
	//
	//
	// @Override
	// public void internalWorked(double work) {
	// if (monitor != null) {
	// monitor.internalWorked(work);
	// }
	// }
	//
	// @Override
	// public void subTask(String name) {
	// if (monitor != null) {
	// monitor.subTask(name);
	// }
	// Log.fine(name);
	// }



	//
	// /**
	// * You must set the current monitor to be available to manage the process
	// * progress
	// *
	// * @param monitor
	// * can be null
	// */
	// public boolean setCurrentMonitor(SubMonitor monitor) {
	// if (monitor == null) {
	// return false;
	// }
	// if (monitor.isCanceled()) {
	// return false;
	// }
	// this.monitor = monitor;
	// return true;
	// }
	//

	/**
	 * Gets the user defined name.
	 * 
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Gets the result type. Should be localized (eg. CA => AFC, Word cloud => Nuage de mots).
	 * This method can be used for logging or UI.
	 * This method can also be used to sort the result by type (alphanumeric order) according to the current language.
	 * 
	 * @return an intended localized result type
	 */
	public abstract String getResultType();


	@Override
	public int compareTo(TXMResult o) {
		// Compare node weights
		return Integer.compare(this.weight, o.weight);
	}

	/**
	 * 
	 * @param e1
	 * @param e2
	 * @return
	 */
	public static int compare(TXMResult e1, TXMResult e2) {
		if (e1 == null && e2 == null) return 0;
		if (e1 == null) return 1;

		return e1.getParametersNodePath().compareTo(e2.getParametersNodePath());
	}

	/**
	 * Gets a boolean state defining if the result has already been computed at least once or not.
	 * 
	 * true if the result has already been lazy LOADED...........
	 * 
	 * @return
	 */
	public boolean hasBeenComputedOnce() {
		return parametersHistory != null;
	}


	/**
	 * Gets the internal persistable state.
	 * 
	 * @return the persistable
	 */
	public boolean isInternalPersistable() {
		return false;
	}

	/**
	 * Gets the lazy name.
	 * Since some result names need computed data, this name is dedicated to display the result name even if it has not been recomputed after being loaded.
	 * 
	 * @return the lazyName
	 */
	public String getLazyName() {
		return lazyName;
	}

	/**
	 * Gets the user defined persistable state of the result.
	 * 
	 * @return the state
	 */
	public boolean isUserPersistable() {
		return userPersistable;
	}

	/**
	 * Sets the user defined persistable state of the result.
	 * 
	 * When making a result persistent, since a result is always created from its parent(s) and need them to be reconstructed, the state of all the parents in the branch will be set as persistent.
	 * When making a result non persistent, its children will not be able to be reconstructed, so they are recursively set as non persistent.
	 * Also, since some kind of TXM shortcut commands create some hidden results (e.g. Specificities on Partition that creates an hidden Lexical Table),
	 * when making a result non persistent, all its parents that are hidden in the branch are set as non persistent (otherwise they wouldn't be deleted).
	 * 
	 * This method also flushes the result parameter node and so saves the persistence file.
	 * 
	 * @param userPersistable the state to set
	 */
	public void setUserPersistable(boolean userPersistable) {

		if (this.userPersistable == userPersistable) {
			return;
		}

		this.userPersistable = userPersistable;

		// 1- MANAGE THE PARENTS
		// recursively force the persistable state of the parents if this result is set to persistable
		if (userPersistable && this.parent != null && !this.parent.isInternalPersistable()) {
			this.parent.setUserPersistable(true);
		}
		// recursively unpersist hidden parents
		else if (!userPersistable && !this.parent.isVisible()) {
			this.parent.setUserPersistable(false);
		}
		// 2- MANAGE THE CHILDREN
		// recursively force non-persistable state of children if this result is set to non-persistable
		// also transfer this result state to its children marked as "must be synchronized with parent"
		for (TXMResult child : this.getChildren()) {
			if (!userPersistable || child.mustBeSynchronizedWithParent()) {
				child.setUserPersistable(userPersistable);
			}
		}

		// directly save and flush the preference
		try {
			if (this.isUserPersistable()) {
				this.autoSaveParametersFromAnnotations();
				this.saveParameters();
				TXMPreferences.flush(this);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * Swaps the user persistable state.
	 * 
	 * propagate persistence to parent if necessary
	 */
	public void swapUserPersistableState() {
		this.setUserPersistable(!this.userPersistable);
	}

	/**
	 * Gets the project of the result.
	 * 
	 * @return the project of the result
	 */
	public final Project getProject() {
		return this.getFirstParent(Project.class);
	}

	/**
	 * Gets the parameters node path.
	 * 
	 * @return the parametersNodePath
	 */
	public String getParametersNodePath() {
		return parametersNodePath;
	}

	/**
	 * @return the parametersHistory
	 */
	public ArrayList<HashMap<String, Object>> getParametersHistory() {
		return parametersHistory;
	}

	/**
	 * Gets the last computing date.
	 * 
	 * @return the last computing date
	 */
	public Date getLastComputingDate() {
		return this.lastComputingDate;
	}

	/**
	 * Gets the creation date.
	 * 
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return this.creationDate;
	}


	public boolean toParametersFile(File outfile) throws IOException {
		return TXMPreferences.flush(this, outfile);
	}


	/**
	 * Generic toString() method for TXMResult.
	 */
	@Override
	public String toString() {
		if (userName != null && !userName.isEmpty()) {
			return userName;
		}
		else if (lazyName != null && !lazyName.isEmpty()) {
			return lazyName;
		}
		return this.getSimpleName();
	}

	/**
	 * Default implementation getSimpleDetails
	 * 
	 * @return
	 */
	public String getSimpleDetails() {
		return getDetails().replace("\n", " "); //$NON-NLS-1$
	}


	/**
	 * Checks if the result is in computing stage.
	 * 
	 * @return the computing
	 */
	public boolean isComputing() {
		return computing;
	}


	/**
	 * Checks if this result must always been computed when its parent is computed.
	 * 
	 * @return the synchronizedWithParent
	 */
	public boolean mustBeSynchronizedWithParent() {
		return synchronizedWithParent;
	}

	/**
	 * Set the result synchronized to its parent
	 * 
	 */
	public void setMustBeSynchronizedWithParent(boolean synchronizedWithParent) {
		this.synchronizedWithParent = synchronizedWithParent;
	}


	/**
	 * @return the computingThread
	 */
	public Thread getComputingThread() {
		return computingThread;
	}

	public boolean hasField(String key) {

		return BeanParameters.getField(this, key) != null;
	}

	/**
	 * 
	 * @return the first visible parent TXMResult
	 */
	public TXMResult getFirstVisibleParent() {

		TXMResult visibleParent = this.getParent();
		while (!visibleParent.isVisible()) {
			visibleParent = visibleParent.getParent();
			if (visibleParent == null) return null; // Workspace result parent is null
			if (visibleParent == visibleParent.getParent()) return null; // should not happen
		}
		return visibleParent;
	}

	public TXMResult moveUp() {

		TXMResult newParent = this.getParent();
		if (newParent == null) {
			return null; // strange but necessary to test
		}
		newParent = newParent.getParent();
		while (newParent != null 
				&& !this.canComputeWith(newParent)) {
			newParent = newParent.getParent();
		}

		if (newParent == null) {
			return null;
		}

		// break links
		this.setMustBeSynchronizedWithParent(false);
		TXMPreferences.put("uniqlink", parametersNodePath, false);
		
		moveTo(newParent);
		return newParent;
	}

	public boolean moveTo(TXMResult newParent) {
		
		this.parent.getChildren().remove(this);
		
		this.parent = newParent;
		this.parent.getChildren().add(this);
		this.saveParameter(TXMPreferences.PARENT_PARAMETERS_NODE_PATH, this.parent.getParametersNodePath());
		return true;
	}

	public boolean canComputeWith(TXMResult newParent) {

		for (Constructor<?> m : this.getClass().getConstructors()) {
			if (m.getParameterCount() == 1 
					&& (m.getParameters()[0].getType().equals(newParent.getClass()) 
							|| m.getParameters()[0].getType().isAssignableFrom(newParent.getClass()))) {
				return true;
			}
		}

		return false;
	}
}
