package org.txm.core.engines;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashSet;

import org.eclipse.core.runtime.IStatus;
import org.txm.core.results.TXMResult;

/**
 * Wrapper of old xxxLoader scripts 
 * 
 * @author mdecorde
 *
 */
public abstract class ExportEngine<T extends TXMResult> implements Engine {
	
	public static final String EXTENSION_POINT_ID = ExportEngine.class.getCanonicalName();
	
	protected String name;
	
	/**
	 * List of the TXMResults classes 
	 */
	protected Class<T> managed;
	
	protected LinkedHashSet<String> formats;
	
	public boolean canExecute(File script) {
		return true;
	}
	
	public ExportEngine(String name, Class<T> managed, LinkedHashSet<String> formats) {
		this.name = name;
		this.managed = managed;
		this.formats = formats;
		
		initializeManagedResults();
		initializeManagedFormats();
	}
	
	public ExportEngine(String name) {
		this.name = name;
		this.managed = null;
		this.formats = new  LinkedHashSet<String>();
		
		initializeManagedResults();
		initializeManagedFormats();
	}
	
	public abstract IStatus export(T result, HashMap<String, Object> parameters);
	
	public abstract IStatus initializeManagedResults();
	
	public abstract IStatus initializeManagedFormats();
	
	@Override
	public String getName() {
		return name;
	}
	
	public final Class<T> getManagedResultClass() {
		return managed;
	}
	
	public final LinkedHashSet<String> getManagedFormats() {
		return formats;
	}
	
	@Override
	public void notify(TXMResult r, String state) { }
	
	public String getDetails() {
		return this.getClass()+ " "+this.toString(); //$NON-NLS-1$
	}

	
	public String hasAdditionalDetailsForResult(TXMResult result) {
		return null;
	}
	
	public String getAdditionalDetailsForResult(TXMResult result) {
		return null;
	}
}
