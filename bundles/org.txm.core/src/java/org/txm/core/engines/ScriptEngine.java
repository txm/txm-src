package org.txm.core.engines;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.txm.core.results.TXMResult;

/**
 * Wrapper of old xxxLoader scripts 
 * 
 * @author mdecorde
 *
 */
public abstract class ScriptEngine implements Engine {
	
	public static final String EXTENSION_POINT_ID = ScriptEngine.class.getCanonicalName();
	
	protected String name;
	
	protected String scriptExtension;
	
	public boolean canExecute(File script) {
		return true;
	}
	
	public ScriptEngine(String name, String scriptExtension) {
		this.name = name;
		this.scriptExtension = scriptExtension;
	}
	
	public IStatus executeScript(String scriptPath) {
		return executeScript(new File(scriptPath));
	}
	
	public IStatus executeScript(File script) {
		return executeScript(script, null, null);
	}

	public IStatus executeScript(File script, HashMap<String, Object> env) {
		return executeScript(script, env, null);
	}

	public abstract IStatus executeScript(File script, HashMap<String, Object> env, List<String> args);
	
	public IStatus executeText(String text) {
		return executeText(text);
	}
	
	public abstract IStatus executeText(String str, HashMap<String, Object> env);
	
	@Override
	public String getName() {
		return name;
	}
	
	public String getScriptExtension() {
		return scriptExtension;
	}
	
	@Override
	public void notify(TXMResult r, String state) {
		//nothing to do
	}
	
	public String getDetails() {
		return this.getClass()+ " "+this.toString(); //$NON-NLS-1$
	}

	public IStatus executeScript(File perlScript, List<String> args) {
		return executeScript(perlScript, null, args);		
	}
	
	public String hasAdditionalDetailsForResult(TXMResult result) {
		return null;
	}
	
	public String getAdditionalDetailsForResult(TXMResult result) {
		return null;
	}
}
