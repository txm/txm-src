package org.txm.core.engines;

import org.txm.Toolbox;

public class ExportEnginesManager extends EnginesManager<ScriptEngine> {

	private static final long serialVersionUID = -3058514572644848458L;

	@Override
	public boolean fetchEngines() {
		return this.fetchEngines(ScriptEngine.EXTENSION_POINT_ID);
	}

	@Override
	public EngineType getEnginesType() {
		return EngineType.EXPORT;
	}
	
	// commons script engines
	public static ScriptEngine getGroovyEngine() {
		return (ScriptEngine) Toolbox.getEngineManager(EngineType.SCRIPT).getEngine("groovy"); //$NON-NLS-1$
	}
	
	public static ScriptEngine getREngine() {
		return (ScriptEngine) Toolbox.getEngineManager(EngineType.SCRIPT).getEngine("R"); //$NON-NLS-1$
	}
	
	public static ScriptEngine getPerlEngine() {
		return (ScriptEngine) Toolbox.getEngineManager(EngineType.SCRIPT).getEngine("perl"); //$NON-NLS-1$
	}
	
	public static ScriptEngine getPythonEngine() {
		return (ScriptEngine) Toolbox.getEngineManager(EngineType.SCRIPT).getEngine("python"); //$NON-NLS-1$
	}

	public ScriptEngine getEngineForExtension(String ext) {
		
		for (ScriptEngine e : this.getEngines().values()) {
			if (ext.equals(e.getScriptExtension())) {
				return e;
			}
		}
		return null;
	}
}
