package org.txm.core.engines;

import java.io.File;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.txm.core.results.TXMResult;
import org.txm.objects.Project;

/**
 * Wrapper of old xxxLoader scripts 
 * 
 * @author mdecorde
 *
 */
public abstract class ScriptedImportEngine extends ImportEngine {
	protected File mainScript;
	protected String name;
	
	public ScriptedImportEngine(String name, File script) {
		this.name = name;
		this.mainScript = script;
	}

	@Override
	public boolean isRunning() {
		return mainScript.exists();
	}

	@Override
	public boolean initialize() throws Exception {
		return mainScript.exists();
	}

	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		return mainScript.exists();
	}

	@Override
	public boolean stop() throws Exception {
		return true;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public final IStatus build(Project project, IProgressMonitor monitor) {
		if (mainScript == null || !mainScript.exists()) {
			System.out.println("Startup script not set: "+mainScript);
			return new Status(Status.ERROR, "org.txm.core", "Startup script not set: "+mainScript);
		}
		return _build(project, monitor);
	}
	
	public String hasAdditionalDetailsForResult(TXMResult result) {
		return null;
	}
	
	public String getAdditionalDetailsForResult(TXMResult result) {
		return null;
	}
	
	public abstract IStatus _build(Project project, IProgressMonitor monitor);
	
}
