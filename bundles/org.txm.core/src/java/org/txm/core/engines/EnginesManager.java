package org.txm.core.engines;

import java.util.Collection;
import java.util.HashMap;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.RegistryFactory;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.LogMonitor;
import org.txm.utils.logger.Log;

/**
 * Extension point for an engines manager (eg. SearchEnginesManager, StatsEnginesManager, ChartsEnginesManager, etc.).
 * Engines manager are in charge of retrieving the engines from the engine installed contributions.
 * They also batch start, stop, etc. the engines of the type they managed (eg. Search engine, Stats engine, etc.).
 * It is a service that may be used by TXM commands. The Toolbox will manage the engines life-cycle.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public abstract class EnginesManager<T extends Engine> extends HashMap<String, T> {

	private static final long serialVersionUID = 9186978207738860351L;

	public static final String ENGINES_MANAGER_EXTENSION_POINT_ID = EnginesManager.class.getName();

	/**
	 * Current engine.
	 */
	protected T currentEngine = null;

	/**
	 * Finds out the engines that matches this manager engines extension point.
	 * 
	 * @return
	 */
	public abstract boolean fetchEngines();

	/**
	 * Creates and initializes the engines from contributions with the specified extension point id.
	 * 
	 * @param engineExtensionPointId
	 * @return true if at least one engine has been created otherwise false
	 */
	protected boolean fetchEngines(String engineExtensionPointId) {

		IConfigurationElement[] contributions = RegistryFactory.getRegistry().getConfigurationElementsFor(engineExtensionPointId);

		Log.finest(TXMCoreMessages.bind("Looking for {0} engines contributions with extension point id {1}...", this.getEnginesDescription(), engineExtensionPointId)); //$NON-NLS-1$
		Log.finest(TXMCoreMessages.bind("{0} engine(s) found.", contributions.length)); //$NON-NLS-1$

		for (int i = 0; i < contributions.length; i++) {
			try {
				@SuppressWarnings("unchecked")
				T engine = (T) contributions[i].createExecutableExtension("class"); //$NON-NLS-1$

				Log.fine(TXMCoreMessages.bind("Initializing engine: {0}...", engine));

				if (engine.initialize()) {
					this.put(engine.getName(), engine);
				}
				else {
					Log.severe("Failed to initialize " + engine.getName() + " engine.");
				}
			}
			catch (Exception e) {
				Log.severe("Error: failed to instantiate " + contributions[i].getName() + ".");
				e.printStackTrace();
			}
		}

		return this.size() > 0;
	}

	/**
	 * Starts the engines.
	 * 
	 * @param monitor
	 * @return
	 */
	public boolean startEngines(IProgressMonitor monitor) {
		for (Engine engine : this.values()) {

			String log = "Starting " + this.getEnginesDescription() + " engine: " + engine.getName() + "...";
			Log.fine(log);

			if (monitor != null) {
				monitor.subTask(log);
			}

			try {
				engine.start(monitor);
			}
			catch (Exception e) {
				Log.severe("Error: failed to start engine " + engine.getName() + ".");
				e.printStackTrace();
			}
		}

		if (this.currentEngine == null && this.size() > 0) {
			for (T e : this.values()) { // get the first one and done
				this.setCurrentEngine(e);
				break;
			}
		}
		return true;
	}

	@Override
	public String toString() {
		return getEnginesType() + ": " + super.toString();
	}

	/**
	 * 
	 * @return true if at least one engine is ready
	 */
	public boolean isCurrentEngineAvailable() {
		if (currentEngine == null) {
			return false;
		}

		return currentEngine.isRunning();
	}

	/**
	 * Stops the engines.
	 * 
	 * @return
	 */
	public boolean stopEngines() {
		for (Engine engine : this.values()) {

			Log.fine("Stopping " + this.getEnginesDescription() + " engine: " + engine.getName() + "..."); //$NON-NLS-1$

			try {
				engine.stop();
			}
			catch (Exception e) {
				Log.severe("Error: failed to stop engine " + engine.getName() + ".");
				e.printStackTrace();
			}
		}
		return true;
	}

	/**
	 * Restarts the engines.
	 * 
	 * @return
	 */
	public boolean restartEngines() {

		Log.finest("Restarting " + this.size() + " " + this.getEnginesDescription() + " engine(s)."); //$NON-NLS-1$

		for (Engine engine : this.values()) {
			try {
				if (engine.isRunning()) {
					Log.fine("Stopping " + this.getEnginesDescription() + " engine: " + engine.getName() + "..."); //$NON-NLS-1$
					engine.stop();
				}
				if (!engine.isRunning()) {
					Log.fine("Starting " + this.getEnginesDescription() + " engine: " + engine.getName() + "..."); //$NON-NLS-1$
					engine.start(new LogMonitor());
				}
			}
			catch (Exception e) {
				Log.severe("Error: failed to stop engine " + engine.getName() + ".");
				e.printStackTrace();
			}
		}
		return true;
	}

	/**
	 * Gets the engine type that this manager supports.
	 * 
	 * @return the engine type that this manager supports
	 */
	public abstract EngineType getEnginesType();



	/**
	 * Gets the engine description that this manager supports.
	 * 
	 * @return the engine description that this manager supports
	 */
	public String getEnginesDescription() {
		return this.getEnginesType().getDescription();
	}

	/**
	 * Gets the available engines.
	 * 
	 * @return the available engines
	 */
	public HashMap<String, T> getEngines() {
		return this;
	}
	
	/**
	 * Gets the available engines.
	 * 
	 * @return the available engines
	 */
	public Collection<T> getEnginesList() {
		return this.values();
	}

	/**
	 * Gets an engine specified by its name.
	 * 
	 * @param name
	 * @return an engine specified by its name if exists, otherwise null
	 */
	public T getEngine(String name) {
		return get(name);
	}

	/**
	 * Returns the current engine.
	 * 
	 * @return
	 */
	public T getCurrentEngine() {
		return this.currentEngine;
	}

	/**
	 * Sets the current engine.
	 * 
	 * @param engine
	 */
	public void setCurrentEngine(T engine) {
		Log.finest("Setting current " + this.getEnginesDescription() + " engine to " + engine + ".");
		this.currentEngine = engine;
	}

	/**
	 * Sets the current engine specified by its name.
	 * 
	 * @param name
	 */
	public void setCurrentEngine(String name) {
		this.setCurrentEngine(this.getEngine(name));
	}

	/**
	 * Gets an installed engine according to its class type.
	 * 
	 * @param classType
	 * @return the installed engine according to its class type if it exists
	 *         otherwise null
	 */
	public T getEngine(Class<?> classType) {
		T engine = null;
		for (T engine2 : this.values()) {
			if (engine2.getClass().equals(classType)) {
				engine = engine2;
				break;
			}
		}
		return engine;
	}
}
