package org.txm.core.engines;

public class ImportEngines extends EnginesManager<ImportEngine>{

	private static final long serialVersionUID = -3058514572644848458L;

	@Override
	public boolean fetchEngines() {
		return this.fetchEngines(ImportEngine.EXTENSION_POINT_ID);
	}

	@Override
	public EngineType getEnginesType() {
		return EngineType.IMPORT;
	}
	
	// commons import modules
	
	public ImportEngine getTXTEngine() {
		return this.getEngine("txt"); //$NON-NLS-1$
	}
	
	public ImportEngine getXTZEngine() {
		return this.getEngine("xtz"); //$NON-NLS-1$
	}
	
	public ImportEngine getXMLEngine() {
		return this.getEngine("xmlw"); //$NON-NLS-1$
	}
		
	public ImportEngine getTranscriberEngine() {
		return this.getEngine("transcriber"); //$NON-NLS-1$
	}
}
