package org.txm.core.engines;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.txm.core.results.TXMResult;
import org.txm.objects.Project;

public abstract class ImportEngine implements Engine {
	
	public static final String EXTENSION_POINT_ID = ImportEngine.class.getCanonicalName();
	
	@Override
	public abstract String getName();
	
	/**
	 * Regenerate all project results
	 * 
	 * @param project
	 * @param monitor
	 * @return true if the build was successful
	 */
	public abstract IStatus build(Project project, IProgressMonitor monitor);
	
	@Override
	public void notify(TXMResult r, String state) {
		// nothing to do
	}
	
	@Override
	public String getDetails() {
		return this.getClass() + " " + this.toString();
	}
	
	public String hasAdditionalDetailsForResult(TXMResult result) {
		return null;
	}
	
	public String getAdditionalDetailsForResult(TXMResult result) {
		return null;
	}
}
