package org.txm.core.engines;

/**
 * Engine types.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
// FIXME: SJ: the description (and name) of the engine should be done in extensions points as for the charts engine, it would be better and not break the plugins modularity
// It also could be done with abstract methods that implementations need to implement.
public enum EngineType {
	
	SEARCH("search"), //$NON-NLS-1$
	STATS("statistics"), //$NON-NLS-1$
	CHARTS("charts"), //$NON-NLS-1$
	SCRIPT("scripts"), //$NON-NLS-1$
	ANNOTATION("annotations"), //$NON-NLS-1$
	NLP("nlp"), //$NON-NLS-1$
	OTHER("others engines"), //$NON-NLS-1$
	IMPORT("import"), //$NON-NLS-1$
	EXPORT("export"); //$NON-NLS-1$

	/**
	 * Description.
	 */
	final private String description;

	/**
	 * Creates a new engine type.
	 * @param description
	 */
	private EngineType(String description) {
		this.description = description;
	}

	/**
	 * Gets the description of the type.
	 * @return
	 */
	public String getDescription() {
		return this.description;
	}

}
