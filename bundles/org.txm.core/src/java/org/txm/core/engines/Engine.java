package org.txm.core.engines;

import org.eclipse.core.runtime.IProgressMonitor;
import org.txm.core.results.TXMResult;

/**
 * An engine gives access to services. The user must know its implementation Class to access those services.
 *
 * @author mdecorde
 *
 */
public abstract interface Engine {
	
	/**
	 * An engine must at least give its name (not null).
	 * 
	 * @return the engine name
	 */
	public String getName();
	
	/**
	 * Returns the running state.
	 * @return
	 */
	public abstract boolean isRunning();
	
	/**
	 * Initializes the engine. Can do some tasks needed before the start.
	 * @return
	 * @throws Exception
	 */
	public abstract boolean initialize() throws Exception;
	
	/**
	 * Starts the engine.
	 * 
	 * @param monitor
	 * @return true if the engine is well started
	 * @throws Exception
	 */
	public abstract boolean start(IProgressMonitor monitor) throws Exception;
	
	/**
	 * Stops the engine.
	 * @return true if the engine is stopped
	 * @throws Exception
	 */
	public abstract boolean stop() throws Exception;

	/**
	 * Notify the engine of a result state
	 * example of usage: when a MainCorpus is deleted some engines needs to unload/delete files
	 * 
	 * @param r
	 * @param state
	 */
	public void notify(TXMResult r, String state);

	/**
	 * Gets description of the engine.
	 * 
	 * @return details about the engine (eg. the elements managed)
	 */
	public String getDetails();

	/**
	 * test if there is some information of a result if the engine needs to 
	 */
	public String hasAdditionalDetailsForResult(TXMResult result);
	
	/**
	 * gives an information name about a result if the engine needs to 
	 */
	public String getAdditionalDetailsForResult(TXMResult result);
}
