package org.txm.core;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
/**
 * TXM corpus project nature
 * 
 * TODO use this class instead of Project.open/close stuff
 * 
 * @author mdecorde
 *
 */
public class CorpusNature implements IProjectNature {
	//
	public static final String NATURE_ID = "org.txm.core.CorpusNature"; //$NON-NLS-1$
	
	/**
	 * the eclipse project pointing to the TXM corpus project
	 */
	private IProject project;
	
	@Override
	public void configure() throws CoreException {
		// TODO Auto-generated method stub
	}

	@Override
	public void deconfigure() throws CoreException {
		// nothing to do
	}

	@Override
	public IProject getProject() {
		return project;
	}

	@Override
	public void setProject(IProject project) {
		this.project = project;
	}
}
