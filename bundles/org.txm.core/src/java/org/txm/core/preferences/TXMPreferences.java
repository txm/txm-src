package org.txm.core.preferences;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.osgi.util.NLS;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMParameters;
import org.txm.core.results.TXMResult;
import org.txm.objects.Project;
import org.txm.utils.io.FileCopy;
import org.txm.utils.logger.Log;



/**
 * Base preferences initializer and manager.
 * 
 * Getters return a value of preference looking in this order:
 * 
 * 1) local node linked to a TXM result if exists									(ProjectScope - {@code /project/<project_name>/<UUID of TXMResult>/<preference key>})
 * 
 * 
 * 		Later, we can add the project/corpus preference level:
 * 			project/corpus user value
 * 			project/corpus default value (seem useless?)
 * 
 *  * 
 * 2) command user value (global value)												(InstanceScope - {@code /instance/<bundle_id>/<preference key>}) (ex. org.txm.ca.core: preference "show_legend")
 *  
 * 3) command default value (global value)											(DefaultScope - {@code /bundle_defaults/<bundle_id>/<preference key>})
 * 
 * 4) Additional node qualifiers to look in
 * 
 * 		3a) additional specified user value,
 * 	 		ex. engine preference added to TXMPreference.alternativeNodesQualifiers		(InstanceScope - {@code /instance/<bundle_id>/<preference key>}) (eg. org.txm.chartsengine.core: preference "show_legend")
 * 		4a) additional specified default value,
 *  		ex. engine preference added to TXMPreference.alternativeNodesQualifiers		(DefaultScope - {@code /bundle_defaults/<bundle_id>/<preference key>})
 *  
 *  ex. for a SearchEngine
 * 		3b) CQPEngine user preference
 * 		4b) CQPEngine default preference
 *
 *    
 * 5) hard coded in getters
 * 
 * 
 * 
 * 
 * Persistence of TXM results is done by calling flush() that creates a file named by the result node qualifier,
 *
 *
 * @author mdecorde
 * @author sjacquot
 * 
 *
 */
public abstract class TXMPreferences extends AbstractPreferenceInitializer {
	
	/**
	 * Root node of the Eclipse preference hierarchy. 
	 */
	public static IEclipsePreferences preferencesRootNode = Platform.getPreferencesService().getRootNode();
	
	/**
	 * Additional nodes to look up when getting a preference.
	 */
	public static ArrayList<String> additionalNodesQualifiers = new ArrayList<>();
	
	
	// TXMResult internal parameters, essentially for Links and Persistence
	/**
	 * Result parameters node path in preferences.
	 */
	public final static String RESULT_PARAMETERS_NODE_PATH = "result_parameters_node_path"; //$NON-NLS-1$
	
	/**
	 * Parent parameters node path in preferences.
	 */
	public final static String PARENT_PARAMETERS_NODE_PATH = "parent_parameters_node_path"; //$NON-NLS-1$
	
	/**
	 * The class of the TXMResult.
	 */
	public final static String CLASS = "class"; //$NON-NLS-1$
	
	/**
	 * The bundle id of the result.
	 */
	public final static String BUNDLE_ID = "bundle_id"; //$NON-NLS-1$
	
	public final static String USER_NAME = "user_name"; //$NON-NLS-1$
	
	public final static String LAZY_NAME = "lazy_name"; //$NON-NLS-1$
	
	public final static String VISIBLE = "visible"; //$NON-NLS-1$
	
	public static final String LOCK = "locked_result"; //$NON-NLS-1$
	
	/**
	 * Result creation date.
	 */
	public static final String CREATION_DATE = "creation_date"; //$NON-NLS-1$
	
	/**
	 * Last computing date of a result.
	 */
	public static final String LAST_COMPUTING_DATE = "last_computing_date"; //$NON-NLS-1$
	// End of TXMResult internal parameters	

	
	// FIXME: SJ: do not store the "persistable" state in the preferences and file 
	public static final String PERSITABLE = "persistable"; //$NON-NLS-1$

	// FIXME: SJ: became useless?
	public static final String INTERNAL_PERSITABLE = "internal_persistable"; //$NON-NLS-1$

	
	
	
	
	// Command preferences shared keys strings
	/**
	 * Queries.
	 */
	public static final String QUERIES = "queries"; //$NON-NLS-1$
	
	/**
	 * Query.
	 */
	public static final String QUERY = "query"; //$NON-NLS-1$

	/**
	 * Query.
	 */
	public static final String TARGET_STRATEGY = "target_strategy"; //$NON-NLS-1$
	
	/**
	 * Result index.
	 */
	public static final String INDEX = "index"; //$NON-NLS-1$
	
	/**
	 * Result sub index.
	 */
	public static final String SUB_INDEX = "sub_index"; //$NON-NLS-1$
	
	/**
	 * Structural unit.
	 */
	public static final String STRUCTURAL_UNIT = "structural_unit"; //$NON-NLS-1$
	
	/***
	 * Structural unit property.
	 */
	public static final String STRUCTURAL_UNIT_PROPERTY = "structural_unit_property"; //$NON-NLS-1$
	
	/**
	 * Structural unit properties.
	 */
	public static final String STRUCTURAL_UNIT_PROPERTIES = "structural_unit_properties"; //$NON-NLS-1$
	
	/**
	 * Unit property.
	 */
	public static final String UNIT_PROPERTY = "unit_property"; //$NON-NLS-1$
	
	/**
	 * Unit properties.
	 */
	public static final String UNIT_PROPERTIES = "unit_properties"; //$NON-NLS-1$
	
	/**
	 * List of values
	 */
	public static final String VALUES = "values"; //$NON-NLS-1$
	
	/**
	 * Matches.
	 */
	public static final String MATCHES = "matches"; //$NON-NLS-1$
	
	/**
	 * Minimum frequency filter value.
	 */
	public static final String F_MIN = "f_min"; //$NON-NLS-1$
	
	/**
	 * Maximum frequency filter value.
	 */
	public static final String F_MAX = "f_max"; //$NON-NLS-1$
	
	/**
	 * V max filter (max number of lines).
	 */
	public static final String V_MAX = "v_max"; //$NON-NLS-1$
	
	/**
	 * Separator used when converting a list from and to a string.
	 */
	public static final String LIST_SEPARATOR = "\t"; //$NON-NLS-1$
	
	/**
	 * Separator used when building some names based on a parent name and the result name (e.g. VOEUX/text@loc).
	 */
	public static final String PARENT_NAME_SEPARATOR = "/"; //$NON-NLS-1$
	
	/**
	 * Number of lines to display per result page.
	 */
	public static final String NB_OF_ELEMENTS_PER_PAGE = "nb_of_elements_per_page"; //$NON-NLS-1$
	
	/**
	 * Current page number.
	 */
	public static final String CURRENT_PAGE = "current_page"; //$NON-NLS-1$
	
	/**
	 * Encoding.
	 */
	public final static String ENCODING = "encoding"; //$NON-NLS-1$
	
	/**
	 * Default encoding value.
	 */
	public final static String DEFAULT_ENCODING = "UTF-8"; //$NON-NLS-1$
	
	/**
	 * Default unit/token property String.
	 */
	public final static String DEFAULT_UNIT_PROPERTY = "word"; //$NON-NLS-1$
	
	/**
	 * Default structural unit String.
	 */
	public final static String DEFAULT_STRUCTURAL_UNIT = "text"; //$NON-NLS-1$
	
	/**
	 * Default structural unit property String.
	 */
	public final static String DEFAULT_STRUCTURAL_UNIT_PROPERTY = "text_id"; //$NON-NLS-1$
	// End of command preferences shared strings
	
	
	
	
	/**
	 * Preferences node qualifier of the instance of the preferences initializer and manager.
	 */
	protected String commandPreferencesNodeQualifier;
	
	/**
	 * TXMPreferences instances map
	 */
	protected static Map<Class<? extends TXMPreferences>, TXMPreferences> instances = new HashMap<>();
	
	
	/**
	 * Sub classes should not forget to call this to set TXMResults main behavior (visible, etc...)
	 */
	@Override
	// FIXME: SJ: define an abstract method as _initializeDefaultPreferences() called here and that must be implemented by subclasses
	public void initializeDefaultPreferences() {
		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.putBoolean(TBXPreferences.VISIBLE, true);
		preferences.putBoolean(TBXPreferences.LOCK, false);
		preferences.put(TBXPreferences.CORPUS_VERSION, "0.8.0"); //$NON-NLS-1$
	}
	
	/**
	 * 
	 */
	protected TXMPreferences() {
		super();
		TXMPreferences.instances.put(this.getClass(), this);
		this.commandPreferencesNodeQualifier = FrameworkUtil.getBundle(this.getClass()).getSymbolicName();
		Log.finest(NLS.bind("TXMPreferences.TXMPreferences(): initializing preferences for node \"{0}\" (class: {1}).", this.commandPreferencesNodeQualifier, this.getClass())); //$NON-NLS-1$
	}
	
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public double getDouble(String key) {
		return getDouble(key, this.commandPreferencesNodeQualifier);
	}
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public boolean getBoolean(String key) {
		return getBoolean(key, this.commandPreferencesNodeQualifier);
	}
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public int getInt(String key) {
		return getInt(key, this.commandPreferencesNodeQualifier);
	}
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public long getLong(String key) {
		return getLong(key, this.commandPreferencesNodeQualifier);
	}
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public String getString(String key) {
		return getString(key, this.commandPreferencesNodeQualifier);
	}
	
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public float getFloat(String key) {
		return getFloat(key, this.commandPreferencesNodeQualifier);
	}
	
	
	/**
	 * Gets the preferences node qualifier of the instance.
	 * 
	 * @return the preferencesNode
	 */
	public String getPreferencesNodeQualifier() {
		return commandPreferencesNodeQualifier;
	}
	
	
	
	//FIXME
	// /**
	// * Stores a pairs of key / value in a local node dedicated to the specified result. The node qualifier is generated by the <code>Object.toString()</code> method.
	// *
	// * @param key
	// * @param pValue
	// */
	// public static void remove(String key, String node) {
	// scope.getNode(node).remove(key);
	// }
	
	/**
	 * Saves all preferences and session results in the default directory.
	 */
	public static void saveAll() {
		
		Log.fine(TXMCoreMessages.info_savingPreferencesAndResults);
		
		try {
			preferencesRootNode.flush();
		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.error_errorWhileSavingPreferencesAndResults);
			e.printStackTrace();
		}
	}
	
	/**
	 * Looks for the value of the specified <code>key</code> in the specified parameters and preference nodes.
	 * Returns the value of a key in the specified parameters if exists.
	 * Otherwise try to get it from the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns <code>false</code> if no value has been found for the specified key.
	 * 
	 * @param commandParameters
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static Serializable getSerializable(String key, TXMParameters commandParameters, TXMResult result, String nodePath) {
		if (commandParameters != null && commandParameters.get(key) != null) {
			return (Serializable) commandParameters.get(key);
		}
		return getSerializable(key, result, nodePath);
	}
	
	
	public static Serializable getSerializable(String key, TXMParameters commandParameters, TXMResult result) {
		return getSerializable(key, commandParameters, result, result.getCommandPreferencesNodePath());
	}
	
	/**
	 * Looks for the value of the specified <code>key</code> in preference nodes.
	 * Returns the value of the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns defaultValue if no value has been found for the specified key.
	 * 
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static Serializable getSerializable(String key, TXMResult result, String nodePath, Serializable defaultValue) {
		
		nodePath = findNodePath(nodePath, result, key);
		
		String data_default = DefaultScope.INSTANCE.getNode(nodePath).get(key, ""); //$NON-NLS-1$
		if (data_default.length() == 0) {
			return null;
		}
		
		try {
			defaultValue = fromString(data_default);
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
		
		String data_return = preferencesRootNode.node(nodePath).get(key, ""); //$NON-NLS-1$
		if (data_return.length() == 0) {
			return defaultValue;
		}
		
		try {
			return fromString(data_return);
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
		return null;
	}
	
	/**
	 * Looks for the value of the specified <code>key</code> in preference nodes.
	 * Returns the value of the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns <code>false</code> if no value has been found for the specified key.
	 * 
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static Serializable getSerializable(String key, TXMResult result, String nodePath) {
		return getSerializable(key, result, nodePath, false);
	}
	
	/**
	 *
	 * @param nodePath
	 * @param key
	 * @return
	 */
	public static Serializable getSerializable(String key, String nodePath) {
		return getSerializable(key, null, nodePath);
	}
	
	/**
	 *
	 * @param result
	 * @param key
	 * @return
	 */
	public static Serializable getSerializable(String key, TXMResult result) {
		return getSerializable(key, result, null);
	}
	
	/**
	 * Looks for the value of the specified <code>key</code> in the specified parameters and preference nodes.
	 * Returns the value of a key in the specified parameters if exists.
	 * Otherwise try to get it from the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns <code>false</code> if no value has been found for the specified key.
	 * 
	 * @param commandParameters
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static boolean getBoolean(String key, TXMParameters commandParameters, TXMResult result, String nodePath) {
		if (commandParameters != null && commandParameters.get(key) != null) {
			return (Boolean) commandParameters.get(key);
		}
		return getBoolean(key, result, nodePath);
	}
	
	/**
	 * 
	 * @param key
	 * @param commandParameters
	 * @param result
	 * @return
	 */
	public static boolean getBoolean(String key, TXMParameters commandParameters, TXMResult result) {
		return getBoolean(key, commandParameters, result, result.getCommandPreferencesNodePath());
	}
	
	/**
	 * Looks for the value of the specified <code>key</code> in preference nodes.
	 * Returns the value of the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns defaultValue if no value has been found for the specified key.
	 * 
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static boolean getBoolean(String key, TXMResult result, String nodePath, boolean defaultValue) {
		
		nodePath = findNodePath(nodePath, result, key);
		
		// defaultValue = DefaultScope.INSTANCE.getNode(nodePath).getBoolean(key, defaultValue);
		return preferencesRootNode.node(nodePath).getBoolean(key, defaultValue);
	}
	
	/**
	 * Looks for the value of the specified <code>key</code> in preference nodes.
	 * Returns the value of the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns <code>false</code> if no value has been found for the specified key.
	 * 
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static boolean getBoolean(String key, TXMResult result, String nodePath) {
		return getBoolean(key, result, nodePath, false);
	}
	
	/**
	 *
	 * @param nodePath
	 * @param key
	 * @return
	 */
	public static boolean getBoolean(String key, String nodePath) {
		return getBoolean(key, null, nodePath);
	}
	
	/**
	 *
	 * @param result
	 * @param key
	 * @return
	 */
	public static boolean getBoolean(String key, TXMResult result) {
		return getBoolean(key, result, null);
	}
	
	
	
	/**
	 * Try to cast and create a typed object from the String value of the key.
	 * 
	 * @param commandParameters
	 * @param result
	 * @param nodePath
	 * @param key
	 * @return
	 */
	// FIXME: SJ: does not work, need to test furthermore
	// public static Object get(TXMResultParameters commandParameters, TXMResult result, String nodePath, String key) {
	//
	//
	// if(commandParameters != null && commandParameters.get(key) != null) {
	// return commandParameters.get(key);
	// }
	//
	// nodePath = findNodeQualifier(nodePath, result, key);
	//
	// String stringValue = getString(nodePath, key);
	//
	// try {
	// // double
	// try {
	// return Double.parseDouble(stringValue);
	// }
	// catch(NumberFormatException e) {
	// }
	// // float
	// try {
	// return Float.parseFloat(stringValue);
	// }
	// catch(NumberFormatException e) {
	// }
	// // long
	// try {
	// return Long.parseLong(stringValue);
	// }
	// catch(NumberFormatException e) {
	// }
	// // integer
	// try {
	// return Integer.parseInt(stringValue);
	// }
	// catch(NumberFormatException e) {
	// }
	// // short
	// try {
	// return Short.parseShort(stringValue);
	// }
	// catch(NumberFormatException e) {
	// }
	// // boolean
	// try {
	// return Boolean.parseBoolean(stringValue);
	// }
	// catch(NumberFormatException e) {
	// }
	// }
	// catch(Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// System.err.println("TXMPreferences.get(): can't parse and primitive cast the specified string value: " + stringValue + "."); //$NON-NLS-1$
	// }
	// return stringValue;
	// }
	
	
	
	// /**
	// * Looks for the value of the specified <code>key</code> in the specified parameters and preference nodes.
	// * Returns the value of a key in the specified parameters if exists.
	// * Otherwise try to get it from the local result node if exists.
	// * Otherwise try to get it from the specified node qualifier if exists.
	// * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	// * Returns an empty <code>String</code> if no value has been found for the specified key.
	// * @param commandParameters
	// * @param nodePath
	// * @param result
	// * @param key
	// * @return
	// */
	// public static String getString(String key, TXMParameters commandParameters, TXMResult result, String nodePath) {
	// if(commandParameters != null && commandParameters.get(key) != null) {
	// return (String) commandParameters.get(key);
	// }
	// return getString(key, result, nodePath);
	// }
	//
	// public static String getString(String key, TXMParameters commandParameters, TXMResult result) {
	// return getString(key, commandParameters, result, result.getCommandPreferencesNodePath());
	// }
	
	
	/**
	 * Looks for the value of the specified <code>key</code> in preference nodes.
	 * Returns the value of the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns defaultValue if no value has been found for the specified key.
	 * 
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static String getString(String key, TXMResult result, String nodePath, String defaultValue) {
		
		nodePath = findNodePath(nodePath, result, key);
		
		// System.out.println("--- TXMPreferences.getString() node path = " + nodePath);
		
		// FIXME defaultValue is always replaced even if set
		defaultValue = DefaultScope.INSTANCE.getNode(nodePath).get(key, defaultValue);
		return preferencesRootNode.node(nodePath).get(key, defaultValue);
	}
	
	/**
	 * Looks for the value of the specified <code>key</code> in preference nodes.
	 * Returns the value of the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns defaultValue if no value has been found for the specified key.
	 * 
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static Date getDate(String key, TXMResult result, String nodePath, Date defaultValue) {
		
		nodePath = findNodePath(nodePath, result, key);
		
		// System.out.println("--- TXMPreferences.getString() node path = " + nodePath);
		
		Date v = null;
		try {
			v = TXMResult.ID_TIME_FORMAT.parse(DefaultScope.INSTANCE.getNode(nodePath).get(key, null));
		}
		catch (Exception e) {
		}
		if (v != null) {
			defaultValue = v;
		}
		v = null;
		try {
			v = TXMResult.ID_TIME_FORMAT.parse(preferencesRootNode.node(nodePath).get(key, null));
		}
		catch (Exception e) {
		}
		
		if (v != null) {
			return defaultValue;
		}
		else {
			return v;
		}
	}
	
	/**
	 * Looks for the value of the specified <code>key</code> in preference nodes.
	 * Returns the value of the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns an empty <code>String</code> if no value has been found for the specified key.
	 * 
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static String getString(String key, TXMResult result, String nodePath) {
		return getString(key, result, nodePath, ""); //$NON-NLS-1$
	}
	
	/**
	 * Looks for the value of the specified <code>key</code> in preference nodes.
	 * Returns the value of the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns a 0 time <code>Date</code> if no value has been found for the specified key.
	 * 
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static Date getDate(String key, TXMResult result, String nodePath) {
		return getDate(key, result, nodePath, new Date(0));
	}
	
	
	/**
	 *
	 * @param nodePath
	 * @param key
	 * @return
	 */
	public static String getString(String key, String nodePath) {
		return getString(key, null, nodePath);
	}
	
	
	/**
	 *
	 * @param result
	 * @param key
	 * @return
	 */
	public static String getString(String key, TXMResult result) {
		return getString(key, result, null);
	}
	
	
	
	
	/**
	 * Looks for the value of the specified <code>key</code> in the specified parameters and preference nodes.
	 * Returns the value of a key in the specified parameters if exists.
	 * Otherwise try to get it from the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns 0 if no value has been found for the specified key.
	 * 
	 * @param commandParameters
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static int getInt(String key, TXMParameters commandParameters, TXMResult result, String nodePath) {
		if (commandParameters != null && commandParameters.get(key) != null) {
			return (Integer) commandParameters.get(key);
		}
		return getInt(key, result, nodePath);
	}
	
	
	public static int getInt(String key, TXMParameters commandParameters, TXMResult result) {
		return getInt(key, commandParameters, result, result.getCommandPreferencesNodePath());
	}
	
	/**
	 * Looks for the value of the specified <code>key</code> in preference nodes.
	 * Returns the value of the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns defaultValue if no value has been found for the specified key.
	 * 
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static int getInt(String key, TXMResult result, String nodePath, int defaultValue) {
		nodePath = findNodePath(nodePath, result, key);
		
		// int defaultValue = DefaultScope.INSTANCE.getNode(nodePath).getInt(key, 0);
		return preferencesRootNode.node(nodePath).getInt(key, defaultValue);
		
	}
	
	/**
	 * 
	 * @param key
	 * @param result
	 * @param nodePath
	 * @return
	 */
	public static int getInt(String key, TXMResult result, String nodePath) {
		return getInt(key, result, nodePath, 0);
	}
	
	/**
	 *
	 * @param nodePath
	 * @param key
	 * @return
	 */
	public static int getInt(String key, String nodePath) {
		return getInt(key, null, nodePath);
	}
	
	/**
	 *
	 * @param result
	 * @param key
	 * @return
	 */
	public static int getInt(String key, TXMResult result) {
		return getInt(key, result, null);
	}
	
	
	
	
	/**
	 * Looks for the value of the specified <code>key</code> in the specified parameters and preference nodes.
	 * Returns the value of a key in the specified parameters if exists.
	 * Otherwise try to get it from the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns 0L if no value has been found for the specified key.
	 * 
	 * @param commandParameters
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static long getLong(String key, TXMParameters commandParameters, TXMResult result, String nodePath) {
		if (commandParameters != null && commandParameters.get(key) != null) {
			return (Long) commandParameters.get(key);
		}
		return getLong(key, result, nodePath);
	}
	
	public static long getLong(String key, TXMParameters commandParameters, TXMResult result) {
		return getLong(key, commandParameters, result, result.getCommandPreferencesNodePath());
	}
	
	
	
	/**
	 * Looks for the value of the specified <code>key</code> in preference nodes.
	 * Returns the value of the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns defaultValue if no value has been found for the specified key.
	 * 
	 * @param nodePath
	 * @param result
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	public static long getLong(String key, TXMResult result, String nodePath, long defaultValue) {
		nodePath = findNodePath(nodePath, result, key);
		
		// defaultValue = DefaultScope.INSTANCE.getNode(nodePath).getLong(key, defaultValue);
		return preferencesRootNode.node(nodePath).getLong(key, defaultValue);
		
	}
	
	/**
	 * Looks for the value of the specified <code>key</code> in preference nodes.
	 * Returns the value of the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns 0L if no value has been found for the specified key.
	 * 
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static long getLong(String key, TXMResult result, String nodePath) {
		return getLong(key, result, nodePath, 0L);
	}
	
	/**
	 *
	 * @param nodePath
	 * @param key
	 * @return
	 */
	public static long getLong(String key, String nodePath) {
		return getLong(key, null, nodePath);
	}
	
	/**
	 *
	 * @param result
	 * @param key
	 * @return
	 */
	public static long getLong(String key, TXMResult result) {
		return getLong(key, result, null);
	}
	
	
	/**
	 * Looks for the value of the specified <code>key</code> in the specified parameters and preference nodes.
	 * Returns the value of a key in the specified parameters if exists.
	 * Otherwise try to get it from the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns 0.0d if no value has been found for the specified key.
	 * 
	 * @param commandParameters
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static double getDouble(String key, TXMParameters commandParameters, TXMResult result, String nodePath) {
		if (commandParameters != null && commandParameters.get(key) != null) {
			return (Double) commandParameters.get(key);
		}
		return getDouble(key, result, nodePath);
	}
	
	public static double getDouble(String key, TXMParameters commandParameters, TXMResult result) {
		return getDouble(key, commandParameters, result, result.getCommandPreferencesNodePath());
	}
	
	
	/**
	 * Looks for the value of the specified <code>key</code> in preference nodes.
	 * Returns the value of the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns defaultValue if no value has been found for the specified key.
	 * 
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static double getDouble(String key, String nodePath, TXMResult result, double defaultValue) {
		nodePath = findNodePath(nodePath, result, key);
		
		// defaultValue = DefaultScope.INSTANCE.getNode(nodePath).getDouble(key, defaultValue);
		return preferencesRootNode.node(nodePath).getDouble(key, defaultValue);
	}
	
	/**
	 * Looks for the value of the specified <code>key</code> in preference nodes.
	 * Returns the value of the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns 0.0d if no value has been found for the specified key.
	 * 
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static double getDouble(String key, TXMResult result, String nodePath) {
		return getDouble(key, nodePath, result, 0.0d);
	}
	
	
	/**
	 *
	 * @param nodePath
	 * @param key
	 * @return
	 */
	public static double getDouble(String key, String nodePath) {
		return getDouble(key, null, nodePath);
	}
	
	
	/**
	 *
	 * @param result
	 * @param key
	 * @return
	 */
	public static double getDouble(String key, TXMResult result) {
		return getDouble(key, result, null);
	}
	
	
	
	
	/**
	 * Looks for the value of the specified <code>key</code> in the specified parameters and preference nodes.
	 * Returns the value of a key in the specified parameters if exists.
	 * Otherwise try to get it from the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns 0f if no value has been found for the specified key.
	 * 
	 * @param commandParameters
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static float getFloat(String key, TXMParameters commandParameters, TXMResult result, String nodePath) {
		if (commandParameters != null && commandParameters.get(key) != null) {
			return (Float) commandParameters.get(key);
		}
		return getFloat(key, result, nodePath);
	}
	
	public static float getFloat(String key, TXMParameters commandParameters, TXMResult result) {
		return getFloat(key, commandParameters, result, result.getCommandPreferencesNodePath());
	}
	
	
	
	/**
	 * Looks for the value of the specified <code>key</code> in preference nodes.
	 * Returns the value of the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns 0f if no value has been found for the specified key.
	 * 
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static float getFloat(String key, TXMResult result, String nodePath) {
		return getFloat(key, nodePath, result, 0f);
	}
	
	
	/**
	 *
	 * @param nodePath
	 * @param key
	 * @return
	 */
	public static float getFloat(String key, String nodePath) {
		return getFloat(key, null, nodePath);
	}
	
	/**
	 *
	 * @param result
	 * @param key
	 * @return
	 */
	public static float getFloat(String key, TXMResult result) {
		return getFloat(key, result, null);
	}
	
	/**
	 * Looks for the value of the specified <code>key</code> in preference nodes.
	 * Returns the value of the local result node if exists.
	 * Otherwise try to get it from the specified node qualifier if exists.
	 * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	 * Returns defaultValue if no value has been found for the specified key.
	 * 
	 * @param nodePath
	 * @param result
	 * @param key
	 * @return
	 */
	public static float getFloat(String key, String nodePath, TXMResult result, float defaultValue) {
		nodePath = findNodePath(nodePath, result, key);
		
		// defaultValue = DefaultScope.INSTANCE.getNode(nodePath).getFloat(key, defaultValue);
		return preferencesRootNode.node(nodePath).getFloat(key, defaultValue);
	}
	
	
	public static Serializable toSerializable(byte[] bytes) {
		
		try {
			ByteArrayInputStream breader = new ByteArrayInputStream(bytes);
			ObjectInputStream reader = new ObjectInputStream(breader);
			
			Object o = reader.readObject();
			reader.close();
			breader.close();
			return (Serializable) o;
		}
		catch (Exception e) {
			Log.severe(NLS.bind("Error while loading bytes: {0}", e.getLocalizedMessage()));
			Log.printStackTrace(e);
		}
		return null;
	}
	
	public static Serializable getSerializable(String key, String nodePath, TXMResult result, Serializable defaultValue) {
		
		try {
			byte[] defaultBytes = toByteArray(defaultValue);
			
			nodePath = findNodePath(nodePath, result, key);
			
			defaultBytes = DefaultScope.INSTANCE.getNode(nodePath).getByteArray(key, defaultBytes);
			byte[] bytes = preferencesRootNode.node(nodePath).getByteArray(key, defaultBytes);
			
			return toSerializable(bytes);
		}
		catch (Exception e) {
			System.out.println(NLS.bind("Error while reading {0} serialized preference: {1}", key, e.getLocalizedMessage()));
			Log.printStackTrace(e);
		}
		return null;
	}
	

	/**
	 * Checks if a local node exists for the specified TXM result.
	 * 
	 * @param result
	 * @return <code>true</code> if the node exists, otherwise <code>false</code>
	 */
	public static boolean resultScopeNodeExists(TXMResult result) {
		// FIXME: SJ: this code seems to create the node but it shouldn't
		return preferencesRootNode.node(result.getParametersNodePath()) != null;
		// FIXME: SJ: retester que ça ne crée pas le nom
		// Try this one instead
		// try {
		// return preferencesRootNode.nodeExists(result.getParametersNodePath());
		// }
		// catch (BackingStoreException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// return false;
	}
	
	/**
	 * 
	 * @param nodePath
	 * @param key
	 * @return
	 */
	public static boolean keyExists(String nodePath, String key) {
		
		// if (!nodePath.startsWith("/instance")) {
		// nodePath = "/instance/" + nodePath;
		// }
		
		
		if (preferencesRootNode.node(nodePath) != null) {
			try {
				return Arrays.asList(preferencesRootNode.node(nodePath).keys()).contains(key);
			}
			catch (BackingStoreException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
	/**
	 * Checks if a preference is defined as empty.
	 * Can be used to determine whatever a functionality is available or not by overriding the preference as empty.
	 * Essentially used for UI purpose, eg. to hide or disable a button that modify a preference.
	 * 
	 * @param nodePath
	 * @param key
	 * @return
	 */
	public static boolean isEmpty(String nodePath, String key) {
		return getString(key, nodePath).equals(""); //$NON-NLS-1$
	}
	
	
	/**
	 * Sets a preference as empty.
	 * Can be used to determine whatever a functionality is available or not by overriding the preference as empty.
	 * Essentially used for UI purpose, eg. to hide or disable a button that modify a preference.
	 * 
	 * @param nodePath
	 * @param key
	 */
	public static void setEmpty(String nodePath, String key) {
		if (!nodePath.startsWith("/instance")) { //$NON-NLS-1$
			nodePath = "/instance/" + nodePath; //$NON-NLS-1$
		}
		preferencesRootNode.node(nodePath).put(key, ""); //$NON-NLS-1$
	}
	
	/**
	 * Gets the default preference node of the initializer instance.
	 * 
	 * @return
	 */
	public IEclipsePreferences getDefaultPreferencesNode() {
		return DefaultScope.INSTANCE.getNode(this.commandPreferencesNodeQualifier);
	}
	
	/**
	 * print TxmPreferences in the console.
	 */
	public static String dumpToString(String nodePath) {
		StringBuilder sb = new StringBuilder();
		Preferences preferences = preferencesRootNode.node(nodePath);
		
		try {
			String[] keys = preferences.keys();
			for (int i = 0; i < keys.length; i++) {
				sb.append(keys[i] + " = " + preferences.get(keys[i], "") + "\t"); //$NON-NLS-1$
			}
		}
		catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	/**
	 * print TxmPreferences in the console.
	 */
	public static Preferences getNode(String nodePath) {
		return preferencesRootNode.node(nodePath);
	}
	
	/**
	 * print TxmPreferences in the console.
	 */
	public static void dump() {
		dump(System.out);
	}
	
	/**
	 * print TxmPreferences in the output printstream parameter.
	 * 
	 * @param out output printstream !! must be closed after used
	 */
	public static void dump(PrintStream out) {
		try {
			out.println("root:"); //$NON-NLS-1$
			for (String children : preferencesRootNode.childrenNames()) {
				out.println("********************************************************************************************************"); //$NON-NLS-1$
				out.println(" scope: " + children); //$NON-NLS-1$
				String[] subchildren = preferencesRootNode.node(children).childrenNames();
				Arrays.sort(subchildren);
				for (String children2 : subchildren) {
					out.println(" scope: " + children + "  node: " + children2); //$NON-NLS-1$
					String[] keys = preferencesRootNode.node(children).node(children2).keys();
					Arrays.sort(keys);
					for (String key2 : keys) {
						out.println("          " + key2 + " = " + preferencesRootNode.node(children).node(children2).get(key2, null)); //$NON-NLS-1$ //$NON-NLS-2$
					}
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Dumps the keys and values of the specified node of the specified scope.
	 * 
	 * @param nodePath
	 */
	public static void dump(IScopeContext scope, String nodePath) {
		IEclipsePreferences preferences = scope.getNode(nodePath);
		System.out.println("TXMPreferences.dump(): ***************************************************************"); //$NON-NLS-1$
		System.out.println("TXMPreferences.dump(): node qualifier = " + nodePath); //$NON-NLS-1$
		System.out.println("TXMPreferences.dump():" + preferences.absolutePath()); //$NON-NLS-1$
		try {
			String[] keys = preferences.keys();
			for (int i = 0; i < keys.length; i++) {
				System.out.println("TXMPreferences.dump(): " + keys[i] + " = " + preferences.get(keys[i], ""));
			}
		}
		catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Dumps the keys and values of the specified node of the instance and default scopes.
	 * 
	 * @param nodePath
	 */
	public static void dump(String nodePath) {
		dump(DefaultScope.INSTANCE, nodePath);
		dump(InstanceScope.INSTANCE, nodePath);
	}
	
	
	/**
	 * 
	 * @param nodePath
	 * @throws BackingStoreException
	 */
	public static String[] getKeys(IScopeContext scope, String nodePath) throws BackingStoreException {
		IEclipsePreferences preferences = scope.getNode(nodePath);
		return preferences.keys();
	}
	
	/**
	 * 
	 * @param nodePath
	 * @return
	 * @throws BackingStoreException
	 */
	public static String[] getCommandScopeKeys(String nodePath) throws BackingStoreException {
		return getKeys(InstanceScope.INSTANCE, nodePath);
	}
	
	/**
	 * 
	 * @param nodePath
	 * @return
	 * @throws BackingStoreException
	 */
	public static String[] getDefaultScopeKeys(String nodePath) throws BackingStoreException {
		return getKeys(DefaultScope.INSTANCE, nodePath);
	}
	
	/**
	 * Gets the keys and values of the specified node path in the specified scope context as Map.
	 * 
	 * @param scope
	 * @param nodePath
	 * @return
	 */
	public static HashMap<String, Object> getKeysAndValuesAsMap(IScopeContext scope, String nodePath) {
		
		HashMap<String, Object> str = new HashMap<>();
		
		IEclipsePreferences preferences = scope.getNode(nodePath);
		
		try {
			String[] keys = preferences.keys();
			Arrays.sort(keys);
			for (int i = 0; i < keys.length; i++) {
				str.put(keys[i], preferences.get(keys[i], "")); //$NON-NLS-1$
			}
		}
		catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return str;
	}
	
	/**
	 * Gets the keys and values of the specified node path as string.
	 * 
	 * @param nodePath
	 * @return
	 */
	public static String getKeysAndValues(String nodePath) {
		
		StringBuilder str = new StringBuilder();
		
		Preferences preferences = preferencesRootNode.node(nodePath);
		
		str.append("Path = " + preferences.absolutePath() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		str.append("Name = " + preferences.name() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		
		try {
			String[] keys = preferences.keys();
			Arrays.sort(keys);
			for (int i = 0; i < keys.length; i++) {
				str.append(keys[i] + " = " + preferences.get(keys[i], "") + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return str.toString();
	}
	
	
	
	
	/**
	 * Dumps the keys and values of the specified node, of the specified result node and also of the alternative nodes.
	 * 
	 * @param nodePath
	 * @param result
	 */
	public static void dump(String nodePath, TXMResult result) {
		
		// result scope node
		if (result != null) {
			if (resultScopeNodeExists(result)) {
				Log.finest("TXMPreferences.dump(): Result scope preferences for node " + result.getParametersNodePath() + ":"); //$NON-NLS-1$ //$NON-NLS-2$
				dump(result.getParametersNodePath());
			}
			else {
				Log.severe("TXMPreferences.dump(): No result scope preferences was found for node" + result.getParametersNodePath() + "."); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		// command scope node
		if (nodePath != null) {
			Log.finest("TXMPreferences.dump(): Command scope preferences for node " + nodePath + ":"); //$NON-NLS-1$ //$NON-NLS-2$
			dump(nodePath);
		}
		else {
			Log.finest("TXMPreferences.dump(): No Command scope was asked to dump."); //$NON-NLS-1$
		}
		
		// alternative global scope nodes
		for (int i = 0; i < additionalNodesQualifiers.size(); i++) {
			Log.finest("TXMPreferences.dump(): Alternative global scope preferences for node " + additionalNodesQualifiers.get(i) + ":"); //$NON-NLS-1$ //$NON-NLS-2$
			dump(additionalNodesQualifiers.get(i));
		}
	}
	
	
	
	/**
	 * Deletes the local node if exists and the associated .prefs persistence file.
	 * 
	 * @param result
	 */
	public static void delete(TXMResult result) {
		delete(preferencesRootNode.node(result.getParametersNodePath()));
		Log.finest("TXMPreferences.delete(): Local preferences node for object " + result.getParametersNodePath() + " deleted."); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	
	/**
	 * Deletes the specified node if exists and the associated .prefs file.
	 * 
	 * @param node
	 */
	public static void delete(Preferences node) {
		try {
			node.clear();
			node.flush();
			node.removeNode();
		}
		catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	/**
	 * Gets all the result nodes qualifiers (from scope or *.prefs files starting with TXMResult.UUID_PREFIX, eg.: "txm_res_").
	 * 
	 * @return
	 */
	// FIXME: old method, from Instance scope
	// public static ArrayList<String> getAllResultsNodesQualifiers() {
	// ArrayList<String> resultsNodesQualifiers = new ArrayList<String>();
	// try {
	// IPreferencesService service = Platform.getPreferencesService();
	// for (String scopeName : service.getRootNode().childrenNames()) {
	// if(scopeName != TXMPreferences.scope.getName()) {
	// continue;
	// }
	//
	// String[] nodesNames = service.getRootNode().node(scopeName).childrenNames();
	// Arrays.sort(nodesNames);
	// for (String nodeName : nodesNames) {
	// if(nodeName.startsWith(TXMResult.UUID_PREFIX)) {
	// resultsNodesQualifiers.add(nodeName);
	// }
	// }
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// Collections.sort(resultsNodesQualifiers);
	// return resultsNodesQualifiers;
	// }
	
	/**
	 * Gets all the result nodes qualifiers from the specified node path (from the scope or *.prefs files starting with TXMResult.UUID_PREFIX, eg.: "txm_res_").
	 * 
	 * @return
	 */
	// FIXME: try to use the scope directly and not pass through the reference service
	public static ArrayList<String> getAllResultsNodePaths(String rootPath) {
		Log.finest("TXMPreferences.getAllResultsNodePaths(): looking for nodes in path \"" + preferencesRootNode.node(rootPath) + "\".");
		
		ArrayList<String> nodePaths = new ArrayList<>();
		
		try {
			String[] nodesNames = preferencesRootNode.node(rootPath).childrenNames();
			
			Log.finest("TXMPreferences.getAllResultsNodePaths(): " + nodesNames.length + " node(s) found.");
			
			Arrays.sort(nodesNames);
			for (String nodeName : nodesNames) {
				// Debug
				// Log.finest("TXMPreferences.getAllResultsNodePaths(): node name = " + nodeName);
				
				if (nodeName.startsWith(TXMResult.UUID_PREFIX)) {
					// Debug
					// Log.finest("TXMPreferences.getAllResultsNodePaths(): node path = " + rootPath + nodeName);
					nodePaths.add(rootPath + nodeName);
				}
			}
			
			Collections.sort(nodePaths);
		}
		catch (BackingStoreException e) {
			e.printStackTrace();
		}
		return nodePaths;
		
	}
	
	
	// FIXME: became useless?
	// /**
	// * Deletes all the result nodes from the preferences and files that are not set as persistable.
	// */
	// public static void deleteAllNotPersistableResultsNodes(String path) {
	//
	// ArrayList<String> resultsNodesQualifier = getAllResultsNodePaths(path);
	// for (int i = 0; i < resultsNodesQualifier.size(); i++) {
	// TXMResult result = TXMResult.getResult(resultsNodesQualifier.get(i));
	// if(result != null && !result.isPersistable()) {
	// delete(preferencesRootNode.node(path).node(resultsNodesQualifier.get(i)));
	// }
	// }
	// }
	
	// FIXME: became useless?
	// /**
	// * Deletes all the result nodes from the preferences and files.
	// */
	// public static void deleteAllResultsNodes(String path) {
	//
	// ArrayList<String> resultsNodesQualifier = getAllResultsNodePaths(path);
	// for (int i = 0; i < resultsNodesQualifier.size(); i++) {
	// delete(preferencesRootNode.node(path).node(resultsNodesQualifier.get(i)));
	// }
	// }
	
	
	/**
	 * Internal method to find the preference node path of a result otherwise look in instance, default scopes and in alternatives nodes qualifiers.
	 *
	 * @param result
	 * @param key
	 */
	public static String findNodePath(String nodePath, TXMResult result, String key) {
		
		boolean keyExists = (result != null && keyExists(result.getParametersNodePath(), key));
		
		
		//FIXME: SJ: there is a problem with the looking order? the default command preference should be tested before all alternative preferences nodes?
		// eg. if I put preferences.putBoolean(SHOW_TITLE, false); in command preference, it doesn't work since the ChartsEnginePreference is returned before this one
		
		// original code
		// get the result scope preference (formed as "project/project_name/uuid_class")
//		if (keyExists) {
//			return result.getParametersNodePath();
//		}
//		// look in the instance scope (user command preference)
//		else if (keyExists("instance/" + nodePath, key)) {
//			return "instance/" + nodePath;
//		}
//		// look in the global alternative preferences nodes
//		else {
//			for (int i = 0; i < additionalNodesQualifiers.size(); i++) {
//				// user preference
//				if (keyExists("instance/" + additionalNodesQualifiers.get(i), key)) {
//					return "instance/" + additionalNodesQualifiers.get(i);
//				}
//				// default preference
//				else if (keyExists("default/" + additionalNodesQualifiers.get(i), key)) {
//					return "default/" + additionalNodesQualifiers.get(i);
//				}
//			}
//			// default command preference
//			return "default/" + nodePath;
//		}
		
		
		// test with changed order
		// get the result scope preference (formed as "project/project_name/uuid_class")
		if (keyExists) {
			return result.getParametersNodePath();
		}
		// look in the instance scope (user command preference)
		else if (keyExists("instance/" + nodePath, key)) { //$NON-NLS-1$
			return "instance/" + nodePath; //$NON-NLS-1$
		}
		// look in the default scope (default command preference)
		else if (keyExists("default/" + nodePath, key)) { //$NON-NLS-1$
			return "default/" + nodePath; //$NON-NLS-1$
		}
		// look in the global alternative preferences nodes
		else {
			for (int i = 0; i < additionalNodesQualifiers.size(); i++) {
				// user preference
				if (keyExists("instance/" + additionalNodesQualifiers.get(i), key)) { //$NON-NLS-1$
					return "instance/" + additionalNodesQualifiers.get(i); //$NON-NLS-1$
				}
				// default preference
				else if (keyExists("default/" + additionalNodesQualifiers.get(i), key)) { //$NON-NLS-1$
					return "default/" + additionalNodesQualifiers.get(i); //$NON-NLS-1$
				}
			}
		}
		// default command preference
		return "default/" + nodePath; //$NON-NLS-1$
	}
	
	
	// /**
	// * Looks for the value of the specified <code>key</code> in preference nodes.
	// * Returns the value of the local result node if exists.
	// * Otherwise try to get it from the specified node qualifier if exists.
	// * Otherwise try to get it from the specified alternative node qualifiers defined in the list <code>TXMPreferences.alternativeNodesQualifiers</code>.
	// * Returns defaultValue if no value has been found for the specified key.
	// *
	// * @param nodePath
	// * @param result
	// * @param key
	// * @param defaultValue
	// * @return
	// */
	// public static long getLong(String key, TXMResult result, String nodePath, long defaultValue) {
	// if(nodePath != null) {
	// defaultValue = DefaultScope.INSTANCE.getNode(nodePath).getLong(key, defaultValue);
	// }
	//
	// nodePath = findNodePath(nodePath, result, key);
	//
	// return preferencesRootNode.node(nodePath).getLong(key, defaultValue);
	//
	// }
	//
	//
	
	
	
	/**
	 * Stores the specified parameters pairs of key / value in a local node dedicated to the specified result. The node qualifier is generated by the <code>Object.toString()</code> method.
	 * 
	 * @param result
	 * @param parameters
	 */
	// FIXME: SJ: became useless?
	// public static void putLocalParameters(TXMResult result, TXMParameters parameters) {
	// Set keys = parameters.keySet();
	// Iterator it = keys.iterator();
	// while (it.hasNext()){
	// Object key = it.next();
	// Object value = parameters.get(key);
	// if(value != null) {
	// putLocal(result, (String) key, value);
	// }
	// }
	// }
	
	/**
	 * Puts a preference in the specified node.
	 * 
	 * @param nodePath
	 * @param key
	 * @param value
	 */
	public static void put(String nodePath, String key, Object value) {
		if (!nodePath.startsWith("/project") && !nodePath.startsWith("/instance")) { //$NON-NLS-1$
			nodePath = "/instance/" + nodePath; //$NON-NLS-1$
		}
		if (Integer.class.isInstance(value)) {
			preferencesRootNode.node(nodePath).putInt(key, (Integer) value);
		}
		else if (Double.class.isInstance(value)) {
			preferencesRootNode.node(nodePath).putDouble(key, (Double) value);
		}
		else if (Float.class.isInstance(value)) {
			preferencesRootNode.node(nodePath).putFloat(key, (Float) value);
		}
		else if (Long.class.isInstance(value)) {
			preferencesRootNode.node(nodePath).putLong(key, (Long) value);
		}
		else if (String.class.isInstance(value)) {
			preferencesRootNode.node(nodePath).put(key, (String) value);
		}
		else if (Boolean.class.isInstance(value)) {
			preferencesRootNode.node(nodePath).putBoolean(key, (Boolean) value);
		}
		else if (File.class.isInstance(value)) {
			preferencesRootNode.node(nodePath).put(key, ((File) value).getPath()); // keep path as it is (relative or absolute)
		}
		else if (Serializable.class.isInstance(value)) {
			try {
				preferencesRootNode.node(nodePath).put(key, toString((Serializable) value));
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// // FIXME: to do, MD: not for now
		// // FIXME: SJ: need to prove/validate this method
		// else if (byte.class.isAssignableFrom(value.getClass().getComponentType())) {
		// System.err.println("TXMPreferences.put(): byte array -> need to prove/validate this method.");
		// scope.getNode(nodePath).putByteArray(key, (byte[]) value);
		// }
		else if (value != null) {
			preferencesRootNode.node(nodePath).put(key, value.toString());
			
			// FIXME: to do if needed
			// FIXME: Debug
			// System.err.println("TXMPreferences.put(): error, can't find a put method that matches the value type: " + value.getClass() + "=" + value + ".");
		}
	}
	
	
	
	
	/**
	 * 
	 * @param key
	 * @param value
	 */
	public void put(String key, Object value) {
		put(this.commandPreferencesNodeQualifier, key, value);
	}
	
	
	/**
	 * Stores the specified parameters pairs of key/value in a local node dedicated to the specified result.
	 * 
	 * @param result
	 * @param key
	 * @param value
	 */
	public static void putLocal(TXMResult result, String key, Object value) {
		put(result.getParametersNodePath(), key, value);
	}
	
	
	// FIXME: TO do if needed
	private static void putLocalSerializable(TXMResult result, String key, Serializable value) {
		byte[] b = toByteArray(value);
		preferencesRootNode.node(result.getParametersNodePath()).putByteArray(key, b);
	}
	
	
	
	
	
	
	/** Read the object from Base64 string. */
	private static Serializable fromString(String s) throws IOException,
			ClassNotFoundException {
		
		byte[] data = Base64.getDecoder().decode(s);
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
		Object o = ois.readObject();
		ois.close();
		return (Serializable) o;
	}
	
	/** Write the object to a Base64 string. */
	public static String toString(Serializable o) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(o);
		oos.close();
		return Base64.getEncoder().encodeToString(baos.toByteArray());
	}
	
	/**
	 * Decode a Base64 String to a Serializable object
	 */
	public static Serializable toSerializable(String str) {
		
		return toSerializable(Base64.getDecoder().decode(str));
	}
	
	//
	// /**
	// *
	// * @param nodePath
	// * @param key
	// * @param value
	// */
	// public static void putByteArray(String nodePath, String key, byte[] value) {
	// scope.getNode(nodePath).putByteArray(key, value);
	// }
	
	
	
	
	public static byte[] toByteArray(Serializable s) {
		try {
			ByteArrayOutputStream bwriter = new ByteArrayOutputStream();
			ObjectOutputStream writer = new ObjectOutputStream(bwriter);
			writer.writeObject(s);
			return bwriter.toByteArray();
		}
		catch (IOException e) {
			System.out.println("Error while saving " + s + ": " + e.getLocalizedMessage()); //$NON-NLS-1$
			Log.printStackTrace(e);
		}
		return null;
	}
	
	
	/**
	 * Clones nodes. Creates a local node for the destination result and copy it all the source result parameters.
	 * 
	 * @param srcResult node path
	 * @param dstResult node path
	 */
	public static void cloneNode(String srcResult, String dstResult) {
		Preferences preferences = preferencesRootNode.node(srcResult);
		Preferences dstPreferences = preferencesRootNode.node(dstResult);
		if (preferences != null) {
			try {
				for (String key : preferences.keys()) {
					dstPreferences.put(key, preferences.get(key, "")); //$NON-NLS-1$
				}
				dstPreferences.flush();
			}
			catch (BackingStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Clones nodes. Creates a local node for the destination result and copy it all the source result parameters.
	 * 
	 * @param srcResult
	 * @param dstResult
	 */
	public static void cloneNode(TXMResult srcResult, TXMResult dstResult) {
		cloneNode(srcResult.getParametersNodePath(), dstResult.getParametersNodePath());
	}
	
	public Date getDate(String key) {
		// if (this.pCreationDate != null) {
		// this.saveParameter(TBXPreferences.CREATE_DATE, Toolbox.dateformat.format(this.pCreationDate));
		// }
		//
		// if (this.pLastUpdateDate != null) {
		// this.saveParameter(TBXPreferences.UPDATE_DATE, Toolbox.dateformat.format(this.pLastUpdateDate));
		// }
		// String date = this.getStringParameterValue(TBXPreferences.CREATE_DATE);
		// if (date !=null && date.length() > 0) {
		// this.pCreationDate = Toolbox.dateformat.parse(date);
		// } else {
		// this.pCreationDate = new Date(0);
		// }
		String date = this.getString(key);
		if (date != null && date.length() > 0) {
			try {
				return TXMResult.ID_TIME_FORMAT.parse(date);
			}
			catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		return new Date(0);
	}
	
	
	public void setDate(String key, Date date) {
		this.put(key, TXMResult.ID_TIME_FORMAT.format(date));
	}
	
	/**
	 * Saves the local preferences to a file.
	 * 
	 * @param result
	 * @throws IOException
	 */
	public static boolean flush(TXMResult result, File parametersFile) throws IOException {
		
//		// FIXME: less wtf version... but does not work -> the properties name is absolute instead of local : "/project/VOEUX/txm_res_20230530_161548166_0000_Concordance/class=" instead of "class"
//		IPreferencesService service = org.eclipse.core.runtime.Platform.getPreferencesService();
//		FileOutputStream output = null;
//		try {
//			output = new FileOutputStream(parametersFile);
//			service.exportPreferences((IEclipsePreferences) preferencesRootNode.node(result.getParametersNodePath()), output, null);
//			output.close();
//		}
//		catch (Exception e1) {
//			Log.severe(NLS.bind("Error: parameters NOT exported to the {0} file: {1}.", parametersFile, e1));
//			e1.printStackTrace();
//			if (output !=null) {
//				output.close();
//			}
//			return false;
//		}
//		//return true;
		
		
		// FIXME: wtf version...
		try {
			preferencesRootNode.node(result.getParametersNodePath()).flush();
			
			Project p = result.getProject();
			String store = p.getPreferencesScope().getLocation().toString();
			String internalPath = result.getParametersNodePath();
			String prefix = "/project/" + p.getRCPProject().getName(); //$NON-NLS-1$
			if (internalPath.startsWith(prefix)) {
				internalPath = internalPath.substring(prefix.length());
			}
			File prefFile = new File(store, internalPath + ".prefs"); //$NON-NLS-1$
			if (prefFile.exists()) {
				parametersFile.delete();
				FileCopy.copy(prefFile, parametersFile);
				if (parametersFile.exists()) {
					return true;
				}
				else {
					System.out.println(NLS.bind("** Error: could not write parameters to {0} file.", parametersFile));
				}
			}
			else {
				System.out.println(NLS.bind("** Error: internal preference {0} file not found.", prefFile));
			}
		}
		catch (BackingStoreException e) {
			System.out.println(NLS.bind("Parameters not exported to the {0} file: {1}.", parametersFile, e));
			e.printStackTrace();
			Log.severe(e.getMessage());
		}
		return true;
	}
	
	
	/**
	 * Saves the preference node of the specified result into file.
	 * 
	 * @param result
	 */
	public static void flush(TXMResult result) {
		Log.finest("TXMPreferences.flush(): Local preferences for object " + result.getParametersNodePath() + " saved to file."); //$NON-NLS-1$
		flush(result.getParametersNodePath());
	}
	
	/**
	 * Saves the specified node into file.
	 * 
	 */
	public static void flush(String nodePath) {
		try {
			preferencesRootNode.node(nodePath).flush();
		}
		catch (BackingStoreException e) {
			e.printStackTrace();
			Log.severe(e.getMessage());
		}
	}
	
	/**
	 * Saves the preference node of the current instance into file.
	 * Generally the bundle id node path.
	 */
	public void flush() {
		TXMPreferences.flush(this.commandPreferencesNodeQualifier);
	}
	
	
	// FIXME: this code is dedicated to dynamically retrieve the static field PREFERENCES_NODE of the runtime subclass and use it super class to avoid to pass it to all methods.
	// eg. to use ProgressionPreferences.getBoolean("test") instead of TXMPreferences.getBoolean(ProgressionPreferences.PREFERENCES_NODE, "test")
	// but it doesn"t work, need to continue the tests...
	// /**
	// *
	// * @author sjacquot
	// *
	// */
	// public static class CurrentClassGetter extends SecurityManager {
	// public Class getCurrentClass() {
	// //FIXME: debug
	// for(int i = 0; i < getClassContext().length; i++) {
	// System.err.println("TXMPreferences.CurrentClassGetter.getCurrentClass() class " + i + " = " + getClassContext()[i]);
	// }
	// return getClassContext()[2];
	// }
	// }
	//
	//
	// /**
	// * Gets the static field PREFERENCES_NODE of the runtime subclass.
	// * Convenience method to avoid to pass it to all methods.
	// * @return
	// */
	// public static String getNodeQualifier() {
	// String runtimeSubClassNodeQualifier = null;
	// try {
	//
	// //System.err.println("TXMPreferences.getNodeQualifier() class name using thread stack = " + Thread.currentThread().getStackTrace()[1].getClassName());
	// System.err.println("TXMPreferences.getNodeQualifier() class name using encolsing class = " + new Object() { }.getClass().getEnclosingClass());
	//
	//
	// Class c = new CurrentClassGetter().getCurrentClass();
	// System.out.println("TXMPreferences.getNodeQualifier(): class = " + c);
	// Field f = c.getDeclaredField("PREFERENCES_NODE");
	// f.setAccessible(true);
	// if(f.isAccessible()) {
	// runtimeSubClassNodeQualifier = (String) f.get(null);
	// }
	// }
	// catch (NoSuchFieldException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// catch (SecurityException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// catch (IllegalArgumentException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// catch (IllegalAccessException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// //System.out.println("TXMPreferences.getNodeQualifier() runtime class: " + );
	//// // FIXME: new tests with bundles context class loading
	//// HashSet<ClassLoader> loaders = new HashSet<ClassLoader>();
	//// BundleContext bundleContext = InternalPlatform.getDefault().getBundleContext();
	////
	//// for (Bundle b : bundleContext.getBundles()) {
	//// BundleWiring bundleWiring = b.adapt(BundleWiring.class);
	//// loaders.add(bundleWiring.getClassLoader());
	////
	//// //FIXME: debug
	//// System.out.println("GSERunner.createGSERunner(): class loader added: " + bundleWiring.getClassLoader());
	//// }
	////
	//
	// return runtimeSubClassNodeQualifier;
	// }
	//
	//
	// public static String getString(String key) {
	//// Class c = new CurrentClassGetter().getCurrentClass();
	//// System.out.println("TXMPreferences.getString(): class = " + c);
	// return getString(getNodeQualifier(), key);
	// }
	//
	//// /**
	//// *
	//// * @param key
	//// * @return
	//// */
	//// public static String getString(String key) {
	//// return getString(getNodeQualifier(), key);
	//// }
	////
	// end of fixme
	
	public String getProjectPreferenceValue(Project project, String key) {
		
		String defaultValue = this.getString(key);
		return getProjectPreferenceValue(project, key, defaultValue);
	}
	
	public String getProjectPreferenceValue(Project project, String key, String defaultValue) {
		
		if (project.getPreferencesScope() == null) return defaultValue;
		
		Preferences node = project.getPreferencesScope().getNode(commandPreferencesNodeQualifier);
		return node.get(key, defaultValue);
	}
	
	public void setProjectPreferenceValue(Project project, String key, String value) {
		
		if (project.getPreferencesScope() == null) return;
		
		Preferences node = project.getPreferencesScope().getNode(commandPreferencesNodeQualifier);
		node.put(key, value);
	}
	
}
