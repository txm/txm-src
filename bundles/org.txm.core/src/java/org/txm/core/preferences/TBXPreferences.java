package org.txm.core.preferences;

import java.util.Locale;

import org.osgi.service.prefs.Preferences;

public class TBXPreferences extends TXMPreferences {

	public static String VERSION = "version"; //$NON-NLS-1$

	public static final String FETCH_UPDATES_AT_STARTUP = "fetch_updates_at_startup"; //$NON-NLS-1$

	/** The Constants for Toolbox properties. */
	public static final String LOG_LEVEL = "log_level"; //$NON-NLS-1$

	public static final String ADD_TECH_LOGS = "log_in_console"; //$NON-NLS-1$

	public static final String SHOW_TIMINGS = "show_timings"; //$NON-NLS-1$

	public static final String LOG_DIR = "log_directory"; //$NON-NLS-1$

	public static final String LOG_IN_FILE = "log_in_file"; //$NON-NLS-1$

	/**
	 * Display the
	 */
	public static final String EXPERT_USER = "txm_expert_user"; //$NON-NLS-1$
	
	/**
	 * Hide options in TXM interface to simplify the learning of TXM usage
	 */
	public static final String BEGINNER_USER = "txm_beginner_user"; //$NON-NLS-1$

	public static final String LOG_STACKTRACE = "log_stacktrace"; //$NON-NLS-1$

	/** The Constant SCRIPT_ROOT_DIR. */
	public static final String SCRIPT_ROOT_DIR = "script_root_dir"; //$NON-NLS-1$

	/** The Constant SCRIPT_CURRENT_DIR. */
	public static final String SCRIPT_CURRENT_DIR = "script_current_dir"; //$NON-NLS-1$

	/** The Constant UI_LOCALE. */
	public static final String UI_LOCALE = "ui_locale"; //$NON-NLS-1$

	/** The Constant INSTALL_DIR. */
	public static final String INSTALL_DIR = "install_dir"; //$NON-NLS-1$

	/** The Constant USER_TXM_HOME. */
	public static final String USER_TXM_HOME = "user_txm_home"; //$NON-NLS-1$

	public static final String METADATA_ENCODING = "metadata_encoding"; //$NON-NLS-1$

	public static final String METADATA_COLSEPARATOR = "metadata_colseparator"; //$NON-NLS-1$

	public static final String METADATA_TXTSEPARATOR = "metadata_txtseparator"; //$NON-NLS-1$

	public static final String EMPTY_PROPERTY_VALUE_CODE = "empty_property_value_code"; //$NON-NLS-1$

	public static final String CLIPBOARD_IMPORT_DEFAULT_LANG = "import_default_lang"; //$NON-NLS-1$

	/** The Constant EXPORT_SHOW. */
	public static final String EXPORT_SHOW = "export_show"; //$NON-NLS-1$

	/** The Constant EXPORT_ENCODING. */
	public static final String EXPORT_ENCODING = "export_encoding"; //$NON-NLS-1$

	/** The Constant EXPORT_COLSEPARATOR. */
	public static final String EXPORT_COL_SEPARATOR = "export_colseparator"; //$NON-NLS-1$

	/** The Constant EXPORT_TXTSEPARATOR. */
	public static final String EXPORT_TXT_SEPARATOR = "export_txtseparator"; //$NON-NLS-1$

	public static final String EDITION_DEFINITION_BUILD = "edition_definition_build"; //$NON-NLS-1$

	public static final String EDITION_DEFINITION_PAGINATE = "edition_definition_paginate"; //$NON-NLS-1$

	public static final String EDITION_DEFINITION_WORDS_PER_PAGE = "edition_definition_words_per_page"; //$NON-NLS-1$

	public static final String EDITION_DEFINITION_ENABLE_COLLAPSIBLE_METADATA = "edition_definition_enable_collapsible"; //$NON-NLS-1$

	public static final String EDITION_DEFINITION_PAGE_BREAK_ELEMENT = "edition_definition_page_break_element"; //$NON-NLS-1$

	public static final String EDITION_DEFINITION_IMAGES_DIRECTORY = "edition_definition_images_directory"; //$NON-NLS-1$

	public static final String EDITION_DEFINITION_TOOLTIP_PROPERTIES = "edition_definition_tooltip_properties"; //$NON-NLS-1$

	/**
	 * To show or not all nodes in some views, eg. in CorporaView tree.
	 */
	public static final String SHOW_ALL_RESULT_NODES = "show_all_result_nodes"; //$NON-NLS-1$

	/*
	 * Base URL of TXM update site
	 */
	public static final String UPDATESITE = "update_site"; //$NON-NLS-1$

	/*
	 * Base URL of TXM files site
	 */
	public static final String RAWFILESSITE = "raw_files_site"; //$NON-NLS-1$

	/*
	 * Base URL of TXM files site
	 */
	public static final String FILESSITE = "files_site"; //$NON-NLS-1$

	/**
	 * To enable/disable the results persistence (save after a computing and load at
	 * Toolbox start).
	 */
	public static final String AUTO_PERSISTENCE_ENABLED = "auto_persistence_enabled"; //$NON-NLS-1$

	public static final String VALUE = "value"; //$NON-NLS-1$

	public static final String INDEX = "index"; //$NON-NLS-1$

	public static final String NAME = "name"; //$NON-NLS-1$

	public static final String NAMES = "names"; //$NON-NLS-1$

	public static final String TYPE = "type"; //$NON-NLS-1$

	public static final String SOURCE = "source"; //$NON-NLS-1$

	public static final String XMLTXM = "xmltxm"; //$NON-NLS-1$

	public static final String LANG = "lang"; //$NON-NLS-1$

	/**
	 * start script of the import process of a corpus
	 */
	public static final String IMPORT_MODULE_NAME = "import_module_name"; //$NON-NLS-1$

	/**
	 * corpus alignment configurations
	 */
	public static final String LINKS = "corpus_links"; //$NON-NLS-1$

	public static final String IMPORTXMLFILE = "import_xml_path"; //$NON-NLS-1$

	public static final String FONT = "font"; //$NON-NLS-1$

	public static final String AUTHOR = "author"; //$NON-NLS-1$

	public static final String FRONT_XSL = "front_xsl"; //$NON-NLS-1$

	public static final String DEFAULT_EDITION = "default_edition"; //$NON-NLS-1$

	public static final String FRONT_XSL_PARAMETERS = "front_xsl_parameters"; //$NON-NLS-1$

	public static final String DESCRIPTION = "description"; //$NON-NLS-1$

	public static final String ANNOTATE = "annotate"; //$NON-NLS-1$

	public static final String ID = "id"; //$NON-NLS-1$

	public static final String REF_PROPERTY = "ref_property"; //$NON-NLS-1$

	public static final String SORT_PROPERTY = "sort_property"; //$NON-NLS-1$

	public static final String VIEW_PROPERTY = "view_property"; //$NON-NLS-1$

	public static final String CLEAN = "clean.directories"; //$NON-NLS-1$

	public static final String TTMODEL = "annotate.model"; //$NON-NLS-1$

	public static final String TTANNOTATE = "annotate.run"; //$NON-NLS-1$

	public static final String MULTITHREAD = "multithread"; //$NON-NLS-1$

	public static final String DEBUG = "debug"; //$NON-NLS-1$

	public static final String UPDATECORPUS = "corpus.update"; //$NON-NLS-1$
	public static final String UPDATEEDITIONS = "update_editions"; //$NON-NLS-1$

	public static final String NORMALISEANAVALUES = "normalize.ana.values"; //$NON-NLS-1$

	public static final String NORMALISEATTRIBUTEVALUES = "normalize.attribute.values"; //$NON-NLS-1$

	/**
	 * currently 0.8
	 */
	public static final String CORPUS_VERSION = "corpus_version"; //$NON-NLS-1$
	
	/**
	 * Use to set the TXM version when importing a corpus
	 */
	public static final String TXM_VERSION = "txm_version"; //$NON-NLS-1$

	public static final String CLIPBOARD_IMPORT_ASK_PARAMETERS = "clipboard_import_ask_parameters"; //$NON-NLS-1$

	public static final String QUERY_HISTORY_SIZE = "query_history_size"; //$NON-NLS-1$

	public static final String MEMORY_SIZE = "memory_size"; //$NON-NLS-1$

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(TBXPreferences.class)) {
			new TBXPreferences();
		}
		return TXMPreferences.instances.get(TBXPreferences.class);
	}

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.putBoolean(TBXPreferences.EXPERT_USER, false);
		preferences.putBoolean(TBXPreferences.BEGINNER_USER, false);
		preferences.putBoolean(TBXPreferences.VISIBLE, true);
		preferences.putBoolean(TBXPreferences.CLEAN, true);
		preferences.putBoolean(TBXPreferences.PERSITABLE, false);
		
//		long m = Runtime.getRuntime().maxMemory();
//		if (m < 2000000000) {
//			m = 2000000000;
//		}
//		String mString = ""; //$NON-NLS-1$
//		if (m < 1000) {
//			mString = Long.toString(m);
//		} else if (m < 1000000) {
//			mString = Long.toString(m / 1000)+"k"; //$NON-NLS-1$
//		} else if (m < 1000000000) {
//			mString = Long.toString(m / 1000000)+"m"; //$NON-NLS-1$
//		} else {
//			mString = Long.toString(m / 1000000000)+"g"; //$NON-NLS-1$
//		}
		preferences.put(TBXPreferences.MEMORY_SIZE, "2g");

		// FIXME
		preferences.putBoolean(TBXPreferences.ADD_TECH_LOGS, true);
		preferences.putBoolean(TBXPreferences.LOG_STACKTRACE, false);
		preferences.putBoolean(TBXPreferences.LOG_IN_FILE, false);
		preferences.put(TBXPreferences.LOG_LEVEL, "INFO"); //$NON-NLS-1$

		preferences.putBoolean(TBXPreferences.CLIPBOARD_IMPORT_ASK_PARAMETERS, true);
		preferences.put(TBXPreferences.CLIPBOARD_IMPORT_DEFAULT_LANG, Locale.getDefault().getLanguage().toLowerCase());
		preferences.put(TBXPreferences.UI_LOCALE, "fr"); //$NON-NLS-1$

		preferences.put(TBXPreferences.LANG, Locale.getDefault().getLanguage().toLowerCase());
		preferences.put(TBXPreferences.ENCODING, DEFAULT_ENCODING);
		preferences.put(TBXPreferences.DEFAULT_EDITION, "default"); //$NON-NLS-1$
		preferences.putBoolean(TBXPreferences.ANNOTATE, false);

		preferences.put(TBXPreferences.METADATA_ENCODING, "UTF-8"); //$NON-NLS-1$

		if (Locale.getDefault().getCountry().equals("fr")) { //$NON-NLS-1$
			preferences.put(TBXPreferences.METADATA_COLSEPARATOR, ";"); //$NON-NLS-1$
			preferences.put(TBXPreferences.METADATA_TXTSEPARATOR, "\""); //$NON-NLS-1$
		} else {
			preferences.put(TBXPreferences.METADATA_COLSEPARATOR, ","); //$NON-NLS-1$
			preferences.put(TBXPreferences.METADATA_TXTSEPARATOR, "\""); //$NON-NLS-1$
		}
		preferences.put(TBXPreferences.EMPTY_PROPERTY_VALUE_CODE, "__UNDEF__"); //$NON-NLS-1$

		preferences.put(EXPORT_COL_SEPARATOR, "\t"); //$NON-NLS-1$
		preferences.put(EXPORT_ENCODING, System.getProperty("file.encoding")); //$NON-NLS-1$
		preferences.put(EXPORT_TXT_SEPARATOR, ""); //$NON-NLS-1$
		preferences.putBoolean(EXPORT_SHOW, false);

		preferences.putBoolean(SHOW_ALL_RESULT_NODES, false);
		preferences.putBoolean(AUTO_PERSISTENCE_ENABLED, false);

		preferences.putBoolean(UPDATEEDITIONS, true);

		preferences.put(UPDATESITE, "https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/dist/"); //$NON-NLS-1$
		preferences.put(RAWFILESSITE, "https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/"); //$NON-NLS-1$
		preferences.put(FILESSITE, "https://txm.gitpages.huma-num.fr/textometrie/files/"); //$NON-NLS-1$

		preferences.putInt(TBXPreferences.QUERY_HISTORY_SIZE, 100);
	}
}
