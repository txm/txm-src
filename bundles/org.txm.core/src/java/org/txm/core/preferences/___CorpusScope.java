/**
 * 
 */
package org.txm.core.preferences;

import java.io.File;
import java.util.HashMap;

import org.eclipse.core.internal.preferences.AbstractScope;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.preferences.ConfigurationScope;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

/**
 * Tests to save the results and corpus parameters in the binary corpus path.
 * Temporary, in future we'll need to use ProjectScope + IProject + Eclipse Workspace. 
 * @author sjacquot
 *
 */
//FIXME: Temporary, in future we'll need to use ProjectScope + IProject + Eclipse Workspace.
public class ___CorpusScope extends AbstractScope {

	/**
	 * Project scopes.
	 */
	public static HashMap<String, ___CorpusScope> projectScopes = new HashMap<String, ___CorpusScope>(); 
		
	/**
	 * Scope name.
	 */
	protected String name;
	
	/**
	 * Location.
	 */
	protected IPath location;
			
	/**
	 * 
	 * @param name
	 * @param location
	 */
	public ___CorpusScope(String name, File location) {
		super();
		this.name = name;
		this.location = new Path(location.getAbsolutePath() + "/"); //$NON-NLS-1$
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.internal.preferences.AbstractScope#getName()
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.internal.preferences.AbstractScope#getLocation()
	 */
	@Override
	public IPath getLocation() {
		return this.location;
	}

	
	/**
	 * 
	 * @param scope
	 */
	@SuppressWarnings("restriction")
	public static void addScope(___CorpusScope scope)	{
		
		projectScopes.put(scope.getName(), scope);
		
		Preferences prefs = scope.getNode("import");  //$NON-NLS-1$
		prefs.put("test", "value"); //$NON-NLS-1$
		System.err.println("CorpusScope.addScope(): " + prefs.absolutePath()); //$NON-NLS-1$
		System.err.println("CorpusScope.addScope(): test = " + prefs.get("test", "default value")); //$NON-NLS-1$
		try {
			//prefs.sync();
			prefs.flush();
		}
		catch(BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.err.println("CorpusScope.addScope(): default scope location: " + ConfigurationScope.INSTANCE.getLocation()); //$NON-NLS-1$
		System.err.println("CorpusScope.addScope(): workspace location: " + ResourcesPlugin.getWorkspace().getRoot().getLocation().toString()); //$NON-NLS-1$
		System.out.println("CorpusScope.addScope(): add scope " + scope.getName() + " - " + scope.getLocation()); //$NON-NLS-1$
	}
	
	/**
	 * 
	 * @param name
	 */
	public static void removeScope(String name)	{
		projectScopes.remove(name);
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public static ___CorpusScope getScope(String name)	{
		return projectScopes.get(name);
	}
	
}
