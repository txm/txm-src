package org.txm.core.messages;

import java.text.NumberFormat;
import java.util.Locale;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Toolbox core messages.
 *
 * @author mdecorde
 * @author sjacquot
 *
 */
// FIXME: SJ: this class should be split in one containing only the core messages and another that contains the utils methods bind() and UTF-8 initializing
// The new manager class should contain other helping method eg. some based on MessageFormat to manage plurals, etc.
public class TXMCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.core.messages.messages"; //$NON-NLS-1$

	public static String binaryDirectoryP0NotFound;
	public static String CallingTheP0CommandWithTheP1Parameters;
	public static String cancel;
	public static String CannotCallATXMCommandWithoutIDParametersP0;
	public static String cantFindStructuralUnitForMetadataWithIdColon;
	public static String CantNotifyOfP0TheP1ExtensionP2P3P4;
	public static String catalogFileNotFoundP0;
	public static String checkingBinaryFormat;

	public static String common_absoluteFrequency;
	public static String common_cols;
	public static String common_defaultFont;
	public static String common_delete;
	public static String common_description;
	public static String common_done;
	public static String common_filtersColon;
	public static String common_fMax;
	public static String common_fMin;
	public static String common_fMinEqualsP0;
	public static String common_frequency;
	public static String common_metadataColon;
	public static String common_name;
	public static String common_noResults;
	public static String common_ok;
	public static String common_P0ItemsForP1Occurrences;
	public static String common_P0Occurrences;
	public static String common_P0Positions;
	public static String common_properties;
	public static String common_property;
	public static String common_queriesColonP0;
	public static String common_query;
	public static String common_reference;
	public static String common_rows;
	public static String common_size;
	public static String common_structuralUnitAndProperty;
	public static String common_structure;
	public static String common_subcorpus;
	public static String common_subcorpusName;
	public static String common_thresholds;
	public static String common_units;
	public static String common_unnamed;
	public static String common_vMax;
	public static String common_warning;
	
	public static String computingColon;
	public static String computingParentsOfP0;
	public static String connectedToCQP;
	public static String corpusColon;
	public static String corpusColongetLocaleColonCQPIsNotReadyToAnswerColon;
	public static String corpusImportDoneInP0;
	public static String corpusUpdateDoneInP0;
	public static String couldNodFindInjectionForTextP0;
	public static String couldNotCreateTXMHOMEDirectoryColonP0;
	public static String CouldNotDeleteTheP0Result;
	public static String couldntReadCorpusColon;
	public static String cQiClientNotInitialized;
	public static String creatingCorpus;
	public static String dataDirectoryIsMissingColonP0;
	public static String doesNotExists;
	public static String encodingColon;
	public static String endOfCQPWaitTest;
	
	public static String error_error;
	public static String error_errorColonCanNotWriteInFileP0;
	public static String error_errorP0;
	public static String error_errorWhileSavingPreferencesAndResults;
	public static String error_errorWhileSettingExecutionFileRightsToColonP0;
	public static String error_errorWhileSortingResult;
		
	public static String eRRORColonExists;
	public static String errorColonLinkGrplinkAttargetMalformedColonP0;
	public static String errorColonP0;
	public static String errorColonP0CorpusDirectoryIsNotConformantToTXM079CorpusBinaryFormatColonCorpusSkipped;
	public static String errorColonP0CorpusDirectoryIsNotConformantToTXMCorpusBinaryFormatColonCorpusSkipped;
	public static String eRRORColonRead;
	public static String errorColonSkippingTheLoadingOfTheP0Corpus;
	public static String errorColonTheConnexionToTheServerFailedColonWrongPortFormatP0;
	public static String errorColonThisExceptionShouldNotBeThrown;
	public static String errorColonWhileConnectionToSearchEngineWithTheFollowingParametersDblColonP0P1;
	public static String errorConfigdirDirColon;
	public static String errorDuringWorkspaceInitializationColonP0;
	public static String errorFailToLoadMetadataFromTheP0File;
	public static String errorLoadingWorkspaceFromFileColonP0;
	public static String errorNoHeaderInTheMetadataFileP0WithSeparatorsColumnP1AndSeparatorP2;
	public static String errorNoTagHasBeenFoundWithTheXpathP0;
	public static String errorRegDirColon;
	public static String errorTheFirstColumnNameIntheHeaderLineOfTheMetadataFileP0MustBeIdEtcP1P2P3;
	public static String errorTheMetadataFileP0ContainsDuplicatedColumnNamesP1;
	public static String errorTheP0CSVFileDoesNotExists;
	public static String ErrorToolboxIsNotCorrectlyInitializedAborting;
	public static String ErrorWhileComputingP0DbldotP1;
	public static String errorWhileConnectingToSearchEngine;
	public static String errorWhileGettingMetadatasFromBaseimportMetadatasColon;
	public static String errorWhilePostInstallingPluginsP0;
	public static String errorWhilePreInstallingPluginsP0;
	public static String ErrorWhileStartingEngines;
	public static String ErrorWhileStartingEnginesOfTheP0EnginesManager;
	public static String errorWhileWaitingForCQPP0;
	public static String ErrorWorkspaceNotUpdatedIsNull;
	public static String evaluatingExtensionColonP0FromP1;
	public static String exceptionColonP0;
	public static String ExceptionWhileInstallingExtensionIsP0;
	public static String failedToAccessStructuralPropertyValues;
	public static String FailedToCallTheP0HyperlinkedCommandWithP1AndTheP2Parameters;
	public static String failedToCopyTXMFilesFromTXMINSTALLDIRToTXMHOMEColonP0;
	public static String failedToCreatePartColon;
	public static String failedToExecP0P1;
	public static String failedToExecTaskkillIMCqpserverexeFColonP0;
	public static String failedToExecTskillCqpserverexeColonP0;
	public static String failedToGetLastCQPErrorColon;
	public static String failedToGetSupValuesOf;
	public static String failedToLoadBaseParametersFromTheP0File;
	public static String FailedToOpenTheP0Editor;
	public static String failedToRestoreTheP0PartitionOfP1P2;
	public static String failedToRestoreTheP0SubcorpusOfP1P2;
	public static String failedToStartMemCqiClientColon;
	public static String FailToInstallExtensionP0FilesInP1;
	public static String file;
	public static String fillImportxmlWithColon;
	public static String filterManagerErrorColonInitializationP0NotFound;
	public static String fminP0fmaxP1tP2vP3;
	public static String focusIsEmptyOrNull;
	public static String hasNoBase;
	public static String htmlDirectoryIsMissingColonP0;
	
	public static String info_cancelingComputingOfP0P1;
	public static String info_computingChildrenOfP0P1;
	public static String info_P0P1Deleted;
	public static String info_savingPreferencesAndResults;
		
	public static String initializationOfGroovyImportScriptRunner;
	public static String initializingCorporaP0P1;
	public static String InitializingToolbox;
	public static String initializingToolbox;
	public static String iOColon;
	public static String isNotAValidCQPIDForACorpusItMustBeInUppercaseCharacters;
	public static String isNotAValidCQPIdForASubcorpusItMustBeAnUppercaseCharactersFollowedByLowercaseCharacters;
	public static String languageColon;
	public static String listTruncationThresholdCommaOnlyTheMostFrequentFirstVmaxLinesAreRetained;
	public static String loadingExtensionPostInstallation;
	public static String loadingExtensionPreInstallation;
	public static String loadingExtensions;
	public static String loadingWorkspaceFromFileColonP0;
	public static String malformedURLFileP0;
	public static String managerErrorDuringInitializationColonP0NotFound;
	public static String matchesFound;
	public static String maximumFrequencyThresholdCommaAllValuesAboveFmaxAreIgnored;
	public static String maybeTheMetadataFileDoestHaveTheRightFormat;
	public static String minimumFrequencyThresholdCommaAllValuesBelowFminAreIgnored;
	public static String no;
	public static String noValuesGiven;
	public static String numberOfMatch;
	public static String numberOfSubMatch;
	public static String orTheEngineWasNotStartedWithTheCommandLineColonP0;
	public static String P0isMissing;
	public static String pARAMSColonP0;
	public static String parentDirectoryP0NotFound;
	public static String processing;
	public static String processingTextColonP0;
	public static String queryingPartitionP0;
	public static String queryOnP0ColonP1InfP2;
	public static String queryWasColon;
	public static String readCorpusInfosFromOldImportxmlFileColonP0;
	public static String readError;
	public static String registryFileIsMissingColonP0;
	public static String reorganizingFilesOfP0;
	public static String sAXColon;
	public static String selectionErrorColonTheCorpus;
	public static String supBeginningExecutionOfP0;
	public static String supEndOfExecution;
	public static String TabTryingWithSeparatorsColumnsTAndText;
	public static String textMetadataColon;
	public static String theFocusCannotBeNullOrEmpty;
	public static String theFocusIsEmptyOrNull;
	public static String theMetadataFileIsMissingAttributeId;
	public static String theP0PartFocusDoesntBelongToPartitionP1;
	public static String ToolboxStatesWorkspaceColonP0;
	public static String txmDirectoryIsMissingColonP0;
	public static String tXMNeedsFoldersColonP0P1P2AndP3;
	public static String TXMWasNotCorrectlyClosedSeeTheP0ReportFile;
	public static String unknownPartitionName;
	public static String warningCantFindMetadataForTextOfIDP0;
	public static String warningColonCantLoadCorpusP0;
	public static String warningColonDuplicateQueryEntryColonP0;
	public static String warningColonQueryFailedColonP0;
	public static String warningMalformedDataLineP0AtP1forHeaderP2;
	public static String warningNoCorpusTagFoundInTheP0FileBlehblehbleh;
	public static String warningTheP0thColumnNameIsEmpty;
	public static String warningTheP0thColumnNameIsEmptyAndWillBeIgnored;
	public static String WarningWorkspaceCannotFoundTheRedistDirectoryP0;
	public static String wrongLoginOrPasswordOrPortColonP0P1P203;
	public static String xmlValidationColonP0DoesNotExists;
	public static String xmlValidationColonP0IsADirectory;
	public static String xmlValidationColonP0IsNotReadable;
	public static String yes;

	public static String fontFamily;

	/**
	 * Binds the given message's substitution locations with the given string values.
	 *
	 * @param message
	 * @param bindings
	 * @return the given message's substitution locations with the given string values.
	 */
	public static String bind(String message, Object... bindings) {
		return NLS.bind(message, bindings);
	}


	/**
	 * Returns a string dedicated to display a Min filter string.
	 *
	 * @param value
	 * @return
	 */
	// FIXME: SJ: this method may be moved to another class
	public static String formatMinFilter(int value, int minimumValue) {
		if (value >= minimumValue) {
			return " ≥" + TXMCoreMessages.formatNumber(value); //$NON-NLS-1$
		}
		else return ""; //$NON-NLS-1$
	}

	/**
	 * Returns a string dedicated to display a Min filter string.
	 *
	 * @param value
	 * @return
	 */
	// FIXME: SJ: this method may be moved to another class
	public static String formatMinFilter(int value) {
		return formatMinFilter(value, 2);
	}



	/**
	 * Returns a string dedicated to display a Min filter string.
	 *
	 * @param value
	 * @return
	 */
	// FIXME: SJ: this method may be moved to another class
	public static String formatMinFilter(double value, double minimumValue) {
		if (value >= minimumValue) {
			return " ≥" + TXMCoreMessages.formatNumber(value); //$NON-NLS-1$
		}
		else return ""; //$NON-NLS-1$
	}

	/**
	 * Returns a string dedicated to display a Min filter string.
	 *
	 * @param value
	 * @return
	 */
	// FIXME: SJ: this method may be moved to another class
	public static String formatMinFilter(double value) {
		return formatMinFilter(value, 2);
	}


	/**
	 * Returns a string dedicated to display a Max filter string.
	 *
	 * @param value
	 * @return
	 */
	// FIXME: SJ: this method may be moved to another class
	public static String formatMaxFilter(int value) {
		if (value != Integer.MAX_VALUE) {
			return " ≤" + TXMCoreMessages.formatNumber(value); //$NON-NLS-1$
		}
		else return ""; //$NON-NLS-1$
	}

	/**
	 * Returns a string dedicated to display a VMax filter string.
	 *
	 * @param value
	 * @return
	 */
	// FIXME: SJ: this method may be moved to another class
	public static String formatVMaxFilter(int value) {
		if (value != Integer.MAX_VALUE) {
			return " /" + TXMCoreMessages.formatNumber(value); //$NON-NLS-1$
		}
		else return ""; //$NON-NLS-1$
	}

	/**
	 * Formats the specified number using default locale syntax (e.g. 2 150 or 2,150 ).
	 *
	 * @param value
	 * @return
	 */
	// FIXME: SJ: this method may be moved to another class
	public static String formatNumber(int value) {
		return NumberFormat.getInstance(Locale.getDefault()).format(value);
	}

	/**
	 * Formats the specified number using default locale syntax (e.g. 2 150,35 or 2,150.35 ).
	 *
	 * @param value
	 * @return
	 */
	// FIXME: SJ: this method may be moved to another class
	public static String formatNumber(double value) {
		return NumberFormat.getInstance(Locale.getDefault()).format(value);
	}

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, TXMCoreMessages.class);
	}
}
