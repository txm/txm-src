// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.metadatas;

import java.util.HashMap;

import org.w3c.dom.Element;

/**
 * The Class Metadata: contains a metadata declaration.
 */
public class Metadata
{
	
	/** The id. */
	public String id;
	public String shortname;
	public String longname;
	
	/** The type. */
	public String type = "String"; //$NON-NLS-1$
	public String sortBy = null;
	
	/** The selection. */
	public boolean selection = true;
	
	/** The partition. */
	public boolean partition = false;
	
	/** The display. */
	public boolean display = false;
	public boolean open = false;
	public int colwidth = 100;

	/**
	 * Instantiates a new metadata.
	 *
	 * @param n the n
	 */
	public Metadata(groovy.util.Node n)
	{
		id = (String) n.attribute("id"); //$NON-NLS-1$
		shortname =(String) n.attribute("shortname"); //$NON-NLS-1$
		longname = (String) n.attribute("longname"); //$NON-NLS-1$
		type = (String) n.attribute("type"); //$NON-NLS-1$
		sortBy = (String) n.attribute("sortBy"); //$NON-NLS-1$
		if (n.attribute("selection") != null) //$NON-NLS-1$
			selection = Boolean.parseBoolean((String) n.attribute("selection")); //$NON-NLS-1$
		if (n.attribute("partition") != null) //$NON-NLS-1$
			partition = Boolean.parseBoolean((String) n.attribute("partition")); //$NON-NLS-1$
		if (n.attribute("display") != null) //$NON-NLS-1$
			display = Boolean.parseBoolean((String) n.attribute("display")); //$NON-NLS-1$
		if (n.attribute("open") != null) //$NON-NLS-1$
			open = Boolean.parseBoolean((String) n.attribute("open")); //$NON-NLS-1$
		if (n.attribute("colwidth") != null) //$NON-NLS-1$
			colwidth = Integer.parseInt((String) n.attribute("colwidth")); //$NON-NLS-1$
	}
	
	/**
	 * Instantiates a new metadata.
	 *
	 * @param id the id
	 * @param longname the longname
	 * @param type the type
	 * @param selection the selection
	 * @param partition the partition
	 * @param display the display
	 */
	public Metadata(String id, String shortname, String longname, String type, String sortBy, 
			boolean selection, boolean partition, boolean display, boolean open, int colwidth) {
		
		this.id = id;
		this.shortname = shortname;
		this.longname = longname;
		this.type = type;
		this.selection = selection;
		this.partition = partition;
		this.display = display;
		this.open = open;
		this.sortBy = sortBy;
	}
	
	//TODO: ambiguity with the Metadata defined in import.xml 
	public Metadata(Element n) {
		
		shortname = n.getAttribute("shortname"); //$NON-NLS-1$
		longname = n.getAttribute("longname"); //$NON-NLS-1$
		type = n.getAttribute("type"); //$NON-NLS-1$
		id = n.getAttribute("id"); //$NON-NLS-1$
		sortBy = n.getAttribute("sortBy"); //$NON-NLS-1$
		
		if (n.getAttribute("selection") != null) //$NON-NLS-1$
			try {
				selection = Boolean.valueOf(n.getAttribute("selection")); //$NON-NLS-1$
			} catch (Exception e) { System.out.println("Metadata: "+id+" error @selection "+e); } //$NON-NLS-1$ //$NON-NLS-2$
		if (n.getAttribute("partition") != null) //$NON-NLS-1$
			try {
				partition = Boolean.valueOf(n.getAttribute("partition")); //$NON-NLS-1$
			} catch (Exception e) { System.out.println("Metadata: "+id+" error @partition "+e); } //$NON-NLS-1$ //$NON-NLS-2$
		if (n.getAttribute("display") != null) //$NON-NLS-1$
			try {
				display = Boolean.valueOf(n.getAttribute("display")); //$NON-NLS-1$
			} catch (Exception e) { System.out.println("Metadata: "+id+" error @display "+e); } //$NON-NLS-1$ //$NON-NLS-2$
		if (n.getAttribute("open") != null) //$NON-NLS-1$
			try {
				open = Boolean.valueOf(n.getAttribute("open")); //$NON-NLS-1$
			} catch (Exception e) { System.out.println("Metadata: "+id+" error @open "+e); } //$NON-NLS-1$ //$NON-NLS-2$
		if (n.getAttribute("colwidth") != null) //$NON-NLS-1$
			try {
				colwidth = Integer.parseInt(n.getAttribute("colwidth")); //$NON-NLS-1$
			} catch (Exception e) { System.out.println("Metadata: "+id+" error @colwidth: "+e); } //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	/**
	 * Instantiates a new metadata.
	 * @param sattributeInfos 
	 * @param n the n
	 */
	public Metadata(HashMap<String, String> sattributeInfos, Element n) {
		
		shortname = sattributeInfos.get("shortname"); //$NON-NLS-1$
		longname = sattributeInfos.get("longname"); //$NON-NLS-1$
		type = sattributeInfos.get("type"); //$NON-NLS-1$
		
		id = n.getAttribute("ref"); //$NON-NLS-1$
		id = id.substring(5); // remove 'text_'
		sortBy = n.getAttribute("sortBy"); //$NON-NLS-1$
		if (n.getAttribute("selection") != null) //$NON-NLS-1$
			try {
				selection = Boolean.valueOf(n.getAttribute("selection")); //$NON-NLS-1$
			} catch (Exception e) { System.out.println("Metadata: "+id+" error @selection"); } //$NON-NLS-1$ //$NON-NLS-2$
		if (n.getAttribute("partition") != null) //$NON-NLS-1$
			try {
				partition = Boolean.valueOf(n.getAttribute("partition")); //$NON-NLS-1$
			} catch (Exception e) { System.out.println("Metadata: "+id+" error @partition"); } //$NON-NLS-1$ //$NON-NLS-2$
		if (n.getAttribute("display") != null) //$NON-NLS-1$
			try {
				display = Boolean.valueOf(n.getAttribute("display")); //$NON-NLS-1$
			} catch (Exception e) { System.out.println("Metadata: "+id+" error @display"); } //$NON-NLS-1$ //$NON-NLS-2$
		if (n.getAttribute("open") != null) //$NON-NLS-1$
			try {
				open = Boolean.valueOf(n.getAttribute("open")); //$NON-NLS-1$
			} catch (Exception e) { System.out.println("Metadata: "+id+" error @open"); } //$NON-NLS-1$ //$NON-NLS-2$
		if (n.getAttribute("colwidth") != null) //$NON-NLS-1$
			try {
				colwidth = Integer.parseInt(n.getAttribute("colwidth")); //$NON-NLS-1$
			} catch (Exception e) { System.out.println("Metadata: "+id+" error @colwidth"); } //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return id+"/"+shortname+"/"+longname+", "+type+", sortBy="+sortBy+", sel="+selection+ //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
				", part="+partition+", display="+display+", open="+open+", colwidth="+colwidth; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	}
}
