// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.metadatas;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

// TODO: Auto-generated Javadoc
/**
 * The Class TextInjection.
 */
public class TextInjection extends ArrayList<Entry>
{
	
	/** The id. */
	public String id;
	
	/** The xpath. */
	public String xpath;

	public TextInjection(Element elem) {
		
		this.id = elem.getAttribute("id"); //$NON-NLS-1$
		this.xpath = elem.getAttribute("xpath"); //$NON-NLS-1$
		NodeList entries = elem.getElementsByTagName("entry"); //$NON-NLS-1$
		for(int i = 0 ; i < entries.getLength() ; i++)
		{
			Element e = (Element)entries.item(i);
			this.add(new Entry(e.getAttribute("id"), e.getAttribute("value"))); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}
	
	/**
	 * create empty TextInjection.
	 *
	 * @param id the id
	 * @param xpath the xpath
	 */
	public TextInjection(String id, String xpath)
	{
		this.id = id;
		this.xpath = xpath;
	}


	/* (non-Javadoc)
	 * @see java.util.AbstractCollection#toString()
	 */
	@Override
	public String toString()
	{
		return "<"+id+", "+xpath+"> : "+super.toString(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}
}
