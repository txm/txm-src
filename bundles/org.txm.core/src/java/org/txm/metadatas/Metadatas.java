// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.metadatas;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.importer.AddAttributeInXml;
import org.txm.importer.PersonalNamespaceContext;
import org.txm.libs.msoffice.ReadExcel;
import org.txm.libs.office.ReadODS;
import org.txm.utils.AsciiUtils;
import org.txm.utils.CsvReader;
import org.txm.utils.Pair;
import org.txm.utils.logger.Log;
import org.txm.utils.xml.DomUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import groovy.util.Node;

/**
 * The Class Metadatas.
 */
public class Metadatas extends LinkedHashMap<String, TextInjection> {
	
	private static final long serialVersionUID = -179183433465353130L;

	/**
	 * Load metadatas from a xml file $lt;metadatas> $lt;meta key="key1"> $lt;entry
	 * name="attr1" value="value1"/> $lt;entry name="attr1" value="value1"/>
	 * $lt;meta/> $lt;/metadatas>.
	 */
	File xmlfile;
	
	/** The metadatas. */
	ArrayList<Metadata> metadatas = new ArrayList<>();
	
	/** The headers list. */
	ArrayList<String> headersList = new ArrayList<>();
	
	/** The isinialize. */
	boolean isInitialize = false;
	
	/**
	 * Checks if is initialized.
	 *
	 * @return true, if is initialized
	 */
	public boolean isInitialized() {
		
		return isInitialize;
	}
	
	/** The ns context. */
	NamespaceContext nsContext = new PersonalNamespaceContext();
	
	private File inputMetadataFile;
	
	/**
	 * Instantiates a new metadatas.
	 *
	 * @param xmlfile the xmlfile
	 */
	public Metadatas(File xmlfile) {
		
		this.xmlfile = xmlfile;
		if (!initialize()) {
			Log.warning(NLS.bind(TXMCoreMessages.errorFailToLoadMetadataFromTheP0File, xmlfile));
		}
	}
	
	/**
	 * Find a metadata file in a directory
	 * 
	 * @param directory
	 * @return an input file, try in order : ods, xlsx, tsv and finally csv
	 *         extension. If no file is found returns NULL
	 */
	public static File findMetadataFile(File directory) {
		
		File f = new File(directory, "metadata.ods"); //$NON-NLS-1$
		if (f.exists()) return f;
		
		f = new File(directory, "metadata.xlsx"); //$NON-NLS-1$
		if (f.exists()) return f;
		
		f = new File(directory, "metadata.xls"); //$NON-NLS-1$
		if (f.exists()) return f;
		
		f = new File(directory, "metadata.tsv"); //$NON-NLS-1$
		if (f.exists()) return f;
		
		f = new File(directory, "metadata.csv"); //$NON-NLS-1$
		if (f.exists()) return f;
		
		return null;
	}
	
	/**
	 * Instantiates a new metadatas using an input file : ods, xlsx, tsv or csv.
	 *
	 * @param inputFile the CSV, XSLS or ODS
	 * @param encoding the encoding
	 * @param separator the separator
	 * @param nbheaderline the nbheaderline
	 */
	public Metadatas(File inputFile, String encoding, String separator, String txtseparator, int nbheaderline) {
		
		this.inputMetadataFile = inputFile;
		File xmlfile = new File(inputFile.getParent(), "metadata.xml"); //$NON-NLS-1$
		// println "create xml file version of "+csvfile+" : "+xmlfile
		try {
			if (inputFile.getName().endsWith(".ods")) { //$NON-NLS-1$
				if (convertODSToXml(inputFile, xmlfile)) {
					this.xmlfile = xmlfile;
					// println "xml file : "+xmlfile
					
				}
			}
			else if (inputFile.getName().endsWith(".xlsx") || inputFile.getName().endsWith(".xls")) { //$NON-NLS-1$ //$NON-NLS-2$
				if (convertXLSXToXml(inputFile, xmlfile)) {
					this.xmlfile = xmlfile;
					// println "xml file : "+xmlfile
					
				}
			}
			else if (inputFile.getName().endsWith(".tsv")) { //$NON-NLS-1$
				if (convertCsvToXml(inputFile, xmlfile, encoding, "\t", "", 0)) { //$NON-NLS-1$ //$NON-NLS-2$
					this.xmlfile = xmlfile;
					// println "xml file : "+xmlfile
					
				}
			}
			else {
				if (convertCsvToXml(inputFile, xmlfile, encoding, separator, txtseparator, nbheaderline)) {
					this.xmlfile = xmlfile;
					// println "xml file : "+xmlfile
					
				}
			}
			
			if (!initialize()) {
				Log.warning(NLS.bind(TXMCoreMessages.errorFailToLoadMetadataFromTheP0File, xmlfile));
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
			Log.severe(e.toString());
		}
	}
	
	private boolean convertXLSXToXml(File inputFile, File xmlFile) throws Exception {
		
		ArrayList<ArrayList<String>> data = ReadExcel.toTable(inputFile, "metadata"); //$NON-NLS-1$
		return data.size() > 0 && convertTableToXml(data, xmlFile);
	}
	
	private boolean convertTableToXml(ArrayList<ArrayList<String>> data, File xmlFile) throws Exception {
		
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		FileOutputStream output = new FileOutputStream(xmlFile);
		XMLStreamWriter writer = factory.createXMLStreamWriter(output, "UTF-8");// create a new file //$NON-NLS-1$
		
		writer.writeStartDocument("UTF-8", "1.0"); //$NON-NLS-1$ //$NON-NLS-2$
		writer.writeCharacters("\n"); //$NON-NLS-1$
		writer.writeStartElement("enrichissement"); //$NON-NLS-1$
		writer.writeCharacters("\n"); //$NON-NLS-1$
		writer.writeStartElement("metadatas"); //$NON-NLS-1$
		writer.writeCharacters("\n"); //$NON-NLS-1$
		
		ArrayList<String> headers = data.get(0); // first line
		ArrayList<Integer> indexes = new ArrayList<>();
		for (int i = 1; i < headers.size(); i++) {
			if (headers.get(i).length() == 0) {
				//headers.set(i, "noname");
				Log.finer(NLS.bind(TXMCoreMessages.warningTheP0thColumnNameIsEmptyAndWillBeIgnored, i + 1));
				continue;
			}
			// if(!headers[i].equals("id"))// the first
			// {
			writer.writeStartElement("metadata"); //$NON-NLS-1$
			writer.writeAttribute("id", AsciiUtils.buildAttributeId(headers.get(i))); //$NON-NLS-1$
			writer.writeAttribute("shortname", headers.get(i)); //$NON-NLS-1$
			writer.writeAttribute("longname", headers.get(i)); //$NON-NLS-1$
			writer.writeAttribute("type", "String"); //$NON-NLS-1$ //$NON-NLS-2$
			writer.writeAttribute("colwidth", "100"); //$NON-NLS-1$ //$NON-NLS-2$
			writer.writeAttribute("selection", "true"); //$NON-NLS-1$ //$NON-NLS-2$
			writer.writeAttribute("partition", "true"); //$NON-NLS-1$ //$NON-NLS-2$
			writer.writeAttribute("display", "true"); //$NON-NLS-1$ //$NON-NLS-2$
			
			writer.writeEndElement();
			writer.writeCharacters("\n"); //$NON-NLS-1$
			indexes.add(i);
		}
		writer.writeEndElement(); // close metadatas
		writer.writeCharacters("\n"); //$NON-NLS-1$
		
		writer.writeStartElement("texts"); //$NON-NLS-1$
		writer.writeCharacters("\n"); //$NON-NLS-1$
		for (int i = 1; i < data.size(); i++) { // the next lines
			ArrayList<String> dataline = data.get(i);
			
			writer.writeStartElement("text"); //$NON-NLS-1$
			
			// write the id attribute
			for (int j = 0; j < headers.size(); j++) {
				if (headers.get(j).equals("id")) { //$NON-NLS-1$
					writer.writeAttribute("id", dataline.get(j)); //$NON-NLS-1$
				}
			}
			// write the other attributes
			for (int j : indexes) {
				if (headers.get(j).length() == 0 || headers.get(j).equals("id") || headers.get(j).length() == 0) { //$NON-NLS-1$
					continue;
				}
				writer.writeStartElement("entry"); //$NON-NLS-1$
				writer.writeAttribute("id", headers.get(j)); //$NON-NLS-1$
				if (dataline.size() <= j) {
					Log.warning(TXMCoreMessages.bind(TXMCoreMessages.warningMalformedDataLineP0AtP1forHeaderP2, dataline, j, headers));
					writer.writeAttribute("value", ""); //$NON-NLS-1$ //$NON-NLS-2$
				}
				else {
					writer.writeAttribute("value", dataline.get(j)); //$NON-NLS-1$
				}
				writer.writeEndElement();
			}
			writer.writeEndElement();
			writer.writeCharacters("\n"); //$NON-NLS-1$
		}
		writer.writeEndElement();// close texts
		writer.writeCharacters("\n"); //$NON-NLS-1$
		writer.writeEndElement();// close metadatas
		
		writer.close();
		output.close();
		writer.close();
		return true;
	}
	
	private boolean convertODSToXml(File inputFile, File xmlFile) throws Exception {
		
		ArrayList<ArrayList<String>> data = ReadODS.toTable(inputFile, "metadata"); //$NON-NLS-1$
		
		return data.size() > 0 && convertTableToXml(data, xmlFile);
	}
	
	public File getXMLFile() {
		
		return xmlfile;
	}
	
	/** The sattr. */
	String sattr = ""; //$NON-NLS-1$
	
	/**
	 * Initialize.
	 *
	 * @return true, if successful
	 */
	private boolean initialize() {
		
		Document doc;
		try {
			doc = DomUtils.load(xmlfile);
		}
		catch (Exception e1) {
			org.txm.utils.logger.Log.printStackTrace(e1);
			Log.severe(e1.toString());
			return false;
		}
		NodeList metadatasNodes = doc.getElementsByTagName("metadatas"); //$NON-NLS-1$
		NodeList metadataNodes = null;
		// get metadata nodes
		for (int i = 0; i < metadatasNodes.getLength();) {
			Element metadatasNode = (Element) metadatasNodes.item(0);
			metadataNodes = metadatasNode.getElementsByTagName("metadata"); //$NON-NLS-1$
			break;
		}
		for (int i = 0; i < metadataNodes.getLength(); i++) {
			Element metadataNode = (Element) metadataNodes.item(i);
			Metadata m = new Metadata(metadataNode);
			if (m.id == null) {
				Log.severe(TXMCoreMessages.theMetadataFileIsMissingAttributeId);
				return false;
			}
			this.metadatas.add(m);
			sattr += "+" + m.id; //$NON-NLS-1$
			headersList.add(m.id);
		}
		
		// List<Node> Lmetadatas = doc.texts.text;
		// get metadata nodes
		NodeList textsNodes = doc.getElementsByTagName("texts"); //$NON-NLS-1$
		NodeList textNodes = null;
		for (int i = 0; i < textsNodes.getLength();) { // get text nodes
			Element textsNode = (Element) textsNodes.item(0);
			textNodes = textsNode.getElementsByTagName("text"); //$NON-NLS-1$
			break;
		}
		for (int i = 0; i < textNodes.getLength(); i++) {
			Element e = (Element) textNodes.item(i);
			TextInjection inj = new TextInjection(e);
			this.put(inj.id, inj);
		}
		
		isInitialize = true;
		return true;
	}
	
	public HashMap<String, String> getTextMetadata(File f) {
		
		HashMap<String, String> data = new HashMap<>();
		String txtname = f.getName();
		int idx = txtname.lastIndexOf("."); //$NON-NLS-1$
		if (idx > 0) txtname = txtname.substring(0, idx);
		
		TextInjection injection = this.get(txtname);
		if (injection == null) {
			Log.warning(NLS.bind(TXMCoreMessages.couldNodFindInjectionForTextP0, txtname));
			return data;
		}
		for (org.txm.metadatas.Entry e : injection) {
			data.put(e.getId(), e.getValue());
		}
		
		return data;
	}
	
	/**
	 * Convert csv to xml.
	 *
	 * @param csvfile the csvfile
	 * @param xmlFile the xml file
	 * @param encoding the encoding
	 * @param separator the separator
	 * @param nbheaderline the nbheaderline
	 * @return true, if successful
	 * @throws Exception
	 */
	public static boolean convertCsvToXml(File csvfile, File xmlFile, String encoding, String separator, String txtseparator, int nbheaderline) throws Exception {
		
		try {
			if (separator == null || separator.length() == 0) {
				separator = "\t"; //$NON-NLS-1$
			}
			if (encoding == null || encoding.length() == 0) {
				encoding = "UTF-8"; //$NON-NLS-1$
			}
			xmlFile.createNewFile();
			
			if (!csvfile.exists()) {
				Log.warning(NLS.bind(TXMCoreMessages.errorTheP0CSVFileDoesNotExists,csvfile));
				return false;
			}
			
			XMLOutputFactory factory = XMLOutputFactory.newInstance();
			FileOutputStream output = new FileOutputStream(xmlFile);
			XMLStreamWriter writer = factory.createXMLStreamWriter(output, "UTF-8");// create a new file //$NON-NLS-1$
			
			CsvReader reader = new CsvReader(csvfile.getAbsolutePath(), separator.charAt(0), Charset.forName(encoding));
			if (txtseparator != null && txtseparator.length() > 0)
				reader.setTextQualifier(txtseparator.charAt(0));
			
			reader.readHeaders();
			
			String[] headers = reader.getHeaders();
			
			if (headers.length == 0) {
				Log.warning(TXMCoreMessages.bind(TXMCoreMessages.errorNoHeaderInTheMetadataFileP0WithSeparatorsColumnP1AndSeparatorP2, csvfile, separator, txtseparator));
				writer.close();
				output.close();
				return false;
			}
			
			if (!headers[0].equals("id")) { //$NON-NLS-1$
				Log.info(TXMCoreMessages.bind(TXMCoreMessages.errorTheFirstColumnNameIntheHeaderLineOfTheMetadataFileP0MustBeIdEtcP1P2P3, csvfile, headers[0], separator, txtseparator));
				writer.close();
				output.close();
				if (!separator.equals("\t")) { //$NON-NLS-1$
					Log.info(TXMCoreMessages.TabTryingWithSeparatorsColumnsTAndText);
					return convertCsvToXml(csvfile, xmlFile, encoding, "\t", "", nbheaderline); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}
			
			// check for double columns
			HashSet<String> testhash = new HashSet<>();
			HashSet<String> doubles = new HashSet<>();
			for (String str : headers) {
				if (testhash.contains(str))
					doubles.add(str);
				testhash.add(str);
			}
			if (doubles.size() > 0) {
				Log.info(TXMCoreMessages.bind(TXMCoreMessages.errorTheMetadataFileP0ContainsDuplicatedColumnNamesP1, csvfile, doubles));
				return false;
			}
			
			String[] longnames = new String[headers.length];
			String[] types = new String[headers.length];
			if (nbheaderline > 1) {// get longnames
				reader.readRecord();
				for (int i = 0; i < headers.length; i++) {
					longnames[i] = reader.get(headers[i]);
				}
			}
			else {
				for (int i = 0; i < headers.length; i++) {
					longnames[i] = headers[i];
				}
			}
			
			if (nbheaderline > 2) {// got types
				reader.readRecord();
				for (int i = 0; i < headers.length; i++) {
					types[i] = reader.get(headers[i]);
				}
			}
			else {
				for (int i = 0; i < headers.length; i++) {
					types[i] = "String"; //$NON-NLS-1$
				}
			}
			
			writer.writeStartDocument("UTF-8", "1.0"); //$NON-NLS-1$ //$NON-NLS-2$
			writer.writeStartElement("enrichissement"); //$NON-NLS-1$
			writer.writeStartElement("metadatas"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			// println "headers : "+Arrays.toString(headers)
			for (int i = 1; i < headers.length; i++) {
				if (headers[i].length() == 0) {
					//headers[i] = "noname";
					Log.finer(NLS.bind(TXMCoreMessages.warningTheP0thColumnNameIsEmpty, i + 1));
					continue;
				}
				// if(!headers[i].equals("id"))// the first
				// {
				writer.writeStartElement("metadata"); //$NON-NLS-1$
				writer.writeAttribute("id", AsciiUtils.buildAttributeId(headers[i])); //$NON-NLS-1$
				writer.writeAttribute("shortname", headers[i]); //$NON-NLS-1$
				writer.writeAttribute("longname", longnames[i]); //$NON-NLS-1$
				writer.writeAttribute("type", types[i]); //$NON-NLS-1$
				writer.writeAttribute("colwidth", "100"); //$NON-NLS-1$ //$NON-NLS-2$
				writer.writeAttribute("selection", "true"); //$NON-NLS-1$ //$NON-NLS-2$
				writer.writeAttribute("partition", "true"); //$NON-NLS-1$ //$NON-NLS-2$
				writer.writeAttribute("display", "true"); //$NON-NLS-1$ //$NON-NLS-2$
				
				writer.writeEndElement();
				writer.writeCharacters("\n"); //$NON-NLS-1$
				// }
			}
			writer.writeEndElement();// close metadatas
			writer.writeCharacters("\n"); //$NON-NLS-1$
			
			writer.writeStartElement("texts"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			while (reader.readRecord()) {
				writer.writeStartElement("text"); //$NON-NLS-1$
				for (int i = 0; i < headers.length; i++)
					if (headers[i].equals("id")) { //$NON-NLS-1$
						writer.writeAttribute("id", reader.get(headers[i])); //$NON-NLS-1$
					}
					else if (headers[i].equals("xpath")) { //$NON-NLS-1$
						writer.writeAttribute("xpath", reader.get(headers[i])); //$NON-NLS-1$
					}
				
				for (int i = 0; i < headers.length; i++)
					if (headers[i].length() > 0 && !headers[i].equals("id") && !headers[i].equals("xpath")) { //$NON-NLS-1$ //$NON-NLS-2$
						writer.writeEmptyElement("entry"); //$NON-NLS-1$
						writer.writeAttribute("id", AsciiUtils.buildAttributeId(headers[i])); //$NON-NLS-1$
						String value = reader.get(headers[i]);
						if (value.length() == 0)
							writer.writeAttribute("value", "N/A"); //$NON-NLS-1$ //$NON-NLS-2$
						else
							writer.writeAttribute("value", value); //$NON-NLS-1$
						
					}
				writer.writeEndElement();
				writer.writeCharacters("\n"); //$NON-NLS-1$
			}
			writer.writeEndElement();// close texts
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement();// close metadatas
			
			reader.close();
			writer.close();
			output.close();
			
			output = null;
			writer = null;
		}
		catch (Exception e) {
			Log.warning(TXMCoreMessages.bind(TXMCoreMessages.error_errorP0, e));
			Log.printStackTrace(e);
			return false;
		}
		return true;
	}
	
	/**
	 * Keep only metadatas.
	 *
	 * @param ids the ids
	 */
	public void keepOnlyMetadatas(String[] ids) {
		
		// println metadatas
		List<String> Lids = Arrays.asList(ids);
		headersList.removeAll(Lids);
		for (int i = 0; i < metadatas.size(); i++) {
			Metadata m = metadatas.get(i);
			if (!Lids.contains(m.id)) {
				metadatas.remove(i);
				i--;
			}
		}
		
		for (TextInjection inj : this.values()) {
			for (int i = 0; i < inj.size(); i++) {
				org.txm.metadatas.Entry e = inj.get(i);
				if (!Lids.contains(e.getId())) {
					inj.remove(i);
					i--;
				}
			}
		}
	}
	
	/**
	 * Inject metadatas in xml txm.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 * @return true, if successful
	 * @throws XMLStreamException
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public boolean injectMetadatasInXmlTXM(File infile, File outfile) throws MalformedURLException, IOException, XMLStreamException {
		
		return injectMetadatasInXml(infile, outfile, "text", "tei"); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	/**
	 * Inject metadatas in xml.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 * @param tag the tag
	 * @return true, if successful
	 * @throws XMLStreamException
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public boolean injectMetadatasInXml(File infile, File outfile, String tag) throws MalformedURLException, IOException, XMLStreamException {
		
		return injectMetadatasInXml(infile, outfile, tag, null);
	}
	
	/**
	 * Inject metadatas in xml.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 * @param tag the tag
	 * @param namespace the namespace
	 * @return true, if successful
	 * @throws XMLStreamException
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public boolean injectMetadatasInXml(File infile, File outfile, String tag, String namespace) throws MalformedURLException, IOException, XMLStreamException {
		
		String key = infile.getName();
		if (key.lastIndexOf(".") > 0) { //$NON-NLS-1$
			key = key.substring(0, key.lastIndexOf(".")); //$NON-NLS-1$
		}
		ArrayList<org.txm.metadatas.Entry> metas = get(key);
		
		if (metas == null) {
			Log.info(NLS.bind(TXMCoreMessages.warningCantFindMetadataForTextOfIDP0, key));
			if (this.inputMetadataFile != null && inputMetadataFile.getName().toLowerCase().endsWith(".csv")) { //$NON-NLS-1$
				Log.info(TXMCoreMessages.maybeTheMetadataFileDoestHaveTheRightFormat);
			}
			
			// Build an empty metadata injection -> all texts must have the same properties
			metas = new TextInjection(key, ""); //$NON-NLS-1$
			for (String k : this.headersList) {
				metas.add(new Entry(k, "")); //$NON-NLS-1$
			}
		}
		
		// ensure attribute names format
		for (org.txm.metadatas.Entry e : metas) {
			e.o1 = AsciiUtils.buildAttributeId(e.o1);
		}
		
		AddAttributeInXml builder = new AddAttributeInXml(infile, tag, metas);
		builder.onlyOneElement();
		return builder.process(outfile);
	}
	
	/**
	 * Save.
	 *
	 * @param doc the doc
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	public boolean save(org.w3c.dom.Node doc, File outfile) {
		
		try {
			// Création de la source DOM
			Source source = new DOMSource(doc);
			
			// Création du fichier de sortie
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outfile), "UTF-8")); //$NON-NLS-1$
			Result resultat = new StreamResult(writer);
			
			// Configuration du transformer
			TransformerFactory fabrique = new net.sf.saxon.TransformerFactoryImpl();
			Transformer transformer = fabrique.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes"); //$NON-NLS-1$
			transformer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8"); //$NON-NLS-1$
			
			// Transformation
			transformer.transform(source, resultat);
			writer.close();
			doc = null;
			return true;
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
	}
	
	/**
	 * Insert.
	 *
	 * @param doc the doc
	 * @param xpath the xpath
	 * @param pairs the pairs
	 * @return true, if successful
	 * @throws XPathExpressionException
	 */
	public boolean insert(Node doc, String xpath, List<Pair<String, String>> pairs) throws XPathExpressionException {
		
		// println ("insert $pairs into $xpath")
		XPathFactory factory = XPathFactory.newInstance();
		XPath XpathObj = factory.newXPath();
		
		XpathObj.setNamespaceContext(nsContext);
		XPathExpression expr = XpathObj.compile(xpath);
		
		NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		int count = 0;
		for (int i = 0; i < nodes.getLength(); i++) {
			org.w3c.dom.Node node = nodes.item(i);
			Element elem = (Element) node;
			for (Pair<String, String> p : pairs) {
				// println "add attr "+p.getFirst()+"="+ p.getSecond()
				elem.setAttribute(p.getFirst(), p.getSecond());
			}
			count++;
		}
		
		factory = null;
		XpathObj = null;
		expr = null;
		if (count > 0)
			return true;
		else {
			Log.warning(TXMCoreMessages.bind(TXMCoreMessages.errorNoTagHasBeenFoundWithTheXpathP0, xpath));
			return false;
		}
	}
	
	/**
	 * Gets the property names.
	 *
	 * @return the property names
	 */
	public List<String> getPropertyNames() {
		
		return headersList;
	}
	
	/**
	 * Gets the sattributes.
	 *
	 * @return the sattributes
	 */
	public String getSattributes() {
		
		String sattr = ""; //$NON-NLS-1$
		for (String attr : headersList)
			sattr += "+" + attr; //$NON-NLS-1$
		return sattr;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.util.AbstractMap#toString()
	 */
	@Override
	public String toString() {
		
		StringBuffer str = new StringBuffer();
		str.append("Metadata: \n"); //$NON-NLS-1$
		for (Metadata data : metadatas) {
			str.append(" " + data.toString() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		
		str.append("Injections: \n"); //$NON-NLS-1$
		for (TextInjection injection : this.values()) {
			str.append(" " + injection.toString() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return str.toString();
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		
		String userhome = System.getProperty("user.home"); //$NON-NLS-1$
//		File xmlfile = new File(userhome, "/xml/metadata.xml"); //$NON-NLS-1$
//		File csvfile = new File(userhome, "/xml/metadata.csv"); //$NON-NLS-1$
		File odsfile = new File(userhome, "/xml/metadata.ods"); //$NON-NLS-1$
//		File xlsxfile = new File(userhome, "/xml/metadata.xlsx"); //$NON-NLS-1$
		
		Metadatas m = new Metadatas(odsfile, "UTF-8", ",", "\"", 1); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		if (m.isInitialized()) {
			System.out.println(m.toString());
		}
	}
	
	public ArrayList<Metadata> getMetadatas() {
		
		return metadatas;
	}
	
	public ArrayList<String> getHeadersList() {
		
		return this.headersList;
	}
}
