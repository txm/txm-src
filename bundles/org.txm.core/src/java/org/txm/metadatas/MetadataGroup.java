package org.txm.metadatas;
import java.util.ArrayList;

import org.w3c.dom.Element;

public class MetadataGroup extends Metadata{

	public ArrayList<Metadata> metadatas;
	public int position;
	public MetadataGroup(String id, String shortname, String longname, String type, String sortBy, 
			boolean selection, boolean partition, boolean display, int colwidth)
	{
		super(id, shortname, longname, type, sortBy, selection, partition, display, true, colwidth);
		this.metadatas = new ArrayList<Metadata>();
		this.position = 0;
	}
	
	public MetadataGroup(Element node) {
		super(node);
		this.metadatas = new ArrayList<Metadata>();
		this.position = Integer.parseInt(node.getAttribute("position")); //$NON-NLS-1$
		this.id = node.getAttribute("ref"); //$NON-NLS-1$
	}
	
	public MetadataGroup(String id, String shortname, String longname, String type, boolean selection, 
			boolean partition, boolean display, ArrayList<Metadata> metadatas, int position, int colwidth)
	{
		super(id, shortname, longname, type, null, selection, partition, display, true, colwidth);
		this.metadatas = metadatas;
		this.position = position;
	}
	
	public ArrayList<Metadata> getMetadatas() {
		return metadatas;
	}
	
	@Override
	public String toString() {
		return super.toString() + ", metadatas: "+metadatas+", position: "+position; //$NON-NLS-1$ //$NON-NLS-2$
	}
}
