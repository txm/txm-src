package org.txm.metadatas;

import org.txm.objects.CorpusBuild;
import org.w3c.dom.Element;

public class MinMaxDateGroup extends MetadataGroup
{
	String inputFormat = "yyyy-MM-dd"; //$NON-NLS-1$
	String outputFormat = "yyyy-MM-dd"; //$NON-NLS-1$
	public static String TYPE = "MinMaxDateGroup"; //$NON-NLS-1$
	
	public MinMaxDateGroup(String id, String shortname, String longname, boolean selection, boolean partition, boolean display, Metadata min, Metadata max, String inputFormat, String outputFormat, int position, int colwidth)
	{
		super(id, shortname, longname, TYPE, null, selection, partition, display, colwidth);
		
		this.position = position;
		metadatas.add(min);
		metadatas.add(max);
		this.inputFormat = inputFormat;
		this.outputFormat = outputFormat;
	}

	public MinMaxDateGroup(CorpusBuild corpus, Element group) {
		super(group);
		id = group.getAttribute("ref"); //$NON-NLS-1$
		//System.out.println("create1 MinMaxDateGroup: "+group);
		String minname = group.getAttribute("min"); //$NON-NLS-1$
		String maxname = group.getAttribute("max"); //$NON-NLS-1$
		this.outputFormat = group.getAttribute("outputFormat"); //$NON-NLS-1$
		this.inputFormat = group.getAttribute("inputFormat"); //$NON-NLS-1$
		//System.out.println("create minmaxdategroup with: "+corpus.importmetadatas);
		//get the Metadata objects
		Metadata min=null, max=null;
		for (Metadata meta : corpus.importmetadatas) {
			//System.out.println("declared meta: "+meta);
			if (min != null && max != null) // stop when all metadatas are found
				break;
			if (meta.id.equals(minname))
				min = meta;
			else if (meta.id.equals(maxname))
				max = meta;
		}
		metadatas.add(min);
		metadatas.add(max);
		//System.out.println(this);
	}
	
	@Override
	public String toString()
	{
		return super.toString()+", output: "+outputFormat+", intput: "+inputFormat; //$NON-NLS-1$ //$NON-NLS-2$
	}

	public String getInputFormat() {
		return inputFormat;
	}

	public void setInputFormat(String inputFormat) {
		this.inputFormat = inputFormat;
	}

	public String getOutputFormat() {
		return outputFormat;
	}

	public void setOutputFormat(String outputFormat) {
		this.outputFormat = outputFormat;
	}
}
