// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.metadatas;

import java.util.HashMap;

import org.w3c.dom.Element;

// TODO: Auto-generated Javadoc
/**
 * The Class Metadata.
 */
public class DateMetadata extends org.txm.metadatas.Metadata
{
	public String inputFormat, outputFormat;
	
	/**
	 * Instantiates a new metadata.
	 *
	 * @param meta the n
	 */
	public DateMetadata(Element meta)
	{
		super(meta);
		inputFormat = meta.getAttribute("inputFormat"); //$NON-NLS-1$
		outputFormat = meta.getAttribute("outputFormat"); //$NON-NLS-1$
	}
	
	/**
	 * Instantiates a new metadata.
	 *
	 * @param id the id
	 * @param longname the longname
	 * @param type the type
	 * @param selection the selection
	 * @param partition the partition
	 * @param display the display
	 */
	public DateMetadata(String id, String shortname, String longname, String type, boolean selection, boolean partition, boolean display, String inputFormat, String outputFormat, int colwidth)
	{
		super(id, shortname, longname, type, null, selection, partition, display, true, colwidth);
		this.inputFormat = inputFormat;
		this.outputFormat = outputFormat;
	}
	
	/**
	 * Instantiates a new metadata.
	 * @param sAttributeInfos 
	 *
	 * @param n the dom element
	 */
	public DateMetadata(HashMap<String, String> sAttributeInfos, Element n) {
		super(sAttributeInfos, n);
		inputFormat = sAttributeInfos.get("inputFormat"); //$NON-NLS-1$
		outputFormat = sAttributeInfos.get("outputFormat"); //$NON-NLS-1$
		//System.out.println("FORMATS: "+inputFormat+" "+outputFormat);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return super.toString()+", inputFormat="+inputFormat+", outputFormat="+outputFormat; //$NON-NLS-1$ //$NON-NLS-2$
	}
}
