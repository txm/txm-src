package org.txm;

/**
 * Called by the Toolbox when a plugin needs to install things before beeing used
 * 
 * @author mdecorde
 *
 */
public class PostInstallationStep {
	
	public static final String EXTENSION_ID = PostInstallationStep.class.getCanonicalName();
	
	protected String name = "noname"; //$NON-NLS-1$
	
	public void install()
	{
		System.out.println("Installing "+name+"..."); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	public void preInstall()
	{
		System.out.println("Pre-Installing "+name+"..."); //$NON-NLS-1$ //$NON-NLS-2$
	}
}
