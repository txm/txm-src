package org.txm;

import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.Version;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.objects.Workspace;
import org.txm.utils.BundleUtils;
import org.txm.utils.logger.Log;

/**
 * Called by the Toolbox when a plugin needs to install things in a newly created/updated TXM user directory
 * 
 * @author mdecorde
 *
 */
public abstract class PostTXMHOMEInstallationStep {
	
	public static final String EXTENSION_ID = PostTXMHOMEInstallationStep.class.getCanonicalName();
	
	/**
	 * called after the Core has created its files
	 * 
	 * the do_install method is called only if the plugin version has changed
	 * 
	 * @return true if success, false if installation failed
	 */
	public final boolean install(Workspace workspace) {
		String bundle_id = FrameworkUtil.getBundle(getClass()).getSymbolicName(); 
		Version extcurrentVersion = BundleUtils.getBundleVersion(bundle_id);
		String extinstalledVersionString = Toolbox.getPreference(getName());
		if (extinstalledVersionString.isEmpty()) {
			extinstalledVersionString = "0.0.0"; //$NON-NLS-1$
		}
		Version extinstalledVersion = new Version(extinstalledVersionString);
		Log.fine(TXMCoreMessages.bind("{0} installed version: {1}.", getName(), extinstalledVersion)); //$NON-NLS-1$
		//System.out.println("Toolbox.startWorkspace(): workspace location = " + location);
		boolean extDoUpdateworkspace = extcurrentVersion.compareTo(extinstalledVersion) > 0;
		if (needsReinstall(workspace) || extDoUpdateworkspace) {
			Log.finer("Installing extension "+getName()+" reasons: workspace or extDoUpdateworkspace="+extDoUpdateworkspace); //$NON-NLS-1$ //$NON-NLS-2$
			if (do_install(workspace)) {
				Toolbox.setPreference(getName(), extcurrentVersion.toString()); // SAVE the installed version
			}
		} else {
			Log.finer(" no need to re-install "+getName()); //$NON-NLS-1$
		}
		return true;
	}
	
	/**
	 * copy files etc.
	 * 
	 * @param workspace
	 * @return
	 */
	public abstract boolean do_install(Workspace workspace);
	
	/**
	 * Allow an extension to test if the needed files are installed
	 * @param workspace
	 * @return true if the extension needs to reinstall itself
	 */
	public abstract boolean needsReinstall(Workspace workspace);
	
	/**
	 * 
	 * @return extension unique name
	 */
	public abstract String getName();
}
