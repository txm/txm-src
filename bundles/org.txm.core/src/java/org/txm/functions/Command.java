package org.txm.functions;

import java.util.HashSet;

import org.txm.core.results.TXMResult;

/**
 * Command description
 * 
 * @author mdecorde
 *
 */
public class Command {
	
	public final String cmd;
	public final Class<TXMResult> clazz;
	public final HashSet<String> mandatoryParameters = new HashSet<String>();
	public final HashSet<String> optionalParameters = new HashSet<String>();
	
	public Command(Class<TXMResult> clazz) {
		this.cmd = clazz.getSimpleName();
		this.clazz = clazz;
	}
	
	public Command(String cmd, Class<TXMResult> clazz) {
		this.cmd = cmd;
		this.clazz = clazz;
	}
	
	public String toString() {
		return cmd+" -> "+clazz.getSimpleName()+"("+mandatoryParameters+", "+optionalParameters+")";
	}
}
