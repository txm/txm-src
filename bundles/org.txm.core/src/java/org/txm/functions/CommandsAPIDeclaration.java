package org.txm.functions;

import java.util.ArrayList;

/**
 * Extension point used to declare new commands to the TXM core commands API
 * 
 * The plugin can add Command objects (like RCPCommand)
 * 
 * @author mdecorde
 *
 */
public abstract class CommandsAPIDeclaration {
	
	/**
	 * returns the Commands list to install
	 */
	public abstract ArrayList<Command> getCommandsList();
}
