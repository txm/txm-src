package org.txm.functions;

import java.util.HashMap;
import java.util.HashSet;

import org.txm.core.results.TXMParameters;
import org.txm.core.results.TXMResult;
import org.txm.utils.logger.Log;

/**
 * API to all knowned TXM commands.
 * 
 * Initialized during Toolbox initialisation using the TXMResult extension point
 * 
 * For now a command is :
 * - an ID
 * - a TXMResult class
 * - a HashSet of mandatory parameter names (the commands must check itself the parameters class)
 * - a HashSet of optional parameter names (the commands must check itself the parameters class)
 * 
 * @author mdecorde
 *
 */
public class CommandsAPI {
	
	protected static final HashMap<String, Command> cmds = new HashMap<String, Command>();
	
	public static void install(String cmd, Class<TXMResult> clazz) {
		CommandsAPI.cmds.put(cmd, new Command(cmd,clazz));
	}
	
	/**
	 * Manually install a command
	 * 
	 * @param command
	 */
	public static void install(Command command) {
		if (CommandsAPI.cmds.containsKey(command.cmd)) {
			Log.warning("The "+command.cmd+" command is already installed. Installing the new one: "+command);
		}
		CommandsAPI.cmds.put(command.cmd, command);
	}
	
	/**
	 * Manually install a command
	 * 
	 * @param cmd
	 * @param clazz
	 */
	public static void install(String cmd, Class<TXMResult> clazz, HashSet<String> mandatoryParameters) {
		Command command = new Command(cmd,clazz);
		command.mandatoryParameters.addAll(mandatoryParameters);
		CommandsAPI.cmds.put(cmd, command);
	}
	
	/**
	 * Manually install a command
	 * 
	 * @param cmd
	 * @param clazz
	 */
	public static void install(String cmd, Class<TXMResult> clazz, HashSet<String> mandatoryParameters, HashSet<String> optionalParameters) {
		Command command = new Command(cmd,clazz);
		command.mandatoryParameters.addAll(mandatoryParameters);
		command.optionalParameters.addAll(optionalParameters);
		CommandsAPI.cmds.put(cmd, command);
	}
	
	/**
	 * Manually uninstall a command
	 * 
	 * @param cmd
	 */
	public static void uninstall(String cmd) {
		CommandsAPI.cmds.remove(cmd);
	}
	
	public Object call(String cmd, TXMResult source, TXMParameters parameters) {
		
		Command command = cmds.get(cmd);
		if (command == null) {
			System.out.println("No command found with name: "+cmd);
			return null;
		}
		
		// compute directly the command using the parameters 
		TXMResult result = null;
		try {
			if (command.mandatoryParameters.containsAll(parameters.keySet())) {
				System.out.println("Computing "+command.cmd+"...");
				//result = command.clazz.newInstance(null, source);
				result = command.clazz.getConstructor(String.class, TXMResult.class).newInstance(null, source);
				result.setParameters(parameters);
				if (result.canCompute()) {
					result.compute();
				} else {
					System.out.println("Cannot compute result with "+cmd+" command and parameters: "+parameters);
				}
				//FIXME: debug
				System.out.println(result);
			} else {
				System.out.println("Failed to compute result with "+cmd+" command: missing some mandatory parameters: "+command.mandatoryParameters);
			}
		} catch (Throwable e) {
			System.out.println("Failed to compute result with "+cmd+" command and "+parameters+" parameters: "+e.getLocalizedMessage());
			Log.printStackTrace(e);
		}
		return result;
	}
	
	// portail : ?
	// groovy : CommandsApi.call("Concordances", ["query":"Je|je]");
	// bash-like : compute Concordances query:"Je|je"
}
