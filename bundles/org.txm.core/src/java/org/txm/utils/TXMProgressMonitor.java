package org.txm.utils;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.txm.utils.logger.Log;

/**
 * 
 * Wrapper for a {@link SubMonitor} instance dedicated to computing tasks.
 * The wrapper permits to protect the main root task.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class TXMProgressMonitor {
	
	
	/**
	 * Wrapped sub-monitor instance.
	 */
	protected SubMonitor monitor;
	
	
	/**
	 * Creates a new {@link TXMProgressMonitor} wrapping a default {@link LogMonitor}.
	 */
	public TXMProgressMonitor() {
		this(new LogMonitor());
	}
	
	
	/**
	 * Creates a new {@link TXMProgressMonitor} wrapping specified monitor.
	 * Also converts the monitor into a {@link SubMonitor} with 100 ticks of remaining work.
	 * 
	 * @param monitor the monitor to wrap and convert
	 */
	public TXMProgressMonitor(IProgressMonitor monitor) {
		this.monitor = SubMonitor.convert(monitor, 100);
	}
	
	
	/**
	 * Sets the monitor state as canceled.
	 * 
	 * @param value
	 */
	public void setCanceled(boolean value) {
		// Log.info(NLS.bind("Canceling computing of {0} ({1})...", this.getName(), this.getClass().getSimpleName()));
		this.monitor.setCanceled(value);
	}
	
	/**
	 * Sets the monitor current task name.
	 * 
	 * @param name
	 */
	public void setTask(String name) {
		this.monitor.subTask(name);
		Log.fine(name);
	}
	
	/**
	 * Increments the progress monitor of the specified amount of work.
	 *
	 * @param amount of work
	 */
	public void worked(int amount) {
		this.monitor.worked(amount);
	}
	
	
	/**
	 * Checks if the monitor is canceled.
	 *
	 * @return true if the monitor has been canceled by the user
	 */
	public boolean isCanceled() {
		return this.monitor.isCanceled();
	}
	
	/**
	 * Sets the monitor state as done.
	 */
	public void done() {
		this.monitor.done();
	}
	
	/**
	 * Sets the work remaining for the internal SubMonitor instance.
	 * 
	 * @param workRemaining number of ticks
	 */
	public void setWorkRemaining(int workRemaining) {
		this.monitor.setWorkRemaining(workRemaining);
	}
	
	/**
	 * Splits the current computing monitor if exists and returns a new {@link TXMProgressMonitor} with 100 ticks of remaining work.
	 * If current computing monitor doesn't exist, typically if calling this method outside of the TXMResult._compute() implementations,
	 * it returns a new LogMonitor with 100 ticks.
	 * 
	 * @param totalWork
	 * @return
	 */
	public TXMProgressMonitor createNewMonitor(int totalWork) {
		return new TXMProgressMonitor(this.monitor.split(totalWork));
	}
	
	
	/**
	 * Returns the instance of the internal {@link SubMonitor}.
	 * Using this {@link SubMonitor} may leads to redefine the main task.
	 * 
	 * @return the instance of the internal {@link SubMonitor}
	 */
	public SubMonitor internalGetSubMonitor() {
		return this.monitor;
	}
	
}
