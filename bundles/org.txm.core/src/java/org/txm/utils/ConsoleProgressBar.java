package org.txm.utils;
import org.eclipse.core.runtime.IProgressMonitor;

public class ConsoleProgressBar implements IProgressMonitor {
	static int LINE_PROGRESS = 10; // number of tick to draw a line
	static int TOTAL_PROGRESS = 100;
	
	double progress = 0.0d;
	double progress_per_tick = 1.0d;
	int point_progress = 0;
	boolean done = false;
	String mode = "  % ";
	int percent = 1;
	
	public ConsoleProgressBar(long amount) {
		start(amount);
	}
	
	public void start(long amount) {
		progress = 0.0d;
		percent = 1;
		point_progress = 0;
		done = false;
		progress_per_tick = 100f / amount;
		
		if (amount <= 100L) {
			mode = String.format("%d ", amount);
			if (mode.length() == 2) {
				mode = "  "+mode;
			} else if (mode.length() == 3) {
				mode = " "+mode;
			}
			progress_per_tick = 1;
		}
	}
	
	public synchronized void tick() {
		if (done) return;
		if (progress == 0.0d) System.out.print(mode);
		
		progress += progress_per_tick;
		while (progress > point_progress) {
			point_progress++;
			if (point_progress%LINE_PROGRESS == 0) {
//				System.out.print("|");
				System.out.print(percent++);
			}
			else System.out.print(".");
			
			if (point_progress == TOTAL_PROGRESS) {
				done = true;
			}
		}
	}
		
	public static void main(String[] args) {
		int[] amounts = {10,42,150,155,15000,150000};
		for (int amount : amounts) {
			System.out.println(""+amount+" ticks");
			ConsoleProgressBar cpb = new ConsoleProgressBar(amount);
			for (int i = 0 ; i < amount ; i++) cpb.tick();
			cpb.done();
		}
	}

	public void worked(int amount, String message) {
		this.tick();
		System.out.println(message);
	}

	public void setMessage(String message) {
		String space = String.format("%"+(4+point_progress)+"s", " ");
		System.out.println("\n"+space+message);
		System.out.print(space);
	}

	@Override
	public boolean isCanceled() {
		return false;
	}

	@Override
	public void done() {
		done = true;
		System.out.print(" - Done\n");
	}

	@Override
	public void worked(int step) {
		for (int i = 0 ; i < step ; i++) tick();
	}
	
	public void worked() {
		tick();
	}

	public void acquireSemaphore() {
		
	}

	public boolean tryAcquireSemaphore() {
		return false;
	}

	public void releaseSemaphore() { }

	@Override
	public void beginTask(String name, int l) {
		System.out.println("Begining "+name);
		this.start(l);
	}

	@Override
	public void subTask(String name) {
		System.out.println("[ "+name+"]");
	}

	@Override
	public void internalWorked(double work) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCanceled(boolean value) {
		if (value) done();
	}

	@Override
	public void setTaskName(String name) {
		// nothing to show
	}
}