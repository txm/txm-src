package org.txm.utils;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.Version;
import org.osgi.framework.wiring.BundleWiring;
import org.txm.Toolbox;
import org.txm.core.preferences.TBXPreferences;
import org.txm.utils.io.FileCopy;
import org.txm.utils.logger.Log;
import org.txm.utils.zip.Zip;

public class BundleUtils {
	
	// protected static File INSTALLDIR = new File(System.getProperty("eclipse.launcher")).getParentFile().getParentFile();
	/**
	 * 
	 * @param bundle_id the Bundle ID
	 * @return the relative path to the bundle directory
	 */
	public static Version getBundleVersion(String bundle_id) {
		Bundle bundle = Platform.getBundle(bundle_id);
		if (bundle == null) {
			// System.out.println("Error while initializing "+bundle_id+" plugin: bundle not found.");
			return null;
		}
		return bundle.getVersion();
	}
	
	public static Bundle getBundle(Class<?> clazz) {
		return FrameworkUtil.getBundle(clazz);
	}
	
	public static Bundle getBundle(String bundle_id) {
		return Platform.getBundle(bundle_id);
	}
	
	public static String getBundleId(Class<?> clazz) {
		return getBundle(clazz).getSymbolicName();
	}
	
	/**
	 * 
	 * @param bundle_id the Bundle ID
	 * @return the relative path to the bundle directory
	 */
	public static String getBundleLocation(String bundle_id) {
		Bundle bundle = Platform.getBundle(bundle_id);
		if (bundle == null) {
			return null;
		}
		try {
			return FileLocator.getBundleFile(bundle).getAbsolutePath();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error path " + bundle_id;
		}
		// return bundle.getLocation();
	}
	
	/**
	 * 
	 * @param bundle_id the Bundle ID
	 * @return the relative path to the bundle directory
	 */
	public static File getBundleFile(String bundle_id) {
		Bundle bundle = Platform.getBundle(bundle_id);
		if (bundle == null) {
			return null;
		}
		try {
			return FileLocator.getBundleFile(bundle).getAbsoluteFile();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		// return bundle.getLocation();
	}
	
	/**
	 * 
	 * @param bundle_id the Bundle ID
	 * @return the relative path to the bundle directory
	 */
	public static File getFile(String bundle_id, String source_relative_directory, String file_path, String file_name) {
		File bundleDir = getBundleFile(bundle_id);
		File fileInToolbox = null;
		
		if (bundleDir.isDirectory()) { // TXM is run from Eclipse or we've decided to ship the Toolbox plugin in a directory
			fileInToolbox = new File(bundleDir, file_path + file_name);
			if (!fileInToolbox.exists()) { // maybe run from Eclipse, then add the relative directory
				fileInToolbox = new File(bundleDir, source_relative_directory + file_path + file_name);
			}
		}
		else { // jar file
			Bundle bundle = Platform.getBundle(bundle_id);
			try {
				fileInToolbox = new File(bundle.getEntry(file_path + file_name).toURI());
			}
			catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
		return fileInToolbox;
	}
	
	// /**
	// *
	// * @param bundle_id the Bundle ID
	// * @return the absolute File to the bundle directory. The File is a directory or a file.
	// */
	// public static File getBundleFile(String bundle_id) {
	// Log.info("Retrieving file_path from "+bundle_id+" plugin.");
	// Bundle bundle = Platform.getBundle(bundle_id);
	// if (bundle == null) {
	// System.out.println("Error while initializing '"+bundle_id+"' plugin: bundle not found.");
	// return null;
	// }
	// String bundleLocation = bundle.getLocation();
	// String eclipseHomeLocation = System.getProperty("eclipse.home.location");
	//
	// int idx = bundleLocation.lastIndexOf(":");
	// if (idx > 0) bundleLocation = bundleLocation.substring(idx + 1);
	//
	// idx = eclipseHomeLocation.indexOf("file:");
	// if (idx == 0) eclipseHomeLocation = eclipseHomeLocation.substring(5);
	//
	// File bundleDir = new File(bundleLocation);
	// if (bundleLocation.startsWith("/")) {
	// bundleDir = new File(bundleLocation);
	// } else {
	// bundleDir = new File(eclipseHomeLocation, bundleLocation);
	// }
	//
	// return bundleDir;
	// }
	
	public static boolean copyFiles(String bundle_id, String source_relative_directory, String file_path, String file_name, File outputDirectory) {
		return copyFiles(bundle_id, source_relative_directory, file_path, file_name, outputDirectory, true);
	}
	
	public static final SimpleDateFormat qualifierDateFormatter = new SimpleDateFormat("YYYYMMDDHHmm");
	
	public static long getDate(String bundle_id) {
		
//		File bundleDir = getBundleFile(bundle_id);
//		long l1 = bundleDir.lastModified();
////		System.out.println("bundle dir "+bundleDir+" date="+l1);
//		
//		Bundle bundle = Platform.getBundle(bundle_id);
//		long l2 = bundle.getLastModified();
////		System.out.println("bundle dir "+bundle+" date="+l2);
//		return Math.max(l1, l2);
		
		Version v = getBundleVersion(bundle_id);
		String qualifier = v.getQualifier(); // 	Textometrie.org	Macro	1.0.0.202305171515	org.txm.groovy.core 
		// "(....)(..)(..)(..)(..)", "$1-$2-$3 $4h$5"
		
		try {
			Date d = qualifierDateFormatter.parse(qualifier);
			return d.getTime();
		} catch(Exception e) {
			return 0;
		}
		
	}
	
	/**
	 * Copy a file from the bundle directory or Jar to @outputDirectory
	 * 
	 * if the bundle file is a directory then the asked file must be in the "res" directory
	 * 
	 * @param bundle_id
	 * @param source_relative_directory only used when TXM is run from Eclipse
	 * @param file_path relative path to the file to copy
	 * @param file_name the file name to copy <b>if file_name is a directory be sure to add "/" at the end of the file</b>
	 * @param outputDirectory
	 * @return true if success
	 */
	public static boolean copyFiles(String bundle_id, String source_relative_directory, String file_path, String file_name, File outputDirectory, boolean replace) {
		
		if (source_relative_directory.length() > 0 && !source_relative_directory.endsWith("/")) source_relative_directory += "/";
		if (file_path.length() > 0 && !file_path.endsWith("/")) file_path += "/";
		
		File bundleDir = getBundleFile(bundle_id);
		File outfile = new File(outputDirectory, file_name);
		
		if (bundleDir.isDirectory()) { // TXM is run from Eclipse or we've decided to ship the Toolbox plugin in a directory
			File fileInToolbox = new File(bundleDir, file_path + file_name);
			if (!fileInToolbox.exists()) { // maybe run from Eclipse, then add the relative directory
				fileInToolbox = new File(bundleDir, source_relative_directory + file_path + file_name);
			}
			try {
				if (fileInToolbox.isDirectory())
					FileCopy.copyFiles(fileInToolbox, outfile, replace);
				else {
					FileCopy.copy(fileInToolbox, outfile, replace);
				}
			}
			catch (IOException e) {
				System.out.println("Copy of ressource script failed: " + e);
				Log.printStackTrace(e);
				return false;
			}
		}
		else { // jar file
				 // if (!bundleDir.exists()) { // get from the installed Toolbox
				 // bundleDir = new File(INSTALLDIR, "TXM/"+bundleLocation);
				 // }
			
			if (!Zip.extractOneFile(bundleDir, file_path + file_name, outfile, false)) {
				System.out.println("Extraction of ressource failed from " + bundleDir + " with path " + file_path + file_name);
				return false;
			}
		}
		return true;
	}
	
	public static boolean replaceFilesIfNewer(String bundle_id, String source_relative_directory, String file_path, String file_name, File fileToReplace) {
		if (!source_relative_directory.endsWith("/")) source_relative_directory += "/";
		if (!file_path.endsWith("/")) file_path += "/";
		
		File bundleDir = getBundleFile(bundle_id);
		// String bundleLocation = getBundleLocation(bundle_id);
		
		if (bundleDir.isDirectory()) { // TXM is run from Eclipse or we've decided to ship the Toolbox plugin in a directory
			File fileInToolbox = new File(bundleDir, file_path + file_name);
			if (!fileInToolbox.exists()) { // TXM may be run from Eclipse, then add the source relative directory
				fileInToolbox = new File(bundleDir, source_relative_directory + file_path + file_name);
			}
			
			if (!fileInToolbox.exists()) {
				return false;
			}
			
			if (fileInToolbox.lastModified() <= fileToReplace.lastModified()) { // no need to update
				return true;
			}
			
			try {
				if (fileInToolbox.isDirectory()) {
					FileCopy.copyFiles(fileInToolbox, fileToReplace);
				}
				else {
					FileCopy.copy(fileInToolbox, fileToReplace);
				}
			}
			catch (IOException e) {
				System.out.println("Copy of ressource script failed: " + e);
				Log.printStackTrace(e);
				return false;
			}
		}
		else { // jar file
				 // if (!bundleDir.exists()) { // get from the installed Toolbox
				 // bundleDir = new File(INSTALLDIR, "TXM/"+bundleLocation);
				 // }
			
			if (!Zip.extractOneFile(bundleDir, file_path + file_name, fileToReplace, true)) {
				System.out.println("Extraction of ressource failed from " + bundleDir + " with path " + file_path + file_name);
				return false;
			}
		}
		return true;
		
	}
	
	/**
	 * 
	 * @return install directory path
	 */
	public static String getInstallDirectory() {
		String path = System.getProperty("osgi.install.area");
		if (path == null) {
			path = System.getProperty("eclipse.home.location");
		}
		if (path == null) {
			path = Toolbox.getPreference(TBXPreferences.INSTALL_DIR);
		}
		if (path.startsWith("file:")) {
			if (OSDetector.isFamilyWindows()) {
				path = path.substring(6); // file://C:/ ...
			}
			else {
				path = path.substring(5); // file://home/ ...
			}
		}
		return path;
	}
	
	public static ClassLoader getLoader(String bundle_id) {
		Bundle bundle = Platform.getBundle(bundle_id);
		if (bundle == null) {
			System.out.println("Error while initializing '" + bundle_id + "' plugin: bundle not found.");
			return null;
		}
		
		BundleWiring bundleWiring = bundle.adapt(BundleWiring.class);
		ClassLoader classLoader = bundleWiring.getClassLoader();
		return classLoader;
	}
}
