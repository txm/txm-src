package org.txm.utils;

import java.util.UUID;

import org.eclipse.core.runtime.IProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * A silent monitor that logs in console FINE level messages only.
 * 
 * @author mdecorde
 *
 */
public class LogMonitor implements IProgressMonitor {
	
	String monitorName;
	
	boolean cancel = false;
	
	/**
	 * Un-named monitor
	 */
	public LogMonitor() {
		this("M" + UUID.randomUUID());
	}
	
	/**
	 * Named monitor
	 * 
	 * @param name the monitor's name
	 */
	public LogMonitor(String name) {
		this.monitorName = name;
	}
	
	@Override
	public void beginTask(String name, int totalWork) {
		Log.fine(this.monitorName + " -> " + name + " " + totalWork);
	}
	
	@Override
	public void done() {
		Log.fine(this.monitorName + " -> Done.");
	}
	
	@Override
	public void internalWorked(double work) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean isCanceled() {
		return cancel;
	}
	
	@Override
	public void setCanceled(boolean value) {
		this.cancel = value;
	}
	
	@Override
	public void setTaskName(String name) {
		Log.fine(this.monitorName + " -> task " + name);
	}
	
	@Override
	public void subTask(String name) {
		Log.fine(this.monitorName + " -> subTask " + name);
		
	}
	
	@Override
	public void worked(int work) {
		Log.finest(this.monitorName + " -> worked "+work);
	}
}
