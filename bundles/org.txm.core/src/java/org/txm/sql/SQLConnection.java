package org.txm.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.txm.utils.logger.Log;

public class SQLConnection {

	public static SQLConnection sqlconn;

	public static String SQL_ADDRESS = "address";
	public static String SQL_USER = "user";
	public static String SQL_DRIVER = "driver";
	public static String SQL_PASSWORD = "password";
	public static String SQL_PORT = "port";

	public SQLConnection getSQLConnection(){
		if(sqlconn == null){
			return new SQLConnection();
		}
		return sqlconn;
	}

	public static Connection createConnection(String dbaddress, String dbdrivername, String dbusername, String dbpassword) throws Exception{
		Connection con = null;
		
		Class.forName(dbdrivername);
		con = DriverManager.getConnection(dbaddress, dbusername, dbpassword);
		Log.fine("Base de données connectée ["+dbaddress+"]");
		return con;
	}

	public static boolean closeConnection(Connection con){
		try {
			con.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}


}
