// add a click listeners to all 'collapsible' class objects when the page is loaded


function onCollapsibleClicked(node) {

    node.classList.toggle("active");
    var content = node.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
	node.textContent="➕"
    } else {
      content.style.display = "block";
      node.textContent="➖"
    }
}