<!-- The Identity Transformation -->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xd:doc type="stylesheet">
		<xd:short>
			This stylesheet is designed for TXM XTZ+CSV import module to split a
			single file containing a teiCorpus into individual files for each TEI
			child. This stylesheet should be used at "1-split-merge" step. See
			TXM User Manual for more details
			(http://textometrie.ens-lyon.fr/spip.php?rubrique64)
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2017, CNRS / UMR 5317 IHRIM (CACTUS research group)</xd:copyright>
	</xd:doc>

	<xsl:param name="output-directory">
		<xsl:analyze-string select="document-uri(.)"
			regex="^(.*)/([^/]+)\.[^/.]+$">
			<xsl:matching-substring>
				<xsl:value-of select="regex-group(1)" />
			</xsl:matching-substring>
		</xsl:analyze-string>
	</xsl:param>

	<xsl:variable name="filename">
		<xsl:analyze-string select="document-uri(.)"
			regex="^(.*)/([^/]+)\.[^/.]+$">
			<xsl:matching-substring>
				<xsl:value-of select="regex-group(2)" />
			</xsl:matching-substring>
		</xsl:analyze-string>
	</xsl:variable>

	<xsl:template match="*:teiCorpus">
		<xsl:for-each select="descendant::*:TEI">
			<xsl:variable name="position" as="xs:integer">
				<xsl:number level="any" />
			</xsl:variable>
			<xsl:variable name="result-filename">
				<xsl:choose>
					<xsl:when test="descendant::*:text[1]/@*:id">
						<xsl:value-of select="descendant::*:text[1]/@*:id" />
					</xsl:when>
					<xsl:when test="@*:id">
						<xsl:value-of select="@*:id" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of
							select="concat($filename,'-',format-number($position,'0000'))" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:result-document
				href="{$output-directory}/{$result-filename}.xml">
				<xsl:copy-of select="."></xsl:copy-of>
			</xsl:result-document>
		</xsl:for-each>
	</xsl:template>


</xsl:stylesheet>
