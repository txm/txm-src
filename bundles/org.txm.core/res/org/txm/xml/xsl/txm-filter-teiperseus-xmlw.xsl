<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	exclude-result-prefixes="tei edate xd" version="2.0">

	<xsl:import href="txm-filter-teip5-xmlw-preserve.xsl" />
	<xd:doc type="stylesheet">
		<xd:short>
			A stylesheet to prepare PERSEUS XML-TEI texts to TXM import.
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2012, CNRS / ICAR (ICAR3 LinCoBaTO)</xd:copyright>
	</xd:doc>


	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />

	<xsl:template match="//tei:text">
		<xsl:copy>
			<xsl:copy-of select="@*" />
			<xsl:attribute name="author"><xsl:value-of
				select="//tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author" /></xsl:attribute>
			<xsl:attribute name="title"><xsl:value-of
				select="//tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type='work']" /></xsl:attribute>
			<xsl:attribute name="editor"><xsl:value-of
				select="//tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:editor" /></xsl:attribute>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="//tei:sp">
		<div3 type="sp" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates />
		</div3>
	</xsl:template>

	<xsl:template match="//tei:speaker">
		<head>
			<xsl:apply-templates />
		</head>
	</xsl:template>

	<xsl:template match="//tei:bibl">
		<note type="bibl" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates />
		</note>
	</xsl:template>
</xsl:stylesheet>