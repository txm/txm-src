<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	exclude-result-prefixes="tei edate xd" version="2.0">

	<xd:doc type="stylesheet">
		<xd:short>
			Feuille de style de suppression de tous les éléments XML sauf
			"select",
			à utiliser comme prétraitement de l'import XML/w+CSV.

			Pour changer le nom de la balise à conserver, mettez le nouveau nom
			à la place de "select" dans la balise
			<xsl:param />
			ci-dessous. Vous pouvez
			mettre plusieurs noms en les séparant par une barre verticale. Vous
			pouvez également indiquer le(s) nom(s) de balises à conserver comme un
			paramètre au lancement de cette feuille de style

			L'espace de nommage de la balise à conserver n'est pas pris en compte.
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2013, CNRS / ICAR (ICAR3 LinCoBaTO)</xd:copyright>
	</xd:doc>


	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />

	<!-- Mettez le nom de la balise à conserver à la place de "select" ci-dessous -->
	<xsl:param name="tagToKeep">
		select
	</xsl:param>

	<xsl:variable name="tagToKeepStrict">
		<xsl:value-of select="concat('^',$tagToKeep,'$')" />
	</xsl:variable>

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>


	<xsl:template match="/">
		<xsl:apply-templates
			select="processing-instruction()" />
		<xsl:choose>
			<xsl:when
				test="descendant::*[matches(local-name(),$tagToKeepStrict)]">
				<xsl:apply-templates mode="with-select" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="*" mode="with-select">
		<xsl:choose>
			<xsl:when
				test="ancestor-or-self::*[matches(local-name(),$tagToKeepStrict)]">
				<xsl:copy>
					<xsl:apply-templates select="@*|node()" />
				</xsl:copy>
			</xsl:when>
			<xsl:when
				test="descendant::*[matches(local-name(),$tagToKeepStrict)]">
				<xsl:copy>
					<xsl:apply-templates select="@*" />
					<xsl:apply-templates
						select="*[descendant-or-self::*[matches(local-name(),$tagToKeepStrict)]]"
						mode="with-select" />
				</xsl:copy>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>