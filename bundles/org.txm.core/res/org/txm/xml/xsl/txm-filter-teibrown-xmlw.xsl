<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	exclude-result-prefixes="tei edate xd" version="2.0">

	<xd:doc type="stylesheet">
		<xd:short>
			This stylesheet prepares XML-TEI P5 files of the Brown corpus
			for TXM import with xml/w+csv module.
			Specify the location of this file on your system in the "Front
			XSLT" import option.
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2012, CNRS / ICAR (ICAR3 LinCoBaTO)</xd:copyright>
	</xd:doc>


	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />


	<!-- <xsl:template match="/"> <xsl:apply-templates/> </xsl:template> -->

	<xsl:template match="*">
		<xsl:element namespace="http://www.tei-c.org/ns/1.0"
			name="{local-name(.)}">
			<xsl:apply-templates
				select="*|@*|processing-instruction()|comment()|text()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="@*|comment()">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="text()">
		<xsl:analyze-string select="."
			regex="&amp;|&lt;|&gt;|--|\*(\w+)">
			<xsl:matching-substring>
				<xsl:choose>
					<xsl:when test="matches(.,'&amp;')">
						<expan xmlns="http://www.tei-c.org/ns/1.0">and</expan>
					</xsl:when>
					<xsl:when test="matches(.,'&lt;')">
						<xsl:text>[</xsl:text>
					</xsl:when>
					<xsl:when test="matches(.,'&gt;')">
						<xsl:text>]</xsl:text>
					</xsl:when>
					<xsl:when test="matches(.,'--')">
						<!-- double dash provokes malformed xml in the notes in the notes placed 
							in xml comments by the tokenizer -->
						<xsl:text> - </xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="." />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:matching-substring>
			<xsl:non-matching-substring>
				<xsl:value-of select="." />
			</xsl:non-matching-substring>
		</xsl:analyze-string>
	</xsl:template>


	<xsl:template match="processing-instruction()" />

	<!-- teiHeader deleted for the xml/w import module -->

	<xsl:template match="tei:teiHeader">
		<!--<xsl:copy-of select="."/> -->
	</xsl:template>

	<xsl:template match="tei:text">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="type">
			<xsl:choose>
				<xsl:when test="@decls='A'">PRESS: REPORTAGE</xsl:when>
				<xsl:when test="@decls='B'">PRESS: EDITORIAL</xsl:when>
				<xsl:when test="@decls='C'">PRESS: REVIEWS</xsl:when>
				<xsl:when test="@decls='D'">RELIGION</xsl:when>
				<xsl:when test="@decls='E'">SKILL AND HOBBIES</xsl:when>
				<xsl:when test="@decls='F'">POPULAR LORE</xsl:when>
				<xsl:when test="@decls='G'">BELLES-LETTRES</xsl:when>
				<xsl:when test="@decls='H'">MISCELLANEOUS: GOVERNMENT AND HOUSE ORGANS</xsl:when>
				<xsl:when test="@decls='J'">LEARNED</xsl:when>
				<xsl:when test="@decls='K'">FICTION: GENERAL</xsl:when>
				<xsl:when test="@decls='L'">FICTION: MYSTERY</xsl:when>
				<xsl:when test="@decls='M'">FICTION: SCIENCE</xsl:when>
				<xsl:when test="@decls='N'">FICTION: ADVENTURE</xsl:when>
				<xsl:when test="@decls='P'">FICTION: ROMANCE</xsl:when>
				<xsl:when test="@decls='R'">HUMOR</xsl:when>				
			</xsl:choose>
		</xsl:attribute>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="@type|@pos">
		<xsl:attribute name="type">
		<xsl:value-of select="translate(.,' *','_')"></xsl:value-of>
	</xsl:attribute>
	</xsl:template>

	<xsl:template match="tei:mw">
		<w xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</w>
	</xsl:template>

	<xsl:template match="tei:c[@type='pct']">
		<w xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</w>
	</xsl:template>

</xsl:stylesheet>