<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:txm="http://textometrie.org/1.0"
	exclude-result-prefixes="tei edate txm"
	xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />


	<!-- Partie générale : on copie les éléments, les attributs, le texte, les 
		commentaires, on supprime mes prrocessing intstructions -->

	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates
				select="*|@*|processing-instruction()|comment()|text()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="*" mode="position">
		<xsl:value-of select="count(preceding-sibling::*)" />
	</xsl:template>


	<xsl:template
		match="@*|comment()|processing-instruction()">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="text()">
		<xsl:value-of select="." />
	</xsl:template>

	<xsl:template match="teiHeader" />

	<xsl:template match="text">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:if test="not(child::*[1][self::pb])">
				<pb xmlns="http://www.tei-c.org/ns/1.0" n="title" />
			</xsl:if>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="choice[not(@txm:wordPart)]">
		<xsl:choose>
			<xsl:when test="child::corr">
				<corr xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:attribute name="sic"><xsl:value-of
						select="normalize-space(child::sic)" /></xsl:attribute>
					<xsl:apply-templates select="child::corr/w" />
					<xsl:if test="child::sic//lb and not(child::corr//lb)">
						<xsl:copy-of select="child::sic//lb" />
					</xsl:if>
				</corr>
			</xsl:when>
			<xsl:when test="child::reg">
				<reg xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:attribute name="orig"><xsl:value-of
						select="normalize-space(child::orig)" /></xsl:attribute>
					<xsl:apply-templates select="child::reg/w" />
					<xsl:if test="child::orig//lb  and not(child::reg//lb)">
						<xsl:copy-of select="child::orig//lb" />
					</xsl:if>
				</reg>
			</xsl:when>
			<xsl:when test="child::expan">
				<expan xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:attribute name="abbr"><xsl:value-of
						select="normalize-space(child::abbr)" /></xsl:attribute>
					<xsl:apply-templates select="child::expan/w" />
				</expan>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="w">
		<xsl:variable name="nextLb">
			<xsl:choose>
				<xsl:when test="following::lb">
					<xsl:apply-templates select="following::lb[1]"
						mode="position" />
				</xsl:when>
				<!-- <xsl:when test="following::lb[@rend='hyphen']"> <xsl:apply-templates 
					select="following::lb[@rend='hyphen'][1]" mode="position"/> </xsl:when> -->
				<xsl:otherwise>
					99999999
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="nextW1">
			<xsl:choose>
				<xsl:when test="following-sibling::w[1]">
					<xsl:apply-templates select="following::w[1]"
						mode="position" />
				</xsl:when>
				<xsl:otherwise>
					0
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="nextW2">
			<xsl:choose>
				<xsl:when test="following::w[2]">
					<xsl:apply-templates select="following::w[2]"
						mode="position" />
				</xsl:when>
				<xsl:otherwise>
					0
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="ref">
			<xsl:choose>
				<xsl:when test="preceding::pb[1][@n]">
					<xsl:value-of
						select="concat(ancestor::text/@id,', p. ',preceding::pb[1]/@n)" />
				</xsl:when>
				<xsl:when test="preceding::pb[1][@facs]">
					<xsl:value-of
						select="concat(ancestor::text/@id,', p. ',substring-before(preceding::pb[1]/@facs,'.'))" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="ancestor::text/@id" />
				</xsl:otherwise>
			</xsl:choose>

		</xsl:variable>
		<xsl:variable name="lang">
			<xsl:choose>
				<xsl:when test="ancestor::*[@lang]">
					<xsl:value-of select="ancestor::*[@lang][1]/@lang" />
				</xsl:when>
				<xsl:when test="ancestor::*[@xml:lang]">
					<xsl:value-of
						select="ancestor::*[@xml:lang][1]/@xml:lang" />
				</xsl:when>
				<xsl:otherwise>
					frm
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="nametype">
			<xsl:choose>
				<xsl:when test="ancestor::name">
					<xsl:choose>
						<xsl:when test="ancestor::name[1][@type]">
							<xsl:value-of select="ancestor::name[1]/@type" />
						</xsl:when>
						<xsl:otherwise>
							name
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					NA
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="lb">
			<xsl:choose>
				<xsl:when test="preceding-sibling::*[1][self::lb]">
					avant
				</xsl:when>
				<xsl:when
					test="following-sibling::*[1][self::lb or self::pb]">
					après
				</xsl:when>
				<xsl:otherwise>
					non
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="sic">
			<xsl:choose>
				<xsl:when test="parent::corr[parent::choice]">
					<xsl:value-of
						select="ancestor::choice/sic/w[position()]" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="."></xsl:value-of>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="orig">
			<xsl:choose>
				<xsl:when test="parent::reg[parent::choice]">
					<xsl:value-of
						select="ancestor::choice/orig/w[position()]" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="."></xsl:value-of>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="abbr">
			<xsl:choose>
				<xsl:when test="parent::expan[parent::choice]">
					<xsl:value-of
						select="ancestor::choice/abbr/w[position()]" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="."></xsl:value-of>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when
				test="matches(.,'^\s*[\-=]\s*$') and ($nextW1 &gt; $nextLb)">
				<xsl:comment>
					trait d'union effacé
				</xsl:comment>
			</xsl:when>
			<xsl:when
				test="matches(.,'[\-=]\s*$') and ($nextW1 &gt; $nextLb)">
				<w xmlns="http://www.tei-c.org/ns/1.0" ref="{$ref}" lang="{$lang}"
					nametype="{$nametype}" sic="{$sic}" orig="{$orig}" abbr="{$abbr}">
					<xsl:apply-templates select="@*" />
					<xsl:attribute name="lb">milieu-marqué</xsl:attribute>
					<xsl:analyze-string select="normalize-space(.)"
						regex="^(.+)(.)$">
						<xsl:matching-substring>
							<xsl:value-of select="regex-group(1)" />
							<lb rend="hyphen({regex-group(2)})" />
						</xsl:matching-substring>
					</xsl:analyze-string>
					<xsl:value-of
						select="normalize-space(following::w[1])" />
				</w>
			</xsl:when>
			<xsl:when
				test="following::w[1][matches(.,'^\s*[\-=]\s*$')] and ($nextW2 &gt; $nextLb)">
				<w xmlns="http://www.tei-c.org/ns/1.0" ref="{$ref}" lang="{$lang}"
					nametype="{$nametype}" sic="{$sic}" orig="{$orig}" abbr="{$abbr}">
					<xsl:apply-templates select="@*" />
					<xsl:attribute name="lb">milieu-marqué2</xsl:attribute>
					<xsl:value-of select="normalize-space(.)" />
					<lb rend="hyphen({normalize-space(following::w[1])})" />
					<xsl:value-of
						select="normalize-space(following::w[2])" />
				</w>
			</xsl:when>
			<xsl:when
				test="not(matches(.,'-\s*$')) and following-sibling::*[1][self::lb[@rend='hyphen']]">
				<w xmlns="http://www.tei-c.org/ns/1.0" ref="{$ref}" lang="{$lang}"
					nametype="{$nametype}" sic="{$sic}" orig="{$orig}" abbr="{$abbr}">
					<xsl:apply-templates select="@*" />
					<xsl:attribute name="lb">milieu-non-marqué</xsl:attribute>
					<xsl:value-of select="normalize-space(.)" />
					<lb rend="noHyphen" />
					<xsl:value-of
						select="normalize-space(following::w[1])" />
				</w>
			</xsl:when>
			<xsl:when
				test="preceding-sibling::*[1][self::lb[@rend='hyphen']]">
				<xsl:comment>
					fin de mot coupé :
					<xsl:value-of select="." />
				</xsl:comment>
			</xsl:when>
			<xsl:when
				test="preceding-sibling::*[1][self::lb] and preceding-sibling::*[2][self::w[matches(.,'\w[\-=]\s*$')]]">
				<xsl:comment>
					fin de mot coupé (lb non marqué !) :
					<xsl:value-of select="." />
				</xsl:comment>
			</xsl:when>
			<xsl:when
				test="preceding-sibling::*[1][matches(@txm:wordPart,'^(end|middle)$')]">
				<xsl:comment>
					fin de mot coupé par une balise :
					<xsl:value-of select="normalize-space(.)" />
				</xsl:comment>
			</xsl:when>
			<xsl:when
				test="preceding-sibling::*[1][@txm:wordPart='start']">
				<xsl:choose>
					<xsl:when
						test="preceding-sibling::*[1][self::choice][child::reg]">
						<reg>
							<xsl:apply-templates
								select="preceding-sibling::choice[1]/reg/@*" />
							<w xmlns="http://www.tei-c.org/ns/1.0" ref="{$ref}"
								lang="{$lang}" nametype="{$nametype}" sic="{$sic}"
								abbr="{$abbr}" lb="{$lb}">
								<xsl:attribute name="orig"><xsl:value-of
									select="concat(preceding-sibling::choice[1]/orig[1]/w[1],.)" /></xsl:attribute>
								<xsl:apply-templates select="@*" />
								<xsl:value-of
									select="preceding-sibling::choice[1]/reg/w" />
								<xsl:apply-templates />
							</w>
						</reg>
					</xsl:when>
					<xsl:when
						test="preceding-sibling::*[1][self::choice][child::corr]">
						<corr>
							<xsl:apply-templates
								select="preceding-sibling::choice[1]/corr/@*" />
							<w xmlns="http://www.tei-c.org/ns/1.0" ref="{$ref}"
								lang="{$lang}" nametype="{$nametype}" orig="{$orig}"
								abbr="{$abbr}" lb="{$lb}">
								<xsl:attribute name="sic"><xsl:value-of
									select="concat(preceding-sibling::choice[1]/sic[1]/w[1],.)" /></xsl:attribute>
								<xsl:apply-templates select="@*" />
								<xsl:value-of
									select="preceding-sibling::choice[1]/reg/w" />
								<xsl:apply-templates />
							</w>
						</corr>
					</xsl:when>
					<xsl:when test="preceding-sibling::*[1][self::supplied]">
						<w xmlns="http://www.tei-c.org/ns/1.0" ref="{$ref}"
							lang="{$lang}" nametype="{$nametype}" orig="{$orig}"
							abbr="{$abbr}" lb="{$lb}">
							<xsl:attribute name="sic"><xsl:value-of
								select="." /></xsl:attribute>
							<xsl:apply-templates select="@*" />
							<supplied xmlns="http://www.tei-c.org/ns/1.0">
								<xsl:value-of
									select="preceding-sibling::supplied[1]/w" />
							</supplied>
							<xsl:apply-templates />
						</w>
					</xsl:when>
					<xsl:when
						test="preceding-sibling::*[1][self::subst][child::add]">
						<w xmlns="http://www.tei-c.org/ns/1.0" ref="{$ref}"
							lang="{$lang}" nametype="{$nametype}" orig="{$orig}"
							abbr="{$abbr}" lb="{$lb}"
							del="{normalize-space(preceding-sibling::subst[1]/del)}">
							<xsl:attribute name="del"><xsl:value-of
								select="concat('(',preceding-sibling::subst[1]/del[1]/w[1],')',.)" /></xsl:attribute>
							<xsl:apply-templates select="@*" />
							<xsl:element name="add"
								namespace="http://www.tei-c.org/ns/1.0">
								<!-- <xsl:attribute name="rend">inWord</xsl:attribute> -->
								<xsl:attribute name="del"><xsl:value-of
									select="normalize-space(preceding-sibling::subst[1]/del)" /></xsl:attribute>
								<xsl:apply-templates
									select="preceding-sibling::subst[1]/add/@*" />
								<xsl:value-of
									select="preceding-sibling::subst[1]/add/w" />
							</xsl:element>
							<xsl:apply-templates />
						</w>
					</xsl:when>
					<xsl:otherwise>
						<w xmlns="http://www.tei-c.org/ns/1.0" ref="{$ref}"
							lang="{$lang}" nametype="{$nametype}" sic="{$sic}" orig="{$orig}"
							abbr="{$abbr}" lb="{$lb}">
							<xsl:apply-templates select="@*" />
							<xsl:element
								name="{local-name(preceding-sibling::*[1])}"
								namespace="http://www.tei-c.org/ns/1.0">
								<xsl:value-of select="preceding-sibling::*[1]//w" />
							</xsl:element>
							<xsl:apply-templates />
						</w>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when
				test="following-sibling::*[1][matches(@txm:wordPart,'^(end|middle)$')]">
				<xsl:variable name="wordPartPosition">
					<xsl:value-of
						select="following-sibling::*[1]/@txm:wordPart" />
				</xsl:variable>
				<xsl:choose>
					<xsl:when
						test="following-sibling::*[1][self::choice][child::reg]">
						<reg>
							<xsl:apply-templates
								select="following-sibling::choice[1]/reg/@*" />
							<w xmlns="http://www.tei-c.org/ns/1.0" ref="{$ref}"
								lang="{$lang}" nametype="{$nametype}" sic="{$sic}"
								abbr="{$abbr}" lb="{$lb}">
								<xsl:attribute name="orig"><xsl:value-of
									select="concat(.,following-sibling::choice[1]/orig[1]/w[1])" />
                  <xsl:if test="$wordPartPosition='middle'"><xsl:value-of
									select="following-sibling::w[1]" /></xsl:if>
                </xsl:attribute>
								<xsl:apply-templates select="@*" />
								<xsl:apply-templates />
								<xsl:value-of
									select="following-sibling::choice[1]/reg/w" />
								<xsl:if test="$wordPartPosition='middle'">
									<xsl:value-of select="following-sibling::w[1]" />
								</xsl:if>
							</w>
						</reg>
					</xsl:when>
					<xsl:when
						test="following-sibling::*[1][self::choice][child::corr]">
						<corr>
							<xsl:apply-templates
								select="following-sibling::choice[1]/corr/@*" />
							<w xmlns="http://www.tei-c.org/ns/1.0" ref="{$ref}"
								lang="{$lang}" nametype="{$nametype}" orig="{$orig}"
								abbr="{$abbr}" lb="{$lb}">
								<xsl:attribute name="sic"><xsl:value-of
									select="concat(.,following-sibling::choice[1]/sic[1]/w[1])" />
                  <xsl:if test="$wordPartPosition='middle'"><xsl:value-of
									select="following-sibling::w[1]" /></xsl:if>
                </xsl:attribute>
								<xsl:apply-templates select="@*" />
								<xsl:apply-templates />
								<xsl:value-of
									select="following-sibling::choice[1]/reg/w" />
								<xsl:if test="$wordPartPosition='middle'">
									<xsl:value-of select="following-sibling::w[1]" />
								</xsl:if>
							</w>
						</corr>
					</xsl:when>
					<xsl:when test="following-sibling::*[1][self::supplied]">
						<w xmlns="http://www.tei-c.org/ns/1.0" ref="{$ref}"
							lang="{$lang}" nametype="{$nametype}" orig="{$orig}"
							abbr="{$abbr}" lb="{$lb}">
							<xsl:attribute name="sic"><xsl:value-of
								select="." /></xsl:attribute>
							<xsl:apply-templates select="@*" />
							<xsl:apply-templates />
							<supplied xmlns="http://www.tei-c.org/ns/1.0">
								<xsl:value-of
									select="following-sibling::supplied[1]/w" />
							</supplied>
							<xsl:if test="$wordPartPosition='middle'">
								<xsl:value-of select="following-sibling::w[1]" />
							</xsl:if>
						</w>
					</xsl:when>
					<xsl:when
						test="following-sibling::*[1][self::subst][child::add]">
						<w xmlns="http://www.tei-c.org/ns/1.0" ref="{$ref}"
							lang="{$lang}" nametype="{$nametype}" orig="{$orig}"
							abbr="{$abbr}" lb="{$lb}"
							del="{normalize-space(following-sibling::subst[1]/del)}">
							<xsl:attribute name="del"><xsl:value-of
								select="concat('(',following-sibling::subst[1]/del[1]/w[1],')',.)" /></xsl:attribute>
							<xsl:apply-templates select="@*" />
							<xsl:apply-templates />
							<xsl:element name="add"
								namespace="http://www.tei-c.org/ns/1.0">
								<!-- <xsl:attribute name="rend">inWord</xsl:attribute> -->
								<xsl:attribute name="del"><xsl:value-of
									select="normalize-space(following-sibling::subst[1]/del)"></xsl:value-of></xsl:attribute>
								<xsl:apply-templates
									select="following-sibling::subst[1]/add/@*" />
								<xsl:value-of
									select="following-sibling::subst[1]/add/w" />
							</xsl:element>
							<xsl:if test="$wordPartPosition='middle'">
								<xsl:value-of select="following-sibling::w[1]" />
							</xsl:if>
						</w>
					</xsl:when>
					<xsl:otherwise>
						<w xmlns="http://www.tei-c.org/ns/1.0" ref="{$ref}"
							lang="{$lang}" nametype="{$nametype}" sic="{$sic}" orig="{$orig}"
							abbr="{$abbr}" lb="{$lb}">
							<xsl:apply-templates select="@*" />
							<xsl:apply-templates />
							<xsl:element
								name="{local-name(following-sibling::*[1])}"
								namespace="http://www.tei-c.org/ns/1.0">
								<xsl:value-of select="following-sibling::*[1]//w" />
							</xsl:element>
							<xsl:if test="$wordPartPosition='middle'">
								<xsl:value-of select="following-sibling::w[1]" />
							</xsl:if>
						</w>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<w xmlns="http://www.tei-c.org/ns/1.0" ref="{$ref}" lang="{$lang}"
					nametype="{$nametype}" sic="{$sic}" orig="{$orig}" abbr="{$abbr}"
					lb="{$lb}">
					<xsl:apply-templates select="@*" />
					<xsl:apply-templates
						select="*|processing-instruction()|comment()|text()" />
				</w>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="*[@txm:wordPart]">
		<xsl:choose>
			<xsl:when
				test="matches(local-name(.),'choice') and count(child::*[1]/tei:w) &gt; 1">
				<xsl:comment>
					Attention : problème de tokenisation (choice) :
					<xsl:value-of select="normalize-space(.)" />
				</xsl:comment>
				<xsl:for-each select="tei:corr|tei:reg|tei:expan">
					<xsl:element name="{name(.)}">
						<xsl:apply-templates
							select="descendant::tei:w[not(position()=1)]" />
					</xsl:element>
				</xsl:for-each>
			</xsl:when>
			<xsl:when
				test="matches(local-name(.),'subst') and count(child::*[1]/tei:w) &gt; 1">
				<xsl:comment>
					Attention : problème de tokenisation (subst) :
					<xsl:value-of select="normalize-space(.)" />
				</xsl:comment>
				<xsl:copy>
					<xsl:apply-templates
						select="@*[not(local-name()='wordPart')]" />
					<xsl:for-each select="tei:add|tei:del">
						<xsl:element name="{name(.)}">
							<xsl:apply-templates
								select="descendant::tei:w[not(position()=1)]" />
						</xsl:element>
					</xsl:for-each>
				</xsl:copy>
			</xsl:when>
			<xsl:when test="count(child::w) &gt; 1">
				<xsl:comment>
					Attention : problème de tokenisation :
					<xsl:value-of select="normalize-space(.)" />
				</xsl:comment>
				<xsl:copy>
					<xsl:apply-templates
						select="@*[not(local-name()='wordPart')]" />
					<xsl:apply-templates
						select="descendant::tei:w[not(position()=1)]" />
				</xsl:copy>
			</xsl:when>
			<xsl:otherwise>
				<xsl:comment>
					Mot coupé par une balise :
					<xsl:value-of select="normalize-space(.)" />
				</xsl:comment>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<xsl:template
		match="lb[@rend='hyphen' and preceding-sibling::*[1][self::w]]">
		<xsl:comment>
			lb déplacé à l'intérieur du mot précédent
		</xsl:comment>
	</xsl:template>

	<xsl:template
		match="lb[not(@rend='hyphen') and matches(preceding-sibling::tei:w[1],'\w+-\s*$')]">
		<xsl:comment>
			Attention : coupure de mot non marquée ! lb déplacé à l'intérieur du
			mot précédent
		</xsl:comment>
		<!--<note xmlns="http://www.tei-c.org/ns/1.0" type="auto">Attention : coupure 
			de mot non marquée !</note> -->
	</xsl:template>


	<xsl:template match="s">
		<xsl:apply-templates />
	</xsl:template>

</xsl:stylesheet>