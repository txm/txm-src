<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:txm="http://textometrie.org/1.0"
	exclude-result-prefixes="tei edate xd" version="2.0">

	<xd:doc type="stylesheet">
		<xd:short>
			Feuille de style de suppression de certains éléments XML à utiliser comme
			prétraitement de l'import XML/w+CSV.
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Serge Heiden slh@ens-lyon.fr</xd:author>
		<xd:copyright>2013, CNRS / ICAR (ICAR3 LinCoBaTO)</xd:copyright>
	</xd:doc>


	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="yes" />
		
		<xsl:param name="targetelement">//tei:u[@spkid='ENQ']</xsl:param>

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

    <xsl:template match="//tei:u[@spkid='ENQ']/tei:w">
        <xsl:copy-of select="txm:form/text()"/>
        <xsl:call-template name="spacing-xmltxm"/>
    </xsl:template>
    
    <xsl:template match="//tei:u[@spkid='ENQ']//text()">
        <xsl:copy-of select="normalize-space(.)"/>
    </xsl:template>

    <xsl:template name="spacing-xmltxm">
        <xsl:choose>
            <xsl:when test="ancestor::tei:w"/>
            <xsl:when test="following::tei:w[1][matches(descendant::txm:form[1],'^[.,)\]]+$')]"/>
            <xsl:when test="matches(descendant::txm:form[1],'^[(\[‘]+$|\w(''|’)$')"></xsl:when>
            <xsl:when test="position()=last() and (ancestor::tei:choice or ancestor::tei:supplied[not(@rend='multi_s')])"></xsl:when>
            <xsl:when test="following-sibling::*[1][self::tei:note]"></xsl:when>
            <xsl:when test="following::tei:w[1][matches(descendant::txm:form[1],'^[:;!?]+$')]">
                <xsl:text>&#xa0;</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text> </xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template> 

</xsl:stylesheet>
