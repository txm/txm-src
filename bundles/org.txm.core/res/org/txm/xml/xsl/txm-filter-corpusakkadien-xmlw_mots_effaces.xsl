<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	exclude-result-prefixes="tei edate xd" version="2.0">

	<xd:doc type="stylesheet">
		<xd:short>
			Cette feuille de style permet de préparer les transcriptions de tablettes
			d'argile (projet UMR PROCLAC) à l'import XML/W + CSV de TXM en créant
			les
			propriétés de mots "no-unclear" et "non-supplied" pour éliminer les formes
			non présentes ou peu lisibles dans les sources.
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev, alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2014-2015, CNRS / ICAR (équipe CACTUS)</xd:copyright>
	</xd:doc>


	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="yes" />

	<xsl:template match="@*|*">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="@*|*" mode="nosupplied">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="@*|*" mode="nounclear">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="*[local-name()='teiHeader']" />

	<xsl:template match="text()" mode="#all">
		<xsl:choose>
			<xsl:when test="ancestor::*[local-name()='w']">
				<xsl:value-of select="." />
			</xsl:when>
			<xsl:otherwise />
		</xsl:choose>
	</xsl:template>

	<!-- On supprime les mots qui sont illisibles dans la source et on ajoute 
		une propriété lexicale qui correspond à la lisibilité -->

	<xsl:template match="//*[local-name()='w']">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="no-supplied">
            <xsl:choose>
              <xsl:when
				test="ancestor::*[local-name()='supplied']">¤</xsl:when>
              <xsl:otherwise><xsl:apply-templates
				mode="nosupplied" /></xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
			<xsl:attribute name="no-unclear">
            <xsl:choose>
              <xsl:when
				test="ancestor::*[local-name()='supplied'] or ancestor::*[local-name()='unclear']">¤</xsl:when>
              <xsl:otherwise><xsl:apply-templates
				mode="nounclear" /></xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="*[local-name()='supplied']"
		mode="nosupplied" />

	<xsl:template match="*[local-name()='supplied']"
		mode="nounclear" />

	<xsl:template match="*[local-name()='unclear']"
		mode="nounclear" />

</xsl:stylesheet>
