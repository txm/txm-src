<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:txm="http://textometrie.org/ns/1.0"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	exclude-result-prefixes="tei edate"
	xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0">


	<xd:doc type="stylesheet">
		<xd:short>
			This stylesheet is designed for TXM XTZ+CSV import module to re-unite
			word tokens separated by lb, cb and pb tags bearing @break="no"
			attribute. This stylesheet should be used at "3-posttok" step.
			All the tags separating the beginning and the end of a word must have
			a @break='no' and their number must not exceed 3 in each case.
			See TXM User Manual for more details
			(http://textometrie.ens-lyon.fr/spip.php?rubrique64)
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2017, CNRS / UMR 5317 IHRIM (CACTUS research group)</xd:copyright>
	</xd:doc>


	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />


	<!-- General patterns: all elements, attributes, comments and processing 
		instructions are copied -->

	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates
				select="*|@*|processing-instruction()|comment()|text()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="*" mode="position">
		<xsl:value-of select="count(preceding-sibling::*)" />
	</xsl:template>

	<xsl:template
		match="@*|comment()|processing-instruction()">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="*:w">
		<xsl:choose>
			<xsl:when test="following-sibling::*[1][@break='no']">
				<xsl:copy>
					<xsl:apply-templates select="@*" />
					<xsl:apply-templates />
					<xsl:copy-of
						select="following-sibling::*[position() lt 4][@break='no']" />
					<xsl:apply-templates
						select="following-sibling::w[1]/node()" />
				</xsl:copy>
			</xsl:when>
			<xsl:when test="preceding-sibling::*[1][@break='no']">
				<!--Word ending agglutinated to the previous word -->
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@*" />
					<xsl:apply-templates />
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="*[@break='no']" />


</xsl:stylesheet>