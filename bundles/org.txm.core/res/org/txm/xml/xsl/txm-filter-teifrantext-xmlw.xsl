<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	exclude-result-prefixes="tei edate xd" version="2.0">

	<xsl:import href="txm-filter-teip5-xmlw-preserve.xsl" />

	<xd:doc type="stylesheet">
		<xd:short>
			Feuille de style de préparation des fichiers TEI de la Revue
			Discours à l'importation TXM dans un format xml simple.
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2012, CNRS / ICAR (ICAR3 LinCoBaTO)</xd:copyright>
	</xd:doc>

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />

	<!--<xsl:variable name="langue"><xsl:value-of select="lower-case(//tei:teiHeader[1]/tei:profileDesc[1]/tei:langUsage[1]/tei:language[1])"/></xsl:variable> -->

	<!-- On injecte les mots clés dans la langue de l'article et on ne garde 
		que l'abstract dans la langue de l'article -->

	<xsl:template match="text()">
		<xsl:analyze-string select="."
			regex="&amp;lt;|&amp;gt;|&amp;|&lt;|&gt;|--|\*(\w+)">
			<xsl:matching-substring>
				<xsl:choose>
					<xsl:when test="matches(.,'&amp;lt;')">
						<xsl:text>‹</xsl:text>
					</xsl:when>
					<xsl:when test="matches(.,'&amp;gt;')">
						<xsl:text>›</xsl:text>
					</xsl:when>
					<!-- <xsl:when test="matches(.,'&amp;')"> <!-\-<expan xmlns="http://www.tei-c.org/ns/1.0">and</expan>-\-> 
						<xsl:text>&amp;amp;</xsl:text> </xsl:when> -->
					<xsl:when test="matches(.,'&lt;')">
						<xsl:text>‹</xsl:text>
					</xsl:when>
					<xsl:when test="matches(.,'&gt;')">
						<xsl:text>›</xsl:text>
					</xsl:when>
					<!-- pose problème si se trouve dans les notes transformés en commentaires 
						xml par le tokeniseur -->
					<xsl:when test="matches(.,'--')">
						<xsl:text> - </xsl:text>
					</xsl:when>
					<!-- spécial Frantext!!! -->
					<xsl:when test="matches(.,'\*(\w+)')">
						<w xmlns="http://www.tei-c.org/ns/1.0" type="maj">
							<xsl:value-of select="regex-group(1)" />
						</w>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="." />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:matching-substring>
			<xsl:non-matching-substring>
				<xsl:value-of select="." />
			</xsl:non-matching-substring>
		</xsl:analyze-string>
	</xsl:template>

	<xsl:template match="tei:text">
		<xsl:copy>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:div">
		<xsl:copy>
			<xsl:attribute name="n"><xsl:value-of
				select="count(preceding-sibling::tei:div) + 1" /></xsl:attribute>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>