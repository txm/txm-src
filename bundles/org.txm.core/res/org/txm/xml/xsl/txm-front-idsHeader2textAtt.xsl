<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc" version="2.0">

	<xd:doc type="stylesheet">
		<xd:short>
			This stylesheet is designed for TXM XTZ+CSV import module to extract
			metadata from the idsHeader (IDS-XCES) and add them as attributes
			of the text element. It should be used at "2-front" step. See TXM
			User Manual for more details
			(http://textometrie.ens-lyon.fr/spip.php?rubrique64)
			For more details on source corpus encoding schema see
			http://www1.ids-mannheim.de/kl/projekte/korpora/textmodell.html
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2017, CNRS / UMR 5317 IHRIM (CACTUS research group)</xd:copyright>
	</xd:doc>



	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />


	<!-- General patterns: all elements, attributes, comments and processing 
		instructions are copied -->

	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates
				select="*|@*|processing-instruction()|comment()|text()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template
		match="@*|comment()|processing-instruction()">
		<xsl:copy />
	</xsl:template>

	<xsl:variable name="current-file-name">
		<xsl:analyze-string select="document-uri(.)"
			regex="^(.*)/([^/]+)\.[^/]+$">
			<xsl:matching-substring>
				<xsl:value-of select="regex-group(2)" />
			</xsl:matching-substring>
		</xsl:analyze-string>
	</xsl:variable>

	<!-- Extracting metadata to use -->

	<xsl:variable name="title">
		<xsl:value-of
			select="/idsText/idsHeader//h.title[@type='abbr']" />
	</xsl:variable>
	<xsl:variable name="date">
		<xsl:value-of select="idsText/idsHeader//creatDate" />
	</xsl:variable>


	<!-- Injecting metadata into attibutes of the text element -->

	<xsl:template match="text">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="title"><xsl:value-of
				select="$title" /></xsl:attribute>
			<xsl:attribute name="date"><xsl:value-of
				select="$date" /></xsl:attribute>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>


</xsl:stylesheet>