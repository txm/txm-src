<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:txm="http://textometrie.org/1.0" exclude-result-prefixes="#all"
	version="2.0">

	<xsl:output method="xml" encoding="UTF-8"
		omit-xml-declaration="no" indent="no"
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />

	<xsl:strip-space elements="*" />

	<xsl:param name="editionpagetag">
		pb
	</xsl:param>

	<xsl:param name="textcontainer">
		text
	</xsl:param>

	<!--<xsl:param name="frontmatter"></xsl:param> <xsl:param name="backmatter">back</xsl:param> -->

	<xsl:variable name="inputtype">
		<xsl:choose>
			<xsl:when test="//tei:w//txm:form">
				xmltxm
			</xsl:when>
			<xsl:otherwise>
				xmlw
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:template match="/">
		<html>
			<head>
				<title>
					<xsl:value-of select="//tei:text[1]/@id" />
				</title>
				<meta http-equiv="Content-Type"
					content="text/html;charset=UTF-8" />
			</head>
			<body>
				<a class="txm-page" title="1" />
				<div class="metadata-page">
					<h1>
						<xsl:value-of select="//tei:text[1]/@id"></xsl:value-of>
					</h1>
					<br />
					<table>
						<xsl:for-each select="//tei:text[1]/@*">
							<tr>
								<td>
									<xsl:value-of select="name()" />
								</td>
								<td>
									<xsl:value-of select="." />
								</td>
							</tr>
						</xsl:for-each>
					</table>
					<br />
					<br />
				</div>
				<xsl:apply-templates
					select="descendant::*[local-name()=$textcontainer]" />


				<!-- <xsl:if test="//tei:note[not(@place='inline') and not(matches(@type,'intern|auto')) 
					and not(following::*[local-name()=$editionpagetag]) and preceding::*[local-name()=$editionpagetag]]"> 
					<br/> <br/> <span class="footnotes"> <xsl:for-each select="//tei:note[not(@place='inline') 
					and not(matches(@type,'intern|auto')) and not(following::*[local-name()=$editionpagetag]) 
					and preceding::*[local-name()=$editionpagetag]]"> <xsl:variable name="note_count"> 
					<xsl:value-of select="count(preceding::tei:note[not(@place='inline') and 
					not(matches(@type,'intern|auto'))]) + 1"/> </xsl:variable> <!-\-<p><xsl:value-of 
					select="$note_count"/>. <a href="#noteref_{$note_count}" name="note_{$note_count}">[<xsl:value-of 
					select="preceding::tei:cb[1]/@xml:id"/>, l. <xsl:value-of select="preceding::tei:lb[1]/@n"/>]</a><xsl:text> 
					</xsl:text> <xsl:value-of select="."/></p>-\-> <span class="note"> <span 
					class="footnotenumber"><a href="#noteref_{$note_count}" name="note_{$note_count}"><xsl:value-of 
					select="$note_count"/></a>. </span> <xsl:apply-templates mode="#current"/> 
					</span> </xsl:for-each> </span> </xsl:if> -->
			</body>
		</html>
	</xsl:template>

	<xsl:template match="//*[local-name()=$textcontainer]">
		<xsl:variable name="pbcount"></xsl:variable>

		<div class="text" style="margin-left:60px;">
			<xsl:apply-templates />
		</div>
	</xsl:template>



	<xsl:template match="*">
		<xsl:choose>
			<xsl:when test="descendant::tei:p|descendant::tei:ab">
				<div>
					<xsl:call-template name="addClass" />
					<xsl:apply-templates />
				</div>
				<xsl:text>&#xa;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<span>
					<xsl:call-template name="addClass" />
					<xsl:if test="self::tei:add[@del]">
						<xsl:attribute name="title"><xsl:value-of
							select="@del" /></xsl:attribute>
					</xsl:if>
					<xsl:apply-templates />
				</span>
				<!--<xsl:call-template name="spacing"/> -->
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template
		match="@*|processing-instruction()|comment()">
		<!--<xsl:copy/> -->
	</xsl:template>

	<!-- <xsl:template match="comment()"> <xsl:copy/> </xsl:template> -->
	<xsl:template match="text()">
		<xsl:value-of select="normalize-space(.)" />
	</xsl:template>

	<xsl:template name="addClass">
		<xsl:attribute name="class">
			<xsl:value-of select="local-name(.)" />
			<xsl:if test="@type"><xsl:value-of
			select="concat('-',@type)" /></xsl:if>
			<xsl:if test="@subtype"><xsl:value-of
			select="concat('-',@subtype)" /></xsl:if>
			<xsl:if test="@rend"><xsl:value-of
			select="concat('-',@rend)" /></xsl:if>
		</xsl:attribute>
	</xsl:template>

	<!-- <xsl:template match="tei:p|tei:ab"> <p> <xsl:call-template name="addClass"/> 
		<xsl:apply-templates/> </p> <xsl:text>&#xa;</xsl:text> </xsl:template> -->

	<xsl:template match="//word">
		<xsl:choose>
			<xsl:when test="@line = preceding::word[1]/@line">
				<xsl:text> </xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<br />
				<xsl:text>&#xa;</xsl:text>
				<span style="position:relative">
					<span class="linenumber"
						style="position:absolute;width:40px;left:-50px;padding-right:20px;text-align:right">
						<xsl:value-of select="@line" />
						&#xa0;
					</span>
				</span>
			</xsl:otherwise>
		</xsl:choose>
		<span>
			<xsl:call-template name="addClass" />
			<xsl:apply-templates />
		</span>
	</xsl:template>



	<!-- Page breaks -->
	<xsl:template match="//*[local-name()=$editionpagetag]">
		<xsl:variable name="editionpagetype">
			<xsl:choose>
				<xsl:when test="ancestor::tei:ab">
					editionpageverse
				</xsl:when>
				<xsl:otherwise>
					editionpage
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="pagenumber">
			<xsl:choose>
				<xsl:when test="@n">
					<xsl:value-of select="@n" />
				</xsl:when>
				<xsl:when test="@facs">
					<xsl:value-of select="substring-before(@facs,'.')" />
				</xsl:when>
				<xsl:otherwise>
					[NN]
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="page_id">
			<xsl:value-of
				select="count(preceding::*[local-name()=$editionpagetag])" />
		</xsl:variable>
		<xsl:if test="@break='no'">
			<span class="hyphen">-</span>
		</xsl:if>
		<xsl:if
			test="//tei:note[not(@place='inline') and not(matches(@type,'intern|auto'))][following::*[local-name()=$editionpagetag][1][count(preceding::*[local-name()=$editionpagetag]) = $page_id]]">
			<xsl:text>&#xa;</xsl:text>
			<br />
			<br />
			<span class="footnotes">
				<xsl:for-each
					select="//tei:note[not(@place='inline') and not(matches(@type,'intern|auto'))][following::*[local-name()=$editionpagetag][1][count(preceding::*[local-name()=$editionpagetag]) = $page_id]]">
					<xsl:variable name="note_count">
						<xsl:value-of
							select="count(preceding::tei:note[not(@place='inline')]) + 1" />
					</xsl:variable>
					<!--<p><xsl:value-of select="$note_count"/>. <a href="#noteref_{$note_count}" 
						name="note_{$note_count}">[<xsl:value-of select="preceding::tei:cb[1]/@xml:id"/>, 
						l. <xsl:value-of select="preceding::tei:lb[1]/@n"/>]</a><xsl:text> </xsl:text> 
						<xsl:value-of select="."/></p> -->
					<span class="note">
						<span class="footnotenumber">
							<a href="#noteref_{$note_count}" name="note_{$note_count}">
								<xsl:value-of select="$note_count" />
							</a>
							.
						</span>
						<xsl:apply-templates mode="#current" />
					</span>
				</xsl:for-each>
			</span>
			<xsl:text>&#xa;</xsl:text>

		</xsl:if>

		<xsl:text>&#xa;</xsl:text>
		<br />
		<xsl:text>&#xa;</xsl:text>
		<a class="txm-page"
			title="{count(preceding::*[local-name()=$editionpagetag]) + 2}" />
		<span class="{$editionpagetype}">
			-
			<xsl:value-of select="$pagenumber" />
			-
		</span>
		<br />
		<xsl:text>&#xa;</xsl:text>
	</xsl:template>

	<!-- Notes -->
	<!-- <xsl:template match="tei:note[not(@place='inline') and not(matches(@type,'intern|auto'))]"> 
		<!-\-<span style="color:violet"> [<b>Note :</b> <xsl:apply-templates/>] </span>-\-> 
		<xsl:variable name="note_count"><xsl:value-of select="count(preceding::tei:note[not(@place='inline') 
		and not(matches(@type,'intern|auto'))]) + 1"/></xsl:variable> <a title="{.}" 
		class="noteref" href="#note_{$note_count}" name="noteref_{$note_count}">[<xsl:value-of 
		select="$note_count"/>]</a> <xsl:call-template name="spacing"/> </xsl:template> -->

	<xsl:template match="//w">
		<span class="w" style="font-family:Santakku">
			<xsl:variable name="unicode-chars" as="element()">
				<unicode>
					<xsl:choose>
						<xsl:when test="contains(@ref-unicode,'&amp;')">
							<char>
								<xsl:call-template name="hex2num">
									<xsl:with-param name="hex">
										<xsl:value-of
											select="substring-before(substring(@ref-unicode,4),'&amp;')" />
									</xsl:with-param>
								</xsl:call-template>
							</char>
							<char>
								<xsl:call-template name="hex2num">
									<xsl:with-param name="hex">
										<xsl:value-of
											select="substring-after(@ref-unicode,'&amp;U+')" />
									</xsl:with-param>
								</xsl:call-template>
							</char>
						</xsl:when>
						<xsl:otherwise>
							<char>
								<xsl:call-template name="hex2num">
									<xsl:with-param name="hex">
										<xsl:value-of select="substring(@ref-unicode,4)" />
									</xsl:with-param>
								</xsl:call-template>
							</char>
						</xsl:otherwise>
					</xsl:choose>
				</unicode>
			</xsl:variable>
			<xsl:if test="@id">
				<xsl:attribute name="id"><xsl:value-of
					select="@id" /></xsl:attribute>
			</xsl:if>
			<xsl:attribute name="title">
			<xsl:choose>
				<xsl:when test="descendant::txm:ana">
					<xsl:for-each select="descendant::txm:ana">
						<xsl:value-of
				select="concat(substring(@type,2),': ',.,' ; ')" />				
					</xsl:for-each>
					<xsl:value-of
				select="concat('translit : ',descendant::txm:form)" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:for-each select="@*">
						<xsl:value-of select="concat(name(),': ',.,' ; ')" />				
					</xsl:for-each>
					<xsl:value-of select="concat('translit : ',.)" />
				</xsl:otherwise>
			</xsl:choose>

		</xsl:attribute>
			<xsl:choose>
				<xsl:when test="descendant::txm:form">
					<xsl:apply-templates select="txm:form" />
				</xsl:when>
				<xsl:otherwise>
					<!--<xsl:value-of select="codepoints-to-string($unicode)"/> -->
					<xsl:for-each select="$unicode-chars/char">
						<xsl:value-of select="codepoints-to-string(.)" />
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>

		</span>
		<!--<xsl:if test="following-sibling::tei:w"></xsl:if> -->
	</xsl:template>

	<xsl:template name="hex2num">
		<xsl:param name="hex" />
		<xsl:param name="num" select="0" />
		<xsl:param name="MSB"
			select="translate(substring($hex, 1, 1), 'abcdef', 'ABCDEF')" />
		<xsl:param name="value"
			select="string-length(substring-before('0123456789ABCDEF', $MSB))" />
		<xsl:param name="result" select="16 * $num + $value" />
		<xsl:choose>
			<xsl:when test="string-length($hex) > 1">
				<xsl:call-template name="hex2num">
					<xsl:with-param name="hex"
						select="substring($hex, 2)" />
					<xsl:with-param name="num" select="$result" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$result" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<xsl:template match="//txm:form">
		<xsl:variable name="unicode-chars" as="element()">
			<unicode>
				<xsl:choose>
					<xsl:when
						test="contains(following-sibling::txm:ana[@type='#ref-unicode'],'&amp;')">
						<xsl:for-each
							select="tokenize(substring(following-sibling::txm:ana[@type='#ref-unicode'],2),'&amp;')">
							<char>
								<xsl:call-template name="hex2num">
									<xsl:with-param name="hex">
										<xsl:value-of select="substring-after(.,'U+')" />
									</xsl:with-param>
								</xsl:call-template>
							</char>
						</xsl:for-each>
						<!-- <char><xsl:call-template name="hex2num"><xsl:with-param name="hex"><xsl:value-of 
							select="substring-before(substring(following-sibling::txm:ana[@type='#ref-unicode'],4),'&amp;')"/></xsl:with-param></xsl:call-template></char> 
							<char><xsl:call-template name="hex2num"><xsl:with-param name="hex"><xsl:value-of 
							select="substring-after(following-sibling::txm:ana[@type='#ref-unicode'],'&amp;U+')"/></xsl:with-param></xsl:call-template></char> -->
					</xsl:when>
					<xsl:otherwise>
						<char>
							<xsl:call-template name="hex2num">
								<xsl:with-param name="hex">
									<xsl:value-of
										select="substring(following-sibling::txm:ana[@type='#ref-unicode'],4)" />
								</xsl:with-param>
							</xsl:call-template>
						</char>
					</xsl:otherwise>
				</xsl:choose>
			</unicode>
		</xsl:variable>
		<!--<xsl:variable name="unicode"> <xsl:call-template name="hex2num"><xsl:with-param 
			name="hex"><xsl:value-of select="ancestor::w/substring(child::txm:ana[@type='#ref-unicode'],4)"/></xsl:with-param></xsl:call-template> 
			</xsl:variable> -->

		<!--<xsl:value-of select="codepoints-to-string($unicode)"/> -->
		<xsl:for-each select="$unicode-chars/char">
			<xsl:choose>
				<!--<xsl:when test="number(.) = 74034">xxx</xsl:when> -->
				<xsl:when test="number(.) gt 50">
					<xsl:value-of select="codepoints-to-string(.)" /><!--[<xsl:value-of 
						select="."/>] -->
				</xsl:when>
				<xsl:otherwise>
					[?]
				</xsl:otherwise>
			</xsl:choose>

			<!--<xsl:value-of select="."/> -->
		</xsl:for-each>
	</xsl:template>

	<!-- <xsl:template name="spacing"> <xsl:choose> <xsl:when test="$inputtype='xmltxm'"> 
		<xsl:call-template name="spacing-xmltxm"/> </xsl:when> <xsl:otherwise> <xsl:call-template 
		name="spacing-xmlw"/> </xsl:otherwise> </xsl:choose> </xsl:template> <xsl:template 
		name="spacing-xmlw"> <xsl:choose> <xsl:when test="ancestor::tei:w"/> <xsl:when 
		test="following::tei:w[1][matches(.,'^\s*[.,)\]]+\s*$')]"/> <xsl:when test="matches(.,'^\s*[(\[]+$|\w(''|’)\s*$')"></xsl:when> 
		<xsl:when test="position()=last() and (ancestor::tei:choice or ancestor::tei:supplied[not(@rend='multi_s')])"></xsl:when> 
		<xsl:when test="following-sibling::*[1][self::tei:note]"></xsl:when> <xsl:when 
		test="following::tei:w[1][matches(.,'^\s*[:;!?]+\s*$')]"> <xsl:text>&#xa0;</xsl:text> 
		</xsl:when> <xsl:otherwise> <xsl:text> </xsl:text> </xsl:otherwise> </xsl:choose> 
		</xsl:template> <xsl:template name="spacing-xmltxm"> <xsl:choose> <xsl:when 
		test="ancestor::tei:w"/> <xsl:when test="following::tei:w[1][matches(descendant::txm:form[1],'^[.,)\]]+$')]"/> 
		<xsl:when test="matches(descendant::txm:form[1],'^[(\[]+$|\w(''|’)$')"></xsl:when> 
		<xsl:when test="position()=last() and (ancestor::tei:choice or ancestor::tei:supplied[not(@rend='multi_s')])"></xsl:when> 
		<xsl:when test="following-sibling::*[1][self::tei:note]"></xsl:when> <xsl:when 
		test="following::tei:w[1][matches(descendant::txm:form[1],'^[:;!?]+$')]"> 
		<xsl:text>&#xa0;</xsl:text> </xsl:when> <xsl:otherwise> <xsl:text> </xsl:text> 
		</xsl:otherwise> </xsl:choose> </xsl:template> -->


</xsl:stylesheet>
