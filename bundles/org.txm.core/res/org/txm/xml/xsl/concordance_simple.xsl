﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
	xmlns:exsl="http://exslt.org/common">
	<!-- Revised 8 Feb 2012 by TMR -->

	<!-- Delimiter is tab. written as &#09; -->

	<xsl:output method="text" encoding="UTF-8" indent="no" />

	<xsl:param name="cx" select="15" />
	<!-- vary this value to change the size of the context -->

	<xsl:param name="ntTypes"></xsl:param>
	<!-- non-terminal pivot features to show -->

	<xsl:param name="tTypes">
		pos form
	</xsl:param>
	<!-- terminal pivot features to show -->

	<xsl:key name="idkey" match="t|nt" use="@id" />
	<!-- Lezius' lookup key -->

	<xsl:template match="/">

		<xsl:variable name="valueshownt">
			<xsl:value-of select="$ntTypes" />
		</xsl:variable>

		<xsl:variable name="valueshowt">
			<xsl:value-of select="$tTypes" />
		</xsl:variable>

		<!--Write a header -->

		<xsl:text>ID&#x09;LeftCxOutsideSnt&#x09;LeftCxInsideSnt&#x09;Pivot&#x09;</xsl:text>

		<xsl:for-each
			select="tokenize($valueshownt/node(), '\s+')">

			<xsl:value-of select="." />
			<xsl:text>&#x09;</xsl:text>

		</xsl:for-each>

		<xsl:for-each
			select="tokenize($valueshowt/node(), '\s+')">

			<xsl:value-of select="." />
			<xsl:text>&#x09;</xsl:text>

		</xsl:for-each>

		<xsl:text>RightCxInsideSnt&#x09;RightCxOutsideSnt&#x0D;&#x0A;</xsl:text>

		<xsl:for-each
			select="descendant::variable[@name='#pivot']">

			<!-- Each #pivot node in the document is selected -->

			<xsl:variable name="inpivot">

				<pivot>
					<xsl:apply-templates
						select="key('idkey', @idref)" mode="get-terminals" />
				</pivot>

			</xsl:variable>

			<xsl:variable name="inpivot-sort">
				<pivot>
					<xsl:for-each select="exsl:node-set($inpivot)//t">
						<xsl:sort select="@id" />
						<xsl:copy-of select="." />
					</xsl:for-each>
				</pivot>
			</xsl:variable>

			<xsl:variable name="firstpivotid">

				<xsl:apply-templates
					select="exsl:node-set($inpivot-sort)//t[1]" mode="id" />

			</xsl:variable>

			<xsl:variable name="lastpivotid">

				<xsl:apply-templates
					select="exsl:node-set($inpivot-sort)//t[last()]" mode="id" />

			</xsl:variable>

			<!-- sentence ID -->

			<xsl:apply-templates select="ancestor::s"
				mode="id" />
			<xsl:text>&#x09;</xsl:text>

			<!-- left context -->

			<xsl:apply-templates
				select="key('idkey', $firstpivotid)" mode="left-cx" />
			<xsl:text>&#x09;</xsl:text>

			<!-- pivot text -->

			<xsl:apply-templates select="ancestor::s"
				mode="pivot">
				<xsl:with-param name="inpivot"
					select="exsl:node-set($inpivot-sort)" />
			</xsl:apply-templates>
			<xsl:text>&#x09;</xsl:text>

			<!-- pivot properties -->

			<xsl:variable name="thispivot"
				select="key('idkey', @idref)" />

			<xsl:for-each
				select="tokenize($valueshownt/node(), '\s+')">

				<xsl:variable name="attr" select="." />

				<xsl:value-of select="$thispivot/@*[name()=$attr]" />
				<xsl:text>&#x09;</xsl:text>

			</xsl:for-each>
			<xsl:for-each
				select="tokenize($valueshowt/node(), '\s+')">

				<xsl:variable name="attr" select="." />

				<xsl:value-of select="$thispivot/@*[name()=$attr]" />
				<xsl:text>&#x09;</xsl:text>

			</xsl:for-each>

			<!-- right context -->
			<xsl:apply-templates
				select="key('idkey', $lastpivotid)" mode="right-cx" />
			<xsl:text>&#x0D;&#x0A;</xsl:text>

		</xsl:for-each>

	</xsl:template>

	<xsl:template match="t" mode="get-terminals">

		<xsl:copy-of select="." />

	</xsl:template>

	<xsl:template match="t" mode="left-cx">

		<xsl:variable name="pivot-node" select="." />
		<xsl:variable name="this-sid">
			<xsl:value-of select="ancestor::s/@id" />
		</xsl:variable>

		<xsl:for-each
			select="preceding::t[position() &lt; number($cx)]">
			<!-- xsl:sort select="@id" / -->
			<xsl:choose>
				<!-- switch from out-of-sentence to in-sentence context -->
				<xsl:when
					test="ancestor::s[@id = $this-sid] and not(preceding-sibling::t)">
					<xsl:text>&#x09;</xsl:text>
				</xsl:when>

				<!-- first word of the context and in the same sentence -->

				<xsl:when
					test="position() = 1 and ancestor::s[@id=$this-sid]">
					<xsl:text>&#x09;</xsl:text>
				</xsl:when>

				<!-- first word of the context -->

				<xsl:when test="position() = 1" />

				<!-- first word in a sentence -->
				<xsl:when test="not(preceding-sibling::t)">
					<xsl:text> / </xsl:text>
				</xsl:when>

				<xsl:otherwise>
					<xsl:text> </xsl:text>
				</xsl:otherwise>

			</xsl:choose>

			<xsl:apply-templates select="." mode="output" />

			<!-- add blank column if there's no in-sentence context -->
			<xsl:if
				test="position() = last() and ancestor::s[@id != $this-sid]">
				<xsl:text>&#x09;</xsl:text>
			</xsl:if>

		</xsl:for-each>

	</xsl:template>

	<xsl:template match="t" mode="right-cx">

		<xsl:variable name="pivot-node" select="." />
		<xsl:variable name="this-sid">
			<xsl:value-of select="ancestor::s/@id" />
		</xsl:variable>

		<xsl:for-each
			select="following::t[position() &lt; number($cx)]">
			<!-- xsl:sort select="@id" / -->
			<xsl:choose>
				<!-- switch from in-sentence to out-of-sentence context -->
				<xsl:when
					test="ancestor::s[@id != $this-sid] and preceding::t[position()=1]/ancestor::s[@id = $this-sid]">
					<xsl:text>&#x09;</xsl:text>
				</xsl:when>

				<!-- first word of the context -->

				<xsl:when test="position() = 1" />

				<!-- first word in a sentence -->
				<xsl:when test="not(preceding-sibling::t)">
					<xsl:text> / </xsl:text>
				</xsl:when>

				<xsl:otherwise>
					<xsl:text> </xsl:text>
				</xsl:otherwise>

			</xsl:choose>
			<xsl:apply-templates select="." mode="output" />
			<!-- add blank column if there's no out-of-sentence context -->
			<xsl:if
				test="position() = last() and ancestor::s[@id = $this-sid]">
				<xsl:text>&#x09;</xsl:text>
			</xsl:if>

		</xsl:for-each>

	</xsl:template>

	<xsl:template match="s" mode="pivot">
		<xsl:param name="inpivot" />

		<xsl:variable name="firstpivotid">

			<xsl:apply-templates select="$inpivot//t[1]"
				mode="id" />

		</xsl:variable>

		<xsl:variable name="lastpivotid">
			<xsl:apply-templates select="$inpivot//t[last()]"
				mode="id" />

		</xsl:variable>

		<xsl:for-each select="descendant::t">
			<xsl:variable name="thisid" select="@id" />
			<xsl:choose>
				<xsl:when test="following-sibling::t[@id=$firstpivotid]" />
				<xsl:when test="preceding-sibling::t[@id=$lastpivotid]" />
				<xsl:when test="$inpivot//t[@id=$thisid]">
					<xsl:apply-templates select="." mode="output" />
					<xsl:text> </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					[
					<xsl:apply-templates select="." mode="output" />
					]
					<xsl:text> </xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>

	</xsl:template>

	<xsl:template match="t" mode="output">

		<xsl:value-of select="@word" />

	</xsl:template>

	<xsl:template match="nt|t|s" mode="id">

		<xsl:value-of select="@id" />

	</xsl:template>

	<xsl:template match="nt" mode="get-terminals">

		<xsl:for-each select="edge">

			<xsl:apply-templates
				select="key('idkey', @idref)" mode="get-terminals" />

		</xsl:for-each>

	</xsl:template>

</xsl:stylesheet>

