﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
	xmlns:exsl="http://exslt.org/common">

	<xsl:output method="text" encoding="UTF-8" indent="no" />

	<xsl:strip-space elements="*" />

	<xsl:param name="cx" select="30" />
	<!-- vary this value to change the size of the context -->

	<xsl:param name="ntTypes">
		cat
	</xsl:param>
	<!-- non-terminal pivot features to show -->

	<xsl:param name="tTypes">
		pos
	</xsl:param>
	<!-- terminal pivot features to show -->

	<xsl:variable name="valueshownt">
		<xsl:value-of select="$ntTypes" />
	</xsl:variable>

	<xsl:variable name="valueshowt">
		<xsl:value-of select="$tTypes" />
	</xsl:variable>

	<xsl:key name="idkey" match="t|nt" use="@id" />
	<!-- Lezius' lookup key -->

	<xsl:key name="idkey-reverse" match="edge" use="@idref" />
	<!-- reverse lookup key -->

	<xsl:template match="/">

		<!--Write a header -->

		<xsl:text>ID&#x09;LeftCxOutsideSnt&#x09;LeftCxInsideSnt&#x09;Pivot&#x09;</xsl:text>

		<xsl:for-each
			select="tokenize($valueshowt/node(), '\s+')">

			<xsl:value-of select="." />
			<xsl:text>&#x09;</xsl:text>

		</xsl:for-each>

		<xsl:text>PivotStr&#x09;</xsl:text>

		<xsl:for-each
			select="tokenize($valueshownt/node(), '\s+')">

			<xsl:value-of select="." />
			<xsl:text>&#x09;</xsl:text>

		</xsl:for-each>

		<xsl:text>RightCxInsideSnt&#x09;RightCxOutsideSnt&#x0D;&#x0A;</xsl:text>

		<xsl:for-each select=".//match">

			<!-- only treat match nodes -->

			<!-- first column: print sentence ID -->

			<xsl:value-of select="ancestor::s/@id" />

			<!-- next, verify that pivot is a terminal node -->

			<xsl:variable name="pivot-var"
				select="./variable[@name='#pivot']" />

			<xsl:variable name="pivot-node"
				select="key('idkey', exsl:node-set($pivot-var)/@idref)" />

			<xsl:choose>

				<xsl:when test="exsl:node-set($pivot-node)[self::t]">

					<xsl:apply-templates
						select="exsl:node-set($pivot-node)" mode="write-table" />

				</xsl:when>

				<xsl:otherwise>
					<xsl:text>&#x09;Query error: #pivot is not a terminal node</xsl:text>
				</xsl:otherwise>

			</xsl:choose>

			<xsl:text>&#x0d;&#x0a;</xsl:text>

		</xsl:for-each>

	</xsl:template>

	<xsl:template match="t" mode="write-table">

		<!-- Context node is a t-node pivot -->

		<!-- First stage: produce left context -->

		<xsl:text>&#x09;</xsl:text>
		<xsl:apply-templates select="." mode="left-cx" />

		<!-- Next stage, write the word in the pivot -->

		<xsl:text>&#x09;</xsl:text>
		<xsl:apply-templates select="." mode="output" />

		<!-- Next stage, write other required terminal attributes -->

		<xsl:variable name="thispivot" select="." />

		<xsl:text>&#x09;</xsl:text>
		<xsl:for-each
			select="tokenize($valueshowt/node(), '\s+')">

			<xsl:variable name="attr" select="." />

			<xsl:value-of select="$thispivot/@*[name()=$attr]" />
			<xsl:text>&#x09;</xsl:text>

		</xsl:for-each>

		<!-- Next stage, write the lexical string contained in the parent NT node -->

		<xsl:apply-templates
			select="key('idkey-reverse', @id)/ancestor::nt" mode="output" />

		<!-- Next stage, write the required <nt /> properties -->

		<xsl:variable name="pivotnt"
			select="key('idkey-reverse', @id)/.." />

		<xsl:text>&#x09;</xsl:text>
		<xsl:for-each
			select="tokenize($valueshownt/node(), '\s+')">

			<xsl:variable name="attr" select="." />

			<xsl:value-of select="$pivotnt/@*[name()=$attr]" />
			<xsl:text>&#x09;</xsl:text>

		</xsl:for-each>

		<!-- Next stage, write the right-cx -->

		<xsl:apply-templates select="." mode="right-cx" />

	</xsl:template>

	<xsl:template match="t" mode="left-cx">

		<xsl:variable name="pivot-node" select="." />
		<xsl:variable name="this-sid">
			<xsl:value-of select="ancestor::s/@id" />
		</xsl:variable>

		<xsl:for-each
			select="preceding::t[position() &lt; number($cx)]">
			<!-- xsl:sort select="@id" / -->
			<xsl:choose>
				<!-- switch from out-of-sentence to in-sentence context -->
				<xsl:when
					test="ancestor::s[@id = $this-sid] and not(preceding-sibling::t)">
					<xsl:text>&#x09;</xsl:text>
				</xsl:when>

				<!-- first word of the context and in the same sentence -->

				<xsl:when
					test="position() = 1 and ancestor::s[@id=$this-sid]">
					<xsl:text>&#x09;</xsl:text>
				</xsl:when>

				<!-- first word of the context -->

				<xsl:when test="position() = 1" />

				<!-- first word in a sentence -->
				<xsl:when test="not(preceding-sibling::t)">
					<xsl:text> / </xsl:text>
				</xsl:when>

				<xsl:otherwise>
					<xsl:text> </xsl:text>
				</xsl:otherwise>

			</xsl:choose>

			<xsl:apply-templates select="." mode="output" />

			<!-- add blank column if there's no in-sentence context -->
			<xsl:if
				test="position() = last() and ancestor::s[@id != $this-sid]">
				<xsl:text>&#x09;</xsl:text>
			</xsl:if>

		</xsl:for-each>

	</xsl:template>

	<xsl:template match="t" mode="right-cx">

		<xsl:variable name="pivot-node" select="." />
		<xsl:variable name="this-sid">
			<xsl:value-of select="ancestor::s/@id" />
		</xsl:variable>

		<xsl:for-each
			select="following::t[position() &lt; number($cx)]">
			<!-- xsl:sort select="@id" / -->
			<xsl:choose>
				<!-- switch from in-sentence to out-of-sentence context -->
				<xsl:when
					test="ancestor::s[@id != $this-sid] and preceding::t[position()=1]/ancestor::s[@id = $this-sid]">
					<xsl:text>&#x09;</xsl:text>
				</xsl:when>

				<!-- first word of the context -->

				<xsl:when test="position() = 1" />

				<!-- first word in a sentence -->
				<xsl:when test="not(preceding-sibling::t)">
					<xsl:text> / </xsl:text>
				</xsl:when>

				<xsl:otherwise>
					<xsl:text> </xsl:text>
				</xsl:otherwise>

			</xsl:choose>
			<xsl:apply-templates select="." mode="output" />
			<!-- add blank column if there's no out-of-sentence context -->
			<xsl:if
				test="position() = last() and ancestor::s[@id = $this-sid]">
				<xsl:text>&#x09;</xsl:text>
			</xsl:if>

		</xsl:for-each>

	</xsl:template>

	<xsl:template match="nt|t|s" mode="id">

		<xsl:value-of select="@id" />

	</xsl:template>

	<xsl:template match="variable" mode="name">

		<xsl:value-of select="@name" />

	</xsl:template>

	<xsl:template match="t" mode="output">

		<xsl:value-of select="@word" />

	</xsl:template>



	<xsl:template match="nt" mode="output">

		<!-- prints lexical content of nt node in correct order with constituents 
			not in the pivot bracketed -->

		<!-- stuff a copy of all the tnodes in the pivot in $in-pivot -->
		<xsl:variable name='in-pivot'>
			<xsl:apply-templates select="."
				mode="get-terminals" />
		</xsl:variable>

		<!-- create a copy of terminals, with an attribute "in-pivot='True'" -->

		<xsl:variable name="new-terminals">

			<xsl:for-each select="ancestor::s//t">

				<xsl:variable name="current-id">
					<xsl:value-of select="@id" />
				</xsl:variable>

				<!-- copy element into variable -->

				<xsl:copy>

					<xsl:if
						test="exsl:node-set($in-pivot)//t[@id = $current-id]">
						<xsl:attribute name="in-pivot">True</xsl:attribute>
					</xsl:if>
					<xsl:apply-templates select="@*" mode="copy" />

				</xsl:copy>

			</xsl:for-each>

		</xsl:variable>

		<!-- $new-terminals is in the correct order, so we can calculate the limits 
			of the segment -->

		<xsl:variable name="first-pivot-id">
			<xsl:value-of
				select="exsl:node-set($new-terminals)//t[@in-pivot][position()=1]/@id" />
		</xsl:variable>

		<xsl:variable name="last-pivot-id">
			<xsl:value-of
				select="exsl:node-set($new-terminals)//t[@in-pivot][position()=last()]/@id" />
		</xsl:variable>

		<!-- At last! At last! We can output the MF! -->

		<xsl:for-each select="exsl:node-set($new-terminals)//t">

			<xsl:if
				test="@id = $first-pivot-id or @id = $last-pivot-id or (preceding-sibling::t[@id = $first-pivot-id] and following-sibling::t[@id = $last-pivot-id])">

				<!-- print the word -->

				<xsl:if test="not(@id = $first-pivot-id)">
					<xsl:text> </xsl:text>
				</xsl:if>

				<xsl:choose>

					<xsl:when test="@in-pivot">
						<xsl:apply-templates select="." mode="output" />
					</xsl:when>

					<xsl:otherwise>
						<xsl:text>[</xsl:text>
						<xsl:apply-templates select="." mode="output" />
						<xsl:text>]</xsl:text>
					</xsl:otherwise>

				</xsl:choose>

			</xsl:if>

		</xsl:for-each>

	</xsl:template>

	<xsl:template match="@*" mode="copy">
		<!-- returns a copy of all attribute nodes -->
		<xsl:copy />
	</xsl:template>

	<xsl:template match="t" mode="get-terminals">

		<xsl:copy-of select="." />

	</xsl:template>

	<xsl:template match="nt" mode="get-terminals">

		<xsl:for-each select="edge">

			<xsl:apply-templates
				select="key('idkey', @idref)" mode="get-terminals" />

		</xsl:for-each>

	</xsl:template>

</xsl:stylesheet>
