<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:me="http://www.menota.org/ns/1.0"
	xmlns:bfm="http://bfm.ens-lsh.fr/ns/1.0">

	<!-- Based on a stylesheet for creating separate XML-TEI files for each 
		page by Paul Caton (posted on TEI-L 2008-12-11) -->

	<xsl:output method="xml" encoding="UTF-8" />

	<xsl:param name="pbval1">
		7
	</xsl:param>
	<xsl:param name="pbval2">
		8
	</xsl:param>

	<xsl:function name="tei:page-content" as="node()*">
		<xsl:param name="pb1" as="node()" />
		<xsl:param name="pb2" as="node()" />
		<xsl:param name="node" as="node()" />
		<xsl:choose>
			<xsl:when test="$node[self::*]">
				<!-- $node is an element() -->
				<xsl:choose>
					<!-- <xsl:when test="$node is $pb1"> <xsl:copy-of select="$node"/> </xsl:when> -->
					<xsl:when
						test="some $n in $node/descendant::* satisfies ($n is $pb1 or $n is $pb2)">
						<xsl:element name="{name($node)}"
							namespace="http://www.tei-c.org/ns/1.0">
							<!--<xsl:attribute name="testttt">http://www.menota.org/ns/1.0</xsl:attribute> -->
							<xsl:sequence
								select="for $i in ( $node/node() | $node/@* ) return tei:page-content($pb1, $pb2, $i)" />
						</xsl:element>
					</xsl:when>
					<xsl:when test="($node >> $pb1) and ($node &lt;&lt; $pb2)">
						<xsl:copy-of select="$node" />
					</xsl:when>
					<xsl:otherwise>
						<!-- do nothing -->
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$node[count(. | ../@*) = count(../@*)]">
				<!-- $node is an attribute -->
				<xsl:attribute name="{name($node)}">
                    <xsl:sequence select="data($node)" />
                </xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="($node >> $pb1) and ($node &lt;&lt; $pb2)">
						<xsl:copy-of select="$node" />
					</xsl:when>
					<xsl:otherwise>
						<!-- do nothing -->
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<xsl:template match="*|text()|@*">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="processing-instruction()">
		<xsl:copy>
			<xsl:value-of select="." />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="body">
		<!-- <xsl:variable name="containingNode" select="//text"/> -->
		<xsl:sequence
			select="tei:page-content(//pb[@id=$pbval1], //pb[@id=$pbval2], //body)" />
	</xsl:template>
</xsl:stylesheet>
