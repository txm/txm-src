<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:txm="http://textometrie.org/1.0" exclude-result-prefixes="#all"
	version="2.0">

	<xsl:output method="xml" encoding="UTF-8"
		omit-xml-declaration="no" indent="no"
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />

	<xsl:strip-space elements="*" />

	<xsl:param name="editionpagetag">
		pb
	</xsl:param>

	<xsl:param name="textcontainer">
		text
	</xsl:param>

	<!--<xsl:param name="frontmatter"></xsl:param> <xsl:param name="backmatter">back</xsl:param> -->

	<xsl:variable name="inputtype">
		<xsl:choose>
			<xsl:when test="//tei:w//txm:form">
				xmltxm
			</xsl:when>
			<xsl:otherwise>
				xmlw
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:template match="/">
		<html>
			<head>
				<title>
					<xsl:value-of select="//tei:text[1]/@id" />
				</title>
				<meta http-equiv="Content-Type"
					content="text/html;charset=UTF-8" />
			</head>
			<body>
				<a class="txm-page" title="1" />
				<div class="metadata-page">
					<h1>
						<xsl:value-of select="//tei:text[1]/@id"></xsl:value-of>
					</h1>
					<br />
					<table>
						<xsl:for-each select="//tei:text[1]/@*">
							<tr>
								<td>
									<xsl:value-of select="name()" />
								</td>
								<td>
									<xsl:value-of select="." />
								</td>
							</tr>
						</xsl:for-each>
					</table>
					<br />
					<br />
				</div>
				<xsl:apply-templates
					select="descendant::*[local-name()=$textcontainer]" />


			</body>
		</html>
	</xsl:template>

	<xsl:template match="//*[local-name()=$textcontainer]">
		<xsl:variable name="pbcount"></xsl:variable>

		<div class="text" style="margin-left:60px;">
			<xsl:apply-templates />
		</div>
	</xsl:template>



	<xsl:template match="*">
		<xsl:choose>
			<xsl:when test="descendant::tei:p|descendant::tei:ab">
				<div>
					<xsl:call-template name="addClass" />
					<xsl:apply-templates />
				</div>
				<xsl:text>&#xa;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<span>
					<xsl:call-template name="addClass" />
					<xsl:if test="self::tei:add[@del]">
						<xsl:attribute name="title"><xsl:value-of
							select="@del" /></xsl:attribute>
					</xsl:if>
					<xsl:apply-templates />
				</span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template
		match="@*|processing-instruction()|comment()">

	</xsl:template>

	<xsl:template match="text()">
		<xsl:value-of select="normalize-space(.)" />
	</xsl:template>

	<xsl:template name="addClass">
		<xsl:attribute name="class">
			<xsl:value-of select="local-name(.)" />
			<xsl:if test="@type"><xsl:value-of
			select="concat('-',@type)" /></xsl:if>
			<xsl:if test="@subtype"><xsl:value-of
			select="concat('-',@subtype)" /></xsl:if>
			<xsl:if test="@rend"><xsl:value-of
			select="concat('-',@rend)" /></xsl:if>
		</xsl:attribute>
	</xsl:template>



	<xsl:template match="//word">
		<xsl:choose>
			<xsl:when test="@line = preceding::word[1]/@line">
				<xsl:text> </xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<br />
				<xsl:text>&#xa;</xsl:text>
				<span style="position:relative">
					<span class="linenumber"
						style="position:absolute;width:40px;left:-50px;padding-right:20px;text-align:right">
						<xsl:value-of select="@line" />
						&#xa0;
					</span>
				</span>
			</xsl:otherwise>
		</xsl:choose>
		<span>
			<xsl:call-template name="addClass" />
			<xsl:apply-templates />
		</span>
	</xsl:template>



	<!-- Page breaks -->
	<xsl:template match="//*[local-name()=$editionpagetag]">
		<xsl:variable name="editionpagetype">
			<xsl:choose>
				<xsl:when test="ancestor::tei:ab">
					editionpageverse
				</xsl:when>
				<xsl:otherwise>
					editionpage
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="pagenumber">
			<xsl:choose>
				<xsl:when test="@n">
					<xsl:value-of select="@n" />
				</xsl:when>
				<xsl:when test="@facs">
					<xsl:value-of select="substring-before(@facs,'.')" />
				</xsl:when>
				<xsl:otherwise>
					[NN]
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="page_id">
			<xsl:value-of
				select="count(preceding::*[local-name()=$editionpagetag])" />
		</xsl:variable>
		<xsl:if test="@break='no'">
			<span class="hyphen">-</span>
		</xsl:if>
		<xsl:if
			test="//tei:note[not(@place='inline') and not(matches(@type,'intern|auto'))][following::*[local-name()=$editionpagetag][1][count(preceding::*[local-name()=$editionpagetag]) = $page_id]]">
			<xsl:text>&#xa;</xsl:text>
			<br />
			<br />
			<span class="footnotes">
				<xsl:for-each
					select="//tei:note[not(@place='inline') and not(matches(@type,'intern|auto'))][following::*[local-name()=$editionpagetag][1][count(preceding::*[local-name()=$editionpagetag]) = $page_id]]">
					<xsl:variable name="note_count">
						<xsl:value-of
							select="count(preceding::tei:note[not(@place='inline')]) + 1" />
					</xsl:variable>
					<!--<p><xsl:value-of select="$note_count"/>. <a href="#noteref_{$note_count}" 
						name="note_{$note_count}">[<xsl:value-of select="preceding::tei:cb[1]/@xml:id"/>, 
						l. <xsl:value-of select="preceding::tei:lb[1]/@n"/>]</a><xsl:text> </xsl:text> 
						<xsl:value-of select="."/></p> -->
					<span class="note">
						<span class="footnotenumber">
							<a href="#noteref_{$note_count}" name="note_{$note_count}">
								<xsl:value-of select="$note_count" />
							</a>
							.
						</span>
						<xsl:apply-templates mode="#current" />
					</span>
				</xsl:for-each>
			</span>
			<xsl:text>&#xa;</xsl:text>

		</xsl:if>

		<xsl:text>&#xa;</xsl:text>
		<br />
		<xsl:text>&#xa;</xsl:text>
		<a class="txm-page"
			title="{count(preceding::*[local-name()=$editionpagetag]) + 2}" />
		<span class="{$editionpagetype}">
			-
			<xsl:value-of select="$pagenumber" />
			-
		</span>
		<br />
		<xsl:text>&#xa;</xsl:text>
	</xsl:template>

	<!-- Notes -->
	<!-- <xsl:template match="tei:note[not(@place='inline') and not(matches(@type,'intern|auto'))]"> 
		<!-\-<span style="color:violet"> [<b>Note :</b> <xsl:apply-templates/>] </span>-\-> 
		<xsl:variable name="note_count"><xsl:value-of select="count(preceding::tei:note[not(@place='inline') 
		and not(matches(@type,'intern|auto'))]) + 1"/></xsl:variable> <a title="{.}" 
		class="noteref" href="#note_{$note_count}" name="noteref_{$note_count}">[<xsl:value-of 
		select="$note_count"/>]</a> <xsl:call-template name="spacing"/> </xsl:template> -->

	<xsl:template match="//w">
		<span class="w">
			<xsl:if test="@id">
				<xsl:attribute name="id"><xsl:value-of
					select="@id" /></xsl:attribute>
			</xsl:if>
			<xsl:attribute name="title">
			<xsl:choose>
				<xsl:when test="descendant::txm:ana">
					<xsl:for-each select="descendant::txm:ana">
						<xsl:value-of
				select="concat(substring(@type,2),': ',.,' ; ')" />				
					</xsl:for-each>
					<xsl:value-of
				select="concat('translit : ',descendant::txm:form)" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:for-each select="@*">
						<xsl:value-of select="concat(name(),': ',.,' ; ')" />				
					</xsl:for-each>
					<xsl:value-of select="concat('translit : ',.)" />
				</xsl:otherwise>
			</xsl:choose>

		</xsl:attribute>
			<xsl:choose>
				<xsl:when test="descendant::txm:form">
					<xsl:apply-templates select="txm:form" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="." />
				</xsl:otherwise>
			</xsl:choose>

		</span>
		<xsl:if test="following-sibling::w">
			-
		</xsl:if>
	</xsl:template>


	<xsl:template match="//txm:form">
		<xsl:value-of select="." />
	</xsl:template>



</xsl:stylesheet>
