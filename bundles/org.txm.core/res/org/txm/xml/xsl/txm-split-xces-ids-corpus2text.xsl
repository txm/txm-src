<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc">

	<xd:doc type="stylesheet">
		<xd:short>
			This stylesheet transforms a single file of a XCES-IDS corpus into as
			many files as separate texts for TXM XTZ import module
			For more details on source corpus encoding schema see
			http://www1.ids-mannheim.de/kl/projekte/korpora/textmodell.html
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2018, CNRS / UMR 5317 IHRIM (CACTUS research group)</xd:copyright>
	</xd:doc>

	<!-- Whenever you match any node or any attribute -->
	<xsl:template match="node()|@*">
		<!-- Copy the current node -->
		<xsl:copy>
			<!-- Including any attributes it has and any child nodes -->
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:param name="output-directory">
		<xsl:analyze-string select="document-uri(.)"
			regex="^(.*)/([^/]+)\.[^/.]+$">
			<xsl:matching-substring>
				<xsl:value-of select="regex-group(1)" />
			</xsl:matching-substring>
		</xsl:analyze-string>
	</xsl:param>

	<xsl:template match="/">
		<note>Empty output. The content is transferred into result-documents</note>
		<xsl:for-each select="descendant::idsText">
			<xsl:variable name="filename">
				<xsl:value-of
					select="substring-after(idsHeader//textSigle,'MK2')" />
			</xsl:variable>
			<xsl:result-document
				href="{$output-directory}/{$filename}.xml">
				<TEI>
					<xsl:apply-templates />
				</TEI>
			</xsl:result-document>
		</xsl:for-each>
	</xsl:template>


</xsl:stylesheet>
