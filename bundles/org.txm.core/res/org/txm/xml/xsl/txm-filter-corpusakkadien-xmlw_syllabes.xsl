<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc" version="2.0">

	<xd:doc type="stylesheet">
		<xd:short>
			Cette feuille de style permet de préparer les transcriptions de tablettes
			d'argile (projet UMR PROCLAC) à l'import XML/W + CSV de TXM en
			tokenisant
			le corpus au niveau des syllabes.
			Les propriétés de mots sont projetées vers les propriétés des syllabes
			(line,
			lemma, pos).
			Les propriétés unclear et supplied prennent la valeur "yes" si la
			syllabe se
			trouve à l'intérieur de la balise correspondante.
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev, alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2014-2015, CNRS / ICAR (équipe CACTUS)</xd:copyright>
	</xd:doc>

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />

	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates
				select="*|@*|processing-instruction()|comment()|text()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="@*|comment()">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="processing-instruction()" />

	<xsl:template match="*[local-name()='teiHeader']" />

	<xsl:template match="*[local-name()='w']//text()">
		<xsl:variable name="line">
			<xsl:value-of
				select="ancestor::*[local-name()='w']/@line" />
		</xsl:variable>
		<xsl:variable name="lemma">
			<xsl:value-of
				select="ancestor::*[local-name()='w']/@lemma" />
		</xsl:variable>
		<xsl:variable name="pos">
			<xsl:value-of
				select="ancestor::*[local-name()='w']/@pos" />
		</xsl:variable>
		<xsl:variable name="word-form">
			<xsl:value-of select="ancestor::*[local-name()='w']" />
		</xsl:variable>
		<xsl:variable name="unclear">
			<xsl:choose>
				<xsl:when test="ancestor::*[local-name()='unclear']">
					yes
				</xsl:when>
				<xsl:otherwise>
					no
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="supplied">
			<xsl:choose>
				<xsl:when test="ancestor::*[local-name()='supplied']">
					yes
				</xsl:when>
				<xsl:otherwise>
					no
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:analyze-string select="."
			regex="[-.]|\([^\)]*\)">
			<xsl:matching-substring>
				<w line="{$line}" lemma="{$lemma}" pos="{$pos}"
					word-form="{$word-form}" supplied="{$supplied}"
					unclear="{$unclear}">
					<xsl:value-of select="." />
				</w>
			</xsl:matching-substring>
			<xsl:non-matching-substring>
				<w line="{$line}" lemma="{$lemma}" pos="{$pos}"
					word-form="{$word-form}" supplied="{$supplied}"
					unclear="{$unclear}">
					<xsl:value-of select="." />
				</w>
			</xsl:non-matching-substring>
		</xsl:analyze-string>
	</xsl:template>

	<xsl:template
		match="text()[not(ancestor::*[local-name()='w'])]">
		<xsl:analyze-string select="." regex="\[…\]">
			<xsl:matching-substring>
				<w line="¤" lemma="¤" pos="¤" word-form="¤" supplied="¤"
					unclear="¤">
					<xsl:value-of select="." />
				</w>
			</xsl:matching-substring>
			<xsl:non-matching-substring>
				<xsl:comment>
					<xsl:copy-of select="." />
				</xsl:comment>
			</xsl:non-matching-substring>
		</xsl:analyze-string>
	</xsl:template>

	<xsl:template match="*[local-name()='w']">
		<word>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="no-supplied">
      <xsl:choose>
        <xsl:when test="ancestor::*[local-name()='supplied']">¤</xsl:when>
        <xsl:otherwise><xsl:apply-templates
				mode="nosupplied" /></xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
			<xsl:attribute name="no-unclear">
      <xsl:choose>
        <xsl:when
				test="ancestor::*[local-name()='supplied'] or ancestor::*[local-name()='unclear']">¤</xsl:when>
        <xsl:otherwise><xsl:apply-templates
				mode="nounclear" /></xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
			<xsl:apply-templates />
		</word>
	</xsl:template>


</xsl:stylesheet>