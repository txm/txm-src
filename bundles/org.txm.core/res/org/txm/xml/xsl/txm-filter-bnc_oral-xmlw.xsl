<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	exclude-result-prefixes="tei edate xd" version="2.0">

	<xd:doc type="stylesheet">
		<xd:short>
			Feuille de style de préparation de fichiers BNC oral à l'importation dans
			TXM
			(module XML/W+CSV).
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2015, CNRS / ICAR (Cédilles-CACTUS)</xd:copyright>
	</xd:doc>


	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />

	<!-- Listes des balises par type de traitement. -->

	<!--Par défaut, les balises sont conservées (les espaces de nommage sont 
		respectés). On peut utiliser les paramètres ci-dessous pour indiquer les 
		balises à supprimer en conservant ou non leur contenu -->

	<!-- balises à supprimer avec leur contenu -->

	<xsl:param name="deleteAll">
		teiHeader
	</xsl:param>

	<xsl:template
		match="*[matches(name(),concat('^',$deleteAll,'$'))]" />

	<!-- balises à supprimer en conservant le contenu -->

	<xsl:param name="deleteTag"></xsl:param>

	<xsl:template
		match="*[matches(name(),concat('^',$deleteTag,'$'))]">
		<xsl:apply-templates />
	</xsl:template>


	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates
				select="*|@*|processing-instruction()|comment()|text()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="@*|comment()">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="processing-instruction()" />

	<xsl:template match="text()">
		<xsl:value-of select="." />
	</xsl:template>

	<xsl:template match="stext">
		<xsl:element name="text">
			<xsl:attribute name="type"><xsl:value-of
				select="@type" /></xsl:attribute>
			<xsl:attribute name="title"><xsl:value-of
				select="normalize-space(/bncDoc/teiHeader/fileDesc/titleStmt/title[1])" /></xsl:attribute>
			<xsl:attribute name="creation"><xsl:value-of
				select="normalize-space(/bncDoc/teiHeader/profileDesc/creation[1])" /></xsl:attribute>
			<xsl:attribute name="dlee"><xsl:value-of
				select="/bncDoc/teiHeader/profileDesc/textClass/classCode[@scheme='DLEE']" /></xsl:attribute>
			<xsl:choose>
				<xsl:when test="not(descendant::div[@decls])">

					<xsl:element name="div">
						<xsl:attribute name="placename"><xsl:value-of
							select="normalize-space(/bncDoc/teiHeader/profileDesc/settingDesc/setting[1]/placeName[1])" /></xsl:attribute>
						<xsl:attribute name="locale"><xsl:value-of
							select="normalize-space(/bncDoc/teiHeader/profileDesc/settingDesc/setting[1]/locale[1])" /></xsl:attribute>
						<xsl:attribute name="activity"><xsl:value-of
							select="normalize-space(/bncDoc/teiHeader/profileDesc/settingDesc/setting[1]/activity[1])" /></xsl:attribute>
						<xsl:attribute name="activity-spont"><xsl:value-of
							select="normalize-space(/bncDoc/teiHeader/profileDesc/settingDesc/setting[1]/activity[1]/@spont)" /></xsl:attribute>
						<xsl:attribute name="recordingdate"><xsl:value-of
							select="normalize-space(/bncDoc/teiHeader/profileDesc/creation[1])" /></xsl:attribute>
						<xsl:attribute name="dlee"><xsl:value-of
							select="/bncDoc/teiHeader/profileDesc/textClass/classCode[@scheme='DLEE']" /></xsl:attribute>
						<xsl:apply-templates />
					</xsl:element>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>

	<xsl:template match="div[@decls]">
		<xsl:variable name="decls">
			<xsl:value-of select="@decls" />
		</xsl:variable>
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="placename"><xsl:value-of
				select="normalize-space(/bncDoc/teiHeader/profileDesc/settingDesc/setting[matches($decls,@xml:id)]/placeName[1])" /></xsl:attribute>
			<xsl:attribute name="locale"><xsl:value-of
				select="normalize-space(/bncDoc/teiHeader/profileDesc/settingDesc/setting[matches($decls,@xml:id)]/locale[1])" /></xsl:attribute>
			<xsl:attribute name="activity"><xsl:value-of
				select="normalize-space(/bncDoc/teiHeader/profileDesc/settingDesc/setting[matches($decls,@xml:id)]/activity[1])" /></xsl:attribute>
			<xsl:attribute name="activity-spont"><xsl:value-of
				select="normalize-space(/bncDoc/teiHeader/profileDesc/settingDesc/setting[1]/activity[1]/@spont)" /></xsl:attribute>

			<xsl:attribute name="recordingdate"><xsl:value-of
				select="normalize-space(/bncDoc/teiHeader/fileDesc/sourceDesc/recordingStmt/recording[matches($decls,@xml:id)]/@date)" /></xsl:attribute>
			<xsl:attribute name="dlee"><xsl:value-of
				select="/bncDoc/teiHeader/profileDesc/textClass/classCode[@scheme='DLEE']" /></xsl:attribute>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="u">
		<xsl:copy>
			<xsl:variable name="who">
				<xsl:value-of select="@who" />
			</xsl:variable>
			<xsl:attribute name="who"><xsl:value-of
				select="$who" /></xsl:attribute>
			<xsl:for-each
				select="/bncDoc/teiHeader/profileDesc/particDesc/person[@xml:id=$who]/@*[not(local-name()='id')]">
				<xsl:attribute name="{lower-case(local-name())}"><xsl:value-of
					select="." /></xsl:attribute>
			</xsl:for-each>
			<xsl:for-each
				select="/bncDoc/teiHeader/profileDesc/particDesc/person[@xml:id=$who]/*">
				<xsl:attribute name="{lower-case(local-name())}"><xsl:value-of
					select="normalize-space(.)" /></xsl:attribute>
			</xsl:for-each>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>



</xsl:stylesheet>