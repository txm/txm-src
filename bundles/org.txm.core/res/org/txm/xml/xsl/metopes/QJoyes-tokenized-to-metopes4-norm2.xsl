<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
       xmlns:edate="http://exslt.org/dates-and-times"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                xmlns:me="http://www.menota.org/ns/1.0"
                xmlns:bfm="http://bfm.ens-lsh.fr/ns/1.0"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                exclude-result-prefixes="#all"
                xmlns="http://www.tei-c.org/ns/1.0">

<xsl:output method="xml" encoding="utf-8" indent="no"/>

<xsl:template match="@*|*|comment()|processing-instruction()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

<!-- <xsl:template match="tei:p[@style='txt_Normal']">
    <xsl:variable name="num">
      <xsl:number count="tei:p[@style='txt_Normal']"/>
    </xsl:variable> 
  

    <p style="txt_Paranum"><xsl:value-of select="$num"/></p>
    <p style="txt_Normal">
      <xsl:attribute name="n">
         <xsl:value-of select="$num"/>      
</xsl:attribute>
      <xsl:apply-templates/>
    </p>
     

   
  
</xsl:template> -->

<!--<xsl:template match="tei:seg">
  <xsl:copy-of select="./text()"/>
</xsl:template>-->
  
  <!--<xsl:template match="tei:seg">
    <xsl:apply-templates/>
  </xsl:template>-->
  <xsl:template match="tei:w">
    <xsl:apply-templates/>
    <xsl:call-template name="spacing-norm"/>
  </xsl:template>

<!-- on supprime del dans la version normalisée -->
  <xsl:template match="tei:del">
    <xsl:apply-templates select="descendant::tei:lb[@n]"/>
  </xsl:template>
    

  <xsl:template match="tei:add">
    <xsl:apply-templates/>
    <xsl:call-template name="spacing-norm"/>
  </xsl:template>
  
  <xsl:template match="tei:subst">
    <xsl:apply-templates/>
    <!--<xsl:call-template name="spacing-norm"/>-->
  </xsl:template>

  <xsl:template match="tei:supplied">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="tei:surplus">
    <seg xmlns="http://www.tei-c.org/ns/1.0" type="surplus">
      <xsl:text>(</xsl:text>
      <xsl:apply-templates/>
      <xsl:text>)</xsl:text>
    </seg>
  </xsl:template>

  <!-- sauts de lignes préservés en norm2 -->
  <xsl:template match="tei:lb[not(@n)]"><xsl:text> </xsl:text></xsl:template>
  
  <xsl:template match="tei:lb[@n]">
    <xsl:if test="ancestor::tei:w and not(preceding-sibling::tei:pb) and not(ancestor::tei:del or following-sibling::*[1][self::tei:del])">
      <xsl:text>-</xsl:text>
    </xsl:if>
        <xsl:if test="preceding-sibling::tei:w or not(matches(preceding-sibling::text()[1],'\n\s*$')) or preceding-sibling::*[1][self::tei:q]">
          <xsl:text>&#xa;</xsl:text>
        </xsl:if>
    <xsl:text>&#x9;</xsl:text>
        <!--<xsl:copy>
                                                <xsl:apply-templates select="@*"/>
                                </xsl:copy>--><xsl:if test="matches(@n,'[05a-z]\]?$')"><num xmlns="http://www.tei-c.org/ns/1.0"><xsl:value-of select="@n"/></num></xsl:if>
        <!--<num xmlns="http://www.tei-c.org/ns/1.0"><xsl:value-of select="@n"/></num>-->
        <xsl:text>&#x9;</xsl:text>
        <xsl:if test="@rend='alinea' or matches(@rend,'indent\([123]\)')">
          <xsl:text>&#x9;</xsl:text>
        </xsl:if>
        <xsl:if test="matches(@rend,'indent\([23]\)')"><xsl:text>     </xsl:text></xsl:if>
        <xsl:if test="matches(@rend,'indent\([3]\)')"><xsl:text>     </xsl:text></xsl:if>
  </xsl:template>
  
  <xsl:template match="tei:ex|tei:expan">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="tei:g">
    <xsl:apply-templates/>
  </xsl:template>
  
  
  <xsl:template match="tei:space">
    <xsl:choose>
      <xsl:when test="@rend='degl-fgmt'">
        <xsl:text> </xsl:text>
      </xsl:when>
      <xsl:otherwise/>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="tei:pb[@n]">
    <xsl:choose>
      <xsl:when test="following-sibling::node()[1][self::text()][matches(.,'^\s*$')] and following-sibling::node()[2][self::tei:pb]">
        <xsl:comment>Page blanche</xsl:comment>
      </xsl:when>
      <xsl:otherwise>
        <!--<xsl:if test="following::tei:lb[1][@n]">
          <xsl:text>&#xa;</xsl:text>
        </xsl:if>-->
        <xsl:text>&#xa;</xsl:text>
        <xsl:copy>
        </xsl:copy>
        <xsl:choose>
          <xsl:when test="ancestor::tei:p">
            <hi style="typo_pagenum" xmlns="http://www.tei-c.org/ns/1.0">
              <xsl:value-of select="concat('&lt;',@n,'>')"/>
            </hi>
          </xsl:when>
          <xsl:otherwise>
            <p xmlns="http://www.tei-c.org/ns/1.0">
              <hi style="typo_pagenum" xmlns="http://www.tei-c.org/ns/1.0"><xsl:value-of select="concat('&lt;',@n,'>')"/></hi>
            </p>
          </xsl:otherwise>
        </xsl:choose>
<!--        <xsl:if test="following::tei:lb[1][@n] or following-sibling::*[1][self::tei:div or self::tei:ap or self::tei:p]">
          <xsl:text>&#xa;</xsl:text>
        </xsl:if>-->
        <!--<xsl:text>&#xa;</xsl:text>-->
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="tei:choice">
    <xsl:apply-templates select="tei:corr|tei:reg|tei:expan"/>
  </xsl:template>
  
  <xsl:template match="tei:corr">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="tei:note[@type='corr']"></xsl:template>
  
  <xsl:template match="tei:note[not(@type='corr')]">
    <!--<xsl:variable name="nfoot">
    <xsl:number count="tei:note[@type='philo']" format="1" from="tei:body" level="any"/>
  </xsl:variable>
    <xsl:variable name="nsic">
    <xsl:number count="tei:note[@type='sic']" format="a" from="tei:body" level="any"/>
  </xsl:variable>-->
    
    <note type="{@type}" place="{@place}">
      <xsl:if test="./@resp">
        <xsl:attribute name="resp">
          <xsl:value-of select="./@resp"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="./@xml:id">
        <xsl:attribute name="xml:id">
          <xsl:value-of select="./@xml:id"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:attribute name="n">
        <xsl:number level="any"/>
      </xsl:attribute>
      <xsl:attribute name="style">
        <!--          <xsl:if test="@type='philo'">
            <xsl:value-of select="'txt_Note'"/>
          </xsl:if>
          <xsl:if test="@type='sic'">
            <xsl:value-of select="'txt_Note_sic'"/>
          </xsl:if>-->
        <xsl:value-of select="'txt_Note'"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </note>
    <xsl:call-template name="spacing-norm"/>
    
  </xsl:template>
  
  
  <xsl:template match="text()">
    <xsl:choose>
      <xsl:when test="parent::tei:w[@pos='PONpga'] and matches(.,'^''$')">‘</xsl:when>
      <xsl:when test="parent::tei:w[@pos='PONpga'] and matches(.,'^&quot;$')">“</xsl:when>
      <xsl:when test="parent::tei:w[@pos='PONpdr'] and matches(.,'^&quot;$')">”</xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="translate(.,'''','’')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="spacing-norm">
    <xsl:choose>
      <xsl:when test="@aggl='fgmt'"/>
      <xsl:when test="ancestor::tei:w"/>
      <xsl:when test="following::tei:w[1][matches(.,'^\s*[.,)\]]+\s*$')]"/>	
      <xsl:when test="following-sibling::node()[1][self::text()][matches(.,'^\s*[.,)\]]+\s*$')]"/>
      <xsl:when test="matches(.,'^\s*[(\[]+$|\w(''|’)\s*$')"></xsl:when>
      <xsl:when test="position()=last() and (ancestor::tei:choice or ancestor::tei:supplied[not(@rend='multi_s')] or ancestor::tei:surplus)"></xsl:when>
      <xsl:when test="following-sibling::*[1][self::tei:note]"></xsl:when>
      <xsl:when test="following::tei:w[1][matches(.,'^\s*[:;!?]+\s*$')]">
        <xsl:text>&#xa0;</xsl:text>
      </xsl:when>
      <xsl:when test="following::tei:w[1][matches(.,'^\s*&#xa0;[:;!?»]+\s*$')]">
      </xsl:when>
      <xsl:when test="matches(.,'«')"><xsl:text>&#xa0;</xsl:text></xsl:when>
      <xsl:otherwise>
        <xsl:text> </xsl:text>
      </xsl:otherwise>
    </xsl:choose>                
  </xsl:template>
  


</xsl:stylesheet> 