<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
       xmlns:edate="http://exslt.org/dates-and-times"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                xmlns:me="http://www.menota.org/ns/1.0"
                xmlns:bfm="http://bfm.ens-lsh.fr/ns/1.0"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:xi="http://www.w3.org/2001/XInclude"
                exclude-result-prefixes="#all"
                xmlns="http://www.tei-c.org/ns/1.0">

<xsl:output method="xml" encoding="utf-8" indent="no"/>

<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

<!--<xsl:template match="tei:p[@style='txt_Normal']">
<xsl:choose>
  <xsl:when test="child::tei:p[@style='txt_pagenum']">
    <pb/>
    <p style="txt_pagenum">
       <xsl:value-of select="."/>
    </p>
  </xsl:when>
  <xsl:otherwise>
  
    <xsl:copy-of select="."/>
  
  </xsl:otherwise>
</xsl:choose>
</xsl:template>-->

<xsl:template match="tei:note">
  <!-- ancienne condition [@type='général' and @place='foot'] -->
  <note type="philo">
    <xsl:attribute name="place">
      <xsl:choose>
        <xsl:when test="@place"><xsl:value-of select="@place"/></xsl:when>
        <xsl:otherwise>foot</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="class">
      <xsl:choose>
        <xsl:when test="descendant::tei:p">txt_Note_suite</xsl:when>
        <xsl:otherwise>txt_Note</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
     <xsl:if test="./@resp">
          <xsl:attribute name="resp">
            <xsl:value-of select="./@resp"/>
          </xsl:attribute>
        </xsl:if>
          <xsl:if test="./@xml:id">
          <xsl:attribute name="xml:id">
            <xsl:value-of select="./@xml:id"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:apply-templates/>
    <xsl:if test="./@resp">
      <xsl:if test="not(matches(.,'\s$'))">
        <xsl:text> </xsl:text>
      </xsl:if>
      <hi style="typo_noteResp">
        <xsl:text>(</xsl:text>
        <xsl:choose>
          <xsl:when test="matches(@resp,'#GLM #NK')">GLM &amp; NK</xsl:when>
          <xsl:when test="matches(@resp,'^#GLM$')">GLM</xsl:when>
          <xsl:when test="matches(@resp,'^#NK$')">NK</xsl:when>
          <xsl:otherwise><xsl:value-of select="replace(@resp,'#','')"/></xsl:otherwise>
        </xsl:choose>
        <xsl:text>)</xsl:text>
      </hi>
    </xsl:if>
      </note>
    </xsl:template>

</xsl:stylesheet>