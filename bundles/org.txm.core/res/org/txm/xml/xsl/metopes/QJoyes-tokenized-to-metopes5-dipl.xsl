<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
       xmlns:edate="http://exslt.org/dates-and-times"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                xmlns:me="http://www.menota.org/ns/1.0"
                xmlns:bfm="http://bfm.ens-lsh.fr/ns/1.0"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                exclude-result-prefixes="#all"
                xmlns="http://www.tei-c.org/ns/1.0">

<xsl:output method="xml" encoding="utf-8" indent="no"/>

<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

<!-- <xsl:template match="tei:p[@style='txt_Normal']">
    <xsl:variable name="num">
      <xsl:number count="tei:p[@style='txt_Normal']"/>
    </xsl:variable> 
  

    <p style="txt_Paranum"><xsl:value-of select="$num"/></p>
    <p style="txt_Normal">
      <xsl:attribute name="n">
         <xsl:value-of select="$num"/>      
</xsl:attribute>
      <xsl:apply-templates/>
    </p>
     

   
  
</xsl:template> -->

<!--<xsl:template match="tei:seg">
  <xsl:copy-of select="./text()"/>
</xsl:template>-->
  
  <!--<xsl:template match="tei:seg">
    <xsl:apply-templates/>
  </xsl:template>-->
  <xsl:template match="tei:w">
    <xsl:apply-templates select="@dipl"/>
    <xsl:call-template name="spacing-dipl"/>
  </xsl:template>
  
  
  
  <xsl:template match="tei:w/@dipl">
    <!--<xsl:variable name="document"><xsl:copy-of select="/"></xsl:copy-of></xsl:variable>-->
    <!--<xsl:variable name="lb-n">
			<xsl:value-of select="preceding::tei:milestone[unit='lb-facs'][1]/@n"/>
		</xsl:variable>-->
    <!--<xsl:variable name="lb-n-adjust">
			<xsl:number count="text()[contains(.,'||')]" from="tei:lb"/>
		</xsl:variable>-->
    <xsl:variable name="lb-n">
      <xsl:choose>
        <xsl:when test="ancestor::tei:w//tei:lb">
          <xsl:value-of select="ancestor::tei:w//tei:lb[1]/@n"/>
        </xsl:when>
        <xsl:otherwise>NaN</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="line-break-pattern" as="element()">
      <xsl:choose>
        <xsl:when test="parent::tei:w//tei:lb">
          <with-line>
            <xsl:apply-templates select="parent::tei:w//tei:lb"/>
          </with-line>
        </xsl:when>
        <xsl:otherwise><no-line/></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="page-break-pattern" as="element()">
      <xsl:choose>
        <xsl:when test="ancestor::tei:w//tei:pb">
          <with-page>
            <xsl:apply-templates select="ancestor::tei:w//tei:pb[1]"/>
            <!--<xsl:text>XXX</xsl:text>-->
          </with-page>
        </xsl:when>
        <xsl:otherwise><no-page/></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="color">
      <xsl:choose>
        <xsl:when test="matches(parent::tei:w//tei:g[@type='initiale']/@rend,'color\(\w+\)')">
          <xsl:value-of select="replace(parent::tei:w//tei:g[@type='initiale']/@rend,'^.*color\((\w+)\).*$','$1')"/>
        </xsl:when>
        <xsl:otherwise>undef</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="supplied-rend">
      <xsl:choose>
        <xsl:when test="ancestor::tei:w//tei:supplied[@rend]">
          <xsl:value-of select="ancestor::tei:w//tei:supplied/@rend"/>
        </xsl:when>
        <xsl:otherwise>normal</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="in-supplied">
      <xsl:choose>
        <xsl:when test="ancestor::tei:w[parent::tei:supplied and position() eq 1]">yes</xsl:when>
        <xsl:otherwise>no</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    
    <xsl:analyze-string select="." regex="?\|\||\{{([^\}}]*)\}}">
      <xsl:matching-substring>
        <xsl:choose>
          <xsl:when test="matches(.,'?\|\|')">
<!--            <xsl:if test="matches(.,'')">-->
              <xsl:text>-</xsl:text>
            <!--</xsl:if>-->
            <!--<xsl:copy-of select="$page-break-pattern"/>-->
                        <xsl:if test="$page-break-pattern[self::*:with-page]">
              <xsl:copy-of select="$page-break-pattern/node()"/>
              </xsl:if>
            <xsl:copy-of select="$line-break-pattern/node()"/>
          </xsl:when>          
          <xsl:when test="matches(.,'\{([^}]*)\}')">
            <seg type="lettrine" style="typo_gras" xmlns="http://www.tei-c.org/ns/1.0">
              <!--<xsl:if test="not($color='undef')">
                <xsl:attribute name="style">
                  <xsl:value-of select="concat('color:',$color)"/>
                </xsl:attribute>
              </xsl:if>-->
<!--              <xsl:if test="$in-supplied='yes'">
                <span class="lettrine-crochet">[</span>
              </xsl:if>
-->              <xsl:value-of select="regex-group(2)"/>
            </seg>
          </xsl:when>
        </xsl:choose>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:call-template name="process-add-dipl">
          <xsl:with-param name="text">
            <xsl:value-of select="."/>
          </xsl:with-param>
          <xsl:with-param name="supplied-rend"><xsl:value-of select="$supplied-rend"/></xsl:with-param>
        </xsl:call-template>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  
  <xsl:template name="process-add-dipl">
    <xsl:param name="text"/>
    <xsl:param name="supplied-rend"/>
    
    <xsl:analyze-string select="$text" regex="\[+([^\]]+)\]+">
      <xsl:matching-substring>
        <xsl:choose>
          <xsl:when test="matches(.,'\[[^\]]*/[^\]]*\]')">
            <xsl:if test="matches(substring-before(regex-group(1),'/'),'.+')">
              <seg type="del" style="typo_Del" xmlns="http://www.tei-c.org/ns/1.0">
                <xsl:call-template name="process-ex-dipl"><xsl:with-param name="text"><xsl:value-of select="substring-before(regex-group(1),'/')"/></xsl:with-param></xsl:call-template>
              </seg>
            </xsl:if>
            <xsl:if test="matches(substring-after(regex-group(1),'/'),'.+')">
              <seg type="add" style="typo_souligne">
                <xsl:call-template name="process-ex-dipl"><xsl:with-param name="text"><xsl:value-of select="substring-after(regex-group(1),'/')"/></xsl:with-param></xsl:call-template>
              </seg>
            </xsl:if>
          </xsl:when>
          <xsl:when test="matches(.,'\[[^\]]*\]')">
            <seg type="supplied" style="typo_supplied" xmlns="http://www.tei-c.org/ns/1.0">
              <xsl:if test="matches($supplied-rend,'normal|initial')">
                <xsl:text>[</xsl:text>
              </xsl:if>
              <xsl:value-of select="regex-group(1)"/>
              <xsl:if test="matches($supplied-rend,'normal|final')">
                <xsl:text>]</xsl:text>
              </xsl:if>
            </seg>
          </xsl:when>
        </xsl:choose>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:call-template name="process-ex-dipl">
          <xsl:with-param name="text">
            <xsl:value-of select="."/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  
    <xsl:template name="process-ex-dipl">
      <xsl:param name="text"/>
      <xsl:analyze-string select="$text" regex="‹([^›]+)›">
        <xsl:matching-substring>
          <seg type="ex" style="typo_Italique" xmlns="http://www.tei-c.org/ns/1.0"><xsl:value-of select="regex-group(1)"></xsl:value-of></seg>
        </xsl:matching-substring>
        <xsl:non-matching-substring>
          <xsl:call-template name="deglutinations">
            <xsl:with-param name="string">
              <xsl:value-of select="."/>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:non-matching-substring>
      </xsl:analyze-string>
    
  </xsl:template>
  
  <xsl:template name="deglutinations">
    <xsl:param name="string"/>
    <xsl:analyze-string select="$string" regex="__?\??">
      <xsl:matching-substring>
        <xsl:choose>
          <xsl:when test="matches(.,'__')">
            <xsl:text> </xsl:text>
          </xsl:when>
          <xsl:otherwise/>
        </xsl:choose>
      </xsl:matching-substring>
      <xsl:non-matching-substring><xsl:value-of select="replace(.,'','-')"/></xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  
  <!--<xsl:template match="tei:w/@dipl">
    <xsl:variable name="line-break-pattern" as="element()">
      <xsl:choose>
        <xsl:when test="parent::tei:w//tei:lb">
          <with-line>
            <xsl:apply-templates select="parent::tei:w//tei:lb"/>
          </with-line>
        </xsl:when>
        <xsl:otherwise><no-line/></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="page-break-pattern" as="element()">
      <xsl:choose>
        <xsl:when test="parent::tei:w//tei:pb">
          <with-page>
            <xsl:apply-templates select="parent::tei:w//tei:pb"/>
            <!-\-<xsl:text>XXX</xsl:text>-\->
          </with-page>
        </xsl:when>
        <xsl:otherwise><no-page/></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="color">
      <xsl:choose>
        <xsl:when test="matches(parent::tei:w//tei:hi[@style='typo_initiale']/@rend,'color\(\w+\)')">
          <xsl:value-of select="replace(ancestor::tei:w//tei:hi[@style='typo_initiale']/@rend,'^.*color\((\w+)\).*$','$1')"/>
        </xsl:when>
        <xsl:otherwise>black</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    
    <xsl:analyze-string select="." regex="‹([^›]+)›|?\|\||\{{([^\}}]*)\}}">
      <xsl:matching-substring>
        <xsl:choose>
          <xsl:when test="matches(.,'‹[^›]+›')">
            <hi style="typo_Italic" xmlns="http://www.tei-c.org/ns/1.0"><xsl:value-of select="regex-group(1)"></xsl:value-of></hi>
          </xsl:when>
          <xsl:when test="matches(.,'?\|\|')">
            <xsl:if test="matches(.,'')">
              <xsl:text>-</xsl:text>
            </xsl:if>
<!-\-            <xsl:if test="$page-break-pattern[self::with-page]">-\->
              <xsl:copy-of select="$page-break-pattern/node()"/>
            <!-\-</xsl:if>-\->
            <xsl:copy-of select="$line-break-pattern/node()"/>
          </xsl:when>
          <xsl:when test="matches(.,'\{([^}]*)\}')">
            <hi style="typo_gras" xmlns="http://www.tei-c.org/ns/1.0">
              <xsl:if test="matches($color,'.+') and not(matches($color,'black'))">
                <xsl:attribute name="rend"><xsl:value-of select="concat('color(',$color,')')"/></xsl:attribute>
              </xsl:if>
              <xsl:value-of select="regex-group(2)"/>
            </hi>
          </xsl:when>
        </xsl:choose>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:call-template name="deglutinations">
          <xsl:with-param name="string">
            <xsl:value-of select="."/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>-->
  
  
  
  <xsl:template match="tei:del">
    <seg type="del" style="typo_Del" xmlns="http://www.tei-c.org/ns/1.0">
      <xsl:attribute name="rend">
        <xsl:value-of select="@rend"/>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="matches(.,'^ch.+ſ$')">ch<hi xmlns="http://www.tei-c.org/ns/1.0" style="typo_Italique">evalie</hi>rs</xsl:when>
        <xsl:when test="matches(.,'^chꝛ.$')">ch<hi xmlns="http://www.tei-c.org/ns/1.0" style="typo_Italique">evalie</hi>r</xsl:when>
        <xsl:when test="matches(.,'^lanc.\.$')">lanc<hi xmlns="http://www.tei-c.org/ns/1.0" style="typo_Italique">elot</hi></xsl:when>
        <xsl:when test="matches(.,'^que au comencem.t$')">que au comencem<hi xmlns="http://www.tei-c.org/ns/1.0" style="typo_Italique">en</hi>t</xsl:when>
        <xsl:when test="matches(.,'^remai.dra$')">remai<hi xmlns="http://www.tei-c.org/ns/1.0" style="typo_Italique">n</hi>dra</xsl:when>
        <xsl:when test="matches(.,'^fontai.ne$')">fontai<hi xmlns="http://www.tei-c.org/ns/1.0" style="typo_Italique">n</hi>ne</xsl:when>
        <xsl:when test="matches(.,'^perc.\.$')">perc<hi xmlns="http://www.tei-c.org/ns/1.0" style="typo_Italique">eual</hi></xsl:when>
        <xsl:otherwise><xsl:apply-templates/></xsl:otherwise>
      </xsl:choose>
    </seg>
    <xsl:call-template name="spacing-dipl"/>
  </xsl:template>
  
  <xsl:template match="tei:add">
    <seg type="add" style="typo_souligne" xmlns="http://www.tei-c.org/ns/1.0">
      <xsl:apply-templates/>
    </seg>
    <xsl:call-template name="spacing-dipl"/>
  </xsl:template>
  
  <xsl:template match="tei:subst">
    <xsl:apply-templates/>
    <xsl:call-template name="spacing-dipl"/>
  </xsl:template>
  
  <xsl:template match="tei:supplied">
    <xsl:apply-templates/>
  </xsl:template>
  
  <!-- sauts de lignes préservés en norm2 -->
  
  <xsl:template match="tei:pb[@n]">
    <xsl:choose>
      <xsl:when test="following-sibling::node()[1][self::text()][matches(.,'^\s*$')] and following-sibling::node()[2][self::tei:pb]">
        <xsl:comment>Page blanche</xsl:comment>
      </xsl:when>
      <xsl:otherwise>
        <!--<xsl:if test="following::tei:lb[1][@n]">
          <xsl:text>&#xa;</xsl:text>
        </xsl:if>-->
        <xsl:text>&#xa;</xsl:text>
        <!--<xsl:copy/>-->
        
        <xsl:choose>
          <xsl:when test="ancestor::tei:p">
            <hi style="typo_pagenum" xmlns="http://www.tei-c.org/ns/1.0">
              <xsl:value-of select="concat('&lt;',@n,'>')"/>
            </hi>
          </xsl:when>
          <xsl:otherwise>
            <p xmlns="http://www.tei-c.org/ns/1.0">
              <hi style="typo_pagenum" xmlns="http://www.tei-c.org/ns/1.0"><xsl:value-of select="concat('&lt;',@n,'>')"/></hi>
            </p>
          </xsl:otherwise>
        </xsl:choose>
        <!--        <xsl:if test="following::tei:lb[1][@n] or following-sibling::*[1][self::tei:div or self::tei:ap or self::tei:p]">
          <xsl:text>&#xa;</xsl:text>
        </xsl:if>-->
        <!--<xsl:text>&#xa;</xsl:text>-->
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  
  
  <xsl:template match="tei:lb[not(@n)]"><xsl:text> </xsl:text></xsl:template>
  
  <xsl:template match="tei:lb[@n]">
        <xsl:if test="preceding-sibling::tei:w or not(matches(preceding-sibling::text()[1],'\n\s*$')) or preceding-sibling::*[1][self::tei:q]">
          <xsl:text>&#xa;</xsl:text>
        </xsl:if>
    <xsl:text>&#x9;</xsl:text>
        <!--<xsl:copy>
                                                <xsl:apply-templates select="@*"/>
                                </xsl:copy>--><xsl:if test="matches(@n,'[05a-z]\]?$')"><num xmlns="http://www.tei-c.org/ns/1.0"><xsl:value-of select="@n"/></num></xsl:if>
        <!--<num xmlns="http://www.tei-c.org/ns/1.0"><xsl:value-of select="@n"/></num>-->
        <xsl:text>&#x9;</xsl:text>
        <xsl:if test="@rend='alinea' or matches(@rend,'indent\([123]\)')">
          <xsl:text>&#x9;</xsl:text>
        </xsl:if>
        <xsl:if test="matches(@rend,'indent\([23]\)')"><xsl:text>     </xsl:text></xsl:if>
        <xsl:if test="matches(@rend,'indent\([3]\)')"><xsl:text>     </xsl:text></xsl:if>
  </xsl:template>
  
  <xsl:template match="tei:space">
    <xsl:choose>
      <xsl:when test="@rend='degl-fgmt'">
        <xsl:text> </xsl:text>
      </xsl:when>
      <xsl:otherwise/>
    </xsl:choose>
  </xsl:template>
  
  

<xsl:template match="tei:hi[@style='typo_pagenum']">
  <xsl:text>&#xa0;</xsl:text>
  <xsl:copy-of select="."/>
  <xsl:text>&#xa0;</xsl:text>
</xsl:template>
  
<!--  <xsl:template match="tei:choice">
    <xsl:apply-templates select="tei:sic|tei:orig|tei:expan"/>
  </xsl:template>-->
  
  <xsl:template match="tei:choice[tei:sic]">
    
      <xsl:apply-templates select="tei:sic"/>
    
    <note xmlns="http://www.tei-c.org/ns/1.0" place="foot" type="corr">
      <xsl:attribute name="n">
        <xsl:value-of select="count(preceding::tei:choice[tei:corr]) + count(preceding::tei:surplus)+ 1"/>
      </xsl:attribute>
      <hi xmlns="http://www.tei-c.org/ns/1.0" style="typo_Italic">Lire :</hi>
      <xsl:text> «&#xa0;</xsl:text>                 
      <!--<xsl:value-of select="tei:corr"/>-->
      <!--<xsl:apply-templates select="tei:corr//tei:w"/>-->
      <xsl:for-each select="tei:corr//tei:w">
        <xsl:value-of select="."/>
        <xsl:if test="not(matches(.,'[’'']$')) and following-sibling::tei:w">
          <xsl:text> </xsl:text>
        </xsl:if>
      </xsl:for-each>
      <xsl:text>&#xa0;».</xsl:text>
    </note>
    <xsl:call-template name="spacing-dipl"/>
  </xsl:template>

<xsl:template match="tei:sic">
<!--  <seg type="sic" style="typo_Sic" xmlns="http://www.tei-c.org/ns/1.0">-->
    <xsl:apply-templates/>
  <!--</seg>-->
  <xsl:if test="not(parent::tei:choice)"><xsl:call-template name="spacing-dipl"/></xsl:if>
</xsl:template>

<xsl:template match="tei:surplus">
<!--  <seg type="surplus" style="typo_Sic" xmlns="http://www.tei-c.org/ns/1.0">-->
    <xsl:apply-templates/>
  <!--</seg>-->
  <xsl:if test="@reason">
    <note xmlns="http://www.tei-c.org/ns/1.0" place="foot" type="corr">
      <xsl:attribute name="n">
        <xsl:value-of select="count(preceding::tei:choice[tei:corr]) + count(preceding::tei:surplus)+ 1"/>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="@reason='répété' and count(descendant::tei:w) gt 1">
          <xsl:text>Mots répétés.</xsl:text>
        </xsl:when>
        <xsl:when test="@reason='répété' and count(descendant::tei:w) le 1">
          <xsl:text>Mot répété.</xsl:text>
        </xsl:when>
        <xsl:when test="@reason='phrase_déplacée'">
          <xsl:text>Phrase déplacée.</xsl:text>
        </xsl:when>
        <xsl:when test="@reason='superflu' and count(descendant::tei:w) gt 1">
          <xsl:text>Mots superflus.</xsl:text>
        </xsl:when>
        <xsl:when test="@reason='superflu' and count(descendant::tei:w) le 1">
          <xsl:text>Mot superflu.</xsl:text>
        </xsl:when>
      </xsl:choose>
    </note>
  </xsl:if>
  <xsl:call-template name="spacing-dipl"/>
</xsl:template>

  <!--<xsl:template match="tei:note[@type='sic']"></xsl:template>
  
  <xsl:template match="tei:note[not(@type='sic')]">
    
  <xsl:variable name="nfoot">
    <xsl:number count="tei:note[@type='philo']" format="1" from="tei:body" level="any"/>
  </xsl:variable>
    <xsl:variable name="ncorr">
    <xsl:number count="tei:note[@type='corr']" format="a" from="tei:body" level="any"/>
  </xsl:variable>

      <note type="{@type}" place="{@place}">
        <xsl:if test="./@resp">
          <xsl:attribute name="resp">
            <xsl:value-of select="./@resp"/>
          </xsl:attribute>
        </xsl:if>
          <xsl:if test="./@xml:id">
          <xsl:attribute name="xml:id">
            <xsl:value-of select="./@xml:id"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:attribute name="n">
          <xsl:if test="@type='philo'">
            <xsl:value-of select="$nfoot"/>
          </xsl:if>
          <xsl:if test="@type='sic'">
            <xsl:value-of select="$ncorr"/>
          </xsl:if>
        </xsl:attribute>
        <xsl:attribute name="style">
          <xsl:if test="@type='philo'">
            <xsl:value-of select="'txt_Note'"/>
          </xsl:if>
          <xsl:if test="@type='corr'">
            <xsl:value-of select="'txt_Note_corr'"/>
          </xsl:if>
        </xsl:attribute>
          <xsl:apply-templates/>
        </note>
    <xsl:call-template name="spacing-dipl"/>
</xsl:template>
-->
  
  <xsl:template match="tei:note">
    <xsl:call-template name="spacing-dipl"/>
  </xsl:template>

  <xsl:template name="spacing-dipl">
    <xsl:choose>
      <xsl:when test="@aggl='fgmt'"/>
      <xsl:when test="@aggl='aggl'"/>
      <xsl:when test="@rend='elision'"/>
      <xsl:when test="ancestor::tei:w"/>
      <xsl:when test="following::tei:w[1][matches(@dipl,'^$')]"/>
      <xsl:when test="following::tei:w[1][matches(@dipl,'^\s*[.,)\]]+\s*$')]"/>	
      <xsl:when test="following-sibling::node()[1][self::text()][matches(.,'^\s*[.,)\]]+\s*$')]"/>
      <xsl:when test="matches(@dipl,'^$') and (preceding-sibling::*[1][self::tei:lb] or (parent::tei:p and position()=1))"></xsl:when>
      <xsl:when test="matches(@dipl,'^\s*[(\[]+$|\w(''|’)\s*$')"></xsl:when>
      <xsl:when test="position()=last() and (ancestor::tei:choice or ancestor::tei:supplied[not(@rend='multi_s') or parent::tei:sic] or ancestor::tei:surplus)"></xsl:when>
      <xsl:when test="following-sibling::*[1][self::tei:note]"></xsl:when>
      <xsl:when test="following::tei:w[1][matches(@dipl,'^\s*[:;!?]+\s*$')]">
        <xsl:text>&#xa0;</xsl:text>
      </xsl:when>
      <xsl:when test="following::tei:w[1][matches(@dipl,'^\s*&#xa0;[:;!?»]+\s*$')]">
      </xsl:when>
      <xsl:when test="matches(@dipl,'«')"><xsl:text>&#xa0;</xsl:text></xsl:when>
      <xsl:otherwise>
        <xsl:text> </xsl:text>
      </xsl:otherwise>
    </xsl:choose>                
  </xsl:template>

</xsl:stylesheet> 