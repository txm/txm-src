<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                exclude-result-prefixes="#all"
                xmlns="http://www.tei-c.org/ns/1.0"
                xmlns:edate="http://exslt.org/dates-and-times" 
                xmlns:me="http://www.menota.org/ns/1.0" 
                xmlns:bfm="http://bfm.ens-lsh.fr/ns/1.0" 
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
                xmlns:xi="http://www.w3.org/2001/XInclude" 
                xmlns:txm="http://textometrie.org/1.0"
                >

<xsl:output method="xml" encoding="utf-8" indent="no"/>

<!-- Copie l'ensemble des éléments, leurs attributs et leur contenu -->

<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="tei:appInfo"/>

<xsl:template match="tei:classDecl"/>

<xsl:template match="tei:encodingDesc">
    <encodingDesc><p> </p></encodingDesc>
</xsl:template>


<!-- Traitement des espaces -->

<xsl:strip-space elements="*"/>
<xsl:preserve-space elements="tei:teiHeader"/>
<xsl:preserve-space elements="tei:note"/>

<!-- Propose via une erreur jaune la selection du langage -->

<xsl:template match="tei:language">
    <language>
        <xsl:attribute name="ident">##-##</xsl:attribute>
            <xsl:apply-templates/>
    </language>
</xsl:template>

<xsl:template match="tei:titlePart">
    <xsl:variable name="form">
        <xsl:value-of select="descendant::txm:form"/>
    </xsl:variable>
    <titlePart style="T_3_Article" type="main">
        <xsl:value-of select="$form"/>
        <xsl:apply-templates/>
    </titlePart>
</xsl:template>


<xsl:template match="tei:w">
    <xsl:variable name="aggl">
        <xsl:value-of select="./txm:ana[@type='#aggl']"/>
    </xsl:variable>
    <xsl:variable name="dipl">
        <xsl:value-of select="./txm:ana[@type='#dipl']"/>
    </xsl:variable>
    <xsl:variable name="pos">
        <xsl:value-of select="./txm:ana[@type='#pos']"/>
    </xsl:variable>
    <!--<xsl:variable name="aggl">
        <xsl:choose>
            <xsl:when test="matches(./txm:ana[@type='#rend'],'aggl|degl|fgmt')">
                <xsl:value-of select="./txm:ana[@type='#rend']"/>
            </xsl:when>
            <xsl:otherwise>no</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>-->
    <!--<xsl:variable name="form">
        <xsl:value-of select="./txm:form"/>
    </xsl:variable>-->
    <xsl:choose>
        <xsl:when test="ancestor::tei:titlePart"/>
        <xsl:otherwise>
            <w dipl="{$dipl}" aggl="{$aggl}" pos="{$pos}">
                <xsl:apply-templates select="txm:form"/>
            </w>
        </xsl:otherwise>
    </xsl:choose>
    
</xsl:template>

<xsl:template match="txm:form">
    <xsl:apply-templates/>
</xsl:template>

<!--<xsl:template match="tei:w">
    <xsl:variable name="aggl">
        <xsl:value-of select="./txm:ana[@type='#aggl']"/>
    </xsl:variable>
    <xsl:variable name="dipl">
        <xsl:value-of select="./txm:ana[@type='#dipl']"/>
    </xsl:variable>
    <xsl:variable name="form">
        <xsl:value-of select="./txm:form"/>
    </xsl:variable> 
    <xsl:variable name="lb">
        <copy-of select="./txm:form/tei:lb"/>
    </xsl:variable>
    <xsl:choose>
        <xsl:when test="ancestor::tei:titlePart"/>
        <xsl:when test="./txm:form/tei:lb">
            <w dipl="{$dipl}" aggl="{$aggl}">
                <xsl:value-of select="$form"/>
                <xsl:value-of select="$lb"/>
            </w>
        </xsl:when>
        <xsl:otherwise>
            <w dipl="{$dipl}" aggl="{$aggl}">
                <xsl:value-of select="./$form"/>
            </w>
        </xsl:otherwise>
    </xsl:choose>
    
</xsl:template>-->

<!--<xsl:template match="tei:lb[@break]">
    <lb n="@n"></lb>
</xsl:template>-->

</xsl:stylesheet>