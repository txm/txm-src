<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
       xmlns:edate="http://exslt.org/dates-and-times"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                xmlns:me="http://www.menota.org/ns/1.0"
                xmlns:bfm="http://bfm.ens-lsh.fr/ns/1.0"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                exclude-result-prefixes="#all"
                xmlns="http://www.tei-c.org/ns/1.0">

<xsl:output method="xml" encoding="utf-8" indent="no"/>

<xsl:template match="@*|*|text()|processing-instruction()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>
    
    <xsl:template match="comment()"/>

<xsl:strip-space elements="*"/>
<xsl:preserve-space elements="tei:teiHeader"/>



<xsl:template match="tei:teiHeader">
	<xsl:copy>
		<xsl:apply-templates/>
		<!-- <encodingDesc>
			<projectDesc>
				<p>Informations sur l'encodage.</p>
			</projectDesc>
		</encodingDesc>
		<revisionDesc>
			<change>
				<xsl:attribute name="when">##/##/####</xsl:attribute>
				<xsl:attribute name="who">réviseur</xsl:attribute>
			<xsl:text>Nature de l'intervention</xsl:text>
			</change>
		</revisionDesc> -->
	</xsl:copy>
</xsl:template>

<xsl:template match="tei:editionStmt/child::tei:ab"/>


<xsl:template match="tei:editionStmt">
	<editionStmt>
		<edition>
			<date>
				<xsl:apply-templates/>
			</date>
		</edition>
	</editionStmt>
</xsl:template>

<xsl:template match="tei:author">
	<author>
		<name>
			<xsl:value-of select="tei/author"/>
			<xsl:apply-templates/>
		</name>
	</author>
</xsl:template>

<xsl:template match="tei:language">
	<language>
    	<xsl:attribute name="ident">##-##</xsl:attribute>
            <xsl:apply-templates/>
	</language>
</xsl:template>

<xsl:template match="tei:text">
	<text>
	<xsl:attribute name="xml:id">text
	</xsl:attribute>
	<xsl:apply-templates/>
</text>
</xsl:template>

<xsl:template match="tei:front">

<front>

	<titlePage>
    <docTitle>
      <titlePart type="main" style="T_3_Article">
        <xsl:apply-templates select="//tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type='main']/node()"/>
      </titlePart>
      <xsl:choose>
        <xsl:when test="//tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type!='main']">
        <xsl:variable name="titleType" select="//tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type!='main']/@type"/>
          <titlePart>
            <xsl:attribute name="type" select="$titleType"/>
            <xsl:apply-templates select="//tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type!='main']/node()"/>
          </titlePart>
        </xsl:when>
      </xsl:choose>
    </docTitle>
		<byline>
        <xsl:for-each select="//tei:teiHeader//tei:author">
    			<docAuthor style="txt_auteur">
    				<xsl:value-of select="child::tei:name"/>
    			</docAuthor>
          <xsl:if test="child::tei:affiliation">
              <xsl:apply-templates select="child::tei:affiliation"/>
          </xsl:if>
        </xsl:for-each>
		</byline>
	</titlePage>
	
</front>
</xsl:template>


<xsl:template match="tei:div">
    <xsl:text>&#xa;</xsl:text>
    <xsl:copy>
        <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
</xsl:template>

<xsl:template match="tei:div[@type='div2']/tei:head">
    <xsl:copy>
        <xsl:apply-templates select="@*"/>
        <xsl:attribute name="style">Titre_1</xsl:attribute>
        <xsl:apply-templates/>
    </xsl:copy>
</xsl:template>
    

 <xsl:template match="tei:p">
    
        <xsl:text>&#xa;&#xa;</xsl:text>
        <p style="txt_vers">
            <!-- <p style="txt_vers"> option mise en page en vers -->
            <!-- <p style="txt_Normal"> option mise en page normale -->
              <xsl:if test="@n"><seg style="typo_pagenum">
                  <xsl:text>§ </xsl:text>
                  <xsl:value-of select="@n"/>
              </seg></xsl:if>
            <xsl:text>&#xa;</xsl:text>
            <xsl:apply-templates/>
        </p>
    
</xsl:template>

 <!--<xsl:template match="tei:note">
    <xsl:copy-of select="."/>
    <xsl:call-template name="spacing"/>
</xsl:template> -->

<!-- <xsl:template match="tei:p[@style='txt_Normal']">
  <xsl:choose>
    
    <xsl:otherwise>
      <p style="txt_Normal">
        <xsl:attribute name="n">
          <xsl:value-of select="$nume_page"/>
        </xsl:attribute>
      <xsl:copy-of select="."/>
      </p>
    </xsl:otherwise>
  </xsl:choose> 
  
</xsl:template> -->

<xsl:template match="tei:ab[matches(@type,'bloc')]">
    <xsl:copy>
        <xsl:apply-templates select="@*"/>
            <xsl:attribute name="style">txt_Normal</xsl:attribute>
            <xsl:if test="@n">
            <xsl:value-of select="concat('[',@n,'.]')"/>
                 <xsl:if test="not(matches(child::text()[1],'^\s+'))">
                     <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:if>
        <xsl:apply-templates/>
    </xsl:copy>
</xsl:template>

<xsl:template match="tei:w">
    <!--<xsl:variable name="contenuW">
        <xsl:value-of select="."/>
    </xsl:variable>
    <xsl:choose>
        <xsl:when test="@aggl='fgmt'">
            <seg type="dipl" rend="fgmt">
                <xsl:value-of select="."/>
            </seg>
          <!-\-  <xsl:if test="child::tei:lb">
                <lb n="{./tei:lb/@n}"></lb>
            </xsl:if> -\->
        </xsl:when>
         <xsl:when test="@aggl='aggl'">
            <seg type="dipl" rend="aggl">
                <xsl:value-of select="."/>
            </seg>
           <!-\-  <xsl:if test="child::tei:lb">
                <lb n="{./tei:lb/@n}"></lb>
            </xsl:if> -\->
        </xsl:when>
        <xsl:when test="@dipl=$contenuW">
            <xsl:apply-templates/>
          <!-\-  <xsl:if test="child::tei:lb">
                <lb n="{./tei:lb/@n}"></lb>
            </xsl:if> -\->
        </xsl:when>
        <xsl:when test="@dipl=''">
            <seg type="dipl" rend="noPunct">
                <xsl:value-of select="."/>
            </seg>
             <!-\-  <xsl:if test="child::tei:lb">
                <lb n="{./tei:lb/@n}"></lb>
            </xsl:if> -\->
        </xsl:when>
        <xsl:otherwise>
            <seg type="dipl">
                <xsl:attribute name="rend">
                    <xsl:call-template name="valeur_rend"/>
                </xsl:attribute>
                <xsl:value-of select="."/>
            </seg>
            <!-\-  <xsl:if test="child::tei:lb">
                <lb n="{./tei:lb/@n}"></lb>
            </xsl:if> -\->
        </xsl:otherwise>
    </xsl:choose>-->
    <xsl:copy>
        <xsl:apply-templates select="@*"/>
        <xsl:if test="matches(.,'.+[''’]$')">
            <xsl:attribute name="rend">elision</xsl:attribute>
            <!--<xsl:attribute name="aggl">elision</xsl:attribute>-->
        </xsl:if>
        <xsl:apply-templates/>
    </xsl:copy>
    <!--<xsl:call-template name="spacing"/>-->
</xsl:template>

<!--<xsl:template match="tei:seg/tei:ex">
    <seg>
        <xsl:attribute name="type">ex</xsl:attribute>
        <xsl:apply-templates/>
    </seg>
</xsl:template>-->

<!--<xsl:template match="tei:del">
    <seg type="del">
        <xsl:attribute name="rend">
            <xsl:call-template name="valeur_del"/>
        </xsl:attribute>
        <xsl:value-of select="tei:del"/>
        <xsl:apply-templates/>
    </seg>
</xsl:template>-->

<xsl:template match="tei:add">
    <seg type="add">
        <xsl:attribute name="rend">
            <xsl:call-template name="valeur_add"/>
        </xsl:attribute>
        <xsl:value-of select="tei:add"/>
        <xsl:apply-templates/>
    </seg>
</xsl:template>

<xsl:template match="tei:name">
    <name>
        <xsl:attribute name="type">dipl</xsl:attribute>
        <xsl:attribute name="rend">
            <xsl:value-of select="child::tei:w/@dipl"/>
        </xsl:attribute>
        <xsl:value-of select="tei:name"/>
        <xsl:apply-templates/>
    </name>
</xsl:template>

<!--<xsl:template match="tei:g">
    <hi style="typo_Initiale" rend="{@rend}">
        <xsl:value-of select="."/>
    </hi>
</xsl:template>-->

<xsl:template match="tei:cb">
    <xsl:text>&#xa;</xsl:text>
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
        </xsl:copy>
</xsl:template>

<xsl:template match="tei:supplied">
    <seg type="supplied" style="typo_supplied" xmlns="http://www.tei-c.org/ns/1.0">
        <xsl:apply-templates select="@*"/>
        <xsl:if test="not(matches(@rend,'middle|final'))">
        <xsl:text>[</xsl:text>
    </xsl:if>
        <xsl:apply-templates/>
    <xsl:if test="not(matches(@rend,'initial|middle'))">
        <xsl:text>]</xsl:text>
    </xsl:if>
    </seg>
    <xsl:call-template name="spacing"/>
</xsl:template>

<!-- patch pour la version diplomatique -->
<xsl:template match="tei:choice/tei:sic">
    <xsl:copy>
        <xsl:apply-templates select="@*"/>
        <w xmlns="http://www.tei-c.org/ns/1.0" dipl="{@dipl}"><xsl:apply-templates/></w>
    </xsl:copy>
</xsl:template>

<xsl:template match="tei:gap">
    <xsl:text>[…]</xsl:text>
</xsl:template>
    
    <xsl:template match="tei:emph">
        <xsl:if test="descendant::tei:g[@type='initiale']">
            <seg type="lettrine" style="typo_lettrine" xmlns="http://www.tei-c.org/ns/1.0">
                <xsl:apply-templates select="descendant::tei:g/node()"/>
            </seg>
        </xsl:if>
        <hi style="typo_gras" xmlns="http://www.tei-c.org/ns/1.0"><xsl:apply-templates/></hi>
    </xsl:template>
    
    <xsl:template match="tei:g[@type='initiale']">
        <xsl:choose>
            <xsl:when test="ancestor::tei:emph"/>
            <xsl:otherwise><seg type="lettrine" style="typo_lettrine" xmlns="http://www.tei-c.org/ns/1.0">
                <xsl:apply-templates/>
            </seg></xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="tei:hi">
        <xsl:choose>
            <xsl:when test="not(child::node())"></xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@*|node()"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

<xsl:template match="tei:hi/@rend">
    <xsl:choose>
        <xsl:when test="matches(.,'italic')">
            <xsl:attribute name="style">typo_Italique</xsl:attribute>
        </xsl:when>
        <xsl:when test="matches(.,'sup')">
            <xsl:attribute name="style">typo_Exposant</xsl:attribute>
        </xsl:when>
        <xsl:when test="matches(.,'smallcaps')">
            <xsl:attribute name="style">typo_SC</xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
            <xsl:copy-of select="."/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

 <xsl:template match="tei:q">
     <xsl:if test="@rend='tiret'">
         <!-- pour la traduction ? -->
         <xsl:text>&#xa;—&#xa0;</xsl:text>
     </xsl:if>
        <xsl:apply-templates/>
</xsl:template>
                
<xsl:template match="tei:foreign[not(@rend)]">
    <hi xmlns="http://www.tei-c.org/ns/1.0" style="typo_Italic">
        <xsl:apply-templates/>
    </hi>
</xsl:template>

<xsl:template match="tei:s">
    <!-- on supprime les phrases pour Métopes -->
    <xsl:apply-templates/>
</xsl:template>

<!--<xsl:template match="tei:surplus">
    <xsl:copy>
        <!-\-<xsl:text>(</xsl:text>-\->
        <xsl:apply-templates/>
        <!-\-<xsl:text>)</xsl:text>-\->
    </xsl:copy>
    <note type="philo" place="foot">
        <xsl:choose>
            <xsl:when test="@unit='chars'">Lettre</xsl:when>
            <xsl:when test="@unit='sentence'">Phrase</xsl:when>
            <xsl:otherwise>Mot</xsl:otherwise>
        </xsl:choose>
        <xsl:if test="not(@quantity='1')">s</xsl:if>
        <xsl:text> répété</xsl:text>
        <xsl:if test="matches(@unit,'chars|sentence')">e</xsl:if>
        <xsl:if test="not(@quantity='1')">s</xsl:if>
        <xsl:text>.</xsl:text>
    </note>
    <xsl:call-template name="spacing"/>
</xsl:template>-->
    
<!--<xsl:template match="tei:space[@type='degl-fgmt']">
    <seg type="degl-fgmt">
        <xsl:value-of select="current()"/>
    </seg>
</xsl:template>
<xsl:template match="tei:space[@type='degl']">
    <seg type="degl">
        <xsl:value-of select="current()"/>
    </seg>
</xsl:template>-->

<!--<xsl:template match="tei:sic">
    <note place="foot" type="sic">
        <xsl:copy-of select="descendant::tei:*"/>
    </note>
</xsl:template>-->

<!--<xsl:template match="tei:note[@place='inline' or @type='inline']">
    <ab style="txt_inline" type="note_inline">
        <xsl:value-of select="replace(.,'\s*\n\s*',' ')"/>
        <!-\-<xsl:apply-templates/>-\->
    </ab>
    <xsl:text>&#xa;</xsl:text>
        
</xsl:template>
-->    
    <xsl:template match="tei:note/@place">
        <xsl:attribute name="place">
            <xsl:choose>
                <xsl:when test="matches(.,'^app$')">foot</xsl:when>
                <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="tei:choice[tei:corr]">
        <xsl:copy>
            <xsl:apply-templates/>
        </xsl:copy>
        <xsl:if test="not(following::*[1][self::tei:note])">
            <note xmlns="http://www.tei-c.org/ns/1.0" place="foot" type="sic" resp="#NK">Le manuscrit a <hi xmlns="http://www.tei-c.org/ns/1.0" style="typo_Italique"><xsl:value-of select="tei:sic"/></hi>.</note>
        </xsl:if>
        <xsl:call-template name="spacing"></xsl:call-template>
    </xsl:template>

<!-- transféré à l'étape 3 -->
<!--<xsl:template match="tei:lb"/>-->


<!--

<xsl:template match="tei:lb[not(parent::tei:w)]">
    <xsl:if test="preceding-sibling::tei:w or not(matches(preceding-sibling::text()[1],'\n\s*$')) or preceding-sibling::*[1][self::tei:q] and not(parent::tei:w)">
        <lb n="{@n}"></lb>
    </xsl:if>
    <xsl:if test="matches(@n,'[05a-z]$')">
        <num xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:value-of select="@n"/>
        </num>
        <xsl:text>&#x9;</xsl:text>
    </xsl:if>
</xsl:template> -->

 

<xsl:template match="tei:c">
    <!--<seg type="c">
        <xsl:value-of select="current()"/>
    </seg>-->
    <xsl:apply-templates/>
</xsl:template>
    


<!--<xsl:template match="tei:p[@style='txt_Normal']">
  <xsl:choose>
    <xsl:when test="child::tei:p/child::tei:p[@style='txt_pagenum']">
      <pb/>
        <p style="txt_pagenum">
            <xsl:value-of select="child::tei:p/child::tei:p[@style='txt_pagenum']"/>
        </p>
    </xsl:when>
    <xsl:otherwise>
   
      <xsl:copy-of select="."/>

    </xsl:otherwise>
  </xsl:choose>
  
</xsl:template>-->


<!--<xsl:template match="tei:pb">
    <xsl:if test="following::tei:lb[1][@n]">
        <xsl:text>&#xa;</xsl:text>
    </xsl:if>
        <p style="txt_pagenum" xmlns="http://www.tei-c.org/ns/1.0">
        <xsl:value-of select="concat('&lt;',@n,'>')"/>
        </p>
    <xsl:choose>
    <xsl:when test="following::tei:lb[1][@n] or following-sibling::*[1][self::tei:div or self::tei:ap or self::tei:p]">
        <xsl:text>&#xa;</xsl:text>
          </xsl:when>
        <xsl:when test="child::tei:p/child::tei:p[@style='txt_pagenum']">
      <pb/>
          <p style="txt_pagenum">
            <xsl:value-of select="child::tei:p/child::tei:p[@style='txt_pagenum']"/>
          </p>
    </xsl:when>
    <xsl:when test="matches(following::text()[1],'^\w')">
            <xsl:text> </xsl:text>
        </xsl:when>
    </xsl:choose>
</xsl:template>-->
    
    
    <xsl:template match="text()[ancestor::tei:note]">
        <xsl:value-of select="replace(.,' *&#xA; *',' ')"/>
    </xsl:template>

<!-- Named Templates -->
    
    <xsl:template name="spacing">
        <xsl:choose>
            <xsl:when test="ancestor::tei:w"/>
            <xsl:when test="following::tei:w[1][matches(.,'^\s*[.,)\]]+\s*$')]"/>			
            <xsl:when test="matches(.,'^\s*[(\[]+$|\w(''|’)\s*$')"></xsl:when>
            <xsl:when test="position()=last() and (ancestor::tei:choice or ancestor::tei:supplied[not(@rend='multi_s')])"></xsl:when>
            <xsl:when test="following-sibling::*[1][self::tei:note]"></xsl:when>
            <xsl:when test="following::tei:w[1][matches(.,'^\s*[:;!?]+\s*$')]">
                <xsl:text>&#xa0;</xsl:text>
            </xsl:when>
            <xsl:when test="following::tei:w[1][matches(.,'^\s*&#xa0;[:;!?»]+\s*$')]">
            </xsl:when>
            <xsl:when test="matches(.,'«')"><xsl:text>&#xa0;</xsl:text></xsl:when>
            <xsl:otherwise>
                <xsl:text> </xsl:text>
            </xsl:otherwise>
        </xsl:choose>                
    </xsl:template>
    

<xsl:template name="valeur_add">
    <xsl:value-of select="@place"/>
</xsl:template>

<!--<xsl:template name="valeur_del">
    <xsl:value-of select="@rend"/>
</xsl:template>-->

<xsl:template name="valeur_rend">
    <xsl:value-of select="@dipl"/>
</xsl:template>


</xsl:stylesheet>