<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:txm="http://textometrie.org/1.0"
	exclude-result-prefixes="tei edate" version="2.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />

	<xd:doc type="stylesheet">
		<xd:short>
			This stylesheet prepares TextGrid TEI corpus for TXM xml/w import module
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2014, CNRS / ICAR (CACTUS)</xd:copyright>
	</xd:doc>



	<!-- Partie générale : on copie les éléments, les attributs, le texte, les 
		commentaires, on supprime mes prrocessing intstructions -->
	<!-- + nommage auto des fichiers -->

	<xsl:variable name="filedir">
		<xsl:analyze-string select="document-uri(.)"
			regex="^(.*)/([^/]+)$">
			<xsl:matching-substring>
				<xsl:value-of select="regex-group(1)"></xsl:value-of>
			</xsl:matching-substring>
		</xsl:analyze-string>
	</xsl:variable>


	<xsl:template match="/">
		<note>See output files in the OUT directory</note>
		<xsl:for-each select="descendant::tei:TEI">
			<xsl:variable name="author">
				<xsl:value-of
					select="tei:teiHeader[1]/tei:fileDesc[1]/tei:sourceDesc[1]/tei:biblFull[1]/tei:titleStmt[1]/tei:author[1]"></xsl:value-of>
			</xsl:variable>
			<xsl:variable name="authorNorm">
				<xsl:value-of select="replace($author,'[, ]','')" />
			</xsl:variable>
			<xsl:variable name="title">
				<xsl:value-of
					select="tei:teiHeader[1]/tei:fileDesc[1]/tei:titleStmt[1]/tei:title[1]"></xsl:value-of>
			</xsl:variable>
			<xsl:variable name="titleNorm">
				<xsl:value-of select="replace($title,'[, \[\]]','')" />
			</xsl:variable>
			<xsl:variable name="creationDate">
				<xsl:choose>
					<xsl:when
						test="tei:teiHeader[1]/tei:profileDesc[1]/tei:creation[1]/tei:date[1]/@when">
						<xsl:value-of
							select="tei:teiHeader[1]/tei:profileDesc[1]/tei:creation[1]/tei:date[1]/@when" />
					</xsl:when>
					<xsl:when
						test="tei:teiHeader[1]/tei:profileDesc[1]/tei:creation[1]/tei:date[1]/@notAfter">
						<xsl:value-of
							select="tei:teiHeader[1]/tei:profileDesc[1]/tei:creation[1]/tei:date[1]/@notAfter" />
					</xsl:when>
					<xsl:when
						test="tei:teiHeader[1]/tei:profileDesc[1]/tei:creation[1]/tei:date[1]/@notBefore">
						<xsl:value-of
							select="tei:teiHeader[1]/tei:profileDesc[1]/tei:creation[1]/tei:date[1]/@notBefore" />
					</xsl:when>
					<xsl:otherwise>
						9999
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:result-document
				href="{$filedir}/out/{$creationDate}{$authorNorm}{$titleNorm}.xml">
				<xsl:copy>
					<xsl:copy-of select="@*" />
					<xsl:comment>
						teiHeafer deleted for TXM xml/w import module
					</xsl:comment>
					<xsl:element name="text"
						namespace="http://www.tei-c.org/ns/1.0">
						<xsl:attribute name="author"><xsl:value-of
							select="$author" /></xsl:attribute>
						<xsl:attribute name="title"><xsl:value-of
							select="$title" /></xsl:attribute>
						<xsl:attribute name="creationDate"><xsl:value-of
							select="$creationDate" /></xsl:attribute>
						<xsl:for-each select="tei:text/*">
							<xsl:copy-of select="." />
						</xsl:for-each>
					</xsl:element>
				</xsl:copy>
			</xsl:result-document>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>