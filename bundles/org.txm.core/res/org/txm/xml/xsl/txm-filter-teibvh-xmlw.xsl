<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:txm="http://textometrie.org/1.0"
	exclude-result-prefixes="tei edate" version="2.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />

	<xd:doc type="stylesheet">
		<xd:short>
			Cette feuille de style prépare les textes BVH à l'import BFM
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2012, CNRS / ICAR (ICAR3 LinCoBaTO)</xd:copyright>
	</xd:doc>


	<!-- !!! Enlever &#x2028 ou &#8232; dans 1600_BeroaldRich.xml !!! -->

	<!-- Modifications par AL le 2012-01-25 g dépalacé de "deleteAll" vers "deleteTag" 
		classe de balise "name" créée (geogName, placeName, persName et name) -->

	<!-- Modifications par LBe le 2012-02-17 Transformation automatique des 
		esperluettes dans les métadonnées Suppression des balises label et heraldry 
		Signed n'est plus transformé en ab opener est transformé en ab L'institution 
		et le dépôt sont récupérés pour le lieu de conservation -->

	<!-- Modifications par AL le 2012-02-20 rolename -> roleName suppression 
		des balises surname et forename -->

	<!-- Modifications par LBe le 2012-03-09 Automatisation du nommage des sorties -->

	<!-- Modifications par LBe le 2013-03-05 Ajout des nouveaux descripteurs 
		pour Presto -->

	<!-- Modifications par LBe le 2014-01-10 -->

	<!-- Modifications par AL le 2014-01-13 - Fusion des versions BVH et BFM 
		- Détectioon des mots coupés (detextBrokenWord) -->

	<!-- Listes des balises par type de traitement (les templates sont plus 
		bas) -->

	<!-- balises à supprimer avec leur contenu -->
	<xsl:template
		match="tei:figDesc|tei:figure|tei:fw
    |tei:space">
		<xsl:call-template name="deleteAll" />
	</xsl:template>

	<!-- balises à supprimer, contenu à conserver -->
	<xsl:template
		match="tei:castItem|tei:castGroup[ancestor::tei:castList]
    |tei:castList[ancestor::tei:p]|tei:cell|tei:cit|tei:date
    |tei:forename|tei:g|tei:head[parent::tei:list]|tei:heraldry|tei:hi|tei:item|tei:label|tei:lem
    |tei:list[ancestor::tei:p|ancestor::tei:ab]|tei:lg[child::tei:lg|ancestor::tei:sp]
    |tei:p[ancestor::tei:sp]|tei:performance
    |tei:ref|tei:role|tei:roleDesc
    |tei:row|tei:salute|tei:signed[ancestor::tei:closer]|tei:speaker|tei:stage[ancestor::tei:p|ancestor::tei:ab]
    |tei:surname|tei:table[ancestor::tei:p]|tei:term|tei:unclear">
		<xsl:call-template name="deleteTag" />
	</xsl:template>

	<!-- balises à transformer en notes -->
	<!-- <xsl:template match="tei:bibl[not(ancestor::tei:note)] |tei:rdg[parent::tei:app 
		and not(ancestor::tei:note)]"> <xsl:call-template name="makeNote"/> </xsl:template> -->
	<!-- balises de noms à indexer -->

	<xsl:template
		match="tei:name
    |tei:geogName
    |tei:persName
    |tei:placeName
    |tei:roleName">
		<xsl:call-template name="makeName" />
	</xsl:template>

	<!-- à transformer en W -->

	<xsl:template
		match="tei:abbr[not(parent::tei:choice)]
    |tei:num[matches(.,'.')]">
		<xsl:call-template name="makeWord" />
	</xsl:template>

	<!-- à transformer en AB typé -->

	<xsl:template
		match="tei:byline|tei:lg[not(child::tei:lg or ancestor::tei:sp)]|tei:list[not(ancestor::tei:p|ancestor::tei:ab)]
    |tei:closer|tei:opener|tei:sp|tei:castGroup[not(ancestor::tei:castList)]|tei:castList[not(ancestor::tei:p)]
    |tei:signed[not(ancestor::tei:closer)]
    |tei:stage[not(ancestor::tei:p|ancestor::tei:ab)]
    |tei:table[not(ancestor::tei:p)]
    |tei:trailer">
		<xsl:call-template name="makeAb" />
	</xsl:template>

	<!-- à transformer en DIV typé -->

	<xsl:template match="tei:set">
		<xsl:call-template name="makeDiv" />
	</xsl:template>

	<!-- Partie générale : on copie les éléments, les attributs, le texte, les 
		commentaires, on supprime mes prrocessing intstructions -->
	<!-- + nommage auto des fichiers -->

	<xsl:variable name="filedir">
		<xsl:analyze-string select="document-uri(.)"
			regex="^(.*)/([^/]+)$">
			<xsl:matching-substring>
				<xsl:value-of select="regex-group(1)"></xsl:value-of>
			</xsl:matching-substring>
		</xsl:analyze-string>
	</xsl:variable>


	<xsl:template match="/">
		<xsl:variable name="compare1" select="//tei:TEI/@xml:id" />
		<!--<xsl:variable name="compare2" select="document('Nommage_Output_TXM.xml')//name/@id"/> -->
		<xsl:variable name="compare2" select="$nommage//name/@id" />
		<xsl:variable name="name"
			select="document('Nommage_Output_TXM.xml')//name" />
		<xsl:if test="$compare1 = $compare2">
			<xsl:result-document
				href="{concat($filedir,'/out/',$name[@id=$compare1], '.xml')}">
				<xsl:apply-templates />
			</xsl:result-document>
		</xsl:if>
	</xsl:template>



	<xsl:template match="*">
		<xsl:if
			test="descendant::tei:pb and (parent::tei:text or (position() &gt; 2))">
			<!-- on déplace les pb initiaux avant la balise de l'élément le plus haut -->
			<xsl:call-template name="raisePb" />
		</xsl:if>
		<xsl:choose>
			<xsl:when test="namespace-uri()=''">
				<xsl:element namespace="http://www.tei-c.org/ns/1.0"
					name="{local-name(.)}">
					<xsl:apply-templates
						select="*|@*|processing-instruction()|comment()|text()" />
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates
						select="*|@*|processing-instruction()|comment()|text()" />
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template
		match="@*|comment()|text()|processing-instruction()">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="@instant[.='false']"></xsl:template>
	<xsl:template match="@status[.='unremarkable']"></xsl:template>

	<!-- <xsl:template match="text()"> <xsl:analyze-string select="." regex="&amp;"> 
		<xsl:matching-substring><expan xmlns="http://www.tei-c.org/ns/1.0">et</expan></xsl:matching-substring> 
		<xsl:non-matching-substring> <xsl:value-of select="."/> </xsl:non-matching-substring> 
		</xsl:analyze-string> </xsl:template> <xsl:template match="processing-instruction()"/> -->

	<!-- On supprime teiHeader -->

	<xsl:template match="tei:teiHeader">
		<!-- <xsl:copy-of select="."/> -->
	</xsl:template>

	<!-- On supprime les balises et tout ce qu'elles contiennent pour les éléments 
		hors texte à ignorer lors de l'import -->

	<xsl:template name="deleteAll" />

	<!-- On supprime les balises non essentielles pour l'exploitation textométrique 
		et susceptibles de poser des problèmes au tokeniseur, mais on garde leur 
		contenu qui fait partie du matériau textuel -->

	<xsl:template name="deleteTag">
		<xsl:apply-templates />
	</xsl:template>

	<!-- on met en note les éléments hors texte qu'on aimerait garder pour l'édition, 
		etc. -->

	<!-- <xsl:template name="makeNote"> <note type="{local-name()}" xmlns="http://www.tei-c.org/ns/1.0"> 
		<xsl:value-of select="."/> </note> </xsl:template> -->
	<!-- On supprime toutes les balises à l'intérieur des notes -->
	<!-- <xsl:template match="tei:note"> <xsl:copy> <xsl:value-of select="."></xsl:value-of> 
		</xsl:copy> </xsl:template> -->

	<!-- On transforme en DIV typés les balises de niveau de division textuelle 
		(pouvant contenir des p) -->

	<xsl:template name="makeDiv">
		<div type="{local-name()}" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:copy-of select="@*" />
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<!-- On transforme en bloc anonyme les balises de niveau équivalent au paragraphe -->

	<xsl:template name="makeAb">
		<ab type="{local-name()}" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:copy-of select="@*" />
			<xsl:apply-templates />
		</ab>
	</xsl:template>

	<!-- on pré-tokenise les éléments équivalents à un mot -->

	<xsl:template name="makeWord">
		<xsl:variable name="wordType">
			<xsl:choose>
				<xsl:when test="@type">
					<xsl:value-of select="concat(local-name(),'-',@type)" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="local-name()"></xsl:value-of>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<w type="{$wordType}" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates />
		</w>
	</xsl:template>

	<xsl:template name="makeName">
		<xsl:variable name="nameType">
			<xsl:choose>
				<xsl:when test="@type">
					<xsl:value-of select="concat(local-name(),'-',@type)" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="local-name()"></xsl:value-of>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<name type="{$nameType}" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates />
		</name>
	</xsl:template>

	<!-- on avant l'élément au début duquel ils se situent -->

	<xsl:template name="raisePb">
		<xsl:if test="descendant::tei:pb">
			<xsl:for-each
				select="child::*[1][descendant-or-self::tei:pb]">
				<xsl:choose>
					<xsl:when test="self::tei:pb">
						<xsl:comment>
							Pb remonté
						</xsl:comment>
						<xsl:copy>
							<xsl:apply-templates select="@*" />
							<xsl:if
								test="not(@n) and not(@facs) and following-sibling::tei:fw[1 or 2][@type='pageNum']">
								<xsl:attribute name="n"><xsl:value-of
									select="normalize-space(following-sibling::tei:fw[@type='pageNum'][1])" /></xsl:attribute>
							</xsl:if>
						</xsl:copy>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="raisePb"></xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>


	<xsl:template
		match="tei:pb[not(preceding-sibling::*) and not(parent::tei:text)]">
		<xsl:comment>
			Pb déplacé plus haut
		</xsl:comment>
	</xsl:template>

	<xsl:template match="tei:pb[preceding-sibling::*]">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:if
				test="not(@n) and not(@facs) and following-sibling::tei:fw[1 or 2][@type='pageNum']">
				<xsl:attribute name="n"><xsl:value-of
					select="normalize-space(following-sibling::tei:fw[@type='pageNum'][1])" /></xsl:attribute>
			</xsl:if>
		</xsl:copy>
	</xsl:template>

	<!-- les l sont transformés en lb -->

	<xsl:template match="tei:l">
		<lb xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:if test="@n">
				<xsl:attribute name="n"><xsl:value-of
					select="@n" /></xsl:attribute>
			</xsl:if>
		</lb>
		<xsl:apply-templates />
	</xsl:template>


	<!-- On met la ponctuation forte à l'extérieur de la balise foreign : Cet 
		élément sera toujours à l'intérieur d'une phrase. Attention au risque de 
		perdre les balises éventuelles à l'intérieur -->

	<xsl:template match="tei:foreign[matches(.,'[.!?]')]">
		<xsl:variable name="lang">
			<xsl:choose>
				<xsl:when test="@lang">
					<xsl:value-of select="@lang" />
				</xsl:when>
				<xsl:when test="@xml:lang">
					<xsl:value-of select="@xml:lang" />
				</xsl:when>
				<xsl:otherwise>
					xx
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:for-each select="child::node()">
			<xsl:choose>
				<xsl:when test="self::text()">
					<xsl:analyze-string select="."
						regex="([^.!?]*)([.?!]+)">
						<xsl:matching-substring>
							<foreign lang="{$lang}"
								xmlns="http://www.tei-c.org/ns/1.0">
								<xsl:analyze-string select="regex-group(1)"
									regex="&amp;">
									<xsl:matching-substring>
										<expan xmlns="http://www.tei-c.org/ns/1.0">et</expan>
									</xsl:matching-substring>
									<xsl:non-matching-substring>
										<xsl:value-of select="." />
									</xsl:non-matching-substring>
								</xsl:analyze-string>
							</foreign>
							<xsl:value-of select="regex-group(2)" />
							<xsl:text> </xsl:text>
						</xsl:matching-substring>
					</xsl:analyze-string>
				</xsl:when>
				<xsl:when test="self::*[not(descendant::text())]">
					<xsl:apply-templates />
				</xsl:when>
				<xsl:otherwise>
					<foreign lang="{$lang}" xmlns="http://www.tei-c.org/ns/1.0">
						<xsl:apply-templates select="." />
					</foreign>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<!-- Injection des métadonnées -->

	<!-- copie de la template créée pour le header BVH -->

	<xsl:template match="tei:text">
		<xsl:variable name="auteur">
			<xsl:choose>
				<xsl:when
					test="//tei:monogr/tei:author[1]/tei:persName/tei:forename">
					<xsl:value-of
						select="//tei:monogr/tei:author[1]/tei:persName/tei:forename" />
					&#160;
					<xsl:value-of
						select="//tei:monogr/tei:author[1]/tei:persName/tei:surname" />
				</xsl:when>
				<xsl:when test="//tei:monogr/tei:author[1]/tei:persName">
					<xsl:value-of
						select="//tei:monogr/tei:author[1]/tei:persName" />
				</xsl:when>
				<xsl:when test="//tei:monogr/tei:author">
					<xsl:value-of select="//tei:monogr/tei:author" />
				</xsl:when>
				<xsl:otherwise>
					anonyme
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="titre">
			<xsl:value-of
				select="translate(//tei:monogr/tei:title[@type='titre_court'],'&amp;', 'et')" />
		</xsl:variable>

		<text xmlns="http://www.tei-c.org/ns/1.0">
			<!-- id -->
			<xsl:variable name="compare1" select="//tei:TEI/@xml:id" />
			<xsl:variable name="compare2"
				select="document('Nommage_Output_TXM.xml')//name/@id" />
			<xsl:variable name="name"
				select="document('Nommage_Output_TXM.xml')//name" />
			<xsl:if test="$compare1 = $compare2">
				<xsl:attribute name="id">             
            <xsl:value-of select="$name[@id=$compare1]" />
          </xsl:attribute>
			</xsl:if>
			<!-- sigle -->
			<xsl:variable name="compare1" select="//tei:TEI/@xml:id" />
			<xsl:variable name="compare2"
				select="document('Nommage_Output_TXM.xml')//name/@id" />
			<xsl:variable name="name"
				select="document('Nommage_Output_TXM.xml')//name" />
			<xsl:if test="$compare1 = $compare2">
				<xsl:attribute name="nom">             
            <xsl:value-of select="$name[@id=$compare1]" />
          </xsl:attribute>
			</xsl:if>
			<!-- auteur -->
			<xsl:attribute name="auteur">
              <xsl:value-of select="$auteur" />
            </xsl:attribute>
			<!-- titre -->
			<xsl:attribute name="titre">
                <xsl:value-of select="$titre" />
            </xsl:attribute>
			<!-- date de composition -->
			<xsl:attribute name="date">
            <xsl:value-of
				select="//tei:monogr//tei:imprint/tei:date" />
          </xsl:attribute>
			<!-- forme -->
			<xsl:attribute name="forme">
                <xsl:choose>
                    <xsl:when
				test="//tei:TEI[@xml:id='B360446201_B343_1_tei' 
                      or @xml:id='B360446201_B343_2_tei'
                      or @xml:id='B360446201_THIA63_1_tei.xml'
                      or @xml:id='B372615206_1263_tei'
                      or @xml:id='B372615206_3436_tei'
                      or @xml:id='B372615206_4023_tei'
                      or @xml:id='B372615206_4040_tei'
                      or @xml:id='B372616101_3537_tei'
                      or @xml:id='B410186201_I65_tei'
                      or @xml:id='B410186201_LI139_tei'
                      or @xml:id='B693836101_346632_tei'
                      or @xml:id='B693836101_A489170_tei'
                      or @xml:id='B751041006_FR_1513_tei'
                      or @xml:id='B751131010_FR3370_suppl_tei'
                      or @xml:id='B751131011_Y2_251_tei'
                      or @xml:id='B751131011_Y22789_tei'
                      or @xml:id='B751131015_X1888_tei'
                      or @xml:id='B751131015_Z1918_tei'
                      or @xml:id='B759999999_Y2_2162_tei'
                      or @xml:id='B759999999_Y2_2164_tei'
                      or @xml:id='B861946101_DP1139_tei'
                      or @xml:id='XUVA_Gordon1578_L47_tei']">
                        <xsl:text>Prose</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:text>Vers</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
			<!-- domaine -->
			<xsl:attribute name="domaine">
               <xsl:text>Littéraire</xsl:text>
            </xsl:attribute>
			<!-- genre -->
			<xsl:attribute name="genre">
            <xsl:text>|</xsl:text><xsl:value-of
				select="translate(@type, '_', ' ')" /><xsl:text>|</xsl:text><xsl:value-of
				select="translate(@subtype, '_', ' ')" /><xsl:text>|</xsl:text>
            </xsl:attribute>
			<!-- nb mots -->
			<xsl:attribute name="mots" />
			<!-- nb pages -->
			<xsl:attribute name="pages">
            <xsl:value-of select="count(//tei:pb)" />
            </xsl:attribute>
			<!-- texte intégral -->
			<xsl:attribute name="integral">
            <xsl:text>Oui</xsl:text>
          </xsl:attribute>
			<!-- editeur scientifique -->
			<xsl:attribute name="edsci">
            <xsl:text>Marie-Luce Demonet</xsl:text>
          </xsl:attribute>
			<!-- maison d'édition -->
			<xsl:attribute name="edcomm">
            <xsl:text>Bibliothèques Virtuelles Humanistes (</xsl:text><xsl:value-of
				select="//tei:date[@type='mise_en_ligne']/@when" /><xsl:text>)</xsl:text>
                     </xsl:attribute>
			<!-- date d'édition -->
			<xsl:attribute name="eddate">
               <xsl:value-of
				select="//tei:monogr//tei:imprint/tei:date" />
            </xsl:attribute>
			<!-- origine du texte -->
			<xsl:choose>
				<xsl:when
					test="//tei:msDesc/tei:msIdentifier/tei:repository[text()]">
					<xsl:attribute name="origine">
              <xsl:choose>
              <xsl:when
						test="//tei:msDesc/tei:msIdentifier/tei:institution">
               <xsl:value-of
						select="//tei:msDesc/tei:msIdentifier/tei:settlement" />,<xsl:value-of
						select="//tei:msDesc/tei:msIdentifier/tei:institution" />,<xsl:value-of
						select="//tei:msDesc/tei:msIdentifier/tei:repository" />
              </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of
						select="//tei:msDesc/tei:msIdentifier/tei:settlement" />,<xsl:value-of
						select="//tei:msDesc/tei:msIdentifier/tei:repository" />
                </xsl:otherwise>
              </xsl:choose>              
            </xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="origine">
                  <xsl:value-of
						select="//tei:msDesc/tei:msIdentifier/tei:settlement" />,<xsl:value-of
						select="//tei:msDesc/tei:msIdentifier/tei:institution" />
                </xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>

			<!-- copyright -->
			<xsl:attribute name="statut">
                  <xsl:text>Creative Commons BY-NC-ND 2.0</xsl:text>
            </xsl:attribute>
			<!-- etat -->
			<xsl:attribute name="etat">              
                <xsl:text>TEI P5</xsl:text>           
            </xsl:attribute>
			<!-- caractères modernisés -->
			<xsl:attribute name="carmod">
            <xsl:text>Oui</xsl:text>
          </xsl:attribute>
			<!-- orthographe modernisée -->
			<xsl:attribute name="orthomod">
            <xsl:text>Non</xsl:text>
          </xsl:attribute>
			<!-- lieu de publication -->
			<xsl:if test="//tei:monogr//tei:pubPlace">
				<xsl:attribute name="lieuPublication">
                <xsl:value-of
					select="//tei:monogr//tei:pubPlace" />
            </xsl:attribute>
			</xsl:if>
			<!-- imprimeur -->
			<xsl:if
				test="//tei:monogr//tei:publisher[contains(@role,'imprimeur')]/tei:persName">
				<xsl:attribute name="imprimeur">  
                <xsl:for-each
					select="//tei:monogr//tei:publisher[contains(@role,'imprimeur')][not(position()=last())]/tei:persName">|<xsl:call-template name="persname" /></xsl:for-each>
                <xsl:for-each
					select="//tei:monogr//tei:publisher[contains(@role,'imprimeur')][last()]/tei:persName">|<xsl:call-template name="persname" />|</xsl:for-each>
            </xsl:attribute>
			</xsl:if>
			<!-- libraire -->
			<xsl:if
				test="//tei:monogr//tei:publisher[contains(@role,'libraire')]/tei:persName">
				<xsl:attribute name="libraire">
                <xsl:for-each
					select="//tei:monogr//tei:publisher[contains(@role,'libraire')][not(position()=last())]/tei:persName">|<xsl:call-template name="persname" /></xsl:for-each>
                <xsl:for-each
					select="//tei:monogr//tei:publisher[contains(@role,'libraire')][last()]/tei:persName">|<xsl:call-template name="persname" />|</xsl:for-each>
            </xsl:attribute>
			</xsl:if>
			<!-- période du texte -->
			<xsl:attribute name="siecle">
                <xsl:choose>
                    <xsl:when
				test="//tei:sourceDesc//tei:biblStruct//tei:date[number(.)&gt;=1301 and number(.)&lt;=1400]">
                        <xsl:text>XIVème siècle</xsl:text>
                    </xsl:when>
                    <xsl:when
				test="//tei:sourceDesc//tei:biblStruct//tei:date[number(.)&gt;=1401 and number(.)&lt;=1500]">
                        <xsl:text>XVème siècle</xsl:text>
                    </xsl:when>
                    <xsl:when
				test="//tei:sourceDesc//tei:biblStruct//tei:date[number(.)&gt;=1501 and number(.)&lt;=1600]">
                        <xsl:text>XVIème siècle</xsl:text>
                    </xsl:when>
                    <xsl:when
				test="//tei:sourceDesc//tei:biblStruct//tei:date[number(.)&gt;=1601 and number(.)&lt;=1700]">
                        <xsl:text>XVIIème siècle</xsl:text>
                    </xsl:when>
                </xsl:choose>
            </xsl:attribute>
			<!-- lieu de conservation -->
			<xsl:if test="//tei:msDesc/tei:msIdentifier">
				<xsl:choose>
					<xsl:when
						test="//tei:msDesc/tei:msIdentifier/tei:repository[text()]">
						<xsl:attribute name="lieuConservation">
               <xsl:value-of
							select="//tei:msDesc/tei:msIdentifier/tei:institution" />&#160;<xsl:value-of
							select="//tei:msDesc/tei:msIdentifier/tei:repository" />
            </xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="lieuConservation">
                  <xsl:value-of
							select="//tei:msDesc/tei:msIdentifier/tei:institution" />
                </xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<!-- format de l'ouvrage -->
			<xsl:if test="//tei:dimensions/tei:dim[@type='format']">
				<xsl:attribute name="format">
               <xsl:value-of
					select="//tei:dimensions/tei:dim[@type='format']" />
            </xsl:attribute>
			</xsl:if>
			<!-- langues présentes dans le livre, par ordre d'importance -->
			<xsl:attribute name="langue">
              <xsl:for-each
				select="//tei:langUsage/tei:language[not(position()=last())]"><xsl:value-of select="." />&#160;</xsl:for-each><xsl:value-of
				select="//tei:langUsage/tei:language[last()]" />               
            </xsl:attribute>
			<!-- copyright -->
			<xsl:attribute name="copyright">
                  <xsl:text>Bibliothèques Virtuelles Humanistes, CESR, Creative Commons BY-NC-ND 2.0</xsl:text>
            </xsl:attribute>
			<xsl:apply-templates />
		</text>
	</xsl:template>

	<xsl:template name="persname">
		<xsl:choose>
			<xsl:when test="tei:surname">
				<xsl:value-of select="tei:forename" />
				&#160;
				<xsl:value-of select="tei:surname" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="." />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:app">
		<xsl:choose>
			<xsl:when test="tei:lem">
				<xsl:apply-templates select="tei:lem" />
				<note xmlns="http://www.tei-c.org/ns/1.0" type="app">
					<xsl:for-each select="tei:rdg">
						<xsl:value-of select="concat(@wit,' : ')" />
						<xsl:apply-templates />
						<xsl:choose>
							<xsl:when test="following-sibling::tei:rdg">
								&#160;;
							</xsl:when>
							<xsl:otherwise>
								.
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</note>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="tei:rdg[1]" />
				<note xmlns="http://www.tei-c.org/ns/1.0" type="app">
					<xsl:for-each select="tei:rdg[not(position()=1)]">
						<xsl:value-of select="concat(@wit,' : ')" />
						<xsl:apply-templates />
						<xsl:choose>
							<xsl:when test="following-sibling::tei:rdg">
								&#160;;
							</xsl:when>
							<xsl:otherwise>
								.
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</note>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template
		match="//tei:choice|//tei:corr|//tei:expan|//tei:reg|//tei:supplied|//tei:subst|//tei:add|//tei:del">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:call-template name="detectBrokenWord" />
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template name="detectBrokenWord">
		<xsl:if test="not(matches(.,'^\s*[.,:;!?\])]'))">
			<xsl:choose>
				<xsl:when
					test="following-sibling::text()[1][matches(.,'^\w')] and preceding-sibling::text()[1][matches(.,'\w$')]">
					<xsl:attribute name="txm:wordPart">middle</xsl:attribute>
				</xsl:when>
				<xsl:when
					test="following-sibling::text()[1][matches(.,'^(’|'')?\w')]">
					<xsl:attribute name="txm:wordPart">start</xsl:attribute>
				</xsl:when>
				<xsl:when
					test="preceding-sibling::text()[1][matches(.,'\w$')]">
					<xsl:attribute name="txm:wordPart">end</xsl:attribute>
				</xsl:when>
				<xsl:otherwise />
			</xsl:choose>
		</xsl:if>
	</xsl:template>

	<!--<xsl:variable name="filename"> <xsl:analyze-string select="document-uri(.)" 
		regex="^.*/([^/]+)\.xml$"> <xsl:matching-substring> <xsl:value-of select="regex-group(1)"></xsl:value-of> 
		</xsl:matching-substring> </xsl:analyze-string></xsl:variable> <xsl:variable 
		name="metadata" select="document(concat($filedir,'/meta/metadata.csv.xml'))//text[@id=$filename]" 
		as="node()"/> <xsl:template match="tei:text"> <xsl:choose> <xsl:when test="matches($metadata,'.+')"> 
		<text xmlns="http://www.tei-c.org/ns/1.0"> <xsl:for-each select="$metadata/entry"> 
		<xsl:attribute name="{@id}"><xsl:value-of select="@value"/></xsl:attribute> 
		</xsl:for-each> <xsl:apply-templates/> </text> </xsl:when> <xsl:otherwise> 
		<xsl:copy> <xsl:apply-templates select="*|@*|processing-instruction()|comment()|text()"/> 
		</xsl:copy> </xsl:otherwise> </xsl:choose> </xsl:template> -->

	<xsl:variable name="nommage">
		<collection>
			<name id="B372615206_3436_tei">1586_GarzoniThe</name>
			<name id="B372616101_3537_tei">1546_RabelaisTL</name>
			<name id="B410186201_LI139_tei">1554_CalenzioRo</name>
			<name id="B693836101_A489170_tei">1561_DPeriersNR</name>
			<name id="B751131015_PYC612_tei">1545_HoraceAP</name>
			<name id="B751131015_X1888_tei">1549_DuBellayDef</name>
			<name id="B751131015_YE11562_tei">1555_LabeOeuv</name>
			<name id="B751131015_YE1735_tei">1550_DuBellayOl</name>
			<name id="XUVA_Gordon1543_B53_tei">1543_MarotBlaso</name>
			<name id="XUVA_Gordon1578_L47_tei">1578_LeryBresil</name>
			<name id="B372615206_4040_tei">1566_GelliDisc</name>
			<name id="B751131015_Z1918_tei">1533_ToryMouche</name>
			<name id="B861946101_DP1139_tei">1556_VinetDisc</name>
			<name id="B372615206_1263_tei">1538_Castiglion</name>
			<name id="B410186201_I65_tei">1529_ToryChampf</name>
			<name id="B693836101_355925_tei">1547_SceveSauls</name>
			<name id="B751131011_PYE868_tei">1563_RonsardRep</name>
			<name id="B751131011_Y2_251_tei">1536_FloresFlam</name>
			<name id="XUVA_Gordon1563_R65b_2_tei">1563_RonsardMis</name>
			<name id="XUVA_Gordon1563_R66_tei">1563_RonsardIns</name>
			<name id="XUVA_Gordon1563_R67b_tei">1563_RonsardRem</name>
			<name id="XUVA_Gordon1563_R656_tei">1563_RonsardEle</name>
			<name id="XUVA_Gordon1543_K43_tei">1543_CorroCebes</name>
			<name id="B751131015_YE1457_1460_tei">1538_MarotAdole</name>
			<name id="B759999999_Y2_2164_tei">1552_RabelaisQL</name>
			<name id="B360446201_THIA63_1_tei">1576_DeschpsHT</name>
			<name id="B372615206_4023_tei">1600_BeroaldRich</name>
			<name id="B759999999_Y2_2162_tei">1552_RabelaisTL</name>
			<name id="B693836101_346632_tei">1610_BeroaldVoyg</name>
			<name id="B751131011_YE4769_tei">1550_RonsardQuat</name>
			<name id="B360446201_B343_2_tei">1542_RabelaisGrgt</name>
			<name id="B751131010_FR3370_suppl_tei">1552_RabelaisBDcl</name>
			<name id="B751131011_YE645_tei">1550_RonsardPaix</name>
			<name id="B372616101_2950_tei">1549_DuBellayOl</name>
			<name id="XUVA_Gordon1563_R656b_tei">1563_RonsardEle2</name>
			<name id="B751041006_FR_1513_tei">1548_RabelaisQL</name>
			<name id="B360446201_B343_1_tei">1542_RabelaisPtgl</name>
			<name id="B751131011_Y22168_tei">1564_RabelaisCL</name>
			<name id="B751131011_Y22789_tei">1616_BeroaldMoy</name>
			<name id="B452346101_D1497_3_tei">1572_MansSavoye</name>
			<name id="B452346101_H6513_tei">1567_BuchananJephte</name>
			<name id="M0275_01_tei">1580_MontaigneEssais</name>
			<name id="Essais_88_tei">1588_MontaigneEssais</name>
		</collection>
	</xsl:variable>



</xsl:stylesheet>