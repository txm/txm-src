<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	exclude-result-prefixes="tei edate"
	xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0">

	<xd:doc type="stylesheet">
		<xd:short>
			This stylesheet is designed for TXM XTZ+CSV import module to extract
			metadata from the teiHeader and add them as attributes
			of the text element. It should be used at "2-front" step. See TXM
			User Manual for more details
			(http://textometrie.ens-lyon.fr/spip.php?rubrique64)
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2017, CNRS / UMR 5317 IHRIM (CACTUS research group)</xd:copyright>
	</xd:doc>



	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />


	<!-- General patterns: all elements, attributes, comments and processing 
		instructions are copied -->

	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates
				select="*|@*|processing-instruction()|comment()|text()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="*" mode="position">
		<xsl:value-of select="count(preceding-sibling::*)" />
	</xsl:template>

	<xsl:template
		match="@*|comment()|processing-instruction()">
		<xsl:copy />
	</xsl:template>

	<xsl:variable name="current-file-name">
		<xsl:analyze-string select="document-uri(.)"
			regex="^(.*)/([^/]+)\.[^/]+$">
			<xsl:matching-substring>
				<xsl:value-of select="regex-group(2)" />
			</xsl:matching-substring>
		</xsl:analyze-string>
	</xsl:variable>

	<!-- Extracting metadata to use -->

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when
				test="/TEI/teiHeader/fileDesc/titleStmt/title[@type='main']">
				<xsl:value-of
					select="/TEI/teiHeader/fileDesc/titleStmt/title[@type='main'][1]" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of
					select="/TEI/teiHeader/fileDesc/titleStmt/title[1]" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="author">
		<xsl:choose>
			<xsl:when test="/TEI/teiHeader/fileDesc/titleStmt/author">
				<xsl:for-each
					select="/TEI/teiHeader/fileDesc/titleStmt/author">
					<xsl:value-of select="." />
					<xsl:if test="following-sibling::author">
						<xsl:text>, </xsl:text>
					</xsl:if>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>anonyme</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="text_creation_date">
		<xsl:choose>
			<xsl:when
				test="/TEI/teiHeader/profileDesc/creation/date[@type='compo']">
				<xsl:choose>
					<xsl:when
						test="/TEI/teiHeader/profileDesc/creation/date[@type='compo'][1]/@when">
						<xsl:value-of
							select="/TEI/teiHeader/profileDesc/creation/date[@type='compo'][1]/@when" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of
							select="/TEI/teiHeader/profileDesc/creation/date[@type='compo'][1]" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="/TEI/teiHeader/profileDesc/creation/date">
				<xsl:choose>
					<xsl:when
						test="/TEI/teiHeader/profileDesc/creation/date[1]/@when">
						<xsl:value-of
							select="/TEI/teiHeader/profileDesc/creation/date[1]/@when" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of
							select="/TEI/teiHeader/profileDesc/creation/date[1]" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>sans date</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="genre">
		<xsl:choose>
			<xsl:when
				test="/TEI/teiHeader//keywords/term[@type='genre']">
				<xsl:value-of
					select="/TEI/teiHeader//keywords[1]/term[@type='genre'][1]" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>inconnu</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<!-- Injecting metadata into attibutes of the text element -->

	<xsl:template match="text">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="id"><xsl:value-of
				select="$current-file-name" /></xsl:attribute>
			<xsl:attribute name="titre"><xsl:value-of
				select="$title" /></xsl:attribute>
			<xsl:attribute name="auteur"><xsl:value-of
				select="$author" /></xsl:attribute>
			<xsl:attribute name="date-compo"><xsl:value-of
				select="$text_creation_date" /></xsl:attribute>
			<xsl:attribute name="genre"><xsl:value-of
				select="$genre" /></xsl:attribute>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>


</xsl:stylesheet>