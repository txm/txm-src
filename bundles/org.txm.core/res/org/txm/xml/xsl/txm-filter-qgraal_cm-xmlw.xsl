<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:me="http://www.menota.org/ns/1.0"
	xmlns:bfm="http://bfm.ens-lsh.fr/ns/1.0"
	exclude-result-prefixes="tei edate bfm me" version="1.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />

	<xsl:strip-space elements="me:norm tei:w" />

	<!-- Original version -->
	<!-- <xsl:template match="*"> <xsl:choose> <xsl:when test="namespace-uri()=''"> 
		<xsl:element namespace="http://www.tei-c.org/ns/1.0" name="{local-name(.)}"> 
		<xsl:apply-templates select="*|@*|processing-instruction()|comment()|text()"/> 
		</xsl:element> </xsl:when> <xsl:otherwise> <xsl:copy> <xsl:apply-templates 
		select="*|@*|processing-instruction()|comment()|text()"/> </xsl:copy> </xsl:otherwise> 
		</xsl:choose> </xsl:template> -->
	<!-- Eliminating all namespaces and comments -->
	<xsl:template match="*">
		<xsl:element namespace="http://www.tei-c.org/ns/1.0"
			name="{local-name(.)}">
			<xsl:apply-templates
				select="*|@*|processing-instruction()|text()" />
		</xsl:element>
	</xsl:template>


	<xsl:template match="@*|processing-instruction()">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="processing-instruction('oxygen')" />

	<xsl:template match="text()">
		<xsl:value-of select="." />
	</xsl:template>

	<xsl:template match="//tei:TEI">
		<xsl:element name="TEI"
			namespace="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates />
		</xsl:element>
	</xsl:template>

	<xsl:template match="tei:teiHeader" />

	<xsl:template match="tei:text">
		<xsl:element name="text"
			namespace="http://www.tei-c.org/ns/1.0">
			<xsl:attribute name="idbfm"><xsl:value-of
				select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type='reference']" /></xsl:attribute>
			<!--<xsl:attribute name="deaf"><xsl:value-of select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:biblFull/tei:publicationStmt/tei:idno[@type='sigle_DEAF']"/></xsl:attribute> -->
			<xsl:attribute name="titre"><xsl:value-of
				select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type='normal']" /></xsl:attribute>
			<xsl:attribute name="auteur"><xsl:value-of
				select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author" /></xsl:attribute>
			<xsl:attribute name="siecle"><xsl:value-of
				select="ancestor::tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:date[@type='compo']/@n" /></xsl:attribute>
			<xsl:attribute name="ssiecle"><xsl:value-of
				select="ancestor::tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:date[@type='compo_sous_siecle']/@n" /></xsl:attribute>
			<xsl:attribute name="datecompolibre"><xsl:value-of
				select="ancestor::tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:date[@type='compo']" /></xsl:attribute>
			<xsl:attribute name="datecompo"><xsl:value-of
				select="ancestor::tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:date[@type='compo']/@when" /></xsl:attribute>
			<!--<xsl:attribute name="notbefore"><xsl:value-of select="ancestor::tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:date[@type='compo']/@notBefore"/></xsl:attribute> 
				<xsl:attribute name="notafter"><xsl:value-of select="ancestor::tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:date[@type='compo']/@notAfter"/></xsl:attribute> 
				<xsl:attribute name="msdatelibre"><xsl:value-of select="ancestor::tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:date[@type='ms']"/></xsl:attribute> 
				<xsl:attribute name="msdate"><xsl:value-of select="ancestor::tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:date[@type='ms']/@when"/></xsl:attribute> 
				<xsl:attribute name="msnotbefore"><xsl:value-of select="ancestor::tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:date[@type='ms']/@notBefore"/></xsl:attribute> 
				<xsl:attribute name="msnotafter"><xsl:value-of select="ancestor::tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:date[@type='ms']/@notAfter"/></xsl:attribute> -->
			<xsl:attribute name="forme"><xsl:value-of
				select="ancestor::tei:TEI/tei:teiHeader/tei:profileDesc/tei:textClass/tei:catRef/@target[contains(.,'forme')]" /></xsl:attribute>
			<xsl:attribute name="domaine"><xsl:value-of
				select="ancestor::tei:TEI/tei:teiHeader/tei:profileDesc/tei:textDesc/tei:domain/@type" /></xsl:attribute>
			<xsl:attribute name="genre"><xsl:value-of
				select="ancestor::tei:TEI/tei:teiHeader/tei:profileDesc/tei:textDesc/@n" /></xsl:attribute>
			<xsl:attribute name="dialecte"><xsl:value-of
				select="ancestor::tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:region[@type='dialecte_auteur']" /></xsl:attribute>
			<!--<xsl:attribute name="morphosynt"><xsl:value-of select="ancestor::tei:TEI/tei:teiHeader/tei:revisionDesc/tei:change[contains(.,'étiquetage 
				morpho')]"/></xsl:attribute> <xsl:attribute name="restrictions"><xsl:value-of 
				select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:availability/tei:ab[@type='availability_txmweb']/@subtype"/></xsl:attribute> 
				<xsl:attribute name="relation"><xsl:value-of select="ancestor::tei:TEI/tei:teiHeader/tei:profileDesc/tei:textDesc/tei:derivation/@type"/></xsl:attribute> 
				<xsl:attribute name="edsci"><xsl:value-of select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:biblFull/tei:titleStmt/tei:respStmt[contains(.,'Éditeur 
				scientifique')]/tei:name"/></xsl:attribute> <xsl:attribute name="edcomm"><xsl:value-of 
				select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:biblFull/tei:publicationStmt/tei:publisher"/></xsl:attribute> 
				<xsl:attribute name="edville"><xsl:value-of select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:biblFull/tei:publicationStmt/tei:pubPlace"/></xsl:attribute> 
				<xsl:attribute name="eddate"><xsl:value-of select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:biblFull/tei:publicationStmt/tei:date"/></xsl:attribute> 
				<xsl:attribute name="ednum"><xsl:value-of select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:biblFull/tei:editionStmt/tei:edition"/></xsl:attribute> -->
			<xsl:apply-templates />
		</xsl:element>
	</xsl:template>

	<xsl:template match="//tei:w">
		<xsl:element name="w"
			namespace="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates
				select="@*[not(name()='type')]" />
			<xsl:attribute name="pos"><xsl:value-of
				select="@type" /></xsl:attribute>
			<xsl:attribute name="dipl"><xsl:apply-templates
				select="descendant::me:dipl" /></xsl:attribute>
			<xsl:attribute name="ref"><xsl:value-of
				select="concat('graal_cm, col. ',substring-after(preceding::tei:cb[1]/@xml:id,'col_'),', l. ',preceding::tei:lb[1]/@n)" /></xsl:attribute>
			<xsl:attribute name="q">
      <xsl:value-of select="count(ancestor::tei:q)" />
    </xsl:attribute>
			<xsl:apply-templates />
		</xsl:element>
	</xsl:template>

	<xsl:template match="//tei:choice[parent::tei:w]">
		<xsl:choose>
			<xsl:when test="descendant::me:norm">
				<xsl:apply-templates select="me:norm" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="me:facs" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="me:dipl">
		<xsl:for-each select="child::node()">
			<xsl:choose>
				<xsl:when test="self::tei:ex">
					<xsl:value-of select="concat('‹',.,'›')" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="normalize-space(.)" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>


	<xsl:template match="//tei:choice[descendant::tei:w]">
		<xsl:apply-templates select="tei:corr|tei:reg" />
	</xsl:template>

	<xsl:template match="//tei:surplus" />

	<xsl:template match="//tei:del" />

	<xsl:template match="//tei:note" />

	<xsl:variable name="apostrophe" select='"&apos;"' />

	<xsl:template match="@bfm:aggl">
		<xsl:attribute name="rend"><xsl:value-of
			select="." /></xsl:attribute>
	</xsl:template>

	<xsl:template match="me:norm|me:facs">
		<xsl:apply-templates />
		<!-- added 2011-02-10 -->
		<xsl:if
			test="ancestor::tei:w[@*='elision'] and not(substring(., string-length(.))=$apostrophe)">
			<xsl:text>'</xsl:text>
		</xsl:if>
	</xsl:template>

	<!-- <xsl:template match="tei:milestone[@unit='column']"> <xsl:element name="pb" 
		namespace="http://www.tei-c.org/ns/1.0"> <xsl:attribute name="n"><xsl:value-of 
		select="substring-after(@xml:id,'col_')"/></xsl:attribute> </xsl:element> 
		</xsl:template> <xsl:template match="tei:pb[@ed='#Pauphilet1923']"> <xsl:element 
		name="milestone" namespace="http://www.tei-c.org/ns/1.0"> <xsl:attribute 
		name="unit">page-Pauphilet</xsl:attribute> <xsl:attribute name="n"><xsl:value-of 
		select="@n"/></xsl:attribute> </xsl:element> </xsl:template> -->

	<xsl:template match="tei:cb[@rend='hidden']" />

	<xsl:template match="tei:cb[not(@rend='hidden')]">
		<xsl:element name="pb"
			namespace="http://www.tei-c.org/ns/1.0">
			<xsl:attribute name="n"><xsl:value-of
				select="substring-after(@xml:id,'col_')" /></xsl:attribute>
		</xsl:element>
	</xsl:template>

	<xsl:template match="tei:pb" />


	<xsl:template match="bfm:sb" />

	<xsl:template match="tei:lb[@ed='#facs']" />


</xsl:stylesheet>
