<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	exclude-result-prefixes="tei edate xd" version="2.0">

	<xd:doc type="stylesheet">
		<xd:short>
			Feuille de style de préparation de fichiers TEI P5 à l'importation
			dans TXM en utilisant le module d'import xml-w. Permet de supprimer
			le contenu de teiHeader et des élements hors-texte et de supprimer
			les balises non utilisées dans les index TXM.
			Par défaut, toutes les balises sauf celles qui sont listées dans le
			paramètre "copyAll" sont supprimées
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2012, CNRS / ICAR (ICAR3 LinCoBaTO)</xd:copyright>
	</xd:doc>


	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />

	<!-- Listes des balises par type de traitement. -->

	<!-- balises à supprimer avec leur contenu -->

	<xsl:param name="deleteAll">
		teiHeader|facsimile|note
	</xsl:param>

	<xsl:template
		match="*[matches(name(),concat('^',$deleteAll,'$'))]">
		<xsl:call-template name="deleteAll" />
	</xsl:template>

	<!-- balises à conserver -->
	<xsl:param name="copyAll">
		ab|body|div|front|head|lb|p|pb|s|TEI|text|w
	</xsl:param>

	<xsl:template
		match="*[matches(name(),concat('^',$copyAll,'$'))]">
		<xsl:call-template name="copyAll"></xsl:call-template>
	</xsl:template>

	<!-- balises à supprimer, contenu à conserver, c'est la règle par défaut -->
	<xsl:template match="*">
		<xsl:call-template name="deleteTag" />
	</xsl:template>

	<!-- balises à transformer en notes -->
	<!--<xsl:template match=""> <xsl:call-template name="makeNote"/> </xsl:template> -->


	<!-- balises à transformer en W -->

	<!-- <xsl:template match="tei:abbr[not(parent::tei:choice)] |tei:expan |tei:num[matches(.,'.')]"> 
		<xsl:call-template name="makeWord"/> </xsl:template> -->

	<!-- à transformer en AB typé -->

	<!-- <xsl:template match=""> <xsl:call-template name="makeAb"/> </xsl:template> -->
	<!-- à transformer en DIV typé -->

	<!-- <xsl:template match=""> <xsl:call-template name="makeDiv"/> </xsl:template> -->
	<!-- Partie générale : on copie les éléments, les attributs, le texte, les 
		commentaires, on supprime mes prrocessing intstructions -->

	<xsl:template match="@*|comment()">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="processing-instruction()" />

	<xsl:template match="text()">
		<xsl:value-of select="." />
	</xsl:template>

	<!-- ancien filtre pour les noeuds texte, n'est plus nécessaire pour l'import 
		xml/w TXM 0.5 -->

	<!--<xsl:template match="text()"> <xsl:analyze-string select="." regex="&amp;lt;|&amp;gt;|&lt;|&gt;|-\-"> 
		<xsl:matching-substring> <xsl:choose> <xsl:when test="matches(.,'&amp;lt;')"> 
		<xsl:text>‹</xsl:text> </xsl:when> <xsl:when test="matches(.,'&amp;gt;')"> 
		<xsl:text>›</xsl:text> </xsl:when> <xsl:when test="matches(.,'&lt;')"> <xsl:text>‹</xsl:text> 
		</xsl:when> <xsl:when test="matches(.,'&gt;')"> <xsl:text>›</xsl:text> </xsl:when> 
		<!-\- pose problème si se trouve dans les notes transformés en commentaires 
		xml par le tokeniseur -\-> <xsl:when test="matches(.,'-\-')"> <xsl:text> 
		- </xsl:text> </xsl:when> <xsl:otherwise> <xsl:value-of select="."/> </xsl:otherwise> 
		</xsl:choose> </xsl:matching-substring> <xsl:non-matching-substring> <xsl:value-of 
		select="."/> </xsl:non-matching-substring> </xsl:analyze-string> </xsl:template> -->


	<!-- On garde les balises utiles pour l'exploitation TXM -->

	<xsl:template name="copyAll">
		<xsl:copy>
			<xsl:apply-templates
				select="*|@*|processing-instruction()|comment()|text()" />
		</xsl:copy>
	</xsl:template>


	<!-- On supprime les balises et tout ce qu'elles contiennent pour les éléments 
		hors texte à ignorer lors de l'import -->

	<xsl:template name="deleteAll" />

	<!-- On supprime les balises non essentielles pour l'exploitation textométrique 
		et susceptibles de poser des problèmes au tokeniseur, mais on garde leur 
		contenu qui fait partie du matériau textuel -->

	<xsl:template name="deleteTag">
		<xsl:apply-templates />
	</xsl:template>

	<!-- on met en note les éléments hors texte qu'on aimerait garder pour l'édition, 
		etc. -->

	<xsl:template name="makeNote">
		<note type="{local-name()}" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:value-of select="." />
		</note>
	</xsl:template>

	<!-- On supprime toutes les balises à l'intérieur des notes -->
	<!-- <xsl:template match="tei:note"> <xsl:copy> <xsl:value-of select="."></xsl:value-of> 
		</xsl:copy> </xsl:template> -->

	<!-- On transforme en DIV typés les balises de niveau de division textuelle 
		(pouvant contenir des p) -->

	<xsl:template name="makeDiv">
		<div type="{local-name()}" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:copy-of select="@*" />
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<!-- On transforme en bloc anonyme les balises de niveau équivalent au paragraphe -->

	<xsl:template name="makeAb">
		<ab type="{local-name()}" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:copy-of select="@*" />
			<xsl:apply-templates />
		</ab>
	</xsl:template>

	<!-- on pré-tokenise les éléments équivalents à un mot -->

	<xsl:template name="makeWord">
		<xsl:variable name="wordType">
			<xsl:choose>
				<xsl:when test="@type">
					<xsl:value-of select="concat(local-name(),'-',@type)" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="local-name()"></xsl:value-of>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<w type="{$wordType}" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates />
		</w>
	</xsl:template>

	<!--<xsl:template name="makeName"> <xsl:variable name="nameType"> <xsl:choose> 
		<xsl:when test="@type"> <xsl:choose> <xsl:when test="local-name()='name'"><xsl:value-of 
		select='@type'/></xsl:when> <xsl:otherwise><xsl:value-of select="concat(local-name(),'-',@type)"/></xsl:otherwise> 
		</xsl:choose> </xsl:when> <xsl:otherwise> <xsl:value-of select="local-name()"></xsl:value-of> 
		</xsl:otherwise> </xsl:choose> </xsl:variable> <name type="{$nameType}" xmlns="http://www.tei-c.org/ns/1.0"> 
		<xsl:apply-templates/> </name> </xsl:template> -->
	<!-- les l sont transformés en lb -->

	<xsl:template match="tei:l">
		<lb xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:if test="@n">
				<xsl:attribute name="n"><xsl:value-of
					select="@n" /></xsl:attribute>
			</xsl:if>
		</lb>
		<xsl:apply-templates />
	</xsl:template>


</xsl:stylesheet>