<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	exclude-result-prefixes="tei edate" version="2.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />

	<xsl:template match="*">

		<xsl:element name="{local-name(.)}">
			<xsl:apply-templates
				select="*|@*|processing-instruction()|comment()|text()" />
		</xsl:element>

	</xsl:template>

	<xsl:template
		match="@*|processing-instruction()|comment()">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="head" />

	<xsl:template match="w">
		<xsl:element name="w">
			<xsl:attribute name="lex"><xsl:value-of
				select="child::ana/@lex" /></xsl:attribute>
			<xsl:attribute name="gr"><xsl:value-of
				select="child::ana/@gr" /></xsl:attribute>
			<xsl:attribute name="withstress"><xsl:value-of
				select="normalize-space(child::text())" /></xsl:attribute>
			<xsl:value-of select="replace(child::text(),'`','')" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="text()">
		<xsl:value-of select="." />
	</xsl:template>

</xsl:stylesheet>
