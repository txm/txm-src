<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	exclude-result-prefixes="tei edate xd" version="2.0">

	<xd:doc type="stylesheet">
		<xd:short>
			Feuille de style de préparation de fichiers TEI P5 à l'importation
			dans TXM. Permet de simplifier la structure du document afin
			d'optimiser les index CWB
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2012, CNRS / ICAR (ICAR3 LinCoBaTO)</xd:copyright>
	</xd:doc>


	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />

	<!-- Modifications par AL le 2012-01-25 g dépalacé de "deleteAll" vers "deleteTag" 
		classe de balise "name" créée (geogName, placeName, persName et name) -->

	<!-- Modifications par LBe le 2012-02-17 Transformation automatique des 
		esperluettes dans les métadonnées Suppression des balises label et heraldry 
		Signed n'est plus transformé en ab opener est transformé en ab L'institution 
		et le dépôt sont récupérés pour le lieu de conservation -->

	<!-- Modifications par AL le 2012-02-20 rolename -> roleName suppression 
		des balises surname et forename -->

	<!-- Modifications par LBe le 2012-03-09 Automatisation du nommage des sorties -->

	<!-- Listes des balises par type de traitement (les templates sont plus 
		bas) -->

	<!-- balises à supprimer avec leur contenu -->
	<xsl:template
		match="tei:figDesc|tei:figure|tei:fw|tei:g
    |tei:space|tei:titlePage ">
		<xsl:call-template name="deleteAll" />
	</xsl:template>

	<!-- balises à supprimer, contenu à conserver -->
	<xsl:template
		match="tei:add|tei:app
    |tei:caesura|tei:castItem|tei:castGroup[ancestor::tei:castList]
    |tei:castList[ancestor::tei:p]|tei:cell|tei:choice|tei:cit|tei:corr|tei:date|tei:ex
    |tei:forename|tei:g|tei:head[parent::tei:list]|tei:heraldry|tei:hi|tei:item|tei:label|tei:lem
    |tei:list[ancestor::tei:p|ancestor::tei:ab]|tei:lg[child::tei:lg|ancestor::tei:sp]
    |tei:p[ancestor::tei:sp]|tei:performance
    |tei:ref|tei:reg|tei:role|tei:roleDesc
    |tei:row|tei:salute|tei:signed[ancestor::tei:closer]|tei:speaker|tei:stage[ancestor::tei:p|ancestor::tei:ab]
    |tei:surname|tei:table[ancestor::tei:p]|tei:subst|tei:term|tei:unclear">
		<xsl:call-template name="deleteTag" />
	</xsl:template>

	<!-- balises à transformer en notes -->
	<xsl:template
		match="tei:abbr[parent::tei:choice and not(ancestor::tei:note)]
    |tei:bibl[not(ancestor::tei:note)]
    |tei:del
    |tei:orig[parent::tei:choice and not(ancestor::tei:note)]    
    |tei:rdg[parent::tei:app and not(ancestor::tei:note)]
    |tei:sic[parent::tei:choice and not(ancestor::tei:note)]
    |tei:surplus[not(ancestor::tei:w)]">
		<xsl:call-template name="makeNote" />
	</xsl:template>

	<!-- balises de noms à indexer -->

	<xsl:template
		match="tei:name
    |tei:geogName
    |tei:persName
    |tei:placeName
    |tei:roleName
    |tei:orgName">
		<xsl:call-template name="makeName" />
	</xsl:template>

	<!-- à transformer en W -->

	<xsl:template
		match="tei:abbr[not(parent::tei:choice)]
    |tei:expan
    |tei:num[matches(.,'.')]">
		<xsl:call-template name="makeWord" />
	</xsl:template>

	<!-- à transformer en AB typé -->

	<xsl:template
		match="tei:byline|tei:lg[not(child::tei:lg or ancestor::tei:sp)]|tei:list[not(ancestor::tei:p|ancestor::tei:ab)]
    |tei:closer|tei:opener|tei:sp|tei:castGroup[not(ancestor::tei:castList)]|tei:castList[not(ancestor::tei:p)]
    |tei:signed[not(ancestor::tei:closer)]
    |tei:stage[not(ancestor::tei:p|ancestor::tei:ab)]
    |tei:table[not(ancestor::tei:p)]
    |tei:trailer">
		<xsl:call-template name="makeAb" />
	</xsl:template>

	<!-- à transformer en DIV typé -->

	<xsl:template match="tei:set">
		<xsl:call-template name="makeDiv" />
	</xsl:template>

	<!-- Partie générale : on copie les éléments, les attributs, le texte, les 
		commentaires, on supprime mes prrocessing intstructions -->

	<!-- <xsl:template match="/"> <xsl:apply-templates/> </xsl:template> -->

	<xsl:template match="*">
		<xsl:choose>
			<xsl:when test="namespace-uri()=''">
				<xsl:element namespace="http://www.tei-c.org/ns/1.0"
					name="{local-name(.)}">
					<xsl:apply-templates
						select="*|@*|processing-instruction()|comment()|text()" />
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates
						select="*|@*|processing-instruction()|comment()|text()" />
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="@*|comment()">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="text()">
		<xsl:analyze-string select="."
			regex="&amp;|&lt;|&gt;|--|\*(\w+)">
			<xsl:matching-substring>
				<xsl:choose>
					<xsl:when test="matches(.,'&amp;')">
						<expan xmlns="http://www.tei-c.org/ns/1.0">et</expan>
					</xsl:when>
					<xsl:when test="matches(.,'&lt;')">
						<xsl:text>[</xsl:text>
					</xsl:when>
					<xsl:when test="matches(.,'&gt;')">
						<xsl:text>]</xsl:text>
					</xsl:when>
					<!-- pose problème si se trouve dans les notes transformés en commentaires 
						xml par le tokeniseur -->
					<xsl:when test="matches(.,'--')">
						<xsl:text> - </xsl:text>
					</xsl:when>
					<!-- <!-\- Spécial FRANTEXT -\-> <xsl:when test="matches(.,'\*(\w+)')"> 
						<name xmlns="http://www.tei-c.org/ns/1.0" type="maj"><xsl:value-of select="regex-group(1)"/></name> 
						</xsl:when> -->
					<xsl:otherwise>
						<xsl:value-of select="." />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:matching-substring>
			<xsl:non-matching-substring>
				<xsl:value-of select="." />
			</xsl:non-matching-substring>
		</xsl:analyze-string>
	</xsl:template>


	<xsl:template match="processing-instruction()" />

	<!-- On ne touche pas au teiHeader -->

	<xsl:template match="tei:teiHeader">
		<xsl:copy-of select="." />
	</xsl:template>

	<!-- On supprime les balises et tout ce qu'elles contiennent pour les éléments 
		hors texte à ignorer lors de l'import -->

	<xsl:template name="deleteAll" />

	<!-- On supprime les balises non essentielles pour l'exploitation textométrique 
		et susceptibles de poser des problèmes au tokeniseur, mais on garde leur 
		contenu qui fait partie du matériau textuel -->

	<xsl:template name="deleteTag">
		<xsl:apply-templates />
	</xsl:template>

	<!-- on met en note les éléments hors texte qu'on aimerait garder pour l'édition, 
		etc. -->

	<xsl:template name="makeNote">
		<note type="{local-name()}" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates select="text()" />
		</note>
	</xsl:template>

	<!-- On supprime toutes les balises à l'intérieur des notes -->
	<xsl:template match="tei:note">
		<xsl:copy>
			<xsl:apply-templates select="text()" />
		</xsl:copy>
	</xsl:template>

	<!-- On transforme en DIV typés les balises de niveau de division textuelle 
		(pouvant contenir des p) -->

	<xsl:template name="makeDiv">
		<div type="{local-name()}" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:copy-of select="@*" />
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<!-- On transforme en bloc anonyme les balises de niveau équivalent au paragraphe -->

	<xsl:template name="makeAb">
		<ab type="{local-name()}" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:copy-of select="@*" />
			<xsl:apply-templates />
		</ab>
	</xsl:template>

	<!-- on pré-tokenise les éléments équivalents à un mot -->

	<xsl:template name="makeWord">
		<xsl:variable name="wordType">
			<xsl:choose>
				<xsl:when test="@type">
					<xsl:value-of select="concat(local-name(),'-',@type)" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="local-name()"></xsl:value-of>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="descendant::tei:w or ancestor::tei:w">
				<!-- <xsl:element namespace="http://www.tei-c.org/ns/1.0" name="seg"> 
					<xsl:attribute name="type"><xsl:value-of select="local-name(.)"/></xsl:attribute> 
					<xsl:apply-templates select="*|@*[not(name()='type')]|processing-instruction()|comment()|text()"/> 
					</xsl:element> -->
				<xsl:apply-templates /> <!-- on supprime ces balises pour éviter la réduplication des mots -->
			</xsl:when>
			<xsl:otherwise>
				<w type="{$wordType}" xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:apply-templates />
				</w>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="makeName">
		<xsl:variable name="nameType">
			<xsl:choose>
				<xsl:when test="@type">
					<xsl:choose>
						<xsl:when test="local-name()='name'">
							<xsl:value-of select='@type' />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of
								select="concat(local-name(),'-',@type)" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="local-name()"></xsl:value-of>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<name type="{$nameType}" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates />
		</name>
	</xsl:template>

	<!-- les l sont transformés en lb -->

	<xsl:template match="tei:l">
		<lb xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:if test="@n">
				<xsl:attribute name="n"><xsl:value-of
					select="@n" /></xsl:attribute>
			</xsl:if>
		</lb>
		<xsl:apply-templates />
	</xsl:template>


	<!-- On met la ponctuation forte à l'extérieur de la balise foreign : Cet 
		élément sera toujours à l'intérieur d'une phrase. Attention au risque de 
		perdre les balises éventuelles à l'intérieur -->

	<xsl:template
		match="tei:foreign[not(contains(@rend,'multi_s'))][matches(.,'[.!?]')]">
		<xsl:variable name="lang">
			<xsl:choose>
				<xsl:when test="@lang">
					<xsl:value-of select="@lang" />
				</xsl:when>
				<xsl:when test="@xml:lang">
					<xsl:value-of select="@xml:lang" />
				</xsl:when>
				<xsl:otherwise>
					xx
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:for-each select="child::node()">
			<xsl:choose>
				<xsl:when test="self::text()">
					<xsl:analyze-string select="."
						regex="([^.!?]*)([.?!]+)">
						<xsl:matching-substring>
							<xsl:element name="foreign"
								namespace="http://www.tei-c.org/ns/1.0">
								<xsl:attribute name="lang"><xsl:value-of
									select="$lang" /></xsl:attribute>
								<xsl:analyze-string select="regex-group(1)"
									regex="&amp;">
									<xsl:matching-substring>
										<expan xmlns="http://www.tei-c.org/ns/1.0">et</expan>
									</xsl:matching-substring>
									<xsl:non-matching-substring>
										<xsl:value-of select="." />
									</xsl:non-matching-substring>
								</xsl:analyze-string>
							</xsl:element>
							<xsl:value-of select="regex-group(2)" />
							<xsl:text> </xsl:text>
						</xsl:matching-substring>
						<xsl:non-matching-substring>
							<xsl:analyze-string select="." regex="&amp;">
								<xsl:matching-substring>
									<expan xmlns="http://www.tei-c.org/ns/1.0">et</expan>
								</xsl:matching-substring>
								<xsl:non-matching-substring>
									<xsl:value-of select="." />
								</xsl:non-matching-substring>
							</xsl:analyze-string>
						</xsl:non-matching-substring>
					</xsl:analyze-string>
				</xsl:when>
				<xsl:when test="self::*[not(descendant::text())]">
					<xsl:apply-templates />
				</xsl:when>
				<xsl:otherwise>
					<xsl:element name="foreign"
						namespace="http://www.tei-c.org/ns/1.0">
						<xsl:attribute name="lang"><xsl:value-of
							select="$lang" /></xsl:attribute>
						<xsl:apply-templates select="." />
					</xsl:element>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>


	<xsl:template name="persname">
		<xsl:choose>
			<xsl:when test="tei:surname">
				<xsl:value-of select="tei:forename" />
				&#160;
				<xsl:value-of select="tei:surname" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="." />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<!-- <!-\- traitement spécifique anciennes étiquettes bfm -\-> <xsl:template 
		match="tei:w/@type[contains(.,'_')]"> <xsl:attribute name="type"><xsl:value-of 
		select="substring-before(.,'_')"/></xsl:attribute> </xsl:template> -->
</xsl:stylesheet>