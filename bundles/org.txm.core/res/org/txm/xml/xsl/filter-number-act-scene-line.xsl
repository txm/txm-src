<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc" version="2.0">

	<xd:doc type="stylesheet">
		<xd:short>
			Feuille de style de numérotation d'éléments pouvant être utilisée comme
			prétraitement de l'import XML/w+CSV. Elle est conçue pour numéroter
			les
			actes, scènes et lignes de l'édition XML de la pièce "All's Well That Ends
			Well"
			de William Shakespeare suivante
			https://www.ibiblio.org/xml/examples/shakespeare/all_well.xml
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Serge Heiden slh@ens-lyon.fr</xd:author>
		<xd:copyright>2015, ENS de Lyon / UMR 5191 ICAR (Cactus)</xd:copyright>
	</xd:doc>

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="yes" indent="yes" />

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="ACT|SCENE">
		<xsl:copy>
			<xsl:attribute name="n">
			<xsl:number level="multiple" format="I.I"
				count="ACT|SCENE" />
		</xsl:attribute>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="LINE">
		<xsl:copy>
			<xsl:attribute name="n">
			<xsl:number level="any" format="1" count="LINE"
				from="SCENE" />
		</xsl:attribute>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>

