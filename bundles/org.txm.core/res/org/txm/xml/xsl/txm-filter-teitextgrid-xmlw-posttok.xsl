<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:txm="http://textometrie.org/1.0"
	exclude-result-prefixes="tei edate txm"
	xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0">

	<xd:doc type="stylesheet">
		<xd:short>
			This stylesheet is used on tokenized DARIAH-DE TextGrid files
			in the TXM XML/w + csv import process.
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2014, CNRS / ICAR (CACTUS)</xd:copyright>
	</xd:doc>


	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />

	<!-- By default, copy all attributes, comments, processing instructions 
		and text nodes -->

	<xsl:template
		match="@*|comment()|processing-instruction()">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="text()">
		<xsl:value-of select="." />
	</xsl:template>

	<!-- delete the teiHeader for xml/w import module -->

	<xsl:template match="teiHeader" />


	<!-- The page breaks must be placed before the structural units (text divions) 
		that start on a given page (to avoid empty elements creation when splitting 
		the pages of the TXM edition) -->

	<xsl:template match="text">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:if test="not(child::*[1][self::pb])">
				<pb xmlns="http://www.tei-c.org/ns/1.0" n="title" />
			</xsl:if>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="*">
		<xsl:if
			test="descendant::tei:pb and (parent::tei:text or preceding-sibling::*)">
			<!-- on déplace les pb initiaux avant la balise de l'élément le plus haut -->
			<xsl:call-template name="raisePb" />
		</xsl:if>
		<xsl:choose>
			<xsl:when test="namespace-uri()=''">
				<xsl:element namespace="http://www.tei-c.org/ns/1.0"
					name="{local-name(.)}">
					<xsl:apply-templates
						select="*|@*|processing-instruction()|comment()|text()" />
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates
						select="*|@*|processing-instruction()|comment()|text()" />
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template
		match="tei:pb[not(preceding-sibling::*) and not(parent::tei:text)]">
		<xsl:comment>
			Pb déplacé plus haut
		</xsl:comment>
	</xsl:template>

	<xsl:template name="raisePb">
		<xsl:if test="descendant::tei:pb">
			<xsl:for-each
				select="child::*[1][descendant-or-self::tei:pb]">
				<xsl:choose>
					<xsl:when test="self::tei:pb">
						<xsl:comment>
							Pb remonté
						</xsl:comment>
						<xsl:copy>
							<xsl:apply-templates select="@*" />
							<xsl:if
								test="not(@n) and not(@facs) and following-sibling::tei:fw[1 or 2][@type='pageNum']">
								<xsl:attribute name="n"><xsl:value-of
									select="normalize-space(following-sibling::tei:fw[@type='pageNum'][1])" /></xsl:attribute>
							</xsl:if>
						</xsl:copy>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="raisePb"></xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>


	<!-- add @ref to words (for concordances) -->

	<xsl:variable name="filename">
		<xsl:analyze-string select="document-uri(.)"
			regex="^(.*)/([^/]+)\.[^/]+$">
			<xsl:matching-substring>
				<xsl:value-of select="regex-group(2)" />
			</xsl:matching-substring>
		</xsl:analyze-string>
	</xsl:variable>

	<xsl:template match="w">
		<xsl:variable name="ref">
			<xsl:choose>
				<xsl:when test="preceding::pb[1][@n]">
					<xsl:value-of
						select="concat($filename,', p. ',preceding::pb[1]/@n)" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$filename" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<w xmlns="http://www.tei-c.org/ns/1.0" ref="{$ref}">
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates
				select="*|processing-instruction()|comment()|text()" />
		</w>
	</xsl:template>

	<!-- transform speaker into an attribute of the sp element -->

	<xsl:template match="sp">
		<xsl:copy>
			<xsl:if test="child::speaker">
				<xsl:variable name="who">
					<xsl:value-of select="speaker" />
				</xsl:variable>
				<xsl:attribute name="who"><xsl:value-of
					select="normalize-space(replace($who,'\s*\p{P}+\s*$',''))" /></xsl:attribute>
				<xsl:apply-templates />
			</xsl:if>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="speaker" />


</xsl:stylesheet>