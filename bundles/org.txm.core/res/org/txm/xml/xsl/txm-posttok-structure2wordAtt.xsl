<?xml version="1.0"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc">

	<xd:doc type="stylesheet">
		<xd:short>
			This stylesheet projects the number of nesting selected ancestor
			elements to attributes of the w element.
			Designed for 3-posttok step of the XTZ + CSV TXM import module.
			Enter element names separated by | as the value of
			elementsToProject parameter.
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2018, CNRS / UMR 5317 IHRIM (CACTUS research group)</xd:copyright>
	</xd:doc>

	<!-- List elements to count and project here -->
	<xsl:param name="elementsToProject">
		q|quote
	</xsl:param>

	<!-- Whenever you match any node or any attribute -->
	<xsl:template match="node()|@*">
		<!-- Copy the current node -->
		<xsl:copy>
			<!-- Including any attributes it has and any child nodes -->
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="*:w">
		<xsl:variable name="ancestors">
			<ancestors>
				<xsl:for-each select="ancestor::*">
					<xsl:copy />
				</xsl:for-each>
			</ancestors>
		</xsl:variable>
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:for-each select="tokenize($elementsToProject,'\|')">
				<xsl:variable name="elementName">
					<xsl:value-of select="." />
				</xsl:variable>
				<xsl:attribute name="{$elementName}">
          <xsl:choose>
            <xsl:when
					test="$ancestors//*[local-name(.)=$elementName]">
              <xsl:number
					value="count($ancestors//*[local-name()=$elementName])"></xsl:number>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
			</xsl:for-each>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
