package org.txm.importer.metopes.preferences;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

public class MetopesPreferences extends TXMPreferences{

	public static final String VERSION = "metopes.version"; //$NON-NLS-1$
	
	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.put(VERSION, "");
	}
	
	/**
	 * Gets the instance.
	 * @return the instance
	 */
	public static TXMPreferences getInstance()	{
		if (!TXMPreferences.instances.containsKey(MetopesPreferences.class)) {
			new MetopesPreferences();
		}
		return TXMPreferences.instances.get(MetopesPreferences.class);
	}
}
