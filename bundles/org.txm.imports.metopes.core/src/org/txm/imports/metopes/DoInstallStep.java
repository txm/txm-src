package org.txm.imports.metopes;

import java.io.File;

import org.osgi.framework.Version;
import org.txm.PostInstallationStep;
import org.txm.Toolbox;
import org.txm.importer.metopes.preferences.MetopesPreferences;
import org.txm.utils.BundleUtils;
import org.txm.utils.logger.Log;

/**
 * Copy macros from bundle to TXM macro directory
 * 
 * @author mdecorde
 *
 */
public class DoInstallStep  extends PostInstallationStep {

	/** The ID. */
	public static String ID = "org.txm.imports.metopes.DoInstallStep"; //$NON-NLS-1$
	public static final String VERSION = "org.txm.imports.metopes.version";

	public DoInstallStep() { name = "Metopes"; }
	
	public void install() { // after the Toolbox is initialized
		
		Log.fine("Metopes.DoInstallStep.install()");
		String saved = MetopesPreferences.getInstance().getString(MetopesPreferences.VERSION);
		Version currentVersion = BundleUtils.getBundleVersion("org.txm.imports.metopes");
		
		if (saved != null && saved.length() > 0) {
			Version savedVersion = new Version(saved);
			if (currentVersion.compareTo(savedVersion) <= 0) {
				Log.fine("No post-installation of Metopes to do");
				//System.out.println("No post-installation of Metopes to do");
				return; // nothing to do
			}
		}
		System.out.println("Post-installing Metopes version="+currentVersion);
		
		File macroDirectory = new File(Toolbox.getTxmHomePath(), "scripts/macro/org/txm/macro");
		macroDirectory.mkdirs();
		if (!BundleUtils.copyFiles("org.txm.imports.metopes", "src", "org/txm/macro/", "metopes", macroDirectory, true)) {
			Log.severe("Error while coping Metopes org/txm/macro/metopes in "+macroDirectory.getAbsolutePath());
			return;
		}
		
		File importDirectory = new File(Toolbox.getTxmHomePath(), "scripts/import");
		importDirectory.mkdirs();
		if (!BundleUtils.copyFiles("org.txm.imports.metopes", "src", "org/txm/importer/metopes", "metopesLoader.groovy", importDirectory, true)) {
			Log.severe("Error while coping Metopes org/txm/importer/metopes in "+importDirectory.getAbsolutePath());
			return;
		}
		
		File xslDirectory = new File(Toolbox.getTxmHomePath(), "xsl");
		xslDirectory.mkdirs();
		if (!BundleUtils.copyFiles("org.txm.imports.metopes", "res", "org/txm/xml/xsl", "metopes", xslDirectory, true)) {
			Log.severe("Error while coping Metopes org/txm/xml/xsl/metopes in "+xslDirectory.getAbsolutePath());
			return;
		}
		
		Log.info("Metopes post-installation done.");
		MetopesPreferences.getInstance().put(MetopesPreferences.VERSION, currentVersion.toString());
		return;
	}
}
