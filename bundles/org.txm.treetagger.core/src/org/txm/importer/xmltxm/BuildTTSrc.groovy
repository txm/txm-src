// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $
//
package org.txm.importer.xmltxm

import java.text.DateFormat;
import java.util.Date;
import java.util.ArrayList;
import javax.xml.stream.*;
import java.net.URL;

import org.txm.Toolbox;
import org.txm.importer.filters.*;
import org.txm.treetagger.core.preferences.TreeTaggerPreferences

// TODO: Auto-generated Javadoc
/**
 * The Class BuildTTSrc.
 *
 * @author mdecorde
 * build the TT source for tigerSearch
 */

public class BuildTTSrc {

	/** The url. */
	private def url;

	/** The input data. */
	private def inputData;

	/** The factory. */
	private def factory;

	/** The parser. */
	private XMLStreamReader parser;

	/** The output. */
	private BufferedWriter output;

	/**
	 * Instantiates a new builds the tt src.
	 * uses XML-TXM V2
	 *
	 * @param url the url of the file to process
	 */
	public BuildTTSrc(URL url) {
		try {
			this.url = url;
			inputData = url.openStream();
			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);

		} catch (XMLStreamException ex) {
			System.out.println(ex);
		} catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput(File outfile) {
		try {
			File f = outfile;
			output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outfile),
					"UTF-8"));
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Process.
	 *
	 * @param outfile the outfile
	 * @param formtype, if multiple form, use this param to choose the correct one, if null takes the first form found
	 * @return true, if successful
	 */
	public boolean process(File outfile, String formtype) {
		if (!createOutput(outfile))
			return false;

		boolean inW = false
		boolean flagform = false; // to catch the content of the form tag
		boolean firstform = false; // to know if its the first form of the w element
		String form = ""; // the content of the form tag
		String lastopenlocalname = "";
		String localname = "";
		StringBuffer buffer = new StringBuffer();
		try {
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				switch (event) {
					case XMLStreamConstants.START_ELEMENT:
						localname = parser.getLocalName();
						
						switch (localname) {
							case "w":
							//firstform = true;
								inW = true
								break;
							case "form":
								if (inW) {
									//								if (firstform) {
									//									if (formtype != null) {
									//										if(parser.getAttributeCount() > 0
									//											&& parser.getAttributeValue(0).equals(formtype)) // only one attribute in form, type
									//											flagform = true;
									//									}
									//									else
									flagform = true;
									form = "";
									firstform = false;
									//}
								}
								break;
							case "s": // TreeTagger can use s tags
								buffer.append("<s>\n");
								break;
						}
						break;
					case XMLStreamConstants.END_ELEMENT:
						localname = parser.getLocalName();
						switch (localname) {
							case "w":
								inW = false
								break
							case "form":
								if (inW) { // ensure to process a form inside a w
									flagform = false;
									form = form.trim()
									if (form.length() == 0) buffer.append("__EMPTY__\n");
									else buffer.append(form.replace("\n", "").replace("<", "&lt;")+ "\n");
									//buffer.append(form+ "\n"); // its a txt file no need to use entities
								}
								break;

							case "s":
								buffer.append("</s>\n");
								break;
						}
						break;

					case XMLStreamConstants.CHARACTERS:
						if (flagform) {
							if (parser.getText().length() > 0)
								form += parser.getText();
						}
						break;
				}
			}

			String str = buffer.toString()
			if ("false".equals(TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.FIX_APOSTROPHES))) {
				str = str.replace("’", "'").replace("‘", "'");
			}
			output.write(str)
			output.close();
			parser.close();
			inputData.close();
		} catch (Exception ex) {
			System.out.println(ex);
			return false;
		}

		return true;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		String rootDir = "~/xml/rgaqcj/";
		// new File(rootDir+"/identity/").mkdir();

		ArrayList<String> milestones = new ArrayList<String>();// the tags who
		// you want them
		// to stay
		// milestones
		milestones.add("tagUsage");
		milestones.add("pb");
		milestones.add("lb");
		milestones.add("catRef");

		File srcfile = new File(rootDir + "anainline/", "roland-p5.xml");
		File resultfile = new File(rootDir + "ttsrc/", "roland-p5.tt");
		println("build ttsrc file : " + srcfile + " to : " + resultfile);

		def builder = new BuildTTSrc(srcfile.toURL(), milestones);
		builder.process(resultfile);

		return;
	}

}
