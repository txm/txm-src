// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2013-12-06 16:57:35 +0100 (Fri, 06 Dec 2013) $
// $LastChangedRevision: 2583 $
// $LastChangedBy: mdecorde $
//
package org.txm.importer.xmltxm

import java.io.File
import java.text.DateFormat
import java.util.Date

import org.txm.Toolbox
import org.txm.importer.*
import org.txm.importer.cwb.*
import org.txm.objects.*
import org.txm.treetagger.core.preferences.TreeTaggerPreferences
import org.txm.utils.ConsoleProgressBar
import org.txm.utils.DeleteDir;
import org.txm.utils.LangDetector;
import org.txm.utils.io.IOUtils
import org.txm.utils.treetagger.TreeTagger

// TODO: Auto-generated Javadoc
/**
 * Annotate and replace the TEI-TXM files of the folder $rootDirFile/txm with TreeTagger.
 * creates $rootDirFile/interp and $rootDirFile/treetagger
 *
 */
class AnnotateCQP {

	/** The debug. */
	boolean debug = true;

	/**
	 * Sets the debug.
	 */
	public void setDebug() { debug=true; }

	String id;

	/**
	 * Apply tt.
	 *
	 * @param ttsrcfile the ttsrcfile
	 * @param ttoutfile the ttoutfile
	 * @param modelfile the modelfile
	 * @return true, if successful
	 */
	public boolean applyTT(File ttsrcfile, File ttoutfile, File modelfile)
	{
		try {
			File infile = ttsrcfile;
			File outfile = ttoutfile;

			// TODO: why org.txm.utils.treetagger.TreeTagger tt = ..., throw IllegalAccessError ???
			String opt = Toolbox.getPreference(Toolbox.TREETAGGER_OPTIONS);
			if (opt == null) opt = "";
			String[] options = opt.split("  ");
			def tt = new TreeTagger(Toolbox.getPreference(Toolbox.TREETAGGER_INSTALL_PATH)+"/bin/", options);
			tt.settoken();
			tt.setlemma();
			tt.setquiet();
			tt.setsgml();
			tt.setnounknown();
			tt.seteostag("<s>");
			tt.treetagger(modelfile.getAbsolutePath(), infile.getAbsolutePath(), outfile.getAbsolutePath())
			//infile.delete();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Run.
	 *
	 * @param rootDirFile the root dir file
	 * @param modelfilename the modelfilename
	 * @return true, if successful
	 */
	public boolean run(File binDir, File txmDir,  String modelfilename)
	{
		//test if modelfile exists
		if(debug) {
			println "rootDirFile "+binDir
			println "txmDir "+txmDir
			println "TREETAGGER INSTALL PATH : "+TreeTaggerPreferences.getInstance().getBoolean(TreeTaggerPreferences.INSTALL_PATH)
			println "TREETAGGER MODELS PATH : "+TreeTaggerPreferences.getInstance().getBoolean(TreeTaggerPreferences.MODELS_PATH)
		}

		//test if the Toolbox know TreeTagger
		if (!new File(Toolbox.getPreference(Toolbox.TREETAGGER_INSTALL_PATH)+"/bin/").exists())
		{
			println("Could not find TreeTagger binaries in "+Toolbox.getPreference(Toolbox.TREETAGGER_INSTALL_PATH)+"/bin/")
			return false;
		}

		//cleaning
		File annotDir = new File(binDir, "annotations")
		annotDir.mkdir();

		//BUILD TT FILE READY TO BE TAGGED
		List<File> files = txmDir.listFiles(IOUtils.HIDDENFILE_FILTER)

		// get model file and check it
		File modelfile = new File(TreeTaggerPreferences.getInstance().getBoolean(TreeTaggerPreferences.MODELS_PATH), modelfilename);
		if (debug)
			println "model file : "+modelfile;

		if (!modelfile.exists()) {
			println "Skipping ANNOTATE: Incorrect modelfile path: "+modelfile;
			if(System.getProperty("os.name").startsWith("Windows"))
				println "Windows users: Windows might be hiding files extension. To see them, in the explorer parameters."
			return false;
		}

		//APPLY TREETAGGER
		println("Applying $modelfilename TreeTagger model on dir: "+txmDir+ " ("+files.size()+" files)")
		if (files == null || files.size() == 0)
			return false;

		ConsoleProgressBar cpb = new ConsoleProgressBar(files.size());
		for (File f : files) {
			cpb.tick();
			
			File infile = f;
			File outfile = new File(new File(binDir, "annotations"), f.getName());
			if (outfile.exists() && // outfile exists
				outfile.lastModified() >= infile.lastModified() && // outfile is more recent 
				outfile.length() > infile.length()) { // outfile is bigger
				// skip
			} else {
				if (!applyTT(infile, outfile, modelfile)) {
					System.out.println("Failed to apply treetagger on file "+f);
					return false;
				}
			}
		}
		println("")

//		if (DeleteDir.deleteDirectory(txmDir)) {
//			if (annotDir.renameTo(txmDir)) {
//
//			} else {
//				println "Could not rename  'annotations' directory to 'txm' directory"
//				return false
//			}
//		} else {
//			println "Could not delete txmDir: $txmDir (and could not copy 'annotations' directory"
//		}

		return true;
	}

	public boolean run(File xmlFile, String lang, File binaryCorpusDirectory, File parentFile) {
		System.out.println("AnnotateCQP called from the TreeTaggerEngine.processFile() is not yet implemented.")
		return false;
	}
}