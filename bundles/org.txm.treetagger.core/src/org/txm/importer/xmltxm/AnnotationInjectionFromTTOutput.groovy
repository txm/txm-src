// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2016-03-02 14:17:06 +0100 (mer., 02 mars 2016) $
// $LastChangedRevision: 3134 $
// $LastChangedBy: mdecorde $
//
package org.txm.importer.xmltxm
import javax.xml.stream.*

import org.txm.importer.StaxIdentityParser
import org.txm.importer.filters.*
import org.txm.importer.PersonalNamespaceContext

/**
 * The Class AnnotationInjection.
 *
 * @author mdecorde
 * 
 * inject annotation from a stand-off file into a xml-tei-txm file
 */

public class AnnotationInjectionFromTTOutput extends StaxIdentityParser {

	public static String TXMNS = "http://textometrie.org/1.0"

	/** The xml reader factory. */
	private def factory;

	/** The links. */
	private List<String> links;

	/** The linkparsers. key=type*/
	private HashMap<String, XMLStreamReader> linkparsers;

	/** The anaurl. */
	private def anaurl;

	/** The anainput data. */
	private def anainputData;

	/** The anafactory. */
	private XMLInputFactory anafactory = XMLInputFactory.newInstance();

	/** The anaparser. */
	private XMLStreamReader anaparser;
	private XMLStreamReader headerparser;

	/** The resp stmt id. */
	String respStmtID = "";

	/** The present taxonomies. */
	ArrayList<String> presentTaxonomies = new ArrayList();

	/**
	 * Instantiates a new annotation injection.
	 *
	 * @param url the xml-tei-txm file
	 * @param anaurl the stand-off file
	 */
	public AnnotationInjectionFromTTOutput(URL url, URL anaurl) {
		super(url); // init reader and writer
		try {
			this.anaurl = anaurl;
			factory = XMLInputFactory.newInstance();
			this.buildLinkParsers();// build a parser per linkgroup
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		} catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}

	private void getHeaderInfos(String containertag, boolean captureTheTag)
	{
		anainputData = new BufferedInputStream(anaurl.openStream());
		headerparser = anafactory.createXMLStreamReader(anainputData);
		boolean start = false;
		String localname;
		for (int event = headerparser.next(); event != XMLStreamConstants.END_DOCUMENT; event = headerparser.next())
		{
			//String prefix = headerparser.getNamespaceURI();
			if (event == XMLStreamConstants.START_ELEMENT) {
				localname = headerparser.getLocalName();
				if (captureTheTag && localname == containertag) // start copy  after the tag
					start = true;
				if (start) { // copy header
					String prefix = headerparser.getPrefix();
					if (prefix.length() > 0)
						writer.writeStartElement(Nscontext.getNamespaceURI(prefix), localname)
					else
						writer.writeStartElement(localname);
					for (int i = 0 ; i < headerparser.getNamespaceCount(); i++)
						writer.writeNamespace(headerparser.getNamespacePrefix(i), headerparser.getNamespaceURI(i));
					for (int i = 0 ; i < headerparser.getAttributeCount(); i++)
						writer.writeAttribute(headerparser.getAttributeLocalName(i), headerparser.getAttributeValue(i));
				}
				if (!captureTheTag && localname == containertag) // start copy  after the tag
					start = true;
			} else if (event == XMLStreamConstants.END_ELEMENT) {
				localname = headerparser.getLocalName();
				if (!captureTheTag && localname == containertag)
					break;// stop looping

				if (start)
					writer.writeEndElement();

				if (captureTheTag && localname == containertag)
					break;// stop looping
			} else if (event == XMLStreamConstants.CHARACTERS) {
				if (start)
					writer.writeCharacters(headerparser.getText());
			} else if (event == XMLStreamConstants.COMMENT) {
				if (start)
					writer.writeComment(headerparser.getText().replace("--", "&#x2212;&#x2212;"));
			}
		}
		headerparser.close();
		anainputData.close();
	}

	/**
	 * find all refs.
	 *
	 * @return the list of link parser
	 */
	private List<String> findGrpLink()
	{
		ArrayList<String> links = new ArrayList<String>();
		anainputData = anaurl.openStream();
		anaparser = anafactory.createXMLStreamReader(anainputData);

		for (int event = anaparser.next(); event != XMLStreamConstants.END_DOCUMENT; event = anaparser.next()) {
			if (event == XMLStreamConstants.START_ELEMENT) {
				if (anaparser.getLocalName().matches("linkGrp")) {
					String targetsvalue = anaparser.getAttributeValue(0)

					if (links.contains(targetsvalue)) {
						System.err.println("Warning: Multiple group declaration : "+targetsvalue+" has already been added, the first one will be used")
					} else {
						links.add(targetsvalue); // add the taxonomy type
					}
				} else if (anaparser.getLocalName().matches("respStmt")) {
					respStmtID = anaparser.getAttributeValue(0); // one attribute (id) only
				}
			}
		}
		anaparser.close();
		anainputData.close();
		return links;
	}

	/**
	 * Builds the link parsers.
	 * I need to know what groups exists to build a parser per taxonomy and go to the first link element
	 */
	private void buildLinkParsers() {
		// link group of the standoff file
		links = findGrpLink();
		linkparsers = new HashMap<String, XMLStreamReader>();

		// build one parser per link group
		for (String link : links) { // build a parser per group
			anainputData = new BufferedInputStream(anaurl.openStream());
			linkparsers.put(link, anafactory.createXMLStreamReader(anainputData));
		}

		//for each parser
		for (String link : links) {
			anaparser = linkparsers.get(link);
			for (int event = anaparser.next(); event != XMLStreamConstants.END_DOCUMENT; event = anaparser.next()) {
				if (event == XMLStreamConstants.START_ELEMENT) {
					if (anaparser.getLocalName().equals("linkGrp")) { // position the parser to the right group
						String targetsvalue = anaparser.getAttributeValue(0)
						if (targetsvalue.equals(link)) {
							break; // next element is a link start tag
						}
					}
				}
			}
		}
	}

	/**
	 * get the next tei:link value of a tei:LinkGrp.
	 *
	 * @param link the link
	 * @return the next ana
	 */
	private String getNextAnaValue(String link, String wordId) {
		anaparser = linkparsers.get(link);
		def m;
		for (int event = anaparser.next(); event != XMLStreamConstants.END_DOCUMENT; event = anaparser.next()) {
			if (event == XMLStreamConstants.START_ELEMENT) {
				if (anaparser.getLocalName().matches("link")) {
					String targetsvalue = anaparser.getAttributeValue(0)
					if ((m = targetsvalue =~ /#(.*) #(.*)/)) { // balise externe
						def g1 = m[0][1];
						def g2 = m[0][2];

						String anavalue = g2;
						anavalue = anavalue.replace("&lt;", "<")
						return anavalue;
					} else {
						System.err.println("Error: getNextAna(): link target is not well formed:  = "+anaparser.getAttributeValue(0));
					}
				}
			}
		}
		anaparser.close();
		return "";
	}

	/**
	 * build the ana tags of a word.
	 *
	 * @param wordId the word id
	 * @return the ana tag
	 */
	private void writeAnaTag(String wordId)
	{
		String anabalises ="\n";
		for (String link : links) {
			writer.writeStartElement(TXMNS, "ana");
			writer.writeAttribute("resp", "#"+respStmtID);
			writer.writeAttribute("type", "#"+link);
			writer.writeCharacters(getNextAnaValue(link, wordId));
			writer.writeEndElement(); // txm:ana
		}
	}

	String wordId;
	boolean flagSourceDesc = false;
	protected void processStartElement()
	{
		if (parser.getLocalName().matches("taxonomy")) {
			String taxo = parser.getAttributeValue(0) // taxonomy type
			presentTaxonomies.add(taxo);
		}

		for (int i= 0 ; i < parser.getAttributeCount() ;i++ ) {
			if (parser.getAttributeLocalName(i) == "id") {
				wordId = parser.getAttributeValue(i);
				break
			}
		}
		super.processStartElement();
	}

	boolean applicationWritten = false;
	boolean taxonomiesWritten = false;
	protected void processEndElement() {
		switch (parser.getLocalName()) {
			case "w":
				writeAnaTag(wordId);
				break;

			case "appInfo":
				applicationWritten = true;
				getHeaderInfos("appInfo", false);
				break;

			case "classDecl":
				taxonomiesWritten = true;
				getHeaderInfos("classDecl", false);
				break;

			case "encodingDesc":
				if (!applicationWritten) {
					writer.writeStartElement("appInfo");
					getHeaderInfos("appInfo", false);
					writer.writeEndElement(); // appInfo
				}
				if (!taxonomiesWritten) {
					writer.writeStartElement("classDecl");
					getHeaderInfos("classDecl", false);
					writer.writeEndElement(); // classDecl
				}
				break;

			case "titleStmt":
				if (flagSourceDesc) {
					//output.write(this.respStmt+"\n")
					getHeaderInfos("respStmt", true);
					flagSourceDesc = false;
					break;
				}
				break;
		}
		super.processEndElement();
	}

	/** The declarenamespace. */
	boolean declarenamespace = false;

	/**
	 * Declare namespace.
	 *
	 * @return the java.lang. object
	 */
	private declareNamespace() {
		if (!declarenamespace) {
			writer.writeDefaultNamespace("http://www.tei-c.org/ns/1.0");
			writer.writeNamespace("txm", TXMNS);
			declarenamespace = true;
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		String rootDir = "~/xml/rgaqcj/";
		new File(rootDir + "/injection/").mkdir();

		def milestones = [
			"tagUsage",
			"pb",
			"lb",
			"catRef"]// the tags who
		File srcfile = new File(rootDir, "/anainline/", "roland.xml");
		File pos1file = new File(rootDir, "/pos/", "rolandTT1-w-ana.xml");

		File src2file = new File(rootDir, "/injection/", "roland.xml");
		File pos2file = new File(rootDir, "/pos/", "rolandTT2-w-ana.xml");

		println("process file : " + srcfile + " with : " + pos1file);
		def builder = new AnnotationInjectionFromTTOutput(srcfile.toURI().toURL(),
				pos1file.toURI().toURL(), milestones);
		builder.transfomFile(new File(rootDir + "/injection/", "roland.xml"));

		println("process file : " + src2file + " with : " + pos1file);
		builder = new AnnotationInjectionFromTTOutput(src2file.toURI().toURL(), pos2file.toURI().toURL(),
				milestones);
		builder.transfomFile(rootDir + "/injection/", "roland-FINAL.xml");

		return;
	}
}
