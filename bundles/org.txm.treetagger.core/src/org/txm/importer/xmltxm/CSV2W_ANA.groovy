// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2016-03-29 09:51:35 +0200 (mar. 29 mars 2016) $
// $LastChangedRevision: 3185 $
// $LastChangedBy: mdecorde $
//
package org.txm.importer.xmltxm

import java.nio.charset.Charset;
import javax.xml.stream.*;
import org.txm.importer.PersonalNamespaceContext;
import org.txm.utils.treetagger.TreeTagger;

/**  
 * @author mdecorde  
 * 
 * Take a CVS file with columns [word, pos1, pos2, ...] Build xml-pos With given infos : - setAnnotationTypes( types, idform); - setResp( person, software, cmdLine);  types : list of type [pos1, pos2, ...] idform : the ___ of the words' id person : the resp software : the software which creates annotations cmdline : the used cmd line 
 * 
 */
public class CSV2W_ANA {
	String encoding;

	/** The target. */
	String target = "" // ana file targeted

	/** The distributor. */
	String distributor= "";

	/** The publi chat. */
	String publiChat= "";

	/** The source chat. */
	String sourceChat= "";

	/** The reportfile. */
	File reportfile;

	/** The resp person. */
	String respPerson; // used for respStmt/resp/name(person)

	/** The resp id. */
	String respId;

	/** The resp chat. */
	String respChat;

	/** The resp date. */
	String respDate;

	/** The resp when. */
	String respWhen;

	/** The app ident. */
	String appIdent;

	/** The app version. */
	String appVersion;

	/** The types. */
	def types ;// used for linkGrp type

	/** The types desc. */
	def typesDesc ;// used for types' description

	/** The types uri. */
	def typesURI; // used for types' URI

	/** The types web. */
	def typesWEB;//used for types siteweb

	/** The idform. */
	String idform ="";

	/** The writer. */
	XMLOutputFactory factory = XMLOutputFactory.newInstance();
	BufferedOutputStream output;
	XMLStreamWriter writer;

	public static String TXMNS = "http://textometrie.org/1.0";
	public static String TEINS = "http://www.tei-c.org/ns/1.0";

	/**
	 * Process.
	 *
	 * @param TTrez the tt result file
	 * @param xmlform the xmlform the standoff file created
	 * @param encoding the encoding of the result file
	 * @return true, if successful
	 */
	public boolean process(File TTrez,File xmlform, String encoding )
	{
		try {
			this.encoding = encoding;
			output = new BufferedOutputStream(new FileOutputStream(xmlform));
			writer = factory.createXMLStreamWriter(output, "UTF-8"); // create a new file
			writer.setNamespaceContext(new PersonalNamespaceContext());
			writeHead();
			writeBody(TTrez,encoding)
			writeTail();
			writer.close();
			output.close();
		} catch(Exception e) {
			e.printStackTrace();
			writer.close();
			output.close();
			return false;
		}
		return true;
	}

	/**
	 * Write head.
	 */
	private void writeHead()
	{
		Charset UTF8 = Charset.forName("UTF8");
		writer.writeStartDocument(encoding, "1.0");
		writer.writeStartElement("TEI");
		writer.writeDefaultNamespace(TEINS);
		writer.writeNamespace("txm", TXMNS);
		writer.writeStartElement("teiHeader");
		writer.writeStartElement("fileDesc");
		writer.writeStartElement("titleStmt");
		writer.writeStartElement("title");
		writer.writeCharacters("Standoff annotation file for "+target);
		writer.writeEndElement(); // title
		writer.writeStartElement("respStmt");
		writer.writeAttribute("xml:id", respId);
		writer.writeStartElement("resp");
		writer.writeCharacters(respChat)
		writer.writeStartElement("date");
		writer.writeAttribute("when", respWhen);
		writer.writeCharacters(respDate);
		writer.writeEndElement(); // date
		writer.writeEndElement(); // resp
		writer.writeStartElement("name");
		writer.writeAttribute("type", "person");
		writer.writeCharacters(respPerson+"\n");
		writer.writeEndElement(); // name
		writer.writeEndElement(); // respStmt
		writer.writeEndElement(); // titleStmt
		writer.writeStartElement("publicationStmt")
		writer.writeCharacters("\n");
		writer.writeStartElement("distributor");
		writer.writeCharacters("\n");
		writer.flush();
		output.write(distributor.getBytes(UTF8))
		output.flush()
		writer.writeEndElement(); // distributor
		writer.writeCharacters("\n");
		writer.flush();
		output.write(publiChat.getBytes(UTF8));
		output.flush()
		writer.writeEndElement(); // publicationStmt
		writer.writeStartElement("sourceDesc");
		writer.writeCharacters("\n");
		writer.flush();
		output.write(sourceChat.getBytes(UTF8));
		output.flush()
		writer.writeEndElement(); // sourceDesc
		writer.writeEndElement(); // fileDesc
		writer.writeStartElement("encodingDesc");
		writer.writeStartElement("appInfo");
		writer.writeStartElement(TXMNS,"application");
		writer.writeAttribute("ident",appIdent);
		writer.writeAttribute("version", appVersion);
		writer.writeAttribute("resp", respId);
		writer.writeCharacters("\n");
		writer.flush();
		def report = this.reportfile;
		if (report != null) {
			//System.out.println("read report : "+report);
			Reader reader = new FileReader(report);
			String line = reader.readLine();
			while (line != null) {
				if (line.length() != 0)
					output.write((line.replace("&","&amp;")+"\n").getBytes(UTF8));
				line = reader.readLine();
			}
			reader.close();
			output.flush()
		} else {
			writer.writeEmptyElement(TXMNS, "commandLine");
			writer.writeCharacters("\n");
		}

		writer.writeStartElement("ab");
		writer.writeAttribute("type", "annotation");
		writer.writeCharacters("\n");
		writer.writeStartElement("list");
		writer.writeCharacters("\n");
		for (int i = 0; i < types.size(); i++) {
			writer.writeStartElement("item");
			writer.writeEmptyElement("ref");
			writer.writeAttribute("type", "tagset");
			writer.writeAttribute("target","#"+types[i])
			writer.writeEndElement(); // item
			writer.writeCharacters("\n");
		}
		writer.writeEndElement(); // list
		writer.writeEndElement(); // ab
		writer.writeEndElement(); // txm:application
		writer.writeEndElement(); // appInfo
		writer.writeStartElement("classDecl");
		for (int i = 0; i < types.size(); i++) {
			writer.writeStartElement("taxonomy");
			writer.writeAttribute("xml:id", types[i]);
			writer.writeStartElement("bibl");
			writer.writeStartElement("title");
			writer.writeCharacters(typesDesc[i]);
			writer.writeEndElement(); // title
			if (!typesURI[i].equals("")) {
				writer.writeEmptyElement("ref");
				writer.writeAttribute("type", "tagset");
				writer.writeAttribute("target", typesURI[i]);
				writer.writeCharacters("\n");
			}
			if (!typesWEB[i].equals("")) {
				writer.writeEmptyElement("ref");
				writer.writeAttribute("type", "siteweb");
				writer.writeAttribute("target", typesWEB[i]);
				writer.writeCharacters("\n");
			}
			writer.writeEndElement(); // bibl
			writer.writeCharacters("\n");
			writer.writeEndElement(); // taxonomy
			writer.writeCharacters("\n");
		}
		writer.writeEndElement(); // classDecl
		writer.writeCharacters("\n");
		writer.writeEndElement(); // encodingDesc
		writer.writeCharacters("\n");
		writer.writeEndElement(); // teiHeader

		writer.writeStartElement("text");
		writer.writeAttribute("type", "standoff");
		writer.writeStartElement("body");
		writer.writeCharacters("\n");
		writer.writeStartElement("div");
		writer.writeCharacters("");
	}

	/**
	 * Write body.
	 *
	 * @param TTrez the tt result file
	 * @param encoding the encoding
	 * @return the java.lang. object
	 */
	private writeBody(File TTrez, String encoding) {
		String tmp = "";
		String targets= idform+"";

		//def content = TTrez.getText(encoding)
		//content = content.replace("&lt;", "<")

		def separator= "\t"
		def maxsize = types.size()+1;
		for (int i = 0 ; i< types.size() ; i++) {
			int id = 1;
			//output.write("<linkGrp type=\""+types[i]+"\">\n");
			writer.writeStartElement("linkGrp");
			writer.writeAttribute("type", types[i]);
			TTrez.eachLine("UTF-8") { line ->
				line = line.replaceAll("&lt;", "<")
				def fields = line.split(separator);
				//content.splitEachLine(separator) {fields ->
					if (fields.size() == maxsize)
						if (!(fields[0].startsWith("<s>") || fields[0].startsWith("</s>")))// don't use the sentence tags
					{
						writer.writeEmptyElement("link");
						writer.writeAttribute("target", "#"+targets+id+" #"+ fields[i+1]);
						id++;
						writer.writeCharacters("\n");
					}
				//}
			}
			writer.writeEndElement(); // linkGrp
		}
	}

	/**
	 * Write tail.
	 */
	private void writeTail() {
		writer.writeEndElement(); // div
		writer.writeEndElement(); // body
		writer.writeEndElement(); // text
		writer.writeEndElement(); // TEI
	}

	/**
	 * Sets the annotation types.
	 *
	 * @param types the types
	 * @param typesDesc the types desc
	 * @param typesURI the types uri
	 * @param typesWEB the types web
	 * @param idform the idform
	 */
	public void setAnnotationTypes(def types,def typesDesc,def typesURI,def typesWEB, String idform) {
		this.types = types;
		this.typesDesc = typesDesc;
		this.typesURI = typesURI;
		this.typesWEB = typesWEB;
		this.idform = idform;
	}

	/**
	 * Sets the resp.
	 *
	 * @param respId the resp id
	 * @param respChat the resp chat
	 * @param respDate the resp date
	 * @param respPerson the resp person
	 * @param respWhen the resp when
	 */
	public void setResp(respId, respChat,respDate, respPerson,respWhen) {
		this.respId = respId;
		this.respChat = respChat;
		this.respDate = respDate
		this.respPerson = respPerson;
		this.respWhen = respWhen;
	}

	/**
	 * Sets the target.
	 *
	 * @param target the target
	 * @param reportFile the report file
	 */
	public void setTarget(target, reportFile) {
		this.target = target;
		this.reportfile = reportFile;
	}

	/**
	 * Sets the app.
	 *
	 * @param appIdent the app ident
	 * @param appVersion the app version
	 */
	public void setApp(String appIdent, String appVersion) {
		this.appIdent = appIdent;
		this.appVersion = appVersion;
	}

	/**
	 * Sets the infos.
	 *
	 * @param distributor the distributor
	 * @param publiChat the publi chat
	 * @param sourceChat the source chat
	 */
	public void setInfos(distributor,  publiChat, sourceChat) {
		this.distributor = distributor;
		this.publiChat = publiChat;
		this.sourceChat = sourceChat;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		String rootDir = "~/xml/rgaqcj/";
		new File(rootDir,"proj").mkdir();

		//targeted file for annotations
		String target = "roland.xml"
		//contains txm:application/txm:commandLine
		File reportFile = new File("~/xml/rgaqcj/tt1_REPORT.xml");

		String respPerson ="slh";
		String respId = "TT1";
		String respDesc = "NLP annotation tool";
		String respDate = "Tue Mar  11 1:02:55 Paris, Madrid 2010";
		String respWhen="2010-03-11"

		String appIdent = "TreeTagger";
		String appVersion = "3.2";

		String distributor = "BFM project - http://bfm.ens-lsh.fr";
		String publiStmt = """<availability>
			  <p>(c) 2010 Projet BFM - CNRS/ENS-LSH.
			  <hi>Conditions d'utilisation</hi> : 
			  Sous licence <ref target="http://creativecommons.org/licenses/by-sa/2.0/fr/">Creative Commons</ref>.
			  </p>
			</availability>""";

		String sourceStmt = """<p>Born digital: TXM project - http://textometrie.org</p>
			<p>Automatically generated on <date when=" """+respWhen+"""">"""+respDate+"""</date>
			from <ref target="#"""+respId+"""">Tree Tagger</ref> applied on the text roland-tt.
			</p>""";

		def types = ["CATTEX2009"];
		def typesTITLE = ["CATTEX2009"];
		def typesDesc = ["CATTEX2009 POS description"]
		def typesTAGSET = ["http://bfm.ens-lsh.fr/IMG/xml/cattex2009.xml"]
		def typesWEB = ["http://bfm.ens-lsh.fr/article.php3?id_article=176"]
		String idform ="w_fro_";

		File ttfile = new File(rootDir+"/proj/","rol_rga.tt");
		File posfile = new File(rootDir+"/pos/","rolandTT1-w-ana.xml");
		String encoding ="UTF-8";
		println("Process file : "+ttfile+" to : "+posfile );

		def transfo = new CSV2W_ANA();
		transfo.setAnnotationTypes( types, typesDesc, typesTAGSET, typesWEB, idform);
		transfo.setResp(respId, respDesc,respDate, respPerson, respWhen);
		transfo.setApp(appIdent, appVersion);
		transfo.setTarget(target, reportFile);
		transfo.setInfos(distributor,  publiStmt, sourceStmt);
		transfo.process( ttfile, posfile, encoding );

		//create annotation file for afrpar on roalnd.xml
		String infile = rootDir+"/ttsrc/roland-learn.tt";
		String modelfile =  rootDir+"/models/afr.par";
		String outfile = rootDir+"/proj/afr_rol.tt";

		println("proj "+modelfile+ " on " +infile +" >> "+outfile);

		TreeTagger tt = new TreeTagger("~/Bureau/treetagger/bin/");
		tt.settoken();
		tt.setquiet();
		tt.setsgml();
		tt.seteostag("<s>");
		tt.treetagger( modelfile, infile, outfile)

		reportFile = new File("~/xml/rgaqcj/tt2_REPORT.xml");
		respPerson ="slh";
		respId = "TT2";
		respDesc = "NLP annotation tool";
		respDate = "Tue Mar  15 1:02:55 Paris, Madrid 2010";
		respWhen = "2010-03-15"

		types = ["AFRPOS"];
		typesDesc = ["AFRPOS description"]
		typesWEB = ["http://www.uni-stuttgart.de/lingrom/stein/forschung/resource.html"]
		typesTAGSET = [""]
		idform ="w_fro_";

		ttfile = new File(rootDir+"/proj/","afr_rol.tt");
		posfile = new File(rootDir+"/pos/","rolandTT2-w-ana.xml");
		encoding ="UTF-8";
		println("Process file : "+ttfile+" to : "+posfile );

		transfo = new CSV2W_ANA();
		transfo.setAnnotationTypes( types, typesDesc, typesTAGSET, typesWEB, idform);
		transfo.setResp(respId, respDesc,respDate, respPerson,respWhen);
		transfo.setTarget(target, reportFile);
		transfo.setApp(appIdent, appVersion);
		transfo.setInfos(distributor,  publiStmt, sourceStmt);
		transfo.process( ttfile, posfile, encoding );
	}
}