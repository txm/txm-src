// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2016-10-03 15:30:36 +0200 (lun. 03 oct. 2016) $
// $LastChangedRevision: 3313 $
// $LastChangedBy: mdecorde $
//
package org.txm.importer.xmltxm

import java.text.DateFormat
import java.util.concurrent.*

import org.txm.importer.scripts.xmltxm.AnnotationInjection
import org.txm.treetagger.core.preferences.TreeTaggerPreferences
import org.txm.utils.ConsoleProgressBar
import org.txm.utils.LangDetector;
import org.txm.utils.ThreadFile
import org.txm.utils.io.IOUtils
import org.txm.utils.logger.Log;
import org.txm.utils.treetagger.TreeTagger

/**
 * Annotate and replace the TEI-TXM files of the folder $rootDirFile/txm with TreeTagger.
 * creates $rootDirFile/interp and $rootDirFile/treetagger
 *
 */
class Annotate {
	boolean cancelNow = false;
	
	/** The report file. */
	File reportFile;//contains the txm:application tag content
	
	/** The resp person. */
	String respPerson;
	
	/** The resp id. */
	String respId;
	
	/** The resp desc. */
	String respDesc;
	
	/** The resp date. */
	String respDate;
	
	/** The resp when. */
	String respWhen;
	
	/** The app ident. */
	String appIdent;
	
	/** The app version. */
	String appVersion;
	
	/** The distributor. */
	String distributor;
	
	/** The publi stmt. */
	String publiStmt;
	
	/** The source stmt. */
	String sourceStmt;
	
	/** The types. */
	def types;
	
	/** The types title. */
	def typesTITLE;
	
	/** The types desc. */
	def typesDesc;
	
	/** The types tagset. */
	def typesTAGSET;
	
	/** The types web. */
	def typesWEB;
	
	/** The idform. */
	String idform;
	
	/** The debug. */
	boolean debug = false;
	
	File modelsDirectory;
	
	public Annotate() {
		modelsDirectory = new File(TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.MODELS_PATH)); // default models directory is set in the Toolbox
	}
	
	/**
	 * Sets the debug.
	 */
	public void setDebug() {
		debug = true;
	}
	
	String id;
	/**
	 * Inits the tt outfile infos.
	 *
	 * @param rootDirFile the root dir file
	 * @param modelfile the modelfile
	 */
	public void initTTOutfileInfos(File rootDirFile, File modelfile, String modelfilename) {
		initTTOutfileInfos(rootDirFile, modelfile, modelfilename, null);
	}
	
	/**
	 * Inits the tt outfile infos.
	 *
	 * @param rootDirFile the root dir file
	 * @param modelfile the modelfile
	 * @param properties : 2 element array that contains the word properties to create. It can be null (the modelfilename will be used)
	 */
	public void initTTOutfileInfos(File rootDirFile, File modelfile, String modelfilename, String[] properties) {
		
		id = modelfilename;
		String[] split = id.split("\\.");
		if (split.length > 0) id = split[0];
		if (id.equals("??")) id = "xx"
		
		reportFile = new File(rootDirFile,"NLPToolsParameters.xml");
		
		respPerson = System.getProperty("user.name");
		respId = "txm";
		respDesc = "NLP annotation tool";
		respDate = DateFormat.getDateInstance(DateFormat.SHORT, Locale.UK).format(new Date());
		respWhen = DateFormat.getDateInstance(DateFormat.FULL, Locale.UK).format(new Date());
		
		appIdent = "TreeTagger";
		appVersion = "3.2";
		
		distributor = "";
		publiStmt = """""";
		sourceStmt = """""";
		
		if (properties != null && properties.length == 2) {
			types = [properties[0], properties[1]];
			typesTITLE = [properties[0], properties[1]];
		} else {
			types = [id+"pos", id+"lemma"];
			typesTITLE = [id+"pos", id+"lemma"];
		}
		
		//TODO: the tagset, website and description should be referenced in the model catalog
		if (modelfile.getName() == "rgaqcj.par") {
			typesDesc = [
				"CATTEX pos tagset built with BFM texts",
				"fr lemma of the model "+modelfile+" - "
			]
			typesTAGSET = [
				"http://bfm.ens-lyon.fr/IMG/pdf/Cattex2009_Manuel.pdf",
				""
			]
			typesWEB = [
				"http://bfm.ens-lyon.fr/",
				""
			]
		} else {
			typesDesc = [
				"pos tagset built from model "+modelfile,
				id+" lemma of the model "+modelfile+" - "
			]
			typesTAGSET = ["", ""]
			typesWEB = ["", ""]
		}
		
		idform ="w";
	}
	
	/**
	 * Apply tt.
	 *
	 * @param ttsrcfile the ttsrcfile
	 * @param ttoutfile the ttoutfile
	 * @param modelfile the modelfile
	 * @return true, if successful
	 */
	public boolean applyTT(File ttsrcfile, File ttoutfile, File modelfile) {
		return applyTT(ttsrcfile, ttoutfile, modelfile, null)
	}
	
	/**
	 * Apply tt.
	 *
	 * @param ttsrcfile the ttsrcfile
	 * @param ttoutfile the ttoutfile
	 * @param modelfile the modelfile
	 * @param options, if null use value set in Toolbox preferences
	 * @return true, if successful
	 */
	public boolean applyTT(File ttsrcfile, File ttoutfile, File modelfile, String[] options) {
		
		try {
			File infile = ttsrcfile;
			File outfile = ttoutfile;
			
			def tt = new TreeTagger(TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.INSTALL_PATH)+"/bin/", options);
			tt.settoken();
			tt.setlemma();
			tt.setsgml();
			if (TreeTaggerPreferences.getInstance().getBoolean(TreeTaggerPreferences.OPTIONS_UNKNOWN)) {
				tt.setnounknown();
			}
			tt.seteostag("<s>");
			if (TreeTaggerPreferences.getInstance().getBoolean(TreeTaggerPreferences.OPTIONS_DEBUG)) {
				tt.debug(true);
			} else {
				tt.setquiet();
			}
			if (TreeTaggerPreferences.getInstance().getBoolean(TreeTaggerPreferences.OPTIONS_HYPHENHEURISTIC)) {
				tt.sethyphenheuristics();
			}
			if (TreeTaggerPreferences.getInstance().getBoolean(TreeTaggerPreferences.OPTIONS_PROB)) {
				tt.setprob();
			}
			
			String lex = TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.OPTIONS_LEX);
			if (lex !=null && lex.length() > 0) {
				tt.setlex(lex);
			}
			String wc = TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.OPTIONS_WC);
			if (wc !=null && wc.length() > 0) {
				tt.setwc(wc);
			}
			tt.treetagger(modelfile.getAbsolutePath(), infile.getAbsolutePath(), outfile.getAbsolutePath())
			infile.delete();
		} catch(Exception e) {
			Log.printStackTrace(e);
			System.out.println("Failed to apply TreeTagger on $ttsrcfile input file with the $modelfile model file.");
			return false;
		}
		return true;
	}
	
	/**
	 * Write standoff file.
	 *
	 * @param ttoutfile the ttoutfile
	 * @param posfile the posfile
	 * @return true, if successful
	 */
	public boolean writeStandoffFile(File ttoutfile, File posfile)
	{
		def encoding ="UTF-8";
		def transfo = new CSV2W_ANA();
		//println("build w-interp "+ttfile.getName()+ ">>"+posfile.getName())
		transfo.setAnnotationTypes( types, typesDesc, typesTAGSET, typesWEB, idform);
		transfo.setResp(respId, respDesc, respDate, respPerson, respWhen);
		transfo.setApp(appIdent, appVersion);
		transfo.setTarget(ttoutfile.getAbsolutePath(), reportFile);
		transfo.setInfos(distributor,  publiStmt, sourceStmt);
		return transfo.process( ttoutfile, posfile, encoding );
	}
	
	/**
	 * Run step by step : build TT src files, run TT, build xml-standoff files, inject standoff annotations
	 *
	 * @param rootDirFile the root dir file
	 * @param modelfilename the modelfilename
	 * @return true, if successful
	 */
	public boolean run(File binDir, File txmDir,  String modelfilename)
	{
		//test if modelfile exists
		if (debug) {
			println "rootDirFile "+binDir
			println "txmDir "+txmDir
			println "TREETAGGER INSTALL PATH : "+TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.INSTALL_PATH);
			println "TREETAGGER MODELS PATH : "+TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.MODELS_PATH)
		}
		
		//test if the Toolbox know TreeTagger
		if (!new File(TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.INSTALL_PATH)+"/bin/").exists()) {
			println("Could not find TreeTagger binaries in "+TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.INSTALL_PATH)+"/bin/")
			return false;
		}
		String langAll = null
		String lang;
		if (modelfilename.startsWith("??")) {
			langAll = new LangDetector(binDir).getLang();
			println "General lang $langAll"
		}
		
		//cleaning
		new File(binDir, "annotations").deleteDir();
		new File(binDir, "annotations").mkdir();
		new File(binDir, "treetagger").deleteDir();
		new File(binDir, "treetagger").mkdir();
		
		ArrayList<String> milestones = [];
		
		//BUILD TT FILE READY TO BE TAGGED
		List<File> files = txmDir.listFiles(IOUtils.HIDDENFILE_FILTER)
		
		println("Building TT source files ("+files.size()+") from directory "+txmDir)
		ConsoleProgressBar cpb = new ConsoleProgressBar(files.size())
		for (File f : files) {
			cpb.tick()
			File srcfile = f;
			File resultfile = new File(binDir, "treetagger/"+f.getName()+".tt");
			if(debug)
				println "build tt src : "+srcfile+" >> "+resultfile
			def ttsrcbuilder = new BuildTTSrc(srcfile.toURI().toURL())
			if (!ttsrcbuilder.process(resultfile, null))
				System.out.println("Failed to build tt src file of "+srcfile);
		}
		
		if (cancelNow) {
			return false;
		}
		
		File modelDirectory = new File(TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.MODELS_PATH));
		if (!modelDirectory.exists()) {
			println "Skipping ANNOTATE: TreeTagger language model file directory not found: "+modelDirectory.getAbsolutePath();
			return false;
		} else 	if (!modelDirectory.canRead()) {
			println "Skipping ANNOTATE: impossible to access the TreeTagger language model file directory: "+modelDirectory.getAbsolutePath();
			return false;
		}
		println("")
		//Convert encoding if needed
		
		//APPLY TREETAGGER
		files = new File(binDir, "treetagger").listFiles(IOUtils.HIDDENFILE_FILTER)
		println("Applying $modelfilename TreeTagger model on dir: "+new File(binDir, "treetagger")+ " ("+files.size()+" files)")
		if (files == null || files.size() == 0) {
			return false;
		}
		File modelfile;
		cpb = new ConsoleProgressBar(files.size())
		for (File f : files) {
			String tmpModelFileName = modelfilename
			if (modelfilename.startsWith("??")) {
				lang = langAll;
				if (f.length() > LangDetector.MINIMALSIZE) {
					lang = new LangDetector(f).getLang();
					//println "guessing lang $f : $lang"
				}
				tmpModelFileName = lang+".par"
			}
			modelfile = new File(modelsDirectory, tmpModelFileName);
			if (debug)
				println "model file: "+modelfile;
			
			
			
			if (!modelfile.exists()) {
				println "Skipping ANNOTATE: '$modelfile' TreeTagger language model file not found."
				if(System.getProperty("os.name").startsWith("Windows") || System.getProperty("os.name").startsWith("Mac")) {
					println "Windows&Mac users: the operating system might be hiding file extensions. Use your file explorer to check the file name."
				}
				return false;
			} else if (!modelfile.canRead()) {
				println "Skipping ANNOTATE: impossible to access the '$modelfile' TreeTagger language model file."
				return false;
			}
			
			//			if (modelfile.getName().equals("sp.par")) {//UTF >> Latin1
			//				if(debug)
			//					println "fix encoding for model "+modelfile
			//				new EncodingConverter(f, "UTF-8", "ISO-8859-1")
			//			}
			
			cpb.tick()
			File infile = f;
			File outfile = new File(f.getParent(),f.getName()+"-out.tt");
			if (!applyTT(infile, outfile, modelfile)) {
				return false;
			}
			
			//			//Reconvert encoding if needed
			//			if (modelfile.getName().equals("sp.par")) {
			//				if(debug)
			//					println "convert "+f+" latin1 >> UTF-8"
			//				new EncodingConverter(f, "ISO-8859-1", "UTF-8")
			//			}
			
			
			
			initTTOutfileInfos(binDir, modelfile, modelfilename);
			
			File annotfile = new File(binDir, "annotations/"+outfile.getName()+"-STOFF.xml");
			if (!writeStandoffFile(outfile, annotfile)) {
				println("Failed to build standoff file of "+outfile);
			}
			if (cancelNow) return;
		}
		println("")
		
		if (cancelNow) {
			return false;
		}
		
		//INJECT ANNOTATIONS
		List<File> interpfiles = new File(binDir, "annotations").listFiles(IOUtils.HIDDENFILE_FILTER);
		List<File> txmfiles = txmDir.listFiles(IOUtils.HIDDENFILE_FILTER);
		if (txmfiles == null) {
			println "No file to annotate in "+txmDir.getAbsolutePath()
			return false;
		}
		interpfiles.sort(); // same order
		txmfiles.sort(); //same order
		println "Injecting stdoff files ("+interpfiles.size()+") data from "+new File(binDir, "annotations")+ " to xml-txm files of "+txmDir;
		if (interpfiles == null || interpfiles.size() == 0)
			return false;
		cpb = new ConsoleProgressBar(interpfiles.size())
		for (int i = 0 ; i < interpfiles.size() ; i++) {
			cpb.tick()
			File srcfile = txmfiles.get(i);
			File pos1file = interpfiles.get(i);
			File temp = File.createTempFile("Annotate", "temp", srcfile.getParentFile());
			def builder = new AnnotationInjection(srcfile.toURI().toURL(), pos1file.toURI().toURL());
			if (!builder.process(temp)) {
				return false;
			}
			builder = null;
			
			//println "renaming files..."
			if (!(srcfile.delete() && temp.renameTo(srcfile)))
				println "Warning can't rename file "+temp+" to "+srcfile
		}
		
		println("")
		return true;
	}
	
	public void setModelsDirectory(File modelsDirectory) {
		this.modelsDirectory = modelsDirectory;
	}
	
	/**
	 * Run file by file. Allow to have one different lang per file. Default behavior add new word properties
	 *
	 * @param binDir
	 * @param txmDir
	 * @param lang associate a file name with a model filename
	 * @return true, if successful
	 */
	public boolean run(File binDir, File txmDir, HashMap<String, String> langs) {
		return run(binDir, txmDir, langs, false, new String[0], new String[0]);
	}
	
	/**
	 * Run file by file. Allow to have one different lang per file
	 *
	 * @param binDir 
	 * @param txmDir
	 * @param lang associate a file name with a model filename
	 * @param replace, replace or create a word property
	 * @return true, if successful
	 */
	public boolean run(File binDir, File txmDir, HashMap<String, String> langs, boolean replace, String[] properties, String[] options) {
		
		if (!new File(TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.INSTALL_PATH)+"/bin/").exists()) {
			println("Path to TreeTagger is wrong "+TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.INSTALL_PATH)+"/bin/")
			return true;
		}
		
		List<File> listfiles = txmDir.listFiles(IOUtils.HIDDENFILE_FILTER);
		
		//cleaning
		File annotDir = new File(binDir,"annotations");
		annotDir.deleteDir();
		annotDir.mkdir();
		File ptreetaggerDir = new File(binDir,"ptreetagger");
		ptreetaggerDir.deleteDir();
		ptreetaggerDir.mkdir();
		File treetaggerDir = new File(binDir,"treetagger");
		treetaggerDir.deleteDir();
		treetaggerDir.mkdir();
		
		def files = txmDir.listFiles(IOUtils.HIDDENFILE_FILTER) as List
		//		println "$txmDir: $files"
//		for (File txmCorpusDirectory : files) {
			
//			if (txmCorpusDirectory.isDirectory()) continue;
			
//			def files2 = txmCorpusDirectory.listFiles(IOUtils.HIDDENFILE_FILTER) as List
			//			println "$txmCorpusDirectory: $files2"
			ConsoleProgressBar cpb = new ConsoleProgressBar(files.size())
			//for (File teiFile : files2) {
			files.parallelStream().forEach() { f->
				int counter = 1;
				
				if (langs.get(f.getName()) == null) {
					println "Error: no lang defined for file $f"
					return;
				}
				
				String lang = langs.get(f.getName());
				run(f, lang, binDir, txmDir, replace, properties, options, annotDir, ptreetaggerDir, treetaggerDir)
				
				cpb.tick();
				
			}
			cpb.done();
//		}
		
		return true;
	}
	
	public boolean run(File f, String lang, boolean fixExistingValues, File binDir, File txmDir) {
		
		File annotDir = new File(binDir,"annotations");
		annotDir.mkdir();
		File ptreetaggerDir = new File(binDir,"ptreetagger");
		ptreetaggerDir.mkdir();
		File treetaggerDir = new File(binDir,"treetagger");
		treetaggerDir.mkdir();
		
		return run(f, lang, binDir, txmDir, fixExistingValues, new String[0], new String[0], annotDir, ptreetaggerDir, treetaggerDir)
	}
	
	public boolean run(File f, String lang, File binDir, File txmDir, boolean fixExistingValues, String[] properties, String[] options, File annotDir, File ptreetaggerDir, File treetaggerDir) {
		
		File modelfile = new File(modelsDirectory, lang+".par");
		if (!"??".equals(lang) && !modelfile.exists()) {
			println "Error: No Modelfile available for lang "+modelfile+" (and no lang detection set). Continue import process ";
			return false;
		}
		File annotfile = new File(annotDir, f.getName()+"-STDOFF.xml");
		File ttsrcfile = new File(ptreetaggerDir, f.getName()+"-src.tt");
		File ttrezfile = new File(treetaggerDir, f.getName()+"-out.tt");
		//println ("TT with $model "+f+"+"+annotfile+" > "+ttsrcfile+" > "+ttrezfile);
		
		//BUILD TT FILE READY TO BE TAGGED
		def builder = new BuildTTSrc(f.toURL());
		builder.process(ttsrcfile, null);
		
		String tmpModelFileName = modelfile.getName()
		if (tmpModelFileName.startsWith("??")) {
			if (f.length() > LangDetector.MINIMALSIZE) {
				
				String dlang = new LangDetector(f).getLang();
				//println "$f detected lang is $dlang"
				tmpModelFileName = dlang+".par"
				println "Lang detection activated lang="+dlang
			} else {
				tmpModelFileName = Locale.getDefault().getCountry().toLowerCase()+".par"
				println "Lang detection activated but the input file is too small: using the defaut lang="+Locale.getDefault().getCountry().toLowerCase()
			}
		}
		modelfile = new File(modelsDirectory, tmpModelFileName);
		
		//Apply TT
		applyTT(ttsrcfile, ttrezfile, modelfile, options);
		
		//CREATE STANDOFF FILES
		initTTOutfileInfos(binDir, modelfile, lang, properties);
		writeStandoffFile(ttrezfile, annotfile)
		
		//INJECT ANNOTATIONS
		File tmpFile = new File(txmDir, "temp_"+f.getName())
		builder = new AnnotationInjection(f.toURL(), annotfile.toURL(), fixExistingValues);
		builder.process(tmpFile);
		if (!(f.delete() && tmpFile.renameTo(f))) println "Warning can't rename file "+tmpFile+" to "+f
		
		return f.exists();
	}
	
	public void setCancelNow() {
		cancelNow = true;
	}
	
}