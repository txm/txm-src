package org.txm.treetagger.core.preferences;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.eclipse.osgi.util.NLS;
import org.osgi.framework.Version;
import org.osgi.service.prefs.Preferences;
import org.txm.Toolbox;
import org.txm.core.preferences.TXMPreferences;
import org.txm.utils.BundleUtils;
import org.txm.utils.io.FileCopy;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

/**
 * Default preferences initializer.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class TreeTaggerPreferences extends TXMPreferences {


	/**
	 * contains the last bundle version setting the TreeTagger binariesl directory
	 */
	public static final String INSTALLED_BINARIES_VERSION = "installed_binaries_version"; //$NON-NLS-1$

	/**
	 * contains the last bundle version setting the TreeTagger models directory
	 */
	public static final String INSTALLED_MODELS_VERSION = "installed_models_version"; //$NON-NLS-1$

	/**
	 * Installation path.
	 */
	public static final String INSTALL_PATH = "install_path"; //$NON-NLS-1$

	/**
	 * Models path.
	 */
	public static final String MODELS_PATH = "models_path"; //$NON-NLS-1$

	//public static final String OPTIONS = "options"; //$NON-NLS-1$

	public static final String OPTIONS_LEX = "lex"; //$NON-NLS-1$

	public static final String OPTIONS_WC = "wc"; //$NON-NLS-1$

	public static final String OPTIONS_UNKNOWN = "unknown"; //$NON-NLS-1$

	public static final String OPTIONS_DEBUG = "debug"; //$NON-NLS-1$

	public static final String OPTIONS_HYPHENHEURISTIC = "hyphen_heuristic"; //$NON-NLS-1$

	public static final String OPTIONS_PROB = "prob"; //$NON-NLS-1$

	public static final String OPTIONS_CL = "cl"; //$NON-NLS-1$

	public static final String OPTIONS_DTG = "dtg"; //$NON-NLS-1$

	public static final String OPTIONS_SW = "sw"; //$NON-NLS-1$

	public static final String OPTIONS_ATG = "atg"; //$NON-NLS-1$

	public static final String OPTIONS_ECW = "ecw"; //$NON-NLS-1$

	public static final String OPTIONS_LT = "lt"; //$NON-NLS-1$

	public static final String FIX_APOSTROPHES = "fix_apostrophes"; //$NON-NLS-1$



	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(TreeTaggerPreferences.class)) {
			new TreeTaggerPreferences();
		}
		return TXMPreferences.instances.get(TreeTaggerPreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		// FIXME: SJ: some code in this method should only be done at first run or update (eg. modifying chmod, etc.)


		// Default preferences if no org.txm.treetagger.core fragment is found
		Preferences preferences = this.getDefaultPreferencesNode();

		// FIXME: SJ: became useless since we embed it in fragments?
		String installPath = "/usr/lib/treetagger"; //"System.getProperty("osgi.user.area") + "/TXM/treetagger"; //$NON-NLS-1$
		if (System.getProperty("os.name").contains("Windows")) {//$NON-NLS-1$ //$NON-NLS-2$
			installPath = "C:/Program Files/TreeTagger"; //$NON-NLS-1$
		}
		else if (System.getProperty("os.name").contains("Mac")) { //$NON-NLS-1$ //$NON-NLS-2$
			installPath = "/Applications/TreeTagger"; //$NON-NLS-1$
		}

		preferences.put(INSTALL_PATH, installPath);
		preferences.put(MODELS_PATH, installPath + "/models"); //$NON-NLS-1$
		preferences.putBoolean(FIX_APOSTROPHES, false);

		preferences.putBoolean(OPTIONS_DEBUG, false);
		preferences.putBoolean(OPTIONS_HYPHENHEURISTIC, false);
		preferences.putBoolean(OPTIONS_UNKNOWN, true);
		preferences.putBoolean(OPTIONS_PROB, false);

		// FIXME: need to validate this code + need to check if it's still useful
		String bversion = TreeTaggerPreferences.getInstance().getString(INSTALLED_BINARIES_VERSION);
		String mversion = TreeTaggerPreferences.getInstance().getString(INSTALLED_MODELS_VERSION);

		// if TXM is launch for the first time bversion and mversion valus are empty
		if (bversion == null || bversion.equals("") || mversion == null || mversion.equals("")) {

			// Restore previous TreeTagger preferences
			File previousPreferenceFile = new File(System.getProperty("java.io.tmpdir"), "org.txm.rcp.prefs"); //$NON-NLS-1$ //$NON-NLS-2$

			if (System.getProperty("os.name").indexOf("Mac") >= 0) { //$NON-NLS-1$ //$NON-NLS-2$
				previousPreferenceFile = new File("/tmp/org.txm.rcp.prefs"); //$NON-NLS-1$
			}

			if (previousPreferenceFile.exists()) {
				try {
					Log.info(NLS.bind("Restoring preferences from \"{0}\".", previousPreferenceFile)); //$NON-NLS-1$
					Properties previousProperties = new Properties();
					BufferedReader reader = IOUtils.getReader(previousPreferenceFile, "ISO-8859-1"); //$NON-NLS-1$
					previousProperties.load(reader);

					String[] keys = { INSTALL_PATH, MODELS_PATH };
					for (String k : keys) {
						if (previousProperties.getProperty(previousProperties.getProperty(k)) != null) {
							TreeTaggerPreferences.getInstance().put(k, previousProperties.getProperty(k));
						}
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}

			TreeTaggerPreferences.getInstance().put(INSTALLED_BINARIES_VERSION, "0.0.0"); //$NON-NLS-1$
			TreeTaggerPreferences.getInstance().put(INSTALLED_MODELS_VERSION, "0.0.0"); //$NON-NLS-1$

			bversion = TreeTaggerPreferences.getInstance().getString(INSTALLED_BINARIES_VERSION);
			mversion = TreeTaggerPreferences.getInstance().getString(INSTALLED_MODELS_VERSION);
		}

		// look for org.txm.treetagger.core.<osname> fragment
		String mfragmentid = "org.txm.treetagger.core.models"; //$NON-NLS-1$
		String bfragmentid = "org.txm.treetagger.core"; //$NON-NLS-1$
		String osname = System.getProperty("os.name").toLowerCase();
		if (osname.contains("windows")) { //$NON-NLS-1$
			osname = "win32"; //$NON-NLS-1$
		}
		else if (osname.contains("mac os x")) { //$NON-NLS-1$
			osname = "macosx"; //$NON-NLS-1$
		}
		else {
			osname = "linux"; //$NON-NLS-1$
		}
		bfragmentid += "." + osname; //$NON-NLS-1$

		Version currentBVersion = new Version(bversion);
		Version currentMVersion = new Version(mversion);
		Version binariesFragmentVersion = BundleUtils.getBundleVersion(bfragmentid);
		if (binariesFragmentVersion != null && binariesFragmentVersion.compareTo(currentBVersion) >= 0) { // udpate binaries path !
			Log.fine("Updating TreeTagger binaries path..."); //$NON-NLS-1$
			File path = BundleUtils.getBundleFile(bfragmentid);
			File binariesDir = new File(path, "res/" + osname); //$NON-NLS-1$
			new File(binariesDir, "bin/separate-punctuation").setExecutable(true); // linux&mac //$NON-NLS-1$
			new File(binariesDir, "bin/tree-tagger").setExecutable(true); // linux&mac //$NON-NLS-1$
			new File(binariesDir, "bin/train-tree-tagger").setExecutable(true); // linux&mac //$NON-NLS-1$
			preferences.put(INSTALL_PATH, binariesDir.getAbsolutePath());
			TreeTaggerPreferences.getInstance().put(INSTALLED_BINARIES_VERSION, binariesFragmentVersion.toString());
			Log.fine("Done."); //$NON-NLS-1$
		}

		Version modelsFragmentVersion = BundleUtils.getBundleVersion(mfragmentid);
		if (modelsFragmentVersion != null && modelsFragmentVersion.compareTo(currentMVersion) >= 0) { // udpate models path!
			Log.fine("Updating TreeTagger models path..."); //$NON-NLS-1$
			File path = BundleUtils.getBundleFile(mfragmentid);

			File installModelsDir = new File(path, "res/models"); //$NON-NLS-1$
			File modelsDir = new File(Toolbox.getTxmHomePath(), "treetagger-models"); //$NON-NLS-1$
			modelsDir.mkdirs();
			try {
				FileCopy.copyFiles(installModelsDir, modelsDir);
				preferences.put(MODELS_PATH, modelsDir.getAbsolutePath()); //$NON-NLS-1$
				TreeTaggerPreferences.getInstance().put(INSTALLED_MODELS_VERSION, modelsFragmentVersion.toString());
				Log.fine("Done."); //$NON-NLS-1$
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
