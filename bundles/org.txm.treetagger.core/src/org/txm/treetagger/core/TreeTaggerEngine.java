package org.txm.treetagger.core;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.osgi.util.NLS;
import org.txm.core.results.TXMResult;
import org.txm.importer.xmltxm.Annotate;
import org.txm.importer.xmltxm.AnnotateCQP;
import org.txm.nlp.core.NLPEngine;
import org.txm.treetagger.core.preferences.TreeTaggerPreferences;
import org.txm.utils.FileUtils;
import org.txm.utils.logger.Log;
import org.txm.utils.treetagger.TreeTagger;


public class TreeTaggerEngine extends NLPEngine {

	private File ttModelsDirectory;

	private File ttBinaryDirectory;

	protected HashMap<File, TreeTagger> instances = new HashMap<>();

	private String[] options;

	@Override
	public boolean isRunning() {

		ttBinaryDirectory = new File(TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.INSTALL_PATH) + "/bin/");
		if (!ttBinaryDirectory.exists()) {
			Log.fine(NLS.bind("** Error: TreeTagger software not found in the {0} directory.", ttBinaryDirectory));
			return false;
		}

		ttModelsDirectory = new File(TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.MODELS_PATH));
		if (!ttModelsDirectory.exists()) {
			Log.fine(NLS.bind("** Error: TreeTagger models not found in the {0} directory.", ttModelsDirectory));
			return false;
		}

		// options = TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.OPTIONS).split(" ");

		return ttBinaryDirectory != null && ttBinaryDirectory.exists() && ttModelsDirectory != null && ttModelsDirectory.exists();
	}

	@Override
	public boolean stop() throws Exception {

		ttBinaryDirectory = null;
		ttModelsDirectory = null;
		for (TreeTagger tt : instances.values()) {
			tt.kill();
		}
		instances.clear();
		return false;
	}

	@Override
	public String getName() {

		return "TreeTagger";
	}

	@Override
	public boolean initialize() {

		return true;
	}

	@Override
	public boolean start(IProgressMonitor monitor) {

		return true;
	}

	public TreeTagger getInstance(File modelFile) {

		if (!instances.containsKey(modelFile)) {
			instances.put(modelFile, new TreeTagger(ttBinaryDirectory.getAbsolutePath(), options));
		}
		return instances.get(modelFile);
	}

	@Override
	public boolean processFile(File xmlFile, File binaryCorpusDirectory, HashMap<String, Object> parameters) {

		if (!isRunning()) return false;

		String lang = null;
		Object ps = parameters.get("langs"); //$NON-NLS-1$
		Object p = parameters.get("lang"); //$NON-NLS-1$

		if (p == null && ps == null) {
			Log.warning("Warning: can't annotate. No 'lang' (String) or 'langs' (Map<String, String>) parameter specified in " + parameters);
			return false;
		}

		if (ps != null && ps instanceof Map) {
			Map<?, ?> map = (Map<?, ?>) ps;
			String text_id = xmlFile.getName();
			if (map.get(text_id) != null) {
				lang = map.get(text_id).toString().toLowerCase();
				if (!canAnnotateLang(lang)) {
					Log.warning("Warning: can't annotate text_id=${text_id} with $lang, will use the default lang=$p");
					return false;
				}
			}
		}

		if (lang == null && p == null) {
			System.out.println(NLS.bind("** Error: no 'lang' parameter given: {0}. Aborting TreeTagger annotation.", parameters));
			return false;
		}
		else {
			lang = p.toString();
		}

		if (!canAnnotateLang(lang)) {
			return false;
		}

		boolean fixExistingValues = false; // default behavior is to replace existing values
		if (parameters.get("fix_existing_values") != null) {
			fixExistingValues = "true".equals(parameters.get("fix_existing_values"));
		}
		if (FileUtils.isExtension(xmlFile, "cqp")) {
			AnnotateCQP annotate = new AnnotateCQP();
			return annotate.run(xmlFile, lang, binaryCorpusDirectory, xmlFile.getParentFile());
		}
		else {
			Annotate annotate = new Annotate();
			return annotate.run(xmlFile, lang, fixExistingValues, binaryCorpusDirectory, xmlFile.getParentFile());
		}
	}

	/**
	 * if the 'langs' HashMap<String=xml file path, String=lang code> parameter is set, then the Annotate class will do multi-threading to process the files
	 * 
	 * @return true if success
	 */
	@Override
	public boolean processDirectory(File xmlFilesDirectory, File binaryCorpusDirectory, HashMap<String, Object> parameters) {

		Object p = parameters.get("langs");
		if (p != null && p instanceof HashMap<?, ?>) {

			Annotate annotate = new Annotate();
			HashMap<String, String> langs = (HashMap<String, String>) p;
			return annotate.run(binaryCorpusDirectory, new File(binaryCorpusDirectory, "txm"), langs);
		}
		else {
			p = parameters.get("lang");
			if (p == null) {
				Log.severe(NLS.bind("** Error: no annotation language set to annotate the {0} directory.", xmlFilesDirectory));
				return false;
			}
			String lang = p.toString();
			if (!canAnnotateLang(lang)) {
				return false;
			}
			return super.processDirectory(xmlFilesDirectory, binaryCorpusDirectory, parameters);
		}
	}

	public static boolean canAnnotateLang(String lang) {

		if (lang == null) {
			Log.warning("** Error: not lang given (null)");
			return false;
		}
		File ttInstallDirectory = new File(TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.INSTALL_PATH)); // default models directory is set in the Toolbox
		if (!ttInstallDirectory.exists()) {
			Log.warning(NLS.bind("** Error: TreeTagger install directory not found at {0}", ttInstallDirectory));
			return false;
		}
		File modelsDirectory = new File(TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.MODELS_PATH)); // default models directory is set in the Toolbox
		File modelfile = new File(modelsDirectory, lang + ".par"); //$NON-NLS-1$
		if (!"??".equals(lang) && !modelfile.exists()) { //$NON-NLS-1$
			Log.warning(NLS.bind("** Error: no {0} model file found for the {1} lang.", modelfile, lang));
			return false;
		}
		return true;
	}

	@Override
	public void notify(TXMResult r, String state) {

	}

	@Override
	public String getDetails() {

		return "TreeTagger files: binaries=" + ttBinaryDirectory + " models=" + ttModelsDirectory + " current instances=" + instances.toString();
	}

}
