// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.ahc.rcp.adapters;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.FrameworkUtil;
import org.txm.ahc.core.functions.AHC;
import org.txm.ca.rcp.adapters.CAAdapterFactory;
import org.txm.rcp.adapters.TXMResultAdapter;
import org.txm.rcp.adapters.TXMResultAdapterFactory;

/**
 * A factory for creating Adapter objects.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class AHCAdapterFactory extends TXMResultAdapterFactory {


	public static final ImageDescriptor ICON = AbstractUIPlugin.imageDescriptorFromPlugin(
			FrameworkUtil.getBundle(AHCAdapterFactory.class).getSymbolicName(),
			"platform:/plugin/" + FrameworkUtil.getBundle(AHCAdapterFactory.class).getSymbolicName() + "/icons/ahc.png"); //$NON-NLS-1$ //$NON-NLS-2$
	
	

	/**
	 * Gets the adapter.
	 *
	 * @param adaptableObject the adaptable object
	 * @param adapterType the adapter type
	 * @return the adapter
	 * @see org.eclipse.core.runtime.IAdapterFactory#getAdapter(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T getAdapter(Object adaptableObject, Class<T> adapterType) {
		if (this.canAdapt(adapterType) && adaptableObject instanceof AHC) {
			return adapterType.cast(new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return ICON;
				}
			});
		}
		return null;
	}


}
