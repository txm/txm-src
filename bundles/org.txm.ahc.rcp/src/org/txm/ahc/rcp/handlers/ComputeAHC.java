// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.ahc.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.ahc.core.functions.AHC;
import org.txm.ahc.core.messages.AHCCoreMessages;
import org.txm.ahc.rcp.editors.AHCEditor;
import org.txm.ahc.rcp.messages.AHCUIMessages;
import org.txm.ca.core.functions.CA;
import org.txm.ca.rcp.editors.CAFactorialMapChartEditor;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.editors.ChartEditorInput;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.index.core.functions.PartitionIndex;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.utils.logger.Log;

/**
 * Opens an AHC editor.
 * Computes the result if it contains all required parameters.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class ComputeAHC extends BaseAbstractHandler {


	@Override
	public AHC execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkStatsEngine()) {
			return null;
		}

		Object selection = this.getCorporaViewSelectedObject(event);
		AHC ahc = null;

		final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
		try {
			// reopening an existing result
			if (selection instanceof AHC ahc2) {
				ahc = ahc2;
			}
			// Creating from CA
			else if (selection instanceof CA) { // Creating

				CA ca = (CA) selection;
				ahc = new AHC(ca);
			}
			// Creating from Partition Index
			else if (selection instanceof PartitionIndex pIndex) {

				LexicalTable lexicalTable = new LexicalTable(pIndex);
				lexicalTable.setVisible(false);
				
				CA ca = new CA(lexicalTable);
				ca.setVisible(false);
				
				ahc = new AHC(ca);
			}
			// Creating from Lexical Table
			else if (selection instanceof LexicalTable lexicalTable) {

				// FIXME: SJ: problem here because we need the LT to be computed to make this check
				if (lexicalTable.hasBeenComputedOnce() && lexicalTable.getNColumns() < 3) {
					MessageDialog.openError(window.getShell(), AHCCoreMessages.Error, AHCCoreMessages.error_canNotComputeAHCOnLexicalTableWithLessThan4Columns);
					return null;
				}
				// FIXME: SJ: problem here because we need the LT has been computed to make this check
				if (lexicalTable.hasBeenComputedOnce() && lexicalTable.getNRows() < 3) {
					MessageDialog.openError(window.getShell(), AHCCoreMessages.Error, AHCCoreMessages.error_canNotComputeAHCWithLessThan4RowsInTheTable);
					return null;
				}

				CA ca = new CA(lexicalTable);
				ca.setVisible(false);
				
				ahc = new AHC(ca);
			}
			// Creating from Partition
			else if (selection instanceof Partition partition) {

				if (partition.getPartsCount() < 3) {
					MessageDialog d = new MessageDialog(window.getShell(), TXMCoreMessages.error_error, null,
							NLS.bind(AHCUIMessages.error_canNotComputeAHCWithP0, partition.getPartsCount()), 0, new String[] { TXMCoreMessages.common_ok }, 0);
					d.open();
					return null;
				}


				LexicalTable lexicalTable = new LexicalTable(partition); // use the CAPreferencePage Fmin
				lexicalTable.setVisible(false);
				
				CA ca = new CA(lexicalTable);
				ca.setVisible(false);
				
				ahc = new AHC(ca);
			}

			open(ahc, true);

		}
		catch (Exception e) {
			Log.printStackTrace(e);
			Log.severe(AHCCoreMessages.bind(AHCCoreMessages.failToBuildTheAHCForSelectionP0DbldotP1, selection, e.getMessage()));
			return null;
		}
		return ahc;
	}

	public static void open(AHC ahc, boolean openInfos) {
		// TODO Auto-generated method stub
		ChartEditorInput<AHC> editorInput = new ChartEditorInput<>(ahc);
		boolean wasOpened = SWTEditorsUtils.isOpenEditor(editorInput, CAFactorialMapChartEditor.class.getName());
		ChartEditor<?> editor = ChartEditor.openEditor(ahc);
		if (openInfos && !wasOpened && editor != null && editor instanceof AHCEditor ahcEditor) {

			//SWTEditorsUtils.boxEditor(caFactorEditor, null, false); // not working yet

			//				ComputeAHCIndividualsInfos.open(ahc);
			//				ComputeAHCVariablesInfos.open(ahc);
			ComputeAHCInertiasInfo.open(ahc);
			ahcEditor.setFocus();
		}
	}
}
