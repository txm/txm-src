// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.ahc.rcp.handlers;

import java.util.Set;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.txm.ahc.core.functions.AHC;
import org.txm.ahc.core.functions.AHCInertiasInfo;
import org.txm.ahc.core.messages.AHCCoreMessages;
import org.txm.ahc.rcp.editors.AHCColsRowsInfosEditor;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.ITXMResultEditor;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.utils.logger.Log;

/**
 * Opens an AHC Variables infos editor.
 * 
 * @author mdecorde
 * 
 */
public class ComputeAHCInertiasInfo extends BaseAbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkStatsEngine()) {
			return null;
		}

		Object selection = this.getCorporaViewSelectedObject(event);

		return open(selection);
	}

	public static AHCColsRowsInfosEditor open(Object selection) {

		if (selection == null) return null;

		AHCInertiasInfo infos = null;

		try {
			if (selection instanceof AHC ahc) {
				infos = ahc.getFirstChild(AHCInertiasInfo.class);
				if (infos == null) {
					infos = new AHCInertiasInfo(ahc);
				}
			}
			else if (selection instanceof AHCInertiasInfo) { // Creating

				infos = (AHCInertiasInfo) selection;
			}

			TXMEditor<?> editor = (TXMEditor<?>) SWTEditorsUtils.getEditor(infos);
			if (editor == null) {
				editor = TXMEditor.openEditor(infos, AHCColsRowsInfosEditor.class.getName());
				if (editor != null && editor instanceof AHCColsRowsInfosEditor e2) {
					Set<ITXMResultEditor<TXMResult>> parentEditors = SWTEditorsUtils.getEditors(infos.getParent());
					for (ITXMResultEditor<TXMResult> parentEditor : parentEditors) {
						SWTEditorsUtils.moveEditor((TXMEditor<?>) parentEditor, e2, EModelService.RIGHT_OF, 0.3f);
						break;
					}
					return e2;
				}
			}
			else {
				editor.getSite().getPage().activate(editor);
				return (AHCColsRowsInfosEditor) editor;
			}

		}
		catch (Exception e) {
			Log.printStackTrace(e);
			Log.severe(AHCCoreMessages.bind(AHCCoreMessages.failToBuildTheAHCForSelectionP0DbldotP1, selection, e.getMessage()));
		}
		return null;
	}
}
