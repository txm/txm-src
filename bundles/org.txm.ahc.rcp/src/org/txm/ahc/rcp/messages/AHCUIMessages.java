package org.txm.ahc.rcp.messages;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.messages.Utf8NLS;

/**
 * AHC UI messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class AHCUIMessages extends TXMCoreMessages {


	private static final String BUNDLE_NAME = "org.txm.ahc.rcp.messages.messages"; //$NON-NLS-1$

	public static String determinesTheCutoffLevelOfTheClassDendrogram;
	public static String linkingMethodForHierarchicalClustering;
	public static String distanceUsedByTheAlgorithm;
	public static String cahClassesAreConsolidatedByPostClustering;
	public static String clustersConsolidation;
	public static String common_metric;
	public static String computeColumns;
	public static String computeRows;
	public static String displayChartsIn2D;
	public static String error_canNotComputeAHCWithP0;
	public static String ifTheNumberOfColumntoProcessEtc;
	public static String kmeansPreProcessLimit;
	public static String method;
	public static String numberOfClusters;
	public static String numberOfClustersColon;
	public static String numberOfAxesUsed;
	public static String postProcesses;
	public static String structuralUnitUsedToBuildTheLexicalTable;
	public static String theNumberOfClustersToBuild;
	public static String tooltip_displayTheClustersTreeIn2D3D;
	public static String tooltip_showColumnsInformation;
	public static String tooltip_showFactorsInformation;
	public static String tooltip_showInertiasInformation;
	public static String tooltip_showRowsInformation;
	public static String tooltip_showTheClustersAnalysis;
	
	public static String numberOfFactorsDataStoredInTheInternalRVariableTheGreaterTheBetter;
	public static String ifSetAKMeansConsolidationOfTheClustersIsPerformed;
	public static String in;

	public static String classificationOf;

	static {
		Utf8NLS.initializeMessages(BUNDLE_NAME, AHCUIMessages.class);
	}


}
