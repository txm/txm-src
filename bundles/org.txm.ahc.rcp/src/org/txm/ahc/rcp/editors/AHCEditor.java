package org.txm.ahc.rcp.editors;

import java.util.HashMap;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.ToolItem;
import org.txm.ahc.core.functions.AHC;
import org.txm.ahc.core.functions.AHCColsInfo;
import org.txm.ahc.core.functions.AHCFactorsInfo;
import org.txm.ahc.core.functions.AHCInertiasInfo;
import org.txm.ahc.core.functions.AHCInfos;
import org.txm.ahc.core.functions.AHCRowsInfo;
import org.txm.ahc.core.messages.AHCCoreMessages;
import org.txm.ahc.core.preferences.AHCPreferences;
import org.txm.ahc.rcp.handlers.ComputeAHCColsInfo;
import org.txm.ahc.rcp.handlers.ComputeAHCFactorsInfo;
import org.txm.ahc.rcp.handlers.ComputeAHCInertiasInfo;
import org.txm.ahc.rcp.handlers.ComputeAHCRowsInfo;
import org.txm.ahc.rcp.messages.AHCUIMessages;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.preferences.CAPreferences;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.index.core.functions.PartitionIndex;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.ITXMResultEditor;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.widget.BooleanCombo;
import org.txm.rcp.swt.widget.TXMParameterCombo;
import org.txm.rcp.swt.widget.TXMParameterSpinner;
import org.txm.rcp.swt.widget.ThresholdsGroup;
import org.txm.rcp.swt.widget.structures.PropertiesComboViewer;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Partition;

/**
 * AHC chart editor.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class AHCEditor extends ChartEditor<AHC> {


	//	/**
	//	 * Rows computing button.
	//	 */
	//	protected ToolItem rowsComputing;

	/**
	 * Unit property.
	 */
	@Parameter(key = TXMPreferences.UNIT_PROPERTY)
	protected PropertiesComboViewer unitPropertyComboViewer;

	/**
	 * Minimum frequency filtering spinner.
	 */
	@Parameter(key = TXMPreferences.F_MIN)
	protected Spinner fMinSpinner;

	/**
	 * Minimum frequency filtering spinner.
	 */
	@Parameter(key = TXMPreferences.F_MAX)
	protected Spinner fMaxSpinner;

	/**
	 * Maximum number of lines filtering.
	 */
	@Parameter(key = TXMPreferences.V_MAX)
	protected Spinner vMaxSpinner;


	/**
	 * The metric (euclidean, manhattan).
	 */
	@Parameter(key = AHCPreferences.METRIC)
	protected String metric;

	/**
	 * The method (METHODS <- c("average", "single", "complete", "ward", "weighted", "flexible")).
	 */
	@Parameter(key = AHCPreferences.METHOD)
	protected String method;

	/**
	 * The number of clusters.
	 */
	@Parameter(key = AHCPreferences.N_CLUSTERS)
	protected Spinner numberOfClusters;

	/**
	 * The number of clusters.
	 */
	@Parameter(key = AHCPreferences.N_MAX_CLUSTER_TO_PROCESS)
	protected Spinner maximumColumnsNumbertoProcess;

	/**
	 * To compute the columns or the rows.
	 */
	@Parameter(key = AHCPreferences.COLUMNS_COMPUTING)
	protected BooleanCombo columnsComputing;
	protected Label columnsComputingLabel;

	/**
	 * To render in 2D or 3D.
	 */
	@Parameter(key = AHCPreferences.RENDERING)
	protected Combo rendering;

	/**
	 * Maximum number of lines filtering.
	 */
	@Parameter(key = AHCPreferences.CONSOLIDATION)
	protected Button conslidationButton;

	/**
	 * Maximum number of lines filtering.
	 */
	@Parameter(key = CAPreferences.N_FACTORS_MEMORIZED)
	protected Spinner nCoordsSavedSpinner;


	HashMap<Class<? extends AHCInfos>, AHCColsRowsInfosEditor> infosEditors = new HashMap<Class<? extends AHCInfos>, AHCColsRowsInfosEditor>();



	@Override
	public void __createPartControl() throws Exception {

		// Main paramaters area
		GLComposite mainParametersArea = this.getMainParametersComposite();
		mainParametersArea.getLayout().numColumns = 5;

		// parent parameters
		TXMResult ca = this.getResult().getParent();
		if (ca instanceof CA && ca.isVisible()) {
			// ok the CA shows the unitPropertyComboViewer
		}
		else {
			TXMResult lt = ca.getParent();
			if (lt instanceof LexicalTable && lt.isVisible()) {
				// ok the LT shows the unitPropertyComboViewer
			}
			else {
				TXMResult parent = lt.getParent();
				if (parent instanceof PartitionIndex) {
					// ok the LT shows the unitPropertyComboViewer
				}
				else if (parent instanceof Partition) {
					new Label(mainParametersArea, SWT.NONE).setText(TXMCoreMessages.common_property);
					this.unitPropertyComboViewer = new PropertiesComboViewer(mainParametersArea, this, false,
							CQPCorpus.getFirstParentCorpus(this.getResult()).getOrderedProperties(),
							this.getResult().getUnitProperty(), false);
					unitPropertyComboViewer.getCombo().setToolTipText(AHCUIMessages.structuralUnitUsedToBuildTheLexicalTable);
				}
			}
		}
		//		if (!this.getResult().getParent().isVisible() // CA parent parent is not visible
		//				&& !(this.getResult().getParent().getParent().getParent() instanceof PartitionIndex)) { // and its 2x parent is not a PartitionIndex
		//			// unit property
		//			
		//		}



		// Computing listeners
		ComputeSelectionListener computeSelectionListener = new ComputeSelectionListener(this, true);

		// Columns
		
		this.columnsComputingLabel = new Label(mainParametersArea, SWT.NONE);
		this.columnsComputingLabel.setText(AHCUIMessages.classificationOf);
		
		this.columnsComputing = new BooleanCombo(mainParametersArea, SWT.READ_ONLY | SWT.SINGLE, AHCCoreMessages.columns, AHCCoreMessages.rows);
		this.columnsComputing.setToolTipText(AHCUIMessages.computeColumns);
//		this.columnsComputing.addSelectionListener(new SelectionListener() {
//
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				
//				if (columnsComputing.getSelection()) {
//					columnsComputing.setText(AHCCoreMessages.columns);
//					columnsComputing.setToolTipText(AHCUIMessages.computeColumns);
//				} else {
//					columnsComputing.setText(AHCCoreMessages.rows);
//					columnsComputing.setToolTipText(AHCUIMessages.computeRows);
//				}
//			}
//
//			@Override
//			public void widgetDefaultSelected(SelectionEvent e) { }
//		});
		//this.columnsComputing.setImage(IImageKeys.getImage(AHCEditor.class, "icons/compute_columns.png")); //$NON-NLS-1$
		//		// need to reset view and selection when changing mode
		//		this.columnsComputing.addSelectionListener( new SelectionListener() {
		//			
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				getResult().setNeedsToResetView(true);
		//				getResult().setNeedsToClearItemsSelection(true);
		//			}
		//			
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) {
		//				
		//				// TODO Auto-generated method stub
		//				
		//			}
		//		});
		this.columnsComputing.addSelectionListener(computeSelectionListener);

		// Number of clusters
		TXMParameterSpinner clusters = new TXMParameterSpinner(mainParametersArea, this, AHCCoreMessages.clusters);
		this.numberOfClusters = clusters.getControl();
		this.numberOfClusters.setToolTipText(AHCUIMessages.theNumberOfClustersToBuild);
		this.numberOfClusters.setMinimum(2);
		
//		Label l = new Label(mainParametersArea, SWT.NONE);
//		l.setText(AHCCoreMessages.clusters);

		// Extend the chart editor tool bar
		new ToolItem(this.chartToolBar, SWT.SEPARATOR);

		//		// Rows
		//		this.rowsComputing = new ToolItem(this.chartToolBar, SWT.RADIO);
		//		this.rowsComputing.setToolTipText(AHCUIMessages.computeRows);
		//		this.rowsComputing.setImage(IImageKeys.getImage(AHCEditor.class, "icons/compute_rows.png")); //$NON-NLS-1$
		//		


		// open/display columns information editor
		ToolItem varInfos = new ToolItem(this.chartToolBar, SWT.PUSH);
		varInfos.setImage(IImageKeys.getImage(AHCColsRowsInfosEditor.class, "icons/show_columns_information.png")); //$NON-NLS-1$
		varInfos.setToolTipText(AHCUIMessages.tooltip_showColumnsInformation);
		varInfos.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				ComputeAHCColsInfo.open(getResult());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// open/display rows information editor
		ToolItem indInfos = new ToolItem(this.chartToolBar, SWT.PUSH);
		indInfos.setImage(IImageKeys.getImage(AHCColsRowsInfosEditor.class, "icons/show_rows_information.png")); //$NON-NLS-1$
		indInfos.setToolTipText(AHCUIMessages.tooltip_showRowsInformation);
		indInfos.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				ComputeAHCRowsInfo.open(getResult());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// open/display inertia information editor
		ToolItem inertiaInfos = new ToolItem(this.chartToolBar, SWT.PUSH);
		inertiaInfos.setImage(IImageKeys.getImage(AHCColsRowsInfosEditor.class, "icons/show_inertias_information.png")); //$NON-NLS-1$
		inertiaInfos.setToolTipText(AHCUIMessages.tooltip_showInertiasInformation);
		inertiaInfos.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				ComputeAHCInertiasInfo.open(getResult());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// open/display axes information editor
		ToolItem axesInfos = new ToolItem(this.chartToolBar, SWT.PUSH);
		axesInfos.setImage(IImageKeys.getImage(AHCColsRowsInfosEditor.class, "icons/show_axes_information.png")); //$NON-NLS-1$
		axesInfos.setToolTipText(AHCUIMessages.tooltip_showFactorsInformation);
		axesInfos.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				ComputeAHCFactorsInfo.open(getResult());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});





		// 2D/3D rendering
		TXMParameterCombo renderingParameterCombo = new TXMParameterCombo(this.getFirstLineComposite(), this, SWT.READ_ONLY, new String[] { "2D", "3D" }); //$NON-NLS-1$ //$NON-NLS-2$
		renderingParameterCombo.getControl().setToolTipText(AHCUIMessages.tooltip_displayTheClustersTreeIn2D3D);
		//renderingParameterCombo.getComputingSelectionListener().setAutoCompute(true);
		this.rendering = renderingParameterCombo.getControl();

		if (this.getResult().isRendering2D()) {
			this.rendering.select(0);
		}
		else {
			this.rendering.select(1);
		}

		this.rendering.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (rendering.getText().equals("2D")) { //$NON-NLS-1$
					getResult().setDefaultChartType();
				} else {
					getResult().setChartType("r"); //$NON-NLS-1$
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});

		if (!this.getResult().getParent().isVisible()) { // the CA is not visible

			if (!(this.getResult().getParent().getParent().getParent() instanceof PartitionIndex)) { // and its 2x parent is not a PartitionIndex
				ThresholdsGroup thresholdsGroup = new ThresholdsGroup(this.getExtendedParametersGroup(), SWT.NONE, this, true, true);
				this.fMinSpinner = thresholdsGroup.getFMinSpinner();
				this.fMaxSpinner = thresholdsGroup.getFMaxSpinner();
				this.vMaxSpinner = thresholdsGroup.getVMaxSpinner();
				this.vMaxSpinner.setMinimum(2);
				CQPCorpus corpus = getResult().getFirstParent(MainCorpus.class);
				if (corpus != null) {
					this.vMaxSpinner.setMaximum(corpus.getSize());
					this.fMaxSpinner.setMaximum(corpus.getSize());
					this.fMinSpinner.setMaximum(corpus.getSize());
				}
			}

			Group c = new Group(this.getExtendedParametersGroup(), SWT.NONE);
			c.setText(" CA");
			c.setLayout(new GridLayout(2, false));
			
//			GLComposite c = new GLComposite(this.getExtendedParametersGroup(), SWT.NONE);
//			c.getLayout().numColumns = 2;
//			c.getLayout().horizontalSpacing = 2;
			Label l2 = new Label(c, SWT.NONE);
			l2.setText(AHCUIMessages.numberOfAxesUsed);
			l2.setToolTipText(AHCUIMessages.numberOfFactorsDataStoredInTheInternalRVariableTheGreaterTheBetter);
			
			nCoordsSavedSpinner = new Spinner(c, SWT.BORDER);
			nCoordsSavedSpinner.setToolTipText(AHCUIMessages.numberOfFactorsDataStoredInTheInternalRVariableTheGreaterTheBetter);
			nCoordsSavedSpinner.setMinimum(1);
			try {
				if (!this.getResult().getLexicalTable().hasBeenComputedOnce()) this.getResult().getLexicalTable().compute(false);
				nCoordsSavedSpinner.setMaximum(Math.min(this.getResult().getLexicalTable().getNRows(), this.getResult().getLexicalTable().getNColumns()));
			}
			catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		}


		// Pre/Post processing commands
		Group c = new Group(this.getExtendedParametersGroup(), SWT.NONE);

		c.setText(" "+AHCUIMessages.postProcesses);
		GridLayout glayout = new GridLayout(2, false);
		glayout.horizontalSpacing = 2;
		c.setLayout(glayout);

		//		Label l = new Label(c, SWT.NONE);
		//		l.setText(AHCUIMessages.kmeansPreProcessLimit);
		//		l.setToolTipText(AHCUIMessages.ifTheNumberOfColumntoProcessEtc);
		//		maximumColumnsNumbertoProcess = new Spinner(c, SWT.BORDER);
		//		maximumColumnsNumbertoProcess.setMinimum(1);
		//		maximumColumnsNumbertoProcess.setMaximum(Integer.MAX_VALUE);
		//		maximumColumnsNumbertoProcess.setToolTipText(AHCUIMessages.ifTheNumberOfColumntoProcessEtc);

		conslidationButton = new Button(c, SWT.CHECK);
		conslidationButton.setText(AHCUIMessages.clustersConsolidation);
		conslidationButton.setToolTipText(AHCUIMessages.ifSetAKMeansConsolidationOfTheClustersIsPerformed);
	}


	@Override
	public void updateEditorFromChart(boolean update) {
		AHC ahc = this.getResult();

		// Update the maximum value of the number of clusters spinner according to the new AHC configuration
		this.numberOfClusters.setMaximum(ahc.getMaxClustersCount());

		// Updates the current value of the number of clusters spinner
		this.numberOfClusters.setSelection(ahc.getNumberOfClusters());

		//		this.rowsComputing.setSelection(!ahc.isColumnsComputing());

		if (!update) {
			if (this.nCoordsSavedSpinner != null && ahc.getParent() instanceof CA ca) {
				this.nCoordsSavedSpinner.setSelection(ca.getNumberOfFactorsUpTo80PercentOfRepresentation());
			}
		}
	}

	/**
	 * 
	 * @param clazz
	 * @return
	 */
	protected AHCColsRowsInfosEditor getInfosEditor(Class<? extends AHCInfos> clazz) {
		if (infosEditors.get(clazz) != null && !infosEditors.get(clazz).isDisposed()) {
			return infosEditors.get(clazz);
		}

		infosEditors.put(clazz, null);
		AHCInfos infos = getResult().getFirstChild(clazz);
		if (infos == null) return null;

		Set<ITXMResultEditor<TXMResult>> editors = SWTEditorsUtils.getEditors(infos);
		if (editors != null) {
			for (ITXMResultEditor<TXMResult> editor : editors) {
				if (editor instanceof AHCColsRowsInfosEditor eEditor) {
					infosEditors.put(clazz, eEditor);
					break;
				}
			}
		}

		return infosEditors.get(clazz);
	}

	/**
	 * 
	 * @return
	 */
	public AHCColsRowsInfosEditor getInertiaInfosEditor() {
		return getInfosEditor(AHCInertiasInfo.class);
	}

	/**
	 * 
	 * @return
	 */
	public AHCColsRowsInfosEditor getIndividualInfosEditor() {
		return getInfosEditor(AHCRowsInfo.class);
	}

	/**
	 * 
	 * @return
	 */
	public AHCColsRowsInfosEditor getAxesInfosEditor() {
		return getInfosEditor(AHCFactorsInfo.class);
	}

	/**
	 * 
	 * @return
	 */
	public AHCColsRowsInfosEditor getVariableInfosEditor() {
		return getInfosEditor(AHCColsInfo.class);
	}

	@Override
	public void onClosed() {

		if (getInertiaInfosEditor() != null) getInertiaInfosEditor().close();
		if (getIndividualInfosEditor() != null) getIndividualInfosEditor().close();
		if (getVariableInfosEditor() != null) getVariableInfosEditor().close();
		if (getAxesInfosEditor() != null) getAxesInfosEditor().close();
	}
}
