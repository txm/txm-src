// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.ahc.rcp.editors;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.ToolItem;
import org.txm.Toolbox;
import org.txm.ahc.core.functions.AHCColsInfo;
import org.txm.ahc.core.functions.AHCFactorsInfo;
import org.txm.ahc.core.functions.AHCInertiasInfo;
import org.txm.ahc.core.functions.AHCInfos;
import org.txm.ahc.core.functions.AHCRowsInfo;
import org.txm.ahc.rcp.handlers.ComputeAHC;
import org.txm.ahc.rcp.messages.AHCUIMessages;
import org.txm.ca.core.functions.CA;
import org.txm.ca.rcp.editors.CAEditor;
import org.txm.ca.rcp.editors.CAFactorialMapChartEditor;
import org.txm.core.results.TXMResult;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.ITXMResultEditor;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TXMResultEditorInput;
import org.txm.rcp.editors.TableKeyListener;
import org.txm.rcp.editors.TableLinesViewerComparator;
import org.txm.rcp.editors.TableResultEditor;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.utils.SWTEditorsUtils;

/**
 * Used to display rows and columns information of an AHC result
 * 
 * @author mdecorde
 * 
 */
public class AHCColsRowsInfosEditor extends TXMEditor<AHCInfos> implements TableResultEditor {

	/**
	 * The viewer.
	 */
	private TableViewer viewer;

	private ArrayList<TableViewerColumn> infosColumns = new ArrayList<TableViewerColumn>();

	private TableLinesViewerComparator viewerComparator;

	private TableKeyListener tableKeyListener;

	private TableViewerColumn namesColumn;

	public AHCColsRowsInfosEditor() {
	}

	/**
	 * 
	 * @param result
	 */
	public AHCColsRowsInfosEditor(AHCInfos result, final String tabTooltip) {
		super(new TXMResultEditorInput<TXMResult>(result) {

			@Override
			public String getToolTipText() {
				return tabTooltip;
			}
		});
	}

	@Override
	public void _init() {

		if (getResult() instanceof AHCColsInfo) {
			this.setTitleImage(IImageKeys.getImage(AHCColsRowsInfosEditor.class, "icons/show_columns_information.png")); //$NON-NLS-1$
		}
		else if (getResult() instanceof AHCRowsInfo) {
			this.setTitleImage(IImageKeys.getImage(AHCColsRowsInfosEditor.class, "icons/show_rows_information.png")); //$NON-NLS-1$
		}
		else if (getResult() instanceof AHCInertiasInfo) {
			this.setTitleImage(IImageKeys.getImage(AHCColsRowsInfosEditor.class, "icons/show_inertias_information.png")); //$NON-NLS-1$
		}
		else if (getResult() instanceof AHCFactorsInfo) {
			this.setTitleImage(IImageKeys.getImage(AHCColsRowsInfosEditor.class, "icons/show_axes_information.png")); //$NON-NLS-1$
		}
	}


	@Override
	public void _createPartControl() {

		// remove the compute button
		this.removeComputeButton();
		
		ToolItem showAHCButton = new ToolItem(this.getTopToolbar(), SWT.PUSH);
		showAHCButton.setToolTipText(AHCUIMessages.tooltip_showTheClustersAnalysis);
		showAHCButton.setImage(IImageKeys.getImage(AHCColsRowsInfosEditor.class, "icons/ahc.png")); //$NON-NLS-1$
		showAHCButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				ComputeAHC.open(getResult().getParent(), false);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});

		GLComposite resultArea = this.getResultArea();

		viewer = new TableViewer(resultArea, SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI | SWT.FULL_SELECTION | SWT.VIRTUAL);
		viewer.getTable().setLayoutData(new GridData(GridData.FILL_BOTH));

		tableKeyListener = TableUtils.installCopyAndSearchKeyEvents(viewer);
		tableKeyListener.installSearchGroup(this);
		tableKeyListener.installMenuContributions(this);
		TableUtils.installColumnResizeMouseWheelEvents(viewer);
		TableUtils.installTooltipsOnSelectionChangedEvent(viewer);

		Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		namesColumn = new TableViewerColumn(viewer, SWT.LEFT);
		namesColumn.getColumn().setText(getResult().getFirstColumnName());

		// creates the viewer comparator
		viewerComparator = new TableLinesViewerComparator(Toolbox.getCollator(this.getResult())) {

			@Override
			public int compare(Viewer viewer, Object e1, Object e2) {

				boolean directionUp = AHCColsRowsInfosEditor.this.viewer.getTable().getSortDirection() == SWT.UP;

				if (lastColumnIndex == 0) {
					try {
						if (directionUp) {
							return Integer.parseInt(e1.toString()) - Integer.parseInt(e2.toString());
						}
						else {
							return Integer.parseInt(e2.toString()) - Integer.parseInt(e1.toString());
						}
					} catch(Exception e) {
						return super.compare(viewer, e1, e2);
					}
					
				}
				else if (lastColumnIndex == AHCColsRowsInfosEditor.this.viewer.getTable().getColumnCount() - 1) {
					return 0;
				}
				else {
					Object[] infos1 = getResult().getInfos(e1.toString());
					Object[] infos2 = getResult().getInfos(e2.toString());

					Object i1 = infos1[lastColumnIndex - 1];
					Object i2 = infos2[lastColumnIndex - 1];

					if (i1 == null) return 0;
					if (i2 == null) return 0;

					if (directionUp) {
						return -Double.compare((Double) i2, (Double) i1);
					}
					else {
						return Double.compare((Double) i2, (Double) i1);
					}
				}
			}
		};

		viewer.setContentProvider(ArrayContentProvider.getInstance());

		// To draw the bar plot column
		viewer.getTable().addListener(SWT.EraseItem, new Listener() {

			@Override
			public void handleEvent(Event event) {

				if ("Histogram".equals(viewer.getTable().getColumn(event.index).getData("type"))) { //$NON-NLS-1$  //$NON-NLS-2$
					int itemIndex = viewer.getTable().indexOf((TableItem) event.item);
					Object elem = viewer.getElementAt(itemIndex);
					Object[] infos = getResult().getInfos(elem.toString());
					double percent = (double) infos[event.index - 1]; // comma replacement according to the locale
					event.gc.setBackground(viewer.getTable().getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));
					
					int l = Math.max(0, (int) (viewer.getTable().getColumn(event.index).getWidth() * percent) - 1);
					event.gc.fillRectangle(event.x +1, event.y + 1, l, event.height - 1);
				}
			}
		});

		// Register the context menu
		TXMEditor.initContextMenu(this.viewer.getTable(), this.getSite(), this.viewer);
	}

	/**
	 * Gets the data to give to the viewer.
	 * 
	 * @return
	 */
	public Object getTableInput() {
		return this.getResult().getInfos();
	}

	public String[] getTableTitles() {
		return this.getResult().getInfoNames();
	}


	/**
	 * Gets the ordered labels according to the current table sort.
	 * 
	 * @return
	 */
	public String[] getOrdererLabels() {

		TableItem[] items = this.viewer.getTable().getItems();
		String[] labels = new String[items.length];
		for (int i = 0; i < items.length; i++) {
			labels[i] = items[i].getText();
		}

		return labels;
	}


	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void _setFocus() {
		viewer.getControl().setFocus();
	}

	/**
	 * @return the viewer
	 */
	public TableViewer getViewer() {
		return viewer;
	}


	@Override
	public void updateResultFromEditor() {
		// TODO Auto-generated method stub

	}


	@Override
	public void updateEditorFromResult(boolean update) throws Exception {
		if (this.viewer.getTable().isDisposed()) return; // abort

		namesColumn.getColumn().setText(getResult().getFirstColumnName());
		
		String[] titles = this.getTableTitles();

		for (TableViewerColumn col : infosColumns) {
			col.getColumn().dispose();
		}

		for (int i = 0; i < titles.length; i++) {
			final int index = i;
			final TableViewerColumn column;
			if (index == 0) {
				column = new TableViewerColumn(viewer, SWT.LEFT);
			}
			else {
				column = new TableViewerColumn(viewer, SWT.RIGHT);
			}
			if ("Histogram".equals(titles[i])) { //$NON-NLS-1$
				column.getColumn().setText("                "); //$NON-NLS-1$
				column.getColumn().setData("type", "Histogram"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			else {
				column.getColumn().setText(titles[i]);
			}

			infosColumns.add(column);
		}
		TableViewerColumn sepColumn = new TableViewerColumn(viewer, SWT.LEFT);
		sepColumn.getColumn().setText(""); //$NON-NLS-1$
		sepColumn.getColumn().setResizable(false);

		infosColumns.add(sepColumn);

		viewer.setLabelProvider(new AHCColRowInfosLabelProvider(this.getResult(), this.viewer));

		viewer.setComparator(viewerComparator);
		viewerComparator.addSelectionAdapters(viewer);

		if (this.getResult().getInfos() != null) {
			Set<String> keys = this.getResult().getInfos().keySet();
			this.viewer.setInput(keys);
		} else {
			this.viewer.setInput(new HashSet<String>());
		}

		// Pack the columns
		TableUtils.packColumns(this.viewer);
	}

	CAFactorialMapChartEditor caFactorialMapEditor;

	public CAFactorialMapChartEditor getCAFactorialMapChartEditor() {

		if (caFactorialMapEditor != null && !caFactorialMapEditor.isDisposed()) {
			return caFactorialMapEditor;
		}

		caFactorialMapEditor = null;
		CA ca = getResult().getFirstParent(CA.class);
		if (ca == null) return null;

		Set<ITXMResultEditor<TXMResult>> editors = SWTEditorsUtils.getEditors(ca);
		if (editors != null) {
			for (ITXMResultEditor<TXMResult> editor : editors) {
				if (editor instanceof CAFactorialMapChartEditor eEditor) {
					caFactorialMapEditor = eEditor;
					break;
				}
				else if (editor instanceof CAEditor eEditor) {
					caFactorialMapEditor = (CAFactorialMapChartEditor) eEditor.getChartEditor();
					break;
				}
			}
		}

		return caFactorialMapEditor;
	}

	@Override
	public TableViewer[] getTableViewers() {

		return new TableViewer[] { viewer };
	}

	@Override
	public TableKeyListener getTableKeyListener() {

		return tableKeyListener;
	}

}
