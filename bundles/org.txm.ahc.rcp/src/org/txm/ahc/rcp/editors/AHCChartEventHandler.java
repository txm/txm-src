package org.txm.ahc.rcp.editors;

import java.util.Arrays;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.PlotEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.txm.chartsengine.rcp.events.EventCallBack;

/**
 * On cluster selection should show the cluster row&col infos (kinda like the CAEditor event handler)
 */
//FIXME: SJ: not used at this moment. MD: and not finished yet
public class AHCChartEventHandler extends EventCallBack<AHCEditor> {

	public AHCChartEventHandler() {

		// TODO Auto-generated constructor stub
	}

	@Override
	public void processEvent(Object event, int eventArea, Object customData) {

		if (!(chartEditor.getChart() instanceof JFreeChart)) {
			return;
		}

		chartEditor.getComposite().getDisplay().asyncExec(new Runnable() {

			@Override
			public void run() {
				//System.out.println("event="+event+ " eventArea="+eventArea+" customData="+customData);
				if (customData instanceof ChartMouseEvent cme) {

					int x = cme.getTrigger().getX();
					int y = cme.getTrigger().getY();

					ChartEntity entity = cme.getEntity();
					JFreeChart chart = (JFreeChart) chartEditor.getChart();

					int icluster = -1;

					if (entity instanceof XYItemEntity xyEntity) { // cluster !!
						icluster = xyEntity.getSeriesIndex();
					}
					else if (entity instanceof PlotEntity plotEntity) {
						// TODO must convert mouse position to chart position

						//						System.out.println("Plot: "+plotEntity);
						//						int nclusters = chartEditor.getResult().getNumberOfClusters();
						//						Map<Integer, XYDataset> dataset = chart.getXYPlot().getDatasets();
						//						XYSeriesCollection collection = (XYSeriesCollection) dataset.get(0);
						//						System.out.println("DATASETS: "+collection);
						//						System.out.println("X: "+x+" Y: "+y);
						//						for (int i = 0 ; i < nclusters ; i++) {
						//							
						//							XYSeries s = collection.getSeries(i);
						//							//System.out.println(""+s.getMinX()+" <= "+x+" && "+x+" <= "+s.getMaxX()+" && "+s.getMinY()+" <= "+y+" && "+y+" <= "+s.getMaxY());
						//							if (s.getMinX() <= x && x <= s.getMaxX() && s.getMinY() <= y && y <= s.getMaxY()) {
						//								icluster = i;
						//								break; // we found our cluster
						//							}
						//						}
					}

					if (icluster >= 0) {
						System.out.println("Variables: " + Arrays.toString(chartEditor.getResult().getClusterVariables(icluster + 1))); //$NON-NLS-1$
						System.out.println("Individuals: " + chartEditor.getResult().getClustersInd().get(icluster)); //$NON-NLS-1$
					}
				}
			}
		});
	}

}
