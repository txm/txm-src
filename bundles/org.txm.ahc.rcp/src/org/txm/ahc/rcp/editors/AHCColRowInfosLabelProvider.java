// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.ahc.rcp.editors;

import java.text.DecimalFormat;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.graphics.Image;
import org.txm.ahc.core.functions.AHCInfos;

/**
 * The Class AHCColRowInfosLabelProvider fetch the info from the AHCInfos
 */
public class AHCColRowInfosLabelProvider extends LabelProvider implements ITableLabelProvider {

	/** The names. */
	String[] names;

	/** The distpattern. */
	DecimalFormat distpattern = new DecimalFormat("0.0000"); //$NON-NLS-1$

	/** The chipattern. */
	DecimalFormat chipattern = new DecimalFormat("0.0000"); //$NON-NLS-1$

	private AHCInfos infos;

	TableViewer viewer;

	/**
	 * Instantiates a new col row infos label provider.
	 *
	 * @param names the names
	 */
	public AHCColRowInfosLabelProvider(AHCInfos infos, TableViewer viewer) {
		super();
		this.infos = infos;
		this.viewer = viewer;

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
	 */
	@Override
	public Image getColumnImage(Object element, int columnIndex) {

		return null;
	}

	String EMPTY = ""; //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
	 */
	@Override
	@SuppressWarnings("unchecked") //$NON-NLS-1$
	public String getColumnText(Object element, int columnIndex) {

		if (element instanceof String k) {

			if (columnIndex == 0) return k;

			Object[] data = infos.getInfos().get(element);

			if ("Histogram".equals(viewer.getTable().getColumn(columnIndex).getData("type"))) { //$NON-NLS-1$
				return "";
			}
			if (columnIndex < data.length + 1 && data[columnIndex - 1] != null) {
				return distpattern.format(data[columnIndex - 1]);
			}
		}
		return EMPTY;
	}
}
