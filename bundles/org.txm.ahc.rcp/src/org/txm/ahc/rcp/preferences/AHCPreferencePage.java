// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.ahc.rcp.preferences;

import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.txm.ahc.core.functions.AHC;
import org.txm.ahc.core.messages.AHCCoreMessages;
import org.txm.ahc.core.preferences.AHCPreferences;
import org.txm.ahc.rcp.adapters.AHCAdapterFactory;
import org.txm.ahc.rcp.messages.AHCUIMessages;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.rcp.swt.widget.preferences.ComboFieldEditor;
import org.txm.rcp.swt.widget.preferences.IntegerFieldEditor;

/**
 * CAH preferences page.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class AHCPreferencePage extends TXMPreferencePage {

	@Override
	public void createFieldEditors() {

		IntegerFieldEditor nClusters = new IntegerFieldEditor(AHCPreferences.N_CLUSTERS, AHCUIMessages.numberOfClusters, getFieldEditorParent());
		nClusters.setToolTipText(AHCUIMessages.determinesTheCutoffLevelOfTheClassDendrogram);
		addField(nClusters);

		String list[] = AHC.getMethods();
		String methods[][] = new String[list.length][2];
		for (int i = 0; i < list.length; i++)
			methods[i][0] = methods[i][1] = list[i];

		ComboFieldEditor method = new ComboFieldEditor(AHCPreferences.METHOD, AHCUIMessages.method, methods, getFieldEditorParent());
		method.setToolTipText(AHCUIMessages.linkingMethodForHierarchicalClustering);
		addField(method);

		list = AHC.getMetrics();
		String metrics[][] = new String[list.length][2];
		for (int i = 0; i < list.length; i++)
			metrics[i][0] = metrics[i][1] = list[i];

		ComboFieldEditor metric = new ComboFieldEditor(AHCPreferences.METRIC, AHCUIMessages.common_metric, metrics, getFieldEditorParent());
		metric.setToolTipText(AHCUIMessages.distanceUsedByTheAlgorithm);
		addField(metric);

//		IntegerFieldEditor preprocessColumns = new IntegerFieldEditor(AHCPreferences.N_MAX_CLUSTER_TO_PROCESS, AHCUIMessages.kmeansPreProcessLimit, getFieldEditorParent());
//		addField(preprocessColumns);
		
		BooleanFieldEditor conslidation = new BooleanFieldEditor(AHCPreferences.CONSOLIDATION, AHCUIMessages.clustersConsolidation, getFieldEditorParent());
		conslidation.setToolTipText(AHCUIMessages.cahClassesAreConsolidatedByPostClustering);
		addField(conslidation);

		// Charts
		Composite chartsTab = SWTChartsComponentsProvider.createChartsRenderingPreferencesTabFolderComposite(getFieldEditorParent());

		String[][] labelAndValues = { { "2D", "2D" }, { "3D", "3D" } }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		RadioGroupFieldEditor default2D3Ddisplay = new RadioGroupFieldEditor(AHCPreferences.RENDERING, AHCUIMessages.displayChartsIn2D, 2, labelAndValues, chartsTab);
		addField(default2D3Ddisplay);


		// other shared preferences
		SWTChartsComponentsProvider.createChartsRenderingPreferencesFields(this, chartsTab);
	}

	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(AHCPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setTitle(AHCCoreMessages.clusterDendrogram);
		this.setImageDescriptor(AHCAdapterFactory.ICON);
	}

}
