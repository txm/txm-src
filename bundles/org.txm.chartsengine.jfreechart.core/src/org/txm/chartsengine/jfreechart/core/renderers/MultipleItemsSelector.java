/**
 *
 */
package org.txm.chartsengine.jfreechart.core.renderers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

import org.jfree.chart.event.RendererChangeEvent;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.XYPlot;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;

/**
 * Chart multiple items selector dedicated to managing and rendering.
 * Class providing methods to store selected series and items, selected shapes coordinates and methods for updating data sets for drawing selected items as last, over other elements.
 * 
 * @author sjacquot
 *
 */
public class MultipleItemsSelector extends MouseOverItemSelector {



	/**
	 * The selected items in series.
	 */
	protected Map<Integer, List<Integer>> multipleSelectedSeries;

	/**
	 * The series of the item added to the selector.
	 */
	protected int lastSelectedSeries = -1;

	/**
	 * The last item added to the selector.
	 */
	protected int lastSelectedItem = -1;


	/**
	 * Simple selection mode.
	 */
	protected boolean simpleSelectionMode = false;

	/**
	 * Cyclic ordering map containing the order of the items to use when selecting next and previous items.
	 */
	protected TreeMap<Object, ArrayList<Integer>> cyclicItemsOrder = null;;

	/**
	 * @param renderer
	 */
	public MultipleItemsSelector(IRendererWithItemSelection renderer) {
		super(renderer);
		this.removeAllSelectedItems();
	}



	/**
	 * Adds the item to be selected.
	 * 
	 * @param series
	 * @param item
	 */
	public void addSelectedItem(int series, int item) {

		// Not active
		if (!this.active) {
			return;
		}

		this.lastSelectedSeries = series;
		this.lastSelectedItem = item;

		// Already selected
		if (this.isSelectedItem(series, item)) {
			return;
		}

		List<Integer> selectedItems = this.multipleSelectedSeries.get(series);
		if (selectedItems == null) {
			selectedItems = new ArrayList<Integer>();
			this.multipleSelectedSeries.put(series, selectedItems);
		}
		if (!selectedItems.contains(item)) {
			selectedItems.add(item);
		}


		// FIXME: Debug
		// System.out.println("MultipleItemsSelector.addSelectedItem(): added: series = " + series + " item = " + item);
		// System.out.println("MultipleItemsSelector.addSelectedItem(): added: selectd items count = " + this.multipleSelectedSeries.get(series).size() + " in series " + series);


		this.renderer.updateDatasetForDrawingSelectedItemAsLast();

		// Refresh draw
		this.renderer.notifyListeners(new RendererChangeEvent(this.renderer));
	}


	/**
	 * Checks if the specified item in specified series is selected.
	 * 
	 * @param series
	 * @param item
	 * @return
	 */
	public boolean isSelectedItem(int series, int item) {
		List<Integer> selectedItems = this.multipleSelectedSeries.get(series);
		if (selectedItems != null && selectedItems.contains(item)) {
			return true;
		}
		return false;
	}


	/**
	 * Removes the selected item in the specified series.
	 * 
	 * @param series
	 * @param item
	 */
	public void removeSelectedItem(int series, int item) {
		List<Integer> selectedItems = this.multipleSelectedSeries.get(series);
		if (selectedItems != null) {
			for (int i = 0; i < selectedItems.size(); i++) {
				if (selectedItems.get(i) == item) {
					selectedItems.remove(i);
					break;
				}
			}

		}
		// Remove the series if there are no more selected items
		if (selectedItems.isEmpty()) {
			this.multipleSelectedSeries.remove(series);
		}

		if (series == this.lastSelectedSeries && item == this.lastSelectedItem) {
			this.lastSelectedSeries = -1;
			this.lastSelectedItem = -1;
		}

		this.renderer.updateDatasetForDrawingSelectedItemAsLast();

		// Refresh draw
		this.renderer.notifyListeners(new RendererChangeEvent(this.renderer));

	}


	/**
	 * Removes all selected items in the specified series.
	 */
	public void removeAllSelectedItems(int series) {
		if (this.multipleSelectedSeries.containsKey(series)) {
			this.multipleSelectedSeries.remove(series);

			if (series == this.lastSelectedSeries) {
				this.lastSelectedSeries = -1;
				this.lastSelectedItem = -1;
			}
		}
	}

	/**
	 * Clears the selection in all series.
	 */
	public void removeAllSelectedItems() {
		this.multipleSelectedSeries = new HashMap<Integer, List<Integer>>();

		this.lastSelectedSeries = -1;
		this.lastSelectedItem = -1;
	}


	/**
	 * Adds or removes the selected item in the specified series.
	 * 
	 * @param series
	 * @param item
	 * @param selected
	 */
	public void setSelectedItem(int series, int item, boolean selected) {
		if (selected) {
			this.addSelectedItem(series, item);
		}
		else {
			this.removeSelectedItem(series, item);
		}
	}


	/**
	 * Gets the number of selected items in the specified series.
	 * 
	 * @param series
	 * @return
	 */
	public int getSelectedItemsCount(int series) {
		if (this.multipleSelectedSeries.get(series) == null) {
			return 0;
		}
		return this.multipleSelectedSeries.get(series).size();
	}

	/**
	 * Gets the selected items in the specified series.
	 * 
	 * @param series
	 * @return
	 */
	public int[] getSelectedItems(int series) {
		if (this.multipleSelectedSeries.get(series) == null) {
			return null;
		}
		int[] selectedItems = new int[this.multipleSelectedSeries.get(series).size()];
		for (int i = 0; i < this.multipleSelectedSeries.get(series).size(); i++) {
			selectedItems[i] = this.multipleSelectedSeries.get(series).get(i);
		}
		return selectedItems;
	}

	/**
	 * Gets the number of series with some selected items.
	 * 
	 * @return
	 */
	public int getSelectedSeriesCount() {
		return this.multipleSelectedSeries.size();
	}


	/**
	 * Gets the series which contain selected items.
	 */
	public Integer[] getSelectedSeries() {
		Integer[] selectedSeries = this.multipleSelectedSeries.keySet().toArray(new Integer[this.multipleSelectedSeries.size()]);
		return selectedSeries;
	}


	/**
	 * Gets the simple selection mode state.
	 * 
	 * @return the simpleSelectionMode
	 */
	public boolean isSimpleSelectionMode() {
		return simpleSelectionMode;
	}


	/**
	 * Sets the simple selection mode. If true, the multiple selection will be disabled.
	 * 
	 * @param simpleSelectionMode the simpleSelectionMode to set
	 */
	public void setSimpleSelectionMode(boolean simpleSelectionMode) {
		this.simpleSelectionMode = simpleSelectionMode;
	}



	/**
	 * @return the lastSelectedSeries
	 */
	public int getLastSelectedSeries() {
		return lastSelectedSeries;
	}



	/**
	 * @return the lastSelectedItem
	 */
	public int getLastSelectedItem() {
		return lastSelectedItem;
	}



	/**
	 * Selects or adds the next item (according to the last previous selected item) in the selection.
	 * 
	 * @param extendedSelection
	 */
	public void selectNextItem(boolean extendedSelection) {


		int nextSeries = this.lastSelectedSeries;
		int nextItem = this.lastSelectedItem + 1;

		// get the next item to select in the order tree map
		if (this.cyclicItemsOrder != null) {
			Set keys = this.cyclicItemsOrder.keySet();
			Iterator it = keys.iterator();
			while (it.hasNext()) {
				Object key = it.next();
				ArrayList<Integer> value = this.cyclicItemsOrder.get(key);
				if (value.get(0) == this.lastSelectedSeries && value.get(1) == this.lastSelectedItem) {
					if (it.hasNext()) {
						key = it.next();
						value = this.cyclicItemsOrder.get(key);
						nextSeries = value.get(0);
						nextItem = value.get(1);
					}
					// loop, go back to the first item of the first series
					// FIXME: useless ?
					else {

						// nextSeries = 0;
						// nextItem = 0;

						value = this.cyclicItemsOrder.firstEntry().getValue();
						nextSeries = value.get(0);
						nextItem = value.get(1);

					}
					break;
				}
			}
		}

		// for the item selection at first time, when none are selected
		if (nextSeries == -1) {
			nextSeries = 0;
			nextItem = 0;
		}

		int itemsCount = 0;
		int seriesCount = 0;

		// XY plot
		if (this.renderer.getPlot() instanceof XYPlot) {

			XYPlot plot = (XYPlot) this.renderer.getPlot();
			itemsCount = plot.getDataset().getItemCount(nextSeries);
			seriesCount = plot.getSeriesCount();

		}
		// Category plot
		else if (this.renderer.getPlot() instanceof CategoryPlot) {

			// FIXME: not yet implemented
			System.err.println("MultipleItemsSelector.selectNextItem(): category plot selection is not yet implemented."); //$NON-NLS-1$
			// CategoryPlot plot = (CategoryPlot) this.renderer.getPlot();
			// CategoryDataset dataset = plot.getDataset();
			//
			// seriesCount = dataset.getRowCount();
			// itemsCount = dataset.getColumnIndex(itemEntity.getColumnKey());
			//
			//
			// itemsCount = plot.getDataset().getItemCount(nextSeries);
		}

		// Change series if the last item was the last
		if (nextItem == itemsCount) {
			nextSeries++;
			nextItem = 0;
			if (nextSeries == seriesCount) {
				nextSeries = 0;
			}
		}
		this.selectItem(nextSeries, nextItem, extendedSelection);
	}

	/**
	 * Selects or adds the previous item (according to the last previous selected item) in the selection.
	 * 
	 * @param extendedSelection
	 */
	public void selectPreviousItem(boolean extendedSelection) {

		int previousSeries = this.lastSelectedSeries;
		int previousItem = this.lastSelectedItem - 1;


		// get the previous item to select in the order tree map
		if (this.cyclicItemsOrder != null) {
			NavigableMap<Object, ArrayList<Integer>> orderMap = this.cyclicItemsOrder.descendingMap();
			Set keys = orderMap.keySet();
			Iterator<Integer> it = keys.iterator();
			while (it.hasNext()) {
				Object key = it.next();
				ArrayList<Integer> value = orderMap.get(key);
				if (value.get(0) == this.lastSelectedSeries && value.get(1) == this.lastSelectedItem) {
					if (it.hasNext()) {
						key = it.next();

						// FIXME: Debug
						// if(key.equals(".")) {
						// System.err.println("MultipleItemsSelector.selectPreviousItem()");
						// }

						value = orderMap.get(key);
						previousSeries = value.get(0);
						previousItem = value.get(1);
					}
					// loop, go back to the last item of the last series
					else {
						// previousSeries = 0;

						// previousItem = -1;

						value = orderMap.get(orderMap.firstKey());
						previousSeries = value.get(0);
						previousItem = value.get(1);


					}
					break;
				}
			}
		}

		// for the item selection at first time, when none are selected
		if (previousSeries == -1) {
			previousSeries = 0;
			previousItem = 0;
		}

		// Change series if the last item was the first
		if (previousItem == -1) {

			int seriesCount = 0;

			// XY plot
			if (this.renderer.getPlot() instanceof XYPlot) {
				seriesCount = ((XYPlot) this.renderer.getPlot()).getSeriesCount();
			}
			// Category plot
			else if (this.renderer.getPlot() instanceof CategoryPlot) {
				// FIXME: not yet implemented
				System.err.println("MultipleItemsSelector.selectPreviousItem(): category plot selection is not yet implemented."); //$NON-NLS-1$
			}


			previousSeries--;
			if (previousSeries == -1) {
				previousSeries = seriesCount - 1;
			}

			// XY plot
			if (this.renderer.getPlot() instanceof XYPlot) {
				previousItem = ((XYPlot) this.renderer.getPlot()).getDataset().getItemCount(previousSeries) - 1;

				// System.err.println("MultipleItemsSelector.selectPreviousItem(): previous series: " + previousSeries);
				// System.err.println("MultipleItemsSelector.selectPreviousItem(): previous item: " + previousItem);

			}
			// Category plot
			else if (this.renderer.getPlot() instanceof CategoryPlot) {
				// FIXME: not yet implemented
				System.err.println("MultipleItemsSelector.selectPreviousItem(): category plot selection is not yet implemented."); //$NON-NLS-1$
			}

		}


		this.selectItem(previousSeries, previousItem, extendedSelection);

	}


	// /**
	// * Selects or adds the previous item (according to the last previous selected item) in the selection.
	// * @param extendedSelection
	// */
	// public void selectPreviousItem(boolean extendedSelection) {
	//
	// int previousSeries = this.lastSelectedSeries;
	// int previousItem = this.lastSelectedItem - 1;
	//
	//
	// // get the previous item to select in the order tree map
	// if(this.cyclicItemsOrder != null) {
	// NavigableMap<Object, ArrayList<Integer>> orderMap = this.cyclicItemsOrder.descendingMap();
	// Set keys = orderMap.keySet();
	// Iterator<Integer> it = keys.iterator();
	// while(it.hasNext()) {
	// Object key = it.next();
	// ArrayList<Integer> value = orderMap.get(key);
	// if(value.get(0) == this.lastSelectedSeries && value.get(1) == this.lastSelectedItem) {
	// if(it.hasNext()) {
	// key = it.next();
	// value = orderMap.get(key);
	// previousSeries = value.get(0);
	// previousItem = value.get(1);
	// }
	// // loop, go back to the last item of the last series
	// else {
	//
	// previousItem = -1;
	// }
	// break;
	// }
	// }
	// }
	//
	//
	//// if(previousSeries == -1) {
	//// previousSeries = 0;
	//// previousItem = 0;
	//// }
	//
	//
	// int seriesCount = 0;
	//
	//
	//
	// // FIXME: Debug
	// System.err.println("MultipleItemsSelector.selectPreviousItem(): previous series: " + previousSeries);
	// System.err.println("MultipleItemsSelector.selectPreviousItem(): previous item : " + previousItem);
	//
	//
	// // Change series if the last item was the first
	// if(previousItem == -1) {
	//
	// previousSeries--;
	//
	// if(previousSeries == -1) {
	//
	// // XY plot
	// if(this.renderer.getPlot() instanceof XYPlot) {
	// seriesCount = ((XYPlot) this.renderer.getPlot()).getSeriesCount();
	// previousSeries = seriesCount - 1;
	// previousItem = ((XYPlot) this.renderer.getPlot()).getDataset().getItemCount(previousSeries) - 1;
	//
	// // FIXME: Debug
	// System.err.println("MultipleItemsSelector.selectPreviousItem(): previous series: " + previousSeries);
	// System.err.println("MultipleItemsSelector.selectPreviousItem(): previous item : " + previousItem);
	//
	// }
	// // Category plot
	// else if(this.renderer.getPlot() instanceof CategoryPlot) {
	// // FIXME: not yet implemented
	// System.err.println("MultipleItemsSelector.selectPreviousItem(): category plot selection is not yet implemented.");
	// }
	//
	//
	//// previousSeries = seriesCount - 1;
	//
	//
	//// // XY plot
	//// if(this.renderer.getPlot() instanceof XYPlot) {
	//// previousItem = ((XYPlot) this.renderer.getPlot()).getDataset().getItemCount(previousSeries) - 1;
	//// }
	//// // Category plot
	//// else if(this.renderer.getPlot() instanceof CategoryPlot) {
	//// // FIXME: not yet implemented
	//// System.err.println("MultipleItemsSelector.selectPreviousItem(): category plot selection is not yet implemented.");
	//// }
	// }
	//
	//
	//
	// }
	//
	// this.selectItem(previousSeries, previousItem, extendedSelection);
	//
	// }
	//

	/**
	 * Selects or adds the specified item in the selection with extended selection management.
	 * 
	 * @param series
	 * @param item
	 * @param extendedSelection
	 */
	public void selectItem(int series, int item, boolean extendedSelection) {

		// Cyclic exclusive selection
		if (!extendedSelection) {
			this.removeAllSelectedItems(); // clear the selection and the last selected series and item
			this.setSelectedItem(series, item, true);
		}
		// Extended selection, inverse the last selected item
		else {
			boolean select = !this.isSelectedItem(series, item);
			if (select) {
				this.setSelectedItem(series, item, select);
			}
			else {
				this.setSelectedItem(this.lastSelectedSeries, this.lastSelectedItem, select);
			}
			this.lastSelectedSeries = series;
			this.lastSelectedItem = item;
		}
	}


	/**
	 * @param cyclicItemsOrder the cyclicItemsOrder to set
	 */
	public void setCyclicItemsOrder(TreeMap<Object, ArrayList<Integer>> cyclicItemsOrder) {
		this.cyclicItemsOrder = cyclicItemsOrder;
	}


}
