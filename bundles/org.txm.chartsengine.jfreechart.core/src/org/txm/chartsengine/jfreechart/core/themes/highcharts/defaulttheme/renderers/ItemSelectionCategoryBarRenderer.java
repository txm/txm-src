package org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers;

import java.awt.Color;
import java.awt.Paint;
import java.text.DecimalFormat;

import org.jfree.chart.labels.CategoryToolTipGenerator;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.txm.chartsengine.core.messages.ChartsEngineCoreMessages;
import org.txm.chartsengine.jfreechart.core.renderers.MouseOverItemSelector;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.swing.CustomHTMLToolTip;

/**
 * Renderer providing item selection system and drawing features for selected item and custom rendering for bar charts.
 * 
 * @author Sebastien Jacquot
 *
 */
public class ItemSelectionCategoryBarRenderer extends BarRenderer implements IRendererWithItemSelection {


	private static final long serialVersionUID = -7638025885937801432L;

	/**
	 * Chart item selector.
	 */
	protected MouseOverItemSelector mouseOverItemSelector;

	/**
	 * Values number format.
	 */
	protected DecimalFormat valuesNumberFormat;

	/**
	 *
	 */
	public ItemSelectionCategoryBarRenderer() {
		super();
		this.mouseOverItemSelector = new MouseOverItemSelector(this);

		// Default Values format
		this.valuesNumberFormat = new DecimalFormat("#0.####"); //$NON-NLS-1$


		// Labels
		this.initItemLabelGenerator();

		// Tooltips
		this.initToolTipGenerator(this);
	}

	// FIXME : set a minimum width for bars, tests for drawing large bar plot, to use with scrolling
	// @Override
	// protected void calculateBarWidth(CategoryPlot plot,
	// Rectangle2D dataArea,
	// int rendererIndex,
	// CategoryItemRendererState state) {
	//
	// CategoryAxis domainAxis = getDomainAxis(plot, rendererIndex);
	// CategoryDataset dataset = plot.getDataset(rendererIndex);
	// if (dataset != null) {
	// int columns = dataset.getColumnCount();
	// int rows = state.getVisibleSeriesCount() >= 0
	// ? state.getVisibleSeriesCount() : dataset.getRowCount();
	// double space = 0.0;
	// PlotOrientation orientation = plot.getOrientation();
	// if (orientation == PlotOrientation.HORIZONTAL) {
	// space = dataArea.getHeight() * 4;
	// }
	// else if (orientation == PlotOrientation.VERTICAL) {
	// space = dataArea.getWidth() * 4;
	// }
	// double maxWidth = space * getMaximumBarWidth();
	// double categoryMargin = 0.0;
	// double currentItemMargin = 0.0;
	// if (columns > 1) {
	// categoryMargin = domainAxis.getCategoryMargin();
	// }
	// if (rows > 1) {
	// currentItemMargin = getItemMargin();
	// }
	// double used = space * (1 - domainAxis.getLowerMargin()
	// - domainAxis.getUpperMargin()
	// - categoryMargin - currentItemMargin);
	// if ((rows * columns) > 0) {
	// state.setBarWidth(Math.min(used / (rows * columns), maxWidth));
	// }
	// else {
	// state.setBarWidth(Math.min(used, maxWidth));
	// }
	// }
	// }
	//
	//
	// protected double calculateBarW0(CategoryPlot plot,
	// PlotOrientation orientation, Rectangle2D dataArea,
	// CategoryAxis domainAxis, CategoryItemRendererState state,
	// int row, int column) {
	// // calculate bar width...
	// double space;
	// if (orientation == PlotOrientation.HORIZONTAL) {
	// space = dataArea.getHeight() * 4;
	// }
	// else {
	// space = dataArea.getWidth() * 4;
	// }
	// double barW0 = domainAxis.getCategoryStart(column, getColumnCount(),
	// dataArea, plot.getDomainAxisEdge());
	// int seriesCount = state.getVisibleSeriesCount() >= 0
	// ? state.getVisibleSeriesCount() : getRowCount();
	// int categoryCount = getColumnCount();
	// if (seriesCount > 1) {
	// double seriesGap = space * getItemMargin()
	// / (categoryCount * (seriesCount - 1));
	// double seriesW = calculateSeriesWidth(space, domainAxis,
	// categoryCount, seriesCount);
	// barW0 = barW0 + row * (seriesW + seriesGap)
	// + (seriesW / 2.0) - (state.getBarWidth() / 2.0);
	// }
	// else {
	// barW0 = domainAxis.getCategoryMiddle(column, getColumnCount(),
	// dataArea, plot.getDomainAxisEdge()) - state.getBarWidth()
	// / 2.0;
	// }
	// return barW0;
	// }
	//
	/**
	 * Initializes the item label generator.
	 */
	public void initItemLabelGenerator() {
		// TODO
	}

	/**
	 * Initializes the tool tip generator.
	 */
	public void initToolTipGenerator(final IRendererWithItemSelection renderer) {
		this.setDefaultToolTipGenerator(new CategoryToolTipGenerator() {

			@Override
			public String generateToolTip(CategoryDataset dataset, int row, int column) {

				// Hexadecimal color
				Color color = (Color) getSeriesPaint(row);
				String hex = "#" + Integer.toHexString(color.getRGB()).substring(2); //$NON-NLS-1$

				DefaultCategoryDataset catDataset = (DefaultCategoryDataset) dataset;
				Number value = catDataset.getValue(row, column);

				return CustomHTMLToolTip.getDefaultHTMLBody(renderer, hex) + "<p>" + catDataset.getColumnKey(column) + "</p><p><span style=\"color: " + hex + ";\">" + catDataset.getRowKey(row) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						+ "</span>" //$NON-NLS-1$
						+ ChartsEngineCoreMessages.EMPTY + " <b>" + valuesNumberFormat.format(value) + "</b></p></body><html>"; //$NON-NLS-1$ //$NON-NLS-2$
			}
		});
	}



	/**
	 * Sets the values number format. Used by the tool tips generator to create the values labels.
	 * 
	 * @param valuesNumberFormat the valuesNumberFormat to set
	 */
	public void setValuesNumberFormat(DecimalFormat valuesNumberFormat) {
		this.valuesNumberFormat = valuesNumberFormat;
	}



	@Override
	public Paint getItemOutlinePaint(int row, int col) {
		// FIXME : en attente de voir si l'on trace un contour ou non pour l'item sélectionnée
		// Draw item shape outline
		// if(this.itemSelector.isSelectedItem(row, col)) {
		// return super.getItemOutlinePaint(row, col);
		// }
		// Do not trace not selected item outline
		return null;
	}


	@Override
	public Paint getItemPaint(int row, int col) {

		Color color = (Color) super.getItemPaint(row, col);

		// Bright the item color paint for selected item
		if (this.mouseOverItemSelector.isMouseOverItem(row, col)) {

			// Gray scale rendering mode
			if (color.getRed() == color.getGreen() && color.getGreen() == color.getBlue()) {
				color = Color.RED;
			}
			// Color rendering and Monochrome modes
			else {
				// FIXME : old version, to remove when new version will be validated, brighter() does not work when value are 0
				color = color.brighter();
				// FIXME : new version, brightening
				// float hsbVals[] = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
				//// //color = Color.getHSBColor(hsbVals[0], hsbVals[1], 0.5f * ( 1f + hsbVals[2]));
				// // saturation version
				// color = Color.getHSBColor(hsbVals[0], 0.7f * hsbVals[1], hsbVals[2]);

			}


		}
		return color;
	}



	/**
	 * @return the itemSelector
	 */
	public MouseOverItemSelector getItemsSelector() {
		return mouseOverItemSelector;
	}



	@Override
	public void updateDatasetForDrawingSelectedItemAsLast() {
		// Do nothing for bar renderer
	}


	@Override
	public void init() {
		// TODO Auto-generated method stub

	}



}
