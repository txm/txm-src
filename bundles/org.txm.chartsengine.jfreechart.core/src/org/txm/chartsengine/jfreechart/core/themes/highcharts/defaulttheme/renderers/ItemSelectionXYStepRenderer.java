package org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.chart.renderer.xy.XYStepRenderer;
import org.jfree.chart.text.TextUtils;
import org.jfree.chart.ui.GradientPaintTransformer;
import org.jfree.chart.ui.LengthAdjustmentType;
import org.jfree.chart.ui.RectangleAnchor;
import org.jfree.chart.ui.RectangleEdge;
import org.jfree.data.Range;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeriesCollection;
import org.txm.chartsengine.core.messages.ChartsEngineCoreMessages;
import org.txm.chartsengine.jfreechart.core.renderers.MouseOverItemSelector;
import org.txm.chartsengine.jfreechart.core.renderers.MultipleItemsSelector;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.RendererUtils;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.swing.CustomHTMLToolTip;

public class ItemSelectionXYStepRenderer extends XYStepRenderer implements IRendererWithItemSelection {



	private static final long serialVersionUID = -6455966288345207714L;



	/**
	 * Chart item selector.
	 */
	protected MultipleItemsSelector multipleItemsSelector;



	/**
	 * State value to keep track of the type of rendering pass and skip some selection rendering modifications during the line pass.
	 */
	protected boolean isLinePass = true;


	/**
	 * Label that will be drawn for the selected item in drawing pass.
	 */
	protected String tmpSelectedItemLabel = null;

	/**
	 *
	 */
	public ItemSelectionXYStepRenderer() {
		this(true, false);
	}

	/**
	 *
	 * @param linesVisible
	 * @param shapesVisible
	 */
	public ItemSelectionXYStepRenderer(boolean linesVisible, boolean shapesVisible) {
		this.setDefaultLinesVisible(linesVisible);
		this.setDefaultShapesVisible(shapesVisible);
		// this.setShapesVisible(true); // FIXME: SJ: code for old jfreechart library, to remove after checking all works well

		// Multiple items selector
		this.multipleItemsSelector = new MultipleItemsSelector(this);

		// Labels
		this.initItemLabelGenerator();
		// Tooltips
		this.initToolTipGenerator(this);
	}


	/**
	 * Initializes the item label generator.
	 */
	public void initItemLabelGenerator() {
		// TODO
	}

	/**
	 * Initializes the tool tip generator.
	 */
	public void initToolTipGenerator(final IRendererWithItemSelection renderer) {
		this.setDefaultToolTipGenerator(new XYToolTipGenerator() {

			@Override
			public String generateToolTip(XYDataset dataset, int series, int item) {

				// Hexadecimal color
				Color color = (Color) getSeriesPaint(series);
				String hex = "#" + Integer.toHexString(color.getRGB()).substring(2); //$NON-NLS-1$
				XYSeriesCollection xyDataset = (XYSeriesCollection) dataset;

				return CustomHTMLToolTip.getDefaultHTMLBody(renderer, hex) + "<p><span style=\"color: " + hex + ";\">" + xyDataset.getSeriesKey(series) //$NON-NLS-1$ //$NON-NLS-2$
						+ "</span></p><p><b>" + RendererUtils.valuesNumberFormat.format(xyDataset.getSeries(series).getY(item)) + ChartsEngineCoreMessages.EMPTY //$NON-NLS-1$
						+ " </b><b>" + RendererUtils.valuesNumberFormat.format(xyDataset.getSeries(series).getX(item)) + "</b></p></body><html>"; //$NON-NLS-1$ //$NON-NLS-2$
			}
		});
	}


	@Override
	public Paint getItemOutlinePaint(int series, int item) {
		// Trace the shape outline
		if (this.multipleItemsSelector.isMouseOverItem(series, item) || this.multipleItemsSelector.isSelectedItem(series, item)) {
			return super.getItemOutlinePaint(series, item);
		}
		// Do not trace not selected item outline
		return null;
	}


	@Override
	public Font getItemLabelFont(int series, int item) {

		Font font = super.getItemLabelFont(series, item);

		// Change item label font size and style
		// if(this.itemSelector.isSelectedItem(series, item)) {
		font = font.deriveFont(Font.BOLD, font.getSize());
		// }
		return font;
	}


	/**
	 * @return the itemSelector
	 */
	public MouseOverItemSelector getItemsSelector() {
		return multipleItemsSelector;
	}



	@Override
	public void updateDatasetForDrawingSelectedItemAsLast() {
		// FIXME: put the mouse over item series at last in the dataset
		// this.getPlot().getDatasetRenderingOrder().

		// int index = this.multipleItemsSelector.getMouseOverSeries();
		// if(index != -1) {
		// this.getPlot().getDataset().setDatasetRenderingOrder();
		// }

		// create a new dataset with the mouse over series at last position
		// if(this.multipleItemsSelector.getMouseOverSeries() != -1) {
		// System.out.println("ItemSelectionXYStepRenderer.updateDatasetForDrawingSelectedItemAsLast() update dataset order");
		// XYSeriesCollection dataset = (XYSeriesCollection) this.getPlot().getDataset();
		// XYSeriesCollection newDataset = new XYSeriesCollection();
		// for(int i = 0; i < dataset.getSeriesCount(); i++) {
		// if(!this.multipleItemsSelector.isMouseOverSeries(i)) {
		// newDataset.addSeries(dataset.getSeries(i));
		// }
		// }
		// newDataset.addSeries(dataset.getSeries(this.multipleItemsSelector.getMouseOverSeries()));
		// this.getPlot().setDataset(newDataset);
		//
		// }

		// FIXME: temporary ugly workaround, set the rendering order according to the mouse over series and the half of the series count
		// if(this.multipleItemsSelector.getMouseOverSeries() <= this.getPlot().getDataset().getSeriesCount() / 2) {
		// this.getPlot().setSeriesRenderingOrder(SeriesRenderingOrder.FORWARD);
		// }
		// else {
		// this.getPlot().setSeriesRenderingOrder(SeriesRenderingOrder.REVERSE);
		// }
	}


	/**
	 * Draws a vertical line on the chart to represent a 'range marker'.
	 *
	 * @param g2 the graphics device.
	 * @param plot the plot.
	 * @param domainAxis the domain axis.
	 * @param marker the marker line.
	 * @param dataArea the axis data area.
	 */
	@Override
	public void drawDomainMarker(Graphics2D g2, XYPlot plot,
			ValueAxis domainAxis, Marker marker, Rectangle2D dataArea) {

		// Value marker
		if (marker instanceof ValueMarker) {
			ValueMarker vm = (ValueMarker) marker;
			double value = vm.getValue();
			Range range = domainAxis.getRange();
			if (!range.contains(value)) {
				return;
			}

			double v = domainAxis.valueToJava2D(value, dataArea,
					plot.getDomainAxisEdge());

			PlotOrientation orientation = plot.getOrientation();
			Line2D line = null;
			if (orientation == PlotOrientation.HORIZONTAL) {
				line = new Line2D.Double(dataArea.getMinX(), v,
						dataArea.getMaxX(), v);
			}
			else if (orientation == PlotOrientation.VERTICAL) {
				line = new Line2D.Double(v, dataArea.getMinY(), v,
						dataArea.getMaxY());
			}
			else {
				throw new IllegalStateException();
			}

			final Composite originalComposite = g2.getComposite();
			g2.setComposite(AlphaComposite.getInstance(
					AlphaComposite.SRC_OVER, marker.getAlpha()));
			g2.setPaint(marker.getPaint());
			g2.setStroke(marker.getStroke());
			g2.draw(line);

			String label = marker.getLabel();
			RectangleAnchor anchor = marker.getLabelAnchor();
			if (label != null) {
				Font labelFont = marker.getLabelFont();
				g2.setFont(labelFont);
				g2.setPaint(marker.getLabelPaint());
				Point2D coordinates = calculateDomainMarkerTextAnchorPoint(
						g2, orientation, dataArea, line.getBounds2D(),
						marker.getLabelOffset(),
						LengthAdjustmentType.EXPAND, anchor);

				// Draw rotated marker labels rather than horizontal
				TextUtils.drawRotatedString(label, g2,
						(float) coordinates.getX(), (float) coordinates.getY() - 3,
						marker.getLabelTextAnchor(), 3.14 / 2, (float) coordinates.getX(), (float) coordinates.getY());

			}
			g2.setComposite(originalComposite);
		}
		// Interval marker
		else if (marker instanceof IntervalMarker) {
			IntervalMarker im = (IntervalMarker) marker;
			double start = im.getStartValue();
			double end = im.getEndValue();
			Range range = domainAxis.getRange();
			if (!(range.intersects(start, end))) {
				return;
			}

			double start2d = domainAxis.valueToJava2D(start, dataArea,
					plot.getDomainAxisEdge());
			double end2d = domainAxis.valueToJava2D(end, dataArea,
					plot.getDomainAxisEdge());
			double low = Math.min(start2d, end2d);
			double high = Math.max(start2d, end2d);

			PlotOrientation orientation = plot.getOrientation();
			Rectangle2D rect = null;
			if (orientation == PlotOrientation.HORIZONTAL) {
				// clip top and bottom bounds to data area
				low = Math.max(low, dataArea.getMinY());
				high = Math.min(high, dataArea.getMaxY());
				rect = new Rectangle2D.Double(dataArea.getMinX(),
						low, dataArea.getWidth(),
						high - low);
			}
			else if (orientation == PlotOrientation.VERTICAL) {
				// clip left and right bounds to data area
				low = Math.max(low, dataArea.getMinX());
				high = Math.min(high, dataArea.getMaxX());
				rect = new Rectangle2D.Double(low,
						dataArea.getMinY(), high - low,
						dataArea.getHeight());
			}

			final Composite originalComposite = g2.getComposite();
			g2.setComposite(AlphaComposite.getInstance(
					AlphaComposite.SRC_OVER, marker.getAlpha()));
			Paint p = marker.getPaint();
			if (p instanceof GradientPaint) {
				GradientPaint gp = (GradientPaint) p;
				GradientPaintTransformer t = im.getGradientPaintTransformer();
				if (t != null) {
					gp = t.transform(gp, rect);
				}
				g2.setPaint(gp);
			}
			else {
				g2.setPaint(p);
			}
			g2.fill(rect);

			// now draw the outlines, if visible...
			if (im.getOutlinePaint() != null && im.getOutlineStroke() != null) {
				if (orientation == PlotOrientation.VERTICAL) {
					Line2D line = new Line2D.Double();
					double y0 = dataArea.getMinY();
					double y1 = dataArea.getMaxY();
					g2.setPaint(im.getOutlinePaint());
					g2.setStroke(im.getOutlineStroke());
					if (range.contains(start)) {
						line.setLine(start2d, y0, start2d, y1);
						g2.draw(line);
					}
					if (range.contains(end)) {
						line.setLine(end2d, y0, end2d, y1);
						g2.draw(line);
					}
				}
				else { // PlotOrientation.HORIZONTAL
					Line2D line = new Line2D.Double();
					double x0 = dataArea.getMinX();
					double x1 = dataArea.getMaxX();
					g2.setPaint(im.getOutlinePaint());
					g2.setStroke(im.getOutlineStroke());
					if (range.contains(start)) {
						line.setLine(x0, start2d, x1, start2d);
						g2.draw(line);
					}
					if (range.contains(end)) {
						line.setLine(x0, end2d, x1, end2d);
						g2.draw(line);
					}
				}
			}

			String label = marker.getLabel();
			RectangleAnchor anchor = marker.getLabelAnchor();
			if (label != null) {
				Font labelFont = marker.getLabelFont();
				g2.setFont(labelFont);
				g2.setPaint(marker.getLabelPaint());
				Point2D coordinates = calculateDomainMarkerTextAnchorPoint(
						g2, orientation, dataArea, rect,
						marker.getLabelOffset(), marker.getLabelOffsetType(),
						anchor);

				// Draw rotated marker labels rather than horizontal
				TextUtils.drawRotatedString(label, g2,
						(float) coordinates.getX(), (float) coordinates.getY() - 3,
						marker.getLabelTextAnchor(), 3.14 / 2, (float) coordinates.getX(), (float) coordinates.getY());
			}
			g2.setComposite(originalComposite);

		}

	}


	/**
	 * Returns <code>true</code> if the specified pass is the one for drawing the selected items that have been pushed to draw as last elements.
	 *
	 * @param pass the pass.
	 *
	 * @return A boolean.
	 */
	protected boolean isSelectedItemPass(int pass) {
		return pass == 2;
	}


	@Override
	public int getPassCount() {
		return 3;
	}

	@Override
	public void drawItem(Graphics2D g2, XYItemRendererState state, Rectangle2D dataArea, PlotRenderingInfo info, XYPlot plot, ValueAxis domainAxis, ValueAxis rangeAxis, XYDataset dataset, int series,
			int item, CrosshairState crosshairState, int pass) {

		isLinePass = isLinePass(pass);

		// Mouse over selected item or multiple selected items
		if (this.multipleItemsSelector.isMouseOverItem(series, item) || this.multipleItemsSelector.isSelectedItem(series, item)) {
			// First pass, push the selected item and skip the drawing
			if (this.isLinePass(pass)) {

				// Push the mouse over item
				if (this.multipleItemsSelector.isMouseOverItem(series, item)) {
					this.multipleItemsSelector.pushItemToDrawAsLast(series, item, true);
				}
				// FIXME: Push the entire mouse over series to all its items as last (ne fonctionne pas car les autres séries sont déjà tracées à cet instant,
				// il faudrait pouvoir swapper avec une autre series ou sinon modifier les Plot.render() pour swapper complètement le tracé de series)
				// else if(this.multipleItemsSelector.isMouseOverSeries(series)) {
				// this.multipleItemsSelector.pushItemToDrawAsLast(series, item, false);
				// }
				// Multiple selection
				else if (this.multipleItemsSelector.isSelectedItem(series, item)) {
					this.multipleItemsSelector.pushItemToDrawAsLast(series, item, false);
				}
				return;
			}
			// Second pass, skip the drawing
			else if (this.isItemPass(pass)) {
				return;
			}
			// Third pass, pop the selected item and draw all the previously skipped passes
			else if (this.isSelectedItemPass(pass)) {

				EntityCollection entities = null;
				if (info != null) {
					entities = info.getOwner().getEntityCollection();
				}


				item = this.multipleItemsSelector.popItemToDrawAsLast(series);
				// draw all skipped passes of the selected item
				for (int i = 0; i < this.getPassCount() - 1; i++) {
					super.drawItem(g2, state, dataArea, info, plot, domainAxis, rangeAxis, dataset, series, item, crosshairState, i);
				}

				// force shape rendering because the XYStepRenderer doesn't manage it by default
				drawSecondaryPass(g2, plot, dataset, pass, series, item, domainAxis, dataArea, rangeAxis, crosshairState, entities);

				// Multiple selection highlighting
				if (this.multipleItemsSelector.isSelectedItem(series, item)) {
					this.drawSelectedItem(g2, state, dataArea, info, plot, domainAxis, rangeAxis, dataset, series, item, crosshairState, pass);
				}


				// FIXME: draw all passes of all previously skipped selected items
				// for(series = 0; series < ((MultipleItemSelector)this.itemSelector).getSelectedSeriesCount(); series++) {
				// int[] selectedItems = ((MultipleItemSelector)this.itemSelector).getSelectedItems(series);
				// for(int i = 0; i < selectedItems.length; i++) {
				// for(pass = 0; pass < this.getPassCount() - 1; pass++) {
				// System.out.println("ItemSelectionXYLineAndShapeRenderer.drawItem(): draw skipped pass " + pass + " for selected item " + item);
				// super.drawItem(g2, state, dataArea, info, plot, domainAxis, rangeAxis, dataset, series, selectedItems[i], crosshairState, pass);
				// }
				// }
				// }



			}

		}
		// Normal item
		else {
			super.drawItem(g2, state, dataArea, info, plot, domainAxis, rangeAxis, dataset, series, item, crosshairState, pass);

			// force shape rendering because the XYStepRenderer doesn't manage it by default
			// EntityCollection entities = null;
			// if(info != null) {
			// entities = info.getOwner().getEntityCollection();
			// }
			//
			// drawSecondaryPass(g2, plot, dataset, pass, series, item, domainAxis, dataArea, rangeAxis, crosshairState, entities);
		}
	}

	/**
	 * Draws the item using selected item rendering.
	 * 
	 * @param g2
	 * @param state
	 * @param dataArea
	 * @param info
	 * @param plot
	 * @param domainAxis
	 * @param rangeAxis
	 * @param dataset
	 * @param series
	 * @param item
	 * @param crosshairState
	 * @param pass
	 */
	public void drawSelectedItem(Graphics2D g2, XYItemRendererState state, Rectangle2D dataArea, PlotRenderingInfo info, XYPlot plot, ValueAxis domainAxis, ValueAxis rangeAxis, XYDataset dataset,
			int series, int item, CrosshairState crosshairState, int pass) {
		// get the selected item screen coords
		double x1 = dataset.getXValue(series, item);
		double y1 = dataset.getYValue(series, item);
		if (Double.isNaN(y1) || Double.isNaN(x1)) {
			return;
		}
		RectangleEdge xAxisLocation = plot.getDomainAxisEdge();
		RectangleEdge yAxisLocation = plot.getRangeAxisEdge();
		int shapeScreenX = (int) domainAxis.valueToJava2D(x1, dataArea, xAxisLocation);
		int shapeScreenY = (int) rangeAxis.valueToJava2D(y1, dataArea, yAxisLocation);

		// Construct and draw the label
		String label = getSelectedItemLabelFromDataset(dataset, series, item);
		if (label != null) {

			int labelScreenX = shapeScreenX - 50;
			int labelScreenY = shapeScreenY - 60;


			g2.setPaint(getItemPaint(series, item));
			g2.setStroke(new BasicStroke(1));


			Font font = getDefaultItemLabelFont();

			// compute the multi line label bounds
			String[] lines = label.split("\n"); //$NON-NLS-1$
			double width = 0;
			double height = 0;
			for (int i = 0; i < lines.length; i++) {
				if (i == 0) {
					font = getDefaultItemLabelFont().deriveFont(Font.BOLD);
				}
				else {
					font = getDefaultItemLabelFont();
				}

				Rectangle2D lineBounds = font.getStringBounds(lines[i], g2.getFontRenderContext());
				if (lineBounds.getWidth() > width) {
					width = lineBounds.getWidth();
				}
				height += lineBounds.getHeight();
			}

			labelScreenX -= width / 2;


			// label background
			int margin = 10;
			width += margin;
			height += margin;
			Shape labelBackground = new RoundRectangle2D.Double(labelScreenX - margin / 2, labelScreenY - height + 10, width, height, 5, 5);


			// line between the shape and the label background
			g2.drawLine(shapeScreenX, shapeScreenY, (int) (labelScreenX + width / 2), (int) (labelScreenY + height / 2 - (margin * 2)));

			// background
			int opacity = 180;
			if (this.multipleItemsSelector.isMouseOverItem(series, item)) {
				opacity = 255;
			}
			g2.setPaint(new Color(250, 250, 250, opacity));
			g2.fill(labelBackground);

			// background outline
			g2.setPaint(getItemPaint(series, item));
			g2.draw(labelBackground);

			labelScreenY -= height - margin;


			// label
			for (int i = 0; i < lines.length; i++) {
				if (i == 0) {
					font = getDefaultItemLabelFont().deriveFont(Font.BOLD);
				}
				else {
					font = getDefaultItemLabelFont();
				}
				g2.setFont(font);
				g2.drawString(lines[i], labelScreenX, labelScreenY += g2.getFontMetrics().getHeight());
			}

		}
	}

	/**
	 * Constructs the label for the specified selected item using the dataset.
	 * 
	 * @param dataset
	 * @param series
	 * @param item
	 * @return
	 */
	public String getSelectedItemLabelFromDataset(XYDataset dataset, int series, int item) {
		return "test label"; //$NON-NLS-1$
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}
}
