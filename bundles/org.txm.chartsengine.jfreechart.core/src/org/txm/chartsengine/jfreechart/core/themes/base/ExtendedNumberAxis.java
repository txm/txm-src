package org.txm.chartsengine.jfreechart.core.themes.base;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.axis.AxisState;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTick;
import org.jfree.chart.axis.Tick;
import org.jfree.chart.axis.TickType;
import org.jfree.chart.ui.RectangleEdge;

/**
 * An axis with extended drawing options as drawing only positive/negative ticks values or not drawing values greater/lower than a specified value.
 * 
 * @author Sebastien Jacquot
 *
 */
public class ExtendedNumberAxis extends NumberAxis {


	/**
	 * 
	 */
	private static final long serialVersionUID = 2257886275383825542L;

	/**
	 * To draw or not negative values.
	 */
	protected boolean negativeValuesVisible;

	/**
	 * To draw or not positives values.
	 */
	protected boolean positivesValuesVisible;


	/**
	 * The minimum value to draw.
	 */
	protected double minimumVisibleValue;

	/**
	 * The maximum value to draw.
	 */
	protected double maximumVisibleValue;


	/**
	 * Creates an axis and copy data from the specified <code>NumberAxis</code>.
	 * 
	 * @param axis
	 * @param negativeValuesVisible
	 * @param positivesValuesVisible
	 * @param minimumVisibleValue
	 * @param maximumVisibleValue
	 */
	public ExtendedNumberAxis(NumberAxis axis, boolean negativeValuesVisible, boolean positivesValuesVisible, double minimumVisibleValue, double maximumVisibleValue) {
		super(axis.getLabel());

		this.negativeValuesVisible = negativeValuesVisible;
		this.positivesValuesVisible = positivesValuesVisible;
		this.minimumVisibleValue = minimumVisibleValue;
		this.maximumVisibleValue = maximumVisibleValue;

		this.setLabelFont(axis.getLabelFont());
		this.setLabelPaint(axis.getLabelPaint());
		this.setAxisLineVisible(axis.isAxisLineVisible());
		this.setTickMarksVisible(axis.isTickMarksVisible());
		this.setTickLabelPaint(axis.getTickLabelPaint());
		this.setTickLabelFont(axis.getTickLabelFont());
		this.setStandardTickUnits(axis.getStandardTickUnits());

		// TODO: SJ: add other values copies
	}


	@Override
	public List refreshTicks(Graphics2D g2, AxisState state, Rectangle2D dataArea, RectangleEdge edge) {

		List<Tick> allTicks = super.refreshTicks(g2, state, dataArea, edge);
		List<Tick> newTicks = new ArrayList<>();

		for (Object tick : allTicks) {
			NumberTick numberTick = (NumberTick) tick;

			// Do not draw negative values
			if (this.negativeValuesVisible == false && TickType.MAJOR.equals(numberTick.getTickType()) && numberTick.getValue() < 0) {
				continue;
			}
			// Do not draw positive values
			if (this.positivesValuesVisible == false && TickType.MAJOR.equals(numberTick.getTickType()) && numberTick.getValue() > 0) {
				continue;
			}
			// Do not draw values less than minimum value
			if (TickType.MAJOR.equals(numberTick.getTickType()) && numberTick.getValue() < this.minimumVisibleValue) {
				continue;
			}
			// Do not draw values greater than maximum value
			if (TickType.MAJOR.equals(numberTick.getTickType()) && numberTick.getValue() > this.maximumVisibleValue) {
				continue;
			}

			newTicks.add(numberTick);
		}
		return newTicks;
	}


	/**
	 * @return the negativeValuesVisible
	 */
	public boolean isNegativeValuesVisible() {
		return negativeValuesVisible;
	}


	/**
	 * @param negativeValuesVisible the negativeValuesVisible to set
	 */
	public void setNegativeValuesVisible(boolean negativeValuesVisible) {
		this.negativeValuesVisible = negativeValuesVisible;
	}


	/**
	 * @return the positivesValuesVisible
	 */
	public boolean isPositivesValuesVisible() {
		return positivesValuesVisible;
	}


	/**
	 * @param positivesValuesVisible the positivesValuesVisible to set
	 */
	public void setPositivesValuesVisible(boolean positivesValuesVisible) {
		this.positivesValuesVisible = positivesValuesVisible;
	}


	/**
	 * @return the minimumVisibleValue
	 */
	public double getMinimumVisibleValue() {
		return minimumVisibleValue;
	}


	/**
	 * @param minimumVisibleValue the minimumVisibleValue to set
	 */
	public void setMinimumVisibleValue(double minimumVisibleValue) {
		this.minimumVisibleValue = minimumVisibleValue;
	}


	/**
	 * @return the maximumVisibleValue
	 */
	public double getMaximumVisibleValue() {
		return maximumVisibleValue;
	}


	/**
	 * @param maximumVisibleValue the maximumVisibleValue to set
	 */
	public void setMaximumVisibleValue(double maximumVisibleValue) {
		this.maximumVisibleValue = maximumVisibleValue;
	}
}
