package org.txm.chartsengine.jfreechart.core.renderers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.event.RendererChangeEvent;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.XYPlot;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers.ItemSelectionXYLineAndShapeRenderer;

/**
 * Chart item selector dedicated to mouse over item managing and rendering.
 * Class providing methods to store selected series and item, selected shape coordinates and methods for updating data sets for drawing the mouse over selected item as last, over other elements.
 * 
 * @author sjacquot
 *
 */
public class MouseOverItemSelector {

	/**
	 * The renderer providing methods do draw selected items.
	 */
	protected IRendererWithItemSelection renderer;

	/**
	 * Linked chart result.
	 */
	protected ChartResult result;

	/**
	 * The series of the mouse over item.
	 */
	protected int mouseOverSeries = -1;

	/**
	 * The selected item in selected series.
	 */
	protected int mouseOverItemInSeries = -1;


	/**
	 * The x coordinate in screen-space of the selected chart entity.
	 */
	protected int xCoord;

	/**
	 * The y coordinate in screen-space of the selected chart entity.
	 */
	protected int yCoord;

	/**
	 * The active state.
	 */
	protected boolean active;

	
	/**
	 * The value to add to label dimensions of an item under mouse cursor.
	 */
	protected float labelSizeIncreaseValue = 7;
	
	/**
	 * The value to add to shape dimensions of an item under mouse cursor.
	 */
	protected float shapeSizeIncreaseValue = 7;
	
	
	/**
	 * Used to disable the label and shape size increase on mouse over.
	 */
	//FIXME: SJ: tmp fix to manage mouse over scaling and size styling together,
	// until org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.renderers.CAItemStyleRenderer.
	// will be moved to a styler class outside the CA renderer.
	protected boolean sizeIncreaseEnabled = true;
	

	/**
	 * Temporary list to store the selected items and draw them as last elements, over other entities.
	 */
	protected Map<Integer, List<Integer>> itemsToDrawAsLastElements;


	protected ChartPanel chartPanel;
	
	

	/**
	 * 
	 * @param renderer
	 */
	public MouseOverItemSelector(IRendererWithItemSelection renderer) {
		this.renderer = renderer;
		this.active = true;

		this.itemsToDrawAsLastElements = new HashMap<Integer, List<Integer>>();
	}


	/**
	 * Sets the selected chart entity x, y screen-space coordinates.
	 * (Used by the chart mouse listeners to store the coordinates)
	 * 
	 * @param x
	 * @param y
	 */
	public void setMouseOverItemCoords(int x, int y) {
		this.xCoord = x;
		this.yCoord = y;
	}


	/**
	 * Sets the item to be selected ((-1, -1) for none selected).
	 * 
	 * @param series the series index
	 * @param item the item index
	 */
	public void setMouseOverItem(int series, int item) {

		if (!this.active) {
			return;
		}

		if (this.mouseOverSeries == series && this.mouseOverItemInSeries == item) {
			return;
		}

		this.mouseOverSeries = series;
		this.mouseOverItemInSeries = item;


		this.renderer.updateDatasetForDrawingSelectedItemAsLast();

		// Refresh drawing
		this.renderer.notifyListeners(new RendererChangeEvent(this.renderer));
	}



	/**
	 * Checks if the specified item in specified series is selected.
	 * 
	 * @param series
	 * @param item
	 * @return
	 */
	public boolean isMouseOverItem(int series, int item) {
		return (this.mouseOverSeries == series && this.mouseOverItemInSeries == item);
	}



	/**
	 * Checks if the specified series is selected.
	 * 
	 * @param series
	 * @return
	 */
	public boolean isMouseOverSeries(int series) {
		return (this.mouseOverSeries == series);
	}



	/**
	 * @return the xCoord
	 */
	public int getXCoord() {
		return xCoord;
	}


	/**
	 * @return the yCoord
	 */
	public int getYCoord() {
		return yCoord;
	}


	/**
	 * Returns the selected series.
	 * 
	 * @return the selectedSeries
	 */
	public int getMouseOverSeries() {
		return mouseOverSeries;
	}

	/**
	 * Returns the selected item in the specified series or -1 if not item is selected.
	 * 
	 * @param series
	 * @return
	 */
	public int getMouseOverItemInSeries(int series) {
		if (series != this.mouseOverSeries) {
			return -1;
		}
		return mouseOverItemInSeries;
	}


	/**
	 * Checks if the item selection is enabled or disabled.
	 * 
	 * @return the active state
	 */
	public boolean isActive() {
		return active;
	}


	/**
	 * Sets the active state. Setting state to false results in disabling the item selection.
	 * 
	 * @param active the active state to set
	 */
	public void setActive(boolean active) {
		// Reset the selected item
		if (!active) {
			this.setMouseOverItem(-1, -1);
		}
		this.active = active;
	}


	/**
	 * Gets the linked result.
	 * 
	 * @return the result
	 */
	public ChartResult getResult() {
		return result;
	}


	/**
	 * Sets the linked result.
	 * 
	 * @param result the result to set
	 */
	public void setResult(ChartResult result) {
		this.result = result;

		// FIXME: SJ: test to recycle the existing chart panel to fix a null pointer exception in the renderer
		if (result.getChart() != null && result.getChart() instanceof JFreeChart) {
			JFreeChart existingChart = (JFreeChart) result.getChart();
			if (existingChart.getPlot() instanceof XYPlot && existingChart.getXYPlot().getRenderer() instanceof IRendererWithItemSelection) {
				this.setChartPanel(((IRendererWithItemSelection) existingChart.getXYPlot().getRenderer()).getItemsSelector().getChartPanel());
			}
			else if (existingChart.getPlot() instanceof CategoryPlot && existingChart.getCategoryPlot().getRenderer() instanceof IRendererWithItemSelection) {
				this.setChartPanel(((IRendererWithItemSelection) existingChart.getCategoryPlot().getRenderer()).getItemsSelector().getChartPanel());
			}
		}

	}



	/**
	 * @return the itemsToDrawAsLastElements
	 */
	public Map<Integer, List<Integer>> getItemsToDrawAsLastElements() {
		return itemsToDrawAsLastElements;
	}

	/**
	 * Pushes the specified item to draw as last.
	 * 
	 * @param series
	 * @param item
	 */
	public void pushItemToDrawAsLast(int series, int item, boolean drawItemTheLast) {
		List selectedItems = this.itemsToDrawAsLastElements.get(series);
		if (selectedItems == null) {
			selectedItems = new ArrayList<Integer>();
			this.itemsToDrawAsLastElements.put(series, selectedItems);
		}
		if (!selectedItems.contains(item)) {
			if (drawItemTheLast) {
				selectedItems.add(0, item);
			}
			else {
				selectedItems.add(item);
			}
		}
	}



	/**
	 * Pops item to draw as last from list.
	 * 
	 * @return
	 */
	public int popItemToDrawAsLast(int series) {

		int item = -1;
		List<Integer> selectedItems = this.itemsToDrawAsLastElements.get(series);
		if (selectedItems != null) {
			item = selectedItems.get(selectedItems.size() - 1);
			selectedItems.remove(selectedItems.size() - 1);
			// Remove the series if there are no more selected items
			if (selectedItems.isEmpty()) {
				this.itemsToDrawAsLastElements.remove(series);
			}
		}
		return item;
	}

	// /**
	// * Changes the CA plot series rendering order according the selected series in item selector.
	// * @param renderer
	// */
	// public void setItemDrawnAtLast(FCAItemSelectionRenderer renderer) {
	//
	// if(this.mouseOverSeries != -1) {
	//
	// XYPlot plot = (XYPlot) this.renderer.getPlot();
	//
	// // FIXME: it swaps only between rows and cols order but we may manage order of item inside same series
	// // Changing the rendering order
	// if(this.mouseOverSeries == 0) {
	// // Row
	// plot.setSeriesRenderingOrder(SeriesRenderingOrder.REVERSE);
	// }
	// else {
	// // Col
	// plot.setSeriesRenderingOrder(SeriesRenderingOrder.FORWARD);
	// }
	//
	//
	//
	//// // FIXME : test de tracé d'un background pour les labels des points en passant par une annotation
	// // exemple de tracé
	//// this.getPlot().addAnnotation(new XYAnnotation() {
	////
	//// @Override
	//// public void removeChangeListener(AnnotationChangeListener listener) {
	//// // TODO Auto-generated method stub
	////
	//// }
	////
	//// @Override
	//// public void addChangeListener(AnnotationChangeListener listener) {
	//// // TODO Auto-generated method stub
	////
	//// }
	////
	//// @Override
	//// public void draw(Graphics2D g2, XYPlot plot,
	//// Rectangle2D dataArea, ValueAxis domainAxis,
	//// ValueAxis rangeAxis, int rendererIndex,
	//// PlotRenderingInfo info) {
	//// g2.setColor(Color.red);
	//// g2.fillRect(itemSelector.getXCoord(), itemSelector.getYCoord(), 50, 20);
	////
	//// }
	//// });
	// }
	//
	// }


//	public void setItemDrawnAtLast(ItemSelectionXYLineAndShapeRenderer itemSelectionXYLineAndShapeRenderer) {
//		// TODO Auto-generated method stub
//
//	}


	/**
	 * @return the chartPanel
	 */
	public ChartPanel getChartPanel() {
		return this.chartPanel;
	}


	/**
	 * @param chartPanel the chartPanel to set
	 */
	public void setChartPanel(ChartPanel chartPanel) {
		this.chartPanel = chartPanel;
	}


	/**
	 * Enables or disables the label and shape item size increase on mouse over.
	 * 
	 * @param sizeIncreaseEnabled the sizeIncreaseEnabled to set
	 */
	public void setSizeIncreaseEnabled(boolean sizeIncreaseEnabled) {
		this.sizeIncreaseEnabled = sizeIncreaseEnabled;
	}

	
	/**
	 * @return the sizeIncreaseEnabled
	 */
	public boolean isSizeIncreaseEnabled() {
		return sizeIncreaseEnabled;
	}
	
	
	/**
	 * 
	 * @return the shapeSizeIncreaseValue
	 */
	public float getShapeSizeIncreaseValue() {
		return shapeSizeIncreaseValue;
	}


	
	/**
	 * @param shapeSizeIncreaseValue the shapeScalingValue to set
	 */
	public void setShapeScalingValue(float shapeSizeIncreaseValue) {
		this.shapeSizeIncreaseValue = shapeSizeIncreaseValue;
	}



	/**
	 * @return the labelSizeIncreaseValue
	 */
	public float getLabelSizeIncreaseValue() {
		return labelSizeIncreaseValue;
	}


	
	/**
	 * @param labelSizeIncreaseValue the labelScalingValue to set
	 */
	public void setLabelSizeIncreaseValue(float labelSizeIncreaseValue) {
		this.labelSizeIncreaseValue = labelSizeIncreaseValue;
	}

}
