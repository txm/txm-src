package org.txm.chartsengine.jfreechart.core.themes.base;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.LegendItem;
import org.jfree.chart.LegendItemCollection;
import org.jfree.chart.LegendItemSource;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.SpiderWebPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.AbstractRenderer;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.CategoryStepRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.chart.renderer.xy.XYStepRenderer;
import org.jfree.chart.title.LegendTitle;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.jfreechart.core.JFCChartsEngine;
import org.txm.chartsengine.jfreechart.core.renderers.XYCardinalSplineRenderer;

public class JFCTheme extends StandardChartTheme {


	/**
	 * 
	 */
	private static final long serialVersionUID = -5208579375272020649L;

	/**
	 * Linked charts engine.
	 */
	protected JFCChartsEngine chartsEngine;

	/**
	 * Series paints.
	 */
	protected ArrayList<Color> seriesPaints;

	/**
	 * Series shapes.
	 */
	protected HashMap<Integer, Shape> seriesShapes;

	/**
	 * Series strokes.
	 */
	protected ArrayList<Stroke> seriesStrokes;



	// TODO : preferences
	/**
	 * Items size scaling factor.
	 */
	protected float itemShapesScalingFactor = 1.2f;

	/**
	 * Line chart width.
	 */
	protected float seriesLineWidth = 1.8f;

	/**
	 * Global Font name.
	 */
	protected String baseFontName = "Lucida Sans Unicode"; //$NON-NLS-1$

	/**
	 * Fonts points to add for other font than regular with regular as base font.
	 */
	protected int fontsPointsToAdd = 0;

	/**
	 * Antialiasing (text and shapes).
	 */
	protected boolean antialiasing = true;

	/**
	 * Grid lines color.
	 */
	protected Color gridLinesColor = Color.decode("#C0C0C0"); //$NON-NLS-1$



	/**
	 * The number of points between data items when drawn using XYSplineRenderer.
	 */
	protected int splineRendererPrecision = 5;


	/**
	 * Global font.
	 */
	protected Font regularFont;



	/**
	 * 
	 * @param chartsEngine
	 * @param name
	 */
	protected JFCTheme(JFCChartsEngine chartsEngine, String name) {
		super(name);
		this.chartsEngine = chartsEngine;
	}


	/**
	 * 
	 * @param result
	 */
	public void applyThemeToChart(ChartResult result) {
		// FIXME
	}



	/**
	 * Creates fonts from the result font parameter or charts engine preferences current font.
	 */
	public void createFonts(ChartResult result) {

		Font baseFont = ChartsEngine.createFont(result.getFont());

		//	System.err.println("JFCTheme.createFonts(): Font = " + baseFont);

		// Titles
		this.setExtraLargeFont(baseFont.deriveFont(baseFont.getStyle(), baseFont.getSize() + 5));

		// Axis
		this.setLargeFont(baseFont.deriveFont(baseFont.getStyle(), baseFont.getSize() + 4)); // axis labels

		// Used by annotations
		this.setSmallFont(baseFont.deriveFont(Font.BOLD, this.getExtraLargeFont().getSize() - 4));

		// Item labels, legend labels, axis ticks labels
		this.regularFont = baseFont.deriveFont(baseFont.getStyle(), baseFont.getSize());
		this.setRegularFont(this.regularFont);

	}

	/**
	 * Disables outline shape paint and sets the new shapes.
	 * 
	 * @param legendTitle
	 * @param overrideShapes
	 * @param palette
	 */
	public void applyLegendDefaultSettings(LegendTitle legendTitle, boolean overrideShapes, boolean linesVisible, ArrayList<Color> palette) {

		if (legendTitle != null) {

			legendTitle.setMargin(10, 10, 10, 10);

			// Disable outline shape paint and set the new shape if defined
			LegendItemSource[] sources = legendTitle.getSources();

			if (sources != null) {

				List<LegendItemSource> tmpSources = new ArrayList<LegendItemSource>();


				for (int i = 0; i < sources.length; i++) {

					// Collection of legend items to contain modified item object
					final LegendItemCollection legendItemsCollection = new LegendItemCollection();

					LegendItemCollection legendItems = sources[i].getLegendItems();

					for (int k = 0; k < legendItems.getItemCount(); k++) {
						LegendItem legendItem = legendItems.get(k);

						Shape shape = legendItem.getShape();
						if (overrideShapes) {
							// Round rectangle
							Rectangle2D bounds = shape.getBounds2D();
							// shape = new RoundRectangle2D.Double(bounds.getX(), bounds.getY(), bounds.getWidth() + 7, bounds.getHeight() + 4, 5, 5);
							shape = new RoundRectangle2D.Double(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight(), 5, 5);
						}

						// Items rendering colors mode
						Paint fillPaint = legendItem.getFillPaint();
						Paint linePaint = legendItem.getLinePaint();
						fillPaint = linePaint = palette.get(k);

						// New legend item
						legendItem = new LegendItem(legendItem.getLabel(), legendItem.getDescription(), legendItem.getToolTipText(), legendItem.getURLText(), legendItem.isShapeVisible(),
								shape, legendItem.isShapeFilled(), fillPaint, false, legendItem.getOutlinePaint(),
								legendItem.getOutlineStroke(), linesVisible, legendItem.getLine(), legendItem.getLineStroke(), linePaint);

						legendItem.setLabelFont(this.regularFont.deriveFont(Font.BOLD));

						legendItemsCollection.add(legendItem);
					}

					LegendItemSource source = new LegendItemSource() {

						public LegendItemCollection getLegendItems() {
							return legendItemsCollection;
						}
					};

					tmpSources.add(source);
				}

				// Create a new source array for adding to legend
				LegendItemSource[] newSources = new LegendItemSource[tmpSources.size()];

				for (int i = 0; i < tmpSources.size(); i++) {
					newSources[i] = tmpSources.get(i);
				}

				// Add the updated sources array to legend
				legendTitle.setSources(newSources);

			}
		}
	}


	/**
	 * Creates a chart panel with theme settings.
	 * The overriding methods of subclass should manually calls:
	 * <code>
	 * // Apply theme to the chart
	 * 	if(chartPanel.getChart() != null)	{
	 *		this.apply(chartPanel.getChart());
	 *	}
	 *</code>
	 * for applying the them to the chart and to the chart panel.
	 *
	 * @param chart
	 * @return the chart panel
	 */
	public ChartPanel createChartPanel(JFreeChart chart) {
		return new ChartPanel(chart);
	}

	/**
	 * Creates an XY chart line and shape renderer.
	 * 
	 * @param linesVisible
	 * @param shapesVisible
	 * @return
	 */
	public XYLineAndShapeRenderer createXYLineAndShapeRenderer(ChartResult result, JFreeChart chart, boolean linesVisible, boolean shapesVisible) {
		return new XYLineAndShapeRenderer(linesVisible, shapesVisible);
	}


	/**
	 * Creates an XY step renderer.
	 * 
	 * @param linesVisible
	 * @param shapesVisible
	 * @return
	 */
	public XYStepRenderer createXYStepRenderer(boolean linesVisible, boolean shapesVisible) {
		XYStepRenderer renderer = new XYStepRenderer();
		renderer.setDefaultLinesVisible(linesVisible);
		renderer.setDefaultShapesVisible(shapesVisible);
		return renderer;
	}



	/**
	 * Creates a Category step renderer.
	 * 
	 * @return
	 */
	public CategoryStepRenderer createCategoryStepRenderer() {
		CategoryStepRenderer renderer = new CategoryStepRenderer();
		return renderer;
	}



	/**
	 * Creates an XY chart spline renderer.
	 * 
	 * @param linesVisible
	 * @param shapesVisible
	 * @return
	 */
	public XYSplineRenderer createXYSplineRenderer(boolean linesVisible, boolean shapesVisible) {
		XYSplineRenderer renderer = new XYSplineRenderer(this.splineRendererPrecision);
		renderer.setDefaultLinesVisible(linesVisible);
		renderer.setDefaultShapesVisible(shapesVisible);
		return renderer;
	}


	/**
	 * Creates an XY chart cardinal spline renderer.
	 * 
	 * @param linesVisible
	 * @param shapesVisible
	 * @return
	 */
	public XYCardinalSplineRenderer createXYCardinalSplineRenderer(boolean linesVisible, boolean shapesVisible) {
		XYCardinalSplineRenderer renderer = new XYCardinalSplineRenderer(this.splineRendererPrecision, 0.3, linesVisible, shapesVisible);
		return renderer;
	}


	/**
	 * Creates an XY bar chart renderer.
	 * 
	 * @return
	 */
	public XYBarRenderer createXYBarRenderer() {
		return new XYBarRenderer();
	}


	/**
	 * Creates a category bar chart renderer.
	 * 
	 * @return
	 */
	public BarRenderer createCategoryBarRenderer() {
		return new BarRenderer();
	}


	/**
	 * Creates a category chart line and shape renderer.
	 * 
	 * @param linesVisible
	 * @param shapesVisible
	 * @return
	 */
	public LineAndShapeRenderer createCategoryLineAndShapeRenderer(boolean linesVisible, boolean shapesVisible) {
		return new LineAndShapeRenderer(linesVisible, shapesVisible);
	}



	/**
	 * Sets the series paints in the plot renderer according to user defined paints and specified items colors rendering mode.
	 * 
	 */
	public ArrayList<Color> applySeriesPaint(ChartResult result) {

		ArrayList<Color> palette = null;

		Plot plot = ((JFreeChart) result.getChart()).getPlot();

		// XYPlot
		if (plot instanceof XYPlot) {

			XYPlot typedPlot = (XYPlot) plot;
			XYItemRenderer renderer = typedPlot.getRenderer();

			palette = this.chartsEngine.getTheme().getPaletteFor(result.getRenderingColorsMode(), typedPlot.getSeriesCount());
			for (int i = 0; i < palette.size(); i++) {
				renderer.setSeriesPaint(i, palette.get(i));
			}
			
		}
		// CategoryPlot
		else if (plot instanceof CategoryPlot) {

			CategoryPlot typedPlot = (CategoryPlot) plot;
			CategoryItemRenderer renderer = (CategoryItemRenderer) typedPlot.getRenderer();

			palette = this.chartsEngine.getTheme().getPaletteFor(result.getRenderingColorsMode(), typedPlot.getDataset().getRowCount());
			for (int i = 0; i < palette.size(); i++) {
				renderer.setSeriesPaint(i, palette.get(i));
			}
		}
		// SpiderPlot
		else if (plot instanceof SpiderWebPlot) {
			SpiderWebPlot typedPlot = (SpiderWebPlot) plot;
			palette = this.chartsEngine.getTheme().getPaletteFor(result.getRenderingColorsMode(), typedPlot.getDataset().getRowCount());
			for (int i = 0; i < palette.size(); i++) {
				typedPlot.setSeriesPaint(i, palette.get(i));
			}
		}
		// PiePlot
		else if (plot instanceof PiePlot) {
			PiePlot typedPlot = (PiePlot) plot;
			palette = this.chartsEngine.getTheme().getPaletteFor(result.getRenderingColorsMode(), typedPlot.getDataset().getItemCount());
			for (int i = 0; i < palette.size(); i++) {
				typedPlot.setSectionPaint(i, palette.get(i));
			}
		}
		return palette;
	}



	/**
	 * Sets the series shapes in the plot renderer according to user defined shapes in a cyclic way.
	 * 
	 * @param plot
	 */
	public void applySeriesShapes(Plot plot) {
		this.applySeriesShapes(plot, true);
	}


	/**
	 * Sets the series shapes in the plot renderer according to user defined shapes.
	 * 
	 * @param plot
	 */
	public void applySeriesShapes(Plot plot, boolean cyclic) {

		if (plot instanceof XYPlot) {

			XYPlot typedPlot = (XYPlot) plot;
			XYItemRenderer renderer = typedPlot.getRenderer();

			for (Entry<Integer, Shape> entry : this.seriesShapes.entrySet()) {
				int index = entry.getKey();
				renderer.setSeriesShape(index, entry.getValue());
			}

		}
		else if (plot instanceof CategoryPlot) {

			CategoryPlot typedPlot = (CategoryPlot) plot;
			CategoryItemRenderer renderer = (CategoryItemRenderer) typedPlot.getRenderer();

			for (Entry<Integer, Shape> entry : this.seriesShapes.entrySet()) {
				int index = entry.getKey();
				renderer.setSeriesShape(index, entry.getValue());
			}

		}
	}


	/**
	 * Sets the series strokes in the plot renderer according to user defined strokes in a cyclic way.
	 * 
	 * @param result
	 */
	public void applySeriesStrokes(ChartResult result) {
		this.applySeriesStrokes(result, true);
	}


	/**
	 * Sets the series strokes in the plot renderer according to user defined strokes.
	 * 
	 * @param result
	 */
	public void applySeriesStrokes(ChartResult result, boolean cyclic) {

		Plot plot = ((JFreeChart) result.getChart()).getPlot();

		// XYPlot
		if (plot instanceof XYPlot) {

			XYPlot typedPlot = (XYPlot) plot;
			AbstractRenderer renderer = (AbstractRenderer) typedPlot.getRenderer();

			for (int i = 0, j = 0; i < typedPlot.getSeriesCount(); i++, j++) {

				// Multiple styles
				if (result.isMultipleLineStrokes()) {
					if (j == this.seriesStrokes.size()) {
						if (cyclic) {
							j = 0;
						}
						else {
							break;
						}
					}
					renderer.setSeriesStroke(i, this.seriesStrokes.get(j));
				}
				// Unique style
				else {
					renderer.setSeriesStroke(i, new BasicStroke(seriesLineWidth));
				}
			}

		}
		// CategoryPlot
		else if (plot instanceof CategoryPlot) {

			CategoryPlot typedPlot = (CategoryPlot) plot;
			CategoryItemRenderer renderer = (CategoryItemRenderer) typedPlot.getRenderer();

			for (int i = 0, j = 0; i < typedPlot.getDataset().getRowCount(); i++, j++) {

				// Multiple styles
				if (result.isMultipleLineStrokes()) {
					if (j == this.seriesStrokes.size()) {
						if (cyclic) {
							j = 0;
						}
						else {
							break;
						}
					}
					renderer.setSeriesStroke(i, this.seriesStrokes.get(j));
				}
				// Unique style
				else {
					renderer.setSeriesStroke(i, new BasicStroke(seriesLineWidth));
				}
			}

		}
	}



}
