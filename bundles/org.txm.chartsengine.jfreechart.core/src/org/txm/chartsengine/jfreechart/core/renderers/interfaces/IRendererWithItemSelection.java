package org.txm.chartsengine.jfreechart.core.renderers.interfaces;

import java.awt.Paint;

import org.jfree.chart.event.RendererChangeEvent;
import org.jfree.chart.plot.Plot;
import org.txm.chartsengine.jfreechart.core.renderers.MouseOverItemSelector;


/**
 * Chart item selection renderer.
 * 
 * @author sjacquot
 *
 */
public interface IRendererWithItemSelection {



	/**
	 * Gets the item selector.
	 * 
	 * @return the item selector assigned to the renderer
	 */
	public MouseOverItemSelector getItemsSelector();

	/**
	 * Updates data set and plot for drawing selected series and item as last, over others element.
	 */
	public void updateDatasetForDrawingSelectedItemAsLast();

	/**
	 * Returns the plot that the renderer has been assigned to (where
	 * <code>null</code> indicates that the renderer is not currently assigned
	 * to a plot).
	 *
	 * @return The plot (possibly <code>null</code>).
	 *
	 */
	public Plot getPlot();

	/**
	 * Notifies all registered listeners that the renderer has been modified.
	 *
	 * @param event information about the change event.
	 */
	public void notifyListeners(RendererChangeEvent event);

	/**
	 * Returns the paint used to fill an item drawn by the renderer.
	 *
	 * @param series the series index (zero-based).
	 *
	 * @return The paint (possibly <code>null</code>).
	 *
	 * @see "#applySeriesPaint(int, Paint)"
	 */
	public Paint getSeriesPaint(int series);

	/**
	 * Initializes the renderer.
	 */
	public void init();

}
