package org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme;

import java.awt.Color;
import java.awt.Paint;
import java.text.DecimalFormat;

/**
 * 
 * @author sjacquot
 *
 */
// FIXME: to complete the code mutualisation work for all renderers
public class RendererUtils {


	/**
	 * Values number format.
	 */
	public static DecimalFormat valuesNumberFormat = new DecimalFormat(new String("#0.####")); //$NON-NLS-1$


	private RendererUtils() {
		// TODO Auto-generated constructor stub
	}



	/**
	 * Gets the color for mouse over item.
	 * 
	 * @return
	 */
	public static Paint getMouseOverItemPaint(Color color) {

		// Gray scale rendering mode
		if (color.getRed() == color.getGreen() && color.getGreen() == color.getBlue()) {
			// FIXME : other version, fixed color
			color = Color.RED;
		}
		// Color rendering and Monochrome modes
		else {
			// FIXME : old version, to remove when new version will be validated, brighter() does not work when value are 0
			// color = color.brighter();
			// FIXME : new version, brightening
			float hsbVals[] = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
			// color = Color.getHSBColor(hsbVals[0], hsbVals[1], 0.5f * ( 1f + hsbVals[2]));
			// // change saturation
			color = Color.getHSBColor(hsbVals[0], 0.3f * hsbVals[1], hsbVals[2]);

		}
		return color;
	}


	/**
	 * Gets the color for a selected item.
	 * 
	 * @return
	 */
	public static Paint getSelectedItemPaint(Color color) {
		// FIXME: tests
		// color = color.darker();
		// change saturation
		float hsbVals[] = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
		color = Color.getHSBColor(hsbVals[0], 0.5f * hsbVals[1], hsbVals[2]);
		return color;
	}
}
