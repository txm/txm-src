package org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.HashMap;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.AbstractRenderer;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.CategoryStepRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.chart.renderer.xy.XYStepRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.ui.RectangleEdge;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.chart.util.ShapeUtils;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.jfreechart.core.JFCChartsEngine;
import org.txm.chartsengine.jfreechart.core.renderers.XYCardinalSplineRenderer;
import org.txm.chartsengine.jfreechart.core.themes.base.BlockRoundBorder;
import org.txm.chartsengine.jfreechart.core.themes.base.CategoryAxisBetweenTicks;
import org.txm.chartsengine.jfreechart.core.themes.base.JFCTheme;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers.ItemSelectionCategoryBarRenderer;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers.ItemSelectionCategoryLineAndShapeRenderer;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers.ItemSelectionCategoryStepRenderer;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers.ItemSelectionXYBarRenderer;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers.ItemSelectionXYCardinalSplineRenderer;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers.ItemSelectionXYLineAndShapeRenderer;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers.ItemSelectionXYSplineRenderer;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers.ItemSelectionXYStepRenderer;

/**
 * Highcharts GWT library default rendering theme.
 * The theme can not be fully supported by ChartFactory.setChartTheme() because additional plot, renderer and chart settings modifications
 * are made by <code>applyThemeToChart()</code> according to the type of chart .
 * 
 * @author sjacquot
 *
 */
public class HighchartsDefaultTheme extends JFCTheme {

	private static final long serialVersionUID = 9123057676853993928L;



	public HighchartsDefaultTheme(JFCChartsEngine chartsEngine) {
		super(chartsEngine, "Highcharts - Default"); // $NON-NLS-1$ //$NON-NLS-1$


		// Title
		this.setTitlePaint(Color.decode("#4B4B6D")); //$NON-NLS-1$
		this.setSubtitlePaint(Color.decode("#4B4B6D")); //$NON-NLS-1$

		// Axis
		this.setAxisLabelPaint(Color.decode("#4D759E")); //$NON-NLS-1$
		this.setAxisOffset(new RectangleInsets(0, 0, 0, 0));

		// Labels
		this.setItemLabelPaint(Color.decode("#333333")); //$NON-NLS-1$

		// Grid
		this.setGridBandPaint(Color.white);
		this.setRangeGridlinePaint(gridLinesColor);
		this.setDomainGridlinePaint(gridLinesColor);

		this.setChartBackgroundPaint(Color.white);

		// Legends
		this.setLegendItemPaint(Color.decode("#274B6D")); //$NON-NLS-1$


		this.setPlotBackgroundPaint(Color.white);
		this.setTickLabelPaint(Color.decode("#666666")); //$NON-NLS-1$

		// TODO : useless for this theme ?
		// this.setBaselinePaint(Color.decode("#C0D0E0"));
		// this.setSmallFont(regularFont.deriveFont(Font.BOLD));


		// Series paints (Highchart color codes)
		// FIXME: SJ: this should be moved to a preference as discussed
		this.seriesPaints = new ArrayList<Color>();
		this.seriesPaints.add(0, Color.decode("#2F7ED8")); // Blue //$NON-NLS-1$
		this.seriesPaints.add(1, Color.decode("#0D233A")); // Dark blue //$NON-NLS-1$
		this.seriesPaints.add(2, Color.decode("#8BBC21")); // Green //$NON-NLS-1$
		this.seriesPaints.add(3, Color.decode("#910000")); // Red //$NON-NLS-1$



		// Series shape (Highchart shapes)
		this.seriesShapes = new HashMap<Integer, Shape>();
		// Diamond
		this.seriesShapes.put(1, ShapeUtils.createDiamond(itemShapesScalingFactor * 3.4f));
		// Ellipse
		float size = 6.4f * itemShapesScalingFactor;
		this.seriesShapes.put(0, new Ellipse2D.Float(-size / 2, -size / 2, size, size));
		// Triangle
		this.seriesShapes.put(3, ShapeUtils.createUpTriangle(itemShapesScalingFactor * 3.2f));


		// Series stroke
		this.seriesStrokes = new ArrayList<Stroke>();
		this.seriesStrokes.add(new BasicStroke(seriesLineWidth)); // Plain
		this.seriesStrokes.add(new BasicStroke(seriesLineWidth, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1.0f, new float[] { 1.0f, 6.0f }, 0.0f)); // Dashed
		this.seriesStrokes.add(new BasicStroke(seriesLineWidth)); // Plain
		this.seriesStrokes.add(new BasicStroke(seriesLineWidth, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1.0f, new float[] { 1.0f, 9.0f }, 0.0f)); // Dashed 2


		// Bar charts flat painters
		this.setBarPainter(new StandardBarPainter());
		this.setXYBarPainter(new StandardXYBarPainter());


		this.splineRendererPrecision = 18;

	}


	/**
	 * Applies this theme to the specified chart. Modifies plot and renderer too.
	 * (The theme can not be used with ChartFactory.setChartTheme() because of additional plot, renderer and chart settings according to the type of chart.)
	 * 
	 * @param result
	 */
	public void applyThemeToChart(ChartResult result) {

		this.createFonts(result);

		JFreeChart chart = (JFreeChart) result.getChart();

		super.apply(chart);



		// int itemsColorsRenderingMode = result.getRenderingColorsMode();

		// FIXME : UI settings
		// UIManager.put("ToolTip.background", Color.gray);
		// Border border = BorderFactory.cr
		// UIManager.put("ToolTip.foreground", myFGColor);
		// UIManager.put("ToolTip.font", myFont);
		// UIManager.put("ToolTip.background",new Color(0x40,0x51,0x67,0x40));

		// TODO : Rendering misc settings
		// chart.setRenderingHints( new RenderingHints( RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY) );
		// chart.setRenderingHints( new RenderingHints( RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY) );
		// chart.setRenderingHints( new RenderingHints( RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY) );

		// Antialiasing
		// chart.setRenderingHints( new RenderingHints( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON ) );
		chart.setAntiAlias(this.antialiasing);
		chart.setTextAntiAlias(this.antialiasing);

		chart.getPlot().setNoDataMessage("Loading chart...");
		chart.getPlot().setInsets(new RectangleInsets(10, 10, 10, 10));
		if (chart.getTitle() != null) {
			chart.getTitle().setMargin(10, 0, 0, 0);
		}


		// Add plot renderer modifications
		Plot tmpPlot = chart.getPlot();


		// Shared by all charts
		tmpPlot.setOutlineVisible(false);


		// XY Line chart / CA Factorial Map / XY Bar chart / AHC 2D Tree chart
		if (tmpPlot instanceof XYPlot) {

			XYPlot plot = (XYPlot) tmpPlot;
			AbstractRenderer renderer = (AbstractRenderer) plot.getRenderer();

			// Mouse pan (CTRL + drag)
			plot.setDomainPannable(true);
			plot.setRangePannable(true);

			// Legends
			// renderer.setLegendLine( ShapeUtilities.createDiamond(preferencesItemsScalingFactor * 10));
			LegendTitle legendTitle = chart.getLegend(0);


			// Series lines width and style
			// if(result.isMultipleLineStrokes()) {
			// this.applySeriesStrokes(result);
			// }
			// else {
			// for(int i = 0; i < plot.getSeriesCount(); i++) {
			// renderer.setSeriesStroke(i, new BasicStroke(seriesLineWidth));
			// }
			// }


			// Used for item selection mouse over
			if (renderer instanceof XYLineAndShapeRenderer) {
				((XYLineAndShapeRenderer) renderer).setUseOutlinePaint(true);
			}


			// Items colors
			// ArrayList<Color> palette = this.applySeriesPaint(result);

			//renderer.setDefaultOutlinePaint(Color.WHITE);

			// Grid
			// use plain stroke for charts using string symbol axis
			if (plot.getDomainAxis() instanceof SymbolAxis) {
				plot.setRangeGridlineStroke(new BasicStroke());
			}
			// plot.setDomainGridlinesVisible(false);

			// Legends
			if (legendTitle != null) {
				legendTitle.setFrame(BlockBorder.NONE);
				legendTitle.setPosition(RectangleEdge.RIGHT);
			}


			// XY bar chart
			if (renderer instanceof ItemSelectionXYBarRenderer) {
				// X axis label rotation
				plot.getDomainAxis().setVerticalTickLabels(true);

				plot.getDomainAxis().setTickLabelInsets(new RectangleInsets(10, 0, 0, 0));

				// Space between bar of same series
				((ItemSelectionXYBarRenderer) renderer).setMargin(0.3);

				// Do not trace the grid bands for symbol axes
				if (plot.getDomainAxis() instanceof SymbolAxis) {
					((SymbolAxis) plot.getDomainAxis()).setGridBandsVisible(false);
				}

				((ItemSelectionXYBarRenderer) renderer).setDefaultOutlinePaint(Color.RED);
				((ItemSelectionXYBarRenderer) renderer).setDrawBarOutline(true);

			}

			// XY spline chart
			else if (renderer instanceof ItemSelectionXYSplineRenderer) {
				// Markers label rotation
				((ItemSelectionXYSplineRenderer) renderer).setMarkerLabelRotationAngle(90);
			}
			// XY line chart
			else if (renderer instanceof ItemSelectionXYLineAndShapeRenderer) {
				// X axis label rotation
				plot.getDomainAxis().setVerticalTickLabels(true);
			}


			// Series shapes
			this.applySeriesShapes(plot);


			// Axis
			plot.getRangeAxis().setAxisLineVisible(false);
			plot.getRangeAxis().setTickMarksVisible(false);

			plot.getRangeAxis().setAxisLinePaint(Color.decode("#C0D0E0")); //$NON-NLS-1$
			plot.getRangeAxis().setTickMarkPaint(Color.decode("#C0D0E0")); //$NON-NLS-1$
			plot.getRangeAxis().setTickMarkOutsideLength(5);
			plot.getRangeAxis().setTickLabelFont(regularFont.deriveFont(Font.BOLD));


			// SJ: to fix a bug with unbreakable space thousands separator that is displayed as a white square glyph with some fonts (#3369)
			//			NumberAxis range = (NumberAxis) plot.getRangeAxis();
			//			DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance();
			//			DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
			//			
			//			if((int)symbols.getGroupingSeparator() == 8239) { //unbreakable space char code
			//				symbols.setGroupingSeparator(' '); //$NON-NLS-1$
			//				formatter.setDecimalFormatSymbols(symbols);
			//				range.setNumberFormatOverride(formatter);
			//			}


			plot.getDomainAxis().setAxisLinePaint(Color.decode("#C0D0E0")); //$NON-NLS-1$
			plot.getDomainAxis().setTickMarkPaint(Color.decode("#C0D0E0")); //$NON-NLS-1$
			plot.getDomainAxis().setTickMarkOutsideLength(5);
			plot.getDomainAxis().setTickLabelFont(regularFont.deriveFont(Font.BOLD));

			plot.setRangeZeroBaselineVisible(true);

			// Zero base lines colors
			plot.setDomainZeroBaselinePaint(Color.decode("#C0D0E0").darker()); //$NON-NLS-1$
			plot.setRangeZeroBaselinePaint(Color.decode("#C0D0E0").darker()); //$NON-NLS-1$


			// NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
			// domainAxis.setAutoRangeIncludesZero(false);
			// domainAxis.setTickMarkInsideLength(2.0f);
			// domainAxis.setTickMarkOutsideLength(0.0f);
			//
			// NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
			// rangeAxis.setTickMarkInsideLength(2.0f);
			// rangeAxis.setTickMarkOutsideLength(0.0f);


			// Legends
			if (legendTitle != null
			// && chart.getXYPlot().getRenderer() instanceof XYBarRenderer
			) {
				legendTitle.setFrame(new BlockRoundBorder(Color.LIGHT_GRAY));
				legendTitle.setPosition(RectangleEdge.RIGHT);
				legendTitle.setItemFont(this.regularFont.deriveFont(Font.BOLD));
				legendTitle.setMargin(10, 10, 10, 10);
			}
			// renderer.setBaseLegendShape(shape);


			// Legends : need to be called after the shape series items definition because it uses them
			// this.applyLegendDefaultSettings(legendTitle, (chart.getXYPlot().getRenderer() instanceof XYBarRenderer), palette);

		}
		// Category Bar chart / Category Line Chart
		else if (tmpPlot instanceof CategoryPlot) {

			CategoryPlot plot = (CategoryPlot) tmpPlot;

			CategoryItemRenderer renderer = (CategoryItemRenderer) chart.getCategoryPlot().getRenderer();

			plot.setRangePannable(true);


			// Grid
			plot.setRangeGridlineStroke(new BasicStroke());
			// plot.setDomainGridlinesVisible(false);

			// Zero base line
			plot.setRangeZeroBaselineVisible(true);
			plot.setRangeZeroBaselinePaint(Color.decode("#000000")); //$NON-NLS-1$


			// Axis
			plot.getRangeAxis().setAxisLineVisible(false);
			plot.getRangeAxis().setTickMarksVisible(false);
			plot.getRangeAxis().setTickLabelPaint(Color.decode("#666666")); //$NON-NLS-1$
			plot.getRangeAxis().setTickLabelFont(regularFont.deriveFont(Font.BOLD));


			// For drawing tick marks between categories and not in theirs middle
			plot.setDomainAxis(new CategoryAxisBetweenTicks(plot.getDomainAxis()));

			plot.getDomainAxis().setAxisLinePaint(Color.decode("#C0D0E0")); //$NON-NLS-1$
			plot.getDomainAxis().setTickMarkPaint(Color.decode("#C0D0E0")); //$NON-NLS-1$
			plot.getDomainAxis().setTickMarkOutsideLength(5);
			plot.getDomainAxis().setTickLabelFont(regularFont.deriveFont(Font.BOLD));
			// plot.getDomainAxis().setCategoryMargin(0.3); // TODO : incompatible with CategoryAxisBetween ? need to check again



			// Items colors
			// ArrayList<Color> palette = this.applySeriesPaint(plot, itemsColorsRenderingMode);


			// Category bar chart
			if (renderer instanceof ItemSelectionCategoryBarRenderer) {
				ItemSelectionCategoryBarRenderer barRenderer = (ItemSelectionCategoryBarRenderer) renderer;
				barRenderer.setDrawBarOutline(true);
				barRenderer.setMaximumBarWidth(0.1);

				barRenderer.setItemMargin(0.1);		// the space between bar of same category

				plot.getDomainAxis().setLowerMargin(0); // the space before the items in the axis
				plot.getDomainAxis().setUpperMargin(0); // the space after the items in the axis
				// plot.getDomainAxis().setMaximumCategoryLabelLines(5); // FIXME : see the compatibility with and without the scroll pane system for very large charts


				// Rotate domain axis labels to enhance readability
				// if(barRenderer.getChartType() == ChartsEngine.CHART_TYPE_PARTITION_DIMENSIONS) {
				// plot.getDomainAxis().setTickLabelFont(new Font("Arial", Font.BOLD, 12 + fontsPointsToAdd));
				plot.getDomainAxis().setCategoryLabelPositions(CategoryLabelPositions.UP_90);
				// }


				// Extended range axis with custom options
				// TODO
				// barRenderer.setNegativeValuesVisible(false);
				// barRenderer.setPositivesValuesVisible(true);
				// barRenderer.setMinimumVisibleValue(0);
				// barRenderer.setMaximumVisibleValue(DatasetUtilities.findMaximumRangeValue(chart.getCategoryPlot().getDataset()).doubleValue());

				// // Parts dimensions chart or Distribution bar chart
				// if(chartType == ChartsEngine.DISTRIBUTION_CHART || chartType == ChartsEngine.PARTS_DIMENSIONS_CHART) {
				// plot.setRangeAxis(new ExtendedNumberAxis((NumberAxis) plot.getRangeAxis(), false, true, 0,
				// DatasetUtilities.findMaximumRangeValue(chart.getCategoryPlot().getDataset()).doubleValue()));
				// }
				// // Specificities bar chart
				// else if(chartType == ChartsEngine.SPECIFICITIES_CHART) {
				// plot.setRangeAxis(new ExtendedNumberAxis((NumberAxis) plot.getRangeAxis(), true, true, DatasetUtilities.findMinimumRangeValue(chart.getCategoryPlot().getDataset()).doubleValue(),
				// DatasetUtilities.findMaximumRangeValue(chart.getCategoryPlot().getDataset()).doubleValue()));
				// }



				// TODO : test zoom seulement sur les valeurs positives de l'axe Y
				// ((NumberAxis) plot.getRangeAxis()).setAutoRangeMinimumSize(100);
				// ((NumberAxis) plot.getRangeAxis()).setRangeType(RangeType.POSITIVE);
				// ((NumberAxis) plot.getRangeAxis()).setAutoRangeStickyZero(true);
				// ((NumberAxis) plot.getRangeAxis()).setAutoRange(true);
				// ((NumberAxis) plot.getRangeAxis()).setFixedDimension(200);

			}
			// Category Line chart
			else {

				// Series lines width and style
				// if(result.isMultipleLineStrokes()) {
				// this.applySeriesStrokes(result);
				// }
				// else {
				// for(int i = 0; i < ((CategoryPlot)plot).getDataset().getRowCount(); i++) {
				// renderer.setSeriesStroke(i, new BasicStroke(seriesLineWidth));
				// }
				// }


				plot.setRangeZeroBaselineVisible(true);

				// TODO : tests
				// plot.getRangeAxis().setRange(0, 1);
				// plot.getRangeAxis().setUpperBound(100);


				// TODO : Rotate domain axis labels to enhance readability
				// plot.getDomainAxis().setTickLabelFont(new Font("Arial", Font.BOLD, 12 + fontsPointsToAdd));
				plot.getDomainAxis().setCategoryLabelPositions(CategoryLabelPositions.UP_90);


				// Series shapes
				this.applySeriesShapes(plot);

				// // Series shapes
				// // Diamond
				// Shape shape = ShapeUtilities.createDiamond(itemShapesScalingFactor * 3.4f);
				// renderer.setSeriesShape(1, shape);
				// float size = 6.4f * itemShapesScalingFactor;
				// renderer.setSeriesShape(0, new Ellipse2D.Float(-size / 2, -size / 2, size, size));
				//
				// // Triangle
				// shape = ShapeUtilities.createUpTriangle(itemShapesScalingFactor * 3.2f);
				// renderer.setSeriesShape(3, shape);

				// Used for item selection mouse over
				renderer.setDefaultOutlinePaint(Color.WHITE);


			}


			// Legends
			// renderer.setLegendLine( ShapeUtilities.createDiamond(preferencesItemsScalingFactor * 10));

			LegendTitle legendTitle = chart.getLegend(0);
			if (legendTitle != null) {
				legendTitle.setFrame(new BlockRoundBorder(Color.GRAY));
				legendTitle.setPosition(RectangleEdge.RIGHT);
				legendTitle.setMargin(10, 10, 10, 10);
			}



			// for(int i = 0; i < nbSeries; i++) {
			// // Bold font
			// renderer.setLegendTextFont(i, regularFont.deriveFont(Font.BOLD));
			// }

			// renderer.setShadowVisible( true );
			// renderer.setShadowXOffset( 2 );
			// renderer.setShadowYOffset( 0 );
			// renderer.setShadowPaint( Color.decode( "#C0C0C0"));


			// Legends : need to be called after the shape series items definition cause it uses them
			// this.applyLegendDefaultSettings(legendTitle, (chart.getCategoryPlot().getRenderer() instanceof BarRenderer), palette);



			// Category Line chart
			if (renderer instanceof LineAndShapeRenderer) {
				// need to be called after applyLegendDefaultSettings()
				((LineAndShapeRenderer) renderer).setUseOutlinePaint(true);
			}


		}
		// Other plot (eg. SpiderWebPlot)
		else {
			Plot plot = chart.getPlot();

			// Items colors
			// ArrayList<Color> palette = this.applySeriesPaint(plot, itemsColorsRenderingMode);


			// Series lines width and style
			// if(result.isMultipleLineStrokes()) {
			// this.applySeriesStrokes(result);
			// }

			this.applySeriesShapes(plot);

			LegendTitle legendTitle = chart.getLegend(0);
			if (legendTitle != null) {
				legendTitle.setFrame(new BlockRoundBorder(Color.GRAY));
				// legendTitle.setPosition(RectangleEdge.RIGHT);
			}

		}

	}



	@Override
	public XYLineAndShapeRenderer createXYLineAndShapeRenderer(ChartResult result, JFreeChart chart, boolean linesVisible, boolean shapesVisible) {
		return new ItemSelectionXYLineAndShapeRenderer(result, chart, linesVisible, shapesVisible);
	}


	@Override
	public XYStepRenderer createXYStepRenderer(boolean linesVisible, boolean shapesVisible) {
		return new ItemSelectionXYStepRenderer(linesVisible, shapesVisible);
	}

	@Override
	public CategoryStepRenderer createCategoryStepRenderer() {
		return new ItemSelectionCategoryStepRenderer();
	}


	@Override
	public XYSplineRenderer createXYSplineRenderer(boolean linesVisible, boolean shapesVisible) {
		ItemSelectionXYSplineRenderer renderer = new ItemSelectionXYSplineRenderer(this.splineRendererPrecision, linesVisible, shapesVisible);
		return renderer;
	}


	@Override
	public XYCardinalSplineRenderer createXYCardinalSplineRenderer(boolean linesVisible, boolean shapesVisible) {
		ItemSelectionXYCardinalSplineRenderer renderer = new ItemSelectionXYCardinalSplineRenderer(this.splineRendererPrecision, 0.3, linesVisible, shapesVisible);
		return renderer;
	}


	@Override
	public BarRenderer createCategoryBarRenderer() {
		return new ItemSelectionCategoryBarRenderer();
	}

	@Override
	public XYBarRenderer createXYBarRenderer() {
		return new ItemSelectionXYBarRenderer();
	}



	@Override
	public LineAndShapeRenderer createCategoryLineAndShapeRenderer(boolean linesVisible, boolean shapesVisible) {
		return new ItemSelectionCategoryLineAndShapeRenderer(linesVisible, shapesVisible);
	}

}
