package org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.CategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.CategoryItemRendererState;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.txm.chartsengine.core.messages.ChartsEngineCoreMessages;
import org.txm.chartsengine.jfreechart.core.renderers.MouseOverItemSelector;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.swing.CustomHTMLToolTip;

/**
 * Renderer providing item selection system and drawing features for selected item and custom rendering for category charts.
 * 
 * @author Sebastien Jacquot
 *
 */
public class ItemSelectionCategoryLineAndShapeRenderer extends LineAndShapeRenderer implements IRendererWithItemSelection {


	private static final long serialVersionUID = -240362060858062557L;

	/**
	 * Chart item selector.
	 */
	protected MouseOverItemSelector mouseOverItemSelector;



	/**
	 *
	 */
	public ItemSelectionCategoryLineAndShapeRenderer() {
		this(true, true);
	}


	/**
	 *
	 * @param linesVisible
	 * @param shapesVisible
	 */
	public ItemSelectionCategoryLineAndShapeRenderer(boolean linesVisible, boolean shapesVisible) {
		super(linesVisible, shapesVisible);
		this.mouseOverItemSelector = new MouseOverItemSelector(this);

		// Labels
		this.initItemLabelGenerator();

		// Tooltips
		this.initToolTipGenerator(this);
	}



	/**
	 * Initializes the item label generator.
	 */
	public void initItemLabelGenerator() {
		// TODO
	}

	/**
	 * Initializes the tool tip generator.
	 */
	public void initToolTipGenerator(final IRendererWithItemSelection renderer) {
		this.setDefaultToolTipGenerator(new CategoryToolTipGenerator() {

			@Override
			public String generateToolTip(CategoryDataset dataset, int row, int column) {

				// Hexadecimal color
				Color color = (Color) getSeriesPaint(row);
				String hex = "#" + Integer.toHexString(color.getRGB()).substring(2); //$NON-NLS-1$

				DefaultCategoryDataset catDataset = (DefaultCategoryDataset) dataset;
				return CustomHTMLToolTip.getDefaultHTMLBody(renderer, hex) + "<p>" + catDataset.getColumnKey(column) + "</p><p><span style=\"color: " + hex + ";\">" + catDataset.getRowKey(row) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						+ ChartsEngineCoreMessages.EMPTY + " </span><b>" + catDataset.getValue(row, column) + "</b></p></body><html>"; //$NON-NLS-1$ //$NON-NLS-2$
			}
		});
	}


	@Override
	public Paint getItemOutlinePaint(int series, int item) {
		// Change item shape outline color
		if (this.mouseOverItemSelector.isMouseOverItem(series, item)) {
			return super.getItemOutlinePaint(series, item);
		}
		// Do not trace not selected item outline
		return null;
	}

	@Override
	public Shape getItemShape(int series, int item) {

		Shape shape = super.getItemShape(series, item);

		// Scale item shape
		if (this.mouseOverItemSelector.isMouseOverItem(series, item)) {
			AffineTransform t = new AffineTransform();
			t.setToScale(1.9f, 1.9f);
			shape = t.createTransformedShape(shape);
		}
		return shape;
	}


	@Override
	public Font getItemLabelFont(int series, int item) {

		Font font = super.getItemLabelFont(series, item);

		// Change item label font size and style
		if (this.mouseOverItemSelector.isMouseOverItem(series, item)) {
			font = font.deriveFont(Font.BOLD, font.getSize() * 1.5f);
		}
		return font;
	}


	@Override
	public Stroke getSeriesStroke(int series) {
		BasicStroke stroke = (BasicStroke) super.getSeriesStroke(series);

		// Change line width of selected series
		if (this.mouseOverItemSelector.isMouseOverSeries(series)) {
			stroke = new BasicStroke(stroke.getLineWidth() * 1.8f);
		}
		return stroke;
	}

	/**
	 * @return the itemSelector
	 */
	public MouseOverItemSelector getItemsSelector() {
		return mouseOverItemSelector;
	}



	@Override
	public void updateDatasetForDrawingSelectedItemAsLast() {
		// FIXME : to remove when validated
		// this.itemSelector.setItemDrawnAtLast(this);

	}


	@Override
	public void drawItem(Graphics2D g2, CategoryItemRendererState state, Rectangle2D dataArea, CategoryPlot plot, CategoryAxis domainAxis,
			ValueAxis rangeAxis, CategoryDataset dataset, int row, int column, int pass) {

		// Manage the drawing as last of the selected plot item if exists
		if (this.mouseOverItemSelector.getMouseOverSeries() != -1) {

			// FIXME: this code bug, sometimes selected point is not display especially after a zoom, because the last index item is computed on the whole dataset but it should be the last item index
			// only of the visible current area, see the tips XY renderer to correct this
			int lastSeriesIndex = ((CategoryPlot) this.getPlot()).getDataset().getRowCount() - 1;

			// Draw the item of last series instead of the selected
			if (row == this.mouseOverItemSelector.getMouseOverSeries()) {
				row = lastSeriesIndex;
			}
			// Draw the item of the selected series instead of the last
			else if (row == lastSeriesIndex) {
				row = this.mouseOverItemSelector.getMouseOverSeries();
			}
		}
		super.drawItem(g2, state, dataArea, plot, domainAxis, rangeAxis, dataset, row, column, pass);
	}


	@Override
	public void init() {
		// TODO Auto-generated method stub

	}



}
