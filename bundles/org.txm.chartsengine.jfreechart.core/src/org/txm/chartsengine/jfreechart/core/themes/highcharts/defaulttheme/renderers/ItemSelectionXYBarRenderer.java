package org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers;

import java.awt.Color;
import java.awt.Paint;
import java.awt.Shape;

import org.jfree.chart.LegendItem;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.labels.XYSeriesLabelGenerator;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.ClusteredXYBarRenderer;
import org.jfree.data.xy.XYDataset;
import org.txm.chartsengine.core.messages.ChartsEngineCoreMessages;
import org.txm.chartsengine.jfreechart.core.renderers.MouseOverItemSelector;
import org.txm.chartsengine.jfreechart.core.renderers.MultipleItemsSelector;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.RendererUtils;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.swing.CustomHTMLToolTip;

/**
 * Renderer providing item selection system and drawing features for selected item and custom rendering for bar charts.
 * 
 * @author sjacquot
 *
 */
public class ItemSelectionXYBarRenderer extends ClusteredXYBarRenderer implements IRendererWithItemSelection {


	private static final long serialVersionUID = 3319544935579282435L;

	/**
	 * Chart item selector.
	 */
	protected MouseOverItemSelector itemsSelector;

	/**
	 *
	 */
	public ItemSelectionXYBarRenderer() {
		super();
		this.itemsSelector = new MultipleItemsSelector(this);

		// Labels
		this.initItemLabelGenerator();

		// Tooltips
		this.initToolTipGenerator(this);

	}


	/**
	 * Initializes the item label generator.
	 */
	public void initItemLabelGenerator() {
		// TODO
	}

	/**
	 * Initializes the tool tip generator.
	 */
	public void initToolTipGenerator(final IRendererWithItemSelection renderer) {
		this.setDefaultToolTipGenerator(new XYToolTipGenerator() {

			@Override
			public String generateToolTip(XYDataset dataset, int series, int item) {

				// Hexadecimal color
				Color color = (Color) getSeriesPaint(series);
				if (color != null) {
					String hex = "#" + Integer.toHexString(color.getRGB()).substring(2); //$NON-NLS-1$

					XYDataset xyDataset = (XYDataset) dataset;
					Number value = xyDataset.getY(series, item);


					String label;

					// Symbol Axis
					if (getPlot().getDomainAxis() instanceof SymbolAxis && item < ((SymbolAxis) getPlot().getDomainAxis()).getSymbols().length) {
						label = ((SymbolAxis) getPlot().getDomainAxis()).getSymbols()[item];
					}
					else {
						label = String.valueOf(xyDataset.getXValue(series, item));
					}

					return CustomHTMLToolTip.getDefaultHTMLBody(renderer, hex) + "<p>" + label + "</p><p><span style=\"color: " + hex + ";\">" + xyDataset.getSeriesKey(series) + "</span>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
							+ ChartsEngineCoreMessages.EMPTY + " <b>" + RendererUtils.valuesNumberFormat.format(value) + "</b></p></body><html>"; //$NON-NLS-1$ //$NON-NLS-2$
				}
				return ""; //$NON-NLS-1$
			}
		});
	}



	@Override
	public Paint getItemOutlinePaint(int row, int col) {
		// Multiple selection, draw item shape outline
		if (this.itemsSelector instanceof MultipleItemsSelector && ((MultipleItemsSelector) this.itemsSelector).isSelectedItem(row, col)) {
			return super.getItemOutlinePaint(row, col);
		}
		// Do not trace not selected item outline
		return null;
	}


	@Override
	public Paint getItemPaint(int row, int col) {

		Color color = (Color) super.getItemPaint(row, col);

		// Mouse over item
		if (this.itemsSelector.isMouseOverItem(row, col)) {
			color = (Color) RendererUtils.getMouseOverItemPaint(color);
		}
		// Multiple selection
		else if (this.itemsSelector instanceof MultipleItemsSelector && ((MultipleItemsSelector) this.itemsSelector).isSelectedItem(row, col)) {
			color = (Color) RendererUtils.getSelectedItemPaint(color);
		}
		return color;
	}


	@Override
	public LegendItem getLegendItem(int datasetIndex, int series) {
		XYPlot xyplot = getPlot();
		if (xyplot == null) {
			return null;
		}
		XYDataset dataset = xyplot.getDataset(datasetIndex);
		if (dataset == null) {
			return null;
		}
		LegendItem result;
		XYSeriesLabelGenerator lg = getLegendItemLabelGenerator();
		String label = lg.generateLabel(dataset, series);
		String description = label;
		String toolTipText = null;
		if (getLegendItemToolTipGenerator() != null) {
			toolTipText = getLegendItemToolTipGenerator().generateLabel(
					dataset, series);
		}
		String urlText = null;
		if (getLegendItemURLGenerator() != null) {
			urlText = getLegendItemURLGenerator().generateLabel(dataset,
					series);
		}
		Shape shape = this.getLegendBar();
		Paint paint = lookupSeriesPaint(series);
		result = new LegendItem(label, description, toolTipText, urlText, shape, paint);
		result.setLabelFont(lookupLegendTextFont(series));
		Paint labelPaint = lookupLegendTextPaint(series);
		if (labelPaint != null) {
			result.setLabelPaint(labelPaint);
		}
		result.setDataset(dataset);
		result.setDatasetIndex(datasetIndex);
		result.setSeriesKey(dataset.getSeriesKey(series));
		result.setSeriesIndex(series);
		if (getGradientPaintTransformer() != null) {
			result.setFillPaintTransformer(getGradientPaintTransformer());
		}
		return result;
	}

	/**
	 * @return the itemSelector
	 */
	public MouseOverItemSelector getItemsSelector() {
		return itemsSelector;
	}



	@Override
	public void updateDatasetForDrawingSelectedItemAsLast() {
		// Do nothing for bar renderer
	}


	@Override
	public void init() {
		// TODO Auto-generated method stub

	}



}
