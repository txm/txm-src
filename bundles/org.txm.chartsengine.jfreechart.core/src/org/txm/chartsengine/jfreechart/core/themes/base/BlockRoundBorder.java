package org.txm.chartsengine.jfreechart.core.themes.base;

import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

import org.jfree.chart.block.BlockBorder;

/**
 * Used to add a rounded border to legend.
 * 
 * @author Sebastien Jacquot
 *
 */
public class BlockRoundBorder extends BlockBorder {

	private static final long serialVersionUID = -267036227096041661L;

	/**
	 * 
	 * @param top
	 * @param left
	 * @param bottom
	 * @param right
	 * @param paint
	 */
	public BlockRoundBorder(double top, double left, double bottom, double right, Paint paint) {
		super(top, left, bottom, right, paint);
	}

	/**
	 * 
	 * @param paint
	 */
	public BlockRoundBorder(Paint paint) {
		this(1, 1, 1, 1, paint);
	}


	@Override
	public void draw(Graphics2D g2, Rectangle2D area) {
		RoundRectangle2D shape = new RoundRectangle2D.Double(area.getX(), area.getY(), area.getWidth(), area.getHeight(), 10, 10);
		g2.setPaint(this.getPaint());
		g2.draw(shape);
	}

}
