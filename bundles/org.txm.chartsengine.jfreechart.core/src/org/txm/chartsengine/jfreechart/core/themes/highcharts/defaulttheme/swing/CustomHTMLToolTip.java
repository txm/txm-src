package org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.swing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;

import javax.swing.BorderFactory;
import javax.swing.JToolTip;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.XYPlot;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;

/**
 * Custom tool tip with Highcharts default graphics theme.
 * 
 * @author sjacquot
 *
 */
public class CustomHTMLToolTip extends JToolTip {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1588391142786937703L;


	/**
	 * Chart.
	 */
	protected JFreeChart chart;


	protected boolean visible;

	/**
	 * Creates a custom tool tip linked to the specified chart.
	 * 
	 * @param chart
	 */
	public CustomHTMLToolTip(JFreeChart chart, boolean visible) {
		this.chart = chart;
		this.visible = visible;

		this.setOpaque(false);
		this.setBackground(new Color(255, 255, 255, 185)); // transparency
		this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10)); // margin
	}


	/**
	 * Returns the default HTML body string to use in the custom tool tips.
	 * This method is intended to be shared the header between multiple renderer implementations.
	 * 
	 * @return
	 */
	public static String getDefaultHTMLBody(IRendererWithItemSelection renderer, String hexBorderColor) {

		return "<html><body>"; //$NON-NLS-1$

		// FIXME: SJ: tests to put CSS styles in head and share them
		// return "<head>"
		// + "<style>"
		// + "body {border: solid " + hexBorderColor + " 4px; border-radius: 25px; overflow: auto; padding: 5px; margin: 0px;}"
		// + "</style>"
		// + "</head><html><body>";
	}

	@Override
	public void paintComponent(Graphics g) {
		if (this.visible) {

			// FIXME: for transparent tool tip version, to restore when the transparency bug will be fixed
			// Shape round = new RoundRectangle2D.Float(4, 4, this.getWidth() - 10, this.getHeight() - 10, 10, 10);

			Shape round = new RoundRectangle2D.Float(2, 2, this.getWidth() - 5, this.getHeight() - 5, 5, 5);

			Graphics2D g2 = (Graphics2D) g;
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

			// Background
			g2.setColor(this.getBackground());
			g2.fill(round);

			// Set the border color according to the renderer type
			Color color = Color.gray;
			// FIXME: transparency test
			// Color color = new Color(128, 128, 128, 185);


			// XY plots
			if (this.chart.getPlot() instanceof XYPlot) {
				XYPlot plot = this.chart.getXYPlot();
				// Get the current selected series color
				color = (Color) ((IRendererWithItemSelection) plot.getRenderer()).getSeriesPaint(((IRendererWithItemSelection) plot.getRenderer()).getItemsSelector().getMouseOverSeries());
			}
			// Category plots
			else if (this.chart.getPlot() instanceof CategoryPlot) {
				CategoryPlot plot = this.chart.getCategoryPlot();
				// Get the current selected series color
				color = (Color) ((IRendererWithItemSelection) plot.getRenderer()).getSeriesPaint(((IRendererWithItemSelection) plot.getRenderer()).getItemsSelector().getMouseOverSeries());
			}


			g2.setColor(color);

			g2.draw(round);
			// TODO : useless ?
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_DEFAULT);


			super.paintComponent(g);


			// // draw the text
			// String text = this.getComponent().getToolTipText();
			// if(text != null) {
			// FontMetrics fm = g2.getFontMetrics();
			// int h = fm.getAscent();
			// g2.setColor(Color.black);
			// g2.drawString(text, 10, (this.getHeight() + h) / 2);
			// }
		}

	}

}
