package org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Stroke;
import java.text.DecimalFormat;

import org.jfree.chart.labels.CategoryToolTipGenerator;
import org.jfree.chart.renderer.category.CategoryStepRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.txm.chartsengine.core.messages.ChartsEngineCoreMessages;
import org.txm.chartsengine.jfreechart.core.renderers.MouseOverItemSelector;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.swing.CustomHTMLToolTip;

public class ItemSelectionCategoryStepRenderer extends CategoryStepRenderer implements IRendererWithItemSelection {



	private static final long serialVersionUID = -6124688778407632159L;



	/**
	 * Chart item selector.
	 */
	protected MouseOverItemSelector mouseOverItemSelector;



	/**
	 * Values number format.
	 */
	protected DecimalFormat valuesNumberFormat;


	/**
	 *
	 */
	public ItemSelectionCategoryStepRenderer() {

		this.mouseOverItemSelector = new MouseOverItemSelector(this);

		// Default Values format
		String pattern = new String("#0.####"); //$NON-NLS-1$
		this.valuesNumberFormat = new DecimalFormat(pattern);

		// Labels
		this.initItemLabelGenerator();
		// Tooltips
		this.initToolTipGenerator(this);
	}


	/**
	 * Initializes the item label generator.
	 */
	public void initItemLabelGenerator() {
		// TODO
	}

	/**
	 * Initializes the tool tip generator.
	 */
	public void initToolTipGenerator(final IRendererWithItemSelection renderer) {
		this.setDefaultToolTipGenerator(new CategoryToolTipGenerator() {

			@Override
			public String generateToolTip(CategoryDataset dataset, int row, int column) {

				// Hexadecimal color
				Color color = (Color) getSeriesPaint(row);
				String hex = "#" + Integer.toHexString(color.getRGB()).substring(2); //$NON-NLS-1$

				DefaultCategoryDataset catDataset = (DefaultCategoryDataset) dataset;
				Number value = catDataset.getValue(row, column);

				return CustomHTMLToolTip.getDefaultHTMLBody(renderer, hex) + "<p>" + catDataset.getColumnKey(column) + "</p><p><span style=\"color: " + hex + ";\">" + catDataset.getRowKey(row) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						+ ChartsEngineCoreMessages.EMPTY
						+ " </span><b>" + valuesNumberFormat.format(value) + "</b></p></body><html>"; //$NON-NLS-1$ //$NON-NLS-2$
			}
		});
	}


	@Override
	public Paint getItemOutlinePaint(int series, int item) {
		// Trace the shape outline
		if (this.mouseOverItemSelector.isMouseOverItem(series, item)) {
			return super.getItemOutlinePaint(series, item);
		}
		// Do not trace not selected item outline
		return null;
	}


	@Override
	public Stroke getSeriesStroke(int series) {
		BasicStroke stroke = (BasicStroke) super.getSeriesStroke(series);

		// Change line width of selected series
		if (this.mouseOverItemSelector.isMouseOverSeries(series)) {
			stroke = new BasicStroke(stroke.getLineWidth() * 1.8f);
		}
		return stroke;
	}


	@Override
	public Font getItemLabelFont(int series, int item) {

		Font font = super.getItemLabelFont(series, item);

		// Change item label font size and style
		if (this.mouseOverItemSelector.isMouseOverItem(series, item)) {
			font = font.deriveFont(Font.BOLD, font.getSize() * 1.5f);
		}
		return font;
	}


	/**
	 * @return the itemSelector
	 */
	public MouseOverItemSelector getItemsSelector() {
		return mouseOverItemSelector;
	}



	@Override
	public void updateDatasetForDrawingSelectedItemAsLast() {
		// FIXME : to remove when validated
		// this.itemSelector.setItemDrawnAtLast(this);
	}


	@Override
	public void init() {
		// TODO Auto-generated method stub

	}



}
