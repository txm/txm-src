package org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.chart.text.TextUtils;
import org.jfree.chart.ui.GradientPaintTransformer;
import org.jfree.chart.ui.LengthAdjustmentType;
import org.jfree.chart.ui.RectangleAnchor;
import org.jfree.data.Range;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeriesCollection;
import org.txm.chartsengine.core.messages.ChartsEngineCoreMessages;
import org.txm.chartsengine.jfreechart.core.renderers.MouseOverItemSelector;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.swing.CustomHTMLToolTip;

/**
 * Renderer providing item selection system and drawing features for selected item and custom rendering for XY charts with splines drawing support.
 * 
 * @author sjacquot
 *
 */
public class ItemSelectionXYSplineRenderer extends XYSplineRenderer implements IRendererWithItemSelection {


	private static final long serialVersionUID = 4888753200588789887L;

	/**
	 * Chart item selector.
	 */
	protected MouseOverItemSelector mouseOverItemSelector;

	/**
	 * Marker label rotation angle.
	 */
	protected float markerLabelRotationAngle;


	/**
	 *
	 */
	public ItemSelectionXYSplineRenderer() {
		this(10, true, true);
	}


	/**
	 *
	 * @param precision the number of points between data items
	 * @param linesVisible
	 * @param shapesVisible
	 */
	public ItemSelectionXYSplineRenderer(int precision, boolean linesVisible, boolean shapesVisible) {
		super(precision);
		this.setDefaultLinesVisible(linesVisible);
		this.setDefaultShapesVisible(shapesVisible);
		this.mouseOverItemSelector = new MouseOverItemSelector(this);
		this.markerLabelRotationAngle = 0;


		// Labels
		this.initItemLabelGenerator();
		// Tooltips
		this.initToolTipGenerator(this);
	}



	/**
	 *
	 * @param precision the number of points between data items
	 */
	public ItemSelectionXYSplineRenderer(int precision) {
		this(precision, true, true);
	}


	/**
	 * Initializes the item label generator.
	 */
	public void initItemLabelGenerator() {
		// TODO
	}


	/**
	 * Initializes the tool tip generator.
	 */
	public void initToolTipGenerator(final IRendererWithItemSelection renderer) {
		this.setDefaultToolTipGenerator(new XYToolTipGenerator() {

			@Override
			public String generateToolTip(XYDataset dataset, int series, int item) {

				// Hexadecimal color
				Color color = (Color) getSeriesPaint(series);
				String hex = "#" + Integer.toHexString(color.getRGB()).substring(2); //$NON-NLS-1$

				XYSeriesCollection xyDataset = (XYSeriesCollection) dataset;
				return CustomHTMLToolTip.getDefaultHTMLBody(renderer, hex) + "<p><span style=\"color: " + hex + ";\">" + xyDataset.getSeriesKey(series) + "</span></p><p><b>" + xyDataset.getSeries( //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						series).getY(item)
						+ ChartsEngineCoreMessages.EMPTY + " </b><b>" + xyDataset.getSeries(series).getX(item) + "</b></p></body><html>"; //$NON-NLS-1$ //$NON-NLS-2$
			}
		});

	}



	@Override
	public void drawDomainMarker(Graphics2D g2, XYPlot plot, ValueAxis domainAxis, Marker marker, Rectangle2D dataArea) {

		// Overrides for adding rotated marker label functionality
		if (marker instanceof ValueMarker) {
			ValueMarker vm = (ValueMarker) marker;
			double value = vm.getValue();
			Range range = domainAxis.getRange();
			if (!range.contains(value)) {
				return;
			}

			double v = domainAxis.valueToJava2D(value, dataArea,
					plot.getDomainAxisEdge());

			PlotOrientation orientation = plot.getOrientation();
			Line2D line = null;
			if (orientation == PlotOrientation.HORIZONTAL) {
				line = new Line2D.Double(dataArea.getMinX(), v,
						dataArea.getMaxX(), v);
			}
			else if (orientation == PlotOrientation.VERTICAL) {
				line = new Line2D.Double(v, dataArea.getMinY(), v,
						dataArea.getMaxY());
			}
			else {
				throw new IllegalStateException();
			}

			final Composite originalComposite = g2.getComposite();
			g2.setComposite(AlphaComposite.getInstance(
					AlphaComposite.SRC_OVER, marker.getAlpha()));
			g2.setPaint(marker.getPaint());
			g2.setStroke(marker.getStroke());
			g2.draw(line);

			String label = marker.getLabel();
			RectangleAnchor anchor = marker.getLabelAnchor();
			if (label != null) {
				Font labelFont = marker.getLabelFont();
				g2.setFont(labelFont);
				g2.setPaint(marker.getLabelPaint());
				Point2D coordinates = calculateDomainMarkerTextAnchorPoint(
						g2, orientation, dataArea, line.getBounds2D(),
						marker.getLabelOffset(),
						LengthAdjustmentType.EXPAND, anchor);
				// TextUtilities.drawAlignedString(label, g2, (float) coordinates.getX(), (float) coordinates.getY(), marker.getLabelTextAnchor());
				// Draw rotated marker label
				TextUtils.drawRotatedString(label, g2, Math.toRadians(90), (float) coordinates.getX(), (float) coordinates.getY());
			}
			g2.setComposite(originalComposite);
		}
		else if (marker instanceof IntervalMarker) {
			IntervalMarker im = (IntervalMarker) marker;
			double start = im.getStartValue();
			double end = im.getEndValue();
			Range range = domainAxis.getRange();
			if (!(range.intersects(start, end))) {
				return;
			}

			double start2d = domainAxis.valueToJava2D(start, dataArea,
					plot.getDomainAxisEdge());
			double end2d = domainAxis.valueToJava2D(end, dataArea,
					plot.getDomainAxisEdge());
			double low = Math.min(start2d, end2d);
			double high = Math.max(start2d, end2d);

			PlotOrientation orientation = plot.getOrientation();
			Rectangle2D rect = null;
			if (orientation == PlotOrientation.HORIZONTAL) {
				// clip top and bottom bounds to data area
				low = Math.max(low, dataArea.getMinY());
				high = Math.min(high, dataArea.getMaxY());
				rect = new Rectangle2D.Double(dataArea.getMinX(),
						low, dataArea.getWidth(),
						high - low);
			}
			else if (orientation == PlotOrientation.VERTICAL) {
				// clip left and right bounds to data area
				low = Math.max(low, dataArea.getMinX());
				high = Math.min(high, dataArea.getMaxX());
				rect = new Rectangle2D.Double(low,
						dataArea.getMinY(), high - low,
						dataArea.getHeight());
			}

			final Composite originalComposite = g2.getComposite();
			g2.setComposite(AlphaComposite.getInstance(
					AlphaComposite.SRC_OVER, marker.getAlpha()));
			Paint p = marker.getPaint();
			if (p instanceof GradientPaint) {
				GradientPaint gp = (GradientPaint) p;
				GradientPaintTransformer t = im.getGradientPaintTransformer();
				if (t != null) {
					gp = t.transform(gp, rect);
				}
				g2.setPaint(gp);
			}
			else {
				g2.setPaint(p);
			}
			g2.fill(rect);

			// now draw the outlines, if visible...
			if (im.getOutlinePaint() != null && im.getOutlineStroke() != null) {
				if (orientation == PlotOrientation.VERTICAL) {
					Line2D line = new Line2D.Double();
					double y0 = dataArea.getMinY();
					double y1 = dataArea.getMaxY();
					g2.setPaint(im.getOutlinePaint());
					g2.setStroke(im.getOutlineStroke());
					if (range.contains(start)) {
						line.setLine(start2d, y0, start2d, y1);
						g2.draw(line);
					}
					if (range.contains(end)) {
						line.setLine(end2d, y0, end2d, y1);
						g2.draw(line);
					}
				}
				else { // PlotOrientation.HORIZONTAL
					Line2D line = new Line2D.Double();
					double x0 = dataArea.getMinX();
					double x1 = dataArea.getMaxX();
					g2.setPaint(im.getOutlinePaint());
					g2.setStroke(im.getOutlineStroke());
					if (range.contains(start)) {
						line.setLine(x0, start2d, x1, start2d);
						g2.draw(line);
					}
					if (range.contains(end)) {
						line.setLine(x0, end2d, x1, end2d);
						g2.draw(line);
					}
				}
			}

			String label = marker.getLabel();
			RectangleAnchor anchor = marker.getLabelAnchor();
			if (label != null) {
				Font labelFont = marker.getLabelFont();
				g2.setFont(labelFont);
				g2.setPaint(marker.getLabelPaint());
				Point2D coordinates = calculateDomainMarkerTextAnchorPoint(
						g2, orientation, dataArea, rect,
						marker.getLabelOffset(), marker.getLabelOffsetType(),
						anchor);
				// TextUtilities.drawAlignedString(label, g2, (float) coordinates.getX(), (float) coordinates.getY(), marker.getLabelTextAnchor());
				// Draw rotated marker label
				TextUtils.drawRotatedString(label, g2, Math.toRadians(90), (float) coordinates.getX(), (float) coordinates.getY());
			}
			g2.setComposite(originalComposite);

		}

	}


	@Override
	public Paint getItemOutlinePaint(int series, int item) {
		// Change item shape outline color
		if (this.mouseOverItemSelector.isMouseOverItem(series, item)) {
			return super.getItemOutlinePaint(series, item);
		}
		// Do not trace not selected item outline
		return null;
	}

	@Override
	public Shape getItemShape(int series, int item) {

		Shape s = super.getItemShape(series, item);

		// Scale item shape
		if (this.mouseOverItemSelector.isMouseOverItem(series, item)) {
			AffineTransform t = new AffineTransform();
			t.setToScale(1.9f, 1.9f);
			s = t.createTransformedShape(s);
		}
		return s;
	}

	@Override
	public Stroke getSeriesStroke(int series) {
		BasicStroke stroke = (BasicStroke) super.getSeriesStroke(series);

		// Change line width of selected series
		if (this.mouseOverItemSelector.isMouseOverSeries(series)) {
			stroke = new BasicStroke(stroke.getLineWidth() * 1.8f);
		}
		return stroke;
	}

	@Override
	public Paint getItemLabelPaint(int series, int item) {
		// Change item label font color
		if (this.mouseOverItemSelector.isMouseOverItem(series, item)) {
			return Color.orange;
		}
		return super.getItemLabelPaint(series, item);
	}

	@Override
	public Font getItemLabelFont(int series, int item) {

		Font font = super.getItemLabelFont(series, item);

		// Change item label font size and style
		if (this.mouseOverItemSelector.isMouseOverItem(series, item)) {
			font = font.deriveFont(Font.BOLD, font.getSize() * 1.5f);
		}
		return font;
	}


	/**
	 * @return the itemSelector
	 */
	public MouseOverItemSelector getItemsSelector() {
		return mouseOverItemSelector;
	}


	@Override
	public void updateDatasetForDrawingSelectedItemAsLast() {
		// FIXME : to remove when validated
		// this.itemSelector.setItemDrawnAtLast(this);
	}


	/**
	 * @param markerLabelRotationAngle the markerLabelRotationAngle to set
	 */
	public void setMarkerLabelRotationAngle(float markerLabelRotationAngle) {
		this.markerLabelRotationAngle = markerLabelRotationAngle;
	}



	@Override
	public void init() {
		// TODO Auto-generated method stub

	}


}
