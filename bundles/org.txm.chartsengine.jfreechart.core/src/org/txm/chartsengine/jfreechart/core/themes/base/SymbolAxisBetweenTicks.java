package org.txm.chartsengine.jfreechart.core.themes.base;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.List;

import org.jfree.chart.axis.AxisState;
import org.jfree.chart.axis.NumberTick;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.axis.Tick;
import org.jfree.chart.axis.TickType;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.axis.ValueTick;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.text.G2TextMeasurer;
import org.jfree.chart.text.TextBlock;
import org.jfree.chart.text.TextLine;
import org.jfree.chart.text.TextUtils;
import org.jfree.chart.ui.RectangleEdge;
import org.jfree.chart.ui.TextAnchor;
import org.jfree.data.general.DatasetUtils;

/**
 * Class providing a way to draw tick marks of series axis between the series items rather than on theirs middle.
 * 
 * @author sjacquot
 *
 */
public class SymbolAxisBetweenTicks extends SymbolAxis {



	/**
	 * Creates an axis.
	 * 
	 * @param label
	 * @param symbols
	 */
	public SymbolAxisBetweenTicks(String label, String[] symbols) {
		super(label, symbols);
	}

	/**
	 * Creates an axis and copy data from the specified <code>SymbolAxis</code>.
	 * 
	 * @param axis
	 */
	public SymbolAxisBetweenTicks(SymbolAxis axis) {
		this(axis.getLabel(), axis.getSymbols());

		this.setLabelFont(axis.getLabelFont());
		this.setLabelPaint(axis.getLabelPaint());
		this.setTickLabelPaint(axis.getTickLabelPaint());
		this.setAxisLinePaint(axis.getAxisLinePaint());
		this.setTickMarkPaint(axis.getTickMarkPaint());
		this.setTickMarkOutsideLength(axis.getTickMarkOutsideLength());
		this.setTickLabelFont(axis.getTickLabelFont());

		// TODO : add other value copies

	}


	/**
	 * Draws ticks marks between series items.
	 */
	@Override
	protected AxisState drawTickMarksAndLabels(Graphics2D g2,
			double cursor, Rectangle2D plotArea, Rectangle2D dataArea,
			RectangleEdge edge) {

		AxisState state = new AxisState(cursor);

		if (isAxisLineVisible()) {
			drawAxisLine(g2, cursor, dataArea, edge);
		}

		List ticks = refreshTicks(g2, state, dataArea, edge);
		state.setTicks(ticks);
		g2.setFont(getTickLabelFont());
		Iterator iterator = ticks.iterator();


		ValueTick lastTick = null;

		while (iterator.hasNext()) {

			ValueTick tick = (ValueTick) iterator.next();

			// Skip the ticks out of range
			if (tick.getValue() < DatasetUtils.findMinimumDomainValue(((XYPlot) this.getPlot()).getDataset()).doubleValue()
					|| tick.getValue() > DatasetUtils.findMaximumDomainValue(((XYPlot) this.getPlot()).getDataset()).doubleValue()) {
				continue;
			}


			if (isTickLabelsVisible()) {
				g2.setPaint(getTickLabelPaint());
				float[] anchorPoint = calculateAnchorPoint(tick, cursor,
						dataArea, edge);
				TextUtils.drawRotatedString(tick.getText(), g2,
						anchorPoint[0], anchorPoint[1], tick.getTextAnchor(),
						tick.getAngle(), tick.getRotationAnchor());
			}

			if ((isTickMarksVisible() && tick.getTickType().equals(
					TickType.MAJOR)) || (isMinorTickMarksVisible()
							&& tick.getTickType().equals(TickType.MINOR))) {

				double ol = (tick.getTickType().equals(TickType.MINOR))
						? getMinorTickMarkOutsideLength()
						: getTickMarkOutsideLength();

				double il = (tick.getTickType().equals(TickType.MINOR))
						? getMinorTickMarkInsideLength()
						: getTickMarkInsideLength();


				// Move the ticks X position
				float xx;
				if (lastTick != null) {
					// Skip outside tick
					if ((tick.getValue() + (tick.getValue() - lastTick.getValue()) / 2) > this.getUpperBound()) {
						continue;
					}

					xx = (float) valueToJava2D(tick.getValue() + (tick.getValue() - lastTick.getValue()) / 2, dataArea, edge);
				}
				// First tick
				else {
					xx = (float) valueToJava2D(tick.getValue() + 0.5, dataArea, edge);
				}

				Line2D mark = null;
				g2.setStroke(getTickMarkStroke());
				g2.setPaint(getTickMarkPaint());
				if (edge == RectangleEdge.LEFT) {
					mark = new Line2D.Double(cursor - ol, xx, cursor + il, xx);
				}
				else if (edge == RectangleEdge.RIGHT) {
					mark = new Line2D.Double(cursor + ol, xx, cursor - il, xx);
				}
				else if (edge == RectangleEdge.TOP) {
					mark = new Line2D.Double(xx, cursor - ol, xx, cursor + il);
				}
				else if (edge == RectangleEdge.BOTTOM) {
					mark = new Line2D.Double(xx, cursor + ol, xx, cursor - il);
				}
				g2.draw(mark);

				// Draw first tick
				if (lastTick == null && (tick.getValue() - 0.5) >= this.getLowerBound()) {
					xx = (float) valueToJava2D((tick.getValue() - 0.5), dataArea, edge);
					mark = new Line2D.Double(xx, cursor + ol, xx, cursor - il);
					g2.draw(mark);
				}

				lastTick = tick;

			}
		}

		double used = 0.0;
		if (isTickLabelsVisible()) {
			if (edge == RectangleEdge.LEFT) {
				used += findMaximumTickLabelWidth(ticks, g2, plotArea, isVerticalTickLabels());
				state.cursorLeft(used);
			}
			else if (edge == RectangleEdge.RIGHT) {
				used = findMaximumTickLabelWidth(ticks, g2, plotArea, isVerticalTickLabels());
				state.cursorRight(used);
			}
			else if (edge == RectangleEdge.TOP) {
				used = findMaximumTickLabelHeight(ticks, g2, plotArea, isVerticalTickLabels());
				state.cursorUp(used);
			}
			else if (edge == RectangleEdge.BOTTOM) {
				used = findMaximumTickLabelHeight(ticks, g2, plotArea, isVerticalTickLabels());
				state.cursorDown(used);
			}
		}

		return state;
	}



	@Override
	protected List<Tick> refreshTicksHorizontal(Graphics2D g2, Rectangle2D dataArea, RectangleEdge edge) {

		List<Tick> ticks = new java.util.ArrayList<>();

		Font tickLabelFont = getTickLabelFont();
		g2.setFont(tickLabelFont);

		double size = getTickUnit().getSize();
		int count = calculateVisibleTickCount();
		double lowestTickValue = calculateLowestVisibleTickValue();

		double previousDrawnTickLabelPos = 0.0;
		double previousDrawnTickLabelLength = 0.0;

		if (count <= ValueAxis.MAXIMUM_TICK_COUNT) {
			for (int i = 0; i < count; i++) {
				double currentTickValue = lowestTickValue + (i * size);
				double xx = valueToJava2D(currentTickValue, dataArea, edge);
				String tickLabel;
				NumberFormat formatter = getNumberFormatOverride();
				if (formatter != null) {
					tickLabel = formatter.format(currentTickValue);
				}
				else {
					tickLabel = valueToString(currentTickValue);
				}
				if (tickLabel == null) {
					tickLabel = ""; //$NON-NLS-1$
				}

				// avoid to draw overlapping tick labels
				Rectangle2D bounds = TextUtils.getTextBounds(tickLabel, g2,
						g2.getFontMetrics());
				double tickLabelLength = isVerticalTickLabels()
						? bounds.getHeight()
						: bounds.getWidth();
				boolean tickLabelsOverlapping = false;
				if (i > 0) {
					double avgTickLabelLength = (previousDrawnTickLabelLength
							+ tickLabelLength) / 2.0;
					if (Math.abs(xx - previousDrawnTickLabelPos) < avgTickLabelLength) {
						tickLabelsOverlapping = true;
					}
				}
				if (tickLabelsOverlapping) {
					tickLabel = ""; // don't draw this tick label //$NON-NLS-1$
				}
				else {
					// remember these values for next comparison
					previousDrawnTickLabelPos = xx;
					previousDrawnTickLabelLength = tickLabelLength;
				}

				TextAnchor anchor;
				TextAnchor rotationAnchor;
				double angle = 0.0;
				if (isVerticalTickLabels()) {
					anchor = TextAnchor.CENTER_RIGHT;
					rotationAnchor = TextAnchor.CENTER_RIGHT;
					if (edge == RectangleEdge.TOP) {
						angle = Math.PI / 2.0;
					}
					else {
						angle = -Math.PI / 2.0;
					}
				}
				else {
					if (edge == RectangleEdge.TOP) {
						anchor = TextAnchor.BOTTOM_CENTER;
						rotationAnchor = TextAnchor.BOTTOM_CENTER;
					}
					else {
						anchor = TextAnchor.TOP_CENTER;
						rotationAnchor = TextAnchor.TOP_CENTER;
					}
				}
				if (tickLabel != "") { //$NON-NLS-1$
					float l;
					if (RectangleEdge.isLeftOrRight(edge)) {
						l = (float) dataArea.getWidth();
					}
					else {
						l = (float) dataArea.getHeight();
					}


					l += 1000;

					float ratio = 0.1f;

					TextBlock label = TextUtils.createTextBlock(tickLabel, getTickLabelFont(), getTickLabelPaint(), l * ratio, 1, new G2TextMeasurer(g2));

					Tick tick = new NumberTick(new Double(currentTickValue), ((TextLine) label.getLines().get(0)).getLastTextFragment().getText(), anchor, rotationAnchor, angle);
					ticks.add(tick);
				}
			}
		}
		return ticks;

	}

}
