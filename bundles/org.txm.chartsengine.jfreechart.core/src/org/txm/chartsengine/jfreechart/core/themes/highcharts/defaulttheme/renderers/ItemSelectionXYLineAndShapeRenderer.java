package org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.text.DecimalFormat;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeriesCollection;
import org.txm.chartsengine.core.messages.ChartsEngineCoreMessages;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.jfreechart.core.renderers.MouseOverItemSelector;
import org.txm.chartsengine.jfreechart.core.renderers.MultipleItemsSelector;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.swing.CustomHTMLToolTip;

/**
 * Renderer providing item selection system and drawing features for selected item and custom rendering for XY charts.
 *
 * @author sjacquot
 *
 */
public class ItemSelectionXYLineAndShapeRenderer extends XYLineAndShapeRenderer implements IRendererWithItemSelection {


	private static final long serialVersionUID = -32079335535308863L;


	/**
	 * Chart items selector.
	 */
	protected MultipleItemsSelector multipleItemsSelector;


	/**
	 * The linked chart.
	 */
	protected JFreeChart chart;


	/**
	 * Values number format.
	 */
	protected DecimalFormat valuesNumberFormat;


	/**
	 * Creates a renderer with visible lines and visible shapes.
	 */
	public ItemSelectionXYLineAndShapeRenderer(ChartResult result, JFreeChart chart) {
		this(result, chart, true, true);
	}


	/**
	 * Creates a renderer.
	 *
	 * @param linesVisible
	 * @param shapesVisible
	 */
	public ItemSelectionXYLineAndShapeRenderer(ChartResult result, JFreeChart chart, boolean linesVisible, boolean shapesVisible) {
		super(linesVisible, shapesVisible);

		this.multipleItemsSelector = new MultipleItemsSelector(this);
		this.multipleItemsSelector.setResult(result);

		// // FIXME: SJ: test to recycle the existing chart panel to fix a null pointer exception in the renderer
		// if(result.getChart() != null) {
		// JFreeChart existingChart = (JFreeChart) result.getChart();
		// if(existingChart.getPlot() instanceof XYPlot && existingChart.getXYPlot().getRenderer() instanceof IRendererWithItemSelection) {
		// this.multipleItemsSelector.setChartPanel(((IRendererWithItemSelection) existingChart.getXYPlot().getRenderer()).getItemsSelector().getChartPanel());
		// }
		// else if(existingChart.getPlot() instanceof CategoryPlot && existingChart.getCategoryPlot().getRenderer() instanceof IRendererWithItemSelection) {
		// this.multipleItemsSelector.setChartPanel(((IRendererWithItemSelection) existingChart.getCategoryPlot().getRenderer()).getItemsSelector().getChartPanel());
		// }
		// }

		this.chart = chart;

		// Default Values format
		String pattern = new String("#0.####"); //$NON-NLS-1$
		this.valuesNumberFormat = new DecimalFormat(pattern);


		this.init();

		// Labels
		this.initItemLabelGenerator();
		// Tooltips
		this.initToolTipGenerator(this);


	}


	@Override
	public void init() {
		// TODO Auto-generated method stub
	}

	/**
	 * Initializes the item label generator.
	 */
	public void initItemLabelGenerator() {
		// TODO
	}

	/**
	 * Initializes the tool tip generator.
	 */
	public void initToolTipGenerator(final IRendererWithItemSelection renderer) {
		this.setDefaultToolTipGenerator(new XYToolTipGenerator() {

			@Override
			public String generateToolTip(XYDataset dataset, int series, int item) {

				// Hexadecimal color
				Color color = (Color) getSeriesPaint(series);
				String hex = "#" + Integer.toHexString(color.getRGB()).substring(2); //$NON-NLS-1$

				XYSeriesCollection xyDataset = (XYSeriesCollection) dataset;


				String label;

				try {
					// Symbol Axis
					if (getPlot().getDomainAxis() instanceof SymbolAxis) {
						label = ((SymbolAxis) getPlot().getDomainAxis()).getSymbols()[item];
					}
					else {
						label = String.valueOf(xyDataset.getXValue(series, item));
					}
				}
				catch (Exception e) {
					label = "Error"; //$NON-NLS-1$
				}


				return CustomHTMLToolTip.getDefaultHTMLBody(renderer, hex) + "<p><span style=\"color: " + hex + ";\">" + label + "</span></p><p>" + xyDataset.getSeriesKey(series) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						+ ChartsEngineCoreMessages.EMPTY + " <b>" + valuesNumberFormat.format(xyDataset.getSeries(series).getY(item)) + "</b></p></body><html>"; //$NON-NLS-1$ //$NON-NLS-2$
			}
		});
	}


	@Override
	public Paint getItemOutlinePaint(int series, int item) {
		// Trace the shape outline for unique selected item
		if (this.multipleItemsSelector.isMouseOverItem(series, item) || this.multipleItemsSelector.isSelectedItem(series, item)) {
			return super.getItemOutlinePaint(series, item);
		}
		// FIXME: SJ: rendering tests
		// // Trace the shape outline for multiple selected item
		// else if(this.itemSelector.isMultipleSelectedItem(series, item)) {
		// return super.getItemPaint(series, item);
		// }
		// Do not trace not selected item outline
		return null;
	}

	@Override
	public Shape getItemShape(int series, int item) {


		Shape shape = super.getItemShape(series, item);

		// FIXME: SJ:
		// int lastItemIndex = ((XYPlot)this.getPlot()).getDataset().getItemCount(series) - 1;
		//
		// // Store the selected shape to draw it at last if it's not already the last
		// if(item != lastItemIndex && this.itemSelector.isSelectedItem(series, item)) {
		// this.selectedShape = shape;
		// return super.getItemShape(series, lastItemIndex); // return the last shape instead of the selected
		// }
		//
		// // Draw the selected item instead of the last
		// if(item == lastItemIndex && this.selectedShape != null) {
		// // Scale item shape
		// AffineTransform t = new AffineTransform();
		// t.setToScale(1.9f, 1.9f);
		// Shape selectedShape = t.createTransformedShape(this.selectedShape);
		// this.selectedShape = null;
		// return selectedShape;
		// }

		// Scale item shape
		if (this.multipleItemsSelector.isMouseOverItem(series, item) || this.multipleItemsSelector.isSelectedItem(series, item)) {
			AffineTransform t = new AffineTransform();
			t.setToScale(1.9f, 1.9f);
			shape = t.createTransformedShape(shape);
		}

		return shape;
	}



	@Override
	public Font getItemLabelFont(int series, int item) {

		Font font = super.getItemLabelFont(series, item);

		// Change item label font size and style for mouse over or selected item
		double increaseValue = 0;
		if(this.multipleItemsSelector.isSizeIncreaseEnabled()) {
			increaseValue = this.multipleItemsSelector.getLabelSizeIncreaseValue();
		}
		if (this.multipleItemsSelector.isMouseOverItem(series, item) || this.multipleItemsSelector.isSelectedItem(series, item)) {
			int style;
			if(font.getStyle() == (Font.BOLD + Font.ITALIC) || font.getStyle() == Font.ITALIC) {
				style = (Font.BOLD + Font.ITALIC);
			}
			else {
				style = Font.BOLD;
			}
			font = font.deriveFont(style, (float) (font.getSize() + increaseValue));
		}

		return font;
	}



	@Override
	public Stroke getSeriesStroke(int series) {
		BasicStroke stroke = (BasicStroke) super.getSeriesStroke(series);

		// Change line width of selected series
		if (this.multipleItemsSelector.isMouseOverSeries(series)) {
			stroke = new BasicStroke(stroke.getLineWidth() * 1.8f);
		}
		return stroke;
	}


	/**
	 * @return the itemSelector
	 */
	public MultipleItemsSelector getItemsSelector() {
		return multipleItemsSelector;
	}



	// FIXME: SJ:
	// @Override
	// public XYItemRendererState initialise(Graphics2D g2, Rectangle2D dataArea,
	// XYPlot plot, XYDataset data, PlotRenderingInfo info) {
	//
	// final Graphics2D fG2 = g2;
	// final Rectangle2D fDataArea = dataArea;
	// final XYPlot fPlot = plot;
	// final XYDataset fData = data;
	//
	// return new State(info) {
	// @Override
	// public void endSeriesPass(XYDataset dataset, int series, int firstItem, int lastItem, int pass, int passCount) {
	// // TODO Auto-generated method stub
	// super.endSeriesPass(dataset, series, firstItem, lastItem, pass, passCount);
	//
	//
	// // FIXME: draw all passes of all previously skipped selected items
	// for(series = 0; series < ((MultipleItemSelector)itemSelector).getSelectedSeriesCount(); series++) {
	// int[] selectedItems = ((MultipleItemSelector)itemSelector).getSelectedItems(series);
	// for(int i = 0; i < selectedItems.length; i++) {
	// for(pass = 0; pass < passCount - 1; pass++) {
	// System.out.println("ItemSelectionXYLineAndShapeRenderer.drawItem(): draw skipped pass " + pass + " for selected item " + selectedItems[i]);
	// super.drawItem(fG2, ((XYLineAndShapeRenderer.State)this), fDataArea, this.getInfo(), fPlot, fPlot.getDomainAxis(), fPlot.getRangeAxis(), dataset, series, selectedItems[i], null, pass);
	// }
	// }
	// }
	//
	// }
	// };
	// }


	/**
	 * Returns <code>true</code> if the specified pass is the one for drawing the selected items that have been pushed to draw as last elements.
	 *
	 * @param pass the pass.
	 *
	 * @return A boolean.
	 */
	protected boolean isSelectedItemPass(int pass) {
		return pass == 2;
	}


	@Override
	public int getPassCount() {
		return 3;
	}


	@Override
	public void updateDatasetForDrawingSelectedItemAsLast() {
		// TODO Auto-generated method stub

	}

	@Override
	public void drawItem(Graphics2D g2, XYItemRendererState state,
			Rectangle2D dataArea, PlotRenderingInfo info, XYPlot plot,
			ValueAxis domainAxis, ValueAxis rangeAxis, XYDataset dataset,
			int series, int item, CrosshairState crosshairState, int pass) {


		// Mouse over selected item or multiple selected items
		if (this.multipleItemsSelector.isMouseOverItem(series, item) || this.multipleItemsSelector.isSelectedItem(series, item)) {
			// First pass, push the selected item and skip the drawing
			if (this.isLinePass(pass)) {

				// Mouse over
				if (this.multipleItemsSelector.isMouseOverItem(series, item)) {
					this.multipleItemsSelector.pushItemToDrawAsLast(series, item, true);
				}
				// Multiple selection
				else if (this.multipleItemsSelector.isSelectedItem(series, item)) {
					this.multipleItemsSelector.pushItemToDrawAsLast(series, item, false);
				}
				return;
			}
			// Second pass, skip the drawing
			else if (this.isItemPass(pass)) {
				return;
			}
			// Third pass, pop the selected item and draw all the previously skipped passes
			else if (this.isSelectedItemPass(pass)) {

				item = this.multipleItemsSelector.popItemToDrawAsLast(series);
				// draw all skipped passes of the selected item
				for (int i = 0; i < this.getPassCount() - 1; i++) {
					if (series > -1 && item > -1) {
						super.drawItem(g2, state, dataArea, info, plot, domainAxis, rangeAxis, dataset, series, item, crosshairState, i);
					}
				}

				// FIXME: SJ: draw all passes of all previously skipped selected items
				// for(series = 0; series < ((MultipleItemSelector)this.itemSelector).getSelectedSeriesCount(); series++) {
				// int[] selectedItems = ((MultipleItemSelector)this.itemSelector).getSelectedItems(series);
				// for(int i = 0; i < selectedItems.length; i++) {
				// for(pass = 0; pass < this.getPassCount() - 1; pass++) {
				// System.out.println("ItemSelectionXYLineAndShapeRenderer.drawItem(): draw skipped pass " + pass + " for selected item " + item);
				// super.drawItem(g2, state, dataArea, info, plot, domainAxis, rangeAxis, dataset, series, selectedItems[i], crosshairState, pass);
				// }
				// }
				// }



			}

		}
		// Normal item
		else {
			super.drawItem(g2, state, dataArea, info, plot, domainAxis, rangeAxis, dataset, series, item, crosshairState, pass);
		}

	}

	/**
	 * Gets the linked chart result.
	 *
	 * @return the linked chart result
	 */
	public ChartResult getResult() {
		return this.multipleItemsSelector.getResult();
	}

}
