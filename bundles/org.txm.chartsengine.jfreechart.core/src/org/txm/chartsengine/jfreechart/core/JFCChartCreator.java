package org.txm.chartsengine.jfreechart.core;

import java.awt.Color;
import java.util.ArrayList;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.XYPlot;
import org.txm.chartsengine.core.ChartCreator;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.utils.logger.Log;


/**
 * JFreeChart base chart creator.
 * 
 * All JFC charts creators should extend this class.
 * The updateChart(ChartResult result) method implementation of the subclasses must call super.update(result) to benefit to the shared system.
 * Before updating, the subclasses should call chart.setNotify(false) to avoid conflict between the updating and the rendering threads.
 * 
 * @author sjacquot
 * @author mdecorde
 *
 */
public abstract class JFCChartCreator<R extends ChartResult> extends ChartCreator<JFCChartsEngine, R> {


	@Override
	public abstract JFreeChart createChart(R result);

	@Override
	public void updateChart(R result) {
		this.updateChart(result, true);
	}

	/**
	 * Updates the chart according to the shared parameters, also applies the current theme to the chart and the user renderer settings.
	 * 
	 * @param result
	 * @param applyTheme
	 */
	public void updateChart(R result, boolean applyTheme) {

		// Java object
		if (result.getChart() instanceof JFreeChart) {
			JFreeChart chart = (JFreeChart) result.getChart();

			// freeze rendering
			chart.setNotify(false);

			// applying full theme
			if (applyTheme) {
				this.getChartsEngine().getJFCTheme().applyThemeToChart(result);
			}


			// rendering color mode
			this.getChartsEngine().getJFCTheme().applySeriesPaint(result);

			// multiple line strokes
			this.getChartsEngine().getJFCTheme().applySeriesStrokes(result);

			
			// title visibility
			if (chart.getTitle() != null) { // TODO restore the  && result.hasParameterChanged(ChartsEnginePreferences.SHOW_TITLE) is necessary
				chart.getTitle().setVisible(result.isTitleVisible());
			}
			// subtitle visibility
			if (chart.getSubtitleCount() > 0) { // TODO restore the  && result.hasParameterChanged(ChartsEnginePreferences.SHOW_TITLE) is necessary
				chart.getSubtitle(0).setVisible(result.isTitleVisible());
			}


			// legend visibility
			if (chart.getLegend() != null) { // TODO restore the  && result.hasParameterChanged(ChartsEnginePreferences.SHOW_LEGEND) is necessary
				chart.getLegend().setVisible(result.isLegendVisible());
			}
			// CategoryPlot
			if (chart.getPlot() instanceof CategoryPlot) {
				CategoryPlot plot = (CategoryPlot) chart.getPlot();
				// grid visibility
				plot.setDomainGridlinesVisible(result.isGridVisible() && result.isDomainGridLinesVisible());
				plot.setRangeGridlinesVisible(result.isGridVisible());

				// force the renderer initialization to override theme settings
				if (plot.getRenderer() instanceof IRendererWithItemSelection) {
					((IRendererWithItemSelection) plot.getRenderer()).init();
				}
			}
			// XYPlot
			else if (chart.getPlot() instanceof XYPlot) {
				XYPlot plot = (XYPlot) chart.getPlot();
				// grid visibility
				plot.setDomainGridlinesVisible(result.isGridVisible() && result.isDomainGridLinesVisible());
				plot.setRangeGridlinesVisible(result.isGridVisible());

				// force the renderer initialization to override theme settings
				if (plot.getRenderer() instanceof IRendererWithItemSelection) {
					((IRendererWithItemSelection) plot.getRenderer()).init();
				}
			}

			chart.setNotify(true);
		}
		else {
			Log.severe("Error: JFCChartCreator only manage JFC charts: " + result.getChart());
		}
		// // File
		// else if (result.getChart() instanceof File) {
		// // creates a new chart but using the same file
		// this.createChartFile(result, (File) result.getChart());
		// }
		// else { // FIXME hack to set the chart File
		// result.setChart(this.createChartFile(result));
		// }
	}


	@Override
	public ArrayList<Color> getSeriesShapesColors(Object chart) {
		ArrayList<Color> colors = new ArrayList<>();
		JFreeChart c = (JFreeChart) chart;

		// XY plot
		if (c.getXYPlot() != null) {
			for (int i = 0; i < c.getXYPlot().getSeriesCount(); i++) {
				colors.add((Color) c.getXYPlot().getRenderer().getSeriesPaint(i));
			}
		}
		// category plot
		else if (c.getCategoryPlot() != null) {
			for (int i = 0; i < c.getCategoryPlot().getCategories().size(); i++) {
				colors.add((Color) c.getCategoryPlot().getRenderer().getSeriesPaint(i));
			}
		}
		return colors;
	}

	@Override
	public Class<JFCChartsEngine> getChartsEngineClass() {
		return JFCChartsEngine.class;
	}
}
