package org.txm.chartsengine.jfreechart.core.themes.base;

import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;
import java.util.List;

import org.jfree.chart.axis.AxisState;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.ui.RectangleEdge;

/**
 * Class providing a way to draw tick marks of category axis between the category items rather than on theirs middle.
 * 
 * @author sjacquot
 *
 */
public class CategoryAxisBetweenTicks extends CategoryAxis {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5452013431211435419L;


	/**
	 *
	 */
	public CategoryAxisBetweenTicks() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * Creates an axis and copy data from the specified <code>CategoryAxis</code>.
	 * 
	 * @param axis
	 */
	public CategoryAxisBetweenTicks(CategoryAxis axis) {
		super(axis.getLabel());

		this.setLabelFont(axis.getLabelFont());
		this.setLabelPaint(axis.getLabelPaint());
		this.setTickLabelPaint(axis.getTickLabelPaint());
		this.setAxisLinePaint(axis.getAxisLinePaint());
		this.setTickMarkPaint(axis.getTickMarkPaint());
		this.setTickMarkOutsideLength(axis.getTickMarkOutsideLength());
		this.setTickLabelFont(axis.getTickLabelFont());

		// TODO : add other value copies
	}


	/**
	 * Draws ticks marks between categories items.
	 */
	public void drawTickMarks(Graphics2D g2, double cursor,
			Rectangle2D dataArea, RectangleEdge edge, AxisState state) {


		Plot p = getPlot();
		if (p == null) {
			return;
		}
		CategoryPlot plot = (CategoryPlot) p;
		double il = getTickMarkInsideLength();
		double ol = getTickMarkOutsideLength();
		Line2D line = new Line2D.Double();
		List<?> categories = plot.getCategoriesForAxis(this);
		g2.setPaint(getTickMarkPaint());
		g2.setStroke(getTickMarkStroke());


		int categoriesCount = categories.size();


		if (edge.equals(RectangleEdge.TOP)) {

			int i = 0;

			while (i < categoriesCount - 1) {

				double x = getCategoryStart(i + 1, categoriesCount, dataArea, edge) - getCategoryEnd(i, categoriesCount, dataArea, edge);
				x = getCategoryEnd(i, categoriesCount, dataArea, edge) + x / 2;

				line.setLine(x, cursor, x, cursor + il);
				g2.draw(line);
				line.setLine(x, cursor, x, cursor - ol);
				g2.draw(line);

				i++;

			}
			state.cursorUp(ol);
		}
		else if (edge.equals(RectangleEdge.BOTTOM)) {

			double x;
			double xDecals = 0;

			// x decals
			if (categoriesCount > 1) {
				xDecals = getCategoryStart(1, categoriesCount, dataArea, edge) - getCategoryEnd(0, categoriesCount, dataArea, edge);
				xDecals /= 2;
			}

			int i = 0;

			while (i < categoriesCount - 1) { // TODO : (- 1) for not drawing the last tick because of same problem of margin as above

				x = getCategoryEnd(i, categoriesCount, dataArea, edge) + xDecals;

				line.setLine(x, cursor, x, cursor - il);
				g2.draw(line);
				line.setLine(x, cursor, x, cursor + ol);
				g2.draw(line);

				i++;
			}

			state.cursorDown(ol);
		}
		else if (edge.equals(RectangleEdge.LEFT)) {
			Iterator iterator = categories.iterator();
			while (iterator.hasNext()) {
				Comparable key = (Comparable) iterator.next();
				double y = getCategoryMiddle(key, categories, dataArea, edge);
				line.setLine(cursor, y, cursor + il, y);
				g2.draw(line);
				line.setLine(cursor, y, cursor - ol, y);
				g2.draw(line);
			}
			state.cursorLeft(ol);
		}
		else if (edge.equals(RectangleEdge.RIGHT)) {
			Iterator iterator = categories.iterator();
			while (iterator.hasNext()) {
				Comparable key = (Comparable) iterator.next();
				double y = getCategoryMiddle(key, categories, dataArea, edge);
				line.setLine(cursor, y, cursor - il, y);
				g2.draw(line);
				line.setLine(cursor, y, cursor + ol, y);
				g2.draw(line);
			}
			state.cursorRight(ol);
		}
	}


}
