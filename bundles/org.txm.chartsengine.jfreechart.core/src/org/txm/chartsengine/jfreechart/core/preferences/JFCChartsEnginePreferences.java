package org.txm.chartsengine.jfreechart.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.chartsengine.jfreechart.core.JFCChartsEngine;
import org.txm.core.preferences.TXMPreferences;

/**
 * Preferences initializer and manager.
 * 
 * @author sjacquot
 *
 */
public class JFCChartsEnginePreferences extends TXMPreferences {


	public final static String PREFERENCES_PREFIX = JFCChartsEngine.NAME + "_"; //$NON-NLS-1$

	public static final String OUTPUT_FORMAT = PREFERENCES_PREFIX + "output_format"; //$NON-NLS-1$


	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(JFCChartsEnginePreferences.class)) {
			new JFCChartsEnginePreferences();
		}
		return TXMPreferences.instances.get(JFCChartsEnginePreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences defaultPreferences = this.getDefaultPreferencesNode();
		defaultPreferences.put(OUTPUT_FORMAT, JFCChartsEngine.OUTPUT_FORMAT_JFC_JAVA2D);
	}

}
