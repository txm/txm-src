package org.txm.chartsengine.jfreechart.core;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DatasetUtils;
import org.jfree.data.general.PieDataset;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.graphics2d.svg.SVGGraphics2D;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.jfreechart.core.preferences.JFCChartsEnginePreferences;
import org.txm.chartsengine.jfreechart.core.themes.base.ExtendedNumberAxis;
import org.txm.chartsengine.jfreechart.core.themes.base.JFCTheme;
import org.txm.chartsengine.jfreechart.core.themes.base.SymbolAxisBetweenTicks;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.HighchartsDefaultTheme;
import org.txm.utils.logger.Log;

import com.lowagie.text.Document;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.DefaultFontMapper;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Charts engine providing methods to create charts using the JFreeChart library.
 *
 * @author sjacquot
 *
 */
public class JFCChartsEngine extends ChartsEngine {

	/**
	 * Constants for extra output formats.
	 */
	public final static String OUTPUT_FORMAT_JFC_JAVA2D = "JFreeChart/Java2D"; //$NON-NLS-1$


	/**
	 * The charts engine internal name.
	 */
	public final static String NAME = "jfreechart_charts_engine"; //$NON-NLS-1$

	/**
	 * The charts engine description.
	 */
	public final static String DESCRIPTION = Messages.JFCChartsEngine_0;

	/**
	 * The rendering theme.
	 */
	protected JFCTheme jfcTheme;



	/**
	 * Creates a JFreeChart charts engine implementation with the specified theme and output format.
	 *
	 * @param outputFormat the output format for the chart, it can be a file or an UI object
	 */
	public JFCChartsEngine(String outputFormat) {
		super(DESCRIPTION, outputFormat);
		this.jfcTheme = new HighchartsDefaultTheme(this);
	}

	/**
	 * Creates a JFreeChart charts engine with default Highcharts theme and output format from the preferences.
	 */
	public JFCChartsEngine() {
		this(JFCChartsEnginePreferences.getInstance().getString(JFCChartsEnginePreferences.OUTPUT_FORMAT));
	}



	/**
	 * Creates an XY line chart from the specified dataset.
	 *
	 * @param dataset
	 * @return the JFreeChart chart
	 */
	public JFreeChart createXYLineChart(ChartResult result, XYSeriesCollection dataset, String title, String xAxisLabel, String yAxisLabel, boolean showLegends, boolean useSplines,
			boolean linesVisible, boolean shapesVisible,
			boolean domainAxisintegerTicks, boolean rangeAxisintegerTicks, boolean applyTheme, String[] domainAxisSymbols) {

		JFreeChart chart = ChartFactory.createXYLineChart(title, xAxisLabel, yAxisLabel, dataset, PlotOrientation.VERTICAL, true, false, false);

		// Custom renderer
		XYPlot plot = chart.getXYPlot();
		if (!useSplines) {
			// Custom XYLineAndShapeRenderer selection renderer
			plot.setRenderer(this.jfcTheme.createXYLineAndShapeRenderer(result, chart, linesVisible, shapesVisible));
		}
		else {
			plot.setRenderer(this.jfcTheme.createXYCardinalSplineRenderer(linesVisible, shapesVisible));
		}

		// Custom range axis for ticks drawing options
		plot.setRangeAxis(new ExtendedNumberAxis((NumberAxis) plot.getRangeAxis(), true, true, DatasetUtils.findMinimumRangeValue(plot.getDataset()).doubleValue(), DatasetUtils.findMaximumRangeValue(
				plot.getDataset()).doubleValue()));


		// Domain axis integer tick units, X
		if (domainAxisintegerTicks) {
			chart.getXYPlot().getDomainAxis().setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		}

		// Range axis integer tick units, Y
		if (rangeAxisintegerTicks) {
			chart.getXYPlot().getRangeAxis().setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		}


		// Domain axis symbols instead of values
		if (domainAxisSymbols != null) {
			chart.getXYPlot().setDomainAxis(new SymbolAxisBetweenTicks(xAxisLabel, domainAxisSymbols));
		}

		// Apply theme to the chart
		if (applyTheme) {
			this.jfcTheme.apply(chart);
		}


		return chart;

	}

	public JFreeChart createXYLineChart(ChartResult result, XYSeriesCollection dataset, String title, String xAxisLabel, String yAxisLabel, boolean showLegends, boolean useSplines,
			boolean linesVisible, boolean shapesVisible,
			boolean domainAxisintegerTicks, boolean rangeAxisintegerTicks, boolean applyTheme) {
		return this.createXYLineChart(result, dataset, title, xAxisLabel, yAxisLabel, showLegends, useSplines, linesVisible, shapesVisible, domainAxisintegerTicks, rangeAxisintegerTicks, applyTheme,
				null);
	}

	/**
	 * Creates an XY line chart from the specified dataset.
	 *
	 * @param dataset
	 * @param title
	 * @param xAxisLabel
	 * @param yAxisLabel
	 * @param showLegends
	 * @param useSplines
	 * @param linesVisible
	 * @param shapesVisible
	 * @return
	 */
	public JFreeChart createXYLineChart(ChartResult result, XYSeriesCollection dataset, String title, String xAxisLabel, String yAxisLabel, boolean showLegends, boolean useSplines,
			boolean linesVisible, boolean shapesVisible) {
		return this.createXYLineChart(result, dataset, title, xAxisLabel, yAxisLabel, showLegends, useSplines, linesVisible, shapesVisible, false, false, true, null);
	}



	/**
	 *
	 * @param dataset
	 * @param title
	 * @param showLegends
	 * @return
	 */
	public JFreeChart createPieChart(PieDataset dataset, String title, boolean showLegends) {
		JFreeChart chart = ChartFactory.createPieChart(title, dataset, showLegends, false, false);

		return chart;
	}


	/**
	 * Creates an XY step chart from the specified dataset.
	 *
	 * @param dataset
	 * @param title
	 * @param xAxisLabel
	 * @param yAxisLabel
	 * @param showLegends
	 * @param linesVisible
	 * @param shapesVisible
	 * @param rangeAxisintegerTicks
	 * @param domainAxisintegerTicks
	 * @return
	 */
	public JFreeChart createXYStepChart(XYSeriesCollection dataset, String title, String xAxisLabel, String yAxisLabel, boolean showLegends, boolean linesVisible, boolean shapesVisible,
			boolean domainAxisintegerTicks, boolean rangeAxisintegerTicks) {

		JFreeChart chart = ChartFactory.createXYLineChart(title, xAxisLabel, yAxisLabel, dataset, PlotOrientation.VERTICAL, true, false, false);

		// Custom renderer
		XYPlot plot = chart.getXYPlot();
		plot.setRenderer(this.jfcTheme.createXYStepRenderer(linesVisible, shapesVisible));


		// Domain axis integer tick units, X
		if (domainAxisintegerTicks) {
			chart.getXYPlot().getDomainAxis().setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		}

		// Range axis integer tick units, Y
		if (rangeAxisintegerTicks) {
			chart.getXYPlot().getRangeAxis().setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		}

		return chart;

	}



	/**
	 * Creates an XY step chart from the specified dataset.
	 *
	 * @param dataset
	 * @param title
	 * @param xAxisLabel
	 * @param yAxisLabel
	 * @param showLegends
	 * @param linesVisible
	 * @param shapesVisible
	 * @return
	 */
	public JFreeChart createXYStepChart(XYSeriesCollection dataset, String title, String xAxisLabel, String yAxisLabel, boolean showLegends, boolean linesVisible, boolean shapesVisible) {
		return this.createXYStepChart(dataset, title, xAxisLabel, yAxisLabel, showLegends, linesVisible, shapesVisible, false, false);
	}

	/**
	 * Creates category step chart from the specified dataset.
	 *
	 * @param dataset
	 * @param title
	 * @param xAxisLabel
	 * @param yAxisLabel
	 * @param showLegends
	 * @return
	 */
	public JFreeChart createCategoryStepChart(CategoryDataset dataset, String title, String xAxisLabel, String yAxisLabel, boolean showLegends) {
		return this.createCategoryStepChart(dataset, title, xAxisLabel, yAxisLabel, showLegends, false);
	}



	/**
	 * Creates category step chart from the specified dataset.
	 *
	 * @param dataset
	 * @param title
	 * @param xAxisLabel
	 * @param yAxisLabel
	 * @param showLegends
	 * @param rangeAxisintegerTicks
	 * @return
	 */
	public JFreeChart createCategoryStepChart(CategoryDataset dataset, String title, String xAxisLabel, String yAxisLabel, boolean showLegends, boolean rangeAxisintegerTicks) {

		JFreeChart chart = ChartFactory.createLineChart(title, xAxisLabel, yAxisLabel, dataset, PlotOrientation.VERTICAL, true, false, false);

		// Custom renderer
		CategoryPlot plot = chart.getCategoryPlot();
		plot.setRenderer(this.jfcTheme.createCategoryStepRenderer());


		// Range axis integer tick units, Y
		if (rangeAxisintegerTicks) {
			chart.getCategoryPlot().getRangeAxis().setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		}


		// Apply theme to the chart
		this.jfcTheme.apply(chart);

		return chart;

	}



	/**
	 * Creates a category line chart from the specified dataset.
	 *
	 * @param dataset
	 * @param title
	 * @param categoryAxisLabel
	 * @param valueAxisLabel
	 * @param showLegends
	 * @param integerTicks
	 * @return the JFreeChart chart
	 */
	public JFreeChart createCategoryLineChart(CategoryDataset dataset, String title, String categoryAxisLabel, String valueAxisLabel, boolean showLegends, boolean integerTicks) {

		JFreeChart chart = ChartFactory.createLineChart(title, categoryAxisLabel, valueAxisLabel, dataset, PlotOrientation.VERTICAL, showLegends, true, false);

		// Custom renderer
		chart.getCategoryPlot().setRenderer(this.jfcTheme.createCategoryLineAndShapeRenderer(true, true));

		// Axis integer tick units
		if (integerTicks) {
			chart.getCategoryPlot().getRangeAxis().setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		}

		return chart;
	}


	/**
	 * Creates a category bar chart from specified dataset.
	 *
	 * @param dataset
	 * @param title
	 * @param categoryAxisLabel
	 * @param valueAxisLabel
	 * @param showLegends
	 * @return the JFreeChart chart
	 */
	public JFreeChart createCategoryBarChart(DefaultCategoryDataset dataset, String title, String categoryAxisLabel, String valueAxisLabel, boolean showLegends, boolean rangeAxisIntegerTicks,
			boolean applyTheme) {

		JFreeChart chart = ChartFactory.createBarChart(title, categoryAxisLabel, valueAxisLabel, dataset, PlotOrientation.VERTICAL, showLegends, true, false);

		// Custom renderer
		chart.getCategoryPlot().setRenderer(this.jfcTheme.createCategoryBarRenderer());


		// Axis integer tick units
		if (rangeAxisIntegerTicks) {
			chart.getCategoryPlot().getRangeAxis().setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		}

		// Apply theme to the chart
		if (applyTheme) {
			this.jfcTheme.apply(chart);
		}

		return chart;
	}

	/**
	 * Creates a category bar chart from specified dataset.
	 *
	 * @param dataset
	 * @param title
	 * @param categoryAxisLabel
	 * @param valueAxisLabel
	 * @param showLegends
	 * @param rangeAxisIntegerTicks
	 * @return
	 */
	public JFreeChart createCategoryBarChart(DefaultCategoryDataset dataset, String title, String categoryAxisLabel, String valueAxisLabel, boolean showLegends, boolean rangeAxisIntegerTicks) {
		return this.createCategoryBarChart(dataset, title, categoryAxisLabel, valueAxisLabel, showLegends, rangeAxisIntegerTicks, true);
	}


	/**
	 * Creates an XY bar chart from the specified dataset
	 *
	 * @param dataset
	 * @param title
	 * @param xAxisLabel
	 * @param yAxisLabel
	 * @param showLegends
	 * @return
	 */
	public JFreeChart createXYBarChart(IntervalXYDataset dataset, String title, String xAxisLabel, String yAxisLabel, boolean showLegends, boolean rangeAxisIntegerTicks, boolean applyTheme,
			String[] domainAxisSymbols) {

		JFreeChart chart = ChartFactory.createXYBarChart(title, xAxisLabel, false, yAxisLabel, dataset, PlotOrientation.VERTICAL, showLegends, true, false);


		// Custom renderer
		chart.getXYPlot().setRenderer(this.jfcTheme.createXYBarRenderer());


		// Range axis integer tick units
		if (rangeAxisIntegerTicks) {
			chart.getXYPlot().getRangeAxis().setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		}

		// Domain axis symbols instead of values
		if (domainAxisSymbols != null) {
			chart.getXYPlot().setDomainAxis(new SymbolAxisBetweenTicks(xAxisLabel, domainAxisSymbols));
		}

		// Apply theme to the chart
		if (applyTheme) {
			this.jfcTheme.apply(chart);
		}

		return chart;
	}


	/**
	 * Creates an XY bar chart from the specified dataset
	 *
	 * @param dataset
	 * @param title
	 * @param xAxisLabel
	 * @param yAxisLabel
	 * @param showLegends
	 * @param rangeAxisIntegerTicks
	 * @return
	 */
	public JFreeChart createXYBarChart(IntervalXYDataset dataset, String title, String xAxisLabel, String yAxisLabel, boolean showLegends, boolean rangeAxisIntegerTicks) {
		return this.createXYBarChart(dataset, title, xAxisLabel, yAxisLabel, showLegends, rangeAxisIntegerTicks, true, null);
	}


	/**
	 * @return the theme
	 */
	public JFCTheme getJFCTheme() {
		return jfcTheme;
	}

	
	@Override
	public File exportChartResultToFile(ChartResult result, File file, int imageWidth, int imageHeight, String outputFormat) {
		Object chart = result.getChart();
		if (chart == null) {
			Log.severe("Error: null chart in " + result);
			return null;
		}
		if (chart instanceof JFreeChart) {
			return exportChartToFile(chart, file, outputFormat, imageWidth, imageHeight);
		}
		else {
			Log.severe("Error: the chart is not a JFreeChart " + chart + " " + result); //$NON-NLS-2$
			return null;
		}
	}

	// @Override
	// public File createChartFile(Object chart, File file, String outputFormat) {
	// return this.export((JFreeChart) chart, file);
	// }

	@Override
	public File exportChartToFile(Object chart, File file, String outputFormat, int imageWidth, int imageHeight, int drawingAreaX, int drawingAreaY, int drawingAreaWidth, int drawingAreaHeight) {
		return JFCChartsEngine.export(chart, file, outputFormat, imageWidth, imageHeight, drawingAreaX, drawingAreaY, drawingAreaWidth, drawingAreaHeight);
	}


	/**
	 * Exports the chart to the specified file. Also crops the image according to the specified drawing area values.
	 *
	 * @param chart
	 * @param file
	 * @param outputFormat
	 * @param imageWidth
	 * @param imageHeight
	 * @param drawingAreaX
	 * @param drawingAreaY
	 * @param drawingAreaWidth
	 * @param drawingAreaHeight
	 * @return
	 */
	public static File export(Object chart, File file, String outputFormat, int imageWidth, int imageHeight, int drawingAreaX, int drawingAreaY, int drawingAreaWidth, int drawingAreaHeight) {
		// SVG
		if (outputFormat.equals(OUTPUT_FORMAT_SVG)) {
			return JFCChartsEngine.exportSVG((JFreeChart) chart, file, imageWidth, imageHeight, drawingAreaX, drawingAreaY, drawingAreaWidth, drawingAreaHeight);
		}
		// PDF
		else if (outputFormat.equals(OUTPUT_FORMAT_PDF)) {
			return JFCChartsEngine.exportPDF((JFreeChart) chart, file, imageWidth, imageHeight, drawingAreaX, drawingAreaY, drawingAreaWidth, drawingAreaHeight);
		}
		// Raster
		else {
			return JFCChartsEngine.exportRasterImage((JFreeChart) chart, file, outputFormat, imageWidth, imageHeight, drawingAreaX, drawingAreaY, drawingAreaWidth, drawingAreaHeight);
		}
	}

	/**
	 * Exports the chart to the specified file.
	 *
	 * @param chart
	 * @param file
	 * @param outputFormat
	 * @param imageWidth
	 * @param imageHeight
	 * @param drawingAreaWidth
	 * @param drawingAreaHeight
	 * @return
	 */
	public static File export(Object chart, File file, String outputFormat, int imageWidth, int imageHeight, int drawingAreaWidth, int drawingAreaHeight) {
		return JFCChartsEngine.export(chart, file, outputFormat, imageWidth, imageHeight, 0, 0, drawingAreaWidth, drawingAreaHeight);
	}

	/**
	 * Exports the chart to the specified file.
	 *
	 * @param chart
	 * @param file
	 * @param outputFormat
	 * @param imageWidth
	 * @param imageHeight
	 * @return
	 */
	public static File export(Object chart, File file, String outputFormat, int imageWidth, int imageHeight) {
		return JFCChartsEngine.export(chart, file, outputFormat, imageWidth, imageHeight, imageWidth, imageHeight);
	}



	/**
	 * Exports the chart to the specified file in SVG format. Also crops the image according to the specified drawing area values.
	 *
	 * @param chart
	 * @param file
	 */
	public static File exportSVG(JFreeChart chart, File file, int imageWidth, int imageHeight, int drawingAreaX, int drawingAreaY, int drawingAreaWidth, int drawingAreaHeight) {

		try {

			//FIXME: SJ: tests for better export
			//System.err.println("JFCChartsEngine.exportSVG(): writing chart to file " + file + "...");


			SVGGraphics2D g2 = new SVGGraphics2D(imageWidth, imageHeight);
			//SVGGraphics2D g2 = new SVGGraphics2D(drawingAreaWidth, drawingAreaHeight);

			Rectangle2D drawingArea = new Rectangle2D.Double(drawingAreaX, drawingAreaY, drawingAreaWidth, drawingAreaHeight);


			// Suppress shadow generation, because SVG is a vector format and the shadow effect is applied via bitmap effects...
			g2.setRenderingHint(JFreeChart.KEY_SUPPRESS_SHADOW_GENERATION, true);

			//FIXME: SJ: original code
			//			String svg = null;
			//			chart.draw(g2, drawingArea);
			//			svg = g2.getSVGElement();

			//FIXME: SJ: tests for better export
			// scale the chart to match the document width user preference
			JFCChartsEngine.scaleChart(chart, g2, imageWidth, imageHeight, drawingAreaX, drawingAreaY, drawingAreaWidth, drawingAreaHeight);

			String svg = g2.getSVGElement();
			g2.dispose();


			// JFreeSVG generates rgba(x,x,x,x.x) CSS style which is part of SVG version 2. It's incompatible with the actual Batik version used by TXM to display SVG Files
			// To fix this, replace from rgba() to rgb(), e.g.: stroke: rgba(255,0,0,0.352941);stroke-opacity: 1.0
			svg = svg.replaceAll("rgba\\(([0-9]+),([0-9]+),([0-9]+),([0-9\\.]+)\\);(stroke|fill)-opacity: [0-9\\.]+", "rgb\\($1,$2,$3\\);$5-opacity: $4"); //$NON-NLS-1$ //$NON-NLS-2$
			// second pass
			svg = svg.replaceAll("rgba\\(([0-9]+),([0-9]+),([0-9]+),[0-9\\.]+\\)", "rgb\\($1,$2,$3\\)"); //$NON-NLS-1$ //$NON-NLS-2$

			// fix angular line join bug in step charts #1056
			svg = svg.replaceAll("<svg ", "<svg style=\"stroke-linecap:square;\" "); //$NON-NLS-1$ //$NON-NLS-2$

			// fix double ;; character bug #1554
			svg = svg.replaceAll("(;)(; )(stroke|fill)", "$2$3"); //$NON-NLS-1$ //$NON-NLS-2$



			if (file != null) {
				BufferedWriter writer = null;
				try {
					writer = new BufferedWriter(new FileWriter(file));
					writer.write("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n"); //$NON-NLS-1$
					writer.write(svg + "\n"); //$NON-NLS-1$
					writer.flush();
				}
				finally {
					try {
						if (writer != null) {
							writer.close();
						}
					}
					catch (IOException ex) {
						throw new RuntimeException(ex);
					}
				}

			}
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		//System.err.println("JFCChartsEngine.exportSVG(): File " + file + " saved.");

		return file;
	}



	/**
	 * Exports the chart to the specified file in Raster format. Also crops the image according to the specified drawing area values.
	 *
	 * @param chart
	 * @param file
	 * @param outputFormat
	 * @param imageWidth the exported image width in pixels
	 * @param imageHeight the exported image width in pixels
	 * @param drawingAreaX
	 * @param drawingAreaY
	 * @param drawingAreaWidth the chart width
	 * @param drawingAreaHeight the chart height
	 * @return
	 */
	// TODO: SJ: enhance this method to add compression mode and quality, see for example: ImageIO.getImageWritersByFormatName("jpg")
	public static File exportRasterImage(JFreeChart chart, File file, String outputFormat, int imageWidth, int imageHeight, int drawingAreaX, int drawingAreaY, int drawingAreaWidth,
			int drawingAreaHeight) {


		//NOTE: SJ: when using this createBufferedImage() method, the image type is ARGB instead of RGB so it doesn't work for JPEG, BMP, etc. only for PNG,
		// that's why the created image data is copied to a new one of RGB type. A better way to do that may be to adapt source code of createBufferedImage()
		//		BufferedImage bufferedImage = chart.createBufferedImage(imageWidth, imageHeight, drawingAreaWidth, drawingAreaHeight, null);
		//		BufferedImage convertedImg = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.TYPE_INT_RGB);
		//	    convertedImg.getGraphics().drawImage(bufferedImage, 0, 0, null);
		//	    convertedImg.getGraphics().dispose();


		BufferedImage bufferedImage = chart.createBufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB, null);
		Graphics2D g = (Graphics2D) bufferedImage.getGraphics();

		// scale the chart to match the document width user preference
		JFCChartsEngine.scaleChart(chart, g, imageWidth, imageHeight, drawingAreaX, drawingAreaY, drawingAreaWidth, drawingAreaHeight);
		g.dispose();
		
		try {
			javax.imageio.ImageIO.write(bufferedImage, outputFormat, file);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return file;
	}



	/**
	 * Exports the chart to the specified file in PDF format. Also crops the image according to the specified drawing area values.
	 *
	 * @param chart
	 * @param file
	 */
	public static File exportPDF(JFreeChart chart, File file, int documentWidth, int documentHeight, int drawingAreaX, int drawingAreaY, int drawingAreaWidth, int drawingAreaHeight) {

		// FIXME: UTF-8 Russian characters are not supported
		// FIXME: The fonts used in the PDF are not based on the source chart fonts

		// Lowagie iTtext version
		//Document document = new Document(new Rectangle(drawingAreaX, drawingAreaY, drawingAreaWidth, drawingAreaHeight));
		Document document = new Document(new Rectangle(documentWidth, documentHeight));

		// FIXME: for size in millimeters
		//		float width = 112.86f * 72f / 25.4f;
		//		float height = 169.33f * 72f / 25.4f;


		//System.out.println("JFCChartsEngine.exportPDF() width " + documentWidth);


		try {

			Rectangle2D drawingArea = new Rectangle2D.Double(drawingAreaX, drawingAreaY, drawingAreaWidth, drawingAreaHeight);

			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));

			// FontFactory.register(this.theme.getRegularFont().getName(), "MY_FONT");

			// FontSelector fs = new FontSelector();
			// fs.addFont(FontFactory.getFont("MY_FONT", BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
			// //fs.process();

			document.open();

			PdfContentByte cb = writer.getDirectContent();



			// GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
			// Font[] fonts = e.getAllFonts();
			// for(int i = 0; i < fonts.length; i++) {
			// System.out.println("JFCChartsEngine.exportPDF(): " + fonts[i].getPSName() + " - " + fonts[i].getFontName() + " - " + fonts[i].getName() + " - " + fonts[i].getFamily());
			// }

			// Test de récupération de tous les caractères de la font
			// for (char c = 0x0000; c <= 0xFFFF; c++) {
			// if (this.theme.getRegularFont().canDisplay(c)) {
			//
			// }
			// }

			// FontMapper fm = new FontMapper() {
			//
			// @Override
			// public Font pdfToAwt(BaseFont arg0, int arg1) {
			// // TODO Auto-generated method stub
			// return null;
			// }
			//
			// @Override
			// public BaseFont awtToPdf(Font arg0) {
			//
			// BaseFont unicode = null;
			//
			// try {
			// // FIXME: fonctionne pour l'UTF-8 mais n'est pas portable et n'est pas basé sur la police réelle du chart
			// //unicode = BaseFont.createFont("c:/windows/fonts/arialuni.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED); // Windows
			//
			//
			// // FIXME : test de passage direct des bytes de la Font
			//// byte ttfAfm[] = new byte[]{12, 58, 23};
			//// byte pfb[] = new byte[]{32, 25, 2};
			//// unicode = BaseFont.createFont(theme.getRegularFont().getName(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED, BaseFont.CACHED, ttfAfm, pfb);
			//
			//
			//
			// // FIXME : test de récupération du chemin vers la police en fonction de l'OS
			// //unicode = BaseFont.createFont("c:/windows/fonts/arialuni.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED); // Windows
			//
			//
			// //unicode = BaseFont.createFont(BaseFont.COURIER, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			//
			//
			// //unicode = BaseFont.createFont(this.theme.getRegularFont().getPSName(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			//
			// // Default font
			// unicode = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.EMBEDDED);
			//
			//
			// }
			// catch(DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// catch(IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			//
			// return unicode;
			// }
			// };

			// Graphics2D g = cb.createGraphics(documentWidth, documentHeight, fm);


			// Autres tests
			// FontRenderContext frc = g.getFontRenderContext();
			// GlyphVector gv = this.theme.getRegularFont().createGlyphVector(frc, "aérouiez");


			// Graphics2D g = cb.createGraphics(documentWidth, documentHeight, new AsianFontMapper(AsianFontMapper.JapaneseFont_Min, AsianFontMapper.JapaneseEncoding_H));

			//
			// try {
			// GraphicsEnvironment ge =
			// GraphicsEnvironment.getLocalGraphicsEnvironment();
			// Font fonts[] = ge.getAllFonts();
			// for (int k = 0; k < fonts.length; ++k)
			// System.out.println(fonts[k].getFontName() + " * " +
			// fonts[k].toString());
			// }
			// catch (Exception de) {
			// de.printStackTrace();
			// }



			// FIXME: test 1
			// DefaultFontMapper mapper = new DefaultFontMapper() {
			// @Override
			// public BaseFont awtToPdf(Font font) {
			//
			// BaseFont outputFont = null;
			//
			// try {
			// // Try to configure the font as Unicode font
			// DefaultFontMapper.BaseFontParameters pp;
			//
			// System.err.println("JFCChartsEngine.exportPDF(...).new DefaultFontMapper() {...}.awtToPdf(): " + font.getFontName());
			//
			// pp = this.getBaseFontParameters(font.getFontName());
			// if (!font.getFontName().equals("Dialog.plain") && pp != null) {
			// pp.encoding = BaseFont.IDENTITY_H;
			// pp.embedded = true;
			// outputFont = BaseFont.createFont(font.getFontName(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			// }
			// else {
			// System.err.println("JFCChartsEngine.exportPDF(): ********************************* pp = null ********************************************");
			// // Default font
			// outputFont = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.EMBEDDED);
			// }
			// }
			// catch(DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// catch(IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			//
			// return outputFont;
			// }
			// };


			// FIXME: test 2
			DefaultFontMapper mapper = new DefaultFontMapper();

			// FIXME: need to add here the Mac OS X and Linux font path
			// Platform specific fonts paths

			// FIXME: Windows, test fonts included in JRE
			// mapper.insertDirectory("C:/Tools/Coding/Java/jdk1.7.0_45/jre/lib/fonts/");

			// Windows
			mapper.insertDirectory(System.getenv().get("SystemRoot") + "/Fonts/"); //$NON-NLS-1$ //$NON-NLS-2$

			// Linux
			mapper.insertDirectory("/usr/share/fonts/"); //$NON-NLS-1$
			mapper.insertDirectory("/usr/local/share/fonts/"); //$NON-NLS-1$
			mapper.insertDirectory(System.getProperty("user.home") + "/.fonts"); //$NON-NLS-1$ //$NON-NLS-2$

			// Mac
			mapper.insertDirectory(System.getProperty("user.home") + "/Library/Fonts"); //$NON-NLS-1$ //$NON-NLS-2$
			mapper.insertDirectory("/System/Library/Fonts"); //$NON-NLS-1$
			mapper.insertDirectory("/Library/Fonts"); //$NON-NLS-1$



			// FIXME: recursive addition of subdirectories of root font paths
			// static protected void traverseDir(File folder, DefaultFontMapper mapper) {
			// File[] files = folder.listFiles(IOUtils.FILTER_HIDDEN);
			// for (int i = 0; i < files.length; i++) {
			// if (files[i].isDirectory()) {
			// mapper.insertDirectory(files[i].getPath());
			// traverseDir(new File(files[i].getPath()), mapper);
			// }
			// }
			// }


			DefaultFontMapper.BaseFontParameters pp;
			// FIXME: SJ: the problem here is that the font is based only on the Base Item Label Font of the source chart
			String fontName = null;
			// Category plots
			if (chart.getPlot() instanceof CategoryPlot) {
				fontName = chart.getCategoryPlot().getRenderer().getDefaultItemLabelFont().getFontName();
			}
			// XY plots
			else if (chart.getPlot() instanceof XYPlot) {
				fontName = chart.getXYPlot().getRenderer().getDefaultItemLabelFont().getFontName();
			}
			// other plots that have no renderer, eg. SpiderWebPlot
			else {
				fontName = ((JFCChartsEngine) ChartsEngine.getCurrent()).getJFCTheme().getRegularFont().getFontName();
			}



			pp = mapper.getBaseFontParameters(fontName);

			if (pp != null) {
				pp.encoding = BaseFont.IDENTITY_H;
				pp.embedded = true;
			}

			Graphics2D g = cb.createGraphics(documentWidth, documentHeight, mapper);

			// scale the chart to match the document width user preference
			JFCChartsEngine.scaleChart(chart, g, documentWidth, documentHeight, drawingAreaX, drawingAreaY, drawingAreaWidth, drawingAreaHeight);


		}
		catch (Exception e) {
			e.printStackTrace();
		}

		document.close();


		return file;
	}


	/**
	 * Exports the chart to the specified file according to the current output format of the charts engine.
	 *
	 * @param chart
	 * @param file
	 * @return
	 */
	public File export(JFreeChart chart, File file) {

		// Apply theme to the chart before export
		// FIXME: removed for the Raster image rendering colors mode works
		// this.theme.apply(chart);

		return this.exportChartToFile(chart, file, this.outputFormat, 800, 600);

	}



	/**
	 * Scales a chart to match the document or image dimensions and draws it to the specified Graphics2D.
	 * 
	 * @param chart the chart to scale and draw
	 * @param g the Graphics2D to draw in
	 * @param imageWidth the output width
	 * @param imageHeight the output height
	 * @param drawingAreaX
	 * @param drawingAreaY
	 * @param drawingAreaWidth
	 * @param drawingAreaHeight
	 */
	public static void scaleChart(JFreeChart chart, Graphics2D g, int imageWidth, int imageHeight, int drawingAreaX, int drawingAreaY, int drawingAreaWidth, int drawingAreaHeight) {
		ChartsEngine.scaleGraphics2D(g, imageWidth, imageHeight, drawingAreaWidth, drawingAreaHeight);
		chart.draw(g, new Rectangle2D.Double(drawingAreaX, drawingAreaY, drawingAreaWidth, drawingAreaHeight));
	}

	

	@Override
	public ArrayList<String> getSupportedOutputDisplayFormats() {
		ArrayList<String> formats = new ArrayList<>(1);

		formats.add(OUTPUT_FORMAT_JFC_JAVA2D);

		return formats;
	}

	@Override
	public ArrayList<String> getSupportedOutputFileFormats() {
		ArrayList<String> formats = new ArrayList<>(6);

		formats.add(OUTPUT_FORMAT_SVG); // Default format

		// FIXME: SJ:
		// Auto-populate supported output file raster formats from available image writers
		// NOTE: calling this method here leads to a freeze on Mac OS X when ChartsEngine.initDefaultProperties() is called
		// populateSupportedOutputRasterFileFormats(supportedOutputFileFormats);

		formats.add(OUTPUT_FORMAT_PNG);
		formats.add(OUTPUT_FORMAT_JPEG);
		formats.add(OUTPUT_FORMAT_TIFF);
		formats.add(OUTPUT_FORMAT_BMP);
		formats.add(OUTPUT_FORMAT_GIF);
		formats.add(OUTPUT_FORMAT_PDF);

		return formats;
	}



	//
	//
	// @Override
	// public void updateChartCAFactorialMapSetDimensions(Object chart, CA ca, int dimension1, int dimension2) {
	// super.updateChartCAFactorialMapSetDimensions(chart, ca, dimension1, dimension2);
	//
	// // Modify data set
	// ((FCAXYDataset) ((JFreeChart) chart).getXYPlot().getDataset()).setAxis1(dimension1);
	// ((FCAXYDataset) ((JFreeChart) chart).getXYPlot().getDataset()).setAxis2(dimension2);
	//
	// // Update axis labels
	// // FIXME : create a method in CA to directly get a singular value as percent ?
	// try {
	// double sinuglarValuesSum = ca.getValeursPropresSum();
	// DecimalFormat f = new DecimalFormat("###.00"); //$NON-NLS-1$
	// ((JFreeChart) chart).getXYPlot().getDomainAxis().setLabel(ChartsEngineMessages.ChartsEngine_CA_FACTORIAL_MAP_AXIS_LABEL_PREFIX + " " + dimension1 + " (" + f.format(100 *
	// ca.getValeursPropres()[dimension1 - 1] / sinuglarValuesSum) + "%)");
	// ((JFreeChart) chart).getXYPlot().getRangeAxis().setLabel(ChartsEngineMessages.ChartsEngine_CA_FACTORIAL_MAP_AXIS_LABEL_PREFIX + " " + dimension2 + " (" + f.format(100 *
	// ca.getValeursPropres()[dimension2 - 1] / sinuglarValuesSum) + "%)");
	//
	// // Refresh data set
	// // FIXME : any way to fire a data set event rather than reassign the same data set ?
	// ((JFreeChart) chart).getXYPlot().setDataset(((JFreeChart) chart).getXYPlot().getDataset());
	// // the code below doesn't neither center the view nor update the axes ticks of the new chart, continue tests
	// //((JFreeChart) chart).setNotify(true);
	//
	//
	// // Update the limits border
	// JFCChartsEngine.createCAFactorialMapChartLimitsBorder((JFreeChart) chart);
	//
	// }
	// catch(StatException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
	//
	//
	// @Override
	// public void updateChartCAFactorialMapHighlightPoints(Object chart, boolean rows, String[] labels) {
	//
	// MultipleItemsSelector selector = (MultipleItemsSelector) ((FCAItemSelectionRenderer)((JFreeChart) chart).getXYPlot().getRenderer()).getItemsSelector();
	//
	// // Rows
	// int series = 0;
	// // Columns
	// if(!rows) {
	// series = 1;
	// }
	// selector.removeAllSelectedItems(series);
	// int[] items = ((FCAXYDataset)((JFreeChart) chart).getXYPlot().getDataset()).getLabelIndices(series, labels);
	//
	// for(int i = 0; i < items.length; i++) {
	// selector.addSelectedItem(series, items[i]);
	// }
	//
	// ((JFreeChart) chart).setNotify(true);
	// }
	//



	/**
	 * Defines the theme to use for rendering charts.
	 *
	 * @param theme the theme to set
	 */
	public void setTheme(JFCTheme theme) {
		this.jfcTheme = theme;
	}


	/**
	 * Constrains axes ticks to square aspect ratio.
	 *
	 * @param width
	 * @param height
	 */
	public void squareOffGraph(Object c, double width, double height) {

		JFreeChart chart = (JFreeChart) c;

		if (chart.getPlot() instanceof XYPlot) {

			XYPlot plot = (XYPlot) chart.getPlot();

			ValueAxis xAxis = plot.getDomainAxis();
			ValueAxis yAxis = plot.getRangeAxis();

			double xLower = xAxis.getLowerBound();
			double xUpper = xAxis.getUpperBound();


			double yLower = yAxis.getLowerBound();
			double yUpper = yAxis.getUpperBound();


			// FIXME: SJ: to fix a bug when no rows and no cols are displayed, but we need to avoid this case directly in the CA editor tool bar
			if (plot.getDataRange(xAxis) == null || plot.getDataRange(yAxis) == null) {
				return;
			}

			double xDataRangeLength = plot.getDataRange(xAxis).getLength();
			double yDataRangeLength = plot.getDataRange(yAxis).getLength();
			// System.out.println("ItemSelectionChartPanel.squareOffGraph() data range " + xDataRangeLength + " : " + yDataRangeLength);
			// System.out.println("ItemSelectionChartPanel.squareOffGraph() width/height" + width + " : " + height);


			// if(width > height) {


			if (xDataRangeLength > yDataRangeLength) {

				double idealYRange = (xUpper - xLower) * height / width;
				double currYRange = yUpper - yLower;

				double yDelta = (idealYRange - currYRange) / 2;


				// FIXME: SJ: test en checkant que la totalité du graph soit bien visible
				if (plot.getDataRange(yAxis).getLowerBound() < (yLower - yDelta) || plot.getDataRange(yAxis).getUpperBound() > (yUpper + yDelta)) {

					// System.err.println("ItemSelectionChartPanel.squareOffGraph()");

					double idealXRange = (yUpper - yLower) * width / height;
					double currXRange = xUpper - xLower;

					double xDelta = (idealXRange - currXRange) / 2;

					// Are we within a pixel of being square?
					if ((int) (xDelta * width) == 0) {
						return;
					}

					xAxis.setLowerBound(xLower - xDelta);
					xAxis.setUpperBound(xUpper + xDelta);

				}
				else {

					// Are we within a pixel of being square?
					if ((int) (yDelta * height) == 0) {
						return;
					}

					yAxis.setLowerBound(yLower - yDelta);
					yAxis.setUpperBound(yUpper + yDelta);
				}
			}
			else {

				double idealXRange = (yUpper - yLower) * width / height;
				double currXRange = xUpper - xLower;

				double xDelta = (idealXRange - currXRange) / 2;


				// FIXME: SJ: test en checkant que la totalité du graph soit bien visible
				if (plot.getDataRange(xAxis).getLowerBound() < (xLower - xDelta) || plot.getDataRange(xAxis).getUpperBound() > (xUpper + xDelta)) {

					double idealYRange = (xUpper - xLower) * height / width;
					double currYRange = yUpper - yLower;

					double yDelta = (idealYRange - currYRange) / 2;

					// Are we within a pixel of being square?
					if ((int) (yDelta * height) == 0) {
						return;
					}

					yAxis.setLowerBound(yLower - yDelta);
					yAxis.setUpperBound(yUpper + yDelta);

				}
				else {
					// Are we within a pixel of being square?
					if ((int) (xDelta * width) == 0) {
						return;
					}

					xAxis.setLowerBound(xLower - xDelta);
					xAxis.setUpperBound(xUpper + xDelta);
				}

			}


			// }
			// else if(height > width) {
			//
			// if(xDataRangeLength < yDataRangeLength) {
			//
			// double idealYRange = (xUpper - xLower) * height / width;
			// double currYRange = yUpper - yLower;
			//
			// double yDelta = (idealYRange - currYRange) / 2;
			//
			//// // Are we within a pixel of being square?
			//// if ((int)(yDelta * height) == 0) {
			//// return;
			//// }
			//
			// yAxis.setLowerBound(yLower - yDelta);
			// yAxis.setUpperBound(yUpper + yDelta);
			//
			// }
			// else {
			//
			// double idealXRange = (yUpper - yLower) * width / height;
			// double currXRange = xUpper - xLower;
			//
			// double xDelta = (idealXRange - currXRange) / 2;
			//
			//// // Are we within a pixel of being square?
			//// if ((int)(xDelta * width) == 0) {
			//// return;
			//// }
			//
			// xAxis.setLowerBound(xLower - xDelta);
			// xAxis.setUpperBound(xUpper + xDelta);
			//
			// }
			// }
		}
	}



	@Override
	public boolean isRunning() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean stop() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getName() {
		return JFCChartsEngine.NAME;
	}
}
