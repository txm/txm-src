package org.txm.whatsnew.rcp;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * 
 * @author mdecorde
 *
 */
public class NewsPreferences extends TXMPreferences {

	public static final String SHOW_NEWS = "show_news"; //$NON-NLS-1$

	public static final String NEWS_VERSION = "news_version"; //$NON-NLS-1$

	public static final String SHOW_NEW_SETUP = "show_new_setups"; //$NON-NLS-1$

	public static final String NEW_SETUP_VERSION = "new_setups_version"; //$NON-NLS-1$

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static NewsPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(NewsPreferences.class)) {
			new NewsPreferences();
		}
		return (NewsPreferences) TXMPreferences.instances.get(NewsPreferences.class);
	}

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.putBoolean(SHOW_NEWS, false);
		preferences.put(NEWS_VERSION, "0"); // $NON-NLS-1$

		preferences.putBoolean(SHOW_NEW_SETUP, false);
		preferences.put(NEW_SETUP_VERSION, "0.8.5"); // $NON-NLS-1$
	}
}
