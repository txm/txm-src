package org.txm.whatsnew.rcp;

import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.OpenWindowListener;
import org.eclipse.swt.browser.WindowEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
import org.osgi.framework.Version;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.Activator;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMBrowserEditor;
import org.txm.rcp.p2.plugins.TXMUpdateHandler;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;
import org.txm.whatsnew.rcp.messages.Messages;


public class NewsEditor extends EditorPart {

	private Browser browser;

	private ToolItem stop;

	int vMax = 0;

	int vCurrentNews = NewsPreferences.getInstance().getInt(NewsPreferences.NEWS_VERSION);

	private ToolItem next;

	private ToolItem previous;

	private ToolItem last;

	@Override
	public void doSave(IProgressMonitor monitor) {

	}

	@Override
	public void doSaveAs() {

	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {

		this.setInput(input);
		this.setSite(site);

	}

	@Override
	public boolean isDirty() {
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void createPartControl(Composite parent) {
		parent.setLayout(new GridLayout(1, true));

		ToolBar toolbar = new ToolBar(parent, SWT.HORIZONTAL);
		toolbar.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));

		previous = new ToolItem(toolbar, SWT.PUSH | SWT.BORDER);
		previous.setText(Messages.previousNews);
		previous.setImage(IImageKeys.getImage("platform:/plugin/org.eclipse.ui/icons/full/elcl16/backward_nav.png")); //$NON-NLS-1$
		previous.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int vNews = vCurrentNews - 1;
				if (vNews <= 0) return;

				vCurrentNews = vNews;
				refreshNews();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		next = new ToolItem(toolbar, SWT.PUSH | SWT.BORDER);
		next.setText(Messages.nextNews);
		next.setImage(IImageKeys.getImage("platform:/plugin/org.eclipse.ui/icons/full/elcl16/forward_nav.png")); //$NON-NLS-1$
		next.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int vNews = vCurrentNews + 1;
				if (vNews > vMax) return;

				vCurrentNews = vNews;

				refreshNews();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		last = new ToolItem(toolbar, SWT.PUSH | SWT.BORDER);
		last.setText(Messages.lastNews);
		last.setImage(IImageKeys.getImage("platform:/plugin/org.eclipse.ui/icons/full/etool16/redo_edit.png")); //$NON-NLS-1$
		last.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				vCurrentNews = vMax;

				refreshNews();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		new ToolItem(toolbar, SWT.SEPARATOR);

		stop = new ToolItem(toolbar, SWT.CHECK | SWT.BORDER);
		stop.setText(Messages.showNewsAtTXMStartup);
		stop.setImage(IImageKeys.getImage("platform:/plugin/org.eclipse.ui.editors/icons/full/obj16/quick_assist_obj.gif")); //$NON-NLS-1$
		stop.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				NewsPreferences.getInstance().put(NewsPreferences.SHOW_NEWS, stop.getSelection());
				NewsPreferences.saveAll();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		browser = new Browser(parent, SWT.EMBEDDED);
		browser.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		browser.addOpenWindowListener(new OpenWindowListener() {

			@Override
			public void open(WindowEvent event) {

				if (org.txm.utils.OSDetector.isFamilyUnix()) {
					event.browser = browser;
					event.required = false;
				}
			}
		});
		browser.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseScrolled(MouseEvent e) {

				if ((e.stateMask & SWT.CTRL) == SWT.CTRL) {
					if (e.count > 0) {
						TXMBrowserEditor.zoomIn(browser);
					}
					else {
						TXMBrowserEditor.zoomOut(browser);
					}
				}
			}
		});
		refreshNews();
	}

	/**
	 * 
	 * @return the last news version available
	 */
	public static Version getLastVersion() {
		try {
			if (!TXMUpdateHandler.isUpdateSiteAvailable()) {
				return new Version("0"); //$NON-NLS-1$
			}

			Version v = Activator.getDefault().getBundle().getVersion();
			String version = "" + v.getMajor() + "." + v.getMinor() + "." + v.getMicro(); //$NON-NLS-1$

			URL newVersionURL;

			newVersionURL = new URL(TBXPreferences.getInstance().getString(TBXPreferences.UPDATESITE) + version + "/news.version"); //$NON-NLS-1$

			String newsVersionString = IOUtils.getText(newVersionURL, "UTF-8"); //$NON-NLS-1$
			if (newsVersionString == null || newsVersionString.length() == 0) {
				Log.warning(Messages.bind(Messages.noNewVersionAvailableAtP0, newVersionURL));
				return new Version("0"); //$NON-NLS-1$
			}

			return new Version(newsVersionString);
		}
		catch (Exception e) {
			Log.printStackTrace(e);
			return new Version("0"); //$NON-NLS-1$
		}
	}

	/**
	 * 
	 * @return the last known available setup version
	 */
	public static String getLastSetupVersion() {
		try {
			if (!TXMUpdateHandler.isUpdateSiteAvailable()) {
				return "0"; //$NON-NLS-1$
			}

			URL newSetupVersionURL;

			newSetupVersionURL = new URL(TBXPreferences.getInstance().getString(TBXPreferences.RAWFILESSITE) + "software/TXM/new_setup.version"); //$NON-NLS-1$

			String newSetupVersionString = IOUtils.getText(newSetupVersionURL, "UTF-8"); //$NON-NLS-1$
			if (newSetupVersionString == null || newSetupVersionString.length() == 0) {
				Log.warning(Messages.bind(Messages.noSetupVersionAvailableAtP0, newSetupVersionURL));
				return "0"; //$NON-NLS-1$
			}

			return newSetupVersionString;
		}
		catch (Exception e) {
			Log.printStackTrace(e);
			return "0"; //$NON-NLS-1$
		}
	}

	public void refreshNews() {

		vMax = getLastVersion().getMajor();
		if (vCurrentNews > vMax) {
			vCurrentNews = vMax;
		}
		Version v = Activator.getDefault().getBundle().getVersion();
		String version = "" + v.getMajor() + "." + v.getMinor() + "." + v.getMicro(); //$NON-NLS-1$

		stop.setSelection(NewsPreferences.getInstance().getBoolean(NewsPreferences.SHOW_NEWS));
		next.setEnabled(vCurrentNews < vMax);
		previous.setEnabled(vCurrentNews > 1);
		last.setEnabled(vCurrentNews != vMax);

		this.setPartName(Messages.bind(Messages.newsP0, vCurrentNews));

		try {
			if (vCurrentNews == 0) {
				return;
			}
			URL baseURL = new URL(TBXPreferences.getInstance().getString(TBXPreferences.UPDATESITE) + version + "/news_" + vCurrentNews + ".html"); //$NON-NLS-1$

			browser.setText(IOUtils.getText(baseURL, "UTF-8")); //$NON-NLS-1$
		}
		catch (IOException e) {
			Log.warning(Messages.bind(Messages.errorNoNewsFoundForNumeroP0, vCurrentNews));
			Log.printStackTrace(e);
		}
	}

	@Override
	public void setFocus() {
		browser.setFocus();
	}
}
