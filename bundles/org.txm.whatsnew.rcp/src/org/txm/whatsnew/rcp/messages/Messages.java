package org.txm.whatsnew.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Toolbox core messages.
 *
 * @author mdecorde
 *
 */
public class Messages extends NLS {

	private static final String BUNDLE_NAME = Messages.class.getPackage().getName()+".messages"; //$NON-NLS-1$

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	public static String aNewTXMSetupIsAvailable;
	public static String annonce_availability_of_versions;
	public static String current_news_version;
	public static String current_setup_version;
	public static String DoyouWantToDownloadItNow;
	public static String errorWhileFetching;
	public static String errorWhileFetchingNewSetupInfosP0;
	public static String new_version;
	public static String noDontBotherAgain;
	public static String No;
	public static String whatsNew;
	public static String whatsNewSetupStep;
	public static String Yes;
	public static String previousNews;
	public static String nextNews;
	public static String lastNews;
	public static String showNewsAtTXMStartup;
	public static String noNewVersionAvailableAtP0;
	public static String noSetupVersionAvailableAtP0;
	public static String newsP0;
	public static String errorNoNewsFoundForNumeroP0;
}
