package org.txm.whatsnew.rcp;

import java.net.MalformedURLException;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.utils.logger.Log;


public class OpenNews extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		try {
			open();
		}
		catch (MalformedURLException e) {
			Log.warning(e.toString());
			Log.printStackTrace(e);
		}
		return null;
	}

	public static void open() throws MalformedURLException {

		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				try {
					IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
					IWorkbenchPage page = window.getActivePage();

					NewsEditor editor = (NewsEditor) page.openEditor(new NewsEditorInput(), NewsEditor.class.getName(), true);
				}
				catch (Exception e) {
					Log.printStackTrace(e);
				}
			}
		});
	}
}
