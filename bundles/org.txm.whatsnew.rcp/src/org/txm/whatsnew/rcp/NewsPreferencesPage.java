package org.txm.whatsnew.rcp;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.preferences.RCPPreferences;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.whatsnew.rcp.messages.Messages;


public class NewsPreferencesPage extends TXMPreferencePage {

	@Override
	public void init(IWorkbench workbench) {

		this.setPreferenceStore(new TXMPreferenceStore(NewsPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setTitle(Messages.new_version);
		this.setImageDescriptor(IImageKeys.getImageDescriptor("platform:/plugin/org.eclipse.ui.editors/icons/full/obj16/quick_assist_obj.gif")); //$NON-NLS-1$
	}

	@Override
	protected void createFieldEditors() {

		//this.addField(new BooleanFieldEditor(NewsPreferences.SHOW_NEWS, "Show news when TXM starts up", this.getFieldEditorParent()));

		this.addField(new BooleanFieldEditor(NewsPreferences.SHOW_NEW_SETUP, Messages.annonce_availability_of_versions, this.getFieldEditorParent()));
		this.addField(new org.eclipse.jface.preference.StringFieldEditor(NewsPreferences.NEW_SETUP_VERSION, Messages.current_setup_version, this.getFieldEditorParent()));
		// enable if dev needs to update the NEWS_VERSION value
		if (RCPPreferences.getInstance().getBoolean(RCPPreferences.EXPERT_USER)) {
			this.addField(new org.eclipse.jface.preference.StringFieldEditor(NewsPreferences.NEWS_VERSION, Messages.current_news_version, this.getFieldEditorParent()));
		}
	}
}
