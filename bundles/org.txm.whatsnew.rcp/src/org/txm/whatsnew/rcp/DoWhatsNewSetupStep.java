package org.txm.whatsnew.rcp;


import java.net.MalformedURLException;

import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.txm.PostInstallationStep;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.commands.OpenBrowser;
import org.txm.rcp.swt.dialog.MultipleChoiceDialog;
import org.txm.utils.logger.Log;
import org.txm.whatsnew.rcp.messages.Messages;

/**
 * Opens news about new TXM setup
 * 
 * @author mdecorde
 *
 */
public class DoWhatsNewSetupStep extends PostInstallationStep {

	public static String ID = DoWhatsNewSetupStep.class.getName(); //$NON-NLS-1$

	public void install() {

		try {
			String lastNewSetupVersionString = NewsEditor.getLastSetupVersion();

			String currentShownNewSetupVersionString = NewsPreferences.getInstance().getString(NewsPreferences.NEW_SETUP_VERSION);
			if (currentShownNewSetupVersionString.compareTo(lastNewSetupVersionString) > 0) { // the current version should be greater or equal to the last newest version
				currentShownNewSetupVersionString = lastNewSetupVersionString;
			}
			NewsPreferences.getInstance().put(NewsPreferences.NEW_SETUP_VERSION, lastNewSetupVersionString);

			if (NewsPreferences.getInstance().getBoolean(NewsPreferences.SHOW_NEW_SETUP)
					&& lastNewSetupVersionString.compareTo(currentShownNewSetupVersionString) > 0) { // currentShownNewSetupVersion < lastNewsVersion
				open();
			}
		}
		catch (Exception e) {
			Log.warning(NLS.bind(Messages.errorWhileFetchingNewSetupInfosP0, e));
			Log.printStackTrace(e);
		}
	}

	public static final String YES = Messages.Yes;

	public static final String NO = Messages.No;

	public static final String DONTBOTHERAGAIN = Messages.noDontBotherAgain;

	public static void open() throws MalformedURLException {

		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				try {
					MultipleChoiceDialog dialog = new MultipleChoiceDialog(Display.getDefault().getActiveShell(),
							Messages.aNewTXMSetupIsAvailable,
							Messages.DoyouWantToDownloadItNow,
							new String[] { YES, NO, DONTBOTHERAGAIN }, NO);
					dialog.open();

					String rep = dialog.getAnswer();
					if (YES.equals(rep)) {
							String lastNewSetupVersion = NewsEditor.getLastSetupVersion();
							String newSetupVersionURL = TBXPreferences.getInstance().getString(TBXPreferences.FILESSITE) + "software/TXM/" + lastNewSetupVersion; //$NON-NLS-1$
							OpenBrowser.openExternalBrowser(newSetupVersionURL);
					} else if (NO.equals(rep)) {

					} else if (DONTBOTHERAGAIN.equals(rep)) {
							NewsPreferences.getInstance().put(NewsPreferences.SHOW_NEW_SETUP, false);
					}
				}
				catch (Exception e) {
					Log.printStackTrace(e);
				}
			}
		});
	}

	public void preInstall() {

	}

	public DoWhatsNewSetupStep() {
		name = Messages.whatsNewSetupStep;
	}
}
