package org.txm.whatsnew.rcp;


import org.eclipse.osgi.util.NLS;
import org.osgi.framework.Version;
import org.txm.PostInstallationStep;
import org.txm.utils.logger.Log;
import org.txm.whatsnew.rcp.messages.Messages;

/**
 * Opens news about TXM development
 * 
 * @author mdecorde
 *
 */
public class DoWhatsNewStep extends PostInstallationStep {

	public static String ID = DoWhatsNewStep.class.getName(); //$NON-NLS-1$

	/**
	 * will check for news from the txm-sofware git repo
	 */
	public void install() {

		try {
			Version lastNewsVersion = NewsEditor.getLastVersion();

			String currentShownNewsVersionString = NewsPreferences.getInstance().getString(NewsPreferences.NEWS_VERSION);
			Version currentShownNewsVersion = new Version(currentShownNewsVersionString);
			if (currentShownNewsVersion.compareTo(lastNewsVersion) > 0) { // the current version should be greater or equal to the last nest version
				currentShownNewsVersion = lastNewsVersion;
			}
			NewsPreferences.getInstance().put(NewsPreferences.NEWS_VERSION, lastNewsVersion.getMajor());

			if (NewsPreferences.getInstance().getBoolean(NewsPreferences.SHOW_NEWS)
					&& lastNewsVersion.compareTo(currentShownNewsVersion) > 0) { // currentShownNewsVersion < lastNewsVersion
				OpenNews.open();
			}
		}
		catch (Exception e) {
			Log.warning(NLS.bind(Messages.errorWhileFetching, e));
			Log.printStackTrace(e);
		}
	}

	public void preInstall() {

	}

	public DoWhatsNewStep() {
		name = Messages.whatsNew;
	}
}
