// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-05-30 16:02:59 +0200 (Mon, 30 May 2016) $
// $LastChangedRevision: 3220 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.importer.cwb;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.libs.cqp.CQPLibPreferences;
import org.txm.searchengine.cqp.clientExceptions.ServerNotFoundException;
import org.txm.searchengine.cqp.core.messages.CQPSearchEngineCoreMessages;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * This class aims at handling a Cwb process. it uses a java ProcessBuilder
 * 
 * @author Adrien Yepdieu
 */

abstract class CwbProcess {

	/** The debug. */
	protected boolean debug = false;

	/** The process. */
	private Process process;

	/** The name. */
	private String name;

	/**
	 * * Instantiates a new Cwb process.
	 *
	 */
	protected CwbProcess() {
		name = "Cwb_Process"; //$NON-NLS-1$
	}

	/**
	 * Instantiates a new cwb process.
	 *
	 * @param name the name
	 */
	protected CwbProcess(String name) {
		this.name = name;
	}

	/**
	 * Sets the debug.
	 *
	 * @param b the new debug
	 */
	public void setDebug(boolean b) {
		this.debug = b;
	}

	/**
	 * 
	 * @return true if the 'filename' executable file set in preferences is available
	 */
	public static boolean isExecutableAvailable(String filename) {
		File pathToExecutable = new File(CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PATH_TO_CQPLIB), filename);

		if (!pathToExecutable.exists()) {
			Log.severe(TXMCoreMessages.bind("Error: \"{0}\" not found.", pathToExecutable.getAbsolutePath()));
			return false;
		}

		if (!pathToExecutable.canExecute()) {
			Log.severe(TXMCoreMessages.bind("Error: \"{0}\" not executable.", pathToExecutable.getAbsolutePath()));
			return false;
		}
		return true;
	}

	/**
	 * * run the command in cmd.
	 *
	 * @param cmd the cmd to be execute by the process
	 * @param monitorOutput the monitor output
	 * @return true, if successful
	 * @throws ServerNotFoundException the server not found exception
	 * @throws InterruptedException
	 * @throws IOException
	 */
	protected boolean run(List<String> cmd, boolean monitorOutput, boolean waitFor) throws ServerNotFoundException, InterruptedException, IOException {

		Log.fine(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.startingProcessWithCommandP0, StringUtils.join(cmd, " "))); //$NON-NLS-1$ 

		//System.out.println(StringUtils.join(cmd, " "));
		for (String s : cmd) {
			//System.out.println("s="+s);
			if (s == null) {
				Log.severe(TXMCoreMessages.bind("ERROR null arg parameter: {0}.", cmd));
				return false;
			}
		}
		ProcessBuilder processBuilder = new ProcessBuilder(cmd);
		processBuilder.redirectErrorStream(true);
		this.process = processBuilder.start();

		final InputStream is = process.getInputStream();
		new Thread() {

			@Override
			public void run() {
				if (debug) {
					System.out.println(TXMCoreMessages.bind("Starting logging {0} process...", name));
				}
				try {
					InputStreamReader isr = new InputStreamReader(is);
					BufferedReader br = new BufferedReader(isr);
					String line;
					while ((line = br.readLine()) != null) {
						if (debug) {
							System.out.println(line);
						}
					}
					isr.close();
					if (debug) {
						System.out.println(TXMCoreMessages.bind("End of {0} logging process.", name));
					}
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}.start();

		if (waitFor) {
			Log.fine(TXMCoreMessages.bind("Waiting for end of process: {0}...", name)); //$NON-NLS-1$
			endProcess();
		}

		return true;
	}

	public static String getOutputMessages(Process process) throws InterruptedException {
		final InputStream is = process.getInputStream();
		StringBuffer buffer = new StringBuffer();
		Thread t = new Thread() {

			@Override
			public void run() {
				try {
					InputStreamReader isr = new InputStreamReader(is);
					BufferedReader br = new BufferedReader(isr);
					String line;
					while ((line = br.readLine()) != null) {
						buffer.append(line + "\n"); //$NON-NLS-1$
					}
					isr.close();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		t.start();
		t.join();

		return buffer.toString();

	}

	public void endProcess() throws InterruptedException {
		this.process.waitFor();

		int scount = 0;
		do {
			Thread.sleep(10);
			scount++;
		}
		while (isRunning() && scount <= 5);
		this.process.destroy();
	}

	public OutputStream getOutPutStream() {
		if (process == null) return null;
		return process.getOutputStream();
	}

	/**
	 * Stop the CWB process.
	 */
	public void stop() {
		Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.stoppingProcessP0, name.toUpperCase()));
		this.process.destroy();
		Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.P0ProcessStoped, name.toUpperCase()));
	}

	/**
	 * Checks if the process is running.
	 * 
	 * @return true, if is running
	 */
	public boolean isRunning() {
		try {
			if (this.process.exitValue() == 0)
				return true;
			// this throws an IllegalThreadStateException if the process is
			// running
			// The IllegalThreadStateException has not been thrown, the process
			// is not running.
			// false;
			return false;
		}
		catch (IllegalThreadStateException e) {
			// The IllegalThreadStateException has been thrown, all good.
			return true;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		try {
			this.stop();
		}
		finally {
			super.finalize();
		}
	}

	/**
	 * Wait for.
	 *
	 * @return the int
	 * @throws InterruptedException the interrupted exception
	 */
	public int waitFor() throws InterruptedException {
		return this.process.waitFor();
	}

	/**
	 * Gets the error stream.
	 *
	 * @return the error stream
	 */
	public InputStream getErrorStream() {
		return process.getErrorStream();
	}

	/**
	 * Gets the input stream.
	 *
	 * @return the input stream
	 */
	public InputStream getInputStream() {
		return process.getInputStream();
	}

	/**
	 * Gets the output stream.
	 *
	 * @return the output stream
	 */
	public OutputStream getOutputStream() {
		return process.getOutputStream();
	}

	/**
	 * Gets the CWB binaries location.
	 *
	 * @return the cWB location
	 */
	public static String getCWBLocation() {
		String cwbLoc = CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PATH_TO_CQPLIB) + File.separator;

		if (!new File(cwbLoc).exists()) {
			Log.severe(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.cantFindCQPLocationP0, cwbLoc));
			return null;
		}

		return cwbLoc;
	}
}
