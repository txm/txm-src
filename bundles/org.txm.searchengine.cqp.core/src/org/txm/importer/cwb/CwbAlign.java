// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (Tue, 24 Jan 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.importer.cwb;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.txm.libs.cqp.CQPLibPreferences;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * Align a corpus with an other Generated with ProcessBuilderBuilder 28/07/2010.
 */
public class CwbAlign {

	/** The binpath. */
	String binpath = ""; //$NON-NLS-1$

	/**
	 * Instantiates a new cwb align using the executable path set in preferences
	 *
	 */
	public CwbAlign() {
		this.binpath = CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PATH_TO_CQPLIB);
	}


	/**
	 * 
	 * @return if the cwb-makeall executable file set in preferences is available
	 */
	public static boolean isExecutableAvailable() {
		return CwbProcess.isExecutableAvailable("cwb-align" + (System.getProperty("os.name").toLowerCase().contains("windows") ? ".exe" : ""));
	}

	/**
	 * Instantiates a new cwb align.
	 *
	 * @param binpath the binpath
	 */
	public CwbAlign(String binpath) {
		this.binpath = binpath;
	}

	/** The version. */
	String version = "3.2"; //$NON-NLS-1$

	/** The desc. */
	String desc = "Align a corpus with an other"; //$NON-NLS-1$

	/** The debug. */
	boolean debug = false;

	/**
	 * Debug.
	 *
	 * @param b the b
	 */
	public void debug(boolean b) {
		debug = b;
	};

	// use positional attribute [word]
	/** The is p. */
	private Boolean isP = false;

	/** The P. */
	private String P;

	/**
	 * Sets the p.
	 *
	 * @param arg the new p
	 */
	public void setP(String arg) {
		this.isP = true;
		this.P = arg;
	}

	/**
	 * Unset p.
	 */
	public void unsetP() {
		this.isP = false;
	}

	// ordered prealignment
	/** The is s. */
	private Boolean isS = false;

	/** The S. */
	private String S;

	/**
	 * Sets the s.
	 *
	 * @param arg the new s
	 */
	public void setS(String arg) {
		this.isS = true;
		this.S = arg;
	}

	/**
	 * Unset s.
	 */
	public void unsetS() {
		this.isS = false;
	}

	// ordered prealignment with annotations, ex : seg_id
	/** The is v. */
	private Boolean isV = false;

	/** The V. */
	private String V;

	/**
	 * Sets the v.
	 *
	 * @param arg the new v
	 */
	public void setV(String arg) {
		this.isV = true;
		this.V = arg;
	}

	/**
	 * Unset v.
	 */
	public void unsetV() {
		this.isV = false;
	}

	// outfile file [out.align]
	/** The iso. */
	private Boolean iso = false;

	/** The o. */
	private String o;

	/**
	 * Sets the o.
	 *
	 * @param arg the new o
	 */
	public void seto(String arg) {
		this.iso = true;
		this.o = arg;
	}

	/**
	 * Unseto.
	 */
	public void unseto() {
		this.iso = false;
	}

	// set 2:2 alignment split factor [1.2]
	/** The iss. */
	private Boolean iss = false;

	/** The s. */
	private String s;

	/**
	 * Sets the s.
	 *
	 * @param arg the new s
	 */
	public void sets(String arg) {
		this.iss = true;
		this.s = arg;
	}

	/**
	 * Unsets.
	 */
	public void unsets() {
		this.iss = false;
	}

	// use best path search beam of width [50]
	/** The isw. */
	private Boolean isw = false;

	/** The w. */
	private String w;

	/**
	 * Sets the w.
	 *
	 * @param arg the new w
	 */
	public void setw(String arg) {
		this.isw = true;
		this.w = arg;
	}

	/**
	 * Unsetw.
	 */
	public void unsetw() {
		this.isw = false;
	}

	// use registry directory
	/** The isr. */
	private Boolean isr = false;

	/** The r. */
	private String r;

	/**
	 * Sets the r.
	 *
	 * @param arg the new r
	 */
	public void setr(String arg) {
		this.isr = true;
		this.r = arg;
	}

	/**
	 * Unsetr.
	 */
	public void unsetr() {
		this.isr = false;
	}

	// verbose mode
	/** The isv. */
	private Boolean isv = false;

	/**
	 * Setv.
	 */
	public void setv() {
		this.isv = true;
	}

	/**
	 * Unsetv.
	 */
	public void unsetv() {
		this.isv = false;
	}

	// show help
	/** The ish. */
	private Boolean ish = false;

	/**
	 * Seth.
	 */
	public void seth() {
		this.ish = true;
	}

	/**
	 * Unseth.
	 */
	public void unseth() {
		this.ish = false;
	}

	// create align file
	/**
	 * Cwbalign.
	 *
	 * @param source the source
	 * @param target the target
	 * @param sattrib the sattrib
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void cwbalign(String source, String target, String sattrib)
			throws IOException
	// arg : source corpus identif ier
	// arg : target corpus identif ier
	// arg : the aligned structure, ex : seg
	{
		ArrayList<String> args = new ArrayList<String>();
		args.add(binpath + "/cwb-align"); //$NON-NLS-1$
		if (isP) {
			args.add("-P"); //$NON-NLS-1$
			args.add("" + P); //$NON-NLS-1$
		}
		if (isS) {
			args.add("-S"); //$NON-NLS-1$
			args.add("" + S); //$NON-NLS-1$
		}
		if (isV) {
			args.add("-V"); //$NON-NLS-1$
			args.add("" + V); //$NON-NLS-1$
		}
		if (iso) {
			args.add("-o"); //$NON-NLS-1$
			args.add("" + o); //$NON-NLS-1$
		}
		if (iss) {
			args.add("-s"); //$NON-NLS-1$
			args.add("" + s); //$NON-NLS-1$
		}
		if (isw) {
			args.add("-w"); //$NON-NLS-1$
			args.add("" + w); //$NON-NLS-1$
		}
		if (isr) {
			args.add("-r"); //$NON-NLS-1$
			args.add("" + r); //$NON-NLS-1$
		}
		if (isv)
			args.add("-v"); //$NON-NLS-1$
		if (ish)
			args.add("-h"); //$NON-NLS-1$
		args.add("" + source); //$NON-NLS-1$
		args.add("" + target); //$NON-NLS-1$
		args.add("" + sattrib); //$NON-NLS-1$

		ProcessBuilder pb = new ProcessBuilder(args);
		pb.redirectErrorStream(true);
		Process process = null;
		try {
			process = pb.start();
		}
		catch (IOException e) {
			System.err.println(e);
		}
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		int e = 0;
		try {
			e = process.waitFor();
		}
		catch (Exception err) {
		}
		if (e != 0) {
			System.err
					.println("Process exited abnormally with code " + e + " at " + DateFormat.getDateInstance(DateFormat.FULL, Locale.UK).format(new Date())); //$NON-NLS-1$ //$NON-NLS-2$
			for (int c = 0; c < args.size(); c++)
				System.out.print("" + args.get(c) + " "); //$NON-NLS-1$ //$NON-NLS-2$
			System.out.println();
		}
	}

	// write data file to directory
	/** The isd. */
	private Boolean isd = false;

	/** The d. */
	private String d;

	/**
	 * Sets the d.
	 *
	 * @param arg the new d
	 */
	public void setd(String arg) {
		this.isd = true;
		this.d = arg;
	}

	/**
	 * Unsetd.
	 */
	public void unsetd() {
		this.isd = false;
	}

	// write data files to corpus data directory
	/** The is d. */
	private Boolean isD = false;

	/**
	 * Sets the d.
	 */
	public void setD() {
		this.isD = true;
	}

	/**
	 * Unset d.
	 */
	public void unsetD() {
		this.isD = false;
	}

	// compatibility mode (creates .alg file)
	/** The is c. */
	private Boolean isC = false;

	/**
	 * Sets the c.
	 */
	public void setC() {
		this.isC = true;
	}

	/**
	 * Unset c.
	 */
	public void unsetC() {
		this.isC = false;
	}

	// create corpus align data
	/**
	 * Cwbalignencode.
	 *
	 * @param alignmentfile the alignmentfile
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void cwbalignencode(String alignmentfile) throws IOException
	// arg : the align file created by cwb-align
	{
		ArrayList<String> args = new ArrayList<String>();
		args.add(new File(binpath, "cwb-align-encode").getAbsolutePath()); //$NON-NLS-1$
		if (isd) {
			args.add("-d"); //$NON-NLS-1$
			args.add("" + d); //$NON-NLS-1$
		}
		if (isD)
			args.add("-D"); //$NON-NLS-1$
		if (isC)
			args.add("-C"); //$NON-NLS-1$
		if (isr) {
			args.add("-r"); //$NON-NLS-1$
			args.add("" + r); //$NON-NLS-1$
		}
		if (isv)
			args.add("-v"); //$NON-NLS-1$
		if (ish)
			args.add("-h"); //$NON-NLS-1$
		args.add("" + alignmentfile); //$NON-NLS-1$

		ProcessBuilder pb = new ProcessBuilder(args);
		pb.redirectErrorStream(true);
		Process process = null;
		try {
			process = pb.start();
		}
		catch (IOException e) {
			System.err.println(e);
		}
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		int e = 0;
		try {
			e = process.waitFor();
		}
		catch (Exception err) {
		}
		if (e != 0) {
			System.err
					.println("Process exited abnormally with code " + e + " at " + DateFormat.getDateInstance(DateFormat.FULL, Locale.UK).format(new Date())); //$NON-NLS-1$ //$NON-NLS-2$
			for (int c = 0; c < args.size(); c++)
				System.out.print("" + args.get(c) + " "); //$NON-NLS-1$ //$NON-NLS-2$
			System.out.println();
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		// ./cwb-align -V seg_id -r ~/TXM/corpora/tmxtest/registry/
		// tmxen tmxfr seg
		// ./cwb-align-encode -D -r ~/TXM/corpora/tmxtest/registry/
		// -v out.align
		CwbAlign tt = new CwbAlign("~/TXMinstall/cwb/bin/"); //$NON-NLS-1$
		tt.setV("seg_id"); //$NON-NLS-1$
		tt
				.setr(new File("~/TXM/corpora/tmxtest/registry/").getAbsolutePath()); //$NON-NLS-1$
		try {
			tt.cwbalign("tmxen", "tmxfr", "seg"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		catch (IOException e) {
			Log.severe("Error while calling cwb-align: " + e.getLocalizedMessage());
			return;
		}

		tt = new CwbAlign("~/TXMinstall/cwb/bin/"); //$NON-NLS-1$
		tt.setD();
		tt.setv();
		tt
				.setr(new File("~/TXM/corpora/tmxtest/registry/").getAbsolutePath()); //$NON-NLS-1$
		try {
			tt.cwbalignencode("out.align"); //$NON-NLS-1$
		}
		catch (IOException e) {
			Log.severe("Error while calling cwb-align: " + e.getLocalizedMessage());
			return;
		}
	}
}
