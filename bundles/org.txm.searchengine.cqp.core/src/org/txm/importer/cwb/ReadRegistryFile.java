package org.txm.importer.cwb;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.txm.utils.io.IOUtils;

/**
 * Read a registry file a retrive the declared p attributes and s attributes informations.
 * 
 * Call constructor then use : getPattributes and getSattributes for cwb-encode
 * 
 * or use getSattributesMap, getSattributeProfs and getAnatypes() to get the declared attributes
 * 
 * @author mdecorde
 *
 */
public class ReadRegistryFile {

	File registryFile, dataDirectory;

	String id;

	ArrayList<String> pAttributes;

	ArrayList<String> sAttributes;

	HashMap<String, HashSet<String>> sattrs;

	HashMap<String, Integer> sattrsProfs;

	public ReadRegistryFile(File registryFile) {
		this.registryFile = registryFile;
		read();
	}

	/*
	 * reload the informations
	 */
	public void read() {
		pAttributes = new ArrayList<>();
		sAttributes = new ArrayList<>();
		sattrs = new HashMap<>();
		sattrsProfs = new HashMap<>();

		for (String line : IOUtils.getLines(registryFile, System.getProperty("file.encoding"))) { //$NON-NLS-1$
			line = line.trim(); // remove first tab

			if (line.startsWith("HOME ")) { //$NON-NLS-1$
				line = line.substring(5); // remove 'ATTRIBUTE '
				if (line.startsWith("\"") && line.endsWith("\"")) { //$NON-NLS-1$ //$NON-NLS-2$
					line = line.substring(1, line.length() - 1);
				}
				dataDirectory = new File(line);
			}
			else if (line.startsWith("ID ")) { //$NON-NLS-1$
				line = line.substring(3); // remove 'ATTRIBUTE '
				id = line;
			}
			else if (line.startsWith("ATTRIBUTE ")) { //$NON-NLS-1$
				line = line.substring(10); // remove 'ATTRIBUTE '
				pAttributes.add(line);
			}
			else if (line.startsWith("STRUCTURE ")) { //$NON-NLS-1$
				line = line.substring(9); // remove 'STRUCTURE '
				line = line.replaceAll("\\#.*", ""); //$NON-NLS-1$ //$NON-NLS-2$
				line = line.trim();
				String[] split = line.split("_", 2); //$NON-NLS-1$
				String sname = split[0];
				// println split
				if (split.length == 1) { // sattr decl
					if (sname.matches(".+[1-9]") && sattrs.containsKey(sname.substring(0, sname.length() - 1))) { // recursive structure //$NON-NLS-1$
						sname = sname.substring(0, sname.length() - 1);
						sattrsProfs.put(sname, sattrsProfs.get(sname) + 1);
					}
					else {
						sattrs.put(sname, new HashSet<String>());
						sattrsProfs.put(sname, 0);
					}
				}
				else {
					String satt = split[1];
					if (satt.matches(".+[1-9]") && sattrs.get(sname).contains(satt.substring(0, satt.length() - 1))) { //$NON-NLS-1$
						// recursive attribute -> to be ignored
					}
					else {
						sattrs.get(sname).add(satt);
					}
				}
			}
		}

		for (String sattr : sattrs.keySet()) {
			String tmp = "" + sattr + ":" + sattrsProfs.get(sattr); //$NON-NLS-1$ //$NON-NLS-2$
			for (String attr : sattrs.get(sattr)) {
				tmp += "+" + attr; //$NON-NLS-1$
			}
			sAttributes.add(tmp);
		}
	}

	/**
	 * 
	 * @return the value of the ID field
	 */
	public String getID() {
		return id;
	}

	/**
	 * 
	 * @return the data directory pointed by the value of the HOME field
	 */
	public File getDataDirectory() {
		return dataDirectory;
	}

	/**
	 * Test the CQP index files of each p-attribute and s-attribute properties
	 * 
	 * @param dataDirectory the directory where the binary files should be found
	 * @return true if all CQP files are present
	 */
	public ArrayList<String> isCorpusBuildValid(File dataDirectory) {
		if (pAttributes == null) {
			read();
		}

		ArrayList<String> errors = new ArrayList<>();

		// test p-attributes
		String[] exts = { ".lexicon", ".corpus.cnt", ".lexicon.idx", ".lexicon.srt" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		for (String p : pAttributes) {
			for (String ext : exts) {
				File f = new File(dataDirectory, p + ext);
				if (!f.exists()) {
					// System.out.println("MISSING: " + f.exists() + " " + f.getAbsolutePath());
					errors.add(f.getName());
				}
			}
		}

		// test p-attributes with optional compression files
		String[] extsCompressed = { ".corpus	.hcd	.huf	.huf.syn", ".corpus.rdx	.crc	.crx", ".corpus.rev	.crc	.crx" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		for (String p : pAttributes) {
			for (String extsTabulated : extsCompressed) {
				String[] exts2 = extsTabulated.split("\t"); //$NON-NLS-1$
				String ext = exts2[0];
				File f = new File(dataDirectory, p + ext);
				if (!f.exists()) {
					// System.out.println("MISSING: " + f.exists() + " " + f.getAbsolutePath());
					for (int i = 1; i < exts2.length; i++) {
						f = new File(dataDirectory, p + exts2[i]);
						if (!f.exists()) {
							errors.add(f.getName());
						}
					}
				}
			}
		}

		String[] sexts = { ".rng" }; //$NON-NLS-1$
		String[] spexts = { ".avs", ".avx", ".rng" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		for (String s : sattrs.keySet()) {
			for (String ext : sexts) {
				File f = new File(dataDirectory, s + ext);
				if (!f.exists()) {
					// System.out.println("MISSING: " + f.exists() + " " + f.getAbsolutePath());
					errors.add(f.getName());
				}
			}

			for (String sp : sattrs.get(s)) {
				for (String ext : spexts) {
					File f = new File(dataDirectory, s + "_" + sp + ext); //$NON-NLS-1$
					if (!f.exists()) {
						// System.out.println("MISSING: " + f.exists() + " " + f.getAbsolutePath());
						errors.add(f.getName());
					}
				}
			}
		}
		return errors;
	}

	/**
	 * 
	 * @return the cwb-encode arguments for p attributes
	 */
	public ArrayList<String> getPAttributes() {
		return pAttributes;
	}

	/**
	 * 
	 * @return the cwb-encode arguments for s attributes
	 */
	public ArrayList<String> getSAttributes() {
		return sAttributes;
	}

	/**
	 * 
	 * @return the attributes of the structures
	 */
	public HashMap<String, HashSet<String>> getSAttributesMap() {
		return sattrs;
	}

	/**
	 * 
	 * @return the recursive level of the structures
	 */
	public HashMap<String, Integer> getSAttributesProfs() {
		return sattrsProfs;
	}

	public static void main(String[] args) {
		File registry = new File(System.getProperty("user.home"), "runtime-rcpapplication.product/corpora/VOEUX/registry/voeux"); //$NON-NLS-1$ //$NON-NLS-2$
		File data = new File(System.getProperty("user.home"), "runtime-rcpapplication.product/corpora/VOEUX/data/VOEUX"); //$NON-NLS-1$ //$NON-NLS-2$
		ReadRegistryFile reader = new ReadRegistryFile(registry);
		System.out.println("pAttributes: " + reader.getPAttributes()); //$NON-NLS-1$
		System.out.println("sAttributes Map: " + reader.getSAttributesMap()); //$NON-NLS-1$
		System.out.println("sAttributes: " + reader.getSAttributes()); //$NON-NLS-1$

		System.out.println("Validation: " + reader.isCorpusBuildValid(data)); //$NON-NLS-1$
	}
}
