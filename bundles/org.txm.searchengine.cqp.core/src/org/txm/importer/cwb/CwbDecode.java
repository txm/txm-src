// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.importer.cwb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

// Decode CWB indexes
/**
 * The Class CwbDecode.
 */
public class CwbDecode {

	/** The binpath. */
	String binpath = "";

	/**
	 * Instantiates a new cwb decode.
	 *
	 * @param binpath the binpath
	 */
	public CwbDecode(String binpath) {
		this.binpath = binpath;
	}

	/** The version. */
	String version = "0.0.0"; //$NON-NLS-1$

	/** The desc. */
	String desc = "Decode CWB indexes"; //$NON-NLS-1$

	/** The debug. */
	boolean debug = false;

	/**
	 * Debug.
	 *
	 * @param b the b
	 */
	public void debug(boolean b) {
		debug = b;
	};

	// Lisp output mode

	/** The is l. */
	private Boolean isL = false;

	/**
	 * Sets the l.
	 */
	public void setL() {
		this.isL = true;
	}

	/**
	 * Unset l.
	 */
	public void unsetL() {
		this.isL = false;
	}

	// concordance line ('horizonta'l) output mode

	/** The is h. */
	private Boolean isH = false;

	/**
	 * Sets the h.
	 */
	public void setH() {
		this.isH = true;
	}

	/**
	 * Unset h.
	 */
	public void unsetH() {
		this.isH = false;
	}

	// compact output mode (suitable for encode)

	/** The is c. */
	private Boolean isC = false;

	/**
	 * Sets the c.
	 */
	public void setC() {
		this.isC = true;
	}

	/**
	 * Unset c.
	 */
	public void unsetC() {
		this.isC = false;
	}

	// XML-compatible compact output (for {encode -x ...}))

	/** The is cx. */
	private Boolean isCx = false;

	/**
	 * Sets the cx.
	 */
	public void setCx() {
		this.isCx = true;
	}

	/**
	 * Unset cx.
	 */
	public void unsetCx() {
		this.isCx = false;
	}

	// XML output mode

	/** The is x. */
	private Boolean isX = false;

	/**
	 * Sets the x.
	 */
	public void setX() {
		this.isX = true;
	}

	/**
	 * Unset x.
	 */
	public void unsetX() {
		this.isX = false;
	}

	// show corpus position ('numbers')

	/** The isn. */
	private Boolean isn = false;

	/**
	 * Setn.
	 */
	public void setn() {
		this.isn = true;
	}

	/**
	 * Unsetn.
	 */
	public void unsetn() {
		this.isn = false;
	}

	// first token to print (at corpus position {n})

	/** The iss. */
	private Boolean iss = false;

	/** The s. */
	private int s;

	/**
	 * Sets the s.
	 *
	 * @param arg the new s
	 */
	public void sets(int arg) {
		this.iss = true;
		this.s = arg;
	}

	/**
	 * Unsets.
	 */
	public void unsets() {
		this.iss = false;
	}

	// last token to print (at corpus position {n})

	/** The ise. */
	private Boolean ise = false;

	/** The e. */
	private int e;

	/**
	 * Sets the e.
	 *
	 * @param arg the new e
	 */
	public void sete(int arg) {
		this.ise = true;
		this.e = arg;
	}

	/**
	 * Unsete.
	 */
	public void unsete() {
		this.ise = false;
	}

	// set registry directory

	/** The isr. */
	private Boolean isr = false;

	/** The r. */
	private String r;

	/**
	 * Sets the r.
	 *
	 * @param arg the new r
	 */
	public void setr(String arg) {
		this.isr = true;
		this.r = arg;
	}

	/**
	 * Unsetr.
	 */
	public void unsetr() {
		this.isr = false;
	}

	// matchlist mode (input from stdin)

	/** The isp. */
	private Boolean isp = false;

	/**
	 * Setp.
	 */
	public void setp() {
		this.isp = true;
	}

	/**
	 * Unsetp.
	 */
	public void unsetp() {
		this.isp = false;
	}

	// matchlist mode (input from {file})

	/** The isf. */
	private Boolean isf = false;

	/** The f. */
	private String f;

	/**
	 * Sets the f.
	 *
	 * @param arg the new f
	 */
	public void setf(String arg) {
		this.isf = true;
		this.f = arg;
	}

	/**
	 * Unsetf.
	 */
	public void unsetf() {
		this.isf = false;
	}

	// this help page

	/** The ish. */
	private Boolean ish = false;

	/**
	 * Seth.
	 */
	public void seth() {
		this.ish = true;
	}

	/**
	 * Unseth.
	 */
	public void unseth() {
		this.ish = false;
	}

	// print p-attribute {att}

	/** The is p. */
	private Boolean isP = false;

	/** The P. */
	private List<String> P;

	/**
	 * Sets the p.
	 *
	 * @param arg the new p
	 */
	public void setP(List<String> arg) {
		this.isP = true;
		this.P = arg;
	}

	/**
	 * Unset p.
	 */
	public void unsetP() {
		this.isP = false;
	}

	// print s-attribute {att} (possibly including annotations)

	/** The is s. */
	private Boolean isS = false;

	/** The S. */
	private List<String> S;

	/**
	 * Sets the s.
	 *
	 * @param arg the new s
	 */
	public void setS(List<String> arg) {
		this.isS = true;
		this.S = arg;
	}

	/**
	 * Unset s.
	 */
	public void unsetS() {
		this.isS = false;
	}

	// show s-attribute annotation for each range in matchlist mode

	/** The is v. */
	private Boolean isV = false;

	/** The V. */
	private List<String> V;

	/**
	 * Sets the v.
	 *
	 * @param arg the new v
	 */
	public void setV(List<String> arg) {
		this.isV = true;
		this.V = arg;
	}

	/**
	 * Unset v.
	 */
	public void unsetV() {
		this.isV = false;
	}

	// print alignment attribute {att}

	/** The is a. */
	private Boolean isA = false;

	/** The A. */
	private List<String> A;

	/**
	 * Sets the a.
	 *
	 * @param arg the new a
	 */
	public void setA(List<String> arg) {
		this.isA = true;
		this.A = arg;
	}

	/**
	 * Unset a.
	 */
	public void unsetA() {
		this.isA = false;
	}

	// print all p-attributes and s-attributes

	/** The is all. */
	private Boolean isALL = false;

	/**
	 * Sets the all.
	 */
	public void setALL() {
		this.isALL = true;
	}

	/**
	 * Unset all.
	 */
	public void unsetALL() {
		this.isALL = false;
	}

	// expand ranges to full {att} region (matchlist mode)

	/** The isc. */
	private Boolean isc = false;

	/** The c. */
	private String c;

	/**
	 * Sets the c.
	 *
	 * @param arg the new c
	 */
	public void setc(String arg) {
		this.isc = true;
		this.c = arg;
	}

	/**
	 * Unsetc.
	 */
	public void unsetc() {
		this.isc = false;
	}

	String[] options;

	public void setAdditionalOptions(String[] options) {
		this.options = options;
	}

	/**
	 * Cwbdecode.
	 *
	 * @param corpus the corpus
	 */
	public void cwbdecode(String corpus) throws IOException
	// arg : corpus name
	{
		ArrayList<String> args = new ArrayList<String>();
		if (System.getProperty("os.name").contains("Windows")) { //$NON-NLS-1$ //$NON-NLS-2$
			args.add(binpath + "cwb-decode.exe"); //$NON-NLS-1$
		}
		else {
			args.add(binpath + "cwb-decode"); //$NON-NLS-1$
		}
		if (isL)
			args.add("-L"); //$NON-NLS-1$
		if (isH)
			args.add("-H"); //$NON-NLS-1$
		if (isC)
			args.add("-C"); //$NON-NLS-1$
		if (isCx)
			args.add("-Cx"); //$NON-NLS-1$
		if (isX)
			args.add("-X"); //$NON-NLS-1$
		if (isn)
			args.add("-n"); //$NON-NLS-1$
		if (iss) {
			args.add("-s"); //$NON-NLS-1$
			args.add("" + s); //$NON-NLS-1$
		}
		if (ise) {
			args.add("-e"); //$NON-NLS-1$
			args.add("" + e); //$NON-NLS-1$
		}
		if (isr) {
			args.add("-r"); //$NON-NLS-1$
			args.add("" + r); //$NON-NLS-1$
		}
		if (isp)
			args.add("-p"); //$NON-NLS-1$
		if (isf) {
			args.add("-f"); //$NON-NLS-1$
			args.add("" + f); //$NON-NLS-1$
		}
		args.add("" + corpus); //$NON-NLS-1$
		if (ish)
			args.add("-h"); //$NON-NLS-1$
		if (isP)
			for (int c = 0; c < P.size(); c++) {
				args.add("-P"); //$NON-NLS-1$
				args.add("" + P.get(c)); //$NON-NLS-1$
			}
		if (isS)
			for (int c = 0; c < S.size(); c++) {
				args.add("-S"); //$NON-NLS-1$
				args.add("" + S.get(c)); //$NON-NLS-1$
			}
		if (isV)
			for (int c = 0; c < V.size(); c++) {
				args.add("-V"); //$NON-NLS-1$
				args.add("" + V.get(c)); //$NON-NLS-1$
			}
		if (isA)
			for (int c = 0; c < A.size(); c++) {
				args.add("-A"); //$NON-NLS-1$
				args.add("" + A.get(c)); //$NON-NLS-1$
			}
		if (isALL)
			args.add("-ALL"); //$NON-NLS-1$
		if (isc) {
			args.add("-c"); //$NON-NLS-1$
			args.add("" + c); //$NON-NLS-1$
		}

		if (options != null) {
			for (String opt : options) {
				args.add(opt);
			}
		}

		ProcessBuilder pb = new ProcessBuilder(args);
		pb.redirectErrorStream(false);
		Process process = null;
		try {
			process = pb.start();
		}
		catch (IOException e) {
			System.err.println(e);
		}
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		int e = 0;
		try {
			e = process.waitFor();
		}
		catch (Exception err) {
		}
		if (e != 0) {
			System.err.println("Process exited abnormally with code " //$NON-NLS-1$
					+ e
					+ " at " //$NON-NLS-1$
					+ DateFormat.getDateInstance(DateFormat.FULL, Locale.UK)
							.format(new Date()));

			for (int c = 0; c < args.size(); c++)
				System.out.print(args.get(c) + " "); //$NON-NLS-1$
			System.out.println();
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		CwbDecode tt = new CwbDecode(""); //$NON-NLS-1$
	}
}
