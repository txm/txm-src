package org.txm.importer.cwb;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.txm.utils.io.IOUtils;

/**
 * Remove milestone declarations and index files. Milestones are empty structures that cannot be used with CQL
 * 
 * @author mdecorde
 *
 */
public class FixMilestoneDeclarations {

	File registryFile;

	File dataDirectory;

	boolean debug = false;

	public static final String[] exts = { ".avs", ".avx", ".rng" }; //$NON-NLS-1$

	public FixMilestoneDeclarations(File registryFile, File dataDirectory) {
		this.registryFile = registryFile;
		this.dataDirectory = dataDirectory;
	}


	public boolean process() throws IOException {
		if (!dataDirectory.exists()) return false;
		if (!registryFile.exists()) return false;

		// get structural units and their properties
		ArrayList<String> structures = new ArrayList<String>();
		HashMap<String, ArrayList<String>> structureAttributes = new HashMap<String, ArrayList<String>>();
		BufferedReader reader = new BufferedReader(new FileReader(registryFile));
		String line = reader.readLine();
		String structure = ""; //$NON-NLS-1$
		while (line != null) {

			if (line.startsWith("STRUCTURE ")) { //$NON-NLS-1$
				if (!line.contains("_")) { //$NON-NLS-1$
					structure = line.substring(10);
					structures.add(structure);
					structureAttributes.put(structure, new ArrayList<String>());
				}
				else {
					line = line.replaceAll("[ ]+# \\[annotations\\]", ""); //$NON-NLS-1$
					structureAttributes.get(structure).add(line.substring(10));
				}

			}
			line = reader.readLine();
		}
		reader.close();
		if (debug) System.out.println("Structures: " + structures); //$NON-NLS-1$

		File[] files = dataDirectory.listFiles(IOUtils.HIDDENFILE_FILTER);
		if (files == null) return false;

		//removing empty data files
		ArrayList<String> linesToRemove = new ArrayList<String>();
		for (String s : structures) {
			File f = new File(dataDirectory, s + ".rng"); //$NON-NLS-1$

			if (f.exists() && f.length() == 0) {
				if (debug) System.out.println("Milestone detected : " + s); //$NON-NLS-1$
				f.delete();
				linesToRemove.add(s);

				if (debug) System.out.println("Removing files: " + structureAttributes.get(s)); //$NON-NLS-1$
				for (String attribute : structureAttributes.get(s)) {
					linesToRemove.add(attribute);
					for (String ext : exts) {
						new File(dataDirectory, attribute + ext).delete();
					}
				}
				continue;
			}

			//			if (!f.exists()) {
			//				if (debug) System.out.println("Structure "+s+" RNG file is missing: "+f);
			//				linesToRemove.add(s);
			//			}
		}

		// remove structure declarations
		if (debug) System.out.println("Attributes to remove from registry files: " + linesToRemove); //$NON-NLS-1$
		reader = new BufferedReader(new FileReader(registryFile));
		File registryFile_tmp = new File(registryFile.getAbsolutePath() + ".tmp"); //$NON-NLS-1$
		BufferedWriter writer = new BufferedWriter(new FileWriter(registryFile_tmp));
		line = reader.readLine();
		while (line != null) {

			if (line.startsWith("STRUCTURE ")) { //$NON-NLS-1$

				structure = line.substring(10).replaceAll("[ ]+# \\[annotations\\]", ""); //$NON-NLS-1$
				if (linesToRemove.contains(structure)) {
					if (debug) System.out.println("Remove line: " + line); //$NON-NLS-1$
					// don't write attribute line
				}
				else {
					writer.write(line + "\n"); //$NON-NLS-1$
				}
			}
			else {
				writer.write(line + "\n"); //$NON-NLS-1$
			}
			line = reader.readLine();
		}
		reader.close();
		writer.close();

		return (registryFile.delete() && registryFile_tmp.renameTo(registryFile));
	}

	public static void main(String args[]) throws IOException {
		File registryFile = new File("TXM/corpora/BAIP/registry/baip"); //$NON-NLS-1$
		File dataDirectory = new File("TXM/corpora/BAIP/data/BAIP"); //$NON-NLS-1$
		FixMilestoneDeclarations fm = new FixMilestoneDeclarations(registryFile, dataDirectory);
		System.out.println("Result: " + fm.process()); //$NON-NLS-1$
	}
}
