// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-05-24 10:43:03 +0200 (Tue, 24 May 2016) $
// $LastChangedRevision: 3216 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.importer.cwb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.searchengine.cqp.core.messages.CQPSearchEngineCoreMessages;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

/**
 * The Class PatchCwbRegistry.
 */
public class PatchCwbRegistry {

	/**
	 * Patch.
	 *
	 * @param registryfile the file to patch
	 * @param datadir the directory containing the cwb index
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void patch(File registryfile, File datadir)
			throws IOException {
		//String corpus = registryfile.getName();

		// HOME INFO
		String datadirPath = datadir.getAbsolutePath();
		File temp = File.createTempFile("temp", ".registry", registryfile.getParentFile()); //$NON-NLS-1$ //$NON-NLS-2$

		// fix UNC paths for mingw
		if (datadirPath.startsWith("\\\\")) { //$NON-NLS-1$
			datadirPath = datadirPath.replace("\\\\", "//"); //$NON-NLS-1$
		}

		String encoding = IOUtils.UTF8;
		//if (OSDetector.isFamilyWindows()) encoding = "ISO-8859-1"; //$NON-NLS-1$
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(registryfile), encoding));
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(temp), encoding);

		String line = reader.readLine();
		while (line != null) {
			//System.out.println("LINE: "+line);
			if (line.startsWith("HOME")) //$NON-NLS-1$
				writer.write("HOME \"" + datadirPath + "\"\n"); //$NON-NLS-1$ //$NON-NLS-2$
			else if (line.startsWith("INFO")) //$NON-NLS-1$
				writer.write("INFO \"" + datadirPath + "/.info\"\n"); //$NON-NLS-1$ //$NON-NLS-2$
			else
				writer.write(line + "\n"); //$NON-NLS-1$
			line = reader.readLine();
		}
		writer.close();
		reader.close();

		registryfile.delete();

		//		System.out.println("TMP file: "+temp);
		//		System.out.println("TMP file exists?: "+temp.exists());
		//		System.out.println("REG file exists?: "+registryfile.exists());
		if (registryfile.exists()) {
			System.err.println(TXMCoreMessages.bind("ERROR: old registry path still exists. Result in {0}.", temp));
		}
		else {
			temp.renameTo(registryfile);
		}

		//		System.out.println("TMP file exists?: "+temp.exists());
		//		System.out.println("REG file exists?: "+registryfile.exists());
	}

	/**
	 * Change the encoding value.
	 *
	 * @param registryfile the registry file to patch
	 * @param corpusEncoding the new encoding value
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void patchEncoding(File registryfile, String corpusEncoding) throws IOException {
		String corpus = registryfile.getName();

		// HOME INFO
		File temp = File.createTempFile("temp", ".regsitry", registryfile.getParentFile()); //$NON-NLS-1$ //$NON-NLS-2$

		String encoding = IOUtils.UTF8;
		//if (OSDetector.isFamilyWindows()) encoding = "ISO-8859-1"; //$NON-NLS-1$
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(registryfile), encoding));
		OutputStreamWriter writer = new OutputStreamWriter(
				new FileOutputStream(temp), encoding);

		if (corpusEncoding.startsWith("ISO") || corpusEncoding.startsWith("iso")) { //$NON-NLS-1$ //$NON-NLS-2$
			corpusEncoding = "latin" + corpusEncoding.substring(9);//remove iso-8859- //$NON-NLS-1$
		}
		corpusEncoding = corpusEncoding.toLowerCase();

		Log.fine(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.updating, corpus, corpusEncoding));

		String line = reader.readLine();
		while (line != null) {
			if (line.startsWith("##:: charset") || line.startsWith("charset")) { //$NON-NLS-1$ //$NON-NLS-2$
				writer.write("charset = \"" + corpusEncoding + "\" # change if your corpus uses different charset : latin1, latin2 ... utf-8\n"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			else {
				writer.write(line + "\n"); //$NON-NLS-1$
			}
			line = reader.readLine();
		}
		writer.close();
		reader.close();
		registryfile.delete();
		temp.renameTo(registryfile);
	}

	/**
	 * Changes the language value.
	 *
	 * @param registryfile the registry file to patch
	 * @param corpusLanguage the new language value
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void patchLanguage(File registryfile, String corpusLanguage) throws IOException {

		String corpus = registryfile.getName();

		// HOME INFO
		File temp = File.createTempFile("temp", ".regsitry", registryfile.getParentFile()); //$NON-NLS-1$ //$NON-NLS-2$

		String encoding = "UTF-8"; //$NON-NLS-1$
		//if (OSDetector.isFamilyWindows()) encoding = "ISO-8859-1"; //$NON-NLS-1$
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(registryfile), encoding));
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(temp), encoding);

		corpusLanguage = corpusLanguage.toLowerCase();

		Log.fine(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.updatingP0CorpusLanguageToP1, corpus, corpusLanguage));

		String line = reader.readLine();
		while (line != null) {
			if (line.startsWith("##:: language") || line.startsWith("language")) { //$NON-NLS-1$ //$NON-NLS-2$
				writer.write("language = \"" + corpusLanguage + "\" # insert ISO code for language (de, en, fr, ...)\n"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			else {
				writer.write(line + "\n"); //$NON-NLS-1$
			}
			line = reader.readLine();
		}
		writer.close();
		reader.close();
		registryfile.delete();
		temp.renameTo(registryfile);
	}

	/**
	 * Add the ALIGNED {@code <corpus>} line to a registry file.
	 *
	 * @param registryfile the registry file to patch
	 * @param targetcorpus the aligned corpus
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static boolean patchAlignment(File registryfile, String targetcorpus) throws IOException {

		targetcorpus = targetcorpus.toLowerCase();
		String corpus = registryfile.getName();

		if (!new File(registryfile.getParent(), targetcorpus).exists()) {
			Log.severe(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.theP0TargetCorpusDoesNotExist, new File(registryfile.getParent(), targetcorpus)));
			return false;
		}
		if (!registryfile.exists()) {
			Log.severe(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.sourceRegistryDoesNotExistsP0, registryfile.getAbsolutePath()));
			return false;
		}

		boolean alignmentwritten = false;
		// HOME INFO
		File temp = File.createTempFile("temp", ".registry", registryfile.getParentFile()); //$NON-NLS-1$ //$NON-NLS-2$

		String encoding = "UTF-8"; //$NON-NLS-1$
		//if (OSDetector.isFamilyWindows()) encoding = "ISO-8859-1"; //$NON-NLS-1$
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(registryfile), encoding));
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(temp), encoding);

		Log.fine(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.addingAlignmentAttributeToCorpusP0RegistryColonP1, corpus, targetcorpus));

		String line = reader.readLine();
		while (line != null) {
			if (line.startsWith("ALIGNED " + targetcorpus)) { //$NON-NLS-1$
				alignmentwritten = true;
				writer.write(line + "\n"); //$NON-NLS-1$
			}
			else if (line.startsWith("# Yours sincerely, the Encode tool.")) { //$NON-NLS-1$
				if (!alignmentwritten) {
					writer.write("ALIGNED " + targetcorpus + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
					alignmentwritten = true;
				}
				writer.write("# Yours sincerely, the Encode tool."); //$NON-NLS-1$
			}
			else {
				writer.write(line + "\n"); //$NON-NLS-1$
			}
			line = reader.readLine();
		}

		writer.close();
		reader.close();
		return registryfile.delete() && temp.renameTo(registryfile);
	}

	/**
	 * Add the ALIGNED {@code <corpus>} line to a registry file.
	 *
	 * @param registryfile the registry file to patch
	 * @param oldname the oldname
	 * @param newname the newname
	 * @return true, if successful
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static boolean patchAttribute(File registryfile, String oldname, String newname) throws IOException {

		boolean found = false;
		String datapath = ""; //$NON-NLS-1$

		if (!registryfile.exists()) {
			Log.fine(CQPSearchEngineCoreMessages.cantFindRegistryFile);
			return false;
		}

		// HOME INFO
		File temp = File.createTempFile("temp", ".regsitry", registryfile.getParentFile()); //$NON-NLS-1$ //$NON-NLS-2$

		String encoding = IOUtils.UTF8;
		//if (OSDetector.isFamilyWindows()) encoding = "ISO-8859-1"; //$NON-NLS-1$
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(registryfile), encoding));
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(temp), encoding);

		String line = reader.readLine();
		while (line != null) {
			if (line.startsWith("ATTRIBUTE " + oldname)) { //$NON-NLS-1$
				writer.write("ATTRIBUTE " + newname + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
				found = true;
			}
			else {
				if (line.startsWith("HOME")) { //$NON-NLS-1$
					datapath = line.substring(5);
				}
				writer.write(line + "\n"); //$NON-NLS-1$
			}
			line = reader.readLine();
		}

		if (found && datapath.length() > 0) {
			System.out.println("rename " + oldname + ".* files to " + newname + ".*"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			File datadir = new File(datapath);
			if (datadir.exists()) {
				for (File f : datadir.listFiles(IOUtils.HIDDENFILE_FILTER))
					if (f.getName().startsWith(oldname + ".")) //$NON-NLS-1$
						f.renameTo(new File(f.getParent(), f.getName().replace(oldname + ".", newname + "."))); //$NON-NLS-1$ //$NON-NLS-2$
			}
			else {
				System.out.println(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.dataDirectory, datadir));
				writer.close();
				reader.close();
				return false;
			}
		}
		else {
			Log.severe(CQPSearchEngineCoreMessages.couldNotPatchTheRegistryFile);

			if (!found || !(datapath.length() > 0)) {
				Log.severe(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.P0couldNotBeFoundInRegistryFileP1, oldname, registryfile));
			}
			writer.close();
			reader.close();
			return false;
		}


		writer.close();
		reader.close();
		registryfile.delete();
		temp.renameTo(registryfile);
		return true;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		try {
			/*
			 * PatchCwbRegistry.patchEncoding(new File(
			 * "~/TXM/corpora/tmxtest/registry/tmxen"), "utf-8"); //$NON-NLS-1$ //$NON-NLS-2$
			 * PatchCwbRegistry.patchLanguage(new File(
			 * "~/TXM/corpora/tmxtest/registry/tmxen"), "en"); //$NON-NLS-1$ //$NON-NLS-2$
			 * PatchCwbRegistry.patchAlignment(new File(
			 * "~/TXM/corpora/tmxtest/registry/tmxen"), "TMXFR"); //$NON-NLS-1$ //$NON-NLS-2$
			 */
			File registry = new File("/home/mdecorde/TXM/corpora/VOEUX/registry/voeux"); //$NON-NLS-1$
			File data = new File("/home/mdecorde/TXM/corpora/VOEUX/data/VOEUX"); //$NON-NLS-1$
			PatchCwbRegistry.patch(registry, data);
			//			PatchCwbRegistry.patchAttribute(registry, "word", "form"); //$NON-NLS-1$ //$NON-NLS-2$
			//			try {
			//				new CwbMakeAll().run("/usr/lib/TXM/cwb/bin/cwb-makeall", "lasla", registry.getAbsolutePath()); //$NON-NLS-1$ //$NON-NLS-2$
			//			} catch (Exception e) {
			//				// TODO Auto-generated catch block
			//				org.txm.utils.logger.Log.printStackTrace(e);
			//			}
		}
		catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}
}
