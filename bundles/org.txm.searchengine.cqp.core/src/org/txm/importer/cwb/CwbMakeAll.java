// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-05-26 17:42:36 +0200 (Thu, 26 May 2016) $
// $LastChangedRevision: 3219 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.importer.cwb;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.txm.libs.cqp.CQPLibPreferences;
import org.txm.searchengine.cqp.clientExceptions.ServerNotFoundException;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * This class aims at handling a Cwb Make All.
 * 
 * @author Adrien Yepdieu
 */
public class CwbMakeAll extends CwbProcess {

	/**
	 * Instantiates a new cwb make all.
	 */
	public CwbMakeAll() {
		super("cwb-makeall"); //$NON-NLS-1$
	}

	/**
	 * Instantiates a new cwb make all.
	 *
	 * @param name the name
	 */
	public CwbMakeAll(String name) {
		super(name);
	}

	/**
	 * * Instantiates a new Cwb Make All.
	 *
	 * @param pathToExecutable the path to the executable
	 * @param corpusName the corpus name
	 * @param pathToRegistry the path to the registry
	 * @return true, if successful
	 * @throws ServerNotFoundException the server not found exception
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public boolean run(String pathToExecutable, String corpusName,
			String pathToRegistry) throws ServerNotFoundException, InterruptedException, IOException {
		return run(pathToExecutable, corpusName, pathToRegistry, false);
	}

	/**
	 * * Instantiates a new Cwb Make All using executable file set in preferences
	 *
	 * @param corpusName the corpus name
	 * @param pathToRegistry the path to the registry
	 * @return true, if successful
	 * @throws ServerNotFoundException the server not found exception
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public boolean run(String corpusName,
			String pathToRegistry) throws ServerNotFoundException, InterruptedException, IOException {
		return run(corpusName, pathToRegistry, false);
	}

	/**
	 * * Instantiates a new Cwb Make All using executable file set in preferences
	 *
	 * @param corpusName the corpus name
	 * @param pathToRegistry the path to the registry
	 * @return true, if successful
	 * @throws ServerNotFoundException the server not found exception
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public boolean run(String corpusName,
			String pathToRegistry, boolean monitorOutput) throws ServerNotFoundException, InterruptedException, IOException {
		new File(pathToRegistry).mkdirs();
		File pathToExecutable = new File(CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PATH_TO_CQPLIB),
				"cwb-makeall" + (System.getProperty("os.name").toLowerCase().contains("windows") ? ".exe" : "")); //$NON-NLS-1$

		return run(pathToExecutable.getAbsolutePath(), corpusName, pathToRegistry, monitorOutput);
	}


	/**
	 * 
	 * @return if the cwb-makeall executable file set in preferences is available
	 */
	public static boolean isExecutableAvailable() {
		return isExecutableAvailable("cwb-makeall" + (System.getProperty("os.name").toLowerCase().contains("windows") ? ".exe" : "")); //$NON-NLS-1$
	}

	/**
	 * execute cwb-makeall.
	 *
	 * @param pathToExecutable path to cwb-makeall executable
	 * @param corpusName the corpus name you want to build
	 * @param pathToRegistry path to the directory which contains the corpus registry file
	 * @param monitorOutput show output messages or not
	 * @return true, if successful
	 * @throws ServerNotFoundException the server not found exception
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public boolean run(String pathToExecutable, String corpusName, String pathToRegistry, boolean monitorOutput) throws ServerNotFoundException, InterruptedException, IOException {

		// FIXME: SJ: old code that created a wrong file path
		// need to check on a real TXM install if this code is very useless then remove it
		//		if (OSDetector.isFamilyWindows()) { //$NON-NLS-1$ //$NON-NLS-2$
		////			pathToExecutable = "\"" + pathToExecutable + "\""; //$NON-NLS-1$ //$NON-NLS-2$
		////			pathToRegistry = "\"" + pathToRegistry + "\""; //$NON-NLS-1$ //$NON-NLS-2$
		//			
		//			pathToExecutable = pathToExecutable + "/"; //$NON-NLS-1$ //$NON-NLS-2$
		//			pathToRegistry = pathToRegistry + "/"; //$NON-NLS-1$ //$NON-NLS-2$
		//
		//			
		//		}

		if (!new File(pathToExecutable).exists()) {
			Log.severe("Error: path to cwb-makeall not found: " + pathToExecutable);
			return false;
		}
		if (!new File(pathToExecutable).canExecute()) {
			Log.severe("Error: path to cwb-makeall not executable: " + pathToExecutable);
			return false;
		}

		ArrayList<String> cmd = new ArrayList<String>();
		cmd.addAll(Arrays.asList(pathToExecutable, "-r", pathToRegistry, "-V", corpusName.toUpperCase())); //$NON-NLS-1$ //$NON-NLS-2$
		if (debug) {
			cmd.add(1, "-D"); //$NON-NLS-1$
		}
		boolean ret = run(cmd, monitorOutput, true); // always wait for cwb-makeall to end

		//		if (ret) {
		//			File regFile = new File(pathToRegistry);
		//			ReadRegistryFile registry = new ReadRegistryFile(regFile);
		//			registry.read();
		//			
		//			File dataDirectory = registry.getDataDirectory();
		//			
		//			for (String struct : registry.getSAttributesMap().keySet()) {
		//				for (String attr : registry.getSAttributesMap().get(struct)) {
		//					File avxFile = new File(dataDirectory, attr+".avx");
		//					File avsFile = new File(dataDirectory, attr+".avs");
		//					File rngFile = new File(dataDirectory, attr+".rng");
		//					File cpos2idFile = new File(dataDirectory, attr+".corpus.idx");
		//					File id2StrFile = new File(dataDirectory, attr+".lexicon.idx");
		//					
		//					if (avsFile.exists()) {
		//						System.out.println("Building id -> cpos: "+id2StrFile);
		//						ArrayList<String> strings = new ArrayList<String>();
		//						String dataString = IOUtils.getText(avsFile);
		//					
		//						System.out.println("Building cpos -> id: "+cpos2idFile);
		//					}
		//				}
		//			}
		//			
		//			return ret;
		//		} else {
		//			return false;
		//		}
		return ret;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		CwbMakeAll cwbMa = new CwbMakeAll();
		try {
			// cwbMa.run("/home/ayepdieu/textometrie/trunk/toolbox/src/main/C/cwb-3.0/utils/cwb-makeall",
			// "QGRAALCM", "/home/txm/form/registry");
			cwbMa.run("/home/ayepdieu/textometrie/trunk/toolbox/src/main/C/cwb-3.0/utils/cwb-makeall", "QGRAALFRMOD", "/home/txm/form/registry"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			// System.out.println(cwbMa.getErrorStream().toString());
		}
		catch (Exception ex) {
			System.out.println(ex);
			// System.out.println(cwbMa.getErrorStream());
		}
	}
}
