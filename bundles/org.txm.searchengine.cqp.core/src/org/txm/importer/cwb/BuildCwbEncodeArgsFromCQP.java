// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-08-29 14:23:55 +0200 (Thu, 29 Aug 2013) $
// $LastChangedRevision: 2519 $
// $LastChangedBy: nilskredens $ 
//
package org.txm.importer.cwb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.searchengine.cqp.core.messages.CQPSearchEngineCoreMessages;

/**
 * build the sAttributes and pAttributs List from an xmlfile pAttributes
 * (attributes of tei:w tags) and the sAttributes (attributes + max inclusion of
 * tags).
 *
 * @author mdecorde
 */
public class BuildCwbEncodeArgsFromCQP {

	/** The stack. */
	LinkedList<String> stack = new LinkedList<String>();

	/** The tagcount. */
	HashMap<String, Integer> tagcount = new HashMap<String, Integer>();

	/** The tagmax. */
	HashMap<String, Integer> tagmax = new HashMap<String, Integer>();

	/** The tagattrs. */
	HashMap<String, HashSet<String>> tagattrs = new HashMap<String, HashSet<String>>();

	int nWordProps = 0;

	/**
	 * pattern to find structure attributes
	 */
	public static final Pattern p = Pattern.compile("(([^= ]+)=\"([^\"]+)\")"); //$NON-NLS-1$

	/**
	 * process !!!.
	 *
	 * @param cqpfile the input file in CQP format
	 * @return true if successful
	 * @throws XMLStreamException the xML stream exception
	 * @throws FactoryConfigurationError the factory configuration error
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public boolean process(File cqpfile)
			throws XMLStreamException, FactoryConfigurationError, IOException {
		if (cqpfile == null || !cqpfile.exists()) {
			System.out.println(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.theFileP0CannotBeFound, cqpfile));
			return false;
		}

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(cqpfile), "UTF-8"))) { //$NON-NLS-1$
			String line = reader.readLine();
			String localname;

			//for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			for (; line != null; line = reader.readLine()) {

				if (line.startsWith("</") && line.endsWith(">")) { // END_ELEMENT //$NON-NLS-1$
					int idx = line.indexOf(" "); //$NON-NLS-1$
					if (idx == -1) idx = line.indexOf(">"); //$NON-NLS-1$
					localname = line.substring(2, idx);

					if (stack.size() > 0) {
						stack.pop();
						int count = tagcount.get(localname);
						count--;
						tagcount.put(localname, count);// decrement the
						// recursion counter of the tag
					}

				}
				else if (line.startsWith("<") && line.endsWith("/>")) { // EMPTY_ELEMENT //$NON-NLS-1$
					// ignore milestones
				}
				else if (line.startsWith("<") && line.endsWith(">")) { // START_ELEMENT //$NON-NLS-1$
					int idx = line.indexOf(" "); //$NON-NLS-1$
					if (idx == -1) idx = line.indexOf(">"); // no attributes //$NON-NLS-1$
					localname = line.substring(1, idx);

					stack.push(localname);

					if (!tagattrs.containsKey(localname)) {// initialize HashMaps
						tagattrs.put(localname, new HashSet<String>());
						tagcount.put(localname, 0);
						tagmax.put(localname, 0);
					}

					Matcher m = p.matcher(line);
					while (m.find()) {
						if (m.groupCount() == 3) {
							tagattrs.get(localname).add(m.group(2));
						}
					}

					int count = tagcount.get(localname);
					count++;
					tagcount.put(localname, count);// increment the
					// recursion counter of the tag
					int max = tagmax.get(localname);
					if (max < count) {
						tagmax.put(localname, count);// update max recursion of the tag
					}

				}
				else { // WORD LINE
					if (nWordProps == 0) {
						//System.out.println("Line: "+line);
						String[] split = line.split("\t"); //$NON-NLS-1$
						nWordProps = split.length;
						//System.out.println("nprops: "+nWordProps);
					}

				}
			} // end while

			reader.close();
		}

		return true;
	}

	/**
	 * Gets the p attributes.
	 *
	 * @return the p attributes
	 */
	public Collection<String> getPAttributes() {
		ArrayList<String> props = new ArrayList<String>();
		for (int i = 1; i < nWordProps; i++) { // don't count the "word" property
			props.add("p" + i); //$NON-NLS-1$
		}
		return props;
	}

	/**
	 * Gets the s attributes.
	 *
	 * @return the s attributes
	 */
	public Collection<String> getSAttributes() {
		ArrayList<String> rez = new ArrayList<String>();

		for (String tag : tagattrs.keySet()) {
			String s = tag.toLowerCase();
			s += ":"; //$NON-NLS-1$

			if (tagmax.get(tag) >= 0)
				s += "" + (tagmax.get(tag) - 1); //$NON-NLS-1$

			for (String attr : tagattrs.get(tag))
				s += "+" + attr.toLowerCase(); //$NON-NLS-1$
			rez.add(s);
		}
		return rez;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		try {
			BuildCwbEncodeArgsFromCQP argsgetter = new BuildCwbEncodeArgsFromCQP();
			argsgetter.process(new File("xml/cqp/discours.cqp")); //$NON-NLS-1$

			System.out.println("pAttributes : " + argsgetter.getPAttributes()); //$NON-NLS-1$
			System.out.println("sAttributes : " + argsgetter.getSAttributes()); //$NON-NLS-1$
		}
		catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		catch (FactoryConfigurationError e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}
}
