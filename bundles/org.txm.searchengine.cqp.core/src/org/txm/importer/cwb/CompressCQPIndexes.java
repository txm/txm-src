// Copyright © 2010-2022 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-05-24 10:43:03 +0200 (Tue, 24 May 2016) $
// $LastChangedRevision: 3216 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.importer.cwb;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.txm.utils.OSDetector;

/**
 * The Class CompressCQPIndexes.
 */
public class CompressCQPIndexes {

	/**
	 * 
	 * @param cqpToolsDirectory directory to the CWB tools
	 * @param registryfile the corpus registry file
	 * @param corpusid the corpus ID in UPPERCASE
	 * @param dataDirectory directory to the CQP corpus indexes
	 * @param txm081fix set to true for a corpus targeting TXM 0.8.1
	 * @return true if the corpus was compressed
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static boolean compressAll(File cqpToolsDirectory, File registryfile, String corpusid, File dataDirectory, boolean txm081fix)
			throws IOException, InterruptedException {
		//Runtime.getRuntime().

		if (!registryfile.exists()) {
			System.out.println("No registry file found: " + registryfile);
			return false;
		}

		if (!dataDirectory.exists()) {
			System.out.println("No data directory found: " + dataDirectory);
			return false;
		}

		File huff = new File(cqpToolsDirectory, "cwb-huffcode"); //$NON-NLS-1$
		if (OSDetector.isFamilyWindows()) {
			huff = new File(cqpToolsDirectory, "cwb-huffcode.exe"); //$NON-NLS-1$
		}

		File rdxcompressor = new File(cqpToolsDirectory, "cwb-compress-rdx"); //$NON-NLS-1$
		if (OSDetector.isFamilyWindows()) {
			rdxcompressor = new File(cqpToolsDirectory, "cwb-compress-rdx.exe"); //$NON-NLS-1$
		}

		if (!huff.exists()) {
			System.out.println("No huff executable found: " + huff);
			return false;
		}

		if (!rdxcompressor.exists()) {
			System.out.println("No rdxcompressor executable found: " + rdxcompressor);
			return false;
		}

		ArrayList<String> args = new ArrayList<>(Arrays.asList(huff.getAbsolutePath(), "-T", "-r", registryfile.getParent())); //$NON-NLS-1$ //$NON-NLS-2$

		//		ReadRegistryFile rrf = new ReadRegistryFile(registryfile);
		//		rrf.read();
		//		for (String p : rrf.pAttributes) {
		//			args.add("-P");
		//			args.add(p);
		//		}
		args.add("-A"); //$NON-NLS-1$
		args.add(corpusid);

		ProcessBuilder processBuilder = new ProcessBuilder(args);
		processBuilder.redirectErrorStream(true);
		Process process = processBuilder.start();
		String messages = CwbProcess.getOutputMessages(process);
		System.out.println(messages);
		process.waitFor();
		if (process.exitValue() != 0) {
			System.out.println("Error while compressing with huff");
			return false;
		}

		ArrayList<String> args2 = new ArrayList<>(Arrays.asList(rdxcompressor.getAbsolutePath(), "-T", "-r", registryfile.getParent())); //$NON-NLS-1$ //$NON-NLS-2$

		//		for (String p : rrf.pAttributes) {
		//			File f = new File(dataDirectory, p+".corpus");
		//			if (f.length() > 0) {
		//				args2.add("-P");
		//				args2.add(p);
		//			}
		//		}
		args2.add("-A"); //$NON-NLS-1$
		args2.add(corpusid);

		processBuilder = new ProcessBuilder(args2);
		processBuilder.redirectErrorStream(true);
		process = processBuilder.start();
		messages = CwbProcess.getOutputMessages(process);
		System.out.println(messages);
		if (process.exitValue() != 0) {
			System.out.println("Error while compressing rdx files");
			return false;
		}

		// remove .corpus files if the compression was successful
		int s = 0;
		int a = 0;
		for (File f : dataDirectory.listFiles()) {
			if (f.getName().endsWith(".corpus")) { //$NON-NLS-1$


				String path = f.getAbsolutePath();
				path = path.substring(0, path.length() - 7) + ".huf"; //$NON-NLS-1$
				File cfile = new File(path);
				if (cfile.exists()) {
					s += f.length();
					f.delete();
					if (txm081fix) f.createNewFile();
				}
				else {
					continue;
				}
			}
			if (f.getName().matches(".+(\\.hcd|\\.huf|\\.huf\\.syn)")) { //$NON-NLS-1$
				a += f.length();
			}
		}

		// remove .corpus.rdx and corpus.rev files if the compression was successful
		for (File f : dataDirectory.listFiles()) {
			if (f.getName().endsWith(".corpus.rdx") || f.getName().endsWith(".corpus.rev")) { //$NON-NLS-1$ //$NON-NLS-2$

				String path = f.getAbsolutePath();
				path = path.substring(0, path.length() - 7) + ".crc"; //$NON-NLS-1$
				File cfile = new File(path);
				if (cfile.exists()) {
					s += f.length();
					f.delete();
					if (txm081fix) f.createNewFile();
				}
				else {
					continue;
				}
			}
			if (f.getName().matches(".+(\\.crc|\\.crx)")) { //$NON-NLS-1$
				a += f.length();
			}
		}

		System.out.println("cleared: " + s); //$NON-NLS-1$
		System.out.println("created: " + a); //$NON-NLS-1$
		System.out.println("diff=" + (s - a)); //$NON-NLS-1$
		return true;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		try {
			String userdir = System.getProperty("user.home"); //$NON-NLS-1$
			File tools = new File(userdir, "SVN/txm-sf/CWB/cwb-lib/src/builds/linux-64"); //$NON-NLS-1$
			File registry = new File(userdir, "runtime-rcpapplication.product/corpora/NOV13-P1-2022-06-15-V1/registry/nov13-p1-2022-06-15-v1"); //$NON-NLS-1$
			File data = new File(userdir, "runtime-rcpapplication.product/corpora/NOV13-P1-2022-06-15-V1/data/NOV13-P1-2022-06-15-V1"); //$NON-NLS-1$
			CompressCQPIndexes.compressAll(tools, registry, "NOV13-P1-2022-06-15-V1", data, true); //$NON-NLS-1$

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
