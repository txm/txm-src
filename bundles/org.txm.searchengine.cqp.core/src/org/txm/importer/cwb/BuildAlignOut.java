package org.txm.importer.cwb;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

/**
 * Create the CWB "align.out" file usually created with cwb-align using 2 CQP files. The alignement structure and property is not checked. Only structure order is used.
 * 
 * @author mdecorde
 *
 */
public class BuildAlignOut {

	File cqpFile1;

	File cqpFile2;

	public BuildAlignOut(File cqpFile1, File cqpFile2) {
		this.cqpFile1 = cqpFile1;
		this.cqpFile2 = cqpFile2;
	}

	public boolean process(File alignOutFile, String su, String sup) throws IOException {

		ArrayList<Integer> positions1 = getWordPositions(cqpFile1, su, sup);
		ArrayList<Integer> positions2 = getWordPositions(cqpFile2, su, sup);

		if (positions1.size() != positions2.size()) {
			System.out.println("Error: the number of segment differs. " + positions1.size() + " in " + cqpFile1 + " and " + positions2.size() + " in " + cqpFile2);
			return false;
		}

		Log.fine("Writing " + alignOutFile.getName() + " with " + positions1.size() + " positions.");
		String encoding = "UTF-8"; //$NON-NLS-1$
		PrintWriter writer = IOUtils.getWriter(alignOutFile, encoding);

		String c1 = cqpFile1.getName().toLowerCase();
		c1 = c1.substring(0, c1.length() - 4);
		String c2 = cqpFile2.getName().toLowerCase();
		c2 = c2.substring(0, c2.length() - 4);

		writer.println(c1 + "\t" + su + "\t" + c2 + "\t" + su); //$NON-NLS-1$
		for (int i = 0; i < positions1.size(); i = i + 2) {
			writer.println("" + positions1.get(i) + "\t" + positions1.get(i + 1) + "\t" + positions2.get(i) + "\t" + positions2.get(i + 1) + "\t1:1\t42"); //$NON-NLS-1$
		}
		writer.close();

		return true;
	}

	private static ArrayList<Integer> getWordPositions(File cqpFile, String su,
			String sup) throws IOException {

		BufferedReader reader = IOUtils.getReader(cqpFile);
		int position_counter = 0;
		ArrayList<Integer> positions1 = new ArrayList<Integer>();

		String pattern = " " + sup + "=\""; //$NON-NLS-1$
		String line = reader.readLine();
		while (line != null) {
			if (line.startsWith("<" + su) && line.contains(pattern)) { //$NON-NLS-1$
				// align structure !
				positions1.add(position_counter);
			}
			else if (line.startsWith("</" + su + ">")) { //$NON-NLS-1$
				// align structure !
				positions1.add(position_counter - 1);
			}
			else if (line.startsWith("<")) { //$NON-NLS-1$
				// structure !
			}
			else if (line.length() == 0) {
				// empty line
			}
			else { // word line
				//System.out.println("WORD: "+line);
				position_counter++;
			}
			line = reader.readLine();
		}
		return positions1;
	}

	public static void main(String args[]) throws IOException {

		//		PatchCwbRegistry.patchAlignment(new File("/home/mdecorde/TEMP/align/registry", "c1"), "c2");
		//		PatchCwbRegistry.patchAlignment(new File("/home/mdecorde/TEMP/align/registry", "c2"), "c1");

		File cqpFile1 = new File("TXM/corpora/SAMPLE/cqp/SAMPLE_en0.cqp"); //$NON-NLS-1$
		File cqpFile2 = new File("TXM/corpora/SAMPLE/cqp/SAMPLE_fr3.cqp"); //$NON-NLS-1$
		File alignOutFile = new File("TXM/corpora/SAMPLE/align.out");

		BuildAlignOut bao = new BuildAlignOut(cqpFile1, cqpFile2);
		System.out.println("Result: " + bao.process(alignOutFile, "seg", "id")); //$NON-NLS-1$
	}
}
