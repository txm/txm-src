// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-08-29 14:23:55 +0200 (jeu., 29 août 2013) $
// $LastChangedRevision: 2519 $
// $LastChangedBy: nilskredens $ 
//
package org.txm.importer.cwb;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.searchengine.cqp.core.messages.CQPSearchEngineCoreMessages;

// TODO: Auto-generated Javadoc
/**
 * build the sAttributes and pAttributs List from an xmlfile pAttributes
 * (attributes of tei:w tags) and the sAttributes (attributes + max inclusion of
 * tags).
 *
 * @author mdecorde
 */
public class BuildCwbEncodeArgs {

	/** The stack. */
	LinkedList<String> stack = new LinkedList<String>();

	/** The tagcount. */
	HashMap<String, Integer> tagcount = new HashMap<String, Integer>();

	/** The tagmax. */
	HashMap<String, Integer> tagmax = new HashMap<String, Integer>();

	/** The tagattrs. */
	HashMap<String, HashSet<String>> tagattrs = new HashMap<String, HashSet<String>>();

	/** The wattrs. */
	HashSet<String> wattrs = new HashSet<String>();

	private String scanElem;

	/**
	 * process !!!.
	 *
	 * @param xmlfile the xmlfile
	 * @param wordstag the wordstag
	 * @return true, if successful
	 * @throws XMLStreamException the xML stream exception
	 * @throws FactoryConfigurationError the factory configuration error
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public boolean process(File xmlfile, ArrayList<String> wordstag)
			throws XMLStreamException, FactoryConfigurationError, IOException {
		if (xmlfile == null || !xmlfile.exists()) {
			System.out.println(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.theFileP0CannotBeFound, xmlfile));
			return false;
		}
		URL u = xmlfile.toURI().toURL();
		InputStream in = u.openStream();
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLStreamReader parser = factory.createXMLStreamReader(in);

		gotoStartElement(parser);

		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser
				.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					stack.push(parser.getLocalName());

					if (!tagattrs.containsKey(parser.getLocalName()))// initialize
					// HashMaps
					{
						tagattrs.put(parser.getLocalName(), new HashSet<String>());
						tagcount.put(parser.getLocalName(), 0);
						tagmax.put(parser.getLocalName(), 0);
					}

					if (wordstag.contains(parser.getLocalName()))// get w attrs
						for (int i = 0; i < parser.getAttributeCount(); i++) {
							String att = parser.getAttributeLocalName(i);
							wattrs.add(att);
						}

					for (int i = 0; i < parser.getAttributeCount(); i++)// get tags
					// attrs
					{
						String att = parser.getAttributeLocalName(i);
						tagattrs.get(parser.getLocalName()).add(att);
					}

					int count = tagcount.get(parser.getLocalName());
					count++;
					tagcount.put(parser.getLocalName(), count);// increment the
					// recursion counter
					// of the tag
					int max = tagmax.get(parser.getLocalName());
					if (max < count)
						tagmax.put(parser.getLocalName(), count);// update max
					// recursion of
					// the tag
					break;
				case XMLStreamConstants.END_ELEMENT:
					if (stack.size() > 0) {
						stack.pop();
						count = tagcount.get(parser.getLocalName());
						count--;
						tagcount.put(parser.getLocalName(), count);// decrement the
						// recursion counter
						// of the tag
					}

					break;
				case XMLStreamConstants.CHARACTERS:

					break;
				case XMLStreamConstants.CDATA:

					break;
			} // end switch
		} // end while
		tagattrs.remove("w");//we dont need to report the w tags :) //$NON-NLS-1$
		tagmax.remove("w"); //$NON-NLS-1$
		parser.close();
		in.close();
		return true;
	}

	public void gotoStartElement(XMLStreamReader parser) throws XMLStreamException {
		if (this.scanElem == null) return;
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser
				.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					if (parser.getLocalName() == scanElem)
						return;
			}
		}
	}

	public void setElementToScan(String elem) {
		this.scanElem = elem;
	}

	/**
	 * Gets the p attributes.
	 *
	 * @return the p attributes
	 */
	public Collection<String> getPAttributes() {
		return wattrs;
	}

	/**
	 * Gets the s attributes.
	 *
	 * @return the s attributes
	 */
	public Collection<String> getSAttributes() {
		ArrayList<String> rez = new ArrayList<String>();

		for (String tag : tagattrs.keySet()) {
			String s = tag.toLowerCase();
			s += ":"; //$NON-NLS-1$

			if (tagmax.get(tag) >= 0)
				s += "" + (tagmax.get(tag) - 1); //$NON-NLS-1$

			for (String attr : tagattrs.get(tag))
				s += "+" + attr.toLowerCase(); //$NON-NLS-1$
			rez.add(s);
		}
		return rez;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		try {
			BuildCwbEncodeArgs argsgetter = new BuildCwbEncodeArgs();
			ArrayList<String> wordstag = new ArrayList<String>();
			String[] awords = { "M", "PC", "MC" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

			for (String s : awords) {
				wordstag.add(s);
			}
			argsgetter.process(new File(
					"~/xml/fleurs/src/fleursduMal-2.xml"), wordstag); //$NON-NLS-1$

			System.out.println("pAttributes : " + argsgetter.getPAttributes()); //$NON-NLS-1$
			System.out.println("sAttributes : " + argsgetter.getSAttributes()); //$NON-NLS-1$
		}
		catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		catch (FactoryConfigurationError e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}
}
