// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-05-30 16:02:59 +0200 (Mon, 30 May 2016) $
// $LastChangedRevision: 3220 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.importer.cwb;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.txm.Toolbox;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.libs.cqp.CQPLibPreferences;
import org.txm.searchengine.cqp.clientExceptions.ServerNotFoundException;
import org.txm.searchengine.cqp.core.messages.CQPSearchEngineCoreMessages;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.OSDetector;
import org.txm.utils.io.IOUtils;

/**
 * This class aims at handling a CQi server process. It is not a CQi server per
 * se.
 * 
 * @author Adrien Yepdieu, mdecorde
 */

public class CwbEncode extends CwbProcess {

	/**
	 * Instantiates a new CQi server.
	 */
	public CwbEncode() {
		super("cwb-encode"); //$NON-NLS-1$
	}

	/**
	 * Instantiates a new cwb encode.
	 *
	 * @param name the name
	 */
	public CwbEncode(String name) {
		super(name);
	}

	/**
	 * Instantiates a new CQi server using executable path set in CQPLibPreferences.
	 *
	 * @param pathToData the path to data
	 * @param pathToRegistry the path to the registry
	 * @param pAttributes the positional attribute
	 * @param sAttributes the structural attribute
	 * @return true if successful
	 * @throws ServerNotFoundException the server not found exception
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public boolean run(String pathToData,
			String inputFile, String pathToRegistry, String[] pAttributes,
			String[] sAttributes) throws ServerNotFoundException, InterruptedException, IOException {
		return run(pathToData, inputFile,
				pathToRegistry, pAttributes, sAttributes, false);
	}

	/**
	 * Instantiates a new CQi server using executable path set in CQPLibPreferences.
	 *
	 * @param pathToData the path to data
	 * @param pathToRegistry the path to the registry
	 * @param pAttributes the positional attribute
	 * @param sAttributes the structural attribute
	 * @return true if successful
	 * @throws ServerNotFoundException the server not found exception
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public boolean run(String pathToData,
			String inputFile, String pathToRegistry, String[] pAttributes,
			String[] sAttributes, boolean monitorOutput) throws ServerNotFoundException, InterruptedException, IOException {

		File pathToExecutable = new File(CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PATH_TO_CQPLIB),
				"cwb-encode" + (System.getProperty("os.name").toLowerCase().contains("windows") ? ".exe" : "")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$

		return run(pathToExecutable.getAbsolutePath(), pathToData, inputFile,
				pathToRegistry, pAttributes, sAttributes, false);
	}

	/**
	 * 
	 * @return if the cwb-encode executable file set in preferences is available
	 */
	public static boolean isExecutableAvailable() {
		return isExecutableAvailable("cwb-encode" + (System.getProperty("os.name").toLowerCase().contains("windows") ? ".exe" : "")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
	}

	/**
	 * * Instantiates a new CQi server.
	 *
	 * @param pathToExecutable the path to the executable
	 * @param pathToData the path to data
	 * @param inputFiles the input files
	 * @param pathToRegistry the path to the registry
	 * @param pAttributes the positional attribute
	 * @param sAttributes the structural attribute
	 * @return true if successful
	 * @throws ServerNotFoundException the server not found exception
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public boolean run(String pathToExecutable, String pathToData,
			List<File> inputFiles, String pathToRegistry, String[] pAttributes,
			String[] sAttributes) throws ServerNotFoundException, InterruptedException, IOException {
		boolean ret = run(pathToExecutable, pathToData, inputFiles, pathToRegistry, pAttributes, sAttributes, false);

		return ret;
	}

	/**
	 * * Instantiates a new CQi server.
	 *
	 * @param pathToExecutable the path to the executable
	 * @param pathToData the path to data
	 * @param inputFile the input file
	 * @param pathToRegistry the path to the registry
	 * @param pAttributes the positional attribute
	 * @param sAttributes the structural atribute
	 * @return true, if successful
	 * @throws ServerNotFoundException the server not found exception
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public boolean run(String pathToExecutable, String pathToData,
			String inputFile, String pathToRegistry, String[] pAttributes,
			String[] sAttributes) throws ServerNotFoundException, InterruptedException, IOException {
		boolean ret = run(pathToExecutable, pathToData, inputFile, pathToRegistry, pAttributes, sAttributes, false);

		return ret;
	}

	/*
	 * delete all files and directory of the given path
	 */
	/**
	 * Delete dir.
	 *
	 * @param dir the dir
	 * @return true, if successful
	 */
	public static boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		// The directory is now empty so delete it
		return dir.delete();
	}

	public boolean run(String pathToExecutable, String pathToData,
			String inputFile, String pathToRegistry, String[] pAttributes,
			String[] sAttributes, boolean monitorOutput)
			throws ServerNotFoundException, InterruptedException, IOException {

		List<File> inputFiles = Arrays.asList(new File(inputFile));

		return run(pathToExecutable, pathToData, inputFiles, pathToRegistry, pAttributes, sAttributes, monitorOutput);
	}

	/**
	 * execute cwb-encode.
	 *
	 * @param pathToExecutable the path to the executable of cwb-encode
	 * @param pathToData the path to cwb indexes
	 * @param inputFiles the input file
	 * @param pathToRegistry the path to registry
	 * @param pAttributes the attributes
	 * @param sAttributes the s attributes
	 * @param monitorOutput the monitor output
	 * @return true, if successful
	 * @throws ServerNotFoundException the server not found exception
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public boolean run(String pathToExecutable, String pathToData,
			List<File> inputFiles, String pathToRegistry, String[] pAttributes,
			String[] sAttributes, boolean monitorOutput)
			throws ServerNotFoundException, InterruptedException, IOException {
		File data = new File(pathToData);
		deleteDir(data);
		data.mkdirs();

		// fix UNC paths for mingw
		if (pathToRegistry.startsWith("\\\\")) { //$NON-NLS-1$
			pathToRegistry = pathToRegistry.replace("\\\\", "//"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		// fix UNC paths for mingw
		if (pathToData.startsWith("\\\\")) { //$NON-NLS-1$
			pathToData = pathToData.replace("\\\\", "//"); //$NON-NLS-1$ //$NON-NLS-2$
		}

		if (!new File(pathToExecutable).exists()) {
			System.err.println(TXMCoreMessages.bind("Error: path to cwb-encode not found: {0}.", pathToExecutable));
			return false;
		}
		if (!new File(pathToExecutable).canExecute()) {
			System.err.println(TXMCoreMessages.bind("Error: path to cwb-encode not executable: {0}.", pathToExecutable));
			return false;
		}

		/*
		 * if (System.getProperty("os.name").contains("Win")) { pathToExecutable
		 * = "\""+pathToExecutable+"\""; pathToData = "\""+pathToData+"\"";
		 * inputFile = "\""+inputFile+"\""; pathToRegistry =
		 * "\""+pathToRegistry+"\""; }
		 */

		// Warning !! for WINDOWS ONLY the "" value does not work with the ProcessBuilder we must set the value to "\"\""
		String empty_value_code = Toolbox.getPreference(TBXPreferences.EMPTY_PROPERTY_VALUE_CODE);
		if (OSDetector.isFamilyWindows()) {
			if (empty_value_code == null) empty_value_code = "\"\"";//$NON-NLS-1$
			empty_value_code = empty_value_code.trim();
			if (empty_value_code.length() == 0) empty_value_code = "\"\"";//$NON-NLS-1$
		}

		ArrayList<String> cmd = new ArrayList<>();
		if (inputFiles != null) { // read input from file
			cmd.add(pathToExecutable);
			cmd.add("-d");//$NON-NLS-1$
			cmd.add(new File(pathToData).getAbsolutePath());
			cmd.add("-R"); //$NON-NLS-1$
			cmd.add(new File(pathToRegistry).getAbsolutePath());
			cmd.add("-U");//$NON-NLS-1$
			cmd.add(empty_value_code);
			cmd.add("-c"); //$NON-NLS-1$
			cmd.add("utf8"); //$NON-NLS-1$

			if (debug) {
				cmd.add("-D"); //$NON-NLS-1$
			}

			for (File inputFile : inputFiles) { // append '-f path' parameters
				cmd.add("-f"); //$NON-NLS-1$
				cmd.add(inputFile.getAbsolutePath());
			}
		}
		else { // read input from stdin
			String[] c = { pathToExecutable,
					"-d", new File(pathToData).getAbsolutePath() //$NON-NLS-1$
					, "-R", new File(pathToRegistry).getAbsolutePath() //$NON-NLS-1$
					, "-U", empty_value_code //$NON-NLS-1$
					, "-c", "utf8" //$NON-NLS-1$ //$NON-NLS-2$
					// , "-x" //$NON-NLS-1$
			};
			cmd.addAll(Arrays.asList(c));
		}

		cmd.add("-xsB"); //$NON-NLS-1$
		for (int i = 0; i < pAttributes.length; i++) {
			cmd.add("-P"); //$NON-NLS-1$
			// lower case pattributes
			cmd.addAll(Arrays.asList(pAttributes[i].toLowerCase().split(" "))); //$NON-NLS-1$

		}
		for (int i = 0; i < sAttributes.length; i++) {
			cmd.add("-S"); //$NON-NLS-1$
			// lower case sattributes
			cmd.addAll(Arrays.asList(sAttributes[i].toLowerCase().split(" "))); //$NON-NLS-1$
		}
		// String[] tmp = new String[cmd.length];
		boolean waitForCWBEncodeToEnd = inputFiles != null;
		return run(cmd, monitorOutput, waitForCWBEncodeToEnd) && new File(pathToRegistry).exists();
	}

	/**
	 * Patch dir.
	 *
	 * @param directory the directory
	 */
	public static void patchDir(File directory) {
		File bin = new File(directory, "bin"); //$NON-NLS-1$
		File registry = new File(directory, "registry"); //$NON-NLS-1$
		File corpora = new File(directory, "corpora"); //$NON-NLS-1$
		if (!directory.exists()) {
			System.err.println(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.theP0DirectoryDoesNotExists, directory));
			return;
		}
		if (!bin.exists()) {
			System.err.println(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.theBinaryDirectoryDoesNotExistsP0, bin));
			return;
		}
		if (!registry.exists()) {
			System.err.println(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.theRegistryDirectoryDoesNotExistsInP0, registry));
			return;
		}
		if (!corpora.exists()) {
			System.err.println(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.theCorporaDirectoryDoesNotExistsP0, corpora));
			return;
		}

		for (File corpus : corpora.listFiles(IOUtils.HIDDENFILE_FILTER)) {
			if (!corpus.isDirectory()) continue;

			System.out.println(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.processingP0Corpus, corpus.getName()));
			File data = new File(corpus, "data"); //$NON-NLS-1$

			if (!data.exists()) {
				System.err.println(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.theDataDirectoryDoesNotExistsP0, data));
				return;
			}

			String datapath = data.getAbsolutePath();
			String regpath = registry.getAbsolutePath() + "/" + corpus.getName(); //$NON-NLS-1$
			// fix UNC paths for mingw
			if (datapath.startsWith("\\\\")) { //$NON-NLS-1$
				datapath = datapath.replace("\\\\", "//"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			System.out.println(NLS.bind(CQPSearchEngineCoreMessages.fixingRegistryFileP0WithP1, regpath, datapath));
			try {
				PatchCwbRegistry.patch(new File(regpath), new File(datapath));
			}
			catch (IOException e) {
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
	}

	/**
	 * concatenates CQP files into one master CQP file and wraps with the 'txmcorpus' structure.
	 * 
	 * @param cqpFiles
	 * @param allcqpFile
	 * @return true if the CQP files are concatenated
	 * @throws IOException
	 */
	public static boolean concat(ArrayList<File> cqpFiles, File allcqpFile) throws IOException {
		PrintWriter output = IOUtils.getWriter(allcqpFile);
		ConsoleProgressBar cpb = new ConsoleProgressBar(cqpFiles.size());

		output.write("<txmcorpus lang=\"fr\">\n"); //$NON-NLS-1$
		for (File cqpFile : cqpFiles) {
			cpb.tick();
			BufferedReader reader = IOUtils.getReader(cqpFile, "UTF-8"); //$NON-NLS-1$
			String line = reader.readLine();
			while (line != null) {
				output.println(line); //$NON-NLS-1$
				line = reader.readLine();
				if (line != null) {
					output.write("\n"); //$NON-NLS-1$
				}
			}
			reader.close();

			output.flush();
		}
		output.write("</txmcorpus>\n"); //$NON-NLS-1$
		output.close();
		return true;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		// CwbEncode cwbEn = new CwbEncode();
		// String[] pAttributes = {"pos", "col", "line", "id", "facs", "dipl"};
		// String[] sAttributes = {"p:0+n", "q:1", "s:0+n+id"};
		/*
		 * String[] pAttributes = {"pos", "id", "col"}; String[] sAttributes =
		 * {"p:0+n", "s:0+n"}; try {//cwbEn.run(
		 * "/home/ayepdieu/textometrie/trunk/toolbox/src/main/C/cwb-3.0/utils/cwb-encode"
		 * , "/home/txm/form/data/qgraalcm", "/home/txm/form/cqp/qgraalcm.cqp",
		 * "/home/txm/form/registry/qgraalcm",pAttributes, sAttributes);
		 * cwbEn.run(
		 * "/home/ayepdieu/textometrie/trunk/toolbox/src/main/C/cwb-3.0/utils/cwb-encode"
		 * , "/home/txm/form/data/qgraalfrmod",
		 * "/home/txm/form/cqp/qgraalfrmod.cqp",
		 * "/home/txm/form/registry/qgraalfrmod", pAttributes, sAttributes);
		 * System.out.println(cwbEn.getErrorStream().toString()); } catch
		 * (Exception ex) { System.out.println(ex);
		 * System.out.println(cwbEn.getErrorStream()); }
		 */
		CwbEncode.patchDir(new File("C:/Documents and Settings/H/Mes documents/TXM/cwb")); //$NON-NLS-1$
	}
}
