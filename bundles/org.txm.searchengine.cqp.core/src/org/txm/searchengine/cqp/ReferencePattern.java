// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-10-22 09:15:18 +0200 (Thu, 22 Oct 2015) $
// $LastChangedRevision: 3040 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.VirtualProperty;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.utils.logger.Log;

import com.google.gson.Gson;

/**
 * References configuration : a list of properties and optionally a sprintf like pattern.
 *
 * @author mdecorde
 */
public class ReferencePattern implements Iterable<Property> {

	/** The properties. */
	private List<Property> properties = new ArrayList<>();

	private String pattern;

	/**
	 * 
	 * @param corpus
	 * @param v
	 * @return a list of word and structure properties read from a String
	 */
	public static ReferencePattern stringToReferencePattern(CQPCorpus corpus, String v) {
		ReferencePattern ref = fromGSonString(corpus, v);
		if (ref == null) {
			ref = fromNLSString(corpus, v);
		}
		if (ref == null) {
			ref = fromTabulatedString(corpus, v);
		}
		return ref;
	}

	/**
	 * 
	 * @return a list of word and structure properties read from a String
	 */
	public static boolean isGsonString(String gsonString) {
		try {
			return new Gson().fromJson(gsonString, HashMap.class) != null;
		}
		catch (Exception e) {
			return false;
		}
	}

	/**
	 * 
	 * @return a list of word and structure properties read from a String
	 */
	public static String fromGSonToPattern(String gsonString) {
		try {
			Gson gson = new Gson();

			HashMap<?, ?> values = gson.fromJson(gsonString, HashMap.class);
			if (values == null) return ""; //$NON-NLS-1$

			Object oFormat = values.get("format"); //$NON-NLS-1$
			if (oFormat != null) {
				return oFormat.toString();
			}

		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 
	 * @return a list of word and structure properties read from a String
	 */
	public static List<String> fromGSonToPropertyNames(String gsonString) {
		ArrayList<String> props = new ArrayList<>();
		try {
			Gson gson = new Gson();

			HashMap<?, ?> values = gson.fromJson(gsonString, HashMap.class);
			if (values == null) return props;

			Object oProperties = values.get("properties"); //$NON-NLS-1$

			if (oProperties != null && oProperties instanceof List) {
				List<?> propertyNames = (List<?>) oProperties;
				for (Object o : propertyNames) {
					if (o == null) continue;
					String s = o.toString();
					props.add(s);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return props;
	}

	/**
	 * 
	 * @param corpus
	 * @param v
	 * @return a list of word and structure properties read from a String
	 */
	public static ReferencePattern fromGSonString(CQPCorpus corpus, String v) {
		try {
			Gson gson = new Gson();

			HashMap<?, ?> values = gson.fromJson(v, HashMap.class);

			if (values == null) return new ReferencePattern();

			Object oProperties = values.get("properties"); //$NON-NLS-1$
			Object oFormat = values.get("format"); //$NON-NLS-1$
			String pattern = null;
			if (oFormat != null) {
				pattern = oFormat.toString();
			}

			ReferencePattern props = new ReferencePattern(pattern);

			if (oProperties != null && oProperties instanceof List) {
				List<?> propertyNames = (List<?>) oProperties;
				for (Object o : propertyNames) {
					if (o == null) continue;
					String s = o.toString();
					if (s.contains("_")) { //$NON-NLS-1$
						String[] split2 = s.split("_", 2); //$NON-NLS-1$
						StructuralUnit su = corpus.getStructuralUnit(split2[0]);
						if (su != null) {
							Property prop = su.getProperty(split2[1]);
							if (prop != null) {
								props.addProperty(prop);
							}
						}
					}
					else {
						Property prop = corpus.getProperty(s);
						if (prop != null) {
							props.addProperty(prop);
						}
					}
				}
			}

			return props;
		}
		catch (Exception e) {
			// System.out.println("Error: " + v + " property not found in " + corpus);
			return null;
		}
	}



	/**
	 * 
	 * @param corpus
	 * @param v "${strct_prop} like string"
	 * @return
	 */
	protected static ReferencePattern fromNLSString(CQPCorpus corpus, String v) {
		try {
			ReferencePattern props = new ReferencePattern();
			if (v == null) return props;
			if (v.length() == 0) return props;

			Pattern pattern = Pattern.compile("\\$\\{([^\\}]+)\\}"); //$NON-NLS-1$
			Matcher matcher = pattern.matcher(v);
			int nPropExpected = 0;
			while (matcher.find()) {
				nPropExpected++;
				String gg = matcher.group(1).trim();
				String[] split = gg.split("_"); //$NON-NLS-1$
				if (split.length == 2) {
					String sstruct = split[0];
					String sprop = split[1];
					StructuralUnit struct = corpus.getStructuralUnit(sstruct);
					if (struct != null) {
						StructuralUnitProperty prop = struct.getProperty(sprop);
						if (prop != null) {
							props.addProperty(prop);
						}
					}
				}
				else if (split.length == 1) {
					String sprop = split[0];
					WordProperty prop = corpus.getProperty(sprop);
					if (prop != null) {
						props.addProperty(prop);
					}
				}
			}

			if (nPropExpected > 0 && props.getProperties().size() == nPropExpected) {
				props.setPattern(matcher.replaceAll("%s")); //$NON-NLS-1$
				return props;
			}
			else {
				return null;
			}
		}
		catch (Exception e) {
			// System.out.println("Error: " + v + " property not found in " + corpus);
			return null;
		}
	}

	protected static ReferencePattern fromTabulatedString(CQPCorpus corpus, String v) {
		try {
			ReferencePattern props = new ReferencePattern();
			if (v == null) return props;
			if (v.length() == 0) return props;

			String[] split = v.split("\t"); //$NON-NLS-1$
			for (String s : split) {
				s = s.trim();
				if (s.contains("_")) { //$NON-NLS-1$
					String[] split2 = s.split("_", 2); //$NON-NLS-1$
					StructuralUnit su = corpus.getStructuralUnit(split2[0]);
					if (su != null) {
						Property prop = su.getProperty(split2[1]);
						if (prop != null) {
							props.addProperty(prop);
						}
					}
				}
				else {
					Property prop = corpus.getProperty(s);
					if (prop != null) {
						props.addProperty(prop);
					}
				}
			}
			return props;
		}
		catch (Exception e) {
			// System.out.println("Error: " + v + " property not found in " + corpus);
			return null;
		}
	}

	/**
	 * Instantiates a new reference pattern.
	 */
	public ReferencePattern() {
		properties = new ArrayList<>();
	}

	/**
	 * Instantiates a new reference pattern.
	 *
	 * @param prop the word property
	 */
	public ReferencePattern(Property prop) {
		this.properties = new ArrayList<>();
		properties.add(prop);
	}

	/**
	 * Instantiates a new reference pattern.
	 *
	 * @param prop the structural unit property
	 */
	public ReferencePattern(StructuralUnitProperty prop) {
		properties.add(prop);
		this.pattern = null;
	}

	/**
	 * Instantiates a new reference pattern.
	 *
	 * @param properties the properties
	 */
	public ReferencePattern(List<Property> properties) {
		this.properties = properties;
		this.pattern = null;
	}

	/**
	 * Instantiates a new reference pattern.
	 *
	 * @param properties the properties
	 */
	public ReferencePattern(List<Property> properties, String pattern) {
		this.properties = properties;
		this.pattern = pattern;
	}

	public ReferencePattern(Property property, String pattern) {
		this.properties.add(property);
		this.pattern = pattern;
	}

	/**
	 * Don't forget to set the properties latter
	 * 
	 * @param pattern the sprintf pattern
	 */
	public ReferencePattern(String pattern) {
		this.pattern = pattern;
	}

	/**
	 * Adds the property.
	 *
	 * @param property the property
	 * @return the reference pattern
	 */
	public ReferencePattern addProperty(Property property) {
		this.properties.add(property);
		return this;
	}

	public String format(Collection<?> collection) {
		try {
			return String.format(pattern, collection.toArray()).toString();
		}
		catch (Exception e) {
			Log.severe(NLS.bind("Error: formatting failed: {0}.", e));
			return StringUtils.join(collection, ", "); //$NON-NLS-1$
		}
	}

	public String format(Object[] values) {
		return String.format(pattern, values).toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Property> iterator() {
		return properties.iterator();
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public List<Property> getProperties() {
		return properties;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		for (Property property : properties) {
			buffer.append(property.getName() + " "); //$NON-NLS-1$
		}
		return buffer.toString().trim();
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		if (this.properties == null || this.properties.size() == 0) {
			return "none"; //$NON-NLS-1$
		}

		if (pattern != null && pattern.length() > 0) {
			return this.format(this.properties);
		}

		String title = ""; //$NON-NLS-1$
		for (Property p : this.properties) {
			if (p == null) {
				continue;
			}
			if (p instanceof StructuralUnitProperty || p instanceof VirtualProperty) {
				title += p.getFullName() + ", "; //$NON-NLS-1$
			}
			else {
				title += p.getName() + ", "; //$NON-NLS-1$
			}
		}
		if (title.length() == 0) {
			title = TXMCoreMessages.common_reference;
		}
		else {
			title = title.substring(0, title.length() - 2);
		}
		return title;
	}

	public static String referenceToString(List<String> properties, String patternString) {
		Gson gson = new Gson();
		HashMap<String, Object> values = new HashMap<>();
		values.put("properties", properties); //$NON-NLS-1$
		values.put("format", patternString); //$NON-NLS-1$
		return gson.toJson(values);
	}

	public static String referenceToString(ReferencePattern pattern) {
		if (pattern == null) return "";
		ArrayList<String> names = new ArrayList<>();
		for (Property p : pattern.getProperties()) {
			names.add(p.getFullName());
		}

		return referenceToString(names, pattern.getPattern());
	}

	public CQPCorpus getCorpus() {
		if (properties.size() == 0) {
			return null;
		}
		return properties.get(0).getCorpus();
	}

	public static List<Property> getAvailableProperties(CQPCorpus corpus) {
		List<Property> availableReferenceItems = new ArrayList<>();
		try {
			// add word properties
			availableReferenceItems.addAll(corpus.getOrderedProperties());

			// add structural units properties
			for (StructuralUnit unit : corpus.getOrderedStructuralUnits()) {
				availableReferenceItems.addAll(unit.getOrderedProperties());
			}

			// add virtual properties
			availableReferenceItems.addAll(corpus.getVirtualProperties());
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
		return availableReferenceItems;
	}
}
