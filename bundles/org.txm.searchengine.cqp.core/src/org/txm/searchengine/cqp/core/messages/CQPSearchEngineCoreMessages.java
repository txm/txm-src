package org.txm.searchengine.cqp.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;


/**
 * CQP Search engine core messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class CQPSearchEngineCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.searchengine.cqp.core.messages.messages"; //$NON-NLS-1$

	public static String errorColonTextP0IsNull;

	public static String errorColonEditionDefaultIsNull;

	public static String errorColonTextidsP0AndWordidsP1;

	public static String p0IsNotAValidCQPIDForASubcorpusItMustBeAnUppercaseCharacterFollowedByLowercaseCharacters;

	public static String connectingToTheSearchEngineWithTheFollowingParametersColonP0ColonP1AtP2P3;

	public static String connectedToTheSearchEngine;

	public static String gettingLastCQPError;

	public static String searchEngineLaunched;

	public static String failedToConnectToSearchEngine;

	public static String theP0DirectoryDoesNotExists;

	public static String theBinaryDirectoryDoesNotExistsP0;

	public static String theRegistryDirectoryDoesNotExistsInP0;

	public static String theCorporaDirectoryDoesNotExistsP0;

	public static String processingP0Corpus;

	public static String theDataDirectoryDoesNotExistsP0;

	public static String fixingRegistryFileP0WithP1;

	public static String startingProcessWithCommandP0;

	public static String stoppingProcessP0;

	public static String P0ProcessStoped;

	public static String cantFindCQPLocationP0;

	public static String internalCQIErrorColon;

	public static String unknownCQPCodesColonB1;

	public static String queryError;

	public static String cLErrorP0;

	public static String cQPErrorColonP0;

	public static String unknownCQPErrorColonB1;

	public static String unknownCQPCQIErrorColonB1;

	public static String unknownCQPCodeColonB1;

	public static String b2;

	public static String lastCQiErrorP0;

	public static String lastCQPErrorP0;

	public static String couldNotReconnectToServerColon;

	public static String clientdumpSubCorpussubcorpusNetCqiClientCQICONSTFIELDMATCH12Colon;

	public static String cqpserverStdoutColon;

	public static String cqpserverStderrColon;

	public static String noError;

	public static String theFileP0CannotBeFound;

	public static String sourceRegistryDoesNotExistsP0;

	public static String cantFindRegistryFile;

	public static String dataDirectory;

	public static String couldNotPatchTheRegistryFile;

	public static String P0couldNotBeFoundInRegistryFileP1;

	public static String updating;

	public static String updatingP0CorpusLanguageToP1;

	public static String theP0TargetCorpusDoesNotExist;

	public static String addingAlignmentAttributeToCorpusP0RegistryColonP1;

	public static String readString2DIntegerFromTheCQiServerColonP0;

	public static String readBooleanFromTheCQiServerColonP0;

	public static String readByteFromTheCQiServerColonP0;

	public static String readIntegerArrayFromTheCQiServerColonP0;

	public static String readIntegerFromTheCQiServerColonP0;

	public static String readStringArrayFromTheCQiServerColonP0;

	public static String readStringFromTheCQiServerColonP0;

	public static String readingAnIntegerTableFromTheCQiServer;

	public static String readingBooleanFromTheCQiServer;

	public static String readingHeaderFromTheCQiServer;

	public static String readingIntegerArrayFromTheCQiServer;

	public static String readingIntegerFromTheCQiServer;

	public static String readingStringArrayFromTheCQiServer;

	public static String readingStringFromTheCQiServer;

	public static String error_errorColonCQLlengthLimitReachedP0WithP1Length;

	public static String error_errorColonCouldNotComputeSubcorpusFromTheDumpFileColonP0;



	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, CQPSearchEngineCoreMessages.class);
	}

	private CQPSearchEngineCoreMessages() {
	}

}
