package org.txm.searchengine.cqp.core.functions.preview;

import java.io.File;
import java.util.Collection;
import java.util.List;

import org.txm.core.results.TXMResult;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.utils.TXMProgressMonitor;

public class Preview extends TXMResult {

	private CQPCorpus corpus;

	private Partition partition;

	private int n;

	public Preview(CQPCorpus corpus, int n) {
		super(corpus);
		this.corpus = corpus;
		this.n = n;
	}

	public Preview(Partition partition, int n) {
		super(partition);
		this.partition = partition;
		this.n = n;
	}

	protected boolean computeCorpus() {
		boolean ok = false;

		return ok;
	}

	protected boolean computePartition() {
		boolean ok = false;
		List<? extends org.txm.objects.Match> matches = corpus.getMatches();
		for (org.txm.objects.Match m : matches) {
			if (m.size() >= n) {
				ok = true;
			}
		}
		return ok;
	}

	private String getLine(CQPCorpus corpus) {
		List<? extends org.txm.objects.Match> matches = corpus.getMatches();
		if (matches.size() == 0) return ""; //$NON-NLS-1$

		org.txm.objects.Match firstOkMatch = matches.get(0);
		for (org.txm.objects.Match m : matches) {
			if (m.size() >= n) {
				Collection<? extends Integer> positions = m.getRange();

			}
		}
		return "";// getString(firstOkMatch); //$NON-NLS-1$
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSimpleName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean canCompute() {
		return corpus != null;
	}

	@Override
	public boolean loadParameters() {
		// TODO Auto-generated method stub
		System.err.println("Preview.loadParameters(): not yet implemented."); //$NON-NLS-1$
		return true;
	}

	@Override
	public boolean saveParameters() {
		// TODO Auto-generated method stub
		System.err.println("Preview.saveParameters(): not yet implemented."); //$NON-NLS-1$
		return true;
	}


	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {
		if (partition != null) {
			return computePartition();
		}
		else {
			return computeCorpus();
		}
	}

	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getResultType() {
		return "Preview"; //$NON-NLS-1$
	}
}
