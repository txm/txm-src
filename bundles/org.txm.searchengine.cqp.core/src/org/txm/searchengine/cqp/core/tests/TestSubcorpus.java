package org.txm.searchengine.cqp.core.tests;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

import org.txm.searchengine.cqp.NetCqiClient;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.serverException.CqiServerError;

public class TestSubcorpus {

	public static String CORPUS = "DISCOURS"; //$NON-NLS-1$

	public static boolean allTests(File outdir) {
		TestSubcorpus tester = new TestSubcorpus();
		return tester.test1(outdir) && tester.test2(outdir) && tester.test3(outdir);
	}

	public boolean test1(File outdir) {
		try {
			CQPCorpus corpus = CorpusManager.getCorpusManager().getCorpus(CORPUS);
			Subcorpus sub = corpus.createSubcorpus(new CQLQuery("\"je\" expand to p"), "JEP"); //$NON-NLS-1$ //$NON-NLS-2$

			writePositions(sub, new File(outdir, TestSubcorpus.class.getName() + "_test1.csv")); //$NON-NLS-1$
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	public boolean test2(File outdir) {
		try {
			CQPCorpus corpus = CorpusManager.getCorpusManager().getCorpus(CORPUS);
			Subcorpus sub = corpus.createSubcorpus(corpus.getStructuralUnit("text"), corpus.getStructuralUnit("text").getProperty("loc"), "De_Gaulle", "DG"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$

			writePositions(sub, new File(outdir, TestSubcorpus.class.getName() + "_test2.csv")); //$NON-NLS-1$
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	public boolean test3(File outdir) {
		try {
			CQPCorpus corpus = CorpusManager.getCorpusManager().getCorpus(CORPUS);
			Subcorpus sub = corpus.createSubcorpus(corpus.getStructuralUnit("text"), corpus.getStructuralUnit("text").getProperty("type"), //$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
					Arrays.asList("Allocution radiotélévisée", "Conférence de presse"), "alloc+presse");    //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

			writePositions(sub, new File(outdir, TestSubcorpus.class.getName() + "_test3.csv")); //$NON-NLS-1$
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	private void writePositions(Subcorpus sub, File outfile) throws UnexpectedAnswerException, IOException, CqiServerError {
		int[] positions = CorpusManager.getCorpusManager().getCqiClient().dumpSubCorpus(sub.getQualifiedCqpId(), NetCqiClient.CQI_CONST_FIELD_MATCH, 0, sub.getNMatch());
		PrintWriter writer = new PrintWriter(new FileWriter(outfile));
		for (int i : positions)
			writer.println(i);
		writer.close();
	}
}
