package org.txm.searchengine.cqp.core.preferences;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

public class PartitionPreferences extends TXMPreferences {

	public static String EDITOR_MODE = "editor_mode"; //$NON-NLS-1$

	public PartitionPreferences() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(PartitionPreferences.class)) {
			new PartitionPreferences();
		}
		return TXMPreferences.instances.get(PartitionPreferences.class);
	}

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.putBoolean(EDITOR_MODE, false);
	}
}
