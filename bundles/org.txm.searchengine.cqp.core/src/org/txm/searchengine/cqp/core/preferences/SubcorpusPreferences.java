package org.txm.searchengine.cqp.core.preferences;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

public class SubcorpusPreferences extends TXMPreferences {

	public static String EDITORMODE = "editor_mode"; //$NON-NLS-1$

	public static String OPTIMIZED_MODE = "optimized_mode"; //$NON-NLS-1$

	public static final String TARGET_STRATEGY = "target_strategy"; //$NON-NLS-1$

	public SubcorpusPreferences() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(SubcorpusPreferences.class)) {
			new SubcorpusPreferences();
		}
		return TXMPreferences.instances.get(SubcorpusPreferences.class);
	}

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.putBoolean(EDITORMODE, false);
		preferences.putBoolean(OPTIMIZED_MODE, true);
	}
}
