// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.searchengine.cqp.core.functions.selection;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;
import org.txm.metadatas.Metadata;
import org.txm.metadatas.MetadataGroup;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * Used to build sub-corpus of texts.
 * It links texts to their properties and compute some stats
 * 
 * @author mdecorde
 *
 */
public class Selection extends TXMResult {

	/** Full selection. */
	CQPCorpus corpus;

	/** The query to get texts metadatas and number. */
	CQLQuery query;

	/** The cqp positions of the texts start element. */
	int[] positions;

	/** The all properties. */
	List<StructuralUnitProperty> allProperties;

	/** The textids. */
	List<String> textids;

	/** The numberofwords. */
	HashMap<String, Integer> numberOfWordsPerText;

	/** The text metadatas values. */
	HashMap<String, HashMap<String, String>> metadatasValuesPerText;

	HashMap<String, List<String>> valuesPerMetadata;

	/** The metadatas and groups. */
	ArrayList<Metadata> metadatas;

	ArrayList<MetadataGroup> metadatagroups;

	/** The property and group names per usage. */
	private ArrayList<String> selectionPropertyNames;

	private ArrayList<String> displayPropertyNames;

	private ArrayList<String> selectionGroupNames;

	private ArrayList<String> displayGroupNames;

	/** the list of property used per group **/
	private HashMap<String, List<String>> propertyNamesPerGroup;

	/**
	 * Instantiates a new selection.
	 *
	 * @param corpus the corpus
	 */
	public Selection(MainCorpus corpus) {
		super(corpus);
		// this query returns N results, where N is the number of text
		this.query = new CQLQuery("<text>[]"); //$NON-NLS-1$
		this.corpus = corpus;
		init();
	}

	/**
	 * get positions, properties
	 * and compute texts stats (but not words)
	 * and get texts metadatas.
	 */
	private boolean init() {
		try {
			if (corpus.getProject() == null) {
				System.out.println(TXMCoreMessages.selectionErrorColonTheCorpus + corpus + TXMCoreMessages.hasNoBase);
				return false;
			}
			metadatas = corpus.getImportMetadata();
			metadatagroups = corpus.getMetadataGroups();

			// get texts positions
			QueryResult result = corpus.query(query, "selection", false); //$NON-NLS-1$
			List<Match> matches = result.getMatches();
			result.drop();
			// System.out.println("Nb of texts: "+matches.size());
			positions = new int[matches.size()]; // starting positions of texts in the corpus
			for (int i = 0; i < matches.size(); i++)
				positions[i] = matches.get(i).getStart();

			// select used structuralUnitsProperties in corpus structuralUnitsProperties
			StructuralUnit text_su = corpus.getStructuralUnit("text"); //$NON-NLS-1$
			allProperties = new ArrayList<>();
			if (metadatas.size() > 0) {
				for (Metadata m : metadatas) {
					try {
						StructuralUnitProperty sup = text_su.getProperty(m.id);
						if (sup == null) {
							Log.warning(TXMCoreMessages.cantFindStructuralUnitForMetadataWithIdColon + m.id);
							continue;
						}
						allProperties.add(sup);
					}
					catch (Exception e) {
						Log.severe(TXMCoreMessages.errorWhileGettingMetadatasFromBaseimportMetadatasColon + e);
					}
				}
			}

			// in case no metadatas are declared in import.xml
			if (allProperties.size() == 0)
				for (StructuralUnitProperty sup : text_su.getOrderedProperties())
				allProperties.add(sup);

			// get text ids (using starting position of texts) + text size using the positions
			StructuralUnitProperty sup = text_su.getProperty("id"); //$NON-NLS-1$
			int[] idx = CorpusManager.getCorpusManager().getCqiClient().cpos2Struc(sup.getQualifiedName(), positions);
			// System.out.println("cpos2Struc: "+Arrays.toString(idx));
			String[] textsIdArray = CorpusManager.getCorpusManager().getCqiClient().struc2Str(sup.getQualifiedName(), idx);
			// System.out.println("struc2Str: "+Arrays.toString(textsIdArray));
			textids = Arrays.asList(textsIdArray);

			metadatasValuesPerText = new HashMap<>();
			numberOfWordsPerText = new HashMap<>();
			for (int i = 0; i < textids.size(); i++) {
				this.metadatasValuesPerText.put(textids.get(i), new HashMap<String, String>());

				// QueryResult result2 = corpus.query(new Query("[_.text_id=\""+textids.get(i)+"\"] expand to text"), "temp", false); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				QueryResult result2 = corpus.query(new CQLQuery("/region[text,a]:: a.text_id=\"" + textids.get(i) + "\""), "temp", false); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				int tot = 0;
				for (Match m : result2.getMatches(0, 0))
					tot += m.getEnd() - m.getStart();
				numberOfWordsPerText.put(textids.get(i), tot);
				result2.drop();
			}

			// System.out.println(textMetadatas);
			// INITIALIZATIONS
			for (StructuralUnitProperty property : allProperties) {
				readTextMetadatas(property);
			}

			getTextMetadatas();
			getStructuralUnitPropertyValues();
			getDisplayPropertyNames();
			getSelectionPropertyNames();
			getDisplayGroupNames();
			getSelectionGroupNames();
			getMetadataGroupNames();
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	public CQPCorpus getCorpus() {
		return this.corpus;
	}

	public HashMap<String, List<String>> getMetadataGroupNames() {
		if (propertyNamesPerGroup != null)
			return propertyNamesPerGroup;
		propertyNamesPerGroup = new HashMap<>();
		for (MetadataGroup gp : metadatagroups) {
			ArrayList<String> list = new ArrayList<>();
			System.out.println("gp: " + gp.id); //$NON-NLS-1$
			propertyNamesPerGroup.put(gp.id, list);
			for (Metadata m : gp.metadatas)
				list.add(m.id);
		}
		return propertyNamesPerGroup;
	}

	/**
	 * print the full texts stats.
	 *
	 * @throws CqiClientException the cqi client exception
	 */
	public void fullDump() throws CqiClientException {
		System.out.println(TXMCoreMessages.textMetadataColon);
		System.out.println(TXMCoreMessages.corpusColon + corpus);
		System.out.println("Text query: " + query); //$NON-NLS-1$
		System.out.println("Texts ids: " + textids); //$NON-NLS-1$
		System.out.println("Text positions: " + Arrays.toString(positions)); //$NON-NLS-1$
		System.out.println("Text nbwords: " + numberOfWordsPerText); //$NON-NLS-1$
		System.out.println();
		System.out.println("StructuralUnitsProperties: " + allProperties); //$NON-NLS-1$
		System.out.println("Meta values per texts: " + metadatasValuesPerText); //$NON-NLS-1$
		System.out.println("Meta values per name: " + valuesPerMetadata); //$NON-NLS-1$
		System.out.println();
		System.out.println("Declared metadatas: " + metadatas); //$NON-NLS-1$
		System.out.println("Declared metadata groups: " + metadatagroups); //$NON-NLS-1$
		System.out.println(""); //$NON-NLS-1$
		System.out.println("Selection props: " + selectionPropertyNames); //$NON-NLS-1$
		System.out.println("Displayed props: " + displayPropertyNames); //$NON-NLS-1$
		System.out.println("Selection groups: " + selectionGroupNames); //$NON-NLS-1$
		System.out.println("Displayed groups: " + displayGroupNames); //$NON-NLS-1$
		System.out.println("Properties used per group: " + propertyNamesPerGroup); //$NON-NLS-1$
	}

	/**
	 * Gets the number of word.
	 *
	 * @param property the property
	 * @param value the value
	 * @return the number of words which 'property' value matches param 'value'
	 */
	public int getNumberOfWord(StructuralUnitProperty property, String value) {
		int nb = 0;
		for (String text : metadatasValuesPerText.keySet())
			if (metadatasValuesPerText.get(text).get(property.getName()).equals(value))
				nb += numberOfWordsPerText.get(text);
		return nb;
	}

	/**
	 * Gets the text number.
	 *
	 * @param property the property
	 * @param value the value
	 * @return the number of text which 'property' value matches param 'value'
	 */
	public int getNumberOfText(StructuralUnitProperty property, String value) {
		int nb = 0;
		for (String text : metadatasValuesPerText.keySet())
			if (metadatasValuesPerText.get(text).get(property.getName()).equals(value))
				nb++;
		return nb;
	}

	/**
	 * Loads the metadatas values and store them per text id.
	 *
	 * @param property the property
	 * @return the text metadatas
	 * @throws UnexpectedAnswerException the unexpected answer exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	private HashMap<String, HashMap<String, String>> readTextMetadatas(StructuralUnitProperty property) throws UnexpectedAnswerException, IOException, CqiServerError {
		int[] idx = CorpusManager.getCorpusManager().getCqiClient().cpos2Struc(property.getQualifiedName(), positions);
		String[] values = CorpusManager.getCorpusManager().getCqiClient().struc2Str(property.getQualifiedName(), idx);

		for (int i = 0; i < values.length; i++) {
			this.metadatasValuesPerText.get(textids.get(i)).put(property.getName(), values[i]);
		}

		return metadatasValuesPerText;
	}

	/**
	 * Gets the text metadatas values per text.
	 *
	 * @return the text metadatas
	 */
	public HashMap<String, HashMap<String, String>> getTextMetadatas() {
		return this.metadatasValuesPerText;
	}

	/**
	 * Gets the number of words.
	 *
	 * @return the number of words
	 */
	public HashMap<String, Integer> getNumberOfWordsPerText() {
		return this.numberOfWordsPerText;
	}

	/**
	 * Gets the text ids.
	 *
	 * @return the text ids, corpus ordered
	 */
	public List<String> getTextIds() {
		return this.textids;
	}

	/**
	 * Gets and loads the selection properties name.
	 *
	 * @return the selection text metadata
	 */
	public ArrayList<String> getSelectionPropertyNames() {
		if (selectionPropertyNames != null)
			return selectionPropertyNames;
		StructuralUnit text_su;
		selectionPropertyNames = new ArrayList<>();
		/*
		 * try {
		 * text_su = corpus.getStructuralUnit("text");
		 * // if corpus has metadatas declared
		 * if (metadatas.size() > 0)
		 * {
		 * for(Metadata m : metadatas)
		 * {
		 * selectionPropertyNames.add(m.id);
		 * }
		 * }
		 * //if no props added via metadata then get all text properties
		 * if (selectionPropertyNames.size() == 0)
		 * {
		 * for(Property p : text_su.getProperties())
		 * selectionPropertyNames.add(p.getName());
		 * }
		 * } catch (Exception e) {
		 * // TODO Auto-generated catch block
		 * org.txm.utils.logger.Log.printStackTrace(e);
		 * } //$NON-NLS-1$
		 */
		for (StructuralUnitProperty prop : allProperties)
			selectionPropertyNames.add(prop.getName());
		return selectionPropertyNames;
	}

	/**
	 * Gets and loads the selection group names.
	 *
	 * @return the selection text metadata
	 */
	public ArrayList<String> getSelectionGroupNames() {
		if (selectionGroupNames != null)
			return selectionGroupNames;
		selectionGroupNames = new ArrayList<>();
		for (Metadata m : metadatagroups) {
			if (m.selection) {
				selectionGroupNames.add(m.id);
			}
		}

		return selectionGroupNames;
	}

	/**
	 * Gets the display properties name.
	 *
	 * @return the display text metadata
	 */
	public ArrayList<String> getDisplayPropertyNames() {
		if (displayPropertyNames != null)
			return displayPropertyNames;
		StructuralUnit text_su;
		displayPropertyNames = new ArrayList<>();
		try {
			text_su = corpus.getStructuralUnit("text"); //$NON-NLS-1$

			// if corpus has metadatas declared
			if (metadatas.size() > 0) {
				for (Metadata m : metadatas) {
					if (m.display) // only keep metadata for selection
						displayPropertyNames.add(m.id);
				}
			}

			if (displayPropertyNames.size() == 0) {
				for (Property p : text_su.getOrderedProperties())
					displayPropertyNames.add(p.getName());
			}

		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return displayPropertyNames;
	}

	/**
	 * Gets and loads the display group names.
	 *
	 * @return the display text metadata
	 */
	public ArrayList<String> getDisplayGroupNames() {
		if (displayGroupNames != null)
			return displayGroupNames;
		displayGroupNames = new ArrayList<>();
		for (Metadata m : metadatagroups) {
			if (m.display) // only keep metadata for selection
				displayGroupNames.add(m.id);
		}
		return displayGroupNames;
	}

	/**
	 * Gets the structural unit property values per structuralUnitProperty.
	 *
	 * @return
	 */
	public HashMap<String, List<String>> getStructuralUnitPropertyValues() {
		if (valuesPerMetadata != null)
			return valuesPerMetadata;
		valuesPerMetadata = new HashMap<>();

		try {
			for (StructuralUnitProperty sup : allProperties) {
				try {
					valuesPerMetadata.put(sup.getName(), sup.getValues());
				}
				catch (CqiClientException e) {
					System.out.println(TXMCoreMessages.failedToGetSupValuesOf + sup.getFullName());
					org.txm.utils.logger.Log.printStackTrace(e);
					return valuesPerMetadata;
				}
			}
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return valuesPerMetadata;
	}

	public ArrayList<Metadata> getMetadatas() {
		return metadatas;
	}

	public ArrayList<MetadataGroup> getMetadatagroups() {
		return metadatagroups;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSimpleName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean canCompute() {
		return this.corpus != null;
	}

	@Override
	public boolean loadParameters() {
		// TODO Auto-generated method stub
		System.err.println("Selection.loadParameters(): not yet implemented."); //$NON-NLS-1$
		return true;
	}

	@Override
	public boolean saveParameters() {
		// TODO Auto-generated method stub
		System.err.println("Selection.saveParameters(): not yet implemented."); //$NON-NLS-1$
		return true;
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {
		System.err.println("Selection.compute() not implemented"); //$NON-NLS-1$
		return false;
	}

	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getResultType() {
		return "Selection"; //$NON-NLS-1$
	}
}
