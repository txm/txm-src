package org.txm.searchengine.cqp.core.functions.selection;

import java.util.ArrayList;
import java.util.HashMap;

public class SelectionResult extends ArrayList<String> {

	public HashMap<String, ArrayList<String>> critera = new HashMap<String, ArrayList<String>>();

	@Override
	public String toString() {
		return super.toString() + critera.toString();
	}
}
