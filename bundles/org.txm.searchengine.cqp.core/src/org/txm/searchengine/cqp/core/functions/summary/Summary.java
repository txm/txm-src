package org.txm.searchengine.cqp.core.functions.summary;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.objects.Edition;
import org.txm.objects.Page;
import org.txm.objects.Text;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.core.messages.CQPSearchEngineCoreMessages;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

public class Summary extends TXMResult {

	MainCorpus corpus;

	TreeNode treenodes = new TreeNode("root"); //$NON-NLS-1$

	@Parameter(key = "props")
	protected ArrayList<StructuralUnitProperty> properties;

	public Summary(MainCorpus corpus) {
		super(corpus);
		this.corpus = corpus;
	}

	@Override
	public boolean canCompute() {
		return corpus != null && properties != null && properties.size() > 0;
	}

	@Override
	public boolean loadParameters() {
		System.err.println("Summary.loadParameters(): not yet implemented."); //$NON-NLS-1$
		return true;
	}


	@Override
	public boolean saveParameters() {
		System.err.println("Summary.saveParameters(): not yet implemented."); //$NON-NLS-1$
		return true;
	}


	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {
		treenodes.end = corpus.getSize();
		treenodes.id = corpus.getID();
		treenodes.start = 0;
		treenodes.parent = null;
		treenodes.clear();
		// get all type of structures
		for (int j = 0; j < properties.size(); j++) {
			String suname = properties.get(j).getStructuralUnit().getName();
			// System.out.println("getting : "+properties.get(j).getFullName());

			QueryResult result = corpus.query(new CQLQuery("<" + properties.get(j).getFullName() + ">[]"), "summary", false); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			int[] starts = result.getStarts();
			//result.drop();
			result = corpus.query(new CQLQuery("[]</" + properties.get(j).getFullName() + ">"), "summary", false); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			int[] ends = result.getStarts();
			//result.drop();
			// System.out.println("starts: "+Arrays.toString(starts));
			// System.out.println("ends: "+Arrays.toString(ends));
			int[] strcutpos = CQPSearchEngine.getCqiClient().cpos2Struc(properties.get(j).getQualifiedName(), starts);
			String[] infos = CQPSearchEngine.getCqiClient().struc2Str(properties.get(j).getQualifiedName(), strcutpos);
			// allMatches.put(suname, list);
			if (starts.length != ends.length) {
				System.out.println(suname + ": starts.length " + starts.length + " != ends.length " + ends.length); //$NON-NLS-1$ //$NON-NLS-2$
				return false;
			}

			// create the nodes list
			for (int i = 0; i < starts.length; i++) {
				// System.out.println("suname: "+suname+" start: "+m.getStart());
				TreeNode node = new TreeNode();
				node.start = starts[i];
				node.end = ends[i];
				node.struct = suname;
				node.id = infos[i];
				treenodes.add(node);
			}
		}
		// this.print();

		// System.out.println("sorting... "+treenodes.size()+" nodes");
		// sorting and create hierarchy nodes
		processNodeList(treenodes);

		// System.out.println("checking result..."+treenodes.size()+" nodes");
		if (treenodes.check()) {
			return true;
		}
		else {
			return false;
		}
	}

	public TreeNode getRoot() {
		return treenodes;
	}

	private void processNodeList(TreeNode nodes) {
		// System.out.println("+processing node list: "+nodes.size());
		if (nodes.size() == 0) {
			return;
		}

		// 1st loop
		// System.out.println("1st loop: "+nodes.size());
		for (int i = 0; i < nodes.size(); i++) {
			TreeNode node = nodes.get(i);
			if (node.end == -1)
				System.out.println("ERROR: node.end = -1"); //$NON-NLS-1$
			for (int j = 0; j < nodes.size(); j++) {
				if (j != i) {
					TreeNode n2 = nodes.get(j);
					if (n2.start >= node.start && n2.end <= node.end) {
						n2.parent = node;
						node.add(n2);
						nodes.remove(j);
						j--;
						if (j < i) {
							i--;
						}
					}
				}
			}
		}

		// process sublists
		// System.out.println("processing sublists: "+nodes.size());
		for (int i = 0; i < nodes.size(); i++) {
			TreeNode node = nodes.get(i);
			processNodeList(node);
		}
	}

	public ArrayList<StructuralUnitProperty> getProperties() {
		return properties;
	}

	public void print() {
		for (TreeNode node : treenodes) {
			System.out.println("\n" + node); //$NON-NLS-1$
		}
	}


	public Page getDefaultEditionPage(TreeNode node) throws Exception {
		StructuralUnitProperty text_id = corpus.getStructuralUnit("text").getProperty("id"); //$NON-NLS-1$ //$NON-NLS-2$
		Property w_id = corpus.getProperty("id"); //$NON-NLS-1$
		int[] positions = { node.getStart() };
		final String[] wordids = CQPSearchEngine.getCqiClient().cpos2Str(w_id.getQualifiedName(), positions);
		int[] textidstruct = CQPSearchEngine.getCqiClient().cpos2Struc(text_id.getQualifiedName(), positions);
		String[] textids = CQPSearchEngine.getCqiClient().struc2Str(text_id.getQualifiedName(), textidstruct);

		if (wordids.length == 1 && textids.length == 1) {
			Text text = corpus.getProject().getText(textids[0]);
			if (text == null) {
				Log.severe(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.errorColonTextP0IsNull, textids[0]));
				return null;
			}

			Edition edition = text.getEdition("default"); //$NON-NLS-1$
			if (edition == null) {
				if (text.getEditions().size() == 0) {
					System.out.println("No edition for text: " + text);
				}
				else {
					for (Edition ed : text.getEditions()) {
						edition = ed;
						break;
					}
				}
			}
			if (edition == null) {
				System.out.println(CQPSearchEngineCoreMessages.errorColonEditionDefaultIsNull);
				return null;
			}

			node.wordid = wordids[0];
			return edition.getPageForWordId(wordids[0]);
		}
		else {
			Log.severe(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.errorColonTextidsP0AndWordidsP1, Arrays.toString(textids), Arrays.toString(wordids)));
			return null;
		}
	}

	public class TreeNode extends ArrayList<TreeNode> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public TreeNode parent;

		boolean leaf = false;

		String struct;

		String id;

		int start = -1;

		int end = -1;

		public String wordid;

		public TreeNode() {

		}

		public boolean check() {
			for (TreeNode node : this) {
				if (node.start >= this.start && node.end <= this.end && (node.end - node.start) < (this.end - this.start))
					return node.check();
				else
					return false;
			}
			return true;
		}

		public TreeNode(String struct) {
			this();
			this.struct = struct;
		}

		@Override
		public String toString() {
			String str = struct + "(" + start + ", " + end + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			if (size() > 0) {
				str += "\n\t" + super.toString(); //$NON-NLS-1$
			}
			return str;
		}

		public TreeNode getParent() {
			return parent;
		}

		public String getInfo() {
			return id;
		}

		public int getStart() {
			return start;
		}

		public String getWordId() {
			return wordid;
		}
	}

	@Override
	public String getName() {
		if (properties != null && properties.size() > 0) {
			return corpus.getName();
		}
		else {
			return StringUtils.join(properties, ", "); //$NON-NLS-1$
		}
	}

	@Override
	public String getSimpleName() {
		if (properties != null && properties.size() > 0) {
			return corpus.getName();
		}
		else {
			return StringUtils.join(properties, ", "); //$NON-NLS-1$
		}
	}

	@Override
	public String getDetails() {
		// TODO Auto-generated method stub
		return "" + corpus + " " + properties; //$NON-NLS-1$
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub
	}


	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {

		return false;
	}

	public void setProperties(ArrayList<StructuralUnitProperty> properties) {
		this.properties = properties;
	}


	@Override
	public String getResultType() {
		return "Summary"; //$NON-NLS-1$
	}

	public MainCorpus getCorpus() {
		return corpus;
	}
}
