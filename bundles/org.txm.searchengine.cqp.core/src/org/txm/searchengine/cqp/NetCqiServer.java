// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2013-09-05 09:27:13 +0200 (jeu., 05 sept. 2013) $
// $LastChangedRevision: 2529 $
// $LastChangedBy: mdecorde $
//
package org.txm.searchengine.cqp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.txm.searchengine.core.messages.SearchEngineCoreMessages;
import org.txm.searchengine.cqp.clientExceptions.ServerNotFoundException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.core.messages.CQPSearchEngineCoreMessages;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * This class aims at handling a CQi server process. It is not a CQi server per
 * se.
 * 
 * @author Jean-Philippe Magué
 */
public class NetCqiServer extends AbstractCqiServer {

	/** The cmd line. */
	String[] cmdLine;

	/**
	 * Gets the last cmd line.
	 *
	 * @return the last cmd line
	 */
	@Override
	public String getLastCmdLine() {
		return StringUtils.join(cmdLine, " "); //$NON-NLS-1$
	}

	/** The additional options. */
	String additionalOptions;

	public boolean debug = false;

	/** The monitor output. */
	boolean monitorOutput;

	/** The path to executable. */
	String pathToExecutable;

	/** The path to init file. */
	String pathToInitFile;

	/** The path to registry. */
	String pathToRegistry;

	/** The process. */
	private Process process;

	/**
	 * Instantiates a new CQi server.
	 * 
	 * @param pathToExecutable
	 *            the path to the executable
	 * @param pathToRegistry
	 *            the path to the registry
	 * @param pathToInitFile
	 *            the path to the init file
	 * 
	 * @throws ServerNotFoundException
	 *             the server not found exception
	 */
	public NetCqiServer(String pathToExecutable, String pathToRegistry, String pathToInitFile) throws ServerNotFoundException {
		this(pathToExecutable, pathToRegistry, pathToInitFile, ""); //$NON-NLS-1$
	}

	/**
	 ** Instantiates a new CQi server.
	 * 
	 * @param pathToExecutable
	 *            the path to the executable
	 * @param pathToRegistry
	 *            the path to the registry
	 * @param pathToInitFile
	 *            the path to the init file
	 * @param additionalOptions
	 *            the additional command line options
	 * 
	 * @throws ServerNotFoundException
	 *             the server not found exception
	 */
	public NetCqiServer(String pathToExecutable, String pathToRegistry, String pathToInitFile, String additionalOptions) throws ServerNotFoundException {
		this(pathToExecutable, pathToRegistry, pathToInitFile, additionalOptions, false);
	}

	/**
	 * Instantiates a new cqi server.
	 *
	 * @param pathToExecutable
	 *            the path to executable
	 * @param pathToRegistry
	 *            the path to registry
	 * @param pathToInitFile
	 *            the path to init file
	 * @param additionalOptions
	 *            the additional options
	 * @param monitorOutput
	 *            the monitor output
	 */
	public NetCqiServer(String pathToExecutable, String pathToRegistry, String pathToInitFile, String additionalOptions, boolean monitorOutput) {

		this.pathToExecutable = pathToExecutable;
		this.pathToRegistry = pathToRegistry;
		this.pathToInitFile = pathToInitFile;
		this.additionalOptions = additionalOptions;
		this.monitorOutput = monitorOutput;
	}

	/**
	 * Convert stream to string.
	 *
	 * @param is
	 *            the is
	 * @return the string
	 */
	@Override
	public String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		}
		catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		finally {
			try {
				is.close();
			}
			catch (IOException e) {
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}

		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		try {
			this.stop();
		}
		finally {
			super.finalize();
		}
	}

	/**
	 * Gets the error stream.
	 *
	 * @return the error stream
	 */
	@Override
	public InputStream getErrorStream() {
		return process.getErrorStream();
	}

	/**
	 * Gets the input stream.
	 *
	 * @return the input stream
	 */
	@Override
	public InputStream getInputStream() {
		return process.getInputStream();
	}

	/**
	 * Gets the output stream.
	 *
	 * @return the output stream
	 */
	@Override
	public OutputStream getOutputStream() {
		return process.getOutputStream();
	}

	/**
	 * Checks if the server is running.
	 * 
	 * @return true, if is running
	 */
	@Override
	public Boolean isRunning() {
		try {
			this.process.exitValue();// this throws an
										// IllegalThreadStateException if te
										// process is running
										// The IllegalThreadStateException has not been thrown, the process
										// is not running.
			return false;
		}
		catch (IllegalThreadStateException e) {
			// The IllegalThreadStateException has been thrown, all good.
			return true;
		}
	}

	@Override
	public void setDebug(boolean val) {
		debug = val;
	}

	/**
	 * Start.
	 *
	 * @return true, if successful
	 */
	@Override
	public Boolean start() {
		ArrayList<String> cmd = new ArrayList<String>();
		cmd.add(""); //$NON-NLS-1$ executable path will be foudn in preferences
		if (pathToInitFile != null && pathToInitFile.length() > 0) {
			cmd.add("-I");//$NON-NLS-1$
			cmd.add(pathToInitFile);
		}
		cmd.add("-r"); //$NON-NLS-1$
		cmd.add(pathToRegistry);
		cmd.addAll(Arrays.asList(additionalOptions.trim().split(" "))); //$NON-NLS-1$
		cmdLine = cmd.toArray(new String[cmd.size()]);
		Log.fine("Starting NullSearchEngineServer: " + cmd + " ... "); //$NON-NLS-1$

		try {
			// System.out.println(Messages.bind(Messages.STARTING_SERVER,
			// StringUtils.join(cmdLine, " "))); //$NON-NLS-1$
			ProcessBuilder processBuilder = new ProcessBuilder(cmdLine);
			processBuilder.redirectErrorStream(true);
			this.process = processBuilder.start();

			final InputStream stdout = this.process.getInputStream();
			final InputStream stderr = this.process.getErrorStream();

			Thread t1 = new Thread() {

				@Override
				public void run() {
					InputStreamReader isr = new InputStreamReader(stdout);
					BufferedReader br = new BufferedReader(isr);
					String line = null;
					try {
						while ((line = br.readLine()) != null) {
							if (debug)
								System.out.println(CQPSearchEngineCoreMessages.cqpserverStdoutColon + line);
						}
					}
					catch (IOException e) {
						Log.severe(Log.toString(e));
					}
				}
			};
			t1.start();

			Thread t2 = new Thread() {

				@Override
				public void run() {
					InputStreamReader isr = new InputStreamReader(stderr);
					BufferedReader br = new BufferedReader(isr);
					String line = null;
					try {
						while ((line = br.readLine()) != null) {
							if (debug)
								System.out.println(CQPSearchEngineCoreMessages.cqpserverStderrColon + line);
						}
					}
					catch (IOException e) {
						// TODO Auto-generated catch block
						org.txm.utils.logger.Log.printStackTrace(e);
					}
				}
			};
			t2.start();

			return isRunning();
		}
		catch (IOException e) {
			// System.err.println("CQP server failed to start with the following command
			// line: "
			// + StringUtils.join(cmd, " "));
		}
		return false;
	}

	/**
	 * Stop the server.
	 */
	@Override
	public Boolean stop() {
		Log.fine(SearchEngineCoreMessages.info_stoppingSearchEngine);

		try {
			process.destroy();
			process.waitFor();
		}
		catch (Exception e) {
			Log.finest(Log.toString(e));
			return false;
		}
		Log.fine(SearchEngineCoreMessages.info_searchEngineStopped);
		return true;
	}

	public static void main(String args[]) {
		try {
			NetCqiServer server = new NetCqiServer("C:\\Program files\\TXM\\cwb\\bin\\cqpserver", "C:\\Documents and Settings\\mdecorde\\TXM\\registry", "C:\\Program files\\TXM\\cwb\\cqpserver.init", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					"-b 1000000 -d OFF -P 4877"); //$NON-NLS-1$
			System.out.println(server.start());

			NetCqiClient client = new NetCqiClient("localhost", 4877); //$NON-NLS-1$
			System.out.println(client.connect("", "")); //$NON-NLS-1$ //$NON-NLS-2$
			server.stop();
		}
		catch (ServerNotFoundException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		catch (UnexpectedAnswerException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		catch (CqiServerError e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}
}
