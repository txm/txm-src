// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2011-12-14 10:52:18 +0100 (mer., 14 déc. 2011) $
// $LastChangedRevision: 2080 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.txm.libs.cqp.CQPLibPreferences;
import org.txm.searchengine.cqp.clientExceptions.ServerNotFoundException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.serverException.CqiCqpErrorNoSuchCorpus;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.ExecTimer;
import org.txm.utils.OSDetector;
import org.txm.utils.logger.Log;

/**
 * Memory implementation of CQP Server usqing the cqi JNI library
 * 
 * @author mdecorde
 */
public class MemCqiServer extends AbstractCqiServer {

	/**
	 * Instantiates a new CQi server.
	 * 
	 * @param pathToExecutable
	 *            the path to the executable
	 * @param pathToRegistry
	 *            the path to the registry
	 * @param pathToInitFile
	 *            the path to the init file
	 * 
	 * @throws ServerNotFoundException
	 *             the server not found exception
	 */
	public MemCqiServer(String pathToExecutable, String pathToRegistry,
			String pathToInitFile) throws ServerNotFoundException {
		this(pathToExecutable, pathToRegistry, pathToInitFile, ""); //$NON-NLS-1$
	}

	/**
	 ** Instantiates a new CQi server.
	 * 
	 * @param pathToExecutable
	 *            the path to the executable
	 * @param pathToRegistry
	 *            the path to the registry
	 * @param pathToInitFile
	 *            the path to the init file
	 * @param additionalOptions
	 *            the additional command line options
	 * 
	 * @throws ServerNotFoundException
	 *             the server not found exception
	 */
	public MemCqiServer(String pathToExecutable, String pathToRegistry,
			String pathToInitFile, String additionalOptions)
			throws ServerNotFoundException {

	}

	/** The path to executable. */
	String pathToExecutable;

	/** The path to registry. */
	String pathToRegistry;

	/** The path to init file. */
	String pathToInitFile;

	/** The additional options. */
	String additionalOptions;

	/** The monitor output. */
	boolean monitorOutput;

	/** The cmd line. */
	static String[] cmdLine;

	public boolean debug = false;

	@Override
	public void setDebug(boolean val) {
		debug = val;
	}

	public native synchronized int getErrorCode();


	/**
	 * Instantiates a new cqi server.
	 *
	 * @param pathToRegistry the path to registry
	 * @param pathToInitFile the path to init file
	 * @param additionalOptions the additional options
	 * @param monitorOutput the monitor output
	 * @throws Exception
	 */
	public MemCqiServer(String pathToRegistry, String pathToInitFile, String additionalOptions, boolean monitorOutput) throws Exception {

		this.pathToRegistry = pathToRegistry;
		this.pathToInitFile = pathToInitFile;
		this.additionalOptions = additionalOptions;
		this.monitorOutput = monitorOutput;
	}

	/**
	 * Gets the last cmd line.
	 *
	 * @return the last cmd line
	 */

	@Override
	public String getLastCmdLine() {
		return StringUtils.join(cmdLine, " "); //$NON-NLS-1$
	}

	/**
	 * Start.
	 *
	 * @return true, if successful
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 */
	@Override
	public Boolean start() throws Exception {

		if (!isLoaded) {

			//			Field LIBRARIES = ClassLoader.class.getDeclaredField("loadedLibraryNames"); //$NON-NLS-1$
			//			LIBRARIES.setAccessible(true);

			// FIXME: Debug
			// System.err.println(">>>>>>>>>>>>>>>>>>>> MemCqiServer.start() " + this.getClass().getClassLoader());

			try {

				String cqplibpath = CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PATH_TO_CQPLIB);
				if (cqplibpath == null || cqplibpath.length() == 0) {
					System.err.println("Error: cqp lib path is not set in preferences");
					return false;
				}
				String path = System.getProperty("java.library.path"); //$NON-NLS-1$
				//				String libpath = System.getProperty("path.separator") + cqplibpath; //$NON-NLS-1$
				//				
				//				if (!path.contains(libpath)) {
				//					path += libpath;
				//				}
				//				System.setProperty("java.library.path", path); //$NON-NLS-1$
				//				// System.out.println("Current java.library.path "+System.getProperty("java.library.path"));
				//				
				//				// this is a hack to force the JVM to reload java.library.path
				//				Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths"); //$NON-NLS-1$
				//				fieldSysPath.setAccessible(true);
				//				fieldSysPath.set(null, null);


				//System.loadLibrary("cqpjni"); //$NON-NLS-1$
				if (OSDetector.isFamilyWindows()) {

					System.load(cqplibpath + "/dependencies/libcharset-1.dll"); //$NON-NLS-1$
					//System.load(cqplibpath+"/dependencies/libgnurx-0.dll"); //$NON-NLS-1$
					System.load(cqplibpath + "/dependencies/libiconv-2.dll"); //$NON-NLS-1$
					System.load(cqplibpath + "/dependencies/libintl-8.dll"); //$NON-NLS-1$
					System.load(cqplibpath + "/dependencies/libglib-2.0-0.dll"); //$NON-NLS-1$
					System.load(cqplibpath + "/dependencies/pcre3.dll"); //$NON-NLS-1$
					System.load(cqplibpath + "/dependencies/libpcre-1.dll"); //$NON-NLS-1$
					System.load(cqplibpath + "/cqpjni.dll"); //$NON-NLS-1$
				}
				else if (OSDetector.isFamilyMac()) {
					System.load(cqplibpath + "/libcqpjni.dylib"); //$NON-NLS-1$
				}
				else {
					System.load(cqplibpath + "/libcqpjni.so"); //$NON-NLS-1$
				}

				// FIXME: SJ: tests to use absolute path rather than lib name in Linux.
				// It works and may avoid some problems about the JRE versions (e.g. Oracle or OpenJDK) and PATH configurations, but need to have the lib filename for each OS
				// System.load(cqplibpath + "libcqpjni.so"); //$NON-NLS-1$

				Log.fine("cqpjni loaded from dirs: " + path + "."); //$NON-NLS-1$
				isLoaded = true;
			}
			catch (Throwable e) {
				Log.severe(NLS.bind("MemCqiServer: Failed to load CQP lib with exception: {0}.", e));
				Log.warning(NLS.bind("Tried to load Cqi libs in: {0}.", System.getProperty("java.library.path")));
				Log.warning(NLS.bind("Current directory: {0}.", new File(".").getAbsolutePath()));
				throw new Exception(e);
			}
		}

		// String[] cmd = { "", "-I", pathToInitFile, "-r", pathToRegistry }; //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<String> cmd = new ArrayList<>();
		cmd.add(""); //$NON-NLS-1$ executable path will be foudn in preferences
		if (pathToInitFile != null && pathToInitFile.length() > 0) {
			cmd.add("-I");//$NON-NLS-1$
			cmd.add(pathToInitFile);
		}
		cmd.add("-r"); //$NON-NLS-1$
		cmd.add(pathToRegistry);
		cmd.addAll(Arrays.asList(additionalOptions.trim().split(" "))); //$NON-NLS-1$
		cmdLine = cmd.toArray(new String[cmd.size()]);
		Log.fine(NLS.bind("Starting NullSearchEngineServer: {0} ... ", cmd)); //$NON-NLS-1$

		// try {
		isLoaded = start(cmdLine);
		// System.out.println("IS LOADED: "+isLoaded);
		if (isLoaded) {
			// System.out.println(TXMCoreMessages.CqiClient_34);
		}
		else {
			Log.severe("Failed to start CQP Search Engine in memory mode.");
		}
		return isLoaded;
	}

	/**
	 * Convert stream to string.
	 *
	 * @param is the is
	 * @return the string
	 */

	@Override
	public String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		}
		catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		finally {
			try {
				is.close();
			}
			catch (IOException e) {
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}

		return sb.toString();
	}

	/**
	 * Checks if the server is running.
	 * 
	 * @return true, if is running
	 */

	@Override
	public Boolean isRunning() {
		return isLoaded;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {

	}

	/**
	 * Gets the error stream.
	 *
	 * @return the error stream
	 */

	@Override
	public InputStream getErrorStream() {
		return null;
	}

	/**
	 * Gets the input stream.
	 *
	 * @return the input stream
	 */

	@Override
	public InputStream getInputStream() {
		return new InputStream() {

			@Override
			public int read() throws IOException {
				return 0;
			}
		};
	}

	/**
	 * Gets the output stream.
	 *
	 * @return the output stream
	 */

	@Override
	public OutputStream getOutputStream() {
		return new OutputStream() {

			@Override
			public void write(int arg0) throws IOException {
			}
		};
	}

	static Boolean isLoaded = false;

	public native synchronized Boolean start(String[] args);

	public native synchronized Boolean load_a_system_corpus(String regfilepath, String entryname) throws IOException, UnexpectedAnswerException, CqiServerError;

	@Override
	public native synchronized Boolean stop();

	public native synchronized int[] alg2Cpos(String arg0, int arg1) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized Integer attributeSize(String arg0) throws IOException, UnexpectedAnswerException, CqiServerError;

	public boolean connect(String arg0, String arg1) {
		return true;
	};

	public native synchronized String[] corpusAlignementAttributes(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized String corpusCharset(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized String getOption(String name) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized String setOption(String name, String value) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized String corpusFullName(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized String[] corpusInfo(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized String[] corpusPositionalAttributes(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized String[] corpusProperties(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized Boolean corpusStructuralAttributeHasValues(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized String[] corpusStructuralAttributes(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized int[] cpos2Alg(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized int[] cpos2Id(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized int[] cpos2LBound(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized int[] cpos2RBound(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized String[] cpos2Str(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized int[] cpos2Struc(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError;

	// TODO to implement in CQi and MemCQiServer
	// public native synchronized int[] cpos2Struc2cpos(String arg0, ...) throws UnexpectedAnswerException, IOException, CqiServerError ;

	// TODO to implement in CQi and MemCQiServer
	// public native synchronized String[] cpos2struc2str(String arg0, ...) throws UnexpectedAnswerException, IOException, CqiServerError ;

	public native synchronized Boolean cqpQuery(String arg0, String arg1, String arg2)
			throws IOException, UnexpectedAnswerException, CqiServerError;

	public native synchronized Boolean query(String arg0)
			throws IOException, UnexpectedAnswerException, CqiServerError;

	public Boolean disconnect() {
		return true;
	};

	public native synchronized Boolean dropAttribute(String arg0) throws IOException, UnexpectedAnswerException, CqiServerError;

	public native synchronized Boolean dropCorpus(String arg0) throws Exception;

	public native synchronized Boolean dropSubCorpus(String arg0) throws IOException, UnexpectedAnswerException, CqiServerError;

	public native synchronized int[] dumpSubCorpus(String arg0, byte arg1, int arg2, int arg3)
			throws IOException, UnexpectedAnswerException, CqiServerError;

	public int[][] fdist1(String arg0, int arg1, byte arg2, String arg3)
			throws IOException, UnexpectedAnswerException, CqiServerError {
		int[] all = nfdist1(arg0, arg1, arg2, arg3);
		if (all == null) {
			return new int[0][0];
		}
		int[][] freqs = new int[all.length / 2][2];
		int c = 0;
		for (int i = 0; i < freqs.length; i++) {
			freqs[i][0] = all[c++];
			freqs[i][1] = all[c++];
		}
		return freqs;

	}

	public native synchronized int[] nfdist1(String arg0, int arg1, byte arg2, String arg3)
			throws IOException, UnexpectedAnswerException, CqiServerError;

	public int[][] fdist2(String arg0, int arg1, byte arg2, String arg3,
			byte arg4, String arg5) throws IOException,
			UnexpectedAnswerException, CqiServerError {
		int[] all = nfdist2(arg0, arg1, arg2, arg3, arg4, arg5);
		if (all == null) {
			return new int[0][0];
		}
		int[][] freqs = new int[all.length / 2][3];
		int c = 0;
		for (int i = 0; i < freqs.length; i++) {
			freqs[i][0] = all[c++];
			freqs[i][1] = all[c++];
			freqs[i][2] = all[c++];
		}
		return freqs;
	}

	public native synchronized int[] nfdist2(String arg0, int arg1, byte arg2, String arg3,
			byte arg4, String arg5) throws IOException, UnexpectedAnswerException, CqiServerError;

	public native synchronized String getLastCQPError();

	public native synchronized String getLastCqiError() throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized int[] id2Cpos(String arg0, int arg1) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized int[] id2Freq(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized String[] id2Str(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized int[] idList2Cpos(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized Integer lexiconSize(String arg0) throws IOException, UnexpectedAnswerException, CqiServerError;

	public native synchronized String[] listCorpora() throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized String[] listSubcorpora(String arg0) throws UnexpectedAnswerException, IOException, CqiCqpErrorNoSuchCorpus;

	public Boolean reconnect() {
		return true;
	};

	public native synchronized int[] regex2Id(String arg0, String arg1) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized int[] str2Id(String arg0, String[] arg1) throws IOException, UnexpectedAnswerException, CqiServerError;

	public native synchronized int[] struc2Cpos(String arg0, int arg1) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized String[] struc2Str(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError;

	public native synchronized Boolean subCorpusHasField(String arg0, byte arg1) throws IOException, UnexpectedAnswerException, CqiServerError;

	public native synchronized Integer subCorpusSize(String arg0) throws IOException, UnexpectedAnswerException, CqiServerError;

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		String pathToCQPLibDirectory = "/home/mdecorde/workspace047/org.txm.libs.cqp.linux/res/linux64/"; //$NON-NLS-1$
		String cqpserverPathToExecutable = pathToCQPLibDirectory + "cqpserver"; //$NON-NLS-1$
		String pathToRegistry = System.getProperty("user.home") + "TXM-0.8.x/corpora/DEMOCRATLYON5/registry"; //$NON-NLS-1$
		String pathToInitFile = System.getProperty("user.home") + "workspace047/org.txm.libs.cqp.linux/res/cqpserver.init"; //$NON-NLS-1$

		String corpus = "DEMOCRATLYON5"; //$NON-NLS-1$
		String subcorpusname = "SUB"; //$NON-NLS-1$
		String subcorpusid = corpus + ":" + subcorpusname; //$NON-NLS-1$
		String query = "[word=\"j.*\"]"; //$NON-NLS-1$
		String regex = "j.*"; //$NON-NLS-1$
		String pattribute1 = corpus + ".word"; //$NON-NLS-1$
		String pattribute2 = corpus + ".frpos"; //$NON-NLS-1$
		String pattribute3 = corpus + ".frlemma"; //$NON-NLS-1$
		String sattribute1 = corpus + ".text_id"; //$NON-NLS-1$
		String sattribute2 = corpus + ".p_id"; //$NON-NLS-1$
		String sattribute3 = corpus + ".s_id"; //$NON-NLS-1$

		String align_attr = "p_id"; //$NON-NLS-1$

		int[] cpos = { 0, 10000, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		// int id = 1;
		int[] ids = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		String[] strings = { "je", "jamais" }; //$NON-NLS-1$ //$NON-NLS-2$
		int[] strucpos = { 0, 1, 2, 3, 4 };

		CQPLibPreferences.getInstance().put(CQPLibPreferences.CQI_SERVER_PATH_TO_CQPLIB, pathToCQPLibDirectory);

		MemCqiServer server = new MemCqiServer(pathToRegistry, pathToInitFile, " -b 10000000 -d OFF", false); //$NON-NLS-1$
		if (server.start()) System.out.println("Mem Server ok"); //$NON-NLS-1$
		System.out.println("SERVER=" + server); //$NON-NLS-1$
		MemCqiClient client = new MemCqiClient(server);
		if (client.connect("anonymous", "")) System.out.println("Mem Client ok"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		NetCqiServer netServer = new NetCqiServer(cqpserverPathToExecutable, pathToRegistry, pathToInitFile, "", false); //$NON-NLS-1$
		if (netServer.start()) System.out.println("Net Server ok"); //$NON-NLS-1$
		NetCqiClient netClient = new NetCqiClient("127.0.0.1", 4877); //$NON-NLS-1$
		if (netClient.connect("anonymous", "")) System.out.println("Net Client ok"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		ExecTimer.start();

		try {
			System.out.println("client.alg2Cpos(sattribute, struct_id) : " + Arrays.toString(client.alg2Cpos(sattribute1, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.alg2Cpos(sattribute, struct_id) : " + Arrays.toString(netClient.alg2Cpos(sattribute1, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.attributeSize(sattribute1) : " + client.attributeSize(sattribute1)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.attributeSize(sattribute1) : " + netClient.attributeSize(sattribute1)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}


		try {
			System.out.println("client.attributeSize(sattribute2) : " + client.attributeSize(sattribute2)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.attributeSize(sattribute2) : " + netClient.attributeSize(sattribute2)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}


		try {
			System.out.println("client.attributeSize(sattribute3) : " + client.attributeSize(sattribute3)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.attributeSize(sattribute3) : " + netClient.attributeSize(sattribute3)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}


		try {
			System.out.println("client.corpusAlignementAttributes(corpus) : " + Arrays.toString(client.corpusAlignementAttributes(corpus))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.corpusAlignementAttributes(corpus) : " + Arrays.toString(netClient.corpusAlignementAttributes(corpus))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.corpusCharset(corpus) : " + client.corpusCharset(corpus)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.corpusCharset(corpus) : " + netClient.corpusCharset(corpus)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}


		try {
			System.out.println("client.corpusFullName(corpus) : " + client.corpusFullName(corpus)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.corpusFullName(corpus) : " + netClient.corpusFullName(corpus)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.corpusInfo(corpus) : " + client.corpusInfo(corpus)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.corpusInfo(corpus) : " + netClient.corpusInfo(corpus)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}


		try {
			System.out.println("client.corpusPositionalAttributes(corpus) : " + Arrays.toString(client.corpusPositionalAttributes(corpus))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.corpusPositionalAttributes(corpus) : " + Arrays.toString(netClient.corpusPositionalAttributes(corpus))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}


		try {
			System.out.println("client.corpusProperties(corpus) : " + Arrays.toString(client.corpusProperties(corpus))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.corpusProperties(corpus) : " + Arrays.toString(netClient.corpusProperties(corpus))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}


		try {
			System.out.println("client.corpusStructuralAttributeHasValues(sattribute1) : " + client.corpusStructuralAttributeHasValues(sattribute1)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.corpusStructuralAttributeHasValues(sattribute1) : " + netClient.corpusStructuralAttributeHasValues(sattribute1)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.corpusStructuralAttributeHasValues(sattribute2) : " + client.corpusStructuralAttributeHasValues(sattribute2)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.corpusStructuralAttributeHasValues(sattribute2) : " + netClient.corpusStructuralAttributeHasValues(sattribute2)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.corpusStructuralAttributeHasValues(sattribute3) : " + client.corpusStructuralAttributeHasValues(sattribute3)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.corpusStructuralAttributeHasValues(sattribute3) : " + netClient.corpusStructuralAttributeHasValues(sattribute3)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.corpusStructuralAttributes(corpus) : " + Arrays.toString(client.corpusStructuralAttributes(corpus))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.corpusStructuralAttributes(corpus) : " + Arrays.toString(netClient.corpusStructuralAttributes(corpus))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2Alg(align_attr, cpos) : " + Arrays.toString(client.cpos2Alg(align_attr, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.cpos2Alg(align_attr, cpos) : " + Arrays.toString(netClient.cpos2Alg(align_attr, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2Id(pattribute1, cpos) : " + Arrays.toString(client.cpos2Id(pattribute1, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.cpos2Id(pattribute1, cpos) : " + Arrays.toString(netClient.cpos2Id(pattribute1, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2Id(pattribute2, cpos) : " + Arrays.toString(client.cpos2Id(pattribute2, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.cpos2Id(pattribute2, cpos) : " + Arrays.toString(netClient.cpos2Id(pattribute2, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2Id(pattribute3, cpos) : " + Arrays.toString(client.cpos2Id(pattribute3, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.cpos2Id(pattribute3, cpos) : " + Arrays.toString(netClient.cpos2Id(pattribute3, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2LBound(sattribute1, cpos) : " + Arrays.toString(client.cpos2LBound(sattribute1, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.cpos2LBound(sattribute1, cpos) : " + Arrays.toString(netClient.cpos2LBound(sattribute1, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2LBound(sattribute2, cpos) : " + Arrays.toString(client.cpos2LBound(sattribute2, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.cpos2LBound(sattribute2, cpos) : " + Arrays.toString(netClient.cpos2LBound(sattribute2, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2LBound(sattribute3, cpos) : " + Arrays.toString(client.cpos2LBound(sattribute3, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.cpos2LBound(sattribute3, cpos) : " + Arrays.toString(netClient.cpos2LBound(sattribute3, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2RBound(sattribute1, cpos) : " + Arrays.toString(client.cpos2RBound(sattribute1, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.cpos2RBound(sattribute1, cpos) : " + Arrays.toString(netClient.cpos2RBound(sattribute1, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2RBound(sattribute2, cpos) : " + Arrays.toString(client.cpos2RBound(sattribute2, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.cpos2RBound(sattribute2, cpos) : " + Arrays.toString(netClient.cpos2RBound(sattribute2, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2RBound(sattribute3, cpos) : " + Arrays.toString(client.cpos2RBound(sattribute3, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.cpos2RBound(sattribute3, cpos) : " + Arrays.toString(netClient.cpos2RBound(sattribute3, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2Str(pattribute1, cpos) : " + Arrays.toString(client.cpos2Str(pattribute1, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.cpos2Str(pattribute1, cpos) : " + Arrays.toString(netClient.cpos2Str(pattribute1, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2Str(pattribute2, cpos) : " + Arrays.toString(client.cpos2Str(pattribute2, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.cpos2Str(pattribute2, cpos) : " + Arrays.toString(netClient.cpos2Str(pattribute2, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2Str(pattribute3, cpos) : " + Arrays.toString(client.cpos2Str(pattribute3, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.cpos2Str(pattribute3, cpos) : " + Arrays.toString(netClient.cpos2Str(pattribute3, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2Struc(sattribute1, cpos) : " + Arrays.toString(client.cpos2Struc(sattribute1, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e); //$NON-NLS-1$
		}
		try {
			System.out.println("netClient.cpos2Struc(sattribute1, cpos) : " + Arrays.toString(netClient.cpos2Struc(sattribute1, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.cpos2Struc(sattribute2, cpos) : " + Arrays.toString(client.cpos2Struc(sattribute2, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.cpos2Struc(sattribute2, cpos) : " + Arrays.toString(netClient.cpos2Struc(sattribute2, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.cpos2Struc(sattribute3, cpos) : " + Arrays.toString(client.cpos2Struc(sattribute3, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.cpos2Struc(sattribute3, cpos) : " + Arrays.toString(netClient.cpos2Struc(sattribute3, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.cqpQuery(corpus, subcorpus, query)"); //$NON-NLS-1$
			client.cqpQuery(corpus, subcorpusname, query);
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.cqpQuery(corpus, subcorpus, query)"); //$NON-NLS-1$
			netClient.cqpQuery(corpus, subcorpusname, query);
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.dropAttribute(pattribute1)"); //$NON-NLS-1$
			client.dropAttribute(pattribute1);
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.dropAttribute(pattribute1)"); //$NON-NLS-1$
			netClient.dropAttribute(pattribute1);
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.dropAttribute(pattribute2)"); //$NON-NLS-1$
			client.dropAttribute(pattribute2);
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.dropAttribute(pattribute2)"); //$NON-NLS-1$
			netClient.dropAttribute(pattribute2);
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.dropAttribute(pattribute3)"); //$NON-NLS-1$
			client.dropAttribute(pattribute3);
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.dropAttribute(pattribute3)"); //$NON-NLS-1$
			netClient.dropAttribute(pattribute3);
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.dropCorpus(corpus)"); //$NON-NLS-1$
			client.dropCorpus(corpus);
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.dropCorpus(corpus)"); //$NON-NLS-1$
			netClient.dropCorpus(corpus);
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.dumpSubCorpus(subcorpus, CQI_CONST_FIELD_MATCH, 1, 2) : " + Arrays.toString(client.dumpSubCorpus(subcorpusid, NetCqiClient.CQI_CONST_FIELD_MATCH, 1, 10))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.dumpSubCorpus(subcorpus, CQI_CONST_FIELD_MATCH, 1, 2) : " + Arrays.toString(netClient.dumpSubCorpus(subcorpusid, NetCqiClient.CQI_CONST_FIELD_MATCH, 1, 10))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.dumpSubCorpus(subcorpus, CQI_CONST_FIELD_MATCHEND, 1, 2) : " + Arrays.toString(client.dumpSubCorpus(subcorpusid, NetCqiClient.CQI_CONST_FIELD_MATCHEND, 1, 10))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.dumpSubCorpus(subcorpus, CQI_CONST_FIELD_MATCHEND, 1, 2) : " + Arrays.toString(netClient.dumpSubCorpus(subcorpusid, NetCqiClient.CQI_CONST_FIELD_MATCHEND, 1, //$NON-NLS-1$
					10)));
		}
		catch (Exception e) {
			System.out.println(e);
		}

		// try {
		// System.out.println("client.fdist1(arg0, arg1, arg2, arg3) : "+client.fdist1(arg0, arg1, arg2, arg3));
		// } catch(Exception e) { System.out.println("Exception : "+e);}
		// try {
		// System.out.println("netClient.fdist1(arg0, arg1, arg2, arg3) : "+netClient.fdist1(arg0, arg1, arg2, arg3));
		// } catch(Exception e) { System.out.println("Exception : "+e);}

		// try {
		// System.out.println("//client.fdist2(arg0, arg1, arg2, arg3, arg4, arg5) : "+client.fdist2(arg0, arg1, arg2, arg3, arg4, arg5));
		// } catch(Exception e) { System.out.println("Exception : "+e);}
		// try {
		// System.out.println("//netClient.fdist2(arg0, arg1, arg2, arg3, arg4, arg5) : "+netClient.fdist2(arg0, arg1, arg2, arg3, arg4, arg5));
		// } catch(Exception e) { System.out.println("Exception : "+e);}

		try {
			System.out.println("client.getLastCqiError() : " + client.getLastCqiError()); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.getLastCqiError() : " + netClient.getLastCqiError()); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.getLastCQPError() : " + client.getLastCQPError()); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.getLastCQPError() : " + netClient.getLastCQPError()); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.id2Cpos(pattribute1, 0) : " + Arrays.toString(client.id2Cpos(pattribute1, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.id2Cpos(pattribute1, 0) : " + Arrays.toString(netClient.id2Cpos(pattribute1, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.id2Cpos(pattribute2, 0) : " + Arrays.toString(client.id2Cpos(pattribute2, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.id2Cpos(pattribute2, 0) : " + Arrays.toString(netClient.id2Cpos(pattribute2, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.id2Cpos(pattribute3, 0) : " + Arrays.toString(client.id2Cpos(pattribute3, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.id2Cpos(pattribute3, 0) : " + Arrays.toString(netClient.id2Cpos(pattribute3, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.id2Cpos(pattribute1, 1) : " + Arrays.toString(client.id2Cpos(pattribute1, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.id2Cpos(pattribute1, 1) : " + Arrays.toString(netClient.id2Cpos(pattribute1, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.id2Cpos(pattribute2, 1) : " + Arrays.toString(client.id2Cpos(pattribute2, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.id2Cpos(pattribute2, 1) : " + Arrays.toString(netClient.id2Cpos(pattribute2, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.id2Cpos(pattribute3, 1) : " + Arrays.toString(client.id2Cpos(pattribute3, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.id2Cpos(pattribute3, 1) : " + Arrays.toString(netClient.id2Cpos(pattribute3, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.id2Cpos(pattribute1, 2) : " + Arrays.toString(client.id2Cpos(pattribute1, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.id2Cpos(pattribute1, 2) : " + Arrays.toString(netClient.id2Cpos(pattribute1, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.id2Cpos(pattribute2, 2) : " + Arrays.toString(client.id2Cpos(pattribute2, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.id2Cpos(pattribute2, 2) : " + Arrays.toString(netClient.id2Cpos(pattribute2, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.id2Cpos(pattribute3, 2) : " + Arrays.toString(client.id2Cpos(pattribute3, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.id2Cpos(pattribute3, 2) : " + Arrays.toString(netClient.id2Cpos(pattribute3, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.id2Freq(pattribute1, ids) : " + Arrays.toString(client.id2Freq(pattribute1, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.id2Freq(pattribute1, ids) : " + Arrays.toString(netClient.id2Freq(pattribute1, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.id2Freq(pattribute2, ids) : " + Arrays.toString(client.id2Freq(pattribute2, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.id2Freq(pattribute2, ids) : " + Arrays.toString(netClient.id2Freq(pattribute2, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.id2Freq(pattribute3, ids) : " + Arrays.toString(client.id2Freq(pattribute3, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.id2Freq(pattribute3, ids) : " + Arrays.toString(netClient.id2Freq(pattribute3, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.id2Str(pattribute1, ids) : " + Arrays.toString(client.id2Str(pattribute1, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.id2Str(pattribute1, ids) : " + Arrays.toString(netClient.id2Str(pattribute1, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.id2Str(pattribute2, ids) : " + Arrays.toString(client.id2Str(pattribute2, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.id2Str(pattribute2, ids) : " + Arrays.toString(netClient.id2Str(pattribute2, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.id2Str(pattribute3, ids) : " + Arrays.toString(client.id2Str(pattribute3, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.id2Str(pattribute3, ids) : " + Arrays.toString(netClient.id2Str(pattribute3, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.idList2Cpos(pattribute1, ids) : " + Arrays.toString(client.idList2Cpos(pattribute1, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.idList2Cpos(pattribute1, ids) : " + Arrays.toString(netClient.idList2Cpos(pattribute1, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.idList2Cpos(pattribute2, ids) : " + Arrays.toString(client.idList2Cpos(pattribute2, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.idList2Cpos(pattribute2, ids) : " + Arrays.toString(netClient.idList2Cpos(pattribute2, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.idList2Cpos(pattribute3, ids) : " + Arrays.toString(client.idList2Cpos(pattribute3, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.idList2Cpos(pattribute3, ids) : " + Arrays.toString(netClient.idList2Cpos(pattribute3, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.lexiconSize(pattribute1) : " + client.lexiconSize(pattribute1)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.lexiconSize(pattribute1) : " + netClient.lexiconSize(pattribute1)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.lexiconSize(pattribute2) : " + client.lexiconSize(pattribute2)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.lexiconSize(pattribute2) : " + netClient.lexiconSize(pattribute2)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.lexiconSize(pattribute3) : " + client.lexiconSize(pattribute3)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.lexiconSize(pattribute3) : " + netClient.lexiconSize(pattribute3)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.listCorpora() : " + Arrays.toString(client.listCorpora())); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.listCorpora() : " + Arrays.toString(netClient.listCorpora())); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.listSubcorpora(corpus) : " + Arrays.toString(client.listSubcorpora(corpus))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.listSubcorpora(corpus) : " + Arrays.toString(netClient.listSubcorpora(corpus))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.regex2Id(pattribute1, regex) : " + Arrays.toString(client.regex2Id(pattribute1, regex))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.regex2Id(pattribute1, regex) : " + Arrays.toString(netClient.regex2Id(pattribute1, regex))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.regex2Id(pattribute2, regex) : " + Arrays.toString(client.regex2Id(pattribute2, regex))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.regex2Id(pattribute2, regex) : " + Arrays.toString(netClient.regex2Id(pattribute2, regex))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.regex2Id(pattribute3, regex) : " + Arrays.toString(client.regex2Id(pattribute3, regex))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.regex2Id(pattribute3, regex) : " + Arrays.toString(netClient.regex2Id(pattribute3, regex))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.str2Id(pattribute1, strings) : " + Arrays.toString(client.str2Id(pattribute1, strings))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.str2Id(pattribute1, strings) : " + Arrays.toString(netClient.str2Id(pattribute1, strings))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.str2Id(pattribute2, strings) : " + Arrays.toString(client.str2Id(pattribute2, strings))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.str2Id(pattribute2, strings) : " + Arrays.toString(netClient.str2Id(pattribute2, strings))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.str2Id(pattribute3, strings) : " + Arrays.toString(client.str2Id(pattribute3, strings))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.str2Id(pattribute3, strings) : " + Arrays.toString(netClient.str2Id(pattribute3, strings))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.struc2Cpos(sattribute1, 0) : " + Arrays.toString(client.struc2Cpos(sattribute1, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.struc2Cpos(sattribute1, 0) : " + Arrays.toString(netClient.struc2Cpos(sattribute1, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.struc2Cpos(sattribute2, 0) : " + Arrays.toString(client.struc2Cpos(sattribute2, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.struc2Cpos(sattribute2, 0) : " + Arrays.toString(netClient.struc2Cpos(sattribute2, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.struc2Cpos(sattribute3, 0) : " + Arrays.toString(client.struc2Cpos(sattribute3, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.struc2Cpos(sattribute3, 0) : " + Arrays.toString(netClient.struc2Cpos(sattribute3, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.struc2Cpos(sattribute1, 1) : " + Arrays.toString(client.struc2Cpos(sattribute1, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.struc2Cpos(sattribute1, 1) : " + Arrays.toString(netClient.struc2Cpos(sattribute1, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.struc2Cpos(sattribute2, 1) : " + Arrays.toString(client.struc2Cpos(sattribute2, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.struc2Cpos(sattribute2, 1) : " + Arrays.toString(netClient.struc2Cpos(sattribute2, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.struc2Cpos(sattribute3, 1) : " + Arrays.toString(client.struc2Cpos(sattribute3, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.struc2Cpos(sattribute3, 1) : " + Arrays.toString(netClient.struc2Cpos(sattribute3, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.struc2Cpos(sattribute1, 2) : " + Arrays.toString(client.struc2Cpos(sattribute1, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.struc2Cpos(sattribute1, 2) : " + Arrays.toString(netClient.struc2Cpos(sattribute1, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.struc2Cpos(sattribute2, 2) : " + Arrays.toString(client.struc2Cpos(sattribute2, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.struc2Cpos(sattribute2, 2) : " + Arrays.toString(netClient.struc2Cpos(sattribute2, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.struc2Cpos(sattribute3, 2) : " + Arrays.toString(client.struc2Cpos(sattribute3, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.struc2Cpos(sattribute3, 2) : " + Arrays.toString(netClient.struc2Cpos(sattribute3, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.struc2Cpos(sattribute1, 3) : " + Arrays.toString(client.struc2Cpos(sattribute1, 3))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.struc2Cpos(sattribute1, 3) : " + Arrays.toString(netClient.struc2Cpos(sattribute1, 3))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.struc2Cpos(sattribute2, 3) : " + Arrays.toString(client.struc2Cpos(sattribute2, 3))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.struc2Cpos(sattribute2, 3) : " + Arrays.toString(netClient.struc2Cpos(sattribute2, 3))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.struc2Cpos(sattribute3, 3) : " + Arrays.toString(client.struc2Cpos(sattribute3, 3))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.struc2Cpos(sattribute3, 3) : " + Arrays.toString(netClient.struc2Cpos(sattribute3, 3))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.struc2Str(sattribute1, strucpos) : " + Arrays.toString(client.struc2Str(sattribute1, strucpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.struc2Str(sattribute1, strucpos) : " + Arrays.toString(netClient.struc2Str(sattribute1, strucpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.struc2Str(sattribute2, strucpos) : " + Arrays.toString(client.struc2Str(sattribute2, strucpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.struc2Str(sattribute2, strucpos) : " + Arrays.toString(netClient.struc2Str(sattribute2, strucpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.struc2Str(sattribute3, strucpos) : " + Arrays.toString(client.struc2Str(sattribute3, strucpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.struc2Str(sattribute3, strucpos) : " + Arrays.toString(netClient.struc2Str(sattribute3, strucpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.subCorpusHasField(subcorpus, (byte)0) : " + client.subCorpusHasField(subcorpusid, (byte) 0)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.subCorpusHasField(subcorpus, (byte)0) : " + netClient.subCorpusHasField(subcorpusid, (byte) 0)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.subCorpusSize(subcorpus) : " + client.subCorpusSize(subcorpusid)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.subCorpusSize(subcorpus) : " + netClient.subCorpusSize(subcorpusid)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println(e);
		}

		try {
			System.out.println("client.dropSubCorpus(subcorpus)"); //$NON-NLS-1$
			client.dropSubCorpus(subcorpusid);
		}
		catch (Exception e) {
			System.out.println(e);
		}
		try {
			System.out.println("netClient.dropSubCorpus(subcorpus)"); //$NON-NLS-1$
			netClient.dropSubCorpus(subcorpusid);
		}
		catch (Exception e) {
			System.out.println(e);
		}

		System.out.println("TIME: " + ExecTimer.stop()); //$NON-NLS-1$
		System.out.println("STOP MEM CLIENT " + client.disconnect()); //$NON-NLS-1$
		System.out.println("STOP MEM SERVER " + server.stop()); //$NON-NLS-1$
		System.out.println("STOP NET CLIENT " + netClient.disconnect()); //$NON-NLS-1$
		System.out.println("STOP NET SERVER " + netServer.stop()); //$NON-NLS-1$
	}
}
