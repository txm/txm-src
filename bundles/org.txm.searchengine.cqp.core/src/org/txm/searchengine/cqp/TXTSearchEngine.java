package org.txm.searchengine.cqp;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.txm.Toolbox;
import org.txm.core.engines.EngineType;
import org.txm.core.results.TXMResult;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Match;
import org.txm.searchengine.core.EmptySelection;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.SearchEngine;
import org.txm.searchengine.core.SearchEngineProperty;
import org.txm.searchengine.core.Selection;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.query.TXTQuery;

public class TXTSearchEngine extends SearchEngine {

	public static final String NAME = "TXT"; //$NON-NLS-1$

	/**
	 * initialization state
	 */
	boolean state = false;

	boolean useNetCQi = false;

	@Override
	public String getName() {
		return TXTSearchEngine.NAME;
	}

	/**
	 * Starts the corpus engine if not remove then launch cqpserver.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		return true;
	}


	@Override
	public boolean initialize() {
		return true;
	}

	/**
	 * Shutdown.
	 */
	@Override
	public boolean stop() {
		return true;
	}

	@Override
	public boolean isRunning() {
		return CQPSearchEngine.getEngine().isRunning();
	}

	@Override
	public Selection query(CorpusBuild corpus, IQuery query, String name, boolean saveQuery) throws CqiClientException {
		if (query instanceof TXTQuery && corpus instanceof CQPCorpus) {
			TXTQuery tquery = (TXTQuery) query;
			return CQPSearchEngine.getEngine().query(corpus, tquery.fixQuery(corpus.getLang()), name, saveQuery);
		}
		else {
			return new EmptySelection(query);
		}
	}

	@Override
	public TXTQuery newQuery() {
		return new TXTQuery(""); //$NON-NLS-1$
	}

	@Override
	public boolean hasIndexes(CorpusBuild corpus) {
		return CQPSearchEngine.getEngine().hasIndexes(corpus);
	}

	@Override
	public void notify(TXMResult r, String state) {
		// nothing to do the CQPEngine will manage the notifications
	}

	public static TXTSearchEngine getEngine() {
		return (TXTSearchEngine) Toolbox.getEngineManager(EngineType.SEARCH).getEngine(TXTSearchEngine.NAME);
	}

	@Override
	public String getValueForProperty(Match match, SearchEngineProperty property) {
		return CQPSearchEngine.getEngine().getValueForProperty(match, property);
	}

	@Override
	public List<String> getValuesForProperty(Match match, SearchEngineProperty property) {
		return CQPSearchEngine.getEngine().getValuesForProperty(match, property);
	}
}
