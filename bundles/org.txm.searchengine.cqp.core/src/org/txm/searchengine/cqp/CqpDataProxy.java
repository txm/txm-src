// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.utils.SparseList;

// TODO: Auto-generated Javadoc
/**
 * The Class CqpDataProxy.
 */
public class CqpDataProxy {

	/**
	 * Data is cached in a sparse list which give for each position the
	 * corresponding string (given the property it is dedicated to).
	 */
	private SparseList<String> data;

	/** The property getQualifiedName. */
	private String property;

	private boolean isWordProperty = true;

	private NetCqiClient netCqiClient;

	/**
	 * Instantiates a new cqp data proxy.
	 *
	 * @param property the property getQualifiedName
	 */
	public CqpDataProxy(NetCqiClient netCqiClient, String property) {
		this(netCqiClient, property, true);
	}

	public CqpDataProxy(NetCqiClient netCqiClient, Property property) {
		this(netCqiClient, property.getQualifiedName(), !(property instanceof StructuralUnitProperty));
	}

	/**
	 * Instantiates a new cqp data proxy.
	 *
	 * @param property the property getQualifiedName
	 * @param isWordProperty true is the default value
	 */
	public CqpDataProxy(NetCqiClient netCqiClient, String property, boolean isWordProperty) {
		this.data = new SparseList<String>();
		this.property = property;
		this.isWordProperty = isWordProperty;
		this.netCqiClient = netCqiClient;
	}

	/**
	 * Return lists of strings. The positions parameter gives a list of starting
	 * points, and the nWords parameter gives a list of sizes: The i^th string
	 * starts at the position positions.get(i) and is nWords.get(i)
	 *
	 * @param positions the positions
	 * @param nWords the n words
	 * @return the data
	 * @throws CqiClientException the cqi client exception
	 */
	public List<List<String>> getData(List<Integer> positions,
			List<Integer> nWords) throws CqiClientException {
		List<List<String>> lines = new ArrayList<List<String>>();
		for (int i = 0; i < positions.size(); i++)
			lines.add(data.getRange(positions.get(i), positions.get(i)
					+ nWords.get(i)));
		List<Integer> allPositionsNeeded = new ArrayList<Integer>();
		// the position in the corpus of the missing values

		for (int i = 0; i < lines.size(); i++) {
			List<String> line = lines.get(i);
			for (int j = 0; j < line.size(); j++) {
				if (line.get(j) == null)
					allPositionsNeeded.add(positions.get(i) + j);
			}
		}
		if (allPositionsNeeded.size() > 0) {
			String[] missingValues;
			try {
				if (!isWordProperty) {
					//System.out.println("DataProxy: "+allPositionsNeeded);
					int[] strucs = netCqiClient.cpos2Struc(
							property,
							ArrayUtils
									.toPrimitive(allPositionsNeeded
											.toArray(new Integer[0])));
					//System.out.println("structs: "+Arrays.toString(strucs));
					missingValues = netCqiClient._struc2Str(
							property, strucs);
					//System.out.println("missingValues: "+missingValues);
				}
				else {
					missingValues = netCqiClient._cpos2Str(
							property,
							ArrayUtils
									.toPrimitive(allPositionsNeeded
											.toArray(new Integer[0])));
				}
			}
			catch (Exception e) {
				throw new CqiClientException(e);
			}
			for (int i = 0; i < missingValues.length; i++)
				data.add(allPositionsNeeded.get(i), missingValues[i]);
			// There is no more missing values in data, we can query it again
			// This is is pretty ugly since all the non missing values have
			// already been retrieved...
			lines = new ArrayList<List<String>>();
			for (int i = 0; i < positions.size(); i++)
				lines.add(data.getRange(positions.get(i), positions.get(i)
						+ nWords.get(i)));
		}
		return lines;
	}

	public List<String> getData(List<Integer> positions) throws CqiClientException {
		int[] ipositions = new int[positions.size()];
		for (int i = 0; i < positions.size(); i++)
			ipositions[i] = positions.get(i);
		return getData(ipositions);
	}

	/**
	 * Gets the data.
	 *
	 * @param positions the positions
	 * @return the data
	 * @throws CqiClientException the cqi client exception
	 */
	public List<String> getData(int[] positions) throws CqiClientException {
		//System.out.println("CqpDataProxy: start "+property);
		//System.out.println("positions: "+Arrays.toString(positions));
		List<String> lines = new ArrayList<String>();
		for (int i = 0; i < positions.length; i++)
			lines.add(data.getRange(positions[i], positions[i] + 1).get(0));
		//System.out.println("lines: "+lines);
		List<Integer> missingValuePositions = new ArrayList<Integer>();// the position in the corpus of the missing values
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			if (line == null)
				missingValuePositions.add(positions[i]);
		}
		//System.out.println("missingValuePositions: "+missingValuePositions);
		if (missingValuePositions.size() > 0) {
			String[] missingValues;
			try {
				if (!isWordProperty) {
					int[] strucs = netCqiClient.cpos2Struc(
							property,
							ArrayUtils.toPrimitive(missingValuePositions
									.toArray(new Integer[0])));
					//System.out.println("structs: "+Arrays.toString(strucs));
					missingValues = netCqiClient._struc2Str(property, strucs);
				}
				else {
					missingValues = netCqiClient._cpos2Str(
							property,
							ArrayUtils.toPrimitive(missingValuePositions
									.toArray(new Integer[0])));
					//System.out.println("missingValues: "+missingValues);
				}
			}
			catch (Exception e) {
				throw new CqiClientException(e);
			}
			for (int i = 0; i < missingValues.length; i++)
				data.add(missingValuePositions.get(i), missingValues[i]);
			// There is no more missing values in data, we can query it again
			// This is is pretty ugly since all the non missing values have
			// already been retrieved...
			lines = new ArrayList<String>();
			for (int i = 0; i < positions.length; i++)
				lines.add(data.getRange(positions[i], positions[i] + 1).get(0));
		}
		return lines;
	}
}
