// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-05-13 09:46:56 +0200 (Wed, 13 May 2015) $
// $LastChangedRevision: 2967 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp;

import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Arrays;
import java.util.HashMap;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.ServerNotFoundException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.core.messages.CQPSearchEngineCoreMessages;
import org.txm.searchengine.cqp.serverException.CqiClErrorCorpusAccess;
import org.txm.searchengine.cqp.serverException.CqiClErrorInternal;
import org.txm.searchengine.cqp.serverException.CqiClErrorNoSuchAttribute;
import org.txm.searchengine.cqp.serverException.CqiClErrorOutOfMemory;
import org.txm.searchengine.cqp.serverException.CqiClErrorOutOfRange;
import org.txm.searchengine.cqp.serverException.CqiClErrorRegex;
import org.txm.searchengine.cqp.serverException.CqiClErrorWrongAttributeType;
import org.txm.searchengine.cqp.serverException.CqiCqpErrorErrorGeneral;
import org.txm.searchengine.cqp.serverException.CqiCqpErrorInvalidField;
import org.txm.searchengine.cqp.serverException.CqiCqpErrorNoSuchCorpus;
import org.txm.searchengine.cqp.serverException.CqiCqpErrorOutOfRange;
import org.txm.searchengine.cqp.serverException.CqiCqpErrorSyntax;
import org.txm.searchengine.cqp.serverException.CqiErrorConnectRefused;
import org.txm.searchengine.cqp.serverException.CqiErrorGeneralError;
import org.txm.searchengine.cqp.serverException.CqiErrorSyntaxError;
import org.txm.searchengine.cqp.serverException.CqiErrorUserAbort;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.ExecTimer;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * This class implements a Java CQi client.
 *
 * @author Jean-Philippe Magué
 */
public class NetCqiClient extends AbstractCqiClient {

	/** The socket. */
	private Socket socket;

	/** The server address. */
	private SocketAddress serverAddress;

	/** The stream to write data on the socket. */
	private DataOutput streamToServer;

	/** The stream to read data from the socket. */
	private DataInput streamFromServer;

	/** The host. */
	private String host;

	/** The port. */
	private int port;

	private String username;

	private String password;

	//key 1 : property name
	private HashMap<String, CqpDataProxy> corporacache = new HashMap<String, CqpDataProxy>();

	/**
	 * Write a string on the socket.
	 * 
	 * @param string the string
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private synchronized void writeString(String string) throws IOException {
		byte[] bytes = string.getBytes("UTF-8"); //$NON-NLS-1$
		this.streamToServer.writeShort(bytes.length);
		this.streamToServer.write(bytes);

		// short length = (short) string.length();

		// long start = System.currentTimeMillis();
		// this.streamToServer.writeShort(length);
		// this.streamToServer.write(string.getBytes("UTF-8"));
		// long end = System.currentTimeMillis();
		// System.out.println("String \"" + string + "\" writen at " +
		// (string.getBytes("ISO-8859-1").length/(1+end-start)) +"kb/s");

	}

	/**
	 * Write a string array on the socket.
	 * 
	 * @param strings the string array
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private synchronized void writeStringArray(String[] strings) throws IOException {
		int length = strings.length;
		this.streamToServer.writeInt(length);
		for (int i = 0; i < length; i++)
			writeString(strings[i]);
	}

	/**
	 * Write int array on the socket.
	 * 
	 * @param ints
	 *            the int array
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private synchronized void writeIntArray(int[] ints) throws IOException {
		int length = ints.length;
		this.streamToServer.writeInt(length);
		for (int i = 0; i < length; i++) {
			// System.out.println(i+"/"+length);
			this.streamToServer.writeInt(ints[i]);
		}
	}

	/**
	 * Read a string from the socket.
	 * 
	 * @return the string
	 * 
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             Signals that the cqi server raised an error
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private synchronized String readString() throws UnexpectedAnswerException, CqiServerError, IOException {

		Log.finest(CQPSearchEngineCoreMessages.readingStringFromTheCQiServer);

		if (readHeaderFromServer() != CQI_DATA_STRING) {
			throw new UnexpectedAnswerException();
		}
		short length = this.streamFromServer.readShort();
		byte[] bytes = new byte[length];
		this.streamFromServer.readFully(bytes);
		String res = new String(bytes, "UTF-8"); //$NON-NLS-1$

		Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.readStringFromTheCQiServerColonP0, res));

		return res;
	}

	/**
	 * Read a boolean from the socket.
	 * 
	 * @return the boolean
	 * 
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             Signals that the cqi server raised an error
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private synchronized boolean readBoolean() throws UnexpectedAnswerException, CqiServerError, IOException {
		byte b = 0;

		Log.finest(CQPSearchEngineCoreMessages.readingBooleanFromTheCQiServer);

		if (readHeaderFromServer() != CQI_DATA_BOOL) {
			throw new UnexpectedAnswerException();
		}
		b = this.streamFromServer.readByte();
		boolean res = (b == 1);

		Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.readBooleanFromTheCQiServerColonP0, res));

		return res;
	}

	/**
	 * Read an int from the socket.
	 * 
	 * @return the int
	 * 
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             Signals that the cqi server raised an error
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private synchronized int readInt() throws UnexpectedAnswerException, CqiServerError, IOException {

		Log.finest(CQPSearchEngineCoreMessages.readingIntegerFromTheCQiServer);

		if (readHeaderFromServer() != CQI_DATA_INT) {
			throw new UnexpectedAnswerException();
		}
		int res = this.streamFromServer.readInt();

		Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.readIntegerFromTheCQiServerColonP0, res));

		return res;
	}

	/**
	 * Read a string array from the socket.
	 * 
	 * @return the string array
	 * 
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             Signals that the cqi server raised an error
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private synchronized String[] readStringArray() throws UnexpectedAnswerException, CqiServerError, IOException {

		Log.finest(CQPSearchEngineCoreMessages.readingStringArrayFromTheCQiServer);

		byte[] header = readHeaderFromServer();
		if (header != CQI_DATA_STRING_LIST) {
			throw new UnexpectedAnswerException();
		}
		// long start = System.currentTimeMillis();
		int arrayLength = this.streamFromServer.readInt();
		int nByteRead = 4;
		String[] strings = new String[arrayLength];
		/*
		 * System.out.println("Available: "+this.socket.getInputStream().available
		 * ()); byte[] allb = readInByte(this.socket.getInputStream());
		 * System.out.println("count = "+allb.length);
		 * System.out.println("array length = "+arrayLength);
		 * int readindex = 0; for(int i=0; i<arrayLength; i++){ short
		 * stringLength = bytesToShort(allb[readindex],allb[readindex+1]);
		 * readindex += 2;
		 * byte[] strbytes = new byte[stringLength]; System.arraycopy(allb,
		 * readindex, strbytes, 0, stringLength); strings[i] = new
		 * String(strbytes,"UTF-8");
		 * //System.out.println(""+i+" Str len "+stringLength
		 * +" >> "+strings[i]); readindex += stringLength; }
		 */
		for (int i = 0; i < arrayLength; i++) {
			short stringLength = this.streamFromServer.readShort();
			nByteRead += 2;
			byte[] bytes = new byte[stringLength];
			// long start2 = System.currentTimeMillis();
			this.streamFromServer.readFully(bytes);
			// long end2 = System.currentTimeMillis();
			// System.out.println("Reading at " +
			// (stringLength/(1+end2-start2))+ "kb/s");
			nByteRead += stringLength;
			strings[i] = new String(bytes, "UTF-8");//, "ISO-8859-1"); //$NON-NLS-1$
		}
		// System.out.println("read : "+nByteRead);
		// long end = System.currentTimeMillis();
		// System.out.println(nByteRead + " bytes read in " +(end-start)+ "ms ("
		// + (nByteRead/(1+end-start)) + "kb/s)");

		Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.readStringArrayFromTheCQiServerColonP0, Arrays.asList(strings)));

		return strings;
	}

	/**
	 * Read in byte.
	 *
	 * @param in the in
	 * @return the byte[]
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private synchronized byte[] readInByte(InputStream in) throws IOException {
		int count;
		byte[] buffer = new byte[8192];
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		while ((count = in.read(buffer)) > 0) {
			out.write(buffer, 0, count);
		}
		out.close();
		return out.toByteArray();
	}

	/**
	 * Byte array to string.
	 *
	 * @param b the b
	 * @return the string
	 */
	private synchronized String byteArrayToString(byte[] b) {
		StringBuffer res = new StringBuffer("["); //$NON-NLS-1$
		if (b.length > 0) {
			res.append(b[0]);
			for (int i = 1; i < b.length; i++)
				res.append("; " + b[i]); //$NON-NLS-1$
		}
		res.append("]"); //$NON-NLS-1$
		return res.toString();
	}

	/**
	 * Read an int array from the socket.
	 * 
	 * @return the int array
	 * 
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             Signals that the cqi server raised an error
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private synchronized int[] readIntList() throws UnexpectedAnswerException,
			CqiServerError, IOException {

		Log.finest(CQPSearchEngineCoreMessages.readingIntegerArrayFromTheCQiServer);

		if ((readHeaderFromServer()) != CQI_DATA_INT_LIST) {
			throw new UnexpectedAnswerException();
		}
		int arrayLength = this.streamFromServer.readInt();
		int bsize = arrayLength * 4;

		int[] ints = new int[arrayLength];
		byte[] b = new byte[bsize];// int size

		// System.out.println("bsize : "+bsize);
		streamFromServer.readFully(b);
		// System.out.println("read "+ ((DataInputStream)
		// streamFromServer).readFully(b));
		int nbline = 0;
		for (int i = 0; i + 3 < bsize; i += 4) {
			ints[nbline++] = bytesToInt(b[i], b[i + 1], b[i + 2], b[i + 3]);
		}
		/*
		 * for(int i=0; i<arrayLength; i++){ ints[i] =
		 * this.streamFromServer.readInt(); }
		 */

		Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.readIntegerArrayFromTheCQiServerColonP0, Arrays.asList(ints)));

		return ints;
	}

	/**
	 * Bytes to int.
	 *
	 * @param bytes the bytes
	 * @return the int
	 */
	protected synchronized static int bytesToInt(byte[] bytes) {
		int value = 0;

		for (int i = 0; i < bytes.length; i++) {
			value = value << 8;
			value += bytes[i] & 0xff;
		}

		return value;
	}

	/**
	 * Bytes to int.
	 *
	 * @param a the a
	 * @param b the b
	 * @param c the c
	 * @param d the d
	 * @return the int
	 */
	protected synchronized static int bytesToInt(byte a, byte b, byte c, byte d) {
		return (((a & 0xff) << 24) | ((b & 0xff) << 16) | ((c & 0xff) << 8) | (d & 0xff));
	}

	/**
	 * Bytes to short.
	 *
	 * @param hi the hi
	 * @param lo the lo
	 * @return the short
	 */
	protected synchronized static short bytesToShort(byte hi, byte lo) {
		return (short) (((hi & 0xFF) << 8) | (lo & 0xFF));
	}

	/**
	 * Read an int array from the socket.
	 * 
	 * @return the int array
	 * 
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             Signals that the cqi server raised an error
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private synchronized int[][] readIntTable() throws UnexpectedAnswerException, CqiServerError, IOException {

		Log.finest(CQPSearchEngineCoreMessages.readingAnIntegerTableFromTheCQiServer);

		if ((readHeaderFromServer()) != CQI_DATA_INT_TABLE) {
			throw new UnexpectedAnswerException();
		}
		int nRow = this.streamFromServer.readInt();
		int nCol = this.streamFromServer.readInt();
		int[][] ints = new int[nRow][nCol];
		for (int i = 0; i < nRow; i++)
			for (int j = 0; j < nCol; j++)
				ints[i][j] = this.streamFromServer.readInt();

		Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.readString2DIntegerFromTheCQiServerColonP0, Arrays.asList(ints)));

		return ints;
	}

	/**
	 * Read two successive ints from the socket.
	 *
	 * @return an array of size 2 containing the int
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	private synchronized int[] readIntInt() throws IOException, UnexpectedAnswerException, CqiServerError {

		Log.finest(CQPSearchEngineCoreMessages.readingIntegerArrayFromTheCQiServer);

		if (readHeaderFromServer() != CQI_DATA_INT_INT) {
			throw new UnexpectedAnswerException();
		}
		int[] res = new int[2];
		res[0] = this.streamFromServer.readInt();
		res[1] = this.streamFromServer.readInt();

		Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.readIntegerArrayFromTheCQiServerColonP0, Arrays.asList(res)));

		return res;
	}

	/**
	 * Read four successive ints from the socket.
	 *
	 * @return an array of size 4 containing the int
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	private synchronized int[] readIntIntIntInt() throws IOException, UnexpectedAnswerException, CqiServerError {

		Log.finest(CQPSearchEngineCoreMessages.readingIntegerArrayFromTheCQiServer);

		if (readHeaderFromServer() != CQI_DATA_INT_INT_INT_INT) {
			throw new UnexpectedAnswerException();
		}
		int[] res = new int[4];
		res[0] = this.streamFromServer.readInt();
		res[1] = this.streamFromServer.readInt();
		res[2] = this.streamFromServer.readInt();
		res[3] = this.streamFromServer.readInt();

		Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.readIntegerArrayFromTheCQiServerColonP0, Arrays.asList(res)));
		return res;
	}

	/**
	 * Read the header of the data send by the server.
	 * 
	 * @return the header
	 * 
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             Signals that the cqi server raised an error
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private synchronized byte[] readHeaderFromServer() throws UnexpectedAnswerException, CqiServerError, IOException {

		Log.finest(CQPSearchEngineCoreMessages.readingHeaderFromTheCQiServer);

		byte b = this.streamFromServer.readByte();

		Log.finest(CQPSearchEngineCoreMessages.bind(CQPSearchEngineCoreMessages.readByteFromTheCQiServerColonP0, b));

		switch (b) {
			case 0x00:// cf cqi.h:29
				return CQI_PADDING;
			case 0x01:// cf cqi.h:37
				b = this.streamFromServer.readByte();

				Log.finest(CQPSearchEngineCoreMessages.bind(CQPSearchEngineCoreMessages.readByteFromTheCQiServerColonP0, b));

				switch (b) {
					case 0x01:// cf cqi.h:39
						return CQI_STATUS_OK;
					case 0x02:// cf cqi.h:40
						return CQI_STATUS_CONNECT_OK;
					case 0x03:// cf cqi.h:41
						return CQI_STATUS_BYE_OK;
					case 0x04:// cf cqi.h:42
						return CQI_STATUS_PING_OK;
				}
			case 0x02:// cf cqi.h:45
				b = this.streamFromServer.readByte();

				Log.finest(CQPSearchEngineCoreMessages.bind(CQPSearchEngineCoreMessages.readByteFromTheCQiServerColonP0, b));

				lastError = CQPSearchEngineCoreMessages.internalCQIErrorColon + b;
				switch (b) {
					case 0x01:// cf cqi.h:39
						throw new CqiErrorGeneralError();
					case 0x02:// cf cqi.h:40
						throw new CqiErrorConnectRefused();
					case 0x03:// cf cqi.h:41
						throw new CqiErrorUserAbort();
					case 0x04:// cf cqi.h:42
						throw new CqiErrorSyntaxError();
				}
			case 0x03:// cf cqi.h:53
				b = this.streamFromServer.readByte();

				Log.finest(CQPSearchEngineCoreMessages.bind(CQPSearchEngineCoreMessages.readByteFromTheCQiServerColonP0, b));

				switch (b) {
					case 0x01:// cf cqi.h:39
						return CQI_DATA_BYTE;
					case 0x02:// cf cqi.h:40
						return CQI_DATA_BOOL;
					case 0x03:// cf cqi.h:41
						return CQI_DATA_INT;
					case 0x04:// cf cqi.h:42
						return CQI_DATA_STRING;
					case 0x05:// cf cqi.h:42
						return CQI_DATA_BYTE_LIST;
					case 0x06:// cf cqi.h:42
						return CQI_DATA_BOOL_LIST;
					case 0x07:// cf cqi.h:42
						return CQI_DATA_INT_LIST;
					case 0x08:// cf cqi.h:42
						return CQI_DATA_STRING_LIST;
					case 0x09:// cf cqi.h:42
						return CQI_DATA_INT_INT;
					case 0x0A:// cf cqi.h:42
						return CQI_DATA_INT_INT_INT_INT;
					case 0x0B:// cf cqi.h:42
						return CQI_DATA_INT_TABLE;
				}
			case 0x04:// cf cqi.h:67

				b = this.streamFromServer.readByte();
				lastError = CQPSearchEngineCoreMessages.cLErrorP0 + b;

				Log.finest(CQPSearchEngineCoreMessages.bind(CQPSearchEngineCoreMessages.readByteFromTheCQiServerColonP0, b));

				switch (b) {
					case 0x01:// cf cqi.h:39
						throw new CqiClErrorNoSuchAttribute();
					case 0x02:// cf cqi.h:40
						throw new CqiClErrorWrongAttributeType();
					case 0x03:// cf cqi.h:41
						throw new CqiClErrorOutOfRange();
					case 0x04:// cf cqi.h:42
						lastError = getLastCqiError();
						throw new CqiClErrorRegex();
					case 0x05:// cf cqi.h:42
						throw new CqiClErrorCorpusAccess();
					case 0x06:// cf cqi.h:42
						throw new CqiClErrorOutOfMemory();
					case 0x07:// cf cqi.h:42
						throw new CqiClErrorInternal();
				}
			case 0x05:// cf cqi.h:94

				b = this.streamFromServer.readByte();
				lastError = CQPSearchEngineCoreMessages.bind(CQPSearchEngineCoreMessages.cQPErrorColonP0, b);

				Log.finest(CQPSearchEngineCoreMessages.bind(CQPSearchEngineCoreMessages.readByteFromTheCQiServerColonP0, b));

				switch (b) {
					case 0x01:// cf cqi.h:39
						throw new CqiCqpErrorErrorGeneral();
					case 0x02:// cf cqi.h:40
						throw new CqiCqpErrorNoSuchCorpus();
					case 0x03:// cf cqi.h:41
						throw new CqiCqpErrorInvalidField();
					case 0x04:// cf cqi.h:42
						throw new CqiCqpErrorOutOfRange();
					case 0x05:// cf cqi.h:44
						lastError = getLastCQPError();
						throw new CqiCqpErrorSyntax();
				}
		}
		return null;
	}

	/**
	 * Ask the server to execute a function with a String->String signature.
	 *
	 * @param string the argument
	 * @param function the function to be executed
	 * @return the result
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	private synchronized String genericStringToString(String string, byte[] function) throws UnexpectedAnswerException, IOException,
			CqiServerError {
		this.streamToServer.write(function);
		this.writeString(string);
		return readString();
	}

	/**
	 * Ask the server to execute a function with a String->StringArray signature.
	 *
	 * @param string the argument
	 * @param function the function to be executed
	 * @return the result
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	private synchronized String[] genericStringToStringArray(String string, byte[] function) throws UnexpectedAnswerException, IOException, CqiServerError {
		this.streamToServer.write(function);
		this.writeString(string);
		return readStringArray();
	}

	/**
	 * Ask the server to execute a function with a String x int[]->String[]
	 * signature.
	 *
	 * @param string the string argument
	 * @param ints the int[] argument
	 * @param function the function to be executed
	 * @return the result
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	private synchronized String[] genericStringXIntArraytoStringArray(String string, int[] ints, byte[] function) throws IOException, UnexpectedAnswerException, CqiServerError {
		// long start = System.currentTimeMillis();
		this.streamToServer.write(function);
		// System.out.println(System.currentTimeMillis()-start);
		this.writeString(string);
		// System.out.println(System.currentTimeMillis()-start);
		this.writeIntArray(ints);
		// System.out.println(System.currentTimeMillis()-start);
		String[] res = readStringArray();
		// System.out.println(System.currentTimeMillis()-start);
		return res;
	}

	/**
	 * Ask the server to execute a function with a String x int[]->int[]
	 * signature.
	 *
	 * @param string the string argument
	 * @param ints the int[] argument
	 * @param function the function to be executed
	 * @return the result
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private synchronized int[] genericStringXIntArraytoIntArray(String string, int[] ints, byte[] function) throws UnexpectedAnswerException, CqiServerError, IOException {
		this.streamToServer.write(function);
		this.writeString(string);
		this.writeIntArray(ints);
		return readIntList();
	}

	/**
	 * Instantiates a new cqi client.
	 * 
	 * @param host
	 *            the host of the CQI server
	 * @param port
	 *            the port of the CQI server
	 * 
	 * @throws ServerNotFoundException
	 *             the server not found exception
	 */
	public NetCqiClient(String host, int port) throws ServerNotFoundException {
		try {
			Thread.sleep(500);
		}
		catch (InterruptedException ix) {
		}

		int attempts = 10;
		while (attempts > 0) {
			try {
				this.host = host;
				this.port = port;
				this.socket = new Socket();
				this.serverAddress = new InetSocketAddress(host, port);
				this.socket.connect(serverAddress);
				this.streamToServer = new DataOutputStream(this.socket.getOutputStream());
				this.streamFromServer = new DataInputStream(this.socket.getInputStream());
				Log.fine(CQPSearchEngineCoreMessages.searchEngineLaunched);
				return;
			}
			catch (IOException e) {
				System.err.print("."); //$NON-NLS-1$
				try {
					Thread.sleep(2000);
				}
				catch (InterruptedException ix) {
				}
			}

			attempts--;
		}
		throw new ServerNotFoundException(CQPSearchEngineCoreMessages.failedToConnectToSearchEngine);
	}

	// CQI_ERROR_CONNECT_REFUSED
	/**
	 * Connect the client to a server.
	 *
	 * @param username the username
	 * @param password the password
	 * @return true, if successful
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized boolean connect(String username, String password) throws UnexpectedAnswerException, IOException, CqiServerError {

		this.username = username;
		this.password = password;

		Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.connectingToTheSearchEngineWithTheFollowingParametersColonP0ColonP1AtP2P3, username, password, host, port));

		this.streamToServer.write(CQI_CTRL_CONNECT);
		this.writeString(username);
		this.writeString(password);
		try {
			readHeaderFromServer();
			Log.finest(CQPSearchEngineCoreMessages.connectedToTheSearchEngine);
			return true;
		}
		catch (CqiErrorConnectRefused e) {
			return false;
		}
		catch (CqiServerError e) {
			return false;
		}
	}

	// None
	/**
	 * Disconnect.
	 * 
	 * @return true, if successful
	 * 
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public synchronized boolean disconnect() throws UnexpectedAnswerException, CqiServerError, IOException {
		this.streamToServer.write(CQI_CTRL_BYE);
		return (readHeaderFromServer() == CQI_STATUS_BYE_OK);
	}

	String lastError;

	@Override
	public synchronized String getLastError() {
		if (lastError != null && lastError.length() > 0) {
			return lastError;
		}
		return CQPSearchEngineCoreMessages.noError;
	}

	// CQI_CTRL_LAST_GENERAL_ERROR
	/**
	 * return the last CQP error.
	 *
	 * @return the last error
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized String getLastCqiError() throws UnexpectedAnswerException, IOException, CqiServerError {

		Log.severe(CQPSearchEngineCoreMessages.gettingLastCQPError);

		this.streamToServer.write(CQI_CTRL_LAST_GENERAL_ERROR);

		try {
			String ret = readString();
			return getLastCQPError() + "\t" + ret; //$NON-NLS-1$
		}
		catch (CqiServerError e) {
			Log.severe(NLS.bind(CQPSearchEngineCoreMessages.lastCQiErrorP0, e));
			return "error"; //$NON-NLS-1$
		}
	}

	// CQI_CTRL_LAST_CQP_ERROR
	/**
	 * return the last CQP error.
	 *
	 * @return the last error
	 */
	@Override
	public synchronized String getLastCQPError() {
		try {
			this.streamToServer.write(CQI_CTRL_LAST_CQP_ERROR);
			String ret = readString();
			return ret;
		}
		catch (Exception e) {
			Log.severe(NLS.bind(CQPSearchEngineCoreMessages.lastCQPErrorP0, e));
			return "error"; //$NON-NLS-1$
		}
	}

	// None
	/**
	 * Lists the corpora available on the server.
	 *
	 * @return the name of the corpora
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized String[] listCorpora() throws UnexpectedAnswerException, IOException, CqiServerError {
		this.streamToServer.write(CQI_CORPUS_LIST_CORPORA);
		return readStringArray();
	}

	// None
	/**
	 * Gives the corpus charset.
	 *
	 * @param corpus the corpus
	 * @return the name of the charset
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized String corpusCharset(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError {
		return genericStringToString(corpus, CQI_CORPUS_CHARSET);
	}

	// None (not really implemented anyway)
	/**
	 * Gives the corpus properties.
	 *
	 * @param corpus the corpus
	 * @return the properties
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized String[] corpusProperties(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError {
		return genericStringToStringArray(corpus, CQI_CORPUS_PROPERTIES);
	}

	// CQI_CQP_ERROR_NO_SUCH_CORPUS
	/**
	 * Gives the corpus positional attributes.
	 *
	 * @param corpusID the corpus id
	 * @return the name of the attributes
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized String[] corpusPositionalAttributes(String corpusID) throws UnexpectedAnswerException, IOException, CqiServerError {
		return genericStringToStringArray(corpusID, CQI_CORPUS_POSITIONAL_ATTRIBUTES);
	}

	// CQI_CQP_ERROR_NO_SUCH_CORPUS
	/**
	 * Gives the corpus structural attributes.
	 *
	 * @param corpus the corpus
	 * @return the name of the attributes
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized String[] corpusStructuralAttributes(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError {
		return genericStringToStringArray(corpus, CQI_CORPUS_STRUCTURAL_ATTRIBUTES);
	}

	// CQI_CQP_ERROR_NO_SUCH_CORPUS, CQI_CL_ERROR_NO_SUCH_ATTRIBUTE,
	// CQI_CL_ERROR_WRONG_ATTRIBUTE_TYPE
	/**
	 * Check wether a structural attribute has values.
	 *
	 * @param attribute the attribute
	 * @return true, if it has values
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized boolean corpusStructuralAttributeHasValues(String attribute) throws UnexpectedAnswerException, IOException, CqiServerError {
		this.streamToServer.write(CQI_CORPUS_STRUCTURAL_ATTRIBUTE_HAS_VALUES);
		this.writeString(attribute);
		return readBoolean();
	}

	// CQI_CQP_ERROR_NO_SUCH_CORPUS
	/**
	 * Gives the corpus alignement attributes.
	 *
	 * @param corpus the corpus
	 * @return the name of attributes
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized String[] corpusAlignementAttributes(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError {
		return genericStringToStringArray(corpus, CQI_CORPUS_ALIGNMENT_ATTRIBUTES);
	}

	// CQI_CQP_ERROR_NO_SUCH_CORPUS
	/**
	 * Gives the corpus full name.
	 *
	 * @param corpus the corpus
	 * @return the full name
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized String corpusFullName(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError {
		return genericStringToString(corpus, CQI_CORPUS_FULL_NAME);
	}

	/**
	 * Gives the corpus info listed in the .INFO file.
	 *
	 * @param corpus the corpus
	 * @return the info
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized String[] corpusInfo(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError {
		throw new UnsupportedOperationException();
	}

	/**
	 * Drop a corpus.
	 * 
	 * @param corpus the corpus
	 * @throws IOException
	 */
	@Override
	public synchronized void dropCorpus(String corpus) throws IOException {
		this.streamToServer.write(CQI_CORPUS_DROP_CORPUS);
		this.writeString(corpus);
		return;
	}

	// CQI_CQP_ERROR_NO_SUCH_CORPUS, CQI_CL_ERROR_NO_SUCH_ATTRIBUTE,
	// CQI_CL_ERROR_WRONG_ATTRIBUTE_TYPE, CQI_CL_ERROR_CORPUS_ACCESS
	/**
	 * Gives an attribute size (the number of token).
	 *
	 * @param attribute the attribute
	 * @return the size
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized int attributeSize(String attribute) throws IOException, UnexpectedAnswerException, CqiServerError {
		this.streamToServer.write(CQI_CL_ATTRIBUTE_SIZE);
		this.writeString(attribute);
		return readInt();
	}

	// CQI_CQP_ERROR_NO_SUCH_CORPUS, CQI_CL_ERROR_NO_SUCH_ATTRIBUTE,
	// CQI_CL_ERROR_WRONG_ATTRIBUTE_TYPE, CQI_CL_ERROR_CORPUS_ACCESS
	/**
	 * Gives the lexicon size of an attribute.
	 *
	 * @param attribute the attribute
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 * @return the number of entries in the lexicon of a positional attribute
	 */
	@Override
	public synchronized int lexiconSize(String attribute) throws IOException, UnexpectedAnswerException, CqiServerError {
		this.streamToServer.write(CQI_CL_LEXICON_SIZE);
		this.writeString(attribute);
		return readInt();
	}

	/**
	 * Drop attribute.
	 *
	 * @param attribute the attribute
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized void dropAttribute(String attribute) throws IOException, UnexpectedAnswerException, CqiServerError {
		throw new UnsupportedOperationException();
	}

	/**
	 * Converts an array of attribute values to their ID.
	 *
	 * @param attribute the attribute
	 * @param strings the values
	 * @return the IDs
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized int[] str2Id(String attribute, String[] strings) throws IOException, UnexpectedAnswerException, CqiServerError {
		this.streamToServer.write(CQI_CL_STR2ID);
		this.writeString(attribute);
		this.writeStringArray(strings);
		return readIntList();
	}

	/**
	 * Converts an array of attribute ID to their values.
	 *
	 * @param attribute the positional attribute
	 * @param ids the ids
	 * @return the values
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized String[] id2Str(String attribute, int[] ids) throws UnexpectedAnswerException, IOException, CqiServerError {
		return genericStringXIntArraytoStringArray(attribute, ids, CQI_CL_ID2STR);
	}

	/**
	 * Converts an array of attribute IDs to their frequency.
	 *
	 * @param attribute the attribute
	 * @param ids the ids
	 * @return the frequencies
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized int[] id2Freq(String attribute, int[] ids) throws UnexpectedAnswerException, IOException, CqiServerError {
		return genericStringXIntArraytoIntArray(attribute, ids, CQI_CL_ID2FREQ);
	}

	/**
	 * Converts an array of position to their ID given an attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the positions
	 * @return the IDs
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public synchronized int[] cpos2Id(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError {
		return genericStringXIntArraytoIntArray(attribute, cpos, CQI_CL_CPOS2ID);
	}

	/**
	 * Converts an array of position to their value given an attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the cpos
	 * @return the values
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public synchronized String[] cpos2Str(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError {

		if (corporacache.get(attribute) == null) {
			corporacache.put(attribute, new CqpDataProxy(this, attribute, true));
		}

		try {
			return corporacache.get(attribute).getData(cpos).toArray(new String[cpos.length]);
		}
		catch (CqiClientException e) {
			throw new CqiServerError(e.getMessage());
		}
		//		return genericStringXIntArraytoStringArray(attribute, cpos,
		//				CQI_CL_CPOS2STR);
	}

	public synchronized String[] _cpos2Str(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError {
		return genericStringXIntArraytoStringArray(attribute, cpos, CQI_CL_CPOS2STR);
	}

	/**
	 * Computes for each position of an array the Id of the enclosing structural
	 * attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the positions
	 * @return the IDs
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public synchronized int[] cpos2Struc(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError {
		return genericStringXIntArraytoIntArray(attribute, cpos, CQI_CL_CPOS2STRUC);
	}

	/**
	 * Computes for each position of an array the position of the left boundary
	 * of the enclosing structural attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the cpos
	 * @return the positions of the left boundaries
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public synchronized int[] cpos2LBound(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError {
		return genericStringXIntArraytoIntArray(attribute, cpos, CQI_CL_CPOS2LBOUND);
	}

	/**
	 * Computes for each position of an array the position of the right boundary
	 * of the enclosing structural attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the cpos
	 * @return the positions of the right boundaries
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public synchronized int[] cpos2RBound(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError {
		return genericStringXIntArraytoIntArray(attribute, cpos, CQI_CL_CPOS2RBOUND);
	}

	/**
	 * Computes for each position of an array the Id of the enclosing alignment
	 * attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the cpos
	 * @return the int[]
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public synchronized int[] cpos2Alg(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError {
		return genericStringXIntArraytoIntArray(attribute, cpos, CQI_CL_CPOS2ALG);
	}

	/**
	 * Retrieves annotated string values of structure regions in "strucs"; "" if
	 * out of range.
	 *
	 * @param attribute the attribute
	 * @param strucs the strucs
	 * @return the string[]
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public synchronized String[] struc2Str(String attribute, int[] strucs) throws UnexpectedAnswerException, IOException, CqiServerError {
		return _struc2Str(attribute, strucs);
	}

	/**
	 * Retrieves annotated string values of structure regions in "strucs"; "" if
	 * out of range.
	 *
	 * @param attribute the attribute
	 * @param strucs the strucs
	 * @return the string[]
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	public synchronized String[] _struc2Str(String attribute, int[] strucs) throws UnexpectedAnswerException, IOException, CqiServerError {
		return genericStringXIntArraytoStringArray(attribute, strucs, CQI_CL_STRUC2STR);
	}

	/**
	 * Retrieves all corpus positions where the given token occurs.
	 *
	 * @param attribute the attribute
	 * @param id the id
	 * @return the position
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public synchronized int[] id2Cpos(String attribute, int id) throws UnexpectedAnswerException, IOException, CqiServerError {
		this.streamToServer.write(CQI_CL_ID2CPOS);
		this.writeString(attribute);
		this.streamToServer.writeInt(id);
		return readIntList();
	}

	/**
	 * Retrieves all corpus positions where one of the tokens in "id_list"
	 * occurs; the returned list is sorted as a whole, not per token id.
	 *
	 * @param attribute the attribute
	 * @param ids the ids
	 * @return the positions
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public synchronized int[] idList2Cpos(String attribute, int[] ids) throws UnexpectedAnswerException, IOException, CqiServerError {
		return genericStringXIntArraytoIntArray(attribute, ids,
				CQI_CL_IDLIST2CPOS);
	}

	/**
	 * Retrieves the lexicon IDs of all tokens that match "regex"; the returned
	 * list may be empty (size 0);.
	 *
	 * @param attribute the attribute
	 * @param regex the regex
	 * @return the IDs
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public synchronized int[] regex2Id(String attribute, String regex) throws UnexpectedAnswerException, IOException, CqiServerError {
		this.streamToServer.write(CQI_CL_REGEX2ID);
		this.writeString(attribute);
		this.writeString(regex);
		return readIntList();
	}

	/**
	 * Retrieves the start and end corpus positions of structure region "struc".
	 *
	 * @param attribute the attribute
	 * @param struc the struc
	 * @return an array of size 2 containing the start and end positions
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public synchronized int[] struc2Cpos(String attribute, int struc) throws UnexpectedAnswerException, IOException, CqiServerError {
		this.streamToServer.write(CQI_CL_STRUC2CPOS);
		this.writeString(attribute);
		this.streamToServer.writeInt(struc);
		return this.readIntInt();
	}

	/**
	 * Retrieves start and end corpus positions of an alignement region in the source and target corpora "struc".
	 *
	 * @param attribute the attribute
	 * @param struc the struc
	 * @return an array of size 4 containing (src_start, src_end, target_start,
	 *         target_end)
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public synchronized int[] alg2Cpos(String attribute, int struc) throws UnexpectedAnswerException, IOException, CqiServerError {
		this.streamToServer.write(CQI_CL_ALG2CPOS);
		this.writeString(attribute);
		this.streamToServer.writeInt(struc);
		return readIntIntIntInt();
	}

	/**
	 * Runs a CQP query.
	 *
	 * @param motherCorpus the mother corpus
	 * @param subcorpus the subcorpus
	 * @param query the query
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public synchronized void cqpQuery(String motherCorpus, String subcorpus, String query) throws IOException, UnexpectedAnswerException, CqiServerError {
		//System.out.println("CQPQUERY: Mother: "+motherCorpus+", sub:"+subcorpus+", "+query);
		this.streamToServer.write(CQI_CQP_QUERY);
		this.writeString(motherCorpus);
		this.writeString(subcorpus);
		this.writeString(query);
		this.readHeaderFromServer(); // may receive a CQI_CQP_ERROR_GENERAL

	}

	/**
	 * Evaluate and run a raw CQP query.
	 *
	 * @param query the query
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError the cqi server error
	 */
	public synchronized void query(String query) throws IOException, UnexpectedAnswerException, CqiServerError {
		//System.out.println("CQPQUERY: Mother: "+motherCorpus+", sub:"+subcorpus+", "+query);
		this.streamToServer.write(CQI_CQP_QUERY_EVAL);
		this.writeString(query);
		this.readHeaderFromServer(); // may receive a CQI_CQP_ERROR_GENERAL

	}

	/**
	 * Lists all the subcorpora of a corpus.
	 *
	 * @param corpus the corpus
	 * @return the name of the subcorpora
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiCqpErrorNoSuchCorpus the cqi cqp error no such corpus
	 */
	@Override
	public synchronized String[] listSubcorpora(String corpus) throws UnexpectedAnswerException, IOException, CqiCqpErrorNoSuchCorpus {
		try {
			return genericStringToStringArray(corpus, CQI_CQP_LIST_SUBCORPORA);
		}
		catch (CqiCqpErrorNoSuchCorpus e) {
			throw e;
		}
		catch (CqiServerError e) {
			throw new UnexpectedAnswerException(e);
		}
	}

	/**
	 * Gives the size of a subcorpus .
	 * 
	 * @param subcorpus
	 *            the subcorpus
	 * 
	 * @return the size
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	@Override
	public synchronized int subCorpusSize(String subcorpus) throws IOException, UnexpectedAnswerException, CqiServerError {
		this.streamToServer.write(CQI_CQP_SUBCORPUS_SIZE);
		this.writeString(subcorpus);
		return this.readInt();
	}

	/**
	 * Checks wether a subcorpus has a field.
	 * 
	 * @param subcorpus
	 *            the subcorpus
	 * @param field
	 *            the field
	 * 
	 * @return true, if the subcorpus has the field
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	@Override
	public synchronized boolean subCorpusHasField(String subcorpus, byte field) throws IOException, UnexpectedAnswerException, CqiServerError {
		this.streamToServer.write(CQI_CQP_SUBCORPUS_HAS_FIELD);
		this.writeString(subcorpus);
		this.streamToServer.writeByte(field);
		return this.readBoolean();
	}

	/**
	 * Dumps the values of "field"for match ranges "first" .. "last" in
	 * "subcorpus". "field" is one of the CQI_CONST_FIELD_* constants.
	 * 
	 * @param subcorpus
	 *            the subcorpus
	 * @param field
	 *            the field
	 * @param first
	 *            the first
	 * @param last
	 *            the last
	 * 
	 * @return the values
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	@Override
	public synchronized int[] dumpSubCorpus(String subcorpus, byte field, int first, int last) throws IOException, UnexpectedAnswerException, CqiServerError {
		this.streamToServer.write(CQI_CQP_DUMP_SUBCORPUS);
		this.writeString(subcorpus);
		this.streamToServer.writeByte(field);
		this.streamToServer.writeInt(first);
		this.streamToServer.writeInt(last);
		return this.readIntList();
	}

	/**
	 * Drops a subcorpus.
	 * 
	 * @param subcorpus
	 *            the subcorpus
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	@Override
	public synchronized void dropSubCorpus(String subcorpus) throws IOException, UnexpectedAnswerException, CqiServerError {
		this.streamToServer.write(CQI_CQP_DROP_SUBCORPUS);
		this.writeString(subcorpus);
		this.readHeaderFromServer();
	}

	/**
	 * Returns {@code <n>} (id, frequency) pairs flattened into a list of size 2*{@code <n>} NB:
	 * pairs are sorted by frequency desc.
	 * 
	 * @param subcorpus
	 *            the subcorpus
	 * @param cutoff
	 *            the cutoff
	 * @param field
	 *            the field : one of CQI_CONST_FIELD_MATCH,
	 *            CQI_CONST_FIELD_TARGET, CQI_CONST_FIELD_KEYWORD
	 * @param attribute
	 *            the attribute
	 * 
	 * @return the int[][]
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	@Override
	public synchronized int[][] fdist1(String subcorpus, int cutoff, byte field, String attribute) throws IOException, UnexpectedAnswerException, CqiServerError {
		this.streamToServer.write(CQI_CQP_FDIST_1);
		this.writeString(subcorpus);
		this.streamToServer.writeInt(cutoff);
		this.streamToServer.writeByte(field);
		this.writeString(attribute);
		return this.readIntTable();
	}

	/**
	 * Returns {@code <n>} (id1, id2, frequency) pairs flattened into a list of size
	 * 3*{@code <n>} NB: triples are sorted by frequency desc. .
	 * 
	 * @param subcorpus
	 *            the subcorpus
	 * @param cutoff
	 *            the cutoff
	 * @param field1
	 *            the field1
	 * @param attribute1
	 *            the attribute1
	 * @param field2
	 *            the field2
	 * @param attribute2
	 *            the attribute2
	 * 
	 * @return the int[]
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	@Override
	public synchronized int[][] fdist2(String subcorpus, int cutoff, byte field1, String attribute1, byte field2, String attribute2) throws IOException, UnexpectedAnswerException, CqiServerError {
		this.streamToServer.write(CQI_CQP_FDIST_1);
		this.writeString(subcorpus);
		this.streamToServer.writeInt(cutoff);
		this.streamToServer.writeByte(field1);
		this.writeString(attribute1);
		this.streamToServer.writeByte(field2);
		this.writeString(attribute2);
		return this.readIntTable();
	}

	@Override
	public boolean reconnect() {
		try {
			return connect(username, password);
		}
		catch (Exception e) {
			System.out.println(CQPSearchEngineCoreMessages.couldNotReconnectToServerColon + e);
			return false;
		}
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		String pathToExecutable = "/usr/lib/TXM/cwb/bin/cqpserver"; //$NON-NLS-1$
		String pathToRegistry = "/home/mdecorde/TXM/registry"; //$NON-NLS-1$
		String pathToInitFile = "/usr/lib/TXM/cwb/cqpserver.init"; //$NON-NLS-1$

		String corpus = "DISCOURS"; //$NON-NLS-1$
		String subcorpusname = "SUB"; //$NON-NLS-1$
		String subcorpusid = corpus + ":" + subcorpusname; //$NON-NLS-1$
		String query = "[word=\"j.*\"]"; //$NON-NLS-1$
		String regex = "j.*"; //$NON-NLS-1$
		String pattribute1 = corpus + ".word"; //$NON-NLS-1$
		String pattribute2 = corpus + ".frpos"; //$NON-NLS-1$
		String pattribute3 = corpus + ".frlemma"; //$NON-NLS-1$
		String sattribute1 = corpus + ".text_id"; //$NON-NLS-1$
		String sattribute2 = corpus + ".p_id"; //$NON-NLS-1$
		String sattribute3 = corpus + ".s_id"; //$NON-NLS-1$

		String align_attr = "p_id"; //$NON-NLS-1$

		int[] cpos = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		//		int id = 1;
		int[] ids = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		String[] strings = { "je", "jamais" }; //$NON-NLS-1$ //$NON-NLS-2$
		int[] strucpos = { 0, 1, 2, 3, 4 };

		NetCqiServer server = new NetCqiServer(pathToExecutable, pathToRegistry, pathToInitFile, "", false); //$NON-NLS-1$
		if (server.start()) System.out.println("Server ok"); //$NON-NLS-1$
		NetCqiClient client = new NetCqiClient("127.0.0.1", 4877); //$NON-NLS-1$
		if (client.connect("anonymous", "")) System.out.println("Client ok"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		ExecTimer.start();
		try {
			System.out.println("client.alg2Cpos(sattribute, struct_id) : " + Arrays.toString(client.alg2Cpos(sattribute1, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.attributeSize(sattribute1) : " + client.attributeSize(sattribute1)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.attributeSize(sattribute2) : " + client.attributeSize(sattribute2)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.attributeSize(sattribute3) : " + client.attributeSize(sattribute3)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.corpusAlignementAttributes(corpus) : " + Arrays.toString(client.corpusAlignementAttributes(corpus))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.corpusCharset(corpus) : " + client.corpusCharset(corpus)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.corpusFullName(corpus) : " + client.corpusFullName(corpus)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.corpusInfo(corpus) : " + client.corpusInfo(corpus)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.corpusPositionalAttributes(corpus) : " + Arrays.toString(client.corpusPositionalAttributes(corpus))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.corpusProperties(corpus) : " + Arrays.toString(client.corpusProperties(corpus))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.corpusStructuralAttributeHasValues(sattribute1) : " + client.corpusStructuralAttributeHasValues(sattribute1)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.corpusStructuralAttributeHasValues(sattribute2) : " + client.corpusStructuralAttributeHasValues(sattribute2)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.corpusStructuralAttributeHasValues(sattribute3) : " + client.corpusStructuralAttributeHasValues(sattribute3)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.corpusStructuralAttributes(corpus) : " + Arrays.toString(client.corpusStructuralAttributes(corpus))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2Alg(align_attr, cpos) : " + Arrays.toString(client.cpos2Alg(align_attr, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2Id(pattribute1, cpos) : " + Arrays.toString(client.cpos2Id(pattribute1, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.cpos2Id(pattribute2, cpos) : " + Arrays.toString(client.cpos2Id(pattribute2, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.cpos2Id(pattribute3, cpos) : " + Arrays.toString(client.cpos2Id(pattribute3, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2LBound(sattribute1, cpos) : " + Arrays.toString(client.cpos2LBound(sattribute1, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.cpos2LBound(sattribute2, cpos) : " + Arrays.toString(client.cpos2LBound(sattribute2, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.cpos2LBound(sattribute3, cpos) : " + Arrays.toString(client.cpos2LBound(sattribute3, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2RBound(sattribute1, cpos) : " + Arrays.toString(client.cpos2RBound(sattribute1, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.cpos2RBound(sattribute2, cpos) : " + Arrays.toString(client.cpos2RBound(sattribute2, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.cpos2RBound(sattribute3, cpos) : " + Arrays.toString(client.cpos2RBound(sattribute3, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2Str(pattribute1, cpos) : " + Arrays.toString(client.cpos2Str(pattribute1, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.cpos2Str(pattribute2, cpos) : " + Arrays.toString(client.cpos2Str(pattribute2, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.cpos2Str(pattribute3, cpos) : " + Arrays.toString(client.cpos2Str(pattribute3, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cpos2Struc(sattribute1, cpos) : " + Arrays.toString(client.cpos2Struc(sattribute1, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.cpos2Struc(sattribute2, cpos) : " + Arrays.toString(client.cpos2Struc(sattribute2, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.cpos2Struc(sattribute3, cpos) : " + Arrays.toString(client.cpos2Struc(sattribute3, cpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.cqpQuery(corpus, subcorpus, query)"); //$NON-NLS-1$
			client.cqpQuery(corpus, subcorpusname, query);
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.dropAttribute(pattribute1)"); //$NON-NLS-1$
			client.dropAttribute(pattribute1);
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.dropAttribute(pattribute2)"); //$NON-NLS-1$
			client.dropAttribute(pattribute2);
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.dropAttribute(pattribute3)"); //$NON-NLS-1$
			client.dropAttribute(pattribute3);
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.dropCorpus(corpus)"); //$NON-NLS-1$
			client.dropCorpus(corpus);
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println(CQPSearchEngineCoreMessages.clientdumpSubCorpussubcorpusNetCqiClientCQICONSTFIELDMATCH12Colon
					+ Arrays.toString(client.dumpSubCorpus(subcorpusid, NetCqiClient.CQI_CONST_FIELD_MATCH, 1, 10)));
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.dumpSubCorpus(subcorpus, NetCqiClient.CQI_CONST_FIELD_MATCHEND, 1, 2) : " //$NON-NLS-1$
					+ Arrays.toString(client.dumpSubCorpus(subcorpusid, NetCqiClient.CQI_CONST_FIELD_MATCHEND, 1, 10)));
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			//System.out.println("//client.fdist1(arg0, arg1, arg2, arg3) : "+client.fdist1(arg0, arg1, arg2, arg3));
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			//System.out.println("//client.fdist2(arg0, arg1, arg2, arg3, arg4, arg5) : "+client.fdist2(arg0, arg1, arg2, arg3, arg4, arg5));
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.getLastCqiError() : " + client.getLastCqiError()); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.getLastCQPError() : " + client.getLastCQPError()); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.id2Cpos(pattribute1, 0) : " + Arrays.toString(client.id2Cpos(pattribute1, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.id2Cpos(pattribute2, 0) : " + Arrays.toString(client.id2Cpos(pattribute2, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.id2Cpos(pattribute3, 0) : " + Arrays.toString(client.id2Cpos(pattribute3, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.id2Cpos(pattribute1, 1) : " + Arrays.toString(client.id2Cpos(pattribute1, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.id2Cpos(pattribute2, 1) : " + Arrays.toString(client.id2Cpos(pattribute2, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.id2Cpos(pattribute3, 1) : " + Arrays.toString(client.id2Cpos(pattribute3, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.id2Cpos(pattribute1, 2) : " + Arrays.toString(client.id2Cpos(pattribute1, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.id2Cpos(pattribute2, 2) : " + Arrays.toString(client.id2Cpos(pattribute2, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.id2Cpos(pattribute3, 2) : " + Arrays.toString(client.id2Cpos(pattribute3, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.id2Freq(pattribute1, ids) : " + Arrays.toString(client.id2Freq(pattribute1, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.id2Freq(pattribute2, ids) : " + Arrays.toString(client.id2Freq(pattribute2, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.id2Freq(pattribute3, ids) : " + Arrays.toString(client.id2Freq(pattribute3, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.id2Str(pattribute1, ids) : " + Arrays.toString(client.id2Str(pattribute1, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.id2Str(pattribute2, ids) : " + Arrays.toString(client.id2Str(pattribute2, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.id2Str(pattribute3, ids) : " + Arrays.toString(client.id2Str(pattribute3, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.idList2Cpos(pattribute1, ids) : " + Arrays.toString(client.idList2Cpos(pattribute1, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.idList2Cpos(pattribute2, ids) : " + Arrays.toString(client.idList2Cpos(pattribute2, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.idList2Cpos(pattribute3, ids) : " + Arrays.toString(client.idList2Cpos(pattribute3, ids))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.lexiconSize(pattribute1) : " + client.lexiconSize(pattribute1)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.lexiconSize(pattribute2) : " + client.lexiconSize(pattribute2)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.lexiconSize(pattribute3) : " + client.lexiconSize(pattribute3)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.listCorpora() : " + Arrays.toString(client.listCorpora())); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.listSubcorpora(corpus) : " + Arrays.toString(client.listSubcorpora(corpus))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.regex2Id(pattribute1, regex) : " + Arrays.toString(client.regex2Id(pattribute1, regex))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.regex2Id(pattribute2, regex) : " + Arrays.toString(client.regex2Id(pattribute2, regex))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.regex2Id(pattribute3, regex) : " + Arrays.toString(client.regex2Id(pattribute3, regex))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.str2Id(pattribute1, strings) : " + Arrays.toString(client.str2Id(pattribute1, strings))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.str2Id(pattribute2, strings) : " + Arrays.toString(client.str2Id(pattribute2, strings))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.str2Id(pattribute3, strings) : " + Arrays.toString(client.str2Id(pattribute3, strings))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.struc2Cpos(sattribute1, 0) : " + Arrays.toString(client.struc2Cpos(sattribute1, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.struc2Cpos(sattribute2, 0) : " + Arrays.toString(client.struc2Cpos(sattribute2, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.struc2Cpos(sattribute3, 0) : " + Arrays.toString(client.struc2Cpos(sattribute3, 0))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.struc2Cpos(sattribute1, 1) : " + Arrays.toString(client.struc2Cpos(sattribute1, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.struc2Cpos(sattribute2, 1) : " + Arrays.toString(client.struc2Cpos(sattribute2, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.struc2Cpos(sattribute3, 1) : " + Arrays.toString(client.struc2Cpos(sattribute3, 1))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.struc2Cpos(sattribute1, 2) : " + Arrays.toString(client.struc2Cpos(sattribute1, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.struc2Cpos(sattribute2, 2) : " + Arrays.toString(client.struc2Cpos(sattribute2, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.struc2Cpos(sattribute3, 2) : " + Arrays.toString(client.struc2Cpos(sattribute3, 2))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.struc2Cpos(sattribute1, 3) : " + Arrays.toString(client.struc2Cpos(sattribute1, 3))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.struc2Cpos(sattribute2, 3) : " + Arrays.toString(client.struc2Cpos(sattribute2, 3))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.struc2Cpos(sattribute3, 3) : " + Arrays.toString(client.struc2Cpos(sattribute3, 3))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.struc2Str(sattribute1, strucpos) : " + Arrays.toString(client.struc2Str(sattribute1, strucpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.struc2Str(sattribute2, strucpos) : " + Arrays.toString(client.struc2Str(sattribute2, strucpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}
		try {
			System.out.println("client.struc2Str(sattribute3, strucpos) : " + Arrays.toString(client.struc2Str(sattribute3, strucpos))); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.subCorpusHasField(subcorpus, (byte)0) : " + client.subCorpusHasField(subcorpusid, (byte) 0)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.subCorpusSize(subcorpus) : " + client.subCorpusSize(subcorpusid)); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		try {
			System.out.println("client.dropSubCorpus(subcorpus)"); //$NON-NLS-1$
			client.dropSubCorpus(subcorpusid);
		}
		catch (Exception e) {
			System.out.println("Exception : " + e); //$NON-NLS-1$
		}

		System.out.println("TIME: " + ExecTimer.stop()); //$NON-NLS-1$

		server.stop();

	}

	@Override
	public boolean load_a_system_corpus(String regfilepath, String entry) throws IOException, UnexpectedAnswerException, CqiServerError {
		this.streamToServer.write(CQI_CQP_LOAD_SYSTEM_CORPUS);
		this.writeString(regfilepath);
		this.writeString(entry);
		return readBoolean();
	}

	@Override
	public String setOption(String name, String value) throws UnexpectedAnswerException, IOException, CqiServerError {

		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getOption(String name) throws UnexpectedAnswerException, IOException, CqiServerError {

		// TODO Auto-generated method stub
		return null;
	}

}
