package org.txm.searchengine.cqp;

import java.io.IOException;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.core.messages.CQPSearchEngineCoreMessages;
import org.txm.searchengine.cqp.serverException.CqiClErrorCorpusAccess;
import org.txm.searchengine.cqp.serverException.CqiClErrorInternal;
import org.txm.searchengine.cqp.serverException.CqiClErrorNoSuchAttribute;
import org.txm.searchengine.cqp.serverException.CqiClErrorOutOfMemory;
import org.txm.searchengine.cqp.serverException.CqiClErrorOutOfRange;
import org.txm.searchengine.cqp.serverException.CqiClErrorRegex;
import org.txm.searchengine.cqp.serverException.CqiClErrorWrongAttributeType;
import org.txm.searchengine.cqp.serverException.CqiCqpErrorErrorGeneral;
import org.txm.searchengine.cqp.serverException.CqiCqpErrorInvalidField;
import org.txm.searchengine.cqp.serverException.CqiCqpErrorNoSuchCorpus;
import org.txm.searchengine.cqp.serverException.CqiCqpErrorOutOfRange;
import org.txm.searchengine.cqp.serverException.CqiCqpErrorSyntax;
import org.txm.searchengine.cqp.serverException.CqiErrorConnectRefused;
import org.txm.searchengine.cqp.serverException.CqiErrorGeneralError;
import org.txm.searchengine.cqp.serverException.CqiErrorSyntaxError;
import org.txm.searchengine.cqp.serverException.CqiErrorUserAbort;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.logger.Log;

/**
 * @author mdecorde
 *         Declares all the natives methods
 */
public class MemCqiClient extends AbstractCqiClient {

	/**
	 * The server to deal with.
	 */
	protected MemCqiServer server;

	/**
	 * 
	 * @param server
	 * @throws Exception
	 */
	public MemCqiClient(MemCqiServer server) throws Exception {
		super();
		this.server = server;
	}

	public boolean start(String[] args) {
		return true;
	}

	public synchronized void stop() {
		server.stop();
	}

	public static final byte[] intToByteArray(int value) {
		return new byte[] {
				(byte) (value >>> 24),
				(byte) (value >>> 16),
				(byte) (value >>> 8),
				(byte) value };
	}

	public byte[] throwExceptionFromCqi(int error) throws CqiServerError, UnexpectedAnswerException, IOException {
		return throwExceptionFromCqi("", error);
	}

	public byte[] throwExceptionFromCqi(String message, int error) throws CqiServerError, UnexpectedAnswerException, IOException {
		byte[] ret = intToByteArray(error);

		byte b = ret[2];
		byte b2 = ret[3];
		// System.out.println("bytes: "+ret[0]+" "+ret[1]+" "+ret[2]+" "+ret[3]);
		// System.out.println("b1 "+b+ " b2 "+b2);
		Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.readByteFromTheCQiServerColonP0, b));
		switch (b) {
			case 0x00:// cf cqi.h:29
				return CQI_PADDING;
			case 0x01:// cf cqi.h:37
				Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.readByteFromTheCQiServerColonP0, b));
				switch (b2) {
					case 0x01:// cf cqi.h:39
						return CQI_STATUS_OK;
					case 0x02:// cf cqi.h:40
						return CQI_STATUS_CONNECT_OK;
					case 0x03:// cf cqi.h:41
						return CQI_STATUS_BYE_OK;
					case 0x04:// cf cqi.h:42
						return CQI_STATUS_PING_OK;
					default:
						throw new CqiErrorGeneralError(CQPSearchEngineCoreMessages.unknownCQPErrorColonB1 + b + CQPSearchEngineCoreMessages.b2 + b2);
				}
			case 0x02:// cf cqi.h:45
				Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.readByteFromTheCQiServerColonP0, b));

				lastError = CQPSearchEngineCoreMessages.internalCQIErrorColon + " CQI_ERROR (" + b + ")"; //$NON-NLS-1$ //$NON-NLS-2$

				switch (b2) {
					case 0x01:// cf cqi.h:39
						throw new CqiErrorGeneralError();
					case 0x02:// cf cqi.h:40
						throw new CqiErrorConnectRefused();
					case 0x03:// cf cqi.h:41
						throw new CqiErrorUserAbort();
					case 0x04:// cf cqi.h:42
						throw new CqiErrorSyntaxError();
					default:
						throw new CqiErrorGeneralError(CQPSearchEngineCoreMessages.unknownCQPCQIErrorColonB1 + b + CQPSearchEngineCoreMessages.b2 + b2);
				}
			case 0x03:// cf cqi.h:53
				Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.readByteFromTheCQiServerColonP0, b));
				switch (b2) {
					case 0x01:// cf cqi.h:39
						return CQI_DATA_BYTE;
					case 0x02:// cf cqi.h:40
						return CQI_DATA_BOOL;
					case 0x03:// cf cqi.h:41
						return CQI_DATA_INT;
					case 0x04:// cf cqi.h:42
						return CQI_DATA_STRING;
					case 0x05:// cf cqi.h:42
						return CQI_DATA_BYTE_LIST;
					case 0x06:// cf cqi.h:42
						return CQI_DATA_BOOL_LIST;
					case 0x07:// cf cqi.h:42
						return CQI_DATA_INT_LIST;
					case 0x08:// cf cqi.h:42
						return CQI_DATA_STRING_LIST;
					case 0x09:// cf cqi.h:42
						return CQI_DATA_INT_INT;
					case 0x0A:// cf cqi.h:42
						return CQI_DATA_INT_INT_INT_INT;
					case 0x0B:// cf cqi.h:42
						return CQI_DATA_INT_TABLE;
					default:
						throw new CqiErrorGeneralError(CQPSearchEngineCoreMessages.unknownCQPCodeColonB1 + b + CQPSearchEngineCoreMessages.b2 + b2);
				}
			case 0x04:// cf cqi.h:67

				lastError = CQPSearchEngineCoreMessages.cLErrorP0 + "CL_ERROR (" + b + ")"; //$NON-NLS-1$ //$NON-NLS-2$
				Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.readByteFromTheCQiServerColonP0, b));
				switch (b2) {
					case 0x01:// cf cqi.h:39
						lastError += ": NoSuchAttribute error"; //$NON-NLS-1$
						throw new CqiClErrorNoSuchAttribute(message);
					case 0x02:// cf cqi.h:40
						lastError += ": WrongAttributeType error"; //$NON-NLS-1$
						throw new CqiClErrorWrongAttributeType(message);
					case 0x03:// cf cqi.h:41
						lastError += ": OutOfRange error"; //$NON-NLS-1$
						throw new CqiClErrorOutOfRange(message);
					case 0x04:// cf cqi.h:42
						lastError = this.getLastCqiError();
						throw new CqiClErrorRegex(message);
					case 0x05:// cf cqi.h:42
						lastError += ": CorpusAccess error"; //$NON-NLS-1$
						throw new CqiClErrorCorpusAccess(message);
					case 0x06:// cf cqi.h:42
						lastError += ": OutOfMemory error"; //$NON-NLS-1$
						throw new CqiClErrorOutOfMemory(message);
					case 0x07:// cf cqi.h:42
						lastError += ": Internal error"; //$NON-NLS-1$
						throw new CqiClErrorInternal(message);
					default:
						lastError += ": Unknown error"; //$NON-NLS-1$
						throw new CqiErrorGeneralError(CQPSearchEngineCoreMessages.unknownCQPErrorColonB1 + b + CQPSearchEngineCoreMessages.b2 + b2);
				}

			case 0x05:// cf cqi.h:94

				lastError = NLS.bind(CQPSearchEngineCoreMessages.cQPErrorColonP0, "CQI_CQP_ERROR (" + b + ")"); //$NON-NLS-1$ //$NON-NLS-2$
				Log.finest(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.readByteFromTheCQiServerColonP0, b));
				switch (b2) {
					case 0x01:// cf cqi.h:39
						lastError += ": General error"; //$NON-NLS-1$
						throw new CqiCqpErrorErrorGeneral(message);
					case 0x02:// cf cqi.h:40
						lastError += ": NoSuchCorpus error"; //$NON-NLS-1$
						throw new CqiCqpErrorNoSuchCorpus(message);
					case 0x03:// cf cqi.h:41
						lastError += ": InvalidField error"; //$NON-NLS-1$
						throw new CqiCqpErrorInvalidField(message);
					case 0x04:// cf cqi.h:42
						lastError += ": OutOfRange error"; //$NON-NLS-1$
						throw new CqiCqpErrorOutOfRange(message);
					case 0x05:// cf cqi.h:44
						lastError = getLastCQPError();
						throw new CqiCqpErrorSyntax(message);
					default:
						throw new CqiErrorGeneralError(CQPSearchEngineCoreMessages.unknownCQPErrorColonB1 + b + CQPSearchEngineCoreMessages.b2 + b2);
				}
			default:
				throw new CqiErrorGeneralError(CQPSearchEngineCoreMessages.unknownCQPCodesColonB1 + b + CQPSearchEngineCoreMessages.b2 + b2);
		}
		// return null;
	}

	@Override
	public synchronized int[] alg2Cpos(String arg0, int arg1) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null) {
			throw new CqiServerError("alg2Cpos called with NULL parameter."); //$NON-NLS-1$
		}
		int[] rez = server.alg2Cpos(arg0, arg1);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized int attributeSize(String arg0) throws IOException, UnexpectedAnswerException, CqiServerError {
		if (arg0 == null) {
			throw new CqiServerError("attributeSize called with NULL parameter."); //$NON-NLS-1$
		}
		Integer ret = server.attributeSize(arg0);
		if (ret == null || ret == -1) {
			throwExceptionFromCqi(server.getErrorCode());
			// throw exception
		}
		return ret;
	}

	@Override
	public synchronized boolean connect(String arg0, String arg1) {
		return server.isLoaded;
	};

	@Override
	public synchronized String[] corpusAlignementAttributes(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null) {
			throw new CqiServerError("corpusAlignementAttributes called with NULL parameter."); //$NON-NLS-1$
		}
		String[] rez = server.corpusAlignementAttributes(arg0);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized String corpusCharset(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null) {
			throw new CqiServerError("corpusCharset called with NULL parameter."); //$NON-NLS-1$
		}
		String rez = server.corpusCharset(arg0);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	//public native String getOption(String name) throws UnexpectedAnswerException, IOException, CqiServerError;
	@Override
	public synchronized String getOption(String name) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (name == null) {
			throw new CqiServerError("getOption called with NULL parameter."); //$NON-NLS-1$
		}
		String rez = server.getOption(name);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	//public native String setOption(String name, String value) throws UnexpectedAnswerException, IOException, CqiServerError;
	@Override
	public synchronized String setOption(String name, String value) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (name == null) {
			throw new CqiServerError("setOption called with NULL name parameter."); //$NON-NLS-1$
		}
		if (value == null) {
			throw new CqiServerError("setOption called with NULL value parameter."); //$NON-NLS-1$
		}
		String rez = server.setOption(name, value);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized String corpusFullName(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null) {
			throw new CqiServerError("corpusFullName called with NULL parameter."); //$NON-NLS-1$
		}
		String rez = server.corpusFullName(arg0);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public String[] corpusInfo(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError {
		throw new UnsupportedOperationException();
		/*
		 * String[] rez = server.corpusInfo(arg0);
		 * if (rez == null) {
		 * throwExceptionFromCqi(server.getErrorCode());
		 * }
		 * return rez;
		 */
	}

	@Override
	public synchronized String[] corpusPositionalAttributes(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null) {
			throw new CqiServerError("corpusPositionalAttributes called with NULL parameter."); //$NON-NLS-1$
		}
		String[] rez = server.corpusPositionalAttributes(arg0);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized String[] corpusProperties(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null) {
			throw new CqiServerError("corpusProperties called with NULL parameter."); //$NON-NLS-1$
		}
		String[] rez = server.corpusProperties(arg0);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized boolean corpusStructuralAttributeHasValues(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null) {
			throw new CqiServerError("corpusStructuralAttributeHasValues called with NULL parameter."); //$NON-NLS-1$
		}
		Boolean rez = server.corpusStructuralAttributeHasValues(arg0);
		if (rez == null || !rez) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized String[] corpusStructuralAttributes(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null) {
			throw new CqiServerError("corpusStructuralAttributes called with NULL parameter."); //$NON-NLS-1$
		}
		String[] rez = server.corpusStructuralAttributes(arg0);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized int[] cpos2Alg(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null || arg1 == null) {
			throw new CqiServerError("cpos2Alg called with NULL parameter."); //$NON-NLS-1$
		}
		int[] rez = server.cpos2Alg(arg0, arg1);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized int[] cpos2Id(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null || arg1 == null) {
			throw new CqiServerError("cpos2Id called with NULL parameter."); //$NON-NLS-1$
		}
		int[] rez = server.cpos2Id(arg0, arg1);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized int[] cpos2LBound(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null || arg1 == null) {
			throw new CqiServerError("cpos2LBound called with NULL parameter."); //$NON-NLS-1$
		}
		int[] rez = server.cpos2LBound(arg0, arg1);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized int[] cpos2RBound(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null || arg1 == null) {
			throw new CqiServerError("cpos2RBound called with NULL parameter."); //$NON-NLS-1$
		}
		int[] rez = server.cpos2RBound(arg0, arg1);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized String[] cpos2Str(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null || arg1 == null) {
			throw new CqiServerError("cpos2Str called with NULL parameter."); //$NON-NLS-1$
		}
		String[] rez = server.cpos2Str(arg0, arg1);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized int[] cpos2Struc(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null || arg1 == null) {
			throw new CqiServerError("cpos2Struc called with NULL parameter."); //$NON-NLS-1$
		}
		int[] rez = server.cpos2Struc(arg0, arg1);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized void cqpQuery(String arg0, String arg1, String arg2)
			throws IOException, UnexpectedAnswerException, CqiServerError {
		if (arg0 == null || arg1 == null || arg2 == null) {
			throw new CqiServerError("cqpQuery called with NULL parameter."); //$NON-NLS-1$
		}
		Boolean ret = server.cqpQuery(arg0, arg1, arg2);
		// System.out.println("CQPSERVER return: "+ret);
		if (ret == null || !ret) {
			int e = server.getErrorCode();
			// System.out.println("throw exception with cqp error code: "+e);
			throwExceptionFromCqi(e);
		}
	}

	@Override
	public synchronized void query(String arg0)
			throws IOException, UnexpectedAnswerException, CqiServerError {
		if (arg0 == null) {
			throw new CqiServerError("query called with NULL parameter."); //$NON-NLS-1$
		}
		Boolean ret = server.query(arg0);
		if (ret == null || !ret) {
			System.out.println(CQPSearchEngineCoreMessages.queryError);
			throwExceptionFromCqi(server.getErrorCode());
		}
	}

	@Override
	public boolean disconnect() {
		return true;
	};

	@Override
	public void dropAttribute(String arg0) throws IOException, UnexpectedAnswerException, CqiServerError {
		throw new UnsupportedOperationException();
		// Boolean ret = server.dropAttribute(arg0);
		// if(ret == null || !ret) {
		// throwExceptionFromCqi(server.getErrorCode());
		// }
	}

	@Override
	public synchronized void dropCorpus(String arg0) throws Exception {
		Log.finest(NLS.bind("Droping corpus {0}...", arg0)); //$NON-NLS-1$
		if (arg0 == null) {
			throw new Exception("dropCorpus called with NULL parameter."); //$NON-NLS-1$
		}
		Boolean ret = server.dropCorpus(arg0);
		if (ret == null || !ret) {
			throwExceptionFromCqi(server.getErrorCode());
		}
	}

	@Override
	public synchronized void dropSubCorpus(String arg0) throws IOException, UnexpectedAnswerException, CqiServerError {
		Log.finest(NLS.bind("Droping subcorpus {0}...", arg0)); //$NON-NLS-1$
		if (arg0 == null) {
			throw new CqiServerError("dropSubCorpus called with NULL parameter."); //$NON-NLS-1$
		}
		Boolean ret = server.dropSubCorpus(arg0);
		if (ret == null || !ret) {
			throwExceptionFromCqi("Fail to drop subcorpus " + arg0 + ": ", server.getErrorCode()); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	@Override
	public synchronized int[] dumpSubCorpus(String arg0, byte arg1, int arg2, int arg3)
			throws IOException, UnexpectedAnswerException, CqiServerError {
		if (arg0 == null) {
			throw new CqiServerError("dumpSubCorpus called with NULL parameter."); //$NON-NLS-1$
		}
		int[] rez = server.dumpSubCorpus(arg0, arg1, arg2, arg3);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public int[][] fdist1(String arg0, int arg1, byte arg2, String arg3)
			throws IOException, UnexpectedAnswerException, CqiServerError {
		if (arg0 == null || arg3 == null) {
			throw new CqiServerError("fdist1 called with NULL parameter."); //$NON-NLS-1$
		}
		int[] all = nfdist1(arg0, arg1, arg2, arg3);

		int[][] freqs = new int[all.length / 2][2];
		int c = 0;
		for (int i = 0; i < freqs.length; i++) {
			freqs[i][0] = all[c++];
			freqs[i][1] = all[c++];
		}
		return freqs;

	}

	public synchronized int[] nfdist1(String arg0, int arg1, byte arg2, String arg3)
			throws IOException, UnexpectedAnswerException, CqiServerError {
		if (arg0 == null || arg3 == null) {
			throw new CqiServerError("nfdist1 called with NULL parameter."); //$NON-NLS-1$
		}
		int[] rez = server.nfdist1(arg0, arg1, arg2, arg3);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public int[][] fdist2(String arg0, int arg1, byte arg2, String arg3,
			byte arg4, String arg5) throws IOException,
			UnexpectedAnswerException, CqiServerError {
		if (arg0 == null || arg3 == null || arg5 == null) {
			throw new CqiServerError("fdist2 called with NULL parameter."); //$NON-NLS-1$
		}
		int[] all = nfdist2(arg0, arg1, arg2, arg3, arg4, arg5);
		if (all == null) {
			return new int[0][0];
		}
		int[][] freqs = new int[all.length / 2][3];
		int c = 0;
		for (int i = 0; i < freqs.length; i++) {
			freqs[i][0] = all[c++];
			freqs[i][1] = all[c++];
			freqs[i][2] = all[c++];
		}
		return freqs;
	}

	public synchronized int[] nfdist2(String arg0, int arg1, byte arg2, String arg3,
			byte arg4, String arg5) throws IOException, UnexpectedAnswerException, CqiServerError {
		if (arg0 == null || arg3 == null || arg5 == null) {
			throw new CqiServerError("nfdist2 called with NULL parameter."); //$NON-NLS-1$
		}
		int[] rez = server.nfdist2(arg0, arg1, arg2, arg3, arg4, arg5);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized String getLastCQPError() throws UnexpectedAnswerException, IOException, CqiServerError {
		String rez = server.getLastCQPError();
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public String getLastCqiError() throws UnexpectedAnswerException, IOException, CqiServerError {
		String rez = server.getLastCqiError();
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized int[] id2Cpos(String arg0, int arg1) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null) {
			throw new CqiServerError("id2Cpos called with NULL parameter."); //$NON-NLS-1$
		}
		int[] rez = server.id2Cpos(arg0, arg1);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized int[] id2Freq(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null || arg1 == null) {
			throw new CqiServerError("id2Freq called with NULL parameter."); //$NON-NLS-1$
		}
		int[] rez = server.id2Freq(arg0, arg1);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized String[] id2Str(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null || arg1 == null) {
			throw new CqiServerError("id2Str called with NULL parameter."); //$NON-NLS-1$
		}
		String[] rez = server.id2Str(arg0, arg1);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized int[] idList2Cpos(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null || arg1 == null) {
			throw new CqiServerError("idList2Cpos called with NULL parameter."); //$NON-NLS-1$
		}
		int[] rez = server.idList2Cpos(arg0, arg1);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized int lexiconSize(String arg0) throws IOException, UnexpectedAnswerException, CqiServerError {
		if (arg0 == null) {
			throw new CqiServerError("lexiconSize called with NULL parameter."); //$NON-NLS-1$
		}
		Integer rez = server.lexiconSize(arg0);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized String[] listCorpora() throws UnexpectedAnswerException, IOException, CqiServerError {
		String[] rez = server.listCorpora();
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized String[] listSubcorpora(String arg0) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null) {
			throw new CqiServerError("listSubcorpora called with NULL parameter."); //$NON-NLS-1$
		}
		String[] rez = server.listSubcorpora(arg0);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public boolean reconnect() {
		return true;
	};

	@Override
	public synchronized int[] regex2Id(String arg0, String arg1) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null || arg1 == null) {
			throw new CqiServerError("regex2Id called with NULL parameter."); //$NON-NLS-1$
		}
		int[] rez = server.regex2Id(arg0, arg1);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized int[] str2Id(String arg0, String[] arg1) throws IOException, UnexpectedAnswerException, CqiServerError {
		if (arg0 == null || arg1 == null) {
			throw new CqiServerError("str2Id called with NULL parameter."); //$NON-NLS-1$
		}
		int[] rez = server.str2Id(arg0, arg1);
		if (rez == null) {
			// System.out.println("error: "+server.getErrorCode());
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized int[] struc2Cpos(String arg0, int arg1) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null) {
			throw new CqiServerError("struc2Cpos called with NULL parameter."); //$NON-NLS-1$
		}
		int[] rez = server.struc2Cpos(arg0, arg1);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized String[] struc2Str(String arg0, int[] arg1) throws UnexpectedAnswerException, IOException, CqiServerError {
		if (arg0 == null || arg1 == null) {
			throw new CqiServerError("struc2Str called with NULL parameter."); //$NON-NLS-1$
		}
		String[] rez = server.struc2Str(arg0, arg1);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized boolean subCorpusHasField(String arg0, byte arg1) throws IOException, UnexpectedAnswerException, CqiServerError {
		Boolean rez = server.subCorpusHasField(arg0, arg1);
		if (rez == null || !rez) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized int subCorpusSize(String arg0) throws IOException, UnexpectedAnswerException, CqiServerError {
		if (arg0 == null) {
			throw new CqiServerError("subCorpusSize called with NULL parameter."); //$NON-NLS-1$
		}
		Integer rez = server.subCorpusSize(arg0);
		if (rez == null) {
			throwExceptionFromCqi(server.getErrorCode());
		}
		return rez;
	}

	@Override
	public synchronized boolean load_a_system_corpus(String regfilepath, String entry) throws IOException, UnexpectedAnswerException, CqiServerError {
		if (regfilepath == null) {
			throw new CqiServerError("load_a_system_corpus called with NULL regfilepath parameter."); //$NON-NLS-1$
		}
		if (entry == null) {
			throw new CqiServerError("load_a_system_corpus called with NULL entry parameter."); //$NON-NLS-1$
		}
		return server.load_a_system_corpus(regfilepath, entry);
	}

}
