// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2012-03-01 08:14:11 +0100 (jeu., 01 mars 2012) $
// $LastChangedRevision: 2134 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.regex.Pattern;

import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.core.messages.CQPSearchEngineCoreMessages;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.serverException.CqiCqpErrorNoSuchCorpus;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.logger.Log;

/**
 * This class implements a Java Interface CQi client.
 *
 * @author mdecorde
 */
public abstract class AbstractCqiClient implements ICqiClient {

	static Pattern pattern = Pattern.compile("\\p{Upper}(\\p{Upper}|\\p{Digit}|[_-])*"); //$NON-NLS-1$

	static Pattern pattern2 = Pattern.compile("\\p{Upper}(\\p{Lower}|\\p{Digit}|[_-])*"); //$NON-NLS-1$

	String lastError;

	/**
	 * Check wether <code>id</code> is a valid CQi for a corpus.
	 *
	 * @param id the id
	 * @return true if <code>id</code> is in uppercase chararcters
	 */
	public static synchronized boolean checkCorpusId(String id) {
		// System.out.println("Pattern: "+pattern+" test with "+id);
		return pattern.matcher(id).matches();
	}

	/**
	 * Check whether <code>id</code> is a valid CQi for a subcorpus.
	 *
	 * @param id the id
	 * @return true if <code>id</code> is an uppercase character followed by
	 *         lowercase characters
	 */
	public static synchronized boolean checkSubcorpusId(String id) {
		// System.out.println("Pattern: "+pattern+" test with "+id);
		return pattern2.matcher(id).matches();
	}

	public synchronized String getLastError() {
		if (lastError != null && lastError.length() > 0) {
			return lastError;
		}
		return CQPSearchEngineCoreMessages.noError;
	}

	public List<String> getSingleData(Property prop) throws IOException, CqiServerError, CqiClientException {
		boolean isWordProperty = !(prop instanceof StructuralUnitProperty);
		String qname = prop.getQualifiedName();
		AbstractCqiClient cqiClient = CorpusManager.getCorpusManager().getCqiClient();

		if (!isWordProperty) {
			StructuralUnitProperty sprop = (StructuralUnitProperty) prop;
			QueryResult qresult = prop.getCorpus().query(new CQLQuery("<" + sprop.getFullName() + ">[] expand to " + sprop.getName()), "TMP", false); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			int[] strucs = this.cpos2Struc(qname, qresult.getStarts());
			return Arrays.asList(cqiClient.struc2Str(qname, strucs));
		}
		else {
			int[] positions = { 0, 1, 2, 3 };
			return Arrays.asList(cqiClient.cpos2Str(qname, positions));
		}
	}

	public List<String> getSingleData(Property prop, int[] positions) throws UnexpectedAnswerException, IOException, CqiServerError {
		String qname = prop.getQualifiedName();
		AbstractCqiClient cqiClient = CorpusManager.getCorpusManager().getCqiClient();
		if (prop instanceof StructuralUnitProperty) {
			int[] strucs = cqiClient.cpos2Struc(qname, positions);
			return Arrays.asList(cqiClient.struc2Str(qname, strucs));
		}
		else if (prop instanceof WordProperty) {
			return Arrays.asList(cqiClient.cpos2Str(qname, positions));
		}
		return null;
	}

	public List<List<String>> getData(Property prop, List<Integer> positions,
			List<Integer> nWords) throws CqiClientException, IOException, CqiServerError {

		// System.out.println("START prop: "+prop);
		// System.out.println("positions : "+positions);
		// System.out.println("nwords : "+nWords);

		List<List<String>> result = new ArrayList<>();

		// get all the positions needed, possible overlap
		HashSet<Integer> allPositionsNeeded = new HashSet<>();
		for (int i = 0; i < positions.size(); i++) {
			int n = nWords.get(i);
			for (int j = 0; j < n; j++) {
				allPositionsNeeded.add(positions.get(i) + j);
			}
		}

		// conversion from List<Integer> to int[]
		int[] cpos = new int[allPositionsNeeded.size()];
		int c = 0;
		for (int i : allPositionsNeeded) {
			cpos[c++] = i;
		}

		// get values for positions
		String[] values;
		if (prop instanceof StructuralUnitProperty) {
			String qname = ((StructuralUnitProperty) prop).getQualifiedName();
			int[] structs = this.cpos2Struc(qname, cpos);
			values = this.struc2Str(qname, structs);
		}
		else if (prop instanceof WordProperty) {
			values = ((WordProperty) prop).cpos2Str(cpos);
		}
		else {
			return null;
		}

		// sort results by position
		TreeMap<Integer, String> map = new TreeMap<>();
		for (int i = 0; i < values.length; i++) {
			map.put(cpos[i], values[i]);
		}

		// fill results
		int start, end;
		SortedMap<Integer, String> smap;
		for (int i = 0; i < positions.size(); i++) {
			start = positions.get(i);
			end = positions.get(i) + nWords.get(i);
			if (start > end) {
				Log.warning("Error: trying to get " + prop + " values from " + start + " to " + end);
				result.add(new ArrayList<String>());
			}
			else {
				smap = map.subMap(start, end);
				result.add(new ArrayList<>(smap.values()));
			}
		}

		return result;
	}

	/**
	 * Return lists of strings.
	 *
	 * @return the data
	 * @throws CqiClientException the cqi client exception
	 */
	public List<String> getData(StructuralUnitProperty property, CQPCorpus corpus) throws CqiClientException {
		QueryResult tmp = corpus.query(new CQLQuery("<" + property.getFullName() + ">[]"), UUID.randomUUID().toString(), false); //$NON-NLS-1$ //$NON-NLS-2$
		List<Match> matches = tmp.getMatches();
		tmp.drop();

		ArrayList<String> ret = new ArrayList<>(new HashSet<>(Match.getValuesForProperty(property, matches)));
		return ret;
	}

	/**
	 * Gets the data.
	 *
	 * @param number the number
	 * @return return an abstract of values
	 * @throws CqiClientException the cqi client exception
	 */
	public List<String> getData(StructuralUnitProperty prop, int number) throws CqiClientException {

		QueryResult tmp = prop.getCorpus().query(new CQLQuery("<" + prop.getFullName() + ">[] expand to " + prop.getName()), UUID.randomUUID().toString(), false); //$NON-NLS-1$ //$NON-NLS-2$
		if (number > tmp.getNMatch()) {
			number = tmp.getNMatch();
		}
		List<Match> matches = tmp.getMatches(0, number);
		tmp.drop();

		return new ArrayList<>(new HashSet<>(Match.getValuesForProperty(prop, matches)));
		// System.out.println("Data "+property.getQualifiedName()+": "+data);

	}

	// CQI_ERROR_CONNECT_REFUSED
	/**
	 * Connect the client to a server.
	 *
	 * @param username the username
	 * @param password the password
	 * @return true, if successful
	 */
	@Override
	public abstract boolean connect(String username, String password) throws UnexpectedAnswerException, IOException, CqiServerError;

	// None
	/**
	 * Disconnect.
	 * T
	 * 
	 * @return true, if successful
	 */
	@Override
	public abstract boolean disconnect() throws UnexpectedAnswerException, CqiServerError, IOException;

	// CQI_CTRL_LAST_GENERAL_ERROR
	/**
	 * return the last CQP error.
	 *
	 * @return the last error
	 */
	@Override
	public abstract String getLastCqiError() throws UnexpectedAnswerException, IOException, CqiServerError;

	// CQI_CTRL_LAST_CQP_ERROR
	/**
	 * return the last CQP error.
	 *
	 * @return the last error
	 */
	@Override
	public abstract String getLastCQPError() throws UnexpectedAnswerException, IOException, CqiServerError;

	// None
	/**
	 * Lists the corpora available on the server.
	 *
	 * @return the name of the corpora
	 */
	@Override
	public abstract String[] listCorpora() throws UnexpectedAnswerException, IOException, CqiServerError;



	// None
	/**
	 * Gives the corpus charset.
	 *
	 * @param corpus the corpus
	 * @return the name of the charset
	 */
	@Override
	public abstract String corpusCharset(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError;


	// None (not really implemented anyway)
	/**
	 * Gives the corpus properties.
	 *
	 * @param corpus the corpus
	 * @return the properties
	 */
	@Override
	public abstract String[] corpusProperties(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError;

	// CQI_CQP_ERROR_NO_SUCH_CORPUS
	/**
	 * Gives the corpus positional attributes.
	 *
	 * @param corpusID the corpus id
	 * @return the name of the attributes
	 */
	@Override
	public abstract String[] corpusPositionalAttributes(String corpusID) throws UnexpectedAnswerException, IOException, CqiServerError;

	// CQI_CQP_ERROR_NO_SUCH_CORPUS
	/**
	 * Gives the corpus structural attributes.
	 *
	 * @param corpus the corpus
	 * @return the name of the attributes
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public abstract String[] corpusStructuralAttributes(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError;

	// CQI_CQP_ERROR_NO_SUCH_CORPUS, CQI_CL_ERROR_NO_SUCH_ATTRIBUTE,
	// CQI_CL_ERROR_WRONG_ATTRIBUTE_TYPE
	/**
	 * Check wether a structural attribute has values.
	 *
	 * @param attribute the attribute
	 * @return true, if it has values
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public abstract boolean corpusStructuralAttributeHasValues(String attribute) throws UnexpectedAnswerException, IOException,
			CqiServerError;

	// CQI_CQP_ERROR_NO_SUCH_CORPUS
	/**
	 * Gives the corpus alignement attributes.
	 *
	 * @param corpus the corpus
	 * @return the name of attributes
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public abstract String[] corpusAlignementAttributes(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError;

	// CQI_CQP_ERROR_NO_SUCH_CORPUS
	/**
	 * Gives the corpus full name.
	 *
	 * @param corpus the corpus
	 * @return the full name
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public abstract String corpusFullName(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Gives the corpus info listed in the .INFO file.
	 *
	 * @param corpus the corpus
	 * @return the info
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public abstract String[] corpusInfo(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Drop a corpus.
	 * 
	 * @param corpus
	 *            the corpus
	 */
	@Override
	public abstract void dropCorpus(String corpus) throws Exception;

	// CQI_CQP_ERROR_NO_SUCH_CORPUS, CQI_CL_ERROR_NO_SUCH_ATTRIBUTE,
	// CQI_CL_ERROR_WRONG_ATTRIBUTE_TYPE, CQI_CL_ERROR_CORPUS_ACCESS
	/**
	 * Gives an attribute size (the number of token).
	 *
	 * @param attribute the attribute
	 * @return the size
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public abstract int attributeSize(String attribute) throws IOException, UnexpectedAnswerException, CqiServerError;

	// CQI_CQP_ERROR_NO_SUCH_CORPUS, CQI_CL_ERROR_NO_SUCH_ATTRIBUTE,
	// CQI_CL_ERROR_WRONG_ATTRIBUTE_TYPE, CQI_CL_ERROR_CORPUS_ACCESS
	/**
	 * Gives the lexicon size of an attribute.
	 *
	 * @param attribute the attribute
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 * @return the number of entries in the lexicon of a positional attribute
	 */
	@Override
	public abstract int lexiconSize(String attribute) throws IOException, UnexpectedAnswerException, CqiServerError;

	/**
	 * Drop attribute.
	 *
	 * @param attribute the attribute
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public abstract void dropAttribute(String attribute) throws IOException, UnexpectedAnswerException, CqiServerError;

	/**
	 * Converts an array of attribute values to their ID.
	 *
	 * @param attribute the attribute
	 * @param strings the values
	 * @return the IDs
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public abstract int[] str2Id(String attribute, String[] strings) throws IOException, UnexpectedAnswerException, CqiServerError;

	/**
	 * Converts an array of attribute ID to their values.
	 *
	 * @param attribute the attribute
	 * @param ids the ids
	 * @return the values
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public abstract String[] id2Str(String attribute, int[] ids) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Converts an array of attribute IDs to their frequency.
	 *
	 * @param attribute the attribute
	 * @param ids the ids
	 * @return the frequencies
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public abstract int[] id2Freq(String attribute, int[] ids) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Converts an array of position to their ID given an attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the positions
	 * @return the IDs
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	@Override
	public abstract int[] cpos2Id(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Converts an array of position to their value given an attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the cpos
	 * @return the values
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public abstract String[] cpos2Str(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Computes for each position of an array the Id of the enclosing structural
	 * attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the positions
	 * @return the IDs
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public abstract int[] cpos2Struc(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Computes for each position of an array the position of the left boundary
	 * of the enclosing structural attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the cpos
	 * @return the positions of the left boundaries
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public abstract int[] cpos2LBound(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Computes for each position of an array the position of the right boundary
	 * of the enclosing structural attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the cpos
	 * @return the positions of the right boundaries
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public abstract int[] cpos2RBound(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Computes for each position of an array the Id of the enclosing alignment
	 * attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the cpos
	 * @return the int[]
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public abstract int[] cpos2Alg(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Retrieves annotated string values of structure regions in "strucs"; "" if
	 * out of range.
	 *
	 * @param attribute the attribute
	 * @param strucs the strucs
	 * @return the string[]
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public abstract String[] struc2Str(String attribute, int[] strucs) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Retrieves all corpus positions where the given token occurs.
	 *
	 * @param attribute the attribute
	 * @param id the id
	 * @return the position
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public abstract int[] id2Cpos(String attribute, int id) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Retrieves all corpus positions where one of the tokens in "id_list"
	 * occurs; the returned list is sorted as a whole, not per token id.
	 *
	 * @param attribute the attribute
	 * @param ids the ids
	 * @return the positions
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public abstract int[] idList2Cpos(String attribute, int[] ids) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Retrieves the lexicon IDs of all tokens that match "regex"; the returned list may be empty (size 0).
	 *
	 * @param attribute the attribute
	 * @param regex the regex
	 * @return the IDs
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public abstract int[] regex2Id(String attribute, String regex) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Retrieves the start and end corpus positions of structure region "struc".
	 *
	 * @param attribute the attribute
	 * @param struc the struc
	 * @return an array of size 2 containing the start and end positions
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public abstract int[] struc2Cpos(String attribute, int struc) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Retrieves start and end corpus positions of an alignement region in the
	 * source and target corpora "struc".
	 *
	 * @param attribute the attribute
	 * @param struc the struc
	 * @return an array of size 4 containing (src_start, src_end, target_start,
	 *         target_end)
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public abstract int[] alg2Cpos(String attribute, int struc) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Runs a CQL query.
	 *
	 * @param motherCorpus the mother corpus
	 * @param subcorpus the subcorpus
	 * @param query the query
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public abstract void cqpQuery(String motherCorpus, String subcorpus, String query) throws IOException, UnexpectedAnswerException, CqiServerError;

	/**
	 * Runs a CQP query line.
	 *
	 * @param query the query
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public abstract void query(String query) throws IOException, UnexpectedAnswerException, CqiServerError;

	/**
	 * Load a CQP corpus (system) from a registry file
	 *
	 * @return
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError the cqi server error
	 */
	@Override
	public abstract boolean load_a_system_corpus(String regfilepath, String entry) throws IOException, UnexpectedAnswerException, CqiServerError;

	/**
	 * Lists all the subcorpora of a corpus.
	 *
	 * @param corpus the corpus
	 * @return the name of the subcorpora
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiCqpErrorNoSuchCorpus the cqi cqp error no such corpus
	 */
	@Override
	public abstract String[] listSubcorpora(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Gives the size of a subcorpus .
	 * 
	 * @param subcorpus
	 *            the subcorpus
	 * 
	 * @return the size
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	@Override
	public abstract int subCorpusSize(String subcorpus) throws IOException,
			UnexpectedAnswerException, CqiServerError;

	/**
	 * Checks wether a subcorpus has a field.
	 * 
	 * @param subcorpus
	 *            the subcorpus
	 * @param field
	 *            the field
	 * 
	 * @return true, if the subcorpus has the field
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	@Override
	public abstract boolean subCorpusHasField(String subcorpus, byte field) throws IOException, UnexpectedAnswerException, CqiServerError;

	/**
	 * Dumps the values of "field" for match ranges "first" .. "last" in
	 * "subcorpus". "field" is one of the CQI_CONST_FIELD_* constants.
	 * 
	 * @param subcorpus
	 *            the subcorpus
	 * @param field
	 *            the field
	 * @param first
	 *            the first
	 * @param last
	 *            the last
	 * 
	 * @return the values
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	@Override
	public abstract int[] dumpSubCorpus(String subcorpus, byte field, int first, int last) throws IOException, UnexpectedAnswerException,
			CqiServerError;

	/**
	 * Drops a subcorpus.
	 * 
	 * @param subcorpus
	 *            the subcorpus
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	@Override
	public abstract void dropSubCorpus(String subcorpus) throws IOException,
			UnexpectedAnswerException, CqiServerError;

	/**
	 * Returns {@code <n>} (id, frequency) pairs flattened into a list of size 2*{@code <n>} NB:
	 * pairs are sorted by frequency desc.
	 * 
	 * @param subcorpus
	 *            the subcorpus
	 * @param cutoff
	 *            the cutoff
	 * @param field
	 *            the field : one of CQI_CONST_FIELD_MATCH,
	 *            CQI_CONST_FIELD_TARGET, CQI_CONST_FIELD_KEYWORD
	 * @param attribute
	 *            the attribute
	 * 
	 * @return the int[][]
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	@Override
	public abstract int[][] fdist1(String subcorpus, int cutoff,
			byte field, String attribute) throws IOException,
			UnexpectedAnswerException, CqiServerError;

	/**
	 * Returns {@code <n>} (id1, id2, frequency) pairs flattened into a list of size
	 * 3*{@code <n>} NB: triples are sorted by frequency desc. .
	 * 
	 * @param subcorpus
	 *            the subcorpus
	 * @param cutoff
	 *            the cutoff
	 * @param field1
	 *            the field1
	 * @param attribute1
	 *            the attribute1
	 * @param field2
	 *            the field2
	 * @param attribute2
	 *            the attribute2
	 * 
	 * @return the int[]
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	@Override
	public abstract int[][] fdist2(String subcorpus, int cutoff,
			byte field1, String attribute1, byte field2, String attribute2) throws IOException, UnexpectedAnswerException, CqiServerError;

	@Override
	public abstract boolean reconnect();

	public String setMatchingStrategyNoCheck(String newMatchingStrategy) {
		try {
			return CQPSearchEngine.getCqiClient().setOption("MatchingStrategy", newMatchingStrategy); //$NON-NLS-1$
		}
		catch (Exception e) {
			try {
				CQPSearchEngine.getCqiClient().query("set MatchingStrategy " + newMatchingStrategy + ";"); //$NON-NLS-1$
			}
			catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return null;
		}
	}

	public void setMatchingStrategy(String newMatchingStrategy) {

		String currentMatchingStrategy = null;
		try {
			currentMatchingStrategy = CQPSearchEngine.getCqiClient().getOption("MatchingStrategy"); //$NON-NLS-1$
		}
		catch (Exception e2) {
			e2.printStackTrace();
		}

		if (!newMatchingStrategy.equals(currentMatchingStrategy)) { // set only if the MatchingStrategy changed

			Log.info("Setting MatchingStrategy to: " + newMatchingStrategy);
			String returnedValue = setMatchingStrategyNoCheck(newMatchingStrategy);
			if (returnedValue != null && !returnedValue.equals(newMatchingStrategy)) {
				Log.warning("The MatchingStrategy was not changed to " + newMatchingStrategy + " and is still " + returnedValue);
			}
		}

	}
}
