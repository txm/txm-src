// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-09-04 11:17:02 +0200 (mer., 04 sept. 2013) $
// $LastChangedRevision: 2526 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp;

import java.util.LinkedHashMap;
import java.util.List;

import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.query.Match;

// TODO: Auto-generated Javadoc
/**
 * The Class Reference:
 * defines a pattern of word and/or structure properties
 *
 * @author mdecorde
 */
public class Reference {

	/** The values. */
	LinkedHashMap<Property, String> values;

	/** The reference pattern. */
	private ReferencePattern referencePattern = new ReferencePattern();

	/**
	 * Instantiates a new reference.
	 *
	 * @param values the values
	 * @param referencePattern the reference pattern
	 */
	public Reference(LinkedHashMap<Property, String> values, ReferencePattern referencePattern) {
		this.values = values;
		this.referencePattern = referencePattern;
	}

	/**
	 * Instantiates a new reference.
	 *
	 * @param match the match
	 * @param referencePattern the reference pattern
	 * @throws CqiClientException the cqi client exception
	 */
	public Reference(Match match, ReferencePattern referencePattern)
			throws CqiClientException {
		this.referencePattern = referencePattern;
		this.values = new LinkedHashMap<>();
		for (Property property : referencePattern) {
			values.put(property, match.getValueForProperty(property));
		}
	}

	/**
	 * Gets the reference pattern.
	 *
	 * @return the reference pattern
	 */
	public ReferencePattern getReferencePattern() {
		return referencePattern;
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public List<Property> getProperties() {
		return referencePattern.getProperties();
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public String format() {
		if (referencePattern.getPattern() != null && referencePattern.getPattern().length() > 0) {
			return referencePattern.format(values.values());
		}
		else {
			return toString();
		}
	}

	/**
	 * Gets the value.
	 *
	 * @param property the property
	 * @return the value
	 */
	public String getValue(Property property) {
		return values.get(property);
	}

	/**
	 * Checks if is oK.
	 *
	 * @return true, if is oK
	 */
	public boolean isOK() {
		return (referencePattern != null && !values.isEmpty());
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (values.isEmpty())
			return ""; //$NON-NLS-1$
		// Iterator<Entry<Property, String>> iter = values.entrySet().iterator();
		// Entry<Property, String> entry = iter.next();
		StringBuffer buffer = new StringBuffer(""); //$NON-NLS-1$
		/*
		 * if (StructuralUnitProperty.class.isInstance(entry.getKey())) {
		 * StructuralUnitProperty sProperty = (StructuralUnitProperty) entry.getKey();
		 * //buffer.append(sProperty.getStructuralUnit() + "(" + entry.getKey().toString() + "):" + entry.getValue()); //$NON-NLS-1$ //$NON-NLS-2$
		 * buffer.append(entry.getValue()); //$NON-NLS-1$ //$NON-NLS-2$
		 * } else {
		 * //buffer.append(entry.getKey().toString() + ":" + entry.getValue()); //$NON-NLS-1$
		 * buffer.append(entry.getValue()); //$NON-NLS-1$
		 * }
		 * while (iter.hasNext()) {
		 * entry = iter.next();
		 * if (StructuralUnitProperty.class.isInstance(entry.getKey())) {
		 * StructuralUnitProperty sProperty = (StructuralUnitProperty) entry.getKey();
		 * //buffer.append(", " + sProperty.getStructuralUnit() + "(" + entry.getKey().toString() + "):" + entry.getValue()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		 * buffer.append(", " +entry.getValue()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		 * } else {
		 * //buffer.append(", " + entry.getKey().toString() + ":" + entry.getValue()); //$NON-NLS-1$ //$NON-NLS-2$
		 * buffer.append(", " + entry.getValue()); //$NON-NLS-1$ //$NON-NLS-2$
		 * }
		 * }
		 */
		for (Property p : this.getProperties()) {
			buffer.append(values.get(p) + ", "); //$NON-NLS-1$
		}
		if (buffer.length() > 0)
			return buffer.substring(0, buffer.length() - 2);
		else
			return ""; //$NON-NLS-1$
	}
}
