package org.txm.searchengine.cqp;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.txm.Toolbox;
import org.txm.core.engines.EngineType;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;
import org.txm.libs.cqp.CQPLibPreferences;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Match;
import org.txm.searchengine.core.EmptySelection;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.SearchEngine;
import org.txm.searchengine.core.SearchEngineProperty;
import org.txm.searchengine.core.Selection;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.ServerNotFoundException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.OSDetector;
import org.txm.utils.Sh;
import org.txm.utils.logger.Log;

public class CQPSearchEngine extends SearchEngine {

	public static final String NAME = "CQP"; //$NON-NLS-1$

	/**
	 * initialization state
	 */
	boolean state = false;

	boolean useNetCQi = false;

	@Override
	public String getName() {
		return CQPSearchEngine.NAME;
	}

	/**
	 * Starts the corpus engine if not remove then launch cqpserver.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {

		boolean cqiserverok = false;

		if (monitor != null) {
			monitor.subTask("Starting CQP Engine...");
		}

		CorpusManager.cleanCorpora();// to be sure to call cqpserver

		useNetCQi = CQPLibPreferences.getInstance().getBoolean(CQPLibPreferences.CQI_NETWORK_MODE);

		Log.fine("Killing CQP processes if any..."); //$NON-NLS-1$
		killSearchEngine();

		boolean remote = CQPLibPreferences.getInstance().getBoolean(CQPLibPreferences.CQI_SERVER_IS_REMOTE);
		state = false;
		// test if must-have properties for CWB are set

		String init_path = CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PATH_TO_INIT_FILE);
		if (init_path.length() > 0 && !new File(init_path).exists()) {
			Log.warning(TXMCoreMessages.bind("Warning: the given CQP init file path can not be found in {0}.", init_path));
			init_path = null;
		}

		if (!remote) {

			if (!useNetCQi) {
				try {
					cqiServer = new MemCqiServer("",  //$NON-NLS-1$
							init_path,
							CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_ADDITIONAL_OPTIONS)
									+ " -P " + CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PORT), //$NON-NLS-1$
							Boolean.parseBoolean(CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_MONITOR_OUTPUT)));
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					org.txm.utils.logger.Log.printStackTrace(e);
				}
			}
			else {

				String exec_path = CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PATH_TO_EXECUTABLE);

				if (exec_path == null || exec_path.length() == 0 || new File(exec_path).exists()) {
					Log.severe(TXMCoreMessages.bind("Error: the '{0}' preference is not set.", CQPLibPreferences.CQI_SERVER_PATH_TO_EXECUTABLE));
				}
				// if (new File(exec_path).exists()) {
				// System.out.println("Error: the '"+CQPLibPreferences.CQI_SERVER_PATH_TO_EXECUTABLE+"' preference is not set.");
				// }

				cqiServer = new NetCqiServer(exec_path,
						"",  //$NON-NLS-1$
						init_path,
						CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_ADDITIONAL_OPTIONS)
								+ " -P " + CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PORT), //$NON-NLS-1$
						CQPLibPreferences.getInstance().getBoolean(CQPLibPreferences.CQI_SERVER_MONITOR_OUTPUT));
			}

			if (cqiServer != null) {
				cqiserverok = cqiServer.start();
			}

		}

		// create cqi client
		try {
			if (cqiserverok) {
				if (!useNetCQi) {
					try {
						cqiClient = new MemCqiClient((MemCqiServer) cqiServer);
					}
					catch (Exception e) {
						Log.severe(TXMCoreMessages.failedToStartMemCqiClientColon);
						Log.printStackTrace(e);
						cqiserverok = false;
					}
				}
				else {
					int port = CQPLibPreferences.getInstance().getInt(CQPLibPreferences.CQI_SERVER_PORT);
					String host = CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_HOST);
					cqiClient = new NetCqiClient(host, port);
				}
			}
		}
		catch (NumberFormatException e1) {
			Log.severe(
					TXMCoreMessages.bind(TXMCoreMessages.errorColonTheConnexionToTheServerFailedColonWrongPortFormatP0, CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PORT)));
		}
		catch (ServerNotFoundException e1) {
			Log.severe(TXMCoreMessages.bind(TXMCoreMessages.errorColonWhileConnectionToSearchEngineWithTheFollowingParametersDblColonP0P1, CQPLibPreferences.getInstance().getString(
					CQPLibPreferences.CQI_SERVER_HOST),
					CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PORT)));
		}

		// try connecting to the CWB server
		try {
			if (cqiserverok) {
				String login = CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_LOGIN);
				String password = CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PASSWORD);
				state = cqiClient.connect(login, password);
			}
		}
		catch (Exception e1) {
			Log.severe(Log.toString(e1));
		}

		if (state == false) {
			Log.severe(TXMCoreMessages.errorWhileConnectingToSearchEngine);
			if (cqiClient instanceof NetCqiClient) {
				Log.severe(TXMCoreMessages.bind(TXMCoreMessages.wrongLoginOrPasswordOrPortColonP0P1P203, CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_LOGIN),
						CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PASSWORD), CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_HOST),
						CQPLibPreferences.getInstance().getInt(CQPLibPreferences.CQI_SERVER_PORT)));
			}
			if (cqiServer != null && cqiServer instanceof NetCqiServer) {
				Log.severe(TXMCoreMessages.bind(TXMCoreMessages.orTheEngineWasNotStartedWithTheCommandLineColonP0, cqiServer.getLastCmdLine()));
			}
		}
		else {
			if (cqiServer == null || cqiServer instanceof NetCqiServer)
				Log.fine(TXMCoreMessages.connectedToCQP);
		}

		CorpusManager.setCqiClient(cqiClient);

		// wait for CQPSERVER TO BE READY
		int scount = 0;
		try {
			CorpusManager.getCorpusManager().getCorpora();
			do {
				Thread.sleep(100);
				CorpusManager.cleanCorpora();// to be sure to call cqpserver
				CorpusManager.getCorpusManager().getCorpora();
				scount++;
				if (scount >= 10) {
					Log.fine(TXMCoreMessages.endOfCQPWaitTest);
					break;
				}
			}
			while (CorpusManager.getCorpusManager().getCorpora().size() == 0);
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			Log.severe(TXMCoreMessages.bind(TXMCoreMessages.errorWhileWaitingForCQPP0, e));
		}

		// Initialize default query options
		HashMap<String, List<String>> possibleOptions = this.getAvailableQueryOptions();
		for (String option : possibleOptions.keySet()) {
			String value = CQPLibPreferences.getInstance().getString(option);
			if (possibleOptions.get(option).contains(value)) {
				//System.out.println("SET OPTION: "+option+"="+value);
				cqiClient.setOption(option, value);
			}
		}

		// FIXME: SJ: this test should be moved at start of this method? before running CQP?
		if (Toolbox.workspace == null) {
			Log.severe("Can't start CQP with workspace not ready.");
			return false;
		}

		//// initialize pre-configured subcorpus and partition of corpus registered in the default.xml workspace file
		// System.out.print(TXMCoreMessages.Toolbox_workspace_init);
		// for (Project b : Toolbox.workspace.getProjects()) {
		//
		// Log.info("Loading CQP corpora of " + b.getName() + " corpus...");
		//
		// if (!loadCQPCorpora(b, monitor)) {
		// Log.severe("Failed to load CQP corpora of " + b.getName() + " corpus.");
		// }
		// }

		Log.fine(TXMCoreMessages.common_done);

		return state;
	}

	/**
	 * 
	 * @return all option names and possible values
	 */
	@Override
	public HashMap<String, List<String>> getAvailableQueryOptions() {
		HashMap<String, List<String>> options = new HashMap<String, List<String>>();
		if (!OSDetector.isFamilyWindows()) options.put(CQPLibPreferences.MATCHINGSTRATEGY, Arrays.asList("shortest", "standard", "longest", "traditional")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
//		options.put(CQPLibPreferences.DEFAULTNONBRACKATTR, Arrays.asList());
//		options.put(CQPLibPreferences.STRICTREGIONS, Arrays.asList("yes", "no"));
		return options;
	}

	// public boolean loadCQPCorpora(Project base, IProgressMonitor monitor) {
	// //System.out.println("Load corpus from params: "+params.corpora);
	// for (String corpusname : base.getProjectParameters().corpora.keySet()) {
	// Element c = base.getProjectParameters().corpora.get(corpusname);
	// try {
	// MainCorpus corp = CorpusManager.getCorpusManager().getCorpus(corpusname);
	// if (monitor != null) {
	// monitor.subTask("Loading " + corp + " subcorpora and partitions of corpus " + corpusname + "...");
	// }
	// corp.setSelfElement(c);
	// corp.setParent(base);
	// corp.load(); // don't load corpus right now, wait for the search engine to be ready
	// base.addChild(corp);
	// if (!corp.getMetadatas().containsKey("lang")) { //$NON-NLS-1$
	// corp.setAttribute("lang", c.getAttribute("lang")); //$NON-NLS-1$ //$NON-NLS-2$
	// }
	// } catch (Exception e) {
	// System.out.println(e.getLocalizedMessage());
	// org.txm.utils.logger.Log.printStackTrace(e);
	// return false;
	// }
	// }
	// return true;
	// }

	@Override
	public boolean initialize() {
		// all initialization done in the start method because we need CQP executable/lib to restart from the beginning
		return true;
	}

	/** The CQI server. */
	private static AbstractCqiServer cqiServer;

	/** The CQI client. */
	private static AbstractCqiClient cqiClient;

	/**
	 * Sets the CQI server.
	 *
	 * @param cqiServer the new cqi server
	 */
	public static void setCqiServer(AbstractCqiServer cqiServer) {
		CQPSearchEngine.cqiServer = cqiServer;
	}

	/**
	 * Gets the cqi client.
	 *
	 * @return the cqi client
	 */
	public static AbstractCqiClient getCqiClient() {
		return cqiClient;
	}

	/**
	 * Sets the cqi client.
	 *
	 * @param cqiClient the new cqi client
	 */
	public static void setCqiClient(NetCqiClient cqiClient) {
		CQPSearchEngine.cqiClient = cqiClient;
	}

	/**
	 * Gets the CQI server.
	 *
	 * @return the CQI server
	 */
	public static AbstractCqiServer getCqiServer() {
		return cqiServer;
	}

	/**
	 * Shuts the toolbox searchengine down.
	 */
	@Override
	public boolean stop() {
		try {
			if (cqiClient != null) {

				if (cqiClient instanceof MemCqiClient) {
					for (String name : ((MemCqiClient) cqiClient).listCorpora()) {
						((MemCqiClient) cqiClient).dropCorpus(name);
					}
				}
				cqiClient.disconnect();
			}

			if (cqiServer != null && cqiServer.isRunning()) {
				cqiServer.stop();

				CorpusManager.clean();
				if (cqiServer instanceof NetCqiServer) {
					killSearchEngine();
				}

				cqiClient = null;
				cqiServer = null;
			}
			System.gc();

			state = false;
		}
		catch (Exception e) {
			Log.severe("Error while closing CQP: " + e.getLocalizedMessage());
			return false;
		}
		return !state;
	}

	/**
	 * kill cqpserver process.
	 */
	private static void killSearchEngine() {
		if (!CQPLibPreferences.getInstance().getBoolean(CQPLibPreferences.CQI_SERVER_IS_REMOTE) && cqiServer instanceof MemCqiServer) {
			// Windows OS
			if (OSDetector.isFamilyWindows()) {
				try {
					Process p = Runtime.getRuntime().exec("taskkill /IM cqpserver.exe /F"); //$NON-NLS-1$
					p.waitFor();
					p = Runtime.getRuntime().exec("taskkill /IM cqpserver.exe /F"); //$NON-NLS-1$
					p.waitFor();
				}
				catch (IOException e) {
					Log.severe(TXMCoreMessages.bind(TXMCoreMessages.failedToExecTaskkillIMCqpserverexeFColonP0, Log.toString(e)));
					System.out.println(TXMCoreMessages.bind(TXMCoreMessages.failedToExecTaskkillIMCqpserverexeFColonP0, Log.toString(e)));
					try {
						Process p = Runtime.getRuntime().exec("tskill cqpserver.exe"); //$NON-NLS-1$
						p.waitFor();
						p = Runtime.getRuntime().exec("tskill cqpserver.exe"); //$NON-NLS-1$
						p.waitFor();
					}
					catch (IOException e2) {
						e2.printStackTrace();
						Log.severe(TXMCoreMessages.bind(TXMCoreMessages.failedToExecTskillCqpserverexeColonP0, Log.toString(e2)));
						System.out.println(TXMCoreMessages.bind(TXMCoreMessages.failedToExecTskillCqpserverexeColonP0, Log.toString(e2)));
					}
					catch (InterruptedException e3) {
						e3.printStackTrace();
						Log.severe(Log.toString(e3));
						System.out.println(Log.toString(e3));
					}
				}
				catch (InterruptedException e) {
					Log.severe(TXMCoreMessages.bind("Error while closing CQP: {0}.", e.getLocalizedMessage()));
					e.printStackTrace();
					System.out.println(Log.toString(e));
				}
			}
			else { // Mac, Linux
				String cmd = "kill `ps aux | grep cqpserver | awk '/-P " + CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PORT) + "/ {print $2}'`"; // //$NON-NLS-1$ //$NON-NLS-2$
				try {
					// Process p = Runtime.getRuntime().exec("killall -9 cqpserver"); //$NON-NLS-1$
					// System.out.println("kill `ps aux | grep cqpserver | awk '/-P "
					// + CQPPreferences.getInstance().getString(CQI_SERVER_PORT) + "/ {print $2}'`");
					// String[] args =
					// {"kill","`ps aux | grep cqpserver | awk '/-P " +
					// CQPPreferences.getInstance().getString(CQI_SERVER_PORT) + "/ {print $2}'`"};
					// System.out.println("before Sh constructor");
					Sh sh = new Sh(""); //$NON-NLS-1$
					// System.out.println("after Sh constructor");
					// System.out.println("EXECUTE SH : "+"kill `ps aux | grep cqpserver | awk '/-P "
					// + CQPPreferences.getInstance().getString(CQI_SERVER_PORT) + "/ {print $2}'`");
					sh.setIsc(cmd);
					sh.sh();
					// Runtime.getRuntime().exec("sh kill `ps aux | grep cqpserver | awk '/-P "
					// + CQPPreferences.getInstance().getString(CQI_SERVER_PORT) + "/ {print $2}'`");

				}
				catch (Exception e) {
					Log.severe(TXMCoreMessages.bind(TXMCoreMessages.failedToExecP0P1, cmd, Log.toString(e)));
					System.out.println(TXMCoreMessages.bind(TXMCoreMessages.failedToExecP0P1, cmd, Log.toString(e)));
					org.txm.utils.logger.Log.printStackTrace(e);
				}
			}
		}
	}

	/**
	 * Restart the toolbox's searchengine.
	 *
	 * @return true, if successful
	 */
	public boolean restart() {
		try {
			boolean ret = stop();
			Thread.sleep(10);
			ret = ret && start(null);
			return ret;
		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.bind("Error while restarting CQP: {0}.", e.getLocalizedMessage()));
		}
		return false;
	}

	/**
	 * test if searchEngine is initialized.
	 *
	 * @return true, if is initialized
	 */
	public static boolean isInitialized() {
		return Toolbox.getEngineManager(EngineType.SEARCH).getEngine(CQPSearchEngine.NAME).isRunning();
	}

	@Override
	public boolean isRunning() {
		return state;
	}

	@Override
	public Selection query(CorpusBuild corpus, IQuery query, String name, boolean saveQuery) throws CqiClientException {
		if (query instanceof CQLQuery && corpus instanceof CQPCorpus) {
			return ((CQPCorpus) corpus).query((CQLQuery) query, name, saveQuery);
		}
		else {
			return new EmptySelection(query);
		}
	}

	@Override
	public Query newQuery() {
		return new CQLQuery(""); //$NON-NLS-1$
	}

	@Override
	public boolean hasIndexes(CorpusBuild corpus) {
		if (corpus instanceof CQPCorpus) return true;

		try {
			String id = corpus.getID();

			return CorpusManager.getCorpusManager().getCorpus(id) != null;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void notify(TXMResult r, String state) {
		// TODO Auto-generated method stub
	}

	public static CQPSearchEngine getEngine() {
		return (CQPSearchEngine) Toolbox.getEngineManager(EngineType.SEARCH).getEngine(CQPSearchEngine.NAME);
	}

	@Override
	public String getValueForProperty(Match match, SearchEngineProperty property) {
		if (!(property instanceof Property)) return null;
		Property p = (Property) property;
		try {
			return org.txm.searchengine.cqp.corpus.query.Match.getValueForProperty(p, match.getStart());
		}
		catch (CqiClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<String> getValuesForProperty(Match match, SearchEngineProperty property) {
		if (!(property instanceof Property)) return null;

		Property p = (Property) property;
		try {
			return org.txm.searchengine.cqp.corpus.query.Match.getValuesForProperty(p, match);
		}
		catch (CqiClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public List<String> getValuesForProperty(int[] positions, WordProperty property) throws CqiClientException {
		return org.txm.searchengine.cqp.corpus.query.Match.getValuesForProperty(property, positions);
	}
}
