// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2012-03-01 08:14:11 +0100 (jeu., 01 mars 2012) $
// $LastChangedRevision: 2134 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp;

import java.io.IOException;

import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.serverException.CqiCqpErrorNoSuchCorpus;
import org.txm.searchengine.cqp.serverException.CqiServerError;

/**
 * This class implements a Java Interface CQi client.
 *
 * @author mdecorde
 */
public interface ICqiClient {

	/** The Constant CQI_PADDING. */
	final static public byte[] CQI_PADDING = { (byte) 0x00 };

	/** The Constant CQI_STATUS_OK. */
	final static public byte[] CQI_STATUS_OK = { (byte) 0x01, (byte) 0x01 };

	/** The Constant CQI_STATUS_CONNECT_OK. */
	final static public byte[] CQI_STATUS_CONNECT_OK = { (byte) 0x01, (byte) 0x02 };

	/** The Constant CQI_STATUS_BYE_OK. */
	final static public byte[] CQI_STATUS_BYE_OK = { (byte) 0x01, (byte) 0x03 };

	/** The Constant CQI_STATUS_PING_OK. */
	final static public byte[] CQI_STATUS_PING_OK = { (byte) 0x01, (byte) 0x04 };

	/** The Constant CQI_ERROR_GENERAL_ERROR. */
	final static public byte[] CQI_ERROR_GENERAL_ERROR = { (byte) 0x02, (byte) 0x01 };

	/** The Constant CQI_ERROR_CONNECT_REFUSED. */
	final static public byte[] CQI_ERROR_CONNECT_REFUSED = { (byte) 0x02, (byte) 0x02 };

	/** The Constant CQI_ERROR_USER_ABORT. */
	final static public byte[] CQI_ERROR_USER_ABORT = { (byte) 0x02, (byte) 0x03 };

	/** The Constant CQI_ERROR_SYNTAX_ERROR. */
	final static public byte[] CQI_ERROR_SYNTAX_ERROR = { (byte) 0x02, (byte) 0x04 };

	/** The Constant CQI_DATA_BYTE. */
	final static public byte[] CQI_DATA_BYTE = { (byte) 0x03, (byte) 0x01 };

	/** The Constant CQI_DATA_BOOL. */
	final static public byte[] CQI_DATA_BOOL = { (byte) 0x03, (byte) 0x02 };

	/** The Constant CQI_DATA_INT. */
	final static public byte[] CQI_DATA_INT = { (byte) 0x03, (byte) 0x03 };

	/** The Constant CQI_DATA_STRING. */
	final static public byte[] CQI_DATA_STRING = { (byte) 0x03, (byte) 0x04 };

	/** The Constant CQI_DATA_BYTE_LIST. */
	final static public byte[] CQI_DATA_BYTE_LIST = { (byte) 0x03, (byte) 0x05 };

	/** The Constant CQI_DATA_BOOL_LIST. */
	final static public byte[] CQI_DATA_BOOL_LIST = { (byte) 0x03, (byte) 0x06 };

	/** The Constant CQI_DATA_INT_LIST. */
	final static public byte[] CQI_DATA_INT_LIST = { (byte) 0x03, (byte) 0x07 };

	/** The Constant CQI_DATA_STRING_LIST. */
	final static public byte[] CQI_DATA_STRING_LIST = { (byte) 0x03, (byte) 0x08 };

	/** The Constant CQI_DATA_INT_INT. */
	final static public byte[] CQI_DATA_INT_INT = { (byte) 0x03, (byte) 0x09 };

	/** The Constant CQI_DATA_INT_INT_INT_INT. */
	final static public byte[] CQI_DATA_INT_INT_INT_INT = { (byte) 0x03, (byte) 0x0A };

	/** The Constant CQI_DATA_INT_TABLE. */
	final static public byte[] CQI_DATA_INT_TABLE = { (byte) 0x03, (byte) 0x0B };

	/** The Constant CQI_CL_ERROR_NO_SUCH_ATTRIBUTE. */
	final static public byte[] CQI_CL_ERROR_NO_SUCH_ATTRIBUTE = { (byte) 0x04,
			(byte) 0x01 };

	/** The Constant CQI_CL_ERROR_WRONG_ATTRIBUTE_TYPE. */
	final static public byte[] CQI_CL_ERROR_WRONG_ATTRIBUTE_TYPE = { (byte) 0x04,
			(byte) 0x02 };

	/** The Constant CQI_CL_ERROR_OUT_OF_RANGE. */
	final static public byte[] CQI_CL_ERROR_OUT_OF_RANGE = { (byte) 0x04, (byte) 0x03 };

	/** The Constant CQI_CL_ERROR_REGEX. */
	final static public byte[] CQI_CL_ERROR_REGEX = { (byte) 0x04, (byte) 0x04 };

	/** The Constant CQI_CL_ERROR_CORPUS_ACCESS. */
	final static public byte[] CQI_CL_ERROR_CORPUS_ACCESS = { (byte) 0x04, (byte) 0x05 };

	/** The Constant CQI_CL_ERROR_OUT_OF_MEMORY. */
	final static public byte[] CQI_CL_ERROR_OUT_OF_MEMORY = { (byte) 0x04, (byte) 0x06 };

	/** The Constant CQI_CL_ERROR_INTERNAL. */
	final static public byte[] CQI_CL_ERROR_INTERNAL = { (byte) 0x04, (byte) 0x07 };

	/** The Constant CQI_CQP_ERROR_GENERAL. */
	final static public byte[] CQI_CQP_ERROR_GENERAL = { (byte) 0x05, (byte) 0x01 };

	/** The Constant CQI_CQP_ERROR_NO_SUCH_CORPUS. */
	final static public byte[] CQI_CQP_ERROR_NO_SUCH_CORPUS = { (byte) 0x05,
			(byte) 0x02 };

	/** The Constant CQI_CQP_ERROR_INVALID_FIELD. */
	final static public byte[] CQI_CQP_ERROR_INVALID_FIELD = { (byte) 0x05,
			(byte) 0x03 };

	/** The Constant CQI_CQP_ERROR_OUT_OF_RANGE. */
	final static public byte[] CQI_CQP_ERROR_OUT_OF_RANGE = { (byte) 0x05, (byte) 0x04 };

	/** The Constant CQI_CQP_ERROR_OUT_OF_RANGE. */
	final static public byte[] CQI_CQP_ERROR_SYNTAX = { (byte) 0x05, (byte) 0x05 };

	/** The Constant CQI_CTRL_CONNECT. */
	final static public byte[] CQI_CTRL_CONNECT = { (byte) 0x11, (byte) 0x01 };

	/** The Constant CQI_CTRL_BYE. */
	final static public byte[] CQI_CTRL_BYE = { (byte) 0x11, (byte) 0x02 };

	/** The Constant CQI_CTRL_USER_ABORT. */
	final static public byte[] CQI_CTRL_USER_ABORT = { (byte) 0x11, (byte) 0x03 };

	/** The Constant CQI_CTRL_PING. */
	final static public byte[] CQI_CTRL_PING = { (byte) 0x11, (byte) 0x04 };

	/** The Constant CQI_CTRL_LAST_GENERAL_ERROR. */
	final static public byte[] CQI_CTRL_LAST_GENERAL_ERROR = { (byte) 0x11,
			(byte) 0x05 };

	/** The Constant CQI_CTRL_LAST_CQP_ERROR. */
	final static public byte[] CQI_CTRL_LAST_CQP_ERROR = { (byte) 0x11,
			(byte) 0x06 };

	/** The Constant CQI_CORPUS_LIST_CORPORA. */
	final static public byte[] CQI_CORPUS_LIST_CORPORA = { (byte) 0x13, (byte) 0x01 };

	/** The Constant CQI_CORPUS_CHARSET. */
	final static public byte[] CQI_CORPUS_CHARSET = { (byte) 0x13, (byte) 0x03 };

	/** The Constant CQI_CORPUS_PROPERTIES. */
	final static public byte[] CQI_CORPUS_PROPERTIES = { (byte) 0x13, (byte) 0x04 };

	/** The Constant CQI_CORPUS_POSITIONAL_ATTRIBUTES. */
	final static public byte[] CQI_CORPUS_POSITIONAL_ATTRIBUTES = { (byte) 0x13,
			(byte) 0x05 };

	/** The Constant CQI_CORPUS_STRUCTURAL_ATTRIBUTES. */
	final static public byte[] CQI_CORPUS_STRUCTURAL_ATTRIBUTES = { (byte) 0x13,
			(byte) 0x06 };

	/** The Constant CQI_CORPUS_STRUCTURAL_ATTRIBUTE_HAS_VALUES. */
	final static public byte[] CQI_CORPUS_STRUCTURAL_ATTRIBUTE_HAS_VALUES = {
			(byte) 0x13, (byte) 0x07 };

	/** The Constant CQI_CORPUS_ALIGNMENT_ATTRIBUTES. */
	final static public byte[] CQI_CORPUS_ALIGNMENT_ATTRIBUTES = { (byte) 0x13,
			(byte) 0x08 };

	/** The Constant CQI_CORPUS_FULL_NAME. */
	final static public byte[] CQI_CORPUS_FULL_NAME = { (byte) 0x13, (byte) 0x09 };

	/** The Constant CQI_CORPUS_INFO. */
	final static public byte[] CQI_CORPUS_INFO = { (byte) 0x13, (byte) 0x0A };

	/** The Constant CQI_CORPUS_DROP_CORPUS. */
	final static public byte[] CQI_CORPUS_DROP_CORPUS = { (byte) 0x13, (byte) 0x0B };

	/** The Constant CQI_CL_ATTRIBUTE_SIZE. */
	final static public byte[] CQI_CL_ATTRIBUTE_SIZE = { (byte) 0x14, (byte) 0x01 };

	/** The Constant CQI_CL_LEXICON_SIZE. */
	final static public byte[] CQI_CL_LEXICON_SIZE = { (byte) 0x14, (byte) 0x02 };

	/** The Constant CQI_CL_DROP_ATTRIBUTE. */
	final static public byte[] CQI_CL_DROP_ATTRIBUTE = { (byte) 0x14, (byte) 0x03 };

	/** The Constant CQI_CL_STR2ID. */
	final static public byte[] CQI_CL_STR2ID = { (byte) 0x14, (byte) 0x04 };

	/** The Constant CQI_CL_ID2STR. */
	final static public byte[] CQI_CL_ID2STR = { (byte) 0x14, (byte) 0x05 };

	/** The Constant CQI_CL_ID2FREQ. */
	final static public byte[] CQI_CL_ID2FREQ = { (byte) 0x14, (byte) 0x06 };

	/** The Constant CQI_CL_CPOS2ID. */
	final static public byte[] CQI_CL_CPOS2ID = { (byte) 0x14, (byte) 0x07 };

	/** The Constant CQI_CL_CPOS2STR. */
	final static public byte[] CQI_CL_CPOS2STR = { (byte) 0x14, (byte) 0x08 };

	/** The Constant CQI_CL_CPOS2STRUC. */
	final static public byte[] CQI_CL_CPOS2STRUC = { (byte) 0x14, (byte) 0x09 };

	/** The Constant CQI_CL_CPOS2LBOUND. */
	final static public byte[] CQI_CL_CPOS2LBOUND = { (byte) 0x14, (byte) 0x20 };

	/** The Constant CQI_CL_CPOS2RBOUND. */
	final static public byte[] CQI_CL_CPOS2RBOUND = { (byte) 0x14, (byte) 0x21 };

	/** The Constant CQI_CL_CPOS2ALG. */
	final static public byte[] CQI_CL_CPOS2ALG = { (byte) 0x14, (byte) 0x0A };

	/** The Constant CQI_CL_STRUC2STR. */
	final static public byte[] CQI_CL_STRUC2STR = { (byte) 0x14, (byte) 0x0B };

	/** The Constant CQI_CL_ID2CPOS. */
	final static public byte[] CQI_CL_ID2CPOS = { (byte) 0x14, (byte) 0x0C };

	/** The Constant CQI_CL_IDLIST2CPOS. */
	final static public byte[] CQI_CL_IDLIST2CPOS = { (byte) 0x14, (byte) 0x0D };

	/** The Constant CQI_CL_REGEX2ID. */
	final static public byte[] CQI_CL_REGEX2ID = { (byte) 0x14, (byte) 0x0E };

	/** The Constant CQI_CL_STRUC2CPOS. */
	final static public byte[] CQI_CL_STRUC2CPOS = { (byte) 0x14, (byte) 0x0F };

	/** The Constant CQI_CL_ALG2CPOS. */
	final static public byte[] CQI_CL_ALG2CPOS = { (byte) 0x14, (byte) 0x10 };

	/** The Constant CQI_CQP_QUERY. */
	final static public byte[] CQI_CQP_QUERY = { (byte) 0x15, (byte) 0x01 };

	/** The Constant CQI_CQP_LIST_SUBCORPORA. */
	final static public byte[] CQI_CQP_LIST_SUBCORPORA = { (byte) 0x15, (byte) 0x02 };

	/** The Constant CQI_CQP_SUBCORPUS_SIZE. */
	final static public byte[] CQI_CQP_SUBCORPUS_SIZE = { (byte) 0x15, (byte) 0x03 };

	/** The Constant CQI_CQP_SUBCORPUS_HAS_FIELD. */
	final static public byte[] CQI_CQP_SUBCORPUS_HAS_FIELD = { (byte) 0x15,
			(byte) 0x04 };

	/** The Constant CQI_CQP_DUMP_SUBCORPUS. */
	final static public byte[] CQI_CQP_DUMP_SUBCORPUS = { (byte) 0x15, (byte) 0x05 };

	/** The Constant CQI_CQP_DROP_SUBCORPUS. */
	final static public byte[] CQI_CQP_DROP_SUBCORPUS = { (byte) 0x15, (byte) 0x09 };

	/** The Constant CQI_CQP_FDIST_1. */
	final static public byte[] CQI_CQP_FDIST_1 = { (byte) 0x15, (byte) 0x10 };

	/** The Constant CQI_CQP_FDIST_2. */
	final static public byte[] CQI_CQP_FDIST_2 = { (byte) 0x15, (byte) 0x11 };

	/** The Constant CQI_CQP_QUERY. */
	final static public byte[] CQI_CQP_QUERY_EVAL = { (byte) 0x15, (byte) 0x012 };

	/** The Constant CQI_CQP_LOAD_SYSTEM_CORPUS. */
	final static public byte[] CQI_CQP_LOAD_SYSTEM_CORPUS = { (byte) 0x15, (byte) 0x13 };

	/** The Constant CQI_CONST_FIELD_MATCH. */
	final public static byte CQI_CONST_FIELD_MATCH = (byte) 0x10;

	/** The Constant CQI_CONST_FIELD_MATCHEND. */
	final public static byte CQI_CONST_FIELD_MATCHEND = (byte) 0x11;

	/** The Constant CQI_CONST_FIELD_TARGET. */
	final public static byte CQI_CONST_FIELD_TARGET = (byte) 0x00;

	/** The Constant CQI_CONST_FIELD_KEYWORD. */
	final public static byte CQI_CONST_FIELD_KEYWORD = (byte) 0x09;

	final public static byte CQI_CONST_FIELD_TARGET_0 = (byte) 0x00;

	final public static byte CQI_CONST_FIELD_TARGET_1 = (byte) 0x01;

	final public static byte CQI_CONST_FIELD_TARGET_2 = (byte) 0x02;

	final public static byte CQI_CONST_FIELD_TARGET_3 = (byte) 0x03;

	final public static byte CQI_CONST_FIELD_TARGET_4 = (byte) 0x04;

	final public static byte CQI_CONST_FIELD_TARGET_5 = (byte) 0x05;

	final public static byte CQI_CONST_FIELD_TARGET_6 = (byte) 0x06;

	final public static byte CQI_CONST_FIELD_TARGET_7 = (byte) 0x07;

	final public static byte CQI_CONST_FIELD_TARGET_8 = (byte) 0x08;

	final public static byte CQI_CONST_FIELD_TARGET_9 = (byte) 0x09;

	// CQI_ERROR_CONNECT_REFUSED
	/**
	 * Connect the client to a server.
	 *
	 * @param username the username
	 * @param password the password
	 * @return true, if successful
	 */
	public boolean connect(String username, String password) throws UnexpectedAnswerException, IOException, CqiServerError;

	// None
	/**
	 * Disconnect.
	 * 
	 * @return true, if successful
	 */
	public boolean disconnect() throws UnexpectedAnswerException, CqiServerError, IOException;

	// CQI_CTRL_LAST_GENERAL_ERROR
	/**
	 * return the last CQP error.
	 *
	 * @return the last error
	 */
	public String getLastCqiError() throws UnexpectedAnswerException, IOException, CqiServerError;

	// CQI_CTRL_LAST_CQP_ERROR
	/**
	 * return the last CQP error.
	 *
	 * @return the last error
	 */
	public String getLastCQPError() throws UnexpectedAnswerException, IOException, CqiServerError;

	// None
	/**
	 * Lists the corpora available on the server.
	 *
	 * @return the name of the corpora
	 */
	public String[] listCorpora() throws UnexpectedAnswerException, IOException, CqiServerError;

	// None
	/**
	 * Gives the corpus charset.
	 *
	 * @param corpus the corpus
	 * @return the name of the charset
	 */
	public String corpusCharset(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Get the String value of a CQP environment option
	 *
	 * @param name the option full name
	 * @return the value
	 */
	public String getOption(String name) throws UnexpectedAnswerException, IOException, CqiServerError;

	// None
	/**
	 * set the String value of a CQP environment option
	 *
	 * @param name the option full name
	 * @param value the option value to set
	 * @return the value
	 */
	public String setOption(String name, String value) throws UnexpectedAnswerException, IOException, CqiServerError;

	// None (not really implemented anyway)
	/**
	 * Gives the corpus properties.
	 *
	 * @param corpus the corpus
	 * @return the properties
	 */
	public String[] corpusProperties(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError;

	// CQI_CQP_ERROR_NO_SUCH_CORPUS
	/**
	 * Gives the corpus positional attributes.
	 *
	 * @param corpusID the corpus id
	 * @return the name of the attributes
	 */
	public String[] corpusPositionalAttributes(String corpusID) throws UnexpectedAnswerException, IOException, CqiServerError;

	// CQI_CQP_ERROR_NO_SUCH_CORPUS
	/**
	 * Gives the corpus structural attributes.
	 *
	 * @param corpus the corpus
	 * @return the name of the attributes
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	public String[] corpusStructuralAttributes(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError;

	// CQI_CQP_ERROR_NO_SUCH_CORPUS, CQI_CL_ERROR_NO_SUCH_ATTRIBUTE,
	// CQI_CL_ERROR_WRONG_ATTRIBUTE_TYPE
	/**
	 * Check wether a structural attribute has values.
	 *
	 * @param attribute the attribute
	 * @return true, if it has values
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	public boolean corpusStructuralAttributeHasValues(String attribute) throws UnexpectedAnswerException, IOException,
			CqiServerError;

	// CQI_CQP_ERROR_NO_SUCH_CORPUS
	/**
	 * Gives the corpus alignement attributes.
	 *
	 * @param corpus the corpus
	 * @return the name of attributes
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	public String[] corpusAlignementAttributes(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError;

	// CQI_CQP_ERROR_NO_SUCH_CORPUS
	/**
	 * Gives the corpus full name.
	 *
	 * @param corpus the corpus
	 * @return the full name
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	public String corpusFullName(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Gives the corpus info listed in the .INFO file.
	 *
	 * @param corpus the corpus
	 * @return the info
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	public String[] corpusInfo(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Drop a corpus.
	 * 
	 * @param corpus
	 *            the corpus
	 */
	public void dropCorpus(String corpus) throws Exception;

	// CQI_CQP_ERROR_NO_SUCH_CORPUS, CQI_CL_ERROR_NO_SUCH_ATTRIBUTE,
	// CQI_CL_ERROR_WRONG_ATTRIBUTE_TYPE, CQI_CL_ERROR_CORPUS_ACCESS
	/**
	 * Gives an attribute size (the number of token).
	 *
	 * @param attribute the attribute
	 * @return the size
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	public int attributeSize(String attribute) throws IOException, UnexpectedAnswerException, CqiServerError;

	// CQI_CQP_ERROR_NO_SUCH_CORPUS, CQI_CL_ERROR_NO_SUCH_ATTRIBUTE,
	// CQI_CL_ERROR_WRONG_ATTRIBUTE_TYPE, CQI_CL_ERROR_CORPUS_ACCESS
	/**
	 * Gives the lexicon size of an attribute.
	 *
	 * @param attribute the attribute
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 * @return the number of entries in the lexicon of a positional attribute
	 */
	public int lexiconSize(String attribute) throws IOException, UnexpectedAnswerException, CqiServerError;

	/**
	 * Drop attribute.
	 *
	 * @param attribute the attribute
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	public void dropAttribute(String attribute) throws IOException, UnexpectedAnswerException, CqiServerError;

	/**
	 * Converts an array of attribute values to their ID.
	 *
	 * @param attribute the attribute
	 * @param strings the values
	 * @return the IDs
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	public int[] str2Id(String attribute, String[] strings) throws IOException, UnexpectedAnswerException, CqiServerError;

	/**
	 * Converts an array of attribute ID to their values.
	 *
	 * @param attribute the attribute
	 * @param ids the ids
	 * @return the values
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	public String[] id2Str(String attribute, int[] ids) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Converts an array of attribute IDs to their frequency.
	 *
	 * @param attribute the attribute
	 * @param ids the ids
	 * @return the frequencies
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	public int[] id2Freq(String attribute, int[] ids) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Converts an array of position to their ID given an attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the positions
	 * @return the IDs
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError Signals that the cqi server raised an error
	 */
	public int[] cpos2Id(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Converts an array of position to their value given an attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the cpos
	 * @return the values
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	public String[] cpos2Str(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Computes for each position of an array the Id of the enclosing structural
	 * attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the positions
	 * @return the IDs
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	public int[] cpos2Struc(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Computes for each position of an array the position of the left boundary
	 * of the enclosing structural attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the cpos
	 * @return the positions of the left boundaries
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	public int[] cpos2LBound(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Computes for each position of an array the position of the right boundary
	 * of the enclosing structural attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the cpos
	 * @return the positions of the right boundaries
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	public int[] cpos2RBound(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Computes for each position of an array the Id of the enclosing alignment
	 * attribute.
	 *
	 * @param attribute the attribute
	 * @param cpos the cpos
	 * @return the int[]
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	public int[] cpos2Alg(String attribute, int[] cpos) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Retrieves annotated string values of structure regions in "strucs"; "" if
	 * out of range.
	 *
	 * @param attribute the attribute
	 * @param strucs the strucs
	 * @return the string[]
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	public String[] struc2Str(String attribute, int[] strucs) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Retrieves all corpus positions where the given token occurs.
	 *
	 * @param attribute the attribute
	 * @param id the id
	 * @return the position
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	public int[] id2Cpos(String attribute, int id) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Retrieves all corpus positions where one of the tokens in "id_list"
	 * occurs; the returned list is sorted as a whole, not per token id.
	 *
	 * @param attribute the attribute
	 * @param ids the ids
	 * @return the positions
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	public int[] idList2Cpos(String attribute, int[] ids) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Retrieves the lexicon IDs of all tokens that match "regex"; the returned
	 * list may be empty (size 0).
	 *
	 * @param attribute the attribute
	 * @param regex the regex
	 * @return the IDs
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	public int[] regex2Id(String attribute, String regex) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Retrieves the start and end corpus positions of structure region "struc".
	 *
	 * @param attribute the attribute
	 * @param struc the struc
	 * @return an array of size 2 containing the start and end positions
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	public int[] struc2Cpos(String attribute, int struc) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Retrieves start and end corpus positions of an alignement region in the
	 * source and target corpora"struc".
	 *
	 * @param attribute the attribute
	 * @param struc the struc
	 * @return an array of size 4 containing (src_start, src_end, target_start,
	 *         target_end)
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	public int[] alg2Cpos(String attribute, int struc) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Runs a CQP query.
	 *
	 * @param motherCorpus the mother corpus
	 * @param subcorpus the subcorpus
	 * @param query the query
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws CqiServerError the cqi server error
	 */
	public void cqpQuery(String motherCorpus, String subcorpus, String query) throws IOException, UnexpectedAnswerException, CqiServerError;

	/**
	 * Lists all the subcorpora of a corpus.
	 *
	 * @param corpus the corpus
	 * @return the name of the subcorpora
	 * @throws UnexpectedAnswerException Signals that the data read on the socket is unexpected
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiCqpErrorNoSuchCorpus the cqi cqp error no such corpus
	 */
	public String[] listSubcorpora(String corpus) throws UnexpectedAnswerException, IOException, CqiServerError;

	/**
	 * Gives the size of a subcorpus .
	 * 
	 * @param subcorpus
	 *            the subcorpus
	 * 
	 * @return the size
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	public int subCorpusSize(String subcorpus) throws IOException,
			UnexpectedAnswerException, CqiServerError;

	/**
	 * Checks wether a subcorpus has a field.
	 * 
	 * @param subcorpus
	 *            the subcorpus
	 * @param field CQI_CONST_FIELD_MATCH, CQI_CONST_FIELD_MATCHEND or CQI_CONST_FIELD_TARGET
	 * 
	 * @return true, if the subcorpus has the field
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	public boolean subCorpusHasField(String subcorpus, byte field) throws IOException, UnexpectedAnswerException, CqiServerError;

	/**
	 * Dumps the values of "field" for match ranges "first" .. "last" in
	 * "subcorpus". "field" is one of the CQI_CONST_FIELD_* constants.
	 * 
	 * @param subcorpus
	 *            the subcorpus
	 * @param field CQI_CONST_FIELD_MATCH, CQI_CONST_FIELD_MATCHEND or CQI_CONST_FIELD_TARGET
	 * @param first the first
	 * @param last the last
	 * 
	 * @return the values
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	public int[] dumpSubCorpus(String subcorpus, byte field, int first, int last) throws IOException, UnexpectedAnswerException,
			CqiServerError;

	/**
	 * Drops a subcorpus.
	 * 
	 * @param subcorpus the subcorpus
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	public void dropSubCorpus(String subcorpus) throws IOException,
			UnexpectedAnswerException, CqiServerError;

	/**
	 * Returns {@code <n>} (id, frequency) pairs flattened into a list of size 2*{@code <n>} NB:
	 * pairs are sorted by frequency desc.
	 * 
	 * @param subcorpus the subcorpus
	 * @param cutoff the cutoff
	 * @param field the field : one of CQI_CONST_FIELD_MATCH, CQI_CONST_FIELD_MATCHEND or CQI_CONST_FIELD_TARGET
	 * @param attribute the attribute
	 * 
	 * @return the int[][]
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	public int[][] fdist1(String subcorpus, int cutoff,
			byte field, String attribute) throws IOException,
			UnexpectedAnswerException, CqiServerError;

	/**
	 * Returns {@code <n>} (id1, id2, frequency) pairs flattened into a list of size
	 * 3*{@code <n>} NB: triples are sorted by frequency desc. .
	 * 
	 * @param subcorpus the subcorpus
	 * @param cutoff the cutoff
	 * @param field1 the field1
	 * @param attribute1 the attribute1
	 * @param field2 the field2
	 * @param attribute2 the attribute2
	 * 
	 * @return the int[]
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnexpectedAnswerException
	 *             Signals that the data read on the socket is unexpected
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	public int[][] fdist2(String subcorpus, int cutoff,
			byte field1, String attribute1, byte field2, String attribute2) throws IOException, UnexpectedAnswerException, CqiServerError;

	public boolean reconnect();

	/**
	 * Runs a CQP query line (ends with ";").
	 *
	 * @param query the query
	 * @throws CqiServerError
	 * @throws UnexpectedAnswerException
	 * @throws IOException
	 */
	public void query(String query) throws IOException, UnexpectedAnswerException, CqiServerError;

	/**
	 * Load a system corpus
	 *
	 * @param regfilepath the subcorpus
	 * @param entry the corpus ID (uppercase)
	 * @return
	 * @throws CqiServerError
	 * @throws UnexpectedAnswerException
	 * @throws IOException
	 */
	public boolean load_a_system_corpus(String regfilepath, String entry) throws IOException, UnexpectedAnswerException, CqiServerError;
}
