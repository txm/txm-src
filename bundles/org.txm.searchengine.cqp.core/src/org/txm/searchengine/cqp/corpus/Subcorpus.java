// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-12-01 16:28:17 +0100 (Tue, 01 Dec 2015) $
// $LastChangedRevision: 3073 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.libs.cqp.CQPLibPreferences;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Match;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.QueryBasedTXMResult;
import org.txm.searchengine.core.Selection;
import org.txm.searchengine.core.messages.SearchEngineCoreMessages;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.NetCqiClient;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.InvalidCqpIdException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.core.functions.selection.SelectionResult;
import org.txm.searchengine.cqp.core.messages.CQPSearchEngineCoreMessages;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.SubcorpusCQLQuery;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.OSDetector;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * The Class Subcorpus represent a MainCorpus subcorpus
 * 
 * @author jmague
 */
public class Subcorpus extends CQPCorpus {

	/**
	 * Query used to build the corpus.
	 */
	@Parameter(key = TXMPreferences.QUERY)
	protected IQuery pQuery;

	//FIXME: SJ, 2024-11-15: not used?
	//	/**
	//	 * Query used to build the corpus.
	//	 */
	//	@Parameter(key = CQPLibPreferences.MATCHINGSTRATEGY)
	//	protected String pMatchingStrategy;

	protected Selection qresult;

	protected SelectionResult selectionResult;

	/**
	 * CQP text ids cache
	 */
	protected String[] cqpTextIDS = null;

	public static final HashSet<String> matchingStrategies = new HashSet<String>(Arrays.asList("shortest", "standard", "longest", "traditional")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	/**
	 * 
	 * @param corpus
	 */
	public Subcorpus(CQPCorpus corpus) {
		super(corpus);
	}

	/**
	 * 
	 * @param partition
	 */
	public Subcorpus(Partition partition) {
		super(partition);
	}

	/**
	 * 
	 * @param result parent result
	 */
	public Subcorpus(QueryBasedTXMResult result) {
		super(result);
		this.pQuery = result.getQuery();
		this.setUserName(pQuery.toString());
	}

	/**
	 * 
	 * @param parametersNodePath
	 */
	public Subcorpus(String parametersNodePath) {
		super(parametersNodePath);
	}

	/**
	 * 
	 * @param parent
	 * @param name
	 * @param c1
	 * @param c2
	 * @return
	 * @throws Exception
	 */
	public static Subcorpus union(CQPCorpus parent, String name, Subcorpus c1, Subcorpus c2) throws Exception {

		c1.compute(false);
		c2.compute(false);

		QueryResult result = union(parent, c1.getQualifiedCqpId(), c2.getQualifiedCqpId());

		Subcorpus unionzed = new Subcorpus(parent);
		unionzed.setParameters(result.getQualifiedCqpId(), name, result);
		unionzed.compute(false); // so the subcorpus is ready to be used
		return unionzed;
	}

	/**
	 * 
	 * @param parent
	 * @param name
	 * @param c1
	 * @param c2
	 * @return
	 * @throws Exception
	 */
	public static Subcorpus intersect(CQPCorpus parent, String name, Subcorpus c1, Subcorpus c2) throws Exception {

		c1.compute(false);
		c2.compute(false);

		QueryResult result = intersect(parent, c1.getQualifiedCqpId(), c2.getQualifiedCqpId());

		Subcorpus unionzed = new Subcorpus(parent);
		unionzed.setParameters(result.getQualifiedCqpId(), name, result);
		unionzed.compute(false); // so the subcorpus is ready to be used
		return unionzed;
	}

	/**
	 * 
	 * @param parent
	 * @param name
	 * @param c1
	 * @param c2
	 * @return
	 * @throws Exception
	 */
	public static Subcorpus difference(CQPCorpus parent, String name, Subcorpus c1, Subcorpus c2) throws Exception {

		c1.compute(false);
		c2.compute(false);

		QueryResult result = difference(parent, c1.getQualifiedCqpId(), c2.getQualifiedCqpId());

		Subcorpus unionzed = new Subcorpus(parent);
		unionzed.setParameters(result.getQualifiedCqpId(), name, result);
		unionzed.compute(false); // so the subcorpus is ready to be used
		
		return unionzed;
	}

	public static QueryResult union(CQPCorpus parent, String qualifiedID1, String qualifiedID2) throws InvalidCqpIdException, UnexpectedAnswerException, IOException, CqiServerError {

		return operation(parent, "join", qualifiedID1, qualifiedID2); //$NON-NLS-1$
	}

	public static QueryResult intersect(CQPCorpus parent, String qualifiedID1, String qualifiedID2) throws InvalidCqpIdException, UnexpectedAnswerException, IOException, CqiServerError {

		return operation(parent, "intersect", qualifiedID1, qualifiedID2); //$NON-NLS-1$
	}

	public static QueryResult difference(CQPCorpus parent, String qualifiedID1, String qualifiedID2) throws InvalidCqpIdException, UnexpectedAnswerException, IOException, CqiServerError {

		return operation(parent, "difference", qualifiedID1, qualifiedID2); //$NON-NLS-1$
	}

	public static QueryResult operation(CQPCorpus parent, String operation, String qualifiedID1, String qualifiedID2)
			throws InvalidCqpIdException, UnexpectedAnswerException, IOException, CqiServerError {

		String cqpid = "S" + Subcorpus.getNextSubcorpusCounter(); //$NON-NLS-1$
		String query = cqpid + "= " + operation + " " + qualifiedID1 + " " + qualifiedID2 + ";"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		AbstractCqiClient cqi = CQPSearchEngine.getCqiClient();

		cqi.query(query);

		return new QueryResult(cqpid, "noname", parent, new CQLQuery(query)); //$NON-NLS-1$
	}

	/**
	 * Instantiates a new subcorpus.
	 * 
	 * Not intended to be used directly.
	 *
	 * @throws Exception the invalid cqp id exception
	 *             {@link CQPCorpus#createSubcorpus(CQLQuery, String)}
	 */
	@Override
	protected boolean __compute(TXMProgressMonitor monitor) throws Exception {

		if (this.getParent() instanceof QueryBasedTXMResult) { // update the pQuery
			this.pQuery = ((QueryBasedTXMResult) this.getParent()).getQuery();
			this.setName(pQuery.getName());
		}

		if (pID == null || pID.length() == 0) {
			pID = subcorpusNamePrefix + getNextSubcorpusCounter().toString();
		}

		if (this.pQuery != null) {
			this.qresult = null; // reset
			String parent_id = this.getCorpusParent().getQualifiedCqpId();
			if (pQuery.getQueryString().length() > CQLQuery.MAX_CQL_LENGTH) {
				Log.warning(NLS.bind(CQPSearchEngineCoreMessages.error_errorColonCQLlengthLimitReachedP0WithP1Length, CQLQuery.MAX_CQL_LENGTH, pQuery.getQueryString().length()));
				return false;
			}

			AbstractCqiClient CQI = CorpusManager.getCorpusManager().getCqiClient();
			String q = this.pQuery.getQueryString();
			if (q.matches("undump \"[^\"]+\";")) { // undump query //$NON-NLS-1$
				String path = q.substring(8, q.length() - 2);
				if (!new File(path).exists()) {
					Log.severe(NLS.bind(CQPSearchEngineCoreMessages.error_errorColonCouldNotComputeSubcorpusFromTheDumpFileColonP0, path));
					return false;
				}
				CQI.query(this.getCorpusParent().getQualifiedCqpId() + ";"); //$NON-NLS-1$
				CQI.query("undump " + this.pID + " < " + q.substring(7)); //$NON-NLS-1$ //$NON-NLS-2$
			}
			else if (q.matches("(union|join|intersect|inter|difference|diff) [^ ]+ [^ ]+;")) { //$NON-NLS-1$
				CQI.query(this.getCorpusParent().getQualifiedCqpId() + ";"); //$NON-NLS-1$
				CQI.query(this.pID + " = " + q); //$NON-NLS-1$
			}
			else {
				String previousMatchingStrategy = null;
				String matchingStrategy = pQuery.getOption(CQPLibPreferences.MATCHINGSTRATEGY);
				if (matchingStrategies.contains(matchingStrategy) && !OSDetector.isFamilyWindows()) { // set MatchingStrategy for this query
					previousMatchingStrategy = CQI.getOption(CQPLibPreferences.MATCHINGSTRATEGY);
					if (previousMatchingStrategy.equals(matchingStrategy)) {
						previousMatchingStrategy = null; // finally no need to change strategy
					}
					else { // ok we need to change strategy
						CQI.setOption(CQPLibPreferences.MATCHINGSTRATEGY, matchingStrategy);
					}
				}

				CQI.cqpQuery(parent_id, this.pID, CQLQuery.fixQuery(q, this.getLang()));

				if (previousMatchingStrategy != null && !OSDetector.isFamilyWindows()) { // restore MatchingStrategy for this query
					CQI.setOption(CQPLibPreferences.MATCHINGSTRATEGY, previousMatchingStrategy);
				}
			}

			//this.qresult = this.pQuery.getSearchEngine().query(this.getCorpusParent(), this.pQuery, this.getUserName(), true); // not working yet

			this.qresult = new QueryResult(this.pID, this.getUserName(), this.getCorpusParent(), (CQLQuery) this.pQuery); // getCorpusParent().query(pQuery, this.pID, true);
		}

		cqpTextIDS = null; // reset text ids cache

		return qresult != null;
	}

	//	public void setMatchingStrategy(String strategy) {
	//		this.pMatchingStrategy = strategy;
	//	}
	//	
	//	public String getMatchingStrategy() {
	//		return this.pMatchingStrategy;
	//	}

	@Override
	public String getComputingStartMessage() {

		if (this.pQuery == null) {
			return TXMCoreMessages.bind(SearchEngineCoreMessages.info_computingTheP0SubCorpusOfP1CommaQueryP2, this.getSimpleName(), this.getParent().getName(), "<no query>"); //$NON-NLS-1$
		}

		// created with structure, property and value(s)
		if (this.pQuery instanceof SubcorpusCQLQuery) {
			SubcorpusCQLQuery query = (SubcorpusCQLQuery) this.pQuery;
			return TXMCoreMessages.bind(SearchEngineCoreMessages.info_computingTheP0SubCorpusOfP1CommaStructureP2CommaPropertyP3ColonP4, this.getSimpleName(), this.getParent().getName(),
					query.getStructure(), query.getProperty().asString(), query.getPropertyValues());
		}
		// created directly from a query
		else {
			return TXMCoreMessages.bind(SearchEngineCoreMessages.info_computingTheP0SubCorpusOfP1CommaQueryP2, this.getSimpleName(), this.getParent().getName(), this.getQuery().asString());
		}
	}

	/*
	 * retro compatibility method for import.xml file
	 * @see org.txm.searchengine.cqp.corpus.Corpus#getLocale()
	 */
	@Override
	protected boolean _load(Element e) {
		if (e != null) {
			this.setUserName(e.getAttribute(NAME));
			this.pID = "S" + getNextSubcorpusCounter(); //$NON-NLS-1$
			this.pQuery = new CQLQuery(e.getAttribute("query")); //$NON-NLS-1$

			NodeList subcorpusElems = e.getElementsByTagName("subcorpus"); //$NON-NLS-1$
			for (int i = 0; i < subcorpusElems.getLength(); i++) {
				Element subcorpusElem = (Element) subcorpusElems.item(i);
				try {
					Subcorpus subcorp = new Subcorpus(this);
					subcorp._load(subcorpusElem);
				}
				catch (Exception ex) {
					Log.warning(TXMCoreMessages.bind(TXMCoreMessages.failedToRestoreTheP0SubcorpusOfP1P2, this.pID, subcorpusElem.getAttribute("name"), ex)); //$NON-NLS-1$
				}
			}

			NodeList partitionElems = e.getElementsByTagName("partition"); //$NON-NLS-1$
			for (int i = 0; i < partitionElems.getLength(); i++) {
				Element partitionElem = (Element) partitionElems.item(i);
				String name = partitionElem.getAttribute("name"); //$NON-NLS-1$
				List<String> names = new ArrayList<>();
				List<String> queries = new ArrayList<>();

				NodeList partElems = partitionElem.getElementsByTagName("part"); //$NON-NLS-1$
				for (int j = 0; j < partElems.getLength(); j++) {
					Element part = (Element) partElems.item(j);
					names.add(part.getAttribute("name")); //$NON-NLS-1$
					queries.add(part.getAttribute("query")); //$NON-NLS-1$
				}
				try {
					// System.out.println("Create Partition with corpus "+this+" : "+name+", queries "+queries+", names "+names);
					Partition partition = new Partition(this);
					partition.setParameters(name, queries, names);
					// partition.setSelfElement(partitionElem);
					partition._load(partitionElem);
				}
				catch (Exception ex) {
					Log.warning(TXMCoreMessages.bind(TXMCoreMessages.failedToRestoreTheP0SubcorpusOfP1P2, name, this.pID, ex));
				}
			}
		}
		initSelectionResult(e);
		// System.out.println("Subcorpus.load: Selection of "+this+" = "+selectionResult);
		// return super.load();
		return true;
	}

	@Override
	public boolean canCompute() {
		return getUserName() != null && getUserName().length() > 0 &&
				((pQuery != null && pQuery.getQueryString().length() > 0) || qresult != null);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.txm.searchengine.cqp.corpus.CqpObject#checkCqpId(java.lang
	 * .String)
	 */
	@Override
	public boolean checkCqpId(String pID) throws InvalidCqpIdException {
		if (!AbstractCqiClient.checkSubcorpusId(pID))
			throw new InvalidCqpIdException(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.p0IsNotAValidCQPIDForASubcorpusItMustBeAnUppercaseCharacterFollowedByLowercaseCharacters, pID));
		return true;
	}

	@Override
	public void clean() {
		super.clean();

		// nothing to do if the corpus has not yet been computed
		if (this.hasBeenComputedOnce()) {
			try {
				AbstractCqiClient CQI = CorpusManager.getCorpusManager().getCqiClient();
				if (CQPSearchEngine.isInitialized()) {
					CQI.dropSubCorpus(this.getQualifiedCqpId());
				}
			}
			catch (Exception e) {
				Log.severe(e.getLocalizedMessage());
				Log.severe(TXMCoreMessages.bind("Qualified CQP id: {0}.", this.getQualifiedCqpId()));
				Log.printStackTrace(e);
			}
		}

		qresult = null;
		selectionResult = null;
	}


	@Override
	public List<? extends Match> getMatches() {
		if (qresult == null) { // not computed
			return new ArrayList<>();
		}
		try {
			return qresult.getMatches();
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return new ArrayList<>();
		}
	}

	public int getNMatch() {
		try {
			return qresult.getNMatch();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
			return 0;
		}
	}

	// protected void finalize() throws Throwable {
	// try {
	// //
	//
	// //As finalize is run in a separate thread, we have problems of concurrent
	// accesses to the cqi server if we drop the subcorpus here.
	// //It as to be done manually
	// } catch (Exception e) {
	// Log.finest("Subcorpus failed to finalize", e);
	// }
	// finally {
	// super.finalize();
	// }
	// }

	/*
	 * (non-Javadoc)
	 * @see
	 * org.txm.searchengine.cqp.corpus.CqpObject#getQualifiedCqpId()
	 */
	@Override
	public String getQualifiedCqpId() {
		return getMainCorpus().getCqpId() + ":" + this.pID; //$NON-NLS-1$
	}

	// /**
	// * Register to parent.
	// *
	// * @param subcorpusName the subcorpus name
	// * @param query the query
	// */
	// private void registerToParent(String subcorpusName, CQLQuery query) {
	// if (this.getSelfElement() == null && this.getParent() != null)
	// try {
	// Element parentElem = (Element) this.getParent().getSelfElement();
	// if (parentElem == null) return;
	// Document doc = parentElem.getOwnerDocument();
	// Element subcorpusElem = doc.createElement("subcorpus"); //$NON-NLS-1$
	// subcorpusElem.setAttribute("name", subcorpusName); //$NON-NLS-1$
	// subcorpusElem.setAttribute("desc", subcorpusName); //$NON-NLS-1$
	// if (query != null)
	// subcorpusElem.setAttribute("query", query.getQueryString()); //$NON-NLS-1$
	// Element corporaElem;
	// if (this.getParent() instanceof MainCorpus) {
	// NodeList corporaList = parentElem.getElementsByTagName("preBuild"); //$NON-NLS-1$
	// corporaElem = (Element) corporaList.item(0);
	// } else {
	// corporaElem = parentElem;
	// }
	// corporaElem.appendChild(subcorpusElem);
	// this.setSelfElement(subcorpusElem);
	// } catch (Exception e) {
	// System.out.println(TXMCoreMessages.Subcorpus_2+Log.toString(e));
	// }
	//
	// this._load();
	// }

	/**
	 * Gets the query.
	 *
	 * @return the query
	 */
	public IQuery getQuery() {
		return pQuery;
	}

	@Override
	public String getResultType() {
		return "Corpus"; //$NON-NLS-1$
	}

	public SelectionResult getSelectionResult() {
		return selectionResult;
	}


	/**
	 * Returns the number of occurrences in the subcorpus.
	 * 
	 * @return the size
	 * 
	 * @throws CqiClientException the cqi client exception
	 * 
	 * @see org.txm.searchengine.cqp.corpus.CQPCorpus#getSize()
	 */
	@Override
	public int getSize() throws CqiClientException {
		if (!this.hasBeenComputedOnce()) return 0;

		if (this.size == -1) {
			// Log.finest(TXMCoreMessages.bind(TXMCoreMessages.SUBCORPUS_SIZE, new Object[]{this.pName, "N/A"}));
			long start = System.currentTimeMillis();

			try {
				AbstractCqiClient cqiClient = CorpusManager.getCorpusManager().getCqiClient();
				String qid = this.getQualifiedCqpId();
				int nMatch = cqiClient.subCorpusSize(qid);
				// System.out.println("n matchs: "+nMatch);
				if (nMatch == 0) {
					this.size = 0;
				}
				else {
					int[] match = cqiClient.dumpSubCorpus(getQualifiedCqpId(), NetCqiClient.CQI_CONST_FIELD_MATCH, 0, nMatch - 1);
					int[] matchend = cqiClient.dumpSubCorpus(getQualifiedCqpId(), NetCqiClient.CQI_CONST_FIELD_MATCHEND, 0, nMatch - 1);
					if (match.length != matchend.length) {
						throw new UnexpectedAnswerException();
					}
					this.size = 0;

					for (int i = 0; i < match.length; i++) {
						size += matchend[i] - match[i] + 1;
					}
				}
			}
			catch (Exception e) {
				throw new CqiClientException(e);
			}
			long end = System.currentTimeMillis();
			Log.finest(NLS.bind(SearchEngineCoreMessages.sizeOfSubcorpusP0P1ComputedInP2Ms, new Object[] { this.pID, this.size, (end - start) }));
		}
		return this.size;
	}

	@Override
	public int[] getStartLimits(String sup) throws IOException, CqiServerError, InvalidCqpIdException, CqiClientException {
		return this.getMainCorpus().getStartLimits(sup);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.txm.searchengine.cqp.corpus.Corpus#getStructuralUnit(java
	 * .lang.String)
	 */
	@Override
	public StructuralUnit getStructuralUnit(String name) throws CqiClientException {
		return getMainCorpus().getStructuralUnit(name);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.searchengine.cqp.corpus.Corpus#getStructuralUnits()
	 */
	@Override
	public List<StructuralUnit> getStructuralUnits() throws CqiClientException {
		return getMainCorpus().getStructuralUnits();
	}

	public SelectionResult initSelectionResult(Element e) {

		if (e != null) {
			selectionResult = new SelectionResult();
			NodeList selectionList = e.getElementsByTagName("selection"); //$NON-NLS-1$
			for (int i = 0; i < selectionList.getLength();) {
				Element selection = (Element) selectionList.item(i); // the selection element

				NodeList textList = selection.getElementsByTagName("selText"); // get text ids //$NON-NLS-1$
				for (int j = 0; j < textList.getLength(); j++) {
					Element text = (Element) textList.item(j);
					selectionResult.add(text.getAttribute("id")); //$NON-NLS-1$
				}

				NodeList critList = selection.getElementsByTagName("selCrit"); // get the criteria //$NON-NLS-1$
				for (int j = 0; j < critList.getLength(); j++) {
					Element crit = (Element) critList.item(j);
					String name = crit.getAttribute("id"); // the name of the critera //$NON-NLS-1$
					if (!selectionResult.critera.containsKey(name)) // create new list if new critera
						selectionResult.critera.put(name, new ArrayList<String>());
					selectionResult.critera.get(name).add(crit.getAttribute("value")); // add the value //$NON-NLS-1$
				}
				break; // process only one selection node
			}
		}
		return selectionResult;
	}

	@Override
	public boolean loadParameters() throws Exception {
		String q = this.getStringParameterValue(TXMPreferences.QUERY);
		if (!q.isEmpty()) {
			pQuery = new CQLQuery(q);
		}

		return true;
	}

	@Override
	public boolean saveParameters() throws Exception {
		if (pQuery != null) {
			this.saveParameter(TBXPreferences.QUERY, pQuery.getQueryString());
		}
		return true;
	}

	/**
	 * Instantiates a new subcorpus.
	 * 
	 * Not intended to be used directly.
	 *
	 * @param pID the cqp id
	 * @param name the name
	 * @param query the query
	 * @throws InvalidCqpIdException the invalid cqp id exception
	 *             {@link CQPCorpus#createSubcorpus(CQLQuery, String)}
	 */
	protected void setParameters(String pID, String name, CQLQuery query) throws InvalidCqpIdException {

		this.pID = pID;
		this.pQuery = query;
		this.setUserName(name);
	}

	/**
	 * 
	 * @param cqpId
	 * @param name
	 * @param queryResult
	 */
	public void setParameters(String cqpId, String name, QueryResult queryResult) {

		this.pID = cqpId;
		this.setUserName(name);
		this.pQuery = queryResult.getQuery();
		this.qresult = queryResult;

		this.setDirty();
	}

	/**
	 * Sets the query to use.
	 * 
	 * @param query
	 */
	public void setQuery(CQLQuery query) {
		this.pQuery = query;
	}


	// TODO merge SelectionResult and Subcorpus ?
	public void setSelectionResult(SelectionResult selectionResult) {
		this.selectionResult = selectionResult;
		// TODO persists the selectionResult ?
		// if (this.getSelfElement() != null) {
		// //remove old node if any
		// NodeList selectionList = getSelfElement().getElementsByTagName("selection"); //$NON-NLS-1$
		// for (int i = 0 ; i < selectionList.getLength() ; i++)
		// getSelfElement().removeChild(selectionList.item(i));
		//
		// Document doc = this.getParent().getSelfElement().getOwnerDocument();
		// Element selectionElem = doc.createElement("selection"); // the selection element //$NON-NLS-1$
		// for (String txt : selectionResult) // the text ids
		// {
		// Element textElem = doc.createElement("selText"); //$NON-NLS-1$
		// textElem.setAttribute("id", txt); //$NON-NLS-1$
		// selectionElem.appendChild(textElem);
		// }
		// for (String crit : selectionResult.critera.keySet()) // the critera
		// {
		// ArrayList<String> values = selectionResult.critera.get(crit);
		// for (String value : values) {
		// Element criteraElem = doc.createElement("selCrit"); //$NON-NLS-1$
		// criteraElem.setAttribute("id", crit); //$NON-NLS-1$
		// criteraElem.setAttribute("value", value); //$NON-NLS-1$
		// selectionElem.appendChild(criteraElem);
		// }
		// }
		// getSelfElement().appendChild(selectionElem);// append selection elem to self
		// }
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getName();
	}

	@Override
	@Deprecated
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		
		IOUtils.write(outfile, this.getName()+" ("+this.getCqpId()+"): "+this.getSize()+" tokens query="+this.getQuery());
		return outfile.exists();
	}

	@Override
	public void setIsModified(boolean b) {
		// nothing
	}

	@Override
	public CorpusBuild getRootCorpusBuild() {
		return getMainCorpus();
	}

	/**
	 * Return the CQP START positions of the main corpus texts
	 */
	@Override
	public int[] getTextStartLimits() throws CqiClientException, IOException, CqiServerError, InvalidCqpIdException {
		if (textLimits == null) {
			textLimits = getStartLimits("[text] expand to text"); //$NON-NLS-1$
		}
		return textLimits;
	}

	/**
	 * Return the CQP END positions of the main corpus texts
	 */
	@Override
	public int[] getTextEndLimits() throws CqiClientException, IOException, CqiServerError, InvalidCqpIdException {
		if (textEndLimits == null) {
			String queryResultId = queryResultNamePrefix + UUID.randomUUID().toString();
			CorpusManager.getCorpusManager().getCqiClient().cqpQuery(
					this.getQualifiedCqpId(), queryResultId, "[text] expand to text"); //$NON-NLS-1$
			QueryResult queryResult = new QueryResult(queryResultId, queryResultId, this, new CQLQuery("[text] expand right to text")); //$NON-NLS-1$
			textEndLimits = queryResult.getEnds();
		}
		return textEndLimits;
	}

	/**
	 * Gets the nb texts.
	 *
	 * @return the nb texts
	 * @throws CqiClientException the cqi client exception
	 * @throws CqiServerError
	 * @throws IOException
	 */
	@Override
	public int getNbTexts() throws CqiClientException, IOException, CqiServerError {
		if (nbtext == -1) {
			List<Integer> structsIncorpus = getTextNumberInCorpus();
			nbtext = structsIncorpus.size();
		}
		return nbtext;
	}

	/**
	 * 
	 * @return
	 * @throws CqiClientException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	private List<Integer> getTextNumberInCorpus() throws CqiClientException, IOException, CqiServerError {

		StructuralUnit text_su = this.getStructuralUnit("text"); //$NON-NLS-1$
		StructuralUnitProperty text_id_sup = text_su.getProperty("id"); //$NON-NLS-1$
		nbtext = CorpusManager.getCorpusManager().getCqiClient().attributeSize(text_id_sup.getQualifiedName());

		int[] structs = new int[nbtext];
		int[][] structs_positions = new int[structs.length][2];
		for (int i = 0; i < nbtext; i++) {
			structs[i] = i;
			structs_positions[i] = CorpusManager.getCorpusManager().getCqiClient().struc2Cpos(text_id_sup.getQualifiedName(), i);
		}

		List<Integer> structsIncorpus = new ArrayList<>();
		// filter structs with matches
		List<? extends Match> matches = this.getMatches();
		int iText = 0;
		int iMatch = 0;
		nbtext = 0;
		while (iText < structs.length && iMatch < matches.size()) {
			if (structs_positions[iText][1] < matches.get(iMatch).getStart()) { // match before text
				iText++;
			}
			else if (matches.get(iMatch).getEnd() < structs_positions[iText][0]) { // text before match
				iMatch++;
			}
			else { // a match in the text
				structsIncorpus.add(iText);
				iText++;
			}
		}
		return structsIncorpus;
	}

	/**
	 * 
	 * @return the text_id values of the CQP corpus ordered by position
	 * @throws CqiClientException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	@Override
	public String[] getCorpusTextIdsList() throws CqiClientException, IOException, CqiServerError {

		if (cqpTextIDS == null) {
			StructuralUnit text_su = this.getStructuralUnit("text"); //$NON-NLS-1$
			StructuralUnitProperty text_id_sup = text_su.getProperty("id"); //$NON-NLS-1$

			List<Integer> list = getTextNumberInCorpus();
			int[] structs = new int[list.size()];
			for (int i = 0; i < list.size(); i++) {
				structs[i] = list.get(i);
			}

			cqpTextIDS = CorpusManager.getCorpusManager().getCqiClient().struc2Str(text_id_sup.getQualifiedName(), structs);
		}
		return cqpTextIDS;
	}

	/**
	 * Gets the texts ids and order number in corpus.
	 *
	 * @return the texts ids
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 * @throws CqiClientException the cqi client exception
	 */
	@Override
	public HashMap<String, Integer> getTextsIds() throws IOException, CqiServerError, CqiClientException {
		if (textids == null) {
			textids = new HashMap<>();
			List<Integer> list = getTextNumberInCorpus();
			int[] structs = new int[list.size()];
			for (int i = 0; i < list.size(); i++) {
				structs[i] = list.get(i);
			}

			StructuralUnit text_su = this.getStructuralUnit("text"); //$NON-NLS-1$
			StructuralUnitProperty text_id_sup = text_su.getProperty("id"); //$NON-NLS-1$

			String[] ids = CorpusManager.getCorpusManager().getCqiClient().struc2Str(text_id_sup.getQualifiedName(), structs);
			for (int i = 0; i < ids.length; i++) {
				textids.put(ids[i], structs[i]);
			}
		}
		return textids;
	}

	@Override
	public String getName() {
		return ""+this.getCorpusParent().getSimpleName() + TXMPreferences.PARENT_NAME_SEPARATOR + this.getSimpleName();
	}

	@Override
	public String getDetails() {
		try {
			if (hasBeenComputedOnce()) {
				return ""+this.getSimpleName() + "\tT " + NumberFormat.getInstance().format(this.getSize()) + "\t(" + this.pID + ")"; //$NON-NLS-1$
			}
			else {
				return this.getName();
			}
		}
		catch (CqiClientException e) {
			return this.getName() + " (" + this.getQualifiedCqpId() + ")"; //$NON-NLS-1$
		}
	}

	@Override
	public List<?> isBuildValid() {

		if (this.getParent() != null) return new LinkedList<String>(); // ok

		return Arrays.asList("No parent corpus result");
	}

	@Override
	public String isReady() {

		if (!hasBeenComputedOnce()) return "the subcorpus is not a computed TXMResult";

		try {
			int s = this.getSize();
			if (s >= 0) return null;
			return "negative size"; //$NON-NLS-1$
		}
		catch (CqiClientException e) {
			return e.getMessage();
		}
	}
}
