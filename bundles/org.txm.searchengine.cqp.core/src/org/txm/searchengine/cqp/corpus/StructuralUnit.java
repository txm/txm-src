// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * A structural units of a corpus Represents a <strong>type</strong>, not a
 * specif ic unit.
 * 
 * @author jmague
 * 
 */
public class StructuralUnit implements Comparable {


	/**
	 * Internal properties names.
	 */
	private static String[] INTERNAL_PROPERTIES_NAMES = new String[] { "base", "project" }; //$NON-NLS-1$ //$NON-NLS-2$


	/** The name. */
	private String name;

	/** The corpus. */
	private CQPCorpus corpus;

	/** The properties. */
	private List<StructuralUnitProperty> properties;



	/**
	 * Instantiates a new StrucutalUnit.
	 *
	 * @param name the name
	 * @param properties the properties
	 * @param corpus the corpus
	 * @throws CqiClientException the cqi client exception
	 */
	protected StructuralUnit(String name, List<String> properties, CQPCorpus corpus)
			throws CqiClientException {
		this.name = name;
		this.corpus = corpus;
		this.properties = new ArrayList<StructuralUnitProperty>();
		for (String property : properties)
			this.properties.add(new StructuralUnitProperty(this, property, corpus));
	}

	/**
	 * Gets the qualified name: CORPUS.struct
	 * 
	 * @return the qualified name
	 */
	public String getQualifiedName() {
		return corpus.getCqpId() + "." + this.getName(); //$NON-NLS-1$
	}

	/**
	 * Gets the name without the corpus name
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the actual properties defined on this type of structural unit.
	 * 
	 * @return the properties
	 */
	public List<StructuralUnitProperty> getProperties() {
		return properties;
	}

	/**
	 * Gets the properties defined on this type of structural unit.
	 * Does not return the internal properties as base, project, etc.
	 * 
	 * @return
	 */
	public List<StructuralUnitProperty> getUserDefinedProperties() {
		List<StructuralUnitProperty> userDefinedProperties = new ArrayList<StructuralUnitProperty>();

		for (int i = 0; i < this.properties.size(); i++) {
			StructuralUnitProperty p = this.properties.get(i);
			if (Arrays.asList(INTERNAL_PROPERTIES_NAMES).contains(p.getName())) {
				continue;
			}
			userDefinedProperties.add(p);
		}
		return userDefinedProperties;
	}


	/**
	 * Gets the ordered properties.
	 * Does not return the internal properties as base, project, etc.
	 *
	 * @return the ordered properties
	 */
	public List<StructuralUnitProperty> getOrderedProperties() {
		List<StructuralUnitProperty> props = this.getProperties();
		Collections.sort(props);
		return props;
	}


	/**
	 * Gets the user defined ordered properties.
	 * 
	 * @return
	 */
	public List<StructuralUnitProperty> getUserDefinedOrderedProperties() {
		List<StructuralUnitProperty> props = this.getUserDefinedProperties();
		Collections.sort(props);
		return props;
	}


	/**
	 * Gets a property from its name.
	 * 
	 * @param name
	 *            the name
	 * 
	 * @return the property
	 */
	public StructuralUnitProperty getProperty(String name) {
		for (StructuralUnitProperty property : properties) {
			if (property.getName().equals(name)) {
				return property;
			}
		}
		return null;

	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name;
	}

	/**
	 * Equals.
	 *
	 * @param su the su
	 * @return true, if successful
	 */
	public boolean equals(StructuralUnit su) {
		return name.equals(su.name) && corpus.equals(su.corpus);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Object arg0) {
		if (arg0 instanceof StructuralUnit)
			return this.getQualifiedName().compareTo(
					((StructuralUnit) arg0).getQualifiedName());
		return 0;
	}


	/**
	 * 
	 * @param corpus
	 * @param v
	 * @return
	 */
	public static List<StructuralUnit> stringToStructuralUnits(CQPCorpus corpus, String v) {
		ArrayList<StructuralUnit> structs = new ArrayList<StructuralUnit>();
		if (v == null) return structs;
		if (v.length() == 0) return structs;

		String[] split = v.split("\t"); //$NON-NLS-1$
		for (String s : split) {
			StructuralUnit struc;
			try {
				struc = corpus.getStructuralUnit(s);
			}
			catch (CqiClientException e) {
				Log.severe(NLS.bind("Error: {0} structure not found in {1}", v, corpus));
				Log.printStackTrace(e);
				return null;
			}
			if (struc != null) {
				structs.add(struc);
			}
		}
		return structs;
	}
}
