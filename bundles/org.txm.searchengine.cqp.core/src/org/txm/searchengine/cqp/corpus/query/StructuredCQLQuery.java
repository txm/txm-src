// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-06-15 15:29:16 +0200 (Mon, 15 Jun 2015) $
// $LastChangedRevision: 2989 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus.query;

import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;

/**
 * A query that select a structured subcorpus.
 * 
 * @author mdecorde
 */
public class StructuredCQLQuery extends CQLQuery {

	/** The structure. */
	protected StructuralUnit structure;

	/** The property. */
	protected StructuralUnitProperty property;

	/** The value. */
	protected String value;

	/**
	 * Instantiates a new query part.
	 * 
	 * @param structure the structure
	 * @param property the property
	 * @param query the query
	 */
	public StructuredCQLQuery(StructuralUnit structure, StructuralUnitProperty property, String query) {
		super(query);
		this.structure = structure;
		this.property = property;
	}

	public StructuralUnit getStructure() {
		return structure;
	}

	public StructuralUnitProperty getProperty() {
		return property;
	}
}
