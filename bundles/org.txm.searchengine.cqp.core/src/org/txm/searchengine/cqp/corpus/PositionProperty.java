package org.txm.searchengine.cqp.corpus;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.txm.searchengine.cqp.clientExceptions.CqiClientException;

/**
 * A CQP property at token/word level
 * 
 * @author mdecorde
 *
 */
public class PositionProperty extends VirtualProperty {

	int maxLength;

	NumberFormat f;

	/**
	 * Creates a new WordProperty.
	 * 
	 * @param corpus
	 */
	public PositionProperty(CQPCorpus corpus) {
		super("position", corpus);
		try {
			maxLength = Integer.toString(corpus.getMainCorpus().getSize()).length();
		}
		catch (CqiClientException e) {
			e.printStackTrace();
			maxLength = Integer.toString(Integer.MAX_VALUE).length();
		}

		f = NumberFormat.getNumberInstance();
		f.setMinimumIntegerDigits(maxLength);
	}

	public String cpos2Str(int position) {
		return f.format(position);
	}

	public String[] cpos2Str(int[] positions) {
		return getValues(positions).toArray(new String[positions.length]);
	}

	public List<String> getValues(int[] positions) {
		ArrayList<String> rez = new ArrayList<>();
		for (int p : positions) {
			rez.add(cpos2Str(p));
		}
		return rez;
	}

	@Override
	public int[] cpos2Id(int[] allpositionsarray) {
		return allpositionsarray; // lol
	}

	@Override
	public int cpos2Id(int position) {
		return position; // lol
	}

	@Override
	public String[] id2Str(int[] indices) {
		String[] rez = new String[indices.length];
		for (int i = 0; i < rez.length; i++) {
			rez[i] = cpos2Str(indices[i]);
		}
		return rez;
	}

	@Override
	public String id2Str(int indice) {
		return cpos2Str(indice);
	}

	@Override
	public String getCQLTest(String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCQLTest(List<String> values) {
		// TODO Auto-generated method stub
		return null;
	}
}

