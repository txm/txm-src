// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-06-15 15:29:16 +0200 (Mon, 15 Jun 2015) $
// $LastChangedRevision: 2989 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus.query;

import java.util.List;

import org.txm.searchengine.cqp.core.preferences.SubcorpusPreferences;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;

/**
 * A query that select a structured subcorpus from a corpus using a regular expression.
 * 
 * @author mdecorde
 */
public class SubcorpusCQLQuery extends CQLQuery {

	/** The structure. */
	protected StructuralUnit structure;

	/** The property. */
	protected StructuralUnitProperty property;

	/**
	 * Selected values of the property.
	 */
	protected List<String> propertyValues;

	/** The value. */
	protected String regexp;


	/**
	 * 
	 * 
	 * @param structure the structure
	 * @param property the property
	 * @param regexp the regexp
	 */
	public SubcorpusCQLQuery(StructuralUnit structure, StructuralUnitProperty property, String regexp) {
		this(structure, property, regexp, null);
	}

	/**
	 * 
	 * @param structure
	 * @param property
	 * @param regexp
	 * @param propertyValues
	 */
	public SubcorpusCQLQuery(StructuralUnit structure, StructuralUnitProperty property, String regexp, List<String> propertyValues) {
		super(null); // set later in this method
		this.structure = structure;
		this.property = property;
		this.propertyValues = propertyValues;
		this.regexp = regexp;

		// String tag = structure.getName() + TAG_ATTRIBUTE_SEPARATOR + property.getName();
		// String label = "a"; //$NON-NLS-1$
		// StringBuffer query = new StringBuffer();
		// query.append(REGION_MACRO + START_MACRO_ARGS);
		// query.append(tag + MACRO_ARGS_SEPARATOR + label + END_MACRO_ARGS);
		// query.append(START_GLOBAL_CONSTRAINT + label + DOT + tag + EQUALS
		// + START_DOUBLE_QUOTE + value + END_DOUBLE_QUOTE);
		// queryString = query.toString();

		// if ("text".equals(structure.getName())) { // optimisation only for the text structure
		// queryString = "<" + property.getFullName() + "=\"" + regexp + "\">[] expand to " + structure.getName();
		// }
		// else {
		// queryString = "[_." + property.getFullName() + "=\"" + regexp + "\"] expand to " + structure.getName();
		// }

		if (SubcorpusPreferences.getInstance().getBoolean(SubcorpusPreferences.OPTIMIZED_MODE)) {
			//SJ: other version
			//queryString = String.format("<%s=\"%s\">[] expand to %s", property.getFullName(), regexp, structure.getName());
			queryString = "<" + property.getFullName() + "=\"" + regexp + "\">[] expand to " + structure.getName(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		else {
			//SJ: other version
//			queryString = String.format("[_.%s=\"%s\"] expand to %s", property.getFullName(), regexp, structure.getName());
			queryString = "[_." + property.getFullName() + "=\"" + regexp + "\"] expand to " + structure.getName(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			
		}
	}


	public String getRegExp() {
		return regexp;
	}


	/**
	 * Gets the structure used to build the query.
	 * 
	 * @return the structure
	 */
	public StructuralUnit getStructure() {
		return structure;
	}


	/**
	 * Gets the property used to build the query.
	 * 
	 * @return the property
	 */
	public StructuralUnitProperty getProperty() {
		return property;
	}


	/**
	 * @return the propertyValues
	 */
	public List<String> getPropertyValues() {
		return propertyValues;
	}

	// /*
	// * (non-Javadoc)
	// *
	// * @see org.txm.searchengine.cqp.corpus.query.Query#getQueryString()
	// */
	// /*
	// * Produce a query such as: [_.struct_property="value"] expand to struct
	// *
	// * OLD VERSION: /region[TEI_type,a]::a.TEI_type="value"
	// */
	// @Override
	// public String getQueryString() {
	// return queryString;
	//
	// // another way to query parts less restrictif but missleading
	// //return String.format("[_.%s=\"%s\"] expand to %s", tag, value, structure.getName());
	// }
}
