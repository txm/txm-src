// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-09-05 09:27:13 +0200 (jeu., 05 sept. 2013) $
// $LastChangedRevision: 2529 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus.query;

import java.util.ArrayList;
import java.util.List;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.searchengine.cqp.corpus.Part;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * Denote a part in a partition.
 * 
 * @author sloiseau
 */
public class PartitionFocus {

	/** The analysis property. */
	private final Partition partition;

	/** The modality. */
	private final List<String> modality;

	/** The focus index. */
	private int[] focusIndex = null;

	/**
	 * Instantiates a new focus.
	 *
	 * @param partition the partition
	 * @param modality the modality
	 */
	public PartitionFocus(Partition partition, String modality) {
		// this(analysisProperty, new ArrayList<String>().add(modality));
		super();
		this.partition = partition;
		this.modality = new ArrayList<String>();
		this.modality.add(modality);
		checkModalityList();
	}

	/**
	 * Instantiates a new focus.
	 *
	 * @param partition the partition
	 * @param modality the modality
	 */
	public PartitionFocus(Partition partition, List<String> modality) {
		super();
		this.partition = partition;
		this.modality = modality;
		checkModalityList();
	}

	/**
	 * Check modality list.
	 */
	private void checkModalityList() {
		List<String> partNames = partition.getPartNames();
		for (String mod : modality) {
			if (mod == null || mod.equals("")) { //$NON-NLS-1$
				Log.finest(TXMCoreMessages.theFocusCannotBeNullOrEmpty);
				throw new IllegalArgumentException(TXMCoreMessages.focusIsEmptyOrNull);
			}
			if (!partNames.contains(mod)) {
				Log.finest(TXMCoreMessages.bind(TXMCoreMessages.theP0PartFocusDoesntBelongToPartitionP1, mod, partNames));
				throw new IllegalArgumentException(TXMCoreMessages.unknownPartitionName);
			}
		}
	}

	/**
	 * Gets the focused parts.
	 *
	 * @return the focused parts
	 */
	public List<Part> getFocusedParts() {
		List<Part> focused = new ArrayList<Part>(modality.size());
		for (int i = 0; i < partition.getPartsCount(); i++) {
			if (modality.contains(partition.getParts().get(i).getName())) {
				focused.add(partition.getParts().get(i));
			}
		}
		return focused;
	}

	/**
	 * Gets the not focused parts.
	 *
	 * @return the not focused parts
	 */
	public List<Part> getNotFocusedParts() {
		List<Part> notFocused = new ArrayList<Part>(modality.size());
		for (int i = 0; i < partition.getPartsCount(); i++) {
			if (!modality.contains(partition.getParts().get(i).getName())) {
				notFocused.add(partition.getParts().get(i));
			}
		}
		return notFocused;
	}

	/**
	 * Gets the analysis property.
	 * 
	 * @return the analysis property
	 */
	public Partition getPartition() {
		return partition;
	}

	/**
	 * Gets the modality.
	 * 
	 * @return the modality
	 */
	public List<String> getModality() {
		return modality;
	}

	/**
	 * Gets the focus index.
	 *
	 * @return the focus index
	 */
	public int[] getFocusIndex() {
		if (focusIndex == null)
			computeFocusIndex();
		return focusIndex;
	}

	/**
	 * Compute focus index.
	 */
	private void computeFocusIndex() {
		focusIndex = new int[modality.size()];
		List<String> partNames = partition.getPartNames();
		int y = 0;
		for (int i = 0; i < modality.size(); i++) {
			if (partNames.contains(modality.get(i))) {
				focusIndex[y] = i;
				y++;
			}
		}
	}
}
