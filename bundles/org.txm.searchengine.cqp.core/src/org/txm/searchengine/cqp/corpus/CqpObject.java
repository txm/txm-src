// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus;

import org.txm.searchengine.cqp.clientExceptions.InvalidCqpIdException;

// TODO: Auto-generated Javadoc
/**
 * The top class for all object having a counterpart in CQP.
 * 
 * @author Jean-Philippe Magué
 */
public interface CqpObject {

	/** The Constant subcorpusNamePrefix. */
	public static final String subcorpusNamePrefix = "S"; //$NON-NLS-1$

	/** The Constant partNamePrefix. */
	public static final String partNamePrefix = "P"; //$NON-NLS-1$

	/** The Constant queryResultNamePrefix. */
	public static final String queryResultNamePrefix = "Q"; //$NON-NLS-1$

	/**
	 * The cqp id.
	 *
	 * @param cqpId the cqp id
	 * @return true, if successful
	 * @throws InvalidCqpIdException the invalid cqp id exception
	 */
	// protected String cqpId;

	/** The name. */
	// protected String name;

	/** The size. */
	// protected int size = -1;

	/** The mother main corpus. */
	// protected MainCorpus motherMainCorpus;

	/**
	 * Instantiates a new cqp object mirroring the corpus in CQP with the given
	 * id.
	 * 
	 * @param cqpId
	 *            the cqp id
	 * 
	 * @throws InvalidCqpIdException
	 *             the invalid cqp id exception
	 */
	// public void CqpObject(String cqpId);// throws InvalidCqpIdException;/*{

	/**
	 * Check whether the ID is a valid CQP ID.
	 * 
	 * @param cqpId
	 *            the cqp id
	 * 
	 * @return true, if cqpId is a valid CQP ID.
	 * 
	 * @throws InvalidCqpIdException
	 *             the invalid cqp id exception
	 */
	public boolean checkCqpId(String cqpId) throws InvalidCqpIdException;

	/**
	 * Gets the cqp id.
	 * 
	 * @return the cqp id
	 */
	public String getCqpId();/*
								 * { return cqpId; }
								 */

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName();/*
							 * { return name; }
							 */

	/**
	 * Gets the qualified cqp id.
	 * 
	 * @return the qualified cqp id
	 */
	public String getQualifiedCqpId();

	/**
	 * Gets the mother corpus.
	 * 
	 * @return the mother corpus
	 */
	public MainCorpus getMainCorpus();/*
										 * { return motherMainCorpus; }
										 */
}
