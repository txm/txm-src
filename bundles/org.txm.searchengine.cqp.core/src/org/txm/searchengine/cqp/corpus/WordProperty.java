package org.txm.searchengine.cqp.corpus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.MatchUtils;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.logger.Log;

/**
 * A CQP property at token/word level
 * 
 * @author mdecorde
 *
 */
public class WordProperty extends Property {

	/**
	 * Creates a new WordProperty.
	 * 
	 * @param name
	 * @param corpus
	 */
	protected WordProperty(String name, CQPCorpus corpus) {
		super(name, corpus);
	}

	@Override
	public String getFullName() {
		return getName();
	}


	/**
	 * Creates a readable formated string from the specified properties list.
	 * 
	 * @param properties
	 * @return
	 */
	public static String asString(List<WordProperty> properties) {

		String str = ""; //$NON-NLS-1$
		for (int i = 0; i < properties.size(); i++) {
			if (i > 0) {
				str += "_"; //$NON-NLS-1$
			}
			str += properties.get(i).getFullName();
		}
		return "@" + str; //$NON-NLS-1$
	}

	/**
	 * Converts the specified string to a list of Property from the specified Corpus.
	 * 
	 * @param corpus
	 * @param str the String to 'deserialize'. Each property is separated with 1 tabulation. Then calls fromString on each token
	 * @return a list of StructuralUnitProperty if the s param is well formed
	 */
	public static List<WordProperty> stringToProperties(CQPCorpus corpus, String str) {
		ArrayList<WordProperty> properties = new ArrayList<>();

		if (str == null || corpus == null) {
			return properties;
		}

		String[] split = str.split(TXMPreferences.LIST_SEPARATOR);
		for (String s : split) {
			try {
				WordProperty p = corpus.getProperty(s); // manage all Property implementations : Word, StructuralUnits and Virtual

				if (p != null) {
					properties.add(p);
				}
			}
			catch (CqiClientException e) {
				Log.warning(TXMCoreMessages.bind("** Could not find the ''{0}'' word property found in the ''{1}'' corpus. Error: {2}.", s, corpus, e));
				Log.printStackTrace(e);
			}
		}
		return properties;
	}

	/**
	 * 
	 * @param positions
	 * @return the values for the property's corpus positions
	 * @throws UnexpectedAnswerException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public int[] cpos2Id(int[] positions) throws UnexpectedAnswerException, IOException, CqiServerError {
		return CorpusManager.getCorpusManager().getCqiClient().cpos2Id(this.getQualifiedName(), positions);
	}

	/**
	 * 
	 * @param position
	 * @return
	 * @throws UnexpectedAnswerException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public String cpos2Str(int position) throws UnexpectedAnswerException, IOException, CqiServerError {
		return CorpusManager.getCorpusManager().getCqiClient().cpos2Str(this.getQualifiedName(), new int[] { position })[0];
	}

	/**
	 * 
	 * @param positions
	 * @return
	 * @throws UnexpectedAnswerException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public String[] cpos2Str(int[] positions) throws UnexpectedAnswerException, IOException, CqiServerError {
		return CorpusManager.getCorpusManager().getCqiClient().cpos2Str(this.getQualifiedName(), positions);
	}

	/**
	 * 
	 */
	public String[] id2Str(int[] ids) throws UnexpectedAnswerException, IOException, CqiServerError {
		return CorpusManager.getCorpusManager().getCqiClient().id2Str(this.getQualifiedName(), ids);
	}

	/**
	 * 
	 * @param values
	 * @return
	 * @throws UnexpectedAnswerException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public int[] str2Id(String[] values) throws UnexpectedAnswerException, IOException, CqiServerError {
		return CorpusManager.getCorpusManager().getCqiClient().str2Id(this.getQualifiedName(), values);
	}

	/**
	 * 
	 * @param regex
	 * @return
	 * @throws UnexpectedAnswerException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public int[] regex2Id(String regex) throws UnexpectedAnswerException, IOException, CqiServerError {
		return CorpusManager.getCorpusManager().getCqiClient().regex2Id(this.getQualifiedName(), regex);
	}


	/**
	 * 
	 * @param value
	 * @return
	 * @throws UnexpectedAnswerException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public int str2Id(String value) throws UnexpectedAnswerException, IOException, CqiServerError {
		return CorpusManager.getCorpusManager().getCqiClient().str2Id(this.getQualifiedName(), new String[] { value })[0];
	}

	/**
	 * 
	 * @param value
	 * @return the CQL inner word test in [] for a value
	 */
	public String getCQLTest(String value) {
		return this.getName() + "=\"" + CQLQuery.addBackSlash(value) + "\""; //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * 
	 * @param values
	 * @return the CQL inner word test in [] for several values
	 */
	public String getCQLTest(List<String> values) {

		StringBuilder sb = new StringBuilder();
		sb.append(this.getName() + "=\""); //$NON-NLS-1$
		for (int i = 0; i < values.size(); i++) {
			String s = values.get(i);
			s = CQLQuery.addBackSlash(s);
			sb.append(s);
			if (i < values.size() - 1) {
				sb.append("|"); //$NON-NLS-1$
			}
		}
		sb.append("\""); //$NON-NLS-1$
		return sb.toString();
	}

	public String combine(List<String> values) {
		return StringUtils.join(values, " "); //$NON-NLS-1$
	}

	/**
	 * 
	 * @return all indexed values in the main corpus
	 * 
	 * @throws UnexpectedAnswerException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public String[] getValues() throws UnexpectedAnswerException, IOException, CqiServerError {
		int n = CorpusManager.getCorpusManager().getCqiClient().lexiconSize(this.getQualifiedName());
		int[] ids = MatchUtils.toPositions(0, n - 1);
		return CorpusManager.getCorpusManager().getCqiClient().id2Str(this.getQualifiedName(), ids);
	}

	/**
	 * 
	 * @return the number of indexed values in the main corpus
	 * 
	 * @throws UnexpectedAnswerException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public int getNValues() throws UnexpectedAnswerException, IOException, CqiServerError {
		return CorpusManager.getCorpusManager().getCqiClient().lexiconSize(this.getQualifiedName());
	};
}
