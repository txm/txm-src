package org.txm.searchengine.cqp.corpus;

import java.util.ArrayList;

import org.txm.utils.TXMProgressMonitor;

public class TextsPartition extends StructuredPartition {

	public TextsPartition(CQPCorpus parent) {

		super(parent);

		this.setUserName("Texts");
	}

	public TextsPartition(String parametersNodePath) {

		super(parametersNodePath);
		this.setUserName("Texts");
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		if (this.getPartsCount() == 0) {
			this.pProperty = this.getParent().getTextIdStructuralUnitProperty();
			_compute_with_property(monitor);
		}
		this.totalsize = -1; // refresh total size at next call of getTotalSize
		return true;
	}

	public void setParameters(ArrayList<String> selectedValues) {

		this.pValues = selectedValues;
	}
}
