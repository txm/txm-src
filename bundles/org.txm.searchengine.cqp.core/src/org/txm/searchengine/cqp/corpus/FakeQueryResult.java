// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-03-01 10:10:58 +0100 (mar., 01 mars 2016) $
// $LastChangedRevision: 3133 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.InvalidCqpIdException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.serverException.CqiServerError;

// TODO: Auto-generated Javadoc
/**
 * The result of a CQP query on a corpus.
 * 
 * @author mdecorde
 */
public class FakeQueryResult extends QueryResult {

	protected int[] starts;

	protected int[] ends;

	protected int[] targets;

	/**
	 * Instantiates a new query result.
	 * 
	 * Not intended to be used directly.
	 * 
	 * @param name
	 *            the name
	 * @param queriedCorpus
	 *            the queried corpus
	 * @param query
	 *            the query
	 * 
	 * @throws InvalidCqpIdException
	 *             the invalid cqp id exception
	 */
	public FakeQueryResult(String name, CQPCorpus queriedCorpus,
			CQLQuery query, int[] starts, int[] ends, int[] targets) throws InvalidCqpIdException {
		super("Fakeqr", name, queriedCorpus, query); //$NON-NLS-1$
		this.starts = starts;
		this.ends = ends;
		this.targets = targets;
	}

	/**
	 * Gets the number of matches.
	 * 
	 * @return the number of matches
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	@Override
	public int getNMatch() throws CqiClientException {
		if (nMatch == -1)
			nMatch = starts.length;
		return nMatch;
	}

	/**
	 * Retrieves some of the matches of a query
	 * 
	 * Not all the matches are returned, but only those between from and to. For
	 * example, getMatches(0,9) returns the first 10 matches while
	 * getMatches(0,getNMatches()) return them all.
	 * 
	 * @param from
	 *            the from
	 * @param to
	 *            the to
	 * 
	 * @return the matches
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	/**
	 * @return
	 * @throws CqiClientException
	 */
	@Override
	public int[] getStartMatches(int from, int to) throws CqiClientException {
		int[] substarts = new int[to - from];
		System.arraycopy(starts, from, substarts, 0, to - from);
		return substarts;
	}

	/**
	 * Retrieves some of the matches of a query
	 * 
	 * Not all the matches are returned, but only those between from and to. For
	 * example, getMatches(0,9) returns the first 10 matches while
	 * getMatches(0,getNMatches()) return them all.
	 * 
	 * @param from
	 *            the from
	 * @param to
	 *            the to
	 * 
	 * @return the matches
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	/**
	 * @return
	 * @throws CqiClientException
	 */
	@Override
	public List<Match> getMatches(int from, int to) throws CqiClientException {
		List<Match> res = new ArrayList<Match>();

		for (int i = 0; i <= to - from; i++) {
			if (targets == null) res.add(new Match(starts[i], ends[i]));
			else res.add(new Match(starts[i], ends[i], targets[i]));
		}
		return res;
	}

	@Override
	public int[] getEnds(int from, int to) throws UnexpectedAnswerException, IOException, CqiServerError {
		int[] subends = new int[to - from];
		System.arraycopy(starts, from, subends, 0, to - from);
		return subends;
	}

	@Override
	public int[] getEnds() throws CqiClientException, IOException, CqiServerError {
		return ends;
	}

	@Override
	public int[] getStarts(int from, int to) throws UnexpectedAnswerException, IOException, CqiServerError {
		int[] substarts = new int[to - from];
		System.arraycopy(starts, from, substarts, 0, to - from);
		return substarts;
	}

	@Override
	public int[] getStarts() throws CqiClientException, IOException, CqiServerError {
		return starts;
	}

	/**
	 * FAKE Drops the query results. More specifically, it drops its CQP counterpart.
	 * 
	 * This method has to be called when a query result is no longer usefull.
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	@Override
	public void drop() throws CqiClientException {
		// lol
	}

	@Override
	public void finalize() {
		// lol
	}

	/**
	 * get the Nth match
	 * 
	 * @param i
	 * @return
	 * @throws UnexpectedAnswerException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	@Override
	public Match getMatch(int i) throws UnexpectedAnswerException, IOException, CqiServerError {
		return new Match(starts[i], ends[i]);
	}
}
