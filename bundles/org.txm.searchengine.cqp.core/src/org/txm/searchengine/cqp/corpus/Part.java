// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus;

import java.io.File;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.searchengine.core.messages.SearchEngineCoreMessages;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * A Part is related to the {@link Partition} and is a CQP this.
 *
 * @author jmague
 */
public class Part extends Subcorpus {

	/** The number of texts composing this part. */
	private int nTexts = -1;

	/**
	 * 
	 * @param partition
	 */
	public Part(Partition partition, String partName, String query) {
		super(partition);

		this.setUserName(partName);
		this.pQuery = new CQLQuery(query);
		this.pID = CqpObject.partNamePrefix + CQPCorpus.getNextSubcorpusCounter();
		this.setVisible(false);
		this.synchronizedWithParent = true;
	}

	/**
	 * 
	 * @param parametersNodePath
	 */
	public Part(String parametersNodePath) {
		super(parametersNodePath);

		this.setVisible(false);
		this.synchronizedWithParent = true;
	}

	@Override
	public boolean loadParameters() throws Exception {
		return super.loadParameters();
	}

	@Override
	protected boolean __compute(TXMProgressMonitor monitor) throws Exception {

		try {
			super.__compute(monitor);
		}
		catch (Exception e) {
			try {
				throw new CqiClientException(
						TXMCoreMessages.failedToCreatePartColon + this.getParent().getName() + "_" + this.getName() + e + " last error: " + CQPSearchEngine.getCqiClient().getLastCQPError()); //$NON-NLS-1$ //$NON-NLS-2$
			}
			catch (Exception e1) {
				Log.severe(TXMCoreMessages.failedToGetLastCQPErrorColon + e1);
				org.txm.utils.logger.Log.printStackTrace(e1);
				return false;
			}
		}
		return true;
	}


	@Override
	public String getComputingStartMessage() {
		return NLS.bind(SearchEngineCoreMessages.info_computingPartP0WithQueryP1, this.getName(), this.pQuery);
	}



	//	public Part createPart(String partitionName, String partName, String query) throws CqiClientException {
	//		Part part;
	//		String partCqpId = CqpObject.partNamePrefix + CQPCorpus.getNextSubcorpusCounter();
	//		Log.finest(NLS.bind(TXMCoreMessages.CREATING_PART, partName, query));
	//		long start = System.currentTimeMillis();
	//		try {
	//			CorpusManager.getCorpusManager().getCqiClient().cqpQuery(this.getParent().getQualifiedCqpId(), partCqpId, query);
	//			part = new Part(this);
	//			part.setParameters(partCqpId, partName, new CQLQuery(query));
	//			part.compute();
	//			// FIXME: persistence tests: define the UUID as the concatenation of all part CQP id
	////			this.uniqueID += partCqpId;
	//
	//		} catch (Exception e) {
	//			try {
	//				throw new CqiClientException(TXMCoreMessages.failedToCreatePartColon + partitionName + "_" + partName + e + " last error: " + CQPSearchEngine.getCqiClient().getLastCQPError()); //$NON-NLS-1$ //$NON-NLS-2$
	//			} catch (Exception e1) {
	//				Log.severe(TXMCoreMessages.failedToGetLastCQPErrorColon + e1);
	//				org.txm.utils.logger.Log.printStackTrace(e1);
	//				return null;
	//			}
	//		}
	//		long end = System.currentTimeMillis();
	//		Log.finest(NLS.bind(TXMCoreMessages.info_partCreatedInXMs, partitionName + "_" + partName, (end - start))); //$NON-NLS-1
	//
	//		return part;
	//	}
	//	


	@Override
	public String getName() {
		return this.getSimpleName();
	}


	/**
	 * Gets the number of texts in this part.
	 * 
	 * @return the n texts
	 * 
	 * @throws CqiClientException the cqi client exception
	 */
	// FIXME: SJ: became useless? + wrong method name?
	public int getNTexts() throws CqiClientException {
		if (nTexts < 0) {
			try {
				nTexts = CorpusManager.getCorpusManager().getCqiClient().subCorpusSize(this.getQualifiedCqpId());
			}
			catch (Exception e) {
				throw new CqiClientException(e);
			}
		}
		return nTexts;
	}

	@Override
	@Deprecated
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		return this.toTxt(outfile, encoding, colseparator, txtseparator);
	}

	@Override
	public String getResultType() {
		return "Part"; //$NON-NLS-1$
	}
}
