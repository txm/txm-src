// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-03-01 10:10:58 +0100 (Tue, 01 Mar 2016) $
// $LastChangedRevision: 3133 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.searchengine.core.Selection;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.ICqiClient;
import org.txm.searchengine.cqp.MemCqiClient;
import org.txm.searchengine.cqp.NetCqiClient;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.InvalidCqpIdException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.serverException.CqiServerError;

/**
 * The result of a CQP query on a corpus.
 * 
 * @author jmague
 */
public class QueryResult extends Selection implements CqpObject {

	/** The cqp id. */
	protected String cqpId;

	/** The mother main corpus. */
	protected MainCorpus motherMainCorpus;

	/** The name. */
	protected String name;

	/** The n match. */
	protected int nMatch = -1;

	/** The queried corpus. */
	protected CQPCorpus queriedCorpus;

	/** The query. */
	protected CQLQuery query;

	/** The size. */
	protected int size = -1;

	/**
	 * Instantiates a new query result.
	 * 
	 * Not intended to be used directly. See {@link CQPCorpus#query(CQLQuery, String, boolean)}
	 * 
	 * @param cqpId the cqp id
	 * @param name the name
	 * @param queriedCorpus the queried corpus
	 * @param query the query
	 * 
	 * @throws InvalidCqpIdException the invalid cqp id exception
	 */
	public QueryResult(String cqpId, String name, CQPCorpus queriedCorpus, CQLQuery query) throws InvalidCqpIdException {

		// super(cqpId);
		checkCqpId(cqpId);
		this.cqpId = cqpId;
		this.name = name;
		this.queriedCorpus = queriedCorpus;
		if (queriedCorpus instanceof Subcorpus) {
			this.motherMainCorpus = ((Subcorpus) queriedCorpus).getMainCorpus();
		}
		else {
			this.motherMainCorpus = ((MainCorpus) queriedCorpus);
		}
		this.query = query;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.txm.searchengine.cqp.corpus.CqpObject#checkCqpId(java.lang
	 * .String)
	 */
	@Override
	public boolean checkCqpId(String cqpId) throws InvalidCqpIdException {

		if (!AbstractCqiClient.checkSubcorpusId(cqpId)) {
			throw new InvalidCqpIdException(cqpId + TXMCoreMessages.isNotAValidCQPIdForASubcorpusItMustBeAnUppercaseCharactersFollowedByLowercaseCharacters);
		}
		return true;
	}

	/**
	 * Drops the query results. More specifically, it drops its CQP counterpart.
	 * 
	 * This method has to be called when a query result is no longer usefull.
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	@Override
	public void drop() throws CqiClientException {

		try {
			CorpusManager.getCorpusManager().getCqiClient().dropSubCorpus(this.getQualifiedCqpId());
		}
		catch (Exception e) {
			throw new CqiClientException(e);
		}
	}

	@Override
	public void finalize() {
		// try { drop(); }
		// catch(Exception e) { }
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.searchengine.cqp.corpus.CqpObject#getCqpId()
	 */
	@Override
	public String getCqpId() {

		return cqpId;
	}

	public int[] getEnds() throws CqiClientException, IOException, CqiServerError {

		int n = this.getNMatch();
		if (n > 0) {
			return getEnds(0, n - 1);
		}
		else {
			return new int[0];
		}
	}

	public int[] getEnds(int from, int to) throws UnexpectedAnswerException, IOException, CqiServerError {

		return CorpusManager.getCorpusManager().getCqiClient()
				.dumpSubCorpus(getQualifiedCqpId(),
						NetCqiClient.CQI_CONST_FIELD_MATCHEND, from, to);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.searchengine.cqp.corpus.CqpObject#getMotherMainCorpus()
	 */
	@Override
	public MainCorpus getMainCorpus() {

		return motherMainCorpus;
	}

	/**
	 * get the Nth match
	 * 
	 * @param i
	 * @return
	 * @throws UnexpectedAnswerException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public Match getMatch(int i) throws UnexpectedAnswerException, IOException, CqiServerError {

		AbstractCqiClient cqi = CorpusManager.getCorpusManager().getCqiClient();

		int[] starts = cqi.dumpSubCorpus(getQualifiedCqpId(), NetCqiClient.CQI_CONST_FIELD_MATCH, i, i);

		int[] ends = cqi.dumpSubCorpus(getQualifiedCqpId(), NetCqiClient.CQI_CONST_FIELD_MATCHEND, i, i);

		return new Match(starts[0], ends[0]); // the starts&ends arrays should be of size 1
	}

	/**
	 * Gets all the matches.
	 * 
	 * WARNING: on win32 OS, this function may takes a long time if called with sockets
	 * 
	 * @return the matches
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	@Override
	public List<Match> getMatches() throws CqiClientException {

		int n = this.getNMatch();
		if (n > 0) {
			return getMatches(0, n - 1);
		}
		else {
			return new ArrayList<>();
		}
	}



	/**
	 * Retrieves some of the matches of a query
	 * 
	 * Not all the matches are returned, but only those between from and to. For
	 * example, getMatches(0,9) returns the first 10 matches while
	 * getMatches(0,getNMatches()) return them all.
	 * 
	 * @param from
	 *            the from
	 * @param to
	 *            the to
	 * 
	 * @return the matches
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	/**
	 * @return
	 * @throws CqiClientException
	 */
	@Override
	public List<Match> getMatches(int from, int to) throws CqiClientException {

		int[] starts, ends, targets;
		List<Match> res = new ArrayList<>();
		try {
			starts = getStarts(from, to);
			ends = getEnds(from, to);
			targets = getTargets(from, to);
		}
		catch (Exception e) {
			throw new CqiClientException(e);
		}
		for (int i = 0; i <= to - from; i++) {
			res.add(new Match(starts[i], ends[i], targets[i]));
		}
		return res;
	}

	private int[] getTargets(int from, int to) throws UnexpectedAnswerException, IOException, CqiServerError {

		return CorpusManager.getCorpusManager().getCqiClient()
				.dumpSubCorpus(getQualifiedCqpId(),
						NetCqiClient.CQI_CONST_FIELD_TARGET, from, to);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.searchengine.cqp.corpus.CqpObject#getName()
	 */
	@Override
	public String getName() {

		return name;
	}

	/**
	 * Gets the number of matches.
	 * 
	 * @return the number of matches
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	@Override
	public int getNMatch() throws CqiClientException {

		// if (nMatch == -1) //
		try {
			nMatch = CorpusManager.getCorpusManager().getCqiClient().subCorpusSize(getQualifiedCqpId());
		}
		catch (Exception e) {
			return 0;
		}
		return nMatch;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.txm.searchengine.cqp.corpus.CqpObject#getQualifiedCqpId()
	 */
	@Override
	public String getQualifiedCqpId() {

		return motherMainCorpus.getCqpId() + ":" + this.cqpId; //$NON-NLS-1$
	}

	/**
	 * Gets the queried corpus.
	 * 
	 * @return the queried corpus
	 */
	public CQPCorpus getQueriedCorpus() {

		return queriedCorpus;
	}

	// /**
	// * Gets all the matches.
	// * WARNING: on win32 OS, this function may takesQUETE a long time
	// *
	// * @return the matches
	// *
	// * @throws CqiClientException
	// * the cqi client exception
	// */
	// public List<Match> getRandomMatches(float perc) throws CqiClientException {
	//
	// ArrayList<Match> allmatches = new ArrayList<Match>(getMatches(0, getNMatch() - 1));
	// ArrayList<Match> sub = new ArrayList<Match>();
	// int count = 0;
	// for(Match m : allmatches)
	// {
	// if(count >= size)
	// return sub;
	// count++;
	// if(Math.random() > perc)
	// sub.add(m);
	// }
	// return sub;
	// }

	/**
	 * Gets the query.
	 * 
	 * @return the query
	 */
	@Override
	public CQLQuery getQuery() {

		return query;
	}

	/**
	 * Retrieves some of the matches of a query
	 * 
	 * Not all the matches are returned, but only those between from and to. For
	 * example, getMatches(0,9) returns the first 10 matches while
	 * getMatches(0,getNMatches()) return them all.
	 * 
	 * @param from
	 *            the from
	 * @param to
	 *            the to
	 * 
	 * @return the matches
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	/**
	 * @return
	 * @throws CqiClientException
	 */
	public int[] getStartMatches(int from, int to) throws CqiClientException {

		int n = this.getNMatch();
		if (to > n) to = n;
		try {
			return CorpusManager.getCorpusManager().getCqiClient()
					.dumpSubCorpus(getQualifiedCqpId(),
							NetCqiClient.CQI_CONST_FIELD_MATCH, from, to);
		}
		catch (Exception e) {
			throw new CqiClientException(e);
		}
	}

	@Override
	public int[] getStarts() throws CqiClientException, IOException, CqiServerError {

		int n = this.getNMatch();
		if (n > 0) {
			return getStarts(0, n - 1);
		}
		else {
			return new int[0];
		}
	}

	public int[] getStarts(int from, int to) throws UnexpectedAnswerException, IOException, CqiServerError {

		return CorpusManager.getCorpusManager().getCqiClient().dumpSubCorpus(getQualifiedCqpId(), NetCqiClient.CQI_CONST_FIELD_MATCH, from, to);
	}

	@Override
	public String toString() {

		return this.getQualifiedCqpId();
	}

	@Override
	public boolean isTargetUsed() throws UnexpectedAnswerException, IOException, CqiServerError {

		return CQPSearchEngine.getCqiClient().subCorpusHasField(this.getQualifiedCqpId(), ICqiClient.CQI_CONST_FIELD_TARGET);
	}

	@Override
	public boolean delete(int iMatch) {

		AbstractCqiClient CQI = CorpusManager.getCorpusManager().getCqiClient();
		if (CQI instanceof MemCqiClient) {
			MemCqiClient MCQI = (MemCqiClient) CQI;

			String query = "delete " + this.getQualifiedCqpId() + " " + iMatch + ";"; //$NON-NLS-1$
			try {
				MCQI.query(query);
				size = -1;
				nMatch = -1;
				return true;
			}
			catch (UnexpectedAnswerException | IOException | CqiServerError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public boolean delete(org.txm.objects.Match match) {

		AbstractCqiClient CQI = CorpusManager.getCorpusManager().getCqiClient();
		if (CQI instanceof MemCqiClient) {
			try {
				MemCqiClient MCQI = (MemCqiClient) CQI;

				Match m2;
				List<? extends Match> allMatches = getMatches();
				int to = allMatches.size();
				for (int i = 0; i < to; i++) {
					m2 = allMatches.get(i);
					if (m2.getStart() == match.getStart() && m2.getEnd() == match.getEnd()) {
						String query = "delete " + this.getQualifiedCqpId() + " " + i + ";"; //$NON-NLS-1$

						MCQI.query(query);

						size = -1;
						nMatch = -1;
						return true;
					}
				}
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return false;
	}

	@Override
	public boolean delete(ArrayList<org.txm.objects.Match> matchesToRemove) {

		AbstractCqiClient CQI = CorpusManager.getCorpusManager().getCqiClient();
		List<? extends Match> allMatches;
		try {
			allMatches = getMatches();

			int to = allMatches.size();
			int c = 0;
			if (CQI instanceof MemCqiClient) {
				MemCqiClient MCQI = (MemCqiClient) CQI;
				for (org.txm.objects.Match m : matchesToRemove) {
					Match m2;

					for (int i = 0; i < to; i++) {// TODO may be a bit faster since allMatches&matchesToRemove are ordered
						m2 = allMatches.get(i);
						if (m2.getStart() == m.getStart() && m2.getEnd() == m.getEnd()) {
							String query = "delete " + this.getQualifiedCqpId() + " " + (i) + ";"; //$NON-NLS-1$
							MCQI.query(query);
							c++;
							break; // one match per line
						}
					}
				}
				return c > 0;
			}
		}
		catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return false;
	}
}
