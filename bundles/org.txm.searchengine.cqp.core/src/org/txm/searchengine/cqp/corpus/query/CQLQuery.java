// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-05-13 09:46:56 +0200 (Wed, 13 May 2015) $
// $LastChangedRevision: 2967 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus.query;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.txm.libs.cqp.CQPLibPreferences;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.SearchEngine;
import org.txm.searchengine.core.SearchEnginesManager;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.tokenizer.SimpleStringTokenizer;
import org.txm.utils.PatternUtils;

/**
 * Implements a CQL query. Use fixQuery to use query sugar.
 * 
 * @author sloiseau
 * @author jpmague
 * @author sheiden
 * @author sjacquot
 */

public class CQLQuery extends Query {

	/** The Constant NoStrategy. */
	private static final Integer NoStrategy = 0;

	/** The Constant JPMStrategy. */
	private static final Integer JPMStrategy = 1;

	/** The Constant WLXStrategy. */
	private static final Integer WLXStrategy = 2;

	/** The Constant ElectricStrategy. */
	private static final Integer ElectricStrategy = 3;

	public static final int MAX_CQL_LENGTH = 16000; // current CQL length limit is set to ~16000. The corpus name will affect the query limit length


	/** espace char |"{()}+*. */
	private static String chars = "^$<>|\\\"{([])}?+*."; //$NON-NLS-1$
	// ^ $ \ . * + ? ( ) [ ] { } | 


	public CQLQuery() {
		super("", CQPSearchEngine.getEngine());
	}

	//	/**
	//	 * The query string.
	//	 */
	//	protected String queryString = "";

	/**
	 * Instantiates a new query.
	 * 
	 * @param queryString
	 *            the query string
	 * 
	 *            public Query(String queryString){
	 *            this.queryString=queryString.trim(); }
	 */
	public CQLQuery(String queryString) {
		super(queryString, CQPSearchEngine.getEngine());
		if (queryString != null) {
			this.queryString = queryString.trim();
		}
		else {
			this.queryString = ""; //$NON-NLS-1$
		}
	}

	/**
	 * Creates a readable formated string from the query.
	 * 
	 * @return
	 */
	@Override
	public String asString() {
		return "<" + this.queryString + ">"; //$NON-NLS-1$ //$NON-NLS-2$
	}


	// FIXME: SJ: can we remove this method, is it used in real query?
	// I think it should not, toString() is more generally dedicated to log not to be used
	// The rename the above method asString() to toString()
	@Override
	public String toString() {
		return queryString;
	}


	/**
	 * Creates a readable formated string from the specified queries list.
	 * 
	 * @param queries
	 * @return
	 */
	public static String asString(List<CQLQuery> queries) {
		String queriesString = StringUtils.join(queries, ">, <"); //$NON-NLS-1$
		queriesString = "<" + queriesString + ">";  //$NON-NLS-1$ //$NON-NLS-2$
		return "[" + queriesString + "]";  //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Normalize white chars.
	 * 
	 * SLH algorithm :
	 * - first normalize white chars : We don't know from where the query
	 * comes from and how it was produced (pasted from a text editor after
	 * having been double-clicked and copied, typed in the query input
	 * field, etc.) The CQP syntax analyzer is sensible to white chars, so
	 * we have to normalize what the user may have leaved inadvertently
	 * because she may not see well the true query string (white chars are
	 * invisible), and it is dif ficult to accept and manage a syntax error
	 * when you can't "see" the source of the problem. That strategy decides
	 * to interpret every white chars contiguous sequence as one space
	 * character.
	 * 
	 * Please note that this may be compatible (or not) with other Search
	 * Engines query scanners.
	 * 
	 * Please note also that this may be compatible (or not) with the
	 * tokenization policy of a given corpus dealing with compound words.
	 * That strategy is compatible with a policy which separates words in
	 * compound words by one space character.
	 * 
	 * - then remove outer white chars : What is not seen should not be
	 * there if no explicit expression deals with it from the user's point
	 * of view. So white chars at the beginning or at the end of the query
	 * are removed. Those characters often come from rapid copy/paste or
	 * double-click/copy/paste (in a word processing software, the
	 * double-click strategy can for example select the white chars on the
	 * right of the selected strings to help the user move words around with
	 * their delimiters).
	 * 
	 * That strategy may seem aggressive, but experience shows that this
	 * little service helps silently users a lot and is rarely
	 * inappropriate.
	 * 
	 * In case of doubt, the user should be able to switch to another
	 * strategy.
	 *
	 * @param str the str
	 * @return the string with white chars normalized
	 */
	public static String normalizeWhiteChars(String str) {

		// normalize everything white to space
		String str1 = str.replaceAll("\\s+", " "); //$NON-NLS-1$ //$NON-NLS-2$
		// remove border whites
		str1 = str1.trim(); //$NON-NLS-1$ //$NON-NLS-2$
		return str1;
	}

	/**
	 * Transforms a simplified query string into a proper CQP query string
	 * 
	 * Follows several different strategies to do that :
	 * 
	 * JPM Strategy : if the input query matches \w quote are added if the input
	 * query matches \w*="\w*", square brackets are added if the input query
	 * matches .\w=.\w (i.e. without quotes), quotes and square brackets are
	 * added (i.e. -> [$1="$2"]) otherwise, the query is left as if
	 * 
	 * Weblex Strategy : normalize white chars add double quotes if no double
	 * quotes and no brackets
	 * 
	 * Electric Strategy : normalize white chars add double quote, equal,
	 * brackets, etc. depending on the presence/absence of those chars in the
	 * original query
	 * 
	 * No Strategy : do nothing
	 * 
	 * @param query
	 *            The query string to fix
	 * @return the fixed query string
	 */
	static public String fixQuery(String query, String lang) {

		if ("\"".equals(query)) return "\"\\\"\""; //$NON-NLS-1$ //$NON-NLS-2$

		Integer QueryFixStrategy = WLXStrategy;

		if (QueryFixStrategy == JPMStrategy) {

			// query = abc -> put double quotes
			Pattern simple = Pattern.compile("\\p{L}*"); //$NON-NLS-1$
			if (simple.matcher(query).matches()) {
				// System.out.println("match 1");
				return '"' + StringUtils.join(query.split(" "), "\" \"") + '"'; // $NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}

			// query = prop="abc" -> put square brackets
			Pattern equalsWithQuotes = Pattern.compile("\\p{L}+=\"\\p{L}*\""); //$NON-NLS-1$
			if (equalsWithQuotes.matcher(query).matches()) {
				// System.out.println("match 2");
				return '[' + query + ']';
			}

			// query = abc=def -> put brackets + equal sign + double quotes
			Pattern equalsWithoutQuotes = Pattern.compile("(\\p{L}*)=(\\p{L}*)"); //$NON-NLS-1$
			Matcher matcher = equalsWithoutQuotes.matcher(query);
			if (matcher.matches()) {
				// System.out.println("match 2");
				return '[' + matcher.group(1) + '=' + '"' + matcher.group(2) + '"' + ']'; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}

			// query doesn't start with ", ' and [ -> put double quotes
			if (!query.startsWith("\"") && !query.startsWith("\'") && !query.startsWith("[")) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			{
				return '"' + StringUtils.join(query.split(" "), "\" \"") + '"'; // $NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}

			// do nothing
			return query;
		}
		else if (QueryFixStrategy == WLXStrategy) {

			// empty query
			if (query.isEmpty()) {
				return query;
			}
			// query contains double quotes
			else if (query.contains("\"")) { //$NON-NLS-1$
				return query;
			}
			// query contains brackets
			else if (query.contains("[") && query.contains("]")) { //$NON-NLS-1$ //$NON-NLS-2$
				return query; // probably [] or [...]
			}
			else {
				if (CQPLibPreferences.getInstance().getBoolean(CQPLibPreferences.QUERY_FIX_TOKENIZE)) {
					// TODO set the tokenizer rules to not break the regex expressions like "j.+"
					List<List<String>> sentences = new SimpleStringTokenizer(lang).processText(query);
					StringBuilder buffer = new StringBuilder();
					for (List<String> sent : sentences) {
						for (String word : sent) {
							buffer.append(" \"" + addBackSlash(word) + "\""); //$NON-NLS-1$ //$NON-NLS-2$
						}
					}
					return buffer.substring(1);
				}
				else {
					return '"' + query + '"'; // $NON-NLS-1$
				}


				//				query = normalizeWhiteChars(query);
				//				return '"' + StringUtils.join(query.split(" "), "\" \"") + '"'; // $NON-NLS-1$
			}
		}
		else if (QueryFixStrategy == ElectricStrategy) {

			// DON'T USE THAT STRATEGY : CODE NEEDS TO BE FIXED
			query = normalizeWhiteChars(query);

			if (query.contains("\"")) { //$NON-NLS-1$
				if (query.contains("=")) { //$NON-NLS-1$
					if (query.contains("[")) { //$NON-NLS-1$
						return query;
					}
					else {
						return '[' + query + ']';
					}
				}
				else {
					return query;
				}
			}
			else {
				if (query.contains("=")) { //$NON-NLS-1$
					return query.replaceAll("=([^= \"]+)", "=\"$1\""); //$NON-NLS-1$ //$NON-NLS-2$
				}
				else {
					return '"' + query + '"'; // $NON-NLS-1$
				}
			}
		}
		else if (QueryFixStrategy == NoStrategy) {

			// no strategy to apply : do nothing
			return query;

		}
		else {

			// we shouldn't be there
			// let silently return the query for the moment
			return query;
		}
	}



	/**
	 * Adds the back slash.
	 *
	 * @param squery the squery
	 * @return the string
	 */
	public static String addBackSlash(String squery) {
		if ("\"".equals(squery)) return "\\\""; //$NON-NLS-1$ //$NON-NLS-2$

		//		return Pattern.quote(squery);

		return PatternUtils.quote(squery, chars);
	}


	/**
	 * Sets the string of the query.
	 * 
	 * @param queryString
	 */
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}

	/**
	 * Checks whatever the query is empty (null, empty or contains only double quotes) or not.
	 * 
	 * @return true if empty otherwise false
	 */
	@Override
	public boolean isEmpty() {
		return queryString == null || queryString.length() == 0 || "\"\"".equals(queryString); //$NON-NLS-1$
	}


	//	@Override
	//	public boolean equals(Object obj) {
	//		if (obj == null) return false;
	//		if (!(obj instanceof CQLQuery)) {
	//			return false;
	//		}
	//		if (this.queryString == null) return false;
	//		
	//		return this.queryString.equals(((CQLQuery) obj).queryString);
	//	}
	//	


	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		String test = "^$sdf<>sdf|\\sdf\"{(sdf)}?+sdf*."; //$NON-NLS-1$
		System.out.println(CQLQuery.addBackSlash(test));
	}

	@Override
	public SearchEngine getSearchEngine() {
		return SearchEnginesManager.getCQPSearchEngine();
	}

	@Override
	public IQuery setQuery(String rawQuery) {
		this.queryString = rawQuery;
		return this;
	}

	@Override
	public IQuery fixQuery(String lang) {
		this.queryString = fixQuery(this.queryString, lang);
		return this;
	}

	/**
	 * 
	 * @return the CQL matching strategy for this query only. see the CQL 'MatchingStrategy' option
	 */
	public String getMatchingStrategy() {
		return getOption("MatchingStrategy"); //$NON-NLS-1$
	}

	/**
	 * 
	 * @return the property which don't needs brackets. see the CQL 'DefaultNonbrackAttr' option
	 */
	public String getDefaultNonbrackAttr() {
		return getOption("DefaultNonbrackAttr"); //$NON-NLS-1$
	}
}
