package org.txm.searchengine.cqp.corpus;

import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

import org.txm.searchengine.cqp.CQPSearchEngine;

/**
 * A CQP property at token/word level
 * 
 * NOT IMPLEMENTED YET: the actual codes needs to accees
 * 
 * @author mdecorde
 *
 */
public class LengthProperty extends VirtualProperty {

	/**
	 * Creates a new WordProperty.
	 * 
	 * @param corpus
	 */
	public LengthProperty(CQPCorpus corpus) {
		super("length", corpus); //$NON-NLS-1$
	}

	@Override
	public String cpos2Str(int position) {

		try {
			String[] rez = CQPSearchEngine.getCqiClient().cpos2Str(corpus.getWordProperty().getQualifiedName(), new int[] { position });
			return Integer.toString(rez[0].length());
		}
		catch (Exception e) {
			return "0"; //$NON-NLS-1$
		}
	}

	public List<String> getValues(int[] positions) {
		return Arrays.asList(cpos2Str(positions));
	}

	@Override
	public String[] cpos2Str(int[] positions) {
		try {
			String[] rez = CQPSearchEngine.getCqiClient().cpos2Str(corpus.getWordProperty().getQualifiedName(), positions);
			for (int i = 0; i < rez.length; i++) {
				rez[i] = Integer.toString(rez[i].length());
			}
			return rez;
		}
		catch (Exception e) {
			return new String[positions.length];
		}
	}

	@Override
	public int[] cpos2Id(int[] positions) {
		try {
			String[] tmp = CQPSearchEngine.getCqiClient().cpos2Str(corpus.getWordProperty().getQualifiedName(), positions);
			int[] rez = new int[tmp.length];
			for (int i = 0; i < rez.length; i++) {
				rez[i] = tmp[i].length();
			}
			return rez;
		}
		catch (Exception e) {
			return new int[positions.length];
		}
	}

	@Override
	public int cpos2Id(int position) {
		return cpos2Id(new int[] { position })[0];
	}

	@Override
	public String[] id2Str(int[] indices) {
		String[] rez = new String[indices.length];
		for (int i = 0; i < rez.length; i++) {
			rez[i] = Integer.toString(indices[i]);
		}
		return rez;
	}

	@Override
	public String id2Str(int indice) {
		return Integer.toString(indice);
	}

	@Override
	public String getCQLTest(String value) {
		try {
			int l = Integer.parseInt(value);
			StringBuilder sb = new StringBuilder();
			sb.append("word=\""); //$NON-NLS-1$
			for (int i = 0; i < l; i++) {
				sb.append("."); //$NON-NLS-1$
			}
			sb.append("\""); //$NON-NLS-1$
			return sb.toString();
		}
		catch (Exception e) {

		}
		return null;
	}

	@Override
	public String getCQLTest(List<String> values) {
		try {
			TreeSet<String> hash = new TreeSet<>();
			StringBuilder sb = new StringBuilder();
			for (String value : values) {
				int l = Integer.parseInt(value);

				sb.setLength(0);
				for (int i = 0; i < l; i++) {
					sb.append("."); //$NON-NLS-1$
				}
				hash.add(sb.toString());
			}

			sb.setLength(0);
			sb.append("word=\""); //$NON-NLS-1$
			for (String dots : hash) {
				sb.append(dots);
			}
			sb.append("\""); //$NON-NLS-1$
			return sb.toString();
		}
		catch (Exception e) {

		}
		return null;
	}
}

