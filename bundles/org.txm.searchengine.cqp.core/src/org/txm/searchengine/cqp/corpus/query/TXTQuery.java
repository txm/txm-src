package org.txm.searchengine.cqp.corpus.query;

import java.util.Locale;

import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.cqp.TXTSearchEngine;

public class TXTQuery extends Query {

	public TXTQuery(String query) {
		super(query, TXTSearchEngine.getEngine());
	}


	public IQuery fixQuery() {
		return fixQuery(Locale.getDefault().getCountry());
	}

	@Override
	public IQuery fixQuery(String lang) {
		// SimpleStringTokenizer tokenizer = new SimpleStringTokenizer(lang); // TODO setup the tokenizer to not create tokens * and ? when they are next to letters
		StringBuilder buffer = new StringBuilder();
		String[] rs = queryString.split(" "); //$NON-NLS-1$
		for (int i = 0; i < rs.length; i++) {
			String r = rs[i];
			if (i > 0) buffer.append(" "); //$NON-NLS-1$
			r = CQLQuery.addBackSlash(r);
			buffer.append("\"" + r.replace("\\*", ".*") + "\"%cd"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		System.out.println("CQL QUERY=" + buffer.toString()); //$NON-NLS-1$
		return new CQLQuery(buffer.toString());
	}
}
