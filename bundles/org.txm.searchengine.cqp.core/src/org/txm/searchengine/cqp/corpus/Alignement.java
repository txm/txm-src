package org.txm.searchengine.cqp.corpus;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.txm.objects.Edition;
import org.txm.objects.Page;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.InvalidCqpIdException;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.serverException.CqiServerError;

public class Alignement {

	protected String from;

	protected String to;

	protected String struct;

	protected int level;

	public Alignement(String from, String to, String struct, int level) {
		this.from = from;
		this.to = to;
		this.struct = struct;
		this.level = level;
		if (level > 1)
			this.struct = this.struct + level;
	}

	public Alignement(String from, String to) {
		this(from, to, "w", 1); //$NON-NLS-1$
	}

	public Alignement(String from, String to, String struct) {
		this(from, to, struct, 1);
	}

	/**
	 * 
	 * @param wordid
	 * @param textid
	 * @return the word id and text id of the [to] corpus.
	 * @throws CqiClientException
	 * @throws InvalidCqpIdException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public List<String> resolve(String wordid, String textid) throws CqiClientException, InvalidCqpIdException, IOException, CqiServerError {
		System.out.println("resolve " + wordid + " " + textid + " " + struct); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		CQPCorpus src = CorpusManager.getCorpusManager().getCorpus(from);
		StructuralUnitProperty align_src = src.getStructuralUnit(struct).getProperty("align");//$NON-NLS-1$ 

		CQPCorpus dest = CorpusManager.getCorpusManager().getCorpus(to);
		Property id_dest = dest.getProperty("id");//$NON-NLS-1$ 
		StructuralUnitProperty text_dest = dest.getStructuralUnit("text").getProperty("id"); //$NON-NLS-1$ //$NON-NLS-2$ 

		if (struct.equals("w")) { //$NON-NLS-1$
			QueryResult rez = src.query(new CQLQuery("[_.text_id=\"" + CQLQuery.addBackSlash(textid) + "\" & id=\"" + wordid + "\"]"), "Align", false); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			int[] position = { rez.getMatch(0).getStart() };
			int[] struct_align_pos = CQPSearchEngine.getCqiClient().cpos2Struc(align_src.getQualifiedName(), position);
			String[] struct_align_str = CQPSearchEngine.getCqiClient().struc2Str(align_src.getQualifiedName(), struct_align_pos);
			if (struct_align_str.length > 0) {
				String aligned_struct = struct_align_str[0]; // the struct align value
				// get the position of the first token of this struct
				QueryResult rez2 = dest.query(new CQLQuery("<" + struct + "_align=\"" + aligned_struct + "\">[]"), "Align", false); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				int[] position2 = { rez2.getMatch(0).getStart() };
				// get the id of this position
				int[] struct_text_pos = CQPSearchEngine.getCqiClient().cpos2Struc(text_dest.getQualifiedName(), position2);
				String[] struct_text_str = CQPSearchEngine.getCqiClient().struc2Str(text_dest.getQualifiedName(), struct_text_pos);
				return Arrays.asList(wordid, struct_text_str[0]);
			}
			else {
				return null;
			}
		}
		else {
			//get the position for the word id and the text
			String query = "[_.text_id=\"" + CQLQuery.addBackSlash(textid) + "\" & id=\"" + wordid + "\"]"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			//System.out.println("query: "+query);
			QueryResult rez = src.query(new CQLQuery(query), "Align", false); //$NON-NLS-1$
			//System.out.println(rez.getMatches());
			int[] position = { rez.getMatch(0).getStart() };
			//get the struct@align value for this position
			int[] struct_align_pos = CQPSearchEngine.getCqiClient().cpos2Struc(align_src.getQualifiedName(), position);
			String[] struct_align_str = CQPSearchEngine.getCqiClient().struc2Str(align_src.getQualifiedName(), struct_align_pos);
			if (struct_align_str.length > 0) {
				String aligned_struct = struct_align_str[0]; // the struct align value
				// get the position of the first token of this struct
				query = "<" + struct + "_align=\"" + aligned_struct + "\">[]"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				QueryResult rez2 = dest.query(new CQLQuery(query), "Align", false); //$NON-NLS-1$
				int[] position2 = { rez2.getMatch(0).getStart() };
				// get the id of this position
				String[] ids = CQPSearchEngine.getCqiClient().cpos2Str(id_dest.getQualifiedName(), position2);

				if (ids.length > 0) {
					int[] struct_text_pos = CQPSearchEngine.getCqiClient().cpos2Struc(text_dest.getQualifiedName(), position2);
					String[] struct_text_str = CQPSearchEngine.getCqiClient().struc2Str(text_dest.getQualifiedName(), struct_text_pos);
					return Arrays.asList(ids[0], struct_text_str[0]);
				}
				else
					return null;
			}
			else {
				return null;
			}
		}
	}

	public List<String> resolve(Page p) throws CqiClientException, InvalidCqpIdException, IOException, CqiServerError {
//		System.out.println("resolving " + p);
		return resolve(p.getWordId(), p.getEdition().getText().getName());
	}

	public List<String> resolve(Match match) throws IOException, CqiServerError, CqiClientException, InvalidCqpIdException {
		return resolve(match.getStart());
	}

	/**
	 * resolve alignemnt by a position:
	 * 1) get seg
	 * 2) resolve seg
	 * 
	 * @param idx
	 * @return
	 * @throws IOException
	 * @throws CqiServerError
	 * @throws CqiClientException
	 * @throws InvalidCqpIdException
	 */
	public List<String> resolve(int idx) throws IOException, CqiServerError, CqiClientException, InvalidCqpIdException {
		CQPCorpus src = CorpusManager.getCorpusManager().getCorpus(from);
		StructuralUnitProperty align_src = src.getStructuralUnit(struct).getProperty("align"); //$NON-NLS-1$
		int[] position = { idx };
		int[] struct_align_pos = CQPSearchEngine.getCqiClient().cpos2Struc(align_src.getQualifiedName(), position);
		String[] struct_align_str = CQPSearchEngine.getCqiClient().struc2Str(align_src.getQualifiedName(), struct_align_pos);
		if (struct_align_str.length > 0) {
			return resolveSeg(struct_align_str[0]);
		}
		return null;
	}


	public List<String> resolveSeg(String seg_id) throws IOException, CqiServerError, CqiClientException, InvalidCqpIdException {
		System.out.println("resolve seg " + seg_id); //$NON-NLS-1$
		CQPCorpus dest = CorpusManager.getCorpusManager().getCorpus(to);
		String query = "<" + struct + "_align=\"" + seg_id + "\">[]"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		//System.out.println("query: "+query);
		QueryResult rez = dest.query(new CQLQuery(query), "Align", false); //$NON-NLS-1$
		//System.out.println("rez: "+rez.getMatches());
		int[] position = { rez.getMatch(0).getStart() };

		Property id_dest = dest.getProperty("id"); //$NON-NLS-1$
		String[] ids = CQPSearchEngine.getCqiClient().cpos2Str(id_dest.getQualifiedName(), position);
		String wordid = ids[0];

		StructuralUnitProperty text_dest = dest.getStructuralUnit("text").getProperty("id"); //$NON-NLS-1$ //$NON-NLS-2$
		int[] struct_text_pos = CQPSearchEngine.getCqiClient().cpos2Struc(text_dest.getQualifiedName(), position);
		String[] struct_text_str = CQPSearchEngine.getCqiClient().struc2Str(text_dest.getQualifiedName(), struct_text_pos);
		String textid = struct_text_str[0];

		return Arrays.asList(wordid, textid);
	}

	public Page getPage(String edition, String wordid, String txtid) throws CqiClientException, InvalidCqpIdException {
		System.out.println("get edition: from " + to + " : " + edition + " " + wordid + " " + txtid); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		CQPCorpus dest = CorpusManager.getCorpusManager().getCorpus(to);
		Edition ed = dest.getProject().getText(txtid).getEdition(edition);
		if (ed == null)
			ed = dest.getProject().getText(txtid).getEdition("default"); //$NON-NLS-1$

		return ed.getPageForWordId(wordid);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + level;
		result = prime * result + ((struct == null) ? 0 : struct.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alignement other = (Alignement) obj;
		if (from == null) {
			if (other.from != null)
				return false;
		}
		else if (!from.equals(other.from))
			return false;
		if (level != other.level)
			return false;
		if (struct == null) {
			if (other.struct != null)
				return false;
		}
		else if (!struct.equals(other.struct))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		}
		else if (!to.equals(other.to))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Alignement [from=" + from + ", to=" + to + ", struct=" + struct + ", level=" + level + "]"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
	}

	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @return the to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * @return the struct
	 */
	public String getStruct() {
		return struct;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}
}
