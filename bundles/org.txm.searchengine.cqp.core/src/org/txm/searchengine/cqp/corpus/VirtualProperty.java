package org.txm.searchengine.cqp.corpus;

import java.util.List;

/**
 * A CQP property at token/word level
 * 
 * @author mdecorde
 *
 */
public abstract class VirtualProperty extends WordProperty {

	/**
	 * Creates a new WordProperty.
	 * 
	 * @param name
	 * @param corpus
	 */
	public VirtualProperty(String name, CQPCorpus corpus) {
		super(name, corpus);
	}

	@Override
	public String getFullName() {
		return "(" + getName() + ")"; //$NON-NLS-1$
	}

	@Override
	public abstract String cpos2Str(int position);

	@Override
	public abstract String[] cpos2Str(int[] positions);

	@Override
	public abstract int[] cpos2Id(int[] allpositionsarray);

	public abstract int cpos2Id(int position);

	@Override
	public abstract String[] id2Str(int[] indices);

	public abstract String id2Str(int indice);

	@Override
	public String toString() {
		return getFullName();
	}

	public static boolean isAVirtualPropertyFullName(String name) {
		return name != null && name.startsWith("(") && name.endsWith(")") && name.length() > 2;
	}

	public static String fullnameToName(String fullname) {
		return fullname.substring(1, fullname.length() - 1);
	}

	@Override
	public String getCQLTest(String value) {
		return null;
	}

	@Override
	public String getCQLTest(List<String> values) {
		return null;
	}
}

