// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-09-05 09:27:13 +0200 (jeu., 05 sept. 2013) $
// $LastChangedRevision: 2529 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.UUID;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.objects.Match;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.logger.Log;

/**
 * A Property on the structural units the corpus. Represents a
 * <strong>type</strong>, not a the value for a specific unit.
 * 
 * @author Jean-Philippe Magué
 */
public class StructuralUnitProperty extends Property {

	/** The values for this property corpus */
	List<String> values;

	/** The structural unit. */
	private StructuralUnit structuralUnit;

	/**
	 * Instantiates a new structural unit property.
	 * 
	 * Not intended to be used directly. See
	 *
	 * @param structuralUnit the structural unit
	 * @param name the name
	 * @param corpus the corpus {@link StructuralUnit#getProperty(String)}
	 */
	public StructuralUnitProperty(StructuralUnit structuralUnit, String name, CQPCorpus corpus) {

		super(name, corpus);
		this.structuralUnit = structuralUnit;
	}

	/**
	 * Gets the structural unit on wich the property is defined.
	 * 
	 * @return the structural unit
	 */
	public StructuralUnit getStructuralUnit() {

		return structuralUnit;
	}

	/**
	 * Gets the full name: "struct_propname"
	 *
	 * @return the full name
	 */
	@Override
	public String getFullName() {

		return structuralUnit.getName() + "_" + this.getName(); //$NON-NLS-1$
	}


	/**
	 * Gets the full name as string: "structure@property".
	 *
	 * @return the full name
	 */
	public String asFullNameString() {

		return this.structuralUnit.getName() + this.asString();
	}


	/**
	 * Creates a readable formated string from the specified structural unit properties list.
	 * 
	 * @param properties
	 * @return
	 */
	public static String asFullNameString(List<StructuralUnitProperty> properties) {

		String str = ""; ///$NON-NLS-1$
		for (int i = 0; i < properties.size(); i++) {
			if (i > 0) {
				str += ", "; //$NON-NLS-1$
			}
			str += properties.get(i).asFullNameString();
		}
		return str;
	}


	/**
	 * Gets a list of all the values this structural property takes in the
	 * subcorpus.
	 *
	 * @param subcorpus The subcorpus in which the search is restricted
	 * @return a List of values
	 * @throws CqiClientException the cqi client exception
	 */
	public List<String> getValues(CQPCorpus subcorpus) throws CqiClientException {

		try {
			AbstractCqiClient cqiClient = CorpusManager.getCorpusManager().getCqiClient();
			return cqiClient.getData(this, subcorpus);
		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.failedToAccessStructuralPropertyValues
					+ this.getFullName() + " : " + e); //$NON-NLS-1$
		}
		return new ArrayList<>();

		/*
		 * //if (values == null) { QueryResult tmp = subcorpus.query(new
		 * Query("<"+this.getFullName()+">[]"), UUID.randomUUID().toString());
		 * //$NON-NLS-1$ //$NON-NLS-2$ List<Match> matches = tmp.getMatches();
		 * tmp.drop(); values = new ArrayList<String>(new
		 * HashSet<String>(Match.getValuesForProperty(this,matches))); } return
		 * new ArrayList<String>(values);
		 */
	}

	/**
	 * Gets a list of all the values this structural property takes in the
	 * subcorpus
	 * 
	 * return an abstract of values.
	 *
	 * @param subcorpus The subcorpus in which the search is restricted
	 * @param number the number
	 * @return a List of values
	 * @throws CqiClientException the cqi client exception
	 */
	public List<String> getValues(CQPCorpus subcorpus, int number) throws CqiClientException {

		try {
			AbstractCqiClient cqiClient = CorpusManager.getCorpusManager().getCqiClient();
			return cqiClient.getData(this, number);
		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.failedToAccessStructuralPropertyValues
					+ this.getFullName() + " : " + e); //$NON-NLS-1$
		}
		return new ArrayList<>();

		/*
		 * //if (values == null) { QueryResult tmp = subcorpus.query(new
		 * Query("<"+this.getFullName()+">[]"), UUID.randomUUID().toString());
		 * //$NON-NLS-1$ //$NON-NLS-2$ List<Match> matches = tmp.getMatches();
		 * tmp.drop(); values = new ArrayList<String>(new
		 * HashSet<String>(Match.getValuesForProperty(this,matches))); } return
		 * new ArrayList<String>(values);
		 */
	}

	/**
	 * Gets a list of all the values this structural property takes in the main
	 * corpus.
	 *
	 * @return a List of values
	 * @throws CqiClientException the cqi client exception
	 */
	public List<String> getValues() throws CqiClientException {

		return getValues(this.getCorpus());
	}

	/**
	 * Gets a list of all the values this structural property takes in the main
	 * corpus.
	 *
	 * @return a List of values
	 * @throws CqiClientException the cqi client exception
	 */
	public List<String> getOrderedValues() throws CqiClientException {

		List<String> values = getValues(this.getCorpus());
		Collections.sort(values);
		return values;
	}

	/**
	 * Gets a list of all the values this structural property takes in the main
	 * corpus.
	 *
	 * @param corpus the corpus
	 * @return a List of values
	 * @throws CqiClientException the cqi client exception
	 */
	public List<String> getOrderedValues(CQPCorpus corpus) throws CqiClientException {

		List<String> values = getValues(corpus);
		Collections.sort(values);
		return values;
	}

	/**
	 * Gets the number of values.
	 *
	 * @param corpus the corpus
	 * @return the number of values in this corpus, -1 if an error occured
	 */
	public Integer getNumberOfValues(CQPCorpus corpus) {

		try {
			QueryResult tmp = corpus.query(new CQLQuery("<" + this.getFullName() + ">[]"), UUID.randomUUID().toString(), false); //$NON-NLS-1$ //$NON-NLS-2$
			int ret = tmp.getNMatch();
			tmp.drop();
			return ret;
		}
		catch (CqiClientException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return -1;// error
	}

	/**
	 * Converts the specified string to a list of StructuralUnitProperty from the specified Corpus.
	 * 
	 * @param corpus
	 * @param str
	 * @return
	 */
	public static List<StructuralUnitProperty> stringToProperties(CQPCorpus corpus, String str) {

		ArrayList<StructuralUnitProperty> structs = new ArrayList<>();
		if (str == null || str.length() == 0) {
			return structs;
		}

		String[] split = str.split(TXMPreferences.LIST_SEPARATOR);
		for (String s : split) {
			String[] split2 = s.split("_", 2); //$NON-NLS-1$
			if (split2.length != 2) {
				continue;
			}
			try {
				StructuralUnit struc = corpus.getStructuralUnit(split2[0]);
				StructuralUnitProperty struc_p = struc.getProperty(split2[1]);
				if (struc_p != null) {
					structs.add(struc_p);
				}
			}
			catch (CqiClientException e) {
				System.out.println("Error: " + str + " structure property not found in " + corpus);
				Log.printStackTrace(e);
				return null;
			}

		}
		return structs;
	}


	/**
	 * Converts the specified string to a StructuralUnitProperty.
	 * 
	 * @param corpus
	 * @param str the String to 'deserialize'. Follows the structure_property syntax
	 * @return
	 * @throws CqiClientException
	 */
	public static StructuralUnitProperty stringToStructuralUnitProperty(CQPCorpus corpus, String str) throws CqiClientException {

		if (str == null) {
			return null;
		}

		String[] split = str.split("_", 2); //$NON-NLS-1$
		if (split.length == 2) {
			StructuralUnit su = corpus.getStructuralUnit(split[0]);
			if (su == null) {
				return null;
			}

			return su.getProperty(split[1]);
		}

		return null;
	}

	public String cpos2Str(int position) throws UnexpectedAnswerException, IOException, CqiServerError {

		AbstractCqiClient cqiClient = CorpusManager.getCorpusManager().getCqiClient();
		return cqiClient.getSingleData(this, new int[] { position }).get(0);
	}

	public String[] cpos2Str(int[] positions) throws UnexpectedAnswerException, IOException, CqiServerError {

		AbstractCqiClient cqiClient = CorpusManager.getCorpusManager().getCqiClient();
		return cqiClient.getSingleData(this, positions).toArray(new String[positions.length]);
	}

	/**
	 * Very slow
	 * 
	 * @return
	 * @throws IOException
	 * @throws CqiServerError
	 * @throws CqiClientException
	 */
	public int getNValues() throws IOException, CqiServerError, CqiClientException {

		return getValues().size();
	}

	/**
	 * Very slow
	 * 
	 * @return
	 * @throws IOException
	 * @throws CqiServerError
	 * @throws CqiClientException
	 */
	public String[] getValuesAsStrings() throws IOException, CqiServerError, CqiClientException {

		List<String> values = getValues();
		return values.toArray(new String[values.size()]);
	}

	@Override
	public int[] cpos2Id(int[] positions) throws Exception {

		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] id2Str(int[] positions) throws Exception {

		// TODO Auto-generated method stub
		return null;
	}

	public List<String> getFirstValues(CQPCorpus result, int pMaxPropertiesToDisplay) {

		AbstractCqiClient cqiClient = CorpusManager.getCorpusManager().getCqiClient();

		try {
			int nstructs = cqiClient.attributeSize(getQualifiedName());
			List<? extends Match> matches = result.getMatches();

			HashSet<Integer> toFetch = new HashSet<>();

			for (int i = 0; i < nstructs; i++) { // for all the structures
				int[] structCpos = cqiClient.struc2Cpos(getQualifiedName(), i);
				//System.out.println("N struct="+i+" cpos="+structCpos);

				for (Match m : matches) {
					if (m.getStart() <= structCpos[0] && structCpos[1] <= m.getEnd()) {
						toFetch.add(i);
						break;
					}
				}

				// int iMatch = 0;
				//				for (int icpos = 0 ; icpos < structCpos.length ; icpos++) { // test if a values is used by this corpus
				//					
				//					if (iMatch > matches.size()) break; // no more matches to test
				//					
				//					if (structCpos[icpos] < matches.get(iMatch).getStart()) {
				//						icpos++; // test with the next cpos
				//					} else if (structCpos[icpos] >= matches.get(iMatch).getEnd()) {
				//						iMatch++; // test with the next match
				//					} else {
				//						toFetch.add(i); // the cpos is between start and end
				//						break;
				//					}
				//				}
				//					int structs[] = cqiClient.cpos2Struc(getQualifiedName(), match.getStartEnd());
				//					String[] values = cqiClient.struc2Str(getQualifiedName(), structs);
				//					for (String v : values) {
				//						firstValues.add(v);
				//						if (pMaxPropertiesToDisplay > 0 && firstValues.size() == pMaxPropertiesToDisplay) return new ArrayList<>(firstValues);
				//					}



			}

			int[] structs = new int[toFetch.size()];
			int s = 0;
			for (int struct : toFetch) {
				structs[s] = struct;
				s++;
			}
			//Arrays.sort(structs);
			String[] values = cqiClient.struc2Str(getQualifiedName(), structs);
			LinkedHashSet<String> uniqSortedValues = new LinkedHashSet<>();
			for (String v : values) {
				uniqSortedValues.add(v);
				if (pMaxPropertiesToDisplay > 0 && uniqSortedValues.size() > pMaxPropertiesToDisplay) {
					break; // I have enough values
				}
			}
			return new ArrayList<>(uniqSortedValues);

		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ArrayList<>();
	}
}
