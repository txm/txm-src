package org.txm.searchengine.cqp.corpus;

import java.util.ArrayList;

import org.txm.objects.Edition;
import org.txm.objects.Page;
import org.txm.objects.Project;
import org.txm.objects.Text;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.io.IOUtils;

/**
 * Build "onthefly" HTML pages from the CQP indexes (words + structures)
 * 
 * Not working yet, this class should help to have big corpus editions
 * 
 * @author mdecorde
 *
 */
public class CQPEdition extends Edition {

	ArrayList<Page> cache = new ArrayList<>();

	int MAX = 20;

	public CQPEdition(Text text) {

		super(text);
	}

	public CQPEdition(String id) {

		super(id);
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {
		pPageNames.clear();
		pPageFirstWordIds.clear();
		cache.clear();

		Project p = getProject();
		MainCorpus corpus = p.getChildren(MainCorpus.class).get(0);
		String paginantionElementName = p.getEditionDefinition("cqp").getPageElement(); //$NON-NLS-1$
		StructuralUnit structure = corpus.getStructuralUnit(paginantionElementName);
		if (structure == null) {
			paginantionElementName = "p"; //$NON-NLS-1$
			structure = corpus.getStructuralUnit(paginantionElementName);
			if (structure == null) {
				paginantionElementName = "div"; //$NON-NLS-1$
				structure = corpus.getStructuralUnit(paginantionElementName);
				if (structure == null) {
					paginantionElementName = "text"; //$NON-NLS-1$
				}
			}
		}
		QueryResult qr = corpus.query(new CQLQuery("<" + paginantionElementName + ">[]"), "TMP", false); //$NON-NLS-1$

		String[] ids = corpus.getProperty("id").cpos2Str(qr.getStarts()); //$NON-NLS-1$
		int nPages = qr.size;

		for (int i = 0; i <= nPages; i++) {
			pPageNames.add("" + i);
			pPageFirstWordIds.add(ids[i]);
		}
		qr.drop(); // no more usefull
		return nPages > 0;
	}

	/**
	 * Gets the page.
	 *
	 * @param n the no of the page to get
	 * @return the page
	 */
	@Override
	public Page getPage(int n) {

		if (n >= 0 && pPageNames.size() > n) {

			String wId = "w_0"; // default word id //$NON-NLS-1$
			if (pPageFirstWordIds.size() > n) {
				wId = pPageFirstWordIds.get(n);
			}
			Page p = new Page(this, pPageNames.get(n), wId, n);
			if (cache.contains(p)) {

				return p; // html file is alredy built
			}
			p.getFile().getParentFile().mkdirs();
			IOUtils.write(p.getFile(), "<html><body>TODO n=" + n + "w=" + wId + "</body></html>"); //$NON-NLS-1$s

			cache.add(p);
			if (cache.size() > MAX) {
				Page toClean = cache.remove(0);
				toClean.getFile().delete();
			}
			return p;
		}
		else {
			return null;
		}
	}
}
