package org.txm.searchengine.cqp.corpus;

/**
 * 
 * An object which can gives Partition parameters: NAME, PARTNAMES and QUERIES
 * 
 * @author mdecorde
 *
 */
public interface Partitionable {

	/**
	 * 
	 * @return the part names
	 */
	public String[] getNamesForPartition();

	/**
	 * 
	 * @return the part CQL queries
	 */
	public String[] getQueryStringsforPartition();

	/**
	 * 
	 * @return the new partition parent corpus
	 */
	public CQPCorpus getCorpus();

	/**
	 * 
	 * @return the new partition name
	 */
	public String getNameForPartition();

	/**
	 * 
	 * @return the Partitionable object name to display
	 */
	public String getSimpleName();

	/**
	 * ensure the Partitionable object is ready
	 */
	public boolean compute() throws Exception;
}
