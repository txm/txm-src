// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-08-30 09:45:56 +0200 (Tue, 30 Aug 2016) $
// $LastChangedRevision: 3283 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus;

import java.util.HashMap;

import org.txm.Toolbox;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Project;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.InvalidCqpIdException;
import org.txm.utils.logger.Log;

/**
 * The Class CorpusManager provide access through a CQi client to the corpora
 * managed by CQP.
 * 
 * The corpus manager is a singleton, and before the instance can be acceded via
 * {@link #getCorpusManager()} it has to configured with {@link #setCqiClient(AbstractCqiClient)}.
 * 
 * 
 * @author Jean-Philippe Magué
 */
public class CorpusManager {

	/** The cqi client. */
	private static AbstractCqiClient cqiClient;

	/** The singleton. */
	private static CorpusManager corpusManager = null;

	/** the corpora. */
	private static HashMap<String, MainCorpus> corpora = null;

	/**
	 * Instantiates a new corpus manager.
	 */
	private CorpusManager() {
		corpora = new HashMap<>();
	}

	/**
	 * Sets the cqi client.
	 * 
	 * @param cqiClient
	 *            the new cqi client
	 */
	public static void setCqiClient(AbstractCqiClient cqiClient) {
		CorpusManager.cqiClient = cqiClient;
	}

	/**
	 * Gets the corpus manager.
	 * 
	 * @return the corpus manager
	 */
	public static CorpusManager getCorpusManager() {
		// if (cqiClient == null)
		// throw new RuntimeException(TXMCoreMessages.cQiClientNotInitialized);
		if (corpusManager == null)
			corpusManager = new CorpusManager();
		return corpusManager;
	}

	/**
	 * Clean.
	 */
	public static void clean() {
		cqiClient = null;
		corpora = null;
		corpusManager = null;
	}

	/**
	 * Clean corpora : empty the 'corpora' member.
	 */
	public static void cleanCorpora() {
		corpora = new HashMap<>();
	}

	/**
	 * Gets a corpus.
	 *
	 * @param corpusName the corpus name
	 * @return the corpus
	 * @throws CqiClientException the cqi client exception
	 * @throws InvalidCqpIdException the invalid cqp id exception
	 */
	public MainCorpus getCorpus(String corpusName) throws CqiClientException, InvalidCqpIdException {

		if (corpora == null) {
			// System.out.println("error corpora is null ");
			this.getCorpora();
		}

		for (MainCorpus corpus : CorpusManager.corpora.values()) {
			if (corpus.getID().equals(corpusName)) {
				// System.out.println("found corpus "+corpusName);
				return corpus;
			}
		}
		// System.out.println("create corpus "+corpusName);
		for (Project p : Toolbox.workspace.getProjects()) {
			CorpusBuild c = p.getCorpusBuild(corpusName);
			if (c != null && c instanceof MainCorpus) {
				return (MainCorpus) c;
			}
		}
		return null;
	}

	/**
	 * Checks for corpus.
	 *
	 * @param name the name
	 * @return true, if successful
	 */
	public boolean hasCorpus(String name) {
		if (corpora == null)
			return false;
		for (MainCorpus c : corpora.values())
			if (name.equals(c.getID()))
				return true;
		return false;
	}

	/**
	 * Gets a list of the corpora managed by CQP.
	 * 
	 * @return the corpora
	 * 
	 */
	public HashMap<String, MainCorpus> getCorpora() {
		// //System.out.println("get corpora");
		// if (corpora != null)
		// return corpora;
		//
		// String[] corporaName;
		// try {
		// corporaName = cqiClient.listCorpora();
		// } catch (Exception e) {
		// throw new CqiClientException(e);
		// }
		//
		// corpora = new ArrayList<MainCorpus>(corporaName.length);
		// for (int i = 0; i < corporaName.length; i++) {
		// try {
		// MainCorpus c = this.getCorpus(corporaName[i]);
		// if (c != null) corpora.add(c);
		// } catch (InvalidCqpIdException e) {
		// throw new CqiClientException(TXMCoreMessages.errorColonThisExceptionShouldNotBeThrown, e);
		// }
		// }
		//
		// for (CQPCorpus corpus : corpora) {
		// try {
		// corpus.setCharset(cqiClient.corpusCharset(corpus.getCqpId()));
		// // TODO : implements cqiClient.corpusLanguage
		// // corpus.setLanguage(cqiClient.corpusLanguage(corpus.getCqpId()));
		// } catch (Exception e) {
		// org.txm.utils.logger.Log.printStackTrace(e);
		// }
		// }
		// // System.out.println("END creating corpora");
		return corpora;
	}

	/**
	 * Gets the cqi client.
	 * 
	 * @return the cqi client
	 */
	public AbstractCqiClient getCqiClient() {
		return cqiClient;
	}

	/**
	 * Delete corpus.
	 *
	 * @param corpus the corpus
	 */
	public void deleteCorpus(MainCorpus corpus) {
		if (corpora != null)
			corpora.remove(corpus.getID());
		if (CQPSearchEngine.isInitialized()) {
			try {
				CQPSearchEngine.getCqiClient().dropCorpus(corpus.getID());
			}
			catch (Exception e) {
				Log.warning("Warning while removing corpus: Failed to drop corpus " + corpus);
				Log.printStackTrace(e);
			}
		}
	}

	public boolean hasCorpus(MainCorpus mainCorpus) {
		if (corpora != null) {
			return corpora.containsKey(mainCorpus.getID());
		}
		return false;
	}

	public void add(MainCorpus mainCorpus) {
		if (corpora != null) {
			corpora.put(mainCorpus.getID(), mainCorpus);
		}
	}
}
