// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.searchengine.core.SearchEngine;
import org.txm.searchengine.core.SearchEngineProperty;
import org.txm.searchengine.core.SearchEnginesManager;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.utils.logger.Log;


/**
 * An abstract CQP corpus property. Represents a <strong>type</strong>, not a the value for a specific unit.
 * 
 * see WordProperty and StructuralUnitProperty form implementations
 * 
 * @author Jean-Philippe Magué, mdecorde
 */
public abstract class Property extends org.txm.searchengine.core.Property implements Comparable<Property>, SearchEngineProperty {

	/** The corpus. */
	protected CQPCorpus corpus;

	/**
	 * Instantiates a new property.
	 * 
	 * Not intended to be used directly. See {@link CQPCorpus#getProperty(String)}
	 * 
	 * @param name
	 *            the name
	 * @param corpus
	 *            the corpus
	 */
	protected Property(String name, CQPCorpus corpus) {
		super(name);
		this.corpus = corpus;
	}

	public SearchEngine getSearchEngine() {
		return SearchEnginesManager.getCQPSearchEngine();
	}

	/**
	 * Gets the qualified name = corpus name + "." + this.getFullName() .
	 * 
	 * @return the qualified name
	 */
	public String getQualifiedName() {
		return this.corpus.getMainCorpus().getCqpId() + "." + this.getFullName(); //$NON-NLS-1$
	}

	/**
	 * 
	 * @return the full name of the property without corpus reference
	 */
	@Override
	public abstract String getFullName();


	/**
	 * Gets the corpus over which the property is defined.
	 * 
	 * @return the name
	 */
	public CQPCorpus getCorpus() {
		return corpus;
	}

	/**
	 * Creates a readable formated string from the property.
	 * 
	 * @return
	 */
	public String asString() {
		return "@" + this.name; //$NON-NLS-1$
	}

	@Override
	// FIXME: SJ: can we remove this method, is it used in real query?
	// I think it should not, toString() is more generally dedicated to log not to be used
	// The rename the above method asString() to toString()
	public String toString() {
		return this.getName();
	}


	/**
	 * Equals.
	 *
	 * @param p the p
	 * @return true, if successful
	 */
	public boolean equals(Property p) {
		return this.getQualifiedName().equals(p.getQualifiedName());
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Property p) {
		return (this.getQualifiedName().compareTo((p).getQualifiedName()));
	}

	/**
	 * Converts the specified string to a list of Property from the specified Corpus.
	 * 
	 * @param corpus
	 * @param str the String to 'deserialize'. Each property is separated with 1 tabulation. Then calls fromString on each token
	 * @return a list of StructuralUnitProperty if the s param is well formed
	 */
	public static List<? extends Property> stringToProperties(CQPCorpus corpus, String str) {
		ArrayList<Property> properties = new ArrayList<>();

		if (str == null || corpus == null) {
			return properties;
		}

		String[] split = str.split(TXMPreferences.LIST_SEPARATOR);
		for (String s : split) {
			try {
				Property p = null;
				if (s.contains("_")) { //$NON-NLS-1$
					p = corpus.getStructuralUnitProperty(s); // manage all Property implementations : Word, StructuralUnits and Virtual
				}
				else {
					p = corpus.getProperty(s); // manage all Property implementations : Word, StructuralUnits and Virtual
				}


				if (p != null) {
					properties.add(p);
				}
			}
			catch (CqiClientException e) {
				Log.warning(TXMCoreMessages.bind("** Could not find the ''{0}'' word property found in the ''{1}'' corpus: {2}", s, corpus, e));
				Log.printStackTrace(e);
			}
		}
		return properties;
	}

	/**
	 * Converts the specified list of Property to a string.
	 * 
	 * @param pProperties the list of properties. May be null (return "")
	 * @return
	 */
	public static String propertiesToString(List<? extends Property> pProperties) {
		if (pProperties == null) {
			return ""; //$NON-NLS-1$
		}
		if (pProperties.size() == 0) {
			return ""; //$NON-NLS-1$
		}

		ArrayList<String> names = new ArrayList<>();

		for (Property p : pProperties) {
			names.add(p.getFullName());
		}

		return StringUtils.join(names, TXMPreferences.LIST_SEPARATOR);
	}
}
