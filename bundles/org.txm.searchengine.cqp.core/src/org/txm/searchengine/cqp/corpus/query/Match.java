// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-04-04 14:59:04 +0200 (Mon, 04 Apr 2016) $
// $LastChangedRevision: 3188 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;

/**
 * A Match of a query on a corpus = [start -> end]
 * 
 * @author jmague, mdecorde
 */
public class Match extends org.txm.objects.Match {

	/** The start. */
	protected int start;

	/** The end. */
	protected int end;

	/** The target position */
	protected int target = -1;

	/** The query result. */
	// private QueryResult queryResult;

	/**
	 * Instantiates a new match.
	 * 
	 * @param start
	 *            the cqp index of the start of the match
	 * @param end
	 *            the cqp index of the end of the match
	 */
	public Match(int start, int end) {
		this.start = start;
		this.end = end;
		// this.queryResult = queryResult;
	}

	public Match(int start, int end, int target) {
		this(start, end);
		this.target = target;
	}

	/**
	 * Gets the start.
	 * 
	 * @return the start
	 */
	@Override
	public int getStart() {
		return start;
	}

	/**
	 * Gets the start.
	 * 
	 * @return the start
	 */
	@Override
	public int getTarget() {
		return target;
	}

	/**
	 * Gets the end.
	 * 
	 * @return the end
	 */
	@Override
	public int getEnd() {
		return end;
	}

	// /**
	// * Gets the query result.
	// *
	// * @return the query result
	// */
	// public QueryResult getQueryResult() {
	// return queryResult;
	// }

	/**
	 * Gets the value for property for the first position of the match.
	 * 
	 * @param property
	 *            the property
	 * 
	 * @return the value for property
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public String getValueForProperty(Property property)
			throws CqiClientException {
		List<Match> match = new ArrayList<>();
		match.add(this);
		return Match.getValuesForProperty(property, match).get(0);
	}

	/**
	 * Gets the values for property for all the positions of the match.
	 * 
	 * @param property
	 *            the property
	 * 
	 * @return the value for property
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public List<String> getValuesForProperty(Property property)
			throws CqiClientException {
		int[] positions = new int[this.end - this.start + 1];
		for (int i = 0; i < this.end - this.start + 1; i++)
			positions[i] = this.start + i;
		return getValuesForProperty(property, positions);
	}

	/**
	 * Gets the values for property.
	 * 
	 * @param property
	 *            the property
	 * @param matches
	 *            the matches to get the values for
	 * 
	 * @return the values for property
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public static List<String> getValuesForProperty(Property property,
			List<? extends Match> matches) throws CqiClientException {
		int[] starts = new int[matches.size()];
		for (int i = 0; i < matches.size(); i++)
			starts[i] = matches.get(i).start;
		return getValuesForProperty(property, starts);
	}

	/**
	 * Gets the values for property.
	 * 
	 * @param property the property
	 * @param match the match to get the values for
	 * 
	 * @return the values for property
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public static List<String> getValuesForProperty(Property property, org.txm.objects.Match match) throws CqiClientException {
		int[] starts = new int[match.size()];
		for (int i = 0; i < match.size(); i++)
			starts[i] = match.getStart() + i;

		return getValuesForProperty(property, starts);
	}

	/**
	 * Gets the values for property.
	 * 
	 * @param property
	 *            the property
	 * @param positions
	 *            the positions to get the values for
	 * 
	 * @return the values for property
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public static List<String> getValuesForProperty(Property property, int[] positions) throws CqiClientException {

		AbstractCqiClient cqiClient = CorpusManager.getCorpusManager().getCqiClient();
		try {
			if (property instanceof StructuralUnitProperty) {
				int[] struc = cqiClient.cpos2Struc(property.getQualifiedName(), positions);
				return Arrays.asList(cqiClient.struc2Str(property.getQualifiedName(), struc));
			}
			else {
				return Arrays.asList(property.id2Str(property.cpos2Id(positions)));
			}
		}
		catch (Exception e) {
			throw new CqiClientException(e);
		}
	}

	/**
	 * Gets the values for property.
	 * 
	 * @param property the property
	 * 
	 * @return the values for property
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public static String getValueForProperty(Property property,
			int position) throws CqiClientException {
		AbstractCqiClient cqiClient = CorpusManager.getCorpusManager().getCqiClient();
		try {
			if (property instanceof StructuralUnitProperty) {
				int[] struc = cqiClient.cpos2Struc(property.getQualifiedName(),
						new int[] { position });
				return cqiClient.struc2Str(property
						.getQualifiedName(), struc)[0];
			}
			else {
				return cqiClient.cpos2Str(property
						.getQualifiedName(), new int[] { position })[0];
			}
		}
		catch (Exception e) {
			throw new CqiClientException(e);
		}
	}

	/**
	 * Gets the ids.
	 *
	 * @param property the property
	 * @param positions the positions
	 * @return the ids
	 * @throws CqiClientException the cqi client exception
	 */
	public int[] getIds(Property property, int[] positions)
			throws CqiClientException {
		AbstractCqiClient cqiClient = CorpusManager.getCorpusManager().getCqiClient();

		try {
			if (property instanceof StructuralUnitProperty) {
				int[] struc = cqiClient.cpos2Struc(property.getQualifiedName(),
						positions);
				return struc;
			}
			else {
				int[] ids = cqiClient.cpos2Id(property.getQualifiedName(),
						positions);
				return ids;
			}
		}
		catch (Exception e) {
			throw new CqiClientException(e);
		}
	}

	@Override
	public void setEnd(int p) {
		this.end = p;
	}

	@Override
	public void setStart(int p) {
		this.start = p;
	}

	@Override
	public void setTarget(int p) {
		this.target = p;
	}
}
