// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus.query;

// TODO: Auto-generated Javadoc
/**
 * The Class Occurrence.
 */
public class Occurrence {

	/** The position. */
	int position;

	/** The signature. */
	int[] signature;

	/** The signsum. */
	int signsum = 0;

	/**
	 * Instantiates a new occurrence.
	 *
	 * @param p the p
	 * @param props the props
	 */
	public Occurrence(int p, int[] props) {
		position = p;
		signature = props;
		for (int i = 0; i < signature.length; i++)
			signsum += signature[i];
	}

	/**
	 * Equals.
	 *
	 * @param o the o
	 * @return true, if successful
	 */
	public boolean equals(Occurrence o) {
		if (signature.length != o.signature.length)
			return false;
		if (signsum != o.signsum)
			return false;
		for (int i = 0; i < signature.length; i++)
			if (signature[i] != o.signature[i])
				return false;
		return true;
	}
}
