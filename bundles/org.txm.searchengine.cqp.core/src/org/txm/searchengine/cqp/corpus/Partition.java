// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-11-17 17:01:31 +0100 (Thu, 17 Nov 2016) $
// $LastChangedRevision: 3341 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.searchengine.core.messages.SearchEngineCoreMessages;
import org.txm.searchengine.cqp.CQPPreferences;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * A partition on a corpus. The partition is composed of {@link Part}s
 * *
 * Set names and queries to define the partition parts
 *
 * @author mdecorde
 * @author sjacquot
 * @author Jean-Philippe Magué
 * 
 */
public class Partition extends org.txm.objects.Partition {

	/** sum of parts size, stored for efficiency reasons **/
	int totalsize = -1;

	/**
	 * The parts queries
	 */
	@Parameter(key = TXMPreferences.QUERIES)
	protected List<String> pQueries;

	/**
	 * The parts names -> do the parts order
	 */
	@Parameter(key = CQPPreferences.PART_NAMES)
	protected List<String> pPartNames;


	@Override
	public String[] getExportTXTExtensions() {
		return new String[] { "*.csv", "*.txt", "*.groovy" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	/**
	 * 
	 * @param corpus MainCorpus or Subcorpus
	 */
	public Partition(CQPCorpus corpus) {
		super(corpus);
	}

	/**
	 * 
	 * @param parametersNodePath
	 */
	public Partition(String parametersNodePath) {
		super(parametersNodePath);
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		// create the parts only if they not already exist
		if (this.getPartsCount() == 0) {
			_compute_with_lists(monitor);
		}
		this.totalsize = -1; // refresh total size at next call of getTotalSize
		return true;
	}

	public TXMResult clone(boolean all) {
		Log.warning("Error: currently the partitions are not clonable.");
		return null;
	}

	/**
	 * Computes the Partition using a list of queries.
	 *
	 * @throws Exception
	 */
	public boolean _compute_with_lists(TXMProgressMonitor monitor) throws Exception {

		if (this.getUserName() == null || this.getUserName().length() == 0) {
			this.setUserName(""); //$NON-NLS-1$
		}

		Log.finest(NLS.bind(SearchEngineCoreMessages.info_creatingNewPartitionP0OfP1, this.getUserName(), this.getParent()));
		long start = System.currentTimeMillis();

		monitor.setTask("Building parts...");

		for (int i = 0; i < pQueries.size(); i++) {
			String queryS = pQueries.get(i);

			String partName = String.valueOf(i);
			if (pPartNames != null && i < pPartNames.size()) {
				partName = pPartNames.get(i);
			}
			// ensure the part has a name, if empty
			if (partName.trim().length() == 0) {
				partName = "-"; //$NON-NLS-1$
			}
			new Part(this, partName, queryS);

		}
		long end = System.currentTimeMillis();
		Log.fine(NLS.bind(SearchEngineCoreMessages.info_partitionP0CreatedInP1Ms, this.getUserName(), (end - start)));

		return pQueries.size() > 0;
	}

	@Override
	public boolean canCompute() {
		return this.getParent() != null && this.pQueries != null && this.pPartNames != null;
	}

	@Override
	public void clean() {
		// nothing to clean
	}

	@Override
	public int compareTo(TXMResult o) {
		// Compare node weights
		int output = super.compareTo(o);
		// Compare simple names if weights are equal
		if (output == 0) {
			output = this.getSimpleName().compareToIgnoreCase(o.getSimpleName());
		}
		return output;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.objects.TxmObject#load()
	 */
	protected boolean _load(Element e) {

		// partition already computed
		if (this.hasBeenComputedOnce()) {
			return true;
		}

		if (e != null && this.getParent() != null) {
			NodeList partList = e.getChildNodes();
			for (int i = 0; i < partList.getLength(); i++) {
				if (partList.item(i).getNodeType() == 1) {
					Element c = (Element) partList.item(i);
					if (c.getNodeName().equals("part")) { //$NON-NLS-1$
						String pname = c.getAttribute("name"); //$NON-NLS-1$
						String pshortname = c.getAttribute("shortname"); //$NON-NLS-1$
						String pquery = c.getAttribute("query"); //$NON-NLS-1$
						for (Part part : getParts()) {
							if (part.getName().equals(pname)) {
								part.setName(pname);
								part.setQuery(new CQLQuery(pquery));
							}
						}
					}
				}
			}
			// this.loadMetadata();
			return true;
		}

		return false;
	}


	@Override
	public String getDetails() {
		String name = this.getUserName();
		int partsCount = this.getPartsCount();

		if (partsCount > 0) {
			name += NLS.bind(" ({0})", partsCount);
		}

		return name;
	}

	@Override
	public String getSimpleName() {
		return this.getUserName();
	}

	@Override
	public String getName() {
		return this.getCorpus() + TXMPreferences.PARENT_NAME_SEPARATOR + this.getSimpleName();
	}

	@Override
	public String getComputingStartMessage() {
		return TXMCoreMessages.bind(SearchEngineCoreMessages.info_computingTheP0PartitionOfTheP1Corpus, this.getSimpleName(), this.getCorpus().getName());
	}


	@Override
	public String getComputingDoneMessage() {
		return TXMCoreMessages.bind(SearchEngineCoreMessages.info_doneP0Parts, this.getPartsCount());
	}

	/**
	 * Gets the number of parts.
	 * 
	 * @return the number of parts.
	 */
	public int getPartsCount() {
		return this.getParts().size();
	}


	/**
	 * Gets a part specified by its index.
	 * The index also represents the raw order in the children list.
	 * 
	 * @param index
	 * @return the ith part
	 */
	// FIXME: SJ: became useless?
	public Part getPart(int index) {
		return this.getParts().get(index);
	}

	/**
	 * Gets the name of the parts.
	 * 
	 * @return the part names
	 */
	// FIXME: SJ: later, do not cache the names => the names may have been modified by the user
	public List<String> getPartNames() {
		if (this.hasBeenComputedOnce()) {
			List<Part> parts = this.getParts();
			List<String> partNames = new ArrayList<>(parts.size());
			for (Subcorpus part : this.getParts()) {
				partNames.add(part.getName());
			}
			return partNames;
		}
		else {
			return this.pPartNames;
		}
	}

	/**
	 * Gets the children parts.
	 * 
	 * @return the parts
	 */
	@Override
	public List<Part> getParts() {
		return this.getChildren(Part.class);
	}

	/**
	 * Gets the part sizes as int array.
	 * 
	 * Do N CQi requests -> avoid calling multiple times this method
	 * 
	 * @return
	 * @throws CqiClientException
	 */
	public int[] getPartSizes() throws CqiClientException {
		int[] partSizes = new int[this.getParts().size()];
		for (int i = 0; i < getParts().size(); i++) {
			Part p = this.getParts().get(i);
			partSizes[i] = p.getSize();
		}
		return partSizes;
	}

	/**
	 * Gets the total of all part sizes.
	 * 
	 * @return
	 * @throws CqiClientException
	 */
	public int getTotalSize() throws CqiClientException {
		if (totalsize == -1) { // cached size
			totalsize = 0;
			for (Part p : getParts()) {
				totalsize += p.getSize();
			}
		}
		return totalsize;
	}

	/**
	 * Run a query on all the parts of this partition.
	 * 
	 * @param query the query
	 * @param name the name
	 * 
	 * @return the list "subcorpus"
	 * 
	 * @throws CqiClientException the cqi client exception
	 */
	public List<QueryResult> query(CQLQuery query, String name) throws CqiClientException {
		Log.finest(TXMCoreMessages.bind(TXMCoreMessages.queryingPartitionP0, this.getUserName()));
		List<Part> parts = getParts();
		List<QueryResult> results = new ArrayList<>(parts.size());
		for (Subcorpus part : parts) {
			results.add(part.query(query, part.getName() + "_" + name, false)); //$NON-NLS-1$
		}
		return results;
	}

	@Override
	public CQPCorpus getParent() {
		return (CQPCorpus) super.getParent();
	}

	/**
	 * Remove the specified <code>Part</code> from the partition.
	 * 
	 * @param p
	 * @throws CqiClientException
	 */
	public void removePart(Part p) throws CqiClientException {
		p.delete();
		totalsize = -1; // reset
	}

	@Override
	public String toString() {
		if (this.getUserName() != null && !this.getUserName().isEmpty()) {
			return this.getUserName();
		}
		return super.toString();
	}

	@Override
	public boolean _toTxt(File output, String encoding, String colseparator, String txtseparator) throws Exception {

		String s = null;
		if (output.getName().endsWith(".csv")) { //$NON-NLS-1$
			StringBuffer buffer = new StringBuffer();
			for (Part p : this.getParts()) {
				if (buffer.length() > 0) buffer.append("\n"); //$NON-NLS-1$
				buffer.append(p.getName() + "\t" + p.getQuery()); //$NON-NLS-1$
			}
			s = buffer.toString();
		}
		else if (output.getName().endsWith(".groovy")) { //$NON-NLS-1$
			StringBuffer buffer = new StringBuffer();
			buffer.append("def partnames = ["); //$NON-NLS-1$

			int i = 0;
			for (Part p : this.getParts()) {
				if (i++ > 0) buffer.append(", "); //$NON-NLS-1$
				buffer.append("\"" + p.getName() + "\""); //$NON-NLS-1$ //$NON-NLS-2$
			}
			buffer.append("]\n"); //$NON-NLS-1$
			buffer.append("def queries = ["); //$NON-NLS-1$
			i = 0;
			for (Part p : this.getParts()) {
				if (i++ > 0) buffer.append(", "); //$NON-NLS-1$
				buffer.append("\"" + p.getQuery() + "\""); //$NON-NLS-1$ //$NON-NLS-2$
			}
			buffer.append("]\n"); //$NON-NLS-1$

			buffer.append("def corpus = CorpusManager.getCorpus(\"" + this.getCorpus().getMainCorpus().getID() + "\")\n"); //$NON-NLS-1$ //$NON-NLS-2$
			buffer.append("def partition = new Partition(corpus);\n"); //$NON-NLS-1$
			buffer.append("partition.setParameters(\"" + this.getUserName() + "\", partnames, queries);\n"); //$NON-NLS-1$ //$NON-NLS-2$
			buffer.append("partition.compute();\n"); //$NON-NLS-1$

			s = buffer.toString();
		}
		else {
			StringBuffer buffer = new StringBuffer();
			for (Part p : this.getParts()) {
				if (buffer.length() > 0) buffer.append("\n"); //$NON-NLS-1$
				buffer.append(p.getName() + "=" + p.getQuery()); //$NON-NLS-1$
			}
			s = buffer.toString();
		}

		IOUtils.write(output, s);
		return output.exists();
	}

	public boolean setParameters(String name, List<String> queries, List<String> names) {
		this.setUserName(name);
		this.pQueries = queries;
		this.pPartNames = names;
		return true;
	}

	@Override
	public boolean loadParameters() {
		String tmp = null;

		tmp = this.getStringParameterValue(CQPPreferences.PART_NAMES);
		if (tmp != null) {
			this.pPartNames = new ArrayList<>(Arrays.asList(tmp.split("\t"))); //$NON-NLS-1$
		}
		else {
			this.pPartNames = null;
		}

		tmp = this.getStringParameterValue(TXMPreferences.QUERIES);
		if (tmp != null) {
			this.pQueries = new ArrayList<>(Arrays.asList(tmp.split("\t"))); //$NON-NLS-1$
		}
		else {
			this.pQueries = null;
		}

		return true;
	}

	@Override
	public boolean saveParameters() {

		if (this.pPartNames != null) {
			this.saveParameter(CQPPreferences.PART_NAMES, StringUtils.join(pPartNames, "\t")); //$NON-NLS-1$
		}

		if (this.pQueries != null) {
			this.saveParameter(TXMPreferences.QUERIES, StringUtils.join(pQueries, "\t")); //$NON-NLS-1$
		}

		return true;
	}

	/**
	 * Get the safe values (special char regexp safe).
	 *
	 * @param property the property
	 * @param corpus the corpus
	 * @return the safe values
	 * @throws CqiClientException the cqi client exception
	 */
	private static List<String> getSafeValues(StructuralUnitProperty property, CQPCorpus corpus) throws CqiClientException {
		List<String> values = property.getValues(corpus);
		for (int i = 0; i < values.size(); i++) {
			values.set(i, CQLQuery.addBackSlash(values.get(i)));
		}
		return values;
	}

	@Override
	public String getResultType() {
		return "Partition"; //$NON-NLS-1$
	}

	/**
	 * Gets the parent Partition of the specified result if exists.
	 * 
	 * @param result
	 * @return the parent Partition if exists otherwise null
	 */
	synchronized public static Partition getFirstParentPartition(TXMResult result) {
		return result.getFirstParent(Partition.class);
	}


	@Override
	public CQPCorpus getCorpus() {
		return (CQPCorpus) parent;
	}

	public Part getPartForName(String pName) {
		if (pName == null) return null;

		for (Part p : getChildren(Part.class)) {
			if (pName.equals(p.getName())) {
				return p;
			}
		}
		return null;
	}

	public List<String> getQueriesParameter() {
		return pQueries;
	}

	public List<String> getPartNamesParameter() {
		return pPartNames;
	}

	public void removeAllParts() {

		for (Part p : getParts()) {
			p.delete();
		}
	}
}
