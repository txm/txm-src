// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus.query;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.searchengine.cqp.corpus.Property;

/**
 * A focus is a modality of a {@link Property}.
 * 
 * @author sloiseau
 */
public class Focus {

	/** The analysis property. */
	private final Property analysisProperty;

	/** The modality. */
	private final List<String> modality;

	/**
	 * Instantiates a new focus.
	 * 
	 * @param analysisProperty
	 *            the analysis property
	 * @param modality
	 *            the modality
	 */
	public Focus(Property analysisProperty, String modality) {
		// this(analysisProperty, new ArrayList<String>().add(modality));
		super();
		this.analysisProperty = analysisProperty;
		this.modality = new ArrayList<String>();
		this.modality.add(modality);
		checkModalityList();
	}

	/**
	 * Instantiates a new focus.
	 * 
	 * @param analysisProperty
	 *            the analysis property
	 * @param modality
	 *            the modality
	 */
	public Focus(Property analysisProperty, List<String> modality) {
		super();
		this.analysisProperty = analysisProperty;
		this.modality = modality;
		checkModalityList();
	}

	/**
	 * Check modality list.
	 */
	private void checkModalityList() {
		for (String mod : modality) {
			if (mod == null || mod.equals("")) throw new IllegalArgumentException(TXMCoreMessages.theFocusIsEmptyOrNull); //$NON-NLS-1$
		}
	}

	/**
	 * Gets the analysis property.
	 * 
	 * @return the analysis property
	 */
	public Property getAnalysisProperty() {
		return analysisProperty;
	}

	/**
	 * Gets the modality.
	 * 
	 * @return the modality
	 */
	public List<String> getModality() {
		return modality;
	}

	/**
	 * Gets the query string.
	 * 
	 * @return the query string
	 */
	public String getQueryString() {
		StringBuffer queryString = new StringBuffer();
		queryString.append(CqpQueryConstant.START_ATTRIBUTE);
		queryString.append(analysisProperty.getName());
		queryString.append(CqpQueryConstant.EQUALS);
		queryString.append(CqpQueryConstant.START_QUOTE);
		if (modality.size() == 1) {
			queryString.append(modality.get(0));
		}
		else {
			for (Iterator<String> iter = modality.iterator(); iter.hasNext();) {
				String element = iter.next();
				queryString.append(element);
				if (iter.hasNext()) {
					queryString.append(CqpQueryConstant.OR);
				}
			}
		}
		queryString.append(CqpQueryConstant.END_QUOTE);
		queryString.append(CqpQueryConstant.END_ATTRIBUTE);
		queryString.append(CqpQueryConstant.END_QUERY_EXPR);
		return queryString.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getQueryString();
	}

}
