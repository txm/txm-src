// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2016-11-29 16:47:07 +0100 (Tue, 29 Nov 2016) $
// $LastChangedRevision: 3349 $
// $LastChangedBy: mdecorde $
//
package org.txm.searchengine.cqp.corpus;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.TXMResult;
import org.txm.libs.cqp.CQPLibPreferences;
import org.txm.objects.CorpusCommandPreferences;
import org.txm.objects.Project;
import org.txm.searchengine.core.QueryBasedTXMResult;
import org.txm.searchengine.core.messages.SearchEngineCoreMessages;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.InvalidCqpIdException;
import org.txm.searchengine.cqp.core.functions.selection.SelectionResult;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.SubcorpusCQLQuery;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * The Class Corpus. A corpus is either a whole corpus, as managed by CQP or a
 * subcorpus.
 * 
 * @see org.txm.searchengine.cqp.corpus.MainCorpus
 * @see org.txm.searchengine.cqp.corpus.Subcorpus
 * 
 * @author Jean-Philippe Magué, Sylvain Loiseau, mdecorde
 */
public abstract class CQPCorpus extends org.txm.objects.CorpusBuild implements CqpObject {

	/**
	 * subresults cache. eg used to store CQP lexicons
	 */
	protected static HashMap<Class<?>, ArrayList<?>> cache = new HashMap<>();

	private static final int CACHE_MAXIMUM_SIZE = 5;

	/** The nocorpus. */
	protected static int nocorpus = 1;

	/** The prefix r. */
	protected static String prefixR = "Corpus"; //$NON-NLS-1$

	private static Integer QUERY_COUNTER = new Integer(0);

	protected static Integer SUBCORPUS_COUNTER = new Integer(0);

	protected static String WORD = TXMPreferences.DEFAULT_UNIT_PROPERTY;

	public static final Comparator<WordProperty> wordFirstComparator = new Comparator<WordProperty>() {

		@Override
		public int compare(WordProperty o1, WordProperty o2) {
			if (o1.getName().equals("word")) { //$NON-NLS-1$
				return -1;
			}
			if (o2.getName().equals("word")) { //$NON-NLS-1$
				return 1;
			}
			return o1.compareTo(o2);
		}
	};

	/**
	 * store an object for a maximum of 5 object cached per object
	 * 
	 * @param toCache
	 * @return
	 */
	public Object cache(Object toCache) {
		if (toCache == null) {
			return null;
		}

		ArrayList<?> ocCache = getObjectCacheForClass(toCache.getClass());
		if (ocCache.size() > CACHE_MAXIMUM_SIZE) {
			ocCache.remove(0); // oldest value
		}
		return toCache;
	}

	/**
	 * Returns the first parent object that is a Subcorpus, a Corpus or a
	 * MainCorpus. Returns the object itself if its class is one of the the three
	 * listed below.
	 * 
	 * @return
	 */
	synchronized public static CQPCorpus getFirstParentCorpus(TXMResult result) {
		CQPCorpus corpus = result.getFirstParent(Subcorpus.class);
		if (corpus == null) {
			corpus = result.getFirstParent(CQPCorpus.class);
		}
		if (corpus == null) {
			corpus = (CQPCorpus) getParentMainCorpus(result);
		}
		return corpus;
	}

	public static String getNextQueryCounter() {
		QUERY_COUNTER++;
		return QUERY_COUNTER.toString();
	}

	/**
	 * append the current timestamp to the subcorpus counter -> should avoid clash between subcorpus created between different TXM sessions
	 * 
	 * @return
	 */
	public static String getNextSubcorpusCounter() {
		SUBCORPUS_COUNTER++;
		return "" + System.currentTimeMillis() + "_" + SUBCORPUS_COUNTER.toString(); //$NON-NLS-1$
	}

	/**
	 * retrieve all cached objects for an object
	 * 
	 * @return
	 */
	public HashMap<Class<?>, ArrayList<?>> getObjectCache() {
		return cache;
	}

	/**
	 * retrieve all cached objects of a certain class for an object
	 * 
	 * @param c
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T extends Object> ArrayList<T> getObjectCacheForClass(Class<T> c) {
		HashMap<Class<?>, ArrayList<?>> oCache = getObjectCache();
		if (!oCache.containsKey(c)) {
			oCache.put(c, new ArrayList<T>());
		}
		return (ArrayList<T>) oCache.get(c);
	}

	/**
	 * Gets the main corpus parent.
	 * 
	 * @return the first parent of class MainCorpus or the object itself if it is
	 *         its class.
	 */
	synchronized public static TXMResult getParentMainCorpus(TXMResult result) {
		return result.getFirstParent(MainCorpus.class);
	}

	/**
	 * remove a cached object
	 * 
	 * @param classToUnCache
	 * @return
	 */
	public static ArrayList<?> uncache(Class<?> classToUnCache) {
		if (classToUnCache == null) {
			return null;
		}
		return cache.remove(classToUnCache);
	}

	/**
	 * remove a cached object
	 * 
	 * @param toUnCache
	 * @return
	 */
	public boolean uncache(Object toUnCache) {
		if (toUnCache == null) {
			return false;
		}
		ArrayList<?> ocCache = getObjectCacheForClass(toUnCache.getClass());
		return ocCache.remove(toUnCache);
	}

	/** All private infos of the corpus. */
	protected String charset = "latin1"; //$NON-NLS-1$

	/**
	 * Gets the locale of the corpus.
	 * 
	 */
	protected String lang = null;

	/** stores The corpus lexical units properties. */
	protected List<WordProperty> lexicalUnitsProperties;

	/** The size in token */
	protected int size = -1;

	/** The number of starting text. */
	protected int nbtext = -1;

	protected int[] textEndLimits;

	/** The textids. */
	protected HashMap<String, Integer> textids = null;

	// /**
	// * Creates a new object mapping a CQP corpus. This constructor is not
	// * intended to be used directly. Use {@link CorpusManager#getCorpus(String)}
	// * .
	// *
	// * @param pID
	// * the id of the cqp corpus
	// *
	// * @throws InvalidCqpIdException
	// * the invalid cqp id exception
	// */
	// protected Corpus(Corpus parentCorpus) throws InvalidCqpIdException {
	// super(parentCorpus);
	// if (parentCorpus ins)
	//// checkCqpId(pID);
	//// this.pID = pID;
	////
	//// // FIXME: at this moment, store the CQPID as UUIDD for persistence tests
	//// this.uniqueID = this.pID;
	// }
	//
	// /**
	// * Creates a new object mapping a CQP corpus. This constructor is not
	// * intended to be used directly. Use {@link CorpusManager#getCorpus(String)}
	// * .
	// *
	// * @param pID
	// * the id of the cqp corpus
	// *
	// * @throws InvalidCqpIdException
	// * the invalid cqp id exception
	// */
	// protected Corpus(Corpus parentCorpus) throws InvalidCqpIdException {
	// super(null, null, pID);
	//// checkCqpId(pID);
	//// this.pID = pID;
	////
	//// // FIXME: at this moment, store the CQPID as UUIDD for persistence tests
	//// this.uniqueID = this.pID;
	// }

	/** The corpus structural units. */
	protected List<StructuralUnit> structuralUnits;

	/** The symbol. */
	protected String symbol;

	protected StructuralUnitProperty textIdStructuralUnitProperty;
	// public HashSet<Object> getStoredData(Class class1) {
	//
	// if (!storedData.containsKey(class1)) {
	// storedData.put(class1, new HashSet<Object>());
	// }
	//
	// return storedData.get(class1);
	// }
	//
	// /**
	// * TODO: fix this hack to store Lexicons
	// *
	// * @param o a lexicon
	// */
	// public void storeData(Object o) {
	// if (storedData.containsKey(o.getClass())) {
	// storedData.put(o.getClass(), new HashSet<Object>());
	// }
	//
	// HashSet<Object> hash = storedData.get(o.getClass());
	// hash.add(o);
	// }

	protected int[] textLimits;

	protected StructuralUnit textStructuralUnit;

	protected WordProperty wordProperty = null;

	private List<VirtualProperty> virtualProperties;

	/**
	 * 
	 * @param corpus
	 */
	public CQPCorpus(CQPCorpus corpus) {
		super(corpus);
	}

	/**
	 * 
	 * @param partition
	 */
	public CQPCorpus(Partition partition) {
		super(partition);
	}

	/**
	 * 
	 * @param project
	 */
	protected CQPCorpus(Project project) {
		super(project);
	}

	// /**
	// * Drop all the partitions.
	// *
	// * @throws CqiClientException
	// * the cqi client exception
	// */
	// public void dropAllPartitions() throws CqiClientException {
	// List<Partition> _partitions = (List<Partition>) getChildren(Partition.class);
	// for (Partition partition : _partitions) {
	// this.dropPartition(partition);
	// }
	// _partitions = null;
	// }

	/**
	 * 
	 * @param parametersNodePath
	 */
	public CQPCorpus(String parametersNodePath) {
		super(parametersNodePath);
	}

	// /**
	// * Drops a partition.
	// *
	// * @param partition
	// * the partition
	// *
	// * @throws CqiClientException
	// * the cqi client exception
	// */
	// public void dropPartition(Partition partition) throws CqiClientException {
	// Log.finest(SearchEngineCoreMessages.info_deletingPartitionP0 +
	// partition.getName());
	// for (Part part : partition.getParts())
	// try {
	// CorpusManager.getCorpusManager().getCqiClient().dropSubCorpus(part.getQualifiedCqpId());
	// } catch (Exception e) {
	// throw new CqiClientException(e);
	// }
	// }

	public CQPCorpus(QueryBasedTXMResult result) {

		super(result);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.objects.TxmObject#load()
	 */
	@Override
	protected boolean _load(Element e) {
		// System.out.println("*** Load corpus: "+name);
		if (e == null) {
			Log.warning(TXMCoreMessages.bind(TXMCoreMessages.warningColonCantLoadCorpusP0, this));
			return false;
		}

		// 1- texts queries then
		// 2- get metadatas, pattributes, sattributes, editions, biblios, uis,
		super._load(e);
		// System.out.println("Load corpus : "+this.name);
		// this.loadMetadata(); // read <prop>s

		// 3- build saved subcorpus and partition
		// System.out.println("restore subcorpora: "+selfElement);
		NodeList preBuildElems = e.getElementsByTagName("preBuild"); //$NON-NLS-1$
		if (preBuildElems.getLength() > 0) {
			Element preBuildElem = (Element) preBuildElems.item(0);
			NodeList subcorpusElems = preBuildElem.getElementsByTagName("subcorpus"); //$NON-NLS-1$
			for (int i = 0; i < subcorpusElems.getLength(); i++) {
				Element subcorpusElem = (Element) subcorpusElems.item(i);
				if (!subcorpusElem.getParentNode().equals(preBuildElem)) {
					continue; // level 1 elements
				}
				Subcorpus subcorp = new Subcorpus(this);
				subcorp._load(subcorpusElem);
			}

			NodeList partitionElems = preBuildElem.getElementsByTagName("partition"); //$NON-NLS-1$
			for (int i = 0; i < partitionElems.getLength(); i++) {
				Element partitionElem = (Element) partitionElems.item(i);
				if (!partitionElem.getParentNode().equals(preBuildElem)) {
					continue; // level 1 elements
				}
				String name = partitionElem.getAttribute("name"); //$NON-NLS-1$
				List<String> names = new ArrayList<>();
				List<String> queries = new ArrayList<>();

				NodeList partElems = partitionElem.getElementsByTagName("part"); //$NON-NLS-1$
				for (int j = 0; j < partElems.getLength(); j++) {
					Element part = (Element) partElems.item(j);
					names.add(part.getAttribute("name")); //$NON-NLS-1$
					queries.add(part.getAttribute("query")); //$NON-NLS-1$
				}
				try {
					Partition partition = new Partition(this);
					partition.setParameters(name, queries, names);
				}
				catch (Exception ex) {
					Log.warning(TXMCoreMessages.bind(TXMCoreMessages.failedToRestoreTheP0PartitionOfP1P2, name, this.pID, ex));
				}
			}
		}

		return true;
	}

	/**
	 * Empty the corpus caches
	 */
	@Override
	public void clean() {
		if (cache != null) {
			cache.clear();
		}

		charset = "latin1"; //$NON-NLS-1$
		lang = null;
		lexicalUnitsProperties = null;
		virtualProperties = null;
		size = -1;
		structuralUnits = null;
		symbol = null;
		textIdStructuralUnitProperty = null;
		textLimits = null;
		textStructuralUnit = null;
		wordProperty = null;
	}

	/**
	 * recursive method to clone a result
	 * 
	 * @param newParent
	 *            necessary for cloning children in the right clone parent
	 * @param all
	 * @return
	 */
	@Override
	public TXMResult clone(TXMResult newParent, boolean all) {
		CQPCorpus c = (CQPCorpus) super.clone(newParent, all);
		if (c == null) {
			return null;
		}
		c.pID = "S" + getNextSubcorpusCounter();
		return c;
	}

	@Override
	public int compareTo(TXMResult o) {
		// Compare node weights
		int output = super.compareTo(o);
		// Compare simple names if weights are equal
		if (this.pID == null) {
			return -1;
		}
		if (o.getSimpleName() == null) {
			return 1;
		}
		if (output == 0) {
			output = this.getSimpleName().compareTo(o.getSimpleName());
		}
		return output;
	}

	/**
	 * Create a named subcorpus from a query on this corpus.
	 * 
	 * @param query
	 *            the query
	 * @param subcorpusName
	 *            the resulting subcorpus name
	 * 
	 * @return the resulting subcorpus
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public Subcorpus createSubcorpus(CQLQuery query, String subcorpusName) throws CqiClientException {
		Subcorpus sc = createSubcorpus(query, subcorpusName, false);
		return sc;
	}

	/**
	 * Creates a subcorpus and save configuration in the workspace.
	 *
	 * @param query
	 *            the query
	 * @param subcorpusName
	 *            the subcorpus name
	 * @param registered
	 *            the registered
	 * @return the subcorpus
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public Subcorpus createSubcorpus(CQLQuery query, String subcorpusName, boolean registered) throws CqiClientException {

		Subcorpus subcorpus;
		String subcorpusCqpId = subcorpusNamePrefix + getNextSubcorpusCounter().toString();

		long start = System.currentTimeMillis();
		try {
			if (query.getQueryString().length() > CQLQuery.MAX_CQL_LENGTH) {
				Log.warning(NLS.bind("Error: CQL length limit reached ({0}) with the {1} length.", CQLQuery.MAX_CQL_LENGTH, query.getQueryString().length()));
				return null;
			}

			//CorpusManager.getCorpusManager().getCqiClient().cqpQuery(this.getQualifiedCqpId(), subcorpusCqpId, CQLQuery.fixQuery(query.getQueryString()));
			// System.out.println("SUBCORPUS: "+subcorpusCqpId+"
			// q="+query.getQueryString());
			subcorpus = new Subcorpus(this);
			subcorpus.setParameters(subcorpusCqpId, subcorpusName, query);
			subcorpus.compute();
		}
		catch (Exception e) {
			throw new CqiClientException(e);
		}
		long end = System.currentTimeMillis();

		Log.fine(TXMCoreMessages.bind(SearchEngineCoreMessages.subcorpusP0CreatedInP1Ms, subcorpusName, (end - start)));

		// if (!registered) subcorpus.registerToParent();
		return subcorpus;
	}

	/**
	 * Creates a subcorpus using workspace definition.
	 *
	 * @param elem
	 *            the XML element
	 * @return the subcorpus
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public Subcorpus createSubcorpus(Element elem) throws CqiClientException {
		CQLQuery query = new CQLQuery(elem.getAttribute("query")); //$NON-NLS-1$
		String subcorpusName = elem.getAttribute("name"); //$NON-NLS-1$

		Subcorpus subcorpus;
		String subcorpusCqpId = subcorpusNamePrefix + getNextSubcorpusCounter();

		// long start = System.currentTimeMillis();
		try {
			CorpusManager.getCorpusManager().getCqiClient().cqpQuery(this.getQualifiedCqpId(), subcorpusCqpId, CQLQuery.fixQuery(query.getQueryString(), this.getLang()));
			subcorpus = new Subcorpus(this);
			subcorpus.setParameters(subcorpusCqpId, subcorpusName, query);
			subcorpus.compute();
		}
		catch (Exception e) {
			throw new CqiClientException(e);
		}

		return subcorpus;
	}

	public Subcorpus createSubcorpus(String name, QueryResult queryResult) throws Exception {
		Subcorpus sub = new Subcorpus(this);
		sub.setParameters(queryResult.getCqpId(), name, queryResult);
		sub.compute();
		return sub;
	}

	/**
	 * Creates the subcorpus.
	 *
	 * @return the subcorpus
	 * @throws CqiClientException the cqi client exception
	 * @throws InterruptedException
	 */
	public Subcorpus createSubcorpus(String name, SelectionResult selectionResult) throws CqiClientException, InterruptedException {
		Subcorpus sub = createSubcorpus(this.getStructuralUnit("text"), this.getStructuralUnit("text").getProperty("id"), selectionResult, name); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		sub.setSelectionResult(selectionResult);
		sub.compute();
		return sub;
	}

	public Subcorpus createSubcorpus(String name, String cqpid) throws Exception {
		Subcorpus sub = new Subcorpus(this);
		sub.setParameters(cqpid, name, new QueryResult(cqpid, name, this, null));
		sub.compute();
		return sub;
	}

	/**
	 * Creates the subcorpus.
	 *
	 * @param structuralUnit the structural unit
	 * @param structuralUnitProperty the structural unit property
	 * @param values the values
	 * @param subcorpusName the subcorpus name
	 * 
	 * @return the subcorpus
	 * 
	 * @throws CqiClientException the cqi client exception
	 */
	public Subcorpus createSubcorpus(StructuralUnit structuralUnit, StructuralUnitProperty structuralUnitProperty, List<String> values, String subcorpusName) throws CqiClientException {
		if (values.size() == 0) {
			Log.warning(TXMCoreMessages.noValuesGiven);
			return null;
		}
		String regexp = ""; //$NON-NLS-1$
		for (String v : values) {
			regexp += CQLQuery.addBackSlash(v) + "|"; //$NON-NLS-1$
		}
		regexp = regexp.substring(0, regexp.length() - 1);

		SubcorpusCQLQuery query = new SubcorpusCQLQuery(structuralUnit, structuralUnitProperty, regexp, values);
		return createSubcorpus(query, subcorpusName);
	}

	/**
	 * Create a subcorpus from a structural unit, a property on this structural unit
	 * and a value for this property.
	 *
	 * @param structuralUnit the structural unit
	 * @param structuralUnitProperty the structural unit property
	 * @param value the value
	 * @param subcorpusName the subcorpus name
	 * 
	 * @return the subcorpus
	 * 
	 * @throws CqiClientException the cqi client exception
	 */
	public Subcorpus createSubcorpus(StructuralUnit structuralUnit, StructuralUnitProperty structuralUnitProperty, String value, String subcorpusName) throws CqiClientException {
		SubcorpusCQLQuery query = new SubcorpusCQLQuery(structuralUnit, structuralUnitProperty, CQLQuery.addBackSlash(value));
		return createSubcorpus(query, subcorpusName);
	}

	@Override
	protected final boolean _compute(TXMProgressMonitor monitor) throws Exception {
		// reset the internal variables that store cached information

		lexicalUnitsProperties = null;
		size = -1;
		nbtext = -1;
		textEndLimits = null;
		textids = null;
		structuralUnits = null;
		symbol = null;
		textIdStructuralUnitProperty = null;
		textLimits = null;
		textStructuralUnit = null;
		wordProperty = null;

		return __compute(monitor);
	}

	protected abstract boolean __compute(TXMProgressMonitor monitor) throws Exception;

	/**
	 * Create a subcorpus from a structural unit, a property on this structural unit
	 * and a value for this property.
	 *
	 * @param structuralUnit the structural unit
	 * @param structuralUnitProperty the structural unit property
	 * @param value the value
	 * @param subcorpusName the subcorpus name
	 * 
	 * @return the subcorpus
	 * 
	 * @throws CqiClientException the cqi client exception
	 */
	public Subcorpus createSubcorpusWithQueryString(StructuralUnit structuralUnit, StructuralUnitProperty structuralUnitProperty, String value, String subcorpusName) throws CqiClientException {
		SubcorpusCQLQuery query = new SubcorpusCQLQuery(structuralUnit, structuralUnitProperty, value);
		return createSubcorpus(query, subcorpusName);
	}

	/**
	 * Drop all the subcorpora.
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public void dropAllSubcorpora() throws CqiClientException {
		// System.out.println("!! drop all subcorpora: "+subcorpora.size());
		List<Subcorpus> subcorpora = getSubcorpora();
		while (subcorpora.size() > 0) {
			this.dropSubcorpus(subcorpora.get(0));
		}
	}

	/**
	 * Drop query result.
	 * 
	 * @param queryResult
	 *            the query result
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public void dropQueryResult(QueryResult queryResult) throws CqiClientException {
		Log.fine(SearchEngineCoreMessages.bind(SearchEngineCoreMessages.deletingQueryResultP0, queryResult.getName()));
		try {
			CorpusManager.getCorpusManager().getCqiClient().dropSubCorpus(queryResult.getQualifiedCqpId());
		}
		catch (Exception e) {
			throw new CqiClientException(e);
		}
	}

	/**
	 * Drops a subcorpus in CQP also its subcorpus and partitions.
	 * 
	 * @param subcorpus
	 *            the subcorpus
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public boolean dropSubcorpus(CQPCorpus subcorpus) throws CqiClientException {
		subcorpus.dropAllSubcorpora();
		// subcorpus.dropAllPartitions();
		try {
			CorpusManager.getCorpusManager().getCqiClient().dropSubCorpus(subcorpus.getQualifiedCqpId());
		}
		catch (Exception e) {
			throw new CqiClientException(e);
		}
		return true;
	}

	/**
	 * Test if 2 corpus have the same CQP id.
	 *
	 * @param c
	 *            the c
	 * @return true, if successful
	 */
	public boolean equals(CQPCorpus c) {
		return pID.equals(c.pID);
	}

	/**
	 * Get the charset set in the CQP registry file.
	 *
	 * @return the charset
	 */
	public String getCharset() {
		if (this.charset.matches("latin1")) //$NON-NLS-1$
			return "ISO-8859-1"; //$NON-NLS-1$
		else if (this.charset.matches("latin2")) //$NON-NLS-1$
			return "ISO-8859-2"; //$NON-NLS-1$
		else if (this.charset.matches("latin3")) //$NON-NLS-1$
			return "ISO-8859-3"; //$NON-NLS-1$
		else if (this.charset.matches("latin4")) //$NON-NLS-1$
			return "ISO-8859-4"; //$NON-NLS-1$
		else if (this.charset.matches("latin5")) //$NON-NLS-1$
			return "ISO-8859-5"; //$NON-NLS-1$
		else if (this.charset.matches("latin6")) //$NON-NLS-1$
			return "ISO-8859-6"; //$NON-NLS-1$
		else if (this.charset.matches("latin7")) //$NON-NLS-1$
			return "ISO-8859-7"; //$NON-NLS-1$
		else if (this.charset.matches("latin8")) //$NON-NLS-1$
			return "ISO-8859-8"; //$NON-NLS-1$
		else if (this.charset.matches("latin9")) //$NON-NLS-1$
			return "ISO-8859-9"; //$NON-NLS-1$
		return charset;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.searchengine.cqp.corpus.CqpObject#getMotherMainCorpus()
	 */
	public CQPCorpus getCorpusParent() {
		// return (CQPCorpus) getFirstParent(CQPCorpus.class);
		TXMResult p = getFirstParent(Subcorpus.class);
		if (p != null)
			return (Subcorpus) p;
		return getFirstParent(MainCorpus.class);
	}

	@Override
	public String getCQLLimitQuery() {
		CorpusCommandPreferences prefs = getProject().getCommandPreferences("concordance"); //$NON-NLS-1$
		if (prefs == null) {
			return "<text>[]"; //$NON-NLS-1$
		}
		String str = prefs.get("context_limits"); //$NON-NLS-1$
		if (str != null) {
			String type = prefs.get("context_limits_type"); //$NON-NLS-1$
			if ("list".equals(type)) {
				String[] ss = str.split(","); //$NON-NLS-1$
				ArrayList<String> parts = new ArrayList<>();
				for (String s : ss) {
					parts.add("<" + s + "> []"); //$NON-NLS-1$
				}
				return org.apache.commons.lang.StringUtils.join(parts, " | "); //$NON-NLS-1$
			}
			else {
				return str;
			}
		}
		else {
			return "<text>[]"; //$NON-NLS-1$
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.searchengine.cqp.corpus.CqpObject#getCqpId()
	 */
	@Override
	public String getCqpId() {
		return pID;
	}

	/**
	 * Gets the default property (by default: "word").
	 * 
	 * @return
	 * @throws CqiClientException
	 */
	public WordProperty getDefaultProperty() throws CqiClientException {
		return this.getProperty(WORD);
	}

	/**
	 * Gets the corpus informations using CQP registry.
	 *
	 * @return the infos
	 */
	public String getInfos() {
		return TXMCoreMessages.corpusColon + this.pID + TXMCoreMessages.encodingColon + getCharset() + TXMCoreMessages.languageColon + getLang();
	}

	/**
	 * Gets the language.
	 *
	 * @return the language
	 */
	@Override
	public String getLang() {
		//getLocale();
		return getProject().getLang();
	}

	//	/**
	//	 * Get the corpus locale using txmcorpus_lang property
	//	 * @deprecated use getProject.getLlang() instead, will be fixed later to mange multi lang corpus
	//	 * @return
	//	 */
	//	public Locale getLocale() {
	//		/*
	//		 * Serializable lang = this.getAttribute("lang"); //$NON-NLS-1$ if (lang !=
	//		 * null) { return new Locale(lang.toString()); } else return
	//		 * Locale.getDefault();
	//		 */
	//		// System.out.println("getLang1: "+lang);
	//		if (lang == null) {
	//			try {
	//				StructuralUnit su = this.getStructuralUnit("txmcorpus"); //$NON-NLS-1$
	//				StructuralUnitProperty prop = su.getProperty("lang"); //$NON-NLS-1$
	//				List<String> values = prop.getValues();
	//				if (values.size() > 0)
	//					lang = values.get(0);
	//				// System.out.println("get lang: "+lang);
	//			}
	//			catch (Exception e) {
	//				// TODO Auto-generated catch block
	//				// org.txm.utils.logger.Log.printStackTrace(e);
	//				System.out.println(TXMCoreMessages.corpusColongetLocaleColonCQPIsNotReadyToAnswerColon + e);
	//			}
	//		}
	//		if (lang != null)
	//			return new Locale(lang);
	//		else
	//			return Locale.getDefault();
	//	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.searchengine.cqp.corpus.CqpObject#getMotherMainCorpus()
	 */
	@Override
	public MainCorpus getMainCorpus() {
		if (this instanceof MainCorpus)
			return (MainCorpus) this;

		return getFirstParent(MainCorpus.class);
	}

	@Override
	abstract public List<? extends org.txm.objects.Match> getMatches();

	@Override
	public String getName() {
		return this.getUserName();
	}

	/**
	 * Get the ordered properties (by name).
	 *
	 * @return the ordered properties
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	@SuppressWarnings("unchecked")
	public List<WordProperty> getOrderedProperties() throws CqiClientException {
		List<WordProperty> props = this.getProperties();
		Collections.sort(props, wordFirstComparator);
		return props;
	}

	/**
	 * Get the ordered Structural unit properties (by name)
	 * 
	 * @return all the StructuralUnitProperty ordered by name
	 * @throws CqiClientException
	 */
	public Collection<StructuralUnitProperty> getOrderedStructuralUnitProperties() throws CqiClientException {
		List<StructuralUnitProperty> properties = new ArrayList<>();
		for (StructuralUnit su : this.getOrderedStructuralUnits())
			if (su.getProperties() != null)
				properties.addAll(su.getProperties());

		return properties;
	}

	/**
	 * Get the ordered structural units.
	 *
	 * @return the ordered structural units
	 * @throws CqiClientException the cqi client exception
	 */
	@SuppressWarnings("unchecked")
	public List<StructuralUnit> getOrderedStructuralUnits() throws CqiClientException {
		List<StructuralUnit> units = this.getStructuralUnits();
		Collections.sort(units);
		return units;
	}

	/*
	 * public List<Property> getPreferedOrderedProperties() throws
	 * CqiClientException { if (this.getAttribute("viewprops_order")) List<Property>
	 * props = this.getProperties(); Collections.sort(props); return props; }
	 */

	/**
	 * Get all the partitions defined on a given corpus.
	 *
	 * @param name
	 *            the name
	 * @return the partitions
	 */
	public Partition getPartition(String name) {
		List<Partition> partitions = getChildren(Partition.class);
		for (Partition p : partitions)
			if (p.getUserName().equals(name))
				return p;
		return null;
	}

	/**
	 * Gets all the partitions defined on a given corpus.
	 * 
	 * @return the partitions
	 */
	public List<Partition> getPartitions() {
		return getChildren(Partition.class);
	}

	/**
	 * Gets the virtual properties of this corpus.
	 * 
	 * current implementation only returns the "position" virtual property
	 * 
	 * @return the properties
	 * 
	 */
	public List<VirtualProperty> getVirtualProperties() {
		if (virtualProperties == null) {
			virtualProperties = new ArrayList<>();
			virtualProperties.add(new PositionProperty(this));
			virtualProperties.add(new LengthProperty(this));
		}
		return virtualProperties;
	}

	/**
	 * Gets the properties available in this corpus.
	 * 
	 * @return the properties
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public List<WordProperty> getProperties() throws CqiClientException {
		if (this.lexicalUnitsProperties != null) {
			return this.lexicalUnitsProperties;
		}

		String[] propertiesName;
		try {
			propertiesName = CQPSearchEngine.getCqiClient().corpusPositionalAttributes(this.getMainCorpus().pID);
		}
		catch (Exception e) {
			throw new CqiClientException(e);
		}

		List<WordProperty> properties = new ArrayList<>(propertiesName.length);
		for (int i = 0; i < propertiesName.length; i++) {
			properties.add(new WordProperty(propertiesName[i], this));
		}
		this.lexicalUnitsProperties = properties;
		return properties;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.txm.searchengine.cqp.corpus.Corpus#getProperty(java.lang.
	 * String)
	 */
	public WordProperty getProperty(String name) throws CqiClientException {
		if (this.lexicalUnitsProperties == null) {
			getProperties();
		}

		int idx = name.indexOf("."); //$NON-NLS-1$
		if (idx > 0) { // the name if a qualified name
			name = name.substring(idx + 1);
		}

		if (VirtualProperty.isAVirtualPropertyFullName(name)) {
			name = VirtualProperty.fullnameToName(name);

			for (WordProperty property : getVirtualProperties()) {
				if (property.getName().equals(name)) {
					return property;
				}
			}
		}
		else {
			for (WordProperty property : getProperties()) {
				if (property.getName().equals(name)) {
					return property;
				}
			}
		}


		return null;
	}

	@Override
	public String getSimpleName() {
		return this.getUserName();
	}

	// /* (non-Javadoc)
	// * @see org.txm.objects.TxmObject#removeChildren(org.txm.objects.TxmObject)
	// */
	// @Override
	// public TxmObject removeChildren(TxmObject children) {
	// Element preBuildElement =
	// this.getProject().getProjectParameters().getPreBuildElement();
	// if (children instanceof Subcorpus) {
	// try {
	// if (children.getSelfElement() != null &&
	// children.getSelfElement().getParentNode() == getSelfElement())
	// preBuildElement.removeChild(children.getSelfElement());
	//
	// if (!dropSubcorpus((Corpus) children)) {
	// System.out.println("Corpus.removeChildren: dropSubcorpus returned false.");
	// return null;
	// }
	// super.removeChildren(children);
	// return children;
	// } catch (CqiClientException e) {
	// return null;
	// }
	// } else if (children instanceof Partition) {
	// if (children.getSelfElement() != null &&
	// children.getSelfElement().getParentNode() == getSelfElement()) {
	// preBuildElement.removeChild(children.getSelfElement());
	// }
	// try {
	// dropPartition((Partition) children);
	// super.removeChildren(children);
	// return children;
	// } catch (CqiClientException e) {
	// return null;
	// }
	//
	// }
	// return null;
	// }

	// /**
	// * Send the Corpus indexes to R.
	// *
	// * Only the word properties are send.
	// * TODO: send structural unit properties too
	// *
	// * @return the string
	// * @throws Exception the exception
	// */
	// public String SendToR() throws Exception
	// {
	// int initialPosition = 0;
	// if (this.getMatches() != null && this.getMatches().size() > 0)
	// initialPosition = this.getMatches().get(0).getStart();
	//
	// // cols name to export
	// List<Property> props = this.getProperties();
	// String[] colnames = new String[this.getProperties().size()];
	//
	// // compute positions list
	// int length = 0;
	// for (Match match : getMatches()) { // 1) get number of positions
	// length += match.getLength();
	// }
	// int[] positions = new int[length];
	// int iPosition = 0;
	// for (Match match : getMatches()) { // 2) append positions
	// int p_end = match.getEnd();
	// for (int p = match.getStart() ; p < p_end ; p++)
	// positions[iPosition] = p;
	// }
	//
	// // 0- create the R object
	// this.symbol = prefixR + (nocorpus++);
	// RWorkspace rw = RWorkspace.getRWorkspaceInstance();
	// rw.eval(symbol +" <- matrix(ncol="+colnames.length+",
	// nrow="+positions.length+")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	//
	// for(int i = 0 ; i < props.size() ; i++) {
	// Property prop = props.get(i);
	// colnames[i] = prop.getName();
	// // send positions to R
	// int[] idx =
	// CorpusManager.getCorpusManager().getCqiClient().cpos2Id(prop.getQualifiedName(),
	// positions);
	// rw.addVectorToWorkspace("tmp", idx); //$NON-NLS-1$
	// //if (initialPosition > 0) rw.eval("tmp <- tmp - "+initialPosition);
	// //System.out.println("lengths "+prop.getName()+" : idx="+idx.length+" mat
	// "+positions.length);
	// rw.eval(symbol+"[,"+(i+1)+"] <- tmp"); //$NON-NLS-1$ //$NON-NLS-2$
	// }
	//
	// //set matrix col names
	// rw.addVectorToWorkspace("tmpcol", colnames); //$NON-NLS-1$
	// rw.eval("colnames("+symbol+") <- tmpcol"); //$NON-NLS-1$ //$NON-NLS-2$
	//
	// rw.eval(symbol+" <- list(data="+symbol+")"); //$NON-NLS-1$ //$NON-NLS-2$
	//
	// // create lexicons
	// for (Property prop : this.getProperties()) {
	// Lexicon lex = this.getLexicon(prop);
	// rw.addVectorToWorkspace("tmp", lex.getForms()); //$NON-NLS-1$
	// rw.eval(symbol+"$lex$"+prop.getName()+" <- tmp"); //$NON-NLS-1$ //$NON-NLS-2$
	// }
	//
	// StructuralUnit text_su = this.getStructuralUnit("text");
	// if (text_su != null) {
	// StructuralUnitProperty text_id = text_su.getProperty("id");
	// if (text_id != null) {
	// int[] textlimits = this.getTextStartLimits();
	// rw.addVectorToWorkspace("text_limits", textlimits);
	// if (initialPosition > 0) rw.eval("text_limits <- text_limits -
	// "+initialPosition);
	// rw.eval(symbol+"$structs$text$start <- text_limits"); //$NON-NLS-1$
	// //$NON-NLS-2$
	//
	// textlimits = this.getTextEndLimits();
	// rw.addVectorToWorkspace("text_limits", textlimits);
	// if (initialPosition > 0) rw.eval("text_limits <- text_limits -
	// "+initialPosition);
	// //System.out.println("lengths "+prop.getName()+" : idx="+idx.length+" mat
	// "+positions.length);
	// rw.eval(symbol+"$structs$text$end <- text_limits"); //$NON-NLS-1$
	// //$NON-NLS-2$
	// }
	// }
	//
	// return symbol;
	// }

	/**
	 * Gets the CQP size of the corpus.
	 *
	 * @return the size
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public abstract int getSize() throws CqiClientException;

	abstract public int[] getStartLimits(String structural_unit_property) throws IOException, CqiServerError, InvalidCqpIdException, CqiClientException;

	/**
	 * Gets a structural unit from its name.
	 *
	 * @param name
	 *            the name
	 * @return the structural unit
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public abstract StructuralUnit getStructuralUnit(String name) throws CqiClientException;

	// public List<TXMResult> clearResults() {
	// List<TXMResult> old = this.children;
	// this.children = new ArrayList<TXMResult>();
	// return old;
	// }

	// HashMap<Class, HashSet<Object>> storedData = new HashMap<Class,
	// HashSet<Object>>();

	/**
	 * Gets all structural units property of a corpus.
	 *
	 * @return the structural unit properties
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public List<StructuralUnitProperty> getStructuralUnitProperties() throws CqiClientException {
		if (this.structuralUnits == null)
			getStructuralUnits();

		List<StructuralUnitProperty> properties = new ArrayList<>();
		if (this.structuralUnits != null)
			for (StructuralUnit su : this.structuralUnits)
			if (su.getOrderedProperties() != null)
				properties.addAll(su.getProperties());

		return properties;
	}

	/**
	 * Gets a structural units property of a structural unit by its name.
	 *
	 * @param name
	 *            the name
	 * @return the structural unit properties
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public List<StructuralUnitProperty> getStructuralUnitProperties(String name) throws CqiClientException {
		StructuralUnit su = this.getStructuralUnit(name);

		if (su == null) return new ArrayList<>();

		return su.getOrderedProperties();
	}

	// /**
	// *
	// * @param o the object to remove
	// * @return true if o has been removed
	// */
	// public boolean removeData(Object o) {
	// if (storedData.containsKey(o.getClass())) {
	// return storedData.get(o.getClass()).remove(o);
	// }
	// return false;
	// }

	/**
	 * Gets the structural units defined on this corpus.
	 * 
	 * @return the structural units
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public abstract List<StructuralUnit> getStructuralUnits() throws CqiClientException;

	/**
	 * Gets all the subcorpora defined on a given corpus.
	 * 
	 * @return the subcorpora
	 */
	public List<Subcorpus> getSubcorpora() {
		return getChildren(Subcorpus.class);
	}

	/**
	 * Gets the subcorpus by name (e.g /CORPUS/subcorpus)
	 *
	 * @param name
	 *            the name
	 * @return the subcorpus by name
	 */
	public Subcorpus getSubcorpusByName(String name) {
		List<Subcorpus> subcorpora = getSubcorpora();
		for (Subcorpus corpus : subcorpora) {
			if (corpus.getName().equals(name)) {
				return corpus;
			}
		}

		return null;
	}

	/**
	 * Gets the subcorpus by name.
	 *
	 * @param name
	 *            the name
	 * @return the subcorpus by name
	 */
	public Subcorpus getSubcorpusBySimpleName(String name) {
		List<Subcorpus> subcorpora = getSubcorpora();
		for (Subcorpus corpus : subcorpora) {
			if (corpus.getSimpleName().equals(name)) {
				return corpus;
			}
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.HasResults#getSubHasResults()
	 */
	public ArrayList<TXMResult> getSubHasResults() {
		ArrayList<TXMResult> subresults = new ArrayList<>();
		subresults.addAll(this.getSubcorpora());
		subresults.addAll(this.getPartitions());
		return subresults;
	}

	/**
	 * Gets the R symbol is corpus was sent in R Workspace.
	 *
	 * @return the symbol
	 */
	public String getSymbol() {
		return symbol;
	}

	/**
	 * 
	 * @return the text_id values of the CQP corpus ordered by position
	 * @throws CqiClientException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	abstract public String[] getCorpusTextIdsList() throws CqiClientException, IOException, CqiServerError;

	/**
	 * Gets the nb texts.
	 *
	 * @return the nb texts
	 * @throws CqiClientException the cqi client exception
	 * @throws CqiServerError
	 * @throws IOException
	 */
	abstract public int getNbTexts() throws CqiClientException, IOException, CqiServerError;

	public StructuralUnitProperty getTextIdStructuralUnitProperty() throws CqiClientException {
		if (textIdStructuralUnitProperty == null) {
			textIdStructuralUnitProperty = getTextStructuralUnit().getProperty(ID);
		}
		return textIdStructuralUnitProperty;
	}

	/**
	 * Return the CQP END positions of the main corpus texts
	 */
	abstract public int[] getTextEndLimits() throws CqiClientException, IOException, CqiServerError, InvalidCqpIdException;

	/**
	 * Gets the texts ids and order number in corpus.
	 *
	 * @return the texts ids
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 * @throws CqiClientException the cqi client exception
	 */
	abstract public HashMap<String, Integer> getTextsIds() throws IOException, CqiServerError, CqiClientException;

	/**
	 * Return the CQP START positions of the main corpus texts
	 */
	abstract public int[] getTextStartLimits() throws CqiClientException, IOException, CqiServerError, InvalidCqpIdException;

	public StructuralUnit getTextStructuralUnit() throws CqiClientException {
		if (textStructuralUnit == null) {
			textStructuralUnit = getStructuralUnit(TEXT);
		}
		return textStructuralUnit;
	}

	public WordProperty getWordProperty() throws CqiClientException {
		if (wordProperty == null)
			wordProperty = getProperty(WORD);
		return wordProperty;
	}

	/**
	 * Runs a query on the corpus and store the result in a QueryResult.
	 * 
	 * !! each working query is store in the Base of the corpus !!
	 * 
	 * @param query
	 *            the query
	 * @param queryResultName
	 *            the resulting subcorpus name
	 * 
	 * @return the resulting query result
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public QueryResult query(CQLQuery query, String queryResultName, boolean save) throws CqiClientException {

		QueryResult queryResult = null;
		// String queryResultId = queryResultNamePrefix + UUID.randomUUID().toString();
		String queryResultId = queryResultNamePrefix + getNextQueryCounter();


		if (query.getQueryString().length() > CQLQuery.MAX_CQL_LENGTH) {
			try {
				Log.warning(NLS.bind("Error: CQL length limit reached ({0}) with {1} length.", CQLQuery.MAX_CQL_LENGTH, query.getQueryString().length()));
				return new FakeQueryResult(queryResultName, this, query, new int[0], new int[0], new int[0]);
			}
			catch (InvalidCqpIdException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		Log.finest(NLS.bind(TXMCoreMessages.queryOnP0ColonP1InfP2, new String[] { this.getQualifiedCqpId(), queryResultId, query.getQueryString() })); // $NON-NLS-1$
		// long start = System.currentTimeMillis();
		try {
			//CorpusManager.getCorpusManager().getCqiClient().cqpQuery(this.getQualifiedCqpId(), queryResultId, fixedQuery);

			String q = query.getQueryString();
			if (q.matches("undump \"[^\"]+\";")) { // undump query //$NON-NLS-1$
				String path = q.substring(8, q.length() - 2);
				if (!new File(path).exists()) {
					Log.severe(NLS.bind("Error: could not compute subcorpus from the dump file: {0}.", path));
					return null;
				}
				CorpusManager.getCorpusManager().getCqiClient().query(this.getQualifiedCqpId() + ";"); //$NON-NLS-1$
				CorpusManager.getCorpusManager().getCqiClient().query("undump " + queryResultId + " < " + q.substring(7)); //$NON-NLS-1$
			}
			else if (q.matches("(union|join|intersect|inter|difference|diff) [^ ]+ [^ ]+;")) { //$NON-NLS-1$
				CorpusManager.getCorpusManager().getCqiClient().query(this.getQualifiedCqpId() + ";"); //$NON-NLS-1$
				CorpusManager.getCorpusManager().getCqiClient().query(queryResultId + " = " + q); //$NON-NLS-1$
			}
			else {
				//String fixedQuery = CQLQuery.fixQuery(query.getQueryString());

				String m = query.getMatchingStrategy();
				if (m != null) {
					String current = CorpusManager.getCorpusManager().getCqiClient().getOption(CQPLibPreferences.MATCHINGSTRATEGY);
					CorpusManager.getCorpusManager().getCqiClient().setMatchingStrategyNoCheck(m);
					m = current; // store the previous matching strategy
				}
				CorpusManager.getCorpusManager().getCqiClient().cqpQuery(this.getQualifiedCqpId(), queryResultId, CQLQuery.fixQuery(query.getQueryString(), this.getLang()));

				if (m != null) { // restore the default matching strategy
					CorpusManager.getCorpusManager().getCqiClient().setMatchingStrategyNoCheck(m);
				}
			}

			queryResult = new QueryResult(queryResultId, queryResultName, this, query);

			// if (save) {
			// new SavedQuery(this).setParameters(query.toString(), new
			// ArrayList<String>());
			// }
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			String error = "???"; //$NON-NLS-1$
			try {
				error = CorpusManager.getCorpusManager().getCqiClient().getLastCQPError();
			}
			catch (Exception e1) {
				Log.severe(e1);
			}
			throw new CqiClientException(error);
		}
		// long end = System.currentTimeMillis();
		// Log.finest(Messages.RETURN_FROM_QUERY+ " " + queryResult.getNMatch() + " " +
		// (end - start)); //$NON-NLS-1$ //$NON-NLS-2$

		return queryResult;
	}

	/**
	 * Sets the corpus charset name, to be able to print it well.
	 *
	 * @param corpusCharset the new charset
	 */
	public void setCharset(String corpusCharset) {
		this.charset = corpusCharset;
	}

	/**
	 * Set the corpus language name, to be able to sort it well.
	 *
	 * @param corpusLanguage
	 *            the new language
	 */
	public void setLanguage(String corpusLanguage) {
		this.lang = corpusLanguage;
	}

	public Property getVirtualProperty(String s) {
		if (s == null || s.length() == 0) {
			return null;
		}

		for (VirtualProperty p : this.getVirtualProperties()) {
			if (s.equals(p.getName())) {
				return p;
			}
		}

		return null;
	}

	/**
	 * parse a "struct_prop" property name to find the matching StructuralUnitProperty
	 * 
	 * @param p
	 * @return
	 */
	public StructuralUnitProperty getStructuralUnitProperty(String p) {

		try {
			return StructuralUnitProperty.stringToStructuralUnitProperty(this, p);
		}
		catch (CqiClientException e) {
			e.printStackTrace();
			return null;
		}
	}

	public StructuralUnit getTXMCorpusStructure() {
		try {
			return this.getStructuralUnit("txmcorpus");
		} catch (CqiClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
