// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus.query;

// TODO: Auto-generated Javadoc
/**
 * The Class CqpQueryConstant.
 */
public class CqpQueryConstant {

	/** The Constant OR. */
	public final static String OR = "|"; //$NON-NLS-1$

	/** The Constant AND. */
	public final static String AND = "&"; //$NON-NLS-1$

	/** The Constant EQUALS. */
	public final static String EQUALS = "="; //$NON-NLS-1$

	/** The Constant START_ATTRIBUTE. */
	public final static String START_ATTRIBUTE = "["; //$NON-NLS-1$

	/** The Constant END_ATTRIBUTE. */
	public final static String END_ATTRIBUTE = "]"; //$NON-NLS-1$

	/** The Constant END_QUERY_EXPR. */
	public final static String END_QUERY_EXPR = ";"; //$NON-NLS-1$

	/** The Constant START_QUOTE. */
	public final static String START_QUOTE = "'"; //$NON-NLS-1$

	/** The Constant END_QUOTE. */
	public final static String END_QUOTE = "'"; //$NON-NLS-1$

	/** The Constant START_DOUBLE_QUOTE. */
	public final static String START_DOUBLE_QUOTE = "\""; //$NON-NLS-1$

	/** The Constant END_DOUBLE_QUOTE. */
	public final static String END_DOUBLE_QUOTE = "\""; //$NON-NLS-1$

	/** The Constant DOT. */
	public final static String DOT = "."; //$NON-NLS-1$

	/** The Constant TAG_ATTRIBUTE_SEPARATOR. */
	public final static String TAG_ATTRIBUTE_SEPARATOR = "_"; //$NON-NLS-1$

	/** The Constant REGION_MACRO. */
	public final static String REGION_MACRO = "/region"; //$NON-NLS-1$

	/** The Constant START_MACRO_ARGS. */
	public final static String START_MACRO_ARGS = "["; //$NON-NLS-1$

	/** The Constant END_MACRO_ARGS. */
	public final static String END_MACRO_ARGS = "]"; //$NON-NLS-1$

	/** The Constant MACRO_ARGS_SEPARATOR. */
	public final static String MACRO_ARGS_SEPARATOR = ","; //$NON-NLS-1$

	/** The Constant START_GLOBAL_CONSTRAINT. */
	public final static String START_GLOBAL_CONSTRAINT = "::"; //$NON-NLS-1$

}
