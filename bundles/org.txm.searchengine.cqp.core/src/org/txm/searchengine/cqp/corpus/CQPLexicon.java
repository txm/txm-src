// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-09-19 10:31:00 +0200 (Mon, 19 Sep 2016) $
// $LastChangedRevision: 3298 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMParameters;
import org.txm.core.results.TXMResult;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.ICqiClient;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.MatchUtils;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.TXMProgressMonitor;
//import org.txm.statsengine.core.StatException;
//import org.txm.statsengine.core.data.Vector;
//import org.txm.statsengine.r.core.data.VectorImpl;
import org.txm.utils.logger.Log;

//  TODO should be put into stat.data package ?
/**
 * Represent a frequency list according to a {@link CQPCorpus} (or a.
 *
 * {@link Subcorpus}) and a {@link Property}.
 * 
 * @author sloiseau
 */
public class CQPLexicon {


	/** The forms. */
	private String[] forms;

	/** The freqs. */
	private int[] freqs;

	/** The ids. */
	private int[] ids;

	/** The number of tokens. */
	int numberOfTokens = -1;

	/** The symbol. */
	private String symbol;

	/** The writer. */
	private OutputStreamWriter writer;

	/**
	 * The property.
	 */
	protected WordProperty pProperty;

	private CQPCorpus corpus;

	/**
	 * @param corpus
	 */
	public CQPLexicon(CQPCorpus corpus) {
		this.corpus = corpus;
	}

	public boolean _compute() throws Exception {

		if (pProperty instanceof VirtualProperty) {
			return computeWithVirtualProperty(this.getParent(), (VirtualProperty) pProperty);
		}
		else {
			if (this.getParent() instanceof MainCorpus) {
				return computeWithMainCorpus((MainCorpus) this.getParent(), pProperty, null); // uses CQi fdist method
			}
			else if (this.getParent() instanceof Subcorpus) {
				return computewithSubCorpus((Subcorpus) this.getParent(), pProperty, null); // uses CQi fdist method
			}
			else {
				Log.severe("Error: Lexicon parent is neither a Maincorpus nor a Subcorpus."); //$NON-NLS-1$
				return false;
			}
		}
	}

	private boolean computeWithVirtualProperty(CQPCorpus corpus, VirtualProperty property) {

		int[] positions = MatchUtils.toPositions(corpus.getMatches());
		int[] ids2 = property.cpos2Id(positions);
		Arrays.sort(ids2);
		HashSet<Integer> test = new HashSet<Integer>();
		int[] freqs2 = new int[ids2.length]; // max freqs length
		int[] idsHash = new int[ids2.length]; // max idsHash length
		idsHash[0] = ids2[0];
		int n = 0; // max size
		for (int i = 0; i < ids2.length; i++) {
			test.add(ids2[i]);
			if (idsHash[n] != ids2[i]) { // new entry
				n++;
				idsHash[n] = ids2[i];
			}
			freqs2[n] = freqs2[n] + 1;
		}

		ids = new int[n];
		System.arraycopy(idsHash, 0, ids, 0, n);
		freqs = new int[n];
		System.arraycopy(freqs2, 0, freqs, 0, n);

		init(corpus, property, freqs, ids);
		return true;

	}

	/**
	 * Gets the lexicon relative to a given property.
	 * 
	 * @param property the property
	 * 
	 * @return the lexicon
	 * 
	 * @throws CqiClientException the cqi client exception
	 */
	protected boolean computeWithMainCorpus(MainCorpus corpus, WordProperty property, IProgressMonitor monitor) throws CqiClientException {
		// System.out.println("in "+this.getCqpId()+" look for cached lexicon "+property);
		// System.out.println("not found");
		int lexiconSize;
		try {
			lexiconSize = CorpusManager.getCorpusManager().getCqiClient().lexiconSize(property.getQualifiedName());
		}
		catch (Exception e) {
			throw new CqiClientException(e);
		}

		int[] ids = new int[lexiconSize];
		for (int i = 0; i < ids.length; i++) {
			ids[i] = i;
		}

		int[] freqs;
		try {
			freqs = CorpusManager.getCorpusManager().getCqiClient().id2Freq(property.getQualifiedName(), ids);
		}
		catch (Exception e) {
			throw new CqiClientException(e);
		}

		this.init(corpus, property, freqs, ids);
		return true;
	}

	// /**
	// *
	// * @param corpus
	// * @param property
	// * @param monitor
	// * @return
	// * @throws CqiClientException
	// */
	// // FIXME: why this method needs to create and delete some new subcorpus???? the computing can't be done directly on the corpus argument???
	// // eg. dist = CorpusManager.getCorpusManager().getCqiClient().fdist1(corpus.getQualifiedCqpId(), 0, ICqiClient.CQI_CONST_FIELD_MATCH, property.getName());
	// protected boolean computewithSubCorpus(Subcorpus corpus, Property property, IProgressMonitor monitor) throws CqiClientException {
	//
	// //System.out.println("not found");
	//// Log.finest(NLS.bind(IndexCoreMessages.computingTheLexiconOfSubcorpusP0, corpus.getName()));
	// //long start = System.currentTimeMillis();
	// int[][] fdist = null;
	// AbstractCqiClient cqi = CorpusManager.getCorpusManager().getCqiClient();
	// String tmp = "TMP" + CQPCorpus.getNextSubcorpusCounter();
	// String qtmp = corpus.getMainCorpus().getQualifiedCqpId()+":"+tmp;
	// try {
	//// System.out.println("subcorpus: "+corpus.getQualifiedCqpId());
	//// System.out.println("query subcorpus: "+qtmp);
	// cqi.cqpQuery(corpus.getQualifiedCqpId(), tmp, "[]"); //$NON-NLS-1$
	// fdist = CorpusManager.getCorpusManager().getCqiClient().fdist1(qtmp, 0, ICqiClient.CQI_CONST_FIELD_MATCH, property.getName());
	// //System.out.println("nb lines: "+fdist.length);
	// } catch (Exception e) {
	// throw new CqiClientException(e);
	// } finally {
	// try {
	// cqi.dropSubCorpus(qtmp);
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (CqiServerError e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
	// int lexiconSize = fdist.length;
	//
	// int[] freqs = new int[lexiconSize];
	// int[] ids = new int[lexiconSize];
	// for (int i = 0; i < fdist.length; i++) {
	// ids[i] = fdist[i][0];
	// freqs[i] = fdist[i][1];
	// }
	//
	// init(corpus, property, freqs, ids);
	// return true;
	// }

	/**
	 * 
	 * @param corpus
	 * @param property
	 * @param monitor
	 * @return
	 * @throws CqiClientException
	 */
	// FIXME: SJ: why this method needs to create and delete some new subcorpus? the computing can't be done directly on the corpus argument?
	// eg. dist = CorpusManager.getCorpusManager().getCqiClient().fdist1(corpus.getQualifiedCqpId(), 0, ICqiClient.CQI_CONST_FIELD_MATCH, property.getName());
	protected boolean computewithSubCorpus(Subcorpus corpus, WordProperty property, IProgressMonitor monitor) throws CqiClientException {

		// System.out.println("not found");
		// Log.finest(NLS.bind(IndexCoreMessages.computingTheLexiconOfSubcorpusP0, corpus.getName()));
		// long start = System.currentTimeMillis();
		int[][] fdist = null;

		boolean fast = false;

		// FIXME: SJ: old version
		if (!fast) {

			AbstractCqiClient cqi = CorpusManager.getCorpusManager().getCqiClient();
			String tmp = "TMP" + CQPCorpus.getNextSubcorpusCounter(); //$NON-NLS-1$
			String qtmp = corpus.getMainCorpus().getQualifiedCqpId() + ":" + tmp; //$NON-NLS-1$

			try {
				// System.out.println("subcorpus: "+corpus.getQualifiedCqpId());
				// System.out.println("query subcorpus: "+qtmp);
				if (tmp.length() > CQLQuery.MAX_CQL_LENGTH) {
					Log.warning(NLS.bind("Error: CQL length limit reached ({0}) with {1} length.", CQLQuery.MAX_CQL_LENGTH, tmp.length()));
					return false;
				}
				cqi.cqpQuery(corpus.getQualifiedCqpId(), tmp, "[]"); //$NON-NLS-1$
				fdist = CorpusManager.getCorpusManager().getCqiClient().fdist1(qtmp, 0, ICqiClient.CQI_CONST_FIELD_MATCH, property.getName());
				// System.out.println("nb lines: "+fdist.length);
			}
			catch (Exception e) {
				throw new CqiClientException(e);
			}
			finally {
				try {
					cqi.dropSubCorpus(qtmp);
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (CqiServerError e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		// FIXME: SJ: new version by MD that seems fastest, need to test on all types of lexicons corpora before replacing old version
		else {

			try {
				int[] positions = MatchUtils.toPositions(corpus.getMatches());
				int[] indexes = CorpusManager.getCorpusManager().getCqiClient().cpos2Id(property.getQualifiedName(), positions);
				int n_indexes = CorpusManager.getCorpusManager().getCqiClient().attributeSize(property.getQualifiedName());
				int[] counts = new int[n_indexes];
				for (int i : indexes) {
					counts[i]++;
				}
				fdist = new int[indexes.length][2];

				int n = 0;
				for (int i : indexes) {
					fdist[n][0] = i;
					fdist[n][1] = counts[i];
					n++;
				}
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		int lexiconSize = fdist.length;

		int[] freqs = new int[lexiconSize];
		int[] ids = new int[lexiconSize];
		for (int i = 0; i < fdist.length; i++) {
			ids[i] = fdist[i][0];
			freqs[i] = fdist[i][1];
		}

		init(corpus, property, freqs, ids);
		return true;
	}


	/**
	 * Compute number of tokens. / this.nbr
	 */
	private void computeNumberOfTokens() {
		numberOfTokens = 0;
		for (int i = 0; i < freqs.length; i++) {
			numberOfTokens += freqs[i];
			// System.out.println(numberOfTokens);
			// if (freqs[i] != 1) System.out.println(freqs[i]);
		}
	}

	/**
	 * Dump lexicon forms and frequencies in a String.
	 *
	 * @param col the col
	 * @param txt the txt
	 * @return the string
	 */
	public String dump(String col, String txt) {
		StringBuffer buffer = new StringBuffer();
		getForms();
		for (int i = 0; i < forms.length; i++) {
			buffer.append(txt + forms[i].replace(txt, txt + txt) + txt + col + freqs[i] + "\n"); //$NON-NLS-1$
		}
		return buffer.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof CQPLexicon)) {
			return false;
		}
		CQPLexicon other = (CQPLexicon) obj;

		if (other.nbrOfType() != this.nbrOfType()) {
			return false;
		}
		return (Arrays.equals(freqs, other.getFreq()) && Arrays.equals(getForms(), other.getForms()));
	}

	/**
	 * The corpus or subcorpus this lexicon is build on.
	 * 
	 * @return the corpus
	 */
	public CQPCorpus getCorpus() {
		return corpus;
	}


	public String getDetails() {
		return this.getParent().getName() + " " + this.pProperty.getName(); //$NON-NLS-1$
	}

	// TODO: move this into a Lexicon chart renderer
	// /**
	// * Draw a pareto graphic with this frequency list and record it into the
	// * provided filename into svg format.
	// *
	// * @param file where to save the pareto graphic.
	// * @return the pareto graphic
	// * @throws StatException if anything goes wrong.
	// */
	// public void getParetoGraphic(File file) throws StatException {
	// String rName = asVector().getSymbol();
	// String expr = "pareto(" + rName + ")"; //$NON-NLS-1$ //$NON-NLS-2$
	// try {
	// RWorkspace.getRWorkspaceInstance().plot(file, expr, RDevice.SVG);
	// } catch (Exception e) {
	// throw new StatException(e);
	// }
	// }

	/**
	 * The different types in the lexicon, the type at the index <code>j</code>
	 * of this array have the frequency at index <code>j</code> in the array
	 * returned by {@link #getFreq()}.
	 * 
	 * @return types as an array of <code>String</code>
	 */
	public String[] getForms() {
		
		if (forms == null) {
			if (ids == null) {
				return new String[0];
			}
			try {
				if (pProperty instanceof WordProperty) {
					forms = pProperty.id2Str(ids);
				}
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return forms;
	}

	/**
	 * The dif ferent types in the lexicon, the type at the index <code>j</code>
	 * of this array have the frequency at index <code>j</code> in the array
	 * returned by {@link #getFreq()}.
	 *
	 * @param number the number
	 * @return types as an array of <code>String</code>
	 */
	public String[] getForms(int number) {
		// System.out.println("Lexicon("+this.property+" get forms. number="+number+", ids len="+ids.length);
		if (forms == null) {
			try {
				number = Math.min(number, ids.length);
				if (number <= 0) {
					return new String[0];
				}
				int[] subpositions = new int[number];
				System.arraycopy(ids, 0, subpositions, 0, number);
				return CorpusManager.getCorpusManager().getCqiClient().id2Str(pProperty.getQualifiedName(), subpositions);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
				return null;
			}
		}
		else {
			number = Math.min(number, ids.length);
			if (number <= 0) {
				return new String[0];
			}
			String[] subforms = new String[number];
			System.arraycopy(ids, 0, subforms, 0, number);
			return subforms;
		}
	}

	/**
	 * The dif ferent frequencies in the lexicon. See {@link #getForms()}.
	 * 
	 * @return frequencies as an array of <code>int</code>
	 */
	public int[] getFreq() {
		return freqs;
	}

	/**
	 * return the ids of the entries.
	 *
	 * @return types as an array of <code>String</code>
	 */
	public int[] getIds() {
		return ids;
	}

	public String getName() {
		try {
			return this.corpus.getSimpleName() + ": " + this.getSimpleName(); //$NON-NLS-1$
		}
		catch (Exception e) {
		}
		return ""; //$NON-NLS-1$
	}

	/**
	 * The property this lexicon is build on.
	 * 
	 * @return the property
	 */
	public Property getProperty() {
		return pProperty;
	}

	public String getSimpleName() {
		return this.getProperty().getName();
	}

	/**
	 * Gets the symbol.
	 *
	 * @return the symbol
	 */
	public String getSymbol() {
		return this.symbol;
	}

	/**
	 * Hack frequencies using a map to set forms and frequencies
	 *
	 * @param map the map
	 */
	public boolean hack(Map<String, Integer> map) {
		if (map.size() != forms.length) {
			return false;
		}

		// super(corpus);
		int size = map.size();
		int[] freqs = new int[size];
		String[] forms = map.keySet().toArray(new String[] {});
		for (int i = 0; i < forms.length; i++) {
			freqs[i] = map.get(forms[i]);
		}

		this.freqs = freqs;
		return true;
	}

	/**
	 * Protected on purpose: should be accessed through others initializer.
	 *
	 * @param corpus the corpus
	 * @param property the property
	 * @param freq the freq
	 * @param ids the ids list
	 */
	protected void init(TXMResult corpus, WordProperty property, int[] freq, int[] ids) {
		if (freq.length != ids.length) {
			throw new IllegalArgumentException("wrong size freq.length != ids.length " + freq.length + " != " + ids.length);
		}
		this.freqs = freq;
		this.ids = ids;
		this.forms = getForms();
		this.pProperty = property;
	}


	/**
	 * Number of tokens (sum of all the frequencies) in the corpus.
	 * 
	 * @return the size of the corpus or subcorpus.
	 */
	public int nbrOfToken() {
		if (numberOfTokens <= 0) {
			computeNumberOfTokens();
		}
		return numberOfTokens;
	}


	/**
	 * Number of different types in the frequency list.
	 * 
	 * @return number of types in the corpus or subcorpus.
	 */
	public int nbrOfType() {
		try {
			return freqs.length;
		}
		catch (Exception e) {
			return 0;
		}
	}

	public void setParameters(WordProperty property) {
		this.pProperty = property;
	}

	public boolean setParameters(TXMParameters parameters) {
		try {
			WordProperty p = (WordProperty) parameters.get("properties"); //$NON-NLS-1$
			this.setParameters(p);
		}
		catch (Exception e) {
			Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	/**
	 * Sets the symbol.
	 *
	 * @param symbol the new symbol
	 */
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	@Override
	public String toString() {
		return this.getName();
	}

	/**
	 * To txt.
	 *
	 * @param outfile the outfile
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @return true, if successful
	 */
	@Deprecated
	public boolean toTxt(File outfile, String encoding, String colseparator, String txtseparator) {
		// NK: writer declared as class attribute to perform a clean if the operation is interrupted
		// OutputStreamWriter writer;
		try {
			this.writer = new OutputStreamWriter(new FileOutputStream(outfile),
					encoding);
		}
		catch (UnsupportedEncodingException e1) {
			org.txm.utils.logger.Log.printStackTrace(e1);
			return false;
		}
		catch (FileNotFoundException e1) {
			org.txm.utils.logger.Log.printStackTrace(e1);
			return false;
		}

		try {
			writer.write(this.dump(colseparator, txtseparator));
			writer.close();
		}
		catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}

		return true;
	}

	/**
	 * Sets the unit property.
	 * 
	 * @param property the unit property
	 */
	public void setProperty(WordProperty property) {
		this.pProperty = property;
	}


	public static CQPLexicon getLexicon(CQPCorpus corpus, WordProperty property, TXMProgressMonitor monitor, boolean visible) throws Exception {
		CQPLexicon lexicon = null;

		property = corpus.getProperty(property.getFullName()); // ensure to use the subcorpus property

		// recycling Lexicon if exists
		ArrayList<CQPLexicon> cachedLexicons = corpus.getObjectCacheForClass(CQPLexicon.class);
		for (int i = 0; i < cachedLexicons.size(); i++) {
			if (cachedLexicons.get(i).getProperty() == property) {
				lexicon = cachedLexicons.get(i);
				break;
			}
		}

		// creating new Lexicon
		if (lexicon == null || !lexicon.getProperty().getFullName().equals(property.getFullName())) {
			lexicon = new CQPLexicon(corpus);
			lexicon.setProperty(property);
			lexicon._compute();

			// and store it !
			ArrayList<CQPLexicon> cache = corpus.getObjectCacheForClass(CQPLexicon.class);
			cache.add(lexicon);
		}

		Log.finest(TXMCoreMessages.bind("Lexicon = {0} {1}.", lexicon, lexicon.hashCode())); //$NON-NLS-1$

		return lexicon;
	}


	/**
	 * Gets a Lexicon from the specified corpus.
	 * If a Lexicon child exists in the Corpus from the specified property, returns it otherwise creates and computes a new Lexicon.
	 * 
	 * @param corpus
	 * @param property
	 * @return
	 * @throws Exception
	 */
	public static CQPLexicon getLexicon(CQPCorpus corpus, WordProperty property, TXMProgressMonitor monitor) throws Exception {
		return getLexicon(corpus, property, monitor, true);
	}


	// /**
	// * Find or build a lexicon given a Corpus (MainCorpus or SubCorpus).
	// *
	// * @param corpus
	// * @param property
	// * @return a Lexicon. May return null if the lexicon forms or freqs are null.
	// * @throws Exception
	// */
	// public static Lexicon getLexicon(Corpus corpus, Property property) throws Exception {
	// HashSet<Object> results = corpus.getStoredData(Lexicon.class);
	// for (Object result : results) {
	// Lexicon lex = (Lexicon)result;
	// if (lex.getProperty().equals(property)) {
	// return lex;
	// }
	// }
	//
	// Lexicon lex = new Lexicon(corpus);
	// lex.setParameters(property);
	// if (lex.compute(null) && lex.getForms() != null && lex.getFreq() != null) {
	// corpus.storeData(lex);
	// return lex;
	// } else {
	// return null;
	// }
	// }

	public CQPCorpus getParent() {
		return corpus;

	}
}
