package org.txm.searchengine.cqp.corpus;

import java.text.Collator;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.searchengine.core.messages.SearchEngineCoreMessages;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.QueryPart;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * Partition build with a selection of values on a structure
 * 
 * @author mdecorde
 *
 */
public class StructuredPartition extends Partition {

	/** The property, if not null the partition will forge its own queries */
	@Parameter(key = TXMPreferences.STRUCTURAL_UNIT_PROPERTY)
	protected StructuralUnitProperty pProperty;

	/**
	 * The parts property values to use, may be null.
	 */
	@Parameter(key = TXMPreferences.VALUES)
	protected List<String> pValues;

	/**
	 * 
	 * @param parent
	 */
	public StructuredPartition(CQPCorpus parent) {
		super(parent);
	}

	/**
	 * 
	 * @param parametersNodePath
	 */
	public StructuredPartition(String parametersNodePath) {
		super(parametersNodePath);
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		// create the parts only if they not already exist
		if (this.getPartsCount() == 0) {

			_compute_with_property(monitor);
		}
		this.totalsize = -1; // refresh total size at next call of getTotalSize
		return true;
	}

	/**
	 * Creates a new partition on a corpus given a structure, a property of this
	 * structure and a list of values this property can take
	 * 
	 * The partition has as many parts as the size of the list of value. The ith
	 * part is composed by all the structure <code>structure</code> such as
	 * <code>strucutre.getValue(property) == values.get(i)</code>
	 * 
	 * @throws Exception
	 */

	protected boolean _compute_with_property(TXMProgressMonitor monitor) throws Exception {

		CQPCorpus corpus = this.getParent();
		if (this.getUserName() == null || this.getUserName().length() == 0) {
			this.setUserName(corpus.getName() + "_" + pProperty.getFullName()); //$NON-NLS-1$
		}

		Log.finest(NLS.bind(SearchEngineCoreMessages.info_creatingNewPartitionP0OfP1, this.getUserName(), this.getParent()));

		List<String> values = pValues;
		if (values == null) {
			values = pProperty.getValues(corpus);
		}

		// sort parts
		HashMap<String, String> infos = this.getParent().getSAttributesInfos().get(pProperty.getFullName());

		String type = "String"; //$NON-NLS-1$
		if (infos != null) {
			type = infos.get("type"); //$NON-NLS-1$
		}
		if (type == null || type.trim().length() == 0 || type.equals("String")) { //$NON-NLS-1$
			Collections.sort(values, Collator.getInstance(Locale.getDefault())); // alpha sort
		}
		else if (type.equals("Integer")) { //$NON-NLS-1$
			Collections.sort(values, new Comparator<String>() {

				@Override
				public int compare(String arg0, String arg1) {
					int i0 = Integer.parseInt(arg0);
					int i1 = Integer.parseInt(arg1);
					return i0 - i1;
				}
			});
		}
		else if (type.equals("Date")) { //$NON-NLS-1$
			String format = infos.get("inputFormat"); //$NON-NLS-1$
			final DateFormat formater = new SimpleDateFormat(format);
			Collections.sort(values, new Comparator<String>() {

				@Override
				public int compare(String arg0, String arg1) {
					try {
						Date i0 = formater.parse(arg0);
						Date i1 = formater.parse(arg1);
						return i0.compareTo(i1);
					}
					catch (ParseException e) {
						return arg0.compareTo(arg1);
					}
				}
			});
		}
		else if (type.contains("|")) { //$NON-NLS-1$
			final List<String> sortedValues = Arrays.asList(type.split("\\|")); //$NON-NLS-1$
			Collections.sort(values, new Comparator<String>() {

				@Override
				public int compare(String arg0, String arg1) {
					int i0 = sortedValues.indexOf(arg0);
					int i1 = sortedValues.indexOf(arg1);
					return i0 - i1;
				}
			});
		}

		pQueries = new ArrayList<>();
		pPartNames = new ArrayList<>();
		for (String value : values) {
			String partName = value.replace("\\", ""); //$NON-NLS-1$ //$NON-NLS-2$
			pPartNames.add(partName);
			CQLQuery query = new QueryPart(pProperty.getStructuralUnit(), pProperty, value); // second option is faster
			pQueries.add(query.getQueryString());
		}

		return _compute_with_lists(monitor);
	}

	@Override
	public boolean canCompute() {
		return this.getParent() != null && this.pProperty != null; // if pValues is not set, all values are used
	}

	/**
	 * Gets the property.
	 *
	 * @return the property
	 */
	public List<String> getValues() {
		if (pValues == null) return null;
		return new ArrayList<>(pValues);
	}

	/**
	 * Gets the property.
	 *
	 * @return the property
	 */
	public Property getProperty() {
		return pProperty;
	}

	/**
	 * Gets the structure.
	 *
	 * @return the structure
	 */
	public StructuralUnit getStructure() {
		if (pProperty == null) return null;
		return pProperty.getStructuralUnit();
	}

	/**
	 * Gets the structure.
	 *
	 * @return the structure
	 */
	public StructuralUnitProperty getStructuralUnitProperty() {
		if (pProperty == null) return null;
		return pProperty;
	}

	@Override
	public boolean loadParameters() {
		String tmp = null;
		try {
			tmp = this.getStringParameterValue(TBXPreferences.STRUCTURAL_UNIT_PROPERTY);
			if (!tmp.isEmpty()) {
				this.pProperty = StructuralUnitProperty.stringToStructuralUnitProperty(this.getParent(), tmp);
			}
		}
		catch (CqiClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		tmp = this.getStringParameterValue(TXMPreferences.VALUES);
		if (tmp != null) {
			this.pValues = new ArrayList<>(Arrays.asList(tmp.split("\t")));
		}
		else {
			this.pValues = null;
		}
		return true;
	}

	@Override
	public boolean saveParameters() {

		if (this.pProperty != null) {
			this.saveParameter(TXMPreferences.STRUCTURAL_UNIT_PROPERTY, this.pProperty.getFullName());
		}

		if (this.pValues != null) {
			this.saveParameter(TBXPreferences.VALUES, StringUtils.join(pValues, "\t"));
		}

		return true;
	}

	public boolean setParameters(String name, StructuralUnitProperty property, List<String> values) {
		this.pProperty = property;
		this.pValues = values;
		this.setUserName(name);
		return true;
	}
}
