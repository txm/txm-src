package org.txm.searchengine.cqp.corpus.query;

import java.util.ArrayList;
import java.util.List;

import org.txm.objects.Match;

public class MatchUtils {

	/**
	 * 
	 * @param list the matches to cover
	 * @param b the matches that covers
	 * @return the indexes of 'b' matches that covers the 'a' matches
	 */
	public static ArrayList<Integer> matchesCoveredByB(List<Match> list, List<Match> b) {
		int ai = 0, bi = 0;
		ArrayList<Integer> result = new ArrayList<>();

		while (ai < list.size() && bi < b.size()) {
			if (b.get(bi).contains(list.get(ai))) {
				result.add(bi);
				bi++;
			}
			else {
				ai++;
			}
		}

		return result;
	}

	/**
	 * Gets an array of all positions of the specified list of matches.
	 * 
	 * @param list the list of matches
	 * @return an array of all positions of the specified list of matches
	 */
	public static int[] toPositions(List<? extends Match> list) {
		ArrayList<Integer> result = new ArrayList<>();
		for (Match m : list) {
			if (m.getStart() == m.getEnd()) {
				result.add(m.getStart());
			}
			else {
				for (int i = m.getStart(); i <= m.getEnd(); i++) {
					result.add(i);
				}
			}
		}

		int[] positions = new int[result.size()];
		for (int i = 0; i < result.size(); i++) {
			positions[i] = result.get(i);
		}
		return positions;
	}

	/**
	 * 
	 * @param match
	 * @return array of int position from a Match
	 */
	public static int[] toPositions(Match match) {
		return toPositions(match, match);
	}

	/**
	 * 
	 * @param match1
	 * @param match2
	 * @return array of int position from a Match start to another Match end
	 */
	public static int[] toPositions(Match match1, Match match2) {
		return toPositions(match1.getStart(), match2.getEnd());
	}

	/**
	 * 
	 * @param from
	 * @param to
	 * @return array of int position from a Match start to another Match end
	 */
	public static int[] toPositions(int from, int to) {
		int len = to - from + 1;

		if (len <= 0) {
			return new int[0];
		}
		else if (len == 1) {
			return new int[] { from };
		}
		else if (len == 2) {
			return new int[] { from, to };
		}
		else {
			int[] positions = new int[len];
			int n = 0;
			for (int i = from; i <= to; i++) {
				positions[n++] = i;
			}
			return positions;
		}
	}
}
