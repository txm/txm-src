// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (Tue, 24 Jan 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp.corpus;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.txm.Toolbox;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.importer.cwb.PatchCwbRegistry;
import org.txm.importer.cwb.ReadRegistryFile;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Project;
import org.txm.searchengine.core.messages.SearchEngineCoreMessages;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.InvalidCqpIdException;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.DeleteDir;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;
import org.w3c.dom.Element;

/**
 * The Class MainCorpus for mirroring CQP corpora.
 *
 * @author jmague, mdecorde
 */
public class MainCorpus extends CQPCorpus {

	protected File cqpFile;

	protected File dataDirectory;

	protected boolean modified = false;

	protected File registryFile;

	/**
	 * CQP text ids cache
	 */
	protected String[] cqpTextIDS = null;

	/**
	 * <del>Instantiates a new sub corpus</del>
	 * 
	 * @param partition the parent partition -> only used for subcorpus
	 */
	// FIXME: SJ: not used anymore? MD: this is a temporary solution to build Parts using a Partition (a Part is a CQPCorpus, etc.)
	public MainCorpus(Partition partition) {
		super(partition);
		throw new IllegalArgumentException("Maincorpus parent must not be a Partition");
	}

	/**
	 * Instantiates a new corpus. This constructor is not intended to be used
	 * directly. Use {@link CorpusManager#getCorpus(String)}.
	 * 
	 * @throws InvalidCqpIdException the invalid cqp id exception
	 * @throws CqiClientException the cqi client exception
	 */
	public MainCorpus(Project project) throws InvalidCqpIdException, CqiClientException {
		super(project);
	}

	/**
	 * Instantiates a new corpus. This constructor is not intended to be used
	 * directly. Use {@link CorpusManager#getCorpus(String)}.
	 * 
	 * @param parametersNodePath the preference node path
	 * 
	 * @throws InvalidCqpIdException the invalid CQP id exception
	 * @throws CqiClientException the cqi client exception
	 */
	public MainCorpus(String parametersNodePath) throws InvalidCqpIdException, CqiClientException {
		super(parametersNodePath);

		if (getID() != null && getProjectDirectory() != null) {
			try {
				if (!compute(false)) {
					Log.warning(NLS.bind("Warning: the {0} corpus won't work correctly", this.getID()));
					throw new IllegalStateException(NLS.bind("{0} CQP MainCorpus not instanciate correctly.", this.getID()));
				}
			}
			catch (InterruptedException e) {
				throw new IllegalStateException(e);
			}
		}
	}

	@Override
	protected boolean __compute(TXMProgressMonitor monitor) throws Exception {

		if (!CQPSearchEngine.isInitialized()) {
			Log.warning("** TXM can't load MainCorpus when CQP search engine is not ready.");
			return false;
		}

		if (this.pID == null || this.pID.length() == 0) {
			Log.severe("** TXM can't load this MainCorpus: no CQP ID set.");
			return false;
		}

		HashMap<String, MainCorpus> corpora = CorpusManager.getCorpusManager().getCorpora();
		if (CorpusManager.getCorpusManager().hasCorpus(this)) {
			Log.fine(NLS.bind("** The \"{0}\" MainCorpus object in the \"{1}\" project can not be computed: another one with the same CQP identifier has already been computed.",
					this.pID, this.getProjectDirectory()));
			return true;
		}
		CorpusManager.getCorpusManager().add(this);

		cqpTextIDS = null; // reset CQP text ids

		this.dataDirectory = new File(getProjectDirectory(), "data/" + getID()); //$NON-NLS-1$
		this.registryFile = new File(getProjectDirectory(), "registry/" + getID().toLowerCase()); //$NON-NLS-1$

		if (!dataDirectory.exists()) {
			Log.warning("MainCorpus data files not found: " + dataDirectory);
			return false;
		}
		if (!registryFile.exists()) {
			Log.warning("MainCorpus registry file not found: " + registryFile);
			return false;
		}
		try {
			try { // fix the absolute paths in the registry file
				PatchCwbRegistry.patch(this.registryFile, this.dataDirectory);
			}
			catch (IOException e) {
				Log.severe(TXMCoreMessages.bind("Error while updating the {0} registry file.", this.registryFile));
				Log.printStackTrace(e);
				return false;
			}

			// check if all corpus index files are present
			ReadRegistryFile rrf = new ReadRegistryFile(this.registryFile);
			ArrayList<String> errors = rrf.isCorpusBuildValid(this.dataDirectory);
			if (errors.size() > 0) {
				Log.warning(TXMCoreMessages.bind("Error: some {0} index files are missing : {1}.", this.getID(), StringUtils.join(errors, ", ")));
				return false;
			}

			Log.fine(NLS.bind("Call CQI: load_a_system_corpus with {0} and {1}.", this.registryFile.getParent(), this.pID)); //$NON-NLS-1$
			if (!CQPSearchEngine.getCqiClient().load_a_system_corpus(this.registryFile.getParent(), this.pID)) {
				Log.warning(TXMCoreMessages.bind("Error: CQI.load_a_system_corpus failed with id={0} and registry={1}.", this.getID(), this.registryFile));
				return false;
			}

			List<String> tmp = Arrays.asList(CQPSearchEngine.getCqiClient().listCorpora());
			if (tmp.contains(this.pID)) {
				Log.fine("Corpus registered: " + pID); //$NON-NLS-1$
				Log.fine(NLS.bind("Call CQI: corpusProperties with {0}.", this.pID)); //$NON-NLS-1$
				try {
					String[] props = CQPSearchEngine.getCqiClient().corpusProperties(this.pID);
					Log.fine(NLS.bind("Corpus {0} loaded with properties: {1}.", pID, Arrays.asList(props))); //$NON-NLS-1$
				}
				catch (Exception e) {
					Log.warning(TXMCoreMessages.bind("Error while loading the {0} corpus: {1}.", pID, e.getMessage()));
					return false;
				}
			}
			else {
				Log.severe(TXMCoreMessages.bind("Error while loading the {0} corpus. Not found in {1}.", this.pID, tmp));
				return false;
			}

			corpora.put(this.pID, this); // register the corpus
		}
		catch (Exception e) {
			Log.severe("MainCorpus not loaded: " + e);
			if (corpora.get(this.pID) == this) {
				corpora.remove(this.pID); // unregister the broken corpus
			}
			return false;
		}

		//		// tests to enable CQP dynamic editions		
		//		if (!this.getProject().hasEditionDefinition("cqp")) {
		//			this.getProject().getEditionDefinition("cqp").setPageBreakTag("p");
		//			this.getProject().getEditionDefinition("cqp").setBuildEdition(true);
		//		}
		//		for (Text t : this.getProject().getTexts()) {
		//			List<CQPEdition> cqpedition = t.getChildren(CQPEdition.class);
		//			if (cqpedition.size() == 0) {
		//				CQPEdition e = new CQPEdition(t);
		//				e.saveParameters();
		//				e.compute();
		//			}
		//		}

		return true;
	}


	@Override
	public String getComputingStartMessage() {
		if (Log.isLoggingFineLevel()) {
			return TXMCoreMessages.bind(SearchEngineCoreMessages.info_computingCorpusP0, this.getName());
		}
		else {
			return ""; //$NON-NLS-1$
		}
	}


	@Override
	public String getComputingDoneMessage() {
		if (Log.isLoggingFineLevel()) {
			return super.getComputingDoneMessage();
		}
		else {
			return ""; //$NON-NLS-1$
		}
	}



	@Override
	public void onProjectClose() {
		try {
			if (this.hasBeenComputedOnce() && this.pID != null) {
				CorpusManager.getCorpusManager().deleteCorpus(this);
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.searchengine.cqp.corpus.Corpus#load()
	 */
	@Override
	public boolean _load(Element e) {
		boolean ret = super._load(e);

		return ret;
	}


	@Override
	public boolean canCompute() throws Exception {
		return this.pID != null && this.pID.length() > 0;
	}

	/**
	 * Check whether the ID is a valid CQP ID.
	 * 
	 * @param pID
	 *            the cqp id
	 * 
	 * @return true, if pID is a valid CQP ID.
	 * 
	 * @throws InvalidCqpIdException
	 *             the invalid cqp id exception
	 */
	@Override
	public boolean checkCqpId(String pID) throws InvalidCqpIdException {
		if (!pID.equals(pID.toUpperCase()))
			throw new InvalidCqpIdException(pID + TXMCoreMessages.isNotAValidCQPIDForACorpusItMustBeInUppercaseCharacters);
		return true;
	}

	@Override
	public void clean() {
		super.clean();

		if (CorpusManager.getCorpusManager().getCorpora().get(this.pID) == this) { // unregister the MainCorpus
			CorpusManager.getCorpusManager().getCorpora().remove(this.pID);
		}

		try {
			if (CQPSearchEngine.isInitialized()) {
				CQPSearchEngine.getCqiClient().dropCorpus(getID());
			}
		}
		catch (Exception e) {
			Log.fine(e.getLocalizedMessage());
			// Log.printStackTrace(e);
		}

		if (dataDirectory != null) {
			DeleteDir.deleteDirectory(dataDirectory);
		}
		if (registryFile != null) {
			registryFile.delete();
		}

		Toolbox.notifyEngines(this, "clean"); //$NON-NLS-1$

		cqpFile = null;

		dataDirectory = null;
		nbtext = -1;
		registryFile = null;
		textEndLimits = null;
		textids = null;
	}

	public boolean equals(MainCorpus corpus) {
		if (corpus == null) return false;
		return this.getQualifiedCqpId().equals(corpus.getQualifiedCqpId());
	}

	/**
	 * Find text no.
	 *
	 * @param textid the textid
	 * @return the int
	 * @throws CqiClientException the cqi client exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 */
	public int findTextNo(String textid) throws CqiClientException, IOException, CqiServerError {
		HashMap<String, Integer> idsmap = this.getTextsIds();
		if (idsmap.containsKey(textid))
			return idsmap.get(textid);
		else
			return -1;
	}

	@Override
	public String getDetails() {
		try {
			return this.getName() + " T=" + NumberFormat.getInstance().format(this.getSize());
		}
		catch (CqiClientException e) {
			return "Corpus not ready.";
		}
	}

	/**
	 * Gets the first position.
	 *
	 * @return the first position
	 */
	public int getFirstPosition() {
		return 0;
	}

	/**
	 * Gets the full name.
	 * 
	 * @return the full name
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public String getFullName() throws CqiClientException {
		try {
			return CorpusManager.getCorpusManager().getCqiClient().corpusFullName(this.pID);
		}
		catch (Exception e) {
			throw new CqiClientException(e);
		}

	}

	@Override
	public List<Match> getMatches() {
		List<Match> rez = new ArrayList<>();
		try {
			rez.add(new Match(0, this.getSize() - 1));
			return rez;
		}
		catch (CqiClientException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return null;
	}

	public int getNMatch() {
		return 1;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.txm.searchengine.cqp.corpus.CqpObject#getQualifiedCqpId()
	 */
	@Override
	public String getQualifiedCqpId() {
		return pID;
	}

	@Override
	public String getResultType() {
		return "Corpus";
	}

	@Override
	public String getSimpleName() {
		if (modified) {
			return getName() + "*"; //$NON-NLS-1$
		}
		else {
			return getName();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.searchengine.cqp.corpus.Corpus#getSize()
	 */
	@Override
	public int getSize() throws CqiClientException {
		if (!this.hasBeenComputedOnce()) return 0;

		if (this.size == -1) {
			try {
				this.size = CorpusManager.getCorpusManager().getCqiClient()
						.attributeSize(this.pID + ".word"); //$NON-NLS-1$
			}
			catch (Exception e) {
				throw new CqiClientException(e);
			}
		}
		return this.size;
	}

	/**
	 * Return the CQP START positions of the result matches of the 'cql_limit' query
	 */
	@Override
	public int[] getStartLimits(String cql_limit) throws IOException, CqiServerError, InvalidCqpIdException, CqiClientException {
		String queryResultId = queryResultNamePrefix + UUID.randomUUID().toString();
		CorpusManager.getCorpusManager().getCqiClient().cqpQuery(
				this.getQualifiedCqpId(), queryResultId, CQLQuery.fixQuery(cql_limit, this.getLang())); // $NON-NLS-1$
		QueryResult queryResult = new QueryResult(queryResultId, queryResultId, this, new CQLQuery(cql_limit));

		int[] rez = queryResult.getStarts();
		queryResult.drop();
		return rez;
	}


	/*
	 * (non-Javadoc)
	 * @see
	 * org.txm.searchengine.cqp.corpus.Corpus#getStructuralUnit(java
	 * .lang.String)
	 */
	@Override
	public StructuralUnit getStructuralUnit(String name) throws CqiClientException {
		if (this.structuralUnits == null) {
			getStructuralUnits();
		}
		for (StructuralUnit unit : this.structuralUnits) {
			if (unit.getName().equals(name)) {
				return unit;
			}
		}
		return null;
	}

	/**
	 * Gets the structural attributes available in this corpus.
	 * 
	 * @return the properties
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	@Override
	public List<StructuralUnit> getStructuralUnits() throws CqiClientException {
		if (this.structuralUnits != null)
			return this.structuralUnits;

		// get structures' name
		Set<String> cqpNames;
		try {
			String[] names = CorpusManager.getCorpusManager().getCqiClient()
					.corpusStructuralAttributes(this.pID);
			cqpNames = new HashSet<>(Arrays.asList(names));
		}
		catch (Exception e) {
			throw new CqiClientException(e);
		}
		// What we call a structural unit is called a structural attribute by
		// cqp and what we call
		// an attribute of a strucutural unit has no corresponding concept in
		// cqp :
		// if a structural unit, say 's', has 2 attributes, say 'n' and
		// 'length', the 2 attributes
		// are stored by cqp as embedded structural units names 's_n' and
		// 's_length' (see the cqp documentation)
		// Here, we go through all the structural attribute known by cqp to
		// decide which is a structural unit and which
		// an attribute of a structural unit

		Map<String, List<String>> attributes = new HashMap<>();

		for (String name : cqpNames) {
			if (!attributes.containsKey(name)) {
				int ex = name.lastIndexOf("_"); //$NON-NLS-1$
				if (ex == -1 || !cqpNames.contains(name.substring(0, ex))) {
					attributes.put(name, new ArrayList<String>());
				}
				else {
					String unitName = name.substring(0, ex);
					String attributeName = name.substring(ex + 1);
					if (!attributes.containsKey(unitName)) {
						attributes.put(unitName, new ArrayList<String>());
					}
					attributes.get(unitName).add(attributeName);
				}
			}
		}

		this.structuralUnits = new ArrayList<>();
		for (String strucuralUnit : attributes.keySet())
			this.structuralUnits.add(new StructuralUnit(strucuralUnit,
					attributes.get(strucuralUnit), this));

		return this.structuralUnits;
	}


	public boolean isModified() {
		return modified;
	}

	@Override
	public boolean loadParameters() throws Exception {
		return true;
	}

	@Override
	public boolean saveParameters() throws Exception {
		return true;
	}

	@Override
	protected void setID(String id) {
		this.pID = id;
	}

	@Override
	public void setIsModified(boolean b) {
		modified = b;
	}

	@Override
	public void setName(String name) {
		this.setUserName(name);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.pID;
	}

	@Override
	public boolean _toTxt(File arg0, String arg1, String arg2, String arg3) throws Exception {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public CorpusBuild getRootCorpusBuild() {
		return this;
	}

	/**
	 * Return the CQP START positions of the main corpus texts
	 */
	@Override
	public int[] getTextStartLimits() throws CqiClientException, IOException, CqiServerError, InvalidCqpIdException {
		if (textLimits == null) {
			textLimits = getStartLimits("<text>[]"); //$NON-NLS-1$
		}
		return textLimits;
	}

	/**
	 * Return the CQP END positions of the main corpus texts
	 */
	@Override
	public int[] getTextEndLimits() throws CqiClientException, IOException, CqiServerError, InvalidCqpIdException {
		if (textEndLimits == null) {
			String queryResultId = queryResultNamePrefix + UUID.randomUUID().toString();
			CorpusManager.getCorpusManager().getCqiClient().cqpQuery(
					this.getQualifiedCqpId(), queryResultId, "<text>[] expand to text"); //$NON-NLS-1$
			QueryResult queryResult = new QueryResult(queryResultId, queryResultId, this, new CQLQuery("<text>[] expand to text")); //$NON-NLS-1$
			textEndLimits = queryResult.getEnds();
		}
		return textEndLimits;
	}

	/**
	 * Gets the nb texts.
	 *
	 * @return the nb texts
	 * @throws CqiClientException the cqi client exception
	 * @throws CqiServerError
	 * @throws IOException
	 */
	@Override
	public int getNbTexts() throws CqiClientException, IOException, CqiServerError {
		if (nbtext == -1) {
			StructuralUnit text_su = this.getStructuralUnit("text"); //$NON-NLS-1$
			StructuralUnitProperty text_id_sup = text_su.getProperty("id"); //$NON-NLS-1$
			nbtext = CorpusManager.getCorpusManager().getCqiClient().attributeSize(text_id_sup.getQualifiedName());
		}
		return nbtext;
	}

	/**
	 * 
	 * @return the text_id values of the CQP corpus ordered by position
	 * @throws CqiClientException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	@Override
	public String[] getCorpusTextIdsList() throws CqiClientException, IOException, CqiServerError {
		if (cqpTextIDS == null) {

			int nbtext = getNbTexts();
			int[] structs = new int[nbtext];
			for (int i = 0; i < nbtext; i++) {
				structs[i] = i;
			}
			StructuralUnit text_su = this.getStructuralUnit("text"); //$NON-NLS-1$
			StructuralUnitProperty text_id_sup = text_su.getProperty("id"); //$NON-NLS-1$

			cqpTextIDS = CorpusManager.getCorpusManager().getCqiClient().struc2Str(text_id_sup.getQualifiedName(), structs);
		}
		return cqpTextIDS;
	}

	/**
	 * Gets the texts ids and order number in corpus.
	 *
	 * @return the texts ids
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError the cqi server error
	 * @throws CqiClientException the cqi client exception
	 */
	@Override
	public HashMap<String, Integer> getTextsIds() throws IOException, CqiServerError, CqiClientException {
		if (textids == null) {
			textids = new HashMap<>();
			int nbtext = getNbTexts();
			int[] structs = new int[nbtext];
			for (int i = 0; i < nbtext; i++)
				structs[i] = i;

			StructuralUnit text_su = this.getStructuralUnit("text"); //$NON-NLS-1$
			StructuralUnitProperty text_id_sup = text_su.getProperty("id"); //$NON-NLS-1$

			String[] ids = CorpusManager.getCorpusManager().getCqiClient().struc2Str(text_id_sup.getQualifiedName(), structs);
			for (int i = 0; i < ids.length; i++) {
				textids.put(ids[i], structs[i]);
			}
		}
		return textids;
	}

	public File getRegistryFile() {
		return registryFile;
	}

	public File getDataDirectory() {
		return dataDirectory;
	}

	@Override
	public List<String> isBuildValid() {

		LinkedList<String> errors = new LinkedList<>();

		if (!dataDirectory.exists()) {
			errors.add("no data directory: " + dataDirectory);
		}

		//File registryFile = new File(this.registryFile);
		if (!registryFile.exists()) {
			errors.add("no registry file: " + registryFile);
		}

		errors.addAll(new ReadRegistryFile(registryFile).isCorpusBuildValid(dataDirectory));

		return errors;
	}

	@Override
	public String isReady() {

		if (!hasBeenComputedOnce()) return "The Maincorpus is not a computed TXMResult.";

		try {
			int s = this.getSize();
			if (s >= 0) return null;
			return "negative size";
		}
		catch (CqiClientException e) {
			return e.getMessage();
		}
	}
}
