package org.txm.searchengine.cqp;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * CQP preferences initializer.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class CQPPreferences extends TXMPreferences {


	/**
	 * List of part names.
	 */
	public static final String PART_NAMES = "part_names"; //$NON-NLS-1$


	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(CQPPreferences.class)) {
			new CQPPreferences();
		}
		return TXMPreferences.instances.get(CQPPreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();
	}
}
