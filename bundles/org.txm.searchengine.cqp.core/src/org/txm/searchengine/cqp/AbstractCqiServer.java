// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2011-12-14 10:52:18 +0100 (mer., 14 déc. 2011) $
// $LastChangedRevision: 2080 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp;

import java.io.InputStream;
import java.io.OutputStream;

// TODO: Auto-generated Javadoc
/**
 * This class aims at handling a CQi server process. It is not a CQi server per
 * se.
 * 
 * @author Jean-Philippe Magué
 */
public abstract class AbstractCqiServer implements ICqiServer {

	@Override
	public abstract void setDebug(boolean val);

	/**
	 * Gets the last cmd line.
	 *
	 * @return the last cmd line
	 */
	public String getLastCmdLine() {
		return ""; //$NON-NLS-1$
	}

	/**
	 * Start.
	 *
	 * @return true, if successful
	 * @throws Exception
	 */
	@Override
	public abstract Boolean start() throws Exception;

	/**
	 * Convert stream to string.
	 *
	 * @param is the is
	 * @return the string
	 */
	@Override
	public abstract String convertStreamToString(InputStream is);

	/**
	 * Stop the server.
	 */
	@Override
	public abstract Boolean stop();

	/**
	 * Checks if the server is running.
	 * 
	 * @return true, if is running
	 */
	@Override
	public abstract Boolean isRunning();

	/**
	 * Gets the error stream.
	 *
	 * @return the error stream
	 */
	@Override
	public abstract InputStream getErrorStream();

	/**
	 * Gets the input stream.
	 *
	 * @return the input stream
	 */
	@Override
	public abstract InputStream getInputStream();

	/**
	 * Gets the output stream.
	 *
	 * @return the output stream
	 */
	@Override
	public abstract OutputStream getOutputStream();
}
