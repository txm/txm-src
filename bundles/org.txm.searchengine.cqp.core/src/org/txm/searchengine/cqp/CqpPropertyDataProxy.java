// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.cqp;

import java.util.List;

import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;

// TODO: Auto-generated Javadoc
/**
 * The Class CqpPropertyDataProxy.
 */
public class CqpPropertyDataProxy {

	/**
	 * Data is cached in a sparse list which give for each position the
	 * corresponding string (given the property it is dedicated to).
	 */
	private List<String> data = null;

	/** The property. */
	private StructuralUnitProperty property;

	/** the corpus. */
	private CQPCorpus corpus;

	/**
	 * The Constructor.
	 *
	 * @param corpus the corpus
	 * @param property The property the dataProxy is dedicated to
	 */
	public CqpPropertyDataProxy(CQPCorpus corpus, StructuralUnitProperty property) {
		this.property = property;
		this.corpus = corpus;
	}


}
