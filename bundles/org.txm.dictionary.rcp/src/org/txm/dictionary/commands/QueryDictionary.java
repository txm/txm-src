package org.txm.dictionary.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.kohsuke.args4j.Option;
import org.txm.dictionary.functions.sql.DictionaryManager;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;

public class QueryDictionary extends AbstractHandler {

	@Option(name="query", usage="SQL query, can contains $NAME which will be replace with the dictionary SQL name", widget="String", required=true, def="SELECT * FROM $NAME")
	public String query;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			if (ParametersDialog.open(this)) {
					return query(query);
			}

			return -1;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int query(String query) throws Exception {
		int n = DictionaryManager.getInstance().queryAndPrint(query);
		System.out.println("Result size="+n);
		return n;
	}
}
