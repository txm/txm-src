package org.txm.dictionary.commands;

import java.io.File;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.kohsuke.args4j.Option;
import org.txm.dictionary.functions.sql.Dictionary;
import org.txm.dictionary.functions.sql.DictionaryManager;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.utils.JobHandler;

public class RecodeColumnDictionary extends AbstractHandler {
	
	@Option(name="name", usage="dictionary name", widget="String", required=true, def="conv.tsv")
	public String name = null;
	@Option(name="conversionFile", usage="conversionFile", widget="File", required=true, def="conv.tsv")
	public File conversionFile = null;
	@Option(name="type", usage="type", widget="String", required=true, def="type")
	public String type = null;
	@Option(name="newType", usage="newType", widget="String", required=true, def="newType")
	public String newType = null;
	@Option(name="mode", usage="'supprimer' to delete entries without rule, 'abandon' to stop if errors and 'copier' to copy value if no rule", widget="String", required=true, def="abandon")
	public String mode = "abandon";
	@Option(name="oneMatch", usage="stop at first match", widget="String", required=true, def="true")
	public boolean oneMatch = true;
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		if (ParametersDialog.open(this)) {
			try {
				if (DictionaryManager.getInstance().hasDictionary(name)) {
					JobHandler job = new JobHandler("UniqSort "+name) {
						
						@Override
						protected IStatus _run(SubMonitor monitor) throws Exception {
							
							Dictionary d = DictionaryManager.getInstance().getDictionary(name);
							recode(d, conversionFile, type, newType, mode, oneMatch);
							return Status.OK_STATUS;
						}
					};
					job.schedule();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static Dictionary recode(Dictionary d, File conversionTSVFile, String type, String newType, String mode, boolean oneMatch) throws Exception {
		
		LinkedHashMap<Pattern, Serializable[]> conversionRules = Dictionary.getConversionRulesFromFile(conversionTSVFile);
		if (conversionRules.size() > 0) {
			d.recodeEntryProperties(type, newType, conversionRules, mode, oneMatch);
		}
		return null;
	}
}
