package org.txm.dictionary.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.kohsuke.args4j.Option;
import org.txm.dictionary.functions.sql.Dictionary;
import org.txm.dictionary.functions.sql.DictionaryManager;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.utils.JobHandler;

public class ExportDictionary extends AbstractHandler {
	
	@Option(name="name", usage="the dictionary name", widget="String", required=true, def="lex")
	public String name = null;
	@Option(name="tsvFile", usage="tsvFile", widget="FileSave", required=true, def="dict.tsv")
	public File tsvFile = null;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		if (ParametersDialog.open(this)) {
			
			try {
				if (!DictionaryManager.getInstance().hasDictionary(name)) {
					System.out.println("Error: no dictionary named "+name);
					return null;
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return null;
			}
			
			JobHandler job = new JobHandler("UniqSort "+name) {
				
				@Override
				protected IStatus _run(SubMonitor monitor) throws Exception {
					
					Dictionary d = DictionaryManager.getInstance().getDictionary(name);
					exportToTSVFile(d, tsvFile);
					return Status.OK_STATUS;
				}
			};
			job.schedule();
		}
		
		return null;
	}
	
	public static Dictionary exportToTSVFile(Dictionary d, File tsvFile) throws Exception {
		
		if (d.print(tsvFile)) {
			return d;
		}
		return null;
	}
}
