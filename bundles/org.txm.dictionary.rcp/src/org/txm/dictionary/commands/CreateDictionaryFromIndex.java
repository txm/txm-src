package org.txm.dictionary.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.kohsuke.args4j.Option;
import org.txm.dictionary.functions.sql.Dictionary;
import org.txm.index.core.functions.Index;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.utils.JobHandler;

public class CreateDictionaryFromIndex  extends AbstractHandler {

	@Option(name="name", usage="dictionary name", widget="String", required=true, def="conv.tsv")
	public String name = null;
	@Option(name="tsvFile", usage="tsvFile", widget="File", required=true, def="conv.tsv")
	public File tsvFile = null;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);

		Object o = selection.getFirstElement();
		if (!(o instanceof Index)) return null;

		final Index index = (Index)o;

		if (ParametersDialog.open(this)) {

			JobHandler job = new JobHandler("UniqSort "+name) {

				@Override
				protected IStatus _run(SubMonitor monitor) throws Exception {
					fromIndex(index, tsvFile, name);
					return Status.OK_STATUS;
				}
			};
			job.schedule();
		}

		return null;
	}

	public static Dictionary fromIndex(Index index, File tsvFile, String name) throws Exception {

		index.toTSVDictionnary(tsvFile, "\t", "UTF-8");

		return ImportDictionary.importFromTSVFile(tsvFile, name);
	}
}
