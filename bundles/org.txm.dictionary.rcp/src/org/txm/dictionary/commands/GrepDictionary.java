package org.txm.dictionary.commands;

import java.util.HashMap;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.kohsuke.args4j.Option;
import org.txm.dictionary.functions.sql.Dictionary;
import org.txm.dictionary.functions.sql.DictionaryManager;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;

public class GrepDictionary extends AbstractHandler {

	@Option(name="name", usage="dictionary name", widget="String", required=true, def="lex")
	public String name = null;
	@Option(name="col", usage="column", widget="String", required=true, def="column")
	public String col;
	@Option(name="pattern", usage="pattern", widget="String", required=true, def=".+")
	public String pattern;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			if (ParametersDialog.open(this)) {
				if (DictionaryManager.getInstance().hasDictionary(name)) {
					Dictionary d = DictionaryManager.getInstance().getDictionary(name);
					return grep(d, col, pattern);
				} else {
					System.out.println("No dictionary named '"+name+"'");
					return -1;
				}
			}

			return -1;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int grep(Dictionary d, String col, String pattern) throws Exception {
		HashMap<String, String> hash = new HashMap<String, String>();
		hash.put(col, pattern);
		return grep(d, hash);
	}
	
	public static int grep(Dictionary d, HashMap<String, String> hash) throws Exception {
		return d.grep(hash);
	}
}
