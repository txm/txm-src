package org.txm.dictionary.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.kohsuke.args4j.Option;
import org.txm.dictionary.functions.sql.Dictionary;
import org.txm.dictionary.functions.sql.DictionaryManager;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;

public class RenameColumnDictionary extends AbstractHandler {

	@Option(name="name", usage="dictionary name", widget="String", required=true, def="conv.tsv")
	public String name = null;
	@Option(name="oldtype", usage="oldtype", widget="String", required=true, def="old")
	public String oldtype = null;
	@Option(name="newtype", usage="the new name", widget="String", required=true, def="new")
	public String newtype = null;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			if (ParametersDialog.open(this)) {
				if (DictionaryManager.getInstance().hasDictionary(name)) {
					Dictionary d = DictionaryManager.getInstance().getDictionary(name);
					rename(d, oldtype, newtype);
				}
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Dictionary rename(Dictionary d, String oldtype, String newtype) throws Exception {
		if (d.getTypes().contains(newtype)) {
			System.out.println("Column already exists: "+d.getTypes());
			return d;
		}
		if (d.getTypes().contains(oldtype)) {
			System.out.println("The old column does not exists: "+d.getTypes());
			return d;
		}
		d.renameType(oldtype, newtype);
		System.out.println("Type renamed="+d.getTypes());
		return null;
	}
}
