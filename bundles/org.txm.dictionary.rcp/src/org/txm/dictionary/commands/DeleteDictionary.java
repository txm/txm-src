package org.txm.dictionary.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.kohsuke.args4j.Option;
import org.txm.dictionary.functions.sql.DictionaryManager;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;

public class DeleteDictionary extends AbstractHandler {

	@Option(name="name", usage="dictionary name 1", widget="String", required=true, def="bfmlex")
	public String name = null;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			if (ParametersDialog.open(this)) {
				remove(name);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static void remove(String name) throws Exception {
		DictionaryManager.getInstance().remove(name);
	}
}