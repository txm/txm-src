package org.txm.dictionary.commands;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.kohsuke.args4j.Option;
import org.txm.dictionary.functions.sql.Dictionary;
import org.txm.dictionary.functions.sql.DictionaryManager;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;

public class MergeDictionaries extends AbstractHandler {

	@Option(name="name", usage="dictionary name 1", widget="String", required=true, def="bfmlex")
	public String name = null;
	@Option(name="name2", usage="dictionary name 2", widget="String", required=true, def="bfmgoldlex")
	public String name2 = null;
	@Option(name="cols", usage="columns", widget="String", required=true, def="word,pos")
	public String cols = null;
	@Option(name="aggregatescols", usage="the other columns", widget="String", required=true, def="F")
	public String aggregatescols = null;
	@Option(name="aggregates", usage="the agregates instruction for each other column.", widget="String", required=true, def="MAX")
	public String aggregates = null;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			if (ParametersDialog.open(this)) {
				if (DictionaryManager.getInstance().hasDictionary(name) && DictionaryManager.getInstance().hasDictionary(name2)) {
					Dictionary d = DictionaryManager.getInstance().getDictionary(name);
					Dictionary d2 = DictionaryManager.getInstance().getDictionary(name2);
					List<String> colsList = Arrays.asList(cols.split(","));
					List<String> aggregatescolsList = Arrays.asList(aggregatescols.split(","));
					List<String> aggregateList = Arrays.asList(aggregates.split(","));
					return merge(d, d2, colsList, aggregatescolsList, aggregateList);
				} else {
					System.out.println("Error: a dictionary is missing: "+name+" or "+name2);
				}
			}

			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Dictionary merge(Dictionary d, Dictionary d2, List<String> cols, List<String> aggregatescols, List<String> aggregates) throws Exception {
		
		if (aggregatescols.size() != aggregates.size()) {
			System.out.println("other columns and aggregates size differs: others="+aggregatescols.size()+" aggregates="+aggregates.size());
			return null;
		}
		
		LinkedHashMap<String, String> agregateInstructions = new LinkedHashMap<String, String>();
		for (int i = 0 ; i < aggregatescols.size() ; i++) {
			agregateInstructions.put(aggregatescols.get(i), aggregates.get(i));
		}
		int n = d2.mergeWith(d, cols, agregateInstructions);
		
		if (n == 0) System.out.println("No entry added");
		else if (n == 1) System.out.println("One entry added.");
		else  System.out.println(""+n+" entries added");
		
		return d2;
	}
}
