package org.txm.dictionary.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.kohsuke.args4j.Option;
import org.txm.dictionary.functions.sql.Dictionary;
import org.txm.dictionary.functions.sql.DictionaryManager;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.utils.JobHandler;

public class ImportDictionary extends AbstractHandler {
	
	@Option(name="name", usage="dictionary name", widget="String", required=true, def="name")
	public String name = null;
	@Option(name="tsvfile", usage="tsvfile", widget="File", required=true, def="lexique.tsv")
	public File tsvfile = null;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			if (ParametersDialog.open(this)) {
				JobHandler job = new JobHandler("UniqSort "+name) {
					
					@Override
					protected IStatus _run(SubMonitor monitor) throws Exception {
						
						importFromTSVFile(tsvfile, name);
						return Status.OK_STATUS;
					}
				};
				job.schedule();
			}
			
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Dictionary importFromTSVFile(File tsvFile, String name) throws Exception {
		
		DictionaryManager dm = DictionaryManager.getInstance();
		Dictionary d = dm.getDictionary(name);
		if (d == null) {
			System.out.println("Error: could not create or get dictionary with name="+name);
		}
		if (d.loadFromTSVFile(tsvFile)) {
			return d;
		}
		return null;
	}
}
