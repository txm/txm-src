package org.txm.dictionary.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.dictionary.functions.sql.Dictionary;
import org.txm.dictionary.functions.sql.DictionaryManager;

public class ListDictionaries extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			list();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static Dictionary list() throws Exception {
		System.out.println("Tables: "+DictionaryManager.getInstance().getHSQLFunctions().getTables());
		System.out.println("Dictionaries: "+DictionaryManager.getInstance().getDictionaries());
		return null;
	}
}