package org.txm.dictionary.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.kohsuke.args4j.Option;
import org.txm.dictionary.functions.sql.Dictionary;
import org.txm.dictionary.functions.sql.DictionaryManager;
import org.txm.dictionary.functions.sql.HSQLFunctions;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;

public class InterDictionaries extends AbstractHandler {

	@Option(name="first", usage="first dictionary name", widget="String", required=true, def="afrlex")
	public String first = null;
	@Option(name="second", usage="second dictionary name", widget="String", required=true, def="frolex")
	public String second = null;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			if (ParametersDialog.open(this)) {
				if (DictionaryManager.getInstance().hasDictionary(first)) {
					System.out.println("No dictionary named: "+first);
					return null;
				}
				if (DictionaryManager.getInstance().hasDictionary(second)) {
					System.out.println("No dictionary named: "+second);
					return null;
				}
						
				if (DictionaryManager.getInstance().hasDictionary(first)) {
					Dictionary firstDict = DictionaryManager.getInstance().getDictionary(first);
					Dictionary secondDict = DictionaryManager.getInstance().getDictionary(second);
					return inter(firstDict, secondDict);
				}
			}

			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Object inter(Dictionary firstDict, Dictionary secondDict) throws Exception {
		HSQLFunctions functions = DictionaryManager.getInstance().getHSQLFunctions();
		functions.printQuery("SELECT * from \""+firstDict.getName()+"\" WHERE form NOT IN (SELECT \"form\" FROM \""+secondDict+"\")");
		return null;
	}
}
