package org.txm.dictionary.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.kohsuke.args4j.Option;
import org.txm.dictionary.functions.sql.Dictionary;
import org.txm.dictionary.functions.sql.DictionaryManager;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.utils.JobHandler;

public class UniqSortDictionary extends AbstractHandler {
	
	@Option(name="name", usage="the dictionary name", widget="String", required=true, def="lex")
	public String name = null;
	@Option(name="tsvFile", usage="conversionFile", widget="FileSave", required=true, def="conv.tsv")
	public File tsvFile = null;
	@Option(name="col", usage="the clumn to uniqsort", widget="String", required=true, def="form")
	public String col = null;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			if (ParametersDialog.open(this)) {
				if (DictionaryManager.getInstance().hasDictionary(name)) {
					
					
					JobHandler job = new JobHandler("UniqSort "+name) {
						
						@Override
						protected IStatus _run(SubMonitor monitor) throws Exception {
							
							if (!DictionaryManager.getInstance().hasDictionary(name)) {
								System.out.println("Error: the dictionary does exists: "+name);
								return Status.CANCEL_STATUS;
							}
							
							Dictionary d = DictionaryManager.getInstance().getDictionary(name);
							if (!d.getTypes().contains(col)) {
								System.out.println("Error: the dictionary does nto contains the given column: "+col);
								return Status.CANCEL_STATUS;
							}
							
							if (uniqsort(d, tsvFile, col) == null) {
								System.out.println("Error: the dictionary uniqsort failed.");
								return Status.CANCEL_STATUS;
							}
							
							return Status.OK_STATUS;
							
						}
					};
					job.schedule();
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Dictionary uniqsort(Dictionary d, File tsvFile, String col) throws Exception {
		if (d.uniqsort(tsvFile, col)) {
			return d;
		}
		return null;
	}
}
