package org.txm.dictionary.commands;


import java.io.File;

import org.osgi.framework.Version;
import org.txm.PostInstallationStep;
import org.txm.Toolbox;
import org.txm.dictionary.preferences.DictionaryPreferences;
import org.txm.utils.BundleUtils;
import org.txm.utils.logger.Log;

/**
 * Copy macros from bundle to TXM macro directory
 * 
 * @author mdecorde
 *
 */
public class DoInstallStep  extends PostInstallationStep {

	public static String ID = "org.txm.dictionary.commands.DoInstallStep"; //$NON-NLS-1$
	public static final String VERSION = "org.txm.dictionary.version";

	public void install() {
		
		Log.fine("Dictionary.DoInstallStep.install()");
		
		String saved = DictionaryPreferences.getInstance().getString(DictionaryPreferences.VERSION);
		Version currentVersion = BundleUtils.getBundleVersion("Dictionary");
		
		if (saved != null && saved.length() > 0) {
			Version savedVersion = new Version(saved);
			if (currentVersion.compareTo(savedVersion) <= 0) {
				Log.fine("No post-installation of Dictionary to do.");
				//System.out.println("No post-installation of Dictionary to do.");
				return; // nothing to do
			}
		}
		System.out.println("Post-installing Dictionary version="+currentVersion);
		
		File macroDirectory = new File(Toolbox.getTxmHomePath(), "scripts/macro/org/txm/macro");
		
		File previousURSMacroDirectory = new File(macroDirectory, "frolex");
		if (previousURSMacroDirectory.exists() && saved != null && saved.length() > 0) {
			File back = new File(previousURSMacroDirectory.getParentFile(), previousURSMacroDirectory.getName()+"_"+saved); //$NON-NLS-1$
			if (back.exists()) {
				return;
			}
			previousURSMacroDirectory.renameTo(back);
		}
		
		if (!BundleUtils.copyFiles("Dictionary", "src", "org/txm/macro/", "frolex", macroDirectory, true)) {
			Log.severe("Error while coping Dictionary org/txm/macro/frolex in "+macroDirectory.getAbsolutePath());
			return;
		}
		
		Log.fine("Dictionary post-installation done.");
		DictionaryPreferences.getInstance().put(DictionaryPreferences.VERSION, currentVersion.toString());
		return;
	}
	
	public DoInstallStep() {
		name = "Dictionary";
	}
}
