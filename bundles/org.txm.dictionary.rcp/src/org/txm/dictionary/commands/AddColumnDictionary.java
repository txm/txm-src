package org.txm.dictionary.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.kohsuke.args4j.Option;
import org.txm.dictionary.functions.sql.Dictionary;
import org.txm.dictionary.functions.sql.DictionaryManager;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;

public class AddColumnDictionary extends AbstractHandler {

	@Option(name="name", usage="dictionary name", widget="String", required=true, def="conv.tsv")
	public String name = null;
	@Option(name="type", usage="type", widget="String", required=true, def="type")
	public String type = null;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			if (ParametersDialog.open(this)) {
				if (DictionaryManager.getInstance().hasDictionary(name)) {
					Dictionary d = DictionaryManager.getInstance().getDictionary(name);
					add(d, type);
				}
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Dictionary add(Dictionary d, String type) throws Exception {
		if (d.getTypes().contains(type)) {
			System.out.println("Column already exists.");
			return d;
		}
		d.addType(type);
		System.out.println("Type="+type+" added to "+d.getName());
		return null;
	}
}
