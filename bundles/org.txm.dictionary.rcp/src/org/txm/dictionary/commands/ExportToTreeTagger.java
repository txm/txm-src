package org.txm.dictionary.commands;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.kohsuke.args4j.Option;
import org.txm.dictionary.functions.sql.Dictionary;
import org.txm.dictionary.functions.sql.DictionaryManager;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

public class ExportToTreeTagger extends AbstractHandler {
	
	@Option(name="name", usage="dictionary name", widget="String", required=true, def="frolex")
	public String name = null;
	@Option(name="pos", usage="pos property", widget="String", required=true, def="msd_cattex_conv")
	public String pos = null;
	@Option(name="lemma", usage="lemma property", widget="String", required=true, def="lemma")
	public String lemma = null;
	@Option(name="ignoredPosValues", usage="types", widget="String", required=true, def="<nopos>,,")
	public String ignoredPosValues = null;
	@Option(name="tsvfile", usage="tsvfile", widget="File", required=true, def="lexicon.txt")
	public File tsvfile = null;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			if (ParametersDialog.open(this)) {
				JobHandler job = new JobHandler("TreeTagger lexicon export "+name) {
					
					@Override
					protected IStatus _run(SubMonitor monitor) throws Exception {
						
						if (DictionaryManager.getInstance().hasDictionary(name)) {
							Dictionary d = DictionaryManager.getInstance().getDictionary(name);
							HashSet<String> ignoredPosValuesSet = new HashSet<String>(split(ignoredPosValues));
							
							export(d, pos, lemma, ignoredPosValuesSet, tsvfile);
						}
						return Status.OK_STATUS;
					}
				};
				job.schedule();
			}
			return null;
		} catch (Exception e) {
			System.out.println("Error: "+e);
			Log.printStackTrace(e);
		}
		return null;
	}
	
	public static ArrayList<String> split(String s) {
		ArrayList<String> result = new ArrayList<String>();
		int idx = s.indexOf(",");
		if (idx < 0) return result;
		
		while (idx >= 0) {
			String s1 = s.substring(0, idx);
			result.add(s1);
			s = s.substring(idx+1);
			idx = s.indexOf(",");
		}
		result.add(s);
		return result;
	}
	
	private static StringBuilder b = new StringBuilder();
	public static String join(Collection<String> values, String sep) {
		b.setLength(0);
		for (String s: values) b.append(s+"\t");
		
		return b.substring(0, b.length()-1);
	}
	
	public static boolean export(Dictionary d, String pos, String lemma, HashSet<String> ignoredPosValues, File tsvfile) throws UnsupportedEncodingException, FileNotFoundException, SQLException {
		
		System.out.println("Merging lines of "+d+" in "+tsvfile);
		System.out.println(" pos property="+pos);
		System.out.println(" lemma property="+lemma);
		System.out.println(" ignoredvalues="+ignoredPosValues);
		
		List<String> columns = d.getTypes();
		if (!columns.contains("form")) {
			System.out.println("Type not found in dictionary: "+"form");
			return false;
		}
		
		if (!columns.contains(pos)) {
			System.out.println(pos+" type not found in dictionary: "+columns);
			return false;
		}
		if (!columns.contains(lemma)) {
			System.out.println(lemma+" type not found in dictionary: "+columns);
			return false;
		}
		String TAB = "\t";
		
		PrintWriter writer = IOUtils.getWriter(tsvfile, "UTF-8");
		ResultSet result = d.getSortedLines("form");
		String currentForm = null;
		HashMap<String, HashSet<String>> posFound = new HashMap<String, HashSet<String>>();
		
		while (result.next()) { // for each form entry
			String form = result.getString("form");
			String ignoreValue = result.getString(pos);
			if (ignoredPosValues.size() > 0 && ignoredPosValues.contains(ignoreValue)) { continue; } // ignore "<nopos>" pos values entries
			
			if (!form.equals(currentForm)) { // append to the current merged line
				if (currentForm != null && posFound.size() > 0) { // write the line if the word has pos
					
					writer.print(currentForm);
					
					for (String posValue : posFound.keySet()) { // for each pos, sort the lemma
						
						ArrayList<String> lemmaValues = new ArrayList<String>();
						lemmaValues.addAll(posFound.get(posValue));
						Collections.sort(lemmaValues, new Comparator<String>(){
							@Override
							public int compare(String o1, String o2) {
								if (o2.contains("no_lemma")) return -1;
								else if (o2.contains("nolem")) {return -1;}
								else if (o1.contains("no_lemma")) return 1;
								else if (o1.contains("nolem")) {return 1;}
								return 0;
							}
						});
						if (lemmaValues.size() > 1) lemmaValues.remove("<no_lemma>"); // remove <no_lemma> if there are others lemma values
						writer.print("\t"+posValue+" "+StringUtils.join(lemmaValues, "|"));
					}
					writer.println(""); // end of line
					
				} else {
					System.out.println("Form '"+currentForm+"' has no pos-lemme couple.");
				}
				
				
				posFound.clear();
				currentForm = form;
			}
			String firstValue = result.getString(pos);
			if (!posFound.containsKey(firstValue)) posFound.put(firstValue, new HashSet<String>()); // avoid repeating the first type value 
			
			String value = result.getString(lemma).trim();
			if (value == null) {
				System.out.println("Error: null value for type="+lemma);
				writer.close();
				return false;
			} 
			
			if (value.length() == 0) {
				value = "<no_lemma>"; 
			}
			posFound.get(firstValue).add(value); // add the new lemma for this pos value
		}
		
		
		writer.close();
		result.getStatement().close(); // :-)
		System.out.println("Result saved in "+tsvfile.getAbsolutePath());
		return true;
	}
	
	public static void main(String[] args) throws Exception {
		File frolexTSV = new File("/home/mdecorde/TXM/results/lexique BFM/result/frolex.tsv");
		File frolexTT = new File("/home/mdecorde/TXM/results/lexique BFM/result/frolex-tt.tsv");
		DictionaryManager.getInstance().remove("bfmlex");
		Dictionary frolex = DictionaryManager.getInstance().getDictionary("frolex");
		frolex.loadFromTSVFile(frolexTSV);
		
		ArrayList<String> types = new ArrayList<String>();
		types.add("msd_cattex_conv");
		types.add("lemma");
		String ignoredTestType = "msd_cattex_conv";
		HashSet<String> ignoredvalues = new HashSet<String>();
		ignoredvalues.add("<nopos>");
		ignoredvalues.add("");
		
		export(frolex,"msd_cattex_conv", "lemma", ignoredvalues, frolexTT);
	}
}
