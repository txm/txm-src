package org.txm.dictionary.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.kohsuke.args4j.Option;
import org.txm.dictionary.functions.sql.Dictionary;
import org.txm.dictionary.functions.sql.DictionaryManager;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;

public class CopyDictionaries extends AbstractHandler {

	@Option(name="orig", usage="orig dictionary name", widget="String", required=true, def="orig")
	public String orig = null;
	@Option(name="copy", usage="copy dictionary name", widget="String", required=true, def="copy")
	public String copy = null;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			if (ParametersDialog.open(this)) {
				if (DictionaryManager.getInstance().hasDictionary(orig)) {
					Dictionary d = DictionaryManager.getInstance().getDictionary(orig);
					return copy(d, copy);
				}
			}

			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Dictionary copy(Dictionary d, String d2) throws Exception {
		int n = d.makeCopy(d2);
		
		if (n == 0) System.out.println("No entry copied");
		else if (n == 1) System.out.println("One entry copied.");
		else  System.out.println(""+n+" entries copied");
		
		return DictionaryManager.getInstance().getDictionary(d2);
	}
}
