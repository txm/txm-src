package org.txm.dictionary.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.kohsuke.args4j.Option;
import org.txm.dictionary.functions.sql.Dictionary;
import org.txm.dictionary.functions.sql.DictionaryManager;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;

public class PrintDictionary extends AbstractHandler {

	@Option(name="name", usage="dictionary name", widget="String", required=true, def="conv.tsv")
	public String name = null;
	@Option(name="n", usage="number of line to show", widget="Integer", required=true, def="5")
	public int n;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			if (ParametersDialog.open(this)) {
				if (DictionaryManager.getInstance().hasDictionary(name)) {
					Dictionary d = DictionaryManager.getInstance().getDictionary(name);
					return print(d, n);
				} else {
					System.out.println("No dictionary named="+name);
				}
			}

			return null;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Dictionary print(Dictionary d, int n) throws Exception {
		d.print(n);
		return d;
	}
}
