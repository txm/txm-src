package org.txm.dictionary.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.kohsuke.args4j.Option;
import org.txm.dictionary.functions.sql.Dictionary;
import org.txm.dictionary.functions.sql.DictionaryManager;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;

public class RemoveEntriesDictionary extends AbstractHandler {

	@Option(name="name", usage="dictionary name", widget="String", required=true, def="conv.tsv")
	public String name = null;
	@Option(name="type", usage="type", widget="String", required=true, def="type")
	public String type = null;
	@Option(name="pattern", usage="newType", widget="String", required=true, def="reg exp pattern")
	public String pattern = null;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			if (ParametersDialog.open(this)) {
				if (DictionaryManager.getInstance().hasDictionary(name)) {
					Dictionary d = DictionaryManager.getInstance().getDictionary(name);
					
					//Pattern p = Pattern.compile(pattern);
					removeEntries(d, type, pattern);
				} else {
					System.out.println("No dictionary named '"+name+"'.");
				}
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Dictionary removeEntries(Dictionary d, String type, String pattern) throws Exception {
		int n = d.removeEntries(type, pattern);
		if (n == 0) System.out.println("No entry removed");
		else if (n == 1) System.out.println("One entry removed.");
		else  System.out.println(""+n+" entries removed");
		return null;
	}
}
