package org.txm.dictionary.preferences;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

public class DictionaryPreferences extends TXMPreferences {

//	public static final String PREFERENCES_NODE = FrameworkUtil.getBundle(DictionaryPreferences.class).getSymbolicName();
	
	public static final String VERSION = "dictionary.version"; //$NON-NLS-1$
	
	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.put(VERSION, "");
	}
	
	/**
	 * Gets the instance.
	 * @return the instance
	 */
	public static TXMPreferences getInstance()	{
		if (!TXMPreferences.instances.containsKey(DictionaryPreferences.class)) {
			new DictionaryPreferences();
		}
		return TXMPreferences.instances.get(DictionaryPreferences.class);
	}
}
