#Reformatage du lexique de l'ancien français afrlex au format :
#une association "forme	msd	lemme	ref" par ligne

use strict;
#use warnings;

my $file = "afrlex";

open (IN,$file) || die "Cannot read from $file\n";
open (OUT, ">$file.$$") || die "Cannot write to $file.$$\n";

print OUT "form\tmsd\tlemma\tlemma-source\n";

my @lines = <IN>; close IN;

for my $line (@lines) {
#	print $line;
	if ($line =~ m/^([^\t]*)\t(.*)$/) {
		#la forme est le premier segment avant tabulation
		my $form = $1;
		#le reste sont des annotations : msd + lemmes
		my $annotations = $2;
		until ($annotations =~ m/^\s*$/) {
			#on traite les annotations en boucle par paire de valeurs séparées par une tabulation
			$annotations =~ m/^\t?([^\t]*)\t([^\t]*)(.*)$/;
			#la 1ère valeur est l'étiquette morphosyntaxique
			my $msd = $1;
			#la seconde est un ensemble lemme_source éventuellement multiples avec séparation par type
			my $lemmesrefs = $2;
			$annotations = $3;
			#s'il y a un "_", on parse
			if ($lemmesrefs =~ m/^([^_]*)_([^_]*)$/) {
				my @lemmes = split/\|/, $1;
				my @refs = split/\|/, $2;
				my $counter = 0;
				foreach my $lemme (@lemmes) {
					print OUT "$form\t$msd\t$lemme\t$refs[$counter]\n";
					$counter++;
				}
			}
			#sinon on copie simplement
			else {
				my @lemmes = split/\|/, $lemmesrefs;
				foreach my $lemme (@lemmes) {
					print OUT "$form\t$msd\t$lemme\t\n";
				}
			}
		}
	}
	else {
		print "ERROR in line format: $line\n"
	}
}

close OUT;
rename "$file.$$","$file-tab" || die "Cannot write to $file-tab\n";

print "Press <Enter> to finish";
<STDIN>
