package org.txm.dictionary.functions.tsv

import java.util.regex.Pattern;

import org.txm.utils.ConsoleProgressBar

def DEBUG = false

File workingDirectory = new File("/home/mdecorde/TXM/results/lexique BFM")
File frolextsv = new File(workingDirectory, "BFM2013-dict.tsv")
/**
form	msd_cattex_bfm	F_bfm
,	NA	170673
et	NA	156135
de	NA	131501
 */
if (!frolextsv.exists()) {
	println "Can't find $frolextsv"
	return
}

File convCTX9NCA = new File(workingDirectory, "conv ctx9 ctx9-nca.tsv")
if (!convCTX9NCA.exists()) {
	println "Can't find $convCTX9NCA"
	return
}

Pattern pattern = Pattern.compile("\t");

String[] noposarray = ["_nopos_"]
def conversion_rules = ["NA":noposarray, "num":noposarray]

def conversion_rules2 = ConvRules.getConversionRulesFromFile(convCTX9NCA)

def cols = ["form","msd_cattex_bfm","F_bfm"]
def lines = frolextsv.readLines("UTF-8");

println "Read file. "+lines.size()+" lines..."
def data = []
for (int i = 1 ; i < lines.size() ; i++) {
	String line = lines[i]
	List split = pattern.split(line, cols.size())
	if (split.size() >= cols.size()) {
		def d = [split[0], split[1], split[2]]
		data << d
		if (DEBUG) println d
	}
}
println "Done: "+data.size()+" entries"

println "Transform NA and num to _nopos_ of the 'msd_cattex_bfm' column..."
int n = data.size()
for (int i = 0 ; i < n ; i++) {
	def d = data[i]
	if (DEBUG) println " $d"
	String msd_value = d[1];
	if (conversion_rules.containsKey(msd_value)) {
		data.remove(i)
		n--;
		i--;

		for (def v : conversion_rules.get(msd_value)) {
			def dtmp = d.clone();
			dtmp[1] = v
			data.add(dtmp)
		}
	}
}
println "Done: "+data.size()+" entries"
if (DEBUG) for (def d : data) println d

def wrongMSD = new HashSet();
wrongMSD.add("PONbfl")
wrongMSD.add("PONpfbl")
wrongMSD.add("PONfbfl")
println "Removing "+wrongMSD.size()+" wrong entries..."
for (int i = 0 ; i < data.size() ; i++) {
	def d = data[i]
	if (wrongMSD.contains(d[1])) {
		data.remove(i)
		i--
	}
}
println "Done: "+data.size()+" entries"
if (DEBUG) for (def d : data) println d

println "Add new msd column..."
def missing_conv = new HashSet()
n = data.size()
for (int i = 0 ; i < n ; i++) {
	def d = data[i]

	String msd_value = d[1];
	if (conversion_rules2.containsKey(msd_value)) {
		data.remove(i)
		i--;
		n--;

		for (def v : conversion_rules2.get(msd_value)) {
			def dtmp = d.clone();
			dtmp << v
			data.add(dtmp)
		}
	} else {
		missing_conv << d[1]
		d << ""
	}
}
println "Done: "+data.size()+" entries"
if (missing_conv.size() > 0) {
	println "Missing conversion rules: "+missing_conv
}
if (DEBUG) for (def d : data) println d

println "Sort by form..."
data.sort() {d1, d2 ->
	return d1[0].compareTo(d2[0])
}
println "Done: "+data.size()+" entries"
if (DEBUG) for (def d : data) println d

println "Remove _nopos_ not alone..."
def NOPOS = "_nopos_"
String currentForm = null
def tmp = []
def tmpi = [];
def toRemove = []
//ConsoleProgressBar cpb = new ConsoleProgressBar(data.size())
for (int i = 0 ; i < data.size() ; i++) {
//	cpb.tick()
	def d = data[i]
	if (d[0] != currentForm) {
		if (currentForm != null && tmp.size() > 1) {
			for (int j = 0 ; j < tmp.size() ; j++) {
				def dtmp = tmp[j]
				if (dtmp[1] == NOPOS) {
//					data.remove(tmpi[j])
//					i--;
					toRemove << tmpi[j]
				}
			}
		}
		currentForm = d[0]
		tmp.clear()
		tmpi.clear()
	}
	tmp << d
	tmpi << i
}
if (currentForm != null && tmp.size() > 1) {
			for (int j = 0 ; j < tmp.size() ; j++) {
				def dtmp = tmp[j]
				if (dtmp[1] == NOPOS) {
//					data.remove(tmpi[j])
//					i--;
					toRemove << tmpi[j]
				}
			}
		}
if (DEBUG) for (def i : toRemove) println data[i]
for (int i = toRemove.size() -1 ; i >= 0 ; i--) {// revert loop !
	//println "remove "+toRemove[i]+" th element = "+data[toRemove[i]]
	data.remove(toRemove[i])
}
println ""+toRemove.size()+" _nopos_ removed."
println "Done: "+data.size()+" entries"
if (DEBUG) for (def d : data) println d

return data