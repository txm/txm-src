package org.txm.dictionary.functions.tsv

import java.util.regex.Pattern;

DEBUG = false

File workingDirectory = new File("/home/mdecorde/TXM/results/lexique BFM")
File frolextsv = new File(workingDirectory, "afrlex.tsv")
if (!frolextsv.exists()) {
    println "Can't find $frolextsv"
    return
}

File convCTX9NCA = new File(workingDirectory, "conv nca ctx9-nca.tsv")
if (!convCTX9NCA.exists()) {
    println "Can't find $convCTX9NCA"
    return
}

Pattern pattern = Pattern.compile("\t");

String[] noposarray = ["_nopos_"]

def conversion_rules2 = ConvRules.getConversionRulesFromFile(convCTX9NCA)

def cols = ["form","msd","lemma","lemma-source"]
def lines = frolextsv.readLines("UTF-8");

println "Read file. "+lines.size()+" lines..."
def data = []
for (int i = 1 ; i < lines.size() ; i++) {
	String line = lines[i]
	List split = pattern.split(line, cols.size())
	if (split.size() >= cols.size()) {
		def d = [split[0], split[1], split[2], split[3]]
		data << d
		if (DEBUG) println d
	} else {
		println "Error with line '$line' gets '$split'"
	}
}
println "Done: "+data.size()+" entries"

println "Add new msd column..."
def missing_conv = new HashSet()
n = data.size()
for (int i = 0 ; i < n ; i++) {
	def d = data[i]
	
	String msd_value = d[1];
	if (conversion_rules2.containsKey(msd_value)) {
		data.remove(i)
		i--;
		n--;

		for (def v : conversion_rules2.get(msd_value)) {
			def dtmp = d.clone();
			dtmp << v
			data.add(dtmp)
		}
	} else {
		missing_conv << d[1]
		d << ""
	}
}
println "Done: "+data.size()+" entries"
if (missing_conv.size() > 0) {
	println "Missing conversion rules: "+missing_conv
}
if (DEBUG) for (def d : data) println d

println "Sort by form..."
data.sort() {d1, d2 ->
	return d1[0].compareTo(d2[0])
}
println "Done: "+data.size()+" entries"
if (DEBUG) for (def d : data) println d

//for (int i = 0 ; i < 20 ; i++) println data[(int)(Math.random()*data.size())]

return data