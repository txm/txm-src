package org.txm.dictionary.functions.tsv

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.regex.Pattern

class ConvRules {
	public static HashMap<Serializable, Serializable[]> getConversionRulesFromFile(
			File conversionTSVFile) throws IOException {
		HashMap<Serializable, Serializable[]> conversionRules = new HashMap<Serializable, Serializable[]>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(conversionTSVFile) , "UTF-8"));
		String line = reader.readLine();
		Pattern pattern = Pattern.compile("\t");
		Pattern multipleValuePattern = Pattern.compile("\\|");

		while (line != null) {
			String[] split = pattern.split(line, 2);
			if (split.length == 2) {
				conversionRules.put(split[0], multipleValuePattern.split(split[1]));
			}
			line = reader.readLine();
		}
		reader.close();

		return conversionRules;
	}
}
