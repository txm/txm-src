package org.txm.dictionary.functions.jpa;

import java.io.File;
import java.util.HashMap;

import org.txm.Toolbox;
import org.txm.utils.logger.Log;

/**
 * Creates Dictionaries
 * 
 * @author mdecorde
 *
 */
public class DictionaryFactory extends HashMap<String, Dictionary> {
	
	private static final long serialVersionUID = 8907433591669449847L;
	private static DictionaryFactory df;
	
	private DictionaryFactory() {
		
	}
	
	public static DictionaryFactory getInstance() {
		if (df != null) return df;
		df = new DictionaryFactory();
		return df;
	}
	/**
	 * 
	 * @param name
	 * @return null if an error occurred
	 */
	public Dictionary getDictionary(String name) {
		if (this.containsKey(name)) return this.get(name);
		
		File dictionariesDirectory = new File(Toolbox.getTxmHomePath(), "dictionaries");
		dictionariesDirectory.mkdirs(); // just in case
		
		if (this.containsKey(name)) return this.get(name);
		
		Dictionary dict;
		try {
			dict = new Dictionary(name, dictionariesDirectory);
			this.put(name, dict);
			return dict;
		} catch (Exception e) {
			System.out.println("Could not instanciate dictionary: "+name);
			Log.printStackTrace(e);
		}
		
		return null;
	}
	
	public void closeDictionary(String name) {
		if (this.containsKey(name)) {
			Dictionary d = this.get(name);
			d.getEntityManager().close();
			this.remove(name);
		}
	}
	
	public static void main(String[] args) {
		DictionaryFactory df = DictionaryFactory.getInstance();
		if (df == null) {
			System.out.println("DictionaryFactory not ready.");
		}
		Dictionary d = df.getDictionary("test");
		if (d != null) {
			System.out.println("Dictionary created: "+d);
		}
	}
}
