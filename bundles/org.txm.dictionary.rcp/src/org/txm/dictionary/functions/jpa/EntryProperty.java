package org.txm.dictionary.functions.jpa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(
  name="findAllEntryProperty",
  query="SELECT p FROM EntryProperty AS p"
)
public class EntryProperty implements Serializable {

	public static final long serialVersionUID = 7511725970194453783L;
	
	@Id
	@GeneratedValue
//	(strategy = GenerationType.SEQUENCE, generator = "MY_ENTITY_SEQ")
//	@SequenceGenerator(name = "MY_ENTITY_SEQ", sequenceName = "MY_ENTITY_SEQ", allocationSize = 10000)
	long i;
	
	public long ref;
	public String type;
	public Serializable value;
		
	public EntryProperty() { }
	
	public EntryProperty(long id, String type, Serializable value) {
		this.ref = id;
		this.type = type;
		this.value = value;
	}

	public String toString() {
		return ref+" "+type+" "+value;
	}
	
	public String toValue() {
		return type+"="+value;
	}
}
