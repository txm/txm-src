package org.txm.dictionary.functions.jpa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(
		  name="findAllEntryId",
		  query="SELECT e FROM EntryId AS e"
		)
public class EntryId implements Serializable {

	private static final long serialVersionUID = 7511725970194453783L;

	@Id @GeneratedValue
	long id;
	
	public EntryId() { }
		
	public EntryId(Long id2) {
		this.id = id2;
	}

	public String toString() {return Long.toString(id);}
}
