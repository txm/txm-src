package org.txm.dictionary.functions.jpa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class EntryPropertyType implements Serializable {

	private static final long serialVersionUID = 7511725970194453783L;

	@Id
	String id;
	
	public EntryPropertyType() { }
	
	public EntryPropertyType(String type) {
		this.id = type;
	}
	
	public String toString(){ return id;}
	
	public static void main(String[] args) {

		//EntryType form = new EntryType("form");
		EntryPropertyType F = new EntryPropertyType("F");
		EntryPropertyType msd = new EntryPropertyType("msd");
		EntryPropertyType lemma = new EntryPropertyType("lemma");
		
		EntryId je = new EntryId();
		EntryId tu = new EntryId();
		
		//EntryProperty form_je = new EntryProperty(je.id, form.id, "je");
		EntryProperty F_je = new EntryProperty(je.id, F.id, 120);
		EntryProperty msd_je = new EntryProperty(je.id, msd.id, "S");
//		EntryProperty lemma_je = new EntryProperty(je.id, lemma.id, "je");

		//EntryProperty form_tu = new EntryProperty(tu.id, form.id, "tu");
		EntryProperty F_tu = new EntryProperty(tu.id, F.id, 50);
		EntryProperty msd_tu = new EntryProperty(tu.id, msd.id, "S");
		EntryProperty lemma_tu = new EntryProperty(tu.id, lemma.id, "tu");
	}
}
