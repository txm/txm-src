package org.txm.dictionary.functions.jpa;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.FlushModeType;
import javax.persistence.Query;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.jpa.PersistenceProvider;
import org.eclipse.persistence.queries.Cursor;
import org.txm.Toolbox;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.logger.Log;

/**
 * Manages dictionary entries
 * 
 * @author mdecorde
 *
 */
public class Dictionary {
	
	public static final int FLUSHLIMIT = 100000;
	public static final String PERSISTENCE_UNIT_NAME = "HSQLKRPERSISTENCE";

	String name;
	File directory;

	EntityManagerFactory emf;
	protected EntityManager jpaem = null;

	private int insertCounter = 0;

	protected Dictionary(String name, File directory) throws Exception {
		this.name = name;
		this.directory = directory;
		this.jpaem = initializeEntityManager();
	}

	public void addEntries(ArrayList<EntryId> ids) {
		addEntries(ids, null);
	}

	private void addEntries(ArrayList<EntryId> ids, IProgressMonitor monitor) {
		jpaem.getTransaction().begin();

		for (EntryId t : ids) {
			jpaem.persist(t);
			if (monitor != null) monitor.worked(1);
			autoFlush();
		}

		jpaem.getTransaction().commit();
		jpaem.clear();
	}

	public void printQuerySize(String queryString) {
		Query query = jpaem.createQuery(queryString);
		query.setHint(QueryHints.CURSOR, true);
		System.out.println("SIZE="+((Cursor)query.getSingleResult()).size());
	}

	public Cursor executeQuery(String queryString) {
		Query query = jpaem.createQuery(queryString);
		query.setHint(QueryHints.CURSOR, true);
		return ((Cursor)query.getSingleResult());
	}

	public void addEntryId(EntryId e) {
		jpaem.getTransaction().begin();
		jpaem.persist(e);
		autoFlush();
		jpaem.getTransaction().commit();
	}

	public void addProperties(ArrayList<EntryProperty> properties) {
		addProperties(properties, null);
	}

	public void addProperties(ArrayList<EntryProperty> properties, ConsoleProgressBar cpb) {
		//System.out.println("addProperties: "+properties.size());
		jpaem.getTransaction().begin();
		for (EntryProperty t : properties) {
			jpaem.persist(t);
			autoFlush();
			if (cpb != null) cpb.tick();
		}
		jpaem.getTransaction().commit();
		jpaem.clear();
	}

	public void addProperty(EntryId e, EntryPropertyType t, Serializable v) {
		jpaem.getTransaction().begin();
		jpaem.persist(new EntryProperty(e.id, t.id, v));
		autoFlush();
		jpaem.getTransaction().commit();
	}

	public void addProperty(EntryProperty e) {
		jpaem.getTransaction().begin();
		jpaem.persist(e);
		autoFlush();
		jpaem.getTransaction().commit();
	}

	public void addPropertyType(EntryPropertyType t) {
		jpaem.getTransaction().begin();
		jpaem.persist(t);
		autoFlush();
		jpaem.getTransaction().commit();
	}

	public void addTypes(ArrayList<EntryPropertyType> types) {
		addTypes(types, null);
	}

	public void addTypes(ArrayList<EntryPropertyType> types, IProgressMonitor monitor) {
		jpaem.getTransaction().begin();
		for (EntryPropertyType t : types) {
			jpaem.persist(t);
			if (monitor != null) monitor.worked(1);
			autoFlush();
		}
		jpaem.getTransaction().commit();
		jpaem.clear();
	}

	private void autoFlush() {
		insertCounter++;
		if (insertCounter > FLUSHLIMIT) {

			jpaem.flush(); 
			jpaem.clear();
			//			if (jpaem.getTransaction().isActive())  {
			//				jpaem.getTransaction().commit();
			//				jpaem.clear();
			//				jpaem.getTransaction().begin();
			//			}

			insertCounter = 0;
		}
	} 

	public void clearAllData() {
		if (jpaem.getTransaction().isActive()) jpaem.getTransaction().rollback();

		jpaem.getTransaction().begin();
		jpaem.createQuery("DELETE FROM EntryId").executeUpdate();
		jpaem.createQuery("DELETE FROM EntryPropertyType").executeUpdate();
		jpaem.createQuery("DELETE FROM EntryProperty").executeUpdate();
		jpaem.getTransaction().commit();
	}


	public EntityManager getEntityManager() {
		return this.jpaem;
	}

	@SuppressWarnings("unchecked")
	public Cursor getEntries() {
		Query query = jpaem.createNamedQuery("findAllEntryId");
		query.setHint(QueryHints.CURSOR, true);
		return (Cursor)query.getSingleResult();
	}

	public Cursor getGroupedEntriesID(EntryPropertyType joinType) {
		Query query = jpaem.createQuery("SELECT p, p2.value FROM EntryProperty AS p JOIN EntryProperty AS p2 ON p.ref=p2.ref WHERE p2.type LIKE '"+joinType.id+"' ORDER BY p2.type");
		query.setHint(QueryHints.CURSOR, true);
		return (Cursor)query.getSingleResult();
	}

	public Cursor getGroupedEntriesID(EntryPropertyType joinType, EntryPropertyType type) {
		Query query = jpaem.createQuery("SELECT p, p2.value FROM EntryProperty AS p JOIN EntryProperty AS p2 ON p.ref=p2.ref WHERE p.type LIKE '"+type.id+"' AND p2.type LIKE '"+joinType.id+"' ORDER BY p2.value");
		query.setHint(QueryHints.CURSOR, true);
		return (Cursor)query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	public Cursor getProperties() {
		Query query =  jpaem.createNamedQuery("findAllEntryProperty");
		query.setHint(QueryHints.CURSOR, true);
		return (Cursor)query.getSingleResult();
	}

	private List<EntryProperty> getProperties(int i) {
		return jpaem.createQuery("SELECT e FROM EntryProperty AS e").setMaxResults(i).getResultList();
	}

	@SuppressWarnings("unchecked")
	public Cursor getProperties(EntryPropertyType type) {
		Query query = jpaem.createQuery("SELECT p FROM EntryProperty AS p WHERE p.type LIKE '"+type.id+"'");
		query.setHint(QueryHints.CURSOR, true);
		return (Cursor)query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	public List<EntryProperty> getProperties(EntryId e) {
		Query query = jpaem.createQuery("SELECT p FROM EntryProperty AS p WHERE p.ref="+e.id+"");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<EntryProperty> getProperties(EntryId e, EntryPropertyType type) {
		Query query = jpaem.createQuery("SELECT p FROM EntryProperty AS p WHERE p.ref="+e.id+" AND p.type LIKE '"+type.id+"'");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public Cursor getGroupedProperties() {
		Query query = jpaem.createQuery("SELECT p FROM EntryProperty AS p ORDER BY p.ref ASC");
		query.setHint(QueryHints.CURSOR, true);
		return (Cursor)query.getSingleResult();
	}

	private Cursor getGroupedProperties(EntryPropertyType type) {
		Query query = jpaem.createQuery("SELECT p FROM EntryProperty AS p WHERE p.type LIKE '"+type.id+"' ORDER BY p.ref ASC");
		query.setHint(QueryHints.CURSOR, true);
		return (Cursor)query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	public List<EntryPropertyType> getTypes() {
		return jpaem.createQuery("SELECT t FROM EntryPropertyType AS t").getResultList();
	}

	public boolean hasEntryId(long id) {
		return jpaem.find(EntryId.class, id) != null;
	}

	public boolean hasEntryPropertyType(String id) {
		return jpaem.find(EntryPropertyType.class, id) != null;
	}

	public boolean hasEntryPropertyType(EntryPropertyType type) {
		return jpaem.find(EntryPropertyType.class, type.id) != null;
	}

	/**
	 * Initialize the JPA persistence database
	 * @return
	 */
	public EntityManager initializeEntityManager() throws Exception {
		HashMap<String, Object> properties = new HashMap<String, Object>();
		properties.put(PersistenceUnitProperties.CLASSLOADER, Toolbox.class.getClassLoader());
		properties.put("javax.persistence.jdbc.driver", "org.hsqldb.jdbcDriver");
		
		if (Log.getLevel().intValue() < Level.INFO.intValue()) {
			properties.put(PersistenceUnitProperties.LOGGING_LEVEL, "WARNING");
			properties.put(PersistenceUnitProperties.LOGGING_PARAMETERS, "true");
		} else {
			properties.put(PersistenceUnitProperties.LOGGING_LEVEL, "OFF");
			properties.put(PersistenceUnitProperties.LOGGING_PARAMETERS, "false");
		}
		
		
		String urlProperty = "jdbc:hsqldb:file:"+directory.getAbsolutePath()+"/"+name+";shutdown=true";
		if (Log.getLevel().intValue() < Level.INFO.intValue()) {
			urlProperty += "";
		} else {
			urlProperty += "hsqldb.applog=0;hsqldb.sqllog=0";
		}
		properties.put("javax.persistence.jdbc.url", urlProperty);
		
		//		properties.put("javax.persistence.jdbc.driver", "org.sqlite.JDBC");
		//		properties.put("javax.persistence.jdbc.url", "jdbc:sqlite:file:"+directory.getAbsolutePath()+"/"+name+";shutdown=true"); //file:"+directory.getAbsolutePath()+"/
		properties.put("javax.persistence.jdbc.username", "SA");
		properties.put(PersistenceUnitProperties.DDL_GENERATION_MODE, "database");
		properties.put(PersistenceUnitProperties.DDL_GENERATION, "create-or-extend-tables"); // create&update table if needed
		//properties.put(PersistenceUnitProperties.DDL_GENERATION, "drop-and-create-tables"); // create&update table if needed

		// <property name="eclipselink.ddl-generation" value="drop-and-create-tables"/>

		emf = new PersistenceProvider().createEntityManagerFactory(PERSISTENCE_UNIT_NAME, properties);
		EntityManager entityManager = (EntityManager) emf.createEntityManager();
		this.jpaem = entityManager;
		jpaem.setFlushMode(FlushModeType.COMMIT);

		return entityManager;

	}

	public boolean isReady() {
		return jpaem != null;
	}

	/**
	 * Load a dictionary from a TSV file, all previously store entries are lost 
	 * 
	 * @param tsvFile
	 * @return true if success
	 * 
	 * @throws IOException
	 */
	public boolean loadFromTSVFile(File tsvFile) throws IOException {
		clearAllData(); // empty before
		//CsvReader reader = new CsvReader(tsvFile.getAbsolutePath(), '\t', Charset.forName("UTF-8"));
		BufferedReader reader = new BufferedReader(new FileReader(tsvFile));

		String l = reader.readLine();
		String[] split = l.split("\t");
		int ncolumn = split.length;
		if (ncolumn == 0) {
			System.out.println("No header found: no column");
			reader.close();
			return false;
		}

		ArrayList<EntryPropertyType> newTypes = new ArrayList<EntryPropertyType>();
		for (int i = 0 ; i < ncolumn ; i++) {
			newTypes.add(new EntryPropertyType(split[i]));
		}
		System.out.println("Add EntryPropertyTypes: "+newTypes.size());
		ConsoleProgressBar cpb = new ConsoleProgressBar(newTypes.size());
		addTypes(newTypes, cpb);

		ArrayList<EntryProperty> properties = new ArrayList<EntryProperty>();
		ArrayList<EntryId> entries = new ArrayList<EntryId>();
		ArrayList<String[]> lines = new ArrayList<String[]>();
		String line = reader.readLine();
		while (line != null) {

			split = line.split("\t", ncolumn);
			if (split.length != ncolumn) {
				continue;
			}

			EntryId e = new EntryId();
			entries.add(e);

			lines.add(split);
			line = reader.readLine();
		}
		reader.close();

		// add entries
		cpb = new ConsoleProgressBar(entries.size());
		System.out.println("Add new Entries: "+(entries.size()));
		this.addEntries(entries, cpb);

		// add properties
		cpb = new ConsoleProgressBar(entries.size()*ncolumn);
		System.out.println("Add new properties: "+(entries.size()*ncolumn));
		for (int iLine = 0 ; iLine < lines.size() ; iLine++) {
			split = lines.get(iLine);
			EntryId e = entries.get(iLine);

			for (int i = 0 ; i < ncolumn ; i++) {
				EntryProperty p = new EntryProperty(e.id, newTypes.get(i).id, split[i]);
				properties.add(p);
			}

			if (properties.size() > FLUSHLIMIT) { // avoid GC heapspace
				addProperties(properties, cpb);
				properties.clear();
			}
		}

		addProperties(properties, cpb); // last remaining properties to add
		properties = null;
		entries = null;
		lines = null;
		//System.gc();
		return true;
	}

	/**
	 * Merge a dictionary into this
	 * @param d2
	 * @param mergingRules
	 * @return
	 */
	public boolean merge(Dictionary d2, String mergingRules) {
		// add missing EntryPropertyType 

		System.out.println("Merging "+this+" with "+d2);
		System.out.println(" ** EntryPropertyType values...");
		List<EntryPropertyType> types = d2.getTypes();
		ConsoleProgressBar cpb = new ConsoleProgressBar(types.size());
		for (EntryPropertyType t : types) {
			cpb.tick();
			if (this.hasEntryPropertyType(t.id)) {
				//System.out.println(t.id+" EntryPropertyType already exists.");
			} else {
				this.addPropertyType(t);
				//System.out.println(t.id+" EntryPropertyType added.");
			}
		}

		// add missing EntryId
		System.out.println(" ** EntryId values...");
		ArrayList<EntryId> entries = new ArrayList<EntryId>();
		Cursor cursor = d2.getEntries();
		while (cursor.hasNext()) {
			EntryId e = (EntryId) cursor.next();
			if (this.hasEntryId(e.id)) {
				//System.out.println(e.id+" EntryId already exists.");
			} else {
				entries.add(e);
			}
		}
		cpb = new ConsoleProgressBar(entries.size());
		System.out.println(" "+entries.size()+ " new entries.");
		this.addEntries(entries);

		// add missing EntryProperty
		System.out.println(" ** EntryProperty values...");
		ArrayList<EntryProperty> properties = new ArrayList<EntryProperty>();
		cursor = d2.getProperties();
		while (cursor.hasNext()) {
			EntryProperty p = (EntryProperty) cursor.next();
			try {
				properties.add(p);
			} catch(Exception e) {
				System.out.println("Error with "+p+"="+e);
				return false;				
			}
		}
		cpb = new ConsoleProgressBar(properties.size());
		System.out.println(" "+properties.size()+ " new properties.");
		this.addProperties(properties, cpb);

		// remove duplicates using a set of criteria ?
		return true;
	}

	/**
	 * 
	 * @param newEntries
	 * @return the entries that collides with existing entries
	 */
	public ArrayList<EntryId[]> mergeEntryIds(ArrayList<EntryId> newEntries) {
		ArrayList<EntryId[]> duplicates = new ArrayList<EntryId[]>();
		ArrayList<EntryId> noduplicates = new ArrayList<EntryId>();
		for (EntryId e : newEntries) {
			EntryId existingEntryProperty = jpaem.find(EntryId.class, e.id);
			if (existingEntryProperty != null) {
				duplicates.add(new EntryId[]{e, existingEntryProperty});
			} else {
				noduplicates.add(e);
			}
		}

		addEntries(noduplicates);
		return duplicates;
	}

	/**
	 * 
	 * @param newEntryProperties the entries that collides with existing entries
	 */
	public void mergeEntryProperties(ArrayList<EntryProperty> newEntryProperties) {

		Cursor cursor = getProperties();
		while (cursor.hasNext()) {
			EntryProperty p = (EntryProperty) cursor.next();
			newEntryProperties.remove(p);
		}

		ConsoleProgressBar cpb = new ConsoleProgressBar(newEntryProperties.size());
		addProperties(newEntryProperties, cpb);
		return;
	}

	/**
	 * 
	 * @param newEntryPropertyTypes
	 * @return the entries that collides with existing entries
	 */
	public ArrayList<EntryPropertyType[]> mergeEntryPropertyTypes(ArrayList<EntryPropertyType> newEntryPropertyTypes) {
		ArrayList<EntryPropertyType[]> duplicates = new ArrayList<EntryPropertyType[]>();
		ArrayList<EntryPropertyType> noduplicates = new ArrayList<EntryPropertyType>();
		for (EntryPropertyType e : newEntryPropertyTypes) {
			EntryPropertyType existingEntryProperty = jpaem.find(EntryPropertyType.class, e.id);
			if (existingEntryProperty != null) {
				duplicates.add(new EntryPropertyType[]{e, existingEntryProperty});
			} else {
				noduplicates.add(e);
			}
		}

		addTypes(newEntryPropertyTypes);

		return duplicates;
	} 

	public void print() {
		//TypedQuery<EntryProperty> query = jpaem.createQuery("SELECT e FROM EntryProperty AS e", EntryProperty.class);
		Cursor properties = getProperties();
		while (properties.hasNext()) {
			System.out.println(properties.next());
		}
	}

	public boolean export(File tsvFile, List<EntryPropertyType> types) {
		PrintWriter writer = null;
		HashSet<String> types_hash = new HashSet<String>();
		String header = "";
		for (int i = 0 ; i < types.size() ; i++) {
			EntryPropertyType type = types.get(i);
			types_hash.add(type.id);
			if (i > 0)
				header += "\t"+type.id;
			else 
				header += type.id;
		}

		StringBuilder str = new StringBuilder();
		boolean success = false;
		try {
			tsvFile.getAbsoluteFile().getParentFile().mkdirs();
			writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tsvFile), "UTF-8")));

			writer.println(header);
			//TypedQuery<EntryProperty> query = jpaem.createQuery("SELECT e FROM EntryProperty AS e", EntryProperty.class);
			HashMap<String, Serializable> values = new HashMap<String, Serializable>();
			long currentEntryId = -1;
			Cursor properties = getGroupedProperties();
			while (properties.hasNext()) {
				EntryProperty p = (EntryProperty) properties.next();

				long ref = p.ref;
				if (ref != currentEntryId) { // check if it's the next Entry
					if (currentEntryId != -1 && values.size() > 0) { // write EntryProperties if necessary
						str.setLength(0);
						for (int i = 0 ; i < types.size() ; i++) {
							EntryPropertyType type = types.get(i);
							if (i != 0)	str.append("\t");
							Serializable s = values.get(type.id);
							if (s == null) {
								str.append("");
							} else {
								str.append(s);
							}
						}
						writer.println(str.toString());
					}
					currentEntryId = p.ref;
					values.clear(); // reset values
				}

				if (types_hash.contains(p.type)) { // store value for the current Entry
					values.put(p.type, p.value);
				}
			}

			if (currentEntryId != -1 && values.size() > 0) { // write LAST EntryProperties if necessary
				str.setLength(0);
				for (int i = 0 ; i < types.size() ; i++) {
					EntryPropertyType type = types.get(i);
					if (i != 0)	str.append("\t");
					Serializable s = values.get(type.id);
					if (s == null) {
						str.append("");
					} else {
						str.append(s);
					}
				}
				writer.println(str.toString());
			}

			success = true;
		} catch (Exception e) {
			System.out.println("Fail to export dictionary: "+e);
			Log.printStackTrace(e);
		} finally {
			if (writer != null) writer.close();
		}
		return success;
	}

	public void printSummary(int NSAMPLE) {
		List<EntryPropertyType> types = this.getTypes();
		System.out.println(""+types.size()+" types: "+types);

		Cursor entries = this.getEntries();
		int i = 0 ;
		ArrayList<EntryId> list = new ArrayList<EntryId>();
		while (i < 10 && entries.hasNext()) {
			list.add((EntryId) entries.next());
		}
		if (entries.size() <= 10)
			System.out.println(""+entries.size()+" entries, value are "+list);
		else 
			System.out.println(""+entries.size()+" entries, 10 first values are "+list);

		List<EntryProperty> properties = getProperties(NSAMPLE);
		Cursor allProperties = getProperties();
		ArrayList<Object> values = new ArrayList<Object>();
		System.out.println(""+allProperties.size()+" properties first values are : "+values);

		if (entries.size() == 0) {
			System.out.println("No sample value.");
			return;
		}
		if (NSAMPLE > 0) {
			System.out.println("Samples: ");
			for (i = 0 ; i < NSAMPLE ; i++) {
				int iEntryId = (int) (Math.random()*entries.size());
				EntryId EntryId = list.get(iEntryId);
				System.out.println("Entry="+EntryId);
				List<EntryProperty> props = jpaem.createQuery("SELECT p FROM EntryProperty AS p WHERE p.ref LIKE '"+EntryId.id+"'").getResultList();
				System.out.println(" Properties="+props);
			}
		}
	}

	public ArrayList<EntryProperty> recodeEntryProperties(EntryPropertyType type, EntryPropertyType newType, File conversionTSVFile) throws IOException {
		HashMap<Serializable, Serializable[]> conversionRules = getConversionRulesFromFile(conversionTSVFile);
		return recodeEntryProperties(type, newType, conversionRules);
	}

	public static HashMap<Serializable, Serializable[]> getConversionRulesFromFile(
			File conversionTSVFile) throws IOException {
		HashMap<Serializable, Serializable[]> conversionRules = new HashMap<Serializable, Serializable[]>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(conversionTSVFile) , "UTF-8"));
		String line = reader.readLine();
		Pattern pattern = Pattern.compile("\t");
		Pattern multipleValuePattern = Pattern.compile("\\|");

		while (line != null) {
			String[] split = pattern.split(line, 2);
			if (split.length == 2) {
				conversionRules.put(split[0], multipleValuePattern.split(split[1]));
			}
			line = reader.readLine();
		}
		reader.close();

		return conversionRules;
	}

	/**
	 * Use conversion rules to translate EntryId properties
	 * 
	 * @param type
	 * @param newType
	 * @param conversionRules
	 * @return the new properties
	 */
	public ArrayList<EntryProperty> recodeEntryProperties(EntryPropertyType type, EntryPropertyType newType, HashMap<Serializable, Serializable[]> conversionRules) {
		if (!hasEntryPropertyType(newType)) {
			addPropertyType(newType);
		}

		// conversion
		ArrayList<EntryProperty> newProperties = new ArrayList<EntryProperty>();
		HashSet<Serializable> missings = new HashSet<Serializable>();
		Cursor cursor = getProperties(type);

		while (cursor.hasNext()) {
			EntryProperty p = (EntryProperty) cursor.next();
			// get a new values if any
			if (conversionRules.containsKey(p.value)) {
				for (Serializable v : conversionRules.get(p.value)) {
					newProperties.add(new EntryProperty(p.ref, newType.id, v));
				}
			} else {
				missings.add(p.value);
			}
		}

		if (missings.size() > 0) {
			System.out.println("Warning: there was some missing values: "+missings);
		}
		return newProperties;
	}

	/**
	 * Use conversion rules to translate EntryId properties
	 * 
	 * @param type
	 * @param conversionRules
	 * @return the new properties
	 */
	public int recodeEntryProperties(EntryPropertyType type, HashMap<Serializable, Serializable[]> conversionRules) {
		int c = 0;
		jpaem.getTransaction().begin();

		Cursor properties = getProperties(type);
		ConsoleProgressBar cpb = new ConsoleProgressBar(properties.size());
		while (properties.hasNext()) {
			cpb.tick();
			EntryProperty p = (EntryProperty) properties.next();

			for (Serializable s : conversionRules.keySet()) {
				if (p.value.equals(s)) {
					for (Serializable news : conversionRules.get(s)) {
						c++;
						p.value = news;
						continue;// one for now
					}
				}
			}
		}

		jpaem.getTransaction().commit();

		return c;
	}

	public int removeEntryIfNotAlone(EntryPropertyType joinType, EntryPropertyType type, Serializable value) {
		ArrayList<Long> entriesToRemove = new ArrayList<Long>();

		// group entries by EntryPropertyType "form"
		Cursor result = getGroupedEntriesID(joinType, type);
		System.out.println("N "+joinType+" properties to process: "+result.size());
//		Serializable currentTypeValue = null;
//		ArrayList<EntryProperty> props = new ArrayList<EntryProperty>();
//
//		while (result.hasNext()) {
//			Object[] o = (Object[]) result.next();
//			EntryProperty p = (EntryProperty) o[0];
//			Serializable v = (Serializable) o[1];
//
//			if (!v.equals(currentTypeValue)) {
//				if (currentTypeValue != null && props.size() > 1) {
//					for (EntryProperty p2 : props) {
//						if (p2.value.equals(value)) {
//							System.out.println("Add entry to remove: "+p2.ref);
//							entriesToRemove.add(p2.ref);
//						}
//					}
//				}
//				currentTypeValue = v; // the new type value
//				props.clear();
//			}
//			props.add(p);
//		}
//
//		if (currentTypeValue != null && props.size() > 1) {
//			for (EntryProperty p2 : props) {
//				if (p2.value.equals(value)) {
//					System.out.println("Add entry to remove: "+p2.ref);
//					entriesToRemove.add(p2.ref);
//				}
//			}
//		}
//
//		System.out.println("Removing "+entriesToRemove.size()+" entries...");
//		this.removeEntries(entriesToRemove);

		return entriesToRemove.size();
	}

	private void removeEntryProperties(
			ArrayList<EntryProperty> propertiesToRemove, ConsoleProgressBar cpb) {
		jpaem.getTransaction().begin();
		for (EntryProperty p : propertiesToRemove) {
			if (cpb != null) cpb.tick();
			jpaem.remove(p);
		}
		jpaem.getTransaction().commit();
	}

	public void removeEntryId(EntryId e) {
		jpaem.getTransaction().begin();
		jpaem.remove(e);
		jpaem.getTransaction().commit();

		removeEntryProperties(e);
	}

	private void removeEntries(ArrayList<Long> entriesToRemove) {
		jpaem.getTransaction().begin();
		for (Long id : entriesToRemove) {
			EntryId e = jpaem.find(EntryId.class, id);
			System.out.println(" remove "+e);
			jpaem.remove(e);
			System.out.println(" remove "+e+" properties");
			jpaem.createQuery("DELETE FROM EntryProperty p WHERE p.ref LIKE '"+e.id+"'").executeUpdate();
		}
		jpaem.getTransaction().commit();
	}

	public void removeEntryProperties(EntryId e) {
		jpaem.getTransaction().begin();
		jpaem.createQuery("DELETE FROM EntryProperty p WHERE p.ref LIKE '"+e.id+"'").executeUpdate();
		jpaem.getTransaction().commit();
	}

	public void removeEntryProperties(EntryId e, EntryPropertyType t) {
		jpaem.getTransaction().begin();
		jpaem.createQuery("DELETE FROM EntryProperty p WHERE p.ref LIKE '"+e.id+"' AND p.type LIKE '"+t.id+"'").executeUpdate();
		jpaem.getTransaction().commit();
	}

	public void removeEntryProperties(EntryPropertyType t) {
		jpaem.getTransaction().begin();
		jpaem.createQuery("DELETE FROM EntryProperty p WHERE p.type LIKE '"+t.id+"'").executeUpdate();
		jpaem.getTransaction().commit();
	}

	public void removeEntryProperty(EntryId e, EntryPropertyType t, Serializable v) {
		jpaem.getTransaction().begin();
		jpaem.remove(new EntryProperty(e.id, t.id, v));
		jpaem.getTransaction().commit();

	}

	public void removeEntryPropertyType(EntryPropertyType t) {
		jpaem.getTransaction().begin();
		jpaem.remove(t);
		jpaem.getTransaction().commit();

		removeEntryProperties(t);
	}

	public static void main(String args[]) {
		try {
			HashMap<Serializable, Serializable[]> rules = 
					Dictionary.getConversionRulesFromFile(new File("/home/mdecorde/TXM/results/lexique BFM", 
							"conv nca ctx9.tsv"));
			for (Serializable k : rules.keySet()) {
				System.out.println(k+"="+Arrays.toString(rules.get(k)));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
