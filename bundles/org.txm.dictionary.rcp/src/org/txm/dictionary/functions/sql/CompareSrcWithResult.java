package org.txm.dictionary.functions.sql;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

import org.txm.utils.io.IOUtils;

public class CompareSrcWithResult {
	public static void main(String args[]) throws IOException {
		File dir = new File("/home/mdecorde/TXM/results/lexique BFM/result");

		File frolex = new File(dir, "frolex.tsv");
		
		File srcs[] = {new File(dir, "bfmlex.tsv"), new File(dir, "afrlex.tsv"), new File(dir, "dmflex.tsv")};
		
		for (File src : srcs) {
			compare(src, frolex);
		}
	}

	private static void compare(File src, File frolex) throws IOException {
		System.out.println("COMPARE "+src.getName()+" with "+frolex.getName());
		BufferedReader readerSRC = IOUtils.getReader(src);
		BufferedReader readerFRO = IOUtils.getReader(frolex);
		
		String lineSRC = readerSRC.readLine(); // no header, src first entry
		String lineFRO = readerFRO.readLine(); // header
		lineFRO = readerSRC.readLine(); // frolex first entry
		
		
		String splitFRO[] = lineFRO.split("\t");
		String firstFRO = splitFRO[0];
		
		while (lineSRC != null) {
			String splitSRC[] = lineSRC.split("\t");
			String firstSRC = splitSRC[0];
			System.out.println(firstSRC);
			
			
			while (firstFRO != null) {
				int c = firstSRC.compareTo(firstFRO);
				if (c == 0) {
					System.out.println("SRC=FRO -> "+firstFRO);
					continue; // ok for this form				
				} else if ( c < 0 ) { 
					continue; // go for next word but this one is missing
				} else { 
					System.out.println("Next FRO line");
					lineFRO = readerFRO.readLine(); // frolex first entry
					if (lineFRO == null) return;
					splitFRO = lineFRO.split("\t");
					firstFRO = splitFRO[0];
				}
			}
//			
//			int c = firstSRC.compareTo(firstFRO);
//			if (c == 0) {
//				continue; // ok for this form				
//			}
//			if (c > 0) {
//				System.out.println("All FRO forms are lower than SRC forms="+firstSRC+", no need to go further");
//				return; // ok for this form				
//			}
//			
//			while (c > 0) {
//				System.out.println("Next FRO line");
//				lineFRO = readerFRO.readLine(); // frolex first entry
//				if (lineFRO == null) return;
//				splitFRO = lineFRO.split("\t");
//				firstFRO = splitFRO[0];
//				c = firstSRC.compareTo(firstFRO);
//			}
			
			lineSRC = readerSRC.readLine();
		}
	}
}
