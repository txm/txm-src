package org.txm.dictionary.functions.sql;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.io.IOUtils;

/**
 * Manages a dictionary using a SQL table
 * 
 * @author mdecorde
 *
 */
public class Dictionary {

	/**
	 * used by ruled conversions methods to abandon as soon a rule is missing
	 */
	public static final String ABANDON = "abandon";
	/**
	 * used by ruled conversions methods to copy the tested value in the destination column if the rule is missing
	 */
	public static final String COPYSRC = "copier";
	/**
	 * used by ruled conversions methods to not modify the destination column if the rule is missing
	 */
	public static final String COPYDEST = "copier_dest";
	/**
	 * used by ruled conversions methods to not delete the entry if the rule is missing
	 */
	public static final String DELETE = "supprimer";
	
	private static boolean debug = false;
	public static final int FLUSHLIMIT = 100000;

	public static int NQUESTIONMARKS = 40;
	public static String[] questionmarks = new String[NQUESTIONMARKS+1];
	static {
		String s= "?";
		for (int i = 1 ; i <= NQUESTIONMARKS ; i++) {
			questionmarks[i] = s;
			s += ",?";
		}
	}
	/**
	 * Build a Pattern to values hashmap from a tabulated file (first column=pattern, second column=values separated with "+"
	 * 
	 * @param conversionTSVFile
	 * @return
	 * @throws IOException
	 */
	public static LinkedHashMap<Pattern, Serializable[]> getConversionRulesFromFile(
			File conversionTSVFile) throws IOException {
		LinkedHashMap<Pattern, Serializable[]> conversionRules = new LinkedHashMap<Pattern, Serializable[]>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(conversionTSVFile) , "UTF-8"));
		String line = reader.readLine();
		Pattern pattern = Pattern.compile("\t");
		Pattern multipleValuePattern = Pattern.compile("\\+");

		while (line != null) {
			String[] split = pattern.split(line, 2);
			if (split.length == 2) {
				conversionRules.put(Pattern.compile(split[0]), multipleValuePattern.split(split[1]));
			}
			line = reader.readLine();
		}
		reader.close();

		return conversionRules;
	}

	public static void main(String args[]) {

		File dir = new File("/home/mdecorde/TEMP/HSQL");
		File tsvFile = new File("/home/mdecorde/TXM/results/lexique BFM/afrlex-full.tsv");
		File tsvFile2 = new File("/home/mdecorde/TXM/results/lexique BFM/BFM2013-dict.tsv");
		dir.mkdir();

		try {
			DictionaryManager dm = DictionaryManager.getInstance();
			Dictionary d = dm.getDictionary("afrlex");
			d.loadFromTSVFile(tsvFile);

			Dictionary d2 = dm.getDictionary("bfmlex");
			d2.loadFromTSVFile(tsvFile2);

		} catch (Exception e) {
			e.printStackTrace();
		}

		//		try {
		//			HashMap<Serializable, Serializable[]> rules = 
		//					Dictionary.getConversionRulesFromFile(new File("/home/mdecorde/TXM/results/lexique BFM", 
		//							"conv nca ctx9.tsv"));
		//			for (Serializable k : rules.keySet()) {
		//				System.out.println(k+"="+Arrays.toString(rules.get(k)));
		//			}
		//		} catch (IOException e) {
		//			e.printStackTrace();
		//		}
	}
	public static void setDebug(boolean b) {debug=b;}

	String EMPTY = "";
	HSQLFunctions functions;

	int iLINE = 0;

	private int insertCounter = 0;

	String name;

	boolean populated = false;

	private ArrayList<String> types;

	private String typesSQL = "*";

	/**
	 * The table must be populate with loadFromTSV or loadFromIndex 
	 * 
	 * @param name
	 * @param functions 
	 * @throws Exception
	 */
	protected Dictionary(String name, HSQLFunctions functions) throws Exception {
		this.name = name;
		this.functions = functions;
		this.types = new ArrayList<String>();

		if (functions.containsTable(name)) {
			types = functions.getTableColumns(name);
			typesSQL = "\""+StringUtils.join(types, "\",\"")+"\"";
		}
	}

	private boolean addEntry(PreparedStatement ps, String[] split) throws SQLException {	
		for (int i = 0 ; i < types.size() ; i++) {
			if (i < split.length)
				ps.setString(i+1, split[i]);
			else 
				ps.setString(i+1, EMPTY);
		}
		//System.out.println(Arrays.toString(split));
		ps.executeUpdate();  
		return true;
	}

	/**
	 * add a new column to the dictionary table
	 * 
	 * @param type
	 * @throws SQLException
	 */
	public void addType(String type) throws SQLException {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return;}

		if (types.contains(type)) return;

		types.add(type);
		typesSQL = "\""+StringUtils.join(types, "\",\"")+"\"";

		functions.addColumn(name, type);
	}

	/**
	 * add multiple columns to a dictionary table
	 * 
	 * @param types
	 * @throws SQLException
	 */
	public void addTypes(List<String> types) throws SQLException {
		if (!functions.containsTable(name)) {
			functions.createTable(name, types);
			this.types.addAll(types);
		}

		for (String type : types) {
			if (this.types.contains(type)) continue;
			this.types.add(type);
			functions.addColumn(name, type);
		}
		typesSQL = "\""+StringUtils.join(types, "\",\"")+"\"";
	}

	private void autoFlush() {
		insertCounter++;
		if (insertCounter > FLUSHLIMIT) {
			insertCounter = 0;
		}
	}

	/**
	 * empty the table 
	 * 
	 * @throws SQLException
	 */
	public void clear() throws SQLException {
		if (functions.containsTable(name)) {
			functions.clearTable(name);
			populated = false;
		}
	}

	/**
	 * 
	 * @return a ResultSet of all the entries of the table
	 * @throws SQLException
	 */
	public ResultSet getLines() throws SQLException {
		if (!populated) return null;

		Statement statement = functions.getConnection().createStatement();

		String query = "SELECT * FROM \""+name+"\";";
		if (debug) System.out.println(query);
		ResultSet result = statement.executeQuery(query);

		return result;
	}
	
	/**
	 * 
	 * @return a ResultSet of all the entries of the table
	 * @throws SQLException
	 */
	public ResultSet getOrderedLines(String[] columns) throws SQLException {
		if (!populated) return null;

		Statement statement = functions.getConnection().createStatement();

		String query = "SELECT * FROM \""+name+"\" ORDER BY \""+StringUtils.join(columns, "\",\"")+"\";";
		if (debug) System.out.println(query);
		ResultSet result = statement.executeQuery(query);

		return result;
	}
	
	public void setPopulated(boolean populated) {
		this.populated = populated;
	}
	
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @return the number of entries in the table
	 * @throws SQLException
	 */
	public int getSize() throws SQLException {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return 0;}

		Statement statement = functions.getConnection().createStatement();
		String query = "SELECT COUNT(*) from \""+name+"\";";
		if (debug) System.out.println(query);
		ResultSet result = statement.executeQuery(query);

		int n = 0;
		if (result.next()) {
			n = result.getInt(1);
		}

		statement.close();
		return n;
	}

	/**
	 * 
	 * @param type
	 * @return a ResultSet of all the entries of the table sorted with a column
	 * @throws SQLException
	 */
	public ResultSet getSortedLines(String type) throws SQLException {
		if (!populated) return null;

		Statement statement = functions.getConnection().createStatement();

		String query = "SELECT * FROM \""+name+"\" ORDER BY \""+type+"\";";

		ResultSet result = statement.executeQuery(query);

		return result;
	}

	public List<String> getTypes() {
		return types;
	}

	/**
	 * Display the entries that matches the regular expression patterns
	 * 
	 * @param patternPerColumn
	 * @return the number of entries that matches
	 * @throws SQLException
	 */
	public int grep(HashMap<String, String> patternPerColumn) throws SQLException {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return 0;}
		String query = "SELECT * FROM \""+name+"\"";

		if (patternPerColumn.size() > 0) query += " WHERE ";
		for (String col : patternPerColumn.keySet()) {
			query += " REGEXP_MATCHES(\""+col+"\", '"+patternPerColumn.get(col)+"')";
		}

		query += ";";

		int n = this.queryAndPrint(query);
		System.out.println("N result: "+n);
		return n;
	}
	
	/**
	 * Display the entries that matches one regular expression pattern
	 * 
	 * @return the number of entries that matches
	 * @throws SQLException
	 */
	public int grep(String type, String pattern) throws SQLException {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return 0;}
		HashMap<String, String> patternPerColumn = new HashMap<String, String>();
		patternPerColumn.put(type, pattern);
		return grep(patternPerColumn);
	}

	/**
	 * Insert values from the d1 Dictionary to this Dictionary if the 'list' matches this Dictionary types
	 * 
	 * @param query a SELECT QUERY
	 * @return the number of entries inserted
	 * @throws SQLException
	 */
	public int insertValues(String query) throws SQLException {
		Statement statement = functions.getConnection().createStatement();
		int n = statement.executeUpdate("INSERT INTO \""+name+"\" "+query); // ("+typesSQL+")
		statement.close();
		
		populated = n > 0;
		return n;
	}
	
	/**
	 * Insert values from the d1 Dictionary to this Dictionary if the 'list' matches this Dictionary types
	 * 
	 * @param d1
	 * @param list
	 * @return the number of entries inserted
	 * @throws SQLException
	 */
	public int insertValues(Dictionary d1, List<String> list) throws SQLException {
		
		if (types.size() != list.size()) return 0;
		
		if (!functions.containsTable(name)) { // create the table with the right columns
			functions.createTable(name, types);
		}

		String batchquery = "INSERT INTO \""+name+"\" VALUES ("+questionmarks[types.size()]+")";
		if (debug) System.out.println(batchquery);
		
		List<String> nlist = new ArrayList<String>();
		for (String s : list) if (s.length() > 0) nlist.add(s);
		String query_select = "SELECT \""+StringUtils.join(nlist, "\",\"")+"\" FROM \""+d1.name+"\"";
		if (debug) System.out.println(query_select);
		
		Statement statement = functions.getConnection().createStatement();
		ResultSet entries = statement.executeQuery(query_select);
		PreparedStatement ps = functions.getConnection().prepareStatement(batchquery);
 
		int n = d1.getSize();
		ConsoleProgressBar cpb = new ConsoleProgressBar(n);
		int nInsert = 0;
		while (entries.next()) {
			
			cpb.tick();
			int i = 1;
			int j = 1;
			for (String s : list) {
				
				if (s.length() == 0) {
					if (types.get(i-1).equals("F") || types.get(i-1).startsWith("F_")) ps.setInt(i, 0); // no value available for this type
					else ps.setString(i, ""); // no value available for this type
					//System.out.println(" value="+"");
				} else if (s.equals("F") || types.get(i-1).startsWith("F_")) {
					ps.setInt(i, entries.getInt(j));
					//System.out.println(" value="+entries.getInt(j));
					j++;
				} else {
					//System.out.println("setString at "+i+" with "+entries.getString(j)+" of "+j);
					ps.setString(i, entries.getString(j));
					//System.out.println(" value="+entries.getString(j));
					j++;
				}
				i++;
			}
			ps.executeUpdate();  
			nInsert++;
		}
		
		statement.close();
		ps.close();
		cpb.done();
		if (nInsert > 0) populated = true;
		return nInsert;
	}

	/**
	 * Load a dictionary from a TSV file, all previously store entries are lost and types are redefined
	 * 
	 * @param tsvFile
	 * @return true if success
	 * 
	 * @throws IOException
	 * @throws SQLException 
	 */
	public boolean loadFromTSVFile(File tsvFile) throws IOException, SQLException {

		if (functions.containsTable(name)) {
			functions.dropTable(name);
		}

		BufferedReader reader = new BufferedReader(new FileReader(tsvFile));

		String l = reader.readLine();
		String[] split = l.split("\t");
		int ncolumn = split.length;
		if (ncolumn == 0) {
			System.out.println("No header found: no column.");
			reader.close();
			return false;
		}

		functions.createTable(name, split);
		types = new ArrayList<String>();
		typesSQL = "";
		for (int i = 0 ; i < ncolumn ; i++) {
			types.add(split[i].trim());
			typesSQL +=",\""+split[i].trim()+"\"";
		}
		if (typesSQL.length() > 0) typesSQL = typesSQL.substring(1);

		int c = 0;
		String line = reader.readLine();
		while (line != null) {line = reader.readLine(); c++;}
		reader.close();

		reader = new BufferedReader(new FileReader(tsvFile));

		System.out.println("Inserting "+c+" lines... "+types.size()+" values per line.");
		String query = "INSERT INTO \""+name+"\" ("+typesSQL+") VALUES ("+questionmarks[types.size()]+")";
		if (debug ) System.out.println(query);
		PreparedStatement ps = functions.getConnection().prepareStatement(query);

		ConsoleProgressBar cpb = new ConsoleProgressBar(c);
		line = reader.readLine(); // header
		line = reader.readLine();
		while (line != null) {
			cpb.tick();
			split = line.split("\t");

			addEntry(ps, split);

			line = reader.readLine();
		}
		
		reader.close();
		cpb.done();
		ps.close();
		functions.getConnection().commit();

		//		Statement stmt = functions.getConnection().createStatement();
		//		stmt.execute("CHECKPOINT ;"); 
		//		stmt.close();
		//		functions.getConnection().commit();
		populated = true;
		return populated;
	}

	/**
	 * Copy this Dictionary table to the 'copyname' table. If the 'copyname' table exists, it is dropped before the copy
	 * 
	 * @param copyname
	 * @return
	 * @throws SQLException
	 */
	public int makeCopy(String copyname) throws SQLException {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return 0;}

		if (functions.containsTable(copyname)) {
			functions.dropTable(copyname);
		}

		String[] stypes = types.toArray(new String[types.size()]);
		functions.createTable(copyname, stypes);

		String query = "INSERT INTO \""+copyname+"\" SELECT * FROM \""+this.name+"\"";
		if (debug) System.out.println(query);
		Statement statement = functions.getConnection().createStatement();
		int n = statement.executeUpdate(query);

		statement.close();
		functions.getConnection().commit();
		return n;
	}

	/**
	 * TODO: NOT DONE
	 * 
	 * @param orderbycolumn sort the resultset with this column values
	 * @param cols
	 * @param typeColumn
	 * @param valueColumn
	 * @return
	 * @throws SQLException
	 */
	public Dictionary mergeByColumn(String orderbycolumn, HashSet<String> cols, String typeColumn,
			String valueColumn) throws SQLException {
		if (!populated) return null;

		String colsSQL = "";
		for (String col : cols) {
			colsSQL += ",\""+col+"\"";
		}
		colsSQL = colsSQL.substring(1);

		// get all new column names
		String query = "SELECT DISTINCT \""+typeColumn+"\" FROM \""+name+"\";";
		Statement statement = functions.getConnection().createStatement();
		ResultSet result = statement.executeQuery(query);
		HashSet<String> values = new HashSet<String>();
		while (result.next()) {
			values.add(typeColumn+"_"+result.getString(1));
		}

		// create a new table with the right columns
		ArrayList<String> newCols = new ArrayList<String>(this.getTypes());
		newCols.remove(typeColumn);
		newCols.remove(valueColumn);
		ArrayList<String> colsToPreserve = new ArrayList<String>(newCols);
		newCols.addAll(values);
		functions.createTable(name+"cpy", newCols);

		// populate the new table

		result = statement.executeQuery("SELECT * FROM \""+name+"\" ORDER BY \""+orderbycolumn+"\";");
		String batchquery = "INSERT INTO \""+name+"\" VALUES ("+questionmarks[newCols.size()]+")";
		//PreparedStatement ps = functions.getConnection().prepareStatement(batchquery);

		HashMap<String, String> newvalues = new HashMap<String, String>(); // contains the new columns values 
		ArrayList<String> currentjoinline = new ArrayList<String>();
		while (result.next()) {
			ArrayList<String> joinline = new ArrayList<String>(); // line signature
			HashMap<String, String> others = new HashMap<String, String>();

			int i = 0;
			String type_column_name= "", type_column_value = "";
			for (String type : colsToPreserve) {
				if (cols.contains(type)) joinline.add(result.getString(type));

				if (type.equals(typeColumn)) type_column_name = result.getString(type);
				else if (type.equals(valueColumn)) type_column_value = result.getString(type);
				else others.put(type, result.getString(type));
			}

			if (!currentjoinline.equals(joinline)) {
				if (currentjoinline.size() > 0) {
					// add the previous line in the new DB
					System.out.println("Add new values: "+newvalues);
				}
				others.put(typeColumn+"_"+type_column_name, type_column_value);
				newvalues = others;

			}
		}

		statement.close();
		//ps.close();
		return this;
	}

	/**
	 * Merge the d dictionary values into this dictionary given a set of columns to insert a aggregateInstructions for columns not grouped
	 * @param d
	 * @param cols
	 * @param agregateInstructions
	 * @return
	 * @throws SQLException
	 */
	public int mergeWith(Dictionary d, List<String> cols, LinkedHashMap<String, String> agregateInstructions) throws SQLException {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return 0;}
		cols = new ArrayList<String>(cols);
		String colsSSQL = "\""+StringUtils.join(cols, "\",\"")+"\"";
		//if (agregateInstructions.size() > 0) colsSSQL += ",\""+StringUtils.join(agregateInstructions.keySet(), "\",\"")+"\"";
		String query = "INSERT INTO \""+this.name+"\" (SELECT "+colsSSQL+" FROM \""+d.name+"\");";
		if (debug) System.out.println(query);

		int n1 = this.getSize();
		Statement statement = functions.getConnection().createStatement();
		statement.executeUpdate(query);

		functions.getConnection().commit(); // release mem

		if (functions.containsTable("txmtmp")) {
			functions.dropTable("txmtmp");
		}

		//ArrayList<String> tcols = new ArrayList<String>(cols);
		//for (String k : agregateInstructions.keySet()) cols.add(k);
		functions.createTable("txmtmp", cols);
		colsSSQL = "";
		String groupByColumns = "";
		for (String col : cols) {
			if (agregateInstructions.containsKey(col)) {
				colsSSQL += ","+agregateInstructions.get(col)+"(\""+col+"\")";
			} else {
				colsSSQL += ",\""+col+"\"";
				groupByColumns += ",\""+col+"\"";
			}
		}
		colsSSQL = colsSSQL.substring(1);
		groupByColumns = groupByColumns.substring(1);
		
		//cols.removeAll(agregateInstructions.keySet()); // remove aggregate columns they are created differently
//		colsSSQL = "\""+StringUtils.join(cols, "\",\"")+"\"";		
		String queryMerge = "INSERT INTO \"txmtmp\" SELECT "+colsSSQL;
//		for (String k : agregateInstructions.keySet()) {
//			queryMerge += ", "+agregateInstructions.get(k)+"(\""+k+"\")";
//		}
		queryMerge +=" FROM \""+name+"\" GROUP BY "+groupByColumns;
//		if (debug) 
			System.out.println(queryMerge);

		int n3 = statement.executeUpdate(queryMerge);
		functions.dropTable(name);
		functions.renameTable("txmtmp", name);

		statement.close();
		functions.getConnection().commit();
		return n3-n1;
	}

	/**
	 * Prints this dictionary into a TSV file
	 * 
	 * @param tsvFile
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */
	public boolean print(File tsvFile) throws SQLException, IOException {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return false;}
		OutputStream out = new FileOutputStream(tsvFile);
		return print(new PrintStream(out, true, "UTF-8"), -1);
	}
	
	/**
	 * Sort and print this dictionary N first lines into a TSV file
	 * 
	 * @param tsvFile
	 * @return
	 * @throws SQLException
	 */
	public boolean print(File tsvFile, int n, String sortColumn,
			String sortLocaleAndStrengh) throws FileNotFoundException, UnsupportedEncodingException, SQLException {
		return print(tsvFile, n, sortColumn, sortLocaleAndStrengh, true);
	}

	/**
	 * Sort using Collator and print this dictionary N first lines into a TSV file
	 * 
	 * @param tsvFile
	 * @return
	 * @throws SQLException
	 */
	public boolean print(File tsvFile, int n, String sortColumn,
			String sortLocaleAndStrengh, boolean writeColumnsHeader) throws FileNotFoundException, UnsupportedEncodingException, SQLException {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return false;}
		OutputStream out = new FileOutputStream(tsvFile);
		return print(new PrintStream(out, true, "UTF-8"), n, sortColumn, sortLocaleAndStrengh, writeColumnsHeader);
	}

	/**
	 * print this dictionary N first lines in console
	 * 
	 * @param N
	 * @return
	 * @throws SQLException
	 */
	public boolean print(int N) throws SQLException {
		return print(System.out, N);
	}

	public boolean print(PrintStream out, int N) throws SQLException {
		return print(out, N, null, null, true);
	}

	public boolean print(PrintStream out, int N, String sortColumn, String sortLocaleAndStrengh, boolean writeColumnsHeader) throws SQLException {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return false;}

		Statement statement = functions.getConnection().createStatement();

		int size = this.getSize();
		System.out.println("Size: "+size);
		ArrayList<String> cols = functions.getTableColumns(name);
		System.out.println("Columns: "+cols);

		String query = "SELECT * FROM \""+name+"\"";
		if (sortColumn != null) {
			query += " ORDER BY \""+sortColumn+"\"";
			if (sortLocaleAndStrengh != null) {
				//query += " COLLATE \""+sortLocaleAndStrengh+"\"";
			}
		}
		query += ";"; // end of query
		if (debug) System.out.println(query);

		ResultSet result = statement.executeQuery(query);
		int Ncol = result.getMetaData().getColumnCount();
		StringBuilder builder = new StringBuilder();
		
		if (writeColumnsHeader) {
			builder.setLength(0);
			for (int t = 0 ; t < Ncol ; t++) {
				if (t != 0) {
					builder.append("\t");
				}
				builder.append(cols.get(t));
			}
			out.println(builder.toString());
		}
		
		//System.out.println("N results: "+result.getFetchSize());
		int i = 0;
		
		if (N == -1) N = size +2;
		while (result.next() && i < N) {
			builder.setLength(0);
			for (int t = 1 ; t <= Ncol ; t++) {
				if (t != 1) {
					builder.append("\t");
				}
				builder.append(result.getObject(t));
			}
			out.println(builder.toString());
			i++;
		}
		statement.close();

		functions.getConnection().commit();
		return true;
	}

	/**
	 * Print randomly lines of this dictionary
	 * 
	 * @param p the probability (between 0 and 1) to select a line
	 * @throws SQLException
	 */
	public void printRandom(float p) throws SQLException {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return;}

		Statement statement = functions.getConnection().createStatement();

		String query = "SELECT * FROM \""+name+"\";";
		if (debug) System.out.println(query);

		ResultSet result = statement.executeQuery(query);

		int N = result.getMetaData().getColumnCount();
		StringBuilder builder = new StringBuilder();
		while (result.next()) {
			if (Math.random() > p) continue;

			builder.setLength(0);
			for (int t = 1 ; t <= N ; t++) {
				if (t == 1) {
					builder.append(result.getObject(t));
				} else {
					builder.append("\t");
					builder.append(result.getObject(t));
				}
			}
			System.out.println(builder.toString());
		}
		statement.close();

		functions.getConnection().commit();
	}

	public int queryAndPrint(String query) throws SQLException {
		return queryAndPrint(new PrintWriter(System.out), query);
	}
	
	/**
	 * Print a dictionary table query result
	 * 
	 * @param query SQL query. attributes and table names must be garded with "
	 * @return the number of matches
	 * @throws SQLException
	 */
	public int queryAndPrint(PrintWriter writer, String query) throws SQLException {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return 0;}

		Statement statement = functions.getConnection().createStatement();

		if (debug) System.out.println(query);

		ResultSet result = statement.executeQuery(query);
		int Ncol = result.getMetaData().getColumnCount();

		int i = 0;
		StringBuilder builder = new StringBuilder();
		while (result.next()) {
			builder.setLength(0);
			for (int t = 1 ; t <= Ncol ; t++) {
				if (t == 1) {
					builder.append(result.getObject(t));
				} else {
					builder.append("\t");
					builder.append(result.getObject(t));
				}
			}
			writer.println(builder.toString());
			i++;
		}
		statement.close();

		functions.getConnection().commit();
		return i;
	}

	/**
	 * Recode a dictionary type into another type (type mat be equals to newType) using rules definiend in the TSV files
     *
	 * @param oldType the type to test
	 * @param newType the type to create or update, may be equals to the oldType
	 * @param conversionTSVFile
	 * @param mode change the conversion behavior when a rule is missing
	 * @param oneMatch if true, only one rule is applied if the value match
	 * @return
	 * @throws Exception
	 */
	public int recodeEntryProperties(String oldType, String newType, File conversionTSVFile, String mode, boolean oneMatch) throws Exception {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return 0;}

		LinkedHashMap<Pattern, Serializable[]> conversionRules = getConversionRulesFromFile(conversionTSVFile);
		return recodeEntryProperties(oldType, newType, conversionRules, mode, oneMatch);
	}

	/**
	 * Use conversion rules to translate entry properties -> update new column
	 * 
	 * @param oldType the type to test
	 * @param newType the type to create or update, may be equals to the oldType
	 * @param conversionRules hash of patterns
	 * @param mode behavior mode when a rule is missing
	 * @param oneMatch if true, stop testing rules if a line have already match a rule
	 * @return
	 * @throws Exception
	 */
	public int recodeEntryProperties(String oldType, String newType, LinkedHashMap<Pattern, Serializable[]> conversionRules, String mode, boolean oneMatch) throws Exception {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return 0;}

		Statement statement = functions.getConnection().createStatement();

		// add the new column if needed
		ArrayList<String> newTypes = new ArrayList<String>(types);
		if (!newTypes.contains(newType)) {
			newTypes.add(newType);
		}
		String newTypesSQL =  "\""+StringUtils.join(newTypes, "\",\"")+"\"";

		if (functions.containsTable("txmtmp")) functions.dropTable("txmtmp");
		functions.createTable("txmtmp", newTypes);

		int iTypePosition = 1 + types.indexOf(newType); // 0 if not a new type, PreparedStatement index starts from 1
		if (iTypePosition == 0) iTypePosition = types.size() + 1;

		String batchquery = "INSERT INTO \"txmtmp\" ("+newTypesSQL+") VALUES ("+questionmarks[newTypes.size()]+")";
		PreparedStatement ps = functions.getConnection().prepareStatement(batchquery);
		if (debug) System.out.println(batchquery); 

		int nCreated = 0;
		ResultSet lines = getLines();
		ConsoleProgressBar cpb = new ConsoleProgressBar(this.getSize());
		int nlines = 0;
		HashSet<String> noMatchValues = new HashSet<String>();
		boolean error = false;
		while (lines.next()) {
			int i = 1;
			cpb.tick();
			for (String type : types) {
				ps.setString(i++, lines.getString(type));
			}

			String value = lines.getString(oldType);
			boolean foundAMatch = false;
			for (Pattern p : conversionRules.keySet()) {

				if (p.matcher(value).matches()) {
					foundAMatch = true;
					// create a line for each conversion rules
					for (Serializable v : conversionRules.get(p)) {
						ps.setString(iTypePosition, v.toString()); // replace params
						ps.executeUpdate();
						nCreated++;
					}
					if (oneMatch) {
						break; // stop at the first match
					}
				} // go to next pattern
			}

			if (!foundAMatch) {
				// just copy the line
				noMatchValues.add(value);
				if (COPYSRC.equals(mode)) {
					ps.setString(iTypePosition, value); // replace params
					ps.executeUpdate();
				} else if (ABANDON.equals(mode)) {
					ps.setString(iTypePosition, "ERROR("+value+")"); // replace params
					ps.executeUpdate();
					error = true;
					break;
				} else if (COPYDEST.equals(mode)) {
					//System.out.println("COPY DEST");
					String valuedest = lines.getString(newType);
					if (valuedest == null) valuedest = "";
					ps.setString(iTypePosition, valuedest); // replace params
					ps.executeUpdate();
				} else { // "supprimer"
					
				}
			}
			nlines++;
		}
		cpb.done();
		System.out.println("Number of lines processed: "+nlines);
		if (noMatchValues.size() > 10000) System.out.println("no match for many values (+10000)");
		else if (noMatchValues.size() > 0) System.out.println("no match for values="+noMatchValues);

		functions.dropTable(name);
		functions.renameTable("txmtmp", name);
		if (!types.contains(newType)) types.add(newType);

		statement.getConnection().commit();
		statement.close();
		ps.close();
		if (error) return -1;
		return nCreated;
	}

	/**
	 * Remove entries matching a pattern for a certain type
	 * 
	 * @param type the type to test
	 * @param pattern regular expression
	 * @return the number of lines removed
	 * @throws SQLException
	 */
	public int removeEntries(String type, String pattern) throws SQLException {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return 0;}

		Statement statement = functions.getConnection().createStatement();

		if (!functions.containsColumn(name, type)) {
			System.out.println("Error: the dictionary does not contains a column named '"+type+"'.");
			return 0;
		}

		String query = "DELETE FROM \""+name+"\" WHERE REGEXP_MATCHES(\""+type+"\", '"+pattern+"');";
		if (debug) System.out.println(query);
		int n = statement.executeUpdate(query);

		statement.close();
		functions.getConnection().commit();
		return n;
	}

	/**
	 * Removes lines entry with 'type' == 'value' AND not alone in the 'jointype' group 
	 * @param jointype the type used to group lines
	 * @param type the type to test
	 * @param value the type value of the line to remove 
	 * @return the number of values removed
	 * @throws Exception
	 */
	public int removeNonAloneLines(String jointype, String type, String value) throws Exception {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return 0;}

		if (functions.containsTable("txmtmp")) functions.dropTable("txmtmp"); // clean

		Statement statement = functions.getConnection().createStatement();

		String cquery = "CREATE TABLE \"txmtmp\" AS (SELECT \""+jointype+"\" FROM \""+name+"\" GROUP BY \""+jointype+"\" HAVING count(*) > 1) WITH DATA";
		if (debug) System.out.println(cquery);
		statement.executeUpdate(cquery);
		
//		System.out.println("txmtmp table is");
//		functions.printTable("txmtmp");
//		
//		System.out.println("select of "+name);
//		int n2 = DictionaryManager.getInstance().queryAndPrint("SELECT * FROM \""+name+"\" WHERE \""+type+"\"='"+value+"';");
//		System.out.println("Result size="+n2);
//		
		String query = "DELETE FROM \""+name+"\" WHERE \""+type+"\"='"+value+"' AND \""+jointype+"\" IN (SELECT * FROM \"txmtmp\");";
		if (debug) System.out.println(query);
		int n = statement.executeUpdate(query);
		statement.close();

		functions.dropTable("txmtmp"); // clean
		return n;
	}
	
	/**
	 * Rename a type and update the SQL table
	 * 
	 * @param oldType
	 * @param newType
	 * @return
	 * @throws SQLException
	 */
	public boolean renameType(String oldType, String newType) throws SQLException {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return false;}

		if (!types.contains(oldType)) return false;

		this.types.remove(oldType);
		this.types.add(newType);
		typesSQL = "\""+StringUtils.join(types, "\",\"")+"\"";

		String query = "ALTER TABLE \""+name+"\" ALTER COLUMN \""+oldType+"\" RENAME TO \""+newType+"\";";
		if (debug) System.out.println(query);
		Statement statement = functions.getConnection().createStatement();
		statement.executeUpdate(query);
		statement.close();

		return true;
	}

	public void setName(String name2) throws SQLException {
		functions.renameTable(name, name2);
		this.name = name2;
	}

	public String toString() {
		return name+" "+typesSQL+" populated="+populated;
	}

	/**
	 * Apply a uniq then a sort transformation to the table and pritn the result in a TSV file
	 * 
	 * @param tsvFile the result file
	 * @param col the column to process
	 * @return
	 * @throws SQLException
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public boolean uniqsort(File tsvFile, String col) throws SQLException, UnsupportedEncodingException, FileNotFoundException {
		if (!populated) {System.out.println("NOT POPULATED: "+name);return false;}

		Statement statement = functions.getConnection().createStatement();
		String query = "SELECT DISTINCT \""+col+"\" FROM \""+name+"\";";
		if (debug) System.out.println(query);

		System.out.println("Execute query...");
		ResultSet result = statement.executeQuery(query);

		System.out.println("Fetching...");
		result.setFetchSize(100);
		HashSet<String> values = new HashSet<String>();
		while (result.next()) {
			values.add(result.getString(1));
		}
		statement.close();

		System.out.println("Sorting...");
		Vector<String> avalues = new Vector<String>(values);
		Collections.sort(avalues, new Comparator<String>() {
			Collator c = Collator.getInstance(Locale.getDefault());

			@Override
			public int compare(String o1, String o2) {
				return c.compare(o1, o2);
			}
		});

		System.out.println("Writting...");
		PrintWriter writer = IOUtils.getWriter(tsvFile, "UTF-8");
		for (String v : avalues) {
			writer.println(v);
		}
		writer.close();

		System.out.println("Result written in "+tsvFile.getAbsoluteFile());
		return true;
	}
}