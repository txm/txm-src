// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-11-23 13:47:58 +0100 (lun., 23 nov. 2015) $
// $LastChangedRevision: 3063 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.dictionary.functions.sql;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.hsqldb.jdbc.JDBCDriver;
import org.txm.utils.DeleteDir;
import org.txm.utils.io.IOUtils;

/**
 * set of SQL functions for HSQL.
 *
 * @author mdecorde
 */
public class HSQLFunctions {

	private static boolean debug = false;
	private static boolean logAll = false;
	public static final File logFile = new File("/tmp/TXM.sql");
	private static PrintWriter logWriter = null;
	public static void setDebug(boolean b) {debug=b;}
	/**
	 * To ref file.
	 *
	 * @param TableName the table name
	 * @param outfile the outfile
	 * @param groupcolumn the groupcolumn
	 * @param argname the argname
	 * @param encoding the encoding
	 * @return the int
	 */
	/*	public int toRefFile(String TableName, String outfile, String groupcolumn,def argname, String encoding)
	{
		assert(argname.size() == 2);
		Sql sql = Sql.newInstance(c)
		LinkedList<String> linkset;
		File f = new File(outfile);
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new PrintStream(new FileOutputStream(f),true,encoding)));
		String content = new File(outfile).getText(encoding);
		int colnum = getColumnNumber(TableName);
		String ref = "";
		String line ="";
		boolean firstline = true;
		HashMap<String,LinkedList<String>> reftable = new HashMap<String,LinkedList<String>>();
		HashMap<String,LinkedList<String>> lemmetable = new HashMap<String,LinkedList<String>>();

		String query = "SELECT "+groupcolumn;
		for (String s : argname)
			query +=","+s
		//System.out.System.out.println(query+" FROM "+TableName)
		sql.eachRow(query+" FROM "+TableName){
			String currentref = it.getAt(0);
			if (!reftable.containsKey(currentref)) {
				linkset = new LinkedHashSet<String>();
				reftable.put(currentref,linkset)
				linkset = new LinkedHashSet<String>();
				lemmetable.put(currentref,linkset)
			}

			linkset = reftable.get(currentref)
			if (!linkset.contains(it.getAt(1))) {
				linkset.add(""+it.getAt(1))
				linkset = lemmetable.get(currentref)
				linkset.add(""+it.getAt(2))
			}
		}
		//System.out.println("ref : "+reftable)
		for (String key : reftable.keySet()) {
			writer.write(key);
			for (int i = 0 ; i < reftable.get(key).size(); i++) {
				String s = (reftable.get(key)).get(i);
				String l = (lemmetable.get(key)).get(i);
				writer.write("\t"+s+"\t"+l);
			}
			writer.write("\n");
		}

		writer.close();
		c.commit();
		return reftable.size();
	}
	 */
	/**
	 * To ref hierarchical file.
	 *
	 * @param TableName the table name
	 * @param outfile the outfile
	 * @param groupcolumn the groupcolumn
	 * @param argname the argname
	 * @param encoding the encoding
	 * @return the int
	 */
	/*	public int toRefHierarchicalFile(String TableName, String outfile, String groupcolumn,def argname, String encoding)
	{
		assert(argname.size() == 2);
		Sql sql = Sql.newInstance(c);
		LinkedList<String> linkset;
		File f = new File(outfile);
		Writer writer = new BufferedWriter(new OutputStreamWriter(new PrintStream(new FileOutputStream(f),true,encoding)));
		String content = new File(outfile).getText(encoding);
		int colnum = getColumnNumber(TableName);
		String ref = "";
		String line ="";
		boolean firstline = true;
		HashMap<String,LinkedList<String>> reftable = new HashMap<String,LinkedList<String>>();
		//HashMap<String,HashMap<String,Integer>> counttable = new HashMap<String,HashMap<String,Integer>>();
		HashMap<String,LinkedList<Integer>> counttable = new HashMap<String,LinkedList<Integer>>();
		HashMap<String,LinkedList<String>> lemmetable = new HashMap<String,LinkedList<String>>();

		String query = "SELECT "+groupcolumn;
		for(String s : argname)
			query +=","+s;
		//System.out.System.out.println(query+" FROM "+TableName)
		sql.eachRow(query+" FROM "+TableName){
			String currentref = it.getAt(0);
			if (!reftable.containsKey(currentref)) {
				linkset = new LinkedHashSet<String>();
				reftable.put(currentref,linkset);
				linkset = new LinkedHashSet<String>();
				lemmetable.put(currentref,linkset);
			}

			linkset = reftable.get(currentref);
			if (!linkset.contains(it.getAt(1))) {
				linkset.add(""+it.getAt(1));
				linkset = lemmetable.get(currentref);
				linkset.add(""+it.getAt(2));
			}


			//if(it.getAt(2) != "-")
			//{
			if(!counttable.containsKey(currentref))
				counttable.put(currentref, new HashMap<String,Integer>());

			HashMap<String,Integer> ht = counttable.get(currentref);
			if(!ht.containsKey(it.getAt(1)))
				ht.put(it.getAt(1),0);

			ht.put(it.getAt(1),ht.get(it.getAt(1)) +1)	

			//}
		}
		//System.out.println("countable : "+counttable)
		for (String key : counttable.keySet()) {
			HashMap<String,Integer> ht = counttable.get(key);
			LinkedList<String> indexorder = new LinkedList<Integer>();

			for(String cat : counttable.get(key).keySet())
			{
				if(indexorder.size() == 0)
				{
					indexorder.add(cat);
				}
				else
				{
					boolean inserted= false;
					for(int j = 0 ; j < indexorder.size();j++)
					{
						String comp = indexorder.get(j);
						if(ht.get(cat) > ht.get(comp))
						{
							indexorder.add(j, cat);
							inserted=true;
							break;
						}
					}
					if(!inserted)
						indexorder.addLast(cat);
				}
			}
			writer.write(key);
			for (String cat : indexorder) {

				int i=0;
				for(int r=0; r < reftable.get(key).size();r++)
					if((reftable.get(key)).get(r) == cat)
					{
						i=r;
						break;
					}
				String s = (reftable.get(key)).get(i);
				String l = (lemmetable.get(key)).get(i);
				writer.write ("\t"+s+"\t"+l);
			}
			writer.write ("\n");
		}

		writer.close();
		c.commit();
		return reftable.size();
	}
	 */
	/**
	 * Clear all.
	 * @param path 
	 */
	public static void clearAll(String path) {
		// HSQL
		new File(path, "db.script").delete();
		new File(path, "db.data").delete();
		new File(path, "db.properties").delete();
		new File(path, "db.log").delete();
		DeleteDir.deleteDirectory(new File(path, "db.tmp"));
		// SQLlite
		new File(path, "db").delete(); 
	}

	Connection c;

	String path;

	//HashMap<String, String[]> tableColumns = new HashMap<String, String[]>();

	public HSQLFunctions(String path) throws SQLException {
		this.path = path;

		File dbDirectory = new File(path, "db");
		String url;
		try {
			url = "jdbc:hsqldb:"+dbDirectory.toURI().toURL().toString();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			url = "jdbc:hsqldb:"+dbDirectory.getAbsolutePath();
		}
		//this.c = DriverManager.getConnection("jdbc:sqlite:file:"+path+"/db", "SA", "");

		Properties infos = new Properties();
		infos.setProperty("user", "SA");
		infos.setProperty("password", "");

		// all http://hsqldb.org/doc/guide/dbproperties-chapt.html
		infos.setProperty("hsqldb.large_data", "true"); // allow 256 billion rows
		infos.setProperty("hsqldb.nio_data_file", "true"); // use of nio access methods for the .data file
		infos.setProperty("hsqldb.nio_max_size", "2048MB"); // nio size limit  
		infos.setProperty("hsqldb.lock_file", "false"); // don't create the .lock file
		infos.setProperty("hsqldb.write_delay", "false"); // write delay performing fsync of log file entries
		infos.setProperty("hsqldb.write_delay_millis", "500"); // write delay performing fsync of log file entries
		infos.setProperty("hsqldb.log_data", "false"); // logging data change or not
		infos.setProperty("hsqldb.sqllog", "0"); // 0 min, 3 max
		infos.setProperty("hsqldb.applog", "0"); // 0 min, 3 max
		infos.setProperty("shutdown", "true"); // shut down the database when the last connection is closed
		infos.setProperty("hsqldb.default_table_type", "CACHED"); // tables are cached 
		infos.setProperty("hsqldb.full_log_replay", "false");
		//String options = ";hsqldb.large_data=true;hsqldb.nio_data_file=true;hsqldb.lock_file=false;hsqldb.write_delay=false;hsqldb.log_data=false;hsqldb.sqllog=0;";
		String options = "";
		this.c = JDBCDriver.getConnection(url.toString()+options, infos);
		//this.c = JDBC.createConnection("jdbc:sqlite:file:"+path+"/db", infos);
		c.setAutoCommit(false);
		Statement stmt = c.createStatement();
		//stmt.execute("SET FILES LOG FALSE;");
		//stmt.execute("SET FILES WRITE DELAY 10 MILLIS ;");
		stmt.close();

		if (logAll) {
			logFile.delete();
			try {
				logWriter = IOUtils.getWriter(logFile);
			} catch (Exception e) {
				System.out.println("** could not start HSQL log: "+e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	public void printQuery(String query) throws SQLException {
		Statement statement = getConnection().createStatement();
		if (debug) System.out.println(query);
		PrintStream out = System.out;
		
		ResultSet result = statement.executeQuery(query);
		int Ncol = result.getMetaData().getColumnCount();
		StringBuilder builder = new StringBuilder();
		
		//System.out.println("N results: "+result.getFetchSize());
		int i = 0;
		while (result.next()) {
			builder.setLength(0);
			for (int t = 1 ; t <= Ncol ; t++) {
				if (t != 1) {
					builder.append("\t");
				}
				builder.append(result.getObject(t));
			}
			out.println(builder.toString());
			i++;
		}
		statement.close();

		getConnection().commit();
	}

	public void clearTable(String name) throws SQLException {
		Statement statement = c.createStatement();
		String query = "DELETE FROM \""+name+"\";";
		if (debug) System.out.println(query);
		statement.executeUpdate(query);
		statement.close();
		c.commit();
	}

	/**
	 * Clear all.
	 * @throws SQLException 
	 */
	public void close() throws SQLException {
		c.close();
	}

	public boolean containsTable(String name) throws SQLException {
		boolean e = false;

		DatabaseMetaData dbm = c.getMetaData();
		ResultSet tables = dbm.getTables(null, null, name, null);
		e = tables.next();
		tables.close();
		return e;		
	}

	/**
	 * Creates the table.
	 *
	 * @param name the name
	 * @param idColumn the column name to create
	 * @throws SQLException 
	 */
	public void createTable(String name, String idColumn) throws SQLException
	{
		Statement statement = c.createStatement();
		String query = "CREATE TABLE \""+name+"\" (\""+idColumn+"\" INTEGER IDENTITY PRIMARY KEY);";

		if (debug) System.out.println(query);
		statement.executeUpdate(query);

		statement.close();
		c.commit();
	}

	/**
	 * Creates the table.
	 *
	 * @param name the name
	 * @param argsnames the argsname
	 * @throws SQLException 
	 */
	public void createTable(String name, List<String> argsnames) throws SQLException {
		String[] argsnames_array = new String[argsnames.size()];
		for (int i = 0 ; i < argsnames.size() ; i++) {
			argsnames_array[i] = argsnames.get(i);
		}
		createTable(name, argsnames_array);
	}

	/**
	 * Creates the table.
	 *
	 * @param name the name
	 * @param argsnames the argsname
	 * @throws SQLException 
	 */
	public void createTable(String name, String argsnames[]) throws SQLException {
		String[] types = new String[argsnames.length];
		for (int i = 0 ; i < argsnames.length ; i++) {
			if ("F".equals(argsnames[i]) || argsnames[i].startsWith("F_")) types[i] = "INT";
			else types[i] = "VARCHAR(100)";
		}
		createTable(name, argsnames, types);
	}

	/**
	 * Creates the table.
	 *
	 * @param name the name
	 * @param argsnames the argsnames
	 * @param types the types
	 * @throws SQLException 
	 */
	public void createTable(String name, String argsnames[], String[] types) throws SQLException {
		assert(argsnames.length == types.length);

		if (containsTable(name)) {
			dropTable(name);
		}
		
		Statement statement = c.createStatement();
		String query = "CREATE TABLE \""+name+"\" ( ";
		for (int i=0; i < types.length ; i++) {
			query += "\""+argsnames[i].trim()+"\" "+types[i];
			if (i < types.length - 1)	{query += ",";}
		}
		query +=")";

		if (debug) System.out.println(query);
		if (logAll) logWriter.println(query);

		statement.executeUpdate(query);
		statement.close();
		c.commit();
		//tableColumns.put(name, argsnames);
	}

	/*	public int ImportOrderedCSVTable(String TableName, List<String> argsname, List<String> types, File csvFile, String separator, String encoding)
	{
		argsname.add("n");
		types.add("INT");
		HSQLFunctions.CreateTable( TableName, argsname, types);
		argsname.remove("n");
		types.remove("INT");
		Statement statement = c.createStatement();
		Reader reader = new InputStreamReader(new FileInputStream(csvFile),encoding);
		int n=0;
		reader.splitEachLine(separator) {fields ->
			n++;
			//System.out.println("fields size "+fields.size())
			String query ="INSERT INTO "+TableName+" (n,";
			for(int i=0;i< argsname.size(); i++)
			{
				query+= argsname[i];
				if(i < argsname.size()-1)	{query += ",";}
			}
			query +=") VALUES ('"+n+"',";
			for(int i=0;i< argsname.size(); i++)
			{
				if( i >= fields.size())
				{
					query+= "''";
				}
				else
				{
					if(fields[i].contains("'"))
						query+= "'"+fields[i].replace("'","''")+"'";
					else
						query+= "'"+fields[i]+"'";
				}
				if(i < argsname.size()-1)	{query += ",";}
			}
			query +=")";

			//System.out.println("query : "+query);
			statement.executeUpdate(query);
		}
		statement.close();
		c.commit();
		return n;
	}

	/**
	 * Import csv table.
	 *
	 * @param TableName the table name
	 * @param argsname the argsname
	 * @param types the types
	 * @param csvFile the csv file
	 * @param separator the separator
	 * @param encoding the encoding
	 */
	/*	public void ImportCSVTable(String TableName, def argsname, def types, File csvFile, String separator, String encoding)
	{
		HSQLFunctions.CreateTable( TableName, argsname, types);

		Statement statement = c.createStatement();
		Reader reader = new InputStreamReader(new FileInputStream(csvFile),encoding);
		reader.splitEachLine(separator) {fields ->
			//System.out.println("fields size "+fields.size())
			String query ="INSERT INTO "+TableName+" (";
			for(int i=0;i< argsname.size; i++)
			{
				query+= argsname[i];
				if(i < argsname.size-1)	{query += ",";}
			}
			query +=") VALUES (";
			for(int i=0;i< argsname.size; i++)
			{
				if( i >= fields.size())
				{
					query+= "''";
				}
				else
				{
					if(fields[i].contains("'"))
						query+= "'"+fields[i].replace("'","''")+"'";
					else
						query+= "'"+fields[i]+"'";
				}
				if(i < argsname.size-1)	{query += ",";}
			}
			query +=")";

			//System.out.println("query : "+query);
			statement.executeUpdate(query);
		}
		statement.close();
		c.commit();
	}
	 */

	public void dropColumn(String name, String column) throws SQLException {
		Statement statement = c.createStatement();
		String query ="ALTER TABLE \""+name+"\" DROP COLUMN \""+column+"\";";

		if (debug) System.out.println(query);
		if (logAll) logWriter.println(query);
		statement.executeUpdate(query);

		statement.close();
		c.commit();

	}

	public void dropTable(String name) throws SQLException {
		Statement statement = c.createStatement();
		String query = "DROP TABLE \""+name+"\";";
		if (debug) System.out.println(query);
		if (logAll) logWriter.println(query);
		statement.executeUpdate(query);
		statement.close();
		c.commit();
	}

	/**
	 * Gets the groovy sql.
	 *
	 * @return the groovy sql
	 */
	/*	public Sql getGroovySql() {
		return Sql.newInstance(c);
	}
	 */

	/**
	 * To cvs file.
	 *
	 * @param TableName the table name
	 * @param outfile the outfile
	 */
	/*	public void toCVSFile(String TableName, String outfile) {
		Sql sql = Sql.newInstance(c);

		FileWriter writer = new FileWriter(outfile);

		int colnum = getColumnNumber(TableName);
		sql.eachRow("select * from "+TableName) {
			for(int i=0;i< colnum; i++)
			{
				//System.out.println('get at '+i)
				writer.write(it.getAt(i));
				if(i < colnum -1)writer.write("\t");
			}
			writer.write("\n");
		}

		writer.close();
		c.commit();
	}
	 */
	// only for sql result with 3 columns

	/**
	 * Execute query.
	 *
	 * @param query the query
	 * @throws SQLException 
	 */
	public void executeQuery(String query) throws SQLException {
		Statement statement = c.createStatement();
		if (debug) System.out.println(query);
		statement.execute(query);
		statement.close();
		c.commit();
	}

	public Connection getConnection() {
		return c;
	}

	/**
	 * Insert csv col.
	 *
	 * @param TableName the table name
	 * @param argsnames the argsnames
	 * @param types the types
	 * @param csvFile the csv file
	 * @param separator the separator
	 * @param encoding the encoding
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public void InsertCsvCol(String TableName, String[] argsnames, String[] types, File csvFile, String separator, String encoding) throws IOException, SQLException
	{
		Statement statement = c.createStatement();
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), encoding));
		String line = reader.readLine();
		//reader.splitEachLine(separator) {fields ->
		while(line != null) {
			String[] fields = line.split(separator);
			//System.out.println("fields size "+fields.size())
			String query ="INSERT INTO "+TableName+" (";
			for(int i=0;i< argsnames.length; i++)
			{
				query+= argsnames[i];
				if(i < argsnames.length-1)	{query += ",";}
			}
			query +=") VALUES (";
			for(int i=0;i< fields.length; i++)
			{
				if(fields[i].contains("'"))
					query+= "'"+fields[i].replace("'","''")+"'";
				else
					query+= "'"+fields[i]+"'";

				if(i < argsnames.length-1)	{query += ",";}
			}
			query +=")";

			//System.out.println("query : "+query);
			statement.executeUpdate(query);
		}
		statement.close();
		c.commit();
		reader.close();
	}

	public void printTable(String tableName) throws SQLException {
		Statement statement = c.createStatement();
		ResultSet result = statement.executeQuery("SELECT * FROM \""+tableName+"\"");
//		String[] cols = tableColumns.get(tableName);
//		if (cols == null) {
//			System.out.println("The table was not created with the 'createTable' method. Aborting.");
//			return;
//		}

		int n = result.getMetaData().getColumnCount();
		
		int i = 0;
		while (result.next()) {
			String line = "";
			for (i = 1 ; i <= n ; i++) line += "\t"+result.getObject(i);
			System.out.println(line.substring(1));
		}

		statement.close();
		c.commit();
	}

	/**
	 * Import ref table.
	 *
	 * @param TableName the table name
	 * @param argsname the argsname
	 * @param types the types
	 * @param csvFile the csv file
	 * @param separator the separator
	 * @param encoding the encoding
	 */
	/*	public void ImportRefTable(String TableName, def argsname, def types, File csvFile, String separator, String encoding)
	{
		HSQLFunctions.CreateTable( TableName, argsname, types);

		Statement statement = c.createStatement();
		Reader reader = new InputStreamReader(new FileInputStream(csvFile),encoding);
		reader.splitEachLine(separator) 
				{fields ->
					String ref;
					if(fields[0].contains("'"))
						ref= "'"+fields[0].replace("'","''")+"'";
					else
						ref= "'"+fields[0]+"'";

					for(int j=1 ; j < fields.size() ; j +=argsname.size -1)
					{
						String query ="INSERT INTO "+TableName+" (";
						for(int i=0;i< argsname.size; i++)
						{
							query+= argsname[i];
							if(i < types.size-1)	{query += ",";}
						}
						query +=") VALUES (";
						query+= ref+",";
						for(int i=j;i < j+argsname.size -1; i++)
						{
							if(fields[i].contains("'"))
								query+= "'"+fields[i].replace("'","''")+"'";
							else
								query+= "'"+fields[i]+"'";

							if(i < j+argsname.size -2)	{query += ",";}
						}
						query +=")";

						//System.out.println("query : "+query);
						statement.executeUpdate(query);
					}
				}
		statement.close();
		c.commit();
	}
	 */
	/**
	 * Test.
	 *
	 * @param table the table
	 * @param file the file
	 * @throws SQLException 
	 */
	public void test(String table, String file) throws SQLException {
		Statement statement = c.createStatement();

		statement.execute("SET TABLE " + table + " SOURCE \"" + file
				+ ";ignore_first=false;all_quoted=false\"");
		statement.close();
		c.commit();
	}

	public final String[] TABLETYPE = {"TABLE"};

	/**
	 * 
	 * @return
	 * @throws SQLException
	 */
	public List<String> getTables() throws SQLException {
		ArrayList<String> tablesList = new ArrayList<String>();
		DatabaseMetaData dbm = c.getMetaData();
		ResultSet tables = dbm.getTables(null, null, null, TABLETYPE);
		while (tables.next()) { // namespace, schema, name, type
			if("TABLE".equals(tables.getString(4))) {
				tablesList.add(tables.getString(3));
			}
		}
		tables.close();
		return tablesList;
	}

	/**
	 * TODO: only works if table is not empty...
	 * 
	 * @param name
	 * @param column_name
	 * @return
	 * @throws SQLException
	 */
	public boolean containsColumn(String name, String column_name) throws SQLException {
		ArrayList<String> cols = getTableColumns(name);
		return cols.contains(column_name);
	}

	public void addColumn(String table_name, String column_name) throws SQLException {
		Statement statement = c.createStatement();
		String query = "ALTER TABLE \""+table_name+"\" ADD \""+column_name+"\" VARCHAR(100) DEFAULT '';";
		if (debug) System.out.println(query);
		if (logAll) logWriter.println(query);
		statement.execute(query);
		statement.close();
		c.commit();
	}

	public ArrayList<String> getTableColumns(String name) throws SQLException {
		ArrayList<String> columns = new ArrayList<String>();

		Statement statement = c.createStatement();
		String query = "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name='"+name+"';";
		if (debug) System.out.println(query);
		if (logAll) logWriter.println(query);
		ResultSet result = statement.executeQuery(query);
		while (result.next()) {
			columns.add(result.getString(1));
		}
		statement.close();
		c.commit();


		return columns;
	}

	/**
	 * Main.
	 *
	 * @param args the args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException
	{
		HSQLFunctions.clearAll("/home/mdecorde/TEMP/dbtest");
		HSQLFunctions hsql = new HSQLFunctions("/home/mdecorde/TEMP/dbtest");


		//	hsql.executeQuery("SELECT col1 from test where col1 LIKE '@'' ESCAPE '@' ")

		String[] argsnames = {"n","col1","col2"};
		String[] types = {"INT","VARCHAR(30)","VARCHAR(30)"};

		if (!hsql.containsTable("test"))
			hsql.createTable( "test", argsnames, types);
		//		hsql.createTable( "test2", argsnames, types);
		hsql.executeQuery("INSERT INTO \"test\" VALUES (1,'poeut','1');");
		hsql.executeQuery("INSERT INTO \"test\" VALUES (2,'poeut','2');");
		hsql.executeQuery("INSERT INTO \"test\" VALUES (3,'poeut','3');");
		hsql.executeQuery("INSERT INTO \"test\" VALUES (4,'poeut','4');");
		hsql.executeQuery("INSERT INTO \"test\" VALUES (5,'poeut','5');");
		hsql.executeQuery("INSERT INTO \"test\" VALUES (5,'poeut','1');");

		//		hsql.executeQuery("INSERT INTO test2 VALUES (11,'poeut',52)");
		//		hsql.executeQuery("INSERT INTO test2 VALUES (22,'poeut',42)");
		//		hsql.executeQuery("INSERT INTO test2 VALUES (33,'poeut',32)");
		//		hsql.executeQuery("INSERT INTO test2 VALUES (44,'poeut',22)");
		//		hsql.executeQuery("INSERT INTO test2 VALUES (45,'poeut',22)");
		//		hsql.executeQuery("INSERT INTO test2 VALUES (55,'poeut',12)");
		//		
		//		hsql.printTable("test");
		//		hsql.printTable("test2");

		//		System.out.println("contains test col1: "+hsql.containsColumn("test", "col1"));
		//		hsql.addColumn("test", "col3");
		//		System.out.println("contains test col3: "+hsql.containsColumn("test", "col3"));

		hsql.getTableColumns("test");

		hsql.close();

		//
		//hsql.CreateTable( "test", argsname, types);
		//	hsql.ImportOrderedCSVTable( "test", argsname, types, new File("~/xml/rgaqcj/lexicon/lex_R"),"\t","UTF-8");
		//hsql.test("test","test.csv");
		//def forms=["hello","pouet","truc"]
		//hsql.CreateSimpleTable("test","form","VARCHAR(20)",forms)
		//hsql.printTable("test");
		//hsql.executeQuery("SELECT col1 from test where col1 LIKE '@'' ESCAPE '@' ");
	}

	public boolean renameTable(String name, String name2) throws SQLException {
		String query = "ALTER TABLE \""+name+"\" RENAME TO \""+name2+"\"";
		if (debug) System.out.println(query);
		if (logAll) logWriter.println(query);
		Statement s = c.createStatement();
		s.executeUpdate(query);
		s.close();
		c.commit();
		return true;
	}
}