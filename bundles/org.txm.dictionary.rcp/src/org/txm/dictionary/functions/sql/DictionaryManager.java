package org.txm.dictionary.functions.sql;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashMap;

import org.txm.Toolbox;

public class DictionaryManager {
	
	File directory;
	HSQLFunctions functions;
	protected static DictionaryManager dm = null;
	
	protected HashMap<String, Dictionary> dictionaries = new HashMap<String, Dictionary>();
	
	public static DictionaryManager getInstance() throws Exception {
		if (dm != null) return dm;
		File dir = new File(Toolbox.getTxmHomePath(), "dictionaries");
		System.out.println("Dictionaries db path: "+dir.getAbsolutePath());
		dir.mkdirs();
		dm = new DictionaryManager(dir);
		return dm;
	}
	
	private DictionaryManager(File directory) throws Exception {
		this.directory = directory;
		directory.mkdirs();
		initializeDB();
	}
	
	public Dictionary getDictionary(String name) throws Exception {
		if (dictionaries.containsKey(name)) return dictionaries.get(name);
		Dictionary d = new Dictionary(name, functions);
		dictionaries.put(name, d);
		if (functions.containsTable(name)) {
			d.addTypes(functions.getTableColumns(name));
			d.populated = true;
		}
		
		return d;
	}

	/**
	 * Initialize the JPA persistence database
	 * @return
	 */
	public boolean initializeDB() throws Exception {
		functions = new HSQLFunctions(directory.getAbsolutePath());
		return isReady();
	}

	public boolean isReady() {
		return functions.getConnection() != null;
	}

	public HSQLFunctions getHSQLFunctions() {
		return functions;
	}

	public boolean hasDictionary(String name) throws SQLException {
		return functions.getTables().contains(name);
	}
	
	public int queryAndPrint(String query) throws SQLException {
		
		Statement statement = functions.getConnection().createStatement();

		int i = 0;
		
		if (query.startsWith("CREATE") || query.startsWith("UPDATE") || query.startsWith("DELETE") || query.startsWith("INSERT")) {
			i = statement.executeUpdate(query);
		} else {
			ResultSet result = statement.executeQuery(query);
			int Ncol = result.getMetaData().getColumnCount();

			
			StringBuilder builder = new StringBuilder();
			while (result.next()) {
				builder.setLength(0);
				for (int t = 1 ; t <= Ncol ; t++) {
					if (t == 1) {
						builder.append(result.getObject(t));
					} else {
						builder.append("\t");
						builder.append(result.getObject(t));
					}
				}
				System.out.println(builder.toString());
				i++;
			}
		}
		
		statement.close();

		functions.getConnection().commit();
		return i;
	}

	public void remove(String name) throws SQLException {
		if (dictionaries.containsKey(name)) {
			dictionaries.remove(name);
		}
		if (functions.containsTable(name)) {
			functions.dropTable(name);
		}
	}

	public boolean rename(Dictionary dict, String newName) throws SQLException {
		if (functions.containsTable(newName)) functions.dropTable(newName);

		this.dictionaries.remove(dict.getName());
		this.dictionaries.put(newName, dict);
		
		dict.setName(newName);
		return true;
	}

	public Collection<Dictionary> getDictionaries() {
		return dictionaries.values();
	}
}
