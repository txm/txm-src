package org.txm.macro.frolex

import java.io.File
import java.io.Serializable
import java.util.ArrayList
import java.util.Arrays
import java.util.HashSet
import java.util.LinkedHashMap
import java.util.List
import java.util.regex.Pattern

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

import org.txm.core.engines.ScriptEnginesManager
import org.txm.Toolbox
import org.txm.dictionary.functions.sql.Dictionary
import org.txm.dictionary.functions.sql.DictionaryManager
import org.txm.dictionary.functions.sql.HSQLFunctions
import org.txm.index.core.functions.Index
import org.txm.searchengine.cqp.corpus.CorpusManager
import org.txm.searchengine.cqp.corpus.MainCorpus
import org.txm.searchengine.cqp.corpus.Property
import org.txm.searchengine.cqp.corpus.query.CQLQuery
import org.txm.utils.DeleteDir
import java.sql.*;

@Field @Option(name="workingDirectory", usage="workingDirectory containing all files needed", widget="Folder", required=true, def="dir")
		File workingDirectory = null
@Field @Option(name="rebuildAFR", usage="Rebuild AFRLEX from afrlex.tsv file", widget="Boolean", required=true, def="true")
		boolean rebuildAFR
@Field @Option(name="rebuildBFM", usage="rebuild BFM", widget="Boolean", required=true, def="true")
		boolean rebuildBFM
@Field @Option(name="rebuildDMF", usage="rebuild DMF", widget="Boolean", required=true, def="true")
		boolean rebuildDMF
@Field @Option(name="rebuildFrolex", usage="rebuild FRO", widget="Boolean", required=true, def="true")
		boolean rebuildFrolex
@Field @Option(name="debug", usage="show SQL queries", widget="Boolean", required=true, def="false")
		boolean debug

if (!ParametersDialog.open(this)) {
	System.out.println("Aborting Frolex creation.")
	return null
}

Dictionary.setDebug(debug)
HSQLFunctions.setDebug(debug)
process(workingDirectory, rebuildAFR, rebuildBFM, rebuildDMF, rebuildFrolex)

Dictionary process(File workingDirectory) {
	return process(workingDirectory, true, true, true, true, true)
}

Dictionary process(File workingDirectory, boolean rebuildAFR, boolean rebuildBFM, boolean rebuildDMF, boolean rebuildFrolex) {
	File resultDirectory = new File(workingDirectory, "result")
	DeleteDir.deleteDirectory(resultDirectory)
	resultDirectory.mkdirs()
	
	MainCorpus bfmCorpus = null
	
	if (Toolbox.isInitialized()) {
		try {
			CorpusManager cm = CorpusManager.getCorpusManager()
			
			bfmCorpus = cm.getCorpus("BFM2016LEX")
			for (MainCorpus corpus : cm.getCorpora()) {
				if (corpus.getID().equals("BFM2016LEX")) bfmCorpus = cm.getCorpus("BFM2016LEX")
			}
		} catch (Exception e) {
			System.out.println("Necessary corpus are missing: "+e)
			e.printStackTrace()
			return null
		}
		
		if (bfmCorpus == null) {
			System.out.println("BFM2016LEX corpus not found. Aborting.")
			return null
		}
	}
	
	try {
		Dictionary afrlex = null
		Dictionary clafrlex = null
		if (rebuildAFR) {
			def ret = buildAFRlex(workingDirectory)
			afrlex = ret[0]
			clafrlex = ret[1]
		} else {
			afrlex = DictionaryManager.getInstance().getDictionary("afrlex")
			clafrlex = DictionaryManager.getInstance().getDictionary("clafrlex")
		}
		if (afrlex == null) {
			System.out.println("Failed to build AFRLEX")
			return null
		}
		
		Dictionary bfmlex = null
		Dictionary clbfmlex = null
		if (rebuildBFM) {
			def ret = buildBFMlex(workingDirectory, bfmCorpus)
			bfmlex = ret[0]
			clbfmlex = ret[1]
		} else {
			bfmlex = DictionaryManager.getInstance().getDictionary("bfmlex")
			clbfmlex = DictionaryManager.getInstance().getDictionary("clbfmlex")
		}
		if (bfmlex == null) {
			System.out.println("Failed to build BFMLEX")
			return null
		}
		
		Dictionary dmflex = null
		Dictionary cldmflex = null
		if (rebuildDMF) {
			def ret = buildDMFlex(workingDirectory)
			dmflex = ret[0]
			cldmflex = ret[1]
		} else {
			dmflex = DictionaryManager.getInstance().getDictionary("dmflex")
			cldmflex = DictionaryManager.getInstance().getDictionary("cldmflex")
		}
		if (dmflex == null) {
			System.out.println("Failed to build DMFLEX & CLDMDF")
			return null
		}
		
		
		Dictionary frolex = null
		if (rebuildFrolex) {
			frolex = buildFROlex(workingDirectory, afrlex, clafrlex, bfmlex, clbfmlex, dmflex, cldmflex)
		} else {
			frolex = DictionaryManager.getInstance().getDictionary("frolex")
		}
		
		if (frolex == null) {
			System.out.println("Failed to build FROLEX")
			return null
		}
	} catch (Exception e) {
		e.printStackTrace()
	}
	return null
}

def buildAFRlex(File workingDirectory) throws Exception {
	System.out.println("** Building AFRlex...")
	DictionaryManager.getInstance().remove("afrlex")
	DictionaryManager.getInstance().remove("clafrlex")
	
	File afrlexSteinFile = new File(workingDirectory, "afrlex.utf.v1.0.txt")
	if (!afrlexSteinFile.exists()) {
		System.out.println("Can't find "+afrlexSteinFile)
		return null
	}
	File perlScript = new File(workingDirectory, "reformat-afrlex.pl")
	if (!perlScript.exists()) {
		System.out.println("Can't find "+perlScript)
		return null
	}
	File convNCA_CTX9 = new File(workingDirectory, "conv nca ctx9-nca.tsv")
	if (!convNCA_CTX9.exists()) {
		System.out.println("Can't find "+convNCA_CTX9)
		return null
	}
	File convNCA_DMF = new File(workingDirectory, "conv nca ctx9-dmf.tsv")
	if (!convNCA_DMF.exists()) {
		System.out.println("Can't find "+convNCA_DMF)
		return null
	}
	File convLemmaSRC = new File(workingDirectory, "conv afr-src frolex-src.tsv")
	if (!convLemmaSRC.exists()) {
		System.out.println("Can't find "+convLemmaSRC)
		return null
	}
	
	
	File afrlexFile = new File(workingDirectory, "afrlex.tsv")
	afrlexFile.delete()
	System.out.println("Recoding original afrlex.txt...")
	def cmd = [perlScript.getAbsolutePath(), afrlexSteinFile.getAbsolutePath(), afrlexFile.getAbsolutePath()]
	def engine = ScriptEnginesManager.getPerlEngine();
	if (!engine.canExecute(perlScript)) {
		println "Error: Perl engine not correctly set to run $perlScript"
		return null;
	}
	def status = engine.runPerl([perlScript.getAbsolutePath(), afrlexSteinFile.getAbsolutePath(), afrlexFile.getAbsolutePath()], null)
	System.out.println(cmd)
	if (status == null) {
		System.out.println("Error: recoding failed. Aborting.")
		return null
	}
	
	if (!afrlexFile.exists()) {
		System.out.println("Error: Can't find "+afrlexFile)
		return null
	}
	
	Dictionary afrlex = DictionaryManager.getInstance().getDictionary("afrlex")
	Dictionary clafrlex = DictionaryManager.getInstance().getDictionary("clafrlex")
	
	System.out.println("Loading data from "+afrlexFile)
	afrlex.loadFromTSVFile(afrlexFile)
	
	// check columns
	List<String> types = afrlex.getTypes()
	String[] colsNeeded = ["form", "msd_afrlex", "lemma", "lemma_src"]
	for (String col : colsNeeded) {
		if (!types.contains(col)) {
			System.out.println("'"+col+"' column is missing in "+types+". Aborting.")
			return null
		}
	}
	
	System.out.println("Recoding msd_afrlex with "+convNCA_CTX9+" in msd_cattex_conv1.")
	int n = afrlex.recodeEntryProperties("msd_afrlex", "msd_cattex_conv1", convNCA_CTX9, "abandon", true)
	if (n == -1) {
		System.out.println("** Conversion failed see errors logs")
		return null
	}
	System.out.println("Done, "+n+" lines recoded.")
	//afrlex.print(5)
	
	System.out.println("Recoding msd_afrlex with "+convNCA_CTX9+" in msd_cattex_conv2.")
	n = afrlex.recodeEntryProperties("msd_afrlex", "msd_cattex_conv2", convNCA_DMF, "abandon", true)
	if (n == -1) {
		System.out.println("** Conversion failed see errors logs")
		return null
	}
	System.out.println("Done, "+n+" lines recoded.")
	//afrlex.print(5)
	
	System.out.println("Recoding 'lemma_src' with "+convLemmaSRC)
	n = afrlex.recodeEntryProperties("lemma_src", "lemma_src", convLemmaSRC, "supprimer", false)
	System.out.println("Done, "+n+" lines recoded.")
	
	// CREATE AFRLEXBULK and select best lemma using source and create the TL,GDF, PKAS, LFA, VFM, PVR and LMG columns
	println "Creating afrlexbulk: building the TL,GDF, PKAS, LFA, VFM, PVR and LMG columns"
	def functions = DictionaryManager.getInstance().getHSQLFunctions()
	if (functions.containsTable("afrlexbulk")) {
		functions.dropTable("afrlexbulk");
	}
	functions.createTable("afrlexbulk",
			["form", "msd_afrlex", "lemma", "lemma_src", "msd_cattex_conv1", "msd_cattex_conv2", "TL", "GDF", "PKAS", "LFA", "VFM", "PVR", "LMG"]);
	def result = afrlex.getOrderedLines("form", "msd_afrlex")
	def current = null
	def msd_cattex = []
	def sources = ["TL":"", "GDF":"", "PKAS":"", "LFA":"", "VFM":"", "PVR":"", "LMG":""]
	String batchquery = "INSERT INTO \"afrlexbulk\" VALUES ("+Dictionary.questionmarks[13]+")";
	PreparedStatement ps = functions.getConnection().prepareStatement(batchquery);
	int nUpdate = 0;
	while (result.next()) {
		def line_key = [result.getObject("form"), result.getObject("msd_afrlex")]
		
		// process the packet with the current line signature is different than the previous ones
		if (current == null) current = line_key
		if (current != line_key || result.isLast()) { // process the line
			// select the right lemma: TL > GDF > PKAS > LFA > VFM > PVR > LMG
			def lemma = sources["TL"]
			def source = "TL"
			def msd_cattex_conv1 = msd_cattex[0]
			def msd_cattex_conv2 = msd_cattex[1]
			lemma = sources["TL"]
			if (lemma.length() == 0) {lemma = sources["GDF"];source = "GDF"}
			if (lemma.length() == 0) {lemma = sources["PKAS"];source = "PKAS"}
			if (lemma.length() == 0) {lemma = sources["LFA"];source = "LFA"}
			if (lemma.length() == 0) {lemma = sources["VFM"];source = "VFM"}
			if (lemma.length() == 0) {lemma = sources["PVR"];source = "PVR"}
			if (lemma.length() == 0) {lemma = sources["LMG"];source = "LMG"}
			//println "INSERT INTO \"txmtmp\" values ('"+current[0]+"', '"+current[1]+"', '"+lemma+"', '"+source+"', '"+sources["TL"]+"', '"+sources["GDF"]+"', '"+sources["PKAS"]+"', '"+sources["LFA"]+"', '"+sources["VFM"]+"', '"+sources["PVR"]+"', '"+sources["LMG"]+"')"
			ps.setString(1, current[0]);
			ps.setString(2, current[1]);
			ps.setString(3, lemma);
			ps.setString(4, source);
			ps.setString(5, msd_cattex_conv1);
			ps.setString(6, msd_cattex_conv2);
			ps.setString(7, sources["TL"]);
			ps.setString(8, sources["GDF"]);
			ps.setString(9, sources["PKAS"]);
			ps.setString(10, sources["LFA"]);
			ps.setString(11, sources["VFM"]);
			ps.setString(12, sources["PVR"]);
			ps.setString(13, sources["LMG"]);
			
			ps.executeUpdate();
			nUpdate++
			if (nUpdate%1000 == 0) ps.getConnection().commit()
			
			// set the new packet signature
			sources = ["TL":"", "GDF":"", "PKAS":"", "LFA":"", "VFM":"", "PVR":"", "LMG":""]
			msd_cattex = [result.getObject("msd_cattex_conv1"), result.getObject("msd_cattex_conv2")]
			current = line_key
		}
		
		//store the lemma + lemma_src
		sources[result.getObject("lemma_src")] = result.getObject("lemma")
	}
	ps.close()
	result.getStatement().close();
	System.out.println("AFRlexBULK created.")
	
	// Clear&Update AFRLEX with the new columns
	afrlex.clear()
	println afrlex.getTypes()
	afrlex.insertValues("SELECT \"form\",\"msd_afrlex\",\"lemma\",\"lemma_src\",\"msd_cattex_conv1\",\"msd_cattex_conv2\" FROM \"afrlexbulk\";")
	afrlex.print(10)
	
	// Build CLAFRLEX & merge corresp columns if necessary
	println "Build CLAFRLEX using AFRLEXBULK"
	clafrlex.addTypes(["msd_afrlex", "lemma", "lemma_src", "msd_cattex_conv1", "msd_cattex_conv2", "TL", "GDF", "PKAS", "LFA", "VFM", "PVR", "LMG"])
	Statement statement = functions.getConnection().createStatement();
	String query = "SELECT * FROM \"afrlexbulk\" ORDER BY \""+["msd_afrlex", "lemma", "lemma_src", "msd_cattex_conv1", "msd_cattex_conv2", "TL", "GDF", "PKAS", "LFA", "VFM", "PVR", "LMG"].join("\",\"")+"\";";
	result = statement.executeQuery(query);
	batchquery = "INSERT INTO \"clafrlex\" VALUES ("+Dictionary.questionmarks[12]+")";
	ps = functions.getConnection().prepareStatement(batchquery);
	current = null
	sources = ["TL":new HashSet(), "GDF":new HashSet(), "PKAS":new HashSet(), "LFA":new HashSet(), "VFM":new HashSet(), "PVR":new HashSet(), "LMG":new HashSet()]
	others = ["lemma_src":"", "msd_cattex_conv1":"", "msd_cattex_conv2": ""]
	while (result.next()) {
		def line_key = [result.getObject("msd_afrlex"), result.getObject("lemma")]
		
		// process the packet with the current line signature is different than the previous ones
		if (current == null) current = line_key
		if (current != line_key || result.isLast()) { // process the line
			
			ps.setString(1, current[0]); // msd_afrlex
			ps.setString(2, current[1]); // lemma
			ps.setString(3, others["lemma_src"]); // lemma_src
			ps.setString(4, others["msd_cattex_conv1"]); // msd_cattex_conv1
			ps.setString(5, others["msd_cattex_conv2"]); // msd_cattex_conv2
			ps.setString(6, sources["TL"].join("|"));
			ps.setString(7, sources["GDF"].join("|"));
			ps.setString(8, sources["PKAS"].join("|"));
			ps.setString(9, sources["LFA"].join("|"));
			ps.setString(10, sources["VFM"].join("|"));
			ps.setString(11, sources["PVR"].join("|"));
			ps.setString(12, sources["LMG"].join("|"));
			
			ps.executeUpdate();
			
			// set the new packet signature
			sources = ["TL":new HashSet(), "GDF":new HashSet(), "PKAS":new HashSet(), "LFA":new HashSet(), "VFM":new HashSet(), "PVR":new HashSet(), "LMG":new HashSet()]
			others = ["lemma_src":"", "msd_cattex_conv1":"", "msd_cattex_conv2": ""]
			current = line_key
		}
		
		//store the lemma + lemma_src
		others = ["lemma_src":result.getObject("lemma_src"), "msd_cattex_conv1":result.getObject("msd_cattex_conv1"), "msd_cattex_conv2":result.getObject("msd_cattex_conv2")]
		for (String key : sources.keySet()) {
			if (result.getObject(key).length() > 0) sources[key] << result.getObject(key);
		}
	}
	ps.close()
	statement.close()
	clafrlex.setPopulated(true) // set the dict as populated
	clafrlex.print(10)
	
	// write in files
	File tsvFile = new File(workingDirectory, "result/afrlex.tsv")
	File tsvFile2 = new File(workingDirectory, "result/clafrlex.tsv")
	if (afrlex.print(tsvFile, -1, "form", "FRENCH 3") && clafrlex.print(tsvFile2, -1, "lemma", "FRENCH 3")) {
		System.out.println("afrlex exported in "+tsvFile.getAbsolutePath())
		System.out.println("clafrlex exported in "+tsvFile2.getAbsolutePath())
		return [afrlex, clafrlex]
	} else {
		System.out.println("Fail to export afrlex.")
		return null
	}
}

def buildDMFlex(File workingDirectory) {
	System.out.println("** Building CLDMFlex and DMFlex...")
	DictionaryManager.getInstance().remove("dmflex")
	DictionaryManager.getInstance().remove("cldmflex")
	
	File dmflexFile = new File(workingDirectory, "dmf.tsv")
	if (!dmflexFile.exists()) {
		System.out.println("Can't find "+dmflexFile)
		return null
	}
	File cldmflexFile = new File(workingDirectory, "cldmf.tsv")
	if (!cldmflexFile.exists()) {
		System.out.println("Can't find "+cldmflexFile)
		return null
	}
	File convDMFCTX9 = new File(workingDirectory, "conv dmf ctx9-dmf.tsv")
	if (!convDMFCTX9.exists()) {
		System.out.println("Can't find "+convDMFCTX9)
		return null
	}
	File convLemmaSRC = new File(workingDirectory, "conv dmf-src frolex-src.tsv")
	if (!convLemmaSRC.exists()) {
		System.out.println("Can't find "+convLemmaSRC)
		return null
	}
	
	Dictionary cldmflex = DictionaryManager.getInstance().getDictionary("cldmflex")
	System.out.println("Loading data from "+cldmflexFile)
	cldmflex.loadFromTSVFile(cldmflexFile)
	// check columns
	List<String> types = cldmflex.getTypes()
	String[] colsNeeded = ["lemma", "category", "lemma_source",
		"DEAF", "DECT", "FEW", "GDF", "GDC", "HUG", "TL", "TLF"]
	for (String col : colsNeeded) {
		if (!types.contains(col)) {
			System.out.println("'"+col+"' column is missing in "+types+". Aborting.")
			return null
		}
	}
	cldmflex.renameType("lemma_source", "lemma_src")
	cldmflex.renameType("category", "msd_cattex_conv2")
	
	System.out.println("Recoding data with "+convDMFCTX9)
	int n = cldmflex.recodeEntryProperties("msd_cattex_conv2", "msd_cattex_conv2", convDMFCTX9, "abandon", true)
	if (n == -1) {
		System.out.println("** Conversion failed see errors logs")
		return null
	}
	System.out.println("Done, "+n+" lines recoded.")
	
	
	Dictionary dmflex = DictionaryManager.getInstance().getDictionary("dmflex")
	
	System.out.println("Loading data from "+dmflexFile)
	dmflex.loadFromTSVFile(dmflexFile)
	
	// check columns
	List<String> types2 = dmflex.getTypes()
	String[] colsNeeded2 = ["form", "category", "lemma", "F_dmf"]
	for (String col : colsNeeded2) {
		if (!types2.contains(col)) {
			System.out.println("'"+col+"' column is missing in "+types2+". Aborting.")
			return null
		}
	}
	
	System.out.println("Recoding data with "+convDMFCTX9)
	n = dmflex.recodeEntryProperties("category", "msd_cattex_conv2", convDMFCTX9, "abandon", true)
	if (n == -1) {
		System.out.println("** Conversion failed see errors logs")
		return null
	}
	System.out.println("Done, "+n+" lines recoded.")
	
	System.out.println("DMFlex created: "+dmflex.getSize()+" entries.")
	dmflex.print(5)
	
	DictionaryManager.getInstance().getHSQLFunctions().getConnection().commit()
	
	File tsvFile = new File(workingDirectory, "result/dmflex.tsv")
	if (dmflex.print(tsvFile, -1, "form", "FRENCH 3")) {
		System.out.println("dmflex exported in "+tsvFile.getAbsolutePath())
		return [dmflex, cldmflex]
	} else {
		System.out.println("Fail to export dmflex.")
		return null
	}
}

def buildBFMlex(File workingDirectory, MainCorpus bfmCorpus) {
	def functions = DictionaryManager.getInstance().getHSQLFunctions()
	System.out.println("** Building BFMlex...")
	DictionaryManager.getInstance().remove("bfmlex")
	DictionaryManager.getInstance().remove("clbfmlex")
	
	File convCTX9NCA = new File(workingDirectory, "conv ctx9 ctx9-nca.tsv")
	if (!convCTX9NCA.exists()) {
		System.out.println("Can't find "+convCTX9NCA)
		return null
	}
	
	File convCTX9DMF = new File(workingDirectory, "conv ctx9 ctx9-dmf.tsv")
	if (!convCTX9DMF.exists()) {
		System.out.println("Can't find "+convCTX9DMF)
		return null
	}
	
	File convPunctLemma = new File(workingDirectory, "conv punct lemma.tsv")
	if (!convPunctLemma.exists()) {
		System.out.println("Can't find "+convPunctLemma)
		return null
	}
	
	File convNumLemma = new File(workingDirectory, "conv num lemma.tsv")
	if (!convNumLemma.exists()) {
		System.out.println("Can't find "+convNumLemma)
		return null
	}
	
	File outfile = new File(workingDirectory, "bfmlex.tsv")
	
	if (Toolbox.isInitialized()) {
		System.out.println("Creating Index of [pos!=\"pon\"] with corpus BFM2016LEX")
		ArrayList<Property> props = new ArrayList<Property>()
		Property posProperty = bfmCorpus.getProperty("pos")
		if (posProperty == null) {
			System.out.println("No 'pos' property in BFM2016LEX corpus. Aborting.")
			return null
		}
		Property lemmaProperty = bfmCorpus.getProperty("lemma")
		if (lemmaProperty == null) {
			System.out.println("No 'lemma' property in BFM2016LEX corpus. Aborting.")
			return null
		}
		Property lemmaSrcProperty = bfmCorpus.getProperty("lemma_src")
		if (lemmaSrcProperty == null) {
			System.out.println("No 'lemma_src' property in BFM2016LEX corpus. Aborting.")
			return null
		}
		props.add(bfmCorpus.getProperty("word"))
		props.add(posProperty)
		props.add(lemmaProperty)
		props.add(lemmaSrcProperty)
		Index index = new Index(bfmCorpus, new CQLQuery("[pos!=\"pon\"]"), props)
		
		index.toTSVDictionnary(outfile, "\t", "UTF-8")
	}
	
	Dictionary bfmlex = DictionaryManager.getInstance().getDictionary("bfmlex")
	Dictionary clbfmlex = DictionaryManager.getInstance().getDictionary("clbfmlex")
	
	if (!outfile.exists()) {
		System.out.println("Error: BFM lex file not created: "+outfile)
		return null
	}
	
	System.out.println("Loading BFMLEX data from "+outfile)
	bfmlex.loadFromTSVFile(outfile)
	
	// check columns
	List<String> types = bfmlex.getTypes()
	String[] colsNeeded = ["word", "pos", "lemma", "lemma_src", "F"]
	for (String col : colsNeeded) {
		if (!types.contains(col)) {
			System.out.println("'"+col+"' column is missing in "+types+". Aborting.")
			return null
		}
	}
	
	System.out.println("Converting 'NA' & 'num' to '<no_pos>'")
	LinkedHashMap<Pattern, Serializable[]> conversion_rules = new LinkedHashMap<Pattern, Serializable[]>()
	String[] values = ["<no_pos>"]
	conversion_rules.put(Pattern.compile("NA|num|"), values)
	int n = bfmlex.recodeEntryProperties("pos", "pos", conversion_rules, "copier", true) // keep the other lines
	System.out.println("Done, "+n+" lines recoded.")
	
	System.out.println("Set punctuation lemma")
	n = bfmlex.recodeEntryProperties("word", "lemma", convPunctLemma, Dictionary.COPYDEST, true)
	if (n == -1) {
		System.out.println("** Conversion failed see errors logs")
		return null
	}
	System.out.println("Done, "+n+" lines recoded.")
	
	System.out.println("Set num lemma")
	n = bfmlex.recodeEntryProperties("word", "lemma", convNumLemma, Dictionary.COPYDEST, true) // copy previously  created lemma
	System.out.println("Done, "+n+" lines recoded.")
	
	System.out.println("Converting '' lemma to '<no_lemma>' lemma")
	LinkedHashMap<Pattern, Serializable[]> conversion_rules2 = new LinkedHashMap<Pattern, Serializable[]>()
	String[] values2 = ["<no_lemma>"]
	conversion_rules2.put(Pattern.compile(""), values2)
	int n2 = bfmlex.recodeEntryProperties("lemma", "lemma", conversion_rules2, "copier", true) // copy previously  created lemma
	System.out.println("Done, "+n2+" lines recoded.")
	
	HashSet<String> wrongMSD = new HashSet<String>()
	wrongMSD.add("PONbfl")
	wrongMSD.add("PONpfbl")
	wrongMSD.add("PONfbfl")
	n = 0
	System.out.println("Removing lines with errors: "+wrongMSD)
	for (String p : wrongMSD) {
		n += bfmlex.removeEntries("pos", p)
	}
	if (n > 0) System.out.println("Done, "+n+" lines removed.")
	
	System.out.println("Remove non-alone '<no_pos>' lines.")
	n = bfmlex.removeNonAloneLines("word", "pos", "<no_pos>")
	System.out.println("Number of lines removed: "+n)
	System.out.println("  left:"+bfmlex.getSize())
	
	System.out.println("Creating msd_cattex_conv1 using "+convCTX9NCA)
	n = bfmlex.recodeEntryProperties("pos", "msd_cattex_conv1", convCTX9NCA, "copier", true)
	System.out.println("Done, "+n+" lines recoded.")
	
	System.out.println("Creating msd_cattex_conv2 using "+convCTX9DMF)
	n = bfmlex.recodeEntryProperties("pos", "msd_cattex_conv2", convCTX9DMF, "copier", true)
	System.out.println("Done, "+n+" lines recoded.")
	
	System.out.println("Renaming columns...")
	bfmlex.renameType("word", "form")
	bfmlex.renameType("pos", "msd_cattex")
	bfmlex.renameType("F", "F_bfm")
	
	System.out.println("BFMlex created: "+bfmlex.getSize()+" entries.")
	bfmlex.print(10)
	
	DictionaryManager.getInstance().getHSQLFunctions().getConnection().commit()
	
	// Build CLBFMLEX & merge corresp columns if necessary
	println "Build CLBFMLEX using BFMLEX"
	clbfmlex.addTypes(["msd_cattex", "msd_cattex_conv1", "msd_cattex_conv2", "lemma", "lemma_src", "DMF", "DECT", "TL", "BFM", "GDF", "AND"])
	Statement statement = functions.getConnection().createStatement();
	String query = "SELECT * FROM \"bfmlex\" ORDER BY \""+["msd_cattex", "lemma"].join("\",\"")+"\";";
	result = statement.executeQuery(query);
	batchquery = "INSERT INTO \"clbfmlex\" VALUES ("+Dictionary.questionmarks[11]+")";
	ps = functions.getConnection().prepareStatement(batchquery);
	current = null
	sources = ["DMF":new HashSet(), "DECT":new HashSet(), "TL":new HashSet(), "BFM":new HashSet(), "GDF":new HashSet(), "AND":new HashSet()]
	others = ["msd_cattex_conv1":"", "msd_cattex_conv2": ""]
	while (result.next()) {
		def line_key = [result.getObject("msd_cattex"), result.getObject("lemma")]
		
		// process the packet with the current line signature is different than the previous ones
		if (current == null) current = line_key
		if (current != line_key || result.isLast()) { // process the line
			
			String lemma_src = "";
			if (sources["BFM"].size() > 0) lemma_src = "BFM"
			else if (sources["DMF"].size() > 0) lemma_src = "DMF"
			else if (sources["TL"].size() > 0) lemma_src = "TL"
			else if (sources["DECT"].size() > 0) lemma_src = "DECT"
			else if (sources["GDF"].size() > 0) lemma_src = "GDF"
			else if (sources["AND"].size() > 0) lemma_src = "AND"
			
			ps.setString(1, current[0]); // msd_cattex
			ps.setString(2, others["msd_cattex_conv1"]);
			ps.setString(3, others["msd_cattex_conv2"]);
			ps.setString(4, current[1]); // lemma
			ps.setString(5, lemma_src); // lemma_src
			ps.setString(6, sources["DMF"].join("|"));
			ps.setString(7, sources["DECT"].join("|"));
			ps.setString(8, sources["TL"].join("|"));
			ps.setString(9, sources["BFM"].join("|"));
			ps.setString(10, sources["GDF"].join("|"));
			ps.setString(11, sources["AND"].join("|"));
			
			ps.executeUpdate();
			
			// set the new packet signature
			sources = ["DMF":new HashSet(), "DECT":new HashSet(), "TL":new HashSet(), "BFM":new HashSet(), "GDF":new HashSet(), "AND":new HashSet()]
			others = ["msd_cattex_conv1":"", "msd_cattex_conv2": ""]
			current = line_key
		}
		
		//store the lemma + lemma_src
		others = ["msd_cattex_conv1":result.getObject("msd_cattex_conv1"), "msd_cattex_conv2":result.getObject("msd_cattex_conv2")]
		String src = result.getObject("lemma_src");
		if (sources.containsKey(src)) {
			sources[src] << result.getObject("lemma");
		}
	}
	ps.close()
	statement.close()
	clbfmlex.setPopulated(true) // set the dict as populated
	clbfmlex.print(10)
	
	File tsvFile = new File(workingDirectory, "result/bfmlex.tsv")
	File tsvFile2 = new File(workingDirectory, "result/clbfmlex.tsv")
	if (bfmlex.print(tsvFile, -1, "form", "FRENCH 3")
			&& clbfmlex.print(tsvFile2, -1, "lemma", "FRENCH 3")) {
		System.out.println("bfmlex exported in "+tsvFile.getAbsolutePath())
		return [bfmlex, clbfmlex]
	} else {
		System.out.println("Fail to export bfmlex.")
		return null
	}
}

Dictionary buildFROlex(File workingDirectory, Dictionary afrlex, Dictionary clafrlex, Dictionary bfmlex, Dictionary clbfmlex, Dictionary dmflex, Dictionary cldmflex) {
	System.out.println("** Building FROlex...")
	def functions = DictionaryManager.getInstance().getHSQLFunctions()
	File convCTX9NCA = new File(workingDirectory, "conv nca ctx9-dmf.tsv")
	if (!convCTX9NCA.exists()) {
		System.out.println("Can't find "+convCTX9NCA)
		return null
	}
	
	// Added AL
	
	File convLemmaSRC2 = new File(workingDirectory, "merge frolex-src.tsv")
	if (!convLemmaSRC2.exists()) {
		System.out.println("Can't find "+convLemmaSRC2)
		return null
	}
	
	
	DictionaryManager.getInstance().remove("frolex")
	DictionaryManager.getInstance().remove("clfrolex")
	
	Dictionary frolex = DictionaryManager.getInstance().getDictionary("frolex")
	Dictionary clfrolex = DictionaryManager.getInstance().getDictionary("clfrolex")
	
	//CLFROLEX
	System.out.println("clfrolex types: "+clfrolex.getTypes())
	clfrolex.addTypes(              Arrays.asList("msd_cattex_conv2",	"lemma",	"lemma_src",	"AND",	"TL",	"DEAF",	"DECT",	"FEW",	"GDF",	"GDC",	"HUG", "TLF", "BFM", "PKAS", "LFA", "VFM", "PVR", "LMG", "DMF"))
	System.out.println("clfrolex types: "+clfrolex.getTypes())
	println "Insert values of clbfmlex"
	clfrolex.insertValues(clbfmlex, Arrays.asList("msd_cattex_conv2",	"lemma",	"lemma_src",	"AND",	"TL",	"",	"DECT",	"",	"GDF",	"",	"", "", "BFM", "", "", "", "", "", "DMF"))
	println "Insert values of clafrlex"
	clfrolex.insertValues(clafrlex, Arrays.asList("msd_cattex_conv2",	"lemma",	"lemma_src",	"",	"TL",	"",	"",	"",	"GDF",	"",	"", "", "", "PKAS", "LFA", "VFM", "PVR", "LMG", ""))
	println "Insert values of cldmflex"
	clfrolex.insertValues(cldmflex, Arrays.asList("msd_cattex_conv2",	"lemma",	"lemma_src",	"AND",	"TL",	"DEAF",	"DECT",	"FEW",	"GDF",	"GDC",	"HUG", "TLF", "", "", "", "", "", "", ""))
	
	System.out.println("Merging lines with lemma and msd_cattex_conv2.")
	
	Statement clstatement = functions.getConnection().createStatement();
	String clquery = "SELECT * FROM \"clfrolex\" ORDER BY \""+["lemma", "msd_cattex_conv2"].join("\",\"")+"\";";
	result = clstatement.executeQuery(clquery);
	if (functions.containsTable("tmp")) functions.dropTable("tmp") // drop table
	def clcolumns = ["msd_cattex_conv2", "lemma","lemma_src" ,
		"AND","TL","DEAF","DECT","FEW","GDF","GDC","HUG","TLF","BFM","PKAS","LFA","VFM","PVR","LMG","DMF"]
	def othersclcolumns = ["lemma_src" ,
		"AND","TL","DEAF","DECT","FEW","GDF","GDC","HUG","TLF","BFM","PKAS","LFA","VFM","PVR","LMG","DMF"]
	functions.createTable("tmp", clcolumns)
	
	int countCLLinesBefore = clfrolex.getSize();
	
	batchquery = "INSERT INTO \"tmp\" VALUES ("+Dictionary.questionmarks[19]+")";
	ps = functions.getConnection().prepareStatement(batchquery);
	current = null
	others = [:]
	for (def col : othersclcolumns) others[col] = new HashSet();
	while (result.next()) {
		def line_key = [result.getObject("lemma"), result.getObject("msd_cattex_conv2")]
		
		// process the packet with the current line signature is different than the previous ones
		if (current == null) current = line_key
		if (current != line_key || result.isLast()) { // process the line
			
			//println " merging: $line_key with $others"
			
			ps.setString(1, current[1]); // msd_cattex_conv2
			ps.setString(2, current[0]); // lemma
			int icol = 1;
			for (def col : othersclcolumns) {
				if (others[col].size() > 1 ) others[col].removeAll("");
				
				ps.setString(2+icol, others[col].join("|"));
				icol++
			}
			
			ps.executeUpdate();
			
			// set the new packet signature
			for (def col : othersclcolumns) others[col] = new HashSet();
			current = line_key
		}
		
		// store other columns values
		for (String k : others.keySet()) {
			others[k] << result.getObject(k);
		}
	}
	ps.close()
	clstatement.close()
	functions.dropTable("clfrolex")
	functions.renameTable("tmp", "clfrolex")
	
	int countCLLinesAfter = clfrolex.getSize();
	println "Number of lines merged: "+(countCLLinesBefore-countCLLinesAfter)
	
	System.out.println("Done clfrolex size:"+clfrolex.getSize()+"\n")
	// FROLEX
	System.out.println("frolex types: "+frolex.getTypes())
	frolex.addTypes(            Arrays.asList("form",	"F_bfm",	"F_dmf",	"msd_afrlex",	"msd_bfm",	"msd_dmf",	"msd_cattex_conv1",	"msd_cattex_conv2",	"lemma",	"lemma_src",	"comment"))
	System.out.println("frolex types: "+frolex.getTypes())
	println "Insert values of bfmlex"
	frolex.insertValues(bfmlex, Arrays.asList("form",	"F_bfm",	"",			"",				"msd_cattex","",		"msd_cattex_conv1","msd_cattex_conv2", "lemma",    "lemma_src",    ""))
	System.out.println("Done frolex size:"+frolex.getSize())
	println "Insert values of afrlex"
	frolex.insertValues(afrlex, Arrays.asList("form", 	"",			"",			"msd_afrlex", 	"",   		"",			"msd_cattex_conv1","msd_cattex_conv2", "lemma",	"lemma_src", 	""))
	System.out.println("Done frolex size:"+frolex.getSize())
	println "Insert values of dmflex"
	frolex.insertValues(dmflex, Arrays.asList("form", 	"",			"F_dmf",	"",				"",			"category",	"","msd_cattex_conv2", "lemma", 	"lemma_src", 	""))
	System.out.println("Done frolex size:"+frolex.getSize()+"\n")
	
	System.out.println("Merging lines with form and msd_cattex_conv.")
	
	Statement statement = functions.getConnection().createStatement();
	String query = "SELECT * FROM \"frolex\" ORDER BY \""+["form", "msd_cattex_conv2"].join("\",\"")+"\";";
	result = statement.executeQuery(query);
	if (functions.containsTable("tmp")) functions.dropTable("tmp") // drop table
	functions.createTable("tmp", ["form","F_bfm","F_dmf","msd_afrlex","msd_bfm","msd_dmf",
		"msd_cattex_conv1","msd_cattex_conv2","lemma","lemma_src","comment"])
	batchquery = "INSERT INTO \"tmp\" VALUES ("+Dictionary.questionmarks[11]+")";
	ps = functions.getConnection().prepareStatement(batchquery);
	current = null
	others = ["F_bfm":[0], "F_dmf": [0], "msd_afrlex":new HashSet(), "msd_bfm":new HashSet(), "msd_dmf":new HashSet(), "msd_cattex_conv1":new HashSet(), "lemma":new HashSet(), "lemma_src":new HashSet(), "comment":new HashSet()]
	while (result.next()) {
		def line_key = [result.getObject("form"), result.getObject("msd_cattex_conv2")]
		// process the packet with the current line signature is different than the previous ones
		if (current == null) current = line_key
		
		if (current != line_key || result.isLast()) { // process the line
			
			
			
			for (String k : others.keySet()) if (others[k].size() > 1 && !k.startsWith("F_")) others[k].removeAll("");
			
			ps.setString(1, current[0]); // form
			ps.setInt(2, others["F_bfm"].sum()); // F_bfm
			ps.setInt(3, others["F_dmf"].sum()); // F_dmf
			ps.setString(4, others["msd_afrlex"].join("|")); // msd_afrlex
			ps.setString(5, others["msd_bfm"].join("|")); // msd_bfm
			ps.setString(6, others["msd_dmf"].join("|")); // msd_dmf
			if (others["msd_cattex_conv1"].size() > 1) others["msd_cattex_conv1"].removeAll("<no_pos>")
			ps.setString(7, others["msd_cattex_conv1"].join("|")); // msd_cattex_conv1
			ps.setString(8, current[1]); // msd_cattex_conv2
			if (others["lemma"].size() > 1) others["lemma"].removeAll("<no_lemma>")
			ps.setString(9, others["lemma"].join("|")); // lemma
			ps.setString(10, others["lemma_src"].join("|")); // lemma_src
			ps.setString(11, others["comment"].join("|")); // comment
			
			ps.executeUpdate();
			
			// set the new packet signature
			others = ["F_bfm":[0], "F_dmf": [0], "msd_afrlex":new HashSet(), "msd_bfm":new HashSet(), "msd_dmf":new HashSet(), "msd_cattex_conv1":new HashSet(), "lemma":new HashSet(), "lemma_src":new HashSet(), "comment":new HashSet()]
			current = line_key
		}
		
		//store the lemma + lemma_src
		for (String k : others.keySet()) {
			others[k] << result.getObject(k);
		}
	}
	ps.close()
	statement.close()
	functions.dropTable("frolex")
	functions.renameTable("tmp", "frolex")
	System.out.println("Done frolex size:"+frolex.getSize()+"\n")
	
	System.out.println("Remove non-alone msd_cattex_conv2='OUT' lines.")
	int n = frolex.removeNonAloneLines("form", "msd_cattex_conv2", "OUT")
	System.out.println("Number of lines removed: "+n+"  left:"+frolex.getSize()+"\n")
	
	System.out.println("Remove non-alone msd_cattex_conv2='<no_pos>' lines.")
	int n2 = frolex.removeNonAloneLines("form", "msd_cattex_conv2", "<no_pos>")
	System.out.println("Number of lines removed: "+n2+"  left:"+frolex.getSize()+"\n")
	
	// Added AL
	
	System.out.println("Recoding 'lemma_src' with "+convLemmaSRC2)
	int n3 = frolex.recodeEntryProperties("lemma_src", "lemma_src", convLemmaSRC2, "abandon", false)
	System.out.println("Done, "+n3+" lines recoded.")
	
	// end added AL
	
	DictionaryManager.getInstance().getHSQLFunctions().getConnection().commit()
	
	File tsvFile = new File(workingDirectory, "result/frolex.tsv")
	File tsvFile2 = new File(workingDirectory, "result/clfrolex.tsv")
	
	if (frolex.print(tsvFile, -1, "form", "FRENCH 3", true) && clfrolex.print(tsvFile2, -1, "lemma", "FRENCH 3", true)) {
		System.out.println("FROLEX exported in "+tsvFile.getAbsolutePath())
		System.out.println("CLFROLEX exported in "+tsvFile2.getAbsolutePath())
		return frolex
	} else {
		System.out.println("Fail to export frolex.")
		return null
	}
}

/* SQL QUERIES
 comparer les 'category' du dmf_site avec celles du dmf (lgerm)
 SELECT DISTINCT "category" FROM "dmfsite" WHERE "category" not in (SELECT DISTINCT "category" FROM "dmf")
 affiche les lemmes de AFRLEX déjà référencés dans le DMF
 SELECT * FROM "clfrolex" WHERE lemma_src='TL' AND "lemma" IN (SELECT "TL" FROM "clfrolex" WHERE "lemma_src"='DMF' AND "TL" != '')
 supprime les lemmes de AFRLEX déjà référencés dans le DMF
 DELETE FROM "clfrolex" WHERE lemma_src='TL' AND "lemma" IN (SELECT "TL" FROM "clfrolex" WHERE "lemma_src"='DMF' AND "TL" != '')
 */
