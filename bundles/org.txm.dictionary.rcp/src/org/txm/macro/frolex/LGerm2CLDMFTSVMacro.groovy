package org.txm.macro.frolex

import java.io.File
import java.io.IOException
import java.io.InputStream
import java.io.PrintWriter
import java.net.URL
import java.util.Arrays
import java.util.LinkedHashMap

import javax.xml.stream.XMLInputFactory
import javax.xml.stream.XMLStreamConstants
import javax.xml.stream.XMLStreamException
import javax.xml.stream.XMLStreamReader

import org.txm.importer.ApplyXsl2
import org.txm.utils.io.IOUtils

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

// PARAMETERS
@Field @Option(name="workingDirectory", usage="workingDirectory containing all files needed", widget="Folder", required=true, def="dir")
File workingDirectory = null
if (!ParametersDialog.open(this)) {
	System.out.println("Aborting CLBFMSITELEX creation.")
	return null
}
File xmlDirectory = new File(workingDirectory, "lgerm-processed")
if (!xmlDirectory.exists()) {
	println "Aborting: file is missing: $xmlDirectory"
	return
}
File tsvFile = new File(workingDirectory, "cldmf-lgerm.tsv")

// INNERCLASS

class Entry2 {
	public String lemma
	public String lemma_source // not written
	public String category
	public LinkedHashMap<String, String> corresps = new LinkedHashMap<String, String>()

	public Entry2() { corresps.put("AND", "")
		corresps.put("DEAF", "")
		corresps.put("DECT", "")
		corresps.put("FEW", "")
		corresps.put("GDF", "")
		corresps.put("GDC", "")
		corresps.put("HUG", "")
		corresps.put("TLF", "")
		corresps.put("TL", "")
		corresps.put("UNDEF", "")}

	public Entry2(String lemma2, String category2) {
		this();
		lemma = lemma2
		category = category2
	}

	public String toString() {
		StringBuilder str = new StringBuilder()
		if (lemma == null) System.out.println("ERROR with entry="+lemma+" "+category+" "+corresps)
		else if (category == null) System.out.println("ERROR with entry="+lemma+" "+category+" "+corresps)
		else {
			str.append(lemma+"\t"+category)
			for (String corresp : corresps.keySet()) {
				str.append("\t"+corresps.get(corresp))
			}
			str.append("\t"+lemma_source)
			str.append("\n")
		}
		return str.toString()
	}

	public boolean equals(Entry2 entry) {
		return lemma.equals(entry.lemma) && category.equals(entry.category)
	}

	public int hashCode() {
		return lemma.hashCode() + category.hashCode()
	}
}


//START

factory = XMLInputFactory.newInstance()
writer = IOUtils.getWriter(tsvFile, "UTF-8")
lines = [:]


File[] xmlFiles = ApplyXsl2.listFiles(xmlDirectory)
if (xmlFiles == null) return false
Arrays.sort(xmlFiles)

for (File xmlFile : xmlFiles) {
	if (!xmlFile.getName().endsWith(".xml")) continue
	if (!processFile(xmlFile)) {
		System.out.println("Error while parsing "+xmlFile)
		return false
	}
}

writer.print("lemma\tcategory")
Entry2 tmp = new Entry2()
for (String corresp : tmp.corresps.keySet()) {
	writer.print("\t"+corresp)
}
writer.print("\tlemma_source")
writer.println("")
def keys = lines.keySet().sort()
for (def key : keys) {
	writer.print(lines[key]);
}
writer.close()

return true



boolean processFile(File xmlFile) throws IOException, XMLStreamException {

	URL inputurl = xmlFile.toURI().toURL()
	InputStream inputData = inputurl.openStream()
	XMLStreamReader parser = factory.createXMLStreamReader(inputData)
	
	try {
		
		Entry2 current = null
		String target = null
		boolean inLemmatizedForm = false
		String orthography = null
		boolean inOrthography = false
		String grammaticalCategory = null
		boolean inGrammaticalCategory = false

		String localname
		for (int event = parser.next() ; event != XMLStreamConstants.END_DOCUMENT ; event = parser.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName()
					if ("lemmatizedForm".equals(localname)) {
						if (current != null) {
							lines[current.lemma+"_"+current.category] = current;
						}
						current = new Entry2()
						inLemmatizedForm = true
					} else if ("orthography".equals(localname)) {
						inOrthography = true
						orthography = ""
						target = parser.getAttributeValue(null, "target")
					} else if ("grammaticalCategory".equals(localname)) {
						inGrammaticalCategory = true
						grammaticalCategory = ""
					}
					break
				case XMLStreamConstants.CHARACTERS:
					if (inGrammaticalCategory) grammaticalCategory += parser.getText()
					else if (inOrthography) orthography += parser.getText()
					break
				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName()
					if ("lemmatizedForm".equals(localname)) {
						inLemmatizedForm = false
					} else if ("orthography".equals(localname)) {
						inOrthography = false
						orthography = orthography.trim()
						if (inLemmatizedForm) { // lemma orthography
							if (target.equals("DMF")) {
								current.lemma = orthography
								current.lemma_source = target
							} else if (target.equals("LGeRM") && !"DMF".equals(current.lemma_source)) {
								current.lemma = orthography
								current.lemma_source = target
							} else if (current.corresps.keySet().contains(target)) {
								current.corresps.put(target, orthography)
							} else {
								System.out.println("Error unknown lemma="+orthography+" with corresp="+target)
							}
						}
					} else if ("grammaticalCategory".equals(localname)) {
						inGrammaticalCategory = false
						grammaticalCategory = grammaticalCategory.trim()
						current.category = grammaticalCategory
					}
					break
			}
		}

		if (current != null) { // last entry
			lines[current.lemma+"_"+current.category] = current;
		}

		parser.close()
		inputData.close()
	} catch(Exception e) {
		System.out.println("Unexpected error while parsing file "+xmlFile+" : "+e)
		System.out.println("Location line: "+parser.getLocation().getLineNumber()+" character: "+parser.getLocation().getColumnNumber())
		org.txm.utils.logger.Log.printStackTrace(e)
		//e.printStackTrace()

		parser.close()
		inputData.close()
		return false
	}

	return true
}

