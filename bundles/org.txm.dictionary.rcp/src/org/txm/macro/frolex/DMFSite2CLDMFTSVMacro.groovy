package org.txm.macro.frolex

import java.io.File
import java.io.IOException
import java.io.PrintWriter
import java.nio.charset.Charset
import java.util.ArrayList
import java.util.HashMap

import javax.xml.stream.XMLStreamException

import org.txm.utils.CsvReader
import org.txm.utils.io.IOUtils

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

// PARAMETERS
@Field @Option(name="workingDirectory", usage="workingDirectory containing all files needed", widget="Folder", required=true, def="dir")
File workingDirectory = null
if (!ParametersDialog.open(this)) {
	System.out.println("Aborting CLBFMSITELEX creation.")
	return null
}

File dmfsiteTSVFile = new File(workingDirectory, "dmf_site.tsv")
if (!dmfsiteTSVFile.exists()) {
	println "Aborting: file is missing: $dmfsiteTSVFile"
	return
}
File tsvFile = new File(workingDirectory, "cldmf-site.tsv")

//START

HashMap<String, Entry2> lemmas = new HashMap<String, Entry2>()

CsvReader reader = new CsvReader(dmfsiteTSVFile.getAbsolutePath(), "\t".charAt(0), Charset.forName("UTF-8"))
reader.readHeaders()
while (reader.readRecord()) {
	String lemma = reader.get("dmf_lemma")
	String category = reader.get("category")
	String corresp = convertCorresp(reader.get("source"))
	String corresp_lemma = reader.get("source_lemma")
	String key = lemma+"_"+category
	if (!lemmas.containsKey(key)) {
		lemmas.put(key, new Entry2(lemma, category))
		lemmas.get(key).lemma_source = "DMF"
	}
	Entry2 current = lemmas.get(key)
	current.lemma_source = "DMF"
	if (current.corresps.keySet().contains(corresp)) {
		current.corresps.put(corresp, corresp_lemma)
	} else {
		System.out.println("Error unknown lemma="+lemma+" with corresp="+corresp+" and corresp_lemma="+corresp_lemma)
	}
}
reader.close()

PrintWriter writer = IOUtils.getWriter(tsvFile)

writer.print("lemma\tcategory")
Entry2 tmp = new Entry2()
for (String corresp : tmp.corresps.keySet()) {
	writer.print("\t"+corresp)
}
writer.print("\tlemma_source")
writer.println("")

ArrayList<String> keys = new ArrayList<String>(lemmas.keySet())
keys.sort()
for (String key : keys) {
	writer.print(lemmas.get(key))
}
writer.close()
return true

// END

String convertCorresp(String corresp) {
	if (corresp.equals("AND")) {
		return "AND"
	} else if (corresp.matches("\\*?DEAF.*")) {
		return "DEAF"
	} else if (corresp.equals("DÉCT")) {
		return "DECT"
	} else if (corresp.startsWith("FEW")) {
		return "FEW"
	} else if (corresp.equals("GD")) {
		return "GDF"
	} else if (corresp.startsWith("GDC")) {
		return "GDC"
	} else if (corresp.equals("gs")) {
		return "TLF"
	} else if (corresp.equals("HUG")) {
		return "HUG"
	} else if (corresp.startsWith("T-L")) {
		return "TL"
	} else if (corresp.startsWith("TLF")) {
		return "TLF"
	} else {
		return "UNDEF"
	}
}

