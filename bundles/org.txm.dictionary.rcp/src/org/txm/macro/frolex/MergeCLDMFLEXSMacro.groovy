package org.txm.macro.frolex

import java.io.File
import java.io.IOException
import java.nio.charset.Charset
import java.util.LinkedHashMap

import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.bindings.keys.KeyStroke
import org.txm.utils.CsvReader
import org.txm.utils.io.IOUtils;

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

// PARAMETERS
@Field @Option(name="workingDirectory", usage="workingDirectory containing all files needed", widget="Folder", required=true, def="dir")
File workingDirectory = null
if (!ParametersDialog.open(this)) {
	System.out.println("Aborting CLBFMLEX creation.")
	return null
}

File cldmflexsitetsv = new File(workingDirectory, "cldmf-site.tsv")
if (!cldmflexsitetsv.exists()) {
	println "Aborting: file is missing: $cldmflexsitetsv"
	return
}
File cldmflexlgermtsv = new File(workingDirectory, "cldmf-lgerm.tsv")
if (!cldmflexlgermtsv.exists()) {
	println "Aborting: file is missing: $cldmflexlgermtsv"
	return
}
File tsvFile = new File(workingDirectory, "cldmf.tsv")

// START

LinkedHashMap<String, String[]> finalvalues = new LinkedHashMap<String, String[]>()
CsvReader reader_site = new CsvReader(cldmflexsitetsv.getAbsolutePath(), "\t".charAt(0), Charset.forName("UTF-8"))
CsvReader reader_lgerm = new CsvReader(cldmflexlgermtsv.getAbsolutePath(), "\t".charAt(0), Charset.forName("UTF-8"))
reader_site.readHeaders()
reader_lgerm.readHeaders()

reader_site.readRecord()
reader_lgerm.readRecord()
String key_site = reader_site.get("lemma")//+"_"+reader_site.get("category")
String key_lgerm = reader_lgerm.get("lemma")//+"_"+reader_lgerm.get("category")

def fixSiteCategory(String s) { // special fix for dmf_site category troncated
		if (s == "s" || s == "su" || s == "sub" || s == "subs" || s == "subst" || s == "subst. " ) {
			return "subst."
		} else if (s == "v" || s == "ve" || s == "ver" || s == "verb" ) {
			return "verbe"
		} else if (s == "a" || s == "adj" ) {
			return "adj."
		}
		return s
}

int nMerge = 0;
int nSite = 0;
int nLgerm = 0;
// merge lgerm and site lemma
while (key_site != null && key_lgerm != null) {
	
	def line = ["","","","","","","","","","","",""]
	if (key_site.equals(key_lgerm)) {
		//println "$key_site VS $key_lgerm"
		nMerge++
		line[0] = reader_site.get("lemma")
		line[1] = reader_lgerm.get("category") // site category is not broken
		
		if (reader_site.get("AND") != reader_lgerm.get("AND") && reader_lgerm.get("AND").length() > 0  && reader_site.get("AND").length() > 0) {
			println "Different AND: $key_site\n site ="+reader_site.get("AND")+"\n lgerm="+reader_lgerm.get("AND");
		}
		if (reader_site.get("AND").length() > 0)
			line[2] = reader_site.get("AND")
		else
			line[2] = reader_lgerm.get("AND")

		line[3] = reader_lgerm.get("DEAF")

		line[4] = reader_site.get("DECT")
		line[5] = reader_site.get("FEW")
		line[6] = reader_site.get("GDF")
		line[7] = reader_site.get("GDC")
		line[8] = reader_site.get("HUG")
		line[9] = reader_site.get("TLF")

		if (reader_site.get("TL") != reader_lgerm.get("TL") && reader_lgerm.get("TL").length() > 0 && reader_site.get("TL").length() > 0) {
			println "Different TL: $key_site\n site ="+reader_site.get("TL")+"\n lgerm="+reader_lgerm.get("TL");
		}
		if (reader_site.get("TL").length() > 0)
			line[10] = reader_site.get("TL")
		else
			line[10] = reader_lgerm.get("TL")
			
		//line[10] = reader_site.get("TLF")

		if (reader_site.get("lemma_source") != reader_lgerm.get("lemma_source")) {
			line[11] = "DMF"
		}
		line[11] = reader_site.get("lemma_source")

		finalvalues[key_site] = line
		
		if (reader_site.readRecord()) key_site = reader_site.get("lemma")//+"_"+reader_site.get("category")
		else key_site = null;
		
		if (reader_lgerm.readRecord()) key_lgerm = reader_lgerm.get("lemma")//+"_"+reader_lgerm.get("category")
		else key_lgerm = null
	} else if (key_site.compareTo(key_lgerm) >= 0) {
		nLgerm++
		line[0] = reader_lgerm.get("lemma")
		line[1] = reader_lgerm.get("category")
		
		line[2] = reader_lgerm.get("AND")
		line[3] = reader_lgerm.get("DEAF")
		line[4] = reader_lgerm.get("DECT")
		line[5] = reader_lgerm.get("FEW")
		line[6] = reader_lgerm.get("GDF")
		line[7] = reader_lgerm.get("GDC")
		line[8] = reader_lgerm.get("HUG")
		line[9] = reader_lgerm.get("TLF")
		line[10] = reader_lgerm.get("TL")
		line[11] = reader_lgerm.get("lemma_source")

		finalvalues[key_lgerm] = line
		
		if (reader_lgerm.readRecord()) key_lgerm = reader_lgerm.get("lemma")//+"_"+reader_lgerm.get("category")
		else key_lgerm = null
	} else {
		nSite++
		/*// ignore entries only in DMF site
		line[0] = reader_site.get("lemma")
		line[1] = reader_site.get("category")
		line[1] = fixSiteCategory(line[1])
		line[2] = reader_site.get("AND")
		line[3] = reader_site.get("DEAF")
		line[4] = reader_site.get("DECT")
		line[5] = reader_site.get("FEW")
		line[6] = reader_site.get("GDF")
		line[7] = reader_site.get("GDC")
		line[8] = reader_site.get("HUG")
		line[9] = reader_site.get("TLF")
		line[10] = reader_site.get("TL")
		line[11] = reader_site.get("lemma_source")

		finalvalues[key_site] = line
		*/
		if (reader_site.readRecord()) key_site = reader_site.get("lemma")//+"_"+reader_site.get("category")
		else key_site = null;
	}
}

// write last Lgerm lemma
while (key_lgerm != null) {
	nLgerm++
	def line = ["","","","","","","","","","","",""]

	line[0] = reader_lgerm.get("lemma")
	line[1] = reader_lgerm.get("category")
	line[2] = reader_lgerm.get("AND")
	line[3] = reader_lgerm.get("DEAF")
	line[4] = reader_lgerm.get("DECT")
	line[5] = reader_lgerm.get("FEW")
	line[6] = reader_lgerm.get("GDF")
	line[7] = reader_lgerm.get("GDC")
	line[8] = reader_lgerm.get("HUG")
	line[9] = reader_lgerm.get("TLF")
	line[10] = reader_lgerm.get("TL")
	line[11] = reader_lgerm.get("lemma_source")

	finalvalues[key_lgerm] = line
	
	if (reader_lgerm.readRecord()) key_lgerm = reader_lgerm.get("lemma")//+"_"+reader_lgerm.get("category")
	else key_lgerm = null
}

// write last site lemma
while (key_site != null) {
	nSite++
	def line = ["","","","","","","","","","","",""]

	line[0] = reader_site.get("lemma")
	line[1] = reader_site.get("category")
	line[1] = fixSiteCategory(line[1])
	line[2] = reader_site.get("AND")
	line[3] = reader_site.get("DEAF")
	line[4] = reader_site.get("DECT")
	line[5] = reader_site.get("FEW")
	line[6] = reader_site.get("GDF")
	line[7] = reader_site.get("GDC")
	line[8] = reader_site.get("HUG")
	line[9] = reader_site.get("TLF")
	line[10] = reader_site.get("TL")
	line[11] = reader_site.get("lemma_source")

	finalvalues[key_site] = line
	
	if (reader_site.readRecord()) key_site = reader_site.get("lemma")//+"_"+reader_site.get("category")
	else key_site = null;
}

def writer = IOUtils.getWriter(tsvFile);
writer.println("lemma	category	AND	DEAF	DECT	FEW	GDF	GDC	HUG	TLF	TL	lemma_source");
for (String key : finalvalues.keySet()) {
	writer.println(StringUtils.join(finalvalues.get(key), "\t"));
}
writer.close();
println "nMerge=$nMerge"
println "nLgerm=$nLgerm"
println "nSite=$nSite"
// END
