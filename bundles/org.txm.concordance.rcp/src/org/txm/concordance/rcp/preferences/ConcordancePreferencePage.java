// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.concordance.rcp.preferences;

import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.txm.concordance.core.preferences.ConcordancePreferences;
import org.txm.concordance.rcp.adapters.ConcordanceAdapterFactory;
import org.txm.concordance.rcp.messages.ConcordanceUIMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;

/**
 * Concordance preferences page.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ConcordancePreferencePage extends TXMPreferencePage {

	public ConcordancePreferencePage() {
		super();
	}

	@Override
	protected void createFieldEditors() {
		
		
		IntegerFieldEditor elementsPerPage = new IntegerFieldEditor(TXMPreferences.NB_OF_ELEMENTS_PER_PAGE, TXMUIMessages.common_elementsPerPage, this.getFieldEditorParent());
		elementsPerPage.getTextControl(this.getFieldEditorParent()).setToolTipText(TXMUIMessages.common_numberOfElementsToDisplayPerPage);
		this.addField(elementsPerPage);	
		
		this.addField(new IntegerFieldEditor(ConcordancePreferences.LEFT_CONTEXT_SIZE, ConcordanceUIMessages.ampLeftContextLengthInWords, this.getFieldEditorParent()));
		this.addField(new IntegerFieldEditor(ConcordancePreferences.RIGHT_CONTEXT_SIZE, ConcordanceUIMessages.ampRightContextLengthInWords, this.getFieldEditorParent()));
	}

	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(ConcordancePreferences.getInstance().getPreferencesNodeQualifier()));
		this.setTitle(ConcordanceUIMessages.concordanceParameters);
		this.setImageDescriptor(ConcordanceAdapterFactory.ICON);
	}
}
