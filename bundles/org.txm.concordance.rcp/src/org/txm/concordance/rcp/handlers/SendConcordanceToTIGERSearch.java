// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.concordance.rcp.handlers;

import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.core.functions.Line;
import org.txm.core.results.TXMResult;
import org.txm.links.rcp.handlers.SendSelectionToQueryable;
import org.txm.rcp.editors.TXMEditor;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;

/**
 * Sends the selected lines of a concordance to compute a TIGERSearch command.
 * 
 * @author mdecorde
 * 
 */
public class SendConcordanceToTIGERSearch extends SendSelectionToQueryable {

	public Concordance getConcordance(ExecutionEvent event, ISelection selection) {
		if (selection instanceof StructuredSelection) {
			StructuredSelection sselection = (StructuredSelection) selection;
			List<?> list = sselection.toList();
			if (list.size() == 0) return null;
			Object o = list.get(0);
			if (!(o instanceof org.txm.concordance.core.functions.Line)) return null;
			Line line = (Line) o;
			return line.getConcordance();
		}
		else {
			return null;
		}
	}

	@Override
	public Query createQuery(ExecutionEvent event, ISelection selection) {
		Concordance concordance = getConcordance(event, selection);
		if (concordance == null) return new CQLQuery(""); //$NON-NLS-1$

		StructuredSelection sselection = (StructuredSelection) selection;
		List<?> list = sselection.toList();
		IQuery query = concordance.getQuery();
		String engine = query.getSearchEngine().getName();
		if ("TIGER".equals(engine)) { //$NON-NLS-1$
			return (Query) query; // TODO the command must also set the current shown graph
		}
		else { // use the pivot word IDs
			StringBuilder buffer = new StringBuilder();
			buffer.append("[editionId=/"); //$NON-NLS-1$
			for (int i = 0; i < list.size(); i++) {
				Line line = (Line) list.get(i);
				if (line.getMatch().size() == 0) continue; // avoid NPE later

				if (i > 0) buffer.append("|"); //$NON-NLS-1$
				buffer.append(line.getKeywordIds().get(0));
			}
			buffer.append("/]"); //$NON-NLS-1$
			return new CQLQuery(buffer.toString());
		}
	}

	@Override
	public Integer getIndexResult(ExecutionEvent event, ISelection selection) {
		Concordance concordance = getConcordance(event, selection);
		if (concordance == null) return null;

		IQuery query = concordance.getQuery();
		String engine = query.getSearchEngine().getName();
		if ("TIGER".equals(engine)) { //$NON-NLS-1$
			return concordance.getTopIndex(); // TODO the command must also set the current shown graph
		}
		else {
			return null;
		}
	}

	@Override
	public Integer getSubIndexResult(ExecutionEvent event, ISelection selection) {
		Concordance concordance = getConcordance(event, selection);
		if (concordance == null) return 0;

		IQuery query = concordance.getQuery();
		String engine = query.getSearchEngine().getName();
		if ("TIGER".equals(engine)) { //$NON-NLS-1$
			return null; // TODO the command must also set the current shown graph
		}
		else {
			return null;
		}
	}

	@Override
	public List<Query> createQueries(ExecutionEvent event, ISelection selection) {
		return null;
	}

	@Override
	public TXMResult getResultParent(ExecutionEvent event, String id) {
		@SuppressWarnings("unchecked")
		TXMEditor<Concordance> editor = (TXMEditor<Concordance>) HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();

		TXMResult result = editor.getResult();
		Concordance concordance = (Concordance) result;
		CQPCorpus corpus = CQPCorpus.getFirstParentCorpus(concordance);

		return corpus;
	}
}
