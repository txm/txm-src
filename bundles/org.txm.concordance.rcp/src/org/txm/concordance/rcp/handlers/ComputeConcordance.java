// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.concordance.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.concordance.rcp.messages.ConcordanceUIMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.TXMResult;
import org.txm.rcp.StatusLine;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.searchengine.core.QueryBasedTXMResult;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.statsengine.core.messages.StatsEngineCoreMessages;
import org.txm.utils.logger.Log;

/**
 * Opens a Concordance editor.
 * Computes the result if it contains all required parameters.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class ComputeConcordance extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkCorpusEngine()) {
			return false;
		}

		// From link/"send to command": creating from a prebuilt parameter node path
		//FIXME: SJ: all "Send to" commands have a different node path schema than normal result creation since we do not know the target class in the source handler (the class string is replaced with "_link")  
		String parametersNodePath = event.getParameter(TXMPreferences.RESULT_PARAMETERS_NODE_PATH);
		if (parametersNodePath != null && !parametersNodePath.isEmpty()) {
			if ("none".equals(parametersNodePath)) { //$NON-NLS-1$
				Log.warning(ConcordanceUIMessages.SelectionError);
				return null;
			}
			Concordance concordance = null;
			if (TXMPreferences.getBoolean("uniqlink", parametersNodePath)) { // remove all other uniqlink concordance of the parent //$NON-NLS-1$
				
				String parentID = TXMPreferences.getString(TXMPreferences.PARENT_PARAMETERS_NODE_PATH, parametersNodePath);
				TXMResult parentResult = TXMResult.getResult(parentID);
				for (Concordance c : parentResult.getChildren(Concordance.class)) {
					SWTEditorsUtils.closeEditors(c);
					c.delete(true);
				}
			}
			
			if (concordance == null) {
				concordance = new Concordance(parametersNodePath);
				concordance.setMustBeSynchronizedWithParent(true);
			}
			return openEditor(concordance);
		} // From view result node
		else {
			Object selection = this.getCorporaViewSelectedObject(event);
			return openEditor(selection);
		}
	}

	public static ConcordanceEditor openEditor(Object selection) {

		if (selection == null) return null;

		// Creating from Corpus
		Concordance concordance = null;
		if (selection instanceof CQPCorpus) {
			CQPCorpus corpus = (CQPCorpus) selection;
			concordance = new Concordance(corpus);
		}
		// Reopening existing result
		else if (selection instanceof Concordance) {
			concordance = (Concordance) selection;
		}
		// Reopening existing result
		else if (selection instanceof QueryBasedTXMResult) {
			QueryBasedTXMResult r = (QueryBasedTXMResult) selection;
			concordance = new Concordance(r.getFirstParent(CQPCorpus.class));
			concordance.setQuery(r.getQuery());
		}
		// Error
		else {
			Log.severe(StatsEngineCoreMessages.bind(StatsEngineCoreMessages.CanNotExecuteTheP0CommandWithTheP1Selection, "Concordances", selection)); //$NON-NLS-1$
			return null;
		}

		return openEditor(concordance);
	}

	public static ConcordanceEditor openEditor(Concordance concordance) {
		StatusLine.setMessage(ConcordanceUIMessages.openingTheConcordanceResult);
		TXMEditor<Concordance> editor = TXMEditor.openEditor(concordance, ConcordanceEditor.class.getName());
		if (editor != null && editor instanceof ConcordanceEditor ceditor) {
			return ceditor;
		}
		return null;
	}
}
