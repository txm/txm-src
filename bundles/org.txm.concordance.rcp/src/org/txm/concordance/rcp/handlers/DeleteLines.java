// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.concordance.rcp.handlers;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.core.functions.Line;
import org.txm.concordance.rcp.editors.ConcordanceEditor;

/**
 * Allow the user to delete lines in a Concordance table.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class DeleteLines extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		// ISelection selection =
		// HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();

		IWorkbenchPart editor = HandlerUtil.getActiveWorkbenchWindow(event)
				.getActivePage().getActivePart();
		if (editor instanceof ConcordanceEditor) {
			ConcordanceEditor ceditor = (ConcordanceEditor) editor;
			deleteConcordanceLines(ceditor);
		}

		return null;
	}


	public static void deleteConcordanceLines(ConcordanceEditor editor) {
		TableViewer viewer = editor.getTableViewer();
		Concordance concordance = editor.getResult();
		if (concordance == null) return;
		IStructuredSelection sel = (IStructuredSelection) viewer.getSelection();
		@SuppressWarnings("unchecked")
		List<Line> lines = sel.toList();

		if (lines.size() == 0) return;

		try {
			//	System.out.println("Delete line "+lines);
			if (concordance.removeLines(lines)) {
				editor.fillDisplayArea(false);
			}
		}
		catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
