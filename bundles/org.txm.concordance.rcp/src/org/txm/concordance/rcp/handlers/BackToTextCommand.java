package org.txm.concordance.rcp.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.objects.Match;

public abstract class BackToTextCommand extends AbstractHandler {

	protected String name = "BackToText"; //$NON-NLS-1$

	@Override
	public abstract Object execute(ExecutionEvent event) throws ExecutionException;

	public boolean backToText(ConcordanceEditor editor, Match match) {
		return false;
	}
}
