// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.concordance.rcp.editors;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.txm.concordance.rcp.messages.ConcordanceUIMessages;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.WordProperty;

// TODO: Auto-generated Javadoc
/**
 * the dialog box called to select the view properties @ author mdecorde.
 */
public class SortPropertySelectionDialog extends Dialog {

	/** The available properties. */
	final private List<WordProperty> availableProperties;

	/** The selected properties. */
	final private List<WordProperty> selectedProperties;

	/** The maxprops. */
	int maxprops = -1;

	/** The available properties view. */
	org.eclipse.swt.widgets.List availablePropertiesSort;

	/** The selected properties view. */
	org.eclipse.swt.widgets.List selectedPropertiesSort;

	/**
	 * Instantiates a new view property selection dialog.
	 *
	 * @param parentShell the parent shell
	 * @param availableProperties the available properties
	 * @param selectedProperties the selected properties
	 */
	public SortPropertySelectionDialog(IShellProvider parentShell,
			List<WordProperty> availableProperties,
			List<WordProperty> selectedProperties) {
		super(parentShell);

		this.availableProperties = availableProperties;
		this.selectedProperties = selectedProperties;
		this.setShellStyle(this.getShellStyle() | SWT.RESIZE);
	}

	/**
	 * Instantiates a new view property selection dialog.
	 *
	 * @param shell the shell
	 * @param availableProperties the available properties
	 * @param selectedProperties the selected properties
	 */
	public SortPropertySelectionDialog(Shell shell,
			List<WordProperty> availableProperties,
			List<WordProperty> selectedProperties) {
		super(shell);

		this.availableProperties = availableProperties;
		this.selectedProperties = selectedProperties;
		this.setShellStyle(this.getShellStyle() | SWT.RESIZE);
	}

	/**
	 * Instantiates a new view property selection dialog.
	 *
	 * @param shell the shell
	 * @param availableProperties the available properties
	 * @param selectedProperties the selected properties
	 * @param maxprops the maxprops
	 */
	public SortPropertySelectionDialog(Shell shell,
			List<WordProperty> availableProperties,
			List<WordProperty> selectedProperties, int maxprops) {
		super(shell);

		this.availableProperties = availableProperties;
		this.selectedProperties = selectedProperties;
		this.setShellStyle(this.getShellStyle() | SWT.RESIZE);
		this.maxprops = maxprops;
	}

	/**
	 * Gets the selected properties.
	 *
	 * @return the selected properties
	 */
	public List<WordProperty> getSelectedProperties() {
		return selectedProperties;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(TXMUIMessages.sortOptions);
		newShell.setMinimumSize(300, 200);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		// parent.setLayout(new FormLayout());
		Composite mainArea = new Composite(parent, SWT.NONE);
		mainArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		// 4 columns
		GridLayout layout = new GridLayout(4, false);
		mainArea.setLayout(layout);

		availablePropertiesSort = new org.eclipse.swt.widgets.List(mainArea,
				SWT.BORDER | SWT.V_SCROLL);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		availablePropertiesSort.setLayoutData(gridData);

		Composite selectionButtons = new Composite(mainArea, SWT.NONE);
		selectionButtons.setLayout(new GridLayout(1, false));
		Button select = new Button(selectionButtons, SWT.ARROW | SWT.RIGHT);
		select.setToolTipText(ConcordanceUIMessages.selectTheCurrentElement);
		Button unselect = new Button(selectionButtons, SWT.ARROW | SWT.LEFT);
		unselect.setToolTipText(ConcordanceUIMessages.deselectTheCurrentElement);

		selectedPropertiesSort = new org.eclipse.swt.widgets.List(mainArea, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		selectedPropertiesSort.setLayoutData(gridData);

		Composite orderButtons = new Composite(mainArea, SWT.NONE);
		orderButtons.setLayout(new GridLayout(1, false));
		Button up = new Button(orderButtons, SWT.ARROW | SWT.UP);
		up.setToolTipText(ConcordanceUIMessages.moveUpTheCurrentElement);
		Button down = new Button(orderButtons, SWT.ARROW | SWT.DOWN);
		down.setToolTipText(ConcordanceUIMessages.moveDownTheCurrentElement);
		
		select.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int index = availablePropertiesSort.getSelectionIndex();
				if (maxprops == 1) {
					selectedProperties.add(availableProperties.get(index));
					availableProperties.remove(index);

					availableProperties.add(selectedProperties.get(0));
					selectedProperties.remove(0);
				}
				else {
					if (maxprops > 0) {
						if (selectedProperties.size() >= maxprops)
							return;
					}

					selectedProperties.add(availableProperties.get(index));
					availableProperties.remove(index);
				}
				reload();
				availablePropertiesSort.setSelection(index);
			}
		});

		unselect.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (maxprops == 1 || selectedProperties.size() > 1) {
					int index = selectedPropertiesSort.getSelectionIndex();

					availableProperties.add(selectedProperties.get(index));
					selectedProperties.remove(index);

					reload();
					selectedPropertiesSort.setSelection(index);
				}
			}
		});

		up.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int index = selectedPropertiesSort.getSelectionIndex();
				if (index > 0) {
					WordProperty selectedP = selectedProperties.get(index);
					WordProperty upperP = selectedProperties.get(index - 1);

					selectedProperties.set(index, upperP);
					selectedProperties.set(index - 1, selectedP);

					reload();
					selectedPropertiesSort.setSelection(index - 1);
				}
			}
		});

		down.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int index = selectedPropertiesSort.getSelectionIndex();
				if (index < selectedProperties.size() - 1) {
					WordProperty selectedP = selectedProperties.get(index);
					WordProperty bellowP = selectedProperties.get(index + 1);

					selectedProperties.set(index, bellowP);
					selectedProperties.set(index + 1, selectedP);

					reload();
					selectedPropertiesSort.setSelection(index + 1);
				}
			}
		});

		availablePropertiesSort.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				int index = availablePropertiesSort.getSelectionIndex();

				selectedProperties.add(availableProperties.get(index));
				availableProperties.remove(index);

				reload();
				availablePropertiesSort.setSelection(index);
			}
		});

		selectedPropertiesSort.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				if (selectedProperties.size() > 1) {
					int index = selectedPropertiesSort.getSelectionIndex();

					availableProperties.add(selectedProperties.get(index));
					selectedProperties.remove(index);

					reload();
					selectedPropertiesSort.setSelection(index);
				}
			}
		});

		reload();
		return mainArea;
	}

	/**
	 * Reload.
	 */
	public void reload() {
		availablePropertiesSort.removeAll();
		for (Property property : availableProperties) {
			//if (!property.getName().equals("id")) //$NON-NLS-1$
			if (property instanceof StructuralUnitProperty)
				availablePropertiesSort.add(((StructuralUnitProperty) property).getFullName());
			else
				availablePropertiesSort.add(property.getName());
		}
		selectedPropertiesSort.removeAll();
		for (Property property : selectedProperties) {
			//if (!property.getName().equals("id")) //$NON-NLS-1$
			if (property instanceof StructuralUnitProperty)
				selectedPropertiesSort.add(((StructuralUnitProperty) property).getFullName());
			else
				selectedPropertiesSort.add(property.getName());
		}

		availablePropertiesSort.getParent().layout();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		if (selectedProperties.size() != 0)
			super.okPressed();
	}

}
