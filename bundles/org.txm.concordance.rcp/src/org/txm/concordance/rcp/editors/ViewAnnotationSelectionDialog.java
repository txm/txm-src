// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.concordance.rcp.editors;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

// TODO: Auto-generated Javadoc
/**
 * the dialog box called to select the view properties @ author mdecorde.
 */
public class ViewAnnotationSelectionDialog extends Dialog {


	/**
	 * Instantiates a new view property selection dialog.
	 *
	 * @param parentShell the parent shell
	 */
	public ViewAnnotationSelectionDialog(IShellProvider parentShell) {
		super(parentShell);

		this.setShellStyle(this.getShellStyle() | SWT.RESIZE);
	}

	/**
	 * Instantiates a new view property selection dialog.
	 *
	 * @param shell the shell
	 */
	public ViewAnnotationSelectionDialog(Shell shell) {
		super(shell);

		this.setShellStyle(this.getShellStyle() | SWT.RESIZE);
	}



	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		//newShell.setText(Messages.ViewPropertySelectionDialog_0);
		newShell.setText("A VOIR"); //$NON-NLS-1$
		newShell.setMinimumSize(200, 100);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		// parent.setLayout(new FormLayout());
		Composite mainArea = new Composite(parent, SWT.NONE);
		mainArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		return mainArea;
	}



	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {

	}

}
