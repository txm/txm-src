package org.txm.concordance.rcp.editors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class OpenFileDialog {

	Display d;

	Shell s;

	OpenFileDialog() {
		d = new Display();
		s = new Shell(d);
		s.setSize(400, 400);

		s.setText("A MessageBox Example"); //$NON-NLS-1$
		//         create the menu system
		Menu m = new Menu(s, SWT.BAR);
		// create a file menu and add an exit item
		final MenuItem file = new MenuItem(m, SWT.CASCADE);
		file.setText("&File"); //$NON-NLS-1$
		final Menu filemenu = new Menu(s, SWT.DROP_DOWN);
		file.setMenu(filemenu);
		final MenuItem openItem = new MenuItem(filemenu, SWT.PUSH);
		openItem.setText("&Open\tCTRL+O"); //$NON-NLS-1$
		openItem.setAccelerator(SWT.CTRL + 'O');
		final MenuItem saveItem = new MenuItem(filemenu, SWT.PUSH);
		saveItem.setText("&Save\tCTRL+S"); //$NON-NLS-1$
		saveItem.setAccelerator(SWT.CTRL + 'S');
		final MenuItem separator = new MenuItem(filemenu, SWT.SEPARATOR);
		final MenuItem exitItem = new MenuItem(filemenu, SWT.PUSH);
		exitItem.setText("E&xit"); //$NON-NLS-1$

		class Open implements SelectionListener {

			public void widgetSelected(SelectionEvent event) {
				FileDialog fd = new FileDialog(s, SWT.OPEN);
				fd.setText("Open"); //$NON-NLS-1$
				fd.setFilterPath("C:/"); //$NON-NLS-1$
				String[] filterExt = { "*.txt", "*.doc", ".rtf", "*.*" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				fd.setFilterExtensions(filterExt);
				String selected = fd.open();
				System.out.println(selected);
			}

			public void widgetDefaultSelected(SelectionEvent event) {
			}
		}

		class Save implements SelectionListener {

			public void widgetSelected(SelectionEvent event) {
				FileDialog fd = new FileDialog(s, SWT.SAVE);
				fd.setText("Save"); //$NON-NLS-1$
				fd.setFilterPath("C:/"); //$NON-NLS-1$
				String[] filterExt = { "*.txt", "*.doc", ".rtf", "*.*" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				fd.setFilterExtensions(filterExt);
				String selected = fd.open();
				System.out.println(selected);
			}

			public void widgetDefaultSelected(SelectionEvent event) {
			}
		}
		openItem.addSelectionListener(new Open());
		saveItem.addSelectionListener(new Save());

		exitItem.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				MessageBox messageBox = new MessageBox(s, SWT.ICON_QUESTION
						| SWT.YES | SWT.NO);
				messageBox.setMessage("Do you really want to exit?"); //$NON-NLS-1$
				messageBox.setText("Exiting Application"); //$NON-NLS-1$
				int response = messageBox.open();
				if (response == SWT.YES)
					System.exit(0);
			}
		});
		s.setMenuBar(m);
		s.open();

		while (!s.isDisposed()) {
			if (!d.readAndDispatch())
				d.sleep();
		}
		d.dispose();
	}

	public static void main(String[] argv) {
		new OpenFileDialog();
	}

}
