// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.concordance.rcp.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.txm.concordance.core.functions.comparators.LineComparator;
import org.txm.rcp.messages.TXMUIMessages;

// TODO: Auto-generated Javadoc
/**
 * dialog box used to define the complex sort @ author mdecorde.
 */
public class CompositeSorterCreationDialog extends Dialog {

	/** The available comparators. */
	final private List<LineComparator> availableComparators;

	/** The selected comparators. */
	final private List<LineComparator> selectedComparators;

	/** The name text. */
	private Text nameText;

	/** The name. */
	private String name;

	/**
	 * Instantiates a new composite sorter creation dialog.
	 *
	 * @param parentShell the parent shell
	 * @param availableComparators the available comparators
	 */
	protected CompositeSorterCreationDialog(IShellProvider parentShell,
			List<LineComparator> availableComparators) {
		super(parentShell);
		this.availableComparators = new ArrayList<LineComparator>(
				availableComparators);
		this.selectedComparators = new ArrayList<LineComparator>();
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the comparators.
	 *
	 * @return the comparators
	 */
	public List<LineComparator> getComparators() {
		return selectedComparators;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(TXMUIMessages.createANewSorter);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite mainArea = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(4, false);
		mainArea.setLayout(layout);

		Label nameLabel = new Label(mainArea, SWT.NONE);
		nameLabel.setText(TXMUIMessages.name);

		nameText = new Text(mainArea, SWT.NONE);
		GridData gridData = new GridData(SWT.FILL, SWT.TOP, true, false);
		gridData.horizontalSpan = 3;
		nameText.setLayoutData(gridData);

		final org.eclipse.swt.widgets.List availbleItems = new org.eclipse.swt.widgets.List(
				mainArea, SWT.BORDER | SWT.V_SCROLL);
		for (LineComparator comparator : availableComparators) {
			availbleItems.add(comparator.getName());
		}
		gridData = new GridData(GridData.FILL, GridData.FILL, true, false);
		Point size = availbleItems.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		gridData.widthHint = size.x;
		availbleItems.setLayoutData(gridData);

		Composite selectionButtons = new Composite(mainArea, SWT.NONE);
		selectionButtons.setLayout(new GridLayout(1, false));
		Button select = new Button(selectionButtons, SWT.ARROW | SWT.RIGHT);
		Button unselect = new Button(selectionButtons, SWT.ARROW | SWT.LEFT);

		final org.eclipse.swt.widgets.List selectedItems = new org.eclipse.swt.widgets.List(
				mainArea, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		selectedItems.setLayoutData(gridData);

		Composite orderButtons = new Composite(mainArea, SWT.NONE);
		orderButtons.setLayout(new GridLayout(1, false));
		Button up = new Button(orderButtons, SWT.ARROW | SWT.UP);
		Button down = new Button(orderButtons, SWT.ARROW | SWT.DOWN);

		select.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int ind = availbleItems.getSelectionIndex();
				selectedItems.add(availbleItems.getItem(ind));
				availbleItems.remove(ind);
				selectedComparators.add(availableComparators.get(ind));
				availableComparators.remove(ind);
				availbleItems.setSelection(ind);
			}
		});

		unselect.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int ind = selectedItems.getSelectionIndex();
				availbleItems.add(selectedItems.getItem(ind));
				selectedItems.remove(ind);
				availableComparators.add(selectedComparators.get(ind));
				selectedComparators.remove(ind);
				selectedItems.setSelection(ind);
			}
		});

		up.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int ind = selectedItems.getSelectionIndex();
				if (ind > 0) {
					selectedItems.add(selectedItems.getItem(ind), ind - 1);
					selectedItems.remove(ind + 1);
					selectedComparators.add(selectedComparators.get(ind - 1));
					selectedComparators.remove(ind + 1);
					selectedItems.setSelection(ind - 1);
				}
			}
		});

		down.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int ind = selectedItems.getSelectionIndex();
				if (ind < selectedItems.getItemCount() - 1) {
					selectedItems.add(selectedItems.getItem(ind), ind + 2);
					selectedItems.remove(ind);
					selectedComparators.add(selectedComparators.get(ind + 2));
					selectedComparators.remove(ind);
					selectedItems.setSelection(ind + 2);
				}
			}
		});
		return mainArea;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		if (nameText.getText().equals("")) { //$NON-NLS-1$
			MessageDialog.openError(getShell(),
					TXMUIMessages.invalidName,
					TXMUIMessages.nameIsMandatory);
			return;
		}
		name = nameText.getText();
		if (selectedComparators.size() < 2) {
			MessageDialog.openError(getShell(),
					TXMUIMessages.invalidComparator,
					TXMUIMessages.youMustSelectAtLeast2Comparators);
			return;
		}
		super.okPressed();
	}
}
