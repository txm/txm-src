// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.concordance.rcp.editors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.PartInitException;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.core.functions.Line;
import org.txm.concordance.core.functions.comparators.CompositeComparator;
import org.txm.concordance.core.functions.comparators.LexicographicKeywordComparator;
import org.txm.concordance.core.functions.comparators.LexicographicLeftContextComparator;
import org.txm.concordance.core.functions.comparators.LexicographicRightContextComparator;
import org.txm.concordance.core.functions.comparators.LineComparator;
import org.txm.concordance.core.functions.comparators.PropertiesReferenceComparator;
import org.txm.concordance.core.functions.comparators.ReverseComparator;
import org.txm.concordance.core.functions.comparators.WordPositionComparator;
import org.txm.concordance.core.preferences.ConcordancePreferences;
import org.txm.concordance.rcp.actions.DefineSortReferencePattern;
import org.txm.concordance.rcp.actions.DefineViewReferencePattern;
import org.txm.concordance.rcp.actions.SetContextSize;
import org.txm.concordance.rcp.actions.SetLineNumber;
import org.txm.concordance.rcp.actions.SortPropertySelection;
import org.txm.concordance.rcp.actions.ViewPropertySelection;
import org.txm.concordance.rcp.handlers.BackToTextCommand;
import org.txm.concordance.rcp.handlers.DeleteLines;
import org.txm.concordance.rcp.messages.ConcordanceUIMessages;
import org.txm.concordance.rcp.widgets.ComplexSortSelector;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.Parameter;
import org.txm.objects.Page;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.StatusLine;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TableKeyListener;
import org.txm.rcp.editors.TableResultEditor;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.editors.listeners.ComputeKeyListener;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.widget.AssistedChoiceQueryWidget;
import org.txm.rcp.swt.widget.NavigationWidget;
import org.txm.rcp.swt.widget.PropertiesSelector;
import org.txm.rcp.swt.widget.ReferencePatternSelector;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.QueriesView;
import org.txm.searchengine.core.SearchEngine;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.ReferencePattern;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.core.messages.CQPSearchEngineCoreMessages;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * Displays the concordances parameters and results in the same editor
 * 
 * @author mdecorde
 */
public class ConcordanceEditor extends TXMEditor<Concordance> implements TableResultEditor {

	public static final String ID = ConcordanceEditor.class.getName();

	String locale = Locale.getDefault().getLanguage();// RCPPreferences.getInstance().getString(RCPPreferences.CONFIG_LOCALE);

	private static final String BACKTOTEXT_COMMAND_ID = "org.txm.rcp.extensionpoint.backtotext"; //$NON-NLS-1$

	/** The lineids. */
	List<String> lineids;

	/** The standard comparators. */
	private List<LineComparator> standardComparators;

	/** The user defined comparators. */
	private List<LineComparator> userDefinedComparators;

	/** The complexsorter. */
	ComplexSortSelector complexsorter;

	org.eclipse.swt.widgets.Text cqlSeparator;

	//private Concordance concordance;

	/** The standard comparator class name. */
	private LineComparator[] standardComparatorClassName = {
			new WordPositionComparator(), new LexicographicKeywordComparator(),
			new LexicographicLeftContextComparator(),
			new LexicographicRightContextComparator(),
			new PropertiesReferenceComparator() };

	// /** The n line per page. */
	// private int result.getNLinePerPage();
	//
	// /** The top line. */
	// private int topLine;
	//
	// /** The bottom line. */
	// private int bottomLine;

	/** The current comparator. */
	protected LineComparator currentComparator;

	// /** The left context size. */
	// private int leftContextSize;
	//
	// /** The right context size. */
	// private int rightContextSize;

	/** The mouseposition. */
	private Point mouseposition;

	/** The line table viewer. */
	protected TableViewer viewerRight, viewerLeft;

	/** The reference column. */
	private TableColumn referenceColumn;

	/** The first column. */
	private TableViewerColumn firstColumn; // due to a restriction on some platforms
	// (actually, MS Windows), the first
	// column is always left aligned. The
	// work around is to have a 0 width
	// first column

	/** The left context column. */
	private TableColumn leftContextColumn;

	/** The keyword column. */
	private TableColumn keywordColumn;

	/** The right context column. */
	private TableColumn rightContextColumn;

	// /** The view properties label. */
	// private Label viewPropertiesLabel;

	/** The set sort property action. */
	private Action setSortPropertyAction;

	/** The set context size action. */
	private Action setContextSizeAction;

	/** The set line number action. */
	private Action setLineNumberAction;

	/** The define view reference pattern action. */
	protected Action defineViewReferencePatternAction;

	/** The define sort reference pattern action. */
	protected Action defineSortReferencePatternAction;

	/** The set view property action. */
	protected Action setViewPropertyAction;

	/** The progresslistener. */
	protected ProgressListener progresslistener;

	/** The textid. */
	protected String textid;

	/** The opened page. */
	Page openedPage;

	/** The query area. */
	private Composite queryArea;

	/** The navigation area. */
	private NavigationWidget navigationWidget;


	/**
	 * The hide btn: hides the form
	 * TODO: remove the button when the TXMEditorToolbar is fully functional.
	 */
	private ToolItem deleteLineButton;

	/** The query size. */
	protected Point querySize;

	private Sash sash;

	private Listener tableResizeListener;

	private Table leftTable;

	private Table rightTable;

	private Label queryLabel;

	private Composite navigationArea;


	/** The query widget. */
	@Parameter(key = ConcordancePreferences.QUERY)
	protected AssistedChoiceQueryWidget queryWidget;

	@Parameter(key = ConcordancePreferences.LEFT_ANALYSIS_PROPERTIES)
	private PropertiesSelector<WordProperty> leftSortProperties;

	@Parameter(key = ConcordancePreferences.SORT_REFERENCE_PATTERN)
	private PropertiesSelector<Property> refSortProperties;

	@Parameter(key = ConcordancePreferences.KEYWORD_ANALYSIS_PROPERTIES)
	private PropertiesSelector<WordProperty> keywordSortProperties;

	@Parameter(key = ConcordancePreferences.RIGHT_VIEW_PROPERTIES)
	private PropertiesSelector<WordProperty> rightViewProperties;

	@Parameter(key = ConcordancePreferences.KEYWORD_VIEW_PROPERTIES)
	private PropertiesSelector<WordProperty> keywordViewProperties;

	@Parameter(key = ConcordancePreferences.LEFT_VIEW_PROPERTIES)
	private PropertiesSelector<WordProperty> leftViewProperties;

	@Parameter(key = ConcordancePreferences.VIEW_REFERENCE_PATTERN)
	private ReferencePatternSelector refViewProperties;

	@Parameter(key = ConcordancePreferences.RIGHT_ANALYSIS_PROPERTIES)
	private PropertiesSelector<WordProperty> rightSortProperties;

	@Parameter(key = ConcordancePreferences.LEFT_CONTEXT_SIZE)
	private Spinner leftSizeSpinner;

	@Parameter(key = ConcordancePreferences.RIGHT_CONTEXT_SIZE)
	private Spinner rightSizeSpinner;

	@Parameter(key = ConcordancePreferences.TARGET_STRATEGY)
	private Combo targetStrategyCombo;

	private TableViewerColumn leftColumnViewer;

	private TableViewerColumn keywordColumnViewer;

	private TableViewerColumn rightContextColumnViewer;

	//tmp background
	private Color queryWidgetInitBackground = null;

	private TableKeyListener tableKeyListener;

	// private PropertiesSelector<Property> propsArea;

	@Override
	public void _init() throws PartInitException {

		//super.init(site, input);

		if (!(this.result instanceof Concordance)) {
			throw new PartInitException(ConcordanceUIMessages.bind(ConcordanceUIMessages.canNotOpenConcordanceEditorWithP0, result));
		}
		this.standardComparators = new ArrayList<>();
		this.userDefinedComparators = new ArrayList<>();
		this.setStandardComparators();
		//result.setNLinePerPage(ConcordancePreferences.getInstance().getInt(ConcordancePreferences.N_LINES_PER_PAGE));
	}

	@Override
	public void _createPartControl() {

		Composite controlArea = getExtendedParametersGroup();

		try {
			composeControlArea(controlArea);
			Composite d = new Composite(getResultArea(), SWT.NONE);
			composeDisplayArea(d);
			d.setLayoutData(new GridData(GridData.FILL_BOTH));
			// finally set the sorter to fill the tables
			if (complexsorter != null) {
				complexsorter.setComparators(this);
			}
		}
		catch (Exception e) {
			System.out.println(ConcordanceUIMessages.bind(ConcordanceUIMessages.errorWhileBuildingInterfaceColonP0, e.getLocalizedMessage()));
			Log.printStackTrace(e);
		}
	}

	private static String EMPTY = ""; //$NON-NLS-1$

	/**
	 * Compose display area.
	 *
	 * @param displayArea the display area
	 */

	private void composeDisplayArea(final Composite displayArea) throws Exception {

		// tableLayout = new TableColumnLayout();
		displayArea.setLayout(new FormLayout());

		viewerLeft = new TableViewer(displayArea, SWT.MULTI | SWT.BORDER);

		viewerLeft.setLabelProvider(new LineLabelProvider(this) {

			@Override
			public String getColumnText(Object element, int columnIndex) {

				if (!(element instanceof Line)) return EMPTY;

				Line line = (Line) element;
				//				if (columnIndex == 0) {
				//					return Integer.toString(line.getKeywordPosition());
				//				} else {
				return line.getViewRef().format();
				//				}
			}
		});
		viewerLeft.setContentProvider(new ConcordancesProvider());
		viewerLeft.getTable().setLinesVisible(true);
		viewerLeft.getTable().setHeaderVisible(true);

		// firstColumn = new TableColumn(viewer2.getTable(), SWT.LEFT);
		// firstColumn.setWidth(10);

		//		TableColumn nColumn = new TableColumn(viewerLeft.getTable(), SWT.LEFT | SWT.H_SCROLL);
		//		nColumn.setText("N");
		//		nColumn.setToolTipText("N");
		//		nColumn.setWidth(30);

		referenceColumn = new TableColumn(viewerLeft.getTable(), SWT.LEFT | SWT.H_SCROLL);
		refreshReferenceColumnTitle();
		referenceColumn.setToolTipText(ConcordanceUIMessages.reference);
		referenceColumn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				StatusLine.setMessage(ConcordanceUIMessages.sortingReferenceColumn);
				LineComparator comparator = new PropertiesReferenceComparator();
				comparator.initialize(result.getCorpus());

				// add the corpus order key
				LineComparator cComparator = new CompositeComparator("current", Arrays.asList(comparator, standardComparatorClassName[0])); //$NON-NLS-1$

				viewerLeft.getTable().setSortColumn(referenceColumn);
				if (viewerLeft.getTable().getSortColumn() != referenceColumn) {
					viewerLeft.getTable().setSortDirection(SWT.UP);
				}
				else if (viewerLeft.getTable().getSortDirection() == SWT.UP) {
					viewerLeft.getTable().setSortDirection(SWT.DOWN);
					cComparator = new ReverseComparator(cComparator);
				}
				else {
					viewerLeft.getTable().setSortDirection(SWT.UP);
				}

				currentComparator = cComparator;

				complexsorter.setSimpleSortKey(4);// set ref key
				sort();
				StatusLine.setMessage(""); //$NON-NLS-1$
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		referenceColumn.setWidth(100);

		sash = new Sash(displayArea, SWT.VERTICAL);
		sash.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {

				((FormData) sash.getLayoutData()).left = new FormAttachment(0, event.x);
				sash.getParent().layout();
			}
		});
		viewerRight = new TableViewer(displayArea, SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER);

		// viewer.setLabelProvider(new LineLabelProvider(this));
		viewerRight.setContentProvider(new ConcordancesProvider());
		viewerRight.getTable().setLinesVisible(true);
		viewerRight.getTable().setHeaderVisible(true);

		// FIXME Mac OS X: in TXM 0.8.x releases, the table line height is not correctly adjusted to the font size
		// String f = result.getCorpus().getFont();
		// if (f != null && f.length() > 0) {
		// Font old = viewerRight.getTable().getFont();
		// FontData fD = old.getFontData()[0];
		// Font font = new Font(Display.getCurrent(), f, fD.getHeight(), fD.getStyle());
		// viewerRight.getTable().setFont(font);
		// viewerLeft.getTable().setFont(font);
		//
		// viewerRight.getTable().addListener(SWT.MeasureItem, new Listener() {
		//
		// @Override
		// public void handleEvent(Event event) {
		// // height cannot be per row so simply set
		// event.height = 67;
		// }
		// });
		// viewerLeft.getTable().addListener(SWT.MeasureItem, new Listener() {
		//
		// @Override
		// public void handleEvent(Event event) {
		// // height cannot be per row so simply set
		// event.height = 67;
		// }
		// });
		// }

		tableResizeListener = new Listener() {

			@Override
			public void handleEvent(Event event) {

				ScrollBar bar = viewerRight.getTable().getHorizontalBar();
				float l = leftContextColumn.getWidth();
				float k = keywordColumn.getWidth();
				float r = rightContextColumn.getWidth();
				float t = viewerRight.getTable().getClientArea().width;// the width of the visible part of the table; l+k+r>=t
				float x = l + k / 2 - t / 2; // the abcisse the visible rectangle must have for the keyword column to be centered
				float M = bar.getMaximum();
				float T = bar.getThumb();
				bar.setSelection(Math.round(x * (M - T) / (l + k + r - t)));// this is how x translate in terms of bar selection
			}
		};

		// try to center on keyword column
		viewerRight.getTable().addListener(SWT.Resize, tableResizeListener);

		// TODO: to enable line deletion, Concordance code must updated -> replace with key binding
		KeyListener kldelete = new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {

				if (e.keyCode != SWT.DEL) {
					return;
				}
				DeleteLines.deleteConcordanceLines(ConcordanceEditor.this);
			}
		};
		viewerRight.getTable().addKeyListener(kldelete);
		viewerLeft.getTable().addKeyListener(kldelete);

		tableKeyListener = TableUtils.installCopyAndSearchKeyEvents(viewerLeft, viewerRight);
		tableKeyListener.installSearchGroup(this);
		tableKeyListener.installMenuContributions(this);
		TableUtils.installColumnResizeMouseWheelEvents(viewerRight, true, false);
		TableUtils.installTooltipsOnSelectionChangedEvent(viewerLeft, viewerRight);

		firstColumn = new TableViewerColumn(viewerRight, SWT.LEFT);
		firstColumn.getColumn().setWidth(0);
		firstColumn.getColumn().setResizable(false);
		firstColumn.setLabelProvider(new CellLabelProvider() {

			@Override
			public void update(ViewerCell cell) {
			}
		});

		leftColumnViewer = new TableViewerColumn(viewerRight, SWT.RIGHT);
		leftContextColumn = leftColumnViewer.getColumn();
		leftContextColumn.setText(ConcordanceUIMessages.leftContext);
		leftContextColumn.setToolTipText(ConcordanceUIMessages.leftContext);
		leftContextColumn.setAlignment(SWT.RIGHT);
		leftColumnViewer.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {

				Line line = (Line) element;
				return line.leftContextToString();
			}
		});

		leftContextColumn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				StatusLine.setMessage(ConcordanceUIMessages.sortingLeftContextColumn);
				LineComparator comparator = new LexicographicLeftContextComparator();
				comparator.initialize(result.getCorpus());

				// add the corpus order key
				LineComparator cComparator = new CompositeComparator("current", Arrays.asList(comparator, standardComparatorClassName[0])); //$NON-NLS-1$

				viewerRight.getTable().setSortColumn(leftContextColumn);
				if (viewerRight.getTable().getSortColumn() != leftContextColumn) {
					viewerRight.getTable().setSortDirection(SWT.UP);
				}
				else if (viewerRight.getTable().getSortDirection() == SWT.UP) {
					viewerRight.getTable().setSortDirection(SWT.DOWN);
					cComparator = new ReverseComparator(cComparator);
				}
				else {
					viewerRight.getTable().setSortDirection(SWT.UP);
				}

				currentComparator = cComparator;

				complexsorter.setSimpleSortKey(2);// set ref key
				sort();
				StatusLine.setMessage(""); //$NON-NLS-1$
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		keywordColumnViewer = new TableViewerColumn(viewerRight, SWT.CENTER);
		keywordColumn = keywordColumnViewer.getColumn();
		keywordColumn.setText(ConcordanceUIMessages.keyword);
		keywordColumn.setToolTipText(ConcordanceUIMessages.keyword);
		keywordColumn.setAlignment(SWT.CENTER);
		keywordColumnViewer.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {

				Line line = (Line) element;
				return line.keywordToString();
			}
		});

		keywordColumn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				StatusLine.setMessage(ConcordanceUIMessages.sortingKeywordColumn);
				LineComparator comparator = new LexicographicKeywordComparator();
				comparator.initialize(result.getCorpus());

				// add the corpus order key
				LineComparator cComparator = new CompositeComparator("current", Arrays.asList(comparator, standardComparatorClassName[0])); //$NON-NLS-1$
				viewerRight.getTable().setSortColumn(keywordColumn);
				if (viewerRight.getTable().getSortColumn() != keywordColumn) {
					viewerRight.getTable().setSortDirection(SWT.UP);
				}
				else if (viewerRight.getTable().getSortDirection() == SWT.UP) {
					viewerRight.getTable().setSortDirection(SWT.DOWN);
					cComparator = new ReverseComparator(cComparator);
				}
				else {
					viewerRight.getTable().setSortDirection(SWT.UP);
				}

				currentComparator = cComparator;

				complexsorter.setSimpleSortKey(1);// set ref key
				sort();
				StatusLine.setMessage(""); //$NON-NLS-1$
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		rightContextColumnViewer = new TableViewerColumn(viewerRight, SWT.LEFT);
		rightContextColumn = rightContextColumnViewer.getColumn();
		rightContextColumn.setText(ConcordanceUIMessages.rightContext);
		rightContextColumn.setToolTipText(ConcordanceUIMessages.rightContext);
		rightContextColumn.setAlignment(SWT.LEFT);
		rightContextColumnViewer.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {

				Line line = (Line) element;
				return line.rightContextToString();
			}
		});
		rightContextColumn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				StatusLine.setMessage(ConcordanceUIMessages.sortingRightContextColumn);
				LineComparator comparator = new LexicographicRightContextComparator();
				comparator.initialize(result.getCorpus());

				// add the corpus order key
				LineComparator cComparator = new CompositeComparator("current", Arrays.asList(comparator, standardComparatorClassName[0])); //$NON-NLS-1$

				viewerRight.getTable().setSortColumn(rightContextColumn);
				if (viewerRight.getTable().getSortColumn() != rightContextColumn) {

				}
				else if (viewerRight.getTable().getSortDirection() == SWT.UP) {
					viewerRight.getTable().setSortDirection(SWT.DOWN);
					cComparator = new ReverseComparator(cComparator);
				}
				else {
					viewerRight.getTable().setSortDirection(SWT.UP);
				}

				currentComparator = cComparator;

				complexsorter.setSimpleSortKey(3); // set ref key
				sort();
				StatusLine.setMessage(""); //$NON-NLS-1$
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		firstColumn = new TableViewerColumn(viewerRight, SWT.LEFT);
		firstColumn.getColumn().setWidth(0);
		firstColumn.getColumn().setResizable(false);
		firstColumn.setLabelProvider(new CellLabelProvider() {

			@Override
			public void update(ViewerCell cell) {
			}
		});

		leftContextColumn.setWidth(200);
		keywordColumn.setWidth(100);
		rightContextColumn.setWidth(200);

		// resetColumnWidths(); // set column widths


		// // And now, a sweet piece of code :)
		// // When the window is resized, the scrollbar is adjusted so that the
		// // keywords remain centered
		// viewer.getTable().addListener(SWT.Resize, new Listener() {
		// @Override
		// public void handleEvent(Event event) {
		// ScrollBar bar = viewer.getTable().getHorizontalBar();
		// //float l = leftContextColumn.getWidth();
		// //float k = keywordColumn.getWidth();
		// rightContextColumn.getWidth();
		// //float t = lineTableViewer.getTable().getClientArea().width;
		// bar.getMaximum();
		// bar.getThumb();
		// }
		// });

		// back to text mouse listener
		MouseListener ms = new MouseListener() {

			int c = 0;

			@Override
			public synchronized void mouseDoubleClick(MouseEvent e) {

				if (e.count == 2 && c == 0) {
					c++;

					StatusLine.setMessage(ConcordanceUIMessages.openingCorpusEdition);
					backToText();
					StatusLine.setMessage(""); //$NON-NLS-1$
					c--;
				}
			}

			@Override
			public void mouseDown(MouseEvent e) {
			}

			@Override
			public void mouseUp(MouseEvent e) {
			}
		};
		viewerRight.getTable().addMouseListener(ms);
		viewerLeft.getTable().addMouseListener(ms);

		// On Windows, the selection is gray if the table does not have focus.
		// To make both tables appear in focus, draw the selection background
		// here.
		// This part only works on version 3.2 or later.
		Listener eraseListener = new Listener() {

			@Override
			public void handleEvent(Event event) {

				event.detail &= ~SWT.HOT;
				if ((event.detail & SWT.SELECTED) != 0) {
					GC gc = event.gc;
					Rectangle rect = event.getBounds();
					gc.setForeground(displayArea.getDisplay().getSystemColor(SWT.COLOR_LIST_SELECTION_TEXT));
					gc.setBackground(displayArea.getDisplay().getSystemColor(SWT.COLOR_LIST_SELECTION));
					gc.fillRectangle(rect);
					event.detail &= ~SWT.SELECTED;
				}
			}
		};

		// Listeners to avoid 0 width columns
		referenceColumn.addControlListener(new ConcordanceColumnSizeControlListener(referenceColumn));
		leftContextColumn.addControlListener(new ConcordanceColumnSizeControlListener(leftContextColumn));
		keywordColumn.addControlListener(new ConcordanceColumnSizeControlListener(keywordColumn));
		rightContextColumn.addControlListener(new ConcordanceColumnSizeControlListener(rightContextColumn));

		viewerRight.getTable().addListener(SWT.EraseItem, eraseListener);

		TableUtils.installVerticalSynchronisation(viewerLeft, viewerRight);

		// set Layout datas
		FormData data = new FormData();
		data.top = new FormAttachment(0);
		data.bottom = new FormAttachment(100);
		data.left = new FormAttachment(0);
		data.right = new FormAttachment(sash, 0);
		viewerLeft.getTable().setLayoutData(data);

		FormData data2 = new FormData();
		data2.top = new FormAttachment(0, 0);
		data2.bottom = new FormAttachment(100, 0);
		data2.left = new FormAttachment(10);
		sash.setLayoutData(data2);

		FormData data3 = new FormData();
		data3.top = new FormAttachment(0);
		data3.bottom = new FormAttachment(100);
		data3.left = new FormAttachment(sash, 0);
		data3.right = new FormAttachment(100);
		viewerRight.getTable().setLayoutData(data3);

		createContextMenu();
	}

	/**
	 * Sort.
	 */
	public void sort() {

		if (result != null && currentComparator != null) {
			//			System.out.println("SORT !!!!");
			// System.out.println("concordance sort props: "+result.getLeftCAnalysisProperty()+", "+result.getKeywordAnalysisProperty()+", "+result.getRightCAnalysisProperty());
			//result.resetLines();

			try {
				Log.fine(NLS.bind(ConcordanceUIMessages.sortingWithP0, currentComparator.getName()));
				if (currentComparator instanceof CompositeComparator) {
					Log.fine(((CompositeComparator) currentComparator).getComparators().toString());
				}

				List<Line> sel = ConcordanceEditor.this.viewerRight.getStructuredSelection().toList();
				final HashSet<Integer> selectedPositions = new HashSet<Integer>();
				for (Line line : sel) {
					selectedPositions.add(line.matchGetStart());
				}

				JobHandler jobhandler = new JobHandler("Sorting...") { //$NON-NLS-1$

					@Override
					protected IStatus _run(SubMonitor monitor) throws Exception {

						try {
							result.sort(currentComparator, new TXMProgressMonitor(monitor));
							result.setTopIndex(0);

							syncExec(new Runnable() {

								@Override
								public void run() {

									StatusLine.setMessage(TXMUIMessages.sortDone);
									try {
										refresh(false);

										if (selectedPositions.size() > 0) {
											List<Line> newLines = (List<Line>) ConcordanceEditor.this.viewerRight.getInput();
											List<Line> newSelection = new ArrayList<Line>();

											for (Line l : newLines) {
												if (selectedPositions.remove(l.matchGetStart())) newSelection.add(l);
												if (selectedPositions.size() == 0) break;
											}
											ConcordanceEditor.this.viewerRight.setSelection(new StructuredSelection(newSelection));
											ConcordanceEditor.this.viewerLeft.setSelection(new StructuredSelection(newSelection));
										}
									}
									catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								}
							});

						}
						catch (Exception e) {
							Log.warning(NLS.bind(ConcordanceUIMessages.anErrorOccurredWhileRetrievingTheConcordanceLinesColonP0, e));
							throw e;
						}
						catch (Error e) {
							Log.warning(ConcordanceUIMessages.bind(ConcordanceUIMessages.fatalErrorColonP0, e));
							throw e;
						}
						return Status.OK_STATUS;
					}
				};

				jobhandler.startJob();
			}
			catch (Exception e) {
				System.err.println(NLS.bind(ConcordanceUIMessages.anErrorOccurredWhileRetrievingTheConcordanceLinesColonP0, e));
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
	}

	/**
	 * Lazy loads the concordance lines range and fills the display area.
	 *
	 */
	public void fillDisplayArea(boolean update) {

		if (viewerRight == null) {
			return;
		}

		int from = result.getTopIndex();
		from = Math.max(from, 0);

		int to = from + result.getNLinePerPage();
		to = Math.min(to, result.getNLines()) - 1;

		viewerRight.getControl().setRedraw(false);
		viewerLeft.getControl().setRedraw(false);

		List<Line> lines = null;
		if (result.getNLines() > 0) {
			try {
				lines = result.getLines(from, to);
			}
			catch (Exception e) {
				System.err.println(NLS.bind(ConcordanceUIMessages.anErrorOccurredWhileRetrievingTheConcordanceLinesColonP0, e));
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		else {
			lines = new ArrayList<>();
		}

		viewerRight.setInput(lines);
		viewerLeft.setInput(lines);

		navigationWidget.setInfoLineText("" + (from + 1), " - " + (to + 1) + " / " + result.getNLines()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ $NON-NLS-3$

		navigationWidget.setPreviousEnabled(result.getTopIndex() > 0);
		navigationWidget.setFirstEnabled(result.getTopIndex() > 0);
		navigationWidget.setNextEnabled((result.getTopIndex() + result.getNLinePerPage()) < result.getNLines() - 1);
		navigationWidget.setLastEnabled((result.getTopIndex() + result.getNLinePerPage()) < result.getNLines() - 1);
		navigationWidget.layout();
		navigationWidget.getParent().layout();

		refreshReferenceColumnTitle();
		viewerRight.getControl().setRedraw(true);
		viewerLeft.getControl().setRedraw(true);
	}

	/**
	 * Get the Composite that contains the query and sort forms
	 * 
	 * GridLayout with 4 columns
	 * 
	 * @return
	 */
	public Composite getQueryArea() {

		return queryArea;
	}

	/**
	 * Get the Composite that contains the navigation buttons
	 * 
	 * GridLayout with 7 columns
	 * 
	 * @return
	 */
	public Composite getNavigationArea() {

		return queryArea;
	}

	/**
	 * Compose control area.
	 *
	 * @param controlArea the control area
	 */
	private void composeControlArea(final Composite controlArea) {

		// Modified computing listeners to manage multi lines query widgets
		ComputeKeyListener computeKeyListener = new ComputeKeyListener(this) {

			@Override
			public void keyPressed(KeyEvent e) {

				if (queryWidget.getQuery() == null) return;
				if (!queryWidget.getQuery().getSearchEngine().hasMultiLineQueries()) super.keyPressed(e);
			}
		};

		GridLayout controlLayout = new GridLayout(1, false);
		controlLayout.verticalSpacing = 0;
		controlLayout.marginWidth = 0;
		controlLayout.marginHeight = 0;
		controlArea.setLayout(controlLayout);

		// Query Area: query itself + view properties
		queryArea = new Composite(controlArea, SWT.NONE);
		queryArea.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		queryArea.setLayout(new GridLayout(4, false));

		////////////////////////////////
		// composeAnnotationArea(controlArea);
		//////////////////////////////////

		navigationArea = new Composite(controlArea, SWT.NONE);
		navigationArea.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
		GridLayout navigationLayout = new GridLayout(7, false);
		navigationLayout.verticalSpacing = 0;
		navigationLayout.marginWidth = 0;
		navigationLayout.marginHeight = 0;
		navigationArea.setLayout(navigationLayout);
		///////////////////////////
		// | Query: [ (v)] [Search] |

		// Query:
		getMainParametersComposite().getLayout().numColumns = 2;
		// make
		getMainParametersComposite().setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));

		queryLabel = new Label(getMainParametersComposite(), SWT.NONE);
		queryLabel.setText(TXMUIMessages.ampQuery);
		queryLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

		// [ (v)]
		queryWidget = new AssistedChoiceQueryWidget(this.getMainParametersComposite(), SWT.DROP_DOWN | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL, result.getCorpus());
		queryWidget.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		queryWidget.addKeyListener(computeKeyListener);
		queryWidget.getQueryWidget().addModifyListener(computeKeyListener);

		// FIXME: SJ: for computing when changing the query from history combo box
		// FIXME: SJ: commented because it doesn't work, the computing is launched as soon as we click on the arrow of the combo box
		// queryWidget.getQueryWidget().addSelectionListener(new ComputeSelectionListener(this));

		// hide the parameters panel
		// getTopToolbar().setVisible(TXMEditor.COMPUTING_PARAMETERS_GROUP_ID, false); // the settings are not necessary to compute the concordance

		// | Properties: word/pos [Edit] |
		// propsArea = new PropertiesSelector<Property>(queryArea, SWT.NONE);
		// propsArea.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		// propsArea.setLayout(new GridLayout(3, false));
		// this.propsArea.setProperties(result.getAvailableKeywordViewProperties(), result.getKeywordViewProperties());
		// propsArea.addValueChangeListener(new Listener() {
		// @Override
		// public void handleEvent(Event event) {
		// result.setKeywordViewProperties(propsArea.getSelectedProperties());
		// refreshViewProperties();
		// }
		// });

		// [Search]
		// Button go = new Button(queryArea, SWT.PUSH);
		// go.setText(TXMUIMessages.SEARCH);
		// go.setTooltipText(TXMUIMessages.SEARCH);
		
		// Font f = go.getFont();
		// FontData defaultFont = f.getFontData()[0];
		// defaultFont.setStyle(SWT.BOLD);
		// Font newf = new Font(go.getDisplay(), defaultFont);
		// go.setFont(newf);
		//
		// go.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		// go.addSelectionListener(new ComputeSelectionListener(this));

		// Complex sort chooser (full line)
		complexsorter = new ComplexSortSelector(queryArea, SWT.NONE);
		complexsorter.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, true, 4, 1));

		// properties widget like in the portal version
		GLComposite propertiesPanel = new GLComposite(queryArea, SWT.BORDER, ConcordanceUIMessages.propertiesPanel);
		propertiesPanel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, true, 4, 1));
		propertiesPanel.getLayout().numColumns = 5;
		propertiesPanel.getLayout().makeColumnsEqualWidth = false;

		//ArrayList<WordProperty> tmp; // tmp to store properties

		try {
			Label l = new Label(propertiesPanel, SWT.NONE);
			l.setText(ConcordanceUIMessages.editor_21);
			l.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			l = new Label(propertiesPanel, SWT.NONE);
			l.setText(ConcordanceUIMessages.references);
			l.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, true, false));
			l = new Label(propertiesPanel, SWT.NONE);
			l.setText(ConcordanceUIMessages.left);
			l.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, true, false));
			l = new Label(propertiesPanel, SWT.NONE);
			l.setText(ConcordanceUIMessages.keyword);
			l.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, true, false));
			l = new Label(propertiesPanel, SWT.NONE);
			l.setText(ConcordanceUIMessages.right);
			l.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, true, false));

			l = new Label(propertiesPanel, SWT.NONE);
			l.setText(ConcordanceUIMessages.view);
			l.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			refViewProperties = new ReferencePatternSelector(propertiesPanel, SWT.NONE);
			refViewProperties.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			refViewProperties.setTitle(ConcordanceUIMessages.editor_21);
			//			refViewProperties.setButtonText(ConcordanceUIMessages.editor_19);
			refViewProperties.addSelectionListener(new ComputeSelectionListener(this) {

				@Override
				public void widgetSelected(SelectionEvent e) {

					result.setRefViewPattern(new ReferencePattern(refViewProperties.getSelectedProperties(), refViewProperties.getPattern()));
					super.widgetSelected(e);
				}
			});

			leftViewProperties = new PropertiesSelector<>(propertiesPanel, SWT.NONE);
			leftViewProperties.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			leftViewProperties.setTitle(ConcordanceUIMessages.editor_21);
			//			leftViewProperties.setButtonText(ConcordanceUIMessages.editor_19);
			leftViewProperties.addSelectionListener(new ComputeSelectionListener(this) {

				@Override
				public void widgetSelected(SelectionEvent e) {

					result.setLeftViewProperties(leftViewProperties.getSelectedProperties());
					result.setLeftAvailableViewProperties(leftViewProperties.getAvailableProperties());
					super.widgetSelected(e);
				}
			});
			keywordViewProperties = new PropertiesSelector<>(propertiesPanel, SWT.NONE);
			keywordViewProperties.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			keywordViewProperties.setTitle(ConcordanceUIMessages.editor_21);
			//			keywordViewProperties.setButtonText(ConcordanceUIMessages.editor_19);
			keywordViewProperties.addSelectionListener(new ComputeSelectionListener(this) {

				@Override
				public void widgetSelected(SelectionEvent e) {

					result.setKeywordViewProperties(keywordViewProperties.getSelectedProperties());
					result.setKeywordAvailableViewProperties(keywordViewProperties.getAvailableProperties());
					super.widgetSelected(e);
				}
			});
			rightViewProperties = new PropertiesSelector<>(propertiesPanel, SWT.NONE);
			rightViewProperties.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			rightViewProperties.setTitle(ConcordanceUIMessages.editor_21);
			//			rightViewProperties.setButtonText(ConcordanceUIMessages.editor_19);
			rightViewProperties.addSelectionListener(new ComputeSelectionListener(this) {

				@Override
				public void widgetSelected(SelectionEvent e) {

					result.setRightViewProperties(rightViewProperties.getSelectedProperties());
					result.setRightAvailableViewProperties(rightViewProperties.getAvailableProperties());
					super.widgetSelected(e);
				}
			});

			l = new Label(propertiesPanel, SWT.NONE);
			l.setText(ConcordanceUIMessages.sort);
			l.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			refSortProperties = new PropertiesSelector<>(propertiesPanel, SWT.NONE);
			refSortProperties.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			refSortProperties.setTitle(ConcordanceUIMessages.editor_21);
			//			refSortProperties.setButtonText(ConcordanceUIMessages.editor_19);
			refSortProperties.addSelectionListener(new ComputeSelectionListener(this) {

				@Override
				public void widgetSelected(SelectionEvent e) {

					result.setRefAnalysePattern(new ReferencePattern(refSortProperties.getSelectedProperties()));
					super.widgetSelected(e);
				}
			});
			leftSortProperties = new PropertiesSelector<>(propertiesPanel, SWT.NONE);
			leftSortProperties.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			leftSortProperties.setTitle(ConcordanceUIMessages.editor_21);
			//			leftSortProperties.setButtonText(ConcordanceUIMessages.editor_19);
			leftSortProperties.addSelectionListener(new ComputeSelectionListener(this) {

				@Override
				public void widgetSelected(SelectionEvent e) {

					result.setLeftAnalysisProperties(leftSortProperties.getSelectedProperties());
					result.setLeftAvailableAnalysisProperties(leftSortProperties.getSelectedProperties());
					super.widgetSelected(e);
				}
			});
			keywordSortProperties = new PropertiesSelector<>(propertiesPanel, SWT.NONE);
			keywordSortProperties.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			keywordSortProperties.setTitle(ConcordanceUIMessages.editor_21);
			//			keywordSortProperties.setButtonText(ConcordanceUIMessages.editor_19);
			keywordSortProperties.addSelectionListener(new ComputeSelectionListener(this) {

				@Override
				public void widgetSelected(SelectionEvent e) {

					result.setKeywordAnalysisProperties(keywordSortProperties.getSelectedProperties());
					result.setKeywordAvailableAnalysisProperties(keywordSortProperties.getSelectedProperties());
					super.widgetSelected(e);
				}
			});
			rightSortProperties = new PropertiesSelector<>(propertiesPanel, SWT.NONE);
			rightSortProperties.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			rightSortProperties.setTitle(ConcordanceUIMessages.editor_21);
			//			rightSortProperties.setButtonText(ConcordanceUIMessages.editor_19);
			rightSortProperties.addSelectionListener(new ComputeSelectionListener(this) {

				@Override
				public void widgetSelected(SelectionEvent e) {

					result.setRightAnalysisProperties(rightSortProperties.getSelectedProperties());
					result.setRightAvailableAnalysisProperties(rightSortProperties.getSelectedProperties());
					super.widgetSelected(e);
				}
			});

			l = new Label(propertiesPanel, SWT.NONE);
			l.setText(ConcordanceUIMessages.size);
			l.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			new Label(propertiesPanel, SWT.NONE);
			leftSizeSpinner = new Spinner(propertiesPanel, SWT.BORDER);
			leftSizeSpinner.setToolTipText(ConcordanceUIMessages.numberOfTokensDisplayedBeforeTheFirstPivotToken);
			leftSizeSpinner.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			leftSizeSpinner.addSelectionListener(new ComputeSelectionListener(this) {

				@Override
				public void widgetSelected(SelectionEvent e) {

					result.setLeftContextSize(leftSizeSpinner.getSelection());
					super.widgetSelected(e);
				}
			});
			leftSizeSpinner.addKeyListener(new KeyListener() {

				@Override
				public void keyReleased(KeyEvent e) {

					if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
						result.setLeftContextSize(leftSizeSpinner.getSelection());
						ConcordanceEditor.this.compute(true);
					}
				}

				@Override
				public void keyPressed(KeyEvent e) {
				}
			});
			new Label(propertiesPanel, SWT.NONE);
			rightSizeSpinner = new Spinner(propertiesPanel, SWT.BORDER);
			rightSizeSpinner.setToolTipText(ConcordanceUIMessages.numberOfTokensDisplayedAfterTheLastPivotToken);
			rightSizeSpinner.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			rightSizeSpinner.addSelectionListener(new ComputeSelectionListener(this) {

				@Override
				public void widgetSelected(SelectionEvent e) {

					result.setRightContextSize(rightSizeSpinner.getSelection());
					super.widgetSelected(e);
				}
			});
			rightSizeSpinner.addKeyListener(new KeyListener() {

				@Override
				public void keyReleased(KeyEvent e) {

					if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
						result.setRightContextSize(rightSizeSpinner.getSelection());
						ConcordanceEditor.this.compute(true);
					}
				}

				@Override
				public void keyPressed(KeyEvent e) {
				}
			});
		}
		catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Delete line button
		deleteLineButton = new ToolItem(getTopToolbar(), SWT.PUSH);
		//		deleteLineButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		deleteLineButton.setToolTipText(ConcordanceUIMessages.deleteSelectedLines);
		deleteLineButton.setImage(IImageKeys.getImage(ConcordanceEditor.class, "icons/concordance_line_delete.png")); //$NON-NLS-1$ //$NON-NLS-2$
		deleteLineButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				DeleteLines.deleteConcordanceLines(ConcordanceEditor.this);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		//		Label l = new Label(navigationArea, SWT.NONE);
		//		l.setText("Target strategy");
		//		l.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		//		
		//		targetStrategyCombo = new Combo(navigationArea, SWT.READ_ONLY);
		//		targetStrategyCombo.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		//		targetStrategyCombo.setItems(Concordance.TARGET_SELECT, Concordance.TARGET_SHOW, Concordance.TARGET_KEEPLEFT, Concordance.TARGET_KEEPRIGHT);
		//		targetStrategyCombo.select(0);

		// Navigation Area: infoLine and buttons
		GLComposite navigationAreaComposite = getBottomToolbar().installGLComposite(ConcordanceUIMessages.navigation, 1, false);
		navigationAreaComposite.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false));

		navigationWidget = new NavigationWidget(navigationAreaComposite, SWT.NONE);
		navigationWidget.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false));

		// | [|<] [<] infos [>] [>|] |
		// [|<]
		navigationWidget.addFirstListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			@Override
			public void widgetSelected(SelectionEvent arg0) {

				result.setTopIndex(0);
				try {
					refresh(false);
					viewerRight.getTable().setTopIndex(0);
				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		// [<]
		navigationWidget.addPreviousListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {

				int top = result.getTopIndex() - result.getNLinePerPage();
				if (top < 0)
					top = 0;
				result.setTopIndex(top);
				try {
					refresh(false);
					viewerRight.getTable().setTopIndex(0);
				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		// [>]
		navigationWidget.addNextListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {

				result.setTopIndex(result.getTopIndex() + result.getNLinePerPage());
				try {
					refresh(false);
					viewerRight.getTable().setTopIndex(0);
				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		// [>|]
		navigationWidget.addLastListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {

				int top = (result.getNLines() / result.getNLinePerPage()) * result.getNLinePerPage();
				// FIXME: SJ: became useless?
				// int bottom = top + result.getNLinePerPage() -1;
				result.setTopIndex(top);
				try {
					refresh(false);
					viewerRight.getTable().setTopIndex(0);
				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		queryArea.layout();
	}

	@Override
	protected void firstInitializeEditorFieldsFromResult(boolean update) throws CqiClientException {
		refViewProperties.setProperties(ReferencePattern.getAvailableProperties(getCorpus()));

		ArrayList<WordProperty> tmp = new ArrayList<>(getCorpus().getOrderedProperties());
		tmp.addAll(getCorpus().getVirtualProperties());
		leftViewProperties.setProperties(tmp);

		tmp = new ArrayList<>(getCorpus().getOrderedProperties());
		tmp.addAll(getCorpus().getVirtualProperties());
		keywordViewProperties.setProperties(tmp);

		tmp = new ArrayList<>(getCorpus().getOrderedProperties());
		tmp.addAll(getCorpus().getVirtualProperties());
		rightViewProperties.setProperties(tmp);

		refSortProperties.setProperties(ReferencePattern.getAvailableProperties(getCorpus()));

		tmp = new ArrayList<>(getCorpus().getOrderedProperties());
		tmp.addAll(getCorpus().getVirtualProperties());
		leftSortProperties.setProperties(tmp);

		tmp = new ArrayList<>(getCorpus().getOrderedProperties());
		tmp.addAll(getCorpus().getVirtualProperties());
		keywordSortProperties.setProperties(tmp);

		tmp = new ArrayList<>(getCorpus().getOrderedProperties());
		tmp.addAll(getCorpus().getVirtualProperties());
		rightSortProperties.setProperties(tmp);
	}

	/**
	 * refresh
	 * 
	 * @param update
	 */
	@Override
	public void updateEditorFromResult(boolean update) {

		StatusLine.setMessage(ConcordanceUIMessages.startComputingConcordance);

		try {
			QueriesView.refresh();

			//System.out.println("lines="+result.getLines());

			this.fillDisplayArea(update);

			//			if (update) {
			//				//System.out.println(this.currentComparator);
			//				if (currentComparator instanceof CompositeComparator) {
			//					CompositeComparator cc = (CompositeComparator)currentComparator;
			//					boolean allDefault = true;
			//					for (LineComparator ccc : cc.getComparators()) {
			//						allDefault = allDefault && ccc instanceof WordPositionComparator;
			//					}
			//					
			//					if (!allDefault) {
			//						this.sort();
			//					}
			//				} else {
			//					this.sort();
			//				}
			//			}

			boolean b = result.getQueryResultParameter() == null;
			this.queryWidget.setEnabled(b); // disable the widget if a QueryResult was provided

			this.queryWidget.memorize();

			if (getResult().isDirty()) { // the result compute failed

				SearchEngine se = getResult().getQuery().getSearchEngine();
				if (se instanceof CQPSearchEngine) {
					try {
						String error = CQPSearchEngine.getCqiClient().getLastCQPError();
						if (error.contains("undefined") ||  //$NON-NLS-1$
								error.contains("Illegal regular expression") ||  //$NON-NLS-1$
								error.contains("syntax error") ||  //$NON-NLS-1$
								error.contains("does not exist")) { //$NON-NLS-1$
							if (queryWidgetInitBackground == null) queryWidgetInitBackground = queryWidget.getBackground();
							queryWidget.setBackground(new Color(queryWidget.getDisplay(), 255, 0, 0, 30));
						}
					}
					catch (Exception e1) {
						e1.printStackTrace();
					}
				}

				fillDisplayArea(true);
			}
			else {
				if (queryWidgetInitBackground != null) queryWidget.setBackground(queryWidgetInitBackground);
			}

			if (update || !resetColumnWidthsCalled) {
				resetColumnWidths();
			}
		}
		catch (Exception e1) {
			Log.warning(NLS.bind(ConcordanceUIMessages.errorWhileComputingTheConcordanceColonP0, Log.toString(e1)));
			Log.printStackTrace(e1);
			try {
				Log.warning(TXMCoreMessages.bind(CQPSearchEngineCoreMessages.lastCQPErrorP0, CQPSearchEngine.getCqiClient().getLastCQPError()));
			}
			catch (Exception e2) {
				Log.severe(NLS.bind(CQPSearchEngineCoreMessages.cQPErrorColonP0, e2));
				Log.printStackTrace(e2);
			}
		}
	}

	/**
	 * Sets the line per page.
	 *
	 * @param nb the new line per page
	 */
	public void setLinePerPage(int nb) {

		result.setNLinePerPage(nb);
		try {
			this.refresh(false);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Gets the line per page.
	 *
	 * @return the line per page
	 */
	public int getLinePerPage() {

		return result.getNLinePerPage();
	}

	// /**
	// * Update view properties label.
	// */
	// void updateViewPropertiesLabel() {
	// String str = StringUtils.join(result.getKeywordViewProperties(), "_");
	// viewPropertiesLabel.setText(str);
	// viewPropertiesLabel.getParent().layout();
	// viewPropertiesLabel.update();
	// queryArea.layout();
	// }

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#getTitleToolTip()
	 */
	@Override
	public String getTitleToolTip() {

		return result.getDetails();
	}

	/**
	 * Gets the reference pattern.
	 *
	 * @return the reference pattern
	 */
	ReferencePattern getViewReferencePattern() {

		return result.getRefViewPattern();
	}

	/**
	 * Gets the reference pattern.
	 *
	 * @return the reference pattern
	 */
	ReferencePattern getSortReferencePattern() {

		return result.getRefAnalysePattern();
	}

	/**
	 * Sets the left sort properties.
	 *
	 * @param list the new left sort properties
	 */
	public void setLeftSortProperties(List<WordProperty> list) {

		result.setLeftAnalysisProperties(list);
		sort();
	}

	/**
	 * Sets the keyword sort properties.
	 *
	 * @param list the new keyword sort property
	 */
	public void setKeywordSortProperties(List<WordProperty> list) {

		result.setAnalysisProperty(list);
		sort();
	}

	/**
	 * Sets the right sort properties.
	 *
	 * @param list the new right sort properties
	 */
	public void setRightSortProperties(List<WordProperty> list) {

		result.setAnalysisProperty(list);
		sort();
	}

	/**
	 * Sets the view reference pattern.
	 *
	 * @param referencePattern the new reference pattern
	 */
	public void setRefViewPattern(ReferencePattern referencePattern) {

		result.setRefViewPattern(referencePattern);
		sort();
		this.resetLeftTableColumnWidths();
		getResultArea().layout(true, true);
	}

	/**
	 * Sets the sort reference pattern.
	 *
	 * @param referencePattern the new reference pattern
	 */
	public void setRefSortPattern(ReferencePattern referencePattern) {

		result.setRefAnalysePattern(referencePattern);
		sort();
	}

	/**
	 * Sets the context size.
	 *
	 * @param leftContextSize the left context size
	 * @param rightContextSize the right context size
	 */
	public void setContextSize(int leftContextSize, int rightContextSize) {

		result.setLeftContextSize(leftContextSize);
		result.setRightContextSize(rightContextSize);
		this.rightSizeSpinner.setSelection(rightContextSize);
		this.leftSizeSpinner.setSelection(leftContextSize);
		try {
			refresh(false);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // we return to the previously
		resetColumnWidths();
	}

	/**
	 * Creates the context menu.
	 */
	private void createContextMenu() {

		createActions();

		// store the mouse position
		viewerRight.getTable().addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDown(MouseEvent arg0) {

				if (arg0.button == 3) {
					mouseposition = new Point(arg0.x, arg0.y);
				}
				// System.out.println("arg.x="+arg0.x);
				// System.out.println("arg.y="+arg0.y);
				// System.out.println("hbar: "+lineTableViewer.getTable().getHorizontalBar().getSelection());
				// System.out.println("vbar: "+lineTableViewer.getTable().getVerticalBar().getSelection());
				// System.out.println("Col: "+getPointedColumn());
			}
		});

		MenuManager menuManager = new MenuManager(); // $NON-NLS-1$
		Menu menu = menuManager.createContextMenu(viewerRight.getTable());
		menuManager.setRemoveAllWhenShown(true);
		menuManager.addMenuListener(new IMenuListener() {

			@Override
			public void menuAboutToShow(IMenuManager manager) {

				IWorkbenchPartSite site = ConcordanceEditor.this.getSite();
				site.getPage().activate(ConcordanceEditor.this);
				manager.add(setViewPropertyAction);
				manager.add(setSortPropertyAction);
				manager.add(new Separator());
				manager.add(setContextSizeAction);
				manager.add(setLineNumberAction);
				manager.add(new Separator());
			}
		});
		viewerRight.getTable().setMenu(menu);

		MenuManager menuManagerLeft = new MenuManager(); // $NON-NLS-1$
		Menu menuLeft = menuManagerLeft.createContextMenu(viewerLeft.getTable());
		menuManagerLeft.setRemoveAllWhenShown(true);
		menuManagerLeft.addMenuListener(new IMenuListener() {

			@Override
			public void menuAboutToShow(IMenuManager manager) {

				IWorkbenchPartSite site = ConcordanceEditor.this.getSite();
				site.getPage().activate(ConcordanceEditor.this);
				manager.add(defineViewReferencePatternAction);
				manager.add(defineSortReferencePatternAction);
			}
		});
		viewerLeft.getTable().setMenu(menuLeft);

		// viewer.getTable().setMenu(menuManager.createContextMenu(viewer.getTable()));
		// register the right menu to the editor site
		getSite().registerContextMenu(menuManager, viewerRight);
		getSite().registerContextMenu(menuManagerLeft, viewerLeft);
		getSite().setSelectionProvider(viewerRight);
	}

	/**
	 * Gets the mouse position.
	 *
	 * @return the mouse position
	 */
	private Point getMousePosition() {

		return this.mouseposition;
	}

	/**
	 * return the column under the mouse pointer.
	 * TODO this method wont work if columns are added before the keyword column
	 * 
	 * @return the pointed column
	 */
	public int getPointedColumn() {

		Point p = getMousePosition();
		if (p == null) return 0;

		int x = p.x; // + lineTableViewer.getTable().get;
		int sumWidthColumn = 0;

		sumWidthColumn += this.leftContextColumn.getWidth();
		if (x < sumWidthColumn)
			return 1;

		sumWidthColumn += this.keywordColumn.getWidth();
		if (x < sumWidthColumn)
			return 2;

		// sumWidthColumn += this.rightContextColumn.getWidth();
		// if (x < sumWidthColumn)
		return 3; // last column
	}

	/**
	 * Creates the actions.
	 */
	private void createActions() {

		setSortPropertyAction = new SortPropertySelection(getSite().getWorkbenchWindow(), this, this.viewerRight);
		setContextSizeAction = new SetContextSize(getSite().getWorkbenchWindow(), this);
		// resetColumnWidths = new ResetColumnWidths(getSite().getWorkbenchWindow(), this);
		setLineNumberAction = new SetLineNumber(getSite().getWorkbenchWindow(), this);
		defineViewReferencePatternAction = new DefineViewReferencePattern(getSite().getWorkbenchWindow(), this);
		defineSortReferencePatternAction = new DefineSortReferencePattern(getSite().getWorkbenchWindow(), this);
		setViewPropertyAction = new ViewPropertySelection(getSite().getWorkbenchWindow(), this, this.viewerRight);
	}

	/**
	 * Gets the available sort properties.
	 *
	 * @return the available sort properties
	 */
	public List<WordProperty> getAvailableSortProperties() {

		return result.getAvailableKeywordSortProperties();
	}

	/**
	 * Gets the user defined comparators.
	 *
	 * @return the user defined comparators
	 */
	public List<LineComparator> getUserDefinedComparators() {

		return userDefinedComparators;
	}

	/**
	 * Sets the user defined comparators.
	 *
	 * @param userDefinedComparators the new user defined comparators
	 */
	public void setUserDefinedComparators(List<LineComparator> userDefinedComparators) {

		this.userDefinedComparators = userDefinedComparators;
	}

	/**
	 * Sets the current comparator.
	 *
	 * @param comparator the new current comparator
	 */
	public void setCurrentComparator(LineComparator comparator) {

		this.currentComparator = comparator;
	}

	/**
	 * Gets the standard comparators.
	 *
	 * @return the standard comparators
	 */
	public List<LineComparator> getStandardComparators() {

		return standardComparators;
	}

	/**
	 * Gets the top line.
	 *
	 * @return the top line
	 */
	public int getTopLine() {

		return result.getTopIndex();
	}

	/**
	 * Gets the bottom line.
	 *
	 * @return the bottom line
	 */
	public int getBottomLine() {

		return result.getTopIndex() + result.getNLinePerPage() - 1;
	}

	/**
	 * Gets the left context size.
	 *
	 * @return the left context size
	 */
	public int getLeftContextSize() {

		return result.getLeftContextSize();
	}

	/**
	 * Gets the right context size.
	 *
	 * @return the right context size
	 */
	public int getRightContextSize() {

		return result.getRightContextSize();
	}

	/**
	 * Gets the corpus.
	 *
	 * @return the corpus
	 */
	public CQPCorpus getCorpus() {

		return result.getCorpus();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void _setFocus() {

		if (this.queryWidget == null) return;
		this.queryWidget.setFocus();
	}

	/**
	 * refresh column display accordingly to the views properties.
	 */
	private void refreshViewProperties() {

		result.resetLines();
		sort();
	}

	/**
	 * Refresh sort properties.
	 */
	private void refreshSortProperties() {

		result.resetLines();
		sort();
	}

	/**
	 * Sets the left c view properties.
	 *
	 * @param selectedViewProperties the new left c view properties
	 */
	public void setLeftViewProperties(List<WordProperty> selectedViewProperties) {

		result.setLeftViewProperties(selectedViewProperties);
		refreshViewProperties();
	}

	/**
	 * Sets the right c view properties.
	 *
	 * @param selectedViewProperties the new right c view properties
	 */
	public void setRightViewProperties(List<WordProperty> selectedViewProperties) {

		result.setRightViewProperties(selectedViewProperties);
		refreshViewProperties();
	}

	/**
	 * Sets the keyword view properties.
	 *
	 * @param selectedViewProperties the new keyword view properties
	 */
	public void setKeywordViewProperties(List<WordProperty> selectedViewProperties) {

		result.setKeywordViewProperties(selectedViewProperties);
		refreshViewProperties();
		// updateViewPropertiesLabel();
	}

	/**
	 * Refresh reference column title.
	 */
	public void refreshReferenceColumnTitle() {
		if (result.getRefViewPattern() != null) {
			referenceColumn.setText(result.getRefViewPattern().getTitle());
		}
		else {
			referenceColumn.setText(ConcordanceUIMessages.References);
		}
	}

	/**
	 * Gets the selected left c view properties.
	 *
	 * @return the selected left c view properties
	 */
	public List<WordProperty> getSelectedLeftViewProperties() {

		return result.getLeftViewProperties();
	}

	/**
	 * Gets the selected keyword view properties.
	 *
	 * @return the selected keyword view properties
	 */
	public List<WordProperty> getSelectedKeywordViewProperties() {

		return result.getKeywordViewProperties();
	}

	/**
	 * Gets the selected right c view properties.
	 *
	 * @return the selected right c view properties
	 */
	public List<WordProperty> getSelectedRightViewProperties() {

		return result.getRightViewProperties();
	}

	/**
	 * Gets the selected left c sort properties.
	 *
	 * @return the selected left c sort properties
	 */
	public List<WordProperty> getSelectedLeftSortProperties() {

		return result.getLeftAnalysisProperties();
	}

	/**
	 * Gets the selected keyword sort properties.
	 *
	 * @return the selected keyword sort properties
	 */
	public List<WordProperty> getSelectedKeywordSortProperties() {

		return result.getKeywordAnalysisProperties();
	}

	/**
	 * Gets the selected right c sort properties.
	 *
	 * @return the selected right c sort properties
	 */
	public List<WordProperty> getSelectedRightSortProperties() {

		return this.result.getRightAnalysisProperties();
	}

	/**
	 * Gets the available left c view properties.
	 *
	 * @return the available left c view properties
	 */
	public List<WordProperty> getAvailableLeftViewProperties() {

		return this.result.getAvailableLeftViewProperties();
	}

	/**
	 * Gets the available keyword view properties.
	 *
	 * @return the available keyword view properties
	 */
	public List<WordProperty> getAvailableKeywordViewProperties() {

		return this.result.getAvailableKeywordViewProperties();
	}

	/**
	 * Gets the available right c view properties.
	 *
	 * @return the available right c view properties
	 */
	public List<WordProperty> getAvailableRightViewProperties() {

		return this.result.getAvailableRightViewProperties();
	}

	/**
	 * Gets the available left sort properties.
	 *
	 * @return the available left sort properties
	 */
	public List<WordProperty> getAvailableLeftSortProperties() {

		return this.result.getAvailableLeftSortProperties();
	}

	/**
	 * Gets the available keyword sort properties.
	 *
	 * @return the available keyword sort properties
	 */
	public List<WordProperty> getAvailableKeywordSortProperties() {

		return this.result.getAvailableKeywordSortProperties();
	}

	/**
	 * Gets the available right sort properties.
	 *
	 * @return the available right sort properties
	 */
	public List<WordProperty> getAvailableRightSortProperties() {

		return this.result.getAvailableRightSortProperties();
	}

	/**
	 * Gets the selected references.
	 *
	 * @return the selected references
	 */
	public List<Property> getSelectedViewReferences() {

		return this.result.getRefViewPattern().getProperties();
	}

	/**
	 * Gets the selected references.
	 *
	 * @return the selected references
	 */
	public List<Property> getSelectedSortReferences() {

		return this.result.getRefAnalysePattern().getProperties();
	}

	/**
	 * Gets the line table viewer.
	 *
	 * @return the line table viewer
	 */
	public TableViewer getTableViewer() {

		return viewerRight;
	}

	/**
	 * Gets the line table viewer.
	 *
	 * @return the line table viewer
	 */
	public TableViewer getReferenceTableViewer() {

		return viewerLeft;
	}

	/**
	 * Reset sorted column.
	 *
	 * @param col the col
	 */
	public void resetSortedColumn(int col) {

		switch (col) {
			case 1:// keyword
				viewerRight.getTable().setSortColumn(this.keywordColumn);
				viewerRight.getTable().setSortDirection(SWT.DOWN);
				break;
			case 2:// left
				viewerRight.getTable().setSortColumn(this.leftContextColumn);
				viewerRight.getTable().setSortDirection(SWT.DOWN);
				break;
			case 3:// right
				viewerRight.getTable().setSortColumn(this.rightContextColumn);
				viewerRight.getTable().setSortDirection(SWT.DOWN);
				break;
			case 4:// references
				viewerRight.getTable().setSortColumn(this.referenceColumn);
				viewerRight.getTable().setSortDirection(SWT.DOWN);
				break;
			default:
				viewerRight.getTable().setSortColumn(null);
				viewerRight.getTable().setSortDirection(SWT.NONE);
		}

	}

	/*
	 * /**
	 * Back to text.
	 * @SuppressWarnings("restriction") //$NON-NLS-1$
	 * public void backToText() {
	 * try {
	 * StructuredSelection select = (StructuredSelection) viewer.getSelection();
	 * if (select == null) return;
	 * Line line = (Line) select.getFirstElement();
	 * if (line == null) return;
	 * StructuralUnit textS = null;
	 * textid = ""; //$NON-NLS-1$
	 * String baseid = ""; //$NON-NLS-1$
	 * String projectid = ""; //$NON-NLS-1$
	 * try {
	 * textS = corpus.getStructuralUnit("text"); //$NON-NLS-1$
	 * Property textP = textS.getProperty("id"); //$NON-NLS-1$
	 * textid = line.getMatch().getValueForProperty(textP);// get text via text struc property id
	 * // Log.info("Corpus' base ("+baseid+") : "+corpus.getParent());
	 * } catch (Exception e2) {
	 * System.out.println(Messages.ConcordancesEditor_8);
	 * org.txm.utils.logger.Log.printStackTrace(e2);
	 * return;
	 * }
	 * Log.info(Messages.ConcordancesEditor_20);
	 * final List<String> wordids = result.getKeywordsId(
	 * textid, getTopLine(), getBottomLine() + 1);
	 * lineids = null;
	 * try {
	 * lineids = line.getMatch().getValuesForProperty(
	 * corpus.getProperty("id")); //$NON-NLS-1$
	 * } catch (Exception e1) {
	 * org.txm.utils.logger.Log.printStackTrace(e1);
	 * return;
	 * }
	 * Log.info(NLS.bind(Messages.ConcordancesEditor_26,
	 * new Object[]{projectid, baseid, textid}));
	 * Text text = corpus.getMainCorpus().getText(textid);
	 * if (text == null) {
	 * System.out.println(NLS.bind(Messages.ConcordancesEditor_27, textid));
	 * StatusLine.setMessage(NLS.bind(Messages.ConcordancesEditor_27, textid));
	 * Log.severe(NLS.bind(Messages.ConcordancesEditor_27, textid));
	 * return;
	 * }
	 * Edition edition = text.getEdition(corpus.getMainCorpus().getDefaultEdition());
	 * if (edition == null) {
	 * System.out.println(Messages.ConcordancesEditor_30);
	 * StatusLine.setMessage(Messages.ConcordancesEditor_30);
	 * Log.severe(Messages.ConcordancesEditor_30);
	 * return;
	 * }
	 * String line_wordid = lineids.get(0);
	 * if (line_wordid == null) {
	 * System.out.println(Messages.ConcordancesEditor_9+edition+Messages.ConcordancesEditor_10);
	 * return;
	 * }
	 * openedPage = edition.getPageForWordId(line_wordid);
	 * if (openedPage != null) {
	 * // System.out.println(page.getFile());
	 * if (attachedBrowserEditor == null || attachedBrowserEditor.isDisposed()) {
	 * attachedBrowserEditor = OpenBrowser.openEdition(openedPage.getFile().getAbsolutePath(), openedPage.getFile().getName());
	 * if (attachedBrowserEditor != null) {
	 * attachedBrowserEditor.setEdition(edition);
	 * } else {
	 * System.out.println("Fail to create Edition editor.");
	 * return;
	 * }
	 * }
	 * if (attachedBrowserEditor != null) {
	 * attachedBrowserEditor.showPage(openedPage);
	 * attachedBrowserEditor.setWordsIds(wordids, lineids);
	 * IWorkbenchPage attachedPage = attachedBrowserEditor.getEditorSite().getPage();
	 * attachedPage.activate(attachedBrowserEditor);
	 * }
	 * } else
	 * System.out.println(NLS.bind(Messages.ConcordancesEditor_29, wordids.get(0)));
	 * } catch (Exception err) {
	 * Log.severe(NLS.bind(Messages.ConcordancesEditor_46, err));
	 * Log.printStackTrace(err);
	 * }
	 * }
	 */

	/**
	 * Gets the current comparator used for sorting lines.
	 * 
	 * @return the currentComparator
	 */
	public LineComparator getCurrentComparator() {

		return currentComparator;
	}

	/**
	 * Gets the number of pages.
	 * 
	 * @return
	 */
	public int getPagesCount() {

		return (int) Math.ceil(((double) this.result.getNLines() / this.result.getNLinePerPage()));
	}

	public void setQuery(String query) {

		queryWidget.setText(query);
	}

	/**
	 * Open the edition for the selected line.
	 * The command used to show the edition is an EditionBackToText Extension
	 */
	public boolean backToText() {

		StructuredSelection select = (StructuredSelection) viewerRight.getSelection();
		if (select == null) return false;

		Line line = (Line) select.getFirstElement();
		if (line == null) return false;

		try {
			IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(BACKTOTEXT_COMMAND_ID);
			for (IConfigurationElement e : config) {
				Object o = e.createExecutableExtension("class"); // //$NON-NLS-1$
				if (o instanceof BackToTextCommand) {
					return ((BackToTextCommand) o).backToText(this, line.getMatch());
				}
			}
		}
		catch (CoreException e1) {
			e1.printStackTrace();
		}

		return false;

		// try {
		// StructuredSelection select = (StructuredSelection) viewer.getSelection();
		// if (select == null) return;
		// Line line = (Line) select.getFirstElement();
		// if (line == null) return;
		// org.txm.rcp.synoptic.commands.BackToText.backToText(line);
		// } catch(Exception err) {
		// System.out.println("Error while calling backtotext: "+err);
		// Log.printStackTrace(err);
		// }
	}

	public void resetLeftTableColumnWidths() {

		int refMax = referenceColumn.getText().length();
		for (TableItem item : viewerLeft.getTable().getItems()) {
			if (refMax < item.getText(0).length()) refMax = item.getText(0).length();
		}

		if (refMax > TableUtils.MAX_CHARS_LENGTH_TO_SHOW) {
			refMax = TableUtils.MAX_CHARS_LENGTH_TO_SHOW;
		}

		// System.out.println("Size="+queryLabel.getSize().x +" label="+queryLabel.getText()+" l size"+ queryLabel.getText().length());
		float X = queryLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
		float W = 1f + X / queryLabel.getText().length();
		// System.out.println("W= "+W+" Maxs "+refMax+ " " +leftMax+" " +keywordMax+" "+rightMax);

		if (sash == null) return; // there was a problem during initialization
		if (sash.getLayoutData() == null) return; // there was a problem during initialization

		((FormData) sash.getLayoutData()).left = new FormAttachment(0, 25 + (int) (refMax * W));

		int s = 2 * (int) (refMax * W);
		referenceColumn.setWidth(s);
		//		System.out.println("REFSIZE="+s);
	}

	/**
	 * @return the number of lines in the table
	 */
	public int resetRightTableColumnWidths() {

		TableUtils.packColumns(viewerRight, false, true);
		return 1;
		//		//float W = 1f + queryLabel.getSize().x / (float) queryLabel.getText().length(); // does nto work until the label is drawn
		//		float W = queryLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT).x / (float) queryLabel.getText().length();
		//		
		//		
		//		int n = 0;
		//		TableColumn[] columns = viewerRight.getTable().getColumns();
		//		for (int i = 0; i < columns.length; i++) {
		//			TableColumn column = columns[i];
		//			if (column.getWidth() == 0) continue; // skip closed columns
		//			
		//			//System.out.println("initial max of '"+column.getText()+"' = "+max);
		//			if (column.getText().length() == 0) continue; // skip no-title columns
		//			
		//			int max = 0;
		//			n = 0;
		//			for (TableItem item : viewerRight.getTable().getItems()) {
		//				if (max < item.getText(i).length()) {
		//					max = item.getText(i).length();
		//					//System.out.println("update max with '"+item.getText(i)+"' at "+i+" = "+max);
		//				}
		//				if (n++ > 100) break; // stop testing after 100 lines
		//			}
		//			
		//			if (viewerRight.getTable().getItems().length == 0) {
		//				max = column.getText().length();
		//			}
		//			
		//			if (max > MAX_CHARS_LENGTH_TO_SHOW) {
		//				max = MAX_CHARS_LENGTH_TO_SHOW;
		//			}
		//			
		//			int s = 15 + (int) (max * W);
		//			column.setWidth(s);
		//			//System.out.println(column.getText()+"SIZE="+s);
		//		}
		//		
		//		viewerRight.getTable().layout(true, true);
		//		
		//		tableResizeListener.handleEvent(null); // center on keyword column
		//		return n;
	}

	boolean resetColumnWidthsCalled = false;

	/**
	 * Computes the column widths using the max length of each column (title+lines)
	 * 
	 * Use this method after recomputing a result
	 * 
	 * The characters width is estimated using the queryLabel length
	 * 
	 * @return the number of lines in the table
	 */
	public int resetColumnWidths() {

		if (!result.hasBeenComputedOnce()) return 0;

		resetColumnWidthsCalled = true;
		resetLeftTableColumnWidths();
		int c = resetRightTableColumnWidths();
		getResultArea().layout(true, true);
		return c;
	}

	public Composite getExtensionButtonComposite() {

		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * ControlerListener to maintain the width of a TableColumn superior to 5
	 * 
	 * @author mdecorde
	 *
	 */
	public static class ConcordanceColumnSizeControlListener implements ControlListener {

		private TableColumn column;

		public ConcordanceColumnSizeControlListener(TableColumn column) {

			this.column = column;
		}

		@Override
		public void controlMoved(ControlEvent e) {
		}

		@Override
		public void controlResized(ControlEvent e) {

			if (column != null && !column.isDisposed() && column.getWidth() < 5) {
				column.setWidth(5);
			}
		}
	}

	public String getLocale() {

		return locale;
	}

	@Override
	public void updateResultFromEditor() {

		if (!queryWidget.getQuery().equals(this.getResult().getQuery())) {
			resetColumnWidthsCalled = false;
		}
	}

	/**
	 * Sets the standard comparators.
	 */
	private void setStandardComparators() {

		for (int i = 0; i < standardComparatorClassName.length; i++) {
			standardComparators.add(standardComparatorClassName[i]);
		}
	}

	@Override
	public TableViewer[] getTableViewers() {

		return new TableViewer[] { viewerLeft, viewerRight };
	}

	@Override
	public TableKeyListener getTableKeyListener() {

		return tableKeyListener;
	}
}
