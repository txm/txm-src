// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.concordance.rcp.adapters;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.FrameworkUtil;
import org.txm.concordance.core.functions.Concordance;
import org.txm.rcp.adapters.TXMResultAdapter;
import org.txm.rcp.adapters.TXMResultAdapterFactory;

/**
 * A factory for creating Adapter objects.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class ConcordanceAdapterFactory extends TXMResultAdapterFactory {


	public static final ImageDescriptor ICON = AbstractUIPlugin.imageDescriptorFromPlugin(FrameworkUtil.getBundle(ConcordanceAdapterFactory.class).getSymbolicName(),
			"platform:/plugin/" + FrameworkUtil.getBundle(ConcordanceAdapterFactory.class).getSymbolicName() + "/icons/concordance.png"); //$NON-NLS-1$ //$NON-NLS-2$

	@Override
	public <T> T getAdapter(Object adaptableObject, Class<T> adapterType) {
		if (this.canAdapt(adapterType) && adaptableObject instanceof Concordance) {
			return adapterType.cast(new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return ICON;
				}
			});
		}
		return null;
	}


}
