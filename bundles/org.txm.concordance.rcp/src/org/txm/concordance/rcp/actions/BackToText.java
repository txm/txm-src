// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.concordance.rcp.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.rcp.messages.TXMUIMessages;

// TODO: Auto-generated Javadoc
/**
 * action called to set the complex sort of the concordance
 * 
 * @author mdecorde.
 */
public class BackToText extends Action implements IWorkbenchAction {

	/** The Constant ID. */
	private static final String ID = "org.txm.rcp.editors.concordances.backtotext"; //$NON-NLS-1$

	/** The window. */
	//private IWorkbenchWindow window;

	/** The concordance editor. */
	private ConcordanceEditor concordanceEditor;

	/**
	 * Instantiates a new back to text.
	 *
	 * @param window the window
	 * @param concordanceEditor the concordance editor
	 */
	public BackToText(IWorkbenchWindow window, ConcordanceEditor concordanceEditor) {
		//this.window = window;
		this.concordanceEditor = concordanceEditor;
		setId(ID);
		setText(TXMUIMessages.displayInFullText);
		setToolTipText(TXMUIMessages.displayInFullText);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	@Override
	public void dispose() {

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {

		concordanceEditor.backToText();
	}
}
