// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.concordance.rcp.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.rcp.messages.TXMUIMessages;

// TODO: Auto-generated Javadoc
/**
 * Action to set the contexts size @ author mdecorde.
 */
public class SetContextSize extends Action implements IWorkbenchAction {

	/** The Constant ID. */
	private static final String ID = "org.txm.rcp.editors.concordances.setcontextsize"; //$NON-NLS-1$

	/** The window. */
	private IWorkbenchWindow window;

	/** The concordance editor. */
	private ConcordanceEditor concordanceEditor;

	/**
	 * Instantiates a new sets the context size.
	 *
	 * @param window the window
	 * @param concordanceEditor the concordance editor
	 */
	public SetContextSize(IWorkbenchWindow window, ConcordanceEditor concordanceEditor) {

		this.window = window;
		this.concordanceEditor = concordanceEditor;
		setId(ID);
		setText(TXMUIMessages.ampContextsDisplayOptions);
		setToolTipText(TXMUIMessages.contextsDisplayOptions);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	@Override
	public void dispose() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		SetContextSizeDialog d = new SetContextSizeDialog(window.getShell());
		if (d.open() == Window.OK) {
			concordanceEditor.setContextSize(d.getLeftContextSize(), d.getRightContextSize());
			// concordanceEditor.getConcordance().setLeftContextSize(d.getLeftContextSize());
			// concordanceEditor.getConcordance().setRightContextSize(d.getRightContextSize());
			// concordanceEditor.sort();
			// concordanceEditor.fillDisplayArea(concordanceEditor.getTopLine(),
			// concordanceEditor.getBottomLine());
		}
	}

	/**
	 * The Class SetContextSizeDialog.
	 */
	private class SetContextSizeDialog extends Dialog {

		/** The right context size. */
		private int rightContextSize;

		/** The left context size. */
		private int leftContextSize;

		/** The left spinner. */
		private Spinner leftSpinner;

		/** The right spinner. */
		private Spinner rightSpinner;

		/**
		 * Instantiates a new sets the context size dialog.
		 *
		 * @param parentShell the parent shell
		 */
		protected SetContextSizeDialog(Shell parentShell) {
			super(parentShell);
			//parentShell.setText(Messages.SetContextSize_1);
		}

		/**
		 * Gets the right context size.
		 *
		 * @return the right context size
		 */
		public int getRightContextSize() {
			return rightContextSize;
		}

		/**
		 * Gets the left context size.
		 *
		 * @return the left context size
		 */
		public int getLeftContextSize() {
			return leftContextSize;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
		 */
		@Override
		protected Control createDialogArea(Composite parent) {
			Composite mainArea = new Composite(parent, SWT.NONE);
			mainArea.setLayout(new GridLayout(2, false));
			new Label(mainArea, SWT.None).setText(TXMUIMessages.leftContextSize);
			leftSpinner = new Spinner(mainArea, SWT.NONE);
			leftSpinner.setMinimum(0);
			leftSpinner.setMaximum(100);
			leftSpinner.setSelection(concordanceEditor.getLeftContextSize());
			new Label(mainArea, SWT.None).setText(TXMUIMessages.rightContextSize);
			rightSpinner = new Spinner(mainArea, SWT.NONE);
			rightSpinner.setMinimum(0);
			rightSpinner.setMaximum(100);
			rightSpinner.setSelection(concordanceEditor.getRightContextSize());
			return mainArea;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
		 */
		@Override
		protected void okPressed() {
			leftContextSize = leftSpinner.getSelection();
			rightContextSize = rightSpinner.getSelection();
			super.okPressed();
		}
	}
}
