package org.txm.concordance.rcp.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchWindow;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.concordance.rcp.messages.ConcordanceUIMessages;

public class ResetColumnWidths extends Action {

	/** The Constant ID. */
	private static final String ID = "org.txm.rcp.editors.concordances.ResetColumnWidths"; //$NON-NLS-1$


	private ConcordanceEditor concordanceEditor;

	public ResetColumnWidths(IWorkbenchWindow window, ConcordanceEditor concordanceEditor) {

		this.concordanceEditor = concordanceEditor;
		setId(ID);
		setText(ConcordanceUIMessages.action_text_reset_column_widths);
	}

	@Override
	public void run() {

		concordanceEditor.resetColumnWidths();
	}
}
