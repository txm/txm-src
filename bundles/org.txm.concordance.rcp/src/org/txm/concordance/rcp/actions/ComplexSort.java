// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.concordance.rcp.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.txm.concordance.core.functions.comparators.CompositeComparator;
import org.txm.concordance.core.functions.comparators.LineComparator;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.rcp.StatusLine;
import org.txm.rcp.messages.TXMUIMessages;

// TODO: Auto-generated Javadoc
/**
 * action called to set the complex sort of the concordance @ author mdecorde.
 */
public class ComplexSort extends Action implements IWorkbenchAction {

	/** The Constant ID. */
	private static final String ID = "org.txm.rcp.editors.concordances.complexsort"; //$NON-NLS-1$

	/** The window. */
	private IWorkbenchWindow window;

	/** The concordance editor. */
	private ConcordanceEditor concordanceEditor;

	/**
	 * Instantiates a new complex sort.
	 *
	 * @param window the window
	 * @param concordanceEditor the concordance editor
	 */
	public ComplexSort(IWorkbenchWindow window,
			ConcordanceEditor concordanceEditor) {
		this.window = window;
		this.concordanceEditor = concordanceEditor;
		setId(ID);
		setText(TXMUIMessages.ampMultipleSort);
		setToolTipText(TXMUIMessages.defineAndUseACompositeSorter);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	@Override
	public void dispose() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		SelectComplexSortDialog d = new SelectComplexSortDialog(window.getShell());

		if (d.open() == Window.OK) {
			concordanceEditor.setUserDefinedComparators(d.getUserDefinedComparators());
			concordanceEditor.setCurrentComparator(d.getSelectedComparator());
			concordanceEditor.sort();
		}
	}

	/**
	 * The Class SelectComplexSortDialog.
	 */
	private class SelectComplexSortDialog extends Dialog {

		/** The selected comparator. */
		private LineComparator selectedComparator;

		/** The user defined comparators. */
		private List<LineComparator> userDefinedComparators;

		/** The list. */
		private org.eclipse.swt.widgets.List list;

		/**
		 * Instantiates a new select complex sort dialog.
		 *
		 * @param shell the shell
		 */
		public SelectComplexSortDialog(Shell shell) {
			super(shell);
			this.userDefinedComparators = new ArrayList<LineComparator>(
					concordanceEditor.getUserDefinedComparators());
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
		 */
		@Override
		protected void configureShell(Shell newShell) {
			super.configureShell(newShell);
			newShell.setText(TXMUIMessages.selectACompositeSorter);
		}

		/**
		 * Gets the user defined comparators.
		 *
		 * @return the user defined comparators
		 */
		public List<LineComparator> getUserDefinedComparators() {
			return userDefinedComparators;
		}

		/**
		 * Gets the selected comparator.
		 *
		 * @return the selected comparator
		 */
		public LineComparator getSelectedComparator() {
			return selectedComparator;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
		 */
		@Override
		protected Control createDialogArea(Composite parent) {
			Composite mainArea = new Composite(parent, SWT.NONE);
			mainArea.setLayout(new GridLayout(2, false));
			list = new org.eclipse.swt.widgets.List(mainArea, SWT.V_SCROLL
					| SWT.BORDER);
			GridData layoutData = new GridData(GridData.FILL, GridData.FILL,
					true, true);
			layoutData.verticalSpan = 2;
			layoutData.minimumWidth = 100;
			layoutData.minimumHeight = 75;
			list.setLayoutData(layoutData);
			for (LineComparator comparator : concordanceEditor
					.getUserDefinedComparators()) {
				list.add(comparator.getName());
			}
			list.select(0);
			Button newButton = new Button(mainArea, SWT.PUSH);
			newButton.setToolTipText("Add a new comparator"); //$NON-NLS-1$
			newButton.setText(TXMUIMessages.ampNew);
			newButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					StatusLine.setMessage(TXMUIMessages.openingComplexSortDialog);
					CompositeSorterCreationDialog sorterCreationDialog = new CompositeSorterCreationDialog(
							getShell());
					int code = sorterCreationDialog.open();
					if (code == Window.OK) {
						CompositeComparator comp = new CompositeComparator(
								sorterCreationDialog.getName(),
								sorterCreationDialog.getComparators());
						list.add(comp.getName());
						userDefinedComparators.add(comp);
						list.select(list.getItemCount() - 1);
					}
					StatusLine.setMessage(""); //$NON-NLS-1$
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
			return mainArea;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
		 */
		@Override
		protected void okPressed() {
			selectedComparator = userDefinedComparators.get(list
					.getSelectionIndex());
			super.okPressed();
		}

	}

	/**
	 * The Class CompositeSorterCreationDialog.
	 */
	private class CompositeSorterCreationDialog extends Dialog {

		/** The available comparators. */
		final private List<LineComparator> availableComparators;

		/** The selected comparators. */
		final private List<LineComparator> selectedComparators;

		/** The name text. */
		private Text nameText;

		/** The name. */
		private String name;

		/**
		 * Instantiates a new composite sorter creation dialog.
		 *
		 * @param shell the shell
		 */
		protected CompositeSorterCreationDialog(Shell shell) {
			super(shell);
			this.availableComparators = new ArrayList<LineComparator>(concordanceEditor.getStandardComparators());
			this.selectedComparators = new ArrayList<LineComparator>();
		}

		/**
		 * Gets the name.
		 *
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * Gets the comparators.
		 *
		 * @return the comparators
		 */
		public List<LineComparator> getComparators() {
			return selectedComparators;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
		 */
		@Override
		protected void configureShell(Shell newShell) {
			super.configureShell(newShell);
			newShell.setText(TXMUIMessages.createANewSorter);
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
		 */
		@Override
		protected Control createDialogArea(Composite parent) {
			Composite mainArea = new Composite(parent, SWT.NONE);
			GridLayout layout = new GridLayout(4, false);
			mainArea.setLayout(layout);

			Label nameLabel = new Label(mainArea, SWT.NONE);
			nameLabel.setText(TXMUIMessages.name);

			nameText = new Text(mainArea, SWT.NONE);
			GridData gridData = new GridData(SWT.FILL, SWT.TOP, true, false);
			gridData.horizontalSpan = 3;
			nameText.setLayoutData(gridData);

			final org.eclipse.swt.widgets.List availbleItems = new org.eclipse.swt.widgets.List(
					mainArea, SWT.BORDER | SWT.V_SCROLL);
			for (LineComparator comparator : availableComparators) {
				availbleItems.add(comparator.getName());
			}
			gridData = new GridData(GridData.FILL, GridData.FILL, true, false);
			Point size = availbleItems.computeSize(SWT.DEFAULT, SWT.DEFAULT);
			gridData.widthHint = size.x;
			availbleItems.setLayoutData(gridData);

			Composite selectionButtons = new Composite(mainArea, SWT.NONE);
			selectionButtons.setLayout(new GridLayout(1, false));
			Button select = new Button(selectionButtons, SWT.ARROW | SWT.RIGHT);
			select.setToolTipText("Select the current element"); //$NON-NLS-1$
			Button unselect = new Button(selectionButtons, SWT.ARROW | SWT.LEFT);
			unselect.setToolTipText("Deselect the current element"); //$NON-NLS-1$
			
			final org.eclipse.swt.widgets.List selectedItems = new org.eclipse.swt.widgets.List(
					mainArea, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
			selectedItems.setLayoutData(gridData);

			Composite orderButtons = new Composite(mainArea, SWT.NONE);
			orderButtons.setLayout(new GridLayout(1, false));
			Button up = new Button(orderButtons, SWT.ARROW | SWT.UP);
			up.setToolTipText("Move up the current element"); //$NON-NLS-1$
			Button down = new Button(orderButtons, SWT.ARROW | SWT.DOWN);
			down.setToolTipText("Move down the current element"); //$NON-NLS-1$
			
			select.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					StatusLine.setMessage(TXMUIMessages.addingToSelection);
					int ind = availbleItems.getSelectionIndex();
					selectedItems.add(availbleItems.getItem(ind));
					availbleItems.remove(ind);
					selectedComparators.add(availableComparators.get(ind));
					availableComparators.remove(ind);
					availbleItems.setSelection(ind);
					StatusLine.setMessage(""); //$NON-NLS-1$
				}
			});

			unselect.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					StatusLine.setMessage(TXMUIMessages.removeFromSelection);
					int ind = selectedItems.getSelectionIndex();
					availbleItems.add(selectedItems.getItem(ind));
					selectedItems.remove(ind);
					availableComparators.add(selectedComparators.get(ind));
					selectedComparators.remove(ind);
					selectedItems.setSelection(ind);
					StatusLine.setMessage(""); //$NON-NLS-1$
				}
			});

			up.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					StatusLine.setMessage(TXMUIMessages.movingUpTheSelection);
					int ind = selectedItems.getSelectionIndex();
					if (ind > 0) {
						selectedItems.add(selectedItems.getItem(ind), ind - 1);
						selectedItems.remove(ind + 1);
						selectedComparators.add(selectedComparators
								.get(ind - 1));
						selectedComparators.remove(ind + 1);
						selectedItems.setSelection(ind - 1);
					}
					StatusLine.setMessage(""); //$NON-NLS-1$
				}
			});

			down.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					StatusLine.setMessage(TXMUIMessages.movingDownTheSelection);
					int ind = selectedItems.getSelectionIndex();
					if (ind < selectedItems.getItemCount() - 1) {
						selectedItems.add(selectedItems.getItem(ind), ind + 2);
						selectedItems.remove(ind);
						selectedComparators.add(selectedComparators
								.get(ind + 2));
						selectedComparators.remove(ind);
						selectedItems.setSelection(ind + 2);
					}
					StatusLine.setMessage(""); //$NON-NLS-1$
				}
			});
			return mainArea;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
		 */
		@Override
		protected void okPressed() {
			if (nameText.getText().equals("")) { //$NON-NLS-1$
				MessageDialog.openError(getShell(), TXMUIMessages.invalidName,
						TXMUIMessages.nameIsMandatory);
				return;
			}
			name = nameText.getText();
			if (selectedComparators.size() < 2) {
				MessageDialog.openError(getShell(), TXMUIMessages.invalidComparator,
						TXMUIMessages.youMustSelectAtLeast2Comparators);
				return;
			}
			super.okPressed();
		}
	}

}
