// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.concordance.rcp.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.concordance.rcp.messages.ConcordanceUIMessages;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.WordProperty;

/**
 * Action to define the sort property to use 
 * @author mdecorde.
 */
public class SetSortProperty extends Action implements IWorkbenchAction {

	/** The Constant ID. */
	private static final String ID = "org.txm.rcp.editors.concordances.setsortproperty"; //$NON-NLS-1$

	/** The window. */
	private IWorkbenchWindow window;

	/** The concordance editor. */
	private ConcordanceEditor concordanceEditor;

	/**
	 * Instantiates a new sets the sort property.
	 *
	 * @param window the window
	 * @param concordanceEditor the concordance editor
	 */
	public SetSortProperty(IWorkbenchWindow window,
			ConcordanceEditor concordanceEditor) {
		this.window = window;
		this.concordanceEditor = concordanceEditor;
		setId(ID);
		setText(TXMUIMessages.ampSortOptions);
		setToolTipText(TXMUIMessages.sortOptions);
	}

	/**
	 * Instantiates a new sets the sort property.
	 */
	public SetSortProperty() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	@Override
	public void dispose() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		List<WordProperty> selectedProperty;
		int columnid = concordanceEditor.getPointedColumn();
		switch (columnid) // the selected properties depends on the columns
		{
			case 1:
				selectedProperty = concordanceEditor.getSelectedLeftSortProperties();
				break;
			case 2:
				selectedProperty = concordanceEditor.getSelectedKeywordSortProperties();
				break;
			default:
				selectedProperty = concordanceEditor.getSelectedRightSortProperties();
				break;
		}

		SetSortPropertyDialog d = new SetSortPropertyDialog(window.getShell(),
				concordanceEditor.getAvailableSortProperties(),
				selectedProperty);

		if (d.open() == Window.OK) {

			switch (columnid) // the selected properties depends on the columns
			{
				case 1:
					concordanceEditor.setLeftSortProperties(d.getSelectedProperty());
					break;
				case 2:
					concordanceEditor.setKeywordSortProperties(d.getSelectedProperty());
					break;
				case 3:
					concordanceEditor.setRightSortProperties(d.getSelectedProperty());
					break;
			}
		}
	}

	/**
	 * The Class SetSortPropertyDialog.
	 */
	private class SetSortPropertyDialog extends Dialog {

		/** The properties. */
		private final List<WordProperty> properties;

		/** The buttons. */
		private List<Button> buttons;

		/** The selected property. */
		private List<WordProperty> selectedProperty;

		/**
		 * Instantiates a new sets the sort property dialog.
		 *
		 * @param shell the shell
		 * @param properties the properties
		 * @param selectedProperty2 the selected property
		 */
		public SetSortPropertyDialog(Shell shell, List<WordProperty> properties, List<WordProperty> selectedProperty2) {
			
			super(shell);
			this.properties = properties;
			this.selectedProperty = selectedProperty2;
			buttons = new ArrayList<Button>();

			this.setShellStyle(this.getShellStyle() | SWT.RESIZE);
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
		 */
		@Override
		protected Control createDialogArea(Composite parent) {
			Composite mainArea = new Composite(parent, SWT.NONE);
			mainArea.setLayout(new RowLayout(SWT.VERTICAL));
			mainArea.setLayoutData(new GridData(GridData.FILL, GridData.FILL,
					true, true));
			Group group = new Group(mainArea, SWT.SHADOW_IN);
			group.setText(TXMUIMessages.sortOptions);
			group.setLayout(new RowLayout(SWT.VERTICAL));
			for (Property property : properties) {
				Button button = new Button(group, SWT.RADIO);
				button.setText(property.getName());
				button.setToolTipText(ConcordanceUIMessages.bind(ConcordanceUIMessages.selectTheP0Property, property));
				buttons.add(button);
			}
			return mainArea;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
		 */
		@Override
		protected void okPressed() {
			selectedProperty.clear();
			for (int i = 0; i < buttons.size(); i++)
				if (buttons.get(i).getSelection())
					selectedProperty.add(properties.get(i));
			super.okPressed();
		}

		/**
		 * Gets the selected property.
		 *
		 * @return the selected property
		 */
		public List<WordProperty> getSelectedProperty() {
			return selectedProperty;
		}
	}
}
