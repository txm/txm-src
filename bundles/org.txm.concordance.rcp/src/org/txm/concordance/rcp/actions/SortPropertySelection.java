// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.concordance.rcp.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.concordance.rcp.editors.SortPropertySelectionDialog;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.corpus.WordProperty;

// TODO: Auto-generated Javadoc
/**
 * Action to define the view properties of each column @ author mdecorde.
 */
public class SortPropertySelection extends Action implements IWorkbenchAction {

	/** The Constant ID. */
	private static final String ID = "org.txm.rcp.editors.concordances.SortPropertySelection"; //$NON-NLS-1$

	/** The concordance editor. */
	private ConcordanceEditor concordanceEditor;

	/** The available view properties. */
	List<WordProperty> availableSortProperties;

	/** The selected view properties. */
	List<WordProperty> selectedSortProperties;

	/**
	 * Instantiates a new view property selection.
	 *
	 * @param window the window
	 * @param concordanceEditor the concordance editor
	 * @param tableViewer the table viewer
	 */
	public SortPropertySelection(IWorkbenchWindow window,
			ConcordanceEditor concordanceEditor, TableViewer tableViewer) {
		this.concordanceEditor = concordanceEditor;
		setId(ID);
		setText(TXMUIMessages.sortOptions);
		setToolTipText(TXMUIMessages.sortOptions);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	@Override
	public void dispose() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		int columnid = concordanceEditor.getPointedColumn(); // get the column id
		IWorkbenchPartSite site = concordanceEditor.getSite();

		List<WordProperty> selectedSortProperties = new ArrayList<WordProperty>();
		switch (columnid) // the selected properties depends on the columns
		{
			case 1:
				availableSortProperties = concordanceEditor.getAvailableLeftSortProperties();
				selectedSortProperties = concordanceEditor.getSelectedLeftSortProperties();
				break;
			case 2:
				availableSortProperties = concordanceEditor.getAvailableKeywordSortProperties();
				selectedSortProperties = concordanceEditor.getSelectedKeywordSortProperties();
				break;
			case 3:
				availableSortProperties = concordanceEditor.getAvailableRightSortProperties();
				selectedSortProperties = concordanceEditor.getSelectedRightSortProperties();
				break;
		}

		//		System.out.println("Column: "+columnid);
		//		System.out.println("available: "+availableSortProperties);
		//		System.out.println("selected: "+selectedSortProperties);

		int ret = new SortPropertySelectionDialog(site, availableSortProperties, selectedSortProperties).open();
		if (ret == SWT.CANCEL)
			return;

		switch (columnid) {
			case 1:
				concordanceEditor.setLeftSortProperties(selectedSortProperties);
				break;
			case 2:
				concordanceEditor.setKeywordSortProperties(selectedSortProperties);
				break;
			case 3:
				concordanceEditor.setRightSortProperties(selectedSortProperties);
				break;
		}

		// concordanceEditor.updateSortPropertiesLabel();
	}
}
