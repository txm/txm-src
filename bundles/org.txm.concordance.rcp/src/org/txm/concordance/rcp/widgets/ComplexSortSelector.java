// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.concordance.rcp.widgets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.txm.concordance.core.functions.comparators.CompositeComparator;
import org.txm.concordance.core.functions.comparators.LineComparator;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.concordance.rcp.messages.ConcordanceUIMessages;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * allow to build a Composite sorter of 1 to 4 keys @ author mdecorde.
 */
public class ComplexSortSelector extends GLComposite {

	/** The standard comparators. */
	HashMap<String, LineComparator> standardComparators = new HashMap<String, LineComparator>();

	/** The comparators names. */
	List<String> comparatorsNames;

	/** The editor. */
	ConcordanceEditor editor;

	/** The default comparator. */
	CompositeComparator defaultComparator;

	/** The first key. */
	Combo firstKey;

	/** The second key. */
	Combo secondKey;

	/** The third key. */
	Combo thirdKey;

	/** The fourth key. */
	Combo fourthKey;

	/**
	 * Instantiates a new complex sort selector.
	 *
	 * @param parent the parent
	 * @param style the style
	 */
	public ComplexSortSelector(Composite parent, int style) {
		super(parent, style, "complex sorter"); //$NON-NLS-1$

		GridLayout gl = this.getLayout();
		gl.marginWidth = gl.marginLeft = gl.marginRight = 0;
		gl.horizontalSpacing = 1;
		this.getLayout().numColumns = 10;

		SelectionListener listener = new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				updateDefaultComparator();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};

		Label l = new Label(this, SWT.NONE);
		l.setLayoutData(new GridData());
		l.setText(TXMUIMessages.sortKeys);

		l = new Label(this, SWT.NONE);
		l.setLayoutData(new GridData());
		l.setText("#1"); //$NON-NLS-1$

		firstKey = new Combo(this, SWT.NONE | SWT.READ_ONLY);
		GridData griddata = new GridData(GridData.FILL, GridData.FILL, false, true);
		//griddata.widthHint = 100;
		firstKey.setLayoutData(griddata);
		firstKey.setToolTipText(ConcordanceUIMessages.theFirstSortKey);

		l = new Label(this, SWT.NONE);
		l.setLayoutData(new GridData());
		l.setText("#2"); //$NON-NLS-1$

		secondKey = new Combo(this, SWT.NONE | SWT.READ_ONLY);
		griddata = new GridData(GridData.FILL, GridData.FILL, false, true);
		secondKey.setLayoutData(griddata);
		secondKey.setToolTipText(ConcordanceUIMessages.theSecondSortKey);

		l = new Label(this, SWT.NONE);
		l.setLayoutData(new GridData());
		l.setText("#3"); //$NON-NLS-1$

		thirdKey = new Combo(this, SWT.NONE | SWT.READ_ONLY);
		griddata = new GridData(GridData.FILL, GridData.FILL, false, true);
		thirdKey.setLayoutData(griddata);
		thirdKey.setToolTipText(ConcordanceUIMessages.theThirdSortKey);

		l = new Label(this, SWT.NONE);
		l.setLayoutData(new GridData());
		l.setText("#4"); //$NON-NLS-1$

		fourthKey = new Combo(this, SWT.NONE | SWT.READ_ONLY);
		griddata = new GridData(GridData.FILL, GridData.FILL, false, true);
		fourthKey.setLayoutData(griddata);
		fourthKey.setToolTipText(ConcordanceUIMessages.theFourthSortKey);

		Button sortButton = new Button(this, SWT.PUSH);
		sortButton.setText(TXMUIMessages.sort);
		sortButton.addSelectionListener(listener);
		sortButton.setLayoutData(new GridData());
		sortButton.setToolTipText(ConcordanceUIMessages.sortTheLinesUsingTheSelectedSortKeys);
	}

	/**
	 * Update default comparator.
	 */
	public void updateDefaultComparator() {
		try {
			ArrayList<LineComparator> comp = new ArrayList<LineComparator>();
			comp.add(standardComparators.get(firstKey.getText()));
			comp.add(standardComparators.get(secondKey.getText()));
			comp.add(standardComparators.get(thirdKey.getText()));
			comp.add(standardComparators.get(fourthKey.getText()));
			defaultComparator.setComparators(comp);

			Log.fine(NLS.bind(TXMUIMessages.complexSortColonP0, comp));

			editor.resetSortedColumn(firstKey.getSelectionIndex());
			editor.setCurrentComparator(defaultComparator);
			editor.sort();
		}
		catch (Exception e) {
			System.err.println(NLS.bind(TXMUIMessages.errorDuringSortColonP0, e));
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	/**
	 * Sets a simple sort with ONE key.
	 * 0,1,2,3 or 4 of the available sorts list
	 *
	 * @param no the new key
	 */
	public void setSimpleSortKey(int no) {
		firstKey.select(no);
		secondKey.select(0);
		thirdKey.select(0);
		fourthKey.select(0);

	}

	/**
	 * Sets the comparators.
	 *
	 * @param editor the new comparators
	 */
	public void setComparators(ConcordanceEditor editor) {
		this.editor = editor;

		List<LineComparator> standardComparatorslist = editor.getStandardComparators();

		comparatorsNames = new ArrayList<String>(standardComparatorslist.size());
		String[] tcomparatorsNames = new String[standardComparatorslist.size()];
		for (int i = 0; i < standardComparatorslist.size(); i++) {
			LineComparator comparator = standardComparatorslist.get(i);
			comparatorsNames.add(comparator.getName());
			tcomparatorsNames[i] = comparator.getName();
			this.standardComparators.put(comparator.getName(), comparator);
		}

		firstKey.setItems(tcomparatorsNames);
		secondKey.setItems(tcomparatorsNames);
		thirdKey.setItems(tcomparatorsNames);
		fourthKey.setItems(tcomparatorsNames);

		List<LineComparator> userDefinedComparators = editor
				.getUserDefinedComparators();
		for (LineComparator c : userDefinedComparators) {
			if (c.getName().equals(TXMUIMessages._default)) {
				defaultComparator = (CompositeComparator) c;
				List<LineComparator> comp = defaultComparator.getComparators();
				if (comp.size() >= 4) {
					firstKey.select(comparatorsNames.indexOf(comp.get(0).getName()));
					secondKey.select(comparatorsNames.indexOf(comp.get(1).getName()));
					thirdKey.select(comparatorsNames.indexOf(comp.get(2).getName()));
					fourthKey.select(comparatorsNames.indexOf(comp.get(3).getName()));
				}
				else if (comp.size() >= 3) {
					firstKey.select(comparatorsNames.indexOf(comp.get(0).getName()));
					secondKey.select(comparatorsNames.indexOf(comp.get(1).getName()));
					thirdKey.select(comparatorsNames.indexOf(comp.get(2).getName()));
					fourthKey.select(comparatorsNames.indexOf(comp.get(2).getName()));
				}
				else if (comp.size() == 2) {
					firstKey.select(comparatorsNames.indexOf(comp.get(0).getName()));
					secondKey.select(comparatorsNames.indexOf(comp.get(1).getName()));
					thirdKey.select(comparatorsNames.indexOf(comp.get(1).getName()));
					fourthKey.select(comparatorsNames.indexOf(comp.get(1).getName()));
				}
				else if (comp.size() == 1) {
					firstKey.select(comparatorsNames.indexOf(comp.get(0).getName()));
					secondKey.select(comparatorsNames.indexOf(comp.get(0).getName()));
					thirdKey.select(comparatorsNames.indexOf(comp.get(0).getName()));
					fourthKey.select(comparatorsNames.indexOf(comp.get(0).getName()));
				}
				return;
			}
		}

		// no default comparator found -> create one
		defaultComparator = new CompositeComparator(TXMUIMessages._default, standardComparatorslist);
		firstKey.select(0);
		secondKey.select(0);
		thirdKey.select(0);
		fourthKey.select(0);
		editor.getUserDefinedComparators().add(defaultComparator);
		editor.setCurrentComparator(defaultComparator);
		//		editor.sort(); // do it later 
	}
}
