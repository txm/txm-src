package org.txm.concordance.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Concordance UI messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ConcordanceUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.concordance.rcp.messages.messages"; //$NON-NLS-1$

	public static String action_text_reset_column_widths;

	public static String errorWhileComputingTheConcordanceColonP0;

	public static String anErrorOccurredWhileRetrievingTheConcordanceLinesColonP0;

	public static String openingTheConcordanceResult;

	public static String concordanceParameters;

	public static String linesPerAmpPageColon;

	public static String ampLeftContextLengthInWords;

	public static String ampRightContextLengthInWords;

	public static String reference;

	public static String inf;

	public static String selectTheCurrentElement;

	public static String sup;

	public static String leftContext;

	public static String keyword;

	public static String sortingReferenceColumn;

	public static String rightContext;

	public static String sortingLeftContextColumn;

	public static String sortingKeywordColumn;

	public static String sortingRightContextColumn;

	public static String openingCorpusEdition;

	public static String startComputingConcordance;

	public static String sorting;

	public static String deleteSelectedLines;

	public static String deselectTheCurrentElement;



	public static String editor_19;

	public static String sort;

	public static String editor_21;



	public static String size;

	public static String navigation;



	public static String fatalErrorColonP0;

	public static String canNotOpenConcordanceEditorWithP0;

	public static String errorWhileBuildingInterfaceColonP0;

	public static String left;

	public static String moveDownTheCurrentElement;

	public static String moveUpTheCurrentElement;

	public static String propertiesPanel;

	public static String references;

	public static String References;

	public static String right;

	public static String SelectionError;

	public static String sortingWithP0;

	public static String view;

	public static String selectTheP0Property;

	public static String numberOfTokensDisplayedBeforeTheFirstPivotToken;

	public static String numberOfTokensDisplayedAfterTheLastPivotToken;

	public static String sortTheLinesUsingTheSelectedSortKeys;

	public static String theFourthSortKey;

	public static String theThirdSortKey;

	public static String theSecondSortKey;

	public static String theFirstSortKey;


	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, ConcordanceUIMessages.class);
	}

}
