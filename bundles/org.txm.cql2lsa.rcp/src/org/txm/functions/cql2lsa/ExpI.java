package org.txm.functions.cql2lsa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;
import org.txm.index.core.messages.IndexCoreMessages;
import org.txm.lexicaltable.core.statsengine.r.data.LexicalTableImpl;
import org.txm.rcp.IImageKeys;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.InvalidCqpIdException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

public class ExpI extends TXMResult implements IAdaptable {

	CQPCorpus corpus;

	List<String> texts;

	int[] textBoundaries;

	LinkedHashMap<String, QueryIndexLine> lines = new LinkedHashMap<>();

	/** The writer. */
	private OutputStreamWriter writer;

	public ExpI(MainCorpus corpus) throws CqiClientException, IOException, CqiServerError, InvalidCqpIdException {
		super(corpus);
		this.corpus = corpus;
		// System.out.println("get text ids");
		texts = Arrays.asList(corpus.getCorpusTextIdsList());
		// System.out.println( "init texts: "+texts);
		// System.out.println("get text limits: "+texts.size());
		textBoundaries = corpus.getTextEndLimits();
		// System.out.println("rdy: "+textBoundaries.length);
		// System.out.println(Arrays.toString(textBoundaries));
		// System.out.println("length: "+textBoundaries.length);
		// System.out.println(texts);
		// System.out.println("length: "+texts.size());
	}

	@Override
	public String getName() {
		return corpus.getName();
	}

	public List<String> getTextNames() {
		return texts;
	}

	@Override
	public org.txm.core.results.TXMResult getParent() {
		return corpus;
	}

	public CQPCorpus getCorpus() {
		return corpus;
	}

	public Collection<QueryIndexLine> getLines() {
		return lines.values();
	}

	public LinkedHashMap<String, QueryIndexLine> getLinesHash() {
		return lines;
	}

	int multi = 1;
	//	
	//	public void sortLines(SortMode mode, boolean revert) {
	//		
	//		multi = 1;
	//		if (revert) multi = -1;
	//		List<Map.Entry<String, QueryIndexLine>> entries = new ArrayList<>(lines.entrySet());
	//		
	//		if (mode == SortMode.FREQUNIT) {
	//			Collections.sort(entries, new Comparator<Map.Entry<String, QueryIndexLine>>() {
	//				
	//				@Override
	//				public int compare(Map.Entry<String, QueryIndexLine> a, Map.Entry<String, QueryIndexLine> b) {
	//					int ret = multi * (a.getValue().getFrequency() - b.getValue().getFrequency());
	//					if (ret == 0) {
	//						return multi * a.getValue().getName().compareTo(b.getValue().getName());
	//					}
	//					return ret;
	//				}
	//			});
	//		}
	//		else if (mode == SortMode.FREQ) {
	//			Collections.sort(entries, new Comparator<Map.Entry<String, QueryIndexLine>>() {
	//				
	//				@Override
	//				public int compare(Map.Entry<String, QueryIndexLine> a, Map.Entry<String, QueryIndexLine> b) {
	//					return multi * (a.getValue().getFrequency() - b.getValue().getFrequency());
	//				}
	//			});
	//		}
	//		else if (mode == SortMode.UNIT) {
	//			Collections.sort(entries, new Comparator<Map.Entry<String, QueryIndexLine>>() {
	//				
	//				@Override
	//				public int compare(Map.Entry<String, QueryIndexLine> a, Map.Entry<String, QueryIndexLine> b) {
	//					return multi * a.getValue().getName().compareTo(b.getValue().getName());
	//				}
	//			});
	//		}
	//		else if (mode == SortMode.UNITFREQ) {
	//			Collections.sort(entries, new Comparator<Map.Entry<String, QueryIndexLine>>() {
	//				
	//				@Override
	//				public int compare(Map.Entry<String, QueryIndexLine> a, Map.Entry<String, QueryIndexLine> b) {
	//					int ret = multi * a.getValue().getName().compareTo(b.getValue().getName());
	//					if (ret == 0) {
	//						return multi * (a.getValue().getFrequency() - b.getValue().getFrequency());
	//					}
	//					return ret;
	//				}
	//			});
	//		}
	//		
	//		LinkedHashMap<String, QueryIndexLine> sortedMap = new LinkedHashMap<>();
	//		for (Map.Entry<String, QueryIndexLine> entry : entries) {
	//			sortedMap.put(entry.getKey(), entry.getValue());
	//		}
	//		
	//		lines = sortedMap;
	//	}

	public void addLinesFromFile(File propFile, TXMProgressMonitor monitor) throws CqiClientException, IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(propFile), "UTF-8")); //$NON-NLS-1$
		ArrayList<String> lines = new ArrayList<>();
		String l = reader.readLine();
		while (l != null) {
			lines.add(l);
			l = reader.readLine();
		}
		reader.close();

		System.out.println("Number of query lines: " + lines.size());
		monitor.setTask("Querying...");

		for (String line : lines) {
			String[] split = line.split("=", 2); //$NON-NLS-1$
			if (split.length == 2) {
				if (hasLine(split[0])) {
					System.out.println(TXMCoreMessages.bind(TXMCoreMessages.warningColonDuplicateQueryEntryColonP0, line));
				}
				else {
					if (addLine(split[0], new CQLQuery(split[1])) == null) {
						System.out.println(TXMCoreMessages.bind(TXMCoreMessages.warningColonQueryFailedColonP0, line));
					}
					monitor.worked(1);
				}
			}
		}
	}

	/**
	 * Write all the lines on a writer.
	 *
	 * @param outfile the outfile
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @return true, if successful
	 */
	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) {
		try {
			toTxt(outfile, 0, lines.size(), encoding, colseparator, txtseparator);
		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.bind(IndexCoreMessages.error_failedToExportLexiconColonP0, Log.toString(e)));
			return false;
		}
		return true;
	}

	/**
	 * Write the lines between from and to on a writer.
	 *
	 * @param outfile the outfile
	 * @param from The first line to be written
	 * @param to The last line to be writen
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @throws CqiClientException the cqi client exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void toTxt(File outfile, int from, int to, String encoding, String colseparator, String txtseparator)
			throws CqiClientException, IOException {
		// NK: writer declared as class attribute to perform a clean if the operation is interrupted
		this.writer = new OutputStreamWriter(new FileOutputStream(outfile),
				encoding);
		String header = "Queries"; //$NON-NLS-1$
		header = txtseparator + header.substring(0, header.length() - 1) + txtseparator;
		header += colseparator + txtseparator + "F" + txtseparator; //$NON-NLS-1$

		for (String t : texts)
			header += colseparator + txtseparator + t.replace(txtseparator, txtseparator + txtseparator) + txtseparator;
		header += "\n"; //$NON-NLS-1$
		writer.write(header);

		// for(Line ligne: lines)
		for (String name : lines.keySet()) {
			QueryIndexLine ligne = lines.get(name);
			writer.write(txtseparator + ligne.getName().replace(txtseparator, txtseparator + txtseparator) + txtseparator + colseparator + ligne.getFrequency());

			for (int j = 0; j < texts.size(); j++)
				writer.write(colseparator + ligne.getFrequency(j));
			writer.write("\n"); //$NON-NLS-1$
		}
		writer.flush();
		writer.close();
	}

	/**
	 * Only one query result and uses texts boundaries to count frequencies for each text
	 * 
	 * @param name
	 * @param query
	 * @return
	 * @throws CqiClientException
	 */
	public QueryIndexLine addLine(String name, CQLQuery query) throws CqiClientException {
		if (lines.containsKey(name)) return null;
		QueryResult qresult = corpus.query(query, "tmp", true); //$NON-NLS-1$
		// System.out.println(query.toString()+" "+qresult.getNMatch());
		int[] counts = new int[textBoundaries.length];
		int count = 0;
		int noText = 0;
		int endOfCurrentText = textBoundaries[noText];
		for (Match m : qresult.getMatches()) {
			while (m.getStart() >= endOfCurrentText) {
				// System.out.println(m.getStart() +">="+endOfCurrentText);
				if (noText >= textBoundaries.length) break;
				// System.out.println("Text: "+texts.get(noText)+" count="+count+" notext="+noText);
				counts[noText] = count;
				noText++;
				if (noText >= textBoundaries.length) break;
				endOfCurrentText = textBoundaries[noText];
				counts[noText] = 0;
				count = 0;
			}
			count++;
		}
		// System.out.println(noText +"<?"+textBoundaries.length+" count="+count);
		if (noText < textBoundaries.length) // last text
			counts[noText] = count;

		qresult.drop();

		QueryIndexLine line = new QueryIndexLine(name, query, null);
		line.setFrequencies(counts);
		lines.put(name, line);
		return line;
	}

	public LexicalTableImpl toLexicalTable() {

		int npart = texts.size();
		int[][] freqs = new int[lines.size()][npart];
		String[] rownames = new String[lines.size()];
		String[] colnames = new String[npart];

		int i = 0;
		for (String k : lines.keySet()) {
			QueryIndexLine line = lines.get(k);
			int[] linefreqs = line.getFreqs();
			rownames[i] = line.getName();
			for (int j = 0; j < npart; j++) {
				freqs[i][j] = linefreqs[j];
			}
			i++;
		}
		int j = 0;
		for (String t : texts) {
			colnames[j] = t;
			j++;
		}

		try {
			LexicalTableImpl lt = new LexicalTableImpl(freqs, rownames, colnames);
			return lt;
		}
		catch (RWorkspaceException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return null;
	}

	public boolean removeLine(String name) {
		if (lines.containsKey(name)) {
			this.acquireSemaphore();
			lines.remove(name);
			this.releaseSemaphore();
			return true;
		}
		else {
			return false;
		}
	}

	public boolean hasLine(String name) {
		return lines.containsKey(name);
	}

	public ArrayList<QueryIndexLine> getLines(int from, int to) {
		if (lines.size() == 0) return new ArrayList<>();

		if (from < 0) from = 0;
		if (to < 0) to = 0;
		if (to > lines.size()) to = lines.size();
		if (from > to) from = to - 1;
		ArrayList<QueryIndexLine> tmp = new ArrayList<>();
		int i = 0;
		for (QueryIndexLine line : lines.values()) {
			if (i >= from && i < to) {
				tmp.add(line);
			}
			i++;
		}

		return tmp;
	}

	@Override
	public String[] getExportTXTExtensions() {
		return new String[] { "*.csv" };
	}

	@Override
	public void clean() {
		try {
			this.writer.flush();
			this.writer.close();
		}
		catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	@Override
	public <T> T getAdapter(Class<T> adapterType) {
		// TODO Auto-generated method stub
		if (adapterType == IWorkbenchAdapter.class)
			return adapterType.cast(queryIndexOfTextAdapter);
		return null;
	}

	/** The WordCloud adapter. */
	private static IWorkbenchAdapter queryIndexOfTextAdapter = new IWorkbenchAdapter() {

		@Override
		public Object[] getChildren(Object o) {
			return new Object[0];
		}

		@Override
		public ImageDescriptor getImageDescriptor(Object object) {
			return IImageKeys.getImageDescriptor(IImageKeys.QUERYINDEX);
		}

		@Override
		public String getLabel(Object o) {
			return ((ExpI) o).getName();
		}

		@Override
		public Object getParent(Object o) {
			return ((ExpI) o).getCorpus();
		}
	};

	@Override
	public boolean _compute(TXMProgressMonitor monitor) throws Exception {

		System.out.println("ExpI.compute(monitor): not implemented.");
		return false;
	}

	@Override
	public String getSimpleName() {
		return "ExpI";
	}

	@Override
	public String getDetails() {
		return texts.toString();
	}

	@Override
	public boolean saveParameters() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean loadParameters() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canCompute() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getResultType() {
		// TODO
		return this.getClass().getSimpleName();
	}

}
