package org.txm.functions.cql2lsa;

import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;

public class QueryIndexLine {

	String name;

	private CQLQuery query;

	QueryResult[] qresults;

	int[] freqs;

	int total;


	public QueryIndexLine(String name, CQLQuery query, QueryResult[] qresults) {
		this.qresults = qresults;
		this.query = query;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public CQLQuery getQuery() {
		return query;
	}

	public int[] getFreqs() {
		computeFreqsAndTotal();
		return freqs;
	}

	private void computeFreqsAndTotal() {
		if (freqs == null) {
			//System.out.println("compute freqs");
			total = 0;
			try {
				freqs = new int[qresults.length];
				for (int i = 0; i < freqs.length; i++) {
					//System.out.println("add "+qresults[i].getNMatch());
					freqs[i] = qresults[i].getNMatch();
					total += freqs[i];
					qresults[i].drop();
				}
				qresults = null;
			}
			catch (CqiClientException e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
				freqs = new int[0];
			}
		}
		else {
			total = 0;
			for (int i = 0; i < freqs.length; i++) {
				total += freqs[i];
			}
		}
	}

	public int getFrequency() {
		computeFreqsAndTotal();
		return total;
	}

	public int getFrequency(int i) {
		computeFreqsAndTotal();
		return freqs[i];
	}

	public void setFrequencies(int[] freqs) {
		this.freqs = freqs;
		//		total = 0 ;
		//		for (int f : freqs) total+=f;
		//		System.out.println("total : "+total);
	}

	/*
	 * private static void mergeMatches(int from, List<Match> matches, List<Match> tmp) {
	 * int im1 = 0;
	 * for (int im2 = 0 ; im2 < tmp.size() && im1 < matches.size() ;) {
	 * Match m1 = matches.get(im1);
	 * Match m2 = tmp.get(im2);
	 * if (m1.getStart() < m2.getStart()) {
	 * im1++;
	 * } else if (m1.getStart() > m2.getStart()) {
	 * im2++;
	 * matches.add(im1,m2);
	 * } else {
	 * if (m1.getEnd() < m2.getEnd()) {
	 * im1++;
	 * } else if (m1.getEnd() > m2.getEnd()) {
	 * im2++;
	 * matches.add(im1,m2);
	 * } else {
	 * System.out.println("ERROR while merging : m1 == m2");
	 * }
	 * }
	 * } // end of match merge
	 * }
	 * public void union(List<QueryIndexLine> lines) throws CqiClientException {
	 * for (QueryIndexLine line : lines) { // process lines
	 * for (QueryResult qresult : line.getQResult()) {
	 * List<Match> matches = qresult.getMatches();
	 * int nmatches = matches.size();
	 * int im1 = 0;
	 * List<Match> matches2 = qresult.getMatches();
	 * int nmatches2 = matches2.size();
	 * ArrayList<Match> tmp = new ArrayList<Match>(); // receive the match to add
	 * // 1-5 2-5 3-6 7-9
	 * // 1.4 1-5 2-5 3-5 7-8 7-9
	 * // select all without doublons
	 * int im2;
	 * for (im2 = 0 ; im2 < nmatches2 && im1 < nmatches ;) {
	 * Match m1 = matches.get(im1);
	 * Match m2 = matches2.get(im2);
	 * if (m1.getStart() == m2.getStart() && m1.getEnd() == m2.getEnd()) {
	 * // don't add
	 * im2++; // test next match2
	 * } else {
	 * tmp.add(m2);
	 * if (m1.getStart() < m2.getStart()) {
	 * im1++;
	 * } else if (m1.getStart() > m2.getStart()){
	 * im2++;
	 * } else {
	 * if (m1.getEnd() < m2.getEnd()) {
	 * im1++;
	 * } else {
	 * im2++;
	 * }
	 * }
	 * }
	 * } // almost end of match selection
	 * for (int i = im2 ; i < nmatches2 ; i++)
	 * tmp.add(matches2.get(im2)); // get the last ones if any
	 * // merge
	 * mergeMatches(0, matches, tmp);
	 * } // end of union
	 * freqs = null; // will be recomputed next time
	 * }
	 * }
	 */

	/*
	 * public void inter(List<QueryIndexLine> lines) throws CqiClientException {
	 * List<Match> matches = qresult.getMatches();
	 * for (QueryIndexLine line : lines) {
	 * List<Match> matches2 = line.getQResult().getMatches();
	 * }
	 * freq = -1;
	 * }
	 * public void minus(List<QueryIndexLine> lines)throws CqiClientException {
	 * List<Match> matches = qresult.getMatches();
	 * for (QueryIndexLine line : lines) {
	 * List<Match> matches2 = line.getQResult().getMatches();
	 * }
	 * freq = -1;
	 * }
	 */
}
