package org.txm.functions.cql2lsa;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;
import org.txm.index.core.messages.IndexCoreMessages;
import org.txm.lexicaltable.core.statsengine.r.data.LexicalTableImpl;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.InvalidCqpIdException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

public class ExpII extends TXMResult implements IAdaptable {

	CQPCorpus corpus;

	List<String> texts;

	int[] textBoundaries;

	LinkedHashMap<String, QueryIndexLine> lines = new LinkedHashMap<>();

	/** The writer. */
	private OutputStreamWriter writer;

	public ExpII(MainCorpus corpus) throws CqiClientException, IOException, CqiServerError, InvalidCqpIdException {
		super(corpus);
		this.corpus = corpus;
		texts = Arrays.asList(corpus.getCorpusTextIdsList());
		textBoundaries = corpus.getTextEndLimits();

	}

	public int getT() {
		int t = 0;
		for (QueryIndexLine line : lines.values()) {
			t += line.getFrequency();
		}
		return t;
	}

	public int getFmin() {
		int t = 999999999;
		for (QueryIndexLine line : lines.values()) {
			int f = line.getFrequency();
			if (f < t) t = f;
		}
		return t;
	}

	public int getFmax() {
		int t = 0;
		for (QueryIndexLine line : lines.values()) {
			int f = line.getFrequency();
			if (f > t) t = f;
		}
		return t;
	}

	public int getV() {
		return lines.values().size();
	}

	@Override
	public String getName() {
		return corpus.getName();
	}

	public List<String> getTextNames() {
		return texts;
	}

	@Override
	public TXMResult getParent() {
		return corpus;
	}

	public CQPCorpus getCorpus() {
		return corpus;
	}

	public Collection<QueryIndexLine> getLines() {
		return lines.values();
	}

	public LinkedHashMap<String, QueryIndexLine> getLinesHash() {
		return lines;
	}

	int multi = 1;

	//	public void sortLines(SortMode mode, boolean revert) {
	//		
	//		multi = 1;
	//		if (revert) multi = -1;
	//		List<Map.Entry<String, QueryIndexLine>> entries = new ArrayList<>(lines.entrySet());
	//		
	//		if (mode == SortMode.FREQUNIT) {
	//			Collections.sort(entries, new Comparator<Map.Entry<String, QueryIndexLine>>() {
	//				
	//				@Override
	//				public int compare(Map.Entry<String, QueryIndexLine> a, Map.Entry<String, QueryIndexLine> b) {
	//					int ret = multi * (a.getValue().getFrequency() - b.getValue().getFrequency());
	//					if (ret == 0) {
	//						return multi * a.getValue().getName().compareTo(b.getValue().getName());
	//					}
	//					return ret;
	//				}
	//			});
	//		}
	//		else if (mode == SortMode.FREQ) {
	//			Collections.sort(entries, new Comparator<Map.Entry<String, QueryIndexLine>>() {
	//				
	//				@Override
	//				public int compare(Map.Entry<String, QueryIndexLine> a, Map.Entry<String, QueryIndexLine> b) {
	//					return multi * (a.getValue().getFrequency() - b.getValue().getFrequency());
	//				}
	//			});
	//		}
	//		else if (mode == SortMode.UNIT) {
	//			Collections.sort(entries, new Comparator<Map.Entry<String, QueryIndexLine>>() {
	//				
	//				@Override
	//				public int compare(Map.Entry<String, QueryIndexLine> a, Map.Entry<String, QueryIndexLine> b) {
	//					return multi * a.getValue().getName().compareTo(b.getValue().getName());
	//				}
	//			});
	//		}
	//		else if (mode == SortMode.UNITFREQ) {
	//			Collections.sort(entries, new Comparator<Map.Entry<String, QueryIndexLine>>() {
	//				
	//				@Override
	//				public int compare(Map.Entry<String, QueryIndexLine> a, Map.Entry<String, QueryIndexLine> b) {
	//					int ret = multi * a.getValue().getName().compareTo(b.getValue().getName());
	//					if (ret == 0) {
	//						return multi * (a.getValue().getFrequency() - b.getValue().getFrequency());
	//					}
	//					return ret;
	//				}
	//			});
	//		}
	//		
	//		LinkedHashMap<String, QueryIndexLine> sortedMap = new LinkedHashMap<>();
	//		for (Map.Entry<String, QueryIndexLine> entry : entries) {
	//			sortedMap.put(entry.getKey(), entry.getValue());
	//		}
	//		
	//		lines = sortedMap;
	//	}
	//	
	HashMap<String, int[]> keywordStartPositions;

	HashMap<String, int[]> keywordEndPositions;

	public void compute(File queriesFile, File keywordFile, File outputDir, TXMProgressMonitor monitor) throws CqiClientException, IOException, CqiServerError {
		System.out.println("Starting ExpII");

		File outputFile = new File(outputDir, "doc_word_freq_keyword_2.txt");
		File lexiconFile = new File(outputDir, "lexicon_2.txt");

		PrintWriter writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), "UTF-8")));

		keywordStartPositions = new HashMap<>();
		keywordEndPositions = new HashMap<>();
		HashMap<String, String> keywordQueriestoName = new HashMap<>();
		int idx_lexicon_counter = 0;

		// assos query et idx
		// si queries est dans keywords alors, pas la peine de calculer keyword, parce que la query est un keyword
		LinkedHashMap<String, Integer> idxLexicon = new LinkedHashMap<>();
		LinkedHashMap<String, int[]> keywordsMaxCountsLexicon = new LinkedHashMap<>();

		BufferedReader keywordFileReader = new BufferedReader(new InputStreamReader(
				new FileInputStream(keywordFile), "UTF-8")); //$NON-NLS-1$
		ArrayList<String> keywordLines = new ArrayList<>();
		String l = keywordFileReader.readLine();
		while (l != null) {
			keywordLines.add(l);
			l = keywordFileReader.readLine();
		}
		keywordFileReader.close();
		System.out.println("Number of keywords lines: " + keywordLines.size());
		monitor.setTask("Querying keywords...");

		int nkeyword = 0;
		for (String line : keywordLines) {
			String[] split = line.split("=", 2); //$NON-NLS-1$
			if (split.length == 2) {
				CQLQuery q = new CQLQuery(split[1]);
				keywordQueriestoName.put(split[1], split[0]);
				QueryResult result = corpus.query(q, "K" + nkeyword++, false); //$NON-NLS-1$
				keywordStartPositions.put(split[1], result.getStarts());
				keywordEndPositions.put(split[1], result.getEnds());
				keywordsMaxCountsLexicon.put(split[1], new int[textBoundaries.length]);
				idxLexicon.put(split[1], idx_lexicon_counter++);
				monitor.worked(1);
			}
		}
		keywordFileReader.close();

		BufferedReader queriesFileReader = new BufferedReader(new InputStreamReader(new FileInputStream(queriesFile), "UTF-8")); //$NON-NLS-1$
		ArrayList<String> lines = new ArrayList<>();
		l = queriesFileReader.readLine();
		while (l != null) {
			lines.add(l);
			l = queriesFileReader.readLine();
		}
		queriesFileReader.close();

		System.out.println("Number of lemma lines: " + lines.size());
		monitor.setTask("Querying...");

		int nquery = 0;
		for (String line : lines) {
			String[] split = line.split("=", 2); //$NON-NLS-1$
			if (split.length == 2) {
				if (hasLine(split[0])) {
					System.out.println(TXMCoreMessages.bind(TXMCoreMessages.warningColonDuplicateQueryEntryColonP0, line));
				}
				else {
					QueryResult result = corpus.query(new CQLQuery(split[1]), "Q" + nquery++, false); //$NON-NLS-1$
					int[] starts = result.getStarts();
					int[] ends = result.getEnds();

					int[] counts = new int[textBoundaries.length];
					int count = 0;
					int noText = 0;
					int endOfCurrentText = textBoundaries[noText];
					for (int i : starts) {
						while (i >= endOfCurrentText) {
							if (noText >= textBoundaries.length) break;
							counts[noText] = count;
							noText++;
							if (noText >= textBoundaries.length) break;
							endOfCurrentText = textBoundaries[noText];
							counts[noText] = 0;
							count = 0;
						}
						count++;
					}
					counts[noText] = count;

					if (idxLexicon.containsKey(split[1])) { // the query is already computed !!
						for (noText = 0; noText < textBoundaries.length; noText++) {
							if (counts[noText] > 0) {
								// System.out.println("MCL-"+split[1]+"\t"+noText+"\t"+idxLexicon.get(split[1])+"\t"+counts[noText]+"\t"+idxLexicon.get(split[1]));
								writer.println(noText + "\t" + idxLexicon.get(split[1]) + "\t" + counts[noText] + "\t" + idxLexicon.get(split[1]));
							}
						}
					}
					else { // test if match is covered by keywords
						idxLexicon.put(split[1], idx_lexicon_counter++); // put Lemma query

						// int max_sum_f = 0;
						int[] max_f = new int[textBoundaries.length];
						String[] max_key = new String[textBoundaries.length];

						for (String key : keywordEndPositions.keySet()) {
							int[] keyMaxValues = keywordsMaxCountsLexicon.get(key);
							int[] f = covered(starts, ends, keywordStartPositions.get(key), keywordEndPositions.get(key));
							for (noText = 0; noText < textBoundaries.length; noText++) {
								if (f[noText] > max_f[noText]) {
									max_f[noText] = f[noText];
									max_key[noText] = key;
								}
								if (f[noText] > keyMaxValues[noText]) {
									keyMaxValues[noText] = f[noText];
								}
							}
						}
						// System.out.println("max_f="+Arrays.toString(counts));
						// System.out.println("max_f="+Arrays.toString(max_f));
						for (noText = 0; noText < textBoundaries.length; noText++) {
							int F = counts[noText] - max_f[noText];
							if (F > 0) {
								// System.out.println("L-"+split[1]+"\t"+noText+"\t"+idxLexicon.get(split[1])+"\t"+F+"\t0");
								writer.println(noText + "\t" + idxLexicon.get(split[1]) + "\t" + F + "\t0");
							}
						}
					}
				}
			}
			monitor.worked(1);
		}

		monitor.setTask("Finalizing doc_word_freq_2.txt file...");
		for (String key : keywordsMaxCountsLexicon.keySet()) {
			int[] keyMaxValues = keywordsMaxCountsLexicon.get(key);
			for (int noText = 0; noText < textBoundaries.length; noText++) {
				int F = keyMaxValues[noText];
				if (F > 0) {
					// System.out.println("MC-"+key+"\t"+noText+"\t"+idxLexicon.get(key)+"\t"+F+"\t"+idxLexicon.get(key));
					writer.println(noText + "\t" + idxLexicon.get(key) + "\t" + F + "\t" + idxLexicon.get(key));
				}
			}
		}
		queriesFileReader.close();
		writer.close();

		monitor.setTask("Writing lexicon file...");
		writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(lexiconFile), "UTF-8")));
		for (String query : idxLexicon.keySet()) {
			String name = keywordQueriestoName.get(query);
			if (name != null) {
				writer.println(keywordQueriestoName.get(query));
			}
			else {
				writer.println(query);
			}

		}
		writer.close();

		System.out.println("Done, result saved in: \n - " + outputFile.getAbsolutePath() + "\n - " + lexiconFile.getAbsolutePath());
	}

	/**
	 * Write all the lines on a writer.
	 *
	 * @param outfile the outfile
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @return true, if successful
	 */
	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) {
		try {
			toTxt(outfile, 0, lines.size(), encoding, colseparator, txtseparator);
		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.bind(IndexCoreMessages.error_failedToExportLexiconColonP0, Log.toString(e)));
			return false;
		}
		return true;
	}

	/**
	 * Write the lines between from and to on a writer.
	 *
	 * @param outfile the outfile
	 * @param from The first line to be written
	 * @param to The last line to be writen
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @throws CqiClientException the cqi client exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void toTxt(File outfile, int from, int to, String encoding, String colseparator, String txtseparator)
			throws CqiClientException, IOException {
		// NK: writer declared as class attribute to perform a clean if the operation is interrupted
		this.writer = new OutputStreamWriter(new FileOutputStream(outfile),
				encoding);
		String header = "Queries"; //$NON-NLS-1$
		header = txtseparator + header.substring(0, header.length() - 1) + txtseparator;
		header += colseparator + txtseparator + "F" + txtseparator; //$NON-NLS-1$

		for (String t : texts)
			header += colseparator + txtseparator + t.replace(txtseparator, txtseparator + txtseparator) + txtseparator;
		header += "\n"; //$NON-NLS-1$
		writer.write(header);

		// for(Line ligne: lines)
		for (String name : lines.keySet()) {
			QueryIndexLine ligne = lines.get(name);
			writer.write(txtseparator + ligne.getName().replace(txtseparator, txtseparator + txtseparator) + txtseparator + colseparator + ligne.getFrequency());

			for (int j = 0; j < texts.size(); j++)
				writer.write(colseparator + ligne.getFrequency(j));
			writer.write("\n"); //$NON-NLS-1$
		}
		writer.flush();
		writer.close();
	}

	/**
	 * Only one query result and uses texts boundaries to count frequencies for each text
	 * 
	 * @param name
	 * @param query
	 * @return
	 * @throws CqiClientException
	 */
	public QueryIndexLine addLine(String name, CQLQuery query) throws CqiClientException {
		if (lines.containsKey(name)) return null;
		QueryResult qresult = corpus.query(query, "tmp", true); //$NON-NLS-1$
		// System.out.println(query.toString()+" "+qresult.getNMatch());
		int[] counts = new int[textBoundaries.length];
		int count = 0;
		int noText = 0;
		int endOfCurrentText = textBoundaries[noText];
		for (Match m : qresult.getMatches()) {
			while (m.getStart() >= endOfCurrentText) {
				// System.out.println(m.getStart() +">="+endOfCurrentText);
				if (noText >= textBoundaries.length) break;
				// System.out.println("Text: "+texts.get(noText)+" count="+count+" notext="+noText);
				counts[noText] = count;
				noText++;
				if (noText >= textBoundaries.length) break;
				endOfCurrentText = textBoundaries[noText];
				counts[noText] = 0;
				count = 0;
			}
			count++;
		}
		// System.out.println(noText +"<?"+textBoundaries.length+" count="+count);
		if (noText < textBoundaries.length) // last text
			counts[noText] = count;

		qresult.drop();

		QueryIndexLine line = new QueryIndexLine(name, query, null);
		line.setFrequencies(counts);
		lines.put(name, line);
		return line;
	}

	public LexicalTableImpl toLexicalTable() {

		int npart = texts.size();
		int[][] freqs = new int[lines.size()][npart];
		String[] rownames = new String[lines.size()];
		String[] colnames = new String[npart];

		int i = 0;
		for (String k : lines.keySet()) {
			QueryIndexLine line = lines.get(k);
			int[] linefreqs = line.getFreqs();
			rownames[i] = line.getName();
			for (int j = 0; j < npart; j++) {
				freqs[i][j] = linefreqs[j];
			}
			i++;
		}
		int j = 0;
		for (String t : texts) {
			colnames[j] = t;
			j++;
		}

		try {
			LexicalTableImpl lt = new LexicalTableImpl(freqs, rownames, colnames);
			return lt;
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return null;
	}

	public boolean removeLine(String name) {
		if (lines.containsKey(name)) {
			this.acquireSemaphore();
			lines.remove(name);
			this.releaseSemaphore();
			return true;
		}
		else {
			return false;
		}
	}

	public boolean hasLine(String name) {
		return lines.containsKey(name);
	}

	public ArrayList<QueryIndexLine> getLines(int from, int to) {
		if (lines.size() == 0) return new ArrayList<>();

		if (from < 0) from = 0;
		if (to < 0) to = 0;
		if (to > lines.size()) to = lines.size();
		if (from > to) from = to - 1;
		ArrayList<QueryIndexLine> tmp = new ArrayList<>();
		int i = 0;
		for (QueryIndexLine line : lines.values()) {
			if (i >= from && i < to) {
				tmp.add(line);
			}
			i++;
		}

		return tmp;
	}

	@Override
	public String[] getExportTXTExtensions() {
		return new String[] { "*.csv" };
	}

	@Override
	public void clean() {
		try {
			this.writer.flush();
			this.writer.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	@Override
	public <T> T getAdapter(Class<T> adapterType) {

		if (adapterType == IWorkbenchAdapter.class)
			return adapterType.cast(ExpIIAAdapter);
		return null;
	}

	/** The WordCloud adapter. */
	private static IWorkbenchAdapter ExpIIAAdapter = new IWorkbenchAdapter() {

		@Override
		public Object[] getChildren(Object o) {
			return new Object[0];
		}

		@Override
		public ImageDescriptor getImageDescriptor(Object object) {
			return null;
		}

		@Override
		public String getLabel(Object o) {
			return ((ExpII) o).getName();
		}

		@Override
		public Object getParent(Object o) {
			return ((ExpII) o).getCorpus();
		}
	};



	private int[] covered(int[] starts, int[] ends, int[] keywordStarts, int[] keywordEnds) {
		int[] counts = new int[textBoundaries.length];
		int noText = 0;
		int endOfCurrentText = textBoundaries[noText];
		int f = 0;
		// printMatchs(starts, ends, keywordStarts, keywordEnds);

		int i_keyword = 0;
		// System.out.println("len i="+starts.length+ " len i_keyword="+keywordStarts.length);
		for (int i = 0; i < starts.length;) {

			// System.out.println("i="+i+ " i_keyword="+i_keyword);
			if (i_keyword >= keywordStarts.length) break; // no more keyword positions
			// System.out.println(""+starts[i]+"->"+ends[i]+" : "+keywordStarts[i_keyword]+"->"+keywordEnds[i_keyword]);

			while (starts[i] >= endOfCurrentText) {
				if (noText >= textBoundaries.length) break;
				counts[noText] = f;
				noText++;
				if (noText >= textBoundaries.length) break;
				endOfCurrentText = textBoundaries[noText];
				counts[noText] = 0;
				f = 0;
			}
			if (starts[i] < keywordStarts[i_keyword]) {
				// System.out.println(" match start is not covered");
				i++;
			}
			else if (starts[i] > keywordEnds[i_keyword]) {
				// System.out.println(" next keyword");
				i_keyword++; // see next keyword match
			}
			else if (ends[i] <= keywordEnds[i_keyword]) {
				// System.out.println(" next match");
				// System.out.println(">>>> "+starts[i]+"->"+ends[i]+" : "+keywordStarts[i_keyword]+"->"+keywordEnds[i_keyword]);
				i++; // OK, test next match
				f++;
			}
			else {
				// System.out.println(" match end is not covered");
				i++; // OK, test next match
			}
		}
		counts[noText] = f;
		return counts;
	}

	private static void printMatchs(int[] starts, int[] ends, int[] keywordStarts, int[] keywordEnds) {
		int min = 999999999;
		int max = 0;
		for (int i : starts)
			if (i < min) min = i;
		for (int i : keywordStarts)
			if (i < min) min = i;
		for (int i : ends)
			if (i > max) max = i;
		for (int i : keywordEnds)
			if (i > max) max = i;
		for (int i = min; i <= max; i++)
			System.out.print("" + i + "\t");
		System.out.println();
		int j = 0;
		int k = 0;
		boolean inout = false;
		for (int i = min; i <= max; i++) {
			if (j < starts.length && starts[j] == i) {
				if (k < ends.length && ends[k] == i) {
					System.out.print("" + starts[j++] + "><\t");
					k++;
				}
				else {
					System.out.print("" + starts[j++] + ">\t");
					inout = true;
				}
			}
			else if (k < ends.length && ends[k] == i) {
				System.out.print("<" + ends[k++] + "\t");
				inout = false;
			}
			else {
				if (inout) {
					System.out.print("-\t");
				}
				else {
					System.out.print("\t");
				}
			}
		}
		System.out.println();

		j = 0;
		k = 0;
		inout = false;
		for (int i = min; i <= max; i++) {
			if (j < keywordStarts.length && keywordStarts[j] == i) {
				if (k < keywordEnds.length && keywordEnds[k] == i) {
					System.out.print("" + keywordStarts[j++] + "><\t");
					k++;
				}
				else {
					System.out.print("" + keywordStarts[j++] + ">\t");
					inout = true;
				}
			}
			else if (k < keywordEnds.length && keywordEnds[k] == i) {
				System.out.print("<" + keywordEnds[k++] + "\t");
				inout = false;
			}
			else {
				if (inout) {
					System.out.print("-\t");
				}
				else {
					System.out.print("\t");
				}

			}
		}
		System.out.println();
	}

	public static void main(String[] args) {
		// int starts[] = {1, 10, 16, 24};
		// int ends[] = {2, 12, 16, 30};
		// int kstarts[] = {5, 9, 16, 22};
		// int kends[] = {8, 12, 16, 35};
		// System.out.println(covered(starts, ends, kstarts, kends));

		// int starts[] = {10, 15 ,24, 50};
		// int ends[] = {12, 16 ,30, 60};
		// int kstarts[] = {5, 9, 14, 20};
		// int kends[] = {8, 12, 18, 40};
		// System.out.println(covered(starts, ends, kstarts, kends));

		// int starts[] = {1, 10, 15 ,24};
		// int ends[] = {2, 12, 16 ,30};
		// int kstarts[] = {5, 9, 14, 20};
		// int kends[] = {8, 12, 18, 40};
		// System.out.println(covered(starts, ends, kstarts, kends));

		// int starts[] = {4, 10, 16};
		// int ends[] = {5, 11, 17};
		// int kstarts[] = {3, 11, 15};
		// int kends[] = {8, 13, 20};
		// System.out.println(covered(starts, ends, kstarts, kends));

		int starts[] = { 4, 10, 16, 20 };
		int ends[] = { 5, 11, 17, 22 };
		int kstarts[] = { 3, 11, 15 };
		int kends[] = { 8, 13, 20 };
		// System.out.println(covered(starts, ends, kstarts, kends));
	}

	@Override
	public String getSimpleName() {
		return "ExpII";
	}

	@Override
	public String getDetails() {
		return texts.toString();
	}

	@Override
	public boolean saveParameters() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean loadParameters() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canCompute() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getResultType() {
		// TODO
		return this.getClass().getSimpleName();
	}

}
