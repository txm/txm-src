package org.txm.functions.cql2lsa;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;

import org.txm.core.results.TXMResult;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.MemCqiClient;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.TXMProgressMonitor;

/**
 * Simple index with no multiple properties
 * 
 * @author mdecorde
 *
 */
public class ValuesOfQuery extends TXMResult {

	CQPCorpus corpus;

	CQLQuery query;

	Property prop;

	int fmin;

	public ValuesOfQuery(CQPCorpus corpus, CQLQuery query, Property prop, int fmin) {
		super(corpus);
		this.corpus = corpus;
		this.query = query;
		this.prop = prop;
		this.fmin = fmin;
	}

	public boolean compute(File outputFile, TXMProgressMonitor monitor) throws CqiClientException, IOException, CqiServerError {

		if (!(CQPSearchEngine.getCqiClient() instanceof MemCqiClient)) return false;

		monitor.setTask("Start querying...");

		File file = File.createTempFile("query", ".txt");
		// ExecTimer t = new ExecTimer();
		// System.out.println("run query "+query);t.start();
		QueryResult result = corpus.query(query, "ValuesOf", false);
		// System.out.println("query done"+t.stop());t.start();

		// System.out.println("group query "+query+" and save in "+file.getAbsolutePath());t.start();
		MemCqiClient cli = (MemCqiClient) CQPSearchEngine.getCqiClient();
		cli.query("group " + result.getQualifiedCqpId() + " match " + prop.getName() + " > \"" + file + "\";");
		// System.out.println("query done"+t.stop());t.start();

		monitor.worked(50);

		if (!file.exists()) return false;

		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		String line = reader.readLine(); // "#---------------------------------"
		line = reader.readLine();        // "(none) word \t freq"

		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), "UTF-8"));

		while (line != null) {
			line = line.substring(30);
			// int len = line.length();
			//
			int i = line.indexOf('\t');
			// while (line.charAt(i) == ' ') i++;
			// int i_start = i;
			// while (line.charAt(i) != ' ') i++;
			// String w = line.substring(i_start, i);
			//
			// while (line.charAt(i) == ' ') i++;
			// i_start = i;
			// while (line.charAt(i) != ' ' && i < len) i++;
			// String f = line.substring(i_start, i);
			//
			// System.out.println("'"+w+"'\t'"+f+"'");
			int f = Integer.parseInt(line.substring(i).trim());
			String s;
			if (f > fmin) {
				// System.out.println(line.substring(0, i).trim() + "\t"+f);
				s = line.substring(0, i).trim();
				writer.write(s + "=[" + prop.getName() + "=\"" + s + "\"]\n");
			}
			else {
				break;
			}
			line = reader.readLine();
			monitor.worked(1);
		}
		reader.close();
		writer.close();
		System.out.println("Done printing queries in " + outputFile.getAbsolutePath());
		return true;

	}

	/**
	 * Test purpose function.
	 * Does not manage big corpus
	 * 
	 * @throws CqiClientException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public void test() throws CqiClientException, IOException, CqiServerError {
		QueryResult result = corpus.query(query, "ValuesOf", false);
		int[] positions = new int[result.getNMatch()];
		int i = 0;
		for (Match m : result.getMatches()) {
			positions[i++] = m.getStart();
		}

		String[] values = null;
		if (prop instanceof StructuralUnitProperty) {
			int[] structs = CQPSearchEngine.getCqiClient().cpos2Struc(prop.getQualifiedName(), positions);
			positions = null;
			structs = uniquify(structs);
			values = CQPSearchEngine.getCqiClient().struc2Str(prop.getQualifiedName(), structs);
			structs = null;
		}
		else {
			int[] indexes = CQPSearchEngine.getCqiClient().cpos2Id(prop.getQualifiedName(), positions);
			positions = null;
			indexes = uniquify(indexes);
			values = CQPSearchEngine.getCqiClient().id2Str(prop.getQualifiedName(), indexes);
			indexes = null;
		}

		System.out.println("Values: ");
		for (String v : values) {
			System.out.println(v);
		}
	}

	/**
	 * 
	 * @param idx
	 * @return the uniq valuesof the idx array
	 */
	public static int[] uniquify(int[] idx) {
		int[] result = new int[idx.length];
		int n = 0;
		Arrays.sort(idx);

		int previous = -1;
		for (int i : idx) {
			if (previous != i) {
				result[n++] = i;
				previous = i;
			}
		}

		int[] final_result = new int[n];
		System.arraycopy(result, 0, final_result, 0, n);

		return final_result;
	}

	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub

	}

	@Override
	public TXMResult getParent() {
		return corpus;
	}

	@Override
	public String getName() {
		return "ValuesOfQuery";
	}

	@Override
	public String getSimpleName() {
		return "ValuesOfQuery";
	}

	@Override
	public String getDetails() {
		return this.corpus.getName() + " " + this.query + " " + this.prop + " " + this.fmin;
	}

	@Override
	public boolean saveParameters() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean loadParameters() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canCompute() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getResultType() {
		// TODO
		return this.getClass().getSimpleName();
	}

}
