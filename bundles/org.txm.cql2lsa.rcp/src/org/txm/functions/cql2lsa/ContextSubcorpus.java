package org.txm.functions.cql2lsa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.txm.core.results.TXMResult;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.MemCqiClient;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.TXMProgressMonitor;

public class ContextSubcorpus extends TXMResult {

	protected ArrayList<QueryResult> results = new ArrayList<>();

	protected HashMap<String, String> keywordQueriestoName = new HashMap<>();

	protected CQPCorpus corpus;

	protected File keywordsFile;

	protected int contextSize;

	public ContextSubcorpus(CQPCorpus corpus, File keywordsFile, int contextSize) {
		super(corpus);
		this.corpus = corpus;
		this.keywordsFile = keywordsFile;
		this.contextSize = contextSize;
	}

	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator,
			String txtseparator) throws Exception {
		return false;
	}

	@Override
	public void clean() {

	}

	@Override
	public TXMResult getParent() {
		return corpus;
	}

	public Subcorpus getSubcorpus() throws Exception {
		if (results.size() > 0) {
			String name = keywordsFile.getName();
			int idx = name.indexOf(".");
			if (idx > 0) name = name.substring(0, idx);

			Subcorpus sub = corpus.createSubcorpus(name + "_contexts", results.get(0));
			return sub;
		}
		return null;
	}

	@Override
	public boolean _compute(TXMProgressMonitor monitor) throws Exception {


		try {
			if (!(CQPSearchEngine.getCqiClient() instanceof MemCqiClient));

			MemCqiClient CQI = (MemCqiClient) CQPSearchEngine.getCqiClient();

			BufferedReader keywordFileReader = new BufferedReader(new InputStreamReader(
					new FileInputStream(keywordsFile), "UTF-8")); //$NON-NLS-1$
			ArrayList<String> keywordLines = new ArrayList<>();
			String l = keywordFileReader.readLine();
			while (l != null) {
				keywordLines.add(l);
				l = keywordFileReader.readLine();
			}
			keywordFileReader.close();
			System.out.println("Number of keywords lines: " + keywordLines.size());
			System.out.println("context left and right size is: " + contextSize);
			monitor.setTask("Querying keywords...");

			int nkeyword = 0;
			for (String line : keywordLines) {
				String[] split = line.split("=", 2); //$NON-NLS-1$
				if (split.length == 2) {
					CQLQuery q = new CQLQuery(split[1] + " expand to " + contextSize);
					keywordQueriestoName.put(split[1], split[0]);
					results.add(corpus.query(q, "K" + nkeyword++, false));
				}
				monitor.worked(1);
			}
			keywordFileReader.close();

			// Loop over QueryResult to Merge them into one subcorpus
			int n = 0;
			monitor.setTask("Creating subcorpus...");
			while (results.size() > 1) {

				QueryResult q1 = results.get(0);
				QueryResult q2 = results.get(1);
				// System.out.println("Mergin... "+q1+" "+q2);
				// System.out.println("match sizes "+q1.getNMatch()+" "+q2.getNMatch());

				String merge_name = "Merge" + (n++);
				CQI.query(merge_name + "= union " + q1.getQualifiedCqpId() + " " + q2.getQualifiedCqpId() + ";");
				results.remove(0);
				results.remove(0);
				results.add(new QueryResult(merge_name, merge_name, corpus, null));
				monitor.worked(1);
			}

			System.out.println("Done.");
			return true;
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSimpleName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean saveParameters() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean loadParameters() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canCompute() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getResultType() {
		// TODO
		return this.getClass().getSimpleName();
	}

}
