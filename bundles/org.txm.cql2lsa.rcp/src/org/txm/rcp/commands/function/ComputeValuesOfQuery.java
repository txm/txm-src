// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands.function;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.functions.cql2lsa.ValuesOfQuery;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.swt.widget.AssistedQueryWidget;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * write result in file
 * 
 * @author mdecorde.
 */
public class ComputeValuesOfQuery extends BaseAbstractHandler {

	/** The ID. */
	public static String ID = "org.txm.rcp.commands.function.ComputeQueryIndex"; //$NON-NLS-1$

	/** The selection. */
	private IStructuredSelection selection;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Log.info("Compute QueryIndex: start");
		selection = this.getCorporaViewSelection(event);

		Object s = selection.getFirstElement();
		if (!(s instanceof CQPCorpus)) return null;

		final CQPCorpus corpus = (CQPCorpus) s;

		CustomDialog dialog = new CustomDialog(HandlerUtil.getActiveShell(event), corpus);
		if (dialog.open() == Dialog.CANCEL) return null;
		final String sprop = dialog.getLemmaProperty();
		final int fmin = dialog.getFMin();
		final CQLQuery query = dialog.getQuery();

		System.out.println("Query=" + query);
		System.out.println("Fmin=" + fmin);
		System.out.println("Lemma property=" + sprop);

		JobHandler jobhandler = new JobHandler("Compute Values for query '" + query + "'") {

			@Override
			protected IStatus _run(SubMonitor monitor) throws CqiClientException, IOException, CqiServerError {

				Property prop = corpus.getProperty(sprop);
				ValuesOfQuery result = new ValuesOfQuery(corpus, query, prop, fmin);
				result.compute(new File(System.getProperty("user.home"), "lemma_queries.properties"), new TXMProgressMonitor(monitor));
				return Status.OK_STATUS;
			}
		};
		jobhandler.schedule();

		return null;
	}

	class CustomDialog extends Dialog {

		CQPCorpus corpus;

		Text lemmaField;

		Spinner spinner;

		AssistedQueryWidget w;

		int fmin = 10;

		String lemmaproperty = "frlemma";

		String q = "[frpos=\"N.*|ADJ|V.*\"  & frlemma!=\"@card@|être|avoir|faire|pouvoir|devoir|vouloir|falloir|aller|dire|savoir|c'.+|s'.+\"]";

		protected CustomDialog(Shell parentShell, CQPCorpus corpus) {
			super(parentShell);
			this.corpus = corpus;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
		 */
		@Override
		protected Control createDialogArea(Composite parent) {
			parent.setLayout(new GridLayout(2, false));
			// Label l = new Label(parent, SWT.NONE);
			// l.setText("Lemma property");
			// l.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
			// lemmaField = new Text(parent, SWT.BORDER);
			// lemmaField.setText(lemmaproperty);

			Label l = new Label(parent, SWT.NONE);
			l.setText("Query");
			l.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
			w = new AssistedQueryWidget(parent, SWT.NONE, corpus);
			w.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
			w.setText(q);

			l = new Label(parent, SWT.NONE);
			l.setText("Fmin");
			l.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
			spinner = new Spinner(parent, SWT.BORDER);
			try {
				spinner.setMaximum(corpus.getSize());
			}
			catch (CqiClientException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			spinner.setMinimum(0);
			spinner.setSelection(fmin);

			return parent;
		}

		@Override
		public void okPressed() {
			if (spinner != null)
				fmin = spinner.getSelection();
			if (w != null)
				q = w.getQueryString();
			if (lemmaField != null)
				lemmaproperty = lemmaField.getText();

			super.okPressed();
		}

		int getFMin() {
			return fmin;
		}

		String getLemmaProperty() {
			return lemmaproperty;
		}

		CQLQuery getQuery() {
			return new CQLQuery(q);
		}
	}
}
