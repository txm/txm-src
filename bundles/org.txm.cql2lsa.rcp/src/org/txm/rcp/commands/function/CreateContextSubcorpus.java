// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands.function;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.functions.cql2lsa.ContextSubcorpus;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.utils.logger.Log;

/**
 * write result in file
 * 
 * @author mdecorde.
 */
public class CreateContextSubcorpus extends AbstractHandler {

	/** The ID. */
	public static String ID = "org.txm.rcp.commands.function.ComputeQueryIndex"; //$NON-NLS-1$

	/** The selection. */
	private IStructuredSelection selection;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Log.info("Compute QueryIndex: start");
		selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);

		Object s = selection.getFirstElement();
		if (!(s instanceof CQPCorpus)) return null;


		final CQPCorpus corpus = (CQPCorpus) s;
		IInputValidator validator = new IInputValidator() {

			@Override
			public String isValid(String newText) {
				try {
					Integer.parseInt(newText);
				}
				catch (Exception e) {
					return "Input string is malformed: '" + newText + "'";
				}
				return null;
			}
		};


		final File keywordFile = getPropFile(event);
		if (keywordFile == null) return null;

		InputDialog dialog = new InputDialog(HandlerUtil.getActiveShell(event), "Context size", "Enter the context size", "10", validator);
		if (dialog.open() == Dialog.CANCEL) return null;
		final int size = Math.max(Integer.parseInt(dialog.getValue()), 1);

		JobHandler jobhandler = new JobHandler("Compute ExpIIA") {

			@Override
			protected IStatus _run(SubMonitor monitor) throws Exception {

				final ContextSubcorpus sub = new ContextSubcorpus(corpus, keywordFile, size);
				sub.compute(monitor);
				Subcorpus subcorpus = sub.getSubcorpus();
				if (subcorpus != null) {
					syncExec(new Runnable() {

						@Override
						public void run() {
							CorporaView.refresh();
							CorporaView.expand(corpus);
						}
					});
				}
				return Status.OK_STATUS;
			}
		};
		jobhandler.schedule();

		return null;
	}

	public static File getPropFile(ExecutionEvent event) {
		File propFile = null;
		if (LastOpened.getFile(ID) != null)
			propFile = new File(LastOpened.getFolder(ID));

		FileDialog dialog = new FileDialog(HandlerUtil.getActiveShell(event), SWT.OPEN);

		String[] exts = { "*.properties" }; // $NON-NLS
		dialog.setFilterExtensions(exts);
		if (propFile != null) {
			dialog.setFilterPath(propFile.getParent());
			dialog.setFileName(propFile.getName());
		}
		String path = dialog.open();
		if (path == null) return null;
		return new File(path);
	}
}
