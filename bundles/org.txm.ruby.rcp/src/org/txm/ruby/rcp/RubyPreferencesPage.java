package org.txm.ruby.rcp;

import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.ruby.core.RubyPreferences;

/**
 * Index preferences page.
 * 
 * @author mdecorde
 *
 */
public class RubyPreferencesPage extends TXMPreferencePage {

	@Override
	public void init(IWorkbench workbench) {

		this.setPreferenceStore(new TXMPreferenceStore(RubyPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setTitle("Ruby"); //$NON-NLS-1$
		this.setImageDescriptor(IImageKeys.getImageDescriptor("org.txm.ruby.rcp", "icon/ruby.png")); //$NON-NLS-1$
	}

	@Override
	protected void createFieldEditors() {
		this.addField(new StringFieldEditor(RubyPreferences.HOME, "Ruby Home", this.getFieldEditorParent()));

		this.addField(new StringFieldEditor(RubyPreferences.EXECUTABLE_FILENAME, "Ruby executable name", this.getFieldEditorParent()));
	}
}
