// STANDARD DECLARATIONS
package org.txm.macro.oriflamms.analyse

import groovy.transform.Field

import org.kohsuke.args4j.*
import org.txm.*
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.utils.ConsoleProgressBar

if (corpusViewSelection == null) {
	println "Il faut d'abord sélectionner un corpus dans la vue 'Corpus'. Abandon."
	return
}

if (!(corpusViewSelection instanceof Corpus)) {
	println "$corpusViewSelection n'est pas un corpus. Abandon"
	return;
}

@Field @Option(name="tsvFile", usage="TSV output file", widget="CreateFile", required=false, def="file.tsv")
def tsvFile

@Field @Option(name="dist_start", usage="Distance depuis le début de ligne", widget="Integer", required=false, def="0")
		def dist_start

@Field @Option(name="dist_end", usage="Distance depuis la fin de ligne", widget="Integer", required=false, def="1")
		def dist_end

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;
// END OF PARAMETERS

if (dist_start <= 0 && dist_end <= 0) {
	println "Au moins, une des deux distances doit être supérieure à 0"
	return
}
dist_start = Math.abs(dist_start)
dist_end = Math.abs(dist_end)

def corpus = corpusViewSelection
def w = corpus.getStructuralUnit("w");

def wordCorpus = (w == null)
if (wordCorpus) {
	println "Corpus de mots"
} else {
	println "Corpus de lettres"
}

println "Dénombrement des abbréviations de $corpus en '"+(wordCorpus?"mots":"lettres")+"' pour des distances au début de $dist_start et à la fin $dist_end de la ligne"

for (String prop : ["pbid", "pbstart", "pbend","cbid", 
	"cbstart","cbend","lbid", "lbstart", "lbend", "letters-all",
	"letters-alignable", "characters","abbr-n" ]) {
	if (corpus.getProperty(prop) == null) {
		println "Le corpus '$corpus' n'a pas de propriété de mot '$prop'. Abandon."
		return
	}
}

text_su = corpus.getStructuralUnit("text")
text_id = text_su.getProperty("id")
pb_id = corpus.getProperty("pbid")
cb_id = corpus.getProperty("cbid")
lb_id = corpus.getProperty("lbid")
lettersAll = corpus.getProperty("letters-all")
lettersAlignable = corpus.getProperty("letters-alignable")
characters = corpus.getProperty("characters")
abbrn = corpus.getProperty("abbr-n")
form = corpus.getProperty("word")

writer = tsvFile.newWriter("UTF-8")
writer.println "text_id\tpb_id\tcb_id\tlb_id\tline part\tNabbr\tNcharAbbr\ttotal\t%=NcharAbbr/Ntotal"
Partition p = corpus.createPartition("tmp", text_su,text_id)
for (Part part : p.getParts()) {
	processText(part, part.getName())
}
writer.close()
p.delete()
println "Result saved in "+tsvFile.getAbsolutePath()

def processText(Corpus corpus, def text_id) {

	def matches = corpus.getMatches()
	def first_match = matches[0]
	def last_match = matches[-1]
	int start = first_match.getStart()
	int end = last_match.getEnd()

	def CQI = CQPSearchEngine.getCqiClient();
	int[] positions;
	def r = corpus.query(new Query("[pbstart=\"0\"]"), "ABBRORI1", false)
	def pb_pos = r.starts
	r.drop()
	r = corpus.query(new Query("[cbstart=\"0\"]"), "ABBRORI2", false)
	def cb_pos = r.starts
	r.drop()
	r = corpus.query(new Query("[lbstart=\"0\"]"), "ABBRORI3", false)
	def lb_pos = r.starts
	r.drop()

	println "N pb = "+pb_pos.length
	println "N cb = "+cb_pos.length
	println "N lb = "+lb_pos.length
	
	def pb_idx = CQI.cpos2Id(pb_id.getQualifiedName(), pb_pos)
	def cb_idx = CQI.cpos2Id(cb_id.getQualifiedName(), cb_pos)
	def lb_idx = CQI.cpos2Id(lb_id.getQualifiedName(), lb_pos)

	def pb_idx_str = CQI.id2Str(pb_id.getQualifiedName(), pb_idx)
	def cb_idx_str = CQI.id2Str(cb_id.getQualifiedName(), cb_idx)
	def lb_idx_str = CQI.id2Str(lb_id.getQualifiedName(), lb_idx)

	ConsoleProgressBar cpb = new ConsoleProgressBar(lb_pos.size())
	int p = 0 ;
	int c = 0 ;

	for (int l = 0 ; l < lb_pos.size() ; l++) {
		cpb.tick();
		
		while (c < cb_pos.length - 1 && lb_pos[l] > cb_pos[c+1]) {
			c++
		}
		while (p < pb_pos.length - 1 && lb_pos[l] > pb_pos[p+1]) {
			p++
		}

		def line_length;
		if (l == lb_pos.size() -1) line_length = end - lb_pos[l]
		else line_length = lb_pos[l+1] - lb_pos[l]

		def (abbrNs, allLetters, alignableLetters, characters, words) = getInfos(lb_pos[l], line_length);

		processLine(text_id, pb_idx_str[p], cb_idx_str[c], lb_idx_str[l], line_length,
				abbrNs, allLetters, alignableLetters, characters, words)
	}
}

def processLine(def text_id, def pb_id, def cb_id, def lb_id, int length,
		def abbrNs, def allLetters, def alignableLetters, def characters, def words) {

	int Nabbr, NsupAbbr, Ntotal;
	int p2 = length - dist_end
	if (p2 < 0) p2 = length+1;
	if (p2 < dist_start) p2 = dist_start
	
	for (int i = 0 ; i < length ; i++) {

		if (i == dist_start) {
			writer.println "$text_id\t$pb_id\t$cb_id\t$lb_id\ts\t$Nabbr\t$NsupAbbr\t$Ntotal\t"+(100.0*(float)NsupAbbr/(float)Ntotal);

			Nabbr = 0;
			NsupAbbr = 0;
			Ntotal = 0;
		} 
		if (i == p2) {
			writer.println "$text_id\t$pb_id\t$cb_id\t$lb_id\tm\t$Nabbr\t$NsupAbbr\t$Ntotal\t"+(100.0*(float)NsupAbbr/(float)Ntotal);

			Nabbr = 0;
			NsupAbbr = 0;
			Ntotal = 0;
		}

		Nabbr += Integer.parseInt(abbrNs[i]);
		NsupAbbr += allLetters[i].length() - characters[i].length();
		Ntotal += allLetters[i].length();

	}
	writer.println "$text_id\t$pb_id\t$cb_id\t$lb_id\te\t$Nabbr\t$NsupAbbr\t$Ntotal\t"+(100.0*(float)NsupAbbr/(float)Ntotal);
}

def getInfos(int from ,int length) {

	int[] positions = new int[length]
	for (int i = 0 ; i < length ; i++) positions[i] = from++;
	def CQI = CQPSearchEngine.getCqiClient();

	def abbrNs = CQI.cpos2Str(abbrn.getQualifiedName(), positions);
	def allLetters = CQI.cpos2Str(lettersAll.getQualifiedName(), positions);
	def alignableLetters = CQI.cpos2Str(lettersAlignable.getQualifiedName(), positions);
	def characters = CQI.cpos2Str(characters.getQualifiedName(), positions);
	def words = CQI.cpos2Str(form.getQualifiedName(), positions);
	
	return [abbrNs,allLetters,alignableLetters,characters,words]
}