// STANDARD DECLARATIONS
package org.txm.macro.oriflamms.analyse

import groovy.transform.Field
import org.kohsuke.args4j.*
import org.txm.*
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.utils.ConsoleProgressBar

if (corpusViewSelection == null) {
	println "Il faut d'abord sélectionner un corpus dans la vue 'Corpus'. Abandon."
	return
}

if (!(corpusViewSelection instanceof Corpus)) {
	println "$corpusViewSelection n'est pas un corpus. Abandon"
	return;
}

@Field @Option(name="tsvFile", usage="TSV output file", widget="CreateFile", required=false, def="file.tsv")
def tsvFile

@Field @Option(name="entities", usage="1	2	3", metaVar="name	persName	placeName	orgName	roleName", widget="StructuralUnits", required=false, def="persName")
def entities

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

entities = entities.split(",")

// END OF PARAMETERS

println "corpora selection: "+corpusViewSelection

def corpus = corpusViewSelection
CQI = CQPSearchEngine.getCqiClient();
def w = corpus.getStructuralUnit("w");

def wordCorpus = (w == null)
if (wordCorpus) {
	println "Corpus de mots"
} else {
	println "Corpus de lettres"
}

println "Dénombrement des abbréviations en '"+(wordCorpus?"mots":"lettres")+"' pour les entités $entities"

for (String prop : ["letters-all", "letters-alignable", "characters","abbr-n"]) {
	if (corpus.getProperty(prop) == null) {
		println "Le corpus '$corpus' n'a pas de propriété de mot '$prop'. Abandon."
		return
	}
}
for (String structName : entities) {
	StructuralUnit su = corpus.getStructuralUnit(structName);
	if (su == null) {
		println "Corpus '$corpus' has no structure '$structName'. Abandon."
		return
	} else if (su.getProperty("n") == null) {
		println "Le corpus '$corpus' n'a pas de structure'$structName' avec une propriété 'n'. Abandon."
		return
	}
}
structures = []
for (String structName : entities) structures << corpus.getStructuralUnit(structName).getProperty("n")

text_su = corpus.getStructuralUnit("text")
text_id = text_su.getProperty("id")
pb_id = corpus.getProperty("pbid")
cb_id = corpus.getProperty("cbid")
lb_id = corpus.getProperty("lbid")
lettersAll = corpus.getProperty("letters-all")
lettersAlignable = corpus.getProperty("letters-alignable")
characters = corpus.getProperty("characters")
abbrn = corpus.getProperty("abbr-n")
form = corpus.getProperty("word")

writer = tsvFile.newWriter("UTF-8")
writer.println "text_id\tpb_id\tcb_id\tlb_id\tentity\tNabbr\tNcharAbbr\ttotal\t%=NcharAbbr/Ntotal"
Partition p = corpus.createPartition("tmp", text_su,text_id)
for (Part part : p.getParts()) {
	processText(part, part.getName())
}
writer.close()
p.delete()
println "Result saved in "+tsvFile.getAbsolutePath()

def processText(Corpus corpus, def text_id) {

	def matches = corpus.getMatches()
	def first_match = matches[0]
	def last_match = matches[-1]
	int start = first_match.getStart()
	int end = last_match.getEnd()

	def CQI = CQPSearchEngine.getCqiClient();
	int[] positions;
	def r = corpus.query(new Query("[pbstart=\"0\"]"), "ABBRORI1", false)
	def pb_pos = r.starts
	r.drop()
	r = corpus.query(new Query("[cbstart=\"0\"]"), "ABBRORI2", false)
	def cb_pos = r.starts
	r.drop()
	r = corpus.query(new Query("[lbstart=\"0\"]"), "ABBRORI3", false)
	def lb_pos = r.starts
	r.drop()

	println "N pb = "+pb_pos.length
	println "N cb = "+cb_pos.length
	println "N lb = "+lb_pos.length
	
	def pb_idx = CQI.cpos2Id(pb_id.getQualifiedName(), pb_pos)
	def cb_idx = CQI.cpos2Id(cb_id.getQualifiedName(), cb_pos)
	def lb_idx = CQI.cpos2Id(lb_id.getQualifiedName(), lb_pos)

	def pb_idx_str = CQI.id2Str(pb_id.getQualifiedName(), pb_idx)
	def cb_idx_str = CQI.id2Str(cb_id.getQualifiedName(), cb_idx)
	def lb_idx_str = CQI.id2Str(lb_id.getQualifiedName(), lb_idx)

	ConsoleProgressBar cpb = new ConsoleProgressBar(lb_pos.size())
	int p = 0 ;
	int c = 0 ;

	for (int l = 0 ; l < lb_pos.size() ; l++) {
		cpb.tick();
		
		// get the current column milestone
		while (c < cb_pos.length - 1 && lb_pos[l] > cb_pos[c+1]) { 
			c++
		}
		// get the current page milestone
		while (p < pb_pos.length - 1 && lb_pos[l] > pb_pos[p+1]) { 
			p++
		}

		def line_length; // compute line length
		if (l == lb_pos.size() -1) line_length = end - lb_pos[l]
		else line_length = lb_pos[l+1] - lb_pos[l]


		def (abbrNs, allLetters, alignableLetters, characters, words, structuresPositions) = getInfos(lb_pos[l], line_length);

		processLine(text_id, pb_idx_str[p], cb_idx_str[c], lb_idx_str[l], line_length, abbrNs, allLetters, alignableLetters, characters, words, structuresPositions)
	}
}

def getInfos(int from ,int length) {

	int[] positions = new int[length]
	for (int i = 0 ; i < length ; i++) positions[i] = from++;
	
	def abbrNs = CQI.cpos2Str(abbrn.getQualifiedName(), positions);
	def allLetters = CQI.cpos2Str(lettersAll.getQualifiedName(), positions);
	def alignableLetters = CQI.cpos2Str(lettersAlignable.getQualifiedName(), positions);
	def characters = CQI.cpos2Str(characters.getQualifiedName(), positions);
	def words = CQI.cpos2Str(form.getQualifiedName(), positions);
	def structuresPositions = [:]
	for (def structProp : structures) {
		structuresPositions[structProp.getFullName()] = CQI.cpos2Struc(structProp.getQualifiedName(), positions);
	}
	
	return [abbrNs,allLetters,alignableLetters,characters,words, structuresPositions]
}

def processLine(def text_id, def pb_id, def cb_id, def lb_id, int length,
		def abbrNs, def allLetters, def alignableLetters, def characters, def words, def structuresPositions) {

	int NabbrTotal = 0, NsupAbbrTotal = 0, NtotalTotal = 0;
	for (int i = 0 ; i < length ; i++) {
		NabbrTotal += Integer.parseInt(abbrNs[i]);
		NsupAbbrTotal += allLetters[i].length() - characters[i].length();
		NtotalTotal += allLetters[i].length();
	}

	for (def strutcProp : structures) {
		int Nabbr = 0, NsupAbbr = 0, Ntotal = 0;
		def structureP = structuresPositions[strutcProp.getFullName()]

		for (int i = 0 ; i < length ; i++) {
			if (structureP[i] >= 0) { // the position is in the structure
				Nabbr += Integer.parseInt(abbrNs[i]);
				NsupAbbr += allLetters[i].length() - characters[i].length();
				Ntotal += allLetters[i].length();
			}
		}
	
		writer.println "$text_id\t$pb_id\t$cb_id\t$lb_id\t"+strutcProp.getStructuralUnit()+"\t$Nabbr\t$NsupAbbr\t$Ntotal\t"+((float)NsupAbbr/(float)Ntotal);
		NabbrTotal -= Nabbr
		NsupAbbrTotal -= NsupAbbr
		NtotalTotal -= Ntotal
	}
	
	int Nabbr = 0, NsupAbbr = 0, Ntotal = 0;	
	writer.println "$text_id\t$pb_id\t$cb_id\t$lb_id\t#REST\t$NabbrTotal\t$NsupAbbrTotal\t$NtotalTotal\t"+((float)NsupAbbrTotal/(float)NtotalTotal);
}
