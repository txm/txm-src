// STANDARD DECLARATIONS
package org.txm.macro.oriflamms.analyse

import org.apache.tools.ant.types.resources.selectors.InstanceOf;
import org.kohsuke.args4j.*

import groovy.transform.Field
import java.util.regex.Pattern
import org.txm.*
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.Corpus
import org.txm.searchengine.cqp.corpus.Part
import org.txm.searchengine.cqp.corpus.Partition
import org.txm.searchengine.cqp.corpus.StructuralUnit
import org.txm.searchengine.cqp.corpus.query.Query
import org.txm.utils.ConsoleProgressBar

// BEGINNING OF PARAMETERS

if (!(corpusViewSelection instanceof Corpus)) {
	println "Corpora view selection must be a Corpus"
	return;
}

Corpus corpus = corpusViewSelection;
if (!corpus.getID().endsWith("C")) {
	println "The selected Corpus is not a character corpus."
	return;
}

@Field @Option(name="tsvFile", usage="TSV output file", widget="CreateFile", required=false, def="file.tsv")
tsvFile

@Field @Option(name="query", usage="The query", widget="Query", required=true, def='<s>[]')
query

@Field @Option(name="sign", usage="The sign", widget="String", required=false, def="amaj")
sign

@Field @Option(name="allograph", usage="The allograph", widget="String", required=false, def="amaj")
allograph

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

println "Dénombrement des allographes '$allograph' de signe '$sign' dans le contexte '$query'"

for (String prop : ["sign", "allograph-expert", "allograph-auto", "characters"]) {
	if (corpus.getProperty(prop) == null) {
		println "Le corpus '$corpus' n'a pas de propriété de mot '$prop'. Abandon."
		return
	}
}

signRegExp = ~/$sign/
characterRegExp = ~/$allograph/

CQI = CQPSearchEngine.getCqiClient();
text_su = corpus.getStructuralUnit("text")
text_id = text_su.getProperty("id")
pb_id = corpus.getProperty("pbid")
cb_id = corpus.getProperty("cbid")
lb_id = corpus.getProperty("lbid")
sign_property = corpus.getProperty("sign")
allograph_expert_property = corpus.getProperty("allograph-expert")
allograph_auto_property = corpus.getProperty("allograph-auto")
characters_property = corpus.getProperty("characters")

writer = tsvFile.newWriter("UTF-8")
writer.println "text_id\tpb_id\tcb_id\tlb_id\tsign\tchar\tcount\t%/sign"
Partition p = corpus.createPartition("tmp", text_su,text_id)
ConsoleProgressBar cpb = new ConsoleProgressBar(p.getNPart())
for (Part part : p.getParts()) {
	processText(part, part.getName())
	cpb.tick();
}
writer.close()
p.delete()
println "Result saved in "+tsvFile.getAbsolutePath()

def processText(Corpus corpus, def text_id) {

	def matches = corpus.getMatches()
	def first_match = matches[0]
	def last_match = matches[-1]
	int start = first_match.getStart()
	int end = last_match.getEnd()

	int[] positions;
	def r = corpus.query(new Query("[pbstart=\"0\"]"), "ABBRORI1", false)
	def pb_pos = r.starts
	r.drop()
	r = corpus.query(new Query("[cbstart=\"0\"]"), "ABBRORI2", false)
	def cb_pos = r.starts
	r.drop()
	r = corpus.query(new Query("[lbstart=\"0\"]"), "ABBRORI3", false)
	def lb_pos = r.starts
	r.drop()

//	println "N pb = "+pb_pos.length
//	println "N cb = "+cb_pos.length
//	println "N lb = "+lb_pos.length
	
	def pb_idx = CQI.cpos2Id(pb_id.getQualifiedName(), pb_pos)
	def cb_idx = CQI.cpos2Id(cb_id.getQualifiedName(), cb_pos)
	def lb_idx = CQI.cpos2Id(lb_id.getQualifiedName(), lb_pos)

	def pb_idx_str = CQI.id2Str(pb_id.getQualifiedName(), pb_idx)
	def cb_idx_str = CQI.id2Str(cb_id.getQualifiedName(), cb_idx)
	def lb_idx_str = CQI.id2Str(lb_id.getQualifiedName(), lb_idx)

	int p = 0 ;
	int c = 0 ;

	for (int l = 0 ; l < lb_pos.size() ; l++) {
		
		// get the current column milestone
		while (c < cb_pos.length - 1 && lb_pos[l] > cb_pos[c+1]) {
			c++
		}
		// get the current page milestone
		while (p < pb_pos.length - 1 && lb_pos[l] > pb_pos[p+1]) {
			p++
		}

		def line_length; // compute line length
		if (l == lb_pos.size() -1) line_length = end - lb_pos[l]
		else line_length = lb_pos[l+1] - lb_pos[l]


		def (signs, allographs_expert, allographs_auto, characters) = getInfos(lb_pos[l], line_length);

		processLine(text_id, pb_idx_str[p], cb_idx_str[c], lb_idx_str[l], line_length, signs, allographs_expert, allographs_auto, characters)
	}
}

def getInfos(int from ,int length) {

	int[] positions = new int[length]
	for (int i = 0 ; i < length ; i++) positions[i] = from++;
	
	def signs = CQI.cpos2Str(sign_property.getQualifiedName(), positions);
	def allographs_expert = CQI.cpos2Str(allograph_expert_property.getQualifiedName(), positions);
	def allographs_auto = CQI.cpos2Str(allograph_auto_property.getQualifiedName(), positions);
	def characters = CQI.cpos2Str(characters_property.getQualifiedName(), positions);
	
	return [signs,allographs_expert,allographs_auto, characters]
}

def processLine(def text_id, def pb_id, def cb_id, def lb_id, int length,
	def signs, def allographs_expert, def allographs_auto,def characters) {
	//println "$sign -> $signs $characters"
	def count_signs = [:]
	
	for (int i = 0 ; i < length ; i++) {
		
		if (signRegExp.matcher(signs[i]).find() && characterRegExp.matcher(characters[i]).find()) {
			if (!count_signs.containsKey(signs[i])) count_signs[signs[i]] = [:];
			def counts = count_signs[signs[i]]
			
			if (!counts.containsKey(characters[i])) counts[characters[i]] = 0;
			counts[characters[i]] = counts[characters[i]] + 1;
		}
	}

	for (def s : count_signs.keySet()) {
		def counts = count_signs[s]
		def sum = counts.values().sum()
		for (def c : counts.keySet()) {
			writer.println "$text_id\t$pb_id\t$cb_id\t$lb_id\t"+s+"\t"+c+"\t"+counts[c]+"\t"+((float)counts[c]/(float)sum);
		}
	}
}
