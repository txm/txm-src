package org.txm.macro.oriflamms.prepare

import java.io.File;
import java.io.InputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLResolver;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.DTD;
import javax.xml.stream.events.EntityReference;
import org.txm.utils.*
import org.txm.utils.io.*

public class CopyXMLFiles {
	File xmlFile;
	File outDir;
	def dtdFiles = []
	
	public CopyXMLFiles(File xmlFile) {
		this.xmlFile = xmlFile;
	}
	
	public def copy(File outDir) {
		XMLInputFactory factory;
		XMLStreamReader parser;
		InputStream inputData = xmlFile.toURI().toURL().openStream();
		factory = XMLInputFactory.newInstance();
		factory.setXMLResolver(new XMLResolver() {
			@Override
			public Object resolveEntity(String publicID, String systemID,
					String baseURI, String namespace) throws XMLStreamException {
				//println "Resolving $publicID $systemID $baseURI $namespace"
				File srcFile = new File(xmlFile.getParentFile(), systemID);
				dtdFiles << srcFile
				return srcFile.toURI().toURL());
			}
		});
	
		parser = factory.createXMLStreamReader(inputData);
		
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			
		}
		
		dtdFiles << xmlFile
		for (File dtd : dtdFiles) {
			File cpy = new File(outDir, dtd.getName());
			FileCopy.copy(dtd, cpy);
		}
		return dtdFiles
	}
	
	public static void main(def args) {
		File xmlFile = new File("/home/mdecorde/Téléchargements/Inscriptions1.xml")
		File outDir = new File("/home/mdecorde/Téléchargements/test")
		outDir.mkdir()
		CopyXMLFiles cdf = new CopyXMLFiles(xmlFile)
		println cdf.copy(outDir)
	}
}