package org.txm.macro.oriflamms.prepare

import java.util.regex.Pattern
import org.codehaus.groovy.transform.trait.SuperCallTraitTransformer;
import org.txm.importer.StaxIdentityParser;
import org.txm.scripts.importer.StaxParser;
import org.txm.utils.FileUtils

class OntologiesProjection extends StaxIdentityParser {

	File xmlFile

	String wordTag
	String textname
	String milestone
	String group

	HashMap links = [:]
	HashMap prefixDefsPatterns = [:]
	HashMap<String, HashMap> ggly_ontologies = [:]
	HashMap<String, HashMap> lgly_ontologies = [:]

	String current_ontology_link_file_name

	File ontologies_links_directory;

	public OntologiesProjection(File xmlFile, File corpusDirectory) {
		super(xmlFile)

		this.xmlFile = xmlFile
		this.ontologies_links_directory = new File(corpusDirectory, "ontologies_links")

		textname = FileUtils.stripExtension(xmlFile)
		textname = textname.replaceAll("-c", "")

		this.wordTag = "c";
	}

	public def buildGGlyOntology(String prefix) {
		String path = prefixDefsPatterns.get(prefix)[1];
		int idx = path.indexOf("#")
		if (idx > 0) path = path.substring(0, idx)

		File ggly_ontology_file = new File(xmlFile.getParentFile(), "../"+path)
		//println "ggly_ontology_file=$ggly_ontology_file "+ggly_ontology_file.exists()
		if (!ggly_ontology_file.exists()) {
			println "WARNING: cannot found global ontology file: $ggly_ontology_file"
			return false
		}
		def global_ontologies = [:]
		def unicode_global_ontologies = [:]
		//println "parse $ggly_ontology_file"
		StaxParser pontologies = new StaxParser(ggly_ontology_file) {
					boolean startChar = false, startLocalName = false, startValue = false, startMapping = false;
					String unicodeChar, standardizedChar, subtype, type;
					String id, charLocalName, charValue;
					StringBuilder c = new StringBuilder();

					void processStartElement() {
						if (localname.equals("char")) {
							// get id
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
								if (parser.getAttributeLocalName(i).equals("id")) {
									id = parser.getAttributeValue(i)
									break;
								}
							}
							startChar = true;
							c.setLength(0);
						} else if (localname.equals("mapping")) {
							subtype = "";
							type = "";
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
								if (parser.getAttributeLocalName(i).equals("subtype")) {
									subtype = parser.getAttributeValue(i)
								} else if (parser.getAttributeLocalName(i).equals("type")) {
									type = parser.getAttributeValue(i)
								}
							}
							startMapping = true;
							c.setLength(0);
						} else if (localname.equals("localName")) {
							startLocalName = true;
							c.setLength(0);
						} else if (localname.equals("value")) {
							startLocalName = true;
							c.setLength(0);
						}
					}

					void processCharacters() {
						//if (startChar) c.append(parser.getText());
						if (startMapping) c.append(parser.getText());
						else if (startLocalName) c.append(parser.getText());
						else if (startValue) c.append(parser.getText());
					}

					void processEndElement() {
						if (localname.equals("char")) {
							startChar = false;
							global_ontologies[id] = ["standard":standardizedChar, "unicode":unicodeChar, "value":charValue, "localname":charLocalName];
							unicode_global_ontologies[unicodeChar] = standardizedChar
						} else if (localname.equals("mapping")) {
							if (subtype.equals("Unicode")) {
								unicodeChar = c.toString().trim();
							} else if (type.equals("standardized")) {
								standardizedChar = c.toString().trim();
							}
							startMapping = false;
						} else if (localname.equals("localName")) {
							charLocalName = c.toString().trim()
							startLocalName = false;
						} else if (localname.equals("value")) {
							charValue = c.toString().trim()
							startValue = false;
						}
					}
				};
		pontologies.process();
		ggly_ontologies[prefix] = [global_ontologies, unicode_global_ontologies]
		//println ggly_ontologies
		return true
	}

	public def buildLGlyOntology(String prefix) {
		String path = prefixDefsPatterns.get(prefix)[1];
		int idx = path.indexOf("#")
		if (idx > 0) path = path.substring(0, idx)

		//File lgly_ontology_file = new File(xmlFile.getParentFile(), "../"+path) // add "../" because we are in txm/<corpus>-c directory
		File lgly_ontology_file = new File(ontologies_links_directory, textname+"-ontolinks.xml") // add "../" because we are in txm/<corpus>-c directory
		//println "lgly_ontology_file=$lgly_ontology_file "+lgly_ontology_file.exists()
		if (!lgly_ontology_file.exists()) {
			println "WARNING: cannot find Local ontology file $lgly_ontology_file"
			return false
		}

		def local_ontologies = [:]
		//println "parse $lgly_ontology_file"
		StaxParser pontologies = new StaxParser(lgly_ontology_file) {
					boolean startNote = false
					String id, change, parent;
					StringBuilder c = new StringBuilder();
					def glyph = [:]

					void processStartElement() {
						if (localname.equals("glyph")) {
							// get id
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
								change = ""
								if (parser.getAttributeLocalName(i).equals("id")) {
									id = parser.getAttributeValue(i)
								} else if (parser.getAttributeLocalName(i).equals("change")) {
									change = parser.getAttributeValue(i)
								}
							}
							glyph = ["change":change, "id":id] // new glyph
							parent = null
						} else if (localname.equals("note")) {
							startNote = true;
							c.setLength(0);
						}
					}

					void processCharacters() {
						if (startNote) c.append(parser.getText());
					}

					void processEndElement() {
						if (localname.equals("char")) {
							if (parent != null)
								glyph["parent"] = local_ontologies[parent]
							local_ontologies[id] = glyph
						} else if (localname.equals("note")) {
							parent = c.toString().trim()
							startNote = false;
						}
					}
				};
		pontologies.process();
		lgly_ontologies[prefix] = local_ontologies

		return true
	}

	public def loadOntologyLinkFile(String name) {
		links = [:]
		prefixDefsPatterns = ["ggly":[Pattern.compile("([a-z]+)"), '../../charDecl.xml#$1'],
			"lgly":[Pattern.compile("([a-z]+)"), '../ontologies/'+textname+'.xml#$1'],
			"txt":[Pattern.compile("([a-z]+)"), '../texts/'+textname+'.xml#$1']]
			
		lgly_ontologies = [:]
		ggly_ontologies = [:]
		File ontology_link_file = new File(ontologies_links_directory, name)
		if (!ontology_link_file.exists()) {
			println "WARNING: no ontology link file: "+ontology_link_file
			return
		}

		StaxParser pLinks = new StaxParser(ontology_link_file) {
					void processStartElement() {
						if (localname.equals("linkGrp")) {
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
								if (parser.getAttributeLocalName(i).equals("type")) {
									group = parser.getAttributeValue(i)
									break
								}
							}
						} else if (localname.equals("prefixDef")) {
							String ident, matchPattern, replacementPattern;
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
								if (parser.getAttributeLocalName(i).equals("ident")) {
									ident = parser.getAttributeValue(i)
								} else if (parser.getAttributeLocalName(i).equals("matchPattern")) {
									matchPattern = parser.getAttributeValue(i)
								} else if (parser.getAttributeLocalName(i).equals("replacementPattern")) {
									replacementPattern = parser.getAttributeValue(i)
								}
							}
							if (!ident.equals("txt")) {
								prefixDefsPatterns[ident] = [Pattern.compile(matchPattern), replacementPattern];
								OntologiesProjection.this.getOntology(ident)
							}
						} else if (localname.equals("link")) {
							String target = "";

							for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
								if (parser.getAttributeLocalName(i).equals("target")) {
									target = parser.getAttributeValue(i)
									break
								}
							}

							def split = target.split(" ", 2) // first part word id next part are the ontologies id
							links[split[0].substring(4)] = split[1].split(" ")
						}
					}
				};
		pLinks.process();
		//		println "links size: "+links.size()
		//		println "ggly_ontologies size: "+ggly_ontologies.size()
		//		println "lgly_ontologies size: "+lgly_ontologies.size()
	}

	public def getOntology(String prefix) {
		if (prefix.startsWith("ggly")) {
			if (!ggly_ontologies.containsKey(prefix)) buildGGlyOntology(prefix);
			return ggly_ontologies.get(prefix)
		} else if (prefix.startsWith("lgly")) {
			if (!lgly_ontologies.containsKey(prefix)) buildLGlyOntology(prefix);
			return lgly_ontologies.get(prefix)
		}
	}

	public void processStartElement() {
		super.processStartElement();
		if (localname.equals("milestone")) {
			String id = "";
			String unit= "";
			for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
				if (parser.getAttributeLocalName(i).equals("id")) {
					id = parser.getAttributeValue(i)
				} else if (parser.getAttributeLocalName(i).equals("unit")) {
					unit = parser.getAttributeValue(i)
				}
			}

			if (unit.equals("surface")) {
				milestone = id;
			}
		} else if (localname.equals(wordTag)) {
			String id = "";
			String characters = "";
			for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
				if (parser.getAttributeLocalName(i).equals("id")) {
					id = parser.getAttributeValue(i)
				} else if (parser.getAttributeLocalName(i).equals("characters")) {
					characters = parser.getAttributeValue(i)
				}
			}

			String ontology_link_file_name = textname+"-ontolinks.xml"
			if (!current_ontology_link_file_name.equals(ontology_link_file_name)) { // rebuild hashmaps
				current_ontology_link_file_name = ontology_link_file_name
				loadOntologyLinkFile(ontology_link_file_name);
				getOntology("ggly")
			}

			String sign = null, allographExpert = null, allographAutomatic = null; // default value is attribute characters

			//AUTO ALLOGRAPH
			if (links.containsKey(id))
				for (String link : links[id]) { // automatic allograph loop
					if (link.startsWith("lgly")) {
						int idx = link.indexOf(":")
						link = link.substring(idx+1);
						if (link.startsWith("auto_")) { // automatic lgly
							if (allographAutomatic == null) allographAutomatic = link.substring(5)
							else if (allographAutomatic.length()+5 < link.length()) allographAutomatic = link.substring(5)
						} else { // manual lgly

						}
					}
				}
			if (allographAutomatic == null) allographAutomatic = characters;

			//EXPERT ALLOGRAPH
			if (links.containsKey(id))
				for (String link : links[id]) { // expert allograph loop, try to find a ggly entity
					//getOntology("ggly")
					if (link.startsWith("ggly")) {
						int idx = link.indexOf(":")
						def prefix = link.substring(0, idx);
						link = link.substring(idx+1);

						def onto = getOntology(prefix)
						if (onto != null) {
							def charOnto = onto[0][link];
							if (charOnto != null) {
								String localname = charOnto["localname"]
								String value =  charOnto["value"]
								if ("entity".equals(localname)) {
									allographExpert = value
								}
							}
						}
					}
				}
			if (allographExpert == null)
				if (links.containsKey(id))
					for (String link : links[id]) { // expert allograph loop, try to find the longest non-autolgly entity
						if (link.startsWith("lgly")) {
							int idx = link.indexOf(":")
							link = link.substring(idx+1);
							if (!link.startsWith("auto_")) { // non automatic lgly
								//println "link= "+link
								if (allographExpert == null) allographExpert = link
								else if (allographExpert.length()+5 < link.length()) allographExpert = link
							}
						}
					}
			if (allographExpert == null) allographExpert = allographAutomatic;

			//SIGN
			if (sign == null)
				if (links.containsKey(id))
					for (String link : links[id]) { // expert allograph loop, try to find the shortest ggly entity
						//getOntology("ggly")
						if (link.startsWith("ggly")) {
							int idx = link.indexOf(":")
							def prefix = link.substring(0, idx);
							link = link.substring(idx+1);

							def onto = getOntology(prefix)
							if (onto != null) {
								def charOnto = onto[0][link];
								if (charOnto != null) {
									sign = charOnto["standard"]
								}
							}
						}
					}
			if (sign == null)
				if (links.containsKey(id))
					for (String link : links[id]) { // sign loop, try to find the shortest non-autolgly entity
						if (link.startsWith("lgly")) {
							int idx = link.indexOf(":")
							link = link.substring(idx+1);
							if (!link.startsWith("auto_")) { // non automatic lgly
								if (sign == null) sign = link
								else if (sign.length()+5 > link.length()) sign = link
							}
						}
					}
			if (sign == null) {
				for (def ggly : ggly_ontologies.values()) {
					def chars = ggly[1]
					if (chars.containsKey(characters)) sign = chars[characters];
				}
			}
			if (sign == null) sign = characters.toLowerCase();

			//println "write characters attributes characters=$characters sign=$sign allograph-expert=$allographExpert allograph-auto=$allographAutomatic"
			writer.writeAttribute("sign", sign)
			writer.writeAttribute("allograph-expert", allographExpert)
			writer.writeAttribute("allograph-auto", allographAutomatic)
		}
	}

	public static void main(String[] args) {
		File corpusDirectory = new File("/home/mdecorde/TEMP/testori/qgraal_cmTest")
		File xmlFile = new File(corpusDirectory, "txm/qgraal_cmTest-c/qgraal_cmTest-c_surf_qgraal_cmTest_lyonbm_pa77-160.xml")
		File outputFile = new File(corpusDirectory, "txm/qgraal_cmTest-c/out.xml")

		OntologiesProjection cp = new OntologiesProjection(xmlFile, corpusDirectory);
		println cp.process(outputFile)
	}
}
