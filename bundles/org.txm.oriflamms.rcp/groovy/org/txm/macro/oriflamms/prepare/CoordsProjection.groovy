package org.txm.macro.oriflamms.prepare

import org.txm.importer.StaxIdentityParser;
import org.txm.scripts.importer.StaxParser;
import org.txm.utils.FileUtils

class CoordsProjection extends StaxIdentityParser {

	File xmlFile
	File img_links_directory
	File zones_directory

	String wordTag
	String textname
	String milestone

	String current_img_file = ""
	String current_zone_file = ""

	String xmlType;
	String group

	HashMap<String, String> zones = [:]
	HashMap<String, String> links = [:]

	public CoordsProjection(File xmlFile, File img_links_directory, File zones_directory, String wordTag) {
		super(xmlFile)

		this.xmlFile = xmlFile
		this.img_links_directory = img_links_directory
		this.zones_directory = zones_directory
		this.wordTag = wordTag;

		textname = FileUtils.stripExtension(xmlFile)

		int idx = textname.indexOf("-w")
		if (idx > 0) {
			textname = textname.substring(0, idx)
			xmlType = "word"
		}

		idx = textname.indexOf("-c")
		if (idx > 0) {
			textname = textname.substring(0, idx)
			xmlType = "character"
		}
	}

	public void processStartElement() {
		super.processStartElement();
		if (localname.equals("milestone")) {
			String id = "";
			String unit= "";
			for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
				if (parser.getAttributeLocalName(i).equals("id")) {
					id = parser.getAttributeValue(i)
				} else if (parser.getAttributeLocalName(i).equals("unit")) {
					unit = parser.getAttributeValue(i)
				}
			}

			if (unit.equals("surface")) {
				milestone = id;
			}
		} else if (localname.equals(wordTag)) {
			String id = "";
			for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
				if (parser.getAttributeLocalName(i).equals("id")) {
					id = parser.getAttributeValue(i)
					break;
				}
			}

			// load next data if needed
			String img_file_name = textname+"_"+milestone+"-links.xml"
			if (!current_img_file.equals(img_file_name)) { // rebuild hashmaps
				String zone_file_name = textname+"_"+milestone+"-zones.xml"
				loadNextData(img_file_name, zone_file_name);
			}

			//			println "Find coords for word_id="+id+" in "+img_file_name+" and "+zone_file_name
			//			println "zone: "+links[id]
			//			println "coords: "+zones[links[id]]
			if (zones.size() > 0 && links.size() > 0) {
				def coords = zones[links[id]]
				if (coords != null) {
					if (coords[0] == null || coords[1] == null || coords[2] == null || coords[3] == null) {
						println "WARNING one of coordinates is missing: $coords"
					} else {
						writer.writeAttribute("x1", coords[0])
						writer.writeAttribute("y1", coords[1])
						writer.writeAttribute("x2", coords[2])
						writer.writeAttribute("y2", coords[3])
					}
				} else {
					println "WARNING No group for word id="+id+" and link id="+links[id]+" in text "+textname
				}
			}
		}
	}

	protected void loadNextData(String img_file_name, String zone_file_name) {
		File img_link_file = new File(img_links_directory, img_file_name)
		File zone_file = new File(zones_directory, zone_file_name)

		zones.clear()
		links.clear()
		if (zone_file.exists()) {
			StaxParser pZones = new StaxParser(zone_file) {
						void processStartElement() {
							if (localname.equals("zone")) {
								String type = "";
								String idZone = "";
								String ulx, uly, lrx, lry;

								for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
									if (parser.getAttributeLocalName(i).equals("id")) {
										idZone = parser.getAttributeValue(i)
									} else if (parser.getAttributeLocalName(i).equals("type")) {
										type = parser.getAttributeValue(i)
									} else if (parser.getAttributeLocalName(i).equals("ulx")) {
										ulx = parser.getAttributeValue(i)
									} else if (parser.getAttributeLocalName(i).equals("uly")) {
										uly = parser.getAttributeValue(i)
									} else if (parser.getAttributeLocalName(i).equals("lrx")) {
										lrx = parser.getAttributeValue(i)
									} else if (parser.getAttributeLocalName(i).equals("lry")) {
										lry = parser.getAttributeValue(i)
									}
								}

								if (type.equals(xmlType)) {
									zones[idZone] = [ulx, uly, lrx, lry]
								}

							}
						}
					};
			pZones.process();
		}
		if (img_link_file.exists()) {
			StaxParser pLinks = new StaxParser(img_link_file) {
						void processStartElement() {
							if (localname.equals("linkGrp")) {
								for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
									if (parser.getAttributeLocalName(i).equals("type")) {
										group = parser.getAttributeValue(i)
										break
									}
								}
							} else if (localname.equals("link") && group.startsWith(xmlType)) {
								String target = "";

								for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
									if (parser.getAttributeLocalName(i).equals("target")) {
										target = parser.getAttributeValue(i)
										break
									}
								}

								def split = target.split(" ")
								links[split[0].substring(4)] = split[1].substring(4)
							}
						}
					};
			pLinks.process();
		}
		//println "zones size: "+zones.size()
		//println "links size: "+links.size()

		current_img_file = img_file_name
	}

	public static void main(String[] args) {
		File corpusDirectory = new File("/home/mdecorde/TEMP/testori/FontenatTestAlignement")
		File xmlFile = new File(corpusDirectory, "txm/FontenayTest-w/FontenayTest-w.xml")
		File img_links_directory = new File(corpusDirectory, "img_links")
		File zones_directory = new File(corpusDirectory, "zones")

		File outputFile = new File(corpusDirectory, "txm/FontenayTest-w/FontenayTest-w-coords.xml")

		CoordsProjection cp = new CoordsProjection(xmlFile, img_links_directory, zones_directory, "w");
		println cp.process(outputFile)
	}
}
