<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:txm="http://textometrie.org/1.0"
	exclude-result-prefixes="tei edate xd txm" version="2.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" indent="yes" />

	<xd:doc type="stylesheet">
		<xd:short>
			Cette feuille permet de convertir les données de zonage d'image produites
			par le logiciel Oriflamms (0.3.2) vers le format XML-TEI-Oriflamms.
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2015, CNRS / ICAR (Équipe CACTUS)</xd:copyright>
	</xd:doc>

	<xsl:variable name="filename">
		<xsl:analyze-string select="document-uri(.)"
			regex="^(.*)/([^/]+)\.[^/]+$">
			<xsl:matching-substring>
				<xsl:value-of select="regex-group(2)" />
			</xsl:matching-substring>
		</xsl:analyze-string>
	</xsl:variable>

	<xsl:variable name="filedir">
		<xsl:analyze-string select="document-uri(.)"
			regex="^(.*)/([^/]+)\.[^/]+$">
			<xsl:matching-substring>
				<xsl:value-of select="regex-group(1)" />
			</xsl:matching-substring>
		</xsl:analyze-string>
	</xsl:variable>

	<xsl:variable name="corpusid">
		<xsl:value-of select="replace($filename,'-w$','')" />
	</xsl:variable>

	<!-- <xsl:variable name="filenameresult"> <xsl:value-of select="replace($filename,'(-ori)?-w$','')"/> 
		</xsl:variable> -->

	<!--<xsl:param name="textFileName"><xsl:value-of select="$filename"/></xsl:param> -->

	<xsl:param name="imageZoneFileName">
		<xsl:value-of select="concat($filename,'-zones.xml')" />
	</xsl:param>

	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates
				select="*|processing-instruction()|comment()|text()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template
		match="@*|comment()|processing-instruction()|text()">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="/">
		<dummy>Les résultats sont enregistrés dans les dossiers zones et
			img-links</dummy>
		<xsl:apply-templates
			select="//tei:milestone[@unit='surface']" />
	</xsl:template>


	<xsl:template
		match="tei:milestone[@unit='surface' and not(ancestor::tei:supplied)]">

		<!--<xsl:variable name="fileid"> <xsl:value-of select="replace(@xml:id,'^surf_','')"/> 
			</xsl:variable> -->

		<xsl:result-document
			href="{$filedir}/../zones/{$corpusid}_{@xml:id}-zones.xml">
			<TEI xmlns="http://www.tei-c.org/ns/1.0">
				<teiHeader xmlns="http://www.tei-c.org/ns/1.0">
					<fileDesc xmlns="http://www.tei-c.org/ns/1.0">
						<titleStmt xmlns="http://www.tei-c.org/ns/1.0">
							<title xmlns="http://www.tei-c.org/ns/1.0">
								Image zones for
								<xsl:value-of select="@xml:id" />
							</title>
						</titleStmt>
						<publicationStmt
							xmlns="http://www.tei-c.org/ns/1.0">
							<p xmlns="http://www.tei-c.org/ns/1.0">Oriflamms project</p>
						</publicationStmt>
						<sourceDesc xmlns="http://www.tei-c.org/ns/1.0">
							<p>Converted from Oriflamms XML-TEI transcription file</p>
						</sourceDesc>
					</fileDesc>
					<revisionDesc xmlns="http://www.tei-c.org/ns/1.0">
						<change
							when="{format-date(current-date(),'[Y]-[M01]-[D01]')}"
							xmlns="http://www.tei-c.org/ns/1.0">File created</change>
					</revisionDesc>
				</teiHeader>
				<facsimile xml:base="../img/"
					xmlns="http://www.tei-c.org/ns/1.0">
					<surface xmlns="http://www.tei-c.org/ns/1.0"
						xml:id="{replace(@xml:id,'^surf_','surf_img')}">
						<!--<graphic url="{replace(following::tei:pb[1]/@facs,'\.tif$','.jpg')}"/> -->
						<graphic url="{following::tei:pb[1]/@facs}" />
					</surface>
				</facsimile>
			</TEI>
		</xsl:result-document>
		<xsl:result-document
			href="{$filedir}/../img_links/{$corpusid}_{@xml:id}-links.xml">
			<TEI xmlns="http://www.tei-c.org/ns/1.0">
				<teiHeader xmlns="http://www.tei-c.org/ns/1.0">
					<fileDesc xmlns="http://www.tei-c.org/ns/1.0">
						<titleStmt xmlns="http://www.tei-c.org/ns/1.0">
							<title xmlns="http://www.tei-c.org/ns/1.0">
								Linking data for document transcriptions and image zones (
								<xsl:value-of select="$filename" />
								)
							</title>
						</titleStmt>
						<publicationStmt
							xmlns="http://www.tei-c.org/ns/1.0">
							<p xmlns="http://www.tei-c.org/ns/1.0">Oriflamms project ((http://oriflamms.hypotheses.org))</p>
						</publicationStmt>
						<sourceDesc xmlns="http://www.tei-c.org/ns/1.0">
							<p>Converted from Oriflamms XML-TEI transcription file</p>
						</sourceDesc>
					</fileDesc>
					<encodingDesc xmlns="http://www.tei-c.org/ns/1.0">
						<listPrefixDef xmlns="http://www.tei-c.org/ns/1.0">
							<prefixDef xmlns="http://www.tei-c.org/ns/1.0"
								ident="txt" matchPattern="([a-z]+)"
								replacementPattern="../texts/{$corpusid}-c.xml#$1" />
							<prefixDef xmlns="http://www.tei-c.org/ns/1.0"
								ident="img" matchPattern="([a-z]+)"
								replacementPattern="../zones/{$corpusid}_{@xml:id}-zones.xml#$1" />
						</listPrefixDef>
					</encodingDesc>
					<revisionDesc xmlns="http://www.tei-c.org/ns/1.0">
						<change
							when="{format-date(current-date(),'[Y]-[M01]-[D01]')}"
							xmlns="http://www.tei-c.org/ns/1.0">File created</change>
					</revisionDesc>
				</teiHeader>
				<text xmlns="http://www.tei-c.org/ns/1.0">
					<body xmlns="http://www.tei-c.org/ns/1.0">
						<ab type="linking" xmlns="http://www.tei-c.org/ns/1.0">
							<linkGrp type="surfaces"
								xmlns="http://www.tei-c.org/ns/1.0" />
							<linkGrp type="pages" xmlns="http://www.tei-c.org/ns/1.0" />
							<linkGrp type="columns"
								xmlns="http://www.tei-c.org/ns/1.0" />
							<linkGrp type="lines" xmlns="http://www.tei-c.org/ns/1.0" />
							<linkGrp type="words" xmlns="http://www.tei-c.org/ns/1.0" />
							<linkGrp type="characters"
								xmlns="http://www.tei-c.org/ns/1.0" />
						</ab>
					</body>
				</text>
			</TEI>
		</xsl:result-document>
	</xsl:template>


</xsl:stylesheet>