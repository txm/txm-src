// STANDARD DECLARATIONS
package org.txm.macro.oriflamms.prepare

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

File projectsDirectory = new File("/home/mdecorde/TEMP/testori/corpus/")
File xslDirectory = new File("/home/mdecorde/TXM/scripts/macro/org/txm/macro/oriflamms/prepare")

def todo = [
"CHARETTE":["xmlFile":new File(projectsDirectory, "Charrette_Ms_A.xml"),xslFile:new File(projectsDirectory, "oriflamms-convert-mss-dates-oriflammsxml.xsl"),imageDirectory:new File(projectsDirectory, "images"), createArchive:false],
"CHARETTEBIS":["xmlFile":new File(projectsDirectory, "Charrette_Ms_A.xml"),xslFile:new File(projectsDirectory, "oriflamms-convert-mss-dates-oriflammsxml.xsl"),imageDirectory:new File(projectsDirectory, "images"), createArchive:false],
]

for (def k : todo.keySet()) {
	println "*** BUILD $k ***"
	try {
		gse.runMacro(TEI2ProjectMacro, todo.get(k))
	} catch(Exception e) { 
		println "ERROR WHILE PROCESSING $k: "+e; 
		e.printStackTrace();
	}
} 