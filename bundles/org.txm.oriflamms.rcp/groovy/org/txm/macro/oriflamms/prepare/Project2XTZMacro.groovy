package org.txm.macro.oriflamms.prepare;

import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.objects.BaseParameters;
import org.txm.rcp.swt.widget.parameters.*
import org.txm.*
import org.txm.importer.ApplyXsl2
import org.txm.utils.*
import org.txm.utils.io.*
import org.txm.scripts.importer.*
import org.txm.utils.logger.*
import org.txm.utils.xml.UpdateXSLParameters
import org.txm.utils.zip.Zip;

File oriflammsMacroDirectory = new File(Toolbox.getTxmHomePath(), "scripts/macro/org/txm/macro/oriflamms/prepare")
if (!oriflammsMacroDirectory.exists()) {
	println "Oriflamms macro directory not found: $oriflammsMacroDirectory. Aborting"
	return;
}
File wFrontXSLFile = new File(oriflammsMacroDirectory, "txm-front-teioriflammsw-xtz.xsl")
if (!wFrontXSLFile.exists()) {
	println "Oriflamms to XML-XTZ front XSL file is missing: $wFrontXSLFile. Aborting"
	return;
}
File cFrontXSLFile = new File(oriflammsMacroDirectory, "txm-front-teioriflammsc-xtz.xsl")
if (!cFrontXSLFile.exists()) {
	println "Oriflamms to XML-XTZ front XSL file is missing: $cFrontXSLFile. Aborting"
	return;
}

File cSplitXSLFile = new File(oriflammsMacroDirectory, "1-oriflamms-split-surfaces.xsl")
if (!cSplitXSLFile.exists()) {
	println "Oriflamms to XML-XTZ split XSL file is missing: $cSplitXSLFile. Aborting"
	return;
}

File editionXSLFile1 = new File(oriflammsMacroDirectory, "1-default-html.xsl")
if (!editionXSLFile1.exists()) {
	println "Oriflamms to XML-XTZ edition XSL file is missing: $editionXSLFile1."
	return;
}
File editionXSLFile2 = new File(oriflammsMacroDirectory, "2-default-pager.xsl")
if (!editionXSLFile2.exists()) {
	println "Oriflamms to XML-XTZ edition XSL file is missing: $editionXSLFile2."
	return;
}
File editionXSLFile3 = new File(oriflammsMacroDirectory, "3-facsimile-pager.xsl")
if (!editionXSLFile3.exists()) {
	println "Oriflamms to XML-XTZ edition XSL file is missing: $editionXSLFile3."
	return;
}
File cssDirectory = new File(oriflammsMacroDirectory, "css")
if (!cssDirectory.exists()) {
	println "Oriflamms css directory is missing: $cssDirectory."
	return;
}
File jsDirectory = new File(oriflammsMacroDirectory, "js")
if (!jsDirectory.exists()) {
	println "Oriflamms js directory is missing: $jsDirectory."
	return;
}
File imagesDirectory = new File(oriflammsMacroDirectory, "images")
if (!imagesDirectory.exists()) {
	println "Oriflamms images directory is missing: $imagesDirectory."
	return;
}

@Field @Option(name="projectDirectory", usage="Oriflamms' project directory", widget="Folder", required=true, def="project")
File projectDirectory

if (!ParametersDialog.open(this)) return;

File textDirectory = new File(projectDirectory, "texts")

File txmDirectory = new File(projectDirectory, "txm");
if (txmDirectory.exists()) txmDirectory.deleteDir();
txmDirectory.mkdir()
if (!txmDirectory.exists()) {
	println "Error: the 'txm' directory could not be created: $txmDirectory. Aborting."
	return
}

File wDirectory = null
File cDirectory = null
File wFile = null
File cFile = null

def xmlFiles = textDirectory.listFiles(IOUtils.HIDDENFILE_FILTER)
if (xmlFiles == null) return;

for (File xmlFile : xmlFiles) {
	if (xmlFile.getName().endsWith("-w.xml")) {
		String name = FileUtils.stripExtension(xmlFile);
		wDirectory = new File(txmDirectory, name)
		wFile = xmlFile
	} else if (xmlFile.getName().endsWith("-c.xml")) {
		String name = FileUtils.stripExtension(xmlFile);
		cDirectory = new File(txmDirectory, name)
		cFile = xmlFile
	}
}

if (wDirectory == null) {
	println "The Word corpus XML file was not found in $textDirectory. Aborting."
	return
}
if (cDirectory == null) {
	println "The Letter corpus XML file was not found in $textDirectory. Aborting."
	return
}

//Create XML-XTZ source directories
wDirectory.mkdirs()
cDirectory.mkdirs()

// Copy XML files and split character XML file
FileCopy.copy(wFile, new File(wDirectory, wFile.getName()));

ApplyXsl2 builder = new ApplyXsl2(cSplitXSLFile);
def xslParams = ["output-directory":cDirectory.getAbsoluteFile().toURI().toString()];
for (String name : xslParams.keySet()) builder.setParam(name, xslParams.get(name));
if (!builder.process(cFile, null)) {
	println "Error: fail to split $cFile"
	return false
}
if (!ApplyXsl2.processImportSources(cFrontXSLFile, cDirectory.listFiles(IOUtils.HIDDENFILE_FILTER), [:])) {
	println "Error: fail to apply front XSL with $cDirectory files"
	return false
}
// INJECT ontologies 

println "Injecting ontologies..."
for (File f : cDirectory.listFiles(IOUtils.HIDDENFILE_FILTER)) {
	if (f.getName().startsWith(cDirectory.getName())) {
		OntologiesProjection cp = new OntologiesProjection(f, projectDirectory);
		File outputFile = new File(cDirectory, "temp.xml")
		cp.process(outputFile)
		if (outputFile.exists() && f.delete() && outputFile.renameTo(f)) {
		
		} else {
			println "Failed to replace XML file $f with $outputFile"
			return;
		}
	}
}

// INJECT word's coordinates
println "Injecting coordinates..."
File xmlFile = new File(wDirectory, wFile.getName())
File img_links_directory = new File(projectDirectory, "img_links")
File zones_directory = new File(projectDirectory, "zones")
File outputFile = new File(wDirectory, "temp.xml")
CoordsProjection cp = new CoordsProjection(xmlFile, img_links_directory, zones_directory, "w");
if (cp.process(outputFile)) {
	if (outputFile.exists() && xmlFile.delete() && outputFile.renameTo(xmlFile)) {
		
	} else {
		println "Failed to replace XML file $xmlFile with $outputFile"
		return;
	}
} else {
	println "Coordinates injection failed. Aborting"
	return;
}

// Create XSL directories

File wXSLDirectory = new File(wDirectory, "xsl")
File cXSLDirectory = new File(cDirectory, "xsl")

//File cSplitXSLDirectory = new File(cXSLDirectory, "1-split-merge")
//cSplitXSLDirectory.mkdirs()

File wFrontXSLDirectory = new File(wXSLDirectory, "2-front")
//File cFrontXSLDirectory = new File(cXSLDirectory, "2-front")
wFrontXSLDirectory.mkdirs()
//cFrontXSLDirectory.mkdirs()

// Copy Split XSL file
//File newCSplitXSLFile = new File(cSplitXSLDirectory, cSplitXSLFile.getName())
//FileCopy.copy(cSplitXSLFile, newCSplitXSLFile);

// Copy Front XSL file
File newWFrontXSLFile = new File(wFrontXSLDirectory, wFrontXSLFile.getName())
//File newCFrontXSLFile = new File(cFrontXSLDirectory, cFrontXSLFile.getName())
FileCopy.copy(wFrontXSLFile, newWFrontXSLFile);
//FileCopy.copy(cFrontXSLFile, newCFrontXSLFile);

// Copy edition XSL file
File wEditionXSLDirectory = new File(wXSLDirectory, "4-edition")
File cEditionXSLDirectory = new File(cXSLDirectory, "4-edition")
wEditionXSLDirectory.mkdirs()
cEditionXSLDirectory.mkdirs()
File newWEditionXSLFile1 = new File(wEditionXSLDirectory, editionXSLFile1.getName())
File newCEditionXSLFile1 = new File(cEditionXSLDirectory, editionXSLFile1.getName())
FileCopy.copy(editionXSLFile1, newWEditionXSLFile1);
FileCopy.copy(editionXSLFile1, newCEditionXSLFile1);
File newWEditionXSLFile2 = new File(wEditionXSLDirectory, editionXSLFile2.getName())
File newCEditionXSLFile2 = new File(cEditionXSLDirectory, editionXSLFile2.getName())
FileCopy.copy(editionXSLFile2, newWEditionXSLFile2);
FileCopy.copy(editionXSLFile2, newCEditionXSLFile2);
File newWEditionXSLFile3 = new File(wEditionXSLDirectory, editionXSLFile3.getName())
File newCEditionXSLFile3 = new File(cEditionXSLDirectory, editionXSLFile3.getName())
FileCopy.copy(editionXSLFile3, newWEditionXSLFile3);
FileCopy.copy(editionXSLFile3, newCEditionXSLFile3);

//patch XSL files with image directory path and set the 'word-element' xsl param
File projectImgDirectory = new File(projectDirectory, "img")
def parameters = ["image-directory":projectImgDirectory.getAbsolutePath(), "word-element":"w"]
println "update $newWEditionXSLFile3 with $parameters"
UpdateXSLParameters p = new UpdateXSLParameters(newWEditionXSLFile3);
if (!p.process(parameters)) {
	println "Fail to patch $newWEditionXSLFile3"
	return
}
parameters = ["image-directory":projectImgDirectory.getAbsolutePath(), "word-element":"c"]
println "update $newCEditionXSLFile3 with $parameters"
UpdateXSLParameters p2 = new UpdateXSLParameters(newCEditionXSLFile3);
if (!p2.process(parameters)) {
	println "Fail to patch $newCEditionXSLFile3"
	return
}

// Copy js and images directories
File wCSSDirectory =  new File(wDirectory, cssDirectory.getName())
wCSSDirectory.mkdir()
File wJsDirectory =  new File(wDirectory, jsDirectory.getName())
wJsDirectory.mkdir()
File wImagesDirectory =  new File(wDirectory, imagesDirectory.getName())
wImagesDirectory.mkdir()
File cCSSDirectory =  new File(cDirectory, cssDirectory.getName())
cCSSDirectory.mkdir()
File cJsDirectory =  new File(cDirectory, jsDirectory.getName())
cJsDirectory.mkdir()
File cImagesDirectory =  new File(cDirectory, imagesDirectory.getName())
cImagesDirectory.mkdir()
FileCopy.copyFiles(cssDirectory, wCSSDirectory);
FileCopy.copyFiles(jsDirectory, wJsDirectory);
FileCopy.copyFiles(imagesDirectory, wImagesDirectory);
FileCopy.copyFiles(cssDirectory, cCSSDirectory);
FileCopy.copyFiles(jsDirectory, cJsDirectory);
FileCopy.copyFiles(imagesDirectory, cImagesDirectory);

// Prepare import.xml files
File wImportXMLFile = new File(wDirectory, "import.xml")
File cImportXMLFile = new File(cDirectory, "import.xml")

BaseParameters.createEmptyParams(wImportXMLFile, AsciiUtils.buildId(wDirectory.getName()).toUpperCase())
BaseParameters wParams = new BaseParameters(wImportXMLFile)
wParams.load()
wParams.setSkipTokenization(true);
wParams.setWordElement("w")
wParams.setDoAnnotation(false)
wParams.setAnnotationLang("fr")
wParams.setWordsPerPage(9999999)
wParams.setTextualPlans("", "note", "teiHeader,facsimile","pb,cb,lb")
wParams.getCorpusElement().setAttribute("font", "Junicode");
wParams.getEditionsElement(wParams.getCorpusElement()).setAttribute("default", "default,facsimile")
wParams.getCorpusElement().setAttribute("name", AsciiUtils.buildId(wDirectory.getName()).toUpperCase());
wParams.save();

BaseParameters.createEmptyParams(cImportXMLFile, AsciiUtils.buildId(cDirectory.getName()).toUpperCase())
BaseParameters cParams = new BaseParameters(cImportXMLFile)
cParams.load()
cParams.setSkipTokenization(true);
cParams.setWordElement("c")
cParams.setDoAnnotation(false)
cParams.setAnnotationLang("fr")
cParams.setWordsPerPage(9999999)
cParams.setTextualPlans("", "note", "teiHeader,facsimile","pb,cb,lb")
cParams.getCorpusElement().setAttribute("font", "Junicode");
cParams.getEditionsElement(cParams.getCorpusElement()).setAttribute("default", "default,facsimile")
cParams.getCorpusElement().setAttribute("name", AsciiUtils.buildId(cDirectory.getName()).toUpperCase());
cParams.save();