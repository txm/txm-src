<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:txm="http://textometrie.org/1.0"
	exclude-result-prefixes="tei edate xd txm" version="2.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" indent="no" />

	<xd:doc type="stylesheet">
		<xd:short>
			Cette feuille XSLT permet de pré-tokeniser par mot des fichiers au format
			XML-TEI Oriflamms brut
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2014, CNRS / ICAR (Équipe CACTUS)</xd:copyright>
	</xd:doc>

	<!-- <xsl:template match="/"> <xsl:apply-templates/> </xsl:template> -->

	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates
				select="*|@*|processing-instruction()|comment()|text()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template
		match="comment()|processing-instruction()|text()">
		<xsl:copy />
	</xsl:template>

	<!-- On supprime les attributs par défaut de la DTD TEI -->

	<xsl:template match="@*">
		<xsl:choose>
			<xsl:when
				test="matches(name(.),'^(part|instant|anchored|full)$')" />
			<xsl:otherwise>
				<xsl:copy />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- <xsl:template match="tei:teiHeader"> <xsl:copy-of select="."/> </xsl:template> -->

	<xsl:template match="tei:w|tei:pc">
		<xsl:copy-of select="." />
	</xsl:template>

	<xsl:template match="tei:text//text()">
		<xsl:choose>
			<!--<xsl:when test="not(ancestor::tei:text/tei:body/tei:div1/tei:p)"><xsl:copy/></xsl:when> -->
			<xsl:when test="matches(.,'^\s*$')">
				<xsl:copy />
			</xsl:when>
			<xsl:when
				test="matches(local-name(parent::*),'^(abbr|expan|sic|corr)$')">
				<xsl:copy />
			</xsl:when>
			<xsl:when
				test="ancestor::*/@ana[contains(.,'ori:align-no')]">
				<xsl:copy />
			</xsl:when>
			<xsl:when test="ancestor::tei:note">
				<xsl:copy />
			</xsl:when>
			<xsl:when test="ancestor::tei:witDetail">
				<xsl:copy />
			</xsl:when>
			<xsl:when
				test="parent::tei:hi and matches(.,'\w+') and not(matches(.,'\s')) and not(preceding-sibling::* or following-sibling::*)">
				<xsl:copy />
			</xsl:when>
			<xsl:when test="parent::tei:am|parent::tei:ex">
				<xsl:copy />
			</xsl:when>
			<!--<xsl:when test="parent::tei:rdg[not(matches(@wit,'^#?A'))]"><xsl:copy/></xsl:when> -->
			<xsl:when test="ancestor::tei:pc|ancestor::tei:w">
				<xsl:copy />
			</xsl:when>
			<xsl:otherwise>
				<xsl:analyze-string select="."
					regex="\w+|\p{{P}}+|\s+">
					<xsl:matching-substring>
						<xsl:choose>
							<xsl:when test="matches(.,'^\w+$')">
								<w xmlns="http://www.tei-c.org/ns/1.0">
									<xsl:value-of select="." />
								</w>
							</xsl:when>
							<!-- <xsl:when test="matches(.,'^\p{P}+$')"><orig xmlns="http://www.tei-c.org/ns/1.0"><pc 
								xmlns="http://www.tei-c.org/ns/1.0"><xsl:value-of select="."/></pc></orig></xsl:when> -->
							<xsl:when test="matches(.,'^\p{P}+$')">
								<pc xmlns="http://www.tei-c.org/ns/1.0">
									<xsl:value-of select="." />
								</pc>
							</xsl:when>
							<xsl:otherwise>
								<xsl:copy />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:matching-substring>
					<xsl:non-matching-substring>
						<xsl:comment>
							Type de caractère non reconnu
						</xsl:comment>
						<xsl:copy />
					</xsl:non-matching-substring>
				</xsl:analyze-string>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:choice">
		<xsl:choose>
			<xsl:when test="descendant::tei:pc">
				<pc xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:apply-templates
						select="descendant::tei:pc[1]/@*" />
					<xsl:copy>
						<xsl:apply-templates />
					</xsl:copy>
				</pc>
			</xsl:when>
			<xsl:when test="descendant::tei:w">
				<xsl:copy>
					<xsl:apply-templates select="@*" />
					<xsl:apply-templates />
				</xsl:copy>
			</xsl:when>
			<xsl:when test="ancestor::tei:pc|ancestor::tei:w">
				<xsl:copy-of select="." />
			</xsl:when>
			<xsl:otherwise>
				<w xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:copy-of select="." />
				</w>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>



	<!-- <xsl:template match="tei:pc[ancestor::tei:choice]"> <xsl:apply-templates/> 
		</xsl:template> -->

	<xsl:template
		match="tei:choice/tei:orig[child::tei:pc]|tei:choice/tei:reg[child::tei:pc]">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:value-of select="normalize-space(.)" />
		</xsl:copy>
	</xsl:template>


	<!-- <xsl:template match="tei:hi[matches(.,'\w+') and not(matches(.,'\s')) 
		and not(descendant::tei:w) and not(ancestor::*/@ana[contains(.,'ori:align-no')])]|tei:am|tei:ex"> -->

	<!--<xsl:template match="*[ancestor::tei:text and descendant::text()[matches(.,'\w')] 
		and not(descendant::text()[matches(.,'\S\s|\s\S|\p{P}')])]"> -->


	<xsl:template
		match="*[matches(.,'\w+') 
    and ancestor::tei:text 
    and not(matches(local-name(),'^(front|body|back|div.*|head|closer|p|ab|lg|l|.*[Nn]ame|num|cell|row|q|w|pc|title|term|emph|note|witDetail|foreign|ref|app|lem|rdg)$'))
    and not(matches(local-name(parent::*),'^(choice|subst|app)$'))
    and not(descendant::text()[matches(.,'\S\s|\s\S|\p{P}')]) 
    and not(descendant::tei:w or descendant::tei:pc) 
    and not(ancestor::*/@ana[contains(.,'ori:align-no')])]|tei:am|tei:ex">
		<w xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:copy-of select="." />
		</w>
	</xsl:template>

	<!-- <xsl:template match="*[matches(local-name(),'[Nn]ame$') and not(descendant::text()[matches(.,'\S\s|\s\S|\p{P}')])]"> -->


	<xsl:template
		match="*[matches(local-name(),'^.*[Nn]ame$') 
    and ancestor::tei:text 
    and not(descendant::*[matches(local-name(),'^(.*[Nn]ame|w|pc|num|term|ref)$')]) 
    and not(descendant::text()[matches(.,'\S\s|\s\S|\p{P}')])]">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<!-- <xsl:for-each select="child::text()"> <xsl:value-of select="concat('[',.,']')"/> 
				</xsl:for-each> -->
			<w xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:for-each select="child::node()">
					<xsl:copy-of select="." />
				</xsl:for-each>
			</w>
		</xsl:copy>

	</xsl:template>


	<!--<xsl:template match="tei:sic[not(parent::tei:choice or descendant::tei:w 
		or ancestor::*/@ana[contains(.,'ori:align-no')])]|tei:corr[not(parent::tei:choice 
		or descendant::tei:w or ancestor::*/@ana[contains(.,'ori:align-no')])]"> 
		<w xmlns="http://www.tei-c.org/ns/1.0"><xsl:copy-of select="."/></w> </xsl:template> -->

</xsl:stylesheet>