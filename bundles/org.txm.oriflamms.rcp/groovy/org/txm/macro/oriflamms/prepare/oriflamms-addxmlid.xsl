<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="tei edate" version="2.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />

	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates
				select="*|@*|processing-instruction()|comment()|text()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template
		match="@*|processing-instruction()|comment()|text()">
		<xsl:copy />
	</xsl:template>

	<xsl:param name="elements">
		w|lb|cb|pb|pc|milestone|seg
	</xsl:param>

	<xsl:variable name="elementsregex">
		<xsl:value-of select="concat('^(',$elements,')$')" />
	</xsl:variable>

	<xsl:variable name="filename">
		<xsl:analyze-string select="document-uri(.)"
			regex="^(.*)/([^/]+)$">
			<xsl:matching-substring>
				<xsl:value-of
					select="replace(regex-group(2),'(-ori)?(-wp?)?\.[^.]+$','')" />
			</xsl:matching-substring>
		</xsl:analyze-string>
	</xsl:variable>


	<xsl:template
		match="*[matches(local-name(),$elementsregex)]">
		<xsl:variable name="elementname">
			<xsl:value-of select="local-name()"></xsl:value-of>
		</xsl:variable>
		<xsl:variable name="number" as="xs:integer">
			<xsl:number level="any" />
		</xsl:variable>
		<xsl:copy>
			<xsl:if test="not(@xml:id)">
				<xsl:attribute name="xml:id">
        <xsl:choose>
          <xsl:when
					test="$elementname='milestone' and @unit='surface'">
            <xsl:value-of
					select="concat('surf_',$filename,'_',substring-before(@facs,'.'))" />
          </xsl:when>
          <xsl:when test="$elementname='pb'">
            <xsl:value-of
					select="concat('page_',$filename,'_',$number)" />
          </xsl:when>
          <xsl:when test="$elementname='cb'">
            <xsl:value-of
					select="concat('col_',$filename,'_',$number)" />
          </xsl:when>
          <xsl:when test="$elementname='lb'">
            <xsl:value-of
					select="concat('line_',$filename,'_',$number)" />
          </xsl:when>
          <xsl:when test="$elementname='seg' and @type='wp'">
            <xsl:value-of
					select="concat('wp_',$filename,'_',$number)" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of
					select="concat($elementname,'_',$filename,'_',$number)" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
			</xsl:if>
			<xsl:if
				test="not(@n) and not(matches($elementname,'^(w|pc|seg)$'))">
				<xsl:attribute name="n"><xsl:value-of
					select="$number" /></xsl:attribute>
			</xsl:if>
			<xsl:if
				test="$elementname='lb' and @type='rejet' and not(@corresp)">
				<xsl:attribute name="corresp">
<!--        <xsl:choose>
          <xsl:when test="@n">
            <xsl:value-of select="concat('#line_',$filename,'_',@n)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat('#line_',$filename,'_',$number + 1)"/>
          </xsl:otherwise>
        </xsl:choose>-->
        <xsl:value-of
					select="concat('#line_',$filename,'_',$number + 1)" />
      </xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>



</xsl:stylesheet>
