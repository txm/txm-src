<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:txm="http://textometrie.org/1.0"
	xmlns:xi="http://www.w3.org/2001/XInclude"
	xmlns:svg="http://www.w3.org/2000/svg"
	xmlns:math="http://www.w3.org/1998/Math/MathML"
	exclude-result-prefixes="tei edate xd txm xi svg math" version="2.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" indent="no" />

	<xd:doc type="stylesheet">
		<xd:short>
			Cette feuille permet de convertir les transcription des
			Chartes de Fontenay au format XML-TEI du projet Oriflamms.
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2015, CNRS / ICAR (Équipe CACTUS)</xd:copyright>
	</xd:doc>

	<!--<xsl:strip-space elements="*"/> -->

	<xsl:param name="baseMsSiglum">
		A
	</xsl:param>

	<xsl:variable name="baseMsPattern">
		<xsl:value-of
			select="concat('^#?',$baseMsSiglum,'| #?',$baseMsSiglum)" />
	</xsl:variable>

	<xsl:template match="*|comment()">
		<!-- Copy the current node -->
		<xsl:copy>
			<!-- Including any attributes it has and any child nodes -->
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="processing-instruction()" />

	<!-- On supprime les attributs par défaut de la DTD TEI -->

	<xsl:template match="@*">
		<xsl:choose>
			<xsl:when
				test="matches(name(.),'^(part|instant|anchored|full)$')" />
			<xsl:otherwise>
				<xsl:copy />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="@facs">
		<xsl:attribute name="facs"><xsl:value-of
			select="replace(.,'\.tif$','.png','i')" /></xsl:attribute>
	</xsl:template>

	<xsl:template match="tei:teiHeader">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>


	<xsl:template match="tei:revisionDesc">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<change xmlns="http://www.tei-c.org/ns/1.0" when="2007-01-01"
				who="#DS">Publication du document original</change>
			<!-- information récupérée à partir de /TEI/text[1]/front[1]/titlePage[1]/docDate[1] -->
			<change xmlns="http://www.tei-c.org/ns/1.0"
				when="{format-date(current-date(),'[Y]-[M01]-[D01]')}" who="#auto">Conversion
				automatique au format XML-TEI-Oriflamms</change>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:encodingDesc">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
			<xsl:if test="not(//tei:prefixDef[@ident='ori'])">
				<listPrefixDef xmlns="http://www.tei-c.org/ns/1.0">
					<prefixDef xmlns="http://www.tei-c.org/ns/1.0"
						ident="ori" matchPattern="([a-z]+)"
						replacementPattern="oriflamms-annotation-scheme.xml#$1">
						<p>
							In the context of this project, private URIs with the prefix
							"ori" point to
							<gi>interp</gi>
							elements in the project's
							oriflamms-annotation-scheme.xml file.
						</p>
					</prefixDef>
				</listPrefixDef>
			</xsl:if>
		</xsl:copy>
	</xsl:template>

	<!-- On restructure le document pour avoir un TEI par charte -->

	<!-- TEI devient teiCorpus -->

	<xsl:template match="tei:TEI">
		<xsl:element name="teiCorpus"
			xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</xsl:element>
	</xsl:template>

	<!-- on supprime l'élément text de niveau 1 et travaille directement sur 
		le niveau 2 (l'élément group intermédiaire disparait) -->

	<!-- <xsl:template match="tei:TEI/tei:text"> <xsl:apply-templates select="descendant::tei:text"/> 
		</xsl:template> -->


	<xsl:template match="tei:text[descendant::tei:text]">

		<!-- <teiCorpus xmlns="http://www.tei-c.org/ns/1.0"> <teiHeader xmlns="http://www.tei-c.org/ns/1.0"> 
			<fileDesc xmlns="http://www.tei-c.org/ns/1.0"> <titleStmt xmlns="http://www.tei-c.org/ns/1.0"> 
			<title xmlns="http://www.tei-c.org/ns/1.0"> <xsl:value-of select="tei:group[1]/tei:head[1]"/> 
			</title> </titleStmt> <publicationStmt xmlns="http://www.tei-c.org/ns/1.0"> 
			<p xmlns="http://www.tei-c.org/ns/1.0">See the header of the top level teiCorpus</p> 
			</publicationStmt> <sourceDesc xmlns="http://www.tei-c.org/ns/1.0"> <p xmlns="http://www.tei-c.org/ns/1.0">See 
			the header of the top level teiCorpus</p> </sourceDesc> </fileDesc> <profileDesc 
			xmlns="http://www.tei-c.org/ns/1.0"> <!-\- argument devient profileDesc/abstract 
			-\-> <xsl:apply-templates select="descendant::tei:argument[1]"/> </profileDesc> 
			</teiHeader> <xsl:apply-templates/> </teiCorpus> -->
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template
		match="tei:text[descendant::tei:text]/tei:front">
		<xsl:choose>
			<xsl:when test="descendant::tei:div">
				<TEI xmlns="http://www.tei-c.org/ns/1.0" ana="ori:align-no">
					<teiHeader xmlns="http://www.tei-c.org/ns/1.0">
						<fileDesc xmlns="http://www.tei-c.org/ns/1.0">
							<titleStmt xmlns="http://www.tei-c.org/ns/1.0">
								<title xmlns="http://www.tei-c.org/ns/1.0">
									<xsl:text>Introduction chercheur</xsl:text>
								</title>
							</titleStmt>
							<publicationStmt
								xmlns="http://www.tei-c.org/ns/1.0">
								<p xmlns="http://www.tei-c.org/ns/1.0">See the header of the top level teiCorpus</p>
							</publicationStmt>
							<sourceDesc xmlns="http://www.tei-c.org/ns/1.0">
								<p xmlns="http://www.tei-c.org/ns/1.0">See the header of the top level teiCorpus</p>
							</sourceDesc>
						</fileDesc>
					</teiHeader>
					<text xmlns="http://www.tei-c.org/ns/1.0">
						<body xmlns="http://www.tei-c.org/ns/1.0">
							<xsl:apply-templates
								select="descendant::tei:div" />
						</body>

					</text>
				</TEI>
			</xsl:when>
			<xsl:otherwise>
				<xsl:comment>
					<xsl:copy-of select="." />
				</xsl:comment>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template
		match="tei:text[descendant::tei:text]/tei:back">
		<xsl:choose>
			<xsl:when test="descendant::tei:div">
				<TEI xmlns="http://www.tei-c.org/ns/1.0" ana="ori:align-no">
					<teiHeader xmlns="http://www.tei-c.org/ns/1.0">
						<fileDesc xmlns="http://www.tei-c.org/ns/1.0">
							<titleStmt xmlns="http://www.tei-c.org/ns/1.0">
								<title xmlns="http://www.tei-c.org/ns/1.0">
									<xsl:text>Annexes</xsl:text>
								</title>
							</titleStmt>
							<publicationStmt
								xmlns="http://www.tei-c.org/ns/1.0">
								<p xmlns="http://www.tei-c.org/ns/1.0">See the header of the top level teiCorpus</p>
							</publicationStmt>
							<sourceDesc xmlns="http://www.tei-c.org/ns/1.0">
								<p xmlns="http://www.tei-c.org/ns/1.0">See the header of the top level teiCorpus</p>
							</sourceDesc>
						</fileDesc>
					</teiHeader>
					<text xmlns="http://www.tei-c.org/ns/1.0">
						<body xmlns="http://www.tei-c.org/ns/1.0">
							<xsl:apply-templates
								select="descendant::tei:div" />
						</body>

					</text>
				</TEI>
			</xsl:when>
			<xsl:otherwise>
				<xsl:comment>
					<xsl:copy-of select="." />
				</xsl:comment>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template
		match="tei:group[@n='edition']|tei:group[@n='recueil']">
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template
		match="tei:group[@n='edition']/ tei:group[@n='introduction']">
		<TEI xmlns="http://www.tei-c.org/ns/1.0">
			<teiHeader xmlns="http://www.tei-c.org/ns/1.0">
				<fileDesc xmlns="http://www.tei-c.org/ns/1.0">
					<titleStmt xmlns="http://www.tei-c.org/ns/1.0">
						<title xmlns="http://www.tei-c.org/ns/1.0">
							<xsl:text>Introduction</xsl:text>
						</title>
					</titleStmt>
					<publicationStmt
						xmlns="http://www.tei-c.org/ns/1.0">
						<p xmlns="http://www.tei-c.org/ns/1.0">See the header of the top level teiCorpus</p>
					</publicationStmt>
					<sourceDesc xmlns="http://www.tei-c.org/ns/1.0">
						<p xmlns="http://www.tei-c.org/ns/1.0">See the header of the top level teiCorpus</p>
					</sourceDesc>
				</fileDesc>
			</teiHeader>
			<xsl:apply-templates />
		</TEI>

	</xsl:template>

	<xsl:template
		match="tei:group[@n='edition']/ tei:group[@n='table']">
		<TEI xmlns="http://www.tei-c.org/ns/1.0" ana="ori:align-no">
			<teiHeader xmlns="http://www.tei-c.org/ns/1.0">
				<fileDesc xmlns="http://www.tei-c.org/ns/1.0">
					<titleStmt xmlns="http://www.tei-c.org/ns/1.0">
						<title xmlns="http://www.tei-c.org/ns/1.0">
							<xsl:text>Table</xsl:text>
						</title>
					</titleStmt>
					<publicationStmt
						xmlns="http://www.tei-c.org/ns/1.0">
						<p xmlns="http://www.tei-c.org/ns/1.0">See the header of the top level teiCorpus</p>
					</publicationStmt>
					<sourceDesc xmlns="http://www.tei-c.org/ns/1.0">
						<p xmlns="http://www.tei-c.org/ns/1.0">See the header of the top level teiCorpus</p>
					</sourceDesc>
				</fileDesc>
			</teiHeader>
			<text xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:copy-of select="." />
			</text>
		</TEI>

	</xsl:template>

	<xsl:template
		match="tei:group[@n='edition']/tei:group[@n='recueil']/tei:text">

		<TEI xmlns="http://www.tei-c.org/ns/1.0">
			<teiHeader xmlns="http://www.tei-c.org/ns/1.0">
				<fileDesc xmlns="http://www.tei-c.org/ns/1.0">
					<titleStmt xmlns="http://www.tei-c.org/ns/1.0">
						<title xmlns="http://www.tei-c.org/ns/1.0">
							<xsl:value-of select="descendant::tei:docTitle[1]"></xsl:value-of>
						</title>
						<xsl:if test="descendant::tei:docAuthor">
							<author xmlns="http://www.tei-c.org/ns/1.0">
								<xsl:value-of select="descendant::tei:docAuthor[1]"></xsl:value-of>
							</author>
						</xsl:if>

					</titleStmt>
					<publicationStmt
						xmlns="http://www.tei-c.org/ns/1.0">
						<p xmlns="http://www.tei-c.org/ns/1.0">See the header of the top level teiCorpus</p>
					</publicationStmt>
					<!-- les éléments listWit et listBibl sont placés dans sourceDesc -->
					<sourceDesc xmlns="http://www.tei-c.org/ns/1.0">
						<p xmlns="http://www.tei-c.org/ns/1.0">See the header of the top level teiCorpus</p>
					</sourceDesc>
				</fileDesc>
				<profileDesc xmlns="http://www.tei-c.org/ns/1.0">
					<!-- docDate devient profileDesc/creation/date[@type='documentCreation'] -->
					<creation xmlns="http://www.tei-c.org/ns/1.0">
						<xsl:apply-templates
							select="descendant::tei:docDate/tei:date" />
					</creation>
				</profileDesc>
			</teiHeader>
			<text xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:if test="preceding-sibling::*[1][self::tei:milestone]">
					<xsl:copy-of
						select="preceding-sibling::tei:milestone[1]"></xsl:copy-of>
				</xsl:if>
				<xsl:apply-templates />
			</text>
		</TEI>
	</xsl:template>

	<xsl:template
		match="tei:text[not(descendant::tei:text)]/tei:front">
	</xsl:template>

	<xsl:template match="tei:argument" />




	<xsl:template match="tei:milestone">
		<xsl:choose>
			<xsl:when test="parent::tei:group" />
			<xsl:otherwise>
				<xsl:copy-of select="." />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:group/tei:head" />

	<!-- on supprime listWit du corps du texte (cf. l'entête) -->
	<xsl:template match="tei:listWit" />

	<!-- on supprime les entêtes et pied de pages (non alignables) -->

	<xsl:template match="tei:fw">
		<xsl:comment>
			fw :
			<xsl:copy-of select="." />
		</xsl:comment>
	</xsl:template>

	<!-- on ajoute un attribut pour faciliter l'identification des segments 
		à ne pas aligner -->

	<xsl:template
		match="tei:choice/tei:expan|tei:choice/tei:corr|tei:supplied|tei:note">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="ana">ori:align-no</xsl:attribute>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:pb">
		<xsl:variable name="facs">
			<xsl:value-of select="@facs" />
		</xsl:variable>
		<xsl:if
			test="//tei:zone[@xml:id=substring-after($facs,'#')] and not(preceding::tei:pb[@facs=$facs])">
			<milestone xmlns="http://www.tei-c.org/ns/1.0"
				unit="surface">
				<xsl:attribute name="facs">
          <xsl:value-of
					select="//tei:surface[tei:zone[@xml:id=substring-after($facs,'#')]][1]/tei:graphic/@url" />
        </xsl:attribute>
			</milestone>
		</xsl:if>
		<xsl:copy-of select="." />
	</xsl:template>


	<xsl:template
		match="text()[following-sibling::*[1][self::tei:milestone or self::tei:pb or self::tei:cb or self::tei:lb] and following-sibling::tei:lb[1][@break='no']]">
		<xsl:choose>
			<!-- patch d'espace blanc devant les sauts de ligne à l'intérieur de mots -->
			<xsl:when test="matches(.,'\s+$')">
				<xsl:value-of select="replace(.,'\s+$','')" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:witDetail">
		<xsl:text> </xsl:text>
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="ana">ori:align-no</xsl:attribute>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>