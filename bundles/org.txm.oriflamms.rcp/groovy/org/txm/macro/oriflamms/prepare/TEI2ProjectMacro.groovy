// STANDARD DECLARATIONS
package org.txm.macro.oriflamms.prepare

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.*
import org.txm.utils.*
import org.txm.utils.io.*
import org.txm.scripts.importer.*
import org.txm.utils.logger.*
import org.txm.utils.zip.Zip;

@Field @Option(name="xmlFile", usage="Raw XML-TEI Oriflamm", widget="File", required=true, def="file.xml")
File xmlFile
@Field @Option(name="xslFile", usage="Optional XSL file applied", widget="File", required=false, def="file.xsl")
File xslFile
@Field @Option(name="imagesDirectory", usage="Optional images directory", widget="Folder", required=false, def="images")
File imagesDirectory
@Field @Option(name="createArchive", usage="Optional zip the project", widget="Boolean", required=false, def="false")
boolean createArchive
if (!ParametersDialog.open(this)) return;

if (!xmlFile.exists()) {
	println "Could not read input XML input file: $xmlFile"
	return;
}

File oriflammsMacroDirectory = new File(Toolbox.getTxmHomePath(), "scripts/macro/org/txm/macro/oriflamms/prepare")
if (!oriflammsMacroDirectory.exists()) {
	println "Oriflamms macro directory not found: "+oriflammsMacroDirectory
	return;
}
File xslTokenizer = new File(oriflammsMacroDirectory, "oriflamms-tokenize-words.xsl");
File xslPatchLbInWords = new File(oriflammsMacroDirectory,"oriflamms-patch-words-with-lb.xsl");
File xslMissingMilestones = new File(oriflammsMacroDirectory, "oriflamms-patch-milestones.xsl");
File xslCharactersTokenizer = new File(oriflammsMacroDirectory, "oriflamms-tokenize-chars-1-tag.xsl");
File xslCharactersIdentifier = new File(oriflammsMacroDirectory, "oriflamms-tokenize-chars-2-identify.xsl");
File xslZones = new File(oriflammsMacroDirectory, "oriflamms-convert-transcriptions-orizones.xsl");

if (!xslTokenizer.exists()  || !xslPatchLbInWords.exists() || !xslMissingMilestones.exists() ||	
!xslCharactersTokenizer.exists() || !xslCharactersIdentifier.exists() || !xslZones.exists()) {
	println "Could not find one of TXM's XSL file : "+
		[xslTokenizer, xslPatchLbInWords, xslMissingMilestones, 
		xslCharactersTokenizer, xslCharactersIdentifier, xslZones];
		
	println ""+[xslTokenizer.exists(), xslPatchLbInWords.exists(), xslMissingMilestones.exists()
	, xslCharactersTokenizer.exists(), xslCharactersIdentifier.exists(), xslZones.exists()]
	return;
}

File xmlFileParentDirectory = xmlFile.getParentFile()
String projectName = xmlFile.getName()
if (projectName.lastIndexOf(".") > 0) projectName = projectName.substring(0, projectName.lastIndexOf("."))
File projectDirectory = new File(xmlFileParentDirectory, projectName)
projectDirectory.deleteDir()
if (projectDirectory.exists()) {
	println "Could not delete previous project directory: $projectDirectory"
	return;
}

projectDirectory.mkdir()

if (!projectDirectory.exists()) {
	println "Could not create project directory: $projectDirectory"
	return;
}

println "Oriflamms project directory: $projectDirectory"

File xmlFileCopy = new File(projectDirectory, xmlFile.getName())
println "Copying XML files: $xmlFile to $projectDirectory"
CopyXMLFiles cdf = new CopyXMLFiles(xmlFile)
projectDirectory.mkdir()
println "Files copied: "+cdf.copy(projectDirectory)
if (!xmlFileCopy.exists()) {
	println "Could not copy input XML input file: $xmlFile to $xmlFileCopy"
	return;
}

if (xslFile != null) {
	if (xslFile.exists()) {
		println "Applying $xslFile to $xmlFileCopy..."
		ApplyXsl2 builder = new ApplyXsl2(xslFile);
		if (!builder.process(xmlFileCopy, xmlFileCopy)) {
			System.out.println("Failed to process "+xmlFileCopy+" with "+xslFile);
			return;
		}
	}
}

File textsDirectory = new File(projectDirectory, "texts")
File imgDirectory = new File(projectDirectory, "img")
File img_linksDirectory = new File(projectDirectory, "img_links")
File ontologiesDirectory = new File(projectDirectory, "ontologies")
File ontologies_linksDirectory = new File(projectDirectory, "ontologies_links")
File zonesDirectory = new File(projectDirectory, "zones")
textsDirectory.mkdir()
imgDirectory.mkdir()
img_linksDirectory.mkdir()
ontologiesDirectory.mkdir()
ontologies_linksDirectory.mkdir()
zonesDirectory.mkdir()

File xmlWFile = new File(textsDirectory, projectName+"-w.xml")
File xmlWCFile = new File(textsDirectory, projectName+"-c.xml")

try {

println "Applying $xslMissingMilestones to $xmlWFile..."
if (monitor != null) monitor.worked(1, "Applying $xslMissingMilestones to $xmlWFile...")
builder = new ApplyXsl2(xslMissingMilestones);
if (!builder.process(xmlFileCopy, xmlWFile)) {
	System.out.println("Failed to process "+xmlWFile+" with "+xslMissingMilestones);
	return;
}

println "Applying $xslTokenizer to $xmlWFile..."
if (monitor != null) monitor.worked(15, "Applying $xslTokenizer to $xmlWFile...")
builder = new ApplyXsl2(xslTokenizer);
if (!builder.process(xmlWFile, xmlWFile)) {
	System.out.println("Failed to process "+xmlFileCopy+" with "+xslTokenizer);
	return;
}

println "Merging words <w/><w>"
if (monitor != null) monitor.worked(15, "Merging words <w/><w>")
String content = xmlWFile.getText("UTF-8")
content = content.replaceAll("</w><w[^>]*>", "")
content = content.replaceAll("</w>\\s*(<milestone[^>]*>)?\\s*(<pb[^>]*>)?\\s*(<cb[^>]*>)?\\s*(<lb[^>]*break=\"no\"[^>]*>)\\s*<w[^>]*>", '$1$2$3$4')
try {
	def writer = xmlWFile.newWriter("UTF-8")
	writer.print(content)
	writer.close()
} catch (Exception e2) {
	println "Error while fixing words: $e2"
	return;
}

println "Applying $xslPatchLbInWords to $xmlWFile..."
if (monitor != null) monitor.worked(15, "Applying $xslPatchLbInWords to $xmlWFile...")
builder = new ApplyXsl2(xslPatchLbInWords);
if (!builder.process(xmlWFile, xmlWFile)) {
	System.out.println("Failed to process "+xmlFileCopy+" with "+xslPatchLbInWords);
	return;
}

println "Fixing 'id' and 'n' attributes in $xmlWFile..."
if (monitor != null) monitor.worked(15, "Fixing 'id' and 'n' attributes in $xmlWFile...")
WriteIdAndNAttributes wiana = new WriteIdAndNAttributes(xmlWFile, projectName)
File tmp = new File(xmlWFile.getParentFile(), "tmp_"+xmlWFile.getName())
if (!wiana.process(tmp)) {
	System.out.println("Failed to fix id and n attributes with of $xmlWFile file")
	return;
} else {
	xmlWFile.delete()
	tmp.renameTo(xmlWFile)
	if (tmp.exists()) {
		System.out.println("Fail to replace $xmlWFile with result file $tmp")
		return;
	}
}

println "Applying $xslCharactersTokenizer to $xmlWFile..."
if (monitor != null) monitor.worked(15, "Applying $xslCharactersTokenizer to $xmlWFile...")
builder = new ApplyXsl2(xslCharactersTokenizer);
if (!builder.process(xmlWFile, xmlWCFile)) {
	System.out.println("Failed to process "+xmlWFile+" with "+xslCharactersTokenizer);
	return;
} 

println "Applying $xslCharactersIdentifier to $xmlWCFile..."
if (monitor != null) monitor.worked(1, "Applying $xslCharactersIdentifier to $xmlWCFile...")
builder = new ApplyXsl2(xslCharactersIdentifier);
if (!builder.process(xmlWCFile, xmlWCFile)) {
	System.out.println("Failed to process "+xmlWCFile+" with "+xslCharactersIdentifier);
	return;
}

println "Applying $xslZones to $xmlWFile..."
if (monitor != null) monitor.worked(15, "Applying $xslZones to $xmlWFile...")
builder = new ApplyXsl2(xslZones);
if (!builder.process(xmlWFile, null)) {
	System.out.println("Failed to process "+xmlFileCopy+" with "+xslZones);
	return;
}

if (imagesDirectory.exists() && imagesDirectory.listFiles(IOUtils.HIDDENFILE_FILTER).size() > 0) {
	println "Copying images files from $imagesDirectory to $imgDirectory..."
	FileCopy.copyFiles(imagesDirectory, imgDirectory);
	def files = imgDirectory.listFiles(IOUtils.HIDDENFILE_FILTER)
	if (files != null) println ""+files.size()+" images copied."
}

if (createArchive) {
	if (monitor != null) monitor.worked(15, "Building Oriflamms binary project... ")
	File zipFile = new File(xmlFileParentDirectory, projectName+".oriflamms")
	zipFile.delete()
	Zip.compress(projectDirectory, zipFile)
	
	if (zipFile.exists()) {
		println "Project oriflamms exported to $zipFile"
		projectDirectory.deleteDir()
	} else {
		println "Fail to export project $projectDirectory"
	}
}

} catch (Exception e) {
	println "Error while applying a XSL file: "+e
	Log.printStackTrace(e)
}
if (monitor != null) monitor.done()