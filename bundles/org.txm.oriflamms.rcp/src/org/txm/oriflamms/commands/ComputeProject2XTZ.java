package org.txm.oriflamms.commands;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.kohsuke.args4j.Option;
import org.txm.oriflamms.functions.Project2XTZ;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.xml.sax.SAXException;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ComputeProject2XTZ extends AbstractHandler {

	@Option(name = "projectDirectory", usage = "Oriflamms' project directory", widget = "Folder", required = true, def = "project")
	File projectDirectory;

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!ParametersDialog.open(this)) return null;

		try {
			return process(projectDirectory);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean process(File projectDirectory) throws IOException, TransformerException, ParserConfigurationException, SAXException, XMLStreamException {
		Project2XTZ p2X = new Project2XTZ(projectDirectory);
		return p2X.process();
	}
}
