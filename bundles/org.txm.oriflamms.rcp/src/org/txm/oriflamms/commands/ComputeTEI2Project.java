package org.txm.oriflamms.commands;

import java.io.File;
import java.io.IOException;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.kohsuke.args4j.Option;
import org.txm.oriflamms.functions.TEI2Project;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ComputeTEI2Project extends AbstractHandler {

	@Option(name = "xmlFile", usage = "Raw XML-TEI Oriflamm", widget = "File", required = true, def = "file.xml")
	File xmlFile;

	@Option(name = "xslFile", usage = "Optional XSL file applied", widget = "File", required = false, def = "file.xsl")
	File xslFile;

	@Option(name = "imagesDirectory", usage = "Optional images directory", widget = "Folder", required = false, def = "images")
	File imagesDirectory;

	@Option(name = "createArchive", usage = "Optional zip the project", widget = "Boolean", required = false, def = "false")
	boolean createArchive;

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!ParametersDialog.open(this)) return null;

		try {
			if (!process(xmlFile, xslFile, imagesDirectory, createArchive)) {
				MessageDialog.openError(Display.getCurrent().getActiveShell(), "Error", "The conversion failed. See messages in the console.");
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static boolean process(final File xmlFile, final File xslFile, final File imagesDirectory, final boolean createArchive) throws IOException, XMLStreamException, TransformerException {
		TEI2Project t2p = new TEI2Project(xmlFile, xslFile, imagesDirectory, createArchive);
		return t2p.process(null);
	}
}
