package org.txm.oriflamms.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.kohsuke.args4j.Option;
import org.txm.oriflamms.functions.AbbreviationsAndSementics;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ComputeAbbreviationsAndSemantics extends AbstractHandler {

	@Option(name = "tsvFile", usage = "TSV output file", widget = "CreateFile", required = false, def = "file.tsv")
	File tsvFile;

	@Option(name = "entities", usage = "1	2	3", metaVar = "name	persName	placeName	orgName	roleName", widget = "StructuralUnits", required = false, def = "persName")
	String entities;

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection isel = HandlerUtil.getCurrentSelection(event);
		if (isel instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) isel;
			Object o = selection.getFirstElement();

			if (!(o instanceof CQPCorpus)) {
				System.out.println(o.toString() + " n'est pas un corpus. Abandon");
				return null;
			}
			else {
				if (!ParametersDialog.open(this)) return null;

				CQPCorpus corpus = (CQPCorpus) o;

				try {
					corpus.compute(false);
					return compute(corpus, entities, tsvFile);
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	public boolean compute(CQPCorpus corpus, String entities, File tsvFile) throws Exception {
		AbbreviationsAndSementics aas = new AbbreviationsAndSementics(corpus, tsvFile, entities);
		return aas.process();
	}
}
