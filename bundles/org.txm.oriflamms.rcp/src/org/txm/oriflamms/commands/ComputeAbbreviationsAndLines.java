package org.txm.oriflamms.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.kohsuke.args4j.Option;
import org.txm.oriflamms.functions.AbbreviationsAndLines;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ComputeAbbreviationsAndLines extends AbstractHandler {

	@Option(name = "tsvFile", usage = "TSV output file", widget = "CreateFile", required = false, def = "file.tsv")
	File tsvFile;

	@Option(name = "dist_start", usage = "Distance depuis le début de ligne", widget = "Integer", required = false, def = "0")
	Integer dist_start;

	@Option(name = "dist_end", usage = "Distance depuis la fin de ligne", widget = "Integer", required = false, def = "1")
	Integer dist_end;

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection isel = HandlerUtil.getCurrentSelection(event);
		if (isel instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) isel;
			Object o = selection.getFirstElement();

			if (!(o instanceof CQPCorpus)) {
				System.out.println(o.toString() + " n'est pas un corpus. Abandon");
				return null;
			}
			else {
				if (!ParametersDialog.open(this)) return null;

				CQPCorpus corpus = (CQPCorpus) o;
				dist_start = Math.abs(dist_start);
				dist_end = Math.abs(dist_end);

				try {
					corpus.compute(false);
					return compute(corpus, dist_start, dist_end, tsvFile);
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	public boolean compute(CQPCorpus corpus, int dist_start, int dist_end, File tsvFile) throws Exception {
		AbbreviationsAndLines aal = new AbbreviationsAndLines(corpus, tsvFile, dist_start, dist_end);
		return aal.process();
	}
}
