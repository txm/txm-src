package org.txm.oriflamms.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.kohsuke.args4j.Option;
import org.txm.oriflamms.functions.UpdateCorpusImagePaths;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.searchengine.cqp.corpus.MainCorpus;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ComputeCorpusImagePath extends AbstractHandler {

	@Option(name = "useCorpusHTMLDefaultDirectory", usage = "use Corpus HTML Default Directory as image directory", widget = "Boolean", required = true, def = "true")
	boolean useCorpusHTMLDefaultDirectory;

	@Option(name = "path", usage = "copy dictionary name", widget = "Folder", required = false, def = "")
	File path;

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection isel = HandlerUtil.getCurrentSelection(event);
		if (isel instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) isel;
			Object o = selection.getFirstElement();

			if (!(o instanceof MainCorpus)) {
				System.out.println(o.toString() + " is not a MainCorpus. Aborting");
				return null;
			}
			else {
				MainCorpus corpus = (MainCorpus) o;

				if (ParametersDialog.open(this)) {
					if (useCorpusHTMLDefaultDirectory) path = null;
					update(corpus, path);
				}
			}
		}

		return null;
	}

	public boolean update(MainCorpus corpus, File path) {
		try {
			corpus.compute(false);
			UpdateCorpusImagePaths upip = new UpdateCorpusImagePaths();
			String spath = null;
			if (path != null) spath = path.getAbsolutePath();
			if (upip.process(corpus, spath)) {
				System.out.println("Corpus HTML paths patched !");
			}
			else {
				System.out.println("Error: Corpus HTML paths not patched.");
				return false;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
