// STANDARD DECLARATIONS
package org.txm.oriflamms.functions;

import java.io.File;
import java.io.IOException;

import javax.xml.stream.XMLStreamException;

import org.txm.importer.StaxIdentityParser;

public class UpdateHTMLFileImagePaths extends StaxIdentityParser {

	String newpathprefix;

	public UpdateHTMLFileImagePaths(File htmlFile, String newpathprefix) throws IOException, XMLStreamException {
		super(htmlFile);
		this.newpathprefix = newpathprefix;

		if (newpathprefix.endsWith("/")) newpathprefix = newpathprefix.substring(0, newpathprefix.length() - 1);
	}

	String src = null;

	protected void processStartElement() throws XMLStreamException, IOException {
		src = null;
		if (localname.equals("img")) {
			//System.out.println("start element img");
			int n = parser.getAttributeCount();
			for (int i = 0; i < n; i++) {
				if (parser.getAttributeLocalName(i).equals("src")) {
					src = parser.getAttributeValue(i);
					//System.out.println("start element img@src="+src);
					break;
				}
			}

			if (src != null) {
				int idx = src.lastIndexOf("/");
				String name = src.substring(idx);
				src = newpathprefix + name;
			}
		}

		super.processStartElement();
	}

	protected void writeAttributes() throws XMLStreamException {
		for (int i = 0; i < parser.getAttributeCount(); i++) {
			if (src != null && "src".equals(parser.getAttributeLocalName(i))) {
				writeAttribute(parser.getAttributePrefix(i), parser.getAttributeLocalName(i), src);
				src = null;
			}
			else {
				writeAttribute(parser.getAttributePrefix(i), parser.getAttributeLocalName(i), parser.getAttributeValue(i));
			}
		}
	}

	public static void main(String[] args) {
		File htmlFile = new File("/home/mdecorde/TXM/corpora/graal/HTML/GRAAL/ms-colonne/qgraal_cm_160a.html");
		File outfile = new File("/home/mdecorde/TXM/corpora/graal/HTML/GRAAL/ms-colonne/qgraal_cm_160a-o.html");
		String prefix = "AAAA";
		UpdateHTMLFileImagePaths p;
		try {
			p = new UpdateHTMLFileImagePaths(htmlFile, prefix);
			System.out.println(p.process(outfile));
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
