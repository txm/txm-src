// STANDARD DECLARATIONS
package org.txm.oriflamms.functions;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Pattern;

import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.serverException.CqiServerError;

public class Allographs extends OriflammsFunction {

	protected String sign, allograph;

	CQLQuery query;

	private Property sign_property;

	private Property allograph_auto_property;

	private Property allograph_expert_property;

	Pattern signRegExp, characterRegExp;

	public Allographs(CQPCorpus corpus, File tsvFile, String sign, CQLQuery query, String allograph) throws CqiClientException, IOException, CqiServerError {
		super(corpus, tsvFile);

		this.query = query;
		this.allograph = allograph;
		this.sign = sign;
	}

	public boolean process() throws Exception {
		String[] props = { "sign", "allograph-expert", "allograph-auto", "characters" };
		System.out.println("Dénombrement des allographes '" + allograph + "' de signe '" + sign + "' dans le contexte '" + query + "'");

		for (String prop : props) {
			if (corpus.getProperty(prop) == null) {
				System.out.println("Le corpus '" + corpus + "' n'a pas de propriété de mot '" + prop + "'. Abandon.");
				return false;
			}
		}

		sign_property = corpus.getProperty("sign");
		allograph_expert_property = corpus.getProperty("allograph-expert");
		allograph_auto_property = corpus.getProperty("allograph-auto");
		signRegExp = Pattern.compile(sign);
		characterRegExp = Pattern.compile(allograph);

		return super.process();
	}

	Object[] getInfos(int from, int length) throws UnexpectedAnswerException, IOException, CqiServerError {

		int[] positions = new int[length];
		for (int i = 0; i < length; i++)
			positions[i] = from++;

		String[] signs = CQI.cpos2Str(sign_property.getQualifiedName(), positions);
		String[] allographs_expert = CQI.cpos2Str(allograph_expert_property.getQualifiedName(), positions);
		String[] allographs_auto = CQI.cpos2Str(allograph_auto_property.getQualifiedName(), positions);
		String[] characters = CQI.cpos2Str(charactersP.getQualifiedName(), positions);

		Object[] rez = { signs, allographs_expert, allographs_auto, characters };
		return rez;
	}

	boolean processLine(String text_id, String pb_id, String cb_id, String lb_id, int length, Object[] infos) {

		String[] signs = (String[]) infos[0];
		String[] allographs_expert = (String[]) infos[1];
		String[] allographs_auto = (String[]) infos[2];
		String[] characters = (String[]) infos[3];
		HashMap<String, HashMap<String, Integer>> count_signs = new HashMap<String, HashMap<String, Integer>>();

		for (int i = 0; i < length; i++) {
			String s = signs[i];
			String c = characters[i];

			if (signRegExp.matcher(s).find() && characterRegExp.matcher(c).find()) {
				if (!count_signs.containsKey(s)) count_signs.put(s, new HashMap<String, Integer>());
				HashMap<String, Integer> counts = count_signs.get(s);

				if (!counts.containsKey(c)) counts.put(c, 0);
				counts.put(c, counts.get(c) + 1);
			}
		}

		for (String s : count_signs.keySet()) {
			HashMap<String, Integer> counts = count_signs.get(s);
			int sum = 0;
			for (Integer i : counts.values())
				sum += i;

			for (String c : counts.keySet()) {
				writer.println(text_id + "\t" + pb_id + "\t" + cb_id + "\t" + lb_id + "\t" + s + "\t" + c + "\t" + counts.get(c) + "\t" + ((float) counts.get(c) / (float) sum));
			}
		}
		return true;
	}
}
