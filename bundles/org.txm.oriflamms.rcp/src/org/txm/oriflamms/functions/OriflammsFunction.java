// STANDARD DECLARATIONS
package org.txm.oriflamms.functions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;

import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Part;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.StructuredPartition;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.ConsoleProgressBar;

public abstract class OriflammsFunction {

	protected CQPCorpus corpus;

	protected File tsvFile;

	protected AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();

	protected StructuralUnit text_su;

	protected StructuralUnitProperty text_id;

	protected Property pb_id;

	protected Property cb_id;

	protected Property lb_id;

	protected Property lettersAll;

	protected Property lettersAlignable;

	protected Property charactersP;

	protected Property abbrn;

	protected Property form;

	protected PrintWriter writer;

	protected boolean wordCorpus;

	public OriflammsFunction(CQPCorpus corpus, File tsvFile) throws CqiClientException, IOException, CqiServerError {
		this.tsvFile = tsvFile;
		this.corpus = corpus;
	}

	boolean process() throws Exception {

		StructuralUnit w = corpus.getStructuralUnit("w");

		wordCorpus = (w == null);
		if (wordCorpus) {
			System.out.println("Corpus de mots");
		}
		else {
			System.out.println("Corpus de lettres");
		}

		String[] props = { "pbid", "pbstart", "pbend", "cbid",
				"cbstart", "cbend", "lbid", "lbstart", "lbend", "letters-all",
				"letters-alignable", "characters", "abbr-n" };
		for (String prop : props) {
			if (corpus.getProperty(prop) == null) {
				System.out.println("Le corpus '" + corpus + "' n'a pas de propriété de mot '" + prop + "'. Abandon.");
				return false;
			}
		}

		text_su = corpus.getStructuralUnit("text");
		text_id = text_su.getProperty("id");
		pb_id = corpus.getProperty("pbid");
		cb_id = corpus.getProperty("cbid");
		lb_id = corpus.getProperty("lbid");
		lettersAll = corpus.getProperty("letters-all");
		lettersAlignable = corpus.getProperty("letters-alignable");
		charactersP = corpus.getProperty("characters");
		abbrn = corpus.getProperty("abbr-n");
		form = corpus.getProperty("word");

		writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tsvFile), "UTF-8")));
		writer.println("text_id\tpb_id\tcb_id\tlb_id\tline part\tNabbr\tNcharAbbr\ttotal\t%=NcharAbbr/Ntotal");
		StructuredPartition p = new StructuredPartition(corpus);
		p.setParameters("tmp", text_id, null);
		p.compute();
		for (Part part : p.getParts()) {
			processText(part, part.getName());
		}
		writer.close();
		p.delete();
		System.out.println("Result saved in " + tsvFile.getAbsolutePath());
		return true;
	}

	boolean processText(CQPCorpus corpus, String text_id) throws CqiClientException, IOException, CqiServerError {

		List<? extends org.txm.objects.Match> matches = corpus.getMatches();
		org.txm.objects.Match last_match = matches.get(matches.size() - 1);
		int end = last_match.getEnd();

		QueryResult r = corpus.query(new CQLQuery("[pbstart=\"0\"]"), "ABBRORI1", false);
		int[] pb_pos = r.getStarts();
		r.drop();
		r = corpus.query(new CQLQuery("[cbstart=\"0\"]"), "ABBRORI2", false);
		int[] cb_pos = r.getStarts();
		r.drop();
		r = corpus.query(new CQLQuery("[lbstart=\"0\"]"), "ABBRORI3", false);
		int[] lb_pos = r.getStarts();
		r.drop();

		System.out.println("N pb = " + pb_pos.length);
		System.out.println("N cb = " + cb_pos.length);
		System.out.println("N lb = " + lb_pos.length);

		int[] pb_idx = CQI.cpos2Id(pb_id.getQualifiedName(), pb_pos);
		int[] cb_idx = CQI.cpos2Id(cb_id.getQualifiedName(), cb_pos);
		int[] lb_idx = CQI.cpos2Id(lb_id.getQualifiedName(), lb_pos);

		String[] pb_idx_str = CQI.id2Str(pb_id.getQualifiedName(), pb_idx);
		String[] cb_idx_str = CQI.id2Str(cb_id.getQualifiedName(), cb_idx);
		String[] lb_idx_str = CQI.id2Str(lb_id.getQualifiedName(), lb_idx);

		ConsoleProgressBar cpb = new ConsoleProgressBar(lb_pos.length);
		int p = 0;
		int c = 0;

		for (int l = 0; l < lb_pos.length; l++) {
			cpb.tick();

			while (c < cb_pos.length - 1 && lb_pos[l] > cb_pos[c + 1]) {
				c++;
			}
			while (p < pb_pos.length - 1 && lb_pos[l] > pb_pos[p + 1]) {
				p++;
			}

			int line_length;
			if (l == lb_pos.length - 1) line_length = end - lb_pos[l];
			else line_length = lb_pos[l + 1] - lb_pos[l];

			Object[] infos = getInfos(lb_pos[l], line_length); //(abbrNs, allLetters, alignableLetters, characters, words)

			processLine(text_id, pb_idx_str[p], cb_idx_str[c], lb_idx_str[l], line_length, infos);
		}
		return true;
	}

	abstract boolean processLine(String text_id, String pb_id, String cb_id, String lb_id, int length, Object[] infos);

	abstract Object[] getInfos(int from, int length) throws UnexpectedAnswerException, IOException, CqiServerError;

}
