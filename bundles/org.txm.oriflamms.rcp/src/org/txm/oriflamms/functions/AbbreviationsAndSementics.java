// STANDARD DECLARATIONS
package org.txm.oriflamms.functions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.serverException.CqiServerError;

public class AbbreviationsAndSementics extends OriflammsFunction {

	protected String entities;

	protected String[] entitiesArray;

	protected ArrayList<StructuralUnitProperty> structures;

	public AbbreviationsAndSementics(CQPCorpus corpus, File tsvFile, String entities) throws CqiClientException, IOException,
			CqiServerError {
		super(corpus, tsvFile);

		entitiesArray = entities.split(",");
		structures = new ArrayList<StructuralUnitProperty>();
		for (String structName : entitiesArray) {
			StructuralUnit su = corpus.getStructuralUnit(structName);
			if (su != null) {
				StructuralUnitProperty sup = su.getProperty("n");
				if (sup != null)
					structures.add(sup);
			}
		}

	}

	public boolean process() throws Exception {

		System.out.println("Dénombrement des abbréviations de " + corpus + " en '" + (wordCorpus ? "mots" : "lettres") + "' pour les entités " + entities);
		if (structures.size() == 0) {
			System.out.println("Erreur: pas de structures disponibles pour les entités suivantes " + entities);
			return false;
		}
		return super.process();
	}

	Object[] getInfos(int from, int length) throws UnexpectedAnswerException, IOException, CqiServerError {

		int[] positions = new int[length];
		for (int i = 0; i < length; i++)
			positions[i] = from++;

		String[] abbrNs = CQI.cpos2Str(abbrn.getQualifiedName(), positions);
		String[] allLetters = CQI.cpos2Str(lettersAll.getQualifiedName(), positions);
		String[] alignableLetters = CQI.cpos2Str(lettersAlignable.getQualifiedName(), positions);
		String[] characters = CQI.cpos2Str(charactersP.getQualifiedName(), positions);
		String[] words = CQI.cpos2Str(form.getQualifiedName(), positions);
		HashMap<String, int[]> structuresPositions = new HashMap<String, int[]>();
		for (StructuralUnitProperty structProp : structures) {
			structuresPositions.put(structProp.getFullName(), CQI.cpos2Struc(structProp.getQualifiedName(), positions));
		}

		Object[] rez = { abbrNs, allLetters, alignableLetters, characters, words, structuresPositions };
		return rez;
	}

	boolean processLine(String text_id, String pb_id, String cb_id, String lb_id, int length, Object[] infos) {

		String[] abbrNs = (String[]) infos[0];
		String[] allLetters = (String[]) infos[1];
		String[] alignableLetters = (String[]) infos[2];
		String[] characters = (String[]) infos[3];
		String[] words = (String[]) infos[4];
		HashMap<String, int[]> structuresPositions = (HashMap<String, int[]>) infos[5];

		int NabbrTotal = 0, NsupAbbrTotal = 0, NtotalTotal = 0;
		for (int i = 0; i < length; i++) {
			NabbrTotal += Integer.parseInt(abbrNs[i]);
			NsupAbbrTotal += allLetters[i].length() - characters[i].length();
			NtotalTotal += allLetters[i].length();
		}

		for (StructuralUnitProperty strutcProp : structures) {
			int Nabbr = 0, NsupAbbr = 0, Ntotal = 0;
			int[] structureP = structuresPositions.get(strutcProp.getFullName());

			for (int i = 0; i < length; i++) {
				if (structureP[i] >= 0) { // the position is in the structure
					Nabbr += Integer.parseInt(abbrNs[i]);
					NsupAbbr += allLetters[i].length() - characters[i].length();
					Ntotal += allLetters[i].length();
				}
			}

			writer.println(text_id + "\t" + pb_id + "\t" + cb_id + "\t" + lb_id + "\t" + strutcProp.getStructuralUnit() + "\t" + Nabbr + "\t" + NsupAbbr + "\t" + Ntotal + "\t"
					+ ((float) NsupAbbr / (float) Ntotal));
			NabbrTotal -= Nabbr;
			NsupAbbrTotal -= NsupAbbr;
			NtotalTotal -= Ntotal;
		}

		int Nabbr = 0, NsupAbbr = 0, Ntotal = 0;
		writer.println(
				text_id + "\t" + pb_id + "\t" + cb_id + "\t" + lb_id + "\t#REST\t" + NabbrTotal + "\t" + NsupAbbrTotal + "\t" + NtotalTotal + "\t" + ((float) NsupAbbrTotal / (float) NtotalTotal));
		return true;
	}
}
