// STANDARD DECLARATIONS
package org.txm.oriflamms.functions;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

import javax.xml.stream.XMLStreamException;

import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.serverException.CqiServerError;

public class UpdateCorpusImagePaths {

	public boolean process(MainCorpus corpus, String newpathprefix) throws CqiClientException, IOException, CqiServerError, XMLStreamException {

		File binDir = corpus.getProjectDirectory();
		File htmlDir = new File(binDir, "HTML");
		if (!htmlDir.exists()) {
			System.out.println("Error: no html directory: " + htmlDir);
			return false;
		}
		File corpushtmlDir = new File(htmlDir, corpus.getID());
		if (!corpushtmlDir.exists()) {
			System.out.println("Error: no html corpus directory: " + corpushtmlDir);
			return false;
		}
		File corpusdefaulthtmlDir = new File(corpushtmlDir, "facsimile");
		if (!corpusdefaulthtmlDir.exists()) {
			System.out.println("Error: no html 'facsimile' corpus directory: " + corpusdefaulthtmlDir);
			return false;
		}
		if (newpathprefix == null) {
			newpathprefix = corpusdefaulthtmlDir.getAbsolutePath() + "/images/";
			System.out.println("No image path prefix specified, using corpus 'facsimile' HTML directory path: " + newpathprefix);
		}

		File[] files = corpusdefaulthtmlDir.listFiles(new FileFilter() {

			@Override
			public boolean accept(File f) {
				return f.isFile() && f.getName().endsWith(".html") && !f.isHidden();
			}
		});

		if (files == null || files.length == 0) {
			System.out.println("Error: no html files in HTML default corpus directory: " + corpusdefaulthtmlDir);
			return false;
		}

		for (File htmlFile : files) {
			UpdateHTMLFileImagePaths uhfip = new UpdateHTMLFileImagePaths(htmlFile, newpathprefix);
			File outhtmlFile = new File(htmlFile.getAbsolutePath() + ".tmp");
			if (!uhfip.process(outhtmlFile)) {
				System.out.println("Fail to process HTML file: " + htmlFile);
				return false;
			}

			if (htmlFile.delete() && outhtmlFile.renameTo(htmlFile)) {
				//ok
			}
			else {
				System.out.println("Fail to replace HTML file: " + htmlFile + " with " + outhtmlFile);
				return false;
			}
		}
		return true;
	}
}
