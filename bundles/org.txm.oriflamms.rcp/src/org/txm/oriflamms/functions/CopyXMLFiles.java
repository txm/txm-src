package org.txm.oriflamms.functions;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLResolver;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.txm.utils.io.FileCopy;

public class CopyXMLFiles {

	File xmlFile;

	File outDir;

	ArrayList<File> dtdFiles = new ArrayList<>();

	public CopyXMLFiles(File xmlFile) {
		this.xmlFile = xmlFile;
	}

	public ArrayList<File> copy(File outDir) throws IOException, XMLStreamException {
		XMLInputFactory factory;
		XMLStreamReader parser;
		InputStream inputData = xmlFile.toURI().toURL().openStream();
		factory = XMLInputFactory.newInstance();
		factory.setXMLResolver(new XMLResolver() {

			@Override
			public Object resolveEntity(String publicID, String systemID,
					String baseURI, String namespace) throws XMLStreamException {
				File srcFile = new File(xmlFile.getParentFile(), systemID);
				dtdFiles.add(srcFile);
				try {
					return srcFile.toURI().toURL().openStream();
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return new ArrayList<>();
				}
			}
		});

		parser = factory.createXMLStreamReader(inputData);

		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {

		}
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
		dtdFiles.add(xmlFile);
		for (File dtd : dtdFiles) {
			File cpy = new File(outDir, dtd.getName());
			FileCopy.copy(dtd, cpy);
		}
		return dtdFiles;
	}

	public static void main(String[] args) throws IOException, XMLStreamException {
		File xmlFile = new File("/home/mdecorde/Téléchargements/Inscriptions1.xml");
		File outDir = new File("/home/mdecorde/Téléchargements/test");
		outDir.mkdir();
		CopyXMLFiles cdf = new CopyXMLFiles(xmlFile);
		System.out.println(cdf.copy(outDir));
	}
}
