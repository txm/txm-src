package org.txm.oriflamms.functions;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamException;

import org.txm.importer.StaxIdentityParser;
import org.txm.scripts.importer.StaxParser;
import org.txm.utils.FileUtils;

class OntologiesProjection extends StaxIdentityParser {

	File xmlFile;

	String wordTag;

	String textname;

	String milestone;

	String group;

	HashMap<String, String[]> links = new HashMap<>();

	HashMap<String, List<Serializable>> prefixDefsPatterns = new HashMap<>();

	HashMap<String, HashMap<String, HashMap<String, String>>> ggly_ontologies = new HashMap<>();

	HashMap<String, HashMap<String, String>> ggly_ontologies_unicodechars = new HashMap<>();

	HashMap<String, HashMap> lgly_ontologies = new HashMap<>();

	String current_ontology_link_file_name = "";

	File ontologies_links_directory;

	public OntologiesProjection(File xmlFile, File corpusDirectory) throws IOException, XMLStreamException {
		super(xmlFile);

		this.xmlFile = xmlFile;
		this.ontologies_links_directory = new File(corpusDirectory, "ontologies_links");

		textname = FileUtils.stripExtension(xmlFile);
		textname = textname.replaceAll("-c", "");

		this.wordTag = "c";
	}

	public boolean buildGGlyOntology(String prefix) {
		String path = (String) prefixDefsPatterns.get(prefix).get(1);
		int idx = path.indexOf("#");
		if (idx > 0) path = path.substring(0, idx);

		File ggly_ontology_file = new File(xmlFile.getParentFile(), "../" + path);
		if (!ggly_ontology_file.exists()) {
			System.out.println("WARNING: cannot found global ontology file: " + ggly_ontology_file);
			return false;
		}
		final HashMap<String, HashMap<String, String>> global_ontologies = new HashMap<>();
		final HashMap<String, String> unicode_global_ontologies = new HashMap<>();
		StaxParser pontologies = new StaxParser(ggly_ontology_file) {

			boolean startChar = false, startLocalName = false, startValue = false, startMapping = false;

			String unicodeChar, standardizedChar, subtype, type;

			String id, charLocalName, charValue;

			StringBuilder c = new StringBuilder();

			@Override
			public void processStartElement() {
				if (localname.equals("char")) {
					// get id
					for (int i = 0; i < parser.getAttributeCount(); i++) {
						if (parser.getAttributeLocalName(i).equals("id")) {
							id = parser.getAttributeValue(i);
							break;
						}
					}
					startChar = true;
					c.setLength(0);
				}
				else if (localname.equals("mapping")) {
					subtype = "";
					type = "";
					for (int i = 0; i < parser.getAttributeCount(); i++) {
						if (parser.getAttributeLocalName(i).equals("subtype")) {
							subtype = parser.getAttributeValue(i);
						}
						else if (parser.getAttributeLocalName(i).equals("type")) {
							type = parser.getAttributeValue(i);
						}
					}
					startMapping = true;
					c.setLength(0);
				}
				else if (localname.equals("localName")) {
					startLocalName = true;
					c.setLength(0);
				}
				else if (localname.equals("value")) {
					startLocalName = true;
					c.setLength(0);
				}
			}

			@Override
			public void processCharacters() {
				if (startMapping) c.append(parser.getText());
				else if (startLocalName) c.append(parser.getText());
				else if (startValue) c.append(parser.getText());
			}

			@Override
			public void processEndElement() {
				if (localname.equals("char")) {
					startChar = false;
					HashMap<String, String> h = new HashMap<>();
					h.put("standard", standardizedChar);
					h.put("unicode", unicodeChar);
					h.put("value", charValue);
					h.put("localname", charLocalName);
					global_ontologies.put(id, h);
					unicode_global_ontologies.put(unicodeChar, standardizedChar);
				}
				else if (localname.equals("mapping")) {
					if (subtype.equals("Unicode")) {
						unicodeChar = c.toString().trim();
					}
					else if (type.equals("standardized")) {
						standardizedChar = c.toString().trim();
					}
					startMapping = false;
				}
				else if (localname.equals("localName")) {
					charLocalName = c.toString().trim();
					startLocalName = false;
				}
				else if (localname.equals("value")) {
					charValue = c.toString().trim();
					startValue = false;
				}
			}
		};
		pontologies.process();
		ggly_ontologies.put(prefix, global_ontologies);
		ggly_ontologies_unicodechars.put(prefix, unicode_global_ontologies);
		// System.out.println(ggly_ontologies
		return true;
	}

	public boolean buildLGlyOntology(String prefix) {
		String path = (String) prefixDefsPatterns.get(prefix).get(1);
		int idx = path.indexOf("#");
		if (idx > 0) path = path.substring(0, idx);

		File lgly_ontology_file = new File(ontologies_links_directory, textname + "-ontolinks.xml"); // add "../" because we are in txm/<corpus>-c directory
		if (!lgly_ontology_file.exists()) {
			System.out.println("WARNING: cannot find Local ontology file " + lgly_ontology_file);
			return false;
		}

		final HashMap<String, HashMap> local_ontologies = new HashMap<>();
		StaxParser pontologies = new StaxParser(lgly_ontology_file) {

			boolean startNote = false;

			String id, change, parent;

			StringBuilder c = new StringBuilder();

			HashMap<String, String> glyph = new HashMap<>();

			@Override
			public void processStartElement() {
				if (localname.equals("glyph")) {
					// get id
					for (int i = 0; i < parser.getAttributeCount(); i++) {
						change = "";
						if (parser.getAttributeLocalName(i).equals("id")) {
							id = parser.getAttributeValue(i);
						}
						else if (parser.getAttributeLocalName(i).equals("change")) {
							change = parser.getAttributeValue(i);
						}
					}
					glyph = new HashMap<>();
					glyph.put("change", change);
					glyph.put("id", id); // new glyph
					parent = null;
				}
				else if (localname.equals("note")) {
					startNote = true;
					c.setLength(0);
				}
			}

			@Override
			public void processCharacters() {
				if (startNote) c.append(parser.getText());
			}

			@Override
			public void processEndElement() {
				if (localname.equals("char")) {
					if (parent != null)
						glyph.put("parent", glyph.get(parent));
					local_ontologies.put(id, glyph);
				}
				else if (localname.equals("note")) {
					parent = c.toString().trim();
					startNote = false;
				}
			}
		};
		pontologies.process();
		lgly_ontologies.put(prefix, local_ontologies);

		return true;
	}

	public void loadOntologyLinkFile(String name) {
		links = new HashMap();
		prefixDefsPatterns = new HashMap();
		prefixDefsPatterns.put("ggly", Arrays.asList(Pattern.compile("([a-z]+)"), "../../charDecl.xml#$1"));
		prefixDefsPatterns.put("lgly", Arrays.asList(Pattern.compile("([a-z]+)"), "../ontologies/" + textname + ".xml#$1"));
		prefixDefsPatterns.put("txt", Arrays.asList(Pattern.compile("([a-z]+)"), "../texts/" + textname + ".xml#$1"));

		lgly_ontologies = new HashMap();
		ggly_ontologies = new HashMap();
		File ontology_link_file = new File(ontologies_links_directory, name);
		if (!ontology_link_file.exists()) {
			System.out.println("WARNING: no ontology link file: " + ontology_link_file);
			return;
		}

		StaxParser pLinks = new StaxParser(ontology_link_file) {

			@Override
			public void processStartElement() {
				if (localname.equals("linkGrp")) {
					for (int i = 0; i < parser.getAttributeCount(); i++) {
						if (parser.getAttributeLocalName(i).equals("type")) {
							group = parser.getAttributeValue(i);
							break;
						}
					}
				}
				else if (localname.equals("prefixDef")) {
					String ident = null, matchPattern = null, replacementPattern = null;
					for (int i = 0; i < parser.getAttributeCount(); i++) {
						if (parser.getAttributeLocalName(i).equals("ident")) {
							ident = parser.getAttributeValue(i);
						}
						else if (parser.getAttributeLocalName(i).equals("matchPattern")) {
							matchPattern = parser.getAttributeValue(i);
						}
						else if (parser.getAttributeLocalName(i).equals("replacementPattern")) {
							replacementPattern = parser.getAttributeValue(i);
						}
					}
					if (ident != null && matchPattern != null && replacementPattern != null && !ident.equals("txt")) {
						prefixDefsPatterns.put(ident, Arrays.asList(Pattern.compile(matchPattern), replacementPattern));
						OntologiesProjection.this.getOntology(ident);
					}
				}
				else if (localname.equals("link")) {
					String target = "";

					for (int i = 0; i < parser.getAttributeCount(); i++) {
						if (parser.getAttributeLocalName(i).equals("target")) {
							target = parser.getAttributeValue(i);
							break;
						}
					}

					String[] split = target.split(" ", 2); // first part word id next part are the ontologies id
					links.put(split[0].substring(4), split[1].split(" "));
				}
			}
		};
		pLinks.process();
		// System.out.println("links size: "+links.size()
		// System.out.println("ggly_ontologies size: "+ggly_ontologies.size()
		// System.out.println("lgly_ontologies size: "+lgly_ontologies.size()
	}

	public HashMap<String, HashMap<String, String>> getOntology(String prefix) {
		if (prefix.startsWith("ggly")) {
			if (!ggly_ontologies.containsKey(prefix)) buildGGlyOntology(prefix);
			return ggly_ontologies.get(prefix);
		}
		else if (prefix.startsWith("lgly")) {
			if (!lgly_ontologies.containsKey(prefix)) buildLGlyOntology(prefix);
			return lgly_ontologies.get(prefix);
		}
		return null;
	}

	@Override
	public void processStartElement() throws XMLStreamException, IOException {
		super.processStartElement();
		if (localname.equals("milestone")) {
			String id = "";
			String unit = "";
			for (int i = 0; i < parser.getAttributeCount(); i++) {
				if (parser.getAttributeLocalName(i).equals("id")) {
					id = parser.getAttributeValue(i);
				}
				else if (parser.getAttributeLocalName(i).equals("unit")) {
					unit = parser.getAttributeValue(i);
				}
			}

			if (unit.equals("surface")) {
				milestone = id;
			}
		}
		else if (localname.equals(wordTag)) {
			String id = "";
			String characters = "";
			for (int i = 0; i < parser.getAttributeCount(); i++) {
				if (parser.getAttributeLocalName(i).equals("id")) {
					id = parser.getAttributeValue(i);
				}
				else if (parser.getAttributeLocalName(i).equals("characters")) {
					characters = parser.getAttributeValue(i);
				}
			}

			String ontology_link_file_name = textname + "-ontolinks.xml";
			if (!current_ontology_link_file_name.equals(ontology_link_file_name)) { // rebuild hashmaps
				current_ontology_link_file_name = ontology_link_file_name;
				loadOntologyLinkFile(ontology_link_file_name);
				getOntology("ggly");
			}

			String sign = null, allographExpert = null, allographAutomatic = null; // default value is attribute characters

			// AUTO ALLOGRAPH
			if (links.containsKey(id))
				for (String link : links.get(id)) { // automatic allograph loop
					if (link.startsWith("lgly")) {
						int idx = link.indexOf(":");
						link = link.substring(idx + 1);
						if (link.startsWith("auto_")) { // automatic lgly
							if (allographAutomatic == null) allographAutomatic = link.substring(5);
							else if (allographAutomatic.length() + 5 < link.length()) allographAutomatic = link.substring(5);
						}
						else { // manual lgly

						}
					}
				}
			if (allographAutomatic == null) allographAutomatic = characters;

			// EXPERT ALLOGRAPH
			if (links.containsKey(id))
				for (String link : links.get(id)) { // expert allograph loop, try to find a ggly entity
					// getOntology("ggly")
					if (link.startsWith("ggly")) {
						int idx = link.indexOf(":");
						String prefix = link.substring(0, idx);
						link = link.substring(idx + 1);

						HashMap<String, HashMap<String, String>> onto = getOntology(prefix);
						if (onto != null) {
							HashMap<String, String> charOnto = onto.get(link);
							if (charOnto != null) {
								String localname = charOnto.get("localname");
								String value = charOnto.get("value");
								if ("entity".equals(localname)) {
									allographExpert = value;
								}
							}
						}
					}
				}
			if (allographExpert == null)
				if (links.containsKey(id))
					for (String link : links.get(id)) { // expert allograph loop, try to find the longest non-autolgly entity
						if (link.startsWith("lgly")) {
							int idx = link.indexOf(":");
							link = link.substring(idx + 1);
							if (!link.startsWith("auto_")) { // non automatic lgly
								// System.out.println("link= "+link
								if (allographExpert == null) allographExpert = link;
								else if (allographExpert.length() + 5 < link.length()) allographExpert = link;
							}
						}
					}
			if (allographExpert == null) allographExpert = allographAutomatic;

			// SIGN
			if (sign == null)
				if (links.containsKey(id))
					for (String link : links.get(id)) { // expert allograph loop, try to find the shortest ggly entity
						// getOntology("ggly")
						if (link.startsWith("ggly")) {
							int idx = link.indexOf(":");
							String prefix = link.substring(0, idx);
							link = link.substring(idx + 1);

							HashMap<String, HashMap<String, String>> onto = getOntology(prefix);
							if (onto != null) {
								HashMap<String, String> charOnto = onto.get(link);
								if (charOnto != null) {
									sign = charOnto.get("standard");
								}
							}
						}
					}
			if (sign == null)
				if (links.containsKey(id))
					for (String link : links.get(id)) { // sign loop, try to find the shortest non-autolgly entity
						if (link.startsWith("lgly")) {
							int idx = link.indexOf(":");
							link = link.substring(idx + 1);
							if (!link.startsWith("auto_")) { // non automatic lgly
								if (sign == null) sign = link;
								else if (sign.length() + 5 > link.length()) sign = link;
							}
						}
					}
			if (sign == null) {
				for (HashMap<String, String> chars : ggly_ontologies_unicodechars.values()) {
					// HashMap<String, String> chars = (HashMap<String, String>)ggly.get(1);
					if (chars.containsKey(characters)) sign = chars.get(characters);
				}
			}
			if (sign == null) sign = characters.toLowerCase();

			try {
				writer.writeAttribute("sign", sign);
				writer.writeAttribute("allograph-expert", allographExpert);
				writer.writeAttribute("allograph-auto", allographAutomatic);
			}
			catch (XMLStreamException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		File corpusDirectory = new File("/home/mdecorde/TEMP/testori/qgraal_cmTest");
		File xmlFile = new File(corpusDirectory, "txm/qgraal_cmTest-c/qgraal_cmTest-c_surf_qgraal_cmTest_lyonbm_pa77-160.xml");
		File outputFile = new File(corpusDirectory, "txm/qgraal_cmTest-c/out2.xml");

		try {
			OntologiesProjection cp = new OntologiesProjection(xmlFile, corpusDirectory);
			System.out.println(cp.process(outputFile));
		}
		catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
