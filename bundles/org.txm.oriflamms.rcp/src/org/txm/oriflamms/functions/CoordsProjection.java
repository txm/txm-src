package org.txm.oriflamms.functions;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.xml.stream.XMLStreamException;

import org.txm.importer.StaxIdentityParser;
import org.txm.scripts.importer.StaxParser;
import org.txm.utils.FileUtils;

class CoordsProjection extends StaxIdentityParser {

	File xmlFile;

	File img_links_directory;

	File zones_directory;

	String wordTag;

	String textname;

	String milestone;

	String current_img_file = "";

	String current_zone_file = "";

	String xmlType;

	String group;

	HashMap<String, String[]> zones = new HashMap<>();

	HashMap<String, String> links = new HashMap<>();

	public CoordsProjection(File xmlFile, File img_links_directory, File zones_directory, String wordTag) throws IOException, XMLStreamException {
		super(xmlFile);

		this.xmlFile = xmlFile;
		this.img_links_directory = img_links_directory;
		this.zones_directory = zones_directory;
		this.wordTag = wordTag;

		if (!FileUtils.isXMLFile(xmlFile)) {
			throw new IllegalArgumentException("File is not an XML file: " + xmlFile);
		}

		textname = FileUtils.stripExtension(xmlFile);

		int idx = textname.indexOf("-w");
		if (idx > 0) {
			textname = textname.substring(0, idx);
			xmlType = "word";
		}

		idx = textname.indexOf("-c");
		if (idx > 0) {
			textname = textname.substring(0, idx);
			xmlType = "character";
		}
	}

	@Override
	public void processStartElement() throws XMLStreamException, IOException {
		super.processStartElement();
		if (localname.equals("milestone")) {
			String id = "";
			String unit = "";
			for (int i = 0; i < parser.getAttributeCount(); i++) {
				if (parser.getAttributeLocalName(i).equals("id")) {
					id = parser.getAttributeValue(i);
				}
				else if (parser.getAttributeLocalName(i).equals("unit")) {
					unit = parser.getAttributeValue(i);
				}
			}

			if (unit.equals("surface")) {
				milestone = id;
			}
		}
		else if (localname.equals(wordTag)) {
			String id = "";
			for (int i = 0; i < parser.getAttributeCount(); i++) {
				if (parser.getAttributeLocalName(i).equals("id")) {
					id = parser.getAttributeValue(i);
					break;
				}
			}

			// load next data if needed
			String img_file_name = textname + "_" + milestone + "-links.xml";
			if (!current_img_file.equals(img_file_name)) { // rebuild hashmaps
				String zone_file_name = textname + "_" + milestone + "-zones.xml";
				loadNextData(img_file_name, zone_file_name);
			}

			// println "Find coords for word_id="+id+" in "+img_file_name+" and "+zone_file_name
			// println "zone: "+links[id]
			// println "coords: "+zones[links[id]]
			if (zones.size() > 0 && links.size() > 0) {
				String[] coords = zones.get(links.get(id));
				if (coords != null) {
					if (coords[0] == null || coords[1] == null || coords[2] == null || coords[3] == null) {
						System.out.println("WARNING one of coordinates is missing: " + coords);
					}
					else {
						try {
							writer.writeAttribute("x1", coords[0]);
							writer.writeAttribute("y1", coords[1]);
							writer.writeAttribute("x2", coords[2]);
							writer.writeAttribute("y2", coords[3]);
						}
						catch (XMLStreamException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				else {
					System.out.println("WARNING No group for word id=" + id + " and link id=" + links.get(id) + " in text " + textname);
				}
			}
		}
	}

	protected void loadNextData(String img_file_name, String zone_file_name) {
		File img_link_file = new File(img_links_directory, img_file_name);
		File zone_file = new File(zones_directory, zone_file_name);

		zones.clear();
		links.clear();
		if (zone_file.exists()) {
			StaxParser pZones = new StaxParser(zone_file) {

				@Override
				public void processStartElement() {
					if (localname.equals("zone")) {
						String type = "";
						String idZone = "";
						String ulx = "", uly = "", lrx = "", lry = "";

						for (int i = 0; i < parser.getAttributeCount(); i++) {
							if (parser.getAttributeLocalName(i).equals("id")) {
								idZone = parser.getAttributeValue(i);
							}
							else if (parser.getAttributeLocalName(i).equals("type")) {
								type = parser.getAttributeValue(i);
							}
							else if (parser.getAttributeLocalName(i).equals("ulx")) {
								ulx = parser.getAttributeValue(i);
							}
							else if (parser.getAttributeLocalName(i).equals("uly")) {
								uly = parser.getAttributeValue(i);
							}
							else if (parser.getAttributeLocalName(i).equals("lrx")) {
								lrx = parser.getAttributeValue(i);
							}
							else if (parser.getAttributeLocalName(i).equals("lry")) {
								lry = parser.getAttributeValue(i);
							}
						}

						if (type.equals(xmlType)) {
							zones.put(idZone, new String[] { ulx, uly, lrx, lry });
						}

					}
				}
			};
			pZones.process();
		}
		if (img_link_file.exists()) {
			StaxParser pLinks = new StaxParser(img_link_file) {

				@Override
				public void processStartElement() {
					if (localname.equals("linkGrp")) {
						for (int i = 0; i < parser.getAttributeCount(); i++) {
							if (parser.getAttributeLocalName(i).equals("type")) {
								group = parser.getAttributeValue(i);
								break;
							}
						}
					}
					else if (localname.equals("link") && group.startsWith(xmlType)) {
						String target = "";

						for (int i = 0; i < parser.getAttributeCount(); i++) {
							if (parser.getAttributeLocalName(i).equals("target")) {
								target = parser.getAttributeValue(i);
								break;
							}
						}

						String[] split = target.split(" ");
						links.put(split[0].substring(4), split[1].substring(4));
					}
				}
			};
			pLinks.process();
		}
		// println "zones size: "+zones.size()
		// println "links size: "+links.size()

		current_img_file = img_file_name;
	}

	public static void main(String[] args) {
		File corpusDirectory = new File("/home/mdecorde/TEMP/testori/FontenatTestAlignement");
		File xmlFile = new File(corpusDirectory, "txm/FontenayTest-w/FontenayTest-w.xml");
		File img_links_directory = new File(corpusDirectory, "img_links");
		File zones_directory = new File(corpusDirectory, "zones");

		File outputFile = new File(corpusDirectory, "txm/FontenayTest-w/FontenayTest-w-coords2.xml");

		CoordsProjection cp;
		try {
			cp = new CoordsProjection(xmlFile, img_links_directory, zones_directory, "w");
			System.out.println(cp.process(outputFile));
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
