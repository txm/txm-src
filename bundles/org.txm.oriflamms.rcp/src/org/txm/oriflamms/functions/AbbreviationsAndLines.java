// STANDARD DECLARATIONS
package org.txm.oriflamms.functions;

import java.io.File;
import java.io.IOException;

import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.serverException.CqiServerError;

public class AbbreviationsAndLines extends OriflammsFunction {

	protected int dist_start;

	protected int dist_end;

	public AbbreviationsAndLines(CQPCorpus corpus, File tsvFile, int dist_start, int dist_end) throws CqiClientException, IOException, CqiServerError {
		super(corpus, tsvFile);
	}

	public boolean process() throws Exception {

		dist_start = Math.abs(dist_start);
		dist_end = Math.abs(dist_end);

		System.out.println("Dénombrement des abbréviations de " + corpus + " en '" + (wordCorpus ? "mots" : "lettres") + "' pour des distances au début de " + dist_start + " et à la fin " + dist_end
				+ " de la ligne");
		return super.process();
	}

	boolean processLine(String text_id, String pb_id, String cb_id, String lb_id, int length, Object[] infos) {

		String[] abbrNs = (String[]) infos[0];
		String[] allLetters = (String[]) infos[1];
		String[] alignableLetters = (String[]) infos[2];
		String[] characters = (String[]) infos[3];
		String[] words = (String[]) infos[4];

		int Nabbr = 0, NsupAbbr = 0, Ntotal = 0;
		int p2 = length - dist_end;
		if (p2 < 0) p2 = length + 1;
		if (p2 < dist_start) p2 = dist_start;

		for (int i = 0; i < length; i++) {

			if (i == dist_start) {
				writer.println(text_id + "\t" + pb_id + "\t" + cb_id + "\t" + lb_id + "\ts\t" + Nabbr + "\t" + NsupAbbr + "\t" + Ntotal + "\t" + (100.0 * (float) NsupAbbr / (float) Ntotal));

				Nabbr = 0;
				NsupAbbr = 0;
				Ntotal = 0;
			}
			if (i == p2) {
				writer.println(text_id + "\t" + pb_id + "\t" + cb_id + "\t" + lb_id + "\tm\t" + Nabbr + "\t" + NsupAbbr + "\t" + Ntotal + "\t" + (100.0 * (float) NsupAbbr / (float) Ntotal));

				Nabbr = 0;
				NsupAbbr = 0;
				Ntotal = 0;
			}

			Nabbr += Integer.parseInt(abbrNs[i]);
			NsupAbbr += allLetters[i].length() - characters[i].length();
			Ntotal += allLetters[i].length();

		}
		writer.println(text_id + "\t" + pb_id + "\t" + cb_id + "\t" + lb_id + "\te\t" + Nabbr + "\t" + NsupAbbr + "\t" + Ntotal + "\t" + (100.0 * (float) NsupAbbr / (float) Ntotal));
		return true;
	}

	Object[] getInfos(int from, int length) throws UnexpectedAnswerException, IOException, CqiServerError {

		int[] positions = new int[length];
		for (int i = 0; i < length; i++)
			positions[i] = from++;
		AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();

		String[] abbrNs = CQI.cpos2Str(abbrn.getQualifiedName(), positions);
		String[] allLetters = CQI.cpos2Str(lettersAll.getQualifiedName(), positions);
		String[] alignableLetters = CQI.cpos2Str(lettersAlignable.getQualifiedName(), positions);
		String[] characters = CQI.cpos2Str(charactersP.getQualifiedName(), positions);
		String[] words = CQI.cpos2Str(form.getQualifiedName(), positions);

		String[][] rez = { abbrNs, allLetters, alignableLetters, characters, words };
		return rez;
	}
}
