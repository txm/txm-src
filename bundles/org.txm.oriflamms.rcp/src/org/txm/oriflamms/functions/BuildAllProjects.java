// STANDARD DECLARATIONS
package org.txm.oriflamms.functions;

import java.io.File;
import java.util.HashMap;

public class BuildAllProjects {

	public BuildAllProjects(File projectsDirectory, File xslDirectory) {

		HashMap<String, HashMap<String, String>> todo = new HashMap<String, HashMap<String, String>>();

		HashMap<String, String> c1 = new HashMap<>();
		//["xmlFile":new File(projectsDirectory, "Charrette_Ms_A.xml"),xslFile:new File(projectsDirectory, "oriflamms-convert-mss-dates-oriflammsxml.xsl"),imageDirectory:new File(projectsDirectory, "images"), createArchive:false],
		todo.put("CHARETTE", c1);
		HashMap<String, String> c2 = new HashMap<>();
		//"CHARETTEBIS":["xmlFile":new File(projectsDirectory, "Charrette_Ms_A.xml"),xslFile:new File(projectsDirectory, "oriflamms-convert-mss-dates-oriflammsxml.xsl"),imageDirectory:new File(projectsDirectory, "images"), createArchive:false],
		todo.put("CHARETTEBIS", c2);

		for (String k : todo.keySet()) {
			System.out.println("*** BUILD " + k + " ***");
			try {
				//TEI2Project p = new TEI2Project();
			}
			catch (Exception e) {
				System.out.println("ERROR WHILE PROCESSING " + k + ": " + e);
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		File projectsDirectory = new File("/home/mdecorde/TEMP/testori/corpus/");
		File xslDirectory = new File("/home/mdecorde/TXM/scripts/macro/org/txm/macro/oriflamms/prepare");
		BuildAllProjects bp = new BuildAllProjects(projectsDirectory, xslDirectory);
		System.out.println(bp);
	}
}

