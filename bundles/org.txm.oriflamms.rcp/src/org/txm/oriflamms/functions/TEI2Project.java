// STANDARD DECLARATIONS
package org.txm.oriflamms.functions;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.txm.importer.ApplyXsl2;
import org.txm.scripts.importer.WriteIdAndNAttributes;
import org.txm.utils.BundleUtils;
import org.txm.utils.DeleteDir;
import org.txm.utils.io.FileCopy;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;
import org.txm.utils.zip.Zip;

public class TEI2Project {

	File xmlFile;

	File xslFile;

	File imagesDirectory;

	boolean createArchive;

	public TEI2Project(File xmlFile, File xslFile, File imagesDirectory, boolean createArchive) {
		this.xmlFile = xmlFile;
		this.xslFile = xslFile;
		this.imagesDirectory = imagesDirectory;
		this.createArchive = createArchive;
	}

	public boolean process(IProgressMonitor monitor) throws IOException, XMLStreamException, TransformerException {
		if (!xmlFile.exists()) {
			System.out.println("Could not read input XML input file: " + xmlFile);
			return false;
		}

		File oriflammsMacroDirectory = new File(BundleUtils.getBundleFile("org.txm.oriflamms.rcp"), "res");
		System.out.println("Ressources files directory: " + oriflammsMacroDirectory);
		if (!oriflammsMacroDirectory.exists()) {
			System.out.println("Oriflamms macro directory not found: " + oriflammsMacroDirectory);
			return false;
		}
		File xslTokenizer = new File(oriflammsMacroDirectory, "oriflamms-tokenize-words.xsl");
		File xslPatchLbInWords = new File(oriflammsMacroDirectory, "oriflamms-patch-words-with-lb.xsl");
		File xslMissingMilestones = new File(oriflammsMacroDirectory, "oriflamms-patch-milestones.xsl");
		File xslCharactersTokenizer = new File(oriflammsMacroDirectory, "oriflamms-tokenize-chars-1-tag.xsl");
		File xslCharactersIdentifier = new File(oriflammsMacroDirectory, "oriflamms-tokenize-chars-2-identify.xsl");
		File xslZones = new File(oriflammsMacroDirectory, "oriflamms-convert-transcriptions-orizones.xsl");

		if (!xslTokenizer.exists() || !xslPatchLbInWords.exists() || !xslMissingMilestones.exists() ||
				!xslCharactersTokenizer.exists() || !xslCharactersIdentifier.exists() || !xslZones.exists()) {
			System.out.println("Could not find one of TXM's XSL file : " +
					Arrays.asList(xslTokenizer, xslPatchLbInWords, xslMissingMilestones,
							xslCharactersTokenizer, xslCharactersIdentifier, xslZones));

			System.out.println(Arrays
					.asList(xslTokenizer.exists(), xslPatchLbInWords.exists(), xslMissingMilestones.exists(), xslCharactersTokenizer.exists(), xslCharactersIdentifier.exists(), xslZones.exists())
					.toString());
			return false;
		}

		File xmlFileParentDirectory = xmlFile.getParentFile();
		String projectName = xmlFile.getName();
		if (projectName.indexOf(".") > 0) projectName = projectName.substring(0, projectName.indexOf("."));
		File projectDirectory = new File(xmlFileParentDirectory, projectName);
		DeleteDir.deleteDirectory(projectDirectory);
		if (projectDirectory.exists()) {
			System.out.println("Could not delete previous project directory: " + projectDirectory);
			return false;
		}

		projectDirectory.mkdir();

		if (!projectDirectory.exists()) {
			System.out.println("Could not create project directory: " + projectDirectory);
			return false;
		}

		System.out.println("Oriflamms project directory: " + projectDirectory);

		File xmlFileCopy = new File(projectDirectory, xmlFile.getName());
		System.out.println("Copying XML files: " + xmlFile + " to " + projectDirectory);
		CopyXMLFiles cdf = new CopyXMLFiles(xmlFile);
		projectDirectory.mkdir();
		System.out.println("Files copied: " + cdf.copy(projectDirectory));
		if (!xmlFileCopy.exists()) {
			System.out.println("Could not copy input XML input file: " + xmlFile + " to " + xmlFileCopy);
			return false;
		}

		if (xslFile != null) {
			if (xslFile.exists()) {
				System.out.println("Applying " + xslFile + " to " + xmlFileCopy + "...");
				ApplyXsl2 builder = new ApplyXsl2(xslFile);
				if (!builder.process(xmlFileCopy, xmlFileCopy)) {
					System.out.println("Failed to process " + xmlFileCopy + " with " + xslFile);
					return false;
				}
			}
		}

		File textsDirectory = new File(projectDirectory, "texts");
		File imgDirectory = new File(projectDirectory, "img");
		File img_linksDirectory = new File(projectDirectory, "img_links");
		File ontologiesDirectory = new File(projectDirectory, "ontologies");
		File ontologies_linksDirectory = new File(projectDirectory, "ontologies_links");
		File zonesDirectory = new File(projectDirectory, "zones");
		textsDirectory.mkdir();
		imgDirectory.mkdir();
		img_linksDirectory.mkdir();
		ontologiesDirectory.mkdir();
		ontologies_linksDirectory.mkdir();
		zonesDirectory.mkdir();

		File xmlWFile = new File(textsDirectory, projectName + "-w.xml");
		File xmlWCFile = new File(textsDirectory, projectName + "-c.xml");

		try {

			System.out.println("Applying " + xslMissingMilestones + " to " + xmlWFile + "...");
			if (monitor != null) {
				monitor.worked(1);
				monitor.subTask("Applying " + xslMissingMilestones + " to " + xmlWFile + "...");
			}
			ApplyXsl2 builder = new ApplyXsl2(xslMissingMilestones);
			if (!builder.process(xmlFileCopy, xmlWFile)) {
				System.out.println("Failed to process " + xmlWFile + " with " + xslMissingMilestones);
				return false;
			}

			System.out.println("Applying " + xslTokenizer + " to " + xmlWFile + "...");
			if (monitor != null) {
				monitor.worked(15);
				monitor.subTask("Applying " + xslTokenizer + " to " + xmlWFile + "...");
			}
			;
			builder = new ApplyXsl2(xslTokenizer);
			if (!builder.process(xmlWFile, xmlWFile)) {
				System.out.println("Failed to process " + xmlFileCopy + " with " + xslTokenizer);
				return false;
			}

			System.out.println("Merging words </w><w>");
			if (monitor != null) {
				monitor.worked(15);
				monitor.subTask("Merging words </w><w>");
			}
			String content = IOUtils.getText(xmlWFile, "UTF-8");
			content = content.replaceAll("</w><w[^>]*>", "");
			content = content.replaceAll("</w>\\s*(<milestone[^>]*>)?\\s*(<pb[^>]*>)?\\s*(<cb[^>]*>)?\\s*(<lb[^>]*break=\"no\"[^>]*>)\\s*<w[^>]*>", "$1$2$3$4");
			try {
				PrintWriter writer = IOUtils.getWriter(xmlWFile);
				writer.print(content);
				writer.close();
			}
			catch (Exception e2) {
				System.out.println("Error while fixing words: " + e2);
				return false;
			}

			System.out.println("Applying " + xslPatchLbInWords + " to " + xmlWFile + "...");
			if (monitor != null) {
				monitor.worked(15);
				monitor.subTask("Applying " + xslPatchLbInWords + " to " + xmlWFile + "...");
			}
			builder = new ApplyXsl2(xslPatchLbInWords);
			if (!builder.process(xmlWFile, xmlWFile)) {
				System.out.println("Failed to process " + xmlFileCopy + " with " + xslPatchLbInWords);
				return false;
			}

			System.out.println("Fixing 'id' and 'n' attributes in " + xmlWFile + "...");
			if (monitor != null) {
				monitor.worked(15);
				monitor.subTask("Fixing 'id' and 'n' attributes in " + xmlWFile + "...");
			}
			WriteIdAndNAttributes wiana = new WriteIdAndNAttributes(xmlWFile, projectName);
			File tmp = new File(xmlWFile.getParentFile(), "tmp_" + xmlWFile.getName());
			if (!wiana.process(tmp)) {
				System.out.println("Failed to fix id and n attributes with of " + xmlWFile + " file");
				return false;
			}
			else {
				wiana = null;
				xmlWFile.delete();
				tmp.renameTo(xmlWFile);
				if (tmp.exists()) {
					System.out.println("Failed to replace " + xmlWFile + " with result file " + tmp);
					return false;
				}
			}

			System.out.println("Applying " + xslCharactersTokenizer + " to " + xmlWFile + "...");
			if (monitor != null) {
				monitor.worked(15);
				monitor.subTask("Applying " + xslCharactersTokenizer + " to " + xmlWFile + "...");
			}
			builder = new ApplyXsl2(xslCharactersTokenizer);
			if (!builder.process(xmlWFile, xmlWCFile)) {
				System.out.println("Failed to process " + xmlWFile + " with " + xslCharactersTokenizer);
				return false;
			}

			System.out.println("Applying " + xslCharactersIdentifier + " to " + xmlWCFile + "...");
			if (monitor != null) {
				monitor.worked(1);
				monitor.subTask("Applying " + xslCharactersIdentifier + " to " + xmlWCFile + "...");
			}
			builder = new ApplyXsl2(xslCharactersIdentifier);
			if (!builder.process(xmlWCFile, xmlWCFile)) {
				System.out.println("Failed to process " + xmlWCFile + " with " + xslCharactersIdentifier);
				return false;
			}

			System.out.println("Applying " + xslZones + " to " + xmlWFile + "...");
			if (monitor != null) {
				monitor.worked(15);
				monitor.subTask("Applying " + xslZones + " to " + xmlWFile + "...");
			}
			builder = new ApplyXsl2(xslZones);
			if (!builder.process(xmlWFile, null)) {
				System.out.println("Failed to process " + xmlFileCopy + " with " + xslZones);
				return false;
			}

			if (imagesDirectory.exists() && imagesDirectory.listFiles(IOUtils.HIDDENFILE_FILTER).length > 0) {
				System.out.println("Copying images files from " + imagesDirectory + " to " + imgDirectory + "...");
				FileCopy.copyFiles(imagesDirectory, imgDirectory);
				File[] files = imgDirectory.listFiles(IOUtils.HIDDENFILE_FILTER);
				if (files != null) System.out.println("" + files.length + " images copied.");
			}

			if (createArchive) {
				if (monitor != null) {
					monitor.worked(15);
					monitor.subTask("Building Oriflamms binary project... ");
				}
				File zipFile = new File(xmlFileParentDirectory, projectName + ".oriflamms");
				zipFile.delete();
				Zip.compress(projectDirectory, zipFile);

				if (zipFile.exists()) {
					System.out.println("Project oriflamms exported to " + zipFile);
					DeleteDir.deleteDirectory(projectDirectory);
				}
				else {
					System.out.println("Fail to export project " + projectDirectory);
				}
			}

		}
		catch (Exception e) {
			System.out.println("Error while applying a XSL file: " + e);
			Log.printStackTrace(e);
		}
		if (monitor != null) monitor.done();
		return true;
	}
}
