package org.txm.oriflamms.functions;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;

import org.txm.importer.ApplyXsl2;
import org.txm.objects.BaseOldParameters;
import org.txm.utils.AsciiUtils;
import org.txm.utils.BundleUtils;
import org.txm.utils.DeleteDir;
import org.txm.utils.FileUtils;
import org.txm.utils.io.FileCopy;
import org.txm.utils.io.IOUtils;
import org.txm.utils.xml.UpdateXSLParameters;
import org.xml.sax.SAXException;

public class Project2XTZ {

	File projectDirectory;

	public Project2XTZ(File projectDirectory) {
		this.projectDirectory = projectDirectory;
	}

	public boolean process() throws IOException, TransformerException, ParserConfigurationException, SAXException, XMLStreamException {
		File oriflammsMacroDirectory = new File(BundleUtils.getBundleFile("org.txm.oriflamms.rcp"), "res");
		System.out.println("Ressources files directory: " + oriflammsMacroDirectory);
		if (!oriflammsMacroDirectory.exists()) {
			System.out.println("Oriflamms macro directory not found: " + oriflammsMacroDirectory + ". Aborting");
			return false;
		}
		File wFrontXSLFile = new File(oriflammsMacroDirectory, "txm-front-teioriflammsw-xtz.xsl");
		if (!wFrontXSLFile.exists()) {
			System.out.println("Oriflamms to XML-XTZ front XSL file is missing: " + wFrontXSLFile + ". Aborting");
			return false;
		}
		File cFrontXSLFile = new File(oriflammsMacroDirectory, "txm-front-teioriflammsc-xtz.xsl");
		if (!cFrontXSLFile.exists()) {
			System.out.println("Oriflamms to XML-XTZ front XSL file is missing: " + cFrontXSLFile + ". Aborting");
			return false;
		}

		File cSplitXSLFile = new File(oriflammsMacroDirectory, "1-oriflamms-split-surfaces.xsl");
		if (!cSplitXSLFile.exists()) {
			System.out.println("Oriflamms to XML-XTZ split XSL file is missing: " + cSplitXSLFile + ". Aborting");
			return false;
		}

		File editionXSLFile1 = new File(oriflammsMacroDirectory, "1-default-html.xsl");
		if (!editionXSLFile1.exists()) {
			System.out.println("Oriflamms to XML-XTZ edition XSL file is missing: " + editionXSLFile1 + ".");
			return false;
		}
		File editionXSLFile2 = new File(oriflammsMacroDirectory, "2-default-pager.xsl");
		if (!editionXSLFile2.exists()) {
			System.out.println("Oriflamms to XML-XTZ edition XSL file is missing: " + editionXSLFile2 + ".");
			return false;
		}
		File editionXSLFile3 = new File(oriflammsMacroDirectory, "3-facsimile-pager.xsl");
		if (!editionXSLFile3.exists()) {
			System.out.println("Oriflamms to XML-XTZ edition XSL file is missing: " + editionXSLFile3 + ".");
			return false;
		}
		File cssDirectory = new File(oriflammsMacroDirectory, "css");
		if (!cssDirectory.exists()) {
			System.out.println("Oriflamms css directory is missing: " + cssDirectory + ".");
			return false;
		}
		File jsDirectory = new File(oriflammsMacroDirectory, "js");
		if (!jsDirectory.exists()) {
			System.out.println("Oriflamms js directory is missing: " + jsDirectory + ".");
			return false;
		}
		File imagesDirectory = new File(oriflammsMacroDirectory, "images");
		if (!imagesDirectory.exists()) {
			System.out.println("Oriflamms images directory is missing: " + imagesDirectory + ".");
			return false;
		}

		File textDirectory = new File(projectDirectory, "texts");

		File txmDirectory = new File(projectDirectory, "txm");
		if (txmDirectory.exists()) DeleteDir.deleteDirectory(txmDirectory);
		txmDirectory.mkdir();
		if (!txmDirectory.exists()) {
			System.out.println("Error: the 'txm' directory could not be created: " + txmDirectory + ". Aborting.");
			return false;
		}

		File wDirectory = null;
		File cDirectory = null;
		File wFile = null;
		File cFile = null;

		File[] xmlFiles = textDirectory.listFiles(IOUtils.HIDDENFILE_FILTER);
		if (xmlFiles == null) return false;

		for (File xmlFile : xmlFiles) {
			if (xmlFile.getName().endsWith("-w.xml")) {
				String name = FileUtils.stripExtension(xmlFile);
				wDirectory = new File(txmDirectory, name);
				wFile = xmlFile;
			}
			else if (xmlFile.getName().endsWith("-c.xml")) {
				String name = FileUtils.stripExtension(xmlFile);
				cDirectory = new File(txmDirectory, name);
				cFile = xmlFile;
			}
		}

		if (wDirectory == null) {
			System.out.println("The Word corpus XML file was not found in " + textDirectory + ". Aborting.");
			return false;
		}
		if (cDirectory == null) {
			System.out.println("The Letter corpus XML file was not found in " + textDirectory + ". Aborting.");
			return false;
		}

		// Create XML-XTZ source directories
		wDirectory.mkdirs();
		cDirectory.mkdirs();

		// Copy XML files and split character XML file
		FileCopy.copy(wFile, new File(wDirectory, wFile.getName()));

		ApplyXsl2 builder = new ApplyXsl2(cSplitXSLFile);
		HashMap<String, String> xslParams = new HashMap<>();
		xslParams.put("output-directory", cDirectory.getAbsoluteFile().toURI().toString());
		for (String name : xslParams.keySet())
			builder.setParam(name, xslParams.get(name));
		if (!builder.process(cFile, null)) {
			System.out.println("Error: fail to split " + cFile);
			return false;
		}
		if (!ApplyXsl2.processImportSources(cFrontXSLFile, ApplyXsl2.listFiles(cDirectory), new HashMap<String, Object>())) {
			System.out.println("Error: fail to apply front XSL with " + cDirectory + " files");
			return false;
		}
		// INJECT ontologies
		System.out.println("Injecting ontologies...");
		for (File f : cDirectory.listFiles(IOUtils.HIDDENFILE_FILTER)) {
			if (f.getName().startsWith(cDirectory.getName())) {
				OntologiesProjection cp = new OntologiesProjection(f, projectDirectory);
				File outputFile = new File(cDirectory, "temp.xml");
				cp.process(outputFile);
				if (outputFile.exists() && f.delete() && outputFile.renameTo(f)) {

				}
				else {
					System.out.println("Failed to replace XML file " + f + " with " + outputFile);
					return false;
				}
			}
		}

		// INJECT word's coordinates
		System.out.println("Injecting coordinates...");
		File xmlFile = new File(wDirectory, wFile.getName());
		File img_links_directory = new File(projectDirectory, "img_links");
		File zones_directory = new File(projectDirectory, "zones");
		File outputFile = new File(wDirectory, "temp.xml");
		CoordsProjection cp = new CoordsProjection(xmlFile, img_links_directory, zones_directory, "w");
		if (cp.process(outputFile)) {
			if (outputFile.exists() && xmlFile.delete() && outputFile.renameTo(xmlFile)) {

			}
			else {
				System.out.println("Failed to replace XML file " + xmlFile + " with " + outputFile);
				return false;
			}
		}
		else {
			System.out.println("Coordinates injection failed. Aborting");
			return false;
		}

		// Create XSL directories

		File wXSLDirectory = new File(wDirectory, "xsl");
		File cXSLDirectory = new File(cDirectory, "xsl");

		// File cSplitXSLDirectory = new File(cXSLDirectory, "1-split-merge")
		// cSplitXSLDirectory.mkdirs()

		File wFrontXSLDirectory = new File(wXSLDirectory, "2-front");
		// File cFrontXSLDirectory = new File(cXSLDirectory, "2-front")
		wFrontXSLDirectory.mkdirs();
		// cFrontXSLDirectory.mkdirs()

		// Copy Split XSL file
		// File newCSplitXSLFile = new File(cSplitXSLDirectory, cSplitXSLFile.getName())
		// FileCopy.copy(cSplitXSLFile, newCSplitXSLFile);

		// Copy Front XSL file
		File newWFrontXSLFile = new File(wFrontXSLDirectory, wFrontXSLFile.getName());
		// File newCFrontXSLFile = new File(cFrontXSLDirectory, cFrontXSLFile.getName())
		FileCopy.copy(wFrontXSLFile, newWFrontXSLFile);
		// FileCopy.copy(cFrontXSLFile, newCFrontXSLFile);

		// Copy edition XSL file
		File wEditionXSLDirectory = new File(wXSLDirectory, "4-edition");
		File cEditionXSLDirectory = new File(cXSLDirectory, "4-edition");
		wEditionXSLDirectory.mkdirs();
		cEditionXSLDirectory.mkdirs();
		File newWEditionXSLFile1 = new File(wEditionXSLDirectory, editionXSLFile1.getName());
		File newCEditionXSLFile1 = new File(cEditionXSLDirectory, editionXSLFile1.getName());
		FileCopy.copy(editionXSLFile1, newWEditionXSLFile1);
		FileCopy.copy(editionXSLFile1, newCEditionXSLFile1);
		File newWEditionXSLFile2 = new File(wEditionXSLDirectory, editionXSLFile2.getName());
		File newCEditionXSLFile2 = new File(cEditionXSLDirectory, editionXSLFile2.getName());
		FileCopy.copy(editionXSLFile2, newWEditionXSLFile2);
		FileCopy.copy(editionXSLFile2, newCEditionXSLFile2);
		File newWEditionXSLFile3 = new File(wEditionXSLDirectory, editionXSLFile3.getName());
		File newCEditionXSLFile3 = new File(cEditionXSLDirectory, editionXSLFile3.getName());
		FileCopy.copy(editionXSLFile3, newWEditionXSLFile3);
		FileCopy.copy(editionXSLFile3, newCEditionXSLFile3);

		// patch XSL files with image directory path and set the 'word-element' xsl param
		File projectImgDirectory = new File(projectDirectory, "img");
		HashMap<String, String> parameters = new HashMap<>();
		parameters.put("image-directory", projectImgDirectory.getAbsolutePath());
		parameters.put("word-element", "w");
		System.out.println("update " + newWEditionXSLFile3 + " with " + parameters);
		UpdateXSLParameters p = new UpdateXSLParameters(newWEditionXSLFile3);
		if (!p.process(parameters)) {
			System.out.println("Fail to patch " + newWEditionXSLFile3);
			return false;
		}
		parameters = new HashMap<>();
		parameters.put("image-directory", projectImgDirectory.getAbsolutePath());
		parameters.put("word-element", "c");
		System.out.println("update " + newCEditionXSLFile3 + " with " + parameters);
		UpdateXSLParameters p2 = new UpdateXSLParameters(newCEditionXSLFile3);
		if (!p2.process(parameters)) {
			System.out.println("Fail to patch " + newCEditionXSLFile3);
			return false;
		}

		// Copy js and images directories
		File wCSSDirectory = new File(wDirectory, cssDirectory.getName());
		wCSSDirectory.mkdir();
		File wJsDirectory = new File(wDirectory, jsDirectory.getName());
		wJsDirectory.mkdir();
		File wImagesDirectory = new File(wDirectory, imagesDirectory.getName());
		wImagesDirectory.mkdir();
		File cCSSDirectory = new File(cDirectory, cssDirectory.getName());
		cCSSDirectory.mkdir();
		File cJsDirectory = new File(cDirectory, jsDirectory.getName());
		cJsDirectory.mkdir();
		File cImagesDirectory = new File(cDirectory, imagesDirectory.getName());
		cImagesDirectory.mkdir();
		FileCopy.copyFiles(cssDirectory, wCSSDirectory);
		FileCopy.copyFiles(jsDirectory, wJsDirectory);
		FileCopy.copyFiles(imagesDirectory, wImagesDirectory);
		FileCopy.copyFiles(cssDirectory, cCSSDirectory);
		FileCopy.copyFiles(jsDirectory, cJsDirectory);
		FileCopy.copyFiles(imagesDirectory, cImagesDirectory);

		// Prepare import.xml files
		File wImportXMLFile = new File(wDirectory, "import.xml");
		File cImportXMLFile = new File(cDirectory, "import.xml");

		BaseOldParameters.createEmptyParams(wImportXMLFile, AsciiUtils.buildId(wDirectory.getName()).toUpperCase());
		BaseOldParameters wParams = new BaseOldParameters(wImportXMLFile);
		wParams.load();
		wParams.setSkipTokenization(true);
		wParams.setWordElement("w");
		wParams.setDoAnnotation(false);
		wParams.setAnnotationLang("fr");
		wParams.setWordsPerPage(9999999);
		wParams.setTextualPlans("", "note", "teiHeader,facsimile", "pb,cb,lb");
		wParams.getCorpusElement().setAttribute("font", "Junicode");
		wParams.getEditionsElement(wParams.getCorpusElement()).setAttribute("default", "default,facsimile");
		wParams.getCorpusElement().setAttribute("name", AsciiUtils.buildId(wDirectory.getName()).toUpperCase());


		BaseOldParameters.createEmptyParams(cImportXMLFile, AsciiUtils.buildId(cDirectory.getName()).toUpperCase());
		BaseOldParameters cParams = new BaseOldParameters(cImportXMLFile);
		cParams.load();
		cParams.setSkipTokenization(true);
		cParams.setWordElement("c");
		cParams.setDoAnnotation(false);
		cParams.setAnnotationLang("fr");
		cParams.setWordsPerPage(9999999);
		cParams.setTextualPlans("", "note", "teiHeader,facsimile", "pb,cb,lb");
		cParams.getCorpusElement().setAttribute("font", "Junicode");
		cParams.getEditionsElement(cParams.getCorpusElement()).setAttribute("default", "default,facsimile");
		cParams.getCorpusElement().setAttribute("name", AsciiUtils.buildId(cDirectory.getName()).toUpperCase());

		return cParams.save() && wParams.save();
	}
}
