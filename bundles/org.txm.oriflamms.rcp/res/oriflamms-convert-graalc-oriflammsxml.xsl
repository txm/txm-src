<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:txm="http://textometrie.org/1.0"
	exclude-result-prefixes="tei edate xd txm" version="2.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" indent="no" />

	<xd:doc type="stylesheet">
		<xd:short>
			Cette feuille permet de convertir la transcription compacte de la
			Queste del saint Graal au format XML-TEI du projet Oriflamms.
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2014, CNRS / ICAR (Équipe CACTUS)</xd:copyright>
	</xd:doc>

	<xsl:template match="/">
		<xsl:apply-templates />
	</xsl:template>


	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates
				select="*|processing-instruction()|comment()|text()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="comment()|text()">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="processing-instruction()"></xsl:template>

	<!-- On supprime les attributs par défaut de la DTD TEI -->

	<xsl:template match="@*">
		<xsl:choose>
			<xsl:when
				test="matches(name(.),'^(part|instant|anchored|full)$')" />
			<xsl:when test="matches(name(.),'rend')">
				<xsl:choose>
					<xsl:when test="matches(.,'^aggl$')">
						<xsl:attribute name="rend">space-after(none)</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:teiHeader">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:revisionDesc">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<change xmlns="http://www.tei-c.org/ns/1.0"
				when="{format-date(current-date(),'[Y]-[M01]-[D01]')}" who="#auto">Conversion
				automatique au format XML-TEI-Oriflamms</change>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:encodingDesc">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
			<xsl:if test="not(//tei:prefixDef[@ident='ori'])">
				<listPrefixDef xmlns="http://www.tei-c.org/ns/1.0">
					<prefixDef xmlns="http://www.tei-c.org/ns/1.0"
						ident="ori" matchPattern="([a-z]+)"
						replacementPattern="oriflamms-annotation-scheme.xml#$1">
						<p>
							In the context of this project, private URIs with the prefix
							"ori" point to
							<gi>interp</gi>
							elements in the project's
							oriflamms-annotation-scheme.xml file.
						</p>
					</prefixDef>
				</listPrefixDef>
			</xsl:if>
		</xsl:copy>
	</xsl:template>


	<xsl:template match="tei:cb[@rend='hidden']">
		<xsl:comment>
			cb factice supprimé
		</xsl:comment>
	</xsl:template>

	<xsl:template
		match="tei:note|tei:supplied[not(descendant::tei:lb)]">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="ana">
      <xsl:if test="@ana"><xsl:value-of
				select="concat(@ana,' ')" /></xsl:if>
      <xsl:text>ori:align-no</xsl:text>
    </xsl:attribute>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:supplied[descendant::tei:lb]">
		<xsl:comment>
			Ajout éditorial supprimé.
		</xsl:comment>
	</xsl:template>

	<!--<xsl:template match="tei:pb[not(preceding-sibling::*[1][self::tei:milestone[@unit='surface']] 
		or ancestor::tei:supplied)]"> <milestone xmlns="http://www.tei-c.org/ns/1.0" 
		unit="surface" facs="{@facs}"/> <xsl:copy> <xsl:apply-templates select="@*"/> 
		</xsl:copy> </xsl:template> -->

	<xsl:template match="tei:w[matches(@type,'^PON')]">
		<pc xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates select="@*" />
			<xsl:analyze-string select="."
				regex="^(.*)%(.*)%(.*)$">
				<xsl:matching-substring>
					<xsl:choose>
						<xsl:when test="matches(regex-group(3),'^$')">
							<xsl:attribute name="ana">ori:align-no</xsl:attribute>
							<reg xmlns="http://www.tei-c.org/ns/1.0" ana="ori:align-no">
								<xsl:value-of select="regex-group(1)" />
							</reg>
						</xsl:when>
						<xsl:when test="matches(regex-group(1),'^$')">
							<orig xmlns="http://www.tei-c.org/ns/1.0">
								<xsl:value-of select="regex-group(3)" />
							</orig>
						</xsl:when>
						<xsl:otherwise>
							<choice xmlns="http://www.tei-c.org/ns/1.0">
								<reg xmlns="http://www.tei-c.org/ns/1.0" ana="ori:align-no">
									<xsl:value-of select="regex-group(1)" />
								</reg>
								<orig xmlns="http://www.tei-c.org/ns/1.0">
									<xsl:value-of select="regex-group(3)" />
								</orig>
							</choice>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:matching-substring>
				<xsl:non-matching-substring>
					<xsl:value-of select="." />
				</xsl:non-matching-substring>
			</xsl:analyze-string>
		</pc>
	</xsl:template>

	<xsl:template match="tei:orig">
		<xsl:apply-templates />
	</xsl:template>



	<xsl:template
		match="tei:w[not(matches(@type,'^PON'))]//text()|tei:del[not(ancestor::tei:w or descendant::tei:w)]//text()">
		<xsl:choose>
			<xsl:when test="matches(.,'^\s*$')">
				<xsl:copy />
			</xsl:when>
			<xsl:otherwise>
				<xsl:analyze-string select="."
					regex="#?\*[ijuvIJUVí]|#[A-Za-zÀ-ÿſ]|\{{\{{[^}}]+\}}\}}|#?\(\([^)]+\)\).?|_">
					<xsl:matching-substring>
						<xsl:choose>
							<xsl:when test="matches(.,'\(\(')">
								<xsl:analyze-string select="."
									regex="^#?\(\(([^\)]+)\)\)(.?)$">
									<xsl:matching-substring>
										<xsl:variable name="capitalize">
											<xsl:choose>
												<xsl:when test="matches(.,'^#') or matches(.,'_#')">
													yes
												</xsl:when>
												<xsl:otherwise>
													no
												</xsl:otherwise>
											</xsl:choose>
										</xsl:variable>
										<xsl:variable name="nextLetter">
											<xsl:value-of select="regex-group(2)" />
										</xsl:variable>
										<xsl:variable name="abbrexpan">
											<xsl:value-of
												select="replace(regex-group(1),'#','')" />
											<!--<xsl:value-of select="regex-group(1)"/> -->
										</xsl:variable>

										<xsl:variable name="abbr">
											<xsl:choose>
												<xsl:when test="matches($abbrexpan,'_')">
													<xsl:value-of
														select="substring-before($abbrexpan,'_')" />
												</xsl:when>
												<xsl:when
													test="matches($abbrexpan,'^#?(.&#x0305;|.&#x0363;|.&#x0364;|.&#x0365;|bn&#x0305;|ch&#x02BC;&#xA75B;|ch&#x0305;&#xA75B;|ch&#xA75B;&#x0305;|gn&#x0363;t|qn&#x0363;t|&#x035B;|&#x1DD1;|&#x204A;|&#x223B;|&#xA76F;|&#xA770;|&#xF153;|&#xF158;|&#xF1C8;)$')">
													<xsl:value-of select="$abbrexpan" />
												</xsl:when>
												<xsl:otherwise>
													xxx
												</xsl:otherwise>
											</xsl:choose>
										</xsl:variable>
										<xsl:variable name="expannocap" as="element()">
											<expan xmlns="http://www.tei-c.org/ns/1.0">
												<xsl:choose>
													<xsl:when test="matches($abbrexpan,'_')">
														<xsl:analyze-string
															select="substring-after($abbrexpan,'_')"
															regex="\[([^\]]+)\]">
															<xsl:matching-substring>
																<ex xmlns="http://www.tei-c.org/ns/1.0">
																	<xsl:value-of select="regex-group(1)" />
																</ex>
															</xsl:matching-substring>
															<xsl:non-matching-substring>
																<xsl:copy />
															</xsl:non-matching-substring>
														</xsl:analyze-string>
													</xsl:when>
													<xsl:when
														test="matches($abbrexpan,'^bn&#x0305;$')">
														b
														<ex xmlns="http://www.tei-c.org/ns/1.0">ie</ex>
														n
													</xsl:when>
													<xsl:when
														test="matches($abbrexpan,'^(ch&#x02BC;&#xA75B;|ch&#x0305;&#xA75B;|ch&#xA75B;&#x0305;)$')">
														ch
														<ex xmlns="http://www.tei-c.org/ns/1.0">evalie</ex>
														r
													</xsl:when>
													<xsl:when test="matches($abbrexpan,'gn&#x0363;t')">
														g
														<ex xmlns="http://www.tei-c.org/ns/1.0">r</ex>
														<c xmlns="http://www.tei-c.org/ns/1.0"
															rend="position(combsup)">a</c>
														nt
													</xsl:when>
													<xsl:when test="matches($abbrexpan,'qn&#x0363;t')">
														q
														<ex xmlns="http://www.tei-c.org/ns/1.0">u</ex>
														<c xmlns="http://www.tei-c.org/ns/1.0"
															rend="position(combsup)">a</c>
														nt
													</xsl:when>
													<xsl:when test="matches($abbrexpan,'.&#x0305;$')">
														<xsl:value-of
															select="substring($abbrexpan,1,1)" />
														<xsl:choose>
															<xsl:when
																test="matches($abbrexpan,'^[aeiou]') and matches($nextLetter,'[bmnp]')">
																<ex xmlns="http://www.tei-c.org/ns/1.0">m</ex>
															</xsl:when>
															<xsl:when test="matches($abbrexpan,'^[aeiou]')">
																<ex xmlns="http://www.tei-c.org/ns/1.0">n</ex>
															</xsl:when>
															<xsl:when test="matches($abbrexpan,'^[q]')">
																<ex xmlns="http://www.tei-c.org/ns/1.0">ue</ex>
															</xsl:when>
															<xsl:when test="matches($abbrexpan,'^[p]')">
																<ex xmlns="http://www.tei-c.org/ns/1.0">re</ex>
															</xsl:when>
															<xsl:otherwise>
																‹bar›
															</xsl:otherwise>
														</xsl:choose>
													</xsl:when>
													<xsl:when test="matches($abbrexpan,'.&#x0363;')">
														<xsl:value-of
															select="substring($abbrexpan,1,1)" />
														<xsl:choose>
															<xsl:when test="matches($abbrexpan,'^[Qq]')">
																<ex xmlns="http://www.tei-c.org/ns/1.0">u</ex>
															</xsl:when>
															<xsl:otherwise>
																<ex xmlns="http://www.tei-c.org/ns/1.0">r</ex>
															</xsl:otherwise>
														</xsl:choose>
														<c xmlns="http://www.tei-c.org/ns/1.0"
															rend="position(combsup)">a</c>
													</xsl:when>
													<xsl:when test="matches($abbrexpan,'.&#x0364;')">
														<xsl:value-of
															select="substring($abbrexpan,1,1)" />
														<xsl:choose>
															<xsl:when test="matches($abbrexpan,'^[Qq]')">
																<ex xmlns="http://www.tei-c.org/ns/1.0">u</ex>
															</xsl:when>
															<xsl:otherwise>
																<ex xmlns="http://www.tei-c.org/ns/1.0">r</ex>
															</xsl:otherwise>
														</xsl:choose>
														<c xmlns="http://www.tei-c.org/ns/1.0"
															rend="position(combsup)">e</c>
													</xsl:when>
													<xsl:when test="matches($abbrexpan,'.&#x0365;')">
														<xsl:value-of
															select="substring($abbrexpan,1,1)" />
														<xsl:choose>
															<xsl:when test="matches($abbrexpan,'^[Qq]')">
																<ex xmlns="http://www.tei-c.org/ns/1.0">u</ex>
															</xsl:when>
															<xsl:otherwise>
																<ex xmlns="http://www.tei-c.org/ns/1.0">r</ex>
															</xsl:otherwise>
														</xsl:choose>
														<c xmlns="http://www.tei-c.org/ns/1.0"
															rend="position(combsup)">i</c>
													</xsl:when>
													<xsl:when
														test="matches($abbrexpan,'&#xA76F;') and matches($nextLetter,'[bmnp]')">
														<ex xmlns="http://www.tei-c.org/ns/1.0">com</ex>
													</xsl:when>
													<xsl:when test="matches($abbrexpan,'&#xA76F;')">
														<ex xmlns="http://www.tei-c.org/ns/1.0">con</ex>
													</xsl:when>
													<xsl:when
														test="matches($abbrexpan,'&#x035B;|&#xF1C8;')">
														<ex xmlns="http://www.tei-c.org/ns/1.0">er</ex>
													</xsl:when>
													<xsl:when
														test="matches($abbrexpan,'&#x204A;|&#xF158;')">
														<ex xmlns="http://www.tei-c.org/ns/1.0">et</ex>
													</xsl:when>
													<xsl:when
														test="matches($abbrexpan,'&#x1DD1;|&#xF153;')">
														<ex xmlns="http://www.tei-c.org/ns/1.0">ur</ex>
													</xsl:when>
													<xsl:when test="matches($abbrexpan,'&#xA770;')">
														<ex xmlns="http://www.tei-c.org/ns/1.0">us</ex>
													</xsl:when>
													<xsl:otherwise>
														<ex xmlns="http://www.tei-c.org/ns/1.0">
															<xsl:value-of select="$abbrexpan" />
														</ex><!--<xsl:comment>Unknown expan!</xsl:comment> -->
													</xsl:otherwise>
												</xsl:choose>
											</expan>
										</xsl:variable>
										<xsl:variable name="expan">
											<xsl:choose>
												<xsl:when test="$capitalize eq 'yes'">
													<expan xmlns="http://www.tei-c.org/ns/1.0">
														<xsl:for-each select="$expannocap/node()">
															<xsl:choose>
																<xsl:when
																	test="self::text()[not(preceding-sibling::*)]">
																	<g xmlns="http://www.tei-c.org/ns/1.0"
																		ana="ori:dipl-small">
																		<xsl:value-of
																			select="upper-case(substring(.,1,1))" />
																	</g>
																	<xsl:value-of select="substring(.,2)" />
																</xsl:when>
																<xsl:when
																	test="self::*[not(preceding-sibling::text())]">
																	<xsl:copy>
																		<g xmlns="http://www.tei-c.org/ns/1.0"
																			ana="ori:dipl-small">
																			<xsl:value-of
																				select="upper-case(substring(.,1,1))" />
																		</g>
																		<xsl:value-of select="substring(.,2)" />
																	</xsl:copy>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:copy-of select="." />
																</xsl:otherwise>
															</xsl:choose>
														</xsl:for-each>
													</expan>
												</xsl:when>
												<xsl:otherwise>
													<xsl:copy-of select="$expannocap" />
												</xsl:otherwise>
											</xsl:choose>
										</xsl:variable>
										<xsl:choose>
											<xsl:when test="matches($abbr,'.+')">
												<choice xmlns="http://www.tei-c.org/ns/1.0">
													<abbr xmlns="http://www.tei-c.org/ns/1.0">
														<xsl:copy-of select="$abbr" />
													</abbr>
													<xsl:copy-of select="$expan" />
												</choice>
											</xsl:when>
											<xsl:otherwise>
												<xsl:copy-of select="$expan" /><!--<xsl:comment>Unknown 
													abbr!</xsl:comment> -->
											</xsl:otherwise>
										</xsl:choose>
										<xsl:value-of select="$nextLetter" />
									</xsl:matching-substring>
									<xsl:non-matching-substring>
										<xsl:copy-of select="." /><!--<xsl:comment>Unknown 
											abbr-expan!</xsl:comment> -->
									</xsl:non-matching-substring>
								</xsl:analyze-string>
							</xsl:when>
							<xsl:when test="matches(.,'#\*([ij])')">
								<g xmlns="http://www.tei-c.org/ns/1.0" ana="ori:dipl-j">
									<xsl:value-of select="upper-case(regex-group(1))" />
								</g>
							</xsl:when>
							<xsl:when test="matches(.,'#\*([uv])')">
								<g xmlns="http://www.tei-c.org/ns/1.0" ana="ori:dipl-v">
									<xsl:value-of select="upper-case(regex-group(1))" />
								</g>
							</xsl:when>
							<xsl:when test="matches(.,'\*í')">
								<g xmlns="http://www.tei-c.org/ns/1.0"
									ana="ori:dipl-i ori:facs-iacute">j</g>
							</xsl:when>
							<xsl:when test="matches(.,'\*([ij])')">
								<g xmlns="http://www.tei-c.org/ns/1.0" ana="ori:dipl-j">
									<xsl:value-of select="regex-group(1)" />
								</g>
							</xsl:when>
							<xsl:when test="matches(.,'\*([uv])')">
								<g xmlns="http://www.tei-c.org/ns/1.0" ana="ori:dipl-v">
									<xsl:value-of select="regex-group(1)" />
								</g>
							</xsl:when>
							<xsl:when test="matches(.,'#\*([IJ])')">
								<g xmlns="http://www.tei-c.org/ns/1.0" ana="ori:dipl-I">
									<xsl:value-of select="lower-case(regex-group(1))" />
								</g>
							</xsl:when>
							<xsl:when test="matches(.,'#\*([UV])')">
								<g xmlns="http://www.tei-c.org/ns/1.0" ana="ori:dipl-V">
									<xsl:value-of select="lower-case(regex-group(1))" />
								</g>
							</xsl:when>
							<xsl:when test="matches(.,'\*([IJ])')">
								<g xmlns="http://www.tei-c.org/ns/1.0" ana="ori:dipl-I">
									<xsl:value-of select="regex-group(1)" />
								</g>
							</xsl:when>
							<xsl:when test="matches(.,'\*([UV])')">
								<g xmlns="http://www.tei-c.org/ns/1.0" ana="ori:dipl-V">
									<xsl:value-of select="regex-group(1)" />
								</g>
							</xsl:when>
							<xsl:when test="matches(.,'#ſ')">
								<xsl:analyze-string select="." regex="^#(.)">
									<xsl:matching-substring>
										<g xmlns="http://www.tei-c.org/ns/1.0"
											ana="ori:dipl-caps ori:facs-slong">S</g>
									</xsl:matching-substring>
								</xsl:analyze-string>
							</xsl:when>
							<!--  -->
							<!-- <xsl:when test="matches(.,'#')"> <xsl:analyze-string select="." 
								regex="^#(.)"> <xsl:matching-substring> <choice xmlns="http://www.tei-c.org/ns/1.0"> 
								<abbr xmlns="http://www.tei-c.org/ns/1.0"><xsl:value-of select="regex-group(1)"/></abbr> 
								<expan xmlns="http://www.tei-c.org/ns/1.0"><ex xmlns="http://www.tei-c.org/ns/1.0"><g 
								xmlns="http://www.tei-c.org/ns/1.0" ana="ori:dipl-small">E</g>t</ex></expan> 
								</choice> </xsl:matching-substring> </xsl:analyze-string> </xsl:when> -->
							<xsl:when test="matches(.,'#[A-Z]')">
								<xsl:analyze-string select="." regex="^#(.)">
									<xsl:matching-substring>
										<g xmlns="http://www.tei-c.org/ns/1.0" ana="ori:dipl-caps">
											<xsl:value-of
												select="lower-case(regex-group(1))" />
										</g>
									</xsl:matching-substring>
								</xsl:analyze-string>
							</xsl:when>
							<xsl:when test="matches(.,'#[^(]')">
								<xsl:analyze-string select="."
									regex="^#(.+)$">
									<xsl:matching-substring>
										<g xmlns="http://www.tei-c.org/ns/1.0" ana="ori:dipl-small">
											<xsl:value-of
												select="upper-case(regex-group(1))" />
										</g>
									</xsl:matching-substring>
								</xsl:analyze-string>
							</xsl:when>
							<xsl:when test="matches(.,'\{\{([^}}]+)\}\}')">
								<xsl:analyze-string select="."
									regex="^\{{\{{([^:]*):([^:]*):([^:]*):([^:]*):?([^:]*)\}}\}}$">
									<xsl:matching-substring>
										<xsl:variable name="lettrine">
											<xsl:value-of select="regex-group(1)" />
										</xsl:variable>
										<xsl:variable name="size">
											<xsl:value-of select="regex-group(2)" />
										</xsl:variable>
										<xsl:variable name="sizeAct">
											<xsl:value-of select="regex-group(3)" />
										</xsl:variable>
										<xsl:variable name="color">
											<xsl:value-of select="regex-group(4)" />
										</xsl:variable>
										<xsl:variable name="deco">
											<xsl:value-of select="regex-group(5)" />
										</xsl:variable>
										<xsl:variable name="rend">
											<xsl:value-of select="concat('size(',$size,')')" />
											<xsl:if test="matches($sizeAct,'.+')">
												<xsl:value-of
													select="concat(' sizeAct(',$sizeAct,')')" />
											</xsl:if>
											<xsl:if test="matches($color,'.+')">
												<xsl:value-of
													select="concat(' color(',$color,')')" />
											</xsl:if>
											<xsl:if test="matches($deco,'.+')">
												<xsl:value-of
													select="concat(' deco(',$deco,')')" />
											</xsl:if>
										</xsl:variable>
										<g xmlns="http://www.tei-c.org/ns/1.0" type="lettrine"
											rend="{$rend}">
											<xsl:value-of select="$lettrine" />
										</g>
									</xsl:matching-substring>
								</xsl:analyze-string>
							</xsl:when>

							<xsl:when test="matches(.,'__')">
								<space xmlns="http://www.tei-c.org/ns/1.0"
									ana="ori:figement" />
							</xsl:when>
							<xsl:when test="matches(.,'_')">
								<space xmlns="http://www.tei-c.org/ns/1.0" />
							</xsl:when>
							<xsl:otherwise>
								Match:
								<xsl:copy />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:matching-substring>
					<xsl:non-matching-substring>
						<xsl:copy />
					</xsl:non-matching-substring>
				</xsl:analyze-string>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>



</xsl:stylesheet>
