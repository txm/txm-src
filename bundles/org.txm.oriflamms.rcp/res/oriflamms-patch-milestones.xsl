<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:txm="http://textometrie.org/1.0"
	xmlns:xi="http://www.w3.org/2001/XInclude"
	xmlns:svg="http://www.w3.org/2000/svg"
	xmlns:math="http://www.w3.org/1998/Math/MathML"
	exclude-result-prefixes="tei edate xd txm xi svg math" version="2.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" indent="no" />

	<xd:doc type="stylesheet">
		<xd:short>
			Cette feuille permet de rajouter les éléments milestone et cb, et les
			identifiants
			manquant sur lb
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2015, CNRS / ICAR (Équipe CACTUS)</xd:copyright>
	</xd:doc>

	<!--<xsl:strip-space elements="*"/> -->

	<xsl:variable name="textID">
		<xsl:analyze-string select="document-uri(.)"
			regex="^(.*)/([^/]+)\.[^/]+$">
			<xsl:matching-substring>
				<xsl:value-of select="regex-group(2)" />
			</xsl:matching-substring>
		</xsl:analyze-string>
	</xsl:variable>

	<xsl:template match="/">
		<xsl:apply-templates />
	</xsl:template>


	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates
				select="*|processing-instruction()|comment()|text()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="processing-instruction()"></xsl:template>

	<xsl:template match="comment()|text()">
		<xsl:copy />
	</xsl:template>

	<!-- On supprime les attributs par défaut de la DTD TEI -->

	<xsl:template match="@*">
		<xsl:choose>
			<xsl:when
				test="matches(name(.),'^(part|instant|anchored|full)$')" />
			<!-- <xsl:when test="matches(name(.),'rend')"> <xsl:choose> <xsl:when 
				test="matches(.,'^aggl$')"><xsl:attribute name="rend">space-after(none)</xsl:attribute></xsl:when> 
				<xsl:otherwise><xsl:copy/></xsl:otherwise> </xsl:choose> </xsl:when> -->
			<xsl:otherwise>
				<xsl:copy />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:pb[not(ancestor::tei:supplied)]">
		<xsl:variable name="facs">
			<xsl:value-of select="@facs" />
		</xsl:variable>
		<xsl:if
			test="matches(@facs,'\.(tiff?|png|jpg)$','i') and not(preceding-sibling::*[position() lt 3][self::tei:milestone[@unit='surface']]) and not(preceding::tei:pb[@facs=$facs])">
			<milestone xmlns="http://www.tei-c.org/ns/1.0"
				unit="surface" facs="{@facs}" />
		</xsl:if>
		<xsl:copy>
			<xsl:apply-templates select="@*" />
		</xsl:copy>
		<xsl:if test="not(following::*[position() lt 5][self::tei:cb])">
			<xsl:comment>
				cb added automatically
			</xsl:comment>
			<cb xmlns="http://www.tei-c.org/ns/1.0" />
		</xsl:if>
		<xsl:if
			test="not(following::*[position() lt 5][self::tei:lb or self::tei:cb])">
			<lb xmlns="http://www.tei-c.org/ns/1.0" />
			<xsl:comment>
				lb added automatically
			</xsl:comment>
		</xsl:if>
	</xsl:template>

	<xsl:template match="tei:cb[not(ancestor::tei:supplied)]">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
		</xsl:copy>
		<xsl:if test="not(following::*[position() lt 5][self::tei:lb])">
			<lb xmlns="http://www.tei-c.org/ns/1.0" />
		</xsl:if>
	</xsl:template>

	<xsl:template match="tei:lb">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:if test="matches(@rend,'^(right|center|centre)#')">
				<xsl:attribute name="type">rejet</xsl:attribute>
			</xsl:if>
		</xsl:copy>
	</xsl:template>

	<xsl:template
		match="tei:milestone[@unit='surface' and @facs=preceding::tei:milestone[@unit='surface']/@facs]">
		<xsl:comment>
			Doublon éliminé
			<xsl:value-of select="@xml:id" />
		</xsl:comment>
	</xsl:template>

</xsl:stylesheet>