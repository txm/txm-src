<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	exclude-result-prefixes="tei edate" version="2.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />

	<xsl:template match="node()|@*">
		<!-- Copy the current node -->
		<xsl:copy>
			<!-- Including any attributes it has and any child nodes -->
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template name="wp1">
		<xsl:for-each
			select="child::node()[not(self::tei:pb or self::tei:cb or self::tei:milestone) and following-sibling::*[descendant-or-self::tei:lb]]">
			<xsl:apply-templates select="." />
		</xsl:for-each>
		<xsl:for-each select="child::*[descendant::tei:lb]">
			<xsl:copy>
				<xsl:apply-templates select="@*" />
				<xsl:call-template name="wp1" />
			</xsl:copy>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="wp2">
		<xsl:for-each select="child::*[descendant::tei:lb]">
			<xsl:copy>
				<xsl:apply-templates select="@*" />
				<xsl:call-template name="wp2" />
			</xsl:copy>
		</xsl:for-each>
		<xsl:for-each
			select="child::node()[not(self::tei:pb or self::tei:cb or self::tei:milestone) and preceding-sibling::*[descendant-or-self::tei:lb]]">
			<xsl:apply-templates select="." />
		</xsl:for-each>
	</xsl:template>

	<xsl:template
		match="tei:w[not(ancestor::tei:w or ancestor::tei:pc)]">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:choose>
				<xsl:when
					test="descendant::tei:lb and not(descendant::tei:seg[@type='wp'])">
					<seg xmlns="http://www.tei-c.org/ns/1.0" type="wp" part="I">
						<xsl:call-template name="wp1" />
					</seg>
					<xsl:apply-templates
						select="descendant::tei:milestone|descendant::tei:pb|descendant::tei:cb|descendant::tei:lb" />
					<seg xmlns="http://www.tei-c.org/ns/1.0" type="wp" part="F">
						<xsl:call-template name="wp2" />
					</seg>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<!-- on supprime les w et pc imbriqués -->
	<xsl:template
		match="tei:w[ancestor::tei:w or ancestor::tei:pc]|tei:pc[ancestor::tei:w or ancestor::tei:pc]">
		<xsl:apply-templates />
	</xsl:template>

</xsl:stylesheet>
