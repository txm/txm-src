<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:txm="http://textometrie.org/1.0"
	exclude-result-prefixes="tei edate xd txm" version="2.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" indent="no" />

	<xd:doc type="stylesheet">
		<xd:short>
			Cette feuille permet de tokéniser un document TEI-Oriflamms au
			niveau de caractère.
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2015, CNRS / ICAR (Équipe CACTUS)</xd:copyright>
	</xd:doc>

	<!--<xsl:param name="spacing-am">&#xA76F;|&#x204A;|&#xF158;|&#x223B;|&#x035B;|&#xF1C8;|&#x1DD1;|&#xF153;|&#xA770;</xsl:param> -->
	<!-- on utilise la classe unicode 'combibning mark' \p{M} pour repérer les 
		diacritiques -->


	<!-- <xsl:variable name="filename"> <xsl:analyze-string select="document-uri(.)" 
		regex="^(.*)/([^/]+)\.[^/]+$"> <xsl:matching-substring> <xsl:value-of select="regex-group(2)"/> 
		</xsl:matching-substring> </xsl:analyze-string> </xsl:variable> <xsl:variable 
		name="filedir"> <xsl:analyze-string select="document-uri(.)" regex="^(.*)/([^/]+)\.[^/]+$"> 
		<xsl:matching-substring> <xsl:value-of select="regex-group(1)"/> </xsl:matching-substring> 
		</xsl:analyze-string> </xsl:variable> <xsl:variable name="filenameresult"> 
		<xsl:value-of select="replace($filename,'(-ori)?-w$','')"/> </xsl:variable> 
		<xsl:variable name="path"> <xsl:value-of select="concat($filedir,'/',$filenameresult,'-w/?select=*.xml;recurse=yes;on-error=warning')"/> 
		</xsl:variable> <xsl:variable name="files" select="collection($path)"/> -->

	<!-- <xsl:template match="/"> <root>Processing files from <xsl:value-of 
		select="$filedir/$filenameresult"/>-w directory. Result files saved in <xsl:value-of 
		select="$filedir/$filenameresult"/>-c-temp directory.</root> <xsl:for-each 
		select="$files"> <xsl:variable name="filename2"> <xsl:analyze-string select="document-uri(.)" 
		regex="^(.*)/([^/]+)$"> <xsl:matching-substring> <xsl:value-of select="regex-group(2)"></xsl:value-of> 
		</xsl:matching-substring> </xsl:analyze-string></xsl:variable> <xsl:result-document 
		href="{$filedir}/{$filenameresult}-c-temp/{$filename2}"> <xsl:apply-templates/> 
		</xsl:result-document> </xsl:for-each> </xsl:template> -->

	<xsl:param name="exclude-no-alignable">
		no
	</xsl:param>


	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates
				select="*|processing-instruction()|comment()|text()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template
		match="@*|comment()|processing-instruction()|text()">
		<xsl:copy />
	</xsl:template>

	<xsl:param name="editorialDeclPatch" as="element()">
		<p xmlns="http://www.tei-c.org/ns/1.0">Fichier tokénisé au niveau des caractères dans le cadre du
			projet ANR Oriflamms (http://oriflamms.hypotheses.org) par le
			logiciel TXM (http://textometrie.ens-lyon.fr)</p>
	</xsl:param>

	<!-- Ob ajoute les infos sur le traitement du fichier dans teiHeader -->

	<xsl:template match="/*/tei:teiHeader">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:choose>
				<xsl:when test="child::tei:encodingDesc">
					<xsl:apply-templates />
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates
						select="tei:fileDesc|tei:profileDesc" />
					<encodingDesc xmlns="http://www.tei-c.org/ns/1.0">
						<editorialDecl xmlns="http://www.tei-c.org/ns/1.0">
							<xsl:copy-of select="$editorialDeclPatch"></xsl:copy-of>
						</editorialDecl>
					</encodingDesc>
					<xsl:choose>
						<xsl:when test="not(tei:revisionDesc)">
							<revisionDesc xmlns="http://www.tei-c.org/ns/1.0">
								<xsl:call-template name="change-txm"></xsl:call-template>
							</revisionDesc>
						</xsl:when>
						<xsl:otherwise>
							<xsl:apply-templates
								select="tei:revisionDesc" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template
		match="/*/tei:teiHeader/tei:fileDesc/tei:titleStmt">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
			<xsl:if test="not(tei:respStmt/tei:name[@xml:id='TXM'])">
				<respStmt xmlns="http://www.tei-c.org/ns/1.0">
					<resp xmlns="http://www.tei-c.org/ns/1.0">Tokénisation des mots et/ou des caractères</resp>
					<name xmlns="http://www.tei-c.org/ns/1.0" xml:id="TXM">Logiciel TXM
						(http://textometrie.ens-lyon.fr)</name>
				</respStmt>
			</xsl:if>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="/*/tei:teiHeader/tei:encodingDesc">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:choose>
				<xsl:when test="child::tei:editorialDecl">
					<xsl:apply-templates />
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates />
					<editorialDecl xmlns="http://www.tei-c.org/ns/1.0">
						<xsl:copy-of select="$editorialDeclPatch"></xsl:copy-of>
					</editorialDecl>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:editorialDecl">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
			<xsl:copy-of select="$editorialDeclPatch"></xsl:copy-of>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="/*/tei:teiHeader//tei:revisionDesc">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
			<xsl:call-template name="change-txm" />
		</xsl:copy>
	</xsl:template>

	<xsl:template name="change-txm">
		<change when="{format-date(current-date(),'[Y]-[M01]-[D01]')}"
			who="#TXM" xmlns="http://www.tei-c.org/ns/1.0">Tokenisation des caractères</change>
	</xsl:template>



	<xsl:template match="tei:supplied">
		<gap reason="supplied" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:comment>
				<xsl:copy-of select="."></xsl:copy-of>
			</xsl:comment>
		</gap>
	</xsl:template>

	<xsl:template
		match="tei:w|tei:pc|tei:seg[@type='wp']|tei:seg[@type='deleted']">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:for-each select="child::node()">
				<xsl:choose>
					<xsl:when
						test="self::tei:choice and $exclude-no-alignable='yes'">
						<xsl:apply-templates
							select="tei:abbr|tei:orig|tei:sic" />
					</xsl:when>
					<xsl:when test="self::text()">
						<xsl:if
							test="matches(.,'\S') and not(ancestor::*[@ana='ori:align-no'])">
							<xsl:call-template name="tokenise-chars" />
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="." />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:orig|tei:sic">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:choose>
				<xsl:when test="ancestor::tei:w|ancestor::tei:pc">
					<xsl:for-each select="child::node()">
						<xsl:choose>
							<xsl:when
								test="self::text() and not(ancestor::*[@ana='ori:align-no'])">
								<xsl:if test="matches(.,'\S')">
									<xsl:call-template name="tokenise-chars" />
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<xsl:apply-templates select="." />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>


	<xsl:template match="tei:abbr">
		<xsl:choose>
			<xsl:when test="matches(.,'\P{M}')">
				<xsl:copy>
					<xsl:apply-templates select="@*" />
					<xsl:choose>
						<xsl:when test="ancestor::tei:w">
							<xsl:for-each select="child::node()">
								<xsl:choose>
									<xsl:when test="self::text()">
										<xsl:if
											test="matches(.,'\S') and not(ancestor::*[@ana='ori:align-no'])">
											<xsl:call-template name="tokenise-chars" />
										</xsl:if>
									</xsl:when>
									<xsl:otherwise>
										<xsl:apply-templates select="." />
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							<xsl:apply-templates />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:copy>
			</xsl:when>
			<xsl:otherwise><!-- on supprime les abréviations consistant uniquement 
					de caractères modifieurs -->
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<xsl:template match="tei:g">
		<xsl:choose>
			<xsl:when test="ancestor::*[@ana='ori:align-no']">
				<xsl:copy-of select="." />
			</xsl:when>
			<xsl:otherwise>
				<c xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:if test="matches(@type,'initiale?|lettrine')">
						<xsl:attribute name="type">initiale</xsl:attribute>
					</xsl:if>
					<xsl:copy-of select="." />
				</c>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:c">
		<xsl:choose>
			<xsl:when
				test="ancestor::*[@ana='ori:align-no'] or ancestor::tei:expan[parent::tei:choice]">
				<xsl:copy-of select="child::node()" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="."></xsl:copy-of>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:am">
		<xsl:choose>
			<xsl:when test="matches(.,'\P{M}')">
				<!-- \p{M} est la classe unicode de caractères combinants => \P{M} = 
					tout sauf combinant -->
				<c xmlns="http://www.tei-c.org/ns/1.0" type="am">
					<xsl:apply-templates select="@*" />
					<xsl:apply-templates />
				</c>
			</xsl:when>
		</xsl:choose>
	</xsl:template>


	<xsl:template match="*[@ana='ori:align-no']">
		<xsl:choose>
			<xsl:when test="$exclude-no-alignable='yes'">
				<xsl:comment>
					<xsl:value-of select="." />
				</xsl:comment>
			</xsl:when>
			<xsl:otherwise>
				<!--<xsl:copy-of select="."/> -->
				<xsl:copy>
					<xsl:apply-templates select="@*|node()" />
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template
		match="tei:ex[not(ancestor-or-self::*[@ana='ori:align-no']) and not(parent::tei:expan/parent::tei:choice/tei:abbr)]">
		<xsl:choose>
			<xsl:when test="$exclude-no-alignable='yes'">
				<xsl:choose>
					<xsl:when
						test="matches(.,'^(et|cum|co[mn]|est|ur|us)$','i')">
						<c xmlns="http://www.tei-c.org/ns/1.0">
							<g xmlns="http://www.tei-c.org/ns/1.0" type="am">
								<xsl:call-template name="abbreviate" />
							</g>
						</c>
					</xsl:when>
					<xsl:otherwise>
						<xsl:comment>
							<xsl:copy-of select="." />
						</xsl:comment>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<choice xmlns="http://www.tei-c.org/ns/1.0">
					<abbr xmlns="http://www.tei-c.org/ns/1.0">
						<xsl:choose>
							<xsl:when
								test="matches(.,'^(et|cum|co[mn]|est|ur|us)$','i')">
								<c xmlns="http://www.tei-c.org/ns/1.0">
									<g xmlns="http://www.tei-c.org/ns/1.0" type="am">
										<xsl:call-template name="abbreviate" />
									</g>
								</c>
							</xsl:when>
							<xsl:otherwise />
						</xsl:choose>
					</abbr>
					<expan xmlns="http://www.tei-c.org/ns/1.0">
						<xsl:copy-of select="." />
					</expan>
				</choice>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="abbreviate">
		<xsl:choose>
			<xsl:when test="matches(.,'^(cum|com|con)$','i')">
				&#xA76F;
			</xsl:when>
			<xsl:when test="matches(.,'^et$','i')">
				&#x204A;
			</xsl:when>
			<xsl:when test="matches(.,'^est$','i')">
				&#x223B;
			</xsl:when>
			<xsl:when test="matches(.,'^us$','i')">
				&#xA770;
			</xsl:when>
			<xsl:otherwise>
				¤
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!--<xsl:template match="tei:ex[parent::tei:expan/parent::tei:choice/tei:abbr 
		and not(ancestor-or-self::*[@ana='ori:align-no'])]"> <xsl:copy-of select="."/> 
		</xsl:template> -->

	<xsl:template
		match="tei:expan[parent::tei:choice/tei:abbr and not(ancestor-or-self::*[@ana='ori:align-no'])]">
		<xsl:copy-of select="." />
	</xsl:template>


	<xsl:template
		match="tei:w//tei:hi[matches(@rend,'suscrit|enclavé')]">
		<!-- les lettres suscrites et enclavées sont regroupées avec le caractère 
			précédent -->
	</xsl:template>

	<xsl:template
		match="tei:w//tei:hi[matches(@rend,'ligature|fusionnées')]">
		<xsl:choose>
			<xsl:when test="descendant::*">
				<xsl:copy>
					<xsl:apply-templates select="@*" />
					<xsl:for-each select="child::node()">
						<xsl:choose>
							<xsl:when test="self::text()">
								<xsl:if
									test="matches(.,'\S') and not(ancestor::*[@ana='ori:align-no'])">
									<xsl:call-template name="tokenise-chars" />
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<xsl:apply-templates select="." />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</xsl:copy>
			</xsl:when>
			<xsl:otherwise>
				<c xmlns="http://www.tei-c.org/ns/1.0">
					<g xmlns="http://www.tei-c.org/ns/1.0" type="{@rend}">
						<xsl:apply-templates />
					</g>
				</c>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template
		match="tei:w//tei:hi[matches(@rend,'lettre majuscule')]">
		<c xmlns="http://www.tei-c.org/ns/1.0">
			<g xmlns="http://www.tei-c.org/ns/1.0"
				type="{replace(@rend,' ','_')}">
				<xsl:apply-templates />
			</g>
		</c>
	</xsl:template>

	<xsl:template
		match="tei:w//tei:hi[matches(@rend,'initiale?|lettrine')]">
		<xsl:choose>
			<xsl:when
				test="tei:choice/tei:abbr and tei:choice/tei:expan">
				<choice xmlns="http://www.tei-c.org/ns/1.0">
					<abbr xmlns="http://www.tei-c.org/ns/1.0">
						<c type="initiale" xmlns="http://www.tei-c.org/ns/1.0">
							<g xmlns="http://www.tei-c.org/ns/1.0" type="initiale">
								<xsl:value-of select="tei:choice/tei:abbr" />
							</g>
						</c>
					</abbr>
					<xsl:copy-of select="descendant::tei:expan" />
				</choice>
			</xsl:when>
			<xsl:otherwise>
				<c type="initiale" xmlns="http://www.tei-c.org/ns/1.0">
					<g xmlns="http://www.tei-c.org/ns/1.0" type="initiale">
						<xsl:apply-templates />
					</g>
				</c>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template
		match="tei:w//tei:hi[matches(@rend,'lettres allongées|red|sup|highlight')]">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="rend"><xsl:value-of
				select="replace(@rend,' ','_')" /></xsl:attribute>
			<xsl:for-each select="child::node()">
				<xsl:choose>
					<xsl:when test="self::text()">
						<xsl:if
							test="matches(.,'\S') and not(ancestor::*[@ana='ori:align-no'])">
							<xsl:call-template name="tokenise-chars" />
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="." />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:copy>
	</xsl:template>

	<xsl:template
		match="*[ancestor::tei:w and not(ancestor-or-self::*[@ana='ori:align-no'])]"
		priority="-0.1">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:for-each select="child::node()">
				<xsl:choose>
					<xsl:when
						test="self::tei:choice and $exclude-no-alignable='yes'">
						<xsl:apply-templates
							select="tei:abbr|tei:orig|tei:sic" />
					</xsl:when>
					<xsl:when test="self::text()">
						<xsl:if test="matches(.,'\S')">
							<xsl:call-template name="tokenise-chars" />
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="." />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:copy>
	</xsl:template>

	<xsl:template
		match="tei:choice[ancestor::tei:w and matches(tei:abbr,'^\p{M}+$')]"
		priority="1">
		<xsl:comment>
			abréviation déplacée :
			<xsl:copy-of select="." />
		</xsl:comment>
	</xsl:template>

	<xsl:template name="tokenise-chars">

		<xsl:variable name="following-am">
			<xsl:if
				test="following-sibling::node()[1][self::tei:abbr] and not(matches(following-sibling::node()[1][self::tei:abbr],'\P{M}'))">
				<g xmlns="http://www.tei-c.org/ns/1.0" type="am">
					<xsl:value-of select="following-sibling::tei:abbr[1]" />
				</g>
			</xsl:if>
			<xsl:if
				test="following-sibling::node()[1][self::tei:choice[tei:abbr]] and not(matches(following-sibling::node()[1][self::tei:choice]/tei:abbr[1],'\P{M}'))">
				<g xmlns="http://www.tei-c.org/ns/1.0" type="am">
					<xsl:value-of select="following::tei:abbr[1]" />
				</g>
			</xsl:if>
			<xsl:if
				test="following-sibling::node()[1][self::tei:am] and not(matches(following-sibling::node()[1][self::tei:am],'\P{M}'))">
				<g xmlns="http://www.tei-c.org/ns/1.0" type="am">
					<xsl:value-of select="following-sibling::tei:am[1]" />
				</g>
			</xsl:if>
			<xsl:if
				test="following-sibling::node()[1][self::tei:hi[@rend='suscrit']]">
				<g xmlns="http://www.tei-c.org/ns/1.0" type="suscrit">
					<xsl:value-of select="following-sibling::tei:hi[1]" />
				</g>
			</xsl:if>
			<xsl:if
				test="following-sibling::node()[1][self::tei:hi[@rend='enclavé']]">
				<g xmlns="http://www.tei-c.org/ns/1.0" type="enclavé">
					<xsl:value-of select="following-sibling::tei:hi[1]" />
				</g>
			</xsl:if>
		</xsl:variable>

		<xsl:variable name="following-expan">
			<xsl:if
				test="following-sibling::node()[1][self::tei:choice[tei:expan]] and not(matches(following-sibling::node()[1][self::tei:choice]/tei:abbr[1],'\P{M}')) and not($exclude-no-alignable='yes')">
				<expan xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:value-of select="substring(.,string-length(.))" />
					<xsl:apply-templates
						select="following-sibling::tei:choice[1]/tei:expan[1]/node()" />
				</expan>
			</xsl:if>
		</xsl:variable>

		<xsl:choose>
			<xsl:when
				test="ancestor::*[@ana='ori:align-no'] 
        or ancestor::tei:expan[parent::tei:choice]
        or ancestor::tei:corr[parent::tei:choice]
        or ancestor::tei:reg[parent::tei:choice]">
				<xsl:copy-of select="." />
			</xsl:when>
			<xsl:otherwise>
				<xsl:analyze-string select="."
					regex="^(.*)(\P{{M}})(\p{{M}}*)">
					<xsl:matching-substring>
						<xsl:variable name="chars">
							<xsl:value-of
								select="replace(regex-group(1),'[''’]','')" />
						</xsl:variable>
						<xsl:variable name="last-char">
							<xsl:value-of
								select="replace(regex-group(2),'[''’]','')" />
						</xsl:variable>
						<xsl:variable name="last-comb-mark">
							<xsl:value-of select="regex-group(3)" />
						</xsl:variable>
						<xsl:analyze-string select="$chars"
							regex="(\P{{M}})(\p{{M}}*)">
							<xsl:matching-substring>
								<c xmlns="http://www.tei-c.org/ns/1.0">
									<xsl:value-of select="regex-group(1)" />
									<xsl:if test="matches(regex-group(2),'.+')">
										<g xmlns="http://www.tei-c.org/ns/1.0" type="am">
											<xsl:value-of select="regex-group(2)" />
										</g>
									</xsl:if>
								</c>
							</xsl:matching-substring>
						</xsl:analyze-string>
						<xsl:if test="matches($last-char,'.+')">
							<xsl:choose>
								<xsl:when test="matches($following-expan,'.+')">
									<choice xmlns="http://www.tei-c.org/ns/1.0">
										<abbr xmlns="http://www.tei-c.org/ns/1.0">
											<c xmlns="http://www.tei-c.org/ns/1.0">
												<xsl:value-of select="$last-char" />
												<xsl:if test="matches($last-comb-mark,'.+')">
													<g xmlns="http://www.tei-c.org/ns/1.0" type="am">
														<xsl:value-of select="$last-comb-mark" />
													</g>
												</xsl:if>
												<xsl:copy-of select="$following-am" />
											</c>
										</abbr>
										<xsl:copy-of select="$following-expan" />
									</choice>
								</xsl:when>
								<xsl:otherwise>
									<c xmlns="http://www.tei-c.org/ns/1.0">
										<xsl:value-of select="$last-char" />
										<xsl:if test="matches($last-comb-mark,'.+')">
											<g xmlns="http://www.tei-c.org/ns/1.0" type="am">
												<xsl:value-of select="$last-comb-mark" />
											</g>
										</xsl:if>
										<xsl:copy-of select="$following-am" />
									</c>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:if>
					</xsl:matching-substring>
				</xsl:analyze-string>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>


</xsl:stylesheet>