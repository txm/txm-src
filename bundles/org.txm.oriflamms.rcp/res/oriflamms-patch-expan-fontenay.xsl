<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:txm="http://textometrie.org/1.0"
	xmlns:xi="http://www.w3.org/2001/XInclude"
	xmlns:svg="http://www.w3.org/2000/svg"
	xmlns:math="http://www.w3.org/1998/Math/MathML"
	exclude-result-prefixes="tei edate xd txm xi svg math" version="2.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" indent="yes" />

	<xd:doc type="stylesheet">
		<xd:short>
			Cette feuille permet de repérer les lettres supprimées dans les
			résolution des abréviations
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2015, CNRS / ICAR (Équipe CACTUS)</xd:copyright>
	</xd:doc>

	<xsl:strip-space elements="*" />


	<xsl:template match="node()">
		<!-- Copy the current node -->
		<xsl:copy>
			<!-- Including any attributes it has and any child nodes -->
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<!-- On supprime les attributs par défaut de la DTD TEI -->

	<xsl:template match="@*">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="tei:expan">
		<xsl:choose>
			<xsl:when test="matches(.,'^(par|per)$','i')">
				<xsl:analyze-string select="." regex="(.)(..)">
					<xsl:matching-substring>
						<xsl:value-of select="regex-group(1)" />
						<ex xmlns="http://www.tei-c.org/ns/1.0">
							<xsl:value-of select="regex-group(2)"></xsl:value-of>
						</ex>
					</xsl:matching-substring>
				</xsl:analyze-string>
			</xsl:when>
		</xsl:choose>
	</xsl:template>


</xsl:stylesheet>