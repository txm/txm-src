<!-- The Identity Transformation -->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xsl:output method="xml" cdata-section-elements="Word"></xsl:output>

	<xsl:param name="words-with-id-file">
		Fontenay-w-id-table.xml
	</xsl:param>

	<xsl:variable name="words-with-id" as="element()">
		<xsl:copy-of select="document($words-with-id-file)//text"></xsl:copy-of>
	</xsl:variable>


	<!-- Whenever you match any node or any attribute -->
	<xsl:template match="node()|@*">
		<!-- Copy the current node -->
		<xsl:copy>
			<!-- Including any attributes it has and any child nodes -->
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="Word">
		<xsl:variable name="position" as="xs:integer">
			<xsl:value-of
				select="count(preceding::Word[not(matches(.,'__NOISE__'))]) + 1" />
			<!--<xsl:number count="Word[not(matches(.,'__NOISE__'))]" level="any"/> -->
		</xsl:variable>
		<xsl:if
			test="$words-with-id/w[$position]/preceding-sibling::*[1][self::w-add]">
			<xsl:comment>
				Added word:
			</xsl:comment>
			<Word x1="{@x1 - 5}" y1="{@y1 - 5}" x2="{@x1 - 1}" y2="{@y2}"
				valid="0" imgsig="-" leftcorr="0" rightcorr="0">
				<xsl:value-of
					select="$words-with-id/w[$position]/preceding-sibling::w-add[1]" />
			</Word>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="matches(.,'__NOISE__')">
				<xsl:comment>
					<xsl:copy-of select="."></xsl:copy-of>
				</xsl:comment>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@*" />
					<xsl:attribute name="id"><xsl:value-of
						select="$words-with-id/w[$position]/@id" /></xsl:attribute>
					<xsl:apply-templates />
					<xsl:if test="not(. = $words-with-id/w[$position])">
						<xsl:comment>
							No match!
							<xsl:value-of
								select="$words-with-id//w[$position]/text()" />
						</xsl:comment>
					</xsl:if>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>
</xsl:stylesheet>
