<!-- The Identity Transformation -->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xsl:output method="xml"></xsl:output>

	<xsl:param name="valid-zones-file">
		qgraalTest-zones-words.xml
	</xsl:param>

	<xsl:variable name="valid-zones-element" as="element()">
		<xsl:copy-of
			select="document($valid-zones-file)//tei:facsimile"></xsl:copy-of>
	</xsl:variable>


	<!-- Whenever you match any node or any attribute -->
	<xsl:template match="node()|@*">
		<!-- Copy the current node -->
		<xsl:copy>
			<!-- Including any attributes it has and any child nodes -->
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<!--<xsl:template match="//tei:facsimile"> <xsl:copy> <xsl:copy-of select="$valid-zones-file"></xsl:copy-of> 
		</xsl:copy> </xsl:template> -->
	<xsl:template match="//tei:zone[@type='word']">

		<xsl:variable name="myId">
			<xsl:value-of select="substring-after(@xml:id,'zone-')" />
		</xsl:variable>

		<xsl:variable name="myIdWp">
			<xsl:if test="matches($myId,'wp')">
				<xsl:value-of
					select="replace($myId,'wp_qgraal_cmTest_(.+)_[12]$','$1')" />
			</xsl:if>
		</xsl:variable>

		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:choose>
				<xsl:when
					test="$valid-zones-element//tei:zone[@type='word'][substring-after(@corresp,'#')=$myId]">
					<xsl:variable name="matching-zone" as="element()">
						<xsl:copy-of
							select="$valid-zones-element//tei:zone[@type='word'][substring-after(@corresp,'#')=$myId]" />
					</xsl:variable>
					<xsl:attribute name="ulx"><xsl:value-of
						select="$matching-zone/@ulx" /></xsl:attribute>
					<xsl:attribute name="uly"><xsl:value-of
						select="$matching-zone/@uly" /></xsl:attribute>
					<xsl:attribute name="lrx"><xsl:value-of
						select="$matching-zone/@lrx" /></xsl:attribute>
					<xsl:attribute name="lry"><xsl:value-of
						select="$matching-zone/@lry" /></xsl:attribute>
				</xsl:when>
				<xsl:when
					test="$valid-zones-element//tei:zone[@type='word'][substring-after(@corresp,'#')=$myIdWp and matches($myId,'_1$')]">
					<xsl:variable name="matching-zone" as="element()">
						<dummy>
							<xsl:copy-of
								select="$valid-zones-element//tei:zone[@type='word'][substring-after(@corresp,'#')=$myIdWp]" />
						</dummy>
					</xsl:variable>
					<xsl:attribute name="ulx"><xsl:value-of
						select="$matching-zone/tei:zone[1]/@ulx" /></xsl:attribute>
					<xsl:attribute name="uly"><xsl:value-of
						select="$matching-zone/tei:zone[1]/@uly" /></xsl:attribute>
					<xsl:attribute name="lrx"><xsl:value-of
						select="$matching-zone/tei:zone[1]/@lrx" /></xsl:attribute>
					<xsl:attribute name="lry"><xsl:value-of
						select="$matching-zone/tei:zone[1]/@lry" /></xsl:attribute>
				</xsl:when>
				<xsl:when
					test="$valid-zones-element//tei:zone[@type='word'][substring-after(@corresp,'#')=$myIdWp and matches($myId,'_2$')]">
					<xsl:variable name="matching-zone" as="element()">
						<dummy>
							<xsl:copy-of
								select="$valid-zones-element//tei:zone[@type='word'][substring-after(@corresp,'#')=$myIdWp]" />
						</dummy>
					</xsl:variable>
					<xsl:attribute name="ulx"><xsl:value-of
						select="$matching-zone/tei:zone[2]/@ulx" /></xsl:attribute>
					<xsl:attribute name="uly"><xsl:value-of
						select="$matching-zone/tei:zone[2]/@uly" /></xsl:attribute>
					<xsl:attribute name="lrx"><xsl:value-of
						select="$matching-zone/tei:zone[2]/@lrx" /></xsl:attribute>
					<xsl:attribute name="lry"><xsl:value-of
						select="$matching-zone/tei:zone[2]/@lry" /></xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:comment>
						No matching validated zone found!
					</xsl:comment>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates />
		</xsl:copy>


	</xsl:template>
</xsl:stylesheet>
