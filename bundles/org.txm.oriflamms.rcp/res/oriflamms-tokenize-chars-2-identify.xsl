<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	exclude-result-prefixes="tei edate" version="2.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" />

	<xsl:template match="node()|@*">
		<!-- Copy the current node -->
		<xsl:copy>
			<!-- Including any attributes it has and any child nodes -->
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<!-- <xsl:variable name="filename"> <xsl:analyze-string select="document-uri(.)" 
		regex="^(.*)/([^/]+)\.[^/]+$"> <xsl:matching-substring> <xsl:value-of select="regex-group(2)"/> 
		</xsl:matching-substring> </xsl:analyze-string> </xsl:variable> <xsl:variable 
		name="filedir"> <xsl:analyze-string select="document-uri(.)" regex="^(.*)/([^/]+)\.[^/]+$"> 
		<xsl:matching-substring> <xsl:value-of select="regex-group(1)"/> </xsl:matching-substring> 
		</xsl:analyze-string> </xsl:variable> <xsl:variable name="filenameresult"> 
		<xsl:value-of select="replace($filename,'(-ori)?-w$','')"/> </xsl:variable> 
		<xsl:variable name="path"> <xsl:value-of select="concat($filedir,'/',$filenameresult,'-c-temp/?select=*.xml;recurse=yes;on-error=warning')"/> 
		</xsl:variable> <xsl:variable name="files" select="collection($path)"/> <xsl:template 
		match="/"> <root>Processing files from <xsl:value-of select="$filedir/$filenameresult"/>-c-temp 
		directory. Result files saved in <xsl:value-of select="$filedir/$filenameresult"/>-c 
		directory.</root> <xsl:for-each select="$files"> <xsl:variable name="filename2"> 
		<xsl:analyze-string select="document-uri(.)" regex="^(.*)/([^/]+).xml$"> 
		<xsl:matching-substring> <xsl:value-of select="regex-group(2)"></xsl:value-of> 
		</xsl:matching-substring> </xsl:analyze-string></xsl:variable> <xsl:result-document 
		href="{$filedir}/{$filenameresult}-c/{$filename2}-c.xml"> <xsl:apply-templates/> 
		</xsl:result-document> </xsl:for-each> </xsl:template> -->


	<xsl:template match="tei:c">
		<xsl:variable name="w-id">
			<xsl:choose>
				<xsl:when test="ancestor::tei:pc">
					<xsl:value-of select="ancestor::tei:pc[1]/@xml:id" />
				</xsl:when>
				<xsl:when test="ancestor::tei:w">
					<xsl:value-of select="ancestor::tei:w[1]/@xml:id" />
				</xsl:when>
				<xsl:when test="ancestor::tei:seg[@type='deleted']">
					<xsl:value-of
						select="ancestor::tei:seg[@type='deleted'][1]/@xml:id" />
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="position">
			<xsl:number from="tei:w|tei:pc|tei:seg[@type='deleted']"
				level="any" />
		</xsl:variable>
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="xml:id"><xsl:value-of
				select="concat('c_',$w-id,'_',$position)" /></xsl:attribute>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>



</xsl:stylesheet>
