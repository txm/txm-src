<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:txm="http://textometrie.org/1.0"
	exclude-result-prefixes="tei edate xd txm" version="2.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" indent="no" />

	<xd:doc type="stylesheet">
		<xd:short>
			Cette feuille permet de convertir la transcription compacte de la
			Queste del saint Graal au format XML-TEI du projet Oriflamms.
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2014, CNRS / ICAR (Équipe CACTUS)</xd:copyright>
	</xd:doc>

	<xsl:template match="/">
		<xsl:apply-templates />
	</xsl:template>


	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates
				select="*|processing-instruction()|comment()|text()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="comment()|text()">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="processing-instruction()"></xsl:template>

	<!-- On supprime les attributs par défaut de la DTD TEI -->

	<xsl:template match="@*">
		<xsl:choose>
			<xsl:when
				test="matches(name(.),'^(part|instant|anchored|full)$')" />
			<xsl:when test="matches(name(.),'rend')">
				<xsl:choose>
					<xsl:when test="matches(.,'^aggl$')">
						<xsl:attribute name="rend">space-after(none)</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:teiHeader">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:revisionDesc">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<change xmlns="http://www.tei-c.org/ns/1.0"
				when="{format-date(current-date(),'[Y]-[M01]-[D01]')}" who="#auto">Conversion
				automatique au format XML-TEI-Oriflamms</change>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:encodingDesc">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
			<xsl:if test="not(//tei:prefixDef[@ident='ori'])">
				<listPrefixDef xmlns="http://www.tei-c.org/ns/1.0">
					<prefixDef xmlns="http://www.tei-c.org/ns/1.0"
						ident="ori" matchPattern="([a-z]+)"
						replacementPattern="oriflamms-annotation-scheme.xml#$1">
						<p>
							In the context of this project, private URIs with the prefix
							"ori" point to
							<gi>interp</gi>
							elements in the project's
							oriflamms-annotation-scheme.xml file.
						</p>
					</prefixDef>
				</listPrefixDef>
			</xsl:if>
		</xsl:copy>
	</xsl:template>


	<xsl:template match="tei:div3[@facs]">
		<xsl:variable name="facs">
			<xsl:value-of select="@facs" />
		</xsl:variable>
		<xsl:if test="not(preceding::tei:div3/@facs=$facs)">
			<milestone unit="surface"
				xmlns="http://www.tei-c.org/ns/1.0" facs="{@facs}" />
		</xsl:if>

		<pb xmlns="http://www.tei-c.org/ns/1.0" facs="{@facs}"
			n="{parent::div2/@n}{@n}" />
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:div3[not(@facs)]">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="ana"><xsl:text>ori:align-no</xsl:text></xsl:attribute>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:div4">
		<cb xmlns="http://www.tei-c.org/ns/1.0" n="{@n}" />
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:l">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<lb xmlns="http://www.tei-c.org/ns/1.0" n="{@n}" />
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:note|tei:supplied|tei:head">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="ana">
      <xsl:if test="@ana"><xsl:value-of
				select="concat(@ana,' ')" /></xsl:if>
      <xsl:text>ori:align-no</xsl:text>
    </xsl:attribute>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:c[@type='lettrine']">
		<g xmlns="http://www.tei-c.org/ns/1.0" n="{@n}">
			<xsl:apply-templates select="@*|node()" />
		</g>
	</xsl:template>

</xsl:stylesheet>
