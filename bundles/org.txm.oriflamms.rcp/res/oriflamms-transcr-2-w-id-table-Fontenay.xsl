<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:me="http://www.menota.org/ns/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="#all" version="2.0">

	<xd:doc type="stylesheet">
		<xd:short>

		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2012, CNRS / ICAR (ICAR3 LinCoBaTO)</xd:copyright>
	</xd:doc>


	<xsl:output method="xml" encoding="utf-8" />

	<xsl:strip-space elements="*" />

	<xsl:template match="/">
		<text>
			<xsl:apply-templates
				select="descendant::tei:w|descendant::tei:pc" />
		</text>
	</xsl:template>


	<xsl:template match="tei:w|tei:pc">
		<xsl:call-template name="word" />
	</xsl:template>

	<xsl:template match="tei:seg[@type='wp']">
		<xsl:call-template name="word" />
	</xsl:template>

	<xsl:template name="word">
		<!--<xsl:if test="self::tei:w"></xsl:if> -->
		<xsl:choose>
			<xsl:when test="descendant::tei:lb">
				<xsl:apply-templates
					select="tei:seg[@type='wp']" />
			</xsl:when>
			<xsl:when test="matches(@xml:id,'[a-z]$')">
				<w-add id="{@xml:id}">
					<xsl:apply-templates />
				</w-add>
				<xsl:text>&#xa;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<w id="{@xml:id}">
					<xsl:apply-templates />
				</w>
				<xsl:text>&#xa;</xsl:text>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<xsl:template match="tei:choice">
		<xsl:apply-templates
			select="tei:abbr|tei:orig|tei:sic" />
	</xsl:template>

</xsl:stylesheet>
