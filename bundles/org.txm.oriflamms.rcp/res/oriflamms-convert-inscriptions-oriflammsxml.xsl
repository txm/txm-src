<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:txm="http://textometrie.org/1.0"
	xmlns:xi="http://www.w3.org/2001/XInclude"
	xmlns:svg="http://www.w3.org/2000/svg"
	xmlns:math="http://www.w3.org/1998/Math/MathML"
	exclude-result-prefixes="tei edate xd txm xi svg math" version="2.0">

	<xsl:output method="xml" encoding="utf-8"
		omit-xml-declaration="no" indent="no" />

	<xd:doc type="stylesheet">
		<xd:short>
			Cette feuille permet de convertir la transcription tokénisée des
			Chartes de Fontenay au format XML-TEI du projet Oriflamms.
		</xd:short>
		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
		<xd:author>Alexei Lavrentiev alexei.lavrentev@ens-lyon.fr</xd:author>
		<xd:copyright>2015, CNRS / ICAR (Équipe CACTUS)</xd:copyright>
	</xd:doc>

	<xsl:variable name="filename">
		<xsl:analyze-string select="document-uri(.)"
			regex="^(.*)/([^/]+)\.[^/]+$">
			<xsl:matching-substring>
				<xsl:value-of select="regex-group(2)" />
			</xsl:matching-substring>
		</xsl:analyze-string>
	</xsl:variable>


	<xsl:strip-space elements="*" />
	<xsl:template match="/">
		<xsl:apply-templates />
	</xsl:template>


	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates
				select="*|processing-instruction()|comment()|text()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="comment()|text()">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="processing-instruction()" />

	<!-- On supprime les attributs par défaut de la DTD TEI -->

	<xsl:template match="@*">
		<xsl:choose>
			<xsl:when
				test="matches(name(.),'^(part|instant|anchored|full)$')" />
			<!-- <xsl:when test="matches(name(.),'rend')"> <xsl:choose> <xsl:when 
				test="matches(.,'^aggl$')"><xsl:attribute name="rend">space-after(none)</xsl:attribute></xsl:when> 
				<xsl:otherwise><xsl:copy/></xsl:otherwise> </xsl:choose> </xsl:when> -->
			<xsl:otherwise>
				<xsl:copy />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:teiHeader">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>


	<xsl:template match="tei:revisionDesc">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<change xmlns="http://www.tei-c.org/ns/1.0" when="2007-01-01"
				who="#DS">Publication du document original</change>
			<!-- information récupérée à partir de /TEI/text[1]/front[1]/titlePage[1]/docDate[1] -->
			<change xmlns="http://www.tei-c.org/ns/1.0"
				when="{format-date(current-date(),'[Y]-[M01]-[D01]')}" who="#auto">Conversion
				automatique au format XML-TEI-Oriflamms</change>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:encodingDesc">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
			<xsl:if test="not(//tei:prefixDef[@ident='ori'])">
				<listPrefixDef xmlns="http://www.tei-c.org/ns/1.0">
					<prefixDef xmlns="http://www.tei-c.org/ns/1.0"
						ident="ori" matchPattern="([a-z]+)"
						replacementPattern="oriflamms-annotation-scheme.xml#$1">
						<p>
							In the context of this project, private URIs with the prefix
							"ori" point to
							<gi>interp</gi>
							elements in the project's
							oriflamms-annotation-scheme.xml file.
						</p>
					</prefixDef>
				</listPrefixDef>
			</xsl:if>
		</xsl:copy>
	</xsl:template>

	<!-- On restructure le document pour avoir un TEI par charte -->

	<!-- TEI devient teiCorpus -->

	<xsl:template match="tei:TEI">
		<xsl:element name="teiCorpus"
			xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates select="tei:teiHeader" />
			<facsimile xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:apply-templates select="tei:facsimile" />
			</facsimile>
			<xsl:apply-templates select="tei:text" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="tei:facsimile">
		<xsl:apply-templates />
	</xsl:template>


	<!-- on supprime l'élément text de niveau 1 et travaille directement sur 
		le niveau 2 (l'élément group intermédiaire disparait) -->

	<xsl:template match="tei:TEI/tei:text">
		<xsl:apply-templates select="descendant::tei:text" />
	</xsl:template>

	<xsl:template
		match="tei:text//tei:text[descendant::*[matches(@facs,'\S')]]">

		<TEI xmlns="http://www.tei-c.org/ns/1.0">
			<teiHeader xmlns="http://www.tei-c.org/ns/1.0">
				<fileDesc xmlns="http://www.tei-c.org/ns/1.0">
					<titleStmt xmlns="http://www.tei-c.org/ns/1.0">
						<title xmlns="http://www.tei-c.org/ns/1.0">
							<xsl:text>Acte original </xsl:text>
							<xsl:value-of
								select="substring-after(preceding-sibling::comment()[contains(.,'acte original')][1],'acte original ')" />
						</title>
						<author xmlns="http://www.tei-c.org/ns/1.0">
							<xsl:value-of select="descendant::tei:docAuthor" />
						</author>
					</titleStmt>
					<publicationStmt
						xmlns="http://www.tei-c.org/ns/1.0">
						<p xmlns="http://www.tei-c.org/ns/1.0">See the header of the teiCorpus</p>
					</publicationStmt>
					<!-- les éléments listWit et listBibl sont placés dans sourceDesc -->
					<sourceDesc xmlns="http://www.tei-c.org/ns/1.0">
						<xsl:copy-of select="descendant::tei:listWit" />
						<xsl:copy-of select="descendant::tei:listBibl[1]"></xsl:copy-of>
					</sourceDesc>
				</fileDesc>
				<profileDesc xmlns="http://www.tei-c.org/ns/1.0">
					<!-- docDate devient profileDesc/creation/date[@type='documentCreation'] -->
					<creation xmlns="http://www.tei-c.org/ns/1.0">
						<xsl:apply-templates
							select="descendant::tei:docDate" />
					</creation>
					<!-- argument devient profileDesc/abstract -->
					<xsl:apply-templates
						select="descendant::tei:argument" />
				</profileDesc>
			</teiHeader>
			<text xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:apply-templates select="@*" />
				<body xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:variable name="facsID">
						<xsl:value-of
							select="replace(descendant::*[@facs[matches(.,'\S+')]][1]/@facs,'^#','')" />
					</xsl:variable>
					<xsl:if test="not(tei:body//tei:milestone[@unit='surface'])">
						<milestone xmlns="http://www.tei-c.org/ns/1.0"
							unit="surface">
							<xsl:attribute name="xml:id">
                <xsl:value-of
								select="concat('surf_',$filename,'_',replace(translate($facsID,' ()','_--'),'\.\w+$',''))" />
              </xsl:attribute>
							<xsl:attribute name="facs">
                <xsl:choose>
                  <xsl:when
								test="matches(descendant::*[@facs][1]/@facs,'^#')">
                    <xsl:value-of
								select="//tei:zone[@xml:id=$facsID]/preceding-sibling::tei:graphic[1]/@url" />
                  </xsl:when>
                  <xsl:otherwise><xsl:value-of
								select="$facsID" /></xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
						</milestone>
					</xsl:if>
					<xsl:if
						test="not(descendant::tei:pb[@facs]) and not(tei:body//tei:milestone[@unit='surface'])">
						<pb xmlns="http://www.tei-c.org/ns/1.0">
							<xsl:attribute name="facs">
              <xsl:choose>
                <xsl:when
								test="matches(descendant::*[@facs][1]/@facs,'^#')">
                  <xsl:value-of
								select="//tei:graphic[following-sibling::tei:zone[@xml:id=$facsID]]/@url" />
                  <!--<xsl:value-of select="$facsID"/>-->
                </xsl:when>
                <xsl:otherwise><xsl:value-of
								select="descendant::*[@facs][1]/@facs" /></xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
						</pb>
					</xsl:if>
					<xsl:apply-templates
						select="descendant::tei:div1" />
				</body>
			</text>
		</TEI>
	</xsl:template>

	<xsl:template
		match="tei:text//tei:text[not(descendant::*[@facs])]">
		<xsl:comment>
			No facs attribute provided for any element in this text (n="
			<xsl:value-of select="@n" />
			")!
		</xsl:comment>
		<xsl:comment>
			<xsl:copy-of select="." />
		</xsl:comment>
	</xsl:template>

	<xsl:template match="tei:docDate[ancestor::tei:group]">
		<date type="documentCreation" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</date>
	</xsl:template>

	<xsl:template match="tei:argument">
		<abstract xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates />
		</abstract>
	</xsl:template>

	<!-- on ajoute un attribut pour faciliter l'identification des segments 
		à ne pas aligner -->

	<xsl:template
		match="tei:choice/tei:expan|tei:choice/tei:corr|tei:div1[@type='edition']">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="ana">ori:align-no</xsl:attribute>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>


</xsl:stylesheet>