package org.txm.progression.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Progression UI messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ProgressionUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.progression.rcp.messages.messages"; //$NON-NLS-1$

	public static String defaultEditionIsMissing;

	public static String textP0IsMissing;

	public static String defaultChartTypeIsCumulative;

	public static String repeatTheValuesOfTheStructureProperties;

	public static String bandeSizeMultiplier;

	public static String chartTypeColon;

	public static String bandewidthMultiplierColon;

	public static String repeatSameValues;

	public static String stripMultiplierErrorColonP0;

	public static String cumulative;

	public static String density;

	public static String regexColon;

	public static String add;

	public static String displayAStepPerOccurrenceratherThanADensityCurve;

	public static String factorForRefiningTheSlopeOfCurvesAtTheLimitsOfStructures;

	public static String displayCurveSpikesWhenThereAreOccurrencesRatherThanStep;

	public static String filterStructuresByTheirValueWithARegularExpression;

	static {
		// initializes resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, ProgressionUIMessages.class);
	}

	private ProgressionUIMessages() {
	}
}
