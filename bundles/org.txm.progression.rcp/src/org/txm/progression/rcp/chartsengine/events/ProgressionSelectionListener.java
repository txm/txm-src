package org.txm.progression.rcp.chartsengine.events;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

/**
 * Progression selection listener.
 * 
 * @author sjacquot
 *
 */
public class ProgressionSelectionListener implements SelectionListener {



	@Override
	public void widgetSelected(SelectionEvent e) {


		// FIXME: to discuss
		// Send to other command
		//		if(this.commandId == ProgressionSelectionListener.SEND_TO_INDEX || this.commandId == ProgressionSelectionListener.SEND_TO_CONCORDANCE
		//				|| this.commandId == ProgressionSelectionListener.SEND_TO_COOCCURRENCE)	{
		//
		//
		//			Progression progression = (Progression) this.chartEditor.getResultData();
		//			// gets the progression queries from the current selected chart series
		//			String query = progression.getQueriesString(chartEditor.getEditorInput().getSWTChartComponentsProvider().getChartsEngine().getProgressionChartSelectedSeries(chartEditor.getChart()));
		//			
		//			// Send to index
		//			if(this.commandId == ProgressionSelectionListener.SEND_TO_INDEX)	{
		//	
		//				IndexEditorInput editorInput = new IndexEditorInput(progression.getCorpus());
		//	
		//				try {
		//					IWorkbenchPage page = this.chartEditor.getEditorSite().getWorkbenchWindow().getActivePage();
		//					IndexEditor editor = (IndexEditor) page.openEditor(editorInput, IndexEditor.ID);
		//					editor.setFocus(query);
		//					editor.compute();
		//				}
		//				catch(PartInitException e1) {
		//					e1.printStackTrace();
		//				}
		//			}
		//			// Send to concordance
		//			else if(this.commandId == ProgressionSelectionListener.SEND_TO_CONCORDANCE)	{
		//	
		//				ConcordancesEditorInput editorInput = new ConcordancesEditorInput(progression.getCorpus(), null);
		//	
		//				try {
		//					IWorkbenchPage page = this.chartEditor.getEditorSite().getWorkbenchWindow().getActivePage();
		//					ConcordanceEditor editor = (ConcordanceEditor) page.openEditor(editorInput, "ConcordanceEditor"); //$NON-NLS-1$
		//					editor.setQuery(query);
		//					editor.compute();
		//					
		//					// move in the concordance to the line of the selected progression point
		//					final ArrayList<Integer> selectedPointPositions = chartEditor.getEditorInput().getSWTChartComponentsProvider().getChartsEngine().getProgressionChartSelectedPointPositions(chartEditor.getChart());
		//					ArrayList<EditorPart> linkedEditors = new ArrayList<EditorPart>(); 
		//					linkedEditors.add(editor);
		//					new ProgressionEventCallBack(this.chartEditor).updateLinkedConcordanceEditor(linkedEditors, selectedPointPositions);
		//
		//					
		//				}
		//				catch(PartInitException e1) {
		//					e1.printStackTrace();
		//				}
		//			}
		//			// Send to cooccurrence
		//			else if(this.commandId == ProgressionSelectionListener.SEND_TO_COOCCURRENCE)	{
		//	
		//				CooccurrenceEditorInput editorInput = new CooccurrenceEditorInput(progression.getCorpus());
		//	
		//				try {
		//					IWorkbenchPage page = this.chartEditor.getEditorSite().getWorkbenchWindow().getActivePage();
		//					CooccurrencesEditor editor = (CooccurrencesEditor) page.openEditor(editorInput, CooccurrencesEditor.ID);
		//					editor.setFocus(query);
		//					editor.compute();
		//				}
		//				catch(PartInitException e1) {
		//					e1.printStackTrace();
		//				}
		//			}
		//		}

	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		// TODO Auto-generated method stub

	}

}
