package org.txm.progression.rcp.chartsengine.events;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.EditorPart;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.entity.ChartEntity;
import org.txm.chartsengine.rcp.events.EventCallBack;
import org.txm.chartsengine.rcp.events.ZoomAndPanCallBack;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.core.functions.comparators.LineComparator;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.edition.rcp.editors.RGBA;
import org.txm.edition.rcp.editors.SynopticEditionEditor;
import org.txm.edition.rcp.handlers.OpenEdition;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Edition;
import org.txm.objects.Page;
import org.txm.objects.Text;
import org.txm.progression.core.chartsengine.base.ProgressionChartCreator;
import org.txm.progression.rcp.editors.ProgressionEditor;
import org.txm.progression.rcp.messages.ProgressionUIMessages;
import org.txm.rcp.editors.TXMResultEditorInput;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.utils.logger.Log;

/**
 * Progression chart mixed mouse and key user entries call back.
 * 
 * @author sjacquot
 *
 */
public class ProgressionEventCallBack extends EventCallBack<ProgressionEditor> {

	/**
	 * To keep track of the concordance lines and recomputing only if the selected item is out of this range.
	 */
	protected int lastConcordanceTopLine = -1;

	/**
	 * To keep track of the concordance lines sort and recomputing only if the sorting order has changed.
	 */
	protected LineComparator lastSortingComparator = null;

	@Override
	public void processEvent(final Object event, final int eventArea, final Object o) {

		// Need to run this in the SWT UI thread because it's called from the AWT UI thread in TBX charts engine layer
		chartEditor.getComposite().getDisplay().asyncExec(new Runnable() {

			@Override
			public void run() {

				AWTEvent awtEvent = (AWTEvent) event;

				// Mouse event
				if (event instanceof MouseEvent) {

					MouseEvent mouseEvent = (MouseEvent) event;

					// return if it's not the left mouse button
					if (mouseEvent.getButton() != MouseEvent.BUTTON1) {
						return;
					}

					ChartMouseEvent chartEvent = (ChartMouseEvent) o;
					ChartEntity entity = chartEvent.getEntity();
					final ArrayList<Integer> selectedPointPositions = ((ProgressionChartCreator) chartEditor.getResult().getChartCreator()).getSelectedPointPositions(chartEditor.getChart());

					if (awtEvent.getID() == MouseEvent.MOUSE_CLICKED && mouseEvent.getClickCount() == 2 && eventArea == EventCallBack.AREA_ITEM && !selectedPointPositions.isEmpty()) {

						// Linked edition (CTRL/CMD + double click)
						if ((mouseEvent.getModifiers() & ZoomAndPanCallBack.keyboardZoomModifierKeyMask) != 0) {
							// Creates or updates the linked editor
							updateLinkedEditionEditor(chartEditor.getLinkedEditor(SynopticEditionEditor.class), selectedPointPositions);
						}
						// Linked concordance
						else {
							// Creates or updates the linked editor
							updateLinkedConcordanceEditor(chartEditor.getLinkedEditors(ConcordanceEditor.class), selectedPointPositions);
						}
					}
					else if (awtEvent.getID() == MouseEvent.MOUSE_CLICKED && mouseEvent.getClickCount() == 1 && eventArea == EventCallBack.AREA_ITEM) {

						// Get the linked concordance editors and update the one linked to the progression curve if it exists
						ArrayList<ConcordanceEditor> linkedConcordancesEditors = chartEditor.getLinkedEditors(ConcordanceEditor.class);
						if (!linkedConcordancesEditors.isEmpty()) {
							updateLinkedConcordanceEditor(linkedConcordancesEditors, selectedPointPositions);
						}


						// Get the linked edition editor and update it if exists
						SynopticEditionEditor linkedEditionEditor = chartEditor.getLinkedEditor(SynopticEditionEditor.class);
						if (linkedEditionEditor != null) {
							updateLinkedEditionEditor(linkedEditionEditor, selectedPointPositions);
						}

					}
					else if (awtEvent.getID() == MouseEvent.MOUSE_CLICKED && mouseEvent.getClickCount() == 2 && eventArea == EventCallBack.AREA_EMPTY) {
						// FIXME: debug
						// System.err.println("ProgressionMouseClickedCallBack.processEvent(...).new Runnable() {...}.run(): double click in empty area");
					}
					else if (awtEvent.getID() == MouseEvent.MOUSE_CLICKED && mouseEvent.getClickCount() == 1 && eventArea == EventCallBack.AREA_EMPTY) {
						// FIXME: debug
						// System.err.println("ProgressionMouseClickedCallBack.processEvent(...).new Runnable() {...}.run(): single click in empty area");
					}
				}
				// Key event
				else if (event instanceof KeyEvent) {
					KeyEvent keyEvent = (KeyEvent) event;

					// return if zoom and pan modifier
					if ((keyEvent.getModifiers() & ZoomAndPanCallBack.keyboardZoomModifierKeyMask) != 0) {
						return;
					}

					// Arrow keys + return key
					if (keyEvent.getKeyCode() == KeyEvent.VK_LEFT || keyEvent.getKeyCode() == KeyEvent.VK_RIGHT
							|| keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {

						final ArrayList<Integer> selectedPointPositions = ((ProgressionChartCreator) chartEditor.getResult().getChartCreator()).getSelectedPointPositions(chartEditor.getChart());

						// Get the linked concordance editors and update the one linked to the progression curve if it exists
						ArrayList<ConcordanceEditor> linkedConcordancesEditors = chartEditor.getLinkedEditors(ConcordanceEditor.class);
						if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER || !linkedConcordancesEditors.isEmpty()) {
							updateLinkedConcordanceEditor(linkedConcordancesEditors, selectedPointPositions);
						}

						// Get the linked edition editor and update it if exists
						SynopticEditionEditor linkedEditionEditor = chartEditor.getLinkedEditor(SynopticEditionEditor.class);
						if (linkedEditionEditor != null) {
							updateLinkedEditionEditor(linkedEditionEditor, selectedPointPositions);
						}
					}
				}
			}
		});

	}



	/**
	 * Creates or updates the linked concordance editor.
	 * If the editor doesn't exist it will be created.
	 * 
	 * @param arrayList
	 * @param selectedPointPositions
	 */
	public void updateLinkedConcordanceEditor(ArrayList<ConcordanceEditor> arrayList, final ArrayList<Integer> selectedPointPositions) {

		// Debug
		Log.finest("ProgressionEventCallBack.updateLinkedConcordanceEditor(): linked editors count: " + arrayList.size()); //$NON-NLS-1$

		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		IWorkbenchPage page = window.getActivePage();

		// Get and format the progression query from the selected chart series
		// FIXME: case 1, the query is the query of the selected chart series
		// ArrayList<Integer> selectedSeries = chartEditor.getEditorInput().getSWTChartComponentsProvider().getChartsEngine().getProgressionChartSelectedSeries(chartEditor.getChart());
		// String query = ((Progression) chartEditor.getResultData()).getQueries().get(selectedSeries.get(selectedSeries.size() - 1).intValue()).getQueryString();

		int lastSelectedSeries = ((ProgressionChartCreator) chartEditor.getResult().getChartCreator()).getLastSelectedSeries(chartEditor.getChart());
		if (lastSelectedSeries == -1) { // no selection
			return;
		}
		String query = (chartEditor.getResult()).getQueries().get(lastSelectedSeries).getQueryString();

		// // Remove "[" and "]" only if it's a simple query as "[faire]"
		// if(query.startsWith("[") && !query.contains("=")) { //$NON-NLS-1$ //$NON-NLS-2$
		// int length = query.length();
		// if(query.endsWith("]")) { //$NON-NLS-1$
		// length--;
		// }
		// query = query.substring(1, length);
		// }

		// FIXME: case 2, the query is the combination of all the queries of the progression
		// String query = ((Progression) chartEditor.getResultData()).getQueriesString();

		// gets the linked editor according to the query if it exists
		ConcordanceEditor linkedEditor = null;
		for (int i = 0; i < arrayList.size(); i++) {
			if (query.equals(((ConcordanceEditor) arrayList.get(i)).getResult().getQuery().getQueryString())) {
				linkedEditor = ((ConcordanceEditor) arrayList.get(i));
				break;
			}
		}



		final TXMResultEditorInput<Concordance> editorInput;
		boolean newEditor = false;
		// Create the linked editor input if needed
		if (linkedEditor == null) {
			Concordance concordance = new Concordance((chartEditor.getResult()).getCorpus());
			concordance.setQuery(query);
			editorInput = new TXMResultEditorInput<>(concordance);
			newEditor = true;
		}
		else {
			editorInput = linkedEditor.getEditorInput();
		}

		try {
			boolean wasOpenedEditor = SWTEditorsUtils.isOpenEditor(editorInput, ConcordanceEditor.ID);

			linkedEditor = (ConcordanceEditor) SWTEditorsUtils.openEditor(editorInput, ConcordanceEditor.ID, false);
			if (newEditor) {
				chartEditor.addLinkedEditor(linkedEditor);
			}

			// Split the area and insert the new editor
			if (!wasOpenedEditor) {

				int position = EModelService.BELOW;
				EditorPart parentEditor = chartEditor;

				// Split vertically if there is already a concordance or an edition editor otherwise split horizontally
				if (arrayList != null && !arrayList.isEmpty()) {
					position = EModelService.RIGHT_OF;
					parentEditor = arrayList.get(arrayList.size() - 1);

					// FIXME: Debug
					System.err.println("ProgressionEventCallBack.updateLinkedConcordanceEditor(): parent editor for splitting: " + parentEditor); //$NON-NLS-1$
				}
				// Split and add linked editor
				if (parentEditor != null && position >= 0) {
					SWTEditorsUtils.moveEditor(parentEditor, linkedEditor, position);
				}
			}

		}
		catch (PartInitException e) {
			System.err.println(NLS.bind(TXMCoreMessages.errorColonP0, e.getLocalizedMessage()));
		}

		// Compute the concordance if it has never been computed
		if (newEditor) {
			Job job = linkedEditor.compute(false);

			final ConcordanceEditor linkedEditor2 = linkedEditor;

			job.addJobChangeListener(new JobChangeAdapter() {

				@Override
				public void done(IJobChangeEvent event) {
					super.done(event);

					// initialize range change tracking variables
					lastConcordanceTopLine = -1;
					// lastConcordanceBottomLine = linkedEditor.getLinePerPage();
					lastSortingComparator = linkedEditor2.getCurrentComparator();

					updateLinkedConcordanceEditor(linkedEditor2, selectedPointPositions, true);

					// give back the focus to the chart editor and chart composite
					chartEditor.getComposite().getDisplay().asyncExec(new Runnable() {

						@Override
						public void run() {
							chartEditor.setFocus();
							chartEditor.getChartComposite().setFocus();
						}
					});

				}
			});



		}
		else {
			updateLinkedConcordanceEditor(linkedEditor, selectedPointPositions, false);
			// chartEditor.getChartComposite().setFocus();
		}

		// give back the focus to chart editor and the chart composite
		// chartEditor.setFocus();
		// chartEditor.getChartComposite().setFocus();
	}


	/**
	 * 
	 * @param linkedEditor
	 * @param selectedPointPositions
	 * @param loadLines
	 */
	public void updateLinkedConcordanceEditor(final ConcordanceEditor linkedEditor, final ArrayList<Integer> selectedPointPositions, boolean loadLines) {

		final TableViewer linesTableViewer = linkedEditor.getTableViewer();
		final TableViewer refTableViewer = linkedEditor.getReferenceTableViewer();

		if (linesTableViewer.getTable().isDisposed()) {
			return;
		}

		if (selectedPointPositions.size() > 0) {

			// Get the index of the last selected point in concordance according to the global token position and concordance sorting order
			int lineIndex = linkedEditor.getResult().indexOf(selectedPointPositions.get(selectedPointPositions.size() - 1));

			int top = lineIndex - (lineIndex % linkedEditor.getLinePerPage());

			linkedEditor.getResult().setTopIndex(top);

			// Manage the lazy loading of new lines range in the concordance table
			// page change
			if (lastConcordanceTopLine != top) {
				loadLines = true;
			}
			// sort change
			else if (lastSortingComparator != linkedEditor.getCurrentComparator()) {
				loadLines = true;
			}


			// Loads new lines, fill the display area and select the table row
			if (loadLines) {
				Display.getDefault().syncExec(new Runnable() {

					@Override
					public void run() {
						linkedEditor.fillDisplayArea(true);
					}
				});
			}

			// update range change tracking variables
			lastConcordanceTopLine = top;
			// lastConcordanceBottomLine = bottom;
			lastSortingComparator = linkedEditor.getCurrentComparator();


			final int selectedPointInTableRange = lineIndex - top;

			linesTableViewer.getControl().getDisplay().asyncExec(new Runnable() {

				@Override
				public void run() {
					linesTableViewer.getTable().deselectAll();
					refTableViewer.getTable().deselectAll();

					Object line = linesTableViewer.getElementAt(selectedPointInTableRange);
					Object refLine = refTableViewer.getElementAt(selectedPointInTableRange);
					if (line != null && refLine != null) {
						refTableViewer.setSelection(new StructuredSelection(refLine), false);
						linesTableViewer.setSelection(new StructuredSelection(line), true);

						// FIXME: commented because the set top index makes the read of the table difficult
						// tableViewer.getTable().setTopIndex(selectedPointInTableRange);
						Log.finest("ProgressionEventCallBack.updateLinkedConcordanceEditor(...).new Runnable() {...}.run(): Selecting progression point (" + selectedPointInTableRange + " / " //$NON-NLS-1$ //$NON-NLS-2$
								+ linesTableViewer.getTable().getItemCount() + ") in the linked Concordance table.");  //$NON-NLS-1$
					}
					else {
						Log.finest("ProgressionEventCallBack.updateLinkedConcordanceEditor(...).new Runnable() {...}.run(): Selected progression point (" + selectedPointInTableRange + " / " //$NON-NLS-1$ //$NON-NLS-2$
								+ linesTableViewer.getTable().getItemCount() + ") doesn't exist in the linked Concordance table.");  //$NON-NLS-1$
					}
				}
			});
		}
		else {
			linesTableViewer.getTable().deselectAll();
			refTableViewer.getTable().deselectAll();
		}
	}

	// FIXME: version with new SynopticEditionEditor system edition
	/**
	 * Updates the linked edition editor.
	 * If the editor doesn't exist it will be created.
	 * 
	 * @param linkedEditor
	 */
	public void updateLinkedEditionEditor(SynopticEditionEditor linkedEditor, ArrayList<Integer> selectedPointPositions) {

		try {

			ArrayList<Color> seriesColors = chartEditor.getResult().getChartCreator().getSeriesShapesColors(chartEditor.getChart());
			RGBA selectedWordColor = new RGBA(255, 10, 10);

			// Unhighlight all tokens
			if (linkedEditor != null) {
				linkedEditor.removeHighlightWords();
				linkedEditor.updateWordStyles();
			}

			CQPCorpus corpus = chartEditor.getResult().getCorpus();
			StructuralUnit textS = corpus.getStructuralUnit(CorpusBuild.TEXT);
			Property textP = textS.getProperty(CorpusBuild.ID);
			Match selectedWordMatch = new Match(selectedPointPositions.get(selectedPointPositions.size() - 1), selectedPointPositions.get(selectedPointPositions.size() - 1));
			String textId = selectedWordMatch.getValueForProperty(textP);

			Text text = corpus.getProject().getText(textId);
			if (text == null) {
				Log.severe(ProgressionUIMessages.bind(ProgressionUIMessages.textP0IsMissing, textId));
				return;
			}

			Edition edition = text.getEdition(corpus.getProject().getDefaultEditionName());

			if (edition == null) {
				Log.severe(ProgressionUIMessages.defaultEditionIsMissing);
				return;
			}

			// Compute the edition if needed
			if (!edition.hasBeenComputedOnce()) {
				edition.compute();
			}

			// Get the current selected word to highlight more
			List<String> selectedWordIds = selectedWordMatch.getValuesForProperty(corpus.getProperty(CorpusBuild.ID));

			// Get all words to highlight from the progression chart data set
			ArrayList<ArrayList<Integer>> allPointPositionsBySeries = ((ProgressionChartCreator) chartEditor.getResult().getChartCreator()).getAllPointPositionsBySeries(chartEditor.getChart());

			ArrayList<ArrayList<String>> allWordIds = new ArrayList<>(allPointPositionsBySeries.size());
			for (int i = 0; i < allPointPositionsBySeries.size(); i++) {
				ArrayList<Integer> positions = allPointPositionsBySeries.get(i);
				ArrayList<String> wordsIds = new ArrayList<>();
				for (int j = 0; j < positions.size(); j++) {
					Match match = new Match(positions.get(j), positions.get(j));
					if (match.getValueForProperty(textP).equals(textId)) {
						wordsIds.add(match.getValueForProperty(corpus.getProperty(CorpusBuild.ID))); // ;
					}
				}
				allWordIds.add(wordsIds);
			}

			Page openPage = edition.getPageForWordId(selectedWordIds.get(0));

			if (openPage != null) {

				// Create the linked editor if needed
				if (linkedEditor == null || linkedEditor.isDisposed()) {
					linkedEditor = OpenEdition.openEdition(corpus, OpenEdition.getDefaultEditions(corpus));
					chartEditor.addLinkedEditor(linkedEditor);

					int position = EModelService.BELOW;
					;
					EditorPart parentEditor = chartEditor;
					// Split vertically if there is already a concordance editor otherwise split horizontally
					ConcordanceEditor linkedConcordanceEditor = (ConcordanceEditor) chartEditor.getLinkedEditor(ConcordanceEditor.class);
					if (linkedConcordanceEditor != null) {
						position = EModelService.RIGHT_OF;
						parentEditor = linkedConcordanceEditor;
					}

					// Split and add linked editor
					if (parentEditor != null && position >= 0) {
						SWTEditorsUtils.moveEditor(parentEditor, linkedEditor, position);
					}
				}

				linkedEditor.goToPage(text.getName(), openPage.getName());

				// Highlight tokens by series
				for (int i = 0; i < allWordIds.size(); i++) {
					linkedEditor.addHighlightWordsById(new RGBA(seriesColors.get(i).getRed(), seriesColors.get(i).getGreen(), seriesColors.get(i).getBlue()), allWordIds.get(i));
				}
				// Highlight selected curve token
				linkedEditor.addHighlightWordsById(selectedWordColor, selectedWordIds);
				linkedEditor.setFocusedWordID(selectedWordIds.get(0)); // to scroll to the selected word

				IWorkbenchPage attachedPage = linkedEditor.getEditorSite().getPage();
				attachedPage.bringToTop(linkedEditor);

				// FIXME: SJ: need to check that. Under Linux it seems we need to give back the focus to the chart editor and composite
				// give back the focus to the chart editor and chart composite
				// chartEditor.getComposite().getDisplay().asyncExec(new Runnable() {
				// @Override
				// public void run() {
				// chartEditor.setFocus();
				// chartEditor.getChartComposite().setFocus();
				// }
				// });
			}
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
	}



}
