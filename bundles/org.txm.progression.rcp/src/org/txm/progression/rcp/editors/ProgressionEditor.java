/**
 * 
 */
package org.txm.progression.rcp.editors;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.chartsengine.r.core.themes.DefaultTheme;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.messages.ChartsEngineUIMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.progression.core.functions.Progression;
import org.txm.progression.core.preferences.ProgressionPreferences;
import org.txm.progression.rcp.messages.ProgressionUIMessages;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.listeners.ComputeKeyListener;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.widget.AssistedChoiceQueryWidget;
import org.txm.rcp.swt.widget.FloatSpinner;
import org.txm.rcp.swt.widget.MultipleQueriesComposite;
import org.txm.rcp.swt.widget.structures.StructuralUnitPropertiesComboViewer;
import org.txm.rcp.swt.widget.structures.StructuralUnitsComboViewer;
import org.txm.rcp.swt.widget.structures.StructuralUnitsCombosGroup;
import org.txm.searchengine.core.IQuery;
import org.txm.utils.logger.Log;

/**
 * Progression command chart editor.
 * 
 * @author sjacquot
 *
 */
public class ProgressionEditor extends ChartEditor<Progression> {

	//	/** The chart type. */
	//	private boolean cumulative = true;

	/** The linewidth. */
	private int lineWidth = 2;


	/** The main panel. */
	Composite mainPanel;

	/** The bande field. */
	@Parameter(key = ProgressionPreferences.BANDE_MULTIPLIER)
	FloatSpinner bandeField;

	/** The cumu button. */
	Button cumulativeButton;

	/** The colors. */
	String[] colors = DefaultTheme.colors;


	private Button densityButton;

	private AssistedChoiceQueryWidget queryWidget;

	private Label miniInfoLabel;

	MultipleQueriesComposite queriesFocusComposite;

	/**
	 * Queries.
	 */
	@Parameter(key = TXMPreferences.QUERIES)
	protected List<IQuery> queryList;

	/**
	 * Structural unit.
	 */
	@Parameter(key = TXMPreferences.STRUCTURAL_UNIT)
	protected StructuralUnitsComboViewer structuralUnitsComboViewer;

	/**
	 * Structural unit property.
	 */
	@Parameter(key = TXMPreferences.STRUCTURAL_UNIT_PROPERTY)
	protected StructuralUnitPropertiesComboViewer structuralUnitPropertiesComboViewer;

	/**
	 * Property REGEX.
	 */
	@Parameter(key = ProgressionPreferences.PROPERTY_REGEX)
	protected Text propertyRegex;

	/**
	 * Multiple line styles.
	 */
	@Parameter(key = ChartsEnginePreferences.MULTIPLE_LINE_STROKES)
	protected Button multipleLineStrokes;

	/**
	 * Repeats or not the same value when displaying the section markers.
	 */
	@Parameter(key = ProgressionPreferences.REPEAT_SAME_VALUES)
	protected Button repeatSameValues;

	@Override
	public void __createPartControl() {

		// FIXME: tests to use Fields editor and ScopedPreferenceStore for command parameters
		// BooleanFieldEditor field = new BooleanFieldEditor(ChartsEnginePreferences.SHOW_TITLE, "test2 show title store", editor.getParametersComposite());
		// field.setPreferenceStore(new TXMPreferenceStore(TXMPreferences.getId(progression)));
		// field.load();
		// field.store();
		// try {
		// ((ScopedPreferenceStore) field.getPreferenceStore()).save();
		// }
		// catch(IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }


		// FIXME: SJ: to discuss
		// // initialize extended menus for item selection
		// Menu menu = editor.getComposite().getContextMenu(EventCallBack.AREA_ITEM);
		//
		// // send to index
		// MenuItem sendToIndex = new MenuItem(menu.getItem(0).getMenu(), SWT.CASCADE);
		// sendToIndex.setText(SWTComponentsProviderMessages.SWTChartsComponentProvider_SHARED_SEND_SELECTION_TO_INDEX);
		// sendToIndex.setImage(IImageKeys.getImage(IImageKeys.ACTION_INDEX));
		// sendToIndex.addSelectionListener(new ProgressionSelectionListener(editor, BaseSelectionListener.SEND_TO_INDEX));
		//
		// // send to concordance
		// MenuItem sendToConcordance = new MenuItem(menu.getItem(0).getMenu(), SWT.CASCADE);
		// sendToConcordance.setText(SWTComponentsProviderMessages.SWTChartsComponentProvider_SHARED_SEND_SELECTION_TO_CONCORDANCE);
		// sendToConcordance.setImage(IImageKeys.getImage(IImageKeys.ACTION_CONCORDANCES));
		// sendToConcordance.addSelectionListener(new ProgressionSelectionListener(editor, BaseSelectionListener.SEND_TO_CONCORDANCE));
		//
		// // send to cooccurrence
		// MenuItem sendToCoorccurrence = new MenuItem(menu.getItem(0).getMenu(), SWT.CASCADE);
		// sendToCoorccurrence.setText(SWTComponentsProviderMessages.SWTChartsComponentProvider_SHARED_SEND_SELECTION_TO_COOCCURRENCE);
		// sendToCoorccurrence.setImage(IImageKeys.getImage(IImageKeys.ACTION_COOCCURRENCE));
		// sendToCoorccurrence.addSelectionListener(new ProgressionSelectionListener(editor, BaseSelectionListener.SEND_TO_COOCCURRENCE));
		// //*****************************************************


		try {
			// Query:
			this.getMainParametersComposite().getLayout().numColumns = 4;
			// make
			this.getMainParametersComposite().setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));


			// Computing listeners
			ComputeSelectionListener computeSelectionListener = new ComputeSelectionListener(this, true);
			ComputeKeyListener computeKeyListener = new ComputeKeyListener(this);


			miniInfoLabel = new Label(getMainParametersComposite(), SWT.NONE);
			miniInfoLabel.setText(TXMUIMessages.noQueryDash);
			miniInfoLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

			// Label queryLabel = new Label(getMainParametersComposite(), SWT.NONE);
			// queryLabel.setText("New query");
			// queryLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

			getComputeButton().addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					if (queryWidget.getQueryString().length() > 0 && queriesFocusComposite.onPlusButtonPressed(null, queryWidget.getQuery())) {
						queryWidget.clearQuery();
					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			// [ (v)]
			// queryWidget = new QueryWidget(queryArea, SWT.DROP_DOWN);
			queryWidget = new AssistedChoiceQueryWidget(getMainParametersComposite(), SWT.DROP_DOWN, getResult().getCorpus());
			queryWidget.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			queryWidget.addKeyListener(new ComputeKeyListener(this) {

				@Override
				public void keyPressed(KeyEvent e) {
					if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
						if (queryWidget.getQueryString().length() > 0 && queriesFocusComposite.onPlusButtonPressed(null, queryWidget.getQuery())) {
							super.keyPressed(e); // recompute only if the query has been added
							queryWidget.clearQuery();
						}
						else {
							queryWidget.setText(queryWidget.getQueryString());
						}
					}
				}
			});
			queryWidget.getQueryWidget().addModifyListener(computeKeyListener);

			//			Button addNewQueryButton = new Button(getMainParametersComposite(), SWT.PUSH);
			//			addNewQueryButton.setText(ProgressionUIMessages.add);
			//			addNewQueryButton.setToolTipText("Add a new query");
			//			addNewQueryButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
			//			addNewQueryButton.addSelectionListener(new SelectionListener() {
			//				
			//				@Override
			//				public void widgetSelected(SelectionEvent e) {
			//					if (queryWidget.getQueryString().length() > 0 && onPlusButtonPressed(null, queryWidget.getQueryString())) {
			//						compute(true);
			//						queryWidget.clearQuery();
			//					}
			//					else {
			//						queryWidget.setText(queryWidget.getQueryString());
			//					}
			//				}
			//				
			//				@Override
			//				public void widgetDefaultSelected(SelectionEvent e) {}
			//			});
			// System.out.println(parent.getLayout());
			Composite mainPanel = this.getExtendedParametersGroup();
			GridData gridData = new GridData(GridData.FILL, GridData.CENTER, true, true);
			mainPanel.setLayoutData(gridData);
			GridLayout glayout = new GridLayout(2, false);
			glayout.marginBottom = glayout.marginTop = glayout.marginHeight = glayout.verticalSpacing = 0;
			mainPanel.setLayout(glayout);

			Composite paramPanel = new Composite(mainPanel, SWT.NONE);
			glayout = new GridLayout(3, false);
			glayout.marginBottom = glayout.marginTop = glayout.marginHeight = glayout.verticalSpacing = 0;
			paramPanel.setLayout(glayout);
			paramPanel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));

			// controls
			// graph param
			Label typeLabel = new Label(paramPanel, SWT.NONE);
			typeLabel.setText(ProgressionUIMessages.chartTypeColon);
			typeLabel.setAlignment(SWT.CENTER);

			cumulativeButton = new Button(paramPanel, SWT.RADIO);
			cumulativeButton.setText(ProgressionUIMessages.cumulative);
			cumulativeButton.setToolTipText(ProgressionUIMessages.displayAStepPerOccurrenceratherThanADensityCurve);
			// listeners
			cumulativeButton.addSelectionListener(new ComputeSelectionListener(this, true) {

				@Override
				public void widgetSelected(SelectionEvent e) {
					bandeField.setEnabled(densityButton.getSelection());
					getResult().setDefaultChartType();
					super.widgetSelected(e);
				}
			});

			densityButton = new Button(paramPanel, SWT.RADIO);
			densityButton.setText(ProgressionUIMessages.density);
			densityButton.setToolTipText(ProgressionUIMessages.displayCurveSpikesWhenThereAreOccurrencesRatherThanStep);
			// listener
			densityButton.addSelectionListener(new ComputeSelectionListener(this, true) {

				@Override
				public void widgetSelected(SelectionEvent e) {
					bandeField.setEnabled(densityButton.getSelection());
					// This is kind of a hack to change the chart type. See the evolution of the Chart type parameter management
					getResult().setChartType(Progression.DENSITY_CHART_TYPE);
					super.widgetSelected(e);
				}
			});

			// Bande size
			typeLabel = new Label(paramPanel, SWT.NONE);
			typeLabel.setText(ProgressionUIMessages.bandewidthMultiplierColon);
			typeLabel.setAlignment(SWT.CENTER);

			bandeField = new FloatSpinner(paramPanel, SWT.BORDER);
			bandeField.setEnabled(densityButton.getSelection());
			bandeField.setSelection(this.getResult().getBandeMultiplier()); //$NON-NLS-1$
			bandeField.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, true, 2, 1));
			bandeField.addKeyListener(computeKeyListener);
			bandeField.addModifyListener(computeKeyListener);
			bandeField.setToolTipText("In density mode, drives the curve precision");


			// Structural units and properties combo viewers
			StructuralUnitsCombosGroup structuration = new StructuralUnitsCombosGroup(paramPanel, this, this.getResult().getStructuralUnit(), this.getResult().getStructuralUnitProperty());
			structuration.setLayout(new GridLayout(4, false));

			GridData gdata = new GridData(GridData.FILL, GridData.BEGINNING, true, false, 3, 1);
			structuration.setLayoutData(gdata); // the group must use the full line

			// Structural Unit
			structuralUnitsComboViewer = structuration.getStructuralUnitsComboViewer();

			// Structural Unit Property
			structuralUnitPropertiesComboViewer = structuration.getStructuralUnitPropertiesComboViewer();


			// property REGEX
			Label regexLabel = new Label(structuration, SWT.NONE);
			regexLabel.setText(ProgressionUIMessages.regexColon);
			regexLabel.setAlignment(SWT.CENTER);

			propertyRegex = new Text(structuration, SWT.SINGLE | SWT.BORDER);
			propertyRegex.addKeyListener(computeKeyListener);
			propertyRegex.addModifyListener(computeKeyListener);
			propertyRegex.setToolTipText(ProgressionUIMessages.filterStructuresByTheirValueWithARegularExpression);

			// Repeat same values in part markers
			repeatSameValues = new Button(structuration, SWT.CHECK);
			repeatSameValues.setText(ProgressionUIMessages.repeatSameValues);
			repeatSameValues.addSelectionListener(computeSelectionListener);
			repeatSameValues.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, false, false, 4, 1));
			repeatSameValues.setToolTipText(ProgressionUIMessages.repeatTheValuesOfTheStructureProperties);

			// Lines styles
			multipleLineStrokes = new Button(paramPanel, SWT.CHECK);
			multipleLineStrokes.setText(ChartsEngineUIMessages.multipleLineStrokeStyles);
			multipleLineStrokes.addSelectionListener(computeSelectionListener);
			multipleLineStrokes.setToolTipText("Style curves from plain, dot, dash...");

			// Queries
			queriesFocusComposite = new MultipleQueriesComposite(mainPanel, SWT.NONE, this, colors.length, miniInfoLabel);
			GridData multiQueriesGData = new GridData(GridData.FILL, GridData.FILL, true, true);
			multiQueriesGData.heightHint = multiQueriesGData.minimumHeight = 120;
			queriesFocusComposite.setLayoutData(multiQueriesGData);

			// queries parameters
			// FIXME: useless?
			// Label focusLabel = new Label(queriesFocusComposite, SWT.NONE);
			// focusLabel.setText(ProgressionUIMessages.ProgressionDialog_8);
			// focusLabel.setAlignment(SWT.CENTER);


			getTopToolbar().setVisible(TXMEditor.COMPUTING_PARAMETERS_GROUP_ID, false);
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
	}



	@Override
	public void _setFocus() {
		// Give the focus to the query widget only if the result has not yet been computed (otherwise it breaks the synchronous Concordance/Edition)
		if (!this.getResult().hasBeenComputedOnce()) {
			if (queryWidget != null && !queryWidget.isDisposed()) {
				queryWidget.setFocus();
			}
		}
	}



	@Override
	public void updateEditorFromChart(boolean update) {

		// queryList = this.getResult().getQueries();

		// initialize query fields
		if (queryList != null) {

			queriesFocusComposite.clearQueries();

			for (IQuery q : queryList) {
				queriesFocusComposite.addFocusQueryField(q);
			}
		}

		cumulativeButton.setSelection(this.getResult().isDefaultChartType());
		densityButton.setSelection(!this.getResult().isDefaultChartType());
		bandeField.setEnabled(densityButton.getSelection());
	}

	@Override
	public void updateResultFromEditor() {

		// create query list from widget
		queryList = queriesFocusComposite.getQueries();

		//		String smultibande = bandeField.getText();
		//		float bandeMultiplier = bandeField.getSelectionAsFloat();
		//		if (smultibande != null && smultibande.length() > 0) {
		//			try {
		//				bandeMultiplier = Float.parseFloat(smultibande);
		//			}
		//			catch (Exception e) {
		//				bandeMultiplier = 1.0f;
		//				Log.severe(NLS.bind(ProgressionUIMessages.stripMultiplierErrorColonP0, e));
		//				StatusLine.error(NLS.bind(ProgressionUIMessages.stripMultiplierErrorColonP0, e));
		//			}
		//		}
		//		else {
		//			bandeMultiplier = 1.0f;
		//		}
	}

}
