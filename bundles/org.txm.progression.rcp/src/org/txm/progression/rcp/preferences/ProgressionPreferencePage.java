// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.progression.rcp.preferences;

import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.progression.core.messages.ProgressionCoreMessages;
import org.txm.progression.core.preferences.ProgressionPreferences;
import org.txm.progression.rcp.adapters.ProgressionAdapterFactory;
import org.txm.progression.rcp.messages.ProgressionUIMessages;
import org.txm.rcp.jface.DoubleFieldEditor;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.widget.preferences.BooleanChoiceField;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;

/**
 * Progression preference page.
 * 
 * @author sjacquot
 *
 */
public class ProgressionPreferencePage extends TXMPreferencePage {

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {


		// Charts rendering
		Composite chartsTab = SWTChartsComponentsProvider.createChartsRenderingPreferencesTabFolderComposite(this.getFieldEditorParent());

		// afficher une « marche » par occurrence plutôt qu'une courbe de densité
		BooleanChoiceField choiceField = new BooleanChoiceField(ProgressionPreferences.CHART_CUMULATIVE, ProgressionUIMessages.chartTypeColon, new String[] {ProgressionUIMessages.cumulative, ProgressionUIMessages.density}, chartsTab);
		choiceField.setToolTipText(ProgressionUIMessages.displayAStepPerOccurrenceratherThanADensityCurve);
		this.addField(choiceField);
		choiceField.getRadioBoxControl().setLayout(new RowLayout());
		
		BooleanFieldEditor field = new BooleanFieldEditor(ProgressionPreferences.REPEAT_SAME_VALUES, ProgressionUIMessages.repeatTheValuesOfTheStructureProperties, chartsTab);
		field.setToolTipText(ProgressionUIMessages.repeatTheValuesOfTheStructureProperties);
		this.addField(field);

		DoubleFieldEditor bandemultiplierfield = new DoubleFieldEditor(ProgressionPreferences.BANDE_MULTIPLIER, ProgressionUIMessages.bandeSizeMultiplier, chartsTab);
		bandemultiplierfield.setToolTipText(ProgressionUIMessages.factorForRefiningTheSlopeOfCurvesAtTheLimitsOfStructures);
		this.addField(bandemultiplierfield);
		// FIXME: valid range
		//if (bandemultiplierfield.getIntValue() == 0)
		//	bandemultiplierfield.setStringValue("1");
		//bandemultiplierfield.setValidRange(1, 10);

		// other shared preferences
		SWTChartsComponentsProvider.createChartsRenderingPreferencesFields(this, chartsTab);
	}

	@Override
	public void init(IWorkbench workbench) {

		this.setPreferenceStore(new TXMPreferenceStore(ProgressionPreferences.getInstance().getPreferencesNodeQualifier()));

		//FIXME: description
		//setDescription("Progression");
		this.setTitle(ProgressionCoreMessages.RESULT_TYPE);
		this.setImageDescriptor(ProgressionAdapterFactory.ICON);
	}

}
