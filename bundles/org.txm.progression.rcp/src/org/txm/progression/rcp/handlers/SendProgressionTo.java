// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.progression.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.txm.links.rcp.handlers.SendSelectionToQueryable;
import org.txm.progression.core.chartsengine.base.ProgressionChartCreator;
import org.txm.progression.core.functions.Progression;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;

/**
 * Sends the selected points of a progression to compute another command.
 * 
 * @author sjacquot
 * 
 */
public class SendProgressionTo extends SendSelectionToQueryable {


	@Override
	public Query createQuery(ExecutionEvent event, ISelection selection) {

		Query query = null;
		if (selection instanceof StructuredSelection sselection) {
			Progression progression = (Progression) BaseAbstractHandler.getActiveEditorResult(event);

			// gets the progression queries from the current selected chart series
			progression.getQueries().get(0);
			query = new CQLQuery(progression.getQueriesString(((ProgressionChartCreator) progression.getChartCreator()).getSelectedSeries(progression.getChart())));
		}
		return query;
	}

}
