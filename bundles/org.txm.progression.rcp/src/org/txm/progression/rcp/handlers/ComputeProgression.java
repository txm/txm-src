// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.progression.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.core.preferences.TXMPreferences;
import org.txm.progression.core.functions.Progression;
import org.txm.progression.core.messages.ProgressionCoreMessages;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.utils.logger.Log;


/**
 * Opens a Progression editor.
 * Computes the result if it contains all required parameters.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class ComputeProgression extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkCorpusEngine()) {
			return false;
		}

		Progression progression = null;

		// From link/"send to command": creating from a prebuilt parameter node path
		//FIXME: SJ: all "Send to" commands have a different node path schema than normal result creation since we do not know the target class in the source handler (the class string is replaced with "_link")  
		String parametersNodePath = event.getParameter(TXMPreferences.RESULT_PARAMETERS_NODE_PATH);
		if (parametersNodePath != null && !parametersNodePath.isEmpty()) {
			progression = new Progression(parametersNodePath);
		}
		// From view result node
		else {
			Object selection = this.getCorporaViewSelectedObject(event);

			// Creating from Corpus
			if (selection instanceof CQPCorpus) {
				CQPCorpus corpus = (CQPCorpus) selection;
				try {
					corpus.compute();
					if (!Progression.canRunProgression(corpus)) {
						Log.severe(ProgressionCoreMessages.theProgressionCommandIsNotYetAvailableForDiscontinuousSubcorpora);
						// La fonctionnalité Progression n'est pas encore disponible pour les sous-corpus discontinus.
						return null;
					}
					progression = new Progression(corpus);
				}
				catch (Exception e) {
					Log.printStackTrace(e);
				}
			}
			// Reopening an existing result
			else if (selection instanceof Progression) {
				progression = (Progression) selection;
			}
			// Error
			else {
				return this.logCanNotExecuteCommand(selection);
			}
		}

		ChartEditor.openEditor(progression);

		return null;
	}

}
