package org.txm.wordcloud.core.chartsengine.r;

import java.io.File;

import org.txm.chartsengine.r.core.RChartCreator;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.wordcloud.core.functions.WordCloud;

public class RWordCloudChartCreator extends RChartCreator<WordCloud> {


	@Override
	public File createChart(WordCloud result, File file) {

		try {
			WordCloud wordCloud = result;


			// parameters
			int maxWords = wordCloud.getMaxWordsCount();
			int minFreq = wordCloud.getFMin();
			float rotPer = wordCloud.getRotationPercent();
			boolean randomOrder = wordCloud.isRandomPositions();


			RWorkspace rw = RWorkspace.getRWorkspaceInstance();
			// System.out.println("load wordcloud...");
			rw.eval("library(wordcloud)"); //$NON-NLS-1$
			// System.out.println("tmpfreqs...");
			rw.addVectorToWorkspace("tmpfreqs", wordCloud.getFreqs()); //$NON-NLS-1$
			// System.out.println("tmplabels...");
			rw.addVectorToWorkspace("tmplabels", wordCloud.getLabels()); //$NON-NLS-1$

			// wordcloud(words,freq,
			// scale : A vector of length 2 indicating the range of the size of the words.
			// min.freq: words with frequency below min.freq will not be plotted
			// max.words : Maximum number of words to be plotted. least frequent terms dropped
			// random.order : plot words in random order. If false, they will be plotted in decreasing frequency
			// random.color : choose colors randomly from the colors. If false, the color is chosen based on the frequency
			// rot.per : proportion words with 90 degree rotation
			// colors : color words from least to most frequent
			// ordered.colors : if true, then colors are assigned to words in order
			// System.out.println("wordcloud...");
			String cmd = "wordcloud(tmplabels, tmpfreqs, random.color=FALSE, colors=brewer.pal(10,\"Dark2\")"; //$NON-NLS-1$

			if (randomOrder == false)
				cmd += ", random.order=FALSE"; //$NON-NLS-1$

			if (maxWords > 0) {
				cmd += ", max.words=" + maxWords; //$NON-NLS-1$
			}
			else {
				cmd += ", max.words=Inf"; //$NON-NLS-1$
			}

			if (minFreq > 0) {
				cmd += ", min.freq=" + minFreq; //$NON-NLS-1$
			}

			if (rotPer >= 0 && rotPer <= 1) {
				cmd += ", rot.per=" + rotPer; //$NON-NLS-1$
			}
			cmd += ");"; //$NON-NLS-1$
			// System.out.println("ploting...");
			// rw.plot(svgFile, cmd, device);

			this.chartsEngine.plot(file, cmd);


			// System.out.println("cleaning");
			rw.eval("rm(tmplabels)"); //$NON-NLS-1$
			rw.eval("rm(tmpfreqs)"); //$NON-NLS-1$
		}
		catch (RWorkspaceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return file;
	}

	@Override
	public Class<WordCloud> getResultDataClass() {
		return WordCloud.class;
	}



}
