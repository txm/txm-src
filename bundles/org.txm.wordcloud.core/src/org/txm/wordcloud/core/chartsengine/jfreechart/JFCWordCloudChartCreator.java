package org.txm.wordcloud.core.chartsengine.jfreechart;

import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeriesCollection;
import org.txm.chartsengine.jfreechart.core.JFCChartCreator;
import org.txm.wordcloud.core.functions.WordCloud;

public class JFCWordCloudChartCreator extends JFCChartCreator<WordCloud> {

	@Override
	public JFreeChart createChart(WordCloud result) {

		// TODO Auto-generated method stub
		System.err.println("JFCWordCloudChartCreator.createChart(): Not yet implemented"); //$NON-NLS-1$


		JFreeChart chart = null;

		XYSeriesCollection dataset = new XYSeriesCollection();

		// XYSeries series = new XYSeries("test wordcloud");
		// dataset.addSeries(series);

		String[] xAxisSymbols = new String[0];


		// for (int i = 0 ; i < 0 ; i++) {
		// //series.add(i, Math.random());
		//
		// // Add X axis symbol
		// xAxisSymbols[i] = "test " + i;
		// }

		// Create chart title
		String title = "";

		// Create the chart
		chart = this.getChartsEngine().createXYBarChart(dataset, title, "", "", false, true, false, xAxisSymbols);  //$NON-NLS-1$
		// ((IItemSelectionRenderer) chart.getXYPlot().getRenderer()).setChartType(ChartsEngine.CHART_TYPE_PARTITION_DIMENSIONS);

		chart.getPlot().setNoDataMessage("JFCWordCloudChartCreator.createChart(): Not yet implemented in JFreeChart charts engine."); //$NON-NLS-1$


		// Custom range axis for ticks drawing options
		// chart.getXYPlot().setRangeAxis(new ExtendedNumberAxis((NumberAxis) chart.getXYPlot().getRangeAxis(), false, true, 0,
		// DatasetUtilities.findMaximumRangeValue(chart.getXYPlot().getDataset()).doubleValue()));

		// this.getChartsEngine().getTheme().apply(chart);


		return chart;

	}

	@Override
	public Class<WordCloud> getResultDataClass() {
		return WordCloud.class;
	}



}
