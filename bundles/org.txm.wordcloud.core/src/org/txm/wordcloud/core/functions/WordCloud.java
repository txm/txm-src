package org.txm.wordcloud.core.functions;

import java.io.File;
import java.util.List;

import org.txm.chartsengine.core.results.ChartResult;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.index.core.functions.Index;
import org.txm.index.core.functions.Line;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;
import org.txm.wordcloud.core.messages.WordCloudCoreMessages;
import org.txm.wordcloud.core.preferences.WordCloudPreferences;

/**
 * WordCloud computes word clouds using the "wordcloud" R package and an Index result
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class WordCloud extends ChartResult {


	private int[] freqs;

	private String[] labels;


	/**
	 * Minimum frequency.
	 */
	@Parameter(key = TXMPreferences.F_MIN)
	protected int fMin;

	/**
	 * 
	 */
	@Parameter(key = TXMPreferences.V_MAX)
	protected int maxWordsCount;

	/**
	 * 
	 */
	@Parameter(key = WordCloudPreferences.ROTATION_PERCENT, type = Parameter.RENDERING)
	protected float rotationPercent;

	/**
	 * 
	 */
	@Parameter(key = WordCloudPreferences.RANDOM_POSITIONS, type = Parameter.RENDERING)
	protected boolean randomPositions;



	/**
	 * Creates a not computed words cloud.
	 * 
	 * @param parent Index
	 */
	public WordCloud(Index parent) {
		super(parent);
	}

	/**
	 * Creates a not computed words cloud
	 * 
	 * @param parametersNodePath
	 */
	public WordCloud(String parametersNodePath) {
		super(parametersNodePath);
	}


	/**
	 * 
	 * @param index
	 */
	private void initWith(Index index) {
		List<Line> lines = index.getAllLines();
		this.freqs = new int[lines.size()];
		this.labels = new String[lines.size()];

		int i = 0;
		for (Line line : lines) {
			freqs[i] = line.getFrequency();
			labels[i] = line.toString("_"); //$NON-NLS-1$
			i++;
		}
	}


	@Override
	protected boolean __compute(TXMProgressMonitor monitor) {
		// // FIXME: source must always be an Index because the corpus source produces an Index
		// // from corpus
		// if (source instanceof Corpus) {
		// try {
		// Corpus corpus = (Corpus) source;
		// Query query;
		// String lang = corpus.getLang();
		// if (corpus.getProperty(lang + "pos") != null) {
		// query = new Query(getQueryForLang(lang));
		// }
		// else {
		// query = new Query("[]");
		// }
		//
		// System.out.println("Used query: " + query);
		//
		// ArrayList<Property> props = new ArrayList<Property>();
		// props.add(corpus.getWordProperty());
		// Index index = new Index(corpus);
		// index.setParameters(query, props, null, null, null, null);
		// if (!query.toString().equals("[]")) {
		// corpus.addChild(index);
		// }
		//
		// index.compute(false, monitor);
		//
		// index.addChild(this);
		//
		// List<Line> lines = index.getAllLines();
		// if (lines != null && lines.size() > 0) {
		// this.initWith(index);
		// }
		// else {
		// Log.severe("Error: Could not build a word selection with this corpus");
		// throw new IllegalArgumentException("Error: Index failed with corpus " + corpus + " with lang " + lang + " and query " + query);
		// }
		// }
		// catch(Exception e) {
		// System.out.println("Error: "+e.getLocalizedMessage());
		// Log.printStackTrace(e);
		// return false;
		// }
		this.initWith((Index) parent);
		// }
		// // from index
		// else if(this.source instanceof Index) {
		// this.initWith((Index) this.source);
		// return true;
		// }
		//
		return true;
	}

	/**
	 * Check and init R packages.
	 * 
	 * @return true if all packages are installed and initialized
	 */
	// FIXME: to factorize and move to RWorkspace
	public static boolean initPackages() {
		String[] packages = { "Rcpp", "RColorBrewer", "wordcloud" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		if (!loadAndInstallNeededPackages(packages)) {
			System.out.println(WordCloudCoreMessages.info_aborting);
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param packages
	 * @return true if packages are installed and initialized
	 */
	// FIXME: to factorize and move to RWorkspace
	public static boolean loadAndInstallNeededPackages(String[] packages) {
		boolean installFailed = false;
		for (String p : packages) {
			try {
				Log.fine(WordCloudCoreMessages.bind(WordCloudCoreMessages.info_loadingTheP0RPackage, p));
				RWorkspace.getRWorkspaceInstance().loadPackage(p);
			}
			catch (Exception e) {
				// Log.severe(Messages.bind(Messages.ComputeWordCloud_3,p,e));
				System.out.println(WordCloudCoreMessages.bind(WordCloudCoreMessages.info_installingTheP0RPackage, p));
				try {
					RWorkspace.getRWorkspaceInstance().eval("install.packages(\"" + p + "\", dependencies=TRUE, repos=\"http://cran.rstudio.com\");"); //$NON-NLS-1$ //$NON-NLS-2$
					RWorkspace.getRWorkspaceInstance().loadPackage(p);
				}
				catch (Exception e2) {
					Log.warning(WordCloudCoreMessages.bind(WordCloudCoreMessages.error_couldNotInstallTheP0RPackageP1, p));
					Log.printStackTrace(e2);
					installFailed = true;
				}
			}
		}

		if (installFailed) {
			Log.severe(WordCloudCoreMessages.error_rPackagesInstallationFailed);
			return false;
		}
		else {
			Log.fine(WordCloudCoreMessages.info_rPackagesLoaded);
			return true;
		}
	}

	/**
	 * 
	 * @param rw
	 * @param p
	 * @throws RWorkspaceException
	 */

	/**
	 * @return the freqs
	 */
	public int[] getFreqs() {
		return freqs;
	}

	/**
	 * @return the labels
	 */
	public String[] getLabels() {
		return labels;
	}

	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub
	}

	@Override
	public String getName() {
		return this.getSimpleName();
	}

	@Override
	public String getSimpleName() {
		return "Fmin=" + this.fMin + " Vmax=" + this.maxWordsCount; //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Override
	public String getDetails() {
		return this.getSimpleName();
	}


	@Override
	public boolean canCompute() {
		return this.parent != null;
	}


	/**
	 * 
	 * @param lang
	 * @return
	 */
	public static String getQueryForLang(String lang) {
		
		if (lang == null) {
			return ".*";//$NON-NLS-1$
		} else if (lang.startsWith("fr")) { //$NON-NLS-1$
			return "[frpos=\"NOM|NAM|ADJ|VER.*\"  & frlemma!=\"être|avoir|faire|pouvoir|devoir|vouloir|falloir|aller|dire|savoir\"]"; // requete de Serge pour le français TT //$NON-NLS-1$
		}
		else if (lang.startsWith("en")) { //$NON-NLS-1$
			return "[enpos=\"N.*|JJ.*|V.*\"  & enlemma!=\"be|have|do|say|go|make|other\"]"; // requete de Serge pour l'anglais TT //$NON-NLS-1$
		}
		else {
			return ".*";//$NON-NLS-1$
		}
	}

	@Override
	public boolean loadParameters() throws Exception {
		// nothing to do
		return true;
	}

	@Override
	public boolean saveParameters() throws Exception {
		// nothing to do
		return true;
	}

	/**
	 * 
	 * @return
	 */
	public int getFMin() {
		return fMin;
	}

	/**
	 * @return the maxWordsCount
	 */
	public int getMaxWordsCount() {
		return maxWordsCount;
	}

	/**
	 * @param maxWordsCount the maxWordsCount to set
	 */
	public void setMaxWordsCount(int maxWordsCount) {
		this.maxWordsCount = maxWordsCount;
	}

	/**
	 * @return the rotationPercent
	 */
	public float getRotationPercent() {
		return rotationPercent;
	}

	/**
	 * @param rotationPercent the rotationPercent to set
	 */
	public void setRotationPercent(float rotationPercent) {
		this.rotationPercent = rotationPercent;
	}

	/**
	 * @return the randomPositions
	 */
	public boolean isRandomPositions() {
		return randomPositions;
	}

	/**
	 * @param randomPositions the randomPositions to set
	 */
	public void setRandomPositions(boolean randomPositions) {
		this.randomPositions = randomPositions;
	}

	/**
	 * @param fMin the fMin to set
	 */
	public void setFMin(int fMin) {
		this.fMin = fMin;
	}

	@Override
	public String getResultType() {
		return WordCloudCoreMessages.RESULT_TYPE;
	}
}
