package org.txm.wordcloud.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Word cloud core messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class WordCloudCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.wordcloud.core.messages.messages"; //$NON-NLS-1$

	public static String RESULT_TYPE;

	public static String info_aborting;

	public static String error_couldNotInstallTheP0RPackageP1;

	public static String info_installingTheP0RPackage;

	public static String info_loadingTheP0RPackage;

	public static String error_rPackagesInstallationFailed;

	public static String info_rPackagesLoaded;

	public static String info_openingWordCloudResult;


	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, WordCloudCoreMessages.class);
	}

	private WordCloudCoreMessages() {
	}
}
