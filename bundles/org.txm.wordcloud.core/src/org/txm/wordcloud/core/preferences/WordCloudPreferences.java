package org.txm.wordcloud.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Preferences initializer and manager.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class WordCloudPreferences extends ChartsEnginePreferences {

	public static final String ROTATION_PERCENT = "rotation_percent"; //$NON-NLS-1$

	public static final String RANDOM_POSITIONS = "random_positions"; //$NON-NLS-1$

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences preferences = getDefaultPreferencesNode();
		preferences.putInt(V_MAX, 50);
		preferences.putInt(F_MIN, 20);
		preferences.putInt(ROTATION_PERCENT, 10);
		preferences.putBoolean(RANDOM_POSITIONS, false);

		// disable unavailable functionalities
		TXMPreferences.setEmpty(this.getPreferencesNodeQualifier(), SHOW_TITLE);
		TXMPreferences.setEmpty(this.getPreferencesNodeQualifier(), SHOW_LEGEND);
		TXMPreferences.setEmpty(this.getPreferencesNodeQualifier(), SHOW_GRID);
		TXMPreferences.setEmpty(this.getPreferencesNodeQualifier(), RENDERING_COLORS_MODE);
		TXMPreferences.setEmpty(this.getPreferencesNodeQualifier(), MULTIPLE_LINE_STROKES);
	}

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(WordCloudPreferences.class)) {
			new WordCloudPreferences();
		}
		return TXMPreferences.instances.get(WordCloudPreferences.class);
	}
}
