package org.txm.chartsengine.jfreechart.rcp.testers;

import org.eclipse.core.expressions.PropertyTester;


/**
 * 
 * Tests if an editor composite is a JFCComposite.
 * 
 * @author sjacquot
 *
 */
// FIXME: the test does not work...
public class __IsJFCComposite extends PropertyTester {

	public static final String PROPERTY_NAMESPACE = "org.txm.chartsengine.jfreechart.rcp.testers"; //$NON-NLS-1$

	public static final String PROPERTY_EXPERT_ENABLED = "IsJFCComposite"; //$NON-NLS-1$

	@Override
	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {

		System.out.println("IsJFCComposite.test() recevier = " + receiver.getClass()); //$NON-NLS-1$
		System.out.println("IsJFCComposite.test() property = " + property); //$NON-NLS-1$
		System.out.println("IsJFCComposite.test() args = " + args); //$NON-NLS-1$
		System.out.println("IsJFCComposite.test() expectedValue = " + expectedValue); //$NON-NLS-1$

		// ChartEditor editor = SWTChartsComponentsProvider.getActiveChartEditor(null);
		// return (editor != null && editor.getComposite() instanceof JFCComposite);
		return true;
	}
}
