/**
 * 
 */
package org.txm.chartsengine.jfreechart.rcp.themes.highcharts;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.txm.chartsengine.jfreechart.core.JFCChartsEngine;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.HighchartsDefaultTheme;
import org.txm.chartsengine.jfreechart.rcp.themes.highcharts.swing.CustomChartPanel;



/**
 * 
 * 
 * @author sjacquot
 *
 */
public class HighchartsThemeSwing extends HighchartsDefaultTheme {

	/**
	 * 
	 */
	public HighchartsThemeSwing(JFCChartsEngine chartsEngine) {
		super(chartsEngine);
	}


	@Override
	public ChartPanel createChartPanel(JFreeChart chart) {
		return new CustomChartPanel(chart);
	}



}
