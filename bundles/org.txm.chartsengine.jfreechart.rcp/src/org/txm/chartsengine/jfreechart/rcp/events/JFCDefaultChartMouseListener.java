/**
 *
 */
package org.txm.chartsengine.jfreechart.rcp.events;

import java.awt.event.MouseEvent;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.entity.CategoryItemEntity;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.JFreeChartEntity;
import org.jfree.chart.entity.PlotEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.xy.XYSeriesCollection;
//import org.txm.ca.core.chartsengine.jfreechart.datasets.CAXYDataset;
import org.txm.chartsengine.jfreechart.core.renderers.MultipleItemsSelector;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.chartsengine.rcp.IChartComponent;
import org.txm.chartsengine.rcp.events.AbstractChartMouseListener;
import org.txm.chartsengine.rcp.events.EventCallBack;
import org.txm.chartsengine.rcp.events.EventCallBackHandler;
import org.txm.utils.logger.Log;

/**
 * Default JFC chart engine mouse listener.
 * 
 * @author sjacquot
 *
 */
public class JFCDefaultChartMouseListener extends EventCallBackHandler implements ChartMouseListener {



	/**
	 * @param chartComponent
	 */
	public JFCDefaultChartMouseListener(IChartComponent chartComponent) {
		super(chartComponent);
	}

	@Override
	public void chartMouseClicked(final ChartMouseEvent event) {

		ChartEntity entity = event.getEntity();

		int eventArea = -1;


		String menuText = ""; //$NON-NLS-1$

		Plot plot = ((ChartPanel) this.chartComponent).getChart().getPlot();

		IRendererWithItemSelection renderer = null;

		if (plot instanceof XYPlot) {
			renderer = (IRendererWithItemSelection) ((XYPlot) plot).getRenderer();
		}
		else if (plot instanceof CategoryPlot) {
			renderer = (IRendererWithItemSelection) ((CategoryPlot) plot).getRenderer();
		}


		// Select/deselect item
		if (entity instanceof XYItemEntity || entity instanceof CategoryItemEntity) {

			int series = -1;
			int item = -1;

			eventArea = EventCallBack.AREA_ITEM;

			// Single click
			if (event.getTrigger().getClickCount() == 1) {

				// Multiple item selector
				if (renderer.getItemsSelector() instanceof MultipleItemsSelector) {

					MultipleItemsSelector itemsSelector = (MultipleItemsSelector) renderer.getItemsSelector();

					// XY plot
					if (entity instanceof XYItemEntity) {
						XYItemEntity itemEntity = (XYItemEntity) entity;
						series = itemEntity.getSeriesIndex();
						item = itemEntity.getItem();
					}
					// Category plot
					else if (entity instanceof CategoryItemEntity) {
						CategoryItemEntity itemEntity = (CategoryItemEntity) entity;
						CategoryDataset dataset = itemEntity.getDataset();
						series = dataset.getRowIndex(itemEntity.getRowKey());
						item = dataset.getColumnIndex(itemEntity.getColumnKey());

						menuText = (String) dataset.getColumnKeys().get(series);
					}


					int onmask = MouseEvent.BUTTON1_MASK;
					int mods = event.getTrigger().getModifiers();

					// Simple selection mode
					if (itemsSelector.isSimpleSelectionMode()
							&& (mods & onmask) == onmask) {
						itemsSelector.removeAllSelectedItems(); // clear the selection
						itemsSelector.setSelectedItem(series, item, !itemsSelector.isSelectedItem(series, item));

					}
					// Multiple selection mode
					else if ((mods & AbstractChartMouseListener.keyboardExtendedSelectionModifierKeyMask) == 0) {

						int offmask = AbstractChartMouseListener.mouseMultipleSelectionModifierKeyMask;

						if ((mods & (onmask | offmask)) == onmask) {
							itemsSelector.removeAllSelectedItems(series);
						}
						if ((mods & onmask) == onmask) {
							itemsSelector.setSelectedItem(series, item, !itemsSelector.isSelectedItem(series, item));
						}
						// right click, do not clear the current selection if the click occurred in the current selection
						if ((mods & MouseEvent.BUTTON3_MASK) == MouseEvent.BUTTON3_MASK) {
							if (!itemsSelector.isSelectedItem(series, item)) {
								itemsSelector.removeAllSelectedItems(series);
							}
							itemsSelector.setSelectedItem(series, item, true);
						}

					}
					// Extended selection by series
					else if (itemsSelector.getLastSelectedItem() != -1 && itemsSelector.getLastSelectedSeries() == series) {
						// Ascendant
						if (itemsSelector.getLastSelectedItem() < item) {
							for (int i = itemsSelector.getLastSelectedItem(); i < item; i++) {
								itemsSelector.selectNextItem(true);
							}
						}
						// Descendant
						else {
							for (int i = itemsSelector.getLastSelectedItem(); i >= item + 1; i--) {
								itemsSelector.selectPreviousItem(true);
							}
						}
					}

					// FIXME: this code must be extracted in each plugin (CA, Progression, etc.) and stored in some EventCallBack extensions
					// update text menu according to the current selection
					if (renderer.getItemsSelector() instanceof MultipleItemsSelector && ((MultipleItemsSelector) renderer.getItemsSelector()).getSelectedSeriesCount() > 0) {
						// XY plot
						if (entity instanceof XYItemEntity itemEntity) {

							XYPlot xyPlot = (XYPlot) plot;
							MultipleItemsSelector selector = (MultipleItemsSelector) renderer.getItemsSelector();
							int selectedSeries;
							for (int i = 0; i < selector.getSelectedSeriesCount(); i++) {

								// FIXME: this case should never happen but it happens since 0.8.0_alpha. Need to check why the selected series is not well removed in the selector
								if (selector.getSelectedSeries()[i] == null) {
									continue;
								}

								selectedSeries = selector.getSelectedSeries()[i];
								int[] selectedItems = selector.getSelectedItems(selectedSeries);
								for (int j = 0; j < selectedItems.length; j++) {
									// Symbol Axis
									if (xyPlot.getDomainAxis() instanceof SymbolAxis symbolAxis) {
										String[] symbols = symbolAxis.getSymbols();
										menuText += symbolAxis.getSymbols()[selectedItems[j]] + "\n"; //$NON-NLS-1$
									}
									// CA
									// FIXME move this to CA.core/rcp ?
									// else if(itemEntity.getDataset() instanceof CAXYDataset) {
									// menuText += ((CAXYDataset)((XYPlot)plot).getDataset()).getLabel(selectedSeries, selectedItems[j]);
									// }
									// Progression
									else if (itemEntity.getDataset() instanceof XYSeriesCollection) {
										menuText += ((XYSeriesCollection) itemEntity.getDataset()).getSeriesKey(selectedSeries);
										menuText += " / "; //$NON-NLS-1$
										break;
									}
									else {
										menuText += String.valueOf(itemEntity.getDataset().getXValue(selectedSeries, selectedItems[j]));
									}

									menuText += " / "; //$NON-NLS-1$
								}
							}
							// remove the last separator
							menuText = menuText.substring(0, menuText.length() - 3);
							// trim the entry
							if (menuText.length() > 30) {
								menuText = menuText.substring(0, 30);
								menuText += "..."; //$NON-NLS-1$
							}

						}
					}
					// End of FIXME
				}
			}
		}
		// Empty area
		else if (entity instanceof PlotEntity || entity instanceof JFreeChartEntity) {
			eventArea = EventCallBack.AREA_EMPTY;
		}
		// FIXME: SJ: force empty area event type for all non item area (later we need to pass legend item type, title item type, etc.)
		else {
			eventArea = EventCallBack.AREA_EMPTY;
		}



		// FIXME: Debug
		// System.err.println("DefaultChartMouseListener.chartMouseClicked(): ");
		// System.out.println("DefaultChartMouseListener.chartMouseClicked(): call backs count: " + eventCallBacks.size());
		// System.err.println("DefaultChartMouseListener.chartMouseClicked(): click count: " + event.getTrigger().getClickCount());
		Log.finest("JFCDefaultChartMouseListener.chartMouseClicked(): entity type: " + entity); //$NON-NLS-1$
		Log.finest("JFCDefaultChartMouseListener.chartMouseClicked(): eventArea: " + eventArea); //$NON-NLS-1$


		// Set the composite current menu
		final int a = eventArea;
		final String t = menuText;

		this.chartComponent.getChartEditor().getComposite().getDisplay().syncExec(new Runnable() {

			@Override
			public void run() {
				chartComponent.getChartEditor().getComposite().setCurrentContextMenu(a);

				if (!t.isEmpty() && chartComponent.getChartEditor().getComposite().getMenu() != null 
						&& chartComponent.getChartEditor().getComposite().getMenu().getItemCount() > 0
						&& chartComponent.getChartEditor().getComposite().getMenu().getItem(0).getText().equals("DYNLABEL")) { // ENSURE this is the right menu item //$NON-NLS-1$

					// FIXME: Debug
					Log.finest("JFCDefaultChartMouseListener.chartMouseClicked(...).new Runnable() {...}.run(): menu = " + chartComponent.getChartEditor().getComposite().getMenu()); //$NON-NLS-1$

					chartComponent.getChartEditor().getComposite().getMenu().getItem(0).setText(t);
				}

				// // FIXME: SJ: temporary workaround to fix the popup menu visibility bug on Linux/Gnome (see: http://forge.cbp.ens-lyon.fr/redmine/issues/1519)
				// if(
				// //OSDetector.isFamilyUnix() &&
				// event.getTrigger().getButton() == MouseEvent.BUTTON3
				//
				// //event.getTrigger().isPopupTrigger()
				// ) {
				// chartComponent.getChartEditor().getComposite().getMenu().setVisible(true);
				// Log.finest("DefaultChartMouseListener.chartMouseClicked(...).new Runnable() {...}.run(): special menu visibility Linux/Gnome bug fix.");
				// }
				//
			}
		});


		// Process event call backs
		for (int i = 0; i < this.eventCallBacks.size(); i++) {
			this.eventCallBacks.get(i).processEvent(event.getTrigger(), eventArea, event);
		}



	}

	@Override
	public void chartMouseMoved(ChartMouseEvent event) {

		// Mouse over selection
		ChartEntity entity = event.getEntity();


		// TODO: SJ: Debug
		//System.out.println("JFCDefaultChartMouseListener.chartMouseMoved(): " + entity);


		Plot plot = ((ChartPanel) this.chartComponent).getChart().getPlot();

		IRendererWithItemSelection renderer = null;

		if (plot instanceof XYPlot) {
			renderer = (IRendererWithItemSelection) ((XYPlot) plot).getRenderer();
		}
		else if (plot instanceof CategoryPlot) {
			renderer = (IRendererWithItemSelection) ((CategoryPlot) plot).getRenderer();
		}

		int series = -1;
		int item = -1;

		// Mouse over, storing the current selected entity on mouse over
		if (entity instanceof XYItemEntity || entity instanceof CategoryItemEntity) {

			// XY plot
			if (entity instanceof XYItemEntity) {
				XYItemEntity itemEntity = (XYItemEntity) entity;
				series = itemEntity.getSeriesIndex();
				item = itemEntity.getItem();
			}
			// Category plot
			else if (entity instanceof CategoryItemEntity) {
				CategoryItemEntity itemEntity = (CategoryItemEntity) entity;
				CategoryDataset dataset = itemEntity.getDataset();
				series = dataset.getRowIndex(itemEntity.getRowKey());
				item = dataset.getColumnIndex(itemEntity.getColumnKey());
			}

			// Storing shape coordinates
			renderer.getItemsSelector().setMouseOverItemCoords(entity.getArea().getBounds().x, entity.getArea().getBounds().y);


		}
		if (renderer != null && renderer.getItemsSelector() != null) {
			renderer.getItemsSelector().setMouseOverItem(series, item);
		}

	}

}
