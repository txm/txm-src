package org.txm.chartsengine.jfreechart.rcp.themes.highcharts.swing;

import java.awt.Color;

import javax.swing.JToolTip;

import org.jfree.chart.JFreeChart;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.swing.CustomHTMLToolTip;
import org.txm.chartsengine.jfreechart.rcp.themes.base.swing.ItemSelectionJFCChartPanel;

/**
 * Chart panel providing custom tool tips with Highcharts graphics theme.
 * 
 * @author sjacquot, mdcorde
 *
 */
public class CustomChartPanel extends ItemSelectionJFCChartPanel {



	/**
	 *
	 * @param chart
	 */
	public CustomChartPanel(JFreeChart chart) {
		super(chart);


		// Set the zoom factors that will be used on key pressed zoom (this value is not use by the internal JFC mouse wheel zoom)
		this.setZoomInFactor(0.9);
		this.setZoomOutFactor(1.1);


		this.initItemSelectionListeners();

		this.initEventListeners();



		// Dimensions
		// chartPanel.setPreferredSize(new java.awt.Dimension(750, 550));
		// TODO : DEbug
		// chartPanel.setPreferredSize(new java.awt.Dimension(1024, 768));

		// TODO : test this to keep fonts readable despite of window size
		// chartPanel.setMinimumDrawWidth(400);
		this.setMaximumDrawWidth(100000);
		// chartPanel.setMinimumDrawHeight(400);
		this.setMaximumDrawHeight(100000);


		// this.setHorizontalAxisTrace(true);

		// Mouse wheel zoom
		this.setMouseWheelEnabled(true);


		// FIXME: test mouse wheel listener for improving the zoom, for sticking axes to the chart
		// this.removeMouseWheelListener(this.getMouseWheelListeners()[0]);
		// this.addMouseWheelListener(new MouseWheelListener() {
		//
		// @Override
		// public void mouseWheelMoved(MouseWheelEvent e) {
		// // TODO Auto-generated method stub
		// if(getChart().getPlot() instanceof XYPlot) {
		// //System.out.println("CustomChartPanel.CustomChartPanel(...).new MouseWheelListener() {...}.mouseWheelMoved(): " + getChart().getXYPlot().getDomainAxisEdge().getInsets());
		//
		//
		// //getChart().getXYPlot().setAxisOffset(new RectangleInsets(0, 0, 0, 0));
		// //((NumberAxis) getChart().getXYPlot().getDomainAxis()).setAutoRangeStickyZero(true);
		// getChart().getXYPlot().getDomainAxis().setTickLabelInsets(new RectangleInsets(10, 0, getChart().getXYPlot().getDomainAxis().getTickLabelInsets().getBottom() + 10 * getZoomOutFactor(), 0));
		// }
		// }
		// });



		// this.setMouseZoomable(true); // useless ?
		this.setZoomOutlinePaint(Color.BLUE);
		this.setFillZoomRectangle(false); // TODO : ne fonctionne pas ? En fait ne fonctionne pas si l'appel est avant setMouseZoomable(true) ?


		// Tooltips
		this.setInitialDelay(500);

		// FIXME MD: for #3082 disable tooltips in Ubuntu -> tooltips make the SWT interface flick/blink
		this.setDisplayToolTips(true);

		// Disable the Swing original pop up menu
		this.setPopupMenu(null);
	}

	@Override
	public JToolTip createToolTip() {
		JToolTip tip = new CustomHTMLToolTip(this.getChart(), true);
		// TODO
		tip.setComponent(this);
		tip.setVisible(true);
		return tip;
	}


}
