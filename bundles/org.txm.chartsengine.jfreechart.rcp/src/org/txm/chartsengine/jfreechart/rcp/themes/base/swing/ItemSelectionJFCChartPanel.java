/**
 *
 */
package org.txm.chartsengine.jfreechart.rcp.themes.base.swing;

import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Pannable;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.txm.chartsengine.jfreechart.core.JFCChartsEngine;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.chartsengine.jfreechart.rcp.events.JFCDefaultChartMouseListener;
import org.txm.chartsengine.jfreechart.rcp.themes.highcharts.events.JFCItemsKeyboardSelectionCallBack;
import org.txm.chartsengine.rcp.IChartComponent;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.events.AbstractChartMouseListener;
import org.txm.chartsengine.rcp.events.ChartKeyListener;
import org.txm.utils.OSDetector;


/**
 * JFC Chart panel that manage chart entities selection.
 * 
 * @author sjacquot
 *
 */
public class ItemSelectionJFCChartPanel extends ChartPanel implements IChartComponent {


	/**
	 * Default modifier key mask used for pan by JFC chart panel.
	 */
	public static int defaultPanMouseModifierKeyMask = MouseEvent.CTRL_DOWN_MASK; // Default mask for Windows and Linux


	/**
	 * Define the modifier keys for Mac Os X.
	 */
	static {
		if (OSDetector.isFamilyMac()) {
			defaultPanMouseModifierKeyMask = MouseEvent.ALT_DOWN_MASK;
		}
	}


	/**
	 * The parent editor.
	 */
	protected ChartEditor<?> editor;


	/**
	 * @param chart
	 */
	public ItemSelectionJFCChartPanel(JFreeChart chart) {
		super(chart);
	}


	@Override
	public void resetView() {
		this.zoomInBoth(20, 20); // SJ: without that restoreAutoBounds() sometimes doesn't work...
		this.restoreAutoBounds();
	}


	@Override
	public void zoom(double x, double y, boolean zoomIn, boolean range, boolean domain) {
		
		if (range && domain) {
			if (zoomIn) {
				this.zoomInBoth(x, y);
			}
			else {
				this.zoomOutBoth(x, y);
			}
		}
		else if (range) {
			if (zoomIn) {
				
				this.zoomInRange(x, y);
				
				//FIXME: SJ: tests to centering zoom on an axis 
		        //this.setZoomAroundAnchor(true);
				//ChartRenderingInfo info = this.getChartRenderingInfo();
				//Rectangle2D dataArea = info.getPlotInfo().getDataArea();
		        //y = this.getChart().getXYPlot().getRangeAxis().valueToJava2D(this.getChart().getXYPlot().getRangeAxis().getLowerBound(), dataArea, this.getChart().getXYPlot().getRangeAxisEdge());
				//y = this.getChart().getXYPlot().getRangeAxis().valueToJava2D(0, dataArea, this.getChart().getXYPlot().getRangeAxisEdge());
				//this.getChart().getXYPlot().getDomainAxis().setAutoRange(false);
				//y = 500;
				//System.out.println("ItemSelectionJFCChartPanel.zoom() " + y);
				//this.zoomInRange(x, 500);
				
			}
			else {
				this.zoomOutRange(x, y);
				
				//FIXME: SJ: tests to centering zoom on an axis. Tests on partition dimensions.
//				this.setZoomAroundAnchor(true);
//				this.zoomOutRange(x, 500);
			}
		}
		else if (domain) {
			if (zoomIn) {
				this.zoomInDomain(x, y);
			}
			else {
				this.zoomOutDomain(x, y);
			}
		}
	}


	@Override
	public void zoomInRange(double x, double y) {
		// manage the plot orientation
		if(this.getPlotOrientation() == PlotOrientation.HORIZONTAL) {
			super.zoomInDomain(x, y);
		}
		else {
			super.zoomInRange(x, y);	
		}
		

		// FIXME: SJ: tests to store the zoom for future persistence
		// if(this.getChart().getPlot() instanceof XYPlot) {
		// XYPlot plot = this.getChart().getXYPlot();
		// System.out.println("ItemSelectionJFCChartPanel.zoomInBoth() range axis lower bound: " + plot.getRangeAxis().getLowerBound());
		// System.out.println("ItemSelectionJFCChartPanel.zoomInBoth() range axis upper bound: " + plot.getRangeAxis().getUpperBound());
		// }
	}

	@Override
	public void zoomOutRange(double x, double y) {
		// manage the plot orientation
		if(this.getPlotOrientation() == PlotOrientation.HORIZONTAL) {
			super.zoomOutDomain(x, y);
		}
		else {
			super.zoomOutRange(x, y);	
		}


		// FIXME: SJ: tests to store the zoom for future persistence
		// if(this.getChart().getPlot() instanceof XYPlot) {
		// XYPlot plot = this.getChart().getXYPlot();
		// System.out.println("ItemSelectionJFCChartPanel.zoomInBoth() range axis lower bound: " + plot.getRangeAxis().getLowerBound());
		// System.out.println("ItemSelectionJFCChartPanel.zoomInBoth() range axis upper bound: " + plot.getRangeAxis().getUpperBound());
		// }

	}

	
	@Override
	public void zoomInDomain(double x, double y) {
		// manage the plot orientation
		if(this.getPlotOrientation() == PlotOrientation.HORIZONTAL) {
			super.zoomInRange(x, y);
		}
		else {
			super.zoomInDomain(x, y);	
		}
	}

	
	@Override
	public void zoomOutDomain(double x, double y) {
		// manage the plot orientation
		if(this.getPlotOrientation() == PlotOrientation.HORIZONTAL) {
			super.zoomOutRange(x, y);
		}
		else {
			super.zoomOutDomain(x, y);	
		}
	}


	/**
	 * Gets the plot orientation.
	 * 
	 * @return the plot orientation
	 */
	PlotOrientation getPlotOrientation() {
		PlotOrientation plotOrientation = PlotOrientation.VERTICAL;
		if(this.getChart().getPlot() instanceof Pannable) {
			plotOrientation =  ((Pannable) this.getChart().getPlot()).getOrientation();
		}
		return plotOrientation;
	}
	
	
	
	@Override
	public void pan(double srcX, double srcY, double dstX, double dstY, double panFactor) {

		Pannable p = (Pannable) this.getChart().getPlot();

		if (dstX < 0) {
			p.panDomainAxes(-panFactor, null, new Point2D.Double(0, 0));
		}
		else if (dstX > 0) {
			p.panDomainAxes(panFactor, null, new Point2D.Double(0, 0));
		}
		else if (dstY < 0) {
			p.panRangeAxes(-panFactor, null, new Point2D.Double(0, 0));
		}
		else if (dstY > 0) {
			p.panRangeAxes(panFactor, null, new Point2D.Double(0, 0));
		}
	}


	/**
	 * Initialize event listeners.
	 */
	public void initEventListeners() {
		ChartKeyListener chartKeyListener = new ChartKeyListener(this, 0.009);
		// register default items keyboard selection call back
		chartKeyListener.registerEventCallBack(new JFCItemsKeyboardSelectionCallBack());

		this.addKeyListener(chartKeyListener);
	}


	/**
	 * Initializes the listeners for items selection support.
	 */
	public void initItemSelectionListeners() {
		// Listeners according to the plot type
		if (this.getChart() != null) {
			Plot plot = this.getChart().getPlot();
			// XY plots
			if (plot instanceof XYPlot && ((XYPlot) plot).getRenderer() instanceof IRendererWithItemSelection) {
				this.addChartMouseListener(new JFCDefaultChartMouseListener(this));
			}
			// Category plot
			else if (plot instanceof CategoryPlot && ((CategoryPlot) plot).getRenderer() instanceof IRendererWithItemSelection) {
				this.addChartMouseListener(new JFCDefaultChartMouseListener(this));
			}
		}
	}


	@Override
	public void mousePressed(MouseEvent event) {

		// Simulate a mouse event to enable the pan with right mouse button + drag rather than the original behavior
		// FIXME: SJ: we need to redefine ChartPanel to do that in a better way and/or to add other functionalities later
		int mods = event.getModifiers();
		if ((mods & AbstractChartMouseListener.mouseMultipleSelectionModifierKeyMask) != AbstractChartMouseListener.mouseMultipleSelectionModifierKeyMask
				&& (mods & MouseEvent.BUTTON1_MASK) == MouseEvent.BUTTON1_MASK) {
			MouseEvent fakeEvent = new MouseEvent(event.getComponent(), event.getID(), event.getWhen(), defaultPanMouseModifierKeyMask, event.getX(), event.getY(), event.getClickCount(), event
					.isPopupTrigger());
			super.mousePressed(fakeEvent);
		}
		// FIXME: SJ: uncomment this code restores the scaling/zooming by rectangle selection. This code is commented because JFC do not use modifier key for
		// this but TXM specifications say to use CTRL/CMD key modifier. The best solution seems to, again, redefine the ChartPanel class.
		// else {
		// super.mousePressed(event);
		// }


		// FIXME: SJ: this bug #993#17 has only been noticed on a VM (Mac OS X 10.9.4) and may be linked to a mouse VM compatibility, that's why this code is commented at this moment
		// Fix #993#17, the points selection from the chart component does not work on Mac OS X + Java 1.6
		// Manually fire a ChartMouseEvent
		// if(System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0) {
		// ChartRenderingInfo info = this.getChartRenderingInfo();
		//
		// ChartEntity entity = null;
		// if (info != null) {
		// EntityCollection entities = info.getEntityCollection();
		// if (entities != null) {
		// entity = entities.getEntity(event.getX(), event.getY());
		// }
		// }
		// ChartMouseEvent chartEvent = new ChartMouseEvent(getChart(), event, entity);
		//
		// EventListener[] listeners = this.getListeners(ChartMouseListener.class);
		// for (int i = listeners.length - 1; i >= 0; i -= 1) {
		// ((ChartMouseListener) listeners[i]).chartMouseClicked(chartEvent);
		// }
		// }

	}

	/**
	 * Creates and returns a chart mouse event according to the specified AWT mouse event.
	 * A chart mouse event contains the selected chart entity.
	 * 
	 * @param event
	 * @return
	 */
	public ChartMouseEvent getChartMouseEvent(MouseEvent event) {
		Insets insets = getInsets();
		int x = (int) ((event.getX() - insets.left) / this.getScaleX());
		int y = (int) ((event.getY() - insets.top) / this.getScaleY());

		this.setAnchor(new Point2D.Double(x, y));
		if (this.getChart() == null) {
			return null;
		}
		this.getChart().setNotify(true);  // force a redraw

		ChartEntity entity = null;
		if (this.getChartRenderingInfo() != null) {
			EntityCollection entities = this.getChartRenderingInfo().getEntityCollection();
			if (entities != null) {
				entity = entities.getEntity(x, y);
			}
		}

		return new ChartMouseEvent(this.getChart(), event, entity);
	}

	@Override
	public void squareOff() {
		((JFCChartsEngine) this.editor.getChartsEngine()).squareOffGraph(this.getChart(), this.getWidth(), this.getHeight());
	}


	@Override
	public ChartEditor getChartEditor() {
		return this.editor;
	}

	@Override
	public void setChartEditor(ChartEditor editor) {
		this.editor = editor;
	}


	@Override
	public void updateMouseOverItem(MouseEvent event) {
		JFCDefaultChartMouseListener listener = (JFCDefaultChartMouseListener) this.editor.getComposite().getMouseCallBackHandler();
		if (listener != null) {
			// FIXME: SJ: Fire chart mouse event to mouse over select chart item before displaying the context menu
			// listener.chartMouseMoved(((ItemSelectionChartPanel)this.editor.getComposite().getChartComponent()).getChartMouseEvent(event));
			// Fire chart mouse event to select chart item before displaying the context menu
			listener.chartMouseClicked(((ItemSelectionJFCChartPanel) this.editor.getComposite().getChartComponent()).getChartMouseEvent(event));
		}
	}


}
