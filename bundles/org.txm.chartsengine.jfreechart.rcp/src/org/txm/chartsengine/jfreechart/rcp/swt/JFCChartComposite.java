package org.txm.chartsengine.jfreechart.rcp.swt;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYPlot;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.jfreechart.core.JFCChartsEngine;
import org.txm.chartsengine.jfreechart.core.renderers.MouseOverItemSelector;
import org.txm.chartsengine.jfreechart.core.renderers.MultipleItemsSelector;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.chartsengine.rcp.IChartComponent;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.events.EventCallBackHandler;
import org.txm.chartsengine.rcp.swt.SwingChartComposite;

/**
 * JFreeChart SWT composite.
 *
 * @author sjacquot
 *
 */
public class JFCChartComposite extends SwingChartComposite {


	/**
	 * Creates a JFreeChart composite.
	 *
	 * @param parent
	 */
	public JFCChartComposite(ChartEditor<? extends ChartResult> chartEditor, Composite parent) {
		super(chartEditor, parent, SWT.EMBEDDED | SWT.NO_BACKGROUND);
	}



	@Override
	public void loadChart(Object chart) {
		((ChartPanel) this.chartComponent).setChart((JFreeChart) chart);

		// FIXME: SJ: tests, restore the zoom from local node
		// this.zoom(x, y, zoomIn);
		// FIXME: SJ: tests, restoring current zoom from result node
		// if(this.getChart().getPlot() instanceof XYPlot) {
		// System.err.println("JFCComposite.loadChart(): restoring current zoom.");
		// XYPlot plot = this.getChart().getXYPlot();
		// plot.getRangeAxis().setLowerBound(-4400);
		// plot.getRangeAxis().setUpperBound(-13000);
		// }

	}


	@Override
	protected IChartComponent _createChartComponent() {

		JFreeChart chart = (JFreeChart) this.chartEditor.getChart();
		ChartPanel chartPanel = ((JFCChartsEngine) this.chartEditor.getChartsEngine()).getJFCTheme().createChartPanel(chart);

		// Store the chart panel in the renderer (eg. can be needed to get the Graphics2D object to use in getItemShape() to compute the label text bounds and draw a background rectangle.)
		if (chart.getPlot() instanceof XYPlot && chart.getXYPlot().getRenderer() instanceof IRendererWithItemSelection) {
			((IRendererWithItemSelection) chart.getXYPlot().getRenderer()).getItemsSelector().setChartPanel(chartPanel);
		}
		else if (chart.getPlot() instanceof CategoryPlot && chart.getCategoryPlot().getRenderer() instanceof IRendererWithItemSelection) {
			((IRendererWithItemSelection) chart.getCategoryPlot().getRenderer()).getItemsSelector().setChartPanel(chartPanel);
		}
		return (IChartComponent) chartPanel;
	}



	@Override
	public void clearChartItemsSelection() {
		IRendererWithItemSelection renderer = null;
		if (this.getChart().getPlot() instanceof XYPlot && this.getChart().getXYPlot().getRenderer() instanceof IRendererWithItemSelection) {
			renderer = (IRendererWithItemSelection) this.getChart().getXYPlot().getRenderer();
		}
		else if (this.getChart().getPlot() instanceof CategoryPlot && this.getChart().getCategoryPlot().getRenderer() instanceof IRendererWithItemSelection) {
			renderer = (IRendererWithItemSelection) this.getChart().getCategoryPlot().getRenderer();
		}

		if (renderer != null && renderer.getItemsSelector() instanceof MultipleItemsSelector) {
			((MultipleItemsSelector) renderer.getItemsSelector()).removeAllSelectedItems();
		}
	}



	@Override
	public EventCallBackHandler getMouseCallBackHandler() {
		return this.getCallBackHandler(ChartMouseListener.class);
	}


	@Override
	public JFreeChart getChart() {
		return (JFreeChart) super.getChart();
	}



	@Override
	public File exportView(File file, String fileFormat) {

		// Disable the item selection to not export the mouse over item modification
		MouseOverItemSelector mouseOverItemSelector = null;
		Plot plot = this.getChart().getPlot();
		if (plot instanceof XYPlot typedPlot && typedPlot.getRenderer() instanceof IRendererWithItemSelection renderer) { 
			mouseOverItemSelector = renderer.getItemsSelector();
		}
		else if (plot instanceof CategoryPlot typedPlot && typedPlot.getRenderer() instanceof IRendererWithItemSelection renderer) {
			mouseOverItemSelector = renderer.getItemsSelector();
		}
		if (mouseOverItemSelector != null) {
			mouseOverItemSelector.setActive(false);
		}

		
		if(fileFormat.equals(ChartsEngine.OUTPUT_FORMAT_SVG) || fileFormat.equals(ChartsEngine.OUTPUT_FORMAT_PDF)) {
		
			// Export the chart, keeping aspect ratio and according to the width export preference
			double aspectRatio = ((double) this.getAWTChartComponent().getHeight() / (double) this.getAWTChartComponent().getWidth());
			int outputImageWidth = ChartsEnginePreferences.getInstance().getInt(ChartsEnginePreferences.EXPORT_WIDTH_IN_PIXELS);
			double outputImageHeight = outputImageWidth * aspectRatio;
			
			file = JFCChartsEngine.export(this.getChart(), file, fileFormat, outputImageWidth, (int) outputImageHeight, this.getAWTChartComponent().getWidth(),
					this.getAWTChartComponent().getHeight());
			
		}
		// Raster parent export (using AWT/Swing methods)
		else {
			super.exportView(file, fileFormat);
		}
		
		// Enable back the item selection
		if (mouseOverItemSelector != null) {
			mouseOverItemSelector.setActive(true);
		}

		return file;
	}


	@Override
	public ArrayList<String> getEditorSupportedExportFileFormats() {
		return this.getChartsEngine().getSupportedOutputFileFormats();
	}

}
