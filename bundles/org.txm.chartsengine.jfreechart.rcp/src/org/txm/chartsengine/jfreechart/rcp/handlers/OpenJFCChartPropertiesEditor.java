package org.txm.chartsengine.jfreechart.rcp.handlers;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.jfree.chart.ChartPanel;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.jfreechart.core.JFCChartsEngine;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.messages.ChartsEngineUIMessages;
import org.txm.chartsengine.rcp.swt.ChartComposite;
import org.txm.utils.OSDetector;
import org.txm.utils.logger.Log;

/**
 * Opens the built-in JFC chart properties components.
 * 
 * @author sjacquot, mdecorde
 *
 */
public class OpenJFCChartPropertiesEditor extends AbstractHandler {


	// FIXME: SJ: warning, test needed because the L&F change crashes TXM on Linux
	static {
		if (!OSDetector.isFamilyUnix()) {
			Log.finest("OpenJFCChartPropertiesEditor.enclosing_method() setting System L&F.	"); //$NON-NLS-1$
			try {
				// Set System L&F
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			}
			catch (UnsupportedLookAndFeelException e) {
				// handle exception
			}
			catch (ClassNotFoundException e) {
				// handle exception
			}
			catch (InstantiationException e) {
				// handle exception
			}
			catch (IllegalAccessException e) {
				// handle exception
			}
		}
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		final ChartEditor<? extends ChartResult> chartEditor = SWTChartsComponentsProvider.getActiveChartEditor(event);

		if (chartEditor.getChartsEngine() instanceof JFCChartsEngine) {

			if (OSDetector.isFamilyUnix() || OSDetector.isFamilyMac()) {
				// FIXME: workaround for #3082 to open the chart editor without breaking GTK interfaces. This fix will make the dialog modal and the SWT thread blocked
				openChartProperties(chartEditor);
				return null;
			}

			chartEditor.getComposite().getDisplay().asyncExec(new Runnable() {

				@Override
				public void run() {
					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							openChartProperties(chartEditor);
						}
					});
				}
			});
		}
		else {
			Log.warning(ChartsEngineUIMessages.canNotOpenChartPropertiesInterfaceForThecurrentChartEngine);
		}

		return null;
	}

	/**
	 * Raw open the chart editor, must be wrap with SWT async and Swing.invokaLater to not block the UI
	 * 
	 * @param chartEditor
	 */
	public static void openChartProperties(ChartEditor<? extends ChartResult> chartEditor) {
		ChartComposite composite = chartEditor.getComposite();
		ChartPanel p = ((ChartPanel) composite.getChartComponent());
		p.doEditChartProperties();
		chartEditor.getResult().setAltered();
	}
}
