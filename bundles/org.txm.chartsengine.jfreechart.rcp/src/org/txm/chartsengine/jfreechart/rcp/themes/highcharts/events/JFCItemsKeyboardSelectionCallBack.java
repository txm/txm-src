/**
 * 
 */
package org.txm.chartsengine.jfreechart.rcp.themes.highcharts.events;

import java.awt.event.KeyEvent;

import org.jfree.chart.ChartPanel;
import org.txm.chartsengine.jfreechart.core.renderers.MultipleItemsSelector;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.chartsengine.jfreechart.rcp.JFCSWTChartsComponentsProvider;
import org.txm.chartsengine.rcp.events.ItemsKeyboardSelectionCallBack;
import org.txm.chartsengine.rcp.events.ZoomAndPanCallBack;

/**
 * Callback for items selection and extended selection from keyboard.
 * 
 * @author sjacquot
 *
 */
public class JFCItemsKeyboardSelectionCallBack extends ItemsKeyboardSelectionCallBack {


	/**	
	 * 
	 */
	public JFCItemsKeyboardSelectionCallBack() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.rcp.chartsengine.base.events.EventCallBack#processEvent(java.awt.AWTEvent, int, java.lang.Object)
	 */
	@Override
	public void processEvent(Object event, int eventArea, Object customData) {

		// TODO Auto-generated method stub
		// Keyboard event
		if (event instanceof KeyEvent) {


			KeyEvent keyEvent = (KeyEvent) event;

			// return if zoom and pan modifier
			if ((keyEvent.getModifiers() & ZoomAndPanCallBack.keyboardZoomModifierKeyMask) != 0) {
				return;
			}

			boolean extendedSelection = false;

			// Extended selection
			if ((keyEvent.getModifiers() & keyboardExtendedSelectionModifierKeyMask) != 0) {
				extendedSelection = true;
			}

			if (keyEvent.getKeyCode() == KeyEvent.VK_LEFT || keyEvent.getKeyCode() == KeyEvent.VK_RIGHT) {
				IRendererWithItemSelection renderer = (IRendererWithItemSelection) JFCSWTChartsComponentsProvider.getRenderer((ChartPanel) customData);

				// Select next item or add it to selection
				if (keyEvent.getKeyCode() == KeyEvent.VK_RIGHT) {
					((MultipleItemsSelector) renderer.getItemsSelector()).selectNextItem(extendedSelection);
				}
				else if (keyEvent.getKeyCode() == KeyEvent.VK_LEFT) {
					((MultipleItemsSelector) renderer.getItemsSelector()).selectPreviousItem(extendedSelection);
				}
			}
		}
	}

}
