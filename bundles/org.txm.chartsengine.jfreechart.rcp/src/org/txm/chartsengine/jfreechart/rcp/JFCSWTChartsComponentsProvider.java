package org.txm.chartsengine.jfreechart.rcp;

import java.util.ArrayList;

import org.eclipse.swt.widgets.Composite;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.LegendItemSource;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYPlot;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.jfreechart.core.JFCChartsEngine;
import org.txm.chartsengine.jfreechart.rcp.swt.JFCChartComposite;
import org.txm.chartsengine.jfreechart.rcp.themes.highcharts.HighchartsThemeSwing;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.swt.ChartComposite;


/**
 * JFreeChart library charts components provider implementation.
 * 
 * @author sjacquot
 *
 */
public class JFCSWTChartsComponentsProvider extends SWTChartsComponentsProvider {


	/**
	 * Default constructor.
	 */
	public JFCSWTChartsComponentsProvider() {
		super();
	}


	@Override
	public void setChartsEngine(ChartsEngine chartsEngine) {
		super.setChartsEngine(chartsEngine);
		// Redefine the TBX theme to manage UI
		((JFCChartsEngine) chartsEngine).setTheme(new HighchartsThemeSwing((JFCChartsEngine) chartsEngine));
	}


	@Override
	public JFCChartsEngine getChartsEngine() {
		return (JFCChartsEngine) this.chartsEngine;
	}

	@Override
	public ChartComposite createComposite(ChartEditor<? extends ChartResult> chartEditor, Composite parent) {
		return new JFCChartComposite(chartEditor, parent);
	}


	@Override
	public ArrayList<String> getChartsEngineSupportedExportFileFormats() {
		return this.chartsEngine.getSupportedOutputFileFormats();
	}

	/**
	 * Gets the items renderer of the specified plot.
	 * 
	 * @param plot
	 * @return
	 */
	public static LegendItemSource getRenderer(Plot plot) {
		LegendItemSource renderer = null;

		if (plot instanceof XYPlot) {
			renderer = ((XYPlot) plot).getRenderer();
		}
		else if (plot instanceof CategoryPlot) {
			renderer = ((CategoryPlot) plot).getRenderer();
		}

		return renderer;
	}

	/**
	 * Gets the items renderer associated to the plot linked to the specified chart panel.
	 * 
	 * @param panel
	 * @return
	 */
	public static LegendItemSource getRenderer(ChartPanel panel) {
		return getRenderer(panel.getChart().getPlot());
	}



}
