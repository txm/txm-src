/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package visuAnalec.util;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import visuAnalec.elements.*;
import visuAnalec.util.GMessages.*;

/**
 *
 * @author Bernard
 */
public abstract class GVisu {
  public static class TitreBarre extends JLabel {
    public TitreBarre(String titre) {
      super(titre);
    }
    public TitreBarre(String titre, Color couleur) {
      super(titre);
      setOpaque(true);
      setBackground(couleur);
    }
  }

  public static abstract class Boutons {
    public static void setDefaut(JButton bouton, boolean oui) {
      ((JFrame) bouton.getTopLevelAncestor()).getRootPane().setDefaultButton(oui ? bouton : null);
      if (oui) bouton.requestFocusInWindow();
    }
  }
  public static String styleIdPopup(String texte, String id) {
    if (texte.length()>35)
      texte = texte.substring(0, 18)+" [...] "+texte.substring(texte.length()-8);
    return "<html>"+id+" (<span style=\"font-style:italic \">"+texte+"</span>)";
  }
  public static String styleIdTooltip(ArrayList<String> textes, ArrayList<String> ids) {
    String tooltip = "<html>";
    for (int i = 0; i<textes.size(); i++) {
      String texte = textes.get(i);
      if (texte.length()>35)
        texte = texte.substring(0, 18)+" [...] "+texte.substring(texte.length()-8);
      tooltip += "<p>"+ids.get(i)+" (<span style=\"font-style:italic \">"+texte+"</span>)";
    }
    return tooltip;
  }
  public static String styleDef(String nom) {
    return "<html><span style=\"color:gray\"> &lt <span style=\"font-style:italic \">"
            +nom+"</span> &gt";
  }
  private final static Font STYLE_COMBO = Font.decode("Arial plain 12");

  public enum SensSpinnerBinaire {
    SUIVANT, PRECEDENT
  }

  public static class ModeleSpinnerBinaire extends AbstractSpinnerModel {
    private SensSpinnerBinaire sens;
    public Object getValue() {
      return sens;
    }
    public void setValue(Object value) {
      sens = (SensSpinnerBinaire) value;
      fireStateChanged();
    }
    public Object getNextValue() {
      return SensSpinnerBinaire.PRECEDENT;
    }
    public Object getPreviousValue() {
      return SensSpinnerBinaire.SUIVANT;
    }
  }
  private static Dimension dimZero = new Dimension(20, 20);
  private static Insets insetsZero = new Insets(0, 0, 0, 0);

  public static class ElementComboBox extends Box implements ActionListener, ChangeListener {
    private ArrayList<ActionListener> listeCorrespondants = new ArrayList<ActionListener>();
    public JComboBox combo;
    private JButton zero;
    private JSpinner spin;
    public Element[] elements;  // liste triée
    public ElementComboBox() {
      super(BoxLayout.X_AXIS);
      zero = new JButton(new GAction("0") {
        public void executer() {
          effSelection();
        }
      });
      zero.setMargin(insetsZero);
      zero.setPreferredSize(dimZero);
      spin = new JSpinner(new ModeleSpinnerBinaire());
      spin.setEditor(new JLabel());
      spin.addChangeListener(this);
      combo = new JComboBox();
      combo.setEditable(false);
      combo.setFont(STYLE_COMBO);
      combo.addActionListener(this);
      add(combo);
      add(spin);
      add(zero);
      setModeleVide();
    }
    public void addActionListener(ActionListener correspondant) {
      listeCorrespondants.add(correspondant);
    }
    public void removeActionListener(ActionListener correspondant) {
      listeCorrespondants.remove(correspondant);
    }
    public void actionPerformed(ActionEvent evt) {
      try {
        evt.setSource(this);
        for (Object corresp : listeCorrespondants.toArray()) {
          ((ActionListener) corresp).actionPerformed(evt);
        }
      } catch (Throwable ex) {
        GMessages.erreurFatale(ex);
      }
    }
    public void setModeleVide() {
      combo.setModel(new DefaultComboBoxModel(new String[]{styleDef("identifiant")}));
      elements = new Element[0];
      setMaximumSize(getPreferredSize());
    }
    public void setModele(Element[] elts, String[] ids) {
      // elts doit être trié et en correspondance avec ids
      combo.setModel(new DefaultComboBoxModel(Tableaux.insererStringEnTete(ids, styleDef("identifiant"))));
      elements = elts;
      setMaximumSize(getPreferredSize());
    }
    public Element[] getElements() {
      return elements;
    }
    public void gotoPrecedent() {
      int index;
      if ((index = combo.getSelectedIndex())==0) index = combo.getItemCount();
      combo.setSelectedIndex(index-1);
    }
    public void gotoSuivant() {
      int index = combo.getSelectedIndex()+1;
      if (index==combo.getItemCount()) index = 0;
      combo.setSelectedIndex(index);
    }
    public boolean pasdeSelection() {
      return combo.getSelectedIndex()<1;
    }
    public Element getSelection() {
      if (combo.getSelectedIndex()>0)
        return elements[combo.getSelectedIndex()-1];
      return null;
    }
    public void effSelection() {
      combo.setSelectedIndex(0);
    }
    public void setSelection(Element elt) {
      int no = Arrays.binarySearch(elements, elt);
      if (no<0)
        throw new ArrayIndexOutOfBoundsException("Element à sélectionner absent");
      combo.setSelectedIndex(no+1);
    }
    public String getIdElement(Element elt) {
      int no = Arrays.binarySearch(elements, elt);
      if (no<0)
        throw new ArrayIndexOutOfBoundsException("Element recherché absent");
      return ((String) combo.getItemAt(no+1));

    }
    public void stateChanged(ChangeEvent e) {
      try {
        switch ((SensSpinnerBinaire) spin.getValue()) {
          case SUIVANT:
            gotoSuivant();
            return;
          case PRECEDENT:
            gotoPrecedent();
            return;
        }
      } catch (Throwable ex) {
        GMessages.erreurFatale(ex);
      }
    }
  }

  public static class Val0ComboBox extends JComboBox {
    private String[] vals;
    private String val0;
    public Val0ComboBox(String val0) {
      super();
      this.val0 = val0;
      setEditable(false);
      setFont(STYLE_COMBO);
      setModeleVide();
    }
    public void setModeleVide() {
      setModel(new DefaultComboBoxModel(new String[]{styleDef(val0)}));
      vals = new String[0];
      setMaximumSize(getPreferredSize());
    }
    public void setModele(String[] types) {
      setModel(new DefaultComboBoxModel(Tableaux.insererStringEnTete(types, styleDef(val0))));
      this.vals = types;
      setMaximumSize(getPreferredSize());
    }
    public boolean pasdeSelection() {
      return getSelectedIndex()<1;
    }
    public int nbItems() {
      return vals.length;
    }
    public String getSelection() {
      if (getSelectedIndex()>0) return vals[getSelectedIndex()-1];
      return null;
    }
    public void effSelection() {
      setSelectedIndex(0);
    }
    public void setSelection(String type) {
      setSelectedItem(type);
    }
    public void setSelection(int i) {
      setSelectedIndex(i+1);
    }
  }
  public static String getNomClasse(Class<? extends Element> classe) {
    if (classe==Element.class) return "élément";
    else if (classe==Unite.class) return "unité";
    else if (classe==Relation.class) return "relation";
    else if (classe==Schema.class) return "schéma";
    else
      throw new UnsupportedOperationException("Sous-classe inconnue pour getNomClasse()");
  }
  public static String getNomDeClasse(Class<? extends Element> classe) {
    if (classe==Element.class) return "d'élément";
    else if (classe==Unite.class) return "d'unité";
    else if (classe==Relation.class) return "de relation";
    else if (classe==Schema.class) return "de schéma";
    else
      throw new UnsupportedOperationException("Sous-classe inconnue pour getNomTypedeClasse()");
  }
  public static String getNomDeLaClasse(Element elt) {
    Class<? extends Element> classe = elt.getClass();
    if (classe==Unite.class) return "de l'unité";
    else if (classe==Relation.class) return "de la relation";
    else if (classe==Schema.class) return "du schéma";
    else
      throw new UnsupportedOperationException("Sous-classe inconnue pour getNomTypedeClasse()");
  }
  public static Class<? extends Element> getClasse(String classeType) {
    if (classeType==null) return null;
    if (classeType.startsWith("REL-")) return Relation.class;
    if (classeType.startsWith("SCH-")) return Schema.class;
    return Unite.class;
  }
  public static String getType(String classeType) {
    if (classeType==null) return null;
    if (classeType.startsWith("REL-")) return classeType.substring(4);
    if (classeType.startsWith("SCH-")) return classeType.substring(4);
    return classeType;
  }
  private static String getClasseType(Class<? extends Element> classe, String type) {
    if (classe==Unite.class) return type;
    else if (classe==Relation.class) return "REL-"+type;
    else if (classe==Schema.class) return "SCH-"+type;
    else
      throw new UnsupportedOperationException("Sous-classe inconnue pour getClasseType()");
  }
  public static String[] getClassesTypes(String[] typesUnit, String[] typesRel, String[] typesSch) {
    String[] types = Arrays.copyOf(typesUnit, typesUnit.length+typesRel.length+typesSch.length);
    for (int i = 0; i<typesRel.length; i++)
      types[i+typesUnit.length] = "REL-"+typesRel[i];
    for (int i = 0; i<typesSch.length; i++)
      types[i+typesUnit.length+typesRel.length] = "SCH-"+typesSch[i];
    return types;
  }

  public static class TypeComboBox extends Val0ComboBox {
    private Class<? extends Element> classe;
    public TypeComboBox(Class<? extends Element> classe) {
      super("type "+getNomDeClasse(classe));
      this.classe = classe;
    }
    @Override
    public void setModele(String[] types) {
      if (this.classe==Element.class)
        throw new UnsupportedOperationException("pas pour la classe Element");
      super.setModele(types);
    }
    public void setModele(String[] typesUnites, String[] typesRelations, String[] typesSchemas) {
      if (this.classe!=Element.class)
        throw new UnsupportedOperationException("uniquement pour la classe Element");
      super.setModele(getClassesTypes(typesUnites, typesRelations, typesSchemas));
    }
    public String getTypeSelection() {
      String classetype = getSelection();
      if (classetype==null) return null;
      if (classe==Element.class) return getType(classetype);
      return classetype;
    }
    public Class<? extends Element> getClasseSelection() {
      String classetype = getSelection();
      if (classetype==null) return null;
      if (classe==Element.class) return getClasse(classetype);
      return classe;
    }
    public void setSelection(Class<? extends Element> classe, String type) {
      if (this.classe!=Element.class)
        throw new UnsupportedOperationException("uniquement pour la classe Element");
      super.setSelection(getClasseType(classe, type));
    }
    @Override
    public void setSelection(String type) {
      if (this.classe==Element.class)
        throw new UnsupportedOperationException("pas pour la classe Element");
      super.setSelection(type);
    }
  }
  private final static Color[] couleursEtNoirEtBlanc = new Color[]{Color.BLACK, Color.WHITE, Color.YELLOW,
    Color.RED, Color.BLUE, Color.ORANGE, Color.GREEN, Color.CYAN, Color.MAGENTA, Color.PINK
  };
  private final static Color[] couleurs = new Color[]{Color.YELLOW, Color.RED, Color.BLUE,
    Color.ORANGE, Color.GREEN, Color.CYAN, Color.MAGENTA, Color.PINK
  };
  public static Color[] getCouleursEtNoirEtBlanc() {
    return couleursEtNoirEtBlanc;
  }
  public static Color getCouleur(int i) {
    Color c = couleurs[i%couleurs.length];
    for (int n = 0; n<i/couleurs.length; n++) {
      float[] rgb = c.getRGBColorComponents(null);
      c = new Color(0.499f+rgb[0]/3, 0.499f+rgb[1]/3, 0.499f+rgb[2]/3);
    }
    return c;
  }
  public static int getTaille(int i) {
    int c;
    c = i + 1;
    if(c > 4){
        c = 4;
    }
    return c;
  }
  public static int getForme(int i) {
    int c;
    c = i;
    return c;
  }
  /*@require : i doit être positif et inférieur ou égal 99*/
  public static String getTexte(int i) {
    String c;
    c = "" + ((i - i%10)%100)/10 + i%10;
    return c;
  }
  
  public static Color getCouleurSansValeur() {
    return Color.LIGHT_GRAY;
  }
  public static int getFormeSansValeur() {
    return 10;
  }
  public static int getTailleSansValeur() {
    return 4;
  }

  public static String getTexteSansValeur() {
    return "99";
  }

  public static class DisqueCouleurIcone implements Icon {
    private Color couleur;
    int rayon;
    int marge;
    public DisqueCouleurIcone(Color couleur, int rayon, int marge) {
      this.couleur = couleur;
      this.rayon = rayon;
      this.marge = marge;
    }
    public void paintIcon(Component c, Graphics g, int x, int y) {
      if (couleur==null) {
        g.setColor(Color.BLACK);
        g.drawLine(x+3*marge, y+3*marge, x+2*rayon-marge, y+2*rayon-marge);
        g.drawLine(x+3*marge, y+2*rayon-marge, x+2*rayon-marge, y+3*marge);
      } else {
        g.setColor(couleur);
        g.fillOval(x+marge, y+marge, 2*rayon, 2*rayon);
        g.setColor(Color.BLACK);
        g.drawOval(x+marge, y+marge, 2*rayon, 2*rayon);
      }
    }
    public int getIconWidth() {
      return 2*rayon+2*marge;
    }
    public int getIconHeight() {
      return 2*rayon+2*marge;
    }
    public int getRayon(){
        return rayon;
    }
    public int getMarge(){
        return marge;
    }
    public Color getColor(){
        return couleur;
    }
    
  }

  public static class CarreCouleurIcone implements Icon {
    private Color couleur;
    int rayon;
    int marge;
    public CarreCouleurIcone(Color couleur, int rayon, int marge) {
      this.couleur = couleur;
      this.rayon = rayon;
      this.marge = marge;
    }
    public void paintIcon(Component c, Graphics g, int x, int y) {
      if (couleur==null) {
        g.setColor(Color.BLACK);
        g.drawLine(x+3*marge, y+3*marge, x+2*rayon-marge, y+2*rayon-marge);
        g.drawLine(x+3*marge, y+2*rayon-marge, x+2*rayon-marge, y+3*marge);
      } else {
        g.setColor(couleur);
        g.fillRect(x+marge, y+marge, 2*rayon, 2*rayon);
        g.setColor(Color.BLACK);
        g.drawRect(x+marge, y+marge, 2*rayon, 2*rayon);
      }
    }
    public int getIconWidth() {
      return 2*rayon+2*marge;
    }
    public int getIconHeight() {
      return 2*rayon+2*marge;
    }
    public int getRayon(){
        return rayon;
    }
    public int getMarge(){
        return marge;
    }
    public Color getColor(){
        return couleur;
    }
  }
  public static class CroixCouleurIcone implements Icon {
    private Color couleur;
    int rayon;
    int marge;
    public CroixCouleurIcone(Color couleur, int rayon, int marge) {
      this.couleur = couleur;
      this.rayon = rayon;
      this.marge = marge;
    }
    public void paintIcon(Component c, Graphics g, int x, int y) {
      if (couleur==null ) {
        g.setColor(Color.BLACK);
        g.drawLine(x+3*marge, y+3*marge, x+2*rayon-marge, y+2*rayon-marge);
        g.drawLine(x+3*marge, y+2*rayon-marge, x+2*rayon-marge, y+3*marge);
      } else {
        g.setColor(couleur);
        int x1[]=new int[]{x+marge,x+marge+rayon-1,x+marge+rayon-1,x+marge+rayon+1,x+marge+rayon+1
                , x + marge + 2 * rayon,x + marge + 2 * rayon,x+marge+rayon+1,x+marge+rayon+1 
                ,x+marge+rayon-1,x+marge+rayon-1,x+marge,x+marge};
        int y1[]=new int[]{y + marge+rayon-1,y + marge+rayon-1,y + marge,y + marge,y + marge+rayon-1
                ,y + marge+rayon-1,y + marge+rayon+1,y + marge+rayon+1,y + marge+2*rayon
                ,y + marge+2*rayon,y + marge+rayon+1,y + marge+rayon+1,y + marge+rayon-1 };
        g.fillPolygon(x1, y1, 13);
        g.setColor(Color.BLACK);
        g.drawPolygon(x1, y1, 13);
      }
    }
    public int getIconWidth() {
      return 2*rayon+2*marge;
    }
    public int getIconHeight() {
      return 2*rayon+2*marge;
    }
    public int getRayon(){
        return rayon;
    }
    public int getMarge(){
        return marge;
    }
    public Color getColor(){
        return couleur;
    }
    
  }
  public static class TriangleCouleurIcone implements Icon {
    private Color couleur;
    int rayon;
    int marge;
    public TriangleCouleurIcone(Color couleur, int rayon, int marge) {
      this.couleur = couleur;
      this.rayon = rayon;
      this.marge = marge;
    }
    public void paintIcon(Component c, Graphics g, int x, int y) {
      if (couleur==null) {
        g.setColor(Color.BLACK);
        g.drawLine(x+3*marge, y+3*marge, x+2*rayon-marge, y+2*rayon-marge);
        g.drawLine(x+3*marge, y+2*rayon-marge, x+2*rayon-marge, y+3*marge);
      } else {
        g.setColor(couleur);
        int x1[]=new int[]{x + marge,x + marge + rayon,x + marge + 2 *rayon,x+marge};
        int y1[]=new int[]{y + marge+ 2 * rayon,y+marge,y + marge+ 2 * rayon, y + marge+ 2 * rayon};
        g.fillPolygon(x1,y1,4);
        g.setColor(Color.BLACK);
        g.drawPolygon(x1, y1, 4);
      }
    }
    public int getIconWidth() {
      return 2*rayon+2*marge;
    }
    public int getIconHeight() {
      return 2*rayon+2*marge;
    }
    public int getRayon(){
        return rayon;
    }
    public int getMarge(){
        return marge;
    }
    public Color getColor(){
        return couleur;
    }
  }
  
    public static class TriangleinverseCouleurIcone implements Icon {
    private Color couleur;
    int rayon;
    int marge;
    public TriangleinverseCouleurIcone(Color couleur, int rayon, int marge) {
      this.couleur = couleur;
      this.rayon = rayon;
      this.marge = marge;
    }
    public void paintIcon(Component c, Graphics g, int x, int y) {
      if (couleur==null) {
        g.setColor(Color.BLACK);
        g.drawLine(x+3*marge, y+3*marge, x+2*rayon-marge, y+2*rayon-marge);
        g.drawLine(x+3*marge, y+2*rayon-marge, x+2*rayon-marge, y+3*marge);
      } else {
        g.setColor(couleur);
        int x1[]=new int[]{x + marge,x + marge + 2 * rayon,x + marge + rayon,x+marge};
        int y1[]=new int[]{y + marge,y+marge,y + marge+ 2 * rayon, y + marge};
        g.fillPolygon(x1,y1,4);
        g.setColor(Color.BLACK);
        g.drawPolygon(x1, y1, 4);
      }
    }
    public int getIconWidth() {
      return 2*rayon+2*marge;
    }
    public int getIconHeight() {
      return 2*rayon+2*marge;
    }
    public int getRayon(){
        return rayon;
    }
    public int getMarge(){
        return marge;
    }
    public Color getColor(){
        return couleur;
    }
  }
  
  
  public static class LegendeLabel {
    String texte;
    String textelegende;
    Icon icone;
    public LegendeLabel(String texte,String textelegende){
        this.texte = texte;
        this.textelegende = textelegende;
    }
    public LegendeLabel(String texte, Color couleur) {
      this.texte = texte;
      this.icone = new DisqueCouleurIcone(couleur, 12, 2);
    }
    public LegendeLabel(String texte, Color couleur, int taille, int forme, int rayon , int marge) {
      this.texte = texte;
      if(taille!=-1 && forme !=-1){
              switch(forme){
            case 0 :
                this.icone = new DisqueCouleurIcone(couleur, rayon / taille
                        , marge);
                break;
            case 1 :
                this.icone = new CarreCouleurIcone(couleur, rayon / taille
                        , marge);
                break;
            case 2 :
                this.icone = new CroixCouleurIcone(couleur, rayon / taille
                        , marge);
                break;
            case 3 :
                this.icone = new TriangleCouleurIcone(couleur, rayon / taille
                        , marge);
                break;
            default :
                this.icone = new TriangleinverseCouleurIcone(couleur, rayon / taille
                        , marge);
                break;
        }
      }
      else{
                        switch(forme){
            case 0 :
                this.icone = new DisqueCouleurIcone(null, 12
                        , 2);
                break;
            case 1 :
                this.icone = new CarreCouleurIcone(null, 12
                        , 2);
                break;
            case 2 :
                this.icone = new CroixCouleurIcone(null, 12
                        , 2);
                break;
            case 3 :
                this.icone = new TriangleCouleurIcone(null, 12
                        , 2);
                break;
            default :
                this.icone = new TriangleinverseCouleurIcone(null, 12
                        , 2);
                break;
      }
    }    
    }
  }

  public static class LegendeCellRenderer extends JLabel implements ListCellRenderer {
    public LegendeCellRenderer() {
      setIconTextGap(12);
    }
    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {
      LegendeLabel label = (LegendeLabel) value;
      setText(label.texte);
      setIcon(label.icone);
      return this;
    }
  }
  /*Modèle pour le tableau de la légende avec icone*/
  public static class LegendeModeleTable extends AbstractTableModel{
      private Object[][] donnees = new Object[0][0];
      private String[] entetes = new String[0];
          	@Override
	public int getColumnCount() {
		return entetes.length;
        }
        @Override
	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	}
        public LegendeModeleTable(){
        }
        public LegendeModeleTable(String[] titres, LegendeLabel[] legende, HashMap<String, Float> freq){
                        this.entetes = titres;
            this.donnees = new Object[legende.length][freq==null ? 2 : 3];
            for(int i = 0;i < legende.length;i++){
                donnees[i][0] = legende[i].icone;
                donnees[i][1] = legende[i].texte;
                if(freq!=null){
                donnees[i][2] = freq.get(legende[i].texte);
                }
            }
        }        
        public int getRowCount() {
		return donnees.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return donnees[rowIndex][columnIndex];
	}
        @Override
        public Class getColumnClass(int columnIndex){
            switch(columnIndex){
                case 0 :
                    return Icon.class;
                case 2:
                    return Float.class;
                default:
                    return String.class;
            }
        }
        public void setLegendeModele(String[] titres, LegendeLabel[] legende, HashMap<String, Float> freq){
            this.entetes = titres;
            this.donnees = new Object[legende.length][3];
            for(int i = 0;i < legende.length;i++){
                donnees[i][0] = legende[i].icone;
                donnees[i][1] = legende[i].texte;
                donnees[i][2] = freq.get(legende[i].texte);
            }
        }
  }
  /*Modèle pour le tableau de la légende sans icone*/
    public static class LegendeModeleTableTexte extends AbstractTableModel{
      private Object[][] donnees = new Object[0][0];
      private String[] entetes = new String[0];
          	@Override
	public int getColumnCount() {
		return entetes.length;
        }
        @Override
	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	}
        public LegendeModeleTableTexte(){
        }
        public LegendeModeleTableTexte(String[] titres, LegendeLabel[] legende, HashMap<String, Float> freq){
                        this.entetes = titres;
            this.donnees = new Object[legende.length][freq==null ? 2 : 3];
            for(int i = 0;i < legende.length;i++){
                donnees[i][0] = legende[i].textelegende;
                donnees[i][1] = legende[i].texte;
                if(freq!=null){
                donnees[i][2] = freq.get(legende[i].texte);
                }
            }
        }        
        public int getRowCount() {
		return donnees.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return donnees[rowIndex][columnIndex];
	}
        @Override
        public Class getColumnClass(int columnIndex){
            switch(columnIndex){
                case 0 :
                    return String.class;
                case 2:
                    return Float.class;
                default:
                    return String.class;
            }
        }
        public void setLegendeModele(String[] titres, LegendeLabel[] legende, HashMap<String, Float> freq){
            this.entetes = titres;
            this.donnees = new Object[legende.length][3];
            for(int i = 0;i < legende.length;i++){
                donnees[i][0] = legende[i].textelegende;
                donnees[i][1] = legende[i].texte;
                donnees[i][2] = freq.get(legende[i].texte);
            }
        }
  }
  
  
  

  public static class RectangleCouleurIcone implements Icon {
    private Color couleur;
    private int largeur;
    private int hauteur;
    private int marge;
    private boolean select;
    public RectangleCouleurIcone(Color couleur, int largeur, int hauteur, int marge, boolean select) {
      this.couleur = couleur;
      this.largeur = largeur;
      this.hauteur = hauteur;
      this.marge = marge;
      this.select = select;
    }
    void setCouleur(Color c) {
      couleur = c;
    }
    public void paintIcon(Component c, Graphics g, int x, int y) {
      g.setColor(couleur);
      g.fillRect(x+marge, y+marge, largeur, hauteur);
      if (select) {
        g.setColor(Color.BLACK);
        g.drawRect(x+marge, y+marge, largeur, hauteur);
      }
    }
    public int getIconWidth() {
      return largeur+2*marge;
    }
    public int getIconHeight() {
      return hauteur+2*marge;
    }
  }

  public static class CouleurCellRenderer extends JLabel implements ListCellRenderer, TableCellRenderer {
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
            boolean cellHasFocus) {
      if (index==list.getModel().getSize()-1||index<0) {
        setText(index<0 ? "" : styleDef("autre"));
        setIcon(null);
      } else {
        setText(null);
        setIcon(new RectangleCouleurIcone((Color) value, 40, 10, 2, isSelected));
      }
      return this;
    }
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
      setText(null);
      setIcon(new RectangleCouleurIcone((Color) value, 80, 10, 2, true));
      return this;
    }
  }

  public static class CouleurComboBoxEditor implements ComboBoxEditor {
    CouleurComboBox comboBox;
    RectangleCouleurIcone icone;
    JLabel editeur;
    Color couleur;
    public CouleurComboBoxEditor(CouleurComboBox cb) {
      comboBox = cb;
      editeur = new JLabel(icone = new RectangleCouleurIcone(null, 80, 10, 2, true));
    }
    public Component getEditorComponent() {
      return editeur;
    }
    private void init(Color couleur) {
      this.couleur = couleur;
      icone.setCouleur(couleur);
    }
    public void setItem(Object anObject) {
      if (anObject==null) {
        Color c = JColorChooser.showDialog(editeur, "Choix de la couleur", couleur);
        if (c!=null) couleur = c;
      } else couleur = (Color) anObject;
      icone.setCouleur(couleur);
      comboBox.setSelectedItem(couleur);
    }
    public Object getItem() {
      return couleur;
    }
    public void selectAll() {
    }
    public void addActionListener(ActionListener l) {
    }
    public void removeActionListener(ActionListener l) {
    }
  }

  public static class CouleurComboBox extends JComboBox {
    public CouleurComboBox(Color[] couleurs) {
      super(couleurs);
      addItem(null);
      setRenderer(new CouleurCellRenderer());
      setEditor(new CouleurComboBoxEditor(this));
      setMaximumRowCount(couleurs.length+1);
    }
  }

  public static class CouleurTableEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {
    private CouleurComboBox comboBox;
    private Color couleur;
    public CouleurTableEditor(Color[] couleurs) {
      comboBox = new CouleurComboBox(couleurs);
      comboBox.setEditable(true);
      comboBox.addActionListener(this);
    }
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      couleur = (Color) value;
      comboBox.getEditor().setItem(value);
      return comboBox;
    }
    public Object getCellEditorValue() {
      return couleur;
    }
    public void actionPerformed(ActionEvent e) {
      couleur = (Color) comboBox.getSelectedItem();
      fireEditingStopped();
    }
  }
  //fonctiontrès grossière mais si quelqu'un réussit à obtenir un charactère à partir  de son code ascci ou unicode
  //quelque soit la machine, il devra remplacer cette fonction 

    public static char intToChar(int value){
        switch(value){
            case 0:
                return '0';
            case 1:
                return '1';
            case 2:
                return '2';
            case 3:
                return '3';
            case 4:
                return '4';
            case 5:
                return '5';
            case 6: 
                return '6';
            case 7:
                return '7';
            case 8 :
                return '8';
            case 9 :
                return '9'; 
            case 10 :
                return 'a';
            case 11 :
                return 'b';
            case 12 :
                return 'c';
            case 13 :
                return 'd';
            case 14 :
                return 'e';
            case 15 :
                return 'f';
            case 16 :
                return 'g';
            case 17 :
                return 'h';
            case 18 :
                return 'i';
            case 19 :
                return 'j';
            case 20 :
                return 'k';
            case 21 :
                return 'l';
            case 22 :
                return 'm';
            case 23 :
                return 'n';
            case 24 :
                return 'o';
            case 25 :
                return 'p';
            case 26 :
                return 'q';
            case 27 :
                return 'r';
            case 28 :
                return 's';
            case 29 :
                return 't';
            case 30 :
                return 'u';
            case 31 :
                return 'v';
            case 32 :
                return 'w';
            case 33 :
                return 'x';
            case 34 :
                return 'y';
            case 35 :
                return 'z';
            case 36 :
                return 'A';
            case 37 : 
                return 'B';
            case 38 :
                return 'C';
            case 39 :
                return 'D';           
            case 40 :
                return 'E';
            case 41 :
                return 'F';              
            case 42 :
                return 'G';                
            case 43 :
                return 'H';               
            case 44 :
                return 'I';
            case 45 :
                return 'J';               
            case 46 :
                return 'K';               
            case 47 :
                return 'L';              
            case 48 :
                return 'M';              
            case 49 :
                return 'N';                
            case 50 :
                return 'O';                
            case 51 :
                return 'P';                
            case 52 :
                return 'Q';               
            case 53 :
                return 'R';               
            case 54 :
                return 'S';               
            case 55 :
                return 'T';
            case 56 :
                return 'U';
            case 57 :
                return 'V';                
            case 58 :
                return 'W';               
            case 59 :
                return 'X';               
            case 60 :
                return 'Y';               
            case 61 :
                return 'Z';             
            default : 
                return '?';
        }
        
    }
    public static int CharToInt(char value){
        switch(value){
            case '0':
                return 0;
            case '1':
                return 1;
            case '2':
                return 2;
            case '3':
                return 3;
            case '4':
                return 4;
            case '5':
                return 5;
            case '6': 
                return 6;
            case '7':
                return 7;
            case '8' :
                return 8;
            case '9' :
                return 9; 
            case 'a' :
                return 10;
            case 'b' :
                return 11;
            case 'c':
                return 12;
            case 'd' :
                return 13;
            case 'e' :
                return 14;
            case 'f' :
                return 15;
            case 'g' :
                return 16;
            case 'h' :
                return 17;
            case 'i' :
                return 18;
            case 'j':
                return 19;
            case 'k' :
                return 20;
            case 'l' :
                return 21;
            case 'm' :
                return 22;
            case 'n' :
                return 23;
            case 'o' :
                return 24;
            case 'p' :
                return 25;
            case 'q' :
                return 26;
            case 'r' :
                return 27;
            case 's' :
                return 28;
            case 't' :
                return 29;
            case 'u' :
                return 30;
            case 'v' :
                return 31;
            case 'w':
                return 32;
            case 'x' :
                return 33;
            case 'y' :
                return 34;
            case 'z' :
                return 35;
            case 'A' :
                return 36;
            case 'B' : 
                return 37;
            case 'C' :
                return 38;
            case 'D' :
                return 39;           
            case 'E' :
                return 40;
            case 'F' :
                return 41;              
            case 'G' :
                return 42;                
            case 'H' :
                return 43;               
            case 'I' :
                return 44;
            case 'J' :
                return 45;               
            case 'K' :
                return 46;               
            case 'L' :
                return 47;              
            case 'M' :
                return 48;              
            case 'N' :
                return 49;                
            case 'O' :
                return 50;                
            case 'P' :
                return 51;                
            case 'Q' :
                return 52;               
            case 'R' :
                return 53;               
            case 'S' :
                return 54;               
            case 'T' :
                return 55;
            case 'U' :
                return 56;
            case 'V' :
                return 57;                
            case 'W' :
                return 58;               
            case 'X' :
                return 59;               
            case 'Y' :
                return 60;               
            case 'Z' :
                return 61;             
            default : 
                return -1;
        }
        
    }
}
