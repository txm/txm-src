/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package visuAnalec.chaines;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;
import javax.swing.*;
import visuAnalec.chaines.ModeleChaines.*;
import visuAnalec.util.GMessages.*;
import visuAnalec.util.GVisu.*;
import visuAnalec.fichiers.FichiersGraphique;
/**
 *
 * @author Bernard
 */
public class GChaines extends JPanel {//donnée de la chaine
  final static Font FONTE_ID = Font.decode("Arial bold 10");
  final static int RAYON_DISQUES = 12;
  final static int MARGE_DISQUES = 2;
  final static int LARGEUR_INTER_CHAINONS = 10;
  final static int MARGE_DEBUT_LIGNE = 15;
  final static int HAUTEUR_LIGNES = 80;
  final static int HAUTEUR_SEGMENT = 40;
  final static int HAUTEUR_CHAINONS = 60;
  final static int policeTailleSVG = 10;
  final static String policeSVG = "font-size:50px;font-family:'Trebuchet MS', Verdana, serif;";
  final static String SYMBOLE_SEGMENT = "§";
//    final static int hauteurinfLien = 25;
//    final static int deltahauteurLien = 5;
//    final static int deltasupArcs = 95;
//    final static int hauteurinfArcs = 5;
//  final static int HAUTEUR_PANNEAU = 120;
  final static String PAS_DE_NUMERO = "~";

  static class GChainon extends JButton {//permet de créer un chainon hérite d'un bouton
    VisuChaines visu;
    VisuCoref visucoref;
    Chainon chainon;
    public GChainon(Chainon ch, VisuChaines vis) {
      super();//on recupere le bouton
      this.visu = vis;//on récupère le chainon et le visu
      this.chainon = ch;
      Icon d = new DisqueCouleurIcone(Color.WHITE, RAYON_DISQUES, MARGE_DISQUES);
      setAction(new GAction(null, d) {
        public void executer() {
          //visu.afficherOccurrences(chainon.unite, chainon.chaine);
        }
      });
  
      setSize(2*(RAYON_DISQUES+MARGE_DISQUES), 2*(RAYON_DISQUES+MARGE_DISQUES));//on calcule la taille
      setBorderPainted(false);
      String t2 = "<html><div style=\"font-size:11pt\"><span style=\"font-style:italic\">"+chainon.texte
              +"</span><p>"+chainon.idChaine+"<p>"+chainon.nbEltsChaine+" occurences </div>";//cree les metadonnees
      setToolTipText(t2);
      setBackground(Color.WHITE);//arrière plan
      setFocusPainted(false);
    }
    public GChainon(Chainon ch, VisuCoref vis) {
      super();//on recupere le bouton
      this.visucoref = vis;//on récupère le chainon et le visu
      this.chainon = ch;
      Icon d = new DisqueCouleurIcone(Color.WHITE, RAYON_DISQUES, MARGE_DISQUES);
      setAction(new GAction(null, d) {
        public void executer() {
          //visu.afficherOccurrences(chainon.unite, chainon.chaine);
        }
      }); 
        
      setSize(2*(RAYON_DISQUES+MARGE_DISQUES), 2*(RAYON_DISQUES+MARGE_DISQUES));//on calcule la taille
      setBorderPainted(false);
      String t2 = "<html><div style=\"font-size:11pt\"><span style=\"font-style:italic\">"+chainon.texte
              +"</span><p>"+chainon.idChaine+"<p>"+chainon.nbEltsChaine+" occurences </div>";//cree les metadonnees
      setToolTipText(t2);
      setBackground(Color.WHITE);//arrière plan
      setFocusPainted(false);
    }
      
    @Override
    public JToolTip createToolTip() {
      JToolTip jt = new JToolTip();
      jt.setBackground(Color.WHITE);
      return jt;
    }
    void changerCouleur(Color couleur) {//change couleur du disque
      setIcon(new DisqueCouleurIcone(couleur, RAYON_DISQUES, MARGE_DISQUES));
    }
    void changerCouleur(Color couleur,int taillediviseur,int forme) {
    //change couleur,forme et taille du chainon
        switch(forme){
            case 0 :
                setIcon(new DisqueCouleurIcone(couleur, RAYON_DISQUES / taillediviseur
                        , MARGE_DISQUES));
                break;
            case 1 :
                setIcon(new CarreCouleurIcone(couleur, RAYON_DISQUES / taillediviseur
                        , MARGE_DISQUES));
                break;
            case 2 :
                setIcon(new CroixCouleurIcone(couleur, RAYON_DISQUES / taillediviseur
                        , MARGE_DISQUES));
                break;
            case 3 :
                setIcon(new TriangleCouleurIcone(couleur, RAYON_DISQUES / taillediviseur
                        , MARGE_DISQUES));
                break;
            default :
                setIcon(new TriangleinverseCouleurIcone(couleur, RAYON_DISQUES / taillediviseur
                        , MARGE_DISQUES));
                break;
        }
    }    
    
  }

  static class GNoChainon extends JLabel {//pas de chainon mais du texte
    public GNoChainon(String contenu) {
      super(contenu);
      setFont(FONTE_ID);//police de texte
      setSize(21,13);
    }
    void localiser(Point chainonPosition, Dimension chainonTaille) {
      int x = chainonPosition.x+chainonTaille.width/2-getSize().width/2;
      int y = chainonPosition.y-getSize().height;
      setLocation(x, y);
    }
    
  }
  
  static class GNoSousChainon extends JLabel {//pas de chainon mais du texte
    public GNoSousChainon(String contenu) {
      super(contenu);
      setFont(FONTE_ID);//police de texte
      setSize(21,13);
    }
    void localiser(Point chainonPosition, Dimension chainonTaille) {
      int x = chainonPosition.x+chainonTaille.width/2-getSize().width/2;
      int y = chainonPosition.y+2*(RAYON_DISQUES+MARGE_DISQUES);
      setLocation(x, y);
    }
  }  

  private enum TypeSegment {//segment correspondant au paragraphe 
    COMPLET("", ""),
    DEBUT("", "-"),
    MILIEU("-", "-"),
    FIN("-", "");
    String marqueAvant, marqueApres;
    private TypeSegment(String avant, String apres) {
      marqueAvant = avant;
      marqueApres = apres;
    }
  }

  static class GSegment extends JLabel {//on ajoute des propriétés à un label texte
    int xmin, xmax;
    int ybas, yhaut;
    int no;
    TypeSegment type;
    GSegment(int xmin, int xmax, int ybas, int yhaut, int no, TypeSegment type) {
      super(type.marqueAvant+SYMBOLE_SEGMENT+Integer.toString(no)+type.marqueApres);
      setFont(FONTE_ID);//police de texte
      this.xmin = xmin;
      this.xmax = xmax;
      this.ybas = ybas;
      this.yhaut = yhaut;
      this.type = type;
      setSize(getPreferredSize());
    }
    void localiser() {//on localise où placer le symbole du paragraphe
      setLocation((xmin+xmax)/2-getSize().width/2, yhaut-getSize().height);
    }
    void dessinerTraits(Graphics graphics) {//dessine les traits du pragraphe
      Graphics2D g = (Graphics2D) graphics;
      g.setColor(Color.BLACK);
      Point p0 = new Point(xmin, ybas);
      Point p1 = new Point(xmin, yhaut);
      Point p2 = new Point(xmax, yhaut);
      Point p3 = new Point(xmax, ybas);
      switch (type) {
        case COMPLET:
          g.draw(new Line2D.Float(p0, p1));
          g.draw(new Line2D.Float(p1, p2));
          g.draw(new Line2D.Float(p2, p3));
          break;
        case DEBUT:
          g.draw(new Line2D.Float(p0, p1));
          g.draw(new Line2D.Float(p1, p2));
          break;
        case FIN:
          g.draw(new Line2D.Float(p1, p2));
          g.draw(new Line2D.Float(p2, p3));
          break;
        case MILIEU:
          g.draw(new Line2D.Float(p1, p2));
          break;
      }
    }
  }
  /*
   * Champs de la classeGChaines
   */
  VisuChaines visu;
  VisuCoref visucoref;
  Chainon[] chainons = new Chainon[0];
  int[] noSegments = new int[0];
  Color[] couleurs = new Color[0];
  int[] taille = new int[0];
  int[] forme = new int[0];
  String[] SurTexte = new String[0];
  String[] SousTexte = new String[0];  
  boolean couperparagraphe = false;
  GChainon[] gChainons;
  GNoChainon[] gNoChainon;
  GNoSousChainon[] gNoSousChainon;
  JButton exporter;
  boolean permettreSousTexte = false;
  ArrayList<GSegment> gSegments = new ArrayList<GSegment>();
  private int nbChainonsParLigne = 1000;
  GChaines(VisuChaines vc) {
    super((LayoutManager) null);
    setBackground(Color.WHITE);
    this.visu = vc;
    addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
       // visu.traiterAffTexteClicVide();
      }
    });
  }
  GChaines(VisuCoref vc) {
    super((LayoutManager) null);
    setBackground(Color.WHITE);
    this.visucoref = vc;
    addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
       // visu.traiterAffTexteClicVide();
      }
    });
  }  
  GChaines(VisuStatGenerale vc) {
    super((LayoutManager) null);
    setBackground(Color.WHITE);
    addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
       // visu.traiterAffTexteClicVide();
      }
    });
  }    
  void setNbChainonsParLigne(int nb) {
    nbChainonsParLigne = nb;
    afficher();
    colorer();
  }
  void setDonnees(Chainon[] chainons, int[] noSegments) {
    this.chainons = chainons;
    this.noSegments = noSegments;
    afficher();
  }
  void setcouperparagraphe(boolean couperparagraphe){
      this.couperparagraphe = couperparagraphe;
  }
  //cherche la longueur du paragraphe
  private int longueurpara(int[] nosegments,int deb){
      for(int  i = deb; i < nosegments.length - 1;i++){
          if(noSegments[i+1]>noSegments[i]){
              return i + 1 - deb;
          }
      }
      return -1;
  }
  private void afficher() {
    effacerTout();
    if (chainons.length==0) return;
    gChainons = new GChainon[chainons.length];
    int xCourant = MARGE_DEBUT_LIGNE+LARGEUR_INTER_CHAINONS;
    int yCourant = 0;
    gNoChainon = new GNoChainon[chainons.length];
    if(permettreSousTexte){
    gNoSousChainon = new GNoSousChainon[chainons.length];
    }
    int positionligne = 0;
    int xminSeg = MARGE_DEBUT_LIGNE+LARGEUR_INTER_CHAINONS/2+2;
    GSegment gseg;
    int xmaxSeg = 0;
    int xmaxligne = 0;
    for (int i = 0; i<chainons.length; i++) {
        if(visu==null){
      gChainons[i] = new GChainon(chainons[i], visu);
        }
        else{
        gChainons[i] = new GChainon(chainons[i], visucoref);    
        }
      add(gChainons[i]);
      gChainons[i].setLocation(xCourant, yCourant+HAUTEUR_CHAINONS);
      add(gNoChainon[i] = new GNoChainon(chainons[i].texte.length()<4 ? 
              chainons[i].texte.substring(0, chainons[i].texte.length()) 
              : chainons[i].texte.substring(0,4)));
      gNoChainon[i].localiser(gChainons[i].getLocation(), gChainons[i].getSize());
      if(permettreSousTexte){
      add(gNoSousChainon[i] = new GNoSousChainon(chainons[i].texte.length()<4 ? 
              chainons[i].texte.substring(0, chainons[i].texte.length()) 
              : chainons[i].texte.substring(0,4)));
      gNoSousChainon[i].localiser(gChainons[i].getLocation(), gChainons[i].getSize());  
      }
      xCourant += gChainons[i].getWidth()+LARGEUR_INTER_CHAINONS;
      xmaxligne = Math.max(xCourant, xmaxligne);
      xmaxSeg = xCourant-LARGEUR_INTER_CHAINONS/2-2;
      if (i+1==chainons.length||noSegments[i+1]>noSegments[i]) {
        // fin de paragraphe, donc on trace un segment terminal  
        if (xminSeg>0) {  // le paragraphe a commencé sur cette ligne
          add(gseg = new GSegment(xminSeg, xmaxSeg, yCourant+HAUTEUR_CHAINONS,
                  yCourant+HAUTEUR_SEGMENT, noSegments[i], TypeSegment.COMPLET));
        } else {  // le paragraphe a commencé avant cette ligne
          xminSeg = MARGE_DEBUT_LIGNE+LARGEUR_INTER_CHAINONS/2+2;
          add(gseg = new GSegment(xminSeg, xmaxSeg, yCourant+HAUTEUR_CHAINONS,
                  yCourant+HAUTEUR_SEGMENT, noSegments[i], TypeSegment.FIN));
        }
        gseg.localiser();
        gSegments.add(gseg);
        xminSeg = xmaxSeg+4;
      }
      if (i+1!=chainons.length && ((!couperparagraphe && (i+1)%nbChainonsParLigne==0) || 
              (couperparagraphe && (((positionligne+1)%nbChainonsParLigne==0) || (noSegments[i+1]>noSegments[i]
              && longueurpara(noSegments,i+1) <= nbChainonsParLigne && (nbChainonsParLigne - positionligne - 1) < 
              longueurpara(noSegments,i+1)))))) {  // on va changer de ligne
        if (xminSeg>xmaxSeg) {  // ça tombe bien : on change aussi de segment
          xminSeg = MARGE_DEBUT_LIGNE+LARGEUR_INTER_CHAINONS/2+2;
        } else { // ca tombe pas bien : il faut tracer un segment non terminal
          if (xminSeg>0) { // le paragraphe a commencé sur cette ligne
            add(gseg = new GSegment(xminSeg, xmaxSeg, yCourant+HAUTEUR_CHAINONS,
                    yCourant+HAUTEUR_SEGMENT, noSegments[i], TypeSegment.DEBUT));
          } else {  // le paragraphe a commencé avant cette ligne
            xminSeg = MARGE_DEBUT_LIGNE+LARGEUR_INTER_CHAINONS/2+2;
            add(gseg = new GSegment(xminSeg, xmaxSeg, yCourant+HAUTEUR_CHAINONS,
                    yCourant+HAUTEUR_SEGMENT, noSegments[i], TypeSegment.MILIEU));
          }
          gseg.localiser();
          gSegments.add(gseg);
          xminSeg = -1;
        }
        xCourant = MARGE_DEBUT_LIGNE+LARGEUR_INTER_CHAINONS;
        yCourant += HAUTEUR_LIGNES;
        positionligne = -1;
      }
      positionligne = positionligne + 1;
    }
    exporter = new JButton("exporter en SVG");
    exporter.setAction(new GAction("exporter en SVG") {
        public void executer() {
          exporterEnSvg();
        }
      });
    add(exporter);
    exporter.setLocation(100, yCourant +2*HAUTEUR_LIGNES);
    exporter.setSize(150, 40);
    exporter.setVisible(true);
    
    setPreferredSize(new Dimension(xmaxligne+30, yCourant+3*HAUTEUR_LIGNES+30));
  }
  void setCouleurs(Color[] couleurs) {//on affecte la couleur 
    this.couleurs = couleurs;
    colorer();
  }
  void setTaille(int[] tailles) {//on affecte la couleur 
    this.taille = tailles;
    Reduire();
  }
    void setForme(int[] formes) {//on affecte la couleur 
    this.forme = formes;
    Former();
  }
    void setSurTexte(String[] SurTexte) {//on affecte la couleur 
    this.SurTexte = SurTexte;
    SurTitrer();
  }
    void setSousTexte(String[] SousTexte) {//on affecte la couleur 
    this.SousTexte = SousTexte;
    SousTitrer();
  }
    
    
  private void colorer() {//on colore les chainons
      boolean taillenoninitialisee=false;
      boolean formenoninitialisee=false;
      boolean SurTextenoninitialisee=false;
      boolean SousTextenoninitialisee=false;
        if(couleurs.length != taille.length){ 
//dans le cas ou on n'a pas encore défini la tailleb et la forme des chainons
            this.taille = new int[gChainons.length];
            taillenoninitialisee = true;
        }
        if(couleurs.length != forme.length){
            this.forme = new int[gChainons.length];
            formenoninitialisee = true;
        }
        if(couleurs.length != SurTexte.length){
            this.SurTexte = new String[gChainons.length];
            SurTextenoninitialisee = true;
        }
        if(couleurs.length != SousTexte.length){
            this.SousTexte = new String[gChainons.length];
            SousTextenoninitialisee = true;
        }
    for (int i = 0; i<gChainons.length; i++) {
        if(taillenoninitialisee){
            this.taille[i] = 1;  
        }
        if(formenoninitialisee){
            this.forme[i] = 0;          
        }
        if(SurTextenoninitialisee){
            this.SurTexte[i] = "";          
        }
        if(SousTextenoninitialisee){
            this.SousTexte[i] = "";          
        }
        gChainons[i].changerCouleur(couleurs[i],this.taille[i],this.forme[i]);
        if(permettreSousTexte){
            gNoChainon[i].setText(this.SurTexte[i]);
            gNoSousChainon[i].setText(this.SousTexte[i]);
        }
        
    }
    if(exporter!=null){
        exporter.setVisible(true);
    }
    revalidate();
    repaint();
  }

  private void Reduire() {//on reduit les chainons
      boolean couleurnoninitialisee=false;
      boolean formenoninitialisee=false;
      boolean SurTextenoninitialisee=false;
      boolean SousTextenoninitialisee=false;
        if(taille.length != couleurs.length){ 
            this.couleurs = new Color[gChainons.length];
            couleurnoninitialisee = true;
        }
        if(taille.length != forme.length){
            this.forme = new int[gChainons.length];
            formenoninitialisee = true;
        }
        if(taille.length != SurTexte.length){
            this.SurTexte = new String[gChainons.length];
            SurTextenoninitialisee = true;
        }
        if(taille.length != SousTexte.length){
            this.SousTexte = new String[gChainons.length];
            SousTextenoninitialisee = true;
        }
    for (int i = 0; i<gChainons.length; i++) {
        if(couleurnoninitialisee){
            this.couleurs[i] = Color.WHITE;  
        }
        if(formenoninitialisee){
            this.forme[i] = 0;          
        }
        if(SurTextenoninitialisee){
            this.SurTexte[i] = "";          
        }
        if(SousTextenoninitialisee){
            this.SousTexte[i] = "";          
        }
        gChainons[i].changerCouleur(this.couleurs[i],taille[i],this.forme[i]);
        if(permettreSousTexte){
            gNoChainon[i].setText(this.SurTexte[i]);
            gNoSousChainon[i].setText(this.SousTexte[i]);
        }
    }
    if(exporter!=null){
        exporter.setVisible(true);
    }
    revalidate();
    repaint();
  }
  
  private void Former() {//on change la forme des chainons
      boolean couleurnoninitialisee=false;
      boolean taillenoninitialisee=false;
      boolean SurTextenoninitialisee=false;
      boolean SousTextenoninitialisee=false;
        if(forme.length != couleurs.length){ 
            this.couleurs = new Color[gChainons.length];
            couleurnoninitialisee = true;
        }
        if(forme.length != taille.length){
            this.taille = new int[gChainons.length];
            taillenoninitialisee = true;
        }
        if(forme.length != SurTexte.length){
            this.SurTexte = new String[gChainons.length];
            SurTextenoninitialisee = true;
        }
        if(forme.length != SousTexte.length){
            this.SousTexte = new String[gChainons.length];
            SousTextenoninitialisee = true;
        }
    for (int i = 0; i<gChainons.length; i++) {
        if(couleurnoninitialisee){
            this.couleurs[i] = Color.WHITE;  
        }
        if(taillenoninitialisee){
            this.taille[i] = 1;          
        }
        if(SurTextenoninitialisee){
            this.SurTexte[i] = "";          
        }
        if(SousTextenoninitialisee){
            this.SousTexte[i] = "";          
        }
        gChainons[i].changerCouleur(this.couleurs[i],this.taille[i],this.forme[i]);
        if(permettreSousTexte){
            gNoChainon[i].setText(this.SurTexte[i]);
            gNoSousChainon[i].setText(this.SousTexte[i]);
        }
    }
    if(exporter!=null){
        exporter.setVisible(true);
    }
    revalidate();
    repaint();
  }
  private void SurTitrer() {//on change le texte au dessus des chainons
      boolean couleurnoninitialisee=false;
      boolean taillenoninitialisee=false;
      boolean Formenoninitialisee=false;
      boolean SousTextenoninitialisee=false;
        if(SurTexte.length != couleurs.length){ 
            this.couleurs = new Color[gChainons.length];
            couleurnoninitialisee = true;
        }
        if(SurTexte.length != taille.length){
            this.taille = new int[gChainons.length];
            taillenoninitialisee = true;
        }
        if(SurTexte.length != forme.length){
            this.forme = new int[gChainons.length];
            Formenoninitialisee = true;
        }
        if(SurTexte.length != SousTexte.length){
            this.SousTexte = new String[gChainons.length];
            SousTextenoninitialisee = true;
        }
    for (int i = 0; i<gChainons.length; i++) {
        if(couleurnoninitialisee){
            this.couleurs[i] = Color.WHITE;  
        }
        if(taillenoninitialisee){
            this.taille[i] = 1;          
        }
        if(Formenoninitialisee){
            this.forme[i] = 0;          
        }
        if(SousTextenoninitialisee){
            this.SousTexte[i] = "";          
        }
        gChainons[i].changerCouleur(this.couleurs[i],this.taille[i],this.forme[i]);
        if(permettreSousTexte){
            gNoChainon[i].setText(this.SurTexte[i]);
            gNoSousChainon[i].setText(this.SousTexte[i]);
        }
    }
    if(exporter!=null){
        exporter.setVisible(true);
    }
    revalidate();
    repaint();
  } 
  
  private void SousTitrer() {//on change le texte au dessus des chainons
      boolean couleurnoninitialisee=false;
      boolean taillenoninitialisee=false;
      boolean Formenoninitialisee=false;
      boolean SurTextenoninitialisee=false;
        if(SousTexte.length != couleurs.length){ 
            this.couleurs = new Color[gChainons.length];
            couleurnoninitialisee = true;
        }
        if(SousTexte.length != taille.length){
            this.taille = new int[gChainons.length];
            taillenoninitialisee = true;
        }
        if(SousTexte.length != forme.length){
            this.forme = new int[gChainons.length];
            Formenoninitialisee = true;
        }
        if(SousTexte.length != SurTexte.length){
            this.SurTexte = new String[gChainons.length];
            SurTextenoninitialisee = true;
        }
    for (int i = 0; i<gChainons.length; i++) {
        if(couleurnoninitialisee){
            this.couleurs[i] = Color.WHITE;  
        }
        if(taillenoninitialisee){
            this.taille[i] = 1;          
        }
        if(Formenoninitialisee){
            this.forme[i] = 0;          
        }
        if(SurTextenoninitialisee){
            this.SurTexte[i] = "";          
        }
        gChainons[i].changerCouleur(this.couleurs[i],this.taille[i],this.forme[i]);
        if(permettreSousTexte){
            gNoChainon[i].setText(this.SurTexte[i]);
            gNoSousChainon[i].setText(this.SousTexte[i]);
        }
    }
    if(exporter!=null){
        exporter.setVisible(true);
    }
    revalidate();
    repaint();
  }  

  private void effacerTout() {//on efface tous les chainons
    removeAll();
    gChainons = new GChainon[0];
    gSegments.clear();
    repaint();
  }
  private void effacer(Graphics graphics) {//on efface le graphique 
    graphics.setColor(Color.WHITE);
    graphics.fillRect(0, 0, getWidth(), getHeight());
  }
  @Override
  protected void paintComponent(Graphics graphics) {//a voir
    effacer(graphics);
    for (GSegment gs : gSegments) gs.dessinerTraits(graphics);
  }
  public void autoriserSousTexte(boolean permettresoustexte){
      this.permettreSousTexte = permettresoustexte;
  } 
  public static String ecrireSVGIcon(GChainon icone){
      String balise ="";
      int x = icone.getX() + icone.getWidth()/2 - icone.getIcon().getIconWidth()/2;
      int y = icone.getY()+ icone.getHeight()/2 - icone.getIcon().getIconHeight()/2;
      if(icone.getIcon().getClass()== DisqueCouleurIcone.class){
          DisqueCouleurIcone figure = (DisqueCouleurIcone) icone.getIcon();
          balise="<ellipse cx=\""+(x + figure.getMarge() + figure.getRayon()
                  )+"\" cy=\""+(y + figure.getMarge() + figure.getRayon()
                  )+"\" rx=\""+ figure.getRayon()+"\" ry=\""+ figure.getRayon()+"\" "
                  + "style=\" fill:rgb("+figure.getColor().getRed()+","+figure.getColor().getGreen()
                  +","+figure.getColor().getBlue()+"); stroke:black\"/>"+System.getProperty("line.separator");
      }
      if(icone.getIcon().getClass()== CarreCouleurIcone.class){
          CarreCouleurIcone figure = (CarreCouleurIcone) icone.getIcon();
          balise="<rect x=\""+(x + figure.getMarge()) +
                  "\" y=\""+(y + figure.getMarge())+"\" width=\""+figure.getRayon()*2+"\" height=\""
                  +figure.getRayon()*2+"\" "+"style=\" fill:rgb("+figure.getColor().getRed()+
                  ","+figure.getColor().getGreen()+","+figure.getColor().getBlue()+"); stroke:black\"/>"
                  +System.getProperty("line.separator");
      }      
      if(icone.getIcon().getClass()== TriangleCouleurIcone.class){
          TriangleCouleurIcone figure = (TriangleCouleurIcone) icone.getIcon();
          balise="<polygon points=\"";
          balise = balise + (x + figure.getMarge()) + "," 
                  + (y + figure.getMarge()+ 2 * figure.getRayon());
          balise = balise + " " + (x + figure.getMarge() + figure.getRayon()) + "," 
                  + (y + figure.getMarge());
          balise = balise + " " + (x + figure.getMarge() + 2 *figure.getRayon()) + "," 
                  + (y + figure.getMarge()+ 2 * figure.getRayon());
          balise = balise + "\" "+"style=\" fill:rgb("+figure.getColor().getRed()+
                  ","+figure.getColor().getGreen()+","+figure.getColor().getBlue()+"); stroke:black\"/>"
                  +System.getProperty("line.separator");
      }       
      if(icone.getIcon().getClass()== CroixCouleurIcone.class){
          CroixCouleurIcone figure = (CroixCouleurIcone) icone.getIcon();
          balise="<polygon points=\"";
          balise = balise + (x + figure.getMarge()) + "," 
                  + (y + figure.getMarge()+figure.getRayon()-1);
          balise = balise + " " + (x + figure.getMarge() + figure.getRayon() - 1) + "," 
                  + (y + figure.getMarge()+figure.getRayon()-1);
          balise = balise + " " + (x + figure.getMarge() + figure.getRayon() - 1) + "," 
                  + (y + figure.getMarge());
          balise = balise + " " + (x + figure.getMarge() + figure.getRayon() + 1) + "," 
                  + (y + figure.getMarge());
          balise = balise + " " + (x + figure.getMarge() + figure.getRayon() + 1) + "," 
                  + (y + figure.getMarge()+figure.getRayon()-1);
          balise = balise + " " + (x + figure.getMarge() + 2 *figure.getRayon()) + "," 
                  + (y + figure.getMarge()+figure.getRayon()-1);
          balise = balise + " " + (x + figure.getMarge() + 2* figure.getRayon()) + "," 
                  + (y + figure.getMarge()+figure.getRayon()+1);
          balise = balise + " " + (x + figure.getMarge() + figure.getRayon() + 1) + "," 
                  + (y + figure.getMarge()+figure.getRayon()+1);
          balise = balise + " " + (x + figure.getMarge() + figure.getRayon() + 1) + "," 
                  + (y + figure.getMarge()+ 2 * figure.getRayon());
          balise = balise + " " + (x + figure.getMarge() + figure.getRayon() - 1) + "," 
                  + (y + figure.getMarge()+2*figure.getRayon());
          balise = balise + " " + (x + figure.getMarge() + figure.getRayon() - 1) + "," 
                  + (y + figure.getMarge()+figure.getRayon()+1);
          balise = balise + " " + (x + figure.getMarge()) + "," 
                  + (y + figure.getMarge()+figure.getRayon()+1);
          balise = balise + "\" "+"style=\" fill:rgb("+figure.getColor().getRed()+
                  ","+figure.getColor().getGreen()+","+figure.getColor().getBlue()+"); stroke:black\"/>"
                  +System.getProperty("line.separator");
      }        
      if(icone.getIcon().getClass()== TriangleinverseCouleurIcone.class){
          TriangleinverseCouleurIcone figure = (TriangleinverseCouleurIcone) icone.getIcon();
          balise="<polygon points=\"";
          balise = balise + (x + figure.getMarge()) + "," 
                  + (y + figure.getMarge());
          balise = balise + " " + (x + figure.getMarge() + 2*figure.getRayon()) + "," 
                  + (y + figure.getMarge());
          balise = balise + " " + (x + figure.getMarge() + figure.getRayon()) + "," 
                  + (y + figure.getMarge()+ 2 * figure.getRayon());
          balise = balise + "\" "+"style=\" fill:rgb("+figure.getColor().getRed()+
                  ","+figure.getColor().getGreen()+","+figure.getColor().getBlue()+"); stroke:black\"/>"
                  +System.getProperty("line.separator");
      }        
      return balise;
  }
  
  public static String ecrireSVGGsegment(GSegment seg){
      String balise = "";
      switch (seg.type) {
        case COMPLET:
            balise = balise + "<line x1=\""+seg.xmin+"\" y1=\""+seg.ybas+"\" x2=\""+seg.xmin+"\" y2=\""+seg.yhaut
                    +"\" style=\" fill:black; stroke:black\"/>"+System.getProperty("line.separator");
            balise = balise + "<line x1=\""+seg.xmin+"\" y1=\""+seg.yhaut+"\" x2=\""+seg.xmax+"\" y2=\""+seg.yhaut
                    +"\" style=\" fill:black; stroke:black\"/>"+System.getProperty("line.separator");
            balise = balise + "<line x1=\""+seg.xmax+"\" y1=\""+seg.yhaut+"\" x2=\""+seg.xmax+"\" y2=\""+seg.ybas
                    +"\" style=\" fill:black; stroke:black\"/>"+System.getProperty("line.separator");
          break;
        case DEBUT:
            balise = balise + "<line x1=\""+seg.xmin+"\" y1=\""+seg.ybas+"\" x2=\""+seg.xmin+"\" y2=\""+seg.yhaut
                    +"\" style=\" fill:black; stroke:black\"/>"+System.getProperty("line.separator");
            balise = balise + "<line x1=\""+seg.xmin+"\" y1=\""+seg.yhaut+"\" x2=\""+seg.xmax+"\" y2=\""+seg.yhaut
                    +"\" style=\" fill:black; stroke:black\"/>"+System.getProperty("line.separator");
          break;
        case FIN:
            balise = balise + "<line x1=\""+seg.xmin+"\" y1=\""+seg.yhaut+"\" x2=\""+seg.xmax+"\" y2=\""+seg.yhaut
                    +"\" style=\" fill:black; stroke:black\"/>"+System.getProperty("line.separator");
            balise = balise + "<line x1=\""+seg.xmax+"\" y1=\""+seg.yhaut+"\" x2=\""+seg.xmax+"\" y2=\""+seg.ybas
                    +"\" style=\" fill:black; stroke:black\"/>"+System.getProperty("line.separator");
          break;
        case MILIEU:
            balise = balise + "<line x1=\""+seg.xmin+"\" y1=\""+seg.yhaut+"\" x2=\""+seg.xmax+"\" y2=\""+seg.yhaut
                    +"\" style=\" fill:black; stroke:black\"/>"+System.getProperty("line.separator");
          break;
      }
      balise = balise + "<text x=\""+seg.getX()+"\" y=\""+(seg.getY() + seg.getHeight() - 4)+"\" "
              + "style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+seg.getText()+"</text>"
              +System.getProperty("line.separator");
      return balise;
  }
  
  public static String ecrireSVGSurTexte(GNoChainon sur){
      String balise = "<text x=\""+sur.getX()+"\" y=\""+(sur.getY() + sur.getHeight())+"\""
              + " style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+sur.getText()+
              "</text>"+System.getProperty("line.separator");
      return balise;
  }  
  public static String ecrireSVGSousTexte(GNoSousChainon sous){
      String balise = "<text x=\""+sous.getX()+"\" y=\""+(sous.getY() + sous.getHeight())+"\""
              + " style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+sous.getText()+
              "</text>"+System.getProperty("line.separator");
      return balise;
  }
  public static String ecrireSVGLegende (JTable legende,int posx, int posy){
      String balise = "";
      int x = posx;
      int y = posy;
      Object cellule;
      for(int i = 0 ; i < legende.getRowCount();i++){
          cellule = legende.getValueAt(i, 0)!=null ? legende.getValueAt(i, 0) : "99";
          if(cellule.getClass()!=String.class){
            y = y + (legende.getRowHeight(i) - ((Icon) cellule).getIconHeight())/2;
          }
          if(cellule.getClass()== DisqueCouleurIcone.class){
              DisqueCouleurIcone figure = (DisqueCouleurIcone) cellule;
              if(figure.getColor()!=null){
              balise= balise + "<ellipse cx=\""+(x + figure.getMarge() + figure.getRayon()
                      )+"\" cy=\""+(y + figure.getMarge() + figure.getRayon()
                      )+"\" rx=\""+ figure.getRayon()+"\" ry=\""+ figure.getRayon()+"\" "
                      + "style=\" fill:rgb("+figure.getColor().getRed()+","+figure.getColor().getGreen()
                      +","+figure.getColor().getBlue()+"); stroke:black\"/>"+System.getProperty("line.separator");
              }
              else{
              balise = balise + "<text x=\""+x+"\" y=\""+(y + (policeTailleSVG + legende.getRowHeight(i))/2
                      - (legende.getRowHeight(i) - ((Icon) cellule).getIconHeight())/2)+"\""
              + " style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+"X"+
              "</text>"+System.getProperty("line.separator");
              }
          }
          if(cellule.getClass()== CarreCouleurIcone.class){
              CarreCouleurIcone figure = (CarreCouleurIcone) cellule;
              if(figure.getColor()!=null){
              balise= balise + "<rect x=\""+(x + figure.getMarge()) +
                      "\" y=\""+(y + figure.getMarge())+"\" width=\""+figure.getRayon()*2+"\" height=\""
                      +figure.getRayon()*2+"\" "+"style=\" fill:rgb("+figure.getColor().getRed()+
                      ","+figure.getColor().getGreen()+","+figure.getColor().getBlue()+"); stroke:black\"/>"
                      +System.getProperty("line.separator");
              }
              else{
              balise = balise + "<text x=\""+x+"\" y=\""+(y + (policeTailleSVG + legende.getRowHeight(i))/2
                      - (legende.getRowHeight(i) - ((Icon) cellule).getIconHeight())/2)+"\""
              + " style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+"X"+
              "</text>"+System.getProperty("line.separator");
              }
              
          }      
          if(cellule.getClass()== TriangleCouleurIcone.class){
              TriangleCouleurIcone figure = (TriangleCouleurIcone) cellule;
              if(figure.getColor()!=null){
              balise= balise + "<polygon points=\"";
              balise = balise + (x + figure.getMarge()) + "," 
                      + (y + figure.getMarge()+ 2 * figure.getRayon());
              balise = balise + " " + (x + figure.getMarge() + figure.getRayon()) + "," 
                      + (y + figure.getMarge());
              balise = balise + " " + (x + figure.getMarge() + 2 *figure.getRayon()) + "," 
                      + (y + figure.getMarge()+ 2 * figure.getRayon());
              balise = balise + "\" "+"style=\" fill:rgb("+figure.getColor().getRed()+
                      ","+figure.getColor().getGreen()+","+figure.getColor().getBlue()+"); stroke:black\"/>"
                      +System.getProperty("line.separator");
              }
              else{
              balise = balise + "<text x=\""+x+"\" y=\""+(y + (policeTailleSVG + legende.getRowHeight(i))/2
                      - (legende.getRowHeight(i) - ((Icon) cellule).getIconHeight())/2)+"\""
              + " style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+"X"+
              "</text>"+System.getProperty("line.separator");
              }
          }       
          if(cellule.getClass()== CroixCouleurIcone.class){
              CroixCouleurIcone figure = (CroixCouleurIcone) cellule;
              if(figure.getColor()!=null){
              balise= balise + "<polygon points=\"";
              balise = balise + (x + figure.getMarge()) + "," 
                      + (y + figure.getMarge()+figure.getRayon()-1);
              balise = balise + " " + (x + figure.getMarge() + figure.getRayon() - 1) + "," 
                      + (y + figure.getMarge()+figure.getRayon()-1);
              balise = balise + " " + (x + figure.getMarge() + figure.getRayon() - 1) + "," 
                      + (y + figure.getMarge());
              balise = balise + " " + (x + figure.getMarge() + figure.getRayon() + 1) + "," 
                      + (y + figure.getMarge());
              balise = balise + " " + (x + figure.getMarge() + figure.getRayon() + 1) + "," 
                      + (y + figure.getMarge()+figure.getRayon()-1);
              balise = balise + " " + (x + figure.getMarge() + 2 *figure.getRayon()) + "," 
                      + (y + figure.getMarge()+figure.getRayon()-1);
              balise = balise + " " + (x + figure.getMarge() + 2* figure.getRayon()) + "," 
                      + (y + figure.getMarge()+figure.getRayon()+1);
              balise = balise + " " + (x + figure.getMarge() + figure.getRayon() + 1) + "," 
                      + (y + figure.getMarge()+figure.getRayon()+1);
              balise = balise + " " + (x + figure.getMarge() + figure.getRayon() + 1) + "," 
                      + (y + figure.getMarge()+ 2 * figure.getRayon());
              balise = balise + " " + (x + figure.getMarge() + figure.getRayon() - 1) + "," 
                      + (y + figure.getMarge()+2*figure.getRayon());
              balise = balise + " " + (x + figure.getMarge() + figure.getRayon() - 1) + "," 
                      + (y + figure.getMarge()+figure.getRayon()+1);
              balise = balise + " " + (x + figure.getMarge()) + "," 
                      + (y + figure.getMarge()+figure.getRayon()+1);
              balise = balise + "\" "+"style=\" fill:rgb("+figure.getColor().getRed()+
                      ","+figure.getColor().getGreen()+","+figure.getColor().getBlue()+"); stroke:black\"/>"
                      +System.getProperty("line.separator");
              }
              else{
              balise = balise + "<text x=\""+x+"\" y=\""+(y + (policeTailleSVG + legende.getRowHeight(i))/2
                      - (legende.getRowHeight(i) - ((Icon) cellule).getIconHeight())/2)+"\""
              + " style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+"X"+
              "</text>"+System.getProperty("line.separator");
              }
          }        
          if(cellule.getClass()== TriangleinverseCouleurIcone.class){
              TriangleinverseCouleurIcone figure = (TriangleinverseCouleurIcone) cellule;
              if(figure.getColor()!=null){
              balise= balise + "<polygon points=\"";
              balise = balise + (x + figure.getMarge()) + "," 
                      + (y + figure.getMarge());
              balise = balise + " " + (x + figure.getMarge() + 2*figure.getRayon()) + "," 
                      + (y + figure.getMarge());
              balise = balise + " " + (x + figure.getMarge() + figure.getRayon()) + "," 
                      + (y + figure.getMarge()+ 2 * figure.getRayon());
              balise = balise + "\" "+"style=\" fill:rgb("+figure.getColor().getRed()+
                      ","+figure.getColor().getGreen()+","+figure.getColor().getBlue()+"); stroke:black\"/>"
                      +System.getProperty("line.separator");
              }
              else{
              balise = balise + "<text x=\""+x+"\" y=\""+(y + (policeTailleSVG + legende.getRowHeight(i))/2
                      - (legende.getRowHeight(i) - ((Icon) cellule).getIconHeight())/2)+"\""
              + " style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+"X"+
              "</text>"+System.getProperty("line.separator");
              }
          }
          if(cellule.getClass() == String.class){
              String texte = cellule.toString();
              balise = balise + "<text x=\""+x+"\" y=\""+(y + legende.getRowHeight(i))+"\""
              + " style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+texte+
              "</text>"+System.getProperty("line.separator");
          }
          if(cellule.getClass()!=String.class){
            y = y - (legende.getRowHeight(i) - ((Icon) cellule).getIconHeight())/2;
          }
          y = y + legende.getRowHeight(i);
      }
      x = x + legende.getColumnModel().getColumn(0).getWidth();
      y = posy;
      for(int i = 0 ; i < legende.getRowCount();i++){
              String texte = legende.getValueAt(i, 1).toString();
              //si le texte commence par une balise html comme pour la valeur <autre> on enlève les balises
              if(texte.charAt(0)=='<'){
                  texte=texte.substring(1, texte.length()-1);
              }
              balise = balise + "<text x=\""+x+"\" y=\""+(y + (policeTailleSVG+legende.getRowHeight(i))/2)+"\""
              + " style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+texte+
              "</text>"+System.getProperty("line.separator");
              y = y + legende.getRowHeight(i);
      }
      x = x + legende.getColumnModel().getColumn(1).getWidth();
      y = posy;
      for(int i = 0 ; i < legende.getRowCount();i++){
              String texte = (legende.getValueAt(i, 2)==null ? "NC" : legende.getValueAt(i, 2).toString());
              balise = balise + "<text x=\""+x+"\" y=\""+(y + (policeTailleSVG+legende.getRowHeight(i))/2)+"\""
              + " style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+texte+
              "</text>"+System.getProperty("line.separator");
              y = y + legende.getRowHeight(i);
      }
      return balise;
  }  
  
  public void exporterEnSvg(){
      String graphique = "";
      String fin = "</svg>";
      for(GChainon figure : this.gChainons){
        graphique = graphique + ecrireSVGIcon(figure);
      }
      for(GSegment figure : this.gSegments){
        graphique = graphique + ecrireSVGGsegment(figure);
      }
      for(GNoChainon figure : this.gNoChainon){
        graphique = graphique + ecrireSVGSurTexte(figure);
      }
      if(this.gNoSousChainon!=null){
        for(GNoSousChainon figure : this.gNoSousChainon){
            graphique = graphique + ecrireSVGSousTexte(figure);
        }
      }
      int x = 40;
      int y = (int) Math.round(this.getPreferredSize().getHeight() + 30);
      if(this.visu==null){
          graphique = graphique + this.visucoref.getSelection(policeTailleSVG,x,y,policeSVG);
          y =  y + policeTailleSVG * 7;//on laisse de la place pour le texte
          y = y + 30;
      }
      else{
          graphique = graphique + this.visu.getSelection(policeTailleSVG,x,y,policeSVG);
          y =  y + policeTailleSVG * 10;//on laisse de la place pour le texte
          y = y +30;
      }
      int ymax = y;
      int xtot =  x;
      if(visu==null){
          graphique = graphique + "<text x=\""+x+"\" y=\""+(y + policeTailleSVG)+"\""
              + " style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+"Champ 1 :"+
              "</text>"+System.getProperty("line.separator");
          graphique = graphique + ecrireSVGLegende(visucoref.getLegende(),x,y+2*policeTailleSVG);
          ymax = y + visucoref.getLegende().getHeight()+2*policeTailleSVG;
          xtot = xtot + visucoref.getLegende().getWidth();
      }
      else{
          graphique = graphique + "<text x=\""+x+"\" y=\""+(y + policeTailleSVG)+"\""
              + " style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+"Champ 1 :"+
              "</text>"+System.getProperty("line.separator");
          graphique = graphique + ecrireSVGLegende(visu.getLegende(0),x,y+2*policeTailleSVG);
          x=x+visu.getLegende(0).getWidth() + 40;
          if(y + visu.getLegende(0).getHeight()+2*policeTailleSVG>ymax){
              ymax = y + visu.getLegende(0).getHeight()+2*policeTailleSVG;
          }
          xtot = xtot + visu.getLegende(0).getWidth();
          graphique = graphique + "<text x=\""+x+"\" y=\""+(y + policeTailleSVG)+"\""
              + " style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+"Champ 2 :"+
              "</text>"+System.getProperty("line.separator");
          graphique = graphique + ecrireSVGLegende(visu.getLegende(1),x,y+2*policeTailleSVG);
          x=x+visu.getLegende(1).getWidth() + 40;
          if(y + visu.getLegende(1).getHeight()+2*policeTailleSVG>ymax){
              ymax = y + visu.getLegende(1).getHeight()+2*policeTailleSVG;
          }
          xtot = xtot + visu.getLegende(1).getWidth();
          graphique = graphique + "<text x=\""+x+"\" y=\""+(y + policeTailleSVG)+"\""
              + " style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+"Champ 3 :"+
              "</text>"+System.getProperty("line.separator");
          graphique = graphique + ecrireSVGLegende(visu.getLegende(2),x,y+2*policeTailleSVG);
          x=x+visu.getLegende(2).getWidth() + 40;
          if(y + visu.getLegende(2).getHeight()+2*policeTailleSVG>ymax){
              ymax = y + visu.getLegende(2).getHeight()+2*policeTailleSVG;
          }
          xtot = xtot + visu.getLegende(2).getWidth();
          x= 40;
          y = ymax +20;
          graphique = graphique + "<text x=\""+x+"\" y=\""+(y + policeTailleSVG)+"\""
              + " style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+"Champ 4 :"+
              "</text>"+System.getProperty("line.separator");
          graphique = graphique + ecrireSVGLegende(visu.getLegende(3),x,y+2*policeTailleSVG);
          x=x+visu.getLegende(3).getWidth() + 40;
          if(y + visu.getLegende(3).getHeight()+2*policeTailleSVG>ymax){
              ymax = y + visu.getLegende(3).getHeight()+2*policeTailleSVG;
          }
          graphique = graphique + "<text x=\""+x+"\" y=\""+(y + policeTailleSVG)+"\""
              + " style=\" fill:black; stroke:black ;font-size:"+policeTailleSVG+"px "+policeSVG+"\">"+"Champ 5 :"+
              "</text>"+System.getProperty("line.separator");
          graphique = graphique + ecrireSVGLegende(visu.getLegende(4),x,y+2*policeTailleSVG);
          if(y + visu.getLegende(4).getHeight()+2*policeTailleSVG>ymax){
              ymax = y + visu.getLegende(4).getHeight()+2*policeTailleSVG;
          }
          xtot = xtot + visu.getLegende(4).getWidth();
      }
      String debut = "<?xml version=\"1.0\" standalone=\"no\"?>" +System.getProperty("line.separator")
             + "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">"
                +System.getProperty("line.separator")+ 
              "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\""+(Math.round(this.getPreferredSize().getWidth()) <
              xtot ? xtot : Math.round(this.getPreferredSize().getWidth()))+"\" height=\""+(ymax+100)+"\">"+
              System.getProperty("line.separator");
      FichiersGraphique.exporterGChaineSVG(debut + graphique + fin);
      
      
  }
 
}
 
 
