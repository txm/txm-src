/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package visuAnalec.chaines;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import org.jfree.chart.ChartPanel;
import visuAnalec.*;
import visuAnalec.elements.*;
import visuAnalec.fichiers.FichiersJava;
import visuAnalec.util.*;
import visuAnalec.util.GMessages.GAction;
import visuAnalec.util.GVisu.LegendeLabel;
import visuAnalec.util.GVisu.LegendeModeleTable;
import visuAnalec.util.GVisu.LegendeModeleTableTexte;
import visuAnalec.util.GVisu.TypeComboBox;
import visuAnalec.util.GVisu.Val0ComboBox;
import visuAnalec.vue.*;

/**
 *
 * @author Marc
 */
public class VisuCoref extends JFrame implements Vue.VueListener, ActionListener {
	
	private static final int[] seuilsValsNbElts = { 3, 10 };
	
	private static final Color[] couleursNbElts = { Color.WHITE, GVisu.getCouleurSansValeur(),
			GVisu.getCouleur(0), GVisu.getCouleur(3), GVisu.getCouleur(1) };
	
	private static final String[] labelsNbElements = {
			"pas de chaîne associée",
			"chaîne d'un seul chaînon",
			"chaîne de 2-" + Integer.toString(seuilsValsNbElts[0]) + " chaînons",
			"chaîne de " + Integer.toString(seuilsValsNbElts[0] + 1) + "-"
					+ Integer.toString(seuilsValsNbElts[1]) + "  chaînons",
			"chaîne de plus de " + Integer.toString(seuilsValsNbElts[1]) + " chaînons"
	};
	
	private final Vue vue;
	
	private final JMenuBar menuBar = new JMenuBar();
	
	private final ModeleChaines donneesChaines;
	
	private String typeChaines;
	
	private String typeUnites;
	
	private String[] valsAffichees;
	
	private HashSet<String> valsAffichees1cachees = new HashSet<>();
	
	private ArrayList<Integer> noparagraphecachees = new ArrayList<>();
	
	private String[] valsAffichees1;
	
	private Integer[] valsAffichees2;
	
	private final TypeComboBox saisieTypeChaines;
	
	private final TypeComboBox saisieTypeUnites;
	
	private Val0ComboBox saisieChampAffiche;
	
	private Val0ComboBox saisieChampChaine;
	
	private final Val0ComboBox saisieChampStatistique;
	
	private JTable legendeChampAffiche;
	
	private JTable legendeChampAfficheChaine;
	
	private JTable legendeChampAfficheParagraphe;
	
	private LegendeModeleTable legendeChampAfficheModele;
	
	private LegendeModeleTableTexte legendeChampAfficheChaineModele;
	
	private LegendeModeleTableTexte legendeChampAfficheParagrapheModele;
	
	private JPopupMenu popupChamp = new JPopupMenu();
	
	private JPopupMenu popupChampChaine = new JPopupMenu();
	
	private JPopupMenu popupChampParagraphe = new JPopupMenu();
	
	private final JCheckBox activehistogramme;
	
	private final Box listeGChaines;
	
	private JTable ResultatStat;
	
	private final HashSet<GChaines> suiteGChaines = new HashSet<>();
	
	private final String[] statpossibles = new String[] { "Répartition des références par paragraphe",
			"Répartition des paragraphes par référence", "Répartition des paragraphe par valeur du champ",
			"Répartition des valeurs du champ par paragraphe", "Répartition des valeurs de champ par référence",
			"Répartition des références par valeur de champ", "Répartition par paragraphe et par réfences "
					+ "des valeurs du champ", "Répartition par paragraphe et par valeur du champ des références",
			"Répartition par référence et par valeur de champ des paragraphes" };
	
	
	public VisuCoref(Container fenetre, Vue vue, PanneauEditeur pEditeur) {
		super("Analyse d'une chaine de coréférence");
		Point pos = new Point(fenetre.getX() + 20, fenetre.getY() + 20);
		Dimension dim = new Dimension(fenetre.getWidth(), fenetre.getHeight());
		setLocation(pos);
		setSize(dim);
		this.vue = vue;
		vue.addEventListener(this);
		saisieTypeChaines = new TypeComboBox(Schema.class);
		saisieTypeChaines.setModele(vue.getTypesSchemasAVoir());
		saisieTypeChaines.addActionListener(this);
		saisieTypeUnites = new TypeComboBox(Unite.class);
		saisieTypeUnites.setModele(vue.getTypesUnitesAVoir());
		saisieTypeUnites.addActionListener(this);
		saisieChampAffiche = new Val0ComboBox("Nombre de chaînons");
		saisieChampAffiche.addActionListener(this);
		saisieChampChaine = new Val0ComboBox("Rien");
		saisieChampChaine.addActionListener(this);
		saisieChampStatistique = new Val0ComboBox("Rien");
		saisieChampStatistique.addActionListener(this);
		Box commandes = Box.createVerticalBox();
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Chaînes : "));
		commandes.add(Box.createVerticalGlue());
		commandes.add(saisieTypeChaines);
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Unites : "));
		commandes.add(Box.createVerticalGlue());
		commandes.add(saisieTypeUnites);
		commandes.add(Box.createVerticalGlue());
		activehistogramme = new JCheckBox("Afficher les histogrammes de répartition");
		commandes.add(activehistogramme);
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Champ à filtrer : "));
		commandes.add(Box.createVerticalGlue());
		commandes.add(saisieChampAffiche);
		commandes.add(Box.createVerticalGlue());
		
		legendeChampAffiche = new JTable();
		legendeChampAffiche.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mousePressed(MouseEvent e) {// on gère les évènements de la souris
				check(e);
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				check(e);
			}
			
			public void check(MouseEvent e) {
				if (e.isPopupTrigger()) {// if the event shows the menu
					popupChampInit(legendeChampAffiche.rowAtPoint(e.getPoint()));
					popupChamp.show(legendeChampAffiche, e.getX(), e.getY());
				}
			}
		});
		
		commandes.add(legendeChampAffiche);
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Champ de la chaine à analyser : "));
		commandes.add(Box.createVerticalGlue());
		commandes.add(saisieChampChaine);
		commandes.add(Box.createVerticalGlue());
		
		legendeChampAfficheParagraphe = new JTable();
		// legendeChampAfficheParagraphe.setCellRenderer(new LegendeCellRenderer());
		legendeChampAfficheParagraphe.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mousePressed(MouseEvent e) {// on gère les évènements de la souris
				check(e);
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				check(e);
			}
			
			public void check(MouseEvent e) {
				if (e.isPopupTrigger()) {// if the event shows the menu
					popupChampInitParagraphe(legendeChampAfficheParagraphe.rowAtPoint(e.getPoint()));
					popupChampParagraphe.show(legendeChampAfficheParagraphe, e.getX(), e.getY());
				}
			}
		});
		
		
		
		legendeChampAfficheChaine = new JTable();
		// legendeChampAfficheChaine.setCellRenderer(new LegendeCellRenderer());
		legendeChampAfficheChaine.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mousePressed(MouseEvent e) {// on gère les évènements de la souris
				check(e);
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				check(e);
			}
			
			public void check(MouseEvent e) {
				if (e.isPopupTrigger()) {// if the event shows the menu
					popupChampInitChaine(legendeChampAfficheChaine.rowAtPoint(e.getPoint()));
					popupChampChaine.show(legendeChampAfficheChaine, e.getX(), e.getY());
				}
			}
		});
		commandes.add(legendeChampAfficheChaine);
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Paragraphe à filtrer : "));
		commandes.add(Box.createVerticalGlue());
		commandes.add(legendeChampAfficheParagraphe);
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Sélectionner une statistique : "));
		commandes.add(Box.createVerticalGlue());
		commandes.add(saisieChampStatistique);
		JScrollPane scrollLegende = new JScrollPane(commandes);
		listeGChaines = Box.createVerticalBox();
		JScrollPane scrollList = new JScrollPane(listeGChaines);
		Box resultatstat = Box.createVerticalBox();
		resultatstat.add(Box.createVerticalGlue());
		resultatstat.add(new JLabel("Statistique : "));
		resultatstat.add(Box.createVerticalGlue());
		ResultatStat = new JTable(new ModeleStatChaine());
		JScrollPane jsp = new JScrollPane(ResultatStat);
		resultatstat.add(ResultatStat.getTableHeader());
		resultatstat.add(jsp);
		JScrollPane scrollstatresultat = new JScrollPane(resultatstat);
		JSplitPane split1 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scrollList, scrollstatresultat);
		JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scrollLegende, split1);
		getContentPane().add(split);
		setVisible(true);
		split.setDividerLocation(1. / 3);
		split1.setDividerLocation(1. / 2);
		setJMenuBar(menuBar);
		JMenu corpusMenu = new JMenu("Paramètres d'affichage");
		menuBar.add(corpusMenu);
		corpusMenu.add(new GAction("Reprendre un affichage") {
			
			@Override
			public void executer() {
				importerAffichage();
			}
		});
		corpusMenu.add(new GAction("Sauver les paramètres d'affichage") {
			
			@Override
			public void executer() {
				exporterAffichage();
			}
		});
		JMenu csvMenu = new JMenu("Exporter les données");
		final VisuCoref visu = this;
		final Vue vues = this.vue;
		final PanneauEditeur pEditeurs = pEditeur;
		menuBar.add(csvMenu);
		csvMenu.add(new GAction("Exporter les données de filtrage") {
			
			@Override
			public void executer() {
				new ExportCSV(getContentPane(), vues, pEditeurs, visu);
			}
		});
		csvMenu.add(new GAction("Exporter le tableau statistique") {
			
			@Override
			public void executer() {
				Object[][] donneesCSV = new Object[ResultatStat.getRowCount() + 1][ResultatStat.getColumnCount()];
				for (int i = 0; i < ResultatStat.getRowCount(); i++) {
					for (int j = 0; j < ResultatStat.getColumnCount(); j++) {
						donneesCSV[i + 1][j] = ResultatStat.getValueAt(i, j);
					}
				}
				for (int j = 0; j < ResultatStat.getColumnCount(); j++) {
					donneesCSV[0][j] = ResultatStat.getColumnName(j);
				}
				
				ExportCSV.exporterCSV(donneesCSV);
			}
		});
		csvMenu.add(new GAction("Exporter les fréquences pour le champ des unités") {
			
			@Override
			public void executer() {
				Object[][] donneesCSV = new Object[legendeChampAffiche.getRowCount() + 1][2];
				for (int i = 0; i < legendeChampAffiche.getRowCount(); i++) {
					donneesCSV[i + 1][0] = legendeChampAffiche.getValueAt(i, 1);
					donneesCSV[i + 1][1] = legendeChampAffiche.getValueAt(i, 2) == null ? "0"
							: legendeChampAffiche.getValueAt(i, 2);
				}
				donneesCSV[0][1] = "frequence";
				donneesCSV[0][0] = saisieChampAffiche.pasdeSelection() ? "Nombre de Chainons"
						: saisieChampAffiche.getSelection();
				ExportCSV.exporterCSV(donneesCSV);
			}
		});
		csvMenu.add(new GAction("Exporter les fréquences pour le champ des chaines") {
			
			@Override
			public void executer() {
				if (!saisieChampChaine.pasdeSelection()) {
					Object[][] donneesCSV = new Object[legendeChampAfficheChaine.getRowCount() + 1][2];
					for (int i = 0; i < legendeChampAfficheChaine.getRowCount(); i++) {
						donneesCSV[i + 1][0] = legendeChampAfficheChaine.getValueAt(i, 1);
						donneesCSV[i + 1][1] = legendeChampAfficheChaine.getValueAt(i, 2) == null ? "0"
								: legendeChampAfficheChaine.getValueAt(i, 2);
					}
					donneesCSV[0][1] = "frequence";
					donneesCSV[0][0] = saisieChampChaine.getSelection();
					ExportCSV.exporterCSV(donneesCSV);
				}
			}
		});
		JMenu typeMenu = new JMenu("Fusionner des types d'unités");
		menuBar.add(typeMenu);
		final Vue vueconv = vue;
		typeMenu.add(new GAction("Fusionner tous les types") {
			
			@Override
			public void executer() {
				JOptionPane jop = new JOptionPane();
				int option = jop.showConfirmDialog(null, "Voulez-créer un nouveau type rassemblant tous les autres ?"
						+ System.getProperty("line.separator") + "Si oui, la fenêtre de visualisation des chaines se fermera pour mettre à jour les annotations.", "Création d'un type d'unités",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				
				if (option == JOptionPane.OK_OPTION) {
					ChaineUtil.fusionTypeUnite(vueconv);
					fermerFenetre();
				}
			}
		});
		saisieTypeChaines.setAlignmentX(0);
		saisieTypeUnites.setAlignmentX(0);
		saisieChampAffiche.setAlignmentX(0);
		saisieChampChaine.setAlignmentX(0);
		saisieChampStatistique.setAlignmentX(0);
		legendeChampAffiche.setAlignmentX(0);
		legendeChampAfficheChaine.setAlignmentX(0);
		legendeChampAfficheParagraphe.setAlignmentX(0);
		
		// Fermeture de la fenêtre
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent evt) {
				fermerFenetre();
			}
		});
		donneesChaines = new ModeleChaines(vue, new GChaines(this));
		if (saisieTypeChaines.nbItems() == 1) {
			saisieTypeChaines.setSelection(0);
			if (saisieTypeUnites.nbItems() == 1) saisieTypeUnites.setSelection(0);
			else saisieTypeUnites.effSelection();
		}
		else {
			saisieTypeChaines.effSelection();
			saisieTypeUnites.effSelection();
		}
	}
	
	private void fermerFenetre() {
		vue.removeEventListener(this);
		dispose();
	}
	
	@Override
	public void traiterEvent(Message evt) { // très grossier : à améliorer
		switch (evt.getType()) {
			case CLEAR_CORPUS:
				fermerFenetre();
				return;
			case NEW_CORPUS:  // impossible en principe : CLEAR_CORPUS a détruit la fenêtre
				throw new UnsupportedOperationException("Fenêtre chaines présente lors d'un message NEW_CORPUS ???");
			case MODIF_TEXTE:
				fermerFenetre();
				return;
			case MODIF_VUE:
				fermerFenetre();
				return;
			case MODIF_STRUCTURE:
				fermerFenetre();
				return;
			case MODIF_ELEMENT:
				// ElementEvent evtE = (ElementEvent) evt;
				// if (evtE.getModif() != TypeModifElement.BORNES_UNITE) fermerFenetre();
				fermerFenetre();
				return;
			case CORPUS_SAVED:
				fermerFenetre();
				return;
			default:
				throw new UnsupportedOperationException("Cas " + evt.getType() + " oublié dans un switch");
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		try {
			if (evt.getSource().equals(saisieTypeChaines)
					|| evt.getSource().equals(saisieTypeUnites)) {
				if (saisieTypeChaines.pasdeSelection() || saisieTypeUnites.pasdeSelection()) {
					initVide();
					return;
				}
				try {
					init();
				}
				catch (UnsupportedOperationException ex) {
					GMessages.impossibiliteAction("Erreur de type : " + ex.getMessage());
					initVide();
					return;
				}
				return;
			}
			if (evt.getSource().equals(saisieChampAffiche)) {
				if (!saisieTypeChaines.pasdeSelection() && !saisieTypeUnites.pasdeSelection()) {
					if (saisieChampAffiche.pasdeSelection()) initCouleursNbElts();
					else initCouleursChamp(saisieChampAffiche.getSelection());
					afficherchaines();
				}
				return;
			}
			if (evt.getSource().equals(saisieChampChaine)) {
				initChaineChamp();
				afficherchaines();
				return;
			}
			if (evt.getSource().equals(saisieChampStatistique)) {
				afficherstat();
				return;
			}
			throw new UnsupportedOperationException("Evénement non prévu : " + evt.paramString());
		}
		catch (Throwable ex) {
			GMessages.erreurFatale(ex);
		}
	}
	
	private void initVide() {
		donneesChaines.permettreaffichage(false);
		typeChaines = null;
		typeUnites = null;
		donneesChaines.initVide();
		saisieChampAffiche.setModeleVide();
		saisieChampChaine.setModeleVide();
		saisieChampStatistique.setModeleVide();
		legendeChampAfficheModele = new LegendeModeleTable(new String[] { "icone", "nom du champ" }, new LegendeLabel[0], new HashMap());
		legendeChampAffiche.setModel(legendeChampAfficheModele);
		legendeChampAfficheChaineModele = new LegendeModeleTableTexte(new String[] { "Affiché", "nom du champ", "frequence" }, new LegendeLabel[0], new HashMap());
		legendeChampAfficheChaine.setModel(legendeChampAfficheChaineModele);
		legendeChampAfficheParagrapheModele = new LegendeModeleTableTexte(new String[] { "Affiché", "nom du champ", "frequence" }, new LegendeLabel[0], new HashMap());
		legendeChampAfficheParagraphe.setModel(legendeChampAfficheParagrapheModele);
	}
	
	private void init() throws UnsupportedOperationException {
		donneesChaines.permettreaffichage(false);
		typeChaines = saisieTypeChaines.getTypeSelection();
		typeUnites = saisieTypeUnites.getTypeSelection();
		donneesChaines.init(typeChaines, typeUnites, seuilsValsNbElts);
		initParagraphechamp();
		initCouleursNbElts();
		afficherchaines();
		saisieChampAffiche.removeActionListener(this);
		saisieChampAffiche.setModele(vue.getNomsChamps(Unite.class, typeUnites));
		saisieChampAffiche.addActionListener(this);
		saisieChampChaine.removeActionListener(this);
		saisieChampChaine.setModele(vue.getNomsChamps(Schema.class, typeChaines));
		saisieChampChaine.addActionListener(this);
		saisieChampStatistique.removeActionListener(this);
		saisieChampStatistique.setModele(statpossibles);
		saisieChampStatistique.addActionListener(this);
		// histochaine.changeHistogramme(donneesChaines.dispositionunites(10),Color.BLUE,this,200);
		// initstat();
		
	}
	
	private void initCouleursNbElts() {
		if (!saisieChampChaine.pasdeSelection()) {
			donneesChaines.setChampAfficheNbElts(couleursNbElts);
		}
		if (!donneesChaines.peutetreafficher()) {
			HashMap<String, Float> freq = donneesChaines.calculerfrequencenbelt(ChaineUtil.filtreparagraphe(saisieChampChaine.pasdeSelection() ? donneesChaines.getUnites()
					: donneesChaines.filtrechaine(donneesChaines.getUnites(), saisieChampChaine.getSelection(),
							valsAffichees1cachees), noparagraphecachees, vue), typeUnites, labelsNbElements);
			legendeChampAfficheModele = new LegendeModeleTable(new String[] { "icone", "nom du champ", "frequence" },
					creerLegendesNbElements(), freq);
			legendeChampAffiche.setModel(legendeChampAfficheModele);
			legendeChampAffiche.getColumnModel().getColumn(0).setMaxWidth(50);
			legendeChampAffiche.getColumnModel().getColumn(2).setMaxWidth(50);
			legendeChampAffiche.setRowHeight(30);
		}
	}
	
	private LegendeLabel[] creerLegendesNbElements() {
		LegendeLabel[] legendes = new LegendeLabel[couleursNbElts.length];
		for (int i = 0; i < legendes.length; i++)
			legendes[i] = new LegendeLabel(labelsNbElements[i],
					donneesChaines.isMasqueeValNbElements(i) ? null : couleursNbElts[i]);
		return legendes;
	}
	
	private void initCouleursChamp(String champ) {
		valsAffichees = vue.getValeursChamp(Unite.class, typeUnites, champ);
		HashMap<String, Color> couleursValeursChamp = new HashMap<>();
		for (int i = 0, nocolor = 0; i < valsAffichees.length; i++)
			if (!donneesChaines.isMasqueeValChamp(champ, valsAffichees[i]))
				couleursValeursChamp.put(valsAffichees[i], GVisu.getCouleur(nocolor++));
		if (!donneesChaines.isMasqueeValNulleChamp(champ))
			couleursValeursChamp.put("", GVisu.getCouleurSansValeur());
		donneesChaines.setChampAffiche(champ, couleursValeursChamp);
		if (!donneesChaines.peutetreafficher()) {
			HashMap<String, Float> freq = ChaineUtil.calculerfrequencechamp(ChaineUtil.filtreparagraphe(saisieChampChaine.pasdeSelection() ? donneesChaines.getUnites()
					: donneesChaines.filtrechaine(donneesChaines.getUnites(), saisieChampChaine.getSelection(),
							valsAffichees1cachees), noparagraphecachees, vue), typeUnites, champ, vue);
			ChaineUtil.triFusionMap(valsAffichees, freq);
			legendeChampAfficheModele = new LegendeModeleTable(new String[] { "icone", "nom du champ", "frequence" },
					creerLegendesChamp(valsAffichees, couleursValeursChamp), freq);
			legendeChampAffiche.setModel(legendeChampAfficheModele);
			legendeChampAffiche.getColumnModel().getColumn(0).setMaxWidth(50);
			legendeChampAffiche.getColumnModel().getColumn(2).setMaxWidth(50);
			legendeChampAffiche.setRowHeight(30);
		}
	}
	
	private LegendeLabel[] creerLegendesChamp(String[] valsAffichees,
			HashMap<String, Color> couleursValeursChamp) {
		LegendeLabel[] legendes = new LegendeLabel[valsAffichees.length + 1];
		for (int i = 0; i < valsAffichees.length; i++)
			legendes[i] = new LegendeLabel(valsAffichees[i],
					couleursValeursChamp.get(valsAffichees[i]));
		legendes[valsAffichees.length] = new LegendeLabel("<aucune valeur>",
				couleursValeursChamp.get(""));
		return legendes;
	}
	
	// initialise la Jlist des paragraphes
	private void initParagraphechamp() {
		valsAffichees2 = donneesChaines.listenumeroparagraphe();
		LegendeLabel[] legende = new LegendeLabel[valsAffichees2.length];
		for (int i = 0; i < valsAffichees2.length; i++) {
			int debutParagraphe = valsAffichees2[i] - 2 < 0 ? 0 : vue.getCorpus().getFinParagraphes()[valsAffichees2[i] - 2];
			// on enlève 2 au paragraphe car quand on cherche le début d'un paragraphe on cherche la fin du précédent
			// et de plus les paragraphes commencent à 1 quand les index de tableau commencent à 1
			int longueur = debutParagraphe < vue.getCorpus().getTexte().length() - 20 ? 20 : vue.getCorpus().getTexte().length() - debutParagraphe;
			if (noparagraphecachees.contains(valsAffichees2[i])) {
				legende[i] = new LegendeLabel("Paragraphe" + valsAffichees2[i].toString() + "  \"" +
						vue.getCorpus().getTexte().substring(debutParagraphe, debutParagraphe + longueur) + "\"", "Non");
			}
			else {
				legende[i] = new LegendeLabel("Paragraphe" + valsAffichees2[i].toString() + "  \"" +
						vue.getCorpus().getTexte().substring(debutParagraphe, debutParagraphe + longueur) + "\"", "Oui");
			}
		}
		if (!donneesChaines.peutetreafficher()) {
			legendeChampAfficheParagrapheModele = new LegendeModeleTableTexte(new String[] { "Affiché", "Paragraphe" }, legende, null);
			legendeChampAfficheParagraphe.setModel(legendeChampAfficheParagrapheModele);
			legendeChampAfficheParagraphe.getColumnModel().getColumn(0).setMaxWidth(50);
		}
	}
	
	// initialise la legende des valeurs de la Chaine
	private void initChaineChamp() {
		if (!donneesChaines.peutetreafficher()) {
			valsAffichees1 = vue.getValeursChamp(Schema.class, typeChaines, saisieChampChaine.getSelection());
			LegendeLabel[] legende = new LegendeLabel[valsAffichees1.length];
			HashMap<String, Float> freq;
			freq = donneesChaines.calculerfrequencechaine(ChaineUtil.filtreparagraphe(donneesChaines.getUnites(), noparagraphecachees, vue), typeChaines, saisieChampChaine.getSelection(),
					valsAffichees1cachees);
			ChaineUtil.triFusionMap(valsAffichees1, freq);
			for (int i = 0; i < valsAffichees1.length; i++) {
				if (valsAffichees1cachees.contains(valsAffichees1[i])) {
					legende[i] = new LegendeLabel(valsAffichees1[i].toString(), "Non");
				}
				else {
					legende[i] = new LegendeLabel(valsAffichees1[i].toString(), "Oui");
				}
			}
			legendeChampAfficheChaineModele = new LegendeModeleTableTexte(new String[] { "Affiché", "Paragraphe", "frequence" }, legende, freq);
			legendeChampAfficheChaine.setModel(legendeChampAfficheChaineModele);
			legendeChampAfficheChaine.getColumnModel().getColumn(0).setMaxWidth(50);
			legendeChampAfficheChaine.getColumnModel().getColumn(2).setMaxWidth(50);
		}
	}
	
	
	private void popupChampInit(final int index) {
		final boolean masquee;
		String[] vals = valsAffichees;
		Val0ComboBox zonedesaisie = saisieChampAffiche;
		JPopupMenu popup = popupChamp;
		popup.removeAll();
		if (zonedesaisie.pasdeSelection()) {
			masquee = donneesChaines.isMasqueeValNbElements(index);
			popup.add(new GAction(masquee ? "Montrer cette valeur" : "Masquer cette valeur") {
				
				@Override
				public void executer() {
					modifierStatutValNbElements(masquee, index, false);
				}
			});
			popup.add(new GAction(masquee ? "Montrer toutes les valeurs" : "Ne montrer que cette valeur") {
				
				@Override
				public void executer() {
					modifierStatutValNbElements(masquee, index, true);
				}
			});
		}
		else if (index == vals.length) {
			masquee = donneesChaines.isMasqueeValNulleChamp(zonedesaisie.getSelection());
			popup.add(new GAction(masquee ? "Montrer cette valeur" : "Masquer cette valeur") {
				
				@Override
				public void executer() {
					modifierStatutValNulleChamp(masquee, false);
				}
			});
			popup.add(new GAction(masquee ? "Montrer toutes les valeurs" : "Ne montrer que cette valeur") {
				
				@Override
				public void executer() {
					modifierStatutValNulleChamp(masquee, true);
				}
			});
		}
		else {
			masquee = donneesChaines.isMasqueeValChamp(zonedesaisie.getSelection(),
					vals[index]);
			popup.add(new GAction(masquee ? "Montrer cette valeur" : "Masquer cette valeur") {
				
				@Override
				public void executer() {
					modifierStatutValChamp(masquee, index, false);
				}
			});
			popup.add(new GAction(masquee ? "Montrer toutes les valeurs" : "Ne montrer que cette valeur") {
				
				@Override
				public void executer() {
					modifierStatutValChamp(masquee, index, true);
				}
			});
		}
		
	}
	
	private void modifierStatutValChamp(boolean masquee, int index, boolean tous) {
		String[] vals;
		Val0ComboBox zonedesaisie;
		vals = valsAffichees;
		zonedesaisie = saisieChampAffiche;
		if (!tous) donneesChaines.masquerValChamp(!masquee,
				zonedesaisie.getSelection(), vals[index]);
		else if (masquee) donneesChaines.demasquerToutesValsChamp(
				zonedesaisie.getSelection());
		else donneesChaines.masquerAutresValsChamp(typeUnites,
				zonedesaisie.getSelection(), vals[index]);
		if (!saisieChampChaine.pasdeSelection()) {
			if (saisieChampAffiche.pasdeSelection()) {
				initCouleursNbElts();
			}
			else {
				initCouleursChamp(saisieChampAffiche.getSelection());
			}
		}
		afficherchaines();
	}
	
	private void modifierStatutValNulleChamp(boolean masquee, boolean tous) {
		Val0ComboBox zonedesaisie;
		zonedesaisie = saisieChampAffiche;
		if (!tous) donneesChaines.masquerValNulleChamp(!masquee, zonedesaisie.getSelection());
		else if (masquee) donneesChaines.demasquerToutesValsChamp(
				zonedesaisie.getSelection());
		else donneesChaines.masquerValsNonNullesChamp(typeUnites,
				zonedesaisie.getSelection());
		if (!saisieChampChaine.pasdeSelection()) {
			if (saisieChampAffiche.pasdeSelection()) {
				initCouleursNbElts();
			}
			else {
				initCouleursChamp(saisieChampAffiche.getSelection());
			}
		}
		afficherchaines();
	}
	
	private void modifierStatutValNbElements(boolean masquee, int index, boolean tous) {
		Val0ComboBox zonedesaisie;
		zonedesaisie = saisieChampAffiche;
		if (!tous) donneesChaines.masquerValNbElements(!masquee, index);
		else if (masquee) donneesChaines.demasquerToutesValsNbElts(
				zonedesaisie.getSelection());
		else donneesChaines.masquerAutresValsNbElts(
				zonedesaisie.getSelection(), index);
		if (!saisieChampChaine.pasdeSelection()) {
			if (saisieChampAffiche.pasdeSelection()) {
				initCouleursNbElts();
			}
			else {
				initCouleursChamp(saisieChampAffiche.getSelection());
			}
		}
		afficherchaines();
	}
	
	public void afficherchaines() {
		donneesChaines.permettreaffichage(true);
		this.suiteGChaines.clear();
		listeGChaines.removeAll();
		if (!saisieChampChaine.pasdeSelection() && !saisieTypeChaines.pasdeSelection()
				&& !saisieTypeUnites.pasdeSelection()) {
			for (String val : valsAffichees1) { // pour toutes les valeurs de la propriété de la chaine
				if (!valsAffichees1cachees.contains(val)) {
					listeGChaines.add(Box.createVerticalGlue());
					GChaines graphique = new GChaines(this);
					donneesChaines.setGChaine(graphique);
					donneesChaines.modifMasqueValsChampChaine(saisieTypeChaines.getSelection(), saisieTypeUnites.getSelection(), noparagraphecachees, saisieChampChaine.getSelection(),
							val);
					if (!saisieChampAffiche.pasdeSelection()) {
						initCouleursChamp(saisieChampAffiche.getSelection());
					}
					else {
						initCouleursNbElts();
					}
					listeGChaines.add(new JLabel(val + " :"));
					listeGChaines.add(graphique);
					suiteGChaines.add(graphique);
					if (activehistogramme.isSelected()) {
						ChartPanel histochaine = Histogramme.histogrammeFactory();
						Histogramme.updatePanel(histochaine, "Répartion des unités", donneesChaines, 10, Color.blue, 200,
								"position de l'unité", "Fréquence d'unités", vue);
						histochaine.setMaximumSize(new Dimension(550, 300));
						listeGChaines.add(histochaine);
					}
				}
			}
		}
		donneesChaines.permettreaffichage(false);
		GChaines graphique = new GChaines(this);
		donneesChaines.setGChaine(graphique);
		donneesChaines.modifMasqueVals(saisieTypeChaines.getSelection(), saisieTypeUnites.getSelection());
		if (!saisieChampChaine.pasdeSelection()) {
			if (saisieChampAffiche.pasdeSelection()) {
				initCouleursNbElts();
			}
			else {
				initCouleursChamp(saisieChampAffiche.getSelection());
			}
			initChaineChamp();
			initParagraphechamp();
		}
		afficherstat();
		listeGChaines.revalidate();
		listeGChaines.repaint();
	}
	
	private void popupChampInitChaine(final int index) {
		final boolean masquee;
		Val0ComboBox zonedesaisie = saisieChampChaine;
		JPopupMenu popup = popupChampChaine;
		popup.removeAll();
		if (zonedesaisie.pasdeSelection()) {
		}
		else {
			masquee = valsAffichees1cachees.contains(valsAffichees1[index]);
			popup.add(new GAction(masquee ? "Montrer cette valeur de la chaine" : "Masquer cette valeur de la chaine") {
				
				@Override
				public void executer() {
					modifierStatutValChampChaine(masquee, index, false);
				}
			});
			popup.add(new GAction(masquee ? "Montrer toutes les valeurs de la chaine" : "Ne montrer que cette valeur de la chaine") {
				
				@Override
				public void executer() {
					modifierStatutValChampChaine(masquee, index, true);
				}
			});
		}
		
	}
	
	private void modifierStatutValChampChaine(boolean masquee, int index, boolean tous) {
		if (!tous) {
			if (masquee) {
				valsAffichees1cachees.remove(valsAffichees1[index]);
			}
			else {
				valsAffichees1cachees.add(valsAffichees1[index]);
			}
		}
		else if (masquee) {
			valsAffichees1cachees.clear();
		}
		else {
			valsAffichees1cachees.clear();
			for (String valeur : valsAffichees1) {
				if (!valeur.equals(valsAffichees1[index])) {
					valsAffichees1cachees.add(valeur);
				}
			}
			
		}
		initChaineChamp();
		if (!saisieChampChaine.pasdeSelection()) {
			afficherchaines();
		}
	}
	
	private void popupChampInitParagraphe(final int index) {
		final boolean masquee;
		Val0ComboBox zonedesaisie = saisieChampChaine;
		JPopupMenu popup = popupChampParagraphe;
		popup.removeAll();
		if (zonedesaisie.pasdeSelection()) {
		}
		else {
			masquee = noparagraphecachees.contains(valsAffichees2[index]);
			popup.add(new GAction(masquee ? "Montrer ce paragraphe" : "Masquer ce paragraphe") {
				
				@Override
				public void executer() {
					modifierStatutValChampParagraphe(masquee, index, false);
				}
			});
			popup.add(new GAction(masquee ? "Montrer tous les paragraphes" : "Ne montrer que ce paragraphe") {
				
				@Override
				public void executer() {
					modifierStatutValChampParagraphe(masquee, index, true);
				}
			});
		}
		
	}
	
	private void modifierStatutValChampParagraphe(boolean masquee, int index, boolean tous) {
		if (!tous) { // on masque ou demasque un paragraphe
			if (masquee) {
				noparagraphecachees.remove(valsAffichees2[index]);
			}
			else {
				noparagraphecachees.add(valsAffichees2[index]);
			}
		}
		else if (masquee) { // on demasque tous les paragraphes
			noparagraphecachees.clear();
		}
		else { // on masque tous les autres paragraphes
			noparagraphecachees.clear();
			for (Integer numero : valsAffichees2) {
				if (numero.intValue() != valsAffichees2[index]) {
					noparagraphecachees.add(numero);
				}
			}
		}
		initParagraphechamp();
		if (!saisieChampChaine.pasdeSelection()) {
			afficherchaines();
		}
	}
	
	public void afficherstat() {
		if (!saisieChampStatistique.pasdeSelection() && !saisieChampChaine.pasdeSelection()
				&& !saisieTypeChaines.pasdeSelection() && !saisieTypeUnites.pasdeSelection()) {
			ModeleStatChaine Modelestat = (ModeleStatChaine) ResultatStat.getModel();
			String typeChampColonne;
			String[] typeChampLigne;
			switch (saisieChampStatistique.getSelection()) {
				case "Répartition des références par paragraphe":
					
					typeChampColonne = "Chaine";
					typeChampLigne = new String[1];
					typeChampLigne[0] = "Paragraphe";
					Modelestat = ModeleStatChaine.calculateStatCoref(saisieChampChaine.getSelection(), saisieChampAffiche.getSelection(),
							valsAffichees1cachees, typeChampColonne, typeChampLigne, vue,
							donneesChaines, noparagraphecachees, typeChaines, typeUnites, labelsNbElements);
					break;
				case "Répartition des paragraphes par référence":
					
					typeChampColonne = "Paragraphe";
					typeChampLigne = new String[1];
					typeChampLigne[0] = "Chaine";
					Modelestat = ModeleStatChaine.calculateStatCoref(saisieChampChaine.getSelection(), saisieChampAffiche.getSelection(),
							valsAffichees1cachees, typeChampColonne, typeChampLigne, vue,
							donneesChaines, noparagraphecachees, typeChaines, typeUnites, labelsNbElements);
					break;
				case "Répartition des paragraphe par valeur du champ":
					
					typeChampColonne = "Paragraphe";
					typeChampLigne = new String[1];
					typeChampLigne[0] = "Unite";
					Modelestat = ModeleStatChaine.calculateStatCoref(saisieChampChaine.getSelection(), saisieChampAffiche.getSelection(),
							valsAffichees1cachees, typeChampColonne, typeChampLigne, vue,
							donneesChaines, noparagraphecachees, typeChaines, typeUnites, labelsNbElements);
					break;
				case "Répartition des valeurs du champ par paragraphe":
					
					typeChampColonne = "Unite";
					typeChampLigne = new String[1];
					typeChampLigne[0] = "Paragraphe";
					Modelestat = ModeleStatChaine.calculateStatCoref(saisieChampChaine.getSelection(), saisieChampAffiche.getSelection(),
							valsAffichees1cachees, typeChampColonne, typeChampLigne, vue,
							donneesChaines, noparagraphecachees, typeChaines, typeUnites, labelsNbElements);
					break;
				case "Répartition des valeurs de champ par référence":
					
					typeChampColonne = "Unite";
					typeChampLigne = new String[1];
					typeChampLigne[0] = "Chaine";
					Modelestat = ModeleStatChaine.calculateStatCoref(saisieChampChaine.getSelection(), saisieChampAffiche.getSelection(),
							valsAffichees1cachees, typeChampColonne, typeChampLigne, vue,
							donneesChaines, noparagraphecachees, typeChaines, typeUnites, labelsNbElements);
					break;
				case "Répartition des références par valeur de champ":
					
					typeChampColonne = "Chaine";
					typeChampLigne = new String[1];
					typeChampLigne[0] = "Unite";
					Modelestat = ModeleStatChaine.calculateStatCoref(saisieChampChaine.getSelection(), saisieChampAffiche.getSelection(),
							valsAffichees1cachees, typeChampColonne, typeChampLigne, vue,
							donneesChaines, noparagraphecachees, typeChaines, typeUnites, labelsNbElements);
					break;
				case "Répartition par paragraphe et par réfences des valeurs du champ":
					
					typeChampColonne = "Unite";
					typeChampLigne = new String[2];
					typeChampLigne[0] = "Chaine";
					typeChampLigne[1] = "Paragraphe";
					Modelestat = ModeleStatChaine.calculateStatCoref(saisieChampChaine.getSelection(), saisieChampAffiche.getSelection(),
							valsAffichees1cachees, typeChampColonne, typeChampLigne, vue,
							donneesChaines, noparagraphecachees, typeChaines, typeUnites, labelsNbElements);
					break;
				case "Répartition par paragraphe et par valeur du champ des références":
					
					typeChampColonne = "Chaine";
					typeChampLigne = new String[2];
					typeChampLigne[0] = "Unite";
					typeChampLigne[1] = "Paragraphe";
					Modelestat = ModeleStatChaine.calculateStatCoref(saisieChampChaine.getSelection(), saisieChampAffiche.getSelection(),
							valsAffichees1cachees, typeChampColonne, typeChampLigne, vue,
							donneesChaines, noparagraphecachees, typeChaines, typeUnites, labelsNbElements);
					break;
				case "Répartition par référence et par valeur de champ des paragraphes":
					
					typeChampColonne = "Paragraphe";
					typeChampLigne = new String[2];
					typeChampLigne[0] = "Chaine";
					typeChampLigne[1] = "Unite";
					Modelestat = ModeleStatChaine.calculateStatCoref(saisieChampChaine.getSelection(), saisieChampAffiche.getSelection(),
							valsAffichees1cachees, typeChampColonne, typeChampLigne, vue,
							donneesChaines, noparagraphecachees, typeChaines, typeUnites, labelsNbElements);
					break;
				default:
					break;
				
			}
			
			ResultatStat.setModel(Modelestat);
			ResultatStat.setAutoCreateRowSorter(true);
			
			for (int i = 0; i < ResultatStat.getColumnCount(); i++) {// on ajuste les tailles des cases du tableau
				ResultatStat.getColumnModel().getColumn(i).setMinWidth(50);
			}
			ResultatStat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			
		}
	}
	
	public JTable getLegende() {
		return this.legendeChampAffiche;
	}
	
	public String getSelection(int taillepolice, int x, int y, String police) {
		String retour = "";
		if (saisieTypeChaines.pasdeSelection()) {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Type de Schéma : aucun" + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		else {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Type de Schéma : " + saisieTypeChaines.getSelection() + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		if (saisieTypeUnites.pasdeSelection()) {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 2 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Type d'Unités : aucun" + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		else {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 2 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Type d'Unités : " + saisieTypeUnites.getSelection() + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		if (saisieChampAffiche.pasdeSelection()) {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 4 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Champ 1 : nombre de Chainons" + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		else {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 4 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Champ 1 : " + saisieChampAffiche.getSelection() + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		if (saisieChampChaine.pasdeSelection()) {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 6 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Champ de la Chaine : aucun" + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		else {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 6 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Champ de la Chaine : " + saisieChampChaine.getSelection() + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		return retour;
	}
	
	public void importerAffichage() {
		HashMap<String, Object> Choix = FichiersJava.ouvrirChaineVue();
		if (Choix != null && Choix.containsKey("choixCommande2") &&
				Fichiers.getNomFichierCourant(Fichiers.TypeFichier.DOC_ANALEC).equals(Choix.get("nomFichier"))) {
			HashMap<String, String> choixCommande = (HashMap<String, String>) Choix.get("choixCommande2");
			
			saisieTypeChaines.removeActionListener(this);
			saisieTypeUnites.removeActionListener(this);
			saisieTypeChaines.setSelectedItem(choixCommande.get("saisieTypeChaines"));
			saisieTypeUnites.setSelectedItem(choixCommande.get("saisieTypeUnites"));
			saisieTypeChaines.addActionListener(this);
			saisieTypeUnites.addActionListener(this);
			init();
			
			saisieChampChaine.removeActionListener(this);
			saisieChampAffiche.removeActionListener(this);
			saisieChampStatistique.removeActionListener(this);
			donneesChaines.setValsMasqueesChamp((HashMap<String, HashSet<String>>) Choix.get("valmasquees"));
			donneesChaines.setValsNbMasqueesChamp((boolean[]) Choix.get("valNbmasquees"));
			donneesChaines.setValsNulleMasqueesChamp((HashSet<String>) Choix.get("valNullemasquees"));
			this.valsAffichees1cachees = (HashSet<String>) Choix.get("valsAffichees1cachees");
			this.noparagraphecachees = (ArrayList<Integer>) Choix.get("noparagraphecachees");
			activehistogramme.setSelected(choixCommande.get("activehistogramme").equals("vrai"));
			if (choixCommande.get("saisieChampAffiche") != null) {
				saisieChampAffiche.setSelectedItem(choixCommande.get("saisieChampAffiche"));
			}
			else {
				saisieChampAffiche.effSelection();
			}
			if (choixCommande.get("saisieChampChaine") != null) {
				saisieChampChaine.setSelectedItem(choixCommande.get("saisieChampChaine"));
			}
			else {
				saisieChampChaine.effSelection();
			}
			if (choixCommande.get("saisieChampStatistique") != null) {
				saisieChampStatistique.setSelectedItem(choixCommande.get("saisieChampStatistique"));
			}
			else {
				saisieChampStatistique.effSelection();
			}
			
			saisieChampChaine.addActionListener(this);
			saisieChampAffiche.addActionListener(this);
			saisieChampStatistique.addActionListener(this);
			
			
			if (saisieChampAffiche.pasdeSelection()) {
				initCouleursNbElts();
			}
			else {
				initCouleursChamp(saisieChampAffiche.getSelection());
			}
			initChaineChamp();
			initParagraphechamp();
			afficherchaines();
			afficherstat();
		}
		else {
			JOptionPane.showMessageDialog(null, "le fichier des paramètres d'affichage n'est pas destiné à cette fenêtre"
					+ System.getProperty("line.separator") +
					"ou à ce corpus", "Erreur de lecture de fichier", JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	public void exporterAffichage() {
		
		HashMap<String, Object> Choix = new HashMap();
		HashMap<String, String> choixCommande = new HashMap();
		
		choixCommande.put("saisieTypeChaines", saisieTypeChaines.pasdeSelection() ? null : saisieTypeChaines.getSelection());
		choixCommande.put("saisieTypeUnites", saisieTypeUnites.pasdeSelection() ? null : saisieTypeUnites.getSelection());
		choixCommande.put("saisieChampAffiche", saisieChampAffiche.pasdeSelection() ? null : saisieChampAffiche.getSelection());
		choixCommande.put("saisieChampChaine", saisieChampChaine.pasdeSelection() ? null : saisieChampChaine.getSelection());
		choixCommande.put("saisieChampStatistique", saisieChampStatistique.pasdeSelection() ? null : saisieChampStatistique.getSelection());
		choixCommande.put("activehistogramme", activehistogramme.isSelected() ? "vrai" : "faux");
		Choix.put("valmasquees", donneesChaines.getValsMasqueesChamp());
		Choix.put("valNbmasquees", donneesChaines.getValsNbMasqueesChamp());
		Choix.put("valNullemasquees", donneesChaines.getValsNulleMasqueesChamp());
		Choix.put("valsAffichees1cachees", valsAffichees1cachees);
		Choix.put("noparagraphecachees", noparagraphecachees);
		Choix.put("choixCommande2", choixCommande);
		Choix.put("nomFichier", Fichiers.getNomFichierCourant(Fichiers.TypeFichier.DOC_ANALEC));
		FichiersJava.enregistrerChaineSous(Choix);
	}
	
	public HashSet<GChaines> getSuiteGChaines() {
		return this.suiteGChaines;
	}
	
	public ModeleChaines getDonneesChaines() {
		return this.donneesChaines;
	}
	
	public Object[][] donneesCSV(boolean[] champ) {
		ArrayList<Object[]> donnees = new ArrayList<>();
		int nbchamp = 0;
		for (boolean tmp : champ) {
			if (tmp) {
				nbchamp = nbchamp + 1;
			}
		}
		// on crée les colonnes
		if (champ.length == 3) {
			Object[] titre = new Object[nbchamp + 1];
			titre[0] = "unité";
			int compteurTitre = 1;
			for (int i = 0; i < champ.length; i++) {
				if (champ[i]) {
					switch (i) {
						case 0:
							titre[compteurTitre] = this.saisieChampAffiche.pasdeSelection() ? "Nombre de Chainons"
									: this.saisieChampAffiche.getSelection();
							break;
						case 1:
							titre[compteurTitre] = this.saisieChampChaine.pasdeSelection() ? ""
									: this.saisieChampChaine.getSelection();
							break;
						case 2:
							titre[compteurTitre] = "Numéro de paragraphe";
							break;
					}
					compteurTitre = compteurTitre + 1;
				}
			}
			donnees.add(titre);
			for (GChaines chaine : this.suiteGChaines) {
				for (int j = 0; j < chaine.chainons.length; j++) {
					ModeleChaines.Chainon unit = chaine.chainons[j];
					Object[] row = new Object[nbchamp + 1];
					row[0] = unit.texte;
					int compteur = 1;
					for (int i = 0; i < champ.length; i++) {
						if (champ[i]) {
							switch (i) {
								case 0:
									row[compteur] = this.saisieChampAffiche.pasdeSelection() ? labelsNbElements[donneesChaines.calculerValNbElements(unit.nbEltsChaine)]
											: vue.getValeurChamp(unit.unite, this.saisieChampAffiche.getSelection());
									break;
								case 1:
									row[compteur] = this.saisieChampChaine.pasdeSelection() ? ""
											: vue.getValeurChamp(unit.chaine, this.saisieChampChaine.getSelection());
									break;
								case 2:
									row[compteur] = chaine.noSegments[j];
									break;
							}
							compteur = compteur + 1;
						}
					}
					donnees.add(row);
				}
			}
		}
		return donnees.toArray(new Object[donnees.size()][nbchamp + 1]);
	}
	
}
