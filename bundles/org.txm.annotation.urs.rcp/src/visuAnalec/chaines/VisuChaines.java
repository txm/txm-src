/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package visuAnalec.chaines;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import org.jfree.chart.ChartPanel;
import visuAnalec.*;
import visuAnalec.elements.*;
import visuAnalec.fichiers.FichiersJava;
import visuAnalec.util.*;
import visuAnalec.util.GMessages.GAction;
import visuAnalec.util.GVisu.LegendeLabel;
import visuAnalec.util.GVisu.LegendeModeleTable;
import visuAnalec.util.GVisu.LegendeModeleTableTexte;
import visuAnalec.util.GVisu.TypeComboBox;
import visuAnalec.util.GVisu.Val0ComboBox;
import visuAnalec.vue.*;
import visuAnalec.vue.Vue.VueListener;

/**
 *
 * @author Bernard
 */
public class VisuChaines extends JFrame implements VueListener, ActionListener {
	
	private static final int[] seuilsValsNbElts = { 3, 10 };
	
	private static final Color[] couleursNbElts = { Color.WHITE, GVisu.getCouleurSansValeur(),
			GVisu.getCouleur(0), GVisu.getCouleur(3), GVisu.getCouleur(1) };
	
	private static final int[] TaillesNbElts = { 1, 2, 3, 4, 4 };
	
	private static final int[] FormesNbElts = { 0, 1, 2, 3, 4 };
	
	private static final String[] TexteNbElts = { "00", "01", "02", "03", "04" };
	
	private final int colwidth = 50;
	
	private final int colheight = 30;
	
	private static final String[] labelsNbElements = {
			"pas de chaîne associée",
			"chaîne d'un seul chaînon",
			"chaîne de 2-" + Integer.toString(seuilsValsNbElts[0]) + " chaînons",
			"chaîne de " + Integer.toString(seuilsValsNbElts[0] + 1) + "-"
					+ Integer.toString(seuilsValsNbElts[1]) + "  chaînons",
			"chaîne de plus de " + Integer.toString(seuilsValsNbElts[1]) + " chaînons"
	};
	
	private final Vue vue;
	
	private final ModeleChaines donneesChaines;
	
	private String typeChaines;
	
	private String typeUnites;
	
	private String[] valsAffichees;
	
	private String[] valsAffichees1;
	
	private String[] valsAffichees2;
	
	private String[] valsAffichees3;
	
	private String[] valsAffichees4;
	
	private HashMap<String, HashMap<String, String>> codeLegende = new HashMap();
	
	private final TypeComboBox saisieTypeChaines;
	
	private final TypeComboBox saisieTypeUnites;
	
	private Val0ComboBox saisieChampAffiche;
	
	private Val0ComboBox saisieChampAffiche1;
	
	private Val0ComboBox saisieChampAffiche2;
	
	private Val0ComboBox saisieChampAffiche3;
	
	private Val0ComboBox saisieChampAffiche4;
	
	private final JComboBox saisieTailleLigne;
	
	private ChartPanel histochaine;
	
	/* private JComboBox saisieTailleLigne; */
	private JTable legendeChampAffiche;
	
	private LegendeModeleTable legendeChampAfficheModele;
	
	private JTable legendeChampAffiche1;
	
	private LegendeModeleTable legendeChampAfficheModele1;
	
	private JTable legendeChampAffiche2;
	
	private LegendeModeleTable legendeChampAfficheModele2;
	
	private JTable legendeChampAffiche3;
	
	private LegendeModeleTableTexte legendeChampAfficheModele3;
	
	private JTable legendeChampAffiche4;
	
	private LegendeModeleTableTexte legendeChampAfficheModele4;
	
	private JPopupMenu popupChamp = new JPopupMenu();
	
	private JPopupMenu popupChamp1 = new JPopupMenu();
	
	private JPopupMenu popupChamp2 = new JPopupMenu();
	
	private JPopupMenu popupChamp3 = new JPopupMenu();
	
	private JPopupMenu popupChamp4 = new JPopupMenu();
	
	private final GChaines panneauGraphique;// affichage graphique des chaines
	
	private final PanneauEditeur panneauEditeur;
	
	private final JCheckBox activeCouperParagraphe;
	
	private NgrammesPanel ngrammes;
	
	private final JSplitPane split2;
	
	private final JMenuBar menuBar = new JMenuBar();
	
	public VisuChaines(Container fenetre, Vue vue, PanneauEditeur pEditeur) {
		super("Analyse des suites d'occurences");
		Point pos = new Point(fenetre.getX() + 20, fenetre.getY() + 20);
		Dimension dim = new Dimension(fenetre.getWidth(), fenetre.getHeight());
		setLocation(pos);
		setSize(dim);
		this.vue = vue;
		vue.addEventListener(this);
		this.panneauEditeur = pEditeur;
		saisieTypeChaines = new TypeComboBox(Schema.class);
		saisieTypeChaines.setModele(vue.getTypesSchemasAVoir());
		saisieTypeChaines.addActionListener(this);
		saisieTypeUnites = new TypeComboBox(Unite.class);
		saisieTypeUnites.setModele(vue.getTypesUnitesAVoir());
		saisieTypeUnites.addActionListener(this);
		saisieTailleLigne = new JComboBox(
				new String[] { "<tous>", "10", "20", "30", "40", "50" });
		saisieTailleLigne.setEditable(true);
		saisieTailleLigne.setMaximumSize(getPreferredSize());
		saisieTailleLigne.addActionListener(this);
		saisieChampAffiche = new Val0ComboBox("Nombre de chaînons");
		saisieChampAffiche.addActionListener(this);
		saisieChampAffiche1 = new Val0ComboBox("Nombre de chaînons");
		saisieChampAffiche1.addActionListener(this);
		saisieChampAffiche2 = new Val0ComboBox("Nombre de chaînons");
		saisieChampAffiche2.addActionListener(this);
		saisieChampAffiche3 = new Val0ComboBox("Nombre de chaînons");
		saisieChampAffiche3.addActionListener(this);
		saisieChampAffiche4 = new Val0ComboBox("Nombre de chaînons");
		saisieChampAffiche4.addActionListener(this);
		// Panneau de présentation du graphique
		JPanel pGraphique = new JPanel(new BorderLayout());
		Box commandes = Box.createVerticalBox();
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Chaînes : "));
		commandes.add(saisieTypeChaines);
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Unites : "));
		commandes.add(saisieTypeUnites);
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Nb de chainons par ligne : "));
		commandes.add(saisieTailleLigne);
		commandes.add(Box.createVerticalGlue());
		activeCouperParagraphe = new JCheckBox("Ne pas couper ls paragraphes");
		commandes.add(activeCouperParagraphe);
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Champ à afficher : "));
		commandes.add(saisieChampAffiche);
		commandes.add(Box.createVerticalGlue());
		
		
		legendeChampAfficheModele = new LegendeModeleTable();
		legendeChampAffiche = new JTable(new Object[0][0], new String[] { "Icone", "Valeur de champ", "Fréquence" });
		legendeChampAffiche.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mousePressed(MouseEvent e) {// on gère les évènements de la souris
				check(e);
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				check(e);
			}
			
			public void check(MouseEvent e) {
				if (e.isPopupTrigger()) {// if the event shows the menu
					popupChampInit(legendeChampAffiche.rowAtPoint(e.getPoint()), 0);
					popupChamp.show(legendeChampAffiche, e.getX(), e.getY());
				}
			}
		});
		
		commandes.add(legendeChampAffiche);
		
		
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Champ à afficher : "));
		commandes.add(saisieChampAffiche1);
		commandes.add(Box.createVerticalGlue());
		
		legendeChampAfficheModele1 = new LegendeModeleTable();
		legendeChampAffiche1 = new JTable(legendeChampAfficheModele1);
		legendeChampAffiche1.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mousePressed(MouseEvent e) {// on gère les évènements de la souris
				check(e);
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				check(e);
			}
			
			public void check(MouseEvent e) {
				if (e.isPopupTrigger()) {// if the event shows the menu
					popupChampInit(legendeChampAffiche1.rowAtPoint(e.getPoint()), 1);
					popupChamp1.show(legendeChampAffiche1, e.getX(), e.getY());
				}
			}
		});
		
		commandes.add(legendeChampAffiche1);
		
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Champ à afficher : "));
		commandes.add(saisieChampAffiche2);
		commandes.add(Box.createVerticalGlue());
		
		legendeChampAfficheModele2 = new LegendeModeleTable();
		legendeChampAffiche2 = new JTable(legendeChampAfficheModele2);
		legendeChampAffiche2.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mousePressed(MouseEvent e) {// on gère les évènements de la souris
				check(e);
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				check(e);
			}
			
			public void check(MouseEvent e) {
				if (e.isPopupTrigger()) {// if the event shows the menu
					popupChampInit(legendeChampAffiche2.rowAtPoint(e.getPoint()), 2);
					popupChamp2.show(legendeChampAffiche2, e.getX(), e.getY());
				}
			}
		});
		commandes.add(legendeChampAffiche2);
		
		
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Champ à afficher : "));
		commandes.add(saisieChampAffiche3);
		commandes.add(Box.createVerticalGlue());
		
		legendeChampAfficheModele3 = new LegendeModeleTableTexte();
		legendeChampAffiche3 = new JTable(legendeChampAfficheModele3);
		legendeChampAffiche3.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mousePressed(MouseEvent e) {// on gère les évènements de la souris
				check(e);
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				check(e);
			}
			
			public void check(MouseEvent e) {
				if (e.isPopupTrigger()) {// if the event shows the menu
					popupChampInit(legendeChampAffiche3.rowAtPoint(e.getPoint()), 3);
					popupChamp3.show(legendeChampAffiche3, e.getX(), e.getY());
				}
			}
		});
		commandes.add(legendeChampAffiche3);
		
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Champ à afficher : "));
		commandes.add(saisieChampAffiche4);
		commandes.add(Box.createVerticalGlue());
		
		legendeChampAfficheModele4 = new LegendeModeleTableTexte();
		legendeChampAffiche4 = new JTable(legendeChampAfficheModele4);
		legendeChampAffiche4.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mousePressed(MouseEvent e) {// on gère les évènements de la souris
				check(e);
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				check(e);
			}
			
			public void check(MouseEvent e) {
				if (e.isPopupTrigger()) {// if the event shows the menu
					popupChampInit(legendeChampAffiche4.rowAtPoint(e.getPoint()), 4);
					popupChamp4.show(legendeChampAffiche4, e.getX(), e.getY());
				}
			}
		});
		commandes.add(legendeChampAffiche4);
		JScrollPane scrollLegende = new JScrollPane(commandes);
		
		panneauGraphique = new GChaines(this);
		panneauGraphique.autoriserSousTexte(true);
		
		donneesChaines = new ModeleChaines(vue, panneauGraphique);
		JScrollPane scrollGraphique = new JScrollPane(panneauGraphique);
		histochaine = Histogramme.histogrammeFactory();
		this.ngrammes = new NgrammesPanel(vue);
		JScrollPane scrollstatresultat = new JScrollPane(this.ngrammes);
		JScrollPane scrollHistogramme = new JScrollPane(histochaine);
		split2 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scrollHistogramme, scrollstatresultat);
		JSplitPane split3 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scrollGraphique, split2);
		pGraphique.add(split3, BorderLayout.CENTER);
		saisieTypeChaines.setAlignmentX(0);
		saisieTypeUnites.setAlignmentX(0);
		saisieTailleLigne.setAlignmentX(0);
		saisieChampAffiche.setAlignmentX(0);
		saisieChampAffiche1.setAlignmentX(0);
		saisieChampAffiche2.setAlignmentX(0);
		saisieChampAffiche3.setAlignmentX(0);
		saisieChampAffiche4.setAlignmentX(0);
		legendeChampAffiche.setAlignmentX(0);
		legendeChampAffiche1.setAlignmentX(0);
		legendeChampAffiche2.setAlignmentX(0);
		legendeChampAffiche3.setAlignmentX(0);
		legendeChampAffiche4.setAlignmentX(0);
		
		
		
		JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scrollLegende, pGraphique);
		getContentPane().add(split);
		setVisible(true);
		split.setDividerLocation(1. / 3);
		split2.setDividerLocation(1. / 2);
		split3.setDividerLocation(1. / 3);
		setJMenuBar(menuBar);
		JMenu corpusMenu = new JMenu("Paramètres d'affichage");
		menuBar.add(corpusMenu);
		corpusMenu.add(new GAction("Reprendre un affichage") {
			
			@Override
			public void executer() {
				importerAffichage();
			}
		});
		corpusMenu.add(new GAction("Sauver les paramètres d'affichage") {
			
			@Override
			public void executer() {
				exporterAffichage();
			}
		});
		JMenu csvMenu = new JMenu("Exporter les données");
		menuBar.add(csvMenu);
		final VisuChaines visu = this;
		final Vue vues = this.vue;
		final PanneauEditeur pEditeurs = pEditeur;
		csvMenu.add(new GAction("Exporter les données de filtrage") {
			
			@Override
			public void executer() {
				new ExportCSV(getContentPane(), vues, pEditeurs, visu);
			}
		});
		csvMenu.add(new GAction("Exporter le tableau statistique") {
			
			@Override
			public void executer() {
				ExportCSV.exporterCSV(ngrammes.donneesCSV());
			}
		});
		csvMenu.add(new GAction("Exporter les fréquences pour le champ1") {
			
			@Override
			public void executer() {
				Object[][] donneesCSV = new Object[legendeChampAffiche.getRowCount() + 1][2];
				for (int i = 0; i < legendeChampAffiche.getRowCount(); i++) {
					donneesCSV[i + 1][0] = legendeChampAffiche.getValueAt(i, 1);
					donneesCSV[i + 1][1] = legendeChampAffiche.getValueAt(i, 2) == null ? "0"
							: legendeChampAffiche.getValueAt(i, 2);
				}
				donneesCSV[0][1] = "frequence";
				donneesCSV[0][0] = saisieChampAffiche.pasdeSelection() ? "Nombre de Chainons"
						: saisieChampAffiche.getSelection();
				ExportCSV.exporterCSV(donneesCSV);
			}
		});
		csvMenu.add(new GAction("Exporter les fréquences pour le champ2") {
			
			@Override
			public void executer() {
				Object[][] donneesCSV = new Object[legendeChampAffiche1.getRowCount() + 1][2];
				for (int i = 0; i < legendeChampAffiche1.getRowCount(); i++) {
					donneesCSV[i + 1][0] = legendeChampAffiche1.getValueAt(i, 1);
					donneesCSV[i + 1][1] = legendeChampAffiche1.getValueAt(i, 2) == null ? "0"
							: legendeChampAffiche1.getValueAt(i, 2);
				}
				donneesCSV[0][1] = "frequence";
				donneesCSV[0][0] = saisieChampAffiche1.pasdeSelection() ? "Nombre de Chainons"
						: saisieChampAffiche1.getSelection();
				ExportCSV.exporterCSV(donneesCSV);
			}
		});
		csvMenu.add(new GAction("Exporter les fréquences pour le champ3") {
			
			@Override
			public void executer() {
				Object[][] donneesCSV = new Object[legendeChampAffiche2.getRowCount() + 1][2];
				for (int i = 0; i < legendeChampAffiche2.getRowCount(); i++) {
					donneesCSV[i + 1][0] = legendeChampAffiche2.getValueAt(i, 1);
					donneesCSV[i + 1][1] = legendeChampAffiche2.getValueAt(i, 2) == null ? "0"
							: legendeChampAffiche2.getValueAt(i, 2);
				}
				donneesCSV[0][1] = "frequence";
				donneesCSV[0][0] = saisieChampAffiche2.pasdeSelection() ? "Nombre de Chainons"
						: saisieChampAffiche2.getSelection();
				ExportCSV.exporterCSV(donneesCSV);
			}
		});
		csvMenu.add(new GAction("Exporter les fréquences pour le champ4") {
			
			@Override
			public void executer() {
				Object[][] donneesCSV = new Object[legendeChampAffiche3.getRowCount() + 1][2];
				for (int i = 0; i < legendeChampAffiche3.getRowCount(); i++) {
					donneesCSV[i + 1][0] = legendeChampAffiche3.getValueAt(i, 1);
					donneesCSV[i + 1][1] = legendeChampAffiche3.getValueAt(i, 2) == null ? "0"
							: legendeChampAffiche3.getValueAt(i, 2);
				}
				donneesCSV[0][1] = "frequence";
				donneesCSV[0][0] = saisieChampAffiche3.pasdeSelection() ? "Nombre de Chainons"
						: saisieChampAffiche3.getSelection();
				ExportCSV.exporterCSV(donneesCSV);
			}
		});
		csvMenu.add(new GAction("Exporter les fréquences pour le champ5") {
			
			@Override
			public void executer() {
				Object[][] donneesCSV = new Object[legendeChampAffiche4.getRowCount() + 1][2];
				for (int i = 0; i < legendeChampAffiche4.getRowCount(); i++) {
					donneesCSV[i + 1][0] = legendeChampAffiche4.getValueAt(i, 1);
					donneesCSV[i + 1][1] = legendeChampAffiche4.getValueAt(i, 2) == null ? "0"
							: legendeChampAffiche4.getValueAt(i, 2);
				}
				donneesCSV[0][1] = "frequence";
				donneesCSV[0][0] = saisieChampAffiche4.pasdeSelection() ? "Nombre de Chainons"
						: saisieChampAffiche4.getSelection();
				ExportCSV.exporterCSV(donneesCSV);
			}
		});
		
		// Fermeture de la fenêtre
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent evt) {
				fermerFenetre();
			}
		});
		// Initialisation
		saisieTailleLigne.removeActionListener(this);
		saisieTailleLigne.setSelectedIndex(0);
		saisieTailleLigne.addActionListener(this);
		saisieTailleLigne.setMaximumSize(getPreferredSize());
		if (saisieTypeChaines.nbItems() == 1) {
			saisieTypeChaines.setSelection(0);
			if (saisieTypeUnites.nbItems() == 1) saisieTypeUnites.setSelection(0);
			else saisieTypeUnites.effSelection();
		}
		else {
			saisieTypeChaines.effSelection();
			saisieTypeUnites.effSelection();
		}
	}
	
	private void fermerFenetre() {
		vue.removeEventListener(this);
		dispose();
	}
	
	@Override
	public void traiterEvent(Message evt) { // très grossier : à améliorer
		switch (evt.getType()) {
			case CLEAR_CORPUS:
				fermerFenetre();
				return;
			case NEW_CORPUS:  // impossible en principe : CLEAR_CORPUS a détruit la fenêtre
				throw new UnsupportedOperationException("Fenêtre chaines présente lors d'un message NEW_CORPUS ???");
			case MODIF_TEXTE:
				fermerFenetre();
				return;
			case MODIF_VUE:
				fermerFenetre();
				return;
			case MODIF_STRUCTURE:
				fermerFenetre();
				return;
			case MODIF_ELEMENT:
				// ElementEvent evtE = (ElementEvent) evt;
				// if (evtE.getModif() != TypeModifElement.BORNES_UNITE) fermerFenetre();
				fermerFenetre();
				return;
			case CORPUS_SAVED:
				fermerFenetre();
				return;
			default:
				throw new UnsupportedOperationException("Cas " + evt.getType() + " oublié dans un switch");
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		try {
			panneauGraphique.setcouperparagraphe(activeCouperParagraphe.isSelected());
			if (evt.getSource().equals(saisieTypeChaines)
					|| evt.getSource().equals(saisieTypeUnites)) {
				if (saisieTypeChaines.pasdeSelection() || saisieTypeUnites.pasdeSelection()) {
					initVide();
					return;
				}
				try {
					init();
				}
				catch (UnsupportedOperationException ex) {
					GMessages.impossibiliteAction("Erreur de type : " + ex.getMessage());
					initVide();
					return;
				}
				return;
			}
			if (evt.getSource().equals(saisieChampAffiche)) {
				if (!saisieTypeChaines.pasdeSelection() && !saisieTypeUnites.pasdeSelection()) {
					if (saisieChampAffiche.pasdeSelection()) initCouleursNbElts();
					else initCouleursChamp(saisieChampAffiche.getSelection());
				}
				return;
			}
			if (evt.getSource().equals(saisieChampAffiche1)) {
				if (!saisieTypeChaines.pasdeSelection() && !saisieTypeUnites.pasdeSelection()) {
					if (saisieChampAffiche1.pasdeSelection()) initFormesNbElts();
					else initFormesChamp(saisieChampAffiche1.getSelection());
				}
				return;
			}
			if (evt.getSource().equals(saisieChampAffiche2)) {
				if (!saisieTypeChaines.pasdeSelection() && !saisieTypeUnites.pasdeSelection()) {
					if (saisieChampAffiche2.pasdeSelection()) initTaillesNbElts();
					else initTaillesChamp(saisieChampAffiche2.getSelection());
				}
				return;
			}
			if (evt.getSource().equals(saisieChampAffiche3)) {
				if (!saisieTypeChaines.pasdeSelection() && !saisieTypeUnites.pasdeSelection()) {
					if (saisieChampAffiche3.pasdeSelection()) initSurTexteNbElts();
					else initSurTexteChamp(saisieChampAffiche3.getSelection());
				}
				return;
			}
			if (evt.getSource().equals(saisieChampAffiche4)) {
				if (!saisieTypeChaines.pasdeSelection() && !saisieTypeUnites.pasdeSelection()) {
					if (saisieChampAffiche4.pasdeSelection()) initSousTexteNbElts();
					else initSousTexteChamp(saisieChampAffiche4.getSelection());
				}
				return;
			}
			throw new UnsupportedOperationException("Evénement non prévu : " + evt.paramString());
		}
		catch (Throwable ex) {
			GMessages.erreurFatale(ex);
		}
	}
	
	private void initVide() {
		typeChaines = null;
		typeUnites = null;
		donneesChaines.initVide();
		saisieChampAffiche.setModeleVide();
		saisieChampAffiche1.setModeleVide();
		saisieChampAffiche2.setModeleVide();
		saisieChampAffiche3.setModeleVide();
		saisieChampAffiche4.setModeleVide();
		this.ngrammes.init();
		legendeChampAfficheModele = new LegendeModeleTable(new String[] { "icone", "nom du champ", "frequence" }, new LegendeLabel[0], new HashMap());
		legendeChampAffiche.setModel(legendeChampAfficheModele);
		legendeChampAfficheModele1 = new LegendeModeleTable(new String[] { "icone", "nom du champ", "frequence" }, new LegendeLabel[0], new HashMap());
		legendeChampAffiche1.setModel(legendeChampAfficheModele1);
		legendeChampAfficheModele2 = new LegendeModeleTable(new String[] { "icone", "nom du champ", "frequence" }, new LegendeLabel[0], new HashMap());
		legendeChampAffiche2.setModel(legendeChampAfficheModele2);
		legendeChampAfficheModele3 = new LegendeModeleTableTexte(new String[] { "Symbole", "nom du champ", "frequence" }, new LegendeLabel[0], new HashMap());
		legendeChampAffiche3.setModel(legendeChampAfficheModele3);
		legendeChampAfficheModele4 = new LegendeModeleTableTexte(new String[] { "Symbole", "nom du champ", "frequence" }, new LegendeLabel[0], new HashMap());
		legendeChampAffiche4.setModel(legendeChampAfficheModele4);
	}
	
	private void init() throws UnsupportedOperationException {
		typeChaines = saisieTypeChaines.getTypeSelection();
		typeUnites = saisieTypeUnites.getTypeSelection();
		donneesChaines.init(typeChaines, typeUnites, seuilsValsNbElts);
		initCouleursNbElts();
		initTaillesNbElts();
		initFormesNbElts();
		initSurTexteNbElts();
		initSousTexteNbElts();
		saisieChampAffiche.removeActionListener(this);
		saisieChampAffiche.setModele(vue.getNomsChamps(Unite.class, typeUnites));
		saisieChampAffiche.addActionListener(this);
		saisieChampAffiche1.removeActionListener(this);
		saisieChampAffiche1.setModele(vue.getNomsChamps(Unite.class, typeUnites));
		saisieChampAffiche1.addActionListener(this);
		saisieChampAffiche2.removeActionListener(this);
		saisieChampAffiche2.setModele(vue.getNomsChamps(Unite.class, typeUnites));
		saisieChampAffiche2.addActionListener(this);
		saisieChampAffiche3.removeActionListener(this);
		saisieChampAffiche3.setModele(vue.getNomsChamps(Unite.class, typeUnites));
		saisieChampAffiche3.addActionListener(this);
		saisieChampAffiche4.removeActionListener(this);
		saisieChampAffiche4.setModele(vue.getNomsChamps(Unite.class, typeUnites));
		saisieChampAffiche4.addActionListener(this);
		Histogramme.updatePanel(histochaine, "Titre", donneesChaines, 10, Color.BLUE, 200,
				"position de l'unité", "Fréquence d'unités", vue);
		this.ngrammes.setDonneesChaines(donneesChaines);
		this.ngrammes.setTypeChaines(typeChaines);
		this.ngrammes.setTypeUnites(typeUnites);
		this.ngrammes.update();
		
		
		
	}
	
	private void initCouleursNbElts() {
		HashMap<String, Float> freq = donneesChaines.calculerfrequencenbelt(donneesChaines.getUnites(), typeUnites, labelsNbElements);
		donneesChaines.setChampAfficheNbElts(couleursNbElts);
		legendeChampAfficheModele = new LegendeModeleTable(new String[] { "icone", "nom du champ", "frequence" }, creerLegendesNbElements(), freq);
		legendeChampAffiche.setModel(legendeChampAfficheModele);
		legendeChampAffiche.getColumnModel().getColumn(0).setMaxWidth(colwidth);
		legendeChampAffiche.getColumnModel().getColumn(2).setMaxWidth(colwidth);
		legendeChampAffiche.setRowHeight(colheight);
	}
	
	private void initTaillesNbElts() {
		HashMap<String, Float> freq = donneesChaines.calculerfrequencenbelt(donneesChaines.getUnites(), typeUnites, labelsNbElements);
		donneesChaines.setChampAfficheTailleNbElts(TaillesNbElts);
		legendeChampAfficheModele2 = new LegendeModeleTable(new String[] { "icone", "nom du champ", "frequence" }, creerLegendesTailleNbElements(), freq);
		Histogramme.updatePanel(histochaine, "Titre", donneesChaines, 10, Color.BLUE, 200,
				"position de l'unité", "Fréquence d'unités", vue);
		legendeChampAffiche2.setModel(legendeChampAfficheModele2);
		legendeChampAffiche2.getColumnModel().getColumn(0).setMaxWidth(colwidth);
		legendeChampAffiche2.getColumnModel().getColumn(2).setMaxWidth(colwidth);
		legendeChampAffiche2.setRowHeight(colheight);
	}
	
	private void initFormesNbElts() {
		HashMap<String, Float> freq = donneesChaines.calculerfrequencenbelt(donneesChaines.getUnites(), typeUnites, labelsNbElements);
		donneesChaines.setChampAfficheFormeNbElts(FormesNbElts);
		legendeChampAfficheModele1 = new LegendeModeleTable(new String[] { "icone", "nom du champ", "frequence" }, creerLegendesFormeNbElements(), freq);
		legendeChampAffiche1.setModel(legendeChampAfficheModele1);
		legendeChampAffiche1.getColumnModel().getColumn(0).setMaxWidth(colwidth);
		legendeChampAffiche1.getColumnModel().getColumn(2).setMaxWidth(colwidth);
		legendeChampAffiche1.setRowHeight(colheight);
	}
	
	private void initSurTexteNbElts() {
		HashMap<String, Float> freq = donneesChaines.calculerfrequencenbelt(donneesChaines.getUnites(), typeUnites, labelsNbElements);
		donneesChaines.setChampAfficheSurTexteNbElts(TexteNbElts);
		legendeChampAfficheModele3 = new LegendeModeleTableTexte(new String[] { "symbole", "nom du champ", "frequence" }, creerLegendesSurTexteNbElements(), freq);
		legendeChampAffiche3.setModel(legendeChampAfficheModele3);
		legendeChampAffiche3.getColumnModel().getColumn(0).setMaxWidth(colwidth);
		legendeChampAffiche3.getColumnModel().getColumn(2).setMaxWidth(colwidth);
	}
	
	private void initSousTexteNbElts() {
		HashMap<String, Float> freq = donneesChaines.calculerfrequencenbelt(donneesChaines.getUnites(), typeUnites, labelsNbElements);
		donneesChaines.setChampAfficheSousTexteNbElts(TexteNbElts);
		legendeChampAfficheModele4 = new LegendeModeleTableTexte(new String[] { "symbole", "nom du champ", "frequence" }, creerLegendesSousTexteNbElements(), freq);
		legendeChampAffiche4.setModel(legendeChampAfficheModele4);
		legendeChampAffiche4.getColumnModel().getColumn(0).setMaxWidth(colwidth);
		legendeChampAffiche4.getColumnModel().getColumn(2).setMaxWidth(colwidth);
	}
	
	private LegendeLabel[] creerLegendesNbElements() {
		LegendeLabel[] legendes = new LegendeLabel[couleursNbElts.length];
		for (int i = 0; i < legendes.length; i++)
			legendes[i] = new LegendeLabel(labelsNbElements[i],
					donneesChaines.isMasqueeValNbElements(i) ? null : couleursNbElts[i]);
		return legendes;
	}
	
	private LegendeLabel[] creerLegendesFormeNbElements() {
		LegendeLabel[] legendes = new LegendeLabel[FormesNbElts.length];
		for (int i = 0; i < legendes.length; i++)
			legendes[i] = new LegendeLabel(labelsNbElements[i], Color.WHITE, 1, (donneesChaines.isMasqueeValNbElements(i) ? -1 : FormesNbElts[i]), 12, 2);
		return legendes;
	}
	
	private LegendeLabel[] creerLegendesTailleNbElements() {
		LegendeLabel[] legendes = new LegendeLabel[TaillesNbElts.length];
		for (int i = 0; i < legendes.length; i++)
			legendes[i] = new LegendeLabel(labelsNbElements[i], Color.WHITE, donneesChaines.isMasqueeValNbElements(i) ? -1 : TaillesNbElts[i], 0, 12, 2);
		return legendes;
	}
	
	private LegendeLabel[] creerLegendesSurTexteNbElements() {
		LegendeLabel[] legendes = new LegendeLabel[TexteNbElts.length];
		for (int i = 0; i < legendes.length; i++)
			legendes[i] = new LegendeLabel(labelsNbElements[i], donneesChaines.isMasqueeValNbElements(i) ? null : TexteNbElts[i]);
		return legendes;
	}
	
	private LegendeLabel[] creerLegendesSousTexteNbElements() {
		LegendeLabel[] legendes = new LegendeLabel[TexteNbElts.length];
		for (int i = 0; i < legendes.length; i++)
			legendes[i] = new LegendeLabel(labelsNbElements[i], donneesChaines.isMasqueeValNbElements(i) ? null : TexteNbElts[i]);
		return legendes;
	}
	
	
	private void initCouleursChamp(String champ) {
		valsAffichees = vue.getValeursChamp(Unite.class, typeUnites, champ);
		HashMap<String, Float> freq = ChaineUtil.calculerfrequencechamp(donneesChaines.getUnites(), typeUnites, champ, vue);
		ChaineUtil.triFusionMap(valsAffichees, freq);
		HashMap<String, Color> couleursValeursChamp = new HashMap<>();
		for (int i = 0, nocolor = 0; i < valsAffichees.length; i++)
			if (!donneesChaines.isMasqueeValChamp(champ, valsAffichees[i]))
				couleursValeursChamp.put(valsAffichees[i], GVisu.getCouleur(nocolor++));
		if (!donneesChaines.isMasqueeValNulleChamp(champ))
			couleursValeursChamp.put("", GVisu.getCouleurSansValeur());
		donneesChaines.setChampAffiche(champ, couleursValeursChamp);
		legendeChampAfficheModele = new LegendeModeleTable(new String[] { "icone", "nom du champ", "frequence" }, creerLegendesChamp(valsAffichees,
				couleursValeursChamp), freq);
		legendeChampAffiche.setModel(legendeChampAfficheModele);
		legendeChampAffiche.getColumnModel().getColumn(0).setMaxWidth(colwidth);
		legendeChampAffiche.getColumnModel().getColumn(2).setMaxWidth(colwidth);
		legendeChampAffiche.setRowHeight(colheight);
	}
	
	private void initTaillesChamp(String champ) {
		valsAffichees2 = vue.getValeursChamp(Unite.class, typeUnites, champ);
		HashMap<String, Float> freq = ChaineUtil.calculerfrequencechamp(donneesChaines.getUnites(), typeUnites, champ, vue);
		ChaineUtil.triFusionMap(valsAffichees2, freq);
		HashMap<String, Integer> TaillesValeursChamp = new HashMap<>();
		for (int i = 0, nocolor = 0; i < valsAffichees2.length; i++)
			if (!donneesChaines.isMasqueeValChamp(champ, valsAffichees2[i]))
				TaillesValeursChamp.put(valsAffichees2[i], GVisu.getTaille(nocolor++));
		if (!donneesChaines.isMasqueeValNulleChamp(champ))
			TaillesValeursChamp.put("", GVisu.getTailleSansValeur());
		donneesChaines.setChampAfficheTaille(champ, TaillesValeursChamp);
		legendeChampAfficheModele2 = new LegendeModeleTable(new String[] { "icone", "nom du champ", "frequence" }, creerLegendesChampTaille(valsAffichees2,
				TaillesValeursChamp), freq);
		Histogramme.updatePanel(histochaine, "Titre", donneesChaines, 10, Color.BLUE, 200,
				"position de l'unité", "Fréquence d'unités", vue);
		legendeChampAffiche2.setModel(legendeChampAfficheModele2);
		legendeChampAffiche2.getColumnModel().getColumn(0).setMaxWidth(colwidth);
		legendeChampAffiche2.getColumnModel().getColumn(2).setMaxWidth(colwidth);
		legendeChampAffiche2.setRowHeight(colheight);
	}
	
	private void initFormesChamp(String champ) {
		valsAffichees1 = vue.getValeursChamp(Unite.class, typeUnites, champ);
		HashMap<String, Float> freq = ChaineUtil.calculerfrequencechamp(donneesChaines.getUnites(), typeUnites, champ, vue);
		ChaineUtil.triFusionMap(valsAffichees1, freq);
		HashMap<String, Integer> FormesValeursChamp = new HashMap<>();
		for (int i = 0, nocolor = 0; i < valsAffichees1.length; i++)
			if (!donneesChaines.isMasqueeValChamp(champ, valsAffichees1[i]))
				FormesValeursChamp.put(valsAffichees1[i], GVisu.getForme(nocolor++));
		if (!donneesChaines.isMasqueeValNulleChamp(champ))
			FormesValeursChamp.put("", GVisu.getFormeSansValeur());
		donneesChaines.setChampAfficheForme(champ, FormesValeursChamp);
		legendeChampAfficheModele1 = new LegendeModeleTable(new String[] { "icone", "nom du champ", "frequence" }, creerLegendesChampForme(valsAffichees1,
				FormesValeursChamp), freq);
		legendeChampAffiche1.setModel(legendeChampAfficheModele1);
		legendeChampAffiche1.getColumnModel().getColumn(0).setMaxWidth(colwidth);
		legendeChampAffiche1.getColumnModel().getColumn(2).setMaxWidth(colwidth);
		legendeChampAffiche1.setRowHeight(colheight);
	}
	
	private void initSurTexteChamp(String champ) {
		valsAffichees3 = vue.getValeursChamp(Unite.class, typeUnites, champ);
		HashMap<String, Float> freq = ChaineUtil.calculerfrequencechamp(donneesChaines.getUnites(), typeUnites, champ, vue);
		ChaineUtil.triFusionMap(valsAffichees3, freq);
		HashMap<String, String> SurTexteValeursChamp = new HashMap<>();
		for (int i = 0, nocolor = 0; i < valsAffichees3.length; i++)
			if (!donneesChaines.isMasqueeValChamp(champ, valsAffichees3[i]))
				SurTexteValeursChamp.put(valsAffichees3[i], codeLegende.containsKey(champ) &&
						codeLegende.get(champ).containsKey(valsAffichees3[i]) ? codeLegende.get(champ).get(valsAffichees3[i])
								: GVisu.getTexte(nocolor++));
		if (!donneesChaines.isMasqueeValNulleChamp(champ))
			SurTexteValeursChamp.put("", codeLegende.containsKey(champ) &&
					codeLegende.get(champ).containsKey("") ? codeLegende.get(champ).get("")
							: GVisu.getTexteSansValeur());
		donneesChaines.setChampAfficheSurTexte(champ, SurTexteValeursChamp);
		legendeChampAfficheModele3 = new LegendeModeleTableTexte(new String[] { "symbole", "nom du champ", "frequence" }, creerLegendesChampSurTexte(valsAffichees3,
				SurTexteValeursChamp), freq);
		legendeChampAffiche3.setModel(legendeChampAfficheModele3);
		legendeChampAffiche3.getColumnModel().getColumn(0).setMaxWidth(colwidth);
		legendeChampAffiche3.getColumnModel().getColumn(2).setMaxWidth(colwidth);
	}
	
	private void initSousTexteChamp(String champ) {
		valsAffichees4 = vue.getValeursChamp(Unite.class, typeUnites, champ);
		HashMap<String, Float> freq = ChaineUtil.calculerfrequencechamp(donneesChaines.getUnites(), typeUnites, champ, vue);
		ChaineUtil.triFusionMap(valsAffichees4, freq);
		HashMap<String, String> SousTexteValeursChamp = new HashMap<>();
		for (int i = 0, nocolor = 0; i < valsAffichees4.length; i++)
			if (!donneesChaines.isMasqueeValChamp(champ, valsAffichees4[i]))
				SousTexteValeursChamp.put(valsAffichees4[i], codeLegende.containsKey(champ) &&
						codeLegende.get(champ).containsKey(valsAffichees4[i]) ? codeLegende.get(champ).get(valsAffichees4[i])
								: GVisu.getTexte(nocolor++));
		if (!donneesChaines.isMasqueeValNulleChamp(champ))
			SousTexteValeursChamp.put("", codeLegende.containsKey(champ) &&
					codeLegende.get(champ).containsKey("") ? codeLegende.get(champ).get("")
							: GVisu.getTexteSansValeur());
		donneesChaines.setChampAfficheSousTexte(champ, SousTexteValeursChamp);
		legendeChampAfficheModele4 = new LegendeModeleTableTexte(new String[] { "symbole", "nom du champ", "frequence" }, creerLegendesChampSousTexte(valsAffichees4,
				SousTexteValeursChamp), freq);
		legendeChampAffiche4.setModel(legendeChampAfficheModele4);
		legendeChampAffiche4.getColumnModel().getColumn(0).setMaxWidth(colwidth);
		legendeChampAffiche4.getColumnModel().getColumn(2).setMaxWidth(colwidth);
	}
	
	
	private LegendeLabel[] creerLegendesChamp(String[] valsAffichees,
			HashMap<String, Color> couleursValeursChamp) {
		LegendeLabel[] legendes = new LegendeLabel[valsAffichees.length + 1];
		for (int i = 0; i < valsAffichees.length; i++)
			legendes[i] = new LegendeLabel(valsAffichees[i],
					couleursValeursChamp.get(valsAffichees[i]));
		legendes[valsAffichees.length] = new LegendeLabel("<aucune valeur>",
				couleursValeursChamp.get(""));
		return legendes;
	}
	
	private LegendeLabel[] creerLegendesChampTaille(String[] valsAffichees,
			HashMap<String, Integer> TaillesValeursChamp) {
		LegendeLabel[] legendes = new LegendeLabel[valsAffichees.length + 1];
		for (int i = 0; i < valsAffichees.length; i++)
			legendes[i] = new LegendeLabel(valsAffichees[i], Color.WHITE,
					donneesChaines.isMasqueeValChamp(saisieChampAffiche2.getSelection(),
							valsAffichees[i]) ? -1 : (TaillesValeursChamp.get(valsAffichees[i])), 0, 12, 2);
		legendes[valsAffichees.length] = new LegendeLabel("<aucune valeur>", Color.WHITE,
				donneesChaines.isMasqueeValNulleChamp(saisieChampAffiche2.getSelection()) ? -1
						: (TaillesValeursChamp.get("")), 0, 6, 2);
		return legendes;
	}
	
	private LegendeLabel[] creerLegendesChampForme(String[] valsAffichees,
			HashMap<String, Integer> FormesValeursChamp) {
		LegendeLabel[] legendes = new LegendeLabel[valsAffichees.length + 1];
		for (int i = 0; i < valsAffichees.length; i++)
			legendes[i] = new LegendeLabel(valsAffichees[i], Color.WHITE, 1,
					donneesChaines.isMasqueeValChamp(saisieChampAffiche1.getSelection(),
							valsAffichees[i]) ? -1 : (FormesValeursChamp.get(valsAffichees[i])), 12, 2);
		legendes[valsAffichees.length] = new LegendeLabel("<aucune valeur>", Color.WHITE, 1,
				donneesChaines.isMasqueeValNulleChamp(saisieChampAffiche1.getSelection()) ? -1
						: (FormesValeursChamp.get("")), 12, 2);
		return legendes;
	}
	
	private LegendeLabel[] creerLegendesChampSurTexte(String[] valsAffichees,
			HashMap<String, String> SurTexteValeursChamp) {
		LegendeLabel[] legendes = new LegendeLabel[valsAffichees.length + 1];
		for (int i = 0; i < valsAffichees.length; i++)
			legendes[i] = new LegendeLabel(valsAffichees[i],
					donneesChaines.isMasqueeValChamp(saisieChampAffiche3.getSelection(),
							valsAffichees[i]) ? "" : (SurTexteValeursChamp.get(valsAffichees[i])));
		legendes[valsAffichees.length] = new LegendeLabel("<aucune valeur>",
				donneesChaines.isMasqueeValNulleChamp(saisieChampAffiche3.getSelection()) ? ""
						: (SurTexteValeursChamp.get("")));
		return legendes;
	}
	
	private LegendeLabel[] creerLegendesChampSousTexte(String[] valsAffichees,
			HashMap<String, String> SousTexteValeursChamp) {
		LegendeLabel[] legendes = new LegendeLabel[valsAffichees.length + 1];
		for (int i = 0; i < valsAffichees.length; i++)
			legendes[i] = new LegendeLabel(valsAffichees[i],
					donneesChaines.isMasqueeValChamp(saisieChampAffiche4.getSelection(),
							valsAffichees[i]) ? "" : (SousTexteValeursChamp.get(valsAffichees[i])));
		legendes[valsAffichees.length] = new LegendeLabel("<aucune valeur>",
				donneesChaines.isMasqueeValNulleChamp(saisieChampAffiche4.getSelection()) ? ""
						: (SousTexteValeursChamp.get("")));
		return legendes;
	}
	
	private void popupChampInit(final int index, final int champ) {
		final boolean masquee;
		String[] vals;
		Val0ComboBox zonedesaisie;
		JPopupMenu popup;
		switch (champ) {
			case 0:
				vals = valsAffichees;
				zonedesaisie = saisieChampAffiche;
				popup = popupChamp;
				break;
			case 1:
				vals = valsAffichees1;
				zonedesaisie = saisieChampAffiche1;
				popup = popupChamp1;
				break;
			case 2:
				vals = valsAffichees2;
				zonedesaisie = saisieChampAffiche2;
				popup = popupChamp2;
				break;
			case 3:
				vals = valsAffichees3;
				zonedesaisie = saisieChampAffiche3;
				popup = popupChamp3;
				break;
			case 4:
				vals = valsAffichees4;
				zonedesaisie = saisieChampAffiche4;
				popup = popupChamp4;
				break;
			default:
				vals = new String[0];
				zonedesaisie = new Val0ComboBox("Nombre de chaînons");
				popup = new JPopupMenu();
				break;
		}
		popup.removeAll();
		if (zonedesaisie.pasdeSelection()) {
			masquee = donneesChaines.isMasqueeValNbElements(index);
			popup.add(new GAction(masquee ? "Montrer cette valeur" : "Masquer cette valeur") {
				
				@Override
				public void executer() {
					modifierStatutValNbElements(masquee, index, false, champ);
				}
			});
			popup.add(new GAction(masquee ? "Montrer toutes les valeurs" : "Ne montrer que cette valeur") {
				
				@Override
				public void executer() {
					modifierStatutValNbElements(masquee, index, true, champ);
				}
			});
		}
		else if (index == vals.length) {
			masquee = donneesChaines.isMasqueeValNulleChamp(zonedesaisie.getSelection());
			popup.add(new GAction(masquee ? "Montrer cette valeur" : "Masquer cette valeur") {
				
				@Override
				public void executer() {
					modifierStatutValNulleChamp(masquee, false, champ);
				}
			});
			popup.add(new GAction(masquee ? "Montrer toutes les valeurs" : "Ne montrer que cette valeur") {
				
				@Override
				public void executer() {
					modifierStatutValNulleChamp(masquee, true, champ);
				}
			});
			if (champ == 3 || champ == 4) {
				popup.add(new GAction("Changer le code de la légende") {
					
					@Override
					public void executer() {
						changerCodeTexte(index, champ);
					}
				});
			}
		}
		else {
			masquee = donneesChaines.isMasqueeValChamp(zonedesaisie.getSelection(),
					vals[index]);
			popup.add(new GAction(masquee ? "Montrer cette valeur" : "Masquer cette valeur") {
				
				@Override
				public void executer() {
					modifierStatutValChamp(masquee, index, false, champ);
				}
			});
			popup.add(new GAction(masquee ? "Montrer toutes les valeurs" : "Ne montrer que cette valeur") {
				
				@Override
				public void executer() {
					modifierStatutValChamp(masquee, index, true, champ);
				}
			});
			if (champ == 3 || champ == 4) {
				popup.add(new GAction("Changer le code de la légende") {
					
					@Override
					public void executer() {
						changerCodeTexte(index, champ);
					}
				});
			}
		}
		
	}
	
	// il doit y avoir une saisie pour saisieChampAffiche3 ou pour saisieChampAffiche4
	private void changerCodeTexte(int index, int noLegende) {
		JOptionPane saisieCode = new JOptionPane();
		String ValAffichees;
		Val0ComboBox saisiechampaffiche;
		switch (noLegende) {
			case 3:
				saisiechampaffiche = saisieChampAffiche3;
				ValAffichees = this.valsAffichees3[index];
				break;
			default:
				saisiechampaffiche = saisieChampAffiche4;
				ValAffichees = this.valsAffichees4[index];
				break;
		}
		String code = saisieCode.showInputDialog(null, "Veuillez saisir un code légende d'au plus trois caractères",
				"Changer le code", JOptionPane.QUESTION_MESSAGE);
		if (code != null) {
			code = code.length() > 3 ? code.substring(0, 2) : code;
			if (!codeLegende.containsKey(saisiechampaffiche.getSelection())) {
				codeLegende.put(saisiechampaffiche.getSelection(), new HashMap());
			}
			codeLegende.get(saisiechampaffiche.getSelection()).put(ValAffichees, code);
			initSurTexteChamp(saisieChampAffiche3.getSelection());
			initSousTexteChamp(saisieChampAffiche4.getSelection());
		}
	}
	
	private void modifierStatutValChamp(boolean masquee, int index, boolean tous, int source) {
		String[] vals;
		Val0ComboBox zonedesaisie;
		switch (source) {
			case 0:
				vals = valsAffichees;
				zonedesaisie = saisieChampAffiche;
				break;
			case 1:
				vals = valsAffichees1;
				zonedesaisie = saisieChampAffiche1;
				break;
			case 2:
				vals = valsAffichees2;
				zonedesaisie = saisieChampAffiche2;
				break;
			case 3:
				vals = valsAffichees3;
				zonedesaisie = saisieChampAffiche3;
				break;
			case 4:
				vals = valsAffichees4;
				zonedesaisie = saisieChampAffiche4;
				break;
			default:
				vals = new String[0];
				zonedesaisie = new Val0ComboBox("Nombre de chaînons");
				break;
		}
		if (!tous) donneesChaines.masquerValChamp(!masquee,
				zonedesaisie.getSelection(), vals[index]);
		else if (masquee) donneesChaines.demasquerToutesValsChamp(
				zonedesaisie.getSelection());
		else donneesChaines.masquerAutresValsChamp(typeUnites,
				zonedesaisie.getSelection(), vals[index]);
		donneesChaines.modifMasqueVals(typeChaines, typeUnites);
		if (saisieChampAffiche.pasdeSelection()) {
			initCouleursNbElts();
		}
		else {
			initCouleursChamp(saisieChampAffiche.getSelection());
		}
		if (saisieChampAffiche1.pasdeSelection()) {
			initFormesNbElts();
		}
		else {
			initFormesChamp(saisieChampAffiche1.getSelection());
		}
		if (saisieChampAffiche2.pasdeSelection()) {
			initTaillesNbElts();
		}
		else {
			initTaillesChamp(saisieChampAffiche2.getSelection());
		}
		if (saisieChampAffiche3.pasdeSelection()) {
			initSurTexteNbElts();
		}
		else {
			initSurTexteChamp(saisieChampAffiche3.getSelection());
		}
		if (saisieChampAffiche4.pasdeSelection()) {
			initSousTexteNbElts();
		}
		else {
			initSousTexteChamp(saisieChampAffiche4.getSelection());
		}
		
		this.ngrammes.setDonneesChaines(donneesChaines);
		this.ngrammes.setTypeChaines(typeChaines);
		this.ngrammes.setTypeUnites(typeUnites);
		this.ngrammes.update();
	}
	
	private void modifierStatutValNulleChamp(boolean masquee, boolean tous, int source) {
		Val0ComboBox zonedesaisie;
		switch (source) {
			case 0:
				zonedesaisie = saisieChampAffiche;
				break;
			case 1:
				zonedesaisie = saisieChampAffiche1;
				break;
			case 2:
				zonedesaisie = saisieChampAffiche2;
				break;
			case 3:
				zonedesaisie = saisieChampAffiche3;
				break;
			case 4:
				zonedesaisie = saisieChampAffiche4;
				break;
			default:
				zonedesaisie = new Val0ComboBox("Nombre de chaînons");
				break;
		}
		if (!tous) donneesChaines.masquerValNulleChamp(!masquee, zonedesaisie.getSelection());
		else if (masquee) donneesChaines.demasquerToutesValsChamp(
				zonedesaisie.getSelection());
		else donneesChaines.masquerValsNonNullesChamp(typeUnites,
				zonedesaisie.getSelection());
		donneesChaines.modifMasqueVals(typeChaines, typeUnites);
		if (saisieChampAffiche.pasdeSelection()) {
			initCouleursNbElts();
		}
		else {
			initCouleursChamp(saisieChampAffiche.getSelection());
		}
		if (saisieChampAffiche1.pasdeSelection()) {
			initFormesNbElts();
		}
		else {
			initFormesChamp(saisieChampAffiche1.getSelection());
		}
		if (saisieChampAffiche2.pasdeSelection()) {
			initTaillesNbElts();
		}
		else {
			initTaillesChamp(saisieChampAffiche2.getSelection());
		}
		if (saisieChampAffiche3.pasdeSelection()) {
			initSurTexteNbElts();
		}
		else {
			initSurTexteChamp(saisieChampAffiche3.getSelection());
		}
		if (saisieChampAffiche4.pasdeSelection()) {
			initSousTexteNbElts();
		}
		else {
			initSousTexteChamp(saisieChampAffiche4.getSelection());
		}
		// initstat();
		
		this.ngrammes.setDonneesChaines(donneesChaines);
		this.ngrammes.setTypeChaines(typeChaines);
		this.ngrammes.setTypeUnites(typeUnites);
		this.ngrammes.update();
	}
	
	private void modifierStatutValNbElements(boolean masquee, int index, boolean tous, int source) {
		Val0ComboBox zonedesaisie;
		switch (source) {
			case 0:
				zonedesaisie = saisieChampAffiche;
				break;
			case 1:
				zonedesaisie = saisieChampAffiche1;
				break;
			case 2:
				zonedesaisie = saisieChampAffiche2;
				break;
			case 3:
				zonedesaisie = saisieChampAffiche3;
				break;
			case 4:
				zonedesaisie = saisieChampAffiche4;
				break;
			default:
				zonedesaisie = new Val0ComboBox("Nombre de chaînons");
				break;
		}
		if (!tous) donneesChaines.masquerValNbElements(!masquee, index);
		else if (masquee) donneesChaines.demasquerToutesValsNbElts(
				zonedesaisie.getSelection());
		else donneesChaines.masquerAutresValsNbElts(
				zonedesaisie.getSelection(), index);
		donneesChaines.modifMasqueVals(typeChaines, typeUnites);
		if (saisieChampAffiche.pasdeSelection()) {
			initCouleursNbElts();
		}
		else {
			initCouleursChamp(saisieChampAffiche.getSelection());
		}
		if (saisieChampAffiche1.pasdeSelection()) {
			initFormesNbElts();
		}
		else {
			initFormesChamp(saisieChampAffiche1.getSelection());
		}
		if (saisieChampAffiche2.pasdeSelection()) {
			initTaillesNbElts();
		}
		else {
			initTaillesChamp(saisieChampAffiche2.getSelection());
		}
		if (saisieChampAffiche3.pasdeSelection()) {
			initSurTexteNbElts();
		}
		else {
			initSurTexteChamp(saisieChampAffiche3.getSelection());
		}
		if (saisieChampAffiche4.pasdeSelection()) {
			initSousTexteNbElts();
		}
		else {
			initSousTexteChamp(saisieChampAffiche4.getSelection());
		}
		// initstat();
		
		this.ngrammes.setDonneesChaines(donneesChaines);
		this.ngrammes.setTypeChaines(typeChaines);
		this.ngrammes.setTypeUnites(typeUnites);
		this.ngrammes.update();
	}
	
	
	public JTable getLegende(int index) {
		JTable retour;
		switch (index) {
			case 0:
				retour = this.legendeChampAffiche;
				break;
			case 1:
				retour = this.legendeChampAffiche1;
				break;
			case 2:
				retour = this.legendeChampAffiche2;
				break;
			case 3:
				retour = this.legendeChampAffiche3;
				break;
			case 4:
				retour = this.legendeChampAffiche4;
				break;
			default:
				retour = new JTable();
				break;
		}
		return retour;
	}
	
	// retourne en format SVG la sélection
	public String getSelection(int taillepolice, int x, int y, String police) {
		String retour = "";
		if (saisieTypeChaines.pasdeSelection()) {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Type de Schéma : aucun" + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		else {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Type de Schéma : " + saisieTypeChaines.getSelection() + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
			
		}
		if (saisieTypeUnites.pasdeSelection()) {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 2 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Type d'Unités : aucun" + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		else {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 2 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Type d'Unités : " + saisieTypeUnites.getSelection() + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		if (saisieChampAffiche.pasdeSelection()) {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 4 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Champ 1 : nombre de Chainons" + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		else {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 4 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Champ 1 : " + saisieChampAffiche.getSelection() + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		if (saisieChampAffiche1.pasdeSelection()) {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 6 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Champ 2 : nombre de Chainons" + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		else {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 6 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Champ 2 : " + saisieChampAffiche1.getSelection() + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		if (saisieChampAffiche2.pasdeSelection()) {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 8 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Champ 3 : nombre de Chainons" + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		else {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 8 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Champ 3 : " + saisieChampAffiche2.getSelection() + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		if (saisieChampAffiche3.pasdeSelection()) {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 10 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Champ 4 : nombre de Chainons" + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		else {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 10 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Champ 4 : " + saisieChampAffiche3.getSelection() + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		if (saisieChampAffiche4.pasdeSelection()) {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 12 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Champ 5 : nombre de Chainons" + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		else {
			retour = retour + "<text x=\"" + x + "\" y=\"" + (y + 12 * taillepolice) + "\""
					+ " style=\" fill:black; stroke:black ;font-size:" + taillepolice + "px " + police + "\">" + "Champ 5 : " + saisieChampAffiche4.getSelection() + ";" +
					"</text>" + System.getProperty("line.separator") + System.getProperty("line.separator");
		}
		return retour;
	}
	
	public void exporterAffichage() {
		HashMap<String, Object> Choix = new HashMap();
		HashMap<String, String> choixCommande = new HashMap();
		
		choixCommande.put("saisieTypeChaines", saisieTypeChaines.pasdeSelection() ? null : saisieTypeChaines.getSelection());
		choixCommande.put("saisieTypeUnites", saisieTypeUnites.pasdeSelection() ? null : saisieTypeUnites.getSelection());
		choixCommande.put("saisieTailleLigne", saisieTailleLigne.getSelectedItem().toString());
		choixCommande.put("activeCouperParagraphe", activeCouperParagraphe.isSelected() ? "vrai" : "faux");
		choixCommande.put("saisieChampAffiche", saisieChampAffiche.pasdeSelection() ? null : saisieChampAffiche.getSelection());
		choixCommande.put("saisieChampAffiche1", saisieChampAffiche1.pasdeSelection() ? null : saisieChampAffiche1.getSelection());
		choixCommande.put("saisieChampAffiche2", saisieChampAffiche2.pasdeSelection() ? null : saisieChampAffiche2.getSelection());
		choixCommande.put("saisieChampAffiche3", saisieChampAffiche3.pasdeSelection() ? null : saisieChampAffiche3.getSelection());
		choixCommande.put("saisieChampAffiche4", saisieChampAffiche4.pasdeSelection() ? null : saisieChampAffiche4.getSelection());
		
		Choix.put("valmasquees", donneesChaines.getValsMasqueesChamp());
		Choix.put("valNbmasquees", donneesChaines.getValsNbMasqueesChamp());
		Choix.put("valNullemasquees", donneesChaines.getValsNulleMasqueesChamp());
		Choix.put("codeLegende", this.codeLegende);
		Choix.put("choixCommande1", choixCommande);
		Choix.put("ngrammes", this.ngrammes.exporterAffichage());
		Choix.put("nomFichier", Fichiers.getNomFichierCourant(Fichiers.TypeFichier.DOC_ANALEC));
		FichiersJava.enregistrerChaineSous(Choix);
	}
	
	public void importerAffichage() {
		HashMap<String, Object> choix = FichiersJava.ouvrirChaineVue();
		if (choix != null && choix.containsKey("choixCommande1") &&
				Fichiers.getNomFichierCourant(Fichiers.TypeFichier.DOC_ANALEC).equals(choix.get("nomFichier"))) {
			HashMap<String, String> choixCommande = (HashMap<String, String>) choix.get("choixCommande1");
			saisieTypeChaines.removeActionListener(this);
			saisieTypeUnites.removeActionListener(this);
			saisieTypeChaines.setSelectedItem(choixCommande.get("saisieTypeChaines"));
			saisieTypeUnites.setSelectedItem(choixCommande.get("saisieTypeUnites"));
			saisieTypeChaines.addActionListener(this);
			saisieTypeUnites.addActionListener(this);
			init();
			// si on ne les paramètres d'affichage ont été enregistrés pour le même corpus
			this.codeLegende = (HashMap<String, HashMap<String, String>>) choix.get("codeLegende");
			donneesChaines.setValsMasqueesChamp((HashMap<String, HashSet<String>>) choix.get("valmasquees"));
			donneesChaines.setValsNbMasqueesChamp((boolean[]) choix.get("valNbmasquees"));
			donneesChaines.setValsNulleMasqueesChamp((HashSet<String>) choix.get("valNullemasquees"));
			saisieTailleLigne.removeActionListener(this);
			saisieChampAffiche.removeActionListener(this);
			saisieChampAffiche1.removeActionListener(this);
			saisieChampAffiche2.removeActionListener(this);
			saisieChampAffiche3.removeActionListener(this);
			saisieChampAffiche4.removeActionListener(this);
			activeCouperParagraphe.setSelected(choixCommande.get("activeCouperParagraphe").equals("vrai"));
			panneauGraphique.setcouperparagraphe(activeCouperParagraphe.isSelected());
			saisieTailleLigne.setSelectedItem(choixCommande.get("saisieTailleLigne"));
			int taille = saisieTailleLigne.getSelectedIndex() == 0 ? Integer.MAX_VALUE : Integer.parseInt((String) saisieTailleLigne.getSelectedItem());
			panneauGraphique.setNbChainonsParLigne(taille);
			if (choixCommande.get("saisieChampAffiche") != null) {
				saisieChampAffiche.setSelectedItem(choixCommande.get("saisieChampAffiche"));
			}
			else {
				saisieChampAffiche.effSelection();
			}
			if (choixCommande.get("saisieChampAffiche1") != null) {
				saisieChampAffiche1.setSelectedItem(choixCommande.get("saisieChampAffiche1"));
			}
			else {
				saisieChampAffiche1.effSelection();
			}
			if (choixCommande.get("saisieChampAffiche2") != null) {
				saisieChampAffiche2.setSelectedItem(choixCommande.get("saisieChampAffiche2"));
			}
			else {
				saisieChampAffiche2.effSelection();
			}
			if (choixCommande.get("saisieChampAffiche3") != null) {
				saisieChampAffiche3.setSelectedItem(choixCommande.get("saisieChampAffiche3"));
			}
			else {
				saisieChampAffiche3.effSelection();
			}
			if (choixCommande.get("saisieChampAffiche4") != null) {
				saisieChampAffiche4.setSelectedItem(choixCommande.get("saisieChampAffiche4"));
			}
			else {
				saisieChampAffiche4.effSelection();
			}
			saisieTailleLigne.addActionListener(this);
			saisieChampAffiche.addActionListener(this);
			saisieChampAffiche1.addActionListener(this);
			saisieChampAffiche2.addActionListener(this);
			saisieChampAffiche3.addActionListener(this);
			saisieChampAffiche4.addActionListener(this);
			donneesChaines.modifMasqueVals(typeChaines, typeUnites);
			
			if (saisieChampAffiche.pasdeSelection()) {
				initCouleursNbElts();
			}
			else {
				initCouleursChamp(saisieChampAffiche.getSelection());
			}
			if (saisieChampAffiche1.pasdeSelection()) {
				initFormesNbElts();
			}
			else {
				initFormesChamp(saisieChampAffiche1.getSelection());
			}
			if (saisieChampAffiche2.pasdeSelection()) {
				initTaillesNbElts();
			}
			else {
				initTaillesChamp(saisieChampAffiche2.getSelection());
			}
			if (saisieChampAffiche3.pasdeSelection()) {
				initSurTexteNbElts();
			}
			else {
				initSurTexteChamp(saisieChampAffiche3.getSelection());
			}
			if (saisieChampAffiche4.pasdeSelection()) {
				initSousTexteNbElts();
			}
			else {
				initSousTexteChamp(saisieChampAffiche4.getSelection());
			}
			this.ngrammes.setDonneesChaines(donneesChaines);
			this.ngrammes.setTypeChaines(typeChaines);
			this.ngrammes.setTypeUnites(typeUnites);
			this.ngrammes.importerAffichage((HashMap) choix.get("ngrammes"));
			this.ngrammes.update();
		}
		else {
			JOptionPane.showMessageDialog(null, "le fichier des paramètres d'affichage n'est pas destiné à cette fenêtre"
					+ System.getProperty("line.separator") +
					"ou à ce corpus", "Erreur de lecture de fichier", JOptionPane.ERROR_MESSAGE);
		}
		
		
	}
	
	public ModeleChaines getDonneesChaines() {
		return this.donneesChaines;
	}
	
	public Object[][] donneesCSV(boolean[] champ) {
		ArrayList<Object[]> donnees = new ArrayList<>();
		int nbchamp = 0;
		for (boolean tmp : champ) {
			if (tmp) {
				nbchamp = nbchamp + 1;
			}
		}
		if (champ.length == 5) {
			GChaines chaine = this.panneauGraphique;
			Object[] titre = new Object[nbchamp + 1];
			titre[0] = "unité";
			int compteurTitre = 1;
			for (int i = 0; i < champ.length; i++) {
				if (champ[i]) {
					switch (i) {
						case 0:
							titre[compteurTitre] = this.saisieChampAffiche.pasdeSelection() ? "Nombre de Chainons"
									: this.saisieChampAffiche.getSelection();
							break;
						case 1:
							titre[compteurTitre] = this.saisieChampAffiche1.pasdeSelection() ? "Nombre de Chainons"
									: this.saisieChampAffiche1.getSelection();
							break;
						case 2:
							titre[compteurTitre] = this.saisieChampAffiche2.pasdeSelection() ? "Nombre de Chainons"
									: this.saisieChampAffiche2.getSelection();
							break;
						case 3:
							titre[compteurTitre] = this.saisieChampAffiche3.pasdeSelection() ? "Nombre de Chainons"
									: this.saisieChampAffiche3.getSelection();
							break;
						case 4:
							titre[compteurTitre] = this.saisieChampAffiche4.pasdeSelection() ? "Nombre de Chainons"
									: this.saisieChampAffiche4.getSelection();
							break;
					}
					compteurTitre = compteurTitre + 1;
				}
			}
			donnees.add(titre);
			for (int j = 0; j < chaine.chainons.length; j++) {
				ModeleChaines.Chainon unit = chaine.chainons[j];
				Object[] row = new Object[nbchamp + 1];
				row[0] = unit.texte;
				int compteur = 1;
				for (int i = 0; i < champ.length; i++) {
					if (champ[i]) {
						switch (i) {
							case 0:
								row[compteur] = this.saisieChampAffiche.pasdeSelection() ? labelsNbElements[donneesChaines.calculerValNbElements(unit.nbEltsChaine)]
										: vue.getValeurChamp(unit.unite, this.saisieChampAffiche.getSelection());
								break;
							case 1:
								row[compteur] = this.saisieChampAffiche1.pasdeSelection() ? labelsNbElements[donneesChaines.calculerValNbElements(unit.nbEltsChaine)]
										: vue.getValeurChamp(unit.unite, this.saisieChampAffiche1.getSelection());
								break;
							case 2:
								row[compteur] = this.saisieChampAffiche2.pasdeSelection() ? labelsNbElements[donneesChaines.calculerValNbElements(unit.nbEltsChaine)]
										: vue.getValeurChamp(unit.unite, this.saisieChampAffiche2.getSelection());
								break;
							case 3:
								row[compteur] = this.saisieChampAffiche3.pasdeSelection() ? labelsNbElements[donneesChaines.calculerValNbElements(unit.nbEltsChaine)]
										: vue.getValeurChamp(unit.unite, this.saisieChampAffiche3.getSelection());
								break;
							case 4:
								row[compteur] = this.saisieChampAffiche4.pasdeSelection() ? labelsNbElements[donneesChaines.calculerValNbElements(unit.nbEltsChaine)]
										: vue.getValeurChamp(unit.unite, this.saisieChampAffiche4.getSelection());
								break;
						}
						compteur = compteur + 1;
					}
				}
				donnees.add(row);
			}
		}
		return donnees.toArray(new Object[donnees.size()][nbchamp]);
	}
}
