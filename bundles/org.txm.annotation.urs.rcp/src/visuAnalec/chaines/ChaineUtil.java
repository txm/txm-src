/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package visuAnalec.chaines;

import java.util.*;
import java.util.ArrayList;
import visuAnalec.elements.*;
import visuAnalec.vue.*;

/**
 *
 * @author Marc
 */
public abstract class ChaineUtil {
	
	// renvoie un HashMap qui à un nombre de chainons de la chaine associe le nombre de chaines
	// qui ont ce nombre de chainons
	public static HashMap<Integer, Integer> calculerdonneecourbechainons(String[] typesdispo, Vue vue) {
		String[] typesChaines = vue.getTypesSchemasAVoir();
		HashMap<Integer, Integer> retour = new HashMap();
		for (String typechaine : typesChaines) {
			Schema[] chaines = vue.getSchemas(typechaine);
			for (Schema chaine : chaines) {
				HashSet<Element> contenu = chaine.getContenu();
				int compte = 0;
				for (Element elt : contenu) {
					if (elt.getClass() == Unite.class && tableauRecherche(typesdispo, elt.getType())) {
						compte = compte + 1;
					}
				}
				if (retour.containsKey(compte)) {
					retour.put(compte, retour.get(compte) + 1);
				}
				else {
					retour.put(compte, 1);
				}
			}
		}
		return retour;
	}
	
	
	
	
	
	// Calcule à partir de la position en caractères la position en mot dans une chaine de caractère
	// @require : la position en caractère doit être inférieure à la longueur de la chaine
	private static int calculerPositionMot(int poscaractere, String chaine, Vue vue) {
		int compteur = 1;
		int i = 0;
		while (i < poscaractere) {
			if (chaine.charAt(i) == '\n' || chaine.charAt(i) == ' ' || chaine.charAt(i) == '\r') {
				while (i < poscaractere && (chaine.charAt(i) == '\n' || chaine.charAt(i) == ' '
						|| chaine.charAt(i) == '\r')) {
					i = i + 1;
				}
				compteur = compteur + 1;
			}
			i = i + 1;
		}
		return compteur;
	}
	
	// cherche la position en nombre d'un caractère mot dans le corpus
	public static int calculerPositionMotCorpus(int poscaractere, Vue vue) {
		String corpus = vue.getCorpus().getTexte();
		int resultat = 0;
		Integer[] finparagraphe = vue.getCorpus().getFinParagraphes();
		int finparagraphecourant = 0;
		for (int i = 0; i < corpus.length(); i++) {
			if (corpus.charAt(i) == ' ' || i > finparagraphe[finparagraphecourant]) {
				resultat = resultat + 1;
				if (i > finparagraphe[finparagraphecourant]) {
					finparagraphecourant = finparagraphecourant + 1;
				}
			}
			if (i == poscaractere) {
				return resultat;
			}
		}
		return resultat;
	}
	
	// donne le nombre de paragraphes du corpus
	public static int nbParagraphe(Vue vue) {
		return vue.getCorpus().getFinParagraphes().length;
	}
	
	// donne le nombre de caractères du corpus
	public static int nbCaractere(Vue vue) {
		return vue.getCorpus().getTexte().length();
	}
	
	// donne le nombre de mots du Corpus
	public static int nbMot(Vue vue) {
		return calculerPositionMotCorpus(nbCaractere(vue) - 1, vue);
	}
	
	// donne le nombre d'unité appartenant à une chaine
	public static int nombreDeReference(String[] typesUnite, Vue vue) {
		String[] types = vue.getTypesSchemasAVoir();
		int compte = 0;
		for (String type : types) {
			Schema[] chaines = vue.getSchemas(type);
			for (Schema chaine : chaines) {
				for (Element unit : chaine.getContenu()) {
					if (unit.getClass() == Unite.class && tableauRecherche(typesUnite, unit.getType())) {
						compte = compte + 1;
					}
				}
			}
		}
		return compte;
	}
	
	// donne le nombre de chaines non vides
	public static int nbChaines(Vue vue) {
		String[] types = vue.getTypesSchemasAVoir();
		int compte = 0;
		for (String type : types) {
			Schema[] chaines = vue.getSchemas(type);
			for (Schema chaine : chaines) {
				if (chaine.getContenu().size() > 0) {
					compte = compte + 1;
				}
			}
		}
		return compte;
	}
	
	// calcule le nombre de références par unité de longueur "choix"
	public static float densiteReferentielle(String choix, String[] types, Vue vue) {
		float compte = 0.0f;
		compte = compte + nombreDeReference(types, vue);
		switch (choix) {
			case "Caractère":
				compte = Math.round(compte * 10000.0f / nbCaractere(vue)) / 10000.0f;
				break;
			case "Mot":
				compte = Math.round(compte * 10000.0f / nbMot(vue)) / 10000.0f;
				break;
			case "Paragraphe":
				compte = Math.round(compte * 10000.0f / nbParagraphe(vue)) / 10000.0f;
				break;
			default:
				break;
			
		}
		return compte;
	}
	
	// attention les unités sont dans le désordre
	public static ArrayList<Integer> positionUnite(String[] types, Vue vue) {
		ArrayList<Integer> retour = new ArrayList<>();
		for (String type : types) {
			Unite[] unites = vue.getUnites(type);
			for (Unite unit : unites) {
				retour.add(unit.getDeb());
			}
		}
		return retour;
	}
	
	// renvoie les unités dont le type est contenu dans le tableau "types" puis et triée par ordre d'apparition
	// dans le corpus
	public static Unite[] getUniteesTrieesEtFiltrees(String[] types, Vue vue) {
		ArrayList<Unite> stockage = new ArrayList<>();
		for (String type : types) {
			Unite[] tmp = vue.getUnites(type);
			for (int i = 0; i < tmp.length; i++) {
				stockage.add(tmp[i]);
			}
		}
		Unite[] retour = stockage.toArray(new Unite[stockage.size()]);
		triFusionUnite(retour);
		return retour;
	}
	
	// calcule les position en nombre de mots au sein d'un corpus des unités dont le
	// type est contenu dans le tableau "types" puis et triée par ordre d'apparition
	public static ArrayList<Integer> positionUniteEnMot(String[] types, Vue vue) {
		Unite[] donnees = getUniteesTrieesEtFiltrees(types, vue);
		int postexte = 0;
		int comptemot = 0;
		String texte = vue.getCorpus().getTexte();
		ArrayList<Integer> retour = new ArrayList<>();
		for (int i = 0; i < donnees.length; i++) {
			while (postexte < donnees[i].getDeb()) {
				if (texte.charAt(postexte) == ' ' || texte.charAt(postexte) == '\n') {
					comptemot = comptemot + 1;
				}
				postexte = postexte + 1;
			}
			retour.add(comptemot);
		}
		return retour;
	}
	
	// calcule les écarts en nombre de caractères au sein d'un corpus des unités dont le
	// type est contenu dans le tableau "types"
	// les écarts sont la différence les positions de début de deux unités consécutives respectant toutes deux
	// un type d'unités contenu dans "types"
	public static ArrayList<Integer> ecartUniteCaractere(String[] types, Vue vue) {
		ArrayList<Integer> retour = new ArrayList<>();
		Unite[] unites = getUniteesTrieesEtFiltrees(types, vue);
		for (int i = 0; i < unites.length - 1; i++) {
			retour.add(unites[i + 1].getDeb() - unites[i].getDeb());
		}
		return retour;
	}
	
	// calcule les écarts en nombre de mots au sein d'un corpus des unités dont le
	// type est contenu dans le tableau "types"
	// les écarts sont la différence les positions de début de deux unités consécutives respectant toutes deux
	// un type d'unités contenu dans "types"
	public static ArrayList<Integer> ecartUniteMot(String[] types, Vue vue) {
		ArrayList<Integer> retour = new ArrayList<>();
		Unite[] unites = getUniteesTrieesEtFiltrees(types, vue);
		for (int i = 0; i < unites.length - 1; i++) {
			String texte = vue.getCorpus().getTexte().substring(unites[i].getDeb(), unites[i + 1].getDeb());
			int compte = 0;
			for (int j = 0; j < texte.length(); j++) {
				if (texte.charAt(j) == ' ' || texte.charAt(j) == '\n') {
					compte = compte + 1;
				}
			}
			retour.add(compte);
		}
		return retour;
	}
	
	// détermine les longueurs en mots des unités contenu dans un corpus et dont le
	// type est contenu dans le tableau "types"
	public static ArrayList<Integer> longueurUniteMot(String[] types, Vue vue) {
		ArrayList<Integer> retour = new ArrayList<>();
		for (String type : types) {
			Unite[] unites = vue.getUnites(type);
			for (int i = 0; i < unites.length; i++) {
				String texte = vue.getCorpus().getTexte().substring(unites[i].getDeb(), unites[i].getFin());
				int compte = 0;
				for (int j = 0; j < texte.length(); j++) {
					if (texte.charAt(j) == ' ' || texte.charAt(j) == '\n') {
						compte = compte + 1;
					}
				}
				retour.add(compte);
			}
		}
		return retour;
	}
	
	// détermine les longueurs en nombre de caractères des unités contenu dans un corpus et dont le
	// type est contenu dans le tableau "types"
	public static ArrayList<Integer> longueurUniteCaractere(String[] types, Vue vue) {
		ArrayList<Integer> retour = new ArrayList<>();
		for (String type : types) {
			Unite[] unites = vue.getUnites(type);
			for (int i = 0; i < unites.length; i++) {
				int compte = unites[i].getFin() - unites[i].getDeb();
				retour.add(compte);
			}
		}
		return retour;
	}
	
	// pas optimisé comme un binary search car on a pas d'ordre défini sur les objets du tableau
	// et donc le tableau n'est pas trié
	public static boolean tableauRecherche(Object[] tab, Object elt) {
		for (Object unit : tab) {
			if (unit.equals(elt)) {
				return true;
			}
		}
		return false;
	}
	
	// détermine la longueur des chaines en caractères
	public static ArrayList<Integer> longueurChaineCaractere(String[] types, Vue vue) {
		String[] typeschaine = vue.getTypesSchemasAVoir();
		ArrayList<Integer> retour = new ArrayList<>();
		for (String type : typeschaine) {
			Schema[] chaines = vue.getSchemas(type);
			for (Schema chaine : chaines) {
				HashSet<Element> contenu = chaine.getContenu();
				Integer compte = 0;
				for (Element elt : contenu) {
					if (elt.getClass() == Unite.class && tableauRecherche(types, elt.getType())) {
						Unite tmp = (Unite) elt;
						compte = compte + tmp.getFin() - tmp.getDeb();
					}
				}
				retour.add(compte);
			}
		}
		return retour;
	}
	
	// détermine la longueur des chaines en mots en sommant la longueur des maillons dont le type est contenu
	// dans "types"
	public static ArrayList<Integer> longueurChaineMot(String[] types, Vue vue) {
		String[] typeschaine = vue.getTypesSchemasAVoir();
		ArrayList<Integer> retour = new ArrayList<>();
		for (String type : typeschaine) {
			Schema[] chaines = vue.getSchemas(type);
			for (Schema chaine : chaines) {
				HashSet<Element> contenu = chaine.getContenu();
				Integer compte = 0;
				for (Element elt : contenu) {
					if (elt.getClass() == Unite.class && tableauRecherche(types, elt.getType())) {
						Unite tmp = (Unite) elt;
						String texte = vue.getCorpus().getTexte().substring(tmp.getDeb(), tmp.getFin());
						for (int i = 0; i < texte.length(); i++) {
							if (texte.charAt(i) == ' ' || texte.charAt(i) == '\n') {
								compte = compte + 1;
							}
						}
					}
				}
				retour.add(compte);
			}
		}
		return retour;
	}
	
	
	// calcule le nombre de chainons(unités) dont le type est contenu dans "types" de chaque chaine
	public static ArrayList<Integer> nombreDeChainons(String[] types, Vue vue) {
		String[] typeschaines = vue.getTypesSchemasAVoir();
		ArrayList<Integer> retour = new ArrayList<>();
		for (String type : typeschaines) {
			Schema[] chaines = vue.getSchemas(type);
			for (Schema chaine : chaines) {
				HashSet<Element> contenu = chaine.getContenu();
				Integer compte = 0;
				for (Element elt : contenu) {
					if (elt.getClass() == Unite.class && tableauRecherche(types, elt.getType())) {
						compte = compte + 1;
					}
				}
				retour.add(compte);
			}
		}
		return retour;
	}
	
	
	
	// recherche le numero de paragraphe dans le corpus qui correspond à une position en caractère
	public static int conversionPositionCaracterEtNoParagraphe(int poscaractere, Vue vue) {
		Integer[] noparagraphe = vue.getCorpus().getFinParagraphes();
		for (int i = 0; i < noparagraphe.length; i++) {
			if (poscaractere > noparagraphe[i] || i == noparagraphe.length - 1) {
				return i;
			}
			
		}
		return -1;
	}
	
	
	// @ensures : retourne la longueur d'une unité en caractère
	public static int longueurUnite(Unite donnee) {
		return (donnee.getFin() - donnee.getDeb());
	}
	
	// @ensures : retourne la longueur d'une unite en mot
	public static int nbMotUnite(Unite donnee, Vue vue) {
		String chaine = (vue.getCorpus()).getTexteUnite(donnee);
		return calculerPositionMot(chaine.length(), chaine, vue);
	}
	
	// cherche le maximum d'un tableau pas forcément trié
	public static Integer maxiTab(Integer[] donnees) {
		Integer max = -1;
		if (donnees.length > 0) {
			max = donnees[0];
			for (Integer nbr : donnees) {
				if (nbr > max) {
					max = nbr;
				}
			}
		}
		return max;
	}
	
	// cherche le minimum d'un tableau pas forcément trié
	public static Integer miniTab(Integer[] donnees) {
		Integer min = -1;
		if (donnees.length > 0) {
			min = donnees[0];
			for (Integer nbr : donnees) {
				if (nbr < min) {
					min = nbr;
				}
			}
		}
		return min;
	}
	
	// retourne la somme des éléments d'un tableau
	public static int somme(Integer[] donnees) {
		int total = 0;
		for (Integer nbr : donnees) {
			total = total + nbr;
		}
		return total;
	}
	
	
	public static int nbMotsDansUneUnite(int deb, int fin, Vue vue) {
		String texteunite = vue.getCorpus().getTexte().substring(deb, fin);
		int compte = (deb < fin ? 1 : 0);
		for (int i = 0; i < texteunite.length(); i++) {
			if (texteunite.charAt(i) == ' ') {
				compte = compte + 1;
			}
		}
		return compte;
	}
	
	// permet de trier un tableau de valeurs suivant leur fréquence
	public static void triFusionMap(String tableau[], HashMap<String, Float> freq) {
		int longueur = tableau.length;
		if (longueur > 0) {
			triFusionMap(tableau, 0, longueur - 1, freq);
		}
	}
	
	private static void triFusionMap(String tableau[], int deb, int fin, HashMap<String, Float> freq) {
		if (deb != fin) {
			int milieu = (fin + deb) / 2;
			triFusionMap(tableau, deb, milieu, freq);
			triFusionMap(tableau, milieu + 1, fin, freq);
			fusionMap(tableau, deb, milieu, fin, freq);
		}
	}
	
	private static void fusionMap(String tableau[], int deb1, int fin1, int fin2, HashMap<String, Float> freq) {
		int deb2 = fin1 + 1;
		
		// on recopie les éléments du début du tableau
		String table1[] = new String[fin1 - deb1 + 1];
		for (int i = deb1; i <= fin1; i++) {
			table1[i - deb1] = tableau[i];
		}
		
		int compt1 = deb1;
		int compt2 = deb2;
		
		for (int i = deb1; i <= fin2; i++) {
			if (compt1 == deb2) // c'est que tous les éléments du premier tableau ont été utilisés
			{
				break; // tous les éléments ont donc été classés
			}
			else if (compt2 == (fin2 + 1)) // c'est que tous les éléments du second tableau ont été utilisés
			{
				tableau[i] = table1[compt1 - deb1]; // on ajoute les éléments restants du premier tableau
				compt1++;
			}
			else if (freq.get(table1[compt1 - deb1]) > freq.get(tableau[compt2])) {
				tableau[i] = table1[compt1 - deb1]; // on ajoute un élément du premier tableau
				compt1++;
			}
			else {
				tableau[i] = tableau[compt2]; // on ajoute un élément du second tableau
				compt2++;
			}
		}
	}
	
	// on enlève les unités dont le numero de paragraphe appartient à Noparagraphe
	public static Unite[] filtreparagraphe(Unite[] donnees, ArrayList<Integer> Noparagraphemasque, Vue vue) {
		HashMap<Unite, Integer> noparagraphe = unitenumeroparagraphe(donnees, vue);
		ArrayList<Unite> unitefiltrees = new ArrayList<>();
		for (Unite unit : donnees) {
			if (!(Noparagraphemasque.contains(noparagraphe.get(unit)))) {
				unitefiltrees.add(unit);
			}
		}
		Unite[] resultat = unitefiltrees.toArray(new Unite[0]);
		return resultat;
	}
	
	// renvoie un tableau contenant les fréquences de chaque valeur d'un champ dans la liste d'unités
	public static HashMap<String, Float> calculerfrequencechamp(Unite[] donnees, String typeUnites, String Champcible, Vue vue) {
		String[] valsdisponibles = vue.getValeursChamp(Unite.class, typeUnites, Champcible);
		HashMap<String, Float> tableaufrequence = new HashMap<>(valsdisponibles.length + 1);
		String valeurchamp;
		// on initialise le tableau résultat
		for (String valeur : valsdisponibles) {
			tableaufrequence.put(valeur, 0.0f);
		}
		tableaufrequence.put("", 0.0f);
		// on compte le nombre d'occurences
		for (Unite unit : donnees) {
			valeurchamp = vue.getValeurChamp(unit, Champcible);
			tableaufrequence.put(valeurchamp, tableaufrequence.get(valeurchamp) + 1);
		}
		// on calcule les pourcentages
		for (String valeur : valsdisponibles) {
			tableaufrequence.put(valeur, tableaufrequence.get(valeur) * 10000.0f /
					donnees.length / 100.0f);
		}
		tableaufrequence.put("", tableaufrequence.get("") * 10000.0f /
				donnees.length / 100.0f);
		
		return tableaufrequence;
	}
	
	// détermine la moyenne par rapport à la deuxième variable d'un HashMap
	public static float Esperance(HashMap<Object, Integer> donnees) {
		float moyenne = 0.0f;
		for (Object i : donnees.keySet()) {
			moyenne = moyenne + donnees.get(i);
		}
		moyenne = moyenne / donnees.size();
		return moyenne;
	}
	
	// détermine la moyenne des valeurs d'un tableau
	public static float Esperance(Integer[] donnees) {
		float moyenne = 0.0f;
		for (Integer i : donnees) {
			moyenne = moyenne + i;
		}
		moyenne = moyenne / donnees.length;
		return moyenne;
	}
	
	// détermine la variance par rapport à la deuxième variable d'un HashMap
	public static float Variance(HashMap<Object, Integer> donnees) {
		float moyenne = Esperance(donnees);
		float variance = 0.0f;
		if (donnees.size() > 1) {
			for (Object i : donnees.keySet()) {
				variance = variance + (donnees.get(i) - moyenne) * (donnees.get(i) - moyenne);
			}
			variance = variance / (donnees.size() - 1);
		}
		
		return variance;
	}
	
	// détermine la variance des valeurs d'un tableau
	public static float Variance(Integer[] donnees) {
		float moyenne = Esperance(donnees);
		float variance = 0.0f;
		if (donnees.length > 1) {
			for (Integer i : donnees) {
				variance = variance + (i - moyenne) * (i - moyenne);
			}
			variance = variance / (donnees.length - 1);
		}
		return variance;
	}
	
	// permet de trier les unités d'un tableau
	// la relation d'ordre entre les unités est la comparaison des positions de débuts des unités au sein du texte
	public static void triFusionUnite(Unite tableau[]) {
		int longueur = tableau.length;
		if (longueur > 0) {
			triFusionUnite(tableau, 0, longueur - 1);
		}
	}
	
	private static void triFusionUnite(Unite tableau[], int deb, int fin) {
		if (deb != fin) {
			int milieu = (fin + deb) / 2;
			triFusionUnite(tableau, deb, milieu);
			triFusionUnite(tableau, milieu + 1, fin);
			fusionUnite(tableau, deb, milieu, fin);
		}
	}
	
	private static void fusionUnite(Unite tableau[], int deb1, int fin1, int fin2) {
		int deb2 = fin1 + 1;
		
		// on recopie les éléments du début du tableau
		Unite table1[] = new Unite[fin1 - deb1 + 1];
		for (int i = deb1; i <= fin1; i++) {
			table1[i - deb1] = tableau[i];
		}
		
		int compt1 = deb1;
		int compt2 = deb2;
		
		for (int i = deb1; i <= fin2; i++) {
			if (compt1 == deb2) // c'est que tous les éléments du premier tableau ont été utilisés
			{
				break; // tous les éléments ont donc été classés
			}
			else if (compt2 == (fin2 + 1)) // c'est que tous les éléments du second tableau ont été utilisés
			{
				tableau[i] = table1[compt1 - deb1]; // on ajoute les éléments restants du premier tableau
				compt1++;
			}
			else if (table1[compt1 - deb1].getDeb() < tableau[compt2].getDeb()) {
				tableau[i] = table1[compt1 - deb1]; // on ajoute un élément du premier tableau
				compt1++;
			}
			else {
				tableau[i] = tableau[compt2]; // on ajoute un élément du second tableau
				compt2++;
			}
		}
	}
	
	// permet de trier les unités d'une arrayList
	// la relation d'ordre entre les unités est la comparaison des positions de débuts des unités au sein du texte
	public static void triFusionUnite(ArrayList<Unite> tableau) {
		int longueur = tableau.size();
		if (longueur > 0) {
			triFusionUnite(tableau, 0, longueur - 1);
		}
	}
	
	private static void triFusionUnite(ArrayList<Unite> tableau, int deb, int fin) {
		if (deb != fin) {
			int milieu = (fin + deb) / 2;
			triFusionUnite(tableau, deb, milieu);
			triFusionUnite(tableau, milieu + 1, fin);
			fusionUnite(tableau, deb, milieu, fin);
		}
	}
	
	private static void fusionUnite(ArrayList<Unite> tableau, int deb1, int fin1, int fin2) {
		int deb2 = fin1 + 1;
		
		// on recopie les éléments du début du tableau
		Unite table1[] = new Unite[fin1 - deb1 + 1];
		for (int i = deb1; i <= fin1; i++) {
			table1[i - deb1] = tableau.get(i);
		}
		
		int compt1 = deb1;
		int compt2 = deb2;
		
		for (int i = deb1; i <= fin2; i++) {
			if (compt1 == deb2) // c'est que tous les éléments du premier tableau ont été utilisés
			{
				break; // tous les éléments ont donc été classés
			}
			else if (compt2 == (fin2 + 1)) // c'est que tous les éléments du second tableau ont été utilisés
			{
				tableau.add(i, table1[compt1 - deb1]); // on ajoute les éléments restants du premier tableau
				tableau.remove(i + 1);
				compt1++;
			}
			else if (table1[compt1 - deb1].getDeb() < tableau.get(compt2).getDeb()) {
				tableau.add(i, table1[compt1 - deb1]); // on ajoute un élément du premier tableau
				tableau.remove(i + 1);
				compt1++;
			}
			else {
				tableau.add(i, tableau.get(compt2)); // on ajoute un élément du second tableau
				tableau.remove(i + 1);
				compt2++;
			}
		}
	}
	
	// permet de créer un ensemble de points à intervalle régulier entre deux bornes
	public static int[] discretisation(int deb, int fin, int nbpoints) {
		int[] subdivision = new int[nbpoints];
		try {
			int pas = (fin - deb) / (nbpoints - 1);
			for (int i = 0; i < nbpoints; i++) {
				subdivision[i] = deb + i * pas;
			}
		}
		catch (ArithmeticException e) {
			subdivision[0] = (deb + fin) / 2;
		}
		return subdivision;
	}
	
	// @require : les paragraphes et les unités sont triés dans l'ordre
	// @ensures : retourne une table associant les unités à leur numero de
	// paragraphe
	public static HashMap<Unite, Integer> unitenumeroparagraphe(Unite[] unites, Vue vue) {
		HashMap<Unite, Integer> numeroparagraphe = new HashMap<>(unites.length);
		int[] noparagraphe = (vue.getCorpus()).calculerNoParagraphe(unites);
		for (int i = 0; i < unites.length; i++) {// pour toutes les unites
			numeroparagraphe.put(unites[i], noparagraphe[i]);
		}
		return numeroparagraphe;
	}
	
	// le nouveau type s'appellera "tout"
	// attention les relations ne sont pas copiées pour vérifier pour maintenir le lien entre les unités
	public static void fusionTypeUnite(Vue vue) {
		// on initialise les propriétés du nouveau type
		long deb = System.nanoTime();
		HashSet<String> nomsChamps = new HashSet<>();
		for (String champ : (vue.getTypesUnitesAVoir().length == 0 ? new String[0] : vue.getNomsChamps(Unite.class, vue.getTypesUnitesAVoir()[0]))) {
			nomsChamps.add(champ);
		}
		// les propriétés du nouveau seront l'intersection des propriétés de tous les types
		for (String type : vue.getTypesUnitesAVoir()) {
			HashSet<String> nomsChampNonFiltres = (HashSet<String>) nomsChamps.clone();
			nomsChamps = new HashSet<>();
			for (String champ : vue.getNomsChamps(Unite.class, type)) {
				// si le champ est partagé par tous les autres types d'unités déjà passé en revue
				// alors on l'ajoute à l'intersection provisoire
				if (nomsChampNonFiltres.contains(champ)) {
					nomsChamps.add(champ);
				}
			}
		}
		// on parcours toutes les unités
		for (String type : vue.getTypesUnitesAVoir()) {
			for (Unite unit : vue.getUnites(type)) {
				Unite cloneUnite = new Unite("tout", unit.getDeb(), unit.getFin());
				for (String champ : nomsChamps) {
					cloneUnite.putProp(champ, vue.getValeurChamp(unit, champ));
				}
				vue.getCorpus().addUniteConvertit(cloneUnite);
				// on ajoute la nouvelle unité aux autre schemas
				for (Schema schemasContenant : unit.getSchemas()) {
					vue.getCorpus().ajouterElementSchemaSaisi(cloneUnite, schemasContenant);
				}
			}
		}
		long fin = System.nanoTime();
		System.out.println((fin - deb) / 1000000);
	}
	
	// crée un type de schéma et les schémas correspondant à partir des choix de l'utilisateur
	/*
	 * Deux éléments d'une même relation appartiennent au même Schéma et on crée un shéma
	 * pour chaque valeur différente d'une propriété de la relation
	 */
	public static Schema[] conversion(String source, String nomType, String typeSource, String primaryKey, String[] otherKey,
			Vue vue) {
		ArrayList<Schema> retour = new ArrayList<>();
		HashMap<String, Schema> triSchema = new HashMap<>();
		long deb = System.nanoTime();
		switch (source) {
			case "Relation":
				Relation[] donnees = vue.getRelations(typeSource);
				// pour toutes les relations
				for (Relation Rel : donnees) {
					String valeur = vue.getValeurChamp(Rel, primaryKey);
					// si aucun schema n'a la même valeur que la relation pour la clé primaire
					if (!triSchema.containsKey(valeur)) {
						triSchema.put(valeur, new Schema(nomType));
						HashMap<String, String> propSchema = triSchema.get(valeur).getProps();
						// le schema prend les mêmes propriétés que le schéma
						for (String champ : Rel.getProps().keySet()) {
							propSchema.put(champ, Rel.getProp(champ));
						}
					}
					triSchema.get(valeur).ajouter(Rel.getElt1());
					triSchema.get(valeur).ajouter(Rel.getElt2());
				}
				break;
			case "typeUnite":
				// pour tous les types d'unités on crée un schéma
				String[] typesUnites = vue.getTypesUnitesAVoir();
				for (String type : typesUnites) {
					triSchema.put(type, new Schema(nomType));
					HashMap<String, String> propSchema = triSchema.get(type).getProps();
					propSchema.put("type", type);
					for (Unite unit : vue.getUnites(type)) {
						triSchema.get(type).ajouter(unit);
					}
				}
				break;
			case "attributUnite":
				Unite[] UnitesAVoir = vue.getUnites(typeSource);
				// pour toutes les unités
				for (Unite unit : UnitesAVoir) {
					String valeur = vue.getValeurChamp(unit, primaryKey);
					// si aucun schéma n'a la même valeur que l'unité pour la clé primaire
					if (!triSchema.containsKey(valeur)) {
						triSchema.put(valeur, new Schema(nomType));
						HashMap<String, String> propSchema = triSchema.get(valeur).getProps();
						// le nouveau schéma prend les mêmes propriétés que l'unité
						propSchema.put(primaryKey, unit.getProp(primaryKey));
						for (String champ : otherKey) {
							propSchema.put(champ, unit.getProp(champ));
						}
					}
					triSchema.get(valeur).ajouter(unit);
				}
				break;
			default:
				break;
			
		}
		for (String valeur : triSchema.keySet()) {
			vue.getCorpus().addSchemaConvertit(triSchema.get(valeur));
		}
		long fin = System.nanoTime();
		System.out.println((fin - deb) / 1000000);
		
		return retour.toArray(new Schema[0]);
	}
	
	// calcule les donnees nécessaires pour l'affichage du graphique dont l'axe des abscisses est le numéro de
	// paragraphe
	public static HashMap<Integer, HashMap<String, Integer>> calculedonneecourbe(String[] typesdispo, String unitecomptage, Vue vue) {
		HashMap<Integer, HashMap<String, Integer>> donnees = new HashMap();
		String texte = vue.getCorpus().getTexte();
		Integer[] noparagraphes = vue.getCorpus().getFinParagraphes();
		int unitecourante = 0;
		int paragraphecourant = 0;
		int comptemot = 0;
		int sommeUniteComptage = 0;
		HashMap<Unite, Schema> chainesUnitesNonFiltrees = calculerChainesUnitesNonFiltrees(typesdispo, vue);
		HashSet<Schema> chaineparagraphe = new HashSet();
		Unite[] unites = ChaineUtil.getUniteesTrieesEtFiltrees(typesdispo, vue);
		// initialisation
		for (int i = 0; i < noparagraphes.length; i++) {
			donnees.put(i, new HashMap());
			donnees.get(i).put("Nombre de Chaines de coreference", 0);
			donnees.get(i).put("Nombre de chainons", 0);
			donnees.get(i).put("ratio nombre de chainons par nombre de " + unitecomptage, 0);
			donnees.get(i).put("Densité référentielle", 0);
		}
		for (int i = 0; i < texte.length(); i++) {
			if (i > noparagraphes[paragraphecourant] || i == texte.length() - 1) {
				donnees.get(paragraphecourant).put("Nombre de Chaines de coreference", chaineparagraphe.size());
				donnees.get(paragraphecourant).put("ratio nombre de chainons par nombre de " + unitecomptage,
						Math.round(sommeUniteComptage * 100.0f / (unitecomptage.equals("Mots") ? comptemot
								: (noparagraphes[paragraphecourant] - (paragraphecourant == 0 ? 0 : noparagraphes[paragraphecourant - 1])))));
				donnees.get(paragraphecourant).put("Densité référentielle",
						Math.round((100.0f * donnees.get(paragraphecourant).get("Nombre de chainons")) /
								(unitecomptage == "Mots" ? comptemot
										: (noparagraphes[paragraphecourant] -
												(paragraphecourant == 0 ? 0 : noparagraphes[paragraphecourant - 1])))));
				chaineparagraphe = new HashSet();
				paragraphecourant = paragraphecourant + 1;
				comptemot = 1;
				sommeUniteComptage = 0;
			}
			// si on decouvre une unité
			while (unitecourante < unites.length && unites[unitecourante].getDeb() == i) {
				if (chainesUnitesNonFiltrees.containsKey(unites[unitecourante])) {
					donnees.get(paragraphecourant).put("Nombre de chainons",
							donnees.get(paragraphecourant).get("Nombre de chainons") + 1);
					chaineparagraphe.add(chainesUnitesNonFiltrees.get(unites[unitecourante]));
					sommeUniteComptage = sommeUniteComptage + (unitecomptage == "Mots" ? ChaineUtil.nbMotsDansUneUnite(unites[unitecourante].getDeb(), unites[unitecourante].getFin(), vue)
							: unites[unitecourante].getFin() - unites[unitecourante].getDeb());
				}
				unitecourante = unitecourante + 1;
			}
			// si on change de mot
			if (unitecomptage.equals("Mots") && texte.charAt(i) == ' ') {
				comptemot = comptemot + 1;
			}
		}
		
		return donnees;
	}
	
	
	// struture qui n'est pas filtrées mais qui associe à une unité qui correspond au bon type d'unités
	// contenu dans "typeUnités" la chaine qui la contient et qui est elle aussi du bon type de Schéma
	private static HashMap<Unite, Schema> calculerChainesUnitesNonFiltrees(String[] typeUnites, Vue vue) throws UnsupportedOperationException {
		HashMap<Unite, Schema> chainesUnitesNonFiltrees = new HashMap();
		for (String typechaine : vue.getTypesSchemasAVoir()) {
			Schema[] chaines = vue.getSchemas(typechaine);
			for (Schema chaine : chaines) {
				HashSet<Element> contenu = chaine.getContenu();
				for (Element elt : contenu) {
					if (elt.getClass() == Unite.class && ChaineUtil.tableauRecherche(typeUnites, elt.getType())) {
						if (chainesUnitesNonFiltrees.containsKey(elt)) {
							throw new UnsupportedOperationException("L'unité " + vue.getIdElement(elt)
									+ " appartient à deux chaînes : " + vue.getIdElement(chaine) + " et "
									+ vue.getIdElement(chainesUnitesNonFiltrees.get(elt)));
						}
						else
							chainesUnitesNonFiltrees.put((Unite) elt, chaine);
					}
				}
			}
		}
		return chainesUnitesNonFiltrees;
	}
	
	// renvoie la valeur en un point d'un polynome interpolé à partir de deux tableau représentant les coordonnées de points
	public static Integer interpolationLagrangienne(Integer[] abscisse, Integer[] ordonnee, int value) {
		if (abscisse.length == ordonnee.length) {
			Double retour = 0.0d;
			for (int i = 0; i < abscisse.length; i++) {
				Double produit = 1.0d;
				for (int j = 0; j < abscisse.length; j++) {
					if (i != j) {
						produit = produit * (value - abscisse[j]) / (abscisse[i] - abscisse[j]);
					}
				}
				retour = retour + produit * ordonnee[i];
			}
			return (Math.round(retour) < 0 ? 0 : (int) Math.round(retour));
			
		}
		return -1;
	}
	
	/* @require: abscisse and length have the same length */
	public static ArrayList<Float[]> coef_spline_quad(Integer[] abscisse, Integer[] ordonnee) {
		Float[] coef_abs = new Float[abscisse.length];
		Float[] coef_ord = new Float[ordonnee.length];
		ArrayList<Float[]> retour = new ArrayList<>();
		coef_abs[0] = 0.0f;
		for (int i = 1; i < abscisse.length; i++) {
			coef_abs[i] = 1.0f * 2 * (ordonnee[i] - ordonnee[i - 1]) / (abscisse[i] - abscisse[i - 1]);
			coef_ord[i] = ordonnee[i - 1] + coef_abs[i - 1] * (abscisse[i] - abscisse[i - 1]) / 2;
			
		}
		retour.add(coef_abs);
		retour.add(coef_ord);
		return retour;
		
	}
	
	/* @require: abscisse and length have the same length superior to 2 */
	public static Integer interpolation_spline_quad(Integer[] abscisse, Integer[] ordonnee, int value) {
		float retour;
		ArrayList<Float[]> coef = coef_spline_quad(abscisse, ordonnee);
		retour = coef.get(1)[1];
		for (int i = 1; i < abscisse.length; i++) {
			if (value > abscisse[i - 1] && value < abscisse[i]) {
				retour = 0 - coef.get(0)[i - 1] * (value - abscisse[i]) * (value - abscisse[i]) + coef.get(0)[i]
						* (value - abscisse[i - 1]) * (value - abscisse[i - 1]) / 2 / (abscisse[i] - abscisse[i - 1])
						+ coef.get(1)[i];
			}
		}
		return Math.round(retour);
	}
	
}
