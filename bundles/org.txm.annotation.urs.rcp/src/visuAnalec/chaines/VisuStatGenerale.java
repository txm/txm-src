/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package visuAnalec.chaines;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import org.jfree.chart.ChartPanel;
import visuAnalec.*;
import visuAnalec.elements.*;
import visuAnalec.fichiers.FichiersJava;
import visuAnalec.util.*;
import visuAnalec.util.GMessages.GAction;
import visuAnalec.util.GVisu.Val0ComboBox;
import visuAnalec.vue.*;

/**
 *
 * @author Marc
 */
public class VisuStatGenerale extends JFrame implements Vue.VueListener, ActionListener {
	
	private Vue vue;
	
	private ModeleChaines donneesChaines;
	
	private String[] typesChainesAffiches;
	
	private HashSet<String> typesChainesMasques = new HashSet();
	
	private Val0ComboBox saisieUniteComptage;
	
	private JList legendeTypeUnite;
	
	private Val0ComboBox saisieAxeOrdonnee;
	
	private Val0ComboBox saisieAxeAbscisse;
	
	private Val0ComboBox saisieModeAffichage;
	
	private JPopupMenu popupChamp = new JPopupMenu();
	
	private PanneauEditeur panneauEditeur;
	
	private Box listeStat = Box.createVerticalBox();
	
	private ChartPanel graphique;
	
	private JMenuBar menuBar = new JMenuBar();
	
	private String[] StatPossiblesAbscisses = new String[] { "Nombre de chainons", "Numéro de paragraphe" };
	
	private String[] StatPossiblesOrdonnees;
	
	private String[] ModesPossibles = new String[] { "Activer l'interpolation", "Désactiver l'affichage des valeurs nulles" };
	
	private Object[][] donneesGenerales = new Object[0][0];
	
	public VisuStatGenerale(Container fenetre, final Vue vue, PanneauEditeur pEditeur) {
		super("Analyse générale du texte");
		Point pos = new Point(fenetre.getX() + 20, fenetre.getY() + 20);
		Dimension dim = new Dimension(fenetre.getWidth(), fenetre.getHeight());
		setLocation(pos);
		setSize(dim);
		this.vue = vue;
		vue.addEventListener(this);
		typesChainesMasques = new HashSet();
		this.panneauEditeur = pEditeur;
		saisieUniteComptage = new Val0ComboBox("Caractère");
		saisieUniteComptage.addActionListener(this);
		saisieAxeOrdonnee = new Val0ComboBox("Rien");
		saisieAxeOrdonnee.addActionListener(this);
		saisieAxeAbscisse = new Val0ComboBox("Rien");
		saisieAxeAbscisse.addActionListener(this);
		saisieModeAffichage = new Val0ComboBox("Normal");
		saisieModeAffichage.addActionListener(this);
		Box commandes = Box.createVerticalBox();
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Statistiques : "));
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Selectionner une unité de comptage : "));
		commandes.add(Box.createVerticalGlue());
		commandes.add(saisieUniteComptage);
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Type unité : "));
		commandes.add(Box.createVerticalGlue());
		
		legendeTypeUnite = new JList();
		legendeTypeUnite.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mousePressed(MouseEvent e) {// on gère les évènements de la souris
				check(e);
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				check(e);
			}
			
			public void check(MouseEvent e) {
				if (e.isPopupTrigger()) {// if the event shows the menu
					popupChampInit(legendeTypeUnite.locationToIndex(e.getPoint()));
					popupChamp.show(legendeTypeUnite, e.getX(), e.getY());
				}
			}
		});
		commandes.add(legendeTypeUnite);
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Ordonnée : "));
		commandes.add(Box.createVerticalGlue());
		commandes.add(saisieAxeOrdonnee);
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Abscisse : "));
		commandes.add(Box.createVerticalGlue());
		commandes.add(saisieAxeAbscisse);
		commandes.add(Box.createVerticalGlue());
		commandes.add(new JLabel("Mode d'Affichage : "));
		commandes.add(Box.createVerticalGlue());
		commandes.add(saisieModeAffichage);
		commandes.add(Box.createVerticalGlue());
		commandes.add(listeStat);
		saisieUniteComptage.setAlignmentX(0);
		legendeTypeUnite.setAlignmentX(0);
		saisieAxeOrdonnee.setAlignmentX(0);
		saisieAxeAbscisse.setAlignmentX(0);
		saisieModeAffichage.setAlignmentX(0);
		
		JScrollPane scrollcommandes = new JScrollPane(commandes);
		graphique = Courbe.courbeFactory();
		JScrollPane scrollcourbe = new JScrollPane(graphique);
		JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scrollcommandes, scrollcourbe);
		split.setDividerLocation(1. / 2);
		setJMenuBar(menuBar);
		JMenu corpusMenu = new JMenu("Paramètres d'affichage");
		menuBar.add(corpusMenu);
		;
		corpusMenu.add(new GAction("Reprendre un affichage") {
			
			@Override
			public void executer() {
				importerAffichage();
			}
		});
		corpusMenu.add(new GAction("Sauver les paramètres d'affichage") {
			
			@Override
			public void executer() {
				exporterAffichage();
			}
		});
		JMenu csvMenu = new JMenu("Exporter les données");
		menuBar.add(csvMenu);
		;
		csvMenu.add(new GAction("Exporter les données de filtrage") {
			
			@Override
			public void executer() {
				ArrayList<Object[]> donnees = new ArrayList<>();
				donnees.add(new Object[] { "Unité", "type d'unité" });
				for (String typeUnit : vue.getTypesUnitesAVoir()) {
					if (!typesChainesMasques.contains(typeUnit)) {
						for (Unite unit : vue.getUnites(typeUnit)) {
							donnees.add(new Object[] { vue.getTexteUnite(unit), unit.getType() });
						}
					}
				}
				ExportCSV.exporterCSV(donnees.toArray(new Object[donnees.size()][2]));
			}
		});
		csvMenu.add(new GAction("Exporter les statiques générales") {
			
			@Override
			public void executer() {
				ExportCSV.exporterCSV(donneesGenerales);
			}
		});
		getContentPane().add(split);
		setVisible(true);
		// Fermeture de la fenêtre
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent evt) {
				fermerFenetre();
			}
		});
		donneesChaines = new ModeleChaines(vue, new GChaines(this));
		donneesChaines.permettreaffichage(false);
		saisieUniteComptage.setModele(new String[] { "Mots", "Caractère" });
		saisieAxeAbscisse.setModele(StatPossiblesAbscisses);
		saisieModeAffichage.setModele(ModesPossibles);
	}
	
	private void fermerFenetre() {
		vue.removeEventListener(this);
		dispose();
	}
	
	@Override
	public void traiterEvent(Message evt) { // très grossier : à améliorer
		switch (evt.getType()) {
			case CLEAR_CORPUS:
				fermerFenetre();
				return;
			case NEW_CORPUS:  // impossible en principe : CLEAR_CORPUS a détruit la fenêtre
				throw new UnsupportedOperationException("Fenêtre chaines présente lors d'un message NEW_CORPUS ???");
			case MODIF_TEXTE:
				fermerFenetre();
				return;
			case MODIF_VUE:
				fermerFenetre();
				return;
			case MODIF_STRUCTURE:
				fermerFenetre();
				return;
			case MODIF_ELEMENT:
				// ElementEvent evtE = (ElementEvent) evt;
				// if (evtE.getModif() != TypeModifElement.BORNES_UNITE) fermerFenetre();
				fermerFenetre();
				return;
			case CORPUS_SAVED:
				fermerFenetre();
				return;
			default:
				throw new UnsupportedOperationException("Cas " + evt.getType() + " oublié dans un switch");
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		try {
			if (evt.getSource().equals(saisieUniteComptage)) {
				if (vue.getTypesUnitesAVoir() == null || saisieUniteComptage.pasdeSelection()) {
					initVide();
				}
				else {
					init();
				}
				return;
			}
			if (evt.getSource().equals(saisieAxeOrdonnee)) {
				if (!saisieAxeAbscisse.pasdeSelection() && !saisieAxeOrdonnee.pasdeSelection()) {
					affichercourbe();
				}
				return;
			}
			if (evt.getSource().equals(saisieAxeAbscisse)) {
				if (!saisieUniteComptage.pasdeSelection()) {
					initChampOrdonnee();
				}
				return;
			}
			if (evt.getSource().equals(saisieModeAffichage)) {
				if (!saisieAxeAbscisse.pasdeSelection() && !saisieAxeOrdonnee.pasdeSelection()) {
					affichercourbe();
				}
				return;
			}
			throw new UnsupportedOperationException("Evénement non prévu : " + evt.paramString());
		}
		catch (Throwable ex) {
			GMessages.erreurFatale(ex);
		}
	}
	
	private void initTypeUnite() {
		String[] legende = new String[typesChainesAffiches.length];
		for (int i = 0; i < typesChainesAffiches.length; i++) {
			if (typesChainesMasques.contains(typesChainesAffiches[i])) {
				legende[i] = "" + typesChainesAffiches[i].toString() + "     Non";
			}
			else {
				legende[i] = "" + typesChainesAffiches[i].toString() + "     Oui";
			}
		}
		legendeTypeUnite.setListData(legende);
	}
	
	private void popupChampInit(final int index) {
		final boolean masquee;
		masquee = typesChainesMasques.contains(typesChainesAffiches[index]);
		popupChamp.removeAll();
		popupChamp.add(new GAction(masquee ? "Montrer ce type d'unité" : "Masquer ce type d'unité") {
			
			@Override
			public void executer() {
				modifierStatutValChampType(masquee, index, false);
			}
		});
		popupChamp.add(new GAction(masquee ? "Montrer tous les types" : "Ne montrer que ce type d'unité") {
			
			@Override
			public void executer() {
				modifierStatutValChampType(masquee, index, true);
			}
		});
		
	}
	
	private void modifierStatutValChampType(boolean masquee, int index, boolean tous) {
		if (!tous) {
			if (masquee) {
				typesChainesMasques.remove(typesChainesAffiches[index]);
			}
			else {
				typesChainesMasques.add(typesChainesAffiches[index]);
			}
		}
		else if (masquee) {
			typesChainesMasques.clear();
		}
		else {
			typesChainesMasques.clear();
			for (String valeur : typesChainesAffiches) {
				if (!valeur.equals(typesChainesAffiches[index])) {
					typesChainesMasques.add(valeur);
				}
			}
			
		}
		afficherCalculs();
		if (!saisieAxeAbscisse.pasdeSelection() && !saisieAxeOrdonnee.pasdeSelection()) {
			affichercourbe();
		}
	}
	
	private void initVide() {
		listeStat.removeAll();
		saisieAxeOrdonnee.setModeleVide();
		legendeTypeUnite.setListData(new String[0]);
	}
	
	private String[] getTypesAutorises() {
		String[] retour;
		if (typesChainesAffiches.length - typesChainesMasques.size() > 0) {
			retour = new String[typesChainesAffiches.length - typesChainesMasques.size()];
			int compte = 0;
			for (String type : typesChainesAffiches) {
				if (!typesChainesMasques.contains(type)) {
					retour[compte] = type;
					compte = compte + 1;
				}
			}
		}
		else {
			retour = new String[typesChainesAffiches.length];
			int compte = 0;
			for (String type : typesChainesAffiches) {
				retour[compte] = type;
				compte = compte + 1;
			}
		}
		return retour;
	}
	
	private void init() {
		typesChainesAffiches = vue.getTypesUnitesAVoir();
		saisieAxeAbscisse.setModele(StatPossiblesAbscisses);
		initChampOrdonnee();
		afficherCalculs();
		
	}
	
	private void afficherCalculs() {
		initTypeUnite();
		String unite = saisieUniteComptage.getSelection();
		listeStat.removeAll();
		this.donneesGenerales = new Object[20][2];
		int donnee;
		float donneef;
		String[] typesDispo = getTypesAutorises();
		listeStat.add(Box.createVerticalGlue());
		donnee = ChaineUtil.nbCaractere(vue);
		listeStat.add(new JLabel("Nombre de Caractères : " + donnee));
		this.donneesGenerales[0][0] = "Nombre de Caractères";
		this.donneesGenerales[0][1] = donnee;
		listeStat.add(Box.createVerticalGlue());
		donnee = ChaineUtil.nbMot(vue);
		listeStat.add(new JLabel("Nombre de Mots : " + donnee));
		this.donneesGenerales[1][0] = "Nombre de Mots";
		this.donneesGenerales[1][1] = donnee;
		listeStat.add(Box.createVerticalGlue());
		donnee = ChaineUtil.nbParagraphe(vue);
		listeStat.add(new JLabel("Nombre de Paragraphes : " + donnee));
		this.donneesGenerales[2][0] = "Nombre de Paragraphes";
		this.donneesGenerales[2][1] = donnee;
		listeStat.add(Box.createVerticalGlue());
		donnee = ChaineUtil.getUniteesTrieesEtFiltrees(typesDispo, vue).length;
		listeStat.add(new JLabel("Nombre d'unités : " + donnee));
		this.donneesGenerales[3][0] = "Nombre d'unités";
		this.donneesGenerales[3][1] = donnee;
		listeStat.add(Box.createVerticalGlue());
		donnee = ChaineUtil.nombreDeReference(typesDispo, vue);
		listeStat.add(new JLabel("Nombre de Références : " + donnee));
		this.donneesGenerales[4][0] = "Nombre de Références";
		this.donneesGenerales[4][1] = donnee;
		listeStat.add(Box.createVerticalGlue());
		donnee = ChaineUtil.nbChaines(vue);
		listeStat.add(new JLabel("Nombre de Chaines : " + donnee));
		this.donneesGenerales[5][0] = "Nombre de Chaines";
		this.donneesGenerales[5][1] = donnee;
		listeStat.add(Box.createVerticalGlue());
		Integer[] tmp = ChaineUtil.nombreDeChainons(typesDispo, vue).toArray(new Integer[0]);
		donneef = ChaineUtil.Esperance(tmp);
		listeStat.add(new JLabel("Nombre moyen de Chainons par Chaine : " + donneef));
		this.donneesGenerales[6][0] = "Nombre moyen de Chainons par Chaine";
		this.donneesGenerales[6][1] = donneef;
		listeStat.add(Box.createVerticalGlue());
		donneef = ChaineUtil.Variance(tmp);
		listeStat.add(new JLabel("Variance du Nombre de Chainons par Chaine : " + donneef));
		this.donneesGenerales[7][0] = "Variance du Nombre de Chainons par Chaine";
		this.donneesGenerales[7][1] = donneef;
		listeStat.add(Box.createVerticalGlue());
		donneef = ChaineUtil.densiteReferentielle("Paragraphe", typesDispo, vue);
		listeStat.add(new JLabel("Densité référentielle par paragraphe : " + donneef));
		this.donneesGenerales[8][0] = "Densité référentielle par paragraphe";
		this.donneesGenerales[8][1] = donneef;
		listeStat.add(Box.createVerticalGlue());
		switch (unite) {
			case "Mots":
				donneef = ChaineUtil.densiteReferentielle("Mot", typesDispo, vue);
				listeStat.add(new JLabel("Densité référentielle par nombre de mots : " + donneef));
				this.donneesGenerales[9][0] = "Densité référentielle par nombre de mots";
				this.donneesGenerales[9][1] = donneef;
				listeStat.add(Box.createVerticalGlue());
				tmp = ChaineUtil.positionUnite(typesDispo, vue).toArray(new Integer[0]);
				// ici la position moyenne est calculé en caractère puis convertit en mots pour plus de précisions
				donneef = ChaineUtil.calculerPositionMotCorpus(Math.round(ChaineUtil.Esperance(tmp)), vue);
				listeStat.add(new JLabel("Position moyenne des unités : " + donneef));
				this.donneesGenerales[10][0] = "Position moyenne des unités";
				this.donneesGenerales[10][1] = donneef;
				listeStat.add(Box.createVerticalGlue());
				tmp = ChaineUtil.positionUniteEnMot(typesDispo, vue).toArray(new Integer[0]);
				donneef = ChaineUtil.Variance(tmp);
				listeStat.add(new JLabel("Variance des positions des unités : " + donneef));
				this.donneesGenerales[11][0] = "Variance des positions des unités";
				this.donneesGenerales[11][1] = donneef;
				listeStat.add(Box.createVerticalGlue());
				tmp = ChaineUtil.ecartUniteMot(typesDispo, vue).toArray(new Integer[0]);
				donneef = ChaineUtil.Esperance(tmp);
				listeStat.add(new JLabel("Ecart moyen entre les unités : " + donneef));
				this.donneesGenerales[12][0] = "Ecart moyen entre les unités";
				this.donneesGenerales[12][1] = donneef;
				listeStat.add(Box.createVerticalGlue());
				donneef = ChaineUtil.Variance(tmp);
				listeStat.add(new JLabel("Variance des écarts des unités : " + donneef));
				this.donneesGenerales[13][0] = "Variance des écarts des unités";
				this.donneesGenerales[13][1] = donneef;
				listeStat.add(Box.createVerticalGlue());
				tmp = ChaineUtil.longueurUniteMot(typesDispo, vue).toArray(new Integer[0]);
				donnee = ChaineUtil.somme(tmp);
				listeStat.add(new JLabel("Longueur totale des unités : " + donnee));
				this.donneesGenerales[14][0] = "Nombre de Mots";
				this.donneesGenerales[14][1] = donnee;
				listeStat.add(Box.createVerticalGlue());
				donneef = ChaineUtil.Esperance(tmp);
				listeStat.add(new JLabel("Longueur moyenne des unités : " + donneef));
				this.donneesGenerales[15][0] = "Longueur moyenne des unités";
				this.donneesGenerales[15][1] = donneef;
				listeStat.add(Box.createVerticalGlue());
				donneef = ChaineUtil.Variance(tmp);
				listeStat.add(new JLabel("Variance des longueurs des unités : " + donneef));
				this.donneesGenerales[16][0] = "Variance des longueurs des unités";
				this.donneesGenerales[16][1] = donneef;
				listeStat.add(Box.createVerticalGlue());
				tmp = ChaineUtil.longueurChaineMot(typesDispo, vue).toArray(new Integer[0]);
				donnee = ChaineUtil.somme(tmp);
				listeStat.add(new JLabel("Longueur totale des chaines de coréférences : " + donnee));
				this.donneesGenerales[17][0] = "Longueur totale des chaines de coréférences";
				this.donneesGenerales[17][1] = donnee;
				listeStat.add(Box.createVerticalGlue());
				donneef = ChaineUtil.Esperance(tmp);
				listeStat.add(new JLabel("Longueur moyenne des chaines de coréférences : " + donneef));
				this.donneesGenerales[18][0] = "Nombre de Mots";
				this.donneesGenerales[18][1] = donneef;
				listeStat.add(Box.createVerticalGlue());
				donneef = ChaineUtil.Variance(tmp);
				listeStat.add(new JLabel("Variance des longueurs des chaines de coréférences : " + donneef));
				this.donneesGenerales[19][0] = "Variance des longueurs des chaines de coréférences";
				this.donneesGenerales[19][1] = donneef;
				
				
				break;
			case "Caractère":
				donneef = ChaineUtil.densiteReferentielle("Caractère", typesDispo, vue);
				listeStat.add(new JLabel("Densité référentielle par nombre de caractères : " + donneef));
				this.donneesGenerales[9][0] = "Densité référentielle par nombre de caractères";
				this.donneesGenerales[9][1] = donneef;
				listeStat.add(Box.createVerticalGlue());
				tmp = ChaineUtil.positionUnite(typesDispo, vue).toArray(new Integer[0]);
				donneef = ChaineUtil.Esperance(tmp);
				listeStat.add(new JLabel("Position moyenne des unités : " + donneef));
				this.donneesGenerales[10][0] = "Position moyenne des unités";
				this.donneesGenerales[10][1] = donneef;
				listeStat.add(Box.createVerticalGlue());
				donneef = ChaineUtil.Variance(tmp);
				listeStat.add(new JLabel("Variance des positions des unités : " + donneef));
				this.donneesGenerales[11][0] = "Variance des positions des unités";
				this.donneesGenerales[11][1] = donneef;
				listeStat.add(Box.createVerticalGlue());
				tmp = ChaineUtil.ecartUniteCaractere(typesDispo, vue).toArray(new Integer[0]);
				donneef = ChaineUtil.Esperance(tmp);
				listeStat.add(new JLabel("Ecart moyen entre les unités : " + donneef));
				this.donneesGenerales[12][0] = "Ecart moyen entre les unités";
				this.donneesGenerales[12][1] = donneef;
				listeStat.add(Box.createVerticalGlue());
				donneef = ChaineUtil.Variance(tmp);
				listeStat.add(new JLabel("Variance des écarts des unités : " + donneef));
				this.donneesGenerales[13][0] = "Variance des écarts des unités";
				this.donneesGenerales[13][1] = donneef;
				listeStat.add(Box.createVerticalGlue());
				tmp = ChaineUtil.longueurUniteCaractere(typesDispo, vue).toArray(new Integer[0]);
				donnee = ChaineUtil.somme(tmp);
				listeStat.add(new JLabel("Longueur totale des unités : " + donnee));
				this.donneesGenerales[14][0] = "Longueur totale des unités";
				this.donneesGenerales[14][1] = donnee;
				listeStat.add(Box.createVerticalGlue());
				donneef = ChaineUtil.Esperance(tmp);
				listeStat.add(new JLabel("Longueur moyenne des unités : " + donneef));
				this.donneesGenerales[15][0] = "Longueur moyenne des unités";
				this.donneesGenerales[15][1] = donneef;
				listeStat.add(Box.createVerticalGlue());
				donneef = ChaineUtil.Variance(tmp);
				listeStat.add(new JLabel("Variance des longueurs des unités : " + donneef));
				this.donneesGenerales[16][0] = "Variance des longueurs des unités";
				this.donneesGenerales[16][1] = donneef;
				listeStat.add(Box.createVerticalGlue());
				tmp = ChaineUtil.longueurChaineCaractere(typesDispo, vue).toArray(new Integer[0]);
				donnee = ChaineUtil.somme(tmp);
				listeStat.add(new JLabel("Longueur totale des chaines de coréférences : " + donnee));
				this.donneesGenerales[17][0] = "Longueur totale des chaines de coréférences";
				this.donneesGenerales[17][1] = donnee;
				listeStat.add(Box.createVerticalGlue());
				donneef = ChaineUtil.Esperance(tmp);
				listeStat.add(new JLabel("Longueur moyenne des chaines de coréférences : " + donneef));
				this.donneesGenerales[18][0] = "Longueur moyenne des chaines de coréférences";
				this.donneesGenerales[18][1] = donneef;
				listeStat.add(Box.createVerticalGlue());
				donneef = ChaineUtil.Variance(tmp);
				listeStat.add(new JLabel("Variance des longueurs des chaines de coréférences : " + donneef));
				this.donneesGenerales[19][0] = "Variance des longueurs des chaines de coréférences";
				this.donneesGenerales[19][1] = donneef;
				break;
			default:
				break;
		}
	}
	
	private void initChampOrdonnee() {
		if (!saisieAxeAbscisse.pasdeSelection()) {
			switch (saisieAxeAbscisse.getSelection()) {
				case "Nombre de chainons":
					StatPossiblesOrdonnees = new String[] { "Nombre de Chaines de coreference" };
					break;
				case "Numéro de paragraphe":
					StatPossiblesOrdonnees = new String[] { "Nombre de chainons", "Nombre de Chaines de coreference",
							"ratio nombre de chainons par nombre de " + saisieUniteComptage.getSelection(), "Densité référentielle" };
					break;
				default:
					StatPossiblesOrdonnees = new String[] {};
					break;
			}
		}
		else {
			StatPossiblesOrdonnees = new String[] {};
		}
		saisieAxeOrdonnee.setModele(StatPossiblesOrdonnees);
	}
	
	private void affichercourbe() {
		switch (saisieAxeAbscisse.getSelection()) {
			case "Nombre de chainons":
				switch (saisieAxeOrdonnee.getSelection()) {
					case "Nombre de Chaines de coreference":
						HashMap donnee = ChaineUtil.calculerdonneecourbechainons(getTypesAutorises(), vue);
						afficherNbChainesEnFonctionNbChainons(donnee);
						break;
				}
				;
				break;
			case "Numéro de paragraphe":
				HashMap donnee = ChaineUtil.calculedonneecourbe(getTypesAutorises(), saisieUniteComptage.getSelection(), vue);
				switch (saisieAxeOrdonnee.getSelection()) {
					case "Nombre de Chaines de coreference":
						afficherEnFonctionParagraphe(donnee);
						break;
					case "Nombre de chainons":
						afficherEnFonctionParagraphe(donnee);
						break;
					case "ratio nombre de chainons par nombre de " + "Mots":
						afficherEnFonctionParagraphe(donnee);
						break;
					case "ratio nombre de chainons par nombre de " + "Caractère":
						afficherEnFonctionParagraphe(donnee);
						break;
					case "Densité référentielle":
						afficherEnFonctionParagraphe(donnee);
						break;
				}
				;
				break;
			default:
				break;
		}
	}
	
	
	private void afficherEnFonctionParagraphe(HashMap<Integer, HashMap<String, Integer>> donnees) {
		ArrayList<Integer> classement = new ArrayList<>();
		ArrayList<Integer> ordonnee = new ArrayList<>();
		for (Integer nopa : donnees.keySet()) {
			classement.add(nopa, nopa);
			ordonnee.add(nopa, donnees.get(nopa).get(saisieAxeOrdonnee.getSelection()));
			
		}
		if (!saisieModeAffichage.pasdeSelection() && saisieModeAffichage.getSelection() == "Désactiver l'affichage des valeurs nulles") {
			for (int nopa = 0; nopa < ordonnee.size(); nopa++) {
				if (nopa != 0 && nopa < ordonnee.size() - 1 && ordonnee.get(nopa) == 0) {
					classement.remove(nopa);
					ordonnee.remove(nopa);
					nopa = nopa - 1;
				}
			}
			
		}
		
		if (!saisieModeAffichage.pasdeSelection() && saisieModeAffichage.getSelection() == "Activer l'interpolation") {
			ArrayList<Integer> valeurlagrange = new ArrayList<>();
			for (int i = 0; i < classement.size(); i++) {
				valeurlagrange.add(i, ChaineUtil.interpolationLagrangienne(classement.toArray(new Integer[0]),
						ordonnee.toArray(new Integer[0]), i));
			}
			ordonnee = valeurlagrange;
		}
		Courbe.updatePanel(graphique, "Représentation de " + saisieAxeOrdonnee.getSelection() + " en fonction du numéro pararaphe",
				classement.toArray(new Integer[0]), ordonnee.toArray(new Integer[0]),
				Color.blue, -1, "numéro pararaphe", saisieAxeOrdonnee.getSelection());
	}
	
	
	
	private void afficherNbChainesEnFonctionNbChainons(HashMap<Integer, Integer> donnees) {
		int max = 0;// on cherche l'abscisse maximum
		for (Integer elt : donnees.keySet()) {
			if (elt > max) {
				max = elt;
			}
		}
		ArrayList<Integer> abscisse;
		ArrayList<Integer> ordonnee;
		
		if (saisieModeAffichage.pasdeSelection() || saisieModeAffichage.getSelection() != "Activer l'interpolation") {
			abscisse = new ArrayList<>();
			ordonnee = new ArrayList<>();
			for (int i = 0; i < max + 1; i++) {
				abscisse.add(i, i);
				if (donnees.containsKey(i)) {
					ordonnee.add(i, donnees.get(i));
				}
				else {
					ordonnee.add(i, 0);
				}
			}
			if (!saisieModeAffichage.pasdeSelection() && saisieModeAffichage.getSelection().equals("Désactiver l'affichage des valeurs nulles")) {
				for (int nopa = 0; nopa < ordonnee.size(); nopa++) {
					if (nopa != 0 && nopa < ordonnee.size() - 1 && ordonnee.get(nopa) == 0) {
						abscisse.remove(nopa);
						ordonnee.remove(nopa);
						nopa = nopa - 1;
					}
				}
				
			}
		}
		else {
			Integer[] abscisses = new Integer[donnees.size()];
			Integer[] ordonnees = new Integer[donnees.size()];
			int compteur = 0;
			for (int i = 0; i < max + 1; i++) {
				
				if (donnees.containsKey(i)) {
					abscisses[compteur] = i;
					ordonnees[compteur] = donnees.get(i);
					compteur = compteur + 1;
				}
			}
			abscisse = new ArrayList<>();
			ordonnee = new ArrayList<>();
			for (int i = 0; i < max + 1; i++) {
				abscisse.add(i, i);
				ordonnee.add(i, ChaineUtil.interpolationLagrangienne(abscisses, ordonnees, i));
			}
		}
		Courbe.updatePanel(graphique, "Représentation de " + "nombre de chaines" + " en fonction du nombre de chainons",
				abscisse.toArray(new Integer[0]), ordonnee.toArray(new Integer[0]),
				Color.blue, -1, "numéro pararaphe", saisieAxeOrdonnee.getSelection());
	}
	
	private void importerAffichage() {
		HashMap<String, Object> Choix = FichiersJava.ouvrirChaineVue();
		if (Choix != null && Choix.containsKey("choixCommande3") &&
				Fichiers.getNomFichierCourant(Fichiers.TypeFichier.DOC_ANALEC).equals(Choix.get("nomFichier"))) {
			HashMap<String, String> choixCommande = (HashMap<String, String>) Choix.get("choixCommande3");
			saisieUniteComptage.removeActionListener(this);
			saisieUniteComptage.setSelectedItem(choixCommande.get("saisieUniteComptage"));
			saisieUniteComptage.addActionListener(this);
			if (vue.getTypesUnitesAVoir() == null || saisieUniteComptage.pasdeSelection()) {
				initVide();
			}
			else {
				init();
			}
			this.typesChainesMasques = (HashSet<String>) Choix.get("typesChainesMasques");
			saisieAxeOrdonnee.removeActionListener(this);
			saisieAxeAbscisse.removeActionListener(this);
			saisieModeAffichage.removeActionListener(this);
			
			
			saisieAxeAbscisse.setSelectedItem(choixCommande.get("saisieAxeAbscisse"));
			initChampOrdonnee();
			saisieAxeOrdonnee.setSelectedItem(choixCommande.get("saisieAxeOrdonnee"));
			saisieModeAffichage.setSelectedItem(choixCommande.get("saisieModeAffichage"));
			saisieAxeOrdonnee.addActionListener(this);
			saisieAxeAbscisse.addActionListener(this);
			saisieModeAffichage.addActionListener(this);
			affichercourbe();
		}
		else {
			JOptionPane.showMessageDialog(null, "le fichier des paramètres d'affichage n'est pas destiné à cette fenêtre"
					+ System.getProperty("line.separator") +
					"ou à ce corpus", "Erreur de lecture de fichier", JOptionPane.ERROR_MESSAGE);
		}
		
		
	}
	
	private void exporterAffichage() {
		
		HashMap<String, Object> Choix = new HashMap();
		HashMap<String, String> choixCommande = new HashMap();
		
		choixCommande.put("saisieUniteComptage", saisieUniteComptage.pasdeSelection() ? null : saisieUniteComptage.getSelection());
		choixCommande.put("saisieAxeOrdonnee", saisieAxeOrdonnee.pasdeSelection() ? null : saisieAxeOrdonnee.getSelection());
		choixCommande.put("saisieAxeAbscisse", saisieAxeAbscisse.pasdeSelection() ? null : saisieAxeAbscisse.getSelection());
		choixCommande.put("saisieModeAffichage", saisieModeAffichage.pasdeSelection() ? null : saisieModeAffichage.getSelection());
		Choix.put("typesChainesMasques", typesChainesMasques);
		Choix.put("choixCommande3", choixCommande);
		Choix.put("nomFichier", Fichiers.getNomFichierCourant(Fichiers.TypeFichier.DOC_ANALEC));
		FichiersJava.enregistrerChaineSous(Choix);
	}
	
	/* renvoie un tableau contenant les statistiques générales du texte */
	public Object[][] donneesCSV() {
		return this.donneesGenerales;
	}
	
}
