/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package visuAnalec.chaines;
import java.awt.*;
import javax.swing.JPopupMenu;
import org.jfree.chart.*;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.statistics.*;
import org.jfree.data.xy.XYDataset;
import visuAnalec.util.GMessages.GAction;

import visuAnalec.vue.Vue;
/**
 *
 * @author Marc
 * inspiré par la page 
 * http://sebastien-estienne.developpez.com/tutoriels/java/exercices-graphisme-java2d/?page=exo1#LI-3
 */
public class Histogramme{
    

 



public static ChartPanel histogrammeFactory(){
    double[] value = new double[1];
    value[0]=1.0f;
    int number = 1;
    HistogramDataset dataset = new HistogramDataset();
    dataset.setType(HistogramType.RELATIVE_FREQUENCY);
    dataset.addSeries("Histogram",value,number);
    String plotTitle = "Histogram"; 
    String xaxis = "";
    String yaxis = ""; 
    PlotOrientation orientation = PlotOrientation.VERTICAL; 
    boolean show = false; 
    boolean toolTips = false;
    boolean urls = false; 
    JFreeChart chart = ChartFactory.createHistogram( plotTitle, xaxis, yaxis, 
    dataset, orientation, show, toolTips, urls);
    final ChartPanel chartPanel = new ChartPanel(chart);
    chartPanel.setFillZoomRectangle(true);
    chartPanel.setMouseWheelEnabled(true);
    chartPanel.setPreferredSize(new Dimension(500,270));
    chartPanel.setVisible(true);
    JPopupMenu save = (JPopupMenu) chartPanel.getPopupMenu().getSubElements()[2].getSubElements()[0];
        save.add(new GAction("Exporter en PDF") {
        @Override
        public void executer() {
        ExportCSV.writeChartToPDF(chartPanel,500,400);
        }
      });
      save.add(new GAction("Exporter en CSV") {
        @Override
        public void executer() {
        XYDataset dataSet = chartPanel.getChart().getXYPlot().getDataset();
        if(dataSet.getSeriesCount()>0 && dataSet.getItemCount(0)>0){
        Object[][] Donnees = new Object[dataSet.getItemCount(0)][dataSet.getSeriesCount()*2];
        for(int i = 0 ; i < dataSet.getItemCount(0) ; i++){
            for(int j = 0 ; j < dataSet.getSeriesCount() ; j++){
                Donnees[i][2*j]=dataSet.getXValue(j, i);
                Donnees[i][2*j+1]=dataSet.getYValue(j, i);
            }
        }
        ExportCSV.exporterCSV(Donnees);
        }
        }
      });
    return chartPanel;
}



public static void updatePanel(ChartPanel window,String titre,ModeleChaines data,int nb_classes,Color couleur, int maxheight, 
           String abscisse,String ordonnee,Vue vue){
    
        double[] value = new double[data.getUnites().length];
    for (int i=1; i < data.getUnites().length; i++) {
            value[i] = (double) data.getUnites()[i].getDeb();
    }
    HistogramDataset dataset = new HistogramDataset();
    dataset.setType(HistogramType.RELATIVE_FREQUENCY);
    dataset.addSeries("Histogram",value,nb_classes,0.0f,(double) vue.getCorpus().getTexte().length());
    String plotTitle = "Distribution spatiale des unités"; 
    String xaxis = abscisse;
    String yaxis = ordonnee; 
    PlotOrientation orientation = PlotOrientation.VERTICAL; 
    boolean show = false; 
    boolean toolTips = false;
    boolean urls = false; 
    JFreeChart chart = ChartFactory.createHistogram( plotTitle, xaxis, yaxis, 
    dataset, orientation, show, toolTips, urls);
    window.setChart(chart);
    window.revalidate();
    window.repaint();
    window.setFillZoomRectangle(true);
    window.setMouseWheelEnabled(true);
    window.setPreferredSize(new Dimension(500,270));
    window.setVisible(true);
    
}
 


   
 

} 


