/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package visuAnalec.chaines;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import visuAnalec.*;
import visuAnalec.elements.*;
import visuAnalec.util.*;
import visuAnalec.util.GVisu.*;
import visuAnalec.vue.*;
import visuAnalec.vue.Vue.*;

/**
 *
 * @author marc
 */
public class VisuConversion extends JFrame implements VueListener, ActionListener {
	
	private Box PanneauCommande = Box.createVerticalBox();
	
	private Box PanneauCommandeSource = Box.createVerticalBox();
	
	private Vue vue;
	
	private PanneauEditeur panneauEditeur;
	
	private JList choixModeConversion = new JList();
	
	private JButton lancerConversion = new JButton("Convertir");
	
	private JTextField nomSchema = new JTextField("Chaine de coréférence");
	
	private Val0ComboBox typeSource = new Val0ComboBox("Rien");
	
	private Val0ComboBox primaryKey = new Val0ComboBox("Rien");
	
	private String[] otherKey = new String[0];
	
	private Val0ComboBox addOtherKey = new Val0ComboBox("Rien");
	
	private Val0ComboBox removeOtherKey = new Val0ComboBox("Rien");
	
	private String[] sourcesPossibles = new String[] { "Type de Relation", "Type d'unité", "Propriété d'unité" };
	
	private String[] typesSourceDisponibles = new String[0];
	
	private ArrayList<String> primaryKeyDisponibles = new ArrayList<>();
	
	private ArrayList<String> otherKeyDisponibles = new ArrayList<>();
	
	private ArrayList<String> otherKeyAjoutees = new ArrayList<>();
	
	public VisuConversion(Container fenetre, Vue vue, PanneauEditeur pEditeur) {
		super("Conversion de données en schéma");
		Point pos = new Point(fenetre.getX() + 20, fenetre.getY() + 20);
		Dimension dim = new Dimension(700, 500);
		setLocation(pos);
		setSize(dim);
		this.vue = vue;
		vue.addEventListener(this);
		this.panneauEditeur = pEditeur;
		choixModeConversion.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mousePressed(MouseEvent e) {// on gère les évènements de la souris
				check(e);
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// check(e);
			}
			
			public void check(MouseEvent e) {
				if (!e.isPopupTrigger()) {// si on a un clique gauche sur la liste
					listAction(choixModeConversion.getSelectedIndex());
				}
			}
		});
		lancerConversion.addActionListener(this);
		PanneauCommandeSource.add(Box.createVerticalStrut(20));
		PanneauCommandeSource.add(new JLabel("Choix de la source de donnée :"));
		PanneauCommandeSource.add(Box.createVerticalStrut(20));
		PanneauCommandeSource.add(choixModeConversion);
		PanneauCommandeSource.add(Box.createVerticalStrut(20));
		PanneauCommandeSource.add(lancerConversion);
		JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, PanneauCommandeSource, PanneauCommande);
		getContentPane().add(split);
		split.setDividerLocation(1. / 3);
		setVisible(true);
		typeSource.setAlignmentX(0);
		primaryKey.setAlignmentX(0);
		addOtherKey.setAlignmentX(0);
		removeOtherKey.setAlignmentX(0);
		choixModeConversion.setAlignmentX(0);
		// Fermeture de la fenêtre
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent evt) {
				fermerFenetre();
			}
		});
		initVide();
	}
	
	public void initVide() {
		PanneauCommande.removeAll();
		choixModeConversion.setListData(sourcesPossibles);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		try {
			if (choixModeConversion.isSelectionEmpty()) {
				initVide();
				return;
			}
			else {
				switch (choixModeConversion.getSelectedIndex()) {
					case 0:
						if (evt.getSource().equals(typeSource)) {
							primaryKeyDisponibles = vue.getListeChamps(Relation.class, typeSource.getSelection());
							primaryKey.removeActionListener(this);
							primaryKey.setModele(primaryKeyDisponibles.toArray(new String[0]));
							primaryKey.addActionListener(this);
							return;
						}
						if (evt.getSource().equals(primaryKey)) {
							return;
						}
						if (evt.getSource().equals(lancerConversion)) {
							if (!typeSource.pasdeSelection() && !primaryKey.pasdeSelection()) {
								ChaineUtil.conversion("Relation", nomSchema.getText(), typeSource.getSelection(),
										primaryKey.getSelection(), null, vue);
								fermerFenetre();
							}
							return;
						}
						break;
					case 1:
						if (evt.getSource().equals(lancerConversion)) {
							ChaineUtil.conversion("typeUnite", nomSchema.getText(), null, null, new String[0], vue);
							fermerFenetre();
							return;
						}
						break;
					case 2:
						if (evt.getSource().equals(typeSource)) {
							if (!typeSource.pasdeSelection()) {
								primaryKeyDisponibles = vue.getListeChamps(Unite.class, typeSource.getSelection());
								primaryKey.removeActionListener(this);
								primaryKey.setModele(primaryKeyDisponibles.toArray(new String[0]));
								primaryKey.addActionListener(this);
								otherKeyDisponibles = vue.getListeChamps(Unite.class, typeSource.getSelection());
								addOtherKey.removeActionListener(this);
								addOtherKey.setModele(otherKeyDisponibles.toArray(new String[0]));
								addOtherKey.addActionListener(this);
								otherKeyAjoutees = new ArrayList<>();
								removeOtherKey.removeActionListener(this);
								removeOtherKey.setModele(otherKeyAjoutees.toArray(new String[0]));
								removeOtherKey.addActionListener(this);
							}
							return;
						}
						if (evt.getSource().equals(primaryKey)) {
							if (!typeSource.pasdeSelection() && !primaryKey.pasdeSelection()) {
								otherKeyDisponibles = vue.getListeChamps(Unite.class, typeSource.getSelection());
								otherKeyDisponibles.remove(primaryKey.getSelection());
								for (String key : otherKeyAjoutees) {
									otherKeyDisponibles.remove(key);
								}
								otherKeyAjoutees.remove(primaryKey.getSelection());
								addOtherKey.removeActionListener(this);
								removeOtherKey.removeActionListener(this);
								addOtherKey.setModele(otherKeyDisponibles.toArray(new String[0]));
								removeOtherKey.setModele(otherKeyAjoutees.toArray(new String[0]));
								addOtherKey.addActionListener(this);
								removeOtherKey.addActionListener(this);
							}
							return;
						}
						if (evt.getSource().equals(lancerConversion)) {
							if (!typeSource.pasdeSelection() && !primaryKey.pasdeSelection()) {
								ChaineUtil.conversion("attributUnite", nomSchema.getText(), typeSource.getSelection(),
										primaryKey.getSelection(), otherKeyAjoutees.toArray(new String[0]), vue);
								fermerFenetre();
							}
							return;
						}
						if (evt.getSource().equals(addOtherKey)) {
							if (!typeSource.pasdeSelection() && !addOtherKey.pasdeSelection()) {
								otherKeyDisponibles.remove(addOtherKey.getSelection());
								otherKeyAjoutees.add(addOtherKey.getSelection());
								addOtherKey.removeActionListener(this);
								removeOtherKey.removeActionListener(this);
								addOtherKey.setModele(otherKeyDisponibles.toArray(new String[0]));
								removeOtherKey.setModele(otherKeyAjoutees.toArray(new String[0]));
								addOtherKey.addActionListener(this);
								removeOtherKey.addActionListener(this);
							}
							return;
						}
						if (evt.getSource().equals(removeOtherKey)) {
							if (!typeSource.pasdeSelection() && !removeOtherKey.pasdeSelection()) {
								otherKeyDisponibles.add(removeOtherKey.getSelection());
								otherKeyAjoutees.remove(removeOtherKey.getSelection());
								addOtherKey.removeActionListener(this);
								removeOtherKey.removeActionListener(this);
								addOtherKey.setModele(otherKeyDisponibles.toArray(new String[0]));
								removeOtherKey.setModele(otherKeyAjoutees.toArray(new String[0]));
								addOtherKey.addActionListener(this);
								removeOtherKey.addActionListener(this);
							}
							return;
						}
						break;
					default:
						initVide();
						break;
					
				}
				
			}
			throw new UnsupportedOperationException("Evénement non prévu : " + evt.paramString());
		}
		catch (Throwable ex) {
			GMessages.erreurFatale(ex);
		}
	}
	
	private void fermerFenetre() {
		vue.removeEventListener(this);
		dispose();
	}
	
	@Override
	public void traiterEvent(Message evt) { // très grossier : à améliorer
		switch (evt.getType()) {
			case CLEAR_CORPUS:
				fermerFenetre();
				return;
			case NEW_CORPUS:  // impossible en principe : CLEAR_CORPUS a détruit la fenêtre
				throw new UnsupportedOperationException("Fenêtre chaines présente lors d'un message NEW_CORPUS ???");
			case MODIF_TEXTE:
				fermerFenetre();
				return;
			case MODIF_VUE:
				fermerFenetre();
				return;
			case MODIF_STRUCTURE:
				fermerFenetre();
				return;
			case MODIF_ELEMENT:
				// ElementEvent evtE = (ElementEvent) evt;
				// if (evtE.getModif() != TypeModifElement.BORNES_UNITE) fermerFenetre();
				fermerFenetre();
				return;
			case CORPUS_SAVED:
				fermerFenetre();
				return;
			default:
				throw new UnsupportedOperationException("Cas " + evt.getType() + " oublié dans un switch");
		}
	}
	
	public void listAction(int Index) {
		if (!choixModeConversion.isSelectionEmpty()) {
			PanneauCommande.removeAll();
			PanneauCommande.add(Box.createVerticalStrut(20));
			PanneauCommande.add(new JLabel("Saisisez un nom pour votre nouveau type de schéma :"));
			nomSchema.setMaximumSize(new Dimension(500, 50));
			PanneauCommande.add(Box.createVerticalStrut(20));
			PanneauCommande.add(nomSchema);
			switch (Index) {
				case 0:
					PanneauCommande.add(Box.createVerticalStrut(20));
					PanneauCommande.add(new JLabel("Sélectionner un type de relation :"));
					PanneauCommande.add(Box.createVerticalStrut(20));
					PanneauCommande.add(typeSource);
					PanneauCommande.add(Box.createVerticalStrut(20));
					PanneauCommande.add(new JLabel("Sélectionner une propriété caractérisant chaque chaine :"));
					PanneauCommande.add(Box.createVerticalStrut(20));
					PanneauCommande.add(primaryKey);
					this.typesSourceDisponibles = vue.getTypesRelationsAVoir();
					typeSource.removeActionListener(this);
					typeSource.setModele(typesSourceDisponibles);
					typeSource.addActionListener(this);
					this.primaryKeyDisponibles = new ArrayList<>();
					primaryKey.removeActionListener(this);
					this.primaryKey.setModele(this.primaryKeyDisponibles.toArray(new String[0]));
					primaryKey.addActionListener(this);
					break;
				case 1:
					break;
				case 2:
					PanneauCommande.add(Box.createVerticalStrut(20));
					PanneauCommande.add(new JLabel("Sélectionner un type d'unité :"));
					PanneauCommande.add(Box.createVerticalStrut(20));
					PanneauCommande.add(typeSource);
					PanneauCommande.add(Box.createVerticalStrut(20));
					PanneauCommande.add(new JLabel("Sélectionner une propriété caractérisant chaque chaine :"));
					PanneauCommande.add(Box.createVerticalStrut(20));
					PanneauCommande.add(primaryKey);
					PanneauCommande.add(Box.createVerticalStrut(20));
					PanneauCommande.add(new JLabel("Sélectionner une propriété des unités à ajouter aux schémas :"));
					PanneauCommande.add(Box.createVerticalStrut(20));
					PanneauCommande.add(addOtherKey);
					PanneauCommande.add(Box.createVerticalStrut(20));
					PanneauCommande.add(new JLabel("Sélectionner une propriété des unités à enlever aux schemas :"));
					PanneauCommande.add(Box.createVerticalStrut(20));
					PanneauCommande.add(removeOtherKey);
					this.typesSourceDisponibles = vue.getTypesUnitesAVoir();
					typeSource.removeActionListener(this);
					typeSource.setModele(typesSourceDisponibles);
					typeSource.addActionListener(this);
					primaryKey.removeActionListener(this);
					this.primaryKeyDisponibles = new ArrayList<>();
					this.primaryKey.setModele(this.primaryKeyDisponibles.toArray(new String[0]));
					primaryKey.addActionListener(this);
					this.otherKeyDisponibles = new ArrayList<>();
					addOtherKey.removeActionListener(this);
					this.addOtherKey.setModele(this.otherKeyDisponibles.toArray(new String[0]));
					addOtherKey.addActionListener(this);
					this.otherKeyAjoutees = new ArrayList<>();
					removeOtherKey.removeActionListener(this);
					this.removeOtherKey.setModele(this.otherKeyAjoutees.toArray(new String[0]));
					removeOtherKey.addActionListener(this);
					break;
				default:
					PanneauCommande.removeAll();
					break;
			}
			revalidate();
			repaint();
		}
	}
	
	
}
