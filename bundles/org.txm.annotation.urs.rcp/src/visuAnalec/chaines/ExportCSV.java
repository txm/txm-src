/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package visuAnalec.chaines;

import com.itextpdf.awt.DefaultFontMapper;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileOutputStream;
import javax.swing.*;
import org.jfree.chart.ChartPanel;
import visuAnalec.*;
import visuAnalec.fichiers.FichiersTexte;
import visuAnalec.util.*;
import visuAnalec.util.GMessages.GAction;
import visuAnalec.vue.*;

/**
 *
 * @author Marc
 */
public class ExportCSV extends JFrame implements Vue.VueListener, ActionListener{
    private Vue vue;
    private PanneauEditeur panneauEditeur;
    private JCheckBox activeChamp1;
    private JCheckBox activeChamp2;
    private JCheckBox activeChamp3;
    private JCheckBox activeChamp4;
    private JCheckBox activeChamp5;
    private JCheckBox activeChampChaine;
    private JCheckBox activeChampUnite;
    private JCheckBox activeChampParagraphe;
    private JButton exporter;
    
    
    public ExportCSV(Container fenetre, Vue vue, PanneauEditeur pEditeur, VisuCoref visucoref){
        super("Analyse générale du texte");
        Point pos = new Point(fenetre.getX()+20, fenetre.getY()+20);
        Dimension dim = new Dimension(300, 500);
        setLocation(pos);
        setSize(dim);
        this.panneauEditeur = pEditeur;
        final VisuCoref visucorefs = visucoref;
        final VisuChaines visuchaine = null;
        this.vue = vue;
        vue.addEventListener(this);
        Box commandes = Box.createVerticalBox();
        commandes.add(this.activeChampChaine = new JCheckBox("Afficher le Champ de la Chaine"));
        commandes.add(Box.createVerticalStrut(20));
        commandes.add(this.activeChampUnite = new JCheckBox("Afficher le Champ ds Unités"));
        commandes.add(Box.createVerticalStrut(20));
        commandes.add(this.activeChampParagraphe = new JCheckBox("Afficher le numéro des paragraphes"));
        commandes.add(Box.createVerticalStrut(20));
        commandes.add(this.exporter=new JButton("Exporter"));
        exporter.setAction(new GAction("exporter") {
            public void executer() {
              exporterCSV(visucorefs.donneesCSV(new boolean[]{activeChampUnite.isSelected(),activeChampChaine.isSelected(),
              activeChampParagraphe.isSelected()}));
              fermerFenetre();
            }
          });
        exporter.setSize(100, 40);
        exporter.setVisible(true);
        getContentPane().add(commandes);
        setVisible(true);
        // Fermeture de la fenêtre
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent evt) {
         fermerFenetre();
        }
        });
    }
    
    public ExportCSV(Container fenetre, Vue vue, PanneauEditeur pEditeur, VisuChaines visuchaine){
        super("Analyse générale du texte");
        Point pos = new Point(fenetre.getX()+20, fenetre.getY()+20);
        Dimension dim = new Dimension(300, 500);
        setLocation(pos);
        setSize(dim);
        this.panneauEditeur = pEditeur;
        final VisuCoref visucoref = null;
        final VisuChaines visuchaines = visuchaine;
        this.vue = vue;
        vue.addEventListener(this);
        Box commandes = Box.createVerticalBox();
        commandes.add(this.activeChamp1 = new JCheckBox("Afficher le Champ1"));
        commandes.add(Box.createVerticalStrut(20));
        commandes.add(this.activeChamp2 = new JCheckBox("Afficher le Champ2"));
        commandes.add(Box.createVerticalStrut(20));
        commandes.add(this.activeChamp3 = new JCheckBox("Afficher le Champ3"));
        commandes.add(Box.createVerticalStrut(20));
        commandes.add(this.activeChamp4 = new JCheckBox("Afficher le Champ4"));
        commandes.add(Box.createVerticalStrut(20));
        commandes.add(this.activeChamp5 = new JCheckBox("Afficher le Champ5"));
        commandes.add(Box.createVerticalStrut(20));
        commandes.add(this.exporter=new JButton("Exporter"));
        exporter.setAction(new GAction("exporter") {
            public void executer() {
              exporterCSV(visuchaines.donneesCSV(new boolean[]{activeChamp1.isSelected(),activeChamp2.isSelected(),
              activeChamp3.isSelected(),activeChamp4.isSelected(),activeChamp5.isSelected()}));
              fermerFenetre();
            }
          });
        exporter.setSize(100, 40);
        exporter.setVisible(true);
        getContentPane().add(commandes);
        setVisible(true);
        // Fermeture de la fenêtre
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent evt) {
         fermerFenetre();
        }
        });
    }    
    
       private void fermerFenetre() {
        vue.removeEventListener(this);
        dispose();
        }
    
    public void traiterEvent(Message evt) { // très grossier : à améliorer
    switch (evt.getType()) {
      case CLEAR_CORPUS:
        fermerFenetre();
        return;
      case NEW_CORPUS:  // impossible en principe : CLEAR_CORPUS a détruit la fenêtre
        throw new UnsupportedOperationException("Fenêtre chaines présente lors d'un message NEW_CORPUS ???");
      case MODIF_TEXTE:
        fermerFenetre();
        return;
      case MODIF_VUE:
        fermerFenetre();
        return;
      case MODIF_STRUCTURE:
        fermerFenetre();
        return;
      case MODIF_ELEMENT:
//        ElementEvent evtE = (ElementEvent) evt;
//        if (evtE.getModif() != TypeModifElement.BORNES_UNITE) fermerFenetre();
        fermerFenetre();
        return;
      case CORPUS_SAVED:
          fermerFenetre();
          return;
      default:
        throw new UnsupportedOperationException("Cas "+evt.getType()+" oublié dans un switch");
    }
  }
    
  public void actionPerformed(ActionEvent evt) {
    try {
      throw new UnsupportedOperationException("Evénement non prévu : "+evt.paramString());
    } catch (Throwable ex) {
      GMessages.erreurFatale(ex);
    }
  }
  
  public static void exporterCSV(Object[][] donnees){
      JOptionPane jop = new JOptionPane();
      String delimiteur = jop.showInputDialog(null, "Veuillez saisir un délimiteur d'un caractère maximum"
              , "Saisie délimiteur pour export en UTF-8", JOptionPane.QUESTION_MESSAGE);
      if(delimiteur!=null && !delimiteur.isEmpty() && delimiteur.length()==1){
          String texte = "";
          for(Object[] ligne : donnees){
              for(Object cellule : ligne){
                  texte = texte + (cellule==null ? "" : cellule.toString().replaceAll(delimiteur, "")) + delimiteur;
              }
              texte = texte + System.getProperty("line.separator");
          }
          FichiersTexte.exporterCSV(texte);
      }
  }
  /*ajouté probablement dans la mauvaise classe, la package fichier semble plus adapté pour recevoir les classes*/
  /*ici on laisse l'utilisateur choisir un dossier où enregistrer*/
  public static void writeChartToPDF(ChartPanel chart,int width,int height) {
    String fileName="";
    
    File fichierGraphique;
    if ((fichierGraphique = Fichiers.choisirFichier(Fichiers.Commande.ENREGISTRER,
            "Nom du fichier ?", Fichiers.TypeFichier.PDF)) == null) return;
    
    fileName = fichierGraphique.getAbsolutePath();
            
    
    PdfWriter writer = null;
 
    Document document = new Document();
 
    try {
        writer = PdfWriter.getInstance(document,  new FileOutputStream(
                fileName));
        document.open();
        PdfContentByte contentByte = writer.getDirectContent();
        PdfTemplate template = contentByte.createTemplate(width, height);
        Graphics2D graphics2d = template.createGraphics(width, height,
                new DefaultFontMapper());
        Rectangle2D rectangle2d = new Rectangle2D.Double(0, 0, width,
                height);
 
        chart.getChart().draw(graphics2d, rectangle2d);
         
        graphics2d.dispose();
        contentByte.addTemplate(template, 0, 0);
 
    } catch (Exception e) {
        e.printStackTrace();
    }
    document.close();
}
}
