/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package visuAnalec.chaines;
import java.awt.*;
import javax.swing.JPopupMenu;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import visuAnalec.util.GMessages.GAction;
/**
 *
 * @author Marc
 */
//requiert des donnees positives
public class Courbe{
    



public static ChartPanel courbeFactory(){
    
        
        final XYSeriesCollection dataset = new XYSeriesCollection();
        
        final JFreeChart chart = ChartFactory.createXYLineChart(
            "Courbe vide",      // chart title
            "X",                      // x axis label
            "Y",                      // y axis label
            dataset,                  // data
            PlotOrientation.VERTICAL,
            true,                     // include legend
            true,                     // tooltips
            false                     // urls
        );


        chart.setBackgroundPaint(Color.white);

        final XYPlot plot = chart.getXYPlot();
        plot.setBackgroundPaint(Color.lightGray);

        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        
        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(0, false);
        renderer.setSeriesShapesVisible(1, false);
        plot.setRenderer(renderer);


        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));

        
        JPopupMenu save = (JPopupMenu) chartPanel.getPopupMenu().getSubElements()[2].getSubElements()[0];
        save.add(new GAction("Exporter en PDF") {
        @Override
        public void executer() {
        ExportCSV.writeChartToPDF(chartPanel,500,400);
        }
      });
      save.add(new GAction("Exporter en CSV") {
        @Override
        public void executer() {
        XYDataset dataSet = chartPanel.getChart().getXYPlot().getDataset();
        if(dataSet.getSeriesCount()>0 && dataSet.getItemCount(0)>0){
        Object[][] Donnees = new Object[dataSet.getItemCount(0)][dataSet.getSeriesCount()*2];
        for(int i = 0 ; i < dataSet.getItemCount(0) ; i++){
            for(int j = 0 ; j < dataSet.getSeriesCount() ; j++){
                Donnees[i][2*j]=dataSet.getXValue(j, i);
                Donnees[i][2*j+1]=dataSet.getYValue(j, i);
            }
        }
        ExportCSV.exporterCSV(Donnees);
        }
        }
      });
    return chartPanel;
}




public static void updatePanel(ChartPanel window,String titre,Integer[] xData,Integer[] yData,Color couleur, int maxheight, 
           String abscisse,String ordonnee){
    
        final XYSeries series1 = new XYSeries("Point");
        final XYSeriesCollection dataset = new XYSeriesCollection();
        for(int i = 0 ; i < xData.length;i++){
            series1.add(xData[i],yData[i]);
        }
        dataset.addSeries(series1);
        
        final JFreeChart chart = ChartFactory.createXYLineChart(
            titre,      // chart title
            abscisse,                      // x axis label
            ordonnee,                      // y axis label
            dataset,                  // data
            PlotOrientation.VERTICAL,
            true,                     // include legend
            true,                     // tooltips
            false                     // urls
        );


        chart.setBackgroundPaint(Color.white);

        final XYPlot plot = chart.getXYPlot();
        plot.setBackgroundPaint(Color.lightGray);

        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        
        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        plot.setRenderer(renderer);


        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        
        window.setChart(chart);
        window.setPreferredSize(new java.awt.Dimension(500, 270));

    
}
 

 
   
 
    
}
