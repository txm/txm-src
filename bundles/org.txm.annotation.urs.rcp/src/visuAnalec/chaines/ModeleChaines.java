/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package visuAnalec.chaines;

import java.awt.*;
import java.util.*;
import java.util.ArrayList;
import visuAnalec.elements.*;
import visuAnalec.vue.*;

/**
 *
 * @author Bernard
 */
public class ModeleChaines {
	
	static class Chainon {
		
		Unite unite;
		
		String texte;
		
		Schema chaine = null;
		
		int nbEltsChaine = 0;
		
		String idChaine = "";
		
		int noChaine = 0;
		
		Chainon(Unite unite, String texte) {
			this.unite = unite;
			this.texte = texte;
		}
	}
	
	private Vue vue;
	
	private GChaines panneauGraphique;
	
	private Unite[] listeUnites;
	
	private boolean peutafficher = true;
	
	private HashMap<Unite, Schema> chainesUnitesNonFiltrees = new HashMap<>();
	
	private int[] seuilsValsNbElements = new int[0];
	
	private HashMap<Unite, Chainon> chainonsUnites = new HashMap<>();
	
	private HashMap<Schema, ArrayList<Unite>> unitesChaines = new HashMap<>();
	
	private HashMap<String, HashSet<String>> valsMasqueesChamp = new HashMap<>();
	
	private HashSet<String> valNulleMasqueeChamp = new HashSet<>();
	
	private boolean[] valsMasqueesNbElements = new boolean[0];
	
	public ModeleChaines(Vue vue, GChaines panneauGraphique) {
		this.vue = vue;
		this.panneauGraphique = panneauGraphique;
	}
	
	Schema getChaine(Unite unit) {
		return chainonsUnites.get(unit).chaine;
	}
	
	public HashMap<Unite, Chainon> getChainonUnites() {
		return chainonsUnites;
	}
	
	Unite[] getUnites() {
		return listeUnites;
	}
	
	Unite[] getUnitesChaine(Schema chaine) {
		ArrayList<Unite> units = unitesChaines.get(chaine);
		return units.toArray(new Unite[0]);
	}
	
	boolean isMasqueeValChamp(String champ, String val) {
		return valsMasqueesChamp.containsKey(champ)
				&& valsMasqueesChamp.get(champ).contains(val);
	}
	
	boolean isMasqueeValNulleChamp(String champ) {
		return valNulleMasqueeChamp.contains(champ);
	}
	
	boolean isMasqueeValNbElements(int i) {
		return i >= valsMasqueesNbElements.length ? false : valsMasqueesNbElements[i];
	}
	
	public HashMap<String, HashSet<String>> getValsMasqueesChamp() {
		return valsMasqueesChamp;
	}
	
	public HashSet<String> getValsNulleMasqueesChamp() {
		return valNulleMasqueeChamp;
	}
	
	public HashMap<Unite, Schema> getchainesUnitesNonFiltrees() {
		return chainesUnitesNonFiltrees;
	}
	
	public boolean[] getValsNbMasqueesChamp() {
		return valsMasqueesNbElements;
	}
	
	public void setValsMasqueesChamp(HashMap<String, HashSet<String>> valsMasqueesChamp) {
		this.valsMasqueesChamp = valsMasqueesChamp;
	}
	
	public void setValsNulleMasqueesChamp(HashSet<String> valNulleMasqueeChamp) {
		this.valNulleMasqueeChamp = valNulleMasqueeChamp;
	}
	
	public void setValsNbMasqueesChamp(boolean[] valsMasqueesNbElements) {
		this.valsMasqueesNbElements = valsMasqueesNbElements;
	}
	
	// retourne le nombre de valeurs masquees pour le champ
	public int nbvaleursmasquees(String champ) {
		int resultat = 0;
		if (champ == null) {// si le champ affiché est le nombre de chainons
			for (boolean val : valsMasqueesNbElements) {
				if (val) {
					resultat = resultat + 1;
				}
			}
		}
		else {
			resultat = valsMasqueesChamp.get(champ).size();
		}
		return resultat;
	}
	
	void demasquerToutesValsChamp(String champ) {
		valsMasqueesChamp.get(champ).clear();
		valNulleMasqueeChamp.remove(champ);
	}
	
	void demasquerToutesValsNbElts(String champ) {
		for (int i = 0; i < valsMasqueesNbElements.length; i++) {
			valsMasqueesNbElements[i] = false;
		}
	}
	
	void setGChaine(GChaines panneaugraphique) {
		this.panneauGraphique = panneaugraphique;
	}
	
	void setUnites(Unite[] donnees) {
		this.listeUnites = donnees;
	}
	
	void permettreaffichage(boolean autorisation) {
		this.peutafficher = autorisation;
	}
	
	boolean peutetreafficher() {
		return this.peutafficher;
	}
	
	void masquerAutresValsChamp(String typeunite, String champ, String val) {
		for (String v : vue.getValeursChamp(Unite.class, typeunite, champ))
			valsMasqueesChamp.get(champ).add(v);
		valsMasqueesChamp.get(champ).remove(val);
		valNulleMasqueeChamp.add(champ);
	}
	
	void masquerValChamp(boolean yes, String champ, String val) {
		if (yes) valsMasqueesChamp.get(champ).add(val);
		else valsMasqueesChamp.get(champ).remove(val);
	}
	
	void masquerValsNonNullesChamp(String typeunite, String champ) {
		for (String v : vue.getValeursChamp(Unite.class, typeunite, champ))
			valsMasqueesChamp.get(champ).add(v);
		valNulleMasqueeChamp.remove(champ);
	}
	
	void masquerValNulleChamp(boolean yes, String champ) {
		if (yes) valNulleMasqueeChamp.add(champ);
		else valNulleMasqueeChamp.remove(champ);
	}
	
	void masquerAutresValsNbElts(String champ, int i0) {
		for (int i = 0; i < valsMasqueesNbElements.length; i++) {
			valsMasqueesNbElements[i] = i != i0;
		}
	}
	
	void masquerValNbElements(boolean yes, int i) {
		valsMasqueesNbElements[i] = yes;
	}
	
	void init(String typeChaines, String typeUnites, int[] seuils)
			throws UnsupportedOperationException {
		seuilsValsNbElements = seuils;
		String[] champs = vue.getNomsChamps(Unite.class, typeUnites);
		valsMasqueesChamp.clear();
		for (String champ : champs)
			valsMasqueesChamp.put(champ, new HashSet<String>());
		valNulleMasqueeChamp.clear();
		valsMasqueesNbElements = new boolean[seuils.length + 3];
		calculerChainesUnitesNonFiltrees(typeChaines, typeUnites);
		calculerDonnees(typeChaines, typeUnites);
	}
	
	// struture qui n'est pas filtrées mais qui associe à une unité qui correspond au bon type d'unités
	// la chaine qui la contient et qui est elle aussi du bon type de Schéma
	private void calculerChainesUnitesNonFiltrees(String typeChaines,
			String typeUnites) throws UnsupportedOperationException {
		chainesUnitesNonFiltrees.clear();
		for (Schema chaine : vue.getSchemasAVoir(typeChaines))
			for (Element elt : chaine.getContenu()) {
				if (elt.getClass() == Unite.class && elt.getType().equals(typeUnites)) {
					if (chainesUnitesNonFiltrees.containsKey(elt)) {
						throw new UnsupportedOperationException("L'unité " + vue.getIdElement(elt)
								+ " appartient à deux chaînes : " + vue.getIdElement(chaine) + " et "
								+ vue.getIdElement(chainesUnitesNonFiltrees.get(elt)));
					}
					else
						chainesUnitesNonFiltrees.put((Unite) elt, chaine);
				}
			}
	}
	
	void modifMasqueVals(String typeChaines, String typeUnites) {
		calculerDonnees(typeChaines, typeUnites);
	}
	
	void modifMasqueValsChampChaine(String typeChaines, String typeUnites, ArrayList noparagraphecachees, String champ, String Valeur) {
		calculerDonnees(typeChaines, typeUnites, noparagraphecachees, champ, Valeur);
	}
	
	private void filtrerListeUnites(String typeUnites, String typeChaines) {
		Unite[] unites = vue.getUnitesAVoir(typeUnites);
		Schema[] chaines = vue.getSchemasAVoir(typeChaines);
		unitesChaines.clear();
		for (Schema chaine : chaines)
			unitesChaines.put(chaine, new ArrayList<Unite>());
		ArrayList<Integer> indFiltrees = new ArrayList<>();
		String[] champs = vue.getNomsChamps(Unite.class, typeUnites);
		boolean agarder;
		for (int i = 0; i < unites.length; i++) {
			agarder = true;
			for (String champ : champs) {// pour tous les champs
				String val = vue.getValeurChamp(unites[i], champ);// je recupere la valeur du champ
				if (val.isEmpty()) {
					if (valNulleMasqueeChamp.contains(champ)) {
						agarder = false;
						break;
					}
				}
				else {
					if (valsMasqueesChamp.get(champ).contains(val)) {
						agarder = false;
						break;
					}
				}
			}
			if (agarder) {
				indFiltrees.add(i);
				if (chainesUnitesNonFiltrees.containsKey(unites[i])) {
					Schema chaine = chainesUnitesNonFiltrees.get(unites[i]);
					unitesChaines.get(chaine).add(unites[i]);
				}
			}
		}
		ArrayList<Integer> indIndFiltrees = new ArrayList<>();
		for (int k = 0; k < indFiltrees.size(); k++) {
			Unite unit = unites[indFiltrees.get(k)];
			if (chainesUnitesNonFiltrees.containsKey(unit)) {
				if (!isMasqueeValNbElements(calculerValNbElements(
						unitesChaines.get(chainesUnitesNonFiltrees.get(unit)).size())))
					indIndFiltrees.add(k);
			}
			else {
				if (!isMasqueeValNbElements(0)) indIndFiltrees.add(k);
			}
		}
		listeUnites = new Unite[indIndFiltrees.size()];
		for (int k = 0; k < listeUnites.length; k++) {
			listeUnites[k] = unites[indFiltrees.get(indIndFiltrees.get(k))];
		}
		for (Schema chaine : chaines) {
			int nb = unitesChaines.get(chaine).size();
			if (nb == 0 || isMasqueeValNbElements(calculerValNbElements(nb)))
				unitesChaines.remove(chaine);
		}
	}
	
	private void calculerDonnees(String typeChaines, String typeUnites) {
		filtrerListeUnites(typeUnites, typeChaines);
		Chainon[] chainons = new Chainon[listeUnites.length];
		for (int i = 0; i < listeUnites.length; i++) {
			Unite unit = listeUnites[i];
			chainons[i] = new Chainon(unit, vue.getTexteUnite(unit));
			if (chainesUnitesNonFiltrees.containsKey(unit)) {
				Schema chaine = chainesUnitesNonFiltrees.get(unit);
				chainons[i].chaine = chaine;
				chainons[i].nbEltsChaine = unitesChaines.get(chaine).size();
				chainons[i].idChaine = vue.getIdElement(chaine);
			}
			chainonsUnites.put(listeUnites[i], chainons[i]);
		}
		HashMap<Schema, Integer> noChaine = new HashMap<>();
		int no = 0;
		for (Schema chaine : vue.getSchemasAVoir(typeChaines)) {
			if (unitesChaines.containsKey(chaine)) {
				int nbElts = unitesChaines.get(chaine).size();
				noChaine.put(chaine, nbElts > 1 ? ++no : 0);
			}
		}
		for (Unite unit : listeUnites) {
			Chainon chainon = chainonsUnites.get(unit);
			if (chainon.chaine != null) chainon.noChaine = noChaine.get(chainon.chaine);
		}
		int[] noPar = vue.getCorpus().calculerNoParagraphe(listeUnites);
		if (peutafficher) {
			panneauGraphique.setDonnees(chainons, noPar);
		}
	}
	
	void initVide() {
		listeUnites = new Unite[0];
		chainesUnitesNonFiltrees.clear();
		seuilsValsNbElements = new int[0];
		chainonsUnites.clear();
		unitesChaines.clear();
		if (peutafficher) {
			panneauGraphique.setDonnees(new Chainon[0], new int[0]);
			panneauGraphique.setCouleurs(new Color[0]);
			panneauGraphique.setTaille(new int[0]);
			panneauGraphique.setForme(new int[0]);
			panneauGraphique.setSurTexte(new String[0]);
			panneauGraphique.setSousTexte(new String[0]);
		}
		valNulleMasqueeChamp.clear();
		valsMasqueesChamp.clear();
		valsMasqueesNbElements = new boolean[0];
	}
	
	void setChampAffiche(String champ, HashMap<String, Color> couleursValeursChamp) {
		if (peutafficher) {
			panneauGraphique.setCouleurs(colorerChamp(champ, couleursValeursChamp));
		}
	}
	
	void setChampAfficheTaille(String champ, HashMap<String, Integer> TaillesValeursChamp) {
		if (peutafficher) {
			panneauGraphique.setTaille(ReduireChamp(champ, TaillesValeursChamp));
		}
	}
	
	void setChampAfficheForme(String champ, HashMap<String, Integer> FormesValeursChamp) {
		if (peutafficher) {
			panneauGraphique.setForme(FormerChamp(champ, FormesValeursChamp));
		}
	}
	
	void setChampAfficheSurTexte(String champ, HashMap<String, String> SurTexteValeursChamp) {
		if (peutafficher) {
			panneauGraphique.setSurTexte(SurTexteChamp(champ, SurTexteValeursChamp));
		}
	}
	
	void setChampAfficheSousTexte(String champ, HashMap<String, String> SousTexteValeursChamp) {
		if (peutafficher) {
			panneauGraphique.setSousTexte(SousTexteChamp(champ, SousTexteValeursChamp));
		}
	}
	
	void setChampAfficheNbElts(Color[] couleurs) {
		if (peutafficher) {
			panneauGraphique.setCouleurs(colorerNbElts(couleurs));
		}
	}
	
	
	
	void setChampAfficheTailleNbElts(int[] taille) {
		if (peutafficher) {
			panneauGraphique.setTaille(ReduireNbElts(taille));
		}
	}
	
	void setChampAfficheFormeNbElts(int[] forme) {
		if (peutafficher) {
			panneauGraphique.setForme(FormeNbElts(forme));
		}
	}
	
	void setChampAfficheSurTexteNbElts(String[] SurTexte) {
		if (peutafficher) {
			panneauGraphique.setSurTexte(SurTexteNbElts(SurTexte));
		}
	}
	
	void setChampAfficheSousTexteNbElts(String[] SousTexte) {
		if (peutafficher) {
			panneauGraphique.setSousTexte(SousTexteNbElts(SousTexte));
		}
	}
	
	private Color[] colorerNbElts(Color[] couleurs) {
		Color[] couls = new Color[listeUnites.length];
		for (int i = 0; i < listeUnites.length; i++) {
			Chainon ch = chainonsUnites.get(listeUnites[i]);
			if (ch.chaine == null) couls[i] = couleurs[0];
			else {
				int nbElts = ch.nbEltsChaine;
				couls[i] = couleurs[calculerValNbElements(nbElts)];
			}
		}
		return couls;
	}
	
	private int[] FormeNbElts(int[] formes) {
		int[] forme = new int[listeUnites.length];
		for (int i = 0; i < listeUnites.length; i++) {
			Chainon ch = chainonsUnites.get(listeUnites[i]);
			if (ch.chaine == null) forme[i] = formes[0];
			else {
				int nbElts = ch.nbEltsChaine;
				forme[i] = formes[calculerValNbElements(nbElts)];
			}
		}
		return forme;
	}
	
	private int[] ReduireNbElts(int[] tailles) {
		int[] taille = new int[listeUnites.length];
		for (int i = 0; i < listeUnites.length; i++) {
			Chainon ch = chainonsUnites.get(listeUnites[i]);
			if (ch.chaine == null) taille[i] = tailles[0];
			else {
				int nbElts = ch.nbEltsChaine;
				taille[i] = tailles[calculerValNbElements(nbElts)];
			}
		}
		return taille;
	}
	
	private String[] SurTexteNbElts(String[] SurTextes) {
		String[] SurTexte = new String[listeUnites.length];
		for (int i = 0; i < listeUnites.length; i++) {
			Chainon ch = chainonsUnites.get(listeUnites[i]);
			if (ch.chaine == null) SurTexte[i] = SurTextes[0];
			else {
				int nbElts = ch.nbEltsChaine;
				SurTexte[i] = SurTextes[calculerValNbElements(nbElts)];
			}
		}
		return SurTexte;
	}
	
	private String[] SousTexteNbElts(String[] SousTextes) {
		String[] SousTexte = new String[listeUnites.length];
		for (int i = 0; i < listeUnites.length; i++) {
			Chainon ch = chainonsUnites.get(listeUnites[i]);
			if (ch.chaine == null) SousTexte[i] = SousTextes[0];
			else {
				int nbElts = ch.nbEltsChaine;
				SousTexte[i] = SousTextes[calculerValNbElements(nbElts)];
			}
		}
		return SousTexte;
	}
	
	
	private Color[] colorerChamp(String champ, HashMap<String, Color> couleursValeursChamp) {
		Color[] couls = new Color[listeUnites.length];
		for (int i = 0; i < listeUnites.length; i++) {
			Chainon ch = chainonsUnites.get(listeUnites[i]);
			couls[i] = couleursValeursChamp.get(vue.getValeurChamp(ch.unite, champ));
		}
		return couls;
	}
	
	// même chose que colorer champ mais en influant la taille des chainons
	private int[] ReduireChamp(String champ, HashMap<String, Integer> TaillesValeursChamp) {
		int[] taille = new int[listeUnites.length];
		for (int i = 0; i < listeUnites.length; i++) {
			Chainon ch = chainonsUnites.get(listeUnites[i]);
			taille[i] = TaillesValeursChamp.get(vue.getValeurChamp(ch.unite, champ));
		}
		return taille;
	}
	
	// même chose que colorer champ mais en influant la forme des chainons
	private int[] FormerChamp(String champ, HashMap<String, Integer> FormesValeursChamp) {
		int[] forme = new int[listeUnites.length];
		for (int i = 0; i < listeUnites.length; i++) {
			Chainon ch = chainonsUnites.get(listeUnites[i]);
			forme[i] = FormesValeursChamp.get(vue.getValeurChamp(ch.unite, champ));
		}
		return forme;
	}
	
	// même chose que colorer champ mais en influant le texte au dessus des chainons
	private String[] SurTexteChamp(String champ, HashMap<String, String> SurTexteValeursChamp) {
		String[] SurTexte = new String[listeUnites.length];
		for (int i = 0; i < listeUnites.length; i++) {
			Chainon ch = chainonsUnites.get(listeUnites[i]);
			SurTexte[i] = SurTexteValeursChamp.get(vue.getValeurChamp(ch.unite, champ));
		}
		return SurTexte;
	}
	
	// même chose que colorer champ mais en influant le texte au dessous des chainons
	private String[] SousTexteChamp(String champ, HashMap<String, String> SousTexteValeursChamp) {
		String[] SousTexte = new String[listeUnites.length];
		for (int i = 0; i < listeUnites.length; i++) {
			Chainon ch = chainonsUnites.get(listeUnites[i]);
			SousTexte[i] = SousTexteValeursChamp.get(vue.getValeurChamp(ch.unite, champ));
		}
		return SousTexte;
	}
	
	
	public int calculerValNbElements(int nbElts) {
		return nbElts == 1 ? 1 : nbElts <= seuilsValsNbElements[0] ? 2 : nbElts <= seuilsValsNbElements[1] ? 3 : 4;
	}
	
	
	
	// retourne un tableau contenant les numéros de paragraphe qui contiennent des unités du modele
	public Integer[] listenumeroparagraphe() {
		int[] noparagraphe = (vue.getCorpus()).calculerNoParagraphe(listeUnites);
		ArrayList<Integer> resultat = new ArrayList<>();
		int former = -1;
		for (int i = 0; i < noparagraphe.length; i++) {
			if (former != noparagraphe[i]) {
				resultat.add(noparagraphe[i]);
				former = noparagraphe[i];
			}
		}
		Integer[] resultatconversion = new Integer[resultat.size()];
		return (resultat.toArray(resultatconversion));
	}
	
	
	// renvoie un Hashmap quidécrit la répartition des unités en position
	// au sein du texte
	// @require : listesUnites est trié selon la position de debut de ses éléments
	public ArrayList<int[]> dispositionunites(int nbclasses) {
		Unite[] unites = listeUnites;
		ArrayList<int[]> locationunites = new ArrayList<>(nbclasses);
		int longueurtexte = vue.getCorpus().getTexte().length();
		int[] listeposition = ChaineUtil.discretisation(0, longueurtexte, nbclasses + 1);
		int posprev = 0;
		if (nbclasses > 0) {
			int posactuel = 1;
			int compte = 0;
			int k = 0;
			while (k < unites.length || posactuel < nbclasses + 1) {
				if (k < unites.length && unites[k].getDeb() < listeposition[posactuel]
						&& unites[k].getDeb() >= listeposition[posprev]) {
					// si l'unite est dans cette partie du texte alors on incrémente le compteur
					compte = compte + 1;
					k = k + 1;
				}
				else {
					posprev = posactuel;// on actualise la position precedente
					locationunites.add(new int[] { listeposition[posprev], compte });
					compte = 0;
					posactuel = posactuel + 1;
				}
			}
		}
		return locationunites;
	}
	
	
	
	
	// renvoie un tableau contenant les fréquences de chaque valeur d'un champ de la chaine dans la liste d'unités
	public HashMap<String, Float> calculerfrequencechaine(Unite[] donnees, String typeChaines, String Champcible, HashSet<String> valeurinterdite) {
		String[] valsdisponibles = vue.getValeursChamp(Schema.class, typeChaines, Champcible);
		HashMap<String, Float> tableaufrequence = new HashMap<>(valsdisponibles.length + 1);
		String valeurchamp;
		// on initialise le tableau résultat
		for (String valeur : valsdisponibles) {
			tableaufrequence.put(valeur, 0.0f);
		}
		tableaufrequence.put("", 0.0f);
		int compteur = 0;
		// on compte le nombre d'occurences
		for (Unite unit : donnees) {
			valeurchamp = chainesUnitesNonFiltrees.containsKey(unit) ? vue.getValeurChamp(chainesUnitesNonFiltrees.get(unit), Champcible) : null;
			if (valeurchamp != null && !valeurinterdite.contains(valeurchamp)) {
				compteur = compteur + 1;
				tableaufrequence.put(valeurchamp, tableaufrequence.get(valeurchamp) + 1);
			}
		}
		// on calcule les pourcentages
		for (String valeur : valsdisponibles) {
			tableaufrequence.put(valeur, compteur == 0 ? 0.0f
					: tableaufrequence.get(valeur) * 10000.0f /
							compteur / 100.0f);
		}
		tableaufrequence.put("", compteur == 0 ? 0.0f
				: tableaufrequence.get("") * 10000.0f /
						compteur / 100.0f);
		
		return tableaufrequence;
	}
	
	// renvoie un tableau contenant les fréquences de chaque longueur des chaines de la liste d'unités
	public HashMap<String, Float> calculerfrequencenbelt(Unite[] donnees, String typeUnites, String[] valsdisponibles) {
		HashMap<String, Float> tableaufrequence = new HashMap<>(valsdisponibles.length);
		String valeurchamp;
		// on initialise le tableau résultat
		for (String valeur : valsdisponibles) {
			tableaufrequence.put(valeur, 0.0f);
		}
		int compte = 0;
		// on compte le nombre d'occurences
		for (Unite unit : donnees) {
			Schema chaine = chainesUnitesNonFiltrees.containsKey(unit) ? chainesUnitesNonFiltrees.get(unit)
					: null;
			int nb = chaine == null ? 0 : unitesChaines.get(chaine).size();
			if (isMasqueeValNbElements(calculerValNbElements(nb))) {
				
			}
			else if (nb == 0) {
				valeurchamp = valsdisponibles[0];
				tableaufrequence.put(valeurchamp, tableaufrequence.get(valeurchamp) + 1);
				compte = compte + 1;
			}
			else {
				valeurchamp = valsdisponibles[calculerValNbElements(nb)];
				tableaufrequence.put(valeurchamp, tableaufrequence.get(valeurchamp) + 1);
				compte = compte + 1;
			}
		}
		
		
		// on calcule les pourcentages
		for (String valeur : valsdisponibles) {
			tableaufrequence.put(valeur, tableaufrequence.get(valeur) * 10000.0f /
					compte / 100.0f);
		}
		
		
		return tableaufrequence;
	}
	
	
	
	
	// on enlève les unités dont les valeurs des chaines les contenant sont masquees
	public Unite[] filtrechaine(Unite[] donnees, String Champ, HashSet<String> Valeurcachees) {
		ArrayList<Unite> unitefiltrees = new ArrayList<>();
		for (Unite unit : donnees) {
			if (chainesUnitesNonFiltrees.containsKey(unit) && !(Valeurcachees.contains(vue.getValeurChamp(chainesUnitesNonFiltrees.get(unit), Champ)))) {
				unitefiltrees.add(unit);
			}
		}
		Unite[] resultat = unitefiltrees.toArray(new Unite[0]);
		return resultat;
	}
	
	// on ne garde que les unités dont les chaines les contenant possèdent la valeur "valeur" pour le champ
	// "Champ"
	public Unite[] filtrevaleurchaine(Unite[] donnees, String Champ, String valeur) {
		ArrayList<Unite> uniteduchamp = new ArrayList<>();
		ArrayList<Unite> unitefiltrees = new ArrayList<>();
		for (Schema chaine : unitesChaines.keySet()) {
			// on trie les unités dont les chaines contenant ont la bonne valeur pour le champ
			if ((vue.getValeurChamp(chaine, Champ)).equals(valeur)) {
				uniteduchamp.addAll(unitesChaines.get(chaine));
			}
		}
		// on ne garde que les unités passée en paramètre
		for (int i = 0; i < donnees.length; i++) {
			if (uniteduchamp.contains(donnees[i])) {
				unitefiltrees.add(donnees[i]);
			}
		}
		// on met le résultat sous forme de tableau
		Unite[] resultat = (Unite[]) unitefiltrees.toArray();
		return resultat;
	}
	
	
	// renvoioe la chaine qui contient l'unité dans ModeleChaine
	public Schema getSchemacontenantunite(Unite unit) {
		return chainesUnitesNonFiltrees.get(unit);
	}
	
	// filtre qui remplit les mêmes fonctions que le précédent mais également qui filtre suivant une liste de
	// paragraphes interdits et ne garde que les unités appartenant aux chaines ayant pour le champ Champ la valeur
	// Valeur
	private void filtrerListeUnites(String typeUnites, String typeChaines, ArrayList<Integer> noparagraphecachees,
			String Champ, String Valeur) {
		Unite[] unites = vue.getUnitesAVoir(typeUnites);
		Schema[] chaines = vue.getSchemasAVoir(typeChaines);
		unitesChaines.clear();
		int[] noparagraphe = (vue.getCorpus()).calculerNoParagraphe(unites);
		for (Schema chaine : chaines)
			unitesChaines.put(chaine, new ArrayList<Unite>());
		ArrayList<Integer> indFiltrees = new ArrayList<>();
		String[] champs = vue.getNomsChamps(Unite.class, typeUnites);
		boolean agarder;
		for (int i = 0; i < unites.length; i++) {
			agarder = true;
			for (String champ : champs) {// pour tous les champs
				String val = vue.getValeurChamp(unites[i], champ);// je recupere la valeur du champ
				if (val.isEmpty()) {
					if (valNulleMasqueeChamp.contains(champ)) {
						agarder = false;
						break;
					}
				}
				else {
					if (valsMasqueesChamp.get(champ).contains(val)) {
						agarder = false;
						break;
					}
				}
			}
			// si l'unité appartient à un paragraphe interdit
			if (noparagraphecachees.contains(noparagraphe[i])) {
				agarder = false;
			}
			// si la chaine contenant l'unité a une mauvaise valeur pour le champ ou si l'unité est sans chaine
			if (!chainesUnitesNonFiltrees.containsKey(unites[i]) ||
					!vue.getValeurChamp(chainesUnitesNonFiltrees.get(unites[i]), Champ).equals(Valeur)) {
				agarder = false;
			}
			if (agarder) {
				indFiltrees.add(i);
				if (chainesUnitesNonFiltrees.containsKey(unites[i])) {
					Schema chaine = chainesUnitesNonFiltrees.get(unites[i]);
					unitesChaines.get(chaine).add(unites[i]);
				}
			}
		}
		ArrayList<Integer> indIndFiltrees = new ArrayList<>();
		for (int k = 0; k < indFiltrees.size(); k++) {
			Unite unit = unites[indFiltrees.get(k)];
			if (chainesUnitesNonFiltrees.containsKey(unit)) {
				if (!isMasqueeValNbElements(calculerValNbElements(
						unitesChaines.get(chainesUnitesNonFiltrees.get(unit)).size())))
					indIndFiltrees.add(k);
			}
			else {
				if (!isMasqueeValNbElements(0)) indIndFiltrees.add(k);
			}
		}
		listeUnites = new Unite[indIndFiltrees.size()];
		for (int k = 0; k < listeUnites.length; k++) {
			listeUnites[k] = unites[indFiltrees.get(indIndFiltrees.get(k))];
		}
		for (Schema chaine : chaines) {
			int nb = unitesChaines.get(chaine).size();
			if (nb == 0 || isMasqueeValNbElements(calculerValNbElements(nb)))
				unitesChaines.remove(chaine);
		}
	}
	
	// permet de calculer les données pour une valeur de champ de la chaine
	private void calculerDonnees(String typeChaines, String typeUnites, ArrayList<Integer> noparagraphecachees, String champ, String Valeur) {
		filtrerListeUnites(typeUnites, typeChaines, noparagraphecachees, champ, Valeur);
		Chainon[] chainons = new Chainon[listeUnites.length];
		for (int i = 0; i < listeUnites.length; i++) {
			Unite unit = listeUnites[i];
			chainons[i] = new Chainon(unit, vue.getTexteUnite(unit));
			if (chainesUnitesNonFiltrees.containsKey(unit)) {
				Schema chaine = chainesUnitesNonFiltrees.get(unit);
				chainons[i].chaine = chaine;
				chainons[i].nbEltsChaine = unitesChaines.get(chaine).size();
				chainons[i].idChaine = vue.getIdElement(chaine);
			}
			chainonsUnites.put(listeUnites[i], chainons[i]);
		}
		HashMap<Schema, Integer> noChaine = new HashMap<>();
		int no = 0;
		for (Schema chaine : vue.getSchemasAVoir(typeChaines)) {
			if (unitesChaines.containsKey(chaine)) {
				int nbElts = unitesChaines.get(chaine).size();
				noChaine.put(chaine, nbElts > 1 ? ++no : 0);
			}
		}
		for (Unite unit : listeUnites) {
			Chainon chainon = chainonsUnites.get(unit);
			if (chainon.chaine != null) chainon.noChaine = noChaine.get(chainon.chaine);
		}
		int[] noPar = vue.getCorpus().calculerNoParagraphe(listeUnites);
		if (peutafficher) {
			panneauGraphique.setDonnees(chainons, noPar);
		}
	}
	
	
	
	
	
	
}
