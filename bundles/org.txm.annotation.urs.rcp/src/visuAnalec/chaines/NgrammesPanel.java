/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package visuAnalec.chaines;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.HashSet;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import visuAnalec.Message;
import visuAnalec.elements.Schema;
import visuAnalec.elements.Unite;
import visuAnalec.util.GMessages;
import visuAnalec.util.GVisu;
import visuAnalec.vue.Vue;

/**
 *
 * @author marc
 * panneau affichage des statistiques des n-grams
 */
public class NgrammesPanel extends JPanel implements Vue.VueListener, ActionListener {

private JList legendechampstatistique;
private JPopupMenu popupChampstat = new JPopupMenu();
private JPopupMenu popupChamptrigramme = new JPopupMenu();
private JCheckBox digrammeXX = new JCheckBox();
private JCheckBox trigrammeXOX = new JCheckBox();
private JCheckBox trigrammeXOY = new JCheckBox();
private JCheckBox trigrammeXXX = new JCheckBox();
private JCheckBox trigrammeXYZ = new JCheckBox();
private final GVisu.Val0ComboBox typeChamp;
private final GVisu.Val0ComboBox champstatistique;
private final GVisu.Val0ComboBox saisiechampchaine;
private final JCheckBox activeparagraphe;
private final JCheckBox activeglissant;
private final JCheckBox activesingleton;
private final Box commandesstat;
private String[] valsAfficheesstat;
private NgrammesModel ngrammesModel;
private HashSet valsAfficheesautrestat=new HashSet();
private Vue vue;
private String typeChaines;
private String typeUnites;
private JTable resultatstatistique;
private ModeleChaines donneesChaines;
private final String[] statpossibles = new String[]{"Digramme","Trigramme"};

public NgrammesPanel(Vue vue){    
    this.vue=vue;
    vue.addEventListener(this);
    
    legendechampstatistique = new JList();
    legendechampstatistique.addMouseListener(new MouseAdapter() {
      @Override
      public void mousePressed(MouseEvent e) {//on gère les évènements de la souris
        check(e);
      }
      @Override
      public void mouseReleased(MouseEvent e) {
        check(e);
      }
      public void check(MouseEvent e) {
        if (e.isPopupTrigger()) {//if the event shows the menu
          popupChampInitStat(legendechampstatistique.locationToIndex(e.getPoint()));
          popupChampstat.show(legendechampstatistique, e.getX(), e.getY());
        }
      }
    });
    typeChamp = new GVisu.Val0ComboBox("Rien");
    typeChamp.addActionListener(this);
    champstatistique = new GVisu.Val0ComboBox("Rien");
    champstatistique.addActionListener(this);  
    saisiechampchaine = new GVisu.Val0ComboBox("Rien");
    saisiechampchaine.addActionListener(this);  
    activeparagraphe = new JCheckBox("activer les paragraphes (chiffre 0)");
    activeglissant = new JCheckBox("activer le mode glissant");
    activesingleton = new JCheckBox("activer l'affichage des singletons");
    commandesstat = Box.createVerticalBox();
    commandesstat.add(Box.createVerticalGlue());
    commandesstat.add(activeparagraphe);
    commandesstat.add(Box.createVerticalGlue());
    commandesstat.add(activeglissant);
    commandesstat.add(Box.createVerticalGlue());
    commandesstat.add(activesingleton);    
    commandesstat.add(Box.createVerticalGlue());
    commandesstat.add(new JLabel("Choisisser une statistique : "));
    commandesstat.add(champstatistique);
    commandesstat.add(Box.createVerticalGlue());
    commandesstat.add(new JLabel("Choisisser un type de champ: "));
    commandesstat.add(typeChamp);    
    commandesstat.add(Box.createVerticalGlue());
    commandesstat.add(new JLabel("Choisisser un champ de la chaine : "));
    commandesstat.add(saisiechampchaine);
    commandesstat.add(Box.createVerticalGlue());
    commandesstat.add(legendechampstatistique);  
    commandesstat.add(Box.createVerticalGlue());
    digrammeXX = new JCheckBox("Compter les digrammes de type XX");
    commandesstat.add(digrammeXX); 
    commandesstat.add(Box.createVerticalGlue());
    trigrammeXOX = new JCheckBox("Compter les digrammes de type X0X");
    commandesstat.add(trigrammeXOX);     
    commandesstat.add(Box.createVerticalGlue());
    trigrammeXOY = new JCheckBox("Compter les digrammes de type X0Y");
    commandesstat.add(trigrammeXOY); 
    commandesstat.add(Box.createVerticalGlue());
    trigrammeXYZ = new JCheckBox("Compter les digrammes de type XYZ");
    commandesstat.add(trigrammeXYZ); 
    commandesstat.add(Box.createVerticalGlue());
    trigrammeXXX = new JCheckBox("Compter les digrammes de type XXX");
    commandesstat.add(trigrammeXXX); 
    JScrollPane scrollstat = new JScrollPane(commandesstat);
    this.donneesChaines = null;
        
    Box resultatstat = Box.createVerticalBox();
    resultatstat.add(Box.createVerticalGlue());
    resultatstat.add(new JLabel("Statistique : "));
    resultatstat.add(Box.createVerticalGlue());
    
    DefaultTableModel initModel = new DefaultTableModel(new Object[0][0],
    new String[]{"Ngramme","Nombre d'occurences","Fréquence","Position moyenne"}){
               Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class,java.lang.Float.class,java.lang.Float.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            };
    
    resultatstatistique = new JTable(initModel);
    resultatstatistique.addMouseListener(new MouseAdapter() {
      @Override
      public void mousePressed(MouseEvent e) {//on gère les évènements de la souris
        check(e);
      }
      @Override
      public void mouseReleased(MouseEvent e) {
        check(e);
      }
      public void check(MouseEvent e) {
        if (e.isPopupTrigger()) {//if the event shows the menu
          popupChampInitTrigramme(resultatstatistique.rowAtPoint(e.getPoint()));
          popupChamptrigramme.show(resultatstatistique, e.getX(), e.getY());
        }
      }
    });
    JScrollPane jsp =new JScrollPane(resultatstatistique);
    resultatstat.add(resultatstatistique.getTableHeader());
    resultatstat.add(jsp);
    typeChamp.setAlignmentX(0);
    champstatistique.setAlignmentX(0);
    saisiechampchaine.setAlignmentX(0);
    legendechampstatistique.setAlignmentX(0); 
    JScrollPane scrollstatresultat = new JScrollPane(resultatstat);
    JSplitPane split1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,  scrollstat,scrollstatresultat);
    add(split1);
    split1.setDividerLocation(1./4);
    
}

public void setDonneesChaines(ModeleChaines donneesChaines){
    this.donneesChaines=donneesChaines;
}

public void setVue(Vue vue){
    this.vue= vue;
}

public void setTypeChaines(String typeChaines){
    this.typeChaines=typeChaines;
}

public void setTypeUnites(String typeUnites){
    this.typeUnites=typeUnites;
}

public void update(){
    if(this.donneesChaines!=null && this.vue!=null && this.typeChaines!=null && this.typeUnites!=null){
        initstat();
    }
    else{
        init();
    }
}

public void importerAffichage(HashMap choix){
        typeChamp.removeActionListener(this);
        saisiechampchaine.removeActionListener(this);
        champstatistique.removeActionListener(this);
        activeparagraphe.setSelected(choix.get("activeparagraphe").equals("vrai"));
        activeglissant.setSelected(choix.get("activeglissant").equals("vrai"));
        activesingleton.setSelected(choix.get("activesingleton").equals("vrai"));
        digrammeXX.setSelected(choix.get("digrammeXX").equals("vrai"));
        trigrammeXOX.setSelected(choix.get("trigrammeXOX").equals("vrai"));
        trigrammeXOY.setSelected(choix.get("trigrammeXOY").equals("vrai"));
        trigrammeXXX.setSelected(choix.get("trigrammeXXX").equals("vrai"));
        trigrammeXYZ.setSelected(choix.get("trigrammeXYZ").equals("vrai"));
        if(choix.get("typeChamp")!=null){
          typeChamp.setSelectedItem(choix.get("typeChamp"));
          if(!typeChamp.pasdeSelection()){
            switch(typeChamp.getSelection()){
                case "Chaine" :
                    saisiechampchaine.removeActionListener(this);
                    saisiechampchaine.setModele(vue.getNomsChamps(Schema.class, typeChaines));
                    saisiechampchaine.addActionListener(this);                       
                    break;
                case "Unité" :
                    saisiechampchaine.removeActionListener(this);
                    saisiechampchaine.setModele(vue.getNomsChamps(Unite.class, typeUnites));
                    saisiechampchaine.addActionListener(this);                        
                    break;
                default :
                    saisiechampchaine.removeActionListener(this);
                    saisiechampchaine.setModeleVide();
                    saisiechampchaine.addActionListener(this);                      
                    break;
            }
          }
          else{
                saisiechampchaine.removeActionListener(this);
                saisiechampchaine.setModeleVide();
                saisiechampchaine.addActionListener(this);                 
          }
        }
        else{
          typeChamp.effSelection();
        }
        
        if(choix.get("saisiechampchaine")!=null){
          saisiechampchaine.setSelectedItem(choix.get("saisiechampchaine"));
        }
        else{
          saisiechampchaine.effSelection();
        }
        if(choix.get("champstatistique")!=null){
        champstatistique.setSelectedItem(choix.get("champstatistique"));
        }
        else{
          champstatistique.effSelection();
        }
        typeChamp.addActionListener(this);
        saisiechampchaine.addActionListener(this);
        champstatistique.addActionListener(this);
        this.valsAfficheesautrestat = (HashSet) choix.get("valsAfficheesautrestat");
        this.valsAfficheesstat = (String[]) choix.get("valsAfficheesstat");
    
}

public HashMap exporterAffichage(){
    HashMap choix = new HashMap();
      choix.put("activeparagraphe",activeparagraphe.isSelected() ? "vrai" : "faux");
      choix.put("activeglissant",activeglissant.isSelected() ? "vrai" : "faux");
      choix.put("activesingleton",activesingleton.isSelected() ? "vrai" : "faux");
      choix.put("digrammeXX",digrammeXX.isSelected() ? "vrai" : "faux");
      choix.put("trigrammeXOX",trigrammeXOX.isSelected() ? "vrai" : "faux");
      choix.put("trigrammeXOY",trigrammeXOY.isSelected() ? "vrai" : "faux");
      choix.put("trigrammeXXX",trigrammeXXX.isSelected() ? "vrai" : "faux");
      choix.put("trigrammeXYZ",trigrammeXYZ.isSelected() ? "vrai" : "faux");
      choix.put("typeChamp",typeChamp.pasdeSelection() ? null : typeChamp.getSelection());
      choix.put("saisiechampchaine",saisiechampchaine.pasdeSelection() ? null : saisiechampchaine.getSelection());
      choix.put("champstatistique",champstatistique.pasdeSelection() ? null : champstatistique.getSelection());
      choix.put("valsAfficheesautrestat",valsAfficheesautrestat);
      choix.put("valsAfficheesstat",valsAfficheesstat);
    return choix;
    
}

private void popupChampInitStat(final int index) {
    popupChampstat.removeAll();
    if (saisiechampchaine.pasdeSelection()) {
    } else if (index>=valsAfficheesstat.length - valsAfficheesautrestat.size()) {
      popupChampstat.add(new GMessages.GAction("Afficher toutes les autres valeurs") {
        @Override
        public void executer() {
          eclaterautres();
        }
      });
    } else {
      popupChampstat.add(new GMessages.GAction("Ajouter à la valeur autre") {
        @Override
        public void executer() {
          ajouteraautres(legendechampstatistique.getModel().getElementAt(index));
        }
      });
    }
  }

private void popupChampInitTrigramme(final int index) {
    popupChamptrigramme.removeAll();
    if (!champstatistique.pasdeSelection() && !saisiechampchaine.pasdeSelection()) {
        String valeur = resultatstatistique.getValueAt(index, 0).toString();
        if(!valeur.equals("x0x") && !valeur.equals("xx") &&!valeur.equals("x0y") &&!valeur.equals("xyz") &&
                !valeur.equals("xxx")){
            String[] affichage;
            switch(champstatistique.getSelection()){
                case "Digramme" :
                    affichage = ngrammesModel.infoSuivant(valeur);
                    break;
                case "Trigramme" :
                    affichage = ngrammesModel.infoSuivant(valeur);
                    break;
                default :
                    affichage = new String[0];
                    break;
            }
            for(int i = 0; i < affichage.length;i++){
                popupChamptrigramme.add(new JLabel(affichage[i]));
            }
        }
    } 
  }
//permet d'ajouter à la catégorie "autre" la valeur d'un champ
  private void ajouteraautres (Object valeur){

        this.valsAfficheesautrestat.add(valeur.toString().substring(4));

    initstat();
  }
  private void eclaterautres(){
    this.valsAfficheesautrestat.clear();
    initstat();
  }
  
  public HashMap<String,Character> LegendeStat(String[] valsAfficheesstat,HashSet valsAfficheesautre){
      HashMap<String,Character> retour = new HashMap<String,Character>();
      int compte = 0;
      for(String unit : valsAfficheesstat){
          if(!valsAfficheesautre.contains(unit) ){
          compte = compte + 1;
          retour.put(unit,GVisu.intToChar(compte));
          }
      }
      retour.put("autre", GVisu.intToChar(compte+1));
      retour.put("singleton", GVisu.intToChar(compte+2));
     return retour;
  }
  

  public void AfficherLegendeStat(HashMap<String,Character> donnee){
  String[] legendes = new String[donnee.size()];
  for(String unit : donnee.keySet()){
    legendes[GVisu.CharToInt(donnee.get(unit)) - 1]= "" + donnee.get(unit) +"   "+ unit;
  }
  legendechampstatistique.setListData(legendes);
  }
  
  public void initstat(){
      if(!champstatistique.pasdeSelection() && !saisiechampchaine.pasdeSelection() && !typeChamp.pasdeSelection()){
            switch(typeChamp.getSelection()){
                case "Chaine" :
                    this.valsAfficheesstat = vue.getValeursChamp(Schema.class,typeChaines,
                            saisiechampchaine.getSelection());              
                    break;
                case "Unité" :
                    this.valsAfficheesstat = vue.getValeursChamp(Unite.class,typeUnites,
                            saisiechampchaine.getSelection());                      
                    break;
            }
        HashMap<String,Character> legende = LegendeStat(this.valsAfficheesstat,this.valsAfficheesautrestat);
        AfficherLegendeStat(legende);
        this.ngrammesModel=new NgrammesModel();
        int nbLettre;
        if( "Trigramme".equals(champstatistique.getSelection())){
            nbLettre = 3;
        }
        else if("Digramme".equals(champstatistique.getSelection())){
            nbLettre = 2;
        }
        else{
            nbLettre = 1;
        }
        //on calcule le modèle des ngrammes
        this.ngrammesModel.ngrammeUpdate(nbLettre, legende,saisiechampchaine.getSelection(),vue,
                activeglissant.isSelected(), donneesChaines,activesingleton.isSelected(), 
                typeChamp.getSelection(),activeparagraphe.isSelected());
        boolean[] ligneSup;
        if( "Trigramme".equals(champstatistique.getSelection())){
            ligneSup = new boolean[4];
            ligneSup[0]=trigrammeXOX.isSelected();
            ligneSup[1]=trigrammeXOY.isSelected();
            ligneSup[2]=trigrammeXYZ.isSelected();
            ligneSup[3]=trigrammeXXX.isSelected();
        }
        else if("Digramme".equals(champstatistique.getSelection())){
            ligneSup = new boolean[1];
            ligneSup[0]=digrammeXX.isSelected();
        }
        else{
            ligneSup = new boolean[0];
        }
        ((DefaultTableModel) resultatstatistique.getModel()).setDataVector(
                this.ngrammesModel.gettableau(ligneSup),new String[]{champstatistique.getSelection()
                        ,"Nombre d'occurences","Fréquence","Position moyenne"});
        resultatstatistique.setAutoCreateRowSorter(true);
          
      }
  }
  
  @Override
  public void actionPerformed(ActionEvent evt) {
    try {
      if (evt.getSource().equals(saisiechampchaine)) {
        this.valsAfficheesautrestat.clear();
        initstat();
        return;
      }
      if (evt.getSource().equals(champstatistique)) {
          this.valsAfficheesautrestat.clear();
          initstat();
        return;
      }
      if (evt.getSource().equals(typeChamp)) {
          if(!typeChamp.pasdeSelection()){
            switch(typeChamp.getSelection()){
                case "Chaine" :
                    saisiechampchaine.removeActionListener(this);
                    saisiechampchaine.setModele(vue.getNomsChamps(Schema.class, typeChaines));
                    saisiechampchaine.addActionListener(this);                       
                    break;
                case "Unité" :
                    saisiechampchaine.removeActionListener(this);
                    saisiechampchaine.setModele(vue.getNomsChamps(Unite.class, typeUnites));
                    saisiechampchaine.addActionListener(this);                        
                    break;
                default :
                    saisiechampchaine.removeActionListener(this);
                    saisiechampchaine.setModeleVide();
                    saisiechampchaine.addActionListener(this);                      
                    break;
            }
          }
          else{
                saisiechampchaine.removeActionListener(this);
                saisiechampchaine.setModeleVide();
                saisiechampchaine.addActionListener(this);                 
          }
        return;
      } 
      throw new UnsupportedOperationException("Evénement non prévu : "+evt.paramString());
    } catch (Throwable ex) {
      GMessages.erreurFatale(ex);
    }
  }

  @Override
  public void traiterEvent(Message evt) { // très grossier : à améliorer
    switch (evt.getType()) {
      case CLEAR_CORPUS:
        return;
      case NEW_CORPUS:  // impossible en principe : CLEAR_CORPUS a détruit la fenêtre
        throw new UnsupportedOperationException("Fenêtre chaines présente lors d'un message NEW_CORPUS ???");
      case MODIF_TEXTE:
        return;
      case MODIF_VUE:
        return;
      case MODIF_STRUCTURE:
        return;
      case MODIF_ELEMENT:
//        ElementEvent evtE = (ElementEvent) evt;
//        if (evtE.getModif() != TypeModifElement.BORNES_UNITE) fermerFenetre();
        return;
      case CORPUS_SAVED:
          return;
      default:
        throw new UnsupportedOperationException("Cas "+evt.getType()+" oublié dans un switch");
    }
  }
  
  public void init(){
      
    typeChamp.removeActionListener(this);
    typeChamp.setModele(new String[]{"Chaine","Unité"});
    typeChamp.setSelectedIndex(0);
    typeChamp.addActionListener(this);      
    saisiechampchaine.removeActionListener(this);
    //saisiechampchaine.setModele(vue.getNomsChamps(Schema.class, typeChaines));
    saisiechampchaine.setModeleVide();
    saisiechampchaine.addActionListener(this);    
    champstatistique.removeActionListener(this);
    champstatistique.setModele(statpossibles);
    champstatistique.addActionListener(this);    
    if(vue!=null && donneesChaines!=null){
        initstat();
    }
      
  }
  
  public void initVide(){
    saisiechampchaine.setModeleVide();
    champstatistique.setModeleVide();
    typeChamp.setModeleVide();
      
  }
  
  public Object[][] donneesCSV(){
        Object[][] donneesCSV = new Object[resultatstatistique.getRowCount()+1][resultatstatistique.getColumnCount()];
        for(int i = 0 ; i < resultatstatistique.getRowCount();i++){
             for(int j = 0 ; j < resultatstatistique.getColumnCount();j++){
                donneesCSV[i+1][j]=resultatstatistique.getValueAt(i, j);
             }
        }
        for(int j = 0 ; j < resultatstatistique.getColumnCount();j++){
                donneesCSV[0][j]=resultatstatistique.getColumnName(j);
        }
        return donneesCSV;
  }


}
