/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package visuAnalec.chaines;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.swing.table.*;
import visuAnalec.elements.Schema;
import visuAnalec.elements.Unite;
import visuAnalec.vue.Vue;

/**
 *
 * @author Marc
 */
public class ModeleStatChaine extends AbstractTableModel {
	
	private Object[][] donnees;
	
	private String[] entetes = new String[] { "Trigramme", "Nombre d'occurences", "Fréquence", "Position moyenne" };
	
	@Override
	public int getColumnCount() {
		return entetes.length;
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	}
	
	public ModeleStatChaine(Object[][] donnees, String[] titres) {
		this.donnees = donnees;
		this.entetes = titres;
	}
	
	public ModeleStatChaine() {
		this.donnees = new Object[0][0];
		this.entetes = new String[0];
	}
	
	@Override
	public int getRowCount() {
		return donnees.length;
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return donnees[rowIndex][columnIndex];
	}
	
	@Override
	public Class getColumnClass(int columnIndex) {
		switch (columnIndex) {
			case 0:
				return String.class;
			default:
				return Float.class;
		}
	}
	
	public HashMap<Object, HashMap<Object, Float>> conversionNbOccurencesToFrequence(
			HashMap<Object, HashMap<Object, Float>> donnees) {
		float sommepartielle;
		for (Object val : donnees.keySet()) {
			sommepartielle = 0.0f;
			for (Object val1 : donnees.get(val).keySet()) {
				sommepartielle = sommepartielle + (donnees.get(val)).get(val1);
			}
			for (Object val1 : donnees.get(val).keySet()) {
				(donnees.get(val)).put(val1, (sommepartielle != 0.0f ? (donnees.get(val)).get(val1) * 10000.0f / sommepartielle / 100.0f : 0.0f));
			}
		}
		return donnees;
	}
	
	public void getTableau(HashMap<Object, HashMap<Object, Float>> donnees, int nbcolonnes, String observation) {
		Object[][] resultat = new Object[donnees.size()][nbcolonnes + 2];
		String[] titre = new String[nbcolonnes + 2];
		int i = 0;
		HashMap<Object, Integer> idcolonnes = new HashMap();
		donnees = conversionNbOccurencesToFrequence(donnees);
		titre[0] = observation;
		// on choisit les colonnes des valeurs
		// grossier
		for (Object val : donnees.keySet()) {
			i = 0;
			for (Object val1 : donnees.get(val).keySet()) {
				titre[i + 1] = val1.toString();
				idcolonnes.put(val1, i + 1);
				i = i + 1;
			}
		}
		i = 0;
		for (Object val : donnees.keySet()) {
			// on écrit l'objet surlequel porte l'observation
			resultat[i][0] = val;
			for (Object val1 : donnees.get(val).keySet()) {
				resultat[i][idcolonnes.get(val1)] = (donnees.get(val)).get(val1);
			}
			i = i + 1;
		}
		this.entetes = titre;
		this.donnees = resultat;
		
	}
	
	// @require : typeChampLigne n'a aucune case nulle, typeUnite et typeChaine sont non-null
	// typeChampColonne est défini
	// génère un modèle pour un tableau en comptant le nombre d'occurrence de la valeur du champ en tête de
	// colonne par par valeur du ou des champs en tête de ligne
	public static ModeleStatChaine calculateStatCoref(String nomChampChaine,
			String nomChampUnite, Collection<String> valeurChaineCachees,
			String typeChampColonne, String[] typeChampLigne, Vue vue, ModeleChaines donneesChaine,
			ArrayList paragrapheCaches, String typeChaine, String typeUnite, String[] valNbElements) {
		HashMap<Object, HashMap<Object, Float>> retour = new HashMap<>();
		Unite[] listeUnites = donneesChaine.getUnites();
		int[] noparag = (vue.getCorpus()).calculerNoParagraphe(listeUnites);
		
		// on créé une ligne par défaut
		HashMap<String, Float> defaultLine = new HashMap<>();
		switch (typeChampColonne) {
			case "Chaine":
				// un champ de la chaine ne peut pas être nul
				for (String val : vue.getValeursChamp(Schema.class, typeChaine, nomChampChaine)) {
					defaultLine.put(val, 0.0f);
				}
				defaultLine.put("Aucune valeur", 0.0f);
				break;
			case "Unite":
				for (String val : (nomChampUnite == null ? valNbElements
						: vue.getValeursChamp(Unite.class, typeUnite, nomChampUnite))) {
					defaultLine.put(val, 0.0f);
				}
				defaultLine.put("Aucune valeur", 0.0f);
				break;
			default:
				for (int val : noparag) {
					defaultLine.put("  Paragraphe " + val, 0.0f);
				}
				break;
		}
		
		
		int numeroUnite = 0;
		for (Unite unit : listeUnites) {
			Schema chaine = donneesChaine.getchainesUnitesNonFiltrees().get(unit);
			
			// on filtre les unités
			String propUnite = (nomChampUnite == null ? valNbElements[donneesChaine.calculerValNbElements(
					donneesChaine.getChainonUnites().get(unit).nbEltsChaine)]
					: vue.getValeurChamp(unit, nomChampUnite));
			
			String propChaine = chaine == null ? null : (vue.getValeurChamp(chaine, nomChampChaine));
			
			if (!paragrapheCaches.contains(noparag[numeroUnite]) && chaine != null &&
					!(nomChampUnite == null ? donneesChaine.isMasqueeValNbElements(donneesChaine.calculerValNbElements(donneesChaine.getChainonUnites().get(unit).nbEltsChaine))
							: donneesChaine.isMasqueeValChamp(nomChampUnite, propUnite)) &&
					!valeurChaineCachees.contains(propChaine)) {
				
				// on calcule la clé de la ligne sous forme de chaine de caractère
				String key1 = new String();
				for (int i = 0; i < typeChampLigne.length; i++) {
					switch (typeChampLigne[i]) {
						case "Chaine":
							key1 = key1 + "  " + (propChaine == null || propChaine.equals("")
									? "Aucune valeur"
									: propChaine);
							break;
						case "Unite":
							key1 = key1 + "  " + (propUnite == null || propUnite.equals("") ? "Aucune valeur" : propUnite);
							break;
						default:
							key1 = key1 + "  Paragraphe " + noparag[numeroUnite];
							break;
					}
				}
				// on cherche la clé de la colonne et on initialise les valeurs
				String key2 = new String();
				switch (typeChampColonne) {
					case "Chaine":
						key2 = key2 + (propChaine == null || propChaine.equals("")
								? "Aucune valeur"
								: propChaine);
						break;
					case "Unite":
						key2 = key2 + (propUnite == null || propUnite.equals("")
								? "Aucune valeur"
								: propUnite);
						break;
					default:
						key2 = key2 + "  Paragraphe " + noparag[numeroUnite];
						break;
				}
				// on ajoute l'unité dans la table correspondante
				HashMap<Object, Float> ligne = (retour.containsKey(key1) ? retour.get(key1) : (HashMap<Object, Float>) defaultLine.clone());
				ligne.put(key2, ligne.get(key2) + 1);
				retour.put(key1, ligne);
			}
			numeroUnite++;
		}
		
		String titreLigne = new String();
		for (String type : typeChampLigne) {
			switch (type) {
				case "Chaine":
					titreLigne = titreLigne + "  Valeur Champ Chaine";
					break;
				case "Unite":
					titreLigne = titreLigne + "  Valeur Champ Unite";
					break;
				default:
					titreLigne = titreLigne + "  Numero Paragraphe";
					break;
			}
		}
		ModeleStatChaine modele = new ModeleStatChaine();
		modele.getTableau(retour, defaultLine.size(), titreLigne);
		return modele;
	}
	
	
	
	
	
	
}
