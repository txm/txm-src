/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package visuAnalec.chaines;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import visuAnalec.elements.Schema;
import visuAnalec.elements.Unite;
import visuAnalec.vue.Vue;

/**
 *
 * @author marc
 */
public class NgrammesModel {
	
	public class ngramme {
		
		private HashSet<Unite[]> unites;
		
		private ArrayList<String> suivant;
		
		private float frequence;
		
		private float posmoyenne;
		
		public HashSet<Unite[]> getUnites() {
			return unites;
		}
		
		public int getnboccurences() {
			return unites.size();
		}
		
		public float getfrequence() {
			return frequence;
		}
		
		public float getposmoyenne() {
			return posmoyenne;
		}
		
		public void setposmoyenne() {
			float somme = 0.0f;
			for (Unite[] unit : unites) {
				somme = somme + unit[0].getDeb();
			}
			this.posmoyenne = somme / unites.size();
		}
		
		public void setfrequence(HashMap<String, ngramme> total) {
			int somme = 0;
			for (String code : total.keySet()) {
				somme = somme + total.get(code).getnboccurences();
			}
			this.frequence = this.unites.size() * 10000.0f / somme / 100.0f;
		}
		
		public void addUnites(Unite[] donnees) {
			this.unites.add(donnees);
		}
		
		public void addSuivant(String suivant) {
			this.suivant.add(suivant);
		}
		
		public ngramme() {
			this.unites = new HashSet<>();
			this.frequence = 0.0f;
			this.posmoyenne = 0.0f;
			this.suivant = new ArrayList<>();
		}
		
		public ngramme(HashSet<Unite[]> unites, ArrayList<String> suivant) {
			this.frequence = 0.0f;
			this.unites = unites;
			this.suivant = suivant;
			setposmoyenne();
		}
	}
	
	private HashMap<String, ngramme> collection;
	
	public NgrammesModel() {
		this.collection = new HashMap<>();
	}
	
	public NgrammesModel(HashMap<String, ngramme> collection) {
		this.collection = collection;
	}
	
	public void setallfrequence() {
		for (String code : this.collection.keySet()) {
			this.collection.get(code).setfrequence(this.collection);
		}
	}
	
	public void setallposmoyenne() {
		for (String code : this.collection.keySet()) {
			this.collection.get(code).setposmoyenne();
		}
	}
	
	
	// @require : les unités doivent être classées par ordre d'apparition
	public void ngrammeUpdate(int nbLettre, HashMap<String, Character> legende, String champ, Vue vue, boolean glissant, ModeleChaines donnees, boolean singleton, String typeChamp,
			boolean activeParagraphe) {
		
		ArrayList<Character> suiteLettre = new ArrayList<>();
		String ngrammeCourant;
		Unite[] unites = donnees.getUnites();
		HashMap<String, ngramme> collection = new HashMap<>();
		HashMap<Unite, Integer> noparagraphe = ChaineUtil.unitenumeroparagraphe(unites, vue);
		int pas;
		// on définit le pas de boucle en fonction du choix de l'utilisateur (glissant ou pas glissant)
		if (glissant) {
			pas = 1;
		}
		else {
			pas = nbLettre;
		}
		// on crée la chaîne de lettres
		for (int i = 0; i < unites.length; i++) {
			Schema chaine;
			if (singleton || donnees.getSchemacontenantunite((donnees.getUnites())[i]) != null) {
				switch (typeChamp) {
					case "Chaine":
						chaine = donnees.getSchemacontenantunite(unites[i]);
						suiteLettre.add(chaine == null ? legende.get("singleton")
								: (legende.get(vue.getValeurChamp(
										chaine, champ))) == null ? legende.get("autre") : legende.get(vue.getValeurChamp(chaine, champ)));
						break;
					default:
						suiteLettre.add(legende.get(vue.getValeurChamp(
								unites[i], champ)) == null ? legende.get("autre") : legende.get(vue.getValeurChamp(unites[i], champ)));
						break;
				}
				if (activeParagraphe && i < (unites.length - 1) && noparagraphe.get(unites[i]) < noparagraphe.get(unites[i + 1])) {
					suiteLettre.add('0');
				}
			}
		}
		// on retient le numéro d'unité qui va être lue
		int numeroUnite = 0;
		// on parcours la chaîne pour créer les ngrammes
		for (int i = 0; i < suiteLettre.size() - (nbLettre - 1); i = i + pas) {
			ArrayList<Unite> ajout = new ArrayList<>();
			ngrammeCourant = "";
			int nbUnite = 0;
			// on crée le code constitué de nbLettre lettres
			for (int j = 0; j < nbLettre; j++) {
				ngrammeCourant = ngrammeCourant + suiteLettre.get(j + i);
				// si ce n'est pas un saut de paragraphe alors on incrémente le numéro de l'unité qui va être lue
				if (!suiteLettre.get(j + i).equals('0')) {
					ajout.add(unites[numeroUnite + nbUnite]);
					nbUnite++;
				}
			}
			// on crée le ngramme obtenu
			if (!collection.containsKey(ngrammeCourant)) {
				collection.put(ngrammeCourant, new ngramme());
			}
			(collection.get(ngrammeCourant)).addUnites(
					ajout.toArray(new Unite[0]));
			// on recherche l'éventuel suivant
			if (i < suiteLettre.size() - nbLettre) {
				(collection.get(ngrammeCourant)).addSuivant(suiteLettre.get(i + nbLettre).toString());
			}
			// si la première lettre correspond à une unité et non un suat de paragraphe on incrémente numeroUnite
			if (!suiteLettre.get(i).equals('0')) {
				numeroUnite++;
			}
		}
		this.collection = collection;
		setallfrequence();
		setallposmoyenne();
	}
	
	
	public HashMap<String, ngramme> getCollection() {
		return this.collection;
	}
	
	public Object[][] gettableau(boolean[] ligneSup) {
		Object[][] donnees;
		int nbRowSup = 0;
		for (int i = 0; i < ligneSup.length; i++) {
			if (ligneSup[i]) {
				nbRowSup++;
			}
		}
		donnees = new Object[collection.size() + nbRowSup][4];
		
		int compte = 0;
		for (String unit : collection.keySet()) {
			donnees[compte][0] = unit;
			donnees[compte][1] = collection.get(unit).getnboccurences();
			donnees[compte][2] = collection.get(unit).getfrequence();
			donnees[compte][3] = collection.get(unit).getposmoyenne();
			compte++;
		}
		if (collection.size() > 0) {
			// on ajoute les éventuelles lignes supplémentaires
			switch (this.collection.keySet().iterator().next().length()) {
				// si on a des digrammes
				case 2:
					if (ligneSup[0]) {
						donnees[compte] = calculXX();
						compte++;
					}
					break;
				// si on a des trigrammes
				case 3:
					if (ligneSup[0]) {
						donnees[compte] = calculXOX();
						compte++;
					}
					if (ligneSup[1]) {
						donnees[compte] = calculXOY();
						compte++;
					}
					if (ligneSup[2]) {
						donnees[compte] = calculXYZ();
						compte++;
					}
					if (ligneSup[3]) {
						donnees[compte] = calculXXX();
						compte++;
					}
					break;
				default:
					break;
			}
		}
		return donnees;
		
	}
	
	// @require : le ngrammes contient des digrammes
	public Object[] calculXX() {
		Object[] retour = new Object[4];
		Float frequencecumulee = 0.0f;
		Float positionmoyenne = 0.0f;
		Integer Nboccurences = 0;
		for (String doubletcourant : this.collection.keySet()) {
			if (doubletcourant.charAt(0) == doubletcourant.charAt(1)) {
				frequencecumulee = frequencecumulee + this.collection.get(doubletcourant).getfrequence();
				positionmoyenne = positionmoyenne + this.collection.get(doubletcourant).getnboccurences()
						* this.collection.get(doubletcourant).getposmoyenne();
				Nboccurences = Nboccurences + this.collection.get(doubletcourant).getnboccurences();
			}
		}
		positionmoyenne = Math.round(100 * positionmoyenne / Nboccurences) / 100.0f;
		retour[0] = "xx";
		retour[1] = Nboccurences;
		retour[2] = frequencecumulee;
		retour[3] = positionmoyenne;
		return retour;
	}
	
	public String[] infoSuivant(String code) {
		HashMap<String, Integer> totaux = new HashMap();
		// on recense les lettres suivants le ngramme identifié par "code"
		for (String next : this.collection.get(code).suivant) {
			if (totaux.containsKey(next)) {
				totaux.put(next, totaux.get(next) + 1);
			}
			else {
				totaux.put(next, 1);
			}
		}
		String[] retour = new String[totaux.size()];
		int i = 0;
		// on calcule des statistiques
		for (String next : totaux.keySet()) {
			retour[i] = code + next + "   Nombre d'occurences : " + totaux.get(next).toString() +
					"   Fréquence : " + (Math.round(100.0f * totaux.get(next) / this.collection.get(code).suivant.size()) / 100.0f);
			i = i + 1;
		}
		return retour;
	}
	
	// @require : le ngrammes contient des digrammes
	public Object[] calculXOX() {
		Object[] retour = new Object[4];
		Float frequencecumulee = 0.0f;
		Float positionmoyenne = 0.0f;
		Integer Nboccurences = 0;
		for (String tripletcourant : this.collection.keySet()) {
			if (tripletcourant.charAt(0) == tripletcourant.charAt(2) && tripletcourant.charAt(1) == '0') {
				frequencecumulee = frequencecumulee + this.collection.get(tripletcourant).getfrequence();
				positionmoyenne = positionmoyenne + this.collection.get(tripletcourant).getnboccurences()
						* this.collection.get(tripletcourant).getposmoyenne();
				Nboccurences = Nboccurences + this.collection.get(tripletcourant).getnboccurences();
			}
		}
		positionmoyenne = Math.round(100 * positionmoyenne / Nboccurences) / 100.0f;
		retour[0] = "x0x";
		retour[1] = Nboccurences;
		retour[2] = frequencecumulee;
		retour[3] = positionmoyenne;
		return retour;
	}
	
	// @require : le ngrammes contient des digrammes
	// on recherche les triplets séparés par des paragraphe au extrémités différentes
	public Object[] calculXOY() {
		Object[] retour = new Object[4];
		Float frequencecumulee = 0.0f;
		Float positionmoyenne = 0.0f;
		Integer Nboccurences = 0;
		for (String tripletcourant : this.collection.keySet()) {
			if (tripletcourant.charAt(1) == '0' && tripletcourant.charAt(0) != tripletcourant.charAt(2)) {
				frequencecumulee = frequencecumulee + this.collection.get(tripletcourant).getfrequence();
				positionmoyenne = positionmoyenne + this.collection.get(tripletcourant).getnboccurences()
						* this.collection.get(tripletcourant).getposmoyenne();
				Nboccurences = Nboccurences + this.collection.get(tripletcourant).getnboccurences();
			}
		}
		positionmoyenne = Math.round(100 * positionmoyenne / Nboccurences) / 100.0f;
		retour[0] = "x0y";
		retour[1] = Nboccurences;
		retour[2] = frequencecumulee;
		retour[3] = positionmoyenne;
		return retour;
	}
	
	// @require : le ngrammes contient des digrammes
	public Object[] calculXYZ() {
		Object[] retour = new Object[4];
		Float frequencecumulee = 0.0f;
		Float positionmoyenne = 0.0f;
		Integer Nboccurences = 0;
		for (String tripletcourant : this.collection.keySet()) {
			if (tripletcourant.charAt(1) != tripletcourant.charAt(2) && tripletcourant.charAt(0) != tripletcourant.charAt(1) &&
					tripletcourant.charAt(0) != tripletcourant.charAt(2) && tripletcourant.charAt(1) != '0') {
				frequencecumulee = frequencecumulee + this.collection.get(tripletcourant).getfrequence();
				positionmoyenne = positionmoyenne + this.collection.get(tripletcourant).getnboccurences()
						* this.collection.get(tripletcourant).getposmoyenne();
				Nboccurences = Nboccurences + this.collection.get(tripletcourant).getnboccurences();
			}
		}
		positionmoyenne = Math.round(100 * positionmoyenne / Nboccurences) / 100.0f;
		retour[0] = "xyz";
		retour[1] = Nboccurences;
		retour[2] = frequencecumulee;
		retour[3] = positionmoyenne;
		return retour;
	}
	
	// @require : le ngrammes contient des digrammes
	public Object[] calculXXX() {
		Object[] retour = new Object[4];
		Float frequencecumulee = 0.0f;
		Float positionmoyenne = 0.0f;
		Integer Nboccurences = 0;
		for (String tripletcourant : this.collection.keySet()) {
			if (tripletcourant.charAt(1) == tripletcourant.charAt(2) && tripletcourant.charAt(0) == tripletcourant.charAt(1)) {
				frequencecumulee = frequencecumulee + this.collection.get(tripletcourant).getfrequence();
				positionmoyenne = positionmoyenne + this.collection.get(tripletcourant).getnboccurences()
						* this.collection.get(tripletcourant).getposmoyenne();
				Nboccurences = Nboccurences + this.collection.get(tripletcourant).getnboccurences();
			}
		}
		positionmoyenne = Math.round(100 * positionmoyenne / Nboccurences) / 100.0f;
		retour[0] = "xxx";
		retour[1] = Nboccurences;
		retour[2] = frequencecumulee;
		retour[3] = positionmoyenne;
		return retour;
	}
	
	
}
