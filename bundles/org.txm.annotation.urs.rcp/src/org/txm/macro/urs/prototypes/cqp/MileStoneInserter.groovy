package org.txm.macro.urs.prototypes.cqp

import java.io.IOException

import javax.xml.stream.XMLStreamException

import org.txm.importer.StaxIdentityParser
import org.txm.macro.urs.AnalecUtils
import org.txm.searchengine.cqp.CQPSearchEngine
import org.txm.searchengine.cqp.corpus.MainCorpus
import visuAnalec.elements.Unite

public class MileStoneInserter extends StaxIdentityParser {
	
	List<Unite> units
	String[] ids
	File inputFile
	def id2Units = [:]
	
	public MileStoneInserter(MainCorpus corpus, File inputFile, List<Unite> units) {
		super(inputFile);
		this.inputFile = inputFile;
		
		this.units = units;
		this.units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }
		
		int[] positions = new int[units.size()];
		for( int i = 0 ; i < units.size() ; i++) {
			positions[i] = units.get(i).getDeb();
		}
		
		ids = CQPSearchEngine.getCqiClient().cpos2Str(corpus.getProperty("id").getQualifiedName(), positions);
		
		for (int i = 0 ; i < ids.length ; i++) {
			String id = ids[i]
			if (id != null) {
				if (!id2Units.containsKey(id)) id2Units[id] = []
				
				id2Units[id] << units[i]
			}
		}
	}
	
	boolean start = false;
	String word_id = null;
	@Override
	protected void processStartElement() throws XMLStreamException, IOException {
		
		if ("text".equals(localname)) {
			start = true;
		} else if ("w".equals(localname) && start) {
			word_id = getParserAttributeValue("id");
			if (word_id == null) {
				println "Warning: found <w> without id at line "+parser.getLocation().getLineNumber()+" in "+inputFile
			} else {
				writeAllUnits(word_id, "before")
			}
		}
		
		super.processStartElement();
	}
	
	protected void writeAllUnits(String id, String position) {
		
		def units = id2Units[id]
		if (units == null) return; // no units to write
		
		for (Unite currentUnit : units) {
			
			if (!position.equals(currentUnit.getProp("position"))) return;
				
			writer.writeStartElement(currentUnit.getProp("type"));
			HashMap props = currentUnit.getProps();
			for (String p : props.keySet()) {
				if (p.equals("type")) continue; // ignore the type since written in tag name
				writer.writeAttribute(p, ""+props.get(p));
			}
			writer.writeEndElement();
		}
	}
	
	@Override
	protected void processEndElement() throws XMLStreamException {
		super.processEndElement();
	
		if ("w".equals(localname) && start && word_id != null) {
			writeAllUnits(word_id, "after")
		}
		
		if ("w".equals(localname)) {
			word_id = null;
		}
	}
}