// ChercherRemplacer
// Auteur Matthieu Quignard
// Date : 14 janvier 2019

/**********
Sert à retoucher une valeur attribuée à une mention et la remplacer par une autre
Par exemple : CATEGORIE=PRO.CHECK => CATEGORIE=PRO.PER
ou bien : REF=roi de France => REF=Le Roi de France
ou encore : REF=      => REF=<EMPTY>
***********/

package org.txm.macro.urs.prototypes.misc

import org.apache.commons.lang.*
import org.kohsuke.args4j.*
import groovy.transform.*
import org.txm.*
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.analec.*
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.Message.*
import visuAnalec.donnees.*
import visuAnalec.elements.*
import visuAnalec.vue.*

// CORPS DU SCRIPT

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a Corpus"
	return
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type", usage="Unité", widget="String", required=true, def="Entity")
def unit_type
@Field @Option(name="prop_name", usage="Propriété", widget="String", required=true, def="Property")
def prop_name
@Field @Option(name="val_cherche", usage="Valeur recherchée", widget="String", required=true, def="")
def val_cherche
@Field @Option(name="val_remplace", usage="Valeur de remplacement", widget="String", required=true, def="")
def val_remplace
@Field @Option(name="inclureVides", usage="Inclure les valeurs vides", widget="Boolean", required=true, def="true")
def inclureVides

if (!ParametersDialog.open(this)) return

corpus = corpusViewSelection
analecCorpus = AnalecCorpora.getCorpus(corpus.getName())
vue = AnalecCorpora.getVue(corpus.getName())
structure = analecCorpus.getStructure()

if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Erreur : le corpus ne contient d'unité de type $unit_type"
	println "Script terminé"
	return
}

if (!structure.getUniteProperties(unit_type).contains(prop_name)) { 
	println "Erreur : les unités $unit_type n'ont pas de propriété $prop_name"
	println "Script terminé"
	return
}

println "Option 'inclure les valeurs vides' : $inclureVides"

def nModified = 0
def nIgnored = 0

def units = analecCorpus.getUnites(unit_type)
units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }

for (Unite unit : units) { // process all units
    def val = unit.getProp( prop_name )
    
    if ( (val == val_cherche) || ( inclureVides && (val == "")) ) {
    	vue.setValeurChamp(unit, prop_name, val_remplace)
    	nModified++
    } else {
    	nIgnored++
    }
}



if (nModified > 0) corpus.setIsModified(true);

println "Result:"
println "- $nModified units of type $unit_type have been modified."
println "- $nIgnored units of type $unit_type have not been modified."

AnalecCorpora.getVue(analecCorpus).retablirVueParDefaut()
