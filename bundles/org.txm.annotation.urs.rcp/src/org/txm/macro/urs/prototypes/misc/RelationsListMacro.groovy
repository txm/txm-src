// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden
// STANDARD DECLARATIONS
package org.txm.macro.urs.prototypes.misc

import org.apache.commons.lang.StringUtils;
import org.apache.tools.ant.types.resources.selectors.InstanceOf;
import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.Toolbox;
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.CQPSearchEngine

import visuAnalec.donnees.Structure;
import visuAnalec.elements.Relation
import visuAnalec.elements.Unite;

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a Corpus"
	return;
}

// BEGINNING OF PARAMETERS
@Field @Option(name="relation_type",usage="", widget="String", required=true, def="Relation")
String relation_type

if (!ParametersDialog.open(this)) return;

MainCorpus corpus = corpusViewSelection
AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();
def word = corpus.getWordProperty()
visuAnalec.donnees.Corpus analecCorpus = URSCorpora.getCorpus(corpus);

int n = 1;
def relations = null
if (relation_type.length() > 0) {
	relations = []
	for (String type : analecCorpus.getStructure().getTypes(Relation.class))
		relations.addAll(analecCorpus.getRelations(type))
} else {
	relations = analecCorpus.getToutesRelations()
}

for (Relation relation : relations) {
	def unit1 = relation.getElt1();
	def unit2 = relation.getElt2();
	def props = relation.getProps()
	if (unit1 instanceof Unite && unit2 instanceof Unite) {
		int[] pos1 = null
		if (unit1.getDeb() == unit1.getFin()) pos1 = [unit1.getDeb()]
		else pos1 = (unit1.getDeb()..unit1.getFin())
		def form1 = StringUtils.join(CQI.cpos2Str(word.getQualifiedName(), pos1), " ")
		
		int[] pos2 = null
		if (unit2.getDeb() == unit2.getFin()) pos2 = [unit2.getDeb()]
		else pos2 = (unit2.getDeb()..unit2.getFin())
		def form2 = StringUtils.join(CQI.cpos2Str(word.getQualifiedName(), pos2), " ")
		
		println "$n - $props : $form1 -> $form2"
	} else {
		println "$n - $props"
	}
	n++
}