package org.txm.macro.urs.prototypes.cqp

import java.io.IOException
import java.util.Date
import java.util.LinkedHashMap

import javax.xml.stream.XMLStreamException

import org.txm.Toolbox
import org.txm.importer.StaxIdentityParser
import org.txm.macro.urs.AnalecUtils
import org.txm.scripts.importer.GetAttributeValue
import org.txm.searchengine.cqp.CQPSearchEngine
import org.txm.searchengine.cqp.corpus.MainCorpus
import visuAnalec.elements.Unite

public class WordUnitsInserter extends StaxIdentityParser {

	List<Unite> units
	String[] ids
	File inputFile
	def id2Units = [:]
	def unit_properties // "*" or list (String)
	def unit_internal_properties = false
	def unit_word // ALL START END
	LinkedHashMap<String, String> anaValues = new LinkedHashMap<String, String>();

	public WordUnitsInserter(MainCorpus corpus, File inputFile, List<Unite> units, String unit_properties, String unit_word, boolean unit_internal_properties) {
		super(inputFile)
		this.inputFile = inputFile
		
		this.unit_internal_properties = unit_internal_properties
		this.unit_properties = unit_properties
		this.unit_word = unit_word
		if (!("*".equals(unit_properties))) {
			this.unit_properties = this.unit_properties.split(",")
		}

		this.units = units
		this.units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }

		for (int i = 0 ; i < units.size() ; i++) {
			Unite u = units[i]
			int[] positions = null;
			if ("START".equals(unit_word)) {
				positions = [u.getDeb()] as int[]
			} else if ("END".equals(unit_word)) {
				positions = [u.getFin()] as int[]
			} else {
				positions = u.getDeb()..u.getFin()
			}

			ids = CQPSearchEngine.getCqiClient().cpos2Str(corpus.getProperty("id").getQualifiedName(), positions)

			for (String id : ids) {
				if (!id2Units.containsKey(id)) {
					id2Units[id] = []
				}
				id2Units[id] << u
			}
		}
	}

	boolean inAna = false
	String ana_type, ana_resp, ana_value
	boolean inW = false
	String word_id = null

	protected void processStartElement() throws XMLStreamException, IOException {

		if ("w".equals(localname)) {
			
			inW = true
			word_id = this.getParserAttributeValue("id")

			if (id2Units.containsKey(word_id)) {
				for (Unite u : id2Units[word_id]) {
					def props = u.getProps()
					
					def propNames = props.keySet();
					if (!("*".equals(unit_properties))) {
						propNames = unit_properties
					}				
					
					for (String p : propNames) {
						
						if (!anaValues.containsKey(p)) {
							anaValues[p] = ""
							ana_resp = "#urs"
						}
						anaValues[p] = (anaValues[p]+" "+props.get(p)).trim()
					}
					if (unit_internal_properties) {
	
						anaValues["urs-type"] = u.getType()
						anaValues["urs-start"] = ""+u.getDeb()
						anaValues["urs-end"] = ""+u.getFin()
					}
				}
				if (anaValues.size() > 0) println anaValues
			}

			super.processStartElement(); // write the tag
			
		} else if ("ana".equals(localname) && inW) {
			
			inAna = true
			ana_type = this.getParserAttributeValue("type").substring(1)
			ana_resp = this.getParserAttributeValue("resp")
			ana_value = ""
			
		} else {
			super.processStartElement()
		}
	}

	@Override
	public void processCharacters() throws XMLStreamException {
		if (inAna) {
			ana_value += parser.getText().trim()
		} else {
			super.processCharacters()
		}
	}

	protected void processEndElement() throws XMLStreamException {

		if ("w".equals(localname)) {
			
			// write the last values
			for (String ana_type : anaValues.keySet()) {
				writer.writeStartElement("txm:ana")
				writer.writeAttribute("type", "#" + ana_type)
				writer.writeAttribute("resp", "#txm") // change
				writer.writeCharacters(anaValues[ana_type])
				writer.writeEndElement()
			}
			
			anaValues.clear()
			super.processEndElement() // finally write word then close annotations
			inW = false
			
		} else if ("ana".equals(localname) && inW) {

			if (!anaValues.containsKey(ana_type)) {
				anaValues[ana_type] = ana_value.trim()
			} else {
				ana_resp = "#txm" // set the resp to txm since anaValues update the ana value
				anaValues[ana_type] = (anaValues[ana_type]+" "+ana_value.trim()).trim()
			}

			String value = anaValues[ana_type]

			writer.writeStartElement("txm:ana")
			writer.writeAttribute("type", "#" + ana_type)
			writer.writeAttribute("resp", ana_resp) // change
			writer.writeCharacters(value)
			writer.writeEndElement()
			
			anaValues.remove(ana_type)
			
			inAna = false
			ana_type = null
			ana_resp = null
			ana_value = null

			// write ana later
		} else {
			super.processEndElement()
		}
	}
}