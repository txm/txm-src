// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden
// STANDARD DECLARATIONS
package org.txm.macro.urs.prototypes.misc

import org.apache.commons.lang.StringUtils
import org.txm.rcp.views.corpora.CorporaView
import groovy.transform.Field

import org.kohsuke.args4j.*
import org.txm.Toolbox
import org.txm.annotation.urs.*
import org.txm.lexicaltable.core.statsengine.r.data.LexicalTableImpl
import org.txm.macro.urs.*
import org.txm.rcp.commands.*
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.CQPSearchEngine
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.statsengine.r.core.RWorkspace

import visuAnalec.donnees.*
import visuAnalec.elements.*
import cern.colt.matrix.DoubleFactory2D
import cern.colt.matrix.DoubleMatrix2D

def scriptName = this.class.getSimpleName()
def parent
def selection = []
if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "** $scriptName please select a Corpus to run the macro"
}


// BEGINNING OF PARAMETERS
@Field @Option(name="schema_ursql", usage="TYPE@PROP=VALUE", widget="String", required=false, def="")
		String schema_ursql
@Field @Option(name="minimum_schema_size", usage="Minimum size needed to consider a schema", widget="Integer", required=true, def="3")
		int minimum_schema_size
@Field @Option(name="maximum_schema_size", usage="Maximum size needed to consider a schema", widget="Integer", required=true, def="9999999")
		int maximum_schema_size
@Field @Option(name="unit_ursql", usage="TYPE@PROP=VALUE", widget="String", required=false, def="Entity")
		String unit_ursql
@Field @Option(name="limit_distance_in_schema", usage="Unit distance in schema (0 = no selection, 1 = first after limit, -1 = last before limit, etc.)", widget="Integer", required=true, def="0")
		int limit_distance_in_schema
@Field @Option(name="limit_cql", usage="CQL to build structure limits", widget="Query", required=false, def="<div> [] expand to div")
		limit_cql
@Field @Option(name="strict_inclusion", usage="Units must be strictly included into corpus matches", widget="Boolean", required=true, def="true")
		boolean strict_inclusion
@Field @Option(name="limit_distance", usage="Unit distance to structure limit (0 = no selection, 1 = first after limit, -1 = last before limit, etc.)", widget="Integer", required=true, def="0")
		int limit_distance
@Field @Option(name="unit_prop1", usage="PROP1", widget="String", required=false, def="Property1")
		String unit_prop1
@Field @Option(name="unit_prop2", usage="PROP2", widget="String", required=false, def="Property2")
		String unit_prop2
@Field @Option(name="corr_method", usage="try them all", widget="StringArray", metaVar="pearson	spearman	kendall", required=false, def="pearson")
		String corr_method
@Field @Option(name="corr_style", usage="try them all", widget="StringArray", metaVar="circle	square	ellipse	number	shade	color	pie", required=false, def="number")
		String corr_style
@Field @Option(name="corr_layout", usage="try them all", widget="StringArray", metaVar="full	lower	upper", required=false, def="upper")
		String corr_layout
@Field @Option(name="corr_order", usage="try them all", widget="StringArray", metaVar="AOE	FPC	hclust	alphabet", required=false, def="hclust")
		String corr_order
@Field @Option(name="output_lexicaltable", usage="create or not a lexical table with the result", widget="Boolean", required=true, def="false")
		output_lexicaltable
@Field @Option(name="debug", usage="Show internal variable content", widget="StringArray", metaVar="OFF	ON	ALL	REALLY ALL", required=true, def="OFF")
		debug
if (!ParametersDialog.open(this)) return
	if (debug == "OFF") debug = 0; else if (debug == "ON") debug = 1; else if (debug == "ALL") debug = 2 else if (debug == "REALLY ALL") debug = 3


def CQI = CQPSearchEngine.getCqiClient()

def correlations = [:]
def values1 = new HashSet()
def values2 = new HashSet()
def corpus = corpusViewSelection

mainCorpus = corpus.getMainCorpus()

def word = mainCorpus.getWordProperty()
def analecCorpus = URSCorpora.getCorpus(mainCorpus)



def selectedUnits = AnalecUtils.selectUnitsInSchema(debug, analecCorpus, corpus, schema_ursql, minimum_schema_size, maximum_schema_size,
		unit_ursql, limit_distance_in_schema, limit_cql, strict_inclusion, limit_distance);

if (selectedUnits.size() == 0) {
	println "No unit selected. Aborting."
	return
}
	
for (def unit : selectedUnits) {
	def value1 = unit.getProp(unit_prop1)
	if (value1 == null) value1 = "<null>"
	if (value1.length() == 0) value1 = "<empty>"
	def value2 = unit.getProp(unit_prop2)
	if (value2 == null) value2 = "<null>"
	if (value2.length() == 0) value2 = "<empty>"

	values1 << value1
	values2 << value2

	if (!correlations.containsKey(value1)) correlations[value1] = [:]
	def line = correlations[value1]
	if (!line.containsKey(value2)) line[value2] = 0
	line[value2] += 1
}

def matrix = new int[values1.size()][values2.size()];
println "\t"+values2.join("\t")
int i = 0;
for (def value1 : values1) {
	print value1
	int j = 0;
	for (def value2 : values2) {
		if (correlations[value1][value2] == null) correlations[value1][value2] = 0;
		print "\t"+correlations[value1][value2]

		matrix[i][j] = correlations[value1][value2]
		j++
	}
	println ""
	i++
}

def r = RWorkspace.getRWorkspaceInstance()
r.addVectorToWorkspace("corrlines", values1 as String[])
r.addVectorToWorkspace("corrcols", values2 as String[])
r.addMatrixToWorkspace("corrmatrix", matrix)
r.eval("rownames(corrmatrix) = corrlines")
r.eval("colnames(corrmatrix) = corrcols")

def resultsDir = new File(Toolbox.getTxmHomePath(), "results")
resultsDir.mkdirs()
file = File.createTempFile("txm_corr_pairs_", ".svg", resultsDir)

def title = "${corpus.getMainCorpus()}.${corpus}\n${unit_ursql}"
if (limit_distance > 1) title += "[${limit_distance}]."
if (limit_cql != null && !limit_cql.getQueryString().equals("\"\"")) title += "\n(${limit_cql} limits)"
title += "\t P1=$unit_prop1 P2=unit_prop2"

def plotScript = """

r1 = cor(corrmatrix, use="complete.obs", method="$corr_method");
r2 = cov(corrmatrix, use="complete.obs") ;

corrplot(r1, type="$corr_layout", order="$corr_order", method="$corr_style")
"""


// execute R script
if (!output_lexicaltable) {
	try {
		r.eval("library(corrplot)")
		try {
			r.plot(file, plotScript)
		} catch (Exception e) {
			println "** Error: "+e
		}
	} catch (Exception e) {
		println "** The 'corrplot' R package is not installed. Start R ("+RWorkspace.getExecutablePath()+") and run 'install.packages(\"corrplot\");'."
	}
}
title = "$unit_prop1 $corr_method correlations"


def lt = null;
if (output_lexicaltable) {
	mFactory = DoubleFactory2D.dense
	dmatrix = mFactory.make(values1.size(), values2.size())
	for (int ii = 0 ; ii < values1.size() ; ii++) {
		for (int jj = 0 ; jj < values2.size() ; jj++) {
			dmatrix.set(ii, jj, matrix[ii][jj])
		}
	}
	if (corpusViewSelection instanceof Partition) {
		lt = new LexicalTableImpl(dmatrix, corpusViewSelection, corpusViewSelection.getCorpus().getProperty("word"),
				values1 as String[], values2 as String[])
		lt.setCorpus(corpusViewSelection.getCorpus());
		corpusViewSelection.storeResult(lt)
	} else {
		lt = new LexicalTableImpl(dmatrix, corpus.getProperty("word"),
				values1 as String[], values2 as String[])
		lt.setCorpus(corpus);
		corpus.storeResult(lt)
	}
}

monitor.syncExec(new Runnable() {
			@Override
			public void run() { try {

					if (output_lexicaltable) {
						CorporaView.refreshObject(corpus)
						CorporaView.expand(lt)
					} else {
						OpenBrowser.openfile(file.getAbsolutePath(), "Correlations Units")
					}
				} catch (e) { e.printStackTrace() }}
		})

return correlations