// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden
// STANDARD DECLARATIONS
package org.txm.macro.urs.prototypes.misc

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.corpus.*

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a Corpus"
	return;
}

// BEGINNING OF PARAMETERS
@Field @Option(name="schema_type",usage="", widget="String", required=true, def="Chain")
String schema_type

if (!ParametersDialog.open(this)) return;

MainCorpus corpus = corpusViewSelection
def analecCorpus = URSCorpora.getCorpus(corpus);
def map = new HashMap()
def unitesInSchema = []
def n = 0
for (def schema : analecCorpus.getSchemas(schema_type)) {
	def unites = schema.getUnitesSousjacentes()
	unitesInSchema.addAll(unites)
	n += unites.size()
}

def counts = unitesInSchema.countBy() { it };
for (def c : counts.keySet()) {
	if (counts[c] > 1) println "ERROR UNIT IN MULTIPLE SCHEMA["+c.getDeb()+", "+c.getFin()+"]="+c.getProps()+" in "+c.getSchemas().collect() {it.getProps()}
}

def set = new HashSet()
set.addAll(unitesInSchema)
for (def s : set.collect { it.getType() }) {
	if (!map.containsKey(s)) map[s] = 0;
	map[s] = map[s] +1
}

println "Unites types: "+map.sort() { it -> map[it]}