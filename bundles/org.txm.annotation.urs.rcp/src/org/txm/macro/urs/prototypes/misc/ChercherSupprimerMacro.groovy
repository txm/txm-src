// ChercherSupprimer
// Auteur Matthieu Quignard
// Date : 14 janvier 2019

/**********
Sert à supprimer des mentions qui une valeur particulière attribuée
Par exemple : CATEGORIE=ERREUR
ou bien : REF=NON_REF
ou encore : CHECK=

ATTENTION : CETTE MACRO N'EST PAS REVERSIBLE
***********/

package org.txm.macro.urs.prototypes.misc

import org.apache.commons.lang.*
import org.kohsuke.args4j.*
import groovy.transform.*
import org.txm.*
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.analec.*
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.Message.*
import visuAnalec.donnees.*
import visuAnalec.elements.*
import visuAnalec.vue.*

// CORPS DU SCRIPT

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a Corpus"
	return
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type", usage="Unité", widget="String", required=true, def="Entity")
def unit_type
@Field @Option(name="prop_name", usage="Propriété", widget="String", required=true, def="Property")
def prop_name
@Field @Option(name="val_cherche", usage="Valeur recherchée", widget="String", required=true, def="")
def val_cherche
@Field @Option(name="inclureVides", usage="Inclure les valeurs vides", widget="Boolean", required=true, def="true")
def inclureVides

if (!ParametersDialog.open(this)) return

corpus = corpusViewSelection
analecCorpus = AnalecCorpora.getCorpus(corpus.getName())
vue = AnalecCorpora.getVue(corpus.getName())
structure = analecCorpus.getStructure()

if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Erreur : le corpus ne contient d'unité de type $unit_type"
	println "Script terminé"
	return
}

if (!structure.getUniteProperties(unit_type).contains(prop_name)) { 
	println "Erreur : les unités $unit_type n'ont pas de propriété $prop_name"
	println "Script terminé"
	return
}

println "Option 'inclure les valeurs vides' : $inclureVides"

def nDeleted = 0
def nIgnored = 0

def units = analecCorpus.getUnites(unit_type)
units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }

def garbageBin = []

for (Unite unit : units) { // process all units
    def val = unit.getProp( prop_name )
    
    if ( (val == val_cherche) || ( inclureVides && (val == "")) ) {
    	garbageBin.add( unit )
    	nDeleted++
    } else {
    	nIgnored++
    }
}

// Suppression effective des unités ciblées
garbageBin.each {
   analecCorpus.supUnite( it )
}

if (nDeleted > 0) corpus.setIsModified(true);

println "Result:"
println "- $nDeleted units of type $unit_type have been deleted."
println "- $nIgnored units of type $unit_type have not been modified."

AnalecCorpora.getVue(analecCorpus).retablirVueParDefaut()
