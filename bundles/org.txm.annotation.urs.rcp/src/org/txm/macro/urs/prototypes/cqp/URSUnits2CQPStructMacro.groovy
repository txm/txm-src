// STANDARD DECLARATIONS
package org.txm.macro.urs.prototypes.cqp

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.annotation.urs.*
import org.txm.importer.ValidateXml
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.elements.*

// BEGINNING OF PARAMETERS

// Open the parameters input dialog box
//if (!ParametersDialog.open(this)) return;

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Selection must be a Corpus"
	return
}

MainCorpus corpus = corpusViewSelection
def analecCorpus = URSCorpora.getCorpus(corpus)

def texts = corpus.getCorpusTextIdsList();
def texts_startlimits = corpus.getTextStartLimits()
def texts_endlimits = corpus.getTextEndLimits()

for (int i = 0 ; i < texts.size() ; i++) {

	println "Processing annotations of "+texts[i]+"..."

	def text_id = texts[i]
	def text_start = texts_startlimits[i]
	def text_end = texts_endlimits[i]

	File xmltxmFile = new File(corpus.getProject().getProjectDirectory(), "txm/"+corpus.getID()+"/"+text_id+".xml")
	File xmltxmFileCopy = new File(corpus.getProject().getProjectDirectory(), text_id+"_copy.xml")

	if (!xmltxmFile.exists()) {
		println "Warning: no text file found: "+xmltxmFile
		continue
	}

	// WRITE MILESTONES UNITS

	println "-> MILESTONES UNITS"

	def units = []
	for (String unit_type : analecCorpus.getStructure().getTypes(Unite.class)) {
		def corpus_units = []
		ArrayList<Unite> all_units = analecCorpus.getUnites(unit_type)
		corpus_units = all_units.findAll() {
			it.getProp("type") != null && it.getProp("type").trim().length() > 0 && "yes".equals(it.getProp("milestone")) && text_start <= it.getDeb() && it.getFin() < text_end && !("true".equals(it.getProp("written")))
		}
		units.addAll(corpus_units)
	}

	if (units.size()== 0) {
		println "No milestones to write"
	} else {
		try {
			println "processing milestones Units ${text_id} and its units "+units.size()
			MileStoneInserter inserter = new MileStoneInserter(corpus, xmltxmFile, units);
			if (inserter.process(xmltxmFileCopy) && ValidateXml.test(xmltxmFileCopy)) {
				xmltxmFile.delete()
				xmltxmFileCopy.renameTo(xmltxmFile)

				for (Unite unit : units) {
					unit.getProps()["written"] = "true"
				}
				URSCorpora.saveCorpus(corpus);
				println "Done, "+units.size()+ " milestones written"

			} else {
				println "Error while processing milestones $xmltxmFile file"
				File error = new File(corpus.getProject().getProjectDirectory(), "error/"+xmltxmFile.getName())
				error.getParentFile().mkdirs()
				println "	moving created file to $error"
				error.delete()
				xmltxmFileCopy.renameTo(error)
			}
		} catch(Exception e) {
			println "Error while processing milestones $xmltxmFile file: "+e
			File error = new File(corpus.getProject().getProjectDirectory(), "error/"+xmltxmFile.getName())
			error.getParentFile().mkdirs()
			println "	moving created file to $error"
			error.delete()
			xmltxmFileCopy.renameTo(error)
		}
	}
	// WRITE NON MILESTONES UNITS
	println "-> OTHER UNITS"

	for (String unit_type : analecCorpus.getStructure().getTypes(Unite.class)) {
		
		def corpus_units = []
		ArrayList<Unite> all_units = analecCorpus.getUnites(unit_type)
		corpus_units = all_units.findAll() {
			it.getProp("type") != null && it.getProp("type").trim().length() > 0 && !"no".equals(it.getProp("milestone")) && text_start <= it.getDeb() && it.getFin() < text_end && !("true".equals(it.getProp("written")))
		}

		if (corpus_units.size() == 0) continue;
		
		println "processing Units ${text_id} and its $unit_type units "+corpus_units.size()
		UnitsInserter inserter2 = new UnitsInserter(corpus, xmltxmFile, corpus_units, unit_type);
		try {
			if (inserter2.process(xmltxmFileCopy) && ValidateXml.test(xmltxmFileCopy)) {
				xmltxmFile.delete()
				xmltxmFileCopy.renameTo(xmltxmFile)

				for (Unite unit : corpus_units) {
					unit.getProps()["written"] = "true"
				}
				URSCorpora.saveCorpus(corpus);
				println "Done, "+corpus_units.size()+ " units written"

			} else {
				println "Error while processing milestones $xmltxmFile file"
				File error = new File(corpus.getProject().getProjectDirectory(), "error/"+xmltxmFile.getName())
				error.getParentFile().mkdirs()
				println "	moving created file to $error"
				error.delete()
				xmltxmFileCopy.renameTo(error)
			}
		} catch(Exception e) {
			println "Error while processing milestones $xmltxmFile file: "+e
			File error = new File(corpus.getProject().getProjectDirectory(), "error/"+xmltxmFile.getName())
			error.getParentFile().mkdirs()
			println "	moving created file to $error"
			error.delete()
			xmltxmFileCopy.renameTo(error)
		}
	}
}
