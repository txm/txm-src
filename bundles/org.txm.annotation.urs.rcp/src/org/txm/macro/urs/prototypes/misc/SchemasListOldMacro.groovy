// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden

// STANDARD DECLARATIONS
package org.txm.macro.urs.prototypes.misc

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.Toolbox
import org.txm.rcp.commands.*
import org.apache.commons.lang.StringUtils

// BEGINNING OF PARAMETERS
@Field @Option(name="schema_type",usage="", widget="String", required=true, def="Chain")
String schema_type

@Field @Option(name="minimum_schema_size",usage="", widget="Integer", required=true, def="3")
int minimum_schema_size

@Field @Option(name="schema_property_name",usage="", widget="String", required=false, def="")
String schema_property_name

@Field @Option(name="schema_property_value",usage="", widget="String", required=false, def=".*")
String schema_property_value

@Field @Option(name="unit_type",usage="", widget="String", required=false, def="Entity")
String unit_type

@Field @Option(name="unit_property_name", usage="", widget="String", required=false, def="")
String unit_property_name

@Field @Option(name="unit_property_value", usage="", widget="String", required=false, def=".*")
String unit_property_value

@Field @Option(name="word_property", usage="", widget="StringArray", metaVar="word	lemma	frlemma	frolemma	#forme#	id", required=false, def="word")
String word_property

@Field @Option(name="separator", usage="", widget="String", required=true, def=", ")
String separator

@Field @Option(name="buildCQL", usage="générer la requête des unités", widget="Boolean", required=true, def='false')
def buildCQL

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpus view selection is not a Corpus"
	return;
}

if (!ParametersDialog.open(this)) return;
// END OF PARAMETERS

MainCorpus corpus = corpusViewSelection
def analecCorpus = URSCorpora.getCorpus(corpus)

// check Schema parameters
if (!analecCorpus.getStructure().getSchemas().contains(schema_type)) {
	println "No schema with name=$schema_type"
	return;
} else {
	if (schema_property_name.length() > 0 && schema_property_value.length() > 0) {
		// test property existance
		def props = analecCorpus.getStructure().getSchemaProperties(schema_type);
		if (!props.contains(schema_property_name)) {
			println "Schema $schema_type has no property named $schema_property_name"
			return;
		}
	}
}

// check unit parameters
if (!analecCorpus.getStructure().getUnites().contains(unit_type)) {
	println "No unit with name=$unit_type"
	return;
} else {
	if (unit_property_name.length() > 0 && unit_property_value.length() > 0) {
		// test property existance
		def props = analecCorpus.getStructure().getUniteProperties(unit_type);
		if (!props.contains(unit_property_name)) {
			println "Unit $unit_type has no property named $unit_property_name"
			return;
		}
	}
}

def CQI = CQPSearchEngine.getCqiClient()

if (buildCQL) {
	word_prop = corpus.getProperty("id")
} else {
	word_prop = corpus.getProperty(word_property)
}

def schemas = analecCorpus.getSchemas(schema_type)
schemas.sort() {it.getProps()}
def nSchemas = 0

def lens = [:]
for (def schema : schemas) {

	if (schema_property_name.length() > 0 && schema_property_value.length() > 0) {
		if (!schema.getProp(schema_property_name).matches(schema_property_value)) {
			// ignoring this schema
			continue
		}
	}
	
	def nUnites = 0
	for (def unit : schema.getUnitesSousjacentes()) {
		if (unit_type.length() > 0) {
			if (!unit.getType().equals(unit_type)) {
				continue
			}
		}
		
		if (unit_property_name.length() > 0 && unit_property_value.length() > 0) {
			if (!unit.getProp(unit_property_name).matches(unit_property_value)) {
				// ignoring this schema
				continue
			}
		}

		nUnites++
	}

	if (nUnites < minimum_schema_size) continue

	print schema.getProps().toString()+ ": "
	def first = true
	for (def unit : schema.getUnitesSousjacentes()) {
		if (unit_type.length() > 0) {
			if (!unit.getType().equals(unit_type)) {
				continue
			}
		}
		
		if (unit_property_name.length() > 0 && unit_property_value.length() > 0) {
			if (!unit.getProp(unit_property_name).matches(unit_property_value)) {
				// ignoring this schema
				continue
			}
		}

		String forme =  null;

		if (buildCQL) {
			int[] pos = null
			if (unit.getDeb() == unit.getFin()) pos = [unit.getDeb()]
			else pos = (unit.getDeb()..unit.getFin())
			def first2= true
			q = ""
			pos.each {
				if (first2) { first2 = false } else { q = q+" " }
				int[] pos2 = [it]
				q = q+"["+word_prop+"=\""+CQI.cpos2Str(word_prop.getQualifiedName(), pos2)[0]+"\"]"
			}
			if (first) { first = false } else { print "|" }
			print "("+q+")"
		} else {
			if (word_prop == null) { // word_property is the analec unit property to use
			forme = unit.getProp(word_property)
			} else {
			int[] pos = null
			if (unit.getDeb() == unit.getFin()) pos = [unit.getDeb()]
			else pos = (unit.getDeb()..unit.getFin())
				
			forme = StringUtils.join(CQI.cpos2Str(word_prop.getQualifiedName(), pos), " ") // ids is enough
			}

			if (first) { first = false } else { print separator }
			print forme
		}
	}
	println ""

	nSchemas++
}
