// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden
// STANDARD DECLARATIONS
package org.txm.macro.urs.prototypes.misc

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.corpus.*

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a Corpus"
	return;
}

MainCorpus corpus = corpusViewSelection
def analecCorpus = URSCorpora.getCorpus(corpus);

def schemas = analecCorpus.getTousSchemas()
def set = new HashMap()
for (def s : schemas.collect { it.getType() }) {
	if (!set.containsKey(s)) set[s] = 0;
	set[s] = set[s] +1
}
println "Schemas types: "+set.sort() { it -> set[it]}