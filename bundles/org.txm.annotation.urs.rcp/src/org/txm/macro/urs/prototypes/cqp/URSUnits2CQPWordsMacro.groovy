package org.txm.macro.urs.prototypes.cqp

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.annotation.urs.*
import org.txm.importer.ValidateXml
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.elements.*

// BEGINNING OF PARAMETERS

// Open the parameters input dialog box
//if (!ParametersDialog.open(this)) return;

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Selection must be a Corpus"
	return
}

@Field @Option(name="unit_type", usage="Corpus name in uppercase", widget="String", required=true, def="Entity")
		String unit_type

@Field @Option(name="unit_properties", usage=" * or comma separated list of properties names", widget="String", required=true, def="Property")
		String unit_properties
		
@Field @Option(name="unit_internal_properties", usage="Corpus name in uppercase", widget="Boolean", required=true, def="false")
def unit_internal_properties

@Field @Option(name="unit_word", usage="CQP word position to annotate", widget="StringArray", metaVar="START	END", required=true, def="START")
		String unit_word

if (!ParametersDialog.open(this)) return

def corpus = corpusViewSelection
def analecCorpus = URSCorpora.getCorpus(corpus)

def texts = corpus.getCorpusTextIdsList();
def texts_startlimits = corpus.getTextStartLimits()
def texts_endlimits = corpus.getTextEndLimits()

for (int i = 0 ; i < texts.size() ; i++) {
	
	println "Processing annotations of "+texts[i]+"..."
	
	def text_id = texts[i]
	def text_start = texts_startlimits[i]
	def text_end = texts_endlimits[i]
	
	File xmltxmFile = new File(corpus.getProject().getProjectDirectory(), "txm/"+corpus.getID()+"/"+text_id+".xml")
	File xmltxmFileCopy = new File(corpus.getProject().getProjectDirectory(), text_id+"_copy.xml")
	
	if (!xmltxmFile.exists()) {
		println "Warning: no text file found: "+xmltxmFile
		continue
	}
	
	def corpus_units = []
	ArrayList<Unite> all_units = analecCorpus.getUnites(unit_type)
	corpus_units = all_units.findAll() {
		text_start <= it.getDeb() && it.getFin() < text_end
	}
	
	if (corpus_units.size() == 0) {
		println " No '$unit_type' unit found."
		continue;
	}
	
	try {
		println " Processing word Units ${text_id} and its '$unit_type' units ("+corpus_units.size()+")"
		WordUnitsInserter inserter = new WordUnitsInserter(corpus, xmltxmFile, corpus_units, unit_properties, unit_word, unit_internal_properties);
		if (inserter.process(xmltxmFileCopy) && ValidateXml.test(xmltxmFileCopy)) {
			xmltxmFile.delete()
			xmltxmFileCopy.renameTo(xmltxmFile)
			
			println "Done, "+corpus_units.size()+ " units written"
			
		} else {
			println "Error while processing the XML-TXM $xmltxmFile file"
			File error = new File(corpus.getProject().getProjectDirectory(), "error/"+xmltxmFile.getName())
			error.getParentFile().mkdirs()
			println "	moving created file to $error"
			error.delete()
			xmltxmFileCopy.renameTo(error)
		}
	} catch(Exception e) {
		println "Error while processing milestones $xmltxmFile file: "+e
		e.printStackTrace();
		File error = new File(corpus.getProject().getProjectDirectory(), "error/"+xmltxmFile.getName())
		error.getParentFile().mkdirs()
		println "	moving created file to $error"
		error.delete()
		xmltxmFileCopy.renameTo(error)
	}
}