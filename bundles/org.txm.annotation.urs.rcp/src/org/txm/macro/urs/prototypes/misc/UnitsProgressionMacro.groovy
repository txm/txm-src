// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden
// STANDARD DECLARATIONS
package org.txm.macro.urs.prototypes.misc

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils
import org.jfree.chart.JFreeChart
import org.jfree.chart.plot.XYPlot
import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.Toolbox
import org.txm.progression.core.chartsengine.jfreechart.themes.highcharts.renderers.ProgressionItemSelectionRenderer
import org.txm.progression.core.functions.Progression
import org.txm.progression.rcp.editors.ProgressionEditor
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.chartsengine.rcp.editors.ChartEditor
import org.txm.chartsengine.rcp.*
import org.txm.macro.urs.AnalecUtils
import org.txm.searchengine.cqp.AbstractCqiClient
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.corpus.query.CQLQuery
import org.txm.rcp.Application
import org.txm.rcp.IImageKeys
import org.txm.rcp.editors.TXMEditor

import visuAnalec.donnees.Structure
import visuAnalec.elements.*

def scriptName = this.class.getSimpleName()
def parent
def selection = []
if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "** $scriptName please select a Corpus to run the macro"
}
selection << corpusViewSelection
parent = corpusViewSelection

// BEGINNING OF PARAMETERS
@Field @Option(name="schema_ursql", usage="TYPE@PROP=VALUE", widget="String", required=false, def="")
		String schema_ursql
@Field @Option(name="minimum_schema_size", usage="Minimum size needed to consider a schema", widget="Integer", required=true, def="3")
		int minimum_schema_size
@Field @Option(name="maximum_schema_size", usage="Maximum size needed to consider a schema", widget="Integer", required=true, def="9999999")
		int maximum_schema_size
@Field @Option(name="unit_ursql", usage="TYPE@PROP=VALUE", widget="String", required=false, def="Entity")
		String unit_ursql
@Field @Option(name="limit_distance_in_schema", usage="Unit distance in schema (0 = no selection, 1 = first after limit, -1 = last before limit, etc.)", widget="Integer", required=true, def="0")
		int limit_distance_in_schema
@Field @Option(name="limit_cql", usage="CQL to build structure limits", widget="Query", required=true, def="<div> [] expand to div")
		limit_cql
@Field @Option(name="strict_inclusion", usage="Units must be strictly included into corpus matches", widget="Boolean", required=true, def="true")
		boolean strict_inclusion
@Field @Option(name="limit_distance", usage="Unit distance to structure limit (0 = no selection, 1 = first after limit, -1 = last before limit, etc.)", widget="Integer", required=true, def="0")
		int limit_distance
@Field @Option(name="unit_property_display", usage="Unit property to count", widget="String", required=true, def="Property")
		String unit_property_display
@Field @Option(name="struct_name", usage="Structure to display", widget="String", required=true, def="div")
		String struct_name
@Field @Option(name="struct_prop", usage="Structure property to display", widget="String", required=true, def="n")
		String struct_prop
@Field @Option(name="line_width", usage="line width", widget="Integer", required=true, def="1")
		int line_width = 2
@Field @Option(name="bande_width", usage="bande width", widget="Float", required=true, def="1.0f")
		float bande_width = 1.0f
@Field @Option(name="debug", usage="Show internal variable content", widget="StringArray", metaVar="OFF	ON	ALL	REALLY ALL", required=true, def="OFF")
		debug
if (!ParametersDialog.open(this)) return
if (debug == "OFF") debug = 0; else if (debug == "ON") debug = 1; else if (debug == "ALL") debug = 2 else if (debug == "REALLY ALL") debug = 3


	def CQI = CQPSearchEngine.getCqiClient()

def queries = []
def queryResults = []
def informations = []
for (def corpus : selection) {
	
	mainCorpus = corpus.getMainCorpus()

	def word = mainCorpus.getWordProperty()
	def analecCorpus = URSCorpora.getCorpus(mainCorpus.getName())

	def selectedUnits = AnalecUtils.selectUnitsInSchema(debug, analecCorpus, corpus, schema_ursql, minimum_schema_size, maximum_schema_size, 
	unit_ursql, limit_distance_in_schema, limit_cql, strict_inclusion, limit_distance);

	def query = ""
	if (limit_cql != null && !limit_cql.getQueryString().equals("\"\"")) query += limit_cql
	if (schema_ursql != null && schema_ursql.length() > 0) { if (query.length() > 0) query += " & "; query += ""+schema_ursql+ " >"}
	if (unit_ursql != null && unit_ursql.length() > 0) query += " "+unit_ursql
	query = new CQLQuery(query)
	int[] starts = new int[selectedUnits.size()];
	int[] ends = new int[selectedUnits.size()];
	def unitsinformations = []
	int n = 0;
	for (Unite unite : selectedUnits) {
		starts[n] = unite.getDeb()
		ends[n] = unite.getFin()
		unitsinformations << AnalecUtils.toString(CQI, word, unite);
		n++
	}
	def queryResult = new FakeQueryResult(corpus.getID(), corpus, query, starts, ends, null)
	queries << query
	queryResults << queryResult
	informations << unitsinformations

	if (unit_property_display != null && unit_property_display.length() > 0) {
		def propvalues = [:]
		for (def unit : selectedUnits) {
			def v = unit.getProp(unit_property_display)
			if (v == null) v = "<null>"
			else if (v.length() == 0) v = "<empty>"
			
			if (!propvalues.containsKey(v))propvalues[v] = []
			propvalues[v] << unit
		}
		
		for (def v : propvalues.keySet().sort()) {
			selectedUnits = propvalues[v]
			query = corpus.getID()+" "+limit_cql
			query = new CQLQuery(v)
			starts = new int[selectedUnits.size()];
			ends = new int[selectedUnits.size()];
			unitsinformations = []
			n = 0;
			for (Unite unite : selectedUnits) {
				starts[n] = unite.getDeb()
				ends[n] = unite.getFin()
				unitsinformations << AnalecUtils.toString(CQI, word, unite);
				n++
			}
			queryResult = new FakeQueryResult(corpus.getID(), corpus, query, starts, ends, null)
			queries << query
			queryResults << queryResult
			informations << unitsinformations
		}
	}
}

corpus = parent
try {
	def struct = corpus.getStructuralUnit(struct_name)
	def struct_p = struct.getProperty(struct_prop)

	Progression progression = new Progression(corpus, queries,
			struct, struct_p,	".*",
			true, false, false,
			line_width, false, bande_width)

	progression.stepQueries(queryResults); // new

	if (!progression.stepStructuralUnits() || monitor.isCanceled())	return
		monitor.worked(20)
	if (!progression.stepFinalize() || monitor.isCanceled()) return
		monitor.worked(20)

	monitor.syncExec(new Runnable() {
				@Override
				public void run() {
					try {
						progression.compute();
						TXMEditor.openEditor(progression, ProgressionEditor.class.getName())
//						ChartEditor charteditorpart = SWTChartsComponentProvider.openEditor(
//							Application.swtComponentProvider.createProgressionChartEditorPart(
//								IImageKeys.getImage(IImageKeys.ACTION_PROGRESSION), progression, progression.isMonochrome(), progression.isMonostyle(), progression.isDoCumulative()))
						JFreeChart chart = progression.getChart()
						def plot = chart.getXYPlot()
						ProgressionItemSelectionRenderer renderer = plot.getRenderer();
						renderer.setAdditionalLabelInformation(informations)
					} catch(Exception e) {e.printStackTrace()}
				}
			})

} catch(Exception e) {
	e.printStackTrace()
	return false
}
