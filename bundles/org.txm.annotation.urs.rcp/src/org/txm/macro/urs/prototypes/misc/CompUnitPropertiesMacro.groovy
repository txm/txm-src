// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden
// STANDARD DECLARATIONS
package org.txm.macro.urs.prototypes.misc

import org.apache.commons.lang.StringUtils;
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.Toolbox;
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.CQPSearchEngine
import visuAnalec.donnees.Structure;
import visuAnalec.elements.Unite;

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a Corpus"
	return;
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type",usage="", widget="String", required=true, def="Entity")
String unit_type

@Field @Option(name="print_diff",usage="", widget="Boolean", required=true, def="true")
boolean print_diff

@Field @Option(name="unit_property_name1", usage="", widget="String", required=false, def="Property1")
String unit_property_name1

@Field @Option(name="unit_property_name2", usage="", widget="String", required=false, def="Property2")
String unit_property_name2

if (!ParametersDialog.open(this)) return;

int n = 1;
int nDiff = 0;
MainCorpus corpus = corpusViewSelection
AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();
def word = corpus.getWordProperty()
def analecCorpus = URSCorpora.getCorpus(corpus);

def units = analecCorpus.getUnites(unit_type)
units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }
for (Unite unit : units) {
	int[] pos = null
	if (unit.getDeb() == unit.getFin()) pos = [unit.getDeb()]
	else pos = (unit.getDeb()..unit.getFin())
	def form = StringUtils.join(CQI.cpos2Str(word.getQualifiedName(), pos), " ")
	def props = unit.getProps()
	def v1 = props.get(unit_property_name1);
	def v2 = props.get(unit_property_name2);
	
	if (v1 != v2) {
		if (print_diff) println "$n - ${unit.getDeb()} -> ${unit.getFin()} - $props : $form"
		nDiff++
	}
	n++
}

if (nDiff == 0) println "$unit_property_name1 and $unit_property_name2 have the same values."
else println "$unit_property_name1 and $unit_property_name2 have $nDiff/$n different values."