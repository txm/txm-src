// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden
// STANDARD DECLARATIONS
package org.txm.macro.urs.prototypes.misc

import org.apache.commons.lang.StringUtils
import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.Toolbox
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.macro.urs.AnalecUtils
import org.txm.searchengine.cqp.AbstractCqiClient
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.CQLQuery

import visuAnalec.donnees.Structure
import visuAnalec.elements.*

def scriptName = this.class.getSimpleName()

def selection = []
for (def s : corpusViewSelections) {
	if (s instanceof CQPCorpus) selection << s
	else if (s instanceof Partition) selection.addAll(s.getParts())
}

if (selection.size() == 0) {
	println "** $scriptName: please select a Corpus or a Partition in the Corpus view: "+corpusViewSelections
	return false
}

// BEGINNING OF PARAMETERS
@Field @Option(name="schema_ursql", usage="TYPE@PROP=VALUE", widget="String", required=true, def="")
		String schema_ursql
@Field @Option(name="minimum_schema_size", usage="Minimum size needed to consider a schema", widget="Integer", required=true, def="3")
		int minimum_schema_size
@Field @Option(name="unit_ursql", usage="TYPE@PROP=VALUE", widget="String", required=false, def="Entity")
		String unit_ursql
@Field @Option(name="limit_cql", usage="CQL to build structure limits", widget="Query", required=true, def="<div> [] expand to div")
		limit_cql
@Field @Option(name="strict_inclusion", usage="Units must be strictly included into corpus matches", widget="Boolean", required=true, def="true")
		boolean strict_inclusion
@Field @Option(name="limit_distance", usage="Unit distance to structure limit (0 = no selection, 1 = first after limit, -1 = last before limit, etc.)", widget="Integer", required=true, def="0")
		int limit_distance
@Field @Option(name="debug", usage="Show internal variable content", widget="Boolean", required=true, def="false")
		debug
if (!ParametersDialog.open(this)) return

	def CQI = CQPSearchEngine.getCqiClient()

//corpus = corpusViewSelection
for (def corpus : selection) {

	mainCorpus = corpus.getMainCorpus()

	def word = mainCorpus.getWordProperty()
	def analecCorpus = URSCorpora.getCorpus(mainCorpus.getName())

	def selectedUnits = AnalecUtils.selectUnitsInSchema(debug, analecCorpus, corpus, schema_ursql, minimum_schema_size, Integer.MAX_VALUE,
			unit_ursql, limit_cql, strict_inclusion, limit_distance);

	for (def unit : selectedUnits) {
		def props = unit.getProps();
		for (def k : props.keySet()) {
			if (props[k] == null) {
				println "$corpus\t"+unit.getDeb()+"->"+unit.getFin()+"\t"+k
			}
		}
	}
}
