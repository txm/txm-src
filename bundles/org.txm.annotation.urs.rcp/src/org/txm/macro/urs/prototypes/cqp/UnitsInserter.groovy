package org.txm.macro.urs.prototypes.cqp

import java.io.IOException

import javax.xml.stream.XMLStreamException

import org.txm.importer.StaxIdentityParser
import org.txm.macro.urs.AnalecUtils
import org.txm.searchengine.cqp.CQPSearchEngine
import org.txm.searchengine.cqp.corpus.MainCorpus
import visuAnalec.elements.Unite

public class UnitsInserter extends StaxIdentityParser {

	List<Unite> units
	String[] ids
	File inputFile
	def open_id2Units = [:]
	def close_id2Units = [:]
	def writing_units = []
	def writing_stacks = []
	
	def stack = "";

	def writing_start, writing_end;
	
	def positions2id = [:] // used to relocate end of units

	public UnitsInserter(MainCorpus corpus, File inputFile, List<Unite> units, String type) {
		super(inputFile);
		this.inputFile = inputFile;
		this.units = units;
		
		this.units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: b.getFin() <=> a.getFin() }

		// get words id limits
		int[] positions = new int[units.size()];
		for( int i = 0 ; i < units.size() ; i++) {
			positions[i] = units.get(i).getDeb();
		}
		ids = CQPSearchEngine.getCqiClient().cpos2Str(corpus.getProperty("id").getQualifiedName(), positions);
		for (int i = 0 ; i < ids.length ; i++) {
			String id = ids[i]
			
			if (id != null) {
				positions2id[positions[i]] = id
				if (!open_id2Units.containsKey(id)) open_id2Units[id] = []
				open_id2Units[id] << units[i]
			}
		}

		positions = new int[units.size()];
		for( int i = 0 ; i < units.size() ; i++) {
			positions[i] = units.get(i).getFin();
		}
		ids = CQPSearchEngine.getCqiClient().cpos2Str(corpus.getProperty("id").getQualifiedName(), positions);
		for (int i = 0 ; i < ids.length ; i++) {
			String id = ids[i]
			if (id != null) {
				positions2id[positions[i]] = id
				if (!close_id2Units.containsKey(id)) close_id2Units[id] = new HashSet<Unite>()
				close_id2Units[id] << units[i]
			}
		}
	}

	boolean start = false;
	String word_id = null;
	@Override
	protected void processStartElement() throws XMLStreamException, IOException {

		stack += "/"+localname

		if ("text".equals(localname)) {
			start = true;
		} else if ("w".equals(localname) && start) {
			word_id = getParserAttributeValue("id");
			if (word_id == null) {
				println "Warning: found <w> without id at line "+parser.getLocation().getLineNumber()+" in "+inputFile
			} else {
				writeOpenUnits()
			}
		}

		super.processStartElement();
	}

	protected void writeOpenUnits() {
		
		def toWrite = open_id2Units[word_id]
		
		if (toWrite != null) {
			for (Unite unite : toWrite) {
				
				for (int i = 0 ; i < writing_units.size() ; i++) {
					Unite u = writing_units.get(i);
					if (unite.getFin() > u.getFin()) {
						// add unite to close_id2Units
						String id = positions2id[u.getFin()]
						close_id2Units[id] << unite // close the unite at the same moment
					}
				}
				
				writeUnit(unite);
			}
		}
	}

	protected void writeCloseUnits() {
		if (word_id != null) {
			def toClose = close_id2Units[word_id]
			if (toClose != null) {
				for (int i = 0 ; i < writing_units.size() ; i++) {
					Unite u = writing_units.get(i);
					if (toClose.contains(u)) {
						writing_stacks.remove(i)
						writing_units.remove(i)
						writer.writeEndElement();
						i--;
					}
				}
			}
		} else {
			for (int i = 0 ; i < writing_stacks.size() ; i++) {
				if (writing_stacks[i].equals(stack)) {
					writing_stacks.remove(i)
					writing_units.remove(i)
					writer.writeEndElement();
					i--
				}
			}
		}
	}

	protected void writeUnit(Unite currentUnit) {

		writing_units << currentUnit
		writing_stacks << currentUnit
		if (currentUnit.getDeb() > writing_start)

		writer.writeStartElement(currentUnit.getProp("type"));
		HashMap props = currentUnit.getProps();
		for (String p : props.keySet()) {
			if (p.equals("type")) continue; // ignore the type since written in tag name
			writer.writeAttribute(p, ""+props.get(p));
		}
	}

	@Override
	protected void processEndElement() throws XMLStreamException {
		
//		println "writing_stacks=$writing_stacks"
//		println "stack=$stack"
		
		if (writing_stacks.size() > 0 && writing_stacks[-1].equals(stack)) {
			writeCloseUnits()
		}

		super.processEndElement();
		
		stack = stack.substring(0, stack.length() - localname.length() - 1);
		
		if ("w".equals(localname)) {
			if (start && word_id != null) {
				writeCloseUnits()
			}
			word_id = null;
		}
	}
}