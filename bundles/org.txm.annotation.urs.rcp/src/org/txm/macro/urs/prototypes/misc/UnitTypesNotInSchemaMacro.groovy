// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden
// STANDARD DECLARATIONS
package org.txm.macro.urs.prototypes.misc

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.corpus.*

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a Corpus"
	return;
}

// BEGINNING OF PARAMETERS
@Field @Option(name="schema_type",usage="", widget="String", required=true, def="Chain")
String schema_type
if (!ParametersDialog.open(this)) return;

MainCorpus corpus = corpusViewSelection
def analecCorpus = URSCorpora.getCorpus(corpus);

def unitesInSchema = new HashSet()
for (def schema : analecCorpus.getSchemas(schema_type)) {
	unitesInSchema.addAll(schema.getUnitesSousjacentes())
}
println "unites: "+analecCorpus.getToutesUnites().size()
println "unites in schema: "+unitesInSchema.size()

def set = new HashMap()
for (def u : analecCorpus.getToutesUnites()) {
	if (unitesInSchema.contains(u)) continue;
	
	if (!set.containsKey(u.getType())) set[u.getType()] = 0;
	set[u.getType()] = set[u.getType()] +1
}

println "unites not in schema: "+set.sort() { it -> set[it]}