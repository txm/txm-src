Macro ChercherRemplacer
Auteur : Matthieu QUIGNARD
Version : 05 Février 2019

Retouche la valeur d'une propriété pour la remplacer par une autre.
Par exemple : CATEGORIE=PRO.CHECK => CATEGORIE=ERREUR

Possibilité d'inclure aussi les mentions dont la valeur initiale est vide.
Par exemple : CATEGORIE=          => CATEGORIE=ERREUR

NB : on peut utiliser cette macro pour retoucher le nom des référents.
Exemple : REF=roi de france       => REF=Le Roi de France

