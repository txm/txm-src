// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden
// STANDARD DECLARATIONS
package org.txm.macro.urs.export

import groovy.transform.Field

import org.jfree.chart.JFreeChart
import org.kohsuke.args4j.*
import org.txm.Toolbox
import org.txm.annotation.urs.*
import org.txm.macro.urs.AnalecUtils
import org.txm.rcp.Application
import org.txm.rcp.IImageKeys
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.CQLQuery
import org.txm.utils.io.FileCopy;
import org.txm.utils.io.IOUtils
import org.txm.utils.zip.Zip

import visuAnalec.elements.*

def scriptName = this.class.getSimpleName()
if (!(corpusViewSelection instanceof MainCorpus)) {
	println "** $scriptName please select a MainCorpus to run the macro"
	return;
}

@Field @Option(name="new_name", usage="Corpus name in uppercase", widget="String", required=true, def="CORPUSNAME")
String new_name
if (!ParametersDialog.open(this)) return

new_name = new_name.toUpperCase()
def pattern = "[A-Z][-A-Z0-9]{1,20}"
if (!new_name.matches(pattern)) {
	println "New corpus name not conformant to CQP corpus name: "+pattern
	return false
}

MainCorpus mainCorpus = corpusViewSelection.getMainCorpus()
String name = mainCorpus.getName()
if (mainCorpus.isModified()) {
	println "Selected corpus is not saved. Aborting"
	return false
}

visuAnalec.donnees.Corpus analecCorpus = URSCorpora.getCorpus(mainCorpus)
if (analecCorpus.isModifie()) {
	println "Selected Analec corpus is not saved. Aborting"
	return false
}

File binDirectory = mainCorpus.getProjectDirectory()
String binName = binDirectory.getName()


File newBinDirectory = new File(binDirectory.getParentFile(), new_name)

if (newBinDirectory.exists()) {
	println "The new corpus directory already exists: $newBinDirectory. Aborting."
	return false
}

FileCopy.copyFiles(binDirectory, newBinDirectory)
if (!newBinDirectory.exists()) {
	println "Fail to copy binary directory $binDirectory to $newBinDirectory"
	return
}

File ecFile = new File(newBinDirectory, "analec/${name}.ec")
File ecvFile = new File(newBinDirectory, "analec/${name}.ecv")
File cssFile = new File(newBinDirectory, "css/${name}.css")
File dataFile = new File(newBinDirectory, "data/${name}")
File htmlFile = new File(newBinDirectory, "HTML/${name}")
File defaultCSSFile = new File(newBinDirectory, "HTML/${name}/default/css/${name}.css")
File registryFile = new File(newBinDirectory, "registry/${name.toLowerCase()}")
File txmFile = new File(newBinDirectory, "txm/${name}")

File ecFile2 = new File(newBinDirectory, "analec/${new_name}.ec")
File ecvFile2 = new File(newBinDirectory, "analec/${new_name}.ecv")
File cssFile2 = new File(newBinDirectory, "css/${new_name}.css")
File dataFile2 = new File(newBinDirectory, "data/${new_name}")
File htmlFile2 = new File(newBinDirectory, "HTML/${new_name}")
File defaultCSSFile2 = new File(newBinDirectory, "HTML/${new_name}/default/css/${new_name}.css")
File registryFile2 = new File(newBinDirectory, "registry/${new_name.toLowerCase()}")
File txmFile2 = new File(newBinDirectory, "txm/${new_name}")

println "renaming $ecFile : "+ecFile.renameTo(ecFile2)
println "renaming $ecvFile : "+ecvFile.renameTo(ecvFile2)
println "renaming $cssFile : "+cssFile.renameTo(cssFile2)
println "renaming $dataFile : "+dataFile.renameTo(dataFile2)
println "renaming $htmlFile : "+htmlFile.renameTo(htmlFile2)
println "renaming $defaultCSSFile : "+defaultCSSFile.renameTo(defaultCSSFile2)
println "renaming $registryFile : "+registryFile.renameTo(registryFile2)
println "renaming $txmFile : "+txmFile.renameTo(txmFile2)

// patch name in settings
println "replacing old name $name ->  ${new_name} in preferences"
File settingsDirectory = new File(newBinDirectory, ".settings")
for (File prefFile : settingsDirectory.listFiles()) {
	IOUtils.write(prefFile, prefFile.getText().replace(name, new_name))
}

File projectSetting = new File(newBinDirectory, ".project")
IOUtils.write(projectSetting, projectSetting.getText().replace(name, new_name))

// patch registry
String oldcontent = registryFile2.getText();
content = oldcontent.replace(name, new_name)
content = content.replace(name.toLowerCase(), new_name.toLowerCase())
registryFile2.withWriter { writer -> 
	writer.write(content)
}

println "creating the TXM file..."
File zipFile = new File(newBinDirectory.getAbsolutePath()+".txm")
Zip.compress(newBinDirectory, zipFile, monitor)

if (!zipFile.exists()) {
	println "Fail to zip binary directory $binDirectory to $zipFile"
	return
} else {
	println "Done: $zipFile"
}