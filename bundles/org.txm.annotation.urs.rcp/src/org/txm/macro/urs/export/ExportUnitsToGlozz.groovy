package org.txm.macro.urs.export

// @author: Bruno Oberlé
// v1.0.0 2017-08-28

/*
 Cette macro exporte le corpus sélectionné et ses annotations vers deux fichiers de format Glozz:
 - un fichier .ac contenant le corpus brut,
 - un fichier .aa contenant les annotations au format XML utilisé par Glozz.
 Le corpus sélectionné dans TXM devrait contenir une structure Analec avec au moins un type d'unité défini (e.g. MENTION, maillon, etc.). S'il n'y a pas de structure,
 ce n'est pas grave: le fichier est exporter, mais aucune annotation n'est créer. Cela permet d'exporter n'importe quel corpus au format Glozz.
 Pour exporter un texte au format Glozz *sans* les annotations qu'il contient, simplement mettre un unit_type qui n'existe pas (e.g. "foobar" au lieu de "MENTION").
 La macro ne produit pour l'instant pas automatiquement de modèle Glozz (fichier .aam).  Cela n'est pas un problème pour ouvrir le résultat dans Glozz ou Analec.
 */

// STANDARD DECLARATIONS

import org.apache.commons.lang.*
import org.kohsuke.args4j.*
import groovy.transform.*
import org.txm.*
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.Message.*
import visuAnalec.donnees.*
import visuAnalec.elements.*
import visuAnalec.vue.*

// CHECK CORPUS

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a MainCorpus: "+corpusViewSelection
	return;
}

// BEGINNING OF PARAMETERS

@Field @Option(name="unit_type",usage="", widget="String", required=true, def="MENTION")
		String unit_type

@Field @Option(name="filename",usage="", widget="String", required=true, def="filename without extension (.ac/.aa)")
		String filename

if (!ParametersDialog.open(this)) return;

// VARIABLES

corpus = corpusViewSelection

doExport(corpus, unit_type, filename)

public void doExport(MainCorpus corpus, String unit_type, String filename) {

	size = corpus.getSize() // you may also use: corpus.getTextEndLimits() (= index of last token = size-1)
	CQI = CQPSearchEngine.getCqiClient()
	word = corpus.getWordProperty()
	// note: using "lbn" seems to work better than "pn" (some imported corpora are missing this information)
	//pn = corpus.getProperty("pn")
	//if (pn == null) pn = corpus.getProperty("div")
	pn = corpus.getProperty("lbn")

	// BUILD THE RAW TEXT, THE POSITIONS AND FIND THE PARAGRAPHS

	rawText = "" // the corpus for the .ac file
	positions = [] // each element is an array [start, end] indicating the position in the rawText
	pnCount = 0 // the par counter, used for indexing the pns array
	lastPn = -1 // the last paragraph number
	pns = [] // each element is an array [start, end] representing the start and end of the paragraph in the rawText
	for (def i=0; i<size; i++) {
		f = CQI.cpos2Str(word.getQualifiedName(), (int[])[i])[0]
		if (pn == null) {
			p = 1
		} else {
			p = CQI.cpos2Str(pn.getQualifiedName(), (int[])[i])[0]
		}
		start = rawText.length()
		rawText += f
		if (lastPn != p) {
			pnCount++;
			if (pnCount > 1) {
				pns[pnCount-2][1] = end
			}
			pns[pnCount-1] = [start, 0]
		}
		lastPn = p
		end = rawText.length() // must be after setting it up in pns!
		if (i != size-1) rawText += " "
		positions[i] = [start, end]
	}
	pns[pnCount-1][1] = end
	println pnCount + " paragraph(s) found."



	// CORPUS ANALEC (GET THE ANNOTATIONS)

	// note that unit_type has been defined with an option of the dialog at the beginning
	def analecCorpus = URSCorpora.getCorpus(corpus);

	// list of properties

	struct = analecCorpus.getStructure();
	propertyList = struct.getUniteProperties(unit_type);

	// export to file (corpus)

	corpusFilename = filename + ".ac";
	def corpusFile = new File(corpusFilename);
	corpusFile.write(rawText)
	println("Corpus written to `"+corpusFilename+"'.");

	// export to file (annotations)

	annotFilename = filename + ".aa";
	def annotFile = new File(annotFilename)
	annotFile.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n<annotations>\n") // erase
	def counter = 0

	// export paragraphs
	for (def i=0; i<pns.size(); i++) {
		def start = pns[i][0]
		def end = pns[i][1]
		annotFile << "<unit id=\"me_"+counter+"\">\n";
		annotFile << "<metadata><author>me</author><creation-date>"+counter+"</creation-date></metadata>\n";
		annotFile << "<characterisation><type>paragraph</type><featureSet /></characterisation>\n";
		annotFile << "<positioning><start><singlePosition index=\""+start+"\" /></start><end><singlePosition index=\""+end+"\" /></end></positioning>\n";
		annotFile << "</unit>\n";
		counter++;
	}

	// export units
	def units = analecCorpus.getUnites(unit_type);
	//units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() };
	def unitCount = 0
	for (Unite unit : units) {
		unitCount++;
		annotFile << "<unit id=\"me_"+counter+"\">\n";
		annotFile << "<metadata><author>me</author><creation-date>"+counter+"</creation-date></metadata>\n";
		annotFile << "<characterisation>\n";
		annotFile << "<type>"+unit_type+"</type>\n";
		annotFile << "<featureSet>\n";
		for (String propertyName : propertyList) {
			annotFile << "<feature name=\""+propertyName+"\">"+unit.getProp(propertyName)+"</feature>\n";
		}
		annotFile << "</featureSet>\n";
		annotFile << "</characterisation>\n";
		start = positions[unit.getDeb()][0]
		end = positions[unit.getFin()][1]
		annotFile << "<positioning><start><singlePosition index=\""+start+"\" /></start><end><singlePosition index=\""+end+"\" /></end></positioning>\n";
		annotFile << "</unit>\n";
		counter++;
	}
	annotFile << "</annotations>\n";

	println unitCount + " unit(s) found."

	println("Annotations written to `"+annotFilename+"'.");
}