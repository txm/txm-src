package org.txm.macro.urs.democrat

import org.apache.commons.lang.*
import org.kohsuke.args4j.*
import groovy.transform.*
import org.txm.*
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.Message.*
import visuAnalec.donnees.*
import visuAnalec.elements.*
import visuAnalec.vue.*

/*
 Calcule la longueur des mentions et attribue une valeur (1, 2, 3 ou plus) dans la propriété "LONGUEUR"
 */

nLongueur1 = 0
nLongueur2 = 0
nLongueur3 = 0

def testRules(def positions, def Mention) {

	if (Mention.length == 1) {
		catégorie = "1"
		nLongueur1++
	}
	else if (Mention.length == 2) {
		catégorie = "2"
		nLongueur2++
	}
	else {
		catégorie = "3 ou plus"
		nLongueur3++
	}

	return catégorie
}

//
// FIN DE LA DÉFINITION DES RÈGLES
//

// CORPS DU SCRIPT

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a MainCorpus: "+corpusViewSelection
	return
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type", usage="", widget="String", required=true, def="MENTION")
def unit_type
@Field @Option(name="pos_property_name", usage="", widget="String", required=true, def="pos")
def pos_property_name
@Field @Option(name="reset", usage="", widget="Boolean", required=true, def="true")
def reset
if (!ParametersDialog.open(this)) return

corpus = corpusViewSelection
CQI = CQPSearchEngine.getCqiClient()
word = corpus.getWordProperty()
posProperty = corpus.getProperty(pos_property_name)
if (posProperty == null) {
	println "Error: CQP corpus does not contains the word property with name=$pos_property_name"
	return
}
analecCorpus = URSCorpora.getCorpus(corpus)
vue = URSCorpora.getVue(corpus)
structure = analecCorpus.getStructure()
if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Error: corpus structure does not contains unit with name=$unit_type"
	return
}

LONGUEUR = "LONGUEUR"
// Si la structure d'annotation ne contient pas LONGUEUR, on la crée avec ses valeurs
if (!structure.getUniteProperties(unit_type).contains(LONGUEUR)) {
	// la propriété
	analecCorpus.ajouterProp(Unite.class, unit_type, LONGUEUR)
	// les valeurs

	structure.ajouterVal(Unite.class, unit_type, LONGUEUR, "1")
	structure.ajouterVal(Unite.class, unit_type, LONGUEUR, "2")
	structure.ajouterVal(Unite.class, unit_type, LONGUEUR, "3 ou plus")

	//...
}

if (!vue.getTypesAVoir(Unite.class).contains(unit_type)) {
	vue.ajouterType(Unite.class, unit_type)
}
if (!vue.getNomsChamps(Unite.class, unit_type).contains(LONGUEUR)) {
	vue.ajouterProp(Unite.class, unit_type, LONGUEUR)
}

def nModified = 0
def nIgnored = 0

errors = new HashMap()
def units = analecCorpus.getUnites(unit_type)
units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }
for (Unite unit : units) { // process all units

	def prop = unit.getProp(LONGUEUR)
	if (!reset && prop != null && prop.length() > 0) continue // l'unité a déjà une LONGUEUR

	int[] positions = null
	if (unit.getDeb() == unit.getFin()) positions = [unit.getDeb()]
	else positions = (unit.getDeb()..unit.getFin())

	//def Mention = CQI.cpos2Str(posProperty.getQualifiedName(), positions)
	def cat = testRules(positions, positions)

	if (cat != null) {
		vue.setValeurChamp(unit, LONGUEUR, cat)
		nModified++
	} else {
		nIgnored++
	}
}

println "Result:"
println "- $nModified units of type $unit_type have been modified."
println "- $nIgnored units of type $unit_type have not been modified.\n"

println "- $nLongueur1 mentions de longueur 1."
println "- $nLongueur2 mentions de longueur 2."
println "- $nLongueur3 mentions de longueur 3 ou plus.\n"

if (errors.size() > 0) {
	println "Some rules should be added to this macro to process the following remaining 'FROPOS / words' values:"
	errors.keySet().each { println "fropos="+it+"\twords="+errors[it].join(" | ") }
}
