package org.txm.macro.urs.democrat

import org.apache.commons.lang.*
import org.kohsuke.args4j.*
import groovy.transform.*
import org.txm.*
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.Message.*
import visuAnalec.donnees.*
import visuAnalec.elements.*
import visuAnalec.vue.*

def testRules(def positions, def Mention) {
	def catégorie = null

// DÉFINITION DES RÈGLES
// elles sont testées les unes après les autres. 
// Dès qu'une règle est appliquée les suivantes sont ignorées.

// Règles de type CONTIENT

	     if (Mention.contains("NOMpro")) catégorie = "Nom Propre"
	else if (Mention.contains("DETpos")) catégorie = "Dét Possessif"
//  ...

// Règles de type COMMENCE ET NE CONTIENT PAS	

	else if (Mention.first() == "DETpos" && !Mention.contains("NOMpro")) catégorie = "GN Possessif"
	else if (Mention.first() == "DETdem" && !Mention.contains("NOMpro")) catégorie = "GN Démonstratif"
//  ...

// Règles de type CONTIENT PLUSIEURS	

	else if (Mention.contains("PROadv") || Mention.contains("ADVgen.PROadv")) catégorie = "Pronom Adverbial"
//  ...

// Règles de type CONTIENT ET NE CONTIENT PAS

	else if (
			 ( Mention.contains("DETdef") || Mention.contains("PRE.DETdef") )
			  &&
			  !Mention.contains("PROpos") && !Mention.contains("NOMpro") && !Mention.contains("PROcar")
			) catégorie = "GN Défini"
//  ...	

// Fin des règles, aucune n'a matchée. On stocke le pattern  qu'on affichera à la fin.
	else {
		def forms = CQI.cpos2Str(word.getQualifiedName(), positions)
		if (!errors.containsKey(Mention)) errors[Mention] = new HashSet()
		errors[Mention] << forms
	}
	
	return catégorie
}

//
// FIN DE LA DÉFINITION DES RÈGLES
//

// CORPS DU SCRIPT

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a MainCorpus: "+corpusViewSelection
	return
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type", usage="", widget="String", required=true, def="MENTION")
def unit_type
@Field @Option(name="pos_property_name", usage="", widget="String", required=true, def="pos")
def pos_property_name
@Field @Option(name="reset", usage="", widget="Boolean", required=true, def="true")
def reset
if (!ParametersDialog.open(this)) return

corpus = corpusViewSelection
CQI = CQPSearchEngine.getCqiClient()
word = corpus.getWordProperty()
posProperty = corpus.getProperty(pos_property_name)
if (posProperty == null) {
	println "Error: CQP corpus does not contains the word property with name=$pos_property_name"
	return
}
analecCorpus = URSCorpora.getCorpus(corpus)
vue = URSCorpora.getVue(corpus)
structure = analecCorpus.getStructure()
if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Error: corpus structure does not contains unit with name=$unit_type"
	return
}

CATEGORIE = "CATEGORIE"
// Si la structure d'annotation ne contient pas CATEGORIE, on la crée avec ses valeurs
if (!structure.getUniteProperties(unit_type).contains(CATEGORIE)) { 
// la propriété
	analecCorpus.ajouterProp(Unite.class, unit_type, CATEGORIE)
// les valeurs
	analecCorpus.ajouterVal(Unite.class, unit_type, CATEGORIE, "Nom Propre")
	analecCorpus.ajouterVal(Unite.class, unit_type, CATEGORIE, "Pronom Impersonnel")
	analecCorpus.ajouterVal(Unite.class, unit_type, CATEGORIE, "Pronom Interrogatif")
	analecCorpus.ajouterVal(Unite.class, unit_type, CATEGORIE, "Pronom Pronom cardinal")
	analecCorpus.ajouterVal(Unite.class, unit_type, CATEGORIE, "Pronom Démonstratif")
	analecCorpus.ajouterVal(Unite.class, unit_type, CATEGORIE, "Pronom Indéfini")
	analecCorpus.ajouterVal(Unite.class, unit_type, CATEGORIE, "Pronom ordinal")
	analecCorpus.ajouterVal(Unite.class, unit_type, CATEGORIE, "Pronom Relatif")
//...
}

if (!vue.getTypesAVoir(Unite.class).contains(unit_type)) {
	vue.ajouterType(Unite.class, unit_type)
}
if (!vue.getNomsChamps(Unite.class, unit_type).contains(CATEGORIE)) {
	vue.ajouterProp(Unite.class, unit_type, CATEGORIE)
}

def nModified = 0
def nIgnored = 0

errors = new HashMap()
def units = analecCorpus.getUnites(unit_type)
units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }
for (Unite unit : units) { // process all units
	
	def prop = unit.getProp(CATEGORIE)
	if (!reset && prop != null && prop.length() > 0) continue // l'unité a déjà une CATEGORIE
	
	int[] positions = null
	if (unit.getDeb() == unit.getFin()) positions = [unit.getDeb()]
	else positions = (unit.getDeb()..unit.getFin())
	
	def Mention = CQI.cpos2Str(posProperty.getQualifiedName(), positions)
	def cat = testRules(positions, Mention)
	
	if (cat != null) {
		vue.setValeurChamp(unit, CATEGORIE, cat)
		nModified++
	} else {
		nIgnored++
	}
}

println "Result:"
println "- $nModified units of type $unit_type have been modified."
println "- $nIgnored units of type $unit_type have not been modified.\n"

if (errors.size() > 0) {
	println "Some rules should be added to this macro to process the following remaining 'FROPOS / words' values:"
	errors.keySet().each { println "fropos="+it+"\twords="+errors[it].join(" | ") }
}
