// STANDARD DECLARATIONS
package org.txm.macro.urs.democrat

import org.apache.commons.lang.*
import org.kohsuke.args4j.*
import groovy.transform.*
import org.txm.*
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.analec.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.Message.*
import visuAnalec.donnees.*
import visuAnalec.elements.*
import visuAnalec.vue.*


/*  MACRO pour corriger une erreur d'annotation
    Retirer le "de" ou le "d'" (forme élidée) du complément du nom

   Algo : 
   POUR CHAQUE MENTION dont le premier mot est "de" (en minuscules)
   SI     il existe une autre MENTION dans laquelle celle-ci est totalement incluse
   ALORS  incrémenter d'un mot la frontière gauche de la mention
   Ajouter la categorie CDN.CHECK pour qu'on puisse verifier facilement le job.
   
   Cette macro ne s'appuie pas sur des catégories morphosyntaxiques. 
   On peut donc la faire tourner avant les frpos2cattex ou fropos2cattex
   
   Auteur : Matthieu QUIGNARD
   Date : 18 janvier 2019
*/

// BEGINNING OF PARAMETERS

// Declare each parameter here
// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type", usage="", widget="String", required=true, def="MENTION")
def unit_type
@Field @Option(name="category_name", usage="", widget="String", required=true, def="CATEGORIE")
def category_name
if (!ParametersDialog.open(this)) return

corpus = corpusViewSelection
CQI = CQPSearchEngine.getCqiClient()
word = corpus.getWordProperty()

analecCorpus = URSCorpora.getCorpus(corpus)
vue = URSCorpora.getVue(corpus)
structure = analecCorpus.getStructure()
if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Error: corpus structure does not contains unit with name=$unit_type"
	return
}


// Si la structure d'annotation ne contient pas CATEGORIE, on la crée avec ses valeurs
if (!structure.getUniteProperties(unit_type).contains(category_name)) { 
   structure.ajouterProp(Unite.class, unit_type, category_name)
}

def check_cat = "CDN.CHECK"
structure.ajouterVal(Unite.class, unit_type, category_name, check_cat)

if (!vue.getTypesAVoir(Unite.class).contains(unit_type)) {
	vue.ajouterType(Unite.class, unit_type)
}
if (!vue.getNomsChamps(Unite.class, unit_type).contains(category_name)) {
	vue.ajouterProp(Unite.class, unit_type, category_name)
}

def nModified = 0
def nIgnored1 = 0
def nIgnored2 = 0
def compteur = 0

errors = new HashMap()
def units = analecCorpus.getUnites(unit_type)
units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }

/* Test sur la premiere mention :
def debut1 = units[1].getDeb()
def fin1 = units[1].getFin()
println "$debut1 - $fin1"
units[1].setDeb( debut1 + 1)
units[1].setFin( fin1 + 1)
def debut2 = units[1].getDeb()
def fin2 = units[1].getFin()
println "$debut2 - $fin2"
*/

for (Unite unit : units) { // process all units
	
	def debut = unit.getDeb()
	def fin = unit.getFin()
	def premierMot = CQI.cpos2Str(word.getQualifiedName(), debut)[0]
		
	if ((premierMot != "de") && (premierMot !="d'")) {
		nIgnored1++
		compteur++
		continue
	} else {
		for (i = compteur-1; i>=0 ; i--) {
			def u = units[i]
			def udeb = u.getDeb()
			def ufin = u.getFin()
			if (ufin >= fin) {
			   println "\nAVANT => Unit $compteur : $debut - $fin"
			   if (fin > debut) unit.setDeb( ++debut ) 
			   else println "not resizing"
			   def debut2 = unit.getDeb()
			   def fin2 = unit.getFin()
			   println "APRES => Unit $compteur : $debut2 - $fin2"
			   unit.getProps().put(category_name, check_cat)
			   break
			}
		}
		if (i <0) nIgnored2++
		else nModified++
		compteur++
	} 
}

println "\nResult:"
println "- $nModified units have been modified."
println "- $nIgnored1 units have been ignored because not concerned"
println "- $nIgnored2 units have been ignored because no overlap.\n"
println "Total ($compteur)."

