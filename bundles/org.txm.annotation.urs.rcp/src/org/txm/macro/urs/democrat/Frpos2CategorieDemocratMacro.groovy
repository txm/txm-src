// @author Bruno Oberlé (2017-04-01 21:50)

/*
Définit la catégorie grammaticale du maillon d'après le champ `frpos'
(tagset de TreeTagger).  Le script est adapté de
http://svn.code.sf.net/p/txm/code/trunk/plugins/Analec/AnalecRCP/src/org/txm/macro/analec/Fropos2CategorieMacro.groovy.

Voici la liste des catégories grammaticales retenues (manuel d'annotation de
Democrat):
- GN: Groupe Nominal (le petit chat, le chat, le même, ce chat etc.)
- POSS: Possessif (mon, ton son, ma, ta, sa, mes, tes, ses, notre, votre,
  leur, nos, vos, leurs)
- PR: Pronom (moi, toi, lui, elle, nous, vous, eux, elles, le tien, le mien,
  moi-même etc.)
- PR_CL_O: Pronom Clitique Objet (me, te, le, la, les, lui, leur, y, en)
- PR_CL_R: Pronom Clitique Réfléchi
- PR_CL_S: Pronom Clitique Sujet (je, tu, il, elle, on, nous, vous, ils,
  elles)
- PR_REL: Pronom Relatif (qui, que, quoi, dont, où, lequel, quiconque etc.)
- PR_WH: Pronom Interrogatif (qui, que, quoi, lequel etc.)

Le script ne peut pas désambiguïser les pronoms clitiques de même forme
(`nous' est-il un sujet, un objet ou un réfléchi?).  Dans ce cas, le script
opte pour le sujet (ou pour l'objet si l'ambiguïté n'est que entre objet et
réfléchi).

Quand il n'y a aucune information disponible (erreurs de l'étiqueteur), la
valeur est UNDEFINED.

L'algorithme est décrit ici:
https://groupes.renater.fr/wiki/democrat/prive/txm_annotation_exploitation

*/

package org.txm.macro.urs.democrat

import org.apache.commons.lang.*
import org.kohsuke.args4j.*
import groovy.transform.*
import org.txm.*
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.Message.*
import visuAnalec.donnees.*
import visuAnalec.elements.*
import visuAnalec.vue.*

def testClitic(def position, def frpos) {

   // je me sers de la forme, parce qu'il est difficile de savoir quel est le
   // lemme de "elle" ("il"?), de "te" ("tu"?) ou encore de "leur"
   def form = CQI.cpos2Str(word.getQualifiedName(), position)[0].toLowerCase()
   if (     form == "je" || form == "j'"
         || form == "tu" || form == "t'"
         || form == "il"
         || form == "elle"
         || form == "on"
         || form == "vous"
         || form == "nous"
         || form == "ils"
         || form == "elles" ) {
      return "PR_CL_S"
   } else if (form == "me" || form == "m'"
           || form == "te"
           || form == "le" || form == "l'"
           || form == "la"
           || form == "lui"
           || form == "leur"
           || form == "les" ) {
      return "PR_CL_O"
   } else if (form == "se" || form == "s'") {
      return "PR_CL_R"
   }
   return null

}

def testPhrase(def positions, def Mention) {

   // on doit regarder ce qui apparaît en premier:
   // - ce peut être un nom, comme dans `le petit chat que j'ai adopté'
   // - ce peut être un pronom relatif, comme dans `(le livre) dans lequel
   // j'ai lu cette histoire...'
   // NOTE: dans Democrat, on n'annote pas, bizarrement, la relative dans le
   // maillon, donc, dans un GN on n'a jamais de relatif inclus.  On aura donc
   // toujours `[le petit chat] [que] [j']ai adopté'.  Mais tout le monde
   // n'annote pas de la sorte...
   for (def i=0; i<Mention.length; i++) {
         def mention = Mention[i]
         //def form = CQI.cpos2Str(word.getQualifiedName(), positions[i])[0]
         if (mention == "NOM" || mention == "NAM") {
            return "GN"
         } else if (mention == "PRO:REL") {
            return "PR_REL"
         }
    }

    return null

}

def testWhPronoun(position, mention) {
   def form = CQI.cpos2Str(word.getQualifiedName(), position)[0]
   if (mention == "PRO" && (form == "qui" || form == "que" || form == "lequel")) {
      return "PR_WH"
   }
   return null

}

def testRules(def positions, def Mention) {
	def catégorie = null

   // a possessive (mon, ma...)
   if (Mention.length == 1 && Mention.contains("DET:POS"))
      catégorie = "POSS"

   // a clitic (subject: je, tu...; object: me, te; reflexive: se)
   if (!catégorie && Mention.length == 1 && Mention.contains("PRO:PER"))
      catégorie = testClitic(positions[0], Mention[0])

   // an interrogative pronoun
   if (!catégorie && Mention.length == 1)
      catégorie = testWhPronoun(positions[0], Mention[0])

   // a noun phrase or a relative pronoun
   if (!catégorie)
      catégorie = testPhrase(positions, Mention)

   // some other kind of pronouns
   if (!catégorie
         && (   Mention.contains("PRO")
             || Mention.contains("PRO:POSS")
             || Mention.contains("PRO:IND")
             || Mention.contains("PRO:DEM")
             || Mention.contains("PRO:PER") )
         && !Mention.contains("NOM")
         && !Mention.contains("NAM") )
      catégorie = "PRO"

// Fin des règles, aucune n'a matchée. On stocke le pattern  qu'on affichera à la fin.
   if (!catégorie) {
      catégorie = "UNDEFINED" // clear the field
		def forms = CQI.cpos2Str(word.getQualifiedName(), positions)
		if (!errors.containsKey(Mention)) errors[Mention] = new HashSet()
		errors[Mention] << forms
	}
	
	return catégorie
}

//
// FIN DE LA DÉFINITION DES RÈGLES
//

// CORPS DU SCRIPT

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a MainCorpus: "+corpusViewSelection
	return
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type", usage="", widget="String", required=true, def="MENTION")
def unit_type
@Field @Option(name="pos_property_name", usage="", widget="String", required=true, def="pos")
def pos_property_name
@Field @Option(name="reset", usage="", widget="Boolean", required=true, def="true")
def reset
if (!ParametersDialog.open(this)) return

corpus = corpusViewSelection
CQI = CQPSearchEngine.getCqiClient()
word = corpus.getWordProperty()
posProperty = corpus.getProperty(pos_property_name)
if (posProperty == null) {
	println "Error: CQP corpus does not contains the word property with name=$pos_property_name"
	return
}
analecCorpus = URSCorpora.getCorpus(corpus)
vue = URSCorpora.getVue(corpus)
structure = analecCorpus.getStructure()
if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Error: corpus structure does not contains unit with name=$unit_type"
	return
}

CATEGORIE = "CATEGORIE"
// Si la structure d'annotation ne contient pas CATEGORIE, on la crée avec ses valeurs
if (!structure.getUniteProperties(unit_type).contains(CATEGORIE)) { 

// FIXME: dans le script original (see also
// http://forge.cbp.ens-lyon.fr/redmine/issues/2065), on utilise
// analecCorpus.ajouterProp/Val, mais cela ne marche pas dans ma version de
// TXM-Analec --> je retourne donc à structure.ajouterProp/Val

// la propriété
	structure.ajouterProp(Unite.class, unit_type, CATEGORIE)
// les valeurs
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "GN")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "POSS")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PR_CL_O")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PR_CL_S")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PR_CL_R")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PR_REL")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PR_WH")
//...
}

if (!vue.getTypesAVoir(Unite.class).contains(unit_type)) {
	vue.ajouterType(Unite.class, unit_type)
}
if (!vue.getNomsChamps(Unite.class, unit_type).contains(CATEGORIE)) {
	vue.ajouterProp(Unite.class, unit_type, CATEGORIE)
}

def nModified = 0
def nIgnored = 0

errors = new HashMap()
def units = analecCorpus.getUnites(unit_type)
units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }
for (Unite unit : units) { // process all units
	
	def prop = unit.getProp(CATEGORIE)
	if (!reset && prop != null && prop.length() > 0) continue // l'unité a déjà une CATEGORIE
	
	int[] positions = null
	if (unit.getDeb() == unit.getFin()) positions = [unit.getDeb()]
	else positions = (unit.getDeb()..unit.getFin())
	
	def Mention = CQI.cpos2Str(posProperty.getQualifiedName(), positions)
	def cat = testRules(positions, Mention)

	if (cat != null) {
		// following line in the original script but doesn't work for me:
      // vue.setValeurChamp(unit, CATEGORIE, cat)
      unit.getProps().put(CATEGORIE, cat)
		nModified++
	} else {
		nIgnored++
	}
}

println "Result:"
println "- $nModified units of type $unit_type have been modified."
println "- $nIgnored units of type $unit_type have not been modified.\n"

if (errors.size() > 0) {
	println "Some rules should be added to this macro to process the following remaining 'FROPOS / words' values:"
	errors.keySet().each { println "fropos="+it+"\twords="+errors[it].join(" | ") }
}

// udpate the view (also see also
// http://forge.cbp.ens-lyon.fr/redmine/issues/2065)
