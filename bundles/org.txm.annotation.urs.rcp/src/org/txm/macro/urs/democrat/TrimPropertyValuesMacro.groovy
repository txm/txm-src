// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// STANDARD DECLARATIONS
package org.txm.macro.urs.democrat

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.macro.urs.AnalecUtils
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.donnees.Corpus
import visuAnalec.donnees.Structure;
import visuAnalec.elements.*
import visuAnalec.vue.Vue

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a MainCorpus: "+corpusViewSelection
	return;
}

@Field @Option(name="element", usage="Unite|Relation|Schema", widget="StringArray", metaVar="Unite	Relation	Schema", required=true, def="Unite")
		String element
@Field @Option(name="ursql", usage="select the elements with a URSQL", widget="String", required=true, def="")
		String ursql
	
@Field @Option(name="replaceValues", usage="do replace the property values", widget="Boolean", required=true, def="false")
		def replaceValues
@Field @Option(name="trimProperties", usage="remove heading and trailing blanks", widget="Boolean", required=true, def="true")
		def trimProperties
@Field @Option(name="fixSpaces", usage="replace several blanks with one blank", widget="Boolean", required=true, def="true")
		def fixSpaces
		@Field @Option(name="fixNewLines", usage="replace", widget="Boolean", required=true, def="true")
		def fixNewLines
@Field @Option(name="debug", usage="Show internal variable content", widget="StringArray", metaVar="OFF	ON	ALL	REALLY ALL", required=true, def="OFF")
		debug
if (!ParametersDialog.open(this)) return

	if (debug == "OFF") debug = 0; else if (debug == "ON") debug = 1; else if (debug == "ALL") debug = 2 else if (debug == "REALLY ALL") debug = 3

int nCreated = 0 // count the number of created CHAINE
int nUpdated = 0 // count the number of updated CHAINE

CQPCorpus corpus = corpusViewSelection
Corpus analecCorpus = URSCorpora.getCorpus(corpus);

Class clazz = Unite.class;
def elements = null
if ("Schema".equals(element)) {
	clazz = Schema.class;
	elements = analecCorpus.getTousSchemas()
} else if ("Relation".equals(element)){
	clazz = Relation.class;
	elements = analecCorpus.getToutesRelations()
} else if ("Unite".equals(element)){
	clazz = Unite.class;
	elements = analecCorpus.getToutesUnites()
}

elements = AnalecUtils.filterElements(debug, elements, ursql)

println "Fixing ${elements.size()} $ursql $element"
int nUpdate = 0;
// update the already existing CHAINES schemas

	for (def e : elements) {
		def props = e.getProps()
		if (debug > 2) println "		e=$props"
		for (def k : props.keySet()) {
			String p = props[k]
			
			if (trimProperties) {
				p = p.trim()
			}
			if (fixNewLines) {
				p = p.replaceAll("\n", "")
			}
			if (fixSpaces) {
				p = p.replaceAll("[ ]++", " ")
			}
			
			if (p != props[k]) {
				nUpdate++
			}
			if (replaceValues) {
				props[k] = p
				if (debug > 3) println "			props[$k]=${props[k]} -> $p"
			} else if (p != props[k]) {
				println "			props[$k]=${props[k]} -> $p"
			}
		}
	}

println "Done: $nUpdate update(s)"