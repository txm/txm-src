package org.txm.macro.urs.democrat

import org.apache.commons.lang.*
import org.kohsuke.args4j.*
import groovy.transform.*
import org.txm.*
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.Message.*
import visuAnalec.donnees.*
import visuAnalec.elements.*
import visuAnalec.vue.*

/*
   Calcule l'accessibilité (faible ou forte) d'une mention à partir de sa CATEGORIE
   La macro fonctionne à partir de 2 listes de catégories : accessibilité forte, accessibilité nulle.
   L'accessibilité faible sera le cas par défaut.
   
   La macro crée une nouvelle propriété pour les MENTION : ACCESSIBILITE
   Les valeurs autorisées sont : "Faible", "Forte" et "N/A"
   Voir si la dénomination de cette dernière valeur convient.
    
   Auteur : Matthieu Quignard
   Date : 19/12/2017
 */

def listeAccessibiliteForte=["PRO.PERA", "PRO.REL", "SUJ.ZERO", "DET.POS"]
def listeAccessibiliteNulle=[]


// CORPS DU SCRIPT

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "Corpora selection is not a Corpus: "+corpusViewSelection
	return
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type", usage="", widget="String", required=true, def="MENTION")
def unit_type
if (!ParametersDialog.open(this)) return

corpus = corpusViewSelection
CQI = CQPSearchEngine.getCqiClient()

analecCorpus = URSCorpora.getCorpus(corpus)
vue = URSCorpora.getVue(corpus)
structure = analecCorpus.getStructure()
if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Error: corpus structure does not contains unit with name=$unit_type"
	return
}

ACCESSIBILITE = "ACCESSIBILITE"
// Si la structure d'annotation ne contient pas ACCESSIBILITE, on la crée avec ses valeurs
if (!structure.getUniteProperties(unit_type).contains(ACCESSIBILITE)) {
	// la propriété
	analecCorpus.ajouterProp(Unite.class, unit_type, ACCESSIBILITE)
	// les valeurs

	structure.ajouterVal(Unite.class, unit_type, ACCESSIBILITE, "Faible")
	structure.ajouterVal(Unite.class, unit_type, ACCESSIBILITE, "Forte")
	structure.ajouterVal(Unite.class, unit_type, ACCESSIBILITE, "N/A")
}
if (!vue.getTypesAVoir(Unite.class).contains(unit_type)) {
	vue.ajouterType(Unite.class, unit_type)
}
if (!vue.getNomsChamps(Unite.class, unit_type).contains(ACCESSIBILITE)) {
	vue.ajouterProp(Unite.class, unit_type, ACCESSIBILITE)
}

def nModified = 0
def nIgnored = 0
def nNulle = 0
def nForte = 0
def nFaible = 0

errors = new HashMap()
def units = analecCorpus.getUnites(unit_type)
units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }
for (Unite unit : units) { // process all units

	def prop = unit.getProp("CATEGORIE")
	
	if (prop== null) {
		// On ignore les cas où la catégorie n'est pas renseignée.
		nIgnored++
	} else {
		nModified++
		if (listeAccessibiliteNulle.contains(prop)) {
			// Cas d'accessibilité nulle
			vue.setValeurChamp(unit, ACCESSIBILITE, "N/A")
			nNulle++
		} else if (listeAccessibiliteForte.contains(prop)) {
			// Cas d'accessibilité forte
			vue.setValeurChamp(unit, ACCESSIBILITE, "Forte")	
			nForte++
		} else {
			// Sinon, on est dans le cas d'accessibilité faible
			vue.setValeurChamp(unit, ACCESSIBILITE, "Faible")	
			nFaible++
		}
	}
	
}

println "Result:"
println "- $nModified mentions ont été modifiées."
println "- $nIgnored mentions ont été ignorées (leur catégorie est vide).\n"

println "- $nForte mentions d'accessibilité forte."
println "- $nFaible mentions d'accessibilité faible."
println "- $nNulle mentions de type 'N/A' (déictiques).\n"

