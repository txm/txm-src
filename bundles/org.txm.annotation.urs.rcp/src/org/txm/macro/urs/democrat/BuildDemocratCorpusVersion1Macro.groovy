package org.txm.macro.urs.democrat

import java.io.File
import java.nio.charset.Charset

import org.eclipse.core.runtime.IProgressMonitor
import org.kohsuke.args4j.*
import groovy.io.FileType
import groovy.transform.Field
import visuAnalec.donnees.Corpus
import visuAnalec.vue.Vue

import org.txm.annotation.urs.URSCorpora
import org.txm.importer.StaxIdentityParser
import org.txm.objects.*
import org.txm.rcp.commands.workspace.LoadBinaryCorpus
import org.txm.rcp.swt.widget.parameters.*
import org.txm.rcp.utils.JobHandler
import org.txm.searchengine.cqp.corpus.*
import org.txm.utils.CsvReader
import org.txm.utils.io.IOUtils
import org.txm.scripts.importer.XPathResult
import javax.xml.xpath.XPathConstants
import org.txm.annotation.urs.commands.*

@Field @Option(name="inputDirectory", usage="Directory with .txm files, metadata.tsv, metadata.properties...", widget="Folder", required=true, def="input directory path")
def inputDirectory

@Field @Option(name="outputDirectory", usage="the default teiHeader of texts", widget="Folder", required=true, def="output directory path")
def outputDirectory

@Field @Option(name="debug", usage="Show internal variable content", widget="StringArray", metaVar="OFF	ON	ALL	REALLY ALL", required=true, def="OFF")
debug

if (!ParametersDialog.open(this)) return;
if (debug == "OFF") debug = 0; else if (debug == "ON") debug = 1; else if (debug == "ALL") debug = 2 else if (debug == "REALLY ALL") debug = 3

def teiHeaderTemplateFile = new File(inputDirectory, "teiHeader.xml")
def xpathFile = new File(inputDirectory, "metadata.properties")
def metadataFile = new File(inputDirectory, "metadata.tsv")

if (!inputDirectory.exists()) {
	println "no inputDirectory found: $inputDirectory"
	return false;
}

outputDirectory.mkdirs()
if (!outputDirectory.exists()) {
	println "outputDirectory not created: $outputDirectory"
	return false;
}

if (!teiHeaderTemplateFile.exists()) {
	println "no teiHeader template found: $teiHeaderTemplateFile"
	return false;
}

if (!xpathFile.exists()) {
	println "no xpath properties found: $xpathFile"
	return false;
}

if (!metadataFile.exists()) {
	println "no metadata TSV found: $metadataFile"
	return false;
}

def xpathProperties = new Properties()
xpathProperties.load(IOUtils.getReader(xpathFile))

println "xpath properties: "+xpathProperties.keySet()

def csvReader = new CsvReader(metadataFile.getAbsolutePath(), "\t".charAt(0), Charset.forName("UTF-8"))

csvReader.readHeaders();
def header = csvReader.getHeaders()
if (!header.contains("text_id")) {
	println "** 'id' column not found in $metadataFile header=$header" 
	return;
}
if (!header.contains("corpus_id")) {
	println "** 'corpus_id' column not found in $metadataFile header=$header"
	return;
}

if (!header.contains("txm_file")) {
	println "** 'txm_file' column not found in $metadataFile header=$header"
	return;
}

def corpora = [:]
for (Project project : Workspace.getInstance().getProjects()) {
	for (MainCorpus corpus : project.getChildren(MainCorpus.class)) {
		corpora[corpus.getID()] = corpus;
	}
}

while (csvReader.readRecord())	{
	String text_id = csvReader.get("text_id")
	if (text_id == null || text_id.length() == 0) {
		println "** @text_id not found for record="+csvReader.getRawRecord()
		continue;
	}
	println "text: "+text_id
	
	String corpus_id = csvReader.get("corpus_id")
	if (corpus_id == null || corpus_id.length() == 0) {
		println " ** @corpus_id <- @text_id="+text_id.toUpperCase()
		corpus_id = text_id.toUpperCase();
	}
	
	String txm_file = csvReader.get("txm_file")
	if (txm_file == null || txm_file.length() == 0) {
		println " ** @txm_file <- @corpus_id=${corpus_id}.txm"
		txm_file = corpus_id.toUpperCase()+".txm";
	}
	
	MainCorpus corpus = corpora[corpus_id]
	Project project = null
	boolean loaded = false
	if (corpus == null) {
		File binCorpusFile = new File(inputDirectory, txm_file)
		if (!binCorpusFile.exists()) {
			println " ** no corpus binary file found for ID='$corpus_id' : $binCorpusFile"
			continue;
		}
		println " loading corpus from ${binCorpusFile}..."
		JobHandler job = LoadBinaryCorpus.loadBinaryCorpusArchive(binCorpusFile)
		job.join();
		project = job.getResultObject();
		corpus = project.getCorpusBuild(corpus_id);
		if (corpus == null) {
			println " ** no corpus for ID='$corpus_id' found in the binary corpus."
			continue
		}
		corpora[corpus.getID()] = corpus;
		loaded = true
	} else {
		println " using loaded corpus ${corpus}..."
		project = corpus.getProject()
	}
	
	def txmDir = new File(project.getProjectDirectory(), "txm/"+corpus.getID())
	
	if (!txmDir.exists()) {
		println " ** the selected corpus has no XML-TXM files. Aborting."
		continue;
	}
	
	File txmFile = new File(txmDir, text_id+".xml")
	if (!txmFile.exists()) { //
		def list = []
		txmDir.eachFileRecurse (FileType.FILES) { file ->
			list << file
		}
		txmFile = list[0]
	}
	if (!txmFile.exists()) {
		println " ** the selected corpus has no XML-TXM file: $txmFile"
		continue;
	}
	
	
	println " compute measures..."
	def data = new ComputeDemocratMeasureMetadata().getStats(corpus, xpathProperties)
	
	Metadata2TEIHeader mthm = new Metadata2TEIHeader(debug);
	for (def h : header) data[h] = csvReader.get(h)
	
	println " creating teiHeader..."
	String xmlteiHeaderContent = mthm.getCustomizedTEiHeader(teiHeaderTemplateFile, data, xpathProperties);
	if (xmlteiHeaderContent != null && xmlteiHeaderContent.length() > 0) {
		println " replacing teiHeader... "
		mthm.replaceHeader(txmFile, xmlteiHeaderContent)
	} else {
		println " ** Text header not updated: $txmFile"
	}
		
	println " URS Export..."
	Corpus analecCorpus = URSCorpora.getCorpus(corpus);
	Vue vue = URSCorpora.getVue(corpus);
	if (!ExportTEICorpus.export(true, outputDirectory, false, null, corpus, analecCorpus, vue)) {
		println " FAIL TO EXPORT CORPUS $corpus"
	}
	
	println " exporting the corpus to '.txm' file..."
	corpus.export(new File(outputDirectory, corpus.getID()+".txm"), null)
	
	if (loaded) {
		println " removing loaded corpus..."
		corpus.delete();
	}
	
	println " done."
}