package org.txm.macro.urs.democrat

import org.apache.commons.lang.*
import org.kohsuke.args4j.*
import groovy.transform.*
import org.txm.*
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.Message.*
import visuAnalec.donnees.*
import visuAnalec.elements.*
import visuAnalec.vue.*

/*
Définit la catégorie grammaticale des mentions d'après le champ `frpos'
(tagset de TreeTagger). 

La liste des catégories grammaticales est celle de CATTEX2009
-- Groupes nominaux :
	GN.NAM (noms propres) : Henri II
	GN.DEF (définis) : Le roi, du roi
	GN.IND (indéfinis) : Un roi
	GN.POS (possessifs) : [Mon roi]
	GN.DEM (demonstratif) : Ce roi
	GN.NUM (numéraux) : Deux rois
	GN.CHECK (GN indéterminés)
-- Déterminants
	DET.POS (possessifs) : [mon] roi
-- Pronoms
	PRO.PER (personnels) : je, moi, me, on, il, etc.
	PRO.ADV (adverbiaux) : y, en
	PRO.IND (indéfinis) : tout, tous, certains, plusieurs, etc.
	PRO.DEM (demonstratifs) : ceci, cela, ce, ça...
	PRO.POS (possessifs) : le mien, les nôtres...
	PRO.NUM (cardinaux, ordinaux) : les deux...
	PRO.REL (relatifs) : qui, que, quoi, duquel, etc.
	PRO.INT (interrogatifs)
	PRO.CHECK (pronoms indéterminés)
-- SUJ.ZERO (Sujet Zéro) : verbes conjugués, éventuellement pronominal

-- ERREUR : erreur (a priori) de mention
-- ADJpos : adjectif possessif à fusionner ou pas avec DETpos
	
*/



def testRules(def positions, def Mention) {
	def catégorie = null
	
	// la forme du premier mot de la mention s'appelle 'forme'
	def forme = CQI.cpos2Str(word.getQualifiedName(), positions)[0].toLowerCase()
    
    
	if (Mention.length == 1) {    
	    
		     if (Mention.first() == "NOMpro"    ) catégorie = "GN.NAM"
		else if (Mention.first() == "DETpos") catégorie = "DET.POS"
		else if (Mention.first() == "ADJpos") catégorie = "ADJ.POS"
		else if (Mention.first() == "PROdem") catégorie = "PRO.DEM"
		else if (Mention.first() == "PROind") catégorie = "PRO.IND"
		else if (Mention.first() == "PROcar") catégorie = "PRO.NUM"
		else if (Mention.first() == "PROord") catégorie = "PRO.NUM" // fusionné avec Cardinaux
		else if (Mention.first() == "PROpos") catégorie = "PRO.POS"
		else if (Mention.first() == "PROper") catégorie = "PRO.PER" 
		else if (Mention.first() == "PROimp") catégorie = "PRO.PER" // fusionné avec Pronoms Personnels
		else if (Mention.first() == "PROint") catégorie = "PRO.INT"
		else if (Mention.first() == "PROadv") catégorie = "PRO.ADV"
		else if (Mention.first() == "PROrel") catégorie = "PRO.REL"
		
		else if (Mention.first().contains("VER")) catégorie = "SUJ.ZERO"
		
		else if (Mention.first() == "NOMcom") catégorie = "GN.CHECK"
		
		// Pronoms "contractés"
		else if (Mention.first() == "PROper.PROper") catégorie = "PRO.PER"  // double pronom personnel : ex 'jel' pour 'je le'
		else if (Mention.first() == "ADVgen.PROper") catégorie = "PRO.PER"  // adverbe + pronom personnel : ex 'sil' pour 'si le'
		else if (Mention.first() == "ADVneg.PROper") catégorie = "PRO.PER"  // adverbe + pronom personnel : ex 'nel' pour 'ne le'
		
		// Erreurs de mention
		else if (Mention.first() == "ADVgen") catégorie = "ERREUR"  // un adverbe seul n'est jamais référentiel
		else if (Mention.first() == "ADVneg") catégorie = "ERREUR"  // un adverbe seul n'est jamais référentiel
		else if (Mention.first() == "PRE") catégorie = "ERREUR"  // une preposition seule n'est jamais référentielle
		else if (Mention.first() == "ADJqua") catégorie = "ERREUR"  // un adjectif seul n'est jamais référentiel
		else if (Mention.first() == "ADJind") catégorie = "ERREUR"  // un adjectif seul n'est jamais référentiel
		else if (Mention.first() == "INJ") catégorie = "ERREUR"  // une interjection seule n'est jamais référentielle
		
		else catégorie = "PRO.CHECK"	

	} else if (Mention.length == 2) {
		if ( (Mention[0] == "NOMpro") || (Mention[1] == "NOMpro") ) catégorie = "GN.NAM"
		else if (Mention[1] == "PROrel") catégorie = "PRO.REL"  // "ce que" prioritaire sur "celui là"
		else if (Mention[1] == "PROpos") catégorie = "PRO.POS"  // "les miens"
		else if (Mention[1].contains("car")) catégorie = "PRO.NUM"  // "les deux"
		else if (Mention[1] == "PROdem") catégorie = "PRO.DEM"  // "Tout cela"
		
		else if (Mention[0].contains("DET") && Mention[1].contains("PROind")) catégorie = "PRO.IND" // "les autres"
		
		else if (Mention[0].contains("VER") && Mention[1].contains("VER")) catégorie = "SUJ.ZERO" // verbe temps composé
		
		else if (!Mention[0].contains("NOM") && !Mention[0].contains("ADJ") && !Mention[1].contains("NOM") && !Mention[1].contains("ADJ")) {
			if (Mention[0] == "PROdem") catégorie = "PRO.DEM"
			else if (Mention[1] == "VERinf") catégorie = "GN.CHECK" // Verbe substantivé
			else if (Mention[1] == "VERppa") catégorie = "GN.CHECK" // Verbe substantivé
			else if ( Mention[0].contains("PRE") && (Mention[1] == "PROper")) catégorie = "GN.CHECK" // Complément de nom
			else catégorie = "PRO.CHECK"
		}
		else catégorie = "GN.CHECK"           
	} 
	
	if ( (catégorie == null) || (catégorie == "GN.CHECK") ) {
		// on est dans les GN
		
		if (Mention[0] == "DETcar"     ) catégorie = "GN.NUM"
		else if (Mention[0] == "DETord"     ) catégorie = "GN.NUM"

		else if (Mention.contains("NOMpro")) catégorie = "GN.NAM"
				
		else if (Mention[0] == "DETpos" ) catégorie = "GN.POS"
				
		else if (Mention[0] == "PROdem" ) catégorie = "GN.DEM"
		else if (Mention[0] == "DETdem" ) catégorie = "GN.DEM"
		
		else if (Mention[0] == "PRE.DETdef" ) catégorie = "GN.DEF"
		else if (Mention[0] == "DETdef")   catégorie = "GN.DEF"
		
		else if (Mention[0] == "DETndf") catégorie = "GN.IND" 
		else if (Mention[0] == "DETind") catégorie = "GN.IND" 
		
		else if (Mention[0] == "PROind" ) catégorie = "GN.IND"
		else if (Mention[0].contains("PRP")) catégorie = "GN.IND"
		else if (Mention[0].contains("ADJ")) catégorie = "GN.IND"
		else if (Mention[0].contains("NOM")) catégorie = "GN.IND"
		
		
		
		else catégorie = "GN.CHECK"
	}


	
	return catégorie
}

//
// FIN DE LA DÉFINITION DES RÈGLES
//

// CORPS DU SCRIPT

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a MainCorpus: "+corpusViewSelection
	return
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type", usage="", widget="String", required=true, def="MENTION")
def unit_type
@Field @Option(name="pos_property_name", usage="", widget="String", required=true, def="pos")
def pos_property_name
@Field @Option(name="reset", usage="", widget="Boolean", required=true, def="true")
def reset
if (!ParametersDialog.open(this)) return

corpus = corpusViewSelection
CQI = CQPSearchEngine.getCqiClient()
word = corpus.getWordProperty()
posProperty = corpus.getProperty(pos_property_name)
if (posProperty == null) {
	println "Error: CQP corpus does not contains the word property with name=$pos_property_name"
	return
}
analecCorpus = URSCorpora.getCorpus(corpus)
vue = URSCorpora.getVue(corpus)
structure = analecCorpus.getStructure()
if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Error: corpus structure does not contains unit with name=$unit_type"
	return
}

CATEGORIE = "CATEGORIE"
// Si la structure d'annotation ne contient pas CATEGORIE, on la crée avec ses valeurs
if (!structure.getUniteProperties(unit_type).contains(CATEGORIE)) { 
// la propriété
	analecCorpus.ajouterProp(Unite.class, unit_type, CATEGORIE)
// les valeurs
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "GN.NAM")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "GN.DEF")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "GN.IND")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "GN.POS")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "GN.DEM")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "GN.NUM")
	
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "GN.CHECK")
	
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "DET.POS")
	
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.PER")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.ADV")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.IND")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.DEM")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.POS")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.NUM")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.INT")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.REL")
	
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.CHECK")
	
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "SUJ.ZERO")
	
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "ERREUR")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "ADJ.POS")
	
//...
}

if (!vue.getTypesAVoir(Unite.class).contains(unit_type)) {
	vue.ajouterType(Unite.class, unit_type)
}
if (!vue.getNomsChamps(Unite.class, unit_type).contains(CATEGORIE)) {
	vue.ajouterProp(Unite.class, unit_type, CATEGORIE)
}

def nModified = 0
def nIgnored = 0

errors = new HashMap()
def units = analecCorpus.getUnites(unit_type)
units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }
for (Unite unit : units) { // process all units
	
	def prop = unit.getProp(CATEGORIE)
	if (!reset && prop != null && prop.length() > 0) continue // l'unité a déjà une CATEGORIE
	
	int[] positions = null
	if (unit.getDeb() == unit.getFin()) positions = [unit.getDeb()]
	else positions = (unit.getDeb()..unit.getFin())
	
	def Mention = CQI.cpos2Str(posProperty.getQualifiedName(), positions)
	def cat = testRules(positions, Mention)
	
	if (cat != null) {
		vue.setValeurChamp(unit, CATEGORIE, cat)
		nModified++
	} else {
		nIgnored++
	}
}

println "Result:"
println "- $nModified units of type $unit_type have been modified."
println "- $nIgnored units of type $unit_type have not been modified.\n"

if (errors.size() > 0) {
	println "Some rules should be added to this macro to process the following remaining 'FROPOS / words' values:"
	errors.keySet().each { println "fropos="+it+"\twords="+errors[it].join(" | ") }
}
