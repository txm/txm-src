package org.txm.macro.urs.democrat

import org.apache.commons.lang.*
import org.kohsuke.args4j.*
import groovy.transform.*
import org.txm.*
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.Message.*
import visuAnalec.donnees.*
import visuAnalec.elements.*
import visuAnalec.vue.*

/*
   Distingue les Pronoms Personnels Anaphoriques (PRO.PERA) des Pronoms Personnels Déictiques (PRO.PERD)
   PRO.PERD = frlemma(je|me|moi|tu|te|toi|nous|vous)
   On rajoute aussi quelques graphies anciennes que TreeTagger ne connaît pas forcément.
   Inutile de différencier minuscules et majuscules. Le test gère cela très bien.
   Auteur : Matthieu Quignard (ICAR)
   Date : 19/12/2017
 */

listeLemmesDeictiques = ["je", "me", "moi", "tu", "te", "toi", "nous", "vous"]
listeFormesDeictiques = ["moy", "toy"]


// CORPS DU SCRIPT

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a MainCorpus: "+corpusViewSelection
	return
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type", usage="", widget="String", required=true, def="MENTION")
def unit_type
@Field @Option(name="pos_property_name", usage="", widget="String", required=true, def="frlemma")
def pos_property_name
if (!ParametersDialog.open(this)) return

corpus = corpusViewSelection
CQI = CQPSearchEngine.getCqiClient()
word = corpus.getWordProperty()
posProperty = corpus.getProperty(pos_property_name)
if (posProperty == null) {
	println "Error: CQP corpus does not contains the word property with name=$pos_property_name"
	return
}
analecCorpus = URSCorpora.getCorpus(corpus)
vue = URSCorpora.getVue(corpus)
structure = analecCorpus.getStructure()
if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Error: corpus structure does not contains unit with name=$unit_type"
	return
}

CATEGORIE = "CATEGORIE"
structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.PERA")
structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.PERD")

if (!vue.getTypesAVoir(Unite.class).contains(unit_type)) {
	vue.ajouterType(Unite.class, unit_type)
}
if (!vue.getNomsChamps(Unite.class, unit_type).contains(CATEGORIE)) {
	vue.ajouterProp(Unite.class, unit_type, CATEGORIE)
}

def nModified = 0
def nIgnored = 0
def nProPerA = 0
def nProPerD = 0

errors = new HashMap()
def units = analecCorpus.getUnites(unit_type)
units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }

for (Unite unit : units) { // process all units
	def prop = unit.getProp(CATEGORIE)
	
	if ( (prop == null) || (!prop.contains("PRO.PER")) ) {
		// On ne s'intéresse qu'aux pronoms personnels (PRO.PER)
		// Les autres sont ignorés
		nIgnored++
	} else {
		// Pour les mentions de type "PRO.PER", on cherche le premier mot
		int[] positions = null
		if (unit.getDeb() == unit.getFin()) positions = [unit.getDeb()]
		else positions = (unit.getDeb()..unit.getFin())
	
		// On récupère le lemme du premier mot
		def tags = CQI.cpos2Str(posProperty.getQualifiedName(), positions)
		def firstTag = tags[0];
		
		// On récupère la forme du premier mot en minuscule
		def forme = CQI.cpos2Str(word.getQualifiedName(), positions)[0].toLowerCase()
		
		if (listeLemmesDeictiques.contains(firstTag)) {
			// Si le lemme est dans la liste de déictiques, on recatégorise en PRO.PERD
			vue.setValeurChamp(unit, CATEGORIE, "PRO.PERD")
			nProPerD++
		} else if (listeFormesDeictiques.contains(forme)) {
			// Si la forme du premier mot dans la liste des formes des déictiques, on recatégorise en PRO.PERD
			vue.setValeurChamp(unit, CATEGORIE, "PRO.PERD")
			nProPerD++
		} else {
			// Sinon on recatégorise en PRO.PERA
			vue.setValeurChamp(unit, CATEGORIE, "PRO.PERA")
			nProPerA++
		}
		nModified++
	}
}

println "Result:"
println "- $nModified units of type $unit_type have been modified."
println "- $nIgnored units of type $unit_type have not been modified.\n"

println "- $nProPerA mentions de ProPer anaphoriques."
println "- $nProPerD mentions de ProPer déictiques."
