package org.txm.macro.urs.democrat

import org.apache.commons.lang.*
import org.kohsuke.args4j.*
import groovy.transform.*
import org.txm.*
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.analec.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.Message.*
import visuAnalec.donnees.*
import visuAnalec.elements.*
import visuAnalec.vue.*

/*
   Retouche les sujets zéros pour les recatégoriser au besoin
   en SUJ.PARTINF lorsque le verbe est au participe ou à 
   l'infinitif
   Auteur : Matthieu QUIGNARD
   Date : 05 Février 2019
 */

def listePartInf = ["VER:pper", "VER:ppre", "VER:infi", "VERppe", "VERppa", "VERinf"]

// CORPS DU SCRIPT

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a MainCorpus: "+corpusViewSelection
	return
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type", usage="", widget="String", required=true, def="MENTION")
def unit_type
@Field @Option(name="property_name", usage="", widget="String", required=true, def="CATEGORIE")
def property_name
@Field @Option(name="suj_zero_cat", usage="", widget="String", required=true, def="SUJ.ZERO")
def suj_zero_cat
@Field @Option(name="pos_property_name", usage="", widget="String", required=true, def="frpos")
def pos_property_name
if (!ParametersDialog.open(this)) return

corpus = corpusViewSelection
CQI = CQPSearchEngine.getCqiClient()
word = corpus.getWordProperty()
posProperty = corpus.getProperty(pos_property_name)
if (posProperty == null) {
	println "Error: CQP corpus does not contains the word property with name=$pos_property_name"
	return
}
analecCorpus = URSCorpora.getCorpus(corpus)
vue = URSCorpora.getVue(corpus)
structure = analecCorpus.getStructure()
if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Erreur : le corpus ne contient d'unité de type $unit_type"
	println "Script terminé"
	return
}

if (!structure.getUniteProperties(unit_type).contains(property_name)) { 
	println "Erreur : les unités $unit_type n'ont pas de propriété $property_name"
	println "Script terminé"
	return
}

if (!vue.getTypesAVoir(Unite.class).contains(unit_type)) {
	vue.ajouterType(Unite.class, unit_type)
}
if (!vue.getNomsChamps(Unite.class, unit_type).contains(property_name)) {
	vue.ajouterProp(Unite.class, unit_type, property_name)
}

def nModified = 0
def nIgnored = 0

def units = analecCorpus.getUnites(unit_type)
units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }

for (Unite unit : units) { // process all units
	def prop = unit.getProp(property_name)
	
	if (prop != suj_zero_cat) {
		// On ne s'intéresse qu'aux SUJ.ZERO
		// Les autres sont ignorés
		nIgnored++
	} else {
		// Pour les mentions de type "GN.IND", on cherche le premier mot
		int[] positions = null
		if (unit.getDeb() == unit.getFin()) positions = [unit.getDeb()]
		else positions = (unit.getDeb()..unit.getFin())
	
		// On récupère le POS du premier mot
		def tags = CQI.cpos2Str(posProperty.getQualifiedName(), positions)
		def firstTag = tags[0];
			
		if ( listePartInf.contains(firstTag) ) {
			vue.setValeurChamp(unit, property_name, "SUJ.PARTINF")
			nModified++
		} else {
			// Sinon on laisse tel quel
			nIgnored++
		}
	}
}

println "Result:"
println "- $nModified units of type $unit_type have been modified."
println "- $nIgnored units of type $unit_type have not been modified.\n"
