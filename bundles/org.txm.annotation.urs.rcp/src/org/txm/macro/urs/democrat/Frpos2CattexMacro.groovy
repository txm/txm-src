// @author Matthieu Quignard
// Date : 04 Mai 2017

/*
Définit la catégorie grammaticale des mentions d'après le champ `frpos'
(tagset de TreeTagger). 

La liste des catégories grammaticales est celle de CATTEX2009
-- Groupes nominaux :
	GN.NAM (noms propres) : Henri II
	GN.DEF (définis) : Le roi, du roi
	GN.IND (indéfinis) : Un roi
	GN.POS (possessifs) : [Mon roi]
	GN.DEM (demonstratif) : Ce roi
	GN.NUM (numéraux) : Deux rois
	GN.CHECK (GN indéterminés)
-- Déterminants
	DET.POS (possessifs) : [mon] roi
-- Pronoms
	PRO.PER (personnels) : je, moi, me, on, il, etc.
	PRO.ADV (adverbiaux) : y, en
	PRO.IND (indéfinis) : tout, tous, certains, plusieurs, etc.
	PRO.DEM (demonstratifs) : ceci, cela, ce, ça...
	PRO.POS (possessifs) : le mien, les nôtres...
	PRO.NUM (cardinaux, ordinaux) : les deux...
	PRO.REL (relatifs) : qui, que, quoi, duquel, etc.
	PRO.INT (interrogatifs)
	PRO.CHECK (pronoms indéterminés)
-- Sujet Zéro : verbes conjugués, éventuellement pronominal

-- ERREUR : erreur (a priori) de mention

	
*/

package org.txm.macro.urs.democrat

import org.apache.commons.lang.*
import org.kohsuke.args4j.*
import groovy.transform.*
import org.txm.*
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.Message.*
import visuAnalec.donnees.*
import visuAnalec.elements.*
import visuAnalec.vue.*

// Propriété donnant le lemme
def lemmaProperty = "frlemma"

/*
   TODO : TreeTagger fait un mauvais étiquetage des premiers mots de la phrase.
   La majuscule lui fait croire qu'il s'agit d'un nom propre.
   Vérifier que ce mot n'est pas en fait un pronom en testant sa présence dans une des listes de formes particulières 
*/


/**
  * Liste de formes utile à la catégorisation des mentions
  * TreeTagger n'est pas forcément très performant sur des états plus anciens de la langue
***/  

formesArticlesDéfinis = ["le", "la", "les", "l'", "au", "aux", "du", "des"]
formesArticlesIndéfinis = ["un", "une", "des"]
formesDéterminantsPossessifs = ["ma", "ta", "sa", "mon", "ton", "son", "mes", "tes", "ses", "notre", "votre", "leur", "nos", "vos", "leurs"]
formesAdjectifsDémonstratifs = ["ce", "cet", "cette", "ces"]

formesPronomsPersonnels = ["je", "tu", "il", "elle", "on", "nous", "vous", "ils", "elles", "moi", "toi", "eux", "me", "te", "se", "lui", "leur"]
formesPronomsAdverbiaux = ["en", "y"]
formesPronomsPossessifs = ["mien", "mienne", "miens", "miennes", "tien", "tienne", "tiens", "tiennes", "sien", "sienne", "siens", "siennes", "nôtre", "nôtres", "vôtre", "vôtres", "leur", "leurs"]
formesPronomsDémonstratifs = ["ce", "c'", "celui", "celle", "ceux", "celles", "ci", "ça", "ceci", "cela", "tel", "telle", "tels", "telles"]

toutesLesFormes = []
toutesLesFormes += formesArticlesDéfinis
toutesLesFormes += formesArticlesIndéfinis
toutesLesFormes += formesDéterminantsPossessifs
toutesLesFormes += formesAdjectifsDémonstratifs
toutesLesFormes += formesPronomsPersonnels
toutesLesFormes += formesPronomsAdverbiaux
toutesLesFormes += formesPronomsPossessifs
toutesLesFormes += formesPronomsDémonstratifs

/** Fin de la déclaration des formes **/



def testRules(def positions, def Mention) {
	def catégorie = null
    def forme = CQI.cpos2Str(word.getQualifiedName(), positions)[0].toLowerCase()
    
	if (Mention.length == 1) {    
	    
		     if (Mention.first() == "NAM"    ) catégorie = "GN.NAM"
		else if (Mention.first() == "DET:POS") catégorie = "DET.POS"
		else if (Mention.first() == "PRO:PER") {
			if (formesPronomsAdverbiaux.contains(forme)) catégorie = "PRO.ADV"
			else catégorie = "PRO.PER"
		}
		else if (Mention.first() == "PRO:DEM") catégorie = "PRO.DEM"
		else if (Mention.first() == "PRO:IND") catégorie = "PRO.IND"
		else if (Mention.first() == "PRO:REL") catégorie = "PRO.REL"
		else if (Mention.first().contains("VER:")) catégorie = "SUJ.ZERO"
		else if (Mention.first() == "PRO") catégorie = "PRO.INT"
		
		// GN indéfinis sans articles
		else if (Mention.first() == "NOM") catégorie = "GN.IND"
		else if (Mention.first() == "ADJ") catégorie = "GN.IND"
		
		// gestion des erreurs de TreeTagger
		else if (Mention.first() == "KON") catégorie = "PRO.REL" // Le 'que' dans une mention simple est un relatif
		else if (Mention.first() == "DET:ART") catégorie = "PRO.PER" // le, les
		else if (forme == "en") catégorie = "PRO.ADV"
		
		else if (Mention.first() == "ADV") catégorie = "ERREUR"  // un adverbe seul n'est jamais référentiel
		else if (Mention.first() == "PRE") catégorie = "ERREUR"  // une preposition seule n'est jamais référentielle
		else if (Mention.first() == "ADJ") catégorie = "ERREUR"  // un adjectif seul n'est jamais référentiel
		else if (Mention.first() == "INT") catégorie = "ERREUR"  // une interjection seule n'est jamais référentielle
		
		else catégorie = "PRO.CHECK"		
	} 
	
	else if (Mention.length == 2) {
		     if (Mention.contains("NAM")) catégorie = "GN.NAM"
		else if (Mention[1] == "PRO:POS") catégorie = "PRO.POS"  // "les miens"
		else if (Mention[1] == "NUM"    ) catégorie = "PRO.NUM"  // "les deux"
		else if (Mention[1] == "PRO:DEM") catégorie = "PRO.DEM"  // "Tout cela"
		else if (Mention[0] == "PRO:IND") catégorie = "GN.IND"   // "Quelques trucs"
		else if (Mention.contains("PRO:REL")) catégorie = "PRO.REL"
		else if ((Mention[0].contains("DET")) && (Mention[1] == "PROind")) catégorie = "PRO.IND" // des autres
		else if (Mention[1].contains("VER:")) catégorie = "SUJ.ZERO"
		else if (!Mention.contains("NOM") && !Mention.contains("ADJ")) {
			if (Mention[0] == "PRO:DEM") catégorie = "PRO.DEM"
			else catégorie = "PRO.CHECK"
		}
		else catégorie = "GN.CHECK"            
	} 
	
	if ( (catégorie == null) || (catégorie == "GN.CHECK") ) {
		// on est dans les GN
		     if (Mention[0] == "DET:POS" ) catégorie = "GN.POS"
		else if (Mention[0] == "NUM"     ) catégorie = "GN.NUM"
		else if (Mention[0] == "PRO:DEM" ) catégorie = "GN.DEM"
		else if (Mention[0] == "PRP:det" ) catégorie = "GN.DEF"
		else if (formesArticlesIndéfinis.contains(forme) || (forme == "une")) catégorie = "GN.IND" 
		else if (formesArticlesDéfinis.contains(forme))   catégorie = "GN.DEF"
		else if (Mention[0] == "PRO:IND" ) catégorie = "GN.IND"
		else if (Mention[0] == "PRP" ) catégorie = "GN.IND"
		else if (Mention[0] == "ADJ" ) catégorie = "GN.IND"
		else if (Mention[0] == "NOM" ) catégorie = "GN.IND"		
		else if (Mention.contains("NAM")) catégorie = "GN.NAM"
		else catégorie = "TEST"
	}
		
	return catégorie
}

//
// FIN DE LA DÉFINITION DES RÈGLES
//

// CORPS DU SCRIPT

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a MainCorpus: "+corpusViewSelection
	return
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type", usage="", widget="String", required=true, def="MENTION")
def unit_type
@Field @Option(name="pos_property_name", usage="", widget="String", required=true, def="pos")
def pos_property_name
@Field @Option(name="reset", usage="", widget="Boolean", required=true, def="true")
def reset
if (!ParametersDialog.open(this)) return

corpus = corpusViewSelection
CQI = CQPSearchEngine.getCqiClient()
word = corpus.getWordProperty()
posProperty = corpus.getProperty(pos_property_name)
if (posProperty == null) {
	println "Error: CQP corpus does not contains the word property with name=$pos_property_name"
	return
}
analecCorpus = URSCorpora.getCorpus(corpus)
vue = URSCorpora.getVue(corpus)
structure = analecCorpus.getStructure()
if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Error: corpus structure does not contains unit with name=$unit_type"
	return
}

CATEGORIE = "CATEGORIE"
// Si la structure d'annotation ne contient pas CATEGORIE, on la crée avec ses valeurs
if (!structure.getUniteProperties(unit_type).contains(CATEGORIE)) { 

// FIXME: dans le script original (see also
// http://forge.cbp.ens-lyon.fr/redmine/issues/2065), on utilise
// analecCorpus.ajouterProp/Val, mais cela ne marche pas dans ma version de
// TXM-Analec --> je retourne donc à structure.ajouterProp/Val

// la propriété
	structure.ajouterProp(Unite.class, unit_type, CATEGORIE)
// les valeurs
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "GN.NAM")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "GN.DEF")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "GN.IND")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "GN.POS")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "GN.DEM")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "GN.NUM")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "GN.CHECK")
	
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "DET.POS")
	
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.PER")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.ADV")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.IND")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.DEM")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.POS")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.NUM")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.INT")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.REL")
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "PRO.CHECK")
	
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "SUJ.ZERO")
	
	structure.ajouterVal(Unite.class, unit_type, CATEGORIE, "ERREUR")
}

if (!vue.getTypesAVoir(Unite.class).contains(unit_type)) {
	vue.ajouterType(Unite.class, unit_type)
}
if (!vue.getNomsChamps(Unite.class, unit_type).contains(CATEGORIE)) {
	vue.ajouterProp(Unite.class, unit_type, CATEGORIE)
}

def nModified = 0
def nIgnored = 0

errors = new HashMap()
def units = analecCorpus.getUnites(unit_type)
units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }
for (Unite unit : units) { // process all units
	
	def prop = unit.getProp(CATEGORIE)
	if (!reset && prop != null && prop.length() > 0) continue // l'unité a déjà une CATEGORIE
	
	int[] positions = null
	if (unit.getDeb() == unit.getFin()) positions = [unit.getDeb()]
	else positions = (unit.getDeb()..unit.getFin())
	
	def Mention = CQI.cpos2Str(posProperty.getQualifiedName(), positions)
	def cat = testRules(positions, Mention)

	if (cat != null) {
		// following line in the original script but doesn't work for me:
      // vue.setValeurChamp(unit, CATEGORIE, cat)
      unit.getProps().put(CATEGORIE, cat)
		nModified++
	} else {
		nIgnored++
	}
}

println "Result:"
println "- $nModified units of type $unit_type have been modified."
println "- $nIgnored units of type $unit_type have not been modified.\n"

if (errors.size() > 0) {
	println "Some rules should be added to this macro to process the following remaining 'FROPOS / words' values:"
	errors.keySet().each { println "fropos="+it+"\twords="+errors[it].join(" | ") }
}
