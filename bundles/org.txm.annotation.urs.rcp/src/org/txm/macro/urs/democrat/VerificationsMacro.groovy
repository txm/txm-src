// Auteur Matthieu Quignard, Alexey Lavrentev
// Date : 14 janvier 2019

/**********
Vérifications automatiques :
1. Repère les mentions sans catégorie : CHECK > CAT
2. Repère les mentions sans référent : CHECK > REF
3. Supprime les ponctuations en début et en fin de mention : CHECK > BORNES
4. Supprime les prépositions autres que 'de' en début de mention : CHECK > BORNES
5. Supprime automatiquement toutes les mentions vides = sans aucun mot = de longueur 0
6. Détecter les mentions qui ont exactement les mêmes bornes  : CHECK > DOUBLON
7 (option). Détecter les pronoms hors mention : CHECK > NEW
***********/

package org.txm.macro.urs.democrat

import org.apache.commons.lang.*
import org.kohsuke.args4j.*
import groovy.transform.*
import org.txm.*

import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.urs.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.Message.*
import visuAnalec.donnees.*
import visuAnalec.elements.*
import visuAnalec.vue.*

// TODO : ajouter les étiquettes équivalentes issues du tagset TreeTagger
// Ponctuations et Prépositions
def interditsAuDebut = ["PONfbl", "PONfrt", "PONpxx", "PRE"]
// Ponctuations
def interditsALaFin = ["PONfbl", "PONfrt", "PONpxx"]
// Pronoms en tous genres
def listePronoms = ["PROadv", "PROcar", "PROdem", "PROimp", "PROind", "PROint", "PROper", "PROpos", "PROrel"]

// CORPS DU SCRIPT

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a MainCorpus: "+corpusViewSelection
	return
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type", usage="Unité", widget="String", required=true, def="MENTION")
def unit_type
@Field @Option(name="pos_property_name", usage="Etiquette de morphosyntaxe", widget="String", required=true, def="frpos")
def pos_property_name
@Field @Option(name="cat_name", usage="Propriété CATEGORIE", widget="String", required=true, def="CATEGORIE")
def cat_name
@Field @Option(name="ref_name", usage="Propriété REF", widget="String", required=true, def="REF")
def ref_name
@Field @Option(name="checkPronouns", usage="Vérifier les pronoms oubliés", widget="Boolean", required=true, def="true")
def checkPronouns

if (!ParametersDialog.open(this)) return

corpus = corpusViewSelection
CQI = CQPSearchEngine.getCqiClient()
word = corpus.getWordProperty()
posProperty = corpus.getProperty(pos_property_name)
if (posProperty == null) {
	println "Error: CQP corpus does not contains the word property with name=$pos_property_name"
	return
}

analecCorpus = URSCorpora.getCorpus(corpus)
vue = URSCorpora.getVue(corpus)
structure = analecCorpus.getStructure()

if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Error: corpus structure does not contains unit with name=$unit_type"
	return
}

if (!structure.getUniteProperties(unit_type).contains(cat_name)) { 
	println "Erreur : les unités $unit_type n'ont pas de propriété $cat_name"
	return
}

if (!structure.getUniteProperties(unit_type).contains(ref_name)) { 
	println "Erreur : les unités $unit_type n'ont pas de propriété $ref_name"
	return
}

println "Détection des pronoms oubliés : $checkPronouns"

// Reinitialiser la propriété CHECK
if (!structure.getUniteProperties(unit_type).contains("CHECK")) {
	analecCorpus.ajouterProp(Unite.class, unit_type, "CHECK")
} else {
	println "Nettoyage des anciennes annotations CHECK"
  	def tmpvalues = new HashSet()
	tmpvalues.addAll(structure.getValeursProp(Unite.class, unit_type, "CHECK"));
	for (String val : tmpvalues) {
		structure.supprimerVal(Unite.class, unit_type, "CHECK", val);
		//println "suppression de l'étiquette $val"
	}
}

structure.ajouterVal(Unite.class, unit_type, "CHECK", "DONE")

	

def nModified = 0
def nIgnored = 0
def nDeleted = 0
def nAdded = 0

def garbageBin = []

def nToks = corpus.getSize()
def tokenIndex = new int[nToks]

def i = 0
for (i=0 ; i< nToks ; i++) tokenIndex[i] = 1

errors = new HashMap()
def units = analecCorpus.getUnites(unit_type)
units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }

// pour les doublons
def lastUnit = null

for (Unite unit : units) { // process all units
	def erreur = ""
	
	// 1. Catégories vides
	def cat = unit.getProp( cat_name );
	if (cat == "") erreur += "CAT "
	
	// 2. Référents vides (plus grave) ; pas besoin de catégories
	def ref = unit.getProp( ref_name );
	if (ref == "") erreur += "REF "
	
	// 3. Suppression des erreurs initiales ; besoin de catégories
	int[] positions = null
	if (unit.getDeb() == unit.getFin()) positions = [unit.getDeb()]
	else positions = (unit.getDeb()..unit.getFin())
	def Mention = CQI.cpos2Str(posProperty.getQualifiedName(), positions)
	
	def isOK = false
	while (isOK == false) {
	   if (interditsAuDebut.contains(Mention[0])) {
	      if (positions.size() == 1) {
	         erreur += "SUPPR"
	         isOK = true
	      } else {
	         def debut = unit.getDeb()
	         unit.setDeb(  debut + 1 )
	         
	         if (unit.getDeb() == unit.getFin()) positions = [unit.getDeb()]
			 else positions = (unit.getDeb()..unit.getFin())
			 Mention = CQI.cpos2Str(posProperty.getQualifiedName(), positions)
			 if (!erreur.contains("BORNESG")) erreur += "BORNESG "
	      }
	   } else { 
	      isOK = true
	   } 
	}
	
	
	// 4. Suppression des erreurs de borne de fin ; besoin de catégories
	if (unit.getDeb() == unit.getFin()) positions = [unit.getDeb()]
	else positions = (unit.getDeb()..unit.getFin())
	Mention = CQI.cpos2Str(posProperty.getQualifiedName(), positions)
	
	isOK = false
	while ((isOK == false) && (erreur != "remove")) {
		def n = Mention.size()
		if (interditsALaFin.contains(Mention[ n-1 ])) {
			if (positions.size() == 1) {
	         if (!erreur.contains("SUPPR")) erreur += "SUPPR"
	         isOK = true
	      } else {
	         def fin = unit.getFin()
	         unit.setFin(  fin - 1 )
	         
	         if (unit.getDeb() == unit.getFin()) positions = [unit.getDeb()]
			 else positions = (unit.getDeb()..unit.getFin())
			 Mention = CQI.cpos2Str(posProperty.getQualifiedName(), positions)
			 if (!erreur.contains("BORNESD")) erreur += "BORNESD "
	      }
		} else isOK = true
	}
	
	// 5. Suppression des unités problématiques (bornes incohérentes ou mot vide) ; pas besoin de catégories
	def forme = CQI.cpos2Str(word.getQualifiedName(), positions)[0].trim().toLowerCase()
	
	if (erreur == "remove") {}
	else if (unit.getFin() < unit.getDeb()) {
	   println "ERREUR GRAVE : segmentation incohérente"
	   erreur = "remove"
	} else if ( forme.length() == 0 ) {
	   println "ERREUR GRAVE : unité sans mot"
	   erreur = "remove"
	}
	
	// 6. Détection des doublons ; pas besoin de catégories
	if (lastUnit != null) {
	   if ((unit.getDeb() == lastUnit.getDeb()) && (unit.getFin() == lastUnit.getFin()) ) {
	   		erreur += " DOUBLON "
	   }
	}
	lastUnit = unit
	
	
	erreur = erreur.trim()
	if (erreur == "remove") {
	    garbageBin.add( unit )
		nDeleted++
	} else if (erreur != "") {
		vue.setValeurChamp(unit, "CHECK", erreur)
		nModified++
	} else {
		nIgnored++
	}
	
	// mise à jour des tokens couverts
	for (int p=unit.getDeb() ; p <= unit.getFin() ; p++) {
	   tokenIndex[p] = 0
	}
}

// Suppression effective des unités incohérentes
garbageBin.each {
   analecCorpus.supUnite( it )
}

// 7. Ajouter les pronoms non couverts par une annotation ; besoin de catégories
// Parcourir les tokens non couverts ; obtenir leur POS et créer une unité si c'est un pronom

if (checkPronouns) {
	println "Détection des pronoms oubliés"
	for (i=0 ; i < nToks ; i++) {
    	if (tokenIndex[i] > 0) {
			Mention = CQI.cpos2Str(posProperty.getQualifiedName(), i)
			if (listePronoms.contains(Mention[0])) {
	    		def props = [:]
        		props["CHECK"] = "NEW"
        		Unite u = analecCorpus.addUniteSaisie(unit_type, i, i, props)
        		vue.setValeurChamp(u, "CHECK", "NEW")
	    		nAdded++
	    	}
		}
	}
}

if (nAdded + nModified + nDeleted > 0) corpus.setIsModified(true);

println "Result:"
println "- $nModified units of type $unit_type have been modified."
println "- $nDeleted units of type $unit_type have been deleted."
println "- $nIgnored units of type $unit_type have not been modified."
println "- $nAdded forgotten pronominal units of type $unit_type have been added.\n"

