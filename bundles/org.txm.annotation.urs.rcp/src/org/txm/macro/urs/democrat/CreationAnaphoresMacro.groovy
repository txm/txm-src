// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// STANDARD DECLARATIONS
package org.txm.macro.urs.democrat

import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.corpus.*

import visuAnalec.donnees.Structure;
import visuAnalec.elements.Relation;
import visuAnalec.elements.Schema
import visuAnalec.elements.Unite;
import visuAnalec.vue.Vue

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a MainCorpus: "+corpusViewSelection
	return;
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type",usage="", widget="String", required=true, def="MENTION")
		String unit_type
@Field @Option(name="schema_type",usage="", widget="String", required=true, def="CHAINE")
		String schema_type
if (!ParametersDialog.open(this)) return;

int nCreated = 0 // count the number of created RELATION

MainCorpus corpus = corpusViewSelection
def analecCorpus = URSCorpora.getCorpus(corpus);
Structure structure = analecCorpus.getStructure()
if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Error: corpus structure does not contains unit with name=$unit_type"
	return
}
if (!structure.getSchemas().contains(schema_type)) { // check if the structure contains the unit_type units
	println "Error: corpus structure does not contains schema with name=$schema_type"
	return
}
if (!structure.getRelations().contains("ANAPHORE")) { // update the structure if needed
	println "Creating the 'ANAPHORE' relation in the structure"
	structure.ajouterType(Relation.class, "ANAPHORE")
	analecCorpus.ajouterProp(Relation.class, "ANAPHORE", "TYPE")
	analecCorpus.ajouterVal(Relation.class, "ANAPHORE", "TYPE", "COREFERENTE")
	analecCorpus.ajouterVal(Relation.class, "ANAPHORE", "TYPE", "ASSOCIATIVE")
}
if (analecCorpus.getRelations("ANAPHORE").size() > 0) {
	println "Error: This macro can't update existing Relations"
	return
}
Vue vue = URSCorpora.getVue(corpus)
if (!vue.getTypesAVoir(Relation.class).contains("ANAPHORE")) {
	vue.ajouterType(Relation.class, "ANAPHORE")
}
if (!vue.getNomsChamps(Relation.class, "ANAPHORE").contains("TYPE")) {
	vue.ajouterProp(Relation.class, "ANAPHORE", "TYPE")
}

for (Schema schema : analecCorpus.getSchemas(schema_type)) { // parse all CHAINE
	def units = []
	for (Unite unit : schema.getUnitesSousjacentes()) { // keep only the 'unit_type' units
		if (unit.type.equals(unit_type)) units << unit
	}
	units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() } // sort them
	
	for (int i = 0 ; i < units.size() - 1 ; i++) { // build RELATIONS and don't process the last unit
		println "creating relation with "+units[i+1].getProps()+", "+units[i].getProps()
		Relation relation = new Relation("ANAPHORE", units[i+1], units[i])
		relation.getProps().put("TYPE", "COREFERENTE")
		analecCorpus.addRelationLue(relation)  // add the new relation
		nCreated++;
	}
}

println "nCreated=$nCreated"
