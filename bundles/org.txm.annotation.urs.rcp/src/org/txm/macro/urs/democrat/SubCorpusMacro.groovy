package org.txm.macro.urs.democrat;

	// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
	// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
	// @author mdecorde
	// @author sheiden
	
	
	import org.kohsuke.args4j.*
	import groovy.transform.Field
	import org.txm.rcpapplication.swt.widget.parameters.*
	import org.txm.searchengine.cqp.*
	import org.txm.searchengine.cqp.corpus.*
	import org.txm.searchengine.cqp.corpus.query.*
	import org.txm.*
	import org.txm.rcpapplication.views.*
	
	// BEGINNING OF PARAMETERS
	
	// Declare each parameter here
	// (available widget types: Query, File, Folder, String, Text, Boolean, Integer, Float and Date)
	
	@Field @Option(name="name", usage="name of the subcorpus result", widget="String", required=true, def="subcorpus1")
	def name
	
	@Field @Option(name="query", usage="query to build subcorpus", widget="String", required=true, def="n:[] :: n = 0 expand to 10")
	def query
	/*
	
	n:[] [] [] [] [] [] [] [] [] [] :: n = 0
	n:[] :: n = 0 expand to 10
	
	*/
	
	// Open the parameters input dialog box
	if (!ParametersDialog.open(this)) return false
	
	def CQI = CQPSearchEngine.getCqiClient()
	
	def scriptName = this.class.getSimpleName()
	
	def utils = new org.txm.macro.urs.exploit.CQPUtils()
	
	def corpora = utils.getCorpora(this)
	
	if ((corpora == null) || corpora.size() == 0) {
		println "** $scriptName: please select a corpus in the Corpus view or provide a corpus name. Aborting."
		return false
	}
	
	/*
	CorpusManager.getCorpusManager().getCorpora().each {
		if (it.getSubcorpusByName(name)) {
			println "** Subcorpus: sub-corpus name already used. Please use another sub-corpus name. Aborting."
			return false
		}
		// it.getSubcorpora().each { sub -> println it.getName()+" subcorpora: "+sub.getName() }
	}
	*/
	
	String resultCqpId = CqpObject.subcorpusNamePrefix + CQPCorpus.getNextSubcorpusCounter()
	String queryString = "$resultCqpId=$query;"
	
	CQI.query(queryString)
	
	def subcorpus = corpora[0].createSubcorpus(new CQLQuery("$query;"), name)
	
	if (binding.variables["monitor"]) {
		utils.refreshCorpusViewExpand(monitor, subcorpus.getParent())
	}
	
	return true


