package org.txm.macro.urs.democrat

import org.txm.searchengine.cqp.corpus.*
import org.txm.rcpapplication.views.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.Toolbox

def scriptName = this.class.getSimpleName()
def utils = new org.txm.macro.urs.exploit.CQPUtils()

// Création des 4 sous-corpus de chapitres

[1, 2, 3, 4].each { chapitre ->
	res = gse.run(SubCorpusMacro, ["args":["name":"Chapitre "+chapitre, "query":"[_.div_n1=\""+chapitre+"\"] expand to div1" ],
                                "selection":selection,
                                "selections":selections,
                                "corpusViewSelection":corpusViewSelection,
                                "corpusViewSelections":corpusViewSelections,
                                "monitor":monitor])
	if (!res) println "** problem calling SubCorpusMacro."
}


// Définition des paragraphes dans les 4 chapitres

def para = [
[1, 14, 17, 22],
[2, 29, 44, 46],
[3, 53, 63, 66],
[4, 74, 75, 79]
]

// get the CQP corpus
if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "Corpus view selection is no a corpus: $corpusViewSelection"
	return false;
}
def corpus = corpusViewSelection
if (corpus.getName() != "DIDEROTESSAIS") {
	println "Warning: this macro was made to work for the DIDEROTESSAIS corpus"
}
	
para.each { chapitre ->

	println chapitre

	def subcorpus = corpus.getSubcorpusByName("Chapitre "+chapitre[0])

	println "chap = "+subcorpus

	[1, 2, 3].each { num ->

		def p = chapitre[num]

		println "p = "+p

		name = "p "+p
		query = '[_.p_n=\"'+p+'\"] expand to p'

		res = gse.run(SubCorpusMacro, ["args":[ "name":name, "query":query ],
                                "corpusViewSelections":[subcorpus],
								"corpusViewSelection":subcorpus,
                                "monitor":monitor])

	} // [1, 2, 3].each

} // para.each
