// @author Bruno Oberlé
// v1.0.0 20170711

// Cette macro individualise tous les noms de référents "SI" en leur attribuant un numéro unique (SI_1, SI_2, etc.).

package org.txm.macro.urs.democrat

import org.apache.commons.lang.*
import org.kohsuke.args4j.*
import groovy.transform.*
import org.txm.*
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.Message.*
import visuAnalec.donnees.*
import visuAnalec.elements.*
import visuAnalec.vue.*

// CORPS DU SCRIPT

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a MainCorpus: "+corpusViewSelection
	return
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type", usage="", widget="String", required=true, def="MENTION")
def unit_type
@Field @Option(name="ref_property_name", usage="", widget="String", required=true, def="REF")
def ref_property_name
if (!ParametersDialog.open(this)) return

corpus = corpusViewSelection
CQI = CQPSearchEngine.getCqiClient()
word = corpus.getWordProperty()
analecCorpus = URSCorpora.getCorpus(corpus)
vue = URSCorpora.getVue(corpus)
structure = analecCorpus.getStructure()
if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Error: corpus structure does not contains unit with name=$unit_type"
	return
}

if (!structure.getUniteProperties(unit_type).contains(ref_property_name)) {
	println "Error: corpus structure does not contains property name=$unit_type"
	return
} 

def nModified = 0
def nIgnored = 0

def units = analecCorpus.getUnites(unit_type)
//units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }

def refSet = new HashSet()
for (Unite unit : units) { // process all units
	def prop = unit.getProp(ref_property_name)
	refSet.add(prop)
}

def counter = 1
for (Unite unit : units) { // process all units
	
	def prop = unit.getProp(ref_property_name)
	if (prop && prop == "SI") {
		def name = "SI_" + counter
		while (refSet.contains(name)) {
			counter++
			name = "SI_" + counter
		}
		counter++
		//println "old prop"+ prop
		unit.getProps().put(ref_property_name, name)
		//println "new prop"+ name
		nModified++
	} else {
		nIgnored++
	}
}

println "Result:"
println "- $nModified units of type $unit_type have been modified."
println "- $nIgnored units of type $unit_type have not been modified.\n"
