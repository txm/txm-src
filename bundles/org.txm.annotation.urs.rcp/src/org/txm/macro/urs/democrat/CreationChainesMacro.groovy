// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// STANDARD DECLARATIONS
package org.txm.macro.urs.democrat

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.donnees.Structure;
import visuAnalec.elements.Schema
import visuAnalec.elements.Unite;
import visuAnalec.vue.Vue

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a MainCorpus: "+corpusViewSelection
	return;
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type",usage="", widget="String", required=true, def="MENTION")
String unit_type
@Field @Option(name="ref_property",usage="", widget="String", required=true, def="REF")
String ref_property
if (!ParametersDialog.open(this)) return;

int nCreated = 0 // count the number of created CHAINE
int nUpdated = 0 // count the number of updated CHAINE

MainCorpus corpus = corpusViewSelection
def analecCorpus = URSCorpora.getCorpus(corpus); 
Structure structure = analecCorpus.getStructure()
if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Error: corpus structure does not contains unit with name=$unit_type"
	return
}
if (!structure.getSchemas().contains("CHAINE")) { // update the structure if needed
	println "Creating the 'CHAINE' schema in the structure"
	analecCorpus.ajouterType(Schema.class, "CHAINE")
	analecCorpus.ajouterProp(Schema.class, "CHAINE", "REF")
	analecCorpus.ajouterProp(Schema.class, "CHAINE", "GENRE")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "GENRE", "INDETERMINABLE")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "GENRE", "FEMININ")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "GENRE", "MASCULIN")
	analecCorpus.ajouterProp(Schema.class, "CHAINE", "NOMBRE")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "NOMBRE", "GROUPE_FLOU")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "NOMBRE", "GROUPE_STRICT")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "NOMBRE", "SINGULIER")
	analecCorpus.ajouterProp(Schema.class, "CHAINE", "NB MAILLONS")
	analecCorpus.ajouterProp(Schema.class, "CHAINE", "TYPE REFERENT")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "TYPE REFERENT", "UNKNOWN")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "TYPE REFERENT", "CONCRET_OBJECT")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "TYPE REFERENT", "ABSTRACT_OBJECT")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "TYPE REFERENT", "TIME")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "TYPE REFERENT", "PRODUCT")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "TYPE REFERENT", "AMOUNT")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "TYPE REFERENT", "EVENT")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "TYPE REFERENT", "GPE")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "TYPE REFERENT", "ORG")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "TYPE REFERENT", "PERSON")
	analecCorpus.ajouterVal(Schema.class, "CHAINE", "TYPE REFERENT", "LIEU")
}

Vue vue = URSCorpora.getVue(corpus)
if (!vue.getTypesAVoir(Schema.class).contains("CHAINE")) {
	vue.ajouterType(Schema.class, "CHAINE")
}
if (!vue.getNomsChamps(Schema.class, "CHAINE").contains("REF")) {
	vue.ajouterProp(Schema.class, "CHAINE", "REF")
}
if (!vue.getNomsChamps(Schema.class, "CHAINE").contains("GENRE")) {
	vue.ajouterProp(Schema.class, "CHAINE", "GENRE")
}
if (!vue.getNomsChamps(Schema.class, "CHAINE").contains("NOMBRE")) {
	vue.ajouterProp(Schema.class, "CHAINE", "NOMBRE")
}
if (!vue.getNomsChamps(Schema.class, "CHAINE").contains("NB MAILLONS")) {
	vue.ajouterProp(Schema.class, "CHAINE", "NB MAILLONS")
}
if (!vue.getNomsChamps(Schema.class, "CHAINE").contains("TYPE")) {
	vue.ajouterProp(Schema.class, "CHAINE", "TYPE")
}

def props = structure.getUniteProperties(unit_type)
if (!props.contains(ref_property)) { // check the unit_type units have the REF property
	println "Error: $unit_type units have no proprerty named '$ref_property'"
	return
}

// parse the units to build CHAINES
def chaines = [:]
def units = analecCorpus.getUnites(unit_type)
units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }
for (Unite unit : units) {
	def ref = unit.getProp(ref_property)
	if (!chaines.containsKey(ref)) {
		chaines[ref] = []
	}
	chaines[ref] << unit
}

// update the already existing CHAINES schemas
for (Schema schema : analecCorpus.getSchemas("CHAINE")) {
	String ref = schema.getProp(ref_property)
	if (chaines.containsKey(ref)) { // the CHAINE exists 
		// maj des unités de la chaine existante
		int size_before = schema.getContenu().size()
		for (def unit : chaines[ref]) schema.ajouter(unit) // insert the new units in the hashset
		
		// Update the CHAINE size
		schema.props.put("NB MAILLONS", Integer.toString(schema.contenu.size())) 
		
		// remove the inserted CHAINE from 'chaines'
		chaines.remove(ref)
		if (size_before < schema.getContenu().size()) // if the size changed, then the CHAIEN have been updated
			nUpdated++
	}
}

// create the remaining CHAINES schemas
for (def ref : chaines.keySet()) { // process the remaining CHAINE of 'chaines'
	nCreated++;
	Schema schema = new Schema()
	schema.type = "CHAINE"
	schema.props.put("REF", ref) 
	schema.props.put("GENRE", "") // set default values
	schema.props.put("NOMBRE", "") // set default values
	schema.props.put("NB MAILLONS", Integer.toString(chaines[ref].size())) 
	schema.props.put("TYPE REFERENT", "") // set default values
	
	for (def unit : chaines[ref]) schema.ajouter(unit) // insert the new units in the hashset
	
	analecCorpus.addSchemaLu(schema)  // add the new schema
}

if (nUpdated > 0 || nCreated > 0) corpus.setIsModified(true);
println "nUpdated=$nUpdated"
println "nCreated=$nCreated"
