// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden
// STANDARD DECLARATIONS
package org.txm.macro.urs.democrat

import org.apache.commons.lang.StringUtils;
import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.Toolbox;
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.CQPSearchEngine
import visuAnalec.Message.StructureEvent;
import visuAnalec.Message.TypeModifStructure;
import visuAnalec.donnees.Structure;
import visuAnalec.elements.Unite;
import visuAnalec.vue.Vue

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "Corpora selection is not a Corpus"
	return;
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type",usage="", widget="String", required=true, def="MENTION")
String unit_type
@Field @Option(name="reset",usage="", widget="Boolean", required=true, def="false")
boolean reset

if (!ParametersDialog.open(this)) return;

def corpus = corpusViewSelection
AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();
def word = corpus.getWordProperty()
def analecCorpus = URSCorpora.getCorpus(corpus);
Structure structure = analecCorpus.getStructure()
if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Error: corpus structure does not contains unit with name=$unit_type"
	return
}
def props = structure.getUniteProperties(unit_type)

String DEFINITUDE = "DEFINITUDE"
if (!props.contains(DEFINITUDE)) { // update the structure if needed
	analecCorpus.ajouterProp(Unite.class, unit_type, DEFINITUDE);
	analecCorpus.ajouterVal(Unite.class, unit_type, DEFINITUDE, "DEFINI");
	analecCorpus.ajouterVal(Unite.class, unit_type, DEFINITUDE, "INDEFINI");
	analecCorpus.ajouterVal(Unite.class, unit_type, DEFINITUDE, "DEMONSTRATIF");
	analecCorpus.ajouterVal(Unite.class, unit_type, DEFINITUDE, "AMBIGU");
	analecCorpus.ajouterVal(Unite.class, unit_type, DEFINITUDE, "NONE");
}
Vue vue = URSCorpora.getVue(corpus)
if (!vue.getTypesAVoir(Unite.class).contains(unit_type)) {
	vue.ajouterType(Unite.class, unit_type)
}
if (!vue.getNomsChamps(Unite.class, unit_type).contains(DEFINITUDE)) {
	vue.ajouterProp(Unite.class, unit_type, DEFINITUDE)
}

int nIgnored = 0 // number of ignored units
int nModified = 0 // number of modified units
int nDefini = 0 // number of "DEFINI" units
int nIndefini = 0 // number of "InDEFINI" units
int nDemonstratif = 0 // number of "DEMONSTRATIF" units
int nAmbigu = 0 // number of "AMBIGU" units
int nNone = 0 // number of "NONE" units

def units = analecCorpus.getUnites(unit_type)
units = units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }
for (Unite unit : units) { // process all units
	
	def prop = unit.getProp(DEFINITUDE);
	
	int[] pos = null
	if (unit.getDeb() == unit.getFin()) pos = [unit.getDeb()]
	else pos = (unit.getDeb()..unit.getFin())
	def form = StringUtils.join(CQI.cpos2Str(word.getQualifiedName(), pos), " ")
	
	if (reset || prop == null || prop.length() == 0 || prop.equals("NONE")) {
		// petits ajouts à faire ? : |(ses\s.+)|(Ses\s.+)|(son\s.+)|(Son\s.+)|(sa\s.+)|(Sa\s.+)|(leurs?\s.+)|(Leurs?\s.+)|(tous\s.+)|(Tous\s.+)|(toutes\s.+)|(Toutes\s.+)
		if (form =~ /^(le\s.+)|(Les\s.+)|(Le\s.+)|(la\s.+)|(La\s.+)|(l'.+)|(L'.+)|(les\s.+)|(au\s.+)|(Au\s.+)|(aux\s.+)|(Aux\s.+)|(du\s.+)|(Du\s.+)/) {
			unit.getProps().put(DEFINITUDE, "DEFINI")
			nDefini++
		} else if (form =~ /^(un\s.+)|(une\s.+)|(Un\s.+)|(Une\s.+)|(Chaque\s.+)|(chaque\s.+)|(Certains\s.+)|(Certaines\s.+)|(certains\s.+)|(certaines\s.+)|(aucun\s.+)|(aucune\s.+)|(Aucun\s.+)|(Aucunes\s.+)|(Autre\s.+)|(Autre\s.+)|(autres\s.+)|(autre\s.+)|(quelque\s.+)|(quelques\s.+)|(Quelque\s.+)|(Quelques\s.+)/) {
			unit.getProps().put(DEFINITUDE, "INDEFINI")
			nIndefini++
		} else if (form =~ /^(ce\s.+)|(cette\s.+)|(Cette\s.+)|(cet\s.+)|(ces\s.+)|(Ce\s.+)|(Cet\s.+)|(Ces\s.+)/) {
			unit.getProps().put(DEFINITUDE, "DEMONSTRATIF")
			nDemonstratif++
		} else if (form =~ /^(des\s.+)|(de\s.+)|(Des\s.+)|(De\s.+)/) {
			unit.getProps().put(DEFINITUDE, "AMBIGU")
			nAmbigu++
		} else {
			unit.getProps().put(DEFINITUDE, "NONE")
			nNone++;
		}
		nModified++
		
	} else {
		// nothing to do
		nIgnored++
	}
}

println "nIgnored=$nIgnored"
println "nModified=$nModified"
println " nDefini=$nDefini"
println " nIndefini=$nIndefini"
println " nDemonstratif=$nDemonstratif"
println " nAmbigu=$nAmbigu"
println " nNone=$nNone"