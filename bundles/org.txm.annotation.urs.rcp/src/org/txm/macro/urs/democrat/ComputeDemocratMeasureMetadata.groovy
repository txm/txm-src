package org.txm.macro.urs.democrat

import org.txm.searchengine.cqp.CQPSearchEngine
import org.txm.searchengine.cqp.corpus.MainCorpus
import org.txm.searchengine.cqp.corpus.StructuralUnit
import org.txm.searchengine.cqp.corpus.query.CQLQuery
import visuAnalec.donnees.Corpus
import org.txm.annotation.urs.URSCorpora
import org.txm.properties.core.functions.*

class ComputeDemocratMeasureMetadata {
	
	def ignored = ["txmcorpus", "text", "body"]
	
	def getStats(MainCorpus corpus, def xpathProperties) {
		def data = [:]
		
		data["number of words"] = corpus.query(new CQLQuery("[]"), "TMP", false).getNMatch();
		
		Corpus analecCorpus = URSCorpora.getCorpus(corpus);
		def chaines = analecCorpus.getSchemas("CHAINE");
		def refchaines = [];
		for (def chaine : chaines) {
			if (chaine.getUnitesSousjacentes().length >= 3) {
				refchaines << chaine
			}
		}
		data["number of coreference chains"] = chaines.size()
		data["number of referring chains"] = refchaines.size()
		data["number of referring expressions"] = analecCorpus.getUnites("MENTION").size()
		
		// add default xpaths for measures
		for (String s : ["number of words", "number of coreference chains", "number of referring chains", "number of referring expressions"]) {
			if (!xpathProperties.containsKey(s)) {
				xpathProperties.put(s,"/tei:TEI/tei:teiHeader/tei:fileDesc/tei:extent/tei:measure[@unit=\"$s\"]/@quantity")
			}
		}
		
		// add structure measures
		for (StructuralUnit su : corpus.getStructuralUnits()) {
			if (!ignored.contains(su.getName())) {
				String s = "structure "+su.getName()
				data[s] = corpus.query(new CQLQuery("<"+su.getName()+">[]"), "TMP", false).getNMatch();
				if (!xpathProperties.containsKey(s)) {
					xpathProperties.put(s,"/tei:TEI/tei:teiHeader/tei:fileDesc/tei:extent/tei:measure[@unit=\"$s\"]/@quantity")
				}
			}
		}
		
		return data
	}
}
