// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden
// STANDARD DECLARATIONS
package org.txm.macro.urs.democrat

import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.corpus.*

import visuAnalec.donnees.Structure;
import visuAnalec.elements.Unite;
import visuAnalec.vue.Vue

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a MainCorpus: "+corpusViewSelection
	return;
}

// BEGINNING OF PARAMETERS
@Field @Option(name="unit_type",usage="", widget="String", required=true, def="MENTION")
String unit_type
@Field @Option(name="reset",usage="", widget="Boolean", required=true, def="true")
boolean reset

if (!ParametersDialog.open(this)) return;

MainCorpus corpus = corpusViewSelection
def analecCorpus = URSCorpora.getCorpus(corpus);
Structure structure = analecCorpus.getStructure()
if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Error: corpus structure does not contains unit with name=$unit_type"
	return
}
def props = structure.getUniteProperties(unit_type)

String NEW = "NEW"
String REF = "REF"
String YES = "YES"
String NO = "NO"
if (!props.contains(NEW)) { // update the structure if needed
	analecCorpus.ajouterProp(Unite.class, unit_type, NEW);
	analecCorpus.ajouterVal(Unite.class, unit_type, NEW, "YES");
	analecCorpus.ajouterVal(Unite.class, unit_type, NEW, "NO");
}

if (!props.contains(REF)) { // check the unit_type units have the REF property
	println "Error: $unit_type units have no proprerty named 'REF'"
	return
}

Vue vue = URSCorpora.getVue(corpus)
if (!vue.getTypesAVoir(Unite.class).contains(unit_type)) {
	vue.ajouterType(Unite.class, unit_type)
}
if (!vue.getNomsChamps(Unite.class, unit_type).contains(NEW)) {
	vue.ajouterProp(Unite.class, unit_type, NEW)
}

int nIgnored = 0; // number of units ignored
int nYes = 0 // number of "YES" unit set
int nNo = 0 // number of "NO" unit set

def allRefs = new HashSet<String>() // store the references already seen, allow to set the 'NEW' property to 'YES' or 'NO'
def units = analecCorpus.getUnites(unit_type)
units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }
for (Unite unit : units) {
	def prop = unit.getProp(NEW);
	def ref = unit.getProp(REF);
	if (reset || prop == null || prop.length() == 0) {
		if (allRefs.contains(ref)) {
			unit.getProps().put(NEW, NO)
			nNo++
		} else { // this is the first MENTION of the CHAINE
			allRefs << ref
			unit.getProps().put(NEW, YES)
			nYes++
		}
	} else {
		// nothing to do "NEW" already exists
		nIgnored++
	}
}

println "nIgnored=$nIgnored"
println "nYes=$nYes"
println "nNo=$nNo"