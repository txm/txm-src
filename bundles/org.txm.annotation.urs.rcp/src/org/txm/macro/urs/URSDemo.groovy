package org.txm.macro.urs

import org.txm.annotation.urs.URSCorpora
import org.txm.searchengine.cqp.corpus.CQPCorpus
import visuAnalec.elements.*

// get the CQP corpus
if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "Corpus view selection is no a corpus: $corpusViewSelection"
	return false;
}
def corpus = corpusViewSelection

// get the Analec corpus : stores the annotations and annotation structure

def analecCorpus = URSCorpora.getCorpus(corpus)

// get the annotations structure : stores the available annotation types, properties and values

def structure = analecCorpus.getStructure()

// get the annotations view : stores annotations and annotation structure display rules

def view = URSCorpora.getVue(corpus)

// set a default vue -> all is visible
view.retablirVueParDefaut()

// list the viewed types and properties
for (def type : view.getTypesAVoir(Unite.class)) {
	println "$type: "+view.getNomsChamps(Unite.class, type)
}

// manage the viewed&enabled type, properties ; the types and properties must be enabled to use the "view.setValeurChamp(TYPE, PROPERTY, VALUE)" method
view.ajouterType(Unite.class, "EXEMPLE")
view.ajouterProp(Unite.class, "EXEMPLE", "PEXEMPLE")

// display annotations per Element type (Unite, Relation, Schema) and per type

// Create annotation type

structure.ajouterType(Unite.class, "EXEMPLE");
//structure.ajouterType(Relation.class, "EXEMPLE");
//structure.ajouterType(Schema.class, "EXEMPLE");
println structure.getTypes(Unite.class)

// Create annotation property

structure.ajouterProp(Unite.class, "EXEMPLE", "PEXEMPLE")
println structure.getNomsProps(Unite.class, "EXEMPLE")

// Create annotation value

structure.ajouterVal(Unite.class, "EXEMPLE", "PEXEMPLE", "oui")
structure.ajouterVal(Unite.class, "EXEMPLE", "PEXEMPLE", "non")

// Create annotations

// Unite
def u = analecCorpus.addUniteSaisie("EXEMPLE", 0, 10, ["PEXEMPLE":"oui"])
def u2 = analecCorpus.addUniteSaisie("EXEMPLE", 12, 12, ["PEXEMPLE":"oui"])
println u

// Edit annotations

u.getProps()["PEXEMPLE"] = "non"

// Unit getters

println u.getDeb() // start of unit
println u.getFin() // end of unit

println u.getProp("EXEMPLE")
println u.getType()

// Relation
Relation relation = new Relation("REXEMPLE", u, u2)
relation.getProps().put("PEXEMPLE", "oui")
analecCorpus.addRelationLue(relation)  // add the new relation

// Schema
Schema schema = new Schema()
schema.type = "SEXEMPLE"
schema.props.put("PEXEMPLE", "oui")
schema.ajouter(u) // insert one unit

analecCorpus.addSchemaLu(schema)  // add the new schema

// Browse Units

println "Units:"
for (String type : structure.getUnites()) {
	def units = analecCorpus.getUnites(type)
	if (units.size() > 0) {
		println "	${units.size()} $type"
	}
}

// Browse Relations

println "Relations:"
for (String type : structure.getRelations()) {
	def relations = analecCorpus.getRelations(type)
	if (relations.size() > 0) {
		println "	${relations.size()} $type"
	}
}

// Browse Schemas

println "Schemas:"
for (String type : structure.getSchemas()) {
	def schemas = analecCorpus.getSchemas(type)
	if (schemas.size() > 0) {
		println "	${schemas.size()} $type"
	}
}

// URS selections

// select Schemas
def debug = 0 // 1 2 for more logs
def strict_inclusion = true
def position = 0
def minimum_schema_size = 1;
def maximum_schema_size = 10;
def schema_ursql = "SEXEMPLE"
def unit_ursql = "EXEMPLE@PEXEMPLE=oui"
def unit_type = "EXEMPLE" 
println AnalecUtils.selectSchemasInCorpus(debug, analecCorpus, corpus, schema_ursql, minimum_schema_size, maximum_schema_size, strict_inclusion)


// with URSQL
println AnalecUtils.filterElements(debug, analecCorpus.getUnites(unit_type), unit_ursql)

// with intersection with CQP corpus matches
println AnalecUtils.filterUniteByInclusion(debug, analecCorpus.getUnites(unit_type), corpus.getMatches(), strict_inclusion, position)

// by size
println AnalecUtils.filterBySize(analecCorpus.getSchemas(schema_ursql), minimum_schema_size, maximum_schema_size)

// Delete annotations

analecCorpus.supUnite(u)
analecCorpus.supUnite(u2)
analecCorpus.supRelation(relation)
analecCorpus.supSchema(schema)

// Delete annotation value

structure.supprimerVal(Unite.class, "EXEMPLE", "PEXEMPLE", "oui")
structure.supprimerVal(Unite.class, "EXEMPLE", "PEXEMPLE", "non")

// Delete annotation property

structure.supprimerProp(Unite.class, "EXEMPLE", "PEXEMPLE")
structure.supprimerProp(Relation.class, "REXEMPLE", "PEXEMPLE")
structure.supprimerProp(Schema.class, "SEXEMPLE", "PEXEMPLE")

// Delete annotation type

structure.supprimerType(Unite.class, "EXEMPLE");
structure.supprimerType(Relation.class, "REXEMPLE");
structure.supprimerType(Schema.class, "SEXEMPLE");

// Revert changes

//URSCorpora.revert(corpus);

// Save changes

//URSCorpora.saveCorpus(corpus)

