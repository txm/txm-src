// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden
// STANDARD DECLARATIONS
package org.txm.macro.urs.edit

import org.apache.commons.lang.StringUtils
import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.Toolbox
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.macro.urs.AnalecUtils
import org.txm.searchengine.cqp.AbstractCqiClient
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.CQLQuery

import visuAnalec.donnees.Corpus
import visuAnalec.donnees.Structure
import visuAnalec.elements.*
import visuAnalec.vue.Vue

def scriptName = this.class.getSimpleName()

def selection = []
for (def s : corpusViewSelections) {
	if (s instanceof CQPCorpus) selection << s
	else if (s instanceof Partition) selection.addAll(s.getParts())
}

if (selection.size() == 0) {
	println "** $scriptName: please select a Corpus or a Partition in the Corpus view: "+corpusViewSelections
	return false
} else {
	for (def c : selection) c.compute(false)
}

// BEGINNING OF PARAMETERS
@Field @Option(name="schema_ursql", usage="TYPE@PROP=VALUE", widget="String", required=true, def="Chain")
		String schema_ursql
@Field @Option(name="minimum_schema_size", usage="Minimum size needed to consider a schema", widget="Integer", required=true, def="3")
		int minimum_schema_size
@Field @Option(name="maximum_schema_size", usage="Maximum size needed to consider a schema", widget="Integer", required=true, def="9999999")
		int maximum_schema_size
@Field @Option(name="unit_ursql", usage="TYPE@PROP=VALUE", widget="String", required=false, def="Entity")
		String unit_ursql
@Field @Option(name="position_in_schema", usage="Unit distance in schema (0 = no selection, 1 = first after limit, -1 = last before limit, etc.)", widget="Integer", required=true, def="0")
		int position_in_schema
@Field @Option(name="cql_limit", usage="CQL to build structure limits", widget="Query", required=false, def="<div> [] expand to div")
		cql_limit
@Field @Option(name="strict_inclusion", usage="Units must be strictly included into corpus matches", widget="Boolean", required=true, def="true")
		boolean strict_inclusion
@Field @Option(name="position_in_matches", usage="Unit distance to structure limit (0 = no selection, 1 = first after limit, -1 = last before limit, etc.)", widget="Integer", required=true, def="0")
		int position_in_matches
@Field @Option(name="property_name", usage="PROP", widget="String", required=false, def="Property")
		String property_name
@Field @Option(name="property_value", usage="VALUE", widget="String", required=false, def="Value")
		String property_value
@Field @Option(name="debug", usage="Show internal variable content", widget="StringArray", metaVar="OFF	ON	ALL	REALLY ALL", required=true, def="OFF")
		debug
if (!ParametersDialog.open(this)) return
	if (debug == "OFF") debug = 0; else if (debug == "ON") debug = 1; else if (debug == "ALL") debug = 2 else if (debug == "REALLY ALL") debug = 3

if (property_name.length() == 0) {
	println "property_name not set: aborting."
}

//corpus = corpusViewSelection
def allResults = [:]
def errors = new HashSet()
for (def corpus : selection) {
	println "Annotating $corpus..."

	def word = corpus.getWordProperty()
	def analecCorpus = URSCorpora.getCorpus(corpus)
	Vue analecView = URSCorpora.getVue(corpus)
	def typesAVoir = analecView.getTypesAVoir(Unite.class)
	
	Structure structure = analecCorpus.getStructure()

	AnalecUtils.defineProperty(Unite.class, analecCorpus, unit_ursql, property_name)

	def selectedUnits = AnalecUtils.selectUnitsInSchema(debug, analecCorpus, corpus, schema_ursql, minimum_schema_size, maximum_schema_size,
			unit_ursql, position_in_schema, cql_limit, strict_inclusion, position_in_matches);
		
	println " "+selectedUnits.size()+" units to annotate..."
	def n = 0
	def nerrors = 0
	for (Unite unit : selectedUnits) {
		String unit_type = unit.getType()
		// ensure the unit_type & property_name are declared in the Vue
		if (!typesAVoir.contains(unit_type)) {
			analecView.ajouterType(Unite.class, unit_type)
		}
		if (!analecView.getNomsChamps(Unite.class, unit_type).contains(property_name)) {
			analecView.ajouterProp(Unite.class, unit_type, property_name)
		}
		if (!analecView.setValeurChamp(unit, property_name, property_value)) {
			errors << ""+unit.getDeb()+"->"+unit.getFin()
			nerrors++
		} else {
			n++
		}
	}
	
	corpus.getMainCorpus().setIsModified(true)
	allResults[corpus] = selectedUnits
	println " $n valeurs affectées à $corpus"
	println " $nerrors erreurs lors de l'annotation de $corpus"
}
if (errors.size() > 0) {
	println " Errors while annotating $property_name=$property_value"
	println errors.join("\n")
}
return allResults