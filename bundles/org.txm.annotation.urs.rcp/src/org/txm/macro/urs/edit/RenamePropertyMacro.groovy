// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden
// STANDARD DECLARATIONS
package org.txm.macro.urs.edit

import org.apache.commons.lang.StringUtils
import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.Toolbox
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.macro.urs.AnalecUtils
import org.txm.searchengine.cqp.AbstractCqiClient
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.CQLQuery

import visuAnalec.donnees.Corpus
import visuAnalec.donnees.Structure
import visuAnalec.elements.*
import visuAnalec.vue.Vue

def scriptName = this.class.getSimpleName()

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "** $scriptName: please select a Corpus in the Corpus view: "+corpusViewSelection
	return false
}

// BEGINNING OF PARAMETERS
@Field @Option(name="element_type", usage="Unit|Relation|Schema", widget="StringArray", metaVar="Unit	Relation	Schema", required=true, def="Unit")
	String element_type
@Field @Option(name="element_name", usage="Unit type", widget="String", required=true, def="Entity")
		String element_name
@Field @Option(name="property_oldname", usage="the property to be renamed", widget="String", required=true, def="Property")
		String property_oldname
@Field @Option(name="property_newname", usage="the new name of the property type", widget="String", required=true, def="Property2")
		String property_newname

if (!ParametersDialog.open(this)) return
		
def corpus = corpusViewSelection
	println "Renaming '$element_type/$element_name@property_oldname' property to '$element_type/$element_name@property_newname'..."

	def word = corpus.getWordProperty()
	def analecCorpus = URSCorpora.getCorpus(corpus)
	Vue analecView = URSCorpora.getVue(corpus)
	Structure structure = analecCorpus.getStructure()

	Class clazz = Unite.class
	if (element_type == "Relation") clazz = Relation.class
	else if (element_type == "Schema") clazz = Schema.class
	
	HashSet unit_properties = structure.getNomsProps(clazz, element_name)
	if (!unit_properties.contains(property_oldname)) {
		println "No property '$element_type/$element_name@property_oldname' in $element_name structure"
	} else {
		structure.renommerProp(clazz, element_name, property_oldname, property_newname);
	}
	
	if (!Arrays.asList(analecView.getNomsChamps(clazz, element_name)).contains(property_oldname)) {
		println "No property '$element_type/$element_name@property_oldname' in $element_name view"
	} else {
		analecView.renommerProp(clazz, element_name, property_oldname, property_newname);
	}
	
	int n = 0;
	for (Element e : analecCorpus.getElements(clazz, element_name)) {
		if (e.getProps().containsKey(property_oldname)) {
			n++
			e.getProps().put(property_newname, e.getProps().get(property_oldname));
			e.getProps().remove(property_oldname)
		}
	}
	
	analecCorpus.modifie = true
	
	println "$n units updated."
	println "Don't forget to rename the property in the view and to check&save annotations."
	
