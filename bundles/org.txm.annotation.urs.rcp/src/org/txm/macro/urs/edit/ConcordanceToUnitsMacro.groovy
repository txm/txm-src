// STANDARD DECLARATIONS
package org.txm.macro.urs.edit

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.rcp.editors.concordances.*
import org.txm.searchengine.cqp.corpus.CQPCorpus
import org.txm.searchengine.cqp.corpus.MainCorpus
import org.txm.functions.concordances.*
import org.txm.annotation.urs.*
import org.txm.concordance.core.functions.Concordance
import org.txm.concordance.rcp.editors.ConcordanceEditor
import visuAnalec.elements.Unite
import visuAnalec.vue.Vue

// BEGINNING OF PARAMETERS

@Field @Option(name="unit_type", usage="The unit type to create", widget="String", required=true, def="Entity")
def unit_type

@Field @Option(name="move_start", usage="move the start position", widget="Integer", required=true, def="0")
def move_start

@Field @Option(name="move_end", usage="move the end position", widget="Integer", required=true, def="0")
def move_end

@Field @Option(name="create_only_if_new", usage="Create the unit if not already annotated", widget="Boolean", required=true, def="true")
def create_only_if_new

@Field @Option(name="property_name", usage="prop", widget="String", required=true, def="Property")
def property_name

@Field @Option(name="property_value", usage="default value", widget="String", required=true, def="VALUE")
def property_value

// END OF PARAMETERS

// get a Concordance from 1) current Concordance editor or 2) CorporaView selection
Concordance concordance
if (editor instanceof ConcordanceEditor) {
	concordance = editor.getConcordance()
} else if (corpusViewSelection instanceof Concordance) {
	concordance = corpusViewSelection
} else {
	println "You must select a concordance or open a concordance result to run this macro."
	return false
}

if (concordance == null) {
	println "You must compute a concordance before."
	return
}

// check the analec corpus is ready
CQPCorpus corpus = concordance.getCorpus().getMainCorpus();
String name = corpus.getID()
if (!URSCorpora.isAnnotationStructureReady(corpus)) {
	println "Annotation structure is not ready."
	return
}

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) {
	println "User canceled parameters"
	return;
}

// check the corpus structure has the unit_type provided
def analecCorpus = URSCorpora.getCorpus(corpus)
if (!analecCorpus.getStructure().getUnites().contains(unit_type)) {
	//println "The corpus structure does not contains unit with type=$unit_type"
	//return;
	analecCorpus.getStructure().ajouterType(Unite.class, unit_type);
}

if (!analecCorpus.getStructure().getNomsProps(Unite.class, unit_type).contains(property_name)) {
	//println "The corpus structure does not contains unit with type=$unit_type"
	//return;
	analecCorpus.getStructure().ajouterProp(Unite.class, unit_type, property_name)
}

if (!analecCorpus.getStructure().getValeursProp(Unite.class, unit_type, property_name).contains(property_value)) {
	//println "The corpus structure does not contains unit with type=$unit_type"
	//return;
	analecCorpus.getStructure().ajouterVal(Unite.class, unit_type, property_name, property_value)
}

Vue vue = URSCorpora.getVue(corpus)
if (!vue.getTypesAVoir(Unite.class).contains(unit_type)) {
	vue.ajouterType(Unite.class, unit_type)
}
if (!vue.getNomsChamps(Unite.class, unit_type).contains(property_name)) {
	vue.ajouterProp(Unite.class, unit_type, property_name)
}

// browse lines and check
def units = analecCorpus.getUnites(unit_type)
def lines = concordance.getLines()

println "Annotating ${lines.size()} lines."
int n = 0
for (int iLine = 0 ; iLine < lines.size() ; iLine++) {
	int iUnit = 0
	def line = lines[iLine]
	def m = line.getMatch()
	def start = m.getStart()+move_start
	def end = m.getEnd()+move_end
	
	def do_create = true
	if (create_only_if_new && iUnit < units.size()) { // test only if create_only_if_new == true
		def unit = null
		//TODO don't iterates over all units
		while (iUnit < units.size() ) { //&& units[iUnit].getDeb() < start) {
			if (iUnit < units.size()) {
				unit = units[iUnit++]
				if (unit.getDeb() == start && unit.getFin() == end) { // skip and print the line
					println("skiping concordance line '"+line.keywordToString()+"' at "+line.getViewRef().toString()+" ("+unit.getDeb()+ ", "+unit.getFin()+")")
					do_create = false
					continue
				}
			}
		}
	}
	if (do_create) {
		n++
		def props = [:]
		props[property_name] = property_value
		Unite u = analecCorpus.addUniteSaisie(unit_type, start, end, props)
		//	println "$props -> "+u.getProps()
	}
}
if (property_name != null) {
	println "$n $unit_type created with $property_name='$property_value'."
} else {
	println "$n $unit_type created."
}

if (n > 0) corpus.setIsModified(true);