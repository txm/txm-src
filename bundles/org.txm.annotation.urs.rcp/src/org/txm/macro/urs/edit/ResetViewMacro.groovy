// STANDARD DECLARATIONS
package org.txm.macro.urs.edit

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.elements.*

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "Corpora selection is not a Corpus"
	return;
}

def corpus = corpusViewSelection
if (!URSCorpora.isAnnotationStructureReady(corpus)) {
	println "** URS Annotation Structure of "+corpusViewSelection+" is not ready. Aborting."
	return
}

@Field @Option(name="CONFIRMATION", usage="select to confirm the annotations reset", widget="Boolean", required=false, def="false")
def CONFIRMATION

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

if (!CONFIRMATION) {
	println "Aborting: CONFIRMATION was no checked."
	return;
}

def analecCorpus = URSCorpora.getCorpus(corpus);
//def structure = analecCorpus.getStructure()
def vue = URSCorpora.getVue(corpus)

/*
Class[] classes = [Unite.class, Relation.class, Schema.class]
println "Removing unites..."
for (String type : vue.getTypes(Unite.class)) {
	for (Unite unite : analecCorpus.getUnites(type).toArray(new Unite[0])) {
		analecCorpus.supUnite(unite)
	}
}

println "Removing relations..."
for (String type : vue.getTypes(Relation.class)) {
	for (Relation relation : analecCorpus.getRelations(type).toArray(new Relation[0])) {
		analecCorpus.supRelation(relation)
	}
}

println "Removing schemas..."
for (String type : vue.getTypes(Schema.class)) {
	for (Schema schema : analecCorpus.getSchemas(type).toArray(new Schema[0])) {
		analecCorpus.supSchema(schema)
	}
}
*/

vue.retablirVueParDefaut();

println "Done. Save the corpus to finish the annotations reset."

