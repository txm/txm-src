// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// STANDARD DECLARATIONS
package org.txm.macro.urs.edit

import org.kohsuke.args4j.*
import org.txm.annotation.urs.*
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*

import groovy.transform.Field
import visuAnalec.donnees.Structure;
import visuAnalec.elements.Schema
import visuAnalec.elements.Unite;
import visuAnalec.vue.Vue

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Corpora selection is not a MainCorpus: "+corpusViewSelection
	return;
}

// BEGINNING OF PARAMETERS
@Field @Option(name="schema_name",usage="", widget="String", required=true, def="Chain")
String schema_name
@Field @Option(name="unit_type",usage="", widget="String", required=true, def="Entity")
String unit_type
@Field @Option(name="ref_property",usage="", widget="String", required=true, def="JoinProperty")
String ref_property
if (!ParametersDialog.open(this)) return;

int nCreated = 0 // count the number of created CHAINE
int nUpdated = 0 // count the number of updated CHAINE

MainCorpus corpus = corpusViewSelection
def analecCorpus = URSCorpora.getCorpus(corpus); 
Structure structure = analecCorpus.getStructure()
if (!structure.getUnites().contains(unit_type)) { // check if the structure contains the unit_type units
	println "Error: corpus structure does not contains unit with name=$unit_type"
	return
}
if (!structure.getSchemas().contains(schema_name)) { // update the structure if needed
	println "Creating the '$schema_name' schema in the structure"
	analecCorpus.ajouterType(Schema.class, schema_name)
	analecCorpus.ajouterProp(Schema.class, schema_name, ref_property)
}

Vue vue = URSCorpora.getVue(corpus)
if (!vue.getTypesAVoir(Schema.class).contains(schema_name)) {
	vue.ajouterType(Schema.class, schema_name)
}
if (!vue.getNomsChamps(Schema.class, schema_name).contains(ref_property)) {
	vue.ajouterProp(Schema.class, schema_name, ref_property)
}

def props = structure.getUniteProperties(unit_type)
if (!props.contains(ref_property)) { // check the unit_type units have the REF property
	println "Error: $unit_type units have no proprerty named '$ref_property'"
	return
}

// parse the units to build CHAINES
def chaines = [:]
def units = analecCorpus.getUnites(unit_type)
units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() }
for (Unite unit : units) {
	def ref = unit.getProp(ref_property)
	if (!chaines.containsKey(ref)) {
		chaines[ref] = []
	}
	chaines[ref] << unit
}

// update the already existing CHAINES schemas
for (Schema schema : analecCorpus.getSchemas(schema_name)) {
	String ref = schema.getProp(ref_property)
	if (chaines.containsKey(ref)) { // the CHAINE exists 
		// maj des unités de la chaine existante
		int size_before = schema.getContenu().size()
		for (def unit : chaines[ref]) schema.ajouter(unit) // insert the new units in the hashset
		
		// remove the inserted CHAINE from 'chaines'
		chaines.remove(ref)
		if (size_before < schema.getContenu().size()) // if the size changed, then the CHAIEN have been updated
			nUpdated++
	}
}

// create the remaining CHAINES schemas
for (def ref : chaines.keySet()) { // process the remaining CHAINE of 'chaines'
	nCreated++;
	Schema schema = new Schema()
	schema.type = schema_name
	schema.props.put(ref_property, ref) 
	
	for (def unit : chaines[ref]) schema.ajouter(unit) // insert the new units in the hashset
	
	analecCorpus.addSchemaLu(schema)  // add the new schema
}

if (nUpdated > 0 || nCreated > 0) corpus.setIsModified(true);
println "nUpdated=$nUpdated"
println "nCreated=$nCreated"
