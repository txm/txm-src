package org.txm.macro.urs.edit

// Copyright © 2021 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden


import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.*
import org.txm.rcpapplication.views.*
import org.txm.macro.cqp.*
import org.txm.annotation.urs.URSCorpora
import org.txm.macro.urs.AnalecUtils
import visuAnalec.elements.Unite


// BEGINNING OF PARAMETERS

if (!(corpusViewSelection instanceof MainCorpus)) {
	scriptName = this.class.getSimpleName()
	println "** $scriptName: please select a corpus in the Corpus view or provide a corpus name (current selection is $corpusViewSelection). Aborting."
	return false
}
// Declare each parameter here
// (available widget types: Query, File, Folder, String, Text, Boolean, Integer, Float and Date)

@Field @Option(name="unit_type", usage="Unit Type", widget="String", required=false, def="Entity")
def unit_type

@Field @Option(name="unit_ursql", usage="TYPE or TYPE@PROP or TYPE@PROP=<regex>", widget="String", required=false, def="Entity")
def unit_ursql

@Field @Option(name="propertyToClone", usage="name of the property to clone", widget="String", required=true, def="Property")
def propertyToClone

@Field @Option(name="newProperty", usage="name of the new property", widget="String", required=true, def="NewProperty")
def newProperty

@Field @Option(name="debug", usage="Show debug information", widget="StringArray", metaVar="OFF	ON	ALL	REALLY ALL", required=false, def="OFF")
def debug

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return false

if (debug == "OFF") debug = 0; else if (debug == "ON") debug = 1; else if (debug == "ALL") debug = 2 else if (debug == "REALLY ALL") debug = 3

// check availability of the corpus

CQI = CQPSearchEngine.getCqiClient()

scriptName = this.class.getSimpleName()

corpus = corpusViewSelection
corpusName = corpus.getName()

// check availability of every information needed in the corpus

if (!URSCorpora.isAnnotationStructureReady(corpus)) {
	println "** URS Annotation Structure of "+corpusName+" is not ready. Aborting."
	return false
}

analecCorpus = URSCorpora.getCorpus(corpus)

if (analecCorpus == null) {
	println "** no URS Annotations for "+corpusName+" corpus. Aborting."
	return false
}

// select Units

selectedUnits = AnalecUtils.selectUnitsInSchema(debug, analecCorpus, corpus, null, null, null, unit_ursql, 0, null, false, 0)

nUnits = selectedUnits.size()

if (nUnits == 0) {
	println "No units selected, no property cloned."
	return
}

// check availability of every information needed in the corpus

corpus_units = analecCorpus.getUnites(unit_type)

if (corpus_units == null) {
	println "** no URS Annotations '"+unit_type+"' corpus unit type in corpus "+corpusName+". Aborting."
	return false
}

struct = analecCorpus.getStructure()

if (struct == null) {
	println "** no URS Annotations structure in corpus "+corpusName+". Aborting."
	return false
}

struct_units = struct.getUnites()

if (struct_units == null) {
	println "** no URS Annotations structure units in corpus "+corpusName+". Aborting."
	return false
}

if (debug > 0) {

println "unit types: "+struct_units
println unit_type+" properties: "+struct.getNomsProps(Unite.class, unit_type)

}

// check Unit type
props = struct.getNomsProps(Unite.class, unit_type)

// if necessary, add new property to unit type

if ((props == null) || (!props.contains(newProperty))) {
	struct.ajouterProp(Unite.class, unit_type, newProperty)
}

// verify that the property is added to unit_type unit type
props = struct.getNomsProps(Unite.class, unit_type)

if (props == null) {
	println "** impossible to add new property '"+newProperty+"' to '"+unit_type+"' unit type in corpus "+corpusName+". Aborting."
	return false
}
	
if (!props.contains(newProperty)) {
	println "** impossible to add new property '"+newProperty+"' to '"+unit_type+"' unit type in corpus "+corpusName+". Aborting."
	return false
}

// check Vue
vue = URSCorpora.getVue(corpus)

if (vue == null) {
	println "** no URS 'vue' for corpus "+corpusName+". Aborting."
	return false
}

vue_units = vue.getTypesUnitesAVoir()

if (vue_units == null) {
	println "** no URS Annotations vue units in corpus "+corpusName+". Aborting."
	return false
}

props = vue.getNomsChamps(Unite.class, unit_type)

if (debug > 0) {
	println "vue unit types: "+vue_units
	println unit_type+" vue fields: "+props
}

// if necessary, add the new property to unit_type unit type in vue

if ((props == null) || (!props.contains(newProperty))) {
	vue.ajouterProp(Unite.class, unit_type, newProperty)
}

// verify that the property is added to unit_type unit type in vue
props = vue.getNomsChamps(Unite.class, unit_type)

if (props == null) {
	println "** impossible to add properties to '"+unit_type+"' unit type in vue of corpus "+corpusName+". Aborting."
	return false
}

if (!props.contains(newProperty)) {
		println "** impossible to add property '"+newProperty+"' to '"+unit_type+"' unit type in vue of corpus "+corpusName+". Aborting."
		return false
}

selectedUnits.each { unit ->
	def val = unit.getProp(propertyToClone)
   	vue.setValeurChamp(unit, newProperty, val)
}

println nUnits+" properties cloned (but not saved)."

corpus.setIsModified(true)

return true

