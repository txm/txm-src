// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden
// STANDARD DECLARATIONS
package org.txm.macro.urs.edit

import org.apache.commons.lang.StringUtils
import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.Toolbox
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.macro.urs.AnalecUtils
import org.txm.searchengine.cqp.AbstractCqiClient
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.CQLQuery

import visuAnalec.donnees.Corpus
import visuAnalec.donnees.Structure
import visuAnalec.elements.*
import visuAnalec.vue.Vue

def scriptName = this.class.getSimpleName()

def selection = []
for (def s : corpusViewSelections) {
	if (s instanceof CQPCorpus) selection << s
	else if (s instanceof Partition) selection.addAll(s.getParts())
}

if (selection.size() == 0) {
	println "** $scriptName: please select a Corpus or a Partition in the Corpus view: "+corpusViewSelections
	return false
} else {
	for (def c : selection) c.compute(false)
}

// BEGINNING OF PARAMETERS
@Field @Option(name="schema_ursql", usage="TYPE@PROP=VALUE", widget="String", required=false, def="Chain")
		String schema_ursql
@Field @Option(name="minimum_schema_size", usage="Minimum size needed to consider a schema", widget="Integer", required=false, def="3")
		int minimum_schema_size
@Field @Option(name="maximum_schema_size", usage="Maximum size needed to consider a schema", widget="Integer", required=false, def="9999999")
		int maximum_schema_size
@Field @Option(name="unit_ursql", usage="TYPE@PROP=VALUE", widget="String", required=false, def="Entity")
		String unit_ursql
@Field @Option(name="position_in_schema", usage="Unit distance in schema (0 = no selection, 1 = first after limit, -1 = last before limit, etc.)", widget="Integer", required=false, def="0")
		int position_in_schema
@Field @Option(name="cql_limit", usage="CQL to build structure limits", widget="Query", required=false, def="<div> [] expand to div")
		cql_limit
@Field @Option(name="strict_inclusion", usage="Units must be strictly included into corpus matches", widget="Boolean", required=false, def="true")
		boolean strict_inclusion
@Field @Option(name="position_in_matches", usage="Unit distance to structure limit (0 = no selection, 1 = first after limit, -1 = last before limit, etc.)", widget="Integer", required=false, def="0")
		int position_in_matches

@Field @Option(name="debug", usage="Show internal variable content", widget="StringArray", metaVar="OFF	ON	ALL	REALLY ALL", required=true, def="OFF")
		debug
if (!ParametersDialog.open(this)) return
	if (debug == "OFF") debug = 0; else if (debug == "ON") debug = 1; else if (debug == "ALL") debug = 2 else if (debug == "REALLY ALL") debug = 3

//corpus = corpusViewSelection
def allResults = [:]
def errors = new HashSet()
for (def corpus : selection) {
	println "Deleting '$unit_ursql' units of '$schema_ursql' schemas in the '$corpus' corpus..."

	def word = corpus.getWordProperty()
	def analecCorpus = URSCorpora.getCorpus(corpus)
	Vue analecView = URSCorpora.getVue(corpus)
	Structure structure = analecCorpus.getStructure()

	def selectedUnits = AnalecUtils.selectUnitsInSchema(debug, analecCorpus, corpus, schema_ursql, minimum_schema_size, maximum_schema_size,
			unit_ursql, position_in_schema, cql_limit, strict_inclusion, position_in_matches);
		
	def n = 0
	def nerrors = 0
	for (Unite unit : selectedUnits) {
		analecCorpus.supUnite(unit);
		n++
	}
	
	corpus.getMainCorpus().setIsModified(true)
	allResults[corpus] = selectedUnits
	allResults["n"] = n
	println " $n units deleted"
}

return allResults