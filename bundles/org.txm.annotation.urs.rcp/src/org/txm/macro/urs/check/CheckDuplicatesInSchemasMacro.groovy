package org.txm.macro.urs.check

// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden
// STANDARD DECLARATIONS

import groovy.transform.Field

import org.jfree.chart.JFreeChart
import org.kohsuke.args4j.*
import org.txm.Toolbox
import org.txm.annotation.urs.*
import org.txm.macro.urs.AnalecUtils
import org.txm.rcp.Application
import org.txm.rcp.IImageKeys
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.CQPSearchEngine
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.CQLQuery

import visuAnalec.elements.*

def scriptName = this.class.getSimpleName()

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "** $scriptName please select a Corpus to run the macro"
	return;
}

// BEGINNING OF PARAMETERS
@Field @Option(name="schema_ursql", usage="TYPE@PROP=VALUE", widget="String", required=true, def="Chain")
		String schema_ursql
@Field @Option(name="schema_property_display", usage="PROP", widget="String", required=true, def="Property")
		String schema_property_display
@Field @Option(name="debug", usage="Show internal variable content", widget="StringArray", metaVar="OFF	ON	ALL	REALLY ALL", required=true, def="OFF")
		debug
if (!ParametersDialog.open(this)) return
	if (debug == "OFF") debug = 0; else if (debug == "ON") debug = 1; else if (debug == "ALL") debug = 2 else if (debug == "REALLY ALL") debug = 3

def CQI = CQPSearchEngine.getCqiClient()

def corpus = corpusViewSelection

def word = corpus.getWordProperty()
def analecCorpus = URSCorpora.getCorpus(corpus)



def errors = AnalecUtils.isPropertyDefined(Schema.class, analecCorpus, schema_ursql)
if (errors.size() > 0) {
	println "** The $schema_ursql schema URSQL cannot be computed in the corpus with types: $errors."
	return;
}

if (schema_property_display.length() > 0) {
	errors = AnalecUtils.isPropertyDefined(Unite.class, analecCorpus, AnalecUtils.getFilterParameters(schema_ursql)[0], schema_property_display).size()
	if (errors > 0) {
		println "Error: some Schema types don't contain the $schema_property_display property: $errors"
		return
	}
}

def allUnits = [:]

def allSchemas = AnalecUtils.selectSchemasInCorpus(debug, analecCorpus, corpus , schema_ursql, -1, Integer.MAX_VALUE, false)

if (allSchemas.size() == 0) {
	println "No schema match for '$schema_ursql' selection. Aborting"
	return
}

allUnits = AnalecUtils.groupAllUnitesInElements(debug, allSchemas)

if (allUnits.size() == 0) {
	println "No unit selection. Aborting"
	return
}

if (debug) println "allUnits=${allUnits.size()}"

def duplicates = [:]
for (Schema schema : allSchemas) {
	def units = allUnits[schema];
	for (def unit : units) {
		if (!duplicates.containsKey(unit)) duplicates[unit] = []
		duplicates[unit] << schema
	}
}

def units = []
units.addAll(duplicates.keySet()) // remove non duplicates from hash
for (def unit : units) {
	if (duplicates[unit].size() < 2) duplicates.remove(unit)
}

if (duplicates.size() > 0) {
	println "${duplicates.size()} duplicates found"
	for (def unit : duplicates.keySet()) {
		println AnalecUtils.toString(CQI, word, unit)+" in: "
		for (Schema schema : duplicates[unit]) {
			println " '"+schema.getProp(schema_property_display)+"'\t"+schema.getProps()
		}
	}
} else {
	println "No duplicates found in $schema_ursql schema units"
}

return duplicates