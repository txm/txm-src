// STANDARD DECLARATIONS
package org.txm.macro.urs.check

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.annotation.urs.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.elements.*

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "Corpora selection is not a Corpus"
	return;
}

@Field @Option(name="unit_type",usage="", widget="String", required=false, def="Entity")
String unit_type

@Field @Option(name="unit_property", usage="", widget="String", required=false, def="Property")
String unit_property

@Field @Option(name="pruneUnusedValues", usage="", widget="Boolean", required=false, def="false")
boolean pruneUnusedValues

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

def corpus = corpusViewSelection
def analecCorpus = URSCorpora.getCorpus(corpus);
def structure = analecCorpus.getStructure()

def unit_types = structure.getTypes(Unite.class);
if (!unit_types.contains(unit_type)) {
	println "Missing unit type: $unit_type"
	return
}

def props = analecCorpus.getStructure().getUniteProperties(unit_type);
if (!props.contains(unit_property)) {
	println "No properties '$unit_property' in '$unit_type' unit."
	return
}

def prop  = unit_property
	
def tmpvalues = new HashSet()
tmpvalues.addAll(structure.getValeursProp(Unite.class, unit_type, prop));
println "Values stored in the structure: $tmpvalues"
	
def used_values = new HashSet();
def unites = analecCorpus.getUnites(unit_type);
for (Unite unite : unites) {
	used_values.add(unite.getProp(prop))
}
	
tmpvalues.removeAll(used_values);
if (tmpvalues.size() > 0) {
	println "The following values ("+tmpvalues.size()+") are not used: "+tmpvalues.join(", ")
	if (pruneUnusedValues) {
		println "Pruning the values..."
		for (String val : tmpvalues) {
			structure.supprimerVal(Unite.class, unit_type, unit_property, val);
		}
	} 
} else {
	println "All the values are used."
}