package org.txm.annotation.urs.testers;

import java.util.List;

import org.eclipse.core.expressions.PropertyTester;
import org.txm.annotation.urs.URSCorpora;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;

public class IsCorpusAnnotated extends PropertyTester {

	public static final String PROPERTY_NAMESPACE = "org.txm.annotation.urs.testers"; //$NON-NLS-1$

	public static final String PROPERTY_CAN_SAVE = "canAnnotate"; //$NON-NLS-1$

	@Override
	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {

		if (receiver instanceof List) {
			@SuppressWarnings("unchecked")
			List<Object> list = (List<Object>) receiver;
			if (list.size() == 0) return false;
			receiver = list.get(0);
		}

		//System.out.println("test org.txm.annotation.urs.testers.canAnnotate with "+receiver.getClass());
		if (receiver instanceof CQPCorpus) {
			return testCorpus((CQPCorpus) receiver);
		}
		return false;
	}

	protected boolean testCorpus(CQPCorpus c) {
		MainCorpus mc = c.getMainCorpus();
		boolean ret = URSCorpora.isAnnotationStructureReady(mc);
		//System.out.println(" Corpus:"+c+" org.txm.annotation.urs.testers.canAnnotate: "+ret);
		return ret;
	}
}
