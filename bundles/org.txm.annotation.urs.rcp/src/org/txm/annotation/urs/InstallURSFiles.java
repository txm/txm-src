package org.txm.annotation.urs;

import java.io.File;

import org.txm.PostTXMHOMEInstallationStep;
import org.txm.Toolbox;
import org.txm.groovy.core.InstallGroovyFiles;
import org.txm.objects.Workspace;
import org.txm.utils.BundleUtils;
import org.txm.utils.DeleteDir;

/**
 * 
 * Copy all Groovy scripts in TXMHome directory
 * 
 * @author mdecorde
 *
 */
public class InstallURSFiles extends PostTXMHOMEInstallationStep {

	final static String createfolders[] = { "scripts/groovy/user" }; //$NON-NLS-1$

	@Override
	public boolean do_install(Workspace workspace) {

		//		String saved = URSPreferences.getInstance().getString(URSPreferences.VERSION); //$NON-NLS-1$
		//		Version currentVersion = BundleUtils.getBundleVersion("org.txm.annotation.urs.rcp"); //$NON-NLS-1$
		//		Log.finer("Testing saved version with current version="+currentVersion+" and saved="+saved);
		//		if (saved != null && saved.length() > 0) {
		//			Version savedVersion = new Version(saved);
		//			if (currentVersion.compareTo(savedVersion) <= 0) {
		//				Log.finer(Messages.DoInstallStep_5);
		//				//System.out.println("No post-installation of URS to do");
		//				return true; // nothing to do
		//			}
		//		}

		File txmhomedir = new File(Toolbox.getTxmHomePath());

		for (String folder : createfolders) {
			new File(txmhomedir, folder).mkdirs();
		}
		File scriptsDirectory = new File(txmhomedir, "scripts/groovy"); //$NON-NLS-1$
		File userDirectory = new File(scriptsDirectory, "user"); //$NON-NLS-1$
		File systemDirectory = new File(scriptsDirectory, "system"); //$NON-NLS-1$
		File macroDirectory = new File(userDirectory, "org/txm/macro"); //$NON-NLS-1$
		File macroDirectory2 = new File(systemDirectory, "org/txm/macro"); //$NON-NLS-1$
		File URSMacroDirectory = new File(macroDirectory, "urs"); //$NON-NLS-1$
		File URSMacroDirectory2 = new File(systemDirectory, "org/txm/macro/urs"); //$NON-NLS-1$

		String bundle_id = "org.txm.annotation.urs.rcp"; //$NON-NLS-1$

		if (URSMacroDirectory.exists()) {
			//			long time = URSMacroDirectory.lastModified();
			//			File backDirectory = new File(macroDirectory, "urs-"+Toolbox.dateformat.format(new Date(time)));
			//			Log.info(NLS.bind("Making a copy of the {0} previous URS macros directory to {1}.", URSMacroDirectory, backDirectory));
			//			
			//			URSMacroDirectory.renameTo(backDirectory);

			InstallGroovyFiles.backupFiles(URSMacroDirectory, URSMacroDirectory2, systemDirectory.getParentFile());

			DeleteDir.deleteDirectory(URSMacroDirectory);
		}


		BundleUtils.copyFiles(bundle_id, "src/", "org/txm/macro", "urs", macroDirectory, true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		BundleUtils.copyFiles(bundle_id, "src/", "org/txm/macro", "urs", macroDirectory2, true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		// saving current version
		//		URSPreferences.getInstance().put(URSPreferences.VERSION, currentVersion.toString());

		return URSMacroDirectory.exists() && URSMacroDirectory.listFiles().length > 0;
	}

	@Override
	public String getName() {
		return "URS Annotation"; //$NON-NLS-1$
	}

	@Override
	public boolean needsReinstall(Workspace workspace) {

		File txmhomedir = new File(Toolbox.getTxmHomePath());
		for (String folder : createfolders) {
			if (!new File(txmhomedir, folder).exists()) {
				return true;
			}
		}
		return false;
	}
}
