package org.txm.annotation.urs;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.osgi.util.NLS;
import org.txm.annotation.core.AnnotationEngine;
import org.txm.annotation.urs.messages.Messages;
import org.txm.core.results.TXMResult;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Project;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.DeleteDir;
import org.txm.utils.logger.Log;

import visuAnalec.donnees.Corpus;
import visuAnalec.donnees.Structure;
import visuAnalec.fichiers.FichiersJava;
import visuAnalec.vue.Vue;

/**
 * Manages Analec corpus: the corpus are associated with the TXM MainCorpus objects
 * 
 * Use URSCorpora.getCorpus(mcorpus) to start using Analec annotations
 * 
 * Test if a corpus has Analec annotations URSCorpora.hasCorpus(mcorpus)
 * 
 * Save changes with URSCorpora.saveCorpus(mcorpus)
 * 
 * @author mdecorde
 *
 */
public class URSCorpora extends AnnotationEngine {

	private static final HashMap<CorpusBuild, Corpus> corpora = new HashMap<>();

	private static final HashMap<CorpusBuild, Vue> vues = new HashMap<>();

	/**
	 * Create the ec file if needed, store a reference to the corpus if not already done.
	 * 
	 * @param mcorpus
	 * @return the Corpus loaded and stored
	 */
	public static Corpus getCorpus(CorpusBuild mcorpus) {
		mcorpus = mcorpus.getRootCorpusBuild(); // ensure this is the root CorpusBuild
		File fichierCorpus = getECFile(mcorpus);
		File fichierVue = getECVFile(mcorpus);
		Corpus corpus = null;
		if (URSCorpora.corpora.containsKey(mcorpus)) {
			corpus = URSCorpora.corpora.get(mcorpus);
		}
		else {
			corpus = new Corpus();

			if (fichierCorpus.exists()) {
				if (!FichiersJava.ouvrirCorpus(corpus, fichierCorpus, false)) {
					System.out.println(NLS.bind(Messages.URSCorpora_3, fichierCorpus));
					return null;
				}
			}
			else {
				fichierCorpus.getParentFile().mkdirs();

				if (!FichiersJava.enregistrerCorpus(corpus, fichierCorpus)) {
					System.out.println(NLS.bind(Messages.URSCorpora_4, fichierCorpus));
					return null;
				}
			}

			URSCorpora.corpora.put(mcorpus, corpus);

			Vue vue = getVue(mcorpus);

			// TODO: does not work :(
			if (fichierVue.exists()) {
				if (!FichiersJava.ouvrirVue(vue, fichierVue)) {
					Log.warning(NLS.bind(Messages.WarningFailToOpenCorpusVueFileP0, fichierVue));
					// return null;
				}
				String[] types = vue.getTypesUnitesAVoir();
				if (types == null || types.length == 0) {
					vue.retablirVueParDefaut();
				}
			}
			else {
				vue.retablirVueParDefaut(); //
				if (!FichiersJava.enregistrerVue(vue, fichierVue)) {
					Log.warning(NLS.bind(Messages.WarningFailToCreateCorpusVueFileP0, fichierVue));
					// return null;
				}
			}
		}
		return corpus;
	}

	public static boolean isAnnotationStructureReady(CorpusBuild mcorpus) {

		mcorpus = mcorpus.getRootCorpusBuild(); // ensure this is the root CorpusBuild

		if (URSCorpora.corpora.containsKey(mcorpus)) {
			return !URSCorpora.corpora.get(mcorpus).getStructure().isVide();
		}
		else {
			File fichierCorpus = getECFile(mcorpus);
			if (fichierCorpus.exists()) {
				return !getCorpus(mcorpus).getStructure().isVide();
			}
			else {
				return false;
			}
		}
	}

	/**
	 * need the corpus to be loaded
	 * 
	 * @param mcorpus
	 * @return the corpus Vue
	 */
	public static Vue getVue(CorpusBuild mcorpus) {
		mcorpus = mcorpus.getRootCorpusBuild(); // ensure this is the root CorpusBuild

		if (!vues.containsKey(mcorpus)) {
			if (!corpora.containsKey(mcorpus)) {
				getCorpus(mcorpus);
			}
			Corpus corpus = getCorpus(mcorpus);
			Vue vue = new Vue(corpus);
			vues.put(mcorpus, vue);
		}
		return vues.get(mcorpus);
	}

	/**
	 * Has a Vue being loaded for a Corpusbuild
	 * 
	 * @param mcorpus
	 * @return true if a Vue is loaded
	 */
	public static boolean hasVue(CorpusBuild mcorpus) {
		mcorpus = mcorpus.getRootCorpusBuild(); // ensure this is the root CorpusBuild

		return vues.containsKey(mcorpus);
	}

	/**
	 * Save the associated AnalecCorpus
	 * 
	 * @param mcorpus
	 * @return true if the annotations have been saved
	 */
	public static boolean saveCorpus(CorpusBuild mcorpus) {
		mcorpus = mcorpus.getRootCorpusBuild(); // ensure this is the root CorpusBuild
		if (hasCorpus(mcorpus)) {
			Corpus corpus = getCorpus(mcorpus);
			Vue vue = getVue(mcorpus);

			File fichierCorpus = getECFile(mcorpus);
			File fichierVueCorpus = getECVFile(mcorpus);
			fichierCorpus.getParentFile().mkdirs();

			return FichiersJava.enregistrerCorpus(corpus, fichierCorpus) && FichiersJava.enregistrerVue(vue, fichierVueCorpus);
		}
		else {
			return false;
		}
	}

	public static Vue getVue(Corpus corpus) {
		for (CorpusBuild mcorpus : corpora.keySet()) {
			if (corpora.get(mcorpus) == corpus) return vues.get(mcorpus);
		}
		return null;
	}

	public static boolean saveCorpus(Corpus corpus) {
		for (CorpusBuild mcorpus : corpora.keySet()) {
			if (corpora.get(mcorpus) == corpus) return saveCorpus(mcorpus);
		}
		return false;
	}

	public static void removeCorpus(CorpusBuild mcorpus) {

		mcorpus = mcorpus.getRootCorpusBuild(); // ensure this is the root CorpusBuild

		if (!vues.containsKey(mcorpus)) {
			vues.remove(mcorpus);
		}
		if (!corpora.containsKey(mcorpus)) {
			corpora.remove(mcorpus);
		}
	}

	public static void revert(CorpusBuild mcorpus) {

		mcorpus = mcorpus.getRootCorpusBuild(); // ensure this is the root CorpusBuild
		Corpus acorpus = corpora.get(mcorpus);
		if (acorpus == null) return;
		corpora.remove(mcorpus);
		vues.remove(mcorpus);
		mcorpus.setIsModified(false);
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	public boolean initialize() throws Exception {
		return true;
	}

	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		return true;
	}

	@Override
	public boolean stop() throws Exception {
		return true;
	}

	@Override
	public void notify(TXMResult r, String state) {
		if (r instanceof CorpusBuild && "clean".equals(state)) { // $NON-NLS //$NON-NLS-1$
			CorpusBuild mcorpus = (CorpusBuild) r;
			mcorpus = mcorpus.getRootCorpusBuild(); // ensure this is the root CorpusBuild

			if (URSCorpora.hasCorpus(mcorpus)) {
				Corpus analec_corpus = URSCorpora.getCorpus(mcorpus);
				analec_corpus.fermer();
			}

			URSCorpora.removeCorpus(mcorpus);

			File fichierCorpus = getECFile(mcorpus);
			File fichierVue = getECVFile(mcorpus);
			fichierCorpus.delete();
			fichierVue.delete();
		}
		else if (r instanceof Project) {
			Project project = ((Project) r);
			if ("clean".equals(state)) { //$NON-NLS-1$
				File analecDir = new File(project.getProjectDirectory(), "analec"); //$NON-NLS //$NON-NLS-1$
				if (analecDir.exists()) {
					DeleteDir.deleteDirectory(analecDir);
				}
			}
			else if ("save".equals(state)) { //$NON-NLS-1$
				for (TXMResult c : project.getChildren(CorpusBuild.class)) {
					URSCorpora.saveCorpus((CorpusBuild) c);
				}
			}
		}
	}

	@Override
	public String getName() {
		return "URS"; //$NON-NLS-1$
	}

	@Override
	public String getDetails() {

		return corpora.toString();
	}

	@Override
	public boolean hasAnnotationsToSave(CorpusBuild mcorpus) {

		return hasAnnotationsToSaveStatic(mcorpus);
	}

	public static boolean hasAnnotationsToSaveStatic(CorpusBuild mcorpus) {

		if (corpora.isEmpty()) return false;
		if (!URSCorpora.hasCorpus(mcorpus)) return false; // no corpus instanced -> no annotations to save
		return URSCorpora.getCorpus(mcorpus).isModifie();
	}

	/**
	 * 
	 * @param mcorpus
	 * @return true is an Analec corpus is already instanced
	 */
	public static boolean hasCorpus(CorpusBuild mcorpus) {

		if (!corpora.containsKey(mcorpus)) {
			return getECFile(mcorpus).exists();
		}

		return true;
	}

	public static File getECFile(CorpusBuild mcorpus) {
		return new File(mcorpus.getProjectDirectory(), "analec/" + mcorpus.getID() + ".ec"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public static File getECVFile(CorpusBuild mcorpus) {
		return new File(mcorpus.getProjectDirectory(), "analec/" + mcorpus.getID() + ".ecv"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public String hasAdditionalDetailsForResult(TXMResult result) {
		if (result instanceof MainCorpus) {
			return hasCorpus((MainCorpus) result) ? Messages.Annotation : null;
		}
		return null;
	}

	public String getAdditionalDetailsForResult(TXMResult result) {

		if (result instanceof MainCorpus) {
			Corpus acorpus = getCorpus((MainCorpus) result);

			Structure s = acorpus.getStructure();
			HashSet<String> unites = s.getUnites();
			HashSet<String> relations = s.getRelations();
			HashSet<String> schemas = s.getSchemas();

			if (unites.size() == 0 && relations.size() == 0 && schemas.size() == 0) {
				return "<p>"+Messages.noURStructure+"</p>"; //$NON-NLS-1$
			}

			StringBuilder buffer = new StringBuilder();
			buffer.append(Messages.URSInformationsHTML);
			buffer.append(Messages.StructureHTML);

			buffer.append(Messages.UnitsHTML);
			if (unites.size() == 0) {
				buffer.append("<p>None.</p>\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
			for (String u : unites) {
				buffer.append("<p>" + u + ": " + StringUtils.join(s.getUniteProperties(u), ", ") + "</p>\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
			buffer.append(Messages.RelationsHTML);

			if (relations.size() == 0) {
				buffer.append("<p>None.</p>\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
			for (String u : relations) {
				buffer.append("<p>" + u + ": " + StringUtils.join(s.getRelationProperties(u), ", ") + "</p>\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
			buffer.append(Messages.SchemasHTML);
			if (schemas.size() == 0) {
				buffer.append("<p>None.</p>\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
			for (String u : schemas) {
				buffer.append("<p>" + u + ": " + StringUtils.join(s.getSchemaProperties(u), ", ") + "</p>\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
			//buffer.append("<p>Name: "+acorpus.getStructure()+"</p>\n");

			return buffer.toString();
		}
		return null;
	}
}
