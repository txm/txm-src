package org.txm.annotation.urs.rcp.testers;

import java.util.List;

import org.eclipse.core.expressions.PropertyTester;
import org.txm.annotation.urs.URSCorpora;
import org.txm.searchengine.cqp.corpus.MainCorpus;

public class IsURSCorpusDirty extends PropertyTester {

	@SuppressWarnings("rawtypes")
	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {

		if (receiver instanceof List) {
			List list = (List) receiver;
			if (list.size() == 0) return true;
			receiver = list.get(0);
		}
		if (receiver instanceof MainCorpus) {
			return URSCorpora.hasAnnotationsToSaveStatic((MainCorpus) receiver);
		}

		return false;
	}
}
