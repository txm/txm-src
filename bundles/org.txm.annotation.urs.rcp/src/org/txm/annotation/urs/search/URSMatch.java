package org.txm.annotation.urs.search;

import org.txm.objects.Match2P;

import visuAnalec.elements.Element;

public class URSMatch extends Match2P {

	Element element;

	public URSMatch(Element element, int start, int end) {
		super(start, end);
		this.element = element;
	}

	public String getValue(String property) {
		return this.element.getProp(property);
	}

}
