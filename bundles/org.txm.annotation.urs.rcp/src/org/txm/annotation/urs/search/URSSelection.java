package org.txm.annotation.urs.search;

import java.util.ArrayList;

import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.SimpleSelection;

public class URSSelection extends SimpleSelection<URSMatch> {

	public URSSelection(IQuery query, ArrayList<URSMatch> matches2) {
		super(query, matches2);
	}

}
