package org.txm.annotation.urs.search;

import org.txm.Toolbox;
import org.txm.core.engines.EngineType;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.SearchEngine;

/**
 * 
 * Simple implementation of URS query in the "TYPE@PROPERTY=REGEX" form
 * 
 * @author mdecorde
 *
 */
public class URSQuery extends Query {

	public URSQuery() {

		super("", (SearchEngine) Toolbox.getEngineManager(EngineType.SEARCH).getEngine("URS")); //$NON-NLS-1$
	}

	public URSQuery(String queryString) {

		super(queryString, (SearchEngine) Toolbox.getEngineManager(EngineType.SEARCH).getEngine("URS")); //$NON-NLS-1$
	}

	@Override
	public IQuery fixQuery(String lang) {
		return this;
	}
}
