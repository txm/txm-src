package org.txm.annotation.urs.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IProgressMonitor;
import org.txm.annotation.urs.URSCorpora;
import org.txm.core.results.TXMResult;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Match;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.SearchEngine;
import org.txm.searchengine.core.SearchEngineProperty;
import org.txm.searchengine.core.Selection;
import org.txm.utils.logger.Log;

import visuAnalec.donnees.Corpus;
import visuAnalec.elements.Element;
import visuAnalec.elements.Relation;
import visuAnalec.elements.Schema;
import visuAnalec.elements.Unite;

public class URSSearchEngine extends SearchEngine {

	public static final String NAME = "URS"; //$NON-NLS-1$

	public URSSearchEngine() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	public boolean initialize() throws Exception {
		return true;
	}

	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		return true;
	}

	@Override
	public boolean stop() throws Exception {
		return true;
	}

	@Override
	public void notify(TXMResult r, String state) {
		// nothing
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public Selection query(CorpusBuild corpus, IQuery query, String name, boolean saveQuery) throws Exception {

		ArrayList<Element> elements = findAllInCorpus(0, URSCorpora.getCorpus(corpus), corpus, query.getQueryString());
		ArrayList<URSMatch> matches = new ArrayList<URSMatch>();
		for (Element e : elements) {
			matches.add(new URSMatch(e, e.getUnite0().getDeb(), e.getUnite0().getFin()));
		}
		return new URSSelection(query, matches);
	}

	static String[] getFilterParameters(String URSQL) {

		String clazz = "U"; //$NON-NLS-1$
		String type = ""; //$NON-NLS-1$
		String prop = ""; //$NON-NLS-1$
		Boolean mustmatch = true ; 
		String value = ""; //$NON-NLS-1$

		if (URSQL == null) return new String[] { type, prop, value };

		int clazzidx = URSQL.indexOf("/"); //TODO parse query to find out which URS Element use (U/R/S/E)  //$NON-NLS-1$
		int atidx = URSQL.indexOf("@"); //$NON-NLS-1$
		int equalidx = URSQL.indexOf("="); //$NON-NLS-1$
		
		// U/Entity@Property=value
		if (clazzidx > 0 && atidx > clazzidx && equalidx > atidx) {
			clazz = URSQL.substring(0, clazzidx );
			type = URSQL.substring(clazzidx + 1, atidx);
			prop = URSQL.substring(atidx + 1, equalidx);
			value = URSQL.substring(equalidx + 1);
			
			mustmatch = equalidx <= 0 || '!' != URSQL.charAt(equalidx - 1); //$NON-NLS-1$
			if (!mustmatch) {
				prop = prop.substring(0, prop.length()-1);
			}
		} // Entity@Property=value
		else if (clazzidx > 0 && atidx > clazzidx) {
			clazz = URSQL.substring(0, clazzidx );
			type = URSQL.substring(clazzidx + 1, atidx);
			prop = URSQL.substring(atidx + 1);
		} // Entity@Property=value
		else if (clazzidx > 0) {
			clazz = URSQL.substring(0, clazzidx );
			type = URSQL.substring(clazzidx + 1);
		} // Entity@Property=value
		else if (atidx > 0 && equalidx >= 0 && atidx < equalidx) {
			type = URSQL.substring(0, atidx);
			prop = URSQL.substring(atidx + 1, equalidx);
			value = URSQL.substring(equalidx + 1);
			
			mustmatch = equalidx <= 0 || '!' != URSQL.charAt(equalidx - 1); //$NON-NLS-1$
			if (!mustmatch) {
				prop = prop.substring(0, prop.length()-1);
			}
			
		} // Entity@Property
		else if (atidx > 0) {
			type = URSQL.substring(0, atidx);
			prop = URSQL.substring(atidx + 1);
		} // Property=value
		else if (equalidx > 0) {
			prop = URSQL.substring(0, equalidx);
			value = URSQL.substring(equalidx + 1);
			
			mustmatch = equalidx <= 0 || '!' != URSQL.charAt(equalidx - 1); //$NON-NLS-1$
			if (!mustmatch) {
				prop = prop.substring(0, prop.length()-1);
			}
		}
		else {
			type = URSQL;
		}
		//	println(["'"+type+"'", "'"+prop+"'", "'"+value+"'"])
		

		return new String[] { clazz, type, mustmatch.toString(), prop, value };
	}
	
	public static void main(String[] args) {
		
		List<String> queries = Arrays.asList(
				"E/Entity@Property=value",
				"E/Entity@Property!=value",
				"E/Entity@Property",
				"E/Entity",
				"Entity@Property=value",
				"Entity@Property!=value",
				"Entity@Property",
				"Property=value",
				"Property!=value",
				"Entity");
		List<String> sysout = Arrays.asList(
				"[E, Entity, true, Property, value]",
				"[E, Entity, false, Property, value]",
				"[E, Entity, true, Property, ]",
				"[E, Entity, true, , ]",
				"[U, Entity, true, Property, value]",
				"[U, Entity, false, Property, value]",
				"[U, Entity, true, Property, ]",
				"[U, , true, Property, value]",
				"[U, , false, Property, value]",
				"[U, Entity, true, , ]");
		for (int i = 0 ; i < queries.size() ; i++) {
			String s  = Arrays.toString(getFilterParameters(queries.get(i)));
			System.out.println(queries.get(i)+ " -> "+s+" OK? "+s.equals(sysout.get(i)));
		}
			
	}

	static ArrayList<Element> findAllInCorpus(int debug, Corpus analecCorpus, CorpusBuild corpus, String URSQL) {
		String[] params = getFilterParameters(URSQL);
		if (debug >= 2) Log.finer("PARAMS=$params"); //$NON-NLS-1$
		return findAllInCorpus(debug, analecCorpus, corpus, params[0], params[1], params[3], Boolean.parseBoolean(params[2]), params[4]);
	}

	static ArrayList<Element> findAllInCorpus(int debug, Corpus analecCorpus, CorpusBuild corpus, String clazzString, String typeRegex, String propName, boolean valueMustMatch, String valueRegex) {
		Class<? extends Element> clazz = Unite.class;
		if ("R".equals(clazzString)) { //$NON-NLS-1$
			clazz = Relation.class;
		}
		else if ("S".equals(clazzString)) { //$NON-NLS-1$
			clazz = Schema.class;
		}
		else if ("E".equals(clazzString)) { //$NON-NLS-1$
			clazz = Element.class;
		}
		return findAllInCorpus(debug, analecCorpus, corpus, clazz, typeRegex, propName, valueMustMatch, valueRegex);
	}

	static ArrayList<Element> findAllInCorpus(int debug, Corpus analecCorpus, CorpusBuild corpus, Class<? extends Element> elemClazz, String typeRegex, String propName, boolean valueMustMatch,
			String valueRegex) {
		ArrayList<Element> allElements = new ArrayList<Element>();

		if (elemClazz != null) {
			if (elemClazz == Unite.class) {
				allElements.addAll(analecCorpus.getToutesUnites());
			} else if (elemClazz == Relation.class) {
				allElements.addAll(analecCorpus.getToutesRelations());
			} else if (elemClazz == Schema.class) {
				allElements.addAll(analecCorpus.getTousSchemas());
			} else if (elemClazz == Element.class) {
				allElements.addAll(analecCorpus.getToutesUnites());
				allElements.addAll(analecCorpus.getToutesRelations());
				allElements.addAll(analecCorpus.getTousSchemas());
			}
		}
		else {
			allElements.addAll(analecCorpus.getToutesUnites());
			allElements.addAll(analecCorpus.getToutesRelations());
			allElements.addAll(analecCorpus.getTousSchemas());
		}

		return filterElements(debug, allElements, corpus, typeRegex, propName, valueMustMatch, valueRegex);
	}

	/**
	 * group units by CQP match
	 * 
	 * units are sorted for faster processing
	 * 
	 * @param allUnites
	 * @param matches
	 * @param strict_inclusion
	 * @return
	 */
	static LinkedHashMap<Match, ArrayList<Element>> groupByMatch(int debug, ArrayList<Element> allUnites, List<? extends Match> matches, boolean strict_inclusion) {
		if (debug >= 2) Log.finer("group " + allUnites.size() + " units with " + matches.size() + " strict=$strict_inclusion"); //$NON-NLS-1$
		//println allUnites.collect() {it -> it.getDeb()}
		Collections.sort(allUnites);// { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=> b.getFin() };
		//println allUnites.collect() {it -> it.getDeb()}
		int unitsSize = allUnites.size();
		int iCurrentUnit = 0;
		ArrayList<Element> selectedUnits = new ArrayList<Element>();

		int matchesSize = matches.size();
		int iCurrentMatch = 0;

		LinkedHashMap<Match, ArrayList<Element>> selectedUnitsPerMatch = new LinkedHashMap<Match, ArrayList<Element>>();
		selectedUnitsPerMatch.put(matches.get(iCurrentMatch), selectedUnits);

		while (iCurrentMatch < matchesSize && iCurrentUnit < unitsSize) {
			if (debug >= 2) Log.finer("** M $iCurrentMatch < $matchesSize && U $iCurrentUnit < $unitsSize"); //$NON-NLS-1$

			Element e = allUnites.get(iCurrentUnit);
			Unite unit = e.getUnite0();
			Match match = matches.get(iCurrentMatch);
			if (debug >= 3) Log.finer("" + unit.getDeb() + "->" + unit.getFin() + "	" + match.getStart() + "->" + match.getEnd()); //$NON-NLS-1$
			if (unit.getFin() < match.getStart()) {
				if (debug >= 3) Log.finer("next unit"); //$NON-NLS-1$
				iCurrentUnit++;
			}
			else if (unit.getDeb() > match.getEnd()) {
				if (debug >= 3) Log.finer("next match"); //$NON-NLS-1$
				iCurrentMatch++;
				selectedUnits = new ArrayList<Element>();
				selectedUnitsPerMatch.put(match, selectedUnits);
			}
			else {
				if (debug >= 3) Log.finer("iCurrentUnit=$iCurrentUnit	iCurrentMatch=$iCurrentMatch"); //$NON-NLS-1$
				if (strict_inclusion) {

					if (debug >= 3) Log.finer("m.start ${match.getStart()} <= u.deb ${unit.getDeb()} && u.fin ${unit.getFin()} <= m.end ${match.getEnd()}"); //$NON-NLS-1$
					if (match.getStart() <= unit.getDeb() && unit.getFin() <= match.getEnd()) {
						selectedUnits.add(unit);
					}
				}
				else {
					selectedUnits.add(unit);
				}

				iCurrentUnit++;
			}
		}
		return selectedUnitsPerMatch;
	}

	static ArrayList<Element> filterUniteByInclusion(int debug, ArrayList<Element> allUnites, List<? extends Match> matches, boolean strict_inclusion, int limit_distance) {

		LinkedHashMap<Match, ArrayList<Element>> selectedUnitsPerMatch = groupByMatch(debug, allUnites, matches, strict_inclusion);
		//println "selectedUnitsPerMatch size="+selectedUnitsPerMatch.size()
		ArrayList<Element> selectedUnits = new ArrayList<Element>();
		if (limit_distance != 0) {
			if (limit_distance > 0) limit_distance--;

			for (Match m : selectedUnitsPerMatch.keySet()) {
				ArrayList<Element> units = selectedUnitsPerMatch.get(m);
				if (units.size() > limit_distance && units.size() > 0) {
					//println "$m -> "+units.collect() {it -> it.getDeb()}
					Collections.sort(units);
					//println "$m -> "+units.collect() {it -> it.getDeb()}
					selectedUnits.add(units.get(limit_distance));
					if (debug >= 3) Log.finer("dist select: " + units.get(limit_distance).getUnite0().getDeb()); //$NON-NLS-1$
				}
			}
		}
		else {
			for (Match m : selectedUnitsPerMatch.keySet())
				selectedUnits.addAll(selectedUnitsPerMatch.get(m));
		}

		return selectedUnits;
	}

	static ArrayList<Element> filterElements(int debug, ArrayList<Element> allElements, CorpusBuild corpus, String typeRegex, String propName, boolean valueMustMatch, String valueRegex) {
		if (debug >= 2) Log.finer("filtering " + allElements.size() + " elements with typeRegex='$typeRegex' propName='$propName' and valueRegex" + (valueMustMatch ? "" : "!") + "='$valueRegex'"); //$NON-NLS-1$

		if (corpus != null) {
			allElements = filterUniteByInclusion(debug, allElements, corpus.getMatches(), true, 0);
		}

		if (typeRegex != null && typeRegex.length() > 0) {
			ArrayList<Element> filteredElements = new ArrayList<Element>();
			Pattern pattern = Pattern.compile(typeRegex);
			for (Element element : allElements) {
				if (pattern.matcher(element.getType()).matches()) {
					filteredElements.add(element);
				}
			}

			allElements = filteredElements;
		}
		if (debug >= 2) Log.finer(" type step result: " + allElements.size()); //$NON-NLS-1$

		if (propName != null && propName.length() > 0) {
			ArrayList<Element> filteredElements = new ArrayList<Element>();
			if (valueRegex != null && valueRegex.length() > 0) {  // select only elements with the prop&value
				Pattern pattern = Pattern.compile(valueRegex);
				for (Element element : allElements) {
					String value = element.getProp(propName);
					if (value != null && pattern.matcher(value).matches() == valueMustMatch) {
						filteredElements.add(element);
					}
				}
			}
			else { // select only elements with the prop
				for (Element element : allElements) {
					if (element.getProps().containsKey(propName)) {
						filteredElements.add(element);
					}
				}
			}

			allElements = filteredElements;
		}
		if (debug >= 2) Log.finer(" prop&value step result: " + allElements.size()); //$NON-NLS-1$
		return allElements;
	}

	@Override
	public Query newQuery() {
		return new URSQuery();
	}

	@Override
	public boolean hasIndexes(CorpusBuild corpus) {
		return URSCorpora.isAnnotationStructureReady(corpus);
	}

	@Override
	public String getValueForProperty(Match match, SearchEngineProperty property) {
		if (match instanceof URSMatch) {
			return ((URSMatch) match).getValue(property.getName());
		}
		return null;
	}

	@Override
	public List<String> getValuesForProperty(Match match, SearchEngineProperty property) {
		ArrayList<String> a = new ArrayList<String>();
		a.add(getValueForProperty(match, property));
		return a;
	}

}
