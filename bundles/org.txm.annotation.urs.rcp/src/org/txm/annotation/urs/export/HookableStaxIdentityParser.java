package org.txm.annotation.urs.export;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.txm.importer.StaxIdentityParser;

public class HookableStaxIdentityParser extends StaxIdentityParser {


	String hookPath;

	private XMLStreamWriter hookedwriter;

	private BufferedOutputStream hookedoutput;

	private XMLStreamReader hookedparser;

	public HookableStaxIdentityParser(File infile, String hookPath) throws IOException, XMLStreamException {
		super(infile);
		// TODO Auto-generated constructor stub
	}

	public boolean process(XMLStreamWriter awriter) throws XMLStreamException, IOException {
		this.writer = awriter;
		goToEnd(hookPath); // write all until text is reached
		this.hookedwriter = writer;
		this.hookedoutput = output;
		this.hookedparser = parser;
		this.writer = null;
		this.output = null;
		this.parser = null;
		return true;
	}

	public XMLStreamWriter getWriter() {
		return hookedwriter;
	}

	public void close() throws XMLStreamException, IOException {
		hookedwriter.close();
		hookedoutput.close();
		hookedparser.close();
	}
}
