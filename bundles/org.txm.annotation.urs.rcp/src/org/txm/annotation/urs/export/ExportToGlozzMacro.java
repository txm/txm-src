package org.txm.annotation.urs.export;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;

import org.eclipse.osgi.util.NLS;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

import visuAnalec.donnees.Corpus;
import visuAnalec.donnees.Structure;
import visuAnalec.elements.Unite;
import visuAnalec.fichiers.FichiersGlozz.ModelGlozzPrinter;

/**
 * @author: Bruno Oberlé
 * 
 *          v1.0.0 2017-08-28
 * 
 *          adapted and included in TXM by mdecorde
 * 
 *          Cette macro exporte le corpus sélectionné et ses annotations vers deux fichiers de format Glozz:
 *          - un fichier .ac contenant le corpus brut,
 *          - un fichier .aa contenant les annotations au format XML utilisé par Glozz,
 *          - un fichier .aam contenant la structure d'annotation.
 * 
 *          Le corpus sélectionné dans TXM devrait contenir une structure Analec avec au moins un type d'unité défini (e.g. MENTION, maillon, etc.). S'il n'y a pas de structure,
 *          ce n'est pas grave: le fichier est exporter, mais aucune annotation n'est créer. Cela permet d'exporter n'importe quel corpus au format Glozz.
 * 
 *          Pour exporter un texte au format Glozz *sans* les annotations qu'il contient, simplement mettre un unit_type qui n'existe pas (e.g. "foobar" au lieu de "MENTION").
 * 
 *          deprecated: La macro ne produit pour l'instant pas automatiquement de modèle Glozz (fichier .aam). Cela n'est pas un problème pour ouvrir le résultat dans Glozz ou Analec.
 **/
public class ExportToGlozzMacro {

	public void doExport(MainCorpus corpus, String unit_type, String filename) throws NumberFormatException, IOException, CqiServerError, CqiClientException {

		// export the annotation structure as a glozz model
		Corpus analecCorpus = URSCorpora.getCorpus(corpus);
		if (analecCorpus == null) {
			Log.warning(Messages.bind(Messages.errorNoAnalecCorpusAssociatedToTheCQPCorpusP0, corpus));
			return;
		}
		Log.info(Messages.writingAnnotationStructure);
		PrintWriter model = null;
		File fichierModel = new File(filename + ".aam"); //$NON-NLS-1$
		try {
			Structure structure = analecCorpus.getStructure();
			model = new PrintWriter(fichierModel, "UTF-8"); //$NON-NLS-1$
			ModelGlozzPrinter gp = new ModelGlozzPrinter(structure, model);
			gp.ecrireModel();
		}
		catch (Exception ex) {
			Log.warning(NLS.bind(Messages.errorWhileWritingP0, ex));
		}
		finally {
			if (model != null) model.close();
			Log.info(NLS.bind(Messages.annotationStructureWrittenInP0, fichierModel));
		}

		int size = corpus.getSize(); // you may also use:
		// corpus.getTextEndLimits() (= index of
		// last token = size-1)
		AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();
		Property word = corpus.getWordProperty();
		// note: using "lbn" seems to work better than "pn" (some imported
		// corpora are missing this information)
		// pn = corpus.getProperty("pn")
		// if (pn == null) pn = corpus.getProperty("div")
		Property pn = corpus.getProperty("lbn"); //$NON-NLS-1$

		// BUILD THE RAW TEXT, THE POSITIONS AND FIND THE PARAGRAPHS

		String rawText = ""; // the corpus for the .ac file $NON-NLS-1$
		ArrayList<int[]> positions = new ArrayList<>(); // each element is an array
		// [start, end] indicating the
		// position in the rawText
		int pnCount = 0; // the par counter, used for indexing the pns array
		int lastPn = -1; // the last paragraph number
		ArrayList<int[]> pns = new ArrayList<>(); // each element is an array [start,
		// end] representing the start and
		// end of the paragraph in the
		// rawText
		int end = -1;
		for (int i = 0; i < size; i++) {
			String f = CQI.cpos2Str(word.getQualifiedName(), new int[] { i })[0];
			int p = 0;
			if (pn == null) {
				p = 1;
			}
			else {
				p = Integer.parseInt(CQI.cpos2Str(pn.getQualifiedName(), new int[] { i })[0]);
			}
			int start = rawText.length();
			rawText += f;
			if (lastPn != p) {
				pnCount++;
				if (pnCount > 1 && pns.size() > 0) {
					pns.get(pnCount - 2)[1] = end;
				}
				pns.add(new int[] { start, 0 });
			}
			lastPn = p;
			end = rawText.length(); // must be after setting it up in pns!
			if (i != size - 1)
				rawText += " "; //$NON-NLS-1$
			positions.add(new int[] { start, end });
		}
		pns.get(pnCount - 1)[1] = end;
		Log.info(NLS.bind(Messages.P0ParagraphsFound, pnCount));

		// CORPUS ANALEC (GET THE ANNOTATIONS)

		// note that unit_type has been defined with an option of the dialog at
		// the beginning


		// list of properties

		Structure struct = analecCorpus.getStructure();
		HashSet<String> propertyList = struct.getUniteProperties(unit_type);

		// export to file (corpus)

		Log.info(Messages.writingCorpus);
		String corpusFilename = filename + ".ac"; //$NON-NLS-1$
		File corpusFile = new File(corpusFilename);
		PrintWriter corpusFileWriter = IOUtils.getWriter(corpusFile);
		corpusFileWriter.write(rawText);
		corpusFileWriter.close();
		Log.info(NLS.bind(Messages.corpusWrittenInP0, corpusFilename));

		// export to file (annotations)
		Log.info(Messages.writingAnnotations);
		String annotFilename = filename + ".aa"; //$NON-NLS-1$
		File annotFile = new File(annotFilename);
		PrintWriter annotFileWriter = IOUtils.getWriter(annotFile);
		annotFileWriter.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n<annotations>");  //$NON-NLS-1$
		int counter = 0;

		// export paragraphs
		for (int i = 0; i < pns.size(); i++) {
			int start = pns.get(i)[0];
			end = pns.get(i)[1];
			annotFileWriter.println("<unit id=\"me_" + counter + "\">"); //$NON-NLS-1$
			annotFileWriter.println("<metadata><author>me</author><creation-date>" + counter + "</creation-date></metadata>"); //$NON-NLS-1$
			annotFileWriter.println("<characterisation><type>paragraph</type><featureSet /></characterisation>"); //$NON-NLS-1$
			annotFileWriter.println("<positioning><start><singlePosition index=\"" + start + "\" /></start><end><singlePosition index=\"" + end + "\" /></end></positioning>"); //$NON-NLS-1$
			annotFileWriter.println("</unit>"); //$NON-NLS-1$
			counter++;
		}

		// export units
		ArrayList<Unite> units = analecCorpus.getUnites(unit_type);
		// units.sort() { a, b -> a.getDeb() <=> b.getDeb() ?: a.getFin() <=>
		// b.getFin() };
		int unitCount = 0;
		for (Unite unit : units) {
			unitCount++;
			annotFileWriter.println("<unit id=\"me_" + counter + "\">"); //$NON-NLS-1$
			annotFileWriter.println("<metadata><author>me</author><creation-date>" + counter + "</creation-date></metadata>"); //$NON-NLS-1$
			annotFileWriter.println("<characterisation>"); //$NON-NLS-1$
			annotFileWriter.println("<type>" + unit_type + "</type>"); //$NON-NLS-1$
			annotFileWriter.println("<featureSet>"); //$NON-NLS-1$
			for (String propertyName : propertyList) {
				annotFileWriter.println("<feature name=\"" + propertyName + "\">" + unit.getProp(propertyName) + "</feature>"); //$NON-NLS-1$
			}
			annotFileWriter.println("</featureSet>"); //$NON-NLS-1$
			annotFileWriter.println("</characterisation>"); //$NON-NLS-1$
			int start = positions.get(unit.getDeb())[0];
			end = positions.get(unit.getFin())[1];
			annotFileWriter.println("<positioning><start><singlePosition index=\"" + start + "\" /></start><end><singlePosition index=\"" + end + "\" /></end></positioning>"); //$NON-NLS-1$
			annotFileWriter.println("</unit>"); //$NON-NLS-1$
			counter++;
		}
		annotFileWriter.println("</annotations>"); //$NON-NLS-1$
		annotFileWriter.close();
		Log.info(NLS.bind(Messages.P0UnitsFound, unitCount));

		Log.info(NLS.bind(Messages.annotationsWrittenToP0, annotFile));
	}
}
