package org.txm.annotation.urs.export;

import java.io.File;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.osgi.util.NLS;
import org.txm.annotation.urs.messages.Messages;
import org.txm.importer.PersonalNamespaceContext;
import org.txm.importer.StaxIdentityParser;
import org.txm.scripts.importer.StaxStackWriter;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.utils.BundleUtils;
import org.txm.utils.DeleteDir;
import org.txm.utils.io.FileCopy;
import org.txm.utils.logger.Log;
import org.txm.utils.zip.Zip;

import visuAnalec.donnees.Corpus;
import visuAnalec.donnees.Structure;
import visuAnalec.elements.Element;
import visuAnalec.elements.Relation;
import visuAnalec.elements.Schema;
import visuAnalec.elements.Unite;
import visuAnalec.fichiers.FichiersGlozz;
import visuAnalec.fichiers.FichiersJava;
import visuAnalec.vue.Vue;

/**
 * Analec annotations exporter. It uses the XML-TXM format described at https://groupes.renater.fr/wiki/txm-info/public/xml_tei_txm
 * 
 * @author mdecorde
 *
 */
public class URSAnnotationTEIExporter {

	public static final DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd"); //$NON-NLS-1$

	protected File outputDirectory;

	protected MainCorpus mainCorpus;

	protected Property wordid;

	protected Corpus analecCorpus;

	protected Vue analecVue;

	protected IProgressMonitor monitor;

	protected boolean copyXMLTXMFiles = true;

	/**
	 * 
	 * @param monitor may be null
	 * @param outputDirectory
	 * @param mainCorpus
	 * @param analecCorpus
	 * @param analecVue
	 */
	public URSAnnotationTEIExporter(IProgressMonitor monitor, File outputDirectory, MainCorpus mainCorpus, Corpus analecCorpus, Vue analecVue, boolean copyXMLTXMFiles) {
		this.outputDirectory = outputDirectory;
		this.mainCorpus = mainCorpus;
		this.analecCorpus = analecCorpus;
		this.analecVue = analecVue;
		this.monitor = monitor;
		this.copyXMLTXMFiles = copyXMLTXMFiles;
	}

	/**
	 * 1- Copy the XML-TXM files
	 * 2- Write the annotations
	 * 3- Write the structure
	 * 4- Write the view
	 * 
	 * @return true if success
	 * @throws Exception
	 */
	public boolean process(boolean standoff, boolean createArchive) throws Exception {

		if (!standoff) {
			Log.warning("Inligned annotations not implemented yet. Aborting"); //$NON-NLS-1$
			return false;
		}

		wordid = mainCorpus.getProperty("id"); //$NON-NLS-1$
		int[] starts = mainCorpus.getTextStartLimits();
		int[] ends = mainCorpus.getTextEndLimits();
		StructuralUnit su_text = mainCorpus.getStructuralUnit("text"); //$NON-NLS-1$
		int[] strucs = CQPSearchEngine.getCqiClient().cpos2Struc(su_text.getQualifiedName(), starts);
		String[] ids = CQPSearchEngine.getCqiClient().struc2Str(su_text.getProperty("id").getQualifiedName(), strucs); //$NON-NLS-1$

		File textsDirectory = new File(outputDirectory, "texts"); //$NON-NLS-1$
		textsDirectory.mkdir();
		File annotationsDirectory = new File(outputDirectory, "annotations"); //$NON-NLS-1$
		if (standoff) annotationsDirectory.mkdir();

		if (monitor != null) monitor.beginTask(Messages.writingAnnotations3Dot, starts.length); //$NON-NLS-1$
		for (int i = 0; i < starts.length; i++) {
			File xmltxmFile = new File(textsDirectory, ids[i] + ".xml"); //$NON-NLS-1$
			File xmltxmAnalecFile = new File(annotationsDirectory, ids[i] + "-urs.xml"); //$NON-NLS-1$
			if (monitor != null) monitor.subTask(ids[i] + Messages.text3Dot); //$NON-NLS-1$
			processText(standoff, xmltxmFile, xmltxmAnalecFile, ids[i], starts[i], ends[i]);
			if (monitor != null) monitor.worked(1);
		}

		if (monitor != null) monitor.beginTask(Messages.writingStructure, 1); //$NON-NLS-1$

		//FichiersGlozz.exporterModeleGlozz(analecCorpus.getStructure());
		File xmltxmStructureFile = new File(outputDirectory, mainCorpus.getName() + ".aam"); //$NON-NLS-1$
		PrintWriter model = new PrintWriter(xmltxmStructureFile, "UTF-8"); //$NON-NLS-1$
		FichiersGlozz.ModelGlozzPrinter gp = new FichiersGlozz.ModelGlozzPrinter(analecCorpus.getStructure(), model);
		gp.ecrireModel();
		model.close();

		//FichiersGlozz.exporterModeleGlozz(analecCorpus.getStructure());
		File fichierVueCorpus = new File(outputDirectory, mainCorpus.getName() + ".ev"); //$NON-NLS-1$
		FichiersJava.enregistrerVue(analecVue, fichierVueCorpus);

		//		File xmltxmStructureFile = new File(outputDirectory, mainCorpus.getName()+"-structure.xml");
		//		StaxStackWriter writer = new StaxStackWriter(xmltxmStructureFile);
		//		PersonalNamespaceContext Nscontext = new PersonalNamespaceContext();
		//		writer.setNamespaceContext(Nscontext);
		//		writer.writeStartElement(StaxIdentityParser.TEINS, "TEI");
		//		writer.writeNamespace("tei", StaxIdentityParser.TEINS);
		//		writer.writeStartElement("teiHeader");
		//		writer.writeStartElement("fileDesc");
		//		writer.writeStartElement("titleStmt");
		//		writer.writeStartElement("title");
		//		writer.writeCharacters(mainCorpus.getName()+" - structure");
		//		writer.writeEndElement(); // title
		//		writer.writeStartElement("respStmt");
		//		writer.writeStartElement("resp");
		//		writer.writeCharacters("annotated by");
		//		writer.writeEndElement(); // resp
		//		writer.writeStartElement("name");
		//		writer.writeCharacters("non renseigné");
		//		writer.writeEndElement(); // name
		//		writer.writeEndElement(); // respStmt
		//		writer.writeEndElement(); // titleStmt
		//		writer.writeStartElement("publicationStmt");
		//		writer.writeStartElement("publisher");
		//		writer.writeCharacters("non renseigné");
		//		writer.writeEndElement(); // publisher
		//		writer.writeEndElement(); // publicationStmt
		//		writer.writeStartElement("sourceDesc");
		//		writer.writeStartElement("bibl");
		//		writer.writeCharacters("non renseigné");
		//		writer.writeEndElement(); // bibl
		//		writer.writeEndElement(); // sourceDesc
		//		writer.writeEndElement(); // fileDesc
		//		writer.writeEndElement(); // teiHeader
		//		writer.writeStartElement("text");
		//		writer.writeStartElement("back");
		//		writeStructure(writer);
		//		writer.writeEndElement(); // back
		//		writer.writeEndElement(); // text
		//		writer.writeEndElement(); // TEI
		//		writer.close();

		if (monitor != null) monitor.worked(1);

		if (createArchive) {
			if (monitor != null) monitor.subTask(Messages.creatingArchive3Dot); //$NON-NLS-1$
			File zipArchive = new File(outputDirectory + ".urs"); //$NON-NLS-1$
			Zip.compress(outputDirectory, zipArchive);
			DeleteDir.deleteDirectory(outputDirectory);
			Log.info(NLS.bind(Messages.DoneP0, zipArchive)); //$NON-NLS-1$
		}
		else {
			Log.info(NLS.bind(Messages.DoneP0, outputDirectory)); //$NON-NLS-1$
		}

		return true;
	}

	protected boolean processText(boolean standoff, File xmltxmFile, File xmltxmAnalecFile, String textid, int from, int to) throws Exception {

		Log.info(NLS.bind(Messages.processingTextP0, textid)); //$NON-NLS-1$

		// 1) copy the XML-TXM file
		File xmltxmFileFromBin = new File(mainCorpus.getProjectDirectory(), "txm/" + mainCorpus.getName().toUpperCase() + "/" + textid + ".xml"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		if (!xmltxmFileFromBin.exists()) {
			Log.warning(NLS.bind(Messages.errorCouldNotFindTheXMLTXMFileP0, xmltxmFileFromBin)); //$NON-NLS-1$
			return false;
		}

		if (copyXMLTXMFiles) {
			if (!FileCopy.copy(xmltxmFileFromBin, xmltxmFile)) {
				Log.warning(NLS.bind(Messages.errorCouldNotCopytheXMLTXMFileP0, xmltxmFileFromBin)); //$NON-NLS-1$
				return false;
			}
		}

		XMLStreamWriter writer = null;
		HookableStaxIdentityParser identidywriter = null;
		if (standoff) { // write a new file
			writer = new StaxStackWriter(xmltxmAnalecFile);
			PersonalNamespaceContext Nscontext = new PersonalNamespaceContext();
			writer.setNamespaceContext(Nscontext);
			writer.writeStartElement(StaxIdentityParser.TEINS, "TEI"); //$NON-NLS-1$
			writer.writeNamespace("tei", StaxIdentityParser.TEINS); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("teiHeader"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("fileDesc"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("titleStmt"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("title"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeCharacters(textid + " - annotations"); //$NON-NLS-1$
			writer.writeEndElement(); // title
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("respStmt"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("resp"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeCharacters("annotated by"); //$NON-NLS-1$
			writer.writeEndElement(); // resp
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("name"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeCharacters(System.getProperty("user.name")); //$NON-NLS-1$
			writer.writeEndElement(); // name
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement(); // respStmt
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement(); // titleStmt
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("publicationStmt"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("publisher"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeCharacters("non renseigné"); //$NON-NLS-1$
			writer.writeEndElement(); // publisher
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement(); // publicationStmt
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("sourceDesc"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("bibl"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeCharacters("non renseigné"); //$NON-NLS-1$
			writer.writeEndElement(); // bibl
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement(); // sourceDesc
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement(); // fileDesc
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement(); // teiHeader
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("text"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement(); // text
		}
		else {
			identidywriter = new HookableStaxIdentityParser(xmltxmFile, "text"); //$NON-NLS-1$
			identidywriter.process(xmltxmAnalecFile);
			writer = identidywriter.getWriter();
		}

		try {
			writer.writeStartElement("standOff"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("soHeader"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("titleStmt"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("title"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeCharacters("URS annotations"); //$NON-NLS-1$
			writer.writeEndElement(); // title
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement(); // titleStmt
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("respStmt"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("resp"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("name"); //$NON-NLS-1$
			writer.writeAttribute("id", System.getProperty("user.name")); //$NON-NLS-1$ //$NON-NLS-2$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeCharacters(System.getProperty("user.name")); //$NON-NLS-1$
			writer.writeEndElement(); // name
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement(); // resp
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement(); // respStmt
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("revisionDesc"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("change"); //$NON-NLS-1$
			writer.writeAttribute("who", "#" + System.getProperty("user.name")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			writer.writeAttribute("when", dateformat.format(Calendar.getInstance().getTime())); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeCharacters("Created with TXM " + BundleUtils.getBundleVersion("org.txm.rcp") + " and Analec extension " + BundleUtils.getBundleVersion("AnalecRCP") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					+ " with 'XML-TEI-URS Export....' export command on " + mainCorpus.getName() + " corpus"); //$NON-NLS-1$ //$NON-NLS-2$
			writer.writeEndElement(); // change
			writer.writeEndElement(); // revisionDesc
			writer.writeStartElement("encodingDesc"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("listPrefixDef"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("prefixDef"); //$NON-NLS-1$
			writer.writeAttribute("ident", "text"); //$NON-NLS-1$ //$NON-NLS-2$
			writer.writeAttribute("matchPattern", "(.+)"); //$NON-NLS-1$ //$NON-NLS-2$
			writer.writeAttribute("replacementPattern", "../texts/" + xmltxmFile.getName() + "#$1"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement(); // prefixDef
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement(); // listPrefixDef
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement(); // encodingDesc
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement(); // soHeader
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("annotations"); //$NON-NLS-1$
			writer.writeAttribute("type", "coreference"); //$NON-NLS-1$ //$NON-NLS-2$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			ArrayList<Element> writtenElements = new ArrayList<Element>();
			writtenElements.addAll(writeUnits(writer, from, to));
			writtenElements.addAll(writeRelations(writer, from, to));
			writtenElements.addAll(writeSchemas(writer, from, to));
			//			System.out.println("Written:");
			//			for (Element e : writtenElements) System.out.print(" "+buildId(e));
			//			System.out.println("");
			writeProperties(writer, writtenElements);
			//writeStructure(writer);
			writer.writeEndElement(); // annotations
			writer.writeEndElement(); // standOff

			writtenElements.clear();

			writer.writeEndElement(); // TEI

			if (identidywriter != null) identidywriter.close();
			else writer.close();
		}
		catch (Exception e) {
			writer.close();
			Log.warning(NLS.bind(Messages.errorP0, e.getLocalizedMessage())); //$NON-NLS-1$
			e.printStackTrace();
			return false;
		}

		return true;
	}

	protected String buildId(Element e) {
		if (e instanceof Unite) return "u-" + analecVue.getIdElement(e); //$NON-NLS-1$
		else if (e instanceof Schema) return "s-" + analecVue.getIdElement(e); //$NON-NLS-1$
		else if (e instanceof Relation) return "r-" + analecVue.getIdElement(e); //$NON-NLS-1$
		else return "iderror:" + e; //$NON-NLS-1$
	}

	protected HashSet<Element> writeUnits(XMLStreamWriter writer, int from, int to) throws Exception {
		HashSet<Element> written = new HashSet<Element>();

		for (String type : analecCorpus.getStructure().getUnites()) {
			ArrayList<Unite> unites = analecCorpus.getUnites(type);
			Collections.sort(unites, new Comparator<Unite>() {

				@Override
				public int compare(Unite u1, Unite u2) {
					if (u1.getDeb() < u2.getDeb()) return -1;
					if (u1.getDeb() > u2.getDeb()) return 1;
					if (u1.getFin() < u2.getFin()) return -1;
					if (u1.getFin() > u2.getFin()) return 1;
					return 0;
				}
			});
			writer.writeStartElement("annotationGrp"); //$NON-NLS-1$
			writer.writeAttribute("type", "Unit"); //$NON-NLS-1$ //$NON-NLS-2$
			writer.writeAttribute("subtype", type); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			for (Unite unite : unites) {
				if (unite.getDeb() > to) continue; // commence apres le texte
				if (unite.getFin() < from) continue; // termine avant le texte

				String id = buildId(unite);
				writer.writeStartElement("span"); //$NON-NLS-1$
				writer.writeAttribute("id", id); //$NON-NLS-1$
				int[] cpos = { unite.getDeb(), unite.getFin() };
				String[] wordids = CQPSearchEngine.getCqiClient().cpos2Str(wordid.getQualifiedName(), cpos);
				writer.writeAttribute("from", "text:" + wordids[0]); //$NON-NLS-1$ //$NON-NLS-2$
				writer.writeAttribute("to", "text:" + wordids[1]); //$NON-NLS-1$ //$NON-NLS-2$
				writer.writeAttribute("ana", "#" + id + "-fs"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				writer.writeEndElement(); // span
				writer.writeCharacters("\n"); //$NON-NLS-1$
				written.add(unite);
			}
			writer.writeEndElement(); // spanGrp
			writer.writeCharacters("\n"); //$NON-NLS-1$
		}
		return written;
	}

	protected HashSet<Element> writeRelations(XMLStreamWriter writer, int from, int to) throws XMLStreamException {
		HashSet<Element> written = new HashSet<Element>();
		for (String type : analecCorpus.getStructure().getRelations()) {
			ArrayList<Relation> relations = analecCorpus.getRelations(type);
			Collections.sort(relations, new Comparator<Relation>() {

				@Override
				public int compare(Relation r1, Relation r2) {
					Unite u1 = r1.getUnite0();
					Unite u2 = r2.getUnite0();
					if (u1.getDeb() < u2.getDeb()) return -1;
					if (u1.getDeb() > u2.getDeb()) return 1;
					if (u1.getFin() < u2.getFin()) return -1;
					if (u1.getFin() > u2.getFin()) return 1;
					return 0;
				}
			});
			writer.writeStartElement("annotationGrp"); //$NON-NLS-1$
			writer.writeAttribute("type", "Relation"); //$NON-NLS-1$ //$NON-NLS-2$
			writer.writeAttribute("subtype", type); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			for (Relation relation : relations) {

				Element e1 = relation.getElt1();
				Element e2 = relation.getElt2();
				if (e1 instanceof Unite && e2 instanceof Unite) {
					Unite u1 = (Unite) e1;
					Unite u2 = (Unite) e2;
					if (u1.getDeb() > to) continue; // commence apres le texte
					if (u1.getFin() < from) continue; // termine avant le texte
					if (u2.getDeb() > to) continue; // commence apres le texte
					if (u2.getFin() < from) continue; // termine avant le texte
				}
				else {
					continue; // TODO: implement other cases
				}
				String id = buildId(relation);
				writer.writeStartElement("link"); //$NON-NLS-1$
				writer.writeAttribute("id", id); //$NON-NLS-1$
				writer.writeAttribute("target", "#" + buildId(e1) + " #" + buildId(e2)); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				writer.writeAttribute("ana", "#" + id + "-fs"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				writer.writeEndElement(); // join
				writer.writeCharacters("\n"); //$NON-NLS-1$
				written.add(relation);
			}
			writer.writeEndElement(); // joinGrp
			writer.writeCharacters("\n"); //$NON-NLS-1$
		}

		return written;
	}

	protected HashSet<Element> writeSchemas(XMLStreamWriter writer, int from, int to) throws XMLStreamException {
		
		HashSet<Element> writtenSchemas = new HashSet<Element>();
		for (String type : analecCorpus.getStructure().getSchemas()) {
			ArrayList<Schema> schemas = analecCorpus.getSchemas(type);
			Collections.sort(schemas, new Comparator<Schema>() {

				@Override
				public int compare(Schema s1, Schema s2) {
					Unite u1 = s1.getUnite0();
					Unite u2 = s2.getUnite0();

					if (u1 == null && u2 == null) return 0;
					if (u1 == null) return -1;
					if (u2 == null) return 1;

					if (u1.getDeb() < u2.getDeb()) return -1;
					if (u1.getDeb() > u2.getDeb()) return 1;

					// u1.deb == u2.deb
					if (u1.getFin() < u2.getFin()) return -1;
					if (u1.getFin() > u2.getFin()) return 1;

					return 0;
				}
			});
			writer.writeStartElement("annotationGrp"); //$NON-NLS-1$
			writer.writeAttribute("type", "Schema"); //$NON-NLS-1$ //$NON-NLS-2$
			writer.writeAttribute("subtype", type); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			for (Schema schema : schemas) {

				HashSet<String> elements = new HashSet<String>();
				for (Element e : schema.getContenu()) {
					if (e instanceof Unite) {
						Unite unite = (Unite) e;
						if (unite.getDeb() > to) continue; // commence apres le texte
						if (unite.getFin() < from) continue; // termine avant le texte
						elements.add("#" + buildId(unite)); //$NON-NLS-1$
					}
					else {
						continue;
						// TODO: to implement
					}
				}
				if (elements.size() == 0) continue;

				String id = buildId(schema);
				writer.writeStartElement("link"); //$NON-NLS-1$
				writer.writeAttribute("id", id); //$NON-NLS-1$
				writer.writeAttribute("target", StringUtils.join(elements, " ")); //$NON-NLS-1$ //$NON-NLS-2$
				writer.writeAttribute("ana", "#" + id + "-fs"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				writer.writeEndElement(); // span
				writer.writeCharacters("\n"); //$NON-NLS-1$

				writtenSchemas.add(schema);
			}
			writer.writeEndElement(); // join
			writer.writeCharacters("\n"); //$NON-NLS-1$
		}
		return writtenSchemas;
	}

	protected void writeProperties(XMLStreamWriter writer, ArrayList<Element> writtenElements) throws Exception {
		//writer.writeStartElement("fvLib");
		//writer.writeAttribute("type", "AnalecElementProperties");
		//writer.writeCharacters("\n");
		Collections.sort(writtenElements, new Comparator<Element>() {

			@Override
			public int compare(Element o1, Element o2) {
				Unite u1 = o1.getUnite0();
				Unite u2 = o2.getUnite0();
				if (u1.getDeb() < u2.getDeb()) return -1;
				if (u1.getDeb() > u2.getDeb()) return 1;
				if (u1.getFin() < u2.getFin()) return -1;
				if (u1.getFin() > u2.getFin()) return 1;
				return 0;
			}
		});

		String[] fstypes = { "unit-fs", "relation-fs", "schema-fs" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		Class<?>[] classes = { Unite.class, Relation.class, Schema.class };

		for (int i = 0; i < fstypes.length; i++) {
			Class<?> c = classes[i];
			String type = fstypes[i];

			writer.writeStartElement("div"); //$NON-NLS-1$
			writer.writeAttribute("type", type); //$NON-NLS-1$

			for (Element e : writtenElements) {

				if (!(e.getClass().equals(c))) continue;

				writer.writeStartElement("fs"); //$NON-NLS-1$
				writer.writeAttribute("id", buildId(e) + "-fs"); //$NON-NLS-1$ //$NON-NLS-2$
				writer.writeCharacters("\n"); //$NON-NLS-1$
				for (String p : e.getProps().keySet()) {
					writer.writeStartElement("f"); //$NON-NLS-1$
					writer.writeAttribute("name", p); //$NON-NLS-1$
					writer.writeStartElement("string"); //$NON-NLS-1$
					writer.writeCharacters(e.getProps().get(p));
					writer.writeEndElement(); // string
					writer.writeEndElement(); // f
					writer.writeCharacters("\n"); //$NON-NLS-1$
				}
				writer.writeEndElement(); // fs
				writer.writeCharacters("\n"); //$NON-NLS-1$
			}
			writer.writeEndElement(); // div
			writer.writeCharacters("\n"); //$NON-NLS-1$
		}
	}

	protected void writeStructure(XMLStreamWriter writer) throws XMLStreamException {
		Structure structure = analecCorpus.getStructure();
		writer.writeStartElement("fvLib"); //$NON-NLS-1$
		writer.writeAttribute("type", "AnalecStructure"); //$NON-NLS-1$ //$NON-NLS-2$
		writer.writeCharacters("\n"); //$NON-NLS-1$
		writeStructureClass(structure, Unite.class, writer);
		writeStructureClass(structure, Relation.class, writer);
		writeStructureClass(structure, Schema.class, writer);

		writer.writeEndElement(); // fvLib
	}

	protected void writeStructureClass(Structure structure, Class<? extends Element> clazz, XMLStreamWriter writer) throws XMLStreamException {
		writer.writeStartElement("fs"); //$NON-NLS-1$
		writer.writeAttribute("type", clazz.getSimpleName()); //$NON-NLS-1$
		writer.writeCharacters("\n"); //$NON-NLS-1$
		for (String type : structure.getTypes(clazz)) {
			writer.writeStartElement("fs"); //$NON-NLS-1$
			writer.writeAttribute("type", type); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			for (String prop : structure.getNomsProps(clazz, type)) {
				writer.writeStartElement("fs"); //$NON-NLS-1$
				writer.writeAttribute("type", prop); //$NON-NLS-1$
				writer.writeCharacters("\n"); //$NON-NLS-1$
				for (String v : structure.getValeursProp(clazz, type, prop)) {
					writer.writeStartElement("f"); //$NON-NLS-1$
					writer.writeStartElement("string"); //$NON-NLS-1$
					writer.writeCharacters(v);
					writer.writeEndElement(); // string
					writer.writeEndElement(); // f
					writer.writeCharacters("\n"); //$NON-NLS-1$
				}
				writer.writeEndElement(); // fs Unite prop
				writer.writeCharacters("\n"); //$NON-NLS-1$
			}
			writer.writeEndElement(); // fs Unite type
			writer.writeCharacters("\n"); //$NON-NLS-1$
		}
		writer.writeEndElement(); // fs Unite
		writer.writeCharacters("\n"); //$NON-NLS-1$
	}
}
