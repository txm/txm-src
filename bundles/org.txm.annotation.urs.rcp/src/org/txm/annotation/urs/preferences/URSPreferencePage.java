package org.txm.annotation.urs.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.txm.annotation.urs.messages.Messages;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;

public class URSPreferencePage extends TXMPreferencePage implements IWorkbenchPreferencePage {

	private BooleanFieldEditor analec_limit_scheme_field;

	private BooleanFieldEditor prefix_autocompletion_field;

	private IntegerFieldEditor save_timer_field;

	// public static final String SELECTED_UNIT_COLOR = "selected_unit_color";
	// private ColorFieldEditor selected_unit_color;
	// public static final String HIGHLIGHTED_UNIT_COLOR = "hightlighted_unit_color";
	// private ColorFieldEditor hightlighted_unit_color;

	ScopedPreferenceStore preferences;

	public static final String COLOR_PALETTE = "color_palette"; //$NON-NLS-1$

	private RadioGroupFieldEditor color_palette;

	private StringFieldEditor hightlighted_unit_color;

	private StringFieldEditor selected_unit_color;

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {
		analec_limit_scheme_field = new BooleanFieldEditor(
				URSPreferences.ANALEC_LIMIT_CORRECTION_SCHEME, Messages.AnalecPreferencePage_3,
				getFieldEditorParent());
		addField(analec_limit_scheme_field);

		prefix_autocompletion_field = new BooleanFieldEditor(
				URSPreferences.PREFIX_AUTOCOMPLETION, Messages.AnalecPreferencePage_4,
				getFieldEditorParent());
		addField(prefix_autocompletion_field);

		save_timer_field = new IntegerFieldEditor(
				URSPreferences.SAVE_TIMER, Messages.delayBetWeenAutomaticSaves, //$NON-NLS-1$
				getFieldEditorParent());
		addField(save_timer_field);

		String[][] values = {	{ "green", "green" } //$NON-NLS-1$ //$NON-NLS-2$
							,	{ "yellow", "yellow" } //$NON-NLS-1$ //$NON-NLS-2$
							,	{ "custom (set colors below)", "custom" } }; //$NON-NLS-1$ //$NON-NLS-2$
		
		color_palette = new RadioGroupFieldEditor(COLOR_PALETTE, Messages.AnalecPreferencePage_9, 3, values, getFieldEditorParent());
		addField(color_palette);

		hightlighted_unit_color = new StringFieldEditor(URSPreferences.HIGHLIGHTED_UNIT_COLOR, Messages.highlightedUnitsColor, getFieldEditorParent()); //$NON-NLS-1$
		addField(hightlighted_unit_color);

		selected_unit_color = new StringFieldEditor(URSPreferences.SELECTED_UNIT_COLOR, Messages.selectedUnitsColor, getFieldEditorParent()); //$NON-NLS-1$
		addField(selected_unit_color);

		hightlighted_unit_color.setEnabled("custom".equals(URSPreferences.getInstance().getString(URSPreferences.COLOR_PALETTE)), getFieldEditorParent()); //$NON-NLS-1$
		selected_unit_color.setEnabled("custom".equals(URSPreferences.getInstance().getString(URSPreferences.COLOR_PALETTE)), getFieldEditorParent()); //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#propertyChange(org.eclipse.jface.util.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {

		hightlighted_unit_color.setEnabled("custom".equals(color_palette.getSelectionValue()), getFieldEditorParent()); //$NON-NLS-1$
		selected_unit_color.setEnabled("custom".equals(color_palette.getSelectionValue()), getFieldEditorParent()); //$NON-NLS-1$
	}

	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(URSPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setTitle("URS"); //$NON-NLS-1$
	}
	
}
