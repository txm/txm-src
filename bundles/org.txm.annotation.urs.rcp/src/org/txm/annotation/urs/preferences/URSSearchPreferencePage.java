package org.txm.annotation.urs.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.txm.rcp.Application;

/**
 * TODO use TXMPreferencesPage
 * 
 * @author mdecorde
 *
 */
public class URSSearchPreferencePage extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage {

	ScopedPreferenceStore preferences;


	public URSSearchPreferencePage() {
		super();

		preferences = new ScopedPreferenceStore(org.eclipse.core.runtime.preferences.InstanceScope.INSTANCE, Application.PLUGIN_ID);
		setPreferenceStore(preferences);
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

	}

	@Override
	public void init(IWorkbench workbench) {
	}
}
