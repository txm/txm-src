package org.txm.annotation.urs.preferences;

import org.osgi.service.prefs.Preferences;
import org.txm.annotation.urs.toolbar.URSAnnotationToolbar;
import org.txm.core.preferences.TXMPreferences;

public class URSPreferences extends TXMPreferences {

	//	public static final String PREFERENCES_NODE = FrameworkUtil.getBundle(URSPreferences.class).getSymbolicName();

	public static final String VERSION = "urs.version"; //$NON-NLS-1$

	public static final String COLOR_PALETTE = "color_palette"; //$NON-NLS-1$

	public static final String PREFIX_AUTOCOMPLETION = "prefix_autocompletion"; //$NON-NLS-1$

	public static final String ANALEC_LIMIT_CORRECTION_SCHEME = "urs_limit_correction_scheme"; //$NON-NLS-1$

	public static final String SAVE_TIMER = "save_timer_duration"; //$NON-NLS-1$

	public static final String SELECTED_UNIT_COLOR = "selected_unit_color"; //$NON-NLS-1$

	public static final String HIGHLIGHTED_UNIT_COLOR = "highlighted_unit_color"; //$NON-NLS-1$


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.put(VERSION, ""); //$NON-NLS-1$
		preferences.put(COLOR_PALETTE, "green"); //$NON-NLS-1$
		preferences.put(SELECTED_UNIT_COLOR, URSAnnotationToolbar.green.toString());
		preferences.put(HIGHLIGHTED_UNIT_COLOR, URSAnnotationToolbar.lightgreen.toString());
		preferences.putBoolean(ANALEC_LIMIT_CORRECTION_SCHEME, false);
		preferences.putBoolean(PREFIX_AUTOCOMPLETION, false);
		preferences.putInt(SAVE_TIMER, 0);

	}

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static URSPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(URSPreferences.class)) {
			new URSPreferences();
		}
		return (URSPreferences) TXMPreferences.instances.get(URSPreferences.class);
	}
}
