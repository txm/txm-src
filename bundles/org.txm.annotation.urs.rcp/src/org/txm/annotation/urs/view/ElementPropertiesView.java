package org.txm.annotation.urs.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.annotation.urs.toolbar.URSAnnotationToolbar;
import org.txm.annotation.urs.toolbar.UnitToolbar;
import org.txm.rcp.IImageKeys;
import org.txm.utils.logger.Log;

import visuAnalec.Message;
import visuAnalec.Message.TypeMessage;
import visuAnalec.donnees.Corpus;
import visuAnalec.donnees.Corpus.CorpusListener;
import visuAnalec.elements.Element;
import visuAnalec.vue.Vue;

public class ElementPropertiesView extends ViewPart {

	public static final String ID = ElementPropertiesView.class.getName();

	private LinkedHashMap<String, PropertyField> textWidgets = new LinkedHashMap<String, PropertyField>();
	//private String[][] properties;

	Element element;

	URSAnnotationToolbar toolbar;

	private ToolBar buttons;

	private Composite fields;

	private ToolItem firstButton;

	private ToolItem previousButton;

	private ToolItem nextButton;

	private ToolItem lastButton;

	RowData cData = new RowData();

	private Corpus analecCorpus;

	private Vue vue;

	private CorpusListener corpusListener = new CorpusListener() {

		@Override
		public void traiterEvent(final Message e) {
			Display.getDefault().syncExec(new Runnable() {

				@Override
				public void run() {
					//System.out.println("EVENT: "+e);
					if (ElementPropertiesView.this.element == null) return;
					if (fields == null || fields.isDisposed()) return;

					if (e.getType() == TypeMessage.MODIF_STRUCTURE) {
						loadElement(corpus, analecCorpus, element, toolbar);
					}
					else if (e.getType() == TypeMessage.CORPUS_SAVED) {
						for (PropertyField c : textWidgets.values()) {
							if (c.isDisposed()) continue;
							c.setBackground(null);
							c.update();
						}
					}
				}
			});

		}
	};

	private org.txm.searchengine.cqp.corpus.CQPCorpus corpus;

	private ToolItem applyButton;

	private ScrolledComposite sc1;

	private GridLayout glayoutFields;

	//	private KeyListener allControlsKeyListener;

	public static void closeView() {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		ElementPropertiesView view = (ElementPropertiesView) page.findView(ElementPropertiesView.ID);

		// close Element View if opened
		if (view != null) {
			page.hideView(view);
		}

		return;
	}

	public static ElementPropertiesView getView() {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		return (ElementPropertiesView) page.findView(ElementPropertiesView.ID);
	}

	public static ElementPropertiesView openView() {
		// open Element View if not opened
		try {
			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			ElementPropertiesView view = (ElementPropertiesView) page.findView(ElementPropertiesView.ID);
			if (view == null) {
				view = (ElementPropertiesView) page.showView(ElementPropertiesView.ID);

				if (view != null && !view.fields.isDisposed() && view.fields.getParent() != null)
					view.fields.getParent().layout(); // sometimes the part layout is broken
			}
			if (view == null) {
				Log.warning(NLS.bind(Messages.errorElementPropertiesViewNotFoundP0, ElementPropertiesView.ID));
				return null;
			}
			view.getSite().getPage().activate(view);
			return view;
		}
		catch (PartInitException e1) {
			Log.warning(NLS.bind(Messages.partInitialisationErrorP0, e1.getLocalizedMessage()));
			Log.printStackTrace(e1);
		}
		return null;
	}

	@Override
	public void createPartControl(Composite parent) {

		GridLayout layout = new GridLayout();
		parent.setLayout(layout);

		//previous and next buttons
		buttons = new ToolBar(parent, SWT.WRAP);
		GridData buttons_gdata = new GridData(GridData.FILL, GridData.CENTER, false, false);
		buttons_gdata.horizontalSpan = 2;
		buttons.setLayoutData(buttons_gdata);

		firstButton = new ToolItem(buttons, SWT.PUSH);
		firstButton.setImage(IImageKeys.getImage(IImageKeys.CTRLSTART));
		//firstButton.setText("First");
		firstButton.setToolTipText(Messages.selectTheFirstElement);
		firstButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (toolbar != null && !toolbar.isDisposed()) toolbar.selectFirst();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		previousButton = new ToolItem(buttons, SWT.PUSH);
		previousButton.setImage(IImageKeys.getImage(IImageKeys.CTRLREVERSE));
		//previousButton.setText("Previous");
		previousButton.setToolTipText(Messages.selectThePreviousElement);
		previousButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (toolbar != null && !toolbar.isDisposed()) toolbar.selectPrevious();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		nextButton = new ToolItem(buttons, SWT.PUSH);
		nextButton.setImage(IImageKeys.getImage(IImageKeys.CTRLPLAY));
		//nextButton.setText("Next");
		nextButton.setToolTipText(Messages.selectTheNextElement);
		nextButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (toolbar != null && !toolbar.isDisposed()) toolbar.selectNext();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		lastButton = new ToolItem(buttons, SWT.PUSH);
		lastButton.setImage(IImageKeys.getImage(IImageKeys.CTRLEND));
		//lastButton.setText("Last");
		lastButton.setToolTipText(Messages.selectTheLastElement);
		lastButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (toolbar != null && !toolbar.isDisposed()) toolbar.selectLast();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		applyButton = new ToolItem(buttons, SWT.PUSH);
		applyButton.setImage(IImageKeys.getImage(IImageKeys.CHECKED));
		//applyButton.setText("OK");
		applyButton.setToolTipText(Messages.applyAllNewValuesInTheFields);
		applyButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				applyAll();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		sc1 = new ScrolledComposite(parent, SWT.V_SCROLL);
		GridLayout glayout = new GridLayout(1, true);
		glayout.marginBottom = glayout.marginTop = glayout.marginHeight = glayout.verticalSpacing = 0;
		sc1.setLayout(glayout);
		sc1.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		sc1.setExpandHorizontal(true);
		sc1.setExpandVertical(true);
		sc1.setMinSize(0, 120);

		fields = new Composite(sc1, SWT.NONE);
		GridData fields_gdata = new GridData(GridData.FILL, GridData.FILL, true, true);
		fields.setLayoutData(fields_gdata);
		glayoutFields = new GridLayout();
		glayoutFields.numColumns = 1;
		fields.setLayout(glayoutFields);
		sc1.setContent(fields);
		fields.addListener(SWT.Resize, event -> {
			sc1.setMinSize(fields.computeSize(sc1.getParent().getSize().x, SWT.DEFAULT));
		});
		this.setPartName(Messages.element);

		// GridDatas to use when a new field is added
		cData.width = 325;

		parent.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				try {
					if (element != null) { // save previous unit changes

						for (PropertyField c : textWidgets.values()) {
							if (c.isDisposed()) continue;
							saveFieldValue(c);
						}
					}
				}
				catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
	}

	/**
	 * Apply all fields values
	 */
	public void applyAll() {

		for (PropertyField c : textWidgets.values()) {
			if (c.isDisposed()) continue;
			saveFieldValue(c);
		}

	}

	public void unloadElement(Corpus analecCorpus, URSAnnotationToolbar toolbar) {

		if (this.element != null) { // save previous unit changes
			for (PropertyField c : textWidgets.values()) {
				if (c.isDisposed()) continue;
				saveFieldValue(c);
			}
		}

		if (this.analecCorpus != null) { // remove corpus listener from previous corpus
			this.analecCorpus.removeEventListener(corpusListener);
		}

		clear();
		this.setPartName(Messages.element);
		this.setContentDescription(Messages.noAnnotationSelected);
	}

	public synchronized void loadElement(org.txm.searchengine.cqp.corpus.CQPCorpus corpus, Corpus analecCorpus, Element newElement, URSAnnotationToolbar toolbar) {

		if (this.element != null) { // save previous unit changes

			for (PropertyField c : textWidgets.values()) {
				if (c.isDisposed()) continue;
				if (!c.isDisposed()) saveFieldValue(c);
			}
		}

		if (this.analecCorpus != null) { // remove corpus listener from previous corpus
			this.analecCorpus.removeEventListener(corpusListener);
		}

		this.corpus = corpus;
		this.analecCorpus = analecCorpus;
		this.analecCorpus.addEventListener(corpusListener);
		this.vue = URSCorpora.getVue(analecCorpus);

		if (this.element != newElement) {
			//System.out.println("CLEAR FIELDS");
			clear();
		}

		//if (this.element == null || (!this.element.getType().equals(newElement.getType()))) { // no need to rebuild the widgets
		//clear(); // remove all previous widgets
		String[][] props = vue.getChamps(newElement.getClass(), newElement.getType());
		HashMap<String, Integer> niveaux = vue.getNiveauxChampsAVoir(newElement.getClass(), newElement.getType());
		HashMap<String, Integer> ordres = vue.getPositionsChampsAVoir(newElement.getClass(), newElement.getType());
		int ncol = 0;
		int nline = props.length;
		for (int l = 0; l < props.length; l++) {

			if (ncol < props[l].length) ncol = props[l].length;
		}
		for (int l = 0; l < nline; l++) {

			Composite lineComposite = new Composite(fields, SWT.NONE);
			//lineComposite.getLayout().numColumns = props[l].length;
			lineComposite.setLayout(new RowLayout());
			lineComposite.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));

			for (String p : props[l]) { // the first is the prop names

				PropertyField c = null;
				if (textWidgets.containsKey(p)) {
					c = textWidgets.get(p);
				}
				else {
					c = new PropertyField(lineComposite, SWT.NONE, p, this);

					c.setLayoutData(cData);
					c.setLayout(new GridLayout(2, false));
					textWidgets.put(p, c);
				}

				c.setItems(c, newElement);
			}
		}

		//glayoutFields.numColumns = ncol;
		fields.layout();
		fields.getParent().layout();

		//}

		Log.fine("Refreshing unit properties view with: " + newElement); //$NON-NLS-1$
		this.toolbar = toolbar;
		this.element = newElement;
		for (PropertyField t : new ArrayList<PropertyField>(textWidgets.values())) {
			String tname = t.getProperty();
			if (newElement.getProps().get(tname) != null) {
				String newValue = newElement.getProps().get(tname);
				String oldValue = t.getText();
				//System.out.println("new="+newValue+" old="+oldValue+" differs? "+(!newValue.equals(t.getText())));
				if (!newValue.equals(oldValue)) {
					t.setText(newValue);
				}
			}
		}

		//		if (newElement instanceof Unite) {
		//			Unite unit = (Unite)newElement;
		//			if (unit.getDeb() == unit.getFin())
		//				this.setPartName(corpus.getName()+" - "+unit.getType()+" ["+unit.getDeb()+"]");
		//			else
		//				this.setPartName(corpus.getName()+" - "+unit.getType()+" ["+unit.getDeb()+"-"+unit.getFin()+"]");
		//		} else if (newElement instanceof Schema) {
		//			Schema schema = (Schema)newElement;
		//			this.setPartName(corpus.getName()+" - "+schema.getType()+" "+schema.getUnitesSousjacentes().length+" unités");
		//		} else {
		//			this.setPartName("Element");
		//		}
		this.setPartName(UnitToolbar.getUnitWordSurface(corpus.getMainCorpus(), newElement, null));
		this.setContentDescription(""); //$NON-NLS-1$
	}

	protected void saveFieldValue(PropertyField c) {
		if (toolbar == null || toolbar.isDisposed() || c.isDisposed()) return;

		String newVal = c.getCombo().getText().trim();
		String v = ElementPropertiesView.this.vue.getValeurChamp(ElementPropertiesView.this.element, c.getProperty());
		if (!v.equals(newVal) && v != null) {

			ElementPropertiesView.this.vue.setValeurChamp(ElementPropertiesView.this.element, c.getProperty(), newVal);
			ElementPropertiesView.this.analecCorpus.isModifie();
			if (toolbar != null && !toolbar.isDisposed()) this.toolbar.fireIsDirty(); // the Analec corpus should be saved

			if (!c.isDisposed()) {
				c.setTextBackground(new Color(c.getDisplay(), 255, 255, 255));
				c.update();
			}
		}
	}


	public Element getCurrentElement() {
		return element;
	}

	public Corpus getCurrentAnalecCorpus() {
		return analecCorpus;
	}

	public void setFocusOnProperty() {
		for (PropertyField w : textWidgets.values()) {
			w.setFocus();
			return;
		}
	}

	public void clear() {
		Control[] children = fields.getChildren();
		for (int i = 0; i < children.length; i++) {
			children[i].dispose();
		}
		if (textWidgets != null) textWidgets.clear();
		fields.layout();
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

	public Vue getAnalecVue() {

		return vue;
	}
}
