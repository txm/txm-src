package org.txm.annotation.urs.view;

import java.text.Collator;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;

import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.fieldassist.ComboContentAdapter;
import org.eclipse.jface.fieldassist.TXMAutoCompleteField;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.txm.annotation.urs.messages.Messages;

import visuAnalec.elements.Element;

public class PropertyField extends Composite {

	String property;

	ElementPropertiesView view;

	boolean first = true;

	public void reinitialiseModify() {
		first = true;
	}

	public PropertyField(Composite parent, int style, String property, ElementPropertiesView view) {

		super(parent, style);
		this.property = property;
		this.view = view;

		label = new Label(this, SWT.NONE);
		label.setText(property);
		label.setAlignment(SWT.RIGHT);
		label.setToolTipText(NLS.bind(Messages.applyTheNewP0ValueWithTheReturnOrTabKeys, property)); //$NON-NLS-1$
		GridData lData = new GridData(GridData.FILL, GridData.CENTER, false, false);
		lData.minimumWidth = 75;
		lData.widthHint = 75;
		label.setLayoutData(lData);

		t = new Combo(this, SWT.BORDER);
		t.setToolTipText(NLS.bind(Messages.applyTheNewP0ValueWithTheReturnOrTabKeys, property)); //$NON-NLS-1$
		GridData tData = new GridData(GridData.FILL, GridData.FILL, true, false);
		tData.minimumWidth = 150;
		tData.widthHint = 150;
		t.setLayoutData(tData);

		t.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				if (e.widget.isDisposed()) return;
				if (first) {
					first = false;
				}
				if (!t.getText().equals(view.getAnalecVue().getValeurChamp(view.element, getProperty()))) {
					t.setBackground(new Color(t.getDisplay(), 254, 216, 177));
				}
				else {
					t.setBackground(null);
				}
			}
		});
		t.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (!(e.widget instanceof Combo)) return;
				view.saveFieldValue(PropertyField.this);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});
		//	t.addKeyListener(allControlsKeyListener);
		t.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (!(e.widget instanceof Combo)) return;
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					view.saveFieldValue(PropertyField.this);

				}
				else if (e.keyCode == SWT.ESC) {
					//								System.out.println("w:"+e.widget+" key pressed2.");

					t.setText(view.getAnalecVue().getValeurChamp(view.element, getProperty()));
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
		t.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {

				if (PropertyField.this.isDisposed()) return;

				view.saveFieldValue(PropertyField.this);
			}

			@Override
			public void focusGained(FocusEvent e) {
			}
		});
	}

	Label label;

	Combo t;

	public void setItems(PropertyField t, Element newElement) {

		// don't use the view to get the values
		//String[] items = view.getAnalecVue().getValeursChamp(newElement.getClass(), newElement.getType(), t.getProperty());
		HashSet<String> valueshash = view.getCurrentAnalecCorpus().getStructure().getValeursProp(newElement.getClass(), newElement.getType(), t.getProperty());
		String[] items = valueshash.toArray(new String[valueshash.size()]);

		Arrays.sort(items, Collator.getInstance(Locale.getDefault()));
		t.getCombo().setItems(items);
		KeyStroke keys = KeyStroke.getInstance(SWT.CONTROL, SWT.SPACE);
		new TXMAutoCompleteField(t.getCombo(), new ComboContentAdapter(), items, keys);

		t.setEnabled(view.getAnalecVue().isChampModifiable(newElement.getClass(), newElement.getType(), property));
	}

	@Override
	public String getToolTipText() {
		return t.getToolTipText();
	}

	public void setText(String value) {
		t.setText(value);
	}

	@Override
	public boolean setFocus() {
		return t.setFocus();
	}

	@Override
	public void update() {
		super.update();
		label.update();
		t.update();
	}

	@Override
	public void setToolTipText(String title) {
		this.getCombo().setToolTipText(title);
	}

	public Combo getCombo() {
		return t;
	}

	public String getProperty() {

		return property;
	}

	public String getText() {

		return t.getText();
	}

	public void setTextBackground(Color color) {

		t.setBackground(color);
	}

}
