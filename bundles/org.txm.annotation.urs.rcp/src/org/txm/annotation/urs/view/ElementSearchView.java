package org.txm.annotation.urs.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.fieldassist.ComboContentAdapter;
import org.eclipse.jface.fieldassist.TXMAutoCompleteField;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.annotation.urs.toolbar.URSAnnotationToolbar;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.concordance.rcp.handlers.ComputeConcordance;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.searchengine.cqp.ReferencePattern;
import org.txm.searchengine.cqp.corpus.FakeQueryResult;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.logger.Log;

import visuAnalec.Message;
import visuAnalec.Message.TypeMessage;
import visuAnalec.donnees.Corpus;
import visuAnalec.donnees.Corpus.CorpusListener;
import visuAnalec.elements.Element;
import visuAnalec.elements.Unite;
import visuAnalec.vue.Vue;

public class ElementSearchView extends ViewPart {

	public static final String ID = ElementSearchView.class.getName();

	private LinkedHashMap<Combo, String> textWidgets = new LinkedHashMap<>();

	Element currentElement = null;

	int currentIndex = -1;

	ArrayList<Element> matchingElements = new ArrayList<>();

	URSAnnotationToolbar toolbar;

	private Composite buttons;

	private Composite fields;

	private Text text;

	private Label label;

	private Button firstButton;

	private Button previousButton;

	private Button nextButton;

	private Button lastButton;

	private Button resetButton;

	private Button searchButton;

	private Button concordanceButton;

	RowData cData = new RowData();

	private GridData lData;

	private GridData tData;

	private Corpus analecCorpus;

	private Class<? extends Element> clazzSearched;

	private String typeSearched;

	private Vue vue;

	private CorpusListener corpusListener = new CorpusListener() {

		@Override
		public void traiterEvent(final Message e) {
			Display.getDefault().syncExec(new Runnable() {

				@Override
				public void run() {
					// System.out.println("EVENT: "+e);
					if (fields == null || fields.isDisposed()) return;

					if (e.getType() == TypeMessage.MODIF_STRUCTURE) {
						needUpdate = true;
						loadType(corpus, analecCorpus, clazzSearched, typeSearched, toolbar);
					}
				}
			});
		}
	};

	private boolean needUpdate = false;

	private SelectionListener selectionChangedListener;

	private org.txm.searchengine.cqp.corpus.CQPCorpus corpus;

	private ConcordanceEditor concordanceEditor;

	private IEditorPart fromEditor;

	private Concordance concordance;

	public static void closeView() {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		ElementSearchView view = (ElementSearchView) page.findView(ElementSearchView.ID);

		// close Element View if opened
		if (view != null) {
			page.hideView(view);
		}

		return;
	}

	public static ElementSearchView openView() {
		// open Element View if not opened
		try {
			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			ElementSearchView view = (ElementSearchView) page.findView(ElementSearchView.ID);
			if (view == null) {
				view = (ElementSearchView) page.showView(ElementSearchView.ID);
				if (view != null && !view.fields.isDisposed() && view.fields.getParent() != null)
					view.fields.getParent().layout(); // sometimes the part layout is broken
			}
			if (view == null) {
				Log.warning(NLS.bind(Messages.errorElementPropertiesViewNotFoundP0, ElementSearchView.ID)); //$NON-NLS-1$
				return null;
			}
			view.getSite().getPage().activate(view);
			return view;
		}
		catch (PartInitException e1) {
			Log.warning(NLS.bind(Messages.partInitialisationErrorP0, e1.getLocalizedMessage())); //$NON-NLS-1$
			Log.printStackTrace(e1);
		}
		return null;
	}

	@Override
	public void createPartControl(Composite parent) {

		GridLayout layout = new GridLayout();
		parent.setLayout(layout);

		// previous and next buttons
		buttons = new Composite(parent, SWT.NONE);
		buttons.setLayout(new GridLayout(10, false));
		GridData buttons_gdata = new GridData(GridData.FILL, GridData.CENTER, true, false);
		buttons_gdata.horizontalSpan = 2;
		buttons.setLayoutData(buttons_gdata);

		searchButton = new Button(buttons, SWT.PUSH);
		searchButton.setImage(IImageKeys.getImage(IImageKeys.START));
		searchButton.setToolTipText(Messages.searchElementsUsingTheSearchFieldsValues); //$NON-NLS-1$
		searchButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				search();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		resetButton = new Button(buttons, SWT.PUSH);
		resetButton.setImage(IImageKeys.getImage("platform:/plugin/org.txm.rcp/icons/cross.png")); //$NON-NLS-1$
		resetButton.setToolTipText(Messages.resetTheSearchFieldValues); //$NON-NLS-1$
		resetButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				for (Combo c : textWidgets.keySet())
					c.setText(""); //$NON-NLS-1$

				ElementSearchView.this.concordanceButton.setEnabled(false);
				ElementSearchView.this.firstButton.setEnabled(false);
				ElementSearchView.this.previousButton.setEnabled(false);
				ElementSearchView.this.nextButton.setEnabled(false);
				ElementSearchView.this.lastButton.setEnabled(false);
				ElementSearchView.this.text.setEnabled(false);
				ElementSearchView.this.resetButton.setEnabled(false);
				currentElement = null;
				currentIndex = -1;
				updateCurrentLabel();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		label = new Label(buttons, SWT.NONE);
		label.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		label.setText(""); //$NON-NLS-1$

		firstButton = new Button(buttons, SWT.PUSH);
		firstButton.setImage(IImageKeys.getImage(IImageKeys.CTRLSTART));
		firstButton.setToolTipText(Messages.selectTheFirstMatch); //$NON-NLS-1$
		firstButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				text.setText("" + 1); //$NON-NLS-1$
				selectionChangedListener.widgetSelected(null);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		previousButton = new Button(buttons, SWT.PUSH);
		previousButton.setImage(IImageKeys.getImage(IImageKeys.CTRLREVERSE));
		previousButton.setToolTipText(Messages.selectThePreviousMatch); //$NON-NLS-1$
		previousButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				splitViewIfNecessary();
				int current = Integer.parseInt(text.getText());
				current--;
				if (current < 1) current = 1;
				text.setText("" + current); //$NON-NLS-1$
				selectionChangedListener.widgetSelected(null);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		text = new Text(buttons, SWT.BORDER);
		text.setText(""); //$NON-NLS-1$
		text.setToolTipText(Messages.setAMatchNumberAndSelectItWithTheEnterKey); //$NON-NLS-1$
		selectionChangedListener = new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (toolbar == null || toolbar.isDisposed()) return;
				// System.out.println("SELECTED!!");
				try {
					currentIndex = Integer.parseInt(text.getText()) - 1;
					currentElement = matchingElements.get(currentIndex);
					toolbar.select(currentElement);
					updateCurrentLabel();
				}
				catch (Exception err) {
					Log.warning(NLS.bind(Messages.couldNotSetCurrentMatchP0, err.getLocalizedMessage())); //$NON-NLS-1$
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};
		text.addSelectionListener(selectionChangedListener);
		text.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (currentElement == null) return;

				if (e.keyCode == SWT.HOME) {
					text.setText("" + 1); //$NON-NLS-1$
					selectionChangedListener.widgetSelected(null);
				}
				else if (e.keyCode == SWT.END) {
					text.setText("" + matchingElements.size()); //$NON-NLS-1$
					selectionChangedListener.widgetSelected(null);
				}
				else if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					selectionChangedListener.widgetSelected(null);
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		GridData gdata = new GridData(GridData.FILL, GridData.FILL, false, false);
		gdata.minimumWidth = 100;
		gdata.widthHint = 100;
		text.setLayoutData(gdata);
		label = new Label(buttons, SWT.NONE);
		label.setText(" / -"); //$NON-NLS-1$

		nextButton = new Button(buttons, SWT.PUSH);
		nextButton.setImage(IImageKeys.getImage(IImageKeys.CTRLPLAY));
		nextButton.setToolTipText(Messages.selectTheNextMatch); //$NON-NLS-1$
		nextButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int current = Integer.parseInt(text.getText());
				current++;
				if (current > matchingElements.size()) current = matchingElements.size();
				text.setText("" + current); //$NON-NLS-1$
				selectionChangedListener.widgetSelected(null);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		lastButton = new Button(buttons, SWT.PUSH);
		lastButton.setImage(IImageKeys.getImage(IImageKeys.CTRLEND));
		lastButton.setToolTipText(Messages.selectTheLastMatch); //$NON-NLS-1$
		lastButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				text.setText("" + matchingElements.size()); //$NON-NLS-1$
				selectionChangedListener.widgetSelected(null);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		concordanceButton = new Button(buttons, SWT.PUSH);
		concordanceButton.setImage(IImageKeys.getImage(ConcordanceEditor.class, IImageKeys.ACTION_CONCORDANCES));
		concordanceButton.setToolTipText(Messages.viewTheMatchesInConcordances); //$NON-NLS-1$
		concordanceButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					toConcordances(e);
				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		this.searchButton.setEnabled(false);
		this.concordanceButton.setEnabled(false);
		this.firstButton.setEnabled(false);
		this.previousButton.setEnabled(false);
		this.nextButton.setEnabled(false);
		this.lastButton.setEnabled(false);
		this.text.setEnabled(false);
		this.resetButton.setEnabled(false);

		fields = new Composite(parent, SWT.NONE);
		GridData fields_gdata = new GridData(GridData.FILL, GridData.FILL, true, true);
		fields.setLayoutData(fields_gdata);
		fields.setLayout(new GridLayout());
		this.setPartName(Messages.element); //$NON-NLS-1$

		// GridDatas to use when a new field is added
		cData.width = 325;
		lData = new GridData(GridData.FILL, GridData.CENTER, false, false);
		lData.minimumWidth = 75;
		lData.widthHint = 75;
		tData = new GridData(GridData.FILL, GridData.FILL, true, false);
		tData.minimumWidth = 150;
		tData.widthHint = 150;

		updateCurrentLabel();
	}

	protected void splitViewIfNecessary() {

		//		for (IViewReference ref : ElementSearchView.this.getSite().getPage().getViewReferences()) {
		//			System.out.println("ref="+ref+" id="+ref.getId());
		//			if (ref.getId().equals(ElementPropertiesView.class.getName())) {
		//				IWorkbenchPage p = ref.getView(true).getSite().getPage();
		//				MPart mp1 = ref.getView(true).getSite().getService(MPart.class);
		//				MPart mp2 = this.getSite().getService(MPart.class);
		//				SWTEditorsUtils.addPart(mp1, mp2, EModelService.RIGHT_OF, 0.5f);
		//				return;
		//			}
		//		}
	}

	protected void updateCurrentLabel() {
		if (currentIndex >= 0) {
			this.setPartName("" + (currentIndex + 1) + " / " + matchingElements.size() + " " + typeSearched); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			text.setText("" + (currentIndex + 1)); //$NON-NLS-1$
			label.setText(" / " + matchingElements.size()); //$NON-NLS-1$
			text.setEnabled(true);
		}
		else {
			if (typeSearched != null) this.setPartName(typeSearched);
			text.setText("1"); //$NON-NLS-1$
			text.setEnabled(false);
			label.setText(" / -"); //$NON-NLS-1$
		}
		label.getParent().layout();
	}

	/**
	 * 
	 * @param corpus the CQP corpus
	 * @param analecCorpus the analec corpus
	 * @param clazz the search Analec Element class (Unite, Schema, Relation)
	 * @param type the searched Element type
	 * @param toolbar the annotation toolbar which called loadType
	 */
	public void loadType(org.txm.searchengine.cqp.corpus.CQPCorpus corpus, Corpus analecCorpus, Class<? extends Element> clazz, String type, URSAnnotationToolbar toolbar) {
		if (toolbar == null || toolbar.isDisposed()) return;

		this.searchButton.setEnabled(true);

		if (this.analecCorpus != null) { // remove corpus listener from previous corpus
			this.analecCorpus.removeEventListener(corpusListener);
		}

		this.fromEditor = toolbar.getEditor();
		if (this.corpus != corpus && concordanceEditor != null) {
			concordanceEditor = null;
		}
		this.corpus = corpus;
		this.analecCorpus = analecCorpus;
		this.analecCorpus.addEventListener(corpusListener);
		this.vue = URSCorpora.getVue(analecCorpus);

		if (this.needUpdate || type.equals(type)) { // no need to rebuild the widgets
			needUpdate = false;
			clear(); // remove all previous widgets

			String[][] props = vue.getChamps(clazz, type);

			HashMap<String, Integer> niveaux = vue.getNiveauxChampsAVoir(clazz, type);
			HashMap<String, Integer> ordres = vue.getPositionsChampsAVoir(clazz, type);
			int ncol = 0;
			int nline = props.length;
			for (int l = 0; l < props.length; l++) {
				if (ncol < props[l].length) ncol = props[l].length;
			}


			//TODO use the mix of rowlayout and gridlayout of the ElementPropertiesView
			for (int l = 0; l < nline; l++) {

				for (String p : props[l]) { // the first is the prop names

					Composite c = new Composite(fields, SWT.NONE);
					c.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
					c.setLayout(new GridLayout(2, false));

					Label l2 = new Label(c, SWT.None);
					l2.setText(p);
					l2.setLayoutData(lData);

					Combo t = new Combo(c, SWT.BORDER);
					t.setLayoutData(tData);
					textWidgets.put(t, p);
					setItems(t, clazz, type, p);
					t.addKeyListener(new KeyListener() {

						@Override
						public void keyReleased(KeyEvent e) {
							if (!(e.widget instanceof Combo)) return;
							if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
								search();
							}
						}

						@Override
						public void keyPressed(KeyEvent e) {
						}
					});
				}

				int missing = ncol - props[l].length;
				while (missing > 0) {
					Composite lineComposite = new Composite(fields, SWT.NONE);
					lineComposite.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
					missing--;
				}
			}
			((GridLayout) fields.getLayout()).numColumns = ncol;

			fields.layout();
			fields.getParent().layout();
		}

		this.toolbar = toolbar;
		this.clazzSearched = clazz;
		this.typeSearched = type;

		this.setPartName(corpus.getName() + " - " + typeSearched); //$NON-NLS-1$
	}

	public static final String letters = "abcefghijklmnopqrstu";// vwxyz0123456789"; //$NON-NLS-1$

	public static final int MAXCONCORDANCEQUERYSIZE = 1150; // 1200 in fact
	// public static String[] doubleletters = new String[letters.length()+ (letters.length()*letters.length())];
	// static {
	// for (int i = 0 ; i < letters.length() ; i++) doubleletters[i] = ""+letters.charAt(i);
	// for (int i = 0 ; i < letters.length() ; i++)
	// for (int j = 0 ; j < letters.length() ; j++) doubleletters[letters.length()+ i + (j*letters.length())] = ""+letters.charAt(i)+letters.charAt(j);
	// System.out.println(Arrays.toString(doubleletters));
	// }
	//
	// /**
	// * just a simple way to create a label using the match length
	// * @param size
	// * @return a uniq String if size is < to doubleletters.length()
	// */
	// public static String getLetter(int size) {
	// if (size >= doubleletters.length) return "zzz";
	// else return doubleletters[size];
	// }

	public void toConcordances(SelectionEvent e) throws Exception {
		if (matchingElements.size() == 0) return;

		LinkedHashSet<Unite> unites = new LinkedHashSet<>();
		for (Element m : matchingElements) {
			for (Unite unite : m.getUnitesSousjacentes()) {
				unites.add(unite);
			}
		}

		List<Unite> unites_list = new ArrayList<>();
		unites_list.addAll(unites);
		Collections.sort(unites_list);

		int[] starts = new int[unites.size()];
		int[] ends = new int[unites.size()];
		int[] targets = null;

		int i = 0;
		for (Unite u : unites_list) {
			starts[i] = u.getDeb();
			ends[i] = u.getFin();
			i++;
		}

		FakeQueryResult fqr = new FakeQueryResult("SearchViewLink", toolbar.getCQPCorpus(), new CQLQuery(""), starts, ends, targets); //$NON-NLS-1$ //$NON-NLS-2$

		HashSet<Integer> sizes = new HashSet<>();
		for (Unite unite : unites) {
			int size = unite.getFin() - unite.getDeb() + 1;
			if (size > letters.length()) size = letters.length() - 1;
			sizes.add(size);
		}


		concordance = null;

		if (concordanceEditor == null || concordanceEditor.isDisposed()) {
			concordance = new Concordance(corpus);
			List<WordProperty> viewProperties = Arrays.asList(corpus.getWordProperty());
			ReferencePattern ref = new ReferencePattern(corpus.getStructuralUnit("text").getProperty("id")); //$NON-NLS-1$ //$NON-NLS-2$
			concordance.setParameters(new CQLQuery(""), //$NON-NLS-1$
					viewProperties, viewProperties, viewProperties,
					viewProperties, viewProperties, viewProperties,
					ref, ref,
					7, 10);
		}
		else {
			concordance = concordanceEditor.getResult();
		}
		concordance.setQueryResult(fqr);

		e.widget.getDisplay().syncExec(new Runnable() {

			@Override
			public void run() {
				if (concordanceEditor == null || // no editor opened
						concordanceEditor.getTableViewer().getTable().isDisposed() // editor was closed
						|| !concordanceEditor.getCorpus().equals(corpus)) { // editor opened but with different corpus

					IEditorPart editor = ComputeConcordance.openEditor(concordance);
					if (editor == null) {
						return;
					}
					concordanceEditor = (ConcordanceEditor) editor;

					if (toolbar.getEditor() != null) {
						int position = -2;
						if (position == -2) {
							Point s = toolbar.getEditor().getParent().getSize();
							if (s.x < s.y) {
								position = EModelService.ABOVE;
							}
							else {
								position = EModelService.RIGHT_OF;
							}
						}
						SWTEditorsUtils.moveEditor(toolbar.getEditor(), concordanceEditor, EModelService.BELOW);
					}
					// BackToText.associateEditors(toolbar.getEditor(), concordanceEditor);
					toolbar.getEditor().addLinkedEditor(concordanceEditor);
					concordanceEditor.addLinkedEditor(toolbar.getEditor());
				}

				concordanceEditor.compute(false);
				IWorkbenchPage attachedPage = concordanceEditor.getEditorSite().getPage();
				attachedPage.activate(concordanceEditor);
			}
		});
	}

	/**
	 * start a new search
	 */
	public void search() {
		if (toolbar == null || toolbar.isDisposed()) return;

		ArrayList<? extends Element> elements = analecCorpus.getElements(clazzSearched, typeSearched);
		matchingElements.clear();
		for (Element e : elements) {
			boolean insert = true;
			for (Combo w : textWidgets.keySet()) {
				if (w.getText().trim().length() > 0
						&& !e.getProp(textWidgets.get(w)).matches(w.getText())) {
					insert = false;
					continue;
				}
			}
			if (insert) matchingElements.add(e);
		}

		if (matchingElements.size() > 0) {

			// sort elements
			Collections.sort(matchingElements, new Comparator<Element>() {

				@Override
				public int compare(Element o1, Element o2) {
					if (o1 == null) return 1;
					else if (o2 == null) return -1;
					else if (o1.getUnite0().getDeb() < o2.getUnite0().getDeb()) {
						return -1;
					}
					else if (o1.getUnite0().getDeb() > o2.getUnite0().getDeb()) {
						return 1;
					}
					else if (o1.getUnite0().getFin() < o2.getUnite0().getFin()) {
						return -1;
					}
					else if (o1.getUnite0().getFin() > o2.getUnite0().getFin()) {
						return 1;
					}
					return 0;
				}
			});

			currentElement = matchingElements.get(0);
			currentIndex = 0;
			toolbar.select(currentElement);
			updateCurrentLabel();
			this.searchButton.setEnabled(true);
			this.concordanceButton.setEnabled(true);
			this.firstButton.setEnabled(true);
			this.previousButton.setEnabled(true);
			this.nextButton.setEnabled(true);
			this.lastButton.setEnabled(true);
			this.text.setEnabled(true);
			this.resetButton.setEnabled(true);
		}
		else {
			this.concordanceButton.setEnabled(false);
			this.firstButton.setEnabled(false);
			this.previousButton.setEnabled(false);
			this.nextButton.setEnabled(false);
			this.lastButton.setEnabled(false);
			this.text.setEnabled(false);
			this.resetButton.setEnabled(false);
			currentElement = null;
			currentIndex = -1;
			updateCurrentLabel();
		}
	}

	private void setItems(Combo t, Class<? extends Element> clazz, String type, String prop) {
		String[] items = vue.getValeursChamp(clazz, type, prop);
		t.setItems(items);
		KeyStroke keys = KeyStroke.getInstance(SWT.CONTROL, SWT.SPACE);
		new TXMAutoCompleteField(t, new ComboContentAdapter(), items, keys);
	}

	public Corpus getCurrentAnalecCorpus() {
		return analecCorpus;
	}

	@Override
	public void setFocus() {
		for (Combo w : textWidgets.keySet()) {
			w.setFocus();
			return;
		}
	}

	public void clear() {
		Control[] children = fields.getChildren();
		for (int i = 0; i < children.length; i++) {
			children[i].dispose();
		}
		textWidgets.clear();
		fields.layout();
	}
}
