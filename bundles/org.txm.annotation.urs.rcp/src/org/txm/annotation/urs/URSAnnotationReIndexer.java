package org.txm.annotation.urs;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.txm.annotation.urs.messages.Messages;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.logger.Log;
import org.txm.utils.messages.Utf8NLS;

import visuAnalec.donnees.Corpus;
import visuAnalec.elements.Unite;

/**
 * Re index Analec corpus words and annotations with TXM corpus words
 * 
 * @author mdecorde
 *
 */
public class URSAnnotationReIndexer {

	MainCorpus corpus;

	Corpus analecCorpus;

	File aafile;

	public boolean debug = false;

	public URSAnnotationReIndexer(MainCorpus corpus, Corpus analecCorpus) {
		this.corpus = corpus;
		this.analecCorpus = analecCorpus;
	}

	public boolean process() throws CqiClientException, IOException, CqiServerError {

		int corpusSize = corpus.getSize();
		if (corpusSize == 0) {
			Log.warning(Messages.ErrorTheCQPCorpusIsEmptyKa);
			return false;
		}
		String text = analecCorpus.getTexte();
		int isearch = 0; // the current search start position

		AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();
		Property word = corpus.getProperty("word"); //$NON-NLS-1$

		int positions[] = new int[corpusSize];
		for (int i = 0; i < corpusSize; i++) {
			positions[i] = i;
		}

		int positionsCorrespondances[] = new int[corpusSize];
		String strs[] = CQI.cpos2Str(word.getQualifiedName(), positions);

		for (int i = 0; i < corpusSize; i++) {
			int idx = text.indexOf(strs[i], isearch);
			if (idx < 0) {
				Log.finer("Error: cannot find word='" + strs[i] + "' (word with position in CQP corpus=" + positions[i] + ") in text with current carret=" + isearch + ". Aborting."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

				Log.finer("Current text slice is (-20, +20 characters): "); //$NON-NLS-1$
				Log.finer("* before: " + text.substring(Math.max(0, isearch - 20), Math.min(isearch + 20, isearch))); //$NON-NLS-1$
				Log.finer("* after: " + text.substring(isearch, Math.min(isearch + 20, text.length() - 1))); //$NON-NLS-1$

				return false;
			}
			positionsCorrespondances[i] = idx;
			isearch = idx + strs[i].length();
		}

		if (debug) Log.finer(Arrays.toString(positions));
		if (debug) Log.finer(Arrays.toString(positionsCorrespondances));

		isearch = 0;
		ArrayList<Unite> unites = analecCorpus.getToutesUnites();
		if (debug) Log.finer("units not-sorted: "); //$NON-NLS-1$
		if (debug) printUnits(unites);
		Collections.sort(unites, new Comparator<Unite>() {

			@Override
			public int compare(Unite o1, Unite o2) {
				int d = o1.getDeb() - o2.getDeb();
				if (d == 0) return o1.getFin() - o2.getFin();
				else return d;
			}
		});
		if (debug) Log.finer("units sorted: "); //$NON-NLS-1$
		if (debug) printUnits(unites);

		int iunite = 0;
		for (Unite unite : unites) {
			if (debug) Log.finer("Updating "); //$NON-NLS-1$
			if (debug) printUnite(unite);
			int start = unite.getDeb();
			int end = unite.getFin();

			boolean startFound = false;
			boolean endFound = false;
			int i = 0; // or not : unites are sorted by start position, we don't need to browse all words \o/
			for (; i < positionsCorrespondances.length; i++) {
				if (startFound && endFound) break; // no need to go further
				if (debug) Log.finer("i=" + i + " positionsCorrespondances[i]=" + positionsCorrespondances[i]); //$NON-NLS-1$ //$NON-NLS-2$
				if (!startFound && start < positionsCorrespondances[i]) {
					unite.setDeb(i - 1);
					startFound = true;
				}
				if (!endFound && end <= positionsCorrespondances[i]) {
					unite.setFin(i - 1);
					endFound = true;
				}
			}

			if (!endFound && i == positionsCorrespondances.length) {
				unite.setFin(i - 1);
				endFound = true;
			}
			if (!startFound && i == positionsCorrespondances.length) {
				unite.setDeb(i - 1);
				startFound = true;
			}

			if (!(startFound && endFound)) {
				String s = Utf8NLS.bind(Messages.ErrorCannotFindWordsPositionsforUnitOfTypeP0AndP1P2, unite.getType(), start, end); //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				Log.finer(s);

				Log.finer("5 last found units are: "); //$NON-NLS-1$
				for (int j = 4; j >= 0; j--) {
					if (iunite - j >= 0) {
						printUnite(unites.get(iunite - j));
					}
				}
				return false;
			}
			// if (i > 0) i--; // restart at previous word
			iunite++;
		}
		if (debug) Log.finer("units updated: "); //$NON-NLS-1$
		if (debug) printUnits(unites);
		return true;
	}

	public static void printUnite(Unite unite) {
		System.out.print(unite.getType() + "[" + unite.getDeb() + ", " + unite.getFin() + "]"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	public static void printUnits(List<Unite> units) {
		int i = 0;
		for (Unite u : units) {
			if (i > 0) System.out.print(", "); //$NON-NLS-1$
			printUnite(u);
			i++;
		}
		System.out.println();
	}
}
