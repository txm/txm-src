package org.txm.annotation.urs.messages;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.messages.Utf8NLS;

public class Messages extends TXMCoreMessages {

	private static final String BUNDLE_NAME = "org.txm.annotation.urs.messages.messages"; //$NON-NLS-1$

	public static String P0ImportReport;

	public static String OK;
	
	public static String P0ParagraphsFound;

	public static String URSCorpora_3;

	public static String URSCorpora_4;

	public static String URSInformationsHTML;

	public static String DoInstallStep_11;

	public static String DoInstallStep_17;

	public static String DoInstallStep_18;

	public static String DoInstallStep_2;

	public static String DoInstallStep_5;

	public static String DoInstallStep_6;

	public static String DoInstallStep_9;

	public static String EditStructure_0;

	public static String ErrorCannotFindWordsPositionsforUnitOfTypeP0AndP1P2;

	public static String ErrorTheCQPCorpusIsEmptyKa;

	public static String ExportGlozzCorpus_0;

	public static String ExportGlozzCorpus_1;

	public static String ExportGlozzCorpus_2;

	public static String ExportGlozzCorpus_3;

	public static String ExportTEICorpus_0;

	public static String ExportTEICorpus_1P0;

	public static String ExportTEICorpus_2;

	public static String ExportTEICorpus_3;

	public static String ImportGlozzAnnotations_0;

	public static String ImportGlozzAnnotations_1;

	public static String ImportGlozzAnnotations_10;

	public static String ImportGlozzAnnotations_2;

	public static String ImportGlozzAnnotations_3;

	public static String ImportGlozzAnnotations_4;

	public static String ImportGlozzAnnotations_6;

	public static String ImportGlozzAnnotations_7;

	public static String ImportGlozzAnnotations_8;

	public static String ImportGlozzAnnotations_9;

	public static String ImportGlozzCorpus_0;

	public static String ImportGlozzCorpus_1;

	public static String ImportGlozzCorpus_10;

	public static String ImportGlozzCorpus_11;

	public static String ImportGlozzCorpus_15;

	public static String ImportGlozzCorpus_17;

	public static String ImportGlozzCorpus_19;

	public static String ImportGlozzCorpus_22;

	public static String ImportGlozzCorpus_23;

	public static String ImportGlozzCorpus_24;

	public static String ImportGlozzCorpus_25;

	public static String ImportGlozzCorpus_26;

	public static String ImportGlozzCorpus_27;

	public static String ImportGlozzCorpus_28;

	public static String ImportGlozzCorpus_29;

	public static String ImportGlozzCorpus_3;

	public static String ImportGlozzCorpus_30;

	public static String ImportGlozzCorpus_32;

	public static String ImportGlozzCorpus_4;

	public static String ImportGlozzCorpus_5;

	public static String ImportGlozzCorpus_8;

	public static String ImportGlozzCorpus_9;

	public static String ImportTEIAnnotations_0;

	public static String ImportTEIAnnotations_1;

	public static String ImportTEIAnnotations_2;

	public static String ImportTEIAnnotations_3;

	public static String ImportTEIAnnotations_5;

	public static String ImportTEICorpus_0;

	public static String ImportTEICorpus_11;

	public static String ImportTEICorpus_14;

	public static String ImportTEICorpus_15;

	public static String ImportTEICorpus_16;

	public static String ImportTEICorpus_18;

	public static String ImportTEICorpus_19;

	public static String ImportTEICorpus_2;

	public static String ImportTEICorpus_20;

	public static String ImportTEICorpus_21;

	public static String ImportTEICorpus_22;

	public static String ImportTEICorpus_23;

	public static String ImportTEICorpus_24;

	public static String ImportTEICorpus_3;

	public static String ImportTEICorpus_5;

	public static String ImportTEICorpus_7;

	public static String ImportTEICorpus_9;

	public static String LoadCorpus_0;

	public static String LoadCorpus_1;

	public static String LoadCorpus_2;

	public static String LoadCorpus_3;

	public static String LoadCorpus_4;

	public static String LoadCorpus_5;

	public static String LoadStructure_0;

	public static String LoadStructure_1;

	public static String LoadStructure_2;

	public static String LoadStructure_3;

	public static String LoadStructureFromGlozz_0;

	public static String LoadStructureFromGlozz_1;

	public static String LoadStructureFromGlozz_2;

	public static String LoadStructureFromGlozz_3;

	public static String LoadStructureFromGlozz_4;

	public static String LoadVue_0;

	public static String LoadVue_1;

	public static String LoadVue_2;

	public static String LoadVue_3;

	public static String SaveCorpus_0;

	public static String SaveCorpus_3;

	public static String SaveCorpus_5;

	public static String SaveStructure_0;

	public static String SaveStructure_3;

	public static String SaveStructure_5;

	public static String SaveStructureAsGlozzModel_0;

	public static String SaveStructureAsGlozzModel_1;

	public static String SaveStructureAsGlozzModel_2;

	public static String SaveStructureAsGlozzModel_3;

	public static String SaveVue_0;

	public static String SaveVue_3;

	public static String SaveVue_5;

	public static String SchemasHTML;

	public static String AnalecPreferencePage_3;

	public static String AnalecPreferencePage_4;

	public static String AnalecPreferencePage_9;

	public static String Annotation;

	public static String AnnotationToolbar_0;

	public static String AnnotationToolbar_6;

	public static String AnnotationToolbar_7;

	public static String UnitsHTML;

	public static String UnitToolbar_10;

	public static String UnitToolbar_12;

	public static String UnitToolbar_14;

	public static String UnitToolbar_16;

	public static String UnitToolbar_27;

	public static String UnitToolbar_28;

	public static String UnitToolbar_29;

	public static String UnitToolbar_3;

	public static String UnitToolbar_32;

	public static String UnitToolbar_44;

	public static String UnitToolbar_47;

	public static String UnitToolbar_48;

	public static String UnitToolbar_5;

	public static String UnitToolbar_54;

	public static String UnitToolbar_59;

	public static String UnitToolbar_6;

	public static String UnitToolbar_7;

	public static String UnitToolbar_8;

	public static String StartButton_0;

	public static String StartButton_1;

	public static String StartButton_11;

	public static String StartButton_12;

	public static String StartButton_4;

	public static String StartButton_5;

	public static String StructureHTML;

	public static String NavigationField_0;

	public static String NavigationField_2;

	public static String NavigationField_4;

	public static String NavigationField_5;

	public static String RelationsHTML;

	public static String WarningFailToCreateCorpusVueFileP0;

	public static String WarningFailToOpenCorpusVueFileP0;

	public static String noURStructure;

	public static String selectionIsNotACorpusP0;

	public static String noExportDirectorySetAborting;

	public static String ursAnnotationsImportedFromP0P1AndP2ByP3;

	public static String importAborted;

	public static String theCorpusWasNotCreatedAborting;

	public static String exportFailed;

	public static String ursAnnotationImportedFromP0InP1ByP2;

	public static String democratMacrosShouldBeIntheP0subdirectory;

	public static String annotationsRestored;

	public static String noAnnotationtoRestore;

	public static String errorNoAnalecCorpusAssociatedToTheCQPCorpusP0;

	public static String noChangeToSaveInP0;

	public static String ursAnnotationsSavedByP0;

	public static String writingAnnotationStructure;

	public static String errorWhileWritingP0;

	public static String annotationStructureWrittenInP0;

	public static String writingCorpus;

	public static String corpusWrittenInP0;

	public static String writingAnnotations;

	public static String P0UnitsFound;

	public static String annotationsWrittenToP0;

	public static String errorCouldNotFindTheXMLTXMFileP0;

	public static String errorCouldNotCopytheXMLTXMFileP0;

	public static String errorP0;

	public static String text3Dot;

	public static String writingAnnotations3Dot;

	public static String writingStructure;

	public static String creatingArchive3Dot;

	public static String DoneP0;

	public static String processingTextP0;

	public static String noXMLFileFoundInP0;

	public static String warningDuplicatedfsElementWithIDP0;

	public static String warningDuplicatedfElementNameP0InP1fsElement;

	public static String errorCannotFindTheStandOffElementInP0;

	public static String errorRelationElementNotFoundP0;

	public static String warningNoPositionFoundForWordIdP0;

	public static String warningDuplicatedUniteIdP0;

	public static String warningNoPropertiesFoundForElementIdP0andAnaP1;

	public static String warningNoPropertiesFoundforElementIdP0;

	public static String warningTooManyPositionsFoundForWordIdP0;

	public static String errorWhileCreatingSchemaWithIdP0;

	public static String warningDuplicatedElementPropertiesP0;

	public static String warningDuplicatedRelationIdP0;

	public static String warningDuplicatedSchemaIdP0;

	public static String warningFoundP0WithoutTypeAndNAttribute;

	public static String warningMissingUnitIdP0;

	public static String warningNoPropertiesFoundForElementIdP0;

	public static String finalImportReport;

	public static String nUnitsAddedP0;

	public static String errorAnnotationsDirectoryIsNotADirectoryP0;

	public static String errorWhileImportingViewP0;

	public static String nRelationsAddedP0;

	public static String nRelationsErrorP0;

	public static String nSchemasAddedP0;

	public static String nSchemasErrorP0;

	public static String nUnitsErrorP0;

	public static String nUnitsNoMatchErrorP0;

	public static String nUnitsTooMuchMatchErrorP0;

	public static String errorAnnotationsDirectoryDoesNotExistP0;

	public static String errorWhileImportingGlozzModelP0;

	public static String processingP0Text3Dot;

	public static String warningAamFileDoesNotExistP0;

	public static String warningCannotFoundTextWithIDP0InCurrentCQPCorpus;

	public static String warningevFileDoesNotExistP0;

	public static String processingP0Texts3Dot;

	public static String delayBetWeenAutomaticSaves;

	public static String highlightedUnitsColor;

	public static String selectedUnitsColor;

	public static String URSAutosaveTimerIsActivatedSetToP0Minutes;

	public static String errorNoMatchForQueryP0;

	public static String errorNoWordSelection;

	public static String savingURSAnnotations3Dot;

	public static String errorURSAnnotationNotSaved;

	public static String doneURSAnnotationsSaved;

	public static String all;

	public static String annotateURSUnits;

	public static String availableValues;

	public static String createUnitForAllLines;

	public static String chevNoneChev;

	public static String createUnitForEachSelectedLine;

	public static String creatingUnits3Dot;

	public static String creationAborted;

	public static String delete;

	public static String deleteUnitForEachSelectedLine;

	public static String endUrsAnnotation;

	public static String forName;

	public static String errorUrsAnnotationsNotSaved;

	public static String newUnitType;

	public static String unit;

	public static String unitURS;

	public static String savingUrsAnnotations;

	public static String noCQPIdForWordIdP0;

	public static String noPosistionFoundForCQPIdP0;

	public static String schemaAnnotation;

	public static String deleteTheSchema;

	public static String units;

	public static String removeTheSelectedUnitToTheSchema;

	public static String schemaType;

	public static String selectP0P1;

	public static String deleteSchemaP0;

	public static String doyouReallyWantToDeleteTheP0Schema;

	public static String removeUnitP0;

	public static String doyouReallyWantToRemoveTheP0UnitFromtheSchema;

	public static String chevSchemaTypeChev;

	public static String errorNoWordFoundForIdP0;

	public static String errorElementPropertiesViewNotFoundP0;

	public static String partInitialisationErrorP0;

	public static String selectTheFirstElement;

	public static String applyAllNewValuesInTheFields;

	public static String element;

	public static String noAnnotationSelected;

	public static String selectThePreviousElement;

	public static String selectTheLastElement;

	public static String selectTheNextElement;

	public static String searchElementsUsingTheSearchFieldsValues;

	public static String resetTheSearchFieldValues;

	public static String selectTheFirstMatch;

	public static String selectThePreviousMatch;

	public static String setAMatchNumberAndSelectItWithTheEnterKey;

	public static String couldNotSetCurrentMatchP0;

	public static String selectTheNextMatch;

	public static String selectTheLastMatch;

	public static String viewTheMatchesInConcordances;

	public static String applyTheNewP0ValueWithTheReturnOrTabKeys;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(Messages.class);
	}

	private Messages() {
	}
}
