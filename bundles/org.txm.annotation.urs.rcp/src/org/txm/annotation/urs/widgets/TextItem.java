package org.txm.annotation.urs.widgets;

import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;

public class TextItem extends Item {

	private ToolBar parent;

	private Text field;

	public TextItem(ToolBar parent, int style) {
		super(parent, style);
		this.parent = parent;
	}

	public String getText() {
		return field.getText();
	}

	public void setText(String s) {
		field.setText(s);
	}

	public void addSelectionListener(SelectionListener listener) {
		field.addSelectionListener(listener);
	}
}
