package org.txm.annotation.urs.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class DropDownButton extends Button {

	public DropDownButton(ToolBar parent, int style) {
		super(parent, style);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		final Display display = new Display();
		final Shell shell = new Shell(display);
		final ToolBar toolBar = new ToolBar(shell, SWT.NONE);
		Rectangle clientArea = shell.getClientArea();
		toolBar.setLocation(clientArea.x, clientArea.y);
		final Menu menu = new Menu(shell, SWT.POP_UP);
		for (int i = 0; i < 8; i++) {
			MenuItem item = new MenuItem(menu, SWT.PUSH);
			item.setText("Item " + i); //$NON-NLS-1$
			item.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					System.out.println("item " + this); //$NON-NLS-1$
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		}

		ToolItem b1 = new ToolItem(toolBar, SWT.PUSH);
		b1.setText("push me"); //$NON-NLS-1$
		b1.setToolTipText("test"); //$NON-NLS-1$
		
		final ToolItem item = new ToolItem(toolBar, SWT.DROP_DOWN);
		item.setText("default item"); //$NON-NLS-1$
		item.setToolTipText("test"); //$NON-NLS-1$
		item.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				if (event.detail == SWT.ARROW) {
					Rectangle rect = item.getBounds();
					Point pt = new Point(rect.x, rect.y + rect.height);
					pt = toolBar.toDisplay(pt);
					menu.setLocation(pt.x, pt.y);
					menu.setVisible(true);
				}
			}
		});

		toolBar.pack();
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) display.sleep();
		}
		menu.dispose();
		display.dispose();
	}
}
