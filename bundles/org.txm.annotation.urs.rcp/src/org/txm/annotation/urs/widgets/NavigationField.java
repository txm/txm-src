package org.txm.annotation.urs.widgets;

import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.fieldassist.TXMAutoCompleteField;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.txm.annotation.urs.messages.Messages;
import org.txm.annotation.urs.toolbar.URSAnnotationToolbar;
import org.txm.annotation.urs.view.ElementSearchView;
import org.txm.rcp.IImageKeys;
import org.txm.utils.logger.Log;

import visuAnalec.elements.Element;

/**
 * Mimic the List and Combo widget to manage big data volume
 * 
 * Don't forget to add a SelectionListener to the parent to manage the selection change
 * 
 * @author mdecorde
 *
 */
public class NavigationField extends Composite {

	protected String[] identifiants;

	protected org.eclipse.swt.widgets.Text identifiantCombo;

	protected Button identifiantComboButtonStart;

	protected Button identifiantComboButtonUp;

	protected Button identifiantComboButtonDown;

	protected Button identifiantComboButtonEnd;

	protected Button searchButton;

	protected TXMAutoCompleteField identifiantComboAutoCompleteField;

	private URSAnnotationToolbar toolbar;

	public NavigationField(URSAnnotationToolbar parent, int style) {
		super(parent.getAnnotationArea(), style);
		this.toolbar = parent;
		GridLayout layout = new GridLayout(6, false);
		layout.marginLeft = layout.marginRight = 0;
		layout.marginWidth = 0;
		//layout.horizontalSpacing = layout.marginBottom = layout.marginHeight = layout.marginLeft = layout.marginRight = layout.marginTop = layout.marginWidth = 0;
		this.setLayout(layout);

		//		Composite identifiantComboButtons = new Composite(this, SWT.NONE);
		//		GridData gdata = new GridData(GridData.FILL, GridData.FILL, false, false);
		//		identifiantComboButtons.setLayoutData(gdata);
		//		//identifiantComboButtons.setLayout(new GridLayout(1, true));
		//		RowLayout fillLayout = new RowLayout(SWT.HORIZONTAL);
		//		fillLayout.spacing = 0;
		//		fillLayout.marginHeight = 0;
		//		fillLayout.marginWidth = 0;
		//		identifiantComboButtons.setLayout(fillLayout);
		//		RowData rdata = new RowData();
		//		//rdata.width = 20;
		identifiantComboButtonStart = new Button(this, SWT.PUSH);
		identifiantComboButtonStart.setImage(IImageKeys.getImage(IImageKeys.CTRLSTART));
		//		identifiantComboButtonStart.setLayoutData(rdata);
		identifiantComboButtonStart.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				selectFirst();
				toolbar.getEditor().updateWordStyles();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		identifiantComboButtonUp = new Button(this, SWT.PUSH);
		identifiantComboButtonUp.setImage(IImageKeys.getImage(IImageKeys.CTRLREVERSE));
		//		identifiantComboButtonUp.setLayoutData(rdata);
		identifiantComboButtonUp.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				selectPrevious();
				toolbar.getEditor().updateWordStyles();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		identifiantCombo = new org.eclipse.swt.widgets.Text(this, SWT.BORDER);
		KeyStroke keys = KeyStroke.getInstance(SWT.CONTROL, SWT.SPACE);
		identifiantComboAutoCompleteField = new TXMAutoCompleteField(identifiantCombo, new TextContentAdapter(), new String[0], keys);
		Cursor cursor = identifiantCombo.getDisplay().getSystemCursor(SWT.CURSOR_SIZENS);
		identifiantCombo.setCursor(cursor);
		identifiantCombo.setText(Messages.NavigationField_0);

		GridData gdata2 = new GridData(GridData.FILL, GridData.FILL, true, false);
		gdata2.minimumWidth = 100;
		identifiantCombo.setLayoutData(gdata2);
		identifiantCombo.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				//System.out.println("e:"+e.keyCode);
				if (identifiantComboAutoCompleteField.isOpen()) return; // ignore keys when content assist is opened

				if (e.keyCode == SWT.ARROW_UP) {
					selectPrevious();
					toolbar.getEditor().updateWordStyles();
				}
				else if (e.keyCode == SWT.ARROW_DOWN) {
					selectNext();
					toolbar.getEditor().updateWordStyles();
				}
				else if (e.keyCode == SWT.PAGE_UP) {
					select10Previous();
					toolbar.getEditor().updateWordStyles();
				}
				else if (e.keyCode == SWT.PAGE_DOWN) {
					select10Next();
					toolbar.getEditor().updateWordStyles();
				}
				else if (e.keyCode == SWT.END) {
					selectLast();
					toolbar.getEditor().updateWordStyles();
				}
				else if (e.keyCode == SWT.HOME) {
					selectFirst();
					toolbar.getEditor().updateWordStyles();
				}
				else if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					String id = identifiantCombo.getText().trim();
					String prefix = type + "-"; //$NON-NLS-1$
					if (id.startsWith(prefix)) {
						id = id.substring(prefix.length());
						//System.out.println("ID: "+id);
						try {
							Integer i = Integer.parseInt(id);
							if (identifiants.length < i) {
								Log.warning(NLS.bind(Messages.NavigationField_2, identifiantCombo.getText()));
								return;
							}
							setSelectionIndex(i);
							//notifySelectionChanged(null);
							toolbar.getEditor().updateWordStyles();
						}
						catch (Exception ex) {
							Log.warning(NLS.bind(Messages.NavigationField_2, identifiantCombo.getText()));
						}
					}
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
		identifiantCombo.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseScrolled(MouseEvent e) {
				if (identifiantComboAutoCompleteField.isOpen()) return;

				if (e.count > 0) {
					selectPrevious();
				}
				else {
					selectNext();
				}
				//toolbar.getEditor().updateWordStyles();
			}
		});

		identifiantComboButtonDown = new Button(this, SWT.PUSH);
		identifiantComboButtonDown.setImage(IImageKeys.getImage(IImageKeys.CTRLPLAY));
		//		identifiantComboButtonDown.setLayoutData(rdata);
		identifiantComboButtonDown.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				selectNext();
				toolbar.getEditor().updateWordStyles();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		identifiantComboButtonEnd = new Button(this, SWT.PUSH);
		identifiantComboButtonEnd.setImage(IImageKeys.getImage(IImageKeys.CTRLEND));
		//		identifiantComboButtonEnd.setLayoutData(rdata);
		identifiantComboButtonEnd.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				selectLast();
				toolbar.getEditor().updateWordStyles();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		searchButton = new Button(this, SWT.PUSH);
		searchButton.setToolTipText(Messages.NavigationField_4);
		searchButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_SEARCH));
		searchButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		searchButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				ElementSearchView view = ElementSearchView.openView();
				view.loadType(NavigationField.this.toolbar.getCQPCorpus(),
						NavigationField.this.toolbar.getAnalecCorpus(),
						clazz,
						type,
						NavigationField.this.toolbar);
				view.setFocus();
			}
		});
		searchButton.setVisible(false);
	}



	protected void notifySelectionChanged(Object object) {
		Event event = new Event();
		event.data = identifiantComboIndex;
		this.notifyListeners(SWT.Selection, event);
	}

	// This is a set of identifiantCombo control methods to replace the ComboViewer Jface widget
	protected int identifiantComboIndex = 0;

	protected int identifiantComboSize = 0;

	private String type;

	private Class<? extends Element> clazz;

	/**
	 * Set the navigation index AND notify the change value listener
	 * 
	 * @param i
	 */
	public void setSelectionIndex(int i) {
		setSelectionIndex(i, true);
	}

	/**
	 * Set the navigation index AND notify the change value listener
	 * 
	 * @param i
	 * @param notify if false listeners won't be notified
	 */
	public void setSelectionIndex(int i, boolean notify) {
		if (identifiantComboSize <= i) return;
		if (i < 0) return;
		identifiantComboIndex = i;
		if (i == 0) {
			this.identifiantCombo.setText(Messages.NavigationField_5);
		}
		else if (type != null && type.length() > 0) {
			this.identifiantCombo.setText(type + "-" + (i)); //$NON-NLS-1$
		}
		else {
			this.identifiantCombo.setText("" + i); //$NON-NLS-1$
		}
		Event e = new Event();
		e.data = i;
		if (notify) this.notifyListeners(SWT.Selection, e);
	}

	/**
	 * 
	 * @param values
	 * @param type mandatory to display the search button
	 * @param clazz mandatory to display the search button
	 */
	public void setIdentifiants(String[] values, String type, Class<? extends Element> clazz) {
		this.identifiants = values;
		this.type = type;
		this.clazz = clazz;

		GridData gdata = (GridData) searchButton.getLayoutData();
		if (type != null && clazz != null) {
			searchButton.setVisible(true);
			gdata.minimumWidth = -1;
			gdata.widthHint = -1;
		}
		else {
			searchButton.setVisible(false);
			gdata.minimumWidth = 0;
			gdata.widthHint = 0;
		}

		identifiantComboSize = values.length + 1; // + 1 because of <identifiants>
		identifiantComboAutoCompleteField.setProposals(values);
	}

	public int getSelectionIndex() {
		return identifiantComboIndex;
	}

	public int getIdentifiantSize() {
		return identifiantComboSize;
	}

	public String getText() {
		return identifiantCombo.getText();
	}

	public void setEnabled(boolean b) {
		this.identifiantCombo.setEnabled(b);
		this.identifiantComboButtonStart.setEnabled(b);
		this.identifiantComboButtonDown.setEnabled(b);
		this.identifiantComboButtonUp.setEnabled(b);
		this.identifiantComboButtonEnd.setEnabled(b);
		this.searchButton.setEnabled(b);
	}

	public int selectNext() {
		if (identifiantComboSize == 0) return 0;

		int i = getSelectionIndex();
		i = (i + 1) % identifiantComboSize;
		setSelectionIndex(i);
		return i;
	}

	public int selectPrevious() {
		if (identifiantComboSize == 0) return 0;

		int i = getSelectionIndex();
		i = (i - 1) % identifiantComboSize;
		if (i < 0) i = identifiantComboSize + i;
		setSelectionIndex(i);

		return i;
	}

	public int selectFirst() {
		if (identifiantComboSize == 0) return 0;

		int i = 1 % identifiantComboSize;
		setSelectionIndex(i);
		return i;
	}

	public int select10Previous() {
		if (identifiantComboSize == 0) return 0;

		int i = getSelectionIndex();
		i = (i - 10) % identifiantComboSize;
		if (i < 0) i = identifiantComboSize + i;
		setSelectionIndex(i);
		return i;
	}

	public int select10Next() {
		if (identifiantComboSize == 0) return 0;

		int i = getSelectionIndex();
		i = (i + 10) % identifiantComboSize;
		setSelectionIndex(i);
		return i;
	}

	public int selectLast() {
		if (identifiantComboSize == 0) return 0;

		int i = (identifiantComboSize - 1);
		if (i < 0) i = identifiantComboSize + i;
		setSelectionIndex(i);
		return i;
	}
}
