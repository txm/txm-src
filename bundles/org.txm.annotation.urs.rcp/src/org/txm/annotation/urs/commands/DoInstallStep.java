package org.txm.annotation.urs.commands;

import java.io.File;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.osgi.util.NLS;
import org.osgi.framework.Version;
import org.txm.Toolbox;
import org.txm.annotation.urs.messages.Messages;
import org.txm.annotation.urs.preferences.URSPreferences;
import org.txm.utils.BundleUtils;
import org.txm.utils.logger.Log;

/**
 * Copy macros from bundle to TXM macro directory
 * 
 * @author mdecorde
 *
 */
public class DoInstallStep extends org.txm.rcp.commands.TxmCommand {

	/** The ID. */
	public static String ID = "org.txm.annotation.urs.commands.DoInstallStep"; //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) {
		install();
		return null;
	}

	public void install() {

		Log.info(Messages.DoInstallStep_2);
		String saved = URSPreferences.getInstance().getString(URSPreferences.VERSION); //$NON-NLS-1$
		Version currentVersion = BundleUtils.getBundleVersion("org.txm.annotation.urs.rcp"); //$NON-NLS-1$

		if (saved != null && saved.length() > 0) {
			Version savedVersion = new Version(saved);
			if (currentVersion.compareTo(savedVersion) <= 0) {
				Log.info(Messages.DoInstallStep_5);
				//System.out.println("No post-installation of URS to do");
				return; // nothing to do
			}
		}
		Log.info(NLS.bind(Messages.DoInstallStep_6, currentVersion));

		File previousURSMacroDirectory = new File(Toolbox.getTxmHomePath(), "scripts/groovy/user/org/txm/macro/analec"); //$NON-NLS-1$
		if (previousURSMacroDirectory.exists() && saved != null && saved.length() > 0) {
			File back = new File(previousURSMacroDirectory.getParentFile(), "urs_" + saved); //$NON-NLS-1$
			if (back.exists()) {
				Log.info(NLS.bind(Messages.DoInstallStep_9, back.getAbsolutePath()));
				return;
			}
			previousURSMacroDirectory.renameTo(back);
			Log.info(NLS.bind(Messages.DoInstallStep_11, back.getAbsolutePath()));
		}

		File macroDirectory = new File(Toolbox.getTxmHomePath(), "scripts/groovy/user/org/txm/macro/analec"); //$NON-NLS-1$
		if (!BundleUtils.copyFiles("org.txm.annotation.urs.rcp", "src", "org/txm/macro/", "urs", macroDirectory, true)) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			Log.warning(NLS.bind(Messages.DoInstallStep_17, macroDirectory.getAbsolutePath()));
			return;
		}

		Log.fine(Messages.DoInstallStep_18);
		URSPreferences.getInstance().put(URSPreferences.VERSION, currentVersion.toString());
		return;
	}
}
