package org.txm.annotation.urs.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.annotation.urs.view.ElementPropertiesView;
import org.txm.core.results.TXMResult;
import org.txm.edition.rcp.editors.SynopticEditionEditor;
import org.txm.rcp.TXMWindows;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.OSDetector;
import org.txm.utils.logger.Log;

import visuAnalec.donnees.Corpus;
import visuAnalec.vue.Vue;

public class SaveCorpus extends AbstractHandler {

	/** The ID. */
	public static String ID = "org.txm.annotation.urs.commands.LoadAnnotationStructure"; //$NON-NLS-1$

	Corpus corpus;

	/** The selection. */

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object first = CorporaView.getFirstSelectedObject();
		MainCorpus mainCorpus = null;
		if (first instanceof org.txm.searchengine.cqp.corpus.MainCorpus) {
			mainCorpus = (MainCorpus) first;
		}
		else if (first instanceof TXMResult) {
			mainCorpus = ((TXMResult) first).getFirstParent(MainCorpus.class);
		}
		else {
			Log.warning(NLS.bind(Messages.SaveCorpus_0, first));
			return null;
		}
		save(mainCorpus, event);

		return null;
	}

	public static boolean save(final MainCorpus mainCorpus, ExecutionEvent event) {

		if (!URSCorpora.hasCorpus(mainCorpus)) {
			Log.warning(Messages.bind(Messages.errorNoAnalecCorpusAssociatedToTheCQPCorpusP0, mainCorpus));
			return false;
		}

		Corpus corpus = URSCorpora.getCorpus(mainCorpus);

		if (corpus == null) {
			Log.warning(Messages.bind(Messages.errorNoAnalecCorpusAssociatedToTheCQPCorpusP0, mainCorpus));
			return false;
		}

		ElementPropertiesView view = ElementPropertiesView.getView();
		if (view != null && view.getCurrentAnalecCorpus().equals(corpus)) {
			view.applyAll();
		}

		Vue vue = URSCorpora.getVue(corpus);
		if (!corpus.isModifie() && !vue.isModifiee()) {
			Log.warning(Messages.bind(Messages.noChangeToSaveInP0, mainCorpus.getCqpId()));
			return false;
		}


		File binaryCorpusDirectory = mainCorpus.getProjectDirectory();
		File fichierCorpus = URSCorpora.getECFile(mainCorpus);
		File fichierVueCorpus = URSCorpora.getECVFile(mainCorpus);

		Log.info(NLS.bind(Messages.SaveCorpus_3, fichierCorpus, fichierVueCorpus));
		if (!URSCorpora.saveCorpus(mainCorpus)) {
			Log.warning(Messages.SaveCorpus_5);
			return false;
		}

		mainCorpus.getProject().appendToLogs(Messages.bind(Messages.ursAnnotationsSavedByP0, OSDetector.getUserAndOSInfos()));

		mainCorpus.setIsModified(false);
		if (event != null) {
			HandlerUtil.getActiveShell(event).getDisplay().syncExec(new Runnable() {

				@Override
				public void run() {
					CorporaView.refreshObject(mainCorpus);

					// find out which editor is using the MainCorpus
					for (IWorkbenchPage page : TXMWindows.getActiveWindow().getPages()) {
						for (IEditorReference editor_ref : page.getEditorReferences()) {
							IEditorPart editor = editor_ref.getEditor(false);
							if (editor != null && editor instanceof SynopticEditionEditor) {
								if (mainCorpus.equals(((SynopticEditionEditor) editor).getCorpus())) {
									((SynopticEditionEditor) editor).fireIsDirty();
								}
							}
						}
					}
				}
			});
		}
		return true;
	}
}
