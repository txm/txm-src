package org.txm.annotation.urs.commands;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.kohsuke.args4j.Option;
import org.txm.annotation.urs.URSAnnotationReIndexer;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.OSDetector;
import org.txm.utils.logger.Log;

import visuAnalec.donnees.Corpus;
import visuAnalec.fichiers.FichiersGlozz;

public class ImportGlozzAnnotations extends AbstractHandler {



	@Option(name = "aafile", usage = "an example file", widget = "File", required = true, def = "text.aa")
	File aafile;

	@Option(name = "aamfile", usage = "an example file", widget = "File", required = true, def = "model.aam")
	File aamfile;

	@Option(name = "acfile", usage = "an example file", widget = "File", required = true, def = "annotations.ac")
	File acfile;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			// Open the parameters input dialog box
			if (!ParametersDialog.open(this)) return null;

			// END OF PARAMETERS

			return doImport(aafile, aamfile, acfile);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static boolean doImport(File aafile, File aamfile, File acfile) throws CqiClientException, IOException, CqiServerError {
		if (!(aafile.exists() && aafile.exists() && aafile.exists())) {
			Log.warning(Messages.ImportGlozzAnnotations_0);
			return false;
		}

		Object first = CorporaView.getFirstSelectedObject();
		if (!(first instanceof org.txm.searchengine.cqp.corpus.CQPCorpus)) {
			Log.warning(NLS.bind(Messages.ImportGlozzAnnotations_1, first));
			return false;
		}

		MainCorpus mainCorpus = ((org.txm.searchengine.cqp.corpus.CQPCorpus) first).getMainCorpus();
		Corpus analecCorpus = URSCorpora.getCorpus(mainCorpus);
		analecCorpus.clearAll(); // remove all : annotations, structure

		Log.info(NLS.bind(Messages.ImportGlozzAnnotations_2, aamfile));
		if (!FichiersGlozz.importerModeleGlozz(analecCorpus, aamfile)) {
			Log.warning(Messages.ImportGlozzAnnotations_3);
			return false;
		}

		Log.info(NLS.bind(Messages.ImportGlozzAnnotations_4, acfile, aafile));
		if (!FichiersGlozz.importerGlozz(analecCorpus, acfile, aafile)) {
			Log.warning(Messages.ImportGlozzAnnotations_6);
			return false;
		}

		Log.info(Messages.ImportGlozzAnnotations_7);
		URSAnnotationReIndexer aari = new URSAnnotationReIndexer(mainCorpus, analecCorpus);
		if (!aari.process()) {
			Log.warning(Messages.ImportGlozzAnnotations_8);
			return false;
		}

		Log.info(Messages.ImportGlozzAnnotations_9);
		URSCorpora.saveCorpus(analecCorpus);

		mainCorpus.getProject().appendToLogs(Messages.bind(Messages.ursAnnotationsImportedFromP0P1AndP2ByP3,  aafile, aamfile, acfile, OSDetector.getUserAndOSInfos()));

		Log.info(Messages.ImportGlozzAnnotations_10);
		return true;
	}
}
