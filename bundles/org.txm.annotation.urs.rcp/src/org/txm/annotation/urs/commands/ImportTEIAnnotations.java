package org.txm.annotation.urs.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.kohsuke.args4j.Option;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.imports.URSAnnotationsImporter;
import org.txm.annotation.urs.messages.Messages;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.OSDetector;
import org.txm.utils.logger.Log;

import visuAnalec.donnees.Corpus;
import visuAnalec.vue.Vue;

/**
 * Export the Analec annotation of the selected Corpus
 * 
 * @author mdecorde
 *
 */
public class ImportTEIAnnotations extends AbstractHandler {

	@Option(name = "directory", usage = "the directory containing the XML-TEI-URS annotations files", widget = "Folder", required = true, def = "annotations")
	File directory;

	@Option(name = "aamFile", usage = "The Glozz model file", widget = "FileOpen", required = false, def = "model.aam")
	File aamFile;

	@Option(name = "evFile", usage = "The Analec view file", widget = "FileOpen", required = false, def = "view.ev")
	File evFile;

	//@Option(name="standoffAnnotations",usage="select this if the directory contains standoff annotation files", widget="Boolean", required=true, def="false")
	//boolean standoffAnnotations;
	@Option(name = "resetAnnotations", usage = "Remove the old annotations", widget = "Boolean", required = true, def = "false")
	Boolean resetAnnotations;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			// Open the parameters input dialog box
			if (!ParametersDialog.open(this)) return null;

			Object first = CorporaView.getFirstSelectedObject();
			if (!(first instanceof org.txm.searchengine.cqp.corpus.CQPCorpus)) {
				Log.warning(NLS.bind(Messages.ImportTEIAnnotations_0, first));
				return null;
			}

			final MainCorpus mainCorpus = ((org.txm.searchengine.cqp.corpus.CQPCorpus) first).getMainCorpus();
			final Corpus analecCorpus = URSCorpora.getCorpus(mainCorpus);
			final Vue vue = URSCorpora.getVue(mainCorpus);

			JobHandler job = new JobHandler(NLS.bind(Messages.ImportTEIAnnotations_1, mainCorpus.getName())) {

				@Override
				protected IStatus _run(SubMonitor monitor) throws Throwable {
					try {
						if (resetAnnotations) {
							analecCorpus.clearAnnot();
						}

						if (!importer(monitor, directory, aamFile, evFile, true, mainCorpus, analecCorpus, vue)) {
							Log.warning(Messages.ImportTEIAnnotations_2);
							return Status.CANCEL_STATUS;
						}
						else {
							return Status.OK_STATUS;
						}
					}
					catch (Throwable e) {
						Log.warning(NLS.bind(Messages.ImportTEIAnnotations_3, e.getLocalizedMessage()));
						throw e;
					}
				}
			};
			job.schedule();

		}
		catch (Exception e) {
			Log.warning(NLS.bind(Messages.ImportTEIAnnotations_3, e.getLocalizedMessage()));
			Log.printStackTrace(e);
		}
		return null;
	}

	/**
	 * <pre>
	 * IProgressMonitor monitor = getIProgressMonitor();
	 * File directory = new File("resultdir");
	 * File aamFile = new File("model.aam");
	 * Boolean stdf = true;
	 * String name = "BROWN";
	 * MainCorpus mainCorpus = CorpusManager.getCorpusManager().getCorpus(name)
	 * Corpus analecCorpus = URSCorpora.getCorpus(name.toUpperCase());
	 * Vue vue = URSCorpora.getVue(name);
	 * 
	 * ImportTEIAnnotations.importer(monitor, directory, aamFile, stdf, mainCorpus, analecCorpus, vue);
	 * </pre>
	 * 
	 * @param monitor
	 * @param annotationDirectory
	 * @param aamFile
	 * @param standoffAnnotations
	 * @param mainCorpus
	 * @param analecCorpus
	 * @param vue
	 * @return
	 * @throws Exception
	 */
	public static boolean importer(IProgressMonitor monitor, File annotationDirectory, File aamFile, File evFile, boolean standoffAnnotations, MainCorpus mainCorpus, Corpus analecCorpus, Vue vue)
			throws Exception {
		//		if (!standoffAnnotations) { // use stax because files are too big for DOM process
		//			AnalecAnnotationsImporter importer = new AnalecAnnotationsImporter(monitor, annotationDirectory, mainCorpus, analecCorpus, vue);
		//			boolean ret = importer.process();
		//			if (ret) System.out.println("Done: "+analecCorpus.getToutesUnites().size()+" unites "+analecCorpus.getToutesRelations().size()+" relations "+analecCorpus.getTousSchemas().size()+" schemas.");
		//			return ret;
		//		} else {
		//			DOMAnalecAnnotationsImporter importer = new DOMAnalecAnnotationsImporter(monitor, annotationDirectory, mainCorpus, analecCorpus, vue);
		//			boolean ret = importer.process();
		//			if (ret) System.out.println("Done: "+analecCorpus.getToutesUnites().size()+" unites "+analecCorpus.getToutesRelations().size()+" relations "+analecCorpus.getTousSchemas().size()+" schemas.");
		//			return ret;
		//		}
		//TODO: DOMAnalecAnnotationsImporter not finished
		URSAnnotationsImporter importer = new URSAnnotationsImporter(monitor, annotationDirectory, aamFile, evFile, mainCorpus, analecCorpus, vue);
		boolean ret = importer.process();

		if (ret) {
			Log.info(TXMCoreMessages.bind(Messages.ImportTEIAnnotations_5, analecCorpus.getToutesUnites().size(), analecCorpus.getToutesRelations().size(), analecCorpus.getTousSchemas().size()));
			mainCorpus.setIsModified(true);

			mainCorpus.getProject().appendToLogs(Messages.bind(Messages.ursAnnotationsImportedFromP0P1AndP2ByP3, annotationDirectory, aamFile, evFile, OSDetector.getUserAndOSInfos()));

			CorporaView.refreshObject(mainCorpus);
		}
		return ret;
	}
}
