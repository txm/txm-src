package org.txm.annotation.urs.commands;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.kohsuke.args4j.Option;
import org.txm.Toolbox;
import org.txm.annotation.urs.URSAnnotationReIndexer;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.objects.Project;
import org.txm.rcp.handlers.scripts.ExecuteImportScript;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.AsciiUtils;
import org.txm.utils.DeleteDir;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

import visuAnalec.donnees.Corpus;
import visuAnalec.fichiers.FichiersGlozz;
import visuAnalec.fichiers.FichiersJava;

public class ImportGlozzCorpus extends AbstractHandler {



	// @Option(name="aafile",usage="an example file", widget="File", required=true, def="text.aa")
	// File aafile;

	// @Option(name="acfile",usage="an example file", widget="File", required=true, def="annotations.ac")
	// File acfile

	@Option(name = "glozzDirectory", usage = "A folder containing the Glozz files: aa ac and aam", widget = "Folder", required = true, def = "glozz")
	File glozzDirectory;

	@Option(name = "aamfile", usage = "The aam file to use", widget = "File", required = true, def = "model.aam")
	File aamfile;

	private String name;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			// Open the parameters input dialog box
			if (!ParametersDialog.open(this)) return null;

			// END OF PARAMETERS
			if (!glozzDirectory.exists()) {
				Log.warning(Messages.ImportGlozzCorpus_0);
				return null;
			}

			name = glozzDirectory.getName();
			name = AsciiUtils.buildId(name);
			String newCorpusName = name.toUpperCase();
			if (Toolbox.workspace.getProject(newCorpusName) != null) {
				boolean b = MessageDialog.openConfirm(Display.getCurrent().getActiveShell(), TXMUIMessages.warning, NLS.bind(TXMUIMessages.theP0CorpusDirectoryAlreadyExistsDoYouWantToReplaceIt,
						newCorpusName));
				if (!b) {
					Log.info(Messages.importAborted);
					return null;
				}
			}

			final File srcDir = new File(glozzDirectory, name);
			Log.info(NLS.bind(Messages.ImportGlozzCorpus_1, srcDir));
			DeleteDir.deleteDirectory(srcDir);
			srcDir.mkdir();

			final File ecDir = new File(glozzDirectory, "ec"); //$NON-NLS-1$
			Log.info(NLS.bind(Messages.ImportGlozzCorpus_3, ecDir));
			DeleteDir.deleteDirectory(ecDir);
			ecDir.mkdir();


			if (!srcDir.exists()) {
				Log.warning(Messages.ImportGlozzCorpus_4);
				return null;
			}

			if (!ecDir.exists()) {
				Log.warning(Messages.ImportGlozzCorpus_5);
				return null;
			}

			// write the TXT file WITH paragraphs
			File[] aaFiles = glozzDirectory.listFiles(new FilenameFilter() {

				@Override
				public boolean accept(File dir, String name) {
					return name.endsWith(".aa"); //$NON-NLS-1$
				}
			});
			File[] acFiles = glozzDirectory.listFiles(new FilenameFilter() {

				@Override
				public boolean accept(File dir, String name) {
					return name.endsWith(".ac"); //$NON-NLS-1$
				}
			});

			if (aaFiles == null || aaFiles.length == 0) {
				Log.warning(NLS.bind(Messages.ImportGlozzCorpus_8, glozzDirectory));
				return false;
			}
			if (acFiles == null || acFiles.length == 0) {
				Log.warning(NLS.bind(Messages.ImportGlozzCorpus_9, glozzDirectory));
				return false;
			}
			if (aaFiles.length != acFiles.length) {
				Log.warning(NLS.bind(Messages.ImportGlozzCorpus_10, glozzDirectory));
				return false;
			}

			Log.info(NLS.bind(Messages.ImportGlozzCorpus_11, glozzDirectory));
			Arrays.sort(acFiles);
			Arrays.sort(aaFiles);
			for (int i = 0; i < aaFiles.length; i++) {
				File acFile = acFiles[i];
				File aaFile = aaFiles[i];
				String name = acFile.getName();
				int idx = name.indexOf("."); //$NON-NLS-1$
				if (idx > 0) name = name.substring(0, idx);

				File txtFile = new File(srcDir, name + ".txt"); //$NON-NLS-1$
				File ecFile = new File(ecDir, name + ".ec"); //$NON-NLS-1$
				Corpus tmpAnalecCorpus = new Corpus(); // need a temporary corpus

				if (!FichiersGlozz.importerGlozz(tmpAnalecCorpus, acFile, aaFile)) {
					Log.warning(Messages.ImportGlozzCorpus_15);
					return null;
				}

				final String texte = tmpAnalecCorpus.getTexte();
				int debParag = 0;
				Integer[] finPars = tmpAnalecCorpus.getFinParagraphes();
				StringBuffer newTexte = new StringBuffer(texte.length() + finPars.length);
				for (int iParagraph = 0; iParagraph < finPars.length; iParagraph++) {
					newTexte.append(texte.substring(debParag, finPars[iParagraph])).append("\n"); //$NON-NLS-1$
					debParag = finPars[iParagraph];
				}

				IOUtils.write(txtFile, newTexte.toString()); // write the TXT file for TXM TXT import module
				FichiersJava.enregistrerCorpus(tmpAnalecCorpus, ecFile); // write for later

				if (!txtFile.exists()) {
					Log.warning(Messages.ImportGlozzCorpus_17);
					return null;
				}
			}

			Project project = Toolbox.workspace.getProject(newCorpusName);
			if (project != null) {
				// CQPSearchEngine.getEngine().stop();
				project.delete();
			}
			project = new Project(Toolbox.workspace, newCorpusName);
			project.setName(newCorpusName);
			project.setSourceDirectory(srcDir.getAbsolutePath());
			project.setImportModuleName("txt"); //$NON-NLS-1$
			project.getEditionDefinition("default").setBuildEdition(true); //$NON-NLS-1$

			JobHandler job = ExecuteImportScript.executeScript(project);
			if (job == null) { // job is scheduled in ExecuteImportScript.executeScript
				Log.warning(Messages.ImportGlozzCorpus_23);
				return null;
			}

			JobHandler job2 = new JobHandler(Messages.ImportGlozzCorpus_24) {

				@Override
				protected IStatus _run(SubMonitor monitor) throws Exception {

					try {
						MainCorpus corpus = CorpusManager.getCorpusManager().getCorpus(name.toUpperCase());
						if (corpus == null) {
							Log.warning(Messages.theCorpusWasNotCreatedAborting);
							return Status.CANCEL_STATUS;
						}
						corpus.compute(false);
						Corpus analecCorpus = URSCorpora.getCorpus(corpus);
						analecCorpus.clearAll(); // remove all : annotations, structure
						File[] ecFiles = ecDir.listFiles(IOUtils.HIDDENFILE_FILTER);
						Arrays.sort(ecFiles);
						for (File ecFile : ecFiles) {
							FichiersJava.concatener(ecFile, analecCorpus);
						}

						Log.info(NLS.bind(Messages.ImportGlozzCorpus_25, aamfile));
						if (!FichiersGlozz.importerModeleGlozz(analecCorpus, aamfile)) {
							Log.warning(Messages.ImportGlozzCorpus_26);
							return Status.CANCEL_STATUS;
						}
						//
						// System.out.println("Importing Glozz corpus from: "+acfile+" and "+aafile);
						// if (!FichiersGlozz.importerGlozz(analecCorpus, acfile, aafile)) {
						// System.out.println("Error while importing Glozz corpus.");
						// return null;
						// }

						Log.info(Messages.ImportGlozzCorpus_27);
						URSCorpora.removeCorpus(corpus); // remove old corpus if any

						URSAnnotationReIndexer aari = new URSAnnotationReIndexer(corpus, analecCorpus);
						if (!aari.process()) {
							Log.warning(Messages.ImportGlozzCorpus_28);
							return Status.CANCEL_STATUS;
						}

						Log.info(Messages.ImportGlozzCorpus_29);
						URSCorpora.saveCorpus(analecCorpus);
						URSCorpora.getVue(analecCorpus).retablirVueParDefaut();
						Log.info(Messages.ImportGlozzCorpus_30);
						analecCorpus.setTexte(""); // free memory //$NON-NLS-1$
						DeleteDir.deleteDirectory(ecDir); // cleaning
						DeleteDir.deleteDirectory(srcDir); // cleaning

						return Status.OK_STATUS;
					}
					catch (Exception e) {
						Log.warning(NLS.bind(Messages.ImportGlozzCorpus_32, e.getLocalizedMessage()));
						throw e;
					}
				}
			};
			job.join(0, null);
			if (job.getResult() == Status.OK_STATUS) {
				job2.startJob(true); // wait for the TXT import job to finish
			}
			else {
				Log.warning(Messages.exportFailed);
			}
			return null;
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
