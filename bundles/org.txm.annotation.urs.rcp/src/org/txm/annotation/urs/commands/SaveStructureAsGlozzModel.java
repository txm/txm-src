package org.txm.annotation.urs.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

import visuAnalec.donnees.Corpus;
import visuAnalec.fichiers.FichiersGlozz;

public class SaveStructureAsGlozzModel extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object first = CorporaView.getFirstSelectedObject();
		if (!(first instanceof org.txm.searchengine.cqp.corpus.CQPCorpus)) {
			Log.warning(NLS.bind(Messages.SaveStructureAsGlozzModel_0, first));
			return null;
		}

		MainCorpus mainCorpus = ((org.txm.searchengine.cqp.corpus.CQPCorpus) first).getMainCorpus();

		Corpus corpus = URSCorpora.getCorpus(mainCorpus);

		//		if (VisuStructure.isOpen()) {
		//			System.out.println("Structure de corpus en cours d'édition. Abandon.");
		//			return null;
		//		}

		Log.info(NLS.bind(Messages.SaveStructureAsGlozzModel_1, corpus));

		if (!FichiersGlozz.exporterModeleGlozz(corpus.getStructure())) {
			Log.warning(Messages.SaveStructureAsGlozzModel_2);
			return null;
		}

		Log.info(Messages.SaveStructureAsGlozzModel_3);
		return null;
	}
}
