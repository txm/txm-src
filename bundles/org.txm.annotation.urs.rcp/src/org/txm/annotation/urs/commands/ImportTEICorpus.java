package org.txm.annotation.urs.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.kohsuke.args4j.Option;
import org.txm.Toolbox;
import org.txm.annotation.urs.URSAnnotationReIndexer;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.objects.Project;
import org.txm.rcp.handlers.scripts.ExecuteImportScript;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.AsciiUtils;
import org.txm.utils.DeleteDir;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

import visuAnalec.donnees.Corpus;
import visuAnalec.fichiers.FichiersTEI;

public class ImportTEICorpus extends AbstractHandler {

	@Option(name = "xmlFile", usage = "an example file", widget = "File", required = true, def = "text.xml")
	File xmlFile;

	private String name;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			// Open the parameters input dialog box
			if (!ParametersDialog.open(this)) return null;

			// END OF PARAMETERS
			if (!(xmlFile.exists())) {
				Log.warning(NLS.bind(Messages.ImportTEICorpus_0, xmlFile));
				return null;
			}

			name = xmlFile.getName();
			int idx = name.indexOf("."); //$NON-NLS-1$
			name = name.substring(0, idx);
			name = AsciiUtils.buildId(name);

			File srcDir = new File(xmlFile.getParentFile(), name);
			Log.info(Messages.ImportTEICorpus_2);
			DeleteDir.deleteDirectory(srcDir);
			srcDir.mkdir();

			if (!srcDir.exists()) {
				Log.warning(Messages.ImportTEICorpus_3);
				return null;
			}

			// write the TXT file WITH paragraphs
			File txtFile = new File(srcDir, name + ".txt"); //$NON-NLS-1$
			Corpus tmpAnalecCorpus = new Corpus(); // need a temporary corpus
			Log.info(NLS.bind(Messages.ImportTEICorpus_5, xmlFile));
			if (!FichiersTEI.convertir(xmlFile, new File("/tmp"), tmpAnalecCorpus)) { //$NON-NLS-1$
				Log.warning(Messages.ImportTEICorpus_7);
				return null;
			}

			final String texte = tmpAnalecCorpus.getTexte();
			int debParag = 0;
			Integer[] finPars = tmpAnalecCorpus.getFinParagraphes();
			StringBuffer newTexte = new StringBuffer(texte.length() + finPars.length);
			for (int i = 0; i < finPars.length; i++) {
				newTexte.append(texte.substring(debParag, finPars[i])).append("\n"); //$NON-NLS-1$
				debParag = finPars[i];
			}

			IOUtils.write(txtFile, newTexte.toString());

			if (!txtFile.exists()) {
				Log.warning(Messages.ImportTEICorpus_9);
				return null;
			}

			Project project = new Project(Toolbox.workspace, name.toUpperCase());
			project.setSourceDirectory(srcDir.getAbsolutePath());
			project.setImportModuleName("txt"); //$NON-NLS-1$
			project.getEditionDefinition("default").setBuildEdition(true); //$NON-NLS-1$

			JobHandler job = ExecuteImportScript.executeScript(project);
			if (job == null) { // job is scheduled in ExecuteImportScript.executeScript
				Log.warning(Messages.ImportTEICorpus_15);
				return null;
			}

			JobHandler job2 = new JobHandler(Messages.ImportTEICorpus_16) {

				@Override
				protected IStatus _run(SubMonitor monitor) throws Exception {
					try {
						MainCorpus corpus = CorpusManager.getCorpusManager().getCorpus(name.toUpperCase());
						File analecDirectory = new File(corpus.getProjectDirectory(), "analec"); //$NON-NLS-1$
						analecDirectory.mkdirs();
						Corpus analecCorpus = URSCorpora.getCorpus(corpus);
						analecCorpus.clearAll(); // remove all : annotations, structure

						Log.info(NLS.bind(Messages.ImportTEICorpus_18, xmlFile));
						if (!FichiersTEI.convertir(xmlFile, analecDirectory, analecCorpus)) {
							Log.warning(Messages.ImportTEICorpus_19);
							return null;
						}

						Log.info(Messages.ImportTEICorpus_20);

						URSCorpora.removeCorpus(corpus); // remove old corpus if any

						URSAnnotationReIndexer aari = new URSAnnotationReIndexer(corpus, analecCorpus);
						if (!aari.process()) {
							Log.warning(Messages.ImportTEICorpus_21);
							return null;
						}

						Log.info(Messages.ImportTEICorpus_22);
						URSCorpora.saveCorpus(analecCorpus);
						URSCorpora.getVue(analecCorpus).retablirVueParDefaut();
						Log.info(Messages.ImportTEICorpus_23);

						return Status.OK_STATUS;
					}
					catch (Exception e) {
						Log.warning(NLS.bind(Messages.ImportTEICorpus_24, e));
						throw e;
					}
				}
			};
			job2.startJob(true); // wait for the TXT import job to finish
			return null;
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
