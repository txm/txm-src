package org.txm.annotation.urs.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

import visuAnalec.donnees.Corpus;
import visuAnalec.fichiers.FichiersJava;

public class SaveStructure extends AbstractHandler {

	/** The ID. */
	public static String ID = SaveStructure.class.getSimpleName();

	/** The selection. */

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object first = CorporaView.getFirstSelectedObject();
		if (!(first instanceof org.txm.searchengine.cqp.corpus.CQPCorpus)) {
			Log.warning(NLS.bind(Messages.SaveStructure_0, first));
			return null;
		}

		MainCorpus mainCorpus = ((org.txm.searchengine.cqp.corpus.CQPCorpus) first).getMainCorpus();
		return save(mainCorpus);
	}

	public static boolean save(MainCorpus mainCorpus) {
		Corpus corpus = URSCorpora.getCorpus(mainCorpus);

		File binaryCorpusDirectory = mainCorpus.getProjectDirectory();
		File fichierStructure = new File(binaryCorpusDirectory, "/analec/" + mainCorpus.getID() + ".ecs"); //$NON-NLS-1$ //$NON-NLS-2$
		Log.info(NLS.bind(Messages.SaveStructure_3, corpus, fichierStructure));

		if (!FichiersJava.enregistrerStructure(corpus, fichierStructure)) {
			Log.warning(Messages.SaveStructure_5);
			return false;
		}

		return true;
	}
}
