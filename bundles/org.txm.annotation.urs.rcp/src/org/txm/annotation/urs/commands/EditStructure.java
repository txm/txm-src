package org.txm.annotation.urs.commands;

import java.awt.Dimension;
import java.awt.Point;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.annotation.urs.view.ElementPropertiesView;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

import visuAnalec.VisuMain;
import visuAnalec.donnees.Corpus;
import visuAnalec.donnees.VisuStructure;
import visuAnalec.util.GMessages;

public class EditStructure extends AbstractHandler {

	/** The ID. */
	public static String ID = "org.txm.annotation.urs.commands.LoadAnnotationStructure"; //$NON-NLS-1$

	/** The selection. */

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object first = CorporaView.getFirstSelectedObject();
		if (!(first instanceof org.txm.searchengine.cqp.corpus.CQPCorpus)) {
			Log.warning(NLS.bind(Messages.EditStructure_0, first));
			return null;
		}

		MainCorpus mainCorpus = ((org.txm.searchengine.cqp.corpus.CQPCorpus) first).getMainCorpus();
		Corpus corpus = URSCorpora.getCorpus(mainCorpus);

		//System.out.println("Editing structure for corpus="+corpus);

		ElementPropertiesView.closeView(); // ensure no properties are modified
		open(corpus);

		return null;
	}

	public static void open(final Corpus corpus2) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				try {
					//System.out.println("Show editor...");
					//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					VisuStructure visuStructure = VisuStructure.newVisuStructure(null, corpus2);
					visuStructure.setExtendedState(VisuMain.NORMAL);

					Point locFenetre = new Point(50, 50);
					Dimension dimFenetre = new Dimension(500, 500);
					visuStructure.setLocation(locFenetre);
					visuStructure.setSize(dimFenetre);

					visuStructure.setVisible(true);
					visuStructure.agencer();
				}
				catch (Throwable ex) {
					GMessages.erreurFatale(ex);
				}
			}
		});
	}
}
