package org.txm.annotation.urs.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.kohsuke.args4j.Option;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

import visuAnalec.donnees.Corpus;
import visuAnalec.fichiers.FichiersGlozz;

public class LoadStructureFromGlozz extends AbstractHandler {

	@Option(name = "aamfile", usage = "an example file", widget = "File", required = true, def = "model.aam")
	File aamfile;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object first = CorporaView.getFirstSelectedObject();
		if (!(first instanceof org.txm.searchengine.cqp.corpus.CQPCorpus)) {
			Log.warning(NLS.bind(Messages.LoadStructureFromGlozz_0, first));
			return null;
		}

		MainCorpus mainCorpus = ((org.txm.searchengine.cqp.corpus.CQPCorpus) first).getMainCorpus();
		Corpus corpus = URSCorpora.getCorpus(mainCorpus);

		//		if (VisuStructure.isOpen()) {
		//			System.out.println("Structure de corpus en cours d'édition. Abandon.");
		//			return null;
		//		}

		if (!ParametersDialog.open(this)) return null;

		Log.info(NLS.bind(Messages.LoadStructureFromGlozz_1, mainCorpus));

		// END OF PARAMETERS
		load(aamfile, corpus);
		return null;
	}

	public static boolean load(File aamfile, Corpus corpus) {
		if (!(aamfile.exists() && aamfile.exists() && aamfile.exists())) {
			Log.warning(Messages.LoadStructureFromGlozz_2);
			return false;
		}

		File fichierModele = aamfile;
		if (!FichiersGlozz.importerModeleGlozz(corpus, fichierModele)) {
			Log.warning(Messages.LoadStructureFromGlozz_3);
			return false;
		}

		Log.info(Messages.LoadStructureFromGlozz_4);
		return true;
	}
}
