package org.txm.annotation.urs.commands;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.kohsuke.args4j.Option;
import org.txm.annotation.urs.export.ExportToGlozzMacro;
import org.txm.annotation.urs.messages.Messages;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.logger.Log;

public class ExportGlozzCorpus extends AbstractHandler {

	@Option(name = "exportDirectory", usage = "the result directory ", widget = "Folder", required = true, def = "result directory")
	File exportDirectory;

	@Option(name = "unit_type", usage = "A unit type to export", widget = "String", required = true, def = "MENTION")
	String unit_type;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			// Open the parameters input dialog box
			if (!ParametersDialog.open(this)) return null;

			Object first = CorporaView.getFirstSelectedObject();
			if (!(first instanceof org.txm.searchengine.cqp.corpus.CQPCorpus)) {
				Log.warning(NLS.bind(Messages.ExportGlozzCorpus_0, first));
				return null;
			}

			final MainCorpus mainCorpus = ((org.txm.searchengine.cqp.corpus.CQPCorpus) first).getMainCorpus();

			JobHandler job = new JobHandler(NLS.bind(Messages.ExportGlozzCorpus_1, mainCorpus.getName())) {

				@Override
				protected IStatus _run(SubMonitor monitor) throws Throwable {

					try {
						if (exportDirectory == null) {
							Log.warning(Messages.noExportDirectorySetAborting);
							return Status.CANCEL_STATUS;
						}
						exportDirectory.mkdirs();

						export(exportDirectory, mainCorpus, unit_type);
					}
					catch (Throwable e) {
						Log.warning(NLS.bind(Messages.ExportGlozzCorpus_3, e.getLocalizedMessage()));
						throw e;
					}
					return Status.OK_STATUS;
				}
			};
			job.schedule();

			return null;
		}
		catch (Throwable e) {
			Log.warning(NLS.bind(Messages.ExportGlozzCorpus_3, e.getLocalizedMessage()));
			Log.printStackTrace(e);
		}
		return null;
	}

	public static void export(File exportDirectory, MainCorpus mainCorpus, String unit_type) throws NumberFormatException, IOException, CqiServerError, CqiClientException {
		ExportToGlozzMacro exporter = new ExportToGlozzMacro();
		File exportFile = new File(exportDirectory, mainCorpus.getID());
		exporter.doExport(mainCorpus, unit_type, exportFile.getAbsolutePath());
		return;
	}
}
