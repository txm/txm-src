package org.txm.annotation.urs.commands;

import java.awt.Dimension;
import java.awt.Point;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

import visuAnalec.VisuMain;
import visuAnalec.donnees.Corpus;
import visuAnalec.util.GMessages;
import visuAnalec.vue.VisuVue;
import visuAnalec.vue.Vue;

public class EditVue extends AbstractHandler {

	/** The ID. */
	public static String ID = EditVue.class.getName(); //$NON-NLS-1$

	Vue vue;

	/** The selection. */

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object first = CorporaView.getFirstSelectedObject();
		if (!(first instanceof org.txm.searchengine.cqp.corpus.CQPCorpus)) {
			Log.warning(Messages.bind(Messages.selectionIsNotACorpusP0, first));
			return null;
		}

		MainCorpus mainCorpus = ((org.txm.searchengine.cqp.corpus.CQPCorpus) first).getMainCorpus();
		Corpus corpus = URSCorpora.getCorpus(mainCorpus);
		vue = URSCorpora.getVue(corpus);

		javax.swing.SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				try {
					//System.out.println("Show editor...");
					//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					VisuVue visuVue = VisuVue.newVisuVue(null, vue);
					visuVue.setExtendedState(VisuMain.NORMAL);

					Point locFenetre = new Point(50, 50);
					Dimension dimFenetre = new Dimension(500, 500);
					visuVue.setLocation(locFenetre);
					visuVue.setSize(dimFenetre);

					visuVue.setVisible(true);
				}
				catch (Throwable ex) {
					GMessages.erreurFatale(ex);
				}
			}
		});

		return null;
	}
}
