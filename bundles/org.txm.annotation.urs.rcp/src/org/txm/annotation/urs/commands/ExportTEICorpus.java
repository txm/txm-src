package org.txm.annotation.urs.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.kohsuke.args4j.Option;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.export.URSAnnotationTEIExporter;
import org.txm.annotation.urs.messages.Messages;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

import visuAnalec.donnees.Corpus;
import visuAnalec.vue.Vue;

/**
 * Export the Analec annotation of the selected Corpus
 * 
 * @author mdecorde
 *
 */
public class ExportTEICorpus extends AbstractHandler {

	@Option(name = "outputDirectory", usage = "zip the result", widget = "Folder", required = true, def = "outputdir")
	File outputDirectory;

	@Option(name = "createArchive", usage = "zip the result", widget = "Boolean", required = true, def = "true")
	Boolean createArchive;

	@Option(name = "copyXMLTXMFiles", usage = "Copy the XML Files in the 'texts' directory", widget = "Boolean", required = true, def = "false")
	Boolean copyXMLTXMFiles;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			// Open the parameters input dialog box
			if (!ParametersDialog.open(this)) return null;

			Object first = CorporaView.getFirstSelectedObject();
			if (!(first instanceof org.txm.searchengine.cqp.corpus.CQPCorpus)) {
				Log.warning(NLS.bind(Messages.ExportTEICorpus_0, first));
				return null;
			}

			final MainCorpus mainCorpus = ((org.txm.searchengine.cqp.corpus.CQPCorpus) first).getMainCorpus();
			final Corpus analecCorpus = URSCorpora.getCorpus(mainCorpus);
			final Vue vue = URSCorpora.getVue(mainCorpus);
			outputDirectory.mkdirs();
			JobHandler job = new JobHandler(NLS.bind(Messages.ExportTEICorpus_1P0, mainCorpus.getName())) {

				@Override
				protected IStatus _run(SubMonitor monitor) throws Exception {
					try {
						if (!export(true, outputDirectory, createArchive, monitor, mainCorpus, analecCorpus, vue, copyXMLTXMFiles)) {
							Log.warning(Messages.ExportTEICorpus_2);
							return Status.CANCEL_STATUS;
						}
						else {
							return Status.OK_STATUS;
						}
					}
					catch (Exception e) {
						Log.warning(NLS.bind(Messages.ExportTEICorpus_3, e.getLocalizedMessage()));
						throw e;
					}
				}
			};
			job.schedule();

		}
		catch (Exception e) {
			Log.warning(NLS.bind(Messages.ExportTEICorpus_3, e.getLocalizedMessage()));
			Log.printStackTrace(e);
		}
		return null;
	}

	public static boolean export(boolean standoffAnnotations, File outputDirectory, boolean createArchive, IProgressMonitor monitor, MainCorpus mainCorpus, Corpus analecCorpus, Vue vue,
			boolean copyXMLTXMFiles) throws Exception {
		String name = mainCorpus.getID();
		File finalOutputDirectory = new File(outputDirectory, name);
		int n = 2;
		while (finalOutputDirectory.exists()) {
			finalOutputDirectory = new File(outputDirectory, name + " (" + n + ")"); //$NON-NLS-1$ //$NON-NLS-2$
			n++;
		}
		finalOutputDirectory.mkdirs();
		URSAnnotationTEIExporter exporter = new URSAnnotationTEIExporter(monitor, finalOutputDirectory, mainCorpus, analecCorpus, vue, copyXMLTXMFiles);
		boolean ret = exporter.process(standoffAnnotations, createArchive);

		return ret;
	}
}
