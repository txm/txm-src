package org.txm.annotation.urs.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.annotation.rcp.editor.AnnotationExtension;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.annotation.urs.view.ElementPropertiesView;
import org.txm.annotation.urs.view.ElementSearchView;
import org.txm.edition.rcp.editors.SynopticEditionEditor;
import org.txm.rcp.TXMWindows;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

import visuAnalec.donnees.Corpus;

public class RestoreAnnotations extends AbstractHandler {

	/** The ID. */
	public static String ID = RestoreAnnotations.class.getSimpleName(); //$NON-NLS-1$

	Corpus corpus;

	/** The selection. */

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object first = CorporaView.getFirstSelectedObject();
		if (!(first instanceof org.txm.searchengine.cqp.corpus.CQPCorpus)) {
			Log.warning(NLS.bind(Messages.SaveCorpus_0, first));
			return null;
		}
		final MainCorpus mainCorpus = ((org.txm.searchengine.cqp.corpus.CQPCorpus) first).getMainCorpus();
		if (restore(mainCorpus, event)) {
			Log.info(Messages.annotationsRestored);
		}
		else {
			Log.info(Messages.noAnnotationtoRestore);
		}

		return null;
	}

	/**
	 * 
	 * @param mainCorpus
	 * @param event
	 * @return true is annotations have been modified
	 */
	public static boolean restore(final MainCorpus mainCorpus, ExecutionEvent event) {


		Corpus acorpus = URSCorpora.getCorpus(mainCorpus);
		if (!acorpus.isModifie()) return false; // nothing to do

		acorpus.fermer();
		URSCorpora.revert(mainCorpus);

		mainCorpus.setIsModified(false);

		if (event != null) {

			HandlerUtil.getActiveShell(event).getDisplay().syncExec(new Runnable() {

				@Override
				public void run() {

					CorporaView.refreshObject(mainCorpus);

					ElementPropertiesView.closeView();

					ElementSearchView.closeView();

					// find out which editor is using the MainCorpus
					for (IWorkbenchPage page : TXMWindows.getActiveWindow().getPages()) {
						for (IEditorReference editor_ref : page.getEditorReferences()) {
							IEditorPart editor = editor_ref.getEditor(false);
							if (editor != null && editor instanceof SynopticEditionEditor) {
								SynopticEditionEditor seditor = ((SynopticEditionEditor) editor);
								if (seditor.getTXMEditorExtensions(AnnotationExtension.class).size() > 0) {
									if (mainCorpus.equals(seditor.getCorpus())) {
										seditor.setDirty(false);
										seditor.close();
									}
								}
							}
						}
					}
				}
			});
		}
		return true;
	}
}
