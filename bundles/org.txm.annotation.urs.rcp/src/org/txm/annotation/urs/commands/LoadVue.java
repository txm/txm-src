package org.txm.annotation.urs.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

import visuAnalec.fichiers.FichiersJava;
import visuAnalec.vue.Vue;

public class LoadVue extends AbstractHandler {

	/** The ID. */
	public static String ID = "org.txm.annotation.urs.commands.LoadAnnotationStructure"; //$NON-NLS-1$

	/** The selection. */

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object first = CorporaView.getFirstSelectedObject();
		if (!(first instanceof org.txm.searchengine.cqp.corpus.CQPCorpus)) {
			Log.warning(NLS.bind(Messages.LoadVue_0, first));
			return null;
		}

		MainCorpus mainCorpus = ((org.txm.searchengine.cqp.corpus.CQPCorpus) first).getMainCorpus();

		load(mainCorpus);
		return null;
	}

	public boolean load(MainCorpus mainCorpus) {
		Log.info(NLS.bind(Messages.LoadVue_1, mainCorpus.getName()));
		Vue vue = URSCorpora.getVue(mainCorpus);
		if (!FichiersJava.ouvrirVue(vue)) {
			Log.warning(Messages.LoadVue_2);
			return false;
		}

		Log.info(Messages.LoadVue_3);
		return true;
	}
}
