package org.txm.annotation.urs.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.kohsuke.args4j.Option;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

import visuAnalec.donnees.Corpus;
import visuAnalec.fichiers.FichiersJava;

public class LoadCorpus extends AbstractHandler {

	/** The ID. */
	public static String ID = "org.txm.annotation.urs.commands.LoadAnnotationStructure"; //$NON-NLS-1$

	@Option(name = "ecfile", usage = "an example file", widget = "File", required = true, def = "text.ec")
	File ecfile;


	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object first = CorporaView.getFirstSelectedObject();
		if (!(first instanceof org.txm.searchengine.cqp.corpus.CQPCorpus)) {
			Log.warning(NLS.bind(Messages.LoadCorpus_0, first));
			return null;
		}

		MainCorpus mainCorpus = ((org.txm.searchengine.cqp.corpus.CQPCorpus) first).getMainCorpus();
		Corpus corpus = URSCorpora.getCorpus(mainCorpus);

		Log.info(Messages.LoadCorpus_1);

		if (!ParametersDialog.open(this)) return null;

		load(ecfile, corpus);

		return null;
	}

	public static boolean load(File ecfile, Corpus corpus) {
		if (!(ecfile.exists())) {
			Log.warning(Messages.LoadCorpus_2);
			return false;
		}
		File fichierCorpus = ecfile;
		Log.info(NLS.bind(Messages.LoadCorpus_3, fichierCorpus));
		if (!FichiersJava.ouvrirCorpus(corpus, fichierCorpus, false)) {
			Log.warning(Messages.LoadCorpus_4);
			return false;
		}

		Log.info(Messages.LoadCorpus_5);
		return true;
	}
}
