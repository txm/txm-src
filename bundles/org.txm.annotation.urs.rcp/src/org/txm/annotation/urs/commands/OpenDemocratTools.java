package org.txm.annotation.urs.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.txm.Toolbox;
import org.txm.annotation.urs.messages.Messages;
import org.txm.rcp.views.fileexplorer.MacroExplorer;
import org.txm.utils.logger.Log;

public class OpenDemocratTools extends AbstractHandler {

	/** The ID. */
	public static String ID = OpenDemocratTools.class.getName(); //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		return open();
	}

	public static boolean open() {
		try {
			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			MacroExplorer view = (MacroExplorer) page.findView(MacroExplorer.ID);
			// close Element View if opened
			if (view == null) {
				view = (MacroExplorer) page.showView(MacroExplorer.ID);
				page.activate(view);
			}

			if (view == null) {
				return false;
			}

			try {
				File ursDirectory = new File(Toolbox.getTxmHomePath(), "scripts/groovy/user/org/txm/macro/urs/democrat"); //$NON-NLS-1$
				view.getTreeViewer().setSelection(new StructuredSelection(ursDirectory));
				view.getTreeViewer().expandToLevel(ursDirectory, TreeViewer.ALL_LEVELS);
			}
			catch (Throwable tr) {
				Log.info(Messages.bind(Messages.democratMacrosShouldBeIntheP0subdirectory, "org/txm/macro/urs/democrat")); //$NON-NLS-1$
			}
			return false;
		}
		catch (PartInitException e1) {
			Log.warning(NLS.bind(Messages.error_errorP0, e1.getLocalizedMessage()));
			Log.printStackTrace(e1);
		}
		return false;
	}
}
