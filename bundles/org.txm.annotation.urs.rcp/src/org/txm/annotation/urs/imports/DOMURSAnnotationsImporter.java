package org.txm.annotation.urs.imports;

import java.io.File;
import java.io.FilenameFilter;
import java.util.HashMap;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.osgi.util.NLS;
import org.txm.annotation.urs.messages.Messages;
import org.txm.importer.PersonalNamespaceContext;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;
import org.txm.utils.xml.DomUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import visuAnalec.donnees.Corpus;
import visuAnalec.vue.Vue;

public class DOMURSAnnotationsImporter {

	MainCorpus mainCorpus;

	Corpus analecCorpus;

	File annotationsDirectory;

//	private Vue analecVue;
//
//	private IProgressMonitor monitor;
//
//	private XMLInputFactory factory;

	protected static PersonalNamespaceContext Nscontext = new PersonalNamespaceContext();

	public DOMURSAnnotationsImporter(IProgressMonitor monitor, File outputDirectory, MainCorpus mainCorpus, Corpus analecCorpus, Vue analecVue) {

		this.annotationsDirectory = outputDirectory;
		this.mainCorpus = mainCorpus;
		this.analecCorpus = analecCorpus;
//		this.analecVue = analecVue;
//		this.monitor = monitor;
	}

	public boolean process() throws Exception {

		if (!annotationsDirectory.exists()) return false;
		if (!annotationsDirectory.isDirectory()) return false;

		File[] ursFiles = annotationsDirectory.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.matches(".+-urs\\.xml"); //$NON-NLS-1$
			}
		});

		if (ursFiles == null || ursFiles.length == 0) {
			Log.warning(NLS.bind(Messages.noXMLFileFoundInP0, annotationsDirectory));
			return false;
		}

//		factory = XMLInputFactory.newInstance();

		for (File xmlAnalec : ursFiles) {

			String textid = xmlAnalec.getName().substring(0, xmlAnalec.getName().length() - 8);
			Log.info(NLS.bind(Messages.processingTextP0, textid));
			processText(textid, xmlAnalec);
		}

		return true;
	}

	private void processText(String textid, File xmlAnalec) throws Exception {
//		AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();

		HashMap<String, HashMap<String, String>> elementProperties = new HashMap<String, HashMap<String, String>>();

		Document doc = DomUtils.load(xmlAnalec);
		NodeList textElements = doc.getElementsByTagName("stdf"); //$NON-NLS-1$
		for (int iText = 0; iText < textElements.getLength();) {
			Element text = (Element) textElements.item(iText);

			NodeList backElements = text.getElementsByTagName("annotations"); //$NON-NLS-1$
			for (int iBack = 0; iBack < backElements.getLength();) {
				Element back = (Element) backElements.item(iBack);

				NodeList divElements = text.getElementsByTagName("div"); //$NON-NLS-1$
				for (int ifvLib = 0; ifvLib < divElements.getLength(); ifvLib++) {
					Element divElement = (Element) divElements.item(ifvLib);
					String n = divElement.getAttribute("type"); //$NON-NLS-1$
					if (n == null) n = ""; //$NON-NLS-1$
					if (n.endsWith("-fs")) { //$NON-NLS-1$
						getElementPropertiesFromFvLibElement(elementProperties, divElement);
					}
				}

				NodeList children = back.getElementsByTagName("spanGrp"); //$NON-NLS-1$
				for (int iChild = 0; iChild < children.getLength(); iChild++) {
					Element spanGrpElement = (Element) children.item(iChild);
					String type = spanGrpElement.getAttribute("type"); //$NON-NLS-1$
					String n = spanGrpElement.getAttribute("subtype"); //$NON-NLS-1$
					if (type.equals("Unit")) { //$NON-NLS-1$
						getUnitesFromSpanGrp(spanGrpElement, n);
					}
				}

				children = back.getElementsByTagName("joinGrp"); //$NON-NLS-1$
				for (int iChild = 0; iChild < children.getLength(); iChild++) {
					Element joinGrpElement = (Element) children.item(iChild);
					String type = joinGrpElement.getAttribute("type"); //$NON-NLS-1$
					String n = joinGrpElement.getAttribute("subtype"); //$NON-NLS-1$
					if (type.equals("Relation")) { //$NON-NLS-1$
						getRelationsFromJoinGrp(joinGrpElement, n);
					}
				}

				children = back.getElementsByTagName("joinGrp"); //$NON-NLS-1$
				for (int iChild = 0; iChild < children.getLength(); iChild++) {
					Element joinGrpElement = (Element) children.item(iChild);
					String type = joinGrpElement.getAttribute("type"); //$NON-NLS-1$
					String n = joinGrpElement.getAttribute("subtype"); //$NON-NLS-1$
					if (type.equals("Schema")) { //$NON-NLS-1$
						getSchemasFromJoinGrp(joinGrpElement, n);
					}
				}

				break; // stop at first back
			}

			break; // stop at first text
		}
	}

	private void getSchemasFromJoinGrp(Element joinGrpElement, String n) {
		NodeList children = joinGrpElement.getElementsByTagName("join"); //$NON-NLS-1$
		for (int iChild = 0; iChild < children.getLength(); iChild++) {
//			Element joinElement = (Element) children.item(iChild);
//			String id = joinElement.getAttribute("id"); //$NON-NLS-1$
//			String target = joinElement.getAttribute("target"); //$NON-NLS-1$
//			String ana = joinElement.getAttribute("ana"); //$NON-NLS-1$
		}
	}

	private void getRelationsFromJoinGrp(Element joinGrpElement, String n) {
		// TODO Auto-generated method stub

	}

	private void getUnitesFromSpanGrp(Element spanGrpElement, String n) {
		// TODO Auto-generated method stub

	}

	private void getElementPropertiesFromFvLibElement(HashMap<String, HashMap<String, String>> elementProperties, Element divElement) {
		NodeList fsElements = divElement.getElementsByTagName("fs"); //$NON-NLS-1$
		for (int iChild = 0; iChild < fsElements.getLength(); iChild++) {
			Element fsElement = (Element) fsElements.item(iChild);
			String id = fsElement.getAttribute("id"); //$NON-NLS-1$

			if (elementProperties.containsKey(id)) {
				Log.warning(NLS.bind(Messages.warningDuplicatedfsElementWithIDP0, id));
			}
			else {
				HashMap<String, String> properties = new HashMap<String, String>();
				elementProperties.put(id, properties);
				NodeList fElements = fsElement.getElementsByTagName("f"); //$NON-NLS-1$
				for (int iChild2 = 0; iChild2 < fElements.getLength(); iChild2++) {
					Element fElement = (Element) fElements.item(iChild2);
					String name = fElement.getAttribute("name"); //$NON-NLS-1$
					if (properties.containsKey(name)) {
						Log.warning(NLS.bind(Messages.warningDuplicatedfElementNameP0InP1fsElement, name, id));
					}
					else {
						properties.put(name, fElement.getTextContent().trim());
					}
				}
			}
		}
	}
}
