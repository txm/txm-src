package org.txm.annotation.urs.imports;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.osgi.util.NLS;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.importer.PersonalNamespaceContext;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.logger.Log;

import visuAnalec.donnees.Corpus;
import visuAnalec.donnees.Structure;
import visuAnalec.elements.Relation;
import visuAnalec.elements.Schema;
import visuAnalec.elements.Unite;
import visuAnalec.fichiers.FichiersGlozz;
import visuAnalec.fichiers.FichiersJava;
import visuAnalec.vue.Vue;

public class URSAnnotationsImporter {

	protected MainCorpus mainCorpus;

	protected Corpus analecCorpus;

	protected File annotationsDirectory;

	protected Vue analecVue;

	protected IProgressMonitor monitor;

	protected XMLInputFactory factory;

	protected File aamFile;

	protected File evFile;

	protected static PersonalNamespaceContext Nscontext = new PersonalNamespaceContext();

	public URSAnnotationsImporter(IProgressMonitor monitor, File outputDirectory, File aamFile, File evFile, MainCorpus mainCorpus, Corpus analecCorpus, Vue analecVue) {
		this.annotationsDirectory = outputDirectory;
		this.aamFile = aamFile;
		this.evFile = evFile;
		this.mainCorpus = mainCorpus;
		this.analecCorpus = analecCorpus;
		this.analecVue = analecVue;
		this.monitor = monitor;
	}

	public boolean process() throws Exception {

		if (!annotationsDirectory.exists()) {
			Log.warning(NLS.bind(Messages.errorAnnotationsDirectoryDoesNotExistP0, annotationsDirectory.getAbsolutePath())); //$NON-NLS-1$
			return false;
		}
		if (!annotationsDirectory.isDirectory()) {
			Log.warning(NLS.bind(Messages.errorAnnotationsDirectoryIsNotADirectoryP0, annotationsDirectory.getAbsolutePath())); //$NON-NLS-1$
			return false;
		}

		if (aamFile == null || !aamFile.exists() || aamFile.isDirectory()) {
			Log.warning(NLS.bind(Messages.warningAamFileDoesNotExistP0, aamFile)); //$NON-NLS-1$
		}
		else {
			if (!FichiersGlozz.importerModeleGlozz(analecCorpus, aamFile)) {
				Log.warning(NLS.bind(Messages.errorWhileImportingGlozzModelP0, aamFile)); //$NON-NLS-1$
				return false;
			}
		}

		if (evFile == null || !evFile.exists() || evFile.isDirectory()) {
			Log.warning(NLS.bind(Messages.warningevFileDoesNotExistP0, evFile)); //$NON-NLS-1$
		}
		else {
			Vue vue = URSCorpora.getVue(analecCorpus);
			if (!FichiersJava.ouvrirVue(vue, evFile)) {
				Log.warning(NLS.bind(Messages.errorWhileImportingViewP0, evFile)); //$NON-NLS-1$
				return false;
			}
		}

		File[] ursFiles = annotationsDirectory.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.matches(".+-urs\\.xml"); //$NON-NLS-1$
			}
		});

		if (ursFiles == null || ursFiles.length == 0) {
			Log.warning(NLS.bind(Messages.noXMLFileFoundInP0, annotationsDirectory)); //$NON-NLS-1$
			return false;
		}
		Arrays.sort(ursFiles);

		factory = XMLInputFactory.newInstance();
		List<String> cqpTextIds = Arrays.asList(mainCorpus.getCorpusTextIdsList());

		List<Integer> all_result_summary = Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0);

		if (monitor != null) monitor.subTask(NLS.bind(Messages.processingP0Texts3Dot, ursFiles.length)); //$NON-NLS-1$
		for (File xmlTXMFile : ursFiles) {
			String textid = xmlTXMFile.getName().substring(0, xmlTXMFile.getName().length() - 8);
			Log.warning(NLS.bind(Messages.processingP0Text3Dot, textid)); //$NON-NLS-1$
			if (cqpTextIds.contains(textid)) {
				if (monitor != null) monitor.subTask(NLS.bind(Messages.processingP0Text3Dot, textid)); //$NON-NLS-1$
				// N unit, N unit error, N unit no match error, N unit too much match error, N Relation, N Relation error, N Schema, N Schema error
				List<Integer> result_summary = processText(textid, xmlTXMFile);

				Log.info(NLS.bind(Messages.P0ImportReport, textid)); //$NON-NLS-1$
				Log.info(NLS.bind(Messages.nUnitsAddedP0, result_summary.get(0))); //$NON-NLS-1$
				Log.info(NLS.bind(Messages.nUnitsErrorP0, result_summary.get(1))); //$NON-NLS-1$
				Log.info(NLS.bind(Messages.nUnitsNoMatchErrorP0, result_summary.get(2))); //$NON-NLS-1$
				Log.info(NLS.bind(Messages.nUnitsTooMuchMatchErrorP0, result_summary.get(3))); //$NON-NLS-1$
				Log.info(NLS.bind(Messages.nRelationsAddedP0, result_summary.get(4))); //$NON-NLS-1$
				Log.info(NLS.bind(Messages.nRelationsErrorP0, result_summary.get(5))); //$NON-NLS-1$
				Log.info(NLS.bind(Messages.nSchemasAddedP0, result_summary.get(6))); //$NON-NLS-1$
				Log.info(NLS.bind(Messages.nSchemasErrorP0, result_summary.get(7))); //$NON-NLS-1$

				for (int i = 0; i < all_result_summary.size(); i++) {
					all_result_summary.set(i, all_result_summary.get(i) + result_summary.get(i));
				}
			}
			else {
				Log.warning(NLS.bind(Messages.warningCannotFoundTextWithIDP0InCurrentCQPCorpus, textid)); //$NON-NLS-1$
			}

			if (monitor != null && monitor.isCanceled()) {
				return false;
			}
		}

		if (!aamFile.exists()) {
			Vue vue = URSCorpora.getVue(analecCorpus);
			vue.retablirVueParDefaut();
		}

		Log.info(Messages.finalImportReport); //$NON-NLS-1$"
		Log.info(NLS.bind(Messages.nUnitsAddedP0, all_result_summary.get(0))); //$NON-NLS-1$
		Log.info(NLS.bind(Messages.nUnitsErrorP0, all_result_summary.get(1))); //$NON-NLS-1$
		Log.info(NLS.bind(Messages.nUnitsNoMatchErrorP0, all_result_summary.get(2))); //$NON-NLS-1$
		Log.info(NLS.bind(Messages.nUnitsTooMuchMatchErrorP0, all_result_summary.get(3))); //$NON-NLS-1$
		Log.info(NLS.bind(Messages.nRelationsAddedP0, all_result_summary.get(4))); //$NON-NLS-1$
		Log.info(NLS.bind(Messages.nRelationsErrorP0, all_result_summary.get(5))); //$NON-NLS-1$
		Log.info(NLS.bind(Messages.nSchemasAddedP0, all_result_summary.get(6))); //$NON-NLS-1$
		Log.info(NLS.bind(Messages.nSchemasErrorP0, all_result_summary.get(7))); //$NON-NLS-1$

		return true;
	}

	private int getPosition(Subcorpus textSubcorpus, HashMap<String, int[]> id2position, String id) {
		int[] positions = id2position.get(id);
		int start = textSubcorpus.getMatches().get(0).getStart();
		int end = textSubcorpus.getMatches().get(0).getEnd();
		if (positions.length == 0) { // no word for id=deb
			return -1;
		}

		for (int p : positions) {
			if (start <= p && p <= end) {
				return p;
			}
		}

		return -2;
	}

	private List<Integer> processText(String textid, File xmlTXMFile) throws Exception {
		AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();
		// N unit, N unit error, N unit no match error, N unit too much match error, N Relation, N Relation error, N Schema, N Schema error
		int nUnitsAdded = 0;
		int nUnitsError = 0;
		int nUnitsNoMatchError = 0;
		int nUnitsTooMuchMatchError = 0;
		int nRelationsAdded = 0;
		int nRelationsError = 0;
		int nSchemaAdded = 0;
		int nSchemaError = 0;

		unites.clear();
		relations.clear();
		schemas.clear();
		elementProperties.clear();

		if (!parseXMLTXMFile(xmlTXMFile)) return null; // fill unites, relations, schemas and elementProperties

		// if (unites.size() > 0) System.out.println(unites);
		// if (relations.size() > 0) System.out.println(relations);
		// if (schemas.size() > 0) System.out.println(schemas);
		// if (elementProperties.size() > 0) System.out.println(elementProperties);
		// for (String u : unites.keySet()) if (!elementProperties.containsKey(u+"-fs")) System.out.println("MISSIGN ELEM PROPERTIES: "+u);
		// for (String u : relations.keySet()) if (!elementProperties.containsKey(u+"-fs")) System.out.println("MISSIGN ELEM PROPERTIES: "+u);
		// for (String u : schemas.keySet()) if (!elementProperties.containsKey(u+"-fs")) System.out.println("MISSIGN ELEM PROPERTIES: "+u);

		CQLQuery textQuery = new CQLQuery("[_.text_id=\"" + CQLQuery.addBackSlash(textid) + "\"] expand to text"); //$NON-NLS-1$ //$NON-NLS-2$ $NON-NLS-2$
		Subcorpus textSubCorpus = mainCorpus.createSubcorpus(textQuery, "TXTTMP"); // $NON-NLS-1$ //$NON-NLS-1$
		textSubCorpus.compute();

		HashSet<String> ids = new HashSet<>();
		for (String id : unites.keySet()) {
			ids.add(unites.get(id)[2]);
			ids.add(unites.get(id)[3]);
		}
		String[] idsArray = ids.toArray(new String[ids.size()]);
		int[] idsIds = CQI.str2Id(mainCorpus.getProperty("id").getQualifiedName(), idsArray); //$NON-NLS-1$

		HashMap<String, int[]> id2position = new HashMap<>();
		for (int i = 0; i < idsArray.length; i++) {
			int[] positions = CQI.id2Cpos(mainCorpus.getProperty("id").getQualifiedName(), idsIds[i]); //$NON-NLS-1$
			id2position.put(idsArray[i], positions);
		}

		Structure structure = analecCorpus.getStructure();
		HashSet<String> unitesStructure = structure.getUnites();
		HashSet<String> relationsStructure = structure.getRelations();
		HashSet<String> schemasStructure = structure.getSchemas();

		ConsoleProgressBar cpb = new ConsoleProgressBar(unites.size());
		for (String id : unites.keySet()) {
			cpb.tick();
			String ana = unites.get(id)[0];
			String type = unites.get(id)[1];
			if (!unitesStructure.contains(type)) {
				structure.ajouterType(Unite.class, type);
			}
			String deb = unites.get(id)[2];
			String fin = unites.get(id)[3];
			if (elementProperties.containsKey(ana)) {

				int start = getPosition(textSubCorpus, id2position, deb);
				int end = getPosition(textSubCorpus, id2position, fin);

				if (start < 0) {
					nUnitsError++;
					if (start == -1) {
						Log.warning(NLS.bind(Messages.warningNoPositionFoundForWordIdP0, deb)); //$NON-NLS-1$
						nUnitsNoMatchError++;
					}
					else {
						Log.warning(NLS.bind(Messages.warningTooManyPositionsFoundForWordIdP0, deb)); //$NON-NLS-1$
						nUnitsTooMuchMatchError++;
					}
				}
				else if (end < 0) {
					nUnitsError++;
					if (end == -1) {
						Log.warning(NLS.bind(Messages.warningNoPositionFoundForWordIdP0, fin)); //$NON-NLS-1$
						nUnitsNoMatchError++;
					}
					else {
						Log.warning(NLS.bind(Messages.warningTooManyPositionsFoundForWordIdP0, fin)); //$NON-NLS-1$
						nUnitsTooMuchMatchError++;
					}
				}
				else { // OK

					// System.out.println("create unite: "+type+" ["+deb+", "+fin+"]");
					Unite unite = analecCorpus.addUniteSaisie(type, start, end);
					HashMap<String, String> props = elementProperties.get(ana);
					HashSet<String> nomsProps = structure.getNomsProps(Unite.class, type);
					for (String prop : props.keySet()) {
						if (!nomsProps.contains(prop)) {
							structure.ajouterProp(Unite.class, type, prop);
						}
						unite.putProp(prop, props.get(prop));
					}
					unitesRef.put(id, unite);
					nUnitsAdded++;
				}
			}
			else {
				Log.warning(NLS.bind(Messages.warningNoPropertiesFoundForElementIdP0andAnaP1, id, ana)); //$NON-NLS-1$
			}
		}
		cpb.done();

		cpb = new ConsoleProgressBar(relations.size());
		for (String id : relations.keySet()) {
			cpb.tick();
			String ana = relations.get(id)[0];
			String type = relations.get(id)[1];
			if (!relationsStructure.contains(type)) {
				structure.ajouterType(Relation.class, type);
			}
			String target = relations.get(id)[2];

			if (elementProperties.containsKey(ana)) {

				String[] wordsref = target.split(" "); //$NON-NLS-1$
				String[] wordsid = new String[wordsref.length];
				for (int i = 0; i < wordsref.length; i++)
					wordsid[i] = wordsref[i].substring(1);
				try {
					Unite elt1 = unitesRef.get(wordsid[0]);
					Unite elt2 = unitesRef.get(wordsid[1]);
					if (elt1 != null && elt2 != null) {
						// System.out.println("create relation: "+type+" ["+deb+", "+fin+"]");

						Relation relation = analecCorpus.addRelationSaisie(type, elt1, elt2);
						HashMap<String, String> props = elementProperties.get(ana);
						HashSet<String> nomsProps = structure.getNomsProps(Relation.class, type);
						for (String prop : props.keySet()) {
							if (!nomsProps.contains(prop)) {
								structure.ajouterProp(Relation.class, type, prop);
							}
							relation.putProp(prop, props.get(prop));
						}
						nRelationsAdded++;
					}
					else {
						Log.warning(NLS.bind(Messages.errorRelationElementNotFoundP0, Arrays.toString(wordsref))); //$NON-NLS-1$
						nRelationsError++;
					}
				}
				catch (Exception e) {

				}
			}
			else {
				Log.warning(NLS.bind(Messages.warningNoPropertiesFoundforElementIdP0, id)); //$NON-NLS-1$
			}
		}
		cpb.done();

		cpb = new ConsoleProgressBar(schemas.size());
		for (String id : schemas.keySet()) {
			cpb.tick();
			String ana = schemas.get(id)[0];
			String type = schemas.get(id)[1];
			if (!schemasStructure.contains(type)) {
				structure.ajouterType(Schema.class, type);
			}
			String target = schemas.get(id)[2];
			if (elementProperties.containsKey(ana)) {
				String[] unitsref = target.split(" "); //$NON-NLS-1$
				try {
					// System.out.println("create relation: "+type+" ["+deb+", "+fin+"]");
					Schema schema = new Schema(type);
					for (String unitid : unitsref) {
						unitid = unitid.substring(1); // remove '#'
						if (unitesRef.containsKey(unitid)) {
							Unite unite = unitesRef.get(unitid);
							schema.ajouter(unite);
						}
						else {
							Log.warning(NLS.bind(Messages.warningMissingUnitIdP0, unitid)); //$NON-NLS-1$
							nSchemaError++;
						}
					}
					analecCorpus.addSchemaLu(schema);
					HashMap<String, String> props = elementProperties.get(ana);
					HashSet<String> nomsProps = structure.getNomsProps(Schema.class, type);
					for (String prop : props.keySet()) {
						if (!nomsProps.contains(prop)) {
							structure.ajouterProp(Schema.class, type, prop);
						}
						schema.putProp(prop, props.get(prop));
					}
					nSchemaAdded++;
				}
				catch (Exception e) {
					Log.warning(NLS.bind(Messages.errorWhileCreatingSchemaWithIdP0, id)); //$NON-NLS-1$
				}
			}
			else {
				Log.warning(NLS.bind(Messages.warningNoPropertiesFoundForElementIdP0, id)); //$NON-NLS-1$
			}
		}
		cpb.done();

		textSubCorpus.delete();
		return Arrays.asList(nUnitsAdded, nUnitsError, nUnitsNoMatchError, nUnitsTooMuchMatchError,
				nRelationsAdded, nRelationsError,
				nSchemaAdded, nSchemaError);
	}

	private boolean parseXMLTXMFile(File xmlTXMFile) throws XMLStreamException, MalformedURLException, IOException {
		InputStream inputData = xmlTXMFile.toURI().toURL().openStream();
		factory = XMLInputFactory.newInstance();
		XMLStreamReader parser = factory.createXMLStreamReader(inputData);

		String currentType = null;
		String currentN = null;
		String currentAna = null;
		String currentName = null;
		String currentPropValue = null;

		int processMode = 0; // 0 nothing, 1 elements 2 properties

		if (!goToStandOff(parser)) {
			Log.warning(NLS.bind(Messages.errorCannotFindTheStandOffElementInP0, xmlTXMFile)); //$NON-NLS-1$
			return false;
		}
		String localname = null;
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();

					if (localname.equals("annotationGrp")) { //$NON-NLS-1$
						currentType = parser.getAttributeValue(null, "type"); //$NON-NLS-1$
						currentN = parser.getAttributeValue(null, "subtype"); //$NON-NLS-1$
						if (currentType != null && currentN != null) {
							processMode = 1;
						}
						else {
							Log.warning(NLS.bind(Messages.warningFoundP0WithoutTypeAndNAttribute, localname)); //$NON-NLS-1$
							processMode = 0;
						}
					}
					else if (localname.equals("div")) { //$NON-NLS-1$
						String type = parser.getAttributeValue(null, "type"); //$NON-NLS-1$
						if (type == null) type = ""; //$NON-NLS-1$
						if (type.endsWith("-fs")) //$NON-NLS-1$
							processMode = 2;
					}

					if (processMode == 1) {
						if (localname.equals("span") || localname.equals("link")) { //$NON-NLS-1$ //$NON-NLS-2$ $NON-NLS-2$
							String id = parser.getAttributeValue(null, "id"); //$NON-NLS-1$
							String ana = parser.getAttributeValue(null, "ana"); //$NON-NLS-1$
							// System.out.println("SPAN id="+id +" ana="+ana+" currentType="+currentType+" currentN="+currentN);
							if ("Unit".equals(currentType)) { //$NON-NLS-1$
								String from = parser.getAttributeValue(null, "from"); //$NON-NLS-1$
								String to = parser.getAttributeValue(null, "to"); //$NON-NLS-1$
								registerUnite(id, ana, currentN, from, to);
							}
							else if ("Relation".equals(currentType)) { //$NON-NLS-1$
								String target = parser.getAttributeValue(null, "target"); //$NON-NLS-1$
								registerRelation(id, ana, currentN, target);
							}
							else if ("Schema".equals(currentType)) { //$NON-NLS-1$
								String target = parser.getAttributeValue(null, "target"); //$NON-NLS-1$
								registerSchema(id, ana, currentN, target);
							}
						}
					}
					else if (processMode == 2) {
						if (localname.equals("fs")) { //$NON-NLS-1$
							currentAna = parser.getAttributeValue(null, "id"); //$NON-NLS-1$
							if (elementProperties.get(currentAna) != null) Log.warning(NLS.bind(Messages.warningDuplicatedElementPropertiesP0, currentAna)); //$NON-NLS-1$
							elementProperties.put(currentAna, new HashMap<String, String>());
						}
						else if (localname.equals("f")) { //$NON-NLS-1$
							currentName = parser.getAttributeValue(null, "name"); //$NON-NLS-1$
							currentPropValue = ""; //$NON-NLS-1$
						}
					}
					else {
						// nothing to do
					}

					break;
				case XMLStreamConstants.CHARACTERS:
					if (processMode == 2 && currentAna != null && currentName != null) {
						currentPropValue += parser.getText();
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName();
					if (localname.equals("standOff")) { // stop all //$NON-NLS-1$
						parser.close();
						inputData.close();
						return true;
					}
					else if (processMode == 1 && localname.equals("annotationGrp")) { // stop all //$NON-NLS-1$
						currentType = null;
						currentN = null;
						processMode = 0;
					}
					else if (processMode == 2 && localname.equals("div")) { //$NON-NLS-1$
						processMode = 0;
					}

					if (processMode == 1) {

					}
					else if (processMode == 2) {
						if (localname.equals("fs")) { //$NON-NLS-1$
							currentAna = null;
						}
						else if (localname.equals("f")) { //$NON-NLS-1$
							if (currentName != null && currentAna != null) {
								elementProperties.get(currentAna).put(currentName, currentPropValue);
							}
							currentName = null;
						}
					}
					else {
						// nothing to do
					}

					break;

				case XMLStreamConstants.END_DOCUMENT:
					parser.close();
					inputData.close();
					return false;
			}
		}
		parser.close();
		inputData.close();

		return false; // standOff not found
	}

	HashMap<String, HashMap<String, String>> elementProperties = new HashMap<>();

	HashMap<String, String[]> unites = new HashMap<>(); // id -> {ana, type, from, to}

	HashMap<String, Unite> unitesRef = new HashMap<>(); // id -> Unite

	private void registerUnite(String id, String ana, String type, String from, String to) {
		String[] data = { ana.substring(1), type, from.substring(5), to.substring(5) };
		if (unites.containsKey(id)) {
			Log.warning(NLS.bind(Messages.warningDuplicatedUniteIdP0, id)); //$NON-NLS-1$
		}
		else {
			unites.put(id, data);
		}
	}

	HashMap<String, String[]> relations = new HashMap<>(); // id -> {ana, type, target}

	private void registerRelation(String id, String ana, String type, String target) {
		String[] data = { ana.substring(1), type, target };
		if (relations.containsKey(id)) {
			Log.warning(NLS.bind(Messages.warningDuplicatedRelationIdP0, id)); //$NON-NLS-1$
		}
		else {
			relations.put(id, data);
		}
	}

	HashMap<String, String[]> schemas = new HashMap<>(); // id -> {ana, type, target}

	private void registerSchema(String id, String ana, String type, String target) {
		String[] data = { ana.substring(1), type, target };
		// System.out.println("R schema: "+id+" : "+Arrays.toString(data));
		if (schemas.containsKey(id)) {
			Log.warning(NLS.bind(Messages.warningDuplicatedSchemaIdP0, id)); //$NON-NLS-1$
		}
		else {
			schemas.put(id, data);
		}
	}

	private boolean goToStandOff(XMLStreamReader parser) throws XMLStreamException {
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					String localname = parser.getLocalName();
					if (localname.equals("standOff")) return true; //$NON-NLS-1$
					break;
			}
		}
		return false;
	}
}
