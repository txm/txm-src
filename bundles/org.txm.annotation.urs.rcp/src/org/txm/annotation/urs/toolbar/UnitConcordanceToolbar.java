package org.txm.annotation.urs.toolbar;

import java.io.File;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.fieldassist.ComboContentAdapter;
import org.eclipse.jface.fieldassist.TXMAutoCompleteField;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.TypedEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableColumn;
import org.txm.annotation.rcp.editor.AnnotationArea;
import org.txm.annotation.rcp.editor.AnnotationExtension;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.core.functions.Line;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.concordance.rcp.editors.ConcordanceEditor.ConcordanceColumnSizeControlListener;
import org.txm.core.results.TXMResult;
import org.txm.objects.Match;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.TXMFontRegistry;
import org.txm.rcp.swt.provider.SimpleLabelProvider;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.utils.logger.Log;

import visuAnalec.donnees.Corpus;
import visuAnalec.elements.Unite;
import visuAnalec.vue.Vue;

public class UnitConcordanceToolbar extends AnnotationArea {

	/**
	 * the limit number of annotation when a confirm dialog box is shown
	 */
	protected static final int NALERTAFFECTANNOTATIONS = 100;

	public static final String EMPTYTEXT = ""; //$NON-NLS-1$

	protected ConcordanceEditor editor;

	/**
	 * All available annotation types
	 */
	protected List<String> typeValuesList;

	/**
	 * display the annotation values of the current shown concordance lines
	 */
	protected TableColumn annotationColumn;

	protected ComboViewer annotationTypesCombo;

	/** add a new type of annotation */
	private Button addAnnotationTypeButton;

	protected Label equalLabel;

	protected Combo annotationValuesText;

	protected Button affectAnnotationButton;

	protected Button deleteAnnotationButton;

	private Button affectAllAnnotationButton;

	protected Vector<String> typesList = new Vector<>();

	protected Concordance concordance;

	private CQPCorpus corpus;

	private TableViewerColumn annotationColumnViewer;

	private Font annotationColummnFont;

	private Font defaultColummnFont;

	private Corpus annotManager;

	private HashMap<Line, ArrayList<Unite>> annotations;

	protected String unitType;

	protected String unitProperty;

	private ComboViewer annotationPropertiesCombo;

	private Vue vue;

	@Override
	public String getName() {
		return Messages.annotateURSUnits; //$NON-NLS-1$
	}

	@Override
	public boolean allowMultipleAnnotations() {
		return true;
	}

	public UnitConcordanceToolbar() {
	}

	@Override
	public boolean install(TXMEditor<? extends TXMResult> txmeditor, AnnotationExtension ext, Composite parent, int position) throws Exception {

		this.extension = ext;
		this.editor = (ConcordanceEditor) txmeditor;
		this.concordance = this.editor.getResult();
		this.corpus = concordance.getCorpus();
		this.annotations = new HashMap<>();
		this.annotManager = URSCorpora.getCorpus(corpus);
		this.vue = URSCorpora.getVue(corpus);

		TableViewer viewer = this.editor.getTableViewer();

		// RESULT
		if (position < 0) {
			position = 3 + extension.getNumberOfAnnotationArea(); // after keyword column and previous annotation columns
		}

		if (annotManager.getStructure().isVide()) {
			annotManager.ajouterType(Unite.class, "Entity"); //$NON-NLS-1$
			annotManager.ajouterProp(Unite.class, "Entity", "Property"); //$NON-NLS-1$ //$NON-NLS-2$ $NON-NLS-2$
		}

		typesList.clear();

		for (String type : this.annotManager.getStructure().getUnites()) {
			typesList.add(type);
		}
		if (typesList.size() > 0) {
			this.unitType = typesList.get(0);
		}

		annotationColumnViewer = new TableViewerColumn(viewer, SWT.CENTER, position);
		annotationColumn = annotationColumnViewer.getColumn();
		annotationColumn.setText(Messages.unit); //$NON-NLS-1$
		annotationColumn.setToolTipText(Messages.unitURS); //$NON-NLS-1$
		annotationColumn.setAlignment(SWT.CENTER);
		annotationColumn.pack();
		annotationColumn.setResizable(true);
		annotationColumn.addControlListener(new ConcordanceColumnSizeControlListener(annotationColumn));
		defaultColummnFont = editor.getContainer().getFont();
		FontData fdata = defaultColummnFont.getFontData()[0];
		annotationColummnFont = TXMFontRegistry.getFont(Display.getCurrent(), fdata.getName(), fdata.getHeight(), SWT.ITALIC);
		annotationColumnViewer.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				Line line = (Line) element;
				ArrayList<Unite> a = annotations.get(line);
				if (a == null || a.size() == 0) return Messages.chevNoneChev; //$NON-NLS-1$

				StringBuffer buffer = new StringBuffer();
				for (Unite unite : a) {
					if (unitProperty.length() > 0) {
						String value = unite.getProp(unitProperty);

						if (value != null) {
							if (unite.getDeb() < line.matchGetStart() - line.getLeftContextSize()) {
								value = "… " + value; //$NON-NLS-1$
							}

							if (unite.getFin() > line.matchGetEnd() + line.getRightContextSize()) {
								value = value + " …"; //$NON-NLS-1$
							}
							if (buffer.length() > 0) buffer.append(", "); //$NON-NLS-1$
							buffer.append(value);
						}
					}
					else {
						buffer.append(unite.getProps().toString());
					}
				}
				return buffer.toString();
			}
		});

		annotationArea = new GLComposite(parent, SWT.NONE);
		annotationArea.getLayout().numColumns = 12;
		annotationArea.getLayout().horizontalSpacing = 2;
		annotationArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		GridData gdata = new GridData(SWT.CENTER, SWT.CENTER, false, false);
		gdata.widthHint = 90;

		Label typeLabel = new Label(annotationArea, SWT.NONE);
		typeLabel.setText("Type"); //$NON-NLS-1$
		typeLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));


		// UNIT TYPE
		annotationTypesCombo = new ComboViewer(annotationArea, SWT.SINGLE);
		annotationTypesCombo.setContentProvider(new ArrayContentProvider());
		annotationTypesCombo.setLabelProvider(new SimpleLabelProvider());
		annotationTypesCombo.getCombo().addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					String name = annotationTypesCombo.getCombo().getText();
					if (typesList.contains(name)) {
						StructuredSelection selection = new StructuredSelection(name);
						annotationTypesCombo.setSelection(selection, true);
					}
					else {
						createNewType(e, name);
					}
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		gdata = new GridData(SWT.CENTER, SWT.CENTER, false, false);
		gdata.widthHint = 200;

		annotationTypesCombo.getCombo().setLayoutData(gdata);
		annotationTypesCombo.setInput(typesList);
		annotationTypesCombo.getCombo().select(0);

		annotationTypesCombo.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				onAnnotationTypeSelected(event);
			}
		});

		// WITH

		equalLabel = new Label(annotationArea, SWT.NONE);
		equalLabel.setText("with"); //$NON-NLS-1$
		equalLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));

		// PROPERTY
		annotationPropertiesCombo = new ComboViewer(annotationArea, SWT.SINGLE | SWT.READ_ONLY);
		annotationPropertiesCombo.setContentProvider(new ArrayContentProvider());
		annotationPropertiesCombo.setLabelProvider(new SimpleLabelProvider());
		annotationPropertiesCombo.getCombo().addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					String name = annotationTypesCombo.getCombo().getText();
					if (typesList.contains(name)) {
						StructuredSelection selection = new StructuredSelection(name);
						annotationTypesCombo.setSelection(selection, true);
					}
					else {
						createNewType(e, name);
					}
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		gdata = new GridData(SWT.CENTER, SWT.CENTER, false, false);
		gdata.widthHint = 200;

		annotationPropertiesCombo.getCombo().setLayoutData(gdata);
		ArrayList<String> properties = new ArrayList<>();
		properties.add(""); // to manage the unit itself //$NON-NLS-1$
		properties.addAll(annotManager.getStructure().getNomsProps(Unite.class, unitType));
		annotationPropertiesCombo.setInput(properties);
		annotationPropertiesCombo.getCombo().select(0);

		annotationPropertiesCombo.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				onAnnotationPropertySelected(event);
			}
		});

		equalLabel = new Label(annotationArea, SWT.NONE);
		equalLabel.setText("="); //$NON-NLS-1$
		equalLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));

		annotationValuesText = new Combo(annotationArea, SWT.BORDER);
		annotationValuesText.setToolTipText(Messages.availableValues); //$NON-NLS-1$
		GridData gdata2 = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gdata2.widthHint = 200;
		annotationValuesText.setLayoutData(gdata2);
		annotationValuesText.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					affectAnnotationToSelection(editor.getTableViewer().getSelection());
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});


		affectAnnotationButton = new Button(annotationArea, SWT.PUSH);
		affectAnnotationButton.setText(Messages.OK); //$NON-NLS-1$
		affectAnnotationButton.setToolTipText(Messages.createUnitForEachSelectedLine); //$NON-NLS-1$
		affectAnnotationButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		affectAnnotationButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				affectAnnotationToSelection(editor.getTableViewer().getSelection());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		affectAllAnnotationButton = new Button(annotationArea, SWT.PUSH);
		affectAllAnnotationButton.setText(Messages.all); //$NON-NLS-1$
		affectAllAnnotationButton.setToolTipText(Messages.createUnitForAllLines); //$NON-NLS-1$
		affectAllAnnotationButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		affectAllAnnotationButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					affectMatchesToLines(concordance.getLines());
				}
				catch (Exception e1) {
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		deleteAnnotationButton = new Button(annotationArea, SWT.PUSH);
		deleteAnnotationButton.setText(Messages.delete); //$NON-NLS-1$
		deleteAnnotationButton.setToolTipText(Messages.deleteUnitForEachSelectedLine); //$NON-NLS-1$
		deleteAnnotationButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		deleteAnnotationButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				deleteAnnotationValues(editor.getTableViewer().getSelection());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// affectAllAnnotationButton = new Button(annotationArea, SWT.PUSH);
		// affectAllAnnotationButton.setText("All");
		// affectAllAnnotationButton.setToolTipText("Create unit for all lines");
		// affectAllAnnotationButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		// affectAllAnnotationButton.addSelectionListener(new SelectionListener() {
		//
		// @Override
		// public void widgetSelected(SelectionEvent e) {
		//
		// try {
		// // List<? extends Match> matches = concordance.getMatches();
		// affectMatchesToLines(concordance.getLines());
		// }
		// catch (Exception e1) {
		// Log.severe(NLS.bind("** Error: {0}", e1.getLocalizedMessage()));
		// Log.printStackTrace(e1);
		// return;
		// }
		// }
		//
		// @Override
		// public void widgetDefaultSelected(SelectionEvent e) {}
		// });

		Button closeButton = new Button(annotationArea, SWT.PUSH);
		closeButton.setToolTipText(Messages.endUrsAnnotation); //$NON-NLS-1$
		closeButton.setLayoutData(new GridData(GridData.END, GridData.CENTER, true, false));
		closeButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_DELETE));

		closeButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				extension.closeArea(UnitConcordanceToolbar.this, true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		onAnnotationTypeSelected(null);

		editor.layout(true);
		//		ext.getSaveButton().setEnabled(true);
		return true;
	}

	private void updateAnnotationWidgetStates() {
		annotationValuesText.setEnabled(unitProperty.length() > 0);
	}

	protected void affectMatchesToLines(final List<Line> lines) {

		final String value = annotationValuesText.getText();

		JobHandler job = new JobHandler(Messages.creatingUnits3Dot, true) { //$NON-NLS-1$

			@Override
			protected IStatus _run(SubMonitor monitor) {

				if (lines.size() == 0) return Status.CANCEL_STATUS;

				for (Line l : lines) {

					Match m = l.getMatch();
					if (annotations.containsKey(l)) { // update existing unites

						if (unitProperty.length() > 0) {
							ArrayList<Unite> unites = annotations.get(l);
							for (Unite u : unites) {
								u.getProps().put(unitProperty, value);
							}
						}
					}
					else {
						ArrayList<Unite> lineUnites = new ArrayList<>();
						annotations.put(l, lineUnites);

						HashMap<String, String> props = new HashMap<>();
						if (unitProperty.length() > 0) {
							props.put(unitProperty, value);
						}

						lineUnites.add(annotManager.addUniteSaisie(unitType, m.getStart(), m.getEnd(), props));
					}
				}

				editor.getTableViewer().getTable().getDisplay().syncExec(new Runnable() {

					@Override
					public void run() {
						editor.getTableViewer().refresh();
					}
				});


				return Status.OK_STATUS;
			}
		};
		job.startJob(true);
	}

	protected void deleteAnnotationValues(ISelection selection) {
		final IStructuredSelection lineSelection = (IStructuredSelection) selection;
		List<Line> lines = lineSelection.toList();
		if (lines.size() == 0) return;

		if (unitProperty.length() > 0) {
			for (Line l : lines) {
				if (!annotations.containsKey(l)) continue;

				for (Unite u : annotations.get(l)) {
					u.getProps().remove(unitProperty);
				}
			}
		}
		else { // remove the units
			for (Line l : lines) {
				if (!annotations.containsKey(l)) continue;
				for (Unite u : annotations.get(l)) {
					annotManager.supUnite(u);
				}

				annotations.remove(l);
			}
		}

		editor.getTableViewer().refresh();
	}

	protected void affectAnnotationToSelection(ISelection selection) {

		final IStructuredSelection lineSelection = (IStructuredSelection) selection;
		List<Line> lines = lineSelection.toList();
		// ArrayList<Match> matches = new ArrayList<>();
		// for (Line l : lines) {
		// matches.add(l.getMatch());
		// }

		affectMatchesToLines(lines);
	}

	protected void onAnnotationPropertySelected(SelectionChangedEvent event) {

		IStructuredSelection sel = (IStructuredSelection) annotationPropertiesCombo.getSelection();
		String type = (String) sel.getFirstElement();
		if (type == null) return;

		unitProperty = type;

		if (unitProperty.length() > 0) {
			annotationColumn.setText(unitType + "@" + unitProperty); //$NON-NLS-1$
		}
		else {
			annotationColumn.setText(unitType);
		}

		setAvailableValues();

		try {
			editor.refresh(false);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		updateAnnotationWidgetStates();
	}

	private void setAvailableValues() {
		if (unitType.length() == 0) {
			annotationValuesText.setItems(new String[0]);
		}
		else {
			String[] items = vue.getValeursChamp(Unite.class, unitType, unitProperty);
			Arrays.sort(items, Collator.getInstance(Locale.getDefault()));
			annotationValuesText.setItems(items);
			KeyStroke keys = KeyStroke.getInstance(SWT.CONTROL, SWT.SPACE);
			new TXMAutoCompleteField(annotationValuesText, new ComboContentAdapter(), items, keys);
		}
	}

	protected void onAnnotationTypeSelected(SelectionChangedEvent event) {
		IStructuredSelection sel = (IStructuredSelection) annotationTypesCombo.getSelection();
		String type = (String) sel.getFirstElement();
		if (type == null) return;

		unitType = type;
		annotationColumn.setText(type);

		ArrayList<String> properties = new ArrayList<>();
		properties.add(""); // to manage the unit itself $NON-NLS-1$ //$NON-NLS-1$
		properties.addAll(annotManager.getStructure().getNomsProps(Unite.class, unitType));
		annotationPropertiesCombo.setInput(properties);
		annotationPropertiesCombo.getCombo().select(0);

		onAnnotationPropertySelected(event);
	}

	protected void createNewType(TypedEvent e, String typeName) {

		if (typeName == null) typeName = ""; //$NON-NLS-1$
		if (typesList.contains(typeName)) return;

		InputDialog dialog = new InputDialog(e.widget.getDisplay().getActiveShell(), Messages.newUnitType, Messages.forName, typeName, null); //$NON-NLS-1$ //$NON-NLS-2$
		if (dialog.open() == InputDialog.OK) {
			String name = dialog.getValue();
			if (name.trim().length() == 0) return;

			annotManager.getStructure().ajouterType(Unite.class, name);
			typesList.add(name);
			annotationTypesCombo.refresh();
			StructuredSelection selection = new StructuredSelection(name);
			annotationTypesCombo.setSelection(selection, true);

		}
		else {
			Log.info(Messages.creationAborted); //$NON-NLS-1$
		}
	}


	@Override
	public boolean save() {
		// System.out.println();
		Log.info(Messages.savingUrsAnnotations); //$NON-NLS-1$
		if (URSCorpora.saveCorpus(corpus)) {
			Log.info(Messages.bind(Messages.DoneP0, new File(corpus.getProjectDirectory(), "analec/" + corpus.getName() + ".ec"))); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			return true;
		}
		else {
			Log.warning(Messages.errorUrsAnnotationsNotSaved); //$NON-NLS-1$
			return false;
		}
	}

	@Override
	public boolean hasChanges() {
		return annotManager.isModifie();
	}

	@Override
	public boolean notifyEndOfCompute() throws Exception {
		return true;
	}

	@Override
	public void notifyStartOfRefresh() throws Exception {
		List<Line> lines = concordance.getLines(concordance.getTopIndex(), concordance.getTopIndex() + concordance.getNLinePerPage());
		annotations.clear();

		ArrayList<Unite> units = annotManager.getUnites(unitType);

		// int iUnit = 0;
		// int iLine = 0;
		// for (; iUnit < units.size() && iLine < lines.size();) {
		// Line line = lines.get(iLine);
		// Unite unite = units.get(iUnit);
		// if (unite.getFin() < line.getMatch().getStart()) {
		// iUnit++;
		// } else if (line.getMatch().getEnd() < unite.getDeb()) {
		// iLine++;
		// } else {
		// annotations.put(line, unite);
		// iLine++;
		// }
		// }
		for (Line line : lines) {
			ArrayList<Unite> lineUnites = new ArrayList<>();

			for (int iUnit = 0; iUnit < units.size(); iUnit++) {

				Unite unite = units.get(iUnit);
				if (unite.getFin() < line.getMatch().getStart()) {
					continue;
				}
				else if (line.getMatch().getEnd() < unite.getDeb()) {
					break; // no need to go further
				}
				else {
					lineUnites.add(unite);
				}
			}
			if (lineUnites.size() > 0) {
				annotations.put(line, lineUnites);
			}
		}

		updateAnnotationWidgetStates();

		// update annotation column width
		if (annotationArea != null) {
			annotationColumn.pack();

			annotationColumn.setResizable(true);
		}
		else {
			annotationColumn.setWidth(0);
		}
	}

	@Override
	protected void _close() {
		if (!annotationColumn.isDisposed()) annotationColumn.dispose();
		if (!annotationArea.isDisposed()) annotationArea.dispose();
		extension.layout();
	}

	@Override
	public void notifyEndOfRefresh() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void notifyStartOfCreatePartControl() throws Exception {
		// TODO Auto-generated method stub
	}

	public void notifyEndOfCreatePartControl() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean canAnnotateResult(TXMResult r) {
		if (r instanceof Concordance && r != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isDirty() {
		return annotManager.isModifie();
	}

	@Override
	public boolean discardChanges() {
		
		return false;
	}

	@Override
	public boolean needToUpdateIndexes() {
		
		return false;
	}

	@Override
	public boolean notifyStartOfCompute() throws Exception {
		
		return true;
	}

	@Override
	public boolean needToUpdateEditions() {

		return false;
	}
}
