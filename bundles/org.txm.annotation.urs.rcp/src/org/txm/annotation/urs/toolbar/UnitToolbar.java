package org.txm.annotation.urs.toolbar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.util.Util;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.events.DragDetectListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.txm.annotation.rcp.editor.AnnotationExtension;
import org.txm.annotation.urs.messages.Messages;
import org.txm.annotation.urs.preferences.URSPreferences;
import org.txm.annotation.urs.view.ElementPropertiesView;
import org.txm.annotation.urs.widgets.NavigationField;
import org.txm.core.results.TXMResult;
import org.txm.edition.rcp.editors.EditionPanel;
import org.txm.edition.rcp.editors.RGBA;
import org.txm.objects.Edition;
import org.txm.objects.Page;
import org.txm.objects.Project;
import org.txm.objects.Text;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.swt.dialog.ConfirmDialog;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.utils.logger.Log;

import visuAnalec.Message;
import visuAnalec.Message.TypeMessage;
import visuAnalec.donnees.Structure;
import visuAnalec.elements.Element;
import visuAnalec.elements.Unite;

public class UnitToolbar extends URSAnnotationToolbar {

	// boolean AnalecUnitLimitCorrectionScheme = TxmPreferences.getBoolean(AnalecPreferencePage.ANALEC_LIMIT_CORRECTION_SCHEME); // use Analec scheme to fix unit limits or TXM's

	public RGBA highlighted_unit_color = new RGBA(183, 191, 237, 0.7f);

	public RGBA selected_unit_color = new RGBA(255, 255, 0, 0.5f);

	private Button createButton;

	private Button deleteButton;

	private Button editButton;

	private KeyListener editorKeyListener;

	private MouseListener editorMouseListener;

	private int fixingAnnotationLimits = 0;

	private NavigationField navigationField;

	private Label l;

	private MenuListener menuListener;

	private List<String> previousSelectedTypeUnitIDS;

	private List<String> previousSelectedUnitIDS;

	protected boolean startFixingUnitLimit;

	private Combo typeCombo;

	private Unite[] unites;

	private String[] typesUnitesAVoir;

	private int maxCorpusPosition;

	private DragDetectListener dragDetectListener;

	private Button drop_b;

	private Button editLeftButton;

	private Button editRightButton;

	public UnitToolbar() {
	}

	@Override
	protected boolean _install(TXMEditor<? extends TXMResult> txmeditor, AnnotationExtension extension, Composite parent, int position) throws Exception {

		annotationArea.getLayout().numColumns = 11;
		annotationArea.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));

		String color_palette = URSPreferences.getInstance().getString(URSPreferences.COLOR_PALETTE);
		if ("yellow".equals(color_palette)) { //$NON-NLS-1$
			this.highlighted_unit_color = URSAnnotationToolbar.lightyellow;
			this.selected_unit_color = URSAnnotationToolbar.yellow;
		}
		else if ("green".equals(color_palette)) { //$NON-NLS-1$
			this.highlighted_unit_color = URSAnnotationToolbar.lightgreen;
			this.selected_unit_color = URSAnnotationToolbar.green;
		}
		else { // custom palette
			URSPreferences urs = URSPreferences.getInstance();
			String highlighted_unit_color_pref = urs.getString(URSPreferences.HIGHLIGHTED_UNIT_COLOR);
			String selected_unit_color_pref = urs.getString(URSPreferences.SELECTED_UNIT_COLOR);
			this.highlighted_unit_color = new RGBA(highlighted_unit_color_pref);
			this.selected_unit_color = new RGBA(selected_unit_color_pref);
		}

		//		this.button = extension.getSaveButton();
		this.maincorpus = this.editor.getCorpus().getMainCorpus();
		try {
			maxCorpusPosition = maincorpus.getSize();
		}
		catch (Exception e) {
			Log.warning(NLS.bind(Messages.UnitToolbar_3, maincorpus, e.getLocalizedMessage()));
			Log.printStackTrace(e);
		}
		analecCorpus.addEventListener(this);


		l = new Label(annotationArea, SWT.NONE);
		l.setText(Messages.UnitToolbar_6);
		l.setToolTipText(Messages.UnitToolbar_6);
		l.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));

		typeCombo = new Combo(annotationArea, SWT.NONE | SWT.READ_ONLY);
		typeCombo.setToolTipText(Messages.UnitToolbar_7);
		GridData gdata = new GridData(GridData.CENTER, GridData.CENTER, false, false);
		gdata.minimumWidth = 150;
		//gdata.widthHint = 100;
		typeCombo.setSize(150, SWT.DEFAULT);
		typeCombo.setLayoutData(gdata);
		typeCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				onTypecomboSelected(e);
			}
		});

		navigationField = new NavigationField(this, SWT.NONE);
		gdata = new GridData(GridData.FILL, GridData.CENTER, true, false);
		navigationField.setLayoutData(gdata);
		navigationField.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				onIdentifiantComboSelected(event);
			}
		});

		// l = new Label(this, SWT.NONE);

		createButton = new Button(annotationArea, SWT.PUSH);
		createButton.setText(Messages.UnitToolbar_8);
		gdata = new GridData(GridData.CENTER, GridData.CENTER, false, false);
		createButton.setLayoutData(gdata);
		createButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				onCreateButtonSelected(e);
			}
		});

		deleteButton = new Button(annotationArea, SWT.PUSH);
		gdata = new GridData(GridData.CENTER, GridData.CENTER, false, false);
		deleteButton.setLayoutData(gdata);
		deleteButton.setImage(IImageKeys.getImage("platform:/plugin/org.txm.rcp/icons/cross.png")); //$NON-NLS-1$
		deleteButton.setToolTipText(Messages.UnitToolbar_10);
		deleteButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				onDeleteComboSelected(e);
			}
		});

		l = new Label(annotationArea, SWT.NONE);

		editLeftButton = new Button(annotationArea, SWT.TOGGLE);
		gdata = new GridData(GridData.CENTER, GridData.CENTER, false, false);
		editLeftButton.setLayoutData(gdata);
		editLeftButton.setText("[ ↔"); //$NON-NLS-1$
		editLeftButton.setToolTipText(Messages.UnitToolbar_12);
		editLeftButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				onEditButtonSelected(e, 1);
			}
		});

		editButton = new Button(annotationArea, SWT.TOGGLE);
		editButton.setText("[↔]"); //$NON-NLS-1$
		editButton.setToolTipText(Messages.UnitToolbar_14);
		editButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				onEditButtonSelected(e);
			}
		});

		editRightButton = new Button(annotationArea, SWT.TOGGLE);
		editRightButton.setText("↔ ]"); //$NON-NLS-1$
		editRightButton.setToolTipText(Messages.UnitToolbar_16);
		editRightButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				onEditButtonSelected(e, 2);
			}
		});

		annotationArea.layout();
		annotationArea.getParent().layout(true);
		editor.layout(true);

		reloadUnits(); // reloads types
		reloadIdentifiants();

		createButton.setEnabled(false);
		deleteButton.setEnabled(false);
		editButton.setEnabled(false);
		editLeftButton.setEnabled(false);
		editRightButton.setEnabled(false);

		// drop_b.setEnabled(false);
		if (typesUnitesAVoir.length > 0) {
			typeCombo.select(1);
			onTypecomboSelected(null);

			testIfAwordSpanIsSelected();
		}

		ElementPropertiesView.openView();
		editor.updateWordStyles();
		annotationArea.layout();
		return true;
	}

	@Override
	public void clearHighlight() {
		if (previousSelectedUnitIDS != null) {
			if (!editor.isDisposed()) {
				editor.removeHighlightWordsById(selected_unit_color, previousSelectedUnitIDS);
				editor.removeFontWeightWordsById(previousSelectedUnitIDS);
			}
			previousSelectedUnitIDS = null; // release memory
		}
		if (previousSelectedTypeUnitIDS != null) {
			editor.removeHighlightWordsById(highlighted_unit_color, previousSelectedTypeUnitIDS);
			previousSelectedTypeUnitIDS = null; // release memory
		}

		if (!createButton.isDisposed()) createButton.setEnabled(false);
		if (!deleteButton.isDisposed()) deleteButton.setEnabled(false);
		if (!editButton.isDisposed()) editButton.setEnabled(false);
		// drop_b.setEnabled(false);
	}

	@Override
	public MouseListener getEditorClickListener() {
		if (editorMouseListener != null) return editorMouseListener;
		editorMouseListener = new MouseListener() {

			boolean dblClick = false;

			boolean debug = Log.isLoggingFineLevel();

			private int t, x, y;

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				if (debug) Log.finer("DOUBLE CLICK"); //$NON-NLS-1$
				dblClick = true;

				EditionPanel panel = editor.getEditionPanel(0);
				if (panel == null) return;
				String[] ids = panel.getWordSelection(); // may be null
				// System.out.println("word id="+id);
				// if (id != null) {
				// org.txm.searchengine.cqp.corpus.Corpus c = UnitToolbar.this.editor.getCorpus();
				// try {
				// if (debug) System.out.println(" HIGHLIGHTING ANNOT IF ANY AT "+id);
				// tryHighlightingWord(id,c );
				// } catch (Exception ex) {
				// ex.printStackTrace();
				// }
				// }

				if (ids != null && typeCombo.getSelectionIndex() > 0 && typeCombo.getEnabled()) {
					createButton.setEnabled(true); // there is a text selection in the browser
				}
			}

			@Override
			public void mouseDown(MouseEvent e) {
				if (debug) Log.finer("MOUSE DOWN"); //$NON-NLS-1$
				dblClick = false;
				t = e.time;
				x = e.x;
				y = e.y;
			}

			@Override
			public void mouseUp(MouseEvent e) {
				if (dblClick) { // doucle click raised by mouseDoubleClick
					dblClick = false;
					return; // stop right now ! :-o
				}
				if (debug) Log.finer("MOUSE UP"); //$NON-NLS-1$
				EditionPanel panel = editor.getEditionPanel(0);
				if (panel == null) return;

				if (typeCombo.getSelectionIndex() <= 0) {
					createButton.setEnabled(false);
					deleteButton.setEnabled(false);
					editLeftButton.setEnabled(false);
					editButton.setEnabled(false);
					editRightButton.setEnabled(false);
					// drop_b.setEnabled(false);
					return; // nothing to do with annotations
				}

				// filter click that are not a left simple click (no drag)
				// System.out.println("click count="+e.count+" button="+e.button+" time="+e.time+" diff="+(e.time-t)+" dist="+(Math.abs(e.x - x) + Math.abs(e.y - y)));
				if (e.count > 1) {
					if (debug) Log.finer(" DOUBLE CLICK"); //$NON-NLS-1$
					// System.out.println("not a simple click");
					return;
				}

				int dist = Math.abs(e.x - x) + Math.abs(e.y - y);
				int deltat = (e.time - t);
				// System.out.println("deltat ="+deltat);
				if (dist != 0 && (deltat > 120 || dist > 4)) {
					// System.out.println(" DRAG dist="+dist+" deltat="+deltat);

					String[] ids = panel.getWordSelection(); // may be null

					if (fixingAnnotationLimits == 3) { // fixing limit with a mouse drag an drop
						if (debug) Log.finer(" EDITING ANNOTATION POSITION mode=" + fixingAnnotationLimits); //$NON-NLS-1$
						onFixingAnnotationLimits(ids);
						return;
					}
					else {

						testIfAwordSpanIsSelected(ids);

						// panel.expandSelectionTo(ids);
						return;
					}
				}

				if (e.button != 1) {
					if (debug) Log.finer(" NO LEFT"); //$NON-NLS-1$
					// System.out.println("not a left click");
					return;
				}
				if ((e.time - t) > 200) {
					if (debug) Log.finer(" LONG"); //$NON-NLS-1$
					// System.out.println("not a short click");
					return;
				}

				String[] ids = panel.getWordSelection(); // may be null

				if (fixingAnnotationLimits > 0) {
					if (debug) Log.finer(" EDITING ANNOTATION POSITION mode=" + fixingAnnotationLimits); //$NON-NLS-1$
					onFixingAnnotationLimits(ids);
					return;
				}

				if (ids == null) { // clear selection
					if (debug) Log.finer(" NO WORD SELECTION"); //$NON-NLS-1$
					navigationField.setSelectionIndex(0); // clear selection

					return;
				}

				org.txm.searchengine.cqp.corpus.CQPCorpus c = UnitToolbar.this.editor.getCorpus();

				try {
					if (debug) Log.finer(" HIGHLIGHTING ANNOT IF ANY AT " + ids[0]); //$NON-NLS-1$
					if (tryHighlightingUnitsWord(ids[0], c)) {
						editor.updateWordStyles();
					}
					else { // a word have been clicked
						testIfAwordSpanIsSelected(ids);
						//panel.expandSelectionTo(ids);
					}
				}
				catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		};
		return editorMouseListener;
	}

	protected void testIfAwordSpanIsSelected() {
		EditionPanel panel = editor.getEditionPanel(0);
		if (panel == null) return;
		String[] ids = panel.getWordSelection(); // may be null
		testIfAwordSpanIsSelected(ids);
	}

	protected void testIfAwordSpanIsSelected(String[] ids) {
		if (ids != null && typeCombo.getItemCount() > 1 && typeCombo.getEnabled()) {
			// System.out.println("ids:"+Arrays.toString(ids));
			if (navigationField.getSelectionIndex() > 0) {
				deleteButton.setEnabled(true);
				editLeftButton.setEnabled(true);
				editButton.setEnabled(true);
				editRightButton.setEnabled(true);
				// drop_b.setEnabled(true);
			}

			createButton.setEnabled(ids != null);
		}
	}



	protected void onFixingAnnotationLimits(String[] ids) {

		if (ids == null || ids.length == 0) {
			Log.info(Messages.UnitToolbar_27);
			return;
		}

		boolean uniteFixed = false;
		int i = navigationField.getSelectionIndex();
		if (i > 0) {
			Unite unite = unites[i - 1];

			try {
				uniteFixed = updateUniteLimits(unite, ids, fixingAnnotationLimits);
			}
			catch (CqiClientException e1) {
				Log.warning(NLS.bind(Messages.UnitToolbar_28, ids[0]));
				Log.printStackTrace(e1);
			}

			editButton.setSelection(false);
			editLeftButton.setSelection(false);
			editRightButton.setSelection(false);
			fixingAnnotationLimits = 0;
			typeCombo.setEnabled(true);

			navigationField.setEnabled(true);
			createButton.setEnabled(true);
			deleteButton.setEnabled(true);

			if (uniteFixed) {
				// button.setEnabled(analecCorpus.isModifie());
				reloadIdentifiants();
				highlightType();
				// highlightUnite(unite, true);
				navigationField.setSelectionIndex(i);
				// updatePropertiesView(unite); // start-end may have changed
				return;
			}
		}
	}



	@Override
	public KeyListener getEditorKeyListener() {
		if (editorKeyListener != null) return editorKeyListener;
		editorKeyListener = new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				int mask = SWT.CTRL;
				if (Util.isMac()) mask = SWT.COMMAND;

				if (((e.stateMask & mask) == mask)) {
					if (e.keyCode == SWT.SPACE) {
						int i = navigationField.getSelectionIndex();
						if (i > 0) {
							onEditButtonSelected(null);
						}
					}
					else if (e.keyCode == SWT.ARROW_LEFT) {
						selectPrevious();
					}
					else if (e.keyCode == SWT.ARROW_RIGHT) {
						selectNext();
						// } else if (e.keyCode == SWT.PAGE_UP) { // clash with Eclipse CTRL+PAGE_UP binding
						// select10Previous();
						// } else if (e.keyCode == SWT.PAGE_DOWN) { // clash with Eclipse CTRL+PAGE_DOWN binding
						// select10Next();
					}
					else if (e.keyCode == SWT.END) {
						selectLast();
					}
					else if (e.keyCode == SWT.HOME) {
						selectFirst();
					}
				}
				else {
					if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
						onCreateButtonSelected(null);
					}
					else if (e.keyCode == SWT.DEL) {
						int i = navigationField.getSelectionIndex();
						if (i > 0) {
							onDeleteComboSelected(e);
						}
					}
				}
			}
		};
		return editorKeyListener;
	}

	@Override
	public MenuListener getMenuListener() {

		if (menuListener != null) return menuListener;

		this.menuListener = new MenuListener() {

			HashSet<MenuItem> entries = new HashSet<>();

			@Override
			public void menuHidden(MenuEvent e) {
			}

			@Override
			public void menuShown(MenuEvent e) {

				EditionPanel panel = editor.getEditionPanel(0);
				if (panel == null) return;
				Menu menu = panel.getBrowser().getMenu();

				for (MenuItem item : entries)
					item.dispose();

				String[] ids = panel.getWordSelection();
				// System.out.println("Word under mouse: "+id);
				if (ids != null && ids.length > 0) {
					int[] startend = getSelectionStartEndWordPositions(); // get the word start end positions
					ArrayList<Unite> unitEntries = new ArrayList<>(); // stores units covering the word
					ArrayList<Integer> noEntries = new ArrayList<>(); // stores units covering the word
					int i = 1;
					for (Unite unit : unites) {
						if (unit.getDeb() > startend[0]) break; // don't go further
						if (unit.getFin() < startend[0]) continue;
						unitEntries.add(unit);
						noEntries.add(i);
						i++;
					}

					if (unitEntries.size() > 0) {
						MenuItem item = new MenuItem(menu, SWT.SEPARATOR);
						entries.add(item);
					}

					for (i = 0; i < unitEntries.size(); i++) {
						final Unite unit = unitEntries.get(i);
						final Integer unitNo = Arrays.binarySearch(unites, unit) + 1;

						MenuItem item = new MenuItem(menu, SWT.NONE);
						if (unit.getDeb() == unit.getFin()) {
							item.setText(NLS.bind(Messages.UnitToolbar_29, getUnitWordSurface(maincorpus, unit, unitNo))); //$NON-NLS-2$
						}
						else {
							item.setText(NLS.bind(Messages.UnitToolbar_32, getUnitWordSurface(maincorpus, unit, unitNo))); //$NON-NLS-2$ //$NON-NLS-3$
						}
						item.addSelectionListener(new SelectionListener() {

							@Override
							public void widgetDefaultSelected(SelectionEvent e) {
							}

							@Override
							public void widgetSelected(SelectionEvent e) {
								selectUniteInIdentifants(unit);
							}
						});
						entries.add(item);
					}
				}
			}
		};

		return menuListener;
	}

	public static String getUnitWordSurface(MainCorpus maincorpus, Element elem, Object unitNo) {
		AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();
		Unite unit = elem.getUnite0();
		String no = ""; //$NON-NLS-1$
		if (unitNo != null) no = "-" + unitNo; //$NON-NLS-1$

		if (unit == null) return elem.getType() + "-" + unitNo; //$NON-NLS-1$

		try {
			if (unit.getDeb() == unit.getFin()) {
				String[] firstWords = CQI.cpos2Str(maincorpus.getWordProperty().getQualifiedName(), new int[] { unit.getDeb() });
				return unit.getType() + no + "=" + firstWords[0];  //$NON-NLS-1$
			}
			else {
				String[] firstWords = CQI.cpos2Str(maincorpus.getWordProperty().getQualifiedName(), new int[] { unit.getDeb(), unit.getFin() });
				if (unit.getFin() - unit.getDeb() > 1) {
					return unit.getType() + no + "=" + firstWords[0] + " ... " + firstWords[1];  //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
				}
				else {
					return unit.getType() + no + "=" + firstWords[0] + " " + firstWords[1];  //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
				}
			}
		}
		catch (Exception e1) {
			e1.printStackTrace();
		}
		return unit.getType() + "-" + unitNo; //$NON-NLS-1$
	}

	public String uniteToWords(Unite unit) {

		AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();
		String[] firstWords = null;
		try {
			if (unit.getDeb() == unit.getFin()) {
				firstWords = CQI.cpos2Str(maincorpus.getWordProperty().getQualifiedName(), new int[] { unit.getDeb() });
			}
			else {
				firstWords = CQI.cpos2Str(maincorpus.getWordProperty().getQualifiedName(), new int[] { unit.getDeb(), unit.getDeb() + 1 });
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return StringUtils.join(firstWords, " "); //$NON-NLS-1$
	}

	/**
	 * Highlight units of the current page
	 */
	protected synchronized void highlightType() {

		if (unites == null) return;
		if (editor.getEditionPanels().size() == 0) return;

		try {
			Page page = editor.getEditionPanel(0).getCurrentPage();
			Edition e = page.getEdition();
			Page next = e.getNextPage(page);
			String currentPageWordID = page.getWordId();
			if (currentPageWordID == null || currentPageWordID.length() == 0) return;

			String qid = maincorpus.getProperty("id").getQualifiedName(); //$NON-NLS-1$
			int first_ids[] = CQPSearchEngine.getCqiClient().str2Id(qid, new String[] { currentPageWordID });

			if (first_ids.length == 0) return;

			int first_id = first_ids[0];
			if (first_id < 0) return;

			int last_ids[] = null;
			if (next != null && next != page) {
				String nextPageWordID = next.getWordId();
				if (!("w_0".equals(nextPageWordID))) { //$NON-NLS-1$
					last_ids = CQPSearchEngine.getCqiClient().str2Id(qid, new String[] { nextPageWordID });
					if (last_ids.length == 0 || last_ids[0] == -1) {
						Log.warning(Messages.bind(Messages.noCQPIdForWordIdP0, nextPageWordID));
					}
				}
			}

			int first_pos = CQPSearchEngine.getCqiClient().id2Cpos(qid, first_id)[0];
			int last_pos = Integer.MAX_VALUE;
			if (last_ids != null && last_ids.length > 0 && last_ids[0] != -1) {
				int[] v = CQPSearchEngine.getCqiClient().id2Cpos(qid, last_ids[0]);
				if (v.length == 0) {
					Log.warning(Messages.bind(Messages.noPosistionFoundForCQPIdP0, last_ids[0]));
				}
				else {
					last_pos = v[0];
				}
			}

			int n2 = 0; // first loop to determine the allpositions array length
			for (int i = 0; i < unites.length; i++) {
				for (int p = unites[i].getDeb(); p <= unites[i].getFin(); p++) {
					if (p >= first_pos && p <= last_pos) {
						n2++;
					}
				}
			}

			int allpositions[] = new int[n2];
			n2 = 0;
			for (int i = 0; i < unites.length; i++) {
				for (int p = unites[i].getDeb(); p <= unites[i].getFin(); p++) {
					if (p >= first_pos && p <= last_pos) {
						allpositions[n2++] = p;
					}
				}
			}

			if (previousSelectedTypeUnitIDS != null) {
				editor.removeHighlightWordsById(highlighted_unit_color, previousSelectedTypeUnitIDS);
				editor.removeFontWeightWordsById(previousSelectedTypeUnitIDS);
				previousSelectedTypeUnitIDS = null; // release memory
			}
			String ids2[] = CQPSearchEngine.getCqiClient().cpos2Str(maincorpus.getProperty("id").getQualifiedName(), allpositions); //$NON-NLS-1$
			previousSelectedTypeUnitIDS = Arrays.asList(ids2);

			editor.addHighlightWordsById(highlighted_unit_color, previousSelectedTypeUnitIDS);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Highlight a unit and don't focus it
	 * 
	 * @param unite
	 * @param focus if set the edition will focus ONE TIME on the unit
	 * 
	 * @return true if the page is changed
	 */
	private synchronized boolean highlightUnite(Unite unite, boolean focus, int clickedWordPosition) {
		int start = unite.getDeb();
		int end = unite.getFin();
		// System.out.println("HU: start="+start+" end="+end+" -> "+(end-start+1));
		int len = end - start + 1;
		if (len < 0) {
			Log.warning(Messages.bind("Error: can not highlight unit={0} : start > end.", unite)); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}
		int positions[] = new int[len]; // build the positions array for CQP
		int n = 0;
		for (int p = start; p <= end; p++) {
			positions[n++] = p;
		}

		try {
			if (previousSelectedUnitIDS != null) { // remove previous highlighted words
				editor.removeHighlightWordsById(selected_unit_color, previousSelectedUnitIDS);
				editor.removeFontWeightWordsById(previousSelectedUnitIDS);
				previousSelectedUnitIDS = null; // release memory
			}

			String ids[] = CQPSearchEngine.getCqiClient().cpos2Str(maincorpus.getProperty("id").getQualifiedName(), positions); //$NON-NLS-1$
			// System.out.println("Highlight words with ids="+Arrays.toString(ids));

			// positions = new int[]{positions[0]};
			Property text_id = maincorpus.getStructuralUnit("text").getProperty("id"); //$NON-NLS-1$ //$NON-NLS-2$
			int[] text_strucs = CQPSearchEngine.getCqiClient().cpos2Struc(text_id.getQualifiedName(), positions);
			String[] text_ids = CQPSearchEngine.getCqiClient().struc2Str(text_id.getQualifiedName(), text_strucs);

			String firstWordId = ids[0];
			String firstWordTextId = text_ids[0];
			if (clickedWordPosition >= 0 && clickedWordPosition < ids.length) {
				firstWordId = ids[clickedWordPosition];
				firstWordTextId = text_ids[clickedWordPosition];
			}


			EditionPanel panel = editor.getEditionPanel(0);
			Edition e = panel.getEdition();
			Project corpus = e.getText().getProject();
			// System.out.println("firstWordTextId="+firstWordTextId);
			Text newText = corpus.getText(firstWordTextId); // get the new text
			// System.out.println("positions="+Arrays.toString(positions));
			// System.out.println("firstWordId="+firstWordId);
			// System.out.println("firstWordTextId="+firstWordTextId);
			// System.out.println("newText="+newText+" e="+e);
			Edition newEdition = newText.getEdition(e.getName()); // get the edition of the new text
			Page newPage = newEdition.getPageForWordId(firstWordId); // find out the page containing the word
			// System.out.println("page="+p+" current page="+panel.getCurrentPage());

			previousSelectedUnitIDS = Arrays.asList(ids);
			editor.addHighlightWordsById(selected_unit_color, previousSelectedUnitIDS);
			editor.addFontWeightWordsById(EditionPanel.WEIGHT_BOLD, previousSelectedUnitIDS);

			if (previousSelectedUnitIDS.size() > 0 && focus) {
				editor.setFocusedWordID(previousSelectedUnitIDS.get(0));
			}
			Page cp = panel.getCurrentPage();
			// System.out.println("Word page= "+p+" current="+panel.getCurrentPage());
			if (!cp.equals(newPage) || !panel.getCurrentText().equals(newText)) { // do we need to change page ?
				editor.goToPage(newText.getName(), newPage.getName());
				return true;
			}
		}
		catch (Exception e1) {
			e1.printStackTrace();
		}
		return false;
	}

	protected void onCreateButtonSelected(SelectionEvent e) {
		// System.out.println("onCreateButtonSelected");
		int[] startend = getSelectionStartEndWordPositions();
		if (startend != null) {
			Log.info(NLS.bind(Messages.UnitToolbar_44, startend[0], startend[1]));
			Unite unite = UnitToolbar.this.vue.getCorpus().addUniteSaisie(typeCombo.getText(), startend[0], startend[1]);
			Structure structure = UnitToolbar.this.analecCorpus.getStructure();
			for (String prop : unite.getProps().keySet()) {
				unite.getProps().put(prop, structure.getValeurParDefaut(unite.getClass(), unite.getType(), prop));
			}

			reloadIdentifiants();
			selectUniteInIdentifants(unite);
			// highlightUnite(unite, true, -1);

			deleteButton.setEnabled(true);
			editLeftButton.setEnabled(true);
			editButton.setEnabled(true);
			editRightButton.setEnabled(true);
			// drop_b.setEnabled(true);
			// updatePropertiesView(unite, true);
			setFocusInPropertiesView();
			fireIsDirty();
		}

		if (e != null) editor.updateWordStyles();
	}

	protected void onDeleteComboSelected(Object e) {
		int i = navigationField.getSelectionIndex();
		if (i <= 0) return;
		// System.out.println("Delete unite at i="+i);
		Unite unite = unites[i - 1];

		String id = navigationField.getText();

		ConfirmDialog dialog = new ConfirmDialog(Display.getCurrent().getActiveShell(),
				"org.txm.annotation.urs.delete", //$NON-NLS-1$
				NLS.bind(Messages.UnitToolbar_47, id),
				NLS.bind(Messages.UnitToolbar_48, id));
		if (dialog.open() == ConfirmDialog.CANCEL) return;

		// System.out.println("unite "+unite.toString());
		UnitToolbar.this.vue.getCorpus().supUnite(unite);
		reloadIdentifiants();

		i = typeCombo.getSelectionIndex();
		if (i > 0) {
			highlightType();
		}
		fireIsDirty();

		if (e != null) editor.updateWordStyles();
	}

	protected void onEditButtonSelected(SelectionEvent e) {
		onEditButtonSelected(e, 3);
	}

	protected void onEditButtonSelected(SelectionEvent e, int mode) {
		if (mode == 0) { // select unit, select words, click the edit button -> deprecated
			int i = navigationField.getSelectionIndex();
			if (i == -1) return;
			// System.out.println("Delete unite at i="+i);
			Unite unite = unites[i - 1];
			int[] startend = getSelectionStartEndWordPositions();
			if (startend != null) {
				analecCorpus.modifBornesUnite(unite, startend[0], startend[1]);
				reloadIdentifiants();
				highlightUnite(unite, false, -1);
				updatePropertiesView(unite);
			}
		}
		else {
			if (fixingAnnotationLimits > 0) {
				typeCombo.setEnabled(true);
				navigationField.setEnabled(true);
				createButton.setEnabled(true);
				deleteButton.setEnabled(true);
				fixingAnnotationLimits = 0;
			}
			else {
				typeCombo.setEnabled(false);
				navigationField.setEnabled(false);
				createButton.setEnabled(false);
				deleteButton.setEnabled(false);
				fixingAnnotationLimits = mode;
			}
		}
		fireIsDirty();
		if (e != null) editor.updateWordStyles();
	}

	/**
	 * called when the NavigationField changed its selection
	 * 
	 * @param e
	 */
	public void onIdentifiantComboSelected(Object e) {
		int i = navigationField.getSelectionIndex();
		boolean pageChanged = false;
		if (i <= 0) {
			if (previousSelectedUnitIDS != null) {
				editor.removeHighlightWordsById(selected_unit_color, previousSelectedUnitIDS);
				editor.removeFontWeightWordsById(previousSelectedUnitIDS);
				previousSelectedUnitIDS = null; // release memory
			}
			highlightType();

			clearPropertiesView(); // unload the selected element if any
			createButton.setEnabled(false);
			deleteButton.setEnabled(false);
			editLeftButton.setEnabled(false);
			editButton.setEnabled(false);
			editRightButton.setEnabled(false);
			// drop_b.setEnabled(false);
			// navigationField.setSelectionIndex(i);
		}
		else {
			createButton.setEnabled(false);
			deleteButton.setEnabled(true);
			editLeftButton.setEnabled(true);
			editButton.setEnabled(true);
			editRightButton.setEnabled(true);
			// drop_b.setEnabled(true);
			// System.out.println("i="+i);
			Unite unite = unites[i - 1];
			// System.out.println("unite="+unite);
			// System.out.println("unite str="+uniteToWords(unite));
			pageChanged = highlightUnite(unite, true, -1);

			updatePropertiesView(unite);
			deleteButton.setEnabled(true);
			editLeftButton.setEnabled(true);
			editButton.setEnabled(true);
			editRightButton.setEnabled(true);
			// drop_b.setEnabled(true);
		}

		if (e != null && !pageChanged) editor.updateWordStyles();
	}

	protected void onTypecomboSelected(SelectionEvent e) {
		reloadIdentifiants();

		if (typeCombo.getSelectionIndex() > 0) {
			// String type = unitesAVoir[typeCombo.getSelectionIndex()-1];
			highlightType();
			navigationField.setEnabled(true);
		}
		else {
			clearHighlight();
			navigationField.setEnabled(false);
		}

		createButton.setEnabled(false);
		deleteButton.setEnabled(false);
		editButton.setEnabled(false);
		// drop_b.setEnabled(false);
		if (e != null) editor.updateWordStyles();
	}

	/**
	 * reload the identifiants combo
	 * 
	 */
	public void reloadIdentifiants() {
		String type0 = typeCombo.getText();
		unites = vue.getUnitesAVoir(type0);
		String[] identifiants = vue.getIdUnites(type0, unites);
		navigationField.setIdentifiants(identifiants, type0, Unite.class);
		navigationField.setSelectionIndex(0);
	}

	public void reloadUnits() {

		typesUnitesAVoir = vue.getTypesUnitesAVoir();
		String[] items = new String[typesUnitesAVoir.length + 1];
		System.arraycopy(typesUnitesAVoir, 0, items, 1, typesUnitesAVoir.length);
		items[0] = Messages.UnitToolbar_54;
		typeCombo.setItems(items);
		if (items.length > 0) {
			typeCombo.select(0);
		}
		typeCombo.layout();

		onTypecomboSelected(null);
	}

	protected void selectUniteInIdentifants(Unite unite) {
		for (int i = 0; i < unites.length; i++) {
			// System.out.println("VS "+unites[i]);
			if (unites[i] == unite) {
				navigationField.setSelectionIndex(i + 1);
				return; // no need to go further
			}
		}
	}

	public void setEnable(boolean enable) {
		l.setEnabled(enable);
		typeCombo.setEnabled(enable);
		navigationField.setEnabled(enable);
		createButton.setEnabled(enable);
		deleteButton.setEnabled(enable);
		editButton.setEnabled(enable);
		editLeftButton.setEnabled(enable);
		editRightButton.setEnabled(enable);
		// drop_b.setEnabled(enable);
	}

	@Override
	public void traiterEvent(Message e) {
		if (annotationArea.isDisposed()) return;

		if (e.getType().equals(TypeMessage.MODIF_STRUCTURE)) {
			// System.out.println("UnitToolbar.traiterEvent: Update toolbar ");
			UnitToolbar.this.annotationArea.getDisplay().syncExec(new Runnable() {

				@Override
				public void run() {
					setEnable(!analecCorpus.isVide());
					// reloadUnits();
				}
			});
		}
		else if (e.getType().equals(TypeMessage.MODIF_ELEMENT)) {
			// System.out.println("UnitToolbar.traiterEvent: Update toolbar ");
			UnitToolbar.this.annotationArea.getDisplay().syncExec(new Runnable() {

				@Override
				public void run() {
					// reloadUnits();
				}
			});
		}
	}

	protected synchronized boolean tryHighlightingUnitsWord(String id, org.txm.searchengine.cqp.corpus.CQPCorpus c) throws CqiClientException {
		EditionPanel panel = editor.getEditionPanel(0);
		if (panel == null) return false;
		if (id != null) {

		}
		QueryResult r = c.query(new CQLQuery("[_.text_id=\"" + CQLQuery.addBackSlash(panel.getEdition().getText().getName()) + "\" & id=\"" + id + "\"]"), "TMP", false); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		List<Match> matches = r.getMatches();
		if (matches.size() == 0) {
			System.out.println(NLS.bind(Messages.UnitToolbar_59, id));
			return false;
		}
		int start = matches.get(0).getStart();
		// System.out.println("start="+start);
		r.drop();
		int i = 0;
		ArrayList<Integer> matching_units = new ArrayList<>();
		for (Unite u : unites) {
			if (u.getDeb() <= start && start <= u.getFin()) {
				matching_units.add(i);
			}
			else if (u.getDeb() > start) { // no need to search further
				continue;
			}
			i++;
		}

		if (matching_units.size() > 0) {
			// get index in matching_units of the current unit if any
			int iUnite = matching_units.indexOf(navigationField.getSelectionIndex() - 1);

			if (iUnite == -1) iUnite = 0; // current unit is not present, take the first in matching_units
			else iUnite = (iUnite + 1) % matching_units.size(); // take the next unit in matching_units

			iUnite = matching_units.get(iUnite); // get the real unit position in 'unites'
			navigationField.setSelectionIndex(iUnite + 1, false); // call onIndentifiantComboChanged(...)

			createButton.setEnabled(false);
			deleteButton.setEnabled(true);
			editLeftButton.setEnabled(true);
			editButton.setEnabled(true);
			editRightButton.setEnabled(true);

			Unite unite = unites[iUnite];
			int offset = start - unite.getDeb(); // to select the right page when the unit words overlap with 2 pages
			highlightUnite(unite, false, offset);

			updatePropertiesView(unite);

			return true;
		}

		if (navigationField.getSelectionIndex() != 0) { // is the navigationField already at 0 ?
			navigationField.setSelectionIndex(0);
		}

		return false;
	}

	protected boolean updateUniteLimits(Unite unite, String[] ids, int mode) throws CqiClientException {

		EditionPanel panel = UnitToolbar.this.editor.getEditionPanel(0);
		String text_name = panel.getEdition().getText().getName();

		int[] startend = getStartEndWordPositions(maincorpus, ids, text_name);
		if (startend == null) return false;

		int pos = startend[0];

		if (mode == 1) { // left
			analecCorpus.modifBornesUnite(unite, pos, unite.getFin());
		}
		else if (mode == 2) { // right
			analecCorpus.modifBornesUnite(unite, unite.getDeb(), pos);
		}
		else if (mode == 3) { // choose using distance
			analecCorpus.modifBornesUnite(unite, startend[0], startend[1]);
		}

		return true;
	}

	@Override
	public void selectNext() {
		int i = navigationField.selectNext();
	}

	@Override
	public void selectPrevious() {
		int i = navigationField.selectPrevious();
	}

	@Override
	public void selectFirst() {
		int i = navigationField.selectFirst();
	}

	@Override
	public void select10Previous() {
		int i = navigationField.select10Previous();
	}

	@Override
	public void select10Next() {
		int i = navigationField.select10Next();
	}

	@Override
	public void selectLast() {
		int i = navigationField.selectLast();
	}

	@Override
	public void select(Element currentElement) {
		if (currentElement.getClass() == Unite.class) {
			int i = Arrays.binarySearch(unites, currentElement);
			if (i >= 0) navigationField.setSelectionIndex(i + 1);
		}
	}

	@Override
	public String getElementType() {
		if (typeCombo.isDisposed()) return ""; //$NON-NLS-1$
		return typeCombo.getText();
	}

	@Override
	public void onBackToText(Text text, String id) {
		if (id.length() > 0) {
			try {
				tryHighlightingUnitsWord(id, this.getCQPCorpus());
			}
			catch (CqiClientException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean canAnnotateResult(TXMResult r) {
		if (r instanceof Text && r != null) {
			return true;
		}
		return false;
	}

	@Override
	public String getName() {
		return Messages.unit;
	}

	@Override
	public boolean notifyStartOfCompute() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean notifyEndOfCompute() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void notifyEndOfRefresh() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void notifyStartOfCreatePartControl() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void notifyStartOfRefresh() throws Exception {
		// TODO Auto-generated method stub
	}

	ProgressListener pageProgressListener = null;

	@Override
	protected ProgressListener getPageReloadListener() {
		if (pageProgressListener != null) {
			return pageProgressListener;
		}

		pageProgressListener = new ProgressListener() {

			@Override
			public void completed(ProgressEvent event) {
				if (typeCombo.getSelectionIndex() >= 0) {
					highlightType();
				}
			}

			@Override
			public void changed(ProgressEvent event) {
			}
		};
		return pageProgressListener;
	}

	@Override
	public boolean needToUpdateIndexes() {
		return false;
	}

	@Override
	public boolean hasChanges() {
		return analecCorpus.isModifie();
	}

	@Override
	public boolean needToUpdateEditions() {

		return false;
	}
}
