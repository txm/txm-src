package org.txm.annotation.urs.toolbar;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.txm.annotation.rcp.editor.AnnotationArea;
import org.txm.annotation.rcp.editor.AnnotationExtension;
import org.txm.annotation.urs.URSCorpora;
import org.txm.annotation.urs.messages.Messages;
import org.txm.annotation.urs.preferences.URSPreferences;
import org.txm.annotation.urs.view.ElementPropertiesView;
import org.txm.annotation.urs.view.ElementSearchView;
import org.txm.core.results.TXMResult;
import org.txm.edition.rcp.editors.EditionPanel;
import org.txm.edition.rcp.editors.RGBA;
import org.txm.edition.rcp.editors.SynopticEditionEditor;
import org.txm.objects.Text;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.utils.logger.Log;

import visuAnalec.Message;
import visuAnalec.donnees.Corpus;
import visuAnalec.donnees.Corpus.CorpusListener;
import visuAnalec.elements.Element;
import visuAnalec.elements.Unite;
import visuAnalec.vue.Vue;

public abstract class URSAnnotationToolbar extends AnnotationArea implements CorpusListener {

	// see https://groupes.renater.fr/wiki/txm-info/public/specs_charte_graphique
	public static final RGBA blue = new RGBA(152, 159, 201, 0.7f);

	public static final RGBA lightblue = new RGBA(185, 193, 239, 0.4f);

	public static final RGBA green = new RGBA(122, 153, 0, 0.7f);

	public static final RGBA lightgreen = new RGBA(206, 240, 83, 0.5f);

	public static final RGBA yellow = new RGBA(255, 255, 50, 1f);

	public static final RGBA lightyellow = new RGBA(255, 255, 176, 0.5f);

	protected Corpus analecCorpus;

	protected Timer saveTimer;

	protected Vue vue;

	protected MainCorpus maincorpus;

	protected SynopticEditionEditor editor;

	//	protected ToolItem button;

	private Button closeButton;

	private TimerTask saveTask;

	public URSAnnotationToolbar() {
	}

	public abstract void selectFirst();

	public abstract void selectPrevious();

	public abstract void select10Previous();

	public abstract void select10Next();

	public abstract void selectNext();

	public abstract void selectLast();

	public abstract void select(Element currentElement);

	@Override
	public boolean save() {
		if (!analecCorpus.isModifie()) return false;

		Log.info(Messages.savingURSAnnotations3Dot);
		if (URSCorpora.saveCorpus(analecCorpus)) {
			Log.info(Messages.doneURSAnnotationsSaved);
			return true;
		}
		else {
			Log.warning(Messages.errorURSAnnotationNotSaved);
			return false;
		}
	}

	protected abstract boolean _install(TXMEditor<? extends TXMResult> txmeditor, AnnotationExtension extension, Composite parent, int position) throws Exception;

	@Override
	public final boolean install(TXMEditor<? extends TXMResult> txmeditor, AnnotationExtension extension, Composite parent, int position) throws Exception {

		this.extension = extension;
		this.editor = (SynopticEditionEditor) txmeditor;
		this.maincorpus = (MainCorpus) this.editor.getCorpus();
		analecCorpus = URSCorpora.getCorpus(maincorpus);
		vue = URSCorpora.getVue(maincorpus);

		if (URSPreferences.getInstance().getInt(URSPreferences.SAVE_TIMER) > 0) {
			saveTimer = new Timer(true);

			Log.info(Messages.bind(Messages.URSAutosaveTimerIsActivatedSetToP0Minutes, URSPreferences.getInstance().getInt(URSPreferences.SAVE_TIMER)));

			saveTask = new TimerTask() {

				@Override
				public void run() {

					Log.fine("URS auto-saving... ", false); //$NON-NLS-1$
					if (save()) {
						Log.fine("Annotations saved."); //$NON-NLS-1$
						editor.getParent().getDisplay().syncExec(new Runnable() {

							@Override
							public void run() {
								editor.fireIsDirty();
							}
						});
					}
					else {
						//Log.info("Annotations were already saved.");
					}
				}
			};

			saveTimer.scheduleAtFixedRate(saveTask, URSPreferences.getInstance().getInt(URSPreferences.SAVE_TIMER) * 60L * 1000L,
					URSPreferences.getInstance().getInt(URSPreferences.SAVE_TIMER) * 60L * 1000L);
		}
		annotationArea = new GLComposite(parent, SWT.NONE); //$NON-NLS-1$
		annotationArea.getLayout().horizontalSpacing = 1;

		if (getEditorClickListener() != null)
			editor.getEditionPanel(0).getBrowser().addMouseListener(getEditorClickListener());
		if (getMenuListener() != null)
			editor.getEditionPanel(0).getBrowser().getMenu().addMenuListener(getMenuListener());
		if (getEditorKeyListener() != null)
			editor.getEditionPanel(0).getBrowser().addKeyListener(getEditorKeyListener());
		if (getEditorKeyListener() != null)
			editor.getEditionPanel(0).getBeforeHighlighListeners().add(getPageReloadListener());

		if (analecCorpus.getStructure().isVide()) {
			analecCorpus.ajouterType(Unite.class, "Entity"); //$NON-NLS-1$
			analecCorpus.ajouterProp(Unite.class, "Entity", "Property"); //$NON-NLS-1$ //$NON-NLS-2$
		}

		//		button = extension.getSaveButton();
		//		button.setEnabled(analecCorpus.isModifie());

		_install(txmeditor, extension, parent, position);

		closeButton = new Button(annotationArea, SWT.PUSH);
		closeButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_DELETE));
		closeButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				extension.closeArea(URSAnnotationToolbar.this, true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		return true;
	}

	protected void installCloseButton() {

	}


	protected abstract ProgressListener getPageReloadListener();

	public Corpus getAnalecCorpus() {
		return analecCorpus;
	}

	protected void updatePropertiesView(Element element) {
		ElementPropertiesView view = ElementPropertiesView.openView();
		if (view != null) view.loadElement(this.maincorpus, this.analecCorpus, element, this);
	}

	@Override
	public boolean isDirty() {
		if (analecCorpus != null) return analecCorpus.isModifie();
		return false;
	}

	public boolean isDisposed() {
		return annotationArea == null || annotationArea.isDisposed();
	}

	protected void clearPropertiesView() {
		ElementPropertiesView view = ElementPropertiesView.openView();
		if (view != null) {
			view.unloadElement(this.analecCorpus, this);
		}
	}

	protected static void setFocusInPropertiesView() {
		ElementPropertiesView view = ElementPropertiesView.openView();
		if (view != null) {
			view.setFocusOnProperty();
		}
	}

	protected int[] getSelectionStartEndWordPositions() {
		EditionPanel panel = this.editor.getEditionPanel(0);
		String text_name = panel.getEdition().getText().getName();
		String[] sel = panel.getWordSelection();
		if (sel == null) {
			Log.warning(Messages.errorNoWordSelection);
			return null;
		}
		// System.out.println("Selection="+sel);
		try {
			return getStartEndWordPositions(maincorpus, sel, text_name);
		}
		catch (CqiClientException e1) {
			e1.printStackTrace();
		}
		return null;
	}

	protected int[] getStartEndWordPositions(MainCorpus c, String[] ids, String text_name) throws CqiClientException {
		// System.out.println("IDS="+Arrays.toString(ids));
		CQLQuery query = new CQLQuery("[_.text_id=\"" + CQLQuery.addBackSlash(text_name) + "\" & id=\"" + StringUtils.join(ids, "|") + "\"]"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		QueryResult result = c.query(query, "TMP", false); //$NON-NLS-1$
		List<Match> matches = result.getMatches();
		int start, end;
		// try {
		// int[] indexes = CQPSearchEngine.getCqiClient().str2Id(c.getProperty("id").getQualifiedName(), ids);
		// int[] positions = CQPSearchEngine.getCqiClient().id2Cpos(c.getProperty("id").getQualifiedName(), indexes);
		//
		// } catch (Exception e) {
		//
		// }
		if (matches.size() == 0) {
			Log.warning(NLS.bind(Messages.errorNoMatchForQueryP0, query));
			return null;
		}
		else {
			start = matches.get(0).getStart();
			end = matches.get(matches.size() - 1).getStart();
		}
		result.drop();
		return new int[] { start, end };
	}

	// listen to corpus structure modification -> enable/disable the button
	// and to new annotations -> set the star modifier to the Corpus view
	protected CorpusListener corpusListener = new CorpusListener() {

		@Override
		public void traiterEvent(final Message e) {
			if (maincorpus == null) return;

			// System.out.println("StartButton.corpusListener.traiterEvent: "+e);

			//			button.getDisplay().syncExec(new Runnable() {
			//				
			//				@Override
			//				public void run() {
			//					if (e.getType() == TypeMessage.MODIF_STRUCTURE) {
			//						boolean ready = URSCorpora.isAnnotationStructureReady(maincorpus);
			//						button.setEnabled(ready);
			//						// if (!ready && utoolbar != null && !utoolbar.isDisposed()) utoolbar.clearHighlight();
			//					}
			//					
			//					if (e.getType() == TypeMessage.MODIF_ELEMENT) {
			//						button.setEnabled(URSCorpora.isAnnotationStructureReady(maincorpus));
			//						if (!maincorpus.isModified()) {
			//							maincorpus.setIsModified(true); // to show the * in corpora view
			//							updateCorporaView(maincorpus);
			//						}
			//						
			//					}
			//					else if (e.getType() == TypeMessage.CORPUS_SAVED) {
			//						button.setEnabled(false);
			//						if (maincorpus.isModified()) {
			//							maincorpus.setIsModified(false); // to remove the * in corpora view
			//							updateCorporaView(maincorpus);
			//						}
			//					}
			//				}
			//			});

		}
	};

	public static void updateCorporaView(final MainCorpus corpus) {
		CorporaView.refreshObject(corpus);
	}

	@Override
	public boolean discardChanges() {
		if (maincorpus.isModified()) {
			URSCorpora.revert(maincorpus);
			analecCorpus = URSCorpora.getCorpus(maincorpus);
			vue = URSCorpora.getVue(maincorpus);
			analecCorpus.removeEventListener(corpusListener);
			CorporaView.refreshObject(maincorpus);
		}
		return false;
	}

	public abstract MenuListener getMenuListener();

	public abstract MouseListener getEditorClickListener();

	public abstract KeyListener getEditorKeyListener();

	public abstract void clearHighlight();

	public abstract void onBackToText(Text text, String id);

	@Override
	public SynopticEditionEditor getEditor() {
		return editor;
	}

	@Override
	public void _close() {

		this.clearHighlight();

		try {
			analecCorpus.removeEventListener(this);
		}
		catch (Exception e) {
		}

		if (!editor.isDisposed()) {
			if (this.getEditorClickListener() != null)
				editor.getEditionPanel(0).getBrowser().removeMouseListener(this.getEditorClickListener());
			if (this.getMenuListener() != null)
				editor.getEditionPanel(0).getBrowser().getMenu().removeMenuListener(this.getMenuListener());
			if (this.getEditorKeyListener() != null)
				editor.getEditionPanel(0).getBrowser().removeKeyListener(this.getEditorKeyListener());
			if (this.getPageReloadListener() != null)
				editor.getEditionPanel(0).getBeforeHighlighListeners().remove(this.getPageReloadListener());
		}
		if (annotationArea != null && annotationArea.isDisposed()) annotationArea.dispose();

		if (saveTimer != null) {
			saveTimer.cancel();
		}

		ElementPropertiesView.closeView();
		ElementSearchView.closeView();
		analecCorpus.removeEventListener(corpusListener);
	}

	public org.txm.searchengine.cqp.corpus.CQPCorpus getCQPCorpus() {
		return maincorpus;
	}

	public abstract String getElementType();

	public void fireIsDirty() {
		editor.fireIsDirty();
		//		this.extension.getSaveButton().setEnabled(true);
	}
}
