package org.txm.annotation.urs.toolbar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.eclipse.jface.util.Util;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.txm.annotation.rcp.editor.AnnotationExtension;
import org.txm.annotation.urs.messages.Messages;
import org.txm.annotation.urs.preferences.URSPreferences;
import org.txm.annotation.urs.view.ElementPropertiesView;
import org.txm.annotation.urs.widgets.NavigationField;
import org.txm.core.results.TXMResult;
import org.txm.edition.rcp.editors.EditionPanel;
import org.txm.edition.rcp.editors.RGBA;
import org.txm.edition.rcp.editors.SynopticEditionEditor;
import org.txm.objects.Edition;
import org.txm.objects.Page;
import org.txm.objects.Project;
import org.txm.objects.Text;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.swt.dialog.ConfirmDialog;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.utils.logger.Log;

import visuAnalec.Message;
import visuAnalec.Message.TypeMessage;
import visuAnalec.elements.Element;
import visuAnalec.elements.Schema;
import visuAnalec.elements.Unite;

public class SchemaToolbar extends URSAnnotationToolbar {

	public RGBA highlighted_unit_color = new RGBA(183, 191, 237, 0.7f);

	public RGBA selected_unit_color = new RGBA(255, 255, 0, 0.5f);

	//	private Button createButton;
	private Button deleteSchemaButton;

	private Button deleteUniteButton;

	//	private Button editButton;
	private KeyListener editorKeyListener;

	private MouseListener editorMouseListener;

	private Label l;

	private MenuListener menuListener;

	//	private List<String> previousSelectedTypeSchemaIDS;
	private List<String> previousSelectedSchemaIDS;

	private List<String> previousSelectedUniteIDS;

	protected boolean startFixingUnitLimit;

	private Schema[] schemas;

	private Unite[] unites;

	private String[] schemasAVoir;

	private Combo typeCombo;

	private NavigationField navigationField;

	//	private Combo unitTypeCombo;
	private NavigationField subNavigationField;

	public SchemaToolbar() {
	}

	@Override
	protected boolean _install(TXMEditor<? extends TXMResult> txmeditor, AnnotationExtension extension, Composite parent, int position) throws Exception {

		annotationArea.getLayout().numColumns = 8;
		annotationArea.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));

		String color_palette = URSPreferences.getInstance().getString(URSPreferences.COLOR_PALETTE);
		if ("yellow".equals(color_palette)) { //$NON-NLS-1$
			this.highlighted_unit_color = URSAnnotationToolbar.lightyellow;
			this.selected_unit_color = URSAnnotationToolbar.yellow;
		}
		else if ("green".equals(color_palette)) { //$NON-NLS-1$
			this.highlighted_unit_color = URSAnnotationToolbar.lightgreen;
			this.selected_unit_color = URSAnnotationToolbar.green;
		}
		else { // custom palette
			String highlighted_unit_color_pref = URSPreferences.getInstance().getString(URSPreferences.HIGHLIGHTED_UNIT_COLOR);
			String selected_unit_color_pref = URSPreferences.getInstance().getString(URSPreferences.SELECTED_UNIT_COLOR);
			this.highlighted_unit_color = new RGBA(highlighted_unit_color_pref);
			this.selected_unit_color = new RGBA(selected_unit_color_pref);
		}

		this.editor = (SynopticEditionEditor) txmeditor;
		analecCorpus.addEventListener(this);

		l = new Label(annotationArea, SWT.NONE);
		l.setText(Messages.schemaType); //$NON-NLS-1$
		l.setToolTipText(Messages.schemaType); //$NON-NLS-1$

		this.maincorpus = this.editor.getCorpus().getMainCorpus();
		typeCombo = new Combo(annotationArea, SWT.NONE | SWT.READ_ONLY);
		typeCombo.setToolTipText(Messages.schemaType); //$NON-NLS-1$
		GridData gdata = new GridData(GridData.FILL, GridData.CENTER, false, false);
		gdata.minimumWidth = 100;
		gdata.widthHint = 100;
		typeCombo.setLayoutData(gdata);
		typeCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				onTypeComboSelected(e);
			}
		});

		navigationField = new NavigationField(this, SWT.NONE);
		gdata = new GridData(GridData.FILL, GridData.FILL, true, false);
		navigationField.setLayoutData(gdata);
		navigationField.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				onIdentifiantComboSelected(null);
			}
		});

		deleteSchemaButton = new Button(annotationArea, SWT.PUSH);
		deleteSchemaButton.setImage(IImageKeys.getImage("platform:/plugin/org.txm.rcp/icons/cross.png")); //$NON-NLS-1$
		deleteSchemaButton.setToolTipText(Messages.deleteTheSchema);
		deleteSchemaButton.setEnabled(false);
		deleteSchemaButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				onDeleteSchemaComboSelected(e);
			}
		});

		l = new Label(annotationArea, SWT.NONE);
		l.setText(Messages.units);
		subNavigationField = new NavigationField(this, SWT.NONE);
		gdata = new GridData(GridData.FILL, GridData.FILL, true, false);
		subNavigationField.setLayoutData(gdata);
		subNavigationField.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				onIdentifiantUniteComboSelected(event);
			}
		});

		deleteUniteButton = new Button(annotationArea, SWT.PUSH);
		deleteUniteButton.setImage(IImageKeys.getImage("platform:/plugin/org.txm.rcp/icons/cross.png")); //$NON-NLS-1$
		deleteUniteButton.setToolTipText(Messages.removeTheSelectedUnitToTheSchema); //$NON-NLS-1$
		deleteUniteButton.setEnabled(false);
		deleteUniteButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				onDeleteUniteComboSelected(e);
			}
		});

		annotationArea.layout();
		annotationArea.getParent().layout(true);
		editor.getEditionArea().layout();

		reloadTypeSchemas(); // reloads ids also
		reloadIdentifiants();

		if (schemasAVoir.length > 0) {
			typeCombo.select(1);
			onTypeComboSelected(null);

			//testIfAwordSpanIsSelected();

			//			if (schemas.length > 0) {
			//				navigationField.selectFirst();
			//				onIdentifiantComboSelected(null);
			//			}
		}

		//		createButton.setEnabled(false);
		//deleteSchemaButton.setEnabled(false);
		//		editButton.setEnabled(false);
		editor.updateWordStyles();
		ElementPropertiesView.openView();

		return true;
	}

	protected void testIfAwordSpanIsSelected() {
		EditionPanel panel = editor.getEditionPanel(0);
		if (panel == null) return;
		String[] ids = panel.getWordSelection(); // may be null
		testIfAwordSpanIsSelected(ids);
	}

	protected void testIfAwordSpanIsSelected(String[] ids) {
		if (ids != null && typeCombo.getItemCount() > 1 && typeCombo.getEnabled()) {
			//System.out.println("ids:"+Arrays.toString(ids));
			if (navigationField.getSelectionIndex() > 0) {
				deleteSchemaButton.setEnabled(true);
			}
		}
	}

	public void clearSchemasHighlight() {
		//System.out.println("clear: "+previousSelectedSchemaIDS);
		if (previousSelectedSchemaIDS != null) {
			editor.removeHighlightWordsById(highlighted_unit_color, previousSelectedSchemaIDS);
			editor.removeFontWeightWordsById(previousSelectedSchemaIDS);
			previousSelectedSchemaIDS = null; // release memory
		}
		//		if (previousSelectedTypeSchemaIDS != null) {
		//			editor.removeHighlightWordsById(lightblue, previousSelectedTypeSchemaIDS);
		//			previousSelectedTypeSchemaIDS = null; // release memory
		//		}
	}


	public void clearUnitesHighlight() {
		if (previousSelectedUniteIDS != null) {
			editor.removeHighlightWordsById(selected_unit_color, previousSelectedUniteIDS);
			editor.removeFontWeightWordsById(previousSelectedUniteIDS);
			previousSelectedUniteIDS = null; // release memory
		}

		if (!deleteUniteButton.isDisposed()) deleteUniteButton.setEnabled(false);
	}

	public MouseListener getEditorClickListener() {

		if (editorMouseListener != null) return editorMouseListener;

		editorMouseListener = new MouseListener() {

			boolean dblClick = false;

			boolean debug = false;

			private int t, x, y;

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				if (debug) Log.finer("DOUBLE CLICK"); //$NON-NLS-1$
				dblClick = true;

				EditionPanel panel = editor.getEditionPanel(0);
				if (panel == null) return;
				String[] ids = panel.getWordSelection(); // may be null
				//System.out.println("word id="+id);
				//				if (id != null) {
				//					org.txm.searchengine.cqp.corpus.Corpus c = SchemaToolbar.this.editor.getCorpus();
				//					try {
				//						if (debug)  System.out.println(" HIGHLIGHTING ANNOT IF ANY AT "+id);
				//						tryHighlightingWord(id,c );
				//					} catch (Exception ex) {
				//						ex.printStackTrace();
				//					}
				//				}

				//				if (id != null && typeCombo.getSelectionIndex() > 0 && typeCombo.getEnabled())
				//					createButton.setEnabled(true); // there is a text selection in the browser
			}

			@Override
			public void mouseDown(MouseEvent e) {
				if (debug) Log.finer("MOUSE DOWN"); //$NON-NLS-1$
				dblClick = false;
				t = e.time;
				x = e.x;
				y = e.y;
			}

			@Override
			public void mouseUp(MouseEvent e) {
				if (dblClick) { // doucle click raised by mouseDoubleClick
					dblClick = false;
					return; // stop right now ! :-o
				}
				if (debug) Log.finer("MOUSE UP"); //$NON-NLS-1$
				EditionPanel panel = editor.getEditionPanel(0);
				if (panel == null) return;

				if (typeCombo.getSelectionIndex() <= 0) {
					//					createButton.setEnabled(false);
					deleteSchemaButton.setEnabled(false);
					//					editButton.setEnabled(false);
					return; // nothing to do with annotations
				}

				// filter click that are not a left simple click (no drag)
				//System.out.println("click count="+e.count+" button="+e.button+" time="+e.time+" diff="+(e.time-t)+" dist="+(Math.abs(e.x - x) + Math.abs(e.y - y)));
				if (e.count > 1) {
					if (debug) Log.finer(" DOUBLE CLICK"); //$NON-NLS-1$
					//System.out.println("not a simple click");
					return;
				}

				if (Math.abs(e.x - x) + Math.abs(e.y - y) > 7) {
					if (debug) Log.finer(" DRAG"); //$NON-NLS-1$

					String[] ids = panel.getWordSelection(); // may be null
					if (ids != null && ids.length > 0 && typeCombo.getItemCount() > 1 && typeCombo.getEnabled()) {

						if (navigationField.getSelectionIndex() > 0 && ids[0] != null) {
							deleteSchemaButton.setEnabled(true);
							//							editButton.setEnabled(true);
						}
						//						
						//						createButton.setEnabled(id != null);
					}
					//panel.getBrowser().forceFocus();
					// extend selection to word boundaries
					//					String id = panel.getWordSelection(); // may be null
					//					if (id != null) {
					//						panel.expandSelectionTo(id);
					//					}
					return;
				}
				if (e.button != 1) {
					if (debug) Log.finer(" NO LEFT"); //$NON-NLS-1$
					//System.out.println("not a left click");
					return;
				}
				if ((e.time - t) > 150) {
					if (debug) Log.finer(" LONG"); //$NON-NLS-1$
					//System.out.println("not a short click");
					return;
				}

				String[] ids = panel.getWordSelection(); // may be null

				if (ids == null) { // clear selection
					if (debug) Log.finer(" NO WORD SELECTION"); //$NON-NLS-1$
					//					navigationField.setSelectionIndex(0); // clear selection
					//					onIdentifiantComboSelected(null);
					return;
				}

				org.txm.searchengine.cqp.corpus.CQPCorpus c = SchemaToolbar.this.editor.getCorpus();

				try {
					if (debug) Log.finer(" HIGHLIGHTING ANNOT IF ANY AT " + ids[0]); //$NON-NLS-1$
					tryHighlightingWord(ids[0], c);
				}
				catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		};
		return editorMouseListener;
	}

	public KeyListener getEditorKeyListener() {
		if (editorKeyListener != null) return editorKeyListener;
		editorKeyListener = new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				//System.out.println("Key: "+e.keyCode);
				int mask = SWT.CTRL;
				if (Util.isMac()) mask = SWT.COMMAND;

				//				if ((e.stateMask & SWT.ALT) == SWT.ALT) {
				//					if (e.keyCode == SWT.ARROW_UP) {
				//						selectPreviousUnite();
				//					} else if (e.keyCode == SWT.ARROW_DOWN) {
				//						selectNextUnite();
				//					} else if (e.keyCode == SWT.HOME) {
				//						selectFirstUnite();
				//					} else if (e.keyCode == SWT.END) {
				//						selectLastUnite();
				//					}
				//				} else 
				if (((e.stateMask & mask) == mask)) {
					if (e.keyCode == SWT.ARROW_UP) {
						selectPrevious();
					}
					else if (e.keyCode == SWT.ARROW_DOWN) {
						selectNext();
					}
					else if (e.keyCode == SWT.HOME) {
						selectFirst();
					}
					else if (e.keyCode == SWT.END) {
						selectLast();
					}
				}
			}
		};
		return editorKeyListener;
	}

	public MenuListener getMenuListener() {
		if (menuListener != null) return menuListener;

		this.menuListener = new MenuListener() {

			HashSet<MenuItem> entries = new HashSet<MenuItem>();

			@Override
			public void menuHidden(MenuEvent e) {
			}

			@Override
			public void menuShown(MenuEvent e) {

				EditionPanel panel = editor.getEditionPanel(0);
				if (panel == null) return;
				Menu menu = panel.getBrowser().getMenu();

				for (MenuItem item : entries)
					item.dispose();

				int[] startend = getSelectionStartEndWordPositions();
				//System.out.println("Word under mouse: "+id);
				if (startend != null && startend.length == 2) {
					// get the word start end positions
					ArrayList<Unite> unitEntries = new ArrayList<Unite>(); // stores units  covering the word
					ArrayList<Integer> noEntries = new ArrayList<Integer>(); // stores units  covering the word
					int i = 1;
					Unite[] unites = schemas[navigationField.getSelectionIndex() - 1].getUnitesSousjacentes();
					for (Unite unit : unites) {
						if (unit.getDeb() > startend[0]) break; // don't go further
						if (unit.getFin() < startend[0]) continue;
						unitEntries.add(unit);
						noEntries.add(i);
						i++;
					}

					if (unitEntries.size() > 0) {
						MenuItem item = new MenuItem(menu, SWT.SEPARATOR);
						entries.add(item);
					}

					for (i = 0; i < unitEntries.size(); i++) {
						final Unite unit = unitEntries.get(i);
						final Integer unitNo = Arrays.binarySearch(unites, unit) + 1;

						MenuItem item = new MenuItem(menu, SWT.NONE);
						item.setText(NLS.bind(Messages.selectP0P1, unit.getType(), unitNo)); //$NON-NLS-1$
						item.addSelectionListener(new SelectionListener() {

							@Override
							public void widgetDefaultSelected(SelectionEvent e) {
							}

							@Override
							public void widgetSelected(SelectionEvent e) {
								highlightUnite(unit);
								updatePropertiesView(unit);
							}
						});
						entries.add(item);
					}
				}
			}
		};

		return menuListener;
	}

	protected void highlightType() {

		if (schemas == null) return;
		if (schemas.length == 0) return;

		// highlight all units of all schemas ? -> too much

	}

	protected void highlightSchema(Schema schema) {
		//clearHighlight();

		unites = schema.getUnitesSousjacentes();

		if (unites == null) return;
		if (unites.length > 0) {

			Unite unite0 = schema.getUnite0();
			int n2 = 0;
			for (int i = 0; i < unites.length; i++) {
				for (int p = unites[i].getDeb(); p <= unites[i].getFin(); p++) {
					n2++;
				}
			}
			int allpositions[] = new int[n2];
			n2 = 0;
			for (int i = 0; i < unites.length; i++) {
				for (int p = unites[i].getDeb(); p <= unites[i].getFin(); p++) {
					allpositions[n2++] = p;
				}
			}

			org.txm.searchengine.cqp.corpus.CQPCorpus c = SchemaToolbar.this.editor.getCorpus();
			try {
				if (previousSelectedSchemaIDS != null) {
					editor.removeHighlightWordsById(highlighted_unit_color, previousSelectedSchemaIDS);
					previousSelectedSchemaIDS = null; // release memory
				}
				String ids2[] = CQPSearchEngine.getCqiClient().cpos2Str(c.getProperty("id").getQualifiedName(), allpositions); //$NON-NLS-1$
				previousSelectedSchemaIDS = Arrays.asList(ids2);

				editor.addHighlightWordsById(highlighted_unit_color, previousSelectedSchemaIDS);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void highlightUnite(Unite unite) {
		int start = unite.getDeb();
		int end = unite.getFin();
		int positions[] = new int[end - start + 1];
		int n = 0;
		for (int p = start; p <= end; p++) {
			positions[n++] = p;
		}
		org.txm.searchengine.cqp.corpus.CQPCorpus c = SchemaToolbar.this.editor.getCorpus();

		try {
			if (previousSelectedUniteIDS != null) {
				editor.removeHighlightWordsById(selected_unit_color, previousSelectedUniteIDS);
				editor.removeFontWeightWordsById(previousSelectedUniteIDS);
				previousSelectedUniteIDS = null; // release memory
			}

			String ids[] = CQPSearchEngine.getCqiClient().cpos2Str(c.getProperty("id").getQualifiedName(), positions); //$NON-NLS-1$
			//System.out.println("Highlight words with ids="+Arrays.toString(ids));

			positions = new int[] { positions[0] };
			Property text_id = c.getStructuralUnit("text").getProperty("id"); //$NON-NLS-1$ //$NON-NLS-2$
			int[] text_strucs = CQPSearchEngine.getCqiClient().cpos2Struc(text_id.getQualifiedName(), positions);
			String[] text_ids = CQPSearchEngine.getCqiClient().struc2Str(text_id.getQualifiedName(), text_strucs);

			String firstWordId = ids[0];
			String firstWordTextId = text_ids[0];

			EditionPanel panel = editor.getEditionPanel(0);
			Edition e = panel.getEdition();
			Project corpus = e.getText().getProject();
			Text newText = corpus.getText(firstWordTextId); // get the new text
			Edition newEdition = newText.getEdition(e.getName()); // get the edition of the new text
			Page newPage = newEdition.getPageForWordId(firstWordId); // find out the page containing the word

			previousSelectedUniteIDS = Arrays.asList(ids);
			//			System.out.println("HU: "+previousSelectedUniteIDS);
			editor.addHighlightWordsById(selected_unit_color, previousSelectedUniteIDS);
			editor.addFontWeightWordsById(EditionPanel.WEIGHT_BOLD, previousSelectedUniteIDS);

			if (previousSelectedUniteIDS.size() > 0) editor.setFocusedWordID(previousSelectedUniteIDS.get(0));
			//System.out.println("Word page= "+p+" current="+panel.getCurrentPage());
			if (panel.getCurrentPage() != newPage || panel.getCurrentText() != newText) { // do we need to change page ?
				//System.out.println("PAGE&TEXT CHANGED");
				editor.goToPage(newText.getName(), newPage.getName());
			}
			else { // just reload the page to highlight words
				//editor.updateWordStyles();
			}

		}
		catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	protected void onDeleteSchemaComboSelected(SelectionEvent e) {
		int iSchema = navigationField.getSelectionIndex();
		if (iSchema <= 0) return;
		//System.out.println("Delete unite at i="+i);
		Schema schema = schemas[iSchema - 1];

		String id = navigationField.getText();

		ConfirmDialog dialog = new ConfirmDialog(Display.getCurrent().getActiveShell(), "org.txm.annotation.urs.delete", Messages.bind(Messages.deleteSchemaP0, id), Messages.bind(Messages.doyouReallyWantToDeleteTheP0Schema, id )); //$NON-NLS-1$
		if (dialog.open() == ConfirmDialog.CANCEL) return;

		//System.out.println("unite "+unite.toString());
		SchemaToolbar.this.vue.getCorpus().supSchema(schema);
		maincorpus.setIsModified(true);
		reloadIdentifiants();
		onIdentifiantComboSelected(null);

		if (typeCombo.getSelectionIndex() > 0) {
			highlightType(); // reload other unit highlight
		}
		editor.fireIsDirty();
		if (e != null) editor.updateWordStyles();
	}

	protected void onDeleteUniteComboSelected(SelectionEvent e) {
		int iSchema = navigationField.getSelectionIndex();
		if (iSchema <= 0) return;
		//System.out.println("Delete unite at i="+i);
		Schema schema = schemas[iSchema - 1];

		int i = subNavigationField.getSelectionIndex();
		if (i <= 0) return;
		//System.out.println("Delete unite at i="+i);
		Unite unite = unites[i - 1];

		String id = subNavigationField.getText();

		ConfirmDialog dialog = new ConfirmDialog(Display.getCurrent().getActiveShell(), "org.txm.annotation.urs.delete", //$NON-NLS-1$
				Messages.bind(Messages.removeUnitP0, id),
				Messages.bind(Messages.doyouReallyWantToRemoveTheP0UnitFromtheSchema, id));
		if (dialog.open() == ConfirmDialog.CANCEL) return;

		//System.out.println("unite "+unite.toString());
		SchemaToolbar.this.vue.getCorpus().supUnite(unite);
		maincorpus.setIsModified(true);
		highlightSchema(schema);
		reloadIdentifiantsUnites();
		onIdentifiantUniteComboSelected(null);
		editor.fireIsDirty();
		if (e != null) editor.updateWordStyles();
	}

	protected void onIdentifiantUniteComboSelected(Event e) {
		if (unites == null) return;

		int iUnite = subNavigationField.getSelectionIndex();
		if (iUnite > 0) {
			Unite unite = unites[iUnite - 1];
			highlightUnite(unite);
			deleteUniteButton.setEnabled(true);
		}
		else {
			clearUnitesHighlight();
			deleteUniteButton.setEnabled(false);
		}

		if (e != null) editor.updateWordStyles();
	}

	protected void onIdentifiantComboSelected(SelectionEvent e) {
		// Log.finer("Identifiants SelectionListener: "+navigationField.getSelectionIndex());
		int iSchema = navigationField.getSelectionIndex();
		if (iSchema <= 0) {
			if (previousSelectedSchemaIDS != null) {
				editor.removeHighlightWordsById(highlighted_unit_color, previousSelectedSchemaIDS);
				previousSelectedSchemaIDS = null; // release memory
			}

			highlightType();
			deleteSchemaButton.setEnabled(false);
			subNavigationField.setEnabled(false);
			subNavigationField.setSelectionIndex(0);
			deleteUniteButton.setEnabled(false);
		}
		else {
			Schema schema = schemas[iSchema - 1];
			clearHighlight();
			highlightSchema(schema);
			reloadIdentifiantsUnites();
			updatePropertiesView(schema);
			deleteSchemaButton.setEnabled(true);
		}

		if (e != null) editor.updateWordStyles();
	}

	protected void onTypeComboSelected(SelectionEvent e) {
		// Log.finer("Types SelectionListener: "+typeCombo.getSelectionIndex());
		reloadIdentifiants();
		if (typeCombo.getSelectionIndex() > 0) {
			//String type = unitesAVoir[typeCombo.getSelectionIndex()-1];
			highlightType();
			navigationField.setEnabled(true);
			if (schemas.length > 0) {
				navigationField.selectFirst();
				onIdentifiantComboSelected(null);
			}
		}
		else {
			clearHighlight();
			unites = null;
			navigationField.setEnabled(false);
		}

		deleteSchemaButton.setEnabled(false);
		if (e != null) editor.updateWordStyles();
	}

	/**
	 * reload the identifiants combo
	 * 
	 */
	public void reloadIdentifiants() {
		// Log.finer("Reloading identifiants combo values.");
		String type0 = typeCombo.getText();
		schemas = vue.getSchemasAVoir(type0);
		String[] identifiants = vue.getIdSchemas(type0, schemas);
		navigationField.setIdentifiants(identifiants, type0, Schema.class);
		navigationField.setSelectionIndex(0);
	}

	/**
	 * reload the identifiants combo
	 * 
	 */
	public void reloadIdentifiantsUnites() {
		// Log.finer("Reloading unites combo values.");
		int iSchema = navigationField.getSelectionIndex();
		if (iSchema <= 0) {
			subNavigationField.setIdentifiants(new String[0], null, null);
			unites = null;
			return;
		}
		else {
			Schema schema = schemas[iSchema - 1];
			unites = schema.getUnitesSousjacentes();
			String[] identifiants = new String[unites.length];
			for (int i = 0; i < unites.length; i++) {
				identifiants[i] = "" + (i + 1);//vue.getIdElement(unites[i]); //$NON-NLS-1$
			}
			subNavigationField.setIdentifiants(identifiants, null, null);

			//		String[] items = new String[identifiants.length +1];
			//		System.arraycopy(identifiants, 0, items, 1, identifiants.length);
			//		//System.out.println("Identifiants: "+Arrays.toString(identifiants));
			//		items[0] = "< identifiant >";
			//		identifiantCombo.setItems(items);
			if (identifiants.length > 0) {
				subNavigationField.setSelectionIndex(1);
				subNavigationField.setEnabled(true);
			}
			else {
				subNavigationField.setSelectionIndex(0);
				subNavigationField.setEnabled(false);
			}
			//identifiantCombo.layout();
		}
	}

	public void reloadTypeSchemas() {

		schemasAVoir = vue.getTypesSchemasAVoir();
		String[] items = new String[schemasAVoir.length + 1];
		System.arraycopy(schemasAVoir, 0, items, 1, schemasAVoir.length);
		items[0] = Messages.chevSchemaTypeChev; //$NON-NLS-1$
		typeCombo.setItems(items);
		if (items.length > 0) {
			typeCombo.select(1);
		}
		typeCombo.layout();

		onTypeComboSelected(null);
	}

	protected void selectSchemaInIdentifants(Schema schema) {
		for (int i = 0; i < schemas.length; i++) {
			//System.out.println("VS "+unites[i]);
			if (schemas[i] == schema) {
				navigationField.setSelectionIndex(i + 1);
				return; // no need to go further
			}
		}
	}

	public void setEnable(boolean enable) {

		l.setEnabled(enable);
		typeCombo.setEnabled(enable);
		//		createButton.setEnabled(enable);
		deleteSchemaButton.setEnabled(enable);
		//		editButton.setEnabled(enable);
		navigationField.setEnabled(enable);
	}

	@Override
	public void traiterEvent(Message e) {

		if (annotationArea.isDisposed()) return;

		if (e.getType().equals(TypeMessage.MODIF_STRUCTURE)) {
			//System.out.println("SchemaToolbar.traiterEvent: Update toolbar ");
			SchemaToolbar.this.annotationArea.getDisplay().syncExec(new Runnable() {

				@Override
				public void run() {
					setEnable(!analecCorpus.isVide());
					//reloadUnits();
				}
			});
		}
		else if (e.getType().equals(TypeMessage.MODIF_ELEMENT)) {
			//System.out.println("SchemaToolbar.traiterEvent: Update toolbar ");
			SchemaToolbar.this.annotationArea.getDisplay().syncExec(new Runnable() {

				@Override
				public void run() {
					//reloadUnits();
				}
			});
		}
	}

	protected void tryHighlightingWord(String id, org.txm.searchengine.cqp.corpus.CQPCorpus c) throws CqiClientException {

		EditionPanel panel = editor.getEditionPanel(0);
		if (panel == null) return;
		if (id != null) {

		}
		QueryResult r = c.query(new CQLQuery("[_.text_id=\"" + CQLQuery.addBackSlash(panel.getEdition().getText().getName()) + "\" & id=\"" + id + "\"]"), "TMP", false); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		List<Match> matches = r.getMatches();
		if (matches.size() == 0) {
			Log.warning(NLS.bind(Messages.errorNoWordFoundForIdP0, id)); //$NON-NLS-1$
			return;
		}
		int start = matches.get(0).getStart();
		//System.out.println("start="+start);
		r.drop();
		Unite[] unites = schemas[navigationField.getSelectionIndex() - 1].getUnitesSousjacentes();
		for (Unite u : unites) {
			if (u.getDeb() <= start && start <= u.getFin()) {
				highlightUnite(u);
				updatePropertiesView(u);

				deleteSchemaButton.setEnabled(true);
				return;
			}
			else if (u.getFin() < start) { // no need to search further

			}
		}

		//	navigationField.setSelectionIndex(0);
		onIdentifiantComboSelected(null);
	}

	protected boolean updateUniteLimits(Unite unite, String id) throws CqiClientException {

		String[] ids = { id };
		EditionPanel panel = SchemaToolbar.this.editor.getEditionPanel(0);
		String text_name = panel.getEdition().getText().getName();

		int[] startend = getStartEndWordPositions(maincorpus, ids, text_name);
		if (startend == null) return false;

		int pos = startend[0];
		Log.fine("Updating annotation limits: " + unite.getDeb() + " -> " + unite.getFin() + " with word id=" + id + " with pos=" + pos); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		if ((pos == unite.getDeb() || pos == unite.getFin()) && (unite.getFin() - unite.getDeb()) == 1) {
			analecCorpus.modifBornesUnite(unite, pos, pos);
		}
		else if (pos <= unite.getDeb()) {
			analecCorpus.modifBornesUnite(unite, pos, unite.getFin());
		}
		else if (pos >= unite.getFin()) {
			analecCorpus.modifBornesUnite(unite, unite.getDeb(), pos);
		}
		else if ((pos - unite.getDeb()) < (unite.getFin() - pos)) {
			analecCorpus.modifBornesUnite(unite, pos, unite.getFin());
		}
		else {
			analecCorpus.modifBornesUnite(unite, unite.getDeb(), pos);
		}

		return true;
	}

	public void selectNextUnite() {
		subNavigationField.selectNext();
	}

	public void selectPreviousUnite() {
		subNavigationField.selectPrevious();
	}

	public void selectFirstUnite() {
		subNavigationField.selectFirst();
	}

	public void selectLastUnite() {
		subNavigationField.selectLast();
	}

	@Override
	public void selectNext() {
		navigationField.selectNext();
	}

	@Override
	public void selectPrevious() {
		navigationField.selectPrevious();
	}

	@Override
	public void selectFirst() {
		navigationField.selectFirst();
	}

	@Override
	public void selectLast() {
		navigationField.selectLast();
	}

	@Override
	public void select10Previous() {
		navigationField.select10Previous();
	}

	@Override
	public void select10Next() {
		navigationField.select10Next();
	}

	@Override
	public void select(Element currentElement) {
		if (currentElement.getClass() == Schema.class) {
			int i = Arrays.binarySearch(schemas, currentElement);
			if (i >= 0) navigationField.setSelectionIndex(i + 1);
		}
	}

	@Override
	public String getElementType() {
		if (typeCombo.isDisposed()) return ""; //$NON-NLS-1$
		return typeCombo.getText();
	}

	@Override
	public void clearHighlight() {
		clearSchemasHighlight();
		clearUnitesHighlight();
	}

	@Override
	public void onBackToText(Text text, String id) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean canAnnotateResult(TXMResult r) {
		if (r instanceof Text && r != null) {
			return true;
		}
		return false;
	}

	@Override
	public String getName() {
		return Messages.schemaAnnotation; //$NON-NLS-1$
	}

	@Override
	public boolean notifyStartOfCompute() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean notifyEndOfCompute() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void notifyEndOfRefresh() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void notifyStartOfCreatePartControl() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void notifyStartOfRefresh() throws Exception {
		// TODO Auto-generated method stub

	}

	ProgressListener pageProgressListener = null;

	@Override
	protected ProgressListener getPageReloadListener() {
		if (pageProgressListener != null) {
			return pageProgressListener;
		}

		pageProgressListener = new ProgressListener() {

			@Override
			public void completed(ProgressEvent event) {
				if (typeCombo.getSelectionIndex() >= 0) {
					highlightType();
				}
			}

			@Override
			public void changed(ProgressEvent event) {
			}
		};
		return pageProgressListener;
	}

	@Override
	public boolean needToUpdateIndexes() {
		return false;
	}

	@Override
	public boolean hasChanges() {
		return analecCorpus.isModifie();
	}

	@Override
	public boolean needToUpdateEditions() {
		return false;
	}
}
