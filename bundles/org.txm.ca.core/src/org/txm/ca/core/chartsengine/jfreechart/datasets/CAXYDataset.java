package org.txm.ca.core.chartsengine.jfreechart.datasets;

import org.jfree.data.xy.AbstractXYDataset;
import org.jfree.data.xy.XYDataset;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.statsengine.core.StatException;

/**
 * XY data set for storing coordinates and frequencies of CA plots.
 * 
 * @author sjacquot
 *
 */
// TODO: some CA data are duplicated in this data set. We could directly use the CA methods here
// rather than copy some data but the performances due off access may be altered, however we would gain memory.
public class CAXYDataset extends AbstractXYDataset implements XYDataset {

	/**
	 * The linked CA result.
	 */
	protected CA ca;

	/**
	 * The row labels.
	 */
	protected String[] rowLabels;

	/**
	 * The column labels.
	 */
	protected String[] columnLabels;


	/**
	 * The row coordinates.
	 */
	protected double rowCoordinates[][];

	/**
	 * The column coordinates.
	 */
	protected double columnCoordinates[][];


	/**
	 * The row frequencies.
	 */
	protected int[] rowFrequencies;


	/**
	 * The column frequencies.
	 */
	protected int[] columnFrequencies;

	/**
	 * First axis for point coordinates.
	 */
	protected int axis1;

	/**
	 * Second axis for point coordinates.
	 */
	protected int axis2;


	/**
	 * Creates a data set from the specified CA result.
	 * 
	 * @param ca
	 * @throws StatException
	 */
	public CAXYDataset(CA ca) throws Exception {

		this.ca = ca;

		// Axis/dimensions
		this.axis1 = ca.getFirstDimension() - 1;
		this.axis2 = ca.getSecondDimension() - 1;

		// Labels
		this.rowLabels = ca.getRowNames();
		this.columnLabels = ca.getColNames();

		// Coordinates
		this.rowCoordinates = ca.getRowsCoords();
		this.columnCoordinates = ca.getColsCoords();

		// Frequencies
		this.rowFrequencies = ca.getLexicalTable().getRowMarginsVector().asIntArray();
		this.columnFrequencies = ca.getLexicalTable().getColMarginsVector().asIntArray();

	}

	public double[][] getRowCoordinates() {
		return this.rowCoordinates;
	}

	public double[][] getColCoordinates() {
		return this.columnCoordinates;
	}


	@Override
	public Number getX(int series, int item) {
		if (item == -1) {
			//System.out.println("CAXYDataset.getX()");
		}
		// Rows
		if (series == 0) {
			return this.rowCoordinates[item][this.axis1];
		}
		// Cols
		else {
			return this.columnCoordinates[item][this.axis1];
		}
	}

	@Override
	public Number getY(int series, int item) {
		// Rows
		if (series == 0) {
			return this.rowCoordinates[item][this.axis2];
		}
		// Cols
		else {
			return this.columnCoordinates[item][this.axis2];
		}

	}


	/**
	 * Gets the maximum X coordinates value in the specified series.
	 * 
	 * @param series
	 * @return
	 */
	public double getMaxX(int series) {
		return this.getMaxValue(series, this.axis1);
	}


	/**
	 * Gets the maximum Y coordinates value in the specified series.
	 * 
	 * @param series
	 * @return
	 */
	public double getMaxY(int series) {
		return this.getMaxValue(series, this.axis2);
	}


	/**
	 * Gets the maximum value in the specified series according to the specified axis.
	 * 
	 * @param series
	 * @param axis
	 * @return
	 */
	public double getMaxValue(int series, int axis) {

		double maxValue = 0;
		double tmpMaxValue;

		double[][] coordinates = this.rowCoordinates;
		if (series != 0) {
			coordinates = this.columnCoordinates;
		}

		for (int i = 0; i < coordinates.length; i++) {
			tmpMaxValue = coordinates[i][axis];
			if (tmpMaxValue > maxValue) {
				maxValue = tmpMaxValue;
			}
		}

		return maxValue;
	}



	/**
	 * Gets the minimum X coordinates value in the specified series.
	 * 
	 * @param series
	 * @return
	 */
	public double getMinX(int series) {
		return this.getMinValue(series, this.axis1);
	}


	/**
	 * Gets the minimum Y coordinates value in the specified series.
	 * 
	 * @param series
	 * @return
	 */
	public double getMinY(int series) {
		return this.getMinValue(series, this.axis2);
	}


	/**
	 * Gets the minimum value in the specified series according to the specified axis.
	 * 
	 * @param series
	 * @param axis
	 * @return
	 */
	public double getMinValue(int series, int axis) {

		double minValue = 0;
		double tmpMinValue;

		double[][] coordinates = this.rowCoordinates;
		if (series != 0) {
			coordinates = this.columnCoordinates;
		}

		for (int i = 0; i < coordinates.length; i++) {
			tmpMinValue = coordinates[i][axis];
			if (tmpMinValue < minValue) {
				minValue = tmpMinValue;
			}
		}

		return minValue;
	}


	@Override
	public int getSeriesCount() {
		return 2;
	}

	@Override
	public int getItemCount(int series) {
		// Rows
		if (series == 0) {
			return this.rowCoordinates.length;
		}
		// Cols
		else {
			return this.columnCoordinates.length;
		}
	}

	/**
	 * Gets the key of the specified series.
	 */
	public Comparable<String> getSeriesKey(int series) {
		// Rows
		if (series == 0) {
			return CACoreMessages.rows;
		}
		// Cols
		else {
			return CACoreMessages.columns;
		}
	}

	/**
	 * Gets the label of a point.
	 * 
	 * @param series
	 * @param item
	 * @return
	 */
	public String getLabel(int series, int item) {
		// Rows
		if (series == 0) {
			return this.rowLabels[item];
		}
		// Cols
		else {
			return this.columnLabels[item];
		}
	}


	/**
	 * Gets the indices of point from their labels.
	 * 
	 * @param series
	 * @param labels
	 * @return
	 */
	public int[] getLabelIndices(int series, String[] labels) {
		int[] indices = new int[labels.length];

		// Rows
		if (series == 0) {
			for (int i = 0, j = 0; i < this.rowLabels.length; i++) {
				for (int k = 0; k < labels.length; k++) {
					if (this.rowLabels[i].equals(labels[k])) {
						indices[j++] = i;
					}
				}
			}
		}
		// Cols
		else {
			for (int i = 0, j = 0; i < this.columnLabels.length; i++) {
				for (int k = 0; k < labels.length; k++) {
					if (this.columnLabels[i].equals(labels[k])) {
						indices[j++] = i;
					}
				}
			}
		}

		return indices;
	}

	/**
	 * Gets the indices of point from their labels keeping the specified label order.
	 * 
	 * @param series
	 * @param labels
	 * @return
	 */
	public int[] getOrderedLabelIndices(int series, String[] labels) {
		int[] indices = new int[labels.length];

		// Rows
		if (series == 0) {
			for (int i = 0, k = 0; i < labels.length; i++) {
				for (int j = 0; j < this.rowLabels.length; j++) {
					if (this.rowLabels[j].equals(labels[i])) {
						indices[k++] = j;
						continue;
					}
				}
			}
		}
		// Cols
		else {
			for (int i = 0, k = 0; i < labels.length; i++) {
				for (int j = 0; j < this.columnLabels.length; j++) {
					if (this.columnLabels[j].equals(labels[i])) {
						indices[k++] = j;
						continue;
					}
				}
			}
		}

		return indices;
	}



	/**
	 * Gets the frequency of the specified series and items.
	 * If <code>series</code> is equal to 0, the method returns the frequency of the specified row, otherwise the sum of the occurrences of the specified column.
	 * 
	 * @param series
	 * @param item
	 * @return
	 */
	public String getFrequency(int series, int item) {
		// Rows
		if (series == 0) {
			return String.valueOf(this.rowFrequencies[item]);
		}
		// Columns
		else {
			return String.valueOf(this.columnFrequencies[item]);
		}
	}

	/**
	 * @param axis1 the axis1 to set
	 */
	public void setAxis1(int axis1) {
		this.axis1 = axis1 - 1;
	}

	/**
	 * @param axis2 the axis2 to set
	 */
	public void setAxis2(int axis2) {
		this.axis2 = axis2 - 1;
	}

	public int getAxis1() {

		return axis1;
	}

	public int getAxis2() {

		return axis2;
	}

}
