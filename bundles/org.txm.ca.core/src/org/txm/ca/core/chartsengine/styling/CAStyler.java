package org.txm.ca.core.chartsengine.styling;

import java.util.HashMap;
import java.util.regex.Pattern;

import org.txm.core.results.ResultStyler;

/**
 * Gives styles for rows and cols of a CAs
 * 
 * TODO replace with Styler AND return a list of StylingRules instead
 */
public interface CAStyler extends ResultStyler {

	/**
	 * the row styles, see styles names in the CAItemStyleRenderer
	 */
	public HashMap<Pattern, HashMap<String, String>> getRowStyles();

	/**
	 * the cols styles, see styles names in the CAItemStyleRenderer
	 */
	public HashMap<Pattern, HashMap<String, String>> getColStyles();

	/**
	 * for messages
	 */
	public String getName();

	/**
	 * Helper method
	 */
	public default HashMap<String, HashMap<Pattern, HashMap<String, String>>> getStyles() {
		HashMap<String, HashMap<Pattern, HashMap<String, String>>> all = new HashMap<String, HashMap<Pattern, HashMap<String, String>>>();
		all.put("rows", getRowStyles()); //$NON-NLS-1$
		all.put("cols", getColStyles()); //$NON-NLS-1$
		return all;
	}
	
}
