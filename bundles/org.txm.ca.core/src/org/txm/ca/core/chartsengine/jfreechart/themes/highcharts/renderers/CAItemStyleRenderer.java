package org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.renderers;

import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.chart.renderer.AbstractRenderer;
import org.jfree.chart.util.ShapeUtils;
import org.jfree.data.xy.XYDataset;
import org.txm.ca.core.chartsengine.jfreechart.datasets.CAXYDataset;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.chartsengine.core.styling.StylingRule;
import org.txm.chartsengine.core.styling.StylingSheet;
import org.txm.chartsengine.core.styling.instructions.StylingInstruction;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers.ItemSelectionXYLineAndShapeRenderer;
import org.txm.utils.TableReader;
import org.txm.utils.logger.Log;


public class CAItemStyleRenderer extends ItemSelectionXYLineAndShapeRenderer {

	private static final long serialVersionUID = -8433484420368294870L;

	/**
	 *  dimensions scaling factor (the same used in default theme for the replaced shapes have same dimension than non-replaced)
	 */
	float itemShapesScalingFactor = 1.2f;

	float circleSize = 6.4f * itemShapesScalingFactor;

	HashMap<String, Shape> shapes;

	/**
	 * stores col labels replacements
	 */
	HashMap<Integer, String> colLabelStyle = new HashMap<Integer, String>();

	/**
	 * stores col index to be hidden
	 */
	HashSet<Integer> colHiddenStyle = new HashSet<Integer>(); // true false
	
	/**
	 * stores font size factors to use in columns labels
	 */
	HashMap<Integer, Double> colFontSizeStyle = new HashMap<Integer, Double>();

	/**
	 * stores font colors to use in columns labels
	 */
	HashMap<Integer, Color> colFontColorStyle = new HashMap<Integer, Color>();

	/**
	 * stores font alpha color to use in columns labels
	 */
	HashMap<Integer, Double> colFontAlphaStyle = new HashMap<Integer, Double>();

	/**
	 * stores font family to use in columns labels
	 */
	HashMap<Integer, String> colFontFamilyStyle = new HashMap<Integer, String>();

	/**
	 * stores font style to use in columns labels (1 = bold, 2 = italic, 3 = bold + italic)
	 */
	HashMap<Integer, Integer> colFontStyleStyle = new HashMap<Integer, Integer>();
	
	/**
	 * stores the size factors to use in columns shapes
	 */
	HashMap<Integer, Double> colPointSizeStyle = new HashMap<Integer, Double>();

	/**
	 * stores font colors to use in columns shapes
	 */
	HashMap<Integer, Color> colPointColorStyle = new HashMap<Integer, Color>();

	/**
	 * stores font alpha colors to use in columns shapes
	 */
	HashMap<Integer, Double> colPointAlphaStyle = new HashMap<Integer, Double>();

	/**
	 * stores shapes to use in columns
	 */
	HashMap<Integer, Shape> colPointShapeStyle = new HashMap<Integer, Shape>(); // circle, square, triangle, etc.


	/**
	 * stores row labels replacements
	 */
	HashMap<Integer, String> rowLabelStyle = new HashMap<Integer, String>();

	/**
	 * Stores row indexes to be hidden.
	 */
	HashSet<Integer> rowHiddenStyle = new HashSet<Integer>();
	
	/**
	 * stores font size factors to use in rows labels
	 */
	HashMap<Integer, Double> rowFontSizeStyle = new HashMap<Integer, Double>();

	/**
	 * stores color values to replace in rows labels
	 */
	HashMap<Integer, Color> rowFontColorStyle = new HashMap<Integer, Color>();

	/**
	 * stores size factors to use in rows labels
	 */
	HashMap<Integer, Double> rowFontAlphaStyle = new HashMap<Integer, Double>();

	/**
	 * stores font family value to replace in rows labels
	 */
	HashMap<Integer, String> rowFontFamilyStyle = new HashMap<Integer, String>();

	/**
	 * stores the size factors to use in rows shapes
	 */
	HashMap<Integer, Double> rowPointSizeStyle = new HashMap<Integer, Double>();

	/**
	 * stores font color value to replace in rows shapes
	 */
	HashMap<Integer, Color> rowPointColorStyle = new HashMap<Integer, Color>();

	/**
	 * stores font alpha value to replace in rows shapes
	 */
	HashMap<Integer, Double> rowPointAlphaStyle = new HashMap<Integer, Double>();

	/**
	 * stores shapes to replace in rows
	 */
	HashMap<Integer, Shape> rowPointShapeStyle = new HashMap<Integer, Shape>(); // circle, square, triangle, etc.
	
	/**
	 * stores font style value to replace in rows labels (1 = bold, 2 = italic, 3 = bold + italic)
	 */
	HashMap<Integer, Integer> rowFontStyleStyle = new HashMap<Integer, Integer>();

	public static Area EMPTYAREA = new Area();
	
	/**
	 * 
	 * @return the number of hidden row and columns points based on the HIDDEN style values
	 */
	public int[] getNumberOfHiddenPoints() {
		return new int[] {rowHiddenStyle.size(), colHiddenStyle.size()};
	}
	
	/**
	 * 
	 * @param ca
	 * @param chart
	 */
	public CAItemStyleRenderer(CA ca, JFreeChart chart) {
		super(ca, chart);
	}

	@Override
	public void init() {

		super.init();

		this.setDefaultItemLabelsVisible(true);
		
		shapes = new HashMap<String, Shape>();
		shapes.put(StylingInstruction.VALUE_ID_SHAPE_DIAMOND, ShapeUtils.createDiamond(itemShapesScalingFactor * 3.4f));
		shapes.put(StylingInstruction.VALUE_ID_SHAPE_SQUARE, AbstractRenderer.DEFAULT_SHAPE);
		shapes.put(StylingInstruction.VALUE_ID_SHAPE_DISK, new Ellipse2D.Float(-circleSize / 2, -circleSize / 2, circleSize, circleSize));
		shapes.put(StylingInstruction.VALUE_ID_SHAPE_UP_TRIANGLE, ShapeUtils.createUpTriangle(itemShapesScalingFactor * 3.2f));
	}

	@Override
	public void initItemLabelGenerator() {
		String EMPTYLABEL= ""; //$NON-NLS-1$
		XYItemLabelGenerator generator = new XYItemLabelGenerator() {

			@Override
			public String generateLabel(XYDataset dataset, int series, int item) {
				if (series == 0 && rowHiddenStyle.contains(item)) {
					return EMPTYLABEL;
				}
				else if (series == 1 && colHiddenStyle.contains(item)) {
					return EMPTYLABEL;
				}
				else if (series == 0 && rowLabelStyle.get(item) != null) {
					return rowLabelStyle.get(item);
				}
				else if (series == 1 && colLabelStyle.get(item) != null) {
					return colLabelStyle.get(item);
				}
				else {
					CAXYDataset caDataset = (CAXYDataset) dataset;
					return caDataset.getLabel(series, item);
				}
			}
		};

		this.setDefaultItemLabelGenerator(generator);
	}
	
	@Override
	public Font getItemLabelFont(int series, int item) {

		// temporary disable the mouse over and selection scaling to not apply the styling on already scaled label
		//FIXME: SJ: tmp fix to manage mouse over scaling and size styling together,
		// until org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.renderers.CAItemStyleRenderer.
		// will be moved to a styler class outside the CA renderer.
		if (this.multipleItemsSelector.isMouseOverItem(series, item) || this.multipleItemsSelector.isSelectedItem(series, item)) {
			this.multipleItemsSelector.setSizeIncreaseEnabled(false);
		}
		
		Font font = super.getItemLabelFont(series, item);
		
		int style = font.getStyle();
		double size = font.getSize();
		String family = font.getFontName();
		
		// size scaling
		if (series == 0 && rowFontSizeStyle.get(item) != null) {
			size = size * rowFontSizeStyle.get(item);
		}
		else if (series == 1 && colFontSizeStyle.get(item) != null) {
			size = size * colFontSizeStyle.get(item);
		}
		
		// re-enable the item selector scaling and a manually apply the item selector scaling
		//FIXME: SJ: tmp fix to manage mouse over scaling and size styling together,
		// until org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.renderers.CAItemStyleRenderer.
		// will be moved to a styler class outside the CA renderer.
		if (this.multipleItemsSelector.isSizeIncreaseEnabled() == false) {
			this.multipleItemsSelector.setSizeIncreaseEnabled(true);
			size += this.multipleItemsSelector.getLabelSizeIncreaseValue();
		}
		
		// family
		if (series == 0 && rowFontFamilyStyle.get(item) != null) {
			family = rowFontFamilyStyle.get(item);
		}
		else if (series == 1 && colFontFamilyStyle.get(item) != null) {
			family = colFontFamilyStyle.get(item);
		}

		// style
		if (series == 0 && rowFontStyleStyle.get(item) != null) {
			style = rowFontStyleStyle.get(item);
		}
		else if (series == 1 && colFontStyleStyle.get(item) != null) {
			style = colFontStyleStyle.get(item);
		}

		return new Font(family, style, (int) size); // TODO use font registry
	}

	@Override
	public Shape getItemShape(int series, int item) {

		// Rows (series == 0), Cols (series == 1)
		if (series == 0 && rowHiddenStyle.contains(item)) {
			return EMPTYAREA; // TODO return null ?
		}
		else if (series == 1 && colHiddenStyle.contains(item)) {
			return EMPTYAREA; // TODO return null ?
		}
		
		// temporary disable the mouse over and selection scaling to not apply the styling on already scaled shape
		//FIXME: SJ: tmp fix to manage mouse over scaling and size styling together,
		// until org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.renderers.CAItemStyleRenderer.
		// will be moved to a styler class outside the CA renderer.
		if (this.multipleItemsSelector.isMouseOverItem(series, item) || this.multipleItemsSelector.isSelectedItem(series, item)) {
			this.multipleItemsSelector.setSizeIncreaseEnabled(false);
		}
		
		Shape shape = super.getItemShape(series, item);
		
		// shape replacement
		if (series == 0 && rowPointShapeStyle.get(item) != null) {
			shape = rowPointShapeStyle.get(item);
		}
		else if (series == 1 && colPointShapeStyle.get(item) != null) {
			shape = colPointShapeStyle.get(item);
		}

		// shape scaling
		double scalingFactor = 0;
		AffineTransform t = new AffineTransform();
		if (series == 0 && rowPointSizeStyle.get(item) != null) {
			scalingFactor = rowPointSizeStyle.get(item);
		}
		else if (series == 1 && colPointSizeStyle.get(item) != null) {
			scalingFactor = colPointSizeStyle.get(item);
		}
		if(scalingFactor != 0) {
			t.setToScale(scalingFactor, scalingFactor);
			shape = t.createTransformedShape(shape);
			
			// re-enable the item selector scaling and a manually apply the item selector scaling
			//FIXME: SJ: tmp fix to manage mouse over scaling and size styling together,
			// until org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.renderers.CAItemStyleRenderer.
			// will be moved to a styler class outside the CA renderer.
			this.multipleItemsSelector.setSizeIncreaseEnabled(true);
			scalingFactor = this.multipleItemsSelector.getLabelSizeIncreaseValue() / scalingFactor;
			t.setToScale(scalingFactor, scalingFactor);
		}
		
		return shape;
	}


	@Override
	public Paint getItemLabelPaint(int series, int item) {
		
		Color color = (Color) super.getItemLabelPaint(series, item);

		if (series == 0 && rowFontColorStyle.get(item) != null) {
			color = rowFontColorStyle.get(item);
		}
		else if (series == 1 && colFontColorStyle.get(item) != null) {
			color = colFontColorStyle.get(item);
		}

		if (series == 0 && rowFontAlphaStyle.get(item) != null) {
			double d = rowFontAlphaStyle.get(item);
			if (d > 255.0) {
				d = 255.0;
			}
			if (color == null) { // avoid NPE
				return color;
			}
			color = new Color(color.getRed(), color.getGreen(), color.getBlue(), (int) d);
		}
		else if (series == 1 && colFontAlphaStyle.get(item) != null) {
			double d = colFontAlphaStyle.get(item);
			if (d >= 255.0) {
				d = 255.0;
			}
			if (color == null) { // avoid NPE
				return color;
			}
			color = new Color(color.getRed(), color.getGreen(), color.getBlue(), (int) d);
		}

		return color;
	}
	
	@Override
	public Paint getItemPaint(int series, int item) {

		Color color = (Color) super.getItemPaint(series, item);

		// visible shapes mode
		if (!this.getResult().isShowPointShapes()) {
			return super.getItemPaint(series, item);
		}
		// custom colorstyling
		if (series == 0 && rowPointColorStyle.get(item) != null) {
			color = rowPointColorStyle.get(item);
		}
		else if (series == 1 && colPointColorStyle.get(item) != null) {
			color = colPointColorStyle.get(item);
		}

		// custom alpha color
		if (series == 0 && rowPointAlphaStyle.get(item) != null) {
//			System.err.println("CAItemStyleRenderer.getItemPaint() rowPointAlphaStyle.get(item).intValue(): " + rowPointAlphaStyle.get(item).intValue());
			color = new Color(color.getRed(), color.getGreen(), color.getBlue(), rowPointAlphaStyle.get(item).intValue());
		}
		else if (series == 1 && colPointAlphaStyle.get(item) != null) {
//			System.err.println("CAItemStyleRenderer.getItemPaint() colPointAlphaStyle.get(item).intValue(): " + colPointAlphaStyle.get(item).intValue());
			color = new Color(color.getRed(), color.getGreen(), color.getBlue(), colPointAlphaStyle.get(item).intValue());
		}

		return color;
	}
	
	
	
	
	
	protected Color rgbaStringToColor(String color) {

		try {
			Field field = Class.forName("java.awt.Color").getField(color); //$NON-NLS-1$
			return (Color) field.get(null);
		}
		catch (Exception e) {
			// e.printStackTrace();
		}

		String[] rgbColor = color.split(" "); //$NON-NLS-1$
		String alpha = "255"; //$NON-NLS-1$
		if (rgbColor.length > 2) {
			if (rgbColor.length > 3) {
				alpha = rgbColor[3];
			}
			return new Color(Integer.parseInt(rgbColor[0]), Integer.parseInt(rgbColor[1]), Integer.parseInt(rgbColor[2]), Integer.parseInt(alpha));
		}
		else {
			Log.warning(NLS.bind(CACoreMessages.errorInColorFormatForRGBOrRGBAStringP0, color));
			return new Color(0, 0, 0);
		}
	}

	/**
	 * Reset all style data to default : nothing is styled
	 */
	public void resetStyles() {

		colLabelStyle = new HashMap<Integer, String>();
		colHiddenStyle = new HashSet<Integer>(); // true false
		colFontSizeStyle = new HashMap<Integer, Double>();
		colFontColorStyle = new HashMap<Integer, Color>();
		colFontAlphaStyle = new HashMap<Integer, Double>();
		colFontFamilyStyle = new HashMap<Integer, String>();
		colPointSizeStyle = new HashMap<Integer, Double>();
		colPointColorStyle = new HashMap<Integer, Color>();
		colPointAlphaStyle = new HashMap<Integer, Double>();
		colPointShapeStyle = new HashMap<Integer, Shape>(); // circle, square, triangle, etc.
		colFontStyleStyle = new HashMap<Integer, Integer>();

		rowLabelStyle = new HashMap<Integer, String>();
		rowHiddenStyle = new HashSet<Integer>(); // true false
		rowFontSizeStyle = new HashMap<Integer, Double>();
		rowFontColorStyle = new HashMap<Integer, Color>();
		rowFontAlphaStyle = new HashMap<Integer, Double>();
		rowFontFamilyStyle = new HashMap<Integer, String>();
		rowPointSizeStyle = new HashMap<Integer, Double>();
		rowPointColorStyle = new HashMap<Integer, Color>();
		rowPointAlphaStyle = new HashMap<Integer, Double>();
		rowPointShapeStyle = new HashMap<Integer, Shape>(); // circle, square, triangle, etc.
		rowFontStyleStyle = new HashMap<Integer, Integer>();
	}

	/**
	 * Utils to fill the hidden style hashmap
	 * 
	 * @param style
	 * @param table
	 * @param minHidden
	 * @param field
	 */
	protected static void filterHiddenTable(HashSet<Integer> style, double[][] table, double minHidden, int field) {

		for (int i = 0; i < table.length; i++) {
			if (table[i][field] < minHidden) {
				style.add(i);
			}
		}
	}

	protected void filterHiddenArray(HashSet<Integer> style, int[] array, double minHidden) {

		double[] sortedArray = new double[array.length];
		System.arraycopy(array, 0, sortedArray, 0, array.length);
		Arrays.sort(sortedArray);
		int index = (int) (array.length * minHidden);

		for (int i = 0; i < array.length; i++) {
			if (array[i] < sortedArray[index]) {
				style.add(i);
			}
		}
	}

	protected void filterHiddenArray(HashSet<Integer> style, double[] array, double minHidden) {

		// System.out.println("Min="+minHidden);
		// System.out.println("Array="+Arrays.toString(array));
		double[] sortedArray = new double[array.length];
		System.arraycopy(array, 0, sortedArray, 0, array.length);
		Arrays.sort(sortedArray);
		int index = (int) (array.length * minHidden);

		for (int i = 0; i < array.length; i++) {
			if (array[i] < sortedArray[index]) {
				style.add(i);
			}
		}
	}

	protected void filterSizeArray(HashMap<Integer, Double> fontSizeStyle, HashMap<Integer, Double> pointSizeStyle, int[] array, double intensity, double cap) {

		double min = Integer.MAX_VALUE;
		double max = 0;
		for (double i : array) {
			if (min > i) min = i;
			if (max < i) max = i;
		}

		double ecart = max - min;
		for (int i = 0; i < array.length; i++) {
			double diff = array[i] - min;
			double perc = intensity * diff / ecart;
			if (cap > 0) perc = Math.min(perc, cap);
			fontSizeStyle.put(i, perc);
			pointSizeStyle.put(i, perc);
		}
	}

	protected void filterSizeArray(HashMap<Integer, Double> fontSizeStyle, HashMap<Integer, Double> pointSizeStyle, double[] array, double intensity, double cap) {

		double min = Integer.MAX_VALUE;
		double max = 0;
		for (double i : array) {
			if (min > i) min = i;
			if (max < i) max = i;
		}

		double ecart = max - min;
		for (int i = 0; i < array.length; i++) {
			double diff = array[i] - min;
			double perc = intensity * diff / ecart;
			if (cap > 0) perc = Math.min(perc, cap);
			fontSizeStyle.put(i, perc);
			pointSizeStyle.put(i, perc);
		}
	}

	protected void filterSizeTable(HashMap<Integer, Double> fontSizeStyle, HashMap<Integer, Double> pointSizeStyle, double[][] array, double intensity, int field, double cap) {

		double min = Integer.MAX_VALUE;
		double max = 0;
		for (double[] i : array) {
			if (min > i[field]) min = i[field];
			if (max < i[field]) max = i[field];
		}

		double ecart = max - min;
		for (int i = 0; i < array.length; i++) {
			double diff = array[i][field] - min;
			double perc = intensity * diff / ecart;
			if (cap > 0) perc = Math.min(perc, cap);
			fontSizeStyle.put(i, perc);
			pointSizeStyle.put(i, perc);
		}
	}

	public void updateRulesFromODS(File patternsODSFile, CA ca) throws Exception {

		boolean debug = true;
		ArrayList<ArrayList<String>> data = TableReader.readAsList(patternsODSFile, "rows");  // read from the "rows" sheet //$NON-NLS-1$
		// System.out.println(data);
		ArrayList<String> keys = data.get(0);
		if (!checkStyleColumns(keys)) {
			return;
		}
		HashMap<Pattern, HashMap<String, String>> row_styles = new HashMap<>(); // reformat data
		for (int i = 1; i < data.size(); i++) {
			ArrayList<String> h = data.get(i);
			HashMap<String, String> style = new HashMap<>(); // create style entry
			String s = h.get(0);
			row_styles.put(Pattern.compile(s), style); // with a regexp pattern as key

			// fill the style
			for (int j = 1; j < h.size(); j++) {
				style.put(keys.get(j), h.get(j));
			}
		}
		if (debug) {
			System.out.println("ROW STYLES=" + row_styles); //$NON-NLS-1$
		}

		data = TableReader.readAsList(patternsODSFile, "cols"); // read from the "cols" sheet //$NON-NLS-1$
		keys = data.get(0);
		if (!checkStyleColumns(keys)) {
			return;
		}
		HashMap<Pattern, HashMap<String, String>> col_styles = new HashMap<>(); // reformat data

		for (int i = 1; i < data.size(); i++) {
			ArrayList<String> h = data.get(i);
			HashMap<String, String> style = new HashMap<>(); // create style entry
			String s = h.get(0);
			col_styles.put(Pattern.compile(s), style); // with a regexp pattern as key
			// fill the style
			for (int j = 1; j < h.size(); j++) {
				style.put(keys.get(j), h.get(j));
			}
		}
		if (debug) {
			System.out.println("COL STYLES=" + col_styles); //$NON-NLS-1$
		}

		// http://txm.sourceforge.net/javadoc/TXM/TBX/org/txm/stat/engine/r/function/CA.html


		updateRulesFromMaps(row_styles, col_styles, ca);
	}

	/**
	 * styles names used when importeing style from hashmaps, table files, StylingSheet, etc.
	 */
	HashSet<String> names = new HashSet<String>(Arrays.asList(StylingInstruction.LABEL_PATTERN, StylingInstruction.LABEL_REPLACEMENT, 
			StylingInstruction.HIDDEN, StylingInstruction.SHAPE_SIZE, StylingInstruction.SHAPE_COLOR, StylingInstruction.SHAPE_ALPHA, StylingInstruction.SHAPE_REPLACEMENT,
			StylingInstruction.LABEL_SIZE, StylingInstruction.LABEL_SIZE, StylingInstruction.LABEL_ALPHA, StylingInstruction.LABEL_FONT_FAMILY, StylingInstruction.LABEL_STYLE));

	
	private boolean checkStyleColumns(ArrayList<String> keys) {

		if (!keys.get(0).equals(StylingInstruction.LABEL_PATTERN)) {
			Log.warning(NLS.bind(CACoreMessages.errorTheFirstColumnNameMustBeLabelPatternP0, keys));
			return false;
		}
		
		ArrayList<String> warnings = new ArrayList<>();
		for (String k : keys) {
			if (!names.contains(k)) {
				warnings.add(k);
			}
		}

		if (warnings.size() > 0) {
			Log.warning(NLS.bind(CACoreMessages.unknownStyleColumnsP0, StringUtils.join(warnings, ", "))); //$NON-NLS-1$
		}

		return true;
	}

	/**
	 * Converts StylingSheet rules to style maps
	 * 
	 * @param sheet
	 * @param ca
	 * @throws Exception
	 */
	public void updateRulesFromStylingsheet(StylingSheet sheet, CA ca) throws Exception {
		if (sheet == null) return;
		if (ca == null) return;

		String[] rows = ca.getRowNames();
		String[] cols = ca.getColNames();

		for (StylingRule rule : sheet.getRules()) {
			for (int i = 0; i < rows.length; i++) {
				rule.updateChartStyle(ca, 0, i,
						rowLabelStyle, rowHiddenStyle, rowPointSizeStyle, rowPointColorStyle, rowPointAlphaStyle, rowPointShapeStyle, rowFontSizeStyle, rowFontColorStyle, rowFontAlphaStyle,
						rowFontFamilyStyle, rowFontStyleStyle);
			}

			for (int i = 0; i < cols.length; i++) {
				rule.updateChartStyle(ca, 1, i,
						colLabelStyle, colHiddenStyle, colPointSizeStyle, colPointColorStyle, colPointAlphaStyle, colPointShapeStyle, colFontSizeStyle, colFontColorStyle, colFontAlphaStyle,
						colFontFamilyStyle, colFontStyleStyle);
			}
		}
		Log.fine(NLS.bind("Chart styles updated from styling sheet \"{0}\".", sheet.getName())); //$NON-NLS-1$
	}

	/**
	 * Converts Style key maps to Style index maps
	 * @param row_styles
	 * @param col_styles
	 * @param ca
	 * @throws Exception
	 */
	//TODO: SJ: factorize duplicated code.
	public void updateRulesFromMaps(HashMap<Pattern, HashMap<String, String>> row_styles, HashMap<Pattern, HashMap<String, String>> col_styles, CA ca) throws Exception {

		boolean debug = false;
		// SOME DATA
		String[] rows = ca.getRowNames();
		String[] cols = ca.getColNames();

		// prepare col style data for the dataset
		if (col_styles != null) {
			for (int i = 0; i < cols.length; i++) {
				for (Pattern p : col_styles.keySet()) {

					if (p.matcher(cols[i]).matches()) {
						HashMap<String, String> style = col_styles.get(p);
						if (style.get(StylingInstruction.LABEL_REPLACEMENT) != null && style.get(StylingInstruction.LABEL_REPLACEMENT).length() > 0) {
							colLabelStyle.put(i, style.get(StylingInstruction.LABEL_REPLACEMENT));
						}
						if (style.get(StylingInstruction.HIDDEN) != null && style.get(StylingInstruction.HIDDEN).toUpperCase() == "T") { //$NON-NLS-1$
							colHiddenStyle.add(i);
						}
						if (style.get(StylingInstruction.SHAPE_SIZE) != null && style.get(StylingInstruction.SHAPE_SIZE).length() > 0) {
							colPointSizeStyle.put(i, Double.parseDouble(style.get(StylingInstruction.SHAPE_SIZE)));
						}
						if (style.get(StylingInstruction.SHAPE_COLOR) != null && style.get(StylingInstruction.SHAPE_COLOR).length() > 0) {
							colPointColorStyle.put(i, rgbaStringToColor(style.get(StylingInstruction.SHAPE_COLOR)));
						}
						if (style.get(StylingInstruction.SHAPE_ALPHA) != null && style.get(StylingInstruction.SHAPE_ALPHA).length() > 0) {
							colPointAlphaStyle.put(i, Double.parseDouble(style.get(StylingInstruction.SHAPE_ALPHA)));
						}
						if (style.get(StylingInstruction.SHAPE_REPLACEMENT) != null && shapes.containsKey(style.get(StylingInstruction.SHAPE_REPLACEMENT))) {
							colPointShapeStyle.put(i, shapes.get(style.get(StylingInstruction.SHAPE_REPLACEMENT)));
						}
						if (style.get(StylingInstruction.LABEL_SIZE) != null && style.get(StylingInstruction.LABEL_SIZE).length() > 0) {
							colFontSizeStyle.put(i, Double.parseDouble(style.get(StylingInstruction.LABEL_SIZE)));
						}
						if (style.get(StylingInstruction.LABEL_SIZE) != null && style.get(StylingInstruction.LABEL_SIZE).length() > 0) {
							colFontColorStyle.put(i, rgbaStringToColor(style.get(StylingInstruction.LABEL_SIZE)));
						}
						if (style.get(StylingInstruction.LABEL_ALPHA) != null && style.get(StylingInstruction.LABEL_ALPHA).length() > 0) {
							colFontAlphaStyle.put(i, Double.parseDouble(style.get(StylingInstruction.LABEL_ALPHA)));
						}
						if (style.get(StylingInstruction.LABEL_FONT_FAMILY) != null && style.get(StylingInstruction.LABEL_FONT_FAMILY).length() > 0) {
							colFontFamilyStyle.put(i, style.get(StylingInstruction.LABEL_FONT_FAMILY));
						}
						if (style.get(StylingInstruction.LABEL_STYLE) != null && style.get(StylingInstruction.LABEL_STYLE).length() > 0) {
							colFontStyleStyle.put(i, Integer.parseInt(style.get(StylingInstruction.LABEL_STYLE)));
						}
					}
				}
			}
		}

		// prepare row style data for the dataset
		if (row_styles != null) {
			for (int i = 0; i < rows.length; i++) {
				for (Pattern p : row_styles.keySet()) {
					if (p.matcher(rows[i]).matches()) {
						HashMap<String, String> style = row_styles.get(p);
						if (style.get(StylingInstruction.HIDDEN) != null && style.get(StylingInstruction.HIDDEN).toUpperCase() == "T") { //$NON-NLS-1$
							rowHiddenStyle.add(i);
						}
						if (style.get(StylingInstruction.LABEL_REPLACEMENT) != null && style.get(StylingInstruction.LABEL_REPLACEMENT).length() > 0) {
							rowLabelStyle.put(i, style.get(StylingInstruction.LABEL_REPLACEMENT));
						}
						if (style.get(StylingInstruction.SHAPE_SIZE) != null && style.get(StylingInstruction.SHAPE_SIZE).length() > 0) {
							rowPointSizeStyle.put(i, Double.parseDouble(style.get(StylingInstruction.SHAPE_SIZE)));
						}
						if (style.get(StylingInstruction.SHAPE_COLOR) != null && style.get(StylingInstruction.SHAPE_COLOR).length() > 0) {
							rowPointColorStyle.put(i, rgbaStringToColor(style.get(StylingInstruction.SHAPE_COLOR)));
						}
						if (style.get(StylingInstruction.SHAPE_ALPHA) != null && style.get(StylingInstruction.SHAPE_ALPHA).length() > 0) {
							rowPointAlphaStyle.put(i, Double.parseDouble(style.get(StylingInstruction.SHAPE_ALPHA)));
						}
						if (style.get(StylingInstruction.SHAPE_REPLACEMENT) != null && shapes.containsKey(style.get(StylingInstruction.SHAPE_REPLACEMENT))) {
							rowPointShapeStyle.put(i, shapes.get(style.get(StylingInstruction.SHAPE_REPLACEMENT)));
						}
						if (style.get(StylingInstruction.LABEL_SIZE) != null && style.get(StylingInstruction.LABEL_SIZE).length() > 0) {
							rowFontSizeStyle.put(i, Double.parseDouble(style.get(StylingInstruction.LABEL_SIZE)));
						}
						if (style.get(StylingInstruction.LABEL_SIZE) != null && style.get(StylingInstruction.LABEL_SIZE).length() > 0) {
							rowFontColorStyle.put(i, rgbaStringToColor(style.get(StylingInstruction.LABEL_SIZE)));
						}
						if (style.get(StylingInstruction.LABEL_ALPHA) != null && style.get(StylingInstruction.LABEL_ALPHA).length() > 0) {
							rowFontAlphaStyle.put(i, Double.parseDouble(style.get(StylingInstruction.LABEL_ALPHA)));
						}
						if (style.get(StylingInstruction.LABEL_FONT_FAMILY) != null && style.get(StylingInstruction.LABEL_FONT_FAMILY).length() > 0) {
							rowFontFamilyStyle.put(i, style.get(StylingInstruction.LABEL_FONT_FAMILY));
						}
						if (style.get(StylingInstruction.LABEL_STYLE) != null && style.get(StylingInstruction.LABEL_STYLE).length() > 0) {
							rowFontStyleStyle.put(i, Integer.parseInt(style.get(StylingInstruction.LABEL_STYLE)));
						}
					}
				}
			}
		}

		ca.getChartCreator().updateChart(ca);
	}
	
	@Override
	public CA getResult() {
		return (CA) super.getResult();
	}

	public HashSet<Integer> getHiddenRowPointsIndexes() {
		return rowHiddenStyle;
	}
	
	public HashSet<Integer> getHiddenColPointsIndexes() {
		return colHiddenStyle;
	}
	
}
