package org.txm.ca.core.chartsengine.styling;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.ca.core.preferences.CAPreferences;
import org.txm.chartsengine.core.styling.Style;
import org.txm.chartsengine.core.styling.StylesRegistry;
import org.txm.chartsengine.core.styling.StylingRule;
import org.txm.chartsengine.core.styling.StylingSheet;
import org.txm.chartsengine.core.styling.base.StylerCatalog;
import org.txm.chartsengine.core.styling.catalogs.SelectionInstructionsCatalog;
import org.txm.chartsengine.core.styling.catalogs.StylingInstructionsCatalog;
import org.txm.chartsengine.core.styling.definitions.ParameterDefinition;
import org.txm.chartsengine.core.styling.definitions.SelectionInstructionDefinition;
import org.txm.chartsengine.core.styling.definitions.StylingInstructionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.TestFunctionDefinition;
import org.txm.chartsengine.core.styling.definitions.functions.TransformationFunctionDefinition;
import org.txm.chartsengine.core.styling.instructions.SelectionInstruction;
import org.txm.chartsengine.core.styling.instructions.StylingInstruction;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.TXMResult;
import org.txm.libs.office.WriteODS;
import org.txm.utils.FileUtils;
import org.txm.utils.TableReader;
import org.txm.utils.logger.Log;
import org.txm.utils.serializable.MinMax;
import org.txm.utils.serializable.ValueMinMax;

/**
 * Gives for the CA results : 1) the available styles and selections  2) the default style sheet 
 * @author mdecorde
 *
 */
public class CAStylerCatalog extends StylerCatalog<CA> implements Serializable {

	private static final long serialVersionUID = -4842345916965110905L;

	public static final String ID_RESULT_INFOS = "result-infos" //$NON-NLS-1$
							, ID_LABEL_REGEX = "label-regex" //$NON-NLS-1$
							, ID_RESULT_METADATA = "result-metadata"; //$NON-NLS-1$

	public static final String ID_OBJECT = "OBJECT" //$NON-NLS-1$
							, ID_LABEL = "Label" //$NON-NLS-1$
							, ID_QUALITY = "Q(x,y)" //$NON-NLS-1$
							, ID_METADATA = "METADATA" //$NON-NLS-1$
							, ID_COSX = "CosX" //$NON-NLS-1$
							, ID_COSY = "CosY" //$NON-NLS-1$
							, ID_CONTXY = "Cont(xy)" //$NON-NLS-1$
							, ID_CONTX = "Cont(x)" //$NON-NLS-1$
							, ID_CONTY = "Cont(y)" //$NON-NLS-1$
							, ID_MASS = "mass" //$NON-NLS-1$
							, ID_DIST = "dist"; //$NON-NLS-1$

	public static final String VALUE_ID_DIMENSION_ROWS_COLS = "rows&columns" //$NON-NLS-1$
							, VALUE_ID_DIMENSION_ROWS = "rows" //$NON-NLS-1$
							, VALUE_ID_DIMENSION_COLS = "columns"; //FIXME: SJ: trop lié à la CA //$NON-NLS-1$

	public static final String DATATEST = "datatest"; //$NON-NLS-1$

	SelectionInstructionsCatalog selectionInstructionsCatalog = new SelectionInstructionsCatalog();

	StylingInstructionsCatalog styleInstructionsCatalog = new StylingInstructionsCatalog();

	StylesRegistry styles;

//	private CA ca;

	private static LinkedHashSet<StylingSheet> sheets;
	
	protected static StylingSheet defaultStylingSheet;

	public class CADimensionParameter extends ParameterDefinition {

		private static final long serialVersionUID = 1875453792900243843L;

		public CADimensionParameter() {

			super(ID_OBJECT, CACoreMessages.object);
		}

		@Override
		public String getValue(TXMResult result, int serie, int item) {
			switch (serie) {
				case 0:
					return VALUE_ID_DIMENSION_ROWS;
				case 1:
					return VALUE_ID_DIMENSION_COLS;
				default:
					return VALUE_ID_DIMENSION_ROWS_COLS; // should not happen
			}
		}
	}

	/**
	 * a parameter returning the label value of a row or col
	 * @author mdecorde
	 *
	 */
	public class CALabelParameter extends ParameterDefinition {

		private static final long serialVersionUID = -3210747619753639806L;

		public CALabelParameter() {

			super(ID_LABEL, CACoreMessages.label);
		}

		@Override
		public String getValue(TXMResult result, int serie, int item) {

			try {
				CA ca = (CA)result;
				
				if (serie == 0) { // rows
					return ca.getRowNames()[item];
				}
				else { // cols
					return ca.getColNames()[item];
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				return CACoreMessages.bind(CACoreMessages.errorP0, e.getMessage());
			}
		}
	}

	/**
	 * A parameter returning the CA property value+min+max of a row or col
	 * 
	 * @author mdecorde
	 *
	 */
	public class CAPropertyParameter extends ParameterDefinition {

		private static final long serialVersionUID = -6667444225987092105L;

		public CAPropertyParameter(String id, String label) {
			super(id, label);
		}

		Double min = 0.0, max = Double.MAX_VALUE; // remove when caInfos.getMin(id) and caInfos.getMax(id) are implemented

		@Override
		public ValueMinMax getValue(TXMResult result, int row, int item) {

			try {
				CA ca = (CA)result;
				
				return new ValueMinMax((Double) ca.getData(row == 0, id, item), ca.getMinData(row == 0, id), ca.getMaxData(row == 0, id));
			}
			catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}

	public CAStylerCatalog() {
//		this.ca = ca;

		CADimensionParameter objectParameter = new CADimensionParameter();
		selectionInstructionsCatalog.getParameters().add(objectParameter);

		CALabelParameter labelParameter = new CALabelParameter();
		selectionInstructionsCatalog.getParameters().add(labelParameter);

		CAPropertyParameter qParameter = new CAPropertyParameter(ID_QUALITY, CACoreMessages.qXY);
		selectionInstructionsCatalog.getParameters().add(qParameter);
		CAPropertyParameter cosxParameter = new CAPropertyParameter(ID_COSX, CACoreMessages.cos2X);
		selectionInstructionsCatalog.getParameters().add(cosxParameter);
		CAPropertyParameter contxParameter = new CAPropertyParameter(ID_CONTX, CACoreMessages.contX);
		selectionInstructionsCatalog.getParameters().add(contxParameter);
		CAPropertyParameter cosyParameter = new CAPropertyParameter(ID_COSY, CACoreMessages.cos2Y);
		selectionInstructionsCatalog.getParameters().add(cosyParameter);
		CAPropertyParameter contyParameter = new CAPropertyParameter(ID_CONTY, CACoreMessages.contY);
		selectionInstructionsCatalog.getParameters().add(contyParameter);
		CAPropertyParameter massParameter = new CAPropertyParameter(ID_MASS, CACoreMessages.mass);
		selectionInstructionsCatalog.getParameters().add(massParameter);
		CAPropertyParameter distParameter = new CAPropertyParameter(ID_DIST, CACoreMessages.distance);
		selectionInstructionsCatalog.getParameters().add(distParameter);
		CAPropertyParameter metadataParameter = new CAPropertyParameter(ID_METADATA, CACoreMessages.anImportedMetadatum);
		selectionInstructionsCatalog.getParameters().add(metadataParameter);
		CAPropertyParameter contxyParameter = new CAPropertyParameter(ID_CONTXY, CACoreMessages.contXY);
		selectionInstructionsCatalog.getParameters().add(contxParameter);

		// Dimension
		SelectionInstructionDefinition objectInstruction = new SelectionInstructionDefinition(ID_OBJECT, CACoreMessages.object);
		selectionInstructionsCatalog.addInstruction(objectInstruction);
		selectionInstructionsCatalog.getFunctions().add(new TestFunctionDefinition(DATATEST, DATATEST) {

			private static final long serialVersionUID = 4836160799746401419L;

			@Override
			public Object apply(Object settings, Object arguments) {
				if (VALUE_ID_DIMENSION_ROWS_COLS.equals(settings)) return true; // always true
				return settings!= null && settings.equals(arguments);
			}
		});
		selectionInstructionsCatalog.associate(objectInstruction, selectionInstructionsCatalog.getFunction(DATATEST));
		selectionInstructionsCatalog.associate(objectInstruction, selectionInstructionsCatalog.getFunction(DATATEST), objectParameter);

		// label REGEX
		SelectionInstructionDefinition etiqueeSelection = new SelectionInstructionDefinition(ID_LABEL_REGEX, CACoreMessages.label);
		selectionInstructionsCatalog.addInstruction(etiqueeSelection);
		selectionInstructionsCatalog.associate(etiqueeSelection, selectionInstructionsCatalog.getFunction(SelectionInstructionsCatalog.REGEX), labelParameter);

		// Result property
		SelectionInstructionDefinition propSelection = new SelectionInstructionDefinition(ID_RESULT_INFOS, CACoreMessages.property);
		selectionInstructionsCatalog.addInstruction(propSelection); // valeur inutile ?

		for (String op : new String[] {SelectionInstructionsCatalog.SUP, SelectionInstructionsCatalog.INF, SelectionInstructionsCatalog.SUPEQUAL, SelectionInstructionsCatalog.INFEQUAL}) {
			selectionInstructionsCatalog.associate(propSelection, selectionInstructionsCatalog.getFunction(op));
			for (CAPropertyParameter p : new CAPropertyParameter[] {qParameter, massParameter, distParameter, cosxParameter, cosyParameter,contxParameter, contyParameter, contxyParameter}) {
				selectionInstructionsCatalog.associate(propSelection, selectionInstructionsCatalog.getFunction(op), p);
			}
		}

		// Result metadata
		SelectionInstructionDefinition metadataInstructions = new SelectionInstructionDefinition(ID_RESULT_METADATA, CACoreMessages.metadadum);
		selectionInstructionsCatalog.addInstruction(metadataInstructions); // valeur inutile ?
		selectionInstructionsCatalog.associate(metadataInstructions, selectionInstructionsCatalog.getFunction(SelectionInstructionsCatalog.EQUALS));

		styleInstructionsCatalog.getParameters().add(labelParameter);
		styleInstructionsCatalog.getParameters().add(qParameter);
		styleInstructionsCatalog.getParameters().add(cosxParameter);
		styleInstructionsCatalog.getParameters().add(contxParameter);
		styleInstructionsCatalog.getParameters().add(cosyParameter);
		styleInstructionsCatalog.getParameters().add(contyParameter);
		styleInstructionsCatalog.getParameters().add(contxyParameter);
		styleInstructionsCatalog.getParameters().add(massParameter);
		styleInstructionsCatalog.getParameters().add(distParameter);
		styleInstructionsCatalog.getParameters().add(metadataParameter);


		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) { // TODO: to discuss
			styleInstructionsCatalog.associate(StylingInstruction.ID_COLOR, StylingInstructionsCatalog.LINEAR_GRADIANT, ID_QUALITY);
			styleInstructionsCatalog.associate(StylingInstruction.ID_COLOR, StylingInstructionsCatalog.LINEAR_GRADIANT, ID_MASS);
			styleInstructionsCatalog.associate(StylingInstruction.ID_COLOR, StylingInstructionsCatalog.LINEAR_GRADIANT, ID_DIST);
			styleInstructionsCatalog.associate(StylingInstruction.ID_COLOR, StylingInstructionsCatalog.LINEAR_GRADIANT, ID_COSX);
			styleInstructionsCatalog.associate(StylingInstruction.ID_COLOR, StylingInstructionsCatalog.LINEAR_GRADIANT, ID_COSY);
			styleInstructionsCatalog.associate(StylingInstruction.ID_COLOR, StylingInstructionsCatalog.LINEAR_GRADIANT, ID_CONTX);
			styleInstructionsCatalog.associate(StylingInstruction.ID_COLOR, StylingInstructionsCatalog.LINEAR_GRADIANT, ID_CONTY);
		}
		styleInstructionsCatalog.associate(StylingInstruction.ID_TRANSPARENCY, StylingInstructionsCatalog.LINEAR, ID_QUALITY);
		styleInstructionsCatalog.associate(StylingInstruction.ID_TRANSPARENCY, StylingInstructionsCatalog.LINEAR, ID_MASS);
		styleInstructionsCatalog.associate(StylingInstruction.ID_TRANSPARENCY, StylingInstructionsCatalog.LINEAR, ID_DIST); //TODO INVERSE ?
		styleInstructionsCatalog.associate(StylingInstruction.ID_TRANSPARENCY, StylingInstructionsCatalog.LINEAR, ID_COSX);
		styleInstructionsCatalog.associate(StylingInstruction.ID_TRANSPARENCY, StylingInstructionsCatalog.LINEAR, ID_COSY);
		styleInstructionsCatalog.associate(StylingInstruction.ID_TRANSPARENCY, StylingInstructionsCatalog.LINEAR, ID_CONTX);
		styleInstructionsCatalog.associate(StylingInstruction.ID_TRANSPARENCY, StylingInstructionsCatalog.LINEAR, ID_CONTY);
		styleInstructionsCatalog.associate(StylingInstruction.ID_TRANSPARENCY, StylingInstructionsCatalog.LINEAR, ID_CONTXY);

		styleInstructionsCatalog.associate(StylingInstruction.ID_SIZE, StylingInstructionsCatalog.LINEAR, ID_QUALITY);
		styleInstructionsCatalog.associate(StylingInstruction.ID_SIZE, StylingInstructionsCatalog.LINEAR, ID_MASS);
		styleInstructionsCatalog.associate(StylingInstruction.ID_SIZE, StylingInstructionsCatalog.LINEAR, ID_DIST); //TODO INVERSE ?
		styleInstructionsCatalog.associate(StylingInstruction.ID_SIZE, StylingInstructionsCatalog.LINEAR, ID_COSX);
		styleInstructionsCatalog.associate(StylingInstruction.ID_SIZE, StylingInstructionsCatalog.LINEAR, ID_COSY);
		styleInstructionsCatalog.associate(StylingInstruction.ID_SIZE, StylingInstructionsCatalog.LINEAR, ID_CONTX);
		styleInstructionsCatalog.associate(StylingInstruction.ID_SIZE, StylingInstructionsCatalog.LINEAR, ID_CONTY);
		styleInstructionsCatalog.associate(StylingInstruction.ID_SIZE, StylingInstructionsCatalog.LINEAR, ID_CONTXY);
	}

	@Override
	public SelectionInstructionsCatalog getSelectionInstructionsCatalog() {

		return selectionInstructionsCatalog;
	}

	@Override
	public StylingInstructionsCatalog getStyleInstructionsCatalog() {

		return styleInstructionsCatalog;
	}

	@Override
	public StylesRegistry getStylesRegistry() {

		return styles;
	}


	@Override
	public StylingRule newDefaultRule() {

		StylingRule s = new StylingRule();

		SelectionInstruction objectSelection = new SelectionInstruction(this.selectionInstructionsCatalog, ID_OBJECT, DATATEST);
		objectSelection.setParameterDefinition(ID_OBJECT);
		objectSelection.setSettings(VALUE_ID_DIMENSION_ROWS);
		s.addSelector(objectSelection);

		StylingInstruction<?> stylingInstruction = new StylingInstruction<>(styleInstructionsCatalog, null);
		stylingInstruction.setMustAppliedToText(true);
		stylingInstruction.setMustAppledToShape(false);

		Style style = new Style();
		style.addInstruction(stylingInstruction);
		s.setStyle(style);

		return s;
	}

	@Override
	protected void populateDefaultStyleSheets() {

		StylingSheet nostyle = new StylingSheet(CACoreMessages.chevNoStyleChev); // move this to parent class
		nostyle.setEditable(false);
		sheets.add(nostyle);

		if (!CAPreferences.getInstance().getBoolean(CAPreferences.USER_INTERPRETATION_HELP)) {
			defaultStylingSheet = nostyle;
		}

		String[] conts = new String[] { ID_CONTXY, ID_CONTX, ID_CONTY, null };
		String[] qcos = new String[] { ID_QUALITY, ID_COSX, ID_COSY, ID_QUALITY };

		for (int i = 0 ; i < conts.length ; i++) {

			String IDCONT = conts[i];
			String IDQCOS = qcos[i];

			String name = ""; //$NON-NLS-1$
			if (IDCONT != null) {
				name += styleInstructionsCatalog.getParameters(IDCONT).getLabel()+"+"+styleInstructionsCatalog.getParameters(IDQCOS).getLabel(); //$NON-NLS-1$
			} else {
				name += styleInstructionsCatalog.getParameters(IDQCOS).getLabel();
			}
			StylingSheet defaultCA = new StylingSheet(name);

			// set the default styling sheet only if help interpretation is activated
			if (defaultStylingSheet == null && CAPreferences.getInstance().getBoolean(CAPreferences.USER_INTERPRETATION_HELP)) {
				defaultStylingSheet = defaultCA;
			}

			StylingRule s1 = new StylingRule();

			SelectionInstruction objectSelection = new SelectionInstruction(this.selectionInstructionsCatalog, ID_OBJECT, DATATEST);
			objectSelection.setSettings(VALUE_ID_DIMENSION_ROWS_COLS);
			objectSelection.setParameterDefinition(ID_OBJECT);
			s1.addSelector(objectSelection);

			StylingInstruction<?> transparenceInstruction = new StylingInstruction<>(styleInstructionsCatalog, StylingInstruction.ID_TRANSPARENCY);
			transparenceInstruction.setFunction(StylingInstructionsCatalog.LINEAR);
			transparenceInstruction.setSettings(new MinMax(50, 255));
			transparenceInstruction.setParameterDefinition(IDQCOS);
			Style style = new Style();
			style.addInstruction(transparenceInstruction);
			s1.setStyle(style);

			defaultCA.add(s1);

			StylingRule s2 = new StylingRule();

			SelectionInstruction objectSelection2 = new SelectionInstruction(this.selectionInstructionsCatalog, ID_OBJECT, DATATEST);
			objectSelection2.setSettings(VALUE_ID_DIMENSION_ROWS_COLS);
			objectSelection2.setParameterDefinition(ID_OBJECT);
			s2.addSelector(objectSelection2);

			StylingInstruction<?> sizeInstruction = new StylingInstruction<>(styleInstructionsCatalog, StylingInstruction.ID_SIZE);
			if (IDCONT != null) {
				sizeInstruction.setFunction(StylingInstructionsCatalog.LINEAR);
				sizeInstruction.setSettings(new MinMax(1,2));
				sizeInstruction.setParameterDefinition(IDCONT);
			} else {
				objectSelection2.setSettings(VALUE_ID_DIMENSION_COLS);
				sizeInstruction.setFunction("x1,40"); //$NON-NLS-1$
			}
			Style style2 = new Style();
			style2.addInstruction(sizeInstruction);
			s2.setStyle(style2);

			defaultCA.add(s2);


			StylingRule s3 = new StylingRule();

			SelectionInstruction objectSelection3 = new SelectionInstruction(this.selectionInstructionsCatalog, ID_OBJECT, DATATEST);
			objectSelection3.setSettings(VALUE_ID_DIMENSION_ROWS_COLS);
			objectSelection3.setParameterDefinition(ID_OBJECT);
			s3.addSelector(objectSelection3);

			StylingInstruction<?> shapeInstruction = new StylingInstruction<>(styleInstructionsCatalog, StylingInstruction.ID_SHAPE_REPLACEMENT);
			shapeInstruction.setFunction(StylingInstruction.VALUE_ID_SHAPE_DISK);
			Style style3 = new Style();
			style3.addInstruction(shapeInstruction);
			s3.setStyle(style3);

			defaultCA.add(s3);

			defaultCA.setEditable(false);
			sheets.add(defaultCA);
		}
	}

	@Override
	protected void populateUserStyleSheets() {
		Preferences userStylingSheetsNode = CAPreferences.getNode("/instance/"+CAPreferences.getInstance().getPreferencesNodeQualifier()+"/user_styling_sheets"); //$NON-NLS-1$ //$NON-NLS-2$
		try {
			for (String sheetname: userStylingSheetsNode.keys()) {
				StylingSheet sheet = JSONToStylingSheet(userStylingSheetsNode.get(sheetname, "{}")); //$NON-NLS-1$
				if (!sheets.contains(sheet) ) {
					sheets.add(sheet);
				}
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void persistUserStyleSheet(StylingSheet ss) {
		try {
			Preferences userStylingSheetsNode = CAPreferences.getNode("/instance/"+CAPreferences.getInstance().getPreferencesNodeQualifier()+"/user_styling_sheets"); //$NON-NLS-1$ //$NON-NLS-2$
			userStylingSheetsNode.put(ss.getName(), StylingSheetToJSON(ss));
			userStylingSheetsNode.flush();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void removePersistedUserStyleSheet(String name) {

		Preferences userStylingSheetsNode = CAPreferences.getNode("/instance/"+CAPreferences.getInstance().getPreferencesNodeQualifier()+"/user_styling_sheets"); //$NON-NLS-1$ //$NON-NLS-2$
		userStylingSheetsNode.remove(name);
		try {
			userStylingSheetsNode.flush();
		}
		catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static final String EMPTYJSON = "{}"; //$NON-NLS-1$
	public static final String NAME = "name"; //$NON-NLS-1$
	public static final String EDITABLE = "editable"; //$NON-NLS-1$
	public static final String ACTIVATED = "activated"; //$NON-NLS-1$
	public static final String LIST = "list"; //$NON-NLS-1$
	public static final String RULES = "rules"; //$NON-NLS-1$
	public static final String STYLES = "styles"; //$NON-NLS-1$
	public static final String SELECTORS = "selectors"; //$NON-NLS-1$
	public static final String INSTRUCTION = "instruction"; //$NON-NLS-1$
	public static final String FUNCTION = "function"; //$NON-NLS-1$
	public static final String PARAMETER = "parameter"; //$NON-NLS-1$
	public static final String SETTINGS = "settings"; //$NON-NLS-1$
	public static final String SHAPES = "shapes"; //$NON-NLS-1$
	public static final String LABELS = "labels"; //$NON-NLS-1$
	public static final String PREDEFINED = "predefined"; //$NON-NLS-1$
	
	public String StylingSheetToJSON(StylingSheet sheet) throws JSONException, IOException {

		JSONObject jsonSheet = new JSONObject();
		jsonSheet.put(NAME, sheet.getName()); //$NON-NLS-1$
		jsonSheet.put(EDITABLE, sheet.isEditable()); //$NON-NLS-1$

		JSONArray jsonRules = new JSONArray();

		for (StylingRule rule : sheet.getRules()) {

			JSONObject jsonRule = new JSONObject();

			JSONObject jsonStyles = new JSONObject();
			JSONArray jsonStylesArray = new JSONArray();
			//jsonStyles.put(PREDEFINED, rule.getStyle().isPredefined()); //$NON-NLS-1$
			jsonStyles.put(NAME, rule.getStyle().getName()); //$NON-NLS-1$

			for (StylingInstruction<?> style : rule.getStyle().getStylingInstructions()) {
				JSONObject jsonStyle = new JSONObject();
				if (style.getInstruction() != null) jsonStyle.put(INSTRUCTION, style.getInstruction().getID()); //$NON-NLS-1$
				if (style.getFunction() != null) jsonStyle.put(FUNCTION, style.getFunction().getID()); //$NON-NLS-1$
				if (style.getParameter() != null) jsonStyle.put(PARAMETER, style.getParameter().getID()); //$NON-NLS-1$
				if (style.getSettings() != null) jsonStyle.put(SETTINGS, TXMPreferences.toString(style.getSettings())); //$NON-NLS-1$
				jsonStyle.put(SHAPES, style.mustBeAppliedToShape()); //$NON-NLS-1$
				jsonStyle.put(LABELS, style.mustBeAppliedToText()); //$NON-NLS-1$
				jsonStylesArray.put(jsonStyle);
			}
			jsonStyles.putOpt(LIST, jsonStylesArray); //$NON-NLS-1$

			JSONArray jsonSelectors = new JSONArray();
			for (SelectionInstruction si : rule.getSelectors()) {
				JSONObject jsonSelector = new JSONObject();
				if (si.getInstruction() != null) jsonSelector.put(INSTRUCTION, si.getInstruction().getID()); //$NON-NLS-1$
				if (si.getFunction() != null) jsonSelector.put(FUNCTION, si.getFunction().getID()); //$NON-NLS-1$
				if (si.getParameter() != null) jsonSelector.put(PARAMETER, si.getParameter().getID()); //$NON-NLS-1$
				if (si.getSettings() != null) jsonSelector.put(SETTINGS, TXMPreferences.toString(si.getSettings())); //$NON-NLS-1$

				jsonSelectors.put(jsonSelector);
			}

			jsonRule.put(ACTIVATED, rule.isActivated()); //$NON-NLS-1$
			jsonRule.put(STYLES, jsonStyles); //$NON-NLS-1$
			jsonRule.put(SELECTORS, jsonSelectors); //$NON-NLS-1$
			jsonRules.put(jsonRule);
		}

		jsonSheet.put(RULES, jsonRules); //$NON-NLS-1$
		return jsonSheet.toString();
	}


	public StylingSheet JSONToStylingSheet(String json) throws JSONException {
		if (json == null) return null;
		if (json.length() == 0) return null;
		if (EMPTYJSON.equals(json)) return null; //$NON-NLS-1$

		JSONObject jsonSheet = new JSONObject(json);
		StylingSheet sheet = new StylingSheet(jsonSheet.getString(NAME)); //$NON-NLS-1$

		JSONArray jsonRules = jsonSheet.getJSONArray(RULES); //$NON-NLS-1$
		int size = jsonRules.length();
		for (int iRule = 0 ; iRule < size ; iRule++) {

			JSONObject jsonRule = jsonRules.getJSONObject(iRule);
			JSONObject jsonStyles = jsonRule.getJSONObject(STYLES); //$NON-NLS-1$
			JSONArray jsonSelectors = jsonRule.getJSONArray(SELECTORS); //$NON-NLS-1$

			StylingRule rule = new StylingRule();
			rule.setActivated(jsonRule.getBoolean(ACTIVATED)); //$NON-NLS-1$

			Style style = new Style();
			style.setName(jsonStyles.getString(NAME)); //$NON-NLS-1$
			//style.setPredefined("true".equals(jsonStyles.getString(PREDEFINED)));
			rule.setStyle(style);

			JSONArray jsonStylesArray = jsonStyles.getJSONArray(LIST); //$NON-NLS-1$
			int stylesSize = jsonStylesArray.length();
			for (int iStyle = 0 ; iStyle < stylesSize ; iStyle++) {
				JSONObject jsonSI = jsonStylesArray.getJSONObject(iStyle);
				String instruction = null;
				if (jsonSI.has(INSTRUCTION)) instruction = jsonSI.getString(INSTRUCTION); //$NON-NLS-1$
				String function = null;
				if (jsonSI.has(FUNCTION)) function = jsonSI.getString(FUNCTION); //$NON-NLS-1$
				String parameter = null;
				if (jsonSI.has(PARAMETER)) parameter = jsonSI.getString(PARAMETER); //$NON-NLS-1$
				Serializable settings = null;
				if (jsonSI.has(SETTINGS)) settings = TXMPreferences.toSerializable(jsonSI.getString(SETTINGS)); //$NON-NLS-1$
				boolean shape = true;
				if (jsonSI.has(SHAPES)) shape = jsonSI.getBoolean(SHAPES); //$NON-NLS-1$
				boolean labels = true;
				if (jsonSI.has(LABELS)) labels = jsonSI.getBoolean(LABELS); //$NON-NLS-1$
				boolean activated = true;
				if (jsonSI.has(ACTIVATED)) activated = jsonSI.getBoolean(ACTIVATED);; //$NON-NLS-1$
				
				StylingInstruction<?> si = new StylingInstruction<>(this.getStyleInstructionsCatalog(), instruction, function);
				si.setParameterDefinition(parameter);
				si.setSettings(settings);
				si.setMustAppledToShape(shape);
				si.setMustAppliedToText(labels);
				si.setActivated(activated);
				style.addInstruction(si);
			}

			int sselectorsSize = jsonSelectors.length();
			for (int iSelection = 0 ; iSelection < sselectorsSize ; iSelection++) {
				JSONObject jsonSI = jsonSelectors.getJSONObject(iSelection);
				String instruction = null;
				if (jsonSI.has(INSTRUCTION)) instruction = jsonSI.getString(INSTRUCTION); //$NON-NLS-1$
				String function = null;
				if (jsonSI.has(FUNCTION)) function = jsonSI.getString(FUNCTION); //$NON-NLS-1$
				String parameter = null;
				if (jsonSI.has(PARAMETER)) parameter = jsonSI.getString(PARAMETER); //$NON-NLS-1$
				Serializable settings = null;
				if (jsonSI.has(SETTINGS)) settings = TXMPreferences.toSerializable(jsonSI.getString(SETTINGS)); //$NON-NLS-1$
				boolean activated = true;
				if (jsonSI.has(ACTIVATED)) activated = jsonSI.getBoolean(ACTIVATED);; //$NON-NLS-1$
				
				SelectionInstruction si = new SelectionInstruction(this.getSelectionInstructionsCatalog(), instruction, function);
				si.setParameterDefinition(parameter);
				si.setSettings(settings);
				si.setActivated(activated);
				rule.getSelectors().add(si);
			}

			sheet.add(rule);
		}

		sheet.setEditable(jsonSheet.getBoolean(EDITABLE)); // do it at the end or else no rule will be added //$NON-NLS-1$
		return sheet;
	}

	@Override
	public LinkedHashSet<StylingSheet> getStyleSheets() {

		if (sheets != null) return sheets;

		sheets = new LinkedHashSet<>();

		populateDefaultStyleSheets();

		populateUserStyleSheets();

		return sheets;
	}


	/**
	 * @deprecated test not validated and not fully working
	 */
	@Deprecated
	@Override
	public StylingSheet importStylingSheet(File file) throws Exception {

		String[] cols = {ID_OBJECT, "style", ID_RESULT_INFOS, ID_LABEL_REGEX}; // the same order as in the UI //$NON-NLS-1$
		String[] colTypes = {"_type", "_function", "_parameter", "_settings", "_settings_human"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		
		StylingSheet sheet = new StylingSheet(FileUtils.stripExtension(file));

		TableReader reader = new TableReader(file);
		reader.readHeaders();
		String[] headers = reader.getHeaders();
		List<String> header = Arrays.asList(headers);
		ArrayList<String> missing = new ArrayList<>();
		for (String c : cols) {
			for (String t : colTypes) {
				if (!header.contains(c+t)) missing.add(c+t);
			}
		}
		if (missing.size() > 0) {
			Log.warning("Missing columns: "+missing); //$NON-NLS-1$
			return null;
		}


		while (reader.readRecord()) {
			LinkedHashMap<String, String> record = reader.getRecord();
			StylingRule rule = new StylingRule();

			for (String c : new String[] {ID_OBJECT, ID_RESULT_INFOS, ID_LABEL_REGEX}) {

				String type = record.get(c+"_type"); //$NON-NLS-1$
				if (type == null || type.length() == 0 || "null".equals(type)) { //$NON-NLS-1$
					continue;
				}

				SelectionInstruction objSelection = new SelectionInstruction(this.selectionInstructionsCatalog, record.get(c+"_type"), record.get(c+"_function")); //$NON-NLS-1$ //$NON-NLS-2$
				objSelection.setParameterDefinition(record.get(c+"_parameter")); //$NON-NLS-1$

				String settings = record.get(c+"_settings"); //$NON-NLS-1$
				if (settings == null || settings.length() == 0 || "null".equals(settings)) { //$NON-NLS-1$
					//continue;
				} else {
					ByteArrayInputStream bis = new ByteArrayInputStream(Base64.getDecoder().decode(settings.getBytes()));
					ObjectInputStream oInputStream = new ObjectInputStream(bis);
					Serializable restoredObject1 = (Serializable) oInputStream.readObject();
					objSelection.setSettings(restoredObject1);
				}

				rule.getSelectors().add(objSelection);
			}

			StylingInstruction<?> s1 = new StylingInstruction<>(this.styleInstructionsCatalog, record.get("style_type")); //$NON-NLS-1$
			s1.setFunction(record.get("style_function")); //$NON-NLS-1$
			s1.setParameterDefinition(record.get("style_parameter")); //$NON-NLS-1$

			String settings = record.get("style_settings"); //$NON-NLS-1$
			if (settings == null || settings.length() == 0 || "null".equals(settings)) { //$NON-NLS-1$
				//continue;
			} else {
				ByteArrayInputStream bis = new ByteArrayInputStream(Base64.getDecoder().decode(settings.getBytes()));
				ObjectInputStream oInputStream = new ObjectInputStream(bis);
				Serializable restoredObject1 = (Serializable) oInputStream.readObject();
				s1.setSettings(restoredObject1);
			}

			rule.setStyle(new Style());
			rule.getStyle().addInstruction(s1);

			sheet.add(rule);
		}

		Log.info("Imported: " + System.lineSeparator() + sheet.getDump()); //$NON-NLS-1$

		return sheet;
	}

	/**
	 * @deprecated test not validated and not fully working
	 */
	@Deprecated
	@Override
	public boolean exportStylingSheet(File file, StylingSheet sheet) throws Exception {
		
		String[] cols = {ID_OBJECT, "style", ID_RESULT_INFOS, ID_LABEL_REGEX}; // the same order as in the UI //$NON-NLS-1$
		String[] colTypes = {"_type", "_function", "_parameter", "_settings", "_settings_human"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		
		WriteODS writer = new WriteODS(file);

		ArrayList<String> header = new ArrayList<>();
		for (String c : cols) {
			for (String t : colTypes) {
				header.add(c+t);
			}
		}
		writer.writeLine(header);

		for (StylingRule rule : sheet.getRules()) {

			ArrayList<String> line = new ArrayList<>();

			for (String c : new String[] {ID_OBJECT}) {
				SelectionInstruction inst = rule.getSelectors().getInstructionForType(c);
				line.addAll(SelectionInstruction.toArrayListOfIDs(inst));
			}

			StylingInstruction<?> inst = rule.getStyle().getStylingInstructions().get(0);
			line.addAll(StylingInstruction.toArrayListOfIDs(inst));

			for (String c : new String[] {ID_RESULT_INFOS, ID_LABEL_REGEX}) {
				SelectionInstruction inst2 = rule.getSelectors().getInstructionForType(c);
				line.addAll(SelectionInstruction.toArrayListOfIDs(inst2));
			}

			writer.writeLine(line);
		}
		writer.save();

		Log.info("Exported: " + System.lineSeparator() + sheet.getDump()); //$NON-NLS-1$

		return true;
	}

	@Override
	public boolean contains(StylingSheet sheet) {
		
		if (sheet == null) return false;
		if (sheets == null) getStyleSheets(); // initialize
		return sheets.contains(sheet);
	}

	@Override
	public boolean add(StylingSheet sheet) {

		if (sheet == null) return false;
		if (sheets == null) getStyleSheets(); // initialize
		persistUserStyleSheet(sheet);
		return sheets.add(sheet);
	}

	@Override
	public boolean remove(StylingSheet sheet) {

		if (sheet == null) return false;
		if (sheets == null) return true; // nothing to remove
		
		removePersistedUserStyleSheet(sheet.getName());
		return sheets.remove(sheet);
	}

	@Override
	public StylingSheet get(String name) {

		if (name == null) return null;
		if (sheets == null) getStyleSheets(); // initialize
		for (StylingSheet sheet : sheets) {
			if (name.equals(sheet.getName())) {
				return sheet;
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		CAStylerCatalog ca = new CAStylerCatalog();

		SelectionInstructionsCatalog catalog = ca.getSelectionInstructionsCatalog();
		System.out.println(CACoreMessages.instructions); 
		for (SelectionInstructionDefinition instr : catalog.getInstructions()) {

			System.out.println("" + instr + CACoreMessages.canBeTestedWith); //$NON-NLS-1$

			for (TestFunctionDefinition f : catalog.getFunctionsFor(instr)) {

				System.out.println("	" + f + CACoreMessages.TabAndParameter); //$NON-NLS-1$

				for (ParameterDefinition p : catalog.getParametersFor(instr, f)) {

					System.out.println("		" + p); //$NON-NLS-1$
				}
			}
		}

		StylingInstructionsCatalog catalogStyles = ca.getStyleInstructionsCatalog();
		System.out.println("STYLES"); //$NON-NLS-1$
		for (StylingInstructionDefinition<?> instr : catalogStyles.getInstructions()) {

			System.out.println("" + instr + CACoreMessages.canChangedWith); //$NON-NLS-1$
			for (TransformationFunctionDefinition f : catalogStyles.getFunctionsFor(instr)) {

				System.out.println("	" + f + CACoreMessages.TabAndParameter); //$NON-NLS-1$
				for (ParameterDefinition p : catalogStyles.getParametersFor(instr, f)) {

					System.out.println("		" + p); //$NON-NLS-1$
				}
			}
		}
	}

	@Override
	public StylingSheet getDefaultStyleSheet() {
		
		return defaultStylingSheet;
	}

	public StylingSheet getStylingSheetByName(String name) {
		
		if (name == null) return null;
		
		for (StylingSheet s: getStyleSheets()) {
			if (name.equals(s.getName())) {
				return s;
			}
		}
		return null;
	}
}
