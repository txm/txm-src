package org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.chartcreators;

import org.eclipse.osgi.util.NLS;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DatasetUtils;
import org.rosuda.REngine.REXPMismatchException;
import org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.renderers.CAEigenvaluesItemSelectionRenderer;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.functions.Eigenvalues;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.chartsengine.jfreechart.core.JFCChartCreator;
import org.txm.chartsengine.jfreechart.core.themes.base.ExtendedNumberAxis;
import org.txm.statsengine.core.StatException;
import org.txm.utils.logger.Log;

/**
 * JFC Eigenvalues chart creator.
 * 
 * @author sjacquot
 *
 */
public class JFCEigenvaluesChartCreator extends JFCChartCreator<Eigenvalues> {


	@Override
	public JFreeChart createChart(Eigenvalues result) {

		CA ca = result.getCA();
		JFreeChart chart = null;

		// Creates the empty dataset
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		// Create the chart
		chart = this.getChartsEngine().createCategoryBarChart(dataset, CACoreMessages.eigenvalues, CACoreMessages.axis, CACoreMessages.eigenvalue, false, false, false);

		// Custom renderer
		chart.getCategoryPlot().setRenderer(new CAEigenvaluesItemSelectionRenderer(ca));

		return chart;
	}


	@Override
	public void updateChart(Eigenvalues result) {

		CA ca = result.getCA();
		JFreeChart chart = (JFreeChart) result.getChart();

		// freeze rendering while computing
		chart.setNotify(false);

		// update the dataset
		try {

			DefaultCategoryDataset dataset = (DefaultCategoryDataset) chart.getCategoryPlot().getDataset();
			dataset.clear();


			double[] singularValues = ca.getValeursPropres();

			for (int i = 0; i < singularValues.length; i++) {
				dataset.setValue(singularValues[i], CACoreMessages.eigenvalue, String.valueOf(i + 1));
			}

		}
		catch (StatException e) {
			Log.severe(NLS.bind(CACoreMessages.cantCreateBarChartOfTheCASingularValuesP0, e));
			Log.printStackTrace(e);
		}
		catch (REXPMismatchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Custom range axis for ticks drawing options
		chart.getCategoryPlot().setRangeAxis(new ExtendedNumberAxis((NumberAxis) chart.getCategoryPlot().getRangeAxis(), false, true, 0, DatasetUtils.findMaximumRangeValue(chart.getCategoryPlot()
				.getDataset()).doubleValue()));

		super.updateChart(result);

		// force the hide of the domain axis lines
		chart.getCategoryPlot().setDomainGridlinesVisible(false);

	}


	@Override
	public Class<Eigenvalues> getResultDataClass() {
		return Eigenvalues.class;
	}

}
