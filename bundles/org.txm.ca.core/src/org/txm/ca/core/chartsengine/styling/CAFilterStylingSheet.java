package org.txm.ca.core.chartsengine.styling;

import java.util.Arrays;

import org.rosuda.REngine.REXPMismatchException;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.preferences.CAPreferences;
import org.txm.chartsengine.core.ColorMessages;
import org.txm.chartsengine.core.styling.Style;
import org.txm.chartsengine.core.styling.StylingRule;
import org.txm.chartsengine.core.styling.StylingSheet;
import org.txm.chartsengine.core.styling.catalogs.SelectionInstructionsCatalog;
import org.txm.chartsengine.core.styling.catalogs.StylingInstructionsCatalog;
import org.txm.chartsengine.core.styling.instructions.SelectionInstruction;
import org.txm.chartsengine.core.styling.instructions.StylingInstruction;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

public class CAFilterStylingSheet extends StylingSheet {

	private SelectionInstruction rowsSelectionInstruction;
	private SelectionInstruction colsSelectionInstruction;
	public StylingRule rowHideRules;
	public StylingRule colHideRules;
	private SelectionInstruction rowQualityInstruction;
	private SelectionInstruction rowContXInstruction;
	private SelectionInstruction rowContYInstruction;
	private SelectionInstruction colContYInstruction;
	private SelectionInstruction colContXInstruction;
	private SelectionInstruction colQualityInstruction;

	private SelectionInstruction rowQualityInstructionSup;
	private SelectionInstruction rowContXInstructionSup;
	private SelectionInstruction rowContYInstructionSup;
	private SelectionInstruction colContYInstructionSup;
	private SelectionInstruction colContXInstructionSup;
	private SelectionInstruction colQualityInstructionSup;

	private CA ca;
	public StylingRule colsColorContYQualiRules;
	public StylingRule colsColorContXYQualiRules;
	public StylingRule colsColorContXQualiRules;
	public StylingRule colsColorQualiRules;
	public StylingRule colsColorContYRules;
	public StylingRule colsColorContXRules;
	public StylingRule rowsColorContXYQualiRules;
	public StylingRule rowsColorContYQualiRules;
	public StylingRule rowsColorContXQualiRules;
	public StylingRule rowsColorQualiRules;
	public StylingRule rowsColorContYRules;
	public StylingRule rowsColorContXRules;
	public StylingRule colsColorContXYRules;
	public StylingRule rowsColorContXYRules;

	public CAFilterStylingSheet(CA ca, String name) {

		super(name);
		this.ca = ca;
		CAStylerCatalog caCatalog = ca.getCatalogs();

		boolean defaultAssist = CAPreferences.getInstance().getBoolean(CAPreferences.USER_INTERPRETATION_HELP);

		Style hideStyle = new Style();
		hideStyle.addInstruction(new StylingInstruction<Boolean>(caCatalog.getStyleInstructionsCatalog(), StylingInstruction.ID_HIDDEN, StylingInstruction.ALWAYS_TRUE));

		// ROWS
		rowsSelectionInstruction = new SelectionInstruction(caCatalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_OBJECT, CAStylerCatalog.DATATEST);
		rowsSelectionInstruction.setParameterDefinition(CAStylerCatalog.ID_OBJECT);
		rowsSelectionInstruction.setSettings(CAStylerCatalog.VALUE_ID_DIMENSION_ROWS);
		rowHideRules = this.add(new StylingRule("Rows filter", hideStyle)); //$NON-NLS-1$
		rowHideRules.setActivated(defaultAssist);
		rowHideRules.getSelectors().add(rowsSelectionInstruction);
		// 	QUALITY
		rowQualityInstruction = new SelectionInstruction(caCatalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_RESULT_INFOS, SelectionInstructionsCatalog.INFEQUAL, CAStylerCatalog.ID_QUALITY); 
		rowHideRules.getSelectors().add(rowQualityInstruction);
		//CONTX
		rowContXInstruction = new SelectionInstruction(caCatalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_RESULT_INFOS, SelectionInstructionsCatalog.INFEQUAL, CAStylerCatalog.ID_CONTX);
		rowHideRules.getSelectors().add(rowContXInstruction);
		//  CONTY
		rowContYInstruction = new SelectionInstruction(caCatalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_RESULT_INFOS, SelectionInstructionsCatalog.INFEQUAL, CAStylerCatalog.ID_CONTY);
		rowHideRules.getSelectors().add(rowContYInstruction);

		// COLS
		colsSelectionInstruction = new SelectionInstruction(caCatalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_OBJECT, CAStylerCatalog.DATATEST);
		colsSelectionInstruction.setParameterDefinition(CAStylerCatalog.ID_OBJECT);
		colsSelectionInstruction.setSettings(CAStylerCatalog.VALUE_ID_DIMENSION_COLS);

		colHideRules = this.add(new StylingRule("Cols filter", hideStyle)); //$NON-NLS-1$
		colHideRules.setActivated(false);
		colHideRules.getSelectors().add(colsSelectionInstruction);
		// 	QUALITY
		colQualityInstruction = new SelectionInstruction(caCatalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_RESULT_INFOS, SelectionInstructionsCatalog.INFEQUAL, CAStylerCatalog.ID_QUALITY);
		colHideRules.getSelectors().add(colQualityInstruction);
		//  CONTX
		colContXInstruction = new SelectionInstruction(caCatalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_RESULT_INFOS, SelectionInstructionsCatalog.INFEQUAL, CAStylerCatalog.ID_CONTX);
		colHideRules.getSelectors().add(colContXInstruction);

		//  CONTY
		colContYInstruction = new SelectionInstruction(caCatalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_RESULT_INFOS, SelectionInstructionsCatalog.INFEQUAL, CAStylerCatalog.ID_CONTY);
		colHideRules.getSelectors().add(colContYInstruction);

		// COLOR SELECTIONS
		Style redContXStyle = new Style();
		redContXStyle.addInstruction(new StylingInstruction<Boolean>(caCatalog.getStyleInstructionsCatalog(), StylingInstruction.ID_COLOR, ColorMessages.red));
		Style yellowQualiStyle = new Style();
		yellowQualiStyle.addInstruction(new StylingInstruction<Boolean>(caCatalog.getStyleInstructionsCatalog(), StylingInstruction.ID_COLOR, ColorMessages.yellow));
		Style blueContYStyle = new Style();
		blueContYStyle.addInstruction(new StylingInstruction<Boolean>(caCatalog.getStyleInstructionsCatalog(), StylingInstruction.ID_COLOR, ColorMessages.blue));
		Style orangeContXYStyle = new Style();
		orangeContXYStyle.addInstruction(new StylingInstruction<Boolean>(caCatalog.getStyleInstructionsCatalog(), StylingInstruction.ID_COLOR, ColorMessages.yellow_ocher));
		Style violetContXQualiStyle = new Style();
		violetContXQualiStyle.addInstruction(new StylingInstruction<Boolean>(caCatalog.getStyleInstructionsCatalog(), StylingInstruction.ID_COLOR, ColorMessages.violet));
		Style greenContYQualiStyle = new Style();
		greenContYQualiStyle.addInstruction(new StylingInstruction<Boolean>(caCatalog.getStyleInstructionsCatalog(), StylingInstruction.ID_COLOR, ColorMessages.green));
		Style blackContXYQualiStyle = new Style();
		blackContXYQualiStyle.addInstruction(new StylingInstruction<Boolean>(caCatalog.getStyleInstructionsCatalog(), StylingInstruction.ID_COLOR, "BLACK"));

		//	 	QUALITY
		rowQualityInstructionSup = new SelectionInstruction(caCatalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_RESULT_INFOS, SelectionInstructionsCatalog.SUP, CAStylerCatalog.ID_QUALITY); 
		rowContXInstructionSup = new SelectionInstruction(caCatalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_RESULT_INFOS, SelectionInstructionsCatalog.SUP, CAStylerCatalog.ID_CONTX);
		rowContYInstructionSup = new SelectionInstruction(caCatalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_RESULT_INFOS, SelectionInstructionsCatalog.SUP, CAStylerCatalog.ID_CONTY);

		colQualityInstructionSup = new SelectionInstruction(caCatalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_RESULT_INFOS, SelectionInstructionsCatalog.SUP, CAStylerCatalog.ID_QUALITY);
		colContXInstructionSup = new SelectionInstruction(caCatalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_RESULT_INFOS, SelectionInstructionsCatalog.SUP, CAStylerCatalog.ID_CONTX);
		colContYInstructionSup = new SelectionInstruction(caCatalog.getSelectionInstructionsCatalog(), CAStylerCatalog.ID_RESULT_INFOS, SelectionInstructionsCatalog.SUP, CAStylerCatalog.ID_CONTY);


		rowsColorContXRules = this.add(new StylingRule("rows contX color", redContXStyle)); //$NON-NLS-1$
		rowsColorContXRules.setActivated(false);
		rowsColorContXRules.getSelectors().add(rowsSelectionInstruction);
		rowsColorContXRules.getSelectors().add(rowContXInstructionSup);

		rowsColorContYRules = this.add(new StylingRule("rows contY color", blueContYStyle)); //$NON-NLS-1$
		rowsColorContYRules.setActivated(false);
		rowsColorContYRules.getSelectors().add(rowsSelectionInstruction);
		rowsColorContYRules.getSelectors().add(rowContYInstructionSup);

		rowsColorQualiRules = this.add(new StylingRule("rows quali color", yellowQualiStyle)); //$NON-NLS-1$
		rowsColorQualiRules.setActivated(false);
		rowsColorQualiRules.getSelectors().add(rowsSelectionInstruction);
		rowsColorQualiRules.getSelectors().add(rowQualityInstructionSup);

		rowsColorContXYRules = this.add(new StylingRule("rows contx conty color", orangeContXYStyle)); //$NON-NLS-1$
		rowsColorContXYRules.setActivated(false);
		rowsColorContXYRules.getSelectors().add(rowsSelectionInstruction);
		rowsColorContXYRules.getSelectors().add(rowContYInstructionSup);
		rowsColorContXYRules.getSelectors().add(rowContXInstructionSup);

		rowsColorContXQualiRules = this.add(new StylingRule("rows contx quali color", violetContXQualiStyle)); //$NON-NLS-1$
		rowsColorContXQualiRules.setActivated(false);
		rowsColorContXQualiRules.getSelectors().add(rowsSelectionInstruction);
		rowsColorContXQualiRules.getSelectors().add(rowQualityInstructionSup);
		rowsColorContXQualiRules.getSelectors().add(rowContXInstructionSup);

		rowsColorContYQualiRules = this.add(new StylingRule("rows conty quali color", greenContYQualiStyle)); //$NON-NLS-1$
		rowsColorContYQualiRules.setActivated(false);
		rowsColorContYQualiRules.getSelectors().add(rowsSelectionInstruction);
		rowsColorContYQualiRules.getSelectors().add(rowQualityInstructionSup);
		rowsColorContYQualiRules.getSelectors().add(rowContYInstructionSup);

		rowsColorContXYQualiRules = this.add(new StylingRule("rows contxy quali color", blackContXYQualiStyle)); //$NON-NLS-1$
		rowsColorContXYQualiRules.setActivated(false);
		rowsColorContXYQualiRules.getSelectors().add(rowsSelectionInstruction);
		rowsColorContXYQualiRules.getSelectors().add(rowQualityInstructionSup);
		rowsColorContXYQualiRules.getSelectors().add(rowContXInstructionSup);
		rowsColorContXYQualiRules.getSelectors().add(rowContYInstructionSup);

		colsColorContXRules = this.add(new StylingRule("cols contX color", redContXStyle)); //$NON-NLS-1$
		colsColorContXRules.setActivated(false);
		colsColorContXRules.getSelectors().add(colsSelectionInstruction);
		colsColorContXRules.getSelectors().add(colContXInstructionSup);

		colsColorContYRules = this.add(new StylingRule("cols contY color", blueContYStyle)); //$NON-NLS-1$
		colsColorContYRules.setActivated(false);
		colsColorContYRules.getSelectors().add(colsSelectionInstruction);
		colsColorContYRules.getSelectors().add(colContYInstructionSup);

		colsColorQualiRules = this.add(new StylingRule("cols quali color", yellowQualiStyle)); //$NON-NLS-1$
		colsColorQualiRules.setActivated(false);
		colsColorQualiRules.getSelectors().add(colsSelectionInstruction);
		colsColorQualiRules.getSelectors().add(colQualityInstructionSup);

		colsColorContXYRules = this.add(new StylingRule("cols contx conty color", orangeContXYStyle)); //$NON-NLS-1$
		colsColorContXYRules.setActivated(false);
		colsColorContXYRules.getSelectors().add(colsSelectionInstruction);
		colsColorContXYRules.getSelectors().add(colContXInstructionSup);
		colsColorContXYRules.getSelectors().add(colContYInstructionSup);

		colsColorContXQualiRules = this.add(new StylingRule("cols contx quali color", violetContXQualiStyle)); //$NON-NLS-1$
		colsColorContXQualiRules.setActivated(false);
		colsColorContXQualiRules.getSelectors().add(colsSelectionInstruction);
		colsColorContXQualiRules.getSelectors().add(colQualityInstructionSup);
		colsColorContXQualiRules.getSelectors().add(colContXInstructionSup);

		colsColorContYQualiRules = this.add(new StylingRule("cols conty quali color", greenContYQualiStyle)); //$NON-NLS-1$
		colsColorContYQualiRules.setActivated(false);
		colsColorContYQualiRules.getSelectors().add(colsSelectionInstruction);
		colsColorContYQualiRules.getSelectors().add(colQualityInstructionSup);
		colsColorContYQualiRules.getSelectors().add(colContYInstructionSup);

		colsColorContXYQualiRules = this.add(new StylingRule("cols contxy quali color", blackContXYQualiStyle)); //$NON-NLS-1$
		colsColorContXYQualiRules.setActivated(false);
		colsColorContXYQualiRules.getSelectors().add(colsSelectionInstruction);
		colsColorContXYQualiRules.getSelectors().add(colQualityInstructionSup);
		colsColorContXYQualiRules.getSelectors().add(colContXInstructionSup);
		colsColorContXYQualiRules.getSelectors().add(colContYInstructionSup);

		try {
			sumContributions80(true, true);

			if (ca.getSingularValues().length == 2) { // when there is 2 singular values Q is always 1
				setRowsQualitySettings(1d);
				setColsQualitySettings(1d);
			} else  if (CAPreferences.getInstance().getString(CAPreferences.FILTER_DEFAULT_QUALITY_MODE).equals("minOfCont")) {
				minQualityOfContributions(true, true);
			} else if (CAPreferences.getInstance().getString(CAPreferences.FILTER_DEFAULT_QUALITY_MODE).equals("80%")) {
				percent80Quality(true, true);
			} else if (CAPreferences.getInstance().getString(CAPreferences.FILTER_DEFAULT_QUALITY_MODE).equals("min")) {
				minQuality(true, true);
			} else if (CAPreferences.getInstance().getString(CAPreferences.FILTER_DEFAULT_QUALITY_MODE).equals("max")) {
				maxQuality(true, true);
			} else if (CAPreferences.getInstance().getString(CAPreferences.FILTER_DEFAULT_QUALITY_MODE).equals("0.5")) {
				meanQuality(true, true);
			} else if (CAPreferences.getInstance().getString(CAPreferences.FILTER_DEFAULT_QUALITY_MODE).equals("mean")) {
				mean2Quality(true, true);
			} else { // 1
				setRowsQualitySettings(1d);
				setColsQualitySettings(1d);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void meanQuality(boolean rows, boolean columns) {

		if (rows) {
			setRowsQualitySettings(0.5d);
		}
		if (columns) {
			setColsQualitySettings(0.5d);
		}
	}

	public void mean2Quality(boolean rows, boolean columns) {
		try {
			double[][] cos2 = null;
			double d = 0;
			if (rows) {
				cos2 = ca.getRowCos2();

				double total = 0;
				for (int i = 0 ; i < cos2.length ; i++) {
					total += cos2[i][ca.getFirstDimension()] + cos2[i][ca.getSecondDimension()];
				}

				setRowsQualitySettings(total / cos2.length);
			}

			if (columns) {
				cos2 = ca.getColCos2();
				double total = 0;
				for (int i = 0 ; i < cos2.length ; i++) {
					total += cos2[i][ca.getFirstDimension()] + cos2[i][ca.getSecondDimension()];
				}

				setColsQualitySettings(total / cos2.length);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void percent80Quality(boolean rows, boolean columns) {
		try {
			double[][] cos2 = null;
			if (rows) {
				cos2 = ca.getRowCos2();
				setRowsQualitySettings(get80PercentOfQuality(cos2, ca.getFirstDimension(), ca.getSecondDimension()));
			}

			if (columns) {
				cos2 = ca.getColCos2();
				setColsQualitySettings(get80PercentOfQuality(cos2, ca.getFirstDimension(), ca.getSecondDimension()));
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void minQualityOfContributions(boolean rows, boolean columns) {
		try {
			double[][] cos2 = null;
			double[][] allcontribs = null;
			double contx, conty;
			double d = 0;
			if (rows) {
				allcontribs = ca.getRowContrib();
				cos2 = ca.getRowCos2();

				contx = (double) rowContXInstruction.getSettings();
				conty = (double) rowContXInstruction.getSettings();

				setColsContXSettings(0d);
				setColsContYSettings(0d);

				double min = 999999;
				for (int i = 0 ; i < cos2.length ; i++) {
					d = cos2[i][ca.getFirstDimension()] + cos2[i][ca.getSecondDimension()];
					if (min > d && allcontribs[i][ca.getFirstDimension()] >= contx && allcontribs[i][ca.getSecondDimension()] >= conty) {
						min = d;
					}
				}

				setRowsQualitySettings(min);
			}

			if (columns) {
				cos2 = ca.getColCos2();
				double min = 999999;
				contx = (double) colContXInstruction.getSettings();
				conty = (double) colContYInstruction.getSettings();
				for (int i = 0 ; i < cos2.length ; i++) {
					d = cos2[i][ca.getFirstDimension()] + cos2[i][ca.getSecondDimension()];
					if (min > d && allcontribs[i][ca.getFirstDimension()] >= contx && allcontribs[i][ca.getSecondDimension()] >= conty) {
						min = d;
					}
				}

				setColsQualitySettings(min);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void minQuality(boolean rows, boolean columns) {
		try {
			double[][] cos2 = null;
			double d = 0;
			if (rows) {
				cos2 = ca.getRowCos2();
				double min = 999999;
				for (int i = 0 ; i < cos2.length ; i++) {
					d = cos2[i][ca.getFirstDimension()] + cos2[i][ca.getSecondDimension()];
					if (min > d) {
						min = d;
					}
				}

				setRowsQualitySettings(min);
			}

			if (columns) {
				cos2 = ca.getColCos2();
				double min = 999999;
				for (int i = 0 ; i < cos2.length ; i++) {
					d = cos2[i][ca.getFirstDimension()] + cos2[i][ca.getSecondDimension()];
					if (min > d) {
						min = d;
					}
				}

				setColsQualitySettings(min);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void maxQuality(boolean rows, boolean columns) {
		try {
			double[][] cos2 = null;
			double d = 0;
			if (rows) {
				cos2 = ca.getRowCos2();
				double max = 0;
				for (int i = 0 ; i < cos2.length ; i++) {
					d = cos2[i][ca.getFirstDimension()] + cos2[i][ca.getSecondDimension()];
					if (max < d) {
						max = d;
					}
				}

				setRowsQualitySettings(max);
			}

			if (columns) {
				cos2 = ca.getColCos2();
				double max = 0;
				for (int i = 0 ; i < cos2.length ; i++) {
					d = cos2[i][ca.getFirstDimension()] + cos2[i][ca.getSecondDimension()];
					if (max < d) {
						max = d;
					}
				}

				setColsQualitySettings(max);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void setDoRows(boolean dorows) {

		rowHideRules.setActivated(dorows);
	}

	public void setDoColumns(boolean docolumns) {

		colHideRules.setActivated(docolumns);
	}

	/**
	 * Sets contributions and quality to minimal values to start filtering using contribution
	 */
	public void RAZ() {

		setRowsContXSettings(0d);
		setRowsContYSettings(0d);
		setRowsQualitySettings(1d);

		setColsContXSettings(0d);
		setColsContYSettings(0d);
		setColsQualitySettings(1d);
	}


	/**
	 * Sets contributions to the mean values
	 * 
	 * @param rows
	 * @param cols
	 * @throws RWorkspaceException
	 * @throws REXPMismatchException
	 */
	public void means(boolean rows, boolean cols) throws RWorkspaceException, REXPMismatchException {

		if (rows) {
			double m = 100d / ca.getRowsCount();
			setRowsContXSettings(m);
			setRowsContYSettings(m);
		}
		if (cols) {
			double m = 100d / ca.getColumnsCount();
			setColsContXSettings(m);
			setColsContYSettings(m);
		}
	}

	/**
	 * Sets contribution and quality to the minimal values to start filtering
	 * 
	 * @param rows
	 * @param cols
	 * @throws RWorkspaceException
	 * @throws REXPMismatchException
	 */
	public void mins(boolean rows, boolean cols) throws RWorkspaceException, REXPMismatchException {

		if (rows) {
			double m = 100d / ca.getRowsCount();
			setRowsContXSettings(m);
			setRowsContYSettings(m);
		}
		if (cols) {
			double m = 100d / ca.getColumnsCount();
			setColsContXSettings(m);
			setColsContYSettings(m);
		}
	}

	public void median(boolean rows, boolean cols, int dim) throws REXPMismatchException, StatException {

		if (rows) {
			double[][] allcontribs = ca.getRowContrib();
			double[] contribs = new double[allcontribs.length];
			for (int i = 0 ; i < allcontribs.length ; i++) {
				contribs[i] = allcontribs[i][dim];
			}

			Arrays.sort(contribs);

			setRowsContXSettings(contribs[contribs.length / 2]);
			setRowsContYSettings(contribs[contribs.length / 2]);
		}
		if (cols) {
			double[][] allcontribs = ca.getColContrib();
			double[] contribs = new double[allcontribs.length];
			for (int i = 0 ; i < allcontribs.length ; i++) {
				contribs[i] = allcontribs[i][dim];
			}

			Arrays.sort(contribs);

			setColsContXSettings(contribs[contribs.length / 2]);
			setColsContYSettings(contribs[contribs.length / 2]);
		}
	}

	public void sumContributions80(boolean rows, boolean cols) throws REXPMismatchException, StatException {

		if (rows) {
			double[][] allcontribs = ca.getRowContrib();
			setRowsContXSettings(get80PercentOfSumContribs(allcontribs, ca.getFirstDimension() - 1));
			setRowsContYSettings(get80PercentOfSumContribs(allcontribs, ca.getSecondDimension() - 1));
		}
		if (cols) {
			double[][] allcontribs = ca.getColContrib();
			setColsContXSettings(get80PercentOfSumContribs(allcontribs, ca.getFirstDimension() - 1));
			setColsContYSettings(get80PercentOfSumContribs(allcontribs, ca.getSecondDimension() - 1));
		}
	}

	private void setRowsContXSettings(double v) {

		rowContXInstruction.setSettings(v);
		rowContXInstructionSup.setSettings(v);
	}

	private void setRowsContYSettings(double v) {

		rowContYInstruction.setSettings(v);
		rowContYInstructionSup.setSettings(v);
	}

	private void setColsContXSettings(double v) {

		colContXInstruction.setSettings(v);
		colContXInstructionSup.setSettings(v);
	}

	private void setColsContYSettings(double v) {

		colContYInstruction.setSettings(v);
		colContYInstructionSup.setSettings(v);
	}

	private void setRowsQualitySettings(double d) {
		rowQualityInstruction.setSettings(d);
		rowQualityInstructionSup.setSettings(d);
	}

	private void setColsQualitySettings(double d) {
		colQualityInstruction.setSettings(d);
		colQualityInstructionSup.setSettings(d);
	}

	/**
	 * 
	 * @param contribs
	 * @param dim the dimension to test in the contribs table
	 * @return the sum of 80% of the values of the dim th dimension
	 */
	public static double get80PercentOfSumContribs(double[][] contribs, int dim) {

		double[] dimcontribs = new double[contribs.length];

		// CONTX OF ROWS
		for (int i = 0 ; i < contribs.length ; i++) {
			dimcontribs[i] = contribs[i][dim];
		}

		Arrays.sort(dimcontribs);

		double sumConts = 0.0d;
		double contMin = 0.0d;
		int i = dimcontribs.length - 1;
		for ( ; i >= 0; i--) {
			if (sumConts >= 80) {
				break;
			}
			contMin = dimcontribs[i];
			sumConts += contMin;
		}

		if (i == 0) {
			contMin = 0;
		}

		return contMin;
	}

	/**
	 * 
	 * @param contribs
	 * @param dim the dimension to test in the contribs table
	 * @return the sum of 80% of the values of the dim th dimension
	 */
	public static double get80PercentOfQuality(double[][] cos2, int dim1, int dim2) {

		double[] qualities = new double[cos2.length];

		// CONTX OF ROWS
		for (int i = 0 ; i < cos2.length ; i++) {
			qualities[i] = cos2[i][dim1] + cos2[i][dim2];
		}

		Arrays.sort(qualities);

		return qualities[(int) (qualities.length*0.2)];
	}

	/**
	 * 
	 * @param contribs
	 * @param dim the dimension to test in the contribs table
	 * @return the median value of the dim th dimension
	 */
	public static double getMedContribs(double[][] contribs, int dim) {

		double[] dimcontribs = new double[contribs.length];

		// CONTX OF ROWS
		for (int i = 0 ; i < contribs.length ; i++) {
			dimcontribs[i] = contribs[i][dim];
		}

		Arrays.sort(dimcontribs);

		return dimcontribs[dimcontribs.length /2];
	}

	/**
	 * 
	 * @param contribs
	 * @param dim the dimension to test in the contribs table
	 * @return the min value of the dim th dimension
	 */
	public static double getMin2(double[][] contribs, int dim) {

		double min = Double.MAX_VALUE;
		// CONTX OF ROWS
		for (int i = 0 ; i < contribs.length ; i++) {
			if (min > contribs[i][dim]) {
				min = contribs[i][dim];
			}
		}

		if (min < 0.001) {
			min = 0;
		}

		return min;
	}

	public static double getMin(double[] contribs) {

		double min = Double.MAX_VALUE;
		// CONTX OF ROWS
		for (int i = 0 ; i < contribs.length ; i++) {
			if (min > contribs[i]) {
				min = contribs[i];
			}
		}

		if (min < 0.001) {
			min = 0;
		}

		return min;
	}
}
