package org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.renderers;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.xy.XYDataset;
import org.txm.ca.core.chartsengine.jfreechart.datasets.CAXYDataset;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.chartsengine.core.messages.ChartsEngineCoreMessages;
import org.txm.chartsengine.jfreechart.core.JFCChartsEngine;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.chartsengine.jfreechart.core.themes.base.BlockRoundBorder;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.swing.CustomHTMLToolTip;
import org.txm.core.preferences.TBXPreferences;

/**
 * Renderer providing item selection system and drawing features for selected item and custom rendering for CA charts.
 * 
 * @author sjacquot
 *
 */
public class CAItemSelectionRenderer extends CAItemStyleRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5647315463691068990L;


	/**
	 * Creates a renderer dedicated to CA charts.
	 */
	public CAItemSelectionRenderer(CA ca, JFreeChart chart) {
		super(ca, chart);
	}


	@Override
	public void init() {

		super.init();

		this.setDefaultLinesVisible(false);
		this.setDefaultOutlinePaint(Color.decode("#666666")); //$NON-NLS-1$


		// Items colors (Highcharts color codes)
		Color blue = new Color(47, 126, 216, 90);
		this.setSeriesPaint(0, blue);
		Color red = new Color(255, 0, 0, 90);
		this.setSeriesPaint(1, red);

		// Create the fonts
		((JFCChartsEngine) this.getResult().getChartsEngine()).getJFCTheme().createFonts(this.getResult());

		// Legends
		ArrayList<Color> palette = new ArrayList<Color>(2);
		palette.add(blue);
		palette.add(red);
		LegendTitle legendTitle = chart.getLegend(0);
		if (legendTitle != null) {
			legendTitle.setFrame(new BlockRoundBorder(Color.GRAY));
			((JFCChartsEngine) this.getResult().getChartsEngine()).getJFCTheme().applyLegendDefaultSettings(legendTitle, false, false, palette);
		}

		// Grid
		chart.getXYPlot().setDomainGridlineStroke(new BasicStroke(0.5f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f, new float[] { 2f }, 0f)); // Dashed
		chart.getXYPlot().setRangeGridlineStroke(chart.getXYPlot().getDomainGridlineStroke());
		chart.getXYPlot().setDomainZeroBaselineVisible(true);
		chart.getXYPlot().setRangeZeroBaselineVisible(true);

		// Axis
		chart.getXYPlot().getRangeAxis().setAxisLineVisible(true);
		chart.getXYPlot().getRangeAxis().setTickMarksVisible(true);
		chart.getXYPlot().getDomainAxis().setVerticalTickLabels(false);
		chart.getXYPlot().getDomainAxis().setAxisLineVisible(true);

	}

	@Override
	public void initToolTipGenerator(final IRendererWithItemSelection renderer) {
		// Rows
		this.setSeriesToolTipGenerator(0, new XYToolTipGenerator() {

			@Override
			public String generateToolTip(XYDataset dataset, int series, int item) {

				try {
					// Coordinates format
					String pattern = new String("#.##"); //$NON-NLS-1$
					DecimalFormat format = new DecimalFormat(pattern);

					// Hexadecimal color
					Color color = (Color) getSeriesPaint(series);
					String hex = "#" + Integer.toHexString(color.getRGB()).substring(2); //$NON-NLS-1$

					CAXYDataset caDataset = (CAXYDataset) dataset;
					StringBuilder tooltip = new StringBuilder(CustomHTMLToolTip.getDefaultHTMLBody(renderer, hex) + "<p style=\"color: " + hex + "\"><b>" + caDataset.getLabel(series, item) + "</b></p><p>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					+ CACoreMessages.frequency + ChartsEngineCoreMessages.EMPTY + " <b>" + caDataset.getFrequency(series, item).replace("-", "") + "</b></p>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					+ "<p>" + CACoreMessages.coordinates + ChartsEngineCoreMessages.EMPTY + " <b>[" + format.format(caDataset.getX(series, item)) + ":" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					+ format.format(caDataset.getY(series, item)) + "]</b></p>"); //$NON-NLS-1$  
					
					// Styling debug data
					if(TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
						tooltip.append(getStylingDebugData(series, item));
					}
					
					tooltip.append("</body><html>"); //$NON-NLS-1$
					return tooltip.toString();
				}
				catch (Exception e) {
				}
				return "";
			}
		});

		// Columns
		this.setSeriesToolTipGenerator(1, new XYToolTipGenerator() {

			@Override
			public String generateToolTip(XYDataset dataset, int series, int item) {

				try {
					// Coordinates format
					String pattern = new String("#.##"); //$NON-NLS-1$
					DecimalFormat format = new DecimalFormat(pattern);

					// Hexadecimal color
					Color color = (Color) getSeriesPaint(series);
					String hex = "#" + Integer.toHexString(color.getRGB()).substring(2); //$NON-NLS-1$

					CAXYDataset caDataset = (CAXYDataset) dataset;
					StringBuilder tooltip = new StringBuilder(CustomHTMLToolTip.getDefaultHTMLBody(renderer, hex) + "<p style=\"color: " + hex + "\"><b>" + caDataset.getLabel(series, item) + "</b></p><p>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					+ CACoreMessages.t + ChartsEngineCoreMessages.EMPTY + " <b>" + caDataset.getFrequency(series, item).replace("-", "") + "</b></p>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					+ "<p>" + CACoreMessages.coordinates + ChartsEngineCoreMessages.EMPTY + " <b>[" + format.format(caDataset.getX(series, item)) + ":" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					+ format.format(caDataset.getY(series, item)) + "]</b></p>"); //$NON-NLS-1$
					
					// Styling debug data
					if(TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
						tooltip.append(getStylingDebugData(series, item));
					}
					
					tooltip.append("</body><html>"); //$NON-NLS-1$
					return tooltip.toString();
				}
				catch (Exception e) {
				}
				return "";
			}
		});

	}


	/**
	 * Gets a string representing some styling data information for the specified item in the specified series.
	 * @param series
	 * @param item
	 * @return
	 */
	public String getStylingDebugData(int series, int item) {
		
		StringBuilder debugData = new StringBuilder("<p><br/><b>Styling debug data</b>"); //$NON-NLS-1$
		Object value = null;
		
		// Font color
		if(series == 0 && rowFontColorStyle.get(item) != null) {
			value = rowFontColorStyle.get(item);
		}
		else if(series == 1 && colFontColorStyle.get(item) != null) {
			value = colFontColorStyle.get(item);
		}
		if (value != null) {
			debugData.append("<br/>Font color channels = "); //$NON-NLS-1$
			debugData.append(value);
			value = null;
		}
		
		// Font alpha color
		if(series == 0 && rowFontAlphaStyle.get(item) != null) {
			value = rowFontAlphaStyle.get(item);
		}
		else if(series == 1 && colFontAlphaStyle.get(item) != null) {
			value = colFontAlphaStyle.get(item);
		}
		if (value != null) {
			debugData.append("<br/>Font alpha channel (opacity) = "); //$NON-NLS-1$
			debugData.append(value);
			value = null;
		}
		
		// Font size scaling
		if(series == 0 && rowFontSizeStyle.get(item) != null) {
			value = rowFontSizeStyle.get(item);
		}
		else if(series == 1 && colFontSizeStyle.get(item) != null) {
			value = colFontSizeStyle.get(item);
		}
		if (value != null) {
			debugData.append("<br/>Font size scaling = x"); //$NON-NLS-1$
			debugData.append(value);
			value = null;
		}
		
		// Shape color
		if(series == 0 && rowPointColorStyle.get(item) != null) {
			value = rowPointColorStyle.get(item);
		}
		else if(series == 1 && colPointColorStyle.get(item) != null) {
			value = colPointColorStyle.get(item);
		}
		if (value != null) {
			debugData.append("<br/>Shape color channels = "); //$NON-NLS-1$
			debugData.append(value);
			value = null;
		}
		
		// Shape alpha color
		if(series == 0 && rowPointAlphaStyle.get(item) != null) {
			value = rowPointAlphaStyle.get(item);
		}
		else if(series == 1 && colPointAlphaStyle.get(item) != null) {
			value = colPointAlphaStyle.get(item);
		}
		if (value != null) {
			debugData.append("<br/>Shape alpha channel (opacity) = "); //$NON-NLS-1$
			debugData.append(value);
			value = null;
		}
		
		// Shape size scaling
		if(series == 0 && rowPointSizeStyle.get(item) != null) {
			value = rowPointSizeStyle.get(item);
		}
		else if(series == 1 && colPointSizeStyle.get(item) != null) {
			value = colPointSizeStyle.get(item);
		}
		if (value != null) {
			debugData.append("<br/>Shape size scaling = x"); //$NON-NLS-1$
			debugData.append(value);
			value = null;
		}
		
		
		debugData.append("</p>"); //$NON-NLS-1$
		return debugData.toString();
		
	}
	
	

	//	@Override
	//	public void initItemLabelGenerator()	{
	//		XYItemLabelGenerator generator = new XYItemLabelGenerator() {
	//
	//			@Override
	//			public String generateLabel(XYDataset dataset, int series, int item) {
	//				CAXYDataset caDataset = (CAXYDataset) dataset;
	//				return caDataset.getLabel(series, item);
	//			}
	//		};
	//
	//		this.setDefaultItemLabelGenerator(generator);
	//		this.setDefaultItemLabelsVisible(true);
	//	}



	//FIXME: SJ, 2024-10-02: to remove when new code will be valdated
	//	@Override
	//	public Font getItemLabelFont(int series, int item) {
	//
	//		// regular
	//		Font font = super.getItemLabelFont(series, item);
	//		font = font.deriveFont(Font.PLAIN, font.getSize());
	//
	//		// Change item label font size and style
	//		if (this.multipleItemsSelector.isMouseOverItem(series, item) || this.multipleItemsSelector.isSelectedItem(series, item)) {
	//			font = font.deriveFont(Font.BOLD, font.getSize() * 1.2f);
	//		}
	//		
	////		// adapt margin between label and shape in proportion to the font size
	////		// FIXME: SJ: leads to some concurrent access modification exceptions 
	//////		if(this.getResult().isShowPointShapes()) {
	//////			this.setItemLabelAnchorOffset(font.getSize() * 0.15f);			
	//////		}
	//		
	//		return font;
	//	}




	@Override
	public Shape getItemShape(int series, int item) {

		Shape shape = super.getItemShape(series, item);
		Shape initShape = shape;
		
		// visible shapes mode
		if (this.getResult().isShowPointShapes()) {

			// Highlight selected item
			if (this.multipleItemsSelector.isMouseOverItem(series, item) || this.multipleItemsSelector.isSelectedItem(series, item)) {
				
				// Abort highlighting if the shape was styled has "hidden"
				if (initShape == EMPTYAREA) {
					return EMPTYAREA;
				}

				String label = ((CAXYDataset) this.getPlot().getDataset()).getLabel(series, item);
				Font font = this.getItemLabelFont(series, item);
				
				// Add a background rectangle to the label selected or mouse over item
				Rectangle2D labelBounds = font.getStringBounds(label, ((Graphics2D) this.multipleItemsSelector.getChartPanel().getGraphics()).getFontRenderContext());

				double horizontalPadding = 5;
				Shape labelBackground = new RoundRectangle2D.Double(-(labelBounds.getWidth() + horizontalPadding) / 2, -labelBounds.getHeight() - this.getItemLabelInsets().getTop() * 2,
						labelBounds.getWidth() + horizontalPadding, labelBounds.getHeight() + this.getItemLabelInsets().getTop(), 5, 5);

				Area a = new Area(shape);
				a.add(new Area(labelBackground));

				// FIXME: SJ: test de tooltip text à la main
				//				Shape tooltip = new Rectangle2D.Double(10, 10, 50, 50);
				//				GlyphVector text =  this.getBaseItemLabelFont().createGlyphVector(((Graphics2D) this.multipleItemsSelector.getChartPanel().getGraphics()).getFontRenderContext(), "testteezrezrzerzerzerzerze");
				////				t.setToTranslation(-100,  -200);
				//				Shape textShape = t.createTransformedShape(text.getOutline());
				//				a.add(new Area(tooltip));
				//				a.add(new Area(textShape));

				return a;
			}
		}
		// non visible shapes mode
		// SJ: sets the shape as bounding box of the item label (essentially for the mouse selection purpose)
		// SJ: we can't use the global preference renderer.setDefaultShapesVisible(ca.isShowPointShapes()) because there is no shape at all then the mouse over features doesn't work
		else {
			
			if (this.multipleItemsSelector.getChartPanel().getGraphics() != null) {
				
				String label = ((CAXYDataset) this.getPlot().getDataset()).getLabel(series, item);
				Font font = this.getItemLabelFont(series, item);
				
				Rectangle2D bounds = font.getStringBounds(label, ((Graphics2D) this.multipleItemsSelector.getChartPanel().getGraphics()).getFontRenderContext());
				shape = new RoundRectangle2D.Double(-(bounds.getWidth()) / 2, -bounds.getHeight() / 2, bounds.getWidth(), bounds.getHeight(), 5, 5);
			}
			// Graphics of ChartPanel object can be null when the chart is exported without an opened CA editor
			else {
				shape = new RoundRectangle2D.Double(-(10) / 2, -10 / 2, 10, 10 - 2, 5, 5);
			}

			// Highlight mouse over and selected item
			if (this.multipleItemsSelector.isMouseOverItem(series, item) || this.multipleItemsSelector.isSelectedItem(series, item)) {
				
				// Abort highlighting if the shape was styled has "hidden"
				if (initShape == EMPTYAREA) {
					return EMPTYAREA;
				}
				
				AffineTransform t = new AffineTransform();
				//FIXME: SJ: the scaling factor is not accurate (still proportional to the shape size ant not a fixed value as ex.: + 7px).
				double scalingFactor = (shape.getBounds2D().getWidth() + this.multipleItemsSelector.getShapeSizeIncreaseValue()) / shape.getBounds2D().getWidth();
				t.setToScale(scalingFactor, scalingFactor);
				shape = t.createTransformedShape(shape);
			}
		}

		return shape;
	}



	@Override
	public Paint getItemLabelPaint(int series, int item) {

		Color color = (Color) super.getItemLabelPaint(series, item);

		// visible shapes mode, hide label using transparency
		// SJ: we can't use the global preference renderer.setDefaultItemLabelsVisible(ca.isShowPointLabels()); because there is no label at all then the mouse over features doesn't show the label
		if (!this.getResult().isShowPointLabels() && !this.multipleItemsSelector.isMouseOverItem(series, item) && !this.multipleItemsSelector.isSelectedItem(series, item)) {
			color = new Color(color.getRed(), color.getGreen(), color.getBlue(), 0);
		}
		return color;
	}




	@Override
	public Paint getItemPaint(int series, int item) {

		Color color = null;

		if (this.getResult().isShowPointShapes()) {
			color = (Color) super.getItemPaint(series, item);

			// Mouse over shape
			if (this.multipleItemsSelector.isMouseOverItem(series, item)) {
				return new Color(color.getRed(), color.getGreen(), color.getBlue(), 195);
			}
			// Multiple item selection
			else if (this.multipleItemsSelector.isSelectedItem(series, item)) {
				return new Color(color.getRed(), color.getGreen(), color.getBlue(), 140);
			}
		}
		else {

			// Mouse over shape
			if (this.multipleItemsSelector.isMouseOverItem(series, item)) {
				color = new Color(255, 255, 255, 220);
			}
			// Multiple item selection
			else if (this.multipleItemsSelector.isSelectedItem(series, item)) {
				return new Color(255, 255, 255, 140);
			}
			else {
				color = new Color(0, 0, 0, 0);
			}

		}
		return color;
	}



}
