/**
 * 
 */
package org.txm.ca.core.chartsengine.base;

import java.util.ArrayList;

import org.txm.ca.core.functions.CA;

/**
 * Progression chart creator interface.
 * 
 * @author sjacquot
 *
 */
public interface CAChartCreator {


	/**
	 * Gets the currently selected item in the specified series in the chart.
	 * 
	 * @param chart
	 * @param series
	 * @return a list of the selected point ID
	 */
	public ArrayList<String> getCAFactorialMapChartSelectedPoints(Object chart, int series);


	/**
	 * Highlights points in the CA factorial map chart according to the specified labels.
	 * 
	 * @param chart
	 * @param rows
	 * @param labels
	 */
	public abstract void updateChartCAFactorialMapHighlightPoints(Object chart, boolean rows, String[] labels);

	/**
	 * Sets the items selection order on label.
	 * 
	 * @param chart
	 */
	public abstract void updateChartCAFactorialMapSetLabelItemsSelectionOrder(Object chart, String[] rowLabels, String[] colLabels);


	/**
	 * Sets the CA factorial map dimensions.
	 */
	public abstract void updateChartCAFactorialMapSetDimensions(CA ca);


}
