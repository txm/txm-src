package org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.chartcreators;

import java.awt.BasicStroke;
import java.awt.Color;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.TreeMap;

import org.eclipse.osgi.util.NLS;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYLineAnnotation;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.chart.ui.TextAnchor;
import org.jfree.chart.util.UnitType;
import org.rosuda.REngine.REXPMismatchException;
import org.txm.ca.core.chartsengine.base.CAChartCreator;
import org.txm.ca.core.chartsengine.base.Utils;
import org.txm.ca.core.chartsengine.jfreechart.datasets.CAXYDataset;
import org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.renderers.CAItemSelectionRenderer;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.ca.core.preferences.CAPreferences;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.chartsengine.jfreechart.core.JFCChartCreator;
import org.txm.chartsengine.jfreechart.core.renderers.MultipleItemsSelector;
import org.txm.statsengine.core.StatException;
import org.txm.utils.logger.Log;

/**
 * JFC CA chart creator.
 *
 * @author sjacquot
 *
 */
public class JFCCAChartCreator extends JFCChartCreator<CA> implements CAChartCreator {

	@Override
	public JFreeChart createChart(CA ca) {

		JFreeChart chart = null;

		try {
			//FIXME: SJ: the dataset should not be recreated in updateChart(). It should be created here and updated in updateChart()
//			CAXYDataset dataset = new CAXYDataset(ca);
//			chart = ChartFactory.createScatterPlot(Utils.createCAFactorialMapChartTitle(ca),
//					"", "", dataset, PlotOrientation.VERTICAL, //$NON-NLS-1$ //$NON-NLS-2$
//					CAPreferences.getInstance().getBoolean(ChartsEnginePreferences.SHOW_LEGEND), false, false);

			chart = ChartFactory.createScatterPlot(Utils.createCAFactorialMapChartTitle(ca),
					"", "", null, PlotOrientation.VERTICAL, //$NON-NLS-1$ //$NON-NLS-2$
					CAPreferences.getInstance().getBoolean(ChartsEnginePreferences.SHOW_LEGEND), false, false);



			// Custom renderer
			chart.getXYPlot().setRenderer(new CAItemSelectionRenderer(ca, chart));
		}
		catch (Exception e) {
			Log.severe(NLS.bind(CACoreMessages.CantCreateCAFactorialMapScatterPlotP0, e));
			Log.printStackTrace(e);
			return null;
		}


		return chart;

	}

	@Override
	public void updateChart(CA ca) {

		JFreeChart chart = (JFreeChart) ca.getChart();

		// freeze rendering while computing
		chart.setNotify(false);

		try {
			chart.getXYPlot().setDataset(new CAXYDataset(ca));
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		CAItemSelectionRenderer renderer = (CAItemSelectionRenderer) chart.getXYPlot().getRenderer();

		renderer.setSeriesVisible(0, ca.isShowVariables());
		renderer.setSeriesVisible(1, ca.isShowIndividuals());

		// Create chart title
		chart.setTitle(Utils.createCAFactorialMapChartTitle(ca));

		// Create the limits border
		this.createCAFactorialMapChartLimitsBorder(chart);

		this.updateChartCAFactorialMapSetDimensions(ca);


		// Configuration for drawing the point shapes (and the item labels or not)
		if (ca.isShowPointShapes()) {

			renderer.setSeriesItemLabelPaint(0, renderer.getDefaultItemLabelPaint());
			renderer.setSeriesItemLabelPaint(1, renderer.getDefaultItemLabelPaint());

			// Labels position
			ItemLabelPosition position = new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, TextAnchor.BOTTOM_CENTER, TextAnchor.BOTTOM_CENTER, 0.0);
			renderer.setDefaultPositiveItemLabelPosition(position);
			renderer.setDefaultNegativeItemLabelPosition(position);
			renderer.setItemLabelInsets(new RectangleInsets(UnitType.ABSOLUTE, 3, 1, 1, 1));

		}
		// Configuration for drawing only the item labels
		else {

			// Labels colors
			Color rowsColor = (Color) renderer.getSeriesPaint(0);
			rowsColor = new Color(rowsColor.getRed(), rowsColor.getGreen(), rowsColor.getBlue(), 235);
			Color colsColor = (Color) renderer.getSeriesPaint(1);
			colsColor = new Color(colsColor.getRed(), colsColor.getGreen(), colsColor.getBlue(), 235);
			renderer.setSeriesItemLabelPaint(0, rowsColor);
			renderer.setSeriesItemLabelPaint(1, colsColor);

			// Labels position
			ItemLabelPosition position = new ItemLabelPosition(ItemLabelAnchor.CENTER, TextAnchor.CENTER, TextAnchor.CENTER, 0.0);
			renderer.setDefaultPositiveItemLabelPosition(position);
			renderer.setDefaultNegativeItemLabelPosition(position);
			//renderer.setItemLabelInsets(RectangleInsets.ZERO_INSETS);

		}

//		// show/hide shapes
		//renderer.setDefaultShapesVisible(ca.isShowPointShapes()); // SJ: we can't use that otherwise the mouse over doesn't work anymore cause it is based on the shape bound detection (the XYItemEntity)
//		// show/hide labels
		//renderer.setDefaultItemLabelsVisible(ca.isShowPointLabels()); // SJ: we can't use that otherwise the mouse over doesn't work anymore, there's no label




		// Apply style sheets
		// TODO: SJ: this code must be moved to super.updateChart()
		try {
			renderer.resetStyles();
			// System.out.println("UPDATE STYLE FROM STYLINGSHEETS");
			renderer.updateRulesFromStylingsheet(ca.getStylingSheet(), ca); // manage a list of stylingsheets ?
			renderer.updateRulesFromStylingsheet(ca.getFilterStylingSheet(), ca); // TODO hard code filter and selection stylesheets ?
//			renderer.updateRulesFromStylingsheet(ca.getSelectionStylingSheet(), ca);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		super.updateChart(ca);

		// FIXME: SJ
		// this.getChartsEngine().squareOffGraph((JFreeChart) chart, 200, 200d);

	}



	/**
	 * Add some borders to the limits of the specified XY plot.
	 *
	 * @param chart
	 */
	public void createCAFactorialMapChartLimitsBorder(JFreeChart chart) {

		XYPlot plot = chart.getXYPlot();

		// Remove all existent annotations
		plot.clearAnnotations();


		// Gets the extreme coordinates values
		double minX = 0, maxX = 0, minY = 0, maxY = 0;

		// FIXME: SJ: new version
		// minX = plot.getDataRange(plot.getDomainAxis()).getLowerBound();
		// maxX = plot.getDataRange(plot.getDomainAxis()).getUpperBound();
		//
		// minY = plot.getDataRange(plot.getRangeAxis()).getLowerBound();
		// maxY = plot.getDataRange(plot.getRangeAxis()).getUpperBound();

		// FIXME: SJ: old version, to remove when new version will be validated
		// Rows
		if (plot.getRenderer().isSeriesVisible(0)) {
			minX = ((CAXYDataset) plot.getDataset()).getMinX(0);
		}
		// Cols
		if (plot.getRenderer().isSeriesVisible(1)) {
			double tmpMinX = ((CAXYDataset) plot.getDataset()).getMinX(1);
			if (tmpMinX < minX) {
				minX = tmpMinX;
			}
		}

		// Rows
		if (plot.getRenderer().isSeriesVisible(0)) {
			maxX = ((CAXYDataset) plot.getDataset()).getMaxX(0);
		}
		// Cols
		if (plot.getRenderer().isSeriesVisible(1)) {
			double tmpMaxX = ((CAXYDataset) plot.getDataset()).getMaxX(1);
			if (tmpMaxX > maxX) {
				maxX = tmpMaxX;
			}
		}

		// Rows
		if (plot.getRenderer().isSeriesVisible(0)) {
			minY = ((CAXYDataset) plot.getDataset()).getMinY(0);
		}
		// Cols
		if (plot.getRenderer().isSeriesVisible(1)) {
			double tmpMinY = ((CAXYDataset) plot.getDataset()).getMinY(1);
			if (tmpMinY < minY) {
				minY = tmpMinY;
			}
		}

		// Rows
		if (plot.getRenderer().isSeriesVisible(0)) {
			maxY = ((CAXYDataset) plot.getDataset()).getMaxY(0);
		}
		// Cols
		if (plot.getRenderer().isSeriesVisible(1)) {
			double tmpMaxY = ((CAXYDataset) plot.getDataset()).getMaxY(1);
			if (tmpMaxY > maxY) {
				maxY = tmpMaxY;
			}
		}

		// Add some margins to the border
		// FIXME: SJ: we should compute proportionally to the real series shape dimensions but the code below, that trying to do that, doesn't work
		// double shapeHalfWidth = chart.getXYPlot().getRenderer().getSeriesShape(0).getBounds2D().getWidth() / 2;
		double shapeHalfWidth = 0.02;
		minX -= shapeHalfWidth;
		maxX += shapeHalfWidth;
		minY -= shapeHalfWidth;
		maxY += shapeHalfWidth;


		// Create the border
		BasicStroke dashedStroke = new BasicStroke(0.5f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f, new float[] { 2f }, 0f);
		XYLineAnnotation annotation;
		Color borderColor = Color.GRAY;

		// Left border
		annotation = new XYLineAnnotation(minX, minY, minX, maxY, dashedStroke, borderColor);
		plot.addAnnotation(annotation);

		// Right border
		annotation = new XYLineAnnotation(maxX, minY, maxX, maxY, dashedStroke, borderColor);
		plot.addAnnotation(annotation);

		// Bottom border
		annotation = new XYLineAnnotation(minX, minY, maxX, minY, dashedStroke, borderColor);
		plot.addAnnotation(annotation);

		// Top border
		annotation = new XYLineAnnotation(minX, maxY, maxX, maxY, dashedStroke, borderColor);
		plot.addAnnotation(annotation);

	}


	@Override
	public void updateChartCAFactorialMapHighlightPoints(Object chart, boolean rows, String[] labels) {

		MultipleItemsSelector selector = (MultipleItemsSelector) ((CAItemSelectionRenderer) ((JFreeChart) chart).getXYPlot().getRenderer()).getItemsSelector();

		// Rows
		int series = 0;
		// Columns
		if (!rows) {
			series = 1;
		}
		selector.removeAllSelectedItems(series);
		int[] items = ((CAXYDataset) ((JFreeChart) chart).getXYPlot().getDataset()).getLabelIndices(series, labels);

		for (int i = 0; i < items.length; i++) {
			selector.addSelectedItem(series, items[i]);
		}

		//FIXME: SJ: became useless? MD maybe only when items is empty
		if (items == null || items.length == 0) {
			((JFreeChart) chart).setNotify(true);
		}
	}


	@Override
	public void updateChartCAFactorialMapSetDimensions(CA ca) {

		int dimension1 = ca.getFirstDimension();
		int dimension2 = ca.getSecondDimension();

		JFreeChart chart = (JFreeChart) ca.getChart();

		// Modify data set
		((CAXYDataset) chart.getXYPlot().getDataset()).setAxis1(dimension1);
		((CAXYDataset) chart.getXYPlot().getDataset()).setAxis2(dimension2);

		// Update axis labels
		// FIXME: SJ: create a method in CA to directly get a singular value as percent ?
		try {
			double sinuglarValuesSum = ca.getValeursPropresSum();
			DecimalFormat f = new DecimalFormat("###.00"); //$NON-NLS-1$
			chart.getXYPlot().getDomainAxis().setLabel(NLS.bind(CACoreMessages.axisP0P1Percent, dimension1, f.format(100 * ca.getValeursPropres()[dimension1 - 1] / sinuglarValuesSum)));
			chart.getXYPlot().getRangeAxis().setLabel(NLS.bind(CACoreMessages.axisP0P1Percent, dimension2, f.format(100 * ca.getValeursPropres()[dimension2 - 1] / sinuglarValuesSum)));

			// Refresh data set
			// FIXME: SJ: any way to fire a data set event rather than reassign the same data set?
			chart.getXYPlot().setDataset(chart.getXYPlot().getDataset());
			// FIXME: SJ: the code below doesn't neither center the view nor update the axes ticks of the new chart, continue tests
			// ((JFreeChart) chart).setNotify(true);


			// Update the limits border
			this.createCAFactorialMapChartLimitsBorder(chart);

		}
		catch (StatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (REXPMismatchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	@Override
	public ArrayList<String> getCAFactorialMapChartSelectedPoints(Object chart, int series) {

		MultipleItemsSelector selector = (MultipleItemsSelector) ((CAItemSelectionRenderer) ((JFreeChart) chart).getXYPlot().getRenderer()).getItemsSelector();
		ArrayList<String> pointLabels = new ArrayList<>(selector.getSelectedItemsCount(series));

		if (selector.getSelectedItemsCount(series) > 0) {

			CAXYDataset dataset = (CAXYDataset) ((JFreeChart) chart).getXYPlot().getDataset();
			int[] selectedItems = selector.getSelectedItems(series);

			for (int i = 0; i < selectedItems.length; i++) {
				pointLabels.add(dataset.getLabel(series, selectedItems[i]));
			}
		}
		return pointLabels;
	}



	@Override
	public void updateChartCAFactorialMapSetLabelItemsSelectionOrder(Object chart, String[] rowLabels, String[] colLabels) {

		MultipleItemsSelector selector = (MultipleItemsSelector) ((CAItemSelectionRenderer) ((JFreeChart) chart).getXYPlot().getRenderer()).getItemsSelector();
		selector.setCyclicItemsOrder(this.getItemsAndSeriesOrderedByLabels((CAXYDataset) ((JFreeChart) chart).getXYPlot().getDataset(), rowLabels, colLabels));
	}


	/**
	 * Gets a map where the values of the dataset are ordered according to the specified label arrays, keeping the order of these arrays and flatten but grouped by series.
	 *
	 * @param dataset
	 * @return
	 */
	public TreeMap<Object, ArrayList<Integer>> getItemsAndSeriesOrderedByLabels(CAXYDataset dataset, String[] rowLabels, String[] colLabels) {

		TreeMap<Object, ArrayList<Integer>> orderedValues = new TreeMap<>();

		int[] rowIndices = dataset.getOrderedLabelIndices(0, rowLabels);
		int[] colIndices = dataset.getOrderedLabelIndices(1, colLabels);

		int i = 0;
		for (; i < rowIndices.length; i++) {
			ArrayList<Integer> itemData = new ArrayList<>(2);
			itemData.add(0); // series
			itemData.add(rowIndices[i]); // item
			orderedValues.put(i, itemData);
		}
		for (int j = 0; j < colIndices.length; j++) {
			ArrayList<Integer> itemData = new ArrayList<>(2);
			itemData.add(1); // series
			itemData.add(colIndices[j]); // item
			orderedValues.put(j + i, itemData);
		}

		return orderedValues;
	}

	@Override
	public Class<CA> getResultDataClass() {
		return CA.class;
	}



}
