package org.txm.ca.core.chartsengine.jfreechart.datasets;

import java.util.List;

import org.txm.ca.core.functions.CA;
import org.txm.statsengine.core.StatException;

public class CA1DXYDataset extends CAXYDataset {

	private static final long serialVersionUID = -308250066453829145L;

	public CA1DXYDataset(CA ca) throws Exception {

		super(ca);

	}


	@Override
	public Number getY(int series, int item) {
		try {
			// Rows
			if (series == 0) {
				// return this.rowCoordinates[item][this.axis2];

				return (Number) (ca.getRowCos2()[item])[this.axis1];

			}
			else { // Cols
				return -(double) (ca.getColCos2()[item])[this.axis1];
			}
		}
		catch (StatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Gets the maximum Y coordinates value in the specified series.
	 * 
	 * @param series
	 * @return
	 */
	public double getMaxY(int series) {

		// return this.getMaxValue(series, this.axis2);
		return 0d;
	}

	/**
	 * Gets the minimum Y coordinates value in the specified series.
	 * 
	 * @param series
	 * @return
	 */
	public double getMinY(int series) {

		// return this.getMinValue(series, this.axis2);
		return 0d;
	}
}
