package org.txm.ca.core.chartsengine.r;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.txm.ca.core.chartsengine.base.CAChartCreator;
import org.txm.ca.core.chartsengine.base.Utils;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.ca.core.statsengine.r.functions.FactoMineRCA;
import org.txm.chartsengine.r.core.RChartCreator;
import org.txm.chartsengine.r.core.RChartsEngine;
import org.txm.utils.logger.Log;

/**
 * R CA chart creator.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class RCAChartCreator extends RChartCreator<CA> implements CAChartCreator {

	/**
	 * Creates a factorial map chart from the specified CA result from "FactoMineR" R package and writes it in the specified file.
	 * 
	 * @param ca
	 * @param destFile
	 * @param showIndividuals
	 * @param showVariables
	 * @param firstDimension
	 * @param secondDimension
	 * @return the generated file containing the chart
	 */
	public File createFactorialMapChart(FactoMineRCA ca, File destFile, boolean showIndividuals, boolean showVariables, int firstDimension, int secondDimension, String title) {

		try {

			String label = ""; //$NON-NLS-1$
			String invisible = ""; //$NON-NLS-1$
			if (showIndividuals)
				label += ", \"col\""; //$NON-NLS-1$
			else
				invisible += ", \"col\""; //$NON-NLS-1$

			if (showVariables)
				label += ", \"row\""; //$NON-NLS-1$
			else
				invisible += ", \"row\""; //$NON-NLS-1$

			if (label.length() > 0)
				label = "c(" + label.substring(1) + ")"; //$NON-NLS-1$ //$NON-NLS-2$
			else
				label = "c(\"all\")"; //$NON-NLS-1$

			if (invisible.length() > 0)
				invisible = "c(" + invisible.substring(1) + ")"; //$NON-NLS-1$ //$NON-NLS-2$
			else
				invisible = "c()"; //$NON-NLS-1$

			String axes = "c(" + firstDimension + ", " + secondDimension + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

			String cmd = "plot.CA(" + ca.getSymbol() + ", label=" + label + ", invisible=" + invisible + ", axes=" + axes + ", title=\"" + title + "\", cex=.8)"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$

			this.getChartsEngine().plot(destFile, cmd);

		}
		catch (Exception e) {
			Log.severe(CACoreMessages.bind(CACoreMessages.cantCreateCAChartFileChartsEngineColonP0P1, RChartsEngine.DESCRIPTION + " - FactoMineR package", e)); //$NON-NLS-1$
			Log.printStackTrace(e);
		}

		return destFile;
	}


	/**
	 * Creates a factorial map chart from the specified CA result from "CA" R package and writes it in the specified file.
	 * 
	 * @param ca
	 * @param destFile
	 * @param showIndividuals
	 * @param showVariables
	 * @param firstDimension
	 * @param secondDimension
	 * @return the generated file containing the chart
	 */
	public File createFactorialMapChart(org.txm.ca.core.statsengine.r.functions.CA ca, File destFile, boolean showIndividuals, boolean showVariables, int firstDimension, int secondDimension) {

		try {

			double[] sv = ca.getEigenvalues();
			double tot = 0;
			DecimalFormat f = new DecimalFormat("###.00"); //$NON-NLS-1$
			for (int i = 0; i < sv.length; i++)
				tot += (sv[i]);

			double inertie1 = (((sv[firstDimension - 1] * sv[firstDimension - 1]) / tot) * 100.0);
			double inertie2 = (((sv[secondDimension - 1] * sv[secondDimension - 1]) / tot) * 100.0);
			String contrib1 = "axis(" + firstDimension + "): inertia=" + f.format(inertie1) + "%"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			String contrib2 = "axis(" + secondDimension + "): inertia=" + f.format(inertie2) + "%"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			String cmd = "plot(" + ca.getSymbol() + ", dim=c(" + firstDimension + ", " + secondDimension + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			cmd += ", what=c(" + (showVariables ? "\"all\"" : "\"none\"") + ", " + (showIndividuals ? "\"all\"" : "\"none\"") + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
			// cmd +=
			// ", mass = c(TRUE, TRUE), contrib = c(\"relative\", \"relative\")";
			cmd += ", col = c(\"#0000FF\", \"#FF0000\")"; //$NON-NLS-1$
			cmd += ", cex=.5);"; //$NON-NLS-1$
			cmd += "mtext(\"" + contrib1 + "\", side = 1, line = 2);"; //$NON-NLS-1$ //$NON-NLS-2$
			cmd += "mtext(\"" + contrib2 + "\", side = 2, line = 2);"; //$NON-NLS-1$ //$NON-NLS-2$

			this.getChartsEngine().plot(destFile, cmd);
		}
		catch (Exception e) {
			Log.severe(CACoreMessages.bind(CACoreMessages.cantCreateCAChartFileChartsEngineColonP0P1, RChartsEngine.DESCRIPTION + " - CA package", e)); //$NON-NLS-1$
			Log.printStackTrace(e);
		}

		return destFile;
	}



	@Override
	public File createChart(CA ca, File file) {

		// FactoMineR R package
		if (ca.useFactoMineR()) {
			return this.createFactorialMapChart((FactoMineRCA) ca.getCA(), file, ca.isShowIndividuals(), ca.isShowVariables(), ca.getFirstDimension(), ca.getSecondDimension(), Utils
					.createCAFactorialMapChartTitle(ca));
		}
		// CA R package
		else {
			return this.createFactorialMapChart((org.txm.ca.core.statsengine.r.functions.CA) ca.getCA(), file, ca.isShowIndividuals(), ca.isShowVariables(), ca.getFirstDimension(), ca
					.getSecondDimension());
		}
	}


	@Override
	public Class<CA> getResultDataClass() {
		return CA.class;
	}



	@Override
	public ArrayList<String> getCAFactorialMapChartSelectedPoints(Object chart, int series) {
		// FIXME: not implemented
		Log.severe(this.getClass() + ".getCAFactorialMapChartSelectedPoints(): Not yet implemented."); //$NON-NLS-1$
		return new ArrayList<>(0);
	}


	@Override
	public void updateChartCAFactorialMapHighlightPoints(Object chart, boolean rows, String[] labels) {
		// FIXME: not implemented
		Log.severe(this.getClass() + ".updateChartCAFactorialMapHighlightPoints(): Not yet implemented."); //$NON-NLS-1$
	}


	@Override
	public void updateChartCAFactorialMapSetLabelItemsSelectionOrder(Object chart, String[] rowLabels, String[] colLabels) {
		// FIXME: not implemented
		Log.severe(this.getClass() + ".updateChartCAFactorialMapSetLabelItemsSelectionOrder(): Not yet implemented."); //$NON-NLS-1$
	}


	@Override
	public void updateChartCAFactorialMapSetDimensions(CA ca) {
		// FIXME: not implemented
		Log.severe(this.getClass() + ".updateChartCAFactorialMapSetDimensions(): Not yet implemented."); //$NON-NLS-1$
	}



}
