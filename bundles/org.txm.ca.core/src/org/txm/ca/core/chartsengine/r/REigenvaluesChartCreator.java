package org.txm.ca.core.chartsengine.r;

import java.io.File;

import org.rosuda.REngine.REXP;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.functions.Eigenvalues;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.ca.core.statsengine.r.functions.FactoMineRCA;
import org.txm.chartsengine.r.core.RChartCreator;
import org.txm.statsengine.r.core.RWorkspace;

/**
 * R CA Eigenvalues chart creator.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class REigenvaluesChartCreator extends RChartCreator<Eigenvalues> {

	@Override
	public File createChart(Eigenvalues result, File file) {

		CA ca = result.getCA();

		// FactoMineR R package
		if (ca.useFactoMineR()) {
			return this.createSingularValuesBarPlot((FactoMineRCA) ca.getCA(), file);
		}
		// CA R package
		else {
			return this.createSingularValuesBarPlot((org.txm.ca.core.statsengine.r.functions.CA) ca.getCA(), file);
		}
	}

	/**
	 * Creates a bar plot with eigenvalues of the specified CA result from "FactoMineR" R package.
	 * 
	 * @param ca
	 * @param file
	 * @return the singular values bar plot object
	 */
	public File createSingularValuesBarPlot(FactoMineRCA ca, File file) {

		// Create the X axis labels with custom prefix
		StringBuilder xLabels = new StringBuilder(30);
		xLabels.append("c("); //$NON-NLS-1$
		REXP sv;
		try {
			sv = RWorkspace.getRWorkspaceInstance().eval("length(" + ca.getSymbol() + "$eig[,1])");  //$NON-NLS-1$ $NON-NLS-2$
			int ncol = sv.asInteger();

			for (int i = 0, c = ncol; i < c; i++) {
				if (i > 0) {
					xLabels.append(", "); //$NON-NLS-1$
				}
				xLabels.append("\"" + (i + 1) + "\""); //$NON-NLS-1$ //$NON-NLS-2$
			}
			xLabels.append(")"); //$NON-NLS-1$

			String cmd = "tmpY <- " + ca.getSymbol() + "$eig[,1];\n"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			cmd += "barplot(tmpY, main=\"" //$NON-NLS-1$
					+ CACoreMessages.eigenvalues + "\", ylab=\"" + //$NON-NLS-1$
					CACoreMessages.eigenvalue + "\", xlab=\"" + //$NON-NLS-1$
					CACoreMessages.axis + "\", names.arg=" + xLabels + ");\n"; //$NON-NLS-1$ //$NON-NLS-2$

			this.getChartsEngine().plot(file, cmd);

			return file;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Creates a bar plot with eigenvalues of the specified CA result from "CA" R package.
	 * 
	 * @param ca
	 * @param file
	 * @return the singular values bar plot object
	 */
	public File createSingularValuesBarPlot(org.txm.ca.core.statsengine.r.functions.CA ca, File file) {
		this.getChartsEngine().plot(file, "barplot(" + ca.getSymbol() + "$sv)"); //$NON-NLS-1$ //$NON-NLS-2$
		return file;
	}

	@Override
	public Class<Eigenvalues> getResultDataClass() {
		return Eigenvalues.class;
	}



}
