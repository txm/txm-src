package org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.renderers;

import java.awt.Color;
import java.text.DecimalFormat;
import java.util.List;

import org.jfree.chart.labels.CategoryToolTipGenerator;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.rosuda.REngine.REXPMismatchException;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.chartsengine.core.messages.ChartsEngineCoreMessages;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers.ItemSelectionCategoryBarRenderer;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.swing.CustomHTMLToolTip;


/**
 * Renderer providing item selection system and drawing features for selected item and custom rendering for CA eigenvalues bar charts.
 * 
 * @author sjacquot
 *
 */
public class CAEigenvaluesItemSelectionRenderer extends ItemSelectionCategoryBarRenderer {

	private static final long serialVersionUID = -3251112562911732353L;

	/**
	 * Percent values number format.
	 */
	protected DecimalFormat percentValuesNumberFormat;


	/**
	 * Creates a renderer dedicated to CA charts.
	 */
	public CAEigenvaluesItemSelectionRenderer(CA ca) {
		super();
		this.mouseOverItemSelector.setResult(ca);
		this.percentValuesNumberFormat = new DecimalFormat("#.00"); //$NON-NLS-1$
	}



	@Override
	public void initToolTipGenerator(final IRendererWithItemSelection renderer) {
		this.setDefaultToolTipGenerator(new CategoryToolTipGenerator() {

			@Override
			public String generateToolTip(CategoryDataset dataset, int row, int column) {

				// Hexadecimal color
				Color color = (Color) getSeriesPaint(row);
				String hex = "#" + Integer.toHexString(color.getRGB()).substring(2); //$NON-NLS-1$

				DefaultCategoryDataset catDataset = (DefaultCategoryDataset) dataset;
				Number value = catDataset.getValue(row, column);


				CA ca = (CA) getItemsSelector().getResult();
				List<List<Object>> singularValuesData;
				try {
					singularValuesData = ca.getSingularValuesInfos();
				}
				catch (REXPMismatchException e) {
					e.printStackTrace();
					return e.getMessage();
				}

				return CustomHTMLToolTip.getDefaultHTMLBody(renderer, hex) + "<p>#" + catDataset.getColumnKey(column) + "</p>" //$NON-NLS-1$
						+ "<p><span style=\"color: " + hex + ";\">" + catDataset.getRowKey(row) + ChartsEngineCoreMessages.EMPTY //$NON-NLS-1$
						+ " </span><b>" + valuesNumberFormat.format(value) + "</b></p>" //$NON-NLS-1$
						+ "<p><span style=\"color: " + hex + ";\">" + CACoreMessages.percent + ChartsEngineCoreMessages.EMPTY //$NON-NLS-1$
						+ " </span><b>" + percentValuesNumberFormat.format(singularValuesData.get(column).get(2)) + "</b></p>" //$NON-NLS-1$
						+ "<p><span style=\"color: " + hex + ";\">" + CACoreMessages.charts_singularValues_tooltipSum + CACoreMessages.percent //$NON-NLS-1$
						+ ChartsEngineCoreMessages.EMPTY
						+ " </span><b>" + percentValuesNumberFormat.format(singularValuesData.get(column).get(3)) + "</b></p>" //$NON-NLS-1$
						+ "</body><html>"; //$NON-NLS-1$
			}
		});
	}



}
