package org.txm.ca.core.chartsengine.base;

import org.txm.ca.core.functions.CA;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.chartsengine.core.messages.ChartsEngineCoreMessages;

/**
 * CA chart utility class.
 * 
 * @author sjacquot
 *
 */
//FIXME: SJ: became useless, the method should be moved as default method in interface CAChartCreator
public abstract class Utils {

	public Utils() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * Creates a string title, shared by charts engine implementations, for the CA factorial map chart from the specified result.
	 * 
	 * @param ca
	 * @return the CA title
	 */
	public static String createCAFactorialMapChartTitle(CA ca) {
		return ChartsEngineCoreMessages.bind(CACoreMessages.correspondenceAnalysisFactorialPlaneOfP0, ca.getParent().getName());
	}


}
