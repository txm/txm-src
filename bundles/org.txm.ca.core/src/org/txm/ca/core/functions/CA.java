// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2016-09-19 10:31:00 +0200 (Mon, 19 Sep 2016) $
// $LastChangedRevision: 3298 $
// $LastChangedBy: mdecorde $
//
package org.txm.ca.core.functions;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.jfree.chart.JFreeChart;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.renderers.CAItemStyleRenderer;
import org.txm.ca.core.chartsengine.styling.CAFilterStylingSheet;
import org.txm.ca.core.chartsengine.styling.CAStylerCatalog;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.ca.core.preferences.CAPreferences;
import org.txm.ca.core.statsengine.r.functions.FactoMineRCA;
import org.txm.ca.core.statsengine.r.functions.ICA;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.core.styling.StylingSheet;
import org.txm.chartsengine.core.styling.base.Stylable;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.index.core.functions.PartitionIndex;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.r.core.RResult;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * Correspondence Analysis of a lexical table.
 *
 * @author mdecorde
 * @author sjacquot
 * @author sloiseau
 * 
 */
public class CA extends ChartResult implements RResult, Stylable<CA> {

	/** The ca. */
	protected ICA r_ca;

	protected float cex = 0.7f;

	protected String colfilter = "cos2 0.0", rowfilter = "cos2 0.0"; //$NON-NLS-1$ //$NON-NLS-2$

	/** The colnames. */
	protected String[] colnames = null;

	protected String pattern = "(cos2|contrib) (([0-9]+\\.[0-9]+)|([0-9]+))"; //$NON-NLS-1$

	/** The rownames. */
	protected String[] rownames = null;


	/** The use Factomine R: change this only if you want to use the default R CA package. */
	protected boolean useFactoMineR = true;

	/**
	 * First dimension.
	 */
	@Parameter(key = CAPreferences.FIRST_DIMENSION, type = Parameter.RENDERING)
	protected int firstDimension;

	/**
	 * Second dimension.
	 */
	@Parameter(key = CAPreferences.SECOND_DIMENSION, type = Parameter.RENDERING)
	protected int secondDimension;

	/**
	 * First dimension.
	 */
	@Parameter(key = CAPreferences.MIRRORED_AXES, type = Parameter.COMPUTING)
	protected ArrayList<Integer> pMirroredDimensions;

	/**
	 * To show/hide individuals.
	 */
	@Parameter(key = CAPreferences.SHOW_INDIVIDUALS, type = Parameter.RENDERING)
	protected boolean showIndividuals;

	/**
	 * To show/hide variables.
	 */
	@Parameter(key = CAPreferences.SHOW_VARIABLES, type = Parameter.RENDERING)
	protected boolean showVariables;

	/**
	 * To show/hide the point shapes or only the labels.
	 */
	@Parameter(key = CAPreferences.SHOW_POINT_SHAPES, type = Parameter.RENDERING)
	protected boolean showPointShapes;

	@Parameter(key = CAPreferences.SHOW_POINT_LABELS, type = Parameter.RENDERING)
	protected boolean showPointLabels;

	/**
	 * Supplementary/illustrative row names
	 */
	@Parameter(key = CAPreferences.ROW_SUP_NAMES)
	protected List<String> pRowSupNames;

	/**
	 * Supplementary/illustrative row names
	 */
	@Parameter(key = CAPreferences.COL_SUP_NAMES)
	protected List<String> pColSupNames;

	/**
	 * Supplementary/illustrative row names
	 */
	@Parameter(key = CAPreferences.N_FACTORS_MEMORIZED)
	protected int pNFactorsMemorized;

	@Parameter(key = CAPreferences.STYLINGSHEET, type = Parameter.RENDERING)
	protected StylingSheet stylingSheet;// TODO move this in the ChartResult class

	/**
	 * Styles used to filter row and col items
	 */
	@Parameter(key = CAPreferences.FILTER_STYLINGSHEET, type = Parameter.RENDERING)
	protected StylingSheet filterStylingSheet;// TODO move this in the ChartResult class
	
	/**
	 * switch to PCA instead of CA compute
	 */
	@Parameter(key = "pca", type = Parameter.COMPUTING)
	protected boolean doPCA;// TODO move this in the ChartResult class
	//
	//	/**
	//	 * direct styles created using the CA selection (row or col)
	//	 */
	//	@Parameter(key = "selection_" + CAPreferences.STYLINGSHEET)
	//	protected StylingSheet selectionStylingSheet;// TODO move this in the ChartResult class

	private CAStylerCatalog caCatalog;



	/**
	 * Creates a new empty CA.
	 *
	 * @param lexicalTable the lexical table
	 */
	public CA(LexicalTable lexicalTable) {

		this(null, lexicalTable);
	}

	/**
	 * Creates a new empty CA.
	 *
	 */
	public CA(String parametersNodePath) {

		this(parametersNodePath, null);
	}

	/**
	 * Creates a new empty CA.
	 *
	 * @param lexicalTable the lexical table
	 */
	public CA(String parametersNodePath, LexicalTable lexicalTable) {

		super(parametersNodePath, lexicalTable);

		this.needsToSquareOff = true;
	}

	@Override
	public String[] getExportTXTExtensions() {
		return new String[] {
				"*.txt", "*.Rdata", "*.rds" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		};
	}

	public void setSupplmentaryRowsCols(List<String> rows, List<String> cols) {
		this.pColSupNames = cols;
		this.pRowSupNames = rows;
	}

	public List<String> getSupplmentaryRows() {
		return this.pRowSupNames;
	}

	public List<String> getSupplmentaryCols() {
		return this.pColSupNames;
	}

	@Override
	public void clean() {

		try {
			if (this.getCA() != null) {
				RWorkspace.getRWorkspaceInstance().removeVariableFromWorkspace(this.getCA().getSymbol());
			}
		}
		catch (RWorkspaceException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected boolean __compute(TXMProgressMonitor monitor) throws Exception {

		try {
			if (this.getLexicalTable().getNRows() == 0 || this.getLexicalTable().getNColumns() == 0) {
				Log.warning(CACoreMessages.errorEmptyLexicalTableComputingAborted);
				return false;
			}
			
			if (secondDimension < 0) return false;
			
			if (secondDimension > getNumberOfFactorsMemorized()) {
				secondDimension = getNumberOfFactorsMemorized();
			}
			
//			if (firstDimension > secondDimension) {
//				firstDimension = secondDimension - 1;
//			}
			
			if (firstDimension < 0) return false;
			
			// clear cache
			this.colnames = null;
			this.rownames = null;
			if (rowMinimums != null) this.rowMinimums.clear();
			if (rowMaximums != null) this.rowMaximums.clear();
			if (colMinimums != null) this.colMinimums.clear();
			if (colMaximums != null) this.colMaximums.clear();
			if (rowAHCClasses != null) this.rowAHCClasses.clear();
			if (colAHCClasses != null) this.colAHCClasses.clear();

			try {
				if (this.getCA() != null) {
					RWorkspace.getRWorkspaceInstance().removeVariableFromWorkspace(this.getCA().getSymbol());
				}
			}
			catch (RWorkspaceException e) {
				e.printStackTrace();
			}

			if (this.useFactoMineR) {
				this.r_ca = new FactoMineRCA(this.getLexicalTable().getData());
			}
			else {
				this.r_ca = new org.txm.ca.core.statsengine.r.functions.CA(this.getLexicalTable().getData());
			}

			this.r_ca.compute(pRowSupNames, pColSupNames, pNFactorsMemorized, pMirroredDimensions, doPCA);

			return true;
		}
		catch (Exception e) {
			Log.severe(NLS.bind(CACoreMessages.failedToComputeCAP0, e));
			Log.printStackTrace(e);
			return false;
		}

	}
	
	public void setDoPCA(boolean doPCA) {
		this.doPCA = doPCA;
	}

	public void displayOptions(String col, String row, float cex) {

		this.cex = Math.abs(cex);
		if (col.matches(pattern)) {
			this.colfilter = col;
		}
		else {
			Log.severe(NLS.bind(CACoreMessages.columnsFilterMaformedFollowThisPatternP0, pattern));
		}

		if (row.matches(pattern)) {
			this.rowfilter = row;
		}
		else {
			Log.severe(NLS.bind(CACoreMessages.rowsFilterMalformedFollowThisPatternP0, pattern));
		}
	}

	/**
	 * Gets the CA R object.
	 *
	 * @return the ca
	 */
	public org.txm.ca.core.statsengine.r.functions.ICA getCA() {

		return r_ca;
	}

	public float getCex() {

		return cex;
	}

	public int[] getNumberOfHiddenPoints() {
		if (this.getChart() instanceof JFreeChart jfcChart 
				&& jfcChart.getXYPlot().getRenderer() instanceof CAItemStyleRenderer styleRenderer) {
			return styleRenderer.getNumberOfHiddenPoints();
		}

		return new int[] {0, 0};
	}

	/**
	 * 
	 * @return 4 item array of sums of : row contx, row conty, col contx and col conty
	 * @throws StatException
	 */
	public double[] getSumContXOfVisiblePoints() throws StatException {
		if (this.getChart() instanceof JFreeChart jfcChart 
				&& jfcChart.getXYPlot().getRenderer() instanceof CAItemStyleRenderer styleRenderer) {
			HashSet<Integer> indexes = styleRenderer.getHiddenRowPointsIndexes();
			double[][] contribs = this.getRowContrib();
			double sumX = 0;
			double sumY = 0;
			for (int i = 0 ; i < contribs.length ; i++) {
				if (!indexes.contains(i)) {
					sumX += contribs[i][getFirstDimension() - 1];
					sumY += contribs[i][getSecondDimension() - 1];
				}
			}

			indexes = styleRenderer.getHiddenColPointsIndexes();
			contribs = this.getColContrib();
			double colSumX = 0;
			double colSumY = 0;
			for (int i = 0 ; i < contribs.length ; i++) {
				if (!indexes.contains(i)) {
					colSumX += contribs[i][getFirstDimension() -1];
					colSumY += contribs[i][getSecondDimension() -1];
				}
			}
			
			return new double[] {sumX, sumY, colSumX, colSumY};
		}

		return new double[] {0, 0, 0, 0};
	}

	/**
	 * 
	 * @return the column filter -> only used by FactoMineR
	 */
	public String getColFilter() {

		return colfilter;
	}

	/**
	 * Gets the column names as array.
	 *
	 * @return the col names
	 * @throws StatException the stat exception
	 */
	public String[] getColNames() throws StatException {

		if (colnames == null) {
			colnames = r_ca.getColNames().asStringsArray();
		}
		return colnames;
	}

	/**
	 * Gets the columns coordinates as array.
	 *
	 * @return the columns coordinates
	 */
	public double[][] getColsCoords() {

		return this.r_ca.getColsCoords();
	}

	/**
	 * Gets the columns distances as array.
	 *
	 * @return the cols dist
	 * @throws StatException the stat exception
	 */
	public double[] getColsDist() throws StatException {

		return this.r_ca.getColDist();
	}

	/**
	 * Get the columns inertia as array.
	 *
	 * @return the cols inertia
	 * @throws StatException the stat exception
	 */
	public double[] getColsInertia() throws StatException {

		return this.r_ca.getColsInertia();
	}

	/**
	 * Gets the cols mass.
	 *
	 * @return the cols mass
	 * @throws StatException the stat exception
	 */
	public double[] getColsMass() throws StatException {

		return this.r_ca.getColsMass();
	}

	/**
	 * Gets the number of columns.
	 * 
	 * @return the number of columns
	 * @throws REXPMismatchException
	 * @throws RWorkspaceException
	 */
	public int getColumnsCount() throws RWorkspaceException, REXPMismatchException {

		return this.r_ca.getColumnsCount();
	}


	/**
	 * Gets the first dimension.
	 * 
	 * @return the first dimension
	 */
	public int getFirstDimension() {

		return firstDimension;
	}

	/**
	 * Gets the lexical table used to compute the CA.
	 *
	 * @return the lexical table
	 */
	public LexicalTable getLexicalTable() {

		return (LexicalTable) this.parent;
	}

	/**
	 * Get the row contributions as array.
	 *
	 * @return the row contrib
	 * @throws StatException
	 */
	public double[][] getRowContrib() throws StatException {

		return r_ca.getRowContrib();
	}

	/**
	 * Get the col contributions as array.
	 *
	 * @return the row contrib
	 * @throws StatException
	 */
	public double[][] getColContrib() throws StatException {

		return r_ca.getColContrib();
	}

	/**
	 * Get the row cos2 as array.
	 *
	 * @return the row cos2
	 * @throws StatException
	 */
	public double[][] getRowCos2() throws StatException {

		return r_ca.getRowCos2();
	}

	/**
	 * Get the col cos2 as array.
	 *
	 * @return the columns cos2 values
	 * @throws StatException
	 */
	public double[][] getColCos2() throws StatException {

		return r_ca.getColCos2();
	}

	public String getRowFilter() {

		return rowfilter;
	}



	/**
	 * Get the row names as array.
	 *
	 * @return the row names
	 * @throws StatException the stat exception
	 */
	public String[] getRowNames() throws Exception {

		if (rownames == null) {
			//rownames = this.getLexicalTable().getRowNames().asStringsArray();
			rownames = r_ca.getRowNames().asStringsArray();
		}
		return rownames;
	}

	/**
	 * Get the rows coordinates as array of arrays
	 *
	 * @return the rows coords
	 */
	public double[][] getRowsCoords() {

		return this.r_ca.getRowsCoords();
	}

	/**
	 * Gets the number of rows.
	 * 
	 * @return the number of rows
	 * @throws REXPMismatchException
	 * @throws RWorkspaceException
	 */
	public int getRowsCount() throws RWorkspaceException, REXPMismatchException {

		return this.r_ca.getRowsCount();
	}

	/**
	 * Get the rows distances as array.
	 *
	 * @return the rows dist
	 * @throws StatException the stat exception
	 */
	public double[] getRowsDist() throws StatException {

		return this.r_ca.getRowDist();
	}

	/**
	 * Get the rows inertia as array.
	 *
	 * @return the rows inertia
	 * @throws StatException the stat exception
	 */
	public double[] getRowsInertia() throws StatException {

		return this.r_ca.getRowsInertia();
	}

	/**
	 * Get the rows mass as array.
	 *
	 * @return the rows mass
	 * @throws StatException the stat exception
	 */
	public double[] getRowsMass() throws StatException {

		return this.r_ca.getRowsMass();
	}

	// /**
	// * step 1.
	// *
	// * @throws StatException the stat exception
	// * @throws CqiClientException the cqi client exception
	// */
	// public boolean stepLexicalTable() throws Exception
	// {
	// table = LexicalTableFactory.getLexicalTable(partition, pAnalysisProperty, fmin);
	// if (table == null) {
	// System.out.println("Error: Lexical table is null.");
	// return false;
	// }
	// partition.addResult(table);
	// table.addResult(this);
	//
	// return true;
	// }

	// /**
	// * step 2.
	// */
	// public boolean stepSortTableLexical()
	// {
	// try {
	// table.getData().filter(vmax, fmin);
	// } catch (Exception e) {
	// System.out.println("Error: "+e.getLocalizedMessage());
	// org.txm.utils.logger.Log.printStackTrace(e);
	// return false;
	// }
	// return true;
	// }

	/**
	 * Gets the second dimension.
	 * 
	 * @return the second dimension
	 */
	public int getSecondDimension() {

		return secondDimension;
	}

	/**
	 * default formatter
	 */
	DecimalFormat defaultDecimalFormat = new DecimalFormat("0.000"); //$NON-NLS-1$
	/**
	 * 
	 * @param info the info to format
	 * @return a DecimalFormatter for each information
	 */
	public DecimalFormat getFormatter(String info) {
		DecimalFormat p = defaultDecimalFormat;
		if (info.startsWith(CACoreMessages.common_mass)) {
			String pattern = CAPreferences.getInstance().getString(CAPreferences.MASS_DISPLAY_FORMAT);
			if (pattern != null && !pattern.trim().equals("")) //$NON-NLS-1$
				p = new DecimalFormat(pattern);
		}
		else if (info.startsWith("chi")) { //$NON-NLS-1$
			String pattern = CAPreferences.getInstance().getString(CAPreferences.CHI_DISPLAY_FORMAT);
			if (pattern != null && !pattern.trim().equals("")) //$NON-NLS-1$
				p = new DecimalFormat(pattern);
		}
		else if (info.startsWith(CACoreMessages.common_dist)) {
			String pattern = CAPreferences.getInstance().getString(CAPreferences.DIST_DISPLAY_FORMAT);
			if (pattern != null && !pattern.trim().equals("")) //$NON-NLS-1$
				p = new DecimalFormat(pattern);
		}
		else if (info.startsWith(CACoreMessages.common_cos2)) {
			String pattern = CAPreferences.getInstance().getString(CAPreferences.COS2_DISPLAY_FORMAT);
			if (pattern != null && !pattern.trim().equals("")) //$NON-NLS-1$
				p = new DecimalFormat(pattern);
		}
		else if (info.startsWith(CACoreMessages.common_cont)) {
			String pattern = CAPreferences.getInstance().getString(CAPreferences.CONTRIB_DISPLAY_FORMAT);
			if (pattern != null && !pattern.trim().equals("")) //$NON-NLS-1$
				p = new DecimalFormat(pattern);
		}
		else if (info.startsWith("Q(")) { //$NON-NLS-1$
			String pattern = CAPreferences.getInstance().getString(CAPreferences.QUALITY_DISPLAY_FORMAT);
			if (pattern != null && !pattern.trim().equals("")) //$NON-NLS-1$
				p = new DecimalFormat(pattern);
		}
		else if (info.startsWith("c(")) //$NON-NLS-1$
		{
			String pattern = CAPreferences.getInstance().getString(CAPreferences.COORD_DISPLAY_FORMAT);
			if (pattern != null && !pattern.trim().equals("")) //$NON-NLS-1$
				p = new DecimalFormat(pattern);
		}
		
		return p;
	}
	
	@Override
	public String getSimpleName() {

		try {
			return this.getParent().getProperty().asString() + " (" + this.firstDimension + "," + this.secondDimension + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		catch (Exception e) {
			return ""; //$NON-NLS-1$
		}
	}

	@Override
	public String getName() {

		return this.getParent().getName() + TXMPreferences.PARENT_NAME_SEPARATOR + " (" + this.firstDimension + "," + this.secondDimension + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ ;
	}


	@Override
	public String getDetails() {

		try {
			return this.getSimpleName() + " " + NLS.bind(TXMCoreMessages.common_fMinEqualsP0, this.getLexicalTable().getFMin()); //$NON-NLS-1$
		}
		catch (Exception e) {
			return ""; //$NON-NLS-1$
		}
	}


	@Override
	public String getComputingStartMessage() {

		// from lexical table
		if (this.parent.isVisible()) {
			return TXMCoreMessages.bind(CACoreMessages.info_caOfTheP0LexcialTable, this.getParent().getName());
		}
		// from partition index
		else if (this.getParent().getParent() instanceof PartitionIndex) {
			return TXMCoreMessages.bind(CACoreMessages.info_caOfTheP0PartitionIndex, this.getParent().getParent().getName());

		}
		// from partition
		else if (this.getParent().getPartition() != null) {
			return TXMCoreMessages.bind(CACoreMessages.info_caOfTheP0PartitionCommaP1Property, this.getParent().getPartition().getName(), this.getParent().getProperty().asString());
		}
		// from sub-corpus lexical table
		// FIXME: SJ: this special case is possible when creating Specificities then calling the command on the invisible Lexical Table
		// need to if we need a special log message here
		else {
			return ""; //$NON-NLS-1$
		}


	}


	/**
	 * Get the singular values as array.
	 *
	 * @return the singular values
	 * @throws StatException the stat exception
	 * @throws REXPMismatchException
	 */
	public double[] getSingularValues() throws StatException, REXPMismatchException {

		return r_ca.getSingularValues();
	}

	/**
	 * Get the singular values infos as List of Lists
	 *
	 * @return the singular values infos
	 * @throws REXPMismatchException
	 */
	public List<List<Object>> getSingularValuesInfos() throws REXPMismatchException {

		List<List<Object>> svtabledata = new ArrayList<>();

		double[] sv = null;
		try {
			sv = getValeursPropres();
		}
		catch (StatException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return svtabledata;
		}

		double sigmaVp;
		try {
			sigmaVp = getValeursPropresSum();
		}
		catch (StatException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return svtabledata;
		}

		double cumul = 0;
		double percent = 0;

		for (int i = 0; i < sv.length; i++) {
			percent = 100 * sv[i] / sigmaVp;
			cumul += percent;

			List<Object> entry = new ArrayList<>(4);
			entry.add(new Integer(i + 1));	// Factor
			entry.add(new Double(sv[i]));	// Singular value
			entry.add(percent);				// Percent
			entry.add(cumul);				// Cumul
			entry.add(percent);				// For bar plot direct drawing in the table

			entry.add(""); // add an empty column to fix a display bug in table in Linux //$NON-NLS-1$

			svtabledata.add(entry);
		}

		return svtabledata;
	}

	@Override
	public String getRSymbol() {

		return r_ca.getSymbol();
	}

	/**
	 * Gets the sqrt of singular values as array.
	 *
	 * @return the valeurs propres
	 * @throws StatException the stat exception
	 * @throws REXPMismatchException
	 */
	public double[] getValeursPropres() throws StatException, REXPMismatchException {

		return r_ca.getEigenvalues();
	}

	/**
	 * Gets the 'valeurs propres' sum.
	 *
	 * @return the valeurs propres sum
	 * @throws StatException the stat exception
	 * @throws REXPMismatchException
	 */
	public double getValeursPropresSum() throws StatException, REXPMismatchException {

		double sum = 0.0d;
		for (double d : getValeursPropres()) {
			sum += d;
		}
		return sum;
	}

	/**
	 * Exports CA R data to a file
	 *
	 * @param outfile the outfile
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @return true, if successful
	 */
	@Override
	// FIXME: to move in an exporter extension
	@Deprecated
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) {

		acquireSemaphore();

		boolean rez = false;
		if (outfile.getName().endsWith(".txt")) { //$NON-NLS-1$
			rez = r_ca.toTxt(outfile, encoding, colseparator, txtseparator);
		}
		else {
			try {
				rez = RResult.exportRdataOrRDS(this, outfile);
			}
			catch (RWorkspaceException e) {
				e.printStackTrace();
				rez = false;
			}
		}

		releaseSemaphore();

		return rez;
	}


	/**
	 * Checks if the CA uses the FactoMineR R package.
	 * 
	 * @return <code>true</code> if the CA uses the FactoMineR R package otherwise <code>false</code>
	 */
	public boolean useFactoMineR() {

		return this.useFactoMineR;
	}

	@Override
	public boolean canCompute() {

		try {
			return this.getLexicalTable().getNColumns() > 2
					&& this.firstDimension > 0
					&& this.secondDimension > 0;
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean saveParameters() {

		if (pColSupNames != null) {
			this.saveParameter(CAPreferences.COL_SUP_NAMES, StringUtils.join(pColSupNames, "\t")); //$NON-NLS-1$
		}
		if (pRowSupNames != null) {
			this.saveParameter(CAPreferences.COL_SUP_NAMES, StringUtils.join(pRowSupNames, "\t")); //$NON-NLS-1$
		}
		
		if (pMirroredDimensions != null) {
			this.saveParameter(CAPreferences.MIRRORED_AXES, StringUtils.join(pMirroredDimensions, "\t")); //$NON-NLS-1$
		}
		
		if (stylingSheet != null) {
			this.saveParameter(CAPreferences.STYLINGSHEET, stylingSheet);
		}
		if (filterStylingSheet != null) {
			try {
				this.saveParameter(CAPreferences.FILTER_STYLINGSHEET, this.getCatalogs().StylingSheetToJSON(filterStylingSheet)); //$NON-NLS-1$
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	@Override
	public boolean loadParameters() {

		String supCols = this.getStringParameterValue(CAPreferences.COL_SUP_NAMES);
		if (supCols != null) {
			if (supCols.length() == 0) {
				this.pColSupNames = new ArrayList<>();
			}
			else {
				this.pColSupNames = Arrays.asList(supCols.split("\t")); //$NON-NLS-1$
			}
		}
		String supRows = this.getStringParameterValue(CAPreferences.ROW_SUP_NAMES);
		if (supRows != null) {
			if (supRows.length() == 0) {
				this.pRowSupNames = new ArrayList<>();
			}
			else {
				this.pRowSupNames = Arrays.asList(supRows.split("\t")); //$NON-NLS-1$
			}
		}
		
		String mirroredAxes = this.getStringParameterValue(CAPreferences.MIRRORED_AXES);
		this.pMirroredDimensions = new ArrayList<>();
		if (mirroredAxes != null) {
			if (mirroredAxes.length() == 0) {
				
			}
			else {
				for (String s : mirroredAxes.split("\t")) //$NON-NLS-1$
				this.pMirroredDimensions.add(Integer.parseInt(s));
			}
		}
		
		String sheetname = this.getStringParameterValue(CAPreferences.STYLINGSHEET);
		if (sheetname != null) {
			stylingSheet = this.getCatalogs().get(sheetname);
		}
		
		String fsheetname = this.getStringParameterValue(CAPreferences.FILTER_STYLINGSHEET); //$NON-NLS-1$
		if (fsheetname != null) {
			try {
				filterStylingSheet = this.getCatalogs().JSONToStylingSheet(fsheetname);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return true;
	}

	/**
	 * @return the showIndividuals
	 */
	public boolean isShowIndividuals() {

		return showIndividuals;
	}

	/**
	 * @param showIndividuals the showIndividuals to set
	 */
	public void setShowIndividuals(boolean showIndividuals) {

		this.showIndividuals = showIndividuals;
	}

	/**
	 * @return the showVariables
	 */
	public boolean isShowVariables() {

		return showVariables;
	}

	/**
	 * @param showVariables the showVariables to set
	 */
	public void setShowVariables(boolean showVariables) {

		this.showVariables = showVariables;
	}

	/**
	 * @return the showPointShapes
	 */
	public boolean isShowPointShapes() {

		return showPointShapes;
	}

	/**
	 * @param showPointShapes the showPointShapes to set
	 */
	public void setShowPointShapes(boolean showPointShapes) {

		this.showPointShapes = showPointShapes;
	}

	/**
	 * @return trus if the chart must show the labels
	 */
	public boolean isShowPointLabels() {

		return showPointLabels;
	}

	/**
	 * @param showPointLabels the showPointLabels to set
	 */
	public void setShowPointLabels(boolean showPointLabels) {
		this.showPointLabels = showPointLabels;
	}

	/**
	 * @param firstDimension the firstDimension to set
	 */
	public void setFirstDimension(int firstDimension) {
		this.firstDimension = firstDimension;
	}

	/**
	 * @param secondDimension the secondDimension to set
	 */
	public void setSecondDimension(int secondDimension) {
		this.secondDimension = secondDimension;
	}

	@Override
	public String getResultType() {
		return CACoreMessages.RESULT_TYPE;
	}


	/**
	 * Gets the unit property.
	 * 
	 * @return the unitProperty
	 */
	public Property getUnitProperty() {
		return this.getLexicalTable().getProperty();
	}


	@Override
	public LexicalTable getParent() {
		if (parent instanceof LexicalTable) return (LexicalTable) this.parent;
		return null;
	}

	/**
	 * 
	 * @return the number of dimensions stored in the R result.
	 */
	public int getNumberOfFactorsMemorized() {
		return pNFactorsMemorized;
	}

	/**
	 * 
	 * @return
	 */
	public int getNumberOfFactorsUpTo80PercentOfRepresentation() {
		try {
			double[] sv = this.getSingularValues();
			double t = 0.0d;
			for (int i = 0; i < sv.length; i++) {
				t += sv[i];
			}
			double t80 = t * 0.8d;
			double r = 0.0d;
			for (int i = 0; i < sv.length; i++) {
				r += sv[i];
				if (r >= t80) {
					return i + 1;
				}
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pNFactorsMemorized;
	}

	public ArrayList<Integer> getMirroredDimensions() {

		return pMirroredDimensions;
	}

	public void setMirroredDimensions(ArrayList<Integer> list) {

		pMirroredDimensions = list;
	}


	@Override
	public CAStylerCatalog getCatalogs() {

		if (caCatalog == null) {
			caCatalog = new CAStylerCatalog();
		}
		return caCatalog;
	}

	/**
	 * 
	 * @return the CA row infos, need to be computed
	 */
	public CARowsInfo getRowsInfo() {

		CARowsInfo infos = this.getFirstChild(CARowsInfo.class);
		if (infos == null) {
			infos = new CARowsInfo(this);
		}
		return infos;
	}

	/**
	 * 
	 * @return the CA row infos, need to be computed
	 */
	public CAColsInfo getColsInfo() {

		CAColsInfo infos = this.getFirstChild(CAColsInfo.class);
		if (infos == null) {
			infos = new CAColsInfo(this);
		}
		return infos;
	}

	/**
	 * 
	 * @return the current styling sheet if any
	 */
	public StylingSheet getStylingSheet() {

		return stylingSheet;
	}

	public void setStylingSheet(StylingSheet stylingSheet2) {

		this.stylingSheet = stylingSheet2;
	}

	public StylingSheet getFilterStylingSheet() {

		if (filterStylingSheet == null) {

			getCatalogs(); // build the caCatalog field

			filterStylingSheet = new CAFilterStylingSheet(this, "Filters"); //$NON-NLS-1$
		}
		return filterStylingSheet;
	}
	//
	//	public StylingSheet getSelectionStylingSheet() {
	//
	//		return selectionStylingSheet;
	//	}

	// // FIXME: SJ: fix to save the eigenvalues when the CA is saved
	// // FIXME: SJ: temporary fix to save the Eigenvalues. This method should be removed when the CAEditor will be managed in a better way
	// @Override
	// public void setUserPersistable(boolean userPersistable) {
	// super.setUserPersistable(userPersistable); // can not call super method, lead to infinite loop
	//
	// // deep persistence
	// for (TXMResult child : getChildren()) {
	// child.setUserPersistable(userPersistable);
	// }
	// }

	HashMap<String, double[]> rowAHCClasses = new HashMap<>();
	HashMap<String, double[]> colAHCClasses = new HashMap<>();
	HashMap<String, Double> rowMinimums = new HashMap<>();
	HashMap<String, Double> colMinimums = new HashMap<>();
	HashMap<String, Double> rowMaximums = new HashMap<>();
	HashMap<String, Double> colMaximums = new HashMap<>();

	/**
	 * 
	 * @param name the data name
	 * @param line the line number
	 * @return the value for the asked line and data
	 * @throws Exception 
	 */
	public Object getData(boolean row, String name) throws Exception {
		if (name.equals(CACoreMessages.common_mass)) {
			if (row) return this.getRowsMass();
			else return this.getColsMass();
		}
		else if (name.equals(CACoreMessages.common_dist)) {
			if (row) return this.getRowsDist();
			else return this.getColsDist();
		}
		else if (name.equals(CAStylerCatalog.ID_CONTX)) {
			double[][] allContribs = null;
			if (row)  allContribs = this.getRowContrib();
			else allContribs = this.getColContrib();
			
			double[] contribs = new double[allContribs.length];
			for (int i = 0 ; i < allContribs.length ; i++)
				contribs[i] = allContribs[i][this.getFirstDimension() - 1];
						
			return contribs;
		}
		else if (name.equals(CAStylerCatalog.ID_CONTY)) {
			double[][] allContribs = null;
			if (row)  allContribs = this.getRowContrib();
			else allContribs = this.getColContrib();
			
			double[] contribs = new double[allContribs.length];
			for (int i = 0 ; i < allContribs.length ; i++)
				contribs[i] = allContribs[i][this.getSecondDimension() - 1];
						
			return contribs;
		}
		else if (name.equals(CAStylerCatalog.ID_CONTXY)) {
			double[][] allData = null;
			if (row)  allData = this.getRowContrib();
			else allData = this.getColContrib();
			
			double[] data = new double[allData.length];
			for (int i = 0 ; i < allData.length ; i++)
				data[i] = Math.max(allData[i][this.getFirstDimension() - 1], allData[i][this.getSecondDimension() - 1]);
						
			return data;
			
		}
		else if (name.equals(CAStylerCatalog.ID_COSX)) {
			
			double[][] allData = null;
			if (row)  allData = this.getRowCos2();
			else allData = this.getColCos2();
			
			double[] data = new double[allData.length];
			for (int i = 0 ; i < allData.length ; i++)
				data[i] = allData[i][this.getFirstDimension() - 1];
						
			return data;
		}
		else if (name.equals(CAStylerCatalog.ID_COSY)) {
			double[][] allData = null;
			if (row)  allData = this.getRowCos2();
			else allData = this.getColCos2();
			
			double[] data = new double[allData.length];
			for (int i = 0 ; i < allData.length ; i++)
				data[i] = allData[i][this.getSecondDimension() - 1];
						
			return data;
		}
		else if (name.equals(TXMCoreMessages.common_rows)) {
			return this.getRowNames();
		}
		else if (name.equals(TXMCoreMessages.common_cols)) {
			return this.getColNames();
		}
		else if (name.startsWith(CACoreMessages.common_cos2)) {
			int dim = Integer.parseInt(name.substring(CACoreMessages.common_cos2.length() +1, name.length() - 1 )); // 'cos2' + '('
			
			double[][] allData = null;
			if (row)  allData = this.getRowCos2();
			else allData = this.getColCos2();
			
			double[] data = new double[allData.length];
			for (int i = 0 ; i < allData.length ; i++)
				data[i] = allData[i][dim - 1];
						
			return data;
		}
		else if (name.startsWith(CACoreMessages.common_cont)) {
			int dim = Integer.parseInt(name.substring(CACoreMessages.common_cont.length() + 1, name.length() - 1));
			
			double[][] allData = null;
			if (row)  allData = this.getRowContrib();
			else allData = this.getColContrib();
			
			double[] data = new double[allData.length];
			for (int i = 0 ; i < allData.length ; i++)
				data[i] = allData[i][dim - 1];
						
			return data;
		}
		else if (name.startsWith("Q(") && name.endsWith(")")) { //$NON-NLS-1$ //$NON-NLS-2$
			double[][] allData = null;
			if (row)  allData = this.getRowCos2();
			else allData = this.getColCos2();
			
			double[] data = new double[allData.length];
			for (int i = 0 ; i < allData.length ; i++)
				data[i] = allData[i][this.getFirstDimension() - 1] +  allData[i][this.getSecondDimension() - 1];
						
			return data;
			
		}
		else if (name.startsWith("c(")) { //$NON-NLS-1$
			
			int dim = Integer.parseInt(name.substring(2, name.length() - 1)); // 'c' + '('
			
			double[][] allData = null;
			if (row)  allData = this.getRowsCoords();
			else allData = this.getColsCoords();
			
			double[] data = new double[allData.length];
			for (int i = 0 ; i < allData.length ; i++)
				data[i] = allData[i][dim - 1];
						
			return data;
		}
		return null;
	}
	
	/**
	 * 
	 * @param name the data name
	 * @param line the line number
	 * @return the value for the asked line and data
	 * @throws Exception 
	 */
	public Object getData(boolean row, String name, int line) throws Exception {

		if (name.equals(CACoreMessages.common_mass)) {
			if (row) return this.getRowsMass()[line];
			else return this.getColsMass()[line];
		}
		else if (name.equals(CACoreMessages.common_dist)) {
			if (row) return this.getRowsDist()[line];
			else return this.getColsDist()[line];
		}
		else if (name.equals(CAStylerCatalog.ID_CONTX)) {
			if (row) return this.getRowContrib()[line][this.getFirstDimension() - 1];
			else return this.getColContrib()[line][this.getFirstDimension() - 1];
		}
		else if (name.equals(CAStylerCatalog.ID_CONTY)) {
			if (row) return this.getRowContrib()[line][this.getSecondDimension() - 1];
			else return this.getColContrib()[line][this.getSecondDimension() - 1];
		}
		else if (name.equals(CAStylerCatalog.ID_CONTXY)) {
			if (row) return Math.max(this.getRowContrib()[line][this.getFirstDimension() - 1], this.getRowContrib()[line][this.getSecondDimension() - 1]);
			else return Math.max(this.getColContrib()[line][this.getFirstDimension() - 1], this.getColContrib()[line][this.getSecondDimension() - 1]);
		}
		else if (name.equals(CAStylerCatalog.ID_COSX)) {
			if (row) return this.getRowCos2()[line][this.getFirstDimension() - 1];
			else return this.getColCos2()[line][this.getFirstDimension() - 1];
		}
		else if (name.equals(CAStylerCatalog.ID_COSY)) {
			if (row) return this.getRowCos2()[line][this.getSecondDimension() - 1];
			else return this.getColCos2()[line][this.getSecondDimension() - 1];
		}
		else if (name.equals(TXMCoreMessages.common_rows)) {
			return this.getRowNames()[line];
		}
		else if (name.equals(TXMCoreMessages.common_cols)) {
			return this.getColNames()[line];
		}
		else if (name.startsWith(CACoreMessages.common_cos2)) {
			int i = Integer.parseInt(name.substring(CACoreMessages.common_cos2.length() +1, name.length() - 1 )); // 'cos2' + '('
			if (row) return this.getRowCos2()[line][i - 1];
			else return this.getColCos2()[line][i - 1];
		}
		else if (name.startsWith(CACoreMessages.common_cont)) {
			int i = Integer.parseInt(name.substring(CACoreMessages.common_cont.length() + 1, name.length() - 1));
			if (row) return this.getRowContrib()[line][i - 1];
			else return this.getColContrib()[line][i - 1];
		}
		else if (name.startsWith("Q(") && name.endsWith(")")) { //$NON-NLS-1$ //$NON-NLS-2$
			double[] cos2 = null;
			if (row) cos2 = this.getRowCos2()[line];
			else  cos2 = this.getColCos2()[line];

			//			System.out.println("Q "+line+": "+Arrays.toString(cos2)+" X="+this.getParent().getFirstDimension()+" Y="+this.getParent().getSecondDimension());
			//			System.out.println("Name: "+this.getNames()[line]);
			return cos2[this.getFirstDimension() - 1] + cos2[this.getSecondDimension() - 1];
		}
		else if (name.startsWith("c(")) { //$NON-NLS-1$
			
			int i = Integer.parseInt(name.substring(2, name.length() - 1)); // 'c' + '(' + number + ')'
			if (row) return this.getRowsCoords()[line][i - 1];
			else return this.getColsCoords()[line][i - 1];
		}
		return null;
	}

	/**
	 * 
	 * @param info the data name
	 * @return the minimum value for the asked data
	 * @throws Exception 
	 */
	public Number getMinData(boolean row, String info) throws Exception {

		HashMap<String, Double> minimums = rowMinimums;
		if (!row) minimums = colMinimums;

		if (!minimums.containsKey(info)) {
			//			System.out.println("BUILD getMinData");
			double minimum = Double.MAX_VALUE;

			int l = this.getRowNames().length;
			if (!row) l = this.getColNames().length;

			for (int i = 0; i < l; i++) {
				Object o = getData(row, info, i);
				if (o instanceof Number n && n.doubleValue() < minimum) {
					minimum = n.doubleValue();
				}
			}
			minimums.put(info, minimum);
		}
		return minimums.get(info);
	}

	/**
	 * 
	 * @param info the data name
	 * @return the minimum value for the asked data
	 * @throws Exception 
	 */
	public Number getSumContForValue(boolean row, int dim, double value) throws Exception {

		double[][] contribs =null;
		if (row) contribs = this.getRowContrib();
		else contribs = this.getColContrib();

		double[] dimcontribs = new double[contribs.length];

		// CONTX OF ROWS
		for (int i = 0 ; i < contribs.length ; i++) {
			dimcontribs[i] = contribs[i][dim];
		}

		Arrays.sort(dimcontribs);

		int i = 1;
		double sum = 0d;
		while (dimcontribs[dimcontribs.length - i] > value && i < dimcontribs.length) {
			sum += dimcontribs[dimcontribs.length - i];
			i++;
		}

		return sum;
	}

	/**
	 * 
	 * @param info the data name
	 * @return the minimum value for the asked data
	 * @throws Exception 
	 */
	public int getAHCClassForValue(boolean row, String info, double value) throws Exception {

		double[] classes10 = getAHCClassesForInfo(row, info);
		if (classes10 == null) return -1;
		
		for (int i = 0 ; i < classes10.length ; i++) {
			if (classes10[i] > value) {
				return i;
			}
		}
		return classes10.length;
	}

	/**
	 * 
	 * @param info the data name
	 * @return the minimum value for the asked data
	 * @throws Exception 
	 */
	public double[] getAHCClassesForInfo(boolean row, String info) throws Exception {

		//System.out.println("getMinData row?"+row+" info="+info);
		HashMap<String, double[]> classes = rowAHCClasses;
		if (!row) classes = colAHCClasses;

		if (!classes.containsKey(info)) {
			//					System.out.println("BUILD getMinData");
			int l = this.getRowNames().length;
			if (!row) l = this.getColNames().length;

			int cut = Math.min(10, l);
			double[] classes10 = new double[cut];
			//					System.out.println("Nclasses="+cut);
			double[] values = new double[l];
			for (int i = 0; i < l; i++) {
				Object o = getData(row, info, i);
				if (o instanceof Number n) {
					values[i] = n.doubleValue();
				}
			}
			Arrays.sort(values);

			RWorkspace.getRWorkspaceInstance().addVectorToWorkspace("tmp", values); //$NON-NLS-1$
			REXP exp = RWorkspace.getRWorkspaceInstance().eval("classes <- cutree(hclust(dist(tmp), method=\"single\"),k="+cut+")"); //$NON-NLS-1$ //$NON-NLS-2$
			int[] affectactions = exp.asIntegers();
			//					System.out.println("Affectations: "+Arrays.toString(affectactions));
			//					System.out.println("Values: "+Arrays.toString(values));
			Arrays.fill(classes10, Double.MAX_VALUE);
			for (int i = 0 ; i < affectactions.length ; i++) {
				if (classes10[affectactions[i] -1] > values[i]) {
					classes10[affectactions[i] -1] = values[i];
				}
			}
			classes.put(info, classes10);
			
			if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
				Log.info(NLS.bind("AHC of {0}: {1}", info, Arrays.toString(classes10) )); //$NON-NLS-1$
			}
		}
		
		return classes.get(info);
	}

	/**
	 * 
	 * @param info the data name
	 * @return the maximum value for the asked data
	 * @throws Exception 
	 */
	public Number getMaxData(boolean row, String info) throws Exception {

		HashMap<String, Double> maximums = rowMaximums;
		if (!row) maximums = colMaximums;

		if (!maximums.containsKey(info)) {
			double maximum = -Double.MAX_VALUE;

			int l = this.getRowNames().length;
			if (!row) l = this.getColNames().length;

			for (int i = 0; i < l; i++) {
				Object o = getData(row, info, i);
				if (o instanceof Number n && n.doubleValue() > maximum) {
					maximum = n.doubleValue();
				}
			}
			maximums.put(info, maximum);
			// System.out.println("NEW MAX\t"+maximum+" FOR\trow="+row+"\tinfo="+info+"\tdims="+this.getFirstDimension()+", "+this.getSecondDimension()+"\tstack="+Arrays.toString(Thread.currentThread().getStackTrace()));
		}
		return maximums.get(info);
	}

}
