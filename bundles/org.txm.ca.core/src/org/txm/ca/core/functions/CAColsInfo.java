package org.txm.ca.core.functions;

import org.eclipse.osgi.util.NLS;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.statsengine.core.StatException;

/**
 * 
 * @author mdecorde
 *
 */
public class CAColsInfo extends CAInfos {

	/**
	 * @param parent
	 */
	public CAColsInfo(CA parent) {
		super(null, parent);
	}

	public CAColsInfo(String parametersNodePath) {
		super(parametersNodePath, null);
	}

	@Override
	public String getRSymbol() {

		return getParent().getRSymbol() + "$col"; //$NON-NLS-1$
	}

	@Override
	public String getInfoName() {

		return TXMCoreMessages.common_cols;
	}

	@Override
	public String getName() {

		return parent.getName();
	}

	@Override
	public String getDetails() {

		if (getParent().hasBeenComputedOnce()) {
			try {
				return NLS.bind(CACoreMessages.P0ColumnInfos, getParent().getColNames().length);
			}
			catch (StatException e) {
				e.printStackTrace();
			}
		}
		return CACoreMessages.theCAColumnInfos;
	}

	@Override
	protected String[] fetchNames() throws StatException {

		CA ca = this.getParent();
		return ca.getColNames();
	}

	@Override
	protected double[][] fetchCoords() throws StatException {

		CA ca = this.getParent();
		return ca.getColsCoords();
	}

	@Override
	protected double[][] fetchContribs() throws StatException {

		CA ca = this.getParent();
		return ca.getColContrib();
	}

	@Override
	protected double[] fetchDists() throws StatException {

		CA ca = this.getParent();
		return ca.getColsDist();
	}

	@Override
	protected double[][] fetchCos2() throws StatException {

		CA ca = this.getParent();
		return ca.getColCos2();
	}

	@Override
	protected double[] fetchMasses() throws Exception {

		CA ca = this.getParent();
		return ca.getColsMass();
	}
}
