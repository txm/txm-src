package org.txm.ca.core.functions;

import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Partition;

// FIXME: SJ: this interface is no more needed. Need to fix code in org.txm.querycooccurrences.rcp then remove it
@Deprecated
public interface ICAComputable {

	public CA toCA() throws Exception;

	public CQPCorpus getCorpus();

	public Partition getPartition();
}
