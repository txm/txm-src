/**
 * 
 */
package org.txm.ca.core.functions;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.lang.StringUtils;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.ca.core.preferences.CAPreferences;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.statsengine.r.core.RResult;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.io.IOUtils;

/**
 * CA informations result. Provides access to the CA informations.
 * 
 * @author mdecorde
 * 
 */
public abstract class CAInfos extends TXMResult implements RResult {

	@Parameter(key = CAPreferences.SHOW_COORDINATES)
	Boolean pShowCoordinates;

	@Parameter(key = CAPreferences.EXPAND_DISPLAYED_AXIS)
	Boolean pExpandDisplayedAxis;

	@Parameter(key = CAPreferences.GROUP_BY_INFORMATIONS_COLUMN)
	Boolean pGroupByInformation;

	private String[] titles;

	private String[] names;

	private double[] mass;

	private double[][] allCoords;

	private double[][] allContribs;

	private double[][] allCos2s;

	private double[] allDists;

	private int nbsv;

	/**
	 * @param parent
	 */
	public CAInfos(CA parent) {
		this(null, parent);
	}

	public CAInfos(String parametersNodePath) {
		this(parametersNodePath, null);
	}

	/**
	 * @param parent
	 */
	public CAInfos(String parametersNodePath, CA parent) {
		super(parametersNodePath, parent);
		this.setVisible(false);
		this.synchronizedWithParent = true;
	}

	/**
	 * Convenience method to get the eigenvalues casted CA parent.
	 * 
	 * @return the eigenvalues CA
	 */
	@Override
	public CA getParent() {
		return (CA) this.parent;
	}

	/**
	 * Convenience method to get the eigenvalues casted CA parent.
	 * 
	 * @return the eigenvalues CA
	 */
	public CA getCA() {
		return (CA) this.parent;
	}

	@Override
	public String getSimpleName() {
		return CACoreMessages.caInfos;
	}

	@Override
	public void clean() {
		// nothing to do all iin the CA
	}

	@Override
	public boolean canCompute() throws Exception {
		return this.parent != null && this.parent.hasBeenComputedOnce();
	}

	@Override
	public boolean saveParameters() {
		return true;
	}

	@Override
	public boolean loadParameters() {
		return true;
	}

	public String[] getTitles() {
		return titles;
	}

	public String[] getNames() {
		return names;
	}

	public double[] getMass() {
		return mass;
	}

	public double[][] getAllCoords() {
		return allCoords;
	}

	public double[][] getAllContribs() {
		return allContribs;
	}

	public double[][] getAllCos2s() {
		return allCos2s;
	}

	public double[] getAllDists() {
		return allDists;
	}

	/**
	 * Cache data for later
	 */
	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		CA ca = this.getParent();
		
		// clear here the CA cache because they are not cleared when the plan selection changed and we need them to be recomputed 
//		ca.rowMinimums.clear();
//		ca.rowMaximums.clear();
//		ca.colMinimums.clear();
//		ca.colMaximums.clear();
//		ca.rowAHCClasses.clear();
//		ca.colAHCClasses.clear();

		names = fetchNames();
		mass = fetchMasses();
		allCoords = fetchCoords();
		allContribs = fetchContribs();
		allCos2s = fetchCos2();
		allDists = fetchDists();
		nbsv = ca.getSingularValues().length;

		titles = updateInfosTitles(); // cached

		return true;
	}

	protected abstract String[] fetchNames() throws Exception;

	protected abstract double[] fetchMasses() throws Exception;

	protected abstract double[][] fetchCoords() throws Exception;

	protected abstract double[][] fetchContribs() throws Exception;

	protected abstract double[][] fetchCos2() throws Exception;

	protected abstract double[] fetchDists() throws Exception;

	@Override
	public String[] getExportTXTExtensions() {
		return new String[] { "*.csv" }; //$NON-NLS-1$
	}

	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {

		PrintWriter writer = IOUtils.getWriter(outfile, encoding);
		ArrayList<String> titles2 = new ArrayList<String>();
		for (String t : titles) {
			titles2.add(t.replace(colseparator, colseparator + colseparator));
		}
		writer.println(StringUtils.join(titles2, colseparator));

		boolean row = TXMCoreMessages.common_rows.equals(getTitles()[0]); // hurk
		CA ca = this.getParent();
		
		StringBuffer line = new StringBuffer();
		for (int i = 0; i < names.length; i++) {
			line.setLength(0);
			for (String col : titles) {
				Object v = ca.getData(row, col, i);
				if (line.length() > 0) line.append(colseparator);

				if (v != null) {
					line.append(v.toString().replace(colseparator, colseparator + colseparator));
				}
			}
			writer.println(line);
		}
		writer.close();
		return outfile.exists();
	}

	@Override
	public String getResultType() {
		return "CA infos"; //$NON-NLS-1$
	}

	/**
	 * Get the infos titles array.
	 *
	 * @return the infos titles
	 */
	public String[] updateInfosTitles() {

		List<String> colnames = new ArrayList<>();

		int d1 = this.getCA().getFirstDimension();
		int d2 = this.getCA().getSecondDimension();
		if (d2 > nbsv) d2 = d1;

		LinkedHashSet<Integer> dims;
		if (pExpandDisplayedAxis) {
			int max = Math.min(this.getParent().getNumberOfFactorsMemorized(), nbsv);

			ArrayList<Integer> list = new ArrayList<Integer>();
			for (int i = 1; i <= max; i++)
				list.add(i);
			Collections.sort(list);
			dims = new LinkedHashSet<>(list); // uniq
		}
		else {
			dims = new LinkedHashSet<>(); // uniq
			dims.add(d1);
			dims.add(d2);

		}

		colnames.add(getInfoName()); // entry
		colnames.add(" "); //$NON-NLS-1$

		if (d1 != d2) {
			colnames.add("Q(" + d1 + "," + d2+")"); // Q12 //$NON-NLS-1$
		}

		colnames.add(CACoreMessages.common_mass); // Mass
		colnames.add(CACoreMessages.common_dist); // Dist
		colnames.add(" "); // separator //$NON-NLS-1$

		if (!pGroupByInformation) {
			for (int d : dims) {
				colnames.add(CACoreMessages.common_cont + "("+d+")"); // ContribN //$NON-NLS-1$
				colnames.add(CACoreMessages.common_cos2 + "("+d+")"); // Cos²N //$NON-NLS-1$

				if (pShowCoordinates) {

					if (d <= this.getCA().getNumberOfFactorsMemorized()) {
						colnames.add("c" + "("+d+")"); // coord axis //$NON-NLS-1$
					}
				}
			}
			colnames.add(""); //$NON-NLS-1$
		}
		else {
			for (int d : dims) {
				colnames.add(CACoreMessages.common_cont + "("+d+")"); // ContribN //$NON-NLS-1$
			}

			for (int d : dims) {
				colnames.add(CACoreMessages.common_cos2 +"("+d+")"); // Cos²N //$NON-NLS-1$
			}

			if (pShowCoordinates) {
				colnames.add(" "); // separator //$NON-NLS-1$
				for (int d : dims) {
					if (d > this.getCA().getNumberOfFactorsMemorized()) break; // avoid NPE
					colnames.add("c" + "("+d+")"); // coord axis //$NON-NLS-1$
				}
			}
			colnames.add(""); //$NON-NLS-1$
		}



		titles = colnames.toArray(new String[] {});
		return titles;
	}

	public abstract String getInfoName();

	public CAInfosLine newCAInfosLine() {
		return new CAInfosLine();
	}

	/**
	 * Stores info for an entry (col or row) of the CA
	 * 
	 * @author mdecorde
	 *
	 */
	public class CAInfosLine {

		public double[] colcontribs;

		public String name;

		public double[] quality;

		public double[] cos2;

		public double mass;

		public double dist;

		public double[] coords;

		@Override
		public String toString() {
			return name + " " + Arrays.asList(quality) + " " + Arrays.asList(cos2) + " " + mass + " " + dist + " " + Arrays.asList(coords); //$NON-NLS-1$
		}

		public CAInfos getCAInfos() {
			return CAInfos.this;
		}

		public CA getCA() {
			return CAInfos.this.getParent();
		}
	}

	public int getNSVD() {

		return nbsv;
	}

	public Object getData(String info, Integer line) throws Exception {
		if (TXMCoreMessages.common_rows.equals(getInfoName())) {
			return this.getParent().getData(true, info, line);
		} else {
			return this.getParent().getData(false, info, line);
		}
	}
}
