/**
 * 
 */
package org.txm.ca.core.functions;

import java.io.File;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.rosuda.REngine.REXPMismatchException;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.core.results.TXMResult;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.r.core.RResult;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.io.IOUtils;

/**
 * Eigenvalues chart result.
 * 
 * @author sjacquot
 *
 */
public class Eigenvalues extends ChartResult implements RResult {

	/**
	 * @param parent
	 */
	public Eigenvalues(TXMResult parent) {
		this(null, parent);
	}

	public Eigenvalues(String parametersNodePath) {
		this(parametersNodePath, null);
	}

	/**
	 * @param parent
	 */
	public Eigenvalues(String parametersNodePath, TXMResult parent) {
		super(parametersNodePath, parent);
		this.setVisible(false);
		this.domainGridLinesVisible = false;
		this.synchronizedWithParent = true;
	}

	/**
	 * Convenience method to get the eigenvalues casted CA parent.
	 * 
	 * @return the eigenvalues CA
	 */
	@Override
	public CA getParent() {
		return (CA) this.parent;
	}

	/**
	 * Convenience method to get the eigenvalues casted CA parent.
	 * 
	 * @return the eigenvalues CA
	 */
	public CA getCA() {
		return (CA) this.parent;
	}

	@Override
	public String getName() {

		return NLS.bind(CACoreMessages.eigenvaluesP0, getParent().getName());
	}

	@Override
	public String getSimpleName() {

		return NLS.bind(CACoreMessages.eigenvaluesP0, this.getCA().getUnitProperty());
	}

	@Override
	public String getDetails() {

		return ""; //$NON-NLS-1$
	}

	@Override
	public String getRSymbol() {
		try {
			return this.getCA().getCA().getEigenvaluesRSymbol();
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void clean() {
		// nothing to do
	}

	@Override
	public boolean canCompute() throws Exception {
		return this.parent != null && this.parent.hasBeenComputedOnce();
	}

	@Override
	public boolean saveParameters() {
		// nothing to do
		return true;
	}

	@Override
	public boolean loadParameters() {
		// nothing to do
		return true;
	}


	@Override
	protected boolean __compute(TXMProgressMonitor monitor) throws Exception {
		// nothing to do
		return true;
	}

	@Override
	@Deprecated
	public String[] getExportTXTExtensions() {
		return new String[] { "*.csv" }; //$NON-NLS-1$
	}

	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		return IOUtils.write(outfile, StringUtils.join(this.getParent().getSingularValues(), "\n")); //$NON-NLS-1$
	}

	@Override
	public String getResultType() {
		return CACoreMessages.eigenvalues;
	}

	public double[] getSingularValues() throws StatException, REXPMismatchException {
		return getParent().getSingularValues();
	}

	public List<List<Object>> getSingularValuesInfos() throws REXPMismatchException {
		return getParent().getSingularValuesInfos();
	}


	// // FIXME: SJ: fix to save the eigenvalues when the CA is saved
	// // FIXME: SJ: temporary fix to save the Eigenvalues. This method should be removed when the CAEditor will be managed in a better way
	// @Override
	// public void setUserPersistable(boolean userPersistable) {
	// this.userPersistable = userPersistable;
	//
	// // directly save and flush the preference
	// try {
	// if (this.isUserPersistable()) {
	// this.autoSaveParametersFromAnnotations();
	// this.saveParameters();
	// TXMPreferences.flush(this);
	// }
	// }
	// catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

}
