package org.txm.ca.core.functions;

import org.eclipse.osgi.util.NLS;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.statsengine.core.StatException;

/**
 * 
 * @author mdecorde
 *
 */
public class CARowsInfo extends CAInfos {

	/**
	 * @param parent
	 */
	public CARowsInfo(CA parent) {

		super(null, parent);
	}

	public CARowsInfo(String parametersNodePath) {

		super(parametersNodePath, null);
	}

	@Override
	public String getInfoName() {

		return TXMCoreMessages.common_rows;
	}

	@Override
	public String getRSymbol() {

		return getParent().getRSymbol() + "$row"; //$NON-NLS-1$
	}

	@Override
	public String getName() {

		return parent.getName();
	}

	@Override
	public String getDetails() {

		if (getParent().hasBeenComputedOnce()) {
			try {
				return NLS.bind(CACoreMessages.P0RowInfos, getParent().getRowNames().length); 
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return CACoreMessages.rowInfos;
	}

	@Override
	protected String[] fetchNames() throws Exception {

		CA ca = this.getParent();
		return ca.getRowNames();
	}

	@Override
	protected double[][] fetchCoords() throws StatException {

		CA ca = this.getParent();
		return ca.getRowsCoords();
	}

	@Override
	protected double[][] fetchContribs() throws StatException {

		CA ca = this.getParent();
		return ca.getRowContrib();
	}

	@Override
	protected double[] fetchDists() throws StatException {

		CA ca = this.getParent();
		return ca.getRowsDist();
	}

	@Override
	protected double[][] fetchCos2() throws StatException {

		CA ca = this.getParent();
		return ca.getRowCos2();
	}

	@Override
	protected double[] fetchMasses() throws Exception {

		CA ca = this.getParent();
		return ca.getRowsMass();
	}
}
