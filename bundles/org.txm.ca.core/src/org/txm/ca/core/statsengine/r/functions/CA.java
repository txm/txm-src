// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2014-06-10 15:10:24 +0200 (Tue, 10 Jun 2014) $
// $LastChangedRevision: 2771 $
// $LastChangedBy: sjacquot $ 
//
package org.txm.ca.core.statsengine.r.functions;

import java.io.File;
import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.rosuda.REngine.REXP;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.lexicaltable.core.statsengine.data.ILexicalTable;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.core.data.Vector;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.data.QuantitativeDataStructureImpl;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

/**
 * Access to the R implementation of the Correspondance Analysis.
 *
 * WARNING: the "CA" package is not shipped in TXM 0.8.X ; this class does not manage changes of the table object ; does not manage empty row or col
 *
 * @author sloiseau, mdecorde, sjacquot
 */
@Deprecated
public class CA implements ICA {

	/** The singular values. */
	private double[] singularValues = null;

	/** The singular values2. */
	private double[] singularValues2 = null;

	/** The rowmass. */
	private double[] rowmass = null;

	/** The rowdist. */
	private double[] rowdist = null;

	/** The rowinertia. */
	private double[] rowinertia = null;

	/** The rowcos2. */
	private double[][] rowcos2 = null;

	/** The rowcontrib. */
	private double[][] rowcontrib = null;

	/** The colmass. */
	private double[] colmass = null;

	/** The coldist. */
	private double[] coldist = null;

	/** The colinertia. */
	private double[] colinertia = null;

	/** The colcos2. */
	private double[][] colcos2 = null;

	/** The colcontrib. */
	private double[][] colcontrib = null;

	/** The rw. */
	private final RWorkspace rw = RWorkspace.getRWorkspaceInstance();

	/** The symbol. */
	private final String symbol = QuantitativeDataStructureImpl.createSymbole(CA.class);

	/** The rowscoords. */
	private double[][] rowscoords;

	/** The colscoords. */
	private double[][] colscoords;

	/**
	 * The lexical table.
	 */
	private ILexicalTable table;

	/**
	 * Create a correspondance analysis on the given table. The correspondance
	 * analysis is computed as soon as the object is created.
	 *
	 * @param table the table
	 * @throws StatException the stat exception
	 */
	public CA(ILexicalTable table) throws StatException {



		loadLibrary();
		this.table = table;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#getColContrib()
	 */
	@Override
	public double[][] getColContrib() throws StatException {

		double[][] colcoords = getColsCoords();
		double[] sv = this.getEigenvalues();
		double[] mass = this.getColsMass();

		colcontrib = new double[colcoords.length][3];

		for (int i = 0; i < colcoords.length; i++) {
			for (int c = 0; c < 3; c++) {
				colcontrib[i][c] = 100.0 * mass[i] * (colcoords[i][c] * colcoords[i][c]) / (sv[c]);
			}
		}
		return colcontrib;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#getColCos2()
	 */
	@Override
	public double[][] getColCos2() {
		if (colcos2 == null) {
			double[][] colcoords = getColsCoords();
			double[] dist = getColDist();
			colcos2 = new double[colcoords.length][3];

			for (int i = 0; i < colcoords.length; i++) {

				double distt = dist[i];

				colcos2[i][0] = Math.pow(colcoords[i][0], 2) / distt;
				colcos2[i][1] = Math.pow(colcoords[i][1], 2) / distt;
				colcos2[i][2] = Math.pow(colcoords[i][2], 2) / distt;
			}
		}
		return colcos2;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#getColDist()
	 */
	@Override
	public double[] getColDist() {
		if (coldist == null) {
			double[][] colcoords = getColsCoords();
			coldist = new double[colcoords.length];
			for (int i = 0; i < colcoords.length; i++)
				for (int c = 0; c < colcoords[i].length; c++)
					coldist[i] += Math.pow(colcoords[i][c], 2);
		}
		return coldist;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#getColsCoords()
	 */
	@Override
	public double[][] getColsCoords() {
		if (colscoords == null) {
			try {
				REXP exp = rw.extractItemFromListByName(symbol, "colcoord"); //$NON-NLS-1$
				colscoords = exp.asDoubleMatrix();

				// multiply coords by sv
				this.getSingularValues();
				for (int i = 0; i < colscoords.length; i++)
					for (int c = 0; c < colscoords[i].length; c++)
						colscoords[i][c] = colscoords[i][c] * singularValues[c];
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return colscoords;
	}

	/**
	 * Get the rows mass.
	 *
	 * @return the cols dist
	 * @throws StatException the stat exception
	 */
	public double[] getColsDist() throws StatException {
		if (coldist == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "coldist"); //$NON-NLS-1$
				coldist = RWorkspace.toDouble(sv);
				// Arrays.sort(coldist);
			}
			catch (RWorkspaceException e) {
				throw new StatException(CACoreMessages.bind(CACoreMessages.error_unableToExtractSingularValuesP0, e.getMessage()), e);
			}
		}
		return coldist;
	}

	/**
	 * Get the rows mass.
	 *
	 * @return the cols inertia
	 * @throws StatException the stat exception
	 */
	@Override
	public double[] getColsInertia() throws StatException {
		if (colinertia == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "colinertia"); //$NON-NLS-1$
				colinertia = RWorkspace.toDouble(sv);
				// Arrays.sort(colinertia);
			}
			catch (RWorkspaceException e) {
				throw new StatException(CACoreMessages.bind(CACoreMessages.error_unableToExtractSingularValuesP0, e.getMessage()), e);
			}
		}
		return colinertia;
	}

	/**
	 * Get the rows mass.
	 *
	 * @return the cols mass
	 * @throws StatException the stat exception
	 */
	@Override
	public double[] getColsMass() throws StatException {
		if (colmass == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "colmass"); //$NON-NLS-1$
				colmass = RWorkspace.toDouble(sv);
				// Arrays.sort(colmass);
			}
			catch (RWorkspaceException e) {
				throw new StatException(CACoreMessages.bind(CACoreMessages.error_unableToExtractSingularValuesP0, e.getMessage()), e);
			}
		}
		return colmass;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#getRowContrib()
	 */
	@Override
	public double[][] getRowContrib() throws StatException {
		if (rowcontrib != null)
			return rowcontrib;

		double[][] rowcoords = getRowsCoords();
		double[] sv = this.getEigenvalues();
		double[] mass = this.getRowsMass();

		rowcontrib = new double[rowcoords.length][3];

		for (int i = 0; i < rowcoords.length; i++) {
			for (int c = 0; c < 3; c++) {
				rowcontrib[i][c] = 100.0 * mass[i] * (rowcoords[i][c] * rowcoords[i][c]) / (sv[c]);
			}
		}
		return rowcontrib;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#getRowCos2()
	 */
	@Override
	public double[][] getRowCos2() {
		if (rowcos2 == null) {
			double[][] rowcoords = getRowsCoords();
			double[] dist = getRowDist();

			rowcos2 = new double[rowcoords.length][3];

			for (int i = 0; i < rowcoords.length; i++) {

				double distt = dist[i];

				rowcos2[i][0] = Math.pow(rowcoords[i][0], 2) / distt;
				rowcos2[i][1] = Math.pow(rowcoords[i][1], 2) / distt;
				rowcos2[i][2] = Math.pow(rowcoords[i][2], 2) / distt;
			}
		}
		return rowcos2;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#getRowDist()
	 */
	@Override
	public double[] getRowDist() {
		if (rowdist == null) {
			double[][] rowcoords = getRowsCoords();
			rowdist = new double[rowcoords.length];
			for (int i = 0; i < rowcoords.length; i++)
				for (int c = 0; c < rowcoords[i].length; c++)
					rowdist[i] += Math.pow(rowcoords[i][c], 2);
		}
		return rowdist;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#getRowsCoords()
	 */
	@Override
	public double[][] getRowsCoords() {
		if (rowscoords == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "rowcoord"); //$NON-NLS-1$
				rowscoords = sv.asDoubleMatrix();

				this.getSingularValues();
				for (int i = 0; i < rowscoords.length; i++)
					for (int c = 0; c < rowscoords[i].length; c++)
						rowscoords[i][c] = rowscoords[i][c] * singularValues[c];
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return rowscoords;
	}

	/**
	 * Get the rows mass.
	 *
	 * @return the rows dist
	 * @throws StatException the stat exception
	 */
	public double[] getRowsDist() throws StatException {
		if (rowdist == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "rowdist"); //$NON-NLS-1$
				rowdist = RWorkspace.toDouble(sv);
				// Arrays.sort(rowdist);
			}
			catch (RWorkspaceException e) {
				throw new StatException(CACoreMessages.bind(CACoreMessages.error_unableToExtractSingularValuesP0, e.getMessage()), e);
			}
		}
		return rowdist;
	}

	/**
	 * Get the rows mass.
	 *
	 * @return the rows inertia
	 * @throws StatException the stat exception
	 */
	@Override
	public double[] getRowsInertia() throws StatException {
		if (rowinertia == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "rowinertia"); //$NON-NLS-1$
				rowinertia = RWorkspace.toDouble(sv);
				// Arrays.sort(rowinertia);
			}
			catch (RWorkspaceException e) {
				throw new StatException(CACoreMessages.bind(CACoreMessages.error_unableToExtractSingularValuesP0, e.getMessage()), e);
			}
		}
		return rowinertia;
	}

	/**
	 * Get the rows mass.
	 *
	 * @return the rows mass
	 * @throws StatException the stat exception
	 */
	@Override
	public double[] getRowsMass() throws StatException {
		if (rowmass == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "rowmass"); //$NON-NLS-1$
				rowmass = RWorkspace.toDouble(sv);
				// Arrays.sort(rowmass);
			}
			catch (RWorkspaceException e) {
				throw new StatException(CACoreMessages.bind(CACoreMessages.error_unableToExtractSingularValuesP0, e.getMessage()), e);
			}
		}
		return rowmass;
	}

	/**
	 * Get the singular values.
	 *
	 * @return the singular values
	 * @throws StatException the stat exception
	 */
	@Override
	public double[] getSingularValues() throws StatException {
		if (singularValues == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "sv"); //$NON-NLS-1$
				singularValues = RWorkspace.toDouble(sv);
				// Arrays.sort(singularValues);
			}
			catch (RWorkspaceException e) {
				throw new StatException(CACoreMessages.bind(CACoreMessages.error_unableToExtractSingularValuesP0, e.getMessage()), e);
			}
		}
		return singularValues;
	}

	/**
	 * Gets the valeurs propres.
	 *
	 * @return the singular valeurs pow 2
	 * @throws StatException the stat exception
	 */
	@Override
	public double[] getEigenvalues() throws StatException {
		if (singularValues2 == null) {
			getSingularValues();
			singularValues2 = new double[singularValues.length];
			for (int i = 0; i < singularValues.length; i++)
				singularValues2[i] = Math.pow(singularValues[i], 2);
		}
		return singularValues2;
	}

	@Override
	public String getEigenvaluesRSymbol() throws StatException {
		return symbol + "$sv"; //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#getSymbol()
	 */
	@Override
	public String getSymbol() {
		return symbol;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#loadLibrary()
	 */
	@Override
	public boolean loadLibrary() {
		try {
			rw.voidEval("library(ca)"); //$NON-NLS-1$
		}
		catch (RWorkspaceException e) {
			System.out.println(NLS.bind(CACoreMessages.error_canNotLoadCALibraryP0, e));
			return false;
		}
		return true;
	}



	/**
	 * Raw export of the R data
	 *
	 * @param file the file
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @return true, if successful
	 */
	@Override
	public boolean toTxt(File file, String encoding, String colseparator, String txtseparator) {
		try {
			RWorkspace rw = RWorkspace.getRWorkspaceInstance();
			rw.eval("sink(file=\"" + file.getAbsolutePath().replace("\\", "\\\\") + "\")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			rw.eval("print(" + symbol + ")"); //$NON-NLS-1$ //$NON-NLS-2$
			rw.eval("sink()"); //$NON-NLS-1$
		}
		catch (StatException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	@Override
	public void compute(List<String> pRowSupNames, List<String> pColSupNames, int nCoordsSaved, List<Integer> mirroredDimensions, boolean doPCA) throws StatException {

		if (!rw.containsVariable(table.getSymbol())) {
			throw new StatException(CACoreMessages.error_lexicalTableNotFoundInWorkspace);
		}
		try {
			if (doPCA) {
				rw.callFunctionAndAffect("pca", new String[] { table.getSymbol() }, symbol); //$NON-NLS-1$
			} else {
				rw.callFunctionAndAffect("ca", new String[] { table.getSymbol() }, symbol); //$NON-NLS-1$
			}

			if (mirroredDimensions != null) {
				for (int d : mirroredDimensions) {
					rw.voidEval(symbol + "$colcoord[," + d + "] = -" + symbol + "$colcoord[," + d + "]"); // ca$row$coord[,1] = -ca$row$coord[,1] $NON-NLS-1$
					rw.voidEval(symbol + "$rowcoord[," + d + "] = -" + symbol + "$rowcoord[," + d + "]"); // $NON-NLS-1$
				}
			}
		}
		catch (RWorkspaceException e) {
			throw new StatException(NLS.bind(CACoreMessages.error_errorWhileComputingCAP0, e.getMessage()), e);
		}
	}

	@Override
	public int getRowsCount() {
		return this.table.getRowsCount();
	}

	@Override
	public int getColumnsCount() {
		return this.table.getColumnsCount();
	}

	@Override
	public Vector getColNames() throws RWorkspaceException {
		return table.getColNames();
	}

	@Override
	public Vector getRowNames() throws RWorkspaceException {
		return table.getRowNames();
	}
}
