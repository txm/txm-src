// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.ca.core.statsengine.r.functions;

import java.io.File;
import java.util.List;

import org.rosuda.REngine.REXPMismatchException;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.core.data.Vector;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

/**
 * The Interface ICA. The CA command needs a R implementation of CA.
 */
public interface ICA {

	public void compute(List<String> pRowSupNames, List<String> pColSupNames, int nFactorsMemorized, List<Integer> pMirroredDimensions, boolean doPCA) throws StatException;

	/**
	 * Gets the col contrib.
	 *
	 * @return the col contrib
	 * @throws StatException the stat exception
	 */
	public double[][] getColContrib() throws StatException;

	/**
	 * Gets the col cos2.
	 *
	 * @return the col cos2
	 * @throws StatException the stat exception
	 */
	public double[][] getColCos2() throws StatException;

	/**
	 * Gets the col dist.
	 *
	 * @return the col dist
	 * @throws StatException the stat exception
	 */
	public double[] getColDist() throws StatException;

	/**
	 * Gets the cols coords.
	 *
	 * @return the cols coords
	 */
	public double[][] getColsCoords();

	/**
	 * Get the rows mass.
	 *
	 * @return the cols inertia
	 * @throws StatException the stat exception
	 */
	public double[] getColsInertia() throws StatException;

	/**
	 * Get the rows mass.
	 *
	 * @return the cols mass
	 * @throws StatException the stat exception
	 */
	public double[] getColsMass() throws StatException;

	/**
	 * Gets the row contrib.
	 *
	 * @return the row contrib
	 * @throws StatException the stat exception
	 */
	public double[][] getRowContrib() throws StatException;

	/**
	 * Gets the row cos2.
	 *
	 * @return the row cos2
	 * @throws StatException the stat exception
	 */
	public double[][] getRowCos2() throws StatException;

	/**
	 * Gets the row dist.
	 *
	 * @return the row dist
	 * @throws StatException the stat exception
	 */
	public double[] getRowDist() throws StatException;

	/**
	 * Gets the rows coords.
	 *
	 * @return the rows coords
	 */
	public double[][] getRowsCoords();

	/**
	 * Get the rows mass.
	 *
	 * @return the rows inertia
	 * @throws StatException the stat exception
	 */
	public double[] getRowsInertia() throws StatException;

	/**
	 * Get the rows mass.
	 *
	 * @return the rows mass
	 * @throws StatException the stat exception
	 */
	public double[] getRowsMass() throws StatException;

	/**
	 * Get the singular values.
	 *
	 * @return the singular values
	 * @throws StatException the stat exception
	 * @throws REXPMismatchException
	 */
	public double[] getSingularValues() throws StatException, REXPMismatchException;


	/**
	 * Gets the number of rows.
	 * 
	 * @return the number of rows
	 * @throws REXPMismatchException
	 * @throws RWorkspaceException
	 */
	public int getRowsCount() throws RWorkspaceException, REXPMismatchException;


	/**
	 * Gets the number of columns.
	 * 
	 * @return the number of columns
	 */
	public int getColumnsCount() throws RWorkspaceException, REXPMismatchException;


	/**
	 * Gets the symbol.
	 *
	 * @return the symbol
	 */
	public String getSymbol();

	/**
	 * Gets the valeurs propres.
	 *
	 * @return the valeurs propres
	 * @throws StatException the stat exception
	 * @throws REXPMismatchException
	 */
	public double[] getEigenvalues() throws StatException, REXPMismatchException;

	/**
	 * Gets the valeurs propres.
	 *
	 * @return the valeurs propres
	 * @throws StatException the stat exception
	 */
	public String getEigenvaluesRSymbol() throws StatException;

	/**
	 * Load library.
	 *
	 * @return true, if successful
	 */
	public boolean loadLibrary();

	/**
	 * Raw export of the CA data.
	 *
	 * @param file the file
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @return true, if successful
	 */
	public boolean toTxt(File file, String encoding, String colseparator, String txtseparator);

	public Vector getColNames() throws RWorkspaceException;

	public Vector getRowNames() throws RWorkspaceException;
}
