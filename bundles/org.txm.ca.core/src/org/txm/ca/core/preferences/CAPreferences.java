package org.txm.ca.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Default preferences initializer.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class CAPreferences extends ChartsEnginePreferences {

	/** The Constant QUALITYDISPLAY. */
	public static final String QUALITY_DISPLAY_FORMAT = "quality_display_format"; //$NON-NLS-1$

	/** The Constant CONTRIBDISPLAY. */
	public static final String CONTRIB_DISPLAY_FORMAT = "contrib_display_format"; //$NON-NLS-1$

	/** The Constant MASSDISPLAY. */
	public static final String MASS_DISPLAY_FORMAT = "mass_display_format"; //$NON-NLS-1$

	public static final String CHI_DISPLAY_FORMAT = "chi_display_format"; //$NON-NLS-1$

	/** The Constant DISTDISPLAY. */
	public static final String DIST_DISPLAY_FORMAT = "dist_display_format"; //$NON-NLS-1$

	/** The Constant COS2DISPLAY. */
	public static final String COS2_DISPLAY_FORMAT = "cos2_display_format"; //$NON-NLS-1$

	/** The Constant COORDDISPLAY. */
	public static final String COORD_DISPLAY_FORMAT = "coord_display_format"; //$NON-NLS-1$

	/**
	 * activate default filter and styling if set to TRUE
	 */
	public static final String USER_INTERPRETATION_HELP = "interpretation_help"; //$NON-NLS-1$
	
	public static final String FILTER_INFOS_SUM = "filter_infos_sum"; //$NON-NLS-1$
	public static final String FILTER_INFOS_N_FILTERED = "filter_infos_n_filtered"; //$NON-NLS-1$
	public static final String FILTER_INFOS_AHC_CLASSES = "filter_infos_ahc_classes"; //$NON-NLS-1$
	
	public static final String OPEN_INFOS = "open_infos"; //$NON-NLS-1$
	
	public static final String OPEN_INFOS_IN_BOX = "open_infos_inbox"; //$NON-NLS-1$

	public static final String FIRST_DIMENSION = "first_dimension"; //$NON-NLS-1$

	public static final String SECOND_DIMENSION = "second_dimension"; //$NON-NLS-1$
	
	public static final String MIRRORED_AXES = "mirrored_dimensions"; //$NON-NLS-1$

	public static final String SHOW_INDIVIDUALS = "chart_show_individuals"; //$NON-NLS-1$

	public static final String SHOW_VARIABLES = "chart_show_variables"; //$NON-NLS-1$

	public static final String SHOW_POINT_SHAPES = "chart_show_point_shapes"; //$NON-NLS-1$
	
	public static final String SHOW_POINT_LABELS = "chart_show_point_labels"; //$NON-NLS-1$

	public static final String ROW_SUP_NAMES = "row_sup_names"; //$NON-NLS-1$

	public static final String COL_SUP_NAMES = "col_sup_names"; //$NON-NLS-1$

	public static final String N_FACTORS_MEMORIZED = "n_factors_memorized"; //$NON-NLS-1$

	public static final String EXPAND_DISPLAYED_AXIS = "expand_displayed_axis"; //$NON-NLS-1$

	public static final String SHOW_COORDINATES = "show_coordinates"; //$NON-NLS-1$

	public static final String GROUP_BY_INFORMATIONS_COLUMN = "group_by_column_informations"; //$NON-NLS-1$

	public static final String STYLINGSHEET = "stylingsheet"; //$NON-NLS-1$
	
	/**
	 * if set, try to find a matching styling sheet name. If not set, use the Catalog default styling sheet
	 */
	public static final String DEFAULT_STYLINGSHEET = "new_stylingsheet"; //$NON-NLS-1$
	
	public static final String FILTER_STYLINGSHEET = "filter_stylingsheet"; //$NON-NLS-1$

	public static final String FILTER_DEFAULT_QUALITY_MODE = "default_quality_mode";

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(CAPreferences.class)) {
			new CAPreferences();
		}
		return TXMPreferences.instances.get(CAPreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.putBoolean(USER_INTERPRETATION_HELP, true);
		preferences.putBoolean(OPEN_INFOS, true);
		preferences.putBoolean(OPEN_INFOS_IN_BOX, false);
		preferences.putBoolean(FILTER_INFOS_SUM, true);
		preferences.putBoolean(FILTER_INFOS_N_FILTERED, true);
		preferences.putBoolean(FILTER_INFOS_AHC_CLASSES, false);
		preferences.put(FILTER_DEFAULT_QUALITY_MODE, "80%");
		preferences.put(DEFAULT_STYLINGSHEET, "");
		
		preferences.putInt(F_MIN, 10);
		preferences.putInt(V_MAX, 200);

		String defaultFormat = "0.00"; //$NON-NLS-1$
		preferences.put(QUALITY_DISPLAY_FORMAT, defaultFormat);
		preferences.put(CONTRIB_DISPLAY_FORMAT, defaultFormat);
		preferences.put(MASS_DISPLAY_FORMAT, defaultFormat);
		preferences.put(CHI_DISPLAY_FORMAT, defaultFormat);
		preferences.put(DIST_DISPLAY_FORMAT, defaultFormat);
		preferences.put(COS2_DISPLAY_FORMAT, defaultFormat);
		preferences.put(COORD_DISPLAY_FORMAT, defaultFormat);

		preferences.putBoolean(SHOW_INDIVIDUALS, true);
		preferences.putBoolean(SHOW_VARIABLES, true);
		preferences.putBoolean(SHOW_POINT_SHAPES, false);
		preferences.putBoolean(SHOW_POINT_LABELS, true);

		preferences.putBoolean(EXPAND_DISPLAYED_AXIS, false);
		preferences.putBoolean(SHOW_COORDINATES, false);
		preferences.putBoolean(GROUP_BY_INFORMATIONS_COLUMN, false);

		preferences.putInt(FIRST_DIMENSION, 1);
		preferences.putInt(SECOND_DIMENSION, 2);

		preferences.putInt(N_FACTORS_MEMORIZED, 10);

		// disable unavailable functionality
		TXMPreferences.setEmpty(this.getPreferencesNodeQualifier(), ChartsEnginePreferences.RENDERING_COLORS_MODE);
	}
}
