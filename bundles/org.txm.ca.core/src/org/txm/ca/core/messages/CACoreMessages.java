package org.txm.ca.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * CA core messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class CACoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.ca.core.messages.messages"; //$NON-NLS-1$

	public static String RESULT_TYPE;

	public static String eigenvalues;

	public static String eigenvaluesP0;

	public static String canBeTestedWith;

	public static String canChangedWith;

	public static String cantCreateCAChartFileChartsEngineColonP0P1;

	public static String cantCreateBarChartOfTheCASingularValuesP0;

	public static String anImportedMetadatum;

	public static String axis;

	public static String axisP0P1Percent;

	public static String CantCreateCAFactorialMapScatterPlotP0;

	public static String columns;

	public static String columnsAndLines;

	public static String columnsFilterMaformedFollowThisPatternP0;

	public static String rowsFilterMalformedFollowThisPatternP0;

	public static String correspondenceAnalysisFactorialPlaneOfP0;

	public static String t;

	public static String coordinates;

	public static String failedToComputeCAP0;

	public static String frequency;

	public static String percent;

	public static String property;

	public static String charts_singularValues_tooltipSum;

	public static String chevNoStyleChev;

	public static String eigenvalue;

	public static String info_caOfTheP0PartitionCommaP1Property;

	public static String info_caOfTheP0LexcialTable;

	public static String info_caOfTheP0PartitionIndex;

	public static String instructions;


	public static String common_c1;

	public static String common_c2;

	public static String common_c3;

	public static String common_cont;

	public static String common_cos2;

	public static String common_dist;

	public static String common_mass;

	public static String common_q12;

	public static String common_q13;

	public static String common_q23;

	public static String contX;

	public static String contXY;

	public static String contY;

	public static String cos2X;

	public static String cos2Y;

	public static String distance;

	public static String error_canNotLoadCALibraryP0;

	public static String error_errorWhileComputingCAP0;

	public static String error_failedToGetConstributionRowP0;

	public static String error_failedToGetContributionColumnP0;

	public static String error_failedToGetCos2ColumnP0;

	public static String error_failedToGetCos2RowP0;

	public static String error_lexicalTableNotFoundInWorkspace;

	public static String error_unableToExtractSingularValuesP0;

	public static String errorEmptyLexicalTableComputingAborted;

	public static String errorInColorFormatForRGBOrRGBAStringP0;

	public static String errorP0;

	public static String errorTheFirstColumnNameMustBeLabelPatternP0;

	public static String label;

	public static String rows;

	public static String mass;

	public static String metadadum;

	public static String object;

	public static String qXY;

	public static String TabAndParameter;

	public static String unknownStyleColumnsP0;

	public static String theCAColumnInfos;

	public static String P0ColumnInfos;

	public static String caInfos;

	public static String rowInfos;
	
	public static String P0RowInfos;

	public static String rowsAndColumns;

	public static String quality; // quality
	public static String contribution; // contribution
	public static String cos2; // cos²

	public static String pointShapes;
	public static String pointLabels;

	public static String columnPoints;
	public static String rowsPoints;

	static {
		Utf8NLS.initializeMessages(BUNDLE_NAME, CACoreMessages.class);
	}
}
