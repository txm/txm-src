package org.kohsuke.args4j.spi;

import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionDef;

/**
 * {@link Object}
 * {@link OptionHandler}
 * {@link OneArgumentOptionHandler}
 * 
 * @author Jan Materne
 * @since 2.0.9
 */
public class ObjectOptionHandler extends OneArgumentOptionHandler<Object> {
	
	public ObjectOptionHandler(CmdLineParser parser, OptionDef option, Setter<? super Object> setter) {
		super(parser, option, setter);
	}
	
	@Override
	protected Object parse(String argument) throws NumberFormatException {
		return argument;
	}
	
}
