package org.kohsuke.args4j.spi;

/**
 * The ConfigElement is an <code>&lt;option></code> or <code>&lt;argument></code> tag in the XML configuration file.
 * 
 * @author Jan Materne
 */
public class ConfigElement {
	
	public String field;
	
	public String method;
	
	public String name;
	
	public String usage = ""; //$NON-NLS-1$
	
	public String handler;
	
	public String metavar = ""; //$NON-NLS-1$
	
	public String[] aliases = {};
	
	public boolean multiValued = false;
	
	public boolean required = false;
	
	/**
	 * Ensures that only a field XOR a method is set.
	 * 
	 * @return
	 */
	public boolean isInvalid() {
		return field == null && method == null || field != null && method != null;
	}
}
