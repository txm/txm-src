package org.kohsuke.args4j.spi;

import java.lang.reflect.Field;

/**
 * {@link Setter} that sets to a {@link Field}.
 *
 * @author Kohsuke Kawaguchi
 */
@SuppressWarnings("rawtypes")
final class FieldSetter implements Setter {
	
	private final Field f;
	
	private final Object bean;
	
	public FieldSetter(Object bean, Field f) {
		this.bean = bean;
		this.f = f;
	}
	
	@Override
	public Class getType() {
		return f.getType();
	}
	
	@Override
	public boolean isMultiValued() {
		return false;
	}
	
	@Override
	public void addValue(Object value) {
		try {
			f.set(bean, value);
		}
		catch (IllegalAccessException ex) {
			// try again
			f.setAccessible(true);
			try {
				f.set(bean, value);
			}
			catch (IllegalAccessException e) {
				throw new IllegalAccessError(e.getMessage());
			}
		}
	}
}
