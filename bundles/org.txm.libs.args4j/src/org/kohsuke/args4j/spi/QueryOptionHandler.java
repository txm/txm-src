package org.kohsuke.args4j.spi;

import java.util.Date;

import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionDef;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;

/**
 * {@link Date}
 * {@link OptionHandler}.
 * {@link OneArgumentOptionHandler}
 * 
 * @author mdecorde
 */
public class QueryOptionHandler extends OneArgumentOptionHandler<CQLQuery> {
	
	public QueryOptionHandler(CmdLineParser parser, OptionDef option, Setter<? super CQLQuery> setter) {
		super(parser, option, setter);
	}
	
	@Override
	protected CQLQuery parse(String argument) throws NumberFormatException {
		return new CQLQuery(argument);
	}
}
