package org.kohsuke.args4j.spi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionDef;

/**
 * {@link Date}
 * {@link OptionHandler}.
 * {@link OneArgumentOptionHandler}
 * 
 * @author Kohsuke Kawaguchi
 */
public class DateOptionHandler extends OneArgumentOptionHandler<Date> {
	
	public static SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd"); //$NON-NLS-1$
	
	public DateOptionHandler(CmdLineParser parser, OptionDef option, Setter<? super Date> setter) {
		super(parser, option, setter);
	}
	
	@Override
	protected Date parse(String argument) throws NumberFormatException {
		try {
			return formater.parse(argument);
		}
		catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new Date();
		}
	}
}
