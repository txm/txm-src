package org.kohsuke.args4j;

/**
 * Run-time copy of the Option or Argument annotation.
 */
public final class NamedOptionDef extends OptionDef {
	
	private String name;
	
	public String def;
	
	private String widget;
	
	private String[] aliases;
	
	public NamedOptionDef(Option o, boolean forceMultiValued) {
		super(o.usage(), o.metaVar(), o.required(), o.handler(), o.multiValued() || forceMultiValued);
		
		this.name = o.name();
		this.def = o.def();
		this.widget = o.widget();
		this.aliases = o.aliases();
	}
	
	public String name() {
		return name;
	}
	
	public String def() {
		return def;
	}
	
	public String widget() {
		return widget;
	}
	
	public String[] aliases() {
		return aliases;
	}
	
	@Override
	public String toString() {
		if (aliases.length > 0) {
			String str = ""; //$NON-NLS-1$
			for (String alias : aliases) {
				if (str.length() > 0) {
					str += ", "; //$NON-NLS-1$
				}
				str += alias;
			}
			return name() + " widget=" + widget() + " (" + str + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		return name();
	}
	
	@Override
	public boolean isArgument() {
		return false;
	}
}
