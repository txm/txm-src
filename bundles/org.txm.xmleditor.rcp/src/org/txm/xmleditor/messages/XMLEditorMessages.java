package org.txm.xmleditor.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

public class XMLEditorMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.xmleditor.messages.messages"; //$NON-NLS-1$

	public static String AbortingTheOpeningOfP0;

	public static String BigFile;

	public static String OpenXMLEditor_1;

	public static String OpenXMLEditor_3;

	public static String errorPO;

	public static String P0IsAVeryBigFileDoYouWantToContinueAndOpensIt;

	public static String SelectedWordFromP0ToP1;

	public static String SelectedWordP0;

	public static String WordSelectionIdentifiersP0;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, XMLEditorMessages.class);
	}

	private XMLEditorMessages() {
	}
}
