package org.txm.xmleditor;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.eclipse.wst.sse.ui.StructuredTextEditor;
import org.eclipse.wst.xml.ui.internal.tabletree.XMLMultiPageEditorPart;
import org.txm.Toolbox;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.core.functions.Line;
import org.txm.edition.rcp.editors.SynopticEditionEditor;
import org.txm.objects.Text;
import org.txm.rcp.StatusLine;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.utils.logger.Log;
import org.txm.xmleditor.messages.XMLEditorMessages;

public class OpenXMLEditor extends AbstractHandler {

	public static final String ID = "org.txm.xmleditor.OpenXMLEditor"; //$NON-NLS-1$

	public static String lastopenedfile;

	public int ALERT = 1000000;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		IEditorPart editor = HandlerUtil.getActiveWorkbenchWindowChecked(event).getActivePage().getActiveEditor();
		ISelection iselection = HandlerUtil.getCurrentSelection(event);
		Object selection = null;
		if (iselection instanceof IStructuredSelection) {
			selection = ((IStructuredSelection) iselection).getFirstElement();
		}

		String txmhome = Toolbox.getTxmHomePath();
		String filename = event.getParameter("org.txm.rcp.commands.commandParameter2"); //$NON-NLS-1$
		IEditorPart reditor = null;
		if (editor instanceof SynopticEditionEditor) {
			Text text = ((SynopticEditionEditor) editor).getEditionPanel(0).getEdition().getText();
			String[] wordSelection = ((SynopticEditionEditor) editor).getEditionPanel(0).getWordSelection();

			File txmFile = text.getXMLTXMFile();
			if (txmFile != null && txmFile.exists()) {
				if (txmFile.length() > ALERT) {
					if (!MessageDialog.openQuestion(shell, XMLEditorMessages.BigFile,
							XMLEditorMessages.bind(XMLEditorMessages.P0IsAVeryBigFileDoYouWantToContinueAndOpensIt, txmFile.getAbsolutePath()))) {
						Log.info(XMLEditorMessages.bind(XMLEditorMessages.AbortingTheOpeningOfP0, txmFile));
						return null;
					}
				}
				reditor = openfile(txmFile);
			}
			else { // wrong link ?
				txmFile = new File(text.getProject().getProjectDirectory(), "txm/" + text.getProject().getName() + "/" + text.getName() + ".xml"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				reditor = openfile(txmFile);
			}

			if (wordSelection != null && wordSelection[0] != null && reditor instanceof XMLMultiPageEditorPart) {
				XMLMultiPageEditorPart xmlEditor = (XMLMultiPageEditorPart) reditor;
				// System.out.println("xmlEditor=" + xmlEditor);
				// System.out.println("word=" + wordSelection[0]);
				Object selectedPage = xmlEditor.getSelectedPage();
				if (selectedPage != null && selectedPage instanceof StructuredTextEditor) {
					// StructuredTextEditor textEditor = (StructuredTextEditor) selectedPage;
					//
					// String search = "\""+wordSelection[0]+"\"";
					if (wordSelection[1] == null) {
						System.out.println(XMLEditorMessages.bind(XMLEditorMessages.SelectedWordP0, wordSelection[0]));
					}
					else {
						System.out.println(XMLEditorMessages.bind(XMLEditorMessages.SelectedWordFromP0ToP1, wordSelection[0], wordSelection[1])); //$NON-NLS-2$
					}
				}
			}
		}
		else if (selection != null && selection instanceof Line) {
			Line line = (Line) selection;
			Concordance conc = line.getConcordance();
			String text = line.getTextId();

			File txmFile = new File(conc.getProject().getProjectDirectory(), "txm/" + conc.getCorpus().getID() + "/" + text + ".xml"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			reditor = openfile(txmFile);
			if (reditor != null & reditor instanceof XMLMultiPageEditorPart) {
				XMLMultiPageEditorPart xmlEditor = (XMLMultiPageEditorPart) reditor;
				Object selectedPage = xmlEditor.getSelectedPage();
				if (selectedPage != null && selectedPage instanceof StructuredTextEditor) {
					System.out.println(XMLEditorMessages.bind(XMLEditorMessages.WordSelectionIdentifiersP0, line.getKeywordIds()));
				}
			}
		}
		else if (filename != null && !filename.matches("") && !filename.matches(" ")) { //$NON-NLS-1$ //$NON-NLS-2$
			String filepath = txmhome + "/scripts/" + filename; //$NON-NLS-1$
			return openfile(new File(filepath));
		}

		if (reditor != null) return null;


		File file = null;
		if (iselection != null & iselection instanceof IStructuredSelection) {
			for (Object o : ((IStructuredSelection) iselection).toList()) {
				if (o != null && o instanceof File) {
					file = (File) o;
					break;
				}
			}
		}

		if (file == null) {

			FileDialog dialog = new FileDialog(shell, SWT.OPEN);
			if (LastOpened.getFile(ID) != null) {
				dialog.setFilterPath(LastOpened.getFolder(ID));
				dialog.setFileName(LastOpened.getFile(ID));
			}
			else {
				dialog.setFilterPath(txmhome);
			}
			if (dialog.open() == null) return null;
			String filepath = dialog.getFilterPath()
					+ "/" + dialog.getFileName(); //$NON-NLS-1$
			lastopenedfile = filepath;
			LastOpened.set(ID, dialog.getFilterPath(), dialog.getFileName());
			openfile(new File(filepath));
		}
		else {
			if (file.isDirectory()) {
				FileDialog dialog = new FileDialog(shell, SWT.OPEN);
				dialog.setFilterPath(file.getAbsolutePath());
				dialog.open();
				String filepath = dialog.getFilterPath()
						+ "/" + dialog.getFileName(); //$NON-NLS-1$
				openfile(new File(filepath));
			}
			else {
				openfile(file);
			}
		}

		return null;
	}

	private IEditorPart openfile(File file) {
		// XMLMultiPageEditorPart editorInput = new ConcordancesEditorInput(corpus, null);
		Log.fine(XMLEditorMessages.OpenXMLEditor_1);
		IFileStore fileOnLocalDisk = EFS.getLocalFileSystem().getStore(
				file.toURI());

		FileStoreEditorInput editorInput = new FileStoreEditorInput(fileOnLocalDisk);
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		IWorkbenchPage page = window.getActivePage();

		String ID = "org.eclipse.wst.xml.ui.internal.tabletree.XMLMultiPageEditorPart"; //$NON-NLS-1$
		try {
			StatusLine.setMessage(XMLEditorMessages.OpenXMLEditor_3);
			IEditorPart editor = page.openEditor(editorInput, ID); // $NON-NLS-1$
			if (editor != null && editor instanceof XMLMultiPageEditorPart) {
				XMLMultiPageEditorPart xmlEditor = ((XMLMultiPageEditorPart) editor);
				xmlEditor.removePage(0);
				return xmlEditor;
			}

			return editor;
		}
		catch (PartInitException e) {
			System.out.println(XMLEditorMessages.bind(XMLEditorMessages.errorPO, e));
		}
		return null;
	}
}
