package org.txm.ruby.core;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleBindings;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.txm.core.engines.ScriptEngine;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

/**
 * 
 * <pre>
 * require 'java'

puts 'Hello World!' + org.txm.Toolbox.workspace.getProjects().toString()
 * </pre>
 * 
 * 
 * @author mdecorde
 *
 */
public class RubyScriptEngine extends ScriptEngine {

	private javax.script.ScriptEngine engine;

	public RubyScriptEngine() {
		super("ruby", "rb"); //$NON-NLS-1$
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	public boolean initialize() throws Exception {
		ScriptEngineManager manager = new ScriptEngineManager();
		engine = manager.getEngineByName("jruby"); //$NON-NLS-1$
		return engine != null;
	}

	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		return true;
	}

	@Override
	public boolean stop() throws Exception {
		engine = null;
		return true;
	}

	@Override
	public IStatus executeScript(File script, HashMap<String, Object> env, List<String> stringArgs) {
		BufferedReader r = null;
		try {
			SimpleBindings bindings = new SimpleBindings();
			bindings.putAll(env);
			r = IOUtils.getReader(script);
			engine.eval(r, bindings);
		}
		catch (Exception e) {
			Log.printStackTrace(e);
			return Status.CANCEL_STATUS;
		}
		finally {
			if (r != null) {
				try {
					r.close();
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return Status.OK_STATUS;
	}

	@Override
	public IStatus executeText(String str, HashMap<String, Object> env) {
		try {
			engine.eval(str, new SimpleBindings(env));
		}
		catch (ScriptException e) {
			Log.printStackTrace(e);
			return Status.CANCEL_STATUS;
		}
		return Status.OK_STATUS;
	}

	public static void main(String args[]) {
	}
}
