package org.txm.ruby.core;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

public class RubyPreferences extends TXMPreferences {

	public static final String HOME = "home"; //$NON-NLS-1$

	public static final String EXECUTABLE_FILENAME = "exec"; //$NON-NLS-1$

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		Preferences preferences = getDefaultPreferencesNode();

	}

	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(RubyPreferences.class)) {
			new RubyPreferences();
		}
		return TXMPreferences.instances.get(RubyPreferences.class);
	}
}
