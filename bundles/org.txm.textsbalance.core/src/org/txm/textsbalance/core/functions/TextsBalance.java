/**
 * 
 */
package org.txm.textsbalance.core.functions;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.txm.chartsengine.core.results.ChartResult;
import org.txm.core.results.Parameter;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.NetCqiClient;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.textsbalance.core.preferences.TextsBalancePreferences;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

/**
 * Texts balance.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class TextsBalance extends ChartResult {

	LinkedHashMap<Comparable<?>[], Integer> dataset;

	protected String propertyName;

	protected List<String> values;

	protected int fMin;

	protected int fMax;

	protected int method;

	/**
	 * Structural unit to explore.
	 */
	@Parameter(key = TextsBalancePreferences.STRUCTURAL_UNIT)
	protected StructuralUnit structuralUnit;

	/**
	 * Structural unit property to explore.
	 */
	@Parameter(key = TextsBalancePreferences.STRUCTURAL_UNIT_PROPERTY)
	protected StructuralUnitProperty structuralUnitProperty;

	/**
	 * Group by texts or not.
	 */
	@Parameter(key = TextsBalancePreferences.COUNT_TEXTS)
	protected boolean countTexts;


	/**
	 * Creates a not computed texts balance.
	 * 
	 * @param parent
	 */
	public TextsBalance(CQPCorpus parent) {
		super(parent);
	}

	/**
	 * Creates a not computed texts balance.
	 * 
	 * @param parametersNodePath
	 */
	public TextsBalance(String parametersNodePath) {
		super(parametersNodePath);
	}


	@Override
	public boolean loadParameters() {
		try {
			this.fMin = Integer.MAX_VALUE;
			this.fMax = 0;
			StructuralUnit su = this.getCorpus().getStructuralUnit(this.getStringParameterValue(TextsBalancePreferences.STRUCTURAL_UNIT));
			this.setParameters(su, su.getProperty(this.getStringParameterValue(TextsBalancePreferences.STRUCTURAL_UNIT_PROPERTY)));
		}
		catch (CqiClientException e) {
			Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param su
	 */
	public void setParameters(StructuralUnit su, StructuralUnitProperty sup) {
		this.structuralUnit = su;
		this.structuralUnitProperty = sup;

		// get the first property if it was null
		if (this.structuralUnitProperty == null) {
			this.structuralUnitProperty = this.structuralUnit.getUserDefinedOrderedProperties().get(0);
		}

		this.setDirty();
	}

	@Override
	protected boolean __compute(TXMProgressMonitor monitor) {

		monitor.setTask(
				"Computing balance with metadata propertyName = " + this.structuralUnitProperty + ", structural unit = " + this.structuralUnit.getName() + " and groupByTexts = " + this.countTexts); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		try {
			this.dataset = new LinkedHashMap<Comparable<?>[], Integer>();

			StructuralUnitProperty p = this.structuralUnitProperty;
			this.values = p.getValues();
			Collections.sort(this.values);

			this.fMax = 0;
			this.fMin = Integer.MAX_VALUE;

			// using CQL to solve matches
			if (this.method == 1) {
				for (String value : this.values) {
					int v = 0;
					if (this.countTexts) {
						QueryResult r = this.getCorpus().query(new CQLQuery("<" + p.getFullName() + "=\"" + value + "\">[]"), "tmp", false); //$NON-NLS-1$
						v = r.getNMatch();
						r.drop();
					}
					else {
						QueryResult r = this.getCorpus().query(new CQLQuery("[_." + p.getFullName() + "=\"" + value + "\"] expand to text"), "tmp", false); //$NON-NLS-1$
						int t = 0;
						for (Match m : r.getMatches()) {
							t += m.size();
						}
						v = t;
						r.drop();
					}
					if (v < this.fMin) {
						this.fMin = v;
					}
					if (v > this.fMax) {
						this.fMax = v;
					}
					this.dataset.put(new Comparable[] { p.getName(), value }, v);
				}
			}
			else {
				AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();
				if (CQI instanceof NetCqiClient) {
					Log.warning("Error: CQP eval method only available with CQP memory mode");
					return false;
				}

				String supqn = p.getQualifiedName();
				int nsup = CQI.attributeSize(supqn);

				ArrayList<Match> supLimits = new ArrayList<Match>(nsup);
				for (int i = 0; i < nsup; i++) {
					int[] l = CQI.struc2Cpos(supqn, i);
					supLimits.add(new Match(l[0], l[1]));
				}
				//System.out.println("All sup limits are "+sup_limits);
				List<? extends org.txm.objects.Match> corpus_matches = this.getCorpus().getMatches();
				//System.out.println("corpus matches: "+corpus_matches);
				ArrayList<Integer> inter = intersect(corpus_matches, supLimits);
				//System.out.println( "No of $sup that covers $corpus: "+inter);
				int[] structId = new int[inter.size()];
				for (int i = 0; i < structId.length; i++) {
					structId[i] = inter.get(i);
				}

				String[] interValues = CQI.struc2Str(supqn, structId);
				//System.out.println( " with values: "+Arrays.toString(inter_values));
				HashMap<String, Integer> interUniq = new HashMap<String, Integer>();

				if (this.countTexts) {
					for (String o : interValues) {
						if (!interUniq.containsKey(o)) {
							interUniq.put(o, 0);
						}
						interUniq.put(o, interUniq.get(o) + 1);
					}
				}
				else {
					for (int iMatch = 0; iMatch < inter.size(); iMatch++) {
						String o = interValues[iMatch];
						if (!interUniq.containsKey(o)) {
							interUniq.put(o, 0);
						}
						int s = supLimits.get(inter.get(iMatch)).size();
						interUniq.put(o, interUniq.get(o) + s);

					}
				}
				Object[] sortedkeys = interUniq.keySet().toArray();
				Arrays.sort(sortedkeys);
				for (Object k : sortedkeys) {
					int v = interUniq.get(k);
					if (v < this.fMin) {
						this.fMin = v;
					}
					if (v > this.fMax) {
						this.fMax = v;
					}
					this.dataset.put(new String[] { p.getName(), k.toString() }, v);
				}
			}
		}
		catch (Exception e) {
			Log.warning("Error while computing text information: " + e);
			e.printStackTrace();
			return false;
		}
		return true;
	}


	/**
	 * 
	 * @param corpus
	 * @param structs
	 * @return
	 */
	public static ArrayList<Integer> intersect(List<? extends org.txm.objects.Match> corpus, List<? extends org.txm.objects.Match> structs) {
		int ai = 0, bi = 0;
		ArrayList<Integer> result = new ArrayList<Integer>();

		while (ai < corpus.size() && bi < structs.size()) {
			if (structs.get(bi).contains(corpus.get(ai))) {
				result.add(bi);
				ai++;
			}
			else if (corpus.get(ai).contains(structs.get(bi))) {
				result.add(bi);
				bi++;
			}
			else {
				if (structs.get(bi).getStart() < corpus.get(ai).getStart()) {
					bi++;
				}
				else {
					ai++;
				}
			}
		}

		return result;
	}


	/**
	 * @return the dataset
	 */
	public HashMap<Comparable<?>[], Integer> getDataset() {
		return dataset;
	}


	/**
	 * @return the corpus
	 */
	public CQPCorpus getCorpus() {
		return (CQPCorpus) this.parent;
	}

	/**
	 * @return the propertyName
	 */
	public String getPropertyName() {
		return propertyName;
	}

	/**
	 * @return the values
	 */
	public List<String> getValues() {
		return values;
	}

	/**
	 * @return the fMin
	 */
	public int getFMin() {
		return fMin;
	}

	/**
	 * @return the fMax
	 */
	public int getFMax() {
		return fMax;
	}

	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {

		StringBuffer buffer = new StringBuffer();
		for (Comparable<?>[] k : dataset.keySet()) {
			buffer.append(txtseparator + k[1] + txtseparator + colseparator + txtseparator + dataset.get(k) + txtseparator + "\n");
		}
		return IOUtils.write(outfile, buffer.toString());
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub
	}

	/**
	 * @return the name
	 */
	@Override
	public String getName() {
		try {
			return this.getParent().getName() + " - " + this.getSimpleName();
		}
		catch (Exception e) {
		}
		return "";
	}

	@Override
	public String getSimpleName() {
		return this.structuralUnit.getName() + "@" + this.structuralUnitProperty + " (group by texts = " + this.countTexts + ")";
	}

	@Override
	public String getDetails() {
		return this.getName();
	}

	@Override
	public boolean canCompute() {
		return this.getCorpus() != null
				&& this.structuralUnit != null
				&& this.structuralUnitProperty != null;
	}


	@Override
	public boolean saveParameters() {
		// TODO Auto-generated method stub
		System.err.println("TextsBalance.saveParameters(): not yet implemented.");
		return true;
	}

	/**
	 * @return the groupByTexts
	 */
	public boolean groupingByTexts() {
		return countTexts;
	}



	/**
	 * @return the structuralUnit
	 */
	public StructuralUnit getStructuralUnit() {
		return structuralUnit;
	}


	/**
	 * @return the structuralUnitProperty
	 */
	public StructuralUnitProperty getStructuralUnitProperty() {
		return structuralUnitProperty;
	}


	/**
	 * @return the groupByTexts
	 */
	public boolean isGroupByTexts() {
		return countTexts;
	}


	/**
	 * @param countTexts the groupByTexts to set
	 */
	public void setDoCountTexts(boolean countTexts) {
		this.countTexts = countTexts;
	}


	/**
	 * @return the method
	 */
	public int getMethod() {
		return method;
	}


	/**
	 * Method 1: use CQL to solve matches, 2 uses corpus struct indexes, 2 is faster !
	 * 
	 * @param method the method to set
	 */
	public void setMethod(int method) {
		this.method = method;
	}

	@Override
	public String getResultType() {
		return "Texts balance"; //$NON-NLS-1$
	}


}
