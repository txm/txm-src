package org.txm.textsbalance.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Preferences initializer and manager.
 * 
 * @author sjacquot
 *
 */
public class TextsBalancePreferences extends ChartsEnginePreferences {


	public static final String COUNT_TEXTS = "group_by_texts"; //$NON-NLS-1$
	//public static final String METHOD = "method"; //$NON-NLS-1$


	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(TextsBalancePreferences.class)) {
			new TextsBalancePreferences();
		}
		return TXMPreferences.instances.get(TextsBalancePreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.putBoolean(COUNT_TEXTS, false);
		preferences.put(STRUCTURAL_UNIT, "text");
		//preferences.putInt(METHOD, 2);

		// to disable the functionality
		TXMPreferences.setEmpty(this.getPreferencesNodeQualifier(), ChartsEnginePreferences.SHOW_GRID);
	}



}


