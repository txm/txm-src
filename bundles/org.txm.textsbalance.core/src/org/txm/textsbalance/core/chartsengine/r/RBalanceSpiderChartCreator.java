package org.txm.textsbalance.core.chartsengine.r;

import java.io.File;

import org.txm.chartsengine.r.core.RChartCreator;
import org.txm.textsbalance.core.functions.TextsBalance;

/**
 * 
 * @author sjacquot
 *
 */
public class RBalanceSpiderChartCreator extends RChartCreator<TextsBalance> {


	@Override
	public File createChart(TextsBalance result, File file) {

		// try {


		// RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		// System.out.println("load wordcloud...");
		// rw.eval("library(wordcloud)"); //$NON-NLS-1$
		// System.out.println("tmpfreqs...");
		// rw.addVectorToWorkspace("tmpfreqs", wordCloud.getFreqs()); //$NON-NLS-1$
		// System.out.println("tmplabels...");
		// rw.addVectorToWorkspace("tmplabels", wordCloud.getLabels()); //$NON-NLS-1$

		// wordcloud(words,freq,
		// scale : A vector of length 2 indicating the range of the size of the words.
		// min.freq: words with frequency below min.freq will not be plotted
		// max.words : Maximum number of words to be plotted. least frequent terms dropped
		// random.order : plot words in random order. If false, they will be plotted in decreasing frequency
		// random.color : choose colors randomly from the colors. If false, the color is chosen based on the frequency
		// rot.per : proportion words with 90 degree rotation
		// colors : color words from least to most frequent
		// ordered.colors : if true, then colors are assigned to words in order
		// System.out.println("wordcloud...");

		// FIXME : stars plot tests
		// String cmd = "stars(mtcars[, 1:7], key.loc = c(14, 2), main = \"Motor Trend Cars : stars(*, full = F)\", full = FALSE)"; //$NON-NLS-1$
		String cmd = "stars(USJudgeRatings, locations = c(0, 0), scale = FALSE, radius  =  FALSE, col.stars = 1:10, key.loc = c(0, 0), main = \"US Judges rated\")"; //$NON-NLS-1$
		// String cmd = "stars(cbind(1:16, 10*(16:1)), draw.segments = TRUE, main = \"A Joke -- do *not* use symbols on 2D data!\")";



		// rw.plot(file, cmd, ((RChartsEngine) this.chartsEngine).getRDevice());
		this.chartsEngine.plot(file, cmd);

		// System.out.println("cleaning");
		// rw.eval("rm(tmplabels)"); //$NON-NLS-1$
		// rw.eval("rm(tmpfreqs)"); //$NON-NLS-1$
		// }
		// catch(RWorkspaceException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// catch(REXPMismatchException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// catch(StatException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		return file;
	}

	@Override
	public Class<TextsBalance> getResultDataClass() {
		return TextsBalance.class;
	}
}
