package org.txm.textsbalance.core.chartsengine.jfreechart;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.CategoryToolTipGenerator;
import org.jfree.chart.plot.SpiderWebPlot;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.txm.chartsengine.jfreechart.core.JFCChartCreator;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.textsbalance.core.functions.TextsBalance;

public class JFCTextsBalanceSpiderChartCreator extends JFCChartCreator<TextsBalance> {


	private SpiderWebPlot plot;

	@Override
	public JFreeChart createChart(TextsBalance result) {

		JFreeChart chart = null;

		// Creates an empty dataset
		//		HashMap<Comparable<?>[], Integer> data = result.getDataset();
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		plot = new SpiderWebPlot(dataset);
		plot.setToolTipGenerator(new CategoryToolTipGenerator() {

			@Override
			public String generateToolTip(CategoryDataset dataset, int row, int col) {
				// FIXME: tests
				// System.out.println("JFCBalanceSpiderChartCreator.createChart(...).new CategoryToolTipGenerator() {...}.generateToolTip() row = " + row + " col = " + col);
				// System.out.println("JFCBalanceSpiderChartCreator.createChart(...).new CategoryToolTipGenerator() {...}.generateToolTip() " + dataset.getValue(row, col));

				// if(balance.getValues().size() > col) {
				// balance.getValues().get(col);
				// }
				// return "error";
				return String.valueOf(dataset.getValue(row, col).intValue());
			}
		});


		chart = new JFreeChart(plot);

		return chart;
	}


	@Override
	public void updateChart(TextsBalance result) {

		TextsBalance textsBalance = (TextsBalance) result;

		JFreeChart chart = (JFreeChart) result.getChart();

		// freeze rendering while computing
		chart.setNotify(false);

		// updating data set
		HashMap<Comparable<?>[], Integer> data = textsBalance.getDataset();
		DefaultCategoryDataset dataset = (DefaultCategoryDataset) ((SpiderWebPlot) chart.getPlot()).getDataset();
		dataset.clear();

		Set<Comparable<?>[]> keys = data.keySet();
		Iterator<Comparable<?>[]> it = keys.iterator();
		while (it.hasNext()) {
			Comparable<?>[] key = it.next();
			Integer value = data.get(key);
			//System.out.println("JFCTextsBalanceSpiderChartCreator.updateChart(): adding " + key + ", " + key[0] + ", " + key[1] + " to dataset."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			dataset.addValue(value, key[0], key[1]);
		}

		chart.setTitle(TXMCoreMessages.bind("metadata = {0}, N = {1}, fmin = {2}, fmax = {3}, counting {4}", textsBalance.getStructuralUnitProperty(), textsBalance.getValues().size(), textsBalance.getFMin(), textsBalance.getFMax(),
				(textsBalance.groupingByTexts() ? "texts" : "words"))); //$NON-NLS-1$
		
		
		plot.setMaxValue(textsBalance.getFMax());

		super.updateChart(result);
	}

	@Override
	public Class<TextsBalance> getResultDataClass() {
		return TextsBalance.class;
	}



}
