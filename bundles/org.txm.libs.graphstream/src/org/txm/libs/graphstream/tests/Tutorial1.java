package org.txm.libs.graphstream.tests;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

public class Tutorial1 {
	
	public static void main(String args[]) {
		
		// System.setProperty("gs.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		// System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		
		Graph graph = new SingleGraph("Tutorial 1");
		graph.addAttribute("ui.stylesheet", styleSheet);
		// graph.setAutoCreate(true);
		// graph.setStrict(false);
		
		graph.addNode("A");
		graph.addNode("B");
		graph.addNode("C");
		graph.addEdge("AB", "A", "B");
		graph.addEdge("BC", "B", "C");
		graph.addEdge("CA", "C", "A");
		
		
		
		
		// Graph graph = new SingleGraph("random euclidean");
		// Generator gen = new RandomEuclideanGenerator();
		// gen.addSink(graph);
		// gen.begin();
		// for(int i=0; i<1000; i++) {
		// gen.nextEvents();
		// }
		// gen.end();
		// graph.display();
		
		
		// Graph graph = new SingleGraph("Dorogovtsev mendes");
		// Generator gen = new DorogovtsevMendesGenerator();
		// gen.addSink(graph);
		// gen.begin();
		// for(int i=0; i<500; i++) {
		// gen.nextEvents();
		// }
		// gen.end();
		//
		
		
		for (Node node : graph) {
			node.addAttribute("ui.label", node.getId());
			// node.addAttribute("ui.size", 50);
			// //node.addAttribute("ui.color", Color.RED);
			// node.addAttribute("ui.style", "fill-color: rgb(0,100,255);");
		}
		graph.display();
		
		
		// JFrame frame = new JFrame();
		// JPanel pane1 = new JPanel();
		// Viewer v = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_GUI_THREAD);
		// v.enableAutoLayout();
		// ViewPanel view = v.addDefaultView(false);
		// pane1.setLayout(new BorderLayout());
		// pane1.add(view,BorderLayout.CENTER);
		// frame.add(pane1);
		// frame.setSize(800, 600);
		// //frame.pack();
		// frame.setVisible(true);
		// frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}
	
	
	// Ouvrage
	static protected String styleSheet = "node {" +
	// " fill-color: black;" +
			"size-mode: dyn-size;" +
			"size: 10px;" +
			// " size: 30px, 30px;" +
			// " text-size: 12px;" +
			// " shape: freeplane;" +
			// // " size-mode: fit;" +
			// " fill-mode: none;" +
			// " stroke-mode: plain;" +
			"}" +
			"edge {" +
			"   shape: cubic-curve;" +
			
			
			
			// " stroke-mode: plain;" +
			
			"}" +
			/*
			 * "node.marked {" +
			 * "   fill-color: red;" +
			 * "}" +
			 */
			
			"node.titre {" +
			"   size: 55px, 45px;" +
			"   fill-color: blue;" +
			"   text-color: red;" +
			"   shape: diamond;" +
			"}" +
			
			"node.axe {" +
			"   fill-color: blue;" +
			"   text-color: blue;" +
			"   shape: box;" +
			"}" +
			/*
			 * "node.mot {" +
			 * "   fill-color: green;" +
			 * "   text-color: green;" +
			 * "}" +
			 */
			"node.auteur {" +
			"   fill-color: red;" +
			"   text-color: gray;" +
			"   shape: diamond;" +
			"}";
			
}
