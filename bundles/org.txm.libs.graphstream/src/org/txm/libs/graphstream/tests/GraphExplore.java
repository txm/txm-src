package org.txm.libs.graphstream.tests;

import java.util.Iterator;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

public class GraphExplore {
	
	public static void main(String args[]) {
		new GraphExplore();
	}
	
	public GraphExplore() {
		
		
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer"); // Pour les fonctionnalités avancées des CSS
		
		
		Graph graph = new SingleGraph("tutorial 1");
		
		
		
		graph.addAttribute("ui.stylesheet", styleSheet);
		graph.setAutoCreate(true);
		graph.setStrict(false);
		
		// graph.display(false);
		graph.display();
		
		// Viewer viewer = graph.display(false);
		// View view = viewer.getDefaultView();
		// view.resizeFrame(800, 600);
		// view.setViewCenter(440000, 2503000, 0);
		// view.setViewPercent(0.25);
		
		
		
		Node n;
		// graph.addEdge("AB", "A", "B");
		// graph.addEdge("BC", "B", "C");
		// graph.addEdge("CA", "C", "A");
		// graph.addEdge("AD", "A", "D");
		// graph.addEdge("DE", "D", "E");
		// graph.addEdge("DF", "D", "F");
		// graph.addEdge("EF", "E", "F");
		// n = graph.getNode("B");
		// n.setAttribute("ui.class", "doc");
		
		// graph.addEdge("1", "Oeuvres de référence", "Documents");
		// graph.addEdge("2", "Oeuvres de référence", "Fiches chorégraphiques");
		// graph.addEdge("3", "Documents", "Fiches chorégraphiques");
		// graph.addEdge("4", "Personnes", "Fiches chorégraphiques");
		// graph.addEdge("5", "Personnes", "Documents");
		// graph.addEdge("6", "Personnes", "Oeuvres de référence");
		// graph.addEdge("6", "Personnes", "Oeuvres de référence");
		
		
		// Bagouet
		/*
		 * graph.addEdge("doc_or_00", "Endenich (1976)", "Archives super 8"); n = graph.getNode("Archives super 8"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Endenich (1976)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_01", "Valse des fleurs (1983)", "Archives super 8"); n = graph.getNode("Archives super 8"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Valse des fleurs (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_00", "Archives super 8", "Endenich (1976-08-02)"); n = graph.getNode("Archives super 8"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Endenich (1976-08-02)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_01", "Archives super 8", "Valse des fleurs (1983-06-25)"); n = graph.getNode("Archives super 8"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Valse des fleurs (1983-06-25)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_10", "Chansons de nuit (1976)", "Pièces 1976-1981, extraits"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Chansons de nuit (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_11", "Danses blanches (1979)", "Pièces 1976-1981, extraits"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Danses blanches (1979)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_12", "Grand Corridor (1980)", "Pièces 1976-1981, extraits"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Grand Corridor (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_13", "Ribatz, Ribatz! (1976)", "Pièces 1976-1981, extraits"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ribatz, Ribatz! (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_14", "Scène rouge (1980)", "Pièces 1976-1981, extraits"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Scène rouge (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_15", "Sous la blafarde (1979)", "Pièces 1976-1981, extraits"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Sous la blafarde (1979)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_16", "Suite pour violes (1977)", "Pièces 1976-1981, extraits"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suite pour violes (1977)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_17", "Toboggan (1981)", "Pièces 1976-1981, extraits"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Toboggan (1981)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_18", "Une Danse blanche avec Eliane (1980)", "Pièces 1976-1981, extraits"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Une Danse blanche avec Eliane (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_19", "Voyage organisé (1977)", "Pièces 1976-1981, extraits"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Voyage organisé (1977)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_10", "Pièces 1976-1981, extraits", "Chansons de nuit (1976-02-27)"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Chansons de nuit (1976-02-27)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_11", "Pièces 1976-1981, extraits", "Danses blanches (1979-10-00)"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Danses blanches (1979-10-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_12", "Pièces 1976-1981, extraits", "Grand Corridor (1980-07-21)"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Grand Corridor (1980-07-21)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_13", "Pièces 1976-1981, extraits", "Ribatz, Ribatz! (1976-11-09)"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ribatz, Ribatz! (1976-11-09)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_14", "Pièces 1976-1981, extraits", "Scène rouge (1980-12-06)"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Scène rouge (1980-12-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_15", "Pièces 1976-1981, extraits", "Sous la blafarde (1979-12-00)"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Sous la blafarde (1979-12-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_16", "Pièces 1976-1981, extraits", "Suite pour violes (1977-04-00)"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suite pour violes (1977-04-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_17", "Pièces 1976-1981, extraits", "Toboggan (1981-12-04)"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Toboggan (1981-12-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_18", "Pièces 1976-1981, extraits", "Une danse blanche avec Eliane (1980-01-04)"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Une danse blanche avec Eliane (1980-01-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_19", "Pièces 1976-1981, extraits", "Voyage organisé (1977-10-06)"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Voyage organisé (1977-10-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_110", "Pièces 1976-1981, extraits", "Voyage organisé, nouvelle version (1980-12-06)"); n = graph.getNode("Pièces 1976-1981, extraits"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Voyage organisé, nouvelle version (1980-12-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_20", "Snark (1976)", "Snark"); n = graph.getNode("Snark"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Snark (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_20", "Snark", "Snark (1976-11-09)"); n = graph.getNode("Snark"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Snark (1976-11-09)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_30", "Sonate Trio (1976)", "Sonate Trio"); n = graph.getNode("Sonate Trio"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Sonate Trio (1976)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_30", "Sonate Trio", "Sonate trio (1976-11-09)"); n = graph.getNode("Sonate Trio"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Sonate trio (1976-11-09)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_40", "Chansons de nuit (1976)", "Chansons de nuit"); n = graph.getNode("Chansons de nuit"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Chansons de nuit (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_40", "Chansons de nuit", "Chansons de nuit (1976-02-27)"); n = graph.getNode("Chansons de nuit"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Chansons de nuit (1976-02-27)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_50", "Etudes Tableaux (1977)", "Etudes Tableaux"); n = graph.getNode("Etudes Tableaux"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Etudes Tableaux (1977)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_50", "Etudes Tableaux", "Études tableaux (1977-10-01)"); n = graph.getNode("Etudes Tableaux"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Études tableaux (1977-10-01)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_60", "Passages (1978)", "Passages"); n = graph.getNode("Passages"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Passages (1978)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_fchor_60", "Passages", "Passages (1978-02-24)"); n = graph.getNode("Passages"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Passages (1978-02-24)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_70", "Sur des herbes lointaines (1978)", "Sur des herbes lointaines"); n = graph.getNode("Sur des herbes lointaines"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Sur des herbes lointaines (1978)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_70", "Sur des herbes lointaines", "Sur des herbes lointaines (1978-10-00)"); n = graph.getNode("Sur des herbes lointaines"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Sur des herbes lointaines (1978-10-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_80", "Tartines (1978)", "Tartines"); n = graph.getNode("Tartines"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Tartines (1978)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_fchor_80", "Tartines", "Tartines (1978-06-02)"); n = graph.getNode("Tartines"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Tartines (1978-06-02)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_90", "Gens de… (Les) (1979)", "Gens de… (Les), conte dansé"); n = graph.getNode("Gens de… (Les), conte dansé"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Gens de… (Les) (1979)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_90", "Gens de… (Les), conte dansé", "Gens de… (Les) (1979-05-00)"); n = graph.getNode("Gens de… (Les), conte dansé"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Gens de… (Les) (1979-05-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_100", "Sous la blafarde (1979)", "Sous la blafarde,répétition"); n = graph.getNode("Sous la blafarde,répétition"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Sous la blafarde (1979)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_100", "Sous la blafarde,répétition", "Sous la blafarde (1979-12-00)"); n = graph.getNode("Sous la blafarde,répétition"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Sous la blafarde (1979-12-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_110", "Suite pour violes (1977)", "Suite pour violes, répétition"); n = graph.getNode("Suite pour violes, répétition"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suite pour violes (1977)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_110", "Suite pour violes, répétition", "Suite pour violes (1977-04-00)"); n = graph.getNode("Suite pour violes, répétition"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suite pour violes (1977-04-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_120", "Chansons de nuit (1976)", "Dominique avec passion"); n = graph.getNode("Dominique avec passion"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Chansons de nuit (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_121", "Grand Corridor (1980)", "Dominique avec passion"); n = graph.getNode("Dominique avec passion"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Grand Corridor (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_122", "Sous la blafarde (1979)", "Dominique avec passion"); n = graph.getNode("Dominique avec passion"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Sous la blafarde (1979)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_120", "Dominique avec passion", "Chansons de nuit (1976-02-27)"); n = graph.getNode("Dominique avec passion"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Chansons de nuit (1976-02-27)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_121", "Dominique avec passion", "Grand Corridor (1980-07-21)"); n = graph.getNode("Dominique avec passion"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Grand Corridor (1980-07-21)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_122", "Dominique avec passion", "Sous la blafarde (1979-12-00)"); n = graph.getNode("Dominique avec passion"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Sous la blafarde (1979-12-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_130", "Émissions de télévision, extraits", "Insaisies (1982-07-05)"); n = graph.getNode("Émissions de télévision, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insaisies (1982-07-05)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_131", "Émissions de télévision, extraits", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Émissions de télévision, extraits"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_132", "Émissions de télévision, extraits", "Voyage organisé (1977-10-06)"); n = graph.getNode("Émissions de télévision, extraits"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Voyage organisé (1977-10-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_140", "Petites Pièces de Berlin (Les) (1988)", "Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame"); n =
		 * graph.getNode("Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_141", "Scène rouge (1980)", "Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame"); n =
		 * graph.getNode("Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Scène rouge (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_142", "So Schnell (1990)", "Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame"); n =
		 * graph.getNode("Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_143", "Voyage organisé (1977)", "Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame"); n =
		 * graph.getNode("Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Voyage organisé (1977)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_140", "Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame", "Petites Pièces de Berlin (Les) (1988-06-04)"); n =
		 * graph.getNode("Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les) (1988-06-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_141", "Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame", "Scène rouge (1980-12-06)"); n =
		 * graph.getNode("Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Scène rouge (1980-12-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_142", "Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame", "So Schnell (1990-12-06)"); n =
		 * graph.getNode("Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990-12-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_143", "Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame", "So Schnell, version 92 (1992-10-11)"); n =
		 * graph.getNode("Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell, version 92 (1992-10-11)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_144", "Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame", "Voyage organisé, nouvelle version (1980-12-06)"); n =
		 * graph.getNode("Extraits divers : répétition Petites Pièces de Berlin, Scène rouge, Voyage organisé, Chaîne et trame"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Voyage organisé, nouvelle version (1980-12-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_150", "Une Danse blanche avec Eliane (1980)", "Une danse blanche avec Eliane : émission de télévision"); n =
		 * graph.getNode("Une danse blanche avec Eliane : émission de télévision"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Une Danse blanche avec Eliane (1980)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_150", "Une danse blanche avec Eliane : émission de télévision", "Une danse blanche avec Eliane (1980-01-04)"); n =
		 * graph.getNode("Une danse blanche avec Eliane : émission de télévision"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Une danse blanche avec Eliane (1980-01-04)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_160", "Une Danse blanche avec Eliane (1980)", "Une danse blanche avec Eliane"); n = graph.getNode("Une danse blanche avec Eliane"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Une Danse blanche avec Eliane (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_160", "Une danse blanche avec Eliane", "Une danse blanche avec Eliane (1980-01-04)"); n = graph.getNode("Une danse blanche avec Eliane"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Une danse blanche avec Eliane (1980-01-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_170", "Ribatz, Ribatz! (1976)", "Conférence dansée Fnac 1980"); n = graph.getNode("Conférence dansée Fnac 1980"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ribatz, Ribatz! (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_171", "Suite pour violes (1977)", "Conférence dansée Fnac 1980"); n = graph.getNode("Conférence dansée Fnac 1980"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suite pour violes (1977)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_172", "Une Danse blanche avec Eliane (1980)", "Conférence dansée Fnac 1980"); n = graph.getNode("Conférence dansée Fnac 1980"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Une Danse blanche avec Eliane (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_170", "Conférence dansée Fnac 1980", "Ribatz, Ribatz! (1976-11-09)"); n = graph.getNode("Conférence dansée Fnac 1980"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ribatz, Ribatz! (1976-11-09)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_171", "Conférence dansée Fnac 1980", "Sous la blafarde (1979-12-00)"); n = graph.getNode("Conférence dansée Fnac 1980"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Sous la blafarde (1979-12-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_172", "Conférence dansée Fnac 1980", "Suite pour violes (1977-04-00)"); n = graph.getNode("Conférence dansée Fnac 1980"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suite pour violes (1977-04-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_173", "Conférence dansée Fnac 1980", "Une danse blanche avec Eliane (1980-01-04)"); n = graph.getNode("Conférence dansée Fnac 1980"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Une danse blanche avec Eliane (1980-01-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_180", "Ribatz, Ribatz! (1976)", "Ribatz, Ribatz! (extrait : duo)"); n = graph.getNode("Ribatz, Ribatz! (extrait : duo)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ribatz, Ribatz! (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_180", "Ribatz, Ribatz! (extrait : duo)", "Ribatz, Ribatz! (1976-11-09)"); n = graph.getNode("Ribatz, Ribatz! (extrait : duo)"); n.setAttribute("ui.class", "doc"); n
		 * = graph.getNode("Ribatz, Ribatz! (1976-11-09)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_190", "Conférence (1979)", "Conférence"); n = graph.getNode("Conférence"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Conférence (1979)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_190", "Conférence", "Conférence (1979-00-00)"); n = graph.getNode("Conférence"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Conférence (1979-00-00)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_200", "Danses blanches (1979)", "Danses blanches"); n = graph.getNode("Danses blanches"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Danses blanches (1979)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_200", "Danses blanches", "Danses blanches (1979-10-00)"); n = graph.getNode("Danses blanches"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Danses blanches (1979-10-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_210", "Sous la blafarde (1979)", "Sous la blafarde"); n = graph.getNode("Sous la blafarde"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Sous la blafarde (1979)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_210", "Sous la blafarde", "Sous la blafarde (1979-12-00)"); n = graph.getNode("Sous la blafarde"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Sous la blafarde (1979-12-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_220", "Suite pour violes (1977)", "Suite pour violes"); n = graph.getNode("Suite pour violes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suite pour violes (1977)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_220", "Suite pour violes", "Suite pour violes (1977-04-00)"); n = graph.getNode("Suite pour violes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suite pour violes (1977-04-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_230", "Grand Corridor (1980)", "Grand Corridor"); n = graph.getNode("Grand Corridor"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Grand Corridor (1980)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_240", "Scène rouge (1980)", "Scène rouge"); n = graph.getNode("Scène rouge"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Scène rouge (1980)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_240", "Scène rouge", "Scène rouge (1980-12-06)"); n = graph.getNode("Scène rouge"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Scène rouge (1980-12-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_250", "Voyage organisé (1977)", "Voyage organisé (nouvelle version)"); n = graph.getNode("Voyage organisé (nouvelle version)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Voyage organisé (1977)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_250", "Voyage organisé (nouvelle version)", "Voyage organisé, nouvelle version (1980-12-06)"); n = graph.getNode("Voyage organisé (nouvelle version)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Voyage organisé, nouvelle version (1980-12-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_260", "Daphnis et Alcimadure (1981)", "Daphnis et alcimadure, pastorale occitane de 1754 en 3 actes avec prologue"); n =
		 * graph.getNode("Daphnis et alcimadure, pastorale occitane de 1754 en 3 actes avec prologue"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Daphnis et Alcimadure (1981)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_260", "Daphnis et alcimadure, pastorale occitane de 1754 en 3 actes avec prologue", "Daphnis et Alcimadure (1981-06-29)"); n =
		 * graph.getNode("Daphnis et alcimadure, pastorale occitane de 1754 en 3 actes avec prologue"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Daphnis et Alcimadure (1981-06-29)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_270", "Scène rouge (1980)", "Promotion Compagnie Bagouet 1981"); n = graph.getNode("Promotion Compagnie Bagouet 1981"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Scène rouge (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_271", "Voyage organisé (1977)", "Promotion Compagnie Bagouet 1981"); n = graph.getNode("Promotion Compagnie Bagouet 1981"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Voyage organisé (1977)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_270", "Promotion Compagnie Bagouet 1981", "Scène rouge (1980-12-06)"); n = graph.getNode("Promotion Compagnie Bagouet 1981"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Scène rouge (1980-12-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_271", "Promotion Compagnie Bagouet 1981", "Voyage organisé, nouvelle version (1980-12-06)"); n = graph.getNode("Promotion Compagnie Bagouet 1981");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Voyage organisé, nouvelle version (1980-12-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_280", "Scène rouge (1980)", "Scène rouge (extraits)"); n = graph.getNode("Scène rouge (extraits)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Scène rouge (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_280", "Scène rouge (extraits)", "Scène rouge (1980-12-06)"); n = graph.getNode("Scène rouge (extraits)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Scène rouge (1980-12-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_290", "Voyageurs (Les) (1981)", "Voyageurs (Les)"); n = graph.getNode("Voyageurs (Les)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Voyageurs (Les) (1981)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_290", "Voyageurs (Les)", "Voyageurs (Les) (1981-04-14)"); n = graph.getNode("Voyageurs (Les)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Voyageurs (Les) (1981-04-14)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_300", "Grand Corridor (1980)", "Danser avec Dominique et les autres"); n = graph.getNode("Danser avec Dominique et les autres"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Grand Corridor (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_301", "Scène rouge (1980)", "Danser avec Dominique et les autres"); n = graph.getNode("Danser avec Dominique et les autres"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Scène rouge (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_302", "Voyage organisé (1977)", "Danser avec Dominique et les autres"); n = graph.getNode("Danser avec Dominique et les autres"); n.setAttribute("ui.class", "doc"); n
		 * = graph.getNode("Voyage organisé (1977)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_300", "Danser avec Dominique et les autres", "Grand Corridor (1980-07-21)"); n = graph.getNode("Danser avec Dominique et les autres"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Grand Corridor (1980-07-21)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_301", "Danser avec Dominique et les autres", "Scène rouge (1980-12-06)"); n = graph.getNode("Danser avec Dominique et les autres"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Scène rouge (1980-12-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_302", "Danser avec Dominique et les autres", "Voyage organisé (1977-10-06)"); n = graph.getNode("Danser avec Dominique et les autres"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Voyage organisé (1977-10-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_310", "Toboggan (1981)", "Toboggan"); n = graph.getNode("Toboggan"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Toboggan (1981)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_310", "Toboggan", "Toboggan (1981-12-04)"); n = graph.getNode("Toboggan"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Toboggan (1981-12-04)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_320", "Insaisies (1982)", "Emissions de TV : Du côté de la jeune danse française, solo Bagouet (Petites Pièces de Berlin), interview Bagouet Hivernales 1991"); n =
		 * graph.getNode("Emissions de TV : Du côté de la jeune danse française, solo Bagouet (Petites Pièces de Berlin), interview Bagouet Hivernales 1991"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insaisies (1982)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_321", "Petites Pièces de Berlin (Les) (1988)",
		 * "Emissions de TV : Du côté de la jeune danse française, solo Bagouet (Petites Pièces de Berlin), interview Bagouet Hivernales 1991"); n =
		 * graph.getNode("Emissions de TV : Du côté de la jeune danse française, solo Bagouet (Petites Pièces de Berlin), interview Bagouet Hivernales 1991"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_320", "Emissions de TV : Du côté de la jeune danse française, solo Bagouet (Petites Pièces de Berlin), interview Bagouet Hivernales 1991",
		 * "Insaisies (1982-07-05)"); n = graph.getNode("Emissions de TV : Du côté de la jeune danse française, solo Bagouet (Petites Pièces de Berlin), interview Bagouet Hivernales 1991");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Insaisies (1982-07-05)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_321", "Emissions de TV : Du côté de la jeune danse française, solo Bagouet (Petites Pièces de Berlin), interview Bagouet Hivernales 1991",
		 * "Petites Pièces de Berlin (Les) (1988-06-04)"); n =
		 * graph.getNode("Emissions de TV : Du côté de la jeune danse française, solo Bagouet (Petites Pièces de Berlin), interview Bagouet Hivernales 1991"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les) (1988-06-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_330", "Déserts d’amour (1984)", "Pièces 1982-1984, extraits"); n = graph.getNode("Pièces 1982-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_331", "F. et Stein (1983)", "Pièces 1982-1984, extraits"); n = graph.getNode("Pièces 1982-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("F. et Stein (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_332", "Grande Maison (1983)", "Pièces 1982-1984, extraits"); n = graph.getNode("Pièces 1982-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Grande Maison (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_333", "Insaisies (1982)", "Pièces 1982-1984, extraits"); n = graph.getNode("Pièces 1982-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insaisies (1982)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_334", "Tant mieux, tant mieux! (1983)", "Pièces 1982-1984, extraits"); n = graph.getNode("Pièces 1982-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Tant mieux, tant mieux! (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_335", "Valse des fleurs (1983)", "Pièces 1982-1984, extraits"); n = graph.getNode("Pièces 1982-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Valse des fleurs (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_330", "Pièces 1982-1984, extraits", "Déserts d'amour (1984-07-01)"); n = graph.getNode("Pièces 1982-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d'amour (1984-07-01)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_331", "Pièces 1982-1984, extraits", "F. et Stein (1983-02-19)"); n = graph.getNode("Pièces 1982-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("F. et Stein (1983-02-19)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_332", "Pièces 1982-1984, extraits", "Grande Maison (1983-12-03)"); n = graph.getNode("Pièces 1982-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Grande Maison (1983-12-03)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_333", "Pièces 1982-1984, extraits", "Insaisies (1982-07-05)"); n = graph.getNode("Pièces 1982-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insaisies (1982-07-05)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_334", "Pièces 1982-1984, extraits", "Tant mieux, tant mieux! (1983-07-00)"); n = graph.getNode("Pièces 1982-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Tant mieux, tant mieux! (1983-07-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_335", "Pièces 1982-1984, extraits", "Valse des fleurs (1983-06-25)"); n = graph.getNode("Pièces 1982-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Valse des fleurs (1983-06-25)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_340", "Déserts d’amour (1984)", "Pièces 1983-1984, extraits"); n = graph.getNode("Pièces 1983-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_341", "F. et Stein (1983)", "Pièces 1983-1984, extraits"); n = graph.getNode("Pièces 1983-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("F. et Stein (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_342", "Grande Maison (1983)", "Pièces 1983-1984, extraits"); n = graph.getNode("Pièces 1983-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Grande Maison (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_343", "Tant mieux, tant mieux! (1983)", "Pièces 1983-1984, extraits"); n = graph.getNode("Pièces 1983-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Tant mieux, tant mieux! (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_340", "Pièces 1983-1984, extraits", "Déserts d'amour (1984-07-01)"); n = graph.getNode("Pièces 1983-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d'amour (1984-07-01)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_341", "Pièces 1983-1984, extraits", "F. et Stein (1983-02-19)"); n = graph.getNode("Pièces 1983-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("F. et Stein (1983-02-19)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_342", "Pièces 1983-1984, extraits", "Grande Maison (1983-12-03)"); n = graph.getNode("Pièces 1983-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Grande Maison (1983-12-03)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_343", "Pièces 1983-1984, extraits", "Tant mieux, tant mieux! (1983-07-00)"); n = graph.getNode("Pièces 1983-1984, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Tant mieux, tant mieux! (1983-07-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_350", "Tant mieux, tant mieux! (1983)", "Tant mieux, tant mieux!"); n = graph.getNode("Tant mieux, tant mieux!"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Tant mieux, tant mieux! (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_350", "Tant mieux, tant mieux!", "Tant mieux, tant mieux! (1983-07-00)"); n = graph.getNode("Tant mieux, tant mieux!"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Tant mieux, tant mieux! (1983-07-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_360", "Tant mieux, tant mieux! (1983)", "Tant mieux, tant mieux! version longue"); n = graph.getNode("Tant mieux, tant mieux! version longue");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Tant mieux, tant mieux! (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_360", "Tant mieux, tant mieux! version longue", "Tant mieux, tant mieux! (1983-07-00)"); n = graph.getNode("Tant mieux, tant mieux! version longue");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Tant mieux, tant mieux! (1983-07-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_370", "Insaisies (1982)", "Insaisies"); n = graph.getNode("Insaisies"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Insaisies (1982)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_370", "Insaisies", "Insaisies (1982-07-05)"); n = graph.getNode("Insaisies"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Insaisies (1982-07-05)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_380", "F. et Stein (1983)", "F. et Stein"); n = graph.getNode("F. et Stein"); n.setAttribute("ui.class", "doc"); n = graph.getNode("F. et Stein (1983)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_380", "F. et Stein", "F. et Stein (1983-02-19)"); n = graph.getNode("F. et Stein"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("F. et Stein (1983-02-19)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_390", "F. et Stein (1983)", "F. et Stein, extraits"); n = graph.getNode("F. et Stein, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("F. et Stein (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_390", "F. et Stein, extraits", "F. et Stein (1983-02-19)"); n = graph.getNode("F. et Stein, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("F. et Stein (1983-02-19)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_400", "Valse des fleurs (1983)", "Valse des fleurs"); n = graph.getNode("Valse des fleurs"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Valse des fleurs (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_400", "Valse des fleurs", "Valse des fleurs (1983-06-25)"); n = graph.getNode("Valse des fleurs"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Valse des fleurs (1983-06-25)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_410", "Divertissement 138 (1985)", "Valse des fleurs ; Divertissement 138"); n = graph.getNode("Valse des fleurs ; Divertissement 138"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Divertissement 138 (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_411", "Valse des fleurs (1983)", "Valse des fleurs ; Divertissement 138"); n = graph.getNode("Valse des fleurs ; Divertissement 138"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Valse des fleurs (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_410", "Valse des fleurs ; Divertissement 138", "Divertissement 138 (1985-06-24)"); n = graph.getNode("Valse des fleurs ; Divertissement 138");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Divertissement 138 (1985-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_411", "Valse des fleurs ; Divertissement 138", "Valse des fleurs (1983-06-25)"); n = graph.getNode("Valse des fleurs ; Divertissement 138");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Valse des fleurs (1983-06-25)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_420", "En Ac ou en Ille (1983)", "F. et Stein / En Ac ou en Ille, extraits"); n = graph.getNode("F. et Stein / En Ac ou en Ille, extraits"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("En Ac ou en Ille (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_421", "F. et Stein (1983)", "F. et Stein / En Ac ou en Ille, extraits"); n = graph.getNode("F. et Stein / En Ac ou en Ille, extraits"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("F. et Stein (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_420", "F. et Stein / En Ac ou en Ille, extraits", "En Ac ou en Ille (1983-06-29)"); n = graph.getNode("F. et Stein / En Ac ou en Ille, extraits");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("En Ac ou en Ille (1983-06-29)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_421", "F. et Stein / En Ac ou en Ille, extraits", "F. et Stein (1983-02-19)"); n = graph.getNode("F. et Stein / En Ac ou en Ille, extraits");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("F. et Stein (1983-02-19)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_430", "Grande Maison (1983)", "Grande Maison, répétitions"); n = graph.getNode("Grande Maison, répétitions"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Grande Maison (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_430", "Grande Maison, répétitions", "Grande Maison (1983-12-03)"); n = graph.getNode("Grande Maison, répétitions"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Grande Maison (1983-12-03)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_440", "Grande Maison (1983)", "Grande Maison"); n = graph.getNode("Grande Maison"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Grande Maison (1983)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_440", "Grande Maison", "Grande Maison (1983-12-03)"); n = graph.getNode("Grande Maison"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Grande Maison (1983-12-03)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_450", "Déserts d’amour (1984)", "Déserts d'amour"); n = graph.getNode("Déserts d'amour"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_450", "Déserts d'amour", "Déserts d'amour (1984-07-01)"); n = graph.getNode("Déserts d'amour"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d'amour (1984-07-01)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_460", "Déserts d’amour (1984)", "Déserts d'amour, extraits"); n = graph.getNode("Déserts d'amour, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_460", "Déserts d'amour, extraits", "Déserts d'amour (1984-07-01)"); n = graph.getNode("Déserts d'amour, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d'amour (1984-07-01)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_470", "Déserts d’amour (1984)", "Déserts d'amour, raccords ; solo de Catherine en studio"); n =
		 * graph.getNode("Déserts d'amour, raccords ; solo de Catherine en studio"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_470", "Déserts d'amour, raccords ; solo de Catherine en studio", "Déserts d'amour (1984-07-01)"); n =
		 * graph.getNode("Déserts d'amour, raccords ; solo de Catherine en studio"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d'amour (1984-07-01)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_or_480", "Fêtes Champêtres (1985)", "Fêtes champêtres"); n = graph.getNode("Fêtes champêtres"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Fêtes Champêtres (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_480", "Fêtes champêtres", "Fêtes champêtres (1985-06-24)"); n = graph.getNode("Fêtes champêtres"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Fêtes champêtres (1985-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_490", "Assaï (1986)", "Pièces 1985-1986, extraits"); n = graph.getNode("Pièces 1985-1986, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Assaï (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_491", "Crawl de Lucien (Le) (1985)", "Pièces 1985-1986, extraits"); n = graph.getNode("Pièces 1985-1986, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Crawl de Lucien (Le) (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_492", "Divertissement 138 (1985)", "Pièces 1985-1986, extraits"); n = graph.getNode("Pièces 1985-1986, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Divertissement 138 (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_493", "Fantasia Semplice (1986)", "Pièces 1985-1986, extraits"); n = graph.getNode("Pièces 1985-1986, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Fantasia Semplice (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_494", "Mes amis (1985)", "Pièces 1985-1986, extraits"); n = graph.getNode("Pièces 1985-1986, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Mes amis (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_495", "Suitte d’un goût étranger (1985)", "Pièces 1985-1986, extraits"); n = graph.getNode("Pièces 1985-1986, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suitte d’un goût étranger (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_490", "Pièces 1985-1986, extraits", "Assaï (1986-09-20)"); n = graph.getNode("Pièces 1985-1986, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Assaï (1986-09-20)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_491", "Pièces 1985-1986, extraits", "Crawl de Lucien (Le) (1985-07-09)"); n = graph.getNode("Pièces 1985-1986, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Crawl de Lucien (Le) (1985-07-09)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_492", "Pièces 1985-1986, extraits", "Divertissement 138 (1985-06-24)"); n = graph.getNode("Pièces 1985-1986, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Divertissement 138 (1985-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_493", "Pièces 1985-1986, extraits", "Fantasia semplice (1986-05-05)"); n = graph.getNode("Pièces 1985-1986, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Fantasia semplice (1986-05-05)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_494", "Pièces 1985-1986, extraits", "Mes amis (1985-01-03)"); n = graph.getNode("Pièces 1985-1986, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Mes amis (1985-01-03)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_495", "Pièces 1985-1986, extraits", "Suitte d'un goût étranger (1985-05-09)"); n = graph.getNode("Pièces 1985-1986, extraits"); n.setAttribute("ui.class", "doc"); n
		 * = graph.getNode("Suitte d'un goût étranger (1985-05-09)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_500", "Assaï (1986)", "Saut de l’Ange (Le), extraits (avec des extraits du Crawl de Lucien et d’Assaï)"); n =
		 * graph.getNode("Saut de l’Ange (Le), extraits (avec des extraits du Crawl de Lucien et d’Assaï)"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Assaï (1986)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_501", "Crawl de Lucien (Le) (1985)", "Saut de l’Ange (Le), extraits (avec des extraits du Crawl de Lucien et d’Assaï)"); n =
		 * graph.getNode("Saut de l’Ange (Le), extraits (avec des extraits du Crawl de Lucien et d’Assaï)"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Crawl de Lucien (Le) (1985)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_502", "Saut de l’ange (Le) (1987)", "Saut de l’Ange (Le), extraits (avec des extraits du Crawl de Lucien et d’Assaï)"); n =
		 * graph.getNode("Saut de l’Ange (Le), extraits (avec des extraits du Crawl de Lucien et d’Assaï)"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l’ange (Le) (1987)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_500", "Saut de l’Ange (Le), extraits (avec des extraits du Crawl de Lucien et d’Assaï)", "Assaï (1986-09-20)"); n =
		 * graph.getNode("Saut de l’Ange (Le), extraits (avec des extraits du Crawl de Lucien et d’Assaï)"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Assaï (1986-09-20)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_501", "Saut de l’Ange (Le), extraits (avec des extraits du Crawl de Lucien et d’Assaï)", "Crawl de Lucien (Le) (1985-07-09)"); n =
		 * graph.getNode("Saut de l’Ange (Le), extraits (avec des extraits du Crawl de Lucien et d’Assaï)"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Crawl de Lucien (Le) (1985-07-09)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_502", "Saut de l’Ange (Le), extraits (avec des extraits du Crawl de Lucien et d’Assaï)", "Saut de l'ange (Le) (1987-06-24)"); n =
		 * graph.getNode("Saut de l’Ange (Le), extraits (avec des extraits du Crawl de Lucien et d’Assaï)"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l'ange (Le) (1987-06-24)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_510", "Mes amis (1985)", "Mes amis"); n = graph.getNode("Mes amis"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Mes amis (1985)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_510", "Mes amis", "Mes amis (1985-01-03)"); n = graph.getNode("Mes amis"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Mes amis (1985-01-03)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_520", "Mes amis (1985)", "Mes amis, extraits"); n = graph.getNode("Mes amis, extraits"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Mes amis (1985)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_520", "Mes amis, extraits", "Mes amis (1985-01-03)"); n = graph.getNode("Mes amis, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Mes amis (1985-01-03)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_530", "Crawl de Lucien (Le) (1985)", "Crawl de Lucien (Le), images de création"); n = graph.getNode("Crawl de Lucien (Le), images de création");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Crawl de Lucien (Le) (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_530", "Crawl de Lucien (Le), images de création", "Crawl de Lucien (Le) (1985-07-09)"); n = graph.getNode("Crawl de Lucien (Le), images de création");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Crawl de Lucien (Le) (1985-07-09)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_540", "Suitte d’un goût étranger (1985)", "Suitte d'un goût étranger"); n = graph.getNode("Suitte d'un goût étranger"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suitte d’un goût étranger (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_540", "Suitte d'un goût étranger", "Suitte d'un goût étranger (1985-05-09)"); n = graph.getNode("Suitte d'un goût étranger"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suitte d'un goût étranger (1985-05-09)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_550", "Divertissement 138 (1985)", "Divertissement 138"); n = graph.getNode("Divertissement 138"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Divertissement 138 (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_551", "Scène rouge (1980)", "Divertissement 138"); n = graph.getNode("Divertissement 138"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Scène rouge (1980)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_550", "Divertissement 138", "Déserts d'amour (1984-07-01)"); n = graph.getNode("Divertissement 138"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d'amour (1984-07-01)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_551", "Divertissement 138", "Divertissement 138 (1985-06-24)"); n = graph.getNode("Divertissement 138"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Divertissement 138 (1985-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_552", "Divertissement 138", "Scène rouge (1980-12-06)"); n = graph.getNode("Divertissement 138"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Scène rouge (1980-12-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_560", "Crawl de Lucien (Le) (1985)", "Crawl de Lucien (Le)"); n = graph.getNode("Crawl de Lucien (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Crawl de Lucien (Le) (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_560", "Crawl de Lucien (Le)", "Crawl de Lucien (Le) (1985-07-09)"); n = graph.getNode("Crawl de Lucien (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Crawl de Lucien (Le) (1985-07-09)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_570", "Crawl de Lucien (Le) (1985)", "Crawl de Lucien (Le), extraits"); n = graph.getNode("Crawl de Lucien (Le), extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Crawl de Lucien (Le) (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_570", "Crawl de Lucien (Le), extraits", "Crawl de Lucien (Le) (1985-07-09)"); n = graph.getNode("Crawl de Lucien (Le), extraits"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Crawl de Lucien (Le) (1985-07-09)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_580", "Assaï (1986)", "Assaï, extraits travaux femis"); n = graph.getNode("Assaï, extraits travaux femis"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Assaï (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_580", "Assaï, extraits travaux femis", "Assaï (1986-09-20)"); n = graph.getNode("Assaï, extraits travaux femis"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Assaï (1986-09-20)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_600", "Fantasia Semplice (1986)", "Fantasia semplice"); n = graph.getNode("Fantasia semplice"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Fantasia Semplice (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_600", "Fantasia semplice", "Fantasia semplice (1986-05-05)"); n = graph.getNode("Fantasia semplice"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Fantasia semplice (1986-05-05)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_610", "Assaï (1986)", "Assaï"); n = graph.getNode("Assaï"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Assaï (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_610", "Assaï", "Assaï (1986-09-20)"); n = graph.getNode("Assaï"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Assaï (1986-09-20)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_620", "Assaï (1986)", "Assaï : entretiens Bagouet et Dusapin"); n = graph.getNode("Assaï : entretiens Bagouet et Dusapin"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Assaï (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_620", "Assaï : entretiens Bagouet et Dusapin", "Assaï (1986-09-20)"); n = graph.getNode("Assaï : entretiens Bagouet et Dusapin"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Assaï (1986-09-20)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_630", "Assaï (1986)", "Assaï, raccords (Haro 1)"); n = graph.getNode("Assaï, raccords (Haro 1)"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Assaï (1986)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_630", "Assaï, raccords (Haro 1)", "Assaï (1986-09-20)"); n = graph.getNode("Assaï, raccords (Haro 1)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Assaï (1986-09-20)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_640", "Assaï (1986)", "Lecture démonstration cie Bagouet 1986"); n = graph.getNode("Lecture démonstration cie Bagouet 1986"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Assaï (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_640", "Lecture démonstration cie Bagouet 1986", "Assaï (1986-09-20)"); n = graph.getNode("Lecture démonstration cie Bagouet 1986"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Assaï (1986-09-20)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_660", "Saut de l’ange (Le) (1987)", "Quelques mots sur Le Saut de l'ange"); n = graph.getNode("Quelques mots sur Le Saut de l'ange"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_660", "Quelques mots sur Le Saut de l'ange", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Quelques mots sur Le Saut de l'ange");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_670", "Assaï (1986)", "Assaï"); n = graph.getNode("Assaï"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Assaï (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_670", "Assaï", "Assaï (1986-09-20)"); n = graph.getNode("Assaï"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Assaï (1986-09-20)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_680", "Assaï (1986)", "Assaï, extraits"); n = graph.getNode("Assaï, extraits"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Assaï (1986)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_680", "Assaï, extraits", "Assaï (1986-09-20)"); n = graph.getNode("Assaï, extraits"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Assaï (1986-09-20)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_690", "Saut de l’ange (Le) (1987)", "Saut de l'ange (Le)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_690", "Saut de l'ange (Le)", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_700", "Saut de l’ange (Le) (1987)", "Saut de l'ange (Le)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_700", "Saut de l'ange (Le)", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_710", "Saut de l’ange (Le) (1987)", "Saut de l'ange (Le)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_710", "Saut de l'ange (Le)", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_720", "Saut de l’ange (Le) (1987)", "Saut de l'ange (Le)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_720", "Saut de l'ange (Le)", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_730", "Psyché (1987)", "Psyché"); n = graph.getNode("Psyché"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Psyché (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_730", "Psyché", "Psyché (1987-07-21)"); n = graph.getNode("Psyché"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Psyché (1987-07-21)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_740", "Psyché (1987)", "Psyché : journal télévisé"); n = graph.getNode("Psyché : journal télévisé"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Psyché (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_740", "Psyché : journal télévisé", "Psyché (1987-07-21)"); n = graph.getNode("Psyché : journal télévisé"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Psyché (1987-07-21)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_750", "Sous la blafarde (1979)", "Sous la blafarde, extrait : Pleure"); n = graph.getNode("Sous la blafarde, extrait : Pleure"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Sous la blafarde (1979)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_750", "Sous la blafarde, extrait : Pleure", "Sous la blafarde (1979-12-00)"); n = graph.getNode("Sous la blafarde, extrait : Pleure"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Sous la blafarde (1979-12-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_760", "Saut de l’ange (Le) (1987)", "Saut de l'ange (Le)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_760", "Saut de l'ange (Le)", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_770", "Necesito, pièce pour Grenade (1991)", "Compilation de journaux télévisés"); n = graph.getNode("Compilation de journaux télévisés"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_771", "Petites Pièces de Berlin (Les) (1988)", "Compilation de journaux télévisés"); n = graph.getNode("Compilation de journaux télévisés"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_772", "Saut de l’ange (Le) (1987)", "Compilation de journaux télévisés"); n = graph.getNode("Compilation de journaux télévisés"); n.setAttribute("ui.class", "doc"); n
		 * = graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_770", "Compilation de journaux télévisés", "Necesito (1991-07-26)"); n = graph.getNode("Compilation de journaux télévisés"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito (1991-07-26)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_771", "Compilation de journaux télévisés", "Petites Pièces de Berlin (Les) (1988-06-04)"); n = graph.getNode("Compilation de journaux télévisés");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988-06-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_772", "Compilation de journaux télévisés", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Compilation de journaux télévisés"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_780", "Dix anges, portraits (1988)", "Dix anges, portraits"); n = graph.getNode("Dix anges, portraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Dix anges, portraits (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_781", "Saut de l’ange (Le) (1987)", "Dix anges, portraits"); n = graph.getNode("Dix anges, portraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_780", "Dix anges, portraits", "Dix anges, portraits (1988-08-00)"); n = graph.getNode("Dix anges, portraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Dix anges, portraits (1988-08-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_781", "Dix anges, portraits", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Dix anges, portraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_790", "Saut de l’ange (Le) (1987)", "Saut de l'ange (Le)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_790", "Saut de l'ange (Le)", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_800", "Saut de l’ange (Le) (1987)", "Saut de l'ange (Le)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_800", "Saut de l'ange (Le)", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_810", "Saut de l’ange (Le) (1987)", "Saut de l'ange (Le)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_810", "Saut de l'ange (Le)", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_820", "Saut de l’ange (Le) (1987)", "Saut de l'ange (Le)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_820", "Saut de l'ange (Le)", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_830", "Saut de l’ange (Le) (1987)", "Saut de l'ange (Le), extraits"); n = graph.getNode("Saut de l'ange (Le), extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_830", "Saut de l'ange (Le), extraits", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Saut de l'ange (Le), extraits"); n.setAttribute("ui.class", "doc"); n
		 * = graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_840", "Petites Pièces de Berlin (Les) (1988)", "Petites Pièces de Berlin (Les), répétition et extraits"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les), répétition et extraits"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_840", "Petites Pièces de Berlin (Les), répétition et extraits", "Petites Pièces de Berlin (Les) (1988-06-04)"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les), répétition et extraits"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988-06-04)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_850", "Petites Pièces de Berlin (Les) (1988)", "Petites Pièces de Berlin (Les), répétitions"); n = graph.getNode("Petites Pièces de Berlin (Les), répétitions");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_850", "Petites Pièces de Berlin (Les), répétitions", "Petites Pièces de Berlin (Les) (1988-06-04)"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les), répétitions"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988-06-04)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_or_860", "Petites Pièces de Berlin (Les) (1988)", "Petites Pièces de Berlin (Les) - spectacle Berlin"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les) - spectacle Berlin"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_fchor_860", "Petites Pièces de Berlin (Les) - spectacle Berlin", "Petites Pièces de Berlin (Les) (1988-06-04)"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les) - spectacle Berlin"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988-06-04)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_880", "Petites Pièces de Berlin (Les) (1988)", "Petites Pièces de Berlin (Les)"); n = graph.getNode("Petites Pièces de Berlin (Les)"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_880", "Petites Pièces de Berlin (Les)", "Petites Pièces de Berlin (Les) (1988-06-04)"); n = graph.getNode("Petites Pièces de Berlin (Les)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988-06-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_890", "Petites Pièces de Berlin (Les) (1988)", "Petites Pièces de Berlin (Les)"); n = graph.getNode("Petites Pièces de Berlin (Les)"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_890", "Petites Pièces de Berlin (Les)", "Petites Pièces de Berlin (Les) (1988-06-04)"); n = graph.getNode("Petites Pièces de Berlin (Les)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988-06-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_900", "Petites Pièces de Berlin (Les) (1988)", "Petites Pièces de Berlin (Les)"); n = graph.getNode("Petites Pièces de Berlin (Les)"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_900", "Petites Pièces de Berlin (Les)", "Petites Pièces de Berlin (Les) (1988-06-04)"); n = graph.getNode("Petites Pièces de Berlin (Les)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988-06-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_910", "Petites Pièces de Berlin (Les) (1988)", "Petites Pièces de Berlin (Les), extraits"); n = graph.getNode("Petites Pièces de Berlin (Les), extraits");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_910", "Petites Pièces de Berlin (Les), extraits", "Petites Pièces de Berlin (Les) (1988-06-04)"); n = graph.getNode("Petites Pièces de Berlin (Les), extraits");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988-06-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_920", "Petites Pièces de Berlin (Les) (1988)", "Petites Pièces de Berlin (Les)"); n = graph.getNode("Petites Pièces de Berlin (Les)"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_920", "Petites Pièces de Berlin (Les)", "Petites Pièces de Berlin (Les) (1988-06-04)"); n = graph.getNode("Petites Pièces de Berlin (Les)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988-06-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_930", "Déserts et Crawl (1988)", "Déserts et Crawl"); n = graph.getNode("Déserts et Crawl"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts et Crawl (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_930", "Déserts et Crawl", "Déserts et Crawl (1988-09-29)"); n = graph.getNode("Déserts et Crawl"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts et Crawl (1988-09-29)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_940", "Demain la veille-spécial Bagouet", "Petites Pièces de Berlin (Les) (1988-06-04)"); n = graph.getNode("Demain la veille-spécial Bagouet");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988-06-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_941", "Demain la veille-spécial Bagouet", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Demain la veille-spécial Bagouet"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_950", "Meublé sommairement (1989)", "Meublé sommairement / Necesito, extraits"); n = graph.getNode("Meublé sommairement / Necesito, extraits");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_951", "Necesito, pièce pour Grenade (1991)", "Meublé sommairement / Necesito, extraits"); n = graph.getNode("Meublé sommairement / Necesito, extraits");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_950", "Meublé sommairement / Necesito, extraits", "Meublé sommairement (1989-07-10)"); n = graph.getNode("Meublé sommairement / Necesito, extraits");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989-07-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_951", "Meublé sommairement / Necesito, extraits", "Necesito (1991-07-26)"); n = graph.getNode("Meublé sommairement / Necesito, extraits");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito (1991-07-26)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_960", "Meublé sommairement (1989)", "Meublé sommairement, bal du bout du monde"); n = graph.getNode("Meublé sommairement, bal du bout du monde");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_960", "Meublé sommairement, bal du bout du monde", "Meublé sommairement (1989-07-10)"); n = graph.getNode("Meublé sommairement, bal du bout du monde");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989-07-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_970", "Roi des Bons (Le) (1989)", "Roi des Bons (Le) - début K 1 à 5"); n = graph.getNode("Roi des Bons (Le) - début K 1 à 5"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Roi des Bons (Le) (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_970", "Roi des Bons (Le) - début K 1 à 5", "Roi des Bons (Le) (1989-03-02)"); n = graph.getNode("Roi des Bons (Le) - début K 1 à 5"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Roi des Bons (Le) (1989-03-02)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_980", "Roi des Bons (Le) (1989)", "Roi des bons (Le) - fin K6"); n = graph.getNode("Roi des bons (Le) - fin K6"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Roi des Bons (Le) (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_980", "Roi des bons (Le) - fin K6", "Roi des Bons (Le) (1989-03-02)"); n = graph.getNode("Roi des bons (Le) - fin K6"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Roi des Bons (Le) (1989-03-02)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_990", "Roi des Bons (Le) (1989)", "Roi des bons (Le), extraits"); n = graph.getNode("Roi des bons (Le), extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Roi des Bons (Le) (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_990", "Roi des bons (Le), extraits", "Roi des Bons (Le) (1989-03-02)"); n = graph.getNode("Roi des bons (Le), extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Roi des Bons (Le) (1989-03-02)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1000", "Meublé sommairement (1989)", "Meublé sommairement, répétitions"); n = graph.getNode("Meublé sommairement, répétitions"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1000", "Meublé sommairement, répétitions", "Meublé sommairement (1989-07-10)"); n = graph.getNode("Meublé sommairement, répétitions"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Meublé sommairement (1989-07-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1010", "Meublé sommairement (1989)", "Meublé sommairement, répétitions"); n = graph.getNode("Meublé sommairement, répétitions"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1010", "Meublé sommairement, répétitions", "Meublé sommairement (1989-07-10)"); n = graph.getNode("Meublé sommairement, répétitions"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Meublé sommairement (1989-07-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1020", "Meublé sommairement (1989)", "Meublé sommairement"); n = graph.getNode("Meublé sommairement"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1020", "Meublé sommairement", "Meublé sommairement (1989-07-10)"); n = graph.getNode("Meublé sommairement"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989-07-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1030", "Meublé sommairement (1989)", "Meublé sommairement"); n = graph.getNode("Meublé sommairement"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1030", "Meublé sommairement", "Meublé sommairement (1989-07-10)"); n = graph.getNode("Meublé sommairement"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989-07-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1040", "Meublé sommairement (1989)", "Meublé sommairement"); n = graph.getNode("Meublé sommairement"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1040", "Meublé sommairement", "Meublé sommairement (1989-07-10)"); n = graph.getNode("Meublé sommairement"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989-07-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1050", "Meublé sommairement (1989)", "Meublé sommairement"); n = graph.getNode("Meublé sommairement"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1050", "Meublé sommairement", "Meublé sommairement (1989-07-10)"); n = graph.getNode("Meublé sommairement"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989-07-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1060", "Meublé sommairement (1989)", "Meublé sommairement, extraits"); n = graph.getNode("Meublé sommairement, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1060", "Meublé sommairement, extraits", "Meublé sommairement (1989-07-10)"); n = graph.getNode("Meublé sommairement, extraits"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Meublé sommairement (1989-07-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1070", "Saut de l’ange (Le) (1987)", "Saut de l'ange (Le), filage studio"); n = graph.getNode("Saut de l'ange (Le), filage studio"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1070", "Saut de l'ange (Le), filage studio", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Saut de l'ange (Le), filage studio");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1080", "Petites Pièces de Berlin (Les) (1988)", "Petites Pièces de Berlin (Les)"); n = graph.getNode("Petites Pièces de Berlin (Les)"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1080", "Petites Pièces de Berlin (Les)", "Petites Pièces de Berlin (Les) (1988-06-04)"); n = graph.getNode("Petites Pièces de Berlin (Les)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988-06-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1090", "Saut de l’ange (Le) (1987)", "Saut de l'ange (Le), répétition"); n = graph.getNode("Saut de l'ange (Le), répétition"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1090", "Saut de l'ange (Le), répétition", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Saut de l'ange (Le), répétition"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1100", "Roi des Bons (Le) (1989)", "Roi des Bons (Le)"); n = graph.getNode("Roi des Bons (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Roi des Bons (Le) (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1100", "Roi des Bons (Le)", "Roi des Bons (Le) (1989-03-02)"); n = graph.getNode("Roi des Bons (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Roi des Bons (Le) (1989-03-02)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1110", "Meublé sommairement (1989)", "Meublé sommairement, le bal"); n = graph.getNode("Meublé sommairement, le bal"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1110", "Meublé sommairement, le bal", "Meublé sommairement (1989-07-10)"); n = graph.getNode("Meublé sommairement, le bal"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989-07-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1120", "Saut de l’ange (Le) (1987)", "Saut de l'ange (Le), extraits, Villetaneuse"); n = graph.getNode("Saut de l'ange (Le), extraits, Villetaneuse");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1120", "Saut de l'ange (Le), extraits, Villetaneuse", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Saut de l'ange (Le), extraits, Villetaneuse");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1130", "Peau du personnage (La) (1990)", "Peau du personnage (La)"); n = graph.getNode("Peau du personnage (La)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Peau du personnage (La) (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1130", "Peau du personnage (La)", "Peau du personnage (La) (1990-07-11)"); n = graph.getNode("Peau du personnage (La)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Peau du personnage (La) (1990-07-11)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1140", "Trois quart dos (1990)", "Trois quart do"); n = graph.getNode("Trois quart do"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Trois quart dos (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1140", "Trois quart do", "Trois quart dos (1990-07-04)"); n = graph.getNode("Trois quart do"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Trois quart dos (1990-07-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1150", "Volare (1990)", "Volare"); n = graph.getNode("Volare"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Volare (1990)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_fchor_1150", "Volare", "Volare (1990-07-11)"); n = graph.getNode("Volare"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Volare (1990-07-11)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1170", "Jours étranges (1990)", "Jours étranges, répétitions"); n = graph.getNode("Jours étranges, répétitions"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1170", "Jours étranges, répétitions", "Jours étranges (1990-07-04)"); n = graph.getNode("Jours étranges, répétitions"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990-07-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1180", "So Schnell (1990)", "Déserts d'amour (duo), répétitions"); n = graph.getNode("Déserts d'amour (duo), répétitions"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1180", "Déserts d'amour (duo), répétitions", "So Schnell (1991-01-26)"); n = graph.getNode("Déserts d'amour (duo), répétitions"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("So Schnell (1991-01-26)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_1181", "Déserts d'amour (duo), répétitions", "So Schnell, version 92 (1992-10-11)"); n = graph.getNode("Déserts d'amour (duo), répétitions");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell, version 92 (1992-10-11)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1190", "Jours étranges (1990)", "Jours étranges, répétitions"); n = graph.getNode("Jours étranges, répétitions"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1190", "Jours étranges, répétitions", "Jours étranges (1990-07-04)"); n = graph.getNode("Jours étranges, répétitions"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990-07-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1200", "Jours étranges (1990)", "Jours étranges, répétitions"); n = graph.getNode("Jours étranges, répétitions"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1200", "Jours étranges, répétitions", "Jours étranges (1990-07-04)"); n = graph.getNode("Jours étranges, répétitions"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990-07-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1210", "Année lumière (1990)", "Année lumière, répétition et spectacle"); n = graph.getNode("Année lumière, répétition et spectacle"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Année lumière (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1210", "Année lumière, répétition et spectacle", "Année lumière (1990-07-11)"); n = graph.getNode("Année lumière, répétition et spectacle");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Année lumière (1990-07-11)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1220", "Meublé sommairement (1989)", "Meublé sommairement"); n = graph.getNode("Meublé sommairement"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1220", "Meublé sommairement", "Meublé sommairement (1989-07-10)"); n = graph.getNode("Meublé sommairement"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989-07-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1240", "Necesito, pièce pour Grenade (1991)", "Entretien de Bagouet sur Cunningham ; Service compris"); n =
		 * graph.getNode("Entretien de Bagouet sur Cunningham ; Service compris"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito, pièce pour Grenade (1991)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1241", "Petites Pièces de Berlin (Les) (1988)", "Entretien de Bagouet sur Cunningham ; Service compris"); n =
		 * graph.getNode("Entretien de Bagouet sur Cunningham ; Service compris"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1242", "Suitte d’un goût étranger (1985)", "Entretien de Bagouet sur Cunningham ; Service compris"); n =
		 * graph.getNode("Entretien de Bagouet sur Cunningham ; Service compris"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Suitte d’un goût étranger (1985)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_or_1250", "Meublé sommairement (1989)", "Meublé sommairement, Besançon"); n = graph.getNode("Meublé sommairement, Besançon"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1250", "Meublé sommairement, Besançon", "Meublé sommairement (1989-07-10)"); n = graph.getNode("Meublé sommairement, Besançon"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Meublé sommairement (1989-07-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1260", "Jours étranges (1990)", "Jours étranges"); n = graph.getNode("Jours étranges"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Jours étranges (1990)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1260", "Jours étranges", "Jours étranges (1990-07-04)"); n = graph.getNode("Jours étranges"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990-07-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1270", "So Schnell (1990)", "So Schnell 1990, répétitions"); n = graph.getNode("So Schnell 1990, répétitions"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1270", "So Schnell 1990, répétitions", "So Schnell (1990-12-06)"); n = graph.getNode("So Schnell 1990, répétitions"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990-12-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1280", "So Schnell (1990)", "So Schnell, 1re version"); n = graph.getNode("So Schnell, 1re version"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1280", "So Schnell, 1re version", "So Schnell (1990-12-06)"); n = graph.getNode("So Schnell, 1re version"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990-12-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1290", "Commando Ursula (1991)", "Commando Ursula 1, pour 3 danseurs"); n = graph.getNode("Commando Ursula 1, pour 3 danseurs"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Commando Ursula (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1290", "Commando Ursula 1, pour 3 danseurs", "Commando Ursula 1, pour 3 danseurs (1991-01-17)"); n = graph.getNode("Commando Ursula 1, pour 3 danseurs");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Commando Ursula 1, pour 3 danseurs (1991-01-17)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1300", "Nénuphars (1991)", "Nénuphars"); n = graph.getNode("Nénuphars"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Nénuphars (1991)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1300", "Nénuphars", "Nénuphars (1991-02-25)"); n = graph.getNode("Nénuphars"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Nénuphars (1991-02-25)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1310", "Volaille est à l'intérieur (La) (1991)", "Volaille est à l'intérieur (La)"); n = graph.getNode("Volaille est à l'intérieur (La)"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Volaille est à l'intérieur (La) (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1310", "Volaille est à l'intérieur (La)", "Volaille est à l'intérieur (La) (1991-02-26)"); n = graph.getNode("Volaille est à l'intérieur (La)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Volaille est à l'intérieur (La) (1991-02-26)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1320", "So Schnell (1990)", "So schnell version 1990, Alès"); n = graph.getNode("So schnell version 1990, Alès"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1320", "So schnell version 1990, Alès", "So Schnell (1990-12-06)"); n = graph.getNode("So schnell version 1990, Alès"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990-12-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_1321", "So schnell version 1990, Alès", "So Schnell (1991-01-26)"); n = graph.getNode("So schnell version 1990, Alès"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1991-01-26)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1330", "Commando Ursula (1991)", "Commando Ursula 2"); n = graph.getNode("Commando Ursula 2"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Commando Ursula (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1330", "Commando Ursula 2", "Commando Ursula 2, pour 9 danseurs (1991-02-27)"); n = graph.getNode("Commando Ursula 2"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Commando Ursula 2, pour 9 danseurs (1991-02-27)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1340", "Necesito, pièce pour Grenade (1991)", "Necesito, répétitions mai-juin 1991"); n = graph.getNode("Necesito, répétitions mai-juin 1991");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1340", "Necesito, répétitions mai-juin 1991", "Necesito (1991-07-26)"); n = graph.getNode("Necesito, répétitions mai-juin 1991"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Necesito (1991-07-26)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1350", "Necesito, pièce pour Grenade (1991)", "Necesito, répétitions mai 1991"); n = graph.getNode("Necesito, répétitions mai 1991"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1350", "Necesito, répétitions mai 1991", "Necesito (1991-07-26)"); n = graph.getNode("Necesito, répétitions mai 1991"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito (1991-07-26)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1360", "Etendue (1991)", "Etendue"); n = graph.getNode("Etendue"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Etendue (1991)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_fchor_1360", "Etendue", "Étendue (1991-05-28)"); n = graph.getNode("Etendue"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Étendue (1991-05-28)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1370", "Necesito, pièce pour Grenade (1991)", "Service compris spécial Bagouet"); n = graph.getNode("Service compris spécial Bagouet"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1370", "Service compris spécial Bagouet", "Necesito (1991-07-26)"); n = graph.getNode("Service compris spécial Bagouet"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito (1991-07-26)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1380", "Necesito, pièce pour Grenade (1991)", "Entretiens Anne Abeille et Dominique Bagouet sur Necesito"); n =
		 * graph.getNode("Entretiens Anne Abeille et Dominique Bagouet sur Necesito"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito, pièce pour Grenade (1991)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1380", "Entretiens Anne Abeille et Dominique Bagouet sur Necesito", "Necesito (1991-07-26)"); n =
		 * graph.getNode("Entretiens Anne Abeille et Dominique Bagouet sur Necesito"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito (1991-07-26)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_or_1390", "Necesito, pièce pour Grenade (1991)", "Documentaire sur Necesito, pièce pour Grenade"); n = graph.getNode("Documentaire sur Necesito, pièce pour Grenade");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1390", "Documentaire sur Necesito, pièce pour Grenade", "Necesito (1991-07-26)"); n = graph.getNode("Documentaire sur Necesito, pièce pour Grenade");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito (1991-07-26)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1400", "Necesito, pièce pour Grenade (1991)", "Necesito, Montpellier"); n = graph.getNode("Necesito, Montpellier"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1400", "Necesito, Montpellier", "Necesito (1991-07-26)"); n = graph.getNode("Necesito, Montpellier"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito (1991-07-26)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1410", "Necesito, pièce pour Grenade (1991)", "Necesito, Grenoble"); n = graph.getNode("Necesito, Grenoble"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1410", "Necesito, Grenoble", "Necesito (1991-07-26)"); n = graph.getNode("Necesito, Grenoble"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito (1991-07-26)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1420", "Meublé sommairement (1989)", "Meublé sommairement, le bal, répétitions"); n = graph.getNode("Meublé sommairement, le bal, répétitions");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1420", "Meublé sommairement, le bal, répétitions", "Meublé sommairement (1989-07-10)"); n = graph.getNode("Meublé sommairement, le bal, répétitions");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989-07-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1440", "Necesito, pièce pour Grenade (1991)", "Necesito, spectacle Paris"); n = graph.getNode("Necesito, spectacle Paris"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1440", "Necesito, spectacle Paris", "Necesito (1991-07-26)"); n = graph.getNode("Necesito, spectacle Paris"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito (1991-07-26)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1450", "One Story as in Falling (1992)", "One Story as in Falling, répétitions"); n = graph.getNode("One Story as in Falling, répétitions"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("One Story as in Falling (1992)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1450", "One Story as in Falling, répétitions", "One Story as in Falling (1992-06-25)"); n = graph.getNode("One Story as in Falling, répétitions");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("One Story as in Falling (1992-06-25)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1460", "Necesito, pièce pour Grenade (1991)", "Necesito"); n = graph.getNode("Necesito"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1460", "Necesito", "Necesito (1991-07-26)"); n = graph.getNode("Necesito"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito (1991-07-26)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1470", "Necesito, pièce pour Grenade (1991)", "Necesito"); n = graph.getNode("Necesito"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1470", "Necesito", "Necesito (1991-07-26)"); n = graph.getNode("Necesito"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito (1991-07-26)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1490", "Necesito, pièce pour Grenade (1991)", "Lecture démonstration"); n = graph.getNode("Lecture démonstration"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1491", "So Schnell (1990)", "Lecture démonstration"); n = graph.getNode("Lecture démonstration"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1500", "Fêtes Champêtres (1985)", "In memoriam Dominique Bagouet"); n = graph.getNode("In memoriam Dominique Bagouet"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Fêtes Champêtres (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1501", "Petites Pièces de Berlin (Les) (1988)", "In memoriam Dominique Bagouet"); n = graph.getNode("In memoriam Dominique Bagouet"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1502", "Suitte d’un goût étranger (1985)", "In memoriam Dominique Bagouet"); n = graph.getNode("In memoriam Dominique Bagouet"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suitte d’un goût étranger (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1510", "One Story as in Falling (1992)", "One Story as in Falling"); n = graph.getNode("One Story as in Falling"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("One Story as in Falling (1992)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1510", "One Story as in Falling", "One Story as in Falling (1992-06-25)"); n = graph.getNode("One Story as in Falling"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("One Story as in Falling (1992-06-25)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1520", "One Story as in Falling (1992)", "Conférence de presse saison 1992/1993"); n = graph.getNode("Conférence de presse saison 1992/1993");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("One Story as in Falling (1992)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1520", "Conférence de presse saison 1992/1993", "One Story as in Falling (1992-06-25)"); n = graph.getNode("Conférence de presse saison 1992/1993");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("One Story as in Falling (1992-06-25)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1530", "So Schnell (1990)", "So schnell version 1992, Grenoble"); n = graph.getNode("So schnell version 1992, Grenoble"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1530", "So schnell version 1992, Grenoble", "So Schnell, version 92 (1992-10-11)"); n = graph.getNode("So schnell version 1992, Grenoble");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell, version 92 (1992-10-11)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1540", "So Schnell (1990)", "So Schnell"); n = graph.getNode("So Schnell"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1540", "So Schnell", "So Schnell, version 92 (1992-10-11)"); n = graph.getNode("So Schnell"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell, version 92 (1992-10-11)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1550", "Meublé sommairement (1989)", "Une journée avec les danseurs de Bagouet"); n = graph.getNode("Une journée avec les danseurs de Bagouet");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1551", "Necesito, pièce pour Grenade (1991)", "Une journée avec les danseurs de Bagouet"); n = graph.getNode("Une journée avec les danseurs de Bagouet");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1552", "Saut de l’ange (Le) (1987)", "Une journée avec les danseurs de Bagouet"); n = graph.getNode("Une journée avec les danseurs de Bagouet");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1560", "One Story as in Falling (1992)", "One Story as in Falling"); n = graph.getNode("One Story as in Falling"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("One Story as in Falling (1992)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1560", "One Story as in Falling", "One Story as in Falling (1992-06-25)"); n = graph.getNode("One Story as in Falling"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("One Story as in Falling (1992-06-25)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1570", "So Schnell (1990)", "So Schnell"); n = graph.getNode("So Schnell"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1570", "So Schnell", "So Schnell, version 92 (1992-10-11)"); n = graph.getNode("So Schnell"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell, version 92 (1992-10-11)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1580", "So Schnell (1990)", "So Schnell, répétition générale"); n = graph.getNode("So Schnell, répétition générale"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1580", "So Schnell, répétition générale", "So Schnell, version 92 (1992-10-11)"); n = graph.getNode("So Schnell, répétition générale"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("So Schnell, version 92 (1992-10-11)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1600", "Meublé sommairement (1989)", "Rencontre avec le public, décembre 92"); n = graph.getNode("Rencontre avec le public, décembre 92"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1601", "Necesito, pièce pour Grenade (1991)", "Rencontre avec le public, décembre 92"); n = graph.getNode("Rencontre avec le public, décembre 92");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1602", "Petites Pièces de Berlin (Les) (1988)", "Rencontre avec le public, décembre 92"); n = graph.getNode("Rencontre avec le public, décembre 92");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1603", "Saut de l’ange (Le) (1987)", "Rencontre avec le public, décembre 92"); n = graph.getNode("Rencontre avec le public, décembre 92"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1610", "Necesito, pièce pour Grenade (1991)", "Petites Pièces de Grenade (Les)"); n = graph.getNode("Petites Pièces de Grenade (Les)"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1610", "Petites Pièces de Grenade (Les)", "Necesito (1991-07-26)"); n = graph.getNode("Petites Pièces de Grenade (Les)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito (1991-07-26)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_1611", "Petites Pièces de Grenade (Les)", "Petites Pièces de Berlin (Les) (1988-06-04)"); n = graph.getNode("Petites Pièces de Grenade (Les)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988-06-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1620", "Saut de l’ange (Le) (1987)", "Compagnie Bagouet, émissions de TV"); n = graph.getNode("Compagnie Bagouet, émissions de TV"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1620", "Compagnie Bagouet, émissions de TV", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Compagnie Bagouet, émissions de TV");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1630", "Saut de l’ange (Le) (1987)", "Montpellier, Le Saut de l'ange"); n = graph.getNode("Montpellier, Le Saut de l'ange"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1630", "Montpellier, Le Saut de l'ange", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Montpellier, Le Saut de l'ange"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1640", "Mes amis (1985)", "Conférence de Laurence Louppe : La Parole et la danse"); n = graph.getNode("Conférence de Laurence Louppe : La Parole et la danse");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Mes amis (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1641", "Meublé sommairement (1989)", "Conférence de Laurence Louppe : La Parole et la danse"); n =
		 * graph.getNode("Conférence de Laurence Louppe : La Parole et la danse"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1640", "Conférence de Laurence Louppe : La Parole et la danse", "Meublé sommairement (1989-07-10)"); n =
		 * graph.getNode("Conférence de Laurence Louppe : La Parole et la danse"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989-07-10)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_fchor_1641", "Conférence de Laurence Louppe : La Parole et la danse", "Saut de l'ange (Le) (1987-06-24)"); n =
		 * graph.getNode("Conférence de Laurence Louppe : La Parole et la danse"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_fchor_1642", "Conférence de Laurence Louppe : La Parole et la danse", "Saut de l'ange (Le) (1999-04-08)"); n =
		 * graph.getNode("Conférence de Laurence Louppe : La Parole et la danse"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l'ange (Le) (1999-04-08)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_or_1650", "Meublé sommairement (1989)", "Rencontre publique CIP 1992-1993"); n = graph.getNode("Rencontre publique CIP 1992-1993"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1651", "So Schnell (1990)", "Rencontre publique CIP 1992-1993"); n = graph.getNode("Rencontre publique CIP 1992-1993"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1650", "Rencontre publique CIP 1992-1993", "Meublé sommairement (1989-07-10)"); n = graph.getNode("Rencontre publique CIP 1992-1993"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Meublé sommairement (1989-07-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_1651", "Rencontre publique CIP 1992-1993", "So Schnell, version 92 (1992-10-11)"); n = graph.getNode("Rencontre publique CIP 1992-1993"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("So Schnell, version 92 (1992-10-11)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1660", "Petites Pièces de Berlin (Les) (1988)", "Petites Pièces de Berlin (Les), 2e nonette"); n = graph.getNode("Petites Pièces de Berlin (Les), 2e nonette");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1660", "Petites Pièces de Berlin (Les), 2e nonette", "Petites Pièces de Berlin (Les) (1993-02-16)"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les), 2e nonette"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1993-02-16)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_or_1690", "Sentiers (1993)", "Sentiers, répétition et filage en public"); n = graph.getNode("Sentiers, répétition et filage en public"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Sentiers (1993)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1690", "Sentiers, répétition et filage en public", "Sentiers (1993-06-29)"); n = graph.getNode("Sentiers, répétition et filage en public");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Sentiers (1993-06-29)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1700", "Saut de l’ange (Le) (1987)", "Saut de l'ange (Le)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1700", "Saut de l'ange (Le)", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1710", "Saut de l’ange (Le) (1987)", "Saut de l'ange (Le)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1710", "Saut de l'ange (Le)", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Saut de l'ange (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1720", "Saut de l’ange (Le) (1987)", "Saut de l'ange (Le) 1993, extraits"); n = graph.getNode("Saut de l'ange (Le) 1993, extraits"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1720", "Saut de l'ange (Le) 1993, extraits", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Saut de l'ange (Le) 1993, extraits");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1730", "Jours étranges (1990)", "Jours étranges"); n = graph.getNode("Jours étranges"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Jours étranges (1990)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1730", "Jours étranges", "Jours étranges (1990-07-04)"); n = graph.getNode("Jours étranges"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990-07-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1740", "Jours étranges (1990)", "Jours étranges"); n = graph.getNode("Jours étranges"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Jours étranges (1990)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1740", "Jours étranges", "Jours étranges (1990-07-04)"); n = graph.getNode("Jours étranges"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990-07-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1750", "Necesito, pièce pour Grenade (1991)", "Necesito"); n = graph.getNode("Necesito"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1750", "Necesito", "Necesito (1991-07-26)"); n = graph.getNode("Necesito"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito (1991-07-26)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1770", "So Schnell (1990)", "So Schnell"); n = graph.getNode("So Schnell"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1770", "So Schnell", "So Schnell, version 92 (1992-10-11)"); n = graph.getNode("So Schnell"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell, version 92 (1992-10-11)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1780", "Assaï (1986)", "Zoulous, pingouins et autres indiens"); n = graph.getNode("Zoulous, pingouins et autres indiens"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Assaï (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1781", "Déserts d’amour (1984)", "Zoulous, pingouins et autres indiens"); n = graph.getNode("Zoulous, pingouins et autres indiens"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1782", "Jours étranges (1990)", "Zoulous, pingouins et autres indiens"); n = graph.getNode("Zoulous, pingouins et autres indiens"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1783", "Petites Pièces de Berlin (Les) (1988)", "Zoulous, pingouins et autres indiens"); n = graph.getNode("Zoulous, pingouins et autres indiens");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1784", "Zoulous, pingouins et autres indiens (1993)", "Zoulous, pingouins et autres indiens"); n = graph.getNode("Zoulous, pingouins et autres indiens");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Zoulous, pingouins et autres indiens (1993)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1780", "Zoulous, pingouins et autres indiens", "Zoulous, pingouins et autres indiens (1993-07-06)"); n = graph.getNode("Zoulous, pingouins et autres indiens");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Zoulous, pingouins et autres indiens (1993-07-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1790", "Meublé sommairement (1989)", "Soirée Vidéo Danse"); n = graph.getNode("Soirée Vidéo Danse"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1791", "Necesito, pièce pour Grenade (1991)", "Soirée Vidéo Danse"); n = graph.getNode("Soirée Vidéo Danse"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1792", "So Schnell (1990)", "Soirée Vidéo Danse"); n = graph.getNode("Soirée Vidéo Danse"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1793", "Soirée Video Danse (1994)", "Soirée Vidéo Danse"); n = graph.getNode("Soirée Vidéo Danse"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Soirée Video Danse (1994)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1790", "Soirée Vidéo Danse", "Soirée Vidéo Danse (1994-07-28)"); n = graph.getNode("Soirée Vidéo Danse"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Soirée Vidéo Danse (1994-07-28)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1800", "Assaï (1986)", "Carnets Bagouet (Les), émission sur Assaï"); n = graph.getNode("Carnets Bagouet (Les), émission sur Assaï"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Assaï (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1800", "Carnets Bagouet (Les), émission sur Assaï", "Assaï, version 1995 (1995-11-08)"); n = graph.getNode("Carnets Bagouet (Les), émission sur Assaï");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Assaï, version 1995 (1995-11-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1810", "Déserts d’amour (1984)", "Déserts d'amour, reprise 1995, extraits"); n = graph.getNode("Déserts d'amour, reprise 1995, extraits"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1810", "Déserts d'amour, reprise 1995, extraits", "Déserts d'amour, reprise 1995 (1996-06-21)"); n = graph.getNode("Déserts d'amour, reprise 1995, extraits");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d'amour, reprise 1995 (1996-06-21)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1820", "Sous la blafarde (1979)", "Sous la blafarde, reprise 1995"); n = graph.getNode("Sous la blafarde, reprise 1995"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Sous la blafarde (1979)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1820", "Sous la blafarde, reprise 1995", "Sous la blafarde, version 1995 (1995-03-27)"); n = graph.getNode("Sous la blafarde, reprise 1995");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Sous la blafarde, version 1995 (1995-03-27)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1830", "Sous la blafarde (1979)", "Sous la blafarde, reprise 1995"); n = graph.getNode("Sous la blafarde, reprise 1995"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Sous la blafarde (1979)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1830", "Sous la blafarde, reprise 1995", "Sous la blafarde, version 1995 (1995-03-27)"); n = graph.getNode("Sous la blafarde, reprise 1995");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Sous la blafarde, version 1995 (1995-03-27)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1840", "Assaï (1986)", "Assaï - mémoire Dieppe 1/2"); n = graph.getNode("Assaï - mémoire Dieppe 1/2"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Assaï (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1840", "Assaï - mémoire Dieppe 1/2", "Assaï, version 1995 (1995-11-08)"); n = graph.getNode("Assaï - mémoire Dieppe 1/2"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Assaï, version 1995 (1995-11-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1850", "Assaï (1986)", "Assaï - mémoire Dieppe 2/2"); n = graph.getNode("Assaï - mémoire Dieppe 2/2"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Assaï (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1850", "Assaï - mémoire Dieppe 2/2", "Assaï, version 1995 (1995-11-08)"); n = graph.getNode("Assaï - mémoire Dieppe 2/2"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Assaï, version 1995 (1995-11-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1860", "Assaï (1986)", "Assaï - mémoire Dieppe studio"); n = graph.getNode("Assaï - mémoire Dieppe studio"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Assaï (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1860", "Assaï - mémoire Dieppe studio", "Assaï, version 1995 (1995-11-08)"); n = graph.getNode("Assaï - mémoire Dieppe studio"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Assaï, version 1995 (1995-11-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1870", "Assaï (1986)", "Assaï, reprise 95, répétition générale"); n = graph.getNode("Assaï, reprise 95, répétition générale"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Assaï (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1870", "Assaï, reprise 95, répétition générale", "Assaï, version 1995 (1995-11-08)"); n = graph.getNode("Assaï, reprise 95, répétition générale");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Assaï, version 1995 (1995-11-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1880", "Assaï (1986)", "Assaï, reprise 1995, Lyon, 2e distribution"); n = graph.getNode("Assaï, reprise 1995, Lyon, 2e distribution"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Assaï (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1880", "Assaï, reprise 1995, Lyon, 2e distribution", "Assaï, version 1995 (1995-11-08)"); n = graph.getNode("Assaï, reprise 1995, Lyon, 2e distribution");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Assaï, version 1995 (1995-11-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1890", "Assaï (1986)", "Assaï, reprise 1995, Lyon, 1re distribution"); n = graph.getNode("Assaï, reprise 1995, Lyon, 1re distribution"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Assaï (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1890", "Assaï, reprise 1995, Lyon, 1re distribution", "Assaï, version 1995 (1995-11-08)"); n = graph.getNode("Assaï, reprise 1995, Lyon, 1re distribution");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Assaï, version 1995 (1995-11-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1900", "Voyage organisé (1977)", "Voyage organisé, reprise JBF"); n = graph.getNode("Voyage organisé, reprise JBF"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Voyage organisé (1977)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1900", "Voyage organisé, reprise JBF", "Voyage organisé, nouvelle version (1995-09-20)"); n = graph.getNode("Voyage organisé, reprise JBF");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Voyage organisé, nouvelle version (1995-09-20)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1910", "Dix anges, portraits (1988)", "À propos de Dominique Bagouet, Boltanski"); n = graph.getNode("À propos de Dominique Bagouet, Boltanski");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Dix anges, portraits (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1911", "Saut de l’ange (Le) (1987)", "À propos de Dominique Bagouet, Boltanski"); n = graph.getNode("À propos de Dominique Bagouet, Boltanski");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1910", "À propos de Dominique Bagouet, Boltanski", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("À propos de Dominique Bagouet, Boltanski");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1930", "Déserts d’amour (1984)", "Déserts d'amour, reprise 1995"); n = graph.getNode("Déserts d'amour, reprise 1995"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1930", "Déserts d'amour, reprise 1995", "Déserts d'amour, reprise 1995 (1996-06-21)"); n = graph.getNode("Déserts d'amour, reprise 1995");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d'amour, reprise 1995 (1996-06-21)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1940", "Déserts d’amour (1984)", "Déserts d'amour, reprise 1995"); n = graph.getNode("Déserts d'amour, reprise 1995"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1940", "Déserts d'amour, reprise 1995", "Déserts d'amour, reprise 1995 (1996-06-21)"); n = graph.getNode("Déserts d'amour, reprise 1995");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d'amour, reprise 1995 (1996-06-21)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1950", "Déserts d’amour (1984)", "Déserts d'amour, reprise 1995"); n = graph.getNode("Déserts d'amour, reprise 1995"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1950", "Déserts d'amour, reprise 1995", "Déserts d'amour, reprise 1995 (1996-06-21)"); n = graph.getNode("Déserts d'amour, reprise 1995");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d'amour, reprise 1995 (1996-06-21)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1960", "Déserts d’amour (1984)", "Déserts d'amour, reprise 1995"); n = graph.getNode("Déserts d'amour, reprise 1995"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1960", "Déserts d'amour, reprise 1995", "Déserts d'amour, reprise 1995 (1996-06-21)"); n = graph.getNode("Déserts d'amour, reprise 1995");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d'amour, reprise 1995 (1996-06-21)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1970", "F. et Stein (1983)", "Emission Festival Montpellier Danse 1996"); n = graph.getNode("Emission Festival Montpellier Danse 1996"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("F. et Stein (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1970", "Emission Festival Montpellier Danse 1996", "F. et Stein (1983-02-19)"); n = graph.getNode("Emission Festival Montpellier Danse 1996");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("F. et Stein (1983-02-19)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1980", "Jours étranges (1990)", "Moderato cantabile"); n = graph.getNode("Moderato cantabile"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1981", "Meublé sommairement (1989)", "Moderato cantabile"); n = graph.getNode("Moderato cantabile"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1982", "Moderato Cantabile (1997)", "Moderato cantabile"); n = graph.getNode("Moderato cantabile"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Moderato Cantabile (1997)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1983", "So Schnell (1990)", "Moderato cantabile"); n = graph.getNode("Moderato cantabile"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_1984", "Suitte d’un goût étranger (1985)", "Moderato cantabile"); n = graph.getNode("Moderato cantabile"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suitte d’un goût étranger (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_1980", "Moderato cantabile", "Jours étranges (1990-07-04)"); n = graph.getNode("Moderato cantabile"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990-07-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_1981", "Moderato cantabile", "Meublé sommairement (1989-07-10)"); n = graph.getNode("Moderato cantabile"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989-07-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_1982", "Moderato cantabile", "So Schnell (1990-12-06)"); n = graph.getNode("Moderato cantabile"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990-12-06)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_1983", "Moderato cantabile", "So Schnell, version 92 (1992-10-11)"); n = graph.getNode("Moderato cantabile"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell, version 92 (1992-10-11)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_1984", "Moderato cantabile", "Suitte d'un goût étranger (1985-05-09)"); n = graph.getNode("Moderato cantabile"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suitte d'un goût étranger (1985-05-09)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_1990", "Jours étranges (1990)", "Jours étranges, reprise 1997 par le Dance Theatre of Ireland"); n =
		 * graph.getNode("Jours étranges, reprise 1997 par le Dance Theatre of Ireland"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_fchor_1990", "Jours étranges, reprise 1997 par le Dance Theatre of Ireland", "Jours étranges (1997-03-11)"); n =
		 * graph.getNode("Jours étranges, reprise 1997 par le Dance Theatre of Ireland"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Jours étranges (1997-03-11)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2000", "Suite pour violes (1977)", "Suite pour violes"); n = graph.getNode("Suite pour violes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suite pour violes (1977)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2000", "Suite pour violes", "Suite pour violes (1997-04-30)"); n = graph.getNode("Suite pour violes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suite pour violes (1997-04-30)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_2001", "Suite pour violes", "Suite pour violes (2002-10-25)"); n = graph.getNode("Suite pour violes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suite pour violes (2002-10-25)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_2002", "Suite pour violes", "Suite pour violes (2004-02-03)"); n = graph.getNode("Suite pour violes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suite pour violes (2004-02-03)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2010", "So Schnell (1990)", "Histoire d'une transmission : So Schnell à l'Opéra"); n = graph.getNode("Histoire d'une transmission : So Schnell à l'Opéra");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2010", "Histoire d'une transmission : So Schnell à l'Opéra", "So Schnell, version 92 (1998-03-17)"); n =
		 * graph.getNode("Histoire d'une transmission : So Schnell à l'Opéra"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell, version 92 (1998-03-17)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_or_2020", "So Schnell (1990)", "So Schnell, répétitions, Opéra Garnier 1/3"); n = graph.getNode("So Schnell, répétitions, Opéra Garnier 1/3"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2020", "So Schnell, répétitions, Opéra Garnier 1/3", "So Schnell, version 92 (1998-03-17)"); n = graph.getNode("So Schnell, répétitions, Opéra Garnier 1/3");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell, version 92 (1998-03-17)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2030", "So Schnell (1990)", "So Schnell, répétitions, Opéra Garnier 2/3"); n = graph.getNode("So Schnell, répétitions, Opéra Garnier 2/3"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2030", "So Schnell, répétitions, Opéra Garnier 2/3", "So Schnell, version 92 (1998-03-17)"); n = graph.getNode("So Schnell, répétitions, Opéra Garnier 2/3");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell, version 92 (1998-03-17)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2040", "So Schnell (1990)", "So Schnell, répétitions, Opéra Garnier 3/3"); n = graph.getNode("So Schnell, répétitions, Opéra Garnier 3/3"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2040", "So Schnell, répétitions, Opéra Garnier 3/3", "So Schnell, version 92 (1998-03-17)"); n = graph.getNode("So Schnell, répétitions, Opéra Garnier 3/3");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell, version 92 (1998-03-17)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2050", "So Schnell (1990)", "So Schnell par le ballet de l'Opéra de Paris- réalisation"); n =
		 * graph.getNode("So Schnell par le ballet de l'Opéra de Paris- réalisation"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2050", "So Schnell par le ballet de l'Opéra de Paris- réalisation", "So Schnell, version 92 (1998-03-17)"); n =
		 * graph.getNode("So Schnell par le ballet de l'Opéra de Paris- réalisation"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell, version 92 (1998-03-17)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2060", "Meublé sommairement (1989)", "Meublé sommairement, lectures 1/2"); n = graph.getNode("Meublé sommairement, lectures 1/2"); n.setAttribute("ui.class", "doc"); n
		 * = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2060", "Meublé sommairement, lectures 1/2", "Meublé sommairement, lectures (1997-04-15)"); n = graph.getNode("Meublé sommairement, lectures 1/2");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement, lectures (1997-04-15)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2070", "Meublé sommairement (1989)", "Meublé sommairement, lectures 2/2"); n = graph.getNode("Meublé sommairement, lectures 2/2"); n.setAttribute("ui.class", "doc"); n
		 * = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2070", "Meublé sommairement, lectures 2/2", "Meublé sommairement, lectures (1997-04-15)"); n = graph.getNode("Meublé sommairement, lectures 2/2");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement, lectures (1997-04-15)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2080", "Dix anges, portraits (1988)", "Dix anges, tournage du film"); n = graph.getNode("Dix anges, tournage du film"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Dix anges, portraits (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2080", "Dix anges, tournage du film", "Dix anges, portraits (1988-08-00)"); n = graph.getNode("Dix anges, tournage du film"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Dix anges, portraits (1988-08-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_2081", "Dix anges, tournage du film", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Dix anges, tournage du film"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2090", "Meublé sommairement (1989)", "Stage : Université d'été 1998 (documentaire version courte)"); n =
		 * graph.getNode("Stage : Université d'été 1998 (documentaire version courte)"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_or_2100", "Meublé sommairement (1989)", "Stage : Université d'été 1998 (documentaire version longue)"); n =
		 * graph.getNode("Stage : Université d'été 1998 (documentaire version longue)"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_or_2110", "Meublé sommairement (1989)", "Stage : Université d'été 1998 (rushes)"); n = graph.getNode("Stage : Université d'été 1998 (rushes)"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2130", "Grand Corridor (1980)", "Grand Corridor, extraits par le Ballet de Genève"); n = graph.getNode("Grand Corridor, extraits par le Ballet de Genève");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Grand Corridor (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2130", "Grand Corridor, extraits par le Ballet de Genève", "Grand Corridor, extraits (1998-10-06)"); n =
		 * graph.getNode("Grand Corridor, extraits par le Ballet de Genève"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Grand Corridor, extraits (1998-10-06)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_or_2140", "Déserts d’amour (1984)", "Stage Polski Teatr Tanca - Poznan"); n = graph.getNode("Stage Polski Teatr Tanca - Poznan"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2141", "Saut de l’ange (Le) (1987)", "Stage Polski Teatr Tanca - Poznan"); n = graph.getNode("Stage Polski Teatr Tanca - Poznan"); n.setAttribute("ui.class", "doc"); n
		 * = graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2150", "Bien fait, pour vous (1999)", "Bien fait, pour vous"); n = graph.getNode("Bien fait, pour vous"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Bien fait, pour vous (1999)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2150", "Bien fait, pour vous", "Bien fait, pour vous (1999-03-16)"); n = graph.getNode("Bien fait, pour vous"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Bien fait, pour vous (1999-03-16)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2160", "Assaï (1986)", "Imaginons"); n = graph.getNode("Imaginons"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Assaï (1986)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_or_2161", "Déserts d’amour (1984)", "Imaginons"); n = graph.getNode("Imaginons"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d’amour (1984)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2162", "Imaginons (1999)", "Imaginons"); n = graph.getNode("Imaginons"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Imaginons (1999)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2163", "Meublé sommairement (1989)", "Imaginons"); n = graph.getNode("Imaginons"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2164", "Necesito, pièce pour Grenade (1991)", "Imaginons"); n = graph.getNode("Imaginons"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2165", "So Schnell (1990)", "Imaginons"); n = graph.getNode("Imaginons"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2160", "Imaginons", "Imaginons (1999-02-26)"); n = graph.getNode("Imaginons"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Imaginons (1999-02-26)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2170", "Jours étranges (1990)", "Jours étranges par le Dance Theatre of Ireland"); n = graph.getNode("Jours étranges par le Dance Theatre of Ireland");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2170", "Jours étranges par le Dance Theatre of Ireland", "Jours étranges (1997-03-11)"); n = graph.getNode("Jours étranges par le Dance Theatre of Ireland");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Jours étranges (1997-03-11)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2180", "Déserts d’amour (1984)", "Déserts d'amour, reprise 1999, extraits"); n = graph.getNode("Déserts d'amour, reprise 1999, extraits"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2180", "Déserts d'amour, reprise 1999, extraits", "Déserts d'amour, reprise 1999, extraits (1999-10-25)"); n =
		 * graph.getNode("Déserts d'amour, reprise 1999, extraits"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d'amour, reprise 1999, extraits (1999-10-25)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2190", "Meublé sommairement (1989)", "Meublé sommairement, reprise 2000 : répétitions"); n = graph.getNode("Meublé sommairement, reprise 2000 : répétitions");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2190", "Meublé sommairement, reprise 2000 : répétitions", "Meublé sommairement (2000-03-08)"); n = graph.getNode("Meublé sommairement, reprise 2000 : répétitions");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (2000-03-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2200", "Meublé sommairement (1989)", "Meublé sommairement, reprise 2000, lecture démonstration"); n =
		 * graph.getNode("Meublé sommairement, reprise 2000, lecture démonstration"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_fchor_2200", "Meublé sommairement, reprise 2000, lecture démonstration", "Meublé sommairement (2000-03-08)"); n =
		 * graph.getNode("Meublé sommairement, reprise 2000, lecture démonstration"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (2000-03-08)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2210", "Déserts d’amour (1984)", "Stage CND : L'Univers chorégraphique de Dominique Bagouet"); n =
		 * graph.getNode("Stage CND : L'Univers chorégraphique de Dominique Bagouet"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2211", "Insaisies (1982)", "Stage CND : L'Univers chorégraphique de Dominique Bagouet"); n =
		 * graph.getNode("Stage CND : L'Univers chorégraphique de Dominique Bagouet"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Insaisies (1982)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2212", "Necesito, pièce pour Grenade (1991)", "Stage CND : L'Univers chorégraphique de Dominique Bagouet"); n =
		 * graph.getNode("Stage CND : L'Univers chorégraphique de Dominique Bagouet"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito, pièce pour Grenade (1991)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2213", "Saut de l’ange (Le) (1987)", "Stage CND : L'Univers chorégraphique de Dominique Bagouet"); n =
		 * graph.getNode("Stage CND : L'Univers chorégraphique de Dominique Bagouet"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_or_2214", "Suite pour violes (1977)", "Stage CND : L'Univers chorégraphique de Dominique Bagouet"); n =
		 * graph.getNode("Stage CND : L'Univers chorégraphique de Dominique Bagouet"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Suite pour violes (1977)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_or_2220", "Meublé sommairement (1989)", "Meublé sommairement, reprise 2000, répétitions"); n = graph.getNode("Meublé sommairement, reprise 2000, répétitions");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2220", "Meublé sommairement, reprise 2000, répétitions", "Meublé sommairement (2000-03-08)"); n = graph.getNode("Meublé sommairement, reprise 2000, répétitions");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (2000-03-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2230", "Meublé sommairement (1989)", "Meublé sommairement, reprise 2000, répétitions"); n = graph.getNode("Meublé sommairement, reprise 2000, répétitions");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2230", "Meublé sommairement, reprise 2000, répétitions", "Meublé sommairement (2000-03-08)"); n = graph.getNode("Meublé sommairement, reprise 2000, répétitions");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (2000-03-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2240", "Meublé sommairement (1989)", "Meublé sommairement, reprise 2000, répétitions"); n = graph.getNode("Meublé sommairement, reprise 2000, répétitions");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2240", "Meublé sommairement, reprise 2000, répétitions", "Meublé sommairement (2000-03-08)"); n = graph.getNode("Meublé sommairement, reprise 2000, répétitions");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (2000-03-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2250", "Meublé sommairement (1989)", "Meublé sommairement, reprise 2000"); n = graph.getNode("Meublé sommairement, reprise 2000"); n.setAttribute("ui.class", "doc"); n
		 * = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2250", "Meublé sommairement, reprise 2000", "Meublé sommairement (2000-03-08)"); n = graph.getNode("Meublé sommairement, reprise 2000"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Meublé sommairement (2000-03-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2260", "Meublé sommairement (1989)", "Carnets Bagouet (Les), émission sur Meublé sommairement"); n =
		 * graph.getNode("Carnets Bagouet (Les), émission sur Meublé sommairement"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_fchor_2260", "Carnets Bagouet (Les), émission sur Meublé sommairement", "Meublé sommairement (2000-03-08)"); n =
		 * graph.getNode("Carnets Bagouet (Les), émission sur Meublé sommairement"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (2000-03-08)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2270", "Petites Pièces de Berlin (Les) (1988)", "Petites Pièces de Berlin (Les), extraits 2e nonette"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les), extraits 2e nonette"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2270", "Petites Pièces de Berlin (Les), extraits 2e nonette", "Petites Pièces de Berlin (Les) (2000-03-30)"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les), extraits 2e nonette"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (2000-03-30)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2290", "Une Danse blanche avec Eliane (1980)", "Une danse blanche avec Eliane"); n = graph.getNode("Une danse blanche avec Eliane"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Une Danse blanche avec Eliane (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2290", "Une danse blanche avec Eliane", "Une danse blanche avec Eliane (2000-06-13)"); n = graph.getNode("Une danse blanche avec Eliane");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Une danse blanche avec Eliane (2000-06-13)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2300", "Bernard (2001)", "Bernard"); n = graph.getNode("Bernard"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Bernard (2001)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_or_2310", "Chaîne et trame, quelques pistes pour une étude de So schnell (2001)", "Chaîne et trame, quelques pistes pour une étude de So Schnell"); n =
		 * graph.getNode("Chaîne et trame, quelques pistes pour une étude de So Schnell"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Chaîne et trame, quelques pistes pour une étude de So schnell (2001)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2311", "So Schnell (1990)", "Chaîne et trame, quelques pistes pour une étude de So Schnell"); n =
		 * graph.getNode("Chaîne et trame, quelques pistes pour une étude de So Schnell"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2310", "Chaîne et trame, quelques pistes pour une étude de So Schnell", "So Schnell (1990-12-06)"); n =
		 * graph.getNode("Chaîne et trame, quelques pistes pour une étude de So Schnell"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990-12-06)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_fchor_2311", "Chaîne et trame, quelques pistes pour une étude de So Schnell", "So Schnell, version 92 (1992-10-11)"); n =
		 * graph.getNode("Chaîne et trame, quelques pistes pour une étude de So Schnell"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell, version 92 (1992-10-11)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2320", "Déserts d’amour (1984)", "B comme Bagouet"); n = graph.getNode("B comme Bagouet"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2321", "Insaisies (1982)", "B comme Bagouet"); n = graph.getNode("B comme Bagouet"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Insaisies (1982)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2322", "Necesito, pièce pour Grenade (1991)", "B comme Bagouet"); n = graph.getNode("B comme Bagouet"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2323", "Saut de l’ange (Le) (1987)", "B comme Bagouet"); n = graph.getNode("B comme Bagouet"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2324", "Suite pour violes (1977)", "B comme Bagouet"); n = graph.getNode("B comme Bagouet"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suite pour violes (1977)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2330", "Déserts d’amour (1984)", "Déserts d'amour (extraits), Le Blanc-Mesnil"); n = graph.getNode("Déserts d'amour (extraits), Le Blanc-Mesnil");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2330", "Déserts d'amour (extraits), Le Blanc-Mesnil", "Déserts d'amour (2001-06-01)"); n = graph.getNode("Déserts d'amour (extraits), Le Blanc-Mesnil");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d'amour (2001-06-01)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2340", "Petites Pièces de Berlin (Les) (1988)", "Petites Pièces de Berlin (Les), transmission du solo de Dominique 1/2"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les), transmission du solo de Dominique 1/2"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2350", "Petites Pièces de Berlin (Les) (1988)", "Petites Pièces de Berlin (Les), transmission du solo de Dominique 2/2"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les), transmission du solo de Dominique 2/2"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2360", "Assaï (1986)", "Dominique Bagouet et l'aventure constante"); n = graph.getNode("Dominique Bagouet et l'aventure constante"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Assaï (1986)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2361", "Déserts d’amour (1984)", "Dominique Bagouet et l'aventure constante"); n = graph.getNode("Dominique Bagouet et l'aventure constante");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2362", "Dominique Bagouet et l'aventure constante (2002)", "Dominique Bagouet et l'aventure constante"); n =
		 * graph.getNode("Dominique Bagouet et l'aventure constante"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Dominique Bagouet et l'aventure constante (2002)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2363", "Jours étranges (1990)", "Dominique Bagouet et l'aventure constante"); n = graph.getNode("Dominique Bagouet et l'aventure constante");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2364", "Matière première (2002)", "Dominique Bagouet et l'aventure constante"); n = graph.getNode("Dominique Bagouet et l'aventure constante");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Matière première (2002)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2365", "Petites Pièces de Berlin (Les) (1988)", "Dominique Bagouet et l'aventure constante"); n = graph.getNode("Dominique Bagouet et l'aventure constante");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2366", "Saut de l’ange (Le) (1987)", "Dominique Bagouet et l'aventure constante"); n = graph.getNode("Dominique Bagouet et l'aventure constante");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2367", "Une Danse blanche avec Eliane (1980)", "Dominique Bagouet et l'aventure constante"); n = graph.getNode("Dominique Bagouet et l'aventure constante");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Une Danse blanche avec Eliane (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2360", "Dominique Bagouet et l'aventure constante", "Assaï (1986-09-20)"); n = graph.getNode("Dominique Bagouet et l'aventure constante");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Assaï (1986-09-20)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_2361", "Dominique Bagouet et l'aventure constante", "Déserts d'amour (1984-07-01)"); n = graph.getNode("Dominique Bagouet et l'aventure constante");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d'amour (1984-07-01)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_2362", "Dominique Bagouet et l'aventure constante", "Jours étranges (1990-07-04)"); n = graph.getNode("Dominique Bagouet et l'aventure constante");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Jours étranges (1990-07-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_2363", "Dominique Bagouet et l'aventure constante", "Matière première (2002-12-10)"); n = graph.getNode("Dominique Bagouet et l'aventure constante");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Matière première (2002-12-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_2364", "Dominique Bagouet et l'aventure constante", "Petites Pièces de Berlin (Les) (1988-06-04)"); n = graph.getNode("Dominique Bagouet et l'aventure constante");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988-06-04)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_2365", "Dominique Bagouet et l'aventure constante", "Saut de l'ange (Le) (1987-06-24)"); n = graph.getNode("Dominique Bagouet et l'aventure constante");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l'ange (Le) (1987-06-24)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_2366", "Dominique Bagouet et l'aventure constante", "Une danse blanche avec Eliane (2000-06-13)"); n = graph.getNode("Dominique Bagouet et l'aventure constante");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Une danse blanche avec Eliane (2000-06-13)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2370", "F. et Stein (1983)", "F. et Stein, ré-interprétation"); n = graph.getNode("F. et Stein, ré-interprétation"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("F. et Stein (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2370", "F. et Stein, ré-interprétation", "F. et Stein, ré-interprétation (2000-11-07)"); n = graph.getNode("F. et Stein, ré-interprétation");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("F. et Stein, ré-interprétation (2000-11-07)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2380", "Matière première (2002)", "Necesito, transmission du solo d'Olivia"); n = graph.getNode("Necesito, transmission du solo d'Olivia"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Matière première (2002)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2381", "Necesito, pièce pour Grenade (1991)", "Necesito, transmission du solo d'Olivia"); n = graph.getNode("Necesito, transmission du solo d'Olivia");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2380", "Necesito, transmission du solo d'Olivia", "Matière première (2002-12-10)"); n = graph.getNode("Necesito, transmission du solo d'Olivia");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Matière première (2002-12-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_2381", "Necesito, transmission du solo d'Olivia", "Necesito (1991-07-26)"); n = graph.getNode("Necesito, transmission du solo d'Olivia"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Necesito (1991-07-26)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2390", "Matière première (2002)", "Matière première, promotion"); n = graph.getNode("Matière première, promotion"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Matière première (2002)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2391", "Meublé sommairement (1989)", "Matière première, promotion"); n = graph.getNode("Matière première, promotion"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2392", "Necesito, pièce pour Grenade (1991)", "Matière première, promotion"); n = graph.getNode("Matière première, promotion"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2393", "Petites Pièces de Berlin (Les) (1988)", "Matière première, promotion"); n = graph.getNode("Matière première, promotion"); n.setAttribute("ui.class", "doc"); n
		 * = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2394", "Saut de l’ange (Le) (1987)", "Matière première, promotion"); n = graph.getNode("Matière première, promotion"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2390", "Matière première, promotion", "Matière première (2002-12-10)"); n = graph.getNode("Matière première, promotion"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Matière première (2002-12-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2400", "Déserts d’amour (1984)", "Stage Ex.e.r.ce CCN Montpellier (captation)"); n = graph.getNode("Stage Ex.e.r.ce CCN Montpellier (captation)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2401", "Matière première (2002)", "Stage Ex.e.r.ce CCN Montpellier (captation)"); n = graph.getNode("Stage Ex.e.r.ce CCN Montpellier (captation)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Matière première (2002)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2402", "Meublé sommairement (1989)", "Stage Ex.e.r.ce CCN Montpellier (captation)"); n = graph.getNode("Stage Ex.e.r.ce CCN Montpellier (captation)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2403", "Necesito, pièce pour Grenade (1991)", "Stage Ex.e.r.ce CCN Montpellier (captation)"); n = graph.getNode("Stage Ex.e.r.ce CCN Montpellier (captation)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2404", "Petites Pièces de Berlin (Les) (1988)", "Stage Ex.e.r.ce CCN Montpellier (captation)"); n = graph.getNode("Stage Ex.e.r.ce CCN Montpellier (captation)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2405", "Saut de l’ange (Le) (1987)", "Stage Ex.e.r.ce CCN Montpellier (captation)"); n = graph.getNode("Stage Ex.e.r.ce CCN Montpellier (captation)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2410", "Déserts d’amour (1984)", "Stage Ex.e.r.ce CCN Montpellier (documentaire)"); n = graph.getNode("Stage Ex.e.r.ce CCN Montpellier (documentaire)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2411", "Matière première (2002)", "Stage Ex.e.r.ce CCN Montpellier (documentaire)"); n = graph.getNode("Stage Ex.e.r.ce CCN Montpellier (documentaire)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Matière première (2002)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2412", "Meublé sommairement (1989)", "Stage Ex.e.r.ce CCN Montpellier (documentaire)"); n = graph.getNode("Stage Ex.e.r.ce CCN Montpellier (documentaire)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2413", "Necesito, pièce pour Grenade (1991)", "Stage Ex.e.r.ce CCN Montpellier (documentaire)"); n = graph.getNode("Stage Ex.e.r.ce CCN Montpellier (documentaire)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2414", "Petites Pièces de Berlin (Les) (1988)", "Stage Ex.e.r.ce CCN Montpellier (documentaire)"); n = graph.getNode("Stage Ex.e.r.ce CCN Montpellier (documentaire)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2415", "Saut de l’ange (Le) (1987)", "Stage Ex.e.r.ce CCN Montpellier (documentaire)"); n = graph.getNode("Stage Ex.e.r.ce CCN Montpellier (documentaire)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2420", "Ribatz, Ribatz! (1976)", "Studio Bagouet"); n = graph.getNode("Studio Bagouet"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ribatz, Ribatz! (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2421", "Une Danse blanche avec Eliane (1980)", "Studio Bagouet"); n = graph.getNode("Studio Bagouet"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Une Danse blanche avec Eliane (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2420", "Studio Bagouet", "Ribatz, Ribatz!, version 2002 (2002-12-09)"); n = graph.getNode("Studio Bagouet"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ribatz, Ribatz!, version 2002 (2002-12-09)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_2421", "Studio Bagouet", "Une danse blanche avec Eliane (2000-06-13)"); n = graph.getNode("Studio Bagouet"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Une danse blanche avec Eliane (2000-06-13)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2430", "Déserts d’amour (1984)", "Matière première, extraits"); n = graph.getNode("Matière première, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2431", "Jours étranges (1990)", "Matière première, extraits"); n = graph.getNode("Matière première, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2432", "Matière première (2002)", "Matière première, extraits"); n = graph.getNode("Matière première, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Matière première (2002)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2433", "Meublé sommairement (1989)", "Matière première, extraits"); n = graph.getNode("Matière première, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2434", "Necesito, pièce pour Grenade (1991)", "Matière première, extraits"); n = graph.getNode("Matière première, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2435", "Petites Pièces de Berlin (Les) (1988)", "Matière première, extraits"); n = graph.getNode("Matière première, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2436", "Saut de l’ange (Le) (1987)", "Matière première, extraits"); n = graph.getNode("Matière première, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2437", "So Schnell (1990)", "Matière première, extraits"); n = graph.getNode("Matière première, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2438", "Une Danse blanche avec Eliane (1980)", "Matière première, extraits"); n = graph.getNode("Matière première, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Une Danse blanche avec Eliane (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2430", "Matière première, extraits", "Matière première (2002-12-10)"); n = graph.getNode("Matière première, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Matière première (2002-12-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2440", "Déserts d’amour (1984)", "Matière première, Montpellier"); n = graph.getNode("Matière première, Montpellier"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2441", "Jours étranges (1990)", "Matière première, Montpellier"); n = graph.getNode("Matière première, Montpellier"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2442", "Matière première (2002)", "Matière première, Montpellier"); n = graph.getNode("Matière première, Montpellier"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Matière première (2002)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2443", "Meublé sommairement (1989)", "Matière première, Montpellier"); n = graph.getNode("Matière première, Montpellier"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2444", "Necesito, pièce pour Grenade (1991)", "Matière première, Montpellier"); n = graph.getNode("Matière première, Montpellier"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2445", "Petites Pièces de Berlin (Les) (1988)", "Matière première, Montpellier"); n = graph.getNode("Matière première, Montpellier"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2446", "Saut de l’ange (Le) (1987)", "Matière première, Montpellier"); n = graph.getNode("Matière première, Montpellier"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2447", "So Schnell (1990)", "Matière première, Montpellier"); n = graph.getNode("Matière première, Montpellier"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2448", "Une Danse blanche avec Eliane (1980)", "Matière première, Montpellier"); n = graph.getNode("Matière première, Montpellier"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Une Danse blanche avec Eliane (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2440", "Matière première, Montpellier", "Matière première (2002-12-10)"); n = graph.getNode("Matière première, Montpellier"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Matière première (2002-12-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2450", "Encore chaud (2003)", "Encore chaud"); n = graph.getNode("Encore chaud"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Encore chaud (2003)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2451", "Jours étranges (1990)", "Encore chaud"); n = graph.getNode("Encore chaud"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Jours étranges (1990)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2450", "Encore chaud", "Jours étranges (1997-03-11)"); n = graph.getNode("Encore chaud"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1997-03-11)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2460", "Ribatz, Ribatz! (1976)", "Ribatz, Ribatz ou le grain du temps"); n = graph.getNode("Ribatz, Ribatz ou le grain du temps"); n.setAttribute("ui.class", "doc"); n
		 * = graph.getNode("Ribatz, Ribatz! (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2460", "Ribatz, Ribatz ou le grain du temps", "Ribatz, Ribatz!, version 2002 (2002-12-09)"); n = graph.getNode("Ribatz, Ribatz ou le grain du temps");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Ribatz, Ribatz!, version 2002 (2002-12-09)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2470", "Ribatz, Ribatz! (1976)", "Ribatz, Ribatz the grain of time"); n = graph.getNode("Ribatz, Ribatz the grain of time"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ribatz, Ribatz! (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2470", "Ribatz, Ribatz the grain of time", "Ribatz, Ribatz!, version 2002 (2002-12-09)"); n = graph.getNode("Ribatz, Ribatz the grain of time");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Ribatz, Ribatz!, version 2002 (2002-12-09)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2480", "Déserts d’amour (1984)", "Matière première"); n = graph.getNode("Matière première"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2481", "Jours étranges (1990)", "Matière première"); n = graph.getNode("Matière première"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2482", "Matière première (2002)", "Matière première"); n = graph.getNode("Matière première"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Matière première (2002)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2483", "Meublé sommairement (1989)", "Matière première"); n = graph.getNode("Matière première"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2484", "Necesito, pièce pour Grenade (1991)", "Matière première"); n = graph.getNode("Matière première"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2485", "Petites Pièces de Berlin (Les) (1988)", "Matière première"); n = graph.getNode("Matière première"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2486", "Saut de l’ange (Le) (1987)", "Matière première"); n = graph.getNode("Matière première"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2487", "So Schnell (1990)", "Matière première"); n = graph.getNode("Matière première"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2488", "Une Danse blanche avec Eliane (1980)", "Matière première"); n = graph.getNode("Matière première"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Une Danse blanche avec Eliane (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2480", "Matière première", "Matière première (2002-12-10)"); n = graph.getNode("Matière première"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Matière première (2002-12-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2490", "Déserts d’amour (1984)", "Matière première, Nîmes"); n = graph.getNode("Matière première, Nîmes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2491", "Jours étranges (1990)", "Matière première, Nîmes"); n = graph.getNode("Matière première, Nîmes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2492", "Matière première (2002)", "Matière première, Nîmes"); n = graph.getNode("Matière première, Nîmes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Matière première (2002)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2493", "Meublé sommairement (1989)", "Matière première, Nîmes"); n = graph.getNode("Matière première, Nîmes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2494", "Necesito, pièce pour Grenade (1991)", "Matière première, Nîmes"); n = graph.getNode("Matière première, Nîmes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2495", "Petites Pièces de Berlin (Les) (1988)", "Matière première, Nîmes"); n = graph.getNode("Matière première, Nîmes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2496", "Saut de l’ange (Le) (1987)", "Matière première, Nîmes"); n = graph.getNode("Matière première, Nîmes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2497", "So Schnell (1990)", "Matière première, Nîmes"); n = graph.getNode("Matière première, Nîmes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2498", "Une Danse blanche avec Eliane (1980)", "Matière première, Nîmes"); n = graph.getNode("Matière première, Nîmes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Une Danse blanche avec Eliane (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2490", "Matière première, Nîmes", "Matière première (2002-12-10)"); n = graph.getNode("Matière première, Nîmes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Matière première (2002-12-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2500", "Déserts d’amour (1984)", "Déserts d'amour (extraits), Poitiers"); n = graph.getNode("Déserts d'amour (extraits), Poitiers"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2500", "Déserts d'amour (extraits), Poitiers", "Déserts d'amour (2003-05-18)"); n = graph.getNode("Déserts d'amour (extraits), Poitiers");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d'amour (2003-05-18)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2510", "Déserts d’amour (1984)", "Déserts d'amour et Le Saut de l'ange, extraits pour étudiants"); n =
		 * graph.getNode("Déserts d'amour et Le Saut de l'ange, extraits pour étudiants"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_or_2511", "Saut de l’ange (Le) (1987)", "Déserts d'amour et Le Saut de l'ange, extraits pour étudiants"); n =
		 * graph.getNode("Déserts d'amour et Le Saut de l'ange, extraits pour étudiants"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l’ange (Le) (1987)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2510", "Déserts d'amour et Le Saut de l'ange, extraits pour étudiants", "Saut de l'ange (Le) (2003-06-06)"); n =
		 * graph.getNode("Déserts d'amour et Le Saut de l'ange, extraits pour étudiants"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Saut de l'ange (Le) (2003-06-06)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2520", "So Schnell (1990)", "So Schnell, extraits"); n = graph.getNode("So Schnell, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2520", "So Schnell, extraits", "So Schnell, version 92 (2003-11-08)"); n = graph.getNode("So Schnell, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell, version 92 (2003-11-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_2521", "So Schnell, extraits", "So Schnell, version 92 (2004-05-28)"); n = graph.getNode("So Schnell, extraits"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell, version 92 (2004-05-28)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2530", "Déserts d’amour (1984)", "Soirée Bagouet"); n = graph.getNode("Soirée Bagouet"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2531", "F. et Stein (1983)", "Soirée Bagouet"); n = graph.getNode("Soirée Bagouet"); n.setAttribute("ui.class", "doc"); n = graph.getNode("F. et Stein (1983)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2532", "So Schnell (1990)", "Soirée Bagouet"); n = graph.getNode("Soirée Bagouet"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2540", "Ribatz, Ribatz! (1976)", "Retour sur Ribatz, les anciens 1/3"); n = graph.getNode("Retour sur Ribatz, les anciens 1/3"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ribatz, Ribatz! (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2540", "Retour sur Ribatz, les anciens 1/3", "Retour sur Ribatz (2004-12-15)"); n = graph.getNode("Retour sur Ribatz, les anciens 1/3"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Retour sur Ribatz (2004-12-15)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2550", "Ribatz, Ribatz! (1976)", "Retour sur Ribatz, répétitions septembre à novembre 2004"); n =
		 * graph.getNode("Retour sur Ribatz, répétitions septembre à novembre 2004"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Ribatz, Ribatz! (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2550", "Retour sur Ribatz, répétitions septembre à novembre 2004", "Retour sur Ribatz (2004-12-15)"); n =
		 * graph.getNode("Retour sur Ribatz, répétitions septembre à novembre 2004"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Retour sur Ribatz (2004-12-15)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_or_2560", "Ribatz, Ribatz! (1976)", "Retour sur Ribatz, étudiants, répétitions 1"); n = graph.getNode("Retour sur Ribatz, étudiants, répétitions 1");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Ribatz, Ribatz! (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2560", "Retour sur Ribatz, étudiants, répétitions 1", "Retour sur Ribatz (2004-12-15)"); n = graph.getNode("Retour sur Ribatz, étudiants, répétitions 1");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Retour sur Ribatz (2004-12-15)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2570", "Ribatz, Ribatz! (1976)", "Retour sur Ribatz, étudiants, répétitions 2"); n = graph.getNode("Retour sur Ribatz, étudiants, répétitions 2");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Ribatz, Ribatz! (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2570", "Retour sur Ribatz, étudiants, répétitions 2", "Retour sur Ribatz (2004-12-15)"); n = graph.getNode("Retour sur Ribatz, étudiants, répétitions 2");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Retour sur Ribatz (2004-12-15)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2580", "Ribatz, Ribatz! (1976)", "Retour sur Ribatz, les anciens 2/3"); n = graph.getNode("Retour sur Ribatz, les anciens 2/3"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ribatz, Ribatz! (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2580", "Retour sur Ribatz, les anciens 2/3", "Retour sur Ribatz (2004-12-15)"); n = graph.getNode("Retour sur Ribatz, les anciens 2/3"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Retour sur Ribatz (2004-12-15)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2590", "Ribatz, Ribatz! (1976)", "Retour sur Ribatz, les anciens 3/3"); n = graph.getNode("Retour sur Ribatz, les anciens 3/3"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ribatz, Ribatz! (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2590", "Retour sur Ribatz, les anciens 3/3", "Retour sur Ribatz (2004-12-15)"); n = graph.getNode("Retour sur Ribatz, les anciens 3/3"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Retour sur Ribatz (2004-12-15)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2600", "Ribatz, Ribatz! (1976)", "Retour sur Ribatz, étudiants, filage"); n = graph.getNode("Retour sur Ribatz, étudiants, filage"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Ribatz, Ribatz! (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2600", "Retour sur Ribatz, étudiants, filage", "Retour sur Ribatz (2004-12-15)"); n = graph.getNode("Retour sur Ribatz, étudiants, filage");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Retour sur Ribatz (2004-12-15)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2610", "Crawl de Lucien (Le) (1985)", "Retransmissions"); n = graph.getNode("Retransmissions"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Crawl de Lucien (Le) (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2611", "Meublé sommairement (1989)", "Retransmissions"); n = graph.getNode("Retransmissions"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2612", "Retransmissions (2004)", "Retransmissions"); n = graph.getNode("Retransmissions"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Retransmissions (2004)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2620", "Ribatz, Ribatz! (1976)", "Retour sur Ribatz, étudiants, spectacle"); n = graph.getNode("Retour sur Ribatz, étudiants, spectacle"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Ribatz, Ribatz! (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2620", "Retour sur Ribatz, étudiants, spectacle", "Retour sur Ribatz (2004-12-15)"); n = graph.getNode("Retour sur Ribatz, étudiants, spectacle");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Retour sur Ribatz (2004-12-15)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2630", "Déserts d’amour (1984)", "Matière première, deuxième"); n = graph.getNode("Matière première, deuxième"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2631", "Jours étranges (1990)", "Matière première, deuxième"); n = graph.getNode("Matière première, deuxième"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2632", "Matière première (2002)", "Matière première, deuxième"); n = graph.getNode("Matière première, deuxième"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Matière première (2002)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2633", "Meublé sommairement (1989)", "Matière première, deuxième"); n = graph.getNode("Matière première, deuxième"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2634", "Necesito, pièce pour Grenade (1991)", "Matière première, deuxième"); n = graph.getNode("Matière première, deuxième"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2635", "Petites Pièces de Berlin (Les) (1988)", "Matière première, deuxième"); n = graph.getNode("Matière première, deuxième"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2636", "Saut de l’ange (Le) (1987)", "Matière première, deuxième"); n = graph.getNode("Matière première, deuxième"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Saut de l’ange (Le) (1987)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2637", "So Schnell (1990)", "Matière première, deuxième"); n = graph.getNode("Matière première, deuxième"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2638", "Une Danse blanche avec Eliane (1980)", "Matière première, deuxième"); n = graph.getNode("Matière première, deuxième"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Une Danse blanche avec Eliane (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2630", "Matière première, deuxième", "Matière première, deuxième (2005-04-08)"); n = graph.getNode("Matière première, deuxième"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Matière première, deuxième (2005-04-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2640", "Déserts d’amour (1984)", "Déserts d'amour, duo"); n = graph.getNode("Déserts d'amour, duo"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2640", "Déserts d'amour, duo", "Déserts d'amour, extraits (2002-01-11)"); n = graph.getNode("Déserts d'amour, duo"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d'amour, extraits (2002-01-11)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2650", "Déserts d’amour (1984)", "Déserts d'amour, extraits pour le CRR de Paris"); n = graph.getNode("Déserts d'amour, extraits pour le CRR de Paris");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2650", "Déserts d'amour, extraits pour le CRR de Paris", "Déserts d'amour, extraits (2005-05-21)"); n =
		 * graph.getNode("Déserts d'amour, extraits pour le CRR de Paris"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d'amour, extraits (2005-05-21)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_or_2660", "Noces d'or (1992)", "Noces d'or, ou la mort du chorégraphe"); n = graph.getNode("Noces d'or, ou la mort du chorégraphe"); n.setAttribute("ui.class", "doc"); n
		 * = graph.getNode("Noces d'or (1992)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2670", "Petites Pièces de Berlin (Les) (1988)", "Extraits divers reconstruits via partition Laban"); n =
		 * graph.getNode("Extraits divers reconstruits via partition Laban"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_or_2671", "So Schnell (1990)", "Extraits divers reconstruits via partition Laban"); n = graph.getNode("Extraits divers reconstruits via partition Laban");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2680", "So Schnell (1990)", "So Schnell, extraits par la Cie Coline"); n = graph.getNode("So Schnell, extraits par la Cie Coline"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2680", "So Schnell, extraits par la Cie Coline", "So Schnell, version 92 (2005-10-28)"); n = graph.getNode("So Schnell, extraits par la Cie Coline");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell, version 92 (2005-10-28)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2690", "Déserts d’amour (1984)", "Déserts d'amour, suite pour neuf danseurs : bande annonce"); n =
		 * graph.getNode("Déserts d'amour, suite pour neuf danseurs : bande annonce"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2690", "Déserts d'amour, suite pour neuf danseurs : bande annonce", "Déserts d'amour, suite pour neuf danseurs (2006-01-19)"); n =
		 * graph.getNode("Déserts d'amour, suite pour neuf danseurs : bande annonce"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d'amour, suite pour neuf danseurs (2006-01-19)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2700", "Déserts d’amour (1984)", "Déserts d'amour, suite pour neuf danseurs"); n = graph.getNode("Déserts d'amour, suite pour neuf danseurs");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2700", "Déserts d'amour, suite pour neuf danseurs", "Déserts d'amour, suite pour neuf danseurs (2006-01-19)"); n =
		 * graph.getNode("Déserts d'amour, suite pour neuf danseurs"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d'amour, suite pour neuf danseurs (2006-01-19)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2720", "Une Danse blanche avec Eliane (1980)", "Une danse blanche avec Eliane"); n = graph.getNode("Une danse blanche avec Eliane"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Une Danse blanche avec Eliane (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2720", "Une danse blanche avec Eliane", "Une danse blanche avec Eliane (2006-03-10)"); n = graph.getNode("Une danse blanche avec Eliane");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Une danse blanche avec Eliane (2006-03-10)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2730", "Crawl de Lucien (Le) (1985)", "Crawl, éclats"); n = graph.getNode("Crawl, éclats"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Crawl de Lucien (Le) (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2730", "Crawl, éclats", "Crawl de Lucien (Le) (1985-07-09)"); n = graph.getNode("Crawl, éclats"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Crawl de Lucien (Le) (1985-07-09)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2740", "Déserts d’amour (1984)", "Déserts d'amour duo, transmission au CNSMD 1/3"); n = graph.getNode("Déserts d'amour duo, transmission au CNSMD 1/3");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2750", "Déserts d’amour (1984)", "Déserts d'amour duo, transmission au CNSMD 2/3"); n = graph.getNode("Déserts d'amour duo, transmission au CNSMD 2/3");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2760", "Déserts d’amour (1984)", "Déserts d'amour duo, transmission au CNSMD 3/3"); n = graph.getNode("Déserts d'amour duo, transmission au CNSMD 3/3");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2770", "So Schnell (1990)", "So schnell, travail préparatoire à la transmission 1/2"); n = graph.getNode("So schnell, travail préparatoire à la transmission 1/2");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2780", "So Schnell (1990)", "So schnell, travail préparatoire à la transmission 2/2"); n = graph.getNode("So schnell, travail préparatoire à la transmission 2/2");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2790", "Suitte d’un goût étranger (1985)", "Music, please! 2007"); n = graph.getNode("Music, please! 2007"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Suitte d’un goût étranger (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2790", "Music, please! 2007", "Petites Pièces de Berlin (Les) (2007-02-08)"); n = graph.getNode("Music, please! 2007"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les) (2007-02-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2800", "Jours étranges (1990)", "Jours étranges et So Schnell par le Ballet de Genève"); n = graph.getNode("Jours étranges et So Schnell par le Ballet de Genève");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2801", "So Schnell (1990)", "Jours étranges et So Schnell par le Ballet de Genève"); n = graph.getNode("Jours étranges et So Schnell par le Ballet de Genève");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2800", "Jours étranges et So Schnell par le Ballet de Genève", "Jours étranges (2007-03-28)"); n =
		 * graph.getNode("Jours étranges et So Schnell par le Ballet de Genève"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Jours étranges (2007-03-28)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_fchor_2801", "Jours étranges et So Schnell par le Ballet de Genève", "So Schnell, version 92 (2007-03-28)"); n =
		 * graph.getNode("Jours étranges et So Schnell par le Ballet de Genève"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell, version 92 (2007-03-28)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2810", "Crawl de Lucien (Le) (1985)", "Crawl, éclats"); n = graph.getNode("Crawl, éclats"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Crawl de Lucien (Le) (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2840", "Déserts d’amour (1984)", "Duo de Déserts d'amour, répétition"); n = graph.getNode("Duo de Déserts d'amour, répétition"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2850", "Jours étranges (1990)", "Programme du Festival Montpellier Danse 07"); n = graph.getNode("Programme du Festival Montpellier Danse 07");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2851", "So Schnell (1990)", "Programme du Festival Montpellier Danse 07"); n = graph.getNode("Programme du Festival Montpellier Danse 07"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2852", "Valse des fleurs (1983)", "Programme du Festival Montpellier Danse 07"); n = graph.getNode("Programme du Festival Montpellier Danse 07");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Valse des fleurs (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2850", "Programme du Festival Montpellier Danse 07", "Valse des fleurs (2007-06-25)"); n = graph.getNode("Programme du Festival Montpellier Danse 07");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Valse des fleurs (2007-06-25)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2860", "Valse des fleurs (1983)", "Valse des fleurs 2007"); n = graph.getNode("Valse des fleurs 2007"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Valse des fleurs (1983)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2860", "Valse des fleurs 2007", "Valse des fleurs (2007-06-25)"); n = graph.getNode("Valse des fleurs 2007"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Valse des fleurs (2007-06-25)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2870", "So Schnell (1990)", "So Schnell… très vite"); n = graph.getNode("So Schnell… très vite"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2870", "So Schnell… très vite", "So Schnell (2007-06-09)"); n = graph.getNode("So Schnell… très vite"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (2007-06-09)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2880", "Petites Pièces de Berlin (Les) (1988)", "Petites Pièces de Berlin (Les), reprise 2008"); n = graph.getNode("Petites Pièces de Berlin (Les), reprise 2008");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2880", "Petites Pièces de Berlin (Les), reprise 2008", "Petites Pièces de Berlin (Les) (2008-09-11)"); n =
		 * graph.getNode("Petites Pièces de Berlin (Les), reprise 2008"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Petites Pièces de Berlin (Les) (2008-09-11)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2890", "So Schnell (1990)", "Compression-décompression"); n = graph.getNode("Compression-décompression"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2890", "Compression-décompression", "So Schnell (2009-05-05)"); n = graph.getNode("Compression-décompression"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (2009-05-05)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2900", "Déserts d’amour (1984)", "Extraits de Jours étranges, Déserts d'amour, So Schnell"); n =
		 * graph.getNode("Extraits de Jours étranges, Déserts d'amour, So Schnell"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2901", "Jours étranges (1990)", "Extraits de Jours étranges, Déserts d'amour, So Schnell"); n =
		 * graph.getNode("Extraits de Jours étranges, Déserts d'amour, So Schnell"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2902", "So Schnell (1990)", "Extraits de Jours étranges, Déserts d'amour, So Schnell"); n = graph.getNode("Extraits de Jours étranges, Déserts d'amour, So Schnell");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2900", "Extraits de Jours étranges, Déserts d'amour, So Schnell", "Jours étranges (2009-05-09)"); n =
		 * graph.getNode("Extraits de Jours étranges, Déserts d'amour, So Schnell"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Jours étranges (2009-05-09)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_fchor_2901", "Extraits de Jours étranges, Déserts d'amour, So Schnell", "So Schnell (2009-05-09)"); n =
		 * graph.getNode("Extraits de Jours étranges, Déserts d'amour, So Schnell"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (2009-05-09)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_or_2910", "Meublé sommairement (1989)", "Meublé sommairement, extraits par le CRR de Paris"); n = graph.getNode("Meublé sommairement, extraits par le CRR de Paris");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2910", "Meublé sommairement, extraits par le CRR de Paris", "Meublé sommairement (2009-05-22)"); n =
		 * graph.getNode("Meublé sommairement, extraits par le CRR de Paris"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (2009-05-22)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_or_2920", "Jours étranges (1990)", "D'après Jours étranges"); n = graph.getNode("D'après Jours étranges"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2920", "D'après Jours étranges", "Jours étranges (2009-05-30)"); n = graph.getNode("D'après Jours étranges"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (2009-05-30)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2940", "Crawl de Lucien (Le) (1985)", "Extraits de Crawl, CRR Poitiers 2010"); n = graph.getNode("Extraits de Crawl, CRR Poitiers 2010"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Crawl de Lucien (Le) (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_2941", "Déserts et Crawl (1988)", "Extraits de Crawl, CRR Poitiers 2010"); n = graph.getNode("Extraits de Crawl, CRR Poitiers 2010"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Déserts et Crawl (1988)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2940", "Extraits de Crawl, CRR Poitiers 2010", "Crawl (2010-02-01)"); n = graph.getNode("Extraits de Crawl, CRR Poitiers 2010"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Crawl (2010-02-01)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2950", "Chansons de nuit (1976)", "Nouvelle vague, génération Bagnolet"); n = graph.getNode("Nouvelle vague, génération Bagnolet"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Chansons de nuit (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2950", "Nouvelle vague, génération Bagnolet", "Chansons de nuit (2009-12-15)"); n = graph.getNode("Nouvelle vague, génération Bagnolet"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Chansons de nuit (2009-12-15)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2960", "So Schnell (1990)", "Autour de Déserts"); n = graph.getNode("Autour de Déserts"); n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (1990)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2960", "Autour de Déserts", "So Schnell (2010-04-23)"); n = graph.getNode("Autour de Déserts"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("So Schnell (2010-04-23)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2970", "Une Danse blanche avec Eliane (1980)", "Des danses blanches"); n = graph.getNode("Des danses blanches"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Une Danse blanche avec Eliane (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2970", "Des danses blanches", "Une Danse blanche avec Eliane (2010-05-27)"); n = graph.getNode("Des danses blanches"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Une Danse blanche avec Eliane (2010-05-27)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_2980", "Meublé sommairement (1989)", "Meublé sommairement, extraits par le CNDC d'Angers"); n = graph.getNode("Meublé sommairement, extraits par le CNDC d'Angers");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (1989)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2980", "Meublé sommairement, extraits par le CNDC d'Angers", "Meublé sommairement (2010-03-24)"); n =
		 * graph.getNode("Meublé sommairement, extraits par le CNDC d'Angers"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Meublé sommairement (2010-03-24)"); n.setAttribute("ui.class",
		 * "fchor");
		 * graph.addEdge("doc_or_2990", "Jours étranges (1990)", "Jours étranges"); n = graph.getNode("Jours étranges"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Jours étranges (1990)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_2990", "Jours étranges", "Jours étranges (2012-03-01)"); n = graph.getNode("Jours étranges"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Jours étranges (2012-03-01)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_3000", "Necesito, pièce pour Grenade (1991)", "Necesito, extraits par le CDC de Toulouse"); n = graph.getNode("Necesito, extraits par le CDC de Toulouse");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito, pièce pour Grenade (1991)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_3000", "Necesito, extraits par le CDC de Toulouse", "Necesito, pièce pour Grenade (2013-03-15)"); n = graph.getNode("Necesito, extraits par le CDC de Toulouse");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Necesito, pièce pour Grenade (2013-03-15)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_3010", "Déserts d’amour (1984)", "So Schnell et Déserts d'amour (extraits)"); n = graph.getNode("So Schnell et Déserts d'amour (extraits)"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Déserts d’amour (1984)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_3011", "So Schnell (1990)", "So Schnell et Déserts d'amour (extraits)"); n = graph.getNode("So Schnell et Déserts d'amour (extraits)"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("So Schnell (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_3010", "So Schnell et Déserts d'amour (extraits)", "Déserts d'amour (2014-03-07)"); n = graph.getNode("So Schnell et Déserts d'amour (extraits)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Déserts d'amour (2014-03-07)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_3011", "So Schnell et Déserts d'amour (extraits)", "So Schnell (2014-03-07)"); n = graph.getNode("So Schnell et Déserts d'amour (extraits)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("So Schnell (2014-03-07)"); n.setAttribute("ui.class", "fchor");
		 */
		// Liptay
		/*
		 * graph.addEdge("doc_or_00", "Fall in light (2001)", "Solos 1993-2001 (Les)"); n = graph.getNode("Solos 1993-2001 (Les)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Fall in light (2001)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_01", "Insula Deserta (1998)", "Solos 1993-2001 (Les)"); n = graph.getNode("Solos 1993-2001 (Les)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta (1998)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_02", "Intervalle (2000)", "Solos 1993-2001 (Les)"); n = graph.getNode("Solos 1993-2001 (Les)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Intervalle (2000)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_03", "Terre du ciel (1993)", "Solos 1993-2001 (Les)"); n = graph.getNode("Solos 1993-2001 (Les)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Terre du ciel (1993)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_00", "Solos 1993-2001 (Les)", "Fall in light (2001-11-23)"); n = graph.getNode("Solos 1993-2001 (Les)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Fall in light (2001-11-23)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_01", "Solos 1993-2001 (Les)", "Insula Deserta - solo (1998-00-00)"); n = graph.getNode("Solos 1993-2001 (Les)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta - solo (1998-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_02", "Solos 1993-2001 (Les)", "Intervalle (2000-12-22)"); n = graph.getNode("Solos 1993-2001 (Les)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Intervalle (2000-12-22)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_03", "Solos 1993-2001 (Les)", "Terre du ciel (1993-06-12)"); n = graph.getNode("Solos 1993-2001 (Les)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Terre du ciel (1993-06-12)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_10", "Daphné (1960)", "Daphné"); n = graph.getNode("Daphné"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Daphné (1960)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_10", "Daphné", "Daphné (1960-00-00)"); n = graph.getNode("Daphné"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Daphné (1960-00-00)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_20", "Miles sketches (1961)", "Miles sketches"); n = graph.getNode("Miles sketches"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Miles sketches (1961)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_20", "Miles sketches", "Miles sketches (1961-00-00)"); n = graph.getNode("Miles sketches"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Miles sketches (1961-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_30", "Blue in Green (1963)", "Blue in Green"); n = graph.getNode("Blue in Green"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Blue in Green (1963)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_30", "Blue in Green", "Blue in Green (1963-00-00)"); n = graph.getNode("Blue in Green"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Blue in Green (1963-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_40", "Into Now (1971)", "Into Now"); n = graph.getNode("Into Now"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Into Now (1971)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_fchor_40", "Into Now", "Into Now (1971-11-00)"); n = graph.getNode("Into Now"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Into Now (1971-11-00)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_50", "Soul of the bible (1972)", "Soul of the bible"); n = graph.getNode("Soul of the bible"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Soul of the bible (1972)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_50", "Soul of the bible", "Soul of the bible (1972-00-00)"); n = graph.getNode("Soul of the bible"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Soul of the bible (1972-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_60", "Danses vives (1973)", "Danses vives"); n = graph.getNode("Danses vives"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Danses vives (1973)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_60", "Danses vives", "Danses vives (1973-00-00)"); n = graph.getNode("Danses vives"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Danses vives (1973-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_70", "Mantram (1974)", "Mantram"); n = graph.getNode("Mantram"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Mantram (1974)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_fchor_70", "Mantram", "Mantram (1974-00-00)"); n = graph.getNode("Mantram"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Mantram (1974-00-00)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_80", "Lame de fond (1975)", "Lame de fond"); n = graph.getNode("Lame de fond"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Lame de fond (1975)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_80", "Lame de fond", "Lame de fond (1975-00-00)"); n = graph.getNode("Lame de fond"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Lame de fond (1975-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_90", "Stocker et transporter debout (1976)", "Stocker et transporter debout"); n = graph.getNode("Stocker et transporter debout"); n.setAttribute("ui.class", "doc"); n
		 * = graph.getNode("Stocker et transporter debout (1976)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_90", "Stocker et transporter debout", "Stocker et transporter debout (1976-00-00)"); n = graph.getNode("Stocker et transporter debout"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Stocker et transporter debout (1976-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_100", "Science-fiction (1977)", "Science-fiction"); n = graph.getNode("Science-fiction"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Science-fiction (1977)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_100", "Science-fiction", "Science-fiction (1977-00-00)"); n = graph.getNode("Science-fiction"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Science-fiction (1977-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_110", "Letho (1978)", "Letho"); n = graph.getNode("Letho"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Letho (1978)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_110", "Letho", "Letho (1978-00-00)"); n = graph.getNode("Letho"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Letho (1978-00-00)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_120", "Phase Y (1979)", "Phase Y"); n = graph.getNode("Phase Y"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Phase Y (1979)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_fchor_120", "Phase Y", "Phase Y (1979-00-00)"); n = graph.getNode("Phase Y"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Phase Y (1979-00-00)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_130", "Version originale (1980)", "Version originale"); n = graph.getNode("Version originale"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Version originale (1980)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_130", "Version originale", "Version originale (1980-00-00)"); n = graph.getNode("Version originale"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Version originale (1980-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_140", "Pas dormir (1981)", "Pas dormir"); n = graph.getNode("Pas dormir"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Pas dormir (1981)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_140", "Pas dormir", "Pas dormir (1981-00-00)"); n = graph.getNode("Pas dormir"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Pas dormir (1981-00-00)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_150", "Terrain vague (1982)", "Terrain vague"); n = graph.getNode("Terrain vague"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Terrain vague (1982)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_150", "Terrain vague", "Terrain vague (1982-00-00)"); n = graph.getNode("Terrain vague"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Terrain vague (1982-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_160", "Voyage de l'autre côté (1985)", "Voyage de l'autre côté"); n = graph.getNode("Voyage de l'autre côté"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Voyage de l'autre côté (1985)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_160", "Voyage de l'autre côté", "Voyage de l'autre côté (1985-00-00)"); n = graph.getNode("Voyage de l'autre côté"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Voyage de l'autre côté (1985-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_170", "Triades (1987)", "Triades"); n = graph.getNode("Triades"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Triades (1987)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_fchor_170", "Triades", "Triades (1987-00-00)"); n = graph.getNode("Triades"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Triades (1987-00-00)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_180", "Ici maintenant (1988)", "Ici maintenant"); n = graph.getNode("Ici maintenant"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Ici maintenant (1988)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_180", "Ici maintenant", "Ici maintenant (1988-00-00)"); n = graph.getNode("Ici maintenant"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ici maintenant (1988-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_190", "Chez moi (1989)", "Chez moi"); n = graph.getNode("Chez moi"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Chez moi (1989)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_190", "Chez moi", "Chez moi (1989-00-00)"); n = graph.getNode("Chez moi"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Chez moi (1989-00-00)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_200", "L'inconnu sur la terre (1990)", "L'inconnu sur la terre"); n = graph.getNode("L'inconnu sur la terre"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("L'inconnu sur la terre (1990)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_200", "L'inconnu sur la terre", "L'inconnu sur la terre (1990-00-00)"); n = graph.getNode("L'inconnu sur la terre"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("L'inconnu sur la terre (1990-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_210", "Fratres (1995)", "Fratres"); n = graph.getNode("Fratres"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Fratres (1995)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_fchor_210", "Fratres", "Fratres (1995-00-00)"); n = graph.getNode("Fratres"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Fratres (1995-00-00)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_220", "Moments nomades (1996)", "Moments nomades"); n = graph.getNode("Moments nomades"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Moments nomades (1996)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_220", "Moments nomades", "Moments nomades (1996-00-00)"); n = graph.getNode("Moments nomades"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Moments nomades (1996-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_230", "No man's land (1996)", "No man's land"); n = graph.getNode("No man's land"); n.setAttribute("ui.class", "doc"); n = graph.getNode("No man's land (1996)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_230", "No man's land", "No man's land (1996-00-00)"); n = graph.getNode("No man's land"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("No man's land (1996-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_240", "Patience dans l'azur (1997)", "Patience dans l'azur"); n = graph.getNode("Patience dans l'azur"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Patience dans l'azur (1997)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_240", "Patience dans l'azur", "Patience dans l'azur (1997-00-00)"); n = graph.getNode("Patience dans l'azur"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Patience dans l'azur (1997-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_250", "Insula Deserta (1998)", "Insula Deserta, rushes cour"); n = graph.getNode("Insula Deserta, rushes cour"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta (1998)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_250", "Insula Deserta, rushes cour", "Insula Deserta (1999-11-00)"); n = graph.getNode("Insula Deserta, rushes cour"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta (1999-11-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_251", "Insula Deserta, rushes cour", "Insula Deserta - trio (2000-02-03)"); n = graph.getNode("Insula Deserta, rushes cour"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta - trio (2000-02-03)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_260", "Insula Deserta (1998)", "Insula Deserta, rushes face"); n = graph.getNode("Insula Deserta, rushes face"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta (1998)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_260", "Insula Deserta, rushes face", "Insula Deserta (1999-11-00)"); n = graph.getNode("Insula Deserta, rushes face"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta (1999-11-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_261", "Insula Deserta, rushes face", "Insula Deserta - trio (2000-02-03)"); n = graph.getNode("Insula Deserta, rushes face"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta - trio (2000-02-03)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_270", "Insula Deserta (1998)", "Insula Deserta, rushes jardin"); n = graph.getNode("Insula Deserta, rushes jardin"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta (1998)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_270", "Insula Deserta, rushes jardin", "Insula Deserta (1999-11-00)"); n = graph.getNode("Insula Deserta, rushes jardin"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta (1999-11-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_271", "Insula Deserta, rushes jardin", "Insula Deserta - trio (2000-02-03)"); n = graph.getNode("Insula Deserta, rushes jardin"); n.setAttribute("ui.class", "doc");
		 * n = graph.getNode("Insula Deserta - trio (2000-02-03)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_280", "Insula Deserta (1998)", "Insula Deserta, Théâtre de Colombes"); n = graph.getNode("Insula Deserta, Théâtre de Colombes"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta (1998)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_280", "Insula Deserta, Théâtre de Colombes", "Insula Deserta (1999-11-00)"); n = graph.getNode("Insula Deserta, Théâtre de Colombes"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Insula Deserta (1999-11-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_281", "Insula Deserta, Théâtre de Colombes", "Insula Deserta - trio (2000-02-03)"); n = graph.getNode("Insula Deserta, Théâtre de Colombes");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Insula Deserta - trio (2000-02-03)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_290", "Fall in light (2001)", "Fall in light"); n = graph.getNode("Fall in light"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Fall in light (2001)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_290", "Fall in light", "Fall in light (2001-11-23)"); n = graph.getNode("Fall in light"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Fall in light (2001-11-23)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_300", "Terre du ciel (1993)", "Terre du ciel (extrait)"); n = graph.getNode("Terre du ciel (extrait)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Terre du ciel (1993)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_300", "Terre du ciel (extrait)", "Terre du ciel (1993-06-12)"); n = graph.getNode("Terre du ciel (extrait)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Terre du ciel (1993-06-12)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_310", "Traverse (2002)", "Traverse"); n = graph.getNode("Traverse"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Traverse (2002)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_310", "Traverse", "Traverse (2002-07-02)"); n = graph.getNode("Traverse"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Traverse (2002-07-02)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_330", "Intervalle (2000)", "Intervalle"); n = graph.getNode("Intervalle"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Intervalle (2000)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_330", "Intervalle", "Intervalle (2000-12-22)"); n = graph.getNode("Intervalle"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Intervalle (2000-12-22)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_340", "Traverse (2002)", "Traverse, Théâtre Sylvia Montfort"); n = graph.getNode("Traverse, Théâtre Sylvia Montfort"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Traverse (2002)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_340", "Traverse, Théâtre Sylvia Montfort", "Traverse (2002-07-02)"); n = graph.getNode("Traverse, Théâtre Sylvia Montfort"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Traverse (2002-07-02)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_350", "Guérir la guerre (2004)", "Guérir la guerre"); n = graph.getNode("Guérir la guerre"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Guérir la guerre (2004)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_350", "Guérir la guerre", "Guérir la guerre (2004-11-14)"); n = graph.getNode("Guérir la guerre"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Guérir la guerre (2004-11-14)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_360", "Insula Deserta (1998)", "Insula Deserta - solo"); n = graph.getNode("Insula Deserta - solo"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta (1998)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_360", "Insula Deserta - solo", "Insula Deserta - solo (1998-00-00)"); n = graph.getNode("Insula Deserta - solo"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta - solo (1998-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_370", "L.I.L.A.", "Insula Deserta (1999-11-00)"); n = graph.getNode("L.I.L.A."); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta (1999-11-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_371", "L.I.L.A.", "Insula Deserta - solo (1998-00-00)"); n = graph.getNode("L.I.L.A."); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta - solo (1998-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_380", "Maïysha (2006)", "Maïysha"); n = graph.getNode("Maïysha"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Maïysha (2006)"); n.setAttribute("ui.class",
		 * "or");
		 * graph.addEdge("doc_fchor_380", "Maïysha", "Maïysha (2006-07-05)"); n = graph.getNode("Maïysha"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Maïysha (2006-07-05)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_390", "Son, le mouvement (Le)", "Maïysha (2006-07-05)"); n = graph.getNode("Son, le mouvement (Le)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Maïysha (2006-07-05)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_400", "Insula Deserta (1998)", "Trayectos"); n = graph.getNode("Trayectos"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Insula Deserta (1998)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_400", "Trayectos", "Insula Deserta - solo (1998-00-00)"); n = graph.getNode("Trayectos"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta - solo (1998-00-00)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_410", "Ailes de la gravité (Les) (2008)", "Ailes de la gravité (Les)"); n = graph.getNode("Ailes de la gravité (Les)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ailes de la gravité (Les) (2008)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_410", "Ailes de la gravité (Les)", "Ailes de la gravité (Les) (2008-02-18)"); n = graph.getNode("Ailes de la gravité (Les)"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ailes de la gravité (Les) (2008-02-18)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_420", "Ailes de la gravité (Les) (2008)", "Lumière du vide ; Ailes de la gravité (Les)"); n = graph.getNode("Lumière du vide ; Ailes de la gravité (Les)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Ailes de la gravité (Les) (2008)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_421", "Lumière du vide (2010)", "Lumière du vide ; Ailes de la gravité (Les)"); n = graph.getNode("Lumière du vide ; Ailes de la gravité (Les)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Lumière du vide (2010)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_420", "Lumière du vide ; Ailes de la gravité (Les)", "Ailes de la gravité (Les) (2008-02-18)"); n = graph.getNode("Lumière du vide ; Ailes de la gravité (Les)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Ailes de la gravité (Les) (2008-02-18)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_421", "Lumière du vide ; Ailes de la gravité (Les)", "Lumière du vide (2010-10-08)"); n = graph.getNode("Lumière du vide ; Ailes de la gravité (Les)");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Lumière du vide (2010-10-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_430", "Ailes de la gravité (Les) (2008)", "Pièces en studio 1/2"); n = graph.getNode("Pièces en studio 1/2"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ailes de la gravité (Les) (2008)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_431", "Maïysha (2006)", "Pièces en studio 1/2"); n = graph.getNode("Pièces en studio 1/2"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Maïysha (2006)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_430", "Pièces en studio 1/2", "Ailes de la gravité (Les) (2008-02-18)"); n = graph.getNode("Pièces en studio 1/2"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ailes de la gravité (Les) (2008-02-18)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_431", "Pièces en studio 1/2", "Maïysha (2006-07-05)"); n = graph.getNode("Pièces en studio 1/2"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Maïysha (2006-07-05)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_440", "Ailes de la gravité (Les) (2008)", "Pièces en studio 2/2"); n = graph.getNode("Pièces en studio 2/2"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ailes de la gravité (Les) (2008)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_441", "Maïysha (2006)", "Pièces en studio 2/2"); n = graph.getNode("Pièces en studio 2/2"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Maïysha (2006)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_440", "Pièces en studio 2/2", "Ailes de la gravité (Les) (2008-02-18)"); n = graph.getNode("Pièces en studio 2/2"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Ailes de la gravité (Les) (2008-02-18)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_441", "Pièces en studio 2/2", "Maïysha (2006-07-05)"); n = graph.getNode("Pièces en studio 2/2"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Maïysha (2006-07-05)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_450", "Ailes de la gravité (Les) (2008)", "Soirée Ingeborg Liptay à Beaulieu"); n = graph.getNode("Soirée Ingeborg Liptay à Beaulieu"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Ailes de la gravité (Les) (2008)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_451", "Traverse (2002)", "Soirée Ingeborg Liptay à Beaulieu"); n = graph.getNode("Soirée Ingeborg Liptay à Beaulieu"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Traverse (2002)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_450", "Soirée Ingeborg Liptay à Beaulieu", "Ailes de la gravité (Les) (2008-02-18)"); n = graph.getNode("Soirée Ingeborg Liptay à Beaulieu");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Ailes de la gravité (Les) (2008-02-18)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_451", "Soirée Ingeborg Liptay à Beaulieu", "Traverse (2002-07-02)"); n = graph.getNode("Soirée Ingeborg Liptay à Beaulieu"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Traverse (2002-07-02)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_460", "Bird (2010)", "Insula Deserta (duo) ; Bird"); n = graph.getNode("Insula Deserta (duo) ; Bird"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Bird (2010)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_or_461", "Insula Deserta (1998)", "Insula Deserta (duo) ; Bird"); n = graph.getNode("Insula Deserta (duo) ; Bird"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta (1998)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_460", "Insula Deserta (duo) ; Bird", "Bird (2010-05-26)"); n = graph.getNode("Insula Deserta (duo) ; Bird"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Bird (2010-05-26)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_fchor_461", "Insula Deserta (duo) ; Bird", "Insula Deserta - trio (2000-02-03)"); n = graph.getNode("Insula Deserta (duo) ; Bird"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Insula Deserta - trio (2000-02-03)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_470", "Lumière du vide (2010)", "Ingeborg Liptay, grande leçon de danse"); n = graph.getNode("Ingeborg Liptay, grande leçon de danse"); n.setAttribute("ui.class",
		 * "doc"); n = graph.getNode("Lumière du vide (2010)"); n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_470", "Ingeborg Liptay, grande leçon de danse", "Lumière du vide (2010-10-08)"); n = graph.getNode("Ingeborg Liptay, grande leçon de danse");
		 * n.setAttribute("ui.class", "doc"); n = graph.getNode("Lumière du vide (2010-10-08)"); n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_480", "Nuageange (2013)", "Nuageange"); n = graph.getNode("Nuageange"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Nuageange (2013)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_480", "Nuageange", "Nuageange (2013-01-03)"); n = graph.getNode("Nuageange"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Nuageange (2013-01-03)");
		 * n.setAttribute("ui.class", "fchor");
		 * graph.addEdge("doc_or_490", "Voix du sang (2013)", "Voix du sang"); n = graph.getNode("Voix du sang"); n.setAttribute("ui.class", "doc"); n = graph.getNode("Voix du sang (2013)");
		 * n.setAttribute("ui.class", "or");
		 * graph.addEdge("doc_fchor_490", "Voix du sang", "Voix du sang (2013-05-09)"); n = graph.getNode("Voix du sang"); n.setAttribute("ui.class", "doc"); n =
		 * graph.getNode("Voix du sang (2013-05-09)"); n.setAttribute("ui.class", "fchor");
		 */
		
		
		graph.addEdge("0", "Titre de volume", "Notion de Performance");
		
		n = graph.getNode("Titre de volume");
		n.setAttribute("ui.class", "titre");
		
		
		graph.addEdge("1", "Notion de Performance", "AGENCEMENT");
		
		n = graph.getNode("Notion de Performance");
		n.setAttribute("ui.class", "axe");
		
		n = graph.getNode("AGENCEMENT");
		n.setAttribute("ui.class", "mot");
		
		graph.addEdge("2", "Notion de Performance", "AR D COR");
		n = graph.getNode("AR D COR");
		n.setAttribute("ui.class", "mot");
		
		graph.addEdge("3", "Notion de Performance", "CRISE");
		n = graph.getNode("CRISE");
		n.setAttribute("ui.class", "mot");
		
		graph.addEdge("4", "Notion de Performance", "FRAGILITE");
		n = graph.getNode("FRAGILITE");
		n.setAttribute("ui.class", "mot");
		
		
		graph.addEdge("a5", "FRAGILITE", "I. Barbaris");
		n = graph.getNode("I. Barbaris");
		n.setAttribute("ui.class", "auteur");
		
		
		
		graph.addEdge("b1", "Titre de volume", "Performance et Document");
		
		n = graph.getNode("Performance et Document");
		n.setAttribute("ui.class", "axe");
		
		graph.addEdge("b24", "Performance et Document", "ARCHÉOLOGIQUEMENT");
		
		n = graph.getNode("ARCHÉOLOGIQUEMENT");
		n.setAttribute("ui.class", "mot");
		
		
		
		
		for (Node node : graph) {
			node.addAttribute("ui.label", node.getId());
		}
		
		// explore(graph.getNode("A"));
	}
	
	public void explore(Node source) {
		Iterator<? extends Node> k = source.getBreadthFirstIterator();
		
		while (k.hasNext()) {
			Node next = k.next();
			next.setAttribute("ui.class", "marked");
			sleep();
		}
	}
	
	protected void sleep() {
		try {
			Thread.sleep(1000);
		}
		catch (Exception e) {
		}
	}
	
	/*
	 * FANA
	 * protected String styleSheet =
	 * "node {" +
	 * "   fill-color: black;" +
	 * "   text-size: 12px;" +
	 * "}" +
	 * "node.marked {" +
	 * "   fill-color: red;" +
	 * "}" +
	 * "node.doc {" +
	 * "   fill-color: blue;" +
	 * "   text-color: blue;" +
	 * "}" +
	 * "node.or {" +
	 * "   fill-color: green;" +
	 * "   text-color: green;" +
	 * "}" +
	 * "node.fchor {" +
	 * "   fill-color: red;" +
	 * "   text-color: red;" +
	 * "}";
	 */
	
	// Ouvrage
	protected String styleSheet = "node {" +
	// " fill-color: black;" +
			"   size: 30px, 30px;" +
			"   text-size: 12px;" +
			"   shape: freeplane;" +
			// " size-mode: fit;" +
			"   fill-mode: none;" +
			"   stroke-mode: plain;" +
			"}" +
			"edge {" +
			"   shape: cubic-curve;" +
			
			
			
			// " stroke-mode: plain;" +
			
			"}" +
			/*
			 * "node.marked {" +
			 * "   fill-color: red;" +
			 * "}" +
			 */
			
			"node.titre {" +
			"   size: 55px, 45px;" +
			"   fill-color: blue;" +
			"   text-color: red;" +
			"   shape: diamond;" +
			"}" +
			
			"node.axe {" +
			"   fill-color: blue;" +
			"   text-color: blue;" +
			"   shape: box;" +
			"}" +
			/*
			 * "node.mot {" +
			 * "   fill-color: green;" +
			 * "   text-color: green;" +
			 * "}" +
			 */
			"node.auteur {" +
			"   fill-color: red;" +
			"   text-color: gray;" +
			"   shape: diamond;" +
			"}";
			
			
}
