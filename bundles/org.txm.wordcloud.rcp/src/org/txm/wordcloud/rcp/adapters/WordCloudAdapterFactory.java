// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.wordcloud.rcp.adapters;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.FrameworkUtil;
import org.txm.rcp.adapters.TXMResultAdapter;
import org.txm.rcp.adapters.TXMResultAdapterFactory;
import org.txm.wordcloud.core.functions.WordCloud;

/**
 * A factory for creating Adapter objects.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class WordCloudAdapterFactory extends TXMResultAdapterFactory {


	public static final ImageDescriptor ICON = AbstractUIPlugin.imageDescriptorFromPlugin(FrameworkUtil.getBundle(WordCloudAdapterFactory.class).getSymbolicName(),
			"platform:/plugin/" + FrameworkUtil.getBundle(WordCloudAdapterFactory.class).getSymbolicName() + "/icons/functions/wordcloud.png"); //$NON-NLS-1$ //$NON-NLS-2$



	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (this.canAdapt(adapterType) && adaptableObject instanceof WordCloud) {
			return new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return ICON;
				}
			};
		}
		return null;
	}


}
