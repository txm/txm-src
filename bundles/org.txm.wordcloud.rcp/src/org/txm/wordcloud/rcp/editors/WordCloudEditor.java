package org.txm.wordcloud.rcp.editors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.core.results.Parameter;
import org.txm.rcp.swt.widget.FloatSpinner;
import org.txm.rcp.swt.widget.ThresholdsGroup;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;
import org.txm.wordcloud.core.functions.WordCloud;
import org.txm.wordcloud.core.preferences.WordCloudPreferences;
import org.txm.wordcloud.rcp.messages.WordCloudUIMessages;

/**
 * Word cloud editor.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class WordCloudEditor extends ChartEditor<WordCloud> {


	@Parameter(key = WordCloudPreferences.F_MIN)
	protected Spinner fMinSpinner;

	@Parameter(key = WordCloudPreferences.V_MAX)
	protected Spinner vMaxSpinner;

	@Parameter(key = WordCloudPreferences.ROTATION_PERCENT)
	protected FloatSpinner rotationPercent;

	@Parameter(key = WordCloudPreferences.RANDOM_POSITIONS)
	protected Button randomPositions;



	@Override
	public void __createPartControl() throws CqiClientException {

		Composite extendedParametersGroup = this.getExtendedParametersGroup();

		// extended parameters

		// Filters
		ThresholdsGroup thresholdsGroup = new ThresholdsGroup(extendedParametersGroup, this, false);
		this.fMinSpinner = thresholdsGroup.getFMinSpinner();
		this.vMaxSpinner = thresholdsGroup.getVMaxSpinner();

		// fmin
		this.fMinSpinner.setMinimum(1);
		this.fMinSpinner.setMaximum(100000000);

		// max words / vmax
		this.vMaxSpinner.setMinimum(10);
		CQPCorpus corpus = getResult().getFirstParent(MainCorpus.class);
		if (corpus != null) {
			this.vMaxSpinner.setMaximum(corpus.getSize());
			this.fMinSpinner.setMaximum(corpus.getSize());
		}

		// rotation percent
		new Label(extendedParametersGroup, SWT.NONE).setText(WordCloudUIMessages.percentOfRotatedLabelsColon);
		rotationPercent = new FloatSpinner(extendedParametersGroup, SWT.BORDER);
		rotationPercent.setMaximumAsFloat(100);

		// random positions
		randomPositions = new Button(extendedParametersGroup, SWT.CHECK);
		randomPositions.setText(WordCloudUIMessages.randomizePositions);


		// FIXME: temporary fix, later must enable the font, rendering mode, etc. in WordCloud R calls
		// disable all component of the rendering tool bar
		this.advancedChartToolBar.setEnabled(false);

		// extendedParametersGroup.pack();

		Log.info(WordCloudUIMessages.bind(WordCloudUIMessages.wordCloudReadyColonP0, this.getResult()));
	}



	@Override
	public void updateEditorFromChart(boolean update) {
		// Nothing to do
	}

}
