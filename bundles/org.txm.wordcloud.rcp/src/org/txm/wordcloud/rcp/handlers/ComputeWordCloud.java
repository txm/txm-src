// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.wordcloud.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.index.core.functions.Index;
import org.txm.rcp.StatusLine;
import org.txm.rcp.commands.OpenBrowser;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.logger.Log;
import org.txm.wordcloud.core.functions.WordCloud;
import org.txm.wordcloud.core.messages.WordCloudCoreMessages;

/**
 * Computes and opens a WordCloud editor.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class ComputeWordCloud extends BaseAbstractHandler {


	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {

		if (!this.checkStatsEngine()) {
			return null;
		}

		Object selection = this.getCorporaViewSelectedObject(event);
		WordCloud wordCloud = null;

		// Reopening an existing result
		if (selection instanceof WordCloud) {
			wordCloud = ((WordCloud) selection);
		}
		else {
			// Inits R packages
			if (!WordCloud.initPackages()) {
				OpenBrowser.openfile("https://groupes.renater.fr/wiki/txm-users/public/extensions#wordcloud"); //$NON-NLS-1$
			}

			// Creating from Corpus
			if (selection instanceof CQPCorpus) {
				try {
					CQPCorpus corpus = (CQPCorpus) selection;
					String lang = corpus.getLang();
					CQLQuery query;
					if (corpus.getProperty(lang + "pos") != null) { //$NON-NLS-1$
						query = new CQLQuery(WordCloud.getQueryForLang(lang));
					}
					else {
						query = new CQLQuery("[]"); //$NON-NLS-1$
					}

					System.out.println("Used query: " + query); //$NON-NLS-1$

					Index index = new Index((CQPCorpus) selection);
					index.setQuery(query);
					index.setProperty(corpus.getWordProperty());

					wordCloud = new WordCloud(index);
				}
				catch (CqiClientException e) {
					Log.printStackTrace(e);
				}
			}
			// Creating from Index
			else if (selection instanceof Index) {
				wordCloud = new WordCloud((Index) selection);
			}
			// Error
			else {
				return super.logCanNotExecuteCommand(selection);
			}

		}

		StatusLine.setMessage(WordCloudCoreMessages.info_openingWordCloudResult);

		ChartEditor.openEditor(wordCloud);

		return null;
	}
}
