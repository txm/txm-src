package org.txm.wordcloud.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Word cloud UI messages.
 * 
 * @author sjacquot
 *
 */
public class WordCloudUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.wordcloud.rcp.messages.messages"; //$NON-NLS-1$



	public static String percentOfRotatedLabelsColon;

	public static String randomizePositions;

	public static String wordCloudReadyColonP0;

	public static String numberOfUsedWords;

	public static String minimalFrequencyForUsedWords;

	public static String percentOfRotatedWords;

	public static String randomWordPositions;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, WordCloudUIMessages.class);
	}

	private WordCloudUIMessages() {
	}
}
