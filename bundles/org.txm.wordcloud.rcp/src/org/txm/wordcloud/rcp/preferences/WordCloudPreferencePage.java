package org.txm.wordcloud.rcp.preferences;

// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté

// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//


import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.wordcloud.core.preferences.WordCloudPreferences;
import org.txm.wordcloud.rcp.adapters.WordCloudAdapterFactory;
import org.txm.wordcloud.rcp.messages.WordCloudUIMessages;

/**
 * Word cloud preference page.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class WordCloudPreferencePage extends TXMPreferencePage {


	@Override
	public void createFieldEditors() {

		
		
		IntegerFieldEditor fMin = new IntegerFieldEditor(TXMPreferences.F_MIN, TXMCoreMessages.common_fMin, this.getFieldEditorParent());
		fMin.getTextControl(this.getFieldEditorParent()).setToolTipText(TXMCoreMessages.minimumFrequencyThresholdCommaAllValuesBelowFminAreIgnored);
		this.addField(fMin);
		
		IntegerFieldEditor vMax = new IntegerFieldEditor(TXMPreferences.V_MAX, TXMCoreMessages.common_vMax, this.getFieldEditorParent());
		vMax.getTextControl(this.getFieldEditorParent()).setToolTipText(TXMCoreMessages.listTruncationThresholdCommaOnlyTheMostFrequentFirstVmaxLinesAreRetained);
		this.addField(vMax);
		
		
		// Charts rendering
		Composite chartsTab = SWTChartsComponentsProvider.createChartsRenderingPreferencesTabFolderComposite(this.getFieldEditorParent());

//		this.addField(new IntegerFieldEditor(WordCloudPreferences.V_MAX, WordCloudUIMessages.numberOfUsedWords, chartsTab));
//		this.addField(new IntegerFieldEditor(WordCloudPreferences.F_MIN, WordCloudUIMessages.minimalFrequencyForUsedWords, chartsTab));

		this.addField(new IntegerFieldEditor(WordCloudPreferences.ROTATION_PERCENT, WordCloudUIMessages.percentOfRotatedWords, chartsTab));
		this.addField(new BooleanFieldEditor(WordCloudPreferences.RANDOM_POSITIONS, WordCloudUIMessages.randomWordPositions, chartsTab));

		// other shared preferences
		SWTChartsComponentsProvider.createChartsRenderingPreferencesFields(this, chartsTab);

	}


	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(WordCloudPreferences.getInstance().getPreferencesNodeQualifier()));
		// this.setTitle("WordCloud");
		this.setImageDescriptor(WordCloudAdapterFactory.ICON);
	}
}
