package org.eclipse.jface.fieldassist;

import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.swt.widgets.Control;

/**
 * AutoCompleteField is a class which attempts to auto-complete a user's
 * keystrokes by activating a popup that filters a list of proposals according
 * to the content typed by the user.
 * 
 * @see ContentProposalAdapter
 * @see SimpleContentProposalProvider
 * 
 * @since 3.3
 */
public class TXMAutoCompleteField {

	private TXMSimpleContentProposalProvider proposalProvider;

	private TXMContentProposalAdapter adapter;

	boolean open = false;

	public boolean isOpen() {
		return adapter.isProposalPopupOpen();
	};

	/**
	 * Construct an AutoComplete field on the specified control, whose
	 * completions are characterized by the specified array of Strings.
	 * 
	 * @param control
	 *            the control for which autocomplete is desired. May not be
	 *            <code>null</code>.
	 * @param controlContentAdapter
	 *            the <code>IControlContentAdapter</code> used to obtain and
	 *            update the control's contents. May not be <code>null</code>.
	 * @param proposals
	 *            the array of Strings representing valid content proposals for
	 *            the field.
	 */
	public TXMAutoCompleteField(Control control,
			IControlContentAdapter controlContentAdapter, String[] proposals, KeyStroke keyStroke) {
		this(control, controlContentAdapter, proposals, keyStroke, null);
	}

	/**
	 * Construct an AutoComplete field on the specified control, whose
	 * completions are characterized by the specified array of Strings.
	 * 
	 * @param control
	 *            the control for which autocomplete is desired. May not be
	 *            <code>null</code>.
	 * @param controlContentAdapter
	 *            the <code>IControlContentAdapter</code> used to obtain and
	 *            update the control's contents. May not be <code>null</code>.
	 * @param proposals
	 *            the array of Strings representing valid content proposals for
	 *            the field.
	 */
	public TXMAutoCompleteField(Control control,
			IControlContentAdapter controlContentAdapter, String[] proposals, KeyStroke keyStroke, char[] autoActivationCharacters) {
		proposalProvider = new TXMSimpleContentProposalProvider(proposals);
		proposalProvider.setFiltering(true);

		adapter = new TXMContentProposalAdapter(control, controlContentAdapter, proposalProvider, keyStroke, autoActivationCharacters);
		adapter.setPropagateKeys(true);
		adapter.setProposalAcceptanceStyle(ContentProposalAdapter.PROPOSAL_REPLACE);
		adapter.addContentProposalListener(new IContentProposalListener2() {

			@Override
			public void proposalPopupOpened(ContentProposalAdapter adapter) {
				open = true;
			}

			@Override
			public void proposalPopupClosed(ContentProposalAdapter adapter) {
				open = false;
			}
		});
	}

	/**
	 * Construct an AutoComplete field on the specified control, whose
	 * completions are characterized by the specified array of Strings.
	 * 
	 * @param control
	 *            the control for which autocomplete is desired. May not be
	 *            <code>null</code>.
	 * @param controlContentAdapter
	 *            the <code>IControlContentAdapter</code> used to obtain and
	 *            update the control's contents. May not be <code>null</code>.
	 * @param proposals
	 *            the array of Strings representing valid content proposals for
	 *            the field.
	 */
	public TXMAutoCompleteField(Control control,
			IControlContentAdapter controlContentAdapter, String[] proposals) {
		this(control, controlContentAdapter, proposals, null, null);
	}

	/**
	 * Set the Strings to be used as content proposals.
	 * 
	 * @param proposals
	 *            the array of Strings to be used as proposals.
	 */
	public void setProposals(String[] proposals) {
		proposalProvider.setProposals(proposals);
	}

	public void open() {
		if (open) adapter.close();
		else adapter.open();
	}
}
