/*******************************************************************************
 * Copyright (c) 2005, 2013 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     Hannes Erven <hannes@erven.at> - Bug 293841 - [FieldAssist] NumLock keyDown event should not close the proposal popup [with patch]
 *******************************************************************************/
package org.eclipse.jface.fieldassist;

import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.swt.widgets.Control;

/**
 * ContentProposalAdapter can be used to attach content proposal behavior to a
 * control. This behavior includes obtaining proposals, opening a popup dialog,
 * managing the content of the control relative to the selections in the popup,
 * and optionally opening up a secondary popup to further describe proposals.
 * <p>
 * A number of configurable options are provided to determine how the control
 * content is altered when a proposal is chosen, how the content proposal popup
 * is activated, and whether any filtering should be done on the proposals as
 * the user types characters.
 * <p>
 * This class provides some overridable methods to allow clients to manually
 * control the popup. However, most of the implementation remains private.
 * 
 * @since 3.2
 */
public class TXMContentProposalAdapter extends ContentProposalAdapter {

	public TXMContentProposalAdapter(Control control, IControlContentAdapter controlContentAdapter, IContentProposalProvider proposalProvider, KeyStroke keyStroke, char[] autoActivationCharacters) {
		super(control, controlContentAdapter, proposalProvider, keyStroke, autoActivationCharacters);
		// TODO Auto-generated constructor stub
	}

	public void open() {
		super.openProposalPopup();
	}

	public void close() {
		super.closeProposalPopup();
	}
}
