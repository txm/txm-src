package org.txm.rcp.editors;

import org.txm.searchengine.cqp.corpus.CQPCorpus;

public interface IEditionEditor {

	public String getTextSelection();

	public CQPCorpus getCorpus();
}
