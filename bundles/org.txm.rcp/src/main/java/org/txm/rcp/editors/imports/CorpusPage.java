// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.editors.imports;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Hyperlink;
import org.eclipse.ui.forms.widgets.ImageHyperlink;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.txm.Toolbox;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.commands.workspace.UpdateCorpus;
import org.txm.rcp.corpuswizard.ImportWizard;
import org.txm.rcp.editors.imports.sections.CommandsSection;
import org.txm.rcp.editors.imports.sections.EditionSection;
import org.txm.rcp.editors.imports.sections.EncodingSection;
import org.txm.rcp.editors.imports.sections.FontSection;
import org.txm.rcp.editors.imports.sections.FrontXSLSection;
import org.txm.rcp.editors.imports.sections.ImportEditorSection;
import org.txm.rcp.editors.imports.sections.InfosSection;
import org.txm.rcp.editors.imports.sections.LangSection;
import org.txm.rcp.editors.imports.sections.OptionsSection;
import org.txm.rcp.editors.imports.sections.StructuresSection;
import org.txm.rcp.editors.imports.sections.TextualPlansSection;
import org.txm.rcp.editors.imports.sections.WordsSection;
import org.txm.rcp.handlers.scripts.ExecuteImportScript;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.RCPPreferences;
import org.txm.rcp.swt.TXMFontRegistry;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.rcp.views.fileexplorer.Explorer;
import org.txm.utils.logger.Log;
import org.xml.sax.SAXException;

/**
 * The Class CorpusPage.
 * set corpus parameters
 */
public class CorpusPage extends FormPage {

	private static String lastOpenedSrcDir;

	ImportFormEditor editor;

	ScrolledForm form;

	FormToolkit toolkit;

	HashMap<String, Boolean> moduleParams; // the import sections configuration depends on the import module selected

	// All the sections that populate the import parameter file
	Section pattrSection;

	Section preBuildSection;

	Section querySection;

	Section sattrSection;

	ImportEditorSection uiSection;

	ImportEditorSection xsltSection;

	ImportEditorSection tokenizerSection;

	ImportEditorSection editionSection;

	ImportEditorSection encodingSection;

	ImportEditorSection fontSection;

	ImportEditorSection infosSection;

	ImportEditorSection langSection;

	ImportEditorSection textualPlansSection;

	ImportEditorSection structuresSection;

	ImportEditorSection optionsSection;

	/**
	 * Import module custom import section, see ImportModuleCustomization.getAdditionalSection(name)
	 */
	ImportEditorSection additionalSection;

	// old widgets to be moved/deleted
	TableViewer preBuildViewer;

	TreeViewer preBuildViewer2;

	TableViewer queriesViewer;

	TableViewer sattributesViewer;

	TableViewer editionsViewer;

	TableViewer viewer;

	TableViewer pattributesViewer;

	//private Project project;

	/**
	 * Instantiates a new main page.
	 *
	 * @param editor the editor
	 */
	public CorpusPage(ImportFormEditor editor) {
		super(editor, TXMUIMessages.parameters, TXMUIMessages.parameters);
		this.editor = editor;
	}

	private int createEditionsSection() {
		editionSection = new EditionSection(editor, toolkit, form, form.getBody(), ExpandableComposite.TITLE_BAR | ExpandableComposite.TWISTIE, moduleParams, editor.getImportName());
		return editionSection.getSectionSize();
	}

	private int createEmptyCell() {
		Label l = toolkit.createLabel(form.getBody(), "", SWT.NONE); //$NON-NLS-1$
		TableWrapData td = new TableWrapData();
		td.colspan = 1;
		l.setLayoutData(td);

		return 1;
	}

	private int createEncodingSection() {
		encodingSection = new EncodingSection(editor, toolkit, form, form.getBody(), ExpandableComposite.TITLE_BAR | ExpandableComposite.TWISTIE);
		return encodingSection.getSectionSize();
	}

	private int createFontSection() {
		fontSection = new FontSection(editor, toolkit, form, form.getBody(), ExpandableComposite.TITLE_BAR | ExpandableComposite.TWISTIE);
		return fontSection.getSectionSize();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.forms.editor.FormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
	 */
	@Override
	protected void createFormContent(final IManagedForm managedForm) {
		form = managedForm.getForm();
		toolkit = managedForm.getToolkit();
		TableWrapLayout layout = new TableWrapLayout();
		layout.numColumns = 2;
		layout.makeColumnsEqualWidth = false;

		form.getBody().setLayout(layout);

		createToolbar();

		String importName = editor.getImportName();
		moduleParams = ImportModuleCustomization.getParameters(importName);

		// CREATE THE SECTIONS
		int N_SECTIONS = 0;
		N_SECTIONS += createInfosSection(); // mandatory

		if (moduleParams.get(ImportModuleCustomization.ENCODING)) {
			N_SECTIONS += createEncodingSection();
		}

		if (moduleParams.get(ImportModuleCustomization.LANG)) {
			N_SECTIONS += createLangSection();
		}

		if (moduleParams.get(ImportModuleCustomization.WORDS)) {
			N_SECTIONS += createTokenizerSection();
		}

		if (moduleParams.get(ImportModuleCustomization.XSLT)) {
			if (N_SECTIONS % 2 != 0) { // needs 2 cells to draw
				N_SECTIONS += createEmptyCell();
			}
			N_SECTIONS += createFrontXSLTSection();
		}

		if (moduleParams.get(ImportModuleCustomization.EDITIONS_PAGEELEMENT) || moduleParams.get(ImportModuleCustomization.EDITIONS_WORDSPERPAGE)) {
			N_SECTIONS += createEditionsSection();
		}

		N_SECTIONS += createFontSection();

		if (moduleParams.get(ImportModuleCustomization.UI)) {
			N_SECTIONS += createUIsSection();
		}

		if (moduleParams.get(ImportModuleCustomization.TEXTUALPLANS)) {
			N_SECTIONS += createTextualPlansSection();
		}
		if (moduleParams.get(ImportModuleCustomization.STRUCTURES)) {
			N_SECTIONS += createStructuresSection();
		}
		if (moduleParams.get(ImportModuleCustomization.OPTIONS)) {
			N_SECTIONS += createOptionsSection();
		}

		N_SECTIONS += createAdditionalSection();

		// END OF SECTIONS CREATION, ENSURE THAT THE NUMBER OF CELL is %2
		if (N_SECTIONS % 2 != 0) N_SECTIONS += createEmptyCell();

		// create notes messages
		Label separatorLabel = toolkit.createLabel(form.getBody(), "", SWT.SEPARATOR | SWT.HORIZONTAL); //$NON-NLS-1$
		TableWrapData gdata = new TableWrapData(TableWrapData.FILL_GRAB, TableWrapData.MIDDLE, 2, 1);
		gdata.colspan = 2;
		separatorLabel.setLayoutData(gdata);

		Label mandatoryLabel = toolkit.createLabel(form.getBody(), TXMUIMessages.dDotMandatoryField, SWT.WRAP);
		gdata = new TableWrapData(TableWrapData.FILL, TableWrapData.MIDDLE, 2, 1);
		gdata.colspan = 2;
		mandatoryLabel.setLayoutData(gdata);

		form.layout(true);
		managedForm.reflow(true);

		if (editor.project != null) { // the project is already set, all the select sources widget are disable
			initializeProjectSettings();
		}
	}

	private int createTextualPlansSection() {
		textualPlansSection = new TextualPlansSection(editor, toolkit, form, form.getBody(), ExpandableComposite.TITLE_BAR | ExpandableComposite.TWISTIE);
		return textualPlansSection.getSectionSize();
	}

	private int createStructuresSection() {
		structuresSection = new StructuresSection(editor, toolkit, form, form.getBody(), ExpandableComposite.TITLE_BAR | ExpandableComposite.TWISTIE);
		return structuresSection.getSectionSize();
	}

	private int createOptionsSection() {
		optionsSection = new OptionsSection(editor, toolkit, form, form.getBody(), ExpandableComposite.TITLE_BAR | ExpandableComposite.TWISTIE);
		return optionsSection.getSectionSize();
	}

	private int createAdditionalSection() {
		String importName = editor.getImportName();

		if (ImportModuleCustomization.getAdditionalSection(importName) != null) {
			Class<? extends ImportEditorSection> clazz = ImportModuleCustomization.getAdditionalSection(importName);

			try {
				Constructor<? extends ImportEditorSection> c = clazz.getConstructor(ImportFormEditor.class, FormToolkit.class, ScrolledForm.class, Composite.class, int.class);
				additionalSection = c.newInstance(editor, toolkit, form, form.getBody(), ExpandableComposite.TITLE_BAR | ExpandableComposite.TWISTIE);
				return additionalSection.getSectionSize();
			}
			catch (Exception e) {
				Log.printStackTrace(e);
			}
		}
		return 0;
	}

	private int createInfosSection() {
		infosSection = new InfosSection(editor, toolkit, form, form.getBody(), ExpandableComposite.TITLE_BAR | ExpandableComposite.TWISTIE);
		return infosSection.getSectionSize();
	}

	/**
	 * Creates the front xslt section.
	 */
	private int createFrontXSLTSection() {
		xsltSection = new FrontXSLSection(editor, toolkit, form, form.getBody(), ExpandableComposite.TITLE_BAR | ExpandableComposite.TWISTIE);
		return xsltSection.getSectionSize();
	}

	/**
	 * Creates the lang section.
	 */
	private int createLangSection() {
		langSection = new LangSection(editor, toolkit, form, form.getBody(), ExpandableComposite.TITLE_BAR | ExpandableComposite.TWISTIE);
		return langSection.getSectionSize();
	}

	private void createPAttributesSection() {
		/*
		 * pattrSection = toolkit.createSection(form.getBody(), ExpandableComposite.TITLE_BAR|ExpandableComposite.TWISTIE);
		 * pattrSection.setText(Messages.CorpusPage_10);
		 * GridLayout layout = new GridLayout(1, true);
		 * pattrSection.setLayout(layout);
		 * pattrSection.setLayoutData(getSectionGridData(2));
		 * pattrSection.setEnabled(false);
		 * pattrSection.addExpansionListener(new ExpansionAdapter() {
		 * @Override
		 * public void expansionStateChanged(ExpansionEvent e) {form.layout(true);}
		 * });
		 * Composite sectionClient = toolkit.createComposite(pattrSection);
		 * sectionClient.setLayout(new GridLayout(2, false));
		 * pattrSection.setClient(sectionClient);
		 * Button addButton = toolkit.createButton(sectionClient, Messages.CorpusPage_11, SWT.PUSH);
		 * addButton.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		 * addButton.addSelectionListener(new SelectionListener() {
		 * @Override
		 * public void widgetDefaultSelected(SelectionEvent e) {
		 * }
		 * @Override
		 * public void widgetSelected(SelectionEvent e) {
		 * if (params == null) return;
		 * LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();
		 * values.put("id", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("shortname", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("longname", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("pattern", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("renderer", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("tooltip", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("type", "String"); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("import", "true"); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("mandatory", "true"); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("inputFormat", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("outputFormat", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * MultipleValueDialog dlg = new MultipleValueDialog(form.getShell(), Messages.CorpusPage_12, values);
		 * if (dlg.open() == Window.OK) {
		 * params.addPAttribute(params.getCorpusElement(), values.get("id"), values.get("shortname"), values.get("longname"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		 * values.get("type"), values.get("renderer"), values.get("tooltip"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		 * values.get("pattern"), values.get("import"), values.get("mandatory"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		 * values.get("inputFormat"), values.get("outputFormat")); //$NON-NLS-1$ //$NON-NLS-2$
		 * updatePAttributeSection();
		 * }
		 * }
		 * });
		 * Button removeButton = toolkit.createButton(sectionClient, Messages.CorpusPage_13, SWT.PUSH);
		 * removeButton.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		 * removeButton.addSelectionListener(new SelectionListener() {
		 * @Override
		 * public void widgetDefaultSelected(SelectionEvent e) {
		 * }
		 * @Override
		 * public void widgetSelected(SelectionEvent e) {
		 * if (params == null) return;
		 * ISelection sel = pattributesViewer.getSelection();
		 * IStructuredSelection ssel = (IStructuredSelection) sel;
		 * Object obj = ssel.getFirstElement();
		 * if (obj instanceof Element) {
		 * Element query = (Element) obj;
		 * params.getPAttributesElement(params.getCorpusElement()).removeChild(query);
		 * updatePAttributeSection();
		 * }
		 * }
		 * });
		 * Table table = toolkit.createTable(sectionClient, SWT.FULL_SELECTION | SWT.MULTI | SWT.V_SCROLL | SWT.VIRTUAL);
		 * pattributesViewer = new TableViewer(table);
		 * pattributesViewer.getTable().addKeyListener(new TableKeyListener(pattributesViewer));
		 * GridData gd = new GridData(GridData.FILL, GridData.FILL, false, false,2,1);
		 * gd.heightHint = 150;
		 * pattributesViewer.getTable().setLayoutData(gd);
		 * pattributesViewer.setContentProvider(new NodeListContentProvider());
		 * pattributesViewer.getTable().setHeaderVisible(true);
		 * pattributesViewer.getTable().setLinesVisible(true);
		 * String[] cols = {"id", "shortname", "longname", "import", "mandatory", "pattern", "renderer", "tooltip", "type"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		 * //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$
		 * for (String col : cols) {
		 * TableViewerColumn columnViewer = new TableViewerColumn(pattributesViewer, SWT.NONE);
		 * TableColumn column = columnViewer.getColumn();
		 * column.setText(col);
		 * column.setWidth(100);
		 * columnViewer.setLabelProvider(new ElementColumnLabelProvider(col));
		 * }
		 */
	}

	private void createPreBuildSection() {
		/*
		 * preBuildSection = toolkit.createSection(form.getBody(), ExpandableComposite.TITLE_BAR|ExpandableComposite.TWISTIE);
		 * preBuildSection.setText(Messages.CorpusPage_14);
		 * GridLayout layout = new GridLayout(1, true);
		 * preBuildSection.setLayout(layout);
		 * preBuildSection.setLayoutData(getSectionGridData(2));
		 * preBuildSection.setEnabled(false);
		 * preBuildSection.addExpansionListener(new ExpansionAdapter() {
		 * @Override
		 * public void expansionStateChanged(ExpansionEvent e) {form.layout(true);}
		 * });
		 * Composite sectionClient = toolkit.createComposite(preBuildSection);
		 * sectionClient.setLayout(new GridLayout(4, false));
		 * preBuildSection.setClient(sectionClient);
		 * Button addButton = toolkit.createButton(sectionClient, Messages.CorpusPage_15, SWT.PUSH);
		 * addButton.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		 * addButton.addSelectionListener(new SelectionListener() {
		 * @Override
		 * public void widgetDefaultSelected(SelectionEvent e) {
		 * }
		 * @Override
		 * public void widgetSelected(SelectionEvent e) {
		 * if (params == null) return;
		 * LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();
		 * values.put("subcorpus", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("query", "[]"); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("desc", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * MultipleValueDialog dlg = new MultipleValueDialog(form.getShell(), Messages.CorpusPage_15, values);
		 * if (dlg.open() == Window.OK) {
		 * params.addSubcorpus(params.getCorpusElement(), values.get("subcorpus"), values.get("query"), values.get("desc")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		 * updatePreBuildSection();
		 * }
		 * }
		 * });
		 * Button removeButton = toolkit.createButton(sectionClient, Messages.CorpusPage_17, SWT.PUSH);
		 * removeButton.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		 * removeButton.addSelectionListener(new SelectionListener() {
		 * @Override
		 * public void widgetDefaultSelected(SelectionEvent e) {
		 * }
		 * @Override
		 * public void widgetSelected(SelectionEvent e) {
		 * if (params == null) return;
		 * ISelection sel = preBuildViewer.getSelection();
		 * IStructuredSelection ssel = (IStructuredSelection) sel;
		 * Object obj = ssel.getFirstElement();
		 * if (obj instanceof Element) {
		 * Element subcorpus = (Element) obj;
		 * System.out.println("SELECTED: "+subcorpus); //$NON-NLS-1$
		 * params.getPreBuildElement(params.getCorpusElement()).removeChild(subcorpus);
		 * updatePreBuildSection();
		 * }
		 * }
		 * });
		 * Button add2Button = toolkit.createButton(sectionClient, Messages.CorpusPage_18, SWT.PUSH);
		 * add2Button.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		 * add2Button.addSelectionListener(new SelectionListener() {
		 * @Override
		 * public void widgetDefaultSelected(SelectionEvent e) {
		 * }
		 * @Override
		 * public void widgetSelected(SelectionEvent e) {
		 * if (params == null) return;
		 * LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();
		 * values.put("partition", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * ArrayList<LinkedHashMap<String, String>> subvaluesArray = new ArrayList<LinkedHashMap<String, String>>();
		 * LinkedHashMap<String, String> subvalues = new LinkedHashMap<String, String>();
		 * subvaluesArray.add(subvalues);
		 * subvalues.put("part", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * subvalues.put("query", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * MultipleValueTreeDialog dlg = new MultipleValueTreeDialog(form.getShell(), Messages.CorpusPage_18, values, subvaluesArray, "part"); //$NON-NLS-1$
		 * if (dlg.open() == Window.OK) {
		 * String[] names = new String[subvaluesArray.size()];
		 * String[] queries = new String[subvaluesArray.size()];
		 * for (int i = 0 ; i < subvaluesArray.size() ; i++) {
		 * LinkedHashMap<String, String> it = subvaluesArray.get(i);
		 * names[i] = it.get("part"); //$NON-NLS-1$
		 * queries[i] = it.get("query"); //$NON-NLS-1$
		 * }
		 * params.addPartition(params.getCorpusElement(), values.get("partition"), queries, names); //$NON-NLS-1$
		 * updatePreBuildSection();
		 * }
		 * }
		 * });
		 * Button remove2Button = toolkit.createButton(sectionClient, Messages.CorpusPage_20, SWT.PUSH);
		 * remove2Button.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		 * remove2Button.addSelectionListener(new SelectionListener() {
		 * @Override
		 * public void widgetDefaultSelected(SelectionEvent e) {
		 * }
		 * @Override
		 * public void widgetSelected(SelectionEvent e) {
		 * if (params == null) return;
		 * ISelection sel = preBuildViewer2.getSelection();
		 * IStructuredSelection ssel = (IStructuredSelection) sel;
		 * Object obj = ssel.getFirstElement();
		 * System.out.println("SELECTED: "+obj); //$NON-NLS-1$
		 * if (obj instanceof Element) {
		 * Element partitionElem = (Element) obj;
		 * if (partitionElem.getParentNode().equals(params.getPreBuildElement(params.getCorpusElement())))
		 * { params.getPreBuildElement(params.getCorpusElement()).removeChild(partitionElem);
		 * updatePreBuildSection();
		 * }
		 * }
		 * }
		 * });
		 * Table table = toolkit.createTable(sectionClient, SWT.FULL_SELECTION | SWT.MULTI | SWT.V_SCROLL | SWT.VIRTUAL);
		 * preBuildViewer = new TableViewer(table);
		 * preBuildViewer.getTable().addKeyListener(new TableKeyListener(preBuildViewer));
		 * GridData gd = new GridData(GridData.FILL, GridData.FILL, false, false,1,1);
		 * gd.heightHint = 150;
		 * preBuildViewer.getTable().setLayoutData(gd);
		 * preBuildViewer.setContentProvider(new NodeListContentProvider());
		 * preBuildViewer.getTable().setHeaderVisible(true);
		 * preBuildViewer.getTable().setLinesVisible(true);
		 * String[] cols = {"name", "query", "desc"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		 * for (String col : cols) {
		 * TableViewerColumn columnViewer = new TableViewerColumn(preBuildViewer, SWT.NONE);
		 * TableColumn column = columnViewer.getColumn();
		 * column.setText(col);
		 * column.setWidth(100);
		 * columnViewer.setLabelProvider(new ElementColumnLabelProvider(col));
		 * }
		 * Tree table2 = toolkit.createTree(sectionClient, SWT.FULL_SELECTION | SWT.MULTI | SWT.V_SCROLL | SWT.VIRTUAL);
		 * preBuildViewer2 = new TreeViewer(table2);
		 * GridData gd2 = new GridData(GridData.FILL, GridData.FILL, false, false, 3, 1);
		 * gd2.heightHint = 150;
		 * preBuildViewer2.getTree().setLayoutData(gd2);
		 * preBuildViewer2.getTree().setHeaderVisible(true);
		 * preBuildViewer2.getTree().setLinesVisible(true);
		 * String[] cols1 = {"name"}; //$NON-NLS-1$
		 * String[] cols2 = {"name", "query"}; //$NON-NLS-1$ //$NON-NLS-2$
		 * preBuildViewer2.setContentProvider(new ElementTreeContentProvider("partition", "part", cols1, cols2)); //$NON-NLS-1$ //$NON-NLS-2$
		 * //preBuildViewer2.setLabelProvider(new ElementTreeLabelProvider("partition", "part", cols1, cols2));
		 * int c = 0;
		 * for (String col : cols2) {
		 * TreeViewerColumn columnViewer = new TreeViewerColumn(preBuildViewer2, SWT.NONE);
		 * TreeColumn column = columnViewer.getColumn();
		 * column.setText(col);
		 * column.setWidth(100);
		 * columnViewer.setLabelProvider(new ElementTreeLabelProvider(c++, "partition", "part", cols1, cols2)); //$NON-NLS-1$ //$NON-NLS-2$
		 * }
		 */
	}

	private void createQueriesSection() {
		/*
		 * querySection = toolkit.createSection(form.getBody(), ExpandableComposite.TITLE_BAR|ExpandableComposite.TWISTIE);
		 * querySection.setText("Queries"); //$NON-NLS-1$
		 * GridLayout layout = new GridLayout(1, true);
		 * querySection.setLayout(layout);
		 * querySection.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false, 2, 1));
		 * querySection.setEnabled(false);
		 * querySection.addExpansionListener(new ExpansionAdapter() {
		 * @Override
		 * public void expansionStateChanged(ExpansionEvent e) {form.layout(true);}
		 * });
		 * //filesection.setDescription("Select how to find source files");
		 * Composite sectionClient = toolkit.createComposite(querySection);
		 * sectionClient.setLayout(new GridLayout(2, false));
		 * querySection.setClient(sectionClient);
		 * Button addButton = toolkit.createButton(sectionClient, Messages.CorpusPage_21, SWT.PUSH);
		 * addButton.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		 * addButton.addSelectionListener(new SelectionListener() {
		 * @Override
		 * public void widgetDefaultSelected(SelectionEvent e) {
		 * }
		 * @Override
		 * public void widgetSelected(SelectionEvent e) {
		 * if (params == null) return;
		 * LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();
		 * values.put("name", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("query", "[]"); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("desc", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * MultipleValueDialog dlg = new MultipleValueDialog(form.getShell(), Messages.CorpusPage_21, values);
		 * if (dlg.open() == Window.OK) {
		 * params.addQuery(params.getCorpusElement(), values.get("name"), values.get("query"), values.get("desc")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		 * updateQuerySection();
		 * }
		 * }
		 * });
		 * Button removeButton = toolkit.createButton(sectionClient, Messages.CorpusPage_23, SWT.PUSH);
		 * removeButton.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		 * removeButton.addSelectionListener(new SelectionListener() {
		 * @Override
		 * public void widgetDefaultSelected(SelectionEvent e) {
		 * }
		 * @Override
		 * public void widgetSelected(SelectionEvent e) {
		 * if (params == null) return;
		 * ISelection sel = queriesViewer.getSelection();
		 * IStructuredSelection ssel = (IStructuredSelection) sel;
		 * Object obj = ssel.getFirstElement();
		 * if (obj instanceof Element) {
		 * Element query = (Element) obj;
		 * params.getQueriesElement(params.getCorpusElement()).removeChild(query);
		 * updateQuerySection();
		 * }
		 * }
		 * });
		 * Table table = toolkit.createTable(sectionClient, SWT.FULL_SELECTION | SWT.MULTI | SWT.V_SCROLL | SWT.VIRTUAL);
		 * queriesViewer = new TableViewer(table);
		 * queriesViewer.getTable().addKeyListener(new TableKeyListener(queriesViewer));
		 * GridData gd = new GridData(GridData.FILL, GridData.FILL, false, false,2,1);
		 * gd.heightHint = 150;
		 * queriesViewer.getTable().setLayoutData(gd);
		 * queriesViewer.setContentProvider(new NodeListContentProvider());
		 * queriesViewer.getTable().setHeaderVisible(true);
		 * queriesViewer.getTable().setLinesVisible(true);
		 * String[] cols = {"name", "value", "desc"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		 * for (String col : cols) {
		 * TableViewerColumn columnViewer = new TableViewerColumn(queriesViewer, SWT.NONE);
		 * TableColumn column = columnViewer.getColumn();
		 * column.setText(col);
		 * column.setWidth(100);
		 * columnViewer.setLabelProvider(new ElementColumnLabelProvider(col));
		 * }
		 */
	}

	private void createSAttributesSection() {
		/*
		 * sattrSection = toolkit.createSection(form.getBody(), ExpandableComposite.TITLE_BAR|ExpandableComposite.TWISTIE);
		 * sattrSection.setText(Messages.CorpusPage_24);
		 * GridLayout layout = new GridLayout(1, true);
		 * sattrSection.setLayout(layout);
		 * sattrSection.setLayoutData(getSectionGridData(2));
		 * sattrSection.setEnabled(false);
		 * sattrSection.addExpansionListener(new ExpansionAdapter() {
		 * @Override
		 * public void expansionStateChanged(ExpansionEvent e) {form.layout(true);}
		 * });
		 * Composite sectionClient = toolkit.createComposite(sattrSection);
		 * sectionClient.setLayout(new GridLayout(2, false));
		 * sattrSection.setClient(sectionClient);
		 * Button addButton = toolkit.createButton(sectionClient, Messages.CorpusPage_25, SWT.PUSH);
		 * addButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.FILL, false, false));
		 * addButton.addSelectionListener(new SelectionListener() {
		 * @Override
		 * public void widgetDefaultSelected(SelectionEvent e) {
		 * }
		 * @Override
		 * public void widgetSelected(SelectionEvent e) {
		 * if (params == null) return;
		 * LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();
		 * values.put("id", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("shortname", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("longname", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("pattern", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("renderer", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("tooltip", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("type", "String"); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("import", "true"); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("mandatory", "true"); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("inputFormat", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("outputFormat", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * MultipleValueDialog dlg = new MultipleValueDialog(form.getShell(), Messages.CorpusPage_25, values);
		 * if (dlg.open() == Window.OK) {
		 * params.addSAttribute(params.getCorpusElement(), values.get("id"), values.get("shortname"), values.get("longname"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		 * values.get("type"), values.get("renderer"), values.get("tooltip"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		 * values.get("pattern"), values.get("import"), values.get("mandatory"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		 * values.get("inputFormat"), values.get("outputFormat")); //$NON-NLS-1$ //$NON-NLS-2$
		 * updateSAttributeSection();
		 * }
		 * }
		 * });
		 * Button removeButton = toolkit.createButton(sectionClient, Messages.CorpusPage_27, SWT.PUSH);
		 * removeButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.FILL, false, false));
		 * removeButton.addSelectionListener(new SelectionListener() {
		 * @Override
		 * public void widgetDefaultSelected(SelectionEvent e) {
		 * }
		 * @Override
		 * public void widgetSelected(SelectionEvent e) {
		 * if ( params == null) return;
		 * ISelection sel = sattributesViewer.getSelection();
		 * IStructuredSelection ssel = (IStructuredSelection) sel;
		 * Object obj = ssel.getFirstElement();
		 * if (obj instanceof Element) {
		 * Element query = (Element) obj;
		 * params.getSAttributesElement(params.getCorpusElement()).removeChild(query);
		 * updateSAttributeSection();
		 * }
		 * }
		 * });
		 * Table table = toolkit.createTable(sectionClient, SWT.FULL_SELECTION | SWT.MULTI | SWT.V_SCROLL | SWT.VIRTUAL);
		 * sattributesViewer = new TableViewer(table);
		 * sattributesViewer.getTable().addKeyListener(new TableKeyListener(sattributesViewer));
		 * GridData gdata = new GridData(GridData.FILL, GridData.FILL, false, false,2,1);
		 * gdata.heightHint = 150;
		 * sattributesViewer.getTable().setLayoutData(gdata);
		 * sattributesViewer.setContentProvider(new NodeListContentProvider());
		 * sattributesViewer.getTable().setHeaderVisible(true);
		 * sattributesViewer.getTable().setLinesVisible(true);
		 * String[] cols = {"id", "shortname", "longname", "import", "mandatory", "pattern", "renderer", "tooltip", "type"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		 * //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$
		 * for (String col : cols) {
		 * TableViewerColumn columnViewer = new TableViewerColumn(sattributesViewer, SWT.NONE);
		 * TableColumn column = columnViewer.getColumn();
		 * column.setText(col);
		 * //column.setWidth(10*col.length());
		 * columnViewer.setLabelProvider(new ElementColumnLabelProvider(col));
		 * column.pack();
		 * }
		 */
	}

	/**
	 * Creates the tokenizer section.
	 */
	private int createTokenizerSection() {
		tokenizerSection = new WordsSection(editor, toolkit, form, form.getBody(), ExpandableComposite.TITLE_BAR | ExpandableComposite.TWISTIE, moduleParams);
		return tokenizerSection.getSectionSize();
	}

	private void createToolbar() {
		Composite hyperlinks = toolkit.createComposite(form.getBody());
		TableWrapData gdata = new TableWrapData(TableWrapData.LEFT, TableWrapData.FILL, 2, 1);
		gdata.colspan = 2;
		hyperlinks.setLayoutData(gdata);

		TableWrapLayout layout = new TableWrapLayout();
		layout.numColumns = 5;
		layout.makeColumnsEqualWidth = false;
		hyperlinks.setLayout(layout);

		Label titleLabel = toolkit.createLabel(hyperlinks, "", SWT.WRAP); //$NON-NLS-1$
		titleLabel.setLayoutData(new TableWrapData(TableWrapData.FILL, TableWrapData.TOP));

		if (editor.project == null) {
			titleLabel.setText(NLS.bind(TXMUIMessages.importParametersOfP0, ImportModuleCustomization.getName(editor.getImportName())));
		}
		else {
			titleLabel.setText(NLS.bind(TXMUIMessages.UpdateParametersForTheP0ImportModule, ImportModuleCustomization.getName(editor.getImportName())));
		}

		FontData[] fD = titleLabel.getFont().getFontData();
		fD[0].setHeight(16);
		titleLabel.setFont(TXMFontRegistry.getFont(titleLabel.getDisplay(), fD[0]));

		HyperlinkAdapter selectDirHListener = null;
		if (editor.project == null) {
			selectDirHListener = new HyperlinkAdapter() {

				@Override
				public void linkActivated(HyperlinkEvent e) {
					// System.out.println("Opening...");
					selectSourceDir();
				}
			};
		}

		HyperlinkAdapter saveHListener = new HyperlinkAdapter() {

			@Override
			public void linkActivated(HyperlinkEvent e) {
				if (editor.project != null) {
					saveConfig();
				}
				else {
					System.out.println(TXMUIMessages.cannotStartImportConfigurationNoSourceFolderSelected);
				}
			}
		};

		HyperlinkAdapter startImportListener = new HyperlinkAdapter() {

			@Override
			public void linkActivated(HyperlinkEvent e) {
				// System.out.println("Starting...");
				if (editor.project != null) {
					editor.project.setDirty();
					if (saveConfig()) {
						startImport();
					}
				}
				else {
					System.out.println(TXMUIMessages.cannotStartImportConfigurationNoSourceFolderSelected);
				}
			}
		};

		HyperlinkAdapter editImporScriptstListener = new HyperlinkAdapter() {

			@Override
			public void linkActivated(HyperlinkEvent e) {
				String scriptPath = editor.getImportName();
				File rootDir = new File(Toolbox.getTxmHomePath(), "scripts/groovy/user/org/txm/scripts/importer"); //$NON-NLS-1$
				File scriptDir = new File(rootDir, scriptPath);
				Explorer.open(scriptDir);
			}
		};

		if (editor.project == null) {
			ImageHyperlink openImportLink = toolkit.createImageHyperlink(hyperlinks, SWT.NULL);
			openImportLink.setLayoutData(new TableWrapData(TableWrapData.FILL, TableWrapData.MIDDLE));
			openImportLink.setImage(IImageKeys.getImage(IImageKeys.FOLDER));
			openImportLink.setToolTipText(TXMUIMessages.selectTheImportSourceDirectory);
			openImportLink.addHyperlinkListener(selectDirHListener);
		}

		ImageHyperlink saveImportLink = toolkit.createImageHyperlink(hyperlinks, SWT.NULL);
		saveImportLink.setLayoutData(new TableWrapData(TableWrapData.FILL, TableWrapData.MIDDLE));
		saveImportLink.setImage(IImageKeys.getImage(IImageKeys.SAVE));
		saveImportLink.setToolTipText(TXMUIMessages.saveParameters);
		saveImportLink.addHyperlinkListener(saveHListener);

		ImageHyperlink startImportLink = toolkit.createImageHyperlink(hyperlinks, SWT.NULL);
		startImportLink.setLayoutData(new TableWrapData(TableWrapData.FILL, TableWrapData.MIDDLE));
		startImportLink.setImage(IImageKeys.getImage(IImageKeys.START));
		startImportLink.setToolTipText(TXMUIMessages.startToImport);
		startImportLink.addHyperlinkListener(startImportListener);

		if (RCPPreferences.getInstance().getBoolean(RCPPreferences.EXPERT_USER)) {
			ImageHyperlink scriptImportLink = toolkit.createImageHyperlink(hyperlinks, SWT.NULL);
			scriptImportLink.setLayoutData(new TableWrapData(TableWrapData.FILL, TableWrapData.MIDDLE));
			scriptImportLink.setImage(IImageKeys.getImage(IImageKeys.SCRIPT));
			scriptImportLink.setToolTipText(TXMUIMessages.showTheModuleScripts);
			scriptImportLink.addHyperlinkListener(editImporScriptstListener);
		}
		int n = 1;
		if (editor.project == null) {

			Composite line1 = toolkit.createComposite(form.getBody());
			gdata = new TableWrapData(TableWrapData.LEFT, TableWrapData.FILL, 2, 1);
			gdata.colspan = 2;
			line1.setLayoutData(gdata);
			RowLayout hLayout = new RowLayout();
			hLayout.center = true;
			line1.setLayout(hLayout);
			toolkit.createLabel(line1, "" + (n++) + ".", SWT.WRAP); //$NON-NLS-1$ //$NON-NLS-2$

			Hyperlink hyperlink1 = toolkit.createHyperlink(line1, TXMUIMessages.selectTheSourceDirectory, SWT.WRAP);
			hyperlink1.addHyperlinkListener(selectDirHListener);

			ImageHyperlink openImportLink2 = toolkit.createImageHyperlink(line1, SWT.WRAP);
			openImportLink2.setImage(IImageKeys.getImage(IImageKeys.FOLDER));
			openImportLink2.addHyperlinkListener(selectDirHListener);
		}

		Composite line2 = toolkit.createComposite(form.getBody());
		gdata = new TableWrapData(TableWrapData.LEFT, TableWrapData.FILL, 2, 1);
		gdata.colspan = 2;
		line2.setLayoutData(gdata);
		RowLayout hLayout = new RowLayout();
		hLayout.center = true;
		line2.setLayout(hLayout);
		toolkit.createLabel(line2, "" + (n++) + ".", SWT.WRAP); //$NON-NLS-1$ //$NON-NLS-2$
		toolkit.createLabel(line2, TXMUIMessages.setImportParametersInTheSectionsBelow, SWT.WRAP);

		Composite line4 = toolkit.createComposite(form.getBody());
		gdata = new TableWrapData(TableWrapData.LEFT, TableWrapData.FILL, 2, 1);
		gdata.colspan = 2;
		line4.setLayoutData(gdata);
		hLayout = new RowLayout();
		hLayout.center = true;
		line4.setLayout(hLayout);
		toolkit.createLabel(line4, "" + (n++) + ".", SWT.WRAP); //$NON-NLS-1$ //$NON-NLS-2$
		Hyperlink hyperlink3 = toolkit.createHyperlink(line4, TXMUIMessages.startCorpusImport, SWT.WRAP);
		if (editor.getUpdateMode()) {
			hyperlink3.setText(TXMUIMessages.StartCorpusUpdate);
		}
		hyperlink3.addHyperlinkListener(startImportListener);

		ImageHyperlink startImportLink2 = toolkit.createImageHyperlink(line4, SWT.NULL);
		startImportLink2.setImage(IImageKeys.getImage(IImageKeys.START));
		startImportLink2.addHyperlinkListener(startImportListener);
		//		
		//		ImageHyperlink scriptImportLink2 = toolkit.createImageHyperlink(line4, SWT.NULL);
		//		scriptImportLink2.setImage(IImageKeys.getImage(IImageKeys.SCRIPT));
		//		scriptImportLink2.addHyperlinkListener(editImporScriptstListener);
	}

	private int createUIsSection() {
		uiSection = new CommandsSection(editor, toolkit, form, form.getBody(), ExpandableComposite.TITLE_BAR | ExpandableComposite.TWISTIE);
		return uiSection.getSectionSize();
	}

	/**
	 * Called by "selectSourceDir".
	 * Theparameter file is read to fill the section fields.
	 * 
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public void loadParams() throws ParserConfigurationException, SAXException, IOException {

		Log.fine(NLS.bind(TXMUIMessages.moduleUIParametersColonP0, moduleParams));
		this.setPartName(editor.getImportName());

		if (infosSection != null && !infosSection.isDisposed()) {
			infosSection.setEnabled(true);
			infosSection.update(editor.project);
		}
		if (langSection != null && !langSection.isDisposed()) {
			langSection.setEnabled(true);
			langSection.update(editor.project);
		}
		if (xsltSection != null && !xsltSection.isDisposed()) {
			xsltSection.setEnabled(true);
			xsltSection.update(editor.project);
		}
		if (tokenizerSection != null && !tokenizerSection.isDisposed()) {
			tokenizerSection.setEnabled(true);
			tokenizerSection.update(editor.project);
		}
		if (uiSection != null && !uiSection.isDisposed()) {
			uiSection.setEnabled(true);
			uiSection.update(editor.project);
		}
		if (encodingSection != null && !encodingSection.isDisposed()) {
			encodingSection.setEnabled(true);
			encodingSection.update(editor.project);
		}
		if (editionSection != null && !editionSection.isDisposed()) {
			editionSection.setEnabled(true);
			editionSection.update(editor.project);
		}
		if (fontSection != null && !fontSection.isDisposed()) {
			fontSection.setEnabled(true);
			fontSection.update(editor.project);
		}
		if (textualPlansSection != null && !textualPlansSection.isDisposed()) {
			textualPlansSection.setEnabled(true);
			textualPlansSection.update(editor.project);
		}

		if (structuresSection != null && !structuresSection.isDisposed()) {
			structuresSection.setEnabled(true);
			structuresSection.update(editor.project);
		}

		if (optionsSection != null && !optionsSection.isDisposed()) {
			optionsSection.setEnabled(true);
			optionsSection.update(editor.project);
		}

		if (additionalSection != null && !additionalSection.isDisposed()) {
			additionalSection.setEnabled(true);
			additionalSection.update(editor.project);
		}

		/*
		 * if (sattrSection != null && !sattrSection.isDisposed()) {
		 * sattrSection.setEnabled(true);
		 * }
		 * if (querySection != null && !querySection.isDisposed()) {
		 * querySection.setEnabled(true);
		 * updateQuerySection();
		 * }
		 * if (preBuildSection != null && !preBuildSection.isDisposed()) {
		 * preBuildSection.setEnabled(true);
		 * updatePreBuildSection();
		 * }
		 * if (pattrSection != null && !pattrSection.isDisposed()) {
		 * pattrSection.setEnabled(true);
		 * updatePAttributeSection();
		 * }
		 */
	}

	/**
	 * Called by "startImport". Save the field values in the Parameter file
	 * 
	 * @return true if import parameters are saved
	 */
	public boolean saveConfig() {
		Log.info(TXMUIMessages.savingImportParameters);

		if (editor.project == null || editor.project.getRCPProject() == null || !editor.project.getRCPProject().exists()) {
			Log.warning(TXMUIMessages.TheCorpushasBeenDeletedSincePleaseReOpenTheImportForm);
			this.getEditor().close(false);
			return false;
		}

		// params.rootDir = project.getSrcdir().getAbsolutePath();
		// project.scriptFile = editor.getGroovyScript().getName();

		// params.getCorpusElement().setAttribute("name", params.name); //$NON-NLS-1$
		// params.getCorpusElement().setAttribute("desc", params.description); //$NON-NLS-1$
		// params.getCorpusElement().setAttribute("cqpid", params.name.toUpperCase()+new Date().getTime()); //$NON-NLS-1$

		// TEST ALL SECTIONS
		boolean doSave = true;
		if (infosSection != null && !infosSection.isDisposed()) {
			doSave = doSave & infosSection.check();
		}
		if (langSection != null && !langSection.isDisposed()) {
			doSave = doSave & langSection.check();
		}
		if (xsltSection != null && !xsltSection.isDisposed()) {
			doSave = doSave & xsltSection.check();
		}
		if (tokenizerSection != null && !tokenizerSection.isDisposed()) {
			doSave = doSave & tokenizerSection.check();
		}
		if (uiSection != null && !uiSection.isDisposed()) {
			doSave = doSave & uiSection.check();
		}
		if (encodingSection != null && !encodingSection.isDisposed()) {
			doSave = doSave & encodingSection.check();
		}
		if (editionSection != null && !editionSection.isDisposed()) {
			doSave = doSave & editionSection.check();
		}
		if (fontSection != null && !fontSection.isDisposed()) {
			doSave = doSave & fontSection.check();
		}
		if (textualPlansSection != null && !textualPlansSection.isDisposed()) {
			doSave = doSave & textualPlansSection.check();
		}
		if (structuresSection != null && !structuresSection.isDisposed()) {
			doSave = doSave & structuresSection.check();
		}
		if (optionsSection != null && !optionsSection.isDisposed()) {
			doSave = doSave & optionsSection.check();
		}
		if (additionalSection != null && !additionalSection.isDisposed()) {
			doSave = doSave & additionalSection.check();
		}
		if (!doSave) {
			System.out.println(TXMUIMessages.SomeFieldsAreNotCorrectlyFilled);
			return false;
		}

		// SAVE ALL SECTIONS
		boolean successfulSave = true;
		if (infosSection != null && !infosSection.isDisposed()) {
			successfulSave = successfulSave & infosSection.save(editor.project);
		}
		if (langSection != null && !langSection.isDisposed()) {
			successfulSave = successfulSave & langSection.save(editor.project);
		}
		if (xsltSection != null && !xsltSection.isDisposed()) {
			successfulSave = successfulSave & xsltSection.save(editor.project);
		}
		if (tokenizerSection != null && !tokenizerSection.isDisposed()) {
			successfulSave = successfulSave & tokenizerSection.save(editor.project);
		}
		if (uiSection != null && !uiSection.isDisposed()) {
			successfulSave = successfulSave & uiSection.save(editor.project);
		}
		if (encodingSection != null && !encodingSection.isDisposed()) {
			successfulSave = successfulSave & encodingSection.save(editor.project);
		}
		if (editionSection != null && !editionSection.isDisposed()) {
			successfulSave = successfulSave & editionSection.save(editor.project);
		}
		if (fontSection != null && !fontSection.isDisposed()) {
			successfulSave = successfulSave & fontSection.save(editor.project);
		}
		if (textualPlansSection != null && !textualPlansSection.isDisposed()) {
			successfulSave = successfulSave & textualPlansSection.save(editor.project);
		}
		if (structuresSection != null && !structuresSection.isDisposed()) {
			successfulSave = successfulSave & structuresSection.save(editor.project);
		}
		if (optionsSection != null && !optionsSection.isDisposed()) {
			successfulSave = successfulSave & optionsSection.save(editor.project);
		}
		if (additionalSection != null && !additionalSection.isDisposed()) {
			successfulSave = successfulSave & additionalSection.save(editor.project);
		}

		if (successfulSave) {
			try {
				editor.project.save();
				// DomUtils.save(params.root.getOwnerDocument(), paramFile);
			}
			catch (Exception e1) {
				org.txm.utils.logger.Log.printStackTrace(e1);
			}

			return true;
		}
		System.out.println(TXMUIMessages.ErrorWhileSavingParameters);
		return false;

	}

	/**
	 * Called by the "folder" button
	 */
	public void selectSourceDir() {

		ImportWizard wiz = new ImportWizard();
		WizardDialog wdialog = new WizardDialog(this.getEditor().getSite().getShell(), wiz);
		wdialog.open();

		editor.project = wiz.getProject();

		initializeProjectSettings();
	}

	private void initializeProjectSettings() {

		if (editor.project == null) {
			// System.out.println("Error: no project created");
			return;
		}

		editor.project.setImportModuleName(editor.getImportName());

		try {
			loadParams();

			this.editor.getMetaPage().reload();
			this.editor.firePropertyChange(IEditorPart.PROP_TITLE);
		}
		catch (Exception e1) {
			org.txm.utils.logger.Log.printStackTrace(e1);
		}
	}

	public void startImport() {
		// TODO: test all sections here
		editor.project.setDirty();
		if (editor.getUpdateMode()) {
			UpdateCorpus.update(editor.project);
		}
		else {
			editor.project.setDoUpdate(false, false);
			editor.project.setNeedToBuild();

			if (editor.project.getChildren().size() > 0) {
				boolean b = MessageDialog.openConfirm(Display.getCurrent().getActiveShell(), TXMUIMessages.warning,
						NLS.bind(TXMUIMessages.theP0CorpusDirectoryAlreadyExistsDoYouWantToReplaceIt, editor.project.getName()));
				if (!b) {
					Log.info(TXMUIMessages.abort);
					return;
				}
			}

			SWTEditorsUtils.closeEditors(editor.project, true);

			ExecuteImportScript.executeScript(editor.project);
		}
	}

	// private void updateEditionsSection() {
	//
	// }
	//
	// private void updateEncodingSection() {
	//
	// }
	//
	// private void updateFontSection() {
	//
	// }
	//
	// private void updateFrontXsltSection() {
	//
	// }
	//
	// private void updateLangSection() {
	//
	// }
	//
	// protected void updatePAttributeSection() {
	// if (pattrSection.isDisposed()) return;
	// if (project == null) return;
	// pattributesViewer.getTable().getParent().layout();
	// Element queriesElem = params.getPAttributesElement(params.getCorpusElement());
	// pattributesViewer.setInput(queriesElem.getElementsByTagName("pattribute")); //$NON-NLS-1$
	// }
	//
	// private void updatePreBuildSection() {
	// if (preBuildSection.isDisposed()) return;
	// if (project == null) return;
	// preBuildViewer.getTable().getParent().layout();
	// Element queriesElem = params.getPreBuildElement(params.getCorpusElement());
	// preBuildViewer.setInput(queriesElem.getElementsByTagName("subcorpus")); //$NON-NLS-1$
	//
	// preBuildViewer2.getTree().getParent().layout();
	// queriesElem = params.getPreBuildElement(params.getCorpusElement());
	// NodeList partitions = queriesElem.getElementsByTagName("partition"); //$NON-NLS-1$
	// preBuildViewer2.setInput(partitions);
	// }
	//
	// private void updateQuerySection() {
	// if (querySection.isDisposed()) return;
	// if (project == null) return;
	// queriesViewer.getTable().getParent().layout();
	// Element queriesElem = params.getQueriesElement(params.getCorpusElement());
	// queriesViewer.setInput(queriesElem.getElementsByTagName("query")); //$NON-NLS-1$
	// }
	//
	// private void updateSAttributeSection() {
	// if (sattrSection.isDisposed()) return;
	// if (project == null) return;
	// sattributesViewer.getTable().getParent().layout();
	// Element queriesElem = params.getSAttributesElement(params.getCorpusElement());
	// sattributesViewer.setInput(queriesElem.getElementsByTagName("sattribute")); //$NON-NLS-1$
	// }
	//
	// private void updateTokenizerSection() {
	//
	// }
	//
	// private void updateUisSection() {
	//
	// }
	//
	public String getRootDir() {
		if (editor.project == null) return null;
		if (editor.project.getSrcdir() == null) return null;
		return editor.project.getSrcdir().getAbsolutePath();
	}

	//	public Project getProject() {
	//		return project;
	//	}
}
