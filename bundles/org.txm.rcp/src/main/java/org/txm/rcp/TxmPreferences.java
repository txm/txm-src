// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Properties;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TBXPreferences;

// TODO: Auto-generated Javadoc
/**
 * Manage TXM configuration prefs: set, remove and get.
 *
 * @author mdecorde
 */
@Deprecated
public class TxmPreferences {


	protected static final String SCOPE = "instance"; //$NON-NLS-1$

	/**
	 * print TxmPreferences in the console.
	 */
	protected static void dump() {
		try {
			IPreferencesService service = Platform.getPreferencesService();
			System.out.println("root:"); //$NON-NLS-1$
			for (String children : service.getRootNode().childrenNames()) {
				System.out.println(" scope: " + children); //$NON-NLS-1$
				String[] subchildren = service.getRootNode().node(children).childrenNames();
				Arrays.sort(subchildren);
				for (String children2 : subchildren) {
					System.out.println("  node: " + children2); //$NON-NLS-1$
					if (children2.equals(Application.PLUGIN_ID)) {
						String[] keys = service.getRootNode().node(children).node(children2).keys();
						Arrays.sort(keys);
						for (String key2 : keys) {
							System.out.println(
									"   " + key2 + " = " + service.getRootNode().node(children).node(children2).get(key2, null)); //$NON-NLS-1$ //$NON-NLS-2$
						}
					}
				}
			}
		}
		catch (Exception e) {
			System.err.println(e);
		}
	}

	protected static Preferences getPreferences() {
		IPreferencesService service = Platform.getPreferencesService();
		return service.getRootNode().node(SCOPE).node(Application.PLUGIN_ID);
	}

	/**
	 * read a UTF-8 tabulated file key1=value1 key2=value2.
	 *
	 * @param file the file
	 */
	protected static void importFromFile(File file) {
		importFromFile(file, "UTF-8", false); //$NON-NLS-1$
	}

	/**
	 * read a tabulated file key1=value1 key2=value2.
	 *
	 * @param file the file
	 * @param encoding the encoding
	 */
	protected static boolean importFromFile(File file, String encoding, boolean javaproperties) {
		boolean ret = true;
		BufferedReader input = null;
		try {
			//System.out.println("importFromFile: "+file+" encoding: "+encoding);
			input = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));

			if (javaproperties) {
				Properties prop = new Properties();
				prop.load(input);
				for (Object key : prop.keySet()) {
					//System.out.println("prop: "+key.toString()+"="+prop.get(key).toString());
					TBXPreferences.getInstance().put(key.toString(), prop.get(key).toString());
				}
			}
			else {
				String line = input.readLine();
				while (line != null) {
					String[] split = line.split("=", 2); //$NON-NLS-1$
					if (split.length == 2) {
						//System.out.println("Set preference: "+line);
						TBXPreferences.getInstance().put(split[0], split[1]);
					}
					line = input.readLine();
				}
			}
		}
		catch (Exception e) {
			System.err.println(e);
			ret = false;
		}
		finally {
			if (input != null)
				try {
					input.close();
				}
				catch (IOException e) {
					org.txm.utils.logger.Log.printStackTrace(e);
				}
		}
		return ret;
	}

	//	/**
	//	 * set a property which value is a String.
	//	 *
	//	 * @param key the key
	//	 * @param value the value
	//	 */
	//	protected static void set(String key, String value) {
	//		IPreferencesService service = Platform.getPreferencesService();
	//		service.getRootNode().node(SCOPE).node(Application.PLUGIN_ID).put(key, value); //$NON-NLS-1$
	//		try {
	//			service.getRootNode().flush();
	//		} catch (BackingStoreException e) {
	//			System.err.println(e);
	//		}
	//	}
	//
	//	/**
	//	 * set a property which value is a boolean.
	//	 *
	//	 * @param key the key
	//	 * @param value the value
	//	 */
	//	protected static void set(String key, boolean value) {
	//		IPreferencesService service = Platform.getPreferencesService();
	//		service.getRootNode()
	//		.node(SCOPE).node(Application.PLUGIN_ID).putBoolean(key, value); //$NON-NLS-1$
	//		try {
	//			service.getRootNode().flush();
	//		} catch (BackingStoreException e) {
	//			System.err.println(e);
	//		}
	//	}
	//
	//	/**
	//	 * set a property which value is a double.
	//	 *
	//	 * @param key the key
	//	 * @param value the value
	//	 */
	//	protected static void set(String key, double value) {
	//		IPreferencesService service = Platform.getPreferencesService();
	//		service.getRootNode()
	//		.node(SCOPE).node(Application.PLUGIN_ID).putDouble(key, value); //$NON-NLS-1$
	//		try {
	//			service.getRootNode().flush();
	//		} catch (BackingStoreException e) {
	//			System.err.println(e);
	//		}
	//	}
	//
	//	/**
	//	 * set a property which value is a float.
	//	 *
	//	 * @param key the key
	//	 * @param value the value
	//	 */
	//	protected static void set(String key, float value) {
	//		IPreferencesService service = Platform.getPreferencesService();
	//		service.getRootNode()
	//		.node(SCOPE).node(Application.PLUGIN_ID).putFloat(key, value); //$NON-NLS-1$
	//		try {
	//			service.getRootNode().flush();
	//		} catch (BackingStoreException e) {
	//			System.err.println(e);
	//		}
	//	}
	//
	//	/**
	//	 * set a property which value is a int.
	//	 *
	//	 * @param key the key
	//	 * @param value the value
	//	 */
	//	protected static void set(String key, int value) {
	//		IPreferencesService service = Platform.getPreferencesService();
	//		service.getRootNode()
	//		.node(SCOPE).node(Application.PLUGIN_ID).putInt(key, value); //$NON-NLS-1$
	//		try {
	//			service.getRootNode().flush();
	//		} catch (BackingStoreException e) {
	//			System.err.println(e);
	//		}
	//	}
	//
	//	/**
	//	 * set a property which value is a long.
	//	 *
	//	 * @param key the key
	//	 * @param value the value
	//	 */
	//	protected static void set(String key, long value) {
	//		IPreferencesService service = Platform.getPreferencesService();
	//		service.getRootNode()
	//		.node(SCOPE).node(Application.PLUGIN_ID).putLong(key, value); //$NON-NLS-1$
	//		try {
	//			service.getRootNode().flush();
	//		} catch (BackingStoreException e) {
	//			System.err.println(e);
	//		}
	//	}
	//
	//	/**
	//	 * set a property which value is a byte array.
	//	 *
	//	 * @param key the key
	//	 * @param value the value
	//	 */
	//	protected static void set(String key, byte[] value) {
	//		IPreferencesService service = Platform.getPreferencesService();
	//		service.getRootNode()
	//		.node(SCOPE).node(Application.PLUGIN_ID).putByteArray(key, value); //$NON-NLS-1$
	//		try {
	//			service.getRootNode().flush();
	//		} catch (BackingStoreException e) {
	//			System.err.println(e);
	//		}
	//	}
	//
	//	/**
	//	 * get a property as a String.
	//	 *
	//	 * @param key the key
	//	 * @return the string
	//	 */
	//	protected static String get(String key) {
	//		IPreferencesService service = Platform.getPreferencesService();
	//		return service.getRootNode()
	//				.node(SCOPE).node(Application.PLUGIN_ID).get(key, null); //$NON-NLS-1$
	//	}
	//
	//	/**
	//	 * get a property as a String.
	//	 *
	//	 * @param key the key
	//	 * @return the string
	//	 */
	//	protected static String get(String key, String def) {
	//		String rez = get(key);
	//		if (rez == null) rez = def;
	//		return rez;
	//	}
	//
	//	/**
	//	 * get a property as a Boolean.
	//	 *
	//	 * @param key the key
	//	 * @return the boolean
	//	 */
	//	protected static Boolean getBoolean(String key) {
	//		IPreferencesService service = Platform.getPreferencesService();
	//		return service.getRootNode()
	//				.node(SCOPE).node(Application.PLUGIN_ID).getBoolean(key, false); //$NON-NLS-1$
	//	}
	//
	//	/**
	//	 * get a property as a Boolean.
	//	 *
	//	 * @param key the key
	//	 * @param defaultvalue the defaultvalue
	//	 * @return the boolean
	//	 */
	//	protected static Boolean getBoolean(String key, boolean defaultvalue) {
	//		IPreferencesService service = Platform.getPreferencesService();
	//		return service.getRootNode()
	//				.node(SCOPE).node(Application.PLUGIN_ID).getBoolean(key, defaultvalue); //$NON-NLS-1$
	//	}
	//
	//	/**
	//	 * get a property as a Double.
	//	 *
	//	 * @param key the key
	//	 * @return the double
	//	 */
	//	protected static Double getDouble(String key) {
	//		IPreferencesService service = Platform.getPreferencesService();
	//		return service.getRootNode()
	//				.node(SCOPE).node(Application.PLUGIN_ID).getDouble(key, 0.0d); //$NON-NLS-1$
	//	}
	//
	//	/**
	//	 * get a property as a Integer.
	//	 *
	//	 * @param key the key
	//	 * @return the int
	//	 */
	//	protected static Integer getInt(String key) {
	//		return getInt(key, 0);
	//	}
	//
	//	/**
	//	 * Gets the int.
	//	 *
	//	 * @param key the key
	//	 * @param i the i
	//	 * @return the int
	//	 */
	//	protected static int getInt(String key, int i) {
	//		IPreferencesService service = Platform.getPreferencesService();
	//		return service.getRootNode()
	//				.node(SCOPE).node(Application.PLUGIN_ID).getInt(key, i); //$NON-NLS-1$
	//	}
	//
	//
	//	/**
	//	 * Gets the float.
	//	 *
	//	 * @param key the key
	//	 * @param def the def
	//	 * @return the float
	//	 */
	//	protected static float getFloat(String key, float def) {
	//		IPreferencesService service = Platform.getPreferencesService();
	//		return service.getRootNode()
	//				.node(SCOPE).node(Application.PLUGIN_ID).getFloat(key, def); //$NON-NLS-1$
	//	}
	//
	//	/**
	//	 * remove a property.
	//	 *
	//	 * @param key the key
	//	 */
	//	protected static void remove(String key) {
	//		IPreferencesService service = Platform.getPreferencesService();
	//		service.getRootNode()
	//		.node(SCOPE).node(Application.PLUGIN_ID).remove(key); //$NON-NLS-1$
	//		try {
	//			service.getRootNode().flush();
	//		} catch (BackingStoreException e) {
	//			System.err.println(e);
	//		}
	//	}
	//
	////	/**
	////	 * return all stored properties.
	////	 *
	////	 * @return the properties
	////	 */
	////	protected static Properties getProperties() {
	////		return ApplicationWorkbenchAdvisor.getProperties();
	////	}
	//
	//	/**
	//	 * Gets the string.
	//	 *
	//	 * @param key the key
	//	 * @param defaultvalue the defaultvalue
	//	 * @return the string
	//	 */
	//	protected static String getString(String key, String defaultvalue) {
	//		String ret = get(key);
	//		if (ret == null)
	//			return defaultvalue;
	//		return ret;
	//	}

	protected static String[] getAllKeys() {
		IPreferencesService service = Platform.getPreferencesService();
		Preferences prefs = service.getRootNode().node(SCOPE).node(Application.PLUGIN_ID); //$NON-NLS-1$
		try {
			return prefs.keys();
		}
		catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
			return new String[0];
		}
	}
}
