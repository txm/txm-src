package org.txm.rcp.editors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.editor.FormEditor;
import org.txm.core.results.TXMResult;
import org.txm.rcp.utils.JobHandler;

/**
 * Editor to see a TXMResult from different point of views
 * 
 * @author mdecorde
 *
 * @param <T> the TXMResult class to be seen
 */
public abstract class TXMFormEditor<T extends TXMResult> extends FormEditor implements ITXMResultEditor<T> {

	protected T result;

	@Override
	public T getResult() {
		return result;
	}

	@Override
	public JobHandler compute(boolean update) {
		for (Object page : pages) {
			if (page instanceof ITXMResultEditor) {
				return ((ITXMResultEditor<?>) page).compute(update);
			}
		}
		return null;
	}

	@Override
	public void refresh(boolean update) throws Exception {
		for (Object page : pages) {
			if (page instanceof ITXMResultEditor) {
				((ITXMResultEditor<?>) page).refresh(update);
			}
		}
	}

	@Override
	public void close() {
		for (Object page : pages) {
			if (page instanceof ITXMResultEditor) {
				((ITXMResultEditor<?>) page).close();
			}
		}
	}

	@Override
	public void close(boolean deleteResult) {
		for (Object page : pages) {
			if (page instanceof ITXMResultEditor) {
				((ITXMResultEditor<?>) page).close(deleteResult);
			}
		}
	}

	@Override
	public void setLocked(boolean locked) {
		for (Object page : pages) {
			if (page instanceof ITXMResultEditor) {
				((ITXMResultEditor<?>) page).setLocked(locked);
			}
		}
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		for (Object page : pages) {
			if (page instanceof ITXMResultEditor) {
				((ITXMResultEditor<?>) page).doSave(monitor);
			}
		}
	}

	@Override
	public void doSaveAs() {
		for (Object page : pages) {
			if (page instanceof ITXMResultEditor) {
				((ITXMResultEditor<?>) page).doSaveAs();
			}
		}
	}

	@Override
	public boolean isSaveAsAllowed() {
		boolean saveAllowed = true;
		for (Object page : pages) {
			if (page instanceof ITXMResultEditor) {
				saveAllowed = saveAllowed && ((ITXMResultEditor<?>) page).isSaveAsAllowed();
			}
		}
		return saveAllowed;
	}

	/**
	 * 
	 * @return the root composite of this multi paged editor
	 */
	public Composite getContainer() {
		return super.getContainer();
	}
}

