package org.txm.rcp.jface;

import java.lang.reflect.Field;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

/**
 * Hacking org.eclipse.jface.preference.ComboFieldEditor to read the combo widget selected value
 * 
 * @author mdecorde
 *
 */
public class ComboFieldEditor extends org.eclipse.jface.preference.ComboFieldEditor {

	public ComboFieldEditor(String name, String labelText, String[][] entryNamesAndValues, Composite parent) {
		super(name, labelText, entryNamesAndValues, parent);
	}

	public String getStringValue() {

		Field field;
		try {
			Class<?> clazz = this.getClass().getSuperclass();
			field = clazz.getDeclaredField("fCombo"); //$NON-NLS-1$
			field.setAccessible(true);
			Combo fCombo = (Combo) field.get(this);
			return fCombo.getText();
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
