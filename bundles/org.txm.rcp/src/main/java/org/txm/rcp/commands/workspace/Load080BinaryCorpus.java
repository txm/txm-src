// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands.workspace;

import java.io.File;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.osgi.util.NLS;
import org.txm.Toolbox;
import org.txm.core.results.TXMResult;
import org.txm.objects.Project;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.DeleteDir;
import org.txm.utils.logger.Log;
import org.txm.utils.zip.Zip;

/**
 * Load a project from a .txm|zip file
 *
 * @author mdecorde
 */
public class Load080BinaryCorpus {


	public static boolean canLoad(File zipFile) {
		String basedirname = Zip.getRoot(zipFile);
		return Zip.hasEntries(zipFile, basedirname + "/.settings/", basedirname + "/.project", basedirname + "/data/", basedirname + "/txm/", basedirname + "/HTML/", basedirname + "/registry/"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
	}

	/**
	 * load the content of the file if its a directory containing at least the sub folders :
	 * txm, html, data and registry.
	 * 
	 * @param replace
	 *
	 * @param zipFile the zipFile to load
	 * @return the base
	 * @throws Exception the exception
	 */
	public static Project loadBinaryCorpusArchive(File zipFile, IProgressMonitor monitor, boolean replace) throws Exception {
		// ZIPPED FILE
		if (monitor != null) monitor.beginTask(TXMUIMessages.loadingBinaryCorpus, 100);
		if (monitor != null) monitor.subTask(TXMUIMessages.extractingBinaryCorpus);

		if (!canLoad(zipFile)) {
			Log.severe(NLS.bind(TXMUIMessages.theP0BinaryCorpusIsNotATXM080CorpusNoSettingsNorProjectFile, zipFile.getName()));
			return null;
		}

		Log.info(NLS.bind(TXMUIMessages.theP0BinaryCorpusVersionIsP1, zipFile.getName(), "0.8.0")); //$NON-NLS-1$

		String basedirname = Zip.getRoot(zipFile);

		Project p = Toolbox.workspace.getProject(basedirname.toUpperCase());
		if (p != null) {
			if (replace) {
				p.delete();
			}
			else {
				System.out.println(NLS.bind(TXMUIMessages.abortingLoadingOfP0ACorpusWithTheSameNameAlreadyExistsInP1, zipFile, basedirname.toUpperCase()));
				return null;
			}
		}

		File corpusDirectory = new File(Toolbox.workspace.getLocation(), basedirname);
		if (corpusDirectory.exists()) {
			if (replace) {
				DeleteDir.deleteDirectory(corpusDirectory);
			}
			else {
				System.out.println(NLS.bind(TXMUIMessages.abortingLoadingOfP0ACorpusWithTheSameNameAlreadyExistsInP1, zipFile, corpusDirectory));
				return null;
			}
		}

		try {
			// System.out.println(NLS.bind(Messages.AddBase_29, zipFile, corporaDir));
			Zip.decompress(zipFile, Toolbox.workspace.getLocation(), false, monitor);
		}
		catch (Exception e) {
			Log.severe(NLS.bind(TXMUIMessages.couldNotUnzipBinaryCorpusColonP0, e));
			org.txm.utils.logger.Log.printStackTrace(e);
			return null;
		}
		if (monitor != null) monitor.worked(50);

		if (monitor != null) monitor.subTask(TXMUIMessages.loadingTheCorpusInThePlatform);

		// test with CQP MainCorpus name
		File dataDirectory = new File(corpusDirectory, "data"); //$NON-NLS-1$
		File[] mainCorpusDirs = dataDirectory.listFiles();
		if (mainCorpusDirs != null && mainCorpusDirs.length > 0) {
			String mainCorpusName = mainCorpusDirs[0].getName();
			p = Toolbox.workspace.getProject(mainCorpusName);
			if (p != null) {
				Log.severe(NLS.bind(TXMUIMessages.abortingLoadingOfP1ACorpusWithTheSameCQPIDP1AlreadyExists, corpusDirectory, mainCorpusName));
				DeleteDir.deleteDirectory(corpusDirectory); // clean files
				return null;
			}
		}

		Project newProject = loadBinaryCorpusAsDirectory(corpusDirectory);
		if (newProject == null) {
			DeleteDir.deleteDirectory(corpusDirectory); // clean files
			Log.severe(TXMUIMessages.failedToLoadBinaryCorpusNullError);
			return null;
		}

		return newProject;
	}

	/**
	 * load the content of the file if its a directory containing at least the sub folders :
	 * txm, html, data and registry.
	 *
	 * @param binCorpusDirectory the basedir must not exists
	 * @return the base
	 * @throws Exception the exception
	 */
	public static Project loadBinaryCorpusAsDirectory(File binCorpusDirectory) throws Exception {
		if (!(binCorpusDirectory.exists() && binCorpusDirectory.isDirectory())) {
			Log.severe(NLS.bind(TXMUIMessages.failedToLoadCorpusP0, binCorpusDirectory.getAbsolutePath()));
			return null;
		}

		File txmcorpora = new File(Toolbox.getTxmHomePath(), "corpora"); //$NON-NLS-1$
		txmcorpora.mkdir();
		Project project = Toolbox.workspace.scanDirectory(binCorpusDirectory);
		if (project == null) {
			Log.severe(NLS.bind(TXMUIMessages.failedToLoadCorpusFromDirectoryColonP0WeCannotFindTheNecessaryComponents, binCorpusDirectory));
			return null;
		}

		boolean ok = true;
		for (TXMResult r : project.getChildren(MainCorpus.class)) {
			MainCorpus c = (MainCorpus) r;
			ok = ok && c.compute(false);
		}
		if (ok) {
			project.saveParameters();
			return project;
		}
		else {
			project.delete();
			return null;
		}
	}
}
