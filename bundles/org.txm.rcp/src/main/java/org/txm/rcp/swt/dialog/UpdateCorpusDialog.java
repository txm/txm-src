package org.txm.rcp.swt.dialog;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.provider.SimpleLabelProvider;
import org.txm.searchengine.cqp.corpus.MainCorpus;

public class UpdateCorpusDialog extends Dialog {

	protected String title = TXMUIMessages.UpdateCorpus;

	protected String topMessage = TXMUIMessages.UpdateTheCorpusFromItsXMLTXMFiles;

	protected String bottomMessage = TXMUIMessages.AllEditorsUsingThisCorpusWillBeClosedandtheCorpusWillBeUpdatedContinue;

	protected Button openCorpusParameters;

	protected Button updateEditionsCheck;

	protected Button forceUpdateCheck;

	public boolean doOpenCorpusParameters = false;

	public boolean doUpdateEditions = false;

	public boolean doForceUpdate = false;

	public MainCorpus corpusToUpdate = null;

	private Button modifiesTextsCheck;

	private ListViewer corpusList;

	private java.util.List<MainCorpus> corpora;



	public UpdateCorpusDialog(Shell parentShell) {

		super(parentShell);
	}


	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(title);
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		Composite comp = (Composite) super.createDialogArea(parent);

		GridLayout layout = (GridLayout) comp.getLayout();
		layout.numColumns = 1;
		layout.verticalSpacing = 20;
		layout.marginWidth = 15;

		createTopMessageArea(comp);

		createButtonsArea(comp);

		createBottomMessageArea(comp);

		return comp;
	}

	protected void createButtonsArea(Composite comp) {

		GridData data = null;

		if (corpora != null) {

			GLComposite line = new GLComposite(comp, SWT.NONE, ""); //$NON-NLS-1$
			data = new GridData(GridData.FILL, GridData.FILL, true, false);
			line.setLayoutData(data);
			line.getLayout().numColumns = 1;

			Label l = new Label(line, SWT.NONE);
			data = new GridData(GridData.FILL, GridData.FILL, false, false);
			l.setLayoutData(data);
			l.setText(TXMUIMessages.corpusToProcess);
			data = new GridData(GridData.FILL, GridData.FILL, true, false);

			corpusList = new ListViewer(line, SWT.SINGLE | SWT.READ_ONLY);
			data = new GridData(GridData.FILL, GridData.FILL, true, false);
			corpusList.getList().setLayoutData(data);
			corpusList.setLabelProvider(new SimpleLabelProvider());
			corpusList.setContentProvider(new ArrayContentProvider());

			corpusList.setInput(corpora);

			if (corpusToUpdate != null && corpora.contains(corpusToUpdate)) {
				corpusList.setSelection(new StructuredSelection(corpusToUpdate));
			}
			else if (corpora.size() > 0) {
				corpusList.getList().selectAll();
			}
		}

		Group textsGroup = new Group(comp, SWT.NONE);
		data = new GridData(GridData.FILL, GridData.FILL, true, false);
		textsGroup.setLayoutData(data);
		textsGroup.setLayout(new RowLayout(SWT.HORIZONTAL));
		textsGroup.setText(TXMUIMessages.textsToProcess);

		modifiesTextsCheck = new Button(textsGroup, SWT.RADIO);
		modifiesTextsCheck.setText(TXMUIMessages.withModifications);
		modifiesTextsCheck.setToolTipText(TXMUIMessages.onlyTheModifiedCorpusTextsAreUpdated);
		modifiesTextsCheck.setSelection(!doForceUpdate);

		forceUpdateCheck = new Button(textsGroup, SWT.RADIO);
		forceUpdateCheck.setText(TXMUIMessages.common_all);
		forceUpdateCheck.setToolTipText(TXMUIMessages.allTheCorpusTextsWillBeUpdated);
		forceUpdateCheck.setSelection(doForceUpdate);

		//		Group todoGroup = new Group(comp, SWT.NONE);
		//		data = new GridData(GridData.FILL, GridData.FILL, true, false);
		//		todoGroup.setLayoutData(data);
		//		todoGroup.setLayout(new RowLayout(SWT.HORIZONTAL));
		//		todoGroup.setText("Update work to do");
		//		
		//		Button updateIndexCheck = new Button(todoGroup, SWT.CHECK);
		//		data = new GridData(GridData.FILL_HORIZONTAL);
		//		//updateIndexCheck.setLayoutData(data);
		//		updateIndexCheck.setText("Search indexes");
		//		updateIndexCheck.setToolTipText("This work cannot be disabled");
		//		updateIndexCheck.setSelection(true);
		//		updateIndexCheck.setEnabled(false);


		updateEditionsCheck = new Button(comp, SWT.CHECK);
		data = new GridData(GridData.FILL_HORIZONTAL);
		updateEditionsCheck.setLayoutData(data);
		updateEditionsCheck.setText(TXMUIMessages.UpdateTheEditionPages);
		updateEditionsCheck.setToolTipText(TXMUIMessages.IfNotCheckedTheEditionPagesAreNotUpdatedAndMightNotReflectThechanges);
		updateEditionsCheck.setSelection(doUpdateEditions);

		openCorpusParameters = new Button(comp, SWT.CHECK);
		data = new GridData(GridData.FILL_HORIZONTAL);
		openCorpusParameters.setLayoutData(data);
		openCorpusParameters.setText(TXMUIMessages.ShowMoreUpdateParametersBeforeUpdating);
		openCorpusParameters.setToolTipText(TXMUIMessages.UseThisToChangeTheCorpusParametersBeforeUpdatingTheCorpus);
		openCorpusParameters.setSelection(doOpenCorpusParameters);
	}

	protected void createTopMessageArea(Composite comp) {

		Label mainMessage = new Label(comp, SWT.WRAP);
		mainMessage.setText(topMessage);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		mainMessage.setLayoutData(data);
	}

	protected void createBottomMessageArea(Composite comp) {

		Label mainMessage = new Label(comp, SWT.WRAP);
		mainMessage.setText(bottomMessage);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		mainMessage.setLayoutData(data);
	}

	@Override
	protected void buttonPressed(int buttonId) {

		if (buttonId == Dialog.OK) {
			doUpdateEditions = updateEditionsCheck.getSelection();
			doForceUpdate = forceUpdateCheck.getSelection();
			doOpenCorpusParameters = openCorpusParameters.getSelection();
			if (corpusList != null) {
				corpusToUpdate = (MainCorpus) ((IStructuredSelection) corpusList.getSelection()).getFirstElement();
			}
		}

		super.buttonPressed(buttonId);
	}

	/**
	 * 
	 * @return the update edition button value AFTER this dialog is closed
	 */
	public boolean getDoUpdateEdition() {

		return doUpdateEditions;
	}

	/**
	 * 
	 * @return the force update button value AFTER this dialog is closed
	 */
	public boolean getDoForceUpdate() {

		return doForceUpdate;
	}

	public void setDoUpdateEdition(boolean doUpdateEditions) {

		this.doUpdateEditions = doUpdateEditions;
	}

	/**
	 * 
	 * @return the selected corpus to update
	 */
	public MainCorpus getCorpustoUpdate() {

		return this.corpusToUpdate;
	}

	public void setCorpusToUpdate(MainCorpus corpus) {

		this.corpusToUpdate = corpus;
	}

	public void setCorporaToUpdate(java.util.List<MainCorpus> corpora) {

		this.corpora = corpora;
	}

	public void setDoForceUpdate(boolean doForceUpdate) {

		this.doForceUpdate = doForceUpdate;
	}

	public boolean getDoOpenCorpusParameters() {

		return doOpenCorpusParameters;
	}

	public void setDoOpenCorpusParameters(boolean doOpenCorpusParameters) {

		this.doOpenCorpusParameters = doOpenCorpusParameters;
	}

	public void setMessages(String topMessage, String bottomMessage) {

		this.topMessage = topMessage;
		this.bottomMessage = bottomMessage;
	}

	public String getTopMessage() {
		return topMessage;
	}

	public String getBottomMessage() {
		return bottomMessage;
	}

	public void setTitle(String title) {

		this.title = title;
	}
}
