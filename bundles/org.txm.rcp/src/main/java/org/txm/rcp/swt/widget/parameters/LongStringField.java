package org.txm.rcp.swt.widget.parameters;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.kohsuke.args4j.NamedOptionDef;

public class LongStringField extends ParameterField {

	Text dt;

	public LongStringField(Composite parent, int style, NamedOptionDef param) {
		super(parent, style, param);
		this.setLayout(new GridLayout(2, false));
		this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		Label l = new Label(this, SWT.NONE);
		l.setAlignment(SWT.LEFT);
		l.setText(getWidgetLabel());
		l.setToolTipText(getWidgetUsage());
		GridData gd = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd.widthHint = 100;
		l.setLayoutData(gd);

		dt = new Text(this, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		GridData gdata = new GridData(SWT.FILL, SWT.FILL, true, true);
		gdata.heightHint = 2 * dt.getLineHeight();
		dt.setLayoutData(gdata);
		dt.setSize(300, 300);
		resetToDefault();
	}

	public String getWidgetValue() {
		return dt.getText();
	}

	@Override
	public void setDefault(Object value) {
		dt.setText("" + value); //$NON-NLS-1$
	}

	@Override
	public void resetToDefault() {
		dt.setText(getWidgetDefault());
	}
}
