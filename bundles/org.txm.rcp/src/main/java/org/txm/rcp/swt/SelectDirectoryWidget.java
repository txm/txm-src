package org.txm.rcp.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.txm.rcp.messages.TXMUIMessages;

/**
 * Composite to select a directory:
 * 
 * [optional title] + [text widget] + [button "..."]
 * 
 * @author mdecorde
 *
 */
public class SelectDirectoryWidget extends GLComposite {

	Text t;

	String path;

	public SelectDirectoryWidget(Composite parent, int style, String initialDefault, String label) {
		super(parent, style, "Select Directory"); //$NON-NLS-1$
		this.path = initialDefault;

		this.getLayout().numColumns = 2;
		this.getLayout().marginBottom = 2;
		this.getLayout().horizontalSpacing = 4;
		if (label != null) {
			this.getLayout().numColumns = this.getLayout().numColumns + 1;

			new Label(this, SWT.NONE).setText(label);
		}

		t = new Text(this, SWT.BORDER);
		t.setText(initialDefault);
		t.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		t.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				path = t.getText();
			}
		});

		Button selectDirectoryButton = new Button(this, SWT.PUSH);
		selectDirectoryButton.setText("..."); //$NON-NLS-1$
		selectDirectoryButton.setToolTipText("Select a directory from system");
		selectDirectoryButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				DirectoryDialog dialog = new DirectoryDialog(e.display.getActiveShell());
				dialog.setMessage(TXMUIMessages.SelectADirectory);
				dialog.setText(t.getText());
				String tpath = dialog.open();
				if (tpath != null) {
					t.setText(tpath);
					path = tpath;
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}

	public String getSelectedPath() {
		if (!t.isDisposed()) return t.getText();

		return path;
	}
}
