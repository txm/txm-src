package org.txm.rcp.testers;

import java.util.List;

import org.eclipse.core.expressions.PropertyTester;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

@SuppressWarnings("unchecked")
public class IsXTZCorpus extends PropertyTester {

	public static final String PROPERTY_NAMESPACE = "org.txm.rcp.testers.corpus"; //$NON-NLS-1$

	public static final String PROPERTY_CAN_SAVE = "xtzCorpus"; //$NON-NLS-1$

	@Override
	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {

		if (receiver instanceof List) {

			List<Object> list = (List<Object>) receiver;
			if (list.size() == 0) return false;
			receiver = list.get(0);
		}
		if (receiver instanceof CQPCorpus) {
			CQPCorpus c = (CQPCorpus) receiver;
			String script = c.getProject().getImportModuleName();
			if (script == null) return false;
			return script.startsWith("xtz"); //$NON-NLS-1$
		}
		return false;
	}
}
