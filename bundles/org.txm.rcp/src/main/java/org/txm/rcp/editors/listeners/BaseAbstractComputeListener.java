/**
 * 
 */
package org.txm.rcp.editors.listeners;

import java.util.EventObject;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Widget;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.TXMEditor;

/**
 * Abstract base listener dedicated to call the TXMEditor.compute() method from some Widgets.
 * Also responsible of setting the editor as dirty if auto-computing is disabled.
 * 
 * @author sjacquot
 *
 */
public abstract class BaseAbstractComputeListener {


	public final static String IGNORE_EVENT = "ignoreEvent"; //$NON-NLS-1$


	/**
	 * Linked editor.
	 */
	protected TXMEditor<? extends TXMResult> editor;


	/**
	 * Auto-computing mode.
	 */
	protected boolean autoCompute;

	/**
	 * 
	 * @param editor
	 * @param autoCompute
	 */
	public BaseAbstractComputeListener(TXMEditor<? extends TXMResult> editor, boolean autoCompute) {
		this.editor = editor;
		this.autoCompute = autoCompute;
	}

	/**
	 * 
	 * @param editor
	 */
	public BaseAbstractComputeListener(TXMEditor<? extends TXMResult> editor) {
		this(editor, false);
	}


	/**
	 * To skip programmatically setSelection() call. The event source Viewer must set the "ignore" option
	 * 
	 * @param event
	 * @return
	 */
	public static boolean mustIgnoreEvent(EventObject event) {
		// to skip programmatically setSelection() call, especially for the auto-updating editor Widgets from result methods
		if (event.getSource() instanceof Viewer) {
			if (((Viewer) event.getSource()).getData(IGNORE_EVENT) != null && (Boolean) ((Viewer) event.getSource()).getData(IGNORE_EVENT) == true) {
				return true;
			}
		}
		else if (event.getSource() instanceof Widget) {
			if (((Widget) event.getSource()).getData(IGNORE_EVENT) != null && (Boolean) ((Widget) event.getSource()).getData(IGNORE_EVENT) == true) {
				return true;
			}
		}

		return false;
	}

	/**
	 * 
	 * @param autoCompute
	 */
	public void setAutoCompute(boolean autoCompute) {
		this.autoCompute = autoCompute;
	}


}
