package org.txm.rcp.views.debug;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PartInitException;
import org.txm.rcp.handlers.BaseAbstractHandler;

public class RefreshDebugView extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			IViewPart view = getView(event, TXMEditorSWTDebugView.class.getName());
			if (view != null && view instanceof TXMEditorSWTDebugView) {
				((TXMEditorSWTDebugView) view).refresh();
			}
		}
		catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
