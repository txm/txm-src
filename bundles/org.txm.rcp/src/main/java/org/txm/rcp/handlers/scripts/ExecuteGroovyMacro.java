// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.scripts;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.txm.Toolbox;
import org.txm.rcp.editors.TxtEditor;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.JobHandler;

/**
 * Handler: execute a Macro (Groovy script that opens an UI for parameters)
 */
public class ExecuteGroovyMacro extends AbstractHandler {

	private static final String ID = "org.txm.rcp.commands.ExecuteMacro"; //$NON-NLS-1$
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		String filenameParameter = event.getParameter("org.txm.rcp.command.parameter.file"); //$NON-NLS-1$
		List<String> stringArgs = null;
		if (event.getParameter("args") != null) {
			stringArgs = Arrays.asList(event.getParameter("args").split("\t")); //$NON-NLS-1$ //$NON-NLS-2$
		}
		else {
			stringArgs = new ArrayList<String>();
		}

		// the file to execute
		String result = ""; //$NON-NLS-1$
		IWorkbenchPart activePart = HandlerUtil.getActivePart(event);
		IWorkbenchPart activeEditor = HandlerUtil.getActiveEditor(event);

		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (activeEditor != null && activeEditor instanceof TxtEditor) {
			TxtEditor te = (TxtEditor) activeEditor;
			selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
			FileStoreEditorInput input = (FileStoreEditorInput) te.getEditorInput();

			File f = new File(input.getURI().getPath());
			result = f.getAbsolutePath();
		}
		else if (filenameParameter != null) {
			result = filenameParameter;
			File f = new File(filenameParameter);
			if (!f.exists()) { // file not found, try locating it in Groovy scripts
				f = new File(Toolbox.getTxmHomePath() + "/scripts/groovy/user", filenameParameter); //$NON-NLS-1$
				if (f.exists()) { // found !
					result = f.getAbsolutePath();
				}
			}
		}
		else if (selection != null
				& selection instanceof IStructuredSelection) {
					File file = null;
					IStructuredSelection strucSelection = (IStructuredSelection) selection;
					for (Iterator<Object> iterator = strucSelection.iterator(); iterator.hasNext();) {
						file = (File) iterator.next();
						result = file.getAbsolutePath();
						break;
					}
				}

		if (result == null || result.length() == 0) {
			Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
			FileDialog dialog = new FileDialog(shell, SWT.OPEN);
			if (LastOpened.getFile(ID) != null) {
				dialog.setFilterPath(LastOpened.getFolder(ID));
				dialog.setFileName(LastOpened.getFile(ID));
			}
			dialog.setFilterExtensions(new String[] { "*.groovy" }); //$NON-NLS-1$

			String scriptRootDir = Toolbox.getTxmHomePath() + "/scripts/groovy/user"; //$NON-NLS-1$

			dialog.setFilterPath(scriptRootDir);
			result = dialog.open();
			if (result == null) return null;
			LastOpened.set(ID, new File(result));
		}

		return execute(result, activePart, activeEditor, selection, stringArgs, null, null);
	}

	public static JobHandler execute(String scriptpath, IWorkbenchPart activePart, IWorkbenchPart activeEditor, ISelection selection, List<String> args, HashMap<String, Object> parameters,
			HashMap<String, Object> defaultParameters) {
		//IPreferencesService service = Platform.getPreferencesService();
		String scriptRootDir = Toolbox.getTxmHomePath() + "/scripts"; //$NON-NLS-1$
		File currentRootDir = new File(scriptRootDir, "groovy/user"); //$NON-NLS-1$

		if (scriptpath.endsWith(".groovy")) { //$NON-NLS-1$
			return ExecuteGroovyScript.executeScript(currentRootDir, scriptpath, activePart, activeEditor, selection, false, args, parameters, defaultParameters);
		}
		else {
			return ExecuteScript.executeScript(scriptpath, activePart, activeEditor, selection, args);
		}
	}
}
