package org.txm.rcp.swt.widget.structures;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnit;

/**
 * Combo viewer for structural units with only multiple selection support.
 * 
 * @author sjacquot
 *
 */
public class ___StructuralUnitsListViewer extends ListViewer {


	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param autoCompute
	 * @param selectedSU
	 * @param addBlankEntry to add an empty blank entry at start of the list to clear value.
	 */
	public ___StructuralUnitsListViewer(Composite parent, TXMEditor editor, boolean autoCompute, StructuralUnit selectedSU, boolean addBlankEntry) {
		super(parent, SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI | SWT.READ_ONLY);

		this.setContentProvider(ArrayContentProvider.getInstance());

		// Input
		try {
			this.setInput(CQPCorpus.getFirstParentCorpus(editor.getResult()).getOrderedStructuralUnits().toArray());
			// add empty entry
			if (addBlankEntry) {
				this.insert(null, 0);
			}
		}
		catch (CqiClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (selectedSU != null) {
			this.setSelection(new StructuredSelection(selectedSU), true);
		}

		// Listeners
		this.addSelectionChangedListener(new ComputeSelectionListener(editor));
	}



}
