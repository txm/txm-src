package org.txm.rcp.swt.provider;

import java.util.HashMap;
import java.util.List;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

public class HashMapColumnLabelProvider extends LabelProvider implements ITableLabelProvider {

	List<? extends Object> keys;

	public HashMapColumnLabelProvider(List<? extends Object> keys) {
		this.keys = keys;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
	 * 
	 * @param element
	 * @param columnIndex
	 */
	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		if (element != null && element instanceof HashMap) {
			if (columnIndex < keys.size())
				return "" + ((HashMap<?, ?>) element).get(keys.get(columnIndex)); //$NON-NLS-1$
			return "#idx too high"; //$NON-NLS-1$
		}
		return "#not list"; //$NON-NLS-1$
	}
}
