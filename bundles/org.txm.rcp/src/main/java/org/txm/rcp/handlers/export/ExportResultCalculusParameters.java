// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.rcp.handlers.export;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.TXMResult;
import org.txm.rcp.StatusLine;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.DeleteDir;
import org.txm.utils.logger.Log;
import org.txm.utils.zip.Zip;

/**
 * Exports the parameters of a TXMREsult to a zip file with .txmcmd extension.
 * Can also export the parents and the children of the branch.
 * The result parameters are stored inside the zip file in some .parameters text files.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
//FIXME: SJ: the last used path is not saved and not restored in the dialog 
public class ExportResultCalculusParameters extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);

		Object o = selection.getFirstElement();
		
		if (o == null && !(o instanceof TXMResult) && event != null) { // cannot use active selection, try the active editor
			IEditorPart editor = HandlerUtil.getActiveEditor(event);
			if (editor instanceof TXMEditor<?> txmEditor) {
				o = txmEditor.getResult();
			}
		}
		
		if (o == null || !(o instanceof TXMResult)) {
			Log.warning(TXMUIMessages.selectionMustBeACalculus);
		}

		TXMResult result = (TXMResult) o;

		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

		ExportResultCalculusParametersDialog dialog = new ExportResultCalculusParametersDialog(shell, result);

		if (dialog.open() == dialog.OK) {
			StatusLine.setMessage(TXMUIMessages.exportingResults);

			String filepath = dialog.getZipFile().getAbsolutePath();
			if (!(filepath.endsWith(".txmcmd"))) { //$NON-NLS-1$
				filepath += filepath + ".txmcmd"; //$NON-NLS-1$
			}

			final File zipfiledir = new File(filepath); // temporary directory to setup the archive
			LastOpened.set(ExportResultCalculusParameters.class.getName(), zipfiledir.getParent(), zipfiledir.getName());
			zipfiledir.delete();
			zipfiledir.mkdirs();

			final File zipfile = new File(filepath + ".tmp"); //$NON-NLS-1$

			JobHandler jobhandler = new JobHandler(TXMUIMessages.exportingResults) {

				@Override
				protected IStatus _run(SubMonitor monitor) throws Exception {

					try {
						HashSet<TXMResult> toExport = new HashSet<TXMResult>();
						for (Object o : selection.toList()) {
							if (!(o instanceof TXMResult)) continue;
							TXMResult r = (TXMResult) o;
							toExport.add(r);
							TXMResult parent = ((TXMResult) o).getParent();

							// add invisible parents to rebuild the result
							while (parent != null && !parent.isVisible()) { // invisible parent must be added to the exported results

								toExport.add(parent);
								parent = parent.getParent();
							}

							// add children 
							if (dialog.getExportChildren()) {
								for (TXMResult c : r.getDeepChildren()) {
									toExport.add(c);
								}
							}

							// add parents 
							if (dialog.getExportParents()) {
								TXMResult p = r.getParent();
								while (p != null && (!(p instanceof MainCorpus))) {
									toExport.add(p);
									p = p.getParent();
								}
							}
						}

						ArrayList<File> files = new ArrayList<>();
						for (TXMResult result : toExport) {

							if (result.isAltered()) {
								Log.info(TXMUIMessages.ExportResultParameters_WarningOnlyTheParametersAreExportedManualChangesAreNotTransfered);
							}
							else {
								result.compute(monitor); // refresh result
							}

							File outfile = new File(zipfiledir, (TXMPreferences.getNode(result.getParametersNodePath()).name() + ".parameters")); //$NON-NLS-1$
							if (result.toParametersFile(outfile)) {
								files.add(outfile);
							}
						}

						Log.info(NLS.bind(TXMUIMessages.ExportResultParameters_ParametersExportedToTheP0File, zipfiledir));
						Zip.compress(files.toArray(new File[files.size()]), zipfile, Zip.DEFAULT_LEVEL_COMPRESSION, null, null);
						DeleteDir.deleteDirectory(zipfiledir);
						zipfile.renameTo(zipfiledir);
					}
					catch (Exception e) {
						Log.severe(NLS.bind(TXMUIMessages.failedToExportResultP0ColonP1, selection.toList(), e));
						throw e;
					}
					return Status.OK_STATUS;
				}
			};

			jobhandler.startJob();
		}
		return null;
	}
}
