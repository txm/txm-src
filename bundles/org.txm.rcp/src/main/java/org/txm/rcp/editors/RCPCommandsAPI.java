package org.txm.rcp.editors;

import org.txm.core.results.TXMParameters;
import org.txm.core.results.TXMResult;
import org.txm.functions.Command;
import org.txm.functions.CommandsAPI;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.utils.logger.Log;


/**
 * Interface to call commands from a RCP plugin
 * 
 * @author mdecorde
 *
 */
public class RCPCommandsAPI extends CommandsAPI {

	/**
	 * @param cmd the command to call or the command to open with the right editor
	 * @param source the result parent
	 * @param parameters the parameters to compute the command or the parameters to set editor parameters with
	 * 
	 * @return A result or the editor's result. null if the command failed to compute or to open editor.
	 */
	//FIXME: the source is not used, see if we decide to put it the TXMParameters or not, otherwise define a method TXMResult.setSource() or maybe better a method TXMResult.compute(TXMResult source)
	public Object call(String cmd, TXMResult source, TXMParameters parameters) {

		Command command = cmds.get(cmd);
		if (command == null) {
			Log.warning(TXMUIMessages.bind(TXMUIMessages.NoCommandFoundWithNameP0, cmd));
			return null;
		}

		if (command instanceof RCPCommand) {

			RCPCommand rcpcommand = (RCPCommand) command;
			TXMResult result = null;

			if (rcpcommand.editor_id == null) {
				System.out.println(TXMUIMessages.FailedToOpenTheEditorNoContextGiven);
				return null;
			}

			try {
				// the editor will compute the command
				result = rcpcommand.clazz.newInstance();
				result.setParameters(parameters);

				TXMEditor.openEditor(result, rcpcommand.editor_id);
				//				if (editor != null) {
				//					return editor;
				//				} else	{
				//					System.out.println("Failed to open editor: internal error during initialisation.");
				//					System.out.println("Failed to open editor with "+cmd+" command and "+parameters+" parameters.");
				//				}
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
		}
		else {
			Object result = super.call(cmd, source, parameters);
			Log.info(TXMUIMessages.bind(TXMUIMessages.DoneP0, result));
			return result;
		}
	}

	//dans Handler.execute() RCPCommandsAPI.call("Concordance", params);
}
