package org.txm.rcp.commands;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ListSelectionDialog;
import org.osgi.framework.Version;
import org.txm.Toolbox;
import org.txm.rcp.Activator;
import org.txm.rcp.commands.workspace.LoadBinaryCorporaDirectory;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.JobHandler;
import org.txm.utils.DeleteDir;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

public class RecoverCorporaFromInstalls extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		JobHandler job = new JobHandler(TXMUIMessages.recoveringCorpora) {

			@Override
			protected IStatus _run(SubMonitor monitor) throws IllegalStateException, IOException {

				restore(false);
				return org.eclipse.core.runtime.Status.OK_STATUS;
			}
		};

		job.schedule();
		return job;
	}

	public static void restore(boolean currentTXM) throws IllegalStateException, IOException {

		String installdirpath = Toolbox.getInstallDirectory();
		File sampleCorporaDirectory = new File(installdirpath, "samples"); //$NON-NLS-1$

		File txmhomedir = Platform.getLocation().toFile().getCanonicalFile();
		txmhomedir.mkdirs(); // creates the directory if needed

		File corporaDir = new File(txmhomedir, "corpora"); //$NON-NLS-1$

		String previousVersions[] = { "0.8.3", "0.8.2", "0.8.1", "0.8.0", "0.7.9" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		String previousVersionLabels[] = new String[previousVersions.length];
		File previousVersionCorporaDirectories[] = new File[previousVersions.length];

		for (int i = 0; i < previousVersions.length; i++) {
			String v = previousVersions[i];

			previousVersionLabels[i] = TXMUIMessages.bind(TXMUIMessages.corporaOfPreviousTXMP0, v);

			File dir = new File(System.getProperty("user.home"), "TXM-" + v + "/corpora"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			if ("0.7.9".equals(v)) { //$NON-NLS-1$
				dir = new File(System.getProperty("user.home"), "TXM/corpora"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			previousVersionCorporaDirectories[i] = dir;
		}

		Version v = Activator.getDefault().getBundle().getVersion();
		String vv = v.getMajor() + "." + v.getMinor() + "." + v.getMicro(); //$NON-NLS-1$ //$NON-NLS-2$ $NON-NLS-2$
		String SAMPLES = "1. " + TXMUIMessages.bind(TXMUIMessages.sampleCorporaOfCurrentTXMP0, vv); //$NON-NLS-1$
		int n = 2;
		String vCURRENT = "2. " + TXMUIMessages.bind(TXMUIMessages.corporaOfPreviousTXMP0, vv); //$NON-NLS-1$


		final ArrayList<Object> selectedCorporaDirs = new ArrayList<>();
		final ArrayList<Object> initialSelectionCorporaDirs = new ArrayList<>();

		selectedCorporaDirs.add(SAMPLES);
		initialSelectionCorporaDirs.add(SAMPLES);

		if (currentTXM && corporaDir.exists() && corporaDir.listFiles(IOUtils.HIDDENFILE_FILTER).length > 0) {
			selectedCorporaDirs.add(vCURRENT);
			initialSelectionCorporaDirs.add(vCURRENT);
			n++;
		}

		for (int i = 0; i < previousVersions.length; i++) {
			if (previousVersionCorporaDirectories[i].exists()
					&& previousVersionCorporaDirectories[i].listFiles(IOUtils.HIDDENFILE_FILTER).length > 0) {
				previousVersionLabels[i] = "" + (n++) + ". " + previousVersionLabels[i]; //$NON-NLS-1$ //$NON-NLS-2$
				selectedCorporaDirs.add(previousVersionLabels[i]); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}

		String OTHER = "" + (n++) + ". " + TXMUIMessages.selectAnotherTXMCorporaToRestore; //$NON-NLS-1$ //$NON-NLS-2$
		//selectedCorporaDirs.add(OTHER);

		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				Display display = Display.getDefault();
				Shell shell = display.getActiveShell();

				ListSelectionDialog dialog = ListSelectionDialog.of(selectedCorporaDirs)
						.contentProvider(new ArrayContentProvider())
						.labelProvider(new LabelProvider())
						.message(TXMUIMessages.corporaWillBeRestoredInTheFollowingOrderCorporaAlreadyRestoredWillNotBeReplaced)
						.title(TXMUIMessages.selectThecorporaSetsToRestore)
						.create(shell);

				dialog.setTitle(TXMUIMessages.selectThecorporaSetsToRestore);
				dialog.setInitialElementSelections(initialSelectionCorporaDirs);
				if (dialog.open() == ListSelectionDialog.OK) {
					selectedCorporaDirs.clear();
					List<Object> selection = Arrays.asList(dialog.getResult());
					selectedCorporaDirs.addAll(selection);
				}
				else {
					selectedCorporaDirs.clear();
				}
			}
		});

		// move the previous-version TXM corpora
		File backupDir = new File(corporaDir.getAbsolutePath() + "-previous"); //$NON-NLS-1$
		if (currentTXM) {
			if (corporaDir.exists()) {
				backupDir.mkdirs();
				for (File dir : corporaDir.listFiles(IOUtils.HIDDENFILE_FILTER)) {
					File bdir = new File(backupDir, dir.getName());
					DeleteDir.deleteDirectory(bdir);
					dir.renameTo(bdir);
				}
			}

			corporaDir.mkdir();
		}

		// load TXM current-version sample corpora (from the install directory)
		if (selectedCorporaDirs.contains(SAMPLES) && sampleCorporaDirectory.exists()) {

			Log.info(TXMUIMessages.installingSampleCorpus);

			JobHandler job = LoadBinaryCorporaDirectory.loadBinaryCorpusFromCorporaDirectory(sampleCorporaDirectory, false, true, false);
			try {
				job.join();
			}
			catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// TXM-CURRENT previous installation corpora
		if (currentTXM && selectedCorporaDirs.contains(vCURRENT) && backupDir.listFiles(IOUtils.HIDDENFILE_FILTER).length > 0) { // not a new install, restore corpus from the corpora-previous directory
			Log.info(NLS.bind(TXMUIMessages.installingPreviousCorpusFromP0, vv, corporaDir));
			JobHandler job = LoadBinaryCorporaDirectory.loadBinaryCorpusFromCorporaDirectory(backupDir, false, false, true);
			try {
				job.join();
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		for (int i = 0; i < previousVersions.length; i++) {
			if (selectedCorporaDirs.contains(previousVersionLabels[i])) {

				Log.info(NLS.bind(TXMUIMessages.installingPreviousCorpusFromP0, previousVersions[i], previousVersionCorporaDirectories[i]));

				JobHandler job = LoadBinaryCorporaDirectory.loadBinaryCorpusFromCorporaDirectory(previousVersionCorporaDirectories[i], false, false, true);
				try {
					job.join();
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		// recovery from an other corpora directory to select by the user
		if (selectedCorporaDirs.contains(OTHER)) { // new installation (empty corpora directory)
			final HashMap<String, String> bindings = new HashMap<>();
			Display.getDefault().syncExec(new Runnable() {

				@Override
				public void run() {
					Display display = Display.getDefault();
					Shell shell = display.getActiveShell();
					DirectoryDialog dialog = new DirectoryDialog(shell);
					String path = dialog.open();
					if (path == null) return;

					bindings.put("path", path); //$NON-NLS-1$
				}
			});
			if (bindings.containsKey("path")) { //$NON-NLS-1$
				File dir = new File(bindings.get("path")); //$NON-NLS-1$
				if (!dir.equals(corporaDir)) {
					Log.info(NLS.bind(TXMUIMessages.restoringCorporaFromTheP0Directory, dir));
					JobHandler job = LoadBinaryCorporaDirectory.loadBinaryCorpusFromCorporaDirectory(dir, true, true, true);
					try {
						job.join();
					}
					catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}

		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				RestartTXM.reloadViews();
			}
		});
	}
}
