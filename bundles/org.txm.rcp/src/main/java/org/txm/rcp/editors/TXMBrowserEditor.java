// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.editors;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.OpenWindowListener;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.browser.WindowEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.internal.browser.BrowserViewer;
import org.eclipse.ui.internal.browser.WebBrowserEditor;
import org.txm.rcp.commands.OpenBrowser;
import org.txm.rcp.editors.menu.PagePropertiesMenu;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.IOClipboard;

/**
 * Call the internal browser of RCP specialized for TXM editions.
 * 
 * A Text can be set then the user can browse pages and others texts of the same corpus
 * 
 * @author mdecorde.
 */
public class TXMBrowserEditor extends WebBrowserEditor {

	/** The Constant ID. */
	public final static String ID = TXMBrowserEditor.class.getName(); //$NON-NLS-1$

	/**
	 * Instantiates a new TXM browser.
	 */
	public TXMBrowserEditor() {

	}

	/**
	 * Gets the browser.
	 *
	 * @return the browser
	 */
	public Browser getBrowser() {
		BrowserViewer webbrowser = this.webBrowser;
		return webbrowser.getBrowser();
	}

	@Override
	public void setPartName(String partName) {
		super.setPartName(partName);
	}

	@Override
	public void setTitleImage(Image titleImage) {
		super.setTitleImage(titleImage);
	}

	CommandLink cmdLink;

	private MenuManager menuManager;

	/**
	 * Tests to see if two input objects are equal in the sense that they can share an
	 * editor.
	 * 
	 * @return true if the url and browser id are equal and the style bits are compatible
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof TXMBrowserEditor))
			return false;
		TXMBrowserEditor other = (TXMBrowserEditor) obj;

		String u1 = this.getBrowser().getUrl();
		String u2 = other.getBrowser().getUrl();

		return u1.equals(u2);
	}


	/**
	 * 
	 */
	public void initializeProgressListener() {

		ProgressListener progresslistener = new ProgressListener() {

			@Override
			public void changed(ProgressEvent event) {
			}

			@Override
			public void completed(ProgressEvent event) {

				Object o = TXMBrowserEditor.this.getBrowser().evaluate("return typeof " + CommandLink.FCT + ";"); //$NON-NLS-1$ //$NON-NLS-2$
				if ("undefined".equals(o)) { //$NON-NLS-1$
					if (cmdLink != null) {
						cmdLink.dispose();
					}
					cmdLink = new CommandLink(TXMBrowserEditor.this, TXMBrowserEditor.this.getBrowser());
				}

				TXMBrowserEditor.this.getBrowser().execute("""
						document.addEventListener('mouseover', function (e) {
						   document.currentHoverElement = e.target
						});	""");
			}
		};
		getBrowser().addProgressListener(progresslistener);
	}

	public static void zoomIn(Browser b) {
		zoom(b, 0.1f);
	}

	public static void zoomOut(Browser b) {
		zoom(b, -0.1f);
	}

	public static void zoomReset(Browser b) {
		zoom(b, 0f);
	}

	public static void zoom(Browser b, float diff) {

		if (b == null || b.isDisposed()) return;

		//	b.evaluate("alert(document);");
		//	b.evaluate("alert(document.children[0].style);");
		//		b.evaluate("alert(document.children[0].children[1]);");
		boolean isHTML = (boolean) b.evaluate("return document.getElementsByTagName('body')[0] != null;"); //$NON-NLS-1$
		if (isHTML) {
			String zoom = (String) b.evaluate("return document.getElementsByTagName('body')[0].style.zoom;"); //$NON-NLS-1$
			if (zoom == null || zoom.isEmpty()) {
				zoom = "1.0"; //$NON-NLS-1$
			}

			float fzoom = Float.parseFloat(zoom) + diff;
			if (diff == 0f) {
				fzoom = 1.0f;
			}

			b.evaluate("document.getElementsByTagName('body')[0].style.zoom = \"" + fzoom + "\";"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		else {
			String zoom = (String) b.evaluate("return document.children[0].style.zoom;"); //$NON-NLS-1$
			if (zoom == null || zoom.isEmpty()) {
				zoom = "1.0"; //$NON-NLS-1$
			}

			float fzoom = Float.parseFloat(zoom) + diff;
			if (diff == 0f) {
				fzoom = 1.0f;
			}

			b.evaluate("document.children[0].style.zoom = \"" + fzoom + "\";"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	MenuItem sep1;

	MenuItem copyItem;

	MenuItem copyLink;

	MenuItem openLink;

	MenuItem zoomInItem;

	MenuItem zoomOutItem;

	MenuItem zoomResetItem;

	MenuItem reloadItem;

	MenuItem backItem;

	MenuItem forwardItem;

	MenuItem propertiesItem;

	public void initMenu() {

		// create a new menu
		menuManager = new MenuManager();

		Menu menu = menuManager.createContextMenu(this.getBrowser());

		menu.addMenuListener(new MenuListener() {

			@Override
			public void menuShown(MenuEvent e) {

				Menu menu = menuManager.getMenu();
				if (menu == null) return;
				
				String dom = TXMBrowserEditor.this.getTextSelectionDOM();
				Object currentHref = TXMBrowserEditor.this.getBrowser().evaluate("return document.currentHoverElement.getAttribute('href')");

				if (copyItem == null) {

					sep1 = new MenuItem(menu, SWT.SEPARATOR);

					copyItem = new MenuItem(menu, SWT.NONE);
					copyItem.setText(TXMUIMessages.Copy);
					copyItem.addSelectionListener(new SelectionListener() {

						@Override
						public void widgetSelected(SelectionEvent e) {

							String text = TXMBrowserEditor.this.getTextSelection();
							if (text != null && text.length() > 0) {
								IOClipboard.write(text);
							}
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent e) {
						}
					});



					openLink = new MenuItem(menu, SWT.NONE);
					openLink.setText("Open link");
					openLink.addSelectionListener(new SelectionListener() {

						@Override
						public void widgetSelected(SelectionEvent e) {

							Object currentHref = TXMBrowserEditor.this.getBrowser().evaluate("return document.currentHoverElement.getAttribute('href')");
							if (currentHref != null) {
								OpenBrowser.openfile(currentHref.toString());
							}
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent e) {
						}
					});

					copyLink = new MenuItem(menu, SWT.NONE);
					copyLink.setText("Copy link");
					copyLink.addSelectionListener(new SelectionListener() {

						@Override
						public void widgetSelected(SelectionEvent e) {

							Object currentHref = TXMBrowserEditor.this.getBrowser().evaluate("return document.currentHoverElement.getAttribute('href')");
							if (currentHref != null) {
								IOClipboard.write(currentHref.toString());
							}
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent e) {
						}
					});

					zoomInItem = new MenuItem(menu, SWT.NONE);
					zoomInItem.setText(TXMUIMessages.zoomIn);
					zoomInItem.addSelectionListener(new SelectionListener() {

						@Override
						public void widgetSelected(SelectionEvent e) {
							zoomIn(getBrowser());
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent e) {
						}
					});

					zoomOutItem = new MenuItem(menu, SWT.NONE);
					zoomOutItem.setText(TXMUIMessages.zoomOut);
					zoomOutItem.addSelectionListener(new SelectionListener() {

						@Override
						public void widgetSelected(SelectionEvent e) {

							zoomOut(getBrowser());
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent e) {
						}
					});

					zoomResetItem = new MenuItem(menu, SWT.NONE);
					zoomResetItem.setText(TXMUIMessages.initialZoom);
					zoomResetItem.addSelectionListener(new SelectionListener() {

						@Override
						public void widgetSelected(SelectionEvent e) {

							zoomReset(getBrowser());
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent e) {
						}
					});

					reloadItem = new MenuItem(menu, SWT.NONE);
					reloadItem.setText(TXMUIMessages.Reload);
					reloadItem.addSelectionListener(new SelectionListener() {

						@Override
						public void widgetSelected(SelectionEvent e) {

							TXMBrowserEditor.this.getBrowser().refresh();
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent e) {
						}
					});
					backItem = new MenuItem(menu, SWT.NONE);
					backItem.setText(TXMUIMessages.Back);
					backItem.addSelectionListener(new SelectionListener() {

						@Override
						public void widgetSelected(SelectionEvent e) {

							TXMBrowserEditor.this.getBrowser().back();
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent e) {
						}
					});
					forwardItem = new MenuItem(menu, SWT.NONE);
					forwardItem.setText(TXMUIMessages.Forward);
					forwardItem.addSelectionListener(new SelectionListener() {

						@Override
						public void widgetSelected(SelectionEvent e) {

							TXMBrowserEditor.this.getBrowser().forward();
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent e) {
						}
					});

					propertiesItem = new PagePropertiesMenu(getBrowser(), menu, SWT.NONE);
				}
				
				copyItem.setEnabled(dom != null && dom.length() > 0);
				openLink.setEnabled(currentHref != null);
				copyLink.setEnabled(currentHref != null);
			}

			@Override
			public void menuHidden(MenuEvent e) {
			}
		});
		// // restore old listeners
		// Menu origMenu = this.browser.getMenu();
		// if (origMenu != null) {
		// if (origMenu.getListeners(SWT.Hide) != null)
		// for (Listener listener : origMenu.getListeners(SWT.Hide)) menu.addListener(SWT.Hide, listener);
		// if (origMenu.getListeners(SWT.Show) != null)
		// for (Listener listener : origMenu.getListeners(SWT.Show)) menu.addListener(SWT.Show, listener);
		// for (MenuItem origItem : origMenu.getItems()) { // restore old menu items
		// MenuItem item = new MenuItem(menu, origItem.getStyle());
		// item.setText(origItem.getText());
		// item.setImage(origItem.getImage());
		// if (origItem.getListeners(SWT.Selection) != null && origItem.getListeners(SWT.Selection).length > 0)
		// item.addListener(SWT.Selection, origItem.getListeners(SWT.Selection)[0]);
		// }
		// }
		// replace the menu
		this.getBrowser().setMenu(menu);
	}

	public static void installZoomControls(Browser browser, Menu menu) {

		browser.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseScrolled(MouseEvent e) {

				if ((e.stateMask & SWT.CTRL) == SWT.CTRL) {
					if (e.count > 0) {
						zoomIn(browser);
					}
					else {
						zoomOut(browser);
					}
				}
			}
		});


		MenuItem zoomInItem = new MenuItem(menu, SWT.NONE);
		zoomInItem.setText(TXMUIMessages.zoomIn);
		zoomInItem.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				zoomIn(browser);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		MenuItem zoomOutItem = new MenuItem(menu, SWT.NONE);
		zoomOutItem.setText(TXMUIMessages.zoomOut);
		zoomOutItem.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				zoomOut(browser);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		MenuItem zoomResetItem = new MenuItem(menu, SWT.NONE);
		zoomResetItem.setText(TXMUIMessages.initialZoom);
		zoomResetItem.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				zoomReset(browser);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.internal.browser.WebBrowserEditor#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		try {
			super.createPartControl(parent);

			cmdLink = new CommandLink(TXMBrowserEditor.this, TXMBrowserEditor.this.getBrowser());

			parent.addDisposeListener(new DisposeListener() {

				@Override
				public void widgetDisposed(DisposeEvent e) {

					cmdLink.dispose();
				}
			});

		}
		catch (Exception e) {
			System.err.println(TXMUIMessages.couldNotLaunchTheInternalWebBrowserYouMustSetThatParameterInThePreferencesMenuColonGeneralSupWebNavigator);
			return;
		}


		//		RowLayout navigLayout = new RowLayout(SWT.HORIZONTAL);
		//		navigLayout.justify = true;
		//		Composite panneau = new Composite(parent, SWT.NONE);
		//		panneau.setLayout(navigLayout);
		//		
		//		RowLayout subLayout = new RowLayout(SWT.HORIZONTAL);
		//		subLayout.pack = true;
		//		subLayout.center = true;
		//		subLayout.marginBottom = subLayout.marginHeight = 0;
		//		subLayout.marginTop = subLayout.marginWidth = 0;
		//		Composite subPanneau = new Composite(panneau, SWT.NONE);
		//		subPanneau.setLayout(subLayout);
		//		subPanneau.setLayoutData(new RowData(450, 22));
		//		
		//		Button firstText = new Button(subPanneau, SWT.FLAT);
		//		Button previousText = new Button(subPanneau, SWT.FLAT);
		//		Button first = new Button(subPanneau, SWT.FLAT | SWT.PUSH);
		//		Button previous = new Button(subPanneau, SWT.FLAT | SWT.PUSH);
		//		page_label = new Label(subPanneau, SWT.NONE);
		//		Button next = new Button(subPanneau, SWT.FLAT | SWT.PUSH);
		//		Button last = new Button(subPanneau, SWT.FLAT | SWT.PUSH);
		//		Button nextText = new Button(subPanneau, SWT.FLAT | SWT.PUSH);
		//		Button lastText = new Button(subPanneau, SWT.FLAT | SWT.PUSH);
		//		
		//		// set sizes
		//		page_label.setLayoutData(new RowData(50, 20));
		//		firstText.setLayoutData(new RowData(30, 20));
		//		first.setLayoutData(new RowData(30, 20));
		//		previous.setLayoutData(new RowData(30, 20));
		//		next.setLayoutData(new RowData(30, 20));
		//		last.setLayoutData(new RowData(30, 20));
		//		nextText.setLayoutData(new RowData(30, 20));
		//		previousText.setLayoutData(new RowData(30, 20));
		//		lastText.setLayoutData(new RowData(30, 20));


		//		// set labels
		//		page_label.setText(""); //$NON-NLS-1$
		//		firstText.setImage(IImageKeys.getImage(IImageKeys.CTRLREWINDSTART));
		//		firstText.setToolTipText("|<<");
		//		previousText.setImage(IImageKeys.getImage(IImageKeys.CTRLREWIND));
		//		previousText.setToolTipText(TXMUIMessages.previousText);
		//		first.setImage(IImageKeys.getImage(IImageKeys.CTRLSTART));
		//		first.setToolTipText(TXMUIMessages.common_beginning);
		//		previous.setImage(IImageKeys.getImage(IImageKeys.CTRLREVERSE));
		//		previous.setToolTipText(TXMUIMessages.previousPage);
		//		next.setImage(IImageKeys.getImage(IImageKeys.CTRLPLAY));
		//		next.setToolTipText(TXMUIMessages.nextPage);
		//		last.setImage(IImageKeys.getImage(IImageKeys.CTRLEND));
		//		last.setToolTipText(TXMUIMessages.common_end);
		//		nextText.setImage(IImageKeys.getImage(IImageKeys.CTRLFASTFORWARD));
		//		nextText.setToolTipText(TXMUIMessages.nextText);
		//		lastText.setImage(IImageKeys.getImage(IImageKeys.CTRLFASTFORWARDEND));
		//		lastText.setToolTipText(">>|");
		//		
		//		
		//		// set listeners
		//		first.addSelectionListener(new SelectionListener() {
		//			
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) {
		//				firstPage();
		//			}
		//			
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				firstPage();
		//			}
		//		});
		//		previous.addSelectionListener(new SelectionListener() {
		//			
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) {
		//				previousPage();
		//			}
		//			
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				previousPage();
		//			}
		//		});
		//		next.addSelectionListener(new SelectionListener() {
		//			
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) {
		//				nextPage();
		//			}
		//			
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				nextPage();
		//			}
		//		});
		//		last.addSelectionListener(new SelectionListener() {
		//			
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) {
		//				lastPage();
		//			}
		//			
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				lastPage();
		//			}
		//		});
		//		nextText.addSelectionListener(new SelectionListener() {
		//			
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) {
		//				nextText();
		//			}
		//			
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				nextText();
		//			}
		//		});
		//		// set listeners
		//		previousText.addSelectionListener(new SelectionListener() {
		//			
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) {
		//				previousText(false);
		//			}
		//			
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				previousText(false);
		//			}
		//		});
		//		lastText.addSelectionListener(new SelectionListener() {
		//			
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) {
		//				lastText();
		//			}
		//			
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				lastText();
		//			}
		//		});
		//		// set listeners
		//		firstText.addSelectionListener(new SelectionListener() {
		//			
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) {
		//				firstText();
		//			}
		//			
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				firstText();
		//			}
		//		});
		//		
		//		subPanneau.pack();
		//		panneau.pack();

		//		FormLayout parentLayout = new FormLayout();

		//		FormData datatop = new FormData();
		//		datatop.top = new FormAttachment(0, 0);
		//		datatop.left = new FormAttachment(0, 0);
		//		datatop.right = new FormAttachment(100, 0);
		//		datatop.bottom = new FormAttachment(panneau, 0);
		//		webBrowser.setLayoutData(datatop);
		//		
		//		FormData databottom = new FormData(200, 30);
		//		databottom.bottom = new FormAttachment(100, 0);
		//		databottom.left = new FormAttachment(30, -100);
		//		databottom.right = new FormAttachment(80, 100);
		//		panneau.setLayoutData(databottom);
		//		
		//		parent.setLayout(parentLayout);
		//		parent.pack();

		//		MenuManager menuManager = new MenuManager();
		//		Menu menu = menuManager.createContextMenu(getBrowser());
		//		
		//		// Set the MenuManager
		//		getBrowser().setMenu(menu);

		initMenu();

		//		getBrowser().addMouseListener(new MouseListener() {
		//			
		//			@Override
		//			public void mouseUp(MouseEvent e) { }
		//			
		//			@Override
		//			public void mouseDown(MouseEvent e) { }
		//			
		//			@Override
		//			public void mouseDoubleClick(MouseEvent e) {
		//				
		//				Browser b = TXMBrowserEditor.this.webBrowser.getBrowser();
		//				b.evaluate(SCRIPT01)
		//			}
		//		});

		getBrowser().addOpenWindowListener(new OpenWindowListener() {

			@Override
			public void open(WindowEvent event) {

				if (org.txm.utils.OSDetector.isFamilyUnix()) {
					event.browser = getBrowser();
					event.required = false;
				}
			}
		});

		getBrowser().addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseScrolled(MouseEvent e) {

				if ((e.stateMask & SWT.CTRL) == SWT.CTRL) {
					if (e.count > 0) {
						zoomIn(getBrowser());
					}
					else {
						zoomOut(getBrowser());
					}
				}
			}
		});

		ISelectionProvider selProvider = new ISelectionProvider() {

			@Override
			public void setSelection(ISelection selection) {
			}

			@Override
			public void removeSelectionChangedListener(ISelectionChangedListener listener) {
			}

			@Override
			public ISelection getSelection() {
				ITextSelection sel = new ITextSelection() {

					@Override
					public boolean isEmpty() {
						return false;
					}

					@Override
					public String getText() {
						return getTextSelection();
					}

					@Override
					public int getStartLine() {
						return 0;
					}

					@Override
					public int getOffset() {
						return 0;
					}

					@Override
					public int getLength() {
						return 0;
					}

					@Override
					public int getEndLine() {
						return 0;
					}
				};
				return sel;
			}

			@Override
			public void addSelectionChangedListener(ISelectionChangedListener listener) {
			}
		};
		getSite().registerContextMenu(menuManager, selProvider);
		getSite().setSelectionProvider(selProvider);

		initializeProgressListener();

		// System.out.println("Browser menu: "+getBrowser().getMenu());
	}

	static String SCRIPT01 = "var html = \"\";" + //$NON-NLS-1$
			"if (typeof window.getSelection != \"undefined\") {" + // modern Web browsers //$NON-NLS-1$
			"var sel = window.getSelection();" + //$NON-NLS-1$
			"if (sel.rangeCount) {" + //$NON-NLS-1$
			"var container = document.createElement(\"div\");" + //$NON-NLS-1$
			"for (var i = 0, len = sel.rangeCount; i < len; ++i) {" + //$NON-NLS-1$
			"container.appendChild(sel.getRangeAt(i).cloneContents());" + //$NON-NLS-1$
			"}" + //$NON-NLS-1$
			"html = container.innerHTML;" + //$NON-NLS-1$
			"}" + //$NON-NLS-1$
			"} else if (typeof document.selection != \"undefined\") {" + // for IE < 11 //$NON-NLS-1$
			"if (document.selection.type == \"Text\") {" + //$NON-NLS-1$
			"html = document.selection.createRange().htmlText;" + //$NON-NLS-1$
			"}" + //$NON-NLS-1$
			"}" + //$NON-NLS-1$
			"return html"; //$NON-NLS-1$

	// return the span@id of the current selection
	static String SCRIPT02 = "function getChildren(node) {\n" //$NON-NLS-1$
			+ "if (node.children != \"undefined\")\n" //$NON-NLS-1$
			+ "	return node.children;\n" //$NON-NLS-1$
			+ "else \n" //$NON-NLS-1$
			+ "return node.ChildNodes;\n" //$NON-NLS-1$
			+ "}\n" //$NON-NLS-1$
			+ "findSpans = function findSpans(c, a) {\n" + //$NON-NLS-1$
			"    for (var i = 0 ; i < c.length ; i++) {\n" + //$NON-NLS-1$
			"        if (c.item(i).tagName == \"SPAN\") {\n" + //$NON-NLS-1$
			"            var id = c.item(i).getAttribute(\"id\")\n" + //$NON-NLS-1$
			"            if (id.indexOf(\"w_\") == 0)\n" + //$NON-NLS-1$
			"                a.push(id);\n" + //$NON-NLS-1$
			"        } else if (c.item(i).nodeType == 1) {\n" + //$NON-NLS-1$
			"            findSpans(c.item(i).children, a);\n" + //$NON-NLS-1$
			"        }\n" + //$NON-NLS-1$
			"    }\n" + //$NON-NLS-1$
			"}\n" + //$NON-NLS-1$
			"    var all = [];" + //$NON-NLS-1$
			"    var sel = window.getSelection();" + //$NON-NLS-1$
			"    if (sel.rangeCount == 0){" + //$NON-NLS-1$
			"        return all;" + //$NON-NLS-1$
			"    }" + //$NON-NLS-1$
			"\n" + //$NON-NLS-1$
			"    var range = sel.getRangeAt(0);" + //$NON-NLS-1$
			"    var frag = range.cloneContents();alert(frag);alert(frag.children);" + //$NON-NLS-1$
			"    findSpans(frag.children, all);" + //$NON-NLS-1$
			"    return all;"; //$NON-NLS-1$

	public String getTextSelectionDOM() {
		return ((String) getBrowser().evaluate(SCRIPT01));
	}

	//	public Object getWordIDsSelection() {
	//		System.out.println("WORDS IDS " + getBrowser().evaluate(SCRIPT02));
	//		return getBrowser().evaluate(SCRIPT02);
	//	}

	public String getTextSelection() {
		//System.out.println("DOM=" + getTextSelectionDOM());
		String rez = getTextSelectionDOM().replaceAll("<[^>]+>", ""); //$NON-NLS-1$ //$NON-NLS-2$
		//System.out.println("STR=" + rez);
		return rez;
	}

	//	/**
	//	 * Gets the current page.
	//	 *
	//	 * @return the current page
	//	 */
	//	public Page getCurrentPage() {
	//		return currentPage;
	//	}
}
