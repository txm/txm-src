package org.txm.rcp.swt.widget.structures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.SubcorpusCQLQuery;
import org.txm.utils.logger.Log;

public class SimpleSubcorpusPanel extends Composite {

	public StructuralUnit selectedStructuralUnit;

	public StructuralUnitProperty selectedStructuralUnitProperty;

	public List<StructuralUnitProperty> selectableStructuralUnitProperties = null;

	Combo structuralUnitsCombo;

	Combo propertyCombo;

	public SuperListViewer valueCombo;

	List<StructuralUnit> structuralUnitsFinal;

	private Button validationButton;

	private CQPCorpus corpus;

	public SimpleSubcorpusPanel(Composite parent, int style, Button validationButton, CQPCorpus corpus) {
		super(parent, style);

		this.validationButton = validationButton;
		this.corpus = corpus;

		GridLayout layout = new GridLayout(2, false);
		this.setLayout(layout);

		Label structureLabel = new Label(this, SWT.NONE);
		structureLabel.setText(TXMUIMessages.ampStructureColon);
		structureLabel.setLayoutData(new GridData(GridData.END, GridData.CENTER, false, false));

		structuralUnitsCombo = new Combo(this, SWT.READ_ONLY);
		structuralUnitsCombo.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));

		Label propertyLabel = new Label(this, SWT.NONE);
		propertyLabel.setText(TXMUIMessages.ampPropertyColon);
		propertyLabel.setLayoutData(new GridData(GridData.END, GridData.CENTER, false, false));

		propertyCombo = new Combo(this, SWT.READ_ONLY);
		propertyCombo.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));

		Label valuesLabel = new Label(this, SWT.NONE);
		valuesLabel.setText(TXMUIMessages.ampValues);
		valuesLabel.setLayoutData(new GridData(GridData.END, GridData.CENTER, false, false));

		valueCombo = new SuperListViewer(this, SWT.MULTI | SWT.V_SCROLL | SWT.BORDER | SWT.VIRTUAL);
		GridData data = new GridData(GridData.FILL, GridData.FILL, true, false);
		data.heightHint = 100;
		valueCombo.setLayoutData(data);
		// Load StructuralUnit
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		List<StructuralUnit> structuralUnits = null;
		try {
			structuralUnits = corpus.getOrderedStructuralUnits();
		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.bind(TXMCoreMessages.error_errorP0, Log.toString(e)));
			return;
		}

		structuralUnitsFinal = new ArrayList<StructuralUnit>();


		for (StructuralUnit unit : structuralUnits) {
			if (unit.getOrderedProperties() != null
					&& unit.getOrderedProperties().size() != 0) {
				if (!"txmcorpus".equals(unit.getName())) { // keep all but txmcorpus //$NON-NLS-1$
					structuralUnitsFinal.add(unit);
					structuralUnitsCombo.add(unit.getName());
				}
			}
		}

		if (structuralUnitsCombo.getItemCount() == 0) {
			if (validationButton != null) {
				validationButton.setEnabled(false);
			}
		}
		else {
			String[] items = structuralUnitsCombo.getItems();
			structuralUnitsCombo.select(0);
			for (int i = 0; i < items.length; i++)
				if (items[i].equals("text")) { //$NON-NLS-1$
					structuralUnitsCombo.select(i);
					break;
				}

			selectedStructuralUnit = structuralUnitsFinal.get(0);
		}
		// Load StructuralUnitProperty
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		structuralUnitsCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent event) {
				System.out.println(TXMUIMessages.structuralUnitComboSelected);
				reloadProperties();
			}
		});

		/*
		 * for(StructuralUnitProperty property :
		 * structuralUnitsFinal.get(0).getProperties()) {
		 * propertyCombo.add(property.getName()); }
		 * propertyCombo.select(0); selectedStructuralUnitProperty =
		 * selectedStructuralUnit.getProperties().get(0);
		 */
		// Load Values
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		propertyCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent event) {
				reloadValues();
			}
		});

		//		valueCombo.getList().addSelectionListener(new SelectionListener() {
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				selectedValues = valueCombo.getSelection().toList();
		//			}
		//
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) { }
		//		});

		reloadProperties();
	}

	/**
	 * Reload values.
	 */
	protected void reloadValues() {
		List<String> values = null;
		selectedStructuralUnitProperty = selectableStructuralUnitProperties.get(propertyCombo.getSelectionIndex());
		//System.out.println("Selectable su prop: "+selectableStructuralUnitProperties); //$NON-NLS-1$
		//System.out.println("Selected line: "+propertyCombo.getSelectionIndex()); //$NON-NLS-1$
		//System.out.println("Combo values: "+Arrays.toString(propertyCombo.getItems())); //$NON-NLS-1$
		//System.out.println("Selected su prop: "+selectedStructuralUnitProperty); //$NON-NLS-1$
		try {
			values = selectedStructuralUnitProperty.getOrderedValues(corpus);
			Collections.sort(values);
		}
		catch (CqiClientException e) {
			Log.severe(TXMCoreMessages.bind(TXMCoreMessages.error_errorP0, Log.toString(e)));
			return;
		}


		valueCombo.setInput(values);

		if (valueCombo.getList().getItemCount() > 0) {
			//valueCombo.getList().select(0);
		}
		GridData data = new GridData(GridData.FILL_BOTH);
		data.heightHint = 10 * valueCombo.getList().getItemHeight();
		valueCombo.setLayoutData(data);
		valueCombo.getParent().layout();
	}

	/**
	 * Reload properties.
	 */
	protected void reloadProperties() {

		selectedStructuralUnit = structuralUnitsFinal.get(structuralUnitsCombo.getSelectionIndex());

		selectableStructuralUnitProperties = new ArrayList<StructuralUnitProperty>(selectedStructuralUnit.getOrderedProperties());
		for (int i = 0; i < selectableStructuralUnitProperties.size(); i++) {
			StructuralUnitProperty property = selectableStructuralUnitProperties.get(i);
			if ("text_base".equals(property.getFullName()) || //$NON-NLS-1$
					"text_project".equals(property.getFullName())) //$NON-NLS-1$
				selectableStructuralUnitProperties.remove(i);
		}
		propertyCombo.removeAll();
		for (StructuralUnitProperty property : selectableStructuralUnitProperties) {
			propertyCombo.add(property.getName());
		}

		if (selectedStructuralUnit.getName().equals("text")) { //$NON-NLS-1$
			String[] items = propertyCombo.getItems();
			for (int i = 0; i < items.length; i++) {
				if (items[i].equals("id")) { //$NON-NLS-1$
					propertyCombo.select(i);
					break;
				}
			}
		}
		else {
			propertyCombo.select(0);
		}
		reloadValues();
	}

	public String getQueryString() {
		String regexp = ""; //$NON-NLS-1$

		List<String> selectedValues = valueCombo.getSelection().toList();

		for (String v : selectedValues) {
			regexp += CQLQuery.addBackSlash(v) + "|"; //$NON-NLS-1$
		}
		regexp = regexp.substring(0, regexp.length() - 1);

		SubcorpusCQLQuery query = new SubcorpusCQLQuery(this.selectedStructuralUnit, this.selectedStructuralUnitProperty, regexp, selectedValues);

		return query.getQueryString();
	}

	public String getGeneratedName() {

		return structuralUnitsCombo.getText() + "@" + propertyCombo.getText() + "=" + StringUtils.join(valueCombo.getList().getSelection(), ", ");
	}

}
