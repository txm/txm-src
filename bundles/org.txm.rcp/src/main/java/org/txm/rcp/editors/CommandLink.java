package org.txm.rcp.editors;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.IParameter;
import org.eclipse.core.commands.Parameterization;
import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.BrowserFunction;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.handlers.IHandlerService;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.logger.Log;

import cern.colt.Arrays;

public class CommandLink extends BrowserFunction {

	String data = ""; //$NON-NLS-1$

	Browser browser;

	Object ret;

	IEditorPart editor;

	public static final String FCT = "txmcommand"; //$NON-NLS-1$

	public CommandLink(IEditorPart editor, Browser browser) {
		super(browser, FCT); //$NON-NLS-1$
		this.browser = browser;
		this.editor = editor;
	}

	@Override
	public synchronized Object function(Object[] arguments) {
		//System.out.println("CALL: "+Arrays.toString(arguments));
		Log.finer("txmcommand() called: " + Arrays.toString(arguments)); //$NON-NLS-1$
		if (editor != null) {
			try { // TODO add option (or another browser command) to not force activated editor
				editor.getSite().getPage().activate(editor);
			}
			catch (Exception e) {
				Log.printStackTrace(e);
			}
		}
		ret = null;
		browser.getDisplay().asyncExec(new Runnable() {

			@Override
			public void run() {
				ret = callTXMCommand(arguments);
			}
		});
		return ret;
	}

	/**
	 * 
	 * @param arguments array of command id + (parameters+values).
	 * 
	 *            <pre>
	 * ex: String[] arguments = {"commandid", "paramname", "value", "paramname2", "othervalue"};
	 *            </pre>
	 * 
	 * @return
	 */
	public static Object callTXMCommand(Object[] arguments) {
		HashMap<String, String> params = new HashMap<>();
		for (int i = 0; i + 1 < arguments.length; i += 2) {
			params.put(arguments[i].toString(), arguments[i + 1].toString());
		}

		Log.finer("txmcommand parameters: " + params); //$NON-NLS-1$
		String id = params.get("id"); //$NON-NLS-1$
		if (id != null) {
			params.remove("id"); //$NON-NLS-1$
			// System.out.println("CALLING CMD with id="+id+" and parameters="+params);
			// get the command from plugin.xml
			return callTXMCommand(id, params);
		}
		else {
			Log.warning(NLS.bind(TXMCoreMessages.CannotCallATXMCommandWithoutIDParametersP0, params));
		}
		return arguments;
	}

	/**
	 * Browser command that opens an edition
	 * 
	 * @author mdecorde
	 *
	 */
	public static Object callTXMCommand(String id, HashMap<String, String> params) {
		Log.finer(NLS.bind(TXMCoreMessages.CallingTheP0CommandWithTheP1Parameters, id, params));

		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		ICommandService cmdService = window.getService(ICommandService.class);
		Command cmd = cmdService.getCommand(id);

		// get the parameter
		ArrayList<Parameterization> parameters = new ArrayList<>();
		ArrayList<String> failedParameters = new ArrayList<>();
		for (String k : params.keySet()) {
			try {
				IParameter iparam = cmd.getParameter(k);
				Parameterization p = new Parameterization(iparam, params.get(k));
				parameters.add(p);
			}
			catch (NotDefinedException e) {
				// Log.warning(NLS.bind(EditionUIMessages.warningColonUnknownedParameterIdEqualsP0, k));
				failedParameters.add(k);
			}
		}

		// build the parameterized command
		ParameterizedCommand pc = new ParameterizedCommand(cmd, parameters.toArray(new Parameterization[parameters.size()]));

		// execute the command
		IHandlerService handlerService = window.getService(IHandlerService.class);
		try {
			return handlerService.executeCommand(pc, null);
		}
		catch (Exception e) {
			Log.warning(TXMCoreMessages.bind(TXMCoreMessages.FailedToCallTheP0HyperlinkedCommandWithP1AndTheP2Parameters, id, parameters, failedParameters));
			// Log.printStackTrace(e);
		}
		return null;
	}
}
