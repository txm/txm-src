package org.txm.rcp.commands.workspace;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.Toolbox;
import org.txm.importer.Convert5To6;
import org.txm.objects.Project;
import org.txm.rcp.commands.RestartTXM;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.JobHandler;
import org.txm.utils.DeleteDir;

public class ConvertCorpus extends AbstractHandler {

	JobHandler jobhandler;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event)
				.getShell();
		DirectoryDialog dialog = new DirectoryDialog(shell);

		dialog.setFilterPath(Toolbox.getTxmHomePath());
		String path = dialog.open();

		final File corpora = new File(Toolbox.getTxmHomePath(),
				"corpora"); //$NON-NLS-1$
		final File indir = new File(path);
		final File outdir = new File(corpora, indir.getName() + 6);
		DeleteDir.deleteDirectory(outdir);

		jobhandler = new JobHandler(TXMUIMessages.ConvertingEtc) {

			@Override
			protected IStatus _run(SubMonitor monitor) throws Throwable {

				try {
					Convert5To6 converter = new Convert5To6(indir, outdir);
					this.acquireSemaphore();
					converter.start();
					this.releaseSemaphore();

					Project base = LoadBinaryCorpus.loadBinaryCorpusAsDirectory(outdir, monitor, false);
					if (base == null) {
						System.out.println(TXMUIMessages.corpusConversionFailed);
						return Status.CANCEL_STATUS;
					}

					this.acquireSemaphore();
					//						org.txm.Toolbox.restartWorkspace(monitor);
					//						Toolbox.getEngineManager(EngineType.SEARCH).restartEngines();
					this.releaseSemaphore();

					syncExec(new Runnable() {

						@Override
						public void run() {
							RestartTXM.reloadViews();
						}
					});

				}
				catch (Throwable e) {
					System.out.println(NLS.bind(TXMUIMessages.errorWhileRunningScriptColonP0, e));
					throw e;
				}

				return Status.OK_STATUS;
			}
		};
		jobhandler.startJob();
		return event;
	}
}
