package org.txm.rcp.swt.widget.parameters;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Text;
import org.kohsuke.args4j.NamedOptionDef;

public class FolderField extends LabelField {

	Text t;

	public FolderField(Composite parent, int style, NamedOptionDef parameter) {
		super(parent, style, parameter);

		this.setLayout(new GridLayout(3, false)); // has 3 cells

		t = new Text(this, SWT.SINGLE | SWT.BORDER);
		t.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));
		resetToDefault();

		Button b = new Button(this, SWT.PUSH);
		b.setText("..."); //$NON-NLS-1$
		b.setToolTipText("Select a directory"); //$NON-NLS-1$
		b.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				String value = null;
				DirectoryDialog d = new DirectoryDialog(FolderField.this.getShell());
				d.setFilterPath(t.getText());
				value = d.open();
				if (value != null) t.setText(value);
			}
		});
	}

	public File getWidgetValue() {
		String value = t.getText();
		if (value != null && value.trim().length() > 0) {
			return new File(value.trim());
		}
		return null;
	}


	public void setDefault(Object text) {
		t.setText("" + text); //$NON-NLS-1$
	}

	@Override
	public void resetToDefault() {
		t.setText(getWidgetDefault());
	}
}
