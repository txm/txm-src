package org.txm.rcp.swt.dialog;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.txm.rcp.messages.TXMUIMessages;

/**
 * A confirm Dialog with a check box to not ask again
 * 
 * @author mdecorde
 *
 */
public class ConfirmDialog extends MessageDialog {

	public final static String[] dialogButtonLabels = { IDialogConstants.OK_LABEL,
			IDialogConstants.CANCEL_LABEL };

	public final static String TRUE = "true"; //$NON-NLS-1$

	public final static String ROOT = "org.txm.confirmdialog.history."; //$NON-NLS-1$

	protected String id;

	public ConfirmDialog(Shell parentShell, String id, String dialogTitle, String dialogMessage) {
		super(parentShell, dialogTitle, null, dialogMessage,
				MessageDialog.QUESTION_WITH_CANCEL, dialogButtonLabels, 1);
		this.id = id;
	}

	protected Button checkDefault;

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		checkDefault = new Button(container, SWT.CHECK);
		checkDefault.setText(TXMUIMessages.DontAskAgain);
		boolean b = TRUE.equals(System.getProperty(ROOT + id));
		checkDefault.setSelection(b);

		checkDefault.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean b = checkDefault.getSelection();
				System.setProperty(ROOT + id, Boolean.toString(b));
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		return container;
	}

	@Override
	public int open() {
		boolean b = TRUE.equals(System.getProperty(ROOT + id));
		if (b) return 0; // ok

		return super.open();
	}
}
