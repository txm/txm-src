package org.txm.rcp.swt.toolbar;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.txm.rcp.IImageKeys;

/**
 * A ToolItem SWT.CHECK with 2 images for the selected and not selected states
 *
 * @author mdecorde
 *
 */
public class OpenCloseToolItem extends ToolItem {


	/**
	 *
	 * @param parent
	 * @param title
	 * @param openIconFilePath
	 * @param closeIconFilePath
	 */
	public OpenCloseToolItem(ToolBar parent, String title, String openIconFilePath, String closeIconFilePath) {
		this(parent, title, openIconFilePath, closeIconFilePath, null);
	}

	/**
	 *
	 * @param parent
	 * @param title used as Text if openIconFilePath icon is not set, and a ToolTip text if openIconFilePath icon is set
	 * @param openIconFilePath
	 * @param closeIconFilePath
	 */
	public OpenCloseToolItem(ToolBar parent, String title, String openIconFilePath, String closeIconFilePath, String buttonTooltip) {
		super(parent, SWT.CHECK);

		if (openIconFilePath != null) {
			openIcon = IImageKeys.getImage(openIconFilePath);
			this.setImage(openIcon);
			if (closeIconFilePath != null) {
				closeIcon = IImageKeys.getImage(closeIconFilePath);
			}
		}
		else {
			this.setText(title);
		}

		if (buttonTooltip != null) {
			this.setToolTipText(buttonTooltip);
		}
		else {
			this.setToolTipText(title);
		}

		this.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (closeIcon != null) {
					if (getSelection()) {
						setImage(closeIcon);
					}
					else {
						setImage(openIcon);
					}
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}

	@Override
	public void checkSubclass() {
	}

	protected Image openIcon;

	protected Image closeIcon;
}
