package org.txm.rcp.swt.provider;

import java.util.HashMap;

import org.eclipse.jface.viewers.ColumnLabelProvider;

public class HashMapLabelProvider extends ColumnLabelProvider {

	String key;

	public HashMapLabelProvider(String key) {
		this.key = key;
	}

	@Override
	public String getText(Object element) {
		if (element != null && element instanceof HashMap) {
			return "" + ((HashMap<?, ?>) element).get(key); //$NON-NLS-1$
		}
		return "#not list"; //$NON-NLS-1$
	}
}
