package org.txm.rcp.editors;



import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;


public class TestEditor extends EditorPart {

	public static final String ID = "TestEMEditor"; //$NON-NLS-1$

	private Button btnParameters;

	public TestEditor() {
	}

	/**
	 * Create contents of the editor part.
	 * 
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		parent.setLayout(null);

		btnParameters = new Button(parent, SWT.NONE);
		btnParameters.setBounds(0, 0, 22, 22);
		//btnParameters.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		btnParameters.setImage(SWTResourceManager.getImage(TestEditor.class, "/org/eclipse/jface/dialogs/images/popup_menu.gif")); //$NON-NLS-1$

		Composite composite = new Composite(parent, SWT.BORDER);
		Rectangle rect = parent.getBounds();
		composite.setBounds(0, 0, rect.width, rect.height);
	}

	@Override
	public void setFocus() {
		// Set the focus
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		// Do the Save operation
	}

	@Override
	public void doSaveAs() {
		// Do the Save As operation
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setSite(site);
		setInput(input);
	}

	@Override
	public boolean isDirty() {
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}
}
