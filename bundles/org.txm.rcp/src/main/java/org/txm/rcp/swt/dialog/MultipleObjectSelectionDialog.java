// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.dialog;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.txm.rcp.messages.TXMUIMessages;

/**
 * The dialog box called to select the view properties.
 * 
 * @author mdecorde
 */
public class MultipleObjectSelectionDialog<P extends Object> extends Dialog {

	/** The available properties. */
	final private List<P> availableProperties;

	/** The selected properties. */
	final private List<P> selectedProperties;

	/** The maxprops. */
	int maxprops = -1;

	/** The available properties view. */
	protected ListViewer availablePropertiesView;

	private ArrayList<P> cancelSelectedProperties;

	private ArrayList<P> cancelAvailableProperties;

	Control sourceWidget;

	protected LabelProvider labelProvider;

	// prevent the deactivate listener from calling cancelPressed
	private boolean closed = false;

	/**
	 * Instantiates a new view property selection dialog.
	 *
	 * @param parentShell the parent shell
	 * @param availableProperties the available properties
	 * @param selectedProperties the selected properties
	 */
	public MultipleObjectSelectionDialog(IShellProvider parentShell,
			List<P> availableProperties,
			List<P> selectedProperties) {
		this(parentShell.getShell(), availableProperties, selectedProperties);
	}

	/**
	 * Instantiates a new view property selection dialog.
	 *
	 * @param shell the shell
	 * @param availableProperties the available properties
	 * @param selectedProperties the selected properties
	 */
	public MultipleObjectSelectionDialog(Shell shell,
			List<P> availableProperties,
			List<P> selectedProperties) {
		this(shell, availableProperties, selectedProperties, -1, null, null);
	}

	/**
	 * Instantiates a new view property selection dialog.
	 *
	 * @param shell the shell
	 * @param availableProperties the available properties
	 * @param selectedProperties the selected properties
	 * @param maxprops the maxprops
	 * @param sourceWidget null or the widget to anchor the dialog
	 * @param labelProvider set a label provider if the toString method of the elements to display is not OK
	 */
	public MultipleObjectSelectionDialog(Shell shell,
			List<P> availableProperties,
			List<P> selectedProperties, int maxprops, Control sourceWidget, LabelProvider labelProvider) {
		super(shell);

		this.availableProperties = new ArrayList<>(availableProperties);
		this.selectedProperties = new ArrayList<>(selectedProperties);
		
		int i = 0;
		for (P p : this.selectedProperties) { // move selected items at the beginning
			this.availableProperties.remove(p);
			this.availableProperties.add(i++, p);
		}
		
		this.cancelAvailableProperties = new ArrayList<>(availableProperties);
		this.cancelSelectedProperties = new ArrayList<>(selectedProperties);
		this.labelProvider = labelProvider;
		
		if (sourceWidget != null) {
			this.setShellStyle(SWT.NONE);
		}
		else {
			this.setShellStyle(this.getShellStyle() | SWT.APPLICATION_MODAL | SWT.RESIZE);
		}
		this.maxprops = maxprops;
		this.sourceWidget = sourceWidget;
	}

	/**
	 * Gets the selected properties.
	 *
	 * @return the selected properties
	 */
	public List<P> getSelection() {
		return selectedProperties;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(TXMUIMessages.select);
		newShell.setMinimumSize(300, 200);
		if (sourceWidget != null) {
			Point m = newShell.getDisplay().map(sourceWidget, null, 0, sourceWidget.getSize().y);
			newShell.setLocation(m);
		}

		newShell.addListener(SWT.Deactivate, new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				if (!closed) cancelPressed();
			}
		});
	}

	@Override
	protected Point getInitialLocation(Point initialSize) {
		if (sourceWidget != null) {
			return sourceWidget.getDisplay().map(sourceWidget, null, 0, sourceWidget.getSize().y);
		}
		return super.getInitialLocation(initialSize);
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		// parent.setLayout(new FormLayout());
		Composite mainArea = new Composite(parent, SWT.NONE);
		mainArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		// 4 columns
		GridLayout layout = new GridLayout(4, false);
		mainArea.setLayout(layout);

		if (maxprops != 1) {
			availablePropertiesView = new ListViewer(mainArea, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		} else {
			availablePropertiesView = new ListViewer(mainArea, SWT.BORDER | SWT.V_SCROLL);
		}
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.heightHint = 500;
		availablePropertiesView.getList().setLayoutData(gridData);
		if (labelProvider != null) {
			availablePropertiesView.setLabelProvider(labelProvider);
		}
		availablePropertiesView.setContentProvider(new ArrayContentProvider());
		availablePropertiesView.getList().setToolTipText("Select one or several entries. Several entries can be selected with the CTRL or SHIFT pressed");

		availablePropertiesView.getList().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				okPressed();
			}
		});
		
		if (maxprops != 1) {
			Composite orderButtons = new Composite(mainArea, SWT.NONE);
			orderButtons.setLayout(new GridLayout(1, false));
			Button up = new Button(orderButtons, SWT.ARROW | SWT.UP);
			up.setToolTipText("Move up the selected entry");
			Button down = new Button(orderButtons, SWT.ARROW | SWT.DOWN);
			down.setToolTipText("Move down the selected entry");

			up.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					int index = availablePropertiesView.getList().getSelectionIndex();
					if (index > 0) {
						P selectedP = availableProperties.get(index);
						P upperP = availableProperties.get(index - 1);

						availableProperties.set(index, upperP);
						availableProperties.set(index - 1, selectedP);

						availablePropertiesView.refresh();
						//					availablePropertiesView.getList().setSelection(index - 1);
					}
				}
			});

			down.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					int index = availablePropertiesView.getList().getSelectionIndex();
					if (index < availableProperties.size() - 1 && index >= 0) {
						P selectedP = availableProperties.get(index);
						P bellowP = availableProperties.get(index + 1);

						availableProperties.set(index, bellowP);
						availableProperties.set(index + 1, selectedP);

						availablePropertiesView.refresh();
					}
				}
			});
		}
		reload();
		return mainArea;
	}

	/**
	 * Reload.
	 */
	public void reload() {

		if (availableProperties == null) {
			System.out.println(TXMUIMessages.ErrorTheAvailablePropertiesListIsNULL);
			return;
		}

		availablePropertiesView.setInput(availableProperties);
		if (selectedProperties != null) {
			availablePropertiesView.setSelection(new StructuredSelection(selectedProperties));
		}
		availablePropertiesView.refresh();
		availablePropertiesView.getList().getParent().layout();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {

		this.selectedProperties.clear();

		List<P> sel = availablePropertiesView.getStructuredSelection().toList();

		selectedProperties.addAll(sel);

		if (selectedProperties.size() != 0) {
			closed = true;
			super.okPressed();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void cancelPressed() {
		this.selectedProperties.clear();
		this.selectedProperties.addAll(cancelSelectedProperties);

		super.cancelPressed();
	}
}
