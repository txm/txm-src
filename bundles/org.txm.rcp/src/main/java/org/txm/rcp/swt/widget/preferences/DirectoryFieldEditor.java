package org.txm.rcp.swt.widget.preferences;

import org.eclipse.swt.widgets.Composite;

public class DirectoryFieldEditor extends org.eclipse.jface.preference.DirectoryFieldEditor {
	
	Composite parent;
	
	public DirectoryFieldEditor(String name, String label, Composite parent) {
		super(name, label, parent);
		this.parent = parent;
	}

	public void setToolTipText(String tooltip) {
		getTextControl(parent).setToolTipText(tooltip);
		getLabelControl(parent).setToolTipText(tooltip);
	}
	
	public void setEnabled(boolean b) {
		
		super.setEnabled(b, parent);
	}
}
