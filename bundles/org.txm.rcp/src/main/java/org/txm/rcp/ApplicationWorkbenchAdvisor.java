package org.txm.rcp;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.preferences.BundleDefaultsScope;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.e4.ui.css.swt.theme.IThemeEngine;
import org.eclipse.e4.ui.css.swt.theme.IThemeManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.preference.IPreferenceNode;
import org.eclipse.jface.preference.PreferenceManager;
import org.eclipse.jface.util.Util;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPreferenceConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.IWorkbenchConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IOConsole;
import org.eclipse.ui.console.IOConsoleInputStream;
import org.eclipse.ui.console.IOConsoleOutputStream;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.internal.IPreferenceConstants;
import org.eclipse.ui.internal.WorkbenchPlugin;
import org.eclipse.ui.internal.dialogs.WorkbenchPreferenceNode;
import org.eclipse.ui.model.ContributionComparator;
import org.eclipse.ui.model.IContributionService;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.Version;
import org.txm.Toolbox;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.TXMResult;
import org.txm.objects.Project;
import org.txm.rcp.commands.OpenWelcomePage;
import org.txm.rcp.commands.RecoverCorporaFromInstalls;
import org.txm.rcp.commands.RestartTXM;
import org.txm.rcp.handlers.scripts.ExecuteGroovyScript;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.p2.plugins.TXMUpdateHandler;
import org.txm.rcp.perspective.TXMPerspective;
import org.txm.rcp.preferences.RCPPreferences;
import org.txm.rcp.swt.dialog.ImageOKCancelDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.utils.BundleUtils;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

public class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {

	private boolean installPreferenceRestored;

	private IOConsoleOutputStream stream;

	private IOConsoleInputStream streamInput;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.application.WorkbenchAdvisor#createWorkbenchWindowAdvisor(org.eclipse.ui.application.IWorkbenchWindowConfigurer)
	 */
	@Override
	public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
		return new ApplicationWorkbenchWindowAdvisor(configurer);
	}

	@Override
	public IAdaptable getDefaultPageInput() {
		// MD test 02022023
		//org.eclipse.core.resources.IWorkspace workspace = org.eclipse.core.resources.ResourcesPlugin.getWorkspace();
		return null;
	}

	/**
	 * initialization code done after the splash screen.
	 *
	 * @param configurer the RCP configurer
	 */
	@Override
	public void initialize(IWorkbenchConfigurer configurer) {
		org.eclipse.ui.ide.IDE.registerAdapters();

		PlatformUI.getPreferenceStore().setValue(IWorkbenchPreferenceConstants.SHOW_PROGRESS_ON_STARTUP, true);

		InitializeLogger();

		configurer.setSaveAndRestore(true);
	}

	@Override
	public void preStartup() {

	}

	@Override
	public ContributionComparator getComparatorFor(String contributionType) {
		if (contributionType.equals(IContributionService.TYPE_PREFERENCE)) {
			return new PreferencesComparator();
		}
		else {
			return super.getComparatorFor(contributionType);
		}
	}
	
	/**
	 * Set Eclipse Theme depending on the OS
	 */
	@SuppressWarnings("rawtypes")
	private void setDefaultTheme() {
		Bundle b = FrameworkUtil.getBundle(getClass());
		BundleContext context = b.getBundleContext();

		ServiceReference serviceRef = context.getServiceReference(IThemeManager.class.getName());
		@SuppressWarnings("unchecked")
		IThemeManager themeManager = (IThemeManager) context.getService(serviceRef);
		IThemeEngine engine = themeManager.getEngineForDisplay(Display.getDefault());

		String theme = "org.txm.rcp.theme.default.linux"; //$NON-NLS-1$

		if (Util.isWindows()) {
			theme = "org.txm.rcp.theme.default.windows"; //$NON-NLS-1$
		}
		else if (Util.isMac()) {
			theme = "org.txm.rcp.theme.default.mac"; //$NON-NLS-1$
		}

		engine.setTheme(theme, true);
	}

	File lockFile;

	/**
	 * initialization code done after the splash screen call a job which does the
	 * post installation the TXMHOME, checks the TreeTagger and finally
	 * starts the Toolbox.
	 */
	@Override
	public void postStartup() {

		try {

			SWTEditorsUtils.cleanAreaModel();

			// create the Toolbox initialization job
			JobHandler jobhandler = new JobHandler(TXMUIMessages.initializationOfThePlatform, true) {

				@Override
				protected IStatus _run(final SubMonitor monitor) throws Exception {

					try {
						monitor.beginTask(TXMUIMessages.startingTxm, 100);

						monitor.setTaskName(TXMUIMessages.loadingTXMInstallPreferencesP0);
						Log.fine(TXMUIMessages.startJobColonLoadInstallPreferences);
						if (!checkInstallDirectory(this)) {
							monitor.done();
							Log.severe(TXMUIMessages.startupFailedPleaseCheckTXMPreferencesInTheFileSupPreferencesMenuOrContactTxmusersMailingListMenuColonHelpSup);
							return Status.CANCEL_STATUS;
						}
						monitor.worked(10);

						monitor.setTaskName(TXMUIMessages.checkingForExistingWorkspaceDirectory);
						Log.warning(TXMUIMessages.startJobColonCheckTXMHOME);
						if (!checkTXMHOME(this, monitor)) {
							monitor.done();
							System.err.println(TXMUIMessages.errorsOccuredDuringTXMPostInstallation);
							return Status.CANCEL_STATUS;
						}
						monitor.worked(30);

						// INITIALIZE TBX AND UI
						monitor.setTaskName(TXMUIMessages.initializingPlatform);
						Log.warning(TXMUIMessages.startJobColonInitializeUI);
						if (!initializeUI(this)) {
							monitor.done();
							return Status.CANCEL_STATUS;
						}

						// CREATE THE CLOSE CONTROL FILE
//						System.out.println("Platform.getIsntanceLocaltion: "+Platform.getInstanceLocation());
//						System.out.println("Platform.getIsntanceLocaltion get URL: "+Platform.getInstanceLocation().getURL());
//						System.out.println("Platform.getIsntanceLocaltion get URL TO URI: "+Platform.getInstanceLocation().getURL().to);
//						System.out.println("Platform.getIsntanceLocaltion get URL TO URI: "+Platform.getInstanceLocation().getURL().toURI());
//						System.out.println("Platform.getIsntanceLocaltion get URL TO URI get PATH: "+Platform.getInstanceLocation().getURL().toURI().getPath());
						String rootPath = Platform.getInstanceLocation().getURL().getFile();
						lockFile = new File(rootPath, "txm_was_not_closed_correctly.lock"); //$NON-NLS-1$
						Log.fine("Lock file path: " + lockFile); //$NON-NLS-1$
						if (lockFile.exists()) { // the close control file was not deleted //$NON-NLS-1$
							lockFile.delete(); //$NON-NLS-1$

							File checkupResultFile = new File(rootPath, "txm_startup_diagnostic.txt"); //$NON-NLS-1$ //$NON-NLS-2$
							checkupResultFile.delete();
							try {
								Toolbox.writeStartupCorporaDiagnostics(checkupResultFile);
							}
							catch (Exception e) {
								e.printStackTrace();
							}

							if (checkupResultFile.exists()) { // ask the user only if errors have been detected 
								this.syncExec(new Runnable() {

									@Override
									public void run() {
										int choice = MessageDialog.open(MessageDialog.QUESTION_WITH_CANCEL, Display.getDefault().getActiveShell(),
												TXMUIMessages.warning,
												TXMUIMessages.TXMWasNotCorrectlyStopped + "\n" + NLS.bind(TXMUIMessages.DoYouWantToStartADiagnosticOfYourCorporaEtcP0FileEtc, checkupResultFile) //$NON-NLS-1$
												, SWT.NONE, new String[] { TXMUIMessages.Cancel, TXMUIMessages.OK, TXMUIMessages.OKOpenTheDiagnosticFile });

										if (choice != 0) {

										}

										if (choice == 2) {
											org.txm.rcp.handlers.files.EditFile.openfile(checkupResultFile);
										}
									}
								});
							}
						}
						lockFile.createNewFile(); //$NON-NLS-1$

						String s = getVersionLevel().trim();
						if (s.length() == 0) { //$NON-NLS-1$
							Log.info(TXMUIMessages.info_txmIsReady);
						}
						else {
							String s2 = TXMUIMessages.youAreUsingAnAlphaBetaVersionOfTXMItIsNotRecommendedToUseItForYourWork;

							Log.warning(s2);

							if (Application.isHeadLess()) {

							}
							else if (org.eclipse.core.runtime.Platform.inDevelopmentMode()) {

							}
							else if (RCPPreferences.getInstance().getBoolean(RCPPreferences.EXPERT_USER)) {

							}
							else {

								Display.getDefault().syncExec(new Runnable() {

									@Override
									public void run() {
										//MessageDialog dialog = new MessageDialog(, MessageDialog.WARNING, 0);
										ImageOKCancelDialog dialog = new ImageOKCancelDialog(Display.getDefault().getActiveShell(), TXMUIMessages.warning, IImageKeys.getImage(IImageKeys.WORKSITE),
												s2);
										if (dialog.open() == ImageOKCancelDialog.CANCEL) {
											System.exit(0);
										}
										//MessageDialog.openWarning(Display.getDefault().getActiveShell(), "Warning", s2);
									}
								});
							}
						}

						monitor.done();
					}
					catch (Exception e) {
						System.out.println(TXMCoreMessages.bind(TXMUIMessages.errorWhileLoadingTXMP0, e));
						throw e;
					}
					catch (ThreadDeath td) {
						System.out.println(TXMUIMessages.TXMInitializationHasBeenCanceledByUserDotPleaseRestartTXM);
						throw td;
					}

					return Status.OK_STATUS;
				}
			};
			jobhandler.startJob();

			// start update NOT IN A JOB
			startUpdateLookUpJob(false);

		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.bind(TXMUIMessages.startupFailedPleaseCheckTXMPreferencesInTheFileSupPreferencesMenuOrContactTxmusersMailingListMenuColonHelpSup, e.getMessage()));
			System.out.println(TXMCoreMessages.bind(TXMUIMessages.startupFailedPleaseCheckTXMPreferencesInTheFileSupPreferencesMenuOrContactTxmusersMailingListMenuColonHelpSup, e.getMessage()));
			Log.printStackTrace(e);
		}
	}

	public static void closeEmptyEditors(boolean closeOthersEditors) {
		Log.fine("Closing empty editors..."); //$NON-NLS-1$
		String REF = "org.eclipse.ui.internal.emptyEditorTab"; //$NON-NLS-1$

		for (final IWorkbenchPage page : SWTEditorsUtils.getPages()) {
			ArrayList<IEditorReference> editorRefsList = new ArrayList<>();
			for (IEditorReference editorref : page.getEditorReferences()) {
				if (closeOthersEditors || editorref.getId().equals(REF)) {
					Log.fine(TXMUIMessages.closingColon + editorref.getId());
					editorRefsList.add(editorref);
				}
			}

			final IEditorReference[] editorRefs = editorRefsList.toArray(new IEditorReference[editorRefsList.size()]);
			Display.getDefault().syncExec(new Runnable() {

				@Override
				public void run() {
					page.closeEditors(editorRefs, false);
				}
			});
		}
	}

	public static String getTXMStartMessage() {

		String pretty_version = getTXMVersionPrettyString();
		return NLS.bind(TXMUIMessages.startingUpP0P1, pretty_version, System.getProperty("os.name") + ", " + System.getProperty("os.version") + ", " + System.getProperty("os.arch")); // os.name, os.version, os.arch


	}

	public static String getTXMVersion() {
		Version version = Activator.getDefault().getBundle().getVersion();
		return version.getMajor() + "." + version.getMinor() + "." + version.getMicro(); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public static String getTXMVersionPrettyString() {

		Version version = getBuildVersion();
		String build_date = getBuildDate(version);

		String txm_date = getUpdateDate();
		version = Activator.getDefault().getBundle().getVersion();

		if (txm_date.equals(build_date)) {
			return (NLS.bind(TXMUIMessages.prettyVersionP0P1,
					version.getMajor() + "." + version.getMinor() + "." + version.getMicro(), build_date)); //$NON-NLS-1$ //$NON-NLS-2$
		}
		else {
			return (NLS.bind(TXMUIMessages.prettyVersionP0P1,
					version.getMajor() + "." + version.getMinor() + "." + version.getMicro(), "install " + build_date + " - update " + txm_date)); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		}
	}

	public static Version getBuildVersion() {
		String version = null;

		// try finding build date from the p2 artifacts file in installation directory
		String path = BundleUtils.getInstallDirectory();
		if (path != null) {
			File installDirectory = new File(path);
			// println "installDirectory=$installDirectory"
			if (installDirectory.exists()) {
				File artifacts = new File(installDirectory, "p2/org.eclipse.equinox.p2.core/cache/artifacts.xml"); //$NON-NLS-1$
				// println "artifacts=$artifacts"
				if (artifacts.exists() && artifacts.canRead()) {
					try {
						String content = IOUtils.getText(artifacts);
						String ARTIFACT = "<artifact classifier='binary' id='org.txm.rcp.app_root"; //$NON-NLS-1$
						int start = content.indexOf(ARTIFACT);
						// println "start=$start"
						if (start > 0) {
							String VERSION = "version='"; //$NON-NLS-1$
							int versionStart = content.indexOf(VERSION, start + ARTIFACT.length());
							// println "versionStart=$versionStart"
							if (versionStart > 0) {
								int versionEnd = content.indexOf("'", versionStart + VERSION.length()); //$NON-NLS-1$
								if (versionEnd > 0) {
									version = content.substring(versionStart + VERSION.length(), versionEnd);
									// System.out.println("VERSION="+version);
								}
							}
						}
					}
					catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

		if (version == null) {
			version = Activator.getDefault().getBundle().getVersion().toString();
		}

		Version v2 = new Version(version);
		return v2;
	}

	public static String getBuildDate(Version version) {
		return version.getQualifier().replaceAll("(....)(..)(..)(..)(..)", "$1-$2-$3 $4h$5"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public static String getUpdateDate() {
		Version version = Activator.getDefault().getBundle().getVersion();
		return version.getQualifier().replaceAll("(....)(..)(..)(..)(..)", "$1-$2-$3 $4h$5"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	private void setPreferencesConfiguration() {
		PreferenceManager pm = PlatformUI.getWorkbench().getPreferenceManager();

		// Remove RCP Update preference page
		if (!TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
			Log.fine(TXMUIMessages.removeUnusedEclipseRCPPreferencePage);
			pm.remove("org.eclipse.equinox.internal.p2.ui.sdk.ProvisioningPreferencePage"); //$NON-NLS-1$
			pm.remove("org.eclipse.ui.internal.console.ansi.preferences.AnsiConsolePreferencePage"); //$NON-NLS-1$
			
			// Remove the Team menu
			pm.remove("org.eclipse.team.ui.TeamPreferences"); //$NON-NLS-1$

			// XML
			// pm.remove("org.eclipse.wst.xml.ui.preferences.xml"); //$NON-NLS-1$

			// Validation
			// pm.remove("ValidationPreferencePage"); //$NON-NLS-1$

			// Debug
			// pm.remove("org.eclipse.debug.ui.DebugPreferencePage"); //$NON-NLS-1$
		}
		// Remove RCP Web browser preference page
		IPreferenceNode[] arr = pm.getRootSubNodes();
		for (IPreferenceNode pn : arr) {
			// System.out.println("Label:" + pn.getLabelText() + " ID:" + pn.getId()+" Type: "+pn.getClass());
			// for (IPreferenceNode subn : pn.getSubNodes()) {
			// System.out.println(" SLabel:" + subn.getLabelText() + " ID:" + subn.getId());
			// }
			if ("org.eclipse.ui.preferencePages.Workbench".equals(pn.getId())) { //$NON-NLS-1$
				WorkbenchPreferenceNode pwn = (WorkbenchPreferenceNode) pn;
				pwn.remove("org.eclipse.ui.browser.preferencePage"); //$NON-NLS-1$
			}
		}
	}

	public void startUpdateLookUpJob(boolean updateCalledFromCommandLine) {
		if (needToRestoreTXMHOME) return; // don't call for update for the first run of TXM

		try {
			boolean doUpdate = TBXPreferences.getInstance().getBoolean(TBXPreferences.FETCH_UPDATES_AT_STARTUP);
			if (!doUpdate) return;
			//
			// this.syncExec(new Runnable() {
			// @Override
			// public void run() {

			try {
				URL url = new URL("https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/dist/index.html"); //$NON-NLS-1$
				Log.fine(TXMCoreMessages.bind("Testing if update site is available: {0}...", url)); //$NON-NLS-1$
				InputStream data = url.openStream();
				data.read();
				data.close();
			}
			catch (Exception e) {
				Log.warning(TXMCoreMessages.bind(TXMUIMessages.TXMCanNotFetchUpdatesP0, e));
				doUpdate = false;
				return;
			}

			Log.info(TXMUIMessages.startingUpdateFetch);

			Display.getDefault().syncExec(new Runnable() {

				@Override
				public void run() {
					IHandlerService handlerService = PlatformUI.getWorkbench().getService(IHandlerService.class);
					if (handlerService != null) {
						try {
							Event e = new Event();
							e.display = Display.getDefault();
							handlerService.executeCommand(TXMUpdateHandler.ID, e);
						}
						catch (Exception e) {
							System.out.println(TXMCoreMessages.bind(TXMUIMessages.errorWhileUpdatingTXMP0, e));
							Log.printStackTrace(e);
						}
					}
					else {
						System.out.println(TXMUIMessages.errorWhileUpdatingTXMP0NoHandlerServiceFound);
					}
				}
			});
		}
		catch (Exception e) {
			System.out.println(NLS.bind(TXMUIMessages.errorWhileFetchingUpdatesP0, e));
		}
		catch (ThreadDeath td) {
			System.out.println(TXMUIMessages.updateCanceled);
		}
	}

	public static boolean testTXMHOMEPreferenceAndDirectory() {
		String path = Toolbox.getTxmHomePath();

		if (path == null || path.trim().equals("")) { //$NON-NLS-1$
			Log.warning(TXMCoreMessages.bind(TXMUIMessages.TXMUserDirectoryIsNotSetP0PreferenceIsNotSet, TBXPreferences.USER_TXM_HOME));
			return true;
		}
		File dir = new File(Toolbox.getTxmHomePath().trim());
		if (!dir.exists()) {
			Log.warning(TXMCoreMessages.bind(TXMUIMessages.TXMUserDirectoryIsSetToP0ButDoesNotExist, dir));
			return true;
		}
		else if (!dir.canWrite()) {
			Log.warning(TXMCoreMessages.bind(TXMUIMessages.TXMUserDirectoryIsSetToP0ButTXMCantWriteInThisDirectory, dir));
			return true;
		}
		else if (!dir.canExecute()) {
			Log.warning(TXMCoreMessages.bind(TXMUIMessages.TXMUserDirectoryIsSetToP0ButTXMCantCdInThisDirectory, dir));
			return true;
		}

		return false;
	}

	public static boolean testTXMINSTALLPreferenceAndDirectory() {
		// TxmPreferences.dump();
		if (Toolbox.getInstallDirectory() == null) {
			Log.warning(TXMCoreMessages.bind(TXMUIMessages.TXMInstallDirectoryIsNotSetP0PreferenceIsNotSet, TBXPreferences.INSTALL_DIR));
			return true;
		}

		File installdir = new File(Toolbox.getInstallDirectory().trim());
		if (Toolbox.getInstallDirectory().equals("")) { //$NON-NLS-1$
			Log.warning(TXMUIMessages.TXMInstallDirectoryIsSetToQuoteQuote);
			return true;
		}
		else if (!installdir.exists()) {
			Log.warning(TXMCoreMessages.bind(TXMUIMessages.TXMInstallDirectoryIsSetToP0ButDoesNotExist, installdir));
			return true;
		}
		else if (!installdir.canRead()) {
			Log.warning(TXMCoreMessages.bind(TXMUIMessages.TXMInstallDirectoryIsSetToP0ButIsNotReadable, installdir));
			return true;
		}
		else if (!installdir.canExecute()) {
			Log.warning(TXMCoreMessages.bind(TXMUIMessages.TXMInstallDirectoryIsSetToP0ButTXMCantCdInThisDirectory, installdir));
			return true;
		}
		return false;
	}

	/**
	 * shutdown code.
	 */
	@Override
	public void postShutdown() {

		callPreStopScript();

		Toolbox.shutdown();

		callStopScript();

		if (lockFile != null) {
			lockFile.delete(); //$NON-NLS-1$
		}
	}

	/**
	 * shutdown code.
	 */
	@Override
	public boolean preShutdown() {
		try {
			closeEmptyEditors(true);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return continueShutDownForPersistedResults();
	}

	private boolean continueShutDownForPersistedResults() {
		if (Toolbox.workspace == null) return true; // nothing to do

		StringBuffer messages = new StringBuffer();
		for (Project project : Toolbox.workspace.getProjects()) {
			ArrayList<String> bugs = new ArrayList<>();
			for (TXMResult result : project.getDeepChildren()) {
				if (result.mustBePersisted() && result.isAltered()) {
					bugs.add(result.getSimpleName() + " (" + result.getResultType() + ")"); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}
			if (bugs.size() > 0) {
				messages.append("- " + project.getName() + ": " + bugs + "\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
		}

		if (messages.length() > 0) {
			return MessageDialog.openConfirm(Display.getCurrent().getActiveShell(), "", NLS.bind(TXMUIMessages.warning_popup_editedResultsWillBelostP0, messages)); //$NON-NLS-1$
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.application.WorkbenchAdvisor#getInitialWindowPerspectiveId()
	 */
	@Override
	public String getInitialWindowPerspectiveId() {
		return TXMPerspective.ID;
	}

	/**
	 * initialize TXM log in console by redirecting stderr and stdout.
	 */
	private void InitializeLogger() {
		IOConsole console = new IOConsole(TXMUIMessages.systemOutput, null);
		ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[] { console });

		// Enable or disable the outputs redirection to the RCP console according to the command line argument "-noredirection"
		boolean outputsRedirection = true;
		String[] args = Platform.getCommandLineArgs();
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-noredirection")) { //$NON-NLS-1$
				outputsRedirection = false;
				break;
			}
		}
		if (outputsRedirection) {
			stream = console.newOutputStream();
			streamInput = console.getInputStream();

			System.setOut(new PrintStream(stream));
			System.setErr(new PrintStream(stream));
			System.setIn(streamInput);
		}
		// System.out.println(Messages.ApplicationWorkbenchAdvisor_3);
	}

	public void setConsoleOutputColor(Color newColor) {
		if (stream != null) stream.setColor(newColor);
	}

	/**
	 * if INSTALLDIR preference is not set or empty, read the install.prefs file and set the
	 *
	 * First: restore preferences from previous TXM
	 * Second: set install preferences from new TXM
	 * Third: restore TreeTagger preferences
	 *
	 * Need to define a list of preferences that must be updated by the new install, then exchange step 1 and 2 (and remove step 3)
	 *
	 * @param jobHandler : the init job
	 * @return true, if successful
	 */
	private boolean checkInstallDirectory(JobHandler jobHandler) {
		try {
			installPreferenceRestored = false;
			boolean needTosetInstallPreference = testTXMINSTALLPreferenceAndDirectory();
			// check if the install preference file has been loaded
			if (needTosetInstallPreference) {

				installPreferenceRestored = false; // reset and finally set true if the process ended correctly

				// set the install directory preference
				String installpath = Platform.getInstallLocation().getURL().getFile(); // the TXM.exe file path
				File installDirectory = new File(installpath);
				// installDirectory = installDirectory.getCanonicalFile();

				// TODO: ensure all preferences of install.prefs file are now stored in the default preferences
				// File preferenceFile = new File(installDirectory, "install.prefs"); //$NON-NLS-1$
				// if (preferenceFile.exists()) {
				// System.out.println(NLS.bind(RCPMessages.ApplicationWorkbenchAdvisor_25, preferenceFile));
				// if (Util.isWindows()) //NSIS has generated ISO-8859-1 preference file
				// TxmPreferences.importFromFile(preferenceFile, "ISO-8859-1", false); //$NON-NLS-1$
				// else
				// TxmPreferences.importFromFile(preferenceFile);
				// } else {
				// System.out.println("No installation preference file found in the installation directory: "+preferenceFile);
				// installPreferenceRestored = false;
				// return true;
				// }
				// // restore previous TreeTagger preferences
				// File previousPreferenceFile = new File(System.getProperty("java.io.tmpdir"), //$NON-NLS-1$
				// "org.txm.rcp.prefs"); //$NON-NLS-1$
				//
				// if (System.getProperty("os.name").indexOf("Mac") >= 0) { //$NON-NLS-1$ //$NON-NLS-2$
				// previousPreferenceFile = new File("/tmp/org.txm.rcp.prefs"); //$NON-NLS-1$
				// }
				//
				// if (previousPreferenceFile.exists()) {
				// System.out.println("Restoring preferences (from "+previousPreferenceFile+").");
				// Properties previousProperties = new Properties();
				// BufferedReader reader = IOUtils.getReader(previousPreferenceFile, "ISO-8859-1");
				// previousProperties.load(reader);
				// String [] keys= {TBXPreferences.TREETAGGER_INSTALL_PATH,
				// TBXPreferences.TREETAGGER_MODELS_PATH,
				// TBXPreferences.TREETAGGER_OPTIONS};
				// for (String k : keys) {
				// if (previousProperties.getProperty(k) != null) {
				// TxmPreferences.set(k, previousProperties.getProperty(k));
				// }
				// }
				// }

				installPreferenceRestored = true;

				if (installDirectory == null || !installDirectory.exists()) {
					Log.severe(TXMUIMessages.errorTheProvidedInstallDirectoryDoesNotExistAborting);
					return false;
				}

				TBXPreferences.getInstance().put(TBXPreferences.INSTALL_DIR, installDirectory.getAbsolutePath());
				TBXPreferences.getInstance().flush();
				return true;
			}
		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.bind(TXMUIMessages.couldNotReadInstallPreferencesFileP0, e));
			Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	boolean go = false;

	File backup;

	File oldbackup;

	File errorbackup;

	File corporaDir;

	/**
	 * if TXMHOME path is not set or wrong
	 * - set default theme
	 * - restore old corpora
	 * - creates TXMHOME and copy "scripts", "samples" directories from TXMINSTALLDIR and mkdirs workspaces, registry,
	 * clipboard plus an empty Workspace (TXMObject) is created then load the sample corpora.
	 *
	 *
	 */
	private static boolean needToRestoreTXMHOME;

	public static boolean getWasFirstLaunch() {
		return needToRestoreTXMHOME;
	}

	File txmhomedir = null;

	File installDirectory = null;

	private Handler severeDialobBoxHandler = new Handler() {

		@Override
		public void publish(final LogRecord record) {
			// System.out.println("severeDialobBoxHandler.publish()");
			boolean showSevereDialobBox = RCPPreferences.getInstance().getBoolean(RCPPreferences.SHOW_SEVERE_DIALOG);
			if (showSevereDialobBox) {
				if (record.getLevel() != Level.SEVERE) return;

				final IWorkbench w = PlatformUI.getWorkbench();
				if (w == null) {
					//System.out.println("no workbench"); //$NON-NLS-1$
					return;
				}

				w.getDisplay().syncExec(new Runnable() {

					@Override
					public void run() {
						//						try {
						//							IWorkbenchWindow aww = w.getActiveWorkbenchWindow();
						//							if (aww == null) {
						//								System.out.println(TXMUIMessages.noActiveWorkbenchWindow);
						//								return;
						//							}
						//							IWorkbenchPage page = aww.getActivePage();
						//							if (page == null) {
						//								System.out.println(TXMUIMessages.noActivePage);
						//								return;
						//							}
						//							IViewPart view = page.findView("org.eclipse.pde.runtime.LogView"); //$NON-NLS-1$
						//							if (view == null) {
						//								view = page.showView("org.eclipse.pde.runtime.LogView"); //$NON-NLS-1$
						//							}
						//						}
						//						catch (Exception e1) {
						//							System.out.println(TXMUIMessages.partInitializationErrorColon + e1);
						//							e1.printStackTrace();
						//							return;
						//						}
						MessageDialog.openError(w.getDisplay().getActiveShell(), TXMUIMessages.error, record.getMessage());
					}
				});

				//				IStatus s = new Status(Status.ERROR, Application.PLUGIN_ID, Status.OK, record.getMessage(), null);
				//				StatusManager.getManager().handle(s, StatusManager.LOG);
			}
		}

		@Override
		public void flush() {
		}

		@Override
		public void close() throws SecurityException {
		}
	};

	/**
	 * internal variable to know if the txm home directory has been created
	 */
	private boolean txmHomeRestored;

	{
		Log.addHandler(severeDialobBoxHandler);
		severeDialobBoxHandler.setLevel(Level.SEVERE);
	}

	private boolean checkTXMHOME(JobHandler jobHandler, IProgressMonitor monitor) {
		txmhomedir = null;
		installDirectory = null;

		// check if TXMHOME of the user is set and exists
		needToRestoreTXMHOME = testTXMHOMEPreferenceAndDirectory();
		Log.fine("needToRestoreTXMHOME=" + needToRestoreTXMHOME + " installPreferenceRestored=" + installPreferenceRestored); //$NON-NLS-1$ //$NON-NLS-2$
		if (needToRestoreTXMHOME || installPreferenceRestored) {

			System.out.println(TXMUIMessages.creatingTXMUserWorkingDirectory);

			try {
				Log.info(NLS.bind(TXMUIMessages.installPathColonP0, Toolbox.getInstallDirectory()));

				//MD test 02022023
				//txmhomedir = ResourcesPlugin.getWorkspace().getRoot().getLocation().toFile().getCanonicalFile();
				txmhomedir = Platform.getLocation().toFile().getCanonicalFile();
				txmhomedir.mkdirs(); // creates the directory if needed

				txmhomedir.mkdirs(); // creates the directory if needed

				if (!txmhomedir.exists()) {
					Log.severe(TXMCoreMessages.bind(TXMUIMessages.errorTheProvidedTXMHOMEDirectoryDoesNotExistsP0Aborting, txmhomedir));
					return false;
				}

				// save preference if USER_TXM_HOME has changed
				TBXPreferences.getInstance().put(TBXPreferences.USER_TXM_HOME, txmhomedir.getAbsolutePath());
				TBXPreferences.getInstance().flush();
				// TXMPreferences.saveAll();

				Log.info(TXMCoreMessages.bind(TXMUIMessages.TXMUserDirectoryIsSetToP0, txmhomedir));

				txmHomeRestored = true;
			}
			catch (Exception e) {
				org.txm.utils.logger.Log.printStackTrace(e);
				System.out.println(TXMCoreMessages.bind(TXMUIMessages.failedToBackupExistingCorporaP0, e.getLocalizedMessage()));
				Log.severe(TXMCoreMessages.bind(TXMUIMessages.failedToBackupExistingCorporaP0, e.getLocalizedMessage()));
				return false;
			}

		} // end if

		return true;
	}

	/**
	 * Install the sample corpora and previous TXM version corpora in TXM corpora directory
	 * 
	 * @param monitor
	 * @throws IOException
	 * @throws IllegalStateException
	 */
	private void installCorporaDirectory(IProgressMonitor monitor) throws IllegalStateException, IOException {
		// if (org.eclipse.core.runtime.Platform.inDevelopmentMode()) {
		// System.out.println("DEV MODE: no corpus restored!");
		// return; // dev mode
		// }
		monitor.setTaskName(TXMUIMessages.restoringCorpora);

		RecoverCorporaFromInstalls.restore(true);

		try {
			Display.getDefault().syncExec(new Runnable() {

				@Override
				public void run() {
					OpenWelcomePage.openWelcomePage();
				}
			});
		}
		catch (Exception e2) {
			try {
				Display.getCurrent().syncExec(new Runnable() {

					@Override
					public void run() {
						OpenWelcomePage.openWelcomePage();
					}
				});
			}
			catch (Exception e3) {
				try {
					OpenWelcomePage.openWelcomePage();
				}
				catch (Exception e4) {
				}
			}
		}

		monitor.setTaskName(""); //$NON-NLS-1$
	}

	/**
	 * Executes the specified Groovy script file.
	 * 
	 * @param script
	 */
	private void callGroovyScript(File script) {
		try {
			if (script.exists() && script.canRead()) {
				ExecuteGroovyScript.executeScript(script.getAbsolutePath(), null, null, null, true, ""); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.bind(TXMUIMessages.failedToExecuteTheP0ScriptColonP1, script, e));
			Log.printStackTrace(e);
		}
	}


	/**
	 * Called just before the Toolbox is initialized
	 */
	private void callPreStartScript() {
		this.callGroovyScript(new File(Toolbox.getTxmHomePath(), "scripts/groovy/user/prestart.groovy")); //$NON-NLS-1$
	}

	/**
	 * Called just after Toolbox is initialized
	 */
	private void callStartScript() {
		this.callGroovyScript(new File(Toolbox.getTxmHomePath(), "scripts/groovy/user/start.groovy")); //$NON-NLS-1$
	}

	/**
	 * Called just before Toolbox is stop call
	 */
	private void callPreStopScript() {
		this.callGroovyScript(new File(Toolbox.getTxmHomePath(), "scripts/groovy/user/prestop.groovy")); //$NON-NLS-1$
	}

	/**
	 * Called just after Toolbox is stop call
	 */
	private void callStopScript() {
		this.callGroovyScript(new File(Toolbox.getTxmHomePath(), "scripts/groovy/user/stop.groovy")); //$NON-NLS-1$
	}

	ProgressMonitorDialog initializeUIPMD; // store the state of the initializeUI step

	/**
	 * after TXMHOME AND INSTALLDIR are ok, we can start the Toolbox
	 * <p>
	 * in a modal dialogs: 1- call pre start scripts, 2- init, 3- call start scripts 4- restore corpora
	 * <p>
	 * 5- we refresh the views (corpora, bases and query).
	 * <p>
	 * 6- we close empty editors
	 * <p>
	 * 7- we remove some preferences pages not useful
	 * 
	 * @param jobHandler the job
	 * @return true, if successful
	 * @throws Exception
	 */
	private boolean initializeUI(final JobHandler jobHandler) throws Exception {

		// Fix the freeze at splash screen on Mac OS X/Cocoa because of a deadlock between AWT/SWT threads (bug #894)
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				try {
					System.out.println(getTXMStartMessage());

					// ensure some preferences are set
					DefaultScope.INSTANCE.getNode("org.eclipse.ui.workbench").putBoolean(IPreferenceConstants.RUN_IN_BACKGROUND, false); //$NON-NLS-1$ //$NON-NLS-2$ $NON-NLS-2$
					DefaultScope.INSTANCE.getNode("org.eclipse.ui.workbench").flush(); //$NON-NLS-1$
					BundleDefaultsScope.INSTANCE.getNode("org.eclipse.ui.workbench").putBoolean(IPreferenceConstants.RUN_IN_BACKGROUND, false); //$NON-NLS-1$ //$NON-NLS-2$ $NON-NLS-2$
					BundleDefaultsScope.INSTANCE.getNode("org.eclipse.ui.workbench").flush(); //$NON-NLS-1$

					WorkbenchPlugin.getDefault().getPreferenceStore().setValue(IPreferenceConstants.RUN_IN_BACKGROUND, false);
					WorkbenchPlugin.getDefault().getPreferenceStore().setDefault(IPreferenceConstants.RUN_IN_BACKGROUND, false);
					WorkbenchPlugin.getDefault().getPreferenceStore().setValue(IPreferenceConstants.WORKBENCH_SAVE_INTERVAL, 30);
					WorkbenchPlugin.getDefault().getPreferenceStore().setDefault(IPreferenceConstants.WORKBENCH_SAVE_INTERVAL, 30);

					jobHandler.setTaskName(TXMUIMessages.setDefaultTheme);
					setDefaultTheme();

					// CLOSE Empty&Error editors
					jobHandler.setTaskName(TXMUIMessages.closingEmptyEditors);
					closeEmptyEditors(false);

					// remove some preference pages
					setPreferencesConfiguration();

					initializeUIPMD = new ProgressMonitorDialog(Display.getDefault().getActiveShell()) {

						@Override
						protected void configureShell(final Shell shell) {
							super.configureShell(shell);
							shell.setText(getTXMStartMessage());
						}
					};

					initializeUIPMD.run(true, true, new IRunnableWithProgress() {

						@Override
						public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {

							if (!Toolbox.isInitialized()) {
								try {
									monitor.worked(1);

									monitor.setTaskName(TXMUIMessages.callingGroovyPrestartScript);
									callPreStartScript();
									monitor.worked(20);

									monitor.setTaskName(TXMUIMessages.initializingToolboxEnginesAndCorpora);
									if (Toolbox.initialize(TBXPreferences.class, monitor)) {
										// StatusLine.setMessage(TXMUIMessages.ready);
									}
									else {
										StatusLine.setMessage(TXMUIMessages.startupFailedPleaseCheckTXMPreferencesInTheFileSupPreferencesMenuOrContactTxmusersMailingListMenuColonHelpSup);
										Log.severe(TXMUIMessages.startupFailedPleaseCheckTXMPreferencesInTheFileSupPreferencesMenuOrContactTxmusersMailingListMenuColonHelpSup);

										monitor.done();
										return;
									}
									monitor.worked(20);

									monitor.setTaskName(TXMUIMessages.callingGroovyStartScript);
									callStartScript();
									monitor.worked(20);

									// restore corpora if TXMHOME has been created
									if (txmHomeRestored) {
										installCorporaDirectory(monitor);

										Toolbox.workspace.saveParameters(true);
										ResourcesPlugin.getWorkspace().save(true, null);
									}
									monitor.worked(20);

									jobHandler.setTaskName(TXMUIMessages.toolboxReady);
								}
								catch (Throwable e) {
									System.out.println(NLS.bind(TXMUIMessages.initialisationErrorP0, e));
									e.printStackTrace();
								}
							}

							monitor.done();
						}
					});

					Log.fine(TXMUIMessages.loadingViews);
					RestartTXM.reloadViews(); // reload views

				}
				catch (Exception e) {
					Log.severe(TXMCoreMessages.bind(TXMUIMessages.errorDuringUIInitializationP0, e.getLocalizedMessage()));
					e.printStackTrace();
				}
			}
		});

		jobHandler.setTaskName(""); //$NON-NLS-1$ s
		// TxmPreferences.dump();
		return initializeUIPMD != null && !initializeUIPMD.getProgressMonitor().isCanceled();
	}

	public static String getVersionLevel() {
		try {
			URL url = BundleUtils.getBundle(Activator.class).getResource("SUBVERSION"); //$NON-NLS-1$
			String subversion = IOUtils.getText(url, "UTF-8"); //$NON-NLS-1$
			return subversion;
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return ""; //$NON-NLS-1$
	}
}
