// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.splashHandlers;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.internal.splash.EclipseSplashHandler;

//// TODO: Auto-generated Javadoc
///**
// * SplashScreen for TXM: not used yet
// */
//public class AbstractSplashHandler2 extends BasicSplashHandler {
//
//	/**
//	 * Instantiates a new abstract splash handler2.
//	 */
//	public AbstractSplashHandler2() {
//		Composite c = this.getContent();
//		new Label(c, SWT.BORDER).setText(Messages.AbstractSplashHandler2_0);
//		IProgressMonitor bundle = this.getBundleProgressMonitor();
//		// .subTask("HELLLOOOOO");
//		this.getBundleProgressMonitor().setTaskName(Messages.AbstractSplashHandler2_1);
//		System.out.println(Messages.AbstractSplashHandler2_2);
//	}
//
//	/**
//	 * Hurler.
//	 *
//	 * @param message the message
//	 */
//	public static void hurler(String message) {
//
//	}
//}

public class TXMSplashHandler extends EclipseSplashHandler {

	private static final int BORDER = 10;

	Font font;

	Color greenTXM, redTXM;

	String version, infos, level;

	@Override
	public void init(Shell splash) {
		super.init(splash);



		//		// here you could check some condition on which decoration to show
		//		
		//		version = "TXM "+ApplicationWorkbenchAdvisor.getTXMVersion(); //$NON-NLS-1$
		//		
		//		String build_date =  ApplicationWorkbenchAdvisor.getBuildDate(ApplicationWorkbenchAdvisor.getBuildVersion());
		//		
		//		String txm_date = ApplicationWorkbenchAdvisor.getUpdateDate();
		//		
		//		level = ApplicationWorkbenchAdvisor.getVersionLevel().toUpperCase();
		//		
		//		greenTXM = new Color(splash.getDisplay(), 109,139,0);
		//		redTXM = new Color(splash.getDisplay(), 255,0,0);
		//		font = new Font(Display.getCurrent(), "Arial", 17, SWT.BOLD); //$NON-NLS-1$
		//		
		//		if (txm_date.equals(build_date)) {
		//			infos = build_date;
		//		}
		//		else {
		//			infos = "update " + txm_date + " - build " + build_date; //$NON-NLS-1$ //$NON-NLS-2$
		//		}
		//		
		//		if ("STABLE".equals(level)) { //$NON-NLS-1$
		//			level = null;
		//		}
		//		
		//		getContent().addPaintListener(new PaintListener() {
		//			
		//			@Override
		//			public void paintControl(PaintEvent e) {
		//				e.gc.setFont(font);
		//				e.gc.setForeground(greenTXM);
		//				Point textSize2 = e.gc.textExtent(version);
		//				e.gc.drawString(version, (int) (316 - textSize2.x - (BORDER*1.5)), 230, true);
		//				
		//				Point textSize = e.gc.textExtent(infos);
		//				e.gc.drawString(infos, (int) (316 - textSize.x - (BORDER*1.5)), 233 + textSize2.y, true);
		//				
		//				if (level != null) {
		//					e.gc.setForeground(redTXM);
		//					textSize = e.gc.textExtent(level);
		//					e.gc.drawString(level, (int) (316 - textSize.x - (BORDER*1.5)), 2*BORDER, true);
		//				}
		//			}
		//		});
	}

	@Override
	public void dispose() {
		super.dispose();
	}

}
