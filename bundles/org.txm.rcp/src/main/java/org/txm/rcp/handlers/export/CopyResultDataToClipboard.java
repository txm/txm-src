// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.rcp.handlers.export;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.TXMResult;
import org.txm.rcp.JobsTimer;
import org.txm.rcp.StatusLine;
import org.txm.rcp.editors.ITXMResultEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.IOClipboard;
import org.txm.rcp.utils.JobHandler;
import org.txm.utils.ExecTimer;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

/**
 * Exports a result by calling the function toTxt(File f) then opens it and copy exported data to clipboard.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class CopyResultDataToClipboard extends AbstractHandler {

	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IEditorPart ieditor = HandlerUtil.getActiveEditor(event);
		if (!(ieditor instanceof ITXMResultEditor)) {
			return null;
		}

		ITXMResultEditor<TXMResult> editor = (ITXMResultEditor) ieditor;
		final Object s = editor.getResult();
		String availables = StringUtils.join(((TXMResult) s).getExportTXTExtensions());
		System.out.println(availables);
		if (s instanceof TXMResult && !availables.contains("*.txt") && !availables.contains("*.csv")) {
			Log.warning(NLS.bind("Result cannot be exported in clipboard (no textual format available {0}). Aborting. ", availables));
			return null;
		}
		StatusLine.setMessage(TXMUIMessages.exportingResults);
		File outfile;
		try {
			outfile = File.createTempFile("clipboard", "export");
		}
		catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}

		final String encoding = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_ENCODING);
		String _colseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_COL_SEPARATOR);
		final String colseparator = _colseparator;
		String _txtseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_TXT_SEPARATOR);
		final String txtseparator = _txtseparator;

		JobHandler jobhandler = new JobHandler(TXMUIMessages.exportingResults) {

			@Override
			protected IStatus _run(SubMonitor monitor) throws Exception {

				try {
					JobsTimer.start();

					if (s instanceof TXMResult) {
						TXMResult r = (TXMResult) s;

						// compute the result if needed
						if (!r.isAltered() && r.isDirty()) {
							r.compute(this);
						}
						Log.info(TXMUIMessages.bind(TXMUIMessages.exportingP0, r.getName()));
						ExecTimer.start();
						
						r.toTxt(outfile, encoding, colseparator, txtseparator);
						Log.info(TXMUIMessages.bind(TXMUIMessages.doneInP0, ExecTimer.stop()));
					}
					else {
						Log.warning(TXMUIMessages.TheExportedObjectIsNotATXMResultResult);
						return Status.CANCEL_STATUS;
					}

					if (outfile.exists()) {
						this.syncExec(new Runnable() {
							
							@Override
							public void run() {
								try {
									IOClipboard.write(IOUtils.getText(outfile));
								}
								catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								outfile.delete();
							}
						});
					}
					else {
						Log.warning(NLS.bind(TXMUIMessages.failedToExportResultP0ColonP1, outfile.getAbsolutePath()));
					}
				}
				catch (ThreadDeath td) {
					TXMResult r = (TXMResult) s;
					r.resetComputingState();
					throw td;
				}
				catch (Exception e) {
					Log.warning(NLS.bind(TXMUIMessages.failedToExportResultP0ColonP1, s, e));
					throw e;
				}
				return Status.OK_STATUS;
			}
		};

		jobhandler.startJob();
		
		return null;
	}
}
