package org.txm.rcp.swt.widget.parameters;

import org.apache.commons.lang.StringUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.kohsuke.args4j.NamedOptionDef;
import org.txm.rcp.swt.widget.MultipleSelectionCombo;

public class StringArrayMultipleField extends LabelField {

	MultipleSelectionCombo dt;

	public StringArrayMultipleField(Composite parent, int style, NamedOptionDef parameter) {
		super(parent, style, parameter);

		String[] values = getMetaVar().trim().split("\t"); //$NON-NLS-1$
		int[] selection = {};
		dt = new MultipleSelectionCombo(this, values, selection, SWT.NONE);
		dt.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));

		//dt.setItems(values);

		resetToDefault();
	}

	public String getWidgetValue() {
		return dt.getText();
	}

	public void setDefault(Object text) {
		if (text instanceof Object[]) {
			Object[] array = (Object[]) text;
			dt.setText(StringUtils.join(array, "\t")); //$NON-NLS-1$
		}
		else {
			dt.setText("" + text); //$NON-NLS-1$
		}
	}

	@Override
	public void resetToDefault() {
		dt.setText(getWidgetDefault());
	}
}
