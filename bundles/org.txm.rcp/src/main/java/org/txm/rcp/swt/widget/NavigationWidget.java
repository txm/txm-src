// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.widget;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.utils.logger.Log;

/**
 * Navigation composite
 * 
 * (|<) (<) current / number of lines (>) (>|)
 * 
 * @author mdecorde.
 */
public class NavigationWidget extends GLComposite {

	/** The info line. */
	Label infoLine;

	Text currentPositionText;

	/** The first. */
	Button first;

	/** The last. */
	Button last;

	/** The next. */
	Button next;

	/** The prev. */
	Button prev;

	/** The n line per page. */
	int nLinePerPage;

	private Color whiteColor;

	/**
	 * Instantiates a new navigation widget.
	 *
	 * @param parent the parent
	 * @param style the style
	 */
	public NavigationWidget(Composite parent, int style) {
		super(parent, style, "Navigation"); //$NON-NLS-1$
		this.getLayout().numColumns = 6;
		this.getLayout().makeColumnsEqualWidth = false;
		this.getLayout().horizontalSpacing = 1;
		// | [|<] [<] [infos] [>] [>|] |


		// [|<]
		// First page
		first = new Button(this, SWT.FLAT | SWT.PUSH);
		first.setEnabled(false);
		first.setToolTipText(TXMUIMessages.navigation_tooltip_firstPage);

		// [<]
		// Previous page
		prev = new Button(this, SWT.FLAT | SWT.PUSH);
		prev.setEnabled(false);
		prev.setToolTipText(TXMUIMessages.navigation_tooltip_previousPage);

		// [current]
		currentPositionText = new Text(this, SWT.BORDER);
		GridData currentPositionLayoutData = new GridData(GridData.VERTICAL_ALIGN_CENTER, SWT.CENTER, true, false);
		currentPositionLayoutData.minimumWidth = 40;
		currentPositionText.setLayoutData(currentPositionLayoutData);
		currentPositionText.setTextLimit(6);
		currentPositionText.setEditable(false);
		currentPositionText.setToolTipText(TXMUIMessages.navigation_tooltip_firstRowNumber);

		// [info]
		infoLine = new Label(this, SWT.NONE);
		GridData layoutData = new GridData(GridData.VERTICAL_ALIGN_CENTER, SWT.CENTER, true, false);
		infoLine.setLayoutData(layoutData);

		whiteColor = currentPositionText.getBackground();
		currentPositionText.setBackground(infoLine.getBackground());

		// [>]
		// Next page
		next = new Button(this, SWT.FLAT | SWT.PUSH);
		next.setEnabled(false);
		next.setToolTipText(TXMUIMessages.navigation_tooltip_nextPage);

		// [>|]
		// Last page
		last = new Button(this, SWT.FLAT | SWT.PUSH);
		last.setEnabled(false);
		last.setToolTipText(TXMUIMessages.navigation_tooltip_lastPage);

		// set icons
		first.setImage(IImageKeys.getImage(IImageKeys.CTRLSTART));
		prev.setImage(IImageKeys.getImage(IImageKeys.CTRLREVERSE));
		next.setImage(IImageKeys.getImage(IImageKeys.CTRLPLAY));
		last.setImage(IImageKeys.getImage(IImageKeys.CTRLEND));

		// set layout data
		GridData buttonGridData = new GridData(GridData.FILL, GridData.CENTER, false, false);
		buttonGridData.widthHint = 30;
		buttonGridData.heightHint = 30;
		GridData centerButtonGridData = new GridData(GridData.END, GridData.CENTER, true, true);
		centerButtonGridData.widthHint = 30;
		centerButtonGridData.heightHint = 30;
		first.setLayoutData(centerButtonGridData);
		prev.setLayoutData(buttonGridData);
		next.setLayoutData(buttonGridData);
		centerButtonGridData = new GridData(GridData.BEGINNING, GridData.CENTER, true, true);
		centerButtonGridData.widthHint = 30;
		centerButtonGridData.heightHint = 30;
		last.setLayoutData(centerButtonGridData);
	}

	public void addGoToKeyListener(KeyListener keyListener) {
		currentPositionText.addKeyListener(keyListener);
		currentPositionText.setEditable(true);
		currentPositionText.setBackground(whiteColor);
		//currentPosition.setBackground(new Color(currentPosition.get));
	}

	/**
	 * Adds the first listener.
	 *
	 * @param selectionListener the selection listener
	 */
	public void addFirstListener(SelectionListener selectionListener) {
		first.addSelectionListener(selectionListener);
	}

	/**
	 * Adds the next listener.
	 *
	 * @param selectionListener the selection listener
	 */
	public void addNextListener(SelectionListener selectionListener) {
		next.addSelectionListener(selectionListener);
	}

	/**
	 * Adds the previous listener.
	 *
	 * @param selectionListener the selection listener
	 */
	public void addPreviousListener(SelectionListener selectionListener) {
		prev.addSelectionListener(selectionListener);
	}

	/**
	 * Adds the last listener.
	 *
	 * @param selectionListener the selection listener
	 */
	public void addLastListener(SelectionListener selectionListener) {
		last.addSelectionListener(selectionListener);
	}

	/**
	 * Sets the first enabled.
	 *
	 * @param b the new first enabled
	 */
	public void setFirstEnabled(boolean b) {
		first.setEnabled(b);
	}

	/**
	 * Sets the last enabled.
	 *
	 * @param b the new last enabled
	 */
	public void setLastEnabled(boolean b) {
		last.setEnabled(b);
	}

	/**
	 * Sets the next enabled.
	 *
	 * @param b the new next enabled
	 */
	public void setNextEnabled(boolean b) {
		next.setEnabled(b);
	}

	/**
	 * Sets the previous enabled.
	 *
	 * @param b the new previous enabled
	 */
	public void setPreviousEnabled(boolean b) {
		prev.setEnabled(b);
	}

	public int getCurrentPositionFromTextField() {
		int ret = -1;
		try {
			ret = Integer.parseInt(currentPositionText.getText());
		}
		catch (Exception e) {
			System.out.println(TXMUIMessages.errorWhileGettingCurrentPositionValueColon + e);
			Log.printStackTrace(e);
		}
		return ret;
	}

	/**
	 * Sets the info line text.
	 *
	 * @param s the new info line text
	 */
	public void setInfoLineText(String current, String s) {
		this.currentPositionText.setText(current);
		this.infoLine.setText(s);
		this.requestLayout();	
	}

	/**
	 * @return the currentPositionText
	 */
	public Text getCurrentPositionText() {
		return currentPositionText;
	}
}
