// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.editors.imports;

import java.io.File;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.txm.Toolbox;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Project;
import org.txm.rcp.editors.input.ImportFormEditorInput;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.views.corpora.CorporaView;

/**
 * display&edit the import parameters
 * 
 * @author mdecorde.
 */
public class ImportFormEditor extends FormEditor {

	/** The Constant ID. */
	public static final String ID = "org.txm.rcp.editors.imports.ImportFormEditor"; //$NON-NLS-1$

	/** The import Name to run. */
	private String importName;

	/** The toolkit. */
	private FormToolkit toolkit;

	/** The form. */
	private ScrolledForm form;

	/** The main. */
	private CorpusPage main;

	/** The meta. */
	MetadataPage meta;

	private ImportFormEditorInput finput;

	private Boolean updateMode;

	protected Project project;

	/**
	 * Instantiates a new import form editor.
	 */
	public ImportFormEditor() {
		super();
	}

	/**
	 * Do save.
	 *
	 * @param arg0 the arg0
	 * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor arg0) {

	}

	/**
	 * Remove a page
	 * 
	 * @param page
	 */
	public void removePage(FormPage page) {
		int idx = pages.indexOf(page);
		this.removePage(idx);
	}

	/**
	 * Do save as.
	 *
	 * @see "org.eclipse.part.EditorPart#doSaveAs()"
	 */
	@Override
	public void doSaveAs() {

	}

	public void firePropertyChange(int propertyId) {
		super.firePropertyChange(propertyId);
	}

	/**
	 * Inits the.
	 *
	 * @param site the site
	 * @param input the input
	 * @throws PartInitException the part init exception
	 * @see org.eclipse.ui.part.EditorPart#init(org.eclipse.ui.IEditorSite,
	 *      org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setSite(site);
		setInput(input);
		finput = ((ImportFormEditorInput) input);
		String txmhome = Toolbox.getTxmHomePath();

		File f = finput.getGroovyFile();
		if (f != null) {
			String filepath = f.getPath();
			String importName = new File(txmhome, filepath).getName();
			if (importName.indexOf("Loader.groovy") > 0) { //$NON-NLS-1$
				importName = importName.substring(0, importName.indexOf("Loader.groovy")); //$NON-NLS-1$
			}
			this.setImportName(importName);
		}
		else { // use the corpora view selection

			Object o = CorporaView.getFirstSelectedObject();

			if (o instanceof CorpusBuild) {
				o = ((CorpusBuild) o).getProject();
			}

			if (o instanceof Project) {
				Project p = (Project) o;
				this.setImportName(p.getImportModuleName());
				this.project = p;
			}
		}

		this.setUpdateMode(finput.getUpdateMode());



		//ExecuteImportScript.getImportScriptFromToolboxPlugin(getGroovyscript()); // always update if script from Toolbox is more recent

		//		if (!getGroovyscript().exists()) // just a warning message
		//			System.out.println(NLS.bind(TXMUIMessages.warningColonTheImportScriptIsMissingColonP0, getGroovyscript()));
	}

	public void setUpdateMode(Boolean updateMode) {

		this.updateMode = updateMode;
	}

	public Boolean getUpdateMode() {

		return this.updateMode;
	}

	/**
	 * Checks if is dirty.
	 *
	 * @return true, if is dirty
	 * @see org.eclipse.ui.part.EditorPart#isDirty()
	 */
	@Override
	public boolean isDirty() {
		return false;
	}

	/**
	 * Checks if is save as allowed.
	 *
	 * @return true, if is save as allowed
	 * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/**
	 * Creates the context menu.
	 *
	 * @param tableViewer the table viewer
	 */
	private void createContextMenu(TableViewer tableViewer) {

		MenuManager menuManager = new MenuManager();
		Menu menu = menuManager.createContextMenu(tableViewer.getTable());

		// Set the MenuManager
		tableViewer.getTable().setMenu(menu);
		getSite().registerContextMenu(menuManager, tableViewer);
		// Make the selection available
		getSite().setSelectionProvider(tableViewer);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.MultiPageEditorPart#setFocus()
	 */
	@Override
	public void setFocus() {

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getName()
	 */
	@Override
	public String getPartName() {

		if (main != null && project != null) {
			if (updateMode) {
				return TXMUIMessages.bind(TXMUIMessages.UpdateP0P1Import, project.getName(), importName);
			}
			else {
				return TXMUIMessages.bind(TXMUIMessages.P0ImportWithP1, project.getName(), importName);
			}
		}
		else {
			return TXMUIMessages.bind(TXMUIMessages.P0Import, importName);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.forms.editor.FormEditor#addPages()
	 */
	@Override
	protected void addPages() {
		main = new CorpusPage(this);
		meta = new MetadataPage(this, main);

		try {
			this.addPage(main);
			this.addPage(meta);
		}
		catch (PartInitException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	public CorpusPage getMainPage() {
		return main;
	}

	public MetadataPage getMetaPage() {
		return meta;
	}

	public String getImportName() {
		return importName;
	}

	public void setImportName(String importName) {
		this.importName = importName;
	}
}
