// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.actions;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.TXMFontRegistry;
import org.txm.rcp.swt.widget.PartitionComposer;
import org.txm.rcp.swt.widget.structures.AdvancedPartitionPanel;
import org.txm.rcp.swt.widget.structures.CrossedPartitionPanel;
import org.txm.rcp.swt.widget.structures.SimplePartitionPanel;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.utils.logger.Log;

/**
 * Allow the user to choose between 3 mode to create a Partition Simple : choose
 * a structural unit and a property Assisted : choose a structural unit and a
 * property, then dispatch them into parts Advanced : user define the queries to
 * build the parts
 * 
 * @author mdecorde.
 */
public class CreatePartitionDialog extends Dialog {

	/** The corpus. */
	private CQPCorpus corpus;

	/** The name text. */
	private Text nameText;

	/** The partition. */
	private Partition partition;

	private SimplePartitionPanel simplePanel;

	private PartitionComposer compositeForAssisted;

	private CrossedPartitionPanel crossedPanel;

	private AdvancedPartitionPanel advancedPartitionPanel;

	private TabItem selectedTab;

	private TabItem simpleTab;

	private TabItem assistedTab;

	private TabItem crossedTab;

	private TabItem advancedTab;

	/**
	 * Instantiates a new creates the partition dialog.
	 *
	 * @param parentShell the parent shell
	 * @param corpus the corpus
	 */
	public CreatePartitionDialog(Shell parentShell, CQPCorpus corpus) {

		super(parentShell);
		this.corpus = corpus;
		this.setShellStyle(this.getShellStyle() | SWT.RESIZE);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {

		super.configureShell(newShell);
		newShell.setText(TXMUIMessages.createPartition);
		newShell.setMinimumSize(400, 400);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		Composite c = new Composite(parent, SWT.NONE);
		c.setLayout(new GridLayout(2, false));
		GridData maindatalayout = new GridData(GridData.FILL, GridData.FILL, true, false);
		maindatalayout.minimumHeight = 400;
		maindatalayout.minimumWidth = 600;
		c.setLayoutData(maindatalayout);
		Label nameLabel = new Label(c, SWT.NONE);
		Font f = nameLabel.getFont();
		FontData defaultFont = f.getFontData()[0];
		defaultFont.setStyle(SWT.BOLD);

		Font newf = TXMFontRegistry.getFont(Display.getCurrent(), defaultFont);
		nameLabel.setText(TXMUIMessages.ampNameColon);
		nameLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));
		nameLabel.setFont(newf);

		nameText = new Text(c, SWT.BORDER);
		GridData data = new GridData(GridData.FILL, GridData.FILL, true, false);
		data.horizontalSpan = 1;
		nameText.setLayoutData(data);

		final TabFolder tabFolder = new TabFolder(parent, SWT.NONE);
		tabFolder.setLayoutData(new GridData(GridData.FILL, GridData.FILL,
				true, true));
		// First tab
		// --------------------------------------------------------

		simpleTab = new TabItem(tabFolder, SWT.NONE);
		simpleTab.setText(TXMUIMessages.simple);
		simplePanel = new SimplePartitionPanel(tabFolder, SWT.NONE, corpus);
		simpleTab.setControl(simplePanel);

		// Second Tab : assisted mode
		assistedTab = new TabItem(tabFolder, SWT.NONE);
		assistedTab.setText(TXMUIMessages.assisted);
		compositeForAssisted = new PartitionComposer(tabFolder, SWT.NONE, this.corpus);
		assistedTab.setControl(compositeForAssisted);

		// Second Tab : crossed mode
		crossedTab = new TabItem(tabFolder, SWT.NONE);
		crossedTab.setText(TXMUIMessages.crossed);
		crossedPanel = new CrossedPartitionPanel(tabFolder, SWT.NONE, this.corpus);
		crossedTab.setControl(crossedPanel);

		// Last tab: advanced mode
		advancedTab = new TabItem(tabFolder, SWT.NONE);
		advancedTab.setText(TXMUIMessages.advanced);
		advancedPartitionPanel = new AdvancedPartitionPanel(tabFolder, SWT.NONE, this.corpus);
		advancedTab.setControl(advancedPartitionPanel);

		selectedTab = simpleTab;

		// check the state of the OK button according to the change of tab
		// --------------------------------

		tabFolder.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {

				selectedTab = tabFolder.getSelection()[0];
			}
		});

		return tabFolder;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {

		if (selectedTab == advancedTab && advancedPartitionPanel.isQueriesSet()) {
			MessageDialog.openError(getShell(),
					TXMUIMessages.invalidQuery,
					TXMUIMessages.queriesCannotBeLeftEmpty);
			return;
		}

		partition = createPartition();
		super.okPressed();
	}

	/**
	 * Creates the partition.
	 *
	 * @return the partition
	 */
	public Partition createPartition() {

		Partition partition = null;

		String name = nameText.getText();

		if (selectedTab == simpleTab) { // Simple mode

			partition = simplePanel.createPartition(name, null);
		}
		else if (selectedTab == assistedTab) { // Assisted mode

			// auto-naming if needed
			if (name.isEmpty()) {
				name = compositeForAssisted.getProperty().asFullNameString();
				Log.info(TXMCoreMessages.bind(TXMUIMessages.noPartitionNameWasSpecifiedTheNameIsP0, name));
			}

			partition = this.compositeForAssisted.createPartition(name, null);
		}
		else if (selectedTab == crossedTab) { // Advanced mode

			partition = crossedPanel.createPartition(name, null);
		}
		else if (selectedTab == advancedTab) { // Advanced mode

			partition = advancedPartitionPanel.createPartition(name, null);
		}

		// Error
		if (partition == null) {
			Log.severe(TXMUIMessages.noPartWasDefined);
			return null;
		}

		return partition;
	}

	/**
	 * Gets the partition.
	 *
	 * @return the partition
	 */
	public Partition getPartition() {

		return partition;
	}
}
