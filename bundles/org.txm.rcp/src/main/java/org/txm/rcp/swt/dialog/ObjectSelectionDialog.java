// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.dialog;

import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.messages.TXMUIMessages;

/**
 * allow to choose between a set of values and choose the order of the selection
 * 
 * @author mdecorde.
 */
public class ObjectSelectionDialog extends Dialog {

	/** The main area. */
	protected Composite mainArea;

	/** The availables. */
	final protected List<Object> availables;

	/** The selecteds. */
	final protected List<Object> selecteds;

	/** The max. */
	protected int max = 2;

	/** The availables view. */
	protected org.eclipse.swt.widgets.List availablesView;

	/** The selecteds view. */
	protected org.eclipse.swt.widgets.List selectedsView;

	/** The regex available. */
	protected Text regexAvailable;

	/** The regex selected. */
	protected Text regexSelected;

	/**
	 * Instantiates a new object selection dialog.
	 *
	 * @param shell the shell
	 * @param available the available
	 * @param selected the selected
	 * @param max the max
	 */
	public ObjectSelectionDialog(Shell shell, List available,
			List selected, int max) {
		super(shell);

		this.availables = available;
		this.selecteds = selected;
		this.setShellStyle(this.getShellStyle() | SWT.RESIZE);
		this.max = max;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell
				.setText(TXMUIMessages.valueSelector);
		newShell.setMinimumSize(300, 260);

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		// parent.setLayout(new FormLayout());
		mainArea = new Composite(parent, SWT.NONE);
		mainArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		// 4 columns
		GridLayout layout = new GridLayout(4, false);
		mainArea.setLayout(layout);

		regexAvailable = new Text(mainArea, SWT.BORDER);
		regexAvailable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		Button searchAvailableBtn = new Button(mainArea, SWT.PUSH);
		searchAvailableBtn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		searchAvailableBtn.setImage(IImageKeys.getImage(IImageKeys.ACTION_SEARCH));
		searchAvailableBtn.setToolTipText("Search in available values using a pattern");
		searchAvailableBtn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				searchAvailable();
				availablesView.showSelection();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		regexSelected = new Text(mainArea, SWT.BORDER);
		regexSelected.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		Button searchSelectedBtn = new Button(mainArea, SWT.PUSH);
		searchSelectedBtn.setToolTipText("Search in selected values using a pattern");
		searchSelectedBtn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		searchSelectedBtn.setImage(IImageKeys.getImage(IImageKeys.ACTION_SEARCH));
		searchSelectedBtn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				searchSelected();
				selectedsView.showSelection();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		availablesView = new org.eclipse.swt.widgets.List(mainArea, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		availablesView.setLayoutData(gridData);

		Composite selectionButtons = new Composite(mainArea, SWT.NONE);
		selectionButtons.setLayout(new GridLayout(1, false));
		Button select = new Button(selectionButtons, SWT.ARROW | SWT.RIGHT);
		select.setToolTipText("Select current element");
		Button unselect = new Button(selectionButtons, SWT.ARROW | SWT.LEFT);
		unselect.setToolTipText("Deselect current element");
		
		selectedsView = new org.eclipse.swt.widgets.List(mainArea, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		selectedsView.setLayoutData(gridData);

		Composite orderButtons = new Composite(mainArea, SWT.NONE);
		orderButtons.setLayout(new GridLayout(1, false));
		Button up = new Button(orderButtons, SWT.ARROW | SWT.UP);
		up.setToolTipText("Move up current element");
		Button down = new Button(orderButtons, SWT.ARROW | SWT.DOWN);
		down.setToolTipText("Move down current element");

		select.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (max == 1) // process the first selected
				{
					int index = availablesView.getSelectionIndex();
					selecteds.add(availables.get(index));
					availables.remove(index);

					availables.add(selecteds.get(0));
					selecteds.remove(0);
					return;
				}
				int[] selectionindices = availablesView.getSelectionIndices();
				if (max > 0) {
					if (selecteds.size() + selectionindices.length >= max)
						return;
				}

				for (int index : selectionindices) {
					selecteds.add(availables.get(index));
				}
				for (int index = selectionindices.length - 1; index >= 0; index--) {
					availables.remove(selectionindices[index]);
				}
				reload();
				// availablesView.setSelection(index);
			}
		});

		unselect.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (max == 1 && selecteds.size() == 1) {
					int index = selectedsView.getSelectionIndex();
					availables.add(selecteds.get(index));
					selecteds.remove(index);
					return;
				}

				int[] selectionindices = selectedsView.getSelectionIndices();
				for (int index : selectionindices) {
					availables.add(selecteds.get(index));
				}
				for (int index = selectionindices.length - 1; index >= 0; index--) {
					selecteds.remove(selectionindices[index]);
				}

				reload();
			}
		});

		up.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int index = selectedsView.getSelectionIndex();
				if (index > 0) {
					Object selectedP = selecteds.get(index);
					Object upperP = selecteds.get(index - 1);

					selecteds.set(index, upperP);
					selecteds.set(index - 1, selectedP);

					reload();
					selectedsView.setSelection(index - 1);
				}
			}
		});

		down.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int index = selectedsView.getSelectionIndex();
				if (index < selecteds.size() - 1) {
					Object selectedP = selecteds.get(index);
					Object bellowP = selecteds.get(index + 1);

					selecteds.set(index, bellowP);
					selecteds.set(index + 1, selectedP);

					reload();
					selectedsView.setSelection(index + 1);
				}
			}
		});

		availablesView.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				int index = availablesView.getSelectionIndex();

				selecteds.add(availables.get(index));
				availables.remove(index);

				reload();
				availablesView.setSelection(index);
			}
		});

		selectedsView.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				if (selecteds.size() > 1) {
					int index = selectedsView.getSelectionIndex();

					availables.add(selecteds.get(index));
					selecteds.remove(index);

					reload();
					selectedsView.setSelection(index);
				}
			}
		});

		reload();
		return mainArea;
	}

	/**
	 * Reload.
	 */
	public void reload() {
		availablesView.removeAll();
		for (Object obj : availables) {
			availablesView.add(obj.toString());
		}
		selectedsView.removeAll();
		for (Object obj : selecteds) {
			selectedsView.add(obj.toString());
		}

		availablesView.getParent().layout();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		if (selecteds.size() != 0)
			super.okPressed();
	}

	/**
	 * Search available.
	 */
	protected void searchAvailable() {
		try {
			int count = 0;
			availablesView.deselectAll();
			String regex = regexAvailable.getText();

			Pattern p = Pattern.compile(regex);
			for (int i = 0; i < availablesView.getItemCount(); i++)
				if (availablesView.getItem(i).toString().matches(regex)) {
					availablesView.select(i);
					count++;
				}
			System.out.println(NLS.bind(TXMUIMessages.foundP0Values, count));
		}
		catch (Exception ex) {
			System.out.println(NLS.bind(TXMUIMessages.error_invalidRegularExpressionP0, ex.getMessage()));
		}
	}

	/**
	 * Search selected.
	 */
	protected void searchSelected() {
		try {
			int count = 0;
			selectedsView.deselectAll();
			String regex = regexSelected.getText();
			if (regex.length() > 0)
				regex = ".*" + regex + ".*"; //$NON-NLS-1$ //$NON-NLS-2$
			Pattern p = Pattern.compile(regex);
			for (int i = 0; i < selectedsView.getItemCount(); i++)
				if (selectedsView.getItem(i).toString().matches(regex)) {
					selectedsView.select(i);
					count++;
				}
			System.out.println(NLS.bind(TXMUIMessages.foundP0Values, count));
		}
		catch (Exception ex) {
			System.out.println(NLS.bind(TXMUIMessages.error_invalidRegularExpressionP0, ex.getMessage()));
		}
	}
}
