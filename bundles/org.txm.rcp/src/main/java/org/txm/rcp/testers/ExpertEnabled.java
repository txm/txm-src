package org.txm.rcp.testers;

import org.eclipse.core.expressions.PropertyTester;
import org.txm.core.preferences.TBXPreferences;

public class ExpertEnabled extends PropertyTester {

	public static final String PROPERTY_NAMESPACE = "org.txm.rcp.testers"; //$NON-NLS-1$

	public static final String PROPERTY_EXPERT_ENABLED = "ExpertEnabled"; //$NON-NLS-1$

	@Override
	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {

		boolean expert = TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER);
		//System.out.println("IS expert enabled: "+expert);
		return expert;
	}
}
