package org.txm.rcp.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.Toolbox;
import org.txm.rcp.handlers.scripts.ExecuteGroovyScript;

public class Calculette extends AbstractHandler {

	public static final String ID = Calculette.class.getCanonicalName();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		String scriptRootDir = Toolbox.getTxmHomePath() + "/scripts"; //$NON-NLS-1$
		File currentRootDir = new File(scriptRootDir, "groovy/user"); //$NON-NLS-1$

		return ExecuteGroovyScript.executeScript(currentRootDir.getAbsolutePath() + "/org/txm/macro/utils/CalculetteMacro.groovy", HandlerUtil.getActivePart(event), HandlerUtil.getActiveEditor(event),
				HandlerUtil.getCurrentSelection(event));
	}
}
