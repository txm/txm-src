// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.perspective;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.console.IConsoleConstants;
import org.txm.rcp.views.corpora.CorporaView;

// TODO: Auto-generated Javadoc
/**
 * define which view are opened at TXM start up
 * 
 * @author mdecorde.
 */
public class TXMPerspective implements IPerspectiveFactory {

	/** The Constant ID. */
	public static final String ID = "org.txm.rcp.perspective.TXMPerspective"; //$NON-NLS-1$

	static IPageLayout initialLayout;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui.IPageLayout)
	 */
	@Override
	public void createInitialLayout(IPageLayout layout) {

		TXMPerspective.initialLayout = layout;
		// System.out.println("INITIAL LAYOUT="+initialLayout);
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(true);

		IFolderLayout left = layout.createFolder("left", IPageLayout.LEFT, 0.15f, editorArea); //$NON-NLS-1$
		left.addView(CorporaView.ID);
		//left.addView(Explorer.ID);

		layout.getViewLayout(CorporaView.ID).setCloseable(false);
		//layout.getViewLayout(Explorer.ID).setCloseable(true);

		IFolderLayout bottom = layout.createFolder("bottom", IPageLayout.BOTTOM, 0.75f, editorArea); //$NON-NLS-1$
		// bottom.addView(IPageLayout.ID_OUTLINE);
		bottom.addView(IConsoleConstants.ID_CONSOLE_VIEW);
		// bottom.addView("org.eclipse.ui.views.ProgressView"); //$NON-NLS-1$
		// bottom.addView(GroovyConsole.ID);
		// bottom.addView(IConsoleConstants.ID_CONSOLE_VIEW);

	}

	public static IPageLayout getInitiaLayout() {
		return initialLayout;
	}
}
