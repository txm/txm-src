// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.rcp.messages.TXMUIMessages;

/**
 * show informations of the selected TxmObject @ author mdecorde.
 */
public class ShowSelected extends AbstractHandler implements IHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@SuppressWarnings("unchecked") //$NON-NLS-1$
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			Object elem = strucSelection.getFirstElement();

			if (elem instanceof File) {
				File f = (File) elem;
				System.out.println(TXMUIMessages.fileColon);
				System.out.println(" path=" + f.getAbsolutePath()); //$NON-NLS-1$
				System.out.println(" size=" + f.length() / 1000 + " kb"); //$NON-NLS-1$ //$NON-NLS-2$
				System.out.println(" rights: read=" + f.canRead() + " write=" + f.canWrite() + " execute=" + f.canExecute()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				// creation time, last access, lastModofiedTime
				// getOwner getGroup
				try {
					Map<String, Object> attrs = Files.readAttributes(f.toPath(), "*"); //$NON-NLS-1$

					if (attrs.get("posix:owner") != null) { //$NON-NLS-1$
						System.out.println(" owner: " + attrs.get("posix:owner")); //$NON-NLS-1$ //$NON-NLS-2$
						System.out.println(" ggroup: " + attrs.get("posix:group")); //$NON-NLS-1$ //$NON-NLS-2$
					}

					System.out.println(" last access time: " + attrs.get("lastAccessTime")); //$NON-NLS-1$ //$NON-NLS-2$
					System.out.println(" last modified time: " + attrs.get("lastModifiedTime")); //$NON-NLS-1$ //$NON-NLS-2$
					System.out.println(" creation time: " + attrs.get("creationTime")); //$NON-NLS-1$ //$NON-NLS-2$

					//					
					//					for (String attr : attrs.keySet()) {
					//						System.out.println(attr+":"+attrs.get(attr));
					//					}
					//					attrs = Files.readAttributes(f.toPath(), "posix:*");
					//					for (String attr : attrs.keySet()) {
					//						System.out.println("posix: "+attr+":"+attrs.get(attr));
					//					}
					//					FileOwnerAttributeView foaw = Files.getFileAttributeView(f.toPath(), FileOwnerAttributeView.class);
					//					
					//					System.out.println("Owner: "+foaw.getOwner());
					//					System.out.println("Last modification date: "+Files.getLastModifiedTime(f.toPath()));
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				System.out.println(elem);
			}
		}
		return null;
	}

	public static void print(IStructuredSelection structSelection) {
		if (structSelection == null) System.out.println("no selection."); //$NON-NLS-1$

		for (Object elem : structSelection.toList()) {
			print(elem);
		}
	}

	public static void print(Object elem) {
		if (elem instanceof File) {
			File f = (File) elem;
			System.out.println(TXMUIMessages.fileColon);
			System.out.println(" path=" + f.getAbsolutePath()); //$NON-NLS-1$
			System.out.println(" size=" + f.length() / 1000 + " kb"); //$NON-NLS-1$ //$NON-NLS-2$
			System.out.println(" rights: read=" + f.canRead() + " write=" + f.canWrite() + " execute=" + f.canExecute()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		else {
			System.out.println(elem);
		}
	}
}
