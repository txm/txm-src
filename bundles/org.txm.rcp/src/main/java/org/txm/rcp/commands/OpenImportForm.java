// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.txm.rcp.editors.input.ImportFormEditorInput;

/**
 * Open the import form
 * 
 * @author mdecorde.
 */
public class OpenImportForm extends AbstractHandler {

	/** The lastopenedfile. */
	public static String lastopenedfile;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		File script = null;
		if (event.getParameters().containsKey("org.txm.rcp.commands.importscript")) { //$NON-NLS-1$
			String filename = event.getParameter("org.txm.rcp.commands.importscript"); //$NON-NLS-1$
			script = new File(filename);
		}
		Boolean updateMode = "true".equals(event.getParameter("org.txm.rcp.commands.importupdatemode")); //$NON-NLS-1$ //$NON-NLS-2$
		open(script, updateMode);
		return null;
	}

	/**
	 * Openfile.
	 *
	 * @param scriptname the filepath
	 */
	public static void open(File scriptname, boolean updateMode) {
		try {
			ImportFormEditorInput editorInput = new ImportFormEditorInput(scriptname);
			editorInput.setUpdateMode(updateMode);
			IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			IWorkbenchPage page = window.getActivePage();
			IEditorPart a = page.openEditor(editorInput, ImportFormEditor.ID, true);
		}
		catch (Throwable e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}
}
