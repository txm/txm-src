package org.txm.rcp.swt.dialog;

import java.io.File;

import org.txm.core.preferences.TXMPreferences;

public class LastOpened {

	public static String PREFERENCES_NODE = LastOpened.class.getCanonicalName();

	public static final String FOLDER = "FOLDER"; //$NON-NLS-1$

	public static final String FILE = "FILE"; //$NON-NLS-1$

	public static void set(String ID, String folder) {
		TXMPreferences.put(PREFERENCES_NODE, ID + FOLDER, folder);
	}

	public static void set(String ID, String folder, String file) {
		TXMPreferences.put(PREFERENCES_NODE, ID + FOLDER, folder);
		TXMPreferences.put(PREFERENCES_NODE, ID + FILE, file);
	}

	public static void set(String ID, File outfile) {
		TXMPreferences.put(PREFERENCES_NODE, ID + FOLDER, outfile.getParent());
		TXMPreferences.put(PREFERENCES_NODE, ID + FILE, outfile.getName());
	}

	public static String getFolder(String ID) {
		return TXMPreferences.getString(ID + FOLDER, PREFERENCES_NODE);
	}

	public static String getFile(String ID) {
		return TXMPreferences.getString(ID + FILE, PREFERENCES_NODE);
	}
}
