package org.txm.rcp.commands.function;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.TXMResult;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.TextDialog;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.utils.logger.Log;

public class RenameResult extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection isel = HandlerUtil.getCurrentSelection(event);
		if (!(isel instanceof IStructuredSelection)) return null;
		IStructuredSelection selection = (IStructuredSelection) isel;

		Object first = selection.getFirstElement();
		if (first instanceof TXMResult) {
			final TXMResult r = (TXMResult) first;

			final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
			TextDialog dialog = new TextDialog(window.getShell(), TXMUIMessages.bind(TXMUIMessages.RenameP0, r), TXMUIMessages.EnterTheNewNameToShowInTheCorpusView, r.getCurrentName());
			if (dialog.open() == TextDialog.OK) {
				String newName = dialog.getText();
				//if (newName.length() > 0) {

				//					if (r instanceof CorpusBuild) { // test if another corpus has the same name
				//						List<TXMResult> corpora = Toolbox.workspace.getDeepChildren(CorpusBuild.class);
				//					}
				//				
				r.setUserName(newName);
				try {
					r.autoSaveParametersFromAnnotations();
				}
				catch (Exception e) {
					Log.printStackTrace(e);
					return null;
				}
				TXMPreferences.flush(r);

				window.getShell().getDisplay().syncExec(new Runnable() {

					@Override
					public void run() {
						CorporaView.refreshObject(r);
					}
				});
				//}
			}
		}
		return null;
	}

}
