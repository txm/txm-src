package org.txm.rcp.swt.toolbar;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.txm.rcp.IImageKeys;

/**
 * A ToolItem SWT.CHECK with 2 images for the selected and not selected states
 * 
 * @author mdecorde
 *
 */
public class DropDownToolItem extends ToolItem {

	boolean selected = false;

	/**
	 * 
	 * @param parent
	 * @param title used as Text if openIconFilePath icon is not set, and as ToolTip text if openIconFilePath icon is set
	 * @param openIconFilePath
	 * @param closeIconFilePath
	 */
	public DropDownToolItem(ToolBar parent, String title, String openIconFilePath, String closeIconFilePath) {
		super(parent, SWT.DROP_DOWN | SWT.CHECK);

		if (openIconFilePath != null) {
			openIcon = IImageKeys.getImage(openIconFilePath);
			this.setImage(openIcon);
			this.setToolTipText(title);
			if (closeIconFilePath != null) {
				closeIcon = IImageKeys.getImage(closeIconFilePath);
			}
		}
		else {
			this.setText(title);
		}

		this.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				//selected = !selected;
				if (closeIcon != null) {
					if (getSelection()) {
						setImage(closeIcon);
					}
					else {
						setImage(openIcon);
					}
				}

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}

	//	public boolean getSelection() {
	//		return selected;
	//	}
	//	
	//	public void setSelection(boolean selected) {
	//		this.selected = selected;
	//	}

	@Override
	public void checkSubclass() {
	}

	protected Image openIcon;

	protected Image closeIcon;
}
