// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.utils;

import java.util.concurrent.Semaphore;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.Application;
import org.txm.rcp.JobsTimer;
import org.txm.rcp.StatusLine;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.utils.logger.Log;

/**
 * Simplified RCP Job that implements the Toolbox IProgressMonitor class to allow TBX functions to manage the Job monitor.
 * The implemented run method returns a Status.OK_STATUS or Status.CANCEL_STATUS that can be tested with the getResult() after the join method is called.
 * It also must call the runInit method to allow the user to force to stop the JobThread.
 * 
 * @author nkredens
 * @author mdecorde
 * 
 */
public abstract class JobHandler extends Job implements IProgressMonitor {

	/** The parent. */
	IProgressMonitor currentMonitor;

	protected Semaphore mySemaphore;

	protected boolean canBeInterrupted = false;

	Object result = null;

	protected boolean success = false;

	/**
	 * Create a job in RCP scheduler.
	 * 
	 * @param title
	 *            the title
	 */
	public JobHandler(String title) {
		this(title, true);
	}

	private SubMonitor subMonitor = null;

	/**
	 * Do something here
	 * 
	 * Implement onJobFailure() to manage error during job execution
	 * 
	 * @param monitor
	 * @return
	 * @throws Exception
	 * @throws Throwable
	 */
	protected abstract IStatus _run(SubMonitor monitor) throws Throwable;

	@Override
	protected final IStatus run(IProgressMonitor monitor) {

		IStatus returnStatus = Status.CANCEL_STATUS;
		try {
			// convert the monitor into sub-monitor
			subMonitor = SubMonitor.convert(monitor, this.getName(), 100);

			this.runInit(subMonitor);

			JobsTimer.start();

			returnStatus = _run(subMonitor);
		} // for user direct canceling
		catch (ThreadDeath | InterruptedException e) {
			onJobFailure();
			Log.warning("Operation canceled by user.");
			return Status.CANCEL_STATUS;
		}
		catch (OutOfMemoryError e) {
			onJobFailure();
			Log.warning(NLS.bind("The operation could not end correctly because of a memory error: {0}.", e));
			Log.warning("You can raise the available memory in the TXM > Advanced preference page.");
			return Status.CANCEL_STATUS;
		}
		catch (Throwable e) {
			onJobFailure();
			Log.warning(NLS.bind("Error during ''{0}'': {1}", this.getName(), e.getMessage()));
			Log.printStackTrace(e);
			return Status.CANCEL_STATUS;
		}
		finally {

			if (subMonitor != null) subMonitor.done();

			JobsTimer.stopAndPrint();
		}

		return returnStatus == null ? Status.CANCEL_STATUS : returnStatus; // avoid error when a null status has been returned
	}

	/**
	 * Re-implement this to manage error after a Job error
	 */
	protected void onJobFailure() {

	}

	// static final ISchedulingRule RULE = new ISchedulingRule() {
	//
	// @Override
	// public String toString() {
	// return "MAIN RULE";
	// }
	//
	// @Override
	// public boolean isConflicting(ISchedulingRule rule) {
	// return rule == this;
	// }
	//
	// @Override
	// public boolean contains(ISchedulingRule rule) {
	// return rule == this;
	// }
	// };

	/**
	 * Instantiates a new job handler.
	 * 
	 * @param title the title
	 * @param show to force displaying the dialog at start up
	 */
	public JobHandler(String title, boolean show) {
		super(title);
		this.setUser(show);
		// this.setRule(RULE);
	}

	final static public String TXM_FAMILY = "TXM"; //$NON-NLS-1$

	@Override
	public boolean belongsTo(Object family) {
		return TXM_FAMILY.equals(family);
	}

	/**
	 * You must set the current monitor to be available to manage the process
	 * progress
	 * 
	 * @param monitor
	 */
	public void setCurrentMonitor(IProgressMonitor monitor) {
		currentMonitor = monitor;
	}

	public void beginTask(String name, long totalWork) {
		if (currentMonitor != null) {
			currentMonitor.beginTask(name, (int) totalWork);
		}
	}

	@Override
	public void subTask(String name) {
		if (currentMonitor != null) {
			currentMonitor.subTask(name);
		}
	}


	@Override
	public void worked(int amount) {
		if (currentMonitor != null) {
			currentMonitor.worked(amount);
		}
	}

	public void worked(int amount, String message) {
		if (currentMonitor != null) {
			currentMonitor.worked(amount);
			currentMonitor.subTask(message);
		}
	}

	public void worked() {
		if (currentMonitor != null) {
			currentMonitor.worked(1);
		}
	}

	@Override
	public boolean isCanceled() {
		if (currentMonitor != null) {
			return currentMonitor.isCanceled();
		}
		return false;
	}

	@Override
	public void done() {
		if (currentMonitor != null) {
			currentMonitor.done();
		}

	}

	public void acquireSemaphore() {
		try {
			this.mySemaphore.acquire();
		}
		catch (InterruptedException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	public boolean tryAcquireSemaphore() {
		return this.mySemaphore.tryAcquire();
	}

	public void releaseSemaphore() {
		this.mySemaphore.release();
		if (isCanceled()) {
			canceling();
		}
	}

	/*
	 * use this function to do something with SWT objects
	 */
	/**
	 * Sync exec.
	 * 
	 * @param runable
	 *            the runable
	 */
	public void syncExec(Runnable runable) {
		if (Display.getDefault() != null && !Application.isHeadLess()) {
			Display.getDefault().syncExec(runable);
		}
		else {
			runable.run();
		}
	}

	public void startJob() {
		startJob(true);
	}

	/**
	 * Start job.
	 */
	public void startJob(boolean modal) {
		// if (modal) { // this will deadlock the job if the jobs needs to work on the RCP workspace files
		// this.setRule(org.eclipse.core.resources.ResourcesPlugin.getWorkspace().getRoot());// set modal job
		// }
		this.setUser(modal);// show popup
		this.schedule();// start
	}

	/**
	 * Allows the job to be interrupted
	 * 
	 * @param monitor
	 */
	private void runInit(IProgressMonitor monitor) {
		this.mySemaphore = new Semaphore(1);
		this.currentMonitor = monitor;
		this.canBeInterrupted = true;
	}

	/**
	 * Action when user requests for interruption
	 */
	@Override
	protected void canceling() {
		if (!this.canBeInterrupted) // Prevent thread system interruption (ThreadDeath not handled by the programmer)
			return;

		try {
			// Log.info(TXMUIMessages.canceling_2 + " " + this.getName() + ".");
			this.syncExec(new Runnable() {

				@Override
				public void run() {
					if (isUser()) {
						StatusLine.setMessage(TXMUIMessages.bind(TXMUIMessages.cancelingP0, getName()));
						Log.warning(TXMUIMessages.bind(TXMUIMessages.cancelingP0, getName()));
					}
				}
			});
			if (currentMonitor != null) this.currentMonitor.setTaskName(TXMUIMessages.canceling);
			this.mySemaphore.acquire();
			this.getThread().stop();

		}
		catch (Exception e) {
			Log.printStackTrace(e);
			Log.severe(TXMUIMessages.failedToCancel + this.getName());
		}
		finally {
			super.canceling();
		}
	}

	/**
	 * can be used to store the job result after join() has been called
	 * 
	 * @return
	 */
	public Object getResultObject() {
		return result;
	}

	/**
	 * can be used to store the job result after join() has been called
	 * 
	 */
	public void setResultObject(Object result) {
		this.result = result;
	}

	@Override
	public void beginTask(String name, int totalWork) {
		if (currentMonitor != null) currentMonitor.beginTask(name, totalWork);
	}

	@Override
	public void internalWorked(double work) {
		if (currentMonitor != null) currentMonitor.internalWorked(work);
	}

	@Override
	public void setCanceled(boolean value) {
		this.cancel();
	}

	@Override
	public void setTaskName(String name) {
		if (currentMonitor != null) currentMonitor.setTaskName(name);
	}

	/**
	 * All TXM jobs are done if there is no more TXM_FAMILY jobs
	 * 
	 * @return true if all TXM jobs are done
	 */
	public static boolean areAllTXMJobsDone() {
		Job[] jobs = JobHandler.getJobManager().find(JobHandler.TXM_FAMILY);
		return jobs == null || jobs.length == 0;
	}
}
