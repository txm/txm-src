package org.txm.rcp.editors.imports.sections;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.txm.objects.Project;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.txm.rcp.messages.TXMUIMessages;

public class TRSSection extends ImportEditorSection {

	String ID = TRSSection.class.getSimpleName();

	private static final int SECTION_SIZE = 1;

	Button displayLocutorsButton;

	Button indexInterviewerButton;

	Button createPagesForSectionsButton;

	Button indexTranscriberMetadataButton;

	/**
	 * 
	 * @param toolkit2
	 * @param form2
	 * @param parent
	 */
	public TRSSection(ImportFormEditor editor, FormToolkit toolkit2, ScrolledForm form2, Composite parent, int style) {

		super(editor, toolkit2, form2, parent, style, TXMUIMessages.Transcriptions);

		TableWrapLayout layout = new TableWrapLayout();
		layout.makeColumnsEqualWidth = true;
		layout.numColumns = 1;
		this.section.setLayout(layout);
		this.section.setLayoutData(getSectionGridData(SECTION_SIZE));
		this.section.setEnabled(false);

		this.section.addExpansionListener(new ExpansionAdapter() {

			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.layout(true);
			}
		});

		// filesection.setDescription("Select how to find source files");
		Composite sectionClient = toolkit.createComposite(this.section);
		TableWrapLayout slayout = new TableWrapLayout();
		slayout.numColumns = 4;
		sectionClient.setLayout(slayout);
		this.section.setClient(sectionClient);

		// build text edition or not button
		displayLocutorsButton = toolkit.createButton(sectionClient, TXMUIMessages.TRSSection_1, SWT.CHECK);
		TableWrapData gdata2 = getButtonLayoutData();
		gdata2.colspan = 4; // one line
		displayLocutorsButton.setLayoutData(gdata2);

		// build text edition or not button
		indexInterviewerButton = toolkit.createButton(sectionClient, TXMUIMessages.IndexInterviewerSpeech, SWT.CHECK);
		gdata2 = getButtonLayoutData();
		gdata2.colspan = 4; // one line
		indexInterviewerButton.setLayoutData(gdata2);

		// build text edition or not button
		createPagesForSectionsButton = toolkit.createButton(sectionClient, TXMUIMessages.CreatePagesForSections, SWT.CHECK);
		gdata2 = getButtonLayoutData();
		gdata2.colspan = 4; // one line
		createPagesForSectionsButton.setLayoutData(gdata2);

		// build text edition or not button
		indexTranscriberMetadataButton = toolkit.createButton(sectionClient, TXMUIMessages.IndexTranscriberTranscriptionMetadata, SWT.CHECK);
		gdata2 = getButtonLayoutData();
		gdata2.colspan = 4; // one line
		indexTranscriberMetadataButton.setLayoutData(gdata2);
	}

	@Override
	public void updateFields(Project project) {
		if (this.section.isDisposed()) return;
		if (project == null) return;

		IEclipsePreferences customNode = project.getImportParameters();
		displayLocutorsButton.setSelection(customNode.getBoolean("display_locutors", true)); //$NON-NLS-1$
		indexInterviewerButton.setSelection(customNode.getBoolean("indexInterviewer", true)); //$NON-NLS-1$
		createPagesForSectionsButton.setSelection(customNode.getBoolean("create_section_pages", true)); //$NON-NLS-1$
		indexTranscriberMetadataButton.setSelection(customNode.getBoolean("ignoreTranscriberMetadata", true)); //$NON-NLS-1$
	}

	@Override
	public boolean saveFields(Project project) {
		if (this.section != null && !this.section.isDisposed()) {

			IEclipsePreferences customNode = project.getImportParameters();
			customNode.putBoolean("display_locutors", displayLocutorsButton.getSelection()); //$NON-NLS-1$
			customNode.putBoolean("indexInterviewer", indexInterviewerButton.getSelection()); //$NON-NLS-1$
			customNode.putBoolean("create_section_pages", createPagesForSectionsButton.getSelection()); //$NON-NLS-1$
			customNode.putBoolean("ignoreTranscriberMetadata", indexTranscriberMetadataButton.getSelection()); //$NON-NLS-1$
		}
		return true;
	}

	@Override
	public boolean checkFields() {
		return true;
	}

	@Override
	public int getSectionSize() {
		return SECTION_SIZE;
	}
}
