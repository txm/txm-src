// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.Toolbox;
import org.txm.utils.logger.Log;

/**
 * Open the internal browser of TXM with a file @ author mdecorde.
 */
public class OpenManual extends AbstractHandler {

	public static final HashMap<String, String> files = new HashMap<String, String>();

	public static final String default_locale = "fr"; //$NON-NLS-1$
	static {
		files.put("fr", "Manuel de TXM 0.8 FR.pdf"); //$NON-NLS-1$ //$NON-NLS-2$
		files.put("en", "TXM Manual 0.8 EN.pdf"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/** The Constant ID. */
	public final static String ID = "org.txm.rcp.commands.OpenWelcomePage"; //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		open();
		return null;
	}

	public static void open() {
		try {
			String locale = Locale.getDefault().getLanguage();
			String installdir = Toolbox.getInstallDirectory();

			String file = files.get(locale);
			if (file == null) file = files.get(default_locale);

			File man = new File(installdir, "doc/" + file); //$NON-NLS-1$
			if (!man.exists()) {
				man = new File(installdir, "doc/" + files.get(default_locale)); //$NON-NLS-1$
			}

			if (man.exists()) {
				Log.info("Opening file: " + man); //$NON-NLS-1$
				try {
					Desktop.getDesktop().open(man);
				}
				catch (IOException e) {
					Log.severe("Error: manual not opened: " + e); //$NON-NLS-1$
					Log.printStackTrace(e);
				}
			}
			else {
				Log.severe("Error: manual not found: " + man.getAbsolutePath()); //$NON-NLS-1$
			}
		}
		catch (Throwable e) {
			Log.severe("Error: manual not opened: " + e); //$NON-NLS-1$
			Log.printStackTrace(e);
		}
	}
}
