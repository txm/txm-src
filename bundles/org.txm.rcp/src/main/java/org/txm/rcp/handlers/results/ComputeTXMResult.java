package org.txm.rcp.handlers.results;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.txm.rcp.editors.TXMEditorToolBar;

/**
 * Calls TXMEditor.compute() on the editor of the TXMEditorToolBar that has triggered the event.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ComputeTXMResult extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Object trigger = event.getTrigger();

		if (trigger instanceof Event) {
			Event e = (Event) trigger;
			if (e.widget instanceof ToolItem) {
				ToolItem ti = (ToolItem) e.widget;
				ToolBar parent = ti.getParent();
				if (parent instanceof TXMEditorToolBar) {
					return ((TXMEditorToolBar) parent).getEditorPart().compute(true);
				}
			}
		}
		return null;
	}
	
}
