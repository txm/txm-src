// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.rcp.handlers.export;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.results.TXMResult;
import org.txm.rcp.StatusLine;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.utils.logger.Log;

/**
 * Import parameters of a result by calling the function toTxt(File f) then show
 * the result in the text editor
 *  
 * @author mdecorde.
 */
@Deprecated
public class ImportResultParameters extends AbstractHandler {

	/**
	 * Export a TXM result parameters in the preferences format
	 *
	 * @param event the event
	 * @return the object
	 * @throws ExecutionException the execution exception
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		final Object s = selection.getFirstElement();
		if (!(s instanceof TXMResult)) {
			return null;
		}
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

		FileDialog dialog = new FileDialog(shell, SWT.OPEN);

		String extensions[] = { "*.parameters" }; //$NON-NLS-1$
		dialog.setFilterExtensions(extensions);

		if (LastOpened.getFile(ImportResultParameters.class.getName()) != null) {
			dialog.setFilterPath(LastOpened.getFolder(ImportResultParameters.class.getName()));
		}

		if (dialog.open() != null) {
			StatusLine.setMessage(TXMUIMessages.exportingResults);
			String filepath = dialog.getFilterPath() + "/" + dialog.getFileName(); //$NON-NLS-1$
			if (!(filepath.endsWith(extensions[0].substring(1)))) {
				filepath += extensions[0].substring(1);
			}

			final File outfile = new File(filepath);
			LastOpened.set(ImportResultParameters.class.getName(), outfile.getParent(), outfile.getName());
			try {
				outfile.createNewFile();
			}
			catch (IOException e1) {
				System.err.println(NLS.bind(TXMUIMessages.exportColonCantCreateFileP0ColonP1, outfile, e1));
			}
			if (!outfile.canWrite()) {
				System.out.println(NLS.bind(TXMUIMessages.impossibleToReadP0, outfile));
				return null;
			}
			if (!outfile.isFile()) {
				System.out.println("Error: " + outfile + " is not a file"); //$NON-NLS-1$ //$NON-NLS-2$
				return null;
			}

			JobHandler jobhandler = new JobHandler(TXMUIMessages.exportingResults) {

				@Override
				protected IStatus _run(SubMonitor monitor) {

					try {
						monitor.beginTask(TXMUIMessages.exporting, 100);

						TXMResult parent = (TXMResult) s;
						TXMResult r = parent.importResult(outfile);
						CorporaView.refreshObject(r);
						if (r != null) {
							System.out.println(NLS.bind(TXMUIMessages.P0ResultsImportedFromTheP1File, r, outfile));
						}
					}
					catch (Exception e) {
						Log.warning(NLS.bind(TXMUIMessages.failedToExportResultP0ColonP1, s, e));
						throw e;
					}
					return Status.OK_STATUS;
				}
			};

			jobhandler.startJob();
		}
		return null;
	}
}
