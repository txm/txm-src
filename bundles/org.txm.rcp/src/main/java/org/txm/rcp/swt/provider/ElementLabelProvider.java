package org.txm.rcp.swt.provider;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.w3c.dom.Element;

public class ElementLabelProvider extends LabelProvider implements ITableLabelProvider {

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
	 * 
	 * @param element
	 * @param columnIndex
	 */
	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		if (element != null && element instanceof Element) {
			Element elem = (Element) element;
			if (columnIndex < elem.getAttributes().getLength())
				return elem.getAttributes().item(columnIndex).getNodeValue();
			return "#idx too high"; //$NON-NLS-1$
		}
		return "#not Element"; //$NON-NLS-1$
	}
}
