package org.txm.rcp.swt.widget.parameters;

import java.io.File;
import java.util.Date;

import org.kohsuke.args4j.Option;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;

public class AtestClass {

	@Option(name = "file", usage = "a file", widget = "File", required = true, def = "aFile.ext")
	File file;

	@Option(name = "dir", usage = "a directory", widget = "Folder", def = "TEMP")
	File dir;

	@Option(name = "ii", usage = "an integer", widget = "Integer", def = "42")
	Integer ii;

	@Option(name = "f", usage = "a float", widget = "Float", def = "42.42")
	Float f;

	@Option(name = "shs", usage = "a one line string", widget = "String", def = "default value")
	String shs;

	@Option(name = "ls", usage = "a very long string with carriage return and stuffs", widget = "Text", def = "default\nvalue")
	String ls;

	@Option(name = "q", usage = "a CQL", widget = "Query", def = "\"je\"")
	CQLQuery q;

	@Option(name = "date", usage = "a date", widget = "Date", def = "1984-09-01")
	Date date;

	@Option(name = "bool", usage = "a boolean", widget = "Boolean", def = "true")
	Boolean bool;

	public AtestClass() {

	}

	@Override
	public String toString() {
		return "file=" + file + " dir=" + dir + " ii=" + ii; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}
}
