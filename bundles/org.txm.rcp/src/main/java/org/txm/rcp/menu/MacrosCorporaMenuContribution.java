package org.txm.rcp.menu;

import java.io.File;

import org.txm.Toolbox;

public class MacrosCorporaMenuContribution extends MacrosMenuContribution {

	public MacrosCorporaMenuContribution() {
		super();
	}

	public MacrosCorporaMenuContribution(String id) {
		super(id);
	}

	public File getMacroDirectory() {
		//create the menu item
		String w = Toolbox.getTxmHomePath();
		if (w == null || w.length() == 0) return null;

		return new File(w, "scripts/groovy/user/org/txm/macro/ui/menu/window"); //$NON-NLS-1$
	}
}
