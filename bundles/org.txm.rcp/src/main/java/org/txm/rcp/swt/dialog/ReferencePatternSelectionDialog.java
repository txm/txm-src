// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.dialog;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.searchengine.cqp.corpus.Property;

/**
 * The dialog box called to select the reference pattern properties and sprintf pattern.
 * 
 * @author mdecorde
 */
public class ReferencePatternSelectionDialog extends MultiplePropertySelection2ListsDialog<Property> {

	Text patternText;

	Label patternLabel;

	String pattern;

	public ReferencePatternSelectionDialog(Shell shell, List<Property> availableProperties, List<Property> selectedProperties, String pattern, Control source) {
		super(shell, availableProperties, selectedProperties, -1, source);
		this.pattern = pattern;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite mainArea = (Composite) super.createDialogArea(parent);

		GLComposite line = new GLComposite(mainArea, SWT.NONE, "motif line"); //$NON-NLS-1$
		line.setLayoutData(new GridData(SWT.WRAP, SWT.WRAP, true, true, 4, 1));
		line.getLayout().numColumns = 2;
		line.getLayout().horizontalSpacing = 3;

		// "properties"
		patternLabel = new Label(line, SWT.NONE);
		patternLabel.setText(TXMUIMessages.pattern);
		patternLabel.setToolTipText(TXMUIMessages.YouNeedToSetAsManyPercentSPrintfMarksAsSelectedProperties);
		patternLabel.setLayoutData(new GridData(SWT.WRAP, SWT.CENTER, true, true));

		// "properties"
		patternText = new Text(line, SWT.BORDER);
		if (pattern != null) {
			patternText.setText(pattern);
		}
		patternText.setToolTipText(TXMUIMessages.YouNeedToSetAsManyPercentSPrintfMarksAsSelectedProperties);
		GridData gdata = new GridData(SWT.WRAP, SWT.WRAP, true, true);
		gdata.widthHint = 200;
		gdata.minimumWidth = 50;
		patternText.setLayoutData(gdata);
		patternText.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				if (validatePattern(patternText.getText())) {
					pattern = patternText.getText();
				}
			}
		});

		validatePattern(this.pattern);
		
		selectedPropertiesView.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent event) {
				validatePattern(pattern);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});
		availablePropertiesView.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent event) {
				validatePattern(pattern);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});

		return mainArea;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(TXMUIMessages.referencesDisplayOptions);
	}

	/**
	 * Reload.
	 */
	@Override
	public void reload() {
		super.reload();
		if (patternText != null) {
			validatePattern(this.pattern);
		}
	}
	
	/**
	 * Reload.
	 */
	@Override
	protected void okPressed() {
		if (validatePattern(this.pattern)) {
			super.okPressed();
		}
	}

	public boolean validatePattern(String pattern) {
		if (pattern == null) return true;
		if (pattern.length() == 0) return true;

		String s = pattern;
		int nPercents = 0;
		int index = s.indexOf("%"); //$NON-NLS-1$
		while (index >= 0) {
			s = s.substring(index + 1);
			index = s.indexOf("%"); //$NON-NLS-1$
			nPercents++;
		}

		if (nPercents != this.selectedPropertiesView.getItemCount()) {
			Color a = new Color(patternText.getDisplay(), 255, 0, 0);
			patternText.setBackground(a);
			String s2 = StringUtils.join(this.selectedPropertiesView.getItems(), ", "); //$NON-NLS-1$
			patternText.setToolTipText(NLS.bind(TXMUIMessages.ThereIsNotEnoughPercentMarksInP0ForTheP1Properties, pattern, s2));
			return false;
		}
		else {
			if (patternText.getParent() != null && patternText.getParent().getBackground() != null) {
				patternText.setBackground(patternText.getParent().getBackground());
			}
			return true;
		}
	}
	
	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		if (!patternText.isDisposed()) {
			patternText.setText(pattern);
			this.pattern = pattern;
		}
	}
}
