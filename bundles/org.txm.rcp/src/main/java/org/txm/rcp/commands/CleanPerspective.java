package org.txm.rcp.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.internal.Workbench;
import org.eclipse.ui.internal.registry.PerspectiveDescriptor;
import org.eclipse.ui.internal.registry.PerspectiveRegistry;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.SWTEditorsUtils;

public class CleanPerspective extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) {
		if (MessageDialog.openConfirm(HandlerUtil.getActiveShell(event), TXMUIMessages.resetPerspective, TXMUIMessages.thisWillRestoreTheViewAndCloseEditors)) {
			reset(event, true, false);
		}
		return null;
	}

	public static void reset(ExecutionEvent event, boolean perspective, boolean parts) {
		//System.out.println("clean perspective");

		IWorkbenchPage page;
		if (event != null) {
			IWorkbenchPart part = HandlerUtil.getActivePart(event);
			page = part.getSite().getPage();
		}
		else {
			page = Workbench.getInstance().getActiveWorkbenchWindow().getActivePage();
		}

		if (page == null) return;

		SWTEditorsUtils.cleanAreaModel();


		if (parts) {
			IEditorReference[] editors = page.getEditorReferences();
			for (IEditorReference er : editors) {
				page.closeEditor(er.getEditor(false), false);
			}
		}

		if (perspective) {
			IPerspectiveDescriptor descriptor = page.getPerspective();
			if (descriptor != null) {
				boolean offerRevertToBase = false;
				if (descriptor instanceof PerspectiveDescriptor) {
					PerspectiveDescriptor desc = (PerspectiveDescriptor) descriptor;
					offerRevertToBase = desc.isPredefined() && desc.hasCustomDefinition();
				}

				if (offerRevertToBase) {

					PerspectiveRegistry reg = (PerspectiveRegistry) PlatformUI.getWorkbench().getPerspectiveRegistry();
					reg.revertPerspective(descriptor);
				}
				page.resetPerspective();
			}
		}
		return;
	}
}
