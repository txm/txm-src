package org.txm.rcp.editors;

import java.util.ArrayList;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorPart;
import org.txm.core.results.TXMResult;
import org.txm.rcp.utils.JobHandler;

public interface ITXMResultEditor<T extends TXMResult> extends IEditorPart {

	public T getResult();

	/**
	 * Computes the TXMResult if all required parameters are set then refreshes the editor UI.
	 * Also loads and saves some parameters from result to Widgets fields and from Widgets fields to result.
	 * 
	 * @param update force the editor to update its result parameters
	 */
	public JobHandler compute(boolean update);

	public void refresh(boolean update) throws Exception;

	/**
	 * 
	 * @return the editor main composite
	 */
	public Composite getContainer();

	public void close();

	/**
	 * Closes the editor from the UI thread and deletes or not the linked result.
	 */
	public void close(boolean deleteResult);

	/**
	 * lock/unlock the editor's result.
	 * 
	 * @param locked
	 */
	public void setLocked(boolean locked);

	public ArrayList<? extends ITXMResultEditor<TXMResult>> getLinkedEditors();

	public void removeLinkedEditor(ITXMResultEditor<TXMResult> editor);

	public void addLinkedEditor(ITXMResultEditor<TXMResult> editor);

	public <E extends ITXMResultEditor<TXMResult>> E getLinkedEditor(Class<E> editorClass);

	public <E extends ITXMResultEditor<TXMResult>> ArrayList<E> getLinkedEditors(Class<E> editorClass);

	/**
	 * 
	 * @return the current result if visible, if not its first visible parent or NULL if none are visible
	 */
	public TXMResult getFirstVisibleResult();
}
