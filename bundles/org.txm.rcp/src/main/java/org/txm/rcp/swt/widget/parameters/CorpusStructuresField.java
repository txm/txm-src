package org.txm.rcp.swt.widget.parameters;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.kohsuke.args4j.NamedOptionDef;
import org.txm.rcp.swt.widget.MultipleSelectionCombo;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnit;


/**
 * Allow to select one or several structural units name of the selected corpus in the corpora view
 * 
 * @author mdecorde
 *
 */
public class CorpusStructuresField extends LabelField {

	MultipleSelectionCombo dt;

	public CorpusStructuresField(Composite parent, int style, NamedOptionDef parameter) {
		super(parent, style, parameter);

		String[] values = {};
		int[] selection = {};

		Object sel = CorporaView.getFirstSelectedObject();
		if ((sel instanceof CQPCorpus)) {
			CQPCorpus c = (CQPCorpus) sel;
			List<StructuralUnit> l;
			try {
				l = new ArrayList<StructuralUnit>(c.getOrderedStructuralUnits());
				for (int i = 0; i < l.size(); i++) {
					String name = l.get(i).getName();
					if ("text".equals(name) || "txmcorpus".equals(name)) { //$NON-NLS-1$ //$NON-NLS-2$
						l.remove(i);
						i--;
					}
				}

				values = new String[l.size()];
				for (int i = 0; i < l.size(); i++) {
					values[i] = l.get(i).getName();
				}
				if (values.length > 0) {
					selection = new int[1];
					selection[0] = 0;
				}
			}
			catch (CqiClientException e) {
				e.printStackTrace();
			}
		}

		dt = new MultipleSelectionCombo(this, values, selection, SWT.NONE);
		dt.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));

		resetToDefault();
	}

	public String getWidgetValue() {
		return dt.getText();
	}

	public void setDefault(Object text) {
		dt.setText("" + text); //$NON-NLS-1$
	}

	@Override
	public void resetToDefault() {
		dt.setText(getWidgetDefault());
	}
}
