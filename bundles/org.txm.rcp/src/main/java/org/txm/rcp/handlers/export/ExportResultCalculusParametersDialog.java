package org.txm.rcp.handlers.export;

import java.io.File;
import java.util.Calendar;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.txm.Toolbox;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.TXMResult;
import org.txm.rcp.messages.TXMUIMessages;

/**
 * 
 * Allows to select the file to create and select elements to include in the binary.
 * 
 */
public class ExportResultCalculusParametersDialog extends Dialog {

	private File zipFile;

	private TXMResult result;

	private Text textField;

	private Button selectButton;

	private Button exportChildrenButton;

	private Button exportParentsButton;

	private boolean exportParents;

	private boolean exportChildren;


	/**
	 * 
	 * @param parentShell
	 */
	public ExportResultCalculusParametersDialog(Shell parentShell, TXMResult result) {
		super(parentShell);
		this.result = result;
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(NLS.bind(TXMUIMessages.exportingCalculusP0, result.getName()));
		newShell.setMinimumSize(400, 300);
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		Composite comp = (Composite) super.createDialogArea(parent);

		GridLayout layout = (GridLayout) comp.getLayout();
		layout.numColumns = 3;

		Label l = new Label(comp, SWT.LEFT);
		l.setText(TXMUIMessages.outputFile);
		l.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false));

		textField = new Text(comp, SWT.SINGLE | SWT.BORDER);
		textField.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		String previousPath = TXMPreferences.getString("path", ExportResultCalculusParametersDialog.class.getName()); //$NON-NLS-1$
		if (previousPath == null || previousPath.length() == 0) {
			previousPath = System.getProperty("user.home"); // //$NON-NLS-1$
		}
		textField.setText(previousPath + File.separator + result.getProject().getName().toUpperCase() + "-" + result.getResultType() + "-" + result.getValidFileName() + "-" //$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$//$NON-NLS-4$
				+ Toolbox.dateformat.format(Calendar.getInstance().getTime()) + ".txmcmd");     //$NON-NLS-1$

		selectButton = new Button(comp, SWT.PUSH);
		selectButton.setText("..."); //$NON-NLS-1$
		selectButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(selectButton.getShell(), SWT.SAVE);

				String[] exts = { "*.txmcmd" }; //$NON-NLS-1$ //$NON-NLS-2$
				dialog.setFilterExtensions(exts);
				String path = textField.getText();
				if (path != null && path.length() > 0) {
					File p = new File(path);
					if (p.isDirectory()) {
						dialog.setFilterPath(textField.getText());
					}
					else {
						dialog.setFilterPath(p.getParent());
						dialog.setFileName(p.getName());
					}
				}

				path = dialog.open();
				if (path != null) {
					textField.setText(path);
					File dir = new File(path).getParentFile();
					getButton(IDialogConstants.OK_ID).setEnabled(dir.exists());
					TXMPreferences.put(ExportResultCalculusParametersDialog.class.getName(), "path", dir.getAbsolutePath()); //$NON-NLS-1$
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		l = new Label(comp, SWT.LEFT);
		l.setText(TXMUIMessages.options);
		l.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 3, 1));

		exportParentsButton = new Button(comp, SWT.CHECK);
		exportParentsButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 3, 1));
		exportParentsButton.setText("Also export result parents hierarchy"); //$NON-NLS-1$

		exportChildrenButton = new Button(comp, SWT.CHECK);
		exportChildrenButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 3, 1));
		exportChildrenButton.setText("Also export result children"); //$NON-NLS-1$

		return comp;
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected void buttonPressed(int buttonId) {

		if (buttonId == Dialog.OK) {
			if (textField.getText().endsWith(".txmcmd")) {
				zipFile = new File(textField.getText());
			} else {
				zipFile = new File(textField.getText()+ ".txmcmd");
			}
			exportParents = exportParentsButton.getSelection();
			exportChildren = exportChildrenButton.getSelection();
		}
		else {
			zipFile = null;
			exportParents = false;
			exportChildren = false;
		}
		super.buttonPressed(buttonId);
	}

	/**
	 * 
	 * @return the ZIP file to create
	 */
	public File getZipFile() {
		return zipFile;
	}

	/**
	 * 
	 * @return true if the result children must be exported
	 */
	public boolean getExportChildren() {
		return exportChildren;
	}

	/**
	 * 
	 * @return true if the result parents must be exported
	 */
	public boolean getExportParents() {
		return exportParents;
	}
}
