package org.txm.rcp.editors.listeners;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Spinner;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.preferences.RCPPreferences;

/**
 * A selection listener that can be added to editors widgets and that calls the TXMEditor.compute() method.
 * Also responsible of setting the editor as dirty if auto-computing is disabled.
 *
 * @author sjacquot
 *
 */
public class ComputeSelectionListener extends BaseAbstractComputeListener implements SelectionListener, ISelectionChangedListener {

	/**
	 * If autocompute is set to true then the computing will be done even if the global auto-computing preference is false.
	 *
	 * @param editor
	 * @param autoCompute
	 */
	public ComputeSelectionListener(TXMEditor<? extends TXMResult> editor, boolean autoCompute) {
		super(editor, autoCompute);
	}

	/**
	 *
	 * @param editor
	 */
	public ComputeSelectionListener(TXMEditor<? extends TXMResult> editor) {
		this(editor, false);
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		// this method is called on a Spinner even when a key is pressed, that's not what we want, so this test breaks the behavior
		if ((this.autoCompute
				|| RCPPreferences.getInstance().getBoolean(RCPPreferences.AUTO_UPDATE_EDITOR))
				&& (!(e.getSource() instanceof Spinner) || (e.stateMask & SWT.BUTTON_MASK) != 0)) {
			this.editor.compute(true);
		}
		else {
			this.editor.setDirty(true);
		}
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		// to skip programmatically setSelection() calls on some widgets or if auto-compute preference is false
		if (!mustIgnoreEvent(event)) {
			if ((this.autoCompute || RCPPreferences.getInstance().getBoolean(RCPPreferences.AUTO_UPDATE_EDITOR))) {
				this.editor.compute(true);
			}
			else {
				this.editor.setDirty(true);
			}
		}
	}

}
