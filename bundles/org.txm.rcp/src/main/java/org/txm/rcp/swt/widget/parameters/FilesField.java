package org.txm.rcp.swt.widget.parameters;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Text;
import org.kohsuke.args4j.NamedOptionDef;

public class FilesField extends LabelField {

	Text t;

	public FilesField(Composite parent, int style, NamedOptionDef parameter) {
		super(parent, style, parameter);

		this.setLayout(new GridLayout(3, false)); // has 3 cells

		t = new Text(this, SWT.SINGLE | SWT.BORDER);
		GridData gdata = new GridData(SWT.FILL, SWT.LEFT, true, false);
		gdata.widthHint = 400;
		t.setLayoutData(gdata);
		resetToDefault();

		Button b = new Button(this, SWT.PUSH);
		b.setText("..."); //$NON-NLS-1$
		b.setToolTipText("Select existing files"); //$NON-NLS-1$
		b.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				String value = null;
				FileDialog d = new FileDialog(FilesField.this.getShell(), SWT.MULTI);

				if (parameter.metaVar() != null) {
					d.setFilterExtensions(parameter.metaVar().split(",")); //$NON-NLS-1$
				}

				try {
					File f = new File(t.getText()).getCanonicalFile();
					//System.out.println(f);
					d.setFilterPath(f.getParent());
					d.setFileName(f.getName());
				}
				catch (IOException e1) {
					// TODO Auto-generated catch block
					org.txm.utils.logger.Log.printStackTrace(e1);
				}

				value = d.open();
				String p = d.getFilterPath();
				if (value != null) t.setText(p + "/" + StringUtils.join(d.getFileNames(), File.pathSeparator + p + "/")); //$NON-NLS-1$ //$NON-NLS-2$
			}
		});
	}

	public File[] getWidgetValue() {
		String value = t.getText();
		String[] paths = value.split(File.pathSeparator);
		File[] files = new File[paths.length];
		for (int i = 0; i < paths.length; i++) {
			files[i] = new File(paths[i]);
		}
		return files;
	}


	public void setDefault(Object text) {
		//		Class<? extends Object> c = text.getClass();
		//		boolean b1 = text instanceof Object[];
		//		b1 = text instanceof File[];
		//		b1 = text instanceof String[];
		//		b1 = text instanceof File;
		if (text instanceof File[]) {
			t.setText(StringUtils.join((File[]) text, File.pathSeparator));
		}
		else {
			t.setText("" + text); //$NON-NLS-1$
		}
	}

	@Override
	public void resetToDefault() {
		t.setText(getWidgetDefault());
	}
}
