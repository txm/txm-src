// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.txm.Toolbox;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.widget.PropertiesSelector;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.core.functions.summary.Summary;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;

/**
 * Display the internal structures hierarchy of a MainCorpus
 * 
 * @author mdecorde
 */
public class SummaryView extends ViewPart {

	MainCorpus selectedCorpus;

	List<StructuralUnitProperty> selectedProps;

	Summary summary;

	/** The tv. */
	TreeViewer tv;

	Combo corpusCombo;

	PropertiesSelector<StructuralUnitProperty> propsArea;


	/** The ID. */
	public static String ID = SummaryView.class.getName();

	/**
	 * Instantiates a new summary view.
	 */
	public SummaryView() {
	}

	/**
	 * Refresh.
	 */
	public static void refresh() {
		SummaryView baseView = (SummaryView) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findView(SummaryView.ID);
		if (baseView != null) {
			baseView.tv.refresh();
		}
	}

	/**
	 * Reload.
	 */
	public static void reload() {
		SummaryView baseView = (SummaryView) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findView(SummaryView.ID);
		if (baseView != null) {
			baseView._reload();
		}
	}

	/**
	 * _reload.
	 */
	public void _reload() {

		initParameters();

		compute();
	}

	public void compute() {

		try {
			selectedProps = propsArea.getSelectedProperties();

			if (selectedCorpus == null || selectedProps == null || selectedProps.size() == 0) {
				return;
			}
			if (summary == null || !summary.getCorpus().equals(selectedCorpus)) {
				summary = new Summary(selectedCorpus);
				CorporaView.refreshObject(selectedCorpus);
			}

			ArrayList<StructuralUnitProperty> properties = new ArrayList<>();
			for (Property p : selectedProps) {
				if (p instanceof StructuralUnitProperty) {
					properties.add((StructuralUnitProperty) p);
				}
			}

			summary.setProperties(properties);
			summary.setDirty();
			if (summary.compute()) {
				tv.setContentProvider(new TreeNodeTreeProvider());
				tv.setLabelProvider(new TreeNodeLabelProvider());

				tv.setInput(summary);
				refresh();
				tv.expandToLevel(selectedProps.size() + 1);
			}
		}
		catch (Exception e) {
			System.out.println(TXMUIMessages.errorWhileComputingCorpusSummary);
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	public void initParameters() {

		if (!Toolbox.isInitialized()) {
			return;
		}
		// init combo
		try {
			corpusCombo.removeAll();
			for (MainCorpus p : CorpusManager.getCorpusManager().getCorpora().values()) {
				corpusCombo.add(p.getName());
			}
		}
		catch (Exception e2) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e2);
		}

		// init default corpus
		// try {
		// if (CorpusManager.getCorpusManager().getCorpora().size() > 0) {
		// selectedCorpus = CorpusManager.getCorpusManager().getCorpora().get(0);
		// corpusCombo.select(0);
		// }
		// } catch (Exception e) { org.txm.utils.logger.Log.printStackTrace(e);}

		if (selectedCorpus != null) reloadProperties();
	}

	public void reloadProperties() {
		if (selectedCorpus == null) return;

		ArrayList<StructuralUnitProperty> availables = new ArrayList<>();
		try {
			availables = new ArrayList<>(selectedCorpus.getOrderedStructuralUnitProperties());
		}
		catch (CqiClientException e1) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e1);
			return;
		}

		// init prop selector
		ArrayList<StructuralUnitProperty> selectorSelectedProps = new ArrayList<>();
		// if (summary != null) {
		// for(StructuralUnitProperty prop : summary.getProperties())
		// {
		// selectorSelectedProps.add(prop);
		// availables.remove(prop);
		// }
		// } else {
		// System.out.println("AVAILABLES SIZE > 0");
		if (availables.size() > 0) {
			boolean foundp = false;
			boolean founds = false;
			for (int i = 0; i < availables.size(); i++) {

				StructuralUnitProperty prop = availables.get(i);
				// System.out.println("check prop : "+prop.getFullName());
				String name = prop.getFullName();
				if ("text_id".equals(name)) { //$NON-NLS-1$
					selectorSelectedProps.add(availables.get(i));
					i--;
					continue;
				}
				if (!foundp) {
					if ("p_n".equals(name) || "p_id".equals(name)) { //$NON-NLS-1$ //$NON-NLS-2$
						selectorSelectedProps.add(availables.get(i));
						i--;
						foundp = true;
						continue;
					}
				}
				if (!founds) {
					if ("s_n".equals(name) || "s_id".equals(name)) { //$NON-NLS-1$ //$NON-NLS-2$
						selectorSelectedProps.add(availables.get(i));
						i--;
						founds = true;
						continue;
					}
				}
			}

			Collections.sort(selectorSelectedProps, new Comparator<Property>() {

				@Override
				public int compare(Property o1, Property o2) {
					StructuralUnitProperty s1 = (StructuralUnitProperty) o1;
					StructuralUnitProperty s2 = (StructuralUnitProperty) o2;
					char c1 = s1.getFullName().charAt(0);
					char c2 = s2.getFullName().charAt(0);
					if (c1 == 't') return -1;
					if (c2 == 't') return 1;
					if (c1 == 's') return 1;
					if (c2 == 's') return -1;
					return 0;
				}
			});
		}
		// }
		propsArea.clear();
		// System.out.println("Set properties "+availables+", "+selectorSelectedProps);
		this.propsArea.setProperties(availables, selectorSelectedProps);
		// init default props
		selectedProps = propsArea.getSelectedProperties();

		if (tv.getContentProvider() != null) {
			tv.setInput(null);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {

		parent.setLayout(new GridLayout(2, false));

		// [[Choose corpus]]
		corpusCombo = new Combo(parent, SWT.READ_ONLY);
		corpusCombo.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1));

		corpusCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				String corpusName = corpusCombo.getText();
				try {
					selectedCorpus = CorpusManager.getCorpusManager().getCorpus(corpusName);
					reloadProperties();
					compute();
				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					org.txm.utils.logger.Log.printStackTrace(e1);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// [Choose properties]
		propsArea = new PropertiesSelector(parent, SWT.NONE);
		propsArea.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		propsArea.setLayout(new GridLayout(3, false));

		// Button reload
		Button openeditdialog = new Button(parent, SWT.PUSH);
		openeditdialog.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		openeditdialog.setText(TXMUIMessages.reload);
		openeditdialog.setToolTipText("Reload the summary from the new properties selection");
		openeditdialog.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				compute();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// show corpus tree
		tv = new TreeViewer(parent);
		tv.getTree().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 2, 1));

		tv.addDoubleClickListener(new IDoubleClickListener() {

			private ProgressListener progresslistener;

			@Override
			public void doubleClick(DoubleClickEvent event) {
				// System.out.println("open Edition or Text editor");
				IStructuredSelection sel = (IStructuredSelection) tv.getSelection();
				Object first = sel.getFirstElement();
				final Summary.TreeNode node = (Summary.TreeNode) first;

				try {
					// final Page openedPage = summary.getDefaultEditionPage(node);
					// if (openedPage != null) {
					// // System.out.println(page.getFile());
					// if (attachedBrowserEditor == null || attachedBrowserEditor.isDisposed()) {
					// attachedBrowserEditor = OpenBrowser
					// .openEdition(openedPage.getFile()
					// .getAbsolutePath(), openedPage.getFile().getName());
					// attachedBrowserEditor.setEdition(openedPage.getEdition());
					// }
					// attachedBrowserEditor.showPage(openedPage);
					// }
					// else {
					// System.out.println(TXMUIMessages.noDefaultEditionPageFoundForElementColon + node);
					// }
					//
					// if (progresslistener != null) {
					// attachedBrowserEditor.getBrowser().removeProgressListener(progresslistener);
					// }
					// progresslistener = new ProgressListener() {
					//
					// @Override
					// public void changed(ProgressEvent event) {}
					//
					// @Override
					// public void completed(ProgressEvent event) {
					// Page currentpage = attachedBrowserEditor.getCurrentPage();
					// // System.out.println("Highlight ! "+currentpage.getEdition().getText()+" name ="+openedPage.getEdition().getText());
					// if (currentpage != null) {
					// if (!(currentpage.getEdition().getText() == openedPage.getEdition().getText())) {
					// return;
					// }
					// }
					// String wordid = node.getWordId();
					// String highlightscript = "try { var elt = document.getElementById(\"" + wordid + "\");" + //$NON-NLS-1$ //$NON-NLS-2$
					// "elt.style.backgroundColor=\"#F9D0D0\";" //$NON-NLS-1$
					// +
					// "elt.style.paddingLeft=\"3px\";" + //$NON-NLS-1$
					// "elt.style.paddingRight=\"3px\";" + //$NON-NLS-1$
					// "elt.style.paddingTop=\"1px\";" + //$NON-NLS-1$
					// "elt.style.paddingBottom=\"1px\";" + //$NON-NLS-1$
					// "} catch (e) { };"; //$NON-NLS-1$
					//
					// attachedBrowserEditor.getBrowser().execute(highlightscript);
					//
					// String script = "try { document.getElementById(\"" + wordid + "\").scrollIntoView(true); } catch (e) { };"; //$NON-NLS-1$ //$NON-NLS-2$
					// attachedBrowserEditor.getBrowser().execute(script);
					// }
					// };
					//
					// attachedBrowserEditor.getBrowser().addProgressListener(progresslistener);
					//
					// IWorkbenchPage attachedPage = attachedBrowserEditor.getEditorSite().getPage();
					// attachedPage.activate(attachedBrowserEditor);

				}
				catch (Exception e) {
					org.txm.utils.logger.Log.printStackTrace(e);
				}
			}
		});

		initParameters();

		MenuManager menuManager = new MenuManager();
		Menu menu = menuManager.createContextMenu(tv.getTree());

		// Set the MenuManager
		tv.getTree().setMenu(menu);
		getSite().registerContextMenu(menuManager, tv);
		// Make the selection available
		getSite().setSelectionProvider(tv);


	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

	public MainCorpus getSelectedCorpus() {
		return selectedCorpus;
	}

	public List<StructuralUnitProperty> getProperties() {
		return selectedProps;
	}

	/**
	 * The Class TxmObjectLabelProvider.
	 */
	public class TreeNodeLabelProvider extends LabelProvider {

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
		 */
		@Override
		public String getText(Object element) {
			if (element instanceof Summary.TreeNode) {
				String s = ((Summary.TreeNode) element).getInfo();
				if (s == null || s.length() == 0) return ((Summary.TreeNode) element).toString();
				return s;
			}
			return NLS.bind(TXMUIMessages.errorColonGetTextColonP0, element);
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
		 */
		@Override
		public final Image getImage(Object element) {
			return null;
		}
	}

	/**
	 * The Class TxmObjectTreeProvider.
	 */
	public class TreeNodeTreeProvider implements ITreeContentProvider {

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
		 */
		@Override
		public Object[] getChildren(Object element) {
			if (element instanceof Summary.TreeNode) {
				Summary.TreeNode b = ((Summary.TreeNode) element);
				return b.toArray();
			}
			return new Object[0];
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
		 */
		@Override
		public Object getParent(Object element) {
			if (element instanceof Summary.TreeNode) {
				return ((Summary.TreeNode) element).getParent();
			}
			return new Object[0];
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
		 */
		@Override
		public boolean hasChildren(Object element) {
			if (element instanceof Summary.TreeNode) {
				Summary.TreeNode b = ((Summary.TreeNode) element);
				return b.size() > 0;
			}
			return false;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
		 */
		@Override
		public Object[] getElements(Object element) {
			if (summary != null) {
				Object[] objects = { summary.getRoot() };
				return objects;
			}

			return new Object[0];
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
		 */
		@Override
		public void dispose() {
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
		 */
		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}
}
