package org.txm.rcp.views.corpora;

import java.io.File;
import java.net.URL;

import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.widgets.TreeItem;
import org.txm.core.results.TXMResult;
import org.txm.rcp.commands.workspace.LoadBinaryCorpus;
import org.txm.rcp.handlers.export.ImportResultCalculusParameters;
import org.txm.rcp.handlers.files.EditFile;
import org.txm.utils.FileUtils;
import org.txm.utils.logger.Log;

/**
 * 
 * Manage minimal D&D for TXM in the corpora view : load corpus, import result and open file
 * 
 * @author mdecorde
 *
 */
public class CorporaViewDropListener extends ViewerDropAdapter {

	public CorporaViewDropListener(Viewer viewer) {

		super(viewer);
	}

	@Override
	public void drop(DropTargetEvent event) {
		
		try {
			int location = this.determineLocation(event);
			Object otarget = event.data;
			
			if (otarget instanceof StructuredSelection ss) {
				otarget = ss.getFirstElement();
			}
			
			if (otarget instanceof TXMResult qbResult 
					&& location == LOCATION_ON 
					&& event.item instanceof TreeItem titem 
					&& titem.getData() instanceof TXMResult targetResult
					&& qbResult.getParent().getClass().equals(targetResult.getClass())
					&& qbResult != targetResult) {
				qbResult.clone(targetResult, true);
				getViewer().refresh();
				return;
			}
			
//			System.out.println("DATA TYPE: "+event.currentDataType);
			File file = null;
			if (otarget instanceof String s && s.startsWith("file:")) {
				file = new File(s);
				if (!file.exists()) {
					file = null;
					s = new URL(s).toURI().getPath();
					file = new File(s);
					if (!file.exists()) {
						file = null;
					}
				}
			}

			if (file != null) {
				String ext = FileUtils.getExtension(file).toLowerCase();
				if ("txm".equals(ext)) {
					LoadBinaryCorpus.loadBinaryCorpusArchive(file);
				} else if ("txmcmd".equals(ext) && location == LOCATION_ON && event.item instanceof TreeItem titem && titem.getData() instanceof TXMResult result) {
					ImportResultCalculusParameters.importCommand(file, result);
				} else if (file.isFile()) {
					EditFile.openfile(file);
				}
			}
		}
		catch (Exception e) {
			Log.fine("D&D error: "+e);
		}
		super.drop(event);
	}

	@Override
	public boolean performDrop(Object data) {

		return false;
	}

	@Override
	public boolean validateDrop(Object target, int operation, TransferData transferType) {
		
		return true;
	}

}
