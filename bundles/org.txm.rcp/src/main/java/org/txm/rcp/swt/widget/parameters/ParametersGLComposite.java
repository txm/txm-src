package org.txm.rcp.swt.widget.parameters;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.kohsuke.args4j.NamedOptionDef;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;

/**
 * Display parameters in specialized widget
 * 
 * @author mdecorde
 *
 */
public class ParametersGLComposite extends GLComposite {

	ParametersManager manager;

	LinkedHashMap<String, ParameterField> widgets = new LinkedHashMap<>();

	public int MAX_PARAMETERS_PER_LINE = 10;

	public boolean mustShowDialog = true;

	private GLComposite columns;

	private int numberOfParameters;

	private int numberOfMandatoryFields;

	public ParametersGLComposite(Composite parent, int style, String name, ParametersManager manager) {
		super(parent, style, name);
		this.manager = manager;

		// Button propButton = createButton(this, IDialogConstants.BACK_ID,
		// "...", false);
		// propButton.removeListener(SWT.Selection, propButton.getListeners(SWT.Selection)[0]);
		//
		// propButton.addSelectionListener(new SelectionAdapter() {
		// @Override
		// public void widgetSelected(SelectionEvent e) {
		// FileDialog dialog = new FileDialog(e.display.getActiveShell());
		// dialog.setFilterPath(propFile.getParent());
		// dialog.setFilterExtensions(new String[] {"*.properties"});
		// dialog.setFileName(propFile.getName());
		// String path = dialog.open();
		// if (path != null) {
		// propFile = new File(path);
		// loadDefaultValuesFromPropFile();
		// }
		// }
		// });

		buildFields();

		Button button = new Button(parent, SWT.PUSH);
		button.removeListener(SWT.Selection, button.getListeners(SWT.Selection)[0]);
		button.setText("Reset");
		button.setToolTipText("Reset parameters to default values");
		button.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				resetWidgets();
			}
		});
	}

	public ParametersManager getManager() {
		return manager;
	}

	public void resetWidgets() {
		// System.out.println("Reseting fields to default values");
		for (String k : widgets.keySet()) {
			widgets.get(k).resetToDefault();
		}
	}

	/**
	 * Build the fields depending on the values parameters and default values
	 * 
	 */
	public void buildFields() {

		if (columns != null && !columns.isDisposed()) {
			columns.dispose();
			widgets.clear();
		}

		columns = new GLComposite(this, SWT.NONE, "columns"); //$NON-NLS-1$
		columns.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		columns.getLayout().numColumns = 1;

		// scrolledComposite.setContent(columns);
		// scrolledComposite.addListener( SWT.Resize, event -> {
		// int width = scrolledComposite.getClientArea().width;
		// scrolledComposite.setMinSize( composite.computeSize( width, SWT.DEFAULT ) );
		// } );

		// count the number of fields to show. If "0" then don't show the dialog, see the "open()" function
		// and select only the NamedOptionDef annotations
		numberOfParameters = 0; // number of parameters not set by args
		numberOfMandatoryFields = 0;
		LinkedHashMap<String, Object> defaultValues = manager.getDefaultValues();
		for (NamedOptionDef option : manager.parameters.values()) {
			if (defaultValues.containsKey(option.name())) {
				continue; // don't show the field
			}

			// count number of mandatory fields set AND add exception for "Separator" widget types
			if (option.required() || "Separator".equals(option.widget())) numberOfMandatoryFields++; //$NON-NLS-1$
			numberOfParameters++;
		}

		mustShowDialog = numberOfMandatoryFields > 0 || (defaultValues.size() == 0 && numberOfParameters > 0);

		if (numberOfParameters > MAX_PARAMETERS_PER_LINE) {
			columns.getLayout().numColumns = 1 + (numberOfParameters / MAX_PARAMETERS_PER_LINE);
		}

		GLComposite column = new GLComposite(columns, SWT.NONE, "column"); //$NON-NLS-1$
		GridData gdata = new GridData(SWT.FILL, SWT.FILL, true, true);
		gdata.minimumWidth = 200;
		column.setLayoutData(gdata);
		column.getLayout().makeColumnsEqualWidth = false;

		// initialize each widget + default value *of the widget*
		int n = 0;
		for (NamedOptionDef option : manager.getParameters().values()) {
			n++;
			if (n % MAX_PARAMETERS_PER_LINE == 0) {
				column = new GLComposite(columns, SWT.NONE, "column"); //$NON-NLS-1$
				gdata = new GridData(SWT.FILL, SWT.FILL, true, true);
				gdata.minimumWidth = 200;
				column.setLayoutData(gdata);
				column.getLayout().makeColumnsEqualWidth = false;
			}

			// each widget is initialized with de default value set in @Field@def attribute
			String widgetName = option.widget();
			ParameterField widget = null;
			if ("File".equals(widgetName) || "FileOpen".equals(widgetName)) { //$NON-NLS-1$ //$NON-NLS-2$
				widget = new FileField(column, SWT.NONE, option);
			}
			else if ("Files".equals(widgetName) || "FilesOpen".equals(widgetName)) { //$NON-NLS-1$ //$NON-NLS-2$
				widget = new FilesField(column, SWT.NONE, option);
			}
			else if ("CreateFile".equals(widgetName) || "FileSave".equals(widgetName)) { //$NON-NLS-1$ //$NON-NLS-2$
				widget = new CreateFileField(column, SWT.NONE, option);
			}
			else if ("Folder".equals(widgetName)) { //$NON-NLS-1$
				widget = new FolderField(column, SWT.NONE, option);
			}
			else if ("Query".equals(widgetName)) { //$NON-NLS-1$
				widget = new QueryField(column, SWT.NONE, option);
			}
			else if ("Date".equals(widgetName)) { //$NON-NLS-1$
				widget = new DateField(column, SWT.NONE, option);
			}
			else if ("Time".equals(widgetName)) { //$NON-NLS-1$
				widget = new TimeField(column, SWT.NONE, option);
			}
			else if ("Separator".equals(widgetName)) { //$NON-NLS-1$
				widget = new SeparatorField(column, SWT.NONE, option);
			}
			else if ("String".equals(widgetName)) { //$NON-NLS-1$
				widget = new StringField(column, SWT.NONE, option);
			}
			else if ("Password".equals(widgetName)) { //$NON-NLS-1$
				widget = new PasswordField(column, SWT.NONE, option);
			}
			else if ("StringArray".equals(widgetName)) { //$NON-NLS-1$
				widget = new StringArrayField(column, SWT.NONE, option);
			}
			else if ("StringArrayMultiple".equals(widgetName)) { //$NON-NLS-1$
				widget = new StringArrayMultipleField(column, SWT.NONE, option);
			}
			else if ("StructuralUnits".equals(widgetName)) { //$NON-NLS-1$
				widget = new CorpusStructuresField(column, SWT.NONE, option);
			}
			else if ("Text".equals(widgetName)) { //$NON-NLS-1$
				widget = new LongStringField(column, SWT.NONE, option);
			}
			else if ("Integer".equals(widgetName)) { //$NON-NLS-1$
				widget = new IntegerField(column, SWT.NONE, option);
			}
			else if ("Float".equals(widgetName)) { //$NON-NLS-1$
				widget = new FloatField(column, SWT.NONE, option);
			}
			else if ("Boolean".equals(widgetName)) { //$NON-NLS-1$
				widget = new BooleanField(column, SWT.NONE, option);
			}
			else {
				System.out.println(TXMUIMessages.unknowedWidgetNameColon + widgetName + TXMUIMessages.FileTreeContentProvider_4);
			}

			if (widget != null) {
				widgets.put(option.name(), widget);

				// set default value in the fields, not enough if args contains the value
				Object value = defaultValues.get(option.name());
				if (value != null) { // set the default value using the properties stored
					widget.setDefault(value);
				}
			}
		}
	}

	/**
	 * 
	 * @return the number of fields after the buildWidgets is called
	 */
	public int getNumberOfParameters() {
		return numberOfParameters;
	}

	/**
	 * 
	 * @return the number of mandatory fields after the buildWidgets is called
	 */
	public int getNumberOfMandatoryFields() {
		return numberOfMandatoryFields;
	}

	/**
	 * 
	 * @return the number of fields after the buildWidgets is called
	 */
	public int getNumberOfFields() {
		return widgets.size();
	}

	public ArrayList<String> getNonSetFields() {
		ArrayList<String> missing = new ArrayList<>();

		if (manager.getParameters() == null) return missing; // no constraint

		LinkedHashMap<String, Object> finalValues = manager.getFinalValues();

		for (NamedOptionDef p : manager.getParameters().values()) {
			if (p.required() && finalValues.get(p.name()) == null) {
				missing.add(p.name());
			}
		}
		return missing;
	}
}
