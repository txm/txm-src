package org.txm.rcp.utils;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.part.EditorPart;
import org.txm.rcp.Activator;

/**
 * Utils :
 * - getShell() from various SWT&Eclipse objects
 * 
 * @author mdecorde
 *
 */
public class SWTUtils {

	public static Shell getShell() {
		Shell shell = Activator.getDefault().getWorkbench().getActiveWorkbenchWindow().getShell();
		if (shell == null) {
			return new Shell();
		}
		return shell;
	}

	public static Shell getShell(EditorPart editor) {
		if (editor == null) return getShell();
		return editor.getSite().getShell();
	}

	public static Shell getShell(ExecutionEvent event) {
		if (event == null) return getShell();
		return HandlerUtil.getActiveShell(event);
	}

	public static Shell getShell(Widget widget) {
		if (widget == null || widget.isDisposed()) return getShell();
		return widget.getDisplay().getActiveShell();
	}
	
	public static int getOptimalWidth(Control c, int size) {
		GC gc = new GC(c);
		gc.setFont(c.getFont());
		StringBuffer buffer = new StringBuffer(size);
		char a = 'a';
		for (int i = 0 ; i < size ; i++) buffer.append(a);
		int i = gc.textExtent(buffer.toString()).x;
		gc.dispose();
		
		return i;
	}
}
