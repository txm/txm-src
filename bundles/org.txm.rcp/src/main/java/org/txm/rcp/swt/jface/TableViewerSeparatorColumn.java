package org.txm.rcp.swt.jface;

import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;

public class TableViewerSeparatorColumn {

	public static final String EMPTY = "";

	public static TableViewerColumn newSeparator(TableViewer viewer) {

		TableViewerColumn separator = new TableViewerColumn(viewer, SWT.NONE);
		separator.getColumn().setResizable(false);
		separator.getColumn().setData("separator", true);
		separator.setLabelProvider(new CellLabelProvider() {

			public void update(ViewerCell cell) {
			}
		});
		return separator;
	}
}
