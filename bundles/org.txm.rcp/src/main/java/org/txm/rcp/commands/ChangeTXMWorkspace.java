// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.ide.ChooseWorkspaceData;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.JobHandler;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * restart TXM: restart searchengine, statengine, workspace...
 * 
 * @author mdecorde
 * 
 */
public class ChangeTXMWorkspace extends AbstractHandler {

	/** The ID. */
	static public String ID = "org.txm.rcp.commands.RestartTXM"; //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		JobHandler jobhandler = new JobHandler(TXMUIMessages.ChangingTheWorkingDirectory) {

			@Override
			protected IStatus _run(SubMonitor monitor) {
				try {
					monitor.worked(95);
					syncExec(new Runnable() {

						@Override
						public void run() {
							change(null);
						}
					});

				}
				catch (Exception e) {
					System.out.println(NLS.bind(TXMUIMessages.errorWhileRunningScriptColonP0, e));
					org.txm.utils.logger.Log.printStackTrace(e);
					return Status.CANCEL_STATUS;
				}
				finally {
					monitor.done();
				}
				return Status.OK_STATUS;
			}
		};
		jobhandler.startJob(false);
		return event;
	}

	public static boolean change(String defaultValue) {
		if (defaultValue == null) {
			defaultValue = ResourcesPlugin.getWorkspace().getRoot().getLocation().toFile().getAbsolutePath();
		}
		ChooseWorkspaceData launchData = new ChooseWorkspaceData(defaultValue);
		DirectoryDialog dialog = new DirectoryDialog(Display.getDefault().getActiveShell());
		dialog.setText(TXMUIMessages.selectWorkingDirectory);
		dialog.setMessage(TXMUIMessages.ThisWillRestartTXM);
		dialog.setFilterPath(defaultValue);
		String path = dialog.open();
		if (path == null || defaultValue.equals(path)) {
			return false;
		}
		launchData.workspaceSelected(path);
		TBXPreferences.getInstance().put(TBXPreferences.USER_TXM_HOME, path);
		TBXPreferences.saveAll();

		// Switch workspace :
		try {
			URL url = new URL("file", "", path); //$NON-NLS-1$ //$NON-NLS-2$

			launchData.writePersistedData();
			ChooseWorkspaceData.setShowDialogValue(false);
			PlatformUI.getWorkbench().restart(); // need to restart TXM to apply changes
		}
		catch (MalformedURLException e1) {
			Log.warning(TXMUIMessages.WorkspacePathCouldNotBeFound);
			return false;
		}

		return false;
	}
}
