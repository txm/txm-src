/**
 * 
 */
package org.txm.rcp.swt.widget;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TXMEditorToolBar;
import org.txm.rcp.editors.listeners.ComputeKeyListener;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.rcp.swt.GLComposite;

/**
 * Convenience class to add a SWT Control, labeled or not, to some editor parameters areas.
 * 
 * If the parent is a GLComposite then the number of columns of its GridLayout will be adjusted to receive the control.
 * If the parent is a TXMEditorToolbar, use the SWT.SEPARATOR trick, plus the packing and the right width setting to add non-ToolItem Control to a tool bar.
 * 
 * This class also adds all the needed computing listeners.
 * 
 * @author sjacquot
 *
 */
public class TXMParameterControl {


	/**
	 * The label.
	 */
	protected Label label;

	/**
	 * The control.
	 */
	protected Control control;


	/**
	 * The computing key listener.
	 */
	protected ComputeKeyListener computingKeyListener;


	/**
	 * The computing selection listener.
	 */
	protected ComputeSelectionListener computingSelectionListener;


	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param control
	 * @param labelText
	 * @param controlTooltip
	 * @param autoCompute
	 */
	protected TXMParameterControl(Composite parent, TXMEditor<?> editor, Control control, String labelText, String controlTooltip, boolean autoCompute) {

		if (labelText != null) {
			this.label = new Label(parent, SWT.NONE);
			this.label.setText(labelText);
			if (controlTooltip != null) this.label.setToolTipText(controlTooltip);
		}

		this.control = control;

		if (controlTooltip != null) {
			this.control.setToolTipText(controlTooltip);
		}

		// add computing listeners
		this.computingKeyListener = new ComputeKeyListener(editor, autoCompute);
		this.control.addKeyListener(this.computingKeyListener);

		// adjust the parent GridLayout number of columns
		if (parent instanceof GLComposite) {
			((GLComposite) parent).getLayout().numColumns++;
			if (this.label != null) {
				((GLComposite) parent).getLayout().numColumns++;
			}
		}
		// fix the non-ToolItem Control addition to the tool bar using the SWT.SEPARATOR trick, plus the packing and the right width setting.
		else if (parent instanceof TXMEditorToolBar) {
			final ToolItem itemSeparator = new ToolItem((ToolBar) parent, SWT.SEPARATOR);
			this.control.pack();
			itemSeparator.setWidth(control.getBounds().width);
			itemSeparator.setControl(control);
			Listener listener = new Listener() {

				@Override
				public void handleEvent(Event event) {
					control.pack();
					itemSeparator.setWidth(control.getBounds().width);
				}
			};
			this.control.addListener(SWT.Modify, listener);
		}

		// force the label to be on the left of the control
		if (this.label != null) {
			this.label.moveAbove(this.control);
		}
	}

	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param control
	 * @param controlTooltip
	 * @param autoCompute
	 */
	public TXMParameterControl(Composite parent, TXMEditor editor, Control control, String controlTooltip, boolean autoCompute) {
		this(parent, editor, control, null, controlTooltip, autoCompute);
	}

	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param control
	 * @param controlTooltip
	 */
	public TXMParameterControl(Composite parent, TXMEditor editor, Control control, String controlTooltip) {
		this(parent, editor, control, controlTooltip, false);
	}

	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param control
	 */
	public TXMParameterControl(Composite parent, TXMEditor editor, Control control) {
		this(parent, editor, control, null, false);
	}

	public void setToolTipText(String tooltip) {
		if (label != null) label.setToolTipText(tooltip);
		if (control != null) control.setToolTipText(tooltip);
	}

	/**
	 * Gets the label.
	 * 
	 * @return the label
	 */
	public Label getLabel() {
		return label;
	}


	/**
	 * Gets the control.
	 * 
	 * @return the control
	 */
	public Control getControl() {
		return control;
	}

	public ComputeKeyListener getComputingKeyListener() {
		return computingKeyListener;
	}

	public ComputeSelectionListener getComputingSelectionListener() {
		return computingSelectionListener;
	}

}
