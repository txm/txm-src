// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.dialog;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.VirtualProperty;

/**
 * The dialog box called to select the view properties.
 * 
 * @deprecated use the MultiPropertiesSelectionDialog instead
 * 
 * @author mdecorde
 */
@Deprecated
public class SinglePropertySelectionDialog<P extends Property> extends Dialog {

	/** The available properties. */
	private List<P> allProperties;

	/** The available properties. */
	private List<P> availableProperties;

	/** The selected properties. */
	private List<P> selectedProperties;

	/** The maxprops. */
	int maxprops = -1;

	/** The available properties view. */
	org.eclipse.swt.widgets.List availablePropertiesView;

	private ArrayList<P> cancelSelectedProperties;

	private ArrayList<P> cancelAvailableProperties;

	Control sourceWidget;

	/**
	 * Instantiates a new view property selection dialog.
	 *
	 * @param parentShell the parent shell
	 * @param availableProperties the available properties
	 * @param selectedProperties the selected properties
	 */
	public SinglePropertySelectionDialog(IShellProvider parentShell,
			List<P> availableProperties,
			List<P> selectedProperties) {
		this(parentShell.getShell(), availableProperties, selectedProperties);
	}

	/**
	 * Instantiates a new view property selection dialog.
	 *
	 * @param shell the shell
	 * @param availableProperties the available properties
	 * @param selectedProperties the selected properties
	 */
	public SinglePropertySelectionDialog(Shell shell,
			List<P> availableProperties,
			List<P> selectedProperties) {
		this(shell, availableProperties, selectedProperties, -1, null);
	}

	/**
	 * Instantiates a new view property selection dialog.
	 *
	 * @param shell the shell
	 * @param availableProperties the available properties
	 * @param selectedProperties the selected properties
	 * @param maxprops the maxprops
	 */
	public SinglePropertySelectionDialog(Shell shell,
			List<P> availableProperties,
			List<P> selectedProperties, int maxprops, Control sourceWidget) {
		super(shell);

		this.availableProperties = availableProperties;
		this.selectedProperties = selectedProperties;
		this.allProperties = new ArrayList<>();
		allProperties.addAll(selectedProperties);
		allProperties.addAll(availableProperties);

		this.cancelAvailableProperties = new ArrayList<>(availableProperties);
		this.cancelSelectedProperties = new ArrayList<>(selectedProperties);

		if (sourceWidget != null) {
			this.setShellStyle(SWT.NONE | SWT.APPLICATION_MODAL);
		}
		else {
			this.setShellStyle(this.getShellStyle() | SWT.APPLICATION_MODAL | SWT.RESIZE);
		}
		this.maxprops = maxprops;
		this.sourceWidget = sourceWidget;
	}

	/**
	 * Gets the selected properties.
	 *
	 * @return the selected properties
	 */
	public List<P> getSelectedProperties() {
		return selectedProperties;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(TXMUIMessages.common_displayOptions);
		newShell.setMinimumSize(300, 200);
		if (sourceWidget != null) {
			Point m = newShell.getDisplay().map(sourceWidget, null, 0, sourceWidget.getSize().y);
			newShell.setLocation(m);
		}
		newShell.addListener(SWT.Deactivate, event -> okPressed());
	}

	@Override
	protected Point getInitialLocation(Point initialSize) {
		if (sourceWidget != null) {
			return sourceWidget.getDisplay().map(sourceWidget, null, 0, sourceWidget.getSize().y);
		}
		return super.getInitialLocation(initialSize);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		// parent.setLayout(new FormLayout());
		Composite mainArea = new Composite(parent, SWT.NONE);
		mainArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		// 4 columns
		GridLayout layout = new GridLayout(4, false);
		mainArea.setLayout(layout);

		availablePropertiesView = new org.eclipse.swt.widgets.List(mainArea, SWT.BORDER | SWT.V_SCROLL);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		availablePropertiesView.setLayoutData(gridData);

		availablePropertiesView.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseUp(MouseEvent e) {

				_updateSelection();
				okPressed();
			}
		});

//		availablePropertiesView.addKeyListener(new KeyListener() {
//
//			@Override
//			public void keyReleased(KeyEvent e) {
//			}
//
//			@Override
//			public void keyPressed(KeyEvent e) {
//
//				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
//					_updateSelection();
//					okPressed();
//				}
//			}
//		});

		reload();
		return mainArea;
	}

	protected void _updateSelection() {

		int index = availablePropertiesView.getSelectionIndex();

		if (selectedProperties.size() > 0) {
			availableProperties.add(0, selectedProperties.get(0));
			selectedProperties.remove(0);
		}

		selectedProperties.add(allProperties.get(index));
		availableProperties.remove(availableProperties.indexOf(allProperties.get(index)));
	}

	/**
	 * Reload.
	 */
	public void reload() {
		if (allProperties == null) {
			System.out.println("Error: the available properties list is NULL."); //$NON-NLS-1$
			return;
		}
		availablePropertiesView.removeAll();
		for (Property property : allProperties) {
			// if (!property.getName().equals("id")) //$NON-NLS-1$
			if (property instanceof StructuralUnitProperty || property instanceof VirtualProperty) {
				availablePropertiesView.add(property.getFullName());
			}
			else {
				availablePropertiesView.add(property.getName());
			}
		}

		availablePropertiesView.getParent().layout();
		if (selectedProperties.size() > 0) {
			availablePropertiesView.setSelection(allProperties.indexOf(selectedProperties.get(0)));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		if (selectedProperties.size() != 0) {
			super.okPressed();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void cancelPressed() {
		this.selectedProperties.clear();
		this.selectedProperties.addAll(cancelSelectedProperties);

		this.availableProperties.clear();
		availableProperties.addAll(cancelAvailableProperties);

		super.cancelPressed();
	}
}
