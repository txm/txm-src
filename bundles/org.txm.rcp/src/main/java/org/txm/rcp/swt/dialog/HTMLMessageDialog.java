package org.txm.rcp.swt.dialog;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.layout.LayoutConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.CloseWindowListener;
import org.eclipse.swt.browser.OpenWindowListener;
import org.eclipse.swt.browser.VisibilityWindowListener;
import org.eclipse.swt.browser.WindowEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

/**
 * A dialog for showing messages to the user.
 * 
 * Same code as org.eclipse.jface.dialogs.MessageDialog but with a setImage method
 * 
 * <p>
 * This concrete dialog class can be instantiated as is, or further subclassed
 * as required.
 * </p>
 * <p>
 * <strong>Note:</strong> This class does not use button IDs from
 * IDialogConstants. Instead, the ID is the index of the button in the supplied
 * array.
 * </p>
 */
public class HTMLMessageDialog extends Dialog {

	/**
	 * Message (a localized string).
	 */
	protected String message;

	/**
	 * Message label is the label the message is shown on.
	 */
	protected Browser messageLabel;

	/**
	 * Return the label for the image.
	 */
	protected Label imageLabel;

	/**
	 * Labels for buttons in the button bar (localized strings).
	 */
	private String[] buttonLabels;

	/**
	 * The buttons. Parallels <code>buttonLabels</code>.
	 */
	private Button[] buttons;

	/**
	 * Index into <code>buttonLabels</code> of the default button.
	 */
	private int defaultButtonIndex;

	/**
	 * Dialog title (a localized string).
	 */
	private String title;

	/**
	 * Dialog title image.
	 */
	private Image titleImage;

	/**
	 * Image, or <code>null</code> if none.
	 */
	private Image image = null;

	/**
	 * The custom dialog area.
	 */
	private Control customArea;

	public HTMLMessageDialog(Shell parentShell, String dialogTitle,
			Image dialogTitleImage, String dialogMessage, Image dialogImage,
			String[] dialogButtonLabels, int defaultIndex) {
		super(parentShell);

		this.title = dialogTitle;
		this.titleImage = dialogTitleImage;
		this.message = dialogMessage;
		this.image = dialogImage;
		this.buttonLabels = dialogButtonLabels;
		this.defaultButtonIndex = defaultIndex;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		if (title != null) {
			shell.setText(title);
		}
		if (titleImage != null) {
			shell.setImage(titleImage);
		}
	}

	/**
	 * Create the area the message will be shown in.
	 * <p>
	 * The parent composite is assumed to use GridLayout as its layout manager,
	 * since the parent is typically the composite created in
	 * {@link Dialog#createDialogArea}.
	 * </p>
	 * <p>
	 * <strong>Note:</strong> Clients are expected to call this method, otherwise
	 * neither the icon nor the message will appear.
	 * </p>
	 * 
	 * @param composite
	 *            The composite to parent from.
	 * @return Control
	 */
	protected Control createMessageArea(Composite composite) {
		// create composite
		// create image
		Image image = getImage();
		if (image != null) {
			imageLabel = new Label(composite, SWT.NULL);
			image.setBackground(imageLabel.getBackground());
			imageLabel.setImage(image);

			GridDataFactory.fillDefaults().align(SWT.CENTER, SWT.BEGINNING)
					.applyTo(imageLabel);
		}
		// create message
		if (message != null) {
			messageLabel = new Browser(composite, getMessageLabelStyle());
			this.messageLabel.addOpenWindowListener(new OpenWindowListener() {

				@Override
				public void open(WindowEvent event) {

					if (org.txm.utils.OSDetector.isFamilyUnix()) {
						event.browser = messageLabel;
						event.required = false;
					}
				}
			});
			Color color = composite.getBackground();
			messageLabel.setText("<body style=\"background-color:rgb(" + color.getRed() + ", " + color.getGreen() + ", " + color.getBlue() + ")\">\n" + message + "\n</body>", true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			//System.out.println("set message: "+message);
			GridDataFactory
					.fillDefaults()
					.align(SWT.FILL, SWT.BEGINNING)
					.grab(true, true)
					.hint(500, 400).applyTo(messageLabel);


			messageLabel.setBackground(composite.getBackground());
			initialize(composite.getDisplay(), messageLabel);
		}
		return composite;
	}

	static void initialize(final Display display, Browser browser) {
		browser.addOpenWindowListener(new OpenWindowListener() {

			@Override
			public void open(WindowEvent e) {
				Shell shell = new Shell(e.display);
				shell.setText("New Window"); //$NON-NLS-1$
				shell.setLayout(new FillLayout());
				Browser browser1 = new Browser(shell, SWT.NONE);
				initialize(e.display, browser1);
				e.browser = browser1;
			}
		});
		browser.addVisibilityWindowListener(new VisibilityWindowListener() {

			@Override
			public void hide(WindowEvent e) {
				Browser browser = (Browser) e.widget;
				Shell shell = browser.getShell();
				shell.setVisible(false);
			}

			@Override
			public void show(WindowEvent e) {
				Browser browser = (Browser) e.widget;
				final Shell shell = browser.getShell();
				if (e.location != null) shell.setLocation(e.location);
				if (e.size != null) {
					Point size = e.size;
					shell.setSize(shell.computeSize(size.x, size.y));
				}
				shell.open();
			}
		});
		browser.addCloseWindowListener(new CloseWindowListener() {

			@Override
			public void close(WindowEvent e) {
				Browser browser1 = (Browser) e.widget;
				Shell shell = browser1.getShell();
				shell.close();
			}
		});
	}

	public Image getImage() {
		return image;
	}

	/**
	 * Returns the style for the message label.
	 * 
	 * @return the style for the message label
	 * 
	 * @since 3.0
	 */
	protected int getMessageLabelStyle() {
		return SWT.WRAP;
	}

	/*
	 * @see Dialog.createButtonBar()
	 */
	@Override
	protected Control createButtonBar(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().numColumns(0) // this is incremented
				// by createButton
				.equalWidth(true).applyTo(composite);

		GridDataFactory.fillDefaults().align(SWT.END, SWT.CENTER).span(2, 1)
				.applyTo(composite);
		composite.setFont(parent.getFont());
		// Add the buttons to the button bar.
		createButtonsForButtonBar(composite);
		return composite;
	}

	/**
	 * This implementation of the <code>Dialog</code> framework method creates
	 * and lays out a composite and calls <code>createMessageArea</code> and
	 * <code>createCustomArea</code> to populate it. Subclasses should
	 * override <code>createCustomArea</code> to add contents below the
	 * message.
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		// create message area
		createMessageArea(parent);
		// create the top level composite for the dialog area
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		composite.setLayout(layout);
		GridData data = new GridData(GridData.FILL_BOTH);
		data.horizontalSpan = 2;
		composite.setLayoutData(data);
		// allow subclasses to add custom controls
		customArea = createCustomArea(composite);
		//If it is null create a dummy label for spacing purposes
		if (customArea == null) {
			customArea = new Label(composite, SWT.NULL);
		}
		return composite;
	}

	/*
	 * @see Dialog.createContents(Composite)
	 */
	@Override
	protected Control createContents(Composite parent) {
		// initialize the dialog units
		initializeDialogUnits(parent);
		Point defaultSpacing = LayoutConstants.getSpacing();
		GridLayoutFactory.fillDefaults().margins(LayoutConstants.getMargins())
				.spacing(defaultSpacing.x * 2,
						defaultSpacing.y)
				.numColumns(getColumnCount()).applyTo(parent);

		GridDataFactory.fillDefaults().grab(true, true).applyTo(parent);
		createDialogAndButtonArea(parent);
		return parent;
	}

	/**
	 * Get the number of columns in the layout of the Shell of the dialog.
	 * 
	 * @return int
	 * @since 3.3
	 */
	int getColumnCount() {
		return 2;
	}

	/**
	 * Create the dialog area and the button bar for the receiver.
	 * 
	 * @param parent
	 */
	protected void createDialogAndButtonArea(Composite parent) {
		// create the dialog area and button bar
		dialogArea = createDialogArea(parent);
		buttonBar = createButtonBar(parent);
		// Apply to the parent so that the message gets it too.
		applyDialogFont(parent);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
	 */
	@Override
	protected void buttonPressed(int buttonId) {
		setReturnCode(buttonId);
		close();
	}


	/**
	 * Creates and returns the contents of an area of the dialog which appears
	 * below the message and above the button bar.
	 * <p>
	 * The default implementation of this framework method returns
	 * <code>null</code>. Subclasses may override.
	 * </p>
	 * 
	 * @param parent
	 *            parent composite to contain the custom area
	 * @return the custom area control, or <code>null</code>
	 */
	protected Control createCustomArea(Composite parent) {
		return null;
	}

	/**
	 * An accessor for the labels to use on the buttons.
	 * 
	 * @return The button labels to used; never <code>null</code>.
	 */
	protected String[] getButtonLabels() {
		return buttonLabels;
	}

	/**
	 * An accessor for the index of the default button in the button array.
	 * 
	 * @return The default button index.
	 */
	protected int getDefaultButtonIndex() {
		return defaultButtonIndex;
	}

	/**
	 * A mutator for the array of buttons in the button bar.
	 * 
	 * @param buttons
	 *            The buttons in the button bar; must not be <code>null</code>.
	 */
	protected void setButtons(Button[] buttons) {
		if (buttons == null) {
			throw new NullPointerException(
					"The array of buttons cannot be null."); //$NON-NLS-1$
		}
		this.buttons = buttons;
	}

	/**
	 * A mutator for the button labels.
	 * 
	 * @param buttonLabels
	 *            The button labels to use; must not be <code>null</code>.
	 */
	protected void setButtonLabels(String[] buttonLabels) {
		if (buttonLabels == null) {
			throw new NullPointerException(
					"The array of button labels cannot be null."); //$NON-NLS-1$
		}
		this.buttonLabels = buttonLabels;
	}
}
