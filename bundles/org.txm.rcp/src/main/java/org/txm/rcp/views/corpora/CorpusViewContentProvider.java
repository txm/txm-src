// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views.corpora;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.TXMResult;
import org.txm.objects.Project;
import org.txm.objects.Workspace;
import org.txm.rcp.CorporaSourceProvider;

/**
 * Content provider of the Corpus view.
 * 
 * @author mdecorde
 */
public class CorpusViewContentProvider implements ITreeContentProvider {

	private CorporaView view;

	public CorpusViewContentProvider(CorporaView view) {
		this.view = view;
	}

	/**
	 * Gets the adapter.
	 *
	 * @param element the element
	 * @return the adapter
	 */
	protected IWorkbenchAdapter getAdapter(Object element) {
		IWorkbenchAdapter adapter = null;

		if (element instanceof IAdaptable) {
			adapter = ((IAdaptable) element).getAdapter(IWorkbenchAdapter.class);
		}
		if (element != null && adapter == null) {
			adapter = (IWorkbenchAdapter) Platform.getAdapterManager().loadAdapter(element, IWorkbenchAdapter.class.getName());
		}
		return adapter;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
	 */
	@Override
	public Object[] getChildren(Object element) {
		IWorkbenchAdapter adapter = getAdapter(element);
		if (adapter != null) {
			Object[] children = adapter.getChildren(element);
			// FIXME: sort here the TXM result according to current sorting preference order, eg. getWeight(), getDate(), getName(), getClass()
			// Arrays.sort(children, new Comparator<Object>() {
			// @Override
			// public int compare(Object o1, Object o2) {
			// if (o1 == null) return -1;
			// if (o2 == null) return 1;
			// return o1.toString().compareTo(o2.toString());
			// }
			// });
			return children;
		}
		else if (element instanceof TXMResult) {
			if (element instanceof Project) {
				Project p = (Project) element;
				if (p.getRCPProject().isOpen()) {
					return p.getChildren().toArray();
				}
			}
			else {
				return ((TXMResult) element).getChildren().toArray();
			}
		}
		return new Object[0];
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	@Override
	public Object[] getElements(Object element) {
		// return element;
		//
		// if (element instanceof Workspace) {
		// ArrayList<MainCorpus> elements = new ArrayList<MainCorpus>();
		// Workspace p = (Workspace) element;
		//
		// for (Project b : p.getProjects()) {
		// for (TXMResult o : b.getChildren(MainCorpus.class)) {
		// elements.add((MainCorpus)o);
		// }
		// }
		// //System.out.println("get elements: "+elements);
		// Collections.sort(elements, new Comparator<MainCorpus>() {
		//
		// @Override
		// public int compare(MainCorpus o1, MainCorpus o2) {
		// if (o1 == null) return -1;
		// if (o2 == null) return 1;
		// return o1.getName().compareTo(o2.getName());
		// }
		// });
		// //System.out.println("corpora view content: "+elements);
		// return elements.toArray();
		// } else
		if (element instanceof Workspace && !TBXPreferences.getInstance().getBoolean(TBXPreferences.SHOW_ALL_RESULT_NODES)) {
			Workspace w = (Workspace) element;
			ArrayList<Object> elements = new ArrayList<>();
			for (Project p : w.getProjects()) {
				if (p.getRCPProject().isOpen()) {
					elements.addAll(p.getCorpora());
				}
			}

			// add projects of corpora projects & not the corpora project & not open
			IWorkspace rcpWorkspace = ResourcesPlugin.getWorkspace();
			IProject corporaDirectory = rcpWorkspace.getRoot().getProject("corpora"); //$NON-NLS-1$
			String path = corporaDirectory.getLocation().toOSString();
			IProject projects[];
			projects = rcpWorkspace.getRoot().getProjects();
			for (IProject project : projects) {
				String path2 = project.getLocation().toOSString();
				if (!project.isOpen() && path2.startsWith(path) && !path2.equals(path)) {
					elements.add(project);
				}
			}

			return elements.toArray();
		}
		else if (element instanceof TXMResult) {
			return ((TXMResult) element).getChildren().toArray();
		}
		else if (element instanceof List) {
			List list = (List) element;
			return list.toArray();
		}
		return new Object[0];
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
	 */
	@Override
	public Object getParent(Object element) {
		// IWorkbenchAdapter adapter = getAdapter(element);
		// if (adapter != null) {
		// return adapter.getParent(element);
		// }
		// else
		if (element instanceof TXMResult) {
			return ((TXMResult) element).getParent();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
	 */
	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof TXMResult) {
			return getChildren(element).length > 0;
		}
		else {
			int n = getChildren(element).length;
			return n > 0;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		CorporaSourceProvider.updateSelection(viewer.getSelection());
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {
	}
}
