package org.txm.rcp.editors;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.util.Util;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.utils.OSDetector;

/**
 * Install events for line search, copy, tooltips, etc.
 * 
 * TODO Move all this in a SuperTableViewer classes that will install automatically the following tools ?
 * 
 * @author mdecorde
 *
 */
public class TableUtils {

	/**
	 * the character to insert to insert new lines in Table header titles
	 * Windows: none since it don't work
	 */
	public static String headersNewLine = (OSDetector.isFamilyWindows()) ? " " : "\n";

	public static int MAX_CHARS_LENGTH_TO_SHOW = 200;

	/**
	 * Adds a mouse adapter to the specified table columns range that executes a command link specified by its id on double click event.
	 * 
	 * @param table
	 * @param startingColumn
	 * @param endingColumn
	 * @param commandId
	 */
	public static void addLineListener(TreeViewer viewer, TreeColumn column, MouseAdapter listener) {

		viewer.getControl().addMouseListener(new MouseAdapter() {

			@Override
			public void mouseUp(MouseEvent e) {

				Point mousePosition = new Point(e.x + viewer.getTree().getHorizontalBar().getSelection(), e.y);
				int pointedColumnIndex = getPointedColumn(viewer.getTree(), mousePosition);
				if (pointedColumnIndex == -1) return;
				TreeColumn columnPointed = viewer.getTree().getColumn(pointedColumnIndex);
				//				if (column.getText().equals(columnPointed.getText())) {
				if (column == columnPointed) {
					listener.mouseUp(e);
				}
				return;
			}
		});
	}
	
	/**
	 * Adds a mouse adapter to the specified table columns range that executes a command link specified by its id on double click event.
	 * 
	 * @param table
	 * @param startingColumn
	 * @param endingColumn
	 * @param commandId
	 */
	public static void addLineListener(TableViewer viewer, TableColumn column, MouseAdapter listener) {

		viewer.getControl().addMouseListener(new MouseAdapter() {

			@Override
			public void mouseUp(MouseEvent e) {

				Point mousePosition = new Point(e.x + viewer.getTable().getHorizontalBar().getSelection(), e.y);
				int pointedColumnIndex = getPointedColumn(viewer.getTable(), mousePosition);
				if (pointedColumnIndex == -1) return;
				TableColumn columnPointed = viewer.getTable().getColumn(pointedColumnIndex);
				//				if (column.getText().equals(columnPointed.getText())) {
				if (column == columnPointed) {
					listener.mouseUp(e);
				}
				return;
			}
		});
	}


	/**
	 * Adds a mouse adapter to the specified table column that executes a command link specified by its id on double click event.
	 * 
	 * @param table
	 * @param column
	 * @param commandId
	 */
	public static void addDoubleClickCommandListener(final Table table, final int column, final String commandId) {
		addDoubleClickCommandListener(table, column, column, commandId);
	}

	/**
	 * Adds a mouse adapter to the specified table columns range that executes a command link specified by its id on double click event.
	 * 
	 * @param table
	 * @param startingColumn
	 * @param endingColumn
	 * @param commandId
	 */
	public static void addDoubleClickCommandListener(final Table table, final int startingColumn, final int endingColumn, final String commandId) {

		table.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {

				Point mousePosition = new Point(e.x + table.getHorizontalBar().getSelection(), e.y);
				int pointedColumn = getPointedColumn(table, mousePosition);
				if (pointedColumn >= startingColumn && pointedColumn <= endingColumn) {
					BaseAbstractHandler.executeCommand(commandId);
				}
				return;
			}
		});
	}


	/**
	 * Adds a mouse adapter to the specified composite that executes a command link specified by its id on double click event.
	 * 
	 * @param commandId
	 */
	public static void addDoubleClickCommandListener(Composite composite, final String commandId) {

		composite.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {

				BaseAbstractHandler.executeCommand(commandId);
			}
		});
	}
	
	/**
	 * Gets the pointed column.
	 *
	 * @param mouseposition the mouseposition
	 * @return the pointed column
	 */
	public static int getPointedColumn(Table table, Point mouseposition) {

		int x = mouseposition.x; // + lineTableViewer.getTable().get;
		int sumWidthColumn = 0;
		for (int i = 0; i < table.getColumnCount(); i++) {
			TableColumn col = table.getColumn(i);
			sumWidthColumn += col.getWidth();
			if (x < sumWidthColumn) {
				return i;
			}
		}

		return -1;
	}
	
	/**
	 * Gets the pointed column.
	 *
	 * @param mouseposition the mouseposition
	 * @return the pointed column
	 */
	public static int getPointedColumn(Tree tree, Point mouseposition) {

		int x = mouseposition.x; // + lineTableViewer.getTable().get;
		int sumWidthColumn = 0;
		for (int i = 0; i < tree.getColumnCount(); i++) {
			TreeColumn col = tree.getColumn(i);
			sumWidthColumn += col.getWidth();
			if (x < sumWidthColumn) {
				return i;
			}
		}

		return -1;
	}

	/**
	 * To increase decrease column widths
	 * 
	 * @param viewers
	 */
	public static void installColumnResizeMouseWheelEvents(TableViewer... viewers) {
		for (TableViewer viewer : viewers) {
			if (viewer.getTable().isDisposed()) continue;

			installColumnResizeMouseWheelEvents(viewer, true, true);
		}
	}

	/**
	 * Resize columns using mouse wheel
	 * 
	 * CTRL : only the hovered column
	 * CTRL + SHIFT : all columns
	 * 
	 * @param viewer
	 * @param currentEvent activate the CTRL event
	 * @param allEvent activate the CTRL+SHIFT event
	 */
	public static void installColumnResizeMouseWheelEvents(TableViewer viewer, boolean currentEvent, boolean allEvent) {

		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
			// COLUMN RESIZE
			viewer.getTable().addListener(SWT.MouseVerticalWheel, new Listener() {

				@Override
				public void handleEvent(Event e) {
					e.doit = (e.stateMask & SWT.CTRL) == 0; // we'll manage the CTRL events
				}
			});
			viewer.getTable().addMouseWheelListener(new MouseWheelListener() {

				@Override
				public void mouseScrolled(MouseEvent e) {

					if ((e.stateMask & SWT.CTRL) != 0) {

						if ((e.stateMask & SWT.SHIFT) != 0) {
							for (TableColumn col : viewer.getTable().getColumns()) {
								if (col.getText().length() > 0) {

									int ns = (int) (col.getWidth() * (1 + (e.count / 100.0f)));

									if (ns == col.getWidth()) ns++;
									if (ns == 0) ns = 1;

									col.setWidth(ns);
								}
							}
						}
						else {
							int s = 0;
							for (TableColumn col : viewer.getTable().getColumns()) {

								if (s < e.x && e.x < (s + col.getWidth())) {
									int ns = (int) (col.getWidth() * (1 + (2 * e.count / 100.0f)));

									if (ns == col.getWidth()) ns++;
									if (ns == 0) ns = 1;

									col.setWidth(ns);
									return;
								}
								s += col.getWidth();
							}
						}
					}
				}
			});
		}
	}

	/**
	 * Adds Key events managing : COPY_IN_CLIPBOARD and SEARCH_IN_CELLS Tools
	 * 
	 * @param viewers
	 */
	public static TableKeyListener installCopyAndSearchKeyEvents(TableViewer... viewers) {
		TableKeyListener listener = new TableKeyListener(viewers);

		for (TableViewer viewer : viewers) {
			if (viewer.getTable().isDisposed()) continue;

			viewer.getTable().addKeyListener(listener);
		}

		return listener;
	}


	/**
	 * Adds Tooltips using input elements .toString() method to fill the tooltip content
	 * 
	 * The tooltip is built using the table selection
	 * 
	 * @param viewers
	 */
	public static void installTooltipsOnSelectionChangedEvent(TableViewer... viewers) {

		for (TableViewer viewer : viewers) {

			if (viewer.getTable().isDisposed()) continue;

			viewer.addSelectionChangedListener(new ISelectionChangedListener() {

				@Override
				public void selectionChanged(SelectionChangedEvent event) {

					IStructuredSelection sel = event.getStructuredSelection();
					String tooltip = StringUtils.join(sel.toList(), "\n");
					if (sel.isEmpty()) {
						tooltip = null;
					}
					else {
						tooltip = StringUtils.join(sel.toList(), "\n");
					}

					for (TableViewer viewer : viewers) {
						if (viewer.getTable().isDisposed()) continue;

						viewer.getTable().setToolTipText(tooltip);
					}
				}
			});
		}
	}

	/**
	 * Adds Tooltips using input elements .toString() method to fill the tooltip content
	 * 
	 * This V2 version don't use the input data but the cell label formater
	 * 
	 * The tooltip is built using the table selection
	 * 
	 * @param viewers
	 */
	public static void installTooltipsV2OnSelectionChangedEvent(TableViewer... viewers) {

		for (TableViewer viewer : viewers) {

			if (viewer.getTable().isDisposed()) continue;

			viewer.addSelectionChangedListener(new ISelectionChangedListener() {

				@Override
				public void selectionChanged(SelectionChangedEvent event) {

					IStructuredSelection sel = event.getStructuredSelection();
					String tooltip = StringUtils.join(sel.toList(), "\n");
					if (sel.isEmpty()) {
						tooltip = null;
					}
					else {
						tooltip = "";
						for (Object element : sel.toList()) {
							for (TableViewer viewer : viewers) {
								if (viewer.getTable().isDisposed()) continue;

								for (int i = 0; i < viewer.getTable().getColumnCount(); i++) {
									if (viewer.getTable().getColumn(i).getText().length() > 0) {
										if (viewer.getLabelProvider(i) instanceof ColumnLabelProvider clp) {
											tooltip += " " + clp.getText(element);
										}
									}
								}
								break;
							}
							tooltip += "\n";
						}
						if (tooltip.length() == 0) {
							tooltip = StringUtils.join(sel.toList(), "\n");
						}
					}

					for (TableViewer viewer : viewers) {
						if (viewer.getTable().isDisposed()) continue;

						viewer.getTable().setToolTipText(tooltip);
					}
				}
			});
		}
	}

	/**
	 * Install listeners to enable vertical synchronisation between all given tables
	 * @param set 
	 * 
	 * @param viewers
	 */
	public static void align(TableViewer viewer, Collection<TableViewer> otherViewers) {
		for (TableViewer col : otherViewers) {
			if (col.getTable().isDisposed()) continue;

			if (col.getTable() != viewer.getTable()) {
				col.getTable().deselectAll();
				col.getTable().select(viewer.getTable().getSelectionIndices());
			}
		}
	}
	
	/**
	 * Install listeners to enable vertical synchronisation between all given tables
	 * @param set 
	 * 
	 * @param viewers
	 */
	public static void align(TableViewer viewer, TableViewer... otherViewers) {
		
		for (TableViewer otherViewer : otherViewers) {
			if (otherViewer.getTable().isDisposed()) continue;

			if (otherViewer.getTable() != viewer.getTable()) {
				otherViewer.getTable().deselectAll();
				otherViewer.getTable().select(viewer.getTable().getSelectionIndices());
				
				if (Util.isLinux()) {
					otherViewer.getTable().getVerticalBar().setSelection(viewer.getTable().getVerticalBar().getSelection());
				}
				else {
					otherViewer.getTable().setTopIndex(viewer.getTable().getTopIndex());
				}
			}
		}
	}
	
	/**
	 * Install listeners to enable vertical synchronisation between all given tables
	 * 
	 * @param viewers
	 */
	public static void installVerticalSynchronisation(TableViewer... viewers) {

		for (TableViewer viewer : viewers) {
			Table columnTable = viewer.getTable();

			// mouse&keyboard selection
			columnTable.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}

				@Override
				public void widgetSelected(SelectionEvent e) {
					align(viewer, viewers);
				}
			});

			// scrollbar selection
			ScrollBar vBarLeft = columnTable.getVerticalBar();
			
			if (Util.isMac()) { // Mac does not send the selection event when the mouse is scrolled
				columnTable.addMouseWheelListener(new MouseWheelListener() {
					
					@Override
					public void mouseScrolled(MouseEvent e) {
						for (TableViewer col : viewers) {

							if (col.getTable().isDisposed()) continue;

							if (col.getTable() != columnTable) {
								if (Util.isLinux()) {
									col.getTable().getVerticalBar().setSelection(vBarLeft.getSelection());
								}
								else {
									col.getTable().setTopIndex(columnTable.getTopIndex());
								}
							}
						}
					}
				});
			}
			
			vBarLeft.addListener(SWT.Selection, new Listener() {

				@Override
				public void handleEvent(Event event) {
					for (TableViewer col : viewers) {

						if (col.getTable().isDisposed()) continue;

						if (col.getTable() != columnTable) {
							if (Util.isLinux()) {
								col.getTable().getVerticalBar().setSelection(vBarLeft.getSelection());
							}
							else {
								col.getTable().setTopIndex(columnTable.getTopIndex());
							}
						}
					}
				}
			});
		}
	}

	/**
	 * Packs the columns of the specified table viewer so the label of the sort column is not truncated due of the sorting order arrow display.
	 * 
	 * 
	 * @param viewer the table viewer
	 */
	public static void packColumns(ColumnViewer viewer) {

		if (viewer instanceof TableViewer tableviewer) {
			packColumns(tableviewer, true, true);
		}
		else if (viewer instanceof TreeViewer treeviewer) {
			packColumns(treeviewer, true, true);
		}
	}

	public static final int MAX_NAME_LENGTH = 40;

	public static final int NMAXLINESTOTEST = 200;

	/**
	 * Packs the columns of the specified table viewer so the label of the sort column is not truncated due of the sorting order arrow display.
	 * 
	 * @param viewer the table viewer
	 */
	public static void packColumns(TableViewer viewer, boolean useHeader, boolean useContent) {

		// Refresh and pack the columns
		viewer.getControl().setRedraw(false);
		viewer.refresh();

		GC gc = new GC(viewer.getTable());
		gc.setFont(viewer.getTable().getFont());

		TableColumn[] columns = viewer.getTable().getColumns();
		TableColumn sortedColumn = viewer.getTable().getSortColumn();
		int ncolumns = columns.length;
		int nlinestotest = viewer.getTable().getItemCount();
		if (nlinestotest > NMAXLINESTOTEST) nlinestotest = NMAXLINESTOTEST;

		Font font = viewer.getTable().getFont();
		FontData fd = font.getFontData()[0];

		//		float W = fd.height * COLUMNS_WIDTH_MULTIPLIER;

		// set width of each column
		for (int i = 0; i < ncolumns; i++) {
			TableColumn col = columns[i];
			int refMax = 0;
			String ref = "";
			if (useHeader) {
				refMax = gc.textExtent(col.getText()).x;
				ref = col.getText();
			}

			if (useContent) {
				for (int j = 0; j < nlinestotest; j++) { // get lines one per one to avoid loading the full table lines
					TableItem item = viewer.getTable().getItem(j);
					//if (ref.length() < item.getText(i).length()) {
					int x = gc.textExtent(item.getText(i)).x;
					if (refMax < x) {
						refMax = gc.textExtent(item.getText(i)).x;
						ref = item.getText(i);
					}
				}

				if (nlinestotest == 0) { // if no line is set use the header
					refMax = gc.textExtent(col.getText()).x;
					ref = col.getText();
				}
			}

			if (ref.length() > MAX_CHARS_LENGTH_TO_SHOW) {
				ref = ref.substring(0, MAX_CHARS_LENGTH_TO_SHOW);
			}

			if (ref.length() < 3 && col.getText().length() > 2) {
				refMax = gc.textExtent(col.getText()).x;
				ref = col.getText();
			}

			int w = refMax;
			if (w != 0) {

				if (ref.length() == 0) {
					w = 1;
				}
				else {
					w += (int) (2 * fd.height);
				}
			}

			if (col.equals(sortedColumn)) { // the sorted column has an extra marker to show
				w += 30;
			}

			if (w == 0 && col.getData("separator") != null) w = 1;
			col.setWidth(w);
		}

		gc.dispose();

		viewer.getControl().setRedraw(true);
		viewer.getControl().update();
	}

	/**
	 * Packs the columns of the specified table viewer so the label of the sort column is not truncated due of the sorting order arrow display.
	 * 
	 * @param viewer the table viewer
	 */
	public static void packColumns(TreeViewer viewer, boolean useHeader, boolean useContent) {

		// Refresh and pack the columns
		viewer.getControl().setRedraw(false);
		viewer.refresh();

		GC gc = new GC(viewer.getTree());
		gc.setFont(viewer.getTree().getFont());

		TreeColumn[] columns = viewer.getTree().getColumns();
		TreeColumn sortedColumn = viewer.getTree().getSortColumn();
		int ncolumns = columns.length;
		int nlinestotest = viewer.getTree().getItemCount();
		if (nlinestotest > NMAXLINESTOTEST) nlinestotest = NMAXLINESTOTEST;

		Font font = viewer.getTree().getFont();
		FontData fd = font.getFontData()[0];

		// set width of each column
		for (int i = 0; i < ncolumns; i++) {
			TreeColumn col = columns[i];
			int refMax = 0;
			String ref = "";
			if (useHeader) {
				refMax = gc.textExtent(col.getText()).x;
				ref = col.getText();
			}

			if (useContent) {
				for (int j = 0; j < nlinestotest; j++) { // get lines one per one to avoid loading the full table lines
					TreeItem item = viewer.getTree().getItem(j);
					
					if (ref.length() < item.getText(i).length()) {
						refMax = gc.textExtent(item.getText(i)).x;
						ref = item.getText(i);
					}
				}

				if (nlinestotest == 0) { // if no line is set use the header
					refMax = gc.textExtent(col.getText()).x;
					ref = col.getText();
				}
			}

			if (ref.length() > MAX_CHARS_LENGTH_TO_SHOW) {
				ref = ref.substring(0, MAX_CHARS_LENGTH_TO_SHOW);
			}

			if (ref.length() < 3 && col.getText().length() > 2) {
				refMax = gc.textExtent(col.getText()).x;
				ref = col.getText();
			}

			int w = refMax;
			if (w != 0) {

				if (ref.length() == 0) {
					w = 1;
				}
				else {
					w += (int) (2 * fd.height);
				}
			}

			if (col.equals(sortedColumn)) { // the sorted column has an extra marker to show
				w += 30;
			}

			if (w == 0 && col.getData("separator") != null) w = 1;
			col.setWidth(w);
		}

		gc.dispose();

		viewer.getControl().setRedraw(true);
		viewer.getControl().update();
	}

	/**
	 * Refresh the columns after updating line sort
	 * 
	 * @param viewer the table viewer
	 */
	public static void refreshColumns(TableViewer viewer) {
		//		viewer.getControl().setRedraw(false);
		viewer.refresh();
		//		viewer.getControl().setRedraw(true);
		//		viewer.getControl().update();
	}
}
