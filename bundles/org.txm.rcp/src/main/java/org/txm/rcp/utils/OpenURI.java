// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.utils;

import java.io.File;
import java.io.IOException;

import org.eclipse.osgi.util.NLS;
import org.txm.rcp.StatusLine;
import org.txm.rcp.messages.TXMUIMessages;

// TODO: Auto-generated Javadoc
/**
 * The Class OpenURI.
 */
public class OpenURI {

	/**
	 * Open.
	 *
	 * @param file the file
	 * @return true, if successful
	 */
	public static boolean open(File file) {

		try {
			if (!java.awt.Desktop.isDesktopSupported()) {
				System.err.println(TXMUIMessages.desktopIsNotSupportedFatal);
				return false;
			}

			java.awt.Desktop desktop = java.awt.Desktop.getDesktop();

			if (desktop.isSupported(java.awt.Desktop.Action.OPEN)) {

				desktop.open(file);
				return true;
			}
			else if (desktop.isSupported(java.awt.Desktop.Action.EDIT)) {
				desktop.edit(file);
				return true;
			}
			else if (desktop.isSupported(java.awt.Desktop.Action.BROWSE)) {
				desktop.browse(file.toURI());
				return true;
			}
		}
		catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			StatusLine.error(NLS.bind(TXMUIMessages.errorWhileOpeningFileP0, file));
		}
		return false;
	}

	/**
	 * Browse.
	 *
	 * @param file the file
	 * @return true, if successful
	 */
	public static boolean browse(File file) {
		if (!java.awt.Desktop.isDesktopSupported()) {
			System.err.println(TXMUIMessages.desktopIsNotSupportedFatal);
			return false;
		}

		java.awt.Desktop desktop = java.awt.Desktop.getDesktop();

		if (desktop.isSupported(java.awt.Desktop.Action.BROWSE)) {
			try {
				desktop.browse(file.toURI());
			}
			catch (IOException e) {
				System.out.println(e.getLocalizedMessage());
				return false;
			}
			return true;
		}
		return false;
	}

}
