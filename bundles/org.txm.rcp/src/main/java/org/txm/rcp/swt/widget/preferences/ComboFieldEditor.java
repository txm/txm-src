package org.txm.rcp.swt.widget.preferences;

import org.eclipse.swt.widgets.Composite;

/**
 * Add access to the internal Button
 */
public class ComboFieldEditor extends org.eclipse.jface.preference.ComboFieldEditor {
	
	Composite parent;
	
	public ComboFieldEditor(String preference, String label, String[][] items, Composite parent) {
		super(preference, label, items, parent);
		this.parent = parent;
	}

	public void setToolTipText(String tooltip) {
		
		getLabelControl(parent).setToolTipText(tooltip);
	}
	
	public void setEnabled(boolean b) {
		
		super.setEnabled(b, parent);
	}
}
