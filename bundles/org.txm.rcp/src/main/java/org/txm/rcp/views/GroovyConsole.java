// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.txm.rcp.messages.TXMUIMessages;

// TODO: Auto-generated Javadoc
/**
 * not used yet, must display the groovy logs only @ author mdecorde.
 */
public class GroovyConsole extends ViewPart implements Runnable {

	/** The ID. */
	public static String ID = GroovyConsole.class.getName();

	/** The text area. */
	private static StyledText textArea;

	/** The out. */
	private static PrintStream out;

	/** The err. */
	private static PrintStream err;

	/** The quit. */
	private boolean quit;

	/** The reader. */
	private Thread reader;

	/** The reader2. */
	private Thread reader2;

	/** The pin. */
	PipedInputStream pin = new PipedInputStream();

	/** The pin2. */
	PipedInputStream pin2 = new PipedInputStream();

	/** The output. */
	static PrintStream output = System.out;

	/** The errput. */
	static PrintStream errput = System.err;

	/**
	 * Instantiates a new groovy console.
	 */
	public GroovyConsole() {
		try {
			PipedOutputStream pout = new PipedOutputStream(pin);
			out = new PrintStream(pout, true);

			new PipedOutputStream(pin2);
			err = new PrintStream(pout, true);

		}
		catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}

		reader = new Thread(this);
		reader.setDaemon(true);
		reader.start();
		reader2 = new Thread(this);
		reader2.setDaemon(true);
		reader2.start();
	}

	/**
	 * Redirect.
	 *
	 * @param b the b
	 */
	public static void redirect(boolean b) {
		if (b) {
			System.setOut(out);
			System.setErr(err);
		}
		else {
			System.setOut(output);
			System.setErr(errput);
		}
	}

	/**
	 * Lecture de la sortie standard.
	 *
	 * @param in the in
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	protected synchronized String readLine(PipedInputStream in)
			throws IOException {
		String input = ""; //$NON-NLS-1$
		do {
			int available = in.available();
			if (available == 0)
				break;
			byte b[] = new byte[available];
			in.read(b);
			input = input + new String(b, 0, b.length);
		}
		while (!input.endsWith("\n") && !input.endsWith("\r\n") && !quit); //$NON-NLS-1$ //$NON-NLS-2$
		return input;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public synchronized void run() {
		try {
			while (Thread.currentThread() == reader) {
				try {
					this.wait(100);
				}
				catch (InterruptedException ie) {
				}
				if (pin.available() != 0) {
					String input = this.readLine(pin);
					textArea.append(input);
				}
				if (quit)
					return;
			}

			while (Thread.currentThread() == reader2) {
				try {
					this.wait(100);
				}
				catch (InterruptedException ie) {
				}
				if (pin2.available() != 0) {
					String input = this.readLine(pin2);
					textArea.append(input);
				}

				if (quit)
					return;
			}
		}
		catch (Exception e) {
			textArea.append(TXMUIMessages.consoleReportsAnInternalError);
			textArea.append(TXMUIMessages.theErrorIsColon + e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		textArea = new StyledText(parent, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL
				| SWT.H_SCROLL);
		textArea.setEditable(false);

		textArea.setLayoutData(new GridData(GridData.FILL_BOTH
				| GridData.GRAB_HORIZONTAL | GridData.GRAB_VERTICAL));
	}

	/**
	 * Println.
	 *
	 * @param txt the txt
	 */
	public static void println(String txt) {
		textArea.append(txt + "\n"); //$NON-NLS-1$
	}

	/**
	 * Prints the.
	 *
	 * @param txt the txt
	 */
	public static void print(String txt) {
		textArea.append(txt);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

}
