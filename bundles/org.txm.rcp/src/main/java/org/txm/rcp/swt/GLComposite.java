package org.txm.rcp.swt;

import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Layout;
import org.txm.utils.logger.Log;

/**
 * A Composite with a GridLayout set by default. Margins are set to a minimum.
 * 
 * @author mdecorde
 *
 */
public class GLComposite extends Composite {

	/**
	 * The grid layout.
	 */
	private GridLayout gl;

	/**
	 * The name (only used for debug purpose).
	 */
	//FIXME: SJ: should stop to use this. It's only used for debug purpose on a view.
	private String name;

	
	/**
	 * 
	 * @param parent
	 * @param style
	 * @param name
	 */
	public GLComposite(Composite parent, int style, String name) {
		super(parent, style);
		this.gl = createDefaultLayout(1);
		this.name = name;
		super.setLayout(this.gl);
	}

	/**
	 * 
	 * @param parent
	 * @param style
	 */
	public GLComposite(Composite parent, int style) {
		this(parent, style, "unnamed"); //$NON-NLS-1$
	}
	
	
	@Override
	public final GridLayout getLayout() {
		return this.gl;
	}

	public String getName() {
		return this.name;
	}

	
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Warning : this composite layout cannot be changed
	 */
	@Override
	public final void setLayout(Layout layout) {
		Log.warning("Warning: GLComposite.setLayout is called without effect: " + this.name); //$NON-NLS-1$
		Thread.dumpStack();
		//super.setLayout(layout);
	}

	/**
	 * Creates a default grid layout with minimum margins.
	 * 
	 * @param numColumns
	 * @return
	 */
	public static GridLayout createDefaultLayout(int numColumns) {
		GridLayout gl = new GridLayout();
		gl.marginBottom = 0;
		gl.marginHeight = 0;
		gl.marginWidth = 0;
		gl.marginTop = 0;
		gl.horizontalSpacing = 1;
		gl.verticalSpacing = 0;
		gl.numColumns = numColumns;
		return gl;
	}
}
