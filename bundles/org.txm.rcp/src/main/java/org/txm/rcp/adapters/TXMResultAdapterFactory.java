// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.rcp.adapters;

import java.util.Arrays;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.eclipse.ui.model.IWorkbenchAdapter2;


/**
 * A factory for creating Adapter objects.
 * Adapters are used to define icon and label in the tree of the corpus view.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public abstract class TXMResultAdapterFactory implements IAdapterFactory {

	private static final Class<?>[] ADAPTER_LIST = new Class<?>[] { IWorkbenchAdapter.class, IWorkbenchAdapter2.class };

	/**
	 * Gets the adapter list.
	 *
	 * @return the adapter list
	 * @see org.eclipse.core.runtime.IAdapterFactory#getAdapterList()
	 */
	@Override
	public Class<?>[] getAdapterList() {
		return ADAPTER_LIST;
	}

	/**
	 * Checks if the factory manage the specified adapter type.
	 * 
	 * @param adapterType
	 * @return
	 */
	protected boolean canAdapt(Class<?> adapterType) {
		return Arrays.asList(this.getAdapterList()).contains(adapterType);
	}
}
