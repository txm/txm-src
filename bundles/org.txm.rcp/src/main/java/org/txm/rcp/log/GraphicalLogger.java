// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.log;

import org.txm.utils.logger.LocalizedLogger;

/**
 * The Class GraphicalLogger.
 *
 * @deprecated @ author mdecorde
 */
@Deprecated
public class GraphicalLogger extends LocalizedLogger {
	/*
	 * public GraphicalLogger(Class<?> clazz) { super(clazz); }
	 * private void dialog (String message) {
	 * MessageDialog.openError(PlatformUI.
	 * getWorkbench().getDisplay().getActiveShell(), Messages.GraphicalLogger_0,
	 * message); }
	 * private void dialog (String message, Throwable t) { if (t instanceof
	 * CqiClientException){ if (t.getCause() instanceof CqiServerError)
	 * message+=Messages
	 * .GraphicalLogger_1+t.getCause().getClass().getSimpleName(
	 * )+')'+Messages.GraphicalLogger_2+t.getCause().getMessage();
	 * if (t.getCause() instanceof SocketException)
	 * message+=Messages.GraphicalLogger_3; } if (t instanceof StatException){
	 * message+=Messages.GraphicalLogger_4; } if (t instanceof
	 * GroovyExecutionException){
	 * message+=Messages.GraphicalLogger_5+t.getCause(); }
	 * MessageDialog.openError
	 * (PlatformUI.getWorkbench().getDisplay().getActiveShell(),
	 * Messages.GraphicalLogger_0, message); }
	 * @Override public void error(String key, Object arg, Throwable t) {
	 * dialog(super.getLocalizer().message(key),t); super.error(key, arg, t); }
	 * @Override public void error(String key, Object arg) {
	 * dialog(super.getLocalizer().message(key)); super.error(key, arg); }
	 * @Override public void error(String key, Object[] arg, Throwable t) {
	 * dialog(super.getLocalizer().message(key),t); super.error(key, arg, t); }
	 * @Override public void error(String key, Object[] arg) {
	 * dialog(super.getLocalizer().message(key)); super.error(key, arg); }
	 * @Override public void error(String key, Throwable t) {
	 * dialog(super.getLocalizer().message(key),t); super.error(key, t); }
	 * @Override public void error(String key) {
	 * dialog(super.getLocalizer().message(key)); super.error(key); }
	 * @Override public void fatal(String key, Object arg, Throwable t) {
	 * dialog(super.getLocalizer().message(key),t); super.fatal(key, arg, t); }
	 * @Override public void fatal(String key, Object arg) {
	 * dialog(super.getLocalizer().message(key)); super.fatal(key, arg); }
	 * @Override public void fatal(String key, Object[] arg, Throwable t) {
	 * dialog(super.getLocalizer().message(key),t); super.fatal(key, arg, t); }
	 * @Override public void fatal(String key, Object[] arg) {
	 * dialog(super.getLocalizer().message(key)); super.fatal(key, arg); }
	 * @Override public void fatal(String key, Throwable t) {
	 * dialog(super.getLocalizer().message(key),t); super.fatal(key, t); }
	 * @Override public void fatal(String key) {
	 * dialog(super.getLocalizer().message(key)); super.fatal(key); }
	 */
}
