package org.txm.rcp.swt.widget.structures;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.dialog.LargeInputDialog;
import org.txm.rcp.utils.IOClipboard;

public class SuperListViewer extends GLComposite {

	ListViewer valueCombo;

	Button selectByListButton;

	String lastValuesSelected = ""; //$NON-NLS-1$

	Button selectByRegex;

	String lastRegexSelected = ""; //$NON-NLS-1$

	public SuperListViewer(Composite parent, int listStyle) {

		super(parent, SWT.NONE, "SuperListViewer"); //$NON-NLS-1$

		this.getLayout().numColumns = 2;
		this.getLayout().verticalSpacing = 1;
		this.getLayout().horizontalSpacing = 1;

		valueCombo = new ListViewer(this, listStyle);
		valueCombo.setContentProvider(new ArrayContentProvider());
		valueCombo.getList().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 1, 5));
		int operations = DND.DROP_COPY | DND.DROP_MOVE;
		Transfer[] transferTypes = new Transfer[] { TextTransfer.getInstance() };
		valueCombo.addDropSupport(operations, transferTypes, new DropTargetAdapter() {

			@Override
			public void drop(DropTargetEvent event) {
				//System.out.println("DROP: "+event);

				if (event.data instanceof String) {
					selectByString((String) event.data);
				}
			}
		});
		valueCombo.getList().addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {

				if ((e.stateMask & SWT.CTRL) != 0 && e.keyCode == 99) { // CTRL + C
					String s = StringUtils.join(valueCombo.getList().getSelection(), "\n");
					IOClipboard.write(s);
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		selectByListButton = new Button(this, SWT.PUSH);
		selectByListButton.setToolTipText(TXMUIMessages.selectUsingAListOfValues);
		selectByListButton.setImage(IImageKeys.getImage("icons/functions/data.png")); //$NON-NLS-1$
		selectByListButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		selectByListButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				InputDialog dialog = new LargeInputDialog(e.display.getActiveShell(), TXMUIMessages.selectUsingAListOfValues,
						TXMUIMessages.listOfValuesToSelect + "\n\nValues must be separated with tabulation or new line", lastValuesSelected, null);

				if (dialog.open() == InputDialog.OK) {
					String str = dialog.getValue();
					str = str.trim();
					selectByString(str);
					lastValuesSelected = str;
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		selectByRegex = new Button(this, SWT.PUSH);
		selectByRegex.setToolTipText(TXMUIMessages.selectValuesUsingARegularExpression);
		selectByRegex.setImage(IImageKeys.getImage(IImageKeys.ACTION_SEARCH));
		selectByRegex.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, false, false));
		selectByRegex.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				InputDialog dialog = new InputDialog(e.display.getActiveShell(), TXMUIMessages.selectValuesUsingARegularExpression, TXMUIMessages.regularExpressionToUse, lastRegexSelected, null);
				if (dialog.open() == InputDialog.OK) {
					String str = dialog.getValue();
					str = str.trim();
					if (str.length() == 0) return;

					selectByRegex(str);
					lastRegexSelected = str;
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Button allButton = new Button(this, SWT.PUSH);
		allButton.setToolTipText(TXMUIMessages.selectAll);
		allButton.setImage(IImageKeys.getImage(IImageKeys.CHECKEDALL));
		allButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, false, false));
		allButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				valueCombo.getList().selectAll();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Button clearButton = new Button(this, SWT.PUSH);
		clearButton.setToolTipText(TXMUIMessages.deselectAll);
		clearButton.setImage(IImageKeys.getImage(IImageKeys.UNCHECKEDALL));
		clearButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, false, false));
		clearButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				valueCombo.getList().deselectAll();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Button invertAllButton = new Button(this, SWT.PUSH);
		invertAllButton.setToolTipText("Invert selection");
		invertAllButton.setImage(IImageKeys.getImage(IImageKeys.REVERSECHECKED));
		invertAllButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, false, false));
		invertAllButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				ArrayList<String> newSelection = new ArrayList<String>(Arrays.asList(valueCombo.getList().getItems()));
				newSelection.removeAll(Arrays.asList(valueCombo.getList().getSelection()));

				String[] listValues = valueCombo.getList().getItems();
				ArrayList<Integer> indices = new ArrayList<>();
				for (String v : newSelection) {
					int i = Arrays.binarySearch(listValues, v);
					if (i >= 0) {
						indices.add(i);
					}
				}
				int[] iindices = new int[indices.size()];
				for (int i = 0; i < indices.size(); i++)
					iindices[i] = indices.get(i);

				valueCombo.getList().deselectAll();
				valueCombo.getList().select(iindices);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}
	
	public void setToolTipText(String tooltip) {
		valueCombo.getControl().setToolTipText(tooltip);
	}

	public void setLabelProvider(IBaseLabelProvider labelProvider) {
		valueCombo.setLabelProvider(labelProvider);
	}

	public void selectAll() {
		valueCombo.getList().selectAll();
	}

	public void deSelectAll() {
		valueCombo.getList().deselectAll();
	}

	public void selectByRegex(String regex) {
		String[] listValues = valueCombo.getList().getItems();
		ArrayList<Integer> indices = new ArrayList<>();

		for (int i = 0; i < listValues.length; i++) {
			if (listValues[i].matches(regex)) {
				indices.add(i);
			}
		}

		int[] iindices = new int[indices.size()];
		for (int i = 0; i < indices.size(); i++)
			iindices[i] = indices.get(i);

		valueCombo.getList().deselectAll();
		valueCombo.getList().select(iindices);
	}

	public void selectByString(String str) {
		if (str == null) return;
		if (str.length() == 0) return;

		selectByStringValues(str.split("[\t\n|]")); //$NON-NLS-1$
	}

	public void selectByStringValues(String[] values) {
		String[] listValues = valueCombo.getList().getItems();
		ArrayList<Integer> indices = new ArrayList<>();
		for (String v : values) {
			int i = Arrays.binarySearch(listValues, v);
			if (i >= 0) {
				indices.add(i);
			}
		}
		int[] iindices = new int[indices.size()];
		for (int i = 0; i < indices.size(); i++)
			iindices[i] = indices.get(i);

		valueCombo.getList().deselectAll();
		valueCombo.getList().select(iindices);
	}

	public void setInput(Object input) {
		valueCombo.setInput(input);
	}

	public ListViewer getListviewer() {

		return valueCombo;
	}

	public List getList() {

		return valueCombo.getList();
	}

	public IStructuredSelection getSelection() {

		return valueCombo.getStructuredSelection();
	}

}
