package org.txm.rcp.menu;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

import org.eclipse.jface.action.ContributionItem;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.txm.Toolbox;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.TXMWindows;
import org.txm.rcp.handlers.scripts.ExecuteGroovyMacro;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.utils.logger.Log;

public class MacrosMenuContribution extends ContributionItem {

	Menu menu;

	int index;

	public MacrosMenuContribution() {
		super();
	}

	public MacrosMenuContribution(String id) {
		super(id);
	}

	@Override
	public void fill(Menu menu, int index) {
		this.menu = menu;
		this.index = index;


		menu.addMenuListener(new MenuListener() {

			@Override
			public void menuShown(MenuEvent e) {
				onOpen(MacrosMenuContribution.this.menu, MacrosMenuContribution.this.index);
			}

			@Override
			public void menuHidden(MenuEvent e) {
			}
		});
	}

	public File getMacroDirectory() {
		//create the menu item
		String w = Toolbox.getTxmHomePath();
		if (w == null || w.length() == 0) return null;

		return new File(w, "scripts/groovy/user/org/txm/macro/"); //$NON-NLS-1$
	}

	public void onOpen(Menu menu, int index) {

		File macroDirectory = getMacroDirectory();
		if (macroDirectory == null) return;

		//System.out.println(macroDirectory);
		File[] files = macroDirectory.listFiles();
		//System.out.println(files.length);
		if (files == null || files.length == 0) {

			MenuItem[] menuItems = menu.getItems();
			for (int i = 0; i < menuItems.length; i++) {
				menuItems[i].dispose();
			}

			MenuItem menuItem = new MenuItem(menu, SWT.CHECK, index);
			menuItem.setText("<no macro>"); //$NON-NLS-1$
			menuItem.addSelectionListener(new SelectionAdapter() {

				public void widgetSelected(SelectionEvent e) {
					File dir = new File(Toolbox.getTxmHomePath(), "scripts/groovy/user/" + getId().replace(".", "/")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					Log.info(NLS.bind(TXMUIMessages.ToPopulateTthisMenuCreateMacrosInTheP0MacroDirectory, dir));
				}
			});
			return;
		}

		populate(menu, macroDirectory, index);
	}

	public static void populate(Menu menu, File directory, int index) {

		// First empty
		MenuItem[] menuItems = menu.getItems();
		for (int i = 0; i < menuItems.length; i++) {
			menuItems[i].dispose();
		}

		// Find files
		File[] files = directory.listFiles();

		// first are directories
		Arrays.sort(files, new Comparator<File>() {

			@Override
			public int compare(File arg0, File arg1) {
				if (arg0.isDirectory()) {
					if (arg1.isDirectory()) {
						return arg0.getName().compareTo(arg1.getName());
					}
					else {
						return -1;
					}
				}
				else if (arg1.isFile()) {
					return arg0.getName().compareTo(arg1.getName());
				}
				else {
					return 1;
				}
			}

		});
		for (File f : files) {
			if (f.getName().endsWith("Macro.groovy")) { //$NON-NLS-1$
				MenuItem menuItem = new MenuItem(menu, SWT.PUSH);
				String name = f.getName().substring(0, f.getName().length() - 12);
				name = name.replaceAll("([0-9]++)", " $1 "); //$NON-NLS-1$ //$NON-NLS-2$
				name = name.replaceAll("(To)", " To "); //$NON-NLS-1$ //$NON-NLS-2$
				name = name.replaceAll("([A-Z]++)([^A-Z])", " $1$2"); //$NON-NLS-1$ //$NON-NLS-2$

				menuItem.setText(name);
				menuItem.setImage(IImageKeys.getImage(IImageKeys.SCRIPT_RUN));
				menuItem.addSelectionListener(new MacroSelectionAdapter(f));
			}
			else if (f.isDirectory()) {
				File[] tmp = f.listFiles();
				if (tmp != null && tmp.length > 0) {
					MenuItem subMenuItem = new MenuItem(menu, SWT.CASCADE);
					subMenuItem.setText(f.getName());
					Menu submenu = new Menu(menu);
					subMenuItem.setMenu(submenu);
					populate(submenu, f, 0);
				}
			}
		}
	}

	public static class MacroSelectionAdapter extends SelectionAdapter {

		File macro;

		public MacroSelectionAdapter(File macro) {
			this.macro = macro;
		}

		public void widgetSelected(SelectionEvent e) {
			IWorkbenchWindow acWindow = TXMWindows.getActiveWindow();
			IWorkbenchPart page = acWindow.getActivePage().getActivePart();
			IWorkbenchPart editor = acWindow.getActivePage().getActiveEditor();
			ExecuteGroovyMacro.execute(macro.getAbsolutePath(), page, editor, CorporaView.getInstance().getTreeViewer().getSelection(), null, null, null); //$NON-NLS-1$
		}
	}

	public static class MacroDirectoryMenu extends Menu {

		File directory;

		int index;

		public MacroDirectoryMenu(Menu parentMenu, File dir, int i) {
			super(parentMenu);
			this.directory = dir;
			this.index = i;
			this.addMenuListener(new MenuListener() {

				@Override
				public void menuShown(MenuEvent e) {
					populate(MacroDirectoryMenu.this, directory, index);
				}

				@Override
				public void menuHidden(MenuEvent e) {
				}
			});
		}
	}
}
