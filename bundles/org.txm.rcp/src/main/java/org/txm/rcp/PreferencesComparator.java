package org.txm.rcp;

import java.text.Collator;
import java.util.Locale;

import org.eclipse.ui.internal.dialogs.WorkbenchPreferenceNode;
import org.eclipse.ui.model.ContributionComparator;
import org.eclipse.ui.model.IComparableContribution;

/**
 * Makes TXM pages first
 * 
 * solution from https://www.eclipse.org/forums/index.php/t/160434/
 */
public class PreferencesComparator extends ContributionComparator {

	public int category(IComparableContribution c) {
		
		int cc = 0;
		
		if (c instanceof WorkbenchPreferenceNode) {
			String id = ((WorkbenchPreferenceNode) c).getId();
			if ("org.txm.rcp.preferences.TXMMainPreferencePage".equals(id)) {
				cc = 0;
			}
			else if ("org.txm.rcp.preferences.UserPreferencePage".equals(id)) {
				cc = 1;
			}
			else if ("org.txm.rcp.preferences.AdvancePreferencePage".equals(id)) {
				cc = 2;
			}
			else if ("org.txm.rcp.preferences.DebugPreferencePage".equals(id)) {
				cc = 999;
			}
			else {
				cc = 3 + super.category(c);
			}
		}
		else {
			cc = 3 + super.category(c);
		}
		
		return cc;
	}
	
	public static Collator collator = Collator.getInstance(Locale.getDefault());
	public int compare(IComparableContribution c1, IComparableContribution c2) {
		int cat1 = category(c1);
		int cat2 = category(c2);

		if (cat1 != cat2) {
			return cat1 - cat2;
		}

		String name1 = c1.getLabel();
		String name2 = c2.getLabel();

		if (name1 == null) {
			name1 = "";//$NON-NLS-1$
		}
		if (name2 == null) {
			name2 = "";//$NON-NLS-1$
		}

		// use the comparator to compare the strings
		return collator.compare(name1, name2);
	}
		
}
