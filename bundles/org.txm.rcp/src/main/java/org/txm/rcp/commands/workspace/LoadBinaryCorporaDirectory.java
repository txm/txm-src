// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands.workspace;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ListSelectionDialog;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.Toolbox;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.objects.Project;
import org.txm.objects.Workspace;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.StatusLine;
import org.txm.rcp.commands.RestartTXM;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.JobHandler;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

/**
 * Create projects from one binary corpus directory
 * 
 * calls LoadBinaryCorpus methods
 *
 * @author mdecorde
 */
public class LoadBinaryCorporaDirectory extends AbstractHandler {

	private static final String ID = LoadBinaryCorporaDirectory.class.getName();

	@Override
	/**
	 * calling this command will : open a directory dialog and try to load the content of the selected directory
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {

		// create dialog to get the corpus directory
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		DirectoryDialog dialog = new DirectoryDialog(shell, SWT.OPEN);
		if (LastOpened.getFile(ID) != null) {
			dialog.setFilterPath(LastOpened.getFolder(ID)); //$NON-NLS-1$
		}

		String dirPath = dialog.open();
		if (dirPath != null) {

			File directory = new File(dirPath);

			if (directory.exists()) {
				LastOpened.set(ID, directory);
				JobHandler job = loadBinaryCorpusFromCorporaDirectory(directory, true, true, true);
				if (job != null) {
					try {
						job.join();
					}
					catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		return null;
	}

	/**
	 * stop engines, try loading binary corpora from a directory, restart engines and refresh views
	 * 
	 * @param directory
	 * @return
	 */
	public static JobHandler loadBinaryCorpusFromCorporaDirectory(final File directory, final boolean askSelection, final boolean replace, final boolean loadDirectories) {


		final List<File> files = getBinaryCorpusSelection(directory, loadDirectories);

		JobHandler jobhandler = new JobHandler(NLS.bind(TXMUIMessages.LoadBinaryCorporaDirectory_loadingCorporaFromTheP0Directory, directory), true) {

			JobHandler self = this;

			@Override
			protected IStatus _run(final SubMonitor monitor) {

				File corporaDirectory = new File(Toolbox.getTxmHomePath(), "corpora"); //$NON-NLS-1$
				if (!corporaDirectory.exists()) {
					Log.severe(TXMUIMessages.LoadBinaryCorporaDirectory_corporaDirectoryNotFoundAborting);
					return Status.CANCEL_STATUS;
				}

				final ArrayList<Project> newProjects = new ArrayList<>();
				if (askSelection) {

					this.syncExec(new Runnable() {

						@Override
						public void run() {

							ArrayList<Project> tmp = loadBinaryCorpusFromCorporaDirectory(files, self, monitor, replace, directory.getAbsolutePath());
							if (tmp != null) {
								newProjects.addAll(tmp);
							}
						}
					});

				}
				else { // try loading corpora and dont replace existing corpus
					List<File> files = Arrays.asList(getBinaryCorpusFiles(directory, loadDirectories));
					ArrayList<Project> tmp = loadBinaryCorpusFromCorporaDirectory(files, this, monitor, replace, directory.getAbsolutePath());
					if (tmp != null) {
						newProjects.addAll(tmp);
					}
				}

				for (Project newProject2 : newProjects) {
					try {
						newProject2.compute(true);
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
				if (newProjects.size() > 0) {
					System.out.println(NLS.bind(TXMUIMessages.P0CorpusLoaded, newProjects.size()));
				}

				this.syncExec(new Runnable() {

					@Override
					public void run() {
						RestartTXM.reloadViews();
						StatusLine.setMessage(TXMUIMessages.info_txmIsReady);
					}
				});
				return Status.OK_STATUS;
			}
		};

		jobhandler.schedule();
		return jobhandler;
	}

	protected static List<File> getBinaryCorpusSelection(File directory, boolean loadDirectories) {
		ArrayList<File> selectedFiles = new ArrayList<>();
		;
		if (!directory.isDirectory()) return selectedFiles;
		if (!directory.exists()) return selectedFiles;

		Shell shell = null;

		if (Display.getCurrent() != null) {
			shell = Display.getCurrent().getActiveShell();
		}
		else if (Display.getDefault() == null) {
			shell = Display.getDefault().getActiveShell();
		}
		else if (Display.findDisplay(Thread.currentThread()) != null) {
			shell = Display.findDisplay(Thread.currentThread()).getActiveShell();
		}
		else {
			return selectedFiles;
		}
		File[] files = getBinaryCorpusFiles(directory, loadDirectories);

		ListSelectionDialog lsd = new ListSelectionDialog(shell, files, new ArrayContentProvider(), new LabelProvider() {

			@Override
			public Image getImage(Object element) {
				File f = (File) element;
				if (f.getName().endsWith(".txm")) { //$NON-NLS-1$
					return IImageKeys.getImage(IImageKeys.TXM1616);
				}
				else if (f.isDirectory()) {
					return IImageKeys.getImage(IImageKeys.FOLDER);
				}
				return null;
			}

			@Override
			public String getText(Object element) {
				if (element == null) return "error";//$NON-NLS-1$

				if (!(element instanceof File)) return "not a file (" + element + ")";//$NON-NLS-1$ //$NON-NLS-2$

				File f = (File) element;
				// if (f.isDirectory()) {
				// if (new File(f, ".settings").exists()) {//$NON-NLS-1$
				// return f.getName();
				// }
				// else if (new File(f, "import.xml").exists()) {//$NON-NLS-1$
				// return f.getName();
				// }
				// }
				// else if (f.getName().endsWith(".txm")) {//$NON-NLS-1$
				// return f.getName();
				// }
				return f.getName();

				// return "not a txm corpus (" + f + ")";//$NON-NLS-1$ //$NON-NLS-2$
			}
		}, TXMUIMessages.bind(TXMUIMessages.LoadBinaryCorporaDirectory_selectTheTXMCorpusToLoadInP0, directory));
		// lsd.setInitialSelections(files);
		lsd.setTitle(TXMUIMessages.LoadBinaryCorporaDirectory_corpusLoad);
		if (lsd.open() == ListSelectionDialog.OK) {
			//Log.info(lsd.getResult());
			for (Object o : lsd.getResult()) {
				selectedFiles.add(new File(o.toString()));
			}
		}

		return selectedFiles;
	}

	private static File[] getBinaryCorpusFiles(File directory, boolean loadDirectories) {
		File[] files = null;

		if (loadDirectories) {
			files = directory.listFiles(new FileFilter() {

				@Override
				public boolean accept(File arg0) {
					return IOUtils.HIDDENFILE_FILTER.accept(arg0) &&
							((arg0.isDirectory() && (new File(arg0, "import.xml").exists()
									|| new File(arg0, ".settings").exists())) //$NON-NLS-1$ //$NON-NLS-2$
									|| arg0.getName().endsWith(".txm") //$NON-NLS-1$

					);
				}
			});
		}
		else {
			files = directory.listFiles(new FileFilter() {

				@Override
				public boolean accept(File arg0) {
					return IOUtils.HIDDENFILE_FILTER.accept(arg0) && arg0.getName().endsWith(".txm"); //$NON-NLS-1$
				}
			});
		}

		Arrays.sort(files, new Comparator<File>() {

			@Override
			public int compare(File o1, File o2) {
				int i = o1.getName().compareTo(o2.getName());
				if (i == 0) {
					return (int) (o1.lastModified() - o2.lastModified());
				}
				return i;
			}
		});

		return files;
	}

	/**
	 * dont manage SearchEngine state
	 * 
	 * @param directory
	 * @param job
	 * @param monitor
	 * @return the loaded projects
	 */
	protected static ArrayList<Project> loadBinaryCorpusFromCorporaDirectory(File directory, JobHandler job, IProgressMonitor monitor, boolean replace) {
		File[] files = directory.listFiles(IOUtils.HIDDENFILE_FILTER);
		return loadBinaryCorpusFromCorporaDirectory(Arrays.asList(files), job, monitor, replace, directory.getAbsolutePath());
	}

	/**
	 * dont manage SearchEngine state
	 * 
	 * @param corporaFiles
	 * @param job
	 * @param monitor
	 * @param replace if true replace existing corpus
	 * @return the loaded projects
	 */
	protected static ArrayList<Project> loadBinaryCorpusFromCorporaDirectory(List<File> corporaFiles, JobHandler job, IProgressMonitor monitor, boolean replace, String sourceName) {

		ArrayList<Project> projects = new ArrayList<>();

		try {

			// SearchEngine engine = SearchEnginesManager.getCQPSearchEngine();
			// if (engine.isRunning()) {
			// engine.stop();
			// }

//			if (sourceName.matches(".+/TXM-.\\..\\../corpora")) { //$NON-NLS-1$
//				sourceName = "TXM" + sourceName.substring(sourceName.length() - 14, sourceName.length() - 8); //$NON-NLS-1$
//			}
			monitor.beginTask(TXMUIMessages.loadingBinaryCorpus + " " + sourceName + " (" + corporaFiles.size() + ").", corporaFiles.size()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			// job.acquireSemaphore();
			// Toolbox.getEngineManager(EngineType.SEARCH).stopEngines();
			// job.releaseSemaphore();


			int n = 0;
			for (File corpusDirectory : corporaFiles) {
				n++;
				monitor.subTask(TXMUIMessages.loadingTheCorpusInThePlatform + ": \n" + corpusDirectory.getName() + " (" + n + "/" + corporaFiles.size() + ")."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				monitor.worked(1);
				try {
					if (corpusDirectory.isHidden()) {
						continue;
					}

					Project project = Toolbox.workspace.getProject(corpusDirectory.getName().toUpperCase());
					if (project != null) {
						Log.warning(TXMCoreMessages.bind(TXMUIMessages.LoadBinaryCorporaDirectory_aCorpusNamedP0alreadyExistsLoadingOfP1Canceled, corpusDirectory.getName().toUpperCase(),
								corpusDirectory));
						continue;
					}

					if (corpusDirectory.isDirectory()) {
						if (Workspace.isCorpusBinarydirectoryValid(corpusDirectory)) {
							project = LoadBinaryCorpus.loadBinaryCorpusAsDirectory(corpusDirectory, monitor, replace);
						}
						else {
							Log.fine("Skip recovering of corpus: " + corpusDirectory); //$NON-NLS-1$
						}
					}
					else {
						project = LoadBinaryCorpus.loadBinaryCorpusArchive(corpusDirectory, job, monitor, replace);
					}

					// Project base = LoadBinaryCorpus.loadBinaryCorpusAsDirectory(corpusDirectory);
					if (project == null) {
						// Log.warning("Failed to load binary corpus: "+corpusDirectory);
					}
					else {
						projects.add(project);
						project.save();
					}
				}
				catch (Exception e2) {
					Log.warning(TXMUIMessages.bind(TXMUIMessages.CouldNotLoadThecorpusFromtheP0DirectoryP1, corpusDirectory, e2));
					Log.printStackTrace(e2);
				}
			}

			monitor.subTask(TXMUIMessages.restartingThePlatform);

		}
		catch (ThreadDeath td) {
		}
		catch (Exception e2) {
			org.txm.utils.logger.Log.printStackTrace(e2);
		}
		finally {
			monitor.done();
		}

		return projects;
	}
}
