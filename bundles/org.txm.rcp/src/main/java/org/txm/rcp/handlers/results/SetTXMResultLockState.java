// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.results;

import java.util.Map;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.menus.UIElement;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.ITXMResultEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.rcp.views.corpora.CorporaView;

/**
 * Swaps the locked state of the selected TXM result nodes.
 * 
 * @author mdecorde
 * 
 */
public class SetTXMResultLockState extends BaseAbstractHandler implements IElementUpdater {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object object = this.getCorporaViewSelectedObject(event);
		if (object instanceof TXMResult) {
			TXMResult result = ((TXMResult) object);
			result.setLocked(!result.isLocked());

			if (result.isLocked()) {
				result.setUserPersistable(true);
			}

			CorporaView.refresh();
			// TODO: MD: should be done with a listener
			// SJ: or in TXMEditor.refresh() using TXMResult.isLocked
			for (ITXMResultEditor<?> editor : SWTEditorsUtils.getEditors(result)) {
				editor.setLocked(editor.getResult().isLocked());
			}

			return null;
		}
		else {
			return super.logCanNotExecuteCommand(object);
		}
	}

	/**
	 * Displays the checked mark in the menu item when the menu is created.
	 */
	@Override
	public void updateElement(UIElement element, Map parameters) {
		Object object = CorporaView.getFirstSelectedObject();
		if (object != null && object instanceof TXMResult) {
			element.setChecked(((TXMResult) object).isLocked());
		}
	}
}
