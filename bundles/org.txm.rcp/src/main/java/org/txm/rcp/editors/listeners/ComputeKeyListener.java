/**
 * 
 */
package org.txm.rcp.editors.listeners;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.TXMEditor;

/**
 * Key and text Modify listener that calls TXMEditor.compute() when the Enter key is pressed.
 * Also manage the dirty editor state when the text of the widget has changed.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ComputeKeyListener extends BaseAbstractComputeListener implements KeyListener, ModifyListener {


	/**
	 * @param editor
	 */
	public ComputeKeyListener(TXMEditor<? extends TXMResult> editor) {
		this(editor, false);
	}

	/**
	 * 
	 * @param editor
	 * @param autoCompute
	 */
	public ComputeKeyListener(TXMEditor<? extends TXMResult> editor, boolean autoCompute) {
		super(editor, autoCompute);
	}


	@Override
	public void keyPressed(KeyEvent e) {
		if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
			this.editor.compute(true);
		}
		// if other key than Return is pressed then makes the editor dirty
		//		else	{
		//			this.editor.setDirty(true);
		//		}
		// FIXME: SJ: update the result from the widget
		// purpose here is, for example, to enable the compute command button when a query is typed
		// the code below doesn't work as expected
		//		else	{
		//			this.editor.autoUpdateResultFromEditorParameters();
		//			this.editor.updateResultFromEditor();
		//		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void modifyText(ModifyEvent e) {
		if (!mustIgnoreEvent(e) && !this.autoCompute) {
			this.editor.setDirty(true);
		}
	}

}
