package org.txm.rcp.swt.dialog;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * Dialog to set HashMap values given a HashMap pre-filled with values to edit
 * 
 * @author mdecorde
 *
 */
public class HashMapDialog extends Dialog {

	LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();

	LinkedHashMap<String, String> defaultValues = new LinkedHashMap<String, String>();

	LinkedHashMap<String, List<String>> availableValues = new LinkedHashMap<>();

	HashMap<String, Object> fields = new HashMap<String, Object>();

	String title;

	/**
	 * 
	 * @param parentShell
	 * @param title
	 * @param values values to edit, value may be null
	 */
	public HashMapDialog(Shell parentShell, String title, LinkedHashMap<String, String> defaultValues, LinkedHashMap<String, String> values, LinkedHashMap<String, List<String>> availableValues) {

		super(parentShell);

		this.title = title;

		if (defaultValues != null) {
			this.values.putAll(defaultValues);
		}

		if (values != null) {
			this.values.putAll(values);
		}

		if (availableValues != null) {
			this.availableValues.putAll(availableValues);
		}
	}

	@Override
	protected void configureShell(Shell newShell) {

		super.configureShell(newShell);
		newShell.setText(title);
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		parent.setLayout(new GridLayout(2, false));

		for (String k : values.keySet()) {
			Label l = new Label(parent, SWT.NONE);
			l.setText(k);
			l.setLayoutData(new GridData());

			if (availableValues != null && availableValues.containsKey(k)) {
				Combo field = new Combo(parent, SWT.BORDER | SWT.MULTI);
				field.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
				if (values.get(k) != null) {
					field.setText(values.get(k));
					field.setToolTipText(k);
				}
				field.setItems(availableValues.get(k).toArray(new String[availableValues.get(k).size()]));
				fields.put(k, field);
			}
			else {
				Text field = new Text(parent, SWT.BORDER);
				field.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
				if (values.get(k) != null) {
					field.setText(values.get(k));
					field.setToolTipText(k);
				}

				fields.put(k, field);
			}
		}
		return parent;
	}

	@Override
	protected void okPressed() {

		values.clear();
		for (String k : fields.keySet()) {
			if (fields.get(k) instanceof Text text) {
				if (text.getText().length() > 0) {
					values.put(k, text.getText());
				}
			}
			else if (fields.get(k) instanceof Combo combo) {
				if (combo.getText().length() > 0) {
					values.put(k, combo.getText());
				}
			}
		}

		super.okPressed();
	}

	/**
	 * 
	 * @return the values set
	 */
	public LinkedHashMap<String, String> getValues() {

		return values;
	}
}
