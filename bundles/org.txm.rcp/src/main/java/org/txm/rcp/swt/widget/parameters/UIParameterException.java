package org.txm.rcp.swt.widget.parameters;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;

public class UIParameterException extends IllegalArgumentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8914778777358750878L;

	/**
	 * If true then the parameter dialog fields went wrong
	 */
	private boolean error;

	public UIParameterException(boolean error) {

		super(NLS.bind(TXMCoreMessages.error_errorP0, error));
		this.error = error;
	}


	public boolean isError() {
		return error;
	}
}
