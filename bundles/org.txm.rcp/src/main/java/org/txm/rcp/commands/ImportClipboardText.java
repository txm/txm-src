// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.Toolbox;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.objects.Project;
import org.txm.objects.Workspace;
import org.txm.rcp.editors.imports.sections.LangSection;
import org.txm.rcp.handlers.scripts.ExecuteImportScript;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.TextDialog;
import org.txm.rcp.swt.dialog.ValuePlusComboDialog;
import org.txm.searchengine.cqp.ReferencePattern;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.treetagger.core.preferences.TreeTaggerPreferences;
import org.txm.utils.AsciiUtils;
import org.txm.utils.LangDetector;
import org.txm.utils.logger.Log;

/**
 * Import the text in the Clipboard as a corpus. The loader used is the QuickLoader.
 * 
 * The Clipboard is saved in the clipoard directory and then imported as TXT.
 * 
 * @author mdecorde
 * 
 */
public class ImportClipboardText extends AbstractHandler {

	/** The nextclipcorpus. */
	static public int nextclipcorpus = 1;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String result = org.txm.rcp.utils.IOClipboard.read();

		if (result == null || result.length() == 0) {
			MessageDialog.openError(Display.getDefault().getActiveShell(), TXMUIMessages.error, TXMUIMessages.clipboardIsEmpty);
			return null;
		}

		result = result.trim();
		if (result.length() == 0) {
			MessageDialog.openError(HandlerUtil.getActiveShell(event), TXMUIMessages.error, TXMUIMessages.clipboardIsEmpty);
			return null;
		}

		try {
			String corpusName = TXMUIMessages.clipboard_2.toUpperCase() + (nextclipcorpus++);
			File wl = Toolbox.workspace.getLocation();
			if (wl == null) {
				Log.warning("Error: Toolbox is not correctly initialized. Clipboard import aborted."); //$NON-NLS-1$
				return null;
			}
			while (new File(wl, corpusName).exists()) {
				corpusName = TXMUIMessages.clipboard_2.toUpperCase() + (nextclipcorpus++);
			}

			String lang = TBXPreferences.getInstance().getString(TBXPreferences.CLIPBOARD_IMPORT_DEFAULT_LANG);
			boolean askLang = TBXPreferences.getInstance().getBoolean(TBXPreferences.CLIPBOARD_IMPORT_ASK_PARAMETERS);
			List<String> langs = Arrays.asList(LangSection.getAvailableLangs());
			//ComboDialog dialog = new ComboDialog(HandlerUtil.getActiveShell(event), TXMUIMessages.SelectTheImportLanguageToUse, TXMUIMessages.WhatLanguageMustBeUsedToImportTheClipboardContent, lang);
			ValuePlusComboDialog dialog = new ValuePlusComboDialog(HandlerUtil.getActiveShell(event), "Import parameters", "Corpus name", corpusName, "Main Language", langs, lang, SWT.SINGLE);
			if (askLang) {
				if (dialog.open() == TextDialog.CANCEL) return null;
				lang = dialog.getSelectedComboValue();
				corpusName = dialog.getValue();
				if (corpusName == null) return null;
				corpusName = AsciiUtils.buildId(corpusName.toUpperCase());
			}
			
			Log.info(TXMUIMessages.importClipboardText);
			importText(result, corpusName, lang);

		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return null;
	}

	/**
	 * Import text.
	 *
	 * @param text the text
	 * @param corpusname the basename
	 * @return the object
	 * @throws Exception
	 */
	public static Object importText(String text, String corpusname, String lang) throws Exception {
		corpusname = corpusname.toUpperCase();
		text = text.trim();
		if (text.length() == 0) {
			Log.warning(TXMUIMessages.clipboardIsEmpty);
			return null;
		}
		String txmhome = Toolbox.getTxmHomePath();
		new File(txmhome, "clipboard").mkdir(); //$NON-NLS-1$
		File clipboardDirectory = new File(txmhome, "clipboard/" + corpusname); //$NON-NLS-1$

		if (CorpusManager.getCorpusManager().hasCorpus(clipboardDirectory.getName())) {
			Shell shell = new Shell();
			boolean b = MessageDialog.openConfirm(shell,
					TXMUIMessages.confirm, NLS.bind(TXMUIMessages.theCorpusAndTheCorpusP0WillBeDeleted, clipboardDirectory.getName()));
			if (!b) {
				return null;
			}
		}

		org.txm.utils.DeleteDir.deleteDirectory(clipboardDirectory);
		clipboardDirectory.mkdirs();

		File quicksrc = new File(clipboardDirectory, corpusname + ".txt"); //$NON-NLS-1$
		try {
			Writer writer = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(quicksrc)), "UTF-8"); //$NON-NLS-1$ 
			writer.write(text.replace("\t", "    ")); //$NON-NLS-1$ //$NON-NLS-2$
			writer.close();
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return null;
		}

		// configure project
		Workspace w = Toolbox.workspace;
		Project project = w.getProject(clipboardDirectory.getName());
		if (project == null) {
			project = new Project(w, clipboardDirectory.getName());
		}
		project.setSourceDirectory(clipboardDirectory.getAbsolutePath());
		project.setImportModuleName("txt"); //$NON-NLS-1$
		project.setEncoding("UTF-8"); //$NON-NLS-1$
		project.getEditionDefinition("default").setBuildEdition(true); //$NON-NLS-1$
		project.getCommandPreferences("concordance").set("view_reference_pattern",  //$NON-NLS-1$ //$NON-NLS-2$
				ReferencePattern.referenceToString(Arrays.asList("text_id", "lbn"), null)); //$NON-NLS-1$ //$NON-NLS-2$

		if (lang.length() == 0) lang = Locale.getDefault().getLanguage();
		if ("??".equals(lang)) { //$NON-NLS-1$
			LangDetector detector = new LangDetector(quicksrc);
			lang = detector.getLang();
			Log.info(TXMCoreMessages.bind(TXMUIMessages.identifiedLanguageP0, lang));
		}
		project.setLang(lang); //$NON-NLS-1$
		Log.info(TXMCoreMessages.bind(TXMUIMessages.usingP0Language, project.getLang()));

		String ttpath = TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.INSTALL_PATH);

		if (ttpath != null && ttpath.length() > 0 && new File(ttpath, "bin").isDirectory()) { //$NON-NLS-1$
			project.setAnnotate(true);
		}

		ExecuteImportScript.executeScript(project);
		return null;
	}
}
