// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.service.datalocation.Location;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.ComboDialog;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;
import org.txm.utils.logger.MessageBox;

/**
 * Switches the language in RCP based products. Works only if the product is
 * deployed to the local file system since the <product>.ini file is altered.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class LanguageSwitchHandler extends AbstractHandler {

	/**
	 * The constructor.
	 */
	public LanguageSwitchHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 *
	 * @param event the event
	 * @return the object
	 * @throws ExecutionException the execution exception
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

		String locale = Locale.getDefault().getLanguage();// RCPPreferences.getInstance().getString(RCPPreferences.UI_LOCALE);

		ArrayList<String> values = new ArrayList<>();
		values.add("fr"); //$NON-NLS-1$
		values.add("en"); //$NON-NLS-1$
		values.add("ru"); //$NON-NLS-1$

		if (!values.contains(locale)) { // set selected locale
			locale = "en"; //$NON-NLS-1$
		}

		ComboDialog d = new ComboDialog(shell, TXMUIMessages.chooseTheLanguage, values, locale);
		if (d.open() == Window.OK) {
			locale = d.getSelectedValue();
			// RCPPreferences.getInstance().put(RCPPreferences.CONFIG_LOCALE, locale);
		}
		else {
			return null;
		}

		// Does not work:
		// System.getProperties().setProperty(IApplicationContext.EXIT_DATA_PROPERTY, "-nl " + locale);
		// changeLocaleTXMIni(locale);

		if (changeLocaleConfigIni(locale)) {
			PlatformUI.getWorkbench().restart();
		}
		else {
			Log.warning(
					"** Error: TXM language configuration could not be set. You can set TXM language by editing HOME/TXM-<version>/.txm/TXM.ini) configuration file by adding '-nl\n<language code>' before the '-vmargs' line."); //$NON-NLS-1$
		}

		return null;
	}

	/**
	 * Change locale of TXM.ini.
	 *
	 * @param locale the locale
	 */
	public static void changeLocaleTXMIni(String locale) {

		URL location = null;
		try {
			String path = System.getProperty("osgi.instance.area"); //$NON-NLS-1$
			location = new URL(path + "TXM.ini"); //$NON-NLS-1$
		}
		catch (MalformedURLException e) {
			System.err.println(NLS.bind(TXMUIMessages.switchLanguageColonMalformedUrlColonP0, location));
			return;
		}

		String fileName = location.getFile();
		File file = new File(fileName);
		if (!file.exists()) {
			MessageBox.error(NLS.bind(TXMUIMessages.theFileP0DoesNotExist, file.getAbsolutePath()));
			return;
		}
		fileName += ".bak"; //$NON-NLS-1$
		file.renameTo(new File(fileName));

		// update TXM.ini
		System.out.println(TXMUIMessages.tXMiniColon + location);
		Log.severe(TXMUIMessages.tXMiniColon + location);
		try {
			BufferedReader in = new BufferedReader(new FileReader(fileName));
			BufferedWriter out = new BufferedWriter(new FileWriter(location.getFile()));
			try {
				boolean isNl = false;
				boolean isNlWiritten = false;
				String line = in.readLine();
				while (line != null) {

					// write nl before vmargs
					if (line.equals("-vmargs") && !isNlWiritten) { //$NON-NLS-1$

						out.write("-nl"); //$NON-NLS-1$
						out.newLine();
						out.write(locale);
						out.newLine();
						isNl = true;
					}

					// write line if -nl was not found in previous line
					// else locale
					if (!isNl) {
						out.write(line);
					}
					else {
						out.write(locale);
						isNl = false;
						isNlWiritten = true;
					}
					out.newLine();

					// check nl line
					if (line.equals("-nl")) { //$NON-NLS-1$
						isNl = true;
					}

					line = in.readLine();
				}
				// create nl if it was not presents
				if (!isNlWiritten) {
					out.write("-nl"); //$NON-NLS-1$
					out.newLine();
					out.write(locale);
					out.newLine();
				}
				out.flush();
			}
			finally {
				if (in != null) {
					try {
						in.close();
					}
					catch (IOException e) {
						org.txm.utils.logger.Log.printStackTrace(e);
					}
				}
				if (out != null) {
					try {
						out.close();
					}
					catch (IOException e) {
						org.txm.utils.logger.Log.printStackTrace(e);
					}
				}
			}
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	/**
	 * Change locale of config.ini.
	 *
	 * @param locale the locale
	 */
	public static boolean changeLocaleConfigIni(String locale) {

		Location configArea = Platform.getConfigurationLocation();
		if (configArea == null) {
			return false;
		}

		String fileName = Platform.getConfigurationLocation().getURL().getFile() + System.getProperty("file.separator") + "config.ini"; //$NON-NLS-1$ //$NON-NLS-2$

		File file = new File(fileName);
		if (!file.exists()) {
			MessageBox.error(NLS.bind(TXMUIMessages.theFileP0DoesNotExist, file.getAbsolutePath()));
			return false;
		}
		String fileNameBackup = fileName + ".bak"; //$NON-NLS-1$
		file.renameTo(new File(fileNameBackup));

		// update config.ini
		Log.info("config.ini: " + fileName); //$NON-NLS-1$
		try {

			BufferedReader in = new BufferedReader(new FileReader(fileNameBackup));
			BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
			try {
				boolean isNlWritten = false;
				String line = in.readLine();
				while (line != null) {

					if (!isNlWritten) {
						if (line.contains("osgi.nl=")) { //$NON-NLS-1$
							out.write("osgi.nl=" + locale); //$NON-NLS-1$
							isNlWritten = true;
						}
						else {
							out.write(line);
						}
					}
					else {
						out.write(line);
					}

					out.newLine();
					line = in.readLine();
				}
				// create osgi.nl if it was not presents
				if (!isNlWritten) {
					out.write("osgi.nl=" + locale); //$NON-NLS-1$
					out.newLine();
				}
				out.flush();
			}
			finally {
				if (in != null) {
					try {
						in.close();
					}
					catch (IOException e) {
						org.txm.utils.logger.Log.printStackTrace(e);
					}
				}
				if (out != null) {
					try {
						out.close();
					}
					catch (IOException e) {
						org.txm.utils.logger.Log.printStackTrace(e);
					}
				}
			}
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}

		String txt;
		try {
			txt = IOUtils.getText(file, "UTF-8"); //$NON-NLS-1$
			return txt.contains("osgi.nl=" + locale); //$NON-NLS-1$
		}
		catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
}
