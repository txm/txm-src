package org.txm.rcp.handlers.export;

import java.io.File;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.internal.ide.ChooseWorkspaceData;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.TXMResult;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.utils.FileUtils;

public class ExportResultDialog extends Dialog {

	boolean checked = false;

	Button checkMessageButton = null;

	ChooseWorkspaceData launchData;

	private Text filePathText;

	private Combo formatCombo;

	private String filePath;

	private String format;

	private String[] formats;

	private TXMResult exported;

	private Button saveConfigurationButton;

	private Boolean saveConfiguration = false;

	public ExportResultDialog(Shell shell, TXMResult exported) {
		super(shell);
		this.exported = exported;
	}

	protected void configureShell(Shell newShell) {
		
		if (exported != null) {
			newShell.setText(NLS.bind(TXMUIMessages.exportOftheP0P1Result, exported.getResultType(), exported));
		}
		else {
			newShell.setText("Export...");
		}
		//newShell.setSize(500, 300);
		super.configureShell(newShell);
	}

	@Override
	protected boolean isResizable() {
	    return true;
	}
	
	@Override
	protected Point getInitialSize() {
	    final Point size = super.getInitialSize();

	    size.x += 100; // more comfortable
	    size.y += 25;

	    return size;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		//Control c = super.createDialogArea(parent);

		GridLayout parentLayout = (GridLayout) parent.getLayout();
		parentLayout.marginLeft = 5;
		parentLayout.marginRight = 5;
		parentLayout.marginHeight = 5;

		GLComposite controls = new GLComposite(parent, SWT.NONE);
		controls.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		controls.getLayout().numColumns = 3;
		controls.getLayout().horizontalSpacing = 5;
		controls.getLayout().verticalSpacing = 10;

		//new Label(messageComposite, SWT.NONE).setImage(IImageKeys.getImage(IImageKeys.TXM_ICON));
		Label directoryLabel = new Label(controls, SWT.NONE);
		directoryLabel.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		directoryLabel.setText(TXMUIMessages.filePath);
		directoryLabel.setToolTipText(TXMUIMessages.filePathInWhichTheResultExportWillBeDone);
		
		filePathText = new Text(controls, SWT.BORDER);
		filePathText.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		filePathText.setToolTipText(TXMUIMessages.filePathInWhichTheResultExportWillBeDone);
		
		Button directoryButton = new Button(controls, SWT.PUSH);
		directoryButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		directoryButton.setText("..."); //$NON-NLS-1$
		directoryButton.setToolTipText(TXMUIMessages.filePathInWhichTheResultExportWillBeDone);
		directoryButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog d = new FileDialog(e.display.getActiveShell(), SWT.SAVE);
				File f = new File(filePathText.getText());
				d.setFilterPath(f.getParent());
				d.setFilterExtensions(new String[] {FormatsRepository.toFormat(formatCombo.getText())});
				d.setFileName(f.getName());
				String path = d.open();
				if (path != null) {
					filePathText.setText(path);
					updateFilename();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Label formatLabel = new Label(controls, SWT.NONE);
		formatLabel.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		formatLabel.setText(TXMUIMessages.format);
		formatLabel.setToolTipText(TXMUIMessages.formatWillDetermineWhatYouCanDoWithYourExport);

		formatCombo = new Combo(controls, SWT.READ_ONLY | SWT.SINGLE);
		formatCombo.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false, 2, 1));
		formatCombo.setToolTipText(TXMUIMessages.formatWillDetermineWhatYouCanDoWithYourExport);
		formatCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				updateFilename();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) { // MD test of export configuration saved for later use. Eg an export configuration for an article. For now only the file and encoding is saved but see later if this could be useful with an ExportEngine ?
			saveConfigurationButton = new Button(controls, SWT.CHECK);
			saveConfigurationButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false, 3, 1));
			saveConfigurationButton.setText("Save export configuration: allow to be updated the exported file each time the result is computed.");
		}

		if (exported != null) {
			if (filePath == null) filePath = exported.getValidFileName(); 
			if (formats == null) formats = exported.getExportTXTExtensions();
			if (format == null) {
				if (formats != null && formats.length > 0) format = formats[0];
			}
		}
		
		updateFromInternals();
		updateFilename();
		
		return parent;
	}

	private void updateFromInternals() {
		
		if (filePath != null) filePathText.setText(filePath);
		if (formats != null) {
			
			String[] formatNames = new String[formats.length];
			for (int i = 0 ; i < formats.length ; i++) {
				formatNames[i] = FormatsRepository.toName(formats[i]);
			}
			formatCombo.setItems(formatNames);
		}
		if (format != null) {
			formatCombo.setText(FormatsRepository.toName(format));
		}
	}

	private String updateFilename() {
		
		String tmpFilename = filePathText.getText();
		String tmpFormat = FormatsRepository.toFormat(formatCombo.getText());
		
		tmpFormat = tmpFormat.substring(1); // remove "*"
		
		if (tmpFilename.endsWith(tmpFormat)) return tmpFilename; // OK

		File f = new File(tmpFilename).getAbsoluteFile();
		String basename = FileUtils.stripExtension(f.getName());
		if (org.txm.utils.OSDetector.isFamilyUnix()) {
			filePathText.setText(f.getParent() + File.separator + basename + tmpFormat);
		}

		return f.getParent() + File.separator + basename + tmpFormat;
	}

	//	@Override
	//	protected Control createButtonBar(Composite parent) {
	//		Control c = super.createButtonBar(parent);
	//		getButton(Window.OK).setEnabled(false);
	//		return c;		
	//	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
	 */
	@Override
	protected void buttonPressed(int buttonId) {

		if (buttonId == 0) {

			if (filePathText.getText().length() == 0) return; // cancel
			if (formatCombo.getText().length() == 0) return; // cancel

			filePath = updateFilename();
			format = formatCombo.getText();
			if (saveConfigurationButton != null) saveConfiguration = saveConfigurationButton.getSelection();
		}

		super.buttonPressed(buttonId);

	}

	public String getFilePath() {
		return filePath;
	}

	/**
	 * 
	 * @return a File of path of directory=getDirectory() and filename=getFilename()
	 */
	public File getFile() {
		return new File(filePath);
	}

	public String getFormat() {
		return format;
	}

	public String[] getFormats() {
		return formats;
	}

	public boolean getSaveConfiguration() {
		return saveConfiguration;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
		if (filePathText != null && !filePathText.isDisposed()) {
			filePathText.setText(filePath);
		}
	}
	
	public void setFilePath(String dir, String filename) {
		
		if (dir == null || dir.length() == 0) {
			this.filePath = filename;
		} else {
			this.filePath = dir + File.separator + filename;
		}
		if (filePathText != null && !filePathText.isDisposed()) {
			filePathText.setText(filePath);
		}
	}

	public void setFormats(String[] formats) {
		this.formats = formats;
		if (formatCombo != null && !formatCombo.isDisposed()) {
			formatCombo.setItems(formats);
		}
	}

	public void setFormat(String format) {
		this.format = format;
		if (formatCombo != null && !formatCombo.isDisposed()) {
			formatCombo.setText(format);
		}
	}

	/**
	 * 
	 * @param dir default directory
	 * @param name default filename and file format (computed from extension)
	 */
	public void setFilepath(String dir, String name) {
		filePath = dir + File.separator + name;
		format = FileUtils.getExtension(name);
		
		if (filePathText != null && !filePathText.isDisposed()) {
			filePathText.setText(filePath);
		}
	}
}
