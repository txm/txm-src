package org.txm.rcp.commands.workspace;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.Toolbox;
import org.txm.objects.Project;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.JobHandler;
import org.txm.utils.Download;

public class DownloadCorpus extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String key = event
				.getParameter("org.txm.rcp.commands.commandParameter3"); //$NON-NLS-1$
		if (key != null && key.trim().length() > 0) {
			try {
				return downloadAndLoad(key);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public static Project downloadAndLoad(String urlString) throws InterruptedException, ExecutionException, IOException {
		URL url = new URL(urlString);
		File binFile = new File(Toolbox.getTxmHomePath(), "results/" + new File(url.getFile()).getName()); //$NON-NLS-1$
		if (!binFile.getName().endsWith(".txm")) { //$NON-NLS-1$
			System.out.println(TXMUIMessages.TheDownloadedFileIsNotATXMCorpus);
			return null;
		}
		Download.download(url, binFile);
		JobHandler job = LoadBinaryCorpus.loadBinaryCorpusArchive(binFile);
		job.join();
		Object r = job.getResultObject();
		if (r != null && r instanceof Project) {
			return (Project) r;
		}

		return null;
	}
}
