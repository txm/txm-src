package org.txm.rcp.swt.dialog;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.txm.rcp.swt.provider.SimpleLabelProvider;

public class ListSelectionDialog<T> extends Dialog {

	List<T> values;

	T selectedValue;
	
	Integer iSelectedValue;

	ListViewer valuesList;

	private String message;

	private String title;

	/**
	 * 
	 * @param parentShell
	 * @param title dialog title
	 * @param message dialog optional message
	 * @param values available values
	 */
	public ListSelectionDialog(Shell parentShell, String title, String message, List<T> values) {

		this(parentShell, title, message, values, null);
	}
	
	/**
	 * 
	 * @param parentShell
	 * @param title dialog title
	 * @param message dialog optional message
	 * @param values available values
	 * @param selectedValues selected values or NULL
	 */
	public ListSelectionDialog(Shell parentShell, String title, String message, List<T> values, T selectedValues) {

		super(parentShell);
		this.values = values;
		this.selectedValue = selectedValues;
		this.message = message;
		this.title = title;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(title);
		//newShell.setSize(SWT.DEFAULT, SWT.DEFAULT);
	}

	public List<T> getInitialValues() {
		return values;
	}

	public T getSelectedValue() {
		return selectedValue;
	}
	
	public Integer getIndexSelectedValue() {
		return iSelectedValue;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite p) {
		super.createDialogArea(p);
		//System.out.println(p.getLayout());
		GridLayout layout = (GridLayout) p.getLayout();
		GridData gdata = (GridData) p.getLayoutData();
		gdata.grabExcessVerticalSpace = gdata.grabExcessHorizontalSpace = true;
		layout.marginLeft = layout.marginRight = 10; 
		layout.verticalSpacing = 10;
		if (message != null) {
			Label l = new Label(p, SWT.NONE);
			l.setText(message);
			l.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		}
		
		valuesList = new ListViewer(p, SWT.SINGLE);
		valuesList.getList().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		valuesList.setLabelProvider(new SimpleLabelProvider());
		valuesList.setContentProvider(new ArrayContentProvider());
		valuesList.setInput(values.toArray());
		if (selectedValue != null) valuesList.setSelection(new StructuredSelection(selectedValue));

		return p;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void okPressed() {

		selectedValue = (T) valuesList.getStructuredSelection().getFirstElement();
		iSelectedValue = valuesList.getList().getSelectionIndex();
		super.okPressed();
	}

}
