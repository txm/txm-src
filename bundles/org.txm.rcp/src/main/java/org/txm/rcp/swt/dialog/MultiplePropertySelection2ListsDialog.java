// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.dialog;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.VirtualProperty;

/**
 * The dialog box called to select the view properties.
 * 
 * @author mdecorde
 */
public class MultiplePropertySelection2ListsDialog<P extends Property> extends Dialog {

	/** The available properties. */
	final private List<P> availableProperties;

	/** The selected properties. */
	final private List<P> selectedProperties;

	/** The maxprops. */
	int maxprops = -1;

	/** The available properties view. */
	org.eclipse.swt.widgets.List availablePropertiesView;

	/** The selected properties view. */
	org.eclipse.swt.widgets.List selectedPropertiesView;

	private ArrayList<P> cancelSelectedProperties;

	private ArrayList<P> cancelAvailableProperties;

	Control sourceWidget;

	/**
	 * Instantiates a new view property selection dialog.
	 *
	 * @param parentShell the parent shell
	 * @param availableProperties the available properties
	 * @param selectedProperties the selected properties
	 */
	public MultiplePropertySelection2ListsDialog(IShellProvider parentShell,
			List<P> availableProperties,
			List<P> selectedProperties) {
		this(parentShell.getShell(), availableProperties, selectedProperties);
	}

	/**
	 * Instantiates a new view property selection dialog.
	 *
	 * @param shell the shell
	 * @param availableProperties the available properties
	 * @param selectedProperties the selected properties
	 */
	public MultiplePropertySelection2ListsDialog(Shell shell,
			List<P> availableProperties,
			List<P> selectedProperties) {
		this(shell, availableProperties, selectedProperties, -1, null);
	}

	/**
	 * Instantiates a new view property selection dialog.
	 *
	 * @param shell the shell
	 * @param availableProperties the available properties
	 * @param selectedProperties the selected properties
	 * @param maxprops the maxprops
	 */
	public MultiplePropertySelection2ListsDialog(Shell shell,
			List<P> availableProperties,
			List<P> selectedProperties, int maxprops, Control sourceWidget) {
		super(shell);

		this.availableProperties = new ArrayList<>(availableProperties);
		this.selectedProperties = new ArrayList<>(selectedProperties);
		this.availableProperties.removeAll(this.selectedProperties);
		this.cancelAvailableProperties = new ArrayList<>(availableProperties);
		this.cancelSelectedProperties = new ArrayList<>(selectedProperties);

		if (sourceWidget != null) {
			this.setShellStyle(SWT.NONE | SWT.APPLICATION_MODAL);
		}
		else {
			this.setShellStyle(this.getShellStyle() | SWT.APPLICATION_MODAL | SWT.RESIZE);
		}
		this.maxprops = maxprops;
		this.sourceWidget = sourceWidget;
	}

	/**
	 * Gets the selected properties.
	 *
	 * @return the selected properties
	 */
	public List<P> getSelectedProperties() {
		return selectedProperties;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(TXMUIMessages.common_displayOptions);
		newShell.setMinimumSize(300, 200);
		if (sourceWidget != null) {
			Point m = newShell.getDisplay().map(sourceWidget, null, 0, sourceWidget.getSize().y);
			newShell.setLocation(m);
		}

		newShell.addListener(SWT.Deactivate, event -> okPressed());
	}

	@Override
	protected Point getInitialLocation(Point initialSize) {
		if (sourceWidget != null) {
			return sourceWidget.getDisplay().map(sourceWidget, null, 0, sourceWidget.getSize().y);
		}
		return super.getInitialLocation(initialSize);
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		// parent.setLayout(new FormLayout());
		Composite mainArea = new Composite(parent, SWT.NONE);
		mainArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		// 4 columns
		GridLayout layout = new GridLayout(4, false);
		mainArea.setLayout(layout);
		
		new Label(mainArea, SWT.NONE).setText("Availables");
		new Label(mainArea, SWT.NONE).setText("");
		new Label(mainArea, SWT.NONE).setText("Selected");
		new Label(mainArea, SWT.NONE).setText("");

		availablePropertiesView = new org.eclipse.swt.widgets.List(mainArea, SWT.BORDER | SWT.V_SCROLL);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.heightHint = 500;
		availablePropertiesView.setLayoutData(gridData);

		Composite selectionButtons = new Composite(mainArea, SWT.NONE);
		selectionButtons.setLayout(new GridLayout(1, false));
		Button select = new Button(selectionButtons, SWT.ARROW | SWT.RIGHT);
		Button unselect = new Button(selectionButtons, SWT.ARROW | SWT.LEFT);

		selectedPropertiesView = new org.eclipse.swt.widgets.List(mainArea, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.heightHint = 500;
		selectedPropertiesView.setLayoutData(gridData);

		Composite orderButtons = new Composite(mainArea, SWT.NONE);
		orderButtons.setLayout(new GridLayout(1, false));
		Button up = new Button(orderButtons, SWT.ARROW | SWT.UP);
		Button down = new Button(orderButtons, SWT.ARROW | SWT.DOWN);

		select.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int index = availablePropertiesView.getSelectionIndex();
				if (maxprops == 1) {
					selectedProperties.add(availableProperties.get(index));
					availableProperties.remove(index);

					availableProperties.add(selectedProperties.get(0));
					selectedProperties.remove(0);
				}
				else {
					if (maxprops > 0) {
						if (selectedProperties.size() >= maxprops)
							return;
					}
					if (index >= 0) {
						selectedProperties.add(availableProperties.get(index));
						availableProperties.remove(index);
					}
				}
				reload();
				availablePropertiesView.setSelection(index);
			}
		});

		unselect.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				deselectProperties();
			}
		});

		up.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int index = selectedPropertiesView.getSelectionIndex();
				if (index > 0) {
					P selectedP = selectedProperties.get(index);
					P upperP = selectedProperties.get(index - 1);

					selectedProperties.set(index, upperP);
					selectedProperties.set(index - 1, selectedP);

					reload();
					selectedPropertiesView.setSelection(index - 1);
				}
			}
		});

		down.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int index = selectedPropertiesView.getSelectionIndex();
				if (index < selectedProperties.size() - 1 && index >= 0) {
					P selectedP = selectedProperties.get(index);
					P bellowP = selectedProperties.get(index + 1);

					selectedProperties.set(index, bellowP);
					selectedProperties.set(index + 1, selectedP);

					reload();
					selectedPropertiesView.setSelection(index + 1);
				}
			}
		});

		availablePropertiesView.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {

				if (maxprops == 1) {
					int index = availablePropertiesView.getSelectionIndex();
					// System.out.println("dl click, maxprops="+maxprops+" idx="+index);
					selectedProperties.add(availableProperties.get(index));
					availableProperties.remove(index);

					availableProperties.add(selectedProperties.get(0));
					selectedProperties.remove(0);

					reload();

				}
				else {
					int index = availablePropertiesView.getSelectionIndex();
					if (index >= 0) {
						selectedProperties.add(availableProperties.get(index));
						availableProperties.remove(index);

						reload();
						availablePropertiesView.setSelection(index);
					}
				}
			}
		});

		selectedPropertiesView.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				deselectProperties();
			}
		});

		reload();
		return mainArea;
	}

	private void deselectProperties() {

		int index = selectedPropertiesView.getSelectionIndex();
		if (selectedProperties.size() > 1 && index >= 0) {

			availableProperties.add(selectedProperties.get(index));
			selectedProperties.remove(index);

			reload();
			selectedPropertiesView.setSelection(index);
		}
	}

	/**
	 * Reload.
	 */
	public void reload() {
		if (availableProperties == null) {
			System.out.println(TXMUIMessages.ErrorTheAvailablePropertiesListIsNULL);
			return;
		}
		availablePropertiesView.removeAll();
		for (Property property : availableProperties) {
			// if (!property.getName().equals("id")) //$NON-NLS-1$
			if (property instanceof StructuralUnitProperty || property instanceof VirtualProperty) {
				availablePropertiesView.add(property.getFullName());
			}
			else {
				availablePropertiesView.add(property.getName());
			}
		}
		selectedPropertiesView.removeAll();
		for (Property property : selectedProperties) {
			// if (!property.getName().equals("id")) //$NON-NLS-1$
			if (property instanceof StructuralUnitProperty || property instanceof VirtualProperty) {
				selectedPropertiesView.add(property.getFullName());
			}
			else {
				selectedPropertiesView.add(property.getName());
			}
		}

		availablePropertiesView.getParent().layout();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		if (selectedProperties.size() != 0) {
			super.okPressed();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void cancelPressed() {
		this.selectedProperties.clear();
		this.selectedProperties.addAll(cancelSelectedProperties);

		this.availableProperties.clear();
		availableProperties.addAll(cancelAvailableProperties);

		super.cancelPressed();
	}
}
