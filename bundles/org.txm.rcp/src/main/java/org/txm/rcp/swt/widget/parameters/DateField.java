package org.txm.rcp.swt.widget.parameters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.kohsuke.args4j.NamedOptionDef;

public class DateField extends LabelField {

	DateTime dt;

	public static String STRINGFORMAT = "yyyy-MM-dd"; //$NON-NLS-1$

	public static SimpleDateFormat formater = new SimpleDateFormat(STRINGFORMAT); //$NON-NLS-1$

	public DateField(Composite parent, int style, NamedOptionDef parameter) {
		super(parent, style, parameter);

		dt = new DateTime(this, SWT.DATE | SWT.BORDER);
		dt.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));

	}

	public Date getWidgetValue() {
		int day = dt.getDay(); // Calendar.DAY_OF_MONTH
		int month = dt.getMonth(); // Calendar.MONTH
		int year = dt.getYear(); // Calendar.YEAR

		//System.out.println("year="+year+" month="+month+" day="+day+ " hour="+hour+" min="+min+" sec="+second); 

		try {
			return formater.parse("" + year + "-" + month + "-" + day); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		catch (ParseException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
			return null;
		}
	}


	public void setDefault(Object text) {
		//t.setText(text);
		//TODO: not implemented
	}

	@Override
	public void resetToDefault() {
		if (getWidgetDefault().length() > 0) {
			try {
				Date date = formater.parse(getWidgetDefault());
				dt.setYear(date.getYear());
				dt.setMonth(date.getMonth());
				dt.setDay(date.getDay());
			}
			catch (Exception e) {
				System.out.println("Failed to set default value with: " + getWidgetDefault() + ": " + e);  //$NON-NLS-1$//$NON-NLS-2$
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
	}
}
