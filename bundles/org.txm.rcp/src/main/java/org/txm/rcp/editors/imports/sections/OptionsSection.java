package org.txm.rcp.editors.imports.sections;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.txm.core.preferences.TBXPreferences;
import org.txm.objects.Project;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.txm.rcp.messages.TXMUIMessages;

public class OptionsSection extends ImportEditorSection {

	private static final int SECTION_SIZE = 1;

	private Button cleanButton;

	private Button multithreadButton;

	public OptionsSection(ImportFormEditor editor, FormToolkit toolkit2, ScrolledForm form2, Composite parent, int style) {
		super(editor, toolkit2, form2, parent, style, "misc"); //$NON-NLS-1$

		this.section.setText(TXMUIMessages.Options);
		TableWrapLayout layout = new TableWrapLayout();
		layout.makeColumnsEqualWidth = true;
		this.section.setLayout(layout);
		this.section.setLayoutData(getSectionGridData(SECTION_SIZE));
		this.section.setEnabled(false);
		this.section.addExpansionListener(new ExpansionAdapter() {

			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.layout(true);
			}
		});

		Composite sectionClient = toolkit.createComposite(this.section);
		TableWrapLayout slayout = new TableWrapLayout();
		slayout.makeColumnsEqualWidth = false;
		slayout.numColumns = 2;
		sectionClient.setLayout(slayout);
		this.section.setClient(sectionClient);

		cleanButton = toolkit.createButton(sectionClient, TXMUIMessages.RemoveTemporarydirectories, SWT.CHECK);
		TableWrapData gdata = getTextGridData();
		gdata.grabHorizontal = true;
		gdata.colspan = 2;
		cleanButton.setLayoutData(gdata);

		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
			multithreadButton = toolkit.createButton(sectionClient, "Experimental multi-threading import", SWT.CHECK);
			gdata = getTextGridData();
			gdata.colspan = 2;
			gdata.grabHorizontal = true;
			multithreadButton.setLayoutData(gdata);
		}
	}

	@Override
	public void updateFields(Project project) {
		cleanButton.setSelection(project.getCleanAfterBuild());

		if (multithreadButton != null) {
			multithreadButton.setSelection(project.getDoMultiThread());
		}
	}

	@Override
	public boolean saveFields(Project project) {
		if (this.section != null && !section.isDisposed()) {

			project.setCleanAfterBuild(cleanButton.getSelection());
			if (multithreadButton != null) {
				project.setDoMultiThread(multithreadButton.getSelection());
			}
		}

		return true;
	}

	@Override
	public boolean checkFields() {
		return true;
	}

	@Override
	public int getSectionSize() {
		return SECTION_SIZE;
	}
}
