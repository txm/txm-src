// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands.queryview;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.objects.SavedQuery;
import org.txm.rcp.utils.IOClipboard;

// TODO: Auto-generated Javadoc
/**
 * The Class Copy.
 */
public class Copy extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);

		copy(selection);

		return null;
	}

	/**
	 * Copy.
	 *
	 * @param selection the selection
	 */
	public static void copy(IStructuredSelection selection) {
		String query = ""; //$NON-NLS-1$
		List<?> list = selection.toList();
		if (list.size() > 1) {
			for (Object q : list) {
				if (q instanceof SavedQuery) {
					if (query.length() != 0)
						query += " | "; //$NON-NLS-1$
					query += "(" + ((SavedQuery) q).getQuery() + ")"; //$NON-NLS-1$ //$NON-NLS-2$
				}
			}
		}
		else {
			query = ((SavedQuery) selection.getFirstElement()).getQuery();
		}

		IOClipboard.write(query);
	}
}
