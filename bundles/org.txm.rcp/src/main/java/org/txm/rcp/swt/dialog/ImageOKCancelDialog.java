package org.txm.rcp.swt.dialog;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class ImageOKCancelDialog extends Dialog {

	String title;

	Image image;

	String message;

	public ImageOKCancelDialog(Shell parent, String title, Image image, String message) {

		super(parent);

		this.setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.APPLICATION_MODAL);

		this.title = title;
		this.image = image;
		this.message = message;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {

		super.configureShell(newShell);

		newShell.setText(title);


	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite p) {

		Object layout = p.getLayout();
		if (layout instanceof GridLayout) {
			GridLayout glayout = (GridLayout) layout;
			glayout.marginWidth = 5;
			glayout.marginTop = 5;
			glayout.verticalSpacing = 5;
		}

		Label imageLabel = new Label(p, SWT.NONE);
		imageLabel.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, true, true));
		imageLabel.setImage(image);

		Label messageLabel = new Label(p, SWT.NONE);
		imageLabel.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		messageLabel.setText(message);

		return p;
	}
}
