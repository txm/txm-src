// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.widget;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.dialog.ComboDialog;
import org.txm.rcp.swt.dialog.QueryAssistDialog;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.SearchEngine;
import org.txm.searchengine.core.SearchEnginePreferences;
import org.txm.searchengine.core.SearchEnginesManager;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

/**
 * The QueryWidget
 * + the button to open the QueryAssisDialog
 * + Engine selector
 * 
 * @author mdecorde.
 */
public class AssistedChoiceQueryWidget extends Composite {

	/** The magicstick button: opens the QueryAssist dialog. */
	protected Button magicstick;

	boolean namedQueries = false;

	Text nameText;

	/** The querywidget. */
	protected QueryWidget querywidget;

	protected QueryAssistDialog d;

	// protected ComboViewer engineCombo;

	protected List<SearchEngine> engines;

	private GridData magcistickGData;

	private Button settingsButton;

	private Label selectedEngineLabel;

	private HashMap<String, String> newQueryOptions = new HashMap<String, String>();

	/**
	 * Instantiates a new assisted query widget WITH NO name.
	 *
	 * @param parent the parent
	 * @param style the style
	 * @param corpus the corpus
	 */
	public AssistedChoiceQueryWidget(Composite parent, int style, final CQPCorpus corpus) {
		this(parent, style, corpus, false);
	}

	/**
	 * Instantiates a new assisted named query widget
	 *
	 * @param parent the parent
	 * @param style the style
	 * @param corpus the corpus
	 * @param namedQueries activate the name field
	 */
	public AssistedChoiceQueryWidget(Composite parent, int style, final CQPCorpus corpus, boolean namedQueries) {
		super(parent, SWT.NONE);
		this.namedQueries = namedQueries;
		GridLayout parentLayout = GLComposite.createDefaultLayout(10);
		this.setLayout(parentLayout);

		engines = SearchEnginesManager.getAvailableEngines(corpus.getMainCorpus());
		SearchEngine initialEngine = null;
		if (engines.size() > 1) {

			// set default engine
			String defaultEngineName = SearchEnginePreferences.getInstance().getString(SearchEnginePreferences.DEFAULT_SEARCH_ENGINE);
			int iDefault = -1;
			int iSuperDefault = 0;
			for (int i = 0; i < engines.size(); i++) {
				SearchEngine engine = engines.get(i);
				if (defaultEngineName.equals(engine.getName())) {
					iDefault = i;
				}
				if ("CQP".equals(engine.getName())) { //$NON-NLS-1$
					iSuperDefault = i;
				}
			}

			if (iDefault >= 0) {
				initialEngine = engines.get(iDefault);
			}
			else {
				initialEngine = engines.get(iSuperDefault);
			}
		}
		else {
			initialEngine = engines.get(0);
		}

		selectedEngineLabel = new Label(this, SWT.NONE);
		selectedEngineLabel.setText(""); //$NON-NLS-1$
		GridData gdata = new GridData(GridData.FILL, GridData.CENTER, false, false);
		selectedEngineLabel.setLayoutData(gdata);

		if (SearchEnginePreferences.getInstance().getBoolean(SearchEnginePreferences.SHOW_SEARCH_ENGINES)) {

			settingsButton = new Button(this, SWT.PUSH);
			settingsButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			settingsButton.setImage(IImageKeys.getImage("platform:/plugin/org.eclipse.jface/org/eclipse/jface/dialogs/images/popup_menu.png")); //$NON-NLS-1$
			settingsButton.setToolTipText(TXMUIMessages.queryOptions);
			settingsButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {

					QueryOptionsDialog dialog = new QueryOptionsDialog(settingsButton, corpus, engines, querywidget.getSearchEngine(), newQueryOptions); //$NON-NLS-1$
					if (dialog.open() == ComboDialog.OK) {

						SearchEngine se = dialog.getSelectedEngine();
						if (se != null && !se.getName().equals(querywidget.getSearchEngine().getName())) {
							changeEngine(se);
						}

						HashMap<String, String> newOptions = dialog.getOptions();
						newQueryOptions.clear();
						for (String k : newOptions.keySet()) {
							HashMap<String, List<String>> a = se.getAvailableQueryOptions();
							if (a.containsKey(k)) {
								newQueryOptions.putAll(newOptions);
							}
						}
					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		}

		magicstick = new Button(this, SWT.PUSH);
		magcistickGData = new GridData(GridData.FILL, GridData.CENTER, false, false);
		magicstick.setLayoutData(magcistickGData);
		magicstick.setImage(IImageKeys.getImage(IImageKeys.ACTION_ASSISTQUERY));
		magicstick.setToolTipText(TXMUIMessages.openTheQueryAssistant);
		magicstick.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				d = new QueryAssistDialog(e.display.getActiveShell(), corpus);
				if (d.open() == Window.OK) {
					querywidget.setText(d.getQuery());
					if (nameText != null) nameText.setText("");
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		setMagicEnabled("CQP".equals(initialEngine.getName())); //$NON-NLS-1$

		if (namedQueries && TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {

			Label l = new Label(this, SWT.NONE);
			l.setText(" " + TXMUIMessages.name);
			l.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));

			nameText = new Text(this, SWT.BORDER);
			nameText.setToolTipText(TXMUIMessages.queryNameDisplayedInResultInsteadOfTheExactQuery);
			GridData gdatat = new GridData(GridData.FILL, GridData.CENTER, false, false);
			gdatat.widthHint = gdatat.minimumWidth = 100;
			nameText.setLayoutData(gdatat);

			l = new Label(this, SWT.NONE);
			l.setText(" " + TXMUIMessages.query);
			l.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));
		}

		querywidget = new QueryWidget(this, style, corpus.getProject(), initialEngine);
		querywidget.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
	}

	protected void changeEngine(SearchEngine se) {

		if (se == null) return;
		if (querywidget.getSearchEngine().getName().equals(se.getName())) return; // nothing to do

		querywidget.setSearchEngine(se);

		// GridData gdata = (GridData) querywidget.getLayoutData();
		// gdata.heightHint = -1;
		// querywidget.layout();
		// relayout();

		setMagicEnabled("CQP".equals(se.getName()) || "CQP".equals(se.getOptionForSearchengine())); //$NON-NLS-1$ //$NON-NLS-2$

		String defaultEngineName = SearchEnginePreferences.getInstance().getString(SearchEnginePreferences.DEFAULT_SEARCH_ENGINE);
		if (defaultEngineName.equals(se.getName()) || defaultEngineName.equals(se.getOptionForSearchengine())) {
			selectedEngineLabel.setText(""); //$NON-NLS-1$
		}
		else if (se.getOptionNameForSearchengine() != null) {
			selectedEngineLabel.setText(" " + se.getOptionNameForSearchengine() + " "); //$NON-NLS-1$ //$NON-NLS-2$
		}
		else {
			selectedEngineLabel.setText(" " + se.getName() + " "); //$NON-NLS-1$ //$NON-NLS-2$
		}
		selectedEngineLabel.getParent().layout();

		newQueryOptions.clear();
	}

	private void setMagicEnabled(boolean b) {
		magicstick.setEnabled(b);

		if (b) {
			magcistickGData.widthHint = magcistickGData.minimumWidth = SWT.DEFAULT;
		}
		else {
			magcistickGData.widthHint = magcistickGData.minimumWidth = 0;
		}
		magicstick.getParent().layout();
	}

	protected void relayout() {
		Composite parent = AssistedChoiceQueryWidget.this.getParent();
		// parent.layout(true);
		// parent.getParent().layout(true);
		try {
			parent.getParent().getParent().layout(true);
		}
		catch (Exception e) {
			parent.layout(true);
		}
		// while (parent instanceof Composite) {
		//
		// if (!(parent.getParent() instanceof Composite)) {
		// parent.layout();
		// return;
		// }
		// parent = parent.getParent();
		// }
	}

	@Override
	public void setEnabled(boolean b) {
		super.setEnabled(b);
		querywidget.setEnabled(b);
		if (nameText != null) nameText.setEnabled(b);
	}

	/**
	 * Warning this reset the Query String
	 * 
	 * @param engine the new engine
	 */
	public void setSearchEngine(SearchEngine engine) {
		int i = engines.indexOf(engine);
		if (i >= 0) {
			changeEngine(engine);
		}
	}

	public void setSearchEngine(String name) {
		for (int i = 0; i < engines.size(); i++) {
			if (name.equals(engines.get(i).getName())) {
				changeEngine(engines.get(i));
			}
		}
	}

	public QueryWidget getQueryWidget() {
		return querywidget;
	}

	@Override
	public boolean setFocus() {
		return this.querywidget.setFocus();
	}
	
	public void setQueryOptions(HashMap<String, String> options) {
		this.newQueryOptions = options;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Control#addKeyListener(org.eclipse.swt.events.KeyListener)
	 */
	@Override
	public void addKeyListener(KeyListener listener) {
		if (querywidget != null)
			this.querywidget.addKeyListener(listener);
		if (nameText != null)
			this.nameText.addKeyListener(listener);
	}

	/**
	 * Gets the query string.
	 *
	 * @return the query string
	 */
	public String getQueryString() {
		return querywidget.getQueryString();
	}

	/**
	 * Gets the query string.
	 *
	 * @return the query string
	 */
	public String getRawString() {
		return querywidget.getText();
	}

	/**
	 * Memorize.
	 */
	public void memorize() {
		querywidget.memorize(getQuery());
	}

//	/**
//	 * Memorize.
//	 *
//	 * @param query the query
//	 */
//	public void memorize(String query) {
//		querywidget.memorize(query);
//	}

	/**
	 * Clears the query test.
	 */
	public void clearQuery() {
		this.querywidget.setText(""); //$NON-NLS-1$
		if (nameText != null) nameText.setText("");
	}
	
	public void setQuery(IQuery q) {
		this.setSearchEngine(q.getSearchEngine());
		this.setText(q.getQueryString());
		this.setQueryName(q.getName());
		this.setQueryOptions(q.getOptions());
	}

	/**
	 * Sets the text.
	 *
	 * @param query the new text
	 */
	public void setText(String query) {

		// if(query.isEmpty()) {
		// query = ""; //$NON-NLS-1$
		// }

		// fix the query
		// query = query.replaceAll("^\"(.*)\"$","$1"); //$NON-NLS-1$ -> don't work for the '"A" "B"' queries

		querywidget.setText(query);
	}

	public void setQueryName(String name) {
		if (nameText != null) nameText.setText(name);
	}

	public String getQueryName() {
		if (nameText != null) return nameText.getText();
		return "";
	}

	public IQuery getQuery() {
		IQuery q = this.querywidget.getQuery();
		if (q != null && nameText != null) q.setName(nameText.getText());
		for (Entry<String, String> option : newQueryOptions.entrySet()) {
			q.setOption(option.getKey(), option.getValue());
		}
		return q;
	}
}
