// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views.fileexplorer;

import java.io.File;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.txm.Toolbox;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.commands.OpenBrowser;
import org.txm.rcp.commands.OpenGraph;
import org.txm.rcp.commands.workspace.LoadBinaryCorpus;
import org.txm.rcp.handlers.files.CopyFile;
import org.txm.rcp.handlers.files.CutFile;
import org.txm.rcp.handlers.files.DeleteFile;
import org.txm.rcp.handlers.files.EditFile;
import org.txm.rcp.handlers.files.PasteFile;
import org.txm.rcp.handlers.files.RenameFile;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.utils.FileUtils;

/**
 * Simple file explorer
 * 
 * @author mdecorde.
 */
public class Explorer extends ViewPart {

	/** The ID. */
	static public String ID = "org.txm.rcp.views.fileexplorer.Explorer"; //$NON-NLS-1$

	/** Initialize currentPath with user home directory */
	private String currentPath = System.getProperty("user.home");// Toolbox.getTxmHomePath(); //$NON-NLS-1$

	/** The tv. */
	TreeViewer tv;

	/** The address. */
	Text address;

	/**
	 * Instantiates a new explorer.
	 */
	public Explorer() {

	}

	/**
	 * Refresh.
	 */
	public static void refresh() {
		Explorer explorerView = (Explorer) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findView(
						Explorer.ID);

		if (explorerView == null) return;

		explorerView.tv.refresh();
	}

	/**
	 * Refresh.
	 */
	public static void refresh(File f) {
		if (f == null) return;

		Explorer explorerView = (Explorer) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findView(
						Explorer.ID);

		if (explorerView == null) return;

		explorerView.tv.refresh(f);
	}


	/**
	 * Opens the Fiel Explorer
	 */
	public static void open(File f) {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();

		try {
			IViewPart v = page.showView(Explorer.ID);
			if (f != null && v instanceof Explorer) {
				Explorer e = (Explorer) v;
				if (f.isDirectory()) {
					e.setRoot(f);
				}
				else {
					e.setRoot(f.getParentFile());
				}
			}
		}
		catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Opens the File Explorer
	 */
	public static void open() {
		open(null);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {

		FormLayout formLayout = new FormLayout();
		formLayout.marginWidth = 0;
		formLayout.marginHeight = 0;
		parent.setLayout(formLayout);

		// controles composite
		GLComposite controls = new GLComposite(parent, SWT.NONE, "buttons"); //$NON-NLS-1$

		FormData controlsData = new FormData();
		controlsData.top = new FormAttachment(0);
		controlsData.left = new FormAttachment(0);
		controlsData.right = new FormAttachment(100);
		controls.setLayoutData(controlsData);

		controls.getLayout().numColumns = 10;
		controls.getLayout().horizontalSpacing = 1;
		// its widgets
		Button go_up = new Button(controls, SWT.NONE);
		go_up.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		go_up.setText(TXMUIMessages.Explorer_4);
		go_up.setToolTipText(TXMUIMessages.goToParentFolder);
		go_up.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				File f = new File(address.getText()).getParentFile();
				if (f.exists() && f.isDirectory()) {
					setRoot(f);
				}
			}
		});

		address = new Text(controls, SWT.BORDER);
		if (currentPath == null) currentPath = "/"; //$NON-NLS-1$
		address.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		address.setText(currentPath);
		address.setToolTipText(TXMUIMessages.goToTXMHome);
		address.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.CR) {
					File f = new File(address.getText());
					if (f.exists() && f.isDirectory()) {
						setRoot(f);
					}
				}
			}
		});

		Button go = new Button(controls, SWT.NONE);
		go.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		go.setText(TXMUIMessages.Explorer_5);
		go.setToolTipText(TXMUIMessages.common_refresh);
		go.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				File f = new File(address.getText());
				if (f.exists() && f.isDirectory()) {
					setRoot(f);
				}
			}
		});

		Button homeBtn = new Button(controls, SWT.NONE);
		homeBtn.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		homeBtn.setText(TXMUIMessages.Explorer_7);
		homeBtn.setToolTipText(TXMUIMessages.goToTXMHome_2);
		homeBtn.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				currentPath = System.getProperty("user.home"); //$NON-NLS-1$
				setRoot(currentPath);
			}
		});

		Button txmHomeBtn = new Button(controls, SWT.NONE);
		txmHomeBtn.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		txmHomeBtn.setImage(IImageKeys.getImage(IImageKeys.TXM1616));
		txmHomeBtn.setToolTipText(TXMUIMessages.goToTXMHomeDirectory);
		txmHomeBtn.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				currentPath = Toolbox.getTxmHomePath();
				setRoot(currentPath);
			}
		});

		// the explorer
		tv = new TreeViewer(parent, SWT.VIRTUAL);
		tv.setContentProvider(new FileTreeContentProvider());
		tv.setUseHashlookup(true);
		tv.setLabelProvider(new FileTreeLabelProvider());
		tv.setInput(new File(currentPath));

		FormData tvData = new FormData();
		tvData.top = new FormAttachment(controls, 0);
		tvData.bottom = new FormAttachment(100);
		tvData.left = new FormAttachment(0);
		tvData.right = new FormAttachment(100);
		tv.getTree().setLayoutData(tvData);

		tv.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				TreeSelection selection = (TreeSelection) tv.getSelection();
				File selectedItem = (File) selection.getFirstElement();
				String ext = FileUtils.getExtension(selectedItem).toLowerCase();
				if (!selectedItem.isDirectory()) {
					if (ext.endsWith("html") || ext.endsWith("bmp") || ext.endsWith("png") || ext.endsWith("jpg") || ext.endsWith("gif")) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
					{
						OpenBrowser.openfile(selectedItem.getAbsolutePath());
					}
					else if (ext.equals("svg")) //$NON-NLS-1$
					{
						OpenGraph.OpenFile(selectedItem.getAbsolutePath());
					}
					else if (ext.equals("txm")) //$NON-NLS-1$
					{
						MessageBox dialog = new MessageBox(tv.getTree().getShell(), SWT.YES | SWT.NO);
						dialog.setMessage(NLS.bind(TXMUIMessages.loadTheP0CorpusFile, selectedItem));

						int buttonID = dialog.open();
						switch (buttonID) {
							case SWT.YES:
								try {
									LoadBinaryCorpus.loadBinaryCorpusArchive(selectedItem);
								}
								catch (ExecutionException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							case SWT.NO:
								// exits here ...
								break;
							case SWT.CANCEL:
								// does nothing ...
						}
					}
					else if (ext.equals("pdf") || ext.equals("odt") || ext.startsWith("doc")) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					{
						Program.launch(selectedItem.getAbsolutePath());
					}
					else {
						EditFile.openfile(selectedItem.getAbsolutePath());
					}
				}
				else {
					changeState(selection);
				}
			}
		});

		tv.getTree().addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				ISelection sel = tv.getSelection();
				if (sel instanceof IStructuredSelection) {
					List<Object> objects = ((IStructuredSelection) sel).toList();
					if (e.keyCode == SWT.DEL) {
						boolean definitive = true; // (e.stateMask & SWT.SHIFT) != 0;
						DeleteFile.delete(objects, definitive);
					}
					else if (e.keyCode == SWT.F5) {
						Explorer.refresh();
					}
					else {
						for (Object o : objects) {
							// Object o = ((IStructuredSelection)sel).getFirstElement();
							if (o instanceof File) {
								File file = (File) o;
								if (e.keyCode == SWT.DEL) {
									boolean definitive = true; // (e.stateMask & SWT.SHIFT) != 0;
									//DeleteFile.delete(file, definitive);
								}
								else if ((char) e.keyCode == 'c' & ((e.stateMask & SWT.CTRL) != 0)) {
									CopyFile.copy(file);
								}
								else if ((char) e.keyCode == 'x' & ((e.stateMask & SWT.CTRL) != 0)) {
									CutFile.cut(file);
								}
								else if ((char) e.keyCode == 'v' & ((e.stateMask & SWT.CTRL) != 0)) {
									if (file.isFile()) file = file.getParentFile();
									PasteFile.paste(file);
								}
								else if (e.keyCode == SWT.F2) {
									RenameFile.rename(file);
								}
							}
						}
					}
					tv.refresh();
				}
			}
		});

		MenuManager menuManager = new MenuManager();
		Menu menu = menuManager.createContextMenu(tv.getTree());

		// Set the MenuManager
		tv.getTree().setMenu(menu);
		getSite().registerContextMenu(menuManager, tv);
		// Make the selection available
		getSite().setSelectionProvider(tv);
	}

	/**
	 * Can delete.
	 *
	 * @param selectedItem the selected item
	 * @return true, if successful
	 */
	protected boolean CanDelete(File selectedItem) {
		File txmhome = new File(Toolbox.getTxmHomePath());
		File scripts = new File(Toolbox.getTxmHomePath(), "scripts"); //$NON-NLS-1$
		File corpora = new File(Toolbox.getTxmHomePath(), "corpora"); //$NON-NLS-1$
		// File registry = new File(Toolbox.getTxmHomePath(), "registry"); //$NON-NLS-1$
		File workspaces = new File(Toolbox.getTxmHomePath(), "workspaces"); //$NON-NLS-1$
		File xsl = new File(Toolbox.getTxmHomePath(), "xsl"); //$NON-NLS-1$
		File samples = new File(Toolbox.getTxmHomePath(), "samples"); //$NON-NLS-1$
		if (selectedItem.equals(txmhome) ||
				selectedItem.equals(txmhome) ||
				selectedItem.equals(scripts) ||
				selectedItem.equals(corpora) ||
				// selectedItem.equals(registry) ||
				selectedItem.equals(workspaces) ||
				selectedItem.equals(samples) ||
				selectedItem.equals(xsl)) {
			System.out.println(NLS.bind(TXMUIMessages.folderP0ShouldNotBeDeleted, selectedItem));
			return false;
		}
		return false;
	}

	/**
	 * Change state.
	 *
	 * @param selection the selection
	 */
	protected void changeState(TreeSelection selection) {
		TreeItem[] items = tv.getTree().getSelection();
		for (TreeItem item : items) {
			if (item.getExpanded()) {
				item.setExpanded(false);
			}
			else {
				item.setExpanded(true);
			}
			tv.refresh();
		}
	}

	/**
	 * Sets the root.
	 *
	 * @param home the new root
	 */
	public void setRoot(String home) {
		this.currentPath = home;
		address.setText(home);
		tv.setInput(new File(home));
		tv.refresh();
	}

	/**
	 * Sets the root.
	 *
	 * @param f the new root
	 */
	public void setRoot(File f) {
		this.currentPath = f.getAbsolutePath();
		address.setText(currentPath);
		tv.setInput(new File(currentPath));
		tv.refresh();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

}
