package org.txm.rcp.commands.workspace;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamException;

import org.txm.importer.StaxIdentityParser;

public class XMLLemmaProjection extends StaxIdentityParser {

	// form -> pos -> source -> lemma
	protected LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>> rules = null;

	protected HashSet<String> formAsLemmaPosList = null;

	protected String lemmaProperty;

	protected HashSet<String> noMatchValues = new HashSet<String>();

	protected String posProperty;

	protected LinkedHashSet<String> lemmaSourcePriorityList;

	public XMLLemmaProjection(File infile, LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>> rules,
			HashSet<String> formAsLemmaPosList,
			LinkedHashSet<String> lemmaSourcePriorityList,
			String posProperty, String lemmaProperty) throws IOException, XMLStreamException {
		super(infile);
		this.rules = rules;
		this.formAsLemmaPosList = formAsLemmaPosList;
		this.lemmaSourcePriorityList = lemmaSourcePriorityList;
		this.lemmaProperty = lemmaProperty;
		this.posProperty = posProperty;

		// the XML-TXM files word properties name starts wit # (they are references)
		if (!this.lemmaProperty.startsWith("#")) this.lemmaProperty = "#" + this.lemmaProperty;
		if (!this.posProperty.startsWith("#")) this.posProperty = "#" + this.posProperty;
	}

	boolean inW = false, inAna = false, inForm;

	LinkedHashMap<String, String> anaValues = new LinkedHashMap<String, String>();

	LinkedHashMap<String, String> anaResps = new LinkedHashMap<String, String>();

	String typeName = null;

	String respName = null;

	String formValue, typeValue = null;

	@Override
	public void processStartElement() throws XMLStreamException, IOException {
		if (!inW) super.processStartElement(); // don't write W content

		if (localname.equals("w")) {
			inW = true;
			anaValues.clear();
			anaResps.clear();

			// initialize the new type to a empty value in case there is transformation rule
			anaValues.put(lemmaProperty, "");
			anaResps.put(lemmaProperty, "#txm_recode");
		}
		else if (localname.equals("ana")) {
			inAna = true;
			typeName = parser.getAttributeValue(null, "type");
			respName = parser.getAttributeValue(null, "resp");
			anaResps.put(typeName, respName);
			// if (typeName != null) typeName = typeName.substring(1); // remove #
			typeValue = "";
		}
		else if (localname.equals("form")) {
			inForm = true;
			formValue = "";
		}
	}

	@Override
	public void processCharacters() throws XMLStreamException {
		if (inW && inAna) typeValue += parser.getText();
		else if (inW && inForm) formValue += parser.getText();
		else super.processCharacters();
	}

	@Override
	public void processEndElement() throws XMLStreamException {
		if (localname.equals("w")) {
			inW = false;

			// write W content
			try {
				// get the value to test
				String posValue = anaValues.get(posProperty);
				if (posValue == null) {
					posValue = "<no_pos>";
					// anaValues.put(posProperty, "<no_pos>");
					// anaResps.put(posProperty, "txm_recode");
				}
				String value = updateAnaValuesIfMatch(formValue.trim(), posValue.trim());
				// System.out.println("form="+formValue+" + pos="+posValue+" -> "+value);
				anaValues.put(lemmaProperty, value);
				anaResps.put(lemmaProperty, "#txm_recode");

				// write the word element
				writer.writeStartElement("txm:form");
				writer.writeCharacters(formValue);
				writer.writeEndElement();

				for (String k : anaValues.keySet()) {
					writer.writeStartElement("txm:ana");
					writer.writeAttribute("resp", anaResps.get(k));
					writer.writeAttribute("type", k);
					writer.writeCharacters(anaValues.get(k));
					writer.writeEndElement();
				}
			}
			catch (XMLStreamException e) {
				e.printStackTrace();
			}
		}
		else if (localname.equals("ana")) {
			anaValues.put(typeName, typeValue);
			inAna = false;
		}
		else if (localname.equals("form")) {
			inForm = false;
		}

		if (!inW) super.processEndElement(); // don't write W content now
	}

	protected String updateAnaValuesIfMatch(String formValue, String posValue) {
		if (posValue == null) return "";

		if (formAsLemmaPosList.contains(posValue)) return formValue;


		if (formValue.equals("virge")) System.out.println("testing: " + formValue + " " + posValue);
		if (formValue.equals("virge")) System.out.println("form connue? " + rules.containsKey(formValue));
		if (rules.containsKey(formValue)) {
			LinkedHashMap<String, LinkedHashMap<String, String>> posHash = rules.get(formValue);
			// if (posHash.containsKey(posValue)) {
			// LinkedHashMap<String, String> sourceHash = posHash.get(posValue);
			// for (String source : lemmaSourcePriorityList) {
			// if (sourceHash.containsKey(source)) {
			// return sourceHash.get(source);
			// }
			// }
			// }
			if (formValue.equals("virge")) System.out.println(" tests" + posHash.keySet());
			for (String posRegexp : posHash.keySet()) {

				if (posValue.matches(posRegexp)) {
					return posHash.get(posRegexp).toString();
				}
			}
		}

		// try without maj
		String formValueMin = formValue.toLowerCase();
		if (rules.containsKey(formValueMin)) {
			LinkedHashMap<String, LinkedHashMap<String, String>> posHash = rules.get(formValueMin);
			// if (posHash.containsKey(posValue)) {
			// LinkedHashMap<String, String> sourceHash = posHash.get(posValue);
			// for (String source : lemmaSourcePriorityList) {
			// if (sourceHash.containsKey(source)) {
			// return sourceHash.get(source);
			// }
			// }
			// }

			for (String posRegexp : posHash.keySet()) {
				if (posValue.matches(posRegexp)) {
					return posHash.get(posRegexp).toString();
				}
			}
		}

		noMatchValues.add(formValue + "|" + posValue);
		return "!" + formValue;
	}

	public HashSet<String> getNoMatchValues() {
		return noMatchValues;
	}

	public static void main(String args[]) {
		File xmlFile = new File("/home/mdecorde/TXM/corpora/XTZTEXTUALPLANS/txm/XTZTEXTUALPLANS/test.xml");
		File tmpFile = new File("/home/mdecorde/TXM/corpora/XTZTEXTUALPLANS/txm/XTZTEXTUALPLANS/test-o.xml");
		String posProperty = "type";
		String newType = "lemma";
		LinkedHashMap<Pattern[], String> rules = new LinkedHashMap<Pattern[], String>();
		rules.put(new Pattern[] { Pattern.compile("w"), Pattern.compile("w") }, "WORD");
		rules.put(new Pattern[] { Pattern.compile("x.+"), Pattern.compile("w") }, "XWORD");
		rules.put(new Pattern[] { Pattern.compile("y"), Pattern.compile("w") }, "YWORD");
		rules.put(new Pattern[] { Pattern.compile("y.*"), Pattern.compile("w") }, "YMULTIWORD");
		// XMLPropertyProjection converter = new XMLPropertyProjection(xmlFile, rules, posProperty, newType);
		// System.out.println(converter.process(tmpFile));
	}
}
