// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.results;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.results.TXMResult;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.utils.logger.Log;

/**
 * Clone the selected TXM result node.
 * 
 * @author sjacquot, mdecorde
 * 
 */
public class PasteTXMResultTree extends BaseAbstractHandler {



	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);

		if (!(selection.getFirstElement() instanceof TXMResult)) {
			System.out.println(TXMUIMessages.OnlyATXMResultCanBeCloned);
			return super.logCanNotExecuteCommand(selection);
		}



		TXMResult newParent = (TXMResult) selection.getFirstElement();

		paste(newParent);
		return null;
	}

	public static Object paste(TXMResult newParent) {

		TXMResult srcNode = CopyTXMResultTree.srcNode;

		if (srcNode == null) {
			Log.fine(TXMUIMessages.nothingToPaste);
			return null;
		}

		if (srcNode.getParent() == null) {
			Log.fine("Error: result parent is null"); //$NON-NLS-1$
			return null;
		}

		if (newParent == srcNode) { // to duplicate the object
			newParent = srcNode.getParent();
		} else if (!srcNode.canComputeWith(newParent)) {
			Log.warning("Cannot paste to "+newParent.getSimpleName());
			return null;
		}

		Log.info(NLS.bind(TXMUIMessages.pastingP0ToP1, srcNode, newParent));

		TXMResult result = null;
		if (CopyTXMResultTree.cut) {
			result = srcNode;
			srcNode.moveTo(newParent);
			CopyTXMResultTree.srcNode = null;
		} else {
			if (srcNode.isAltered()) {
				Log.info(TXMUIMessages.WarningOnlyParametersAreClonedManualChangesAreNotTransfered);
			}
			
			result = srcNode.clone(newParent, CopyTXMResultTree.all);
			if (result == null) {
				Log.warning(TXMUIMessages.errorTheResultWasNotCloned);
			}
		}

		//CorporaView.refreshObject(newParent);
		CorporaView.refresh();
		CorporaView.reveal(result);
		return result;
	}
}
