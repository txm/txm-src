package org.txm.rcp.editors.imports.sections;

import java.awt.GraphicsEnvironment;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.txm.objects.Project;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.txm.rcp.messages.TXMUIMessages;

public class FontSection extends ImportEditorSection {

	private static final int SECTION_SIZE = 1;

	public static final String DEFAULTFONT = ""; //$NON-NLS-1$

	Combo fontCombo;

	public FontSection(ImportFormEditor editor, FormToolkit toolkit2, ScrolledForm form2, Composite parent, int style) {
		super(editor, toolkit2, form2, parent, style, "font"); //$NON-NLS-1$

		this.section.setText(TXMUIMessages.displayFont);
		TableWrapLayout layout = new TableWrapLayout();
		layout.makeColumnsEqualWidth = true;
		this.section.setLayout(layout);
		this.section.setLayoutData(getSectionGridData(SECTION_SIZE));
		this.section.setEnabled(false);

		this.section.addExpansionListener(new ExpansionAdapter() {

			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.layout(true);
			}
		});

		// filesection.setDescription("Select how to find source files");
		Composite sectionClient = toolkit.createComposite(this.section);
		TableWrapLayout slayout = new TableWrapLayout();
		slayout.makeColumnsEqualWidth = false;
		slayout.numColumns = 2;
		sectionClient.setLayout(slayout);
		this.section.setClient(sectionClient);

		Label l = toolkit.createLabel(sectionClient, TXMUIMessages.fontName, SWT.WRAP);
		l.setLayoutData(new TableWrapData(TableWrapData.LEFT, TableWrapData.MIDDLE));

		fontCombo = new Combo(sectionClient, SWT.BORDER);
		TableWrapData gdata = getTextGridData();
		fontCombo.setLayoutData(gdata);

		fontCombo.removeAll();
		GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
		java.awt.Font[] fonts = e.getAllFonts(); // Get the fonts

		fontCombo.add(DEFAULTFONT);
		for (java.awt.Font font : fonts) {
			fontCombo.add(font.getFontName());
		}

		// updateFontSection();


	}

	@Override
	public void updateFields(Project project) {
		if (this.section.isDisposed()) return;
		if (project == null) return;

		String value = project.getFont(); // $NON-NLS-1$

		if (value == null || value.length() == 0) value = DEFAULTFONT;

		if (value != null && value.length() > 0) {
			fontCombo.setText(value);
		}
		else {
			fontCombo.setText(""); //$NON-NLS-1$
		}
	}

	@Override
	public boolean saveFields(Project project) {
		if (!this.section.isDisposed()) {
			String value = fontCombo.getText();
			if (DEFAULTFONT.equals(value)) value = ""; //$NON-NLS-1$

			project.setFont(value); // $NON-NLS-1$
			return true;
		}
		return true;
	}

	@Override
	public boolean checkFields() {
		return true;
	}

	@Override
	public int getSectionSize() {
		return SECTION_SIZE;
	}
}
