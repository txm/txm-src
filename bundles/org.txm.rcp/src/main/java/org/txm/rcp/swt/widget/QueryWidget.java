// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.widget;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.txm.objects.Project;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.QueryBasedTXMResult;
import org.txm.searchengine.core.QueryHistory;
import org.txm.searchengine.core.SearchEngine;

// TODO: Auto-generated Javadoc
/**
 * A widget to enter query strings. It features query history and accepts both
 * full CQP query strings and simplif ied query strings (see
 * {@link org.txm.searchengine.cqp.corpus.query.CQLQuery#fixQuery(String)})
 * 
 * @author jmague, mdecorde
 * 
 */
public class QueryWidget extends Combo {

	SearchEngine se;

	Project project;

	private QueryHistory h;

	/** The history. */
	private static Deque<String> history = new LinkedList<>();

	/**
	 * Instantiates a new query widget.
	 *
	 * @param parent the parent
	 * @param style  the style
	 */
	public QueryWidget(Composite parent, int style, Project project, SearchEngine se) {

		super(parent, style);
		this.se = se;
		if (se == null) {
			System.out.println("WARNING"); //$NON-NLS-1$
		}
		this.project = project;

		if (se != null) {
			this.setToolTipText(TXMUIMessages.bind(TXMUIMessages.queryForTheP0SearchEngine, se.getName()));
		}
		loadHistory();

		DropTarget target = new DropTarget(this, DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_LINK);
		target.setTransfer(new Transfer[] { LocalSelectionTransfer.getTransfer() }); 
		target.addDropListener(new DropTargetAdapter() {

			@Override
			public void drop(DropTargetEvent event) {

				Object o = event.data;
				
				if (o instanceof StructuredSelection ss) {
					o = ss.getFirstElement();
				}
				
//				System.out.println("D&D drop "+event);
				//TODO rethink this drop&down method to do a real setQuery(Query q) instead of setting String values
				if (o instanceof QueryBasedTXMResult qbResult) {
					IQuery q = qbResult.getQuery();
					if (q != null) {
						setSearchEngine(q.getSearchEngine());
						setText(q.getQueryString());
					}
				} else if (o instanceof Query q) {
					setSearchEngine(q.getSearchEngine());
					setText(q.getQueryString());
				}
			}
		});
	}

	public void loadHistory() {

		h = null;

		if (this.project != null) {
			h = this.project.getFirstChild(QueryHistory.class);

			if (h == null) {
				h = new QueryHistory(project);
			}
		} else {
			return;
		}

		try { // load history from queries.txt file
			h.compute(false);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		setHistoryItems();
	}

	private void setHistoryItems() {

		if (this.h != null) {

			ArrayList<? extends Query> queries = null;
			if (se != null) { // get onlny the queries of the current searchengine
				queries = h.getQueries(se.newQuery().getClass());
			} else { // get all queries
				queries = h.getQueries();
			}

			String[] strings = new String[queries.size()];
			for (int i = 0; i < queries.size(); i++) {
				strings[i] = queries.get(queries.size() - i - 1).getQueryString();
			}
			this.setItems(strings);
		} else { // get the general query history
			this.setItems(history.toArray(new String[history.size()]));
		}
	}

	// public QueryWidget(Composite composite, int style) {
	// this(composite, style, SearchEnginesManager.getCQPSearchEngine());
	// }

	/**
	 * Set the SearchEngine to use with query
	 * 
	 * @param se
	 */
	public void setSearchEngine(SearchEngine se) {

		if (this.se == se)
			return;

		if (se != null) {
			this.setToolTipText("Query for " + se.getName());
		}

		this.se = se;

		loadHistory();
		// if (se.newQuery().canBeMultiLine()) {
		// Object l = this.getLayoutData(); // cannot change the widget style -> maybe
		// use a ComboViewer OR recreate a widget (so the setSearchEngine must be moved)
		// if (l instanceof GridData) {
		// GridData gd = (GridData)l;
		// gd.minimumHeight = this.getSize().y * 2;
		// }
		// this.layout();
		// } else {
		// Object l = this.getLayoutData(); // cannot change the widget style -> maybe
		// use a ComboViewer OR recreate a widget (so the setSearchEngine must be moved)
		// if (l instanceof GridData) {
		// GridData gd = (GridData)l;
		// gd.minimumHeight = this.getSize().y / 2;
		// }
		// this.layout();
		// }
	}

	public SearchEngine getSearchEngine() {
		return this.se;
	}

	/**
	 * Gets the CQP query string inferred from the user input.
	 *
	 * @return the CQP query string
	 */
	public String getQueryString() {
		String rawQuery;
		if (getSelectionIndex() < 0) {
			rawQuery = getText();
		} else {
			rawQuery = this.getItem(this.getSelectionIndex());
		}
		return se.newQuery().setQuery(rawQuery).getQueryString();
	}

	/**
	 * Memorize.
	 * @return 
	 */
	public IQuery memorize() {

		String inputString;
		IQuery q;
		if (getSelectionIndex() == -1) {
			inputString = getText();
		} else {
			inputString = getItems()[getSelectionIndex()];
		}

		if (inputString == null || inputString.length() == 0)
			return null;

		if (h != null && se != null && project != null) {
//			Query q = se.newQuery();
//			q.setQuery(inputString);
			h.addQuery(this.getQuery());
			q = this.getQuery();
		} else {
			history.remove(inputString);
			history.addFirst(inputString);
			q = se.newQuery();
			q.setQuery(inputString);
		}

		setHistoryItems();
		this.setText(this.getItem(0));
		return q;
	}
	
	public void memorize(IQuery query) {
		
		if (query == null) return;
		
		String inputString = query.getQueryString();
		if (getSelectionIndex() == -1) {
			inputString = getText();
		} else {
			inputString = getItems()[getSelectionIndex()];
		}

		if (inputString == null || inputString.length() == 0)
			return;

		if (h != null && se != null && project != null) {
			h.addQuery(query);
		} else {
			history.remove(inputString);
			history.addFirst(inputString);
		}

		setHistoryItems();
		this.setText(this.getItem(0));
	}

//	/**
//	 * Memorize.
//	 *
//	 * @param query the query to memorize
//	 */
//	public void memorize(String query) {
//
//		String inputString = query;
//		this.setText(query);
//		history.remove(inputString);
//		history.addFirst(inputString);
//		this.setItems(history.toArray(new String[history.size()]));
//		this.setText(this.getItem(0));
//
//		if (h != null && se != null && project != null) {
//			Query q = se.newQuery();
//			q.setQuery(inputString);
//			h.addQuery(q);
//		}
//	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Combo#checkSubclass()
	 */
	@Override
	protected void checkSubclass() {

	}

	/**
	 * Gets the query.
	 *
	 * @return the query or null if no query is set
	 */
	public IQuery getQuery() {

		if (this.getQueryString().isEmpty()) {
			return null;
		}

		if (se != null) {
			return se.newQuery().setQuery(this.getQueryString());
		} else {
			Query q = new Query();
			q.setQuery(this.getQueryString());
			return q;
		}
	}


}
