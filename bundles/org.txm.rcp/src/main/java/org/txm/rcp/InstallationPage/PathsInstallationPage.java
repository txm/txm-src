package org.txm.rcp.InstallationPage;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.about.InstallationPage;
import org.txm.Toolbox;
import org.txm.rcp.messages.TXMUIMessages;

/**
 * Paths TabFolder for the Installation details page
 * 
 * @author mdecorde
 *
 */
public class PathsInstallationPage extends InstallationPage {

	private TreeViewer treeViewer;

	public PathsInstallationPage() {

		this.setTitle(TXMUIMessages.TXMPaths);
	}

	@Override
	public void createControl(Composite parent) {

		parent.setLayout(new GridLayout(1, false));

		Label l = new Label(parent, SWT.NONE);
		l.setText(TXMUIMessages.TXMCommonPathListClickOnTheOpenButtonEtc);
		l.setLayoutData(new GridData());

		treeViewer = new TreeViewer(parent);
		Tree tree = treeViewer.getTree();
		tree.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		tree.setHeaderVisible(true);
		treeViewer.setContentProvider(new ITreeContentProvider() {

			@SuppressWarnings("rawtypes")
			@Override
			public boolean hasChildren(Object element) {
				if (element instanceof List) {
					return !((List) element).isEmpty();
				}
				return false;
			}

			@Override
			public Object getParent(Object element) {

				return null;
			}

			@Override
			public Object[] getElements(Object inputElement) {

				return ((List) inputElement).toArray();
			}

			@SuppressWarnings("rawtypes")
			@Override
			public Object[] getChildren(Object parentElement) {
				if (parentElement instanceof List) {
					return ((List) parentElement).toArray();
				}
				return new Object[0];
			}
		});

		//treeViewer.setComparator(new ViewerComparator());
		treeViewer.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				openSelection();
			}
		});

		TreeViewerColumn nameColumn = new TreeViewerColumn(treeViewer, SWT.LEFT);
		nameColumn.getColumn().setText(TXMUIMessages.What);
		nameColumn.getColumn().setWidth(200);
		nameColumn.setLabelProvider(new ColumnLabelProvider() {

			public String getText(Object element) {
				return ((Object[]) element)[0].toString();
			}
		});

		TreeViewerColumn pathColumn = new TreeViewerColumn(treeViewer, SWT.LEFT);
		pathColumn.getColumn().setText(TXMUIMessages.Paths);
		pathColumn.getColumn().setWidth(200);
		pathColumn.setLabelProvider(new ColumnLabelProvider() {

			public String getText(Object element) {
				return ((Object[]) element)[1].toString();
			}
		});

		treeViewer.setInput(Arrays.asList(
				new Object[] { "Current path", new File("").getAbsolutePath() }, //$NON-NLS-1$ //$NON-NLS-2$
				new Object[] { TXMUIMessages.LogsDirectory, new File("").getAbsolutePath() }, //$NON-NLS-1$
				new Object[] { TXMUIMessages.CrashLogsDirectory, new File(Toolbox.getInstallDirectory()) },
				new Object[] { TXMUIMessages.TXMUserHomeDirectory, new File(Toolbox.getTxmHomePath()) },
				new Object[] { TXMUIMessages.InstallationDirectory, new File(Toolbox.getInstallDirectory()) },
				new Object[] { TXMUIMessages.InstanceConfigurationFile, new File(Toolbox.getConfigurationDirectory(), "launcher.ini") }, //$NON-NLS-1$
				new Object[] { TXMUIMessages.InstanceConfigurationDirectory, new File(Toolbox.getConfigurationDirectory()) }

		));

		Button openButton = new Button(parent, SWT.NONE);
		openButton.setLayoutData(new GridData());
		openButton.setText(TXMUIMessages.OpenEtc);
		openButton.setToolTipText(TXMUIMessages.OpenTheSelectedPathWithTheDefaultToolSetByTheSystem);
		openButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				openSelection();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}

	protected void openSelection() {

		Object selection = treeViewer.getStructuredSelection().getFirstElement();
		if (selection != null && selection instanceof Object[]) {
			Program.launch(((Object[]) selection)[1].toString());
		}
	}
}
