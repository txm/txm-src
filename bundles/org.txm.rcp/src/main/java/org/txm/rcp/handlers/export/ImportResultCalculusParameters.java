// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.rcp.handlers.export;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Properties;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.osgi.framework.Bundle;
import org.txm.Toolbox;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.TXMResult;
import org.txm.rcp.StatusLine;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.utils.DeleteDir;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;
import org.txm.utils.zip.Zip;

/**
 * Imports the results .parameters files contained in a zip file, reconstructs the branch if needed, and attaches it to an existing parent.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class ImportResultCalculusParameters extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		final Object s = selection.getFirstElement();
		if (!(s instanceof TXMResult selectedParentResult)) {
			return null;
		}
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

		// String txmhome = Toolbox.getTxmHomePath();
		FileDialog dialog = new FileDialog(shell, SWT.OPEN);

		String extensions[] = { "*.txmcmd" }; //$NON-NLS-1$
		dialog.setFilterExtensions(extensions);

		if (LastOpened.getFile(ImportResultCalculusParameters.class.getName()) != null) {
			dialog.setFilterPath(LastOpened.getFolder(ImportResultCalculusParameters.class.getName()));
		}
		try {
			String path = dialog.open();
			if (path != null) {
				importCommand(new File(path), selectedParentResult);
			}

		}
		catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return null;
		}
		return null;
	}

	/**
	 * Imports the results .parameters files contained in the specified zip file, reconstructs the branch if needed, and attaches it to the specified parent.
	 * If parent is set to null, tries to find an existing parent using the parent UIDD stored in the .parameters files.
	 * 
	 * @param zipfile
	 * @param parentResult
	 * @throws IOException
	 */
	public static void importCommand(File zipfile, TXMResult parentResult) throws IOException {
		
		StatusLine.setMessage(TXMUIMessages.importingTheCalculus);
		File outdir = new File(Toolbox.getTxmHomePath(), "results/import"); //$NON-NLS-1$
		DeleteDir.deleteDirectory(outdir);

		Zip.decompress(zipfile, outdir, false);

		LinkedHashMap<File, Properties> fileToProperties = new LinkedHashMap<>(); // to retrieve a new result from its old uuid
		HashMap<String, File> oldUUIDToPropertiesFile = new HashMap<>(); // to retrieve a new result from its old uuid
		HashMap<String, String> oldUUIDToNewUUID = new HashMap<>(); // to retrieve a new result from its old uuid
		HashMap<String, String> UUIDDependencies = new HashMap<>(); // to -> from, if from is null use the selected TXMResult as parent

		File[] files = outdir.listFiles();
		Arrays.sort(files);

		for (File parametersFile : files) { // read parameters files
			if (!parametersFile.getName().endsWith(".parameters")) continue; //$NON-NLS-1$

			Properties props = new Properties();
			BufferedReader r = IOUtils.getReader(parametersFile);
			props.load(r);
			r.close();

			fileToProperties.put(parametersFile, props);
			String UUID = props.getProperty(TXMPreferences.RESULT_PARAMETERS_NODE_PATH);
			oldUUIDToPropertiesFile.put(UUID, parametersFile);
		}

		for (File parametersFile : outdir.listFiles()) { // build imported results dependency
			// if (TXMPreferences.PARENT_PARAMETERS_NODE_PATH.equals(ps)) continue;
			//if (TXMPreferences.RESULT_PARAMETERS_NODE_PATH.equals(ps)) continue;
			Properties props = fileToProperties.get(parametersFile);
			String UUID = props.getProperty(TXMPreferences.RESULT_PARAMETERS_NODE_PATH);
			String parentUUID = props.getProperty(TXMPreferences.PARENT_PARAMETERS_NODE_PATH);
			if (!oldUUIDToPropertiesFile.containsKey(parentUUID)) {
				UUIDDependencies.put(UUID, null);
			}
			else {
				UUIDDependencies.put(UUID, parentUUID);
			}
		}

		JobHandler jobhandler = new JobHandler(TXMUIMessages.importStarted) {

			@Override
			protected IStatus _run(SubMonitor monitor) throws Exception {

				for (File parametersFile : fileToProperties.keySet()) {
					TXMResult importedResult = null;
					try {
						monitor.beginTask(TXMUIMessages.importingTheCalculus, 100);

						Properties props = fileToProperties.get(parametersFile);
						String UUID = props.getProperty(TXMPreferences.RESULT_PARAMETERS_NODE_PATH);

						String className = props.getProperty(TXMPreferences.CLASS, ""); //$NON-NLS-1$
						if (className == null || className.length() == 0) {
							Log.warning(NLS.bind("Warning: can not import object with no class name set with path={0}.", parametersFile)); //$NON-NLS-1$
							continue;
						}
						String bundleId = props.getProperty(TXMPreferences.BUNDLE_ID, ""); //$NON-NLS-1$
						if (bundleId == null || bundleId.length() == 0) {
							Log.warning(NLS.bind("Warning: can not import {0} object with no bundle id set.", parametersFile)); //$NON-NLS-1$
							continue;
						}
						Bundle bundle = Platform.getBundle(bundleId);
						if (bundle == null) {
							Log.warning(NLS.bind("Warning: can not import {0} object with bundle id={1}.", className, parametersFile)); //$NON-NLS-1$
							continue;
						}

						Class<?> cl = bundle.loadClass(className);

						String resultNodePath = parentResult.getProject().getParametersNodeRootPath() + TXMResult.createUUID() + "_" + cl.getSimpleName(); //$NON-NLS-1$
						Log.info(NLS.bind(TXMUIMessages.importingAP0From, cl.getSimpleName(), resultNodePath));
						TXMResult parent = null;

						if (parent == null && UUIDDependencies.get(UUID) != null) {
							parent = TXMResult.getResult(oldUUIDToNewUUID.get(UUIDDependencies.get(UUID)));
						}
						else {
							parent = parentResult;
						}

						Constructor<?> cons = cl.getConstructor(String.class);

						// fill the preference node with the right parameter **and then** set the right parent node path 
						for (Object p : props.keySet()) {
							String ps = p.toString();
							TXMPreferences.put(resultNodePath, ps, props.getProperty(ps, "")); //$NON-NLS-1$
						}
						TXMPreferences.put(resultNodePath, TXMPreferences.PARENT_PARAMETERS_NODE_PATH, parent.getParametersNodePath());

						// this should creates the right TXMResult, parameters string values are set
						importedResult = (TXMResult) cons.newInstance(resultNodePath);
						importedResult.autoLoadParametersFromAnnotations(); // update java parameters
						importedResult.loadParameters();

						oldUUIDToNewUUID.put(UUID, resultNodePath);
					}
					// User canceling
					catch (ThreadDeath td) {
						if (importedResult != null) {
							importedResult.delete();
						}
						throw td;
					}
					catch (Exception e) {
						Log.info(NLS.bind(TXMUIMessages.errorWhileLoadingCalculusP0P1, parentResult, e));
						org.txm.utils.logger.Log.printStackTrace(e);
						if (importedResult != null) {
							importedResult.delete();
						}
						throw e;
					}

				}

				this.syncExec(new Runnable() {

					@Override
					public void run() {

						CorporaView.refresh();
					}
				});
				return Status.OK_STATUS;
			}
		};

		jobhandler.startJob();
	}
}
