// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.export;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.txm.Toolbox;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.utils.io.FileCopy;

/**
 * save the SVG file selected @ author mdecorde.
 */
@Deprecated
public class ExportSVG extends AbstractHandler {

	private static final String ID = "org.txm.rcp.commands.function.ExportSVG"; //$NON-NLS-1$
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// FIXME : implement charts engine after we defined if here we need to export the chart or the view
		//		try {
		//			// After that call directly "ExportChart" or "ExportChartEditorView"
		//			System.out.println(Messages.ExportSVG_0);
		//			Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		//
		//			String txmhome = Toolbox.getTxmHomePath();
		//
		//			// get editor
		//			IWorkbenchPart page = HandlerUtil.getActiveWorkbenchWindow(event)
		//					.getActivePage().getActivePart();
		//
		//			String prefDevice = TxmPreferences.getString(ExportPreferencePage.RDEVICE, "SVG"); //$NON-NLS-1$
		//			RDevice device = RDevice.valueOf(prefDevice.toUpperCase());
		//			System.out.println(NLS.bind(Messages.ExportSVG_2, device));
		//
		//			new File(txmhome, "results").mkdir(); //$NON-NLS-1$
		//			File svgSourceFile = new File(txmhome, "results/temp"+device.getExt()); //$NON-NLS-1$
		//			
		//			if (page instanceof SVGGraphicEditor) {
		//				SVGGraphicEditor te = (SVGGraphicEditor) page;
		//				svgSourceFile = te.getSVGFile();
		//				device = RDevice.SVG;
		//			} else if (page instanceof CorporaView) {
		//				CorporaView view = (CorporaView) page;
		//				StructuredSelection sel = (StructuredSelection) view.getTreeViewer().getSelection();
		//				Object obj = sel.getFirstElement();
		//				//System.out.println("sel: "+obj);
		//				if (obj instanceof CAH) {
		//					ChartsEngine.getChartsEngine(RChartsEngine.class).getChartCreator(CAH.class).createChartFile(obj, svgSourceFile, CAHPreferences.PREFERENCES_NODE);
		////						((CAH)obj).toSVG(svgSourceFile, device);
		//				}
		//				else if (obj instanceof Progression) {
		//					ChartsEngine.getChartsEngine(RChartsEngine.class).getChartCreator(Progression.class).createChartFile(obj, svgSourceFile, ProgressionPreferences.PREFERENCES_NODE);
		////						((Progression)obj).toSvg(svgSourceFile, device);
		//				}
		//			}
		//
		//			if (svgSourceFile != null && svgSourceFile.exists()) {
		//				String ext = null;
		//				if (svgSourceFile.getName().lastIndexOf(".") > 0) { //$NON-NLS-1$
		//					ext = svgSourceFile.getName();
		//					ext = ext.substring(ext.lastIndexOf("."), ext.length()); //$NON-NLS-1$
		//				}
		//				FileDialog dialog = new FileDialog(shell, SWT.SAVE);
		//				String[] exts = {"*"+device.getExt()}; //$NON-NLS-1$
		//				dialog.setFilterExtensions(exts);
		//				
		//				if (LastOpened.getFile(ID) != null) {
		//					dialog.setFilterPath(LastOpened.getFolder(ID));
		//					//dialog.setFileName(LastOpened.getFile(ID));
		//				} else {
		//					dialog.setFilterPath(txmhome);
		//				}
		//
		//				File outfile;
		//
		//				if (dialog.open() != null) {
		//					String filename =  dialog.getFileName(); //$NON-NLS-1$
		//					if (!filename.endsWith(device.getExt())) 
		//						filename = filename+device.getExt();
		//					outfile = new File(dialog.getFilterPath(), filename);
		//					LastOpened.set(ID, outfile);
		//				} else
		//					return null;
		//
		//				try {
		//					JobsTimer.start();
		//					StatusLine.setMessage(""); //$NON-NLS-1$
		//					FileCopy.copy(svgSourceFile, outfile);
		//					if (outfile.exists()) {
		//						System.out.println(NLS.bind(Messages.ExportSVG_1, outfile, device.getName())); 
		//					} else {
		//						System.out.println(NLS.bind(Messages.ExportSVG_3 + outfile, device.getExt()));
		//					}
		//
		//				} catch (IOException e) {
		//					System.err.println("Export failed: " + svgSourceFile //$NON-NLS-1$
		//							+ "" + outfile + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$ 
		//				}
		//			}
		//		} catch(Exception e) {
		//			System.out.println(e.getLocalizedMessage());
		//			org.txm.utils.logger.Log.printStackTrace(e);
		//		}
		return null;
	}

	/**
	 * Export.
	 *
	 * @param infile the infile
	 */
	public static void export(File infile) {
		// Shell shell =
		// HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		String txmhome = Toolbox.getTxmHomePath();
		FileDialog dialog = new FileDialog(new Shell(), SWT.SAVE);
		if (LastOpened.getFile(ID) != null) {
			dialog.setFilterPath(LastOpened.getFolder(ID));
			dialog.setFileName(LastOpened.getFile(ID));
		}
		else {
			dialog.setFilterPath(txmhome);
		}

		File outfile;

		if (dialog.open() != null) {
			outfile = new File(dialog.getFilterPath(), dialog.getFileName());
			LastOpened.set(ID, outfile);
		}
		else
			return;

		try {
			FileCopy.copy(infile, outfile);
		}
		catch (IOException e) {
			System.out.println("Error: " + infile //$NON-NLS-1$
					+ "" + outfile + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$ 
		}

		if (outfile.exists()) {
			System.out.println(NLS.bind(TXMUIMessages.p1FileSavedInP0, outfile, ""));  //$NON-NLS-1$
		}
		else {
			System.out.println(NLS.bind(TXMUIMessages.failedToExportFileP0WithP1, outfile)); //$NON-NLS-1$
		}
	}

}
