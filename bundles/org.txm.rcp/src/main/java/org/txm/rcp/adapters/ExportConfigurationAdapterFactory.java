// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.rcp.adapters;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.txm.rcp.Application;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.handlers.export.ExportConfiguration;


/**
 * A factory for creating Adapter objects.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class ExportConfigurationAdapterFactory extends TXMResultAdapterFactory {

	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (adapterType == IWorkbenchAdapter.class && adaptableObject instanceof ExportConfiguration) {
			return new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, IImageKeys.CHARTS_EXPORT_DATA);
				}
			};
		}
		return null;
	}
}
