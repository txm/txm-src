// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.preferences;

import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.rcp.swt.widget.preferences.ComboFieldEditor;
import org.txm.rcp.swt.widget.preferences.StringFieldEditor;

/**
 * The Class ExportPreferencePage.
 */
public class CorpusImportPreferencePage extends TXMPreferencePage {

	/** The encoding. */
	private ComboFieldEditor encoding;

	/** The colseparators. */
	private ComboFieldEditor colseparators;

	/** The txtseparators. */
	private ComboFieldEditor txtseparators;

	private BooleanFieldEditor askdefaultlang;

	private StringFieldEditor defaultlang;

	private StringFieldEditor empty_code;

	private BooleanFieldEditor update_editions;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 */
	@Override
	protected void createFieldEditors() {

		Set<String> encodings = java.nio.charset.Charset.availableCharsets().keySet();
		String[][] values = new String[encodings.size()][2];
		int i = 0;
		for (String s : encodings) {
			values[i][0] = values[i][1] = s;
			i++;
		}

		//		if (TBXPreferences.getInstance().getString(TBXPreferences.METADATA_ENCODING) == null)
		//			TBXPreferences.getInstance().put(TBXPreferences.METADATA_ENCODING, "UTF-8"); //$NON-NLS-1$
		//		if (TBXPreferences.getInstance().getString(TBXPreferences.METADATA_COLSEPARATOR) == null)
		//			TBXPreferences.getInstance().put(TBXPreferences.METADATA_COLSEPARATOR, ","); //$NON-NLS-1$
		//		if (TBXPreferences.getInstance().get(METADATA_TXTSEPARATOR) == null) {
		//			System.out.println("SET DEFAULT VALUE FOR METADATA_TEXTSEPARATOR");
		//			TBXPreferences.getInstance().put(METADATA_TXTSEPARATOR, "\""); //$NON-NLS-1$
		//		}

		Group csvMetadataGroup = new Group(getFieldEditorParent(), SWT.NONE);
		csvMetadataGroup.setText(TXMUIMessages.metadatacsvFileFormat);
		GridData gridData2 = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData2.horizontalSpan = 3;
		gridData2.verticalIndent = 10;
		csvMetadataGroup.setLayoutData(gridData2);
		//		RowLayout rlayout = new RowLayout(SWT.VERTICAL);
		//		rlayout.marginHeight = 150;
		//		rlayout.marginLeft = 150;
		//		rlayout.marginTop = 150;
		//		rlayout.marginRight = 150;
		//		rlayout.marginBottom = 150;
		GridLayout gridlayout = new GridLayout(3, false);
		gridlayout.marginWidth = 3;
		csvMetadataGroup.setLayout(gridlayout);

		encoding = new ComboFieldEditor(TBXPreferences.METADATA_ENCODING, TXMUIMessages.characterEncoding, values, csvMetadataGroup);
		addField(encoding);

		String[][] cols = { { TXMUIMessages.tabulationMark, "\t" }, { ",", "," }, { ";", ";" } }; //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
		String txts[][] = { { TXMUIMessages.noneMark, "" }, { "\"", "\"" }, { "'", "'" } }; //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
		colseparators = new ComboFieldEditor(TBXPreferences.METADATA_COLSEPARATOR, TXMUIMessages.columnsSeparatedBy, cols, csvMetadataGroup);
		addField(colseparators);

		txtseparators = new ComboFieldEditor(TBXPreferences.METADATA_TXTSEPARATOR, TXMUIMessages.textSeparator, txts, csvMetadataGroup);
		addField(txtseparators);
		
		((GridLayout)csvMetadataGroup.getLayout()).marginWidth = 3;

		Group langGroup = new Group(getFieldEditorParent(), SWT.NONE);
		langGroup.setText(TXMUIMessages.clipboardImport);
		gridData2 = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData2.horizontalSpan = 3;
		gridData2.verticalIndent = 10;
		langGroup.setLayoutData(gridData2);
		
		defaultlang = new StringFieldEditor(TBXPreferences.CLIPBOARD_IMPORT_DEFAULT_LANG, TXMUIMessages.defaultLanguage, langGroup);
		defaultlang.setEmptyStringAllowed(false);

		askdefaultlang = new BooleanFieldEditor(TBXPreferences.CLIPBOARD_IMPORT_ASK_PARAMETERS, TXMUIMessages.AskTheParametersWhenImportingFromAClipboard, langGroup);

		Label l = new Label(langGroup, SWT.NONE);
		l.setText(TXMUIMessages.noteColonUseValueToGuess);
		GridData ldata = new GridData(SWT.FILL, SWT.FILL, true, false);
		ldata.horizontalSpan = 3;
		l.setLayoutData(ldata);

		addField(defaultlang);
		addField(askdefaultlang);
		
		((GridLayout)langGroup.getLayout()).marginWidth = 3;

		empty_code = new StringFieldEditor(TBXPreferences.EMPTY_PROPERTY_VALUE_CODE, TXMUIMessages.EmptyPropertyValuecode, getFieldEditorParent());
		addField(empty_code);
		
		
		
		Group updateGroup = new Group(getFieldEditorParent(), SWT.NONE);
		updateGroup.setText(TXMUIMessages.UpdateCorpus);
		gridData2 = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData2.horizontalSpan = 3;
		gridData2.verticalIndent = 10;
		updateGroup.setLayoutData(gridData2);
		
		update_editions = new BooleanFieldEditor(TBXPreferences.UPDATEEDITIONS, TXMUIMessages.UpdateEditionsWhenUpdatingACorpus, updateGroup);
		addField(update_editions);
		((GridLayout)updateGroup.getLayout()).marginWidth = 3;

	}

	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(TBXPreferences.getInstance().getPreferencesNodeQualifier()));

	}
}
