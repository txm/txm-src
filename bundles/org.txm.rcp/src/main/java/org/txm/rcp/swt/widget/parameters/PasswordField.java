package org.txm.rcp.swt.widget.parameters;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.kohsuke.args4j.NamedOptionDef;

public class PasswordField extends LabelField {

	Text dt;

	public PasswordField(Composite parent, int style, NamedOptionDef parameter) {
		super(parent, style, parameter);

		dt = new Text(this, SWT.PASSWORD | SWT.BORDER);
		dt.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));
		resetToDefault();
	}

	public String getWidgetValue() {
		return dt.getText();
	}

	public void setDefault(Object text) {
		dt.setText("" + text); //$NON-NLS-1$
	}

	@Override
	public void resetToDefault() {
		dt.setText(getWidgetDefault());
	}
}
