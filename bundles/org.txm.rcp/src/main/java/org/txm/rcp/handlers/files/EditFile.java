// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.rcp.handlers.files;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.txm.Toolbox;
import org.txm.rcp.editors.TxtEditor;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.utils.BundleUtils;
import org.txm.utils.logger.Log;

/**
 * Open a file in the TxtEditor
 * 
 * @author mdecorde.
 */
public class EditFile extends AbstractHandler {

	public static final String ID = EditFile.class.getCanonicalName(); //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		String txmhome = Toolbox.getTxmHomePath();

		FileDialog dialog = new FileDialog(shell, SWT.OPEN);
		if (LastOpened.getFile(ID) != null) {
			dialog.setFilterPath(LastOpened.getFolder(ID));
			dialog.setFileName(LastOpened.getFile(ID));
		}
		else {
			dialog.setFilterPath(txmhome);
		}
		if (dialog.open() == null) return null;
		String filepath = dialog.getFilterPath() + "/" + dialog.getFileName(); //$NON-NLS-1$
		LastOpened.set(ID, dialog.getFilterPath(), dialog.getFileName());
		openfile(filepath);

		return null;
	}

	/**
	 * Open a file in TXM TxtEditor.
	 *
	 * @param file the filepath
	 */
	static public boolean openfile(File file) {
		if (file.isDirectory())
			return false;

		try {
			IFileStore fileOnLocalDisk = EFS.getLocalFileSystem().getStore(file.toURI());

			FileStoreEditorInput editorInput = new FileStoreEditorInput(fileOnLocalDisk);
			IWorkbenchWindow window = PlatformUI.getWorkbench().getWorkbenchWindows()[0];
			IWorkbenchPage page = window.getActivePage();

			String ID = null;
			if (file.getName().toLowerCase().endsWith(".xml") &&  //$NON-NLS-1$
					BundleUtils.getBundle("org.txm.xmleditor.rcp") != null) {  //$NON-NLS-1$
				ID = "org.eclipse.wst.xml.ui.internal.tabletree.XMLMultiPageEditorPart"; //$NON-NLS-1$
			}
			else {
				ID = TxtEditor.ID;
				page.openEditor(editorInput, ID, true);
			}

			return true;
		}
		catch (Exception e) {
			Log.printStackTrace(e);
			return false;
		}
	}

	/**
	 * Openfile.
	 *
	 * @param filepath the filepath
	 */
	static public void openfile(String filepath) {
		openfile(new File(filepath));
	}
}
