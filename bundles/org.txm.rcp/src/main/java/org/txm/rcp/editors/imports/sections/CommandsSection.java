package org.txm.rcp.editors.imports.sections;

import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.objects.CorpusCommandPreferences;
import org.txm.objects.Project;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.TXMFontRegistry;
import org.txm.searchengine.cqp.ReferencePattern;

/**
 * Regroup import preferences concerning TXM commands
 * TODO: split the section for each command
 * 
 * @author mdecorde
 *
 */
public class CommandsSection extends ImportEditorSection {

	private static final int SECTION_SIZE = 1;

	private Text structLimitsText;

	private Text referencePatternText;

	private Text referencePatternProperties;

	public CommandsSection(ImportFormEditor editor, FormToolkit toolkit2, ScrolledForm form2, Composite parent, int style) {

		super(editor, toolkit2, form2, parent, style, "commands"); //$NON-NLS-1$

		this.section.setText(TXMUIMessages.commands);
		TableWrapLayout layout = new TableWrapLayout();
		layout.makeColumnsEqualWidth = true;
		this.section.setLayout(layout);
		this.section.setLayoutData(getSectionGridData(SECTION_SIZE));
		this.section.setEnabled(false);
		this.section.addExpansionListener(new ExpansionAdapter() {

			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.layout(true);
			}
		});

		Composite sectionClient = toolkit.createComposite(this.section);
		TableWrapLayout slayout = new TableWrapLayout();
		slayout.makeColumnsEqualWidth = false;
		slayout.numColumns = 2;
		sectionClient.setLayout(slayout);
		this.section.setClient(sectionClient);

		Label sep = toolkit.createLabel(sectionClient, TXMUIMessages.concordances);
		sep.setToolTipText(TXMUIMessages.concordancesSubSection);
		sep.setLayoutData(getSectionGridData(2));
		FontData fdata = sep.getFont().getFontData()[0];
		sep.setFont(TXMFontRegistry.getFont(sep.getDisplay(), fdata.getName(), fdata.getHeight(), SWT.BOLD));

		Label tmpLabel = toolkit.createLabel(sectionClient, TXMUIMessages.concordanceContextStructureLimits);
		tmpLabel.setToolTipText(TXMUIMessages.concordanceContextStructureLimits);
		tmpLabel.setLayoutData(getLabelGridData());

		structLimitsText = toolkit.createText(sectionClient, "text", SWT.BORDER); //$NON-NLS-1$
		structLimitsText.setToolTipText(TXMUIMessages.concordanceContextStructureLimits);
		TableWrapData gdata = getTextGridData();
		structLimitsText.setLayoutData(gdata);

		Label tmp2Label = toolkit.createLabel(sectionClient, TXMUIMessages.defaultReferences);
		tmp2Label.setToolTipText(TXMUIMessages.theDefaultReferencesDisplayedInTheEtc);
		tmp2Label.setLayoutData(getSectionGridData(2));

		Label tmp3Label = toolkit.createLabel(sectionClient, "\t" + TXMUIMessages.TabPattern);  //$NON-NLS-1$
		tmp3Label.setToolTipText(TXMUIMessages.YouNeedToSetAsManyPercentSPrintfMarksAsSelectedProperties);
		tmp3Label.setLayoutData(getLabelGridData());

		referencePatternText = toolkit.createText(sectionClient, "", SWT.BORDER); //$NON-NLS-1$
		referencePatternText.setToolTipText(TXMUIMessages.YouNeedToSetAsManyPercentSPrintfMarksAsSelectedProperties);
		TableWrapData gdata2 = getTextGridData();
		referencePatternText.setLayoutData(gdata2);

		Label tmp4Label = toolkit.createLabel(sectionClient, "	"+TXMCoreMessages.common_properties); //$NON-NLS-1$
		tmp4Label.setToolTipText(TXMUIMessages.PropertiesOfTheCompoundFormForStructurePropertiesAndOfTheSimpleFormForWordProperties);
		tmp4Label.setLayoutData(getLabelGridData());

		referencePatternProperties = toolkit.createText(sectionClient, "", SWT.BORDER); //$NON-NLS-1$
		referencePatternProperties.setToolTipText(TXMUIMessages.PropertiesOfTheCompoundFormForStructurePropertiesAndOfTheSimpleFormForWordProperties);
		TableWrapData gdata3 = getTextGridData();
		referencePatternProperties.setLayoutData(gdata3);
	}

	@Override
	public void updateFields(Project project) {
		CorpusCommandPreferences concPrefs = project.getCommandPreferences("concordance"); //$NON-NLS-1$

		if (concPrefs.get("context_limits") != null) { //$NON-NLS-1$
			structLimitsText.setText(concPrefs.get("context_limits")); //$NON-NLS-1$
		}
		else {
			structLimitsText.setText("text"); // TODO use the concordance preferences instead of hardcoding 'text' //$NON-NLS-1$
		}

		if (concPrefs.get("view_reference_pattern") != null) { //$NON-NLS-1$
			String gsonString = concPrefs.get("view_reference_pattern"); //$NON-NLS-1$
			if (ReferencePattern.isGsonString(gsonString)) {
				referencePatternText.setText(ReferencePattern.fromGSonToPattern(gsonString));
				referencePatternProperties.setText(StringUtils.join(ReferencePattern.fromGSonToPropertyNames(gsonString), ",")); //$NON-NLS-1$
			}
			else if (gsonString.contains("{0}")) { //$NON-NLS-1$
				System.out.println("WARNING: experimental reference pattern format found: " + gsonString); //$NON-NLS-1$
			}
			else {
				referencePatternText.setText(""); //$NON-NLS-1$
				referencePatternProperties.setText(StringUtils.join(gsonString.split("\t"), ",")); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		else {
			String import_name = editor.getImportName();
			if ("transcriber".equals(import_name)) { //$NON-NLS-1$
				referencePatternText.setText("%s %s: %s"); //$NON-NLS-1$
				referencePatternProperties.setText("text_id,sp_who,sp_time"); //$NON-NLS-1$
			}
			else if ("txt".equals(import_name)) { //$NON-NLS-1$
				referencePatternText.setText("%s, %s"); //$NON-NLS-1$
				referencePatternProperties.setText("text_id,lbn"); //$NON-NLS-1$
			}
			else {
				referencePatternText.setText(""); //$NON-NLS-1$
				referencePatternProperties.setText(""); //$NON-NLS-1$
			}
		}
	}

	@Override
	public boolean saveFields(Project project) {
		if (this.section != null && !section.isDisposed()) {
			CorpusCommandPreferences concPrefs = project.getCommandPreferences("concordance"); //$NON-NLS-1$
			concPrefs.set("context_limits_type", "list"); //$NON-NLS-1$ //$NON-NLS-2$ $NON-NLS-2$
			concPrefs.set("context_limits", structLimitsText.getText()); //$NON-NLS-1$

			concPrefs.set("view_reference_pattern", //$NON-NLS-1$
					ReferencePattern.referenceToString(
							Arrays.asList(referencePatternProperties.getText().trim().split(",")), //$NON-NLS-1$
							referencePatternText.getText()));
		}

		return true;
	}

	@Override
	public boolean checkFields() {
		return true;
	}

	@Override
	public int getSectionSize() {
		return SECTION_SIZE;
	}
}
