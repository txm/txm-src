/**
 * 
 */
package org.txm.rcp.swt.widget;

import java.util.LinkedHashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.swt.GLComposite;

/**
 * 
 * @author mdecorde
 *
 */
//FIXME: SJ, 2025-01-29: not used.
public class CustomThresholdsGroup extends Group {

	public static final String F = "F"; //$NON-NLS-1$

	public static final String V = "V"; //$NON-NLS-1$

	public static final String S = "S"; //$NON-NLS-1$

	TXMEditor<? extends TXMResult> editor;

	LinkedHashMap<String, ThresholdField> fields;

	Configuration[] configurations;

	/**
	 * 
	 * @param parent
	 * @param editor
	 */
	public CustomThresholdsGroup(Composite parent, TXMEditor<? extends TXMResult> editor, Configuration... configurations) {
		super(parent, SWT.NONE);

		this.setText(TXMCoreMessages.common_thresholds);

		this.setLayout(new RowLayout());
		this.editor = editor;
		this.configurations = configurations;

		this.fields = new LinkedHashMap<>();
		for (int i = 0; i < configurations.length; i++) {
			Configuration c = configurations[i];

			//if (i > 0) new Label(this, SWT.NONE).setText("|");

			ThresholdField tf = new ThresholdField(this, c);
			fields.put(c.name, tf);


		}
	}

	@Override
	protected void checkSubclass() { // mandatory to avoid subclass exception
	}

	public Spinner getMinField(String key) {
		ThresholdField f = this.fields.get(key);
		if (f != null) {
			return f.getMinField();
		}
		return null;
	}

	public Spinner getMaxField(String key) {
		ThresholdField f = this.fields.get(key);
		if (f != null) {
			return f.getMaxField();
		}
		return null;
	}

	/**
	 * @return the fMinSpinner
	 */
	public Spinner getFMinSpinner() {
		return fields.get(F).getMinField();
	}

	/**
	 * @return the fMaxSpinner
	 */
	public Spinner getFMaxSpinner() {
		return fields.get(F).getMaxField();
	}

	/**
	 * @return the vMaxSpinner
	 */
	public Spinner getVMinSpinner() {
		return fields.get(V).getMinField();
	}

	/**
	 * @return the vMaxSpinner
	 */
	public Spinner getVMaxSpinner() {
		return fields.get(V).getMaxField();
	}

	/**
	 * @return the vMaxSpinner
	 */
	public Spinner getSMinSpinner() {
		return fields.get(S).getMinField();
	}

	/**
	 * @return the vMaxSpinner
	 */
	public Spinner getSMaxSpinner() {
		return fields.get(S).getMaxField();
	}

	public static class Configuration {

		public String name;

		public int min;

		public int max;

		public int digits;

		public boolean showMin;

		public boolean showMax;

		public Configuration(String name, int min, int max, int digits, boolean showMin, boolean showMax) {
			this.name = name;
			this.min = min;
			this.max = max;
			this.digits = digits;
			this.showMin = showMin;
			this.showMax = showMax;
		}
	}

	public static class ThresholdField extends GLComposite {

		Spinner minField;

		Spinner maxField;

		public ThresholdField(Composite parent, Configuration c) {

			super(parent, SWT.NONE, c.name);
			this.getLayout().numColumns = 3;
			this.getLayout().horizontalSpacing = 2;

			if (c.showMin && !c.showMax) {
				Label l = new Label(this, SWT.NONE);
				l.setLayoutData(new GridData());
				l.setText(c.name + " ≤ "); //$NON-NLS-1$
			}

			if (c.showMin) {
				minField = new Spinner(this, SWT.BORDER);
				minField.setLayoutData(new GridData());
				minField.setMinimum(c.min);
				minField.setMaximum(c.max);
				minField.setDigits(c.digits);
				minField.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						if (minField != null) maxField.setMinimum(minField.getSelection());
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});
			}

			if (c.showMin && c.showMax) {
				Label l = new Label(this, SWT.NONE);
				l.setLayoutData(new GridData());
				l.setText(" ≤ " + c.name + " ≤ "); //$NON-NLS-1$ //$NON-NLS-2$
			}

			if (!c.showMin && c.showMax) {
				Label l = new Label(this, SWT.NONE);
				l.setLayoutData(new GridData());
				l.setText(c.name + " ≤ "); //$NON-NLS-1$
			}

			if (c.showMax) {
				maxField = new Spinner(this, SWT.BORDER);
				maxField.setLayoutData(new GridData());
				maxField.setMinimum(c.min);
				maxField.setMaximum(c.max);
				maxField.setDigits(c.digits);
				maxField.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						if (maxField != null) minField.setMaximum(maxField.getSelection());
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});
			}
		}

		public Spinner getMinField() {

			return minField;
		}

		public Spinner getMaxField() {

			return maxField;
		}
	}
}
