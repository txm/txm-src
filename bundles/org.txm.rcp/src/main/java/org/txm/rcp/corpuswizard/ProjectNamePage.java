package org.txm.rcp.corpuswizard;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.RCPPreferences;

public class ProjectNamePage extends WizardPage {

	// private Text corpusName;
	private Text sourceDirectory;
	//    private Text descriptionText;

	private Composite container;

	public ProjectNamePage() {
		super(TXMUIMessages.SelectTheSourceDirectory);
		setTitle(TXMUIMessages.Sources);
		setDescription(TXMUIMessages.TheSourceDirectoryContainsTheSourcesFilesReadByTheImportModule);
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 3;

		GridData gd = new GridData(GridData.FILL_HORIZONTAL);

		Label label2 = new Label(container, SWT.NONE);
		label2.setText(TXMUIMessages.SourceDirectory);

		sourceDirectory = new Text(container, SWT.BORDER | SWT.SINGLE);
		final String lastSourcesLocation = RCPPreferences.getInstance().getString("lastSourcesLocation"); //$NON-NLS-1$
		sourceDirectory.setText(lastSourcesLocation);
		sourceDirectory.setToolTipText(TXMUIMessages.TheCorpusSourceDirectory);
		sourceDirectory.setLayoutData(gd);
		sourceDirectory.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				setPageComplete(isCompleted());
			}
		});

		Button button = new Button(container, SWT.PUSH);
		button.setText("..."); //$NON-NLS-1$
		button.setToolTipText(TXMUIMessages.ClickToSelectTheCorpusSourceDirectory);
		button.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				DirectoryDialog dialog = new DirectoryDialog(e.display.getActiveShell());
				dialog.setFilterPath(getSourcePath());
				dialog.setText(TXMUIMessages.SourceDirectory);
				if (getSourcePath().length() > 0) {
					dialog.setFilterPath(getSourcePath());
				}
				String path = dialog.open();
				if (path != null) {
					sourceDirectory.setText(path);
					RCPPreferences.getInstance().put("lastSourcesLocation", path); //$NON-NLS-1$
					RCPPreferences.getInstance().flush();
					setPageComplete(isCompleted());
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		//        Label label1 = new Label(container, SWT.NONE);
		//        label1.setText("Name");
		//
		//        corpusName = new Text(container, SWT.BORDER | SWT.SINGLE);
		//        corpusName.setText("");
		//        corpusName.setToolTipText("The corpus name");
		//        corpusName.addKeyListener(new KeyListener() {
		//			@Override
		//			public void keyReleased(KeyEvent e) {
		//				setPageComplete(isCompleted());
		//			}
		//			
		//			@Override
		//			public void keyPressed(KeyEvent e) { }
		//		});
		//        
		//        corpusName.setLayoutData(gd);
		new Label(container, SWT.NONE);

		//        label1 = new Label(container, SWT.NONE);
		//        label1.setText("Description");
		//        
		//        descriptionText = new Text(container, SWT.BORDER | SWT.MULTI);
		//        descriptionText.setText("\n\n\n\n\n");
		//        descriptionText.setText("");
		//        descriptionText.setToolTipText("The corpus description in HTML format");
		//        gd = new GridData(GridData.FILL_HORIZONTAL);
		//        gd.grabExcessVerticalSpace = true;
		//        descriptionText.setLayoutData(gd);

		// required to avoid an error in the system
		setControl(container);
		setPageComplete(false);

		setPageComplete(isCompleted());
	}

	protected boolean isCompleted() {
		//return getCorpusName().length() > 0 && getSourcePath().length() > 0 && !ResourcesPlugin.getWorkspace().getRoot().getProject(getCorpusName()).exists();
		return getSourcePath().length() > 0;
	}

	//	public String getCorpusName() {
	//        return corpusName.getText();
	//    }

	public String getSourcePath() {
		return sourceDirectory.getText().trim();
	}
}
