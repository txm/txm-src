// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.editors.input;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.txm.Toolbox;
import org.txm.rcp.editors.SVGGraphicable;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.utils.logger.Log;

/**
 * Defines a tab displaying a graphics in a multi-tab editor.
 * 
 * @author sloiseau
 * 
 */
// FIXME: SJ: useless
@Deprecated
public abstract class AbstractSVGGraphicable extends AbstractEditorInput implements SVGGraphicable {

	/** The path. */
	protected IPath path;

	/**
	 * Creates and writes the desired SVG Graphic in the given file object.
	 *
	 * @param file the file
	 */
	abstract public void doSVGGraphic(File file);

	@Override
	public IPath getIPath() {
		if (this.path == null) {
			File tmp = null;
			try {
				File resultDir = new File(Toolbox.getTxmHomePath(), "results"); //$NON-NLS-1$
				resultDir.mkdirs();
				tmp = File.createTempFile("txm", ".svg", resultDir); //$NON-NLS-1$ //$NON-NLS-2$
				// System.out.println(tmp.getAbsolutePath());
				doSVGGraphic(tmp);
				this.path = Path.fromOSString(tmp.getCanonicalPath());

			}
			catch (IOException e) {
				Log.severe(TXMUIMessages.cannotDrawAGraphicWithTheInformationProvided);
				Log.printStackTrace(e);
			}
		}
		return this.path;
	}

}
