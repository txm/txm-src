// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views.debug;

import java.util.Collection;
import java.util.LinkedHashMap;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.txm.Toolbox;
import org.txm.core.engines.Engine;
import org.txm.core.engines.EngineType;
import org.txm.core.engines.EnginesManager;

/**
 * Alternative display of the TXMEditor form fields
 * 
 * @author mdecorde.
 */
public class EnginesDebugView extends ViewPart {

	/** The ID. */
	public static String ID = EnginesDebugView.class.getName(); //$NON-NLS-1$

	private TreeViewer viewer;

	/**
	 * Instantiates a new queries view.
	 */
	public EnginesDebugView() {
	}

	@Override
	public void dispose() {
	}

	/**
	 * Refresh.
	 */
	public static void refresh() {

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {

		parent.setLayout(new GridLayout(1, true));

		viewer = new TreeViewer(parent);
		viewer.getTree().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		viewer.setLabelProvider(new LabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof Engine) {
					return ((Engine) element).getName() + (((Engine) element).isRunning() ? " OK" : " KO"); //$NON-NLS-1$ //$NON-NLS-2$
				}
				else if (element instanceof EnginesManager) {
					return ((EnginesManager<?>) element).getEnginesType().name();
				}
				else {
					return "error"; //$NON-NLS-1$
				}
			}
		});

		viewer.setContentProvider(new ITreeContentProvider() {

			@Override
			public Object[] getElements(Object inputElement) {
				LinkedHashMap<EngineType, EnginesManager<?>> i = (LinkedHashMap<EngineType, EnginesManager<?>>) inputElement;
				return i.values().toArray();
			}

			@Override
			public Object[] getChildren(Object parentElement) {
				if (parentElement instanceof Engine) {
					return new Object[] {};
				}
				else if (parentElement instanceof EnginesManager) {
					Collection<?> engines = ((EnginesManager<?>) parentElement).getEngines().values();
					return engines.toArray(new Object[engines.size()]);
				}
				else {
					return new Object[] {};
				}
			}

			@Override
			public Object getParent(Object element) {
				if (element instanceof Engine) {
					return null;
				}
				else {
					return null;
				}
			}

			@Override
			public boolean hasChildren(Object element) {
				if (element instanceof Engine) {
					return false;
				}
				else if (element instanceof EnginesManager<?> em) {
					return em.getEngines().size() > 0;
				}
				else {
					return false;
				}
			}
		});

		viewer.getTree().addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.F5) {
					viewer.refresh();
				}
			}
		});

		viewer.getTree().addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseDown(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				ISelection isel = viewer.getSelection();
				if (isel instanceof StructuredSelection) {
					StructuredSelection sel = (StructuredSelection) isel;
					if (!sel.isEmpty()) {
						for (Object o : sel.toList()) {
							if (o instanceof Engine) {
								System.out.println(((Engine) o).getDetails());
							}
						}
					}
				}
			}
		});

		LinkedHashMap<EngineType, EnginesManager<?>> i = Toolbox.getEngineManagers();
		viewer.setInput(i);
	}

	@Override
	public void setFocus() {
		viewer.getTree().setFocus();
	}
}
