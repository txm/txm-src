package org.txm.rcp.swt.widget.parameters;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.kohsuke.args4j.NamedOptionDef;
import org.txm.rcp.swt.widget.AssistedQueryWidget;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.cqp.CQPSearchEngine;

public class QueryField extends ParameterField {

	AssistedQueryWidget w;

	public QueryField(Composite parent, int style, NamedOptionDef parameter) {
		super(parent, style, parameter);

		this.setLayout(new GridLayout(2, false));
		this.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));

		Label l = new Label(this, SWT.NONE);
		l.setAlignment(SWT.LEFT);
		l.setText(getWidgetLabel());
		l.setToolTipText(getWidgetUsage());
		GridData gd = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd.widthHint = 100;
		l.setLayoutData(gd);

		w = new AssistedQueryWidget(this, SWT.BORDER, null, CQPSearchEngine.getEngine());
		w.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));
		resetToDefault();
	}

	public IQuery getWidgetValue() {
		return w.getQuery();
	}

	@Override
	public void setDefault(Object value) {
		w.setText("" + value); //$NON-NLS-1$
	}

	@Override
	public void resetToDefault() {
		if (getWidgetDefault().length() > 0) {
			try {
				w.setText(getWidgetDefault());
			}
			catch (Exception e) {
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		else {
			w.setText(""); //$NON-NLS-1$
		}
	}
}
