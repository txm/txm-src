package org.txm.rcp.swt.widget;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.txm.rcp.swt.dialog.ReferencePatternSelectionDialog;
import org.txm.searchengine.cqp.corpus.Property;

public class ReferencePatternSelector extends PropertiesSelector<Property> {

	Text patternText;

	Label patternLabel;

	private String pattern;

	public ReferencePatternSelector(Composite parent, int style) {
		super(parent, style);

		this.setLayout(new GridLayout(4, false));

		// "properties"
		patternLabel = new Label(this, SWT.NONE);
		patternLabel.setText(""); //$NON-NLS-1$
		patternLabel.setToolTipText(""); //$NON-NLS-1$
		patternLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true));
	}

	/**
	 * Show editor dialog.
	 */
	@Override
	public void showEditorDialog() {
		if (availableProperties == null) {
			return;
		}

		ArrayList<Property> previousAvailable = new ArrayList<>(availableProperties);
		ArrayList<Property> previousSelectedProperties = new ArrayList<>(selectedProperties);

		ReferencePatternSelectionDialog dialog = new ReferencePatternSelectionDialog(this.getShell(), availableProperties, selectedProperties, pattern, propLabel);
		if (dialog.open() == ReferencePatternSelectionDialog.OK) {
			this.pattern = dialog.getPattern();
			this.selectedProperties.clear();
			this.selectedProperties.addAll(dialog.getSelectedProperties());
		}
		else {
			availableProperties.clear();
			availableProperties.addAll(previousAvailable);
			selectedProperties.clear();
			selectedProperties.addAll(previousSelectedProperties);
		}
	}

	@Override
	public void refresh() {
		super.refresh();
		refreshLabel();

	}

	protected void refreshLabel() {
		if (pattern != null && pattern.length() > 0) {
			this.patternLabel.setText("(" + pattern + ")"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		else {
			this.patternLabel.setText(""); //$NON-NLS-1$
		}
	}

	@Override
	public void addSelectionListener(SelectionListener listener) {
		openEditDialogButton.addSelectionListener(listener);
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;

		refreshLabel();

		this.layout(true);
	}
}
