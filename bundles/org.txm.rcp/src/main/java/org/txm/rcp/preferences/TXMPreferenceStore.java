package org.txm.rcp.preferences;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

/**
 * Convenience class to get a scoped preference instance store.
 * 
 * @author sjacquot
 *
 */
public class TXMPreferenceStore extends ScopedPreferenceStore {

	/**
	 * Node qualifier.
	 */
	protected String nodeQualifier;

	/**
	 * 
	 * @param qualifier
	 */
	public TXMPreferenceStore(String qualifier) {
		super(InstanceScope.INSTANCE, qualifier);
		this.nodeQualifier = qualifier;
	}

	/**
	 * @return the nodeQualifier
	 */
	public String getNodeQualifier() {
		return nodeQualifier;
	}

}
