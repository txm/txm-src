package org.txm.rcp.commands.workspace;

import java.io.File;
import java.io.FileFilter;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.preferences.TBXPreferences;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Project;
import org.txm.rcp.commands.CloseEditorsUsing;
import org.txm.rcp.commands.OpenImportForm;
import org.txm.rcp.commands.RestartTXM;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.UpdateCorpusDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.ExecTimer;
import org.txm.utils.logger.Log;

public class UpdateCorpus extends AbstractHandler {

	public static final String ID = UpdateCorpus.class.getCanonicalName();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection sel = HandlerUtil.getCurrentSelection(event);
		if (!(sel instanceof IStructuredSelection)) return null;
		IStructuredSelection selection = (IStructuredSelection) sel;

		Object s = selection.getFirstElement();
		if (!(s instanceof MainCorpus)) {
			return null;
		}
		MainCorpus corpus = (MainCorpus) s;

		UpdateCorpusDialog dialog = new UpdateCorpusDialog(HandlerUtil.getActiveShell(event));
		dialog.setDoUpdateEdition(TBXPreferences.getInstance().getBoolean(TBXPreferences.UPDATEEDITIONS));
		dialog.setDoForceUpdate("true".equals(event.getParameter("org.txm.rcp.commands.workspace.UpdateCorpus.force"))); //$NON-NLS-1$ //$NON-NLS-2$

		if (dialog.open() != Window.OK) {
			return null;
		}

		if (dialog.getDoOpenCorpusParameters()) {

			corpus.getProject().setDoUpdate(true, dialog.getDoUpdateEdition());
			if (dialog.getDoForceUpdate()) {
				corpus.getProject().setDirty();
			}
			OpenImportForm.open(null, true);
		}
		else {

			update(corpus, dialog.getDoForceUpdate(), dialog.getDoUpdateEdition());
		}

		return corpus;
	}

	public static JobHandler update(final Project project) {
		return update(project, false, TBXPreferences.getInstance().getBoolean(TBXPreferences.UPDATEEDITIONS));
	}

	public static JobHandler update(final MainCorpus corpus) {
		return update(corpus.getProject(), false, TBXPreferences.getInstance().getBoolean(TBXPreferences.UPDATEEDITIONS));
	}

	public static JobHandler update(final MainCorpus corpus, boolean updateEdition) {
		return update(corpus.getProject(), false, updateEdition);
	}

	public static JobHandler update(final MainCorpus corpus, boolean forceUpdate, boolean updateEdition) {
		return update(corpus.getProject(), forceUpdate, updateEdition);
	}

	public static JobHandler update(final Project project, boolean forceUpdate, boolean updateEdition) {

		if (project == null) return null;

		// String iname = project.getImportModuleName();
		// if (!iname.matches("xtz|txt|hyperbase|discours|cnr|alceste|xml|xmltxm")) {
		// System.out.println("Can't update a CQP corpus not imported with one of the XTZ, TXT, XML, XML-TXM, CNRS, Alceste, Hyperbase import modules.");
		// System.out.println(NLS.bind("The corpus was imported with the \"{0}\" import module.", iname));
		// return null;
		// }

		Display d = Display.getCurrent();
		if (d == null) {
			d = Display.getDefault();
		}
		if (d != null) {
			d.syncExec(new Runnable() {

				@Override
				public void run() {
					for (CorpusBuild c : project.getCorpora()) {
						CloseEditorsUsing.result(c);
					}
				}
			});
		}

		File txmDir = new File(project.getProjectDirectory(), "txm"); //$NON-NLS-1$
		if (!txmDir.exists()) {
			Log.warning(NLS.bind(TXMUIMessages.ErrorCantUpdateTheCorpusNoTXMDirectoryFoundP0, txmDir.getAbsolutePath()));
			return null;
		}

		File txmCorpusDirectory = new File(txmDir, project.getName());
		if (!txmCorpusDirectory.exists()) {
			Log.warning(NLS.bind(TXMUIMessages.ErrorCantUpdateTheCorpusNoTXTSlashP0DirectoryFoundP1, project.getName(), txmCorpusDirectory.getAbsolutePath()));
			return null;
		}

		if (txmCorpusDirectory.listFiles(new FileFilter() {

			@Override
			public boolean accept(File arg0) {
				return arg0.isFile() && arg0.getName().endsWith(".xml"); //$NON-NLS-1$
			}
		}).length == 0) {
			Log.warning(NLS.bind(TXMUIMessages.ErrorCantUpdateTheCorpusNoXMLFilesFoundInP0, txmCorpusDirectory.getAbsolutePath()));
			return null;
		}

		project.setDirty(forceUpdate);// false=dont propagate dirty
		project.setNeedToBuild();
		project.setDoMultiThread(false); // too soon
		project.setDoUpdate(true, updateEdition);
		String currentModule = project.getImportModuleName();
		if (!(currentModule.equals("xtz") || currentModule.equals("transcriber"))) { //$NON-NLS-1$ //$NON-NLS-2$
			project.setImportModuleName("xtz"); //$NON-NLS-1$
		}

		JobHandler job = new JobHandler(TXMUIMessages.bind(TXMUIMessages.UpdateTheP0Corpus, project)) {

			@Override
			protected IStatus _run(SubMonitor monitor) throws InterruptedException {

				try {
					ExecTimer.start();
					if (project.compute(monitor, true)) { // TODO children should be recomputed later only when the user needs it
						Log.info("Corpus updated in " + ExecTimer.stop() + ".");
						this.syncExec(new Runnable() {

							@Override
							public void run() {
								RestartTXM.reloadViews();
							}
						});
						return Status.OK_STATUS;
					}
					else {
						Log.info("Error: corpus was NOT updated.");
						return Status.CANCEL_STATUS;
					}
				}
				catch (InterruptedException e) {
					Log.info("Error: corpus was NOT updated.");
					throw e;
				}
			}
		};

		job.schedule();

		//// String txmhome = Toolbox.getTxmHomePath();
		//// File scriptDir = new File(txmhome, "scripts/groovy/user/org/txm/scripts/importer/xtz");
		//// File script = new File(scriptDir, "xtzLoader.groovy");
		// System.out.println();
		// JobHandler ret = ExecuteImportScript.executeScript(project);
		//
		return job;
	}
}
