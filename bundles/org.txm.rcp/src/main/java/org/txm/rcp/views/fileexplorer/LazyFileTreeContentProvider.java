package org.txm.rcp.views.fileexplorer;

import java.io.File;

import org.eclipse.jface.viewers.ILazyTreePathContentProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeViewer;

public class LazyFileTreeContentProvider implements ILazyTreePathContentProvider {

	TreeViewer viewer;

	public LazyFileTreeContentProvider(TreeViewer viewer) {
		this.viewer = viewer;
	}

	@Override
	public void updateElement(TreePath parentPath, int index) {
		Object o = parentPath.getFirstSegment();
		if (o instanceof File) {

		}
		else {

		}
	}

	@Override
	public void updateChildCount(TreePath path, int currentChildCount) {
		Object o = path.getFirstSegment();
		if (o instanceof File) {
			File f = (File) o;
			File[] files = f.listFiles();
			if (files != null) {
				viewer.setChildCount(path, files.length);
			}
		}
		else {
			viewer.setChildCount(path, 0);
		}
	}

	@Override
	public void updateHasChildren(TreePath path) {
		Object o = path.getFirstSegment();
		if (o instanceof File) {
			viewer.setHasChildren(path, (((File) o).isDirectory()));
		}
		else {
			viewer.setHasChildren(path, false);
		}
	}

	@Override
	public TreePath[] getParents(Object o) {
		//		if (o instanceof File) {
		//			return new TreePath[0];
		//		} else {
		//			return new TreePath[0];
		//		}
		return null;
	}
}
