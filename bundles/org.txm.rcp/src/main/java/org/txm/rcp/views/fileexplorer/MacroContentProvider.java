// Copyright © 2010-2010 ENS de Lyon, University of Franche-Comté.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views.fileexplorer;

import java.io.File;
import java.io.FileFilter;

import org.txm.utils.io.IOUtils;

/**
 * The Class FileTreeContentProvider.
 */
public class MacroContentProvider extends FileTreeContentProvider {

	public static final String SUFFIX = "Macro.groovy"; //$NON-NLS-1$

	public static final String BETA = "@STATUS=BETA"; //$NON-NLS-1$

	public static final String ALPHA = "@STATUS=ALPHA"; //$NON-NLS-1$

	MacroExplorer macroExplorer = null;

	public MacroContentProvider(MacroExplorer macroExplorer) {
		super();
		this.macroExplorer = macroExplorer;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
	 */
	@Override
	public Object[] getChildren(Object element) {

		Object[] kids = ((File) element).listFiles(new FileFilter() {

			@Override
			public boolean accept(File file) {
				if (file.isHidden()) return false;
				if (file.isDirectory()) return true;

				if (macroExplorer.isShowAll()) return true;
				if (!file.getName().endsWith(SUFFIX)) return false;

				String line = IOUtils.firstLine(file);
				if (line != null) {
					line = line.trim();
					return !(line.contains(BETA) || line.contains(ALPHA));
				}
				return true;
			}
		});

		if (kids != null) {
			java.util.Arrays.sort(kids);
		}

		return kids == null ? new Object[0] : kids;
	}
}
