package org.txm.rcp.handlers.export;

import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.utils.TXMProgressMonitor;

/**
 * Configuration for the "data" export
 * 
 * @author mdecorde
 *
 */
public class ExportResultDataConfiguration extends ExportConfiguration<TXMResult> {

	@Parameter(key = "encoding")
	String pEncoding;

	public ExportResultDataConfiguration(String parametersNodePath) {
		this(parametersNodePath, null);
		// TODO Auto-generated constructor stub
	}

	public ExportResultDataConfiguration(TXMResult parent) {
		this(null, parent);
		// TODO Auto-generated constructor stub
	}

	public ExportResultDataConfiguration(String parametersNodePath, TXMResult parent) {
		super(parametersNodePath, parent);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getDetails() {

		return "file: " + pFile + " encoding: " + pEncoding;
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		return this.getParent().toTxt(pFile, pEncoding);
	}

}
