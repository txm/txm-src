// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.results;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;
import org.txm.rcp.StatusLine;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.RCPPreferences;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.LogMonitor;
import org.txm.utils.logger.Log;

/**
 * Deletes the selected results of Corpora view.
 * 
 * @author mdecorde
 * 
 */
public class DeleteObject extends AbstractHandler {

	private static boolean continueToDelete = true;


	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		final IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		if (selection.isEmpty()) return null;

		if (!askContinueToDelete(selection)) {
			return null;
		}

		JobHandler job = new JobHandler(TXMUIMessages.Deleting, true) {

			@Override
			protected IStatus _run(SubMonitor monitor) {
				delete(selection);
				return Status.OK_STATUS;
			}
		};
		job.schedule();
		try {
			job.join();
		}
		catch (InterruptedException e) {
			e.printStackTrace();
			return Status.CANCEL_STATUS;
		}
		return job.getResultObject();
	}

	/**
	 * Deletes.
	 *
	 * @param sel the selection of objects to delete
	 */
	public static HashSet<Object> delete(ISelection sel) {
		IStructuredSelection selection = (IStructuredSelection) sel;

		if (selection.size() == 0) {
			return new HashSet<>();
		}

		HashSet<Object> objects = new HashSet<Object>(selection.toList());
		HashSet<Object> hidden = new HashSet<>();
		for (Object object : objects) {
			if (object != null && object instanceof TXMResult) {
				TXMResult result = (TXMResult) object;
				TXMResult parent = result.getParent();

				// add all hidden parent results in the branch to the list of objects to delete
				while (parent != null
						&& !parent.isInternalPersistable() // don't delete internally persisted results
						&& !parent.isVisible() // delete if the parent is hidden
						&& parent.getChildren().size() == 1 // AND if there is no sibling
				) {
					hidden.add(parent);
					parent = parent.getParent();
				}
			}
		}
		objects.addAll(hidden);
		return delete(new ArrayList<>(objects));
	}


	/**
	 * Deletes.
	 * 
	 * TODO: select the parent object of the deleted object
	 *
	 * @param objects the objects to delete
	 * @return
	 */
	public static synchronized HashSet<Object> delete(final List<Object> objects) {

		StatusLine.setMessage(NLS.bind(TXMUIMessages.deletingP0, objects));

		final HashSet<Object> deletedObjects = new HashSet<>();

		while (objects.size() > 0) {
			Object object = objects.get(0);
			try {
				List<Object> deleted = delete(object);
				deletedObjects.addAll(deleted);
				if (deleted.size() == 0) {
					StatusLine.setMessage(TXMUIMessages.errorColonDeleteReturnedFalse + " : " + object); //$NON-NLS-1$
					return new HashSet<>();
				}
				// remove results already deleted
				objects.removeAll(deleted);
			}
			catch (Exception e) {
				Log.severe(NLS.bind(TXMUIMessages.errorWhileDeletingP0ColonP1, object, e));
				StatusLine.error(NLS.bind(TXMUIMessages.errorWhileDeletingP0ColonP1, object, e));
				org.txm.utils.logger.Log.printStackTrace(e);
				return new HashSet<>();
			}
		}

		// Refresh UI and close deleted object editors
		if (deletedObjects.size() > 0) {

			Display.getDefault().syncExec(new Runnable() {

				@Override
				public void run() {
					try {

						CorporaView.refresh();

						// close editors
						for (Object obj : deletedObjects) {
							if (obj instanceof TXMResult) {
								SWTEditorsUtils.closeEditors((TXMResult) obj, false);
							}
						}
					}
					catch (Exception e) {
					}
				}
			});
		}

		StatusLine.setMessage(""); //$NON-NLS-1$
		return deletedObjects;
	}

	/**
	 * Deletes the specified object and all its children.
	 * 
	 * Warning: if the object is a MainCorpus then the MainCorpus project is deleted.
	 *
	 * @param object the root result of the branch to delete
	 * @return the list of results that have been deleted
	 */
	public static List<Object> delete(Object object) {
		ArrayList<Object> deleted = new ArrayList<>();

		if (object instanceof TXMResult) {
			TXMResult result = (TXMResult) object;
			List<TXMResult> children = result.getDeepChildren();

			// keep locked result
			if (result.isLocked()) {
				return deleted;
			}

			// delete the root project if the object is a MainCorpus
			if (result instanceof MainCorpus) {
				if (((MainCorpus) object).getProject() != null) {
					result = ((MainCorpus) object).getProject();
				}
			}

			result.delete();

			deleted.add(object);
			deleted.addAll(children);
		}
		else if (object instanceof IProject) {
			try {
				((IProject) object).delete(true, true, new LogMonitor());
				deleted.add(object);
			}
			catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			Log.severe(TXMCoreMessages.bind(TXMUIMessages.CanNotDeleteP0, object));
		}

		return deleted;
	}



	/**
	 * 
	 * @param selection
	 * @return
	 */
	public static synchronized boolean askContinueToDelete(final IStructuredSelection selection) {
		continueToDelete = true;

		boolean showAlert = !RCPPreferences.getInstance().getBoolean(RCPPreferences.DONT_ALERT_USER_BEFORE_DELETE);

		if (showAlert) {
			if (Display.getDefault() == null) { // avoid NPE
				return true;
			}
			Display.getDefault().syncExec(new Runnable() {

				@Override
				public void run() {
					MessageBox messageBox = new MessageBox(Display.getDefault().getActiveShell(), SWT.ICON_QUESTION | SWT.YES | SWT.NO);

					List<?> list = selection.toList();
					if (list.size() == 1) {
						messageBox.setMessage(NLS.bind(TXMUIMessages.deletingOneResultP0Continue, list));
					}
					else {
						messageBox.setMessage(NLS.bind(TXMUIMessages.deletingSeveralResultsP0Continue, list.size()));
					}
					int response = messageBox.open();
					if (response != SWT.YES) {
						StatusLine.setMessage(""); //$NON-NLS-1$
						continueToDelete = false;
					}
				}
			});
		}
		return continueToDelete;
	}


}
