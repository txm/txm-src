// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.jface;

import org.eclipse.swt.widgets.Composite;
import org.txm.rcp.swt.widget.preferences.StringFieldEditor;

/**
 * extends the StringField Editor to manage double values 
 * @author mdecorde.
 */
public class DoubleFieldEditor extends StringFieldEditor {

	/**
	 * Instantiates a new double field editor.
	 *
	 * @param minscore the minscore
	 * @param string the string
	 * @param fieldEditorParent the field editor parent
	 */
	public DoubleFieldEditor(String preferenceName, String label, Composite fieldEditorParent) {
		super(preferenceName, label, fieldEditorParent);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.StringFieldEditor#isValid()
	 */
	@Override
	public boolean isValid() {
		try {
			Double.parseDouble(this.getStringValue());
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Gets the double value.
	 *
	 * @return the double value
	 */
	public double getDoubleValue() {
		return Double.parseDouble(this.getStringValue());
	}
}
