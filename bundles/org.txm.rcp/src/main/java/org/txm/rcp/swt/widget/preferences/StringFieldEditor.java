package org.txm.rcp.swt.widget.preferences;

import org.eclipse.swt.widgets.Composite;

/**
 * Add access to the internal Button
 */
public class StringFieldEditor extends org.eclipse.jface.preference.StringFieldEditor {
	
	Composite parent;
	
	public StringFieldEditor(String preference, String label, Composite parent) {
		super(preference, label, parent);
		this.parent = parent;
	}
	
	public void setToolTipText(String tooltip) {
		
		getLabelControl(parent).setToolTipText(tooltip);
		getTextControl(parent).setToolTipText(tooltip);
	}

	public void setEnabled(boolean b) {
		
		super.setEnabled(b, parent);
	}
}
