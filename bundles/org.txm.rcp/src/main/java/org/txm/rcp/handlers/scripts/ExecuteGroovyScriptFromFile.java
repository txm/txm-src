// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.scripts;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.Toolbox;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.utils.io.IOUtils;

/**
 * Ask the user to select a Groovy script using a File dialog and execute it
 */
public class ExecuteGroovyScriptFromFile extends AbstractHandler {

	private static final String ID = "org.txm.rcp.commands.ExecuteGroovyScriptFromFile"; //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		// the file to execute
		String result; //$NON-NLS-1$
		IWorkbenchPart page = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

		String scriptCurrentDir = Toolbox.getTxmHomePath() + "/scripts"; //$NON-NLS-1$

		FileDialog dialog = new FileDialog(shell, SWT.OPEN);

		if (LastOpened.getFile(ID) != null) {
			dialog.setFilterPath(LastOpened.getFolder(ID));
			dialog.setFileName(LastOpened.getFile(ID));
		}
		else {
			dialog.setFilterPath(scriptCurrentDir);
		}

		dialog.setFilterExtensions(new String[] { "*.groovy" }); //$NON-NLS-1$
		dialog.setFilterPath(scriptCurrentDir);
		result = dialog.open();
		if (result == null) return null;

		LastOpened.set(ID, new File(result));
		try {
			String content = IOUtils.getText(new File(result));
			//ExecuteGroovyScript.executeScript(result, page, selection);
			ExecuteText.executeText(content, result);
		}
		catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}
}
