package org.txm.rcp.editors;

import java.util.HashMap;
import java.util.List;

import org.eclipse.e4.ui.workbench.renderers.swt.HandledContributionItem;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.menus.IMenuService;
import org.txm.core.results.TXMResult;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.toolbar.DropDownCheckToolItem;
import org.txm.rcp.swt.toolbar.OpenCloseToolItem;

/**
 *
 * The default TXM result editor tool bar shared by every <code>TXMEditor</code> used to open/close a parameter panel.
 *
 * The tool bar is accessible and usable in plugin.xml with the 'toolbar:toolbar_local_id' URI specified in the constructor.
 *
 * @author sjacquot
 *
 */
public class TXMEditorToolBar extends ToolBar {

	/**
	 * The linked <code>TXMEditor</code>.
	 */
	protected TXMEditor<? extends TXMResult> editorPart;

	/**
	 * Contains references to installed groups to composite.
	 */
	private HashMap<String, Composite> groups = new HashMap<>();

	private GLComposite subWidgetComposite;

	/**
	 * Contains references to installed groups to ToolItem.
	 */
	private HashMap<String, ToolItem> buttons = new HashMap<>();

	private ToolBarManager manager;

	private DropDownCheckToolItem openCloseButton;

	/**
	 *
	 * The parent layout must be a GridLayout.
	 *
	 * @param parent where the Toolbar will be located
	 * @param subWidgetComposite OPTIONAL: where the installed Widgets (Groups) will be installed
	 * @param style
	 * @param toolbarId
	 * @param editor
	 */
	public TXMEditorToolBar(TXMEditor<? extends TXMResult> editor, Composite parent, GLComposite subWidgetComposite, int style, String toolbarId) {
		super(parent, style);

		FillLayout layout = new FillLayout();
		layout.spacing = 1;
		this.setLayout(layout);

		this.editorPart = editor;
		this.subWidgetComposite = subWidgetComposite;
		// permit to contribute via RCP plugin.xml menu extension
		manager = new ToolBarManager(this);
		IMenuService menuService = this.editorPart.getSite().getService(IMenuService.class);
		menuService.populateContributionManager(manager, "toolbar:" + toolbarId); //$NON-NLS-1$
	}

	@Override
	protected void checkSubclass() {
	}

	public ToolBarManager getToolBarManager() {
		return manager;
	}

	/**
	 *
	 * @return the composite that contains the sub widgets of the toolbar : installed groups, composites.
	 */
	public GLComposite getSubWidgetComposite() {
		return subWidgetComposite;
	}

	/**
	 * Gets the <code>TXMEditor</code> linked to this tool bar.
	 *
	 * @return the editorPart
	 */
	public TXMEditor<? extends TXMResult> getEditorPart() {
		return editorPart;
	}

	/**
	 * Sets the visibility state of the specified composite.
	 * The mechanism is based on GridData.exclude to hide the composite therefore the composite must have a GridData as layout data.
	 *
	 */
	public void setVisible(String id, boolean visible) {
		if (groups.containsKey(id) && !groups.get(id).isDisposed()) {
			Composite composite = groups.get(id);
			setVisible(composite, visible);
		}
	}

	/**
	 * Sets the visibility state of the specified composite.
	 * The mechanism is based on GridData.exclude to hide the composite therefore the composite must have a GridData as layout data.
	 *
	 * @param composite
	 *
	 */
	protected void setVisible(Composite composite, boolean visible) {

		if (composite.getLayoutData() instanceof GridData gdata) {
			gdata.exclude = !visible;
		}

		composite.setVisible(visible);
		subWidgetComposite.layout(true);
		composite.layout(true);
		editorPart.layout(true);
	}


	/**
	 * TODO not generic enough and call editorPart.getParametersGroupsComposite()
	 * Shows/hides the command parameters group.
	 *
	 * @param visible
	 */
	public void setComputingParametersVisible(boolean visible) {
		setVisible(TXMEditor.COMPUTING_PARAMETERS_GROUP_ID, visible);
	}

	//FIXME: SJ, 2025-01-14: moved to TXMParameterCopntrol class (see if we keep this or validate the class)
	//	/**
	//	 * Adds the specified Control to this tool bar.
	//	 * Convenience method to add non-ToolItem Control to the tool bar using the SWT.SEPARATOR trick, plus to pack and to set the right width.
	//	 *
	//	 * @param control
	//	 */
	//	public void addControl(final Control control) {
	//		final ToolItem itemSeparator = new ToolItem(this, SWT.SEPARATOR);
	//		control.pack();
	//		itemSeparator.setWidth(control.getBounds().width);
	//		itemSeparator.setControl(control);
	//		Listener listener = new Listener() {
	//
	//			@Override
	//			public void handleEvent(Event event) {
	//				control.pack();
	//				itemSeparator.setWidth(control.getBounds().width);
	//				//System.err.println("TXMEditorToolBar.addControl(...).new Listener() {...}.handleEvent()");
	//			}
	//		};
	//
	//		//control.addListener(SWT.RESIZE, listener);
	//		//control.addListener(SWT.Resize, listener);
	//		control.addListener(SWT.Modify, listener);
	//
	//		//control.addListener(SWT.CHANGED, listener);
	//
	//		//this.addListener(SWT.Resize, listener);
	//
	//	}



	/**
	 * Creates a <code>GLComposite</code> in the toolbar subwidgets container.
	 *
	 * @param groupTitle the composite uniq title in the toolbar
	 * @return
	 */
	public GLComposite installGLComposite(String groupTitle, int numColumns, boolean makeColumnsEqualWidth) {
		if (this.groups.containsKey(groupTitle)) {
			return (GLComposite) this.groups.get(groupTitle);
		}

		subWidgetComposite.getLayout().numColumns++;

		final GLComposite comp = new GLComposite(subWidgetComposite, SWT.NONE, "Group: " + groupTitle); //$NON-NLS-1$
		comp.getLayout().horizontalSpacing = 1;
		comp.getLayout().numColumns = numColumns;
		comp.getLayout().makeColumnsEqualWidth = makeColumnsEqualWidth;
		return comp;
	}

	/**
	 * Creates a group with a dropdown button and multiple modes
	 *
	 * @param groupTitle
	 * @param buttonToolTip
	 * @param openIconFilePath
	 * @param closeIconFilePath
	 * @param showGroup
	 * @param selectionListener
	 * @param modes The modes names
	 * @param modeSelectionListeners the corresponding mode selectionListener
	 */
	public Group installAlternativesGroup(final String groupTitle, String buttonToolTip, String openIconFilePath, String closeIconFilePath, boolean showGroup,
			SelectionListener selectionListener, List<String> modes, List<SelectionListener> modeSelectionListeners) {

		openCloseButton = new DropDownCheckToolItem(this, buttonToolTip, openIconFilePath, closeIconFilePath);

		openCloseButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (e.doit) {
					setVisible(groupTitle, openCloseButton.getSelection());
					selectionListener.widgetSelected(e);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		org.txm.rcp.swt.listeners.DropdownSelectionListener listenerOne = openCloseButton.getDropDownListener();
		// listenerOne.setDefaultSelectionListener(selectionListener);
		for (int i = 0; i < modes.size() && i < modeSelectionListeners.size(); i++) {
			MenuItem item1 = new MenuItem(listenerOne.getMenu(), SWT.PUSH);
			item1.setText(modes.get(i));
			item1.addSelectionListener(modeSelectionListeners.get(i));
		}

		final Group group = new Group(subWidgetComposite, SWT.NONE);
		group.setText(groupTitle);
		GridData gd2 = new GridData(GridData.FILL_BOTH);
		gd2.grabExcessVerticalSpace = false;
		gd2.grabExcessHorizontalSpace = true;
		gd2.exclude = true;
		group.setLayoutData(gd2);

		this.groups.put(groupTitle, group);
		this.buttons.put(groupTitle, openCloseButton);
		return group;
	}


	public class SelectionListener2 implements SelectionListener {

		Composite c;

		public SelectionListener2(Composite c) {
			this.c = c;
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			OpenCloseToolItem b = (OpenCloseToolItem) e.widget;
			setVisible(c, b.getSelection());
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
		}
	}

	/**
	 * Creates a <code>Group</code> with some configuration to make it visible/invisible and a <code>ToolItem</code> attached to the specified tool bar to show/hide this group.
	 *
	 * @param groupTitle
	 * @param buttonToolTip
	 * @param openIconFilePath
	 * @return the Group widget created
	 */
	public Composite installGroup(String groupTitle, String buttonToolTip, String openIconFilePath, String closeIconFilePath, boolean showGroup) {
		return installGroup(groupTitle, buttonToolTip, openIconFilePath, closeIconFilePath, showGroup, true);
	}

	/**
	 * Creates a <code>Group</code> with some configuration to make it visible/invisible and a <code>ToolItem</code> attached to the specified tool bar to show/hide this group.
	 *
	 * @param groupTitle
	 * @param buttonToolTip
	 * @param openIconFilePath
	 * @return the Group widget created
	 */
	public Composite installGroup(String groupTitle, String buttonToolTip, String openIconFilePath, String closeIconFilePath, boolean showGroup, boolean group) {

		if (this.groups.containsKey(groupTitle)) {
			return this.groups.get(groupTitle);
		}

		Composite c = null;
		if (group) {
			Group group2 = new Group(subWidgetComposite, SWT.NONE);
			group2.setText(" "+groupTitle.trim()+" ");
			c = group2;
		}
		else {
			c = new Composite(subWidgetComposite, SWT.NONE);
		}


		// FIXME: SJ: new version to avoid to redefine the grid layout in each subclasses editors
		// GridLayout layout = new GridLayout();
		// layout.marginLeft = 5;
		// layout.marginWidth = 0;
		// layout.marginBottom = 0;
		// layout.marginHeight = 0;
		// layout.marginTop = 0;

		// FIXME: SJ: old version
		RowLayout layout = new RowLayout();
		layout.wrap = true;
		layout.center = true;
		layout.spacing = 8;
		layout.marginWidth = 0;
		layout.marginBottom = 0;
		layout.marginHeight = 0;
		layout.marginTop = 0;

		c.setLayout(layout);

		GridData gd2 = new GridData(GridData.FILL_BOTH);
		gd2.grabExcessVerticalSpace = false;
		gd2.grabExcessHorizontalSpace = true;
		gd2.exclude = true;
		c.setLayoutData(gd2);
		c.setVisible(false);

		// add finally a button to the toolbar to show/hide the Group
		OpenCloseToolItem showParameters = new OpenCloseToolItem(this, groupTitle, openIconFilePath, closeIconFilePath, buttonToolTip);
		this.setVisible(c, showGroup); // default init

		showParameters.addSelectionListener(new SelectionListener2(c));

		this.groups.put(groupTitle, c);
		this.buttons.put(groupTitle, showParameters);
		return c;
	}

	public void unInstallGroup(String groupTitle) {
		if (this.groups.containsKey(groupTitle)) {
			this.groups.get(groupTitle).dispose();
		}
		if (this.buttons.containsKey(groupTitle)) {
			this.buttons.get(groupTitle).dispose();
		}
	}

	public ToolItem getGroupButton(String groupTitle) {
		return this.buttons.get(groupTitle);
	}


	/**
	 * Gets a ToolItem from RCP item contribution from plugin.xml by its id.
	 *
	 * @param id
	 * @return
	 */
	public ToolItem getItemByContributionId(String id) {
		ToolItem item = null;
		for (int i = 0; i < this.getItemCount(); i++) {

			if (this.getItem(i).getData() != null && this.getItem(i).getData() instanceof HandledContributionItem) {
				HandledContributionItem contribution = (HandledContributionItem) this.getItem(i).getData();
				if (contribution.getId().equals(id)) {
					item = this.getItem(i);
					break;
				}
			}
		}
		return item;
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		for (Control c : this.getChildren()) {
			c.setEnabled(enabled);
		}
	}

	public boolean isVisible(String id) {
		if (!groups.containsKey(id)) return false;

		if (groups.get(id).isDisposed()) return false;

		return groups.get(id).isVisible();
	}

	public DropDownCheckToolItem getOpenCloseButton() {
		return openCloseButton;
	}
}
