package org.txm.rcp.swt.widget.preferences;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.utils.logger.Log;

// FIXME: not used
public class ___TXMDirectoryFieldEditor extends DirectoryFieldEditor {

	Button openModelDirectoryButton;

	public ___TXMDirectoryFieldEditor() {
		// TODO Auto-generated constructor stub
	}

	public ___TXMDirectoryFieldEditor(String name, String labelText,
			Composite parent) {
		super(name, labelText, parent);

		GridLayout gl = (GridLayout) parent.getLayout();
		//System.out.println("GL size "+gl.numColumns);
		setChangeButtonText("..."); //$NON-NLS-1$

		Button changeButton = getChangeControl(parent);
		changeButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, false, false));

		openModelDirectoryButton = new Button(parent, SWT.PUSH);
		openModelDirectoryButton.setText(TXMUIMessages.open);
		openModelDirectoryButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, false, false));
		openModelDirectoryButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					Desktop.getDesktop().open(new File(getTextControl().getText()));
				}
				catch (IOException e1) {
					System.out.println("Error while opening model path directory: " + e1); //$NON-NLS-1$
					Log.printStackTrace(e1);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}

	@Override
	public int getNumberOfControls() {
		return super.getNumberOfControls() + 1;
	}
}
