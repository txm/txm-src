package org.txm.rcp;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.TextField;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class ___SWTAWTFocusBugsSnippet {

	public static void main(String[] args) {

		final Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());

		Composite composite = new Composite(shell, SWT.EMBEDDED);

		Frame frame = SWT_AWT.new_Frame(composite);
		Panel panel = new Panel(new BorderLayout());
		frame.add(panel);
		panel.add(new TextField());

		Text text = new Text(shell, SWT.BORDER);
		text.setToolTipText("test");
		shell.setSize(200, 70);
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
