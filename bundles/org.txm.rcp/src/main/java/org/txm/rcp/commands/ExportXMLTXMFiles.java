package org.txm.rcp.commands;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.results.TXMResult;
import org.txm.objects.Project;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.logger.Log;
import org.txm.utils.zip.Zip;

public class ExportXMLTXMFiles extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object o = getCorporaViewSelectedObject(event);
		if (o instanceof TXMResult) {
			Project p = ((TXMResult) o).getProject();

			Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

			FileDialog d = new FileDialog(shell, SWT.SAVE);
			d.setFilterExtensions(new String[] { "*.zip" }); //$NON-NLS-1$

			String path = d.open();
			if (path != null) {
				File targetZipFile = new File(path);
				File txmDir = new File(p.getProjectDirectory(), "txm/" + p.getName()); //$NON-NLS-1$
				try {
					File[] files = txmDir.listFiles();
					if (files == null) {
						Log.warning(TXMUIMessages.bind(TXMUIMessages.WarningNoTXMDirectoryP0, txmDir));
						return false;
					}
					int s = files.length;
					if (s == 0) {
						Log.warning(TXMUIMessages.bind(TXMUIMessages.WarningNoXMLTXMFilesDirectoryP0, txmDir));
						return false;
					}
					Zip.compress(txmDir, targetZipFile, new ConsoleProgressBar(s));
					Log.info(TXMUIMessages.bind(TXMUIMessages.XMLTXMFilesExportedInP0, targetZipFile));
				}
				catch (IOException e) {
					Log.printStackTrace(e);
				}
			}
		}
		return null;
	}

}
