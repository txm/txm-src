package org.txm.rcp.editors.imports.sections;

import java.lang.reflect.Field;
import java.util.ArrayList;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.txm.core.results.BeanParameters;
import org.txm.core.results.Parameter;
import org.txm.objects.Project;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public abstract class ImportEditorSection {

	ImportFormEditor editor;

	ArrayList<ImportSubSection> subsections = new ArrayList<ImportSubSection>();

	public static TableWrapData getButtonLayoutData() {
		TableWrapData td = new TableWrapData(TableWrapData.LEFT, TableWrapData.MIDDLE);
		return td;
	}

	public static TableWrapData getLabelGridData() {
		TableWrapData gdata = new TableWrapData(TableWrapData.FILL, TableWrapData.MIDDLE);
		return gdata;
	}

	public static TableWrapData getLongTextGridData() {
		TableWrapData gdata = new TableWrapData(TableWrapData.FILL, TableWrapData.MIDDLE);
		gdata.maxWidth = 200;
		return gdata;
	}

	public static TableWrapData getSectionGridData(int size) {
		TableWrapData td = new TableWrapData(TableWrapData.FILL);
		td.colspan = size;
		return td;
	}

	public static TableWrapData getTextGridData() {
		TableWrapData gdata = new TableWrapData(TableWrapData.FILL, TableWrapData.MIDDLE);
		gdata.maxWidth = 150;
		return gdata;
	}

	public FormToolkit getToolkit() {
		return toolkit;
	}

	public ScrolledForm getScrolledForm() {
		return form;
	}

	public Section getSection() {
		return section;
	}

	protected ScrolledForm form;

	protected Project project;

	protected Section section;

	protected FormToolkit toolkit;

	private String name;

	/**
	 * @param toolkit
	 * @param form
	 * @param parent
	 * @param style
	 * @param name the section name must not be nulled and will be used to read the import.xml file
	 */
	public ImportEditorSection(ImportFormEditor editor, FormToolkit toolkit, ScrolledForm form, Composite parent, int style, String name) {

		if (name == null) throw new IllegalArgumentException("Section name must not be null or empty."); //$NON-NLS-1$

		section = toolkit.createSection(parent, style);
		this.editor = editor;
		this.section.setText(name);
		this.toolkit = toolkit;
		this.form = form;
		this.name = name;
	}

	/**
	 * Check if all necessary parameters are set
	 * 
	 * @return true if all necessary parameters are set
	 */
	public final boolean check() {

		if (this.checkFields()) {
			for (ImportSubSection sub : subsections) {
				if (!sub.checkFields()) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Check if all necessary parameters are set
	 * 
	 * @return true if all necessary parameters are set
	 */
	public abstract boolean checkFields();

	public abstract int getSectionSize(); // 1 2 3, it depends the number of element inserted in the form

	public boolean isDisposed() {
		if (section != null) return section.isDisposed();
		return true;
	}

	/**
	 * Update the Project import parameters using the section widgets values
	 * 
	 * @param project
	 */
	public final boolean save(Project project) {

		if (this.saveFields(project)) {
			for (ImportSubSection sub : subsections) {
				if (!sub.saveFields(project)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Update the Project import parameters using the section widgets values
	 * 
	 * @param project
	 */
	public abstract boolean saveFields(Project project);

	/**
	 * write interface parameters to a XML DOM
	 * 
	 * NOT USED YET
	 * 
	 * @param document
	 */
	public boolean save(Document document) {

		NodeList sectionList = document.getElementsByTagName("section"); //$NON-NLS-1$
		Element sectionElement = null;
		for (int i = 0; i < sectionList.getLength(); i++) {
			if (name.equals(sectionList.item(i).getAttributes().getNamedItem("name").getNodeValue())) { //$NON-NLS-1$
				sectionElement = (Element) sectionList.item(i);
			}
		}
		if (sectionElement == null) {
			sectionElement = document.createElement("section"); //$NON-NLS-1$
			sectionElement.setAttribute("name", name); //$NON-NLS-1$
		}

		for (Field f : BeanParameters.getAllFields(this)) {
			Parameter p = f.getAnnotation(Parameter.class);
			// TXMEditor.getValueForWidget(f);
		}

		return false;

	}

	public void setEnabled(boolean b) {
		if (section != null) section.setEnabled(b);
	}

	/**
	 * Update the section widgets with the Project import parameters
	 * 
	 * @param project
	 */
	public final void update(Project project) {

		this.updateFields(project);
		for (ImportSubSection sub : subsections) {
			sub.updateFields(project);
		}
	}

	/**
	 * Update the section widgets with the Project import parameters
	 * 
	 * @param project
	 */
	public abstract void updateFields(Project project);
}

