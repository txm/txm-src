package org.txm.rcp.testers;

import org.eclipse.core.expressions.PropertyTester;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.preferences.RCPPreferences;

/**
 * Checks if the results persistence is enabled.
 * 
 * @author sjacquot
 *
 */
public class AutoPersistenceDisabled extends PropertyTester {

	public static final String PROPERTY_NAMESPACE = "org.txm.rcp.testers"; //$NON-NLS-1$

	public static final String PROPERTY_STATE_ENGINE_READY = "AutoPersistenceDisabled"; //$NON-NLS-1$


	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {

		boolean enabled = RCPPreferences.getInstance().getBoolean(TBXPreferences.AUTO_PERSISTENCE_ENABLED);
		//System.err.println("IS results persistence enabled: " + enabled);
		return !enabled;
	}

}
