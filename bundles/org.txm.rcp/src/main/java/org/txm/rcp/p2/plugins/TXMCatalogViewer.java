package org.txm.rcp.p2.plugins;

import org.eclipse.equinox.internal.p2.discovery.Catalog;
import org.eclipse.equinox.internal.p2.discovery.model.CatalogCategory;
import org.eclipse.equinox.internal.p2.discovery.model.CatalogItem;
import org.eclipse.equinox.internal.p2.ui.discovery.util.ControlListItem;
import org.eclipse.equinox.internal.p2.ui.discovery.wizards.CatalogConfiguration;
import org.eclipse.equinox.internal.p2.ui.discovery.wizards.CatalogViewer;
import org.eclipse.equinox.internal.p2.ui.discovery.wizards.CategoryItem;
import org.eclipse.equinox.internal.p2.ui.discovery.wizards.Messages;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

public class TXMCatalogViewer extends CatalogViewer {

	public TXMCatalogViewer(Catalog catalog, IShellProvider shellProvider,
			IRunnableContext context, CatalogConfiguration configuration) {
		super(catalog, shellProvider, context, configuration);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected ControlListItem<?> doCreateViewerItem(Composite parent, Object element) {
		if (element instanceof CatalogItem) {
			return new TXMDiscoveryItem<>(parent, SWT.BORDER, getResources(), shellProvider, (CatalogItem) element, this);
		}
		else if (element instanceof CatalogCategory) {
			return new CategoryItem<>(parent, SWT.NONE, getResources(), (CatalogCategory) element);
		}
		return null;
	}

	@Override
	public void modifySelection(final CatalogItem connector, boolean selected) {
		super.modifySelection(connector, selected);
	}

	@Override
	protected void doCheckCatalog() {
		// System.out.println("DO CHECK CATALOG");
		int categoryWithConnectorCount = 0;
		int itemCount = getCatalog().getItems().size();

		for (CatalogCategory category : getCatalog().getCategories()) {
			categoryWithConnectorCount += category.getItems().size();
		}

		if (categoryWithConnectorCount == 0 && itemCount == 0) {
			// nothing was discovered: notify the user
			MessageDialog.openWarning(shellProvider.getShell(), Messages.ConnectorDiscoveryWizardMainPage_noConnectorsFound, Messages.ConnectorDiscoveryWizardMainPage_noConnectorsFound_description);
		}
	}
}
