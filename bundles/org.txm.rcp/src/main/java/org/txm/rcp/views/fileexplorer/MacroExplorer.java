// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views.fileexplorer;

import java.io.File;
import java.util.List;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.txm.Toolbox;
import org.txm.core.engines.EngineType;
import org.txm.core.engines.ScriptEngine;
import org.txm.core.engines.ScriptEnginesManager;
import org.txm.rcp.TXMWindows;
import org.txm.rcp.handlers.files.CopyFile;
import org.txm.rcp.handlers.files.CutFile;
import org.txm.rcp.handlers.files.DeleteFile;
import org.txm.rcp.handlers.files.EditFile;
import org.txm.rcp.handlers.files.PasteFile;
import org.txm.rcp.handlers.files.RenameFile;
import org.txm.rcp.handlers.scripts.CreateMacro;
import org.txm.rcp.handlers.scripts.ExecuteGroovyMacro;
import org.txm.rcp.handlers.scripts.ExecuteScript;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.utils.FileUtils;

/**
 * Macro explorer
 * 
 * @author mdecorde
 */
public class MacroExplorer extends ViewPart {

	/** The ID. */
	static public String ID = MacroExplorer.class.getName();

	/** The home. */
	protected static String home = Toolbox.getTxmHomePath();
	// protected static String homeUser = Toolbox.getTxmHomePath();

	protected static File currentRootDir;

	// protected static File currentUserRootDir;

	protected static boolean showAll = false;

	/** The files tree viewer. */
	protected TreeViewer tv;

	protected Button all;

	/**
	 * Instantiates a new explorer.
	 */
	public MacroExplorer() {
		initCurrentDirectory();
	}

	public boolean isShowAll() {
		return showAll;
	}

	private static void initCurrentDirectory() {
		home = Toolbox.getTxmHomePath();
		if (home == null) home = System.getProperty("user.home"); //$NON-NLS-1$

		// File scriptsUserDir = new File(home, "scripts/groovy/user/"); //$NON-NLS-1$
		File scriptsDir = new File(home, "scripts/groovy/user/"); //$NON-NLS-1$
		currentRootDir = new File(scriptsDir, "org/txm/macro"); //$NON-NLS-1$
		// currentUserRootDir = new File(scriptsUserDir, "org/txm/macro"); //$NON-NLS-1$
		home = currentRootDir.getAbsolutePath();
		// homeUser = currentUserRootDir.getAbsolutePath();
	}

	public String getHome() {
		return home;
	}
	// public String getUserHome() { return homeUser;}

	/**
	 * Refresh.
	 */
	public static void refresh() {

		if (!PlatformUI.isWorkbenchRunning()) return;

		MacroExplorer explorerView = (MacroExplorer) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findView(
						MacroExplorer.ID);

		// TODO this might cause big latences
		initCurrentDirectory();

		if (explorerView != null) {
			explorerView.tv.setInput(new File(home));
			explorerView.tv.refresh();
		}
	}

	/**
	 * Refresh a file.
	 */
	public static void refresh(File f) {
		if (f == null) return;

		MacroExplorer explorerView = (MacroExplorer) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findView(
						MacroExplorer.ID);

		if (explorerView != null) {
			explorerView.tv.refresh();
			explorerView.tv.reveal(f);
			explorerView.tv.setSelection(new StructuredSelection(f));
		}
	}

	public TreeViewer getTreeViewer() {
		return tv;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {

		FormLayout formLayout = new FormLayout();
		formLayout.marginWidth = 0;
		formLayout.marginHeight = 0;
		parent.setLayout(formLayout);

		// controles composite
		Composite controls = new Composite(parent, SWT.NONE);

		FormData controlsData = new FormData();
		controlsData.top = new FormAttachment(0);
		controlsData.left = new FormAttachment(0);
		controlsData.right = new FormAttachment(100);
		controls.setLayoutData(controlsData);

		GridLayout controlsLayout = new GridLayout(3, false);
		controlsLayout.marginBottom = controlsLayout.marginHeight = 0;
		controls.setLayout(controlsLayout);

		Button refresh = new Button(controls, SWT.NONE);
		refresh.setText(TXMUIMessages.Explorer_5);
		refresh.setToolTipText(TXMUIMessages.common_refresh);
		refresh.setLayoutData(new GridData());
		refresh.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				initCurrentDirectory();
				tv.setInput(new File(home));
				tv.refresh();
			}
		});

		Button newMacro = new Button(controls, SWT.NONE);
		newMacro.setLayoutData(new GridData());
		newMacro.setText(TXMUIMessages.newMacro);
		newMacro.setToolTipText(TXMUIMessages.createANewMacroFromATemplate);
		newMacro.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				currentRootDir.mkdirs();

				InputDialog dialog = new InputDialog(e.display.getActiveShell(), TXMUIMessages.macroName, TXMUIMessages.pleaseWriteTheMacroName, "", null); //$NON-NLS-1$

				dialog.open();
				String name = dialog.getValue();
				Object o = tv.getStructuredSelection().getFirstElement();
				String rootPath = currentRootDir.getAbsolutePath();
				if (o != null && o instanceof File file) {
					if (file.isDirectory()) {
						rootPath = file.getAbsolutePath();
					}
					else {
						rootPath = file.getParentFile().getAbsolutePath();
					}
				}

				File newMacroFile = CreateMacro.createMacro(rootPath, name);
				if (newMacroFile != null) {
					MacroExplorer.refresh(newMacroFile);
				}
			}
		});

		all = new Button(controls, SWT.CHECK);
		all.setText(TXMUIMessages.all);
		all.setToolTipText(TXMUIMessages.showAllFiles);
		all.setLayoutData(new GridData());
		all.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				showAll = all.getSelection();
				refresh();
			}
		});

		// set positions & sizes
		// FormData widgetData = new FormData();
		// widgetData.right = new FormAttachment(100);
		// refresh.setLayoutData(widgetData);

		// the explorer
		tv = new TreeViewer(parent);
		tv.setContentProvider(new MacroContentProvider(this));
		tv.setLabelProvider(new MacroTreeLabelProvider(this));

		// TreeViewerColumn mainColumn = new TreeViewerColumn(tv, SWT.NONE);
		// mainColumn.getColumn().setText("File");
		// mainColumn.getColumn().setWidth(300);
		// mainColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(new MacroTreeLabelProvider(this)));

		tv.setInput(new File(home));

		FormData tvData = new FormData();
		tvData.top = new FormAttachment(controls, 0);
		tvData.bottom = new FormAttachment(100);
		tvData.left = new FormAttachment(0);
		tvData.right = new FormAttachment(100);
		tv.getTree().setLayoutData(tvData);

		tv.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				TreeSelection selection = (TreeSelection) tv.getSelection();
				File selectedItem = (File) selection.getFirstElement();
				if (!selectedItem.isDirectory()) {
					IWorkbenchWindow acWindow = TXMWindows.getActiveWindow();
					IWorkbenchPart page = acWindow.getActivePage().getActivePart();
					IWorkbenchPart editor = acWindow.getActivePage().getActiveEditor();

					if (selectedItem.getName().toLowerCase().endsWith(".groovy")) { //$NON-NLS-1$
						ExecuteGroovyMacro.execute(selectedItem.getAbsolutePath(), page, editor, selection, null, null, null);
					}
					else {
						String ext = FileUtils.getExtension(selectedItem);
						ScriptEnginesManager manager = (ScriptEnginesManager) Toolbox.getEngineManager(EngineType.SCRIPT);
						ScriptEngine se = manager.getEngineForExtension(ext);
						if (se != null) {
							ExecuteScript.executeScript(selectedItem.getAbsolutePath(), page, editor, selection, null);
						}
						else {
							EditFile.openfile(selectedItem.getAbsolutePath());
						}
					}
				}
				else {
					changeState(selection);
				}
			}
		});

		tv.getTree().addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
			} // nothing to do

			@Override
			public void keyPressed(KeyEvent e) {
				ISelection sel = tv.getSelection();
				if (sel instanceof IStructuredSelection) {
					List<?> objects = ((IStructuredSelection) sel).toList();
					if (e.keyCode == SWT.DEL) {
						boolean definitive = true; //(e.stateMask & SWT.SHIFT) != 0;
						DeleteFile.delete(objects, definitive);
					}
					else if (e.keyCode == SWT.F5) {
						MacroExplorer.refresh();
					}
					else {
						for (Object o : objects) {
							// Object o = ((IStructuredSelection)sel).getFirstElement();
							if (o instanceof File) {
								File file = (File) o;
								if (e.keyCode == SWT.DEL) {
									boolean definitive = true; // (e.stateMask & SWT.SHIFT) != 0;
									DeleteFile.delete(file, definitive);
								}
								else if ((char) e.keyCode == 'c' & ((e.stateMask & SWT.CTRL) != 0)) {
									CopyFile.copy(file);
								}
								else if ((char) e.keyCode == 'x' & ((e.stateMask & SWT.CTRL) != 0)) {
									CutFile.cut(file);
								}
								else if ((char) e.keyCode == 'v' & ((e.stateMask & SWT.CTRL) != 0)) {
									if (file.isFile()) file = file.getParentFile();
									PasteFile.paste(file);
								}
								else if (e.keyCode == SWT.F2) {
									RenameFile.rename(file);
								}
							}
						}
					}

					tv.refresh();
				}
			}
		});

		MenuManager menuManager = new MenuManager();
		Menu menu = menuManager.createContextMenu(tv.getTree());

		// Set the MenuManager
		tv.getTree().setMenu(menu);
		getSite().registerContextMenu(menuManager, tv);
		getSite().setSelectionProvider(tv);
	}

	/**
	 * Can delete.
	 *
	 * @param selectedItem the selected item
	 * @return true, if successful
	 */
	protected boolean CanDelete(File selectedItem) {
		File txmhome = new File(Toolbox.getTxmHomePath());
		File scripts = new File(Toolbox.getTxmHomePath(), "scripts"); //$NON-NLS-1$
		File corpora = new File(Toolbox.getTxmHomePath(), "corpora"); //$NON-NLS-1$
		// File registry = new File(Toolbox.getTxmHomePath(), "registry"); //$NON-NLS-1$
		File workspaces = new File(Toolbox.getTxmHomePath(), "workspaces"); //$NON-NLS-1$
		File xsl = new File(Toolbox.getTxmHomePath(), "xsl"); //$NON-NLS-1$
		File samples = new File(Toolbox.getTxmHomePath(), "samples"); //$NON-NLS-1$
		if (selectedItem.equals(txmhome) ||
				selectedItem.equals(txmhome) ||
				selectedItem.equals(scripts) ||
				selectedItem.equals(corpora) ||
				// selectedItem.equals(registry) ||
				selectedItem.equals(workspaces) ||
				selectedItem.equals(samples) ||
				selectedItem.equals(xsl)) {
			System.out.println(NLS.bind(TXMUIMessages.folderP0ShouldNotBeDeleted, selectedItem));
			return false;
		}
		return false;
	}

	/**
	 * Change state.
	 *
	 * @param selection the selection
	 */
	protected void changeState(TreeSelection selection) {
		TreeItem[] items = tv.getTree().getSelection();
		for (TreeItem item : items) {
			if (item.getExpanded()) {
				item.setExpanded(false);
			}
			else {
				item.setExpanded(true);
			}
			tv.refresh();
		}
	}

	/**
	 * Sets the root.
	 *
	 * @param home the new root
	 */
	public void setRoot(String home) {
		MacroExplorer.home = home;
		tv.setInput(new File(home));
		tv.refresh();
	}

	/**
	 * Sets the root.
	 *
	 * @param f the new root
	 */
	public void setRoot(File f) {
		MacroExplorer.home = f.getAbsolutePath();
		tv.setInput(new File(home));
		tv.refresh();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		tv.getTree().setFocus();
	}
}
