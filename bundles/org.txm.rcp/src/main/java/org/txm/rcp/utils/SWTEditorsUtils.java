/**
 * 
 */
package org.txm.rcp.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.advanced.MArea;
import org.eclipse.e4.ui.model.application.ui.advanced.impl.AreaImpl;
import org.eclipse.e4.ui.model.application.ui.advanced.impl.PlaceholderImpl;
import org.eclipse.e4.ui.model.application.ui.basic.MCompositePart;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainer;
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainerElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.model.application.ui.basic.impl.BasicFactoryImpl;
import org.eclipse.e4.ui.model.application.ui.basic.impl.PartSashContainerImpl;
import org.eclipse.e4.ui.model.application.ui.basic.impl.PartStackImpl;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.internal.Workbench;
import org.eclipse.ui.part.EditorPart;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.ITXMResultEditor;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TXMMultiPageEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.utils.logger.Log;

/**
 * Utility class to manage EditorPart.
 * Methods that modify the UI are intended to be called in the UI thread.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class SWTEditorsUtils {

	private static HashMap<MPartSashContainerElement, MPartStack[]> layouts = new HashMap<MPartSashContainerElement, MPartStack[]>();

	public static PartStackImpl mainPartStackImpl = null;

	public static void cleanAreaModel() {
		// org.eclipse.ui.editorss
		IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
		for (IWorkbenchWindow window : windows) {

			EModelService eModelService = window.getService(EModelService.class);
			//System.out.println(eModelService);
			PlaceholderImpl editorss = (PlaceholderImpl) eModelService.find("org.eclipse.ui.editorss", window.getService(MWindow.class));

			List<PartStackImpl> partStackImpls = eModelService.findElements(editorss, null, PartStackImpl.class);
			int s = partStackImpls.size();
			for (PartStackImpl psi : partStackImpls) {

				if (s == 1) {
					break; // keep one partStack
				}

				if (psi.getChildren() == null || psi.getChildren().size() == 0) {
					psi.getParent().getChildren().remove(psi);
					s--;
				}
			}
		}
	}

	/**
	 * Gets all opened editors which use some results of the specified list of results.
	 * 
	 * @return all opened editors if there is at least one otherwise returns an empty list
	 */
	// FIXME: SJ: do only one pass when we will remove the TXMMultipageEditor
	public static Set<ITXMResultEditor<TXMResult>> getEditors() {


		HashSet<ITXMResultEditor<TXMResult>> editors = new HashSet<>();

		// first pass: get all opened editors
		IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
		for (IWorkbenchWindow window : windows) {
			IWorkbenchPage[] pages = window.getPages();
			for (IWorkbenchPage page : pages) {
				for (IEditorReference reference : page.getEditorReferences()) {
					IEditorPart tmpEditor = reference.getEditor(false);

					if (tmpEditor instanceof TXMEditor txmEditor) {
						editors.add(txmEditor);
					}
				}
			}
		}

		return editors;
	}

	/**
	 * Gets all opened editors which use some results of the specified list of results.
	 * 
	 * @return all opened editors if there is at least one otherwise returns an empty list
	 */
	// FIXME: SJ: do only one pass when we will remove the TXMMultipageEditor
	public static Set<ITXMResultEditor<TXMResult>> getEditors(List<TXMResult> results) {

		Map<TXMResult, ArrayList<ITXMResultEditor<TXMResult>>> tmpEditors = new HashMap<>();

		HashSet<ITXMResultEditor<TXMResult>> editors = new HashSet<>();

		// first pass: get all opened editors
		IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
		for (IWorkbenchWindow window : windows) {
			IWorkbenchPage[] pages = window.getPages();
			for (IWorkbenchPage page : pages) {
				for (IEditorReference reference : page.getEditorReferences()) {
					IEditorPart tmpEditor = reference.getEditor(false);

					if (tmpEditor instanceof TXMMultiPageEditor) {
						TXMMultiPageEditor txmMultipageEditor = ((TXMMultiPageEditor) tmpEditor);
						for (int i = 0; i < txmMultipageEditor.getEditors().size(); i++) {
							if (txmMultipageEditor.getEditors().get(i) instanceof ITXMResultEditor) {
								ITXMResultEditor txmEditor = ((ITXMResultEditor) txmMultipageEditor.getEditors().get(i));
								ArrayList<ITXMResultEditor<TXMResult>> eds = tmpEditors.get(txmEditor.getResult());
								if (eds == null) {
									eds = new ArrayList<>();
									tmpEditors.put(txmEditor.getResult(), eds);
								}
								eds.add(txmEditor);
							}
						}
					}
					else if (tmpEditor instanceof ITXMResultEditor) {

						ITXMResultEditor<TXMResult> txmEditor = (ITXMResultEditor<TXMResult>) tmpEditor;
						ArrayList<ITXMResultEditor<TXMResult>> eds = tmpEditors.get(txmEditor.getResult());
						if (eds == null) {
							eds = new ArrayList<>();
							tmpEditors.put(txmEditor.getResult(), eds);
						}
						eds.add(txmEditor);
					}
				}
			}
		}

		// second pass: filtering on used results if needed
		for (Entry<TXMResult, ArrayList<ITXMResultEditor<TXMResult>>> entry : tmpEditors.entrySet()) {
			if (results == null || results.contains(entry.getKey())) {
				ArrayList<ITXMResultEditor<TXMResult>> eds = entry.getValue();
				for (int i = 0; i < eds.size(); i++) {
					editors.add(eds.get(i));
				}
			}
		}

		return editors;
	}


	/**
	 * Gets all opened editors which use the specified result.
	 * 
	 * @param result
	 * @return all opened editors which use the specified result if there is at least one otherwise returns an empty list
	 */
	public static Set<ITXMResultEditor<TXMResult>> getEditors(TXMResult result) {

		ArrayList<TXMResult> results = new ArrayList<>();
		if (result != null) {
			results.add(result);
		}
		return getEditors(results);
	}

	/**
	 * Gets the first editor of the result
	 * 
	 * @param result
	 * @return the first editor of the result or null
	 */
	public static ITXMResultEditor<TXMResult> getEditor(TXMResult result) {

		for (ITXMResultEditor<TXMResult> editor : getEditors(result)) {
			return editor;
		}
		return null;
	}


	/**
	 * Closes all opened editors which use the specified result.
	 * 
	 * @param result
	 */
	public static void closeEditors(TXMResult result, boolean closeChildrenEditors) {

		ArrayList<TXMResult> results = new ArrayList<>();
		if (result != null) {
			results.add(result);
		}

		// add children if needed
		if (closeChildrenEditors) {
			results.addAll(result.getDeepChildren());
		}

		closeEditors(results);
	}

	/**
	 * Closes all opened editors which use the specified result or one of this child.
	 * 
	 * @param result
	 */
	public static void closeEditors(TXMResult result) {

		closeEditors(result, true);
	}

	/**
	 * Closes all opened editors managed by the addEditor or addPart methods
	 * 
	 * @param result
	 */
	public static void closeEditors() {

		for (MPartSashContainerElement source : layouts.keySet()) {
			if (source.getWidget() != null) {
				System.out.println("Close source: " + source.getWidget());
			}
			if (layouts.get(source) != null) {
				for (MPartStack dest : layouts.get(source)) {
					if (dest != null && dest.getWidget() != null) {
						System.out.println("	Close desc: " + dest.getWidget());
						if (dest.getWidget() instanceof IEditorPart editor) {
							//	editor.getSite().getPage().close(editor);
						}
					}

				}
			}
		}
	}

	/**
	 * Closes all opened editors which use some results of the specified list of results.
	 * 
	 * @param results
	 */
	public static void closeEditors(List<TXMResult> results) {

		Set<ITXMResultEditor<TXMResult>> editors = getEditors(results);
		for (ITXMResultEditor<TXMResult> editor : editors) {
			editor.close();
		}
	}



	// FIXME: SJ: became useless, to remove when editors auto-closing will be validated
	// /**
	// * Closes the opened editors which use the specified Object.
	// * If closeChildrenEditors equals to true also closes the result children opened editors.
	// *
	// * @param object the object used by some editors
	// * @param closeChildrenEditors if true also closes the result children opened editors
	// */
	// // TODO: SJ: this method may be optimized
	// public static void closeEditorOf(Object object, boolean closeChildrenEditors) {
	//
	// if (!(object instanceof TXMResult)) {
	// return;
	// }
	//
	// // gets the result children
	// TXMResult result = (TXMResult) object;
	// ArrayList<TXMResult> resultBranch = new ArrayList<>();
	// resultBranch.add(result);
	// if (closeChildrenEditors) {
	// resultBranch.addAll(result.getDeepChildren());
	// }
	//
	// // close editors of the result and its children
	// ArrayList<ITXMResultEditor<?>> editors = new ArrayList<>(SWTEditorsUtils.getEditors());
	// for (TXMResult tmpResult : resultBranch) {
	// for (ITXMResultEditor<?> editor : editors) {
	// if (tmpResult == editor.getResult()) {
	// editor.getEditorSite().getPage().closeEditor(editor, true);
	// }
	// }
	// }
	// }
	//
	/**
	 * Refreshes all opened editors which use the specified result.
	 * 
	 * @param result
	 * @return true if at least one editor has been refreshed otherwise false
	 */
	public static boolean refreshEditors(TXMResult result) {

		Set<ITXMResultEditor<TXMResult>> editors = getEditors(result);

		for (ITXMResultEditor<TXMResult> editor : editors) {
			try {
				editor.refresh(true);
			}
			catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	//
	// public static boolean activateEditor(TXMResult r) {
	// IEditorPart e = getEditor(r);
	// if (e != null && e instanceof ITXMResultEditor && ((ITXMResultEditor<?>) e).getResult() == r) {
	// try {
	// ((ITXMResultEditor<?>) e).getSite().getPage().activate(e);
	// return true;
	// }
	// catch (Exception e1) {
	// // TODO Auto-generated catch block
	// e1.printStackTrace();
	// }
	// }
	// return false;
	// }

	/**
	 * Gets the current active editor.
	 * 
	 * @param event
	 * @return
	 */
	public static ITXMResultEditor<? extends TXMResult> getActiveEditor(ExecutionEvent event) {

		IEditorPart editor = null;

		if (event != null) {
			editor = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActiveEditor();
		}
		else {
			editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		}

		if (editor instanceof TXMMultiPageEditor) {
			editor = ((TXMMultiPageEditor) editor).getMainEditorPart();

		}

		if (!(editor instanceof ITXMResultEditor)) {
			return null;
		}

		return (ITXMResultEditor<? extends TXMResult>) editor;
	}

	/**
	 * Checks if the editor specified by its editor input is already open in the active page.
	 * 
	 * @param editorInput
	 * @return
	 */
	public static boolean isOpenEditor(IEditorInput editorInput, String editorId) {

		return (getEditor(editorInput, editorId) != null);
	}

	/**
	 * Use this to avoid opening editors in new PartStack sashes
	 */
	private static void selectMainPartStack() {
		if (mainPartStackImpl == null
				|| mainPartStackImpl.getWidget() == null
				|| !mainPartStackImpl.isVisible()) {
			IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
			for (IWorkbenchWindow window : windows) {

				EModelService eModelService = window.getService(EModelService.class);
				//System.out.println(eModelService);
				PlaceholderImpl editorss = (PlaceholderImpl) eModelService.find("org.eclipse.ui.editorss", window.getService(MWindow.class));

				List<PartStackImpl> partStackImpls = eModelService.findElements(editorss, null, PartStackImpl.class);
				for (PartStackImpl psi : partStackImpls) {
					if (psi.isVisible() && psi.getWidget() != null) {
						mainPartStackImpl = psi;
						mainPartStackImpl.getParent().setSelectedElement(mainPartStackImpl);
						return;
					}
				}
			}
		}

		if (mainPartStackImpl == null) return;
		mainPartStackImpl.getParent().setSelectedElement(mainPartStackImpl);
	}

	/**
	 * Gets an editor by its editor input.
	 * 
	 * @param editorInput
	 * @return
	 * @throws PartInitException
	 */
	public static IEditorPart openEditor(IEditorInput editorInput, String editorId, boolean activate) throws PartInitException {

		return openEditor(editorInput, editorId, activate, IWorkbenchPage.MATCH_INPUT | IWorkbenchPage.MATCH_ID);
	}

	public static IEditorPart openEditor(IEditorInput editorInput, String editorId, boolean activate, int matchFlag) throws PartInitException {

		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window == null) return null;
		
		IWorkbenchPage page = window.getActivePage();
		IEditorReference[] editorsReferences = page.findEditors(editorInput, editorId, IWorkbenchPage.MATCH_INPUT | IWorkbenchPage.MATCH_ID);
		if (editorsReferences.length > 0) {
			IEditorPart editor = editorsReferences[0].getEditor(activate);
			editor.getSite().getPage().activate(editor);
			return editor;
		}
		else {
			selectMainPartStack();

			return page.openEditor(editorInput, editorId, activate, matchFlag);
		}
	}

	/**
	 * Gets an editor by its editor input.
	 * 
	 * @param editorInput
	 * @return
	 */
	public static IEditorPart getEditor(IEditorInput editorInput, String editorId) {

		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window == null) return null;
		
		IWorkbenchPage page = window.getActivePage();
		IEditorReference[] editorsReferences = page.findEditors(editorInput, editorId, IWorkbenchPage.MATCH_INPUT | IWorkbenchPage.MATCH_ID);
		if (editorsReferences.length > 0) {
			return editorsReferences[0].getEditor(false);
		}
		return null;
	}

	/**
	 * Gets an editor by its editor name.
	 * 
	 * @param name the name actually shown in the editor
	 * @return the first editor with the given name or null
	 */
	public static IEditorPart getEditor(String name) {

		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window == null) return null;
		
		IWorkbenchPage page = window.getActivePage();
		for (IEditorReference editor : page.getEditorReferences()) {
			if (editor.getName().equals(name)) {
				return editor.getEditor(false);
			}
		}

		return null;
	}

	/**
	 * Gets editors by their editor name.
	 * 
	 * @param name the name actually shown in the editor
	 * @return a list of editors
	 */
	public static ArrayList<IEditorPart> getEditors(String name) {

		ArrayList<IEditorPart> rez = new ArrayList<IEditorPart>();
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window == null) return null;
		
		IWorkbenchPage page = window.getActivePage();
		for (IEditorReference editor : page.getEditorReferences()) {
			if (editor.getName().equals(name)) {
				rez.add(editor.getEditor(false));
			}
		}

		return rez;
	}

	/**
	 * Put an editor in a CompositePart
	 * 
	 * @param parentEditor
	 * @param editorToAdd
	 * @param position
	 * @param percent The percentage of the area to be occupied by the inserted element
	 */
	public static MCompositePart boxEditor(EditorPart editor, EditorPart[] othersEditor) {

		return boxEditor(editor, othersEditor, false);
	}

	/**
	 * Put an editor in a CompositePart
	 * 
	 * @param parentEditor
	 * @param editorToAdd
	 * @param position
	 * @param percent The percentage of the area to be occupied by the inserted element
	 */
	public static MCompositePart boxEditor(EditorPart editor, EditorPart[] othersEditor, boolean vertical) {
		MPart[] otherParts = null;
		if (othersEditor != null) {
			otherParts = new MPart[othersEditor.length];
			for (int i = 0; i < othersEditor.length; i++) {
				otherParts[i] = othersEditor[i].getSite().getService(MPart.class);
			}
		}
		return boxMPart(editor.getSite().getService(MPart.class), otherParts, vertical);
	}

	/**
	 * Splits the parent area of an editor and add another one in this area by splitting it.
	 * (Same behavior than the splitting drag and drop event on editor.)
	 * EModelService.ABOVE, EModelService.BELOW, etc. can be used as position.
	 * 
	 * @param parentEditor
	 * @param editorToAdd
	 * @param position
	 * @param percent The percentage of the area to be occupied by the inserted element
	 */
	public static MCompositePart boxMPart(MPart mainPart, MPart[] otherParts, boolean vertical) {

		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window == null) return null;
		
		EModelService service = window.getService(EModelService.class);

		MUIElement parent = mainPart.getParent(); // Should be a PartStackImpl
		String label = mainPart.getLabel();
		MPartStack parentMPartStack = (MPartStack) parent;
		//MElementContainer<MUIElement> partStackContainer = parentMPartStack.getParent();

		int index = parentMPartStack.getChildren().indexOf(mainPart);

		// partstack containing the MCompositePart
		//		MPartStack boxPartStack = BasicFactoryImpl.eINSTANCE.createPartStack();
		MCompositePart box = BasicFactoryImpl.eINSTANCE.createCompositePart();
		box.setHorizontal(true); // TODO test not using a MPartSashContainer to store the secondary editors, MD I think the split editor needs a SashContainer
		MPartSashContainer boxPartSash = BasicFactoryImpl.eINSTANCE.createPartSashContainer();
		boxPartSash.setHorizontal(!vertical);

		parentMPartStack.getChildren().add(index, box);
		MPartStack firstPartStack = createPartStack(mainPart);

		//service.insert(previousStackToInsert, referenceMPartStack, 0, 0); // insert it into ui
		boxPartSash.getChildren().add(firstPartStack);
		boxPartSash.setSelectedElement(firstPartStack);
		//		
		MPartStack secondPartStack = BasicFactoryImpl.eINSTANCE.createPartStack();

		if (otherParts != null) {
			for (MPart otherPart : otherParts) {
				//MPartStack otherPartStack = createPartStack(otherPart); // replace the edior PartStack with a new one
				insertIntoPartStack(secondPartStack, otherPart);
			}
		}

		box.getChildren().add(boxPartSash);
		box.setSelectedElement(boxPartSash);

		box.setCloseable(true);
		mainPart.setCloseable(false);
		box.setLabel(label);
		box.setElementId(mainPart.getElementId());

		//		boxPartStack.getChildren().add(box);
		//		boxPartStack.setSelectedElement(box);

		return box;
	}

	/**
	 * Splits the parent area of an editor and add another one in this area by splitting it.
	 * (Same behavior than the splitting drag and drop event on editor.)
	 * EModelService.ABOVE, EModelService.BELOW, etc. can be used as position.
	 * 
	 * @param parentEditor
	 * @param editorToAdd
	 * @param position
	 * @param percent The percentage of the area to be occupied by the inserted element
	 */
	public static MPartStack moveEditor(EditorPart parentEditor, EditorPart editorToAdd, int position, float percent) {

		return moveMPart(parentEditor.getSite().getService(MPart.class), editorToAdd.getSite().getService(MPart.class), position, percent);
	}

	/**
	 * Splits the parent area of an editor and add another one in this area by splitting it by the middle.
	 * (Same behavior than the splitting drag and drop event on editor.)
	 * EModelService.ABOVE, EModelService.BELOW, etc. can be used as position.
	 * 
	 * @param parentEditor
	 * @param editorToAdd
	 * @param position
	 */
	public static MPartStack moveEditor(EditorPart parentEditor, EditorPart editorToMove, int position) {

		return moveEditor(parentEditor, editorToMove, position, 0.5f);
	}

	/**
	 * Splits the parent area of a part and add another one in this area by splitting it.
	 * (Same behavior than the splitting drag and drop event on editor.)
	 * EModelService.ABOVE, EModelService.BELOW, etc. can be used as position.
	 * 
	 * @param parentPart
	 * @param partToMove
	 * @param position
	 * @param percent The percentage of the area to be occupied by the inserted element
	 */
	public static MPartStack moveMPart(MPart parentPart, MPart partToMove, int position, float percent) {

		// FIXME: SJ: tests to open an editor in a View, to see if it's possible to use a View for the multipage editor (eg. for CA)
		// parent = service.getContainer(CorporaView.getInstance().getSite().getService(MPart.class));
		// parent.getParent().getChildren().add(partToAdd);
		// CTabFolder widget = (CTabFolder) parent.getWidget();
		// System.out.println("SWTEditorsUtils.addPart(): widget of Corpora view = " + widget);

		if (parentPart == null) return null;
		
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window == null) return null;
		
		EModelService service = window.getService(EModelService.class);

		
		
		MUIElement parent = parentPart.getParent(); // Should be a PartStackImpl
		MPartStack referenceMPartStack = (MPartStack) parent;
		MUIElement parent2 = partToMove.getParent();
		MPartStack previousStackToInsert = null;

		//		if (sashContainer instanceof PartStackImpl psi) {
		//			psi.getChildren();
		//			Object layoutParent = sashContainer.getParent();
		//			if (layoutParent instanceof org.eclipse.e4.ui.model.application.ui.basic.impl.PartSashContainerImpl psci) {
		//				
		//				int partPosition = psci.getChildren().indexOf(parent);
		//				System.out.println("partPosition="+partPosition);
		//				if (position == EModelService.RIGHT_OF && psci.getChildren().size() > partPosition + 1) {
		//					previousStackToInsert = (MPartStack) psci.getChildren().get(partPosition+1);
		//				} else if (position == EModelService.LEFT_OF && partPosition > 0) {
		//					previousStackToInsert = (MPartStack) psci.getChildren().get(partPosition-1);
		//				}
		//			}
		//		}
		//		System.out.println("CURRENT "+sashContainer);


		if (!layouts.containsKey(referenceMPartStack)) {
			//				System.out.println("INIT LAYOUT "+sashContainer);
			layouts.put(referenceMPartStack, new MPartStack[4]);
		}
		previousStackToInsert = layouts.get(referenceMPartStack)[position];

		if (previousStackToInsert == null || previousStackToInsert.getWidget() == null) { // not opened yet OR was closed

			//previousStackToInsert = createPartStack(partToMove); // create a new stack without any parent or configuration
			//service.insert(previousStackToInsert, referenceMPartStack, position, percent); // let Eclipse do its stuff

			// try to stack the 'partToMove' in the previous or next PartStack IF the SashContainer is in the right direction. This usually works BUT when drag&dropping editors Eclipse instead of adding PartStack to Sash often add the PartStack to the sash parent
			MUIElement sashContainer = referenceMPartStack.getParent();
			if (sashContainer instanceof PartSashContainerImpl sash) {
				
				int i = sash.getChildren().indexOf(referenceMPartStack);
				int size = sash.getChildren().size();
				if (sash instanceof AreaImpl area) { // AreaImpl has no spacer ??
					previousStackToInsert = createPartStack(partToMove); // create a new stack without any parent or configuration
					service.insert(previousStackToInsert, referenceMPartStack, position, percent); // insert it into ui
				}
				else if (sash.isHorizontal() && (position == EModelService.LEFT_OF || position == EModelService.RIGHT_OF)) { // the sash 
					if (position == EModelService.LEFT_OF) {
						if (i == 0) { // ok we can insert it before
							previousStackToInsert = createPartStack(partToMove); // create a new stack without any parent or configuration
							sash.getChildren().add(0, previousStackToInsert);
						}
						else { // stack to the previous PartStack
							MPartStack previousStack = (MPartStack) sash.getChildren().get(i - 1);
							previousStack.getChildren().add(partToMove); // no need to create a new PartStack
							previousStack.setSelectedElement(partToMove);
							previousStackToInsert = previousStack;
						}
					}
					else { // RIGHT_OF
						if (i == sash.getChildren().size() - 1) { // ok we can insert it after
							previousStackToInsert = createPartStack(partToMove); // create a new stack without any parent or configuration
							sash.getChildren().add(previousStackToInsert);
						}
						else { // stack to the next PartStack
							MPartStack nextStack = (MPartStack) sash.getChildren().get(i + 1);
							nextStack.getChildren().add(partToMove); // no need to create a new PartStack
							nextStack.setSelectedElement(partToMove);
							previousStackToInsert = nextStack;
						}
					}
				}
				else if (!sash.isHorizontal() && (position == EModelService.ABOVE || position == EModelService.BELOW)) {
					if (position == EModelService.ABOVE) {
						if (i == 0) { // ok we can insert it before
							previousStackToInsert = createPartStack(partToMove); // create a new stack without any parent or configuration
							sash.getChildren().add(0, previousStackToInsert);
						}
						else { // stack to the previous PartStack
							MPartStack previousStack = (MPartStack) sash.getChildren().get(i - 1);
							previousStack.getChildren().add(partToMove);
							previousStack.setSelectedElement(partToMove);
							previousStackToInsert = previousStack;
						}
					}
					else { // BELOW -> should be added after or stack to the next
						if (i == (sash.getChildren().size() - 1)) { // ok we can insert it after
							previousStackToInsert = createPartStack(partToMove); // create a new stack without any parent or configuration
							sash.getChildren().add(previousStackToInsert); // no need to create a new PartStack
						}
						else { // stack to the next PartStack
							MPartStack nextStack = (MPartStack) sash.getChildren().get(i + 1);
							nextStack.getChildren().add(partToMove); // no need to create a new PartStack
							nextStack.setSelectedElement(partToMove);
							previousStackToInsert = nextStack;
						}
					}
				}
				else { // this will create a new Sash
					previousStackToInsert = createPartStack(partToMove); // create a new stack without any parent or configuration
					service.insert(previousStackToInsert, referenceMPartStack, position, percent); // insert it into ui
				}
			}
			else if (sashContainer instanceof MArea area) { //MArea direction makes no sense /o\
				previousStackToInsert = createPartStack(partToMove); // create a new stack without any parent or configuration
				service.insert(area, referenceMPartStack, position, percent); // insert it into ui
			}
			else if (sashContainer instanceof MCompositePart compositePart) { //MArea direction makes no sense /o\
				//previousStackToInsert = createPartStack(partToMove); // create a new stack without any parent or configuration
				//service.insert(compositePart, referenceMPartStack, position, percent); // BREAKS ECLIPSE /o\
				return null;
			}

			layouts.get(referenceMPartStack)[position] = previousStackToInsert; // store for later
			//				System.out.println("SET "+sashContainer+" -"+position+"-> "+previousStackToInsert);

			parent2 = partToMove.getParent();
			if (parent2 == null) {
				Log.warning(NLS.bind(TXMUIMessages.cannotAddPartP0IntoP1, partToMove, parentPart));
				return null;
			}

			while (!(parent2 instanceof MPartSashContainerElement)) {
				parent2 = parent.getParent();
			}

			MPartSashContainerElement sashContainerRetour = (MPartSashContainerElement) parent2;
			if (!layouts.containsKey(sashContainerRetour)) {
				//					System.out.println("LAYOUT RETOUR"+sashContainerRetour);
				layouts.put(sashContainerRetour, new MPartStack[4]);
			}

			int positionRetour = 0; // compute the reverse position
			if (position == EModelService.ABOVE) positionRetour = EModelService.BELOW;
			else if (position == EModelService.RIGHT_OF) positionRetour = EModelService.LEFT_OF;
			else if (position == EModelService.LEFT_OF) positionRetour = EModelService.RIGHT_OF;
			else if (position == EModelService.BELOW) positionRetour = EModelService.ABOVE;

			layouts.get(sashContainerRetour)[positionRetour] = referenceMPartStack; // store for later
			//				System.out.println("SET "+sashContainerRetour+" -"+positionRetour+"-> "+parent2);
		}
		else {
			insertIntoPartStack(previousStackToInsert, partToMove); // only insert the editor in the given stack
		}

		return previousStackToInsert;
	}

	/**
	 * Creates a part stack for the specified part
	 * 
	 * @param part
	 * @return
	 */
	private static MPartStack createPartStack(MPart part) {

		// FIXME: SJ: tests to try to add some editor to view or parent shash container
		// IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		// EModelService service = window.getService(EModelService.class);
		// // MPerspective p = service.getActivePerspective(window);
		//
		// MPerspective p = service.getContainer(CorporaView.getInstance().getSite().getService(MPart.class));
		//
		// // part = CorporaView.getInstance().getSite().getService(MPart.class);

		MPartStack stack = BasicFactoryImpl.eINSTANCE.createPartStack();
		insertIntoPartStack(stack, part);

		return stack;
	}

	/**
	 * Creates a part stack for the specified part
	 * 
	 * @param part
	 * @return
	 */
	private static MPartStack insertIntoPartStack(MPartStack previousStackToInsert, MPart part) {

		previousStackToInsert.getChildren().add(part);
		previousStackToInsert.setSelectedElement(part);
		return previousStackToInsert;
	}

	/**
	 * Gets the Workbench pages.
	 *
	 * @return the pages
	 */
	public static IWorkbenchPage[] getPages() {

		ArrayList<IWorkbenchPage> pages = new ArrayList<IWorkbenchPage>();
		for (IWorkbenchWindow win : Workbench.getInstance().getWorkbenchWindows()) {
			for (IWorkbenchPage p : win.getPages()) {
				pages.add(p);
			}
		}
		return pages.toArray(new IWorkbenchPage[pages.size()]);
	}

	/**
	 * Open and activate an editor
	 * 
	 * @param editorInput
	 * @param id
	 * @return
	 * @throws PartInitException
	 */
	public static IEditorPart openEditor(IEditorInput editorInput, String id) throws PartInitException {

		return openEditor(editorInput, id, true);
	}
}
