package org.txm.rcp.editors.imports.sections;

import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.txm.objects.Project;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.txm.rcp.messages.TXMUIMessages;

public class InfosSection extends ImportEditorSection {

	private static final int SECTION_SIZE = 2;

	// Label corpusNameLabel; // color change if the corpus name is malformed
	private Text descText;

	public InfosSection(ImportFormEditor editor, FormToolkit toolkit2, ScrolledForm form2, Composite parent, int style) {
		super(editor, toolkit2, form2, parent, style, "infos"); //$NON-NLS-1$

		// this = toolkit.createSection(form.getBody(), ExpandableComposite.TITLE_BAR|ExpandableComposite.TWISTIE);
		this.section.setText(TXMUIMessages.description);
		section.setExpanded(true);

		TableWrapLayout layout = new TableWrapLayout();
		this.section.setLayout(layout);
		this.section.setLayoutData(getSectionGridData(SECTION_SIZE));
		this.section.setEnabled(false);

		this.section.addExpansionListener(new ExpansionAdapter() {

			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.layout(true);
			}
		});

		Composite sectionClient = toolkit.createComposite(this.section);
		TableWrapData gdatac = new TableWrapData();
		gdatac.colspan = 2;
		sectionClient.setLayoutData(gdatac);

		TableWrapLayout slayout = new TableWrapLayout();
		slayout.numColumns = 1;
		sectionClient.setLayout(slayout);
		this.section.setClient(sectionClient);

		// corpusNameLabel = toolkit.createLabel(sectionClient, TXMUIMessages.corpusNameOnlyCapsAndNoDigitAtBeginning, SWT.WRAP);
		// TableWrapData gdata = new TableWrapData();
		// corpusNameLabel.setLayoutData(gdata);
		//
		Label label = toolkit.createLabel(sectionClient, TXMUIMessages.completeNameAuthorDateLicenseComment, SWT.WRAP);
		TableWrapData gdata = new TableWrapData();
		label.setLayoutData(gdata);

		descText = toolkit.createText(sectionClient, "", SWT.BORDER | SWT.MULTI | SWT.V_SCROLL); //$NON-NLS-1$
		gdata = new TableWrapData(TableWrapData.FILL, TableWrapData.FILL);
		gdata.heightHint = 50;

		descText.setLayoutData(gdata);
	}

	@Override
	public void updateFields(Project project) {
		if (project.getSrcdir().exists()) {
			section.setText(NLS.bind(TXMUIMessages.descriptionOfTheP0CorpusP1, project.getName(), project.getSrcdir()));
		}
		else {
			section.setText(NLS.bind(TXMUIMessages.descriptionOfTheP0CorpusP1, project.getName(), "...")); //$NON-NLS-1$
		}

		String descContent = project.getDescription();
		descContent = descContent.replace("<pre><br/>", ""); //$NON-NLS-1$ //$NON-NLS-2$
		descContent = descContent.replace("</pre>", ""); //$NON-NLS-1$ //$NON-NLS-2$
		descContent = descContent.replace("<br/>", "\n"); //$NON-NLS-1$ //$NON-NLS-2$

		// System.out.println("LOAD DESC WITH: '"+descContent+"'");
		descText.setText(descContent);
	}

	@Override
	public boolean saveFields(Project project) {
		project.setDescription("<pre><br/>" + descText.getText().replace("\n", "<br/>") + "</pre>"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

		return true;
	}

	@Override
	public boolean checkFields() {
		// corpusNameLabel.setBackground(corpusNameLabel.getParent().getBackground());
		return true;
	}

	@Override
	public int getSectionSize() {
		return SECTION_SIZE;
	}
}
