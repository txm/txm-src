package org.txm.rcp.swt.widget.parameters;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.kohsuke.args4j.NamedOptionDef;

/**
 * Check button + Label
 * 
 * @author mdecorde
 *
 */
public class SeparatorField extends ParameterField {

	public SeparatorField(Composite parent, int style, NamedOptionDef parameter) {
		super(parent, style, parameter);
		this.setLayout(new GridLayout(2, false));
		this.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));

		String str = getWidgetDefault();

		if (str == null || str.length() == 0) {
			Label l = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
			GridData gd = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
			l.setLayoutData(gd);
			l.setText(str);
			l.setToolTipText(getWidgetUsage());
		}
		else {
			Label l = new Label(this, SWT.NONE);
			l.setAlignment(SWT.RIGHT);
			GridData gd = new GridData(SWT.FILL, SWT.FILL, false, false);
			l.setLayoutData(gd);
			l.setText(str);
			l.setToolTipText(getWidgetUsage());
			//			Font f = parent.getFont();
			//			l.setFont(new Font(Display.getCurrent(), new FontData(f.getFontData()[0].getName(), f.getFontData()[0].getHeight(), f.getFontData()[0].getStyle()|SWT.BOLD)));
			Label dt = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
			dt.setLayoutData(new GridData(SWT.FILL, SWT.END, true, false));
			dt.setToolTipText(getWidgetUsage());
		}
	}

	public Object getWidgetValue() {
		return null;
	}

	@Override
	public void setDefault(Object value) {

	}

	@Override
	public void resetToDefault() {
		// TODO Auto-generated method stub
	}
}
