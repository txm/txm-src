package org.txm.rcp.swt.dialog;

import java.net.URI;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ExpandBar;
import org.eclipse.swt.widgets.ExpandItem;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.internal.ide.ChooseWorkspaceData;
import org.txm.rcp.ApplicationWorkbenchAdvisor;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.SelectDirectoryWidget;

public class CGUMessageDialog extends Dialog {

	boolean checked = false;

	Button checkMessageButton = null;

	ChooseWorkspaceData launchData;

	private SelectDirectoryWidget w;

	protected CGUMessageDialog(Shell shell, ChooseWorkspaceData launchData) {
		super(shell);
		this.launchData = launchData;
	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(TXMUIMessages.bind(TXMUIMessages.firstLaunchOfTXMP0, ApplicationWorkbenchAdvisor.getTXMVersionPrettyString()));
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Control c = super.createDialogArea(parent);

		GridLayout parentLayout = (GridLayout) parent.getLayout();
		parentLayout.marginLeft = 5;
		parentLayout.marginRight = 5;
		Display display = Display.getCurrent();

		//new Label(messageComposite, SWT.NONE).setImage(IImageKeys.getImage(IImageKeys.TXM_ICON));
		Label logo = new Label(parent, SWT.NONE);
		logo.setImage(IImageKeys.getImage(IImageKeys.TXM_FLAT_LOGO));//new Image(display, "/home/mdecorde/workspace047/org.txm.rcp/icons/logo/TXM_logo_64x64.png"));
		logo.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));

		new Label(parent, SWT.NONE);

		Composite licenceGroup = new Composite(parent, SWT.NONE);
		//licenceGroup.setText("Licence d'utilisation/Licence agreement");
		licenceGroup.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		GridLayout glayout = new GridLayout(1, true);
		glayout.verticalSpacing = 0;
		licenceGroup.setLayout(glayout);

		Composite messageComposite = new Composite(licenceGroup, SWT.NONE);
		messageComposite.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		messageComposite.setLayout(new RowLayout());

		new Label(messageComposite, SWT.NONE).setText(TXMUIMessages.TXMisDistributedUnderLicence);

		final String gnuLicenseLink = TXMUIMessages.gnuLicenceLink;
		Label gnuLicense = new Label(messageComposite, SWT.NONE);
		gnuLicense.setText(TXMUIMessages.gnuLicenceName); // GNU GPL 2
		gnuLicense.setToolTipText(gnuLicenseLink);
		gnuLicense.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
				//OpenBrowser.openExternalBrowser(gnuLicenseLink);
				try {
					java.awt.Desktop.getDesktop().browse(new URI(gnuLicenseLink));
				}
				catch (Exception e1) {
					e1.printStackTrace();
				}
			}

			@Override
			public void mouseDown(MouseEvent e) {
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}
		});

		Color blue = display.getSystemColor(SWT.COLOR_BLUE);
		Cursor cursor = display.getSystemCursor(SWT.CURSOR_HAND);
		gnuLicense.setForeground(blue);
		gnuLicense.setCursor(cursor);

		checkMessageButton = new Button(licenceGroup, SWT.CHECK);
		checkMessageButton.setText(TXMUIMessages.iAcceptTheLicenceAndAgreeToQuoteTXMInMyWork);
		checkMessageButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				getButton(Window.OK).setEnabled(checkMessageButton.getSelection());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Composite seeComposite = new Composite(licenceGroup, SWT.NONE);
		seeComposite.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		seeComposite.setLayout(new RowLayout());

		new Label(seeComposite, SWT.NONE).setText(TXMUIMessages.seeTheQuoteSectionIn);

		final String txmDownloadLink = "http://www.textometrie.org"; //$NON-NLS-1$
		Label txmDownload = new Label(seeComposite, SWT.NONE);
		txmDownload.setText(txmDownloadLink);
		txmDownload.setToolTipText(txmDownloadLink);
		txmDownload.setForeground(blue);
		txmDownload.setCursor(cursor);
		txmDownload.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
				//OpenBrowser.openExternalBrowser(txmDownloadLink);
				try {
					java.awt.Desktop.getDesktop().browse(new URI(txmDownloadLink));
				}
				catch (Exception e1) {
					e1.printStackTrace();
				}
			}

			@Override
			public void mouseDown(MouseEvent e) {
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}
		});

		new Label(seeComposite, SWT.NONE).setText(")"); //$NON-NLS-1$

		ExpandBar bar = new ExpandBar(parent, SWT.V_SCROLL);
		GridData gdata = new GridData(GridData.FILL, GridData.FILL, true, true);
		bar.setLayoutData(gdata);

		String defaultPath = System.getProperty("user.home") + "/TXM"; //$NON-NLS-1$ //$NON-NLS-2$
		if (launchData != null) {
			defaultPath = launchData.getInitialDefault();
		}
		w = new SelectDirectoryWidget(bar, SWT.NONE, defaultPath, TXMUIMessages.selectWorkingDirectory);
		//w.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));

		// "Options"
		ExpandItem item1 = new ExpandItem(bar, SWT.NONE);
		item1.setText(TXMUIMessages.options);
		item1.setHeight(w.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
		item1.setControl(w);
		item1.setExpanded(false);
		gdata.heightHint = item1.getHeight() * 2;
		gdata.minimumHeight = item1.getHeight();
		//item1.setExpanded(false);
		//		Group dirGroup = new Group(parent, SWT.NONE);
		//		dirGroup.setText(NLS.bind(TXMUIMessages.selectWorkingDirectoryP0, launchData.getInitialDefault())); 
		//		dirGroup.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		//		dirGroup.setLayout(new GridLayout(1, true));
		//		


		return c;
	}

	@Override
	protected Control createButtonBar(Composite parent) {
		Control c = super.createButtonBar(parent);
		getButton(Window.OK).setEnabled(false);
		return c;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
	 */
	@Override
	protected void buttonPressed(int buttonId) {
		checked = checkMessageButton.getSelection();
		if (launchData != null) {
			launchData.workspaceSelected(w.getSelectedPath());
		}
		super.buttonPressed(buttonId);

	}

	public boolean hasCheckBeenPressed() {

		if (checkMessageButton != null && !checkMessageButton.isDisposed()) return checkMessageButton.getSelection();

		return checked;
	}

	public static void main(String[] args) {
		System.out.println("Opening CGU dialog..."); //$NON-NLS-1$
		Display display = new Display();
		Shell shell = new Shell(display);
		CGUMessageDialog dialog = new CGUMessageDialog(shell, null);
		dialog.open();
	}

	//	protected Control createMessageArea(Composite composite) {
	//		super.createMessageArea(composite);
	//
	//		// create checkMessage
	//		if (checkMessage != null) {
	//			checkMessageButton = new Button(composite, SWT.CHECK);
	//			checkMessageButton.setText(checkMessage);
	//			//System.out.println("set message: "+message);
	//			GridDataFactory
	//			.fillDefaults()
	//			.align(SWT.FILL, SWT.BEGINNING)
	//			.grab(true, false)
	//			.span(2, 2)
	//			.applyTo(messageLabel);
	//			
	//			checkMessageButton.addSelectionListener(new SelectionListener() {
	//				
	//				@Override
	//				public void widgetSelected(SelectionEvent e) {
	//					getButton(Window.OK).setEnabled(checkMessageButton.getSelection());
	//				}
	//				
	//				@Override
	//				public void widgetDefaultSelected(SelectionEvent e) { }
	//			});
	//		}
	//		
	//
	//		return composite;
	//	}
	//	

}
