// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.files;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.IOClipboard;
import org.txm.rcp.views.fileexplorer.Explorer;
import org.txm.rcp.views.fileexplorer.MacroExplorer;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * Execute a groovy script file in the same thread a the RCP @ author mdecorde.
 */
public class PasteFile extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			Object o = ((IStructuredSelection) selection).getFirstElement();

			if (o instanceof File) {
				paste((File) o);
			}
		}
		return null;
	}

	public static void paste(File folder) {

		File dest = null;

		if (folder.isFile()) folder = folder.getParentFile();
		if (folder.isDirectory() && folder.canWrite()) {
			if (CopyFile.toCopy != null) {
				File source = CopyFile.toCopy;
				if (source.isDirectory()) return; // don't copy directory

				dest = new File(folder, source.getName());
				while (dest.exists()) {
					dest = new File(folder, "copy_" + dest.getName()); //$NON-NLS-1$
				}
				try {
					if (!org.txm.utils.io.FileCopy.copy(source, dest))
						System.out.println(TXMUIMessages.failedToCopy + source + TXMUIMessages.to + dest);
				}
				catch (IOException e) {
					System.out.println(TXMUIMessages.failedToCopy + source + TXMUIMessages.to + dest + TXMUIMessages.dDot + e);
				}
			}
			else if (CutFile.toCut != null) {
				File source = CutFile.toCut;
				if (source.isDirectory()) return; // don't move directory
				dest = new File(folder, source.getName());
				while (dest.exists()) {
					dest = new File(folder, "copy_" + dest.getName()); //$NON-NLS-1$
				}
				try {
					if (!org.txm.utils.io.FileCopy.copy(source, dest))
						System.out.println(TXMUIMessages.failedToCopy + source + TXMUIMessages.to + dest);
					if (!source.delete())
						System.out.println(TXMUIMessages.failedToDelete + source);
					CutFile.toCut = null;
				}
				catch (IOException e) {
					System.out.println(TXMUIMessages.failedToCopy + source + TXMUIMessages.to + dest + TXMUIMessages.dDot + e);
				}
			}
			else {
				// check clipboard content
				String[] paths = IOClipboard.readFiles();
				if (paths == null) return;
				Log.info(TXMUIMessages.bind(TXMUIMessages.CopingFilesP0, StringUtils.join(Arrays.asList(paths), ", "))); //$NON-NLS-1$
				for (String path : paths) {
					File source = new File(path);
					dest = new File(folder, source.getName());
					while (dest.exists()) {
						dest = new File(folder, "copy_" + dest.getName()); //$NON-NLS-1$
					}

					try {
						if (source.isDirectory()) {
							org.txm.utils.io.FileCopy.copyFiles(source, dest);
						}
						else {
							if (!org.txm.utils.io.FileCopy.copy(source, dest))
								System.out.println(TXMUIMessages.failedToCopy + source + TXMUIMessages.to + dest);
						}

					}
					catch (IOException e) {
						System.out.println(TXMUIMessages.failedToCopy + source + TXMUIMessages.to + dest + TXMUIMessages.dDot + e);
					}
				}
			}
			CopyFile.toCopy = null;
			CutFile.toCut = null;
			Explorer.refresh(dest);
			MacroExplorer.refresh(dest);
		}
	}
}
