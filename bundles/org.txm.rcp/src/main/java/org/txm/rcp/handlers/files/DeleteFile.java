// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.files;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.Toolbox;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.views.fileexplorer.Explorer;
import org.txm.rcp.views.fileexplorer.MacroExplorer;
import org.txm.utils.DeleteDir;


/**
 * Delete file command
 * 
 * @author mdecorde.
 */
public class DeleteFile extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			Object o = ((IStructuredSelection) selection).getFirstElement();

			if (o instanceof File) {
				File file = (File) o;
				boolean definitive = true; // (event.stateMask & SWT.SHIFT) != 0;
				delete(file, definitive);
			}
		}
		return null;
	}

	/**
	 * open a dialog box to confirm and update views
	 * 
	 * @param file the file or directory to delete
	 */
	public static void delete(File file, boolean definitive) {

		delete(Arrays.asList(file), definitive);
	}

	/**
	 * open a dialog box to confirm and update views
	 * 
	 * @param files the files or directories to delete
	 * @param definitive definitively delete the files FIXME NOT IMPLEMENTED YET
	 */
	public static void delete(List<?> files, boolean definitive) {

		String rootPath = Toolbox.getTxmHomePath();

		Shell shell = new Shell();
		MessageBox dialog = new MessageBox(shell, SWT.YES | SWT.NO);
		dialog.setMessage(TXMUIMessages.bind(TXMUIMessages.areYouSureYouWantToDeleteP0Colon, files));

		int buttonID = dialog.open();
		switch (buttonID) {
			case SWT.YES:
				for (Object o : files) {
					if (o instanceof File) {
						File file = (File) o;

						if (!file.getAbsolutePath().startsWith(rootPath)) {
							System.out.println(TXMUIMessages.txmWontDeleteFilesOutOfTXMWorkingDirectory + file);
							continue;
						}

						if (file.exists()) {
							if (file.isFile()) {
								if (definitive) {
									if (!file.delete()) {
										System.out.println(NLS.bind("", file)); //$NON-NLS-1$
									}
								}
								else {
									File dest = new File(System.getProperty("java.io.tmpdir"), file.getName()); //$NON-NLS-1$
									dest.delete();
									if (!file.renameTo(dest)) {
										System.out.println(NLS.bind(TXMUIMessages.failToMoveTheP0FileToThxTMPDirectoryP1, file, dest));
									}
								}
							}
							else if (file.isDirectory()) {
								if (!DeleteDir.deleteDirectory(file, definitive)) {
									System.out.println(NLS.bind("", file)); //$NON-NLS-1$
								}
							}
						}
					}
				}
				Explorer.refresh();
				MacroExplorer.refresh();
			case SWT.NO:
				// exits here ...
				break;
			case SWT.CANCEL:
				// does nothing ...
		}
	}
}
