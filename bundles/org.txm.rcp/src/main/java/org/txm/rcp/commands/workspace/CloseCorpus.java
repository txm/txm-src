package org.txm.rcp.commands.workspace;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.objects.Project;
import org.txm.rcp.commands.CloseEditorsUsing;
import org.txm.rcp.commands.RestartTXM;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

public class CloseCorpus extends AbstractHandler {

	public static final String ID = CloseCorpus.class.getCanonicalName();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection sel = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		if (!(sel instanceof IStructuredSelection)) return null;
		IStructuredSelection selection = (IStructuredSelection) sel;
		ArrayList<MainCorpus> corpora = new ArrayList<MainCorpus>();
		for (Object s : selection.toArray()) {
			if (s instanceof MainCorpus) {
				corpora.add((MainCorpus) s);
			}
		}

		close(corpora);

		return corpora;
	}

	public static JobHandler close(final MainCorpus corpus) {
		ArrayList<MainCorpus> l = new ArrayList<MainCorpus>();
		l.add(corpus);
		return close(l);
	}

	public static JobHandler close(final List<MainCorpus> corpora) {

		for (final MainCorpus corpus : corpora) {
			final Project project = corpus.getProject();
			if (project == null) return null;

			Display d = Display.getCurrent();
			if (d == null) {
				d = Display.getDefault();
			}

			if (d != null) {
				d.syncExec(new Runnable() {

					@Override
					public void run() {
						CloseEditorsUsing.corpus(corpus);
					}
				});
			}
		}
		JobHandler job = new JobHandler(TXMUIMessages.bind(TXMUIMessages.ClosingTheP0Corpus, corpora)) {

			@Override
			protected IStatus _run(SubMonitor monitor) {

				try {
					for (final MainCorpus corpus : corpora) {
						corpus.getProject().close(monitor);
					}

					this.syncExec(new Runnable() {

						@Override
						public void run() {
							RestartTXM.reloadViews();
						}
					});
				}
				catch (ThreadDeath ex) {
					Log.info(TXMUIMessages.executionCanceled);
					return Status.CANCEL_STATUS;
				}
				return Status.OK_STATUS;
			}
		};

		job.schedule();

		return job;
	}
}
