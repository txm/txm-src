// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.rcp.adapters;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.txm.objects.Laboratory;
import org.txm.objects.Project;
import org.txm.rcp.Application;
import org.txm.rcp.IImageKeys;


/**
 * A factory for creating Adapter objects.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class ProjectAdapterFactory extends TXMResultAdapterFactory {

	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (adapterType == IWorkbenchAdapter.class && adaptableObject instanceof Project) {
			return new TXMResultAdapter() {

				@Override
				public Object[] getChildren(Object o) {
					Project p = (Project) o;
					if (p.isOpen()) {
						return p.getCorpora().toArray();
					}

					return new Object[0];
				}

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, IImageKeys.PROJECT);
				}

				@Override
				public String getLabel(Object o) {
					return ((Project) o).getName();
				}
			};
		}
		else if (adapterType == IWorkbenchAdapter.class && adaptableObject instanceof Laboratory) {
			return new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, IImageKeys.LABORATORY);
				}

			};
		}
		return null;
	}
}
