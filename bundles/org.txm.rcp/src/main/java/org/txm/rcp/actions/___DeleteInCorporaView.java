// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Subcorpus;

// TODO: Auto-generated Javadoc
/**
 * This class manage the deletion of nodes in the tree of the corpora view.
 *
 * @author jmague
 */
//FIXME: this class is not used anymore?
public class ___DeleteInCorporaView extends Action implements ISelectionListener,
		IWorkbenchAction {

	/** The ID. */
	public static String ID = "org.txm.rcp.actions.DeleteInCorporaView"; //$NON-NLS-1$

	/** The selection. */
	private IStructuredSelection selection;

	/**
	 * Check classof selection.
	 *
	 * @return true, if successful
	 */
	private boolean checkClassofSelection() {
		boolean b = (selection.size() == 1 && (selection.getFirstElement() instanceof Subcorpus || selection
				.getFirstElement() instanceof Partition));
		return b;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection incoming) {
		// Selection containing elements
		if (incoming instanceof IStructuredSelection) {
			selection = (IStructuredSelection) incoming;
			setEnabled(checkClassofSelection());
		}
		else {
			// Other selections, for example containing text or of other kinds.
			setEnabled(false);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.actions.ActionFactory.IWorkbenchAction#dispose()
	 */
	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		if (selection.getFirstElement() instanceof Subcorpus) {
			Subcorpus subcorpus = (Subcorpus) selection.getFirstElement();
			try {
				subcorpus.getMainCorpus().dropSubcorpus(subcorpus);
			}
			catch (CqiClientException e) {
				System.err.println(NLS.bind(TXMUIMessages.failedToDeleteSubcorpusP0, e));
			}
		}
		if (selection.getFirstElement() instanceof Partition) {
			Partition partition = (Partition) selection.getFirstElement();
			//			try {
			//				partition.getParent().dropPartition(partition);
			//			} catch (CqiClientException e) {
			//				System.err.println(NLS.bind(
			//						TXMUIMessages.DeleteInCorporaView_2, e));
			//			}
		}
	}
}
