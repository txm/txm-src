package org.txm.rcp.swt.provider;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.graphics.Image;
import org.w3c.dom.Element;

public class ElementColumnLabelProvider extends ColumnLabelProvider {

	String key;

	public ElementColumnLabelProvider(String key) {
		this.key = key;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
	 * 
	 * @param element
	 * @param columnIndex
	 */
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	@Override
	public String getText(Object element) {
		if (element != null && element instanceof Element) {
			Element elem = (Element) element;
			return elem.getAttribute(key);
		}
		return "#no Element"; //$NON-NLS-1$
	}
}
