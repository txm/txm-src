package org.txm.rcp.swt.widget.preferences;

import org.eclipse.swt.widgets.Composite;

public class FileFieldEditor extends org.eclipse.jface.preference.FileFieldEditor {
	
	Composite parent;
	
	public FileFieldEditor(String name, String label, Composite parent) {
		super(name, label, parent);
		this.parent = parent;
	}

	public void setToolTipText(String tooltip) {
		getTextControl(parent).setToolTipText(tooltip);
		getLabelControl(parent).setToolTipText(tooltip);
	}
	
	public void setEnabled(boolean b) {
		
		super.setEnabled(b, parent);
	}
}
