package org.txm.rcp.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;

/**
 * Use this to install an item via contribution. You'll need to add SelectionListener on the item to do something.
 * 
 * retieve the item : this.topToolBar.getItemByContributionId(TOP_TOOLBAR_COMPUTE_BUTTON_ID);
 * 
 * @author mdecorde
 *
 */
public class DoNothingCommand extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) {

		return null;
	}
}
