// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands;

import java.io.File;
import java.net.URL;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.internal.browser.BrowserViewer;
import org.eclipse.ui.internal.browser.WebBrowserEditorInput;
import org.txm.Toolbox;
import org.txm.rcp.editors.TXMBrowserEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.utils.logger.Log;

/**
 * Open the internal browser of TXM with a file @ author mdecorde.
 */
public class OpenBrowser extends AbstractHandler {

	/** The Constant ID. */
	public final static String ID = "org.txm.rcp.commands.OpenBrowser"; //$NON-NLS-1$

	/** The lastopenedfile. */
	public static String lastopenedfile;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event)
				.getShell();

		String filename = event.getParameter("org.txm.rcp.commands.commandParameter2"); //$NON-NLS-1$
		if (filename != null && !filename.matches("") && !filename.matches(" ")) { //$NON-NLS-1$ //$NON-NLS-2$
			openfile(filename);
			return null;
		}

		File file = null;
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (Object next : strucSelection.toList()) {
				if (next instanceof File) {
					file = (File) next;
					break;
				}
			}
		}

		if (file == null) {

			FileDialog dialog = new FileDialog(shell, SWT.OPEN);
			String txmhome = Toolbox.getTxmHomePath();
			if (LastOpened.getFile(ID) != null) {
				dialog.setFilterPath(LastOpened.getFolder(ID));
				dialog.setFileName(LastOpened.getFile(ID));
			}
			else {
				dialog.setFilterPath(txmhome);
			}
			if (dialog.open() != null) {
				String filepath = dialog.getFilterPath() + "/" + dialog.getFileName(); //$NON-NLS-1$
				LastOpened.set(ID, dialog.getFilterPath(), dialog.getFileName());
				if (dialog.getFileName().length() > 0) {
					lastopenedfile = filepath;
					return openfile(filepath);
				}
			}
		}
		else {
			if (file.isDirectory()) {
				FileDialog dialog = new FileDialog(shell, SWT.OPEN);
				dialog.setFilterPath(file.getAbsolutePath());
				if (dialog.open() == null) return null;
				String filepath = dialog.getFilterPath() + "/" + dialog.getFileName(); //$NON-NLS-1$
				LastOpened.set(ID, dialog.getFilterPath(), dialog.getFileName());
				lastopenedfile = filepath;
				return openfile(filepath);
			}
			else
				return openfile(file.getAbsolutePath());
		}
		return null;
	}

	/**
	 * Openfile.
	 *
	 * @param filepath the filepath
	 * @return the txm browser
	 */
	static public TXMBrowserEditor openEdition(String filepath) {
		return openEdition(filepath, new File(filepath).getName());
	}

	/**
	 * Openfile.
	 *
	 * @return the txm browser
	 */
	static public TXMBrowserEditor open() {
		return openfile("", null); //$NON-NLS-1$
	}

	/**
	 * Openfile.
	 *
	 * @param filepath the filepath
	 * @return the txm browser
	 */
	static public TXMBrowserEditor openfile(String filepath) {
		return openfile(filepath, new File(filepath).getName());
	}

	static public void openExternalBrowser(String url) {
		try {
			PlatformUI.getWorkbench().getBrowserSupport().getExternalBrowser().openURL(new URL(url));
		}
		catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * Open a file given a path in a Web editor.
	 *
	 * @param filepath the filepath
	 * @param editorname the editorname
	 * @return the TXM browser
	 */
	public static TXMBrowserEditor openEdition(String filepath, String editorname) {
		try {

			File localfile = new File(filepath);
			URL url = null;
			if (filepath == null || filepath.length() == 0) {

			}
			else if (localfile.exists()) {
				url = localfile.toURI().toURL();
			}
			else {
				url = new URL(filepath);

				//url = URLUtils.encodeURL(url);
			}

			WebBrowserEditorInput editorInput = new WebBrowserEditorInput(url, BrowserViewer.BUTTON_BAR | BrowserViewer.LOCATION_BAR, TXMBrowserEditor.ID);

			IEditorPart editor = SWTEditorsUtils.getEditor(editorInput, TXMBrowserEditor.ID);
			if (editor == null) {

				IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				IWorkbenchPage page = window.getActivePage();

				TXMBrowserEditor editor2 = (TXMBrowserEditor) page.openEditor(editorInput, TXMBrowserEditor.ID, true);
				if (editorname != null) {
					editor2.setPartName(editorname);
				}
				return editor2;
			}
			else {
				editor.getEditorSite().getPage().activate(editor);
			}
		}
		catch (Exception e) {
			Log.warning(TXMUIMessages.bind(TXMUIMessages.errorWhileOpeningBibliographicRecordP0WithURLEqualsP1, e.toString(), filepath));
			Log.printStackTrace(e);
		}
		return null;
	}

	/**
	 * Openfile.
	 *
	 * @param filepath the filepath
	 * @param editorname the editorname
	 * @return the txm browser
	 */
	public static TXMBrowserEditor openfile(String filepath, String editorname) {
		try {
			return openEdition(filepath, editorname);
		}
		catch (Exception e) {
			Log.severe(TXMUIMessages.bind(TXMUIMessages.errorWhileOpeningBibliographicRecordP0WithURLEqualsP1, e.toString(), filepath));
			System.out.println(TXMUIMessages.bind(TXMUIMessages.errorWhileOpeningBibliographicRecordP0WithURLEqualsP1, e.toString(), filepath));
		}
		return null;
	}
}
