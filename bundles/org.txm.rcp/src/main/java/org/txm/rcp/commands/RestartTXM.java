// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.ui.PlatformUI;
import org.txm.Toolbox;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.SummaryView;
import org.txm.rcp.views.fileexplorer.MacroExplorer;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * restart TXM: restart searchengine, statengine, workspace...
 * 
 * @author mdecorde
 * 
 */
public class RestartTXM extends AbstractHandler {

	/** The ID. */
	static public String ID = "org.txm.rcp.commands.RestartTXM"; //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!JobHandler.areAllTXMJobsDone()) {
			Log.warning(TXMUIMessages.WaitForAllJobsToEndAndRetry);
			return null;
		}

		JobHandler jobhandler = new JobHandler(TXMUIMessages.RestartingTXM) {

			@Override
			protected IStatus _run(SubMonitor monitor) throws Exception {

				monitor.beginTask(TXMUIMessages.restartTXM, 100);
				// Toolbox.updateProperties(TxmPreferences.getProperties());
				if (Toolbox.restart()) {
					Log.info(TXMUIMessages.info_txmIsReady);
				}
				else {
					Log.warning(TXMUIMessages.TXMWasNotRestartedNormally);
				}

				monitor.worked(95);
				syncExec(new Runnable() {

					@Override
					public void run() {
						reloadViews();
					}
				});

				return Status.OK_STATUS;
			}
		};
		jobhandler.startJob();
		return event;
	}

	public static void reloadViews() {

		if (!PlatformUI.isWorkbenchRunning()) return;

		// System.out.println(Messages.RestartTXM_0);
		// TODO TXM views must have been registered to be reloaded

		org.txm.rcp.views.corpora.CorporaView.reload();
		org.txm.rcp.views.corpora.CorporaView.refresh();
		MacroExplorer.refresh();
		org.txm.rcp.views.BasesView.reload();
		org.txm.rcp.views.BasesView.refresh();
		org.txm.rcp.views.QueriesView.reload();
		org.txm.rcp.views.QueriesView.refresh();
		SummaryView.reload();
		// System.err.println(Messages.AddBase_10);
		// StatusLine.setMessage(Messages.AddBase_10);
	}
}
