package org.txm.rcp.swt;

import java.util.HashMap;

import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Display;

/**
 * Stores SWT Font to be disposed later and avoid leaks
 * 
 * @author mdecorde
 *
 */
public class TXMFontRegistry {

	static HashMap<FontData, Font> fonts = new HashMap<>();

	static HashMap<FontData[], Font> fonts2 = new HashMap<>();

	public static final Font getFont(Display display, FontData fd) {
		if (fonts.containsKey(fd)) return fonts.get(fd);

		Font f = new Font(display, fd);
		fonts.put(fd, f);
		return f;
	}

	public static final Font getFont(Display display, FontData[] fd) {
		if (fonts2.containsKey(fd)) return fonts.get(fd);

		Font f = new Font(display, fd);
		fonts2.put(fd, f);
		return f;
	}

	public static final Font getFont(Display display, String name, int height, int style) {
		return getFont(display, new FontData(name, height, style));
	}

	public static final Font getFont(String name, int height, int style) {
		return getFont(new FontData(name, style, style));
	}

	public static final Font getFont(FontData fd) {
		return getFont(Display.getCurrent(), fd);
	}

	public static Font getFontFromAWTFont(java.awt.Font f) {
		return TXMFontRegistry.getFont(f.getFamily(), f.getSize(), f.getStyle());
	}
	
	public static java.awt.Font getAWTFontFromSWTFont(Font f) {
		return getAWTFontFromSWTFont(f.getFontData()[0]);
	}
	
	public static java.awt.Font getAWTFontFromSWTFont(FontData fdata) {
		return new java.awt.Font(fdata.getName(), fdata.getStyle(), fdata.getHeight());
	}
	
	public static void dispose() {

		try {
			for (Font f : fonts.values())
				f.dispose();
			for (Font f : fonts2.values())
				f.dispose();
		}
		catch (Exception e) {

		}
	}
}
