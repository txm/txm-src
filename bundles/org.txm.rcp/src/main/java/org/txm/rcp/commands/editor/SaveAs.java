// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands.editor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.editors.TxtEditor;
import org.txm.rcp.handlers.files.EditFile;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.views.fileexplorer.Explorer;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * Execute a groovy script file in the same thread a the RCP @ author mdecorde.
 */
public class SaveAs extends AbstractHandler {

	private static final String ID = "org.txm.rcp.commands.editor.SaveAs"; //$NON-NLS-1$

	private static String previousPath;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		// the file to execute
		IWorkbenchPart part = HandlerUtil.getActiveWorkbenchWindow(event)
				.getActivePage().getActivePart();
		if (part instanceof TxtEditor) {
			TxtEditor editor = (TxtEditor) part;
			String text = editor.getTextViewer().getTextWidget().getText();

			Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event)
					.getShell();


			FileDialog dialog = new FileDialog(shell, SWT.SAVE);
			if (LastOpened.getFile(ID) != null) {
				dialog.setFilterPath(LastOpened.getFolder(ID));
				dialog.setFileName(LastOpened.getFile(ID));
			}
			String result = dialog.open();
			if (result == null) return null;
			File resultFile = new File(result);
			LastOpened.set(ID, resultFile.getParent(), resultFile.getName());
			if (resultFile != null && resultFile.getParentFile().exists()) {
				previousPath = result;
				try {
					OutputStreamWriter output = new OutputStreamWriter(new FileOutputStream(resultFile), "UTF-8"); //$NON-NLS-1$
					output.write(text);
					output.close();

					EditFile.openfile(resultFile);
					Explorer.refresh();
				}
				catch (Exception e) {
					Log.severe(TXMCoreMessages.bind(TXMUIMessages.failedToSaveAsP0ErrorP1, resultFile, e));
				}
			}
		}
		return null;
	}
}
