// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.editors.adaptableDataStructure;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.ui.IPersistableElement;
import org.txm.rcp.editors.ITablableEditorInput;
import org.txm.rcp.messages.TXMUIMessages;

// TODO: Auto-generated Javadoc
/**
 * The Class TablableVector.
 */
public class TablableVector implements ITablableEditorInput {

	/** The column titles. */
	String[] columnTitles;

	/** The column types. */
	String[] columnTypes;

	/**
	 * Instantiates a new tablable vector.
	 *
	 * @param freq the freq
	 * @param forms the forms
	 * @param columnTitles the column titles
	 * @param columnTypes the column types
	 */
	public TablableVector(int[] freq, String[] forms, String[] columnTitles,
			String[] columnTypes) {
		this.columnTitles = columnTitles;
		this.columnTypes = columnTypes;
		if (columnTitles.length != columnTypes.length)
			throw new IllegalArgumentException(TXMUIMessages.theTwoArraysMustHaveTheSameSize);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.rcp.editors.ITablableEditorInput#getColumnTitles()
	 */
	@Override
	public String[] getColumnTitles() {
		return columnTitles;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.rcp.editors.ITablableEditorInput#getContentProvider()
	 */
	@Override
	public IStructuredContentProvider getContentProvider() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.rcp.editors.ITablableEditorInput#getInput()
	 */
	@Override
	public Object getInput() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.rcp.editors.ITablableEditorInput#getLabelProvider()
	 */
	@Override
	public LabelProvider getLabelProvider() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#exists()
	 */
	@Override
	public boolean exists() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getImageDescriptor()
	 */
	@Override
	public ImageDescriptor getImageDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getName()
	 */
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getPersistable()
	 */
	@Override
	public IPersistableElement getPersistable() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getToolTipText()
	 */
	@Override
	public String getToolTipText() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@Override
	public Object getAdapter(@SuppressWarnings("rawtypes") Class adapter) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.rcp.editors.ITablableEditorInput#getColumnTypes()
	 */
	@Override
	public String[] getColumnTypes() {
		return columnTypes;
	}

}
