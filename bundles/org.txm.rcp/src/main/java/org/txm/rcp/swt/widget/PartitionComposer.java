// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.widget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.widget.structures.PartitionParameterGenerator;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.SubcorpusCQLQuery;

// TODO: Auto-generated Javadoc
/**
 * used for assisted mode of partition creation @ author mdecorde.
 */
public class PartitionComposer extends Composite implements PartitionParameterGenerator {

	/** The struct combo. */
	Combo structCombo;

	/** The prop combo. */
	Combo propCombo;

	/** The available list. */
	org.eclipse.swt.widgets.List availableList;

	/** The parts. */
	List<PartItem> parts = new ArrayList<>();

	/** The corpus. */
	CQPCorpus corpus;

	/** The structure. */
	StructuralUnit structure;

	/** The property. */
	StructuralUnitProperty property;

	/** The btn area. */
	Composite parent, partArea, firstArea, dispoArea, btnArea;

	private ScrolledComposite sc1;

	/**
	 * Instantiates a new partition composer.
	 *
	 * @param parent the parent
	 * @param style the style
	 * @param corpus the corpus
	 */
	public PartitionComposer(Composite parent, int style, CQPCorpus corpus) {

		super(parent, style);
		this.setLayout(new GridLayout(1, true));
		this.corpus = corpus;

		firstArea = new Composite(this, SWT.NONE);
		firstArea.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		dispoArea = new Composite(this, SWT.NONE);
		dispoArea.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		btnArea = new Composite(this, SWT.NONE);
		btnArea.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));

		sc1 = new ScrolledComposite(this, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		sc1.setLayout(new GridLayout(1, true));
		sc1.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		sc1.setExpandHorizontal(true);
		sc1.setExpandVertical(true);
		sc1.setMinSize(0, 250);

		partArea = new Composite(sc1, SWT.NONE);
		sc1.setContent(partArea);
		partArea.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		// struct&prop
		RowLayout layout = new RowLayout();
		layout.center = true;
		firstArea.setLayout(layout);
		Label structLabel = new Label(firstArea, SWT.NONE);
		structLabel.setText(TXMUIMessages.structureColon);
		structCombo = new Combo(firstArea, SWT.READ_ONLY | SWT.SINGLE);
		try {
			List<StructuralUnit> sus = corpus.getOrderedStructuralUnits();
			Collections.sort(sus);
			for (StructuralUnit su : sus) {
				if (su.getName().equals("txmcorpus")) continue; //$NON-NLS-1$
				structCombo.add(su.getName());
			}
			getStructure();
		}
		catch (CqiClientException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return;
		}

		if (structCombo.getItemCount() > 0) {
			structCombo.select(0);
			String[] items = structCombo.getItems();
			for (int i = 0; i < items.length; i++)
				if (items[i].equals("text")) //$NON-NLS-1$
				{
					structCombo.select(i);
					break;
				}
		}

		structCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				reloadProperties();
				updateDisplay();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Label propLabel = new Label(firstArea, SWT.NONE);
		propLabel.setText(TXMCoreMessages.common_property);

		propCombo = new Combo(firstArea, SWT.READ_ONLY | SWT.SINGLE);
		reloadProperties();

		propCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				reloadPropertyValues();
				updateDisplay();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// dispo
		GridLayout layout2 = new GridLayout(2, false);
		dispoArea.setLayout(layout2);

		Label dispoLabel = new Label(dispoArea, SWT.NONE);
		dispoLabel.setText(TXMUIMessages.selectValuesToAssignColon);
		dispoLabel.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));

		availableList = new org.eclipse.swt.widgets.List(dispoArea, SWT.MULTI | SWT.READ_ONLY | SWT.V_SCROLL | SWT.BORDER);
		GridData data = new GridData(GridData.FILL, GridData.FILL, true, true);
		data.heightHint = 10 * availableList.getItemHeight();
		availableList.setLayoutData(data);
		reloadPropertyValues();

		// btn
		btnArea.setLayout(new RowLayout());
		Button addPart = new Button(btnArea, SWT.PUSH);
		addPart.setText(TXMUIMessages.newPart);
		addPart.setToolTipText("Add a new part");
		addPart.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				// System.out.println(Messages.PartitionComposer_4);
				PartItem p = new PartItem(partArea, SWT.BORDER, PartitionComposer.this);
				GridData gdata = new GridData(GridData.FILL, GridData.FILL, true, true);
				gdata.minimumHeight = 200;
				p.setLayoutData(gdata);
				p.setTitle(TXMUIMessages.part + (parts.size() + 1));
				parts.add(p);
				partArea.layout(true);
				updateDisplay();
				sc1.setMinSize(partArea.computeSize(SWT.DEFAULT, SWT.DEFAULT));
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		Button rmvPart = new Button(btnArea, SWT.PUSH);
		rmvPart.setText(TXMUIMessages.rmvAllTheParts);
		rmvPart.setToolTipText("Remove all parts");
		rmvPart.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				for (PartItem p : parts) {
					for (String s : p.getColumns())
						PartitionComposer.this.add(s);
					p.dispose();
				}
				availableList.update();
				parts.clear();
				updateDisplay();
				sc1.setMinSize(partArea.computeSize(SWT.DEFAULT, SWT.DEFAULT));
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		// parts
		partArea.setLayout(new GridLayout(3, false));
		PartItem p = new PartItem(partArea, SWT.BORDER, this);
		GridData gdata = new GridData(GridData.FILL, GridData.FILL, true, true);
		gdata.minimumHeight = 200;
		p.setLayoutData(gdata);
		p.setTitle(TXMUIMessages.part + (parts.size() + 1));
		parts.add(p);

	}

	/**
	 * Update display.
	 */
	public void updateDisplay() {

		firstArea.layout();
		dispoArea.layout();
		partArea.layout();
		btnArea.layout();
		super.getParent().layout();
		super.getParent().getParent().layout();
	}

	/**
	 * Reload properties.
	 */
	protected void reloadProperties() {

		if (propCombo == null) return;

		propCombo.removeAll();
		StructuralUnit su = getStructure();
		List<StructuralUnitProperty> sups = su.getOrderedProperties();
		Collections.sort(sups);
		for (StructuralUnitProperty sup : sups) {
			if (su.getName().equals("text") && (sup.getName().equals("base") || sup.getName().equals("project"))) continue; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

			propCombo.add(sup.getName());
		}

		if (propCombo.getItemCount() > 0) {
			propCombo.select(0);
			String[] items = propCombo.getItems();
			for (int i = 0; i < items.length; i++)
				if (items[i].equals("id")) { //$NON-NLS-1$
					propCombo.select(i);
					break;
				}
		}
		propCombo.layout();

		reloadPropertyValues();
	}

	/**
	 * Removes the.
	 *
	 * @param values the values
	 */
	protected void remove(List<String> values) {

		for (String s : values)
			this.availableList.remove(s);
		availableList.update();
	}

	/**
	 * Adds the.
	 *
	 * @param s the s
	 */
	protected void add(String s) {

		this.availableList.add(s);
		availableList.update();
	}

	/**
	 * Reload property values.
	 */
	private void reloadPropertyValues() {

		if (availableList == null) return;

		String[] items;
		availableList.removeAll();
		try {
			List<String> plist = getProperty().getOrderedValues(this.corpus);
			Collections.sort(plist);
			items = new String[plist.size()];
			for (int i = 0; i < plist.size(); i++) {
				items[i] = plist.get(i);
			}
		}
		catch (CqiClientException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return;
		}

		availableList.setItems(items);
		availableList.setSize(400, 300);

		for (PartItem p : parts) {
			p.removeAll();
		}

		availableList.update();
		this.layout(true);
		this.getParent().layout(true);
	}

	/**
	 * Gets the structure.
	 *
	 * @return the structure
	 */
	public StructuralUnit getStructure() {

		try {
			int index = structCombo.getSelectionIndex();
			if (index < 0)
				if (structCombo.getItemCount() > 0)
					index = 0;
				else return null;
			structure = corpus.getStructuralUnit(structCombo.getItem(index));
		}
		catch (CqiClientException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return null;
		}
		return structure;
	}

	/**
	 * Gets the property.
	 *
	 * @return the property
	 */
	public StructuralUnitProperty getProperty() {

		if (getStructure() == null)
			return null;
		int index = propCombo.getSelectionIndex();
		if (index < 0)
			if (propCombo.getItemCount() > 0)
				index = 0;
			else return null;
		property = structure.getProperty(propCombo.getItem(index));
		return property;
	}

	/**
	 * Gets the part queries.
	 *
	 * @return the part queries
	 */
	public List<String> getPartQueries() {

		ArrayList<String> queries = new ArrayList<>();
		for (PartItem p : parts)
			queries.add(p.getQuery());
		return queries;
	}

	/**
	 * Gets the selection.
	 *
	 * @return the selection
	 */
	public List<String> getSelection() {

		return Arrays.asList(availableList.getSelection());
	}

	/**
	 * The Class PartItem.
	 */
	class PartItem extends Composite {

		/** The composer. */
		PartitionComposer composer;

		/** The columns. */
		org.eclipse.swt.widgets.List columns;

		/** The title. */
		Text title;


		/**
		 * Instantiates a new part item.
		 *
		 * @param parent the parent
		 * @param style the style
		 * @param _composer the _composer
		 */
		public PartItem(Composite parent, int style, PartitionComposer _composer) {

			super(parent, style);
			this.composer = _composer;
			this.setLayout(new GridLayout(4, false));

			Label titleLabel = new Label(this, SWT.NONE);
			titleLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false, 1, 1));
			titleLabel.setText(TXMUIMessages.titleColon);

			title = new Text(this, SWT.BORDER);
			title.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1));

			Button delete = new Button(this, SWT.PUSH);
			delete.setImage(IImageKeys.getImage(IImageKeys.ACTION_DELETE));
			delete.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false, 1, 1));
			delete.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {

					PartItem p = PartItem.this;

					for (String s : p.getColumns()) {
						PartitionComposer.this.add(s);
					}
					parts.remove(PartItem.this);
					p.dispose();

					availableList.update();
					updateDisplay();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
			Button add = new Button(this, SWT.PUSH);
			add.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1));
			add.setText(TXMUIMessages.assign);
			add.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {

					// System.out.println(Messages.bind(Messages.PartitionComposer_10, composer.getSelection()));
					for (String s : composer.getSelection()) {
						columns.add(s);
					}
					composer.remove(composer.getSelection());
					layout(true);
					if (composer.availableList.getItemCount() > 0) {
						composer.availableList.select(0);
					}
					composer.layout(true);
					updateDisplay();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			Button rmv = new Button(this, SWT.PUSH);
			rmv.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1));
			rmv.setText(TXMUIMessages.remove);
			rmv.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {

					// System.out.println(Messages.bind(Messages.PartitionComposer_12, getSelection()));
					for (String s : getSelection())
						composer.add(s);

					for (String s : getSelection())
						columns.remove(s);
					layout(true);
					composer.layout(true);
					composer.getParent().layout(true);
					availableList.update();
					updateDisplay();

				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			columns = new org.eclipse.swt.widgets.List(this, SWT.MULTI | SWT.READ_ONLY | SWT.BORDER | SWT.V_SCROLL);
			columns.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 4, 1));
			columns.setSize(200, 300);
		}

		/**
		 * Removes the all.
		 */
		public void removeAll() {

			columns.removeAll();
		}

		/**
		 * Sets the title.
		 *
		 * @param string the new title
		 */
		public void setTitle(String string) {

			this.title.setText(string);
		}

		/**
		 * Gets the columns.
		 *
		 * @return the columns
		 */
		public String[] getColumns() {

			return columns.getItems();
		}

		/**
		 * Gets the query.
		 *
		 * @return the query
		 */
		public String getQuery() {

			return getSelection().toString();
		}

		/**
		 * Gets the selection.
		 *
		 * @return the selection
		 */
		public List<String> getSelection() {

			return Arrays.asList(columns.getSelection());
		}
	}

	/**
	 * Creates partition using the specified name and the widget values.
	 *
	 * @return the partition
	 */
	public Partition createPartition(String name, Partition partition) {

		if (this.parts.size() == 0) {
			System.out.println(TXMUIMessages.errorColonThisPartitionHasNoPart);
			return null;
		}

		List<String> queries = new ArrayList<>();
		List<String> partnames = new ArrayList<>();
		for (PartItem part : this.parts) {
			if (part.columns.getItemCount() > 0) {
				String regexp = ""; //$NON-NLS-1$
				for (String s : part.columns.getItems()) {
					regexp += CQLQuery.addBackSlash(s) + "|"; //$NON-NLS-1$
				}
				regexp = regexp.substring(0, regexp.length() - 1);
				SubcorpusCQLQuery q = new SubcorpusCQLQuery(structure, property, regexp);
				queries.add(q.getQueryString());
				partnames.add(part.title.getText());
			}
		}

		try {
			if (partition == null) {
				partition = new Partition(corpus);
			}
			else {
				partition.removeAllParts();
			}
			partition.setParameters(name, queries, partnames);
			return partition;
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return null;
	}

	/**
	 * Checks for unassigned.
	 *
	 * @return true, if successful
	 */
	public boolean hasUnassigned() {

		return this.availableList.getItemCount() > 0;
	}

}
