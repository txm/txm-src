package org.txm.rcp.utils;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Composite;

public class FitFontSize {

	public static int calculate(Composite shell, String txt) {

		GC gc = new GC(shell);

		//return Math.max(shell.getSize().x, gc.stringExtent(txt).x);
		return gc.stringExtent(txt).x;
	}
}
