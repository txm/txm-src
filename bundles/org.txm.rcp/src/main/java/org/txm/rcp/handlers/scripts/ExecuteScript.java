// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.scripts;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.txm.Toolbox;
import org.txm.core.engines.EngineType;
import org.txm.core.engines.ScriptEngine;
import org.txm.core.engines.ScriptEnginesManager;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.editors.TxtEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.RCPPreferences;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.JobHandler;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * Execute a script. It uses the file extension to execute it as Groovy or R
 * 
 * Later : use shabang
 */
public class ExecuteScript extends AbstractHandler {

	private static final String ID = "org.txm.rcp.commands.ExecuteScript"; //$NON-NLS-1$
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		// the file to execute
		String result = ""; //$NON-NLS-1$
		boolean executionFromEditor = false;
		TxtEditor txteditor = null;

		IWorkbenchPart page = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();
		IWorkbenchPart editor = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (editor != null && editor instanceof TxtEditor) {
			txteditor = (TxtEditor) page;
			selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
			FileStoreEditorInput input = (FileStoreEditorInput) txteditor.getEditorInput();

			File f = new File(input.getURI().getPath());
			result = f.getAbsolutePath();
			executionFromEditor = true;
		}
		else if (selection != null
				& selection instanceof IStructuredSelection) {
					File file = null;
					IStructuredSelection strucSelection = (IStructuredSelection) selection;
					for (Iterator<Object> iterator = strucSelection.iterator(); iterator
							.hasNext();) {
						file = (File) iterator.next();
						result = file.getAbsolutePath();
						break;
					}
				}
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event)
				.getShell();
		// no file found, ask user
		if (result == "") //$NON-NLS-1$
		{
			FileDialog dialog = new FileDialog(shell, SWT.OPEN);
			if (LastOpened.getFile(ID) != null) {
				dialog.setFilterPath(LastOpened.getFolder(ID));
				dialog.setFileName(LastOpened.getFile(ID));
			}
			dialog.setFilterExtensions(new String[] { "*.groovy" }); //$NON-NLS-1$

			String scriptCurrentDir = Toolbox.getTxmHomePath() + "/scripts"; //$NON-NLS-1$

			dialog.setFilterPath(scriptCurrentDir);
			result = dialog.open();
			if (result == null) return null;
			LastOpened.set(ID, new File(result));
		}

		if (executionFromEditor && txteditor != null) { // save file if needed 
			if (txteditor.isSaveOnCloseNeeded()) { // check if file need to be saved
				boolean doit = RCPPreferences.getInstance().getBoolean(RCPPreferences.SAVE_BEFORE_EXECUTION);
				//System.out.println("Save file? "+doit);
				if (!doit) { // ask user
					MessageDialog dialog = new MessageDialog(shell, TXMCoreMessages.bind(TXMUIMessages.executionOfP0, result), null,
							TXMUIMessages.saveFileBeforeExecution, MessageDialog.QUESTION, new String[] { TXMUIMessages.saveAndRun, TXMUIMessages.abort,
									TXMUIMessages.dontSaveAndRun },
							0) {

						Button checkDefault;

						@Override
						protected Control createDialogArea(Composite parent) {
							Composite container = (Composite) super.createDialogArea(parent);
							checkDefault = new Button(container, SWT.CHECK);
							checkDefault.setText(TXMUIMessages.alwaysSaveBeforeLaunching);
							boolean b = RCPPreferences.getInstance().getBoolean(RCPPreferences.SAVE_BEFORE_EXECUTION);
							checkDefault.setSelection(b);

							checkDefault.addSelectionListener(new SelectionListener() {

								@Override
								public void widgetSelected(SelectionEvent e) {
									boolean b = checkDefault.getSelection();
									RCPPreferences.getInstance().put(RCPPreferences.SAVE_BEFORE_EXECUTION, b);
								}

								@Override
								public void widgetDefaultSelected(SelectionEvent e) {
								}
							});
							return container;
						}
					};
					int c = dialog.open();
					if (c == 1) {
						System.out.println(TXMUIMessages.executionCanceled);
						return null;
					}
					else if (c == 2) {
						//System.out.println("Don't save and run.");
						doit = false;
					}
					else if (c == 0) {
						//System.out.println("Save and run.");
						doit = true;
					}
				}

				if (doit) {
					//System.out.println("Saving file...");
					txteditor.doSave(null);
				}
			}
		}

		return executeScript(result, page, editor, selection, null);
	}

	/**
	 * Execute script.
	 *
	 * @param scriptpath the scriptfile
	 */
	public static JobHandler executeScript(String scriptpath, IWorkbenchPart part, IWorkbenchPart editor, final ISelection selection, List<String> stringArgs) {
		if (scriptpath == null) return null;
		File script = new File(scriptpath);
		if (!script.exists()) return null;

		String ext = script.getName();
		int i = ext.lastIndexOf("."); //$NON-NLS-1$
		if (i > 0) {
			ext = ext.substring(i + 1);
		}

		if ("groovy".equals(ext)) { // TODO make Groovy a script engine //$NON-NLS-1$
			return ExecuteGroovyScript.executeScript(scriptpath, part, editor, selection);
		}
		else {
			ScriptEnginesManager sem = (ScriptEnginesManager) Toolbox.getEngineManager(EngineType.SCRIPT);
			ScriptEngine se = (ScriptEngine) sem.getEngineForExtension(ext);
			if (se != null) {
				HashMap<String, Object> env = new HashMap<String, Object>();
				env.put("part", part); // selection //$NON-NLS-1$
				env.put("editor", editor); // selection //$NON-NLS-1$
				env.put("selection", selection); // selection //$NON-NLS-1$
				se.executeScript(script, env, stringArgs); // TODO move JobHanddler to the Toolbox plugin
				return null;
			}
			else {
				Log.warning(TXMUIMessages.noInterpreterFoundForScriptFileExtension + script.getName());
			}
		}

		return null;
	}
}
