// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views.fileexplorer;

import java.io.File;
import java.io.FileFilter;
import java.util.regex.Pattern;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.RCPPreferences;

/**
 * The Class FileTreeContentProvider.
 */
public class FileTreeContentProvider implements ITreeContentProvider {

	protected Pattern p = null;

	public FileTreeContentProvider() {
		super();

		String regex = RCPPreferences.getInstance().getString(RCPPreferences.FILES_TO_HIDE);
		p = null;
		try {
			p = Pattern.compile(regex);
		}
		catch (Exception e) {
			System.out.println(TXMUIMessages.fileExplorerColonIncorrectPattern + regex + TXMUIMessages.eRROR + e);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
	 */
	@Override
	public Object[] getChildren(Object element) {
		final boolean showHidden = RCPPreferences.getInstance().getBoolean(RCPPreferences.SHOW_HIDDEN_FILES);

		//final boolean showFolder = RCPPreferences.getInstance().getBoolean(RCPPreferences.SHOW_FOLDERS);
		Object[] kids = ((File) element).listFiles(new FileFilter() {

			@Override
			public boolean accept(File file) {
				boolean rez = true;
				if (p != null)
					rez = !p.matcher(file.getName()).matches();
				if (!showHidden)
					rez = rez & !file.isHidden();
				//				if (!showFolder)
				//					rez = rez & !file.isDirectory();
				return rez;
			}
		});

		if (kids != null) {
			java.util.Arrays.sort(kids);
		}

		return kids == null ? new Object[0] : kids;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	@Override
	public Object[] getElements(Object element) {
		return getChildren(element);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
	 */
	@Override
	public boolean hasChildren(Object element) {
		return getChildren(element).length > 0;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
	 */
	@Override
	public Object getParent(Object element) {
		if (element instanceof File)
			return ((File) element).getParentFile();
		System.out.println(TXMUIMessages.errorElementColon + element + TXMUIMessages.FileTreeContentProvider_4);
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer viewer, Object old_input, Object new_input) {

	}
}
