package org.txm.rcp.p2.plugins;

import org.eclipse.equinox.internal.p2.discovery.Catalog;
import org.eclipse.equinox.internal.p2.ui.discovery.wizards.CatalogConfiguration;
import org.eclipse.equinox.internal.p2.ui.discovery.wizards.CatalogPage;
import org.eclipse.equinox.internal.p2.ui.discovery.wizards.DiscoveryWizard;

public class TXMDiscoveryWizard extends DiscoveryWizard {

	public TXMDiscoveryWizard(Catalog catalog,
			CatalogConfiguration configuration) {
		super(catalog, configuration);

	}

	protected CatalogPage doCreateCatalogPage() {
		return new TXMCatalogPage(getCatalog());
	}

	@Override
	public boolean performFinish() {
		return TXMDiscoveryUi.install(getCatalogPage().getInstallableConnectors(), getContainer());
	}
}
