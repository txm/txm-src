package org.txm.rcp.swt.widget.parameters;

import org.apache.commons.lang.StringUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.kohsuke.args4j.NamedOptionDef;

public class StringArrayField extends LabelField {

	Combo dt;

	public StringArrayField(Composite parent, int style, NamedOptionDef parameter) {
		super(parent, style, parameter);

		dt = new Combo(this, SWT.SINGLE | SWT.BORDER | SWT.READ_ONLY);
		dt.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));

		String[] values = getMetaVar().trim().split("\t"); //$NON-NLS-1$
		dt.setItems(values);

		resetToDefault();
	}

	public String getWidgetValue() {
		return dt.getText();
	}

	public void setDefault(Object text) {
		if (text instanceof Object[]) {
			Object[] array = (Object[]) text;
			dt.setText(StringUtils.join(array, "\t")); //$NON-NLS-1$
		}
		else {
			dt.setText("" + text); //$NON-NLS-1$
		}
	}

	@Override
	public void resetToDefault() {
		dt.setText(getWidgetDefault());
	}
}
