package org.txm.rcp.preferences;

import java.io.File;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.txm.rcp.messages.TXMUIMessages;

public class DirectoryAndOpenFieldEditor extends DirectoryFieldEditor {

	/**
	 * Initial path for the Browse dialog.
	 */
	private File filterPath = null;

	Button openButton;

	/**
	 * Creates a new directory field editor
	 */
	protected DirectoryAndOpenFieldEditor() {
	}

	/**
	 * Creates a directory field editor.
	 * 
	 * @param name the name of the preference this field editor works on
	 * @param labelText the label text of the field editor
	 * @param parent the parent of the field editor's control
	 */
	public DirectoryAndOpenFieldEditor(String name, String labelText, Composite parent) {
		super(name, labelText, parent);
	}

	/*
	 * (non-Javadoc)
	 * Method declared on FieldEditor.
	 */
	@Override
	public int getNumberOfControls() {
		return 4;
	}

	/*
	 * (non-Javadoc)
	 * Method declared on StringFieldEditor (and FieldEditor).
	 */
	@Override
	protected void doFillIntoGrid(Composite parent, int numColumns) {
		super.doFillIntoGrid(parent, numColumns - 1);
		openButton = getOpenButton(parent);
		GridData gd = new GridData();
		gd.horizontalAlignment = GridData.FILL;
		int widthHint = convertHorizontalDLUsToPixels(openButton, IDialogConstants.BUTTON_WIDTH);
		gd.widthHint = Math.max(widthHint, openButton.computeSize(SWT.DEFAULT, SWT.DEFAULT, true).x);
		openButton.setLayoutData(gd);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditor#setEnabled(boolean, org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void setEnabled(boolean enabled, Composite parent) {
		super.setEnabled(enabled, parent);
		if (openButton != null) {
			openButton.setEnabled(enabled);
		}
	}

	/**
	 * Get the change control. Create it in parent if required.
	 * 
	 * @param parent
	 * @return Button
	 */
	protected Button getOpenButton(Composite parent) {
		if (openButton == null) {
			openButton = new Button(parent, SWT.PUSH);
			openButton.setText(TXMUIMessages.Open);
			openButton.setFont(parent.getFont());
			openButton.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent evt) {
					String newValue = changePressed();
					if (newValue != null) {
						setStringValue(newValue);
					}
				}
			});
			openButton.addDisposeListener(new DisposeListener() {

				@Override
				public void widgetDisposed(DisposeEvent event) {
					openButton = null;
				}
			});
		}
		else {
			checkParent(openButton, parent);
		}
		return openButton;
	}
}
