package org.txm.rcp.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.views.corpora.CorporaView;

/**
 * Debug preferences page.
 * 
 * @author sjacquot
 *
 */
public class DebugPreferencePage extends TXMPreferencePage {


	@Override
	public void createFieldEditors() {

		// Show all result nodes in Corpora view
		addField(new BooleanFieldEditor(TBXPreferences.SHOW_ALL_RESULT_NODES, TXMUIMessages.showAllResultNodesInCorporaView, getFieldEditorParent()));

		// enable some Eclipse features deactivated for TXM
		addField(new BooleanFieldEditor(TBXPreferences.EXPERT_USER, TXMUIMessages.expertMode, getFieldEditorParent()));
		
//		// disable some TXM parameters to simplify learning steps
//		addField(new BooleanFieldEditor(TBXPreferences.BEGINNER_USER, "Learn mode", getFieldEditorParent()));

//		addField(new BooleanFieldEditor(RCPPreferences.DISABLE_TEST_WARNING, TXMUIMessages.disableTheWarningDialogWhenTestingTXM, getFieldEditorParent()));
	}

	@Override
	public void init(IWorkbench workbench) {

		this.setPreferenceStore(new TXMPreferenceStore(TBXPreferences.getInstance().getPreferencesNodeQualifier()));
	}

	@Override
	public boolean performOk() {

		if (super.performOk()) {
			CorporaView.refresh();
			return true;
		}

		return false;
	}
}
