package org.txm.rcp;

import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

/**
 * 
 * Utility class to get the active Window and active Page
 * 
 * @author mdecorde
 *
 */
public class TXMWindows {

	public static IWorkbenchWindow getActiveWindow() {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window == null) {
			IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
			if (windows.length > 0) {
				window = windows[0];
			}
		}
		return window;
	}

	public static Display getDisplay() {
		return PlatformUI.getWorkbench().getDisplay();
	}

	public static IWorkbenchPage getActivePage() {
		IWorkbenchWindow window = getActiveWindow();
		if (window != null) {
			return window.getActivePage();
		}
		return null;
	}
}
