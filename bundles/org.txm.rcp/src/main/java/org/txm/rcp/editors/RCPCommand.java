package org.txm.rcp.editors;

import org.txm.core.results.TXMResult;
import org.txm.functions.Command;


/**
 * A command that opens an editor (which will computes the result).
 * 
 * The class adds a new mandatory member 'editor_id'
 * 
 * @author mdecorde
 *
 */
public class RCPCommand extends Command {

	public final String editor_id;

	public RCPCommand(Class<TXMResult> clazz, String editor_id) {
		super(clazz);
		this.editor_id = editor_id;
	}

	public RCPCommand(String cmd, Class<TXMResult> clazz, String editor_id) {
		super(cmd, clazz);
		this.editor_id = editor_id;
	}

	public String toString() {
		return super.toString() + " -> " + editor_id; //$NON-NLS-1$
	}
}
