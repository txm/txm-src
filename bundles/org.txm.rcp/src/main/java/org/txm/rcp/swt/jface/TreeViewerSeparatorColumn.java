package org.txm.rcp.swt.jface;

import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;

public class TreeViewerSeparatorColumn {

	public static final String EMPTY = "";

	public static TreeViewerColumn newSeparator(TreeViewer viewer) {

		TreeViewerColumn separator = new TreeViewerColumn(viewer, SWT.NONE);
		separator.getColumn().setResizable(false);
		separator.getColumn().setData("separator", true);
		separator.setLabelProvider(new CellLabelProvider() {

			public void update(ViewerCell cell) {
			}
		});
		separator.getColumn().setText(" ");
//		separator.getColumn().pack();
		
		return separator;
	}
}
