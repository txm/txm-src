package org.txm.rcp.swt.dialog;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.JobHandler;

/**
 * SWT/JFace in Action
 * GUI Design with Eclipse 3.0
 * Matthew Scarpino, Stephen Holder, Stanford Ng, and Laurent Mihalkovic
 * 
 * ISBN: 1932394273
 * 
 * Publisher: Manning
 * 
 * MD: modified for TXM needs
 *
 */
public class UsernamePasswordDialog extends Dialog {

	private static final int RESET_ID = IDialogConstants.NO_TO_ALL_ID + 1;

	private Text usernameField;

	private Text passwordField;

	boolean must[];

	String user = null, password = null;

	String address;

	String details;

	String title;

	public UsernamePasswordDialog(Shell parentShell, boolean must[], String kr_name) {
		super(parentShell);
		this.must = must;
		this.address = kr_name;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);

		if (title == null) {
			title = NLS.bind(TXMUIMessages.loginToP0, address);
		}
		newShell.setText(title);
		int l = Math.max(title.length() * 10, 500);
		newShell.setSize(l, 200);
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite comp = (Composite) super.createDialogArea(parent);

		GridLayout layout = (GridLayout) comp.getLayout();
		layout.numColumns = 2;

		if (details != null) {
			Label detailsLabel = new Label(comp, SWT.RIGHT);
			detailsLabel.setText(details);
			GridData gdata = new GridData(GridData.BEGINNING, GridData.BEGINNING, true, true, 2, 1);
			detailsLabel.setLayoutData(gdata);
		}

		if (must[0]) {
			Label usernameLabel = new Label(comp, SWT.RIGHT);
			usernameLabel.setText(TXMUIMessages.login);

			usernameField = new Text(comp, SWT.SINGLE);
			GridData data = new GridData(GridData.FILL_HORIZONTAL);
			usernameField.setLayoutData(data);

			if (user != null) {
				usernameField.setText(user);
			}
		}
		if (must[1]) {
			Label passwordLabel = new Label(comp, SWT.RIGHT);
			passwordLabel.setText(TXMUIMessages.password);

			passwordField = new Text(comp, SWT.SINGLE | SWT.PASSWORD);
			GridData data = new GridData(GridData.FILL_HORIZONTAL);
			passwordField.setLayoutData(data);
		}
		return comp;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		super.createButtonsForButtonBar(parent);
		createButton(parent, RESET_ID, TXMUIMessages.resetAll, false);
		this.getButton(OK).setText(TXMUIMessages.connect);
	}

	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == RESET_ID) {
			if (must[0]) {
				usernameField.setText(""); //$NON-NLS-1$
			}
			if (must[1]) {
				passwordField.setText(""); //$NON-NLS-1$
			}
		}
		else {
			if (must[0]) {
				user = usernameField.getText();
			}
			else {
				user = null;
			}
			if (must[1]) {
				password = passwordField.getText();
			}
			else {
				password = null;
			}
			super.buttonPressed(buttonId);
		}
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	/**
	 * must be called before opening the dialog
	 * 
	 * @param user
	 */
	public void setUsername(String user) {
		this.user = user;
	}

	/**
	 * must be called before opening the dialog
	 * 
	 * @param details
	 */
	public void setDetails(String details) {
		this.details = details;
	}

	/**
	 * must be called before opening the dialog
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	public static boolean initializeCredentials(String userKey, String passwordKey, String target, String title, String details) {

		if (System.getProperty(userKey) == null
				|| System.getProperty(passwordKey) == null) {

			Shell shell = Display.getCurrent().getActiveShell();
			if (shell == null) {
				System.out.println("Error: could not open crendetial dialog on current Thread."); //$NON-NLS-1$
				return false;
			}

			UsernamePasswordDialog dialog = new UsernamePasswordDialog(shell, new boolean[] { userKey != null, passwordKey != null }, target);
			if (userKey != null) dialog.setUsername(System.getProperty(userKey));
			dialog.setTitle(title);
			dialog.setDetails(TXMUIMessages.bind(TXMUIMessages.loginToP0, details));
			if (dialog.open() == UsernamePasswordDialog.OK) {
				if (userKey != null) System.setProperty(userKey, dialog.getUser());
				if (passwordKey != null) System.setProperty(passwordKey, dialog.getPassword());
				return true;
			}
			else {
				System.out.println(TXMUIMessages.CancelCredentialSettings);
				return false;
			}
		}
		return false;
	}

	public static boolean initializeCredentials(String userKey, String passwordKey, String target, String title, String details, JobHandler monitor) {

		final Boolean ret[] = new Boolean[1];
		ret[0] = false;

		monitor.syncExec(new Runnable() {

			public void run() {
				System.out.println(TXMUIMessages.OpeningCredentialDialog);
				try {
					ret[0] = initializeCredentials(userKey, passwordKey, target, title, details);
				}
				catch (Throwable t) {
					t.printStackTrace();
				}
			}
		});
		return ret[0];
	}
}
