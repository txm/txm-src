// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands;

import java.util.Locale;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.Platform;
import org.txm.rcp.Activator;

/**
 * Open the internal browser of TXM with a file
 * 
 * @author mdecorde.
 */
public class OpenWelcomePage extends AbstractHandler {

	/** The Constant ID. */
	public final static String ID = "org.txm.rcp.commands.OpenWelcomePage"; //$NON-NLS-1$

	public static String WELCOME = "https://pages.textometrie.org/textometrie/files/software/TXM/"; //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		return openWelcomePage();
	}

	public static boolean openWelcomePage() {

		String locale = Locale.getDefault().getLanguage();
		String version = Platform.getBundle(Activator.PLUGIN_ID).getVersion().toString();
		int idx = version.lastIndexOf("."); //$NON-NLS-1$
		if (idx > 0) version = version.substring(0, idx); // remove the "qualifier" part

		if ("fr".equals(locale)) { //$NON-NLS-1$
			return OpenBrowser.openfile(WELCOME + version + "/welcome") != null; //$NON-NLS-1$ $NON-NLS-2$
		}
		else {
			return OpenBrowser.openfile(OpenLocalizedWebPage.getValidURL(WELCOME + version + "/" + locale + "/welcome", (WELCOME + version + "/welcome"))) != null; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ $NON-NLS-2$ $NON-NLS-3$
		}
	}
}
