package org.txm.rcp.swt.dialog;

import java.lang.reflect.InvocationTargetException;
import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.internal.progress.ProgressMonitorJobsDialog;
import org.txm.core.results.TXMResult;

/**
 * An advanced progress monitor modal dialog managing delay before displaying the window
 * and the forcing of cancel using Thread.stop().
 * 
 * @author sjacquot
 *
 */
public class ComputeProgressMonitorDialog extends ProgressMonitorJobsDialog {

	/**
	 * Result.
	 */
	protected TXMResult result;

	/**
	 * Delay in ms before opening the dialog after starting runnable.
	 */
	protected int delayBeforeOpen;


	/**
	 * Creates a modal progress monitor dialog.
	 * 
	 * @param parent
	 * @param result
	 * @param delayBeforeOpen delay in ms before opening the dialog after starting runnable
	 */
	public ComputeProgressMonitorDialog(Shell parent, TXMResult result, int delayBeforeOpen) {
		super(parent);
		this.result = result;
		this.delayBeforeOpen = delayBeforeOpen;
		this.setOpenOnRun(false);
	}

	/**
	 * Creates a modal progress monitor dialog.
	 * 
	 * @param parent
	 * @param result
	 */
	public ComputeProgressMonitorDialog(Shell parent, TXMResult result) {
		this(parent, result, 1000);
	}


	@Override
	protected void createDetailsButton(Composite parent) {
		// to not create the "Details" button
	}

	@Override
	protected void configureShell(final Shell shell) {
		super.configureShell(shell);
		try {
			// set window title to result start computing message
			try {
				shell.setText(this.result.getComputingStartMessage());
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Runs the computing process on the stored <code>TXMResult</code>.
	 * 
	 * @param cancelable
	 * @throws InvocationTargetException
	 * @throws InterruptedException
	 */
	public void runComputingProcess(boolean cancelable) throws InvocationTargetException, InterruptedException {

		IRunnableWithProgress runnable = new IRunnableWithProgress() {

			@Override
			public void run(IProgressMonitor monitor) throws InterruptedException {
				result.compute(monitor, false);
			}
		};

		this.run(cancelable, runnable);
	}

	/**
	 * Runs the given <code>IRunnableWithProgress</code>.
	 * 
	 * @param cancelable
	 * @param runnable
	 * @throws InvocationTargetException
	 * @throws InterruptedException
	 */
	public void run(boolean cancelable, IRunnableWithProgress runnable) throws InvocationTargetException, InterruptedException {
		this.run(true, cancelable, runnable);
	}

	@Override
	public void run(boolean fork, boolean cancelable, IRunnableWithProgress runnable) throws InvocationTargetException, InterruptedException {

		// manage the dialog open delay
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {

			protected int elapsedTime = 0;

			@Override
			public void run() {
				this.elapsedTime += 10;
				if (this.elapsedTime > delayBeforeOpen) {
					Display.getDefault().asyncExec(new Runnable() {

						@Override
						public void run() {
							open();
						}
					});
					this.cancel();
					timer.purge();
					timer.cancel();
				}
			}
		}, 0, 10);

		super.run(fork, cancelable, runnable);
	}


	@Override
	protected void cancelPressed() {
		this.result.getComputingThread().stop();
		super.cancelPressed();
	}
}

