package org.txm.rcp.swt.provider;

import java.util.Arrays;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class ElementTreeContentProvider implements ITreeContentProvider {

	String parentName;

	String[] parentAttrs;

	String childName;

	String[] childAttrs;

	public ElementTreeContentProvider(String parentName, String childName, String[] parentAttrs, String[] childAttrs) {
		this.parentName = parentName;
		this.childName = childName;
		this.parentAttrs = parentAttrs;
		this.childAttrs = childAttrs;
	}

	@Override
	public void dispose() {
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

	@Override
	public Object[] getElements(Object inputElement) {
		NodeList nlist = (NodeList) inputElement;
		Object[] children = new Object[nlist.getLength()];
		for (int i = 0; i < children.length; i++)
			children[i] = nlist.item(i);
		System.out.println("Elements: " + Arrays.toString(children)); //$NON-NLS-1$
		return children;
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement == null) return new Object[0];
		if (parentElement instanceof Element) {
			Element elem = (Element) parentElement;
			System.out.println("elem: " + elem); //$NON-NLS-1$
			if (elem.getNodeName().equals(parentName)) {
				NodeList nlist = elem.getElementsByTagName(childName);
				Object[] children = new Object[nlist.getLength()];
				for (int i = 0; i < children.length; i++)
					children[i] = nlist.item(i);
				return children;
			}
			else {
				return new Object[0];
			}
		}
		return new Object[0];
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof Element) {
			Element elem = (Element) element;
			if (elem.getNodeName().equals(parentName)) {
				return null;
			}
			else {
				return elem.getParentNode();
			}

		}
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof Element) {
			return ((Element) element).getChildNodes().getLength() > 0;
		}
		return false;
	}

}
