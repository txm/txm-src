package org.txm.rcp.swt.widget.parameters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.kohsuke.args4j.NamedOptionDef;
import org.kohsuke.args4j.spi.OptionHandler;
import org.txm.rcp.messages.TXMUIMessages;

@SuppressWarnings("rawtypes")
public class ParametersDialog2 extends Dialog {

	// protected ArrayList<NamedOptionDef> parameters;

	protected String title;

	protected ParametersManager manager;

	protected ParametersGLComposite columns;

	/**
	 * Don't forget to call init(...) to setup the parameters to show
	 * 
	 * @param parentShell
	 */
	public ParametersDialog2(Shell parentShell) {
		super(parentShell);

		// init(allParameters, defaultParameters);
	}

	/**
	 * 
	 * @param parentShell
	 * @param allParameters all the scripts annotations
	 */
	public ParametersDialog2(Shell parentShell, List<OptionHandler> allParameters, Map<String, Object> defaultParameters) {
		super(parentShell);

		init(allParameters, defaultParameters);
	}

	public void init(List<OptionHandler> allParameters, Map<String, Object> defaultParameters) {

		setShellStyle(getShellStyle() | SWT.RESIZE);

		manager = new ParametersManager();
		ArrayList<NamedOptionDef> parameters = new ArrayList<>();
		if (allParameters != null) {
			for (OptionHandler opt : allParameters) {
				if (opt.option instanceof NamedOptionDef) {
					parameters.add((NamedOptionDef) opt.option);
				}
			}
		}

		manager.setParametersOptions(parameters);

		manager.updateDefaultValues(defaultParameters);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);

		newShell.setText(title);
		newShell.setMinimumSize(250, 450);
	}

	public ParametersManager getParameterManager() {
		return manager;
	}

	public ParametersGLComposite getParameterWidgets() {
		return columns;
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create the reset button (hack of the Back button)
		// Button propButton = createButton(parent, IDialogConstants.BACK_ID,
		// "...", false);
		// propButton.removeListener(SWT.Selection, propButton.getListeners(SWT.Selection)[0]);
		//
		// propButton.addSelectionListener(new SelectionAdapter() {
		// @Override
		// public void widgetSelected(SelectionEvent e) {
		// FileDialog dialog = new FileDialog(e.display.getActiveShell());
		// dialog.setFilterPath(propFile.getParent());
		// dialog.setFilterExtensions(new String[] {"*.properties"});
		// dialog.setFileName(propFile.getName());
		// String path = dialog.open();
		// if (path != null) {
		// propFile = new File(path);
		// loadDefaultValuesFromPropFile();
		// }
		// }
		// });

		Button button = createButton(parent, IDialogConstants.BACK_ID, TXMUIMessages.resetValues, false);
		button.removeListener(SWT.Selection, button.getListeners(SWT.Selection)[0]);

		button.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				columns.resetWidgets();
			}
		});
		button.setEnabled(columns.getNumberOfFields() > 0);

		super.createButtonsForButtonBar(parent);
		this.getButton(IDialogConstants.OK_ID).setText(TXMUIMessages.run);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite composite) {
		// this.parent = composite;
		super.createDialogArea(composite);

		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout(1, true);
		composite.setLayout(layout);

		columns = new ParametersGLComposite(composite, SWT.NONE, "parameters", this.manager); //$NON-NLS-1$

		new Label(composite, SWT.NONE).setText(TXMUIMessages.mandatoryFields);

		return composite;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		ArrayList<String> missing = columns.getNonSetFields();
		if (missing.size() > 0) {
			System.out.println(TXMUIMessages.errorColonMissingValueColon + StringUtils.join(missing, ", ")); //$NON-NLS-1$
			return; // abort
		}

		super.okPressed();
	}

	public HashMap<String, Object> getValues() {
		return manager.getFinalValues();
	}

	public boolean getMustShowDialog() {
		return columns != null && columns.mustShowDialog;
	}
}
