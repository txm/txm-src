package org.txm.rcp.swt.widget;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.jface.util.Util;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.txm.rcp.swt.provider.SimpleLabelProvider;

/**
 * Attempt to replace the TableViewer widget with freezeable, etc. columns which is missing in JFace&SWT Tables
 * 
 * The idea is to have one TableViewer per freezed area, moving a column moves a TableViewerColumn to another TableViewer
 * 
 * not working yet
 * 
 * @author mdecorde
 *
 */
public class SuperTableViewer extends ScrolledComposite {

	protected ArrayList<SuperTableColumn> columns = new ArrayList<SuperTableColumn>();

	SashForm wholeArea;

	protected Composite innerArea, beginArea, endArea;

	protected FormData beginAreaForm, endAreaForm;

	public SuperTableViewer(Composite parent, int style) {

		super(parent, style);

		this.setLayout(new GridLayout());
		this.setExpandHorizontal(true);
		this.setExpandVertical(true);

		innerArea = new Composite(this, SWT.BORDER);
		innerArea.setLayout(new GridLayout(3, false));
		innerArea.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		beginArea = new Composite(innerArea, SWT.BORDER);
		GridData gdata1 = new GridData(GridData.FILL, GridData.FILL, false, false);
		gdata1.minimumWidth = 50;
		beginArea.setLayoutData(gdata1);

		wholeArea = new SashForm(innerArea, SWT.BORDER);
		wholeArea.setLayout(new FormLayout());
		wholeArea.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		endArea = new Composite(innerArea, SWT.BORDER);
		GridData gdata2 = new GridData(GridData.FILL, GridData.FILL, false, false);
		gdata2.minimumWidth = 50;
		endArea.setLayoutData(gdata2);

		//		beginAreaForm = new FormData();
		//		beginAreaForm.left = new FormAttachment(0);
		//		beginAreaForm.top = new FormAttachment(0);
		//		beginAreaForm.bottom = new FormAttachment(100);
		//		beginAreaForm.width = 10;
		//		beginArea.setLayoutData(beginAreaForm);
		//		
		//		endAreaForm = new FormData();
		//		endAreaForm.right = new FormAttachment(100);
		//		endAreaForm.top = new FormAttachment(0);
		//		endAreaForm.bottom = new FormAttachment(100);
		//		endAreaForm.width = 10;
		//		endArea.setLayoutData(endAreaForm);

		this.setContent(innerArea);
		this.setMinSize(this.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	private void updatePositions() {
		for (int i = 0; i < columns.size(); i++) {
			columns.get(i).updatePosition(i);
		}
	}

	public SuperTableColumn newColumn(String name) {
		return newColumn(name, -1);
	}

	public SuperTableColumn newColumn(String name, int position) {
		SuperTableColumn col = new SuperTableColumn(name);
		if (position > 0) {
			this.columns.add(position, col);
		}
		else {
			this.columns.add(col);
		}
		updatePositions();
		return col;
	}

	public void setInput(Object input) {
		for (SuperTableColumn column : columns) {
			column.setInput(input);
		}
	}

	public void addKeyListener(KeyListener listener) {
		//		for (SuperTableColumn column : columns) {
		//			column.getTable().addKeyListener(listener);
		//		}
		super.addKeyListener(listener);
	}

	public SuperTableColumn getColumn(int i) {
		return columns.get(i);
	}

	public int getNColumn() {
		return columns.size();
	}

	public void addMouseListener(MouseListener listener) {
		//		for (SuperTableColumn column : columns) {
		//			column.getTable().addMouseListener(listener);
		//		}
		super.addMouseListener(listener);
	}

	public class SuperTableColumn extends TableViewer {

		Table columnTable;

		//		Sash sash;
		FormData data = new FormData();

		FormData data2 = new FormData();

		String name;

		int position;

		protected SuperTableColumn(String name) {
			super(wholeArea, SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER);

			this.columnTable = this.getTable();
			getTable().setLinesVisible(true);
			getTable().setHeaderVisible(true);

			TableColumn firstColumn = new TableColumn(getTable(), SWT.LEFT);
			firstColumn.setWidth(0);
			firstColumn.setResizable(false);
			firstColumn.setText(name);

			setLabelProvider(new SimpleLabelProvider() {

				@Override
				public String getColumnText(Object element, int columnIndex) {
					return "" + SuperTableColumn.this.position + ": " + element;
				}
			});

			//sash = new Sash(wholeArea, SWT.VERTICAL|SWT.BORDER);
			//			sash.addSelectionListener(new SelectionAdapter() {
			//				public void widgetSelected(SelectionEvent event) {
			//					((FormData) sash.getLayoutData()).left = new FormAttachment(0, event.x);
			//					sash.getParent().layout();
			//				}
			//			});

			// to maintain vertical selection
			columnTable.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					for (SuperTableColumn col : SuperTableViewer.this.columns) {
						if (col != SuperTableColumn.this) {
							col.getTable().deselectAll();
							col.getTable().select(columnTable.getSelectionIndices());
						}
					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});


			// to maintain vertical synchronisation
			ScrollBar vBarLeft = columnTable.getVerticalBar();
			vBarLeft.addListener(SWT.Selection, new Listener() {

				@Override
				public void handleEvent(Event event) {
					for (SuperTableColumn col : SuperTableViewer.this.columns) {
						if (col != SuperTableColumn.this) {
							if (Util.isLinux()) {
								col.getTable().getVerticalBar().setSelection(vBarLeft.getSelection());
							}
							else {
								col.getTable().setTopIndex(columnTable.getTopIndex());
							}
						}
					}
				}
			});

			firstColumn.setWidth(200);
			//firstColumn.pack();
		}

		public void updatePosition(int position) {

			this.position = position;
			//			data.top = new FormAttachment(0); 
			//			data.bottom = new FormAttachment(100);
			//			if (position == 0) {
			//				data.left = new FormAttachment(beginArea, 0);
			//			} else {
			//				data.left = new FormAttachment(SuperTableViewer.this.getColumn(position-1).getSash()); // the table is next to the previous table's sash
			//			}
			//			//data.right = new FormAttachment(sash,0);
			//			columnTable.setLayoutData(data);
			//
			//			data2.top = new FormAttachment(0); 
			//			data2.bottom = new FormAttachment(100); 
			//			data2.left = new FormAttachment(columnTable, 0);
			//			if (position == SuperTableViewer.this.getNColumn() -1) {
			//				//data2.right = new FormAttachment(endArea); 
			//			}
			//			//data2.width = 5;
			//			sash.setLayoutData(data2);
		}

		//		private Control getSash() {
		//			return sash;
		//		}

		public TableViewerColumn newColumn(int style) {
			TableViewerColumn col = new TableViewerColumn(this, style);
			return col;
		}

		public void setWidth(int w) {
			data2.left = new FormAttachment(w);
		}
	}

	public static void main(String args[]) {
		Display display = new Display();

		Shell shell = new Shell(display);
		shell.setMinimumSize(400, 300);
		shell.setLayout(new GridLayout());

		SuperTableViewer composite = new SuperTableViewer(shell, SWT.NONE);
		composite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		composite.newColumn("first");
		SuperTableColumn col = composite.newColumn("second");

		// add some column to test the horizontal scroll bar -> later we'll should be able to add SuperTableColumn and group them
		TableViewerColumn subColumn1 = new TableViewerColumn(col, SWT.NONE);
		subColumn1.getColumn().setWidth(200);
		subColumn1.getColumn().setText("sub1");
		subColumn1.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object elem) {
				return "subbbbbbbbbbbbbbbbbbbb";
			}
		});
		TableViewerColumn subColumn2 = new TableViewerColumn(col, SWT.NONE);
		subColumn2.getColumn().setWidth(100);
		subColumn2.getColumn().setText("sub2");
		subColumn2.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object elem) {
				return "sub22222222222222222222";
			}
		});

		//composite.newColumn("third");

		composite.setContentProvider(new ArrayContentProvider());
		composite.setInput(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18));

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	public void setLabelProvider(SimpleLabelProvider simpleLabelProvider) {
		for (SuperTableColumn col : columns) {
			col.setLabelProvider(simpleLabelProvider);
		}
	}

	public void setContentProvider(ArrayContentProvider arrayContentProvider) {
		for (SuperTableColumn col : columns) {
			col.setContentProvider(arrayContentProvider);
		}
	}

}
