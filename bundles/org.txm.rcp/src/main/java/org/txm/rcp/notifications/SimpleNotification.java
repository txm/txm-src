package org.txm.rcp.notifications;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

/**
 * Sample Notification Popup Class
 */
public class SimpleNotification extends AbstractNotificationPopup {

	private String title;

	private String message;

	/**
	 * @param display
	 */
	public SimpleNotification(Display display, String title, String message) {
		super(display);
		this.title = title;
		this.message = message;
	}

	@Override
	protected void createContentArea(Composite parent) {

		Composite container = new Composite(parent, SWT.NULL);

		GridData data = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		container.setLayoutData(data);

		container.setLayout(new GridLayout(1, false));

		Label successMsg = new Label(container, SWT.NULL);
		successMsg.setText(message);


		//        new Label(container, SWT.NONE);
		//        Label testLabel1 = new Label(container, SWT.NONE);
		//        testLabel1.setText("This is a Test Label");
		//        FontData fontData = testLabel1.getFont().getFontData()[0];
		//        Font font = new Font(Display.getCurrent(),
		//                new FontData(fontData.getName(), fontData.getHeight(), SWT.BOLD));
		//        testLabel1.setFont(font);
		//
		//        String url = "https://www.google.com/";
		//
		//        Link restEP = new Link(container, SWT.WRAP | SWT.LEFT);
		//        restEP.setText(createUrl(url));
		//        GridData linkData = new GridData();
		//        linkData.widthHint = 400;
		//        restEP.setLayoutData(linkData);
		//        restEP.addSelectionListener(new SelectionAdapter() {
		//
		//            @Override
		//            public void widgetSelected(SelectionEvent e) {
		//                Program.launch(url);
		//            }
		//
		//        });
		//            
		new Label(container, SWT.NONE);
	}

	@Override
	protected String getPopupShellTitle() {
		return title;
	}
}
