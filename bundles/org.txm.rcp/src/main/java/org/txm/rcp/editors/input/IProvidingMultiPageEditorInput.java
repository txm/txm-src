// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.editors.input;

import java.util.List;

import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.part.MultiPageEditorPart;

// TODO: Auto-generated Javadoc
/**
 * This interface is implemented by {@link IEditorInput} whose content should be
 * displayed in a GenericMultiPageEditor}.
 * 
 * @author Sylvain Loiseau
 */
//FIXME: an editor input should not be responsible of storing or creating some EditorPart or any UI components.
@Deprecated
public interface IProvidingMultiPageEditorInput {

	/**
	 * Get the instances of editor to be displayed in the.
	 *
	 * @return a list of EditorPart instances.
	 *         {@link MultiPageEditorPart}.
	 */
	public List<EditorPart> getEditors();

	/**
	 * Get the data source ({@link IEditorInput} for each editor given by.
	 *
	 * @return a list of IEditorInput instances.
	 */
	public List<IEditorInput> getEditorInputs();

	/**
	 * Get the name of each page of the MultiPageEditor.
	 * 
	 * @return a list of IEditorInput instances.
	 */
	public List<String> getNames();

}
