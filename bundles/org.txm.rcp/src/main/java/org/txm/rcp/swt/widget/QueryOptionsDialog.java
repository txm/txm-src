// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.widget;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.provider.SimpleLabelProvider;
import org.txm.searchengine.core.SearchEngine;
import org.txm.searchengine.core.SearchEnginesManager;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

/**
 * Set the Query options to use (set the right Query class to use)
 */
public class QueryOptionsDialog extends Dialog {

	/** The main panel. */
	Composite mainPanel;

	/** The parent shell. */
	Shell parentShell;

	/** The values. */
	ArrayList<SearchEngine> values;

	/** The combo. */
	private ComboViewer combo;

	/** The selected index. */
	private SearchEngine selectedEngine;

	/** The default value. */
	private SearchEngine defaultValue;

	CQPCorpus corpus;

	List<SearchEngine> engines;

	LinkedHashMap<String, ArrayList<SearchEngine>> options = new LinkedHashMap<String, ArrayList<SearchEngine>>();

	private Label selectOptionsLabel;

	private Group optionsGroup;

	private Button button;

	public QueryOptionsDialog(Button settingsButton, CQPCorpus corpus, List<SearchEngine> availableEngines, SearchEngine defaultValue, Map<? extends String, ? extends String> currentQueryOptions) {
		super(settingsButton.getDisplay().getActiveShell());

		this.button = settingsButton;
		this.corpus = corpus;
		this.engines = availableEngines;
		this.parentShell = settingsButton.getDisplay().getActiveShell();
		this.queryOptions.clear();
		this.queryOptions.putAll(currentQueryOptions);
		this.setShellStyle(SWT.NONE);

		this.values = new ArrayList<SearchEngine>();
		for (SearchEngine se : availableEngines) {

			if (se.hasIndexes(corpus)) { // show only if corpus has indexes AND if the se is not an option of CQPSearchEngine
				if (se.getOptionForSearchengine() == null) {
					values.add(se);
				}
				else {
					if (!options.containsKey(se.getOptionForSearchengine())) {
						options.put(se.getOptionForSearchengine(), new ArrayList<SearchEngine>());
					}
					options.get(se.getOptionForSearchengine()).add(se);
				}
			}
		}
		this.defaultValue = defaultValue;


	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);

		
		//newShell.addListener( SWT.Deactivate, event -> newShell.close() ); // don't work when the combo opens its menu

		//newShell.setSize(400, 300);

		if (button != null) {
			newShell.setLocation(newShell.getDisplay().map(button, null, 0, button.getSize().y));
			//if (!OSDetector.isFamilyUnix()) newShell.addListener(SWT.Deactivate, event -> okPressed());
			
			newShell.addListener(SWT.Deactivate, new Listener() {

				@Override
				public void handleEvent(Event event) {
					for (Combo c : queryOptionCombos) {
						if (c.getListVisible()) return;
					}
					if (!combo.getCombo().getListVisible()) {
						okPressed(); // behva like a combo since there is no OK/Cancel button
					}
				}
			});
		}
		//newShell.setLocation(button.getLocation().x, button.getLocation().y + button.getSize().y);
		//System.out.println("L: "+button.getLocation());
		//newShell.setBounds(button.setL().x, button.getBounds().y, 400, 400);
	}

	@Override
	protected Point getInitialLocation(Point initialSize) {
		if (button != null) {
			return button.getDisplay().map(button, null, 0, button.getSize().y);
		}
		return super.getInitialLocation(initialSize);
	}


	@Override
	protected boolean isResizable() {
		return true;
	}

	Composite p;

	private HashMap<String, String> queryOptions = new HashMap<String, String>();

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite p) {

		this.p = p;

		//System.out.println(p.getLayout());
		GridLayout layout = (GridLayout) p.getLayout();
		layout.verticalSpacing = 5;
		layout.marginBottom = 5;
		layout.marginTop = 5;
		layout.marginLeft = 5;
		layout.marginRight = 5;

		Label l = new Label(p, SWT.NONE);
		l.setText(TXMUIMessages.selectEngine);
		l.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 1, 1));
		combo = new ComboViewer(new Combo(p, SWT.READ_ONLY | SWT.SINGLE));
		combo.getCombo().setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 1, 1));
		//combo.setItems(values.toArray(new String[] {}));
		combo.getCombo().addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				updateOptions();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		combo.setLabelProvider(new SimpleLabelProvider() {

			/**
			 * The <code>LabelProvider</code> implementation of this
			 * <code>ILabelProvider</code> method returns the element's
			 * <code>toString</code> string. Subclasses may override.
			 */
			@Override
			public String getText(Object element) {
				return element == null ? "" : ((SearchEngine) element).getName(); //$NON-NLS-1$
			}

		});
		combo.setContentProvider(new ArrayContentProvider());
		l.setVisible(values.size() > 1);
		combo.getCombo().setVisible(values.size() > 1);
		((GridData) combo.getCombo().getLayoutData()).exclude = values.size() <= 1;
		((GridData) l.getLayoutData()).exclude = values.size() <= 1;
		
		combo.setInput(values);

		if (defaultValue != null) {
			selectValue(defaultValue);
		}

		optionsGroup = new Group(p, SWT.BORDER);
		optionsGroup.setLayout(new GridLayout(2, false));
		optionsGroup.setText(TXMUIMessages.Options);
		optionsGroup.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, true, 1, 1));

		updateOptions();
		p.layout();
		return p;
	}

	HashSet<Combo> queryOptionCombos = new HashSet<>();
	public void updateOptions() {

		Object o = combo.getStructuredSelection().getFirstElement();
		if (o == null) {
			return; // nothing to do
		}
		if (o == selectedEngine) {
			return; // nothing to do
		}

		if (optionsGroup != null) {
			for (Control c : optionsGroup.getChildren()) {
				c.dispose();
			}
		}

		selectedEngine = (SearchEngine) o;

		if (options.containsKey(selectedEngine.getName())) {

			GridData data = ((GridData) optionsGroup.getLayoutData());

			for (final SearchEngine se : options.get(selectedEngine.getName())) {

				Button option = new Button(optionsGroup, SWT.CHECK);
				option.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 2, 1));
				option.setText(se.getOptionNameForSearchengine() + " : " + se.getOptionDescriptionForSearchengine()); //$NON-NLS-1$
				if (se.equals(defaultValue)) {
					option.setSelection(true);
					selectedEngine = defaultValue;
				}

				option.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {

						Button b = (Button) e.widget;
						if (b.getSelection()) {
							selectedEngine = se;
						}
						else {
							selectedEngine = (SearchEngine) combo.getStructuredSelection().getFirstElement();
						}
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});

			}
		}

		HashMap<String, List<String>> availableQueryOptions = selectedEngine.getAvailableQueryOptions();
		queryOptionCombos.clear();
		for (String optionName : availableQueryOptions.keySet()) {

			Label l = new Label(optionsGroup, SWT.NONE);
			l.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 1, 1));
			l.setText(optionName);

			if (availableQueryOptions.get(optionName).size() > 0) {
				Combo option = new Combo(optionsGroup, SWT.READ_ONLY);
				option.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 1, 1));
				option.setToolTipText(optionName);
				option.setItems(availableQueryOptions.get(optionName).toArray(new String[availableQueryOptions.get(optionName).size()]));
				queryOptionCombos.add(option);
				if (queryOptions.containsKey(optionName)) {
					option.setText(queryOptions.get(optionName));
				}

				option.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						queryOptions.put(optionName, option.getText());
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});

			}
			else {
				Text option = new Text(optionsGroup, SWT.BORDER);
				option.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 1, 1));
				option.setToolTipText(optionName);
				if (queryOptions.containsKey(optionName)) option.setText(queryOptions.get(optionName));
				option.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						queryOptions.put(optionName, option.getText());
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});
			}
		}

		optionsGroup.setVisible(optionsGroup.getChildren().length > 0);
		optionsGroup.getParent().pack();
		if (optionsGroup.getChildren().length == 0) {
			new Label(p, SWT.NONE).setText("No query option available");
		}

		//this.getShell().redraw();
		this.getShell().pack();
		//this.getShell().layout(true);
	}

	/**
	 * Gets the selected value.
	 *
	 * @return the selected value
	 */
	public SearchEngine getSelectedEngine() {
		return selectedEngine;
	}

	public void selectValue(SearchEngine e) {

		if (values.contains(e)) {
			combo.setSelection(new StructuredSelection(e));
		}
		else {
			SearchEngine optionedengine = SearchEnginesManager.getSearchEngine(e.getOptionForSearchengine());
			if (optionedengine != null) {
				combo.setSelection(new StructuredSelection(optionedengine));
			}
			else {
				System.out.println("ERROR: engine not found: " + e.getOptionForSearchengine()); //$NON-NLS-1$
			}
		}
	}

	public HashMap<String, String> getOptions() {

		return queryOptions;
	}
}
