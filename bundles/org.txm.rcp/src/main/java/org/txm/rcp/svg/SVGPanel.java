// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.svg;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.util.List;

import javax.swing.JPanel;

import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.swing.gvt.AbstractImageZoomInteractor;
import org.apache.batik.swing.gvt.AbstractPanInteractor;
import org.apache.batik.swing.gvt.AbstractResetTransformInteractor;
import org.apache.batik.swing.gvt.AbstractZoomInteractor;
import org.apache.batik.swing.gvt.Interactor;

// TODO: Auto-generated Javadoc
/**
 * The Class SVGPanel.
 */
//FIXME: SJ: this class should be linked to /org.txm.chartsengine.svgbatik.rcp/src/org/txm/chartsengine/svgbatik/rcp/swing/SVGCanvas.java (inheritance of the charts engine class from this one)
@Deprecated
public class SVGPanel extends JPanel {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4381954167495751486L;

	// The SVG canvas.
	/** The svg canvas. */
	protected JSVGCanvas svgCanvas = new JSVGCanvas();

	/** The frame. */
	private Frame frame;

	// ----------------------------------------------------------------------
	// Interactors
	// ----------------------------------------------------------------------

	/**
	 * An interactor to perform a zoom.
	 * <p>
	 * Binding: BUTTON1 + CTRL Key
	 * </p>
	 */
	protected Interactor zoomInteractor = new AbstractZoomInteractor() {

		@Override
		public boolean startInteraction(InputEvent ie) {
			int mods = ie.getModifiers();
			return ie.getID() == MouseEvent.MOUSE_PRESSED &&
					(mods & InputEvent.BUTTON1_MASK) != 0 &&
					(mods & InputEvent.CTRL_MASK) != 0;
		}
	};

	/**
	 * An interactor to perform a realtime zoom.
	 * <p>
	 * Binding: BUTTON3 + SHif T Key
	 * </p>
	 */
	protected Interactor imageZoomInteractor = new AbstractImageZoomInteractor() {

		@Override
		public boolean startInteraction(InputEvent ie) {
			int mods = ie.getModifiers();
			return ie.getID() == MouseEvent.MOUSE_PRESSED &&
					(mods & InputEvent.BUTTON3_MASK) != 0 &&
					(mods & InputEvent.SHIFT_MASK) != 0;
		}
	};

	/**
	 * An interactor to perform a translation.
	 * <p>
	 * Binding: BUTTON1 + SHif T Key
	 * </p>
	 */
	protected Interactor panInteractor = new AbstractPanInteractor() {

		@Override
		public boolean startInteraction(InputEvent ie) {
			int mods = ie.getModifiers();
			return ie.getID() == MouseEvent.MOUSE_PRESSED &&
					(mods & InputEvent.BUTTON3_MASK) != 0 &&
					(mods & InputEvent.SHIFT_MASK) != 1;
		}
	};

	/**
	 * An interactor to reset the rendering transform.
	 * <p>
	 * Binding: CTRL+SHif T+BUTTON3
	 * </p>
	 */
	protected Interactor resetTransformInteractor = new AbstractResetTransformInteractor() {

		@Override
		public boolean startInteraction(InputEvent ie) {
			int mods = ie.getModifiers();
			return ie.getID() == MouseEvent.MOUSE_CLICKED &&
					(mods & InputEvent.BUTTON2_MASK) != 0 &&
					(mods & InputEvent.SHIFT_MASK) != 1 &&
					(mods & InputEvent.CTRL_MASK) != 1;
		}
	};


	/** The zoom. */
	int zoom = 0;

	/**
	 * Instantiates a new sVG panel.
	 *
	 * @param frame2 the frame2
	 */
	public SVGPanel(Frame frame2) {
		super(new BorderLayout());
		this.frame = frame2;

		this.add("Center", svgCanvas); //$NON-NLS-1$

		List<Interactor> list = svgCanvas.getInteractors();
		list.clear();
		list.add(zoomInteractor);
		list.add(imageZoomInteractor);
		list.add(panInteractor);
		list.add(resetTransformInteractor);

		this.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (e.getUnitsToScroll() < 0) {
					zoom++;
					//System.out.println("in");
					AffineTransform at;
					JPanel c = (JPanel) e.getSource();

					int xStart = e.getX();
					int yStart = e.getY();

					at = AffineTransform.getTranslateInstance(xStart, yStart);
					int dy = zoom;
					double s;
					if (dy < 0) {
						s = -1f / dy;
					}
					else {
						s = dy;
					}

					at.scale(s, s);
					at.translate(-xStart, -yStart);
					if (at.getDeterminant() != 0)
						svgCanvas.setRenderingTransform(at);
				}
				else {
					zoom--;
					//System.out.println("out");
					AffineTransform at;
					JPanel c = (JPanel) e.getSource();

					int xStart = e.getX();
					int yStart = e.getY();

					at = AffineTransform.getTranslateInstance(xStart, yStart);
					int dy = zoom;
					double s;
					if (dy < 0) {
						s = -1f / dy;
					}
					else {
						s = dy;
					}

					at.scale(s, s);
					at.translate(-xStart, -yStart);
					if (at.getDeterminant() != 0)
						svgCanvas.setRenderingTransform(at);
				}
			}
		});

	}

	/**
	 * Reset zoom.
	 */
	public void resetZoom() {
		AffineTransform trans = AffineTransform.getTranslateInstance(0, 0);
		trans.scale(1, 1);
		svgCanvas.setRenderingTransform(trans);

	}

	/**
	 * Load svg document.
	 *
	 * @param url the url
	 */
	public void loadSVGDocument(String url) {
		svgCanvas.show();
		svgCanvas.loadSVGDocument(url);
		frame.pack();
	}
}
