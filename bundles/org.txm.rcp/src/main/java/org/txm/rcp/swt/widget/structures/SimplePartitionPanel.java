package org.txm.rcp.swt.widget.structures;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.StructuredPartition;
import org.txm.utils.logger.Log;

public class SimplePartitionPanel extends Composite implements PartitionParameterGenerator {

	// For the "simple" tab
	/** The selected structural unit. */
	private StructuralUnit selectedStructuralUnit;

	/** The selected structural unit property. */
	private StructuralUnitProperty selectedStructuralUnitProperty;

	/** The structural units combo. */
	private Combo structuralUnitsCombo;

	/** The structural units final. */
	private List<StructuralUnit> structuralUnitsFinal;

	/** The property combo. */
	private Combo propertyCombo;

	private CQPCorpus corpus;

	public SimplePartitionPanel(Composite parent, int style, CQPCorpus corpus) {
		super(parent, style);

		this.corpus = corpus;

		GridLayout layout = new GridLayout(2, false);
		this.setLayout(layout);

		// StructuralUnit
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		Label structureLabel = new Label(this, SWT.NONE);
		structureLabel.setText(TXMUIMessages.ampStructureColon);
		structureLabel.setLayoutData(new GridData(GridData.END, GridData.CENTER, false, false));

		structuralUnitsCombo = new Combo(this, SWT.READ_ONLY);
		structuralUnitsCombo.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));

		List<StructuralUnit> structuralUnits = null;
		try {
			structuralUnits = corpus.getOrderedStructuralUnits();
		}
		catch (CqiClientException e) {
			Log.severe(TXMCoreMessages.bind(TXMCoreMessages.error_errorP0, Log.toString(e)));
		}
		structuralUnitsFinal = new ArrayList<>();
		for (StructuralUnit unit : structuralUnits) {
			if (unit.getName().equals("txmcorpus")) continue; // ignore txmcorpus structure //$NON-NLS-1$

			if (unit.getOrderedProperties() != null && unit.getOrderedProperties().size() != 0) {
				/*
				 * if ((unit.getName().equals("text") &&
				 * unit.getProperties().size() > 3)) //$NON-NLS-1$ {
				 * structuralUnitsCombo.add(unit.getName());
				 * structuralUnitsFinal.add(unit); } else
				 * if (!unit.getName().equals("text")) //$NON-NLS-1$ {
				 */
				structuralUnitsCombo.add(unit.getName());
				structuralUnitsFinal.add(unit);
				// }
			}
		}

		if (structuralUnitsCombo.getItemCount() == 0) {

		}
		else {
			structuralUnitsCombo.select(0);
			String[] items = structuralUnitsCombo.getItems();
			for (int i = 0; i < items.length; i++) {
				if (items[i].equals("text")) { //$NON-NLS-1$
					structuralUnitsCombo.select(i);
					break;
				}
			}
			selectedStructuralUnit = structuralUnitsFinal.get(0);
		}
		// combo.setSize (200, 200);

		// StructuralUnitProperty
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		Label propertyLabel = new Label(this, SWT.NONE);
		propertyLabel.setText(TXMUIMessages.ampPropertyColon);
		propertyLabel.setLayoutData(new GridData(GridData.END, GridData.CENTER, false, false));

		propertyCombo = new Combo(this, SWT.READ_ONLY);
		propertyCombo.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));

		// combo.setSize (200, 200);
		structuralUnitsCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent event) {

				reloadProperties();
			}
		});

		for (StructuralUnitProperty property : structuralUnitsFinal.get(0).getOrderedProperties()) {
			if (!(structuralUnitsFinal.get(0).getName().equals("text") &&  //$NON-NLS-1$
					(property.getName().equals("project") || property.getName().equals("base")))) //$NON-NLS-1$ //$NON-NLS-2$
				propertyCombo.add(property.getName());
		}
		propertyCombo.select(0);
		selectedStructuralUnitProperty = selectedStructuralUnit.getOrderedProperties().get(0);

		propertyCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent event) {

				selectedStructuralUnitProperty = selectedStructuralUnit.getOrderedProperties().get(propertyCombo.getSelectionIndex());
			}
		});

		reloadProperties();
	}

	/**
	 * Reload values.
	 */
	protected void reloadValues() {

		List<StructuralUnitProperty> properties = null;
		selectedStructuralUnit = structuralUnitsFinal.get(structuralUnitsCombo.getSelectionIndex());

		properties = selectedStructuralUnit.getOrderedProperties();
		propertyCombo.removeAll();
		for (StructuralUnitProperty property : properties) {
			/*
			 * if (!(selectedStructuralUnit.getName().equals("text") &&
			 * //$NON-NLS-1$ (property.getName().equals("id") || //$NON-NLS-1$
			 * property.getName().equals("base") || //$NON-NLS-1$
			 * property.getName().equals("project")))) //$NON-NLS-1$
			 */
			if (selectedStructuralUnit.getName().equals("text") && //$NON-NLS-1$
					(property.getName().equals("project") || property.getName().equals("base"))) //$NON-NLS-1$//$NON-NLS-2$
				continue; // ignore technical properties  
			propertyCombo.add(property.getName());
		}
		if (propertyCombo.getItemCount() > 0) {
			propertyCombo.select(0);
			String[] items = propertyCombo.getItems();
			for (int i = 0; i < items.length; i++) {
				if (items[i].equals("id")) { //$NON-NLS-1$
					propertyCombo.select(i);
					break;
				}
			}
			selectedStructuralUnitProperty = selectedStructuralUnit
					.getOrderedProperties().get(0);
			// this.getButton(IDialogConstants.OK_ID).setEnabled(true);
		}
	}

	/**
	 * Reload properties.
	 */
	protected void reloadProperties() {

		selectedStructuralUnit = structuralUnitsFinal.get(structuralUnitsCombo.getSelectionIndex());
		List<StructuralUnitProperty> properties = null;
		selectedStructuralUnit = structuralUnitsFinal.get(structuralUnitsCombo.getSelectionIndex());

		properties = selectedStructuralUnit.getOrderedProperties();
		propertyCombo.removeAll();
		for (StructuralUnitProperty property : properties) {
			/*
			 * if (!(selectedStructuralUnit.getName().equals("text") &&
			 * //$NON-NLS-1$ (property.getName().equals("id") || //$NON-NLS-1$
			 * property.getName().equals("base") || //$NON-NLS-1$
			 * property.getName().equals("project")))) //$NON-NLS-1$
			 */
			if (selectedStructuralUnit.getName().equals("text") && //$NON-NLS-1$
					(property.getName().equals("project") || property.getName().equals("base"))) //$NON-NLS-1$//$NON-NLS-2$
				continue; // ignore technical properties  
			propertyCombo.add(property.getName());
		}
		if (propertyCombo.getItemCount() > 0) {
			propertyCombo.select(0);
			String[] items = propertyCombo.getItems();
			for (int i = 0; i < items.length; i++) {
				if (items[i].equals("id")) { //$NON-NLS-1$
					propertyCombo.select(i);
					break;
				}
			}
			selectedStructuralUnitProperty = selectedStructuralUnit
					.getOrderedProperties().get(0);
			// this.getButton(IDialogConstants.OK_ID).setEnabled(true);
		}
	}

	/**
	 * Gets the structural unit.
	 *
	 * @return the structural unit
	 */
	public StructuralUnit getStructuralUnit() {

		return selectedStructuralUnit;
	}

	/**
	 * Gets the structural unit property.
	 *
	 * @return the structural unit property
	 */
	public StructuralUnitProperty getStructuralUnitProperty() {

		return selectedStructuralUnitProperty;
	}

	public Partition createPartition(String name, Partition partition) {

		// Simple mode
		StructuralUnit structuralUnit = this.structuralUnitsFinal.get(this.structuralUnitsCombo.getSelectionIndex());
		StructuralUnitProperty property = structuralUnit.getProperty(this.propertyCombo.getText());
		try {
			// auto-naming if needed
			if (name.isEmpty()) {
				name = property.asFullNameString();
			}

			if (partition == null) {
				partition = new StructuredPartition(corpus);
			}

			if (partition instanceof StructuredPartition spartition) {
				spartition.setParameters(name, property, null);
			}
			else {
				partition.removeAllParts();
				List<String> pValues = property.getValues(corpus);
				partition.setParameters(name, pValues, pValues);
			}
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return null;
		}

		return partition;
	}
}
