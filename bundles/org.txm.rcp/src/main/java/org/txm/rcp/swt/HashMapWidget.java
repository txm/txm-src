package org.txm.rcp.swt;

import java.util.LinkedHashMap;
import java.util.List;

import org.eclipse.equinox.internal.p2.ui.discovery.util.SelectionProviderAdapter;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.HashMapDialog;

/**
 * Widget: label + button to open the selector.
 * 
 * @author mdecorde
 *
 */
public class HashMapWidget extends GLComposite {

	Label valueLabel;

	Button changeValues;

	LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();

	LinkedHashMap<String, String> defaultValues = new LinkedHashMap<String, String>();

	LinkedHashMap<String, List<String>> availableValues = new LinkedHashMap<>();

	private ComputeSelectionListener computeSelectionListener;

	public HashMapWidget(Composite parent, int style, String label, LinkedHashMap<String, List<String>> availableValues) {

		super(parent, style);

		this.availableValues = availableValues;

		new Label(parent, SWT.NONE).setText(label);

		valueLabel = new Label(parent, SWT.NONE);
		valueLabel.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {

				HashMapDialog dialog = new HashMapDialog(e.display.getActiveShell(), label, defaultValues, values, availableValues);
				if (dialog.open() == HashMapDialog.OK) {
					values = dialog.getValues();
					updateLabel();
				}

			}
		});

		changeValues = new Button(parent, SWT.PUSH);
		changeValues.setText(TXMUIMessages.edit);
		changeValues.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				HashMapDialog dialog = new HashMapDialog(e.display.getActiveShell(), label, defaultValues, values, availableValues);
				if (dialog.open() == HashMapDialog.OK) {
					values = dialog.getValues();
					updateLabel();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}

	public void setDefaultValues(LinkedHashMap<String, String> values) {

		this.defaultValues.clear();
		if (defaultValues == null) return;

		this.defaultValues.putAll(values);
	}

	public void setValues(LinkedHashMap<String, String> values) {

		this.values.clear();
		if (values == null) return;

		this.values.putAll(values);
		updateLabel();
	}

	protected void updateLabel() {

		if (valueLabel.isDisposed()) return;

		String s = "";
		for (String k : values.keySet()) {
			if (values.get(k) != null && values.get(k).length() > 0) {
				if (s.length() > 0) s += ", ";
				s += k + "=" + values.get(k);
			}
		}
		if( valueLabel.getText().equals(s)) {
			return;
		}
		
		if (computeSelectionListener != null) {
			computeSelectionListener.selectionChanged(new SelectionChangedEvent(new SelectionProviderAdapter(), new StructuredSelection()));
		}
		valueLabel.setText(s);
		if (s == null || s.length() == 0) {
			valueLabel.setText("<none>");
		}
		valueLabel.getParent().layout(true);
		valueLabel.getParent().getParent().layout(true);

	}

	public LinkedHashMap<String, String> getValues() {

		return new LinkedHashMap<>(values);
	}

	public LinkedHashMap<String, String> getDefaultValues() {

		return new LinkedHashMap<>(defaultValues);
	}

	public void addSelectionListener(ComputeSelectionListener computeSelectionListener) {
		this.computeSelectionListener = computeSelectionListener;
	}
	
	public void removeSelectionListener(ComputeSelectionListener computeSelectionListener) {
		this.computeSelectionListener = null;
	}
}
