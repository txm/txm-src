// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views.corpora;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.DecoratingLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.contexts.IContextService;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.part.ViewPart;
import org.txm.Toolbox;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.TXMResult;
import org.txm.objects.Workspace;
import org.txm.rcp.CorporaSourceProvider;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.StatusLine;
import org.txm.rcp.commands.workspace.OpenCorpus;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.handlers.results.CopyTXMResultTree;
import org.txm.rcp.handlers.results.CutTXMResultTree;
import org.txm.rcp.handlers.results.DeleteObject;
import org.txm.rcp.handlers.results.PasteTXMResultTree;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.utils.logger.Log;

//import org.txm.functions.queryindex.QueryIndex;
//import org.txm.rcp.editors.input.QueryIndexEditorInput;
//import org.txm.rcp.editors.queryindex.QueryIndexEditor;
/**
 * This sample class demonstrates how to plug-in a new workbench view. The view
 * shows data obtained from the model. The sample creates a dummy model on the
 * fly, but a real implementation would connect to the model available either in
 * this or another plug-in (e.g. the workspace). The view is connected to the
 * model using a content provider.
 * <p>
 * The view uses a label provider to define how model objects should be
 * presented in the view. Each view can present the same model objects using
 * different labels and icons, if needed. Alternatively, a single label provider
 * can be shared between views in order to ensure that objects of the same type
 * are presented in the same way everywhere.
 * <p>
 */
public class CorporaView extends ViewPart {

	/** The ID. */
	static public String ID = CorporaView.class.getName();

	/** The tree viewer. */
	public TreeViewer treeViewer;

	/**
	 * stores the class that are managed by a Doubleclick handler
	 */
	// private static HashMap<Class, Class> managedDoubleClickClasses = new HashMap<Class, Class>();

	public static HashMap<String, String> doubleClickInstalledCommands = new HashMap<>();

	/**
	 * Gets the single instance of CorporaView.
	 *
	 * @return single instance of CorporaView
	 */
	public static CorporaView getInstance() {

		if (!PlatformUI.isWorkbenchRunning()) return null;

		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window == null) return null;
		IWorkbenchPage page = window.getActivePage();
		if (page == null) return null;

		return (CorporaView) page.findView(CorporaView.ID);
	}

	public TreeViewer getTreeViewer() {

		return treeViewer;
	}


	/**
	 * Reload.
	 */
	public static void reload() {
		// System.out.println("Reload corpora view");
		CorporaView corporaView = getInstance();
		if (corporaView != null) {
			corporaView._reload();
		}
	}

	/**
	 * _reload.
	 */
	public void _reload() {

		if (Toolbox.isInitialized()) {

			treeViewer.setContentProvider(new CorpusViewContentProvider(this));
			ColumnViewerToolTipSupport.enableFor(treeViewer);

			treeViewer.setLabelProvider(new CorpusViewLabelProvider(new WorkbenchLabelProvider(), PlatformUI.getWorkbench().getDecoratorManager().getLabelDecorator()));

			Workspace w = Toolbox.workspace;
			if (w == null) {
				return;
			}
			treeViewer.setInput(w);
			treeViewer.refresh();
		}
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 *
	 * @param parent the parent
	 */
	@Override
	public void createPartControl(Composite parent) {

		treeViewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		PlatformUI.getWorkbench().getHelpSystem().setHelp(treeViewer.getControl(), TXMUIMessages.corpus);
		getSite().setSelectionProvider(treeViewer);
		treeViewer.setColumnProperties(new String[] { "name" }); // mandatory to enable edit mode -> used to identify columns //$NON-NLS-1$

		treeViewer.setContentProvider(new CorpusViewContentProvider(this));
		treeViewer.setLabelProvider(new DecoratingLabelProvider(new WorkbenchLabelProvider(), PlatformUI.getWorkbench().getDecoratorManager().getLabelDecorator()));

		int operations = DND.DROP_COPY | DND.DROP_MOVE;
		Transfer[] transferTypes = new Transfer[]{TextTransfer.getInstance(), LocalSelectionTransfer.getTransfer()};
		treeViewer.addDropSupport(operations, transferTypes, new CorporaViewDropListener(this.getTreeViewer()));
		
		final DragSource source = new DragSource (treeViewer.getTree(), DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_LINK);
		source.setTransfer(new Transfer[] { LocalSelectionTransfer.getTransfer()});
		
		source.addDragListener (new DragSourceAdapter() {
			
			@Override
			public void dragSetData(DragSourceEvent event) {
				LocalSelectionTransfer transfer = LocalSelectionTransfer.getTransfer();
				if (transfer.isSupportedType(event.dataType)) {
					transfer.setSelection(treeViewer.getStructuredSelection());
					transfer.setSelectionSetTime(event.time & 0xFFFF);
				}
			}
		});
		
		// for now the sorter is used to sort the Corpus, Subcorpus and partition objects
		treeViewer.setSorter(new ViewerSorter() {

			@Override
			public int category(Object element) {
				if (element instanceof Subcorpus) return 2; // must do it before the Corpus test
				else if (element instanceof CQPCorpus) return 0;
				else if (element instanceof Partition) return 1;
				else return 3;
			}

			@Override
			public int compare(Viewer viewer, Object e1, Object e2) {

				int cat1 = category(e1);
				int cat2 = category(e2);

				if (cat1 != cat2) {
					return cat1 - cat2;
				}

				if (e1 instanceof TXMResult && e2 instanceof TXMResult) {
					return ((TXMResult) e1).compareTo((TXMResult) e2);
				}
				else {
					return super.compare(viewer, e1, e2);
				}
			}
		});

		treeViewer.setInput(Toolbox.workspace);

		// FIXME: SJ: to remove?
		// PlatformUI.getWorkbench().getHelpSystem().setHelp(treeViewer.getTree(), "org.txm.rcp.corporaView" //$NON-NLS-1$);

		treeViewer.getTree().addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				TreeItem[] sel = treeViewer.getTree().getSelection();
				if (sel.length == 0)
					return;

				CorporaSourceProvider.updateSelection(treeViewer.getSelection());

				CorporaView.this.setFocus(); // force focus on the corpora view -> ensure the active selection is the corpora view selection

				ISelection tsel = treeViewer.getSelection();
				if (!(tsel instanceof TreeSelection)) return;

				TreeSelection selection = (TreeSelection) treeViewer.getSelection();
				Object selectedItem = selection.getFirstElement();

				// FIXME: SJ tests, define here what to display
				if (selectedItem instanceof TXMResult) {
					TXMResult result = ((TXMResult) selectedItem);
					// String mess = result.getSimpleDetails();
					// if (mess == null) {
					// mess = result.getCurrentName();
					// }
					// StatusLine.setMessage(mess);
					// StatusLine.setMessage(((TXMResult)selectedItem).getComputingDoneMessage());

					if (result instanceof MainCorpus) { // just for this result type, pre-compute to show the corpus size
						try {
							result.compute(false);
						}
						catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}

					// Status bar
					//					if (!result.hasBeenComputedOnce() && !(result instanceof Partition)) { // FIXME: SJ: tmp fix for showing Partition parts
					//						StatusLine.setMessage("");
					//					}
					//					else {
					//						// TODO uncomment when status line will be restored
					//						// StatusLine.setMessage(((TXMResult) selectedItem).getDetails());
					//					}

					// FIXME : MD re-enable status line on corpora view selection
					if (selectedItem instanceof TXMResult) {

						TXMResult r = ((TXMResult) selectedItem);
						String mess = null;
						try {
							mess = r.getSimpleDetails();
						}
						catch (Exception e2) {
							Log.warning(NLS.bind(TXMUIMessages.errorWhileGettingResultInformationsP0, e2));
							Log.printStackTrace(e2);
						}

						if (mess == null) mess = r.getName();
						StatusLine.setMessage(mess);
					}
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});


		// double click listener installation from commands
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		ICommandService cmdService = window.getService(ICommandService.class);
		Command[] cmds = cmdService.getDefinedCommands();
		for (int i = 0; i < cmds.length; i++) {
			try {
				if (cmds[i].getReturnType() != null) { // RCP hack using the command return type to associate the selection class with the command
					Log.fine("CorporaView.createPartControl(): registering command " + cmds[i].getId() + " for TXMResult of type " + cmds[i].getReturnType().getId() + ".");  //$NON-NLS-1$ //$NON-NLS-2$//$NON-NLS-3$
					doubleClickInstalledCommands.put(cmds[i].getReturnType().getId(), cmds[i].getId());
				}
			}
			catch (NotDefinedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		treeViewer.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				TreeSelection selection = (TreeSelection) treeViewer.getSelection();
				Object selectedItem = selection.getFirstElement();
				if (selectedItem != null) {
					String commandId = doubleClickInstalledCommands.get(selectedItem.getClass().getName());

					if (commandId == null) {
						for (String clazz : doubleClickInstalledCommands.keySet()) {
							if (selectedItem.getClass().getSuperclass().getName().equals(clazz)) {
								commandId = doubleClickInstalledCommands.get(clazz);
								break;
							}
						}
					}

					if (commandId != null) {
						BaseAbstractHandler.executeCommand(commandId);
					}
					else if (selectedItem instanceof IProject) {
						OpenCorpus.open((IProject) selectedItem);
					}
					else {
						if (treeViewer.getExpandedState(selectedItem)) {
							treeViewer.collapseToLevel(selectedItem, 99);
						}
						else {
							treeViewer.expandToLevel(selectedItem, 99);
						}
					}
				}
			}
		});

		treeViewer.getTree().addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {

				boolean expert = TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER);

				if (expert && (char) e.keyCode == 'c' & ((e.stateMask & SWT.CTRL) != 0)) {
					Object o = treeViewer.getStructuredSelection().getFirstElement();
					if (o != null && o instanceof TXMResult) {
						CopyTXMResultTree.copy((IStructuredSelection) treeViewer.getSelection());
					}
				}
				else if (expert && (char) e.keyCode == 'x' & ((e.stateMask & SWT.CTRL) != 0)) {
					Object o = treeViewer.getStructuredSelection().getFirstElement();
					if (o != null && o instanceof TXMResult) {
						CutTXMResultTree.cut((IStructuredSelection) treeViewer.getSelection());
					}
				}
				else if (expert && (char) e.keyCode == 'v' & ((e.stateMask & SWT.CTRL) != 0)) {
					Object o = treeViewer.getStructuredSelection().getFirstElement();
					if (o != null && o instanceof TXMResult) {
						PasteTXMResultTree.paste((TXMResult) o);
					}
				}
				else if (e.keyCode == SWT.DEL) {
					final ISelection sel = treeViewer.getSelection();
					if (sel instanceof IStructuredSelection) {

						if (!DeleteObject.askContinueToDelete((IStructuredSelection) sel)) return;

						JobHandler job = new JobHandler(TXMUIMessages.deleting, true) {

							@Override
							protected IStatus _run(SubMonitor monitor) {
								DeleteObject.delete(sel);
								this.syncExec(new Runnable() {

									@Override
									public void run() {
										refresh();
									}
								});
								return Status.OK_STATUS;
							}
						};
						job.schedule();
						try {
							job.join();
						}
						catch (InterruptedException ex) {
							ex.printStackTrace();
						}
					}
				}
				// else if (arg0.keyCode == SWT.CR || arg0.keyCode == SWT.KEYPAD_CR) {
				//
				// ISelection isel = treeViewer.getSelection();
				// if (isel instanceof StructuredSelection) {
				// System.out.println("EDIT! "+((StructuredSelection)isel).getFirstElement());
				// treeViewer.editElement(((StructuredSelection)isel).getFirstElement(), 10);
				// }
				//
				// }
			}
		});

		final int IMAGE_MARGIN = 2;
		final Image icon = IImageKeys.getImage("icons/decorators/bullet_green.png"); //$NON-NLS-1$
		final Image icon_lock = IImageKeys.getImage("icons/decorators/lock.png"); //$NON-NLS-1$

		// FIXME: test to show icon for results that are user persistable
		// final Image icon = AbstractUIPlugin.imageDescriptorFromPlugin("org.eclipse.gef","platform:/plugin/org.eclipse.gef/org/eclipse/gef/internal/icons/pinned.gif").createImage();
		// final Image icon = AbstractUIPlugin.imageDescriptorFromPlugin("org.eclipse.team.svn.help","platform:/plugin/org.eclipse.team.svn.help/images/lock.gif").createImage();



		// treeViewer.getTree().addListener(SWT.MeasureItem, new Listener() {
		// public void handleEvent(Event event) {
		// TreeItem item = (TreeItem)event.item;
		// Image trailingImage = (Image)item.getImage();
		// if (trailingImage != null) {
		// event.width += trailingImage.getBounds().width + IMAGE_MARGIN;
		// }
		// }
		// });
		treeViewer.getTree().addListener(SWT.PaintItem, new Listener() {

			@Override
			public void handleEvent(Event event) {
				if (event.item.getData() instanceof TXMResult && ((TXMResult) event.item.getData()).isLocked()) {
					TreeItem item = (TreeItem) event.item;
					Image trailingImage = item.getImage();
					if (trailingImage != null) {
						int x = 3 + event.x + event.width;
						int itemHeight = treeViewer.getTree().getItemHeight();
						int imageHeight = trailingImage.getBounds().height;
						int y = 2 + event.y + (itemHeight - imageHeight) / 2;

						event.gc.drawImage(icon_lock, x, y);
					}
				}
			}
		});

		MenuManager menuManager = new MenuManager();
		Menu menu = menuManager.createContextMenu(treeViewer.getTree());

		// Set the MenuManager
		treeViewer.getTree().setMenu(menu);
		getSite().registerContextMenu(menuManager, treeViewer);
		// Make the selection available
		getSite().setSelectionProvider(treeViewer);

		IContextService contextService = getSite().getService(IContextService.class);
		contextService.activateContext(ID);

		treeViewer.refresh();
	}


	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {

		treeViewer.getControl().setFocus();
		CorporaSourceProvider.updateSelection(treeViewer.getSelection());
	}

	/**
	 * Gets the first selected object in the tree.
	 * 
	 * @return
	 */
	public static Object getFirstSelectedObject() {

		CorporaView corporaView = getInstance();
		if (corporaView != null) {
			try {
				ISelection selection = corporaView.treeViewer.getSelection();
				if (selection != null & selection instanceof IStructuredSelection) {
					return ((IStructuredSelection) selection).getFirstElement();
				}
			}
			catch (SWTException e) {
			}
		}
		return null;
	}

	/**
	 * Gets the selected objects in the tree.
	 * 
	 * @return
	 */
	public static List<?> getSelectedObjects() {

		CorporaView corporaView = getInstance();
		if (corporaView != null) {
			try {
				ISelection selection = corporaView.treeViewer.getSelection();
				if (selection != null & selection instanceof IStructuredSelection) {
					return ((IStructuredSelection) selection).toList();
				}
			}
			catch (SWTException e) {
			}
		}
		return null;
	}

	public static IStructuredSelection getSelection() {
		CorporaView corporaView = getInstance();
		if (corporaView != null) {
			return corporaView.treeViewer.getStructuredSelection();
		}
		return new StructuredSelection();
	}

	/**
	 * Selects some items.
	 * 
	 * @param arrayList
	 */
	public static void select(List<?> arrayList) {

		if (!PlatformUI.isWorkbenchRunning()) return;

		CorporaView corporaView = openView();
		if (corporaView != null) {
			StructuredSelection selection = new StructuredSelection(arrayList);
			corporaView.getTreeViewer().setSelection(selection, true);
			corporaView.getTreeViewer().reveal(arrayList); // to be sure
		}
	}

	/**
	 * Selects some items.
	 * 
	 * @param arrayList
	 */
	public static void select(Object o) {

		select(Arrays.asList(o));
	}

	/**
	 * Opens the Corpora view if not already opened.
	 * 
	 * @return the corpora view if opened
	 */
	public static CorporaView openView() {
		// open Unit View if not opened
		try {
			if (!PlatformUI.isWorkbenchRunning()) return null;

			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			CorporaView view = (CorporaView) page.findView(CorporaView.ID);
			if (view == null) {
				view = (CorporaView) page.showView(CorporaView.ID);
			}
			if (view == null) {
				System.out.println("Error Unite view not found: " + CorporaView.ID); //$NON-NLS-1$
			}
			else {
				view.getSite().getPage().activate(view);
			}
			return view;
		}
		catch (PartInitException e) {
			e.printStackTrace();
		}
		return null;
	}


	/**
	 * Refreshes the tree view.
	 */
	public static void refresh() {
		CorporaView corporaView = getInstance();
		if (corporaView != null) {
			corporaView.treeViewer.refresh();
			corporaView.treeViewer.getTree().redraw();
			// corporaView.treeViewer.expandAll(); // FIXME
			Log.finest("CorporaView.refresh(): corpora view refreshed."); //$NON-NLS-1$
		}
	}

	//FIXME: SJ, 2025-01-27: seems useless.
//	/**
//	 * Refreshes the view and the tree.
//	 *
//	 * @param corporaView the corpora view
//	 */
//	public static void refresh(CorporaView corporaView) {
//		corporaView.treeViewer.refresh();
//	}

	/**
	 * Refreshes the specified result node in the tree view.
	 * 
	 * @param result
	 */
	public static void refreshObject(final TXMResult result) {

		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {

				CorporaView corporaView = getInstance();
				corporaView.treeViewer.refresh(result, true);
				
				try {
					CorporaView.expand(result.getParent());
				}
				catch (NullPointerException e) {
				}
			}
		});
	}

	/**
	 * Refreshes the specified result node in the tree view, also synchronizes the TXMResult and the TXMEditor (as the editor part name from the result name, for example).
	 * 
	 * @param editor
	 */
	public static void refreshObject(TXMEditor editor) {

		refreshObject(editor.getResult());
	}

	/**
	 * Expands the specified tree node.
	 * 
	 * @param obj
	 */
	public static void expand(Object obj) {

		final CorporaView corporaView = getInstance();
		if (obj != null) {
			corporaView.treeViewer.expandToLevel(obj, 1);
		}
	}

	/**
	 * 
	 * @param obj
	 */
	public static void reveal(TXMResult obj) {

		final CorporaView corporaView = getInstance();
		if (obj != null && corporaView != null) {
			corporaView.treeViewer.reveal(obj);
		}
	}

	/**
	 * Checks if the Corpora view is active.
	 * 
	 * @return
	 */
	public static boolean isActive() {

		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window == null) {
			return false;
		}
		IWorkbenchPage page = window.getActivePage();
		if (page == null) {
			return false;
		}
		IWorkbenchPart part = page.getActivePart();
		return part.getSite().getId().equals(CorporaView.ID);
	}


}
