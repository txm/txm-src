package org.txm.rcp.editors.listeners;

import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.preferences.RCPPreferences;

/**
 * Generic listener that calls TXMEditor.compute() on handleEvent().
 * 
 * @author sjacquot
 *
 */
// FIXME: SJ: became useless?
// actually this could be useful to pass it all events
@Deprecated
public class ComputeListener extends BaseAbstractComputeListener implements Listener {


	/**
	 * 
	 * @param editor
	 */
	public ComputeListener(TXMEditor editor) {
		super(editor);
	}

	@Override
	public void handleEvent(Event event) {
		if (RCPPreferences.getInstance().getBoolean(RCPPreferences.AUTO_UPDATE_EDITOR)) {
			this.editor.compute(true);
		}
	}

}
