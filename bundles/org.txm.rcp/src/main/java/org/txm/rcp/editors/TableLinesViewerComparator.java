/**
 * 
 */
package org.txm.rcp.editors;

import java.text.Collator;
import java.util.Date;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.txm.objects.Edition;
import org.txm.utils.NaturalOrderComparator;

/**
 * Generic base viewer comparator for table lines sorting.
 * This comparator tries to determine the primitive type of the objects and compare according to it.
 * The method compare() is dedicated to be override by subclasses if more granularity is needed.
 * Subclasses can for example sort using the column index that is stored each time a sort is done.
 * 
 * TODO use column.getData("initial_sort_direction") , column.getData("is_initial_sort_columns") to drive the sort selection
 * 
 * @author sjacquot
 *
 */
// FIXME: SJ: this class should manage a result of type TableResult extends TXMResult
// This class then should be responsible of the sort of the UI but also of the call of a TableResult.sort() method
// The class TableResult or MatrixResult should define a method sort(int columnIndex), abstract or not
public class TableLinesViewerComparator extends ViewerComparator {


	public static final int DESCENDING = SWT.DOWN;
	public static final int ASCENDING = SWT.UP;

	/**
	 * Last column index.
	 */
	protected int lastColumnIndex = 0;

	/**
	 * Current direction.
	 */
	protected int direction = DESCENDING;

	/**
	 * Collator to use for Strings comparison.
	 */
	protected Collator collator;

	/**
	 * If true all the viewer columns will be packed after a sort.
	 */
	protected boolean autoPackColumns;

	/**
	 * 
	 * @param collator
	 */
	public TableLinesViewerComparator(Collator collator) {
		this(collator, true);
	}

	/**
	 * 
	 * @param collator
	 * @param autoPackColumns
	 */
	public TableLinesViewerComparator(Collator collator, boolean autoPackColumns) {
		this.collator = collator;
		this.autoPackColumns = autoPackColumns;
		this.lastColumnIndex = 0;
		this.direction = ASCENDING;
	}

	public static NaturalOrderComparator naturalOrderComparator = new NaturalOrderComparator();
	
	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {

		int result = 0;

		Object cell1 = e1;
		Object cell2 = e2;

		if (e1 instanceof List<?>) {
			cell1 = ((List<?>) e1).get(this.lastColumnIndex);
			cell2 = ((List<?>) e2).get(this.lastColumnIndex);
		}

		if (e1 instanceof Object[]) {
			cell1 = ((Object[]) e1)[this.lastColumnIndex];
			cell2 = ((Object[]) e2)[this.lastColumnIndex];
		}

		if (Integer.class.isInstance(cell1)) {
			result = ((Integer) cell1).compareTo(((Integer) cell2));
		}
		else if (Double.class.isInstance(cell1)) {
			result = ((Double) cell1).compareTo(((Double) cell2));
		}
		else if (Float.class.isInstance(cell1)) {
			result = ((Float) cell1).compareTo(((Float) cell2));
		}
		else if (Long.class.isInstance(cell1)) {
			result = ((Long) cell1).compareTo(((Long) cell2));
		}
		else if (String.class.isInstance(cell1)) {
			try {
				result = naturalOrderComparator.compare(cell1, cell2);
				//result = Edition.isFirstGTthanSecond((String) cell1, (String) cell2);
			} catch (Exception e) {
				result = this.collator.compare((String) cell1, (String) cell2);
			}
		}
		else if (Boolean.class.isInstance(cell1)) {
			result = ((Boolean) cell1).compareTo(((Boolean) cell2));
		}
		else if (Date.class.isInstance(cell1)) {
			result = ((Date) cell1).compareTo(((Date) cell2));
		}

		// reverse if needed
		if (this.direction == DESCENDING) {
			result = -result;
		}
		return result;
	}

	public int customCompare(Object e1, Object e2) {
		return 0;
	}

	/**
	 * 
	 * @return the SWT direction code
	 */
	public int getDirection() {

		return this.direction;// == 1 ? SWT.DOWN : SWT.UP;
	}

	/**
	 * 
	 * @param column
	 */
	public void setColumn(int column) {
		
		if (column == this.lastColumnIndex) {
			// same column as last, toggle sorting direction
			if (this.direction == DESCENDING) this.direction = ASCENDING;
			else this.direction = DESCENDING;
		}
		else {
			// new column, set default sort direction
			this.lastColumnIndex = column;
			this.direction = getNewSelectedColumnInitialDirection();
		}
	}

	/**
	 * 
	 * @return the default sort direction of a column when newly clicked
	 */
	public int getNewSelectedColumnInitialDirection() {

		return ASCENDING; // UP  BY DEFAULT
	}

	/**
	 * 
	 * @return the number of the initial sorted column
	 */
	public int getInitialSortedColumn() {

		return 0; // The first
	}

	/**
	 * Adds a selection adapter to the specified column to manage the lines sorting.
	 * 
	 * @param viewer
	 * @param column
	 * @param index
	 */
	public void addSelectionAdapter(final TableViewer viewer, final TableColumn column, final int index) {

		column.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				if ((e.stateMask & SWT.SHIFT) != 0) {

				}
				else {
					setColumn(index);

					IStructuredSelection sel = viewer.getStructuredSelection();

					viewer.getTable().setSortDirection(getDirection()); // update arrow direction
					viewer.getTable().setSortColumn(column); // update arrow position

					if (autoPackColumns) {
						TableUtils.packColumns(viewer); //  will do the sort before setting column widths
					}
					else {
						viewer.refresh(); //  will do the sort
					}

					viewer.setSelection(new StructuredSelection(sel.toList()));
				}
			}
		});
	}

	/**
	 * Adds some selection adapters to all columns between the specified indexes range to manage the lines sorting.
	 * 
	 * @param viewer
	 * @param startingColIndex
	 * @param endingColIndex
	 */
	public void addSelectionAdapters(final TableViewer viewer, int startingColIndex, int endingColIndex, int initialSortedColumn, int initialDirection) {

		Table table = viewer.getTable();

		for (int i = startingColIndex; i < endingColIndex; i++) {
			this.addSelectionAdapter(viewer, table.getColumn(i), i);
		}

		if (initialSortedColumn >=0 && initialSortedColumn < table.getColumnCount()) {
			table.setSortDirection(initialDirection); // update arrow direction
			table.setSortColumn(table.getColumn(initialSortedColumn)); // update arrow position
		}
	}

	/**
	 * Adds some selection adapters to all columns that have a non empty header label.
	 * 
	 * @param viewer
	 */
	public void addSelectionAdapters(final TableViewer viewer) {
		//		Table table = viewer.getTable();
		//		for (int i = 0; i < table.getColumnCount(); i++) {
		//			TableColumn column = table.getColumn(i);
		//			if (!column.getText().isEmpty()) {
		//				this.addSelectionAdapter(viewer, column, i);
		//			}
		//		}

		if (viewer.getTable().getColumnCount() == 0) return; // nothing to do
		addSelectionAdapters(viewer, 0, viewer.getTable().getColumnCount(), getInitialSortedColumn(), getNewSelectedColumnInitialDirection());
	}

	/**
	 * @param direction the direction to set
	 * use DESCENDING or ASCENDING
	 */
	public void setDirection(int direction) {
		this.direction = direction;
	}


}
