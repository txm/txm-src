package org.txm.rcp.swt.widget.parameters;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.kohsuke.args4j.NamedOptionDef;

/**
 * Check button + Label
 * 
 * @author mdecorde
 *
 */
public class BooleanField extends LabelField {

	Button dt;

	public BooleanField(Composite parent, int style, NamedOptionDef parameter) {
		super(parent, style, parameter);

		dt = new Button(this, SWT.CHECK);
		dt.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));
		//dt.setText(getWidgetLabel().replace(":", ""));
		dt.setToolTipText(getWidgetUsage());
		dt.setSelection("true".equals(getWidgetDefault())); //$NON-NLS-1$
	}

	public Boolean getWidgetValue() {
		return dt.getSelection();
	}

	public void setDefault(Object text) {
		dt.setSelection("true".equals(text)); //$NON-NLS-1$
	}

	@Override
	public void resetToDefault() {
		dt.setSelection("true".equals(getWidgetDefault())); //$NON-NLS-1$
	}
}
