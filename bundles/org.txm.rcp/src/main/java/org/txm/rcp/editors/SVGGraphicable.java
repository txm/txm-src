// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.editors;

import org.eclipse.core.runtime.IPath;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.ui.IEditorInput;

/**
 * An {@link IEditorInput} implementing this interface may be displayed as a
 * table by the {@link TableEditor}.
 * 
 * @author sloiseau
 */
//FIXME: SJ: useless, should use an aabstract FileEditorInput
@Deprecated
public interface SVGGraphicable extends IEditorInput {

	/**
	 * Gets the i path.
	 *
	 * @return the i path
	 */
	public IPath getIPath();

}
