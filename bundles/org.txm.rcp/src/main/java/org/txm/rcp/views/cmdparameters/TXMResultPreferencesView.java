// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views.cmdparameters;

import java.lang.reflect.Field;
import java.util.HashSet;

import org.eclipse.core.internal.resources.ProjectPreferences;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IPartService;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.osgi.service.prefs.BackingStoreException;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.utils.logger.Log;

/**
 * Provide an access to the TXMResult RCP preferences values
 * 
 * @author mdecorde.
 */
public class TXMResultPreferencesView extends ViewPart {

	/** The ID. */
	public static String ID = TXMResultPreferencesView.class.getName();

	private Text refs;

	private TXMResultParametersComposite parametersComposite;

	static HashSet<String> txmResultEntries = new HashSet<>();
	static {
		txmResultEntries.add("class");
		txmResultEntries.add("bundle_id");
		for (Field f : TXMResult.class.getDeclaredFields()) {

			Parameter parameter = f.getAnnotation(Parameter.class);
			if (parameter != null) {
				txmResultEntries.add(parameter.key());
			}
		}
	}

	/**
	 * Instantiates a new queries view.
	 */
	public TXMResultPreferencesView() {
		IPartService service = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getPartService();
	}

	@Override
	public void dispose() {
		IPartService service = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getPartService();
	}

	/**
	 * Refresh.
	 */
	public static void refresh() {

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {

		parent.setLayout(new GridLayout(10, false));

		Button refresh = new Button(parent, SWT.PUSH);
		refresh.setText("Refresh");
		refresh.setToolTipText("Refresh displayed result");
		refresh.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				Object obj = CorporaView.getFirstSelectedObject();
				if (obj instanceof TXMResult) {
					parametersComposite.updateEditorParameters(obj);
				}
				else {
					System.out.println("Select an object in the corpora view (current=" + obj + ")");
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		refresh.setLayoutData(GridDataFactory.fillDefaults().align(GridData.BEGINNING, GridData.BEGINNING).grab(false, false).create());

		Button upButton = new Button(parent, SWT.PUSH);
		upButton.setText("⇧");
		upButton.setToolTipText("Show parent result");
		upButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				Object obj = parametersComposite.getInput();
				if (obj instanceof TXMResult) {
					TXMResult p = ((TXMResult) obj).getParent();
					if (p != null) {
						parametersComposite.updateEditorParameters(p);
					}
				}
				else {
					System.out.println("Select an object in the corpora view (current=" + obj + ")");
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		upButton.setLayoutData(GridDataFactory.fillDefaults().align(GridData.BEGINNING, GridData.BEGINNING).grab(false, false).create());

		new Label(parent, SWT.NONE).setText("properties");

		refs = new Text(parent, SWT.BORDER);
		refs.setToolTipText("References");
		refs.setText(TXMResultParametersComposite.DEFAULT_REFS);
		refs.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.KEYPAD_CR || e.keyCode == SWT.CR) {
					parametersComposite.setReferences(refs.getText().split(","));
					parametersComposite.refresh();
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		Button addNodeButton = new Button(parent, SWT.PUSH);
		addNodeButton.setToolTipText("Add a preference node");
		addNodeButton.setText("+ node");
		addNodeButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					onAddNodeSelected(e);
					parametersComposite.getTreeViewer().refresh();
				}
				catch (BackingStoreException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		addNodeButton.setLayoutData(GridDataFactory.fillDefaults().align(GridData.BEGINNING, GridData.BEGINNING).grab(false, false).create());

		Button addEntryButton = new Button(parent, SWT.PUSH);
		addNodeButton.setToolTipText("Add a new preference value");
		addEntryButton.setText("+ entry");
		addEntryButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					onAddEntrySelected(e);
					parametersComposite.getTreeViewer().refresh();
				}
				catch (BackingStoreException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		addEntryButton.setLayoutData(GridDataFactory.fillDefaults().align(GridData.BEGINNING, GridData.BEGINNING).grab(false, false).create());

		Button removeEntryButton = new Button(parent, SWT.PUSH);
		addNodeButton.setToolTipText("Remove a preference value");
		removeEntryButton.setText("-");
		removeEntryButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					onRemoveEntrySelected(e);
					parametersComposite.getTreeViewer().refresh();
				}
				catch (BackingStoreException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		removeEntryButton.setLayoutData(GridDataFactory.fillDefaults().align(GridData.BEGINNING, GridData.BEGINNING).grab(false, false).create());

		refresh.setLayoutData(GridDataFactory.fillDefaults().align(GridData.BEGINNING, GridData.BEGINNING).grab(false, false).create());

		parametersComposite = new TXMResultParametersComposite(parent, SWT.NONE);
		parametersComposite.setLayoutData(GridDataFactory.fillDefaults().align(GridData.FILL, GridData.FILL).grab(true, true).span(8, 1).create());
	}

	protected void onAddEntrySelected(SelectionEvent e) throws BackingStoreException {
		ISelection isel = parametersComposite.getTreeViewer().getSelection();
		if (isel.isEmpty()) return;

		if (isel instanceof StructuredSelection) {
			StructuredSelection sel = (StructuredSelection) isel;
			Object obj = sel.getFirstElement();

			ProjectPreferences pref = null;
			if (obj instanceof ProjectPreferences) {
				pref = (ProjectPreferences) obj;
			}
			else if (obj instanceof Object[]) {
				Object[] objs = (Object[]) obj;
				if (!(objs[0] instanceof ProjectPreferences)) {
					return;
				}
				pref = (ProjectPreferences) objs[0];
			}
			else {
				return;
			}

			InputDialog d = new InputDialog(e.display.getActiveShell(), "new entry", "new entry key", "", null);
			if (d.open() == InputDialog.OK) {
				if (java.util.Arrays.asList(pref.keys()).contains(d.getValue())) {
					Log.warning(NLS.bind("Could not create the {0} entry: The {0} entry already exists", d.getValue()));
				}
				else {
					pref.put(d.getValue(), "");
				}
			}
		}
	}

	protected void onRemoveEntrySelected(SelectionEvent e) throws BackingStoreException {
		ISelection isel = parametersComposite.getTreeViewer().getSelection();
		if (isel.isEmpty()) return;

		if (isel instanceof StructuredSelection) {
			StructuredSelection sel = (StructuredSelection) isel;
			Object obj = sel.getFirstElement();

			ProjectPreferences pref = null;
			String entry = null;
			if (obj instanceof ProjectPreferences) {
				pref = (ProjectPreferences) obj;
			}
			else if (obj instanceof Object[]) {
				Object[] objs = (Object[]) obj;
				if (!(objs[0] instanceof ProjectPreferences)) {
					return;
				}
				pref = (ProjectPreferences) objs[0];
				entry = objs[1].toString();
			}
			else {
				return;
			}

			if (entry != null) {
				if (pref.get("class", null) != null && txmResultEntries.contains(entry)) {
					return;
				}
				if (MessageDialog.openQuestion(e.display.getActiveShell(), "Removing entry: " + entry, "Continue ?")) {
					pref.remove(entry);
				}
			}
			else {
				if (pref.get("class", null) != null) {
					return;
				}

				if (MessageDialog.openQuestion(e.display.getActiveShell(), "Removing node: " + pref.name(), "Continue ?")) {
					pref.removeNode();
				}
			}
		}
	}

	protected void onAddNodeSelected(SelectionEvent e) throws BackingStoreException {
		ISelection isel = parametersComposite.getTreeViewer().getSelection();
		if (isel.isEmpty()) return;

		if (isel instanceof StructuredSelection) {
			StructuredSelection sel = (StructuredSelection) isel;
			Object obj = sel.getFirstElement();

			ProjectPreferences pref = null;
			if (obj instanceof ProjectPreferences) {
				pref = (ProjectPreferences) obj;
			}
			else if (obj instanceof Object[]) {
				Object[] objs = (Object[]) obj;
				if (!(objs[0] instanceof ProjectPreferences)) {
					return;
				}
				pref = (ProjectPreferences) objs[0];
			}
			else {
				return;
			}


			InputDialog d = new InputDialog(e.display.getActiveShell(), "new node", "new node name", "", null);
			if (d.open() == InputDialog.OK) {
				if (pref.nodeExists(d.getValue())) {
					Log.warning(NLS.bind("Could not create the {0} entry: The {0} node already exists", d.getValue()));
				}
				else {
					pref.node(d.getValue());
				}
			}
		}
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
	}
}
