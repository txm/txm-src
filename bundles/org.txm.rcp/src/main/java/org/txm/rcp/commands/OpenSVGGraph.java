// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.rcp.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.rcp.editors.SVGGraphicEditor;
import org.txm.rcp.editors.input.SVGGraphicEditorInput;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;

/**
 * open a SVG file in an editor
 * 
 * @author mdecorde.
 */
//FIXME: SJ: this class doesn't work anymore
//FIXME: SJ: this souldn't use TXMEditor system
public class OpenSVGGraph extends AbstractHandler {

	private static final String ID = "org.txm.rcp.commands.OpenSVGGraph"; //$NON-NLS-1$
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		String svgFile = ""; //$NON-NLS-1$
		File file = null;

		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event)
				.getShell();

		if (file == null || file.isDirectory()) {
			FileDialog dialog = new FileDialog(shell, SWT.OPEN);
			if (LastOpened.getFile(ID) != null) {
				dialog.setFilterPath(LastOpened.getFolder(ID));
				dialog.setFileName(LastOpened.getFile(ID));
			}
			if (dialog.open() == null) return null;
			svgFile = dialog.getFilterPath() + "/" + dialog.getFileName(); //$NON-NLS-1$
			LastOpened.set(ID, dialog.getFilterPath(), dialog.getFileName());
			OpenSVGFile(svgFile);
		}

		return null;
	}

	/**
	 * Open svg file.
	 *
	 * @param filepath the filepath
	 * @return the sVG graphic editor
	 */
	public static SVGGraphicEditor OpenSVGFile(String filepath) {
		return OpenSVGFile(filepath, TXMUIMessages.sVGGraphic);
	}

	/**
	 * Open svg file.
	 *
	 * @param filepath the filepath
	 * @param name the name
	 * @return the sVG graphic editor
	 */
	public static SVGGraphicEditor OpenSVGFile(String filepath, String name) {
		//System.out.println("SVGFile path : "+filepath); //$NON-NLS-1$
		SVGGraphicEditorInput editorInput = new SVGGraphicEditorInput(filepath, name);

		// System.out.println("open svg editor" );
		//		TXMEditor editor = TXMEditor.openEditor(editorInput, SVGGraphicEditor.ID);
		//		if (editor !=null) {
		//			((SVGGraphicEditor)editor).resetView();
		//			return (SVGGraphicEditor)editor;
		//		} else {
		//			return null;
		//		}

		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		IWorkbenchPage page = window.getActivePage();
		try {
			IEditorPart e = page.openEditor(editorInput, "org.txm.rcp.editors.SVGGraphicEditor"); //$NON-NLS-1$
			if (e != null && e instanceof SVGGraphicEditor) {
				SVGGraphicEditor editor = (SVGGraphicEditor) e;
				return editor;
			}
			else {
				return null;
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}
}
