package org.txm.rcp.swt.listeners;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class DropdownSelectionListener extends SelectionAdapter {

	private Menu menu;

	private SelectionListener defaultListener;

	public DropdownSelectionListener(ToolBar parent, ToolItem dropdown) {
		menu = new Menu(parent.getShell(), SWT.POP_UP);
	}

	public void setDefaultSelectionListener(SelectionListener defaultListener) {
		this.defaultListener = defaultListener;
	}

	public Menu getMenu() {
		return menu;
	}

	@Override
	public void widgetSelected(SelectionEvent event) {
		if (event.detail == SWT.ARROW) {
			ToolItem item = (ToolItem) event.widget;
			Rectangle rect = item.getBounds();
			Point pt = item.getParent().toDisplay(new Point(rect.x, rect.y));
			menu.setLocation(pt.x, pt.y + rect.height);
			menu.setVisible(true);
			event.doit = false;
		}
		else {
			if (defaultListener != null) {
				defaultListener.widgetSelected(event);
			}
		}
	}
}
