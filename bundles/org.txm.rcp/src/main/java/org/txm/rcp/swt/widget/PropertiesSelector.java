// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.rcp.swt.widget;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.txm.objects.CorpusBuild;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.MultipleObjectSelectionDialog;
import org.txm.rcp.swt.dialog.MultiplePropertySelection2ListsDialog;
import org.txm.rcp.swt.provider.SimpleLabelProvider;
import org.txm.searchengine.core.Property;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.VirtualProperty;
import org.txm.utils.logger.Log;

/**
 * Allows to choose a structural unit property.
 *
 * @author mdecorde
 */
public class PropertiesSelector<P extends Property> extends Composite {

	protected static final int MAX_LABEL_LENGHT = 25;

	/** The corpus. */
	CQPCorpus corpus;

	/** The properties. */
	final List<P> selectedProperties = new ArrayList<>();

	/** The availableProperties: the list is final because we don't want to alter the source list */
	final List<P> availableProperties = new ArrayList<>();

	/** The maxprops: the maximum number of selected properties */
	int maxprops = -1;

	/** The properties label. */
	Label propertiesLabel;

	/** The openEditDialog opens the MultiPropertySelectionDialog or SinglePropertySelectionDialog. */
	Button openEditDialogButton;

	/** The prop label. */
	Label propLabel;

	/** The prop separator. */
	String propSeparator = ", "; //$NON-NLS-1$

	ArrayList<Listener> listeners = new ArrayList<>();

	private boolean showSimpleNames = false;


	/**
	 * Instantiates a new properties selector.
	 *
	 * @param parent the parent
	 * @param style the style
	 */
	public PropertiesSelector(Composite parent, int style) {
		this(parent, style, null);
	}

	/**
	 * Instantiates a new properties selector.
	 *
	 * @param parent the parent
	 * @param style the style
	 * @param title widget title to display
	 */
	public PropertiesSelector(Composite parent, int style, String title) {
		super(parent, style);

		this.setLayout(new GridLayout(3, false));

		// "properties"
		propLabel = new Label(this, SWT.NONE);
		if (title != null) {
			propLabel.setText(title);
		}
		else {
			propLabel.setText(TXMUIMessages.propertiesColon);
		}
		propLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true));

		// selected properties
		propertiesLabel = new Label(this, SWT.NONE);
		propertiesLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, true));
		propertiesLabel.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {

				showEditorDialog();
				refresh();
			}
		});

		// "edit"
		openEditDialogButton = new Button(this, SWT.PUSH);
		openEditDialogButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, true));
		openEditDialogButton.setImage(IImageKeys.getImage("platform:/plugin/org.eclipse.jface/org/eclipse/jface/dialogs/images/popup_menu.png"));
		openEditDialogButton.setToolTipText("Change selection");
		openEditDialogButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				showEditorDialog();
				refresh();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}

	/**
	 * Instantiates a new properties selector.
	 *
	 * @param parent
	 */
	public PropertiesSelector(Composite parent) {
		this(parent, SWT.NONE, null);
	}

	/**
	 * Instantiates a new properties selector.
	 *
	 * @param parent
	 */
	public PropertiesSelector(Composite parent, String title) {
		this(parent, SWT.NONE, title);
	}

	public void setButtonText(String text) {
		if (openEditDialogButton != null && !openEditDialogButton.isDisposed()) {
			openEditDialogButton.setText(text);
		}
	}

	public void addSelectionListener(SelectionListener listener) {
		openEditDialogButton.addSelectionListener(listener);
	}

	/**
	 * Sets the max property number.
	 *
	 * @param max the new max property number
	 */
	public void setMaxPropertyNumber(int max) {
		this.maxprops = max;
	}

	/**
	 * Refresh.
	 */
	public void refresh() {
		String previous = propertiesLabel.getText();
		StringBuffer buffer = new StringBuffer();
		boolean first = true;
		for (Property p : selectedProperties) {
			if (first) {
				first = false;
			}
			else {
				buffer.append(propSeparator);
			}

			if (p instanceof StructuralUnitProperty) {
				if (showSimpleNames) {
					buffer.append(((StructuralUnitProperty) p).getName());
				}
				else {
					buffer.append(((StructuralUnitProperty) p).getFullName());
				}
			}
			else if (p instanceof VirtualProperty) {
				buffer.append(((VirtualProperty) p).getFullName());
			}
			else if (p instanceof Property) {
				buffer.append(p.getName());
			}
			else {
				buffer.append(p.getName() + "(" + p.getSearchEngine().getName() + ")"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		if (buffer.length() > 0) {
			int l = buffer.length();
			String s = buffer.substring(0, buffer.length());
			if (l > MAX_LABEL_LENGHT) {
				s = s.substring(0, MAX_LABEL_LENGHT) + "..."; //$NON-NLS-1$
			}
			propertiesLabel.setText(s);
			propertiesLabel.getParent().layout();
			propertiesLabel.getParent().getParent().layout();
			propertiesLabel.getParent().getParent().getParent().layout();
			// propertiesLabel.update(); //this line cause the SWT env to go
			// crazy with Ubuntu 14.04 !!!
		}

		if (!previous.equals(propertiesLabel.getText())) { // value changed !
			for (Listener l : listeners) {
				l.handleEvent(null);
			}
		}
	}

	/**
	 * Show editor dialog.
	 */
	public void showEditorDialog() {
		if (availableProperties == null) {
			return;
		}
		//		if (maxprops == 1) {
		//			SinglePropertySelectionDialog<?> dialog = new SinglePropertySelectionDialog(this.getDisplay().getActiveShell(), availableProperties, selectedProperties, maxprops, propLabel);
		//			dialog.open();
		//		}
		//		else {
		//			MultipleObjectSelectionDialog<?> dialog = new MultipleObjectSelectionDialog(this.getDisplay().getActiveShell(), availableProperties, selectedProperties, maxprops, propLabel);
		//			dialog.open();
		//		}
		if (availableProperties.size() <= 10) {
			MultipleObjectSelectionDialog<P> dialog = new MultipleObjectSelectionDialog<P>(this.getDisplay().getActiveShell(), availableProperties, selectedProperties, maxprops, openEditDialogButton, new SimpleLabelProvider() {
				@Override
				public String getText(Object element) {
					if (element == null) {
						return "";
					}
					if (element instanceof StructuralUnitProperty sup) {
						return sup.getFullName();
					}
					return ((P)element).getName();
				}
			});
			if (dialog.open() == MultipleObjectSelectionDialog.OK) {
				selectedProperties.clear();
				selectedProperties.addAll(dialog.getSelection());
			}

		} else {
			MultiplePropertySelection2ListsDialog dialog = new MultiplePropertySelection2ListsDialog(this.getDisplay().getActiveShell(), availableProperties, selectedProperties, maxprops, openEditDialogButton);
			if (dialog.open() == MultipleObjectSelectionDialog.OK) {
				selectedProperties.clear();
				selectedProperties.addAll(dialog.getSelectedProperties());
			}
		}
	}

	/**
	 * Sets the corpus.
	 *
	 * @param corpus the new corpus
	 */
	public void setCorpus(CQPCorpus corpus) {
		this.corpus = corpus;
		if (corpus == null) {
			return;
		}

		availableProperties.clear();
		selectedProperties.clear();
		try {
			availableProperties.addAll((Collection<? extends P>) corpus.getOrderedProperties());
			// availableprops.addAll(corpus.getStructuralUnitProperties());

			for (int i = 0; i < availableProperties.size(); i++) {
				// remove TXM internal properties
				if (availableProperties.get(i).getName().equals(CorpusBuild.ID)) {
					availableProperties.remove(i);
					i--;
				}
			}

			availableProperties.addAll((Collection<? extends P>) corpus.getVirtualProperties());
		}
		catch (CqiClientException e) {
			Log.severe(NLS.bind(TXMUIMessages.cantLoadPropertiesFromCorpusP0ColonP1, e.getMessage()));
		}
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public List<P> getSelectedProperties() {
		return new ArrayList<>(this.selectedProperties);
	}

	/**
	 * Gets the Available properties.
	 *
	 * @return the Available properties
	 */
	public List<P> getAvailableProperties() {
		return new ArrayList<>(this.availableProperties);
	}

	/**
	 * Sets the properties.
	 *
	 * @param available All the available properties
	 * @param selected the selected properties of the available properties
	 */
	public void setProperties(List<P> available, List<P> selected) {
		// System.out.println("initialize prop selector with: "+available+" and "+selected);
		this.availableProperties.clear();
		this.selectedProperties.clear();
		this.availableProperties.addAll(available);
		this.selectedProperties.addAll(selected);
		this.refresh();
	}

	/**
	 * Sets the properties.
	 *
	 * @param available the available
	 * @param selected a selected value
	 */
	public void setProperties(List<P> available, P selected) {
		// System.out.println("initialize prop selector with: "+available+" and "+selected);
		availableProperties.clear();
		selectedProperties.clear();
		if (available != null) availableProperties.addAll(available);
		if (selected != null) this.selectedProperties.add(selected);
		refresh();
	}

	/**
	 * Sets the available properties and reset the selected ones.
	 *
	 * @param available a list of properties set to the this.availableProperties list
	 */
	public final void setProperties(List<P> available) {
		this.setProperties(available, new ArrayList<P>());
	}

	/**
	 * Sets the selected properties.
	 *
	 * @param properties
	 */
	public void setSelectedProperties(List<P> properties) {
		this.selectedProperties.clear();
		this.selectedProperties.addAll(properties);
		this.refresh();
	}

	/**
	 * Sets a unique selected property according to its index.
	 *
	 * @param index
	 */
	public void setSelectedProperty(int index) {
		this.setSelectedProperty(this.availableProperties.get(index));
	}

	/**
	 * Sets a unique selected property.
	 *
	 * @param property
	 */
	public void setSelectedProperty(P property) {
		List<P> properties = new ArrayList<>();
		properties.add(property);
		this.setSelectedProperties(properties);
	}

	/**
	 * Sets the text.
	 *
	 * @param string the new text
	 */
	public void setTitle(String string) {
		propLabel.setText(string);
		propLabel.update();
	}

	/**
	 * reset available and selected properties
	 */
	public void clearSelected() {
		availableProperties.addAll(selectedProperties);
		selectedProperties.clear();
	}

	/**
	 * reset available and selected properties
	 */
	public void clear() {
		setTitle(""); //$NON-NLS-1$
		setProperties(new ArrayList<P>(), new ArrayList<P>());
	}

	public void addValueChangeListener(Listener listener) {
		listeners.add(listener);
	}

	public void setShowSimpleNames() {

		this.showSimpleNames = true;
	}
}
