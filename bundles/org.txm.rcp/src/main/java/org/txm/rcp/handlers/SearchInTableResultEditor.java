package org.txm.rcp.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.rcp.editors.TableResultEditor;


public class SearchInTableResultEditor extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IEditorPart editor = HandlerUtil.getActiveEditor(event);
		if (editor instanceof TableResultEditor tre) {
			tre.getTableKeyListener().openCloseSearch();
		}
		return null;
	}

}
