package org.txm.rcp.handlers.export;

import java.io.File;

import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;

/**
 * Stores the export configuration to be reused later
 * 
 * @author mdecorde
 *
 */
public abstract class ExportConfiguration<T extends TXMResult> extends TXMResult {

	@Parameter(key = "file")
	protected File pFile;

	public ExportConfiguration(String parametersNodePath) {

		this(parametersNodePath, null);
	}

	public ExportConfiguration(T parent) {

		this(null, parent);
	}

	public ExportConfiguration(String parametersNodePath, T parent) {

		super(parametersNodePath, parent);
	}

	@Override
	public boolean saveParameters() throws Exception {

		return true;

	}

	@Override
	public boolean loadParameters() throws Exception {

		return true;
	}

	/**
	 * Gets the parent of this result.
	 * 
	 * @return the parent
	 */
	public T getParent() {
		return (T) parent;
	}

	@Override
	public String getName() {

		return "" + pFile;
	}

	@Override
	public String getSimpleName() {
		if (pFile == null) return "...";
		return "" + pFile.getName();
	}

	@Override
	public String getDetails() {

		return "file: " + pFile;
	}

	@Override
	public void clean() {

	}

	@Override
	public boolean canCompute() throws Exception {

		return true;
	}

	@Override
	public String[] getExportTXTExtensions() {
		return new String[0];
	}

	@Override
	protected boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {

		return false;
	}

	@Override
	public String getResultType() {

		return "Export Configuration";
	}
}
