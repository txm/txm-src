// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.eclipse.core.net.proxy.IProxyService;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.ConfigurationScope;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.util.Util;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.osgi.util.TextProcessor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.ide.ChooseWorkspaceData;
import org.eclipse.ui.internal.ide.IDEWorkbenchPlugin;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.Version;
import org.osgi.service.prefs.Preferences;
import org.txm.Toolbox;
import org.txm.core.engines.Engine;
import org.txm.core.engines.EngineType;
import org.txm.core.engines.EnginesManager;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.TXMParameters;
import org.txm.functions.CommandsAPI;
import org.txm.objects.Project;
import org.txm.objects.Workspace;
import org.txm.rcp.commands.workspace.Load080BinaryCorpus;
import org.txm.rcp.handlers.scripts.ExecuteImportScript;
import org.txm.rcp.handlers.scripts.ExecuteScript;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.RCPPreferences;
import org.txm.rcp.swt.dialog.CGUMessageDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.BundleUtils;
import org.txm.utils.LogMonitor;
import org.txm.utils.logger.Log;


/**
 * This class controls all aspects of the application's execution.
 */
public class Application implements IApplication {

	/** The Constant PLUGIN_ID. */
	public static final String PLUGIN_ID = "org.txm.rcp"; //$NON-NLS-1$

	/** The Constant FIRST_LAUNCH. */
	public static final String FIRST_LAUNCH = "first_launch"; //$NON-NLS-1$

	private static ApplicationWorkbenchAdvisor awa;

	private static boolean headlessMode = false;

	private static boolean restMode = false;

	// public static OpenDocumentEventProcessor openDocProcessor = new OpenDocumentEventProcessor();
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.IPlatformRunnable#run(java.lang.Object)
	 */
	@Override
	public Object start(IApplicationContext context) throws Exception {

		Map<?, ?> args = context.getArguments();
		List<String> argsList = Arrays.asList((String[]) (args.get("application.args"))); //$NON-NLS-1$
		System.out.println(TXMCoreMessages.bind(TXMUIMessages.argsColonP0, argsList));

		headlessMode = argsList.contains("--headless"); //$NON-NLS-1$
		if (headlessMode) {
			return startHeadLess(argsList);
		}

		restMode = argsList.contains("--rest"); //$NON-NLS-1$
		if (restMode) {
			return startRest(argsList);
		}

		return startDesktop(argsList);
	}

	private int startDesktop(List<String> argsList) {
		// if not headless continue as usual for the Desktop version to start
		Display display = PlatformUI.createDisplay();


		// if (Platform.inDevelopmentMode() || Platform.inDebugMode()) {
		// // this can not work in development environment
		// // as it need to have a default workspace set to none
		// // see product configuration :
		// // osgi.instance.area=@noDefault
		// // ignored in development
		// // -------------------------------------
		//
		// } else {
		// in Release mode only : platform workspace selection on startup
		// then restart if needed (on workspace error...)

		System.out.println("instance location=" + Platform.getInstanceLocation().getURL()); //$NON-NLS-1$
		if (Platform.getInstanceLocation().getURL() == null) {
			if (argsList.contains("-test")) {


				try {
					URL url = new URL(System.getProperty("osgi.install.area") + "/workspace");
					System.out.println("RUNNING TXM IN TEST MODE ! TXM HOME DIRECTORY SET TO: " + url);
					Platform.getInstanceLocation().set(url, true);
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return IApplication.EXIT_OK;
				}

			}
			else {
				Integer ret = openCGU();
				if (ret == IApplication.EXIT_RESTART) {
					System.out.println("** Working directory set by user."); //$NON-NLS-1$
					return IApplication.EXIT_RESTART;
				}
				else if (ret == IApplication.EXIT_OK) {
					System.out.println("** Fail to open CGU or set working directory."); //$NON-NLS-1$
					return IApplication.EXIT_OK; // an error occurred or user canceled
				}
				else {
					// OK
				}
			}
		}

		// Die if platform WS not set (user cancel)
		// ---------------------------

		if (Platform.getInstanceLocation().getURL() == null) {
			System.out.println("** Working directory not set."); //$NON-NLS-1$
			return IApplication.EXIT_OK;
		}


		// load JVM fonts in SWT env
		if (System.getProperty("os.name").contains("Linux")) { // $NON-NLS-1$ $NON-NLS-2$ //$NON-NLS-1$ //$NON-NLS-2$
			File javaFontsPath = new File(System.getProperty("java.home") //$NON-NLS-1$
					+ System.getProperty("file.separator") + "lib" //$NON-NLS-1$ //$NON-NLS-2$
					+ System.getProperty("file.separator") + "fonts" //$NON-NLS-1$ //$NON-NLS-2$
					+ System.getProperty("file.separator")); //$NON-NLS-1$

			String[] fontFiles = javaFontsPath.list();
			if (fontFiles != null) {
				for (int i = 0; i < fontFiles.length; i++) {
					if (fontFiles[i].endsWith(".ttf")) { //$NON-NLS-1$
						File fontFile = new File(javaFontsPath, fontFiles[i]);
						Log.finest("Loading Java font to SWT Device from file " + fontFile + "..."); //$NON-NLS-1$ //$NON-NLS-2$
						// Load the font in SWT
						Display.getDefault().loadFont(fontFile.getAbsolutePath());
					}
				}
			}
		}

		if (argsList.contains("-log")) { //$NON-NLS-1$
			System.out.println(TXMUIMessages.logOptionDetected);
			Log.setPrintInConsole(true);
			Log.setLevel(Level.ALL);
			Log.severe(TXMUIMessages.forceLoggin);
			Toolbox.setPreference(TBXPreferences.ADD_TECH_LOGS, true);
			Toolbox.setPreference(TBXPreferences.LOG_LEVEL, "ALL"); //$NON-NLS-1$
			Toolbox.setPreference(TBXPreferences.LOG_STACKTRACE, true);
		}

		if (argsList.contains("-run") //$NON-NLS-1$
				|| System.getProperty("os.name").contains("Mac") //$NON-NLS-1$ //$NON-NLS-2$
				|| Platform.inDevelopmentMode()
				|| argsList.contains("-test")) { // special case when TXM is run with Eclipse
			System.out.println("Running TXM"); //$NON-NLS-1$
			// FIXME: need to check that all has been well moved in Workspace class or build.properties
			// } else if (argsList.contains("-standalone")) { //$NON-NLS-1$
			// File userhomeDirectory = new File("workspace").getAbsoluteFile(); // eclipse default workspace directory
			// File defaultWorkspaceFile = new File("workspace/corpora/default.xml");
			// if (!defaultWorkspaceFile.exists()) {
			// System.out.println("Stand alone launch, creating minimal TXM user home directory in "+userhomeDirectory.getAbsolutePath());
			// System.out.println("Sample corpora, scripts, import scripts and macros files are not yet installed.");
			//
			// TBXPreferences.getInstance().put(TBXPreferences.USER_TXM_HOME, userhomeDirectory.getAbsolutePath());
			// TBXPreferences.getInstance().put(TBXPreferences.INSTALL_DIR, userhomeDirectory.getParentFile().getAbsolutePath());
			//
			// File corpusworkspaceDirectory = new File(userhomeDirectory, "workspaces");
			// corpusworkspaceDirectory.mkdirs();
			//
			// String createfolders[] = {
			// "corpora", "clipboard", //$NON-NLS-1$ //$NON-NLS-2$
			// "workspaces", "css", "scripts", "scripts/lib", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			// "scripts/macro", "scripts/user", "xsl", "samples", "schema", "R"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			//
			// for (String folder : createfolders) {
			// new File(userhomeDirectory, folder).mkdir();
			// }
			//
			// Workspace.createEmptyWorkspaceDefinition(new File(corpusworkspaceDirectory, "default.xml")); //$NON-NLS-1$
			// BundleUtils.copyFiles("org.txm.core", "res", "org/txm", "css", new File(userhomeDirectory, "css"));
			// BundleUtils.copyFiles("org.txm.core", "res", "org/txm/xml", "xsl", new File(userhomeDirectory, "xsl"));
			// BundleUtils.copyFiles("org.txm.core", "res", "org/txm/xml", "schema", new File(userhomeDirectory, "schema"));
			// File scriptsDirectory = new File(userhomeDirectory, "scripts");
			// new File(scriptsDirectory, "user").mkdir();
			// new File(scriptsDirectory, "import").mkdir();
			// new File(scriptsDirectory, "lib").mkdir();
			// new File(scriptsDirectory, "macro").mkdir();
			// new File(scriptsDirectory, "samples").mkdir();
			// }
		}
		else {
			System.err.println("TXM must be launch with the argument '-run' to start."); //$NON-NLS-1$
			System.err.println(TXMUIMessages.thisFileIsNotMeantToRunTXMPleaseConsultTXMUserManual);

			javax.swing.JOptionPane.showMessageDialog(null, TXMUIMessages.thisFileIsNotMeantToRunTXMPleaseConsultTXMUserManual);
			return IApplication.EXIT_OK;
		}
		// test if Java 1.6 is installed
		String version = System.getProperty("java.version"); //$NON-NLS-1$
		Log.warning("java.version : " + version); //$NON-NLS-1$

		boolean exit = false;// parseCommandLine();
		if (exit) {
			return IApplication.EXIT_OK;
		}


		// display.addListener(SWT.OpenDocument, openDocProcessor);

		// //Lock the RCP workspace, thus there is only one TXM instance running
		// Log.warning(TXMUIMessages.lockRCPWorkspace);
		// if (!Platform.getInstanceLocation().lock()){
		// //Shell shell = display.getActiveShell();
		// System.out.println("TXM is already running. Exiting...");
		// return IApplication.EXIT_OK;
		// }

		try {
			Log.warning(TXMUIMessages.activateProxyService);
			activateProxyService();
		}
		catch (Exception e) {
			Log.warning(TXMCoreMessages.bind(TXMUIMessages.couldNotStartProxyConfigurationColonP0, e));
		}

		try {
			// run the application mainloop
			awa = new ApplicationWorkbenchAdvisor();
			System.out.println("Running TXM workbench."); //$NON-NLS-1$

			int returnCode = PlatformUI.createAndRunWorkbench(display, awa);

			if (returnCode == PlatformUI.RETURN_RESTART) {
				System.out.println("Restarting TXM."); //$NON-NLS-1$
				return IApplication.EXIT_RESTART;
			}

			return IApplication.EXIT_OK;
		}
		finally {
			// System.out.println(Messages.Application_2);
			display.dispose();
		}
	}

	private int startRest(List<String> argsList) throws IllegalStateException, IOException {
		System.out.println("NOT IMPLEMENTED"); //$NON-NLS-1$

		return 0;
	}

	private int startHeadLess(List<String> argsList) throws IllegalStateException, IOException {

		if (Toolbox.getTxmHomePath() == null || Toolbox.getTxmHomePath().isEmpty()) {
			File txmhomedir = Platform.getLocation().toFile().getCanonicalFile();
			TBXPreferences.getInstance().put(TBXPreferences.USER_TXM_HOME, txmhomedir.getAbsolutePath());
		}
		int ret = IApplication.EXIT_OK;

		try {
			if (Toolbox.initialize(TBXPreferences.class, new LogMonitor())) {

				if (argsList.contains("--action=list")) { //$NON-NLS-1$
					ret = doList();
				}
				else if (argsList.contains("--action=install")) { //$NON-NLS-1$
					ret = doInstall();

				}
				else if (argsList.contains("--action=new")) { //$NON-NLS-1$
					String path = argsList.get(argsList.indexOf("--action=new") + 1); //$NON-NLS-1$
					File sourcePath = new File(path);

					String name = argsList.get(argsList.indexOf("--action=new") + 2); //$NON-NLS-1$
					ret = doNew(sourcePath, name);

				}
				else if (argsList.contains("--action=import")) { //$NON-NLS-1$
					String path = argsList.get(argsList.size() - 1);
					File sourcePath = new File(path);

					ret = doImport(sourcePath);
				}
				else if (argsList.contains("--action=export")) { //$NON-NLS-1$
					String path = argsList.get(argsList.size() - 1).trim();
					ret = doExport(path);
				}
				else if (argsList.contains("--action=remove")) { //$NON-NLS-1$
					String path = argsList.get(argsList.size() - 1).trim();
					ret = doRemove(path);
				}
				else if (argsList.contains("--action=command")) { //$NON-NLS-1$

					String name = argsList.get(argsList.size() - 2).trim();
					String path = argsList.get(argsList.size() - 1).trim();

					ret = doCommand(name, path);
				}
				else if (argsList.contains("--action=groovy")) { //$NON-NLS-1$

					String path = argsList.get(argsList.indexOf("--action=groovy") + 1).trim(); //$NON-NLS-1$
					ret = doGroovy(path, argsList);
				}
				else if (argsList.contains("--action=R")) { //$NON-NLS-1$

					String path = argsList.get(argsList.size() - 1).trim();
					ret = doR(path, argsList);
				} else {
					doHelp();
				}

				Toolbox.workspace.saveParameters(true);
				Toolbox.shutdown();
			}
			else {
				Log.severe("Toolbox not initialized - good luck"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ret = -1;
		}

		return ret;
	}

	private int doGroovy(String path, List<String> argsList) {

		File script = new File(path);
		if (!script.isFile()) return -1;
		if (!script.exists()) return -1;
		if (!script.getName().endsWith(".groovy")) return -1; //$NON-NLS-1$
		try {
			JobHandler job = ExecuteScript.executeScript(path, null, null, new StructuredSelection(), argsList);
			if (job != null) {
				job.join();
			}
		}
		catch (Throwable e) {
			e.printStackTrace();
			return -1;
		}
		return 0;
	}

	private int doR(String path, List<String> argsList) {

		File script = new File(path);
		if (!script.isFile()) return -1;
		if (!script.exists()) return -1;
		if (!script.getName().endsWith(".R")) return -1; //$NON-NLS-1$

		EnginesManager<? extends Engine> em = Toolbox.getEngineManager(EngineType.STATS);
		if (em == null) return -1;

		Engine e = em.getEngine("R"); //$NON-NLS-1$
		if (e == null || !e.isRunning()) return -1;

		JobHandler job = ExecuteScript.executeScript(path, null, null, new StructuredSelection(), argsList);
		if (job != null) {
			try {
				job.join();
			}
			catch (Throwable e1) {
				e1.printStackTrace();
				return -1;
			}
		}

		return 0;
	}

	private int doList() throws UnexpectedAnswerException, IOException, CqiServerError {

		System.out.println(Arrays.asList(CQPSearchEngine.getCqiClient().listCorpora()));
		return IApplication.EXIT_OK;
	}

	private int doExport(String path) throws IOException {

		int ret = IApplication.EXIT_OK;
		if (path.length() == 0) {
			System.err.println("No corpus name provided."); //$NON-NLS-1$
			ret = -1;
		}
		else {
			Project p = Workspace.getInstance().getProject(path);
			if (p == null) {
				System.err.println("No corpus found with name: " + path); //$NON-NLS-1$
				ret = -1;
			}
			else {
				File f = new File(Toolbox.getTxmHomePath(), path + ".txm"); //$NON-NLS-1$
				p.export(f);
				System.out.println("Exported file: " + f.getAbsolutePath()); //$NON-NLS-1$
			}
		}
		return ret;
	}

	private int doCommand(String name, String path) {

		int ret = IApplication.EXIT_OK;
		if (name.length() == 0) {
			System.err.println("No command name parameter provided. Ex: concordance, index"); //$NON-NLS-1$
			ret = -1;
		}
		else if (path.length() == 0) {
			System.err.println("No target path parameter provided. Ex: corpus path /VOEUX , concordance name /VOEUX/myconc"); //$NON-NLS-1$
			ret = -1;
		}
		else {
			System.out.println("CALL: " + name + " WITH " + path); //$NON-NLS-1$ //$NON-NLS-2$
			CommandsAPI api = new CommandsAPI();
			api.call(name, Workspace.getResult(path), new TXMParameters());

		}
		return ret;
	}

	private int doRemove(String path) {

		int ret = IApplication.EXIT_OK;

		if (path.length() == 0) {
			System.err.println("No corpus name provided."); //$NON-NLS-1$
			ret = -1;
		}
		else {
			Project p = Workspace.getInstance().getProject(path);
			if (p == null) {
				System.err.println("No corpus found with name: " + path); //$NON-NLS-1$
				ret = -1;
			}
			else {
				p.delete();
				System.out.println("Deleted corpus: " + path); //$NON-NLS-1$
			}
		}
		return ret;
	}

	private int doHelp() throws Exception {
		
		System.out.println("./launcher -os <linux|win32|macosx> -ws <gtkwin32|cocoa> -arch x86_64 -run -data <path the 'TXM-X.Y.Z' working directory, contains 'corpora', etc.> -user <path to '.TXM-X.Y.Z' configuration directory > -nosplash --headless --action=XYZ param1 param2 param3");
		System.out.println("Actions are: list,new,import,export,command,groovy,R");
		System.out.println("\tinstall: do the post install step");
		System.out.println("\tlist: list corpora");
		System.out.println("\tnew: create a new corpus from source directory and import name");
		System.out.println("\tremove: delete a corpus from the corpora");
		System.out.println("\timport: add a new corpus from .txm file");
		System.out.println("\texport: create a .txm file from a corpus of the corpora");
		System.out.println("\tcommand: call a command with its id and the corpus name. Export the result in a file");
		System.out.println("\tgroovy: run a Groovy script");
		System.out.println("\tR: run a R script");
		
		return 0;
	}
	
	private int doInstall() throws Exception {
		
		System.out.println("NOT IMPLEMENTED: do txm post-intall steps");
		return -1;
	}
	
	private int doImport(File sourcePath) throws Exception {

		int ret = IApplication.EXIT_OK;
		if (!sourcePath.exists()) {
			System.err.println("No TXM corpus file found: " + sourcePath); //$NON-NLS-1$
			ret = -1;
		}
		else if (!sourcePath.canRead()) {
			System.err.println("Cannot read TXM corpus file: " + sourcePath); //$NON-NLS-1$
			ret = -1;
		}
		else if (!sourcePath.getName().endsWith(".txm") || !sourcePath.isFile()) { //$NON-NLS-1$
			System.err.println("Not a TXM corpus file: " + sourcePath); //$NON-NLS-1$
			ret = -1;
		}
		else { // DO IT
			Project p = Load080BinaryCorpus.loadBinaryCorpusArchive(sourcePath, new LogMonitor(), true);

			if (p == null) {
				ret = -1;
			}

			System.out.println(Arrays.asList(CQPSearchEngine.getCqiClient().listCorpora()));
			ret = 0;
		}
		return ret;
	}

	private int doNew(File sourcePath, String name) throws InterruptedException {

		int ret = IApplication.EXIT_OK;

		if (!sourcePath.exists()) {
			System.err.println("No source directory found: " + sourcePath); //$NON-NLS-1$
			ret = -1;
		}
		else if (!sourcePath.canRead()) {
			System.err.println("Cannot read source directory: " + sourcePath); //$NON-NLS-1$
			ret = -1;
		}
		else if (!sourcePath.isDirectory()) {
			System.err.println("Not a source directory: " + sourcePath); //$NON-NLS-1$
			ret = -1;
		}
		else { // DO IT
			Project p = org.txm.rcp.corpuswizard.ImportWizard.doCreateProject(sourcePath, name);
			p.setDirty();
			JobHandler job = ExecuteImportScript.executeScript(p);
			job.join();

			ret = job.getResult() == Status.OK_STATUS ? IApplication.EXIT_OK : -1;
		}
		return ret;
	}

	private Integer openCGU() {
		try {

			Version v = BundleUtils.getBundleVersion("org.txm.rcp");//$NON-NLS-1$
			if (v == null) {
				System.out.println("Error: no 'org.txm.rcp' version found."); //$NON-NLS-1$
				return IApplication.EXIT_OK;
			}
			String version = v.getMajor() + "." + v.getMinor() + "." + v.getMicro(); //$NON-NLS-1$ //$NON-NLS-2$
			if (org.eclipse.core.runtime.Platform.inDevelopmentMode()) {
				System.out.println("RUNNING TXM FROM ECLIPSE - DEV MODE ACTIVATED"); //$NON-NLS-1$
				version += "-dev"; //$NON-NLS-1$
			}
			System.out.println("Version=" + version); //$NON-NLS-1$

			File txmhomedir = new File(System.getProperty("user.home"), "TXM-" + version); //$NON-NLS-1$ //$NON-NLS-2$



			// System.out.println("CGU="+ConfigurationScope.INSTANCE.getNode(IDEWorkbenchPlugin.IDE_WORKBENCH).getBoolean(RCPPreferences.CGU, false));
			if (!ConfigurationScope.INSTANCE.getNode(IDEWorkbenchPlugin.IDE_WORKBENCH).getBoolean(RCPPreferences.CGU, false)) {
				ChooseWorkspaceData launchData = new ChooseWorkspaceData(txmhomedir.getAbsolutePath());
				System.out.println("Opening CGU dialog..."); //$NON-NLS-1$
				CGUMessageDialog dialog = new CGUMessageDialog(Display.getCurrent().getActiveShell(), launchData) {
				};
				boolean ret = (dialog.open() == MessageDialog.OK) && dialog.hasCheckBeenPressed();
				if (ret) {
					// TBXPreferences.getInstance().put(RCPPreferences.CGU, true);

					// check choosen path
					String path = launchData.getSelection();
					if (path != null) { // reset the letadata file
						File metadataFile = new File(path, ".metadata"); //$NON-NLS-1$
						File metadataFilePrevious = new File(path, ".metadata-previous"); //$NON-NLS-1$
						metadataFilePrevious.delete();
						metadataFile.renameTo(metadataFilePrevious);
					}
					Integer ret2 = setInstanceLocation(launchData);
					if (ret2 != -1) {
						return ret2;
					}

					// validate Current as default Workspace, save configuration & history

					launchData.writePersistedData();
					System.out.println("save workspace location: " + launchData.getSelection()); //$NON-NLS-1$
					ChooseWorkspaceData.setShowDialogValue(false);

					Preferences node = ConfigurationScope.INSTANCE.getNode(IDEWorkbenchPlugin.IDE_WORKBENCH);
					node.putBoolean(RCPPreferences.CGU, true);
					node.flush();

					return -1;

				}
				else {
					return IApplication.EXIT_OK;
				}
			}
			else {
				ChooseWorkspaceData launchData = new ChooseWorkspaceData(txmhomedir.getAbsolutePath());
				String[] recent = launchData.getRecentWorkspaces();

				// If the selection dialog was not used then the workspace to use is either the
				// most recent selection or the initialDefault (if there is no history).
				String workspace = null;
				if (recent != null && recent.length > 0) {
					workspace = recent[0];
				}
				if (workspace == null || workspace.length() == 0) {
					workspace = launchData.getInitialDefault();
				}
				launchData.workspaceSelected(TextProcessor.deprocess(workspace));

				return setInstanceLocation(launchData);
			}
		}
		catch (Exception e) {
			System.out.println("Error: can't open CGU dialog: " + e); //$NON-NLS-1$
			e.printStackTrace();
			return IApplication.EXIT_OK;
		}
	}

	private Integer setInstanceLocation(ChooseWorkspaceData launchData) {
		String selWS = launchData.getSelection();
		if (selWS == null) {
			System.out.println("** No workspace set in configuration scope."); //$NON-NLS-1$
			return IApplication.EXIT_OK;// user cancelled (no restart)
		}

		// Switch workspace :
		URL url = null;
		try {
			url = new URL("file", "", selWS); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (MalformedURLException e1) {
			showWorkspaceError("Workspace path could not be found."); //$NON-NLS-1$
			return IApplication.EXIT_OK;
		}

		try {
			if (org.eclipse.core.runtime.Platform.inDevelopmentMode()) {
				return -1; // can't call Platform.getInstanceLocation().set(...) in dev mode
			}

			if (!Platform.getInstanceLocation().set(url, true)) {
				showWorkspaceError("TXM is already running. Exiting..."); //$NON-NLS-1$
				ChooseWorkspaceData.setShowDialogValue(false);
				return IApplication.EXIT_OK;
			}
			else {
				return -1; // OK !!
			}
		}
		catch (IllegalStateException e) {
			showWorkspaceError("Workspace is in an illegal state: " + e); //$NON-NLS-1$
			ChooseWorkspaceData.setShowDialogValue(true);
			return IApplication.EXIT_OK;
		}
		catch (IOException e) {
			showWorkspaceError("Workspace could not be found on disk: " + e); //$NON-NLS-1$
			ChooseWorkspaceData.setShowDialogValue(true);
			return IApplication.EXIT_OK;
		}


	}

	// /**
	// * Open choose workspace dialog and validate the user's choice. Returns true
	// * if restart needed.
	// */
	// private static Integer chooseWorkspaceUrl(Shell shell) {
	// ChooseWorkspaceData launchData = new ChooseWorkspaceData("");
	//// if (!launchData.getShowDialog()) {
	//// launchData.toggleShowDialog();
	//// }
	// ChooseWorkspaceDialog dialog =
	// new ChooseWorkspaceDialog(shell, launchData, true, true);
	//
	// // don't force prompt : setShowDialogValue is set on error
	// dialog.prompt(false);
	//
	// // check choosen path
	// String selWS = launchData.getSelection();
	// if (selWS == null)
	// return IApplication.EXIT_OK;// user cancelled (no restart)
	//
	// // Switch workspace :
	// URL url = null;
	// try {
	// url = new URL("file", "", selWS);
	// } catch (MalformedURLException e1) {
	// showWorkspaceError("Workspace path could not be found.");
	// return IApplication.EXIT_OK;
	// }
	//
	// try {
	// if (!Platform.getInstanceLocation().set(url, true)) {
	// showWorkspaceError("TXM is already running. Exiting...");
	// ChooseWorkspaceData.setShowDialogValue(false);
	// return IApplication.EXIT_OK;
	// }
	//
	// } catch (IllegalStateException e) {
	// showWorkspaceError("Workspace is in an illegal state: "+e);
	// ChooseWorkspaceData.setShowDialogValue(true);
	// return IApplication.EXIT_OK;
	// } catch (IOException e) {
	// showWorkspaceError("Workspace could not be found on disk: "+e);
	// ChooseWorkspaceData.setShowDialogValue(true);
	// return IApplication.EXIT_OK;
	// }
	//
	// // validate Current as default Workspace, save configuration & history
	//
	// launchData.writePersistedData();
	// ChooseWorkspaceData.setShowDialogValue(false);
	// return -1;// user cancelled (no restart)
	// }

	private static void showWorkspaceError(String message) {
		MessageBox messageBox = new MessageBox(new Shell(), SWT.DIALOG_TRIM | SWT.ERROR);
		messageBox.setText(TXMUIMessages.TXTStartingError);
		messageBox.setMessage(message);
		messageBox.open();
	}

	public ApplicationWorkbenchAdvisor getApplicationWorkbenchAdvisor() {
		return awa;
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}

	/**
	 * Activates the proxy service and sets the network connection active provider as native.
	 */
	private void activateProxyService() {
		String systemProxyHost = System.getProperty("http.proxyHost"); // must be called before IProxyService is used //$NON-NLS-1$

		Bundle bundle = Platform.getBundle("org.eclipse.ui.ide"); //$NON-NLS-1$
		IProxyService proxyService = null;
		if (bundle != null) {
			ServiceReference ref = bundle.getBundleContext().getServiceReference(IProxyService.class.getName());
			if (ref != null)
				proxyService = (IProxyService) bundle.getBundleContext().getService(ref);
		}
		if (proxyService == null) {
			Log.warning("Proxy service could not be found."); //$NON-NLS-1$
		}
		else {
			// System.out.println("FIRST RUN TEST RESULT: "+ApplicationWorkbenchAdvisor.firstLaunchAfterInstallation());
			// force active provider as "Manual" on Mac OS X
			if (Util.isMac() && systemProxyHost != null && ApplicationWorkbenchAdvisor.testTXMHOMEPreferenceAndDirectory()) {
				System.out.println("System proxy detected. Force network provider to manual with proxy_host=" + systemProxyHost); //$NON-NLS-1$
				proxyService.setProxiesEnabled(true);
			}

			// if (Util.isMac()) { // configure only for Mac OS X
			// // String proxy_conf = "";
			// // ProxyConf conf = new ProxyConf(this);
			// // if (conf.mustSetProxyConfiguration()) { // there is a system proxy configured
			// // System.out.println(Messages.bind(Messages.Application_11, proxy_conf));
			// // proxyService.setProxiesEnabled(true);
			// // //proxyService.setSystemProxiesEnabled(false);
			// //
			// // // prepare a new proxy configuration
			// // boolean requiresAuthentication = conf.proxy_user != null && conf.proxy_user.length() > 0;
			// // IProxyData[] proxies = new IProxyData[1];
			// // proxies[0] = new ProxyData(IProxyData.HTTP_PROXY_TYPE, conf.proxy_host, conf.proxy_port, requiresAuthentication, "");
			// // proxies[0].setPassword(conf.proxy_password);
			// // proxies[0].setUserid(conf.proxy_user);
			// //
			// // try {
			// // proxyService.setProxyData(proxies); // apply the new proxy configuration
			// // System.out.println("TXM proxy configuration set: "+Arrays.toString(proxies));
			// // } catch (Exception e) {
			// // System.out.println("Failed to set TXM proxy configuration: "+Arrays.toString(proxies));
			// // Log.printStackTrace(e);
			// // }
			// // }
			// }
		}
	}

	public static boolean isHeadLess() {
		return headlessMode || restMode;
	}

	// /**
	// * Parses the command line.
	// *
	// * @return true, if successful
	// * @deprecated, parse the command line the list optionsForPreferences must
	// * be edited too add options
	// */
	// private boolean parseCommandLine() {
	// boolean exit = false;
	// String[] optionsForPreferences = {
	// CQPLibPreferences.CQI_SERVER_PATH_TO_EXECUTABLE,
	// CQPLibPreferences.CQI_SERVER_PATH_TO_INIT_FILE,
	// // TBXPreferences.CQI_SERVER_PATH_TO_REGISTRY,
	// RPreferences.PATH_TO_EXECUTABLE,
	// TBXPreferences.INSTALL_DIR };
	// String setPrefAndExit = "setPrefAndExit"; //$NON-NLS-1$
	//
	// Options options = new Options();
	// options.addOption(OptionBuilder.create("product")); //option passed here when run as an eclipse application. Recognized to prevent a UnrecognizedOptionException //$NON-NLS-1$
	// options.addOption(OptionBuilder.create(setPrefAndExit));
	// for (String preference : optionsForPreferences)
	// options.addOption(OptionBuilder.withValueSeparator().hasArgs()
	// .create(preference));
	//
	// Parser parser = new GnuParser();
	// CommandLine cl;
	// try {
	// cl = parser.parse(options, Platform.getCommandLineArgs());
	// } catch (ParseException e) {
	//
	// System.err.println(NLS.bind(TXMUIMessages.FAILED_TO_PARSE_OPTIONS,
	// StringUtils.join(Platform.getCommandLineArgs(), " "), e)); //$NON-NLS-1$
	// org.txm.utils.logger.Log.printStackTrace(e);
	// exit = true;
	// return exit;
	// }
	//
	// ScopedPreferenceStore preferences = new ScopedPreferenceStore(
	// org.eclipse.core.runtime.preferences.InstanceScope.INSTANCE, Application.PLUGIN_ID);
	// for (String preference : optionsForPreferences)
	// if (cl.hasOption(preference)) {
	// preferences.setValue(preference, cl.getOptionValue(preference));
	// }
	//
	// try {
	// preferences.save();
	// } catch (IOException e) {
	// System.err.println(NLS.bind(TXMUIMessages.failedToSavePreferencesColonP0, e));
	// exit = true;
	// return exit;
	// }
	//
	// if (cl.hasOption(setPrefAndExit))
	// exit = true;
	// return exit;
	// }
}
