package org.txm.rcp.swt.widget.parameters;

import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Spinner;
import org.kohsuke.args4j.NamedOptionDef;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.utils.logger.Log;

public class IntegerField extends LabelField {

	Spinner dt;

	public IntegerField(Composite parent, int style, NamedOptionDef parameter) {
		super(parent, style, parameter);

		dt = new Spinner(this, SWT.BORDER);
		dt.setMinimum(Integer.MIN_VALUE);
		dt.setMaximum(Integer.MAX_VALUE);
		dt.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		resetToDefault();
	}

	public Integer getWidgetValue() {
		return dt.getSelection();
	}

	public void setDefault(Object text) {
		try {
			dt.setSelection(Integer.parseInt("" + text)); //$NON-NLS-1$
		}
		catch (Exception e) {
			//Log.warning(NLS.bind(TXMUIMessages.cannotSetIntegerFieldP0WithDefaultPropertiesValueP1, text, e)); // $NON-NLS-2$
		}
	}

	@Override
	public void resetToDefault() {
		if (getWidgetDefault().length() > 0) {
			try {
				dt.setSelection(Integer.parseInt(getWidgetDefault()));
			}
			catch (Exception e) {
				//Log.warning(NLS.bind(TXMUIMessages.cannotSetIntegerFieldP0WithDefaultPropertiesValueP1, getWidgetDefault(), e)); // $NON-NLS-2$
			}
		}
		else {
			dt.setSelection(0);
		}
	}
}
