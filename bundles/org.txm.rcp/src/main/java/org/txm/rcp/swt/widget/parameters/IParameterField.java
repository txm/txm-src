package org.txm.rcp.swt.widget.parameters;

public interface IParameterField {

	public Object getWidgetValue();
}
