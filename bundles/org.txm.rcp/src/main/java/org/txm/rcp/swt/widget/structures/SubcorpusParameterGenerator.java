package org.txm.rcp.swt.widget.structures;


public interface SubcorpusParameterGenerator {

	public String getQueryString();

	public String getGeneratedName();
}
