// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.txm.Toolbox;
import org.txm.core.results.TXMResult;
import org.txm.objects.Edition;
import org.txm.objects.Page;
import org.txm.objects.Project;
import org.txm.objects.Text;
import org.txm.objects.Workspace;
import org.txm.rcp.Application;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.commands.OpenBrowser;
import org.txm.rcp.handlers.files.EditFile;
import org.txm.rcp.handlers.results.DeleteObject;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;

// TODO: Auto-generated Javadoc
/**
 * display the hierarchy of the workspace Projects, Bases, Texts, Edition and
 * Pages. @author mdecorde
 * 
 */
public class BasesView extends ViewPart {

	/** The tv. */
	TreeViewer tv;

	/** The ID. */
	public static String ID = BasesView.class.getName();

	/**
	 * Instantiates a new bases view.
	 */
	public BasesView() {
	}

	/**
	 * Refresh.
	 */
	public static void refresh() {

		if (!PlatformUI.isWorkbenchRunning()) return;

		BasesView baseView = (BasesView) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findView(
						BasesView.ID);
		if (baseView != null)
			baseView.tv.refresh();
	}

	/**
	 * Reload.
	 */
	public static void reload() {

		if (!PlatformUI.isWorkbenchRunning()) return;

		BasesView baseView = (BasesView) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findView(
						BasesView.ID);
		if (baseView != null)
			baseView._reload();
	}

	/**
	 * _reload.
	 */
	public void _reload() {
		tv.setContentProvider(new TxmObjectTreeProvider());
		tv.setLabelProvider(new TxmObjectLabelProvider());
		tv.setInput(Toolbox.workspace);
		refresh();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		tv = new TreeViewer(parent);
		tv.setContentProvider(new TxmObjectTreeProvider());
		tv.setLabelProvider(new TxmObjectLabelProvider());
		tv.setInput(Toolbox.workspace);

		tv.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				TreeSelection selection = (TreeSelection) tv.getSelection();
				if (selection.getFirstElement() instanceof CQPCorpus) {
					CQPCorpus corpus = (CQPCorpus) selection.getFirstElement();
					File selectedItem = new File(corpus.getProjectDirectory(), "cqp/" + corpus.getID() + ".cqp"); //$NON-NLS-1$ //$NON-NLS-2$
					if (selectedItem.exists())
						EditFile.openfile(selectedItem.getAbsolutePath());
				}
				else if (selection.getFirstElement() instanceof Text) {
					File selectedItem = ((Text) selection.getFirstElement())
							.getSource();
					EditFile.openfile(selectedItem.getAbsolutePath());
				}
				else if (selection.getFirstElement() instanceof Edition) {
					Edition edition = ((Edition) selection.getFirstElement());
					Page firstpage = edition.getFirstPage();
					OpenBrowser.openEdition(firstpage.getFile().getAbsolutePath(), edition.getName());
					// editor.showPage(firstpage);
				}
				else if (selection.getFirstElement() instanceof Page) {
					Page page = ((Page) selection.getFirstElement());
					OpenBrowser.openEdition(page.getFile().getAbsolutePath());
				}
			}
		});

		tv.getTree().addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.DEL) {
					ISelection sel = tv.getSelection();
					if (sel instanceof IStructuredSelection) {
						if (!DeleteObject.askContinueToDelete((IStructuredSelection) sel)) return;
						DeleteObject.delete(sel);
					}
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		MenuManager menuManager = new MenuManager();
		Menu menu = menuManager.createContextMenu(tv.getTree());

		// Set the MenuManager
		tv.getTree().setMenu(menu);
		getSite().registerContextMenu(menuManager, tv);
		// Make the selection available
		getSite().setSelectionProvider(tv);

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

	/**
	 * The Class TxmObjectLabelProvider.
	 */
	public class TxmObjectLabelProvider extends LabelProvider {

		/** The image table. */
		private Map<ImageDescriptor, Image> imageTable = new HashMap<>();

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
		 */
		@Override
		public String getText(Object element) {
			String title = ((TXMResult) element).getName();
			if (title.length() > 41) { // limit text length
				title = title.substring(0, 40) + "..."; //$NON-NLS-1$
			}
			return title;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
		 */
		@Override
		public final Image getImage(Object element) {
			ImageDescriptor imageDescriptor = AbstractUIPlugin
					.imageDescriptorFromPlugin(Application.PLUGIN_ID,
							IImageKeys.FILE);
			if (element instanceof Project)
				imageDescriptor = AbstractUIPlugin.imageDescriptorFromPlugin(
						Application.PLUGIN_ID, IImageKeys.PROJECT);
			else if (element instanceof MainCorpus)
				imageDescriptor = AbstractUIPlugin.imageDescriptorFromPlugin(
						Application.PLUGIN_ID, IImageKeys.CORPUS);
			else if (element instanceof Text)
				imageDescriptor = AbstractUIPlugin.imageDescriptorFromPlugin(
						Application.PLUGIN_ID, IImageKeys.TEXT);
			else if (element instanceof Edition)
				imageDescriptor = AbstractUIPlugin.imageDescriptorFromPlugin(
						Application.PLUGIN_ID, IImageKeys.EDITION);
			else if (element instanceof Page)
				imageDescriptor = AbstractUIPlugin.imageDescriptorFromPlugin(
						Application.PLUGIN_ID, IImageKeys.FILE);

			Image image = imageTable.get(imageDescriptor);
			if (image == null) {
				image = imageDescriptor.createImage();
				imageTable.put(imageDescriptor, image);
			}
			return image;
		}
	}

	/**
	 * The Class TxmObjectTreeProvider.
	 */
	public class TxmObjectTreeProvider implements ITreeContentProvider {

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
		 */
		@Override
		public Object[] getChildren(Object element) {
			if (element instanceof Workspace) {
				return ((Workspace) element).getProjects().toArray();
			}
			else if (element instanceof Project) {
				return ((Project) element).getChildren(MainCorpus.class).toArray();
			}
			else if (element instanceof MainCorpus) {
				return ((CQPCorpus) element).getProject().getTexts().toArray();
			}
			else if (element instanceof Text) {
				return ((Text) element).getEditions().toArray();
			}
			else if (element instanceof Edition) {
				return ((Edition) element).getPageNames().toArray();
			}
			return null;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
		 */
		@Override
		public Object getParent(Object element) {
			if (element instanceof Workspace) {
				return null;
			}
			else if (element instanceof Project) {
				return ((Project) element).getWorkspace();
			}
			else if (element instanceof MainCorpus) {
				return ((MainCorpus) element).getProject();
			}
			else if (element instanceof Text) {
				return ((Text) element).getProject();
			}
			else if (element instanceof Edition) {
				return ((Edition) element).getText();
			}
			else if (element instanceof Page) {
				return ((Page) element).getEdition();
			}
			return null;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
		 */
		@Override
		public boolean hasChildren(Object element) {
			if (element instanceof Workspace) {
				return !((Workspace) element).getProjects().isEmpty();
			}
			else if (element instanceof Project) {
				return !((Project) element).getChildren(MainCorpus.class).isEmpty();
			}
			else if (element instanceof CQPCorpus) {
				return !((CQPCorpus) element).getProject().getTexts().isEmpty();
			}
			else if (element instanceof Text) {
				return !((Text) element).getEditions().isEmpty();
			}
			else if (element instanceof Edition) {
				return !((Edition) element).getPageNames().isEmpty();
			}
			else if (element instanceof Page) {
				return false;
			}
			return false;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
		 */
		@Override
		public Object[] getElements(Object element) {
			return getChildren(element);
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
		 */
		@Override
		public void dispose() {
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
		 */
		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}

	}

}
