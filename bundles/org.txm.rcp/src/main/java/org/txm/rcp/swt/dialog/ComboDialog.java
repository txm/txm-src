// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.dialog;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.txm.rcp.messages.TXMUIMessages;

/**
 * Select a value in a dialog with a Combo widget
 * 
 * @author mdecorde
 */
public class ComboDialog extends Dialog {

	/** The main panel. */
	Composite mainPanel;

	/** The parent shell. */
	Shell parentShell;

	/** The values. */
	List<String> values;

	/** The combo. */
	private Combo combo;

	/** The selected index. */
	private int selectedIndex = -1;

	/** The selected value. */
	private String selectedValue = ""; //$NON-NLS-1$

	/** The default value. */
	private String defaultValue;

	/** The title. */
	private String title;

	private Integer comboStyle;

	/**
	 * Instantiates a new combo dialog.
	 *
	 * @param parentShell the parent shell
	 * @param title the title
	 * @param values the values
	 * @param defaultValue the default value (may be null)
	 */
	public ComboDialog(Shell parentShell, String title, List<String> values, String defaultValue) {

		this(parentShell, title, values, defaultValue, null);
	}

	/**
	 * Instantiates a new combo dialog.
	 *
	 * @param parentShell the parent shell
	 * @param title the title
	 * @param values the values
	 * @param defaultValue the default value (may be null)
	 * @param comboStyle if null combo is SINGLE and READONLY
	 */
	public ComboDialog(Shell parentShell, String title, List<String> values, String defaultValue, Integer comboStyle) {

		super(parentShell);
		this.parentShell = parentShell;
		this.setShellStyle(this.getShellStyle());
		this.values = values;
		this.defaultValue = defaultValue;
		this.title = title;
		this.comboStyle = comboStyle;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {

		super.configureShell(newShell);
		newShell.setText(title);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite p) {
		//System.out.println(p.getLayout());
		GridLayout layout = (GridLayout) p.getLayout();
		layout.verticalSpacing = 5;
		layout.marginBottom = 5;
		layout.marginTop = 5;
		layout.marginLeft = 5;
		layout.marginRight = 5;

		Label l = new Label(p, SWT.NONE);
		l.setText(TXMUIMessages.selectBetweenTheFollowingValues);

		int style = SWT.READ_ONLY | SWT.SINGLE;
		if (comboStyle != null) {
			style = comboStyle;
		}
		combo = new Combo(p, style);
		combo.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 5, 1));
		combo.setItems(values.toArray(new String[] {}));
		// combo.setLayoutData(new GridData(GridData.FILL,GridData.FILL, true,
		// true));
		if (values.size() > 0)
			combo.select(0);
		if (defaultValue != null)
			selectValue(defaultValue);

		return p;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {

		selectedIndex = combo.getSelectionIndex();
		if (selectedIndex > 0) {
			selectedValue = values.get(selectedIndex);
		}
		else {
			selectedValue = combo.getText();
		}
		super.okPressed();
	}

	/**
	 * Gets the values.
	 *
	 * @return the values
	 */
	public List<String> getValues() {

		return values;
	}

	/**
	 * Sets the values.
	 *
	 * @param values the new values
	 */
	public void setValues(List<String> values) {

		this.values = values;
		combo.setItems(values.toArray(new String[] {}));
		if (values.size() > 0) {
			combo.select(0);
		}
	}

	/**
	 * Gets the selected index.
	 *
	 * @return the selected index
	 */
	public int getSelectedIndex() {

		return selectedIndex;
	}

	/**
	 * Gets the selected value.
	 *
	 * @return the selected value
	 */
	public String getSelectedValue() {

		return selectedValue;
	}

	/**
	 * Select value.
	 *
	 * @param string the string
	 */
	public void selectValue(String string) {

		for (int i = 0; i < values.size(); i++)
			if (values.get(i).equals(string)) {
				combo.select(i);
				break;
			}
	}
}
