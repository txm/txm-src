/**
 * 
 */
package org.txm.rcp.swt.widget;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Spinner;
import org.txm.rcp.editors.TXMEditor;

/**
 * Convenience class to add a SWT Spinner to some editor parameters areas.
 * 
 * If the parent is a GLComposite then the number of columns of its GridLayout will be adjusted to receive the control.
 * If the parent is a TXMEditorToolbar, use the SWT.SEPARATOR trick, plus the packing and the right width setting to add non-ToolItem Control to a tool bar.
 * 
 * This class also adds all the needed computing listeners.
 * 
 * @author sjacquot
 *
 */
public class TXMParameterSpinner extends TXMParameterControl {


	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param labelText
	 * @param controlTooltip
	 * @param autoCompute
	 */
	public TXMParameterSpinner(Composite parent, TXMEditor<?> editor, Control control, String labelText, String controlTooltip, boolean autoCompute) {
		super(parent, editor, control, labelText, controlTooltip, autoCompute);

		// default parameters and range
		this.getControl().setMinimum(0);
		this.getControl().setMaximum(Integer.MAX_VALUE);
		this.getControl().setIncrement(1);
		this.getControl().setPageIncrement(100);

		// add computing listener
		this.getControl().addModifyListener(this.computingKeyListener);

	}

	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param labelText
	 * @param controlTooltip
	 * @param autoCompute
	 */
	public TXMParameterSpinner(Composite parent, TXMEditor<?> editor, String labelText, String controlTooltip, boolean autoCompute) {
		this(parent, editor, new Spinner(parent, SWT.BORDER), labelText, controlTooltip, autoCompute);
	}


	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param labelText
	 * @param controlTooltip
	 */
	public TXMParameterSpinner(Composite parent, TXMEditor<?> editor, String labelText, String controlTooltip) {
		this(parent, editor, labelText, controlTooltip, false);
	}

	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param control
	 * @param labelText
	 */
	public TXMParameterSpinner(Composite parent, TXMEditor<?> editor, Control control, String labelText) {
		this(parent, editor, control, labelText, null, false);
	}



	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param labelText
	 */
	public TXMParameterSpinner(Composite parent, TXMEditor<?> editor, String labelText) {
		this(parent, editor, labelText, null, false);
	}

	/***
	 * 
	 * @param parent
	 * @param editor
	 * @param controlTooltip
	 * @param autoCompute
	 */
	public TXMParameterSpinner(Composite parent, TXMEditor<?> editor, String controlTooltip, boolean autoCompute) {
		this(parent, editor, null, controlTooltip, autoCompute);
	}

	/**
	 * 
	 * @param parent
	 * @param editor
	 */
	public TXMParameterSpinner(Composite parent, TXMEditor<?> editor) {
		this(parent, editor, null, false);
	}


	@Override
	public Spinner getControl() {
		return (Spinner) super.getControl();
	}
	
	public void setMinimum(int value) {
		getControl().setMinimum(value);
	}

	public void setMaximum(int value) {
		getControl().setMaximum(value);
	}
	
	public void setSelection(int value) {
		getControl().setSelection(value);
	}
	
	public void setDigits(int value) {
		getControl().setDigits(value);
	}
	
	public void setIncrement(int value) {
		getControl().setIncrement(value);
	}
	
	public void setPageIncrement(int value) {
		getControl().setPageIncrement(value);
	}
}
