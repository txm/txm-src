package org.txm.rcp.swt.widget;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.rcp.swt.GLComposite;

/**
 * Kind of a Check/Toggle button but in a Combo widget
 */
public class BooleanCombo extends GLComposite {
	
	Combo combo;
	
	public BooleanCombo(Composite parent, int style, String trueValue, String falseValue) {
		super(parent, SWT.NONE);
		
		combo = new Combo(this, style);
		combo.setItems(trueValue, falseValue);
		combo.setText(trueValue);
		combo.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
	}
	
	/**
	 * 
	 * @return true if the first item is selected
	 */
	public boolean getSelection() {
		return combo.getSelectionIndex() == 0;
	}
	
	/**
	 * 
	 * @return true if the first item is selected
	 */
	public void setSelection(boolean b) {
		if (b) {
			combo.setText(combo.getItem(0));
		} else {
			combo.setText(combo.getItem(1));
		}
	}

	public void addSelectionListener(ComputeSelectionListener selectionListener) {
		combo.addSelectionListener(selectionListener);
	}

	public String getSelectedItem() {
		
		return combo.getItem(combo.getSelectionIndex());
	}
	
	public void setSelectedItem(String v) {
		
		combo.setText(v);
	}
}
