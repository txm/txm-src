package org.txm.rcp.swt.widget.preferences;

import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.swt.widgets.Composite;


public class BooleanChoiceField extends RadioGroupFieldEditor {
	
	Composite parent;

	public BooleanChoiceField(String preference, String label, String[] booleanChoices, Composite parent) {
		super(preference, label, 4, new String[][] {{booleanChoices[0], "true"}, {booleanChoices[1], "false"}}, parent, false);
		this.parent = parent;
		//getRadioBoxControl(parent).setLayout(new RowLayout());
	}

	public void setToolTipText(String displayAStepPerOccurrenceratherThanADensityCurve) {
		this.getRadioBoxControl(parent);
	}

	public Composite getRadioBoxControl() {
		return getRadioBoxControl(parent);
	}
	
	public void setEnabled(boolean b) {
		
		super.setEnabled(b, parent);
	}
}
