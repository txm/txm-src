// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.editors;

import java.io.File;

import org.eclipse.jface.text.TextViewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.editors.text.IEncodingSupport;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.utils.CharsetDetector;

// TODO: Auto-generated Javadoc
/**
 * The Class TxtEditor.
 *
 * @author mdecorde
 */
public class TxtEditor extends TextEditor {

	/** The Constant ID. */
	public static final String ID = "org.txm.rcp.editors.TxtEditor"; //$NON-NLS-1$

	/** The source. */
	private Object source;

	/**
	 * Instantiates a new txt editor.
	 */
	public TxtEditor() {
		super();
	}

	/**
	 * Inits the.
	 *
	 * @param site the site
	 * @param input the input
	 * @throws PartInitException the part init exception
	 * @see org.eclipse.ui.part.EditorPart#init(org.eclipse.ui.IEditorSite,
	 *      org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		super.init(site, input);

		setInput(input);

		//		IContextService contextService = (IContextService)PlatformUI.getWorkbench()
		//				.getService(IContextService.class);
		//		IContextActivation activation = contextService.activateContext("org.eclipse.ui.textEditorScope");
	}

	@Override
	public void createPartControl(final Composite parent) {
		super.createPartControl(parent);
		//guessEditedFileEncoding();
	}

	/**
	 * Gets the edited file.
	 *
	 * @return the edited file
	 */
	public File getEditedFile() {
		FileStoreEditorInput input = (FileStoreEditorInput) this.getEditorInput();
		return new File(input.getURI().getPath());
	}

	public void guessEditedFileEncoding() {
		File f = getEditedFile();
		if (f != null && f.length() > CharsetDetector.MINIMALSIZE) {
			IEncodingSupport encodingSupport = (IEncodingSupport) this.getAdapter(IEncodingSupport.class);
			String encoding = new CharsetDetector(f).getEncoding();
			if (encoding != null && encodingSupport != null) {
				try {
					encodingSupport.setEncoding(encoding);
					System.out.println(NLS.bind(TXMUIMessages.textEditorColonP0EncodingFoundP1, f, encoding));
				}
				catch (Throwable e) {
				}

			}
		}
	}

	/*
	 * (non-Javadoc)
	 */
	public TextViewer getTextViewer() {
		return ((TextViewer) this.getSourceViewer());
	}
}
