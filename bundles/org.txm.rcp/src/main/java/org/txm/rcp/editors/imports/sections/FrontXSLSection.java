package org.txm.rcp.editors.imports.sections;

import java.io.File;
import java.util.LinkedHashMap;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.txm.Toolbox;
import org.txm.objects.Project;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.txm.rcp.handlers.files.EditFile;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.swt.dialog.MultipleValueDialog;
import org.txm.rcp.swt.provider.HashMapContentProvider;
import org.txm.rcp.swt.provider.HashMapLabelProvider;

public class FrontXSLSection extends ImportEditorSection {

	private static final String ID = "org.txm.rcp.editors.imports.CorpusPage.xslt"; //$NON-NLS-1$

	private static final int SECTION_SIZE = 2;

	File xsltfile;

	Text xsltfileValue;

	String[] xsltparams;// {"val1",1,"val2",2}

	TableViewer xsltParamsViewer;

	public FrontXSLSection(ImportFormEditor editor, FormToolkit toolkit2, ScrolledForm form2, Composite parent, int style) {
		super(editor, toolkit2, form2, parent, style, "xslt"); //$NON-NLS-1$

		this.section.setText(TXMUIMessages.frontXSL);
		TableWrapLayout layout = new TableWrapLayout();
		layout.makeColumnsEqualWidth = true;
		this.section.setLayout(layout);
		this.section.setLayoutData(getSectionGridData(SECTION_SIZE));
		this.section.setEnabled(false);

		this.section.addExpansionListener(new ExpansionAdapter() {

			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.layout(true);
			}
		});

		// filesection.setDescription("Select how to find source files");
		Composite sectionClient = toolkit.createComposite(this.section);
		TableWrapLayout slayout = new TableWrapLayout();
		slayout.makeColumnsEqualWidth = false;
		slayout.numColumns = 4;
		sectionClient.setLayout(slayout);
		this.section.setClient(sectionClient);


		xsltfileValue = toolkit.createText(sectionClient, "", SWT.BORDER); //$NON-NLS-1$
		TableWrapData gdata = getTextGridData();
		xsltfileValue.setLayoutData(gdata);

		Button browseXSLTButton = toolkit.createButton(sectionClient, "...", SWT.PUSH); //$NON-NLS-1$
		browseXSLTButton.setLayoutData(getButtonLayoutData());
		browseXSLTButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(form.getShell(), SWT.OPEN);
				String[] extensions = { "*.xsl" }; //$NON-NLS-1$
				dialog.setFilterExtensions(extensions);
				if (LastOpened.getFile(ID) != null) {
					dialog.setFilterPath(LastOpened.getFolder(ID));
					dialog.setFileName(LastOpened.getFile(ID));
				}
				else {
					dialog.setFilterPath(Toolbox.getTxmHomePath());
				}
				if (dialog.open() != null) {
					xsltfile = new File(dialog.getFilterPath(), dialog.getFileName());
					LastOpened.set(ID, xsltfile.getParent(), xsltfile.getName());
					xsltfileValue.setText(xsltfile.getAbsolutePath());
				}
			}
		});

		Button editXSLTButton = toolkit.createButton(sectionClient, TXMUIMessages.editXSLStylesheet, SWT.PUSH);
		editXSLTButton.setLayoutData(getButtonLayoutData());
		editXSLTButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (xsltfile != null && xsltfile.exists() && xsltfile.isFile()) {
					EditFile.openfile(xsltfile.getAbsolutePath());
				}
			}
		});

		Label l = toolkit.createLabel(sectionClient, ""); // empty cell //$NON-NLS-1$
		l.setLayoutData(new TableWrapData(TableWrapData.FILL_GRAB, TableWrapData.MIDDLE));

		Button addButton = toolkit.createButton(sectionClient, TXMUIMessages.addAXsltParameter, SWT.PUSH);
		addButton.setLayoutData(getButtonLayoutData());
		addButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (project == null) return;

				LinkedHashMap<String, String> values = new LinkedHashMap<>();
				values.put("name", ""); //$NON-NLS-1$ //$NON-NLS-2$
				values.put("value", "");			 //$NON-NLS-1$ //$NON-NLS-2$
				MultipleValueDialog dlg = new MultipleValueDialog(form.getShell(), TXMUIMessages.addAXsltParameter, values);
				if (dlg.open() == Window.OK) {
					project.setXsltParameter(values.get("name"), values.get("value")); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}
		});

		Button removeButton = toolkit.createButton(sectionClient, TXMUIMessages.deleteParameter, SWT.PUSH);
		TableWrapData td = getButtonLayoutData();
		td.colspan = 3;
		removeButton.setLayoutData(td);
		removeButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (project == null) return;

				ISelection sel = xsltParamsViewer.getSelection();
				IStructuredSelection ssel = (IStructuredSelection) sel;
				Object obj = ssel.getFirstElement();
				if (obj instanceof String) {
					project.removeXsltParameter(obj.toString());
				}
			}
		});

		// l = toolkit.createLabel(sectionClient, ""); // empty cell
		// l.setLayoutData(new TableWrapData(TableWrapData.FILL_GRAB, TableWrapData.MIDDLE));
		// l = toolkit.createLabel(sectionClient, ""); // empty cell
		// l.setLayoutData(new TableWrapData(TableWrapData.FILL_GRAB, TableWrapData.MIDDLE));

		Table table = toolkit.createTable(sectionClient, SWT.FULL_SELECTION | SWT.MULTI | SWT.V_SCROLL | SWT.VIRTUAL);
		xsltParamsViewer = new TableViewer(table);
		TableUtils.installCopyAndSearchKeyEvents(xsltParamsViewer);
		TableUtils.installColumnResizeMouseWheelEvents(xsltParamsViewer);
		TableWrapData gd = new TableWrapData(TableWrapData.LEFT, TableWrapData.FILL);
		gd.colspan = 4;
		gd.heightHint = 100;
		xsltParamsViewer.getTable().setLayoutData(gd);
		xsltParamsViewer.setContentProvider(new HashMapContentProvider());
		xsltParamsViewer.getTable().setHeaderVisible(true);
		xsltParamsViewer.getTable().setLinesVisible(true);

		String[] cols = { "name", "value" }; //$NON-NLS-1$ //$NON-NLS-2$
		for (String col : cols) {
			TableViewerColumn columnViewer = new TableViewerColumn(xsltParamsViewer, SWT.NONE);
			TableColumn column = columnViewer.getColumn();
			column.setText(col);
			column.setWidth(100);
			columnViewer.setLabelProvider(new HashMapLabelProvider(col));
		}
	}

	@Override
	public void updateFields(Project project) {
		if (this.section.isDisposed()) return;

		this.project = project;

		String filepath = project.getFrontXSL();
		if (filepath != null) {
			xsltfileValue.setText(filepath);
			xsltfile = new File(filepath);
		}

		xsltParamsViewer.getTable().getParent().layout();
		xsltParamsViewer.setInput(project.getXsltParameters());
	}

	@Override
	public boolean saveFields(Project project) {
		if (xsltfileValue != null && !xsltfileValue.isDisposed())
			project.setXslt(xsltfileValue.getText());

		return true;
	}

	@Override
	public boolean checkFields() {
		return true;
	}

	@Override
	public int getSectionSize() {
		return SECTION_SIZE;
	}
}
