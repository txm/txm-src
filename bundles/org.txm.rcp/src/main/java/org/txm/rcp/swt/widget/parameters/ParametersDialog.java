package org.txm.rcp.swt.widget.parameters;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.NamedOptionDef;
import org.kohsuke.args4j.spi.OptionHandler;
import org.txm.Toolbox;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.Timer;
import org.txm.utils.logger.Log;

@SuppressWarnings("rawtypes")
public class ParametersDialog extends Dialog {

	public int MAX_PARAMETERS_PER_LINE = 15;

	Object bean;

	String script;

	String title;

	List<NamedOptionDef> parameters;

	List<String> fieldNames = new ArrayList<>(); // store all the declared parameters names

	LinkedHashMap<String, ParameterField> widgets = new LinkedHashMap<>();

	protected boolean error = false;

	protected Map args; // store values given by another Macro, if contains all the needed values, no dialog is shown

	protected Map defaultArgs; // store additional default values of parameters (replace default values of script)

	// protected int numberOfParameters; // number of parameters to show in the dialog box (not set by 'args'), if 0 parameters no dialog is shown

	protected ArrayList<String> parametersToSetByTheUser;

	protected boolean mustShowDialog; // show dialog if args.size == 0 OR if there is at least one mandatory field to set

	HashMap<String, Object> values = new HashMap<>(); // store the values of the parameters (may not contains all the values needed

	Properties defaultValues; // default values set by the .properties file

	Properties defaultScriptValues = new Properties(); // default values set by the Groovy script @Field@def annotation attribute

	File propFile;

	protected ArrayList<String> mandatoryParametersToSetByTheUser;
	// private int titleWidth;
	// private Composite parent;

	/**
	 * 
	 * @param parentShell
	 * @param bean
	 * @param allParameters all the scripts annotations
	 * @param args pre-filled fields
	 */
	public ParametersDialog(Shell parentShell, Object bean, List<OptionHandler> allParameters, Map args, Map defaultArgs) {

		super(parentShell);
		if (bean != null) {
			this.script = bean.getClass().getName();
		}
		this.parameters = new ArrayList<>();
		this.bean = bean;
		this.args = args;
		this.defaultArgs = defaultArgs;
		setShellStyle(getShellStyle() | SWT.RESIZE);

		// count the number of fields to show. If "0" then don't show the dialog, see the "open()" function
		// and select only the NamedOptionDef annotations
		// numberOfParameters = 0; // number of parameters not set by args
		parametersToSetByTheUser = new ArrayList<>();
		mandatoryParametersToSetByTheUser = new ArrayList<>();
		for (OptionHandler arg : allParameters) {
			if (arg.option instanceof NamedOptionDef) {
				NamedOptionDef option = (NamedOptionDef) arg.option;
				this.parameters.add(option);
				fieldNames.add(option.name());
				if (args.containsKey(option.name())) {
					continue; // don't show the field
				}

				// count number of mandatory fields set AND add exception for "Separator" widget types
				if (option.required() || "Separator".equals(option.widget())) { //$NON-NLS-1$
					mandatoryParametersToSetByTheUser.add(option.name());
				}
				parametersToSetByTheUser.add(option.name());
			}
		}

		mustShowDialog = mandatoryParametersToSetByTheUser.size() > 0 || (args.size() == 0 && parametersToSetByTheUser.size() > 0);

		// initialize the properties file default values store
		loadDefaultValuesFromPropFile();
	}

	private void loadDefaultValuesFromPropFile() {

		defaultValues = new Properties();
		String scriptRootDir = Toolbox.getTxmHomePath() + "/scripts/groovy/user"; //$NON-NLS-1$

		propFile = new File(scriptRootDir, this.script.replace(".", "/") + ".properties"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		if (propFile.exists()) {
			try {
				defaultValues.load(new FileReader(propFile));
				Log.fine("Retrieving previous macro parameters from " + propFile + " values=" + defaultValues.entrySet() + "..."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			catch (Throwable e) {
				Log.severe(NLS.bind(TXMUIMessages.failedToLoadPreviousValuesFromP0P1, propFile, e));
				Log.printStackTrace(e);
			}
		}

		// initialize the script default values
		for (NamedOptionDef option : this.parameters) {
			String widgetName = option.widget();
			String stringValue = option.def();

			Object value = null;
			try {
				if ("File".equals(widgetName) || "FileOpen".equals(widgetName)) { //$NON-NLS-1$ //$NON-NLS-2$
					value = new File(stringValue);
				}
				else if ("Files".equals(widgetName) || "FilesOpen".equals(widgetName)) { //$NON-NLS-1$ //$NON-NLS-2$
					String[] paths = stringValue.split("" + File.pathSeparatorChar); //$NON-NLS-1$
					File[] files = new File[paths.length];
					for (int i = 0; i < paths.length; i++) {
						files[i] = new File(paths[i]);
					}
					value = files;
				}
				else if ("CreateFile".equals(widgetName) || "FileSave".equals(widgetName)) { //$NON-NLS-1$ //$NON-NLS-2$
					value = new File(stringValue);
				}
				else if ("Folder".equals(widgetName)) { //$NON-NLS-1$
					value = new File(stringValue);
				}
				else if ("Query".equals(widgetName)) { //$NON-NLS-1$
					value = new CQLQuery(stringValue);
				}
				else if ("Date".equals(widgetName)) { //$NON-NLS-1$
					value = DateField.formater.parse(stringValue);
				}
				else if ("Time".equals(widgetName)) { //$NON-NLS-1$
					value = Integer.parseInt(stringValue);
				}
				else if ("String".equals(widgetName)) { //$NON-NLS-1$
					value = stringValue;
				}
				else if ("Separator".equals(widgetName)) { //$NON-NLS-1$
					value = stringValue;
				}
				else if ("Password".equals(widgetName)) { //$NON-NLS-1$
					value = stringValue;
				}
				else if ("StringArray".equals(widgetName)) { //$NON-NLS-1$
					value = stringValue;
				}
				else if ("StringArrayMultiple".equals(widgetName)) { //$NON-NLS-1$
					value = stringValue;
				}
				else if ("StructuralUnits".equals(widgetName)) { //$NON-NLS-1$
					value = stringValue;
				}
				else if ("Text".equals(widgetName)) { //$NON-NLS-1$
					value = stringValue;
				}
				else if ("Integer".equals(widgetName)) { //$NON-NLS-1$
					value = Integer.parseInt(stringValue);
				}
				else if ("Float".equals(widgetName)) { //$NON-NLS-1$
					value = Float.parseFloat(stringValue);
				}
				else if ("Boolean".equals(widgetName)) { //$NON-NLS-1$
					value = Boolean.parseBoolean(stringValue);
				}
				else {
					System.out.println(TXMUIMessages.unknowedWidgetNameColon + widgetName + TXMUIMessages.FileTreeContentProvider_4);
				}
			}
			catch (Exception e) {
				Log.severe("Wrong parameter format: " + stringValue + ". Waiting for: " + widgetName + ". Error = " + e + "."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				value = new Date();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {

		super.configureShell(newShell);
		int idx2 = script.lastIndexOf("Macro"); //$NON-NLS-1$
		if (idx2 < 0) idx2 = script.length();
		title = TXMUIMessages.bind(TXMUIMessages.p0ParametersInput, script.substring(script.lastIndexOf(".") + 1, idx2)); //$NON-NLS-1$
		newShell.setText(title);
		newShell.setMinimumSize(450, 450);

		// newShell.addControlListener(new ControlAdapter() {
		// public void controlResized(ControlEvent e) {
		// Rectangle area = newShell.getClientArea();
		// Point preferredSize = table.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		// int width = area.width - 2*table.getBorderWidth();
		// if (preferredSize.y > area.height + table.getHeaderHeight()) {
		// // Subtract the scrollbar width from the total column width
		// // if a vertical scrollbar will be required
		// Point vBarSize = table.getVerticalBar().getSize();
		// width -= vBarSize.x;
		// }
		// Point oldSize = table.getSize();
		// if (oldSize.x > area.width) {
		// // table is getting smaller so make the columns // smaller first and then resize the table to
		// // match the client area width
		// column1.setWidth(width/3);
		// column2.setWidth(width - column1.getWidth());
		// table.setSize(area.width, area.height);
		// } else {
		// // table is getting bigger so make the table // bigger first and then make the columns wider
		// // to match the client area width
		// table.setSize(area.width, area.height);
		// column1.setWidth(width/3);
		// column2.setWidth(width - column1.getWidth());
		// }
		// }
		// });
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create the reset button (hack of the Back button)
		// Button propButton = createButton(parent, IDialogConstants.BACK_ID,
		// "...", false);
		// propButton.removeListener(SWT.Selection, propButton.getListeners(SWT.Selection)[0]);
		//
		// propButton.addSelectionListener(new SelectionAdapter() {
		// @Override
		// public void widgetSelected(SelectionEvent e) {
		// FileDialog dialog = new FileDialog(e.display.getActiveShell());
		// dialog.setFilterPath(propFile.getParent());
		// dialog.setFilterExtensions(new String[] {"*.properties"});
		// dialog.setFileName(propFile.getName());
		// String path = dialog.open();
		// if (path != null) {
		// propFile = new File(path);
		// loadDefaultValuesFromPropFile();
		// }
		// }
		// });

		Button button = createButton(parent, IDialogConstants.BACK_ID, TXMUIMessages.resetValues, false);
		button.removeListener(SWT.Selection, button.getListeners(SWT.Selection)[0]);

		button.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				// System.out.println("Reseting fields to default values");
				for (String k : widgets.keySet()) {
					widgets.get(k).resetToDefault();
				}
			}
		});
		if (parametersToSetByTheUser.size() == 0) button.setEnabled(false);

		super.createButtonsForButtonBar(parent);
		this.getButton(IDialogConstants.OK_ID).setText(TXMUIMessages.run);
	}

	@Override
	protected Control createButtonBar(Composite parent) {
		Control c = super.createButtonBar(parent);

		// int barWidth = c.getSize().x;
		// if (barWidth > titleWidth) {
		// GridData cdata = (GridData)(this.parent.getLayoutData());
		// cdata.widthHint = barWidth;
		// }

		return c;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite composite) {
		// this.parent = composite;
		super.createDialogArea(composite);

		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout(1, true);
		composite.setLayout(layout);

		// ScrolledComposite scrolledComposite = new ScrolledComposite(composite, SWT.BORDER| SWT.H_SCROLL | SWT.V_SCROLL);
		// scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		// scrolledComposite.setLayout(new GridLayout(1, true));

		GLComposite columns = new GLComposite(composite, SWT.NONE, "columns"); //$NON-NLS-1$
		columns.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		columns.getLayout().numColumns = 1;

		// scrolledComposite.setContent(columns);
		// scrolledComposite.addListener( SWT.Resize, event -> {
		// int width = scrolledComposite.getClientArea().width;
		// scrolledComposite.setMinSize( composite.computeSize( width, SWT.DEFAULT ) );
		// } );


		if (parametersToSetByTheUser.size() > MAX_PARAMETERS_PER_LINE) {
			columns.getLayout().numColumns = 1 + (parametersToSetByTheUser.size() / MAX_PARAMETERS_PER_LINE);
		}

		GLComposite column = new GLComposite(columns, SWT.NONE, "column"); //$NON-NLS-1$
		GridData gdata = new GridData(SWT.FILL, SWT.FILL, true, true);
		gdata.minimumWidth = 200;
		column.setLayoutData(gdata);
		column.getLayout().makeColumnsEqualWidth = false;

		// initialize each widget + default value *of the widget*
		int n = 0;
		for (NamedOptionDef option : parameters) {
			n++;
			if (n % MAX_PARAMETERS_PER_LINE == 0) {
				column = new GLComposite(columns, SWT.NONE, "column"); //$NON-NLS-1$
				gdata = new GridData(SWT.FILL, SWT.FILL, true, true);
				gdata.minimumWidth = 200;
				column.setLayoutData(gdata);
				column.getLayout().makeColumnsEqualWidth = false;
			}

			// each widget is initialized with de default value set in @Field@def attribute
			String widgetName = option.widget();
			ParameterField widget = null;
			if ("File".equals(widgetName) || "FileOpen".equals(widgetName)) { //$NON-NLS-1$ //$NON-NLS-2$
				widget = new FileField(column, SWT.NONE, option);
			}
			else if ("Files".equals(widgetName) || "FilesOpen".equals(widgetName)) { //$NON-NLS-1$ //$NON-NLS-2$
				widget = new FilesField(column, SWT.NONE, option);
			}
			else if ("CreateFile".equals(widgetName) || "FileSave".equals(widgetName)) { //$NON-NLS-1$ //$NON-NLS-2$
				widget = new CreateFileField(column, SWT.NONE, option);
			}
			else if ("Folder".equals(widgetName)) { //$NON-NLS-1$
				widget = new FolderField(column, SWT.NONE, option);
			}
			else if ("Query".equals(widgetName)) { //$NON-NLS-1$
				widget = new QueryField(column, SWT.NONE, option);
			}
			else if ("Date".equals(widgetName)) { //$NON-NLS-1$
				widget = new DateField(column, SWT.NONE, option);
			}
			else if ("Time".equals(widgetName)) { //$NON-NLS-1$
				widget = new TimeField(column, SWT.NONE, option);
			}
			else if ("Separator".equals(widgetName)) { //$NON-NLS-1$
				widget = new SeparatorField(column, SWT.NONE, option);
			}
			else if ("String".equals(widgetName)) { //$NON-NLS-1$
				widget = new StringField(column, SWT.NONE, option);
			}
			else if ("Password".equals(widgetName)) { //$NON-NLS-1$
				widget = new PasswordField(column, SWT.NONE, option);
			}
			else if ("StringArray".equals(widgetName)) { //$NON-NLS-1$
				widget = new StringArrayField(column, SWT.NONE, option);
			}
			else if ("StringArrayMultiple".equals(widgetName)) { //$NON-NLS-1$
				widget = new StringArrayMultipleField(column, SWT.NONE, option);
			}
			else if ("StructuralUnits".equals(widgetName)) { //$NON-NLS-1$
				widget = new CorpusStructuresField(column, SWT.NONE, option);
			}
			else if ("Text".equals(widgetName)) { //$NON-NLS-1$
				widget = new LongStringField(column, SWT.NONE, option);
			}
			else if ("Integer".equals(widgetName)) { //$NON-NLS-1$
				widget = new IntegerField(column, SWT.NONE, option);
			}
			else if ("Float".equals(widgetName)) { //$NON-NLS-1$
				widget = new FloatField(column, SWT.NONE, option);
			}
			else if ("Boolean".equals(widgetName)) { //$NON-NLS-1$
				widget = new BooleanField(column, SWT.NONE, option);
			}
			else {
				System.out.println(TXMUIMessages.unknowedWidgetNameColon + widgetName + TXMUIMessages.FileTreeContentProvider_4);
			}

			if (widget != null) {
				widgets.put(option.name(), widget);

				// set default value in the fields, not enough if args contains the value
				String value = defaultValues.getProperty(option.name());
				String value2 = null;
				if (args.get(option.name()) != null) {
					value2 = "" + args.get(option.name()); //$NON-NLS-1$
				}
				if (value != null) { // set the default value using the properties stored
					widget.setDefault(value);
				}
				else if (value2 != null) { // set the default value using the properties stored
					widget.setDefault(value2);
				}
			}
		}
		new Label(composite, SWT.NONE).setText(TXMUIMessages.mandatoryFields);

		return composite;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		error = false;

		// get widget values
		for (String field : widgets.keySet()) {
			ParameterField w = widgets.get(field);
			Object value = w.getWidgetValue();

			// is widget set if mandatory EXCEPT "Separator" widgets
			if (w.isWidgetMandatory() && value == null && !("Separator".equals(w.getWidgetType()))) { //$NON-NLS-1$
				Log.warning(TXMUIMessages.errorColonMissingValueColon + field);
				error = true;
			}
			else if (value != null) {
				values.put(field, value);
				// System.out.println(""+field+"\t"+values.get(field));
			} // else value is not filled
		}

		// initialize variables
		if (!error) {
			initializeMacroVariables();
			saveParameters();

			if (!error) {
				super.okPressed();
			}
		}
	}

	private void saveParameters() {
		if (!error) {
			try {
				propFile.getParentFile().mkdirs();
				defaultValues.store(new FileWriter(propFile), ""); //$NON-NLS-1$
			}
			catch (IOException e) {
				Log.severe(NLS.bind(TXMUIMessages.warningFailedToStoreDefaultMacroValuesAtP0, propFile) + ": " + e); //$NON-NLS-1$
				Log.printStackTrace(e);
			}
		}
	}


	/**
	 * can use the args map to initialize variable not set with the dialog box
	 */
	private void initializeMacroVariables() {
		// System.out.println("Initialize macro variables of "+fieldNames);

		if (bean == null) return;

		Class c = bean.getClass();
		for (String name : fieldNames) {
			Object value = values.get(name);
			// if (value == null) { // no widget or not set by user
			// value = defaultScriptValues.get(name);
			// }
			// if (value == null) { // no default set by script
			// value = defaultValues.get(name);
			// }
			// System.out.println("test if args contains name="+name+" "+args.containsKey(name));
			if (args.containsKey(name)) { // the 'args' variable may contains values set by another Macro
				value = args.get(name);
			}
			else { // store the default value only if user set it in the dialog box
				if (value != null) {
					//TODO manage the filed type with the associated FieldWidget
					if (value instanceof File[]) {
						defaultValues.setProperty(name, StringUtils.join((File[]) value, File.pathSeparator));
					}
					else if (value instanceof Object[]) {
						defaultValues.setProperty(name, StringUtils.join((File[]) value, ",")); //$NON-NLS-1$
					}
					else {
						defaultValues.setProperty(name, value.toString());
					}
				}
			}

			try {
				// System.out.println("setting "+name+ " with "+values.get(name));
				Field field = c.getDeclaredField(name);
				field.setAccessible(true);
				field.set(bean, value);
			}
			catch (Throwable e) {
				Log.severe(TXMUIMessages.errorInAtOptionNameDeclarationColon + e.getMessage());
				Log.printStackTrace(e);
				error = true;
			}

		}
	}


	protected boolean isError() {
		return error;
	}

	public HashMap<String, Object> getValues() {
		return values;
	}

	static boolean errorOpen, retOpen;

	/**
	 * Conveniance method to initialize the parameters dialog.
	 * Can detect the "args" Groovy binding and use it to set variables
	 * If all variables are set by "args" then don't show the dialog box
	 * 
	 * @param bean
	 * @return true if the user has clicked on the "OK" button
	 */
	public static boolean open(final Object bean) {

		try {

			errorOpen = true;
			retOpen = false;

			// if (!(bean instanceof groovy.lang.Script)) {
			// System.out.println(Messages.ParametersDialog_15);
			// return false;
			// }

			Field[] fields = bean.getClass().getFields();
			Field[] dfields = bean.getClass().getDeclaredFields();
			Object oArgs = null;
			if (bean instanceof groovy.lang.Script) {
				groovy.lang.Script script = (groovy.lang.Script) bean;

				// if the "args" variable exists then use its values. Can be used to set a default value by another Macro
				if (script.getBinding().hasVariable("args")) { //$NON-NLS-1$
					oArgs = script.getBinding().getVariable("args"); //$NON-NLS-1$
				}
			}
			else {
				try {
					if (bean.getClass().getField("args") != null) {
						oArgs = bean.getClass().getField("args").get(bean);
					}
				}
				catch (NoSuchFieldException e) {
				}
			}

			Object oDefaultArgs = null;
			if (bean instanceof groovy.lang.Script) {
				groovy.lang.Script script = (groovy.lang.Script) bean;

				// if the "defaultArgs" variable exists then use its values. Can be used to set a default value by another Macro
				if (script.getBinding().hasVariable("defaultArgs")) { //$NON-NLS-1$
					oDefaultArgs = script.getBinding().getVariable("defaultArgs"); //$NON-NLS-1$
				}
			}
			else {
				try {
					if (bean.getClass().getField("defaultArgs") != null) {
						oDefaultArgs = bean.getClass().getField("defaultArgs").get(bean);
					}
				}
				catch (NoSuchFieldException e) {
				}
			}

			final Map args;
			if (oArgs == null || !(oArgs instanceof Map)) {
				args = new HashMap<String, Object>();
			}
			else {
				args = (Map) oArgs;
			}

			if (bean instanceof groovy.lang.Script) { // create the args binding if not already existing
				groovy.lang.Script script = (groovy.lang.Script) bean;
				if (!script.getBinding().hasVariable("args")) { //$NON-NLS-1$
					script.getBinding().setVariable("args", args); //$NON-NLS-1$
				}
			}

			final Map defaultArgs;
			if (oDefaultArgs == null || !(oDefaultArgs instanceof Map)) {
				defaultArgs = new HashMap<String, Object>();
			}
			else {
				defaultArgs = (Map) oDefaultArgs;
			}

			if (bean instanceof groovy.lang.Script) { // create the args binding if not already existing
				groovy.lang.Script script = (groovy.lang.Script) bean;
				if (!script.getBinding().hasVariable("defaultArgs")) { //$NON-NLS-1$
					script.getBinding().setVariable("defaultArgs", defaultArgs); //$NON-NLS-1$
				}
			}

			Log.finest("Calling syncExec on display: " + Display.getDefault()); //$NON-NLS-1$
			Display.getDefault().syncExec(new Runnable() {

				@Override
				public void run() {
					try {
						boolean okNames = true;
						CmdLineParser parser = new CmdLineParser(bean);

						Display display = Display.getCurrent();
						Shell shell = display.getActiveShell();
						Log.finest("Display=" + display + " Shell=" + shell); //$NON-NLS-1$ //$NON-NLS-2$
						// check names
						Class c = bean.getClass();
						Log.finest("Parsing options: " + parser.getOptions()); //$NON-NLS-1$
						for (OptionHandler opt : parser.getOptions()) { // pre set variable using the defaultArgs binding
							try {
								if (opt.option instanceof NamedOptionDef) {
									NamedOptionDef option = (NamedOptionDef) opt.option;
									c.getDeclaredField(option.name());
									if (defaultArgs.containsKey(option.name())) {
										option.def = "" + defaultArgs.get(option.name()); //$NON-NLS-1$
									}
								}
							}
							catch (NoSuchFieldException e) {
								Log.warning(TXMUIMessages.errorInAtOptionNameDeclarationColon + e.getMessage());
								okNames = false;
							}
						}
						if (okNames) {
							ParametersDialog dialog = new ParametersDialog(shell, bean, parser.getOptions(), args, defaultArgs);
							if (dialog.getShell() != null) {
								Log.finest("Activating shell:" + shell);
								shell.setActive();
							}

							ArrayList missingParameters = dialog.getParametersToSet();
							ArrayList missingMandatoryParameters = dialog.getMandatoryParametersToSet();

							dialog.initializeMacroVariables(); // with args
							if (missingParameters.size() > 0) {
								Log.finer("The parameters dialog must be opened to fill parameters " + missingParameters + " and all mandatory fields: " + missingMandatoryParameters); //$NON-NLS-1$ //$NON-NLS-2$
								retOpen = dialog.open() == ParametersDialog.OK;
								errorOpen = dialog.isError();
							}
							else { // fields initialized by 'args'
								Log.finest("The parameters dialog dont need to be opened all mandatory fields are set: " + args); //$NON-NLS-1$
								if (dialog.getNumberOfFields() > 0) dialog.saveParameters();
								retOpen = true;
								errorOpen = false;
							}

							if (bean instanceof groovy.lang.Script) { // reset the timer binding if already existing

								groovy.lang.Script script = (groovy.lang.Script) bean;
								if (script.getBinding().hasVariable("timer")) { //$NON-NLS-1$
									Log.finest("Reset groovy execution timer: " + args); //$NON-NLS-1$
									Timer timer = (Timer) script.getBinding().getVariable("timer"); //$NON-NLS-1$
									timer.reset();
								}
							}
						}
					}
					catch (Throwable e) {
						Log.severe(NLS.bind(TXMUIMessages.couldNotOpenTheParameterDialogP0, e));
						Log.printStackTrace(e);
						errorOpen = true;
					}
				}
			});

		}
		catch (Throwable e) {
			Log.severe(NLS.bind(TXMUIMessages.couldNotOpenTheParameterDialogP0, e));
			Log.printStackTrace(e);
			errorOpen = true;
		}

		return retOpen && !errorOpen;
	}

	protected ArrayList getMandatoryParametersToSet() {
		return mandatoryParametersToSetByTheUser;
	}

	protected ArrayList getParametersToSet() {
		return parametersToSetByTheUser;
	}

	public boolean getMustShowDialog() {
		return mustShowDialog;
	}

	protected int getNumberOfFields() {
		return parametersToSetByTheUser.size();
	}

	// TODO: uncomment this to enable "args" detection
	// public int open() {
	// System.out.println("Number of parameters: "+numberOfParameters);
	// if (numberOfParameters == 0) {
	// return ParametersDialog.OK;
	// } else {
	// return super.open();
	// }
	// }
}
