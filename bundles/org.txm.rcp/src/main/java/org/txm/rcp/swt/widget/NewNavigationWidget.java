// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.widget;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.utils.logger.Log;

/**
 * [<< |< < label > >| >>]
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class NewNavigationWidget extends Composite {

	/** The info line. */
	Label infoLine;

	Text currentPositionText;

	/** The first. */
	Button first;

	/** The last. */
	Button last;

	/** The next. */
	Button next;

	/** The prev. */
	Button prev;

	/** The n line per page. */
	int nLinePerPage;

	private Color whiteColor;


	/**
	 * Current position;
	 */
	protected int currentPosition;

	/**
	 * Minimum position.
	 */
	protected int minPosition;

	/**
	 * Maximum position.
	 */
	protected int maxPosition;


	/**
	 * Shift to apply between page number and real position.
	 */
	protected int shift;

	/**
	 * Instantiates a new navigation widget.
	 *
	 * @param parent the parent
	 * @param style the style
	 */
	public NewNavigationWidget(Composite parent, int style) {
		super(parent, style);

		this.shift = 1;
		this.currentPosition = 0;

		GridLayout layout = new GridLayout(6, false);
		layout.horizontalSpacing = 1;
		this.setLayout(layout);

		// | [|<] [<] [info] [>] [>|] |

		// [|<]
		// First match
		first = new Button(this, SWT.FLAT | SWT.PUSH);
		first.setToolTipText(TXMUIMessages.navigation2_tooltip_firstMatch);
		first.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		first.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				currentPosition = minPosition;
				refresh();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		// [<]
		// Previous match
		prev = new Button(this, SWT.FLAT | SWT.PUSH);
		prev.setToolTipText(TXMUIMessages.navigation2_tooltip_previousMatch);
		prev.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				currentPosition--;
				refresh();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});


		// [current]
		currentPositionText = new Text(this, SWT.BORDER);
		GridData currentPositionLayoutData = new GridData(GridData.VERTICAL_ALIGN_CENTER, SWT.CENTER, true, false);
		currentPositionLayoutData.minimumWidth = currentPositionLayoutData.widthHint = 50;
		currentPositionText.setToolTipText(TXMUIMessages.navigation2_tooltip_currentMatchNumber);
		currentPositionText.setLayoutData(currentPositionLayoutData);
		currentPositionText.setTextLimit(6);
		currentPositionText.setEditable(false);
		currentPositionText.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					currentPosition = getCurrentPositionFromTextField();
					refresh();
				}
			}
		});
		MouseWheelListener wheelListener = new MouseWheelListener() {

			@Override
			public void mouseScrolled(MouseEvent e) {
				if (e.count > 0) {
					currentPosition++;
					refresh();
				}
				else {
					currentPosition--;
					refresh();
				}
			}
		};
		currentPositionText.addMouseWheelListener(wheelListener);

		// [info]
		infoLine = new Label(this, SWT.NONE);
		GridData layoutData = new GridData(GridData.CENTER, SWT.CENTER, true, false);
		layoutData.minimumWidth = 50;
		infoLine.setLayoutData(layoutData);

		whiteColor = currentPositionText.getBackground();
		currentPositionText.setBackground(infoLine.getBackground());

		// [>]
		// Next match
		next = new Button(this, SWT.FLAT | SWT.PUSH);
		next.setToolTipText(TXMUIMessages.navigation2_tooltip_nextMatch);
		next.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				currentPosition++;
				refresh();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});


		// [>|]
		// Last match
		last = new Button(this, SWT.FLAT | SWT.PUSH);
		last.setToolTipText(TXMUIMessages.navigation2_tooltip_lastMatch);
		last.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				currentPosition = maxPosition;
				refresh();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		first.setImage(IImageKeys.getImage(IImageKeys.CTRLSTART));
		prev.setImage(IImageKeys.getImage(IImageKeys.CTRLREVERSE));
		next.setImage(IImageKeys.getImage(IImageKeys.CTRLPLAY));
		last.setImage(IImageKeys.getImage(IImageKeys.CTRLEND));
	}

	/**
	 * 
	 * @param parent
	 */
	public NewNavigationWidget(Composite parent) {
		this(parent, SWT.NONE);
	}

	public void addGoToKeyListener(KeyListener keyListener) {
		currentPositionText.addKeyListener(keyListener);
		currentPositionText.setEditable(true);
		// currentPositionText.setBackground(whiteColor);
		// currentPosition.setBackground(new Color(currentPosition.get));
	}

	public int getShift() {
		return shift;
	}

	public void setShift(int shift) {
		this.shift = shift;
	}

	/**
	 * Adds the first listener.
	 *
	 * @param selectionListener the selection listener
	 */
	public void addFirstListener(SelectionListener selectionListener) {
		first.addSelectionListener(selectionListener);
	}

	/**
	 * Adds the next listener.
	 *
	 * @param selectionListener the selection listener
	 */
	public void addNextListener(SelectionListener selectionListener) {
		next.addSelectionListener(selectionListener);
	}

	/**
	 * Adds the previous listener.
	 *
	 * @param selectionListener the selection listener
	 */
	public void addPreviousListener(SelectionListener selectionListener) {
		prev.addSelectionListener(selectionListener);
	}

	/**
	 * Adds the last listener.
	 *
	 * @param selectionListener the selection listener
	 */
	public void addLastListener(SelectionListener selectionListener) {
		last.addSelectionListener(selectionListener);
	}

	/**
	 * Sets the first enabled.
	 *
	 * @param b the new first enabled
	 */
	public void setFirstEnabled(boolean b) {
		first.setEnabled(b);
	}

	/**
	 * Sets the last enabled.
	 *
	 * @param b the new last enabled
	 */
	public void setLastEnabled(boolean b) {
		last.setEnabled(b);
	}

	/**
	 * Sets the next enabled.
	 *
	 * @param b the new next enabled
	 */
	public void setNextEnabled(boolean b) {
		next.setEnabled(b);
	}

	/**
	 * Sets the previous enabled.
	 *
	 * @param b the new previous enabled
	 */
	public void setPreviousEnabled(boolean b) {
		prev.setEnabled(b);
	}

	protected int getCurrentPositionFromTextField() {
		int ret = -1;
		try {
			ret = Integer.parseInt(this.currentPositionText.getText());
		}
		catch (Exception e) {
			System.out.println(TXMUIMessages.errorWhileGettingCurrentPositionValueColon);
			Log.printStackTrace(e);
		}
		return ret;
	}

	/**
	 * @return the minPosition
	 */
	public int getMinPosition() {
		return minPosition;
	}

	/**
	 * @param minPosition the minPosition to set
	 */
	public void setMinPosition(int minPosition) {
		this.minPosition = minPosition;
	}

	/**
	 * @param minPosition the minPosition to set
	 */
	public void setMinimum(int minPosition) {
		this.minPosition = minPosition;
	}

	/**
	 * @return the maxPosition
	 */
	public int getMaxPosition() {
		return maxPosition;
	}

	/**
	 * @param maxPosition the maxPosition to set
	 */
	public void setMaxPosition(int maxPosition) {
		this.maxPosition = maxPosition;
	}

	/**
	 * @param maxPosition the maxPosition to set
	 */
	public void setMaximum(int maxPosition) {
		this.maxPosition = maxPosition;
	}

	/**
	 * Refreshes the widgets.
	 */
	public void refresh() {

		// range tests
		if (this.currentPosition < this.minPosition) {
			this.currentPosition = this.minPosition;
		}
		else if (this.currentPosition > this.maxPosition) {
			this.currentPosition = this.maxPosition;
		}

		// update UI
		this.currentPositionText.setText(String.valueOf(this.currentPosition));

		this.prev.setEnabled(this.currentPosition > this.minPosition);
		this.next.setEnabled(this.currentPosition < this.maxPosition);
		this.first.setEnabled(this.currentPosition > this.minPosition);
		this.last.setEnabled(this.currentPosition < this.maxPosition);

		setInfoLineText(" / " + (1 + this.maxPosition - this.minPosition)); //$NON-NLS-1$
		this.layout();
	}


	/**
	 * Sets the info line text.
	 *
	 * @param s the new info line text
	 */
	public void setInfoLineText(String s) {
		this.infoLine.setText(s);
	}

	/**
	 * @param newPosition the currentPosition to set
	 */
	public void setCurrentPosition(int newPosition) {
		if (newPosition < 0) newPosition = 0;
		if (newPosition > maxPosition) newPosition = maxPosition;

		this.currentPosition = newPosition + this.shift;
	}

	/**
	 * To be compatible with the swtCombo.getSelection() method
	 */
	public void setSelection(int newPosition) {
		setCurrentPosition(newPosition);
	}


	/**
	 * @return the currentPosition
	 */
	public int getCurrentPosition() {
		return this.currentPosition - this.shift;
	}

	/**
	 * To be compatible with the swtCombo.getSelection() method
	 * 
	 * @return
	 */
	public int getSelection() {
		return getCurrentPosition();
	}

	/**
	 * @return the currentPositionText
	 */
	public Text getCurrentPositionText() {
		return currentPositionText;
	}

}
