// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands.workspace;

import java.io.File;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.Toolbox;
import org.txm.objects.Project;
import org.txm.rcp.StatusLine;
import org.txm.rcp.commands.RestartTXM;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.DeleteDir;
import org.txm.utils.io.FileCopy;
import org.txm.utils.logger.Log;
import org.txm.utils.zip.Zip;

/**
 * Create a project from one binary file (zip or directory)
 *
 * @author mdecorde
 */
public class LoadBinaryCorpus extends AbstractHandler {

	private static final String ID = LoadBinaryCorpus.class.getName();

	File corpusdir;

	private File zipFile;

	private File backup;

	String basedirname;

	@Override
	/**
	 * calling this command will : open a directory dialog and try to load the content of the selected directory
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {

		// create dialog to get the corpus directory
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

		String url = event.getParameter("url"); //$NON-NLS-1$

		if (url != null && url.length() > 0 && url.endsWith(".txm")) { //$NON-NLS-1$
			zipFile = new File(url);
			if (!zipFile.exists() && url.startsWith("http")) { //$NON-NLS-1$
				try {
					URL u = new URL(url);
					File f = new File(url);
					File corpora = Toolbox.workspace.getLocation();
					zipFile = new File(corpora, f.getName());
					zipFile.delete();
					FileUtils.copyURLToFile(u, zipFile);
				}
				catch (Exception e) {
					Log.printStackTrace(e);
					return null;
				}
			}
		}
		else {
			FileDialog dialog = new FileDialog(shell, SWT.OPEN);
			dialog.setFilterExtensions(new String[] { "*.txm" }); //$NON-NLS-1$
			if (LastOpened.getFile(ID) != null) {
				dialog.setFilterPath(LastOpened.getFolder(ID));
				dialog.setFileName(LastOpened.getFile(ID));
			}

			if (dialog.open() != null) {
				final String path = dialog.getFilterPath();
				final String filename = dialog.getFileName();

				zipFile = new File(path, filename);
				LastOpened.set(ID, zipFile);
			}
			else {
				return null;
			}
		}

		JobHandler job = loadBinaryCorpusArchive(zipFile);
		return job;
	}

	/**
	 * Load a binary corpus archive:
	 * 
	 * 1- test if the corpus directory is already used
	 * 2- delete previous corpus version
	 * 3- unzip the archive
	 * 4- load the binary corpus directory
	 * 5- restart TXM engines
	 */
	public static JobHandler loadBinaryCorpusArchive(final File zipFile) throws ExecutionException {

		Log.info(TXMUIMessages.loadingBinaryCorpus);

		String filename = zipFile.getName();
		final File corporaDir = Toolbox.workspace.getLocation();
		corporaDir.mkdir();

		if (!filename.endsWith(".txm") && !filename.endsWith(".zip")) { //$NON-NLS-1$ //$NON-NLS-2$
			Log.warning(TXMUIMessages.aBinaryCorpusIsATXMFileExtensionTxmOrZip);
			Log.warning(NLS.bind(TXMUIMessages.thisFileIsNotATXMFileColonP0, zipFile));
			return null;
		}

		if (!zipFile.canRead()) {
			Log.warning(NLS.bind(TXMUIMessages.cannotReadTheFileP0, zipFile));
			return null;
		}

		// build binary dir path
		String basedirname = Zip.getRoot(zipFile).toUpperCase();

		File corpusdir = new File(corporaDir, basedirname);
		Project p = Toolbox.workspace.getProject(basedirname);

		if (corpusdir.exists() || p != null) {
			if (Display.getCurrent() == null) { // not a UI thread -> delete the previous corpus
				p.delete(); // the user said yes, so delete the previous project
			}
			else {

				boolean b = MessageDialog.openConfirm(Display.getCurrent().getActiveShell(), TXMUIMessages.warning, NLS.bind(TXMUIMessages.theP0CorpusDirectoryAlreadyExistsDoYouWantToReplaceIt,
						corpusdir.getName().toUpperCase()));
				if (b) {
					// TODO: to enable backup of binary corpus I need to be able to reload correctly a corpus in CQP corpus manager
					// backup = new File(corpusdir.getParentFile(), corpusdir.getName()+"-back"); //$NON-NLS-1$
					// DeleteDir.deleteDirectory(backup);
					// if (backup.exists()) {
					// System.out.println(Messages.LoadBinaryCorpus_1+backup);
					// return false;
					// }
					// if (!corpusdir.renameTo(backup)) {
					// System.out.println(Messages.LoadBinaryCorpus_2+backup);
					// return false;
					// }

					if (p != null) {
						p.delete(); // the user said yes, so delete the previous project
					}
					else {
						DeleteDir.deleteDirectory(corpusdir);
					}

					if (corpusdir.exists()) {
						return null;
					}
				}
				else {
					return null; // abandon
				}
			}
		}

		// scan dir
		JobHandler jobhandler = new JobHandler(
				NLS.bind(TXMUIMessages.newCorpusColonP0, corpusdir.getName())) {

			@Override
			protected IStatus _run(final SubMonitor monitor) {

				final Project newProject2 = loadBinaryCorpusArchive(zipFile, this, monitor, false);
				this.setResultObject(newProject2);
				this.syncExec(new Runnable() {

					@Override
					public void run() {

						if (newProject2 != null) {
							// newProject2.compute(); // TODO calling compute now calls preferences.flush --> JOB LOCK
							RestartTXM.reloadViews();
							// System.out.println("Select newly loaded corpus: "+base2.getCorpora().values());
							CorporaView.select(newProject2.getChildren(MainCorpus.class));
							StatusLine.setMessage(TXMUIMessages.info_txmIsReady);
							Log.info(NLS.bind(TXMUIMessages.P0CorpusLoaded, newProject2));
						}
						else {
							System.out.println(TXMUIMessages.ErrorNoProjectLoaded);
						}
					}
				});
				return Status.OK_STATUS;
				// return Status.CANCEL_STATUS;
			}
		};

		try {
			jobhandler.startJob();
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}

		return jobhandler;
	}

	/**
	 * don't manage the SearchEngine restart
	 * 
	 * @param zipFile
	 * @param jobhandler
	 * @param monitor
	 * @param replace
	 * @return
	 */
	public static Project loadBinaryCorpusArchive(File zipFile, JobHandler jobhandler, final IProgressMonitor monitor, boolean replace) {
		Project project = null;
		try {
			// SearchEngine engine = SearchEnginesManager.getCQPSearchEngine();
			// if (engine.isRunning()) {
			// engine.stop();
			// }

			// CHECK ZIPPED FILE
			if (Load080BinaryCorpus.canLoad(zipFile)) {
				project = Load080BinaryCorpus.loadBinaryCorpusArchive(zipFile, monitor, replace);
			}
			else if (Load079BinaryCorpus.canLoad(zipFile)) {
				project = Load079BinaryCorpus.loadBinaryCorpusArchive(zipFile, monitor, replace);
			}
			else {
				Log.warning(NLS.bind(TXMUIMessages.TheP0ArchiveIsNotATXMCorpus, zipFile));
				return null;
			}
			if (project == null) {
				Log.warning(NLS.bind(TXMUIMessages.FailedToLoadTheP0Corpus, zipFile));
				return null;
			}
		}
		catch (ThreadDeath td) {
			return null;
		}
		catch (Exception e2) {
			org.txm.utils.logger.Log.printStackTrace(e2);
			return null;
		}
		finally {

		}
		return project;
	}

	/**
	 * load the content of the file if its a directory containing at least the sub folders :
	 * txm, html, data and registry.
	 * 
	 * @param replace
	 *
	 * @param corpusDirectory the corpusDirectory
	 * @return the base
	 * @throws Exception the exception
	 */
	public static Project loadBinaryCorpusAsDirectory(File corpusDirectory, IProgressMonitor monitor, boolean replace) throws Exception {
		if (!(corpusDirectory.exists() && corpusDirectory.isDirectory())) {
			Log.warning(NLS.bind(TXMUIMessages.failedToLoadCorpusP0, corpusDirectory.getAbsolutePath()));
			return null;
		}

		// SearchEngine engine = SearchEnginesManager.getCQPSearchEngine();
		// if (engine.isRunning()) {
		// engine.stop();
		// }

		// //File txmregistry = new File(Toolbox.getTxmHomePath(), "registry"); //$NON-NLS-1$
		// //txmregistry.mkdir();
		// File txmcorpora = new File(Toolbox.getTxmHomePath(), "corpora"); //$NON-NLS-1$
		// txmcorpora.mkdir();
		// Project base = Toolbox.workspace.scanDirectory(binCorpusDirectory);
		// if (base == null) {
		// Log.severe(NLS.bind(TXMUIMessages.failedToLoadCorpusFromDirectoryColonP0WeCannotFindTheNecessaryComponents, binCorpusDirectory));
		// return null;
		// }
		//
		// Toolbox.workspace.save();

		// test with project name
		Project p = Toolbox.workspace.getProject(corpusDirectory.getName().toUpperCase());
		if (p != null) {
			Log.warning(NLS.bind(TXMUIMessages.abortingLoadingOfP0ACorpusWithTheSameNameAlreadyExistsInP1, corpusDirectory, corpusDirectory.getName().toUpperCase()));
			return null;
		}

		// test with CQP MainCorpus name
		File dataDirectory = new File(corpusDirectory, "data"); //$NON-NLS-1$
		File[] mainCorpusDirs = dataDirectory.listFiles();
		if (mainCorpusDirs != null && mainCorpusDirs.length > 0) {
			String mainCorpusName = mainCorpusDirs[0].getName();
			p = Toolbox.workspace.getProject(mainCorpusName);
			if (p != null) {
				Log.warning(NLS.bind(TXMUIMessages.abortingLoadingOfP1ACorpusWithTheSameCQPIDP1AlreadyExists, corpusDirectory, mainCorpusName));
				return null;
			}
		}

		// seems like a binary corpus directory
		// copy files in the new current corpora directory
		File destDir = new File(Toolbox.workspace.getLocation(), corpusDirectory.getName().toUpperCase());
		if (!destDir.equals(corpusDirectory)) {
			FileCopy.copyFiles(corpusDirectory, destDir); // the corpus directory is not in the corpora directory
		}

		Project project = null;
		File paramFile = new File(destDir, ".settings"); //$NON-NLS-1$
		File importXMLFile = new File(destDir, "import.xml"); //$NON-NLS-1$
		File dotProjectFile = new File(destDir, ".project"); //$NON-NLS-1$
		if (paramFile.exists() && paramFile.isDirectory() && dotProjectFile.exists() && dotProjectFile.isFile()) {
			Log.info(NLS.bind(TXMUIMessages.theP0BinaryCorpusVersionIsP1, destDir, "0.8.0")); //$NON-NLS-1$
			project = Load080BinaryCorpus.loadBinaryCorpusAsDirectory(destDir);
		}
		else if (importXMLFile.exists()) {
			Log.info(NLS.bind(TXMUIMessages.loadingTheP0BinaryCorpusAsATXM079Corpus, destDir));
			project = Load079BinaryCorpus.loadBinaryCorpusAsDirectory(destDir, null);
		}
		else {
			Log.warning(NLS.bind(TXMUIMessages.theP0directoryNotABinaryCorpus, destDir));
		}

		if (!destDir.equals(corpusDirectory) && project == null) { // clean if necessary
			DeleteDir.deleteDirectory(destDir); // the corpus directory was not in the corpora directory
		}

		return project;
	}
}
