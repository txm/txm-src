// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.dialog;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

// TODO: Auto-generated Javadoc
/**
 * build a query with the help of simple questions @ author mdecorde.
 */
public class ValuePlusComboDialog extends Dialog {

	/** The main panel. */
	Composite mainPanel;

	/** The parent shell. */
	Shell parentShell;

	/** The values. */
	List<String> comboValues;

	/** The combo. */
	private Combo combo;

	/** The selected index. */
	private int selectedComboIndex = -1;

	/** The selected value. */
	private String selectedComboValue = ""; //$NON-NLS-1$

	/** The default value. */
	private String defaultComboValue;

	/** The title. */
	private String title;

	/** The title. */
	private String defaultValue;

	private String valueTitle;

	private Integer comboStyle;

	private Text valueText;

	private String value;

	private String comboTitle;

	/**
	 * Instantiates a new combo dialog.
	 *
	 * @param parentShell the parent shell
	 * @param title the title
	 * @param comboValues the values
	 * @param defaultComboValue the default value (may be null)
	 */
	public ValuePlusComboDialog(Shell parentShell, String title, String valueTitle, String defaultValue, String comboTitle, List<String> comboValues, String defaultComboValue) {

		this(parentShell, title, valueTitle, defaultValue, comboTitle, comboValues, defaultComboValue, null);
	}

	/**
	 * Instantiates a new combo dialog.
	 *
	 * @param parentShell the parent shell
	 * @param title the title
	 * @param comboValues the values
	 * @param defaultComboValue the default value (may be null)
	 * @param comboStyle if null combo is SINGLE and READONLY
	 */
	public ValuePlusComboDialog(Shell parentShell, String title, String valueTitle, String defaultValue, String comboTitle, List<String> comboValues, String defaultComboValue, Integer comboStyle) {

		super(parentShell);
		this.parentShell = parentShell;
		this.setShellStyle(this.getShellStyle());
		this.comboValues = comboValues;
		this.defaultComboValue = defaultComboValue;
		this.title = title;
		this.comboStyle = comboStyle;
		this.valueTitle = valueTitle;
		this.defaultValue = defaultValue;
		this.comboTitle = comboTitle;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {

		super.configureShell(newShell);
		newShell.setText(title);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite p) {
		//System.out.println(p.getLayout());
		GridLayout layout = (GridLayout) p.getLayout();
		layout.verticalSpacing = 5;
		layout.marginBottom = 5;
		layout.marginTop = 5;
		layout.marginLeft = 5;
		layout.marginRight = 5;

		Label l = new Label(p, SWT.NONE);
		if (valueTitle != null) l.setText(valueTitle);

		valueText = new Text(p, SWT.BORDER);
		valueText.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 5, 1));
		if (defaultValue != null) valueText.setText(defaultValue);

		l = new Label(p, SWT.NONE);
		if (comboTitle != null) l.setText(comboTitle);

		int style = SWT.READ_ONLY | SWT.SINGLE;
		if (comboStyle != null) {
			style = comboStyle;
		}
		combo = new Combo(p, style);
		combo.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 5, 1));
		combo.setItems(comboValues.toArray(new String[] {}));
		// combo.setLayoutData(new GridData(GridData.FILL,GridData.FILL, true,
		// true));
		if (comboValues.size() > 0)
			combo.select(0);
		if (defaultComboValue != null)
			selectValue(defaultComboValue);

		return p;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {

		selectedComboIndex = combo.getSelectionIndex();
		if (selectedComboIndex > 0) {
			selectedComboValue = comboValues.get(selectedComboIndex);
		}
		else {
			selectedComboValue = combo.getText();
		}

		value = valueText.getText();

		super.okPressed();
	}

	/**
	 * Gets the values.
	 *
	 * @return the values
	 */
	public List<String> getValues() {

		return comboValues;
	}

	/**
	 * Sets the values.
	 *
	 * @param values the new values
	 */
	public void setValues(List<String> values) {

		this.comboValues = values;
		combo.setItems(values.toArray(new String[] {}));
		if (values.size() > 0) {
			combo.select(0);
		}
	}

	/**
	 * Gets the selected index.
	 *
	 * @return the selected index
	 */
	public int getSelectedIndex() {

		return selectedComboIndex;
	}

	/**
	 * Gets the selected value.
	 *
	 * @return the selected value
	 */
	public String getSelectedComboValue() {

		return selectedComboValue;
	}

	/**
	 * Select value.
	 *
	 * @param string the string
	 */
	public void selectValue(String string) {

		for (int i = 0; i < comboValues.size(); i++)
			if (comboValues.get(i).equals(string)) {
				combo.select(i);
				break;
			}
	}

	public String getValue() {
		return value;
	}
}
