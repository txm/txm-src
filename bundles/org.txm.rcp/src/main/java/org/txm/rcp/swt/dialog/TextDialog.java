package org.txm.rcp.swt.dialog;
/*
SWT/JFace in Action
GUI Design with Eclipse 3.0
Matthew Scarpino, Stephen Holder, Stanford Ng, and Laurent Mihalkovic

ISBN: 1932394273

Publisher: Manning
 */


import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * 
 * 
 *
 */
public class TextDialog extends Dialog {

	private static final int RESET_ID = IDialogConstants.NO_TO_ALL_ID + 1;

	private Text textField;

	String title;

	String question;

	String text;

	/**
	 * 
	 * @param parentShell
	 * @param title
	 * @param question
	 * @param text
	 */
	public TextDialog(Shell parentShell, String title, String question, String text) {
		super(parentShell);
		this.title = title;
		this.question = question;
		this.text = text;
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(title);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite comp = (Composite) super.createDialogArea(parent);

		GridLayout layout = (GridLayout) comp.getLayout();
		layout.numColumns = 1;
		layout.verticalSpacing = 5;

		GridData data = new GridData(GridData.FILL_HORIZONTAL);

		Label questionLabel = new Label(comp, SWT.RIGHT);
		questionLabel.setText(question);
		questionLabel.setLayoutData(data);

		textField = new Text(comp, SWT.SINGLE | SWT.BORDER);
		data = new GridData(GridData.FILL_HORIZONTAL);
		textField.setLayoutData(data);
		if (this.text != null) {
			textField.setText(this.text);
			textField.setSelection(0, this.text.length());
		}
		return comp;
	}

	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == Dialog.OK) {
			text = textField.getText();
		}
		super.buttonPressed(buttonId);
	}

	public String getText() {
		return text;
	}
}
