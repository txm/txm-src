package org.txm.rcp.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

/**
 * Use BaseAbstractHandler instead
 * 
 * @author mdecorde
 *
 */
@Deprecated
public class TxmCommand extends AbstractHandler {

	protected String name = "noname"; //$NON-NLS-1$	@Override

	public Object execute(ExecutionEvent event) throws ExecutionException {
		return null;
	}
}
