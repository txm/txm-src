// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.txm.rcp.messages.TXMUIMessages;

// TODO: Auto-generated Javadoc
/**
 * The Class ScriptPreferencePage.
 */
public class ScriptPreferencePage extends RCPPreferencesPage {

	/** The nosession. */
	private IntegerFieldEditor nosession;

	private BooleanFieldEditor save_before_execution;

	/**
	 * Instantiates a new script preference page.
	 */
	public ScriptPreferencePage() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 */
	@Override
	protected void createFieldEditors() {

		save_before_execution = new BooleanFieldEditor(RCPPreferences.SAVE_BEFORE_EXECUTION,
				TXMUIMessages.saveScriptBeforeExecution, getFieldEditorParent());
		addField(save_before_execution);

		nosession = new IntegerFieldEditor(RCPPreferences.NO_SESSION,
				TXMUIMessages.nextSessionNumber, getFieldEditorParent());
		addField(nosession);
	}
}
