package org.txm.rcp.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.commons.lang.StringUtils;

public class URLUtils {

	@SuppressWarnings("deprecation")
	public static String encodeURL(String url) {
		StringBuilder buffer = new StringBuilder();
		int begin = url.indexOf(":/"); //$NON-NLS-1$
		buffer.append(url.substring(0, begin + 2));
		url = url.substring(begin + 2);
		String[] split = url.split("/"); //$NON-NLS-1$
		String[] ps = new String[split.length];
		for (int i = 0; i < split.length; i++) {
			if (split[i].length() > 0) {
				ps[i] = URLEncoder.encode(split[i]);
			}
			else {
				ps[i] = split[i];
			}
		}
		buffer.append(StringUtils.join(ps, "/")); //$NON-NLS-1$
		return buffer.toString();
	}

	public static URL encodeURL(URL url) throws MalformedURLException {
		return new URL(encodeURL(url.toString()));
	}
}
