package org.txm.rcp.commands.workspace;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.objects.Project;
import org.txm.rcp.handlers.scripts.ExecuteImportScript;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.corpus.MainCorpus;

public class RebuildCorpus extends AbstractHandler {

	public static final String ID = RebuildCorpus.class.getCanonicalName();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection sel = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		if (!(sel instanceof IStructuredSelection)) return null;
		IStructuredSelection selection = (IStructuredSelection) sel;

		Object s = selection.getFirstElement();
		Project p = null;
		if (s instanceof MainCorpus) {
			MainCorpus corpus = (MainCorpus) s;
			p = corpus.getProject();
		}
		else if (s instanceof Project) {
			p = (Project) s;
		}
		if (p != null) {
			return update(p);
		}
		else {
			return null;
		}

	}

	public static JobHandler update(Project project) {
		project.setDirty();
		return ExecuteImportScript.executeScript(project);
	}
}
