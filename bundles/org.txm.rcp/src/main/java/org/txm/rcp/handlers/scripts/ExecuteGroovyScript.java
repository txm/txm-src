// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.scripts;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.txm.Toolbox;
import org.txm.groovy.core.GSERunner;
import org.txm.rcp.editors.TxtEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.swt.widget.parameters.UIParameterException;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.utils.Timer;
import org.txm.utils.logger.Log;

import groovy.lang.Binding;
import groovy.lang.Script;

/**
 * Handler to Execute a Groovy script.
 * 
 * 1- If no parameter "org.txm.rcp.command.parameter.file" is set
 * 2- use the current selection
 * 2.1 TXTEditor text selection and opened file
 * 2.2 File
 * 3- then ask the user to select a script
 * 
 * @author mdecorde
 */
public class ExecuteGroovyScript extends AbstractHandler {

	private static final String ID = "org.txm.rcp.commands.ExecuteGroovyScript"; //$NON-NLS-1$

	private static GSERunner gse;

	private static String previousScriptRootDir;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		String filenameParameter = event
				.getParameter("org.txm.rcp.command.parameter.file"); //$NON-NLS-1$
		String stringArgs = event.getParameter("args"); //$NON-NLS-1$

		// the file to execute
		String result = ""; //$NON-NLS-1$
		IWorkbenchPart activePart = HandlerUtil.getActivePart(event);
		IWorkbenchPart activeEditor = HandlerUtil.getActivePart(event);

		ISelection sel = HandlerUtil.getCurrentSelection(event);

		if (activeEditor != null && activeEditor instanceof TxtEditor) {
			TxtEditor te = (TxtEditor) activeEditor;
			FileStoreEditorInput input = (FileStoreEditorInput) te.getEditorInput();

			File f = new File(input.getURI().getPath());
			result = f.getAbsolutePath();
		}
		else if (filenameParameter != null) {
			result = filenameParameter;
			File f = new File(filenameParameter);
			if (!f.exists()) { // file not found, try locating it in Groovy scripts
				f = new File(Toolbox.getTxmHomePath() + "/scripts/groovy/user", filenameParameter); //$NON-NLS-1$
				if (f.exists()) { // found !
					result = f.getAbsolutePath();
				}
			}
		}
		else if (sel != null & sel instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) sel;
			for (Object o : strucSelection.toList()) {
				if (o instanceof File file) {
					result = file.getAbsolutePath();
					break;
				}
			}
		}

		if ("".equals(result)) { //$NON-NLS-1$
			Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
			FileDialog dialog = new FileDialog(shell, SWT.OPEN);
			if (LastOpened.getFile(ID) != null) {
				dialog.setFilterPath(LastOpened.getFolder(ID));
				dialog.setFileName(LastOpened.getFile(ID));
			}
			dialog.setFilterExtensions(new String[] { "*.groovy" }); //$NON-NLS-1$

			IPreferencesService service = Platform.getPreferencesService();
			String scriptCurrentDir = Toolbox.getTxmHomePath() + "/scripts"; //$NON-NLS-1$

			dialog.setFilterPath(scriptCurrentDir);
			result = dialog.open();
			if (result == null) return null;
			LastOpened.set(ID, new File(result));
		}
		executeScript(result, activePart, activeEditor, sel, stringArgs);

		return null;
	}

	/**
	 * 
	 * @param scriptpath
	 * @param page current active page, might be null
	 * @return
	 */
	public static JobHandler executeScript(String scriptpath, IWorkbenchPart activePart, IWorkbenchPart activeEditor, ISelection selection, boolean modal, String stringArgs) {
		IPreferencesService service = Platform.getPreferencesService();
		//		String scriptRootDir = service.getString(Application.PLUGIN_ID,
		//				ScriptPreferencePage.SCRIPT_ROOT_DIR,
		//				ScriptPreferenceInitializer.SCRIPT_ROOT_DIR_DEFAULT, null);
		//		
		String scriptRootDir = Toolbox.getTxmHomePath() + "/scripts"; //$NON-NLS-1$
		File currentRootDir = new File(scriptRootDir, "groovy/user"); //$NON-NLS-1$
		if (stringArgs == null) stringArgs = ""; //$NON-NLS-1$
		return executeScript(currentRootDir, scriptpath, activePart, activeEditor, selection, modal, Arrays.asList(stringArgs.split("\t")), null, null); //$NON-NLS-1$
	}

	/**
	 * 
	 * @param scriptpath
	 * @param page current active page, might be null
	 */
	public static void executeScript(String scriptpath, IWorkbenchPart activePart, IWorkbenchPart activeEditor, ISelection selection, String stringArgs) {
		executeScript(scriptpath, activePart, activeEditor, selection, false, stringArgs);
	}

	/**
	 * 
	 * @param scriptpath
	 * @param page current active page, might be null
	 */
	public static JobHandler executeScript(String scriptpath, IWorkbenchPart activePart, IWorkbenchPart activeEditor, final ISelection selection) {
		return executeScript(scriptpath, activePart, activeEditor, selection, false, null);
	}

	/**
	 * 
	 * @param currentRootDir
	 * @param scriptpath
	 * @param activePart current active page, might be null
	 * @param sel current selection, might be null
	 */
	public static JobHandler executeScript(final File currentRootDir, String scriptpath, final IWorkbenchPart activePart, final IWorkbenchPart activeEditor, final ISelection sel, final boolean modal,
			final List<String> stringArgs, HashMap<String, Object> parameters, HashMap<String, Object> defaultParameters) {
		// check script
		final File scriptfile = new File(scriptpath);
		if (!scriptfile.exists()) {
			Log.severe(NLS.bind(TXMUIMessages.p0ScriptFileDoesntExist, scriptpath));
			return null;
		}
		ExecuteLastGroovyScript.setLastScript(scriptfile, currentRootDir.getName().equals("macro")); // register last groovy script //$NON-NLS-1$

		// get current selection of the corpora view
		final Object corpusViewSelection = CorporaView.getFirstSelectedObject();
		final Object corpusViewSelections = CorporaView.getSelectedObjects();

		final Object selection;
		final List selections;
		if (sel != null && sel instanceof IStructuredSelection) {
			selection = ((IStructuredSelection) sel).getFirstElement();
			selections = ((IStructuredSelection) sel).toList();
		}
		else {
			selection = sel;
			selections = null;
		}

		//		 //$NON-NLS-1$
		//		if (previousScriptRootDir == null || !tmp.equals(previousScriptRootDir)) {
		//			previousScriptRootDir = tmp;
		//			gse = null;
		//			//System.out.println("SCRIPT ROOT DIR CHANGED tmp="+tmp);
		//		}

		String dir = currentRootDir.getAbsolutePath();
		String path = scriptfile.getAbsolutePath();
		if (!path.startsWith(dir)) {
			Log.severe(TXMUIMessages.bind(TXMUIMessages.theP1GroovyScriptMustBeInTheP0Folder, dir, path));
			return null;
		}
		final String relativepath = path.substring(dir.length() + 1);

		//if (gse == null) {

		//}

		JobHandler jobhandler = new JobHandler(NLS.bind(TXMUIMessages.executionOfP0, scriptfile.getName())) {

			@Override
			protected IStatus _run(SubMonitor monitor) throws Throwable {

				monitor.beginTask(NLS.bind(TXMUIMessages.executionOfP0, scriptfile.getName()), 100);

				try {
					gse = GSERunner.buildDefaultGSE();
					if (gse == null) {
						Log.severe(TXMUIMessages.CouldNotCreateTheGrrovyScriptEngineAborting);
						return Status.CANCEL_STATUS;
					}

					Binding binding = new Binding();
					binding.setVariable("part", activePart); //$NON-NLS-1$
					binding.setVariable("editor", activeEditor); //$NON-NLS-1$
					binding.setVariable("selection", selection); //$NON-NLS-1$
					binding.setVariable("selections", selections); //$NON-NLS-1$
					binding.setVariable("corpusViewSelection", corpusViewSelection); //$NON-NLS-1$
					binding.setVariable("corpusViewSelections", corpusViewSelections); //$NON-NLS-1$
					binding.setProperty("monitor", this); //$NON-NLS-1$
					binding.setProperty("MONITOR", this); //$NON-NLS-1$
					binding.setProperty("stringArgs", stringArgs); //$NON-NLS-1$
					if (parameters != null) binding.setProperty("args", parameters); //$NON-NLS-1$
					if (defaultParameters != null) binding.setProperty("defaultArgs", defaultParameters); //$NON-NLS-1$
					binding.setProperty("gse", gse); //$NON-NLS-1$
					Timer timer = new Timer();
					binding.setProperty("timer", timer); //$NON-NLS-1$

					Log.info(NLS.bind(TXMUIMessages.CompilingP0Etc, scriptfile.getName()));
					Script script = gse.createScript(relativepath, binding);
					Log.info(NLS.bind(TXMUIMessages.executingGroovyScriptP0, scriptfile.getName()));
					script.run();
					Log.info(TXMUIMessages.bind(TXMUIMessages.doneP0Ms, timer.ellaspeTime()));
				}
				catch (UIParameterException e) {
					if (e.isError()) {
						Log.severe(TXMUIMessages.errorDuringParametersInitialisation);
					}
					else {
						Log.severe(TXMUIMessages.theUserCanceledTheScriptExecution);
					}
				}
				catch (Throwable e) {
					Log.severe(TXMUIMessages.errorDuringScriptExecutionColon + e);
					throw e;
				}
				return Status.OK_STATUS;
			}
		};

		jobhandler.startJob(modal);
		return jobhandler;
		//        //gse.loadScriptByName("scripts/B.groovy");
		//		gse.run("A.groovy", new Binding());

		//		ClassLoader parent = getClass().getClassLoader();
		//		GroovyClassLoader loader = new GroovyClassLoader(parent);
		//
		//		List<File> classPath = new ArrayList<File>();
		//		classPath.add(new File(scriptRootDir));
		/*
		 * classPath.add(new File(scriptfile));
		 * classPath.add(new File(scriptfile).getParentFile().getParentFile());
		 * classPath.add(new File("")); //$NON-NLS-1$
		 */
		// System.out.println(classPath);
		//System.out.println("Build groovy scripts - Start");
		//org.txm.Build.run();
		//System.out.println("Build groovy scripts - Done");

		//		Class groovyClass = null;
		//		for (File f : classPath)
		//			loader.addClasspath(f.getAbsolutePath());
		//		loader.setShouldRecompile(true);
		//
		//		try {
		//			groovyClass = loader.parseClass(new File(scriptfile));
		//		} catch (CompilationFailedException e) {
		//			org.txm.utils.logger.Log.printStackTrace(e);
		//		} catch (IOException e) {
		//			org.txm.utils.logger.Log.printStackTrace(e);
		//		}
		//
		//		// let's call some method on an instance
		//		GroovyObject groovyObject = null;
		//		try {
		//			groovyObject = (GroovyObject) groovyClass.newInstance();
		//		} catch (InstantiationException e) {
		//			org.txm.utils.logger.Log.printStackTrace(e);
		//		} catch (IllegalAccessException e) {
		//			org.txm.utils.logger.Log.printStackTrace(e);
		//		}
		//
		//		Object[] args = {};
		//		try {
		//			groovyObject.invokeMethod("run", args); //$NON-NLS-1$
		//		} catch (Exception e) {
		//			System.err.println(e);
		//			org.txm.utils.logger.Log.printStackTrace(e);
		//		}

		/*
		 * try { File script = new File(scriptfile); // GroovyImportScriptRunner
		 * g = new GroovyImportScriptRunner(new File(""),new ArrayList<File>());
		 * GroovyClassLoader classLoader = new
		 * GroovyClassLoader(this.getClass().getClassLoader());
		 * classLoader.addURL(script.getParentFile().toURI().toURL());
		 * GroovyScriptEngine gse = new
		 * GroovyScriptEngine(script.getParent(),classLoader);
		 * gse.run(script.getAbsolutePath(), new Binding());
		 * System.out.println("END : execute script RCP "); } catch (Exception
		 * e) { // TODO Auto-generated catch block org.txm.utils.logger.Log.printStackTrace(e); }
		 * /* if (scriptfile == "") return; final File script = new
		 * File(scriptfile);
		 * String[] roots = new String[] {script.getParent()};
		 * System.out.println("SCRIPTROOTDIR "+script.getParent());
		 * System.out.println("SCRIPT "+script); GroovyScriptEngine gse; try {
		 * gse = new GroovyScriptEngine(roots); for(File f :
		 * script.getParentFile().listFiles(IOUtils.HIDDENFILE_FILTER))
		 * gse.loadScriptByName(f.getName()); Binding binding = new Binding();
		 * System.out.println("start execution"); gse.run(script.getName(),
		 * binding); System.out.println("end execution"); } catch (Exception e)
		 * {org.txm.utils.logger.Log.printStackTrace(e);}
		 */
	}
}
