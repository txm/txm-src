package org.txm.rcp.swt.widget.parameters;

import org.eclipse.jface.dialogs.PopupDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.kohsuke.args4j.NamedOptionDef;

public abstract class ParameterField extends Composite implements IParameterField {

	NamedOptionDef parameter;

	private PopupDialog popup;

	public ParameterField(Composite parent, int style, NamedOptionDef parameter) {
		super(parent, style);
		this.parameter = parameter;

		//		this.addMouseTrackListener(new MouseTrackListener() {
		//			
		//			@Override
		//			public void mouseHover(MouseEvent e) { }
		//			
		//			@Override
		//			public void mouseExit(MouseEvent e) { }
		//			
		//			@Override
		//			public void mouseEnter(MouseEvent e) {
		//				showTooltip();
		//			}
		//
		//			
		//		});
	}

	public String getWidgetName() {
		return parameter.name();
	}

	public String getWidgetUsage() {
		return parameter.usage();
	}

	public String getMetaVar() {
		return parameter.metaVar();
	}

	/**
	 * 
	 * @return the default value if the Macro has not been called previously
	 */
	public String getWidgetDefault() {
		return parameter.def();
	}

	public String getWidgetType() {
		return parameter.widget();
	}

	public boolean isWidgetMandatory() {
		return parameter.required();
	}

	public boolean isWidgetMultiValued() {
		return parameter.isMultiValued();
	}

	public String getWidgetLabel() {
		return getWidgetName() + ((parameter.required()) ? " *" : "") + " : "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	/**
	 * Tests to replace the SWT tooltip with a Eclipse editor popup
	 */
	private synchronized void showTooltip() {

		System.out.println("POPUP: " + popup);
		if (popup == null) {
			//Shell parent, int shellStyle, boolean takeFocusOnOpen, boolean persistSize, boolean persistLocation, boolean showDialogMenu, boolean showPersistActions, String titleText, String infoText
			popup = new PopupDialog(this.getShell(), SWT.NONE, true, true, true, true, true, "Help", parameter.usage() + "\n<a href=\"https://gitlab.huma-num.fr/txm/txm-manual\">manual<a/>");
		}
		if (popup.getShell() == null) {
			popup.open();
		}
	}

	public abstract Object getWidgetValue();

	public abstract void setDefault(Object value);

	public abstract void resetToDefault();
}
