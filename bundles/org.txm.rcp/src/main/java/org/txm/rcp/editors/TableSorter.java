// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.editors;

import java.text.Collator;
import java.util.Locale;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.txm.rcp.messages.TXMUIMessages;

// TODO: Auto-generated Javadoc
/**
 * copied from
 * http://www.vogella.de/articles/EclipseJFaceTable/article.html#sortcolumns
 * 
 * @author sloiseau
 * 
 */
// FIXME: SJ: this class declares some column types but they are not used if tne sorting
// may need to finalize this class or
// also see the class: TableLinesViewerComparator and SpecificitiesLinesViewerComparator for a full implemented other way example
@Deprecated
public class TableSorter extends ViewerSorter {

	/** The locale. */
	private Locale locale = new Locale("en", "US"); //$NON-NLS-1$ //$NON-NLS-2$

	/** The property index. */
	private int propertyIndex;

	/** The Constant DESCENDING. */
	private static final int DESCENDING = 1;

	/** The direction. */
	private int direction = DESCENDING;

	/** The column types. */
	private String[] columnTypes;

	/** The collator. */
	private Collator collator;

	/**
	 * Instantiates a new table sorter.
	 */
	public TableSorter() {
		this.propertyIndex = 0;
		direction = DESCENDING;
		this.collator = Collator.getInstance(locale);
		this.collator.setStrength(Collator.TERTIARY);
	}

	/**
	 * Sets the column types.
	 *
	 * @param types the new column types
	 */
	public void setColumnTypes(String[] types) {
		columnTypes = types;
	}

	/**
	 * Sets the column.
	 *
	 * @param column the new column
	 */
	public void setColumn(int column) {
		if (column == this.propertyIndex) {
			// Same column as last sort; toggle the direction
			direction = 1 - direction;
		}
		else {
			// New column; do an ascending sort
			this.propertyIndex = column;
			direction = DESCENDING;
		}
		if (columnTypes != null && column >= columnTypes.length) {
			throw new IllegalArgumentException(TXMUIMessages.columnIndexIsTooBig);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ViewerComparator#compare(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	@SuppressWarnings("unchecked") //$NON-NLS-1$
	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		int rc = 0;
		if (propertyIndex == -1) {
			return rc;
		}

		java.util.ArrayList<Object> a1 = (java.util.ArrayList<Object>) e1;
		java.util.ArrayList<Object> a2 = (java.util.ArrayList<Object>) e2;

		Object o1 = a1.get(this.propertyIndex);
		Object o2 = a2.get(this.propertyIndex);

		if (o1 instanceof String && o2 instanceof String) {
			rc = collator.compare(o1, o2);
		}
		else if (o1 instanceof Comparable) {
			rc = ((Comparable) o1).compareTo(o2);
		}
		if (direction == DESCENDING) {
			rc = -rc;
		}
		return rc;
	}

	/**
	 * Sets the locale.
	 *
	 * @param locale the new locale
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
		this.collator = Collator.getInstance(locale);
		this.collator.setStrength(Collator.TERTIARY);
	}
}
