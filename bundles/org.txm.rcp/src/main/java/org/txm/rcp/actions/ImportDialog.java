// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.actions;

import java.io.File;
import java.util.Locale;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.ExpandBar;
import org.eclipse.swt.widgets.ExpandItem;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.txm.Toolbox;
import org.txm.rcp.handlers.files.EditFile;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.swt.dialog.TXMMessageBox;

// TODO: Auto-generated Javadoc
/**
 * Allow the user to choose the main language, the main encoding of a an
 * imported corpus @ author mdecorde.
 */
public class ImportDialog extends Dialog {

	static final String ID = "org.txm.rcp.actions.ImportDialog"; //$NON-NLS-1$

	/** The parent. */
	Composite parent;

	/** The main panel. */
	Composite mainPanel;

	/** The btn default lang. */
	Button btnDefaultLang;

	/** The btn guess lang. */
	Button btnGuessLang;

	/** The btn guess2 lang. */
	Button btnGuess2Lang;

	/** The btn select lang. */
	Button btnSelectLang;

	/** The langcombo. */
	Combo langcombo;

	//Combo modelCombo;
	/** The btn default encoding. */
	Button btnDefaultEncoding;

	/** The btn guess encoding. */
	Button btnGuessEncoding;

	/** The btn guess2 encoding. */
	Button btnGuess2Encoding;

	/** The btn select encoding. */
	Button btnSelectEncoding;

	/** The encoding combo. */
	Combo encodingCombo;

	/** The dir value. */
	Text dirValue;

	/** The lastopenedbase. */
	static File lastopenedbase;

	/** The directory. */
	File directory;

	/** The paramtokenizer. */
	File paramtokenizer;

	/** The lang. */
	String lang;

	/** The encoding. */
	String encoding;

	/** The model. */
	String model;

	/** The loadername. */
	String loadername = ""; //$NON-NLS-1$

	/** The expand_state. */
	boolean expand_state = false;

	/**
	 * Instantiates a new import dialog.
	 *
	 * @param parentShell the parent shell
	 * @param loadername the loadername
	 */
	public ImportDialog(Shell parentShell, String loadername) {
		super(parentShell);
		this.setShellStyle(this.getShellStyle() | SWT.RESIZE);
		this.loadername = loadername;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(NLS.bind(TXMUIMessages.importSetupP0, loadername));
		newShell.setMinimumSize(300, 200);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(final Composite p) {
		try {
			this.parent = p;

			// System.out.println(parent.getLayout());
			mainPanel = new Composite(parent, SWT.NONE);
			mainPanel.setLayout(new GridLayout(3, false));
			GridData gridData = new GridData(GridData.FILL, GridData.FILL,
					true, false);
			mainPanel.setLayoutData(gridData);

			// directory
			Label dirLabel = new Label(mainPanel, SWT.NONE);
			dirLabel.setText(TXMUIMessages.sourceDirectoryColon);
			dirLabel.setAlignment(SWT.CENTER);
			dirLabel.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));

			dirValue = new Text(mainPanel, SWT.SINGLE | SWT.BORDER);
			dirValue.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
			if (lastopenedbase != null)
				dirValue.setText(lastopenedbase.getPath());

			Button dirButton = new Button(mainPanel, SWT.PUSH);
			dirButton.setLayoutData(new GridData(GridData.FILL, GridData.FILL,
					false, false));
			dirButton.setText("..."); //$NON-NLS-1$
			dirButton.setToolTipText("Select the corpus sources directory"); //$NON-NLS-1$
			dirButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					DirectoryDialog dialog = new DirectoryDialog(parent
							.getShell(), SWT.OPEN);
					if (lastopenedbase != null) {
						dialog.setFilterPath(dirValue.getText());
					}
					else
						dialog.setFilterPath(new File(Toolbox.getInstallDirectory(), "corpora").getAbsolutePath()); //$NON-NLS-1$
					if (dialog.open() != null) {
						String path = dialog.getFilterPath();

						File srcdir = new File(path);
						if (new File(srcdir, "html").exists() && new File(srcdir, "data").exists() && new File(srcdir, "registry").exists()) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ 
						{
							TXMMessageBox.show(parent
									.getShell(), TXMUIMessages.warning, TXMUIMessages.thisDirectorySeemsToContainABinaryCorpus, SWT.ICON_WARNING);
						}

						directory = new File(path);
						lastopenedbase = new File(path);
						dirValue.setText(path);

					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			// optionnel
			final ExpandBar bar = new ExpandBar(mainPanel, SWT.V_SCROLL);
			bar.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true,
					false, 3, 1));



			Composite optionsPanel = new Composite(bar, SWT.NONE);
			optionsPanel.setLayout(new GridLayout(3, false));

			/*
			 * Label oplabel = new Label(mainPanel, SWT.NONE);
			 * oplabel.setText(Messages.ImportDialog_3);
			 * Font f = oplabel.getFont(); FontData defaultFont =
			 * f.getFontData()[0]; defaultFont.setStyle(SWT.BOLD); Font newf =
			 * new Font(Display.getCurrent(),defaultFont);
			 * oplabel.setFont(newf);
			 * oplabel.setAlignment(SWT.CENTER); oplabel.setLayoutData(new
			 * GridData(GridData.FILL, GridData.FILL, true, false,3,1));
			 */

			// encoding
			Group encodingGroup = new Group(optionsPanel, SWT.SHADOW_ETCHED_IN);
			encodingGroup.setText(TXMUIMessages.sourcesCharactersEncodingColon);
			encodingGroup.setLayout(new GridLayout(2, false));
			encodingGroup.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 3, 1));

			btnDefaultEncoding = new Button(encodingGroup, SWT.RADIO);
			btnDefaultEncoding.setText(TXMUIMessages.thisComputerEncodingColon
					+ System.getProperty("file.encoding")); //$NON-NLS-1$
			btnDefaultEncoding.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1));
			btnDefaultEncoding.setSelection(true);

			btnGuessEncoding = new Button(encodingGroup, SWT.RADIO);
			btnGuessEncoding.setText(TXMUIMessages.guessColon);

			btnGuess2Encoding = new Button(encodingGroup, SWT.PUSH);
			btnGuess2Encoding.setText("??"); //$NON-NLS-1$

			btnSelectEncoding = new Button(encodingGroup, SWT.RADIO);
			btnSelectEncoding.setText(TXMUIMessages.selectColon);

			encodingCombo = new Combo(encodingGroup, SWT.BORDER);
			encodingCombo.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 1, 1));

			//fill combo and select default encoding
			int index = 0;
			int i = 0;
			for (String encoding : java.nio.charset.Charset.availableCharsets().keySet()) {
				encodingCombo.add(encoding);
				if (encoding.equals(System.getProperty("file.encoding"))) //$NON-NLS-1$
					index = i;
				i++;
			}
			encodingCombo.select(index);

			encodingCombo.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					btnSelectEncoding.setSelection(true);
					btnGuessEncoding.setSelection(false);
					btnDefaultEncoding.setSelection(false);
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			// lang
			Group langGroup = new Group(optionsPanel, SWT.SHADOW_ETCHED_IN);
			langGroup.setText(TXMUIMessages.mainLanguageColon);
			langGroup.setLayout(new GridLayout(2, false));
			langGroup.setLayoutData(new GridData(GridData.FILL, GridData.FILL,
					true, false, 3, 1));

			btnDefaultLang = new Button(langGroup, SWT.RADIO);
			btnDefaultLang.setText(TXMUIMessages.thisComputerLanguageColon
					+ Locale.getDefault().getLanguage());
			btnDefaultLang.setLayoutData(new GridData(GridData.FILL,
					GridData.FILL, true, false, 2, 1));
			btnDefaultLang.setSelection(true);

			btnGuessLang = new Button(langGroup, SWT.RADIO);
			btnGuessLang.setText(TXMUIMessages.guessColon);

			btnGuess2Lang = new Button(langGroup, SWT.PUSH);
			btnGuess2Lang.setText("??"); //$NON-NLS-1$

			btnSelectLang = new Button(langGroup, SWT.RADIO);
			btnSelectLang.setText(TXMUIMessages.selectColon);

			langcombo = new Combo(langGroup, SWT.BORDER);
			langcombo.setLayoutData(new GridData(GridData.FILL, GridData.FILL,
					true, false, 1, 1));

			//fill combo and select default language
			index = 0;
			i = 0;
			for (String lang : Locale.getISOLanguages()) {
				langcombo.add(lang);
				if (lang.equals(Locale.getDefault().getLanguage()))
					index = i;
				i++;
			}
			langcombo.select(index);

			//if widget selected change the radio buttons value
			langcombo.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					btnSelectLang.setSelection(true);
					btnGuessLang.setSelection(false);
					btnDefaultLang.setSelection(false);
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			// tokenizer options
			Group selectTokenizerPanel = new Group(optionsPanel,
					SWT.SHADOW_ETCHED_IN);
			selectTokenizerPanel.setLayout(new GridLayout(4, false));
			selectTokenizerPanel.setLayoutData(new GridData(GridData.FILL,
					GridData.FILL, true, false, 3, 1));
			selectTokenizerPanel.setText(TXMUIMessages.tokenizerParameterFileColon);

			final Text tokenizerValue = new Text(selectTokenizerPanel, SWT.SINGLE | SWT.BORDER);
			tokenizerValue.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));

			Button tokenizerButton = new Button(selectTokenizerPanel, SWT.PUSH);
			// tokenizerButton.setLayoutData(new GridData(GridData.FILL,
			// GridData.FILL, false, false));
			tokenizerButton.setText("..."); //$NON-NLS-1$
			tokenizerButton.setToolTipText("Select the tokenizer"); //$NON-NLS-1$
			tokenizerButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					FileDialog dialog = new FileDialog(parent.getShell(), SWT.OPEN);
					if (LastOpened.getFolder(ID) != null)
						lastopenedbase = new File(LastOpened.getFolder(ID));
					if (lastopenedbase != null) {
						dialog.setFilterPath(dirValue.getText());
					}
					else
						dialog.setFilterPath(new File(Toolbox.getInstallDirectory(), "corpora").getAbsolutePath()); //$NON-NLS-1$
					if (dialog.open() != null) {
						paramtokenizer = new File(dialog.getFilterPath(),
								dialog.getFileName());
						tokenizerValue.setText(paramtokenizer.getAbsolutePath());
						LastOpened.set(ID, dialog.getFilterPath(), dialog.getFileName());
					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			tokenizerButton = new Button(selectTokenizerPanel, SWT.PUSH);
			// tokenizerButton.setLayoutData(new GridData(GridData.FILL,
			// GridData.FILL, false, false));
			tokenizerButton.setText(TXMUIMessages.edit);
			tokenizerButton.setToolTipText("Editor the tokenizer parameter file"); //$NON-NLS-1$
			tokenizerButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					if (paramtokenizer != null)
						if (paramtokenizer.exists())
							if (paramtokenizer.isFile()) {
								EditFile.openfile(paramtokenizer
										.getAbsolutePath());
							}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			final ExpandItem item0 = new ExpandItem(bar, SWT.NONE, 0);
			item0.setText(TXMUIMessages.options);
			item0.setHeight(optionsPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
			item0.setControl(optionsPanel);

		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return p;

	}

	/**
	 * Gets the directory.
	 *
	 * @return the directory
	 */
	public File getDirectory() {
		return directory;
	}

	/**
	 * Gets the language.
	 *
	 * @return the language
	 */
	public String getLanguage() {
		return lang;
	}

	/**
	 * Gets the encoding.
	 *
	 * @return the encoding
	 */
	public String getEncoding() {
		return encoding;
	}

	/**
	 * Gets the model.
	 *
	 * @return the model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * Gets the tokenizer param file.
	 *
	 * @return the tokenizer param file
	 */
	public File getTokenizerParamFile() {
		return paramtokenizer;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {

		directory = new File(dirValue.getText());

		//LANGUAGE
		if (btnDefaultLang.getSelection())// default lang
			lang = Locale.getDefault().getLanguage();
		else if (btnGuessLang.getSelection()) {
			if (!btnGuess2Lang.getText().equals("??")) //$NON-NLS-1$
				lang = btnGuess2Lang.getText();
			else
				lang = Locale.getDefault().getLanguage();
		}
		else
			lang = langcombo.getText();

		//ENCODING

		if (btnDefaultEncoding.getSelection())// default lang
			encoding = System.getProperty("file.encoding"); //$NON-NLS-1$
		else if (btnGuessEncoding.getSelection()) {
			if (!btnGuess2Encoding.getText().equals("??")) //$NON-NLS-1$
				encoding = btnGuess2Encoding.getText();
			else
				encoding = System.getProperty("file.encoding"); //$NON-NLS-1$
		}
		else
			encoding = encodingCombo.getText();

		System.out.println("Encoding : " + encoding); //$NON-NLS-1$

		if (lang.length() == 0)
			lang = Locale.getDefault().getLanguage();

		if (encoding.length() == 0)
			encoding = System.getProperty("file.encoding"); //$NON-NLS-1$

		model = lang;

		super.okPressed();
	}

}
