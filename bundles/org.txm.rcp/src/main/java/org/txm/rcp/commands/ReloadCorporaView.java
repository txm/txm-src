package org.txm.rcp.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.rcp.views.corpora.CorporaView;

public class ReloadCorporaView extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IWorkbenchPart part = HandlerUtil.getActivePart(event);
		if (part instanceof CorporaView) {
			CorporaView view = (CorporaView) part;
			view.getTreeViewer().refresh();
		}
		return null;
	}
}
