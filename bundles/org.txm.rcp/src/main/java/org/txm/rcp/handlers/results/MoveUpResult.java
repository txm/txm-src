// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.results;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.results.TXMResult;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.utils.logger.Log;

/**
 * Clone the selected TXM result node.
 * 
 * @author sjacquot
 * 
 */
public class MoveUpResult extends BaseAbstractHandler {



	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		//System.out.println("CloneTXMResult.execute(): clone tests.");
		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		//System.out.println("CloneTXMResult.execute() selection class: " + selection);

		if (!(selection.getFirstElement() instanceof TXMResult)) {
			return null;
		}

		TXMResult srcNode = ((TXMResult) selection.getFirstElement());
		TXMResult parent = srcNode.getParent();
		TXMResult newParent = moveUp(srcNode);
		if (newParent != null) {
			Log.info(srcNode.getSimpleName() +" moved from "+parent.getSimpleName()+" to "+newParent.getSimpleName());
			
			CorporaView.refreshObject(newParent);
		} else {
			Log.warning("No suitable new parent found. Aborting");
		}
		return newParent;
	}
	
	public static TXMResult moveUp(TXMResult srcNode) {

		if (srcNode == null) {
			Log.fine(TXMUIMessages.nothingToPaste);
			return null;
		}

		TXMResult currentParent = srcNode.getParent();
		
		if (currentParent == null) {
			Log.fine("Error: result parent is null"); //$NON-NLS-1$
			return null;
		}

		return srcNode.moveUp();
	}
}
