// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.widget.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;

/**
 * The Class ComplexSubcorpusPanel.
 */
public class ComplexSubcorpusPanel extends Composite implements SubcorpusParameterGenerator {

	/** The corpus. */
	CQPCorpus corpus;

	/** The query. */
	String query;

	/** The btn area. */
	Composite parent, partArea, firstArea, dispoArea, btnArea;

	/** The structuralunits. */
	List<StructuralUnit> structuralunits;

	/** The structural units properties. */
	HashMap<StructuralUnit, List<StructuralUnitProperty>> structuralUnitsProperties = new HashMap<>();

	/** The query items. */
	ArrayList<QueryItem> queryItems = new ArrayList<>();

	/** The queries panel. */
	Composite queriesPanel;

	/** The query field. */
	Text queryField;

	/** The and btn. */
	Button andBtn;

	/**
	 * Instantiates a new complex subcorpus panel.
	 *
	 * @param parent the parent
	 * @param style the style
	 * @param corpus the corpus
	 */
	public ComplexSubcorpusPanel(Composite parent, int style, CQPCorpus corpus) {
		super(parent, style);
		this.setLayout(new GridLayout(3, false));
		this.corpus = corpus;

		try {
			this.structuralunits = new ArrayList<>(corpus.getOrderedStructuralUnits());
			for (int i = 0; i < this.structuralunits.size(); i++) {
				if (this.structuralunits.get(i).getName().equals("txmcorpus")) { //$NON-NLS-1$
					this.structuralunits.remove(i);
					i--;
				}
			}
		}
		catch (CqiClientException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}

		for (StructuralUnit su : this.structuralunits) {
			List<StructuralUnitProperty> properties = new ArrayList<>(su.getOrderedProperties());
			if (su.getName().equals("text")) { //$NON-NLS-1$
				for (int i = 0; i < properties.size(); i++) {
					if (properties.get(i).getName().equals("base") || //$NON-NLS-1$
							properties.get(i).getName().equals("project")) { //$NON-NLS-1$
						properties.remove(i);
						i--;
					}
				}
			}
			structuralUnitsProperties.put(su, properties);
		}

		Label firstStep = new Label(this, SWT.NONE);
		firstStep.setText(TXMUIMessages.oneChooseCriteria);
		firstStep = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		firstStep.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1));

		queriesPanel = new Composite(this, SWT.NONE);
		queriesPanel.setLayout(new GridLayout(1, false));
		queriesPanel.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 3, 1));
		QueryItem item = new QueryItem(queriesPanel, SWT.NONE);
		item.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		queryItems.add(item);
		
		Group andorGroup = new Group(this, SWT.SHADOW_ETCHED_IN);
		andorGroup.setText(TXMUIMessages.combination);
		andorGroup.setLayout(new GridLayout(2, false));
		andorGroup.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 3, 1));

		andBtn = new Button(andorGroup, SWT.RADIO);
		andBtn.setText(TXMUIMessages.allCriteria);
		andBtn.setToolTipText(TXMUIMessages.allCriteriaMustMatch);
		andBtn.setSelection(true);

		Button orBtn = new Button(andorGroup, SWT.RADIO);
		orBtn.setText(TXMUIMessages.someCriteria);
		orBtn.setToolTipText(TXMUIMessages.onlyOneCriterionIsEnoughToMatch);
		orBtn.setSelection(false);

		Label secondStep = new Label(this, SWT.NONE);
		secondStep.setText(TXMUIMessages.twoGenerateTheQuery);
		secondStep = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		secondStep.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1));

		GLComposite line = new GLComposite(this, SWT.NONE);
		line.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1));
		line.getLayout().numColumns = 3;
		line.getLayout().horizontalSpacing = 3;
		
		Button refreshBtn = new Button(line, SWT.PUSH);
		refreshBtn.setImage(IImageKeys.getImage(IImageKeys.REFRESH));
		refreshBtn.setToolTipText(TXMUIMessages.refreshTheQueryUsingParameters);
		refreshBtn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				updateQueryField();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});
		
		Label l = new Label(line, SWT.NONE);
		l.setText(TXMUIMessages.queryDblDot);

		queryField = new Text(this, SWT.BORDER|SWT.MULTI | SWT.WRAP);
		queryField.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 2, 1));
		

		// first init
		updateQueryField();
	}

	public void updateQueryField() {
		queryField.setText(getQueryString());
		queryField.update();
	}

	/**
	 * Update display.
	 */
	public void updateDisplay() {
		firstArea.layout();
		dispoArea.layout();
		partArea.layout();
		btnArea.layout();
		parent.layout();
		parent.getParent().layout();
	}

	/**
	 * Gets the query string.
	 *
	 * @return the query string
	 */
	public String getQueryString() {

		LinkedHashSet<String> parts = new LinkedHashSet<>(); // to avoid duplicates 
		for (QueryItem item : queryItems) {

			String query = ""; //$NON-NLS-1$

			String prop = item.selectedSup.getFullName();
			IStructuredSelection sel = item.valueCombo.getStructuredSelection();
			String value = StringUtils.join(sel.toList(), "|"); //$NON-NLS-1$

			int test = item.testCombo.getSelectionIndex();

			if (test == 0) // is
				query = " _." + prop + "=\"" + value + "\""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			else if (test == 1) // is not
				query = " _." + prop + "!=\"" + value + "\""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			else if (test == 2) // contains
				query = " _." + prop + "=\".*" + value + ".*\""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			else // does not contain
				query = " _." + prop + "!=\".*" + value + ".*\""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

			parts.add(query);
		}

		String sep = " | "; //$NON-NLS-1$
		if (andBtn.getSelection()) {
			sep = " & "; //$NON-NLS-1$
		}

		return "[" + StringUtils.join(parts, sep) + "] expand to " + queryItems.get(queryItems.size() - 1).structCombo.getText(); //$NON-NLS-1$ //$NON-NLS-2$

	}

	public String getQueryFieldValue() {
		String query = this.queryField.getText();
		if (query == null || query.length() == 0) {
			query = getQueryString();
		}
		return query;
	}

	public String getGeneratedName() {

		LinkedHashSet<String> parts = new LinkedHashSet<>();

		for (QueryItem queryItem : queryItems) {

			String testString = "="; //$NON-NLS-1$
			int test = queryItem.testCombo.getSelectionIndex();
			if (test == 0) // is
				testString = "="; //$NON-NLS-1$
			else if (test == 1) // is not
				testString = "!="; //$NON-NLS-1$
			else if (test == 2) // contains
				testString = " contains "; //$NON-NLS-1$
			else // does not contain
				testString = " does not contains"; //$NON-NLS-1$

			String generated = queryItem.selectedSup.asFullNameString() + testString + queryItem.valueCombo.getList().getSelection()[0];
			parts.add(generated);
		}

		String sep = " | "; //$NON-NLS-1$
		if (andBtn.getSelection())
			sep = " & "; //$NON-NLS-1$

		return StringUtils.join(parts, sep);
	}

	/**
	 * The Class QueryItem.
	 */
	class QueryItem extends Composite {

		/** The struct combo. */
		Combo structCombo;

		/** The prop combo. */
		Combo propCombo;

		/** The test combo. */
		Combo testCombo;

		/** The value combo. */
		ListViewer valueCombo;

		/** The sups. */
		List<StructuralUnitProperty> sups;

		/** The selected sup. */
		StructuralUnitProperty selectedSup;

		/** The selected su. */
		StructuralUnit selectedSu;

		/**
		 * Instantiates a new query item.
		 *
		 * @param parent the parent
		 * @param style the style
		 */
		public QueryItem(Composite parent, int style) {
			super(parent, style);

			this.setLayout(new GridLayout(8, false));

			// structure choice
			Label l = new Label(this, SWT.None);
			l.setText(TXMCoreMessages.common_structure);

			structCombo = new Combo(this, SWT.READ_ONLY);
			structCombo.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					for (QueryItem item : queryItems)
						item.refreshProps();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			int i_text = 0, i = 0;
			for (StructuralUnit su : structuralunits) {
				structCombo.add(su.getName());
				if (su.getName().equals("text")) i_text = i; //$NON-NLS-1$
				i++;
			}

			if (structuralunits.size() > 0) {
				structCombo.select(i_text);
			}
			
			l = new Label(this, SWT.None);
			l.setText("@"); //$NON-NLS-1$

			propCombo = new Combo(this, SWT.READ_ONLY);
			propCombo.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
			propCombo.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					refreshValues();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			testCombo = new Combo(this, SWT.READ_ONLY);
			testCombo.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
			testCombo.add(TXMUIMessages.is);
			testCombo.add(TXMUIMessages.isNot);
			testCombo.add(TXMUIMessages.contains);
			testCombo.add(TXMUIMessages.doesNotContain);
			testCombo.select(0);

			valueCombo = new ListViewer(this, SWT.MULTI | SWT.V_SCROLL | SWT.BORDER | SWT.VIRTUAL);
			valueCombo.setContentProvider(new ArrayContentProvider());
			GridData data = new GridData(GridData.FILL, GridData.FILL, true, false);
			data.heightHint = (int) (testCombo.getItemHeight() * 3);
			valueCombo.getList().setLayoutData(data);
			if (structuralunits.size() > 0) {
				refreshProps();
			}

			Button btnAdd = new Button(this, SWT.PUSH);
			btnAdd.setText("+"); //$NON-NLS-1$
			btnAdd.setToolTipText(TXMUIMessages.addANewCriterionAfterThisOne);
			btnAdd.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					// System.out.println("add constraint");
					QueryItem item = new QueryItem(queriesPanel, SWT.NONE);
					item.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
					queryItems.add(item);
					ComplexSubcorpusPanel.this.layout();
					// System.out.println(getQueryString());
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			Button btnRmv = new Button(this, SWT.PUSH);
			btnRmv.setText("-"); //$NON-NLS-1$
			btnRmv.setToolTipText(TXMUIMessages.removeThisCriterion);
			btnRmv.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					if (queryItems.size() > 1) {
						QueryItem.this.dispose();
						queryItems.remove(QueryItem.this);
						ComplexSubcorpusPanel.this.layout();
					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		}

		/**
		 * Refresh props.
		 */
		private void refreshProps() {
			propCombo.removeAll();
			selectedSu = structuralunits.get(structCombo.getSelectionIndex());
			sups = structuralUnitsProperties.get(selectedSu);

			for (StructuralUnitProperty sup : sups)
				propCombo.add(sup.getName());

			if (sups.size() > 0) {
				selectedSup = sups.get(0);
				propCombo.select(0);
				refreshValues();
			}
		}

		/**
		 * Refresh values.
		 */
		private void refreshValues() {

			selectedSup = sups.get(propCombo.getSelectionIndex());
			try {
				valueCombo.setInput(selectedSup.getOrderedValues());
			}
			catch (CqiClientException e1) {
				org.txm.utils.logger.Log.printStackTrace(e1);
			}
			if (valueCombo.getList().getItemCount() > 0)
				valueCombo.getList().select(0);
		}

		/**
		 * Gets the structural unit.
		 *
		 * @return the structural unit
		 */
		public StructuralUnit getStructuralUnit() {
			return null;
		}
	}
}
