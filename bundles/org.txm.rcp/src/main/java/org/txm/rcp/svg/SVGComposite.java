// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.svg;

import java.awt.Frame;
import java.net.URL;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.widgets.Composite;
import org.txm.utils.logger.Log;


// TODO: Auto-generated Javadoc
/**
 * The Class SVGComposite.
 */
//FIXME: SJ: this class should be linked to /org.txm.chartsengine.svgbatik.rcp/src/org/txm/chartsengine/svgbatik/rcp/swt/SVGComposite.java (inheritance of the charts engine class from this one)
@Deprecated
public class SVGComposite extends Composite {

	static {
		System.setProperty("sun.awt.noerasebackground", "true"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/** The old rect. */
	private org.eclipse.swt.graphics.Rectangle oldRect = null;

	/** The panel. */
	SVGPanel panel;

	/**
	 * Instantiates a new sVG composite.
	 *
	 * @param parent the parent
	 * @param style the style
	 */
	public SVGComposite(Composite parent, int style) {
		super(parent, style);
		/*
		 * org.eclipse.swt.graphics.Rectangle newRect = this.getClientArea();
		 * if (oldRect != null) {
		 * int heightDelta = newRect.height - oldRect.height;
		 * int widthDelta = newRect.width - oldRect.width;
		 * if ((heightDelta > 0) || (widthDelta > 0)) {
		 * GC gc = new GC(this);
		 * try {
		 * gc.fillRectangle(newRect.x, oldRect.height, newRect.width, heightDelta);
		 * gc.fillRectangle(oldRect.width, newRect.y, widthDelta, newRect.height);
		 * } finally {
		 * gc.dispose();
		 * }
		 * }
		 * }
		 * oldRect = newRect;
		 */


		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		catch (InstantiationException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}

		Frame frame = SWT_AWT.new_Frame(this);
		try {
			panel = new SVGPanel(frame);
		}
		catch (Throwable exception) {
			Log.printStackTrace(exception);
		}
		frame.add(panel);
		frame.setVisible(true);

	}

	/**
	 * Reset zoom.
	 */
	public void resetZoom() {
		panel.resetZoom();
	}

	/**
	 * Sets the SVG file.
	 *
	 * @param url the new sVG file
	 */
	public void setSVGFile(URL url) {
		//System.out.println("LOAD SVG FROM URL: "+url);
		panel.loadSVGDocument(url.toExternalForm());

	}

}
