// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.files;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.views.fileexplorer.Explorer;
import org.txm.rcp.views.fileexplorer.MacroExplorer;

// TODO: Auto-generated Javadoc
/**
 * Execute a groovy script file in the same thread a the RCP @ author mdecorde.
 */
public class RenameFile extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			Object o = ((IStructuredSelection) selection).getFirstElement();

			if (o instanceof File) {
				File file = (File) o;
				rename(file);
			}
		}
		return null;
	}

	public static void rename(File file) {
		String newname = file.getName();
		InputDialog dlg = new InputDialog(Display.getCurrent().getActiveShell(),
				TXMUIMessages.renameFile, TXMUIMessages.renameFile + newname, newname, null);
		if (dlg.open() == Window.OK) {
			newname = dlg.getValue();
			System.out.println(TXMUIMessages.renamingByColon + newname);
			if (newname.equals(file.getName())) return; // nothing to do
			if (newname == null || newname.trim().length() == 0) return; // error

			File nfile = new File(file.getParentFile(), newname);
			if (!file.renameTo(nfile))
				System.out.println(TXMUIMessages.failedToRename + file + TXMUIMessages.to + newname);
			Explorer.refresh(nfile);
			MacroExplorer.refresh(nfile);
		}
	}
}
