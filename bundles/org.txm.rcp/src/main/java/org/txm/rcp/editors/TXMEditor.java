/**
 * 
 */
package org.txm.rcp.editors;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.SafeRunner;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.menus.IMenuService;
import org.eclipse.ui.part.EditorPart;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.BeanParameters;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.rcp.JobsTimer;
import org.txm.rcp.StatusLine;
import org.txm.rcp.TXMWindows;
import org.txm.rcp.editors.listeners.BaseAbstractComputeListener;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.RCPPreferences;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.HashMapWidget;
import org.txm.rcp.swt.dialog.ComputeProgressMonitorDialog;
import org.txm.rcp.swt.widget.AssistedChoiceQueryWidget;
import org.txm.rcp.swt.widget.AssistedQueryWidget;
import org.txm.rcp.swt.widget.BooleanCombo;
import org.txm.rcp.swt.widget.FloatSpinner;
import org.txm.rcp.swt.widget.LargeQueryField;
import org.txm.rcp.swt.widget.ListText;
import org.txm.rcp.swt.widget.NewNavigationWidget;
import org.txm.rcp.swt.widget.PropertiesSelector;
import org.txm.rcp.swt.widget.QueryWidget;
import org.txm.rcp.swt.widget.RadioGroup;
import org.txm.rcp.swt.widget.ReferencePatternSelector;
import org.txm.rcp.swt.widget.TXMParameterControl;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.rcp.views.debug.TXMResultDebugView;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.cqp.ReferencePattern;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.logger.Log;

/**
 * Base TXM result <code>EditorPart</code> composed of
 * - a main toolbar at the top
 * - display area
 * - a bottom toolbar at the bottom of the editor
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public abstract class TXMEditor<T extends TXMResult> extends EditorPart implements ITXMResultEditor<TXMResult> {

	/**
	 * ID to use in plugin.xml to contribute to the top toolbar.
	 */
	public final static String TOP_TOOLBAR_ID = "TXMEditorTopToolBar"; //$NON-NLS-1$

	public final static String BOTTOM_TOOLBAR_ID = "TXMEditorBottomToolBar"; //$NON-NLS-1$

	// FIXME: SJ: bad idea to use the ID as title, need to change the system
	public final static String COMPUTING_PARAMETERS_GROUP_ID = TXMUIMessages.parameters;

	/**
	 * ID defined in plugin.xml for the compute button contribution.
	 */
	public final static String TOP_TOOLBAR_COMPUTE_BUTTON_ID = "compute"; //$NON-NLS-1$

	/**
	 * ID defined in plugin.xml for the compute button contribution.
	 */
	public final static String TOP_TOOLBAR_EXPORT_BUTTON_ID = "export"; //$NON-NLS-1$

	/**
	 * ID defined in plugin.xml for the compute button contribution.
	 */
	public final static String TOP_TOOLBAR_REVERT_BUTTON_ID = "revert"; //$NON-NLS-1$


	/**
	 * The editor main tool bar, positioned at the top of the editor.
	 */
	protected TXMEditorToolBar topToolBar;

	/**
	 * The main panel containing the displayed result
	 */
	private GLComposite resultArea;

	/**
	 * The editor bottom tool bar, positioned at the bottom of the editor.
	 */
	protected TXMEditorToolBar bottomToolBar;

	/**
	 * The command main parameters area that is always visible in the top tool bar.
	 * The composite displayed before the top tool bar, containing the minimal parameters widgets and that are always visible.
	 */
	protected GLComposite mainParametersComposite;

	/**
	 * The extended parameters area dedicated to receive the extended parameters of the command.
	 */
	protected GLComposite extendedParametersComposite;

	/**
	 * Command extended parameters group that can be hidden.
	 */
	protected Group extendedParametersGroup;

	/**
	 * The SWT parent Composite.
	 */
	private Composite parent; // children must not alter this

	/**
	 * contains the default bottom toolbars widget
	 */
	protected Composite bottomToolBarContainer;

	/**
	 * Computing tool item button.
	 */
	protected ToolItem computeButton;

	/**
	 * Revert tool item button.
	 */
	protected ToolItem revertButton;

	/**
	 * To keep track of the parameters used for the last computing and updates only the widgets if a parameter has changed.
	 */
	// FIXME: SJ: useless?
	// protected HashMap<String, Object> lastComputingParameters = new HashMap<String, Object>();

	/**
	 * contains bottom toolbar subwidgets (installed grouped, etc.)
	 */
	private GLComposite bottomSubWidgetsComposite;

	private GLComposite firstLineComposite;

	private boolean createPartControlDoneSucessfully = true; // FIXME: MD: for now... muahahaha

	/**
	 * installed extensions
	 */
	protected HashSet<TXMEditorExtension<T>> extensions = new HashSet<>();


	/**
	 * Linked editors.
	 */
	protected ArrayList<ITXMResultEditor<TXMResult>> linkedEditors = new ArrayList<ITXMResultEditor<TXMResult>>();

	/**
	 * debug purpose variable -> easier to fetch information that the getResult() method
	 */
	protected T result;

	/**
	 * To store or edit the dirty state of the editor from widget.
	 */
	protected boolean dirty = true;

	private boolean locked = false;


	/**
	 * Computing Job. Stored to restrict the computing to one at time.
	 */
	protected JobHandler computingJob = null;



	ArrayList<TXMEditorExtension<?>> TXMEditorExtensionCache = null;

	private IPartListener2 pl;


	/**
	 * Default constructor.
	 */
	public TXMEditor() {
		super();
	}

	/**
	 * Creates an editor and an editor input containing the specified result.
	 * 
	 * @param result
	 */
	public TXMEditor(TXMResult result) {
		this(new TXMResultEditorInput<>(result));
	}

	/**
	 * Creates an editor with the specified editor input.
	 * 
	 * @param editorInput
	 */
	public TXMEditor(TXMResultEditorInput<TXMResult> editorInput) {
		this.setInput(editorInput); // FIXME: SJ: the input is defined twice, on time here and one time in init(), need to see if we still need the assignment here in the constructor. It may needed for manually opening an editor
	}

	@Override
	public final void init(IEditorSite site, IEditorInput input) throws PartInitException {

		this.setSite(site);
		this.setInput(input);
		this.result = this.getEditorInput().getResult(); // store result for debug purpose
		// this.result.addResultListener(this);
		// FIXME: SJ: see how to use the Adapters and AdapterFactory to define image and title of Editor in the contributing plug-ins
		// another way is to use editors extension in plug-ins and redefine icon and name, using this solution the problem is that the name (which is also editor title) can not be dynamic according to
		// the result data
		// so it seems better to use Adapters if it's possible or keep this solution below
		this.setPartName(input.getName());

		this._init(); // additional initialization code

		this.initExtensions(); // fetch available extensions for this editor

		site.getPage().addPartListener(new IPartListener() {

			@Override
			public void partOpened(IWorkbenchPart part) {
			}

			@Override
			public void partDeactivated(IWorkbenchPart part) {
			}

			@Override
			public void partClosed(IWorkbenchPart part) {

				if (part == TXMEditor.this) {
					TXMEditor.this.getSite().getPage().removePartListener(this);
					onClosed();
				}
			}

			@Override
			public void partBroughtToTop(IWorkbenchPart part) {
			}

			@Override
			public void partActivated(IWorkbenchPart part) {
			}
		});
	}

	/**
	 * To do something after the editor is closed
	 */
	public void onClosed() {

	}

	/**
	 * Put here the additional editor initialization code
	 * 
	 * IEditorSite site, IEditorInput input are already set
	 */
	public void _init() throws PartInitException {
		// optional
	}


	/**
	 * Initializes plug-ins extensions.
	 */
	@SuppressWarnings("rawtypes")
	public void initExtensions() {
		if (TXMEditorExtensionCache == null) { // TODO clear the cache
			TXMEditorExtensionCache = new ArrayList<TXMEditorExtension<?>>();
			IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(TXMEditorExtension.EXTENSION_ID);
			if (config.length > 0) {

				for (IConfigurationElement e : config) {
					try {
						Log.finest("Load TXMEditor extension " + e.getName() + " from " + e.getContributor().getName()); //$NON-NLS-1$ //$NON-NLS-2$
						final Object o = e.createExecutableExtension("class"); //$NON-NLS-1$
						if (o instanceof TXMEditorExtension<?> ext) {
							TXMEditorExtensionCache.add(ext);
						}
					}
					catch (Exception e2) {
						Log.warning("Error: fail to initialize editor extension: " + e2);
					}
				}
			}
		}
		// the extensions cache is ready		
		// Now the extension points are ready

		//SJ: le cash
		for (TXMEditorExtension ext : TXMEditorExtensionCache) {
			try {
				ISafeRunnable runnable = new ISafeRunnable() {

					@Override
					public void handleException(Throwable exception) {

						Log.severe("Error while installing: " + ext + ": " + exception.getLocalizedMessage()); //$NON-NLS-1$ //$NON-NLS-2$
					}

					@SuppressWarnings("unchecked")
					@Override
					public void run() throws Exception {

						TXMResult r = getResult();
						if (r == null) {
							return;
						}

						if (ext.getTXMResultValidClasses() == null || ext.getTXMResultValidClasses().contains(getResult().getClass())) {
							Log.finer("Installing TXMEditorExtension: " + ext.getName()); //$NON-NLS-1$
							ext.setEditor(TXMEditor.this);
							extensions.add(ext);
						}
					}
				};
				SafeRunner.run(runnable);
			}
			catch (Exception ex) {
				Log.severe("Exception while installing " + ext.getName() + ": " + ex.getLocalizedMessage()); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}

		pl = new IPartListener2() {

			// ... Other methods
			@Override
			public void partClosed(IWorkbenchPartReference partRef) {

				// if (getResult() != null) {
				// getResult().removeResultListener(TXMEditor.this);
				// }
				getEditorSite().getPage().removePartListener(pl);
				//				getEditorSite().getPage().addPartListener(pl);

				if (partRef.getId().equals(TXMEditor.this.getClass().getName())) { // called after this.doSave()

					// System.out.println("EVENT partClosed");
					for (TXMEditorExtension<T> extension : extensions) {
						try {
							if (extension.isDirty()) {
								extension.discardChanges();
							}
						}
						catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}

			@Override
			public void partActivated(IWorkbenchPartReference partRef) {

				// FIXME: SJ: why? when opening a new Editor the view will be refreshed twice...
				if (partRef.getId().equals(getEditorSite().getId())) {
					//System.out.println("REFRESH ON PART ACTIVATED "+partRef.getId());
					CorporaView.refresh(); // this will refreshed the view selected element (this editor's result)
				}
			}

			@Override
			public void partBroughtToTop(IWorkbenchPartReference partRef) {
			}

			@Override
			public void partDeactivated(IWorkbenchPartReference partRef) {
			}

			@Override
			public void partOpened(IWorkbenchPartReference partRef) {
			}

			@Override
			public void partHidden(IWorkbenchPartReference partRef) {
			}

			@Override
			public void partVisible(IWorkbenchPartReference partRef) {
			}

			@Override
			public void partInputChanged(IWorkbenchPartReference partRef) {
			}

		};
		this.getEditorSite().getPage().addPartListener(pl);
	}

	public HashSet<TXMEditorExtension<T>> getTXMEditorExtensions() {
		return extensions;
	}

	public HashSet<TXMEditorExtension<T>> getTXMEditorExtensions(Class<? extends TXMEditorExtension<?>> clazz) {

		HashSet<TXMEditorExtension<T>> sel = new HashSet<TXMEditorExtension<T>>();
		for (TXMEditorExtension<T> e : extensions) {
			if (clazz.isAssignableFrom(e.getClass())) {
				sel.add(e);
			}
		}
		return sel;
	}

	/**
	 * the composite containing the main parameters, the top toolbar and eventually other top toolbars
	 * 
	 * @return
	 */
	public GLComposite getFirstLineComposite() {
		return firstLineComposite;
	}


	/**
	 * Gets the shell of the parent composite if exists and not disposed otherwise the Display default active shell.
	 * 
	 * @return
	 */
	public Shell getShell() {
		if (parent != null && !parent.isDisposed()) {
			return parent.getShell();
		}
		return Display.getDefault().getActiveShell();
	}

	/**
	 * Gets the command main parameters area that is always visible in the top tool bar.
	 * @return the composite displayed before the toptoolbar, containing the minimal parameters widgets
	 */
	public GLComposite getMainParametersComposite() {
		return mainParametersComposite;
	}


	/**
	 * Gets the composite dedicated to receive the extended parameters of the command.
	 * 
	 * @return the composite of the extended parameters area
	 */
	public GLComposite getExtendedParametersComposite() {
		return extendedParametersComposite;
	}

	/**
	 * Gets the extended parameters group widget.
	 * Put here you main parameter widgets using RowData data layout.
	 * 
	 * @return the extendedParametersGroup
	 */
	public Group getExtendedParametersGroup() {
		return this.extendedParametersGroup;
	}






	/**
	 * Creates the default toolbar (empty) and default result area (empty).
	 * 
	 */
	@Override
	public final void createPartControl(Composite parent) {

		try {

			this.parent = parent;

			// disable the rendering while creating the widgets
			// FIXME: SJ: would be very nice to do that to avoid to see the empty tables before the refresh() that fill the UI.
			// the re-enabling is done in refresh() but the EditionEditor doesn't seem to call refresh() so it breaks it
			// this.parent.setRedraw(false);

			// to hide and display the extended parameters composite
			this.initParentLayout(parent, 1);

			// contains the minimal panels and top toolbars
			this.firstLineComposite = new GLComposite(parent, SWT.NONE, "Editor first line"); //$NON-NLS-1$
			this.firstLineComposite.getLayout().numColumns = 2;
			this.firstLineComposite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));

			this.mainParametersComposite = new GLComposite(this.firstLineComposite, SWT.NONE, "Main parameters area"); //$NON-NLS-1$
			this.mainParametersComposite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
			this.mainParametersComposite.getLayout().marginLeft = 5;
			this.mainParametersComposite.getLayout().horizontalSpacing = 5;

			this.extendedParametersComposite = new GLComposite(parent, SWT.NONE, "Extended parameters area"); //$NON-NLS-1$
			this.extendedParametersComposite.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));

			this.topToolBar = new TXMEditorToolBar(this, this.firstLineComposite, this.extendedParametersComposite, SWT.FLAT | SWT.RIGHT, TOP_TOOLBAR_ID);

			// store the Compute button to access it later, especially from subclasses
			this.computeButton = this.topToolBar.getItemByContributionId(TOP_TOOLBAR_COMPUTE_BUTTON_ID);

			// this.computeButton = new ToolItem(topToolBar, SWT.PUSH);//.topToolBar.getItemByContributionId(TOP_TOOLBAR_COMPUTE_BUTTON_ID);
			// this.computeButton.setImage(IImageKeys.getImage(IImageKeys.START));
			// this.computeButton.addSelectionListener(new ComputeSelectionListener(this, true));


			// map export commands to the "Export" tool bar button
			ToolItem exportButton = this.topToolBar.getItemByContributionId("txmeditor_exports"); //$NON-NLS-1$
			if (exportButton != null) {
				exportButton.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						
						if (e.detail == SWT.ARROW) {
							// Do nothing
						}
						else {
							//System.out.println("EXPORTS: menu:txmeditor_exports");
							// MD: workaround to call the right export command depending on the editor result 
							try {
								BaseAbstractHandler.executeCommand("ExportChartEditorView"); //$NON-NLS-1$

							}
							catch (Exception e1) {
								BaseAbstractHandler.executeCommand("ExportResultData"); //$NON-NLS-1$
							}
						}
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) { }
				});
			}

			// store the REVERT button to access it later, especially from subclasses
			//this.revertButton = this.topToolBar.getItemByContributionId(TOP_TOOLBAR_REVERT_BUTTON_ID);

			// computing parameters components
			boolean showExtendedParameters = false;
			// only show the area if the result can not compute and is dirty
			// FIXME: to discuss about when hiding or showing the extended parameters
			// if(!this.getResult().canCompute() && this.getResult().isDirty()) {
			// showExtendedParameters = true;
			// }
			this.extendedParametersGroup = (Group) this.topToolBar.installGroup(COMPUTING_PARAMETERS_GROUP_ID, TXMUIMessages.showHideCommandParameters,
					"icons/show_computing_parameters.png",  //$NON-NLS-1$
					"icons/hide_computing_parameters.png",  //$NON-NLS-1$
					showExtendedParameters, true);

			//			this.exportParametersGroup = (Group) this.topToolBar.installGroup("Export", "Export modes",
			//					"platform:/plugin/org.eclipse.ui/icons/full/etool16/export_wiz.png",  //$NON-NLS-1$
			//					"platform:/plugin/org.eclipse.ui/icons/full/etool16/export_wiz.png",  //$NON-NLS-1$
			//					showExtendedParameters, true);
			//			
			//			this.exportToolBar = new TXMEditorToolBar(this, this.exportParametersGroup, this.extendedParametersComposite, SWT.FLAT | SWT.RIGHT, "txmeditor_exports");

			// display main area
			this.resultArea = new GLComposite(parent, SWT.NONE, "Result area"); //$NON-NLS-1$
			this.resultArea.setLayoutData(new GridData(GridData.FILL_BOTH));

			// create the bottom tool bar
			this.bottomToolBarContainer = new Composite(parent, SWT.NONE);
			this.bottomToolBarContainer.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
			RowLayout rl = new RowLayout(SWT.HORIZONTAL);
			rl.marginTop = rl.marginHeight = rl.marginWidth = rl.marginBottom = 0;
			// rl.center = true;
			// rl.justify = true;
			this.bottomToolBarContainer.setLayout(rl);

			this.bottomSubWidgetsComposite = new GLComposite(parent, SWT.NONE, "Bottom parameters area"); //$NON-NLS-1$
			if (parent.getLayout() instanceof GridLayout) {
				this.bottomSubWidgetsComposite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
			}

			this.bottomToolBar = new TXMEditorToolBar(this, this.bottomToolBarContainer, this.bottomSubWidgetsComposite, SWT.FLAT, "bottom"); //$NON-NLS-1$

			this.notifyExtensions("notifyStartOfCreatePartControl"); //$NON-NLS-1$
			this._createPartControl(); // child editor creates its parameters and displays widgets
			this.notifyExtensions("notifyEndOfCreatePartControl"); //$NON-NLS-1$

			this.firstLineComposite.pack();
			this.extendedParametersGroup.pack();


			// remove empty toolbar
			// TODO SJ: Hack? MD: yeah -> need to find a way to not display the bottom toolbar on Windows instead
			if (this.bottomToolBar.getItems().length == 0) {
				this.bottomToolBar.dispose();
			}

			// remove empty toolbar
			// TODO SJ: Hack? MD: yeah -> need to find a way to not display the bottom toolbar on Windows instead
			if (this.topToolBar.getItems().length == 0) {
				this.topToolBar.dispose();
			}

			// uninstall parameters group if not used
			if (this.extendedParametersGroup.getChildren().length == 0 && !this.topToolBar.isDisposed()) {
				this.topToolBar.unInstallGroup(COMPUTING_PARAMETERS_GROUP_ID);
			}

			// uninstall parameters group if BEGINNER MODE ACTIVATED
			if (TBXPreferences.getInstance().getBoolean(TBXPreferences.BEGINNER_USER) && !this.topToolBar.isDisposed()) {
				this.topToolBar.getGroupButton(COMPUTING_PARAMETERS_GROUP_ID).setEnabled(false);
			}

			// FIXME: SJ: this code doesn't work even if we call removeComputeButton()
			// remove empty first line composite
			// if(this.mainParametersComposite.getChildren().length == 0 && this.topToolBar.isDisposed()) {
			// this.firstLineComposite.dispose();
			// }


			if (this.getResult() != null && this.getResult().isLocked()) {
				this.setLocked(true);
			}

			// disable the compute button, it will be re-enable or not later when the UI will check if the editor is dirty or not
			// ToolItem computeButton = this.topToolBar.getItemByContributionId(TOP_TOOLBAR_COMPUTE_BUTTON_ID);
			// if (computeButton != null) {
			// computeButton.setEnabled(false);
			// }


			// dirty state according to the future compute that will be launched by TXMEditor.openEditor()
			if (this.getResult().canCompute()) {
				this.dirty = false;
			}

			this.setDirty(this.dirty);
		}
		catch (Throwable e) {
			Log.severe(TXMCoreMessages.bind("TXMEditor.createPartControl(): can not create the editor for result {0}.", this.getResult())); //$NON-NLS-1$
			Log.printStackTrace(e);
			createPartControlDoneSucessfully = false;
		}
	}

	@Override
	public void setLocked(boolean b) {
		this.locked = b;
		this.firstLineComposite.setEnabled(!b);
	}

	public boolean isLocked() {
		return locked;
	}

	/**
	 * shortcut to call the notify* methods of all extension
	 * 
	 * notify the extension of editors updates
	 * 
	 * notifyStartOfCreatePartControl
	 * notifyEndOfCreatePartControl
	 * notifyStartOfCompute
	 * notifyEndOfCompute
	 * notifyStartOfRefresh
	 * notifyEndOfRefresh
	 * notifyDispose
	 * notifyDoSave
	 * 
	 * @param step the method to call
	 * @throws SecurityException
	 */
	protected void notifyExtensions(final String step) {

		if (!step.startsWith("notify")) return; //$NON-NLS-1$

		ArrayList<TXMEditorExtension<T>> errors = new ArrayList<>();
		for (final TXMEditorExtension<T> ext : extensions) {
			// ISafeRunnable runnable = new ISafeRunnable() {
			// @Override
			// public void handleException(Throwable exception) {
			// System.out.println("Error while installing: "+ext+": "+ exception.getLocalizedMessage());
			// }
			//
			// @Override
			// public void run() throws Exception {
			try {
				java.lang.reflect.Method method;
				method = ext.getClass().getMethod(step);
				if (method != null) {
					method.invoke(ext);
				}
				else {
					ext.notify(step);
				}
			}
			catch (Throwable e) {
				// errors.add(ext);

				Log.warning(TXMCoreMessages.bind(TXMCoreMessages.CantNotifyOfP0TheP1ExtensionP2P3P4, step, ext.getName(), ext.getClass(), e));
				Log.printStackTrace(e);
			}
			// }
			// };
			// SafeRunner.run(runnable);
		}
		extensions.removeAll(errors);
	}

	/**
	 * Subclasses manual creation method called after TXMEditor.createPartControl is called.
	 * 
	 * Use this method to set your own widgets in the minimal parameters area, the top toolbar, parameters group, the result area and the bottom toolbar
	 * 
	 * @throws Exception
	 */
	public abstract void _createPartControl() throws Exception;

	/**
	 * Use this method to add widget next to the default Bottom toolbar
	 * 
	 * @return the composite that contains the default Bottom toolbar.
	 */
	public Composite getBottomToolBarContainer() {
		return bottomToolBarContainer;
	}

	/**
	 * Gets the top toolbar.
	 * 
	 * @return the default Top toolbar of TXMEditors
	 */
	public TXMEditorToolBar getTopToolbar() {
		return topToolBar;
	}

	/**
	 * Gets the result display area.
	 * The user must configure its layout.
	 * 
	 * @return
	 */
	public GLComposite getResultArea() {
		return resultArea;
	}

	/**
	 * Gets the bottom toolbar.
	 * 
	 * @return the default bottom toolbar of editors
	 */
	public TXMEditorToolBar getBottomToolbar() {
		return bottomToolBar;
	}

	/**
	 * Creates the parent layout with some configuration to make children composite visible/invisible.
	 * 
	 * @param colSpan
	 */
	public void initParentLayout(Composite parent, int colSpan) {
		GridLayout gl = GLComposite.createDefaultLayout(1);
		parent.setLayout(gl);
	}


	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void doSave(final IProgressMonitor monitor) {
		// FIXME: SJ: to discuss. A this moment the save is automatically done or not in TXMResult.compute() according to a preference of the TBX
		// if (TBXPreferences.getInstance().getBoolean(TBXPreferences.AUTO_SAVE_RESULTS)) { // ?
		// getResult().saveParameters();
		// }

		/// FIXME: SJ: dirty state tests
		if (parent != null && !parent.isDisposed()) {
			parent.getDisplay().asyncExec(new Runnable() {

				@Override
				public void run() {

					compute(true);
					TXMEditor.this.setDirty(false);
				}
			});
		}

		notifyExtensions("notifyDoSave"); //$NON-NLS-1$
		firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub
	}

	@Override
	public void dispose() {
		notifyExtensions("notifyDispose"); //$NON-NLS-1$
		super.dispose();
	}

	/**
	 * Sets the dirty state of the editor.
	 * Also sets the result dirty state to true if editor is dirty.
	 * Then refreshes the corpora view result node to reflect the dirty state.
	 * 
	 * @param dirty
	 */
	public void setDirty(boolean dirty) {
		this.dirty = dirty;

		// FIXME: SJ: future version that enable the button ONLY if the result can compute
		// need to check all canCompute() result methods before enabling it
		// try {
		// // enable/disable the compute button according to dirty state of the editor
		// if (this.computeButton != null && !this.computeButton.isDisposed()) {
		// this.computeButton.setEnabled(this.getResult().canCompute() && dirty && !this.getResult().isLocked());
		// }
		// }
		// catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		// // FIXME: sometime the button is enable/disable when it should not
		// // enable/disable the compute button according to dirty state of the editor
		// if (this.computeButton != null && !this.computeButton.isDisposed()) {
		// this.computeButton.setEnabled(this.dirty && !this.getResult().isLocked());
		// }

		firePropertyChange(IEditorPart.PROP_DIRTY);
		CorporaView.refreshObject(this.result);
	}

	@Override
	public boolean isDirty() {

		boolean dirty = this.dirty;

		for (TXMEditorExtension<T> extension : this.extensions) {
			try {
				dirty = dirty || extension.isDirty();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return dirty;
	}

	@Override
	public boolean isSaveOnCloseNeeded() {

		boolean needed = false;

		for (TXMEditorExtension<T> extension : this.extensions) {
			try {
				needed = needed || (extension.isSaveOnCloseNeeded() && extension.isDirty());
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return needed;

	}

	@Override
	public final void setFocus() {
		if (this.parent.isDisposed()) return; // avoid SWTException

		this.parent.setFocus();

		this._setFocus();
		// this.resultArea.setFocus();
		// FIXME SJ: this code leads to a bug, the focus must not be regiven to the main parameter after each computing of all result editors
		// if (mainParametersComposite != null && !mainParametersComposite.isDisposed()) {
		// mainParametersComposite.setFocus();
		// }
		CorporaView.reveal(this.getResult());
	}

	/**
	 * Override this method if you need additional focus actions.
	 * this method is called after this.parent.setFocus() and before CorporaView is revealed
	 */
	public void _setFocus() {

	}

	@SuppressWarnings("unchecked")
	@Override
	public TXMResultEditorInput<T> getEditorInput() {
		return (TXMResultEditorInput<T>) super.getEditorInput();
	}

	/**
	 * Returns the result object associated with the editor through its editor input.
	 * 
	 * @return
	 */
	@Override
	public T getResult() {
		return this.getEditorInput().getResult();
	}
	
	public TXMResult getFirstVisibleResult() {
		if (getResult().isVisible()) return getResult();
		
		return getResult().getFirstVisibleParent();
	}

	/**
	 * Gets the value of the specified key in parameters, local result node or default preferences nodes.
	 * 
	 * @param key
	 * @return
	 */
	public int getIntParameterValue(String key) {
		return this.getEditorInput().getResult().getIntParameterValue(key);
	}

	/**
	 * Gets the value of the specified key in parameters, local result node or default preferences nodes.
	 * 
	 * @param key
	 * @return
	 */
	public float getFloatParameterValue(String key) {
		return this.getEditorInput().getResult().getFloatParameterValue(key);
	}

	/**
	 * Gets the value of the specified key in parameters, local result node or default preferences nodes.
	 * 
	 * @param key
	 * @return
	 */
	public double getDoubleParameterValue(String key) {
		return this.getEditorInput().getResult().getDoubleParameterValue(key);
	}

	/**
	 * Gets the value of the specified key in parameters, local result node or default preferences nodes.
	 * 
	 * @param key
	 * @return
	 */
	public boolean getBooleanParameterValue(String key) {
		return this.getEditorInput().getResult().getBooleanParameterValue(key);
	}

	/**
	 * Gets the value of the specified key in parameters, local result node or default preferences nodes.
	 * 
	 * @param key
	 * @return
	 */
	public long getLongParameterValue(String key) {
		return this.getEditorInput().getResult().getLongParameterValue(key);
	}

	/**
	 * Gets the value of the specified key in parameters, local result node or default preferences nodes.
	 * 
	 * @param key
	 * @return
	 */
	public String getStringParameterValue(String key) {
		return this.getEditorInput().getResult().getStringParameterValue(key);
	}

	// FIXME: SJ: tests to enable/disable widget controls while computing
	// public void recursiveSetEnabled(boolean enabled) {
	// this.recursiveSetEnabled(this.parent, enabled);
	// }

	// FIXME: SJ: tests to enable/disable widget controls while computing
	// public void recursiveSetEnabled(Control ctrl, boolean enabled) {
	// if (ctrl instanceof Composite) {
	// Composite comp = (Composite) ctrl;
	// for (Control c : comp.getChildren())
	// recursiveSetEnabled(c, enabled);
	// } else {
	// ctrl.setEnabled(enabled);
	// }
	// }

	String computeChildren = null;

	/**
	 * Computes the TXMResult if all required parameters are set then refreshes the editor UI.
	 * Also loads and saves some parameters from result to Widgets fields and from Widgets fields to result.
	 * 
	 * @param update force the editor to update its result parameters
	 */
	@Override
	public JobHandler compute(final boolean update) {

		// restrict the computing Job to one at time
		if (this.computingJob != null && this.computingJob.getResult() == null) {
			Log.finer(NLS.bind("Editor is already computing {0}. Please wait or abort the computing job ({1}).", this.result, this.computingJob.getName())); //$NON-NLS-1$
			return this.computingJob;
		}

		// TODO uncomment when StatusLine behavior will be decided
		// StatusLine.setMessage(this.getResult().getComputingStartMessage());

		// FIXME: SJ: tests to hide/display the result area while computing
		// this.resultArea.setVisible(false);

		this.computingJob = new JobHandler(this.getResult().getComputingStartMessage()) {

			@Override
			protected IStatus _run(SubMonitor monitor) {

				if (isLocked()) {
					return Status.CANCEL_STATUS;
				}

				try {
					JobsTimer.start();

					boolean[] doComputeChildren = new boolean[] { true };
					computeChildren = RCPPreferences.getInstance().getString(RCPPreferences.COMPUTE_CHILDREN);
					// update the TXMResult data from Editor fields
					this.syncExec(new Runnable() {

						@Override
						public void run() {

							// FIXME: SJ: tests to enable/disable the editor widgets when computing
							// recursiveSetEnabled(false);

							if (update) {

								// popup alert to inform user that the result has been manually edited
								if (TXMEditor.this.getResult().isAltered()
										&& !MessageDialog.openQuestion(getShell(), TXMCoreMessages.common_warning,
												TXMCoreMessages.bind(TXMUIMessages.warning_popup_theP0HasBeenEditedItsChangesWillBeLost, TXMEditor.this.getResult().getResultType()))) {
									setCanceled(true);
								}

								if (TXMEditor.this.getResult().hasVisibleChildNotSynhchronized()) { // maybe do not compute the visible children
									if (RCPPreferences.COMPUTE_CHILDREN_ASK.equals(computeChildren)) {
										MessageDialog dialog = new MessageDialog(getShell(), TXMCoreMessages.common_warning, null, TXMUIMessages.warning_popup_allDescendantResultsWillBeUpdated, 0,
												new String[] { TXMCoreMessages.cancel, TXMCoreMessages.no, TXMCoreMessages.yes }, 0);
										int r = dialog.open();
										if (r == 0) {
											setCanceled(true);
										}
										doComputeChildren[0] = r == 2;

									}
									else if (RCPPreferences.COMPUTE_CHILDREN_ALWAYS.equals(computeChildren)) {
										doComputeChildren[0] = true;
									}
									else { // COMPUTE_CHILDREN_NEVER
										doComputeChildren[0] = false;
									}
								}

								// subclasses manual result updating from editor fields
								Log.finest("TXMEditor.compute(): " + TXMEditor.this.getClass().getSimpleName() + ": manually updating result from editor (call subclass implementation)."); //$NON-NLS-1$ //$NON-NLS-2$
								updateResultFromEditor();

								// auto updating result parameters from parameter annotations of editor
								Log.finest("TXMEditor.compute(): " + TXMEditor.this.getClass().getSimpleName() + ": auto updating result from editor (fill the result parameter from editor field annotation)."); //$NON-NLS-1$ //$NON-NLS-2$
								autoUpdateResultFromEditorParameters();

								// FIXME: SJ: useless at this time?
								// Stores the last parameters before the computing to later auto-update the Widgets only if some parameters have changed
								// setLastComputingParameters(TXMEditor.this.getResult().getLastParametersFromHistory());
							}
							// FIXME: SJ: there is a bug when deleting LT lines for example, the popup alert to inform user that children results will be recomputed is not triggered
							// problem here is the DeleteLines commands, etc. call TXMEditor.compute(false) at this moment, they should call TXMEditor.compute(true) but the LT Editor is not ready to
							// manage that
							// else if(TXMEditor.this.getResult().hasBeenComputedOnce()) {
							// // popup alert to inform user that children results will be recomputed
							// if(!TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)
							// && TXMEditor.this.getResult().hasVisibleChild() && !MessageDialog.openQuestion(getShell(), TXMCoreMessages.common_warning,
							// TXMUIMessages.warning_popup_allDescendantResultsWillBeUpdated)) {
							// setCanceled(true);
							// }
							//
							// }
						}
					});

					if (this.isCanceled()) {
						return Status.CANCEL_STATUS;
					}

					// compute result
					monitor.beginTask(TXMEditor.this.getResult().getComputingStartMessage(), 100);

					notifyExtensions("notifyStartOfCompute"); //$NON-NLS-1$

					// FIXME: SJ: tests for using the delay managed ComputeProgressMonitorDialog.
					// Purpose is to display a Modal Dialog only after an amount of time
					// At this time the monitor is only used in OpenEditor, need to see if we need it for all computing process
					// Actually the system may be robust enough now to not use a modal dialog event in OpenEditor?
					//					this.syncExec(new Runnable() {
					//						
					//						@Override
					//						public void run() {
					//							
					//							try {
					//								ComputeProgressMonitorDialog dialog = new ComputeProgressMonitorDialog(getShell(), TXMEditor.this.getResult());
					//								dialog.runComputingProcess(true);
					//							}
					//							catch (Exception e) {
					//								Log.printStackTrace(e);
					//							}
					//						}
					//					});


					if (!TXMEditor.this.getResult().compute(monitor.split(50), doComputeChildren[0])) {
						Log.fine("TXMEditor.compute(): " + TXMEditor.this.getClass().getSimpleName() + ": computing failed."); //$NON-NLS-1$ //$NON-NLS-2$
					}

					notifyExtensions("notifyEndOfCompute"); //$NON-NLS-1$

					// monitor.worked(50);

					// refresh the UI (especially update the Editor fields from the TXMResult data)
					this.syncExec(new Runnable() {

						@Override
						public void run() {

							try {
								refresh(update);

								// focus in the main widget
								setFocus();

								// TODO uncomment when StatusLine behavior will be decided
								// StatusLine.setMessage(TXMEditor.this.getResult().getComputingDoneMessage());

								// FIXME: SJ: tests to enable/disable the editor widgets while computing
								// recursiveSetEnabled(true);

								// FIXME: SJ: tests to hide/display the result area while computing
								// resultArea.setVisible(true);
							}
							catch (Exception e) {
								Log.finer("TXMEditor.compute(): " + TXMEditor.this.getClass().getSimpleName() + ": Error while refreshing the editor: " + e); //$NON-NLS-1$ //$NON-NLS-2$
								Log.printStackTrace(e);
							}
						}
					});
				}
				catch (Exception e) {
					TXMEditor.this.getResult().resetComputingState();
					Log.warning(NLS.bind(TXMCoreMessages.ErrorWhileComputingP0DbldotP1, this.getResultObject(), e.getMessage()));
					Log.printStackTrace(e);
					return Status.CANCEL_STATUS;
				}
				return Status.OK_STATUS;
			}
		};

		this.computingJob.setPriority(Job.DECORATE);
		this.computingJob.setUser(true);
		this.computingJob.schedule();

		return computingJob;
	}

	/**
	 * Opens an editor specified by its id, computes the specified result then refreshes the editor components.
	 * If the editor was already opened, the computing is skipped and the focus is given to the editor.
	 * 
	 * @param result
	 * @param editorPartId
	 */
	public static <T extends TXMResult> TXMEditor<T> openEditor(T result, Class<TXMEditor<?>> editorclass) {
		return openEditor(new TXMResultEditorInput<T>(result), editorclass.getName());
	}

	/**
	 * Opens an editor specified by its id, computes the specified result then refreshes the editor components.
	 * If the editor was already opened, the computing is skipped and the focus is given to the editor.
	 * 
	 * @param result
	 * @param editorPartId
	 */
	public static <T extends TXMResult> TXMEditor<T> openEditor(T result, String editorPartId) {
		return openEditor(new TXMResultEditorInput<T>(result), editorPartId);
	}

	/**
	 * Opens an editor specified by its id, computes the result stored in the specified editor input then refreshes the editor components.
	 * If the editor was already opened, the computing is skipped and the focus is given to the editor.
	 * 
	 * @param editorInput
	 * @param editorId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T extends TXMResult> TXMEditor<T> openEditor(TXMResultEditorInput<T> editorInput, String editorId) {

		TXMEditor<T> editor = null;
		IWorkbenchWindow window = TXMWindows.getActiveWindow();
		IWorkbenchPage page = window.getActivePage();
		try {
			boolean wasAlreadyOpened = SWTEditorsUtils.isOpenEditor(editorInput, editorId);
			if (!wasAlreadyOpened) {
				try {
					StatusLine.setMessage(NLS.bind(TXMUIMessages.openingP0etc, editorInput.getResult().getSimpleName()));
				}
				catch (Exception e) {
				}
			}
			// since some editor fields need some values of their parent,
			// ensure the parents branch is ready (e.g. Sub-corpus properties in Cooccurrence editor, etc.)
			// show modal blocking&cancelable progression dialog
			if (!wasAlreadyOpened
					// && !editorInput.getResult().getParent().hasBeenComputedOnce()
					) {
				try {
					// TODO uncomment when StatusLine behavior will be decided
					// if (editorInput.getResult().isDirty()) {
					// try {
					// StatusLine.setMessage(editorInput.getResult().getComputingStartMessage());
					// }
					// catch (Exception e) {
					// e.printStackTrace();
					// }
					// }

					ComputeProgressMonitorDialog dialog = new ComputeProgressMonitorDialog(window.getShell(), editorInput.getResult());
					dialog.runComputingProcess(true);
				}
				// user canceling case
				catch (InterruptedException e) {
					CorporaView.refresh();
					return null;
				}
			}


			// opening the editor
			IEditorPart tmpEditor = SWTEditorsUtils.openEditor(editorInput, editorId, true, IWorkbenchPage.MATCH_INPUT | IWorkbenchPage.MATCH_ID);
			if (tmpEditor instanceof TXMEditor) {
				editor = (TXMEditor<T>) tmpEditor;
			}
			else if (tmpEditor instanceof TXMMultiPageEditor) {
				editor = (TXMEditor<T>) ((TXMMultiPageEditor) tmpEditor).getMainEditorPart();
				// If result specify a opening Area use it
			}
			else {
				Log.severe(NLS.bind(TXMCoreMessages.FailedToOpenTheP0Editor, editorId)); // $NON-NLS-1$
				return null;
			}

			if (!wasAlreadyOpened) {

				// }
				// else {
				// TODO until the TXMResult don't know if its parameters are set by the user or by the default preference value, we need to use the AUTO_COMPUTE_ON_EDITOR_OPEN preference
				// compute the result only if the editor wasn't already opened
				// if (//needComputing &&
				// (RCPPreferences.getInstance().getBoolean(RCPPreferences.AUTO_COMPUTE_ON_EDITOR_OPEN))) {
				// editor.compute(false);
				// }
				// else {

				// editor.compute(false); // FIXME: temporary fix for the AHC SVG bug but there are 2 computing (skipped but it stays a quick'n'dirty fix)
				editor.refresh(false);

				// TODO SJ: focus in the main widget or maybe better to do the call directly at the end of TXMEditor.refresh()?
				// TODO MD: I don't think because it will force focus when a linked editor is refreshed
				// editor.setFocus();

				// }
				StatusLine.setMessage(null);
			}

		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return editor;
	}

	/**
	 * Synchronizes the editor from the result and refreshes the corpus view.
	 * Also fires a dirty property change.
	 * 
	 * @param update
	 * @throws Exception
	 */
	@Override
	public final void refresh(boolean update) throws Exception {

		// nothing to refresh
		if (this.parent.isDisposed()) {
			return;
		}

		// skip refresh if the part creation has failed
		if (!this.createPartControlDoneSucessfully) {
			return;
		}

		String className = this.getClass().getSimpleName();
		if (className.isEmpty()) { // anonymous class case
			className = this.getClass().getSuperclass().getSimpleName();
		}

		// this.parent.setRedraw(false);

		this.notifyExtensions("notifyStartOfRefresh"); //$NON-NLS-1$

		TXMResult result = this.getResult();

		// disable the component rendering to avoid strange behaviors when modifying the result area widget
		// this.getContainer().setRedraw(false);

		String title = result.getName();
		// limit tab title length
		if (title.length() > TableUtils.MAX_NAME_LENGTH) {
			title = title.substring(0, TableUtils.MAX_NAME_LENGTH - 1) + "..."; //$NON-NLS-1$
		}
		this.setPartName(title.replace("\n", " ")); // avoid multi-line tab names  //$NON-NLS-1$ //$NON-NLS-2$
		// this.firePropertyChange(TXMEditor.PROP_DIRTY);

		// MD: commented 'if (!update) {...}'
		// SJ: need to check all works fine, it also was dedicated to not refresh the Editor from result after a computing, it's not needed since the computing has been done
		// from fields themselves
		// if (!update) {


		//		if (this.isInBox()) {
		//			getCompositePart(this).setPartName(editor.getGroupName(ca));
		//			this.setPartName(title.replace("\n", " ")); // avoid multi-line tab names  //$NON-NLS-1$ //$NON-NLS-2$
		//		} else {
		//			this.setPartName(title.replace("\n", " ")); // avoid multi-line tab names  //$NON-NLS-1$ //$NON-NLS-2$
		//		}
		this.initializeEditorFieldsFromResult(update);

		Log.finest("TXMEditor.refresh(): " + className + ": auto updating editor from result: " + this.getResult().getClass().getSimpleName() + "."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		this.autoUpdateEditorFieldsFromResult(update); // auto update from Parameter annotations

		Log.finest("TXMEditor.refresh(): " + className + ": updating subclass editor from result: " + this.getResult().getClass().getSimpleName() + "."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		this.updateEditorFromResult(update); // subclasses manual settings
		// }

		// Hide the computing parameter area if the editor wasn't open and the result is computed
		// TODO MD: why was the toolbar hidden ?
		// SJ: need to check all works then remove this code
		// if (!update && !result.isDirty() && !this.topToolBar.isDisposed()) {
		// this.topToolBar.setComputingParametersVisible(false);
		// }

		if (!topToolBar.isDisposed()) {
			this.topToolBar.redraw();
		}

		CorporaView.refreshObject(this);
		TXMResultDebugView.refreshView();

		// re-enable the component rendering
		this.getContainer().setRedraw(true);

		// FIXME: SJ: this code breaks the dirty/chart dirty separation state, eg. if a non electric rendering parameter is changed then the chart is dirty but not the result, need to check this
		this.setDirty(this.getResult().isDirty());

		// FIXME: update all opened editors of the children result but also of the result itself
		// FIXME: debug
		Log.finest("*** TXMEditor.refresh(): " + className + ": synchronizing and refreshing result children opened editors."); //$NON-NLS-1$ //$NON-NLS-2$

		List<TXMResult> results = this.getResult().getDeepChildren();

		for (int i = 0; i < results.size(); i++) {
			Set<ITXMResultEditor<TXMResult>> editors = SWTEditorsUtils.getEditors(results.get(i));

			// FIXME: Debug
			Log.finest("TXMEditor.refresh(): " + className + ": child result = " + results.get(i).getClass().getSimpleName() + " / editors to refresh = " + editors + "."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			// $NON-NLS-2$

			for (ITXMResultEditor<TXMResult> txmEditor : editors) {
				if (txmEditor != this
						// && txmEditor.isDirty() // SJ: break the chart loading
						// SJ 2: some changes has been made in the dirty management, need to check again and may restore the txmEditor.isDirty() test
						) {

					// FIXME: SJ: need to prove/test this code
					// recreate all if the parent result is altered
					// txmEditor.refresh(!this.getResult().isAltered());


					txmEditor.refresh(true);
				}

			}
		}

		Log.finest("___ TXMEditor.refresh(): " + className + ": synchronization done."); //$NON-NLS-1$ //$NON-NLS-2$

		this.notifyExtensions("notifyEndOfRefresh"); //$NON-NLS-1$

		// re-enable the rendering that has been disabled in createPartControl()
		// FIXME: SJ: would be very nice to do that to avoid to see the empty tables before the refresh() that fill the UI.
		// the disabling is done in createPartControl() but the EditionEditor doesn't seem to call refresh() so it breaks it
		// this.parent.setRedraw(true);
	}

	protected boolean firstFieldsInitializationDone = false;

	/**
	 * Fields initialization to be done before setting new values from the TXMResult in the refresh() before calling autoUpdateFieldsValuesFromResult()
	 * 
	 * @param update
	 */
	private void initializeEditorFieldsFromResult(boolean update) throws Exception {
		if (!firstFieldsInitializationDone) {
			firstInitializeEditorFieldsFromResult(update);
			firstFieldsInitializationDone = true;
		}
		reinitializeEditorFieldsFromResult(update);
	}

	/**
	 * Fields re-initialization to be done before setting new values from the TXMResult in the refresh() before calling autoUpdateFieldsValuesFromResult()
	 * 
	 * eg, if a new property is available a List can update its available values
	 * 
	 * rewrite this method if you need extra initialization to be done
	 * 
	 * TODO MD: see if this method should be abstract to force TXMEditors initialization pattern
	 * 
	 * @param update
	 */
	protected void reinitializeEditorFieldsFromResult(boolean update) throws Exception {

	}

	/**
	 * First fields initialization to be done before setting default/new values from the TXMResult
	 * 
	 * This method will be called only one time by initializeEditorFieldsFromResult()
	 * 
	 * rewrite this method if you need extra first initialization to be done
	 * 
	 * TODO MD: see if this method should be abstract to force TXMEditors initialization pattern
	 * 
	 * @param update
	 */
	protected void firstInitializeEditorFieldsFromResult(boolean update) throws Exception {

	}

	/**
	 * Synchronizes the result parameters with editor widgets.
	 * 
	 * It should be called or called itself in the UI thread when accessing Widget parameters.
	 */
	public abstract void updateResultFromEditor();

	/**
	 * Synchronizes the editor widgets with the result parameters.
	 * Dedicated to dynamically change the editor components from the stored result.
	 * 
	 * Use this method if you need to save parameter with Type not managed by the autoUpdateEditorFieldsFromResult method.
	 * 
	 * @throws Exception
	 */
	public abstract void updateEditorFromResult(boolean update) throws Exception;



	/**
	 * Updates the editor fields from TXMResult and declared parameters annotations in both TXMEditor and TXMResult.
	 * Manages these widgets at this moment:
	 * 
	 * ToolItem (Boolean)
	 * FloatSpinner (Float)
	 * Spinner (Integer)
	 * Text (String),
	 * Button (Boolean)
	 * Viewer (Object)
	 * PropertiesSelector (Property and List<Property>)
	 * NewNavigationWidget (Integer)
	 * AssistedQueryWidget (String, from Query)
	 * QueryWidget (String, from Query)
	 * HashMapWidget (HashMap<String, String>)
	 * 
	 */
	public void autoUpdateEditorFieldsFromResult(boolean update) {

		List<Field> fields = BeanParameters.getAllFields(this);

		for (Field f : fields) {
			Parameter parameter = f.getAnnotation(Parameter.class);
			if (parameter == null || parameter.key().isEmpty()) {
				continue;
			}

			Object object = null;
			Object value = null;// getWidgetValue();

			try {
				f.setAccessible(true);
				object = f.get(this);

				// fetch value from all parents
				value = this.getResult().getParameter(parameter.key(), !this.getResult().hasField(parameter.key())); // MD always false, eg the index must not take its query from its subcorpus parent

				if (value == null) {
					// System.out.println("Warning: "+this.getPartName()+" result "+this.getResult()+" "+parameter.key()+" is not initialized.");
					continue; // not initialized
				}

				//				if (object == null) {
				//					continue; // widget not initialized
				//				}

				if (object instanceof TXMParameterControl tpc) {
					object = tpc.getControl();
				}

				if (object instanceof ToolItem) {
					((ToolItem) object).setSelection((Boolean) value);
				}
				else if (object instanceof FloatSpinner) {

					((FloatSpinner) object).setData(BaseAbstractComputeListener.IGNORE_EVENT, true);

					if (value instanceof Double) {
						((FloatSpinner) object).setSelection((Double) value);
					}
					else {
						((FloatSpinner) object).setSelection((Float) value);
					}

					((FloatSpinner) object).setData(BaseAbstractComputeListener.IGNORE_EVENT, false);
				}
				else if (object instanceof Spinner) {
					((Spinner) object).setData(BaseAbstractComputeListener.IGNORE_EVENT, true);
					((Spinner) object).setSelection((Integer) value);
					((Spinner) object).setData(BaseAbstractComputeListener.IGNORE_EVENT, false);
				}
				else if (object instanceof ListText) {
					if (value instanceof String s) {
						((ListText) object).setText(s);
					}
					else if (value instanceof List<?> l) {
						((ListText) object).setText(StringUtils.join(l, "\n"));
					}
					else {
						((ListText) object).setText(String.valueOf(object));
					}
				}
				else if (object instanceof Text) {
					((Text) object).setText(String.valueOf(value));
				}
				else if (object instanceof Combo combo) {
					if (value instanceof String) {
						combo.setText(String.valueOf(value));
					}
					else if (value instanceof String[] strs) {
						combo.setText(StringUtils.join(strs, ","));
					}
					else if (value instanceof int[] ints) {
						combo.setText(StringUtils.join(ints, ","));
					}
					else if (value instanceof float[] floats) {
						combo.setText(StringUtils.join(floats, ","));
					}
					else if (value instanceof double[] doubles) {
						combo.setText(StringUtils.join(doubles, ","));
					}
				}
				else if (object instanceof StyledText) {
					((StyledText) object).setText(String.valueOf(value));
				}
				else if (object instanceof Button) {
					((Button) object).setSelection((Boolean) value);
				}
				else if (object instanceof BooleanCombo bc) {
					if (value instanceof Boolean b)
						bc.setSelection(b);
					else if (value instanceof String s) {
						bc.setSelectedItem(s);
					}
				}
				else if (object instanceof RadioGroup) {
					((RadioGroup) object).setSelection((String) value);
				}
				else if (object instanceof Viewer) {
					if (value != null) {
						// FIXME: SJ: fire a new selection changed event leading to a cyclic recursion...
						// FIXME: SJ: became useless? since there is now a test above on !this.getResult().hasParameterChanged(parameter.key(), this.lastComputingParameters)
						((Viewer) object).setData(BaseAbstractComputeListener.IGNORE_EVENT, true);
						if (value instanceof StructuredSelection) { // no need to create a new StructuredSelection
							((Viewer) object).setSelection((StructuredSelection) value, true);
						}
						else {
							((Viewer) object).setSelection(new StructuredSelection(value), true);
						}
						((Viewer) object).setData(BaseAbstractComputeListener.IGNORE_EVENT, false);
					}
				}
				else if (object instanceof ReferencePatternSelector) {
					if (value instanceof ReferencePattern) {
						ReferencePattern ref = (ReferencePattern) value;
						ReferencePatternSelector selector = (ReferencePatternSelector) object;
						selector.setSelectedProperties(ref.getProperties());
						selector.setPattern(ref.getPattern());
					}
				}
				else if (object instanceof PropertiesSelector) {
					// multiple
					if (value instanceof List || value instanceof ArrayList) {
						((PropertiesSelector) object).setSelectedProperties((List) value);
					}
					else if (value instanceof ReferencePattern) {
						((PropertiesSelector<Property>) object).setSelectedProperties(((ReferencePattern) value).getProperties());
					}
					// single
					else {
						((PropertiesSelector<Property>) object).setSelectedProperty((Property) value);
					}
				}
				else if (object instanceof NewNavigationWidget) {
					((NewNavigationWidget) object).setCurrentPosition((Integer) value);
				}
				else if (object instanceof HashMapWidget) {
					((HashMapWidget) object).setValues((LinkedHashMap) value);
				}
				else if (object instanceof AssistedChoiceQueryWidget) {
					IQuery q = (IQuery) value;
					if (!(q.getQueryString().isEmpty())) {
						((AssistedChoiceQueryWidget) object).setQuery(q);
					}
				}
				else if (object instanceof AssistedQueryWidget) {
					if (!((CQLQuery) value).getQueryString().isEmpty()) {
						((AssistedQueryWidget) object).setText(((CQLQuery) value).getQueryString());
					}
				}
				else if (object instanceof QueryWidget) {
					if (!((CQLQuery) value).getQueryString().isEmpty()) {
						((QueryWidget) object).setText(((CQLQuery) value).getQueryString());
					}
				}
				else {
					try { // last resort, the editor member is the value to set
						f.set(this, value);
					}
					catch (Exception e) {
						Log.finest("TXMEditor.autoUpdateEditorFieldsFromResult(): ???//FIXME SJ: need to describe this case."); //$NON-NLS-1$
					}
				}

				// Log
				if (object != null) {
					String message = "TXMEditor.autoUpdateEditorFieldsFromResult(): " + this.getClass().getSimpleName() + ": setting editor parameter " + parameter.key() + " = " + value; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					if (value != null) {
						message += " (" + value.getClass() + " to object " + object + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					}
					Log.finest(message);
				}

				// FIXME: SJ: need to extend this list of managed Widgets if needed
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				if (object != null) {
					e.printStackTrace();
				}
			}
		}

		// FIXME: SJ: Updating the compute button enabled/disabled state
		// the code below doesn't work as expected for text fields (as query field or other)
		// try {
		// this.topToolBar.getItem(0).setEnabled(this.getResult().canCompute());
		// }
		// catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
	}


	/**
	 * Updates the result parameters from the editor declared parameters annotations in both TXMEditor and TXMResult.
	 * Manages these widgets at this moment: ToolItem, FloatSpinner, Spinner, Text, Button, Viewer, PropertiesSelector, NewNavigationWidget, AssistedQueryWidget, QueryWidget.
	 */
	@SuppressWarnings("unchecked")
	public void autoUpdateResultFromEditorParameters() {

		// List<Field> fields = BeanParameters.getAllFields(this);
		LinkedHashMap<String, Field> fields = BeanParameters.getFieldsPerName(this);
		for (String key : fields.keySet()) {
			Field f = fields.get(key);

			Parameter parameter = f.getAnnotation(Parameter.class);
			if (parameter == null || parameter.key().isEmpty()) {
				continue;
			}

			Field targetField = BeanParameters.getField(getResult(), key);

			//			Field targetField = null;
			//			for (Field f2 : this.getResult().getClass().getFields()) {
			//				Parameter a = f2.getAnnotation(Parameter.class);
			//				if (a != null && a.key().equals(parameter.key())) {
			//					targetField = f2;
			//					break;
			//				}
			//			}

			try {
				f.setAccessible(true);
				Object object = f.get(this);

				if (object == null) {
					// FIXME what about a parametrized member which is not a widget ?
					continue; // the parameterized member variable is not set
				}

				if (object instanceof TXMParameterControl tpc) {
					object = tpc.getControl();
				}

				Object value = null;
				if (object instanceof ToolItem) {
					value = ((ToolItem) object).getSelection();
				}
				else if (object instanceof FloatSpinner) {
					value = ((FloatSpinner) object).getSelectionAsFloat();
				}
				else if (object instanceof HashMapWidget) {
					value = ((HashMapWidget) object).getValues();
				}
				else if (object instanceof Spinner) {
					value = ((Spinner) object).getSelection();
				}
				else if (object instanceof AssistedChoiceQueryWidget) {
					value = ((AssistedChoiceQueryWidget) object).getQuery();
				}
				else if (object instanceof AssistedQueryWidget) {
					value = ((AssistedQueryWidget) object).getQuery();
				}
				else if (object instanceof QueryWidget) {
					value = ((QueryWidget) object).getQuery();
				}
				else if (object instanceof LargeQueryField) {
					value = ((LargeQueryField) object).getQuery();
				}
				else if (object instanceof ListText) {
					value = ((ListText) object).toList();
				}
				else if (object instanceof Text) {
					value = ((Text) object).getText();
				}
				else if (object instanceof StyledText) {
					value = ((StyledText) object).getText();
				}
				else if (object instanceof Button) {
					value = ((Button) object).getSelection();
				}
				else if (object instanceof RadioGroup) {
					value = ((RadioGroup) object).getSelection();
				}
				else if (object instanceof Viewer) {

					IStructuredSelection selection = ((IStructuredSelection) ((Viewer) object).getSelection());

					// Multi-selection
					if (SWT.MULTI == (((Viewer) object).getControl().getStyle() & SWT.MULTI)) {
						value = selection.toList();
					}
					// Single selection
					else {
						value = selection.getFirstElement();
					}
				}
				else if (object instanceof ReferencePatternSelector) {
					Field resultField = BeanParameters.getField(this.getResult(), parameter.key());
					if (resultField != null) {
						if (resultField.getType().isAssignableFrom(ReferencePattern.class)) {
							ReferencePatternSelector selector = (ReferencePatternSelector) object;
							ReferencePattern ref = new ReferencePattern(selector.getSelectedProperties(), selector.getPattern());
							value = ref;
						}
					}
				}
				else if (object instanceof PropertiesSelector) {
					Field resultField = BeanParameters.getField(this.getResult(), parameter.key());
					if (resultField != null) {
						// multiple
						if (resultField.getType().isAssignableFrom(List.class) || resultField.getType().isAssignableFrom(ArrayList.class)) {
							value = new ArrayList<>(((PropertiesSelector<Property>) object).getSelectedProperties());
							// need to copy the list otherwise the properties selector modify the result itself through reference
						}
						else if (resultField.getType().isAssignableFrom(ReferencePattern.class)) {
							value = new ReferencePattern(((PropertiesSelector<Property>) object).getSelectedProperties());
						}
						// single
						else if (resultField.getType().isAssignableFrom(Property.class)) {
							value = ((PropertiesSelector<Property>) object).getSelectedProperties().get(0);
						}
					}
				}
				else if (object instanceof NewNavigationWidget) {
					value = ((NewNavigationWidget) object).getCurrentPosition();
				}
				else if (object instanceof Combo combo) {
					String s = ((Combo) object).getText();
					Class<?> clazz = targetField.getType();
					if (clazz.equals(String.class)) {
						value = s;
					}
					else if (clazz.equals(List.class)) {
						if (!s.isEmpty()) {
							value = new ArrayList<>(Arrays.asList(s.split(","))); //$NON-NLS-1$
						}
						else {
							value = new ArrayList<>();
						}
					}
					else if (clazz.equals(String[].class)) {
						if (!s.isEmpty()) {
							value = s.split(","); //$NON-NLS-1$
						}
						else {
							value = new String[0];
						}
					}
					else if (clazz.equals(int[].class)) {
						if (!s.isEmpty()) {
							String[] values = s.split(","); //$NON-NLS-1$
							int[] ivalues = new int[values.length];
							for (int i = 0; i < ivalues.length; i++) {
								ivalues[i] = Integer.parseInt(values[i]);
							}
						}
						else {
							value = new int[0];
						}
					}
					else if (clazz.equals(double[].class)) {
						if (!s.isEmpty()) {
							String[] values = s.split(","); //$NON-NLS-1$
							double[] ivalues = new double[values.length];
							for (int i = 0; i < ivalues.length; i++) {
								ivalues[i] = Double.parseDouble(values[i]);
							}
						}
						else {
							value = new double[0];
						}
					}
				}
				else if (object instanceof org.eclipse.swt.widgets.List l) {

					String[] s = l.getSelection();
					Class<?> clazz = targetField.getType();
					if (clazz.equals(String.class)) {
						value = StringUtils.join(s, ", "); //$NON-NLS-1$
					}
					else if (clazz.equals(List.class)) {
						value = new ArrayList<>(Arrays.asList(s));
					}

				}
				else if (object instanceof BooleanCombo bc) {

					Class<?> clazz = targetField.getType();
					if (clazz.equals(String.class)) {
						value = bc.getSelectedItem();
					}
					else if (clazz.equals(Boolean.class)) {
						value = bc.getSelection();
					}
				}
				else {
					value = object;
				}
				// FIXME: SJ: need to extend this list of managed Widgets if needed

				// propagate to all parents
				this.getResult().setParameter(parameter.key(), value, true);
			}
			catch (IllegalArgumentException e) {
				Log.severe(NLS.bind("Error while auto-updating parameter field {0}: {1}.", parameter.key(), e)); //$NON-NLS-1$
				Log.printStackTrace(e);
			}
			catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Deletes the TXM result.
	 * 
	 * @return true if the deletion was successful otherwise false
	 */
	public boolean deleteResult() {

		try {
			return this.getEditorInput().deleteResult();
		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.bind(TXMCoreMessages.CouldNotDeleteTheP0Result, this.getResult()));
			Log.printStackTrace(e);
			return false;
		}
	}

	/**
	 * Closes the editor from the UI thread and deletes or not the linked result.
	 */
	@Override
	public void close(final boolean deleteResult) {

		this.getSite().getShell().getDisplay().syncExec(new Runnable() {

			@Override
			public void run() {

				getSite().getPage().closeEditor(TXMEditor.this, false);
				if (deleteResult) {
					deleteResult();
				}
			}
		});
	}

	/**
	 * Closes the editor from the UI thread.
	 */
	@Override
	public void close() {
		this.close(false);
	}


	/**
	 * Creates and populates a pop up menu with the specified id and attaches it to the specified composite.
	 * The menu will be accessible through the RCP extension in plugin.xml by its id, eg. "popup:" + menuId.
	 * 
	 * @param composite
	 * @param menuId
	 * @return
	 */
	public static Menu initContextMenu(Composite composite, String menuId) {
		return initContextMenu(composite, menuId, null, null);
	}

	/**
	 * Creates and populates a pop up menu and attaches it to the specified composite.
	 * The menu will be accessible through the RCP extension in plugin.xml by its id, eg. "popup:" + menuId.
	 * 
	 * The menu ID will be the editor/workbench part ID.
	 * 
	 * @param composite
	 * @param partSite not null
	 * @param selectionProvider
	 * @return
	 */
	public static Menu initContextMenu(Composite composite, IWorkbenchPartSite partSite, ISelectionProvider selectionProvider) {
		return initContextMenu(composite, partSite.getId(), partSite, selectionProvider);
	}

	/**
	 * Creates and populates a pop up menu with the specified id and attaches it to the specified composite.
	 * The menu will be accessible through the RCP extension in plugin.xml by its id, eg. "popup:" + menuId.
	 * 
	 * @param composite
	 * @param menuId
	 * @param partSite
	 * @param selectionProvider
	 * @return
	 */
	protected static Menu initContextMenu(Composite composite, String menuId, IWorkbenchPartSite partSite, ISelectionProvider selectionProvider) {

		MenuManager menuManager = new MenuManager(menuId, menuId);

		Menu menu = menuManager.createContextMenu(composite);
		composite.setMenu(menu);

		if (partSite != null && selectionProvider != null) {
			ISelectionProvider current = partSite.getSelectionProvider();
			if (current == null) {
				partSite.setSelectionProvider(selectionProvider); // Selection provider to retrieve lines/entities of viewer
				partSite.registerContextMenu(menuId, menuManager, selectionProvider);
			} else { // if there is already one selection provider set, make a combined selection provider
				ISelectionProvider combinedSelectionProvider = new ISelectionProvider() {

					@Override
					public void setSelection(ISelection selection) {
						current.setSelection(selection);
						selectionProvider.setSelection(selection);
					}

					@Override
					public void removeSelectionChangedListener(ISelectionChangedListener listener) {
						current.removeSelectionChangedListener(listener);
						selectionProvider.removeSelectionChangedListener(listener);
					}

					@Override
					public ISelection getSelection() { // the current provider is tested first  
						ISelection sel = current.getSelection();
						if (sel == null || sel.isEmpty()) {
							return selectionProvider.getSelection();
						}
						return sel;
					}

					@Override
					public void addSelectionChangedListener(ISelectionChangedListener listener) {
						current.addSelectionChangedListener(listener);
						selectionProvider.addSelectionChangedListener(listener);
					}
				};

				partSite.setSelectionProvider(combinedSelectionProvider); // Selection provider to retrieve lines/entities of viewer
				partSite.registerContextMenu(menuId, menuManager, combinedSelectionProvider);
			}
		}
		else {
			IMenuService menuService = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getService(IMenuService.class);
			menuService.populateContributionManager(menuManager, "popup:" + menuId); //$NON-NLS-1$
		}

		menuManager.update(true);
		return menu;
	}



	/**
	 * Use this to know if this editor is still open or functional
	 */
	public final boolean isDisposed() {
		if (parent == null) return true;
		return parent.isDisposed();
	}

	@Override
	public Composite getContainer() {
		return parent;
	}

	/**
	 * Layouts the parent.
	 */
	public void layout(boolean b) {
		if (parent != null && !parent.isDisposed()) {
			parent.layout(b);
		}
	}


	/**
	 * Links the specified editor to this editor.
	 * 
	 * @param editor
	 */
	@Override
	public void addLinkedEditor(final ITXMResultEditor<TXMResult> editor) {

		if (editor == null) return;

		if (this.linkedEditors.contains(editor)) return; // nothing to do

		this.linkedEditors.add(editor);

		// Debug
		Log.finest(this.getClass().getSimpleName() + ": A linked editor of type \"" + editor.getClass().getSimpleName() + "\" has been added to this one. Linked editors count: " //$NON-NLS-1$ //$NON-NLS-2$
				+ this.linkedEditors.size() + ".");  //$NON-NLS-1$

		// FIXME: temporary solution to break the link with chart editor from another TXM result editor when its closed
		// a better solution would be to redefine the onclose event in a root class of all TXM editors
		editor.getSite().getPage().addPartListener(new IPartListener() {

			@Override
			public void partOpened(IWorkbenchPart part) {
			}

			@Override
			public void partDeactivated(IWorkbenchPart part) {
			}

			@Override
			public void partClosed(IWorkbenchPart part) {

				if (part.equals(editor)) {
					removeLinkedEditor(editor);
					editor.getSite().getPage().removePartListener(this);
				}
			}

			@Override
			public void partBroughtToTop(IWorkbenchPart part) {
			}

			@Override
			public void partActivated(IWorkbenchPart part) {
			}
		});


	}

	/**
	 * Unlinks the specified editor from this editor.
	 * 
	 * @param editor
	 */
	@Override
	public void removeLinkedEditor(ITXMResultEditor<TXMResult> editor) {
		if (this.linkedEditors.remove(editor)) {
			// Debug
			Log.finest(this.getClass().getSimpleName() + ": A linked editor of type \"" + editor.getClass().getSimpleName() + "\" has been removed from this one. Linked editors count: " //$NON-NLS-1$//$NON-NLS-2$
					+ this.linkedEditors.size() + ".");   //$NON-NLS-1$

			//this.disconnected(editor);
		}
	}

	/**
	 * Gets the linked editors to this one if exist.
	 * 
	 * @return
	 */
	@Override
	public ArrayList<ITXMResultEditor<TXMResult>> getLinkedEditors() {
		return this.linkedEditors;
	}

	/**
	 * Gets the first linked editor of the specified class if exists.
	 * 
	 * @param editorClass
	 * @return
	 */
	@Override
	public <E extends ITXMResultEditor<TXMResult>> E getLinkedEditor(Class<E> editorClass) {
		E editor = null;
		for (int i = 0; i < this.linkedEditors.size(); i++) {
			if (this.linkedEditors.get(i).getClass().equals(editorClass)) { // TODO won't work if the linkedEditor inherits E.class
				editor = (E) this.linkedEditors.get(i);
				break;
			}
		}
		return editor;
	}

	/**
	 * Gets the linked editors of the specified class if exist otherwise returns an empty list.
	 * 
	 * @param editorClass
	 * @return
	 */
	@Override
	public <E extends ITXMResultEditor<TXMResult>> ArrayList<E> getLinkedEditors(Class<E> editorClass) {
		ArrayList<E> editors = new ArrayList<>();
		if (this.linkedEditors != null) {
			for (int i = 0; i < this.linkedEditors.size(); i++) {
				if (this.linkedEditors.get(i).getClass().equals(editorClass)) {
					editors.add((E) this.linkedEditors.get(i));
				}
			}
		}
		return editors;
	}

	/**
	 * Gets the Compute Tool item button.
	 * 
	 * @return the computeButton
	 */
	protected ToolItem getComputeButton() {
		return computeButton;
	}

	/**
	 * Gets the Compute Tool item button.
	 * 
	 * @return the computeButton
	 */
	protected ToolItem getRevertButton() {
		return revertButton;
	}

	/**
	 * Removes the Compute Tool item button.
	 */
	protected void removeComputeButton() {

		if (this.computeButton != null && !this.computeButton.isDisposed()) {

			// FIXME: SJ: the contribution should be released before the dispose but this code doesn't seem to work. And without the release the code in createPartControl() that disposes the first
			// line composite if empty doesn't work
			// IMenuService menuService = (IMenuService) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getService(IMenuService.class);
			// menuService.releaseContributions((ContributionManager) ((HandledContributionItem) this.computeButton.getData()).getParent());

			this.computeButton.dispose();
		}
	}

	/**
	 * @return the parent
	 */
	public Composite getParent() {
		return parent;
	}

	public static Object getValueForWidget(Object bean, Field f, Field targetedField) {
		Parameter parameter = f.getAnnotation(Parameter.class);
		if (parameter == null || parameter.key().isEmpty()) {
			return null;
		}

		try {
			f.setAccessible(true);
			Object object = f.get(bean);
			if (object == null) {
				// FIXME what about a parametrized member which is not a widget ?
				return null; // the parameterized member variable is not set
			}

			if (object instanceof TXMParameterControl tpc) {
				object = tpc.getControl();
			}

			Object value = null;
			if (object instanceof ToolItem) {
				value = ((ToolItem) object).getSelection();
			}
			else if (object instanceof FloatSpinner) {
				value = ((FloatSpinner) object).getSelectionAsFloat();
			}
			else if (object instanceof Spinner) {
				value = ((Spinner) object).getSelection();
			}
			else if (object instanceof Text) {
				value = ((Text) object).getText();
			}
			else if (object instanceof StyledText) {
				value = ((StyledText) object).getText();
			}
			else if (object instanceof Button) {
				value = ((Button) object).getSelection();
			}
			else if (object instanceof Viewer) {

				IStructuredSelection selection = ((IStructuredSelection) ((Viewer) object).getSelection());

				// Multi-selection
				if (SWT.MULTI == (((Viewer) object).getControl().getStyle() & SWT.MULTI)) {
					value = selection.toList();
				}
				// Single selection
				else {
					value = selection.getFirstElement();
				}
			}
			else if (object instanceof ReferencePatternSelector) {
				if (targetedField != null) {
					if (targetedField.getType().isAssignableFrom(ReferencePattern.class)) {
						ReferencePatternSelector selector = (ReferencePatternSelector) object;
						ReferencePattern ref = new ReferencePattern(selector.getSelectedProperties(), selector.getPattern());
						value = ref;
					}
				}
			}
			else if (object instanceof PropertiesSelector) {
				if (targetedField != null) {
					// multiple
					if (targetedField.getType().isAssignableFrom(List.class)) {
						value = new ArrayList<>(((PropertiesSelector<Property>) object).getSelectedProperties());
						// need to copy the list otherwise the properties selector modify the result itself through reference
					}
					else if (targetedField.getType().isAssignableFrom(ReferencePattern.class)) {
						value = new ReferencePattern(((PropertiesSelector<Property>) object).getSelectedProperties());
					}
					// single
					else {
						value = ((PropertiesSelector<Property>) object).getSelectedProperties().get(0);
					}
				}
			}
			else if (object instanceof NewNavigationWidget) {
				value = ((NewNavigationWidget) object).getCurrentPosition();
			}
			else if (object instanceof AssistedChoiceQueryWidget) {
				value = ((AssistedChoiceQueryWidget) object).getQuery();
			}
			else if (object instanceof AssistedQueryWidget) {
				value = ((AssistedQueryWidget) object).getQuery();
			}
			else if (object instanceof QueryWidget) {
				value = ((QueryWidget) object).getQuery();
			}
			else {
				value = object;
			}

			// FIXME: need to extend this list of managed Widgets if needed

			// propagate to all parents
			// this.getResult().setParameter(parameter.key(), value, true);
		}
		catch (IllegalArgumentException e) {
			Log.severe("TXMEditor.autoUpdateResultFromEditorParameters(): Error with field " + parameter.key()); //$NON-NLS-1$
			e.printStackTrace();
		}
		catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void setValueForWidget(Object bean, Field f, Object value) {

		if (value == null) {
			// System.out.println("Warning: "+this.getPartName()+" result "+this.getResult()+" "+parameter.key()+" is not initialized.");
			return; // not initialized
		}

		Parameter parameter = f.getAnnotation(Parameter.class);
		Object object = null;

		try {
			f.setAccessible(true);
			object = f.get(bean);

			if (object instanceof TXMParameterControl tpc) {
				object = tpc.getControl();
			}

			if (object instanceof ToolItem) {
				((ToolItem) object).setSelection((Boolean) value);
			}
			else if (object instanceof FloatSpinner fSpinner) {

				fSpinner.setData(BaseAbstractComputeListener.IGNORE_EVENT, true);

				if (value instanceof Double) {
					fSpinner.setSelection((Double) value);
				}
				else {
					fSpinner.setSelection((Float) value);
				}

				fSpinner.setData(BaseAbstractComputeListener.IGNORE_EVENT, false);
			}
			else if (object instanceof Spinner spinner) {
				spinner.setData(BaseAbstractComputeListener.IGNORE_EVENT, true);
				spinner.setSelection((Integer) value);
				spinner.setData(BaseAbstractComputeListener.IGNORE_EVENT, false);
			}
			else if (object instanceof Text) {
				((Text) object).setText(String.valueOf(value));
			}
			else if (object instanceof Combo combo) {
				if (value instanceof String) {
					combo.setText(String.valueOf(value));
				}
				else if (value instanceof String[] strs) {
					combo.setText(StringUtils.join(strs, ","));
				}
			}
			else if (object instanceof StyledText) {
				((StyledText) object).setText(String.valueOf(value));
			}
			else if (object instanceof Button) {
				((Button) object).setSelection((Boolean) value);
			}
			else if (object instanceof Viewer viewer) {
				if (value != null) {
					// FIXME: SJ: fire a new selection changed event leading to a cyclic recursion...
					// FIXME: SJ: became useless? since there is now a test above on !this.getResult().hasParameterChanged(parameter.key(), this.lastComputingParameters)
					viewer.setData(BaseAbstractComputeListener.IGNORE_EVENT, true);
					if (value instanceof StructuredSelection) { // no need to create a new StructuredSelection
						viewer.setSelection((StructuredSelection) value, true);
					}
					else {
						viewer.setSelection(new StructuredSelection(value), true);
					}
					viewer.setData(BaseAbstractComputeListener.IGNORE_EVENT, false);
				}
			}
			else if (object instanceof ReferencePatternSelector) {
				if (value instanceof ReferencePattern) {
					ReferencePattern ref = (ReferencePattern) value;
					ReferencePatternSelector selector = (ReferencePatternSelector) object;
					selector.setSelectedProperties(ref.getProperties());
					selector.setPattern(ref.getPattern());
				}
			}
			else if (object instanceof PropertiesSelector pSelector) {
				// multiple
				if (value instanceof List || value instanceof ArrayList) {
					pSelector.setSelectedProperties((List) value);
				}
				else if (value instanceof ReferencePattern) {
					pSelector.setSelectedProperties(((ReferencePattern) value).getProperties());
				}
				// single
				else {
					pSelector.setSelectedProperty((Property) value);
				}
			}
			else if (object instanceof NewNavigationWidget) {
				((NewNavigationWidget) object).setCurrentPosition((Integer) value);
			}
			else if (object instanceof AssistedChoiceQueryWidget) {
				if (!((IQuery) value).getQueryString().isEmpty()) {
					((AssistedChoiceQueryWidget) object).setQuery(((IQuery) value));
				}
			}
			else if (object instanceof AssistedQueryWidget) {
				if (!((CQLQuery) value).getQueryString().isEmpty()) {
					((AssistedQueryWidget) object).setText(((CQLQuery) value).getQueryString());
				}
			}
			else if (object instanceof QueryWidget) {
				if (!((CQLQuery) value).getQueryString().isEmpty()) {
					((QueryWidget) object).setText(((CQLQuery) value).getQueryString());
				}
			}
			else {
				try {
					f.set(bean, value);
				}
				catch (Exception e) {
					Log.fine(""); //$NON-NLS-1$
				}
			}

			// Log
			if (object != null) {
				String message = "TXMEditor.autoUpdateEditorFieldsFromResult(): " + bean.getClass().getSimpleName() + ": setting editor parameter " + parameter.key() + " = " + value; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				if (value != null) {
					message += " (" + value.getClass() + " to object " + object + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				}
				Log.finest(message);
			}

			// FIXME: SJ: need to extend this list of managed Widgets if needed
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			if (object != null) {
				e.printStackTrace();
			}
		}
	}
}
