package org.txm.rcp.swt.widget;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.listeners.ComputeKeyListener;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

public class MultipleQueriesComposite extends Composite {


	private ScrolledComposite sc1;

	private Composite queriesFocusComposite;

	private Label miniInfoLabel;

	/** The query widgets. */
	private List<QueryField> queryWidgets = new ArrayList<>();

	CQPCorpus corpus;

	int maxQueries;

	/**
	 * Linked editor.
	 */
	protected TXMEditor<?> editor;

	public MultipleQueriesComposite(Composite parent, int style, TXMEditor<?> editor, int maxQueries, Label miniInfoLabel) {
		this(parent, style, editor, maxQueries, miniInfoLabel, false);
	}

	public MultipleQueriesComposite(Composite parent, int style, TXMEditor<?> editor, int maxQueries, Label miniInfoLabel, boolean createAssociatedQueryWidget) {

		super(parent, style);

		this.setLayout(new GridLayout(2, false));

		if (createAssociatedQueryWidget) {
			AssistedChoiceQueryWidget queryWidget = new AssistedChoiceQueryWidget(this, SWT.DROP_DOWN, editor.getResult().getFirstParent(CQPCorpus.class));
			GridData layoutData = new GridData(GridData.VERTICAL_ALIGN_CENTER);
			layoutData.horizontalAlignment = GridData.FILL;
			layoutData.grabExcessHorizontalSpace = true;
			queryWidget.setLayoutData(layoutData);
			queryWidget.addKeyListener(new KeyListener() {

				@Override
				public void keyPressed(KeyEvent e) {
					if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {

						if (queryWidget.getQueryString().length() > 0 && onPlusButtonPressed(null, queryWidget.getQuery())) {
							queryWidget.clearQuery();
							queryWidget.setFocus();
						}
					}
				}

				@Override
				public void keyReleased(KeyEvent e) { }
			});
		}

		sc1 = new ScrolledComposite(this, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		sc1.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 2, 1));
		sc1.setExpandHorizontal(true);
		sc1.setExpandVertical(true);
		sc1.setMinSize(0, 120);

		// add queries from file button
		Button addQueriesFromFileButton = new Button(this, SWT.NONE);
		addQueriesFromFileButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		addQueriesFromFileButton.setText(TXMUIMessages.addQueriesFromFile);
		addQueriesFromFileButton.setToolTipText(TXMUIMessages.thePropertiesFileContainsOneQueryPerLineFormattedWithAKeyPlusaQuery);
		addQueriesFromFileButton.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {

				FileDialog dialog = new FileDialog(event.display.getActiveShell(), SWT.OPEN);
				dialog.setText(TXMUIMessages.selectAnUTF8PropertyFile);
				String path = dialog.open();
				if (path != null) {
					Properties props = new Properties();
					int count = 0;
					try {
						props.load(IOUtils.getReader(new File(path)));
						for (Object k : props.keySet()) {
							IQuery q = Query.stringToQuery(props.get(k).toString());
							q.setName(k.toString());
							if (onPlusButtonPressed(event, q)) {
								count++;
							}
						}
					}
					catch (Exception e) {
						Log.printStackTrace(e);
					}
					if (count > 0) {
						editor.compute(true);
					}
				}
			}
		});

		queriesFocusComposite = new Composite(sc1, SWT.NONE);
		GridLayout layoutForAdvanced = new GridLayout(1, false);
		queriesFocusComposite.setLayout(layoutForAdvanced);

		sc1.setContent(queriesFocusComposite);
		sc1.setExpandVertical(true);
		sc1.setExpandHorizontal(true);
		sc1.addControlListener(new ControlAdapter() {

			@Override
			public void controlResized(ControlEvent e) {

				Rectangle r = sc1.getClientArea();
				sc1.setMinSize(queriesFocusComposite.computeSize(r.width, SWT.DEFAULT));
			}
		});

		this.miniInfoLabel = miniInfoLabel;
		this.editor = editor;
		this.maxQueries = maxQueries;
		corpus = editor.getResult().getFirstParent(CQPCorpus.class);



	}

	public ArrayList<IQuery> getQueries() {

		ArrayList<IQuery> queryList = new ArrayList<>();
		for (MultipleQueriesComposite.QueryField wid : queryWidgets) {
			if (wid.getQuery() != null && !wid.getQuery().isEmpty()) {
				queryList.add(wid.getQuery());
				wid.memorize();
			}
		}
		return queryList;
	}

	/**
	 * Checks if the specified query already exists or not.
	 * 
	 * @param query
	 * @return true if the specified query already exists, otherwise false
	 */
	protected boolean queryExists(IQuery query) {

		return this.queryExists(query, 0);
	}

	/**
	 * Checks if the specified query already exists more than the specified max count.
	 * 
	 * @param query
	 * @param maxCount
	 * @return
	 */
	protected boolean queryExists(IQuery query, int maxCount) {

		int count = 0;
		for (int i = 0; i < queryWidgets.size(); i++) {
			if (queryWidgets.get(i).getQuery().equals(query) || queryWidgets.get(i).getQuery().equals(query)) {
				count++;
			}
		}

		if (count > maxCount) {
			MessageDialog d = new MessageDialog(this.getShell(), TXMCoreMessages.error_error, null,
					NLS.bind(TXMUIMessages.error_theP0QueryIsAlreadyRepresentedByACurveInTheGraphic, query), 0, new String[] { TXMCoreMessages.common_ok }, 0);
			d.open();
			return true;
		}
		return false;
	}

	/**
	 * Adds a query field.
	 *
	 * @return the assisted query widget
	 */
	public QueryField addFocusQueryField(IQuery query) {

		// do not add same query twice
		if (!query.isEmpty() && this.queryExists(query)) {
			return null;
		}

		QueryField queryField = new QueryField(this.queriesFocusComposite, SWT.NONE);
		// avoid null pointer exception
		if (query != null) {
			queryField.setQuery(query);
		}
		GridData gridData = new GridData(GridData.FILL, GridData.FILL, true, false);
		queryField.setLayoutData(gridData);

		this.queriesFocusComposite.layout(true);

		return queryField;
	}

	/**
	 * Adds an empty query field.
	 * 
	 * @return the added QueryField
	 */
	private QueryField addFocusQueryField() {

		return this.addFocusQueryField(new CQLQuery("")); //$NON-NLS-1$
	}

	/**
	 * 
	 * @param event
	 * @param query
	 * @return
	 */
	public boolean onPlusButtonPressed(Event event, IQuery query) {

		if (queryWidgets != null && !queryExists(query)) {
			Log.finest("Adding query field."); //$NON-NLS-1$

			QueryField qf = addFocusQueryField(query);
			sc1.layout(true);
			queriesFocusComposite.layout(true);
			queriesFocusComposite.getParent().requestLayout();
			//			sc1.setMinSize(queriesFocusComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));

			return qf != null;
		}

		return false;
	}

	/**
	 * 
	 */
	public void updateMiniInfoLabelText() {

		if (miniInfoLabel == null || miniInfoLabel.isDisposed()) return;

		if (queryWidgets.size() == 0) {
			miniInfoLabel.setText(TXMUIMessages.noQueryDash);
		}
		else if (queryWidgets.size() == 1) {
			miniInfoLabel.setText(queryWidgets.size() + TXMUIMessages.spaceQueryDash);
		}
		else {
			miniInfoLabel.setText(queryWidgets.size() + TXMUIMessages.spaceQueriesDash);
		}
	}

	public class QueryField extends Composite {

		/**
		 * Assisted query widget.
		 */
		protected AssistedChoiceQueryWidget assistedQueryWidget;


		protected Button deleteButton;

		Composite parent;

		/**
		 * 
		 * @param parent
		 * @param style
		 */
		public QueryField(Composite parent, int style) {

			super(parent, style);

			this.parent = parent;

			GridLayout glayout = new GridLayout(2, false);
			glayout.marginBottom = glayout.marginTop = glayout.marginHeight = glayout.verticalSpacing = 0;

			this.setLayout(glayout);

			queryWidgets.add(this);
			updateMiniInfoLabelText();

			assistedQueryWidget = new AssistedChoiceQueryWidget(this, SWT.BORDER, corpus, true);
			assistedQueryWidget.setFocus();
			assistedQueryWidget.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));

			// key listener
			assistedQueryWidget.addKeyListener(new ComputeKeyListener(editor) {

				@Override
				public void keyPressed(KeyEvent e) {

					if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
						if (!assistedQueryWidget.getRawString().isEmpty() && !queryExists(assistedQueryWidget.getQuery(), 1)) {
							super.keyPressed(e);
						}
					}
					else {
						editor.setDirty(true);
					}
				}
			});

			// delete query button
			deleteButton = new Button(this, SWT.NONE);
			deleteButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			deleteButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_DELETE));
			deleteButton.setToolTipText("Remove this query");
			deleteButton.addListener(SWT.Selection, new Listener() {

				@Override
				public void handleEvent(Event event) {

					deleteQuery();
				}


			});
			// listener
			deleteButton.addSelectionListener(new ComputeSelectionListener(editor));

		}

		private void deleteQuery() {

			QueryField lastwidget = QueryField.this;
			lastwidget.setSize(200, 30);
			queryWidgets.remove(QueryField.this);
			updateMiniInfoLabelText();
			lastwidget.dispose();
			sc1.layout(true);
			queriesFocusComposite.layout(true);
			//			sc1.setMinSize(queriesFocusComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
			// delete button states
		}

		/**
		 * Gets the delete button.
		 * 
		 * @return
		 */
		public Button getDeleteButton() {

			return this.deleteButton;
		}

		/**
		 * 
		 * @param query
		 */
		public void setQuery(IQuery query) {

			// fix the query, remove the double quotes
			// query = query.replaceAll("^\"(.*)\"$","$1"); //$NON-NLS-1$
			assistedQueryWidget.setSearchEngine(query.getSearchEngine());
			assistedQueryWidget.setText(query.getQueryString());
			assistedQueryWidget.setQueryName(query.getName());
			assistedQueryWidget.setQueryOptions(query.getOptions());
		}

		public IQuery getQuery() {

			return assistedQueryWidget.getQuery();
		}

		public void memorize() {

			assistedQueryWidget.memorize();
		}

		public String getRawString() {

			return assistedQueryWidget.getRawString();
		}
	}

	public void clearQueries() {

		while (queryWidgets.size() > 0) {
			queryWidgets.get(0).deleteQuery();
		}
	}
}
