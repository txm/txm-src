// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.results;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.core.results.TXMResult;
import org.txm.objects.Project;
import org.txm.objects.Workspace;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.views.corpora.CorporaView;

/**
 * hide the intermediate parent results of the result
 */
public class HideIntermediateParents extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Object sel = this.getCorporaViewSelectedObject(event);
		if (sel instanceof TXMResult && !(sel instanceof Project) && !(sel instanceof Workspace)) {
			TXMResult result = (TXMResult) sel;
			TXMResult parent = result.getParent();

			int n = 0;
			while (parent != null && parent.isVisible() && !parent.isInternalPersistable() && !(sel instanceof Project) && !(sel instanceof Workspace)) {
				parent.setVisible(false);
				n++;
				parent = parent.getParent();
			}

			if (n > 0) {
				CorporaView.refresh();
			}
		}

		return null;
	}
}
