package org.txm.rcp.editors;

import org.eclipse.jface.viewers.TableViewer;

/**
 * An editor using one or several TableViewers
 * 
 * @author mdecorde
 *
 */
public interface TableResultEditor {

	public TableViewer[] getTableViewers();

	public TableKeyListener getTableKeyListener();
}
