package org.txm.rcp.views.corpora;

import org.eclipse.jface.viewers.DecoratingLabelProvider;
import org.eclipse.jface.viewers.ILabelDecorator;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IToolTipProvider;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.TableUtils;

public class CorpusViewLabelProvider extends DecoratingLabelProvider implements IToolTipProvider {

	public CorpusViewLabelProvider(ILabelProvider provider, ILabelDecorator decorator) {

		super(provider, decorator);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getText(Object element) { // cut label if too long

		String txt = super.getText(element);
		if (txt == null || txt.length() == 0) return "no_name";

		if (txt != null && txt.length() > TableUtils.MAX_NAME_LENGTH) {
			txt = txt.substring(0, TableUtils.MAX_NAME_LENGTH - 1) + "..."; //$NON-NLS-1$
		}

		return txt.replace("\n", " "); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Override
	public String getToolTipText(Object element) {

		if (element == null) return null;
		if (element instanceof TXMResult) return ((TXMResult) element).getDetails();
		return element.toString();
	}
}
