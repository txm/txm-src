package org.txm.rcp.editors.imports.sections;

import java.nio.charset.Charset;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.txm.objects.Project;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.txm.rcp.messages.TXMUIMessages;

public class EncodingSection extends ImportEditorSection {

	private static final int SECTION_SIZE = 1;

	Composite sectionClient;

	Button btnGuessEncoding;

	Button btnSelectEncoding;

	Combo encodingCombo;

	public EncodingSection(ImportFormEditor editor, FormToolkit toolkit2, ScrolledForm form2, Composite parent, int style) {
		super(editor, toolkit2, form2, parent, style, "encoding"); //$NON-NLS-1$

		this.section.setText(TXMUIMessages.sourceCharactersEncoding);
		TableWrapLayout layout = new TableWrapLayout();
		layout.makeColumnsEqualWidth = true;
		layout.numColumns = 1;
		this.section.setLayout(layout);
		this.section.setLayoutData(getSectionGridData(SECTION_SIZE));
		this.section.setEnabled(false);

		this.section.addExpansionListener(new ExpansionAdapter() {

			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.layout(true);
			}
		});

		// filesection.setDescription("Select how to find source files");
		sectionClient = toolkit.createComposite(this.section);
		TableWrapLayout slayout = new TableWrapLayout();
		slayout.numColumns = 2;
		sectionClient.setLayout(slayout);
		this.section.setClient(sectionClient);

		btnGuessEncoding = toolkit.createButton(sectionClient, "", SWT.RADIO); //$NON-NLS-1$
		TableWrapData td = getButtonLayoutData();
		btnGuessEncoding.setLayoutData(td);
		btnGuessEncoding.setText(TXMUIMessages.guessColon);
		toolkit.createLabel(sectionClient, ""); //$NON-NLS-1$

		btnSelectEncoding = toolkit.createButton(sectionClient, "", SWT.RADIO); //$NON-NLS-1$
		btnSelectEncoding.setLayoutData(getButtonLayoutData());
		btnSelectEncoding.setText(TXMUIMessages.selectColon);

		encodingCombo = new Combo(sectionClient, SWT.BORDER);
		TableWrapData gdata = getTextGridData();
		encodingCombo.setLayoutData(gdata);
		encodingCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				btnSelectEncoding.setSelection(true);
				btnGuessEncoding.setSelection(false);
			}
		});
		encodingCombo.removeAll();
		encodingCombo.setToolTipText(TXMUIMessages.AvailableSourceFilesEncodingCodeOnlyMostUsedAreShown);
	}

	String[] commonCharsets = { "UTF-8", "Windows-1252", "MacOSX", "x-MacRoman", "ISO-8859-1", "Windows-1251", "UTF-16", "UTF-32", "GB2312", "EUC-KR", "GBK", "ISO-8859-2", "EUC-JP", "Windows-1250", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$
			"ISO-8859-15", "Big5", "Windows-1256", "ISO-8859-9" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	@Override
	public void updateFields(Project project) {
		if (this.section.isDisposed()) return;
		if (project == null) return;
		// fill combo and select default encoding

		String dencoding = project.getEncoding();
		String defaultEncoding = System.getProperty("file.encoding"); //$NON-NLS-1$
		if ("??".equals(dencoding)) { //$NON-NLS-1$
			btnSelectEncoding.setSelection(false);
			btnGuessEncoding.setSelection(true);
		}
		else if (dencoding == null) {
			btnSelectEncoding.setSelection(true);
		}
		else {
			btnSelectEncoding.setSelection(true);
			defaultEncoding = dencoding;
		}

		// SortedMap<String, Charset> charsets = Charset.availableCharsets();
		// String[] charsets = new String[charsets.size()];
		// int icharset = 0;
		// for (String k : charsets.keySet()) items[icharset++] = k;
		ArrayList<String> availables = new ArrayList<>();
		for (String s : commonCharsets) {
			if (Charset.isSupported(s)) {
				availables.add(s);
			}
		}
		encodingCombo.setItems(availables.toArray(new String[availables.size()]));

		String[] data = encodingCombo.getItems();
		for (int i = 0; i < data.length; i++) {
			String encoding = data[i];
			if (encoding.equals(defaultEncoding)) {
				encodingCombo.select(i);
				break;
			}
		}
		encodingCombo.setText(defaultEncoding);
	}

	@Override
	public boolean saveFields(Project project) {
		if (this.section == null) return true;

		String encoding = project.getEncoding();
		if (!this.section.isDisposed()) {
			// ENCODING
			if (btnGuessEncoding != null && btnGuessEncoding.getSelection()) {
				encoding = "??"; //$NON-NLS-1$
			}
			else if (encodingCombo != null) {
				encoding = encodingCombo.getText();
			}

			if (encoding == null || encoding.length() == 0)
				encoding = System.getProperty("file.encoding"); //$NON-NLS-1$

			project.setEncoding(encoding);
		}
		return true;
	}

	@Override
	public boolean checkFields() {
		return true;
	}

	@Override
	public int getSectionSize() {
		return SECTION_SIZE;
	}
}
