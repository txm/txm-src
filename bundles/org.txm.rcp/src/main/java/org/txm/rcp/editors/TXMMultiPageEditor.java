// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.editors;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.txm.core.results.TXMResult;

/**
 * 
 * Multipage editor.
 * 
 * @author sjacquot
 * @author sloiseau
 */
//FIXME: SJ: this class leads to a lot of problem : focus, activation, etc. find a way to stop to use it. It actually used only for the CA at this moment.
@Deprecated
public abstract class TXMMultiPageEditor<T extends TXMResult> extends MultiPageEditorPart {

	/** The Constant ID. */
	public static final String ID = "org.txm.rcp.editors.GenericMultiPageEditor"; //$NON-NLS-1$

	/** The last editor used. */
	protected static int LAST_EDITOR_USED = 0;

	/** The editors. */
	protected List<EditorPart> editors;

	/** The inputs. */
	protected List<IEditorInput> inputs;

	/** The names. */
	protected List<String> names;

	/**
	 * A main editor can be defined and accessed if needed.
	 */
	protected EditorPart mainEditor;

	/**
	 * 
	 * @return the root composite of this multi paged editor
	 */
	public Composite getContainer() {
		return super.getContainer();
	}
	//
	//
	//	/* (non-Javadoc)
	//	 * @see org.eclipse.ui.part.MultiPageEditorPart#createPages()
	//	 */
	//	@Override
	//	protected void createPages() {
	//
	//		IEditorInput input = getEditorInput();
	//		if (!(input instanceof IProvidingMultiPageEditorInput)) {
	//			throw new IllegalArgumentException(NLS.bind(TXMUIMessages.theInputP0CannotBeDisplayed, input.getName()));
	//		}
	//
	//		editors = ((IProvidingMultiPageEditorInput) input).getEditors();
	//		inputs = ((IProvidingMultiPageEditorInput) input).getEditorInputs();
	//		names = ((IProvidingMultiPageEditorInput) input).getNames();
	//
	//		if (editors.size() != inputs.size()) {
	//			throw new IllegalArgumentException(NLS.bind(TXMUIMessages.theNumberOfEditorsP0AndNumberOfInputsP1MustBeEqual, editors.size(), inputs.size()));
	//		}
	//
	//		for (int i = 0; i < editors.size(); i++) {
	//
	//			int editorIndex = 0;
	//			try {
	//				
	//				editorIndex = addPage(editors.get(i), inputs.get(i));
	//			} catch (PartInitException e) {
	//				// TODO Auto-generated catch block
	//				org.txm.utils.logger.Log.printStackTrace(e);
	//			}
	//			this.setPageText(editorIndex, names.get(i));
	//		}
	//		this.setPartName(getEditorInput().getName());
	//
	//		this.setActivePage(LAST_EDITOR_USED);
	//	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#doSaveAs()
	 */
	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.MultiPageEditorPart#pageChange(int)
	 */
	@Override
	public void pageChange(int index) {
		super.pageChange(index);
		LAST_EDITOR_USED = index;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.MultiPageEditorPart#dispose()
	 */
	@Override
	public void dispose() {
		// FIXME: SJ: useless? the parent method should already do that, need to test furthermore
		for (int i = 0; i < editors.size(); i++) {
			editors.get(i).dispose();
		}
		super.dispose();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.MultiPageEditorPart#getAdapter(java.lang.Class)
	 */
	@Override
	@SuppressWarnings("unchecked") //$NON-NLS-1$
	public Object getAdapter(Class type) {
		return null;
	}

	/**
	 * @return the editors
	 */
	public List<EditorPart> getEditors() {
		return editors;
	}


	@Override
	public boolean isDirty() {
		// return true if one of the editors is dirty
		for (Iterator i = this.editors.iterator(); i.hasNext();) {
			IEditorPart editor = (IEditorPart) i.next();
			if (editor.isDirty()) {
				return true;
			}
		}
		return super.isDirty();
	}


	/**
	 * Gets the main editor if defined otherwise null.
	 * 
	 * @return the main editor if defined otherwise null
	 */
	public EditorPart getMainEditorPart() {
		return this.mainEditor;
	}


	/**
	 * Sets the part name.
	 */
	public void setPartName(String partName) {
		super.setPartName(partName);
	}
}
