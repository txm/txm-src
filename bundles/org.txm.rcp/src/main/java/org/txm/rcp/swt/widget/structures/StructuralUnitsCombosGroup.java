package org.txm.rcp.swt.widget.structures;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.editors.TXMEditor;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;

/**
 * A Widgets group that manages the Structural Unit and Structural Unit Property with two linked combo viewers.
 * 
 * @author sjacquot
 *
 */
public class StructuralUnitsCombosGroup extends Group {


	/**
	 * Structural Units.
	 */
	protected StructuralUnitsComboViewer structuralUnitsComboViewer;

	/**
	 * Structural Unit Properties.
	 */
	protected StructuralUnitPropertiesComboViewer structuralUnitPropertiesComboViewer;


	/**
	 * 
	 * @param parent
	 * @param style
	 * @param editor
	 * @param autoCompute
	 * @param selectedSU
	 * @param selectedSUP
	 * @param addEmptyEntries to add an empty blank entry at start of the lists to clear values.
	 */
	public StructuralUnitsCombosGroup(Composite parent, int style, TXMEditor editor, boolean autoCompute, StructuralUnit selectedSU, StructuralUnitProperty selectedSUP, boolean addEmptyEntries) {
		super(parent, style);

		this.setLayout(new GridLayout(2, false));
		this.setText(TXMCoreMessages.common_structuralUnitAndProperty);

		// Structural Unit combo box
		this.structuralUnitsComboViewer = new StructuralUnitsComboViewer(this, editor, autoCompute, selectedSU, addEmptyEntries);
		structuralUnitsComboViewer.getCombo().setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));
		// Listener to update SUPs
		this.structuralUnitsComboViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				structuralUnitPropertiesComboViewer.updateFromStructuralUnit(structuralUnitsComboViewer.getSelectedStructuralUnit());
			}
		});

		// Structural Unit Property combo box
		this.structuralUnitPropertiesComboViewer = new StructuralUnitPropertiesComboViewer(this, editor, autoCompute, selectedSU, selectedSUP, addEmptyEntries);
		structuralUnitPropertiesComboViewer.getCombo().setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));
	}

	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param autoCompute
	 * @param selectedSU
	 * @param selectedSUP
	 * @param addEmptyEntries
	 */
	public StructuralUnitsCombosGroup(Composite parent, TXMEditor editor, boolean autoCompute, StructuralUnit selectedSU, StructuralUnitProperty selectedSUP, boolean addEmptyEntries) {
		this(parent, SWT.NONE, editor, autoCompute, selectedSU, selectedSUP, addEmptyEntries);
	}

	public StructuralUnitsCombosGroup(Composite parent, TXMEditor editor, StructuralUnit selectedSU, StructuralUnitProperty selectedSUP, boolean addEmptyEntries) {
		this(parent, editor, true, selectedSU, selectedSUP, addEmptyEntries);
	}


	/**
	 * 
	 * @param editor
	 * @param autoCompute
	 * @param selectedSU
	 * @param selectedSUP
	 * @param parent
	 */
	public StructuralUnitsCombosGroup(Composite parent, TXMEditor editor, boolean autoCompute, StructuralUnit selectedSU, StructuralUnitProperty selectedSUP) {
		this(parent, editor, autoCompute, selectedSU, selectedSUP, true);
	}

	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param selectedSU
	 * @param selectedSUP
	 */
	public StructuralUnitsCombosGroup(Composite parent, TXMEditor editor, StructuralUnit selectedSU, StructuralUnitProperty selectedSUP) {
		this(parent, editor, true, selectedSU, selectedSUP);
	}

	/**
	 * @return the structuralUnitsComboViewer
	 */
	public StructuralUnitsComboViewer getStructuralUnitsComboViewer() {
		return structuralUnitsComboViewer;
	}


	/**
	 * @return the structuralUnitPropertiesComboViewer
	 */
	public StructuralUnitPropertiesComboViewer getStructuralUnitPropertiesComboViewer() {
		return structuralUnitPropertiesComboViewer;
	}

	@Override
	protected void checkSubclass() {
	}



}
