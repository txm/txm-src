// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.dialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.txm.rcp.messages.TXMUIMessages;

// TODO: Auto-generated Javadoc
/**
 * build a query with the help of simple questions @ author mdecorde.
 */
public class MultipleValueTreeDialog extends Dialog {

	/** The self. */
	MultipleValueTreeDialog self;

	ScrolledComposite scrollComposite;

	/** The main panel. */
	Composite mainPanel;

	/** The parent shell. */
	Shell parentShell;

	/** The values. */
	LinkedHashMap<String, String> values;

	private ArrayList<LinkedHashMap<String, String>> subvalues;

	HashMap<String, Text> widgets = new HashMap<String, Text>();

	ArrayList<HashMap<String, Text>> subwidgets = new ArrayList<HashMap<String, Text>>();

	/** The title. */
	private String title;

	private Composite partsComposite;

	private String itemSubName;

	/**
	 * Instantiates a new combo dialog.
	 *
	 * @param parentShell the parent shell
	 * @param title the title
	 * @param values the values
	 */
	public MultipleValueTreeDialog(Shell parentShell, String title, LinkedHashMap<String, String> values,
			ArrayList<LinkedHashMap<String, String>> subvalues, String itemSubName) {
		super(parentShell);
		this.parentShell = parentShell;
		this.setShellStyle(this.getShellStyle() | SWT.RESIZE);
		this.values = values;
		this.subvalues = subvalues;
		this.itemSubName = itemSubName;
		this.title = title;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(title);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(final Composite p) {
		//System.out.println(p.getLayout());

		for (String name : values.keySet()) {
			Label label = new Label(p, SWT.NONE);
			label.setText(name + ": "); //$NON-NLS-1$
			label.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));
			Text valueText = new Text(p, SWT.BORDER);
			valueText.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 4, 1)); // 5 ?
			valueText.setText(values.get(name));
			valueText.setToolTipText(name);
			widgets.put(name, valueText);
		}

		scrollComposite = new ScrolledComposite(p, SWT.V_SCROLL | SWT.BORDER);
		scrollComposite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		partsComposite = new Composite(scrollComposite, SWT.BORDER);
		partsComposite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		partsComposite.setLayout(new GridLayout(2, true));
		scrollComposite.setContent(partsComposite);
		scrollComposite.setExpandVertical(true);
		scrollComposite.setExpandHorizontal(true);
		//		scrollComposite.addControlListener(new ControlAdapter() {
		//			public void controlResized(ControlEvent e) {
		//				Rectangle r = scrollComposite.getClientArea();
		//				scrollComposite.setMinSize(partsComposite.computeSize(
		//						r.width, SWT.DEFAULT));
		//			}
		//		});

		Button addButton = new Button(p, SWT.PUSH);
		addButton.setText(TXMUIMessages.addColon + itemSubName);
		addButton.setText("Add a new value of "+itemSubName);
		addButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				LinkedHashMap<String, String> another = subvalues.get(0);
				LinkedHashMap<String, String> subs = new LinkedHashMap<String, String>();
				subvalues.add(subs);
				for (String key : another.keySet()) {
					subs.put(key, key + subvalues.size());
				}
				addSubValues(partsComposite, subs);
				System.out.println(subvalues);
				scrollComposite.layout(true);
				partsComposite.getShell().layout(true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});
		for (String key : subvalues.get(0).keySet()) {
			Label l = new Label(partsComposite, SWT.NONE);
			l.setText(key);
			l.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));
		}
		for (LinkedHashMap<String, String> subs : subvalues) {
			addSubValues(partsComposite, subs);
		}

		return p;
	}

	public void addSubValues(Composite parent, LinkedHashMap<String, String> subs) {
		HashMap<String, Text> texts = new HashMap<String, Text>();
		subwidgets.add(texts);

		for (String subname : subs.keySet()) {
			//			Label l = new Label(parent, SWT.NONE);
			//			l.setText("\t"+subname+": ");
			//			l.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false,	false));

			Text subValueText = new Text(parent, SWT.BORDER);
			subValueText.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 1, 1));
			subValueText.setText(subs.get(subname));
			subValueText.setText(subname);
			texts.put(subname, subValueText);
		}

		Rectangle r = parent.getClientArea();
		parent.setSize(parent.computeSize(r.width, SWT.DEFAULT));

		r = parent.getClientArea();
		scrollComposite.setMinSize(parent.computeSize(r.width,
				SWT.DEFAULT));
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		for (String name : values.keySet()) {
			values.put(name, widgets.get(name).getText());
		}
		for (int i = 0; i < subvalues.size(); i++) {
			HashMap<String, Text> texts = subwidgets.get(i);
			LinkedHashMap<String, String> subs = subvalues.get(i);
			for (String name : texts.keySet()) {
				subs.put(name, texts.get(name).getText());
			}
		}
		super.okPressed();
	}

	/**
	 * Gets the values.
	 *
	 * @return the values
	 */
	public HashMap<String, String> getValues() {
		return values;
	}
}
