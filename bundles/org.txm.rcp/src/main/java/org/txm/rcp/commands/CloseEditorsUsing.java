package org.txm.rcp.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

public class CloseEditorsUsing extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		return null;
	}

	public static int corpus(CQPCorpus corpus) {
		return result(corpus);
	}

	public static int result(TXMResult result) {

		int c = 0;

		for (IWorkbenchPage page : SWTEditorsUtils.getPages()) {
			for (IEditorReference editorref : page.getEditorReferences()) {
				IEditorPart editor = editorref.getEditor(false);

				if (editor instanceof TXMEditor) {
					// System.out.println("checking editor "+editor.getTitle());
					TXMEditor ceditor = ((TXMEditor) editor);
					if (result.equals(ceditor.getResult()))
						editor.getEditorSite().getPage().closeEditor(editor, true);
					else if (result.equals(ceditor.getResult().getParent())) {
						editor.getEditorSite().getPage().closeEditor(editor, true);
					}
				}
			}
		}

		return c;
	}
}
