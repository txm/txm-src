package org.txm.rcp.handlers.files;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.DownloadJobHandler;
import org.txm.utils.logger.Log;

public class DownloadFile extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// TODO Auto-generated method stub
		return null;
	}

	public static boolean download(URL url, File outputDirectory) {
		String filename = url.getFile();

		File outfile = new File(outputDirectory, new File(filename).getName());
		Boolean done = false;

		BufferedOutputStream writeFile = null;
		HttpURLConnection connection = null;
		InputStream input = null;
		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.connect();

			// Check if the request is handled successfully  
			if (connection.getResponseCode() / 100 == 2) {

				// size in bytes  
				final int contentLength = connection.getContentLength();

				if (contentLength == -1) {
					System.out.println("Could not get file size"); //$NON-NLS-1$
				}
				else {
					done = false;

					System.out.print(NLS.bind(TXMUIMessages.DownloadFileFromP0, url));

					DownloadJobHandler job = new DownloadJobHandler(TXMUIMessages.bind(TXMUIMessages.DownloadP0, url), url, outfile, contentLength, done);
					job.setUser(true);
					job.schedule();

					try {
						job.join();
					}
					catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					//					writeFile = new BufferedOutputStream(new FileOutputStream(outfile));
					//					input = connection.getInputStream();
					//					byte[] buffer = new byte[1024];
					//					int read;
					//					while ((read = input.read(buffer)) > 0) {
					//						writeFile.write(buffer, 0, read);
					//					}
					//
					//					writeFile.close();
					//					input.close();
					//					done = true;
					//					System.out.println("Done.");

				}
			}
		}
		catch (IOException e) {
			Log.printStackTrace(e);
			return false;
		}
		finally {
			if (writeFile != null)
				try {
					writeFile.close();
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (input != null)
				try {
					input.close();
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (connection != null) connection.disconnect();
		}
		return true;
	}
}
