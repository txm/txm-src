package org.txm.rcp.swt.dialog;

import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * An Inputdialog with a Text field of several lines
 * 
 * override getNumberOfLines to get more lines (default = 5)
 * 
 * @author mdecorde
 *
 */
public class LargeInputDialog extends InputDialog {

	public LargeInputDialog(Shell shell, String title, String message, String defaultValue, IInputValidator validator) {
		super(shell, title, message, defaultValue, validator);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Control result = super.createDialogArea(parent);

		Text text = getText();  // The input text
		GridData data = new GridData(SWT.FILL, SWT.TOP, true, false);
		data.heightHint = convertHeightInCharsToPixels(getNumberOfLines()); // number of rows 
		text.setLayoutData(data);

		return result;
	}
	
	/**
	 * 
	 * @return the number of lines to show in the Text field
	 */
	public int getNumberOfLines() {
		return 5;
	}

	@Override
	protected int getInputTextStyle() {
		return SWT.MULTI | SWT.BORDER;
	}
}
