// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands.editor;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.editors.text.IEncodingSupport;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.editors.TxtEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.SWTUtils;

/**
 * @author mdecorde
 *         Change the encoding of the currently selected text editor
 */
public class SetEncoding extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchPart page = HandlerUtil.getActiveWorkbenchWindow(event)
				.getActivePage().getActivePart();
		if (page != null && page instanceof TxtEditor) {
			TxtEditor te = (TxtEditor) page;
			setEncoding(te);
		}

		return null;
	}

	public static void setEncoding(TxtEditor te) {
		try {
			IEncodingSupport encodingSupport = te.getAdapter(IEncodingSupport.class);
			String currentEncoding = encodingSupport.getEncoding();
			String defaultEncoding = encodingSupport.getDefaultEncoding();
			if (currentEncoding == null) currentEncoding = defaultEncoding;
			Shell sh = SWTUtils.getShell(te);
			InputDialog dlg = new InputDialog(sh,
					TXMUIMessages.changeCharactersEncoding,
					TXMCoreMessages.bind(TXMUIMessages.changeCharactersEncodingCurrentEncodingColonCurrentP0DefaultP1, currentEncoding, defaultEncoding),
					currentEncoding, null);
			if (dlg.open() == Window.OK) {
				String newEncoding = dlg.getValue();
				if (newEncoding != null && newEncoding.trim().length() > 0 && !newEncoding.trim().equals(currentEncoding)) {
					encodingSupport.setEncoding(newEncoding);
				}
			}
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}
}
