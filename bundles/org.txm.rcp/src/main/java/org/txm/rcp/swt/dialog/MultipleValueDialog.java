// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.dialog;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * dialog with choices
 */
public class MultipleValueDialog extends Dialog {

	/** The self. */
	MultipleValueDialog self;

	/** The main panel. */
	Composite mainPanel;

	/** The parent shell. */
	Shell parentShell;

	/** The values. */
	LinkedHashMap<String, String> values;

	HashMap<String, Text> widgets = new HashMap<String, Text>();

	/** The title. */
	private String title;

	/**
	 * Instantiates a new combo dialog.
	 *
	 * @param parentShell the parent shell
	 * @param title the title
	 * @param values the values
	 */
	public MultipleValueDialog(Shell parentShell, String title, LinkedHashMap<String, String> values) {
		super(parentShell);
		this.parentShell = parentShell;
		this.setShellStyle(this.getShellStyle());
		this.values = values;
		this.title = title;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(title);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite p) {
		//System.out.println(p.getLayout());
		Object layout = p.getLayout();
		if (layout instanceof GridLayout) {
			GridLayout glayout = (GridLayout) layout;
			glayout.marginWidth = 5;
			glayout.marginTop = 5;
			glayout.verticalSpacing = 5;
		}

		for (String name : values.keySet()) {
			Label label = new Label(p, SWT.NONE);
			label.setText(name + ": "); //$NON-NLS-1$
			label.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));

			Text valueText = new Text(p, SWT.BORDER);
			valueText.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1)); // 5 ?
			valueText.setText(values.get(name));
			valueText.setToolTipText("Value of "+name);
			widgets.put(name, valueText);
		}
		return p;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		for (String name : values.keySet()) {
			values.put(name, widgets.get(name).getText());
		}
		super.okPressed();
	}

	/**
	 * Gets the values.
	 *
	 * @return the values
	 */
	public HashMap<String, String> getValues() {
		return values;
	}
}
