// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.widget.structures;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.widget.AssistedQueryWidget;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnit;

/**
 * The Class ComplexSubcorpusPanel.
 */
public class ContextsSubcorpusPanel extends Composite {

	/** The corpus. */
	CQPCorpus corpus;

	/** The query. */
	String query;

	/** The structuralunits. */
	List<String> structuralunits;

	/** The query field. */
	AssistedQueryWidget queryField;

	Combo structuralUnitCombo;

	private Spinner wordContextSize;

	/**
	 * Instantiates a new complex subcorpus panel.
	 *
	 * @param parent the parent
	 * @param style the style
	 * @param corpus the corpus
	 */
	public ContextsSubcorpusPanel(Composite parent, int style, CQPCorpus corpus) {

		super(parent, style);

		this.setLayout(new GridLayout(2, false));
		this.corpus = corpus;

		try {
			ArrayList<StructuralUnit> tmp = new ArrayList<>(corpus.getOrderedStructuralUnits());
			this.structuralunits = new ArrayList<String>();
			for (StructuralUnit su : tmp) {
				if (!(su.getName().equals("txmcorpus"))) { //$NON-NLS-1$
					this.structuralunits.add(su.getName());
				}
			}
		}
		catch (CqiClientException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}

		Label firstStep = new Label(this, SWT.NONE);
		firstStep.setText(TXMUIMessages.oneSetPivot);
		firstStep.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1));
		firstStep = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		firstStep.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1));

		Label l = new Label(this, SWT.NONE);
		l.setText(TXMUIMessages.query);

		queryField = new AssistedQueryWidget(this, SWT.BORDER, corpus, CQPSearchEngine.getEngine());
		queryField.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));

		Label secondStep = new Label(this, SWT.NONE);
		secondStep.setText(TXMUIMessages.twoSetContext); //"2- set context"
		secondStep.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1));
		secondStep = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		secondStep.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1));

		l = new Label(this, SWT.NONE);
		l.setText(TXMUIMessages.limits);

		structuralUnitCombo = new Combo(this, SWT.SINGLE | SWT.READ_ONLY);
		structuralUnitCombo.setItems(structuralunits.toArray(new String[structuralunits.size()]));
		structuralUnitCombo.add("<w>", 0); //$NON-NLS-1$

		structuralUnitCombo.setText("text"); //$NON-NLS-1$

		l = new Label(this, SWT.NONE);
		l.setText(TXMUIMessages.contextSize);

		wordContextSize = new Spinner(this, SWT.NONE);
		wordContextSize.setMinimum(1);
	}

	public void updateQueryField() {

		queryField.update();
	}

	/**
	 * Update display.
	 */
	public void updateDisplay() {
		this.layout();
	}

	/**
	 * Gets the query string.
	 *
	 * @return the generated query string
	 */
	public String getQueryString() {

		String q = queryField.getQuery().fixQuery(corpus.getLang()).getQueryString();
		if (!q.contains(" expand to ") &&  //$NON-NLS-1$
				(wordContextSize.getSelection() > 1 || !("<w>".equals(structuralUnitCombo.getText())))) {  //$NON-NLS-1$

			queryField.memorize();
			q += " expand to";  //$NON-NLS-1$

			if (wordContextSize.getSelection() > 1) {
				q += " " + wordContextSize.getSelection(); //$NON-NLS-1$
			}

			if (!("<w>".equals(structuralUnitCombo.getText()))) { //$NON-NLS-1$
				q += " " + structuralUnitCombo.getText(); //$NON-NLS-1$
			}
		}

		return q;
	}

	/**
	 * 
	 * @return the raw query without the context filter
	 */
	public String getQueryFieldValue() {

		String query = this.queryField.getQueryWidget().getText();
		if (query == null || query.length() == 0) {
			query = getQueryString();
		}
		return query;
	}

	public String getGeneratedName() {

		String s = queryField.getQueryString() + ">"; //$NON-NLS-1$
		if ("<w>".equals(structuralUnitCombo.getText())) { //$NON-NLS-1$

			if (wordContextSize.getSelection() > 1) {
				return "<" + wordContextSize.getSelection() + "w " + s; //$NON-NLS-1$
			}
			else {
				return "<w " + s; //$NON-NLS-1$
			}
		}
		else {
			if (wordContextSize.getSelection() > 1) {
				return "<" + wordContextSize.getSelection() + structuralUnitCombo.getText() + " " + s; //$NON-NLS-1$
			}
			else {
				return "<" + structuralUnitCombo.getText() + " " + s; //$NON-NLS-1$
			}
		}

	}
}
