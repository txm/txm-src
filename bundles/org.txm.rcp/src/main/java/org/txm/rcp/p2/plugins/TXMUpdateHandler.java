package org.txm.rcp.p2.plugins;

import static java.lang.System.setErr;
import static java.util.prefs.Preferences.systemRoot;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Arrays;
import java.util.Iterator;
import java.util.prefs.Preferences;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.equinox.internal.p2.engine.Profile;
import org.eclipse.equinox.internal.p2.engine.SimpleProfileRegistry;
import org.eclipse.equinox.internal.p2.ui.sdk.UpdateHandler;
import org.eclipse.equinox.p2.core.IProvisioningAgent;
import org.eclipse.equinox.p2.core.ProvisionException;
import org.eclipse.equinox.p2.engine.IProfile;
import org.eclipse.equinox.p2.engine.IProfileRegistry;
import org.eclipse.equinox.p2.metadata.IInstallableUnit;
import org.eclipse.equinox.p2.repository.artifact.IArtifactRepositoryManager;
import org.eclipse.equinox.p2.repository.metadata.IMetadataRepositoryManager;
import org.eclipse.equinox.p2.ui.ProvisioningUI;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.osgi.framework.Version;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.Activator;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.RCPPreferences;
import org.txm.utils.BundleUtils;
import org.txm.utils.logger.Log;
import org.txm.utils.zip.GZip;

public class TXMUpdateHandler extends UpdateHandler {

	public static final String ID = "org.txm.rcp.p2.plugins.TXMUpdateHandler"; //$NON-NLS-1$

	/**
	 * Execute the command.
	 */
	@Override
	public Object execute(ExecutionEvent event) {

		// // disabled since java is no more shipped within TXM feature file copy commands
		//		if (OSDetector.isFamilyWindows() && !isWindowsAdministratorUser()) {
		//			TXMMessageBox.show(HandlerUtil.getActiveShell(event), TXMUIMessages.updateWarning, TXMUIMessages.abortingWindowsUpdate, SWT.ICON_WARNING);
		//			Log.warning("Update canceled");
		//			return null;
		//		}

		String key = event.getParameter("org.txm.rcp.parameters.update"); //$NON-NLS-1$
		if ("true".equals(key)) { //$NON-NLS-1$
			String path = BundleUtils.getInstallDirectory();
			if (path != null) {
				try {
					File installDirectory = new File(path);
					Log.fine("Testing install directory rights: " + installDirectory); //$NON-NLS-1$
					if (!installDirectory.canWrite() || !installDirectory.canExecute()) {
						Log.warning(NLS.bind(
								TXMUIMessages.WarningYouNeedAdministratorPrivilegesToFullyUpdateTXMEtcP0InstalledEtc,
								installDirectory));
						return null;
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		// Hacking p2 profile to be able to update installation directory even if
		// -configuration is set

		ProvisioningUI pui = ProvisioningUI.getDefaultUI();
		IProvisioningAgent agent = pui.getSession().getProvisioningAgent();

		try {
			Log.info("Looking for TXM updates..."); //$NON-NLS-1$
			System.out.flush();
			// flush console ?
			//			org.eclipse.ui.console.IOConsole console = (IOConsole) (ConsolePlugin.getDefault().getConsoleManager().getConsoles())[0];
			//			ConsolePlugin.getDefault().getConsoleManager().refresh(console);

			String profileId = pui.getProfileId();


			if (agent != null) {
				IProfileRegistry profileRegistry = (IProfileRegistry) agent.getService(IProfileRegistry.SERVICE_NAME);

				if (profileRegistry != null && profileRegistry instanceof SimpleProfileRegistry) {

					SimpleProfileRegistry spr = (SimpleProfileRegistry) profileRegistry;

					IProfile iprofile = profileRegistry.getProfile(profileId);
					if (iprofile != null && iprofile instanceof Profile) {
						Profile profile = (Profile) iprofile;

						spr.lockProfile(profile);

						// System.out.println("Profile: "+profile.getClass()+" = "+profile);
						Iterator<IInstallableUnit> everything = profile.everything();
						boolean changed = false;
						while (everything.hasNext()) {
							IInstallableUnit iu = everything.next();
							String locked = profile.getInstallableUnitProperty(iu, IProfile.PROP_PROFILE_LOCKED_IU);

							if (locked != null && !("0".equals(locked))) { //$NON-NLS-1$
								profile.setInstallableUnitProperty(iu, IProfile.PROP_PROFILE_LOCKED_IU, "0"); //$NON-NLS-1$
								changed = true;
							}
						}
						if (changed) {
							spr.updateProfile(profile);
						}

						spr.unlockProfile(profile);
					}
				}
			}

			// this is the default repository
			if (!isUpdateSiteAvailable()) {
				return false;
			}
		}
		catch (Exception e) {
			Log.severe(TXMUIMessages.bind(TXMUIMessages.CouldNotUpdateTXMP0, e));
			Log.printStackTrace(e);
			return false;
		}

		addDefaultUpdateSites(agent);
		addDefaultPluginSites(agent);

		// add beta repos if needed
		// from bundle org.eclipse.equinox.p2.console

		Object ret = super.execute(event);
		Log.info(TXMUIMessages.Done);

		return ret;

	}

	public static boolean isUpdateSiteAvailable() {
		String baseURLString = TBXPreferences.getInstance().getString(TBXPreferences.UPDATESITE) + "index.html"; //$NON-NLS-1$
		try {
			URL baseURL = new URL(baseURLString);
			InputStream s = baseURL.openStream();
			s.close();
			return true;
		}
		catch (Exception e) {
			Log.warning(NLS.bind(TXMUIMessages.UpdateSiteIsNotReachableP0P1Aborting, e.getMessage(), baseURLString));
			return false;
		}
	}

	public static boolean isWindowsAdministratorUser() {
		String path = BundleUtils.getInstallDirectory();// System.getProperty("osgi.instance.area");
		if (!path.startsWith("C:/Program Files/TXM")) { //$NON-NLS-1$
			return true; // TXM was not installed in program files no need to test admin user
		}

		return isWindowsAdminCommandTest();
	}

	public static boolean isWindowsAdminCommandTest() {
		Preferences preferences = systemRoot();

		synchronized (System.err) {
			setErr(new PrintStream(new OutputStream() {

				@Override
				public void write(int b) throws IOException {
				}
			}));

			try {
				preferences.put("TxmAdminAccessTestProperty", "TxmAdminAccessTestValue"); // SecurityException on Windows //$NON-NLS-1$ //$NON-NLS-2$
				preferences.remove("TxmAdminAccessTestProperty"); //$NON-NLS-1$
				preferences.flush(); // BackingStoreException on Linux
				return true;
			}
			catch (Exception exception) {
				return false;
			}
			finally {
				setErr(System.err);
			}
		}
	}

	/**
	 * Return a shell appropriate for parenting dialogs of this handler.
	 * 
	 * @return a Shell
	 */
	@Override
	protected Shell getShell() {
		// System.out.println("MY GETSHELL: "+Display.getCurrent().getActiveShell());
		try {
			return Display.getCurrent().getActiveShell();
		}
		catch (Exception e) {
			Log.finer("Error: no active SWT shell available"); //$NON-NLS-1$
		}
		return new Shell();
	}

	public static boolean patchGZProfile() {
		// TODO: is this still useful with TXM 0.7.7 -> yes&no : need to patch when
		// --configuration is set, done by editing the Profile object directly
		String path = BundleUtils.getInstallDirectory();// System.getProperty("osgi.instance.area");
		URL location = null;
		try {
			location = new URL(path);
		}
		catch (MalformedURLException e) {
			Log.warning(NLS.bind(TXMUIMessages.switchLanguageColonMalformedUrlColonP0, location));
			return false;
		}

		File txmConfigDir = new File(location.getFile());
		// File txmConfigDir = new File("/home/mdecorde/.txm"); // test only
		File profileDir = new File(txmConfigDir, "p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile"); //$NON-NLS-1$

		Log.fine("Looking for profile in : " + txmConfigDir); //$NON-NLS-1$
		// System.out.println(txmConfigDir);
		// System.out.println(profileDir);

		if (!profileDir.exists()) {
			Log.warning("p2 profile directory does not exists: " + profileDir); //$NON-NLS-1$
			return false;
		}

		// 1- find profile.gz
		File gzFile = findNewestGZFile(profileDir);
		if (gzFile == null) {
			Log.severe("No profile GZ file found"); //$NON-NLS-1$
			return false;
		}
		else {
			try {
				// 2- extract profile from gz profile
				File profileXMLFile = GZip.uncompress(gzFile, profileDir);
				// 3- patch xml by removing lock properties
				patchProfile(profileXMLFile);
				// 4- delete old gz profile
				if (!gzFile.delete()) {
					Log.warning("Could not delete old profile GZfile: " + gzFile); //$NON-NLS-1$
					return false;
				}

				// File lockFile = new File(profileDir, ".lock");
				// System.out.println("Delete .lock file: "+lockFile+" "+lockFile.delete());

				// 5- compress new profile
				GZip.compress(profileXMLFile, gzFile);
				// 6- clean files
				if (!profileXMLFile.delete()) {
					Log.warning("Could not delete temporary profile XML file: " + profileXMLFile); //$NON-NLS-1$
					return false;
				}
				Log.fine("Profile archive patched: " + gzFile); //$NON-NLS-1$
				return true;
			}
			catch (IOException e) {
				Log.warning("Failed to fix profile file: " + e); //$NON-NLS-1$
				Log.printStackTrace(e);
				return false;
			}
		}
	}

	public static void addDefaultUpdateSites(IProvisioningAgent agent) {

		Version v = Activator.getDefault().getBundle().getVersion();
		String version = "" + v.getMajor() + "." + v.getMinor() + "." + v.getMicro(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		String uriBase = TBXPreferences.getInstance().getString(TBXPreferences.UPDATESITE) + "/" + version + "/main"; //$NON-NLS-1$ //$NON-NLS-2$
		String uriDefault = uriBase + "/stable"; //$NON-NLS-1$

		try {
			URI repoUriDefault = new URI(uriDefault);
			addURL(agent, repoUriDefault, "stable"); //$NON-NLS-1$
			// URI repoKepler = new URI("http://download.eclipse.org/releases/kepler");
			// addURL(agent, repoKepler);
		}
		catch (Exception e) {
			Log.warning(TXMUIMessages.CouldNotAddTheDefaultUpdateRepository + uriDefault);
			Log.printStackTrace(e);

			try {
				URI repoUriDefault = new URI(uriDefault);
				removeURL(agent, repoUriDefault);
			}
			catch (Exception e2) {

			}
		}

		String updateLevel = RCPPreferences.getInstance().getString(RCPPreferences.UPDATE_LEVEL);
		boolean betaMode = false;
		if ("BETA".equals(updateLevel)) {//$NON-NLS-1$
			betaMode = true;
		}
		Log.fine("Update mode: beta=" + betaMode); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		String uriBeta = uriBase + "/beta";//$NON-NLS-1$
		try {
			URI repoUriBeta = new URI(uriBeta);
			if (betaMode) {
				addURL(agent, repoUriBeta, "beta"); //$NON-NLS-1$
			}
			else {
				removeURL(agent, repoUriBeta);
			}
		}
		catch (Exception e) {
			Log.warning(TXMUIMessages.bind(TXMUIMessages.CouldNotAddTheP0UpdateRepositoryP1, uriBeta, e));
			Log.printStackTrace(e);

			try {
				URI repoUriBeta = new URI(uriBeta);
				removeURL(agent, repoUriBeta);
			}
			catch (Exception e2) {

			}
		}
	}

	public static void addDefaultPluginSites(IProvisioningAgent agent) {
		Version v = Activator.getDefault().getBundle().getVersion();
		String version = "" + v.getMajor() + "." + v.getMinor() + "." + v.getMicro(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		String uriBase = TBXPreferences.getInstance().getString(TBXPreferences.UPDATESITE) + "/" + version + "/ext"; //$NON-NLS-1$ //$NON-NLS-2$

		String uriDefault = uriBase + "/stable"; //$NON-NLS-1$
		try {
			URI repoUriDefault = new URI(uriDefault);
			addURL(agent, repoUriDefault, "stable"); //$NON-NLS-1$
			// URI repoKepler = new URI("http://download.eclipse.org/releases/kepler");
			// addURL(agent, repoKepler);
		}
		catch (Exception e) {
			Log.warning(TXMUIMessages.CouldNotAddTheDefaultExtensionRepository + uriDefault);
			Log.printStackTrace(e);

			try {
				URI repoUriDefault = new URI(uriDefault);
				removeURL(agent, repoUriDefault);
			}
			catch (Exception e2) {

			}
		}

		String updateLevel = RCPPreferences.getInstance().getString(RCPPreferences.UPDATE_LEVEL);
		boolean betaMode = false;
		if ("BETA".equals(updateLevel)) {//$NON-NLS-1$
			betaMode = true;
		}
		Log.fine("Update levels: beta=" + betaMode); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		String uriBeta = uriBase + "/beta";//$NON-NLS-1$
		try {
			URI repoUriBeta = new URI(uriBeta);
			if (betaMode) {
				addURL(agent, repoUriBeta, "beta"); //$NON-NLS-1$
			}
			else {
				removeURL(agent, repoUriBeta);
			}
		}
		catch (Exception e) {
			Log.warning(TXMUIMessages.bind(TXMUIMessages.CouldNotAddTheP0ExtensionsUpdateRepositoryP1, uriBeta, e));
			Log.printStackTrace(e);

			try {
				URI repoUriBeta = new URI(uriBeta);
				removeURL(agent, repoUriBeta);
			}
			catch (Exception e2) {

			}
		}
	}

	public static boolean addURL(IProvisioningAgent agent, URI location, String level) {
		IMetadataRepositoryManager manager = (IMetadataRepositoryManager) agent.getService(IMetadataRepositoryManager.SERVICE_NAME);
		IArtifactRepositoryManager manager2 = (IArtifactRepositoryManager) agent.getService(IArtifactRepositoryManager.SERVICE_NAME);

		if (manager == null) {
			System.out.println("No metadata repository manager found"); //$NON-NLS-1$
			return false;
		}
		if (manager2 == null) {
			System.out.println("No artifact repository manager found"); //$NON-NLS-1$
			return false;
		}

		Log.fine("Adding update site URL: " + location + "..."); //$NON-NLS-1$ //$NON-NLS-2$

		try {
			// manager.removeRepository(location)
			manager.loadRepository(location, null);
		}
		catch (Exception e) {
			Log.warning("No repo - create a newmetadata repo"); //$NON-NLS-1$
			// for convenience create and add a repository here
			String repositoryName = location + " - metadata"; //$NON-NLS-1$
			try {
				manager.createRepository(location, repositoryName, IMetadataRepositoryManager.TYPE_SIMPLE_REPOSITORY, null);
			}
			catch (ProvisionException e2) {
				Log.severe("Error while adding metadata repo: " + location); //$NON-NLS-1$
				Log.printStackTrace(e);
				return false;
			}
		}

		try {
			manager2.loadRepository(location, null);
			return true;
		}
		catch (Exception e) {
			// could not load a repo at that location so create one as a convenience
			String repositoryName = location + " - artifacts"; //$NON-NLS-1$
			try {
				Log.severe(NLS.bind("No {0} level update available.", level)); // Aucune mise à jour de niveau {0} n'est disponible. //$NON-NLS-1$
				manager.createRepository(location, repositoryName, IArtifactRepositoryManager.TYPE_SIMPLE_REPOSITORY, null);
				return true;
			}
			catch (Exception e2) {
				Log.severe("Error while adding artifact repo: " + location); //$NON-NLS-1$
				Log.printStackTrace(e);
				return false;
			}
		}
	}

	public static boolean removeURL(IProvisioningAgent agent, URI location) {

		IMetadataRepositoryManager manager = (IMetadataRepositoryManager) agent.getService(IMetadataRepositoryManager.SERVICE_NAME);
		IArtifactRepositoryManager manager2 = (IArtifactRepositoryManager) agent.getService(IArtifactRepositoryManager.SERVICE_NAME);

		if (manager == null) {
			Log.severe("No metadata repository manager found"); //$NON-NLS-1$
			return false;
		}
		if (manager2 == null) {
			Log.severe("No artifact repository manager found"); //$NON-NLS-1$
			return false;
		}

		manager.removeRepository(location);
		manager2.removeRepository(location);

		return true;
	}

	private static void patchProfile(File profileXMLFile) throws IOException {

		File tmpFile = File.createTempFile("tmp", ".profile", profileXMLFile.getParentFile()); //$NON-NLS-1$ //$NON-NLS-2$

		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(profileXMLFile), "UTF-8")); //$NON-NLS-1$
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tmpFile), "UTF-8")); //$NON-NLS-1$

		String line = br.readLine();
		boolean firstLine = true;
		while (line != null) {
			if (!firstLine) {
				writer.write("\n"); //$NON-NLS-1$
			}

			String trimedLine = line.trim();
			if (trimedLine.startsWith("<property name='org.eclipse.equinox.p2.type.lock' value='")) { //$NON-NLS-1$
				writer.write("<property name='org.eclipse.equinox.p2.type.lock' value='0'/>"); //$NON-NLS-1$
			}
			else {
				writer.write(line);
			}
			firstLine = false;
			line = br.readLine();
		}
		writer.close();
		br.close();

		if (!(profileXMLFile.delete() && tmpFile.renameTo(profileXMLFile))) {
			throw new IOException("failed to rewrite profile file: " + profileXMLFile); //$NON-NLS-1$
		}
	}

	private static File findNewestGZFile(File profileDir) {
		File[] files = profileDir.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".gz"); //$NON-NLS-1$
			}
		});

		if (files == null || files.length == 0)
			return null;

		Arrays.sort(files);
		return files[files.length - 1];
	}

	// COPY FROM UpdateHandler class
	// boolean hasNoRepos = false;
	// UpdateOperation operation;
	//
	// @Override
	// protected void doExecute(LoadMetadataRepositoryJob job) {
	// super.doExecute(job);
	//// //System.out.println("TXMUpdateHandler: doExecute(...)");
	//// if (hasNoRepos) {
	//// if (getProvisioningUI().getPolicy().getRepositoriesVisible()) {
	//// System.out.println(ProvSDKMessages.UpdateHandler_NoSitesMessage);
	//// }
	//// return;
	//// }
	//// // Report any missing repositories.
	//// job.reportAccumulatedStatus();
	//// if (continueWorkingWithOperation(operation, getShell())) {
	//// if (operation.getResolutionResult() == Status.OK_STATUS) {
	//// getProvisioningUI().openUpdateWizard(false, operation, job);
	//// } else {
	////
	//// final RemediationOperation remediationOperation = new
	// RemediationOperation(getProvisioningUI().getSession(),
	// operation.getProfileChangeRequest(),
	// RemedyConfig.getCheckForUpdateRemedyConfigs());
	//// ProvisioningJob job2 = new
	// ProvisioningJob(ProvSDKMessages.RemediationOperation_ResolveJobName,
	// getProvisioningUI().getSession()) {
	//// @Override
	//// public IStatus runModal(IProgressMonitor monitor) {
	//// monitor.beginTask(ProvSDKMessages.RemediationOperation_ResolveJobTask,
	// RemedyConfig.getAllRemedyConfigs().length);
	//// return remediationOperation.resolveModal(monitor);
	//// }
	//// };
	//// job2.addJobChangeListener(new JobChangeAdapter() {
	//// public void done(IJobChangeEvent event) {
	//// if (PlatformUI.isWorkbenchRunning()) {
	//// PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
	//// public void run() {
	//// getProvisioningUI().openUpdateWizard(true, operation, remediationOperation,
	// null);
	//// }
	//// });
	//// }
	//// }
	////
	//// });
	//// getProvisioningUI().schedule(job2, StatusManager.SHOW | StatusManager.LOG);
	//// }
	//// }
	// }
	//
	// /**
	// * Answer a boolean indicating whether the caller should continue to work with
	// the
	// * specified operation. This method is used when an operation has been
	// resolved, but
	// * the UI may have further restrictions on continuing with it.
	// *
	// * @param operation the operation in question. It must already be resolved.
	// * @param shell the shell to use for any interaction with the user
	// * @return <code>true</code> if processing of the operation should continue,
	// <code>false</code> if
	// * not. It is up to the implementor to report any errors to the user when
	// answering <code>false</code>.
	// */
	// public boolean continueWorkingWithOperation(ProfileChangeOperation operation,
	// Shell shell) {
	// //System.out.println("Test if any update available");
	// Assert.isTrue(operation.getResolutionResult() != null);
	// IStatus status = operation.getResolutionResult();
	// // user cancelled
	// if (status.getSeverity() == IStatus.CANCEL)
	// return false;
	//
	// // Special case those statuses where we would never want to open a wizard
	// if (status.getCode() == UpdateOperation.STATUS_NOTHING_TO_UPDATE) {
	// //ProvUI.reportStatus(status, StatusManager.BLOCK);
	// //System.out.println("No update available.");
	// return false;
	// }
	//
	// // there is no plan, so we can't continue. Report any reason found
	// if (operation.getProvisioningPlan() == null && !status.isOK()) {
	// StatusManager.getManager().handle(status, StatusManager.LOG |
	// StatusManager.SHOW);
	//
	// return false;
	// }
	//
	// // Allow the wizard to open otherwise.
	// return true;
	// }
	//
	// protected void doPostLoadBackgroundWork(IProgressMonitor monitor) throws
	// OperationCanceledException {
	// operation = getProvisioningUI().getUpdateOperation(null, null);
	// // check for updates
	// IStatus resolveStatus = operation.resolveModal(monitor);
	// if (resolveStatus.getSeverity() == IStatus.CANCEL)
	// throw new OperationCanceledException();
	// }
	//
	// protected boolean preloadRepositories() {
	// hasNoRepos = false;
	// RepositoryTracker repoMan = getProvisioningUI().getRepositoryTracker();
	// if (repoMan.getKnownRepositories(getProvisioningUI().getSession()).length ==
	// 0) {
	// hasNoRepos = true;
	// return false;
	// }
	// return super.preloadRepositories();
	// }
	//
	// @Override
	// protected String getProgressTaskName() {
	// return ProvSDKMessages.UpdateHandler_ProgressTaskName;
	// }

}
