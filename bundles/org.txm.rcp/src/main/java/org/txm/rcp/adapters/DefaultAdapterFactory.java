// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.rcp.adapters;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.txm.core.results.TXMResult;


/**
 * Default TXMResult adapter factory.
 * Essentially dedicated to debug purpose.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class DefaultAdapterFactory extends TXMResultAdapterFactory {


	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (this.canAdapt(adapterType)
				&& (adaptableObject instanceof TXMResult
				//				|| adaptableObject instanceof Edition
				//				|| adaptableObject instanceof Text
				//				|| adaptableObject instanceof SavedQuery

				)) {
			return new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(ISharedImages.IMG_ELCL_STOP_DISABLED);
				}

				@Override
				public String getLabel(Object result) {
					if (result instanceof TXMResult) {
						return ((TXMResult) result).getCurrentName() + " - " + result.getClass().getSimpleName(); //$NON-NLS-1$
					}
					else {
						return result.toString();
					}
				}


			};
		}
		return null;
	}


}
