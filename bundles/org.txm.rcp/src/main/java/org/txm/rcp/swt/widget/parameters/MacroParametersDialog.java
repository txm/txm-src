package org.txm.rcp.swt.widget.parameters;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;

import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.spi.OptionHandler;
import org.txm.Toolbox;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.utils.Timer;
import org.txm.utils.logger.Log;

public class MacroParametersDialog extends ParametersDialog2 {

	Object bean;

	File propFile;

	/**
	 * 
	 * @param parentShell
	 * @param bean
	 */
	public MacroParametersDialog(Shell parentShell, Object bean) {
		super(parentShell);

		if (bean != null) {
			this.bean = bean;
			CmdLineParser parser = new CmdLineParser(bean);
			List<OptionHandler> options = parser.getOptions();

			Object oArgs = null;
			if (bean instanceof groovy.lang.Script) {
				groovy.lang.Script script = (groovy.lang.Script) bean;

				// if the "args" variable exists then use its values. Can be used to set a default value by another Macro
				if (script.getBinding().hasVariable("args")) {
					oArgs = script.getBinding().getVariable("args"); //$NON-NLS-1$
				}
			}

			Map<String, Object> args;
			if (oArgs == null || !(oArgs instanceof Map)) {
				args = new LinkedHashMap<>();
			}
			else {
				args = (Map) oArgs;
			}

			if (bean instanceof groovy.lang.Script) { // create the args binding if not already existing
				groovy.lang.Script script = (groovy.lang.Script) bean;
				if (!script.getBinding().hasVariable("args")) {
					script.getBinding().setVariable("args", args);
				}
			}

			String script = bean.getClass().getName();

			int idx2 = script.lastIndexOf("Macro");
			if (idx2 < 0) idx2 = script.length();
			title = TXMUIMessages.bind(TXMUIMessages.p0ParametersInput, script.substring(script.lastIndexOf(".") + 1, idx2));

			// initialize the properties file default values store
			String scriptRootDir = Toolbox.getTxmHomePath() + "/scripts/groovy/user"; //$NON-NLS-1$

			propFile = new File(scriptRootDir, script.replace(".", "/") + ".properties");
			Properties defaultValuesProperties = loadDefaultValuesFromPropFile();
			for (Object k : defaultValuesProperties.keySet()) {
				if (!args.containsKey(k)) {
					args.put(k.toString(), defaultValuesProperties.get(k));
				}
			}

			init(options, args);
		}
	}


	public boolean saveDefaultParameters() {
		try {
			propFile.getParentFile().mkdirs();
			manager.finalToProperties().store(new FileWriter(propFile), "");
			return true;
		}
		catch (IOException e) {
			System.out.println("Warning: failed to store default macro valuesin " + propFile + ": " + e);
			Log.printStackTrace(e);
			return false;
		}
	}

	public Properties loadDefaultValuesFromPropFile() {
		Properties defaultValues = new Properties();
		if (propFile.exists()) {
			try {
				defaultValues.load(new FileReader(propFile));
				Log.fine("Retrieving previous macro parameters from " + propFile + " values=" + defaultValues.entrySet() + "...");
			}
			catch (Exception e) {
				Log.severe("Failed to load previous values from " + propFile + ": " + e);
				Log.printStackTrace(e);
			}
		}
		return defaultValues;
	}

	/**
	 * Conveniance method to initialize the parameters dialog.
	 * Can detect the "args" Groovy binding and use it to set variables
	 * If all variables are set by "args" then don't show the dialog box
	 * 
	 * @param bean
	 * @return true if the user has clicked on the "OK" button
	 */
	public static boolean open(final Object bean) {

		Callable<Boolean> r = new Callable<Boolean>() {

			Boolean called = null;

			boolean errorOpen = true;

			boolean retOpen = false;

			public boolean returnCode() {
				called = retOpen && !errorOpen;
				return called;
			}

			@Override
			public Boolean call() throws Exception {
				if (called != null) return called; // exit if already called

				MacroParametersDialog dialog = new MacroParametersDialog(Display.getCurrent().getActiveShell(), bean);
				if (dialog.getShell() != null) {
					dialog.getShell().setActive();
				}

				List<String> errors = dialog.columns.getManager().validateParametersDefinition();
				if (errors.size() > 0) {
					errorOpen = true;
				}
				else {
					if (dialog.getMustShowDialog()) {
						retOpen = dialog.open() == ParametersDialog2.OK;
					}
					else { // fields initialized by 'args'

						if (!dialog.initializeMacroVariables()) {
							Log.severe(NLS.bind("{0} fields not updated correctly with {1}", bean, dialog.getParameterManager().getFinalValues()));
							retOpen = false;
							return returnCode();
						}

						if (!dialog.saveDefaultParameters()) {
							Log.warning("Could not save parameters for next call");
						}

						retOpen = true;
						errorOpen = false;
					}

					if (bean instanceof groovy.lang.Script) { // reset the timer binding if already existing
						groovy.lang.Script script = (groovy.lang.Script) bean;
						if (script.getBinding().hasVariable("timer")) {
							Timer timer = (Timer) script.getBinding().getVariable("timer");
							timer.reset();
						}
					}
				}

				return returnCode();
			}
		};

		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				try {
					r.call();
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		try {
			return r.call();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * can use the args map to initialize variable not set with the dialog box
	 */
	protected boolean initializeMacroVariables() {

		// System.out.println("Initialize macro variables of "+fieldNames);
		if (bean == null) return false;

		return manager.finalToBean(bean);
	}
}
