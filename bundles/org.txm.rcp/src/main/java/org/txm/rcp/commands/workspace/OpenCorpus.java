package org.txm.rcp.commands.workspace;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.Toolbox;
import org.txm.objects.Project;
import org.txm.rcp.commands.RestartTXM;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;

public class OpenCorpus extends AbstractHandler {

	public static final String ID = OpenCorpus.class.getCanonicalName();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection sel = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		if (!(sel instanceof IStructuredSelection)) return null;
		IStructuredSelection selection = (IStructuredSelection) sel;

		Object s = selection.getFirstElement();
		if (!(s instanceof IProject))
			return null;
		IProject project = (IProject) s;
		open(project);

		return project;
	}

	public static JobHandler open(final IProject iproject) {

		if (iproject == null) return null;

		Display d = Display.getCurrent();
		if (d == null) {
			d = Display.getDefault();
		}

		JobHandler job = new JobHandler(TXMUIMessages.bind(TXMUIMessages.OpeningTheP0Corpus, iproject.getName())) {

			@Override
			protected IStatus _run(SubMonitor monitor) {

				Project project = null;
				for (Project p : Toolbox.workspace.getProjects()) {
					if (iproject.equals(p.getRCPProject())) {
						project = p;
						break;
					}
				}

				if (project == null) {
					if (!iproject.isOpen()) {
						try {
							iproject.open(monitor);
						}
						catch (CoreException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					project = Project.loadProjectFromProjectScope(iproject);
					project.open(monitor);
				}

				if (project.open(monitor)) {
					final Project project2 = project;
					this.syncExec(new Runnable() {

						@Override
						public void run() {
							RestartTXM.reloadViews();
							CorporaView.refresh();
							CorporaView.select(project2.getCorpora());
						}
					});
					return Status.OK_STATUS;
				}
				else {
					return Status.CANCEL_STATUS;
				}
			}
		};

		job.schedule();

		return job;
	}
}
