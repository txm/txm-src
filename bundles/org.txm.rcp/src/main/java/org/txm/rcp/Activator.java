// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp;

import java.io.BufferedReader;
import java.io.File;

import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.txm.utils.io.IOUtils;

/**
 * The activator class controls the plug-in life cycle.
 */
public class Activator extends AbstractUIPlugin {

	/** The Constant PLUGIN_ID. */
	public static final String PLUGIN_ID = "org.txm.rcp"; //$NON-NLS-1$

	/** The plugin. */
	private static Activator plugin;

	/**
	 * The constructor.
	 */
	public Activator() {
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);

		System.out.println("TXM ACTIVATED. ALL SYSTEMS GO!"); //$NON-NLS-1$
		//		try {
		//			preventBrokenTXMINI();
		//		} catch(Exception e) {
		//			System.out.println("Error while fixing TXM.ini file: "+e);
		//		}
	}

	//TODO: this method is temporary to fix the TXM.ini file -startup and --launcher.library properties
	private void preventBrokenTXMINI() throws Exception {

		File dataDir = new File(Platform.getInstanceLocation().getURL().getFile());
		File txmDir = dataDir.getParentFile();
		File TXMINI = new File(txmDir, "TXM.ini"); //$NON-NLS-1$
		if (TXMINI.exists()) {
			System.out.println("Fixing TXM.ini path=" + TXMINI); //$NON-NLS-1$
			BufferedReader reader = IOUtils.getReader(TXMINI, "UTF-8"); //$NON-NLS-1$
			String line = reader.readLine();
			StringBuilder content = new StringBuilder();
			boolean first = true;
			boolean found = false;
			while (line != null) { // skip jar lines
				if (line.startsWith("-startup")) {
//$NON-NLS-0$
					found = true;
				}
				else if (line.startsWith("--launcher.library")) {
//$NON-NLS-0$
					found = true;
				}
				else if (line.contains("plugins/org.eclipse.equinox.launcher")) {
//$NON-NLS-0$
					found = true;
				}
				else {
					if (!first) content.append("\n"); //$NON-NLS-1$
					content.append(line);
					first = false;
				}
				line = reader.readLine();
			}
			reader.close();

			//update the file only if the plugin configurations are found
			if (found) IOUtils.write(TXMINI, content.toString());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance.
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

}
