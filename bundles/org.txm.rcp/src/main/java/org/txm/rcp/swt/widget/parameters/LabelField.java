package org.txm.rcp.swt.widget.parameters;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.kohsuke.args4j.NamedOptionDef;

/**
 * Check button + Label
 * 
 * @author mdecorde
 *
 */
public abstract class LabelField extends ParameterField {

	Label l;

	public LabelField(Composite parent, int style, NamedOptionDef parameter) {
		super(parent, style, parameter);
		this.setLayout(new GridLayout(2, false));
		this.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));

		l = new Label(this, SWT.NONE);
		l.setAlignment(SWT.LEFT);
		l.setText(getWidgetLabel());
		l.setToolTipText(getWidgetUsage());
		GridData gd = new GridData(SWT.FILL, SWT.FILL, false, false);
		gd.minimumWidth = 100;
		//		gd.widthHint = 100;
		l.setLayoutData(gd);
	}

	public Object getWidgetValue() {
		return null;
	}

	@Override
	public abstract void setDefault(Object value);
}
