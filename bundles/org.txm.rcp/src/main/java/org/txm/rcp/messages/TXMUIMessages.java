
package org.txm.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

public class TXMUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.rcp.messages.messages"; //$NON-NLS-1$

	

	public static String navigation_tooltip_firstPage;
	public static String navigation_tooltip_previousPage;
	public static String navigation_tooltip_firstRowNumber;
	public static String navigation_tooltip_nextPage;
	public static String navigation_tooltip_lastPage;
	 
	public static String navigation2_tooltip_firstMatch;
	public static String navigation2_tooltip_previousMatch;
	public static String navigation2_tooltip_currentMatchNumber;
	public static String navigation2_tooltip_nextMatch;
	public static String navigation2_tooltip_lastMatch;
	
	public static String tableSearch_tooltip_searchWithinTheTable;
	public static String tableSearch_tooltip_regularExpressionOfTheValueToBeSearchedFor; // Regular expression of the value to be searched for
	public static String tableSearch_tooltip_startSearchInTable; // "Start search in table"
	
	public static String is;
	public static String isNot;
	public static String addANewCriterionAfterThisOne; //"Add a new criteria after this one"
	public static String removeThisCriterion; //"Remove criteria"
	public static String refreshTheQueryUsingParameters;// "Refresh the query using the parameters set"
	public static String allCriteriaMustMatch;// "All critera must match"
	public static String onlyOneCriterionIsEnoughToMatch;// "Only one critera is enough"
	public static String twoSetContext;
	public static String oneSetPivot;
	public static String limits;// "Limit"
	public static String valuesSeparator; // "Values separator"
	public static String theSeparatorIsInsertedBetweenTheValues; // "The separator is inserted between the values of the part names.\nWarning: spaces are not trimmed."
	public static String expandedStructure; // "Expanded structure"
	public static String defineTheLimitStructures; // "Define the limit structures. If not set, the last selected structure is used"
	public static String selectAtLeastTwoProperties ; // "Select at least two properties"
	public static String crossed;
	
	public static String theAvailableContexts;

	//MultiQueriesComposite
	public static String addQueriesFromFile;

	public static String noQueryDash;

	public static String spaceQueriesDash;

	public static String spaceQueryDash;

	public static String selectAnUTF8PropertyFile;

	public static String error_theP0QueryIsAlreadyRepresentedByACurveInTheGraphic;

	public static String theP0FileAlreadyExistsWouldYouLikeToReplaceIt;

	public static String theP0directoryNotABinaryCorpus;

	public static String P0CorpusLoaded;

	public static String P0ResultsImportedFromTheP1File;

	public static String EMPTY;

	public static String EmptyPropertyValuecode;

	public static String EnableCollapsibleMetadata;

	public static String EnterTheNewNameToShowInTheCorpusView;

	public static String ErrorCantUpdateTheCorpusNoTXMDirectoryFoundP0;

	public static String ErrorCantUpdateTheCorpusNoTXTSlashP0DirectoryFoundP1;

	public static String ErrorCantUpdateTheCorpusNoXMLFilesFoundInP0;

	public static String ErrorNoProjectLoaded;

	public static String ErrorTheAvailablePropertiesListIsNULL;

	public static String ErrorWhileSavingParameters;

	public static String ExecuteRTextSelectionNotImplemented;

	public static String Explorer_4;

	public static String Explorer_5;

	public static String Explorer_7;

	public static String ExportResultParameters_ParametersExportedToTheP0File;

	public static String ExportResultParameters_WarningOnlyTheParametersAreExportedManualChangesAreNotTransfered;

	public static String FailedToLoadTheP0Corpus;

	public static String FailedToOpenTheEditorNoContextGiven;

	public static String FileTreeContentProvider_4;

	public static String common_all;


	public static String withModifications;

	public static String Forward;

	public static String common_elementsPerPage;
	public static String common_numberOfElementsToDisplayPerPage;

	public static String _default;

	public static String _default_2;

	public static String aBinaryCorpusIsATXMFileExtensionTxmOrZip;

	public static String aWord;

	public static String aWordWithItsProperties;

	public static String aWordWithItsProperty;

	public static String abort;

	public static String abortingLoadingOfP0ACorpusWithTheSameNameAlreadyExistsInP1;

	public static String abortingLoadingOfP1ACorpusWithTheSameCQPIDP1AlreadyExists;


	public static String activateProxyService;

	public static String addAPropertyTest;


	public static String addAWord;

	public static String addAXsltParameter;

	public static String addColon;

	public static String addingToSelection;

	public static String advanced;


	public static String all;


	public static String allCriteria;


	public static String allPropertyTestsAreVerified;


	public static String allTheCorpusTextsWillBeUpdated;

	public static String alwaysSaveBeforeLaunching;

	public static String ampContextsDisplayOptions;

	public static String ampMultipleSort;

	public static String ampNameColon;

	public static String ampNew;

	public static String ampPropertyColon;

	public static String ampQuery;


	public static String ampSortOptions;

	public static String ampStructureColon;


	public static String ampValues;

	public static String andFrom;

	public static String lemmatizeTheCorpus;

	public static String areYouSureYouWantToDeleteP0Colon;

	public static String argsColonP0;

	public static String assign;

	public static String assisted;

	public static String automaticRecalculation;

	public static String automaticUpdates;

	public static String binaryCorpusIsNotATXM079CorpusNoImportXmlFile;

	public static String theP0BinaryCorpusIsNotATXM080CorpusNoSettingsNorProjectFile;

	public static String buildEdition;

	public static String callingGroovyPrestartScript;

	public static String callingGroovyStartScript;

	public static String canNotCreateATableWithTheProvidedInformationColonP0;

	public static String canceling;

	public static String cancelingP0;


	public static String canceling_2;

	public static String cannotAddPartP0IntoP1;


	public static String cannotAskForTheSimpleTabWhenTheUserHasSelectedTheAdvancedTab;

	public static String cannotComputeCooccurrencesWithTheCurrentSelectionP0;

	public static String cannotCreateALexicalTableFromAnIndexCreatedWithACorpusMoreThanOneColumnIsNeeded;

	public static String cannotDrawAGraphicWithTheInformationProvided;

	public static String cannotReadTheFileP0;


	public static String cannotSetIntegerFieldP0WithDefaultPropertiesValueP1;


	public static String cannotStartImportConfigurationNoSourceFolderSelected;

	public static String cantLoadPropertiesFromCorpusP0ColonP1;

	public static String cantOpenGraphicP0;

	public static String caseSensitive;

	public static String changeCharactersEncoding;

	public static String changeCharactersEncodingCurrentEncodingColonCurrentP0DefaultP1;

	public static String characterEncoding;

	public static String checkForUpdatesAtStartup;

	public static String checkingForExistingWorkspaceDirectory;


	public static String checkToAddAStructureConstraint;


	public static String checkToTargetThisWordInTheQuery;

	public static String chooseOutfile;

	public static String chooseTheLanguage;

	public static String clipboard;
	public static String clipboardImport;

	public static String clipboardIsEmpty;

	public static String clipboard_2;

	public static String closingColon;

	public static String closingEmptyEditors;

	public static String columnIndexIsTooBig;

	public static String columnsSeparatedBy;
	public static String characterToUseAsColumnSeparatorForCSVExports;
	public static String characterToUseAsColumnValueDelimiterForCSVExports;
	public static String theExportedResultIsDisplayedInANewWindow;
	public static String commands;


	public static String commaSeparatedList;

	public static String common_areYouSure;

	public static String common_beginning;

	public static String common_delete;


	public static String common_displayOptions;


	public static String common_end;

	public static String common_enterRegularExpressionOfYourSearch;

	public static String common_refresh;

	public static String completeNameAuthorDateLicenseComment;

	public static String complexSortColonP0;

	public static String computeSubCorpusOnP0QueryColonP1;

	public static String computeSubCorpusOnP0StructsColonP1AmpPropsP2ColonP3;



	public static String concordanceContextStructureLimits;


	public static String concordances;


	public static String concordancesSubSection;

	public static String configurationOfTheUserInterfaceLanguage;

	public static String confirm;

	public static String consoleReportsAnInternalError;

	public static String contains;

	public static String contextColon;

	public static String contextsDisplayOptions;

	public static String contexts;


	public static String contextSize;

	public static String coocMatrixColonP0;

	public static String coocMatrixParameters;

	public static String copiedLinesColonP0;


	public static String copyP0;


	public static String copyP0AndItsChildren;


	public static String corpus;

	public static String corpusConversionFailed;


	public static String corpusNameDoesNotMatchesTheAZAZ09120PatternColonP0;


	public static String corpusNameOnlyCapsAndNoDigitAtBeginning;


	public static String couldNotFindDirectoryColonP0;

	public static String couldNotFindTextP0InCorpusP1;


	public static String couldNotGetFileSize;

	public static String couldNotLaunchTheInternalWebBrowserYouMustSetThatParameterInThePreferencesMenuColonGeneralSupWebNavigator;


	public static String couldNotOpenTheParameterDialogP0;

	public static String couldNotReadInstallPreferencesFileP0;

	public static String couldNotStartProxyConfigurationColonP0;

	public static String couldNotUnzipBinaryCorpusColonP0;

	public static String createANewMacroFromATemplate;

	public static String createANewSorter;

	public static String createPartition;

	public static String createSubcorpus;

	public static String creatingANewPartitionWithP0;

	public static String creatingASubcorpusInTheSearchEngine;

	public static String creatingASubcorpusOnP0;

	public static String creatingTXMUserWorkingDirectory;

	public static String dDot;

	public static String dDotMandatoryField;

	public static String defaultLanguage;


	public static String defaultReferences;

	public static String defineAndUseACompositeSorter;

	public static String deleteParameter;


	public static String deleting;


	public static String deletingP0;

	public static String deletingSeveralResultsP0Continue;

	public static String deletingOneResultP0Continue;

	public static String description;

	public static String descriptionOfTheP0CorpusP1;


	public static String deselectAll;

	public static String desktopIsNotSupportedFatal;

	public static String disableTheWarningDialogWhenTestingTXM;


	public static String displayFont;

	public static String displayInFullText;

	public static String displayRuntimesInConsole;

	public static String distance;

	public static String wordsDistanceP0;

	public static String doNotConfirmToDeleteAnObject;

	public static String doesNotContain;

	public static String doneColonP0Created;

	public static String doneColonP0ItemsAndP1Occ;


	public static String done;


	public static String doneInP0;

	public static String doneInP0P1;

	public static String doneP0Ms;

	public static String doneTheCreatedBinaryFileIsP0;

	public static String dontSaveAndRun;

	public static String downloading;


	public static String downloadingTheP0File;

	public static String eRROR;

	public static String edit;

	public static String editTheNameOfTheNewColumnCreatedByTheMergedColumns;

	public static String editTheNameOfTheNewLineCreatedByTheMergedLines;

	public static String editXSLStylesheet;

	public static String editions;

	public static String elisionCharacters;

	public static String enableAutomaticSaveOfAllResultsPersistence;

	public static String endOfColumnMerge;

	public static String endOfSentenceCharacters;

	public static String endsWith;

	public static String equalsTo;

	public static String error;

	public static String errorColonCorporaDirectoryDoesntExistColon;

	public static String errorColonCurrentGroovyScriptDirectoryDoesNotExistColon;

	public static String errorColonDeleteReturnedFalse;

	public static String errorColonElementNotRegistered;

	public static String errorColonGetTextColonP0;

	public static String errorColonMissingValueColon;

	public static String errorColonObjectIsNotAnOrgw3cElement;

	public static String errorColonSubcorpusWasNotCreated;

	public static String errorColonThePartitionCreatedHasNoPart;

	public static String errorColonThePartitionWasNotCreated;

	public static String errorColonThisPartitionHasNoPart;

	public static String errorDuringParametersInitialisation;

	public static String errorDuringScriptExecutionColon;

	public static String errorDuringSortColonP0;

	public static String errorDuringUIInitializationP0;

	public static String errorElementColon;

	public static String errorInAtOptionNameDeclarationColon;



	public static String errorWhileComputingCorpusSummary;

	public static String errorWhileCreatingAPartitionColonP0;

	public static String errorWhileDeletingP0ColonP1;


	public static String errorWhileExportingQueriesP0;

	public static String errorWhileFetchingUpdatesP0;

	public static String errorWhileGettingCurrentPositionValueColon;


	public static String errorWhileGettingResultInformationsP0;

	public static String errorWhileLoadingCalculusP0P1;


	public static String errorWhileLoadingTXMP0;

	public static String errorWhileOpeningBibliographicRecordP0WithURLEqualsP1;

	public static String errorWhileOpeningFileP0;


	public static String errorWhileRunningScriptColonP0;

	public static String errorWhileSavingTokenizerParameters;

	public static String errorWhileUpdatingTXMP0;

	public static String errorWhileUpdatingTXMP0NoHandlerServiceFound;

	public static String errorWithElementP0;

	public static String error_invalidRegularExpressionP0;

	public static String errorsOccuredDuringTXMPostInstallation;

	public static String errorTheProvidedInstallDirectoryDoesNotExistAborting;

	public static String errorTheProvidedTXMHOMEDirectoryDoesNotExistsP0Aborting;


	public static String errorTheResultWasNotCloned;

	public static String executingP0;


	public static String executingGroovyScriptP0;

	public static String executingLastMacroColon;

	public static String executingLastScriptColon;

	public static String executionCanceled;

	public static String executionOfP0;


	public static String expertMode;


	public static String exportAll;

	public static String exportColonCantCreateFileP0ColonP1;

	public static String exportFileEncoding;
	public static String characterEncodingSystemToUseForExport;
	
	public static String exportSettings;

	public static String exporting;


	public static String exportingCalculusP0;

	public static String exportingP0;

	public static String exportingP0ToP1;

	public static String exportingP0ThisMayTakeAWhile;

	public static String exportingResults;

	public static String extractingBinaryCorpus;

	public static String failedToBackupExistingCorporaP0;

	public static String failedToCancel;

	public static String failedToCopy;

	public static String failedToDelete;

	public static String failedToDeleteSubcorpusP0;

	public static String failedToExecuteTheP0ScriptColonP1;

	public static String failedToExportCorpusP0;

	public static String failedToExportFileP0WithP1;

	public static String failedToExportInFileP0WithFormatP1;

	public static String failedToExportResultP0ColonP1;

	public static String failedToGetLastCQPErrorColon;

	public static String failedToLoadBinaryCorpusNullError;

	public static String failedToLoadCorpusFromDirectoryColonP0WeCannotFindTheNecessaryComponents;

	public static String failedToLoadCorpusP0;


	public static String failedToLoadPreviousValuesFromP0P1;


	public static String failedToRename;

	public static String failedToSaveAsP0ErrorP1;

	public static String failedToSavePreferencesColonP0;


	public static String failToMoveTheP0FileToThxTMPDirectoryP1;

	public static String fileColon;

	public static String fileExplorerColonIncorrectPattern;

	public static String folderP0ShouldNotBeDeleted;

	public static String followedBy;

	public static String fontName;

	public static String forceLoggin;

	public static String form;

	public static String P0LinesFound;

	public static String foundOnlyOneMatchOfP0;

	public static String foundP0MatchesOfP1;

	public static String foundP0Values;

	public static String from;

	public static String frontXSL;


	public static String generalPreferencesBackup;

	public static String goToParentFolder;

	public static String goToTXMHome;

	public static String goToTXMHome_2;


	public static String goToTXMHomeDirectory;

	public static String graphicsKeyboardShortcuts;


	public static String guessColon;

	public static String h;


	public static String identifiedLanguageP0;

	public static String imLookingForColon;

	public static String importClipboardText;

	public static String importParametersOfP0;

	public static String importSetupP0;

	public static String importStarted;

	public static String impossibleToReadP0;

	public static String includeTheKeywordStructureInTheCount;

	public static String info_txmIsReady;


	public static String initialisationErrorP0;


	public static String initializationOfThePlatform;

	public static String initializingPlatform;

	public static String initializingToolboxEnginesAndCorpora;


	public static String initialZoom;

	public static String installPathColonP0;

	public static String installingSampleCorpus;

	public static String integer;


	public static String invalidComparator;

	public static String invalidName;

	public static String invalidProperty;

	public static String invalidQuery;

	public static String isDifferentFrom;

	public static String jobP0;

	public static String leftContextSize;


	public static String lexicon;

	public static String linePerPageColon;

	public static String listOfMilestoneElementsSeparatedByComma;

	public static String listOfTagsEncodingTheOutsidetextNoStructureOrWordContentIndexedNoStructureOrWordContentEditedInTXM;

	public static String listOfTagsEncodingTheOutsidetextToEditNoStructureOrWordContentIndexedStructureAndWordContentEditedInTXM;


	public static String listOfValuesToSelect;

	public static String literal;

	public static String loadingBinaryCorpus;

	public static String loadingTXMInstallPreferencesP0;

	public static String loadingTheCorpusInThePlatform;

	public static String loadingTheP0BinaryCorpusAsATXM079Corpus;

	public static String theP0BinaryCorpusVersionIsP1;

	public static String loadingViews;


	public static String loadTheP0CorpusFile;

	public static String lockRCPWorkspace;

	public static String logInAFile;

	public static String logInTheConsole;

	public static String logLevel;

	public static String logOptionDetected;

	public static String logs;

	public static String macroName;

	public static String mainLanguage;

	public static String mainLanguageColon;

	public static String mainPage;

	public static String mandatoryFields;

	public static String match;

	public static String matchAll;

	public static String merge;

	public static String mergeDelete;

	public static String mergeResultName;

	public static String metadataColumnTitlesP0;

	public static String metadataPreview;

	public static String metadatacsvFileFormat;

	public static String metadatasParameters;

	public static String milestoneElements;

	public static String minAnd;

	public static String minimalCoFrequence;

	public static String moduleUIParametersColonP0;


	public static String movingDownTheSelection;

	public static String movingUpTheSelection;

	public static String msec;

	public static String name;

	public static String nameIsMandatory;

	public static String namesP0;

	public static String newCorpusColonP0;

	public static String newFileColonP0;

	public static String newFolderColonP0;

	public static String newMacro;

	public static String newName;

	public static String newParentTypeP0IsDifferentFromTheOldParentTypeP1;


	public static String newPart;

	public static String nextPage;

	public static String nextSessionNumber;

	public static String nextText;

	public static String noActivePage;

	public static String noActiveWorkbenchWindow;


	public static String noCharactersSelected;

	public static String noDefaultEditionPageFoundForElementColon;

	public static String noInterpreterFoundForScriptFileExtension;

	public static String noMatchFoundOfP0;

	public static String noMetadataFileP0;


	public static String noneMark;

	public static String noPartWasDefined;

	public static String noPartitionNameWasSpecifiedTheNameIsP0;

	public static String noPreviousText;

	public static String noSelectionForColon;

	public static String noSubcorpusNameWasSpecifiedWeveGeneratedOneP0;

	public static String noTextInCorpusColonP0;

	public static String noTextNext;

	public static String noteColonUseValueToGuess;

	public static String numberOfEdgesP0;


	public static String numberOfNodesP0;

	public static String objColon;


	public static String oneChooseCriteria;
	public static String combination;


	public static String onlyTheModifiedCorpusTextsAreUpdated;

	public static String openTheQueryAssistant;

	public static String openingComplexSortDialog;

	public static String openingCooccurrentsTable;

	public static String options;

	public static String outfileColonNone;

	public static String outputFile;

	public static String outsidetext;

	public static String outsidetextToedit;

	public static String P0Import;

	public static String p0ParametersInput;

	public static String p0ScriptFileDoesntExist;

	public static String p1ExportSavedInFileColonP0;

	public static String p1FileSavedInP0;

	public static String pageBreakTag;

	public static String paginationOptions;

	public static String panColonRightMousePlusDrag;

	public static String parameters;

	public static String part;

	public static String partInitializationErrorColon;

	public static String pleaseWriteTheMacroName;


	public static String previousPage;

	public static String previousText;

	public static String printTheStacktrace;

	public static String propertiesColon;


	public static String propertyToTest;

	public static String punctuations;

	public static String queriesCannotBeLeftEmpty;

	public static String queriesColon;

	public static String query;
	
	public static String queryDblDot;

	public static String queryAssistant;

	public static String queryCannotBeLeftEmpty;


	public static String queryOptions;


	public static String quitTXM;

	public static String ready;

	public static String readyToShowUpdateUI;


	public static String recoveringCorpora;

	public static String referencesAmpdisplayOptions;

	public static String referencesAmpsortOptions;

	public static String referencesDisplayOptions;

	public static String referencesSortOptions;

	public static String refreshingCorpora;


	public static String refreshTheQuery;

	public static String regexpPatternToHideFiles;
	public static String regexpPatternToFilterFilesInTheExplorerView;


	public static String regularExpressionToUse;

	public static String reload;

	public static String remove;

	public static String removeFromSelection;

	public static String removeTheLastPropertyTest;


	public static String removeTheLastWord;

	public static String removeUnnecessaryE4Elements;

	public static String removeUnusedEclipseRCPPreferencePage;


	public static String renameFile;

	public static String renamingByColon;

	public static String resetTheEditors;


	public static String resetTheViewColonF5;

	public static String restartTXM;

	public static String restartingThePlatform;

	public static String restoringCorpora;

	public static String restoringCorporaFromTheP0Directory;

	public static String rightContextSize;

	public static String rmvAllTheParts;

	public static String run;


	public static String sVGGraphic;

	public static String saveAndRun;

	public static String saveBeforeExecution;

	public static String saveFileBeforeExecution;


	public static String saveParameters;

	public static String saveScript;

	public static String saveScriptBeforeExecution;

	public static String savingImportParameters;

	public static String scriptExecution;

	public static String sec;

	public static String select;

	public static String selectACompositeSorter;


	public static String selectAll;

	public static String selectAProperty;

	public static String selectColon;

	public static String selectTextId;

	public static String selectTheSourceDirectory;

	public static String selectValuesToAssignColon;


	public static String selectValuesUsingARegularExpression;

	public static String selectionIsNotACorpusColon;


	public static String selectionMustBeACalculus;

	public static String separatedByAtLeast0Word;

	public static String separatedByAtLeast1Word;

	public static String separatorCharacters;

	public static String setDefaultTheme;

	public static String setImportParametersInTheSectionsBelow;

	public static String settingStatusLineAndConfiguringActionSets;

	public static String shouldNotHappen;

	public static String showAllFiles;


	public static String showAllResultNodesInCorporaView;

	public static String showExportResultInTheTextEditor;

	public static String showHiddenFiles;
	public static String showHiddenFilesOS;
	public static String showHideCommandParameters;


	public static String showTheModuleScripts;

	public static String simple;

	public static String someCriteria;

	public static String sort;

	public static String sortDone;

	public static String sortKeys;

	public static String sortOptions;

	public static String sortingColumn;

	public static String sourceCharactersEncoding;

	public static String sourceDirectoryColon;

	public static String sourcesCharactersEncodingColon;

	public static String spaces;

	public static String startCorpusImport;

	public static String startJobColonCheckTXMHOME;

	public static String startJobColonInitializeUI;

	public static String startJobColonLoadInstallPreferences;

	public static String startingTxm;

	public static String startingUpP0P1;

	public static String prettyVersionP0P1;

	public static String startingUpdateFetch;

	public static String startsWith;


	public static String startToImport;

	public static String startupFailedPleaseCheckTXMPreferencesInTheFileSupPreferencesMenuOrContactTxmusersMailingListMenuColonHelpSup;

	public static String string;

	public static String structuralUnitComboSelected;


	public static String structureColon;

	public static String structureFieldIsMandatory;

	public static String switchLanguageColonMalformedUrlColonP0;

	public static String systemOutput;

	public static String tXM;



	public static String tXMiniColon;


	public static String testToDo;


	public static String textEditorColonP0EncodingFoundP1;

	public static String textID;

	public static String textP0HasNoBibliographicRecord;

	public static String textSeparator;

	public static String textualPlanes;

	public static String theCorpusAndTheCorpusP0WillBeDeleted;


	public static String theDefaultReferencesDisplayedInTheEtc;

	public static String theErrorIsColon;

	public static String theFileP0DoesNotExist;

	public static String theInputP0CannotBeDisplayed;


	public static String theMaximumNumberOfColumnShownInTables;


	public static String theMaximumNumberOfQueryStoredInTheQueryHistory;

	public static String theNumberOfEditorsP0AndNumberOfInputsP1MustBeEqual;

	public static String theP0CorpusDirectoryAlreadyExistsDoYouWantToReplaceIt;

	public static String theP1GroovyScriptMustBeInTheP0Folder;

	public static String theTableMustContainAtLeastOneLine;

	public static String theTwoArraysMustHaveTheSameSize;

	public static String theUserCanceledTheScriptExecution;

	public static String thisComputerEncodingColon;

	public static String thisComputerLanguageColon;

	public static String thisDirectorySeemsToContainABinaryCorpus;

	public static String thisFileIsNotATXMFileColonP0;

	public static String thisFileIsNotMeantToRunTXMPleaseConsultTXMUserManual;


	public static String thisWillCloseAllEditors;


	public static String thisWillRestoreTheViewAndCloseEditors;

	public static String titleColon;

	public static String to;

	public static String to_2;

	public static String to_3;

	public static String tokenization;

	public static String tokenizerParameterFileColon;

	public static String toolboxReady;


	public static String tooltip_searchOptions;

	public static String tooltipProperties;

	public static String unknowedWidgetNameColon;


	public static String unmanagedElementPolicy;

	public static String updateCanceled;


	public static String updated;

	public static String updateLevel;

	public static String useLeftWindow;

	public static String useRightWindow;

	public static String userInterfaceLanguage;

	public static String usingP0Language;

	public static String valueSelector;

	public static String warning;


	public static String warningColonTheImportScriptIsMissingColonP0;

	public static String warning_popup_allDescendantResultsWillBeUpdated;

	public static String warning_popup_editedResultsWillBelostP0;

	public static String warning_popup_theP0HasBeenEditedItsChangesWillBeLost;


	public static String warningFailedToStoreDefaultMacroValuesAtP0;


	public static String warningImageNotFoundAtP0;

	public static String withinAContextOf;



	public static String wordsPerPage;

	public static String wordTag;


	public static String writingCoocMatrixInGRAPHMLFileColonP0;


	public static String youMustSelectAtLeast2Comparators;

	public static String zoomIn;


	public static String zoomInAmpoutColonMouseWheelORShiftPlusRightMousePlusDrag;


	public static String zoomOut;

	public static String zoomToSelectionColonCtrlPlusLeftMousePlusdrag;

	public static String theP0CorpusWillBeCreatedFromTheP1Directory;

	public static String theP1P0CorpusWillBeReplaced;

	public static String installingPreviousCorpusFromP0;

	public static String P0ImportWithP1;

	public static String sampleCorporaOfCurrentTXMP0;

	public static String corporaOfPreviousTXMP0;

	public static String corporaWillBeRestoredInTheFollowingOrderCorporaAlreadyRestoredWillNotBeReplaced;

	public static String selectThecorporaSetsToRestore;


	public static String selectTheImportSourceDirectory;

	public static String importDoneInP0;

	public static String importingAP0From;


	public static String importingTheCalculus;


	public static String updateDoneInP0;

	public static String gnuLicenceLink;

	public static String gnuLicenceName;

	public static String iAcceptTheLicenceAndAgreeToQuoteTXMInMyWork;


	public static String ignoreTheElementsAreNotWrittenEtc;

	public static String seeTheQuoteSectionIn;

	public static String TabPattern;


	public static String TheAnnotateImportParameterHasBeenActivatedSinceTreeTaggerIsInstalled;

	public static String TheAnnotateImportParameterWasNotActivatedSinceTreeTaggerIsNotInstalled;

	public static String TheCorpushasBeenDeletedSincePleaseReOpenTheImportForm;

	public static String TheCorpusSourceDirectory;

	public static String TheDownloadedFileIsNotATXMCorpus;

	public static String TheExportedObjectIsNotATXMResultResult;

	public static String TheP0ArchiveIsNotATXMCorpus;

	public static String ThereIsNotEnoughPercentMarksInP0ForTheP1Properties;

	public static String TheSelectedFileIsADirectory;

	public static String TheSourceDirectoryContainsTheSourcesFilesReadByTheImportModule;

	public static String ThisWillRestartTXM;

	public static String ToPopulateTthisMenuCreateMacrosInTheP0MacroDirectory;

	public static String Transcriptions;

	public static String OpenTestEditor_0;

	public static String WarningNoTXMDirectoryP0;

	public static String WarningNoXMLTXMFilesDirectoryP0;

	public static String WarningOnlyParametersAreClonedManualChangesAreNotTransfered;

	public static String WarningYouNeedAdministratorPrivilegesToFullyUpdateTXMEtcP0InstalledEtc;

	public static String XMLTXMFilesExportedInP0;

	public static String CrashLogsDirectory;

	public static String CreatePagesForSections;

	public static String IfNotCheckedTheEditionPagesAreNotUpdatedAndMightNotReflectThechanges;

	public static String ImageDirectory;

	public static String IndexInterviewerSpeech;

	public static String IndexTranscriberTranscriptionMetadata;

	public static String InstallationDirectory;

	public static String InstanceConfigurationDirectory;

	public static String InstanceConfigurationFile;

	public static String LogsDirectory;

	public static String Open;

	public static String OpenEtc;

	public static String OpeningCredentialDialog;

	public static String OpeningTheP0Corpus;

	public static String OpenTheSelectedPathWithTheDefaultToolSetByTheSystem;

	public static String Options;

	public static String Paginate;

	public static String Paths;


	public static String TXMCommonPathListClickOnTheOpenButtonEtc;

	public static String TXMPaths;

	public static String TXMUserHomeDirectory;

	public static String What;


	public static String TRSSection_1;

	public static String TXMCanNotFetchUpdatesP0;

	public static String TXMInitializationHasBeenCanceledByUserDotPleaseRestartTXM;

	public static String TXMInstallDirectoryIsNotSetP0PreferenceIsNotSet;

	public static String TXMInstallDirectoryIsSetToP0ButDoesNotExist;

	public static String TXMInstallDirectoryIsSetToP0ButIsNotReadable;

	public static String TXMInstallDirectoryIsSetToP0ButTXMCantCdInThisDirectory;

	public static String TXMInstallDirectoryIsSetToQuoteQuote;

	public static String TXMisDistributedUnderLicence;

	public static String TXMUserDirectoryIsNotSetP0PreferenceIsNotSet;

	public static String TXMUserDirectoryIsSetToP0;

	public static String TXMUserDirectoryIsSetToP0ButDoesNotExist;

	public static String TXMUserDirectoryIsSetToP0ButTXMCantCdInThisDirectory;

	public static String TXMUserDirectoryIsSetToP0ButTXMCantWriteInThisDirectory;

	public static String TXMWasNotCorrectlyStopped;

	public static String TXMWasNotRestartedNormally;

	public static String TXTStartingError;

	public static String selectWorkingDirectory;

	public static String firstLaunchOfTXMP0;

	public static String LoadBinaryCorporaDirectory_aCorpusNamedP0alreadyExistsLoadingOfP1Canceled;

	public static String LoadBinaryCorporaDirectory_corporaDirectoryNotFoundAborting;

	public static String LoadBinaryCorporaDirectory_corpusLoad;

	public static String LoadBinaryCorporaDirectory_loadingCorporaFromTheP0Directory;

	public static String LoadBinaryCorporaDirectory_selectTheTXMCorpusToLoadInP0;

	public static String selectAnotherTXMCorporaToRestore;


	public static String selectBetweenTheFollowingValues;

	public static String resetAll;

	public static String resetPerspective;

	public static String resetValues;

	public static String retokenizePreEncodedWords;

	public static String password;

	public static String pastingP0ToP1;
	
	public static String movingP0ToP1;

	public static String login;

	public static String connect;

	public static String loginToP0;

	public static String SelectADirectory;

	public static String SelectionHasNoFileAborting;

	public static String SelectionMustBeACorpusAborting;

	public static String SelectionP0MustBeAcorpusAborting0;

	public static String SelectTheImportLanguageToUse;

	public static String SelectTheSourceDirectory;

	public static String ShowADialogBoxWhenASevereErrorOccurs;

	public static String ShowMoreUpdateParametersBeforeUpdating;

	public static String SomeAnnotationsAreNotSaved;

	public static String SomeAnnotationsAreNotSavedAndWillBeLostAfterTXMIsClosed;

	public static String SomeFieldsAreNotCorrectlyFilled;

	public static String SomeResultHaveBeenMannuallyModified;

	public static String SomeResultHaveBeenManuallyModified;

	public static String SourceDirectory;

	public static String SourceDirectoryPage_0;

	public static String SourceDirectoryPage_1;

	public static String SourceDirectoryPage_10;

	public static String theP0CorpusWillBeReplaced;

	public static String theP0CorpusWillBeRestarted;

	public static String theP0CorpusWillBeCreated;


	public static String theWordPropertyValue;

	public static String SourceDirectoryPage_14;

	public static String SourceDirectoryPage_2;

	public static String SourceDirectoryPage_3;

	public static String SourceDirectoryPage_5;

	public static String SourceDirectoryPage_6;

	public static String SourceDirectoryPage_8;

	public static String Sources;

	public static String StartCorpusUpdate;

	public static String Structures;

	public static String updateWarning;

	public static String abortingWindowsUpdate;

	public static String AllEditorsUsingThisCorpusWillBeClosedandtheCorpusWillBeUpdatedContinue;

	public static String AnImportDotXMLImportConfigurationFileWasFoundRestoringImportParametersUsingThisfile;

	public static String AnnotationsEngines;

	public static String AskTheParametersWhenImportingFromAClipboard;

	public static String AutomaticallyUpdateSubResults;

	public static String AvailableSourceFilesEncodingCodeOnlyMostUsedAreShown;

	public static String Back;

	public static String BuildFacsEdition;

	public static String BuildWordIdentifiers;

	public static String Cancel;

	public static String CancelCredentialSettings;

	public static String CanNotDeleteP0;

	public static String ChangingTheWorkingDirectory;

	public static String ClickToSelectTheCorpusSourceDirectory;

	public static String Closing;

	public static String ClosingTheP0Corpus;

	public static String CommonSourcesLangOfTreeTaggerModels;

	public static String CompilingP0Etc;

	public static String ContinueAndLostTheUnsavedAnnotations;

	public static String ConvertingEtc;

	public static String CopingFilesP0;

	public static String Copy;

	public static String CouldNotAddTheDefaultExtensionRepository;

	public static String CouldNotAddTheDefaultUpdateRepository;

	public static String CouldNotAddTheP0ExtensionsUpdateRepositoryP1;

	public static String CouldNotAddTheP0UpdateRepositoryP1;

	public static String CouldNotCreateTheGrrovyScriptEngineAborting;

	public static String CouldNotLoadThecorpusFromtheP0DirectoryP1;

	public static String CouldNotUpdateTXMP0;

	public static String CQPStructurePropertiesProjection;

	public static String DashImportedByP0;

	public static String DefaultEdition;

	public static String Deleting;

	public static String Done;

	public static String DoneP0;

	public static String DontAskAgain;

	public static String DownloadFileFromP0;

	public static String DownloadP0;

	public static String DoYouWantToStartADiagnosticOfYourCorporaEtcP0FileEtc;

	public static String NoCommandFoundWithNameP0;


	public static String NoGroovyEntryPointScriptSelectedAborted;

	public static String Note;

	public static String NoteElements;

	public static String NoteElementsThatEncodePonctualNotes;

	public static String pattern;

	public static String performWordSegmentationWithinWord;

	public static String xmlTagNameThatEncodesWords;

	public static String numberOfLines;

	public static String tabulation;


	public static String tabulationMark;

	public static String nothing;


	public static String nothingToPaste;

	public static String OK;

	public static String OKOpenTheDiagnosticFile;

	public static String OnlyATXMResultCanBeCloned;

	public static String Reload;

	public static String RemoveTemporarydirectories;

	public static String RenameP0;

	public static String RestartingTXM;

	public static String UpdateCorpus;

	public static String UpdateEditionsWhenUpdatingACorpus;

	public static String UpdateP0P1Import;

	public static String UpdateParametersForTheP0ImportModule;

	public static String UpdateSiteAddress;

	public static String UpdateSiteIsNotReachableP0P1Aborting;

	public static String UpdateTheCorpusFromItsXMLTXMFiles;

	public static String UpdateTheEditionPages;

	public static String UpdateTheP0CorpusUsingP1;

	public static String UpdateTheP0Corpus;

	public static String UseThisToChangeTheCorpusParametersBeforeUpdatingTheCorpus;

	public static String WaitForAllJobsToEndAndRetry;

	public static String WhatLanguageMustBeUsedToImportTheClipboardContent;


	public static String Words;

	public static String WorkspacePathCouldNotBeFound;

	public static String YouNeedToSetAsManyPercentSPrintfMarksAsSelectedProperties;

	public static String YouReGoingToLostThoseModificationsIfNotAlreadyExportedContinue;

	public static String open;


	public static String openingP0etc;

	public static String corpusToProcess;


	public static String cutP0;


	public static String cutP0AndItsChildren;

	public static String textsToProcess;

	public static String pageProperties;

	public static String textLanguage;


	public static String twoGenerateTheQuery;


	public static String txmWontDeleteFilesOutOfTXMWorkingDirectory;

	public static String selectEngine;


	public static String selectUsingAListOfValues;

	public static String PropertiesOfTheCompoundFormForStructurePropertiesAndOfTheSimpleFormForWordProperties;

	public static String youAreUsingAnAlphaBetaVersionOfTXMItIsNotRecommendedToUseItForYourWork;

	public static String memorySize;



	public static String exportOftheP0P1Result; // "Export of the {0} {1} result"
	public static String filePath; // Filepath
	public static String filePathInWhichTheResultExportWillBeDone;
	public static String format;
	public static String formatWillDetermineWhatYouCanDoWithYourExport;
	public static String display;
	public static String queryNameDisplayedInResultInsteadOfTheExactQuery;
	public static String queryForTheP0SearchEngine;



	public static String thePropertiesFileContainsOneQueryPerLineFormattedWithAKeyPlusaQuery;



	public static String askBeforeDeletingAnObject;



	public static String startTypingPartOfTheValueToOpenTheValueListOfP0;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, TXMUIMessages.class);
	}

	private TXMUIMessages() {
	}
}
