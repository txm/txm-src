// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.widget;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.cqp.corpus.CQPCorpus;


/**
 * the QueryWidget plus the button to open the QueryAssisDialog @ author
 * mdecorde.
 */
public class NamedAssistedQueryWidget extends Composite {

	/** The magicstick. */
	Button magicstick;

	/** The querywidget. */
	AssistedChoiceQueryWidget querywidget;

	Text nameField;

	/**
	 * Instantiates a new assisted query widget.
	 *
	 * @param parent the parent
	 * @param style the style
	 * @param corpus the corpus
	 */
	public NamedAssistedQueryWidget(Composite parent, int style, final CQPCorpus corpus) {
		super(parent, SWT.NONE);
		this.setLayout(new GridLayout(5, false));

		Label l = new Label(this, SWT.NONE);
		l.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, true));
		l.setText("Name: "); //$NON-NLS-1$

		nameField = new Text(this, SWT.BORDER);
		nameField.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, true));

		querywidget = new AssistedChoiceQueryWidget(this, SWT.NONE, corpus);
		querywidget.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, true));
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Control#addKeyListener(org.eclipse.swt.events.KeyListener)
	 */
	@Override
	public void addKeyListener(KeyListener listener) {
		this.querywidget.addKeyListener(listener);
	}

	/**
	 * Gets the query string.
	 *
	 * @return the query string
	 */
	public String getQueryString() {
		return querywidget.getQueryString();
	}

	/**
	 * Gets the query string.
	 *
	 * @return the query string
	 */
	public String getQueryName() {
		return nameField.getText();
	}

	/**
	 * Memorize.
	 */
	public void memorize() {
		querywidget.getQueryWidget().memorize(getQuery());
	}

	public IQuery getQuery() {
		IQuery q = querywidget.getQuery();
		q.setName(getQueryName());
		return q;
	}

	public void setEngine(String name) {
		querywidget.setSearchEngine(name);
	}
//	/**
//	 * Memorize.
//	 *
//	 * @param query the query
//	 */
//	public void memorize(String query) {
//		querywidget.memorize(query);
//	}

	/**
	 * Sets the text.
	 *
	 * @param query the new text
	 */
	public void setText(String query) {
		querywidget.setText(query);
	}

	public void focus() {
		querywidget.forceFocus();
	}

	public void setName(String string) {
		nameField.setText(string);
	}
	
	public void clearQuery() {
		querywidget.clearQuery();
		nameField.setText("");
	}
}
