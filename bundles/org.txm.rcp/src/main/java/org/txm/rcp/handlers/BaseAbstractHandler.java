package org.txm.rcp.handlers;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.IParameter;
import org.eclipse.core.commands.IParameterValues;
import org.eclipse.core.commands.ParameterValuesException;
import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.contexts.IContextActivation;
import org.eclipse.ui.contexts.IContextService;
import org.eclipse.ui.handlers.IHandlerService;
import org.txm.Toolbox;
import org.txm.core.engines.EngineType;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.TXMResult;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.statsengine.core.messages.StatsEngineCoreMessages;
import org.txm.utils.logger.Log;

/**
 * Base command abstract handler.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public abstract class BaseAbstractHandler extends AbstractHandler {


	/**
	 * Dedicated to tell to the target command it's executed from a "send to" command.
	 */
	public final static String SEND_SELECTION_TO_COMMAND_CONTEXT_ID = "sendSelectionToContext"; //$NON-NLS-1$



	/**
	 * Default constructor.
	 */
	public BaseAbstractHandler() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Checks the Statistics Engine running status and displays a message if it's not running.
	 * 
	 * @return true if the stat engine is available
	 */
	public boolean checkStatsEngine() {
		if (!Toolbox.getEngineManager(EngineType.STATS).isCurrentEngineAvailable()) {
			Log.warning(StatsEngineCoreMessages.error_statsEngineNotReadyCancelingCommand);
			return false;
		}
		return true;
	}


	/**
	 * Checks the Corpus Engine running status and displays a message if it's not running.
	 * 
	 * @return true if the corpus engine is available
	 */
	public boolean checkCorpusEngine() {
		if (!CQPSearchEngine.isInitialized()) {
			Log.warning(StatsEngineCoreMessages.CorpusEngineIsNotReadyCancelingCommand);
			return false;
		}
		return true;
	}

	/**
	 * Gets the current selected object.
	 * 
	 * @param event the ExecutionEvent that provoke the command call.
	 * @return the corpora view current selection
	 */
	public IStructuredSelection getCorporaViewSelection(ExecutionEvent event) {

		// FIXME: SJ: check this, because it may be too linked to the Corpora view
		return (IStructuredSelection) (CorporaView.getInstance().getTreeViewer().getSelection());
		//		// SJ: otherwise use this code
		//		// MD: yeah but selection might be tricky
		//		// Object o = (IStructuredSelection) HandlerUtil.getCurrentSelection(event).getFirstElement();
		//		if (o == null) {
		//			Log.severe("BaseAbstractHandler.getCorporaViewSelectedObject(): current selection is null.");
		//		}
		//		
		//		Log.fine("BaseAbstractHandler.getCorporaViewSelectedObject(): selected object = " + o);
		//		
		//		return o;
	}

	/**
	 * Gets the current selected object.
	 * 
	 * @param event the ExecutionEvent that provoke the command call.
	 * @return the corpora view current selected object
	 */
	public Object getCorporaViewSelectedObject(ExecutionEvent event) {

		// FIXME: SJ: check this, because it may be too linked to the Corpora view
		Object o = CorporaView.getFirstSelectedObject();
		// SJ: otherwise use this code
		// MD: yeah but selection might be tricky
		// Object o = (IStructuredSelection) HandlerUtil.getCurrentSelection(event).getFirstElement();
		if (o == null) {
			Log.fine("BaseAbstractHandler.getCorporaViewSelectedObject(): current selection is null."); //$NON-NLS-1$
			return null;
		}

		Log.fine("BaseAbstractHandler.getCorporaViewSelectedObject(): selected object = " + o); //$NON-NLS-1$

		return o;
	}

	/**
	 * Gets the current selected objects.
	 * 
	 * @param event
	 * @return
	 */
	public List<?> getCorporaViewSelectedObjects(ExecutionEvent event) {
		// FIXME: SJ: check this, because it may be too linked to the Corpora view
		return CorporaView.getSelectedObjects();
	}

	public static IViewPart getView(ExecutionEvent event, String id) throws PartInitException {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		return page.showView(id);
	}

	/**
	 * Gets the result stored in the current active editor.
	 * 
	 * @param event
	 * @return
	 */
	public static TXMResult getActiveEditorResult(ExecutionEvent event) {
		try {
			return SWTEditorsUtils.getActiveEditor(event).getResult();
		}
		catch (Exception e) {
			return null;
		}
	}

	/**
	 * Gets the parent of the result stored in the active editor.
	 * This parent will be send to the target command as parent of the new result.
	 * This method is essentially dedicated to be overridden, if needed, to define a parent that is not the direct parent (eg. the corpus parent instead of the partition parent).
	 * 
	 * @param event
	 * @param id
	 * @return
	 */
	public TXMResult getResultParent(ExecutionEvent event, String id) {
		return getActiveEditorResult(event).getParent();
	}

	/**
	 * Logs a severe entry.
	 * 
	 * @param selection
	 */
	protected boolean logCanNotExecuteCommand(Object selection) {
		Log.severe(StatsEngineCoreMessages.bind(StatsEngineCoreMessages.CanNotExecuteTheP0CommandWithTheP1Selection, this.getClass(), selection));
		return false;
	}

	/**
	 * Executes a command handler specified by its id.
	 * 
	 * @param commandId
	 */
	public static Object executeCommand(String commandId) {
		return executeCommand(commandId, null, null);
	}


	public static Object executeCommand(String commandId, Map<String, String> params) {
		return executeCommand(commandId, null, params);
	}


	public static Object executeCommand(String commandId, String resultUUID) {
		return executeCommand(commandId, resultUUID, null);
	}

	/**
	 * Executes a command handler specified by its id.
	 * 
	 * @param commandId
	 * @param resultUUID
	 * @param params
	 * @return
	 */
	public static Object executeCommand(String commandId, String resultUUID, Map<String, String> params) {

		Log.fine("BaseAbstractHandler.executeCommand(): calling command " + commandId + "."); //$NON-NLS-1$ //$NON-NLS-2$


		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		ICommandService cmdService = window.getService(ICommandService.class);
		Command cmd = cmdService.getCommand(commandId);
		IHandlerService handlerService = window.getService(IHandlerService.class);

		// Redefine the command supported parameters. Without that, each command must define the parameter "result_parameters_node_path" in each plugin.xml...
		if (resultUUID != null) {
			Log.fine("BaseAbstractHandler.executeCommand(): preparing the send of result UUID: " + resultUUID + "."); //$NON-NLS-1$ //$NON-NLS-2$
			try {
				IParameter[] parameters = new IParameter[] { new IParameter() {

					@Override
					public boolean isOptional() {
						return false;
					}

					@Override
					public IParameterValues getValues() throws ParameterValuesException {
						return null;
					}

					@Override
					public String getName() {
						return TXMPreferences.RESULT_PARAMETERS_NODE_PATH;
					}

					@Override
					public String getId() {
						return TXMPreferences.RESULT_PARAMETERS_NODE_PATH;
					}
				} };
				cmd.define(cmd.getName(), cmd.getDescription(), cmd.getCategory(), parameters);
			}
			catch (NotDefinedException e) {
				e.printStackTrace();
			}
			// pass the parameter value
			params = Collections.singletonMap(TXMPreferences.RESULT_PARAMETERS_NODE_PATH, resultUUID);
		}

		// Call the command with parameters
		if (params != null) {
			try {
				ParameterizedCommand pc = ParameterizedCommand.generateCommand(cmd, params);
				handlerService.executeCommand(pc, null);
			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}
		// Call the command without parameters
		else {
			try {
				return handlerService.executeCommand(cmd.getId(), null);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}


	/**
	 * Executes a command handler specified by its id and activates/deactivates the "send selection context".
	 * The "send selection context" can be used on the target command to know if it's executed from a "send to" command.
	 * 
	 * @param commandId
	 */
	public static Object executeSendToCommand(String commandId) {
		return executeSendToCommand(commandId, null);
	}



	/**
	 * Executes a command handler specified by its id and activates/deactivates the "send selection context".
	 * The "send selection context" can be used on the target command to know if it's executed from a "send to" command.
	 * 
	 * @param commandId
	 * @param resultParametersNodePath
	 */
	public static Object executeSendToCommand(String commandId, String resultParametersNodePath) {
		IContextService contextService = PlatformUI.getWorkbench().getAdapter(IContextService.class);
		IContextActivation activationToken = contextService.activateContext(SEND_SELECTION_TO_COMMAND_CONTEXT_ID);
		Object o = executeCommand(commandId, resultParametersNodePath);
		contextService.deactivateContext(activationToken);
		return o;
	}
}
