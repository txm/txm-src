/**
 * 
 */
package org.txm.rcp.swt.widget;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.editors.TXMEditor;

/**
 * Widget to manage shared threshold filters : fmin, fmax and vmax.
 * 
 * @author sjacquot
 *
 */
public class ThresholdsGroup extends Group {


	protected Label fMinLabel;

	protected Label fMaxLabel;

	protected Label vMaxLabel;


	/**
	 * Minimum frequency filtering spinner.
	 */
	protected Spinner fMinSpinner;

	/**
	 * Maximum frequency filtering spinner.
	 */
	protected Spinner fMaxSpinner;

	/**
	 * Maximum number of lines filtering spinner.
	 */
	protected Spinner vMaxSpinner;


	/**
	 * 
	 * @param parent
	 * @param editor
	 */
	public ThresholdsGroup(Composite parent, TXMEditor editor) {
		this(parent, SWT.NONE, editor);
	}


	/**
	 * 
	 * @param parent
	 * @param style
	 * @param editor
	 */
	public ThresholdsGroup(Composite parent, int style, TXMEditor editor) {
		this(parent, style, editor, false);
	}

	/**
	 * 
	 * @param parent
	 * @param style
	 * @param editor
	 * @param autoCompute
	 */
	public ThresholdsGroup(Composite parent, int style, TXMEditor editor, boolean autoCompute) {
		this(parent, style, editor, autoCompute, true);
	}

	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param displayFMax
	 */
	public ThresholdsGroup(Composite parent, TXMEditor editor, boolean displayFMax) {
		this(parent, SWT.NONE, editor, true, displayFMax);
	}

	/**
	 * 
	 * @param parent
	 * @param style
	 * @param editor
	 */
	public ThresholdsGroup(Composite parent, int style, TXMEditor editor, boolean autoCompute, boolean displayFMax) {
		super(parent, style);

		this.setText(" " + TXMCoreMessages.common_thresholds);

		GridLayout gridLayout = new GridLayout();

		if (displayFMax) {
			gridLayout.numColumns = 6;
		}
		else {
			gridLayout.numColumns = 4;
		}

		gridLayout.makeColumnsEqualWidth = false;
		this.setLayout(gridLayout);

		//FIXME: SJ, 2025-01-28: not used?
//		GridData fieldsGridData = new GridData(SWT.LEFT, SWT.CENTER, false, false);
//		fieldsGridData.minimumWidth = 100;

		// Fmin
		TXMParameterSpinner fMin = new TXMParameterSpinner(this, editor, TXMCoreMessages.common_fMin, TXMCoreMessages.minimumFrequencyThresholdCommaAllValuesBelowFminAreIgnored);
		this.fMinLabel = fMin.getLabel();
		this.fMinSpinner = fMin.getControl();

		// Fmax
		if (displayFMax) {
			TXMParameterSpinner fMax = new TXMParameterSpinner(this, editor, TXMCoreMessages.common_fMax, TXMCoreMessages.maximumFrequencyThresholdCommaAllValuesAboveFmaxAreIgnored);
			this.fMaxLabel = fMax.getLabel();
			this.fMaxSpinner = fMax.getControl();
		}

		// Vmax
		TXMParameterSpinner vMax = new TXMParameterSpinner(this, editor, TXMCoreMessages.common_vMax, TXMCoreMessages.listTruncationThresholdCommaOnlyTheMostFrequentFirstVmaxLinesAreRetained);
		this.vMaxLabel = vMax.getLabel();
		this.vMaxSpinner = vMax.getControl();

	}

	/**
	 * @return the fMinSpinner
	 */
	public Spinner getFMinSpinner() {
		return fMinSpinner;
	}

	/**
	 * @return the fMaxSpinner
	 */
	public Spinner getFMaxSpinner() {
		return fMaxSpinner;
	}

	/**
	 * @return the vMaxSpinner
	 */
	public Spinner getVMaxSpinner() {
		return vMaxSpinner;
	}


	@Override
	protected void checkSubclass() {
	}


	/**
	 * @return the fMinLabel
	 */
	public Label getFMinLabel() {
		return fMinLabel;
	}


	/**
	 * @return the fMaxLabel
	 */
	public Label getFMaxLabel() {
		return fMaxLabel;
	}


	/**
	 * @return the vMaxLabel
	 */
	public Label getVMaxLabel() {
		return vMaxLabel;
	}

}
