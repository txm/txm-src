package org.txm.rcp.swt.widget.preferences;

import org.eclipse.swt.widgets.Composite;

/**
 * Add access to the internal Button
 */
public class IntegerFieldEditor extends org.eclipse.jface.preference.IntegerFieldEditor {
	
	Composite parent;
	
	public IntegerFieldEditor(String preference, String label, Composite parent) {
		super(preference, label, parent);
		this.parent = parent;
	}

	public void setToolTipText(String tooltip) {
		
		getLabelControl(parent).setToolTipText(tooltip);
		getTextControl(parent).setToolTipText(tooltip);
	}
	
	public void setEnabled(boolean b) {
		
		super.setEnabled(b, parent);
	}
}
