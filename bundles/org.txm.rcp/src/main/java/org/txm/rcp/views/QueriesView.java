// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.txm.Toolbox;
import org.txm.core.results.TXMResult;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Project;
import org.txm.objects.SavedQuery;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.handlers.files.EditFile;
import org.txm.rcp.handlers.results.DeleteObject;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * Display the query saved in the Base, each time a query works it is saved @
 * author mdecorde.
 */
public class QueriesView extends ViewPart {

	/** The empty. */
	ArrayList<Object> empty = new ArrayList<Object>();

	/** The composites. */
	Control[] composites = new Control[3];

	/** The activated project. */
	Project activatedProject;

	/** The tv. */
	TreeViewer tv;

	/** The ID. */
	public static String ID = QueriesView.class.getName();

	/**
	 * Instantiates a new queries view.
	 */
	public QueriesView() {
	}

	/**
	 * Refresh.
	 */
	public static void refresh() {
		QueriesView baseView = (QueriesView) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findView(
						QueriesView.ID);
		if (baseView != null)
			baseView.tv.refresh();
	}

	/**
	 * Reload.
	 */
	public static void reload() {

		if (!PlatformUI.isWorkbenchRunning()) return;

		QueriesView baseView = (QueriesView) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findView(
						QueriesView.ID);
		if (baseView != null)
			baseView._reload();
	}

	/**
	 * _reload.
	 */
	public void _reload() {
		tv.setContentProvider(new TxmObjectTreeProvider());
		tv.setLabelProvider(new TxmObjectLabelProvider());
		if (Toolbox.workspace != null) {
			try {
				Collection<MainCorpus> input = CorpusManager.getCorpusManager().getCorpora().values();
				tv.setInput(input);
				refresh();
			}
			catch (Exception e) {
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {

		parent.setLayout(new GridLayout(1, true));
		Button export = new Button(parent, SWT.PUSH);
		export.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		export.setText(TXMUIMessages.exportAll);
		export.setToolTipText("Export queries in a file");
		export.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				//Collection<Base> bases = Toolbox.workspace.getProject("default").getBases(); //$NON-NLS-1$
				File output = new File("queries.txt"); //$NON-NLS-1$

				FileDialog dialog = new FileDialog(arg0.display.getActiveShell(), SWT.SAVE);
				String txmhome = Toolbox.getTxmHomePath();
				if (LastOpened.getFile(ID) != null) {
					dialog.setFilterPath(LastOpened.getFolder(ID));
					dialog.setFileName(LastOpened.getFile(ID));
				}
				else {
					dialog.setFilterPath(new File(txmhome, TXMUIMessages.tXM).getAbsolutePath());
				}
				String path = dialog.open();
				if (path == null) return;
				if (path.trim().length() > 0)
					output = new File(path);

				LastOpened.set(ID, output.getParent(), output.getName());
				if (!output.getParentFile().exists())
					return;


				try {
					FileWriter writer = new FileWriter(output);
					writer.write(TXMUIMessages.queriesColon);
					for (MainCorpus b : CorpusManager.getCorpusManager().getCorpora().values()) {
						writer.write(" " + b.getID() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
						for (SavedQuery q : b.getQueriesLog()) {
							writer.write("  " + q + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
						}
					}

					writer.close();
					EditFile.openfile(path);
					refresh();
				}
				catch (IOException e) {
					Log.severe(NLS.bind(TXMUIMessages.errorWhileExportingQueriesP0, e.getLocalizedMessage()));
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					org.txm.utils.logger.Log.printStackTrace(e);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});
		tv = new TreeViewer(parent);
		tv.getTree().setLayoutData(
				new GridData(GridData.FILL, GridData.FILL, true, true));
		_reload();

		tv.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
			}
		});

		tv.getTree().addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.DEL) {
					ISelection sel = tv.getSelection();
					if (sel instanceof IStructuredSelection) {
						if (!DeleteObject.askContinueToDelete((IStructuredSelection) sel)) return;
						DeleteObject.delete(sel);
					}
				}
				else if (e.keyCode == SWT.F5) {
					reload();
				}
				else if ((char) e.keyCode == 'c' & ((e.stateMask & SWT.CTRL) != 0)) {
					TreeSelection selection = (TreeSelection) tv.getSelection();
					Object selectedItem = selection.getFirstElement();
					if (selectedItem instanceof SavedQuery) {
						String text = ((SavedQuery) selectedItem).toString();
						org.txm.rcp.utils.IOClipboard.write(text);
					}
				}
			}
		});

		MenuManager menuManager = new MenuManager();
		Menu menu = menuManager.createContextMenu(tv.getTree());

		// Set the MenuManager
		tv.getTree().setMenu(menu);
		getSite().registerContextMenu(menuManager, tv);
		// Make the selection available
		getSite().setSelectionProvider(tv);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

	/**
	 * Gets the activated project.
	 *
	 * @return the activated project
	 */
	public Project getActivatedProject() {
		return activatedProject;
	}

	/**
	 * The Class TxmObjectLabelProvider.
	 */
	public class TxmObjectLabelProvider extends LabelProvider {

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
		 */
		@Override
		public String getText(Object element) {
			if (element instanceof CorpusBuild)
				return ((TXMResult) element).getName();
			else if (element instanceof SavedQuery)
				return ((SavedQuery) element).toString();
			return NLS.bind(TXMUIMessages.errorWithElementP0, element);
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
		 */
		@Override
		public final Image getImage(Object element) {
			if (element instanceof CorpusBuild)
				return IImageKeys.getImage(IImageKeys.CORPUS);
			else if (element instanceof SavedQuery)
				return IImageKeys.getImage(IImageKeys.QUERY);
			return null;
		}
	}

	/**
	 * The Class TxmObjectTreeProvider.
	 */
	public class TxmObjectTreeProvider implements ITreeContentProvider {

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
		 */
		@Override
		public Object[] getChildren(Object element) {
			if (element instanceof CorpusBuild) {
				CorpusBuild c = ((CorpusBuild) element);
				return c.getQueriesLog().toArray();
			}
			return new Object[0];
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
		 */
		@Override
		public Object getParent(Object element) {
			if (element instanceof SavedQuery) {
				return ((SavedQuery) element).getParent();
			}
			return new Object[0];
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
		 */
		@Override
		public boolean hasChildren(Object element) {
			if (element instanceof CorpusBuild) {
				CorpusBuild c = ((CorpusBuild) element);
				return !c.getQueriesLog().isEmpty();
			}
			return false;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
		 */
		@Override
		public Object[] getElements(Object element) {
			Collection<Project> input = (Collection<Project>) element;
			Object[] objects = new Object[input.size()];
			Iterator<Project> it = input.iterator();
			int i = 0;
			while (it.hasNext()) {
				objects[i] = it.next();
				i++;
			}
			return objects;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
		 */
		@Override
		public void dispose() {
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
		 */
		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}
}
