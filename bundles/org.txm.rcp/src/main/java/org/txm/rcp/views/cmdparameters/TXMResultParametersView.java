// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views.cmdparameters;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IPartService;
import org.eclipse.ui.IPropertyListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.part.ViewPart;
import org.txm.core.results.BeanParameters;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.swt.widget.PropertiesSelector;
import org.txm.rcp.swt.widget.QueryWidget;
import org.txm.rcp.swt.widget.ReferencePatternSelector;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.ReferencePattern;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.WordProperty;

import cern.colt.Arrays;

/**
 * Alternative display of the TXMEditor form fields
 * 
 * @author mdecorde.
 */
public class TXMResultParametersView extends ViewPart implements IPartListener, IPropertyListener {

	/** The ID. */
	public static String ID = TXMResultParametersView.class.getName(); // $NON-NLS-1$

	/**
	 * Instantiates a new queries view.
	 */
	public TXMResultParametersView() {

		IPartService service = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getPartService();
		service.addPartListener(this);
	}

	@Override
	public void dispose() {

		IPartService service = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getPartService();
		service.removePartListener(this);
	}

	/**
	 * Refresh.
	 */
	public static void refresh() {

	}

	TXMEditor<? extends TXMResult> activated;

	private Composite panel;

	private ScrolledComposite scrollC;

	private Button recompute;

	public void updateEditorParameters() throws Exception {

		EditorPart editor = activated;

		for (Control c : panel.getChildren()) {
			c.dispose();
		}

		if (editor == null) {
			new Label(panel, SWT.NONE).setText("No editor selected.");
			recompute.setEnabled(false);
			return;
		}

		if (!(editor instanceof TXMEditor)) {
			new Label(panel, SWT.NONE).setText("Editor is not a TXMPartEditor: " + editor);
			return;
		}
		recompute.setEnabled(true);
		TXMEditor reditor = (TXMEditor) editor;
		final TXMResult result = reditor.getResult();

		List<Field> fields = BeanParameters.getAllFields(result);

		Collections.sort(fields, new Comparator<Field>() {

			@Override
			public int compare(Field o1, Field o2) {

				Parameter parameter1 = o1.getAnnotation(Parameter.class);
				Parameter parameter2 = o2.getAnnotation(Parameter.class);

				if (parameter1.type() == parameter2.type()) {
					return o1.getName().compareTo(o2.getName());
				}
				else {
					return parameter1.type() - parameter2.type();
				}
			}
		});

		for (final Field f : fields) {
			Parameter parameter = f.getAnnotation(Parameter.class);
			if (parameter == null) {
				continue;
			}

			String name;
			if (!parameter.key().isEmpty()) {
				name = parameter.key();
			}
			else {
				name = f.getName();
			}
			Class<?> clazz = f.getType();
			f.setAccessible(true);
			final Object value = f.get(result);

			new Label(panel, SWT.NONE).setText(name);
			clazz.equals(Integer.class);
			Control c = null;
			if (clazz.equals(Integer.class)) {
				final Spinner sp = new Spinner(panel, SWT.BORDER);
				sp.setToolTipText(name);
				if (value != null) {
					sp.setSelection((Integer) value);
				}
				sp.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {

						try {
							f.set(result, sp.getSelection());
							if (activated != null && !activated.getExtendedParametersComposite().isDisposed()) {
								activated.compute(false);
							}
						}
						catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});
				c = sp;
			}
			else if (clazz.equals(String.class)) {
				final Text t = new Text(panel, SWT.BORDER);
				t.setToolTipText(name);
				if (value != null) {
					t.setText(value.toString());
				}
				t.addKeyListener(new org.eclipse.swt.events.KeyListener() {

					@Override
					public void keyReleased(org.eclipse.swt.events.KeyEvent e) {

						try {
							f.set(result, t.getText());
							if (activated != null && !activated.getExtendedParametersComposite().isDisposed()) {
								activated.compute(false);
							}
						}
						catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}

					@Override
					public void keyPressed(org.eclipse.swt.events.KeyEvent e) {
						// TODO Auto-generated method stub

					}
				});
				c = t;
			}
			else if (clazz.equals(Float.class)) {
				final Spinner sp = new Spinner(panel, SWT.BORDER);
				sp.setToolTipText(name);
				sp.setSelection((int) ((Float) value * 2));
				sp.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {

						try {
							f.set(result, sp.getSelection());
							if (activated != null && !activated.getExtendedParametersComposite().isDisposed()) {
								activated.compute(false);
							}
						}
						catch (Exception e1) {
							e1.printStackTrace();
						}
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});
				c = sp;
				((Spinner) c).setDigits(2);
			}
			else if (clazz.equals(Boolean.class)) {
				final Button sp = new Button(panel, SWT.CHECK);
				sp.setToolTipText(name);
				sp.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {

						try {
							f.set(result, sp.getSelection());
							if (activated != null && !activated.getExtendedParametersComposite().isDisposed()) {
								activated.compute(false);
							}
						}
						catch (Exception e1) {
							e1.printStackTrace();
						}
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});
				c = sp;
			}
			else if (clazz.equals(IQuery.class)) {
				final QueryWidget qw = new QueryWidget(panel, SWT.NONE, result.getProject(), CQPSearchEngine.getEngine());
				qw.setToolTipText(name);
				if (value != null) {
					qw.setText(((IQuery) value).getQueryString());
				}
				qw.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {

						try {
							f.set(result, qw.getQuery());
							if (activated != null && !activated.getExtendedParametersComposite().isDisposed()) {
								activated.compute(false);
							}
						}
						catch (Exception e1) {
							e1.printStackTrace();
						}
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) { }
				});
				qw.addKeyListener(new org.eclipse.swt.events.KeyListener() {

					@Override
					public void keyReleased(org.eclipse.swt.events.KeyEvent e) {

						if (e.keyCode != SWT.KEYPAD_CR && e.keyCode != SWT.CR) {
							return;
						}
						try {
							f.set(result, qw.getQuery());
							if (activated != null && !activated.getExtendedParametersComposite().isDisposed()) {
								activated.compute(false);
							}
						}
						catch (Exception e1) {
							e1.printStackTrace();
						}
					}

					@Override
					public void keyPressed(org.eclipse.swt.events.KeyEvent e) {
					}
				});
				c = qw;
			}
			else if (clazz.equals(File.class)) {
				final Button b = new Button(panel, SWT.PUSH);
				b.setToolTipText(name);
				if (value != null) {
					b.setText(value.toString());

				}

				b.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {

						File file = (File) value;
						if (file.isDirectory()) {
							DirectoryDialog dialog = new DirectoryDialog(panel.getDisplay().getActiveShell());
							dialog.setText("Choose directory");
							dialog.setFilterPath(file.getAbsolutePath());
							String path = dialog.open();
							if (path != null) {
								try {
									f.set(result, path);
									b.setText(path);
									if (activated != null && !activated.getExtendedParametersComposite().isDisposed()) {
										activated.compute(false);
									}
								}
								catch (IllegalArgumentException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								catch (IllegalAccessException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}
						}
						else {
							FileDialog dialog = new FileDialog(panel.getDisplay().getActiveShell());
							dialog.setText("Choose directory");
							dialog.setFilterPath(file.getAbsolutePath());
							String path = dialog.open();
							if (path != null) {
								try {
									f.set(result, path);
									b.setText(path);
									if (activated != null && !activated.getExtendedParametersComposite().isDisposed()) {
										activated.compute(false);
									}
								}
								catch (Exception e1) {
									e1.printStackTrace();
								}
							}
						}
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});
			}
			else if (clazz.equals(List.class)) {
				List list = (List) value;
				if (list.isEmpty()) {
					Label l3 = new Label(panel, SWT.NONE);
					l3.setText("no value");
					c = l3;
				}
				else {
					Object first = list.get(0);
					if (first instanceof WordProperty) {
						c = createWordPropertyField(f, result, list);
					}
					else if (first instanceof StructuralUnitProperty) {
						c = createStructuralUnitPropertyField(f, result, list);
					}
				}
				// } else if (value instanceof StructuralUnit) {
			}
			else if (clazz.equals(WordProperty.class)) {
				ArrayList<WordProperty> selected = new ArrayList<>();
				selected.add((WordProperty) value);
				c = createWordPropertyField(f, result, selected);
				// } else if (value instanceof StructuralUnit) {
				// Label l2 = new Label(panel,
				// SWT.NONE).setText("="+value.toString());
			}
			else if (clazz.equals(StructuralUnitProperty.class)) {
				ArrayList<StructuralUnitProperty> selected = new ArrayList<>();
				selected.add((StructuralUnitProperty) value);
				c = createStructuralUnitPropertyField(f, result, selected);
				// } else if (value instanceof ReferencePattern) {
				// c = new Label(panel, SWT.NONE).setText("="+value.toString());
			}
			else if (clazz.equals(ReferencePattern.class)) {
				c = createReferencePatternField(f, result, (ReferencePattern) value);
				// } else if (value instanceof ReferencePattern) {
				// c = new Label(panel, SWT.NONE).setText("="+value.toString());
			}
			else {
				Label l2 = new Label(panel, SWT.NONE);
				if (value != null) {
					TypeVariable<?>[] pp = value.getClass().getTypeParameters();
					l2.setText(clazz.getSimpleName() + "=" + value + " (" + Arrays.toString(pp) + ")"); //$NON-NLS-1$
				}
				else {
					l2.setText(clazz.getSimpleName() + "=" + value); //$NON-NLS-1$
				}
				c = l2;
			}

			if (c != null) {
				c.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).create());
			}
		}
		scrollC.setContent(panel);
		scrollC.setMinSize(panel.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		scrollC.setExpandHorizontal(true);
		scrollC.setExpandVertical(true);
		scrollC.layout(true);
	}

	private Control createReferencePatternField(final Field f, final TXMResult result, ReferencePattern value) throws Exception {

		final List<Property> selected = new ArrayList<>();
		selected.addAll(value.getProperties());
		CQPCorpus corpus = value.getCorpus();
		ReferencePatternSelector selector = new ReferencePatternSelector(panel, SWT.NONE);
		if (corpus != null) {
			List<Property> availables = new ArrayList<>();
			availables.addAll(corpus.getOrderedProperties());
			availables.addAll(corpus.getOrderedStructuralUnitProperties());
			selector.setPattern(value.getPattern());
			selector.setProperties(availables, selected);
		}
		selector.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				if (activated != null && !activated.getExtendedParametersComposite().isDisposed()) {
					ReferencePattern p = new ReferencePattern(selected);
					try {
						f.set(result, p);
					}
					catch (Exception e1) {
						e1.printStackTrace();
					}
					activated.compute(false);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		// selector.setLayoutData(layoutData);
		return selector;
	}

	protected PropertiesSelector<WordProperty> createWordPropertyField(final Field f, final TXMResult result, final List<WordProperty> selected) throws CqiClientException {

		WordProperty value = selected.get(0);
		CQPCorpus corpus = value.getCorpus();

		ArrayList<WordProperty> availables = new ArrayList<>(corpus.getOrderedProperties());
		availables.addAll(corpus.getVirtualProperties());

		PropertiesSelector<WordProperty> selector = new PropertiesSelector<>(panel, SWT.NONE);
		selector.setProperties(availables, selected);
		selector.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				if (activated != null && !activated.getExtendedParametersComposite().isDisposed()) {
					try {
						f.set(result, selected);
					}
					catch (Exception e1) {
						e1.printStackTrace();
					}
					activated.compute(false);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		// selector.setLayoutData(layoutData);
		return selector;
	}

	private PropertiesSelector<StructuralUnitProperty> createStructuralUnitPropertyField(final Field f, final TXMResult result, final List<StructuralUnitProperty> selected) throws CqiClientException {

		StructuralUnitProperty value = selected.get(0);
		CQPCorpus corpus = value.getCorpus();
		ArrayList<StructuralUnitProperty> availables = new ArrayList<>();
		availables.addAll(corpus.getOrderedStructuralUnitProperties());
		PropertiesSelector<StructuralUnitProperty> selector = new PropertiesSelector<>(panel, SWT.NONE);
		selector.setProperties(availables, selected);
		selector.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				if (activated != null && !activated.getExtendedParametersComposite().isDisposed()) {
					try {
						f.set(result, selected);
					}
					catch (Exception e1) {
						e1.printStackTrace();
					}
					activated.compute(false);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		return selector;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {

		parent.setLayout(new GridLayout(1, true));

		recompute = new Button(parent, SWT.PUSH);
		recompute.setImage(IImageKeys.getImage(IImageKeys.START));
		recompute.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				if (activated != null && !activated.getExtendedParametersComposite().isDisposed()) {
					activated.compute(false);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		recompute.setLayoutData(GridDataFactory.fillDefaults().align(GridData.BEGINNING, GridData.BEGINNING).grab(false, false).create());
		recompute.setEnabled(false);

		scrollC = new ScrolledComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL);
		scrollC.setLayoutData(GridDataFactory.fillDefaults().grab(true, true).create());

		panel = new Composite(scrollC, SWT.NONE);
		panel.setLayout(new GridLayout(2, true));

		try {
			updateEditorParameters();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void partActivated(IWorkbenchPart part) {

		if (part == null)
			return;
		// System.out.println("partActivated: "+part);
		if (part instanceof TXMEditor) {
			activated = (TXMEditor) part;
		}
		if (activated == null) {
			return;
		}
		activated.addPropertyListener(this);
		recompute.setEnabled(true);
		// activated.addPartPropertyListener(this);
		try {
			updateEditorParameters();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void partBroughtToTop(IWorkbenchPart part) {

		if (part == null) {
			return;
		}
	}

	@Override
	public void partClosed(IWorkbenchPart part) {

		if (part == null) {
			return;
		}
		if (part == activated) {
			activated = null;
			try {
				updateEditorParameters();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void partDeactivated(IWorkbenchPart part) {

		if (part == null)
			return;
		// System.out.println("partDeactivated: "+part);
		// if (part instanceof TXMEditor)
		// activated = null;
		// try {
		// updateEditorParameters();
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
	}

	@Override
	public void propertyChanged(Object source, int propId) {

		// System.out.println("Editor property changed: "+propId);
		if (propId == EditorPart.PROP_DIRTY) {
			try {
				updateEditorParameters();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void partOpened(IWorkbenchPart part) {

		if (part == null)
			return;
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
	}
}
