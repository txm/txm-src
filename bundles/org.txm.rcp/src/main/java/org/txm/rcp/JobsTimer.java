package org.txm.rcp;

import java.util.Date;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.utils.ExecTimer;

/**
 * Print formated timer
 * 
 * @author mdecorde
 *
 */
public class JobsTimer {

	static long startTime = 0, endTime = 0;

	static float time = 0;

	static String message = ""; //$NON-NLS-1$

	public static void start() {
		startTime = new Date().getTime();
	}

	public static String stop() {
		endTime = new Date().getTime();
		time = endTime - startTime;
		return setMessage();
	}

	static float t;

	private static String setMessage() {
		t = time;
		if (t < 1000) {
			message = "" + ((int) t) + TXMUIMessages.msec; //$NON-NLS-1$
		}
		else {
			t = t / 1000;
			if (t < 3) {
				message = "" + t; //$NON-NLS-1$
				message = "" + (message.substring(0, 3)) + TXMUIMessages.sec; //$NON-NLS-1$
			}
			else if (t < 60)
				message = "" + ((int) t) + TXMUIMessages.sec; //$NON-NLS-1$
			else if (t < 3600) {
				message = "" + ((int) t / 60) + TXMUIMessages.minAnd + ((int) t % 60) + TXMUIMessages.sec; //$NON-NLS-1$
			}
			else {
				message = "" + ((int) t / 3600) + TXMUIMessages.h + ((int) (t % 3600) / 60) + TXMUIMessages.minAnd + ((int) (t % 3600) % 60) + TXMUIMessages.sec; //$NON-NLS-1$
			}
		}
		return message;
	}

	public static String getMessage() {
		return message;
	}

	public static void stopAndPrint() {
		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.SHOW_TIMINGS)) {
			if (time <= 1000) System.out.println(NLS.bind(TXMUIMessages.doneInP0P1, stop(), (int) time));
			else System.out.println(NLS.bind(TXMUIMessages.doneInP0, stop()));
		}
	}

	public static void main(String[] args) throws InterruptedException {
		ExecTimer.start();
		Thread.sleep(1000);
		JobsTimer.start();
		Thread.sleep(1000);
		System.out.println(TXMCoreMessages.bind(TXMUIMessages.jobP0, JobsTimer.stop()));
		System.out.println(TXMCoreMessages.bind(TXMUIMessages.executingP0, ExecTimer.stop()));
	}
}
