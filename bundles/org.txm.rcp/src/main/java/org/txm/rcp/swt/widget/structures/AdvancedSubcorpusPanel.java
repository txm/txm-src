// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.widget.structures;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.widget.AssistedChoiceQueryWidget;
import org.txm.rcp.swt.widget.AssistedQueryWidget;
import org.txm.rcp.swt.widget.QueryWidget;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;

/**
 * The Class AdvancedSubcorpusPanel : a query.
 */
public class AdvancedSubcorpusPanel extends Composite {

	/** The corpus. */
	CQPCorpus corpus;

	/** The query. */
	String query;

	/** The query field. */
	AssistedQueryWidget queryField;

	/**
	 * Instantiates a new complex subcorpus panel.
	 *
	 * @param parent the parent
	 * @param style the style
	 * @param corpus the corpus
	 */
	public AdvancedSubcorpusPanel(Composite parent, int style, CQPCorpus corpus) {

		super(parent, style);

		this.setLayout(new GridLayout(2, false));
		this.corpus = corpus;

		Label l = new Label(this, SWT.NONE);
		l.setText(TXMUIMessages.query);

		queryField = new AssistedQueryWidget(this, SWT.BORDER, corpus, CQPSearchEngine.getEngine());
		queryField.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
	}

	public void updateQueryField() {

		queryField.update();
	}

	/**
	 * Update display.
	 */
	public void updateDisplay() {
		this.layout();
	}

	/**
	 * Gets the query string.
	 *
	 * @return the generated query string
	 */
	public String getQueryString() {

		String q = queryField.getQuery().getQueryString();
		queryField.memorize();
		return q;
	}

	/**
	 * 
	 * @return the raw query without the context filter
	 */
	public String getQueryFieldValue() {

		String query = this.queryField.getQueryString();
		if (query == null || query.length() == 0) {
			query = getQueryString();
		}
		return query;
	}
	
	public CQLQuery getQuery() {
		return (CQLQuery) this.queryField.getQuery();
	}

	public String getGeneratedName() {

		return null;
	}
}
