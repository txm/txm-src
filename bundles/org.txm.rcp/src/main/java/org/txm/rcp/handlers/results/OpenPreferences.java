package org.txm.rcp.handlers.results;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.PreferencesUtil;
import org.eclipse.ui.internal.dialogs.WorkbenchPreferenceDialog;
import org.txm.rcp.handlers.BaseAbstractHandler;

public class OpenPreferences extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Shell s = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();

		WorkbenchPreferenceDialog dialog = (WorkbenchPreferenceDialog) PreferencesUtil.createPreferenceDialogOn(s, null, null, null);
		dialog.open();
		//		Object o = this.getCorporaViewSelectedObject(event);
		//		TreeViewer tree = dialog.getTreeViewer();
		//		ViewerFilter[] filters = tree.getFilters();
		//			
		//		PreferencePatternFilter ppf = new PreferencePatternFilter();
		//		ppf.setPattern("TXM");
		//		
		//		ViewerFilter[] newfilters = new ViewerFilter[filters.length+1];
		//		System.arraycopy(filters, 0, newfilters, 0, filters.length);
		//		newfilters[filters.length] = ppf;
		//		tree.setFilters(newfilters);

		//		PreferencePatternFilter ppf = new PreferencePatternFilter();
		//		ppf.setPattern(o.getClass().getSimpleName());
		//		dialog.getTreeViewer().setFilters(ppf);
		return null;
	}
}
