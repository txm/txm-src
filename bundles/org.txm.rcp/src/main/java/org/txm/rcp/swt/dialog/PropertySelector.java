// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.dialog;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Property;

/**
 * Select and order a properties list
 */
public class PropertySelector extends Dialog {

	/** The property. */
	protected Property property;

	/** The properties. */
	protected List<Property> properties;

	/** The ana prop combo. */
	protected Combo anaPropCombo;

	/** The title. */
	protected String title;

	/** The mess. */
	protected String mess;

	/** The corpus. */
	protected CQPCorpus corpus;

	/**
	 * Instantiates a new property selector.
	 *
	 * @param parentShell the parent shell
	 * @param corpus the corpus
	 */
	public PropertySelector(Shell parentShell, CQPCorpus corpus) {

		super(parentShell);
		try {
			this.properties = new ArrayList<Property>(corpus.getOrderedProperties());
			this.title = TXMUIMessages.selectAProperty;
			this.mess = TXMUIMessages.selectAProperty;
		}
		catch (CqiClientException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	/**
	 * Instantiates a new property selector.
	 *
	 * @param parentShell the parent shell
	 * @param corpus the corpus
	 * @param title the title
	 * @param mess the mess
	 */
	public PropertySelector(Shell parentShell, CQPCorpus corpus, String title, String mess) {

		super(parentShell);
		this.corpus = corpus;
		this.title = title;
		this.mess = mess;
		try {
			this.properties = new ArrayList<Property>(corpus.getOrderedProperties());
		}
		catch (CqiClientException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {

		super.configureShell(newShell);
		newShell.setText(TXMUIMessages.selectAProperty);
		if (title != null) {
			newShell.setText(title);
		}
	}

	/** The composite. */
	protected Composite composite;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(2, false);
		composite.setLayout(layout);

		Label anaPropLabel = new Label(composite, SWT.NONE);
		if (mess != null)
			anaPropLabel.setText(mess);
		else
			anaPropLabel
					.setText(TXMCoreMessages.common_properties);
		anaPropLabel.setLayoutData(new GridData(GridData.END, GridData.CENTER,
				false, false));

		anaPropCombo = new Combo(composite, SWT.READ_ONLY);
		anaPropCombo.setLayoutData(new GridData(GridData.FILL, GridData.FILL,
				true, false));
		int i = 0;
		int iword = 0;
		for (Property property : properties) {
			anaPropCombo.add(property.getName());
			if (property.getName().equals("word")) //$NON-NLS-1$
				iword = i;
			i++;
		}
		if (properties.size() > 0)
			anaPropCombo.select(iword);
		return composite;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {

		int propIndex = anaPropCombo.getSelectionIndex();
		if (propIndex == -1) {
			MessageDialog.openError(getShell(),
					TXMUIMessages.invalidProperty,
					TXMUIMessages.structureFieldIsMandatory);
			return;
		}
		property = properties.get(propIndex);

		super.okPressed();
	}

	/**
	 * Gets the property.
	 *
	 * @return the property
	 */
	public Property getProperty() {
		return property;
	}
}
