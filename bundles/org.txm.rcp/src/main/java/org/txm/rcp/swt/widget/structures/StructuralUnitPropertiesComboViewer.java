/**
 * 
 */
package org.txm.rcp.swt.widget.structures;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;

/**
 * Combo viewer for properties of a structural unit supporting single selection only.
 * 
 * @author sjacquot
 *
 */
public class StructuralUnitPropertiesComboViewer extends PropertiesComboViewer {

	/**
	 * 
	 * @param parent
	 * @param style
	 * @param editor
	 * @param autoCompute
	 * @param su
	 * @param selectedSUP
	 * @param addBlankEntry to add an empty blank entry at start of the list to clear property.
	 */
	public StructuralUnitPropertiesComboViewer(Composite parent, int style, TXMEditor<? extends TXMResult> editor, boolean autoCompute, StructuralUnit su, StructuralUnitProperty selectedSUP,
			boolean addBlankEntry) {
		super(parent, style, editor, false, null, null, addBlankEntry, null);

		this.updateFromStructuralUnit(su, selectedSUP);

		// Listener
		this.addSelectionChangedListener(new ComputeSelectionListener(editor, autoCompute));
	}


	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param autoCompute
	 * @param su
	 * @param selectedSUP
	 * @param addEmptyEntry to add an empty blank entry at start of the list to clear property.
	 */
	public StructuralUnitPropertiesComboViewer(Composite parent, TXMEditor<? extends TXMResult> editor, boolean autoCompute, StructuralUnit su, StructuralUnitProperty selectedSUP,
			boolean addEmptyEntry) {
		this(parent, SWT.READ_ONLY, editor, autoCompute, su, selectedSUP, addEmptyEntry);
	}

	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param su
	 * @param selectedSUP
	 */
	public StructuralUnitPropertiesComboViewer(Composite parent, TXMEditor<? extends TXMResult> editor, StructuralUnit su, StructuralUnitProperty selectedSUP) {
		this(parent, editor, true, su, selectedSUP, true);
	}


	/**
	 * Updates the properties list.
	 * 
	 * @param su
	 */
	public void updateFromStructuralUnit(StructuralUnit su) {
		List<StructuralUnitProperty> properties;
		if (su != null) {
			properties = su.getUserDefinedProperties();
		}
		else {
			properties = new ArrayList<StructuralUnitProperty>(0);
		}
		this.updateFromList(properties, null);
	}

	/**
	 * Updates the properties list and selects the specified structural unit property.
	 * 
	 * @param su
	 * @param selectedSUP
	 */
	public void updateFromStructuralUnit(StructuralUnit su, StructuralUnitProperty selectedSUP) {
		if (su != null) {
			this.updateFromList(su.getUserDefinedProperties(), selectedSUP);
		}
	}

}
