// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.preferences;

import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.rcp.swt.widget.preferences.ComboFieldEditor;

/**
 * The Class ExportPreferencePage.
 */
public class ExportImportPreferencePage extends RCPPreferencesPage {

	/** The show_export. */
	private BooleanFieldEditor show_export;

	/** The encoding. */
	private ComboFieldEditor encoding;

	/** The colseparators. */
	private ComboFieldEditor colseparators;

	/** The txtseparators. */
	private ComboFieldEditor txtseparators;

	/**
	 * The charts engine export formats selection combo box.
	 */
	//private ComboFieldEditor chartsEngineExportFormatsComboField;


	/**
	 * Instantiates a new export preference page.
	 */
	public ExportImportPreferencePage() {
		super();
		setTitle(TXMUIMessages.exportSettings);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 */
	@Override
	protected void createFieldEditors() {
		Set<String> encodings = java.nio.charset.Charset.availableCharsets().keySet();
		String[][] values = new String[encodings.size()][2];
		int i = 0;
		for (String s : encodings) {
			values[i][0] = values[i][1] = s;
			i++;
		}

		encoding = new ComboFieldEditor(TBXPreferences.EXPORT_ENCODING, TXMUIMessages.exportFileEncoding, values, getFieldEditorParent());
		encoding.setToolTipText(TXMUIMessages.characterEncodingSystemToUseForExport);
		addField(encoding);

		Group group = new Group(getFieldEditorParent(), SWT.NONE);
		group.setText(" CSV"); //$NON-NLS-1$ the space was purposely written
		group.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1));
		
		String[][] cols = { { TXMUIMessages.tabulation, "\t" }, { ",", "," }, { ";", ";" } }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		String txts[][] = { { TXMUIMessages.nothing, "" }, { "\"", "\"" }, { "'", "'" } }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		colseparators = new ComboFieldEditor(TBXPreferences.EXPORT_COL_SEPARATOR, TXMUIMessages.columnsSeparatedBy, cols, group);
		colseparators.setToolTipText(TXMUIMessages.characterToUseAsColumnSeparatorForCSVExports);
		addField(colseparators);

		txtseparators = new ComboFieldEditor(TBXPreferences.EXPORT_TXT_SEPARATOR, TXMUIMessages.textSeparator, txts, group);
		txtseparators.setToolTipText(TXMUIMessages.characterToUseAsColumnValueDelimiterForCSVExports);
		addField(txtseparators);
		
		((GridLayout) group.getLayout()).marginWidth = 3;

		show_export = new BooleanFieldEditor(TBXPreferences.EXPORT_SHOW, TXMUIMessages.showExportResultInTheTextEditor, getFieldEditorParent());
		show_export.setToolTipText(TXMUIMessages.theExportedResultIsDisplayedInANewWindow );
		addField(show_export);


		// current engine supported export formats
		// FIXME: SJ: doesn't work because even when giving the right preference store. It is automatically set to org.txm.core before performOK() is called, but where?
		//		this.chartsEngineExportFormatsComboField = new ComboFieldEditor(ChartsEnginePreferences.DEFAULT_EXPORT_FORMAT, "Graphiques", ChartsEnginesManager.getExportFormatsEntryNamesAndValues(ChartsEnginesManager.getInstance().getCurrentEngine()), this.getFieldEditorParent());
		//		this.chartsEngineExportFormatsComboField.setPreferenceStore(new TXMPreferenceStore(ChartsEnginePreferences.getInstance().getPreferencesNodeQualifier()));
		//		System.out.println("ExportPreferencePage.createFields(): before add field " + ((TXMPreferenceStore) this.chartsEngineExportFormatsComboField.getPreferenceStore()).getNodeQualifier());
		//		this.addField(this.chartsEngineExportFormatsComboField);
		//		System.out.println("ExportPreferencePage.createFields(): after add field " + ((TXMPreferenceStore) this.chartsEngineExportFormatsComboField.getPreferenceStore()).getNodeQualifier());

		//FIXME: move this code to org.txm.jfreechart.rcp and other SWT components provider implementations
		// Default charts export format
		//		String[][] devicesvalues = new String[SWTChartsComponentsProvider.getCurrent().getEditorSupportedExportFileFormats().size()][2];
		//		for(int j = 0; j < SWTChartsComponentsProvider.getCurrent().getEditorSupportedExportFileFormats().size(); j++) {
		//			devicesvalues[j][0] = devicesvalues[j][1] = SWTChartsComponentsProvider.getCurrent().getEditorSupportedExportFileFormats().get(j);
		//		}
		//		// FIXME : rename the preference "RDEVICE" and store it at charts engine level
		//		// FIXME : change the Messages.ExportPreferencePage_21 message to "Default chart export file format", "Format de fichier d'export des graphiques par défaut", todo : russian
		//		if(SWTChartsComponentsProvider.getCurrent().getEditorSupportedExportFileFormats().size() > 0)	{
		//			String defaultdevice = TxmPreferences.get(RDEVICE);
		//			if (defaultdevice == null)	{
		//				TxmPreferences.set(RDEVICE, SWTChartsComponentsProvider.getCurrent().getEditorSupportedExportFileFormats().get(0));
		//			}
		//		}
		//		devices = new ComboFieldEditor(RDEVICE, Messages.ExportPreferencePage_21, devicesvalues, getFieldEditorParent());
		//addField(devices);


	}

	@Override
	public boolean performOk() {
		// FIXME: SJ: doesn't work because even when giving the right preference store. It is automatically set to org.txm.core before performOK() is called, but where?
		//		try {
		//			System.out.println("ExportPreferencePage.performOk(): " + ((TXMPreferenceStore) this.chartsEngineExportFormatsComboField.getPreferenceStore()).getNodeQualifier());
		//			((ScopedPreferenceStore) this.chartsEngineExportFormatsComboField.getPreferenceStore()).save();
		//		}
		//		catch (IOException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		return super.performOk();
	}

}
