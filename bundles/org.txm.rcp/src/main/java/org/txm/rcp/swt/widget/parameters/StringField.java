package org.txm.rcp.swt.widget.parameters;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.kohsuke.args4j.NamedOptionDef;

public class StringField extends LabelField {

	Text dt;

	int MAX = 300;

	public StringField(Composite parent, int style, NamedOptionDef parameter) {
		super(parent, style, parameter);

		dt = new Text(this, SWT.SINGLE | SWT.BORDER);
		GridData g = new GridData(SWT.FILL, SWT.LEFT, true, false);
		g.widthHint = 300;
		g.minimumWidth = 100;
		dt.setLayoutData(g);

		resetToDefault();
	}

	public String getWidgetValue() {
		return dt.getText();
	}

	public void setDefault(Object text) {
		dt.setText("" + text); //$NON-NLS-1$
	}

	@Override
	public void resetToDefault() {
		dt.setText(getWidgetDefault());
	}
}
