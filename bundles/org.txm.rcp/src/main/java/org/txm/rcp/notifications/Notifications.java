package org.txm.rcp.notifications;

import java.io.File;
import java.io.IOException;

import org.eclipse.swt.widgets.Display;
import org.txm.utils.logger.Log;

public class Notifications {

	final static protected File notifySendFile = new File("/usr/bin/notify-send"); //$NON-NLS-1$

	/**
	 * Display a notification, must be called within the UI thread
	 * 
	 * @param title
	 * @param message
	 */
	public static void simple(String title, String message) {
		simple(Display.getCurrent(), title, message);
	}

	/**
	 * Display a simple notification title+message using the given Display
	 * 
	 * If /usr/bin/notify-send exists (linux & gtk), it is used instead of the SimpleNotification class
	 * 
	 * @param display
	 * @param title
	 * @param message
	 */
	public static void simple(Display display, String title, String message) {
		if (notifySendFile.exists() && notifySendFile.canExecute()) {
			String[] cmds = { "/usr/bin/notify-send", title, message, "--icon=dialog-information" }; //$NON-NLS-1$ //$NON-NLS-2$
			try {
				Runtime.getRuntime().exec(cmds);
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			if (display == null) {
				Log.info(title + ": " + message); //$NON-NLS-1$
			}
			else {
				new SimpleNotification(display, title, message).open();
			}
		}

		// MAC :
		// osascript -e 'tell app "Finder" to display dialog "Hello World"' 
		// foreground : “System Events”
		// osascript -e 'display notification "Lorem ipsum dolor sit amet" with title "Title"'

		// WIN
		// errr...
	}
}
