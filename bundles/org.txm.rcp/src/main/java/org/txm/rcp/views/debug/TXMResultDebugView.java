// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views.debug;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
//import org.txm.chartsengine.core.results.ChartResult;
import org.txm.core.results.TXMResult;
import org.txm.rcp.views.corpora.CorporaView;
//import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.CQPCorpus;


/**
 * Debug view for TXMResult.
 * TODO: SJ: make a chart result debug view/ext point to show more informations about charts
 * 
 * @author sjacquot
 */
public class TXMResultDebugView extends ViewPart implements ISelectionChangedListener {

	/**
	 * Display area.
	 */
	protected StyledText displayArea;

	protected TXMResult currentResult;

	
	/**
	 * Creates the view.
	 */
	public TXMResultDebugView() {
		CorporaView.getInstance().getTreeViewer().addSelectionChangedListener(this);
	}

	@Override
	public void dispose() {
		CorporaView.getInstance().getTreeViewer().removeSelectionChangedListener(this);
	}

	@Override
	public void createPartControl(Composite parent) {
		displayArea = new StyledText(parent, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.WRAP);
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
	}

	/**
	 * Refreshes the view.
	 */
	public static void refreshView() {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		IWorkbenchPage page = window.getActivePage();
		TXMResultDebugView view = (TXMResultDebugView) page.findView(TXMResultDebugView.class.getName());
		if (view != null) {
			view.refresh();
		}
	}

	/**
	 * Refreshes the view.
	 */
	public void refresh() {

		if (this.currentResult == null) {
			this.displayArea.setText(""); //$NON-NLS-1$
			return;
		}

		StringBuffer buffer = new StringBuffer();
		buffer.append("Class: " + this.currentResult.getClass() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("Node path: " + this.currentResult.getParametersNodePath() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("Simple name: " + this.currentResult.getSimpleName() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("User name: " + this.currentResult.getUserName() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("Lazy name: " + this.currentResult.getLazyName() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("Name: " + this.currentResult.getName() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("Creation date: " + this.currentResult.getCreationDate() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("Last computing date: " + this.currentResult.getLastComputingDate() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("Valid filename: " + this.currentResult.getValidFileName() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("Empty name: " + this.currentResult.getEmptyName() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("toString(): " + this.currentResult.toString() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("Details: " + this.currentResult.getDetails() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("Dirty: " + this.currentResult.isDirty() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("Altered: " + this.currentResult.isAltered() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("Has been computed once: " + this.currentResult.hasBeenComputedOnce() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("Must be sync with parent: " + this.currentResult.mustBeSynchronizedWithParent() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		// if (this.currentResult instanceof ChartResult) {
		// buffer.append("Chart dirty: " + ((ChartResult)this.currentResult).isChartDirty() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		// }

		// Object parameters
		buffer.append("\n"); //$NON-NLS-1$
		buffer.append(this.currentResult.dumpParameters());

		// Command preferences
		buffer.append(this.currentResult.dumpPreferences());
		buffer.append("\n"); //$NON-NLS-1$

		// Parameters history stack
		this.currentResult.dumpParametersHistory(buffer);

		// Hierarchy
		buffer.append("\n"); //$NON-NLS-1$
		buffer.append("Selected object = " + this.currentResult + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("Node visible = " + this.currentResult.isVisible() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("Node weight = " + this.currentResult.getWeight() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("Internal persistable = " + this.currentResult.isInternalPersistable() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buffer.append("User persistable = " + this.currentResult.isUserPersistable() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		// if (this.currentResult instanceof ChartResult) {
		// buffer.append("Chart engine = " + ((ChartResult)this.currentResult).getChartsEngine() + ", chart object = " + ((ChartResult)this.currentResult).getChart()
		// + ", chart type = " + ((ChartResult)this.currentResult).getChartType()
		// + "\n");
		// }
		buffer.append("Selection full path name = " + this.currentResult.getFullPathSimpleName() + " - " + this.currentResult.getName() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		buffer.append("Direct children count = " + this.currentResult.getChildren().size() + ", direct visible children count = " + this.currentResult.getChildren(true).size() + ", children count = " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ this.currentResult.getDeepChildren().size() + "\n");  //$NON-NLS-1$
		buffer.append("Root parent = " + this.currentResult.getRootParent() + ", main corpus parent = " + CQPCorpus.getParentMainCorpus(this.currentResult) + ", first parent corpus = " + CQPCorpus //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				.getFirstParentCorpus(this.currentResult) + "\n");  //$NON-NLS-1$


		displayArea.setText(buffer.toString());
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {

		Object selection = CorporaView.getFirstSelectedObject();

		if (selection instanceof TXMResult) {
			this.currentResult = (TXMResult) selection;
			this.refresh();
		}
	}
}
