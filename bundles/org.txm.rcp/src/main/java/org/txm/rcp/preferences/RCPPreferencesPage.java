package org.txm.rcp.preferences;

import org.eclipse.ui.IWorkbench;

//TODO: MD: useless class since new preferences system
public abstract class RCPPreferencesPage extends TXMPreferencePage {

	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(RCPPreferences.getInstance().getPreferencesNodeQualifier()));
	}

	@Override
	protected abstract void createFieldEditors();
}
