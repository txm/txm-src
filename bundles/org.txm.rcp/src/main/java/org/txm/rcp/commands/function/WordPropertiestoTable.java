package org.txm.rcp.commands.function;

import java.io.File;
import java.util.HashMap;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.Toolbox;
import org.txm.rcp.handlers.scripts.ExecuteGroovyMacro;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

public class WordPropertiestoTable extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchPart part = HandlerUtil.getActivePart(event);
		IWorkbenchPart editor = HandlerUtil.getActiveEditor(event);
		IStructuredSelection selection = HandlerUtil.getCurrentStructuredSelection(event);

		Object first = selection.getFirstElement();
		if (!(first instanceof MainCorpus)) {
			Log.warning(TXMUIMessages.SelectionMustBeACorpusAborting);
			return null;
		}

		MainCorpus corpus = (MainCorpus) first;

		File script = new File(Toolbox.getTxmHomePath(), "/scripts/groovy/user/org/txm/macro/annotation/ExportWordPropertiesToTableMacro.groovy"); //$NON-NLS-1$
		//File parametersFile = new File(Toolbox.getTxmHomePath(), "/scripts/groovy/user/org/txm/macro/annotation/ExportWordPropertiesToTableMacro.properties");

		HashMap<String, Object> parameters = new HashMap<String, Object>();

		HashMap<String, Object> defaultParameters = new HashMap<String, Object>();
		defaultParameters.put("properties", "frpos,frlemma"); //$NON-NLS-1$ //$NON-NLS-2$
		defaultParameters.put("tsvFile", corpus.getName() + "_annotations.tsv"); //$NON-NLS-1$ //$NON-NLS-2$

		return ExecuteGroovyMacro.execute(script.getAbsolutePath(), part, editor, selection, null, parameters, defaultParameters); //$NON-NLS-1$
	}
}
