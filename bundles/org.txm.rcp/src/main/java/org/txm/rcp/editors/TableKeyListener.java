package org.txm.rcp.editors;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.menus.AbstractContributionFactory;
import org.eclipse.ui.menus.CommandContributionItem;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.menus.IContributionRoot;
import org.eclipse.ui.menus.IMenuService;
import org.eclipse.ui.services.IServiceLocator;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.widget.NewNavigationWidget;
import org.txm.rcp.utils.SWTUtils;
import org.txm.utils.PatternUtils;
import org.txm.utils.logger.Log;

public class TableKeyListener implements KeyListener {

	TableViewer[] viewers;

	public TableKeyListener(TableViewer... viewers) {

		this.viewers = viewers;
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {

		// System.out.println("key code: "+e.keyCode);
		if ((e.stateMask & SWT.CTRL) != 0 && e.keyCode == 99) { // CTRL + C
			copyLinesToClipboard(viewers);
		}
		else if ((e.stateMask & SWT.CTRL) != 0 && e.keyCode == 102) { // CTRL + F
			openCloseSearch();
		}
		else if ((e.stateMask & SWT.CTRL) != 0 && (e.stateMask & SWT.SHIFT) != 0 && e.keyCode == 103) { // CTRL + G
			goToPreviousSearchMatch();
			searchNavigation.refresh();
		}
		else if ((e.stateMask & SWT.CTRL) != 0 && e.keyCode == 103) { // CTRL + G
			goToNextSearchMatch();
			searchNavigation.refresh();
		}
		else if ((e.stateMask & SWT.COMMAND) != 0 && e.keyCode == 99) { // CMD + C
			copyLinesToClipboard(viewers);
		}
		else if ((e.stateMask & SWT.COMMAND) != 0 && e.keyCode == 102) { // CMD + F
			openCloseSearch();
		}
		else if ((e.stateMask & SWT.COMMAND) != 0 && (e.stateMask & SWT.SHIFT) != 0 && e.keyCode == 103) { // CTRL + G
			goToPreviousSearchMatch();
			searchNavigation.refresh();
		}
		else if ((e.stateMask & SWT.COMMAND) != 0 && e.keyCode == 103) { // CTRL + G
			goToNextSearchMatch();
			searchNavigation.refresh();
		}
	}

	LinkedHashMap<TableViewer, ArrayList<Integer>> matchingIndexes = new LinkedHashMap<>();

	Integer currentSearchMatch = 0;

	Integer totalNSearchMatches = 0;

	/**
	 * Opens a dialog box to enter a REGEX that selects lines in the table.
	 * 
	 * uses the viewers shell to open the dialog
	 */
	public void searchInLines(String reg, TableViewer[] viewers) {

		matchingIndexes.clear();
		totalNSearchMatches = 0;
		currentSearchMatch = 0;

		if (reg == null) {
			return;
		}

		Pattern p = null;
		int style = 0;
		if (caseSensitiveItem != null && !caseSensitiveItem.getSelection()) style = style | Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE;
		if (diacSensitiveItem != null && !diacSensitiveItem.getSelection()) {
			style = style | Pattern.UNICODE_CHARACTER_CLASS;
		}
		if (literalItem != null && literalItem.getSelection()) {
			style = style | Pattern.LITERAL;
		}

		try {
			p = Pattern.compile(reg, style);
		}
		catch (Exception e) { // avoid Exception if user miss write its REGEX or FORGET to use a REGEX
			p = Pattern.compile(PatternUtils.quote(reg), style);
		}

		for (TableViewer viewer : viewers) {

			if (viewer.getTable().isDisposed()) continue;

			ArrayList<Integer> viewerMatchingIndexes = new ArrayList<>();
			matchingIndexes.put(viewer, viewerMatchingIndexes);

			TableItem[] items = viewer.getTable().getItems();
			// System.out.println("search in tablein items: "+Arrays.toString(items));

			TableColumn[] cols = viewer.getTable().getColumns();

			// TODO: useless ?
			// ArrayList<TableItem> matchingItem = new ArrayList<TableItem>();

			for (int l = 0; l < items.length; l++) {
				TableItem item = items[l];
				for (int i = 0; i < cols.length; i++) {
					if (matchAllItem.getSelection()) {
						if (p.matcher(item.getText(i)).matches()) {
							viewerMatchingIndexes.add(l);
							totalNSearchMatches++;
							break; // next line
						}
					} else {
						if (p.matcher(item.getText(i)).find()) {
							viewerMatchingIndexes.add(l);
							totalNSearchMatches++;
							break; // next line
						}
					}
				}
			}
			Log.fine(TXMCoreMessages.bind(TXMUIMessages.P0LinesFound, matchingIndexes));

			// TODO: useless ?
			// int[] indexes = new int[matchingIndexes.size()];
			// TableItem[] mitems = new TableItem[matchingItem.size()];
			// int i = 0;
			// for(int index : matchingIndexes) {
			// indexes[i++] = index;
			// }
			// i = 0;
			// for(TableItem item : matchingItem) {
			// mitems[i++] = item;
			// }

			// TODO: old version which doesn't trigger selection event
			// if (mitems.length > 0) {
			// viewer.getTable().showItem(mitems[0]); // useless: viewer.getTable().setSelection(indexes) can replace these 3 lines of code
			// viewer.getTable().deselectAll(); // useless: viewer.getTable().setSelection(indexes) can replace these 3 lines of code
			// viewer.getTable().select(indexes); // useless: viewer.getTable().setSelection(indexes) can replace these 3 lines of code
		}
		if (getNumberOfMatches() == 0) {
			Log.info(NLS.bind(TXMUIMessages.noMatchFoundOfP0, reg));
		} else if (getNumberOfMatches() == 1) {
			Log.info(NLS.bind(TXMUIMessages.foundOnlyOneMatchOfP0, reg));
			goToSearchMatch(0);
		} else if (getNumberOfMatches() > 0) {
			Log.info(NLS.bind(TXMUIMessages.foundP0MatchesOfP1, getNumberOfMatches(), reg));
			goToSearchMatch(0);
		}

		if (searchNavigation != null && !searchNavigation.isDisposed()) {

			searchText.setText(reg);

			if (getNumberOfMatches() > 0) {
				searchNavigation.setMinimum(1);
				searchNavigation.setMaximum(getNumberOfMatches());
				searchNavigation.setCurrentPosition(0);
			}
			else {
				searchNavigation.setMinimum(0);
				searchNavigation.setMaximum(0);
			}
			searchNavigation.refresh();
		}
	}

	/**
	 * Opens a dialog box to enter a REGEX that selects lines in the table.
	 * 
	 * uses the viewers shell to open the dialog
	 * 
	 * note: openCloseSearch(); is preferred
	 */
	public void openSearchDialog(TableViewer[] viewers) {


		currentSearchMatch = 0;
		totalNSearchMatches = 0;
		InputDialog dialog = new InputDialog(viewers[0].getTable().getShell(), "Search in lines", TXMUIMessages.common_enterRegularExpressionOfYourSearch, "", null);  //$NON-NLS-1$ //$NON-NLS-2$
		if (dialog.open() == Window.OK) {

			String reg = dialog.getValue();

			searchInLines(reg, viewers);

			openSearch();
		}
	}

	public void goToPreviousSearchMatch() {

		goToSearchMatch(-1);
	}

	public void goToNextSearchMatch() {

		goToSearchMatch(1);
	}

	public void goToFirstSearchMatch() {
		currentSearchMatch = 0;
		goToSearchMatch(0);
	}

	public void goToLastSearchMatch() {

		currentSearchMatch = totalNSearchMatches - 1;
		goToSearchMatch(0);
	}

	public void goToSearchMatch(int incre) {

		// trigger selection event on viewer
		currentSearchMatch += incre;
		if (currentSearchMatch < 0) {
			currentSearchMatch = totalNSearchMatches - 1;
		}
		currentSearchMatch = currentSearchMatch % totalNSearchMatches;
		int tmp = 0;
		for (TableViewer viewer : matchingIndexes.keySet()) {

			if (viewer.getTable().isDisposed()) continue;

			ArrayList<Integer> viewerMatchingIndexes = matchingIndexes.get(viewer);

			if (currentSearchMatch < tmp + viewerMatchingIndexes.size()) { // contains the match

				StructuredSelection newSelection = new StructuredSelection(viewer.getElementAt(viewerMatchingIndexes.get(currentSearchMatch - tmp)));
				viewer.setSelection(newSelection, true);
				TableUtils.align(viewer, matchingIndexes.keySet());
				return;
			}

			tmp += matchingIndexes.get(viewer).size();
		}

		if (searchNavigation != null && !searchNavigation.isDisposed()) {
			searchNavigation.setCurrentPosition(getCurrentMatch());
			searchNavigation.refresh();
		}
	}

	/**
	 * Copies the current lines selection to the clipboard.
	 */
	public static void copyLinesToClipboard(TableViewer[] viewers) {

		if (viewers == null) return;
		if (viewers.length == 0) return;

		StringBuffer str = new StringBuffer();

		int nWritten = 0;
		for (TableViewer viewer : viewers) {

			if (viewer.getTable().isDisposed()) continue;

			TableColumn[] cols = viewer.getTable().getColumns();
			for (int i = 0; i < cols.length; i++) {
				if (cols[i].getText().trim().length() == 0) continue; // skip columns without name

				if (nWritten > 0) str.append("\t"); //$NON-NLS-1$
				str.append(cols[i].getText().replace("\n", " ").trim()); //$NON-NLS-1$ //$NON-NLS-2$
				nWritten++;
			}
		}
		str.append("\n"); //$NON-NLS-1$


		for (int l = 0; l < viewers[0].getTable().getSelection().length; l++) { // for each line

			if (l > 0) str.append("\n"); //$NON-NLS-1$
			nWritten = 0;

			for (TableViewer viewer : viewers) { // append the content of the viewers

				if (viewer.getTable().isDisposed()) continue;

				TableColumn[] cols = viewer.getTable().getColumns();

				TableItem[] items = viewer.getTable().getSelection();
				TableItem item = items[l];

				for (int i = 0; i < cols.length; i++) {
					if (cols[i].getText().trim().length() > 0) { // skip columns without name

						if (nWritten > 0) str.append("\t"); //$NON-NLS-1$
						str.append(item.getText(i));
						nWritten++;
					}
				}
			}
		}

		String strr = str.toString();
		org.txm.rcp.utils.IOClipboard.write(strr);
		System.out.println(TXMCoreMessages.bind(TXMUIMessages.copiedLinesColonP0, strr));
	}

	public int getNumberOfMatches() {

		return totalNSearchMatches;
	}

	public int getCurrentMatch() {

		return currentSearchMatch;
	}

	private NewNavigationWidget searchNavigation;

	private Text searchText;

	private Button optionsButton;

	private Button searchButton;

	private GLComposite lowerComposite;

	private Button startSearchButton;

	private MenuItem matchAllItem;

	private MenuItem caseSensitiveItem;
	
	private MenuItem literalItem;

	private MenuItem diacSensitiveItem;

	public void installSearchGroup(TXMEditor<?> txmEditor) {

		//		Composite searchGroup = txmEditor.getBottomToolbar().installGroup("Search", "Search in the table",
		//				IImageKeys.ACTION_SEARCH,  // $NON-NLS-1$
		//				IImageKeys.ACTION_SEARCH,  // $NON-NLS-1$
		//				false, false);

		GLComposite bottomArea = txmEditor.getBottomToolbar().getSubWidgetComposite();

		txmEditor.getBottomToolbar().getSubWidgetComposite().getLayout().numColumns++;



		searchButton = new Button(bottomArea, SWT.PUSH);
		searchButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false));
		searchButton.setToolTipText(TXMUIMessages.tableSearch_tooltip_searchWithinTheTable);

		lowerComposite = new GLComposite(bottomArea, SWT.NONE, "search"); //$NON-NLS-1$
		lowerComposite.getLayout().numColumns = 10;
		lowerComposite.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		Control[] childrencontrols = lowerComposite.getParent().getChildren();
		lowerComposite.moveAbove(childrencontrols[0]);
		searchButton.moveAbove(lowerComposite);

		optionsButton = new Button(lowerComposite, SWT.PUSH);
		optionsButton.setImage(IImageKeys.getImage("platform:/plugin/org.eclipse.jface/org/eclipse/jface/dialogs/images/popup_menu.png")); //$NON-NLS-1$
		GridData gdata2 = new GridData(GridData.BEGINNING, GridData.CENTER, false, false);
		//		gdata2.widthHint = 30;
		optionsButton.setLayoutData(gdata2);
		optionsButton.setToolTipText(TXMUIMessages.tooltip_searchOptions);

		Menu menu = new Menu(optionsButton);
//		menu.addListener(SWT.Show, new Listener() {
//			@Override
//			public void handleEvent(Event event) {
//				
//			}
//		});
		matchAllItem = new MenuItem(menu, SWT.CHECK);
		matchAllItem.setText(TXMUIMessages.matchAll);
		caseSensitiveItem = new MenuItem(menu, SWT.CHECK);
		caseSensitiveItem.setText(TXMUIMessages.caseSensitive);
		caseSensitiveItem.setSelection(false);
		literalItem = new MenuItem(menu, SWT.CHECK);
		literalItem.setText(TXMUIMessages.literal);
		literalItem.setSelection(false);
//		diacSensitiveItem = new MenuItem(menu, SWT.CHECK);
//		diacSensitiveItem.setText("Diacritic sensitive");

		optionsButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				menu.setVisible(true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});
		searchText = new Text(lowerComposite, SWT.BORDER);
		GridData gdata = new GridData(GridData.BEGINNING, GridData.CENTER, false, false);
		gdata.widthHint = gdata.minimumWidth = SWTUtils.getOptimalWidth(lowerComposite, 20);
		searchText.setLayoutData(gdata);
		searchText.setToolTipText(TXMUIMessages.tableSearch_tooltip_regularExpressionOfTheValueToBeSearchedFor);

		startSearchButton = new Button(lowerComposite, SWT.PUSH);
		startSearchButton.setImage(IImageKeys.getImage(IImageKeys.START_T));
		startSearchButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false));
		startSearchButton.setToolTipText(TXMUIMessages.tableSearch_tooltip_startSearchInTable);
		startSearchButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				searchInLines(searchText.getText(), viewers);
			}
		});

		searchNavigation = new NewNavigationWidget(lowerComposite);
		searchNavigation.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false));
		closeSearch();

		searchText.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					searchInLines(searchText.getText(), viewers);
				}
				else if (e.keyCode == SWT.ESC) {
					closeSearch();
				}
				else if ((e.stateMask & SWT.CTRL) != 0 && (e.stateMask & SWT.SHIFT) != 0 && e.keyCode == 103) { // CTRL + G
					goToPreviousSearchMatch();
				}
				else if ((e.stateMask & SWT.CTRL) != 0 && e.keyCode == 103) { // CTRL + G
					goToNextSearchMatch();
				}
				else if ((e.stateMask & SWT.COMMAND) != 0 && (e.stateMask & SWT.SHIFT) != 0 && e.keyCode == 103) { // CTRL + G
					goToPreviousSearchMatch();
				}
				else if ((e.stateMask & SWT.COMMAND) != 0 && e.keyCode == 103) { // CTRL + G
					goToNextSearchMatch();
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		searchButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				openCloseSearch();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		searchNavigation.addFirstListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				goToFirstSearchMatch();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		searchNavigation.addPreviousListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				goToPreviousSearchMatch();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		searchNavigation.addNextListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				goToNextSearchMatch();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		searchNavigation.addLastListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				goToLastSearchMatch();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}


	public void openCloseSearch() {

		if (searchText.isVisible()) {
			closeSearch();
		}
		else {
			openSearch();
		}
	}

	protected void openSearch() {

		optionsButton.setEnabled(true);
		searchButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_DELETE));
		lowerComposite.setVisible(true);
		((GridData) lowerComposite.getLayoutData()).widthHint = ((GridData) lowerComposite.getLayoutData()).minimumWidth = SWT.DEFAULT;
		searchText.setEnabled(true);
		startSearchButton.setEnabled(true);
		searchNavigation.setEnabled(true);

		searchButton.getParent().layout();
		searchButton.getParent().getParent().layout();
		searchText.setFocus();
	}

	protected void closeSearch() {

		optionsButton.setEnabled(false);

		searchButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_SEARCH_TABLE));

		lowerComposite.setVisible(false);
		((GridData) lowerComposite.getLayoutData()).widthHint = ((GridData) lowerComposite.getLayoutData()).minimumWidth = 0;

		searchText.setEnabled(false);
		startSearchButton.setEnabled(false);
		searchNavigation.setEnabled(false);

		searchNavigation.layout();
		searchButton.getParent().layout();
		searchButton.getParent().getParent().layout();
		for (TableViewer viewer : viewers) {
			if (!viewer.getTable().isDisposed()) {
				viewer.getTable().setFocus();
			}
		}
	}

	public static HashSet<String> popUpIds = new HashSet<String>();

	public void installMenuContributions(TXMEditor<?> editor) {

		IWorkbenchPartSite site = editor.getSite();
		String popUpId = "popup:" + site.getId(); //$NON-NLS-1$

		if (popUpIds.contains(popUpId)) return; // avoid reinstalling this contribution
		popUpIds.add(popUpId);

		AbstractContributionFactory contribFactory = new AbstractContributionFactory(popUpId, null) {

			@Override
			public void createContributionItems(IServiceLocator serviceLocator, IContributionRoot additions) {

				CommandContributionItemParameter p = new CommandContributionItemParameter(serviceLocator, "", org.txm.rcp.handlers.SearchInTableResultEditor.class.getName(), CommandContributionItem.STYLE_PUSH); //$NON-NLS-1$
				p.icon = IImageKeys.getImageDescriptor(IImageKeys.ACTION_SEARCH_TABLE);

				CommandContributionItem item = new CommandContributionItem(p);
				item.setVisible(true);
				additions.addContributionItem(item, null);

				CommandContributionItemParameter p2 = new CommandContributionItemParameter(serviceLocator, "", org.txm.rcp.handlers.CopyLinesFromTableResultEditor.class.getName(), CommandContributionItem.STYLE_PUSH); //$NON-NLS-1$
				p2.icon = IImageKeys.getImageDescriptor(IImageKeys.ACTION_COPY);

				CommandContributionItem item2 = new CommandContributionItem(p2);
				item2.setVisible(true);
				additions.addContributionItem(item2, null);

				if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {

					CommandContributionItemParameter p3 = new CommandContributionItemParameter(serviceLocator, "", "org.txm.statsengine.r.rcp.handlers.HistogramFromTableResultEditor", //$NON-NLS-1$ //$NON-NLS-2$
							CommandContributionItem.STYLE_PUSH);
					p3.icon = IImageKeys.getImageDescriptor(IImageKeys.ACTION_HISTO);

					CommandContributionItem item3 = new CommandContributionItem(p3);
					item3.setVisible(true);
					additions.addContributionItem(item3, null);
				}
			}
		};

		IMenuService menuService = editor.getSite().getService(IMenuService.class);
		menuService.addContributionFactory(contribFactory);
	}
}
