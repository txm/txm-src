package org.txm.rcp.swt.provider;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.graphics.Image;
import org.txm.rcp.messages.TXMUIMessages;
import org.w3c.dom.Element;

public class ElementTreeLabelProvider extends ColumnLabelProvider {

	String parentName;

	String[] parentAttrs;

	String childName;

	String[] childAttrs;

	int index = 0;

	public ElementTreeLabelProvider(int column, String parentName, String childName, String[] parentAttrs, String[] childAttrs) {
		this.parentName = parentName;
		this.childName = childName;
		this.parentAttrs = parentAttrs;
		this.childAttrs = childAttrs;
		this.index = column;
	}

	@Override
	public String getText(Object obj) {
		if (obj instanceof Element) {
			Element elem = (Element) obj;
			if (elem.getNodeName().equals(parentName)) {
				return show(elem, index, parentAttrs);
			}
			else if (elem.getNodeName().equals(childName)) {
				return show(elem, index, childAttrs);
			}
			else {
				return TXMUIMessages.errorColonElementNotRegistered;
			}

		}
		return TXMUIMessages.errorColonObjectIsNotAnOrgw3cElement;
	}

	private String show(Element elem, int index, String[] attrs) {
		if (index >= attrs.length) return ""; //$NON-NLS-1$
		return elem.getAttribute(attrs[index]);
	}

	public Image getColumnImage(Object obj, int index) {
		return null;
	}

	@Override
	public Image getImage(Object obj) {
		return null;
	}
}
