package org.txm.rcp.testers;

import java.util.List;

import org.eclipse.core.expressions.PropertyTester;
import org.txm.Toolbox;
import org.txm.core.engines.EngineType;
import org.txm.core.results.TXMResult;
import org.txm.searchengine.cqp.CQPSearchEngine;

/**
 * PropertyTester to check Toolbox states
 * 
 * @author mdecorde
 *
 */
public class ToolboxTester extends PropertyTester {

	public static final String PROPERTY_NAMESPACE = "org.txm.rcp.testers"; //$NON-NLS-1$

	public static final String PROPERTY_STATE_ENGINE_READY = "StateEngineReady"; //$NON-NLS-1$

	public static final String PROPERTY_SEARCH_ENGINE_READY = "SearchEngineReady"; //$NON-NLS-1$

	public static final String PROPERTY_COMPUTABLE = "computable"; //$NON-NLS-1$

	public static final String PROPERTY_PERSISTABLE = "persistable"; //$NON-NLS-1$

	public static final String PROPERTY_LOCK = "lock"; //$NON-NLS-1$

	public ToolboxTester() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		if (PROPERTY_STATE_ENGINE_READY.equals(property)) {
			//System.out.println("TEST: "+property+" = "+Toolbox.getEngineManager(EngineType.STAT).getEngine("R").getState()); //$NON-NLS-1$ //$NON-NLS-2$
			return Toolbox.getEngineManager(EngineType.STATS).getEngine("R").isRunning(); //$NON-NLS-1$
		}
		else if (PROPERTY_SEARCH_ENGINE_READY.equals(property)) {
			System.out.println("TEST: " + property + " = " + CQPSearchEngine.isInitialized()); //$NON-NLS-1$ //$NON-NLS-2$
			return CQPSearchEngine.isInitialized();
		}
		else if (PROPERTY_COMPUTABLE.equals(property)) {
			System.out.println("TEST: receiver=" + receiver + " property=" + property + " args=" + args + " expectedValue=" + expectedValue); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			return false;
		}
		else if (PROPERTY_PERSISTABLE.equals(property)) {
			if (receiver == null) return false;
			if (!(receiver instanceof List)) return false;

			List<?> list = (List<?>) receiver;
			if (list.size() == 0) return false;
			receiver = list.get(0);

			if (receiver instanceof TXMResult) {
				return !((TXMResult) receiver).isInternalPersistable();
				//return true;
			}
			return false;
			//			System.out.println("TEST: receiver="+receiver+" property="+property+" args="+args+" expectedValue="+expectedValue); //$NON-NLS-1$ //$NON-NLS-2$
		}
		else if (PROPERTY_LOCK.equals(property)) {
			if (receiver == null) return false;
			if (!(receiver instanceof List)) return false;

			List<?> list = (List) receiver;
			if (list.size() == 0) return false;
			receiver = list.get(0);

			if (receiver instanceof TXMResult) {
				return !((TXMResult) receiver).isLocked();
				//return true;
			}
			return false;
			//			System.out.println("TEST: receiver="+receiver+" property="+property+" args="+args+" expectedValue="+expectedValue); //$NON-NLS-1$ //$NON-NLS-2$
		}

		return false;
	}
}
