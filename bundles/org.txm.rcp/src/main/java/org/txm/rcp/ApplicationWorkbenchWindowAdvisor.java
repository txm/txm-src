// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.eclipse.e4.ui.model.application.ui.SideValue;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimBar;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimElement;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimmedWindow;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.model.application.ui.menu.MToolControl;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.eclipse.ui.internal.WorkbenchWindow;
import org.eclipse.ui.model.ContributionComparator;
import org.eclipse.ui.model.IContributionService;
import org.txm.Toolbox;
import org.txm.annotation.core.AnnotationEngine;
import org.txm.annotation.core.AnnotationEnginesManager;
import org.txm.core.engines.EngineType;
import org.txm.core.results.TXMResult;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Project;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.TXMFontRegistry;
import org.txm.utils.logger.Log;

/**
 * Configures TXM Windows
 */
public class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

	/**
	 * Instantiates a new application workbench window advisor.
	 *
	 * @param configurer the configurer
	 */
	public ApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
		super(configurer);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.application.WorkbenchWindowAdvisor#preWindowOpen()
	 */
	@Override
	public void preWindowOpen() {

		IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
		configurer.setShowProgressIndicator(true);
		configurer.setInitialSize(new Point(1024, 768));
		configurer.setShowMenuBar(true);
		configurer.setShowCoolBar(true);
		configurer.setShowStatusLine(true);
		//configurer.setShowFastViewBars(false);
		configurer.setShowProgressIndicator(true);
		configurer.setShowPerspectiveBar(true);
		configurer.setTitle(TXMUIMessages.tXM);

		// This is a patch to remove the Quick Search Field and spacers from the Main toolbar
		Log.fine(TXMUIMessages.removeUnnecessaryE4Elements);
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window instanceof WorkbenchWindow) {
			MWindow model = ((WorkbenchWindow) window).getModel();
			EModelService modelService = model.getContext().get(EModelService.class);

			MTrimBar trimBar = modelService.getTrim((MTrimmedWindow) model, SideValue.TOP);
			Iterator<MTrimElement> it = trimBar.getChildren().iterator();
			while (it.hasNext()) {
				MTrimElement next = it.next();
				// System.out.println("Element: "+next.getElementId());
				if ("com.none.playActionSet".equals(next.getElementId())) continue; //$NON-NLS-1$
				if ("org.eclipse.search.searchActionSet".equals(next.getElementId())) continue; //$NON-NLS-1$
				if ("org.eclipse.ui.edit.text.actionSet.annotationNavigation".equals(next.getElementId())) continue; //$NON-NLS-1$
				if ("org.eclipse.ui.edit.text.actionSet.navigation".equals(next.getElementId())) continue; //$NON-NLS-1$
				if ("org.eclipse.search.searchActionSet".equals(next.getElementId())) continue; //$NON-NLS-1$
				if ("org.eclipse.debug.ui.launchActionSet".equals(next.getElementId())) continue; //$NON-NLS-1$
				if ("org.eclipse.ui.browser.editor".equals(next.getElementId())) continue; //$NON-NLS-1$
				// Team
				// if ("org.eclipse.team.svn.ui.SVNTeamPreferences".equals(next.getElementId())) continue;
				if ("group.editor".equals(next.getElementId())) continue; //$NON-NLS-1$
				if ("SearchField".equals(next.getElementId())) continue; //$NON-NLS-1$
				// if ("PerspectiveSpacer".equals(next.getElementId())) continue;
				// if ("Spacer Glue".equals(next.getElementId())) continue;
				// if ("Search-PS Glue".equals(next.getElementId())) continue;
				if ("PerspectiveSwitcher".equals(next.getElementId())) continue; //$NON-NLS-1$
				// System.out.println("DEL Element: "+next.getElementId());
				if (next instanceof MToolControl) next.setToBeRendered(false);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.application.WorkbenchWindowAdvisor#postWindowOpen()
	 */
	@Override
	public void postWindowOpen() {

		//getWindowConfigurer().getWindow().getShell().setMaximized( true );

		Log.fine(TXMUIMessages.settingStatusLineAndConfiguringActionSets);
		StatusLine.set(getWindowConfigurer().getActionBarConfigurer().getStatusLineManager());
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		page.hideActionSet("org.eclipse.ui.edit.text.actionSet.navigation"); //$NON-NLS-1$
		page.hideActionSet("org.eclipse.ui.edit.text.actionSet.annotationNavigation"); //$NON-NLS-1$
		page.hideActionSet("org.eclipse.ui.run"); //$NON-NLS-1$
		// page.hideActionSet("org.eclipse.team.svn.ui.SVNTeamPreferences"); //$NON-NLS-1$
		// StatusLine.setMessage(Messages.ApplicationWorkbenchWindowAdvisor_statusready);

		IContributionItem[] mItems, mSubItems;
		IMenuManager mm = getWindowConfigurer().getActionBarConfigurer().getMenuManager();
		mm.remove("org.eclipse.ui.run"); //$NON-NLS-1$
		// mItems = mm.getItems ();
		// for (int i = 0; i < mItems.length; i++)
		// {
		// if (mItems[i] instanceof MenuManager)
		// {
		// mSubItems = ((MenuManager) mItems[i]).getItems ();
		// for (int j = 0; j < mSubItems.length; j++)
		// {
		// System.out.println("menu : "+mItems[i].getId());
		// if (mItems[i].getId ().equals ("file"))
		// ((MenuManager) mItems[i]).remove ("org.eclipse.ui.openLocalFile");
		// else if (mItems[i].getId ().equals ("help"))
		// {
		// ((MenuManager) mItems[i]).remove ("group.updates");
		// ((MenuManager) mItems[i]).remove ("org.eclipse.update.ui.updateMenu");
		// ((MenuManager) mItems[i]).remove ("org.eclipse.ui.actions.showKeyAssistHandler");
		// }
		// }
		// }
		// }
	}

	/**
	 * Check if some annotations are not saved and ask the user if he really wish to close TXM.
	 *
	 * @return <code>true</code> to allow the window to close, and
	 *         <code>false</code> if some annotations are not saved and the user wants to save them before
	 * @see org.eclipse.ui.IWorkbenchWindow#close
	 * @see WorkbenchAdvisor#preShutdown()
	 */
	public boolean preWindowShellClose() {

		ArrayList<TXMResult> altered = new ArrayList<>();
		for (Project project : Toolbox.workspace.getProjects()) {
			for (TXMResult result : project.getDeepChildren()) {
				if (result.isAltered()) {
					altered.add(result);
				}
			}
		}

		AnnotationEnginesManager aem = (AnnotationEnginesManager) Toolbox.getEngineManager(EngineType.ANNOTATION);
		HashMap<CorpusBuild, ArrayList<AnnotationEngine>> notSaved = aem.getEnginesWithSaveNeededPerCorpus();

		StringBuilder mess = new StringBuilder();

		if (altered.size() > 0) {

			mess.append(TXMUIMessages.SomeResultHaveBeenMannuallyModified);
			for (TXMResult result : altered) {
				mess.append("\n- " + result.getName() + " (" + result.getResultType() + ") : " + result.getSimpleDetails()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			mess.append("\n\n" + TXMUIMessages.YouReGoingToLostThoseModificationsIfNotAlreadyExportedContinue); //$NON-NLS-1$ //$NON-NLS-2$
			//return MessageDialog.openConfirm(this.getWindowConfigurer().getWindow().getShell(), TXMUIMessages.SomeResultHaveBeenManuallyModified, mess.toString());
		}

		if (notSaved.size() > 0) {
			if (mess.length() > 0) mess.append("\n\n"); //$NON-NLS-1$
			mess.append(TXMUIMessages.SomeAnnotationsAreNotSavedAndWillBeLostAfterTXMIsClosed);
			for (CorpusBuild corpus : notSaved.keySet()) {
				mess.append("\n- " + corpus.getName() + ": " + corpus.getSimpleDetails()); //$NON-NLS-1$ //$NON-NLS-2$
				for (AnnotationEngine a : notSaved.get(corpus)) {
					mess.append("\n  - " + a.getName()); //$NON-NLS-1$
				}
			}
			mess.append("\n" + TXMUIMessages.ContinueAndLostTheUnsavedAnnotations); //$NON-NLS-1$
		}

		if (altered.size() > 0 || notSaved.size() > 0) {
			String title = ""; //$NON-NLS-1$
			if (altered.size() > 0) title += TXMUIMessages.SomeResultHaveBeenManuallyModified;
			if (notSaved.size() > 0) {
				if (title.length() > 0) title += " & "; //$NON-NLS-1$
				title += TXMUIMessages.SomeAnnotationsAreNotSaved;
			}

			mess.append("\n\nContinue ?");

			if (!MessageDialog.openConfirm(this.getWindowConfigurer().getWindow().getShell(), title, mess.toString())) {
				return false;
			}
		}

		TXMFontRegistry.dispose();

		return true;
	}
}
