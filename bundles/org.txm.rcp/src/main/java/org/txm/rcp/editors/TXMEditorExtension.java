package org.txm.rcp.editors;

import java.util.Set;

import org.txm.core.results.TXMResult;

/**
 * Extension point class to add a stuff to the TXMResult editor: Hook for editors life cycle
 * 
 * All widgets creation are done after the editor widgets creation
 * 
 * @author mdecorde
 *
 */
public abstract class TXMEditorExtension<T extends TXMResult> {

	public static final String EXTENSION_ID = TXMEditorExtension.class.getName();

	protected TXMEditor<T> editor;

	public TXMEditorExtension() {

	}

	public void setEditor(TXMEditor<T> txmEditor) {
		this.editor = txmEditor;
	}

	/**
	 * 
	 * @return null or the set of TXMResult that can be extended in their editor
	 */
	public abstract Set<Class<? extends TXMResult>> getTXMResultValidClasses();

	/**
	 * 
	 * @return the extension name
	 */
	public abstract String getName();

	/**
	 * implement this command to do something before TXMEditor._createPartControl() is called
	 */
	public abstract void notifyStartOfCreatePartControl() throws Exception;

	/**
	 * implement this command to do something after TXMEditor._createPartControl() is called
	 */
	public abstract void notifyEndOfCreatePartControl() throws Exception;

	/**
	 * implement this command to do something before TXMEditor._compute() is called
	 */
	public abstract void notifyStartOfCompute() throws Exception;

	/**
	 * implement this command to do something after TXMEditor._compute() is called
	 */
	public abstract void notifyEndOfCompute() throws Exception;

	/**
	 * notify the extension the editor is being refreshed
	 * 
	 * @throws Exception
	 */
	public abstract void notifyStartOfRefresh() throws Exception;

	/**
	 * notify the extension the editor has been refreshed
	 * 
	 * @throws Exception
	 */
	public abstract void notifyEndOfRefresh() throws Exception;

	/**
	 * 
	 * @return true if an internal result of the extension is dirty
	 * @throws Exception
	 */
	public abstract boolean isDirty() throws Exception;

	/**
	 * Checks if the extension needs to save the editor on close.
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract boolean isSaveOnCloseNeeded() throws Exception;

	/**
	 * Save something when the doSave method is called
	 * 
	 * @throws Exception
	 */
	public abstract void notifyDoSave() throws Exception;

	/**
	 * Do something when the editor is closing
	 * 
	 * @throws Exception
	 */
	public abstract void notifyDispose() throws Exception;

	public void notify(String step) {
		// TODO Auto-generated method stub
	}

	public void discardChanges() {
		// TODO Auto-generated method stub
	}
}
