package org.txm.rcp.swt.provider;

import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.w3c.dom.NodeList;

public class NodeListContentProvider implements IContentProvider, IStructuredContentProvider {

	@Override
	public Object[] getElements(Object inputElement) {
		NodeList list = (NodeList) inputElement;
		Object[] objs = new Object[list.getLength()];
		for (int i = 0; i < list.getLength(); i++)
			objs[i] = list.item(i);
		return objs;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub

	}
}
