// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands.workspace;

import java.io.File;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.osgi.util.NLS;
import org.txm.Toolbox;
import org.txm.objects.BaseOldParameters;
import org.txm.objects.Project;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.DeleteDir;
import org.txm.utils.logger.Log;
import org.txm.utils.zip.Zip;

/**
 * Create a base from binary files of a base.
 *
 * @author mdecorde
 */
public class Load079BinaryCorpus {

	public static boolean canLoad(File zipFile) {
		String basedirname = Zip.getRoot(zipFile);
		return Zip.hasEntries(zipFile, basedirname + "/import.xml", basedirname + "/data/", basedirname + "/txm/", basedirname + "/HTML/", basedirname + "/registry/"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
	}

	/**
	 * Load a 0.7.9 binary corpus archive:
	 * 
	 * 1- test if the corpus directory is already used
	 * 2- delete previous corpus version
	 * 3- unzip the archive
	 * 4- load the binary corpus directory
	 * 5- restart TXM engines
	 * 
	 * @param replace
	 * @throws Exception
	 */
	public static Project loadBinaryCorpusArchive(final File zipFile, IProgressMonitor monitor, boolean replace) throws Exception {

		final File corporaDir = Toolbox.workspace.getLocation();
		corporaDir.mkdir();

		if (!canLoad(zipFile)) {
			System.out.println(TXMUIMessages.binaryCorpusIsNotATXM079CorpusNoImportXmlFile);
			return null;
		}

		System.out.println(NLS.bind(TXMUIMessages.theP0BinaryCorpusVersionIsP1, zipFile.getName(), "0.7.9")); //$NON-NLS-1$

		String basedirname = Zip.getRoot(zipFile);

		Project p = Toolbox.workspace.getProject(basedirname.toUpperCase());
		if (p != null) {
			if (replace) {
				p.delete();
			}
			else {
				System.out.println(NLS.bind(TXMUIMessages.abortingLoadingOfP0ACorpusWithTheSameNameAlreadyExistsInP1, zipFile, basedirname.toUpperCase()));
				return null;
			}
		}

		File corpusDirectory = new File(corporaDir, basedirname);
		if (replace) {
			DeleteDir.deleteDirectory(corpusDirectory);
		}
		else {
			if (corpusDirectory.exists()) {
				System.out.println(NLS.bind(TXMUIMessages.abortingLoadingOfP0ACorpusWithTheSameNameAlreadyExistsInP1, zipFile, corpusDirectory));
				return null;
			}
		}

		// ZIPPED FILE
		monitor.beginTask(TXMUIMessages.loadingBinaryCorpus, 100);
		monitor.subTask(TXMUIMessages.extractingBinaryCorpus);
		try {
			// System.out.println(NLS.bind(Messages.AddBase_29, zipFile, corporaDir));
			Zip.decompress(zipFile, corporaDir, false, monitor);
			if (!basedirname.equals(basedirname.toUpperCase())) { // manage old corpus with lower cased names
				new File(corporaDir, basedirname).renameTo(new File(corporaDir, basedirname.toUpperCase()));
				basedirname = basedirname.toUpperCase();
			}
		}
		catch (Exception e) {
			System.out.println(NLS.bind(TXMUIMessages.couldNotUnzipBinaryCorpusColonP0, e));
			org.txm.utils.logger.Log.printStackTrace(e);
			return null;
		}
		monitor.worked(50);

		monitor.subTask(TXMUIMessages.loadingTheCorpusInThePlatform);
		return loadBinaryCorpusAsDirectory(new File(corporaDir, basedirname), monitor);
	}

	/**
	 * load the content of the file if its a directory containing at least the sub folders :
	 * txm, html, data and registry.
	 *
	 * @param binCorpusDirectory the basedir
	 * @return the base
	 * @throws Exception the exception
	 */
	public static Project loadBinaryCorpusAsDirectory(File binCorpusDirectory, IProgressMonitor monitor) throws Exception {
		if (!(binCorpusDirectory.exists() && binCorpusDirectory.isDirectory())) {
			Log.severe(NLS.bind(TXMUIMessages.failedToLoadCorpusP0, binCorpusDirectory.getAbsolutePath()));
			System.out.println(NLS.bind(TXMUIMessages.failedToLoadCorpusP0, binCorpusDirectory.getAbsolutePath()));
			return null;
		}

		// File txmregistry = new File(Toolbox.getTxmHomePath(), "registry"); //$NON-NLS-1$
		// txmregistry.mkdir();
		File txmcorpora = new File(Toolbox.getTxmHomePath(), "corpora"); //$NON-NLS-1$
		txmcorpora.mkdir();
		Project project = Toolbox.workspace.scanDirectory079(binCorpusDirectory);
		if (project == null) {
			Log.severe(NLS.bind(TXMUIMessages.failedToLoadCorpusFromDirectoryColonP0WeCannotFindTheNecessaryComponents, binCorpusDirectory));
			return null;
		}

		MainCorpus c = new MainCorpus(project);
		BaseOldParameters params = new BaseOldParameters(new File(binCorpusDirectory, "import.xml")); //$NON-NLS-1$
		params.load();
		c._load(params.getCorpusElement());
		c.setDirty();
		if (c.compute(true)) {
			project.save();
			return project;
		}
		else {
			project.delete();
			return null;
		}
	}
}
