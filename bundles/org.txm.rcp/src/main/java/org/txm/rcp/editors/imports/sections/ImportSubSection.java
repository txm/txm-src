package org.txm.rcp.editors.imports.sections;

import org.eclipse.swt.SWT;
import org.txm.objects.Project;
import org.txm.rcp.swt.GLComposite;

public abstract class ImportSubSection extends GLComposite {

	protected ImportEditorSection isection;

	public ImportSubSection(ImportEditorSection isection, String name) {

		super(isection.section, SWT.NONE, name);
		this.isection = isection;
	}

	public abstract void updateFields(Project project);

	public abstract boolean saveFields(Project project);

	public abstract boolean checkFields();
}
