package org.txm.rcp.editors.imports;


/**
 * Section configurator that adds section or subsection (a part of section)
 * 
 * @author mdecorde
 *
 */
public abstract class ImportEditorSectionConfigurator {

	public ImportEditorSectionConfigurator() {

	}

	public abstract void installSections();
}
