package org.txm.rcp.corpuswizard;

import java.io.File;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.txm.Toolbox;
import org.txm.objects.Project;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.RCPPreferences;
import org.txm.utils.AsciiUtils;
import org.txm.utils.logger.Log;

public class SourceDirectoryPage extends WizardPage {

	private Text corpusName;

	private Label sourceDirectory;
	// private Text descriptionText;

	private Composite container;

	private Label newOrEdit, corpusNameLabel;

	public SourceDirectoryPage() {
		super(TXMUIMessages.SourceDirectoryPage_0);
		setTitle(TXMUIMessages.SourceDirectoryPage_1);
		setDescription(TXMUIMessages.SourceDirectoryPage_2);
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 3;

		GridData gd = new GridData(GridData.FILL_HORIZONTAL);

		Label label2 = new Label(container, SWT.NONE);
		label2.setText(TXMUIMessages.SourceDirectoryPage_3);

		Button button = new Button(container, SWT.PUSH);
		button.setText("..."); //$NON-NLS-1$
		button.setToolTipText(TXMUIMessages.SourceDirectoryPage_5);
		button.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				DirectoryDialog dialog = new DirectoryDialog(e.display.getActiveShell());
				String path = getSourcePath();
				dialog.setFilterPath(path);
				dialog.setText(TXMUIMessages.SourceDirectoryPage_6);
				if (path.length() > 0) {
					dialog.setFilterPath(path);
				}
				path = dialog.open();
				if (path != null) {
					sourceDirectory.setText(path);
					RCPPreferences.getInstance().put("lastSourcesLocation", path); //$NON-NLS-1$
					RCPPreferences.getInstance().flush();

					String fixed = AsciiUtils.buildId(new File(path).getName()).toUpperCase();
					if (fixed.length() == 0) {
						Log.warning(
								TXMUIMessages.SourceDirectoryPage_8);
					}
					corpusName.setText(fixed);

					setPageComplete(isCompleted());
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		sourceDirectory = new Label(container, SWT.SINGLE);
		final String lastSourcesLocation = RCPPreferences.getInstance().getString("lastSourcesLocation"); //$NON-NLS-1$
		sourceDirectory.setText(lastSourcesLocation);
		sourceDirectory.setLayoutData(gd);
		// sourceDirectory.addModifyListener(new ModifyListener() {
		//
		// @Override
		// public void modifyText(ModifyEvent e) {
		// corpusName.setText(AsciiUtils.buildId(new File(getSourcePath()).getName()).toUpperCase());
		// setPageComplete(isCompleted());
		// }
		// });


		Label label1 = new Label(container, SWT.NONE);
		label1.setText(TXMUIMessages.SourceDirectoryPage_10);

		corpusName = new Text(container, SWT.BORDER | SWT.SINGLE);
		corpusName.setLayoutData(GridDataFactory.fillDefaults().span(2, 1).create());
		corpusName.setText(AsciiUtils.buildId(new File(lastSourcesLocation).getName()).toUpperCase());
		corpusName.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				setPageComplete(isCompleted());
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		new Label(container, SWT.NONE);

		newOrEdit = new Label(container, SWT.NONE);
		newOrEdit.setLayoutData(GridDataFactory.fillDefaults().span(3, 1).create());

		corpusNameLabel = new Label(container, SWT.NONE);
		corpusNameLabel.setLayoutData(GridDataFactory.fillDefaults().span(3, 1).create());

		// label1 = new Label(container, SWT.NONE);
		// label1.setText("Description");
		//
		// descriptionText = new Text(container, SWT.BORDER | SWT.MULTI);
		// descriptionText.setText("\n\n\n\n\n");
		// descriptionText.setText("");
		// descriptionText.setToolTipText("The corpus description in HTML format");
		// gd = new GridData(GridData.FILL_HORIZONTAL);
		// gd.grabExcessVerticalSpace = true;
		// descriptionText.setLayoutData(gd);

		// required to avoid an error in the system
		setControl(container);
		setPageComplete(isCompleted());
	}

	protected boolean isCompleted() {
		if (corpusName.getText().length() > 0) {
			// File srcProjectLink = ResourcesPlugin.getWorkspace().getRoot().getProject(corpusName.getText()).getFolder("src").getRawLocation().toFile();
			if (ResourcesPlugin.getWorkspace().getRoot().getProject(getCorpusName()).exists()) {
				Project p = Toolbox.workspace.getProject(getCorpusName());
				if (p != null && p.getChildren().size() > 0) {
					newOrEdit.setText(NLS.bind(TXMUIMessages.theP0CorpusWillBeReplaced, getCorpusName()));
				}
				else {
					newOrEdit.setText(NLS.bind(TXMUIMessages.theP0CorpusWillBeRestarted, getCorpusName()));
				}
			}
			else {
				newOrEdit.setText(NLS.bind(TXMUIMessages.theP0CorpusWillBeCreated, getCorpusName()));
			}


			String corpusname = corpusName.getText();
			Log.fine(TXMUIMessages.SourceDirectoryPage_14 + corpusname);
			String pattern = "[A-Z][-A-Z0-9]+";
			if (!corpusname.matches(pattern)) { //$NON-NLS-1$
				newOrEdit.setText(NLS.bind(TXMUIMessages.corpusNameDoesNotMatchesTheAZAZ09120PatternColonP0, pattern, corpusname));
				return false;
			}
		}

		return getCorpusName().length() > 0 && getSourcePath().length() > 0;
	}

	public String getCorpusName() {
		return corpusName.getText();
	}

	public String getSourcePath() {
		if (sourceDirectory == null || sourceDirectory.isDisposed()) return "";
		return sourceDirectory.getText();
	}
}
