package org.txm.rcp.swt.widget.parameters;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.NamedOptionDef;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

/**
 * Manages parameter and default parameters.
 * 
 * Store parameters history
 * 
 * export to properties or LinkedHashMap.
 * 
 * @author mdecorde
 *
 */
public class ParametersManager {

	/**
	 * parameters informations to manage the values
	 */
	LinkedHashMap<String, NamedOptionDef> parameters;

	/**
	 * values set by default
	 */
	LinkedHashMap<String, Object> defaultValues = new LinkedHashMap<>();

	/**
	 * values set by the user
	 */
	LinkedHashMap<String, Object> chosenValues = new LinkedHashMap<>();

	ArrayList<LinkedHashMap<String, Object>> history = new ArrayList<>();

	int MAX_HISTORY = 10;

	/**
	 * Default history size = 10
	 */
	public ParametersManager() {

	}

	/**
	 * 
	 * @param history_size 0 or + ; 0 deactivate history
	 */
	public ParametersManager(int history_size) {
		if (history_size >= 0) {
			MAX_HISTORY = history_size;
		}
	}

	public boolean setParametersOptions(List<NamedOptionDef> parameters) {
		this.parameters = new LinkedHashMap<>();
		for (NamedOptionDef def : parameters) {
			this.parameters.put(def.name(), def);
		}

		return validateParametersDefinition().size() == 0;
	}

	public List<String> validateParametersDefinition() {
		if (parameters == null) return new ArrayList<>();

		ArrayList<String> errors = new ArrayList<>();
		for (NamedOptionDef p : parameters.values()) {
			if (!CmdLineParser.widgets.contains(p.widget())) {
				errors.add(p.name());
			}
		}
		return errors;
	}

	/**
	 * 
	 * @return true if ALL mandatory parameters are set
	 */
	public boolean validate() {
		for (NamedOptionDef def : parameters.values()) {
			String key = def.name();
			if (!defaultValues.containsKey(key) && !chosenValues.containsKey(key)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * @return true if chosen values are restored
	 */
	public boolean historyBack() {
		if (MAX_HISTORY <= 0) { // no history
			return false;
		}

		if (history.size() > 0) {

			history.remove(history.size() - 1); // remove current

			chosenValues = history.get(history.size() - 1);
			System.out.println("restore: " + chosenValues);
			return true;
		}
		else {
			return false;
		}
	}

	// /**
	// *
	// * @return true if chosen values are forwarded
	// */
	// public boolean historyForward() {
	// if (MAX_HISTORY <= 0) { // no history
	// return false;
	// }
	// if (historyPointer < MAX_HISTORY) {
	// historyPointer++;
	// chosenValues = history.get(historyPointer);
	// return true;
	// }
	// else {
	// return false;
	// }
	// }

	/**
	 * update history if MAX_HISTORY > 0
	 */
	protected void historyUpdate() {
		if (MAX_HISTORY <= 0) { // no history
			return;
		}
		LinkedHashMap<String, Object> clone = (LinkedHashMap<String, Object>) chosenValues.clone();
		// for (String k : clone) { clone.put(k, clone.get(k).clone())
		history.add(clone);

		if (history.size() > MAX_HISTORY) {
			history.remove(0);
		}
	}


	public ParametersManager(LinkedHashMap<String, Object> initialValues) {
		updateChosenValues(initialValues);
	}

	public ParametersManager(LinkedHashMap<String, Object> initialValues, LinkedHashMap<String, Object> defaultValues) {
		updateChosenValues(initialValues);
		updateDefaultValues(defaultValues);
	}


	public void clearChosenValue() {
		chosenValues.clear();

		historyUpdate();
	}

	public void updateChosenValue(String key, Object value) {
		chosenValues.put(key, value);

		historyUpdate();
	}

	public void updateChosenValues(LinkedHashMap<String, Object> newChosenValues) {
		for (String key : newChosenValues.keySet()) {
			chosenValues.put(key, newChosenValues.get(key));
		}

		historyUpdate();
	}

	public void removeChosenValue(String key) {
		chosenValues.remove(key);

		historyUpdate();
	}

	public void removeChosenValues(Collection<String> keys) {
		for (String key : keys) {
			chosenValues.remove(key);
		}

		historyUpdate();
	}

	public void clearDefaultValues(LinkedHashMap<String, Object> newDefaultValues) {
		defaultValues.clear();
	}

	public void updateDefaultValues(Map<String, Object> newDefaultValues) {
		for (String key : newDefaultValues.keySet()) {
			defaultValues.put(key, newDefaultValues.get(key));
		}
	}

	public void updateDefaultValue(String key, Object value) {
		defaultValues.put(key, value);
	}

	public void removeDefaultValue(String key) {
		defaultValues.remove(key);
	}

	public void removeDefaultValue(Collection<String> keys) {
		for (String key : keys) {
			defaultValues.remove(key);
		}
	}

	/**
	 * compile default and chosen values
	 * 
	 * @return
	 */
	public LinkedHashMap<String, Object> getFinalValues() {
		LinkedHashMap<String, Object> finalValues = new LinkedHashMap<>(defaultValues);
		for (String key : chosenValues.keySet()) {
			finalValues.put(key, chosenValues.get(key));
		}
		return finalValues;
	}

	/**
	 * compile and export values to a Properties object
	 * 
	 * @return
	 */
	public Properties finalToProperties() {
		Properties props = new Properties();
		LinkedHashMap<String, Object> values = getFinalValues();
		for (String k : values.keySet()) {
			if (values.get(k).toString() != null) {
				props.setProperty(k, values.get(k).toString());
			}
		}
		return props;
	}

	/**
	 * read parameters from a Properties object.
	 * Values are casted to specific object if the parameters informations are set
	 * 
	 * @return a hashmap of values (Object)
	 */
	public LinkedHashMap<String, Object> propertiesToValues(Properties props) {
		LinkedHashMap<String, Object> newChosenValues = new LinkedHashMap<>();
		for (Object okey : props.keySet()) {
			String key = okey.toString();
			String stringValue = props.getProperty(key);

			Object value = stringToValue(stringValue, key);

			newChosenValues.put(key, value);
		}

		return newChosenValues;
	}

	/**
	 * read parameters from a HashMap<String, String> object.
	 * Values are casted to specific object if the parameters informations are set
	 * 
	 * @return
	 */
	public LinkedHashMap<String, Object> hashToValues(Map<String, String> map) {
		LinkedHashMap<String, Object> newChosenValues = new LinkedHashMap<>();
		for (String key : map.keySet()) {
			String stringValue = map.get(key);

			Object value = stringToValue(stringValue, key);

			newChosenValues.put(key, value);
		}

		return newChosenValues;
	}

	private Object stringToValue(String stringValue, String key) {
		if (stringValue == null) return null;

		String widgetName = getParameterType(key);

		Object value = null;
		if ("File".equals(widgetName) || "FileOpen".equals(widgetName)) { //$NON-NLS-1$ //$NON-NLS-2$
			value = new File(stringValue);
		}
		else if ("Files".equals(widgetName) || "FilesOpen".equals(widgetName)) { //$NON-NLS-1$ //$NON-NLS-2$
			String[] paths = stringValue.split("" + File.pathSeparatorChar); //$NON-NLS-1$
			File[] files = new File[paths.length];
			for (int i = 0; i < paths.length; i++) {
				files[i] = new File(paths[i]);
			}
			value = files;
		}
		else if ("CreateFile".equals(widgetName) || "FileSave".equals(widgetName)) { //$NON-NLS-1$ //$NON-NLS-2$
			value = new File(stringValue);
		}
		else if ("Folder".equals(widgetName) || "Directory".equals(widgetName)) { //$NON-NLS-1$ //$NON-NLS-2$
			value = new File(stringValue);
		}
		else if ("Query".equals(widgetName)) { //$NON-NLS-1$
			value = new CQLQuery(stringValue);
		}
		else if ("Date".equals(widgetName)) { //$NON-NLS-1$
			try {
				value = DateField.formater.parse(stringValue);
			}
			catch (ParseException e) {
				Log.severe("Wrong default date format: " + stringValue + ". Waiting for: " + DateField.STRINGFORMAT + ". Error = " + e + "."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				value = new Date();
			}
		}
		else if ("Time".equals(widgetName)) { //$NON-NLS-1$
			value = Integer.parseInt(stringValue);
		}
		else if ("String".equals(widgetName)) { //$NON-NLS-1$
			value = stringValue;
		}
		else if ("Separator".equals(widgetName)) { //$NON-NLS-1$
			value = stringValue;
		}
		else if ("Password".equals(widgetName)) { //$NON-NLS-1$
			value = stringValue;
		}
		else if ("StringArray".equals(widgetName)) { //$NON-NLS-1$
			value = stringValue;
		}
		else if ("StringArrayMultiple".equals(widgetName)) { //$NON-NLS-1$
			value = stringValue;
		}
		else if ("StructuralUnits".equals(widgetName)) { //$NON-NLS-1$
			value = stringValue;
		}
		else if ("Text".equals(widgetName)) { //$NON-NLS-1$
			value = stringValue;
		}
		else if ("Integer".equals(widgetName)) { //$NON-NLS-1$
			value = Integer.parseInt(stringValue);
		}
		else if ("Float".equals(widgetName)) { //$NON-NLS-1$
			value = Float.parseFloat(stringValue);
		}
		else if ("Boolean".equals(widgetName)) { //$NON-NLS-1$
			value = Boolean.parseBoolean(stringValue);
		}
		else {
			value = stringValue;
		}

		return value;
	}

	private String getParameterType(String key) {
		if (key == null || parameters == null || !parameters.containsKey(key)) {
			return "String"; //$NON-NLS-1$
		}

		return parameters.get(key).widget();
	}

	/**
	 * compile and export values to a Properties object
	 * 
	 * @return true if bean fields have been updated
	 */
	public boolean finalToBean(Object bean) {
		if (bean == null) return false;
		boolean ok = true;
		LinkedHashMap<String, Object> values = getFinalValues();

		Class<?> c = bean.getClass();
		for (String name : values.keySet()) {

			try {
				// System.out.println("setting "+name+ " with "+values.get(name));
				Field field = c.getDeclaredField(name);
				field.setAccessible(true); // enable
				field.set(bean, values.get(name));
			}
			catch (Exception e) {
				System.out.println(TXMUIMessages.errorInAtOptionNameDeclarationColon + e.getMessage());
				Log.printStackTrace(e);
				ok = false;
			}
		}
		return ok;
	}

	/**
	 * compile and export values to a Properties object
	 * 
	 * @throws IOException
	 */
	public void finalToFile(File propFile) throws IOException {
		Properties props = finalToProperties();
		PrintWriter writer = IOUtils.getWriter(propFile);
		props.store(writer, "TXM parameters"); //$NON-NLS-1$
		writer.close();
		return;
	}

	public static void main(String[] args) {
		AtestClass atc = new AtestClass();
		ParametersManager pm = new ParametersManager();
		pm.updateChosenValue("file", new File("testfile")); //$NON-NLS-1$ //$NON-NLS-2$
		pm.updateChosenValue("dir", new File("testdir")); //$NON-NLS-1$ //$NON-NLS-2$
		pm.updateChosenValue("ii", 1); //$NON-NLS-1$
		pm.historyBack();
		pm.historyBack();
		pm.finalToBean(atc);
		System.out.println("atc=" + atc); //$NON-NLS-1$
	}

	public LinkedHashMap<String, Object> getDefaultValues() {
		return defaultValues;
	}

	public LinkedHashMap<String, NamedOptionDef> getParameters() {
		return parameters;
	}
}
