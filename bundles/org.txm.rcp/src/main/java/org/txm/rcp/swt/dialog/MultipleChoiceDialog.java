// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.dialog;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

/**
 * Open a dialog with a button per choice
 */
public class MultipleChoiceDialog extends Dialog {

	/** The self. */
	MultipleChoiceDialog self;

	/** The main panel. */
	Composite mainPanel;

	/** The parent shell. */
	Shell parentShell;

	/** The values. */
	String[] values;

	/** The title. */
	private String title;

	private String answer;

	private String message;

	/**
	 * 
	 * @param parentShell
	 * @param title
	 * @param message displayed before he buttons
	 * @param values in the shown order, not null, not empty
	 * @param defaultAnswer , default value. If null the first value is the default value
	 */
	public MultipleChoiceDialog(Shell parentShell, String title, String message, String[] values, String defaultAnswer) {
		super(parentShell);

		if (values == null || values.length == 0) {
			throw new IllegalStateException("Available values must be set and not empty"); //$NON-NLS-1$
		}

		this.parentShell = parentShell;
		this.setShellStyle(this.getShellStyle());
		this.values = values;
		this.title = title;
		this.answer = defaultAnswer;
		if (answer == null) {
			answer = values[0];
		}
		this.message = message;
	}

	@Override
	public void createButtonsForButtonBar(Composite parent) {
		// default buttons not created
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(title);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite p) {

		p.setLayout(new GridLayout(values.length, false));

		Label label = new Label(p, SWT.NONE);
		label.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, true, values.length, 1));
		if (message != null) label.setText(message);

		for (String name : values) {
			Button btn = new Button(p, SWT.PUSH);
			btn.setText(name); //$NON-NLS-1$
			btn.setToolTipText("Select "+name); //$NON-NLS-1$
			btn.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));
			btn.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					answer = ((Button) e.widget).getText();
					MultipleChoiceDialog.super.okPressed();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		}
		return p;
	}

	public String getAnswer() {
		return answer;
	}
}
