package org.txm.rcp.p2.plugins;

import java.util.Collection;

import org.eclipse.equinox.internal.p2.ui.ProvUIMessages;
import org.eclipse.equinox.internal.p2.ui.dialogs.ISelectableIUsPage;
import org.eclipse.equinox.internal.p2.ui.dialogs.PreselectedIUInstallWizard;
import org.eclipse.equinox.internal.p2.ui.dialogs.ResolutionResultsWizardPage;
import org.eclipse.equinox.internal.p2.ui.dialogs.SelectableIUsPage;
import org.eclipse.equinox.internal.p2.ui.model.IUElementListRoot;
import org.eclipse.equinox.p2.metadata.IInstallableUnit;
import org.eclipse.equinox.p2.operations.InstallOperation;
import org.eclipse.equinox.p2.ui.LoadMetadataRepositoryJob;
import org.eclipse.equinox.p2.ui.ProvisioningUI;

/***
 * PreselectedIUInstallWizard using a personalized main page (SelectableIUsPage)
 * 
 * @author nkredens
 *
 */
public class TXMPreselectedIUInstallWizard extends PreselectedIUInstallWizard {

	public TXMPreselectedIUInstallWizard(ProvisioningUI ui,
			InstallOperation operation,
			Collection<IInstallableUnit> initialSelections,
			LoadMetadataRepositoryJob job) {
		super(ui, operation, initialSelections, job);
	}

	protected ISelectableIUsPage createMainPage(IUElementListRoot input, Object[] selections) {
		mainPage = new TXMSelectableIUsPage(ui, this, input, selections);
		mainPage.setTitle(ProvUIMessages.PreselectedIUInstallWizard_Title);
		mainPage.setDescription(ProvUIMessages.PreselectedIUInstallWizard_Description);
		((SelectableIUsPage) mainPage).updateStatus(input, operation);
		return mainPage;
	}

	protected ResolutionResultsWizardPage createResolutionPage() {
		return new TXMInstallWizardPage(ui, this, root, operation);
	}
}
