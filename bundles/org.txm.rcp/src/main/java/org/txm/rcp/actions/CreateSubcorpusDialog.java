// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.actions;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.widget.structures.AdvancedSubcorpusPanel;
import org.txm.rcp.swt.widget.structures.ComplexSubcorpusPanel;
import org.txm.rcp.swt.widget.structures.ContextsSubcorpusPanel;
import org.txm.rcp.swt.widget.structures.SimpleSubcorpusPanel;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.utils.logger.Log;

/**
 * Allow the user to create a Sub-corpus 2 mode : Simple : he can choose a
 * structural unit, a property and a selection of values Advance: he define the
 * query to build the sub-corpus @ author mdecorde.
 */
public class CreateSubcorpusDialog extends Dialog {

	// for all tabs
	/** The name. */
	private Text nameField;

	/** The name string. */
	private String nameString;

	/** The corpus. */
	private CQPCorpus corpus;

	/** The selected subcorpus mode */
	private TabItem subcorpusMode = null;

	private TabItem simpleTab;

	private TabItem complexTab;

	private TabItem contextsTab;

	private TabItem advancedTab;

	SimpleSubcorpusPanel simplePanel;

	ComplexSubcorpusPanel complexPanel;

	ContextsSubcorpusPanel contextsPanel;

	AdvancedSubcorpusPanel advancedPanel;

	/** The query string. */
	private String queryString;

	private List selectedValues;

	/**
	 * Instantiates a new creates the subcorpus dialog.
	 *
	 * @param parentShell the parent shell
	 * @param corpus the corpus
	 */
	public CreateSubcorpusDialog(Shell parentShell, CQPCorpus corpus) {
		super(parentShell);
		this.corpus = corpus;
		this.setShellStyle(this.getShellStyle() | SWT.RESIZE);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(TXMUIMessages.createSubcorpus);
		newShell.setMinimumSize(300, 300);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		Composite inclosing = new Composite(parent, SWT.NONE);

		GridLayout layout4inclosing = new GridLayout(1, true);
		layout4inclosing.verticalSpacing = 0;
		layout4inclosing.horizontalSpacing = 0;
		inclosing.setLayout(layout4inclosing);
		inclosing.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		// name panel
		Composite nameComposite = new Composite(inclosing, SWT.NONE);
		nameComposite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		GridLayout layout4nameComposite = new GridLayout(2, false);
		nameComposite.setLayout(layout4nameComposite);

		Label nameLabel = new Label(nameComposite, SWT.NONE);
		nameLabel.setText(TXMUIMessages.ampNameColon);
		nameLabel.setLayoutData(new GridData(GridData.END, GridData.CENTER, false, false));

		nameField = new Text(nameComposite, SWT.BORDER);
		nameField.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));

		// Param panel
		final TabFolder tabFolder = new TabFolder(inclosing, SWT.NONE);
		tabFolder.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		// First tab
		simpleTab = new TabItem(tabFolder, SWT.NONE);
		simpleTab.setText(TXMUIMessages.simple);
		simplePanel = new SimpleSubcorpusPanel(tabFolder, SWT.NONE, this.getButton(IDialogConstants.OK_ID), corpus);
		simpleTab.setControl(simplePanel);

		// Second tab: assisted mode
		complexTab = new TabItem(tabFolder, SWT.NONE);
		complexTab.setText(TXMUIMessages.assisted);
		this.complexPanel = new ComplexSubcorpusPanel(tabFolder, SWT.NONE, this.corpus);
		complexTab.setControl(complexPanel);

		// Third tab: context mode
		contextsTab = new TabItem(tabFolder, SWT.NONE);
		contextsTab.setText(TXMUIMessages.contexts);
		this.contextsPanel = new ContextsSubcorpusPanel(tabFolder, SWT.NONE, this.corpus);
		contextsTab.setControl(contextsPanel);

		// Fourth tab: advanced mode
		advancedTab = new TabItem(tabFolder, SWT.NONE);
		advancedTab.setText(TXMUIMessages.advanced);

		this.advancedPanel = new AdvancedSubcorpusPanel(tabFolder, SWT.NONE, this.corpus);
		advancedTab.setControl(advancedPanel);

		subcorpusMode = simpleTab;

		// check the state of the OK button according to the change of tab
		// --------------------------------

		tabFolder.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {

				subcorpusMode = tabFolder.getSelection()[0];
			}
		});

		return tabFolder;
	}

	@Override
	public Button getButton(int id) {

		return super.getButton(id);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {

		if (nameField.getText().length() == 0) { // auto-naming if needed
			String autoName = "subcorpus"; //$NON-NLS-1$

			// Advanced mode
			if (subcorpusMode == simpleTab) {
				autoName = this.simplePanel.getGeneratedName();
			}
			else if (subcorpusMode == complexTab) { // Assisted mode (structure@property=1st_value)
				autoName = this.complexPanel.getGeneratedName();
			}
			else if (subcorpusMode == contextsTab) {
				autoName = this.contextsPanel.getGeneratedName();
			}
			else if (subcorpusMode == advancedTab) {
				autoName += "" + (this.corpus.getSubcorpora().size() + 1); //$NON-NLS-1$
			}
			else {
				autoName = this.complexPanel.getGeneratedName();
			}

			if (autoName.length() > 33) autoName = autoName.substring(0, 30) + "...";
			Log.info(NLS.bind(TXMUIMessages.noSubcorpusNameWasSpecifiedWeveGeneratedOneP0, autoName));

			nameField.setText(autoName);
		}

		if (subcorpusMode == complexTab) {
			// System.out.println("1 "+queryString);
			queryString = this.complexPanel.getQueryFieldValue();
			// System.out.println("2 "+queryString);
			if (queryString == null || queryString.trim().length() == 0) {
				queryString = this.complexPanel.getQueryString();
				// System.out.println("3 "+queryString);
				if (queryString == null || queryString.trim().length() == 0) {
					MessageDialog.openError(getShell(), TXMUIMessages.invalidQuery, TXMUIMessages.queryCannotBeLeftEmpty);
					return;
				}
			}
		}
		else if (subcorpusMode == contextsTab) {
			queryString = this.contextsPanel.getQueryString();
		}
		else if (subcorpusMode == advancedTab) {
			queryString = advancedPanel.getQueryString();
		}

		nameString = nameField.getText();

		if (simplePanel != null) {
			selectedValues = simplePanel.valueCombo.getSelection().toList();
		}
		super.okPressed();
	}

	/**
	 * Gets the structural unit.
	 *
	 * @return the structural unit
	 */
	public StructuralUnit getStructuralUnit() {

		if (subcorpusMode == advancedTab) {
			throw new IllegalStateException(TXMUIMessages.cannotAskForTheSimpleTabWhenTheUserHasSelectedTheAdvancedTab);
		}
		return simplePanel.selectedStructuralUnit;
	}

	/**
	 * Gets the structural unit property.
	 *
	 * @return the structural unit property
	 */
	public StructuralUnitProperty getStructuralUnitProperty() {

		if (subcorpusMode == contextsTab || subcorpusMode == advancedTab) {
			throw new IllegalStateException(TXMUIMessages.cannotAskForTheSimpleTabWhenTheUserHasSelectedTheAdvancedTab);
		}
		return simplePanel.selectedStructuralUnitProperty;
	}

	/**
	 * Gets the values.
	 *
	 * @return the values
	 */
	public List<String> getValues() {

		if (subcorpusMode == contextsTab || subcorpusMode == advancedTab) {
			throw new IllegalStateException(TXMUIMessages.cannotAskForTheSimpleTabWhenTheUserHasSelectedTheAdvancedTab);
		}
		return selectedValues;
	}

	/**
	 * Gets the query.
	 *
	 * @return the query
	 */
	public String getQuery() {

		if (subcorpusMode == simpleTab) {
			throw new IllegalStateException(TXMUIMessages.cannotAskForTheSimpleTabWhenTheUserHasSelectedTheAdvancedTab);
		}
		return queryString;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {

		return nameString;
	}

	/**
	 * Gets the sub corpus name.
	 *
	 * @return the sub corpus name
	 */
	public String getSubCorpusName() {

		return nameField.getText();
	}

	public boolean isQueryBased() {

		return subcorpusMode == complexTab || subcorpusMode == contextsTab || subcorpusMode == advancedTab;
	}
}
