package org.txm.rcp.handlers.export;

import java.lang.reflect.Field;

import org.txm.utils.BiHashMap;

public class FormatsRepository {

	public static final BiHashMap<String, String> ext2format = new BiHashMap<String, String>();
	static {
		
		for (Field field : Messages.class.getFields()) {
			if (java.lang.reflect.Modifier.isStatic(field.getModifiers()) && java.lang.reflect.Modifier.isPublic(field.getModifiers())) {
				try {
					ext2format.put("*."+field.getName(), ""+field.get(Messages.class));
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} //$NON-NLS-1$
		    }
		}
//		System.out.println(ext2format);
	}
	
	public static String toName(String format) {
		if (ext2format.containsKey(format)) {
			return ext2format.get(format);
		}
		
		return format;
	}
	
	public static String toFormat(String name) {
		if (ext2format.containsValue(name)) {
			return ext2format.getKey(name);
		}
		
		return name;
	}
	
	public static void main(String[] args) {
		new FormatsRepository();
	}
}
