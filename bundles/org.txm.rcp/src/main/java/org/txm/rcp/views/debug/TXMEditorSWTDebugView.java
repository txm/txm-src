// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views.debug;

import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IPartService;
import org.eclipse.ui.IPropertyListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.part.ViewPart;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TXMMultiPageEditor;
import org.txm.rcp.swt.GLComposite;


/**
 * Debug view dedicated to display SWT widgets of the current active TXMEditor.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class TXMEditorSWTDebugView extends ViewPart implements IPartListener, IPropertyListener {


	/**
	 * 
	 */
	protected TreeViewer treeViewer;

	/**
	 * 
	 */
	private TreeViewerColumn layoutDataColumn;

	/**
	 * 
	 */
	private TreeViewerColumn layoutColumn;

	/**
	 * 
	 */
	private TreeViewerColumn nameColumn;

	/**
	 * 
	 */
	protected Point mousePosition;


	/**
	 * 
	 */
	public TXMEditorSWTDebugView() {
		IPartService service = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getPartService();
		service.addPartListener(this);
	}

	@Override
	public void dispose() {
		IPartService service = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getPartService();
		service.removePartListener(this);
	}

	@Override
	public void createPartControl(Composite parent) {
		
		treeViewer = new TreeViewer(parent, SWT.NONE);
		treeViewer.setContentProvider(new ITreeContentProvider() {

			@Override
			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			}

			@Override
			public void dispose() {
			}

			@Override
			public boolean hasChildren(Object element) {
				if (element instanceof Composite) {
					return ((Composite) element).getChildren().length > 0;
				}
				else if (element instanceof Control) {
					return false;
				}
				return false;
			}

			@Override
			public Object getParent(Object element) {
				if (element instanceof Composite) {
					return ((Composite) element).getParent();
				}
				else if (element instanceof Control) {
					return ((Control) element).getParent();
				}
				return null;
			}

			@SuppressWarnings("rawtypes")
			@Override
			public Object[] getElements(Object inputElement) {
				if (inputElement == null) return new Object[0];

				Object[] o = new Object[1];
				if (inputElement instanceof TXMEditor) {

					o[0] = ((TXMEditor) inputElement).getContainer();
					return o;
				}
				else if (inputElement instanceof Composite) {
					return ((Composite) inputElement).getChildren();
				}
				return null;
			}

			@Override
			public Object[] getChildren(Object element) {
				if (element instanceof Composite) {
					return ((Composite) element).getChildren();
				}
				else if (element instanceof Control) {
					return new Object[0];
				}
				else if (element instanceof ToolBar) {
					return ((ToolBar) element).getChildren();
				}
				return new Object[0];
			}
		});

		treeViewer.getTree().setHeaderVisible(true);
		treeViewer.getTree().setLinesVisible(true);

		nameColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		nameColumn.getColumn().setText("Element"); //$NON-NLS-1$
		nameColumn.getColumn().pack();
		nameColumn.setLabelProvider(new CellLabelProvider() {

			@Override
			public void update(ViewerCell cell) {
				Object element = cell.getElement();

				if (element instanceof GLComposite) {
					GLComposite c = (GLComposite) element;
					String s = c.getName();
					if (isHighlighted(c)) s += "*"; //$NON-NLS-1$
					cell.setText(s);
				}
				else if (element instanceof Button) {
					Button c = (Button) element;
					cell.setText(c.getClass().getSimpleName() + " '" + c.getText() + "'"); //$NON-NLS-1$ //$NON-NLS-2$
				}
				else if (element instanceof Label) {
					Label c = (Label) element;
					cell.setText(c.getClass().getSimpleName() + " '" + c.getText() + "'"); //$NON-NLS-1$ //$NON-NLS-2$
				}
				else if (element instanceof Spinner) {
					Spinner c = (Spinner) element;
					cell.setText(c.getClass().getSimpleName() + " '" + c.getSelection() + "'\n\tmin=" + c.getMinimum() + " max=" + c.getMaximum()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				}
				else {
					cell.setText(element.getClass().getSimpleName());
				}
			}
		});

		treeViewer.getTree().addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
				mousePosition = new Point(e.x, e.y);
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}
		});

		treeViewer.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {


				Object o = ((StructuredSelection) event.getSelection()).getFirstElement();

				int col = getPointedColumn();

				if (col == 0) {
					if (o != null && o instanceof Control) {
						Control w = (Control) o;
						Color b = w.getDisplay().getSystemColor(SWT.COLOR_BLUE);
						if (isHighlighted(w)) {
							w.setBackground(null);
						}
						else {
							w.setBackground(b);
						}
						treeViewer.refresh();
					}
				}
				else if (col == 1) { // open layout editor
					if (o != null && o instanceof Composite) {
						Composite w = (Composite) o;
						Layout l = w.getLayout();
						if (l instanceof GridLayout) {

						}
						else if (l instanceof RowLayout) {

						}
						else if (l instanceof FormLayout) {

						}
					}
				}
				else {  // open layout data editor
					if (o != null && o instanceof Control) {
						Control w = (Control) o;
						Object ld = w.getLayoutData();
						if (ld instanceof GridLayout) {

						}
						else if (ld instanceof RowLayout) {

						}
						else if (ld instanceof FormLayout) {

						}
					}
				}
			}
		});

		layoutColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		layoutColumn.getColumn().setText("Layout"); //$NON-NLS-1$
		layoutColumn.getColumn().pack();
		layoutColumn.setLabelProvider(new CellLabelProvider() {

			@Override
			public void update(ViewerCell cell) {
				Object element = cell.getElement();

				if (element instanceof Composite) {
					String s = getLayoutDetails(((Composite) element).getLayout());
					cell.setText(s);
				}
				else {
					cell.setText("-"); //$NON-NLS-1$
				}
			}
		});

		layoutDataColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		layoutDataColumn.getColumn().setText("L-data"); //$NON-NLS-1$
		layoutDataColumn.getColumn().pack();
		layoutDataColumn.setLabelProvider(new CellLabelProvider() {

			@Override
			public void update(ViewerCell cell) {
				Object element = cell.getElement();

				if (element instanceof Control) {
					String s = getLayoutDataDetails(((Control) element).getLayoutData());
					cell.setText(s);
				}
				else {
					cell.setText("-"); //$NON-NLS-1$
				}
			}
		});

	}

	protected String getLayoutDataDetails(Object layoutData) {
		if (layoutData == null) return "no layout data"; //$NON-NLS-1$

		if (layoutData instanceof GridData) {
			GridData data = (GridData) layoutData;
			return "GD: heightHint=" + data.heightHint + " widthHint=" + data.widthHint //$NON-NLS-1$ //$NON-NLS-2$
					+ "\n horizontalAlignment=" + data.horizontalAlignment + " horizontalIndent=" + data.horizontalIndent + " horizontalSpan=" + data.horizontalSpan //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					+ "\n minimumHeight=" + data.minimumHeight + " minimumWidth=" + data.minimumWidth //$NON-NLS-1$ //$NON-NLS-2$
					+ "\n verticalAlignment=" + data.verticalAlignment + " verticalIndent=" + data.verticalIndent + " verticalSpan=" + data.verticalSpan; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		else if (layoutData instanceof TableWrapData) {
			TableWrapData data = (TableWrapData) layoutData;
			return "TWD: heightHint=" + data.heightHint + " align=" + data.align //$NON-NLS-1$ //$NON-NLS-2$
					+ "\n colspan=" + data.colspan + " heightHint=" + data.heightHint + " indent=" + data.indent //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					+ "\n maxHeight=" + data.maxHeight + " maxWidth=" + data.maxWidth //$NON-NLS-1$ //$NON-NLS-2$
					+ "\n rowspan=" + data.rowspan + " valign=" + data.valign + " grabHorizontal=" + data.grabHorizontal + " grabVertical=" + data.grabVertical; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		}
		else if (layoutData instanceof FormData) {
			FormData data = (FormData) layoutData;
			return "FD: height=" + data.height + " width=" + data.width //$NON-NLS-1$ //$NON-NLS-2$
					+ "\n bottom=" + data.bottom + " left=" + data.left + " right=" + data.right + " top=" + data.top; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		}
		return layoutData.toString();
	}

	public int getPointedColumn() {
		Point p = mousePosition;
		if (p == null) return 0;

		int x = p.x; // + lineTableViewer.getTable().get;
		int sumWidthColumn = 0;

		sumWidthColumn += this.nameColumn.getColumn().getWidth();
		if (x < sumWidthColumn)
			return 0;

		sumWidthColumn += this.layoutColumn.getColumn().getWidth();
		if (x < sumWidthColumn)
			return 1;

		return 2;
	}

	public static String getLayoutDetails(Layout layout) {
		if (layout == null) return "no layout"; //$NON-NLS-1$

		if (layout instanceof GridLayout) {
			GridLayout gl = (GridLayout) layout;
			return "GL: " + gl.horizontalSpacing + " " + gl.verticalSpacing + " " + gl.marginHeight + " " + gl.marginWidth //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					+ "\n " + gl.marginBottom + " " + gl.marginLeft + " " + gl.marginRight + " " + gl.marginTop //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					+ "\n " + gl.numColumns + " " + gl.makeColumnsEqualWidth; //$NON-NLS-1$ //$NON-NLS-2$

		}
		else if (layout instanceof FormLayout) {
			FormLayout gl = (FormLayout) layout;
			return "FL: marginBottom=" + gl.marginBottom + " marginHeight=" + gl.marginHeight + " marginWidth=" + gl.marginWidth //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					+ "\n marginLeft=" + gl.marginLeft + " marginRight=" + gl.marginRight + " marginTop=" + gl.marginTop //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					+ "\n spacing=" + gl.spacing; //$NON-NLS-1$

		}
		else {
			return layout.toString();
		}
	}

	@Override
	public void partActivated(IWorkbenchPart part) {
		if (part == null)
			return;

		// System.out.println("partActivated: "+part);
		if (part instanceof TXMEditor) {
			treeViewer.setInput(part);
		}
		else if (part instanceof TXMMultiPageEditor) {
			TXMMultiPageEditor tmpe = (TXMMultiPageEditor) part;
			treeViewer.setInput(tmpe.getContainer());
		}
		else if (part instanceof FormEditor) {
			FormEditor tmpe = (FormEditor) part;
			FormPage part2 = (FormPage) tmpe.getSelectedPage();

			treeViewer.setInput(part2.getPartControl());
		}
	}

	public static boolean isHighlighted(Control c) {
		if (c == null) return false;
		Color b = c.getDisplay().getSystemColor(SWT.COLOR_BLUE);
		Color current = c.getBackground();

		return current != null
				&& current.getGreen() == b.getGreen()
				&& current.getBlue() == b.getBlue()
				&& current.getRed() == b.getRed();
	}

	@Override
	public void propertyChanged(Object source, int propId) {
	}

	@Override
	public void partBroughtToTop(IWorkbenchPart part) {
	}

	@Override
	public void partClosed(IWorkbenchPart part) {
		if (!treeViewer.getTree().isDisposed()) {
			treeViewer.setInput(null);
		}
	}

	@Override
	public void partDeactivated(IWorkbenchPart part) {
	}

	@Override
	public void partOpened(IWorkbenchPart part) {
	}

	@Override
	public void setFocus() {
	}

	public void refresh() {
		if (!treeViewer.getTree().isDisposed()) {
			treeViewer.refresh();
		}
	}
}
