// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.preferences;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;

import org.eclipse.equinox.internal.p2.ui.sdk.scheduler.AutomaticUpdatePlugin;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.IHandlerService;
import org.txm.Toolbox;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.rcp.views.fileexplorer.MacroExplorer;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class TextometriePreferencePage.
 */
public class TXMMainPreferencePage extends RCPPreferencesPage {

	/** The preferences. */
	//	StringButtonFieldEditor buttonExport, buttonImport;
	private BooleanFieldEditor update;
	private StringFieldEditor memoryField;

	/**
	 * Instantiates a new Wordcloud preference page.
	 */
	public TXMMainPreferencePage() {
		super();
	}

	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(RCPPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setImageDescriptor(IImageKeys.getImageDescriptor(IImageKeys.WORLD));
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.PreferencePage#isValid()
	 */
	@Override
	public boolean isValid() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 */
	@Override
	protected void createFieldEditors() {
		//		Group generalGroup = new Group(getFieldEditorParent(), SWT.NONE);
		//		generalGroup.setText(TXMUIMessages.generalPreferencesBackup);
		//		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		//		gridData.horizontalSpan = 2;
		//		generalGroup.setLayoutData(gridData);
		//		generalGroup.setLayout(new RowLayout(SWT.VERTICAL));


		//		buttonExport = new StringButtonFieldEditor("export_prefs", "", generalGroup) { //$NON-NLS-1$ //$NON-NLS-2$
		//			@Override
		//			protected String changePressed() {
		//				FileDialog dialog = new FileDialog(new Shell(), SWT.SAVE);
		//				if (LastOpened.getFile(TXMMainPreferencePage.class.getName()) != null) {
		//					dialog.setFilterPath(LastOpened.getFolder(TXMMainPreferencePage.class.getName()));
		//					dialog.setFileName(LastOpened.getFile(TXMMainPreferencePage.class.getName()));
		//				}
		//				String path = dialog.open();
		//				if (path != null) {
		//					File export = new File(path);
		//					LastOpened.set(TXMMainPreferencePage.class.getName(), export.getParent(), export.getName());
		//					FileWriter writer;
		//					try {
		//						//Preferences prefs = Platform.getPreferencesService().getRootNode().node(TxmPreferences.SCOPE); //$NON-NLS-1$
		//						Preferences prefs = TXMPreferences.getPreferences();
		////						writer = new FileWriter(export);
		////						Properties txmprops = org.txm.rcp.ApplicationWorkbenchAdvisor
		////								.getProperties();
		////						Set<Object> tbxKeys = txmprops.keySet();
		////
		////						TxmPreferences.set("software", "TXM"); //$NON-NLS-1$ //$NON-NLS-2$
		////						org.osgi.framework.Version v = Activator.getDefault().getBundle().getVersion();
		////						TxmPreferences.set("version", v.toString()); //$NON-NLS-1$
		////
		////						// write props for the TBX
		////						for(Object prop : txmprops.keySet()) {
		////							writer.write(""+prop+"="+txmprops.get(prop)+"\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		////						}
		////						writer.write("\n"); //$NON-NLS-1$
		////						// write RCP props
		////						
		////						for (String child : prefs.childrenNames()) {
		////							Preferences childNode = prefs.node(child);
		////							for (String key : childNode.keys()) {
		////								if (!tbxKeys.contains(key)) {
		////									Object value = childNode.get(key, null);
		////									if (value != null)
		////										writer.write(""+key+"="+value+"\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		////								}
		////							}
		////						}
		////						writer.close();
		//
		//						IPreferencesService service = Platform.getPreferencesService();
		//						service.exportPreferences((IEclipsePreferences) prefs,new BufferedOutputStream(new FileOutputStream(export)), null);
		//						System.out.println(Messages.TextometriePreferencePage_14+export);
		//					} catch (IOException e) {
		//						// TODO Auto-generated catch block
		//						org.txm.utils.logger.Log.printStackTrace(e);
		////					} catch (BackingStoreException e) {
		////						// TODO Auto-generated catch block
		////						org.txm.utils.logger.Log.printStackTrace(e);
		//					} catch (CoreException e) {
		//						// TODO Auto-generated catch block
		//						org.txm.utils.logger.Log.printStackTrace(e);
		//					}
		//				}
		//				return path;
		//			}
		//		};
		//		buttonExport.setChangeButtonText(Messages.TextometriePreferencePage_15);
		//		addField(buttonExport);
		//
		//		buttonImport = new StringButtonFieldEditor("import_prefs", "", generalGroup) { //$NON-NLS-1$ //$NON-NLS-2$
		//			@Override
		//			protected String changePressed() {
		//				FileDialog dialog = new FileDialog(new Shell(), SWT.OPEN);
		//				if (LastOpened.getFile(TXMMainPreferencePage.class.getName()) != null) {
		//					dialog.setFilterPath(LastOpened.getFolder(TXMMainPreferencePage.class.getName()));
		//					dialog.setFileName(LastOpened.getFile(TXMMainPreferencePage.class.getName()));
		//				}
		//				String path = dialog.open();
		//				if (path != null) {
		//					File export = new File(path);
		//					LastOpened.set(TXMMainPreferencePage.class.getName(), export);
		//					IPreferencesService service = Platform.getPreferencesService();
		//					
		//					//Preferences prefs = service.getRootNode().node("configuration");
		//					try {
		//						service.importPreferences(new FileInputStream(export));
		//						System.out.println(Messages.TextometriePreferencePage_18+export);
		//						System.out.println(Messages.TextometriePreferencePage_19);
		//						performOk();
		//
		//						Composite composite = TXMMainPreferencePage.this.getControl().getParent();
		//						composite.getShell().close();
		//					} catch (FileNotFoundException e) {
		//						org.txm.utils.logger.Log.printStackTrace(e);
		//					} catch (CoreException e) {
		//						org.txm.utils.logger.Log.printStackTrace(e);
		//					} catch (IOException e) {
		//						org.txm.utils.logger.Log.printStackTrace(e);
		//					}
		//				}
		//				return path;
		//			}
		//		};
		//		buttonImport.setChangeButtonText(Messages.TextometriePreferencePage_20);
		//		addField(buttonImport);

		// disable if expert user
		Group updateGroup = new Group(getFieldEditorParent(), SWT.NONE);
		updateGroup.setText(TXMUIMessages.automaticUpdates);
		GridData gridData2 = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData2.horizontalSpan = 2;
		gridData2.verticalIndent = 10;
		updateGroup.setLayoutData(gridData2);
		updateGroup.setLayout(new RowLayout(SWT.VERTICAL));

		//System.out.println(AutomaticUpdatePlugin.getDefault().getStateLocation());
		final IPreferenceStore updatePrefStore = AutomaticUpdatePlugin.getDefault().getPreferenceStore();
		update = new BooleanFieldEditor(TBXPreferences.FETCH_UPDATES_AT_STARTUP, TXMUIMessages.checkForUpdatesAtStartup, updateGroup);

		// Update level
		String[][] choices2 = new String[2][2];
		choices2[0][0] = choices2[0][1] = "STABLE"; //$NON-NLS-1$
		choices2[1][0] = choices2[1][1] = "BETA"; //$NON-NLS-1$
		addField(new ComboFieldEditor(RCPPreferences.UPDATE_LEVEL, TXMUIMessages.updateLevel, choices2, updateGroup));

		// change TXM update site base URL
		addField(new StringFieldEditor(TBXPreferences.UPDATESITE, TXMUIMessages.UpdateSiteAddress, updateGroup));
		
		((GridLayout)updateGroup.getLayout()).marginWidth = 3;
		((GridLayout)updateGroup.getLayout()).numColumns = 2;
		

		memoryField = new StringFieldEditor(TBXPreferences.MEMORY_SIZE, TXMUIMessages.memorySize, getFieldEditorParent());
		memoryField.setEmptyStringAllowed(false);
		memoryField.setValidateStrategy(StringFieldEditor.VALIDATE_ON_KEY_STROKE);
		addField(memoryField);

		//update.setPreferenceStore(updatePrefStore);
		//boolean enabled = updatePrefStore.getBoolean(PreferenceConstants.PREF_AUTO_UPDATE_ENABLED);
		//			System.out.println("before load: "+updatePrefStore.getBoolean(PreferenceConstants.PREF_AUTO_UPDATE_ENABLED));
		//update.load();
		//			System.out.println("after 1st load: "+updatePrefStore.getBoolean(PreferenceConstants.PREF_AUTO_UPDATE_ENABLED));
		addField(update);
	}

	@Override
	public void propertyChange(org.eclipse.jface.util.PropertyChangeEvent event) {
		if (event.getProperty().equals("field_editor_value")) {
			//field for which validation is required
			if (event.getSource() == memoryField) {
				//validation is successful
				if (doMemoryValidation(memoryField.getStringValue())) {
					super.propertyChange(event);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#performOk()
	 */
	@Override
	public boolean performOk() {
		String memory = memoryField.getStringValue();
		memory = memory.trim().replace(" ", "").toLowerCase();
		if (!doMemoryValidation(memory)) {
			return false;
		}
		String oldMemory = TBXPreferences.getInstance().getString(TBXPreferences.MEMORY_SIZE);
		//System.out.println("OLD "+oldMemory+" NEW "+memory);
		boolean b = super.performOk();

		try {
			MacroExplorer.refresh();
			CorporaView.refresh();

			// update the Logger options
			Log.setLevel(Level.parse(Toolbox.getPreference(TBXPreferences.LOG_LEVEL)));
			Log.setPrintInConsole(TBXPreferences.getInstance().getBoolean(TBXPreferences.ADD_TECH_LOGS));

			Log.log_stacktrace_boolean = TBXPreferences.getInstance().getBoolean(TBXPreferences.LOG_STACKTRACE);

			boolean log_in_file = TBXPreferences.getInstance().getBoolean(TBXPreferences.LOG_IN_FILE);
			String file = Log.setPrintInFile(log_in_file);


			if (!memory.equals(oldMemory)) {

				File launcher = new File(Toolbox.getConfigurationDirectory(), "../launcher.ini");
				if (!launcher.exists()) {
					launcher = new File(Toolbox.getConfigurationDirectory(), "../TXM.ini");
				}
				if (launcher.exists()) {
					if (!changeMemoryConfigIni(launcher, memory)) {
						Log.warning("Error: could not update TXM memory configuration file.");
						return false;
					}
					Log.info(NLS.bind("Changing TXM memory to {0}...", memory));
					MessageDialog.openWarning(this.getShell(), "TXM memory updated", NLS.bind("TXM memory has been changed to {0}. You need to restart TXM to make changes effective.", memory));
				}
				else {
					Log.warning("Error: no TXM memory configuration file found.");
				}
			}

			// update repositories
			IHandlerService service = PlatformUI.getWorkbench().getService(IHandlerService.class);
			service.executeCommand("org.txm.rcp.p2.plugins.FixUpdateHandler", null); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.err.println(NLS.bind(TXMUIMessages.failedToSavePreferencesColonP0, e));
			return false;
		}

		return b;
	}


	private boolean doMemoryValidation(String memory) {

		memory = memory.trim().replace(" ", "").toLowerCase();

		String format = "[1-9][0-9]*[gGmMkK]";
		if (!memory.matches(format)) { // check format
			String m = "Error: Wrong memory format, it should match the following format: " + format;
			memoryField.setErrorMessage(m);
			setErrorMessage(m);
			setValid(false);
			return false;
		}
		try { // check size is not too big
			Long l = Long.parseLong(memory.replace("g", "000000000").replace("m", "000000").replace("k", "000"));

			if (l < 1000000000 / 2) {
				String m = "Error: memory value too low: " + memory + ". Minimal value is 500m.";
				memoryField.setErrorMessage(m);
				setErrorMessage(m);
				setValid(false);
				return false;
			}
		}
		catch (Exception e) {
			String m = "Error: Wrong memory value: " + memoryField.getStringValue() + " (" + e + ")";
			memoryField.setErrorMessage(m);
			setErrorMessage(m);
			setValid(false);
			Log.warning("Error: " + e);
			Log.printStackTrace();
			return false;
		}
		memoryField.setErrorMessage(null);
		setErrorMessage(null);
		setValid(true);
		return true;
	}

	/**
	 * Change locale of config.ini.
	 *
	 * @param locale the locale
	 * @throws IOException
	 */
	public static boolean changeMemoryConfigIni(File file, String memory) throws IOException {

		String fileNameBackup = file.getAbsolutePath() + ".bak"; //$NON-NLS-1$
		File backup = new File(fileNameBackup);
		backup.delete();
		org.txm.utils.io.FileCopy.copy(file, backup);

		try {
			BufferedReader in = new BufferedReader(new FileReader(backup));
			BufferedWriter out = new BufferedWriter(new FileWriter(file));
			try {
				boolean isXmxWritten = false;
				boolean isVMargsWritten = false;
				String line = in.readLine();
				while (line != null) {

					// write Xmx after vmargs
					if (line.equals("-vmargs")) { //$NON-NLS-1$
						isVMargsWritten = true;
						out.write("-vmargs"); //$NON-NLS-1$
						out.newLine();
						out.write("-Xmx" + memory); //$NON-NLS-1$
						out.newLine();
						isXmxWritten = true;
					}
					else if (line.startsWith("-Xmx")) {
						// ignore
					}
					else {
						out.write(line);
						out.newLine();
					}

					line = in.readLine();
				}
				// create nl if it was not presents
				if (!isVMargsWritten) {
					out.write("-vmargs"); //$NON-NLS-1$
					out.newLine();
					isVMargsWritten = true;
				}
				if (!isXmxWritten) {
					out.write("-Xmx" + memory); //$NON-NLS-1$
					out.newLine();
					isXmxWritten = true;
				}
				out.flush();
			}
			finally {
				if (in != null) {
					try {
						in.close();
					}
					catch (IOException e) {
						org.txm.utils.logger.Log.printStackTrace(e);
					}
				}
				if (out != null) {
					try {
						out.close();
					}
					catch (IOException e) {
						org.txm.utils.logger.Log.printStackTrace(e);
					}
				}
			}
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}

		try {
			String txt = IOUtils.getText(file, "UTF-8"); //$NON-NLS-1$
			return txt.contains("-Xmx" + memory); //$NON-NLS-1$
		}
		catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
}
