// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.editors.input;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;
import org.txm.rcp.editors.SVGGraphicable;

/**
 * The Class SpecificitiesResultEditorInput.
 * 
 * @author Sylvain Loiseau,mdecorde
 */
//FIXME: SJ: useless, should use an abstract FileEditorInput
@Deprecated
public class SVGGraphicEditorInput implements IEditorInput, SVGGraphicable {

	/** The filename. */
	private String filename;

	/** The name. */
	private String name;


	/**
	 * Instantiates a new sVG graphic editor input.
	 *
	 * @param filename the filename
	 * @param name the name
	 */
	public SVGGraphicEditorInput(String filename, String name) {
		this.filename = filename;
		this.name = name;
	}

	/**
	 * Exists.
	 *
	 * @return true, if successful
	 * @see org.eclipse.ui.IEditorInput#exists()
	 */
	@Override
	public boolean exists() {
		return false;
	}

	/**
	 * Gets the image descriptor.
	 *
	 * @return the image descriptor
	 * @see org.eclipse.ui.IEditorInput#getImageDescriptor()
	 */
	@Override
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 * @see org.eclipse.ui.IEditorInput#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * Gets the persistable.
	 *
	 * @return the persistable
	 * @see org.eclipse.ui.IEditorInput#getPersistable()
	 */
	@Override
	public IPersistableElement getPersistable() {
		return null;
	}

	/**
	 * Gets the tool tip text.
	 *
	 * @return the tool tip text
	 * @see org.eclipse.ui.IEditorInput#getToolTipText()
	 */
	@Override
	public String getToolTipText() {
		return getName();
	}

	/**
	 * Gets the adapter.
	 *
	 * @param arg0 the arg0
	 * @return the adapter
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@Override
	public Object getAdapter(Class arg0) {
		return null;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the specificitiesResult
	 */
	public String getFileName() {
		return filename;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.rcp.editors.SVGGraphicable#getIPath()
	 */
	@Override
	public IPath getIPath() {
		return Path.fromOSString(filename);
	}
}
