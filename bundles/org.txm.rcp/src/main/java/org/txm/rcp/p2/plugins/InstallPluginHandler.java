package org.txm.rcp.p2.plugins;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.equinox.internal.p2.discovery.Catalog;
import org.eclipse.equinox.internal.p2.discovery.DiscoveryCore;
import org.eclipse.equinox.internal.p2.ui.discovery.repository.RepositoryDiscoveryStrategy;
import org.eclipse.equinox.internal.p2.ui.discovery.util.WorkbenchUtil;
import org.eclipse.equinox.internal.p2.ui.discovery.wizards.CatalogConfiguration;
import org.eclipse.equinox.internal.p2.ui.discovery.wizards.DiscoveryWizard;
import org.eclipse.equinox.internal.p2.ui.sdk.InstallNewSoftwareHandler;
import org.eclipse.equinox.p2.ui.LoadMetadataRepositoryJob;
import org.eclipse.equinox.p2.ui.ProvisioningUI;
import org.eclipse.jface.wizard.WizardDialog;
import org.osgi.framework.Version;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.Activator;
import org.txm.rcp.preferences.RCPPreferences;
import org.txm.utils.logger.Log;

/**
 * NK: InstallPluginHandler invokes the simplified install wizard
 * 
 */
public class InstallPluginHandler extends InstallNewSoftwareHandler {

	/**
	 * The constructor.
	 */
	public InstallPluginHandler() {
		super();
	}

	protected void doExecute(LoadMetadataRepositoryJob job) {
		//super.doExecute(job);

		ProvisioningUI provisioningUI = ProvisioningUI.getDefaultUI();
		//URI[] knownedRepos = provisioningUI.getRepositoryTracker().getKnownRepositories(provisioningUI.getSession());
		//System.out.println("REPOS: "+Arrays.toString(knownedRepos));
		//getProvisioningUI().openInstallWizard(null, null, job);


		Catalog catalog = new Catalog();
		catalog.setEnvironment(DiscoveryCore.createEnvironment());
		catalog.setVerifyUpdateSiteAvailability(false);

		RepositoryDiscoveryStrategy strategy = new RepositoryDiscoveryStrategy();
		try {
			//FIXME: should be initialized in the default preference initializer
			String updateLevel = RCPPreferences.getInstance().getString(RCPPreferences.UPDATE_LEVEL);//$NON-NLS-1$

			boolean betaMode = false;

			if ("BETA".equals(updateLevel)) {//$NON-NLS-1$
				betaMode = true;
			}
			Log.fine("Update levels: beta=" + betaMode); //$NON-NLS-1$ //$NON-NLS-2$

			Version v = Activator.getDefault().getBundle().getVersion();
			String version = "" + v.getMajor() + "." + v.getMinor() + "." + v.getMicro(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			String uriBase = TBXPreferences.getInstance().getString(TBXPreferences.UPDATESITE) + "/" + version + "/ext"; //$NON-NLS-1$ //$NON-NLS-2$

			if (betaMode) {
				strategy.addLocation(new URI(uriBase + "/beta")); //$NON-NLS-1$
			}

			strategy.addLocation(new URI(uriBase + "/stable")); //$NON-NLS-1$
		}
		catch (URISyntaxException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}

		//		for (URI location : knownedRepos) {
		//			strategy.addLocation(location);
		//		}

		catalog.getDiscoveryStrategies().add(strategy);
		//		System.out.println("Catalogs: ");
		//		for (AbstractDiscoveryStrategy strg : catalog.getDiscoveryStrategies()) {
		//			System.out.println("Strategy: "+strg);
		//			System.out.println(" Categories: "+strg.getCategories());
		//			System.out.println(" Items: "+strg.getItems());
		//		}
		CatalogConfiguration configuration = new CatalogConfiguration();
		configuration.setShowTagFilter(false);

		DiscoveryWizard wizard = new TXMDiscoveryWizard(catalog, configuration);
		WizardDialog dialog = new WizardDialog(WorkbenchUtil.getShell(), wizard);
		dialog.open();
	}
}
