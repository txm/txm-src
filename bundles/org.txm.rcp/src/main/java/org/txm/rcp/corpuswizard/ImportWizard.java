package org.txm.rcp.corpuswizard;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.txm.Toolbox;
import org.txm.core.engines.Engine;
import org.txm.core.engines.EngineType;
import org.txm.objects.BaseOldParameters;
import org.txm.objects.Project;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.utils.LogMonitor;
import org.txm.utils.io.FileCopy;
import org.txm.utils.logger.Log;

public class ImportWizard extends Wizard implements INewWizard {

	private SourceDirectoryPage page1 = new SourceDirectoryPage();

	private Project project;

	public ImportWizard() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean performFinish() {
		if (page1.isCompleted()) {
			File path = new File(page1.getSourcePath());
			String name = page1.getCorpusName();// AsciiUtils.buildId(path.getName()).toUpperCase();
			project = doCreateProject(path, name);
			return true;
		}
		return false;
	}

	public static Project doCreateProject(File path, String name) {
		try {
			IProject rcpProject = ResourcesPlugin.getWorkspace().getRoot().getProject(name);
			if (rcpProject.exists() && !rcpProject.isOpen()) {
				rcpProject.open(new LogMonitor("Opening corpus for import")); //$NON-NLS-1$
			}
			Project project = null;

			// find last project using this src directory
			for (Project p : Toolbox.workspace.getProjects()) {
				File psrc = p.getSrcdir();
				if (!path.equals(psrc)) {
					continue;
				}
				if (!name.equals(p.getRCPProject().getName())) {
					continue;
				}

				// get the more recent one
				if (project != null && p.getLastComputingDate().compareTo(project.getLastComputingDate()) > 0) {
					project = p;
				}
				else {
					project = p;
				}
			}
			boolean newProject = (project == null);
			if (newProject) { // it's a new project configuration
				File settingsDirectory = new File(path, "parameters"); //$NON-NLS-1$
				if (settingsDirectory.exists()) {
					File settingsCopy = new File(Toolbox.workspace.getLocation(), name + "/.settings"); //$NON-NLS-1$
					settingsCopy.getParentFile().mkdirs();
					FileCopy.copyFiles(settingsDirectory, settingsCopy);
				}

				project = Toolbox.workspace.getProject(name);
				if (project == null) {
					project = new Project(Toolbox.workspace, name);
				}
				project.setSourceDirectory(path.getAbsolutePath());
				project.setDescription(NLS.bind(TXMUIMessages.DashImportedByP0, System.getProperty("user.name"))); //$NON-NLS-2$  //$NON-NLS-1$
				project.setLang(Locale.getDefault().getLanguage().toLowerCase());

				project.getEditionDefinition("default").setEnableCollapsible(false); //$NON-NLS-1$
				project.getEditionDefinition("default").setPageBreakTag("pb"); //$NON-NLS-1$ //$NON-NLS-2$
				project.getEditionDefinition("default").setPaginateEdition(true); //$NON-NLS-1$
				project.getEditionDefinition("default").setWordsPerPage(1000); //$NON-NLS-1$
				project.getEditionDefinition("default").setBuildEdition(true); //$NON-NLS-1$

				Log.info(NLS.bind(TXMUIMessages.theP0CorpusWillBeCreatedFromTheP1Directory, project.getName(), project.getSrcdir()));

				Engine e = Toolbox.getEngineManager(EngineType.ANNOTATION).getEngine("TreeTagger"); //$NON-NLS-1$
				if (e != null) {
					project.setAnnotate(e.isRunning());
				}

				//				if (project.getAnnotate()) {
				//					Log.info(TXMUIMessages.TheAnnotateImportParameterHasBeenActivatedSinceTreeTaggerIsInstalled);
				//				}
				//				else {
				//					Log.info(TXMUIMessages.TheAnnotateImportParameterWasNotActivatedSinceTreeTaggerIsNotInstalled);
				//				}

				File importxml = new File(path, "import.xml"); //$NON-NLS-1$
				if (importxml.exists()) {
					System.out.println(TXMUIMessages.AnImportDotXMLImportConfigurationFileWasFoundRestoringImportParametersUsingThisfile);
					BaseOldParameters params = new BaseOldParameters(importxml);
					params.load();
					params.initializeProject(project);
				}
			}
			else {
				Date date = project.getLastComputingDate();
				if (date == null) {
					date = Calendar.getInstance().getTime(); // today
				}

				//				if (MessageDialog.openQuestion(ImportWizard.this.getShell(), NLS.bind(TXMUIMessages.P0Import, project), NLS.bind(TXMUIMessages.theP1P0CorpusWillBeReplaced+"\nContinue ?", project, date))) {
				Log.info(NLS.bind(TXMUIMessages.theP1P0CorpusWillBeReplaced, project, date));
				//				} else {
				//					Log.info(TXMUIMessages.abort);
				//					return false;
				//				}
			}

			// if (!project.hasEditionDefinition("default")) {
			// EditionDefinition def = project.getEditionDefinition("default");
			// def.setBuildEdition(true);
			// def.setWordsPerPage(1000);
			// def.setPageBreakTag("pb");
			// }

			project.setDoUpdate(false, false);
			project.saveParameters();
			return project;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void addPages() {
		addPage(page1);
	}

	public Project getProject() {
		return project;
	}
}
