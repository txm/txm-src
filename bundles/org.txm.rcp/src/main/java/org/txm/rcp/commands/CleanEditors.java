package org.txm.rcp.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.rcp.messages.TXMUIMessages;

public class CleanEditors extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) {

		if (MessageDialog.openConfirm(HandlerUtil.getActiveShell(event), TXMUIMessages.resetTheEditors, TXMUIMessages.thisWillCloseAllEditors)) {
			CleanPerspective.reset(event, false, true);
		}
		return null;
	}
}
