// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.txm.rcp.messages.TXMUIMessages;

// TODO: Auto-generated Javadoc
/**
 * Simplified RCP Job
 * 
 * @author mdecorde, nkredens.
 */
public class DownloadJobHandler extends JobHandler {

	private File outfile;

	private int contentLength;

	private Boolean done;

	BufferedOutputStream writeFile = null;

	HttpURLConnection connection = null;

	InputStream input = null;

	private URL url;

	public DownloadJobHandler(String title, URL url, File file, int contentLength, Boolean done) {
		super(title);
		this.outfile = file;
		this.contentLength = contentLength;
		this.done = done;
		this.url = url;
	}

	public void done() {
		this.done = true;
	}

	@Override
	protected IStatus _run(final SubMonitor monitor) throws Exception {
		//		//this.setCurrentMonitor(monitor);
		//		monitor.beginTask(TXMUIMessages.downloading, contentLength);
		//		long size = file.length();
		//		while (!done) {
		//			long current_size = file.length();
		//			monitor.worked((int) (current_size - size));
		//			size = current_size;
		//		}
		//		
		//		BufferedOutputStream writeFile = new BufferedOutputStream(new FileOutputStream(outfile));
		//		input = connection.getInputStream();
		//		byte[] buffer = new byte[1024];
		//		int read;
		//		while ((read = input.read(buffer)) > 0) {
		//			writeFile.write(buffer, 0, read);
		//		}
		//
		//		writeFile.close();
		//		input.close();

		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.connect();

			// Check if the request is handled successfully  
			if (connection.getResponseCode() / 100 == 2) {

				// size in bytes  
				final int contentLength = connection.getContentLength();

				if (contentLength == -1) {
					System.out.println(TXMUIMessages.couldNotGetFileSize);
				}
				else {
					done = false;

					System.out.print(NLS.bind(TXMUIMessages.downloadingTheP0File, url));

					//					DownloadJobHandler job = new DownloadJobHandler("Download "+url, outfile, contentLength, done);
					//					job.setUser(true);
					//					job.schedule();					

					writeFile = new BufferedOutputStream(new FileOutputStream(outfile));
					input = connection.getInputStream();
					byte[] buffer = new byte[1024];
					int read;
					while ((read = input.read(buffer)) > 0) {
						writeFile.write(buffer, 0, read);
					}

					writeFile.close();
					input.close();
					done = true;
					System.out.println(TXMUIMessages.done);
					//					job.done();
				}
			}
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (writeFile != null)
				try {
					writeFile.close();
				}
				catch (IOException e) {
					throw e;
				}
			if (input != null)
				try {
					input.close();
				}
				catch (IOException e) {
					throw e;
				}
			if (connection != null) connection.disconnect();
		}

		return Status.OK_STATUS;
	}
}
