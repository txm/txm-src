// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.rcp.handlers.export;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.TXMResult;
import org.txm.rcp.JobsTimer;
import org.txm.rcp.StatusLine;
import org.txm.rcp.editors.ITXMResultEditor;
import org.txm.rcp.handlers.files.EditFile;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.JobHandler;
import org.txm.utils.ExecTimer;
import org.txm.utils.logger.Log;

/**
 * Exports a result by calling the function toTxt(File f) then opens or not the result in the text editor according to the preferences.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class ExportResultFromEditor extends AbstractHandler {

	private static final String ID = ExportResultFromEditor.class.getName();

	/**
	 * Export a TXM result in a CSV file.
	 *
	 * @param event the event
	 * @return the object
	 * @throws ExecutionException the execution exception
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IEditorPart ieditor = HandlerUtil.getActiveEditor(event);
		if (!(ieditor instanceof ITXMResultEditor)) {
			return null;
		}

		ITXMResultEditor<TXMResult> editor = (ITXMResultEditor) ieditor;
		final TXMResult s = editor.getResult();

		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

		// String txmhome = Toolbox.getTxmHomePath();
		ExportResultDialog dialog = new ExportResultDialog(shell, s);
		dialog.setFilePath(LastOpened.getFolder(ID), ((TXMResult) s).getValidFileName());

		if (dialog.open() == dialog.OK) {
			StatusLine.setMessage(TXMUIMessages.exportingResults);

			final File outfile = dialog.getFile();
			LastOpened.set(ID, outfile.getParent(), outfile.getName());
			try {
				outfile.createNewFile();
			}
			catch (IOException e1) {
				Log.warning(NLS.bind(TXMUIMessages.exportColonCantCreateFileP0ColonP1, outfile, e1));
			}
			if (!outfile.canWrite()) {
				Log.warning(NLS.bind(TXMUIMessages.impossibleToReadP0, outfile));
				return null;
			}
			if (!outfile.isFile()) {
				Log.warning("Error: " + outfile + " is not a file"); //$NON-NLS-1$ //$NON-NLS-2$
				return null;
			}

			final String encoding = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_ENCODING);
			String _colseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_COL_SEPARATOR);
			final String colseparator = _colseparator;
			String _txtseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_TXT_SEPARATOR);
			final String txtseparator = _txtseparator;

			JobHandler jobhandler = new JobHandler(TXMUIMessages.exportingResults) {

				@Override
				protected IStatus _run(SubMonitor monitor) throws Exception {

					try {
						JobsTimer.start();

						if (s instanceof TXMResult) {
							TXMResult r = (TXMResult) s;

							// compute the result if needed
							if (!r.isAltered() && r.isDirty()) {
								r.compute(this);
							}
							Log.info(TXMUIMessages.bind(TXMUIMessages.exportingP0ToP1, r.getName(), outfile));
							ExecTimer.start();
							r.toTxt(outfile, encoding, colseparator, txtseparator);
							Log.info(TXMUIMessages.bind(TXMUIMessages.doneInP0, ExecTimer.stop()));
						}
						else {
							Log.warning(TXMUIMessages.TheExportedObjectIsNotATXMResultResult);
							return Status.CANCEL_STATUS;
						}

						if (outfile.exists()) {
							// Open internal editor in the UI thread
							if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPORT_SHOW)) {
								this.syncExec(new Runnable() {

									@Override
									public void run() {
										EditFile.openfile(outfile.getAbsolutePath());
									}
								});
							}
						}
						else {
							Log.warning(NLS.bind(TXMUIMessages.failedToExportResultP0ColonP1, outfile.getAbsolutePath()));
						}
					}
					catch (ThreadDeath td) {
						TXMResult r = (TXMResult) s;
						r.resetComputingState();
						throw td;
					}
					catch (Exception e) {
						Log.warning(NLS.bind(TXMUIMessages.failedToExportResultP0ColonP1, s, e));
						throw e;
					}
					return Status.OK_STATUS;
				}
			};

			jobhandler.startJob();
		}
		return null;
	}
}
