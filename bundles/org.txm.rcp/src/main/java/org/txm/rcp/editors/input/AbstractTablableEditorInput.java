// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.editors.input;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.txm.rcp.editors.ITablableEditorInput;

// TODO: Auto-generated Javadoc
/**
 * Define a tab displaying a graphics in a multi-tab editor.
 * 
 * @author sloiseau
 * 
 */
public abstract class AbstractTablableEditorInput extends AbstractEditorInput implements ITablableEditorInput {

	/*
	 * (non-Javadoc)
	 * @see org.txm.rcp.editors.ITablableEditorInput#getColumnTitles()
	 */
	@Override
	abstract public String[] getColumnTitles();

	/*
	 * (non-Javadoc)
	 * @see org.txm.rcp.editors.ITablableEditorInput#getColumnTypes()
	 */
	@Override
	abstract public String[] getColumnTypes();

	/*
	 * (non-Javadoc)
	 * @see org.txm.rcp.editors.ITablableEditorInput#getContentProvider()
	 */
	@Override
	abstract public IStructuredContentProvider getContentProvider();

	/*
	 * (non-Javadoc)
	 * @see org.txm.rcp.editors.ITablableEditorInput#getInput()
	 */
	@Override
	abstract public Object getInput();

	/*
	 * (non-Javadoc)
	 * @see org.txm.rcp.editors.ITablableEditorInput#getLabelProvider()
	 */
	@Override
	abstract public LabelProvider getLabelProvider();

}
