// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp;

import java.util.HashMap;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.txm.core.results.TXMResult;
//import org.txm.functions.queryindex.QueryIndex;
import org.txm.rcp.adapters.TXMResultAdapter;

/**
 * A factory for creating Adapter objects.
 * 
 * @author jmague
 */
public class AdapterFactory implements IAdapterFactory {

	private static boolean firstCorpusDiag = true;

	private HashMap<Class<?>, IWorkbenchAdapter> externalAdapters = new HashMap<>();

	// /** The corpus adapter. */
	// private IWorkbenchAdapter corpusAdapter = new IWorkbenchAdapter() {
	//
	// @Override
	// public Object[] getChildren(Object corpus) {
	// ArrayList<TXMResult> results = ((Corpus) corpus).getChildren(true);
	// for (int i = 0; i < results.size(); i++) {
	// Object element = results.get(i);
	//
	// // remove Text and SavedQuery from the corpora view
	// if (!TBXPreferences.getInstance().getBoolean(TBXPreferences.SHOW_ALL_RESULT_NODES) && (element instanceof Text || element instanceof SavedQuery)) {
	// results.remove(i);
	// i--;
	// }
	// }
	// List<Object> children = new ArrayList<Object>();
	// children.addAll(results);
	// return children.toArray();
	// }
	//
	// @Override
	// public ImageDescriptor getImageDescriptor(Object object) {
	// return AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, IImageKeys.CORPUS);
	// }
	//
	// @Override
	// public String getLabel(Object corpus) {
	// if (corpus instanceof MainCorpus) {
	// return ((MainCorpus) corpus).getNameForCorporaView();
	// }
	// else {
	// return ((Corpus) corpus).getName();
	// }
	// }
	//
	// @Override
	// public Object getParent(Object corpus) {
	// if (corpus instanceof Subcorpus) {
	// return ((Subcorpus) corpus).getMotherSubcorpus();
	// }
	// else {
	// return CorpusManager.getCorpusManager();
	// }
	// }
	// };

	// /** The partition adapter. */
	// private IWorkbenchAdapter partitionAdapter = new IWorkbenchAdapter() {
	//
	// @Override
	// public Object[] getChildren(Object partition) {
	// ArrayList<TXMResult> children = ((Partition) partition).getResults();
	// // System.out.println("PARTITION RESULTS "+children);
	// return children.toArray();
	// }
	//
	// @Override
	// public ImageDescriptor getImageDescriptor(Object object) {
	// return AbstractUIPlugin.imageDescriptorFromPlugin(
	// Application.PLUGIN_ID, IImageKeys.PARTITION);
	// }
	//
	// @Override
	// public String getLabel(Object partition) {
	// return ((Partition) partition).getSimpleName();
	// }
	//
	// @Override
	// public Object getParent(Object partition) {
	// return ((Partition) partition).getCorpus();
	// }
	// };


	/** The Lexicon adapter. */
	// FIXME: became useless? seems never used
	private IWorkbenchAdapter LexiconAdapter = new TXMResultAdapter() {

		@Override
		public ImageDescriptor getImageDescriptor(Object object) {
			return AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, IImageKeys.ACTION_FREQUENCY_LIST);
		}
	};

	/**
	 * Gets the adapter.
	 *
	 * @param adaptableObject the adaptable object
	 * @param adapterType the adapter type
	 * @return the adapter
	 * @see org.eclipse.core.runtime.IAdapterFactory#getAdapter(java.lang.Object,
	 *      java.lang.Class)
	 */
	@Override
	public <T> T getAdapter(Object adaptableObject, Class<T> adapterType) {
		// if (adapterType == IWorkbenchAdapter.class && adaptableObject instanceof CorpusManager) {
		// return corpusManagerAdapter;
		// }
		// if (adapterType == IWorkbenchAdapter.class && adaptableObject instanceof Corpus) {
		// return corpusAdapter;
		// }
		// if (adapterType == IWorkbenchAdapter.class && adaptableObject instanceof Partition) {
		// return partitionAdapter;
		// }

		// check adapters from plugins
		if (externalAdapters.containsKey(adaptableObject.getClass())) {
			return adapterType.cast(externalAdapters.get(adaptableObject.getClass()));
		}

		if (adapterType == IWorkbenchAdapter.class && adaptableObject instanceof TXMResult) {
			return adapterType.cast(new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, IImageKeys.ACTION_INFO);
				}
			});
		}
		return null;
	}

	/**
	 * Gets the adapter list.
	 *
	 * @return the adapter list
	 * @see org.eclipse.core.runtime.IAdapterFactory#getAdapterList()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Class<?>[] getAdapterList() {
		return new Class[] { IWorkbenchAdapter.class };
	}

}
