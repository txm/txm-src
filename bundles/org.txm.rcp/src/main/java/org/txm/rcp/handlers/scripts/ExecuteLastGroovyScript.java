// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.scripts;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.Toolbox;
import org.txm.rcp.messages.TXMUIMessages;

// TODO: Auto-generated Javadoc
/**
 * Handler: execute a Macro (Groovy script that opens an UI for parameters)
 */
public class ExecuteLastGroovyScript extends AbstractHandler {

	private static final String ID = "org.txm.rcp.commands.ExecuteMacro"; //$NON-NLS-1$

	public static File lastScript;

	public static boolean macro = false;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IWorkbenchPart activePart = HandlerUtil.getActivePart(event);
		IWorkbenchPart activeEditor = HandlerUtil.getActiveEditor(event);
		ISelection selection = HandlerUtil.getCurrentSelection(event);

		execute(activePart, activeEditor, selection);

		return null;
	}

	public static void setLastScript(File s, boolean b) {
		lastScript = s;
		macro = b;
	}

	/**
	 * Execute last Groovy script
	 * 
	 * @param page
	 * @param selection
	 */
	public static void execute(IWorkbenchPart activePart, IWorkbenchPart activeEditor, ISelection selection) {
		if (lastScript == null) return; // no last script
		if (macro)
			System.out.println(TXMUIMessages.executingLastMacroColon + lastScript);
		else
			System.out.println(TXMUIMessages.executingLastScriptColon + lastScript);

		//IPreferencesService service = Platform.getPreferencesService();
		String scriptRootDir = Toolbox.getTxmHomePath() + "/scripts"; //$NON-NLS-1$
		File currentRootDir = new File(scriptRootDir, "groovy/user"); //$NON-NLS-1$

		if (!currentRootDir.exists()) {
			System.out.println(TXMUIMessages.errorColonCurrentGroovyScriptDirectoryDoesNotExistColon + currentRootDir);
			return;
		}

		ExecuteGroovyScript.executeScript(currentRootDir, lastScript.getAbsolutePath(), activePart, activeEditor, selection, false, null, null, null); //$NON-NLS-1$
	}
}
