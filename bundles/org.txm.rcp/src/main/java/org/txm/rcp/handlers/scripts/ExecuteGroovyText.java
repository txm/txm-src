// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.scripts;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.txm.Toolbox;
import org.txm.rcp.editors.TxtEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.utils.logger.Log;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

// TODO: Auto-generated Javadoc
/**
 * execute a selection of Groovy code from the TxtEditor
 * 
 * @author mdecorde.
 */
public class ExecuteGroovyText extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands
	 * .ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event)
				.getActivePage().getSelection();
		if (!(selection instanceof TextSelection))
			return null;
		TextSelection t = (TextSelection) selection;
		String result = t.getText();

		IWorkbenchPart page = HandlerUtil.getActiveWorkbenchWindow(event)
				.getActivePage().getActivePart();
		TxtEditor te = (TxtEditor) page;
		FileStoreEditorInput input = (FileStoreEditorInput) te.getEditorInput();

		File f = new File(input.getURI().getPath());
		String scriptfile = f.getAbsolutePath();

		executeText(result, scriptfile);
		return null;
	}

	/**
	 * Execute text.
	 * 
	 * @param text
	 *            the text
	 * @param scriptfile
	 *            the scriptfile
	 * @return the object
	 */
	public static Object executeText(final String text, final String scriptfile) {
		if (!new File(scriptfile).exists()) {
			System.err.println(NLS.bind(TXMUIMessages.p0ScriptFileDoesntExist, scriptfile));
			return null;
		}
		final String scriptRootDir = Toolbox.getTxmHomePath() + "/scripts"; //$NON-NLS-1$

		// get current selection of the corpora view
		final Object corpusViewSelection = CorporaView.getFirstSelectedObject();
		final Object corpusViewSelections = CorporaView.getSelectedObjects();
		final Object selection = null;
		final List selections = null;

		JobHandler jobhandler = new JobHandler("run") { //$NON-NLS-1$

			@Override
			protected IStatus _run(SubMonitor monitor) throws MalformedURLException {

				List<File> classPath = new ArrayList<File>();
				classPath.add(new File(scriptRootDir));
				classPath.add(new File(scriptfile));
				classPath.add(new File(scriptfile).getParentFile());
				classPath.add(new File(".")); //$NON-NLS-1$
				// System.out.println(classPath);

				Binding binding = new Binding();
				//binding.setVariable("editor", page); //$NON-NLS-1$
				binding.setVariable("selection", selection); //$NON-NLS-1$
				binding.setVariable("selections", selections); //$NON-NLS-1$
				binding.setVariable("corpusViewSelection", corpusViewSelection); //$NON-NLS-1$
				binding.setVariable("corpusViewSelections", corpusViewSelections); //$NON-NLS-1$
				binding.setProperty("monitor", this); //$NON-NLS-1$
				//binding.setProperty("gse", gse);
				GroovyShell shell = new GroovyShell(this.getClass().getClassLoader(), binding);

				for (File file : classPath) {
					try {
						shell.getClassLoader().addURL(file.toURI().toURL());
						shell.getClassLoader().addClasspath(file.getAbsolutePath());
					}
					catch (MalformedURLException e) {
						throw e;
					}
				}

				System.out.println("Executing text selection..."); //$NON-NLS-1$
				long time = System.currentTimeMillis();
				shell.evaluate(text);
				Log.fine(NLS.bind(TXMUIMessages.doneP0Ms, (System.currentTimeMillis() - time)));
				return Status.OK_STATUS;
			}
		};

		jobhandler.startJob();
		return null;
	}
}
