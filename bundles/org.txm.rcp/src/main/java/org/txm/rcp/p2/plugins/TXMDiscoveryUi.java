package org.txm.rcp.p2.plugins;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.equinox.internal.p2.discovery.model.CatalogItem;
import org.eclipse.equinox.internal.p2.ui.discovery.DiscoveryUi;
import org.eclipse.equinox.internal.p2.ui.discovery.util.CommonColors;
import org.eclipse.equinox.internal.p2.ui.discovery.wizards.Messages;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.statushandlers.StatusManager;

/***
 * Copy of DiscoveryUi, uses a TXMDiscoveryInstallOperation runnable object
 * 
 * @author nkredens
 *
 */
public abstract class TXMDiscoveryUi {

	public static final String ID_PLUGIN = "org.eclipse.equinox.p2.ui.discovery"; //$NON-NLS-1$

	private static CommonColors commonColors;

	private TXMDiscoveryUi() {
		// don't allow clients to instantiate
	}

	public static boolean install(List<CatalogItem> descriptors,
			IRunnableContext context) {
		try {
			IRunnableWithProgress runner = new TXMDiscoveryInstallOperation(
					descriptors);
			context.run(true, true, runner);
		}
		catch (InvocationTargetException e) {
			IStatus status = new Status(IStatus.ERROR, DiscoveryUi.ID_PLUGIN,
					NLS.bind(Messages.ConnectorDiscoveryWizard_installProblems,
							new Object[] { e.getCause().getMessage() }),
					e.getCause());
			StatusManager.getManager().handle(
					status,
					StatusManager.SHOW | StatusManager.BLOCK
							| StatusManager.LOG);
			return false;
		}
		catch (InterruptedException e) {
			// canceled
			return false;
		}
		return true;
	}

	public static CommonColors getCommonsColors() {
		if (commonColors == null) {
			commonColors = new CommonColors(Display.getDefault(),
					JFaceResources.getResources());
		}
		return commonColors;
	}
}
