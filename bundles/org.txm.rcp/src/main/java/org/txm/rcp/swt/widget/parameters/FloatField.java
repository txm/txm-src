package org.txm.rcp.swt.widget.parameters;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.kohsuke.args4j.NamedOptionDef;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.logger.Log;

public class FloatField extends LabelField {

	Text dt;

	public FloatField(Composite parent, int style, NamedOptionDef parameter) {
		super(parent, style, parameter);

		dt = new Text(this, SWT.SINGLE | SWT.BORDER);
		dt.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));
		resetToDefault();
	}

	public Float getWidgetValue() {
		try {
			return Float.parseFloat(dt.getText());
		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.bind(TXMCoreMessages.error_errorP0, Log.toString(e)));
		}
		return null;
	}


	public void setDefault(Object text) {
		dt.setText("" + text); //$NON-NLS-1$
	}

	@Override
	public void resetToDefault() {
		dt.setText(getWidgetDefault());
	}
}
