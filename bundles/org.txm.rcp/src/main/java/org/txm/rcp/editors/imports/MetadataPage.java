// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.editors.imports;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.txm.core.preferences.TBXPreferences;
import org.txm.metadatas.Metadata;
import org.txm.metadatas.Metadatas;
import org.txm.metadatas.TextInjection;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.messages.TXMUIMessages;

// TODO: Auto-generated Javadoc
/**
 * The Class MetadataPage.
 */
public class MetadataPage extends FormPage {

	/** The metadatas. */
	protected ArrayList<Metadata> metadatas = new ArrayList<Metadata>();

	/** The srcdir. */
	File srcdir;

	/** The paramtokenizer. */
	File paramtokenizer;

	/** The xsltfile. */
	File xsltfile;

	/** The xsltparams. */
	String[] xsltparams;//{"val1",1,"val2",2}

	/** The form. */
	ScrolledForm form;

	/** The toolkit. */
	FormToolkit toolkit;

	/** The basename text. */
	Text basenameText;

	/** The btn guess lang. */
	Button btnGuessLang;

	/** The btn guess2 lang. */
	Button btnGuess2Lang;

	/** The btn select lang. */
	Button btnSelectLang;

	/** The langcombo. */
	Combo langcombo;

	/** The btn default encoding. */
	Button btnDefaultEncoding;

	/** The btn guess encoding. */
	Button btnGuessEncoding;

	/** The btn guess2 encoding. */
	Button btnGuess2Encoding;

	/** The btn select encoding. */
	Button btnSelectEncoding;

	/** The encoding combo. */
	Combo encodingCombo;

	/** The metadata table. */
	Table metadataTable;

	/** The viewer. */
	//TableViewer viewer;

	/** The rmvmetadata. */
	//private Button rmvmetadata;

	/** The clearmetadata. */
	//private Button clearmetadata;

	/** The scanmetadatafile. */
	Button scanmetadatafile;

	/** The addmetadata. */
	//Button addmetadata;

	/** The partition column. */
	TableViewerColumn partitionColumn;

	/** The selection column. */
	TableViewerColumn selectionColumn;

	/** The display column. */
	TableViewerColumn displayColumn;

	/** The longname column. */
	TableViewerColumn longnameColumn;

	/** The name column. */
	TableViewerColumn nameColumn;

	/** The type column. */
	private TableViewerColumn typeColumn;

	private TableViewer viewer2;

	private TableViewerColumn displayColumn2;

	File metadataFile;

	private ArrayList<TableViewerColumn> columns = new ArrayList<TableViewerColumn>();

	private CorpusPage main;

	/**
	 * Instantiates a new metadata page.
	 *
	 * @param editor the editor
	 * @param main
	 */
	public MetadataPage(FormEditor editor, CorpusPage main) {
		super(editor, TXMUIMessages.mainPage, TXMUIMessages.metadataPreview);
		this.main = main;
	}

	int index = 0;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.forms.editor.FormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
	 */
	@Override
	protected void createFormContent(final IManagedForm managedForm) {
		form = managedForm.getForm();
		toolkit = managedForm.getToolkit();
		form.setText(TXMUIMessages.metadatasParameters);
		GridLayout layout = new GridLayout(2, false);
		form.getBody().setLayout(layout);

		scanmetadatafile = toolkit.createButton(form.getBody(), TXMUIMessages.Reload, SWT.PUSH);
		scanmetadatafile.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 1, 1));
		scanmetadatafile.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				reloadViewers();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		//		clearmetadata = toolkit.createButton(form.getBody(), Messages.MetadataPage_8, SWT.PUSH);
		//		clearmetadata.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 1,1));
		//		clearmetadata.addSelectionListener(new SelectionListener() {
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				metadatas.clear();
		//  			viewer.refresh();
		//				viewer2.refresh();
		//			}
		//
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) { }
		//		});
		//		addmetadata = toolkit.createButton(form.getBody(), Messages.MetadataPage_9, SWT.PUSH);
		//		addmetadata.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 1,1));
		//		addmetadata.addSelectionListener(new SelectionListener() {
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				metadatas.add(new Metadata(Messages.MetadataPage_10, Messages.MetadataPage_11, Messages.MetadataPage_12, "String", null, false, false, false, false, 100)); //$NON-NLS-4$
		//				viewer.setInput(metadatas);
		//				viewer.refresh();
		//				viewer2.setInput(metadatas);
		//				viewer2.refresh();
		//			}
		//
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) { }
		//		});
		//		rmvmetadata = toolkit.createButton(form.getBody(), Messages.MetadataPage_14, SWT.PUSH);
		//		rmvmetadata.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 1,1));
		//		rmvmetadata.addSelectionListener(new SelectionListener() {
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
		//				for(Object obj : selection.toArray()) {
		//					metadatas.remove(obj);
		//				}
		////				viewer.setInput(metadatas);
		////				viewer.refresh();
		//				viewer2.setInput(metadatas);
		//				viewer2.refresh();
		//			}
		//
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) { }
		//		});

		//		metadataTable = toolkit.createTable(form.getBody(), SWT.FULL_SELECTION | SWT.MULTI | SWT.V_SCROLL);
		//		GridData gd = new GridData(GridData.FILL, GridData.FILL, false, false,2,1);
		//		gd.heightHint = 100;
		//		metadataTable.setLayoutData(gd);
		//		metadataTable.setHeaderVisible(true);
		//		metadataTable.setLinesVisible(true);
		//
		//		metadataTable.addKeyListener(new KeyListener() {
		//			@Override
		//			public void keyReleased(KeyEvent e) {
		//				if (e.keyCode == SWT.DEL)
		//				{
		//					IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
		//					for(Object obj : selection.toArray()) {
		//						metadatas.remove(obj);
		//					}
		//					viewer.setInput(metadatas);
		//					viewer.refresh();
		//					viewer2.setInput(metadatas);
		//					viewer2.refresh();
		//				}
		//			}
		//
		//			@Override
		//			public void keyPressed(KeyEvent e) {}
		//		});

		//		viewer = new TableViewer(metadataTable);
		//		viewer.setContentProvider(new IStructuredContentProvider(){
		//
		//			@Override
		//			public Object[] getElements(Object inputElement) {
		//				return metadatas.toArray(new Metadata[metadatas.size()]);
		//			}
		//
		//			@Override
		//			public void dispose() {	}
		//
		//			@Override
		//			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) { }
		//
		//		});
		//		viewer.setInput(metadatas);
		//
		//		nameColumn = new TableViewerColumn(viewer, SWT.NONE);
		//		nameColumn.getColumn().setWidth(100);
		//		nameColumn.getColumn().setText(Messages.ObjectExplorer_26);
		//		nameColumn.setLabelProvider(new ColumnLabelProvider() {
		//			@Override
		//			public String getText(Object element) {
		//				Metadata data = (Metadata) element;
		//				return data.id;
		//			};
		//
		//			public Image getImage(Object element) {return null;};
		//		});
		//		nameColumn.setEditingSupport(new MetadataCellEditor(viewer, 0));

		//		longnameColumn = new TableViewerColumn(viewer, SWT.NONE);
		//		longnameColumn.getColumn().setWidth(150);
		//		longnameColumn.getColumn().setText(Messages.MetadataPage_16);
		//		longnameColumn.setLabelProvider(new ColumnLabelProvider() {
		//			@Override
		//			public String getText(Object element) {
		//				Metadata data = (Metadata) element;
		//				return data.longname;
		//			};
		//
		//			public Image getImage(Object element) {return null;};
		//		});
		//		longnameColumn.setEditingSupport(new MetadataCellEditor(viewer, 1));
		//
		//		typeColumn = new TableViewerColumn(viewer, SWT.NONE);
		//		typeColumn.getColumn().setWidth(100);
		//		typeColumn.getColumn().setText(Messages.FrequencyListEditorInput_9);
		//		typeColumn.setLabelProvider(new ColumnLabelProvider() {
		//			@Override
		//			public String getText(Object element) {
		//				Metadata data = (Metadata) element;
		//				return data.type;
		//			};
		//
		//			public Image getImage(Object element) {return null;};
		//		});
		//		typeColumn.setEditingSupport(new MetadataCellEditor(viewer, 2));
		//
		//		selectionColumn = new TableViewerColumn(viewer, SWT.NONE);
		//		selectionColumn.getColumn().setWidth(100);
		//		selectionColumn.getColumn().setText(Messages.MetadataPage_18);
		//		selectionColumn.setLabelProvider(new ColumnLabelProvider() {
		//			@Override
		//			public String getText(Object element) {
		//				return null;
		//			};
		//			public Image getImage(Object element) {
		//				Metadata data = (Metadata) element;
		//				if (data.selection)
		//					return IImageKeys.getImage(IImageKeys.CHECKED);
		//				else
		//					return IImageKeys.getImage(IImageKeys.UNCHECKED);
		//			};
		//		});
		//
		//		partitionColumn = new TableViewerColumn(viewer, SWT.NONE);
		//		partitionColumn.getColumn().setWidth(100);
		//		partitionColumn.getColumn().setText(Messages.MetadataPage_19);
		//		partitionColumn.setLabelProvider(new ColumnLabelProvider() {
		//			@Override
		//			public String getText(Object element) {
		//
		//				return null;
		//			};
		//
		//			public Image getImage(Object element) 
		//			{
		//				Metadata data = (Metadata) element;
		//				if (data.partition)
		//					return IImageKeys.getImage(IImageKeys.CHECKED);
		//				else
		//					return IImageKeys.getImage(IImageKeys.UNCHECKED);
		//			};
		//		});
		//
		//		displayColumn = new TableViewerColumn(viewer, SWT.NONE);
		//		displayColumn.getColumn().setWidth(100);
		//		displayColumn.getColumn().setText(Messages.MetadataPage_20);
		//		displayColumn.setLabelProvider(new ColumnLabelProvider() {
		//			@Override
		//			public String getText(Object element) {
		//
		//				return null;
		//			};
		//
		//			public Image getImage(Object element) 
		//			{
		//				Metadata data = (Metadata) element;
		//				if (data.display)
		//					return IImageKeys.getImage(IImageKeys.CHECKED);
		//				else
		//					return IImageKeys.getImage(IImageKeys.UNCHECKED);
		//			};
		//		});
		//
		//		viewer.getTable().addMouseListener(new MouseListener() {
		//			@Override
		//			public void mouseUp(MouseEvent e) { }
		//			@Override
		//			public void mouseDown(MouseEvent e) { }
		//
		//			@Override
		//			public void mouseDoubleClick(MouseEvent e) {int colid = getPointedColumn(e);
		//			IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
		//			for (Object obj : selection.toArray()) {
		//				Metadata data = (Metadata) obj;
		//				switch (colid) {
		//				case 3:
		//					data.selection = !data.selection;
		//					break;
		//				case 4:
		//					data.partition = !data.partition;
		//					break;
		//				case 5:
		//					data.display = !data.display;
		//					break;
		//				}
		//			}
		//			viewer.setInput(metadatas);
		//			viewer.refresh(); 
		//			}
		//		});

		metadataTable = toolkit.createTable(form.getBody(), SWT.FULL_SELECTION | SWT.MULTI | SWT.V_SCROLL);
		GridData gd = new GridData(GridData.FILL, GridData.FILL, true, true, 2, 1);
		metadataTable.setLayoutData(gd);
		metadataTable.setHeaderVisible(true);
		metadataTable.setLinesVisible(true);
		viewer2 = new TableViewer(metadataTable);
		TableUtils.installCopyAndSearchKeyEvents(viewer2);
		TableUtils.installColumnResizeMouseWheelEvents(viewer2);
		viewer2.setContentProvider(new IStructuredContentProvider() {

			@Override
			public Object[] getElements(Object inputElement) {
				if (inputElement instanceof Metadatas) {
					Metadatas m = (Metadatas) inputElement;
					ArrayList<String> header = m.getHeadersList();
					System.out.println(NLS.bind(TXMUIMessages.metadataColumnTitlesP0, header));

					Object[] elems = new Object[m.keySet().size()];
					int i = 0;
					for (String key : m.keySet()) {
						elems[i++] = m.get(key);
					}
					return elems;

				}
				else {
					return new Object[0];
				}
			}

			@Override
			public void dispose() {
			}

			@Override
			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			}

		});

		displayColumn2 = new TableViewerColumn(viewer2, SWT.NONE);
		displayColumn2.getColumn().setWidth(120);
		displayColumn2.getColumn().setText(TXMUIMessages.textID);
		displayColumn2.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof TextInjection) {
					TextInjection ti = (TextInjection) element;
					return ti.id;
				}
				else
					return ""; //$NON-NLS-1$
			};

			@Override
			public Image getImage(Object element) {
				return null;
			};
		});

		//if (metadataFile != null) {
		reloadViewers();
		//}
	}

	protected void reloadViewers() {
		if (viewer2 == null) return;
		//viewer2.setInput(null);
		//viewer2.refresh();
		if (main.getRootDir() == null) return;

		metadataFile = Metadatas.findMetadataFile(new File(main.getRootDir()));
		Metadatas temp = null;
		if (metadataFile != null && metadataFile.exists()) {//if their is a metadata file OK
			String encoding = TBXPreferences.getInstance().getString(TBXPreferences.METADATA_ENCODING);
			String colsep = TBXPreferences.getInstance().getString(TBXPreferences.METADATA_COLSEPARATOR);
			String txtsep = TBXPreferences.getInstance().getString(TBXPreferences.METADATA_TXTSEPARATOR);
			temp = new Metadatas(metadataFile, encoding, colsep, txtsep, 1);
		}
		else {
			System.out.println(NLS.bind(TXMUIMessages.noMetadataFileP0, main.getRootDir()));
			return;
		}

		File xmlfile = temp.getXMLFile();
		if (xmlfile.exists()) {
			xmlfile.delete();
		}
		metadatas.clear();
		for (Metadata m : temp.getMetadatas()) {
			if (!metadatas.contains(m))
				metadatas.add(m);
		}

		for (TableViewerColumn col : columns) {
			col.getColumn().dispose();
		}
		columns = new ArrayList<TableViewerColumn>();

		index = 0;
		for (String key : temp.getHeadersList()) {
			TableViewerColumn col = new TableViewerColumn(viewer2, SWT.NONE);
			columns.add(col);
			col.getColumn().setWidth(100);
			col.getColumn().setText(key);
			col.setLabelProvider(new ColumnLabelProvider() {

				int myindex = index++;

				@Override
				public String getText(Object element) {
					if (element instanceof TextInjection) {
						TextInjection ti = (TextInjection) element;
						return ti.get(myindex).getSecond();
					}
					else
						return ""; //$NON-NLS-1$
				};

				@Override
				public Image getImage(Object element) {
					return null;
				};
			});
		}

		//		viewer.setInput(metadatas);
		//		viewer.refresh();
		viewer2.setInput(temp);
		//viewer2.refresh();
	}

	/**
	 * Creates the list section.
	 */
	protected void createListSection() {

	}

	public void setMetadataFile(File metadataFile2) {
		this.metadataFile = metadataFile2;
	}

	public void reload() {
		reloadViewers();
	}
}
