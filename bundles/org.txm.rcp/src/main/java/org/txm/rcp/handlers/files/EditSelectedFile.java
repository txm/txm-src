// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.rcp.handlers.files;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.txm.rcp.editors.TxtEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.utils.logger.Log;

/**
 * Open a file in the TxtEditor
 * 
 * @author mdecorde.
 */
public class EditSelectedFile extends AbstractHandler {

	public static final String ID = EditSelectedFile.class.getCanonicalName(); //$NON-NLS-1$

	/** The lastopenedfile. */
	public static String lastopenedfile;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		File file = null;
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection != null & selection instanceof IStructuredSelection) {
			IStructuredSelection strucSelection = (IStructuredSelection) selection;
			for (Object o : strucSelection.toList())
				if (o != null && o instanceof File) {
					file = (File) o;
					break;
				}
		}

		if (file == null) {
			System.out.println(TXMUIMessages.SelectionHasNoFileAborting);
			return null;
		}
		else {
			if (file.isDirectory()) {
				System.out.println(TXMUIMessages.TheSelectedFileIsADirectory);
				return null;
			}
			else {
				openfile(file.getAbsolutePath());
			}
		}

		return null;
	}

	/**
	 * Open a file in TXM TxtEditor.
	 *
	 * @param file the filepath
	 */
	static public void openfile(File file) {
		if (file.isDirectory())
			return;
		try {
			IFileStore fileOnLocalDisk = EFS.getLocalFileSystem().getStore(file.toURI());

			FileStoreEditorInput editorInput = new FileStoreEditorInput(fileOnLocalDisk);
			IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			IWorkbenchPage page = window.getActivePage();

			IEditorPart a = page.openEditor(editorInput, TxtEditor.ID, true);
			TxtEditor editor = (TxtEditor) a;
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
	}

	/**
	 * Openfile.
	 *
	 * @param filepath the filepath
	 */
	static public void openfile(String filepath) {
		openfile(new File(filepath));
	}
}
