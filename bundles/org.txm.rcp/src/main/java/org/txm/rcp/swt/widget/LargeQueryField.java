package org.txm.rcp.swt.widget;

import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Composite;
import org.txm.searchengine.core.Query;


public class LargeQueryField extends StyledText {

	Class<? extends Query> clazz;

	public LargeQueryField(Composite parent, int style, Class<? extends Query> clazz) {

		super(parent, style);
		this.clazz = clazz;
	}

	public void setQueryClass(Class<? extends Query> clazz) {
		this.clazz = clazz;
	}

	public Query getQuery() {
		try {
			Query q = clazz.newInstance();
			q.setQuery(this.getText());
			return q;
		}
		catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
