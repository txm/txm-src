package org.txm.rcp.handlers.export;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = Messages.class.getPackageName() + ".messages"; //$NON-NLS-1$
	public static String bmp;
	public static String csv;
	public static String docx;
	public static String gif;
	public static String jpeg;
	public static String jpg;
	public static String ods;
	public static String odt;
	public static String pdf;
	public static String png;
	public static String rtf;
	public static String svg;
	public static String tiff;
	public static String tsv;
	public static String txm;
	public static String txmcmd;
	public static String txt;
	public static String xlsx;
	public static String rds;
	public static String rda;
	
	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
