// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.rcp.adapters;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.eclipse.ui.model.WorkbenchAdapter;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.txm.rcp.Application;
import org.txm.rcp.IImageKeys;


/**
 * A factory for creating Adapter objects.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class RCPProjectAdapterFactory extends TXMResultAdapterFactory {

	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (adapterType == IWorkbenchAdapter.class && adaptableObject instanceof IProject) {
			return new WorkbenchAdapter() {

				@Override
				public Object[] getChildren(Object o) {
					return new Object[0];
				}

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, IImageKeys.PROJECT);
				}

				@Override
				public String getLabel(Object o) {
					return ((IProject) o).getName();
				}
			};
		}
		return null;
	}
}
