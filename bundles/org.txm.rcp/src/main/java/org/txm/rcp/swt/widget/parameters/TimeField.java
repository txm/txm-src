package org.txm.rcp.swt.widget.parameters;

import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;
import org.kohsuke.args4j.NamedOptionDef;

public class TimeField extends ParameterField {

	DateTime dt;

	public TimeField(Composite parent, int style, NamedOptionDef parameter) {
		super(parent, style, parameter);
		this.setLayout(new RowLayout(SWT.HORIZONTAL));
		this.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));

		Label l = new Label(this, SWT.NONE);
		l.setAlignment(SWT.RIGHT);
		l.setText(getWidgetLabel());
		l.setToolTipText(getWidgetUsage());
		GridData gd = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd.widthHint = 100;
		l.setLayoutData(gd);

		dt = new DateTime(this, SWT.TIME);
		dt.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));
	}

	public Date getWidgetValue() {
		int day = dt.getDay(); // Calendar.DAY_OF_MONTH
		int hour = dt.getHours(); // Calendar.HOUR_OF_DAY
		int min = dt.getMinutes(); // Calendar.MINUTE
		int month = dt.getMonth(); // Calendar.MONTH
		int second = dt.getSeconds(); // Calendar.SECOND
		int year = dt.getYear(); // Calendar.YEAR
		//TODO: not implemented
		//System.out.println("year="+year+" month="+month+" day="+day+ " hour="+hour+" min="+min+" sec="+second); 
		return null;
	}

	@Override
	public void setDefault(Object value) {
		//TODO: not implemented
	}

	@Override
	public void resetToDefault() {
		//TODO: not implemented
	}
}
