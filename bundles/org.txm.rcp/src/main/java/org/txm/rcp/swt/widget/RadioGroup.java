package org.txm.rcp.swt.widget;

import java.util.LinkedHashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.txm.rcp.swt.GLComposite;

/**
 * Wrap SWT Button (radio) in a Group + methods to get&set selection
 * 
 * @author mdecorde
 *
 */
public class RadioGroup extends GLComposite {

	LinkedHashMap<String, Button> buttons = new LinkedHashMap<>();

	private Group group;

	public RadioGroup(Composite parent, int style, String title, String[][] choices) {

		super(parent, style, "RadioGroup");
		this.getLayout().numColumns = 1;

		group = new Group(this, SWT.NONE);
		if (title != null) group.setText(title);
		group.setLayout(new RowLayout(SWT.HORIZONTAL));

		for (String[] choice : choices) {

			if (choice.length == 0) continue;
			if (choice[0] == null) continue;

			Button b = new Button(group, SWT.RADIO);
			b.setData(choice[0]);
			b.setText(choice[0]);
			
			b.setToolTipText(choice[0]);
			if (choice.length > 1 && choice[1] != null) {
				b.setText(choice[1]);
				b.setToolTipText(choice[1]);
			}
			if (choice.length > 2 && choice[2] != null) {
				b.setToolTipText(choice[2]);
			}
			buttons.put(choice[0], b);
		}
	}

	public void setVertical() {

		group.setLayout(new RowLayout(SWT.VERTICAL));
	}

	public void setHorizontal() {

		group.setLayout(new RowLayout(SWT.HORIZONTAL));
	}

	public String getSelection() {

		for (Button b : buttons.values()) {

			if (b.getSelection()) return b.getData().toString();
		}
		return null;
	}

	public void setSelection(String value) {

		for (Button b : buttons.values()) {

			if (b.getData().toString().equals(value)) b.setSelection(true);
			;
		}
	}

}
