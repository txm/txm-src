// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands;

import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.osgi.util.NLS;
import org.txm.rcp.Activator;
import org.txm.utils.logger.Log;

/**
 * Replace a TXM URL with an accessible one and right language
 * 
 * @author mdecorde.
 */
public class OpenLocalizedWebPage extends AbstractHandler {

	/** The Constant ID. */
	public final static String ID = "org.txm.rcp.commands.OpenLocalizedWebPage"; //$NON-NLS-1$

	public static final String GITPAGES = "https://txm.gitpages.huma-num.fr/"; //$NON-NLS-1$

	public static final String URL_TEXTOMETRIE = GITPAGES + "textometrie/"; //$NON-NLS-1$

	public static final String URL_TXMMANUAL = GITPAGES + "txm-manual/"; //$NON-NLS-1$

	public static final String URL_TXMDEPOT = URL_TEXTOMETRIE + "txm-software/"; //$NON-NLS-1$

	public static final String HTMLPAGES = URL_TEXTOMETRIE + "html/"; //$NON-NLS-1$

	public static final String FILESPAGES = URL_TEXTOMETRIE + "files/"; //$NON-NLS-1$

	public static final String TEXTOMETRIE = "textometrie"; //$NON-NLS-1$

	public static final String TEXTOMETRIE_DOCS = "textometrie_docs"; //$NON-NLS-1$

	public static final String SF = "sf"; //$NON-NLS-1$

	public static final String SF_DOCS = "sf_docs"; //$NON-NLS-1$

	public static final String TREETAGGER = "treetagger"; //$NON-NLS-1$

	public static final String REFMAN = "refman"; //$NON-NLS-1$

	public static final String PDFREFMAN = "pdfrefman"; //$NON-NLS-1$

	public static final String BUG = "bug"; //$NON-NLS-1$

	public static final String FEATURE = "feature"; //$NON-NLS-1$

	public static final String MAILINGLIST = "mailinglist"; //$NON-NLS-1$

	public static final String FAQ = "faq"; //$NON-NLS-1$

	public static final String CONTRIB = "contrib"; //$NON-NLS-1$

	public static final String TXMUSERS = "txm-users wiki"; //$NON-NLS-1$

	public static final String MACROS = "macros"; //$NON-NLS-1$

	public static final String CORPORA = "corpora"; //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String key = event
				.getParameter("org.txm.rcp.commands.commandParameter3"); //$NON-NLS-1$
		if (key != null && key.trim().length() > 0) {
			openfile(key);
			return null;
		}

		return null;
	}

	/**
	 * try connecting to an URL if it fails, the alternative url is returned
	 * 
	 * @param urltoTest
	 * @param alternativeUrl
	 * @return
	 */
	static public String getValidURL(String urltoTest, String alternativeUrl) {
		try {
			URL url = new URL(urltoTest);
			if (url.getProtocol().equals("https")) { //$NON-NLS-1$

				// ts.setCertificateEntry(UUID.randomUUID(), cert);
				HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
				if (SSLContext.getDefault() != null) {

					// Create a trust manager that does not validate certificate chains
					TrustManager[] trustAllCerts = new TrustManager[] {
							new X509TrustManager() {

								@Override
								public java.security.cert.X509Certificate[] getAcceptedIssuers() {
									return new java.security.cert.X509Certificate[0];
								}

								@Override
								public void checkClientTrusted(
										java.security.cert.X509Certificate[] certs, String authType) {
								}

								@Override
								public void checkServerTrusted(
										java.security.cert.X509Certificate[] certs, String authType) {
								}
							}
					};

					// Install the all-trusting trust manager for this connection ONLY
					try {
						SSLContext sc = SSLContext.getInstance("SSL"); //$NON-NLS-1$
						sc.init(null, trustAllCerts, new java.security.SecureRandom());
						connection.setSSLSocketFactory(sc.getSocketFactory());
					}
					catch (GeneralSecurityException e) {
						Log.printStackTrace(e);
					}
				}

				if (connection.getResponseCode() != HttpsURLConnection.HTTP_OK) {
					Log.fine(NLS.bind("HTTP Error while testing the \"{0}\" web page: {1}", urltoTest, connection.getResponseCode())); //$NON-NLS-1$
					connection.disconnect();
					return alternativeUrl;
				}
				connection.disconnect();
			}
			else {
				url.openStream().close();
			}

		}
		catch (Exception e) {
			Log.fine(NLS.bind("Error while testing the \"{0}\" web page: {1}", urltoTest, e.getMessage())); //$NON-NLS-1$
			return alternativeUrl;
		}
		return urltoTest;
	}

	/**
	 * Openfile.
	 *
	 * @return the txm browser
	 */
	static public Object openfile(String key) {

		String version = Platform.getBundle(Activator.PLUGIN_ID).getVersion().toString();
		int idx = version.lastIndexOf("."); //$NON-NLS-1$
		if (idx > 0) version = version.substring(0, idx); // remove the "qualifier" part

		String locale = Locale.getDefault().getLanguage();

		if (TEXTOMETRIE.equals(key)) {
			if ("fr".equals(locale)) { //$NON-NLS-1$
				return OpenBrowser.openfile(URL_TEXTOMETRIE);
			}
			else {
				return OpenBrowser.openfile(getValidURL(URL_TEXTOMETRIE + locale, URL_TEXTOMETRIE + "en")); //$NON-NLS-1$
			}
		}
		else if (FAQ.equals(key)) {
			return OpenBrowser.openfile("https://groupes.renater.fr/wiki/txm-users/public/faq"); //$NON-NLS-1$
		}
		else if (CONTRIB.equals(key)) {
			return OpenBrowser.openfile("https://groupes.renater.fr/wiki/txm-users/public/contribuer"); //$NON-NLS-1$
		}
		else if (SF_DOCS.equals(key)) {
			if ("fr".equals(locale)) { //$NON-NLS-1$
				return OpenBrowser.openfile(URL_TEXTOMETRIE + "Documentation/#txm-manual"); //$NON-NLS-1$
			}
			else {
				return OpenBrowser.openfile(getValidURL(URL_TEXTOMETRIE + locale + "Documentation/#txm-manual", URL_TEXTOMETRIE + "en/Documentation/#txm-manual")); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		else if (SF.equals(key)) {
			if ("fr".equals(locale)) { //$NON-NLS-1$
				return OpenBrowser.openfile(FILESPAGES);
			}
			else {
				return OpenBrowser.openfile(getValidURL(FILESPAGES + locale, URL_TEXTOMETRIE + "en")); //$NON-NLS-1$
			}
		}
		else if (TEXTOMETRIE_DOCS.equals(key)) {
			if ("fr".equals(locale)) { //$NON-NLS-1$
				return OpenBrowser.openfile(URL_TEXTOMETRIE + "Documentation"); //$NON-NLS-1$
			}
			else {
				return OpenBrowser.openfile(getValidURL(URL_TEXTOMETRIE + locale + "/Documentation", URL_TEXTOMETRIE + "en/Documentation")); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		else if (TREETAGGER.equals(key)) {
			if ("fr".equals(locale)) { //$NON-NLS-1$
				return OpenBrowser.openfile(URL_TEXTOMETRIE + "InstallTreeTagger"); //$NON-NLS-1$
			}
			else {
				return OpenBrowser.openfile(getValidURL(URL_TEXTOMETRIE + locale + "/InstallTreeTagger/", URL_TEXTOMETRIE + "en/InstallTreeTagger")); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		else if (REFMAN.equals(key)) {
			if ("fr".equals(locale)) { //$NON-NLS-1$
				return OpenBrowser.openfile(URL_TXMMANUAL);
			}
			else {
				return OpenBrowser.openfile(getValidURL(URL_TXMMANUAL + locale, URL_TXMMANUAL));
			}
		}
		else if (CORPORA.equals(key)) {
			if ("fr".equals(locale)) { //$NON-NLS-1$
				return OpenBrowser.openfile(FILESPAGES + "corpora"); //$NON-NLS-1$
			}
			else {
				return OpenBrowser.openfile(getValidURL(FILESPAGES + "corpora/" + locale, FILESPAGES + "corpora/en")); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		else if (PDFREFMAN.equals(key)) {
			return OpenBrowser.openfile(FILESPAGES + "documentation/Manuel%20de%20TXM%200.8%20FR.pdf"); //$NON-NLS-1$
		}
		else if (BUG.equals(key)) {
			if ("fr".equals(locale)) { //$NON-NLS-1$
				return OpenBrowser.openfile("https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel"); //$NON-NLS-1$
			}
			else {
				return OpenBrowser.openfile("https://sourceforge.net/p/txm/feature-requests"); //$NON-NLS-1$
			}
		}
		else if (FEATURE.equals(key)) {
			if ("fr".equals(locale)) { //$NON-NLS-1$
				return OpenBrowser.openfile("https://groupes.renater.fr/wiki/txm-users/public/demande_de_fonctionnalites"); //$NON-NLS-1$
			}
			else {
				return OpenBrowser.openfile("https://sourceforge.net/p/txm/feature-requests"); //$NON-NLS-1$
			}
		}
		else if (MAILINGLIST.equals(key)) {
			return OpenBrowser.openfile("https://groupes.renater.fr/sympa/subscribe/txm-users"); //$NON-NLS-1$
		}
		else if (MACROS.equals(key)) {
			return OpenBrowser.openfile("https://groupes.renater.fr/wiki/txm-users/public/macros"); //$NON-NLS-1$
		}
		else if (TXMUSERS.equals(key)) {
			return OpenBrowser.openfile("https://groupes.renater.fr/wiki/txm-users"); //$NON-NLS-1$
		}
		else { // no key associated, key is an URL
			return OpenBrowser.openfile(key);
		}
	}

	public static void main(String args[]) {

	}
}
