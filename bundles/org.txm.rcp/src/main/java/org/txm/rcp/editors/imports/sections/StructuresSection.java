package org.txm.rcp.editors.imports.sections;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.txm.objects.Project;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.txm.rcp.messages.TXMUIMessages;

public class StructuresSection extends ImportEditorSection {

	private static final int SECTION_SIZE = 1;

	private Text milestoneElementsText;

	private Text projectionsText;

	private Button lbnButton;

	public StructuresSection(ImportFormEditor editor, FormToolkit toolkit2, ScrolledForm form2, Composite parent, int style) {
		super(editor, toolkit2, form2, parent, style, "structures"); //$NON-NLS-1$

		this.section.setText(TXMUIMessages.Structures);
		TableWrapLayout layout = new TableWrapLayout();
		layout.makeColumnsEqualWidth = true;
		this.section.setLayout(layout);
		this.section.setLayoutData(getSectionGridData(SECTION_SIZE));
		this.section.setEnabled(false);
		this.section.addExpansionListener(new ExpansionAdapter() {

			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.layout(true);
			}
		});

		Composite sectionClient = toolkit.createComposite(this.section);
		TableWrapLayout slayout = new TableWrapLayout();
		slayout.makeColumnsEqualWidth = false;
		slayout.numColumns = 2;
		sectionClient.setLayout(slayout);
		this.section.setClient(sectionClient);

		if (editor.getImportName().equals("xtz")) {

			Label tmpLabel4 = toolkit.createLabel(sectionClient, TXMUIMessages.milestoneElements);
			tmpLabel4.setToolTipText(TXMUIMessages.listOfMilestoneElementsSeparatedByComma);
			tmpLabel4.setLayoutData(getLabelGridData());

			milestoneElementsText = toolkit.createText(sectionClient, "", SWT.BORDER); //$NON-NLS-1$
			TableWrapData gdata = getTextGridData();
			milestoneElementsText.setLayoutData(gdata);

			lbnButton = toolkit.createButton(sectionClient, "Index lb@n attributes in word properties", SWT.CHECK);
			gdata = getLabelGridData();
			gdata.colspan = 2;
			lbnButton.setLayoutData(gdata);
		}

		Label label = toolkit.createLabel(sectionClient, TXMUIMessages.CQPStructurePropertiesProjection, SWT.WRAP);
		TableWrapData gdata = getLabelGridData();
		gdata.colspan = 2;
		label.setLayoutData(gdata);

		projectionsText = toolkit.createText(sectionClient, "", SWT.BORDER | SWT.MULTI | SWT.V_SCROLL); //$NON-NLS-1$
		gdata = getTextGridData();
		gdata.heightHint = 50;
		gdata.colspan = 2;

		projectionsText.setLayoutData(gdata);
	}

	@Override
	public void updateFields(Project project) {
		if (this.section != null && !section.isDisposed()) {
			if (milestoneElementsText != null) milestoneElementsText.setText(project.getTextualPlan("MileStones")); //$NON-NLS-1$
			if (lbnButton != null) lbnButton.setSelection("true".equals(project.getTextualPlan("lbn"))); //$NON-NLS-1$
			if (projectionsText != null) projectionsText.setText(project.getTextualPlan("Projections")); //$NON-NLS-1$
		}
	}

	@Override
	public boolean saveFields(Project project) {
		if (this.section != null && !section.isDisposed()) {

			if (milestoneElementsText != null) project.setTextualPlan("MileStones", milestoneElementsText.getText().trim()); //$NON-NLS-1$
			if (lbnButton != null) project.setTextualPlan("lbn", "" + lbnButton.getSelection()); //$NON-NLS-1$
			if (projectionsText != null) project.setTextualPlan("Projections", projectionsText.getText().trim()); //$NON-NLS-1$

			return true;
		}

		return true;
	}

	@Override
	public boolean checkFields() {
		return true;
	}

	@Override
	public int getSectionSize() {
		return SECTION_SIZE;
	}
}
