// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp;

import java.util.HashMap;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.FrameworkUtil;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.utils.logger.Log;

/**
 * Stores image keys and also Image objects.
 * 
 * @author mdecorde
 * @author sjacquot
 */
// FIXME: this class creates some image but never free them.
public abstract class IImageKeys {

	/** The images. */
	private final static HashMap<String, Image> images = new HashMap<>();

	/** The Constant ACTION_CONCORDANCES. */
	public static final String ACTION_CONCORDANCES = "icons/concordance.png"; //$NON-NLS-1$

	public static final String CONCORDANCE_DELETE = "icons/functions/Concordances_delete_line.png"; //$NON-NLS-1$

	/** The Constant ACTION_COOCCURRENCE. */
	public static final String ACTION_COOCCURRENCE = "icons/functions/Cooccurrences.png"; //$NON-NLS-1$

	/** The Constant ACTION_CORRESPONDANCE_ANALYSIS. */
	public static final String ACTION_CORRESPONDANCE_ANALYSIS = "icons/functions/CorrespondanceAnalysis.png"; //$NON-NLS-1$

	/** The Constant ACTION_PARTITION. */
	public static final String ACTION_PARTITION = "icons/functions/Partition.png"; //$NON-NLS-1$

	/** The Constant ACTION_SPECIFICITIES. */
	public static final String ACTION_SPECIFICITIES = "icons/functions/Specificities.png"; //$NON-NLS-1$

	/** The Constant ACTION_SUBCORPUS. */
	public static final String ACTION_SUBCORPUS = "icons/functions/SubCorpus.png"; //$NON-NLS-1$

	/** The Constant ACTION_RUN_SCRIPT. */
	public static final String ACTION_RUN_SCRIPT = "icons/functions/RunScript.png"; //$NON-NLS-1$

	/** The Constant ACTION_FREQUENCY_LIST. */
	public static final String ACTION_FREQUENCY_LIST = "icons/functions/lexicon.png"; //$NON-NLS-1$

	/** The Constant ACTION_INDEX. */
	public static final String ACTION_INDEX = "icons/functions/Index.png"; //$NON-NLS-1$

	/** The Constant ACTION_PROGRESSION. */
	public static final String ACTION_PROGRESSION = "icons/functions/Progression.png"; //$NON-NLS-1$

	/** The Constant ACTION_ASSISTQUERY. */
	public static final String ACTION_ASSISTQUERY = "icons/functions/Queryassist.png"; //$NON-NLS-1$

	/** The Constant ACTION_DELETE. */
	public static final String ACTION_DELETE = "icons/functions/Delete.png"; //$NON-NLS-1$

	/** The Constant ACTION_SEARCH. */
	public static final String ACTION_SEARCH = "icons/functions/Search.png"; //$NON-NLS-1$

	/** The Constant ACTION_SEARCH. */
	public static final String ACTION_SEARCH_TABLE = "icons/functions/SearchTable.png"; //$NON-NLS-1$
	
	/** The Constant ACTION_SEARCH. */
	public static final String ACTION_COPY = "platform:/plugin/org.eclipse.ui/icons/full/etool16/copy_edit.png"; //$NON-NLS-1$

	/** The Constant ACTION_SEARCH. */
	public static final String ACTION_SEARCH_SETTINGS = "icons/functions/Search_settings.png"; //$NON-NLS-1$

	
	// FIXME: SJ: it seems there are a lot of obsolete keys in this class since the split into plug-ins of TXM 0.8.0.
	// Need to check and clean it.
	/** The Constant ACTION_CAH. */
//	public static final String ACTION_CAH = "icons/functions/CAH.png"; //$NON-NLS-1$

	/** The Constant ACTION_INFO. */
	public static final String ACTION_INFO = "icons/functions/diagnostique.png"; //$NON-NLS-1$

	/** The Constant TEXT. */
	public static final String INFOS = "icons/infos.png"; //$NON-NLS-1$
	
	/** The Constant TEXT. */
	public static final String REFRESH = "icons/refresh.png"; //$NON-NLS-1$

	/** The Constant TEXT. */
	public static final String TEXT = "icons/objects/book.png"; //$NON-NLS-1$

	/** The Constant BASE. */
	public static final String BASE = "icons/objects/books.png"; //$NON-NLS-1$

	/** The Constant QUERY. */
	public static final String QUERY = "icons/objects/query.png"; //$NON-NLS-1$

	/** The Constant CORPUS. */
	public static final String CORPUS = "icons/objects/corpus.png"; //$NON-NLS-1$

	/** The Constant of CORPUS_BROKEN. */
	public static final String CORPUS_BROKEN = "icons/objects/corpus_broken.png"; //$NON-NLS-1$

	public static final String EDITION = "icons/objects/edition.png"; //$NON-NLS-1$

	public static final String FILE = "icons/objects/file.png"; //$NON-NLS-1$

	public static final String FOLDER = "icons/objects/folder.png"; //$NON-NLS-1$

	public static final String SAVE = "icons/functions/save.png"; //$NON-NLS-1$

	public static final String START =   "icons/functions/run.png"; //$NON-NLS-1$
	
	public static final String START_T = "icons/functions/runT.png"; //$NON-NLS-1$

	/** The Constant PARTITION. */
	public static final String PARTITION = "icons/objects/partition.png"; //$NON-NLS-1$

	/** The Constant LEXICALTABLE. */
	public static final String LEXICALTABLE = "icons/objects/lexicaltable.png"; //$NON-NLS-1$

	/** The Constant PROJECT. */
	public static final String PROJECT = "icons/objects/project.png"; //$NON-NLS-1$

	/** The Constant SCRIPT. */
	public static final String SCRIPT = "icons/objects/script.png"; //$NON-NLS-1$

	/** The Constant SCRIPT. */
	public static final String SCRIPT_RUN = "icons/functions/page_go.png"; //$NON-NLS-1$

	/** The Constant SUBCORPUS. */
	public static final String SUBCORPUS = "icons/objects/subcorpus.png"; //$NON-NLS-1$

	/** The Constant ACTION_TEXT. */
	public static final String ACTION_TEXT = "icons/objects/text.png"; //$NON-NLS-1$

	public static final String GRAPH_SPECIFICITIES = "icons/functions/SpecificitiesGraph.png"; //$NON-NLS-1$

	public static final String CHECKEDBOX_SELECTED = "icons/checkboxselected.gif"; //$NON-NLS-1$

	public static final String CHECKEDBOX_CLEARED = "icons/checkboxcleared.gif"; //$NON-NLS-1$

	public static final String CHECKEDBOX_UNSELECTED = "icons/checkboxunselected.gif"; //$NON-NLS-1$

	public static final String CHECKEDBOX_EXPAND = "icons/expand.gif"; //$NON-NLS-1$

	public static final String CHECKEDBOX_GREYED = "icons/checkboxgreyed.gif"; //$NON-NLS-1$

	/** The Constant CHECKED. */
	public static final String CHECKED = "icons/tick.png"; //$NON-NLS-1$

	public static final String CHECKEDALL = "icons/tick_all.png"; //$NON-NLS-1$

	/** The Constant UNCHECKED. */
	public static final String UNCHECKED = "icons/cross.png"; //$NON-NLS-1$

	/** The Constant UNCHECKED. */
	public static final String UNCHECKEDALL = "icons/cross_all.png"; //$NON-NLS-1$

	public static final String REVERSECHECKED = "icons/cross_invert.png"; //$NON-NLS-1$

	public static final String ACTION_INTERTEXTDIST = "icons/functions/dist.png"; //$NON-NLS-1$
	
	public static final String ACTION_HISTO = "icons/functions/histo.png"; //$NON-NLS-1$

	public static final String QUERYINDEX = "icons/functions/QueryIndex.png"; //$NON-NLS-1$

	public static final String COOCMATRIX = "icons/functions/graph.png"; //$NON-NLS-1$

	public static final String ACTION_WORDCLOUD = "icons/functions/WordCloud.png"; //$NON-NLS-1$

	public static final String CTRLSTOP = "icons/control_stop.png"; //$NON-NLS-1$

	public static final String CTRLSTART = "icons/control_start_blue.png"; //$NON-NLS-1$

	public static final String CTRLREVERSE = "icons/control_reverse_blue.png"; //$NON-NLS-1$

	public static final String CTRLPLAY = "icons/control_play_blue.png"; //$NON-NLS-1$

	public static final String CTRLEND = "icons/control_end_blue.png"; //$NON-NLS-1$

	public static final String CTRLFASTFORWARD = "icons/control_fastforward_blue.png"; //$NON-NLS-1$

	public static final String CTRLFASTFORWARDEND = "icons/control_fastforwardend_blue.png"; //$NON-NLS-1$


	public static final String CTRLREWINDSTART = "icons/control_rewindstart_blue.png"; //$NON-NLS-1$

	public static final String CTRLREWIND = "icons/control_rewind_blue.png"; //$NON-NLS-1$

	/**
	 * Chart editor export view command icon.
	 */
	public static final String CHARTS_EXPORT_VIEW = "icons/functions/charts_export_view.png"; //$NON-NLS-1$

	/**
	 * Chart editor reset view command icon.
	 */
	public static final String CHARTS_RESET_VIEW = "icons/functions/charts_reset_view.png"; //$NON-NLS-1$

	/**
	 * CA Chart editor show/hide columns command icon.
	 */
	public static final String CHARTS_CA_SHOW_COLUMNS = "icons/functions/charts_CA_show_colums.png"; //$NON-NLS-1$

	/**
	 * CA Chart editor show/hide rows command icon.
	 */
	public static final String CHARTS_CA_SHOW_ROWS = "icons/functions/charts_CA_show_rows.png"; //$NON-NLS-1$

	/**
	 * CAH Chart editor compute columns command icon.
	 */
	public static final String CHARTS_CAH_COMPUTE_COLUMNS = "icons/functions/charts_CAH_compute_columns.png"; //$NON-NLS-1$

	/**
	 * CAH Chart editor compute rows command icon.
	 */
	public static final String CHARTS_CAH_COMPUTE_ROWS = "icons/functions/charts_CAH_compute_rows.png"; //$NON-NLS-1$

	/**
	 * Global export chart command icon.
	 */
	public static final String CHARTS_EXPORT_CHART = "icons/functions/export_chart.png"; //$NON-NLS-1$

	/**
	 * Global export result data command icon.
	 */
	public static final String CHARTS_EXPORT_DATA = "icons/functions/export_data.png"; //$NON-NLS-1$

	public static final String PENCIL = "icons/functions/pencil.png"; //$NON-NLS-1$

	public static final String PENCIL_ADD = "icons/functions/pencil_add.png"; //$NON-NLS-1$

	public static final String PENCIL_SAVE = "icons/functions/pencil_save.png"; //$NON-NLS-1$

	public static final String PENCIL_OPEN = "icons/functions/pencil_open.png"; //$NON-NLS-1$

	public static final String PENCIL_CLOSE = "icons/functions/pencil_close.png"; //$NON-NLS-1$

	public static final String ACTION_ADD = "icons/add.png";//$NON-NLS-1$

	public static final String ACTION_REMOVE = "icons/remove.png";//$NON-NLS-1$

	public static final String WORLD = "icons/world.png"; //$NON-NLS-1$

	public static final String TXM_ICON = "icons/logo/TXM_logo.png"; //$NON-NLS-1$

	public static final String TXM = "icons/logo/TXM_logo_64x64.png"; //$NON-NLS-1$

	public static final String TXM1616 = "icons/logo/TXM_logo_16x16.png"; //$NON-NLS-1$

	public static final String SPLASH = "splash.bmp"; //$NON-NLS-1$

	public static final String OPEN_DECORATOR = "icons/decorators/bullet_arrow_down.png"; //$NON-NLS-1$

	public static final String CLOSE_DECORATOR = "icons/decorators/bullet_arrow_up.png"; //$NON-NLS-1$

	public static final String TXM_FLAT_LOGO = "icons/logo/TXM_flat_logo.png"; //$NON-NLS-1$

	public static final String FILE_IMAGE = "icons/objects/file_image.png"; //$NON-NLS-1$ ;

	public static final String FILE_AUDIO = "icons/objects/file_audio.png"; //$NON-NLS-1$ ;

	public static final String FILE_VIDEO = "icons/objects/file_video.png"; //$NON-NLS-1$ ;

	public static final String FILE_DOC = "icons/objects/file_doc.png"; //$NON-NLS-1$ ;

	public static final String HOME = "icons/home.png"; //$NON-NLS-1$ ;

	public static final String WORKSITE = "icons/worksite.png"; //$NON-NLS-1$

	public static final String LABORATORY = "icons/laboratory.png";

	/**
	 * Gets the image.
	 *
	 * @param imagekey the imagekey
	 * @return the image
	 */
	public static Image getImage(String imagekey) {
		return getImage(Application.PLUGIN_ID, imagekey);
	}

	/**
	 * 
	 * @param imageFilePath
	 * @return
	 */
	public static ImageDescriptor getImageDescriptor(String imageFilePath) {
		return AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, imageFilePath);
	}

	/**
	 * 
	 * @param plugin ID
	 * @param imageFilePath
	 * @return
	 */
	public static ImageDescriptor getImageDescriptor(String plugin, String imageFilePath) {
		return AbstractUIPlugin.imageDescriptorFromPlugin(plugin, imageFilePath);
	}

	/**
	 * 
	 * @param classOfPlugin of plugin to find
	 * @param imageFilePath
	 * @return
	 */
	public static ImageDescriptor getImageDescriptor(Class<?> classOfPlugin, String imageFilePath) {
		return getImageDescriptor(FrameworkUtil.getBundle(classOfPlugin).getSymbolicName(), imageFilePath);
	}

	/**
	 * @param classOfPlugin
	 * @param imageFilePath
	 * @return
	 */
	public static Image getImage(Class<?> classOfPlugin, String imageFilePath) {
		return getImage(FrameworkUtil.getBundle(classOfPlugin).getSymbolicName(), imageFilePath);
	}

	/**
	 * 
	 * @param pluginId
	 * @param imagekey
	 * @return
	 */
	public static Image getImage(String pluginId, String imagekey) {
		if (!images.containsKey(imagekey)) {
			ImageDescriptor imgg = AbstractUIPlugin.imageDescriptorFromPlugin(pluginId, imagekey);
			if (imgg == null) {
				Log.severe(NLS.bind(TXMUIMessages.warningImageNotFoundAtP0, pluginId + " -> " + imagekey)); //$NON-NLS-2$ //$NON-NLS-3$
				return getImage("icons/null.png"); //$NON-NLS-1$
			}

			images.put(imagekey, new Image(null, imgg.getImageData()));
		}
		return images.get(imagekey);
	}
	
	/**
	 * dispose
	 */
	public static void dispose() {

		try {
			for (Image f : images.values()) {
				if (!f.isDisposed()) {
					f.dispose();
				}
			}
		}
		catch (Exception e) {

		}
	}
}
