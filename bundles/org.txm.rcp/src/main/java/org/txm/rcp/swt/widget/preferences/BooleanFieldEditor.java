package org.txm.rcp.swt.widget.preferences;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

/**
 * Add access to the internal Button
 */
public class BooleanFieldEditor extends org.eclipse.jface.preference.BooleanFieldEditor {
	
	Composite parent;
	
	public BooleanFieldEditor(String preference, String label, Composite parent) {
		super(preference, label, parent);
		this.parent = parent;
	}

	public BooleanFieldEditor(String preference, String label, int style, Composite parent) {
		super(preference, label, style, parent);
		this.parent = parent;
	}
	
	public Button getCheckButton() {
		
		return getChangeControl(parent);
	}
	
	public void setToolTipText(String tooltip) {
		
		getChangeControl(parent).setToolTipText(tooltip);
	}
	
	public void setEnabled(boolean b) {
		
		super.setEnabled(b, parent);
	}
}
