package org.txm.rcp.editors.imports;

import java.util.HashMap;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IContributor;
import org.eclipse.core.runtime.RegistryFactory;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.editors.imports.sections.ImportEditorSection;
import org.txm.rcp.editors.imports.sections.TRSSection;
import org.txm.utils.logger.Log;

/**
 * static class that defines what parameters are used by import modules
 * 
 * @author mdecorde
 *
 */
public class ImportModuleCustomization {

	public static final String PARALLEL = "parallel"; //$NON-NLS-1$

	public static final String ENCODING = "encoding"; //$NON-NLS-1$

	public static final String LANG = "lang"; //$NON-NLS-1$

	public static final String XSLT = "xslt"; //$NON-NLS-1$

	public static final String WORDS = "words"; //$NON-NLS-1$

	public static final String ADVANCEDTOKENIZER = "tokenizer-advanced"; //$NON-NLS-1$

	public static final String EDITIONS_WORDSPERPAGE = "editions-wordsperpage"; //$NON-NLS-1$

	public static final String EDITIONS_PAGEELEMENT = "editions-pageelement"; //$NON-NLS-1$

	public static final String PATTRIBUTES = "pAttributes"; //$NON-NLS-1$

	public static final String SATTRIBUTES = "sAttributes"; //$NON-NLS-1$

	public static final String PREBUILD = "preBuild"; //$NON-NLS-1$

	public static final String QUERIES = "queries"; //$NON-NLS-1$

	public static final String UI = "uis"; //$NON-NLS-1$

	public static final String OPTIONS = "options"; //$NON-NLS-1$

	public static final String TEXTUALPLANS = "textualplans"; //$NON-NLS-1$

	public static final String STRUCTURES = "structures"; //$NON-NLS-1$

	public static final String IMPORT_NAME = "import_name"; //$NON-NLS-1$

	protected static HashMap<String, HashMap<String, Boolean>> sectionsPerImportModule = new HashMap<>();

	protected static HashMap<String, Class<? extends ImportEditorSection>> additionalSections = new HashMap<>();

	protected static HashMap<String, String> names = new HashMap<>();

	static { // init import module
		HashMap<String, Boolean> params = new HashMap<>();
		params.put(PARALLEL, false);
		params.put(ENCODING, true);
		params.put(LANG, false);
		params.put(XSLT, false);
		params.put(WORDS, false);
		params.put(ADVANCEDTOKENIZER, false);
		params.put(EDITIONS_WORDSPERPAGE, true);
		params.put(EDITIONS_PAGEELEMENT, false);
		params.put(PATTRIBUTES, true);
		params.put(SATTRIBUTES, false);
		params.put(PREBUILD, true);
		params.put(QUERIES, true);
		params.put(UI, true);
		params.put(TEXTUALPLANS, false);
		params.put(STRUCTURES, false);
		params.put(OPTIONS, true);
		sectionsPerImportModule.put("discoursLoader.groovy", params); //$NON-NLS-1$
		names.put("discoursLoader.groovy", "CNR + CSV"); //$NON-NLS-1$ //$NON-NLS-2$


		params = new HashMap<>();
		params.put(PARALLEL, false);
		params.put(ENCODING, true);
		params.put(LANG, true);
		params.put(XSLT, false);
		params.put(WORDS, true);
		params.put(ADVANCEDTOKENIZER, false);
		params.put(EDITIONS_WORDSPERPAGE, true);
		params.put(EDITIONS_PAGEELEMENT, true);
		params.put(PATTRIBUTES, true);
		params.put(SATTRIBUTES, true);
		params.put(PREBUILD, true);
		params.put(QUERIES, true);
		params.put(UI, true);
		params.put(TEXTUALPLANS, false);
		params.put(STRUCTURES, false);
		params.put(OPTIONS, true);
		sectionsPerImportModule.put("cqpLoader.groovy", params); //$NON-NLS-1$
		names.put("cqpLoader.groovy", "CQP"); //$NON-NLS-1$ //$NON-NLS-2$

		params = new HashMap<>();
		params.put(PARALLEL, false);
		params.put(ENCODING, true);
		params.put(LANG, true);
		params.put(XSLT, false);
		params.put(WORDS, false);
		params.put(ADVANCEDTOKENIZER, false);
		params.put(EDITIONS_WORDSPERPAGE, true);
		params.put(EDITIONS_PAGEELEMENT, false);
		params.put(PATTRIBUTES, true);
		params.put(SATTRIBUTES, false);
		params.put(PREBUILD, true);
		params.put(QUERIES, true);
		params.put(UI, true);
		params.put(TEXTUALPLANS, false);
		params.put(STRUCTURES, false);
		params.put(OPTIONS, true);
		sectionsPerImportModule.put("hyperbaseLoader.groovy", params); //$NON-NLS-1$
		names.put("hyperbaseLoader.groovy", "Hyperbase"); //$NON-NLS-1$ //$NON-NLS-2$

		params = new HashMap<>();
		params.put(PARALLEL, false);
		params.put(ENCODING, true);
		params.put(LANG, true);
		params.put(XSLT, false);
		params.put(WORDS, true);
		params.put(ADVANCEDTOKENIZER, false);
		params.put(EDITIONS_WORDSPERPAGE, true);
		params.put(EDITIONS_PAGEELEMENT, false);
		params.put(PATTRIBUTES, true);
		params.put(SATTRIBUTES, false);
		params.put(PREBUILD, true);
		params.put(QUERIES, true);
		params.put(UI, true);
		params.put(TEXTUALPLANS, false);
		params.put(STRUCTURES, false);
		params.put(OPTIONS, true);
		sectionsPerImportModule.put("alcesteLoader.groovy", params); //$NON-NLS-1$
		names.put("alcesteLoader.groovy", "Alceste"); //$NON-NLS-1$ //$NON-NLS-2$

		params = new HashMap<>();
		params.put(PARALLEL, false);
		params.put(ENCODING, true);
		params.put(LANG, true);
		params.put(XSLT, false);
		params.put(WORDS, true);
		params.put(ADVANCEDTOKENIZER, false);
		params.put(EDITIONS_WORDSPERPAGE, true);
		params.put(EDITIONS_PAGEELEMENT, false);
		params.put(PATTRIBUTES, true);
		params.put(SATTRIBUTES, false);
		params.put(PREBUILD, true);
		params.put(QUERIES, true);
		params.put(UI, true);
		params.put(TEXTUALPLANS, false);
		params.put(STRUCTURES, false);
		params.put(OPTIONS, true);
		sectionsPerImportModule.put("txtLoader.groovy", params); //$NON-NLS-1$
		names.put("txtLoader.groovy", "TXT + CSV"); //$NON-NLS-1$ //$NON-NLS-2$

		params = new HashMap<>();
		params.put(PARALLEL, false);
		params.put(ENCODING, false);
		params.put(LANG, true);
		params.put(XSLT, true);
		params.put(WORDS, true);
		params.put(ADVANCEDTOKENIZER, true);
		params.put(EDITIONS_WORDSPERPAGE, true);
		params.put(EDITIONS_PAGEELEMENT, true);
		params.put(PATTRIBUTES, true);
		params.put(SATTRIBUTES, true);
		params.put(PREBUILD, true);
		params.put(QUERIES, true);
		params.put(UI, true);
		params.put(TEXTUALPLANS, true);
		params.put(STRUCTURES, true);
		params.put(OPTIONS, true);
		sectionsPerImportModule.put("transcriberLoader.groovy", params); //$NON-NLS-1$
		names.put("transcriberLoader.groovy", "XML-TRS + CSV"); //$NON-NLS-1$ //$NON-NLS-2$

		params = new HashMap<>();
		params.put(PARALLEL, false);
		params.put(ENCODING, false);
		params.put(LANG, true);
		params.put(XSLT, true);
		params.put(WORDS, true);
		params.put(ADVANCEDTOKENIZER, false);
		params.put(EDITIONS_WORDSPERPAGE, true);
		params.put(EDITIONS_PAGEELEMENT, true);
		params.put(PATTRIBUTES, true);
		params.put(SATTRIBUTES, true);
		params.put(PREBUILD, true);
		params.put(QUERIES, true);
		params.put(UI, true);
		params.put(TEXTUALPLANS, false);
		params.put(STRUCTURES, false);
		params.put(OPTIONS, true);
		sectionsPerImportModule.put("tmxLoader.groovy", params); //$NON-NLS-1$
		names.put("tmxLoader.groovy", "XML-TMX"); //$NON-NLS-1$ //$NON-NLS-2$

		params = new HashMap<>();
		params.put(PARALLEL, false);
		params.put(ENCODING, false);
		params.put(LANG, true);
		params.put(XSLT, true);
		params.put(WORDS, true);
		params.put(ADVANCEDTOKENIZER, true);
		params.put(EDITIONS_WORDSPERPAGE, true);
		params.put(EDITIONS_PAGEELEMENT, true);
		params.put(PATTRIBUTES, true);
		params.put(SATTRIBUTES, true);
		params.put(PREBUILD, true);
		params.put(QUERIES, true);
		params.put(UI, true);
		params.put(TEXTUALPLANS, false);
		params.put(STRUCTURES, false);
		params.put(OPTIONS, true);
		sectionsPerImportModule.put("xmlLoader.groovy", params); //$NON-NLS-1$
		names.put("xmlLoader.groovy", "XML/w + CSV"); //$NON-NLS-1$ //$NON-NLS-2$

		params = new HashMap<>();
		params.put(PARALLEL, false);
		params.put(ENCODING, false);
		params.put(LANG, true);
		params.put(XSLT, true);
		params.put(WORDS, true);
		params.put(ADVANCEDTOKENIZER, false);
		params.put(EDITIONS_WORDSPERPAGE, true);
		params.put(EDITIONS_PAGEELEMENT, true);
		params.put(PATTRIBUTES, true);
		params.put(SATTRIBUTES, true);
		params.put(PREBUILD, true);
		params.put(QUERIES, true);
		params.put(UI, true);
		params.put(TEXTUALPLANS, false);
		params.put(STRUCTURES, false);
		params.put(OPTIONS, true);
		sectionsPerImportModule.put("xmltxmLoader.groovy", params); //$NON-NLS-1$
		names.put("xmltxmLoader.groovy", "XML-TEI TXM"); //$NON-NLS-1$ //$NON-NLS-2$

		params = new HashMap<>();
		params.put(PARALLEL, false);
		params.put(ENCODING, false);
		params.put(LANG, true);
		params.put(XSLT, true);
		params.put(WORDS, true);
		params.put(ADVANCEDTOKENIZER, false);
		params.put(EDITIONS_WORDSPERPAGE, true);
		params.put(EDITIONS_PAGEELEMENT, true);
		params.put(PATTRIBUTES, true);
		params.put(SATTRIBUTES, true);
		params.put(PREBUILD, true);
		params.put(QUERIES, true);
		params.put(UI, true);
		params.put(TEXTUALPLANS, false);
		params.put(STRUCTURES, false);
		params.put(OPTIONS, true);
		sectionsPerImportModule.put("factivaLoader.groovy", params); //$NON-NLS-1$
		names.put("factivaLoader.groovy", "XML-PPS"); //$NON-NLS-1$ //$NON-NLS-2$

		params = new HashMap<>();
		params.put(PARALLEL, false);
		params.put(ENCODING, false);
		params.put(LANG, true);
		params.put(XSLT, false);
		params.put(WORDS, true);
		params.put(ADVANCEDTOKENIZER, false);
		params.put(EDITIONS_WORDSPERPAGE, true);
		params.put(EDITIONS_PAGEELEMENT, false);
		params.put(PATTRIBUTES, true);
		params.put(SATTRIBUTES, true);
		params.put(PREBUILD, true);
		params.put(QUERIES, true);
		params.put(UI, true);
		params.put(TEXTUALPLANS, false);
		params.put(STRUCTURES, false);
		params.put(OPTIONS, true);
		sectionsPerImportModule.put("factivamailLoader.groovy", params); //$NON-NLS-1$
		names.put("factivamailLoader.groovy", "Factiva TXT"); //$NON-NLS-1$ //$NON-NLS-2$

		params = new HashMap<>();
		params.put(PARALLEL, false);
		params.put(ENCODING, false);
		params.put(LANG, true);
		params.put(XSLT, true);
		params.put(WORDS, true);
		params.put(ADVANCEDTOKENIZER, false);
		params.put(EDITIONS_WORDSPERPAGE, true);
		params.put(EDITIONS_PAGEELEMENT, false);
		params.put(PATTRIBUTES, true);
		params.put(SATTRIBUTES, true);
		params.put(PREBUILD, true);
		params.put(QUERIES, true);
		params.put(UI, true);
		params.put(TEXTUALPLANS, false);
		params.put(STRUCTURES, false);
		params.put(OPTIONS, true);
		sectionsPerImportModule.put("bfmLoader.groovy", params); //$NON-NLS-1$
		names.put("bfmLoader.groovy", "XML-TEI BFM"); //$NON-NLS-1$ //$NON-NLS-2$

		params = new HashMap<>();
		params.put(PARALLEL, false);
		params.put(ENCODING, false);
		params.put(LANG, true);
		params.put(XSLT, true);
		params.put(WORDS, true);
		params.put(ADVANCEDTOKENIZER, false);
		params.put(EDITIONS_WORDSPERPAGE, true);
		params.put(EDITIONS_PAGEELEMENT, false);
		params.put(PATTRIBUTES, true);
		params.put(SATTRIBUTES, true);
		params.put(PREBUILD, true);
		params.put(QUERIES, true);
		params.put(UI, true);
		params.put(TEXTUALPLANS, false);
		params.put(STRUCTURES, false);
		params.put(OPTIONS, true);
		sectionsPerImportModule.put("frantextLoader.groovy", params); //$NON-NLS-1$
		names.put("frantextLoader.groovy", "XML-TEI Frantext"); //$NON-NLS-1$ //$NON-NLS-2$

		params = new HashMap<>();
		params.put(PARALLEL, false);
		params.put(ENCODING, false);
		params.put(LANG, true);
		params.put(XSLT, false);
		params.put(WORDS, true);
		params.put(ADVANCEDTOKENIZER, true);
		params.put(EDITIONS_WORDSPERPAGE, true);
		params.put(EDITIONS_PAGEELEMENT, true);
		params.put(PATTRIBUTES, false);
		params.put(SATTRIBUTES, false);
		params.put(PREBUILD, false);
		params.put(QUERIES, false);
		params.put(UI, true);
		params.put(TEXTUALPLANS, true);
		params.put(STRUCTURES, true);
		params.put(OPTIONS, true);
		sectionsPerImportModule.put("xtzLoader.groovy", params); //$NON-NLS-1$
		names.put("xtzLoader.groovy", "XML-TEI Zero + CSV"); //$NON-NLS-1$ //$NON-NLS-2$

		IConfigurationElement[] contributions = RegistryFactory.getRegistry().getConfigurationElementsFor("org.txm.rcp.importsection"); //$NON-NLS-1$

		for (int i = 0; i < contributions.length; i++) {
			try {
				IContributor c = contributions[i].getContributor();
				@SuppressWarnings("unchecked")
				ImportEditorSectionConfigurator iesc = (ImportEditorSectionConfigurator) contributions[i].createExecutableExtension("class"); //$NON-NLS-1$

				Log.fine(TXMCoreMessages.bind("Initializing import section with: {0}...", iesc)); //$NON-NLS-1$

				iesc.installSections();
			}
			catch (Exception e) {
				Log.severe("Error: failed to instantiate " + contributions[i].getName() + "."); //$NON-NLS-1$ //$NON-NLS-2$
				e.printStackTrace();
			}
		}
		additionalSections.put("transcriberLoader.groovy", TRSSection.class); //$NON-NLS-1$
	}

	/**
	 * 
	 * @param importscript
	 * @return the import parameter section configuration depending on the <code>importscript</code>. If their is no setting for <code>importscript</code> return a default configuration
	 */
	public static HashMap<String, Boolean> getParameters(String importscript) {
		if (sectionsPerImportModule.containsKey(importscript)) {
			return sectionsPerImportModule.get(importscript);
		}
		else if (sectionsPerImportModule.containsKey(importscript + "Loader.groovy")) { //$NON-NLS-1$
			return sectionsPerImportModule.get(importscript + "Loader.groovy"); //$NON-NLS-1$
		}
		else {
			HashMap<String, Boolean> params = new HashMap<>();
			params.put(PARALLEL, true);
			params.put(ENCODING, true);
			params.put(LANG, true);
			params.put(XSLT, true);
			params.put(WORDS, true);
			params.put(ADVANCEDTOKENIZER, false);
			params.put(EDITIONS_WORDSPERPAGE, true);
			params.put(EDITIONS_PAGEELEMENT, true);
			params.put(PATTRIBUTES, true);
			params.put(SATTRIBUTES, true);
			params.put(PREBUILD, true);
			params.put(QUERIES, true);
			params.put(UI, true);
			params.put(TEXTUALPLANS, false);
			params.put(STRUCTURES, false);
			params.put(OPTIONS, true);
			return params;
		}
	}

	public static void addNewImportModuleConfiguration(String script, String name, HashMap<String, Boolean> params) {
		sectionsPerImportModule.put(script, params); //$NON-NLS-1$
		names.put(script, name); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public static String getName(String importscript) {
		if (sectionsPerImportModule.containsKey(importscript)) {
			return names.get(importscript);
		}
		if (sectionsPerImportModule.containsKey(importscript + "Loader.groovy")) { //$NON-NLS-1$
			return names.get(importscript + "Loader.groovy"); //$NON-NLS-1$
		}
		else {
			return importscript;
		}
	}

	/**
	 * 
	 * @param importName
	 * @return a ImportEditorSection class if any for a given import module name
	 */
	public static Class<? extends ImportEditorSection> getAdditionalSection(String importName) {
		if (!additionalSections.containsKey(importName)) {
			return additionalSections.get(importName + "Loader.groovy"); //$NON-NLS-1$
		}
		return additionalSections.get(importName);
	}

	public static void addAdditionalSections(String name, Class<? extends ImportEditorSection> clazz) {

		additionalSections.put(name, clazz);
	}
}
