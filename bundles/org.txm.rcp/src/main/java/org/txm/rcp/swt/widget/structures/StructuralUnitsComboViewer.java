package org.txm.rcp.swt.widget.structures;

import java.util.ArrayList;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnit;

/**
 * Combo viewer for structural units supporting single selection only.
 * 
 * @author sjacquot
 *
 */
public class StructuralUnitsComboViewer extends ComboViewer {


	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param autoCompute
	 * @param selectedSU
	 * @param addBlankEntry to add an empty blank entry at start of the list to clear value.
	 */
	public StructuralUnitsComboViewer(Composite parent, TXMEditor<?> editor, boolean autoCompute, StructuralUnit selectedSU, boolean addBlankEntry) {
		super(parent, SWT.READ_ONLY);

		this.setContentProvider(ArrayContentProvider.getInstance());

		// Input
		try {
			ArrayList<StructuralUnit> structures = new ArrayList<StructuralUnit>(CQPCorpus.getFirstParentCorpus(editor.getResult()).getOrderedStructuralUnits());
			for (int i = 0 ; i < structures.size() ; i++) {
				if (structures.get(i).getName().equals("txmcorpus")) {
					structures.remove(i);
					i--;
				}
			}
			this.setInput(structures.toArray());
			// add empty entry
			if (addBlankEntry) {
				this.insert(null, 0);
			}
		}
		catch (CqiClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (selectedSU != null) {
			this.setSelection(new StructuredSelection(selectedSU), true);
		}

		// Listeners
		this.addSelectionChangedListener(new ComputeSelectionListener(editor, autoCompute));
	}


	public StructuralUnitsComboViewer(Composite parent, TXMEditor editor, boolean autoCompute) {
		this(parent, editor, autoCompute, null, false);
	}

	/**
	 * Gets the first select structural unit.
	 * 
	 * @return
	 */
	public StructuralUnit getSelectedStructuralUnit() {
		return ((StructuralUnit) ((IStructuredSelection) this.getSelection()).getFirstElement());
	}

}
