package org.txm.rcp.p2.plugins;

import org.eclipse.equinox.internal.p2.discovery.Catalog;
import org.eclipse.equinox.internal.p2.ui.discovery.wizards.CatalogPage;
import org.eclipse.equinox.internal.p2.ui.discovery.wizards.CatalogViewer;
import org.eclipse.swt.widgets.Composite;

public class TXMCatalogPage extends CatalogPage {

	public TXMCatalogPage(Catalog catalog) {
		super(catalog);
		// TODO Auto-generated constructor stub
	}

	protected CatalogViewer doCreateViewer(Composite parent) {
		CatalogViewer viewer = new TXMCatalogViewer(getCatalog(), this, getContainer(), getWizard().getConfiguration());
		viewer.setMinimumHeight(MINIMUM_HEIGHT);
		viewer.createControl(parent);
		return viewer;
	}
}
