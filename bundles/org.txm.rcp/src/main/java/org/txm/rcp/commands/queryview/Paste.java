// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands.queryview;

import java.util.ArrayList;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.objects.SavedQuery;
import org.txm.rcp.utils.IOClipboard;
import org.txm.rcp.views.QueriesView;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

// TODO: Auto-generated Javadoc
/**
 * The Class Paste.
 */
public class Paste extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		if (selection.getFirstElement() instanceof CQPCorpus) {
			CQPCorpus c = (CQPCorpus) selection.getFirstElement();
			String from = IOClipboard.read();
			paste(c, from);
		}
		return null;
	}

	/**
	 * Paste.
	 *
	 * @param corpus the corpus
	 * @param newquery the newquery
	 */
	public static void paste(CQPCorpus corpus, String newquery) {
		new SavedQuery(corpus).setParameters(newquery, new ArrayList<String>());
		QueriesView.refresh();
	}

	/**
	 * Paste.
	 *
	 * @param corpus the corpus
	 */
	public static void paste(CQPCorpus corpus) {
		String newquery = IOClipboard.read();
		paste(corpus, newquery);
	}
}
