/**
 * 
 */
package org.txm.rcp.swt.widget;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;

/**
 * Convenience class to add a SWT Combo to some editor parameters areas.
 * 
 * If the parent is a GLComposite then the number of columns of its GridLayout will be adjusted to receive the control.
 * If the parent is a TXMEditorToolbar, use the SWT.SEPARATOR trick, plus the packing and the right width setting to add non-ToolItem Control to a tool bar.
 * 
 * This class also adds all the needed computing listeners.
 * 
 * @author sjacquot
 *
 */
public class TXMParameterCombo extends TXMParameterControl {


	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param labelText
	 * @param controlTooltip
	 * @param autoCompute
	 */
	public TXMParameterCombo(Composite parent, TXMEditor<?> editor, Combo combo, String labelText, String[] items, String controlTooltip, boolean autoCompute) {
		super(parent, editor, combo, labelText, controlTooltip, autoCompute);

		if (items != null) this.getControl().setItems(items);

		// add computing listeners
		this.getControl().addModifyListener(this.computingKeyListener);
		this.computingSelectionListener = new ComputeSelectionListener(editor, autoCompute);
		this.getControl().addSelectionListener(this.computingSelectionListener);
	}
	
	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param labelText
	 * @param controlTooltip
	 * @param autoCompute
	 */
	public TXMParameterCombo(Composite parent, TXMEditor<?> editor, int style, String labelText, String[] items, String controlTooltip, boolean autoCompute) {
		super(parent, editor, new Combo(parent, style), labelText, controlTooltip, autoCompute);

		if (items != null) this.getControl().setItems(items);

		// add computing listeners
		this.getControl().addModifyListener(this.computingKeyListener);
		this.computingSelectionListener = new ComputeSelectionListener(editor, autoCompute);
		this.getControl().addSelectionListener(this.computingSelectionListener);
	}

	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param labelText
	 * @param items
	 */
	public TXMParameterCombo(Composite parent, TXMEditor<?> editor, int style, String labelText, String[] items) {
		this(parent, editor, style, labelText, items, null, false);
	}

	/***
	 * 
	 * @param parent
	 * @param editor
	 * @param items
	 * @param controlTooltip
	 * @param autoCompute
	 */
	public TXMParameterCombo(Composite parent, TXMEditor<?> editor, int style, String[] items, String controlTooltip, boolean autoCompute) {
		this(parent, editor, style, null, items, controlTooltip, autoCompute);
	}

	/**
	 * 
	 * @param parent
	 * @param editor
	 * @param items
	 */
	public TXMParameterCombo(Composite parent, TXMEditor<?> editor, int style, String[] items) {
		this(parent, editor, style, items, null, false);
	}


	@Override
	public Combo getControl() {
		return (Combo) super.getControl();
	}

	public void setText(String value) {
		getControl().setText(value);
	}

}
