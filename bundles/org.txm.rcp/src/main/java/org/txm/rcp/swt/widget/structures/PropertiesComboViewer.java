/**
 *
 */
package org.txm.rcp.swt.widget.structures;

import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.searchengine.cqp.corpus.Property;

/**
 * Combo viewer for selecting token/word properties of a corpus supporting single selection only.
 *
 * @author sjacquot
 *
 */
public class PropertiesComboViewer extends ComboViewer {

	/**
	 * To add an empty blank entry at start of the list to clear property.
	 */
	protected boolean addBlankEntry;

	/**
	 *
	 * @param parent
	 * @param style
	 * @param editor
	 * @param autoCompute
	 * @param properties
	 * @param selectedProperty
	 * @param addBlankEntry to add an empty blank entry at start of the list to clear property.
	 * @param listener
	 */
	public PropertiesComboViewer(Composite parent, int style, TXMEditor<? extends TXMResult> editor, boolean autoCompute, List<? extends Property> properties, Property selectedProperty,
			boolean addBlankEntry, ComputeSelectionListener listener) {
		super(parent, style);

		this.addBlankEntry = addBlankEntry;

		this.setContentProvider(ArrayContentProvider.getInstance());

		this.updateFromList(properties, selectedProperty);

		// Listener
		if (listener != null) {
			this.addSelectionChangedListener(listener);
		}
	}

	/**
	 *
	 * @param parent
	 * @param style
	 * @param editor
	 * @param autoCompute
	 * @param properties
	 * @param selectedProperty
	 * @param addBlankEntry
	 */
	public PropertiesComboViewer(Composite parent, int style, TXMEditor<? extends TXMResult> editor, boolean autoCompute, List<? extends Property> properties, Property selectedProperty,
			boolean addBlankEntry) {
		this(parent, style, editor, autoCompute, properties, selectedProperty, addBlankEntry, new ComputeSelectionListener(editor, autoCompute));
	}

	/**
	 *
	 * @param parent
	 * @param editor
	 * @param autoCompute
	 * @param properties
	 * @param selectedProperty
	 * @param addBlankEntry
	 */
	public PropertiesComboViewer(Composite parent, TXMEditor<? extends TXMResult> editor, boolean autoCompute, List<? extends Property> properties, Property selectedProperty, boolean addBlankEntry) {
		this(parent, SWT.READ_ONLY, editor, autoCompute, properties, selectedProperty, addBlankEntry);
	}

	/**
	 * Updates the input and the selected property.
	 *
	 * @param properties
	 * @param selectedProperty
	 */
	public void updateFromList(List<? extends Property> properties, Property selectedProperty) {
		if (properties != null) {
			// Input
			this.setInput(properties.toArray());
			// add empty entry
			if (this.addBlankEntry) {
				this.insert(null, 0);
			}
		}
		if (selectedProperty != null) {
			this.setSelection(new StructuredSelection(selectedProperty), true);
		}
	}
}
