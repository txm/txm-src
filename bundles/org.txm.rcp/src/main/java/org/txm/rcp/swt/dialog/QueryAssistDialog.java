// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.dialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.fieldassist.ComboContentAdapter;
import org.eclipse.jface.fieldassist.TXMAutoCompleteField;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.TXMFontRegistry;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.WordProperty;

// TODO: Auto-generated Javadoc
/**
 * build a query with the help of simple questions @ author mdecorde.
 */
public class QueryAssistDialog extends Dialog {

	/** The self. */
	QueryAssistDialog self;

	/** The parent. */
	Composite parent;

	/** The main panel. */
	Composite mainPanel;

	/** The parent shell. */
	Shell parentShell;

	/** The query. */
	String query;

	/** The nbwords. */
	int nbwords = 0;

	/** The wordfields. */
	List<WordField> wordfields = new ArrayList<>();

	/** The linkfields. */
	List<LinkField> linkfields = new ArrayList<>();

	/** The properties. */
	List<WordProperty> properties;

	/** The cherche label. */
	Label chercheLabel;

	/** The wordcontainer. */
	Composite wordcontainer;

	private CQPCorpus corpus;

	private Combo structsCombo;

	private Button withinButton = null;

	private ScrolledComposite scrollComposite;

	private Spinner withinN;

	/**
	 * Instantiates a new query assist dialog.
	 *
	 * @param parentShell the parent shell
	 * @param corpus the corpus
	 */
	public QueryAssistDialog(Shell parentShell, CQPCorpus corpus) {
		super(parentShell);
		this.parentShell = parentShell;
		this.setShellStyle(this.getShellStyle() | SWT.RESIZE);
		this.corpus = corpus;
		try {
			properties = corpus.getOrderedProperties();
		}
		catch (CqiClientException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(TXMUIMessages.queryAssistant);
		newShell.setMinimumSize(500, 350);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite p) {
		try {
			this.self = this;
			this.parent = p;
			// System.out.println(parent.getLayout());
			GridLayout mainlayout = new GridLayout(2, false);
			mainPanel = new Composite(parent, SWT.NONE);
			mainPanel.setLayout(mainlayout);
			GridData gridData = new GridData(GridData.FILL, GridData.FILL, true, true);
			mainPanel.setLayoutData(gridData);

			// directory
			chercheLabel = new Label(mainPanel, SWT.NONE);
			chercheLabel.setAlignment(SWT.CENTER);
			chercheLabel.setLayoutData(new GridData(GridData.BEGINNING, GridData.FILL, true, false, 1, 1));
			chercheLabel.setText(TXMUIMessages.imLookingForColon);

			scrollComposite = new ScrolledComposite(mainPanel, SWT.V_SCROLL | SWT.H_SCROLL);
			scrollComposite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 2, 1));

			wordcontainer = new Composite(scrollComposite, SWT.NONE);
			wordcontainer.setLayout(new GridLayout(1, false));

			scrollComposite.setContent(wordcontainer);
			scrollComposite.setExpandVertical(true);
			scrollComposite.setExpandHorizontal(true);
			scrollComposite.addControlListener(new ControlAdapter() {

				@Override
				public void controlResized(ControlEvent e) {
					// Rectangle r = scrollComposite.getClientArea();
					scrollComposite.setMinSize(wordcontainer.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				}
			});

			addWordField();

			Button addwordButton = new Button(mainPanel, SWT.PUSH);
			FontData[] fd = mainPanel.getFont().getFontData();
			fd[0].setStyle(SWT.BOLD);
			Font font = TXMFontRegistry.getFont(Display.getCurrent(), fd);
			addwordButton.setFont(font);
			addwordButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			addwordButton.setText(TXMUIMessages.addAWord + " ⯯"); //$NON-NLS-1$
			addwordButton.setToolTipText("Add a new word after the last one"); //$NON-NLS-1$
			addwordButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					if (nbwords > 0) addLinkField();
					addWordField();
					parent.layout(true);
					mainPanel.layout(true);
					wordcontainer.layout(true);
					scrollComposite.layout(true);
					mainPanel.update();
					wordcontainer.update();
					Rectangle r = scrollComposite.getClientArea();
					scrollComposite.setMinSize(wordcontainer.computeSize(r.width, SWT.DEFAULT));
					updateShell();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});


			Composite withinComposite = new Composite(mainPanel, SWT.NONE);
			withinComposite.setLayout(new GridLayout(3, false));
			withinComposite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1));
			withinButton = new Button(withinComposite, SWT.CHECK);
			withinButton.setText(TXMUIMessages.withinAContextOf); // "within structure: "
			withinButton.setToolTipText(TXMUIMessages.checkToAddAStructureConstraint);
			withinButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					structsCombo.setEnabled(withinButton.getSelection());
					withinN.setEnabled(withinButton.getSelection());
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			withinN = new Spinner(withinComposite, SWT.CHECK);
			withinN.setToolTipText(TXMUIMessages.contextSize);
			withinN.setMinimum(1);
			withinN.setMaximum(200);
			withinN.setEnabled(false);

			structsCombo = new Combo(withinComposite, SWT.READ_ONLY);
			structsCombo.setToolTipText(TXMUIMessages.theAvailableContexts);
			structsCombo.setEnabled(false);
			structsCombo.add("<word>"); //$NON-NLS-1$

			int iText = -1;
			int iP = -1;
			List<StructuralUnit> sus = corpus.getOrderedStructuralUnits();
			for (int i = 0; i < sus.size(); i++) {
				StructuralUnit su = sus.get(i);
				if (su.getName().equals("txmcorpus")) continue; //$NON-NLS-1$
				structsCombo.add(su.getName());

				if (su.getName().equals("text")) iText = i; //$NON-NLS-1$
				if (su.getName().equals("p")) iP = i; //$NON-NLS-1$
			}
			if (structsCombo.getItemCount() > 0) {
				if (iP >= 0) {
					structsCombo.select(iP + 1);
				}
				else {
					structsCombo.select(iText + 1);
				}
			}


			// Button removeWordButton = new Button(mainPanel, SWT.PUSH);
			// removeWordButton.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
			// removeWordButton.setText(TXMUIMessages.removeTheLastWord);
			// removeWordButton.setToolTipText(TXMUIMessages.removeTheLastWord);
			// removeWordButton.addSelectionListener(new SelectionListener() {
			// @Override
			// public void widgetSelected(SelectionEvent e) {
			// if (nbwords > 1) {
			// removeWordField();
			// removeLinkField();
			//
			// parent.layout(true);
			// mainPanel.layout(true);
			// wordcontainer.layout(true);
			// scrollComposite.layout(true);
			// mainPanel.update();
			// wordcontainer.update();
			// Rectangle r = scrollComposite.getClientArea();
			// scrollComposite.setMinSize(wordcontainer.computeSize(r.width, SWT.DEFAULT));
			// updateShell();
			// }
			// }
			//
			// @Override
			// public void widgetDefaultSelected(SelectionEvent e) {
			// }
			// });

		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return null;
	}

	/**
	 * Adds the link field.
	 */
	protected void addLinkField() {
		linkfields.add(new LinkField(wordcontainer, SWT.NONE));
	}

	/**
	 * Adds the word field.
	 */
	private void addWordField() {
		nbwords++;
		WordField wf = new WordField(wordcontainer, SWT.NONE, nbwords);
		GridData gd = new GridData(GridData.FILL, GridData.CENTER, false, false);
		gd.minimumWidth = 200;
		wf.setLayoutData(gd);
		wordfields.add(wf);

		wordcontainer.update();
		mainPanel.update();
	}

	/**
	 * remove the last link field.
	 */
	protected void removeLinkField() {
		if (linkfields.size() > 0) {
			linkfields.remove(linkfields.size() - 1).dispose();
		}
	}

	/**
	 * remove the last word field.
	 */
	private void removeWordField() {
		if (nbwords == 0) return;

		nbwords--;

		wordfields.remove(nbwords).dispose();

		wordcontainer.update();
		mainPanel.update();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		this.query = ""; //$NON-NLS-1$
		this.query = wordfields.get(0).getQuery();
		for (int i = 1; i < wordfields.size(); i++) {
			query += " "; //$NON-NLS-1$
			LinkField lf = linkfields.get(i - 1);
			WordField wf = wordfields.get(i);
			query += lf.getQuery();
			query += wf.getQuery();
		}

		if (withinButton != null && withinButton.getSelection() && structsCombo.getText().length() > 0) {
			query += " within "; //$NON-NLS-1$

			if (withinN.getSelection() > 1) {
				query += "" + withinN.getSelection() + " "; //$NON-NLS-1$ //$NON-NLS-2$
			}

			if (!structsCombo.getText().equals("<word>")) { //$NON-NLS-1$
				query += structsCombo.getText();
			}
		}

		super.okPressed();
	}

	/**
	 * Update shell.
	 */
	public void updateShell() {
		parentShell.layout(true);
	}

	/**
	 * Gets the query.
	 *
	 * @return the query
	 */
	public String getQuery() {
		return this.query;
	}

	public class WordPropertyField extends GLComposite {


		/** The prop combo. */
		Combo propCombo;

		/** The test combo. */
		Combo testCombo;

		/** The value t. */
		Combo valueT;

		/**
		 * Instantiates a new word field.
		 *
		 * @param parent the parent
		 * @param style the style
		 * @param no the no
		 */
		public WordPropertyField(Composite parent, int style, int no) {
			super(parent, style, "" + no); //$NON-NLS-1$

			this.getLayout().numColumns = 3;

			propCombo = new Combo(this, SWT.READ_ONLY | SWT.SINGLE);
			propCombo.setToolTipText(TXMUIMessages.propertyToTest);
			if (properties != null) {
				for (Property p : properties) {
					propCombo.add(p.getName());
				}
				try {
					for (StructuralUnitProperty p : corpus.getOrderedStructuralUnitProperties()) {
						propCombo.add(p.getFullName());
					}
				}
				catch (CqiClientException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				propCombo.select(0);
			}

			testCombo = new Combo(this, SWT.READ_ONLY | SWT.SINGLE);
			testCombo.setToolTipText(TXMUIMessages.testToDo);
			testCombo.add(TXMUIMessages.equalsTo);
			testCombo.add(TXMUIMessages.startsWith);
			testCombo.add(TXMUIMessages.endsWith);
			testCombo.add(TXMUIMessages.contains);
			testCombo.add(TXMUIMessages.isDifferentFrom);
			testCombo.select(0);

			valueT = new Combo(this, SWT.SINGLE);
			valueT.setToolTipText(TXMUIMessages.theWordPropertyValue);
			GridData gdata = new GridData(GridData.FILL, GridData.FILL, false, false);
			gdata.widthHint = 150;
			gdata.minimumWidth = 150;
			valueT.setLayoutData(gdata);

			propCombo.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					updateAvailableValues();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			updateAvailableValues();
		}

		private void updateAvailableValues() {
			String p = propCombo.getText();
			String[] items = null;
			if (p.contains("_")) { //$NON-NLS-1$
				StructuralUnitProperty sup = corpus.getStructuralUnitProperty(p);
				try {
					if (sup.getNValues() > 1000) {
						valueT.setItems(new String[0]);
						return;
					}
					items = sup.getValuesAsStrings();
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				try {
					WordProperty prop = corpus.getProperty(p);
					if (prop.getNValues() > 1000) {
						valueT.setItems(new String[0]);
						return;
					}
					items = prop.getValues();
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (items != null) {
				Arrays.sort(items);
				valueT.setItems(items);
				KeyStroke keys = KeyStroke.getInstance(SWT.CONTROL, SWT.SPACE);
				new TXMAutoCompleteField(valueT, new ComboContentAdapter(), items, null);
				valueT.setToolTipText(TXMUIMessages.bind(TXMUIMessages.startTypingPartOfTheValueToOpenTheValueListOfP0, p));
			}
			else {
				valueT.setItems(new String[0]);
			}
		}
	}

	/**
	 * The Class WordField.
	 */
	public class WordField extends Composite {

		ArrayList<QueryAssistDialog.WordPropertyField> propertyFields = new ArrayList<>();

		Button noCasseButton;
		Button nodiacriticsButton;
		Button targetButton;

		Button removeButton;

		GLComposite propertiesFieldComposite;

		private Label titleL;

		private int no;

		private Button allCriteriaButton;

		private Label allCriteriaLabel;


		/**
		 * Instantiates a new word field.
		 *
		 * @param parent the parent
		 * @param style the style
		 * @param no the no
		 */
		public WordField(Composite parent, int style, int no) {
			super(parent, style);
			this.no = no;
			this.setLayout(new GridLayout(13, false));

			titleL = new Label(this, SWT.NONE);

			GridData datalayout = new GridData(GridData.FILL, GridData.CENTER, true, false);
			titleL.setLayoutData(datalayout);

			GridData buttonLayout = new GridData(GridData.FILL, GridData.CENTER, false, false);
			buttonLayout.widthHint = 25;

			Button addPropertyButton = new Button(this, SWT.PUSH);
			addPropertyButton.setLayoutData(buttonLayout);
			addPropertyButton.setText("+"); //$NON-NLS-1$
			addPropertyButton.setToolTipText(TXMUIMessages.addAPropertyTest);
			addPropertyButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					addProperty();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			Button removePropertyButton = new Button(this, SWT.PUSH);
			removePropertyButton.setLayoutData(buttonLayout);
			removePropertyButton.setText("-"); //$NON-NLS-1$
			removePropertyButton.setToolTipText(TXMUIMessages.removeTheLastPropertyTest);
			removePropertyButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					removeProperty();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			propertiesFieldComposite = new GLComposite(this, SWT.NONE, "properties"); //$NON-NLS-1$
			GridData propertiesvlayout = new GridData(GridData.FILL, GridData.CENTER, true, false);
			propertiesFieldComposite.setLayoutData(propertiesvlayout);
			propertiesFieldComposite.getLayout().numColumns = 2;

			allCriteriaLabel = new Label(this, SWT.NONE);
			allCriteriaLabel.setText("  "); //$NON-NLS-1$

			allCriteriaButton = new Button(this, SWT.CHECK);
			allCriteriaButton.setToolTipText(TXMUIMessages.allPropertyTestsAreVerified);
			allCriteriaButton.setSelection(true);

			new Label(this, SWT.NONE).setText("%c"); //$NON-NLS-1$
			noCasseButton = new Button(this, SWT.CHECK);
			noCasseButton.setToolTipText("Check to ignore the case property of the characters ");
			
			new Label(this, SWT.NONE).setText("%d"); //$NON-NLS-1$
			nodiacriticsButton = new Button(this, SWT.CHECK);
			nodiacriticsButton.setToolTipText("Check to ignore the diacritics property of the characters");
			
			new Label(this, SWT.NONE).setText("@"); //$NON-NLS-1$
			targetButton = new Button(this, SWT.CHECK);
			targetButton.setToolTipText(TXMUIMessages.checkToTargetThisWordInTheQuery);
			targetButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					if (targetButton.getSelection()) {
						for (WordField wf : wordfields) {
							wf.targetButton.setSelection(false);
						}
						targetButton.setSelection(true);
					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});


			removeButton = new Button(this, SWT.PUSH);
			removeButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_DELETE));
			removeButton.setToolTipText(TXMCoreMessages.common_delete);
			removeButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					if (nbwords < 2) return;

					int idx = QueryAssistDialog.this.wordfields.indexOf(WordField.this);
					// System.out.println("field: "+idx);
					// System.out.println(QueryAssistDialog.this.linkfields);
					if (QueryAssistDialog.this.linkfields.size() > 0) {
						if (idx > 0) idx--;
						LinkField field = QueryAssistDialog.this.linkfields.remove(idx);
						field.dispose();
					}
					QueryAssistDialog.this.wordfields.remove(WordField.this);
					QueryAssistDialog.this.nbwords--;
					WordField.this.dispose();

					wordcontainer.update();
					mainPanel.update();

					QueryAssistDialog.this.parent.layout(true);
					QueryAssistDialog.this.mainPanel.layout(true);
					QueryAssistDialog.this.wordcontainer.layout(true);
					QueryAssistDialog.this.scrollComposite.layout(true);
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			addProperty();
		}

		private void addProperty() {
			WordPropertyField pField = new WordPropertyField(propertiesFieldComposite, SWT.NONE, propertyFields.size());
			pField.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false, 2, 1));
			propertyFields.add(pField);
			updateWidgets();
		}

		private void removeProperty() {
			if (propertyFields.size() > 0) {
				WordPropertyField pField = propertyFields.remove(propertyFields.size() - 1);
				pField.dispose();
			}
			updateWidgets();
		}

		private void updateWidgets() {

			allCriteriaButton.setEnabled(propertyFields.size() > 1);
			allCriteriaButton.setVisible(propertyFields.size() > 1);
			if (allCriteriaButton.isEnabled()) {
				allCriteriaLabel.setText("&&"); //$NON-NLS-1$
			}
			else {
				allCriteriaLabel.setText("  "); //$NON-NLS-1$
			}
			// propertiesAllOrOne.setVisible(propertyFields.size() > 1);
			// ((GridData)propertiesAllOrOne.getLayoutData()).exclude = propertyFields.size() < 2;
			if (propertyFields.size() == 0) {
				titleL.setText(TXMUIMessages.aWord);
			}
			else if (propertyFields.size() == 1) {
				titleL.setText(NLS.bind(TXMUIMessages.aWordWithItsProperty, no));
			}
			else {
				titleL.setText(NLS.bind(TXMUIMessages.aWordWithItsProperties, no));
			}
			propertiesFieldComposite.layout();
			// this.layout(true);
			// mainPanel.layout(true);
			wordcontainer.layout();
			wordcontainer.update();
			// mainPanel.update();
		}

		/**
		 * Gets the query.
		 *
		 * @return the query
		 */
		public String getQuery() {
			String query = ""; //$NON-NLS-1$
			if (targetButton.getSelection()) {
				query += "@"; //$NON-NLS-1$
			}
			query += "["; //$NON-NLS-1$

			String propertiesAllOrOne = " | "; //$NON-NLS-1$
			if (allCriteriaButton.getSelection()) {
				propertiesAllOrOne = " & "; //$NON-NLS-1$
			}

			int nField = 0;
			for (WordPropertyField f : propertyFields) {
				Combo propCombo = f.propCombo;
				Combo testCombo = f.testCombo;
				Combo valueT = f.valueT;
				if (nField > 0) {
					// query += " "+(propertiesAllOrOne.getText().equals("ALL")?"&":"|")+" ";
					query += propertiesAllOrOne;
				}

				String p = propCombo.getItem(propCombo.getSelectionIndex());
				if (p.contains("_")) query += "_."; // structural properties must be prefixed with "_." //$NON-NLS-1$ //$NON-NLS-2$
				query += p;
				if (testCombo.getSelectionIndex() == 0)// equals
					query += " = \"" + valueT.getText().replace("\"", "\\\"") + "\""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				else if (testCombo.getSelectionIndex() == 1)// starts with
					query += " = \"" + valueT.getText().replace("\"", "\\\"") + ".*\""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				else if (testCombo.getSelectionIndex() == 2)// ends with
					query += " = \".*" + valueT.getText().replace("\"", "\\\"") + "\""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				else if (testCombo.getSelectionIndex() == 3)// contains
					query += " = \".*" + valueT.getText().replace("\"", "\\\"") + ".*\""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				else if (testCombo.getSelectionIndex() == 4)// different from
					query += " != \"" + valueT.getText().replace("\"", "\\\"") + "\""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				
				if (noCasseButton.getSelection() && nodiacriticsButton.getSelection()) {
					query += "%cd"; //$NON-NLS-1$
				} else if (noCasseButton.getSelection()) {
					query += "%c"; //$NON-NLS-1$
				} else if (nodiacriticsButton.getSelection()) {
					query += "%d"; //$NON-NLS-1$
				}
				nField++;
			}
			return query + "]"; //$NON-NLS-1$
		}
	}

	/**
	 * The Class LinkField.
	 */
	public class LinkField extends Composite {

		/** The link combo. */
		Combo linkCombo;

		/**
		 * Instantiates a new link field.
		 *
		 * @param parent the parent
		 * @param style the style
		 */
		public LinkField(Composite parent, int style) {
			super(parent, style);
			this.setLayout(new RowLayout());

			linkCombo = new Combo(this, SWT.READ_ONLY | SWT.SINGLE);
			linkCombo.add(TXMUIMessages.followedBy);
			linkCombo.add(TXMUIMessages.separatedByAtLeast0Word);
			linkCombo.add(TXMUIMessages.separatedByAtLeast1Word);

			linkCombo.select(0);
		}

		/**
		 * Gets the query.
		 *
		 * @return the query
		 */
		public String getQuery() {
			if (linkCombo.getSelectionIndex() == 0) // followed
				return ""; //$NON-NLS-1$
			else if (linkCombo.getSelectionIndex() == 1)// at least 0 word
				return "[]{0,50} "; //$NON-NLS-1$
			else if (linkCombo.getSelectionIndex() == 2)// at least 1 word
				return "[]{1, 50} "; //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}
}
