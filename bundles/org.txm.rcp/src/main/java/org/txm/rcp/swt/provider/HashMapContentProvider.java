package org.txm.rcp.swt.provider;

import java.util.AbstractMap;
import java.util.Set;

import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class HashMapContentProvider implements IContentProvider, IStructuredContentProvider {

	@Override
	public Object[] getElements(Object inputElement) {
		Set<?> list = ((AbstractMap<?, ?>) inputElement).entrySet();

		return list.toArray();
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
	}
}
