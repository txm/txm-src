package org.txm.rcp.handlers.results;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.TXMEditorToolBar;
import org.txm.utils.logger.Log;

/**
 * Calls TXMEditor.compute() on the current active editor.
 * 
 * @author mdecorde
 *
 */
public class RevertTXMResult extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		try {
			revert(event);
		}
		catch (Exception e) {
			Log.severe(e);
			Log.printStackTrace(e);
		}

		return null;
	}

	public static Object revert(ExecutionEvent event) throws Exception {

		Object trigger = event.getTrigger();

		if (trigger instanceof Event) {
			Event e = (Event) trigger;
			if (e.widget instanceof ToolItem) {
				ToolItem ti = (ToolItem) e.widget;
				ToolBar parent = ti.getParent();
				if (parent instanceof TXMEditorToolBar) {
					TXMResult result = ((TXMEditorToolBar) parent).getEditorPart().getResult();
					ArrayList<HashMap<String, Object>> history = result.getParametersHistory();
					if (history != null && history.size() > 0) {
						result.revertParametersFromHistory();
						return ((TXMEditorToolBar) parent).getEditorPart().compute(true);
					}
					return null;
				}
			}
		}
		return null;
	}
}
