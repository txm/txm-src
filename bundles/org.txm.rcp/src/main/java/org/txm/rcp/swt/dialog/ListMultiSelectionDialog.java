package org.txm.rcp.swt.dialog;

import java.util.LinkedHashMap;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

public class ListMultiSelectionDialog<T> extends Dialog {

	LinkedHashMap<String, T> values;

	LinkedHashMap<String, T> selectedValues;

	List valuesList;

	private String title;

	private String message;

	public ListMultiSelectionDialog(Shell parentShell, String title, String message, LinkedHashMap<String, T> values) {

		super(parentShell);
		this.values = values;
		this.selectedValues = new LinkedHashMap<>();
		this.title = title;
		this.message = message;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(title);
	}
	
	public LinkedHashMap<String, T> getInitialValues() {
		return values;
	}

	public LinkedHashMap<String, T> getSelectedValues() {
		return selectedValues;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite p) {
		//System.out.println(p.getLayout());

		valuesList = new List(p, SWT.MULTI);

		valuesList.setItems(values.keySet().toArray(new String[values.size()]));

		return p;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		for (String item : valuesList.getSelection()) {
			selectedValues.put(item, values.get(item));
		}
		super.okPressed();
	}

}
