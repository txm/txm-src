// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp;

import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.swt.widgets.Display;

/**
 * Shortcut to the RCP status bar
 * FIXME: define messages, how and when show them then uncomment message methods
 * 
 * @author mdecorde.
 */
public class StatusLine {

	/** The timer */
	private static Timer timer;

	private static int time = 15000;

	private static boolean timerScheduled = false;

	/** The status line. */
	private static IStatusLineManager statusLine;

	/**
	 * Sets the IStatusLineManager.
	 *
	 * @param statusline the statusline
	 */
	public static void set(IStatusLineManager statusline) {
		StatusLine.statusLine = statusline;
	}

	static int MAX = 50;

	/**
	 * Shows a message in the status line.
	 *
	 * @param message the new message
	 */
	public static void setMessage(final String message) {
		// return;

		// FIXME: define messages, how and when show them then uncomment message methods
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				if (statusLine != null) {
					if (message != null && message.length() > MAX) {
						statusLine.setMessage(null, message.substring(0, MAX) + "..."); //$NON-NLS-1$
					}
					else {
						statusLine.setMessage(null, message);
					}
				}
			}
		});

		cancelTimer();
		scheduleTimer();
	}

	/**
	 * Shows a message in the status line and keeps it.
	 *
	 * @param message the new message
	 */
	public static void setMessage(final String message, boolean keep) {
		//return;
		// FIXME: define messages, how and when show them then uncomment message methods
		if (!keep) {
			setMessage(message);
			return;
		}
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				statusLine.setMessage(null, message);
			}
		});
		cancelTimer();
	}

	/**
	 * Shows an error message in the status line.
	 *
	 * @param message the error message string
	 */
	public static void error(final String message) {
		//return;
		// FIXME: define messages, how and when show them then uncomment message methods
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				statusLine.setErrorMessage(null, message);
			}
		});
		cancelTimer();
		scheduleTimer();
	}

	/**
	 * Shows an error message in the status line and keeps it.
	 *
	 * @param message the message string
	 */
	public static void error(final String message, boolean keep) {
		//return;
		// FIXME: define messages, how and when show them then uncomment message methods
		if (!keep) {
			setMessage(message);
			return;
		}
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				statusLine.setErrorMessage(null, message);
			}
		});
		cancelTimer();
	}

	/**
	 * Stops the timer
	 */
	private static void cancelTimer() {
		if (!timerScheduled)
			return;
		timerScheduled = false;
		timer.cancel();
	}

	/**
	 * Starts the timer
	 */
	private static void scheduleTimer() {
		if (timerScheduled)
			return;
		timerScheduled = true;
		timer = new Timer();
		TimerTask timerTask = new TimerTask() {

			@Override
			public void run() {
				Runnable r = new Runnable() {

					@Override
					public void run() {
						StatusLine.setMessage(""); //$NON-NLS-1$
					}
				};
				Display.getDefault().syncExec(r);
			}
		};
		timer.schedule(timerTask, time);
	}
}
