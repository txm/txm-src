package org.txm.rcp.commands.function;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.rcp.views.corpora.CorporaView;

/**
 * Refresh the corpora view TreeViewer
 * 
 * @author mdecorde
 *
 */
public class RefreshCorporaView extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		CorporaView.reload();

		return null;
	}
}
