package org.txm.rcp.editors.menu;

import java.net.URL;
import java.nio.file.Paths;

import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.txm.rcp.commands.ShowSelected;
import org.txm.rcp.messages.TXMUIMessages;

public class PagePropertiesMenu extends MenuItem {

	Browser browser;

	public PagePropertiesMenu(Browser browser, Menu menu, int style) {

		super(menu, style);

		this.browser = browser;
		this.setText(TXMUIMessages.pageProperties);
		this.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					String url = browser.getUrl();
					if (url.startsWith("http://") || url.startsWith("https://")) { //$NON-NLS-1$ //$NON-NLS-2$
						System.out.println("URL=" + url); //$NON-NLS-1$
					}
					else {
						ShowSelected.print(Paths.get(new URL(url).toURI()).toFile());
					}
				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Combo#checkSubclass()
	 */
	@Override
	protected void checkSubclass() {

	}
}
