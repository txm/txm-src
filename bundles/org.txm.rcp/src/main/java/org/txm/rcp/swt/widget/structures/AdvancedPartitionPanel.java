package org.txm.rcp.swt.widget.structures;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.widget.AssistedQueryWidget;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

public class AdvancedPartitionPanel extends Composite implements PartitionParameterGenerator {

	// for the "advanced" tab
	/** The queries. */
	private List<AssistedQueryWidget> queries = new ArrayList<>();

	/** The part names. */
	private List<Text> partNames = new ArrayList<>();

	/** The queries string. */
	List<String> queriesString = new ArrayList<>();

	private CQPCorpus corpus;

	private ScrolledComposite scrollComposite;

	private Composite compositeForAdvanced;

	public AdvancedPartitionPanel(Composite parent, int style, CQPCorpus corpus) {
		super(parent, style);

		this.setLayout(new GridLayout(2, false));
		this.corpus = corpus;

		scrollComposite = new ScrolledComposite(this, SWT.V_SCROLL);
		scrollComposite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 2, 1));

		Button importButton = new Button(this, SWT.NONE);
		importButton.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_CENTER, GridData.FILL, false, false));
		importButton.setText("+ from properties file..."); //$NON-NLS-1$
		importButton.setToolTipText("Add queries from a properties file. One query per file name=query");
		importButton.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				FileDialog d = new FileDialog(event.display.getActiveShell());
				d.setFilterExtensions(new String[] { "*.properties" });
				d.setText("Select a .properties file containing CQL queries");
				String path = d.open();
				if (path != null) {
					Properties props = new Properties();
					try {
						props.load(IOUtils.getReader(new File(path)));
						for (Object key : props.keySet()) {
							String value = props.getProperty(key.toString());
							AssistedQueryWidget qw = addQueryField(key.toString().substring(0, Math.min(20, key.toString().length())), value);
						}
					}
					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

		compositeForAdvanced = new Composite(scrollComposite, SWT.NONE);
		GridLayout layoutForAdvanced = new GridLayout(4, false);
		compositeForAdvanced.setLayout(layoutForAdvanced);

		scrollComposite.setContent(compositeForAdvanced);
		scrollComposite.setExpandVertical(true);
		scrollComposite.setExpandHorizontal(true);
		scrollComposite.addControlListener(new ControlAdapter() {

			@Override
			public void controlResized(ControlEvent e) {

				Rectangle r = scrollComposite.getClientArea();
				scrollComposite.setMinSize(compositeForAdvanced.computeSize(r.width, SWT.DEFAULT));
			}
		});

		createAdvancedQueryField();
	}

	/**
	 * Creates the advanced query field.
	 *
	 */
	private void createAdvancedQueryField() {

		addQueryField();
		Rectangle r = scrollComposite.getClientArea();
		scrollComposite.setMinSize(compositeForAdvanced.computeSize(r.width, SWT.DEFAULT));
	}

	/**
	 * Adds the query field.
	 *
	 */
	private AssistedQueryWidget addQueryField() {

		return addQueryField(null, null);
	}

	/**
	 * Adds the query field.
	 *
	 * @param name the query name
	 * @param value the query
	 */
	private AssistedQueryWidget addQueryField(String name, String value) {

		final Text advancedPartLabel = new Text(compositeForAdvanced, SWT.BORDER);
		advancedPartLabel.setText(TXMUIMessages.part + (partNames.size() + 1));
		advancedPartLabel.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));
		if (name != null && name.length() > 0) {
			advancedPartLabel.setText(name);
		}
		partNames.add(advancedPartLabel);

		final AssistedQueryWidget queryText;
		queryText = new AssistedQueryWidget(compositeForAdvanced, SWT.BORDER, corpus, CQPSearchEngine.getEngine());
		queryText.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		GridData gridData = new GridData(GridData.FILL, GridData.FILL, true, false);
		// gridData.widthHint = convertHeightInCharsToPixels(20);
		queryText.setLayoutData(gridData);
		queries.add(queryText);
		if (value != null) {
			queryText.setText(value);
		}

		Button plusButton = new Button(compositeForAdvanced, SWT.NONE);
		plusButton.setText("+"); //$NON-NLS-1$
		plusButton.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				addQueryField();
			}
		});

		final Button minusButton = new Button(compositeForAdvanced, SWT.NONE);
		minusButton.setText("-"); //$NON-NLS-1$
		minusButton.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				if (queries.size() <= 1) return;
				queries.remove(queryText);
				partNames.remove(advancedPartLabel);
				advancedPartLabel.dispose();
				queryText.dispose();
				minusButton.dispose();
				plusButton.dispose();
				compositeForAdvanced.layout(true);
			}
		});
		Rectangle r = scrollComposite.getClientArea();
		scrollComposite.setMinSize(compositeForAdvanced.computeSize(r.width, SWT.DEFAULT));
		scrollComposite.update();
		scrollComposite.layout();
		compositeForAdvanced.update();
		compositeForAdvanced.layout();

		return queryText;
	}

	/**
	 * Gets the advance part names.
	 *
	 * @return the advance part names
	 */
	public List<String> getNames() {

		ArrayList<String> values = new ArrayList<>();
		int count = 0;
		for (Text t : partNames) {
			String name = t.getText().trim();
			if (name.length() == 0) {
				name = TXMUIMessages.part + count++;
			}
			values.add(name);
		}
		return values;
	}

	/**
	 * Gets the queries.
	 *
	 * @return the queries
	 */
	public List<String> getQueries() {

		return queriesString;
	}

	/**
	 * Return true if no query is currently given in the "advanced" tab (ie if
	 * all Text field opened are empty).
	 *
	 * @return true, if successful
	 */
	public boolean isQueriesSet() {

		for (AssistedQueryWidget query : queries) {
			if (query.getQueryWidget().getText().length() == 0) {
				return true;
			}
		}
		return false;
	}

	public Partition createPartition(String name, Partition partition) {

		queriesString.clear();
		for (AssistedQueryWidget query : queries) {
			queriesString.add(query.getQueryString());
			query.memorize();
		}

		try {
			// auto-naming if needed
			if (name.isEmpty()) {
				name = "partition" + (this.corpus.getPartitions().size() + 1); //$NON-NLS-1$
				Log.info(TXMCoreMessages.bind(TXMUIMessages.noPartitionNameWasSpecifiedTheNameIsP0, name));
			}

			if (partition == null) {
				partition = new Partition(this.corpus);
			}
			else {
				partition.removeAllParts();
			}
			partition.setParameters(name, this.getQueries(), this.getNames());
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return null;
		}

		return partition;
	}

}
