/**
 * 
 */
package org.txm.rcp.swt.widget;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Spinner;

/**
 * A spinner that supports float numbers.
 * 
 * @author sjacquot
 *
 */
public class FloatSpinner extends Spinner {

	/**
	 * Creates a FloatSpinner.
	 * 
	 * @param parent
	 * @param style
	 */
	public FloatSpinner(Composite parent, int style) {
		super(parent, style);
		this.setDigits(1);
		this.setMinimum(0);
		this.setIncrement(1);
	}

	/**
	 * Sets the selection as float.
	 * 
	 * @param value
	 */
	public void setSelection(double value) {
		super.setSelection((int) (value * Math.pow(10, this.getDigits())));
	}

	/**
	 * Gets the selection as a float number.
	 * 
	 * @return
	 */
	public float getSelectionAsFloat() {
		return (float) (super.getSelection() / Math.pow(10, this.getDigits()));
	}

	/**
	 * Sets the maximum that the receiver will allow.
	 * 
	 * @param value
	 */
	public void setMaximumAsFloat(double value) {
		this.setMaximum((int) (value * Math.pow(10, this.getDigits())));
	}

	@Override
	protected void checkSubclass() {

	}
}
