// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.files;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.Toolbox;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.views.fileexplorer.Explorer;
import org.txm.rcp.views.fileexplorer.MacroExplorer;

// TODO: Auto-generated Javadoc
/**
 * Handler: create a directory depending on the selection
 */
public class CreateFolder extends AbstractHandler {

	private static final String ID = "org.txm.rcp.commands.CreateFolder"; //$NON-NLS-1$
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchPart page = HandlerUtil.getActiveWorkbenchWindow(event)
				.getActivePage().getActivePart();

		String dir = Toolbox.getTxmHomePath();

		//if selection is a file, get its path
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		boolean useSavedValue = true;
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection sel = (IStructuredSelection) selection;
			Object obj = sel.getFirstElement();

			if (obj instanceof File) {
				File current = ((File) obj);
				if (current.isDirectory())
					dir = current.getAbsolutePath();
				else
					dir = current.getParent();
				useSavedValue = false;
			}
		}
		//ask new file name
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event)
				.getShell();
		FileDialog dialog = new FileDialog(shell, SWT.SAVE);

		if (LastOpened.getFile(ID) != null && useSavedValue) {
			dialog.setFilterPath(LastOpened.getFolder(ID));
			dialog.setFileName(LastOpened.getFile(ID));
		}
		else {
			dialog.setFilterPath(dir);
		}
		String path = dialog.open();
		if (path == null) return null;
		//create the new file
		File newfile = new File(path);
		LastOpened.set(ID, newfile.getParent(), newfile.getName());
		if (!newfile.exists() && newfile.getParentFile().canWrite()) {
			if (newfile.mkdir())
				System.out.println(NLS.bind(TXMUIMessages.newFolderColonP0, newfile.getAbsolutePath()));

		}
		Explorer.refresh(newfile);
		MacroExplorer.refresh(newfile);

		return null;
	}
}
