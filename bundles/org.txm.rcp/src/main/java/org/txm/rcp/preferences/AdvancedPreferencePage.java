// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.preferences;

import java.util.logging.Level;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.IHandlerService;
import org.txm.Toolbox;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.rcp.views.fileexplorer.MacroExplorer;
import org.txm.utils.logger.Log;

/**
 * Advanced preferences page.
 */
public class AdvancedPreferencePage extends TXMPreferencePage {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 */
	@Override
	protected void createFieldEditors() {

		Group messagesGroup = new Group(getFieldEditorParent(), SWT.NONE);
		messagesGroup.setText(TXMUIMessages.logs);
		GridData gridData2 = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData2.horizontalSpan = 3;
		gridData2.verticalIndent = 10;
		messagesGroup.setLayoutData(gridData2);
		RowLayout rlayout = new RowLayout(SWT.VERTICAL);
		rlayout.marginHeight = 5;
		messagesGroup.setLayout(rlayout);

		// Log level
		String[][] choices = new String[9][2];
		choices[0][0] = choices[0][1] = "OFF"; //$NON-NLS-1$
		choices[1][0] = choices[1][1] = "SEVERE"; //$NON-NLS-1$
		choices[2][0] = choices[2][1] = "WARNING"; //$NON-NLS-1$
		choices[3][0] = choices[3][1] = "INFO"; //$NON-NLS-1$
		choices[4][0] = choices[4][1] = "CONFIG"; //$NON-NLS-1$
		choices[5][0] = choices[5][1] = "FINE"; //$NON-NLS-1$
		choices[6][0] = choices[6][1] = "FINER"; //$NON-NLS-1$
		choices[7][0] = choices[7][1] = "FINEST"; //$NON-NLS-1$
		choices[8][0] = choices[8][1] = "ALL"; //$NON-NLS-1$
		addField(new ComboFieldEditor(TBXPreferences.LOG_LEVEL, TXMUIMessages.logLevel, choices, messagesGroup));

		// Log timings
		addField(new BooleanFieldEditor(TBXPreferences.SHOW_TIMINGS, TXMUIMessages.displayRuntimesInConsole, messagesGroup));

		// Log stack trace
		addField(new BooleanFieldEditor(TBXPreferences.LOG_STACKTRACE, TXMUIMessages.printTheStacktrace, messagesGroup));

		// Log console
		addField(new BooleanFieldEditor(TBXPreferences.ADD_TECH_LOGS, TXMUIMessages.logInTheConsole, messagesGroup));

		// Show dialog on severe error
		addField(new BooleanFieldEditor(RCPPreferences.SHOW_SEVERE_DIALOG, TXMUIMessages.ShowADialogBoxWhenASevereErrorOccurs, messagesGroup));

		// Log in file
		addField(new BooleanFieldEditor(TBXPreferences.LOG_IN_FILE, TXMUIMessages.logInAFile, messagesGroup));
		
		((GridLayout)messagesGroup.getLayout()).marginWidth = 3;
		((GridLayout)messagesGroup.getLayout()).numColumns = 2;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#performOk()
	 */
	@Override
	public boolean performOk() {

		boolean b = super.performOk();

		if (!b) return false;

		try {
			MacroExplorer.refresh();
			CorporaView.refresh();

			// update the Logger options
			Log.setLevel(Level.parse(Toolbox.getPreference(TBXPreferences.LOG_LEVEL)));
			Log.setPrintInConsole(TBXPreferences.getInstance().getBoolean(TBXPreferences.ADD_TECH_LOGS));

			Log.log_stacktrace_boolean = TBXPreferences.getInstance().getBoolean(TBXPreferences.LOG_STACKTRACE);

			boolean log_in_file = TBXPreferences.getInstance().getBoolean(TBXPreferences.LOG_IN_FILE);
			String file = Log.setPrintInFile(log_in_file);

			// update repositories
			IHandlerService service = PlatformUI.getWorkbench().getService(IHandlerService.class);
			service.executeCommand("org.txm.rcp.p2.plugins.FixUpdateHandler", null); //$NON-NLS-1$
		}
		catch (Exception e) {
			System.err.println(NLS.bind(TXMUIMessages.failedToSavePreferencesColonP0, e));
		}
		return true;
	}


	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(TBXPreferences.getInstance().getPreferencesNodeQualifier()));
	}
}
