// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.txm.core.preferences.TBXPreferences;
import org.txm.objects.Project;
import org.txm.objects.Workspace;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.core.QueryHistory;

/**
 * User preferences page.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class UserPreferencePage extends RCPPreferencesPage {

	/**
	 * Instantiates a new user preference page.
	 */
	public UserPreferencePage() {
		super();
	}

	@Override
	protected void createFieldEditors() {
		
		// removed because too hot 
//		this.addField(new BooleanFieldEditor(RCPPreferences.DONT_ALERT_USER_BEFORE_DELETE, TXMUIMessages.askBeforeDeletingAnObject, BooleanFieldEditor.SEPARATE_LABEL,
//		this.getFieldEditorParent()));

		// Auto compute the result when a parameter changes
		this.addField(
			new BooleanFieldEditor(RCPPreferences.AUTO_UPDATE_EDITOR, TXMUIMessages.automaticRecalculation, BooleanFieldEditor.SEPARATE_LABEL, this.getFieldEditorParent()));

		// Auto compute the result when a parameter changes
		String choices[][] = new String[3][2];
		choices[0][0] = choices[0][1] = RCPPreferences.COMPUTE_CHILDREN_ASK;
		choices[1][0] = choices[1][1] = RCPPreferences.COMPUTE_CHILDREN_ALWAYS;
		choices[2][0] = choices[2][1] = RCPPreferences.COMPUTE_CHILDREN_NEVER;
		this.addField(new ComboFieldEditor(RCPPreferences.COMPUTE_CHILDREN, TXMUIMessages.AutomaticallyUpdateSubResults, choices, this.getFieldEditorParent()));

		// this.addField(new BooleanFieldEditor(RCPPreferences.AUTO_COMPUTE_ON_EDITOR_OPEN, "Compute when opening its window", BooleanFieldEditor.SEPARATE_LABEL, this.getFieldEditorParent()));

		// Auto save each result after computing and auto load them at startup
		this.addField(new BooleanFieldEditor(TBXPreferences.AUTO_PERSISTENCE_ENABLED, TXMUIMessages.enableAutomaticSaveOfAllResultsPersistence, BooleanFieldEditor.SEPARATE_LABEL,
				this.getFieldEditorParent()));

		this.addField(new IntegerFieldEditor(RCPPreferences.MAX_NUMBER_OF_COLUMNS, TXMUIMessages.theMaximumNumberOfColumnShownInTables, this.getFieldEditorParent()));

		//		this.addField(new DoubleFieldEditor(RCPPreferences.COLUMNS_WIDTH_MULTIPLIER, "Columns width multiplier", this.getFieldEditorParent()));

		this.addField(new IntegerFieldEditor(TBXPreferences.QUERY_HISTORY_SIZE, TXMUIMessages.theMaximumNumberOfQueryStoredInTheQueryHistory, this.getFieldEditorParent()));
	}

	@Override
	public boolean performOk() {
		boolean ret = super.performOk();
		if (ret) {

			int MAX = TBXPreferences.getInstance().getInt(TBXPreferences.QUERY_HISTORY_SIZE);
			for (Project p : Workspace.getInstance().getProjects()) {
				QueryHistory h = p.getFirstChild(QueryHistory.class);
				if (h != null) {
					h.reduceSize(MAX);
				}
			}
		}

		return ret;
	}
}
