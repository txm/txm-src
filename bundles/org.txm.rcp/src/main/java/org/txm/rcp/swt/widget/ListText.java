package org.txm.rcp.swt.widget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * Edit a list of values in a small Text field. Clicking the field opens a multiline Text field
 * 
 * @author mdecorde
 *
 */
public class ListText extends Text {

	public ListText(Composite parent, int style) {
		super(parent, style);

		this.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
				EditListDialog dialog = new EditListDialog(ListText.this, ListText.this.getText());
				if (dialog.open() == Window.OK) {
					ListText.this.setText(dialog.getText());
				}
			}

			@Override
			public void mouseDown(MouseEvent e) {
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}
		});
	}

	public void checkSubclass() {
		// mandatory to be able to subclass Text
	}

	public List<String> toList() {
		String t = this.getText().trim();
		if (t.length() == 0) return new ArrayList<String>();

		return Arrays.asList(t.split("\n"));
	}

	/**
	 * Set the Query options to use (set the right Query class to use)
	 */
	public class EditListDialog extends Dialog {

		/** The main panel. */
		Composite mainPanel;

		/** The parent shell. */
		Shell parentShell;

		private Text smallerTextField;

		Text textField;

		String text;

		public EditListDialog(Text settingsButton, String content) {
			super(settingsButton.getDisplay().getActiveShell());
			text = content;
			this.setShellStyle(SWT.NONE | SWT.APPLICATION_MODAL);

			this.smallerTextField = settingsButton;
			this.parentShell = settingsButton.getDisplay().getActiveShell();
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
		 */
		@Override
		protected void configureShell(Shell newShell) {
			super.configureShell(newShell);

			newShell.setSize(400, 300);
			newShell.setLocation(Display.getCurrent().map(smallerTextField, null, 0, smallerTextField.getSize().y));
		}

		@Override
		protected Point getInitialLocation(Point initialSize) {
			return new Point(500, 200);
		}


		@Override
		protected boolean isResizable() {
			return true;
		}

		public void okPressed() {

			text = textField.getText();

			super.okPressed();
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
		 */
		@Override
		protected Control createDialogArea(Composite p) {

			GridLayout layout = (GridLayout) p.getLayout();
			layout.verticalSpacing = 5;
			layout.marginBottom = 5;
			layout.marginTop = 5;
			layout.marginLeft = 5;
			layout.marginRight = 5;

			textField = new Text(p, SWT.MULTI);
			textField.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 1, 1));
			if (text != null) textField.setText(text);

			return p;
		}

		public String getText() {
			return this.text;
		}
	}
}
