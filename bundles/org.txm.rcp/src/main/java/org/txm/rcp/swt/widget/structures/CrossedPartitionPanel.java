package org.txm.rcp.swt.widget.structures;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.utils.logger.Log;

public class CrossedPartitionPanel extends Composite implements PartitionParameterGenerator {

	/** The selected structural unit property. */
	private List<StructuralUnitProperty> selectedStructuralUnitProperties = new ArrayList<>();

	/** The structural units final. */
	private List<StructuralUnit> structuralUnitsFinal;

	private Text separatorText;

	/** The property combo. */
	private SuperListViewer propertyCombo;

	private CQPCorpus corpus;

	private Combo expandText;

	public CrossedPartitionPanel(Composite parent, int style, CQPCorpus corpus) {
		super(parent, style);

		this.corpus = corpus;

		GridLayout layout = new GridLayout(2, false);
		this.setLayout(layout);

		// StructuralUnit
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		List<StructuralUnit> structuralUnits = null;
		try {
			structuralUnits = corpus.getOrderedStructuralUnits();
		}
		catch (CqiClientException e) {
			Log.severe(TXMCoreMessages.bind(TXMCoreMessages.error_errorP0, Log.toString(e)));
		}
		structuralUnitsFinal = new ArrayList<>();
		for (StructuralUnit unit : structuralUnits) {
			if (unit.getName().equals("txmcorpus")) continue; // ignore txmcorpus structure //$NON-NLS-1$

			if (unit.getOrderedProperties() != null && unit.getOrderedProperties().size() != 0) {
				/*
				 * if ((unit.getName().equals("text") &&
				 * unit.getProperties().size() > 3)) //$NON-NLS-1$ {
				 * structuralUnitsCombo.add(unit.getName());
				 * structuralUnitsFinal.add(unit); } else
				 * if (!unit.getName().equals("text")) //$NON-NLS-1$ {
				 */
				structuralUnitsFinal.add(unit);
				// }
			}
		}

		// StructuralUnitProperty
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		Label propertyLabel = new Label(this, SWT.NONE);
		propertyLabel.setText(TXMUIMessages.Structures);
		propertyLabel.setLayoutData(new GridData(GridData.END, GridData.CENTER, false, false));
		propertyLabel.setToolTipText(TXMUIMessages.selectAtLeastTwoProperties);

		propertyCombo = new SuperListViewer(this, SWT.MULTI | SWT.V_SCROLL | SWT.BORDER);
		propertyCombo.setLabelProvider(new LabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof StructuralUnitProperty) {
					return "" + ((StructuralUnitProperty) element).getFullName(); //$NON-NLS-1$
				}
				else {
					return "" + element; //$NON-NLS-1$
				}
			}
		});
		GridData gdata = new GridData(GridData.FILL, GridData.FILL, true, true);
		gdata.heightHint = 400;
		propertyCombo.setLayoutData(gdata);

		ArrayList<StructuralUnitProperty> all = new ArrayList<>();
		for (StructuralUnit su : structuralUnitsFinal) {
			for (StructuralUnitProperty property : su.getOrderedProperties()) {
				if (!(su.getName().equals("text") &&  //$NON-NLS-1$
						(property.getName().equals("project") || property.getName().equals("base")))) //$NON-NLS-1$ //$NON-NLS-2$
					all.add(property);
			}
		}

		propertyCombo.getList().addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent event) {
				selectedStructuralUnitProperties = propertyCombo.getSelection().toList();
			}
		});

		propertyCombo.setInput(all);
		propertyCombo.setToolTipText(TXMUIMessages.selectAtLeastTwoProperties);

		Label separatorLabel = new Label(this, SWT.NONE);
		separatorLabel.setText(TXMUIMessages.valuesSeparator);
		separatorLabel.setLayoutData(new GridData(GridData.END, GridData.CENTER, false, false));
		separatorLabel.setToolTipText(TXMUIMessages.theSeparatorIsInsertedBetweenTheValues);

		separatorText = new Text(this, SWT.BORDER);
		separatorText.setText(" x "); //$NON-NLS-1$
		separatorText.setToolTipText(TXMUIMessages.theSeparatorIsInsertedBetweenTheValues);

		Label expandLabel = new Label(this, SWT.NONE);
		expandLabel.setText(TXMUIMessages.expandedStructure);
		expandLabel.setLayoutData(new GridData(GridData.END, GridData.CENTER, false, false));
		expandLabel.setToolTipText(TXMUIMessages.defineTheLimitStructures);

		expandText = new Combo(this, SWT.BORDER);

		String[] items = new String[structuralUnitsFinal.size()];
		for (int i = 0; i < structuralUnitsFinal.size(); i++) {
			items[i] = structuralUnitsFinal.get(i).getName();
		}
		expandText.setItems(items);

		expandText.setText(""); //$NON-NLS-1$
		expandText.setToolTipText(TXMUIMessages.defineTheLimitStructures);
	}

	/**
	 * Gets the structural unit property.
	 *
	 * @return the structural unit property
	 */
	public List<StructuralUnitProperty> getStructuralUnitProperties() {

		return selectedStructuralUnitProperties;
	}

	@Override
	public Partition createPartition(String name, Partition partition) {

		if (selectedStructuralUnitProperties.size() < 2) {
			return null;
		}

		String partNamePrefix = ""; //$NON-NLS-1$
		ArrayList<String> structuralUnits = new ArrayList<>();
		ArrayList<String> structuralUnitProperties = new ArrayList<>();
		for (StructuralUnitProperty sup : selectedStructuralUnitProperties) {
			structuralUnits.add(sup.getStructuralUnit().getName());
			structuralUnitProperties.add(sup.getName());
		}
		ArrayList<String> structuralUnitToIgnore = new ArrayList<>();
		String separator = separatorText.getText();
		String expandTarget = expandText.getText();

		PartitionQueriesGenerator pqg = new PartitionQueriesGenerator();
		try {
			partition = pqg.createPartition(corpus, partition, false,
					name, partNamePrefix, separator,
					structuralUnits, structuralUnitProperties,
					structuralUnitToIgnore, expandTarget);

			return partition;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * Create a list of queries and part names regarding the structural units, structural units properties, structural units to ignore user defined lists and expand target value specified.
	 * 
	 * @author mdecorde,sjaquot
	 *
	 */
	public class PartitionQueriesGenerator {

		public boolean DEBUG = false;					// si DEBUG, alors les requêtes sont affichées mais la partition n'est pas créée

		public String PARTITION_NAME = "";				// Nom de la partition (optionnel) //$NON-NLS-1$

		public List<String> STRUCTURAL_UNITS = new ArrayList<>();				// Liste des unités structurelles sur lesquelles effectuer la partition, ex: ['text', 'div1']

		public List<String> STRUCTURAL_UNITS_PROPERTIES = new ArrayList<>();	// Propriétés des unités structurelles sur lesquelles effectuer la partition, ex : ['id', 'name']

		public List<String> STRUCTURAL_UNITS_TO_IGNORE = new ArrayList<>();	// Structure à ignorer, ex. CQL : !speaker (optionnel)

		public String PART_NAMES_PREFIX = "";			// Prefix pour les noms de partie (optionnel) //$NON-NLS-1$

		public String EXPAND_TARGET = "";			// Expand to target, englobe les empans jusqu'à la balise parente spécifiée. //$NON-NLS-1$

		public String VALUES_SEPARATOR = "x"; //$NON-NLS-1$
		// NOTE : Le expand entre en conflit avec les sections à ignorer.
		// Si la target est à un niveau supérieur aux balises à ignorer, il les remet dans liste de résultats CWB et elles ne sont donc pas ignorées

		public List<String> queries = new ArrayList<>();

		public List<String> partNames = new ArrayList<>();

		/**
		 * Init the generator and process.
		 * 
		 * @throws CqiClientException
		 * @throws InterruptedException
		 */
		public Partition createPartition(CQPCorpus corpus, Partition partition, boolean debug,
				String partitionName, String partNamePrefix, String separator,
				List<String> structuralUnits, List<String> structuralUnitProperties,
				List<String> structuralUnitToIgnore, String expandTarget) throws CqiClientException, InterruptedException {

			PARTITION_NAME = partitionName;
			STRUCTURAL_UNITS = structuralUnits;
			STRUCTURAL_UNITS_PROPERTIES = structuralUnitProperties;
			STRUCTURAL_UNITS_TO_IGNORE = structuralUnitToIgnore;
			PART_NAMES_PREFIX = partNamePrefix;
			EXPAND_TARGET = expandTarget;
			VALUES_SEPARATOR = separator;
			DEBUG = debug;

			if (DEBUG) {
				System.out.println("Arguments: "); //$NON-NLS-1$
				System.out.println("PARTITION_NAME = $PARTITION_NAME"); //$NON-NLS-1$
				System.out.println("STRUCTURAL_UNITS = $STRUCTURAL_UNITS"); //$NON-NLS-1$
				System.out.println("STRUCTURAL_UNITS_PROPERTIES = $STRUCTURAL_UNITS_PROPERTIES"); //$NON-NLS-1$
				System.out.println("STRUCTURAL_UNITS_TO_IGNORE = $STRUCTURAL_UNITS_TO_IGNORE"); //$NON-NLS-1$
				System.out.println("PART_NAMES_PREFIX = $PART_NAMES_PREFIX"); //$NON-NLS-1$
				System.out.println("EXPAND_TARGET = $EXPAND_TARGET"); //$NON-NLS-1$
				System.out.println("DEBUG = $DEBUG"); //$NON-NLS-1$
			}

			if (STRUCTURAL_UNITS.size() > 0 && STRUCTURAL_UNITS.size() == STRUCTURAL_UNITS_PROPERTIES.size()) {

				if (DEBUG) System.out.println("Creating the queries on corpus " + corpus + "\""); //$NON-NLS-1$
				;
				if (DEBUG) System.out.println("PARTITION_NAME: " + PARTITION_NAME); //$NON-NLS-1$

				//Corpus corpus = CorpusManager.getCorpusManager().getCorpus(corpusName);

				// Recursing through the corpus and subcorpus
				process(corpus, 0, "", "", VALUES_SEPARATOR); //$NON-NLS-1$

				// Displaying the queries
				if (DEBUG) {
					System.out.println("Queries processed: "); //$NON-NLS-1$
					for (int i = 0; i < queries.size(); i++) {
						System.out.println(partNames.get(i) + " = " + queries.get(i)); //$NON-NLS-1$
					}
				}

				// Finalizing the queries
				finalizeQueries();

				// Displaying the queries
				if (DEBUG) {
					System.out.println("Queries finalized: "); //$NON-NLS-1$
					for (int i = 0; i < queries.size(); i++) {
						System.out.println(partNames.get(i) + " = " + queries.get(i)); //$NON-NLS-1$
					}
				}

				if (DEBUG) System.out.println("Queries created."); //$NON-NLS-1$

				// Creating the partition
				if (!DEBUG && queries.size() == partNames.size()) {
					if (partition == null) {
						partition = new Partition(corpus);
					}
					else {
						partition.removeAllParts();
					}
					partition.setParameters(PARTITION_NAME, queries, partNames);
					partition.compute();
					return partition;
				}
			}
			else {
				System.out.println("Error: Structural units count or structural units properties count error.");
				return null;
			}
			return null;
		}

		/**
		 * Recurse through structural units and structural units properties of corpus and create the queries and the part names.
		 * 
		 * @param corpus the corpus or subcorpus
		 * @param index the index for recursion
		 * @param tmpQuery the temporary query for creating subcorpus part
		 * @param tmpPartName the temporary part name of the subcorpus part
		 * @throws CqiClientException
		 */
		protected void process(CQPCorpus corpus, int index, String tmpQuery, String tmpPartName, String propertySeparator) throws CqiClientException {
			// End of array
			if (index >= STRUCTURAL_UNITS.size()) {

				queries.add(tmpQuery);
				partNames.add(PART_NAMES_PREFIX + tmpPartName);

				return;
			}

			StructuralUnit su = corpus.getStructuralUnit(STRUCTURAL_UNITS.get(index));
			StructuralUnitProperty sup = su.getProperty(STRUCTURAL_UNITS_PROPERTIES.get(index));

			if (DEBUG) {
				if (index == 0) {
					System.out.println("Pocessing Structural Unit Property " + sup.getFullName() + " on mother corpus " + corpus.getID()); //$NON-NLS-1$
				}
				else {
					System.out.println("Pocessing Structural Unit Property " + sup.getFullName() + " on subcorpus part " + tmpPartName); //$NON-NLS-1$
				}
			}


			// Creating the queries parts for each structural units properties values
			// for (supValue in sup.getOrderedValues()) { // TODO : signaler bug Matthieu, on ne devrait pas être obligé de repasser le sous-corpus à la méthode car sup a déjà été créée depuis le sous-corpus ? getValues() bugge aussi
			for (String supValue : sup.getOrderedValues(corpus)) {

				if (DEBUG) System.out.println("Value " + supValue); //$NON-NLS-1$

				// Getting the subcorpus linked to the structural unit property value
				Subcorpus tmpSubcorpus = corpus.createSubcorpusWithQueryString(su, sup, supValue, "tmp" + UUID.randomUUID()); //$NON-NLS-1$

				// Partition conditions and part name separators
				String and = "";
				String underscore = "";
				if (tmpQuery != "") {
					underscore = propertySeparator;
					and = " & ";
				}

				process(tmpSubcorpus, index + 1, (tmpQuery + and + "_." + sup.getFullName() + "=\"" + supValue + "\""), tmpPartName + underscore + supValue, propertySeparator); //$NON-NLS-1$

				// Deleting the temporary subcorpus
				// TODO : bug : cette méthode ne supprime pas le corpus sans doute car il faut que le sous-corpus ne contienne pas d'autres sous-corpus ? le delete() en revanche fonctionne.
				//			corpus.dropSubcorpus(tmpSubcorpus);
				tmpSubcorpus.delete();
			}
		}


		/**
		 * Autoname the partition.
		 * 
		 * @param partitionName
		 */
		protected void autoNamePartition(String partitionName) {

			// Structural units names and properties
			for (int i = 0; i < STRUCTURAL_UNITS.size(); i++) {
				partitionName += STRUCTURAL_UNITS.get(i) + '_' + STRUCTURAL_UNITS_PROPERTIES.get(i) + VALUES_SEPARATOR;
			}

			// Structural units to ignore
			for (int i = 0; i < STRUCTURAL_UNITS_TO_IGNORE.size(); i++) {
				partitionName += "NOT_" + STRUCTURAL_UNITS_TO_IGNORE.get(i) + '.';
			}

			// Removing last 'x' in partition name
			PARTITION_NAME = partitionName.substring(0, partitionName.length() - 3);
		}

		/**
		 * Finalize the queries.
		 */
		protected void finalizeQueries() {

			String expandTo = ""; //$NON-NLS-1$
			// Expanding to user defined target
			if (EXPAND_TARGET != null && EXPAND_TARGET.length() > 0) {
				expandTo = " expand to " + EXPAND_TARGET;
			}
			// Expanding to last child structural unit in user defined hierarchy
			else if (STRUCTURAL_UNITS_TO_IGNORE.size() == 0) {
				expandTo = " expand to " + STRUCTURAL_UNITS.get(STRUCTURAL_UNITS.size() - 1); //$NON-NLS-1$
			}

			// Autonaming the partition
			if (PARTITION_NAME != null && PARTITION_NAME.length() == 0) {
				autoNamePartition(PARTITION_NAME);
				// Finalizing partition name
				if (EXPAND_TARGET != null && EXPAND_TARGET.length() > 0)
					PARTITION_NAME += expandTo.replace(" expand to", ".EXPAND TO").replace(" ", "_"); //$NON-NLS-1$
			}

			// Finalizing queries
			for (int j = 0; j < queries.size(); j++) {

				String queryEnd = ""; //$NON-NLS-1$

				// Removing some sections
				for (String sectionToIgnore : STRUCTURAL_UNITS_TO_IGNORE) {
					queryEnd += " & !" + sectionToIgnore; //$NON-NLS-1$
				}

				queryEnd += ']' + expandTo; //$NON-NLS-1$
				queries.set(j, '[' + queries.get(j) + queryEnd); //$NON-NLS-1$
			}
		}
	}
}
