// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.swt.widget;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.dialog.QueryAssistDialog;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.SearchEngine;
import org.txm.searchengine.core.SearchEnginesManager;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

// TODO: Auto-generated Javadoc
/**
 * the QueryWidget plus the button to open the QueryAssisDialog @ author
 * mdecorde.
 */
public class AssistedQueryWidget extends Composite {

	protected SearchEngine se;

	/** The magicstick button: opens the QueryAssist dialog. */
	protected Button magicstick;

	/** The querywidget. */
	protected QueryWidget querywidget;

	private QueryAssistDialog d;

	private KeyListener listener;

	/**
	 * Instantiates a new assisted query widget.
	 *
	 * @param parent the parent
	 * @param style the style
	 * @param corpus the corpus
	 */
	public AssistedQueryWidget(Composite parent, int style, final CQPCorpus corpus, SearchEngine se) {
		super(parent, SWT.NONE);
		this.setLayout(GLComposite.createDefaultLayout(5));

		this.se = se;

		magicstick = new Button(this, SWT.PUSH);
		magicstick.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));
		magicstick.setImage(IImageKeys.getImage(IImageKeys.ACTION_ASSISTQUERY));
		magicstick.setToolTipText(TXMUIMessages.openTheQueryAssistant);
		magicstick.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				d = new QueryAssistDialog(e.display.getActiveShell(), corpus);
				if (d.open() == Window.OK) {
					querywidget.setText(d.getQuery());
					Event ev = new Event();
					ev.widget = magicstick;
					KeyEvent ke = new KeyEvent(ev);
					ke.keyCode = SWT.CR;
					listener.keyPressed(ke);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		querywidget = new QueryWidget(this, style, corpus.getProject(), se);
		querywidget.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
	}

	public AssistedQueryWidget(Composite parent, int style, CQPCorpus corpus) {
		this(parent, style, corpus, SearchEnginesManager.getCQPSearchEngine());
	}

	@Override
	public boolean setFocus() {
		return this.querywidget.setFocus();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Control#addKeyListener(org.eclipse.swt.events.KeyListener)
	 */
	@Override
	public void addKeyListener(KeyListener listener) {
		this.listener = listener;
		if (querywidget != null)
			this.querywidget.addKeyListener(listener);
	}

	/**
	 * Gets the query string.
	 *
	 * @return the query string
	 */
	public String getQueryString() {
		return querywidget.getQueryString();
	}

	/**
	 * Gets the query string.
	 *
	 * @return the query string
	 */
	public String getRawString() {
		return querywidget.getText();
	}

	/**
	 * Memorize.
	 */
	public void memorize() {
		querywidget.memorize(getQuery());
	}

//	/**
//	 * Memorize.
//	 *
//	 * @param query the query
//	 */
//	public void memorize(String query) {
//		querywidget.memorize(query);
//	}

	/**
	 * Clears the query test.
	 */
	public void clearQuery() {
		this.querywidget.setText(""); //$NON-NLS-1$
	}

	/**
	 * Sets the text.
	 *
	 * @param query the new text
	 */
	public void setText(String query) {

		// if(query.isEmpty()) {
		// query = ""; //$NON-NLS-1$
		// }

		// // fix the query
		// query = query.replaceAll("^\"(.*)\"$","$1"); //$NON-NLS-1$

		querywidget.setText(query);
	}

	public IQuery getQuery() {
		return this.querywidget.getQuery();
	}

	/**
	 * @return the querywidget
	 */
	public QueryWidget getQueryWidget() {
		return querywidget;
	}
}
