package org.txm.rcp.swt.dialog;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

/**
 * Dialog to set HashMap values given a HashMap pre-filled with values to edit
 * 
 * @author mdecorde
 *
 */
public class IntegerDialog extends Dialog {

	String title;
	
	Integer value = 0;
	Integer minimum = 0;
	Integer maximum = 100;

	private Spinner spinner;

	/**
	 * 
	 * @param parentShell
	 * @param title
	 * @param values values to edit, value may be null
	 */
	public IntegerDialog(Shell parentShell, String title, Integer defaultValue, Integer min, Integer max) {

		super(parentShell);

		this.title = title;

		if (defaultValue != null) {
			this.value = defaultValue;
		}
		
		if (min != null) {
			this.minimum = min;
		}
		
		if (max != null) {
			this.maximum = max;
		}
	}

	@Override
	protected void configureShell(Shell newShell) {

		super.configureShell(newShell);
		newShell.setText(title);
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		parent.setLayout(new GridLayout(2, false));

			Label l = new Label(parent, SWT.NONE);
			l.setText(title);
			l.setLayoutData(new GridData());

			spinner = new Spinner(parent, SWT.BORDER);
			spinner.setSelection(value);
			spinner.setMinimum(minimum);
			spinner.setMaximum(maximum);
		return parent;
	}

	@Override
	protected void okPressed() {

		value = spinner.getSelection();

		super.okPressed();
	}

	/**
	 * 
	 * @return the value set
	 */
	public Integer getValue() {

		return value;
	}
}
