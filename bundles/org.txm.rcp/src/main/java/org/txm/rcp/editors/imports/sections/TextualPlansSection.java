package org.txm.rcp.editors.imports.sections;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.txm.objects.Project;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.txm.rcp.messages.TXMUIMessages;

public class TextualPlansSection extends ImportEditorSection {

	private static final int SECTION_SIZE = 1;

	private Text outsideTextElementsText;

	private Text outsideTextElementsToEditText;

	public TextualPlansSection(ImportFormEditor editor, FormToolkit toolkit2, ScrolledForm form2, Composite parent, int style) {
		super(editor, toolkit2, form2, parent, style, "textualplans"); //$NON-NLS-1$

		this.section.setText(TXMUIMessages.textualPlanes);
		TableWrapLayout layout = new TableWrapLayout();
		layout.makeColumnsEqualWidth = true;
		this.section.setLayout(layout);
		this.section.setLayoutData(getSectionGridData(SECTION_SIZE));
		this.section.setEnabled(false);
		this.section.addExpansionListener(new ExpansionAdapter() {

			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.layout(true);
			}
		});

		Composite sectionClient = toolkit.createComposite(this.section);
		TableWrapLayout slayout = new TableWrapLayout();
		slayout.makeColumnsEqualWidth = false;
		slayout.numColumns = 2;
		sectionClient.setLayout(slayout);
		this.section.setClient(sectionClient);

		Label tmpLabel = toolkit.createLabel(sectionClient, TXMUIMessages.outsidetext);
		tmpLabel.setToolTipText(TXMUIMessages.listOfTagsEncodingTheOutsidetextNoStructureOrWordContentIndexedNoStructureOrWordContentEditedInTXM);
		tmpLabel.setLayoutData(getLabelGridData());

		outsideTextElementsText = toolkit.createText(sectionClient, "teiHeader", SWT.BORDER); //$NON-NLS-1$
		TableWrapData gdata = getTextGridData();
		outsideTextElementsText.setLayoutData(gdata);

		Label tmpLabel2 = toolkit.createLabel(sectionClient, TXMUIMessages.outsidetextToedit);
		tmpLabel2.setToolTipText(TXMUIMessages.listOfTagsEncodingTheOutsidetextToEditNoStructureOrWordContentIndexedStructureAndWordContentEditedInTXM);
		tmpLabel2.setLayoutData(getLabelGridData());

		outsideTextElementsToEditText = toolkit.createText(sectionClient, "", SWT.BORDER); //$NON-NLS-1$
		gdata = getTextGridData();
		outsideTextElementsToEditText.setLayoutData(gdata);

	}

	@Override
	public void updateFields(Project project) {
		if (this.section != null && !section.isDisposed()) {
			outsideTextElementsText.setText(project.getTextualPlan("OutSideTextTags", "teiHeader")); //$NON-NLS-1$ //$NON-NLS-2$
			outsideTextElementsToEditText.setText(project.getTextualPlan("OutSideTextTagsAndKeepContent")); //$NON-NLS-1$
		}
	}

	@Override
	public boolean saveFields(Project project) {
		if (this.section != null && !section.isDisposed()) {

			project.setTextualPlan("OutSideTextTags", outsideTextElementsText.getText().trim()); //$NON-NLS-1$

			project.setTextualPlan("OutSideTextTagsAndKeepContent", outsideTextElementsToEditText.getText().trim()); //$NON-NLS-1$

			return true;
		}

		return true;
	}

	@Override
	public boolean checkFields() {
		return true;
	}

	@Override
	public int getSectionSize() {
		return SECTION_SIZE;
	}
}
