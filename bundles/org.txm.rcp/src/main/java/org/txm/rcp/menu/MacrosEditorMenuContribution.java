package org.txm.rcp.menu;

import java.io.File;

import org.txm.Toolbox;

public class MacrosEditorMenuContribution extends MacrosMenuContribution {

	public MacrosEditorMenuContribution() {
		super();
	}

	public MacrosEditorMenuContribution(String id) {
		super(id);
	}

	public File getMacroDirectory() {
		//create the menu item
		String w = Toolbox.getTxmHomePath();
		if (w == null || w.length() == 0) return null;

		return new File(w, "scripts/groovy/user/org/txm/macro/ui/menu/object"); //$NON-NLS-1$
	}
}
