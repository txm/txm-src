// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views.fileexplorer;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.StyledString.Styler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.TextStyle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.txm.rcp.Application;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.swt.TXMFontRegistry;

// TODO: Auto-generated Javadoc
/**
 * The Class FileTreeLabelProvider.
 */
public class MacroTreeLabelProvider extends LabelProvider {


	private MacroExplorer explorer;

	private Font font;

	public MacroTreeLabelProvider(MacroExplorer explorer) {
		this.explorer = explorer;

		Font font = Display.getCurrent().getSystemFont();
		FontData fontData = font.getFontData()[0];
		fontData.setStyle(SWT.ITALIC);
		this.font = TXMFontRegistry.getFont(Display.getCurrent(), fontData);
	}

	/** The image table. */
	private Map<ImageDescriptor, Image> imageTable = new HashMap<>(7);

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object element) {

		File f = (File) element;
		String name = f.getName();
		if (f.isDirectory()) {
			return name;
		}
		else {
			int idx = name.indexOf("Macro.groovy"); //$NON-NLS-1$
			if (idx > 0) return name.substring(0, idx);

			return name;
		}
	}

	@Override
	public void dispose() {
		if (imageTable != null) {
			for (ImageDescriptor k : imageTable.keySet()) {
				imageTable.get(k).dispose();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
	 */
	@Override
	public final Image getImage(Object element) {

		File f = ((File) element);
		ImageDescriptor imageDescriptor = null;
		if (f.isFile()) {
			if (f.getName().endsWith("Macro.groovy")) { //$NON-NLS-1$
				imageDescriptor = AbstractUIPlugin
						.imageDescriptorFromPlugin(Application.PLUGIN_ID,
								IImageKeys.SCRIPT_RUN);
			}
			else if (f.getName().endsWith(".groovy")) { //$NON-NLS-1$
				imageDescriptor = AbstractUIPlugin
						.imageDescriptorFromPlugin(Application.PLUGIN_ID,
								IImageKeys.SCRIPT);
			}
			else {
				imageDescriptor = AbstractUIPlugin
						.imageDescriptorFromPlugin(Application.PLUGIN_ID,
								IImageKeys.FILE);
			}
		}
		else if (f.isDirectory()) {
			imageDescriptor = AbstractUIPlugin.imageDescriptorFromPlugin(
					Application.PLUGIN_ID, IImageKeys.FOLDER);
		}

		Image image = imageTable.get(imageDescriptor);
		if (image == null) {
			image = imageDescriptor.createImage();
			imageTable.put(imageDescriptor, image);
		}
		return image;
	}

	public StyledString getStyledText(Object element) {

		// if (!explorer.isUserScriptExists(element)) return new StyledString(getText(element), BOLD_FONT_STYLER);

		return new StyledString(getText(element));
	}

	Styler BOLD_FONT_STYLER = new BoldFontStyler();

	public class BoldFontStyler extends Styler {

		@Override
		public void applyStyles(final TextStyle textStyle) {
			textStyle.font = font;
		}
	}
}
