package org.txm.rcp.swt.provider;

import java.util.List;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

public class ListLabelProvider extends LabelProvider implements ITableLabelProvider {

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
	 * 
	 * @param element
	 * @param columnIndex
	 */
	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		if (element != null && element instanceof List) {
			List<Object> list = (List<Object>) element;
			if (columnIndex < list.size())
				return "" + list.get(columnIndex); //$NON-NLS-1$
			return "#idx too high"; //$NON-NLS-1$
		}
		return "#not list"; //$NON-NLS-1$
	}
}
