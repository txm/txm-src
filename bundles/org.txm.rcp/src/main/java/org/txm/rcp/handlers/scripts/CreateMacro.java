// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.scripts;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.Toolbox;
import org.txm.rcp.handlers.files.EditFile;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.views.fileexplorer.Explorer;
import org.txm.rcp.views.fileexplorer.MacroExplorer;
import org.txm.utils.AsciiUtils;
import org.txm.utils.io.IOUtils;

// TODO: Auto-generated Javadoc
/**
 * Handler: creates a macro template
 */
public class CreateMacro extends AbstractHandler {

	private static final String ID = "org.txm.rcp.commands.CreateMacro"; //$NON-NLS-1$
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		// get scripts dir
		String scriptRootDir = Toolbox.getTxmHomePath() + "/scripts"; //$NON-NLS-1$
		File currentRootDir = new File(scriptRootDir, "macro"); //$NON-NLS-1$

		// if selection is a file, get its path
		String dir = null;
		IWorkbenchWindow activeW = HandlerUtil.getActiveWorkbenchWindow(event);
		if (activeW != null) {
			IWorkbenchPage activeP = activeW.getActivePage();
			if (activeP != null) {
				ISelection selection = activeP.getSelection();
				if (selection != null && selection instanceof IStructuredSelection) {
					IStructuredSelection sel = (IStructuredSelection) selection;
					Object obj = sel.getFirstElement();
					if (obj instanceof File) {
						File current = ((File) obj);
						if (current.isDirectory()) {
							dir = current.getAbsolutePath();
						}
						else {
							dir = current.getParent();
						}
					}
				}
			}
		}

		if (dir == null) {
			dir = currentRootDir.getAbsolutePath();
		}

		// ask new file name
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		InputDialog dialog = new InputDialog(shell, TXMUIMessages.macroName, TXMUIMessages.pleaseWriteTheMacroName, "", null); //$NON-NLS-1$

		if (dialog.open() == Window.OK) {
			return createMacro(dir, dialog.getValue());
		}
		return null;
	}

	public static File createMacro(String dir, String name) {
		if (name == null) return null;
		if (dir == null) return null;

		String rootDirPath = "scripts/groovy/user/org/txm/macro/";
		int idx = dir.indexOf(rootDirPath); //$NON-NLS-1$
		String packagePath = "org.txm.macro"; //$NON-NLS-1$
		if (idx > 0) {
			String sub = dir.substring(idx + rootDirPath.length(), dir.length()).replaceAll("/", ".").replaceAll("\\\\", "."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			if (sub.trim().length() > 0) {
				packagePath += "." + sub; //$NON-NLS-1$
			}
		}
		name = name.replaceAll("\\p{Punct}", ""); //$NON-NLS-1$ //$NON-NLS-2$
		name = AsciiUtils.convertNonAscii(name);
		name = AsciiUtils.removePunct(name);
		name = name.substring(0, 1).toUpperCase() + name.substring(1);
		StringBuffer tmp = new StringBuffer();
		for (int i = 0; i < name.length(); i++) {
			if (name.charAt(i) == ' ') {
				i++;
				if (i < name.length())
					tmp.append(("" + name.charAt(i)).toUpperCase()); //$NON-NLS-1$
			}
			else {
				tmp.append(name.charAt(i));
			}
		}
		name = tmp.toString();
		if (name == null) return null;

		// create the new file
		File newfile = new File(dir, name + "Macro.groovy"); //$NON-NLS-1$
		LastOpened.set(ID, newfile.getParent(), newfile.getName());
		if (!newfile.exists() && newfile.getParentFile().canWrite()) {
			try {
				if (newfile.createNewFile()) {
					System.out.println(NLS.bind(TXMUIMessages.newFileColonP0, newfile.getAbsolutePath()));

					PrintWriter writer = IOUtils.getWriter(newfile, "UTF-8"); //$NON-NLS-1$

					writer.println("// Copyright © " + Calendar.getInstance().get(Calendar.YEAR) + " MY_INSTITUTION"); //$NON-NLS-1$ //$NON-NLS-2$
					writer.println("// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)"); //$NON-NLS-1$
					writer.println("// @author " + System.getProperty("user.name")); //$NON-NLS-1$ //$NON-NLS-2$

					writer.println(""); //$NON-NLS-1$
					writer.println("// STANDARD DECLARATIONS"); //$NON-NLS-1$
					writer.println("package " + packagePath); //$NON-NLS-1$

					writer.println(""); //$NON-NLS-1$
					writer.println("import org.kohsuke.args4j.*"); //$NON-NLS-1$
					writer.println("import groovy.transform.Field"); //$NON-NLS-1$
					writer.println("import org.txm.rcp.swt.widget.parameters.*"); //$NON-NLS-1$

					writer.println(""); //$NON-NLS-1$
					writer.println("// BEGINNING OF PARAMETERS"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println("// Declare each parameter here"); //$NON-NLS-1$
					writer.println("// (available widget types: Query, File, Folder, String, Text, Boolean, Integer, Float and Date)"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println("//@Field @Option(name=\"query\", usage=\"an example query\", widget=\"Query\", required=true, def='[pos=\"V.*\"]')"); //$NON-NLS-1$
					writer.println("//def query"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println("//@Field @Option(name=\"inputFile\", usage=\"an example file\", widget=\"FileOpen\", required=true, def=\"\")"); //$NON-NLS-1$
					writer.println("//def inputFile"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println("//@Field @Option(name=\"outputFile\", usage=\"an example file\", widget=\"FileSave\", required=true, def=\"\")"); //$NON-NLS-1$
					writer.println("//def outputFile"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println("//@Field @Option(name=\"folder\", usage=\"an example folder\", widget=\"Folder\", required=false, def=\"\")"); //$NON-NLS-1$
					writer.println("//def folder"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println("//@Field @Option(name=\"date\", usage=\"an example date\", widget=\"Date\", required=false, def=\"1984-09-01\")"); //$NON-NLS-1$
					writer.println("//def date"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println("//@Field @Option(name=\"integer\", usage=\"an example integer\", widget=\"Integer\", required=false, def=\"42\")"); //$NON-NLS-1$
					writer.println("//def integer"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println("//@Field @Option(name=\"floatValue\", usage=\"an example float\", widget=\"Float\", required=false, def=\"42.42\")"); //$NON-NLS-1$
					writer.println("//def floatValue"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println("//@Field @Option(name=\"string\", usage=\"an example string\", widget=\"String\", required=false, def=\"hello world!\")"); //$NON-NLS-1$
					writer.println("//def string"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println("//@Field @Option(name=\"separator\", usage=\"an example separator\", widget=\"Separator\", required=false, def=\"Separator name\")"); //$NON-NLS-1$
					writer.println("//def separator"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println("//@Field @Option(name=\"longString\", usage=\"an example longString\", widget=\"Text\", required=false, def=\"hello world!\")"); //$NON-NLS-1$
					writer.println("//def longString"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println(
							"//@Field @Option(name=\"arraySelection\", usage=\"an example single value selection in a list\", widget=\"StringArray\", metaVar=\"first	second	third\", required=false, def=\"second\")"); //$NON-NLS-1$
					writer.println("//def arraySelection"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println(
							"//@Field @Option(name=\"multipleArraySelection\", usage=\"an example multiple value selection in a list\", widget=\"StringArrayMultiple\", metaVar=\"first	second	third\", required=false, def=\"second\")"); //$NON-NLS-1$
					writer.println("//def multipleArraySelection"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println("//@Field @Option(name=\"password\", usage=\"an example password\", widget=\"Password\", required=false, def=\"hello world!\")"); //$NON-NLS-1$
					writer.println("//def password"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println("//@Field @Option(name=\"bool\", usage=\"an example boolean\", widget=\"Boolean\", required=false, def=\"true\")"); //$NON-NLS-1$
					writer.println("//def bool"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println("// Open the parameters input dialog box"); //$NON-NLS-1$
					writer.println("if (!ParametersDialog.open(this)) return"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println("// END OF PARAMETERS"); //$NON-NLS-1$
					writer.println(""); //$NON-NLS-1$
					writer.println("println \"corpora selection: \"+corpusViewSelection"); //$NON-NLS-1$
					writer.close();

					EditFile.openfile(newfile);
					Explorer.refresh(newfile);
					MacroExplorer.refresh(newfile);

					return newfile;
				}
			}
			catch (IOException e) {
				System.out.println(e.getLocalizedMessage());
				return null;
			}
		}
		return null;
	}
}
