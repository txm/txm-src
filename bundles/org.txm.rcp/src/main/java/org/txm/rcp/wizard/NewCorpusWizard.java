package org.txm.rcp.wizard;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;

public class NewCorpusWizard extends Wizard implements INewWizard {

	WizardNewProjectCreationPage page;

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		page = new WizardNewProjectCreationPage("Corpus TXM"); //$NON-NLS-1$
		page.setTitle("Corpus TXM"); //$NON-NLS-1$
		page.setDescription("select the directory containing the source files"); //$NON-NLS-1$
		addPage(page);
	}

	@Override
	public boolean performFinish() {
		return page.isPageComplete();
	}
}
