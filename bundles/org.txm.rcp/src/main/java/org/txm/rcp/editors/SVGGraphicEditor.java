// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//

package org.txm.rcp.editors;

import java.io.File;
import java.net.MalformedURLException;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
//import org.txm.chartsengine.core.results.ChartResult;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.svg.SVGComposite;
import org.txm.utils.logger.Log;

/**
 * Test the use of the SVGComposite view from Batik_UI_Plugin Show a SVG file in
 * a single panel.
 *
 * @author Sylvain Loiseau
 */
public class SVGGraphicEditor extends EditorPart {

	/** Linking with the OutlineView. */
	// private OverviewOutlinePage overviewOutlinePage;

	private SVGComposite svgComp;

	/** The svg path. */
	private IPath svgPath;

	/** The svgfile displayed in the editor */
	private File svgfile;



	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		this.setSite(site);
		this.setInput(input);
		this.setPartName(input.getName());
	}


	@Override
	public void createPartControl(Composite parent) {

		svgComp = new SVGComposite(parent, SWT.EMBEDDED | SWT.NO_BACKGROUND);
		if (svgPath == null) {
			if (this.getEditorInput() instanceof SVGGraphicable) {
				//svgPath = ((SVGGraphicable) input).getIPath().toFile().toURI().toURL().toString();
				try {
					svgComp.setSVGFile(((SVGGraphicable) this.getEditorInput()).getIPath().toFile().toURI().toURL());
					svgfile = ((SVGGraphicable) this.getEditorInput()).getIPath().toFile();
				}
				catch (MalformedURLException e) {
					Log.printStackTrace(e);
				}

			}
			else {
				Log.severe(TXMUIMessages.cannotDrawAGraphicWithTheInformationProvided);
				return;
			}
		}
		svgComp.resetZoom();
		createContextMenu();
	}

	/**
	 * Sets the focus.
	 *
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		svgComp.setFocus();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#dispose()
	 */
	@Override
	public void dispose() {
		svgComp.dispose();
		super.dispose();
	}

	/**
	 * Gets the sVG file.
	 *
	 * @return the sVG file
	 */
	public File getSVGFile() {
		return svgfile;
	}

	/**
	 * Creates the context menu.
	 */
	private void createContextMenu() {
		/*
		 * Menu menu = new Menu(parent.getShell()); MenuItem item = new MenuItem
		 * (menu, SWT.PUSH); item.setText ("Popup"); svgComp.setMenu (menu);
		 */

		/*
		 * // Set the MenuManager getSite().registerContextMenu(menuManager,
		 * svgComp); // Make the selection available
		 * getSite().setSelectionProvider(svgComp);
		 */
	}

	// TODO : check the good one.
	// public Object getAdapter(Class type) {
	// if (type == IContentOutlinePage.class) {
	// return getOverviewOutlinePage();
	// } else if (type.getClass().getName().equals("IContentOutlinePage")) {
	// return getOverviewOutlinePage();
	// } else if (type.getClass().equals(IContentOutlinePage.class.getClass()))
	// {
	// return getOverviewOutlinePage();
	// }
	// return super.getAdapter(type);
	// }
	//
	// private OverviewOutlinePage getOverviewOutlinePage() {
	// if (null == overviewOutlinePage) {
	// overviewOutlinePage = new OverviewOutlinePage();
	// }
	// return overviewOutlinePage;
	// }

	/**
	 * Reset view.
	 */
	public void resetView() {
		svgComp.resetZoom();
	}
	//
	//	@Override
	//	public void updateResultFromEditor() {
	//
	//	}
	//
	//	@Override
	//	public void updateEditorFromResult(boolean update) {
	//		// TODO implement a SVGResult extends TXMResult ?		
	////		svgFile = getResultData().getSVGFile();
	////		svgComp.setSVGFile(svgfile);
	//		svgComp.redraw();
	//	}


	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO Auto-generated method stub

	}


	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}


	@Override
	public boolean isDirty() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}
}
