package org.txm.rcp;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.AbstractSourceProvider;
import org.txm.utils.logger.Log;

public class CorporaSourceProvider extends AbstractSourceProvider {

	public static final String NAME = "corporaSelection"; //$NON-NLS-1$

	public static final String[] NAMES = { NAME };

	HashMap<String, ISelection> currentState = new HashMap<String, ISelection>();

	public static CorporaSourceProvider instance = null;

	public CorporaSourceProvider() {

		StructuredSelection selection = new StructuredSelection();
		currentState.put(NAME, selection);
		fireSourceChanged(1, NAME, selection);

		instance = this;
	}

	public static void updateSelection(ISelection selection) {

		if (instance != null) {
			instance.currentState.put(NAME, selection);
			instance.fireSourceChanged(1, NAME, selection);
			Log.finest("UPDATE SOURCE: " + selection); //$NON-NLS-1$
		}
		else {
			selection = new StructuredSelection();
			instance.currentState.put(NAME, selection);
			instance.fireSourceChanged(1, NAME, selection);
		}
	}

	@Override
	public void dispose() {

	}

	@Override
	public Map<String, ISelection> getCurrentState() {

		return currentState;
	}

	//	// Selection SourceProvider
	//
	//	ISelection selection;
	//	private IWorkbench workbench;
	//	
	//	@Override
	//	public void initialize(IServiceLocator locator) {
	////		this.locator = locator;
	//		super.initialize(locator);
	//		IWorkbenchLocationService wls = locator.getService(IWorkbenchLocationService.class);
	//		workbench = wls.getWorkbench();
	//	}
	//	
	//	private IWorkbenchWindow getActiveWindow() {
	//		final Shell newActiveShell = workbench.getDisplay().getActiveShell();
	//		final IContextService contextService = workbench.getService(IContextService.class);
	//		if (contextService != null) {
	//			final int shellType = contextService.getShellType(newActiveShell);
	//			if (shellType != IContextService.TYPE_DIALOG) {
	//				return workbench.getActiveWorkbenchWindow();
	//			}
	//		}
	//		return null;
	//	}


	@Override
	public String[] getProvidedSourceNames() {

		return NAMES;
	}

}
