// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.preferences;

import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.rcp.swt.widget.preferences.StringFieldEditor;
import org.txm.rcp.views.fileexplorer.Explorer;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportPreferencePage.
 */
public class FileExplorerPreferencePage extends RCPPreferencesPage {


	/** The show_export. */
	private StringFieldEditor files_to_hide;

	private BooleanFieldEditor show_hidden_files;
	//private BooleanFieldEditor show_folders;

	/** The preferences. */
	private ScopedPreferenceStore preferences;

	/**
	 * Instantiates a new export preference page.
	 */
	public FileExplorerPreferencePage() {
		super();
		setTitle(TXMUIMessages.exportSettings);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 */
	@Override
	protected void createFieldEditors() {

		show_hidden_files = new BooleanFieldEditor(RCPPreferences.SHOW_HIDDEN_FILES, TXMUIMessages.showHiddenFiles, getFieldEditorParent());
		show_hidden_files.setToolTipText(TXMUIMessages.showHiddenFilesOS);
		addField(show_hidden_files);
		
		files_to_hide = new StringFieldEditor(RCPPreferences.FILES_TO_HIDE, TXMUIMessages.regexpPatternToHideFiles, getFieldEditorParent());
		files_to_hide.setToolTipText(TXMUIMessages.regexpPatternToFilterFilesInTheExplorerView);
		addField(files_to_hide);

		//show_folders = new BooleanFieldEditor(SHOW_FOLDERS, Messages.FileExplorerPreferencePage_2, getFieldEditorParent());
		//addField(show_folders);
		RCPPreferences.getInstance().put(RCPPreferences.SHOW_FOLDERS, true);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#performOk()
	 */
	@Override
	public boolean performOk() {
		super.performOk();
		Explorer.refresh();
		return true;
	}
}
