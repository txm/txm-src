package org.txm.rcp.editors.imports.sections;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.txm.Toolbox;
import org.txm.core.engines.Engine;
import org.txm.core.engines.EngineType;
import org.txm.core.engines.EnginesManager;
import org.txm.nlp.core.NLPEngine;
import org.txm.objects.Project;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.treetagger.core.preferences.TreeTaggerPreferences;
import org.txm.utils.FileUtils;
import org.txm.utils.io.IOUtils;

public class LangSection extends ImportEditorSection {

	private static final int SECTION_SIZE = 1;

	Button btnDefaultLang;

	Button btnGuessLang;

	Button btnSelectAnnotate;

	Combo annotateEngineCombo;

	Button btnSelectLang;

	Combo langcombo;

	public LangSection(ImportFormEditor editor, FormToolkit toolkit2, ScrolledForm form2, Composite parent, int style) {
		super(editor, toolkit2, form2, parent, style, "lang"); //$NON-NLS-1$

		this.section.setText(TXMUIMessages.mainLanguage);
		TableWrapLayout layout = new TableWrapLayout();
		layout.makeColumnsEqualWidth = true;
		this.section.setLayout(layout);
		this.section.setLayoutData(getSectionGridData(SECTION_SIZE));
		this.section.setEnabled(false);

		this.section.addExpansionListener(new ExpansionAdapter() {

			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.layout(true);
			}
		});

		// filesection.setDescription("Select how to find source files");
		final Composite sectionClient = toolkit.createComposite(this.section);
		TableWrapLayout slayout = new TableWrapLayout();
		slayout.numColumns = 2;
		slayout.makeColumnsEqualWidth = false;
		sectionClient.setLayout(slayout);
		this.section.setClient(sectionClient);

		Label l = toolkit.createLabel(sectionClient, TXMUIMessages.textLanguage); //$NON-NLS-1$
		TableWrapData gdata = getTextGridData();
		gdata.colspan = 2;
		l.setLayoutData(gdata);
		// if widget selected change the radio buttons value

		btnGuessLang = toolkit.createButton(sectionClient, TXMUIMessages.guessColon, SWT.RADIO);
		gdata = getTextGridData();
		gdata.indent = 20;
		btnGuessLang.setLayoutData(gdata);
		toolkit.createLabel(sectionClient, ""); //$NON-NLS-1$

		btnSelectLang = toolkit.createButton(sectionClient, TXMUIMessages.selectColon, SWT.RADIO);
		gdata = getTextGridData();
		gdata.indent = 20;
		btnSelectLang.setLayoutData(gdata);

		langcombo = new Combo(sectionClient, SWT.BORDER);
		gdata = getTextGridData();
		langcombo.setLayoutData(gdata);
		langcombo.setToolTipText(TXMUIMessages.CommonSourcesLangOfTreeTaggerModels);
		// if widget selected change the radio buttons value
		langcombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				btnSelectLang.setSelection(true);
				btnGuessLang.setSelection(false);
			}
		});

		String locale = Locale.getDefault().getLanguage().toLowerCase(); // default lang
		String[] langs = getAvailableLangs(); // get available langs from treetagger models if any
		Arrays.sort(langs); // sort langs
		int langToSelectIndex = Arrays.binarySearch(langs, locale);
		langcombo.setItems(langs);
		if (langToSelectIndex >= 0) {
			langcombo.select(langToSelectIndex);
		}

		btnSelectAnnotate = toolkit.createButton(sectionClient, TXMUIMessages.lemmatizeTheCorpus, SWT.CHECK);
		toolkit.createLabel(sectionClient, ""); //$NON-NLS-1$

		EnginesManager<? extends Engine> ema = Toolbox.getEngineManager(EngineType.NLP);
		ArrayList<String> engines = new ArrayList<>();
		for (String engine : ema.getEngines().keySet()) {
			NLPEngine e = (NLPEngine) ema.getEngine(engine);
			engines.add(engine);
		}
		if (engines.size() > 1) {

			Label tmp = toolkit.createLabel(sectionClient, "Annotation engine"); //$NON-NLS-1$
			gdata = getTextGridData();
			gdata.indent = 20;
			tmp.setLayoutData(gdata);

			annotateEngineCombo = new Combo(sectionClient, SWT.BORDER|SWT.READ_ONLY);
			gdata = getTextGridData();
			annotateEngineCombo.setLayoutData(gdata);
			annotateEngineCombo.setToolTipText(TXMUIMessages.AnnotationsEngines);
			annotateEngineCombo.setItems(engines.toArray(new String[engines.size()]));
			annotateEngineCombo.setText(annotateEngineCombo.getItem(0));
		}

	}

	public static String[] getAvailableLangs() {

		ArrayList<String> langList = new ArrayList<String>();

		// the 2 main language
		langList.add("fr");
		langList.add("en");

		// additional langs from the TreeTagger models
		String modelsDir = TreeTaggerPreferences.getInstance().getString(TreeTaggerPreferences.MODELS_PATH);
		File files[] = new File(modelsDir).listFiles(IOUtils.HIDDENFILE_FILTER);
		if (files != null) {
			Arrays.sort(files);
			if (files != null && files.length > 0) {
				for (File model : files) {
					if (!model.getName().endsWith(".par")) continue; //$NON-NLS-1$
					String lang = FileUtils.stripExtension(model);
					if (!langList.contains(lang)) {
						langList.add(lang);
					}
				}
			}
		}
		// Add TreeTagger langs
		String[] tmp = { "bg", "cat", "zh", "cop", "cs", "dk", "nl", "ee", "fi ", "gl", "gr", "de", "ne", "it", "kr", "lat", "mn", "pl", "pt", "ro", "ru", "sk", "sl", "es", "tz" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$ //$NON-NLS-20$ //$NON-NLS-21$ //$NON-NLS-22$ //$NON-NLS-23$ //$NON-NLS-24$ //$NON-NLS-25$ //$NON-NLS-26$ //$NON-NLS-27$
		Arrays.sort(tmp);
		for (String lang : tmp) {
			if (!langList.contains(lang)) {
				langList.add(lang);
			}
		}

		return langList.toArray(new String[langList.size()]);
	}

	@Override
	public void updateFields(Project project) {
		if (this.section.isDisposed()) return;
		if (project == null) return;

		String lang = project.getLang();
		boolean annotate = project.getAnnotate();

		// fill combo and select default language
		String defaultLang = Locale.getDefault().getLanguage().toLowerCase();
		if ("??".equals(lang)) { //$NON-NLS-1$
			btnSelectLang.setSelection(false);
			btnGuessLang.setSelection(true);
		}
		else if (lang == null) {
			btnSelectLang.setSelection(true);
			btnGuessLang.setSelection(false);
		}
		else {
			btnSelectLang.setSelection(true);
			btnGuessLang.setSelection(false);
			defaultLang = lang;
		}

		String[] data = langcombo.getItems();
		for (int i = 0; i < langcombo.getItemCount(); i++) {
			String l = data[i];
			if (l.equals(defaultLang)) {
				langcombo.select(i);
				break;
			}
		}

		langcombo.setText(defaultLang);
		btnSelectAnnotate.setSelection(annotate);

		if (annotateEngineCombo != null) {
			annotateEngineCombo.setText(project.getImportParameters().node("annotate").get("engine", "TreeTagger")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
	}

	@Override
	public boolean saveFields(Project project) {
		if (!this.section.isDisposed()) {
			// LANGUAGE
			String lang = null;

			if (btnGuessLang != null && btnGuessLang.getSelection()) {
				lang = "??"; //$NON-NLS-1$
			}
			else if (langcombo != null)
				lang = langcombo.getText();

			if (lang == null || lang.length() == 0)
				lang = Locale.getDefault().getLanguage();

			project.setAnnotate(btnSelectAnnotate.getSelection());
			project.setLang(lang);

			if (annotateEngineCombo != null) {
				project.getImportParameters().node("annotate").put("engine", annotateEngineCombo.getText()); //$NON-NLS-1$ //$NON-NLS-2$
			}

			return true;
		}
		return true;
	}

	@Override
	public boolean checkFields() {
		return true;
	}

	@Override
	public int getSectionSize() {
		return SECTION_SIZE;
	}
}
