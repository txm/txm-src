package org.txm.rcp.testers;

import java.util.List;

import org.eclipse.core.expressions.PropertyTester;
import org.txm.core.results.TXMResult;


public class IsParentHidden extends PropertyTester {

	@SuppressWarnings("rawtypes")
	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {

		if (receiver instanceof List) {
			List list = (List) receiver;
			if (list.size() == 0) return false;
			receiver = list.get(0);
		}
		if (receiver instanceof TXMResult) {
			return !((TXMResult) receiver).getParent().isVisible();
		}

		return false;
	}
}
