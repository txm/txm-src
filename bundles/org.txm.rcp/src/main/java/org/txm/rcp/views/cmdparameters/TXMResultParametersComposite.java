package org.txm.rcp.views.cmdparameters;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.TXMResult;
import org.txm.objects.Project;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.swt.GLComposite;

public class TXMResultParametersComposite extends GLComposite {

	private TreeViewer tv;

	private TreeViewerColumn nameColumn;

	private TreeViewerColumn valueColumn;

	private TextCellEditor cellEditor;

	public static String DEFAULT_REFS = "class,user_name";

	private String[] references = DEFAULT_REFS.split(",");

	public TXMResultParametersComposite(Composite parent, int style) {
		super(parent, style, "Parameters");

		tv = new TreeViewer(this, SWT.VIRTUAL);
		tv.getTree().setLayoutData(GridDataFactory.fillDefaults().align(GridData.FILL, GridData.FILL).grab(true, true).create());
		tv.getTree().setHeaderVisible(true);
		tv.getTree().setLinesVisible(true);
		tv.setContentProvider(new ITreeContentProvider() {

			@Override
			public Object[] getElements(Object inputElement) {
				if (inputElement instanceof String) {
					String npath = inputElement.toString();
					Object[] children = getChildren(TXMPreferences.preferencesRootNode.node(npath));
					return children;
				}
				else if (inputElement instanceof Project) {
					Project project = (Project) inputElement;
					IEclipsePreferences prefs = project.getPreferencesScope().getNode("/project/" + project.getName());
					return getChildren(prefs);
				}
				else if (inputElement instanceof TXMResult) {
					TXMResult result = (TXMResult) inputElement;
					// return new Object[] {TXMPreferences.preferencesRootNode.node(result.getParametersNodePath())};
					return getChildren(TXMPreferences.preferencesRootNode.node(result.getParametersNodePath()));
				}
				else if (inputElement instanceof IProject) {
					IProject project = (IProject) inputElement;
					return getChildren(TXMPreferences.preferencesRootNode.node("/" + project.getName()));
				}

				return new Object[0];
			}

			@Override
			public Object[] getChildren(Object parentElement) {
				if (parentElement instanceof Preferences) {
					Preferences prefs = (Preferences) parentElement;
					try {
						ArrayList<Object> children = new ArrayList<>();
						String[] nodes = prefs.childrenNames();
						Arrays.sort(nodes);
						for (String n : nodes) {
							children.add(prefs.node(n));
						}

						String[] attributes = null;
						// FIXME: SJ => MD, I commented this atm
						// if (RCPPreferences.getInstance().getBoolean(RCPPreferences.EXPERT_USER)) {
						//
						// } else {
						attributes = prefs.keys();
						// }
						Arrays.sort(attributes);
						for (String name : attributes) {
							children.add(new Object[] { prefs, name });
						}
						return children.toArray();
					}
					catch (BackingStoreException e) {
						e.printStackTrace();
					}
				}
				return null;
			}

			@Override
			public Object getParent(Object element) {
				if (element instanceof Preferences) {
					Preferences prefs = (Preferences) element;
					return prefs.parent();
				}
				return null;
			}

			@Override
			public boolean hasChildren(Object element) {
				if (element instanceof Preferences) {
					Preferences prefs = (Preferences) element;
					try {
						return prefs.childrenNames().length > 0 || prefs.keys().length > 0;
					}
					catch (BackingStoreException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				return false;
			}

		});

		nameColumn = new TreeViewerColumn(tv, SWT.NONE);
		nameColumn.getColumn().setText("node");
		nameColumn.getColumn().pack();
		nameColumn.setLabelProvider(new CellLabelProvider() {

			@Override
			public void update(ViewerCell cell) {
				Object element = cell.getElement();

				if (element instanceof Preferences) {
					Preferences c = (Preferences) element;
					if (references != null && references.length > 0) {
						ArrayList<String> values = new ArrayList<>();
						for (String k : references) {
							if (c.get(k, null) != null) {
								values.add(c.get(k, null));
							}
						}

						if (values.size() == 0) values.add(c.name());
						cell.setText(StringUtils.join(values, ", "));
					}
					else {
						cell.setText(c.name());
					}

					if (c.get("class", null) != null && c.get("class", null).startsWith("org.txm")) {
						cell.setImage(IImageKeys.getImage(IImageKeys.PROJECT));
					}
				}
				else if (element instanceof Object[]) {
					Object[] c = (Object[]) element;

					cell.setText(c[1].toString());
				}
				else {
					cell.setText(element.toString());
				}
			}

		});
		valueColumn = new TreeViewerColumn(tv, SWT.NONE);
		valueColumn.getColumn().setText("value");
		valueColumn.getColumn().pack();
		valueColumn.setLabelProvider(new CellLabelProvider() {

			@Override
			public void update(ViewerCell cell) {
				Object element = cell.getElement();

				if (element instanceof Preferences) {
					Preferences c = (Preferences) element;
					cell.setText("");
				}
				else if (element instanceof Object[]) {
					Object[] c = (Object[]) element;
					Preferences p = (Preferences) c[0];
					cell.setText(p.get(c[1].toString(), ""));
				}
				else {
					cell.setText(element.toString());
				}
			}
		});

		cellEditor = new TextCellEditor(tv.getTree());
		valueColumn.setEditingSupport(new EditingSupport(tv) {

			@Override
			protected void setValue(Object element, Object value) {
				if (element instanceof Object[]) {
					Object[] data = (Object[]) element;
					Preferences prefs = (Preferences) data[0];
					String key = data[1].toString();
					prefs.put(key, value.toString());
					tv.refresh(element);

				}
				return;
			}

			@Override
			protected Object getValue(Object element) {
				if (element instanceof Object[]) {
					Object[] data = (Object[]) element;
					Preferences prefs = (Preferences) data[0];
					String key = data[1].toString();
					return prefs.get(key, "");
				}
				return null;
			}

			@Override
			protected TextCellEditor getCellEditor(Object element) {
				return cellEditor;
			}

			@Override
			protected boolean canEdit(Object element) {
				return element instanceof Object[];
			}
		});
	}



	public void updateEditorParameters(Object input) {
		tv.setInput(input);
		nameColumn.getColumn().pack();
		valueColumn.getColumn().pack();
	}



	public void refresh() {
		tv.refresh();
	}

	public void setReferences(String[] split) {
		references = split;
	}

	public TreeViewer getTreeViewer() {
		return tv;
	}

	public Object getInput() {
		return tv.getInput();
	}
}
