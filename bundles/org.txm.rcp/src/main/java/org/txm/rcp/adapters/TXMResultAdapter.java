/**
 * 
 */
package org.txm.rcp.adapters;

import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.model.WorkbenchAdapter;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.ITXMResultEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.utils.SWTEditorsUtils;

/**
 * Base adapter for TXM result objects.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public abstract class TXMResultAdapter extends WorkbenchAdapter {

	/**
	 * Default constructor.
	 */
	public TXMResultAdapter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object[] getChildren(Object result) {
		if (!(result instanceof TXMResult txmResult)) {
			return new Object[0];
		}
		return txmResult.getChildren(!TBXPreferences.getInstance().getBoolean(TBXPreferences.SHOW_ALL_RESULT_NODES)).toArray();
	}

	@Override
	public String getLabel(Object result) {

		if (!(result instanceof TXMResult txmResult)) {
			return result.toString();
		}

		String label = txmResult.getCurrentName();

		// update label to reflect dirty state if at least one opened editor using the result is dirty
		Set<ITXMResultEditor<TXMResult>> editors = SWTEditorsUtils.getEditors(txmResult);
		for (ITXMResultEditor<TXMResult> editor : editors) {
			if (editor.isDirty()) {
				label += " *"; //$NON-NLS-1$
				break;
			}
		}

		return label;
	}

	@Override
	public Object getParent(Object result) {
		if (result instanceof TXMResult txmResult) {
			return txmResult.getParent();
		}
		else {
			return null;
		}
	}

	@Override
	public FontData getFont(Object element) {

		FontData fontData = null;

		if (element instanceof TXMResult result) {

			fontData = Display.getCurrent().getSystemFont().getFontData()[0];

			// highlight the node label of the current active editor
			ITXMResultEditor<? extends TXMResult> editor = SWTEditorsUtils.getActiveEditor(null);
			if (editor != null && element == editor.getFirstVisibleResult()) {
				fontData.setStyle(SWT.BOLD);
			}

			// display non-persisted result with italic font
			if (!result.isUserPersistable() && !result.isInternalPersistable()) {
				fontData.setStyle(fontData.getStyle() | SWT.ITALIC);
			}

		}

		return fontData;
	}

	@Override
	public RGB getForeground(Object element) {
		// debug/advanced mode
		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER) && element instanceof TXMResult txmResult && !txmResult.hasBeenComputedOnce()) {
			// set as gray results that has not yet been computed
			Display display = Display.getCurrent();
			Color color = display.getSystemColor(SWT.COLOR_DARK_GRAY);
			return color.getRGB();
		}
		// FIXME: DEbug
		// else if(element instanceof TXMResult && ((TXMResult)element).isDirty()) {
		// Display display = Display.getCurrent();
		// Color color = display.getSystemColor(SWT.COLOR_YELLOW);
		// return color.getRGB();
		// }

		return null;
	}

	@Override
	public RGB getBackground(Object element) {

		// debug/advanced mode
		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER) && element instanceof TXMResult result && !result.isVisible()) {
			// add a gray background to results that are not visible
			Display display = Display.getCurrent();
			// Color color = display.getSystemColor(SWT.COLOR_GRAY);
			Color color = new Color(display, 240, 240, 240);
			return color.getRGB();
		}


		// FIXME: SJ: tests
		// if(element instanceof TXMResult && ((TXMResult)element).isUserPersistable()) {
		// Display display = Display.getCurrent();
		// Color color = display.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND);
		//
		// // FIXME: random color tests
		//// Random rnd = new Random();
		//// int r = rnd.nextInt(128) + 128; // 128 ... 255
		//// int g = rnd.nextInt(128) + 128; // 128 ... 255
		//// int b = rnd.nextInt(128) + 128; // 128 ... 255
		//// Color blue = new Color(display, r, g, b);
		//
		// return color.getRGB();
		// }
		return null;
	}

}
