package org.txm.rcp.preferences;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.preferences.TXMPreferences;


/**
 * RCP Preferences initializer.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class RCPPreferences extends TBXPreferences {


	// public static final String PREFERENCES_NODE = FrameworkUtil.getBundle(RCPPreferences.class).getSymbolicName();

	/**
	 * enable/disable system notifications when TXM ends a long task
	 */
	public static final String SHOW_NOTIFICATIONS = "show_notifications"; //$NON-NLS-1$

	/** Preference set with the osgi-nl parameter */
	public static final String CONFIG_LOCALE = "ui_locale"; //$NON-NLS-1$

	/** Preference set with the -Xms parameter */
	public static final String CONFIG_XMS = "config_xms"; //$NON-NLS-1$

	/** Preference set with the osgi-nl parameter */
	public static final String CONFIG_XMX = "config_xmx"; //$NON-NLS-1$

	/** The Constant USER_ALERT_DELETE. */
	public static final String DONT_ALERT_USER_BEFORE_DELETE = "user_alert_delete"; //$NON-NLS-1$

	public static final String MAX_NUMBER_OF_COLUMNS = "max_number_of_columns"; //$NON-NLS-1$

	/**
	 * To auto compute result and refresh editor when a form computing parameter is modified.
	 */
	public static final String AUTO_UPDATE_EDITOR = "auto_update_editor"; //$NON-NLS-1$

	/**
	 * NEVER|ASK|ALWAYS compute result children
	 */
	public static final String COMPUTE_CHILDREN = "compute_children"; //$NON-NLS-1$

	public static final String COMPUTE_CHILDREN_ASK = "Ask"; //$NON-NLS-1$

	public static final String COMPUTE_CHILDREN_NEVER = "Never"; //$NON-NLS-1$

	public static final String COMPUTE_CHILDREN_ALWAYS = "Always"; //$NON-NLS-1$

	public static final String SHOW_SEVERE_DIALOG = "show_severe_dialog"; //$NON-NLS-1$

	public static final String UPDATE_LEVEL = "update_level"; //$NON-NLS-1$

	/** The Constant FILES_TO_HIDE. */
	public static final String FILES_TO_HIDE = "files_to_hide"; //$NON-NLS-1$

	public static final String SHOW_HIDDEN_FILES = "show_hidden_files"; //$NON-NLS-1$

	public static final String SHOW_FOLDERS = "show_folders"; //$NON-NLS-1$

	/** The Constant NO_SESSION. */
	public static final String NO_SESSION = "script_nosession"; //$NON-NLS-1$

	public static final String SAVE_BEFORE_EXECUTION = "script_save_before_execution"; //$NON-NLS-1$

	/** The Constant N_LINE_PER_PAGE_DEFAULT. */
	public static final int N_LINE_PER_PAGE_DEFAULT = 100;

	/** The Constant LEFT_CONTEXT_SIZE_DEFAULT. */
	public static final int LEFT_CONTEXT_SIZE_DEFAULT = 15;

	/** The Constant RIGHT_CONTEXT_SIZE_DEFAULT. */
	public static final int RIGHT_CONTEXT_SIZE_DEFAULT = 15;

	public static final String UPDATESITE = "update_site"; //$NON-NLS-1$


	public static final String CGU = "cgu"; //$NON-NLS-1$

	//	public static final String COLUMNS_WIDTH_MULTIPLIER = "columns_width_multiplier";


	@Override
	public void initializeDefaultPreferences() {

		// FIXME: to store/merge and share all the prefs in the Core node
		this.commandPreferencesNodeQualifier = "org.txm.core"; //$NON-NLS-1$

		// super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.put(TBXPreferences.EXPORT_ENCODING, System.getProperty("file.encoding")); //$NON-NLS-1$
		preferences.put(TBXPreferences.EXPORT_COL_SEPARATOR, "\t"); //$NON-NLS-1$
		preferences.put(TBXPreferences.EXPORT_TXT_SEPARATOR, ""); //$NON-NLS-1$
		preferences.put(CONFIG_LOCALE, "en"); //$NON-NLS-1$

		preferences.put(UPDATE_LEVEL, "STABLE"); //$NON-NLS-1$


		preferences.put(COMPUTE_CHILDREN, COMPUTE_CHILDREN_ASK);
		preferences.putBoolean(AUTO_UPDATE_EDITOR, false);
		// preferences.putBoolean(AUTO_COMPUTE_ON_EDITOR_OPEN, true);
		preferences.putBoolean(SHOW_SEVERE_DIALOG, false);
		preferences.putBoolean(DONT_ALERT_USER_BEFORE_DELETE, false);
		preferences.putBoolean(SAVE_BEFORE_EXECUTION, false);
		preferences.putBoolean(SHOW_NOTIFICATIONS, false);

		preferences.putBoolean(CGU, false);

		preferences.putInt(NO_SESSION, 1);
		preferences.putInt(MAX_NUMBER_OF_COLUMNS, 100);
		//		preferences.putFloat(COLUMNS_WIDTH_MULTIPLIER, 0.54f);
	}

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(RCPPreferences.class)) {
			new RCPPreferences();
		}
		return TXMPreferences.instances.get(RCPPreferences.class);
	}
}
