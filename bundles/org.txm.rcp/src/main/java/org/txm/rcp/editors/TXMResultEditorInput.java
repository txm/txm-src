package org.txm.rcp.editors;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;
import org.txm.core.results.TXMResult;

/**
 * Base class of all TXM results <code>IEditorInput</code>.
 * 
 * @author sjacquot
 *
 */
public class TXMResultEditorInput<T extends TXMResult> implements IEditorInput {

	/**
	 * 
	 */
	protected boolean alwaysRecreateEditor;

	/**
	 * TXM result object.
	 */
	protected T result;

	/**
	 * Creates an editor input and stores the specified TXM result as source object for further result computing.
	 */
	public TXMResultEditorInput(T result) {
		this.result = result;
		this.alwaysRecreateEditor = false;
	}

	/**
	 * Gets the TXM result associated with the editor input.
	 * 
	 * @return the linked result
	 */
	public T getResult() {
		return result;
	}

	/**
	 * Sets the result data associated with the editor input.
	 * 
	 * @param resultData the result to set
	 */
	public void setResult(T resultData) {
		this.result = resultData;
	}

	/**
	 * If <code>true</code>, a new editor part will always be recreated even for the same TXM result object by making the <code>TXMResultEditorInput.equals()</code> test returning always true.
	 * 
	 * @param alwaysRecreateEditor
	 */
	public void setAlwaysRecreateEditor(boolean alwaysRecreateEditor) {
		this.alwaysRecreateEditor = alwaysRecreateEditor;
	}

	@Override
	public String getName() {
		try {
			return this.result.getName().replace("\n", " "); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (Exception e) {
		}
		return ""; //$NON-NLS-1$
	}

	@Override
	public String getToolTipText() {
		try {
			return this.result.getDetails();
		}
		catch (Exception e) {
		}
		return ""; //$NON-NLS-1$
	}

	/**
	 * Deletes the linked TXM result.
	 * 
	 * @return true if the object is deleted
	 */
	public boolean deleteResult() {
		try {
			return this.result.delete();
		}
		catch (Exception e) {
			return false;
		}
	}


	@Override
	public boolean exists() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPersistableElement getPersistable() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getAdapter(Class adapter) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj instanceof TXMResultEditorInput) {
			if (this.alwaysRecreateEditor) {
				return false;
			}
			return this.result == ((TXMResultEditorInput) obj).getResult();
		}
		return false;
	}

}
