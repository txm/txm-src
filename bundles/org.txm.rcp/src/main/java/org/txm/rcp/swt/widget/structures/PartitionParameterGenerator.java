package org.txm.rcp.swt.widget.structures;

import org.txm.searchengine.cqp.corpus.Partition;

public interface PartitionParameterGenerator {

	/**
	 * creates or updates a partition
	 * 
	 * @param name
	 * @param partition
	 * @return
	 */
	public Partition createPartition(String name, Partition partition);
}
