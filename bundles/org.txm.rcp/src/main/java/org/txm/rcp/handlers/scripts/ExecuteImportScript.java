// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.scripts;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.Toolbox;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Project;
import org.txm.rcp.ApplicationWorkbenchAdvisor;
import org.txm.rcp.StatusLine;
import org.txm.rcp.commands.OpenImportForm;
import org.txm.rcp.commands.RestartTXM;
import org.txm.rcp.editors.imports.ImportModuleCustomization;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.BundleUtils;
import org.txm.utils.ExecTimer;
import org.txm.utils.OSDetector;
import org.txm.utils.logger.Log;

import groovy.util.GroovyScriptEngine;

// TODO: Auto-generated Javadoc
/**
 * Execute an import module (Groovy script).
 * Binds parameters to the script :
 * project: the corpus project
 * monitor= the job
 * debug=true|false
 * readyToLoad=tested after the script. If true then the process went OK.
 *
 * @author mdecorde
 */
public class ExecuteImportScript extends AbstractHandler {

	/** The lastopenedbase. */
	public static File lastopenedbase;

	protected static GroovyScriptEngine gse;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String filename = null, filepath = null;
		File script = null;
		Boolean updateMode = false;
		if (event.getParameters().containsKey("org.txm.rcp.commands.importscript")) { //$NON-NLS-1$
			filename = event.getParameter("org.txm.rcp.commands.importscript"); //$NON-NLS-1$
			filepath = "scripts/groovy/user/org/txm/scripts/importer/" + filename; //$NON-NLS-1$
			script = new File(filepath);
		}
		else if (event.getParameters().containsKey("org.txm.rcp.commands.usecorporaselection")) { //$NON-NLS-1$
			boolean useCorporaSelection = "true".equals(event.getParameter("org.txm.rcp.commands.usecorporaselection")); //$NON-NLS-1$ //$NON-NLS-2$
			//filepath = "scripts/groovy/user/org/txm/scripts/importer/" + filename; //$NON-NLS-1$
		}
		else {
			Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
			FileDialog dialog = new FileDialog(shell, SWT.SAVE);
			dialog.setFilterPath(Toolbox.getTxmHomePath() + "/scripts/groovy/user/org/txm/scripts/importer"); //$NON-NLS-1$
			filepath = dialog.open();

			if (filepath == null) {
				Log.warning(TXMUIMessages.NoGroovyEntryPointScriptSelectedAborted);
				return null;
			}
			script = new File(filepath);
		}

		if (event.getParameters().containsKey("org.txm.rcp.commands.importupdatemode")) { //$NON-NLS-1$
			updateMode = "true".equals(event.getParameter("org.txm.rcp.commands.importupdatemode")); //$NON-NLS-1$ //$NON-NLS-2$
		}

		OpenImportForm.open(script, updateMode);
		return null;
	}

	// @Deprecated
	// public static JobHandler executeScript(String scriptFilePath,
	// File paramFile) {
	// BaseOldParameters params = null;
	// try {
	// params = new BaseOldParameters(paramFile);
	// } catch (Exception e1) {
	// org.txm.utils.logger.Log.printStackTrace(e1);
	// System.out.println("Error while reading import parameters from: "+paramFile);
	// return null;
	// }
	// return executeScript(scriptFilePath, params);
	// }

	/**
	 * 
	 * @return
	 */
	public static JobHandler executeScript(final Project project) {

		if (project.getImportModuleName() == null) {
			return null;
		}

		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				StatusLine.setMessage(TXMUIMessages.importStarted);
			}
		});

		final String txmhome = Toolbox.getTxmHomePath();
		final File corporadir = new File(txmhome, "corpora"); //$NON-NLS-1$
		corporadir.mkdir(); // the binary corpus are stored here
		if (!corporadir.exists()) {
			System.out.println(TXMUIMessages.errorColonCorporaDirectoryDoesntExistColon + corporadir);
			Log.severe(TXMUIMessages.errorColonCorporaDirectoryDoesntExistColon + corporadir);
			return null;
		}

		// if (CQPSearchEngine.isInitialized()) {
		// Toolbox.getEngineManager(EngineType.SEARCH).stopEngines();
		// }
		final long time = System.currentTimeMillis();
		JobHandler jobhandler = new JobHandler(TXMUIMessages.scriptExecution + " " + project.getImportModuleName()) { //$NON-NLS-1$

			@Override
			protected IStatus _run(SubMonitor monitor) throws Exception {

				try {
					boolean doUpdate = project.getDoUpdate();

					ExecTimer.start();
					if (project.compute(monitor)) {

						this.releaseSemaphore();



						for (CorpusBuild corpus : project.getChildren(CorpusBuild.class)) {
							corpus.compute(false); // pre-compute CorpusBuilds
						}

						monitor.done();
						if (doUpdate) {
							project.appendToLogs("Updated by TXM " + ApplicationWorkbenchAdvisor.getTXMVersionPrettyString() + " with the '"
									+ ImportModuleCustomization.getName(project.getImportModuleName()) + "' import module by " + OSDetector.getUserAndOSInfos());
							Log.info(NLS.bind(TXMUIMessages.updateDoneInP0, ExecTimer.stop()));
						}
						else {
							project.appendToLogs("Built by TXM " + ApplicationWorkbenchAdvisor.getTXMVersionPrettyString() + " with the '"
									+ ImportModuleCustomization.getName(project.getImportModuleName()) + "' import module by " + OSDetector.getUserAndOSInfos());
							Log.info(NLS.bind(TXMUIMessages.importDoneInP0, ExecTimer.stop()));
						}
						return Status.OK_STATUS;
					}
					ExecTimer.stop();
				}
				catch (Exception e) {

					throw e;
				}
				finally {
					System.gc();
					// if (!CQPSearchEngine.isInitialized()) {
					// try {
					// SearchEnginesManager.getCQPSearchEngine().start(monitor);
					// } catch (Exception e) {
					// // TODO Auto-generated catch block
					// e.printStackTrace();
					// }
					// }
					syncExec(new Runnable() {

						@Override
						public void run() {
							// if (RCPPreferences.getInstance().getBoolean(RCPPreferences.SHOW_NOTIFICATIONS)
							// && (System.currentTimeMillis() - time) > 0) { // 10 mins
							// String message = NLS.bind("The {0} corpus was succesfully imported", project.getName());
							// if (project.isDirty()) {
							// message = NLS.bind("The {0} corpus was not imported correctly. Please check console messages.", project.getName());
							// }
							// new SimpleNotification(Display.getCurrent(), "Import done", message).open();
							// }
							RestartTXM.reloadViews();
							CorporaView.select(project.getChildren(MainCorpus.class));
						}
					});
				}

				return Status.CANCEL_STATUS;
			}
		};
		jobhandler.setSystem(false);
		jobhandler.startJob(true);
		return jobhandler;
	}

	/**
	 * Allow to retrieve an import script from the Toolbox plugin.
	 * 
	 * If TXM is run from Eclipse, then the file is copyied, else it is extracted from TXM installation directory
	 * 
	 * @param script the import script Loader to retrieve
	 * @return
	 */
	public static boolean getImportScriptFromToolboxPlugin(File script) {
		Log.fine("Retriving " + script + " from Toolbox plugin if script.date < toolbox_script.date..."); //$NON-NLS-1$ //$NON-NLS-2$

		// retrieve the script relative path "quickLoader" -> "quick"
		String scriptPackage = script.getName();
		int idx = scriptPackage.indexOf("Loader"); //$NON-NLS-1$
		scriptPackage = "org/txm/scripts/importer/" + scriptPackage.substring(0, idx); //$NON-NLS-1$

		return BundleUtils.replaceFilesIfNewer("org.txm.groovy.core", "src/groovy", scriptPackage, //$NON-NLS-1$ //$NON-NLS-2$
				script.getName(), script);
	}
}
