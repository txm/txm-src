// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.views.fileexplorer;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.txm.rcp.Application;
import org.txm.rcp.IImageKeys;

/**
 * The Class FileTreeLabelProvider: cache of TXM images and utility methods to get an image of a plugin
 */
public class FileTreeLabelProvider extends LabelProvider {

	/** The image table. */
	private Map<ImageDescriptor, Image> imageTable = new HashMap<>();

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object element) {
		return ((File) element).getName();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
	 */
	@Override
	public final Image getImage(Object element) {
		File f = ((File) element);
		ImageDescriptor imageDescriptor = null;
		if (f.isDirectory()) {
			imageDescriptor = AbstractUIPlugin.imageDescriptorFromPlugin(
					Application.PLUGIN_ID, IImageKeys.FOLDER);
		}
		else if (f.isFile()) {
			String fname = f.getName().toLowerCase();
			if (fname.matches(".*\\.(mp4|avi|mkv|ogv|mpg|webm)")) { //$NON-NLS-1$
				imageDescriptor = AbstractUIPlugin
						.imageDescriptorFromPlugin(Application.PLUGIN_ID,
								IImageKeys.FILE_VIDEO);
			}
			else if (fname.endsWith(".txm")) { //$NON-NLS-1$
				imageDescriptor = AbstractUIPlugin
						.imageDescriptorFromPlugin(Application.PLUGIN_ID,
								IImageKeys.TXM1616);
			}
			else if (fname.matches(".*\\.(mp3|ogg|wav|flac)")) { //$NON-NLS-1$
				imageDescriptor = AbstractUIPlugin
						.imageDescriptorFromPlugin(Application.PLUGIN_ID,
								IImageKeys.FILE_AUDIO);
			}
			else if (fname.matches(".*\\.(png|jpg|jpeg|bmp|gif|svg)")) { //$NON-NLS-1$
				imageDescriptor = AbstractUIPlugin
						.imageDescriptorFromPlugin(Application.PLUGIN_ID,
								IImageKeys.FILE_IMAGE);
			}
			else if (fname.endsWith("Macro.groovy")) { //$NON-NLS-1$
				imageDescriptor = AbstractUIPlugin
						.imageDescriptorFromPlugin(Application.PLUGIN_ID,
								IImageKeys.SCRIPT_RUN);
			}
			else if (fname.matches(".*\\.(groovy|R|py|pl|java|rb|sh|bat|js|c|cpp|xml|xsl|css)")) { //$NON-NLS-1$
				imageDescriptor = AbstractUIPlugin
						.imageDescriptorFromPlugin(Application.PLUGIN_ID,
								IImageKeys.SCRIPT);
			}
			else if (fname.matches(".*\\.(doc.|od.|pdf|rtf)")) { //$NON-NLS-1$
				imageDescriptor = AbstractUIPlugin
						.imageDescriptorFromPlugin(Application.PLUGIN_ID,
								IImageKeys.FILE_DOC);
			}
			else if (fname.endsWith("html")) { //$NON-NLS-1$
				imageDescriptor = AbstractUIPlugin
						.imageDescriptorFromPlugin(Application.PLUGIN_ID,
								IImageKeys.WORLD);
			}
			else {
				imageDescriptor = AbstractUIPlugin
						.imageDescriptorFromPlugin(Application.PLUGIN_ID,
								IImageKeys.FILE);
			}
		}
		else {
			return null;
		}

		Image image = imageTable.get(imageDescriptor);
		if (image == null) {
			image = imageDescriptor.createImage();
			imageTable.put(imageDescriptor, image);
		}
		return image;
	}
}
