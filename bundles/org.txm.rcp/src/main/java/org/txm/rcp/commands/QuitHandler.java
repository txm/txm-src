package org.txm.rcp.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.widgets.Display;
import org.txm.rcp.messages.TXMUIMessages;

import jline.internal.Log;

/**
 * Call Shell.close() if not the WindowAdvisor.preShellclosed is not called
 * 
 * @author mdecorde
 *
 */
public class QuitHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Log.info(TXMUIMessages.quitTXM);

		Display.getCurrent().getActiveShell().close();

		return null;
	}
}
