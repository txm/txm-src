// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.utils;

import java.io.File;

import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Shell;

// TODO: Auto-generated Javadoc
/*
 import java.awt.Toolkit;
 import java.awt.datatransfer.DataFlavor;
 import java.awt.datatransfer.StringSelection;
 import java.awt.datatransfer.Transferable;
 */
/**
 * manage the clipboard.
 */
public class IOClipboard {

	/**
	 * Read.
	 *
	 * @return the string
	 */
	public static String read() {
		Shell sh = new Shell();
		Clipboard cb = new Clipboard(sh.getDisplay());
		String data = (String) cb.getContents(TextTransfer.getInstance());
		sh.close();
		return data;
	}

	/**
	 * Read.
	 *
	 * @return the string
	 */
	public static String[] readFiles() {
		Shell sh = new Shell();
		Clipboard cb = new Clipboard(sh.getDisplay());
		String[] data = (String[]) cb.getContents(FileTransfer.getInstance());
		sh.close();
		return data;
	}

	/**
	 * Write file paths.
	 *
	 */
	public static void writeFiles(Object[] files) {
		Shell sh = new Shell();
		Clipboard cb = new Clipboard(sh.getDisplay());
		Transfer[] transferts = new Transfer[files.length];
		for (int i = 0; i < files.length; i++) {
			if (files[i] instanceof File) {
				transferts[i] = FileTransfer.getInstance();
			}
		}
		cb.setContents(files, transferts);
	}

	/**
	 * Write.
	 *
	 * @param text the text
	 */
	public static void write(String text) {
		if (text.length() == 0) return;

		Shell sh = new Shell();
		Clipboard cb = new Clipboard(sh.getDisplay());
		TextTransfer textTransfer = TextTransfer.getInstance();
		cb.setContents(new Object[] { text }, new Transfer[] { textTransfer });
		sh.close();
	}

	/*
	 * public static String read() { Transferable t =
	 * Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null); try {
	 * if (t != null && t.isDataFlavorSupported(DataFlavor.stringFlavor)) {
	 * String text = (String) t.getTransferData(DataFlavor.stringFlavor);
	 * return text.trim(); } } catch (Exception e) { } return ""; }
	 * public static void write(String text) { SecurityManager sm =
	 * System.getSecurityManager(); if (sm != null) { try {
	 * sm.checkSystemClipboardAccess(); } catch (Exception e)
	 * {org.txm.utils.logger.Log.printStackTrace(e);} } Toolkit tk = Toolkit.getDefaultToolkit();
	 * StringSelection st = new StringSelection(text);
	 * java.awt.datatransfer.Clipboard cp = tk.getSystemClipboard();
	 * cp.setContents(st, null); }
	 */
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		System.out.println("read : "); //$NON-NLS-1$
		System.out.println(IOClipboard.read());
		System.out.println("write : sdfgdsfg"); //$NON-NLS-1$
		IOClipboard.write("dfsgsdgdsg"); //$NON-NLS-1$
		System.out.println("read : " + IOClipboard.read()); //$NON-NLS-1$
	}
}
