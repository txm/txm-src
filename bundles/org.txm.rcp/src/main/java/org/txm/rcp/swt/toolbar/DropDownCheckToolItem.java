package org.txm.rcp.swt.toolbar;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.swt.listeners.DropdownSelectionListener;

/**
 * A ToolItem SWT.CHECK with 2 images for the selected and not selected states
 * 
 * @author mdecorde
 *
 */
public class DropDownCheckToolItem extends ToolItem {

	boolean selected = false;

	private DropdownSelectionListener dropDownListener;

	private SelectionListener listener;

	/**
	 * 
	 * @param parent
	 * @param title used as Text if openIconFilePath icon is not set, and as ToolTip text if openIconFilePath icon is set
	 * @param openIconFilePath
	 * @param closeIconFilePath
	 */
	public DropDownCheckToolItem(ToolBar parent, String title, String openIconFilePath, String closeIconFilePath) {
		super(parent, SWT.DROP_DOWN);

		this.dropDownListener = new org.txm.rcp.swt.listeners.DropdownSelectionListener(parent, this);
		this.addSelectionListener(dropDownListener); // activate

		if (openIconFilePath != null) {
			openIcon = IImageKeys.getImage(openIconFilePath);
			this.setImage(openIcon);
			this.setToolTipText(title);
			if (closeIconFilePath != null) {
				closeIcon = IImageKeys.getImage(closeIconFilePath);
			}
		}
		else {
			this.setText(title);
		}

		listener = new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (e != null && !e.doit) {
					return; // drop down clicked
				}
				selected = !selected;
				updateIcon();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		};
		this.addSelectionListener(listener);
	}

	public void updateIcon() {
		if (closeIcon != null) {
			if (getSelection()) {
				setImage(closeIcon);
			}
			else {
				setImage(openIcon);
			}
		}
	}

	@Override
	public boolean getSelection() {
		return selected;
	}

	public void select() {
		listener.widgetSelected(null);
	}

	@Override
	public void setSelection(boolean selected) {
		this.selected = selected;
		updateIcon();
	}

	@Override
	public void checkSubclass() {
	}

	protected Image openIcon;

	protected Image closeIcon;

	public DropdownSelectionListener getDropDownListener() {
		return dropDownListener;
	}
}
