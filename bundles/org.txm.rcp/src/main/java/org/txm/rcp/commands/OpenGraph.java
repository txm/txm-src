// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.rcp.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.OpenURI;

// TODO: Auto-generated Javadoc
/**
 * Open the graphics editor. It can open SVN, PNG, etc depending on the Device given.
 */

public class OpenGraph extends AbstractHandler {

	private static final String ID = "org.txm.rcp.commands.OpenGraph"; //$NON-NLS-1$
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String svgFile = ""; //$NON-NLS-1$
		File file = null;

		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event)
				.getShell();

		if (file == null || file.isDirectory()) {
			FileDialog dialog = new FileDialog(shell, SWT.OPEN);
			if (LastOpened.getFile(ID) != null) {
				dialog.setFilterPath(LastOpened.getFolder(ID));
				dialog.setFileName(LastOpened.getFile(ID));
			}
			if (dialog.open() == null) return null;
			svgFile = dialog.getFilterPath() + "/" + dialog.getFileName(); //$NON-NLS-1$
			LastOpened.set(ID, dialog.getFilterPath(), dialog.getFileName());
			OpenFile(svgFile);
		}

		return null;
	}

	/**
	 * Open file.
	 *
	 * @param filepath the filepath
	 * @return the i editor part
	 */
	public static IEditorPart OpenFile(String filepath) {
		return OpenFile(filepath, new File(filepath).getName());
	}

	/**
	 * Open file.
	 *
	 * @param file the file
	 * @return the i editor part
	 */
	public static IEditorPart OpenFile(File file) {
		return OpenFile(file.getAbsolutePath(), file.getName());
	}

	/**
	 * Open file.
	 *
	 * @param filepath the filepath
	 * @param editorname the editorname
	 * @return the i editor part
	 */
	public static IEditorPart OpenFile(String filepath, String editorname) {
		File graphfile = new File(filepath);
		if (filepath.endsWith(".svg") || filepath.endsWith(".SVG")) //$NON-NLS-1$ //$NON-NLS-2$
		{
			return OpenBrowser.openfile(filepath);
		}
		else if (filepath.endsWith(".png") || filepath.endsWith(".PNG") ||  //$NON-NLS-1$ //$NON-NLS-2$
				filepath.endsWith(".jpg") || filepath.endsWith(".JPG") ||  //$NON-NLS-1$ //$NON-NLS-2$
				filepath.endsWith(".jpeg") || filepath.endsWith(".JPEG")) //$NON-NLS-1$ //$NON-NLS-2$
		{
			return OpenBrowser.openfile(graphfile.getAbsolutePath());
		}
		else {
			if (!OpenURI.open(graphfile))
				System.out.println(NLS.bind(TXMUIMessages.cantOpenGraphicP0, graphfile.getAbsolutePath()));
			return null;
		}
	}
}
