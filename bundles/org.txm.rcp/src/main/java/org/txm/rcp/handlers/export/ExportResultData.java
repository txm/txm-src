// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.rcp.handlers.export;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.TXMResult;
import org.txm.rcp.StatusLine;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.files.EditFile;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.utils.ExecTimer;
import org.txm.utils.logger.Log;

/**
 * Exports a result by calling the function toTxt(File f) then opens or not the result in the text editor according to the preferences.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class ExportResultData extends AbstractHandler {

	
	private static final String ID = ExportResultData.class.getName();

	/** The selection. */
	private IStructuredSelection selection;


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IEditorPart activeEditor = HandlerUtil.getActiveEditor(event);
		IWorkbenchPart activePart = HandlerUtil.getActivePart(event);

		Object s = null;
		if (activePart instanceof CorporaView) {
			selection = CorporaView.getSelection();
			s = selection.getFirstElement();
		}
		else if (activeEditor instanceof TXMEditor<?> editor) {
			s = editor.getResult();
		}

		//		ISelection isel = HandlerUtil.getCurrentSelection(event);
		//		if (!(isel instanceof IStructuredSelection)) {
		//			selection = CorporaView.getSelection();
		//		} else {
		//			selection = (StructuredSelection)isel;
		//		}
		//		
		//		final Object s = selection.getFirstElement();
		if (s == null) return null;
		if (!(s instanceof TXMResult)) {
			Log.warning(TXMUIMessages.TheExportedObjectIsNotATXMResultResult);
			return null;
		}

		final TXMResult r = (TXMResult) s;
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

		if (r instanceof ExportResultDataConfiguration conf) {
			conf.setDirty();
			return doExportConfiguration(conf);
		}

		ExportResultDialog dialog = new ExportResultDialog(shell, r);
		dialog.setFilePath(LastOpened.getFolder(ID), r.getValidFileName());

		if (dialog.open() == Window.OK) {
			StatusLine.setMessage(TXMUIMessages.exportingResults);
			String filepath = dialog.getFilePath(); 
			Boolean saveExportConfiguration = dialog.getSaveConfiguration();

			final File outfile = new File(filepath);

			if (outfile.exists()) {
				if (!MessageDialog.openQuestion(shell, "Warning", NLS.bind(TXMUIMessages.theP0FileAlreadyExistsWouldYouLikeToReplaceIt, filepath))) {
					return null; // aborting
				}
			}

			LastOpened.set(ID, outfile.getParent(), outfile.getName());
			try {
				outfile.createNewFile();
			}
			catch (IOException e1) {
				Log.warning(NLS.bind(TXMUIMessages.exportColonCantCreateFileP0ColonP1, outfile, e1));
			}
			if (!outfile.canWrite()) {
				Log.warning(NLS.bind(TXMUIMessages.impossibleToReadP0, outfile));
				return null;
			}
			if (!outfile.isFile()) {
				Log.warning("Error: " + outfile + " is not a file"); //$NON-NLS-1$ //$NON-NLS-2$
				return null;
			}

			final String encoding = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_ENCODING);
			final String colseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_COL_SEPARATOR);
			final String txtseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_TXT_SEPARATOR);

			if (saveExportConfiguration) {
				ExportResultDataConfiguration conf = new ExportResultDataConfiguration(r);
				try {
					conf.setParameter("file", outfile);
					conf.setParameter("encoding", encoding);
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return Status.CANCEL_STATUS;
				}

				doExportConfiguration(conf);
			}

			JobHandler jobhandler = new JobHandler(TXMUIMessages.exportingResults) {

				@Override
				protected IStatus _run(SubMonitor monitor) throws Exception {

					try {

						// compute the result if needed
						if (!r.isAltered() && r.isDirty()) {
							r.compute(this);
						}
						Log.info(TXMUIMessages.bind(TXMUIMessages.exportingP0ToP1, r.getName(), outfile));
						ExecTimer.start();
						r.toTxt(outfile, encoding, colseparator, txtseparator);
						Log.info(TXMUIMessages.bind(TXMUIMessages.doneInP0 + ". See " + outfile.getAbsolutePath(), ExecTimer.stop()));

						if (outfile.exists()) {
							// Open internal editor in the UI thread
							if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPORT_SHOW)) {
								this.syncExec(new Runnable() {

									@Override
									public void run() {
										EditFile.openfile(outfile.getAbsolutePath());
									}
								});
							}
						}
						else {
							Log.warning(NLS.bind(TXMUIMessages.failedToExportResultP0ColonP1, outfile.getAbsolutePath()));
						}
					}
					catch (Exception e) {
						Log.warning(NLS.bind(TXMUIMessages.failedToExportResultP0ColonP1, r, e));
						throw e;
					}
					return Status.OK_STATUS;
				}
			};

			jobhandler.startJob();
		}
		return null;
	}

	private JobHandler doExportConfiguration(ExportResultDataConfiguration conf) {

		JobHandler jobhandler = new JobHandler(TXMUIMessages.exportingResults) {

			@Override
			protected IStatus _run(SubMonitor monitor) throws Exception {

				try {
					conf.setDirty();
					conf.compute(this);
				}
				catch (Exception e) {
					Log.warning("Export failed: " + e);
					throw e;
				}

				return Status.OK_STATUS;
			}
		};
		jobhandler.startJob();
		return jobhandler;
	}
}
