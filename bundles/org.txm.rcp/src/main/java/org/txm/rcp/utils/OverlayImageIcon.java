// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.utils;


import org.eclipse.jface.resource.CompositeImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.txm.rcp.IImageKeys;

// TODO: Auto-generated Javadoc
/**
 * The Class OverlayImageIcon.
 *
 * @author balajik
 * 
 *         This class is used for overlaying image icons
 */
public class OverlayImageIcon extends CompositeImageDescriptor {

	/** Base image of the object. */
	private Image baseImage_;

	/** Size of the base image. */
	private Point sizeOfImage_;

	/** The imagekey. */
	private String imagekey;

	/**
	 * Constructor for overlayImageIcon.
	 *
	 * @param baseImage the base image
	 * @param imagekey the imagekey
	 */
	public OverlayImageIcon(Image baseImage,
			String imagekey) {
		// Base image of the object
		baseImage_ = baseImage;
		// Demo Image Object 
		//demoImage_ = demoImage;
		//imageKey_ = imageKey;
		if (baseImage != null) {
			sizeOfImage_ = new Point(baseImage.getBounds().width,
					baseImage.getBounds().height);
		}
		this.imagekey = imagekey;
	}

	/**
	 * Draw composite image.
	 *
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 * @see org.eclipse.jface.resource.CompositeImageDescriptor#drawCompositeImage(int, int)
	 *      DrawCompositeImage is called to draw the composite image.
	 */
	@Override
	protected void drawCompositeImage(int arg0, int arg1) {
		//System.out.println("draw composite with "+baseImage_);
		// Draw the base image
		drawImage(baseImage_.getImageData(), 0, 0);
		//System.out.println("draw base ok");
		//int[] locations = organizeImages();
		//System.out.println("img key: "+imagekey);
		Image deco = IImageKeys.getImage(imagekey);
		//System.out.println("get deco ok");
		ImageData imageData = deco.getImageData();
		//System.out.println(imageData);
		drawImage(imageData, 0, 0);
		/*
		 * switch(locations[i])
		 * {
		 * // Draw on the top left corner
		 * case TOP_LEFT:
		 * break;
		 * // Draw on top right corner
		 * case TOP_RIGHT:
		 * drawImage(imageData, sizeOfImage_.x - imageData.width, 0);
		 * break;
		 * // Draw on bottom left
		 * case BOTTOM_LEFT:
		 * drawImage(imageData, 0, sizeOfImage_.y - imageData.height);
		 * break;
		 * // Draw on bottom right corner
		 * case BOTTOM_RIGHT:
		 * drawImage(imageData, sizeOfImage_.x - imageData.width,
		 * sizeOfImage_.y - imageData.height);
		 * break;
		 * }
		 */

	}

	/**
	 * Gets the size.
	 *
	 * @return the size
	 * @see org.eclipse.jface.resource.CompositeImageDescriptor#getSize()
	 *      get the size of the object
	 */
	@Override
	protected Point getSize() {
		return sizeOfImage_;
	}

	/**
	 * Get the image formed by overlaying dif ferent images on the base image.
	 *
	 * @return composite image
	 */
	public Image getImage() {
		return createImage();
	}
}
