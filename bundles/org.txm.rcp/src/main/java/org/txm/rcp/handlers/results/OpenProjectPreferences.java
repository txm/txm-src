package org.txm.rcp.handlers.results;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.PreferencesUtil;
import org.txm.core.results.TXMResult;
import org.txm.objects.Project;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.views.corpora.CorporaView;

public class OpenProjectPreferences extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object data = CorporaView.getFirstSelectedObject();
		System.out.println("SEL=" + data);
		if (data instanceof IProject r) {

		}
		else if (data instanceof Project r) {
			data = r.getRCPProject();
		}
		else if (data instanceof TXMResult r) {
			data = r.getProject().getRCPProject();
		}

		Shell s = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		PreferencesUtil.createPropertyDialogOn(s, data, null, null, null, 0).open();

		//WorkbenchPreferenceDialog dialog = (WorkbenchPreferenceDialog)PreferencesUtil.createPreferenceDialogOn(s, null, null, data);
		return 0;
	}
}
