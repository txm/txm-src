package org.txm.rcp.editors.imports.sections;

import java.util.HashMap;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.txm.objects.Project;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.txm.rcp.editors.imports.ImportModuleCustomization;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.tokenizer.TokenizerClasses;

public class WordsSection extends ImportEditorSection {

	static final int SECTION_SIZE = 1;

	Text wordElementText;

	Button doTokenizeStepButton;

	Button doRetokenizeStepButton;

	Button doBuildWordIDsButton;

	Text textElisions;

	Text textPuncts;

	Text textPunctsStrong;

	Text textWhiteSpaces;

	Label titleLabel;

	TableViewer tokenizerParamsViewer;

	TableViewer tokenizerParamsViewer2;

	private HashMap<String, Boolean> moduleParams;

	public WordsSection(ImportFormEditor editor, FormToolkit toolkit2, ScrolledForm form2, Composite parent, int style, HashMap<String, Boolean> moduleParams) {
		super(editor, toolkit2, form2, parent, style, "words"); //$NON-NLS-1$
		this.moduleParams = moduleParams;
		this.section.setText(TXMUIMessages.Words);
		TableWrapLayout layout = new TableWrapLayout();
		layout.makeColumnsEqualWidth = true;
		this.section.setLayout(layout);
		this.section.setLayoutData(getSectionGridData(SECTION_SIZE));
		this.section.setEnabled(false);

		this.section.addExpansionListener(new ExpansionAdapter() {

			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.layout(true);
			}
		});

		Composite sectionClient = toolkit.createComposite(this.section);
		TableWrapLayout slayout = new TableWrapLayout();
		slayout.makeColumnsEqualWidth = false;
		slayout.numColumns = 3;
		sectionClient.setLayout(slayout);
		this.section.setClient(sectionClient);

		Label tmpLabel3 = toolkit.createLabel(sectionClient, TXMUIMessages.wordTag);
		TableWrapData gdata = new TableWrapData(TableWrapData.LEFT, TableWrapData.FILL);
		gdata.colspan = 2;
		tmpLabel3.setLayoutData(gdata);
		tmpLabel3.setToolTipText(TXMUIMessages.xmlTagNameThatEncodesWords);
		tmpLabel3.setVisible(moduleParams.get(ImportModuleCustomization.ADVANCEDTOKENIZER));

		wordElementText = toolkit.createText(sectionClient, "w", SWT.BORDER); //$NON-NLS-1$
		gdata = getTextGridData();
		wordElementText.setLayoutData(gdata);
		wordElementText.setVisible(moduleParams.get(ImportModuleCustomization.ADVANCEDTOKENIZER));
		doTokenizeStepButton = toolkit.createButton(sectionClient, TXMUIMessages.tokenization, SWT.CHECK);
		doTokenizeStepButton.setSelection(true);
		doTokenizeStepButton.setVisible(moduleParams.get(ImportModuleCustomization.ADVANCEDTOKENIZER));
		gdata = new TableWrapData(TableWrapData.LEFT, TableWrapData.FILL);
		gdata.colspan = 3;
		doTokenizeStepButton.setLayoutData(gdata);
		doTokenizeStepButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				updateFieldsEnableState();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		doRetokenizeStepButton = toolkit.createButton(sectionClient, TXMUIMessages.retokenizePreEncodedWords, SWT.CHECK);
		doRetokenizeStepButton.setToolTipText(TXMUIMessages.performWordSegmentationWithinWord);
		doRetokenizeStepButton.setSelection(true);
		doRetokenizeStepButton.setVisible(moduleParams.get(ImportModuleCustomization.ADVANCEDTOKENIZER));
		gdata = new TableWrapData(TableWrapData.LEFT, TableWrapData.FILL);
		gdata.indent = 20;
		gdata.colspan = 3;
		doRetokenizeStepButton.setLayoutData(gdata);

		doBuildWordIDsButton = toolkit.createButton(sectionClient, TXMUIMessages.BuildWordIdentifiers, SWT.CHECK);
		doBuildWordIDsButton.setToolTipText(TXMUIMessages.performWordSegmentationWithinWord);
		doBuildWordIDsButton.setSelection(true);
		doBuildWordIDsButton.setVisible(moduleParams.get(ImportModuleCustomization.ADVANCEDTOKENIZER));
		gdata = new TableWrapData(TableWrapData.LEFT, TableWrapData.FILL);
		gdata.indent = 20;
		gdata.colspan = 3;
		doBuildWordIDsButton.setLayoutData(gdata);

		Label l0 = toolkit.createLabel(sectionClient, TXMUIMessages.separatorCharacters, SWT.WRAP);
		gdata = new TableWrapData(TableWrapData.LEFT, TableWrapData.FILL);
		gdata.colspan = 3;
		gdata.indent = 20;
		l0.setLayoutData(gdata);

		Label l1 = toolkit.createLabel(sectionClient, TXMUIMessages.spaces, SWT.WRAP);
		gdata = new TableWrapData(TableWrapData.LEFT, TableWrapData.FILL);
		gdata.colspan = 2;
		gdata.indent = 40;
		l1.setLayoutData(gdata);

		textWhiteSpaces = toolkit.createText(sectionClient, "", SWT.BORDER); //$NON-NLS-1$
		gdata = getLongTextGridData();
		textWhiteSpaces.setLayoutData(gdata);

		Label l2 = toolkit.createLabel(sectionClient, TXMUIMessages.punctuations, SWT.WRAP);
		gdata = new TableWrapData(TableWrapData.LEFT, TableWrapData.FILL);
		gdata.colspan = 2;
		gdata.indent = 40;
		l2.setLayoutData(gdata);

		textPuncts = toolkit.createText(sectionClient, "", SWT.BORDER); //$NON-NLS-1$
		gdata = getLongTextGridData();
		textPuncts.setLayoutData(gdata);

		Label l4 = toolkit.createLabel(sectionClient, TXMUIMessages.elisionCharacters, SWT.WRAP);
		gdata = new TableWrapData(TableWrapData.LEFT, TableWrapData.FILL);
		gdata.colspan = 2;
		gdata.indent = 20;
		l4.setLayoutData(gdata);

		textElisions = toolkit.createText(sectionClient, "", SWT.BORDER); //$NON-NLS-1$
		gdata = getLongTextGridData();
		textElisions.setLayoutData(gdata);

		Label l3 = toolkit.createLabel(sectionClient, TXMUIMessages.endOfSentenceCharacters, SWT.WRAP);
		gdata = new TableWrapData(TableWrapData.LEFT, TableWrapData.FILL);
		gdata.colspan = 2;
		gdata.indent = 20;
		l3.setLayoutData(gdata);

		textPunctsStrong = toolkit.createText(sectionClient, "", SWT.BORDER); //$NON-NLS-1$
		gdata = getLongTextGridData();
		textPunctsStrong.setLayoutData(gdata);

		Button defaultBtn = toolkit.createButton(sectionClient, TXMUIMessages._default_2, SWT.PUSH);
		gdata = new TableWrapData(TableWrapData.LEFT, TableWrapData.FILL);
		gdata.colspan = 2;
		gdata.indent = 20;
		defaultBtn.setLayoutData(gdata);
		defaultBtn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				TokenizerClasses tc = new TokenizerClasses(); // use default tokenizer classes
				textWhiteSpaces.setText(tc.whitespaces);
				textPuncts.setText(tc.regPunct);
				textPunctsStrong.setText(tc.punct_strong);
				textElisions.setText(tc.regElision);
			}
		});

		/*
		 * setAddSetTestsButton = toolkit.createButton(sectionClient, Messages.CorpusPage_28, SWT.CHECK);
		 * setAddSetTestsButton.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false,4,1));
		 * // setAddSetTestsButton.setSelection(params.getTokenizerElement(params.getCorpusElement()))
		 * Button addButton = toolkit.createButton(sectionClient, Messages.CorpusPage_29, SWT.PUSH);
		 * addButton.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false,1,1));
		 * addButton.addSelectionListener(new SelectionListener() {
		 * @Override
		 * public void widgetDefaultSelected(SelectionEvent e) {
		 * }
		 * @Override
		 * public void widgetSelected(SelectionEvent e) {
		 * if (params == null) return;
		 * LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();
		 * values.put("name", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("value", ""); //$NON-NLS-1$ //$NON-NLS-2$
		 * MultipleValueDialog dlg = new MultipleValueDialog(form.getShell(), Messages.CorpusPage_29, values);
		 * if (dlg.open() == Window.OK) {
		 * params.addTokenizerParameter(params.getCorpusElement(), values.get("name"), values.get("value")); //$NON-NLS-1$ //$NON-NLS-2$
		 * updateTokenizerSection();
		 * }
		 * }
		 * });
		 * Button removeButton = toolkit.createButton(sectionClient, Messages.CorpusPage_31, SWT.PUSH);
		 * removeButton.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false,1,1));
		 * removeButton.addSelectionListener(new SelectionListener() {
		 * @Override
		 * public void widgetDefaultSelected(SelectionEvent e) {
		 * }
		 * @Override
		 * public void widgetSelected(SelectionEvent e) {
		 * if (params == null) return;
		 * ISelection sel = tokenizerParamsViewer.getSelection();
		 * IStructuredSelection ssel = (IStructuredSelection) sel;
		 * Object obj = ssel.getFirstElement();
		 * if (obj instanceof Element) {
		 * Element query = (Element) obj;
		 * params.getTokenizerElement(params.getCorpusElement()).removeChild(query);
		 * updateTokenizerSection();
		 * }
		 * }
		 * });
		 * Button addButton2 = toolkit.createButton(sectionClient, Messages.CorpusPage_32, SWT.PUSH);
		 * addButton2.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false,1,1));
		 * addButton2.addSelectionListener(new SelectionListener() {
		 * @Override
		 * public void widgetDefaultSelected(SelectionEvent e) {
		 * }
		 * @Override
		 * public void widgetSelected(SelectionEvent e) {
		 * if (params == null) return;
		 * LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();
		 * values.put("value", "\\A(.+)(.+)(.+)\\Z"); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("type", "w"); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("before", "1"); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("hit", "2"); //$NON-NLS-1$ //$NON-NLS-2$
		 * values.put("after", "3"); //$NON-NLS-1$ //$NON-NLS-2$
		 * MultipleValueDialog dlg = new MultipleValueDialog(form.getShell(), Messages.CorpusPage_33, values);
		 * if (dlg.open() == Window.OK) {
		 * params.addTokenizerTest(params.getCorpusElement(), values.get("value"), values.get("type"), values.get("before"), values.get("hit"), values.get("after")); //$NON-NLS-1$ //$NON-NLS-2$
		 * //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		 * updateTokenizerSection();
		 * }
		 * }
		 * });
		 * Button removeButton2 = toolkit.createButton(sectionClient, Messages.CorpusPage_34, SWT.PUSH);
		 * removeButton2.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false,1,1));
		 * removeButton2.addSelectionListener(new SelectionListener() {
		 * @Override
		 * public void widgetDefaultSelected(SelectionEvent e) {
		 * }
		 * @Override
		 * public void widgetSelected(SelectionEvent e) {
		 * if (params == null) return;
		 * ISelection sel = tokenizerParamsViewer2.getSelection();
		 * IStructuredSelection ssel = (IStructuredSelection) sel;
		 * Object obj = ssel.getFirstElement();
		 * if (obj instanceof Element) {
		 * Element test = (Element) obj;
		 * params.getTokenizerElement(params.getCorpusElement()).removeChild(test);
		 * updateTokenizerSection();
		 * }
		 * }
		 * });
		 * Table table = toolkit.createTable(sectionClient, SWT.FULL_SELECTION | SWT.MULTI | SWT.V_SCROLL | SWT.VIRTUAL);
		 * tokenizerParamsViewer = new TableViewer(table);
		 * tokenizerParamsViewer.getTable().addKeyListener(new TableKeyListener(tokenizerParamsViewer));
		 * GridData gd = new GridData(GridData.FILL, GridData.FILL, false, false,2,1);
		 * gd.heightHint = 150;
		 * tokenizerParamsViewer.getTable().setLayoutData(gd);
		 * tokenizerParamsViewer.setContentProvider(new NodeListContentProvider());
		 * tokenizerParamsViewer.getTable().setHeaderVisible(true);
		 * tokenizerParamsViewer.getTable().setLinesVisible(true);
		 * String[] cols = {"name", "value"}; //$NON-NLS-1$ //$NON-NLS-2$
		 * for (String col : cols) {
		 * TableViewerColumn columnViewer = new TableViewerColumn(tokenizerParamsViewer, SWT.NONE);
		 * TableColumn column = columnViewer.getColumn();
		 * column.setText(col);
		 * column.setWidth(100);
		 * columnViewer.setLabelProvider(new ElementColumnLabelProvider(col));
		 * }
		 * Table table2 = toolkit.createTable(sectionClient, SWT.FULL_SELECTION | SWT.MULTI | SWT.V_SCROLL | SWT.VIRTUAL);
		 * tokenizerParamsViewer2 = new TableViewer(table2);
		 * tokenizerParamsViewer2.getTable().addKeyListener(new TableKeyListener(tokenizerParamsViewer2));
		 * GridData gd2 = new GridData(GridData.FILL, GridData.FILL, false, false,2,1);
		 * gd.heightHint = 150;
		 * tokenizerParamsViewer2.getTable().setLayoutData(gd2);
		 * tokenizerParamsViewer2.setContentProvider(new NodeListContentProvider());
		 * tokenizerParamsViewer2.getTable().setHeaderVisible(true);
		 * tokenizerParamsViewer2.getTable().setLinesVisible(true);
		 * String[] cols2 = {"value", "type", "before", "hit", "after"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		 * for (String col : cols2) {
		 * TableViewerColumn columnViewer = new TableViewerColumn(tokenizerParamsViewer2, SWT.NONE);
		 * TableColumn column = columnViewer.getColumn();
		 * column.setText(col);
		 * column.setWidth(100);
		 * columnViewer.setLabelProvider(new ElementColumnLabelProvider(col));
		 * }
		 */

	}

	protected void updateFieldsEnableState() {
		boolean enabled = doTokenizeStepButton.getSelection();
		doRetokenizeStepButton.setEnabled(enabled);
		doBuildWordIDsButton.setEnabled(enabled);
		if (!enabled) {
			doRetokenizeStepButton.setSelection(false);
			doBuildWordIDsButton.setSelection(false);
		}
		textWhiteSpaces.setEnabled(enabled);
		textPuncts.setEnabled(enabled);
		textElisions.setEnabled(enabled);
		textPunctsStrong.setEnabled(enabled);
	}

	@Override
	public void updateFields(Project project) {
		if (this.section.isDisposed()) return;
		if (project == null) return;

		TokenizerClasses tc = new TokenizerClasses();
		tc.loadFromProject(project.getPreferencesScope());

		textWhiteSpaces.setText(project.getTokenizerParameter("whitespaces", tc.whitespaces)); //$NON-NLS-1$
		textPuncts.setText(project.getTokenizerParameter("regPunct", tc.regPunct)); //$NON-NLS-1$
		textPunctsStrong.setText(project.getTokenizerParameter("punct_strong", tc.punct_strong)); //$NON-NLS-1$
		textElisions.setText(project.getTokenizerParameter("regElision", tc.regElision)); //$NON-NLS-1$

		String wtagElement = project.getTokenizerWordElement();
		wordElementText.setText(wtagElement);

		boolean doTokenizeStep = project.getDoTokenizerStep();
		doTokenizeStepButton.setSelection(doTokenizeStep);
		boolean doRetokenizeStep = "true".equals(project.getTokenizerParameter("doRetokenizeStep", "false")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		doRetokenizeStepButton.setSelection(doRetokenizeStep);
		boolean doBuildWordIds = "true".equals(project.getTokenizerParameter("doBuildWordIds", "true")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		doBuildWordIDsButton.setSelection(doBuildWordIds);
		updateFieldsEnableState();

		// HashMap<String, TTest> additionalTests = project.getTokenizerAdditionalTests();
		// try {
		// setAddSetTestsButton.setSelection(Boolean.parseBoolean(value.toString()));
		// } catch (Exception e) { };
	}

	@Override
	public boolean saveFields(Project project) {

		if (textWhiteSpaces.getText().length() == 0 && textPuncts.getText().length() == 0) {
			System.out.println(TXMUIMessages.errorWhileSavingTokenizerParameters);
			return false;
		}
		project.addTokenizerParameter("whitespaces", textWhiteSpaces.getText()); //$NON-NLS-1$
		project.addTokenizerParameter("regPunct", textPuncts.getText()); //$NON-NLS-1$
		project.addTokenizerParameter("punct_strong", textPunctsStrong.getText()); //$NON-NLS-1$
		project.addTokenizerParameter("regElision", textElisions.getText()); //$NON-NLS-1$
		project.addTokenizerParameter("word_tags", wordElementText.getText()); //$NON-NLS-1$
		project.setDoTokenizerStep(doTokenizeStepButton.getSelection());
		project.addTokenizerParameter("doRetokenizeStep", "" + doRetokenizeStepButton.getSelection()); //$NON-NLS-1$ //$NON-NLS-2$
		project.addTokenizerParameter("doBuildWordIds", "" + doBuildWordIDsButton.getSelection()); //$NON-NLS-1$ //$NON-NLS-2$
		project.addTokenizerParameter("onlyThoseTests", "false"); //$NON-NLS-1$ //$NON-NLS-2$
		// project.setTokenizerAdditionalTests(additionalTokenizerTests); //$NON-NLS-1$

		return true;
	}

	@Override
	public boolean checkFields() {
		return true;
	}

	@Override
	public int getSectionSize() {
		return SECTION_SIZE;
	}
}
