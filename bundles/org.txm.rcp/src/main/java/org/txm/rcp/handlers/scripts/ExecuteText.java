// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.handlers.scripts;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.txm.rcp.editors.TxtEditor;
import org.txm.rcp.messages.TXMUIMessages;

// TODO: Auto-generated Javadoc
/**
 * Handler: execute a selection of code from the TxtEditor. It uses the file extension to execute it as Groovy or R.
 * 
 * 
 * @author mdecorde.
 */
public class ExecuteText extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event)
				.getActivePage().getSelection();
		if (!(selection instanceof TextSelection)) return null;

		TextSelection t = (TextSelection) selection;
		String result = t.getText();

		IWorkbenchPart page = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();
		if (!(page instanceof TxtEditor)) return null;
		TxtEditor te = (TxtEditor) page;
		FileStoreEditorInput input = (FileStoreEditorInput) te.getEditorInput();

		File f = new File(input.getURI().getPath());
		String scriptfile = f.getAbsolutePath();

		return executeText(result, scriptfile);
	}

	/**
	 * Execute text.
	 *
	 * @param text the text
	 * @param scriptfile the scriptfile
	 * @return the object
	 */
	public static Object executeText(final String text, final String scriptfile) {
		File script = new File(scriptfile);
		if (script.getName().endsWith(".groovy")) { //$NON-NLS-1$
			ExecuteGroovyText.executeText(text, scriptfile);
		}
		else if (script.getName().endsWith(".R")) { //$NON-NLS-1$
			System.out.println(TXMUIMessages.ExecuteRTextSelectionNotImplemented);
			//ExecuteRText.executeText(text);
		}
		else {
			System.out.println(TXMUIMessages.noInterpreterFoundForScriptFileExtension + script.getName());
		}
		return null;
	}
}
