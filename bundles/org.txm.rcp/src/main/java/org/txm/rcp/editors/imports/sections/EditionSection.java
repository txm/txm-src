package org.txm.rcp.editors.imports.sections;

import java.io.File;
import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.txm.Toolbox;
import org.txm.core.preferences.TBXPreferences;
import org.txm.objects.EditionDefinition;
import org.txm.objects.Project;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.txm.rcp.editors.imports.ImportModuleCustomization;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;

public class EditionSection extends ImportEditorSection {

	String ID = "org.txm.rcp.editors.imports.sections.EditionSection"; //$NON-NLS-1$

	private static final int SECTION_SIZE = 1;

	Button buildDefaultEditionButton;

	Button paginateDefaultEditionButton;

	Button collapsibleMetadataDefaultEditionButton;

	private Text noteElementsText;

	Text wordsPerPageText;

	Text pageBreakText;

	Button buildFacsEditionCheckButton;

	Text imageDirectoryText;

	private Text defaultEditions;

	private Combo unmanagedElementsPolicyCombo;

	private Text defaultEditionTooltipPropertiesText;

	/**
	 * 
	 * @param toolkit2
	 * @param form2
	 * @param parent
	 * 
	 * @param moduleParams
	 * @param importName temporary parameter to detect if import module is xtzLoader.groovy
	 */
	public EditionSection(ImportFormEditor editor, FormToolkit toolkit2, ScrolledForm form2, Composite parent, int style, HashMap<String, Boolean> moduleParams, String importName) {
		super(editor, toolkit2, form2, parent, style, "edition"); //$NON-NLS-1$

		this.section.setText(TXMUIMessages.editions);
		TableWrapLayout layout = new TableWrapLayout();
		layout.makeColumnsEqualWidth = true;
		layout.numColumns = 1;
		this.section.setLayout(layout);
		this.section.setLayoutData(getSectionGridData(SECTION_SIZE));
		this.section.setEnabled(false);

		this.section.addExpansionListener(new ExpansionAdapter() {

			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.layout(true);
			}
		});

		// filesection.setDescription("Select how to find source files");
		Composite sectionClient = toolkit.createComposite(this.section);
		TableWrapLayout slayout = new TableWrapLayout();
		slayout.numColumns = 4;
		sectionClient.setLayout(slayout);
		this.section.setClient(sectionClient);

		// build text edition or not button
		buildDefaultEditionButton = toolkit.createButton(sectionClient, TXMUIMessages.buildEdition, SWT.CHECK);
		// buildDefaultEditionButton.setSelection(true);
		TableWrapData gdata2 = getButtonLayoutData();
		gdata2.colspan = 4; // one line
		buildDefaultEditionButton.setLayoutData(gdata2);

		collapsibleMetadataDefaultEditionButton = toolkit.createButton(sectionClient, TXMUIMessages.EnableCollapsibleMetadata, SWT.CHECK);
		// collapsibleMetadataDefaultEditionButton.setSelection(true);
		gdata2 = getButtonLayoutData();
		gdata2.colspan = 4; // one line
		gdata2.indent = 20;
		collapsibleMetadataDefaultEditionButton.setLayoutData(gdata2);

		paginateDefaultEditionButton = toolkit.createButton(sectionClient, TXMUIMessages.Paginate, SWT.CHECK);
		// paginateDefaultEditionButton.setSelection(true);
		gdata2 = getButtonLayoutData();
		gdata2.colspan = 4; // one line
		gdata2.indent = 20;
		paginateDefaultEditionButton.setLayoutData(gdata2);

		// words per page -> pageSize
		Label tmpLabel = null;
		if (moduleParams.get(ImportModuleCustomization.EDITIONS_WORDSPERPAGE)) {
			tmpLabel = toolkit.createLabel(sectionClient, TXMUIMessages.wordsPerPage);
			TableWrapData gdata = getLabelGridData();
			gdata.indent = 40;
			tmpLabel.setLayoutData(gdata);

			wordsPerPageText = toolkit.createText(sectionClient, "1000", SWT.BORDER); //$NON-NLS-1$
			gdata = getTextGridData();
			wordsPerPageText.setLayoutData(gdata);
		}

		if (moduleParams.get(ImportModuleCustomization.EDITIONS_PAGEELEMENT)) {
			// page break tag
			tmpLabel = toolkit.createLabel(sectionClient, TXMUIMessages.pageBreakTag);
			tmpLabel.setLayoutData(getLabelGridData());
			pageBreakText = toolkit.createText(sectionClient, "pb", SWT.BORDER); //$NON-NLS-1$
			TableWrapData gdata = getTextGridData();
			pageBreakText.setLayoutData(gdata);
		}

		if (importName.startsWith("xtz")) { //$NON-NLS-1$

			Label tmpLabel4 = toolkit.createLabel(sectionClient, TXMUIMessages.unmanagedElementPolicy);
			tmpLabel4.setToolTipText(TXMUIMessages.ignoreTheElementsAreNotWrittenEtc);
			TableWrapData gdata4 = getLabelGridData();
			gdata4.indent = 40;
			tmpLabel4.setLayoutData(gdata4);

			unmanagedElementsPolicyCombo = new Combo(sectionClient, SWT.BORDER | SWT.READ_ONLY);
			gdata4 = getTextGridData();
			gdata4.colspan = 3; // one line
			unmanagedElementsPolicyCombo.setLayoutData(gdata4);
			unmanagedElementsPolicyCombo.setItems("ignore", "rename to span"); //$NON-NLS-1$ //$NON-NLS-2$
			unmanagedElementsPolicyCombo.setToolTipText(TXMUIMessages.ignoreTheElementsAreNotWrittenEtc);

			Label tmpLabel3 = toolkit.createLabel(sectionClient, TXMUIMessages.NoteElements);
			tmpLabel3.setToolTipText(TXMUIMessages.NoteElementsThatEncodePonctualNotes);
			TableWrapData gdata3 = getLabelGridData();
			gdata3.indent = 40;
			tmpLabel3.setLayoutData(gdata3);

			noteElementsText = toolkit.createText(sectionClient, "note", SWT.BORDER); //$NON-NLS-1$
			gdata3 = getTextGridData();
			gdata3.colspan = 3; // one line
			noteElementsText.setLayoutData(gdata3);

			// build text edition or not button
			buildFacsEditionCheckButton = toolkit.createButton(sectionClient, TXMUIMessages.BuildFacsEdition, SWT.CHECK);
			buildFacsEditionCheckButton.setSelection(false);
			TableWrapData gdata22 = getButtonLayoutData();
			gdata22.colspan = 4; // one line
			gdata22.indent = 40;
			buildFacsEditionCheckButton.setLayoutData(gdata22);
			buildFacsEditionCheckButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					imageDirectoryText.setEnabled(buildFacsEditionCheckButton.getSelection());
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
			// image directory
			tmpLabel = toolkit.createLabel(sectionClient, TXMUIMessages.ImageDirectory);
			TableWrapData gdata = getLabelGridData();
			gdata.indent = 60;
			tmpLabel.setLayoutData(gdata);
			imageDirectoryText = toolkit.createText(sectionClient, "", SWT.BORDER); //$NON-NLS-1$
			gdata = getTextGridData();
			gdata.colspan = 2;

			imageDirectoryText.setLayoutData(gdata);
			imageDirectoryText.setEnabled(false);

			Button selectImageDirectoryButton = toolkit.createButton(sectionClient, "...", SWT.PUSH); //$NON-NLS-1$
			gdata = getButtonLayoutData();
			selectImageDirectoryButton.setLayoutData(gdata);
			selectImageDirectoryButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					DirectoryDialog dialog = new DirectoryDialog(form.getShell(), SWT.OPEN);
					if (LastOpened.getFile(ID) != null) {
						dialog.setFilterPath(LastOpened.getFolder(ID));
					}
					else {
						dialog.setFilterPath(Toolbox.getTxmHomePath());
					}
					String newPath = dialog.open();
					if (newPath != null) {
						File imgDirectory = new File(newPath);
						LastOpened.set(ID, imgDirectory.getParent(), ""); //$NON-NLS-1$
						imageDirectoryText.setText(imgDirectory.getAbsolutePath());
					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			tmpLabel = toolkit.createLabel(sectionClient, TXMUIMessages.DefaultEdition);
			gdata = getLabelGridData();
			gdata.indent = 20;
			tmpLabel.setLayoutData(gdata);

			defaultEditions = toolkit.createText(sectionClient, "", SWT.BORDER); //$NON-NLS-1$
			defaultEditions.setToolTipText(TXMUIMessages.commaSeparatedList);
			gdata = getTextGridData();
			gdata.colspan = 2;
			defaultEditions.setLayoutData(gdata);
			
			tmpLabel = toolkit.createLabel(sectionClient, "");
			gdata = getLabelGridData();
			gdata.indent = 20;
			tmpLabel.setLayoutData(gdata);

			tmpLabel = toolkit.createLabel(sectionClient, TXMUIMessages.tooltipProperties);
			tmpLabel.setToolTipText(TXMUIMessages.commaSeparatedList);
			gdata = getLabelGridData();
			gdata.indent = 20;
			tmpLabel.setLayoutData(gdata);

			defaultEditionTooltipPropertiesText = toolkit.createText(sectionClient, "", SWT.BORDER); //$NON-NLS-1$
			gdata = getTextGridData();
			gdata.colspan = 2;
			defaultEditionTooltipPropertiesText.setLayoutData(gdata);
		}

		//// First draft of interface to add editions to a corpus
		// Button addButton = toolkit.createButton(sectionClient, Messages.CorpusPage_1, SWT.PUSH);
		// addButton.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		// addButton.addSelectionListener(new SelectionListener() {
		// @Override
		// public void widgetDefaultSelected(SelectionEvent e) {
		// }
		//
		// @Override
		// public void widgetSelected(SelectionEvent e) {
		// if (params == null) return;
		// LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();
		// values.put("name", ""); //$NON-NLS-1$ //$NON-NLS-2$
		// values.put("mode", "groovy"); //$NON-NLS-1$ //$NON-NLS-2$
		// values.put("script", ""); //$NON-NLS-1$ //$NON-NLS-2$
		//
		// MultipleValueDialog dlg = new MultipleValueDialog(form.getShell(), Messages.CorpusPage_1, values);
		// if (dlg.open() == Window.OK) {
		// params.addEditionDefinition(params.getCorpusElement(), values.get("name"), values.get("mode"), values.get("script")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		// updateEditionsSection();
		// }
		// }
		// });
		//
		// Button removeButton = toolkit.createButton(sectionClient, Messages.CorpusPage_2, SWT.PUSH);
		// removeButton.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		// removeButton.addSelectionListener(new SelectionListener() {
		// @Override
		// public void widgetDefaultSelected(SelectionEvent e) {
		// }
		//
		// @Override
		// public void widgetSelected(SelectionEvent e) {
		// if (params == null) return;
		// ISelection sel = editionsViewer.getSelection();
		// IStructuredSelection ssel = (IStructuredSelection) sel;
		// Object obj = ssel.getFirstElement();
		// if (obj instanceof Element) {
		// Element query = (Element) obj;
		// params.getEditionsElement(params.getCorpusElement()).removeChild(query);
		// updateEditionsSection();
		// }
		// }
		// });
		//
		// Table table = toolkit.createTable(sectionClient, SWT.FULL_SELECTION | SWT.MULTI | SWT.V_SCROLL | SWT.VIRTUAL);
		// editionsViewer = new TableViewer(table);
		// editionsViewer.getTable().addKeyListener(new TableKeyListener(editionsViewer));
		// GridData gd = new GridData(GridData.FILL, GridData.FILL, false, false,2,1);
		// gd.heightHint = 150;
		// editionsViewer.getTable().setLayoutData(gd);
		// editionsViewer.setContentProvider(new NodeListContentProvider());
		// editionsViewer.getTable().setHeaderVisible(true);
		// editionsViewer.getTable().setLinesVisible(true);
		//
		// String[] cols = {"name", "mode", "script"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		// for (String col : cols) {
		// TableViewerColumn columnViewer = new TableViewerColumn(editionsViewer, SWT.NONE);
		// TableColumn column = columnViewer.getColumn();
		// column.setText(col);
		// column.setWidth(100);
		// columnViewer.setLabelProvider(new ElementColumnLabelProvider(col));
		// }
	}

	@Override
	public void updateFields(Project project) {
		if (this.section.isDisposed()) return;
		if (project == null) return;

		buildDefaultEditionButton.setSelection(!project.hasEditionDefinition("default")); //$NON-NLS-1$

		if (project.hasEditionDefinition("default")) { //$NON-NLS-1$

			EditionDefinition edition_params = project.getEditionDefinition("default"); //$NON-NLS-1$
			boolean be = edition_params.getBuildEdition();
			buildDefaultEditionButton.setSelection(be);

			boolean pe = edition_params.getPaginateEdition();
			paginateDefaultEditionButton.setSelection(pe);

			boolean ecm = edition_params.getEnableCollapsibleMetadata();
			collapsibleMetadataDefaultEditionButton.setSelection(ecm);

			int wppt = edition_params.getWordsPerPage();
			wordsPerPageText.setText("" + wppt); //$NON-NLS-1$

			if (noteElementsText != null) {
				noteElementsText.setText(project.getTextualPlan("Note", "note")); //$NON-NLS-1$ //$NON-NLS-2$
			}

			if (unmanagedElementsPolicyCombo != null) {
				unmanagedElementsPolicyCombo.setText(project.getTextualPlan("UnmanagedElementsPolicy", "rename to span")); //$NON-NLS-1$ //$NON-NLS-2$
			}

			String pbt = edition_params.getPageElement();
			if (pageBreakText != null) {
				pageBreakText.setText(pbt);
			}
		}

		if (defaultEditionTooltipPropertiesText != null) {
			defaultEditionTooltipPropertiesText.setText(project.getEditionDefinition("default").get(TBXPreferences.EDITION_DEFINITION_TOOLTIP_PROPERTIES, "*").replace(" ", "")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		}

		EditionDefinition edition_params = project.getEditionDefinition("facs"); //$NON-NLS-1$
		boolean bfe = edition_params.getBuildEdition();
		if (buildFacsEditionCheckButton != null && edition_params != null) {
			buildFacsEditionCheckButton.setSelection(bfe);
			imageDirectoryText.setEnabled(buildFacsEditionCheckButton.getSelection());

			String imageDirectory = edition_params.getImagesDirectory();
			if (imageDirectoryText != null && imageDirectory != null) {
				imageDirectoryText.setText(imageDirectory);
			}
		}

		String defaults = project.getDefaultEditionName();
		if (defaults != null && defaults.length() > 0 && defaultEditions != null) {
			defaultEditions.setText(defaults);
		}

	}

	@Override
	public boolean saveFields(Project project) {

		if (this.section != null && !this.section.isDisposed()) {

			String page_break_tag = "pb"; //$NON-NLS-1$

			if (pageBreakText != null) {
				page_break_tag = pageBreakText.getText();
			}
			int words_per_page = 1000;
			if (wordsPerPageText != null) {
				words_per_page = Integer.parseInt(wordsPerPageText.getText());
			}

			boolean build = buildDefaultEditionButton.getSelection();
			boolean paginate = paginateDefaultEditionButton.getSelection();
			boolean collapsible = collapsibleMetadataDefaultEditionButton.getSelection();
			EditionDefinition edition_params = project.getEditionDefinition("default"); //$NON-NLS-1$
			edition_params.setName("default"); //$NON-NLS-1$
			edition_params.setPageBreakTag(page_break_tag);
			edition_params.setWordsPerPage(words_per_page);
			edition_params.setBuildEdition(build);
			edition_params.setPaginateEdition(paginate);
			edition_params.setEnableCollapsible(collapsible);

			if (noteElementsText != null) {
				project.setTextualPlan(TXMUIMessages.Note, noteElementsText.getText());
			}

			if (unmanagedElementsPolicyCombo != null) {
				project.setTextualPlan("UnmanagedElementsPolicy", unmanagedElementsPolicyCombo.getText()); //$NON-NLS-1$
			}

			if (defaultEditionTooltipPropertiesText != null) {
				project.getEditionDefinition("default").set(TBXPreferences.EDITION_DEFINITION_TOOLTIP_PROPERTIES, defaultEditionTooltipPropertiesText.getText().replace(" ", "")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}

			if (buildFacsEditionCheckButton != null) {

				String images_directory = ""; //$NON-NLS-1$
				if (imageDirectoryText != null) {
					images_directory = imageDirectoryText.getText();
				}

				build = buildFacsEditionCheckButton.getSelection();

				EditionDefinition edition_params2 = project.getEditionDefinition("facs"); //$NON-NLS-1$
				edition_params2.setBuildEdition(build);
				edition_params2.setImagesDirectory(images_directory);
			}
			else {
				EditionDefinition edition_params2 = project.getEditionDefinition("facs"); //$NON-NLS-1$
				edition_params2.setBuildEdition(false);
			}

			if (defaultEditions != null) {
				String defaults = defaultEditions.getText();
				if (defaults != null && defaults.length() > 0) {
					project.setDefaultEditionName(defaults);
				}
			}
		}
		return true;
	}

	@Override
	public boolean checkFields() {
		return true;
	}

	@Override
	public int getSectionSize() {
		return SECTION_SIZE;
	}
}
