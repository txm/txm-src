package org.txm.textsbalance.rcp.editors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.core.results.Parameter;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.rcp.swt.widget.structures.StructuralUnitPropertiesComboViewer;
import org.txm.rcp.swt.widget.structures.StructuralUnitsComboViewer;
import org.txm.rcp.swt.widget.structures.StructuralUnitsCombosGroup;
import org.txm.textsbalance.core.functions.TextsBalance;
import org.txm.textsbalance.core.preferences.TextsBalancePreferences;
import org.txm.textsbalance.rcp.Messages;

/**
 * Texts balance editor.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class TextsBalanceEditor extends ChartEditor<TextsBalance> {


	/**
	 * Group by words.
	 */
	protected Button countWordsButton;



	/**
	 * Structural unit to explore.
	 */
	@Parameter(key = TextsBalancePreferences.STRUCTURAL_UNIT)
	protected StructuralUnitsComboViewer structuralUnitsComboViewer;

	/**
	 * Structural unit property to explore.
	 */
	@Parameter(key = TextsBalancePreferences.STRUCTURAL_UNIT_PROPERTY)
	protected StructuralUnitPropertiesComboViewer structuralUnitPropertiesComboViewer;

	/**
	 * Group by texts or not.
	 */
	@Parameter(key = TextsBalancePreferences.COUNT_TEXTS)
	protected Button groupByTexts;



	@Override
	public void __createPartControl() {

		if (((TextsBalance) this.getResult()).getCorpus() == null) {
			System.out.println(Messages.ErrorNoCorpus);
			return;
		}


		// for group tests
		Composite parametersArea = this.getExtendedParametersGroup();

		Group group = new Group(parametersArea, SWT.NONE);

		group.setText(Messages.Count);
		group.setLayout(new RowLayout());

		this.groupByTexts = new Button(group, SWT.RADIO);
		this.groupByTexts.setText("texts"); //$NON-NLS-1$
		this.groupByTexts.setSelection(this.getResult().isGroupByTexts());

		this.countWordsButton = new Button(group, SWT.RADIO);
		this.countWordsButton.setText("words"); //$NON-NLS-1$
		this.countWordsButton.setSelection(!groupByTexts.getSelection());


		// Structural units and properties combo viewers
		StructuralUnitsCombosGroup structuration = new StructuralUnitsCombosGroup(this.getMainParametersComposite(), this, this.getResult().getStructuralUnit(),
				this.getResult().getStructuralUnitProperty(), false);

		// Structural Unit
		structuralUnitsComboViewer = structuration.getStructuralUnitsComboViewer();

		// Structural Unit Property
		structuralUnitPropertiesComboViewer = structuration.getStructuralUnitPropertiesComboViewer();

		//supCombo.addSelectionListener(listener);
		groupByTexts.addSelectionListener(new ComputeSelectionListener(this));
		countWordsButton.addSelectionListener(new ComputeSelectionListener(this));
	}

	@Override
	public void updateEditorFromChart(boolean update) {
		// nothing to do
	}
}
