// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.textsbalance.rcp.adapters;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.FrameworkUtil;
import org.txm.rcp.adapters.TXMResultAdapter;
import org.txm.rcp.adapters.TXMResultAdapterFactory;
import org.txm.textsbalance.core.functions.TextsBalance;


/**
 * A factory for creating Adapter objects.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class TextsBalanceAdapterFactory extends TXMResultAdapterFactory {


	public static final ImageDescriptor ICON = AbstractUIPlugin.imageDescriptorFromPlugin(FrameworkUtil.getBundle(TextsBalanceAdapterFactory.class).getSymbolicName(),
			"platform:/plugin/" + FrameworkUtil.getBundle(TextsBalanceAdapterFactory.class).getSymbolicName() + "/icons/balance.png"); //$NON-NLS-1$ //$NON-NLS-2$


	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (this.canAdapt(adapterType) && adaptableObject instanceof TextsBalance) {
			return new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return ICON;
				}
			};
		}
		return null;
	}


}
