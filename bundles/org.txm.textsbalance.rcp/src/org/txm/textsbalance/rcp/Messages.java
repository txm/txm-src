package org.txm.textsbalance.rcp;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.textsbalance.rcp.messages"; //$NON-NLS-1$

	public static String Count;

	public static String ErrorNoCorpus;

	public static String GroupByWords;

	public static String Method;

	public static String TextsBalanceCharts;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
