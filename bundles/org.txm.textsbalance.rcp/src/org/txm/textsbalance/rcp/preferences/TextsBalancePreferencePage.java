package org.txm.textsbalance.rcp.preferences;

// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté

// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//


import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.textsbalance.core.preferences.TextsBalancePreferences;
import org.txm.textsbalance.rcp.Messages;
import org.txm.textsbalance.rcp.adapters.TextsBalanceAdapterFactory;

/**
 * Texts balance preference page.
 * 
 * @author sjacquot
 *
 */
public class TextsBalancePreferencePage extends TXMPreferencePage {



	@Override
	public void createFieldEditors() {

		// Charts rendering
		Composite chartsTab = SWTChartsComponentsProvider.createChartsRenderingPreferencesTabFolderComposite(this.getFieldEditorParent());


		// FIXME: input text
		//		IntegerFieldEditor method = new IntegerFieldEditor(TextsBalancePreferencesInitializer.METHOD, "Method", this.getFieldEditorParent());
		//		method.setValidRange(1, 2);
		//		method.setTextLimit(1);
		//		method.getTextControl(getFieldEditorParent()).setToolTipText("Method (1: uses CQL to solve matches, 2 uses corpus struct indexes, 2 is faster!)");
		//		this.addField(method);

		//		String[][] methods = new String[2][2];
		//		for(int i = 0; i < 2; i++) {
		//			methods[i][0] = String.valueOf(i + 1);
		//			methods[i][1] = String.valueOf(i + 1);
		//		}

		//this.addField(new ComboFieldEditor(TextsBalancePreferences.METHOD, Messages.Method, methods, chartsTab));
		this.addField(new BooleanFieldEditor(TextsBalancePreferences.COUNT_TEXTS, Messages.GroupByWords, chartsTab));

		// other shared preferences
		SWTChartsComponentsProvider.createChartsRenderingPreferencesFields(this, chartsTab);

	}



	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(TextsBalancePreferences.getInstance().getPreferencesNodeQualifier()));
		this.setDescription(Messages.TextsBalanceCharts);
		this.setImageDescriptor(TextsBalanceAdapterFactory.ICON);
	}

}
