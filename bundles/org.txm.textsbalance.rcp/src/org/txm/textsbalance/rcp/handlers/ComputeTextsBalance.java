// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.textsbalance.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.textsbalance.core.functions.TextsBalance;

/**
 * Computes and opens a texts balance editor.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class ComputeTextsBalance extends BaseAbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object selection = this.getCorporaViewSelectedObject(event);
		TextsBalance textsBalance = null;

		// creates new result
		if (selection instanceof CQPCorpus) {
			textsBalance = new TextsBalance((CQPCorpus) selection);
		}
		// reopens existing result
		else if (selection instanceof TextsBalance) {
			textsBalance = (TextsBalance) selection;
		}

		// Create and open the chart editor
		ChartEditor.openEditor(textsBalance);


		// FIXME: test d'utilisation de la même commande pour la création d'un nouvel éditeur et pour la mise à jour d'un graphique, avec une commande paramétrée (voir plugin.xml)
		//		if(event.getParameter("TextsBalance.createNewEditor") == null)	{
		//			ISelection sel = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		//			if (!(sel instanceof IStructuredSelection)) return null;
		//			selection = (IStructuredSelection) sel;
		//	
		//			Object s = selection.getFirstElement();
		//			if (s instanceof Corpus) {
		//				Corpus corpus = (Corpus) s;
		//				if (!openEditor(corpus)) {
		//					System.out.println("Cannot open Balance editor with "+s);
		//				}
		//			}
		//		}
		//		else	{
		//			//System.out.println("ComputeTextsBalance.execute() " + ((Event)event.getTrigger()).widget);
		//			((BalanceEditor)((ChartEditorToolBar)((ToolItem)((Event)event.getTrigger()).widget).getParent()).getEditorPart()).compute();
		//			((BalanceEditor)((ChartEditorToolBar)((ToolItem)((Event)event.getTrigger()).widget).getParent()).getEditorPart()).reloadChart();
		//			
		//		}



		return null;
	}



}
