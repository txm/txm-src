package org.txm.nlp.core;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.osgi.util.NLS;
import org.txm.core.engines.EngineType;
import org.txm.core.engines.EnginesManager;
import org.txm.utils.logger.Log;


/**
 * 
 * @author mdecorde
 **/
public class NLPEnginesManager extends EnginesManager<NLPEngine> {

	private static final long serialVersionUID = -6288338617609951960L;

	public NLPEnginesManager() {

	}

	@Override
	public boolean startEngines(IProgressMonitor monitor) {

		for (NLPEngine e : values()) {

			// lazy mode

			// NLPEngine se = (NLPEngine)e;
			// if (monitor != null) monitor.subTask("Starting "+ se.getName()+" annotation engine.");
			// try {
			// se.start(monitor);
			// } catch (Exception ex) {
			// System.out.println("Error: failed to start annotation engine: "+se.getName()+": "+ex.getLocalizedMessage());
			// }
		}
		return true;
	}


	@Override
	public boolean stopEngines() {
		for (NLPEngine e : values()) {
			// System.out.println(e);
			NLPEngine se = e;
			Log.fine("Stoping " + se.getName() + " annotation engine."); //$NON-NLS-1$ //$NON-NLS-2$
			try {
				se.stop();
			}
			catch (Exception ex) {
				Log.severe(NLS.bind(Messages.ErrorFailedToStopNLPEngineP0P1, se.getName(), ex.getLocalizedMessage()));
			}
		}
		return true;
	}

	@Override
	public EngineType getEnginesType() {
		return EngineType.NLP;
	}

	@Override
	public boolean fetchEngines() {

		IConfigurationElement[] contributions = Platform.getExtensionRegistry().getConfigurationElementsFor(NLPEngine.EXTENSION_POINT_ID);
		// System.out.println("search engine contributions: "+SearchEngine.EXTENSION_POINT_ID);
		for (int i = 0; i < contributions.length; i++) {
			try {
				NLPEngine e = (NLPEngine) contributions[i].createExecutableExtension("class"); //$NON-NLS-1$
				if (e.initialize()) {
					put(e.getName(), e);
				}
				else {
					Log.warning(NLS.bind(Messages.FailToInitializeTheP0NLPEngine, e.getName()));
				}
			}
			catch (Exception e) {
				Log.warning(NLS.bind(Messages.FailToInstanciateP0P1, contributions[i].getName(), e.getLocalizedMessage()));
				Log.printStackTrace(e);
			}
		}

		return size() > 0;
	}

}
