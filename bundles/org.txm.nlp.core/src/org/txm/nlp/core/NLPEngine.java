package org.txm.nlp.core;

import java.io.File;
import java.io.FileFilter;
import java.util.HashMap;

import org.eclipse.osgi.util.NLS;
import org.txm.core.engines.Engine;
import org.txm.core.results.TXMResult;
import org.txm.tokenizer.StringTokenizer;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.DeleteDir;
import org.txm.utils.logger.Log;

public abstract class NLPEngine implements Engine {

	public static final String EXTENSION_POINT_ID = NLPEngine.class.getCanonicalName();

	@Override
	public abstract String getName();


	/**
	 * 
	 * @return a StringTokenizer, override this method if the engine needs a speciq tokenization
	 * @throws Exception
	 */
	public StringTokenizer getStringTokenizer(String lang) throws Exception {
		return null;
	}

	/**
	 * Automatically annotate a XML-TXM file
	 * 
	 * @param xmlFile to annotate
	 * @param binaryCorpusDirectory
	 * @param parameters
	 * @return
	 */
	public abstract boolean processFile(File xmlFile, File binaryCorpusDirectory, HashMap<String, Object> parameters);

	/**
	 * 
	 * @param xmlFilesDirectory contains the *.xml files to process
	 * @param binaryCorpusDirectory the output directory
	 * @param parameters ["lang": model_file_name_without_extension]
	 * @return
	 */
	public boolean processDirectory(File xmlFilesDirectory, File binaryCorpusDirectory, HashMap<String, Object> parameters) {
		File[] files = xmlFilesDirectory.listFiles(new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				return pathname.isFile() && !pathname.isHidden() && !pathname.getName().startsWith(".") && pathname.getName().endsWith(".xml"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		});
		if (files == null || files.length == 0) {
			Log.warning(NLS.bind(Messages.ErrorNoSuitableFileToProcessInP0, xmlFilesDirectory));
			return false;
		}
		boolean ret = true;
		ConsoleProgressBar cpb = new ConsoleProgressBar(files.length);

		File annotDir = new File(binaryCorpusDirectory, "annotations"); //$NON-NLS-1$
		DeleteDir.deleteDirectory(annotDir);
		annotDir.mkdir();
		File ptreetaggerDir = new File(binaryCorpusDirectory, "ptreetagger"); //$NON-NLS-1$
		DeleteDir.deleteDirectory(ptreetaggerDir);
		ptreetaggerDir.mkdir();
		File treetaggerDir = new File(binaryCorpusDirectory, "treetagger"); //$NON-NLS-1$
		DeleteDir.deleteDirectory(treetaggerDir);
		treetaggerDir.mkdir();

		for (File xmlFile : files) {
			ret = processFile(xmlFile, binaryCorpusDirectory, parameters) && ret;

			cpb.tick();
		}
		cpb.done();
		return ret;
	}

	public String hasAdditionalDetailsForResult(TXMResult result) {
		return null;
	}

	public String getAdditionalDetailsForResult(TXMResult result) {
		return null;
	}
}
