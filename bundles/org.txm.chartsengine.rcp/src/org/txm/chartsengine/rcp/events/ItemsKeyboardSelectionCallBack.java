/**
 * 
 */
package org.txm.chartsengine.rcp.events;

import java.awt.event.KeyEvent;

/**
 * @author sjacquot
 *
 */
public abstract class ItemsKeyboardSelectionCallBack extends EventCallBack {


	/**
	 * Modifier key mask to use for extended selection.
	 */
	public static int keyboardExtendedSelectionModifierKeyMask = KeyEvent.SHIFT_MASK;

	/**	
	 * 
	 */
	public ItemsKeyboardSelectionCallBack() {
		// TODO Auto-generated constructor stub
	}



}
