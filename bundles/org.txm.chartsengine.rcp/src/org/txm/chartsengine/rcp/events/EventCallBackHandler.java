/**
 * 
 */
package org.txm.chartsengine.rcp.events;

import java.util.ArrayList;

import org.txm.chartsengine.rcp.IChartComponent;
import org.txm.utils.logger.Log;

/**
 * Abstract chart event handler.
 * This is the superclass of custom listeners dedicated to various chart types.
 * 
 * @author sjacquot
 *
 */
public abstract class EventCallBackHandler {


	/**
	 * The chart UI component.
	 */
	protected IChartComponent chartComponent;

	/**
	 * Registered event call backs.
	 */
	protected ArrayList<EventCallBack> eventCallBacks;


	/**
	 * 
	 * @param chartComponent
	 */
	public EventCallBackHandler(IChartComponent chartComponent) {
		this.chartComponent = chartComponent;
		this.eventCallBacks = new ArrayList<EventCallBack>();
	}

	/**
	 * Registers an event callback.
	 * Only one event callback is allowed for a same class type.
	 * 
	 * @param eventCallBack
	 */
	public void registerEventCallBack(EventCallBack eventCallBack) {
		// Do not add to event call backs of same type
		for (int i = 0; i < this.eventCallBacks.size(); i++) {
			if (this.eventCallBacks.get(i).getClass().equals(eventCallBack.getClass())) {
				Log.fine(String.format("Event call back handler warning: the event call back of type \"%s\" has not been registered because one of same type is already registered.", //$NON-NLS-1$
						eventCallBack.getClass()));
				return;
			}
		}
		this.eventCallBacks.add(eventCallBack);
	}


	/**
	 * Gets the first call back specified by its class.
	 * 
	 * @param type
	 * @return
	 */
	public EventCallBack getEventCallBack(Class type) {
		EventCallBack callBack = null;

		for (int i = 0; i < this.eventCallBacks.size(); i++) {
			if (this.eventCallBacks.get(i).getClass().equals(type)) {
				callBack = this.eventCallBacks.get(i);
			}
		}

		return callBack;
	}

}
