package org.txm.chartsengine.rcp.events;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import org.txm.chartsengine.rcp.IChartComponent;
import org.txm.utils.OSDetector;

/**
 * Abstract chart mouse listener.
 * This is the superclass of custom mouse listeners dedicated to various chart types.
 * 
 * @author sjacquot
 *
 */
public abstract class AbstractChartMouseListener extends EventCallBackHandler {


	/**
	 * Modifier key mask to use for chart entities (e.g. CA points) multiple selection.
	 */
	public static int mouseMultipleSelectionModifierKeyMask = MouseEvent.CTRL_MASK; // Default mask for Windows and Linux


	/**
	 * Modifier key mask to use for extended selection.
	 */
	public static int keyboardExtendedSelectionModifierKeyMask = KeyEvent.SHIFT_MASK;

	/**
	 * Define the modifier keys for Mac Os X.
	 */
	static {
		if (OSDetector.isFamilyMac()) {
			mouseMultipleSelectionModifierKeyMask = MouseEvent.META_MASK;
		}
	}


	/**
	 *
	 * @param chartPanel
	 */
	public AbstractChartMouseListener(IChartComponent chartPanel) {
		super(chartPanel);

		// FIXME: Add default mouse handler for zoom and pan here rather than in subclasses ?


	}



}
