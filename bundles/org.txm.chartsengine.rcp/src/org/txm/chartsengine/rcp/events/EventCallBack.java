/**
 * 
 */
package org.txm.chartsengine.rcp.events;

import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.core.results.TXMResult;

/**
 * Event call back to manage user entries events in the charts in all chart engine implementations.
 * 
 * @author sjacquot
 *
 */
public abstract class EventCallBack<CE extends ChartEditor<? extends TXMResult>> {


	/**
	 * Area of event constants.
	 */
	public final static int AREA_ITEM = 0, AREA_EMPTY = 1, AREA_LEGEND = 2;


	/**
	 * The linked Chart editor part.
	 */
	protected CE chartEditor;

	/**
	 * 
	 */
	public EventCallBack() {
	}



	/**
	 * Processes the call back.
	 * 
	 * @param event
	 * @param eventArea
	 * @param customData
	 */
	public abstract void processEvent(Object event, int eventArea, Object customData);

	/**
	 * @param chartEditor the chartEditor to set
	 */
	public void setChartEditor(CE chartEditor) {
		this.chartEditor = chartEditor;
	}



	/**
	 * @return the chartEditor
	 */
	public CE getChartEditor() {
		return chartEditor;
	}

}
