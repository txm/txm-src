package org.txm.chartsengine.rcp.events;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import org.txm.chartsengine.rcp.IChartComponent;

/**
 * Chart key listener.
 * This is the superclass of custom key listeners dedicated to various chart types.
 * 
 * @author sjacquot
 *
 */
public class ChartKeyListener extends EventCallBackHandler implements KeyListener {



	/**
	 * Creates a chart key listener and registers the default zoom and pan call backs handler.
	 * 
	 * @param chartComponent
	 */
	public ChartKeyListener(IChartComponent chartComponent, double keyPressedPanFactor) {
		super(chartComponent);

		// register default zoom and pan call back
		this.registerEventCallBack(new ZoomAndPanCallBack(keyPressedPanFactor));
	}


	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}


	@Override
	public void keyPressed(KeyEvent event) {
		// Process event call backs
		for (int i = 0; i < this.eventCallBacks.size(); i++) {
			this.eventCallBacks.get(i).processEvent(event, -1, this.chartComponent);
		}
	}


	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
