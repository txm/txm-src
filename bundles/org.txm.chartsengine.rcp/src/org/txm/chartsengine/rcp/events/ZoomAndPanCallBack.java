package org.txm.chartsengine.rcp.events;

import java.awt.event.KeyEvent;

import org.txm.chartsengine.rcp.IChartComponent;
import org.txm.utils.OSDetector;

/**
 * Zoom and pan user entries call back.
 * 
 * @author sjacquot
 *
 */
public class ZoomAndPanCallBack extends EventCallBack {


	/**
	 * Factor to use for panning on key pressed.
	 */
	protected double keyPressedPanFactor;

	/**
	 * Modifier key mask to use for zoom from keyboard.
	 */
	public static int keyboardZoomModifierKeyMask = KeyEvent.CTRL_MASK; // Default mask for Windows and Linux

	/**
	 * Define the modifier keys for Mac Os X.
	 */
	static {
		if (OSDetector.isFamilyMac()) {
			keyboardZoomModifierKeyMask = KeyEvent.META_MASK;
		}
	}



	/**
	 * 
	 * @param keyPressedPanFactor
	 */
	public ZoomAndPanCallBack(double keyPressedPanFactor) {
		super();
		this.keyPressedPanFactor = keyPressedPanFactor;
	}


	@Override
	public void processEvent(Object event, int eventArea, Object customData) {

		// Keyboard event
		if (event instanceof KeyEvent) {

			KeyEvent keyEvent = (KeyEvent) event;
			IChartComponent chartComponent = (IChartComponent) customData;

			// Zoom
			if ((keyEvent.getModifiers() & keyboardZoomModifierKeyMask) != 0) {
				// Zoom in
				if (keyEvent.getKeyCode() == KeyEvent.VK_ADD || keyEvent.getKeyCode() == KeyEvent.VK_EQUALS) {
					chartComponent.zoom(0, 0, true, true, true);
				}
				// Zoom out
				else if (keyEvent.getKeyCode() == KeyEvent.VK_SUBTRACT || keyEvent.getKeyCode() == KeyEvent.VK_6) {
					chartComponent.zoom(0, 0, false, true, true);
				}
				// Reset view
				else if (keyEvent.getKeyCode() == KeyEvent.VK_0 || keyEvent.getKeyCode() == KeyEvent.VK_NUMPAD0) {
					chartComponent.resetView();
				}
				// Pan
				else if (keyEvent.getKeyCode() == KeyEvent.VK_LEFT || keyEvent.getKeyCode() == KeyEvent.VK_RIGHT || keyEvent.getKeyCode() == KeyEvent.VK_DOWN
						|| keyEvent.getKeyCode() == KeyEvent.VK_UP) {

							double dstX = 0;
							double dstY = 0;

							if (keyEvent.getKeyCode() == KeyEvent.VK_LEFT) {
								dstX = -1;
							}
							else if (keyEvent.getKeyCode() == KeyEvent.VK_RIGHT) {
								dstX = 1;
							}
							else if (keyEvent.getKeyCode() == KeyEvent.VK_DOWN) {
								dstY = -1;
							}
							else if (keyEvent.getKeyCode() == KeyEvent.VK_UP) {
								dstY = 1;
							}

							chartComponent.pan(0, 0, dstX, dstY, keyPressedPanFactor);
						}

			}
		}
	}

}
