package org.txm.chartsengine.rcp.swt;

import org.eclipse.jface.window.DefaultToolTip;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;

/**
 * Custom JFace tool tip.
 * 
 * @author sjacquot
 *
 */
// FIXME: tests for SWT/JFace tool tips in chart editors instead of Swing tool tips
public class ___CustomHTMLToolTip extends DefaultToolTip {



	protected Composite comp;

	protected Browser browser;


	public ___CustomHTMLToolTip(Composite parent, int style, boolean manualActivation) {
		super(parent, style, manualActivation);

		this.browser = new Browser(parent, SWT.NO_BACKGROUND);



		// FIXME: test avec composite
		//		this.comp = new Composite(parent, SWT.NONE);
		//		
		//		//GridLayout l = new GridLayout(1, false);
		////		l.horizontalSpacing = 0;
		////		l.marginWidth = 0;
		////		l.marginHeight = 0;
		////		l.verticalSpacing = 0;
		////
		//		comp.setLayout(new GridLayout(1, false));
		//		comp.setSize(100, 80);
		//
		//		this.browser = new Browser(this.comp, SWT.NO_BACKGROUND);


	}

	@Override
	protected Composite createToolTipContentArea(Event event, Composite parent) {
		//if(event.text != null)	{
		//Composite comp = new Composite(parent, SWT.NONE);
		//			GridLayout l = new GridLayout(1, false);
		//			l.horizontalSpacing = 0;
		//			l.marginWidth = 0;
		//			l.marginHeight = 0;
		//			l.verticalSpacing = 0;
		//
		//			comp.setLayout(l);
		//comp.setLayout(new FillLayout());

		// final Shell shell = new Shell(parent.getDisplay(), SWT.NO_TRIM | SWT.ON_TOP);
		//if(this.browser.isDisposed())	{
		this.browser = new Browser(parent, SWT.NO_BACKGROUND);
		//}

		//this.browser = new Browser(parent, SWT.NO_BACKGROUND);
		//			browser.addPaintListener(new PaintListener() {
		//				    public void paintControl(PaintEvent e) {
		//				    	System.out.println("___CustomHTMLToolTip.createToolTipContentArea(...).new PaintListener() {...}.paintControl()");
		//					      GC gc=e.gc;
		//					      Color red=new Color(null,255,0,0);
		//					      gc.setBackground(red);
		//					      Rectangle rect=browser.getBounds();
		//					      Rectangle rect1 = new Rectangle(rect.x-1, rect.y-1, 
		//					    		  rect.width+2, rect.height+2);
		//					
		//					      gc.fillRectangle(rect1);
		////					      addControlListener(new ControlAdapter() {
		////					        public void controlResized(ControlEvent e) {
		////					          super.controlResized(e);        
		////					          }
		////					      });
		//					    }
		//					 });
		//browser.setForeground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		browser.setText(getText(event));
		//browser.setLayout(new FillLayout());
		browser.setLayout(new GridLayout(1, false));
		browser.setSize(100, 80);


		//browser.setBackgroundMode(SWT.TRANSPARENT);

		//browser.setBackgroundMode(SWT.TRANSPARENT);
		//browser.pack();
		//browser.setLayoutData(new GridData(200, 150));
		return browser;
		//return comp;
		//		}
		//		else	{
		//			return null;
		//		}
	}

}
