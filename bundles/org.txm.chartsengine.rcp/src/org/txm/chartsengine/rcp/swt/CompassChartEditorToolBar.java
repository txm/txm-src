package org.txm.chartsengine.rcp.swt;

import org.eclipse.swt.widgets.Composite;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.rcp.swt.GLComposite;

/**
 * The default advanced chart editor tool bar shared by every <code>ChartEditor</code>.
 * The tool bar is accessible and usable in plugin.xml with the URI toolbar:AdvancedChartEditorToolBar.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class CompassChartEditorToolBar extends ChartEditorToolBar {

	public final static String ID = "CompassChartEditorToolBar"; //$NON-NLS-1$



	/**
	 * Creates a new chart editor tool bar dedicated to advanced parameters.
	 * 
	 * @param parent
	 * @param style
	 */
	public CompassChartEditorToolBar(ChartEditor<? extends ChartResult> chartEditor, Composite parent, GLComposite subWidgetsComposite, int style) {
		super(chartEditor, parent, subWidgetsComposite, style, CompassChartEditorToolBar.ID);



	}



	@Override
	protected void checkSubclass() {
	} // if this method is not defined then the ToolBar cannot be subclassed


}
