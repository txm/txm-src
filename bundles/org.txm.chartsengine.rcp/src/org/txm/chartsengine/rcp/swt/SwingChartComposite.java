/**
 * 
 */
package org.txm.chartsengine.rcp.swt;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.event.WindowListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

import org.eclipse.jface.util.Util;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.events.EventCallBackHandler;
import org.txm.chartsengine.rcp.messages.ChartsEngineUIMessages;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.OSDetector;
import org.txm.utils.logger.Log;





/**
 * Base chart composite with Swing/AWT support.
 * This composite uses the SWT_AWT bridge to add some Swing/AWT chart components through an AWT Frame.
 * 
 * @author sjacquot
 *
 */
// FIXME: SJ: this class contains a lot of tests to fix the SWT/AWT focus bugs and needs a full clean up
public abstract class SwingChartComposite extends ChartComposite {


	// FIXME: SJ, 2025-01-16: manage a SWT bug in SWT_AWT bridge that leads to wrong AWT Frame size
	// see: https://gitlab.huma-num.fr/txm/txm-src/-/issues/4128
	/**
	 * To fix or not Windows screen scaling SWTçAWT bridge bug.
	 */
	public static boolean needToFixAWTFrameSize = false;
	protected int fixAWTFrameOriginalWidth = 0;
	protected int fixAWTFrameOriginalHeight = 0;

	
	static {
		
		// To reduce resize flickering on Windows
		try {
			System.setProperty("sun.awt.noerasebackground", "true"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (NoSuchMethodError error) {
		}
		
		
		// FIXME: SJ, 2025-01-16: manage a SWT bug in SWT_AWT bridge that leads to wrong AWT Frame size
		// see: https://gitlab.huma-num.fr/txm/txm-src/-/issues/4128
		if(Util.isWindows()) {
			
			AffineTransform screenScalingTransform = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getDefaultTransform();
			double scaleX = screenScalingTransform.getScaleX();
			double scaleY = screenScalingTransform.getScaleY();
			
			Log.finest(NLS.bind("SwingChartComposite: screen scaling factor x,y = {0},{1}.", scaleX, scaleY));
		
			if((scaleX != 1 || scaleY != 1)) {
				needToFixAWTFrameSize = true;
				Log.finest("SwingChartComposite: need to fix AWT Frame size.");
			}
		}
		// END OF FIXME
		
	}


	/**
	 * The embedded AWT Frame.
	 */
	protected Frame frame;

	// FIXME: tests
	// protected Panel rootPanel;
	// protected JRootPane rootPanel;
	protected JPanel rootPanel;

	// protected JComponent rootPanel;



	/**
	 * @param chartEditor
	 * @param parent
	 * @param style
	 */
	public SwingChartComposite(ChartEditor<? extends ChartResult> chartEditor, Composite parent, int style) {
		super(chartEditor, parent, style);

		this.frame = SWT_AWT.new_Frame(this);

		// FIXME: SJ, 2025-01-16: manage a SWT bug in SWT_AWT bridge that leads to wrong AWT Frame size
		// see: https://gitlab.huma-num.fr/txm/txm-src/-/issues/4128
		if(needToFixAWTFrameSize) {
			this.frame.addComponentListener(new ComponentAdapter() {
				protected int times = 0;
			
	            @Override
	            public void componentResized(ComponentEvent e) {
	            	//FIXME: SJ: Debug
	            	//System.err.println("SwingChartComposite.SwingChartComposite(...).new ComponentAdapter() {...}.componentResized()" + String.format("SwingChartComposite: Storing AWT Frame size = %d x %d to fix SWT_AWT bridge bug.", frame.getWidth(), frame.getHeight()));
	            	if(times++ == 0) {
		            	Log.finest(String.format("SwingChartComposite: Storing AWT Frame size = %d x %d to fix SWT_AWT bridge bug.", frame.getWidth(), frame.getHeight())); //$NON-NLS-1$
	            		fixAWTFrameOriginalWidth = frame.getWidth();
	            		fixAWTFrameOriginalHeight = frame.getHeight();
	            	}
	            	else {
                    	Log.finest(String.format("SwingChartComposite: Restoring AWT Frame size = %d x %d to fix SWT_AWT bridge bug.", frame.getWidth(), frame.getHeight())); //$NON-NLS-1$
        				frame.setSize(fixAWTFrameOriginalWidth, fixAWTFrameOriginalHeight);
	        			frame.removeComponentListener(this);
	            	}
	            }
	        });
		}
		// END OF FIXME
		
		
		// Need to add an AWT/Swing Panel to fix mouse events and mouse cursors changes on AWT/Swing chart component panel (see: https://bugs.java.com/bugdatabase/view_bug.do?bug_id=4982522)
		this.rootPanel = new JPanel(new BorderLayout());
		this.frame.add(this.rootPanel);

		// FIXME: tests with Applet, see: https://www.eclipse.org/articles/article.php?file=Article-Swing-SWT-Integration/index.html
		// NOTE SJ: avec ce code le changement de moteur à la volée ne fonctionne pas sous Mac OS X
		// final JApplet applet = new JApplet();
		// this.rootPanel = (JComponent) applet.getRootPane().getContentPane();
		// this.frame.add(applet);

		// FIXME: SJ: temporary workaround to Linux focus bugs
		// it breaks all the key events in charts (zoom, pan, selection, etc.) but
		// stops the conflict between AWT and SWT events that freezes the SWT Spinner, TextFields, etc.
		// (Progression query, Specificities chart banality, etc.)
		// see: https://gitlab.huma-num.fr/txm/txm-src/-/issues/2548 (http://forge.cbp.ens-lyon.fr/redmine/issues/2548)
		if (OSDetector.isFamilyUnix()) {
			this.frame.setAutoRequestFocus(false);
		}


		this.frame.setVisible(true);

		this.rootPanel.setBackground(Color.WHITE);
		this.rootPanel.setBorder(new LineBorder(Color.WHITE, 1));


		// Change the border of the root panel to show to user the chart is focused
		this.frame.addWindowFocusListener(new WindowFocusListener() {

			@Override
			public void windowLostFocus(WindowEvent e) {
				((JComponent) rootPanel).setBorder(new LineBorder(Color.WHITE, 1));
			}

			@Override
			public void windowGainedFocus(WindowEvent e) {
				((JComponent) rootPanel).setBorder(new LineBorder(Color.GRAY, 1));

				// FIXME: SJ: SWT/AWT focus bugs thread tests, need to wait to see how SWT/AWT bugs will be fixed before removing this
				// getDisplay().asyncExec(new Runnable() {
				// @Override
				// public void run() {
				//
				// //SwingChartComposite.this.notifyListeners(SWT.MouseDown, SWTChartsComponentsProvider.swingEventToSWT(chartEditor.getComposite(), swingComponent, e, SWT.MouseDown));
				//
				// // FIXME: Debug
				//// System.out.println("SWTChartsComponentProvider.initializeAWTDelegationListeners(...).new MouseListener() {...}.mousePressed(...).new Runnable() {...}.run()");
				// // Activate the editor part on AWT mouse pressed event
				// //SwingChartComposite.this.chartEditor.activate();
				// //chartEditor.getShell().setFocus();
				//// chartEditor.setFocus();
				// //chartEditor.getParent().setFocus();
				//// chartEditor.forceFocus();
				// SwingChartComposite.this.chartEditor.setFocus();
				// //notifyListeners(SWT.FocusIn, null); // Needed to force the composite listener to handle the focus event
				// }
				// });
				//


			}
		});


		// FIXME: SJ: temporary workaround to Linux focus bugs
		// part #1: release the focus at mouse exited on the rootPanel, needed for empty charts, eg. Progression
		// it breaks all the key events in charts (zoom, pan, selection, etc.) but
		// stops the conflict between AWT and SWT events that freezes the SWT Spinner, TextFields, etc.
		// (Progression query, Specificities chart banality, etc.)
		// see: https://gitlab.huma-num.fr/txm/txm-src/-/issues/2548 (http://forge.cbp.ens-lyon.fr/redmine/issues/2548)
		if (OSDetector.isFamilyUnix()) {
			this.rootPanel.addMouseListener(new MouseListener() {

				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub

				}

				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub

				}

				@Override
				public void mouseExited(MouseEvent e) {
					// FIXME: SJ: temporary workaround to Linux focus bugs
					// it breaks all the key events in charts (zoom, pan, selection, etc.) but
					// stops the conflict between AWT and SWT events that freezes the SWT Spinner,
					// TextFields, etc.
					// (Progression query, Specificities chart banality, etc.)
					// see: https://gitlab.huma-num.fr/txm/txm-src/-/issues/2548 (http://forge.cbp.ens-lyon.fr/redmine/issues/2548)
					if (frame.isFocusOwner() || rootPanel.isFocusOwner()) {
						frame.setVisible(false);
						// Log.finest("SwingChartComposite.initEventsListeners().new MouseListener()
						// {...}.mouseExited()");
						frame.setVisible(true);

						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								if (SwingChartComposite.this.chartEditor != null) {
									SwingChartComposite.this.chartEditor.getShell().forceActive();
								}
							}
						});

					}
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub

				}

				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub

				}
			});
		}


		
    	Log.finest(String.format("SwingChartComposite: Storing AWT Frame size = %d x %d to fix SWT_AWT bridge bug.", frame.getWidth(), frame.getHeight())); //$NON-NLS-1$

		
	}



	@Override
	public boolean setFocus() {

		if (this.isDisposed()) {
			System.out.println("SwingChartComposite.setFocus(): is disposed."); //$NON-NLS-1$
			return false;
		}

		// Debug
		Log.finest("SwingChartComposite.setFocus(): trying to give focus to SWT composite and AWT component..."); //$NON-NLS-1$

		// boolean focusState = super.setFocus();
		boolean focusState = false;

		// Debug
		Log.finest(TXMCoreMessages.bind("SwingChartComposite.setFocus(): SWT composite focus given state = {0}.", this.isFocusControl())); //$NON-NLS-1$

		this.requestFocusInChartComponent();

		return focusState;
	}



	/**
	 * Requests the focus in chart component.
	 */
	public void requestFocusInChartComponent() {


		if (chartComponent != null) {

			// try {
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					// if(!((Component) chartComponent).isFocusOwner()) {

					// ((Component) chartComponent).requestFocus();
					((JComponent) chartComponent).grabFocus();
					boolean focusState = ((Component) chartComponent).requestFocusInWindow();

					// Debug
					// Log.finest(TXMCoreMessages.bind("SwingChartComposite.requestFocusInChartComponent(): AWT component focus given state = {0}.", ((Component) chartComponent).isFocusOwner()));
					// //$NON-NLS-1$
					Log.finest(TXMCoreMessages.bind("SwingChartComposite.requestFocusInChartComponent(): AWT component focus given state = {0}.", focusState)); //$NON-NLS-1$



					// }
				}
			});
			// }
			// catch (InvocationTargetException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// catch (InterruptedException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }

		}
	}

	@Override
	public void initEventsListeners() {



		// // AWT to SWT
		// ((Component) getChartComponent()).addMouseMotionListener(new MouseMotionListener() {
		//
		// @Override
		// public void mouseMoved(final MouseEvent e) {
		// if(!isDisposed()) {
		// getDisplay().asyncExec(new Runnable () {
		// public void run() {
		// notifyListeners(SWT.MouseMove, SwingChartComposite.swingEventToSWT(SwingChartComposite.this, ((JComponent) getChartComponent()), e, SWT.MouseMove));
		// }
		// });
		// }
		// }
		//
		// @Override
		// public void mouseDragged(MouseEvent e) {
		// // TODO Auto-generated method stub
		//
		// }
		// });

		((Component) getChartComponent()).addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(final MouseEvent e) {
				if (!isDisposed()) {
					getDisplay().asyncExec(new Runnable() {

						@Override
						public void run() {

							// FIXME: context menu
							if (

							e.getButton() == MouseEvent.BUTTON3
							// e.isPopupTrigger()
							) {

								// System.err.println("SWTChartsComponentsProvider.initializeAWTDelegationListeners(...).new MouseListener() {...}.mouseReleased(...).new Runnable() {...}.run(): AWT
								// popup trigger detected");



								// update the mouse over item selection according to the mouse event
								getChartComponent().updateMouseOverItem(e);

								// manually pop up the menu
								if (getMenu() != null) {
									getMenu().setLocation(new Point(e.getXOnScreen(), e.getYOnScreen()));
									getMenu().setVisible(true);

								}

								// FIXME: the SWT.MenuDetect event delegation doesn't work, it would be better than manually pop up the menu (but other problem is the dynamic menu on chart entity
								// item)
								// chartEditor.getComposite().notifyListeners(SWT.MenuDetect, SWTChartsComponentProvider.swingEventToSWT(chartEditor.getComposite(), swingComponent, e,
								// SWT.MenuDetect));
								// FIXME: try to delegate the Menu detect event, DOES NOT SEEM TO WORK
								// chartEditor.getComposite().notifyListeners(SWT.MenuDetect, SWTChartsComponentsProvider.swingEventToSWT(chartEditor.getComposite(), swingComponent, e,
								// SWT.MenuDetect));
								// chartEditor.getComposite().notifyListeners(SWT.Show, SWTChartsComponentsProvider.swingEventToSWT(chartEditor.getComposite(), swingComponent, e, SWT.Show));

							}
							// chartEditor.getComposite().notifyListeners(SWT.MouseUp, SWTChartsComponentsProvider.swingEventToSWT(chartEditor.getComposite(), swingComponent, e, SWT.MouseUp));
							// chartEditor.activate();
						}
					});
				}
			}

			@Override
			public void mousePressed(final MouseEvent e) {


				// FIXME: fix Linux. On Linux, the focus is never lost when clicking outside the Swing component, so the focus can not be given back again and the EditorPart activation patch code
				// based on
				// windowsFocusGained event is called
				// SWT thread
				getDisplay().asyncExec(new Runnable() {

					@Override
					public void run() {

						// chartEditor.getComposite().notifyListeners(SWT.MouseDown, SWTChartsComponentsProvider.swingEventToSWT(chartEditor.getComposite(), swingComponent, e, SWT.MouseDown));

						// FIXME: Debug
						// System.out.println("SWTChartsComponentProvider.initializeAWTDelegationListeners(...).new MouseListener() {...}.mousePressed(...).new Runnable() {...}.run()");
						// Activate the editor part on AWT mouse pressed event
						// chartEditor.activate();
						// chartEditor.getShell().setFocus();
						chartEditor.setFocus();
						// chartEditor.getParent().setFocus();
						// chartEditor.getParent().getShell().setActive();
						requestFocusInChartComponent();

						// chartEditor.getParent().setFocus();
						// chartEditor.forceFocus();
					}
				});

			}

			@Override
			public void mouseExited(java.awt.event.MouseEvent e) {

				// FIXME: SJ: temporary workaround to Linux focus bugs
				// part #2: release the focus at mouse exited on the AWT/SWing chart component
				// it breaks all the key events in charts (zoom, pan, selection, etc.) but
				// stops the conflict between AWT and SWT events that freezes the SWT Spinner, TextFields, etc.
				// (Progression query, Specificities chart banality, etc.)
				// see: http://forge.cbp.ens-lyon.fr/redmine/issues/2548
				if (OSDetector.isFamilyUnix()) {
					if (((Component) chartComponent).isFocusOwner()
							&&
							!((Component) getChartComponent()).contains(e.getPoint())) {

						// TODO Auto-generated method stub
						// Component f =
						// KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner();
						// frame.setFocusableWindowState(true);
						// frame.setAutoRequestFocus(false);
						// frame.setFocusable(false);
						// KeyboardFocusManager.getCurrentKeyboardFocusManager().clearFocusOwner();
						// KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
						// frame.setFocusableWindowState(false);
						// f.setEnabled(false);
						// f.setEnabled(true);
						// System.out.println("SwingChartComposite.initEventsListeners().new
						// MouseListener() {...}.mouseExited()");
						// frame.setAutoRequestFocus(false);
						// frame.setFocusable(false);
						// KeyboardFocusManager.getCurrentKeyboardFocusManager().clearFocusOwner();
						// KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();

						// setVisible(false);
						frame.setVisible(false);
						Log.finest("SwingChartComposite.initEventsListeners().new MouseListener() {...}.mouseExited()"); //$NON-NLS-1$
						frame.setVisible(true);
						// setVisible(true);

						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								// setVisible(false);
								// setVisible(true);
								// TODO Auto-generated method stub
								// shell.setMinimized(true);
								// shell.setMinimized(false);
								// shell.setActive();
								// getChartComponent().getChartEditor().getShell().forceActive();
								SwingChartComposite.this.chartEditor.getShell().forceActive();
							}
						});

					}


				}



				// TODO Auto-generated method stub
				// Frame frame = (Frame) swingComponent.getFocusCycleRootAncestor();
				// frame.setAutoRequestFocus(false);
				// frame.setFocusable(false);
				// frame.setFocusableWindowState(false);
			}

			@Override
			public void mouseEntered(java.awt.event.MouseEvent e) {

				// TODO: DEbug
				// System.err.println("SWTChartsComponentProvider.initializeSwingDelegationListeners(...) AWT mouse entered");

				// Force the focus in the component on mouse enter
				/*
				 * if(!swingComponent.isFocusOwner()) {
				 * // SWT thread
				 * if(!chartEditor.getComposite().isDisposed()) {
				 * chartEditor.getComposite().getDisplay().asyncExec(new Runnable() {
				 * public void run() {
				 * // FIXME: this code works on Windows and Linux but activate() the chart editor when mouse enters
				 * //chartEditor.activate();
				 * EventQueue.invokeLater(new Runnable () {
				 * public void run () {
				 * // TODO: this code forces to give the focus to the embedded AWT Frame under Windows (#1127 related)
				 * // For unknown reason Frame.requestFocusInWindow() doesn't work and always return false here
				 * // This code is not sufficient for Linux
				 * System.out.println("SWTChartsComponentProvider.initializeAWTDelegationListeners(...) trying to give focus to root embedded frame");
				 * try {
				 * Frame frame = (Frame) swingComponent.getFocusCycleRootAncestor();
				 * //frame.synthesizeWindowActivation(true);
				 * Class clazz = frame.getClass();
				 * Method method = clazz.getMethod("synthesizeWindowActivation", new Class[]{boolean.class});
				 * if(method != null) {
				 * method.invoke(frame, new Object[]{new Boolean(true)});
				 * System.out.println("SWTChartsComponentProvider.initializeSwingDelegationListeners(...).new MouseListener() {...}.mouseEntered(...).new Runnable() {...}.run(): " + frame);
				 * }
				 * //frame.requestFocusInWindow();
				 * }
				 * catch (Throwable e) {
				 * Log.printStackTrace(e);
				 * }
				 * }
				 * });
				 * //System.out.println("SWTChartsComponentProvider.initializeSwingDelegationListeners) AWT give focus to frame = " +
				 * ((JFCComposite)chartEditor.getComposite()).frame.requestFocusInWindow());
				 * // chartEditor.setFocus();
				 * // System.out.println("SWTChartsComponentProvider.initializeSwingDelegationListeners() SWT got focus = " + chartEditor.getComposite().setFocus());
				 * // EventQueue.invokeLater(new Runnable () {
				 * // public void run () {
				 * //swingComponent.requestFocusInWindow();
				 * // System.out.println("SWTChartsComponentProvider.initializeSwingDelegationListeners() AWT got focus = " + swingComponent.requestFocusInWindow());
				 * // }
				 * // });
				 * }
				 * });
				 * }
				 * }
				 */
			}

			@Override
			public void mouseClicked(java.awt.event.MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});


		// internal method to help tracking the focus events of frame and components to fix SWT/AWT bridge focus bugs
		// this.debug_initSWTAWTBridgeFocusBugsTests();



	}


	// FIXME: SJ: some code here should be moved to the parent class ChartComposite
	@Override
	public void loadChart() {

		// loads the chart from the result
		Object chart = this.chartEditor.getChart();

		if (chart != null) {

			// creates components if they not exist
			if (this.chartComponent == null) {

				// // recreates the chart if needed
				// if (this.chartEditor.getResult().isChartDirty()) {
				// try {
				// // this.chartEditor.getResult().clearLastRenderingParameters(); // FIXME: SJ: seems to became useless
				// this.chartEditor.getResult().compute();
				// }
				// catch (Exception e) {
				// e.printStackTrace();
				// }
				// }

				this.createChartComponent();

				this.rootPanel.add((Component) this.chartComponent);

				this.frame.setVisible(true);
			}
			this.loadChart(chart);
			
			
			
//			// FIXME: SJ, 2025-01-16: manage a SWT bug in SWT_AWT bridge that leads to wrong AWT Frame size
//			// see: https://gitlab.huma-num.fr/txm/txm-src/-/issues/4128
//			if(needToFixAWTFrameSize) {
//            	Log.finest(String.format("SwingChartComposite: Restoring AWT Frame size = %d x %d to fix SWT_AWT bridge bug.", frame.getWidth(), frame.getHeight())); //$NON-NLS-1$
//				this.frame.setSize(fixAWTFrameOriginalWidth, fixAWTFrameOriginalHeight);
//			}
			
			
		}
	}

	// // FIXME: SJ: old code that has been lost from SVGCharrtComposite when creating the class SwingChartComposite, dedicated to recreate a chart if its not of the right type
	// when trying to load it in the composite (e.g. File or JFreeChart)
	// @Override
	// public void loadChart() {
	// if(!this.chartEditor.getResult().isChartDirty()) {
	// // creates components if they not exist
	// if(this.chartComponent == null) {
	//
	// this.chartEditor.getSWTChartsComponentsProvider().createChartContainer(this.chartEditor.getEditorInput());
	// this.chartComponent = (IChartComponent) this.chartEditor.getEditorInput().getChartContainer();
	//
	// this.rootPanel.add((Component) this.chartComponent);
	//
	// this.frame.setVisible(true);
	// }
	// // loads the chart from the result
	// Object chart = this.chartEditor.getEditorInput().getChart();
	// // recreates the chart if not of right type
	// if(!(chart instanceof File)) {
	// this.file = this.chartEditor.getSWTChartsComponentsProvider().getChartsEngine().getChartCreator(this.chartEditor.getResult()).createChartFile(this.chartEditor.getResult());
	// }
	// else {
	// this.file = (File) chart;
	// }
	//
	// this.loadSVGDocument(this.file);
	// }
	// }


	/***
	 * Convenience method to return a casted AWT chart component.
	 * @return
	 */
	public Component getAWTChartComponent() {
		return (Component) super.getChartComponent();
	}
	

	@Override
	public void copyChartViewToClipboard() {

		int w = ((Component) this.chartComponent).getWidth();
		int h = ((Component) this.chartComponent).getHeight();
		final BufferedImage bufferedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bufferedImg.createGraphics();
		((Component) this.chartComponent).paint(g);
		g.dispose();

		Transferable img = new Transferable() {

			@Override
			public boolean isDataFlavorSupported(DataFlavor flavor) {
				return DataFlavor.imageFlavor.equals(flavor);
			}

			@Override
			public DataFlavor[] getTransferDataFlavors() {
				return new DataFlavor[] { DataFlavor.imageFlavor };
			}

			@Override
			public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
				if (!DataFlavor.imageFlavor.equals(flavor)) {
					throw new UnsupportedFlavorException(flavor);
				}
				return bufferedImg;
			}

		};

		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(img, null);

	}

	/**
	 * Computes the output image height according to the AWT chart component current dimensions and the width export preference.
	 */
	public int computeOutputImageHeight() {
		double aspectRatio = (double) this.getAWTChartComponent().getHeight() / (double) this.getAWTChartComponent().getWidth();
		return (int) (ChartsEnginePreferences.getInstance().getInt(ChartsEnginePreferences.EXPORT_WIDTH_IN_PIXELS) * aspectRatio);
	}
	
	
	@Override
	public File exportView(File file, String fileFormat) {

		// Export the chart as raster, keeping aspect ratio and according to the width export preference
		int outputImageWidth = ChartsEnginePreferences.getInstance().getInt(ChartsEnginePreferences.EXPORT_WIDTH_IN_PIXELS);
		int outputImageHeight = this.computeOutputImageHeight();
		
		// negative means a value greater than max integer
		if((outputImageWidth * outputImageHeight) < 0) {
			Log.severe(ChartsEngineUIMessages.errorTheSelectedExportResolutionParametersLeadsToATooLargeImage);
		}
		else {
			BufferedImage image = new BufferedImage(outputImageWidth, outputImageHeight, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = image.createGraphics();
			
			ChartsEngine.scaleGraphics2D(g, outputImageWidth, outputImageHeight, this.getAWTChartComponent().getWidth(), this.getAWTChartComponent().getHeight());
			this.getAWTChartComponent().printAll(g);

			g.dispose();
			
			try {
				javax.imageio.ImageIO.write(image, fileFormat, file);
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		return file;
	}
	

	@Override
	public EventCallBackHandler getMouseCallBackHandler() {
		return super.getMouseCallBackHandler((Component) this.chartComponent);
	}


	@Override
	public EventCallBackHandler getKeyCallBackHandler() {
		return super.getKeyCallBackHandler((Component) this.chartComponent);
	}


	/**
	 * Converts an AWT event to SWT event. Also stores the Swing tool tip source text of the specified Swing component in the SWT event to give it to the SWT composite listener in
	 * <code>ChartComposite</code>.
	 * 
	 * @param swtComposite
	 * @param swingComponent
	 * @param e
	 * @param eventType
	 * @return
	 */
	// FIXME : SJ: this method is incomplete and not used ATM but may becomes useful later, need to wait to see how the SWT/AWT focus bugs will be fixed
	public static org.eclipse.swt.widgets.Event swingEventToSWT(Composite swtComposite, JComponent swingComponent, AWTEvent e, int eventType) {

		Event event = new Event();
		if (!swtComposite.isDisposed()) {
			event.display = swtComposite.getDisplay();
		}
		event.widget = swtComposite;
		event.type = eventType;

		if (e instanceof MouseEvent) {
			MouseEvent me = (MouseEvent) e;
			event.button = me.getButton();
			try {
				// Store the Swing tool tip source text in the SWT event to give it to the SWT composite listener in ChartComposite
				// NOTE: this method call sometimes throws a java.lang.IndexOutOfBoundsException
				event.text = swingComponent.getToolTipText(me);
			}
			catch (Exception e1) {
				// e1.printStackTrace();
			}
			event.x = me.getX();
			event.y = me.getY();
		}
		return event;
	}


	/**
	 * Internal method to help tracking the focus events of frame and components to fix SWT/AWT bridge focus bugs.
	 */
	// FIXME: SJ: please keep this method ATM
	protected void debug_initSWTAWTBridgeFocusBugsTests() {

		this.frame.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				Log.finest("SwingChartComposite.SwingChartComposite(...).new FocusListener() {...}.focusLost()"); //$NON-NLS-1$
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				Log.finest("SwingChartComposite.SwingChartComposite(...).new FocusListener() {...}.focusGained()"); //$NON-NLS-1$
			}
		});



		// SJ: Workaround to fix a bug with some Java version and some OS where the focus of an embedded Swing component does not delegate to SWT, eg. the Part is not activated.
		// After some tests, doesn't seem to work on Linux and Mac
		// see: https://bugs.eclipse.org/bugs/show_bug.cgi?id=377104
		// see: https://bugs.java.com/bugdatabase/view_bug.do?bug_id=JDK-8203855
		// this.frame.addWindowListener(new java.awt.event.WindowAdapter() {
		// @Override
		// public void windowActivated(java.awt.event.WindowEvent e) {
		// SwingChartComposite.this.getDisplay().asyncExec(new Runnable() {
		// @Override
		// public void run() {
		// if (Display.getCurrent().getFocusControl() == SwingChartComposite.this) {
		// Stack<Control> stack = new Stack<Control>();
		// Control starter = SwingChartComposite.this;
		// Shell shell = SwingChartComposite.this.getShell();
		// while (starter != null && !(starter instanceof Shell)) {
		// stack.push(starter.getParent());
		// starter = starter.getParent();
		// }
		//
		// Method m = null;
		// try {
		// // instead of calling the originally proposed
		// // workaround solution (below),
		// //
		// // Event event = new Event();
		// // event.display = Display.getCurrent();
		// // event.type = SWT.Activate;
		// // event.widget = stack.pop();
		// // event.widget.notifyListeners(SWT.Activate, event);
		// //
		// // which should but does NOT set the active
		// // widget/control on the shell, we had
		// // to call the setActiveControl method directly.
		// // Updating the active control on the shell is
		// // important so that the last active
		// // control, when selected again, get the proper
		// // activation events fired.
		//
		// m = shell.getClass().getDeclaredMethod("setActiveControl", Control.class);// $NON-NLS-1$
		// m.setAccessible(true);
		// while (!stack.isEmpty()) {
		// m.invoke(shell, stack.pop());
		// }
		//
		// // force the Swing component focus
		// //setFocus();
		//
		// }
		// catch (Exception e) {
		// Log.severe("** Embedded part was not able to set active control on Shell. This will result in other workbench parts not getting activated."); //$NON-NLS-1$
		// Log.printStackTrace(e);
		// }
		// finally {
		// if (m != null) {
		// m.setAccessible(false);
		// }
		// }
		// }
		// }
		// });
		// }
		// });

		this.frame.addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
				Log.finest("SwingChartComposite.SwingChartComposite(...).new WindowListener() {...}.windowDeactivated()"); //$NON-NLS-1$
			}

			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				Log.finest("SwingChartComposite.SwingChartComposite(...).new WindowListener() {...}.windowActivated()"); //$NON-NLS-1$
			}
		});



		// this.frame.addWindowFocusListener(new WindowFocusListener() {
		//
		// @Override
		// public void windowLostFocus(WindowEvent e) {
		// // TODO Auto-generated method stub
		// Log.finest("SwingChartComposite.SwingChartComposite(...).new WindowFocusListener() {...}.windowLostFocus()");
		//
		// // Force the keep the focus if the ChartEditor has the focus
		// if(!SwingChartComposite.this.isDisposed()) {
		// SwingChartComposite.this.getDisplay().asyncExec(new Runnable() {
		// @Override
		// public void run() {
		// if (Display.getCurrent().getFocusControl() == SwingChartComposite.this) {
		// Log.finest("SwingChartComposite.SwingChartComposite(...).new WindowFocusListener() {...}.windowLostFocus(): force focus in chart component.");
		// requestFocusInChartComponent();
		// }
		// }
		// });
		// }
		//
		//
		//
		// // FIXME: For Swing focus debug tests
		// if(RCPPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
		// ((JComponent) rootPanel).setBorder(javax.swing.BorderFactory.createEmptyBorder());
		// }
		//
		//
		// }
		//
		// @Override
		// public void windowGainedFocus(WindowEvent e) {
		//
		// // FIXME: For Swing focus debug tests
		// if(RCPPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
		// ((JComponent) rootPanel).setBorder(new LineBorder(Color.red, 1));
		// }
		//
		//
		// // TODO Auto-generated method stub
		// Log.finest("SwingChartComposite.SwingChartComposite(...).new WindowFocusListener() {...}.windowGainedFocus()");
		//
		//
		// SwingChartComposite.this.getDisplay().asyncExec(new Runnable() {
		// @Override
		// public void run() {
		// if (
		// Display.getCurrent().getFocusControl() == SwingChartComposite.this
		// //&&
		// //SwingChartComposite.this.chartEditor.getContextActivationToken() == null
		//// SwingChartComposite.this.chartEditor != PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor()
		// ) {
		//
		// Log.finest("SwingChartComposite.SwingChartComposite(...).new WindowFocusListener() {...}.windowGainedFocus(): activate the EditorPart.");
		//
		// Stack<Control> stack = new Stack<Control>();
		// Control starter = SwingChartComposite.this;
		// Shell shell = SwingChartComposite.this.getShell();
		// while (starter != null && !(starter instanceof Shell)) {
		// stack.push(starter.getParent());
		// starter = starter.getParent();
		// }
		//
		// Method m = null;
		// try {
		// // instead of calling the originally proposed
		// // workaround solution (below),
		// //
		//// Event event = new Event();
		//// event.display = Display.getCurrent();
		//// event.type = SWT.Activate;
		//// event.widget = stack.pop();
		//// event.widget.notifyListeners(SWT.Activate, event);
		// //
		// // which should but does NOT set the active
		// // widget/control on the shell, we had
		// // to call the setActiveControl method directly.
		// // Updating the active control on the shell is
		// // important so that the last active
		// // control, when selected again, get the proper
		// // activation events fired.
		//
		// //PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().activate(SwingChartComposite.this.chartEditor);
		//
		// m = shell.getClass().getDeclaredMethod("setActiveControl", Control.class);// $NON-NLS-1$
		// m.setAccessible(true);
		// while (!stack.isEmpty()) {
		// m.invoke(shell, stack.pop());
		// }
		//
		// // force the Swing component focus
		// //setFocus();
		//
		// }
		// catch (Exception e) {
		// Log.severe("** Embedded part was not able to set active control on Shell. This will result in other workbench parts not getting activated."); //$NON-NLS-1$
		// Log.printStackTrace(e);
		// }
		// finally {
		// if (m != null) {
		// m.setAccessible(false);
		// }
		// }
		// }
		// }
		// });
		//
		// }
		// });



		// // Swing component focus delegation to the SWT chart composite
		// swingComponent.addFocusListener(new FocusListener() {
		//
		// @Override
		// public void focusLost(final FocusEvent e) {
		//
		// // FIXME: For Swing focus debug tests
		// if(RCPPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
		// swingComponent.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		// }
		//
		// Display.getDefault().asyncExec(new Runnable() {
		//
		// @Override
		// public void run() {
		//
		// //chartEditor.getComposite().notifyListeners(SWT.FocusOut, SWTChartsComponentsProvider.swingEventToSWT(chartEditor.getComposite(), swingComponent, e, SWT.FocusOut));
		//
		// chartEditor.deactivateContext();
		// }
		// });
		// }
		//
		// @Override
		// public void focusGained(final FocusEvent e) {
		//
		// // FIXME: For Swing focus debug tests
		// if(RCPPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
		// swingComponent.setBorder(new LineBorder(Color.red, 1));
		// }
		//
		// Display.getDefault().asyncExec(new Runnable() {
		//
		// @Override
		// public void run() {
		//
		// //chartEditor.getComposite().notifyListeners(SWT.FocusIn, SWTChartsComponentsProvider.swingEventToSWT(chartEditor.getComposite(), swingComponent, e, SWT.FocusIn));
		//
		// //PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().activate(this);
		//
		//// chartEditor.activate();
		// //chartEditor.getComposite().setFocus();
		// //chartEditor.forceFocus();
		//
		// chartEditor.activateContext();
		//
		// }
		// });
		// }
		// });
		//



		// SWT

		// SWT Tooltips tests


		// toolTip = new DefaultToolTip(this, org.eclipse.jface.window.ToolTip.RECREATE, true);
		// toolTip.setPopupDelay(500);



		// FIXME: test HTML
		// toolTip = new ___CustomHTMLToolTip(this, org.eclipse.jface.window.ToolTip.NO_RECREATE, true);
		// toolTip.setShift(new Point(10, 50));



		// // Define AWT events, needed to properly give the focus to the Swing component and manage the mouse events
		// Listener listener;
		//
		// //FIXME: useless ?
		// // Run event queue dispose event
		//// listener = new Listener () {
		//// public void handleEvent (Event e) {
		//// switch (e.type) {
		//// case SWT.Dispose:
		//// chartPanel.getParent().setVisible(false);
		//// EventQueue.invokeLater(new Runnable () {
		//// public void run() {
		//// ((Frame) chartPanel.getParent().getParent()).dispose();
		//// }
		//// });
		//// break;
		//// }
		//// }
		//// };
		//// this.addListener(SWT.Dispose, listener);
		//
		//
		// // Handle the AWT/Swing events delegation
		// listener = new Listener () {
		// public void handleEvent (Event e) {
		// switch (e.type) {
		//
		// case SWT.FocusOut:
		// // FIXME: DEbug
		//// System.out.println("SWTChartsComponentProvider.initializeAWTDelegationListeners(...).new Listener() {...}.handleEvent(): SWT composite gained focus");
		// System.out.println("SWTChartsComponentsProvider.initializeAWTDelegationListeners(...).new Listener() {...}.handleEvent()");
		// break;
		//
		//
		// case SWT.FocusIn:
		// // FIXME: DEbug
		//// System.out.println("SWTChartsComponentProvider.initializeAWTDelegationListeners(...).new Listener() {...}.handleEvent(): SWT composite gained focus");
		// //chartEditor.getComposite().setFocus();
		// //chartEditor.getComposite().requestFocusInComposite();
		// break;
		// case SWT.MouseMove:
		// //System.err.println("SWTChartsComponentProvider.initializeAWTDelegationListeners(...).new Listener() {...}.handleEvent(): SWT mouse move: " + e.x + " : " + e.y);
		//
		//// // FIXME: SWT tool tips test
		//// ToolTip tip = new ToolTip(getShell(), SWT.BALLOON | SWT.ID_HIDE);
		//// tip.setMessage("test tool tip SWT");
		//// tip.setLocation(getDisplay().map(self, null, new Point(e.x, e.y)));
		//// tip.setVisible(true);
		//
		//// // FIXME: JFace tool tips test
		//// //DefaultToolTip tip2 = new DefaultToolTip( self, SWT.NONE, true );
		//// //toolTip.setShift(new Point(e.x, e.y));
		//// //toolTip.setText("<html><body><p>rtrt <font style=\"color: red\">yuyuyu</font></p></body></html>");
		//// toolTip.setText(e.text);
		//// if(e.text != null) {
		//// toolTip.show(new Point(e.x, e.y));
		//// }
		//// else {
		//// toolTip.hide();
		//// }
		//
		// // FIXME: JFace tool tips test 2
		//// toolTip.setText(e.text);
		//// if(e.text != null) {
		//// toolTip.show(new Point(e.x, e.y));
		//// }
		//// else {
		//// toolTip.hide();
		//// }
		//
		//
		//
		// break;
		// case SWT.MouseDown:
		// // FIXME: to test in Linux and Mac, it should fix the focus bugs in CA chart between table focus and chart composite/panel focus + Linux singular values bar plot focus bug
		// //System.err.println("SWTChartsComponentProvider.initializeAWTDelegationListeners(...).new Listener() {...}.handleEvent(): SWT mouse down : set focus in composite and request focus in chart
		// panel");
		// //chartEditor.getComposite().setFocus();
		// //chartEditor.getComposite().requestFocusInComposite();
		// break;
		// case SWT.MouseUp:
		// //System.err.println("SWTChartsComponentProvider.initializeAWTDelegationListeners(...).new Listener() {...}.handleEvent(): SWT mouse up");
		// break;
		//
		// case SWT.MouseEnter:
		// //System.err.println("SWTChartsComponentProvider.initializeAWTDelegationListeners(...).new Listener() {...}.handleEvent(): SWT mouse enter");
		// break;
		//
		//
		// case SWT.Activate:
		// //System.err.println("SWTChartsComponentProvider.initializeAWTDelegationListeners(...).new Listener() {...}.handleEvent(): SWT activate");
		// //chartEditor.getComposite().requestFocusInComposite();
		// break;
		// }
		// }
		// };
		// chartEditor.getComposite().addListener(SWT.FocusOut, listener);
		// chartEditor.getComposite().addListener(SWT.FocusIn, listener);
		// chartEditor.getComposite().addListener(SWT.MouseDown, listener); // needed to manage the focus delegation between SWT and AWT/Swing
		//
		//// chartEditor.getComposite().addListener(SWT.MouseEnter, listener); // TODO : test AWT delegation
		// // chartEditor.getComposite().addListener(SWT.Activate, listener); // TODO : test AWT delegation
		//
		//
		//
		// // FIXME: SWT tool tips test rather than using Swing
		// //chartEditor.getComposite().addListener(SWT.MouseMove, listener);
		// // chartEditor.getComposite().addListener(SWT.MouseUp, listener);



	}


}
