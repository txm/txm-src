package org.txm.chartsengine.rcp.swt;

import java.awt.Component;
import java.io.File;
import java.util.ArrayList;
import java.util.EventListener;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.rcp.IChartComponent;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.events.EventCallBack;
import org.txm.chartsengine.rcp.events.EventCallBackHandler;
import org.txm.rcp.editors.TXMEditor;
import org.txm.utils.logger.Log;

/**
 * Chart drawing area based on a SWT composite. this composite should be extended to implement specific draw component (Swing, etc.)
 * 
 * This composite can only be used with a ChartEditor and a ChartResult
 * 
 * @author sjacquot
 *
 */
public abstract class ChartComposite extends Composite {



	protected static final String CONTEXT_MENU_EMPTY_AREA_ID = "ChartEditorContextMenuEmptyArea"; //$NON-NLS-1$

	protected static final String CONTEXT_MENU_ITEM_AREA_ID = "ChartEditorContextMenuItemArea"; //$NON-NLS-1$

	/**
	 * Custom tool tip.
	 */
	// FIXME: tests HTML tool tips
	protected ___CustomHTMLToolTip toolTip;

	/**
	 * The chart component.
	 */
	protected IChartComponent chartComponent;

	/**
	 * Context menus.
	 */
	protected ArrayList<Menu> contextMenus;


	protected ChartEditor<? extends ChartResult> chartEditor;

	/**
	 *
	 * @param parent
	 * @param style
	 */
	public ChartComposite(ChartEditor<? extends ChartResult> chartEditor, Composite parent, int style) {
		super(parent, style);

		this.chartEditor = chartEditor;

		this.contextMenus = new ArrayList<>();
		this.contextMenus.add(null);
		this.contextMenus.add(null);
	}



	/**
	 * Loads the chart from specified data object (can be a File, an UI component, etc.).
	 * 
	 * @param chart
	 */
	public abstract void loadChart(Object chart);


	/**
	 * Loads or reloads the chart from the chart object stored in the linked editor input.
	 * 
	 * This method is especially used for file based implementations of charts engine for reloading a file that has been externally modified.
	 * 
	 * This methods works only if used with a ChartEditor (the ChartEditor allows to retrieve the ChartResult and the chart to display)
	 */
	public abstract void loadChart();


	/**
	 * Loads or reloads the chart from the chart object stored in the linked editor input.
	 * 
	 * @param resetView
	 * @param clearChartItemsSelection
	 * @param squareOff
	 */
	public void loadChart(boolean resetView, boolean clearChartItemsSelection, boolean squareOff) {
		this.loadChart();

		if (squareOff) {
			this.squareOff();
		}

		if (clearChartItemsSelection) {
			this.clearChartItemsSelection();
		}

		if (resetView) {
			this.resetView();
		}
	}

	/**
	 * Loads or reloads the chart from the chart object stored in the linked editor input.
	 * 
	 * @param resetView
	 * @param clearChartItemsSelection
	 */
	public void loadChart(boolean resetView, boolean clearChartItemsSelection) {

		this.loadChart(resetView, clearChartItemsSelection, false);
	}

	/**
	 * Creates a new chart component.
	 * 
	 * @return
	 */
	protected abstract IChartComponent _createChartComponent();


	/**
	 * Creates a new chart component by calling the implementation method and store it.
	 * 
	 */
	public void createChartComponent() {

		// if (!this.chartEditor.getResult().isChartDirty()) {

		Log.finest(this.getClass().getSimpleName() + ": creating a chart component..."); //$NON-NLS-1$

		this.chartComponent = this._createChartComponent();
		this.chartComponent.setChartEditor(this.chartEditor);

		// initialize the default shared context menus as Export, etc.
		this.initDefaultContextMenus();
		// initialize Listeners.
		this.initEventsListeners();
		// register user entries event call back extensions
		this.registerEventCallBackExtensions();


		// return true;
		// }
		// else {
		// return false;
		// }
	}

	/**
	 * Initializes event listeners.
	 */
	public abstract void initEventsListeners();


	/**
	 * Creates the pop up menu dedicated to empty area location click event that is populated from plugin.xml.
	 */
	public void initEmptyAreaContextMenu() {
		Menu menu = TXMEditor.initContextMenu(this, CONTEXT_MENU_EMPTY_AREA_ID);
		this.contextMenus.set(EventCallBack.AREA_EMPTY, menu);
	}


	/**
	 * Creates the pop up menu dedicated to chart item location click event.
	 */
	public void initItemAreaContextMenu() {
		Menu menu = TXMEditor.initContextMenu(this, CONTEXT_MENU_ITEM_AREA_ID);

		// Menu menu = new Menu(this);
		// MenuItem chartEntityLabelItem = new MenuItem(menu, SWT.NONE);
		// chartEntityLabelItem.setText(""); // this string is dedicated to be overridden by the current selected chart entity label/name

		// FIXME : tests
		// MenuItem refreshItem = new MenuItem(menu, SWT.NONE);
		// refreshItem.setText("Refresh");
		// MenuItem deleteItem = new MenuItem(menu, SWT.NONE);
		// deleteItem.setText("Delete");
		//
		// sub-menu for selected items
		// Menu newMenu = new Menu(menu);
		// chartEntityLabelItem.setMenu(newMenu);

		// MenuItem shortcutItem = new MenuItem(newMenu, SWT.NONE);
		// shortcutItem.setText("Shortcut");
		// MenuItem iconItem = new MenuItem(newMenu, SWT.NONE);
		// iconItem.setText("Icon");

		this.contextMenus.set(EventCallBack.AREA_ITEM, menu);
	}

	/**
	 * Defines the current context menu to display on menu event.
	 * 
	 * @param index
	 */
	public void setCurrentContextMenu(int index) {

		// Debug
		Log.finest("ChartComposite.setCurrentContextMenu(): setting menu to " + index + " / " + this.contextMenus.get(index)); //$NON-NLS-1$ //$NON-NLS-2$

		this.setMenu(this.contextMenus.get(index));
	}


	/**
	 * Gets the menu specified by its index.
	 * 
	 * @param index
	 * @return
	 */
	public Menu getContextMenu(int index) {
		return this.contextMenus.get(index);
	}


	/**
	 * Resets the chart view (zoom, pan, rotation).
	 */
	public void resetView() {
		if (this.chartComponent != null) {
			this.chartComponent.resetView();

			if (this.chartComponent.getChartEditor().getResult().needsToSquareOff()) {
				this.chartComponent.squareOff();
			}
		}
	}


	/**
	 * Copies the chart view as image in the clipboard from the specified chart component.
	 */
	public abstract void copyChartViewToClipboard();


	/**
	 * Exports the current view of the composite in the specified file.
	 * 
	 * @param file
	 * @param fileFormat
	 * @return
	 */
	public abstract File exportView(File file, String fileFormat);


	/**
	 * Returns the export formats supported by the implementation of the <code>ChartComposite</code> for exporting the current the view.
	 * 
	 * @return
	 */
	public abstract ArrayList<String> getEditorSupportedExportFileFormats();

	/**
	 * Clears the selected items in chart.
	 */
	public abstract void clearChartItemsSelection();

	/**
	 * Gets the chart object.
	 * 
	 * @return
	 */
	public Object getChart() {
		return this.getChartResult().getChart();
	};


	/**
	 * Gets the linked result.
	 * 
	 * @return
	 */
	public ChartResult getChartResult() {
		return this.chartEditor.getResult();
	}

	/**
	 * Gets the charts engine of the linked result.
	 * 
	 * @return
	 */
	public ChartsEngine getChartsEngine() {
		return this.getChartResult().getChartsEngine();
	}

	/**
	 * Zooms the chart view at x and y coordinates.
	 * 
	 * @param x zoom center X position
	 * @param y zoom center Y position
	 * @param zoomIn true to zoom in, false to zoom out
	 * @param range true to zoom in range axis
	 * @param domain true to zoom in domain axis

	 */
	public void zoom(double x, double y, boolean zoomIn, boolean range, boolean domain) {
		this.chartComponent.zoom(x, y, zoomIn, range, domain);
	}

	/**
	 * Pans the chart view.
	 * 
	 * @param srcX
	 * @param srcY
	 * @param dstX
	 * @param dstY
	 * @param panFactor
	 */
	public void pan(double srcX, double srcY, double dstX, double dstY, double panFactor) {
		this.chartComponent.pan(srcX, srcY, dstX, dstY, panFactor);
	}

	/**
	 * Constrains axes ticks to square aspect ratio.
	 */
	public void squareOff() {
		if (this.chartComponent != null) {
			this.chartComponent.squareOff();
		}
	}

	/**
	 * Checks if the composite AND the chart component have the focus.
	 * 
	 * @return
	 */
	public boolean hasFocus() {
		return this.chartComponent.hasFocus() && this.isFocusControl();
	}

	/**
	 * Gets the mouse call back handler registered for the chart component.
	 * 
	 * @return
	 */
	public abstract EventCallBackHandler getMouseCallBackHandler();


	/**
	 * Gets the key call back handler registered for the chart component.
	 * 
	 * @return
	 */
	public abstract EventCallBackHandler getKeyCallBackHandler();


	/**
	 * Gets the mouse call back handler registered for the specified component.
	 * 
	 * @param component
	 * @return
	 */
	public EventCallBackHandler getMouseCallBackHandler(Component component) {
		EventCallBackHandler handler = null;
		EventListener[] listeners = component.getMouseListeners();

		for (int i = 0; i < listeners.length; i++) {
			if (listeners[i] instanceof EventCallBackHandler) {
				handler = (EventCallBackHandler) listeners[i];
				break;
			}
		}
		return handler;
	}


	/**
	 * Gets the key call back handler registered for the specified component.
	 * 
	 * @param component
	 * @return
	 */
	public EventCallBackHandler getKeyCallBackHandler(Component component) {
		EventCallBackHandler handler = null;
		EventListener[] listeners = component.getKeyListeners();

		for (int i = 0; i < listeners.length; i++) {
			if (listeners[i] instanceof EventCallBackHandler) {
				handler = (EventCallBackHandler) listeners[i];
				break;
			}
		}
		return handler;
	}

	/**
	 * Gets the event call back handler of the specified class.
	 * 
	 * @param listenerType
	 * @return
	 */
	public EventCallBackHandler getCallBackHandler(Class listenerType) {
		EventCallBackHandler handler = null;
		EventListener[] listeners = ((Component) this.chartComponent).getListeners(listenerType);

		for (int i = 0; i < listeners.length; i++) {
			if (listeners[i] instanceof EventCallBackHandler) {
				handler = (EventCallBackHandler) listeners[i];
				break;
			}
		}
		return handler;
	}

	/**
	 * Initializes the default context menus.
	 * 
	 */
	public void initDefaultContextMenus() {
		this.initItemAreaContextMenu();
		this.initEmptyAreaContextMenu();
		this.setCurrentContextMenu(EventCallBack.AREA_EMPTY);
	}


	/**
	 * Registers user entries event call back extensions.
	 */
	public void registerEventCallBackExtensions() {

		ChartResult result = this.chartEditor.getResult();

		String extensionPointId = "org.txm.chartsengine.eventcallback"; //$NON-NLS-1$
		IConfigurationElement[] contributions = Platform.getExtensionRegistry().getConfigurationElementsFor(extensionPointId);

		for (int i = 0; i < contributions.length; i++) {

			String chartEngineName = contributions[i].getAttribute("chartsEngineName"); //$NON-NLS-1$ 
			String resultDataClass = contributions[i].getAttribute("resultDataClass"); //$NON-NLS-1$ 
			String chartType = contributions[i].getAttribute("chartType"); //$NON-NLS-1$ 

			String resultChartType = result.getChartType();

			if ((chartEngineName == null || chartEngineName.equals(result.getChartsEngine().getName())) // callbacks are dependant to the engine ??
					&& resultDataClass != null // resultDataClass is mandatory
					&& resultDataClass.equals(result.getClass().getName()) // and it must be the same as the result class
					&& ((chartType == null && resultChartType.equals(ChartsEnginePreferences.DEFAULT_CHART_TYPE) //$NON-NLS-1$
							||
							chartType != null && !resultChartType.equals(ChartsEnginePreferences.DEFAULT_CHART_TYPE) && chartType.equals(result.getChartType())  //$NON-NLS-1$
					))

			) {
				try {
					EventCallBack eventCallBack = (EventCallBack) contributions[i].createExecutableExtension("class");  //$NON-NLS-1$

					eventCallBack.setChartEditor(this.chartEditor);

					EventCallBackHandler mouseHandler = this.getMouseCallBackHandler();
					if (mouseHandler != null && mouseHandler.getEventCallBack(eventCallBack.getClass()) == null) {

						Log.fine("ChartEditor.registerEventCallBackExtensions(): call back of type " + eventCallBack.getClass()  //$NON-NLS-1$
								+ " registered in mouse handler (chart editor = " + this.chartEditor.getClass()  //$NON-NLS-1$
								+ ", result data class = " + result.getClass()  //$NON-NLS-1$
								+ ", chart type = " + result.getChartType()  //$NON-NLS-1$
								+ ").");  //$NON-NLS-1$

						mouseHandler.registerEventCallBack(eventCallBack);
					}
					EventCallBackHandler keyboardHandler = this.getKeyCallBackHandler();
					if (keyboardHandler != null && keyboardHandler.getEventCallBack(eventCallBack.getClass()) == null) {

						Log.fine("ChartEditor.registerEventCallBackExtensions(): call back of type " + eventCallBack.getClass()  //$NON-NLS-1$
								+ " registered in key handler (chart editor = " + this.chartEditor.getClass()  //$NON-NLS-1$
								+ ", result data class = " + result.getClass()  //$NON-NLS-1$
								+ ", chart type = " + result.getChartType()  //$NON-NLS-1$
								+ ").");  //$NON-NLS-1$

						keyboardHandler.registerEventCallBack(eventCallBack);
					}
				}
				catch (CoreException e) {
					e.printStackTrace();
				}
				break;
			}
		}

	}

	/**
	 * Gets the chart component dedicated to rendering the chart.
	 * 
	 * @return the chartComponent
	 */
	public IChartComponent getChartComponent() {
		return chartComponent;
	}
}
