package org.txm.chartsengine.rcp.swt;

import org.eclipse.swt.widgets.Composite;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.TXMEditorToolBar;
import org.txm.rcp.swt.GLComposite;

/**
 * The default chart editor tool bar shared by every <code>ChartEditor</code>.
 * The tool bar is accessible and usable in plugin.xml with the URI toolbar:ChartEditorToolBar.
 * 
 * @author sjacquot
 *
 */
// FIXME: SJ: became useless? could now directly use TXMEditorToolBar
// If remove, check that the ID is not used and/or replace it in plugin.xml
public class ChartEditorToolBar extends TXMEditorToolBar {


	public final static String ID = "ChartEditorToolBar"; //$NON-NLS-1$


	/**
	 * 
	 * @param parent
	 * @param style
	 * @param chartEditor
	 */
	public ChartEditorToolBar(ChartEditor<? extends ChartResult> chartEditor, Composite parent, GLComposite subWidgetsComposite, int style) {
		this(chartEditor, parent, subWidgetsComposite, style, ChartEditorToolBar.ID);
	}


	/**
	 * 
	 * @param parent
	 * @param style
	 * @param chartEditor
	 */
	public ChartEditorToolBar(ChartEditor<? extends ChartResult> chartEditor, Composite parent, GLComposite subWidgetsComposite, int style, String toolbarId) {
		super(chartEditor, parent, subWidgetsComposite, style, toolbarId);
	}


	/**
	 * Synchronizes the tool bar with the chart result states.
	 * Enables/disables button according to the chart result states.
	 * 
	 */
	// FIXME: SJ: tests to disable command if the chart is null and reenable it when the chart is computed
	// public void synchronize() {
	// boolean enable = (this.getEditorPart().getResultData().getChart() != null);
	//// HandledContributionItem t = (HandledContributionItem) this.getItem(0).getData();
	// System.out.println("ChartEditorToolBar.synchronize()");
	//
	// IHandlerService handlerService = (IHandlerService) PlatformUI.getWorkbench().getService(IHandlerService.class);
	// ICommandService commandService = (ICommandService) PlatformUI.getWorkbench().getService(ICommandService.class);
	// if (handlerService != null && commandService != null) {
	// try {
	// Command command = commandService.getCommand(ExportChartEditorView.ID);
	// command.setEnabled(null);
	// //command.setEnabled(new Boolean(enable));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// else {
	// System.err.println("ChartEditorToolBar.synchronize() can't get handler service");
	// }
	// }


	@Override
	public ChartEditor<? extends TXMResult> getEditorPart() {
		return (ChartEditor<?>) editorPart;
	}

}
