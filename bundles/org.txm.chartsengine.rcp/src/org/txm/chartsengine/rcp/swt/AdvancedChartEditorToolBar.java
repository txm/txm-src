package org.txm.chartsengine.rcp.swt;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolItem;
import org.txm.Toolbox;
import org.txm.chartsengine.core.ChartCreator;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.ChartsEnginesManager;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.messages.ChartsEngineUIMessages;
import org.txm.core.engines.EngineType;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.preferences.RCPPreferences;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.widget.TXMParameterCombo;

/**
 * The default advanced chart editor tool bar shared by every <code>ChartEditor</code>.
 * The tool bar is accessible and usable in plugin.xml with the URI toolbar:AdvancedChartEditorToolBar.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class AdvancedChartEditorToolBar extends ChartEditorToolBar {

	public final static String ID = "AdvancedChartEditorToolBar"; //$NON-NLS-1$

	/**
	 * Chart type selection combo (expert mode only).
	 */
	protected Combo chartTypeCombo;


	/**
	 * Font selection by physical font name (expert mode only)
	 */
	protected Combo advancedFontCombo;


	/**
	 * Creates a new chart editor tool bar dedicated to advanced parameters.
	 * 
	 * @param parent
	 * @param style
	 */
	public AdvancedChartEditorToolBar(ChartEditor<? extends ChartResult> chartEditor, Composite parent, GLComposite subWidgetsComposite, int style) {
		super(chartEditor, parent, subWidgetsComposite, style, AdvancedChartEditorToolBar.ID);


		// Show/hide chart title button
		final ToolItem showTitleButton = new ToolItem(this, SWT.CHECK);
		showTitleButton.setImage(IImageKeys.getImage(getClass(), "icons/show_title.png")); //$NON-NLS-1$
		showTitleButton.setDisabledImage(IImageKeys.getImage(getClass(), "icons/show_title_disabled.png")); //$NON-NLS-1$
		showTitleButton.setToolTipText(ChartsEngineUIMessages.showhideTitle);

		if (!chartEditor.getResult().isEmptyPreference(ChartsEnginePreferences.SHOW_TITLE)) {
			showTitleButton.setSelection(chartEditor.getResult().isTitleVisible());
		}
		// disable if not managed by the command
		else {
			showTitleButton.setEnabled(false);
		}

		
		//TODO: SJ: try to use this listener instead of calling compute() for some parameters
		// need to see if we can autoupdate chart result parameter from a toolbar with parameters annotations
		// Auto-update chart selection listener
//		UpdateChartSelectionListener updateChartSelectionListener = new UpdateChartSelectionListener((ChartEditor<?>) this.editorPart);
//		showTitleButton.addSelectionListener(updateChartSelectionListener)
		

		// Show/hide chart legend button
		final ToolItem showLegendButton = new ToolItem(this, SWT.CHECK);
		//showLegendButton.setText(ChartsUIMessages.showhideLegend);
		showLegendButton.setImage(IImageKeys.getImage(getClass(), "icons/show_legend.png")); //$NON-NLS-1$
		showLegendButton.setDisabledImage(IImageKeys.getImage(getClass(), "icons/show_legend_disabled.png")); //$NON-NLS-1$
		showLegendButton.setToolTipText(ChartsEngineUIMessages.showhideLegend);

		if (!chartEditor.getResult().isEmptyPreference(ChartsEnginePreferences.SHOW_LEGEND)) {
			showLegendButton.setSelection(chartEditor.getResult().isLegendVisible());
		}
		// disable if not managed by the command
		else {
			showLegendButton.setEnabled(false);
		}


		// Show/hide chart background grid button
		final ToolItem showGridButton = new ToolItem(this, SWT.CHECK);
		//showGridButton.setText(ChartsUIMessages.showhideGrid);
		showGridButton.setImage(IImageKeys.getImage(getClass(), "icons/show_grid.png")); //$NON-NLS-1$
		showGridButton.setDisabledImage(IImageKeys.getImage(getClass(), "icons/show_grid_disabled.png")); //$NON-NLS-1$
		showGridButton.setToolTipText(ChartsEngineUIMessages.showhideGrid);

		if (!chartEditor.getResult().isEmptyPreference(ChartsEnginePreferences.SHOW_GRID)) {
			showGridButton.setSelection(chartEditor.getResult().isGridVisible());
		}
		// disable if not managed by the command
		else {
			showGridButton.setEnabled(false);
		}


		// Rendering colors mode selection
		TXMParameterCombo renderingModeParameterCombo = new TXMParameterCombo(this, chartEditor, SWT.READ_ONLY, new String[] { ChartsEngineUIMessages.colors,
				ChartsEngineUIMessages.grayscale,
				ChartsEngineUIMessages.blackAndWhite,
				ChartsEngineUIMessages.monochrome
		});
		final Combo renderingModeCombo = renderingModeParameterCombo.getControl();
		renderingModeCombo.setToolTipText(ChartsEngineUIMessages.renderingColorsMode);
		if (!chartEditor.getResult().isEmptyPreference(ChartsEnginePreferences.RENDERING_COLORS_MODE)) {
			renderingModeCombo.select(chartEditor.getResult().getRenderingColorsMode());
		}
		// disable if not managed
		else {
			renderingModeCombo.setEnabled(false);
		}


		// Font selection (user-friendly font names)
		final Font currentFont = ChartsEngine.createFont(chartEditor.getResult().getFont());
		TXMParameterCombo fontParameterCombo = new TXMParameterCombo(this, chartEditor, SWT.READ_ONLY, ChartsEngine.FONTS_USER_FRIENDLY_NAMES);
		final Combo fontCombo = fontParameterCombo.getControl();
		fontCombo.setToolTipText(ChartsEngineUIMessages.selectThefontFamilly);
		int currentFontIndex = Arrays.asList(ChartsEngine.FONTS_LOGICAL_NAMES).indexOf(currentFont.getName());
		// force "Arial" if name is not found
		if (currentFontIndex < 0) {
			currentFontIndex = 0;
		}
		fontCombo.select(currentFontIndex);


		// Font size
		String fontSizes[] = new String[30];
		int currentFontSizeIndex = 0;
		for (int i = 0; i < fontSizes.length; i++) {
			fontSizes[i] = String.valueOf(i + 1);
			if (currentFont.getSize() == (i + 1)) {
				currentFontSizeIndex = i;
			}

		}
		TXMParameterCombo fontSizeParameterCombo = new TXMParameterCombo(this, chartEditor, SWT.READ_ONLY, fontSizes);
		final Combo fontSizeCombo = fontSizeParameterCombo.getControl();
		fontSizeCombo.select(currentFontSizeIndex);
		fontSizeCombo.setToolTipText(ChartsEngineUIMessages.selectTheFontSize);


		// Font style
		TXMParameterCombo fontStyleParameterCombo = new TXMParameterCombo(this, chartEditor, SWT.READ_ONLY, ChartsEngine.FONT_STYLES);
		final Combo fontStyleCombo = fontStyleParameterCombo.getControl();
		fontStyleCombo.setToolTipText(ChartsEngineUIMessages.selectThefontStyle);

		//FIXME: SJ: the styles list may be builded from the available styles of the current selected font.
		// Here are some tests to try to get the available styles

		// AWT
		//        for (Entry<TextAttribute, ?> mapElement : currentFont.getAttributes().entrySet()) {
		//            TextAttribute key = mapElement.getKey();
		//            System.out.println(key + " : " + mapElement.getValue());
		//        }
		//		
		//		for (Attribute a : currentFont.getAvailableAttributes()) {
		//			System.out.println("AdvancedChartEditorToolBar.AdvancedChartEditorToolBar()" + a);	
		//		}
		// SWT/JFace
		//		FontData[] fontsData = this.getDisplay().getFontList(null, true);
		//		  for (int i = 0; i < fontsData.length; i++) {
		//		    System.out.println("AdvancedChartEditorToolBar.AdvancedChartEditorToolBar() - Fonts from SWT: " + fontsData[i].getName()
		//		    		+ " - " + fontsData[i].getLocale()
		//		    		+ " - " + fontsData[i].getHeight()
		//		    		+ " - " + fontsData[i].toString()
		//		    		);
		//		    System.out.println("AdvancedChartEditorToolBar.AdvancedChartEditorToolBar() " + StringConverter.asString(fontsData[i]));
		//		  }

		fontStyleCombo.select(currentFont.getStyle());


		// FIXME: SJ: this Combo may not be created if is not in expert mode. At this moment, it's created and hidden

		// Export mode
		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {

			// Chart types from installed chart creators in all installed charts engines
			ArrayList<String> chartsEngineNames = new ArrayList<>();
			ArrayList<ChartCreator<?, ?>> allChartCreators = new ArrayList<>();

			for (ChartsEngine engine : ChartsEnginesManager.getChartsEngines()) {
				for (ChartCreator<?, ?> creator : engine.getChartCreators(this.getEditorPart().getResult())) {
					chartsEngineNames.add(engine.getName());
					allChartCreators.add(creator);
				}
			}


			String chartTypes[] = new String[allChartCreators.size()];
			int currentChartCreatorIndex = 0;

			for (int i = 0; i < allChartCreators.size(); i++) {

				chartTypes[i] = allChartCreators.get(i).getChartType();
				if (chartTypes[i] == null) {
					chartTypes[i] = ChartsEnginePreferences.DEFAULT_CHART_TYPE;
				}

				// preselected combo index
				if (this.getEditorPart().getChartType() != null && this.getEditorPart().getChartType().equals(chartTypes[i])
						&& this.getEditorPart().getResult().getChartsEngine().getName().equals(chartsEngineNames.get(i))) {
					currentChartCreatorIndex = i;
				}

				chartTypes[i] = chartsEngineNames.get(i) + " / " + chartTypes[i]; //$NON-NLS-1$

			}

			TXMParameterCombo chartTypeParameterCombo = new TXMParameterCombo(this, chartEditor, SWT.READ_ONLY, chartTypes);
			this.chartTypeCombo = chartTypeParameterCombo.getControl();
			this.chartTypeCombo.select(currentChartCreatorIndex);
			this.chartTypeCombo.setToolTipText(ChartsEngineUIMessages.selectAchartTypeToRenderTheResult);


			// Advanced font selection (by physical font name)
			String advancedFonts[] = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
			TXMParameterCombo advancedFontParameterCombo = new TXMParameterCombo(this, chartEditor, SWT.READ_ONLY, advancedFonts);
			this.advancedFontCombo = advancedFontParameterCombo.getControl();
			this.advancedFontCombo.setToolTipText(ChartsEngineUIMessages.selectTheSystemFontToUse);

			int currentAllFontIndex = 0;
			for (int i = 0; i < advancedFonts.length; i++) {
				if (advancedFonts[i].equals(currentFont.getName())) {
					currentAllFontIndex = i;
					break;
				}
			}

			advancedFontCombo.select(currentAllFontIndex);

		}



		// Listeners
		SelectionListener listener = new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {

				boolean update = true;

				// FIXME: SJ: title, legend, grid visibility etc. should use the autoUpdateEditor annotation parameters copy functions
				if (e.getSource() == showTitleButton) {
					getEditorPart().getResult().setTitleVisible(showTitleButton.getSelection());
				}
				else if (e.getSource() == showLegendButton) {
					getEditorPart().getResult().setLegendVisible(showLegendButton.getSelection());
				}
				else if (e.getSource() == showGridButton) {
					getEditorPart().getResult().setGridVisible(showGridButton.getSelection());
				}
				else if (e.getSource() == renderingModeCombo) {
					getEditorPart().getResult().setRenderingColorsMode(renderingModeCombo.getSelectionIndex());
				}
				// end of FIXME

				// Font 
				else if (e.getSource() == fontCombo || e.getSource() == fontSizeCombo || e.getSource() == fontStyleCombo || e.getSource() == advancedFontCombo) {

					String fontName;
					if (e.getSource() == advancedFontCombo) {
						fontName = advancedFontCombo.getItem(advancedFontCombo.getSelectionIndex());
					}
					// transpose user-friendly name to logical font name
					else {
						fontName = fontCombo.getItem(fontCombo.getSelectionIndex());
					}

					getEditorPart().getResult().setFont(fontName,
							fontSizeCombo.getItem(fontSizeCombo.getSelectionIndex()),
							fontStyleCombo.getItem(fontStyleCombo.getSelectionIndex()),
							(e.getSource() == fontCombo));

				}
				// Chart type / chart creators
				else if (e.getSource() == chartTypeCombo) {
					String[] data = StringUtils.split(chartTypeCombo.getItem(chartTypeCombo.getSelectionIndex()), '/');//$NON-NLS-1$
					data[0] = StringUtils.trim(data[0]);
					data[1] = StringUtils.trim(data[1]);
					getEditorPart().getResult().setChartType(data[1]); // reset the chart
					getEditorPart().getResult().setChartsEngine((ChartsEngine) Toolbox.getEngineManager(EngineType.CHARTS).getEngine(data[0]));
					update = false;
				}

				getEditorPart().getResult().setChartDirty();

				// Auto-compute only for some of the widgets, not all
				if (RCPPreferences.getInstance().getBoolean(RCPPreferences.AUTO_UPDATE_EDITOR)
						|| e.getSource() == showTitleButton
						|| e.getSource() == showLegendButton
						|| e.getSource() == showGridButton
						|| e.getSource() == renderingModeCombo
						|| e.getSource() == fontSizeCombo
						|| e.getSource() == fontStyleCombo
						|| e.getSource() == chartTypeCombo
						|| e.getSource() == advancedFontCombo) {
					// updates or creates chart
					getEditorPart().compute(update);
					getEditorPart().setFocus();
				}
				else {
					getEditorPart().setDirty(true);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		};


		// Add listeners
		if (showTitleButton.isEnabled()) {
			showTitleButton.addSelectionListener(listener);
		}
		if (showLegendButton.isEnabled()) {
			showLegendButton.addSelectionListener(listener);
		}
		if (showGridButton.isEnabled()) {
			showGridButton.addSelectionListener(listener);
		}
		if (renderingModeCombo.isEnabled()) {
			renderingModeCombo.addSelectionListener(listener);
		}

		fontCombo.addSelectionListener(listener);
		fontSizeCombo.addSelectionListener(listener);
		fontStyleCombo.addSelectionListener(listener);

		if (this.chartTypeCombo != null) {
			this.chartTypeCombo.addSelectionListener(listener);
		}
		if (this.advancedFontCombo != null) {
			this.advancedFontCombo.addSelectionListener(listener);
		}
	}



	@Override
	protected void checkSubclass() {
	} // if this method is not defined then the ToolBar cannot be subclassed


}
