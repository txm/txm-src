package org.txm.chartsengine.rcp.handlers;

import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.core.results.Parameter;
import org.txm.rcp.handlers.export.ExportConfiguration;
import org.txm.utils.FileUtils;
import org.txm.utils.TXMProgressMonitor;

/**
 * Configuration for the "chart" export : width height and density
 * 
 * @author mdecorde
 *
 */
public class ExportChartConfiguration extends ExportConfiguration<ChartResult> {

	@Parameter(key = "width")
	Integer pWidth;

	@Parameter(key = "height")
	Integer pHeight;

	@Parameter(key = "density")
	Integer pDensity;

	@Parameter(key = "somethingelse")
	Integer pSomethingElse;

	public ExportChartConfiguration(String parametersNodePath) {
		this(parametersNodePath, null);
	}

	public ExportChartConfiguration(ChartResult parent) {
		this(null, parent);
	}

	public ExportChartConfiguration(String parametersNodePath, ChartResult parent) {
		super(parametersNodePath, parent);
	}

	@Override
	public String getDetails() {

		return "file: " + pFile + " width: " + pWidth + " height: " + pHeight; //$NON-NLS-1$
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		String selectedExt = FileUtils.getExtension(pFile).toLowerCase();

		return ChartsEngine.getCurrent().exportChartResultToFile(getParent(), pFile, pWidth, pHeight, selectedExt) != null;
	}

}
