package org.txm.chartsengine.rcp.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.messages.ChartsEngineUIMessages;
import org.txm.utils.logger.Log;

/**
 * Copies the chart view of the active chart editor part to the clipboard.
 * 
 * @author sjacquot
 *
 */
public class CopyChartViewToClipboard extends AbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ChartEditor<? extends ChartResult> chartEditor = SWTChartsComponentsProvider.getActiveChartEditor(event);
		chartEditor.copyChartViewToClipboard();

		Log.info(ChartsEngineUIMessages.chartViewCopiedToClipboard);

		return null;
	}

}
