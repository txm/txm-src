package org.txm.chartsengine.rcp.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.graphics.Point;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.messages.ChartsEngineUIMessages;
import org.txm.utils.logger.Log;

/**
 * Zoom in of the range axis the chart view of the active chart editor part.
 * 
 * @author mdecorde
 *
 */
public class ZoomInRangeChartView extends AbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ChartEditor<?> chartEditor = SWTChartsComponentsProvider.getActiveChartEditor(event);

		try {
			Point p = chartEditor.getChartComposite().getSize();
			chartEditor.zoom(p.x / 2, p.y / 2, true, true, false);
		}
		catch (Exception e) {
			Log.warning(NLS.bind(ChartsEngineUIMessages.ErrorWhileZoomingInP0P1, chartEditor, e));
		}
		return chartEditor;
	}

}
