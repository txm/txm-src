// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.chartsengine.rcp.handlers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.chartsengine.rcp.messages.ChartsEngineUIMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.StatusLine;
import org.txm.rcp.commands.OpenGraph;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.handlers.export.ExportResultDialog;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.JobHandler;
import org.txm.utils.FileUtils;
import org.txm.utils.logger.Log;

/**
 * Exports chart using charts engine export.
 * Exports the full chart using charts engine capabilities (e.g. used from outside a chart editor).
 *
 * @author mdecorde
 * @author sjacquot
 *
 */
// FIXME: SJ: this class should not used the SWTChartsComponentsProvider but directly the current charts engine
public class ExportChart extends BaseAbstractHandler {

	private static final String ID = ExportChart.class.getName();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object selection = this.getCorporaViewSelectedObject(event);

		if (!(selection instanceof ChartResult)) {
			return null;
		}

		final ChartResult chartResult = (ChartResult) selection;

		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();

		final ExportResultDialog dialog = new ExportResultDialog(shell, chartResult);

		// Create the extensions from the current charts engine supported file formats
		String extensions[] = new String[SWTChartsComponentsProvider.getCurrent().getChartsEngineSupportedExportFileFormats().size()];
		for (int i = 0; i < SWTChartsComponentsProvider.getCurrent().getChartsEngineSupportedExportFileFormats().size(); i++) {
			extensions[i] = "*." + SWTChartsComponentsProvider.getCurrent().getChartsEngineSupportedExportFileFormats().get(i); //$NON-NLS-1$
		}
		dialog.setFormats(extensions);
		dialog.setFormat("*."+ChartsEnginePreferences.getInstance().getString(ChartsEnginePreferences.DEFAULT_EXPORT_FORMAT)); //$NON-NLS-1$
		dialog.setFilePath(LastOpened.getFolder(ID), chartResult.getValidFileName());

		if (dialog.open() == Window.CANCEL) {
			Log.info(ChartsEngineUIMessages.canceledByUser);
			return null;
		}
		File file = dialog.getFile();
		StatusLine.setMessage(ChartsEngineUIMessages.ExportingChart);

		String selectedExt = FileUtils.getExtension(file).toLowerCase();

		ArrayList<String> formats = SWTChartsComponentsProvider.getCurrent().getChartsEngineSupportedExportFileFormats();
		if (!formats.contains(selectedExt)) {
			Log.warning(NLS.bind(ChartsEngineUIMessages.cannotExportWithTheP0FormatPleaseUserOneOFP1, selectedExt, formats));
			return null;
		}

		LastOpened.set(ID, file.getParent(), file.getName());

		try {
			file.createNewFile();
		}
		catch (IOException e1) {
			Log.severe(NLS.bind(TXMUIMessages.exportColonCantCreateFileP0ColonP1, file, e1));
			return null;
		}
		if (!file.canWrite()) {
			Log.severe(NLS.bind(TXMUIMessages.impossibleToReadP0, file));
			return null;
		}
		if (!file.isFile()) {
			Log.severe(ChartsEngineUIMessages.bind(ChartsEngineUIMessages.ErrorP0IsNotAFile, file)); //$NON-NLS-1$ //$NON-NLS-2$
			return null;
		}

		JobHandler jobhandler = new JobHandler(TXMUIMessages.exportingResults) {

			@Override
			protected IStatus _run(SubMonitor monitor) throws InterruptedException {
				monitor.beginTask(TXMUIMessages.exporting, 100);

				// compute the result if needed
				if (!chartResult.hasBeenComputedOnce()) {
					chartResult.compute(monitor);
				}

				Log.info(NLS.bind(TXMUIMessages.exportingP0, chartResult.getName()));

				int outputWidthInPixels = ChartsEnginePreferences.getInstance().getInt(ChartsEnginePreferences.EXPORT_WIDTH_IN_PIXELS);

				// negative means a value greater than max integer
				if((outputWidthInPixels * outputWidthInPixels) < 0) {
					Log.severe(ChartsEngineUIMessages.errorTheSelectedExportResolutionParametersLeadsToATooLargeImage);
				}
				else {
					chartResult.getChartsEngine().exportChartResultToFile(chartResult, file, outputWidthInPixels, outputWidthInPixels, selectedExt);

					if (file.exists()) {

						Log.info(NLS.bind(TXMUIMessages.p1ExportSavedInFileColonP0, file.getAbsolutePath(), selectedExt.toUpperCase()));

						// Open internal editor in the UI thread
						if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPORT_SHOW)) {
							this.syncExec(new Runnable() {

								@Override
								public void run() {
									OpenGraph.OpenFile(file.getAbsolutePath());
								}
							});
						}
					}
					else {
						Log.warning(NLS.bind(TXMUIMessages.failedToExportInFileP0WithFormatP1, file.getAbsolutePath(), selectedExt));
					}
					return Status.OK_STATUS;
				}
				return Status.CANCEL_STATUS;
			}
		};

		jobhandler.startJob();
		return null;
	}
}
