package org.txm.chartsengine.rcp.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.graphics.Point;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.messages.ChartsEngineUIMessages;
import org.txm.utils.logger.Log;

/**
 * Zoom out of the range axis of the chart view of the active chart editor part.
 * 
 * @author mdecorde
 *
 */
public class ZoomOutRangeChartView extends AbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ChartEditor<? extends ChartResult> chartEditor = SWTChartsComponentsProvider.getActiveChartEditor(event);

		try {
			Point p = chartEditor.getChartComposite().getSize();
			chartEditor.zoom(p.x / 2, p.y / 2, false, true, false);
		}
		catch (Exception e) {
			Log.warning(NLS.bind(ChartsEngineUIMessages.ErrorWhileZoomingOutInP0P1, chartEditor, e));
		}
		return chartEditor;
	}

}
