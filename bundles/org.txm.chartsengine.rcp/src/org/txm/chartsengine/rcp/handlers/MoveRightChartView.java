package org.txm.chartsengine.rcp.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.messages.ChartsEngineUIMessages;
import org.txm.utils.logger.Log;

/**
 * Moves to the right the chart view of the active chart editor part.
 * 
 * @author mdecorde
 *
 */
public class MoveRightChartView extends AbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ChartEditor<? extends ChartResult> chartEditor = SWTChartsComponentsProvider.getActiveChartEditor(event);

		try {
			chartEditor.pan(0, 0, 1, 0, 0.1);
		}
		catch (Exception e) {
			Log.warning(NLS.bind(ChartsEngineUIMessages.ErrorWhileMmovingTotherightInP0P1, chartEditor, e));
		}
		return chartEditor;
	}

}
