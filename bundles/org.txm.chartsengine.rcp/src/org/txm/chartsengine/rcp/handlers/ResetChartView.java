package org.txm.chartsengine.rcp.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.messages.ChartsEngineUIMessages;
import org.txm.utils.logger.Log;

/**
 * Resets the chart view of the active chart editor part.
 * 
 * @author sjacquot, mdecorde
 *
 */
public class ResetChartView extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ChartEditor<? extends ChartResult> chartEditor = SWTChartsComponentsProvider.getActiveChartEditor(event);

		try {
			chartEditor.resetView();
		}
		catch (Exception e) {
			Log.warning(NLS.bind(ChartsEngineUIMessages.ErrorWhileResettingViewInP0P1, chartEditor, e));
		}
		return chartEditor;
	}

}
