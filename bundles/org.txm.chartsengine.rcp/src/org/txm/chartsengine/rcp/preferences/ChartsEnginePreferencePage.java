package org.txm.chartsengine.rcp.preferences;

import java.util.Map;

import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.ChartsEnginesManager;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.chartsengine.rcp.messages.ChartsEngineUIMessages;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.jface.DoubleFieldEditor;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.widget.preferences.ComboFieldEditor;
import org.txm.rcp.swt.widget.preferences.StringFieldEditor;
import org.txm.utils.logger.Log;

/**
 * Charts engine preferences page.
 *
 * @author sjacquot
 *
 */
public class ChartsEnginePreferencePage extends TXMPreferencePage {

	/**
	 * The charts engine selection combo box.
	 */
	private ComboFieldEditor chartsEngineSelectionComboField;

	/**
	 * The charts engine export formats selection combo box.
	 */
	private ComboFieldEditor chartsEngineExportFormatsComboField;


	/**
	 * The export width in centimeters field.
	 */
	private DoubleFieldEditor exportWidthInCmField;;

	/**
	 * The export DPI resolutions selection comb box.
	 */
	private ComboFieldEditor exportDPIsComboField;

	
	
	//FIXME: SJ, 2025-01-30: old version with a disabled IntegerField	
//	/**
//	 * The final exported image computed width in pixels.
//	 */
//	private IntegerFieldEditor exportWidthInPixels;

	
	//FIXME: SJ, 2025-01-30: new version with two Labels
	/**
	 * Label for the final exported image width in pixels.
	 */
	private Label exportWidthInCmLabel;
	
	/**
	 * The final exported image computed width in pixels.
	 */
	private Label exportWidthInCmComputedValue;


	

	//FIXME: SJ: can't be computed because it's based on the height of the chart view component.
	// But we could define a ratio here for the export chart from result node
	//private IntegerFieldEditor exportHeightInPxels;

	private int tmpExportDPI;


	protected boolean charsEngineHasChanged = false;


	@Override
	protected void createFieldEditors() {
		
		// Charts engine selection
		String chartsEngines[][] = new String[ChartsEnginesManager.getInstance().size()][2];

		int i = 0;
		for (Map.Entry<String, ChartsEngine> entry : ChartsEnginesManager.getInstance().entrySet()) {
			chartsEngines[i][0] = entry.getValue().getDetails();
			chartsEngines[i][1] = entry.getValue().getName();
			i++;
		}

		this.chartsEngineSelectionComboField = new ComboFieldEditor(ChartsEnginePreferences.CURRENT_NAME, ChartsEngineUIMessages.currentEngine, chartsEngines, this.getFieldEditorParent());
		this.addField(this.chartsEngineSelectionComboField);


		// FIXME: SJ: test to select the chart engine order rather than a exclusive one since the engines are not exclusive anymore, eg. some to create charts and other to create graphs
		// ListEditor listEditor = new ListEditor(ChartsEnginePreferences.CHARTS_ENGINES_ORDER, "Ordre de préférence des moteurs", this.getFieldEditorParent()) {
		//
		// @Override
		// protected String[] parseString(String enginesOrder) {
		// return ChartsEnginePreferences.getEnginesOrder(enginesOrder);
		// }
		//
		// @Override
		// protected String getNewInputObject() {
		//// InputDialog dialog = new InputDialog(getShell(), "New Buildconfiguration", "Enter the name of the Buildconfiguration to add", "", null);
		//// if (dialog.open() == InputDialog.OK) {
		//// return dialog.getValue();
		//// }
		// return null;
		// }
		//
		// @Override
		// protected String createList(String[] engineNames) {
		// return ChartsEnginePreferences.createEnginesOrder(engineNames);
		// }
		// };
		//
		// // default initialization
		// if(!TXMPreferences.keyExists("/" + InstanceScope.SCOPE +"/" + ChartsEnginePreferences.getInstance().getPreferencesNodeQualifier(), ChartsEnginePreferences.CHARTS_ENGINES_ORDER)) {
		// ChartsEnginePreferences.getInstance().put(ChartsEnginePreferences.CHARTS_ENGINES_ORDER, ChartsEnginesManager.getInstance().createEnginesOrder());
		// }
		//
		// listEditor.setPreferenceName(ChartsEnginePreferences.CHARTS_ENGINES_ORDER);
		// listEditor.load();
		//
		// // hide the "New" and "Remove" buttons
		// listEditor.getButtonBoxControl(this.getFieldEditorParent()).getChildren()[0].setVisible(false);;
		// listEditor.getButtonBoxControl(this.getFieldEditorParent()).getChildren()[1].setVisible(false);
		//
		// this.addField(listEditor);



		// current engine supported export formats
		this.createExportFormatComboField(ChartsEnginesManager.getInstance().getCurrentEngine());


		// Export advanced configuration
		Group exportGroup = new Group(this.getFieldEditorParent(), SWT.NONE);
		exportGroup.setText(ChartsEngineUIMessages.preferences_label_exportedChartsProperties);
		GridData gridData = new GridData(SWT.NONE, SWT.NONE, true, false);
		gridData.horizontalSpan = 2;
		gridData.verticalIndent = 10;
		exportGroup.setLayoutData(gridData);

		
		// width in centimeters to aim for exported image
		this.exportWidthInCmField = new DoubleFieldEditor(ChartsEnginePreferences.EXPORT_WIDTH_IN_CM, ChartsEngineUIMessages.preferences_label_widthCm, exportGroup) {
			@Override
			public boolean isValid() {
		
				//SJ: force the valid state by redefining some value in range
				boolean isValid = true; 

				try {
					double value = Double.parseDouble(this.getStringValue());
					if(value < 1) {
						this.setStringValue("1"); //$NON-NLS-1$
//						isValid = false;
					}
					else if(value > 120) {
	//					isValid = false;
						this.setStringValue("120"); //$NON-NLS-1$
					}
				}
				catch (NumberFormatException e) {
					this.setStringValue("1"); //$NON-NLS-1$
				}
				return isValid;
			}
			
			
			@Override
			protected boolean doCheckState() {
				return isValid();
			}
		};
		this.exportWidthInCmField.setValidateStrategy(StringFieldEditor.VALIDATE_ON_KEY_STROKE);
		this.exportWidthInCmField.setTextLimit(5); // for max value length as 120.50 cm
		this.exportWidthInCmField.getTextControl(exportGroup).setToolTipText(ChartsEngineUIMessages.displayOrPrintWidthInCentimeters);
		this.exportWidthInCmField.getLabelControl(exportGroup).setToolTipText(ChartsEngineUIMessages.displayOrPrintWidthInCentimeters);
		this.addField(this.exportWidthInCmField);
		
		// DPI used for calculating the exported raster image pixels dimensions or scalable dimensions for vector image
		String dpis[][] = new String[7][2];
		dpis[0][0] = dpis[0][1] = "72"; //$NON-NLS-1$
		dpis[1][0] = dpis[1][1] = "96"; //$NON-NLS-1$
		dpis[2][0] = dpis[2][1] = "150"; //$NON-NLS-1$
		dpis[3][0] = dpis[3][1] = "300"; //$NON-NLS-1$
		dpis[4][0] = dpis[4][1] = "400"; //$NON-NLS-1$
		dpis[5][0] = dpis[5][1] = "600"; //$NON-NLS-1$
		dpis[6][0] = dpis[6][1] = "1200"; //$NON-NLS-1$

		this.exportDPIsComboField = new ComboFieldEditor(ChartsEnginePreferences.EXPORT_DPI_RESOLUTION, ChartsEngineUIMessages.preferences_label_resolutionDPI, dpis, exportGroup);
		this.exportDPIsComboField.getLabelControl(exportGroup).setToolTipText(ChartsEngineUIMessages.preferences_tooltip_outputResolutionInDPI);
		this.addField(this.exportDPIsComboField);
		this.tmpExportDPI = ChartsEnginePreferences.getInstance().getInt(ChartsEnginePreferences.EXPORT_DPI_RESOLUTION);
		
		//FIXME: SJ, 2025-01-30: old version with a disabled IntegerField
//		this.exportWidthInPixels = new IntegerFieldEditor(ChartsEnginePreferences.EXPORT_WIDTH_IN_PIXELS, ChartsEngineUIMessages.preferences_label_exportedWidthPixels, exportGroup);
//		this.exportWidthInPixels.getTextControl(exportGroup).setEnabled(false);
//		this.exportWidthInPixels.getTextControl(exportGroup).setToolTipText(ChartsEngineUIMessages.preferences_tooltip_exportedImageWidthInPixels);
//		this.exportWidthInPixels.getLabelControl(exportGroup).setToolTipText(ChartsEngineUIMessages.preferences_tooltip_exportedImageWidthInPixels);
//		this.addField(this.exportWidthInPixels);

		//FIXME: SJ, 2025-01-30: new version with two Labels
		this.exportWidthInCmLabel = new Label(exportGroup, SWT.NONE);
		this.exportWidthInCmLabel.setText(ChartsEngineUIMessages.preferences_label_exportedWidthPixels);
		this.exportWidthInCmLabel.setToolTipText(ChartsEngineUIMessages.preferences_tooltip_exportedImageWidthInPixels);
		
		this.exportWidthInCmComputedValue = new Label(exportGroup, SWT.NONE);
		this.exportWidthInCmComputedValue.setText(ChartsEnginePreferences.getInstance().getString(ChartsEnginePreferences.EXPORT_WIDTH_IN_PIXELS));
		this.exportWidthInCmComputedValue.setToolTipText(ChartsEngineUIMessages.preferences_tooltip_exportedImageWidthInPixels);

		((GridLayout)exportGroup.getLayout()).marginWidth = 3;
		
		
		
		// other shared preferences
		SWTChartsComponentsProvider.createChartsRenderingPreferencesFields(this, this.getFieldEditorParent());

		// Monochrome color
		this.addField(new ColorFieldEditor(ChartsEnginePreferences.MONOCHROME_COLOR, ChartsEngineUIMessages.monochromeRenderingColor, this.getFieldEditorParent()));


		//FIXME: SJ: old version with real font names
		//		FontFieldEditor font = new FontFieldEditor(ChartsEnginePreferences.FONT, "Font", "Specificities / Spécificités / Специфичность", this.getFieldEditorParent()); //$NON-NLS-2$
		//		//FontFieldEditor font = new FontFieldEditor(ChartsEnginePreferences.FONT, "Font", "Specificities / Spécificités / Специфичность / 仕様 / 特別說明 / विशिष्टता / التصميم مثل الفوتوشوب و", this.getFieldEditorParent()); //$NON-NLS-2$
		//		this.addField(font);
		//		// FIXME: SJ: preview area tests
		//		 //font.fillIntoGrid(this.getFieldEditorParent(), 4);
		//		// ((Text)font.getPreviewControl()).setLayoutData(new GridData(GridData.FILL_BOTH));
		//		// System.out.println("ChartsEnginePreferencePage.createFieldEditors() " + font.getPreviewControl());

		// default font preference (user-friendly names)
		String logicalFonts[][] = new String[ChartsEngine.FONTS_USER_FRIENDLY_NAMES.length][2];
		for (int j = 0; j < ChartsEngine.FONTS_USER_FRIENDLY_NAMES.length; j++) {
			logicalFonts[j][0] = ChartsEngine.FONTS_USER_FRIENDLY_NAMES[j];
			logicalFonts[j][1] = String.format("1|%s|14|0", ChartsEngine.FONTS_LOGICAL_NAMES[j]); //$NON-NLS-1$
		}
		ComboFieldEditor fontComboField = new ComboFieldEditor(ChartsEnginePreferences.FONT, TXMCoreMessages.fontFamily, logicalFonts, this.getFieldEditorParent());
		this.addField(fontComboField);

	}


	/**
	 * Creates the export format combo box according the specified charts engine.
	 *
	 * @param engine
	 */
	protected void createExportFormatComboField(ChartsEngine engine) {
		this.chartsEngineExportFormatsComboField = new ComboFieldEditor(ChartsEnginePreferences.DEFAULT_EXPORT_FORMAT, ChartsEngineUIMessages.defaultExportFormat, ChartsEnginesManager
				.getExportFormatsEntryNamesAndValues(engine), this.getFieldEditorParent());
		this.addField(this.chartsEngineExportFormatsComboField);
	}


	@Override
	public void propertyChange(PropertyChangeEvent event) {
		super.propertyChange(event);

		//FIXME: SJ: it seems we can not retrieve the value of a selected ComboFieldEditor so we need to store it
		if (event.getSource() == this.exportDPIsComboField) {
			this.tmpExportDPI = Integer.valueOf(String.valueOf(event.getNewValue()));
		}

		// FIXME: SJ: test to try to update the Export formats combo box when changing of current charts engine
		// NOTE: SJ: warning, event.getProperty() seems bugged and does not return the correct property name but the string "field_value_editor"
		if (event.getSource() == this.chartsEngineSelectionComboField && !event.getNewValue().equals(event.getOldValue())) {
			this.charsEngineHasChanged = true;
			// this.chartsEngineExportFormatsComboField.dispose();
			// this.createExportFormatComboField(ChartsEnginesManager.getInstance().getEngine((String) event.getNewValue()));
			// this.adjustGridLayout();
			// this.getFieldEditorParent().layout();
		}
		// update the image output export width in pixels value
		else if (event.getSource() == this.exportWidthInCmField || event.getSource() == this.exportDPIsComboField) {
			try {
				// update the charts engine preference
				 ChartsEnginePreferences.getInstance().put(ChartsEnginePreferences.EXPORT_WIDTH_IN_PIXELS, String.valueOf(((ChartsEnginePreferences) ChartsEnginePreferences.getInstance()).getExportWidthInPixels(this.tmpExportDPI, this.exportWidthInCmField.getDoubleValue())));
				// update the widget
				this.exportWidthInCmComputedValue.setText(ChartsEnginePreferences.getInstance().getString(ChartsEnginePreferences.EXPORT_WIDTH_IN_PIXELS));
			}
			catch (Exception e) {
				// Nothing to do
			}
		}

	}



	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(ChartsEnginePreferences.getInstance().getPreferencesNodeQualifier()));
		this.setDescription(ChartsEngineUIMessages.thesePreferencesWillApplyToNewChartsPreviouslyCreatedChartsWillNotBeAffected);
	}

	@Override
	public boolean performOk() {

		super.performOk();


		// Set the new monochrome color for all charts engine theme
		for (ChartsEngine engine : ChartsEnginesManager.getChartsEngines()) {
			engine.getTheme().setMonochromeColor(ChartsEnginePreferences.getInstance().getString(ChartsEnginePreferences.MONOCHROME_COLOR));
		}

		Log.info(ChartsEngineUIMessages.recreatingChartsEngineAndSWTChartComponentsProvider);

		ChartsEnginesManager.getInstance().setCurrentEngine(this.getPreferenceStore().getString(ChartsEnginePreferences.CURRENT_NAME));

		// Sets the new current charts SWT component provider according to the new current charts engine
		SWTChartsComponentsProvider.setCurrrentComponentsProvider(ChartsEnginesManager.getInstance().getCurrentEngine());

		// Reset the default export format to the new current charts engine first supported export file format
		// ChartsEnginePreferences.getInstance().put(ChartsEnginePreferences.DEFAULT_EXPORT_FORMAT, ChartsEngine.getCurrent().getSupportedOutputFileFormats().get(0));


		return true;
	}
}
