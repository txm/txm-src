package org.txm.chartsengine.rcp.preferences;

import java.util.Properties;

//TODO : définir ici les entrées souris pour le pan, zoom, etc. et les utiliser dans les listeners des implémentations du ChartsComponentsProvider
// Ainsi ce sera mutualisé entre R et JFC par exemple
/**
 * Mouse controls properties which will be shared by all implementations and chart editors.
 * 
 * @author Sebastien Jacquot
 *
 */
// FIXME : test class. See if we'll use the RCP Store system instead
public class ___MouseBindingProperties extends Properties {


	public final static String PAN = "charts_engine_mouse_binding_pan", ZOOM = "charts_engine_mouse_binding_zoom"; //$NON-NLS-1$ //$NON-NLS-2$


	/**
	 * Creates a mouse controls binding with the specified properties.
	 * 
	 * @param defaults
	 */
	public ___MouseBindingProperties(Properties defaults) {
		super(defaults);
	}

	/**
	 * Creates a mouse controls binding with default properties.
	 */
	public ___MouseBindingProperties() {
		this.setProperty(___MouseBindingProperties.PAN, ""); //$NON-NLS-1$
	}

}
