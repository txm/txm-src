package org.txm.chartsengine.rcp.preferences;

import java.util.Properties;

// TODO : définir ici les touches clavier pour le pan, zoom, etc. et les utiliser dans les listeners des implémentations du ChartsComponentsProvider
//Ainsi ce sera mutualisé entre R et JFC par exemple
/**
 * Mouse controls properties which will be shared by all implementations and chart editors.
 * 
 * @author Sebastien Jacquot
 *
 */
//FIXME : test class. See if we'll use the RCP Store system instead
public class ___KeyboardBindingProperties extends Properties {


	public final static String PAN_LEFT = "charts_engine_keyboard_binding_pan_left", PAN_DOWN = "charts_engine_keyboard_binding_pan_down", PAN_RIGHT = "charts_engine_keyboard_binding_pan_right", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			PAN_UP = "charts_engine_keyboard_binding_pan_up"; //$NON-NLS-1$



	/**
	 * Creates a keyboard controls binding with the specified properties.
	 * 
	 * @param defaults
	 */
	public ___KeyboardBindingProperties(Properties defaults) {
		super(defaults);
	}


	/**
	 * Creates a keyboard controls binding with default properties.
	 */
	public ___KeyboardBindingProperties() {
		this.setProperty(___KeyboardBindingProperties.PAN_LEFT, ""); //$NON-NLS-1$
		this.setProperty(___KeyboardBindingProperties.PAN_DOWN, ""); //$NON-NLS-1$
		this.setProperty(___KeyboardBindingProperties.PAN_RIGHT, ""); //$NON-NLS-1$
		this.setProperty(___KeyboardBindingProperties.PAN_UP, ""); //$NON-NLS-1$
	}



}
