package org.txm.chartsengine.rcp.editors;

import org.txm.chartsengine.core.results.ChartResult;
import org.txm.rcp.editors.TXMResultEditorInput;

/**
 * Base class of all charts SWT <code>IEditorInput</code> implementations.
 * 
 * @author sjacquot
 *
 */
// FIXME: SJ: this class may became useless, we may use TXMEditorInput instead
public class ChartEditorInput<T extends ChartResult> extends TXMResultEditorInput<T> {


	/**
	 * Creates a chart editor input linked to the current SWT charts components provider.
	 * 
	 * @param result
	 */
	public ChartEditorInput(T result) {
		super(result);
	}

	/**
	 * Gets a string representing the type of the chart as defined in the chart creator extension.
	 * 
	 * @return the chartType or <code>null</code> if there only one chart type needed for the linked <code>TXMResult</code>.
	 */
	public String getChartType() {
		return this.getResult().getChartType();
	}

	@Override
	public T getResult() {
		return (T) this.result;
	}


	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ChartEditorInput) {
			ChartEditorInput<?> cei = (ChartEditorInput<?>) obj;
			if (this.alwaysRecreateEditor) {
				return false;
			}
			return this.result == cei.getResult();
		}
		return false;
	}

	/**
	 * Gets the chart object. According to the used charts engine, can be a file, an UI component, etc.
	 * 
	 * @return
	 */
	public Object getChart() {
		try {
			return ((ChartResult) this.result).getChart();
		}
		catch (Exception e) {
		}
		return null;
	}

}
