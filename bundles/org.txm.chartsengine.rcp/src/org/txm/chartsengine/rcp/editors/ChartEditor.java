package org.txm.chartsengine.rcp.editors;


import java.io.File;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IPartService;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.contexts.IContextActivation;
import org.eclipse.ui.contexts.IContextService;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.chartsengine.rcp.messages.ChartsEngineUIMessages;
import org.txm.chartsengine.rcp.swt.AdvancedChartEditorToolBar;
import org.txm.chartsengine.rcp.swt.ChartComposite;
import org.txm.chartsengine.rcp.swt.ChartEditorToolBar;
import org.txm.chartsengine.rcp.swt.CompassChartEditorToolBar;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.rcp.editors.TXMEditor;
import org.txm.utils.logger.Log;

/**
 * Base class of all charts SWT <code>EditorPart</code> subclasses.
 *
 * @author sjacquot
 *
 */
public abstract class ChartEditor<T extends ChartResult> extends TXMEditor<T> {


	public final static String CONTEXT_ID_CHART_EDITOR = ChartEditor.class.getName();


	// ChartEditor RCP context activation/deactivation
	static {

		IPartService service = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getPartService();
		service.addPartListener(new IPartListener() {

			@Override
			public void partOpened(IWorkbenchPart part) {
				// TODO Auto-generated method stub

			}

			@Override
			public void partDeactivated(IWorkbenchPart part) {
				if (part instanceof ChartEditor) {
					((ChartEditor<?>) part).onPartDeactivated();
				}
			}

			@Override
			public void partClosed(IWorkbenchPart part) {
				// TODO Auto-generated method stub

			}

			@Override
			public void partBroughtToTop(IWorkbenchPart part) {
				// TODO Auto-generated method stub

			}

			@Override
			public void partActivated(IWorkbenchPart part) {
				if (part instanceof ChartEditor) {
					((ChartEditor<?>) part).onPartActivated();
				}
			}
		});

	}


	/**
	 * SWT chart components provider used to create the chart composite.
	 * Store it allows the switch of component provider in the RCP in keeping the chart editors in a coherent state, by testing that the SWT Components providers charts engine match the result charts
	 * engine.
	 */
	protected SWTChartsComponentsProvider swtChartsComponentsProvider;

	/**
	 * The tool bar.
	 */
	protected ChartEditorToolBar chartToolBar;

	/**
	 * Export chart editor view tool item button.
	 */
	protected ToolItem exportChartEditorViewButton;

	/**
	 * Copy chart view to clipboard tool item button.
	 */
	protected ToolItem copyChartViewToClipboardButton;

	/**
	 * The advanced tool bar.
	 */
	protected AdvancedChartEditorToolBar advancedChartToolBar;

	/**
	 * The advanced rendering parameters composite.
	 */
	protected Composite advancedToolBarComposite;

	/**
	 * The advanced rendering parameters group.
	 */
	protected Group advancedToolBarGroup;

	/**
	 * The chart drawing area composite.
	 */
	protected ChartComposite chartComposite;

	/**
	 * To check whether this editor was already opened when calling SWTChartsComponentsProvider.openEditor().
	 */
	protected boolean wasAlreadyOpened;

	//	/**
	//	 * The multi pages editor which contains this editor if exists.
	//	 */
	//	protected TXMMultiPageEditor parentMultiPagesEditor = null;

	/**
	 * Token used to deactivate the context after it has been activated.
	 * The context is used by the RCP to bind keys or contextual menu to an editor.
	 */
	protected IContextActivation contextActivationToken;

	private CompassChartEditorToolBar compassChartToolBar;


	/**
	 *
	 */
	public ChartEditor() {
		super();
	}


	/**
	 * Creates a new editor.
	 *
	 * @param chartEditorInput
	 */
	protected ChartEditor(ChartEditorInput<?> chartEditorInput) {
		this.setInput(chartEditorInput);
		this.setPartName(chartEditorInput.getName());

		this.wasAlreadyOpened = false;
	}


	/**
	 * Subclasses manual creation.
	 *
	 * @throws Exception
	 */
	public abstract void __createPartControl() throws Exception;


	@Override
	public final void _createPartControl() throws Exception {

		// Toolbar
		this.chartToolBar = new ChartEditorToolBar(this, this.getFirstLineComposite(), this.getExtendedParametersComposite(), SWT.FLAT | SWT.RIGHT);
		this.getFirstLineComposite().getLayout().numColumns += 1;

		// store buttons to enable/disable them
		this.exportChartEditorViewButton = this.chartToolBar.getItemByContributionId("exportChartEditorView"); //$NON-NLS-1$
		this.copyChartViewToClipboardButton = this.chartToolBar.getItemByContributionId("copyChartViewToClipboard"); //$NON-NLS-1$

		// Advanced tool bar
		Composite group = this.topToolBar.installGroup(ChartsEngineUIMessages.rendering, ChartsEngineUIMessages.showHideRenderingParameters,
				"platform:/plugin/org.txm.chartsengine.rcp/icons/show_rendering_parameters.png",  //$NON-NLS-1$
				"platform:/plugin/org.txm.chartsengine.rcp/icons/hide_rendering_parameters.png", //$NON-NLS-1$
				false);
		this.advancedChartToolBar = new AdvancedChartEditorToolBar(this, group, this.getExtendedParametersComposite(), SWT.FLAT);

		// uninstall chart parameters group if BEGINNER MODE ACTIVATED
		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.BEGINNER_USER)) {
			this.topToolBar.getGroupButton(ChartsEngineUIMessages.rendering).setEnabled(false);
		}

		// Navigation tool bar
		Composite compassGroup = this.chartToolBar.installGroup(ChartsEngineUIMessages.navigation, ChartsEngineUIMessages.navigationToolsToZoomPanEtcInTheView,
				"platform:/plugin/org.txm.chartsengine.rcp/icons/compass.png",  //$NON-NLS-1$
				"platform:/plugin/org.txm.chartsengine.rcp/icons/compass.png", //$NON-NLS-1$
				false);
		this.compassChartToolBar = new CompassChartEditorToolBar(this, compassGroup, this.getExtendedParametersComposite(), SWT.FLAT);

		this.swtChartsComponentsProvider = SWTChartsComponentsProvider.getComponentsProvider(this.getResult().getChartsEngine());

		if (this.swtChartsComponentsProvider == null) {
			throw new Exception("No suitable component found to render the specified chart."); //$NON-NLS-1$
		}

		// Chart composite
		this.chartComposite = this.getSWTChartsComponentsProvider().createComposite(this, this.getResultArea());

		GridData gd = new GridData(GridData.FILL_BOTH);
		this.chartComposite.setLayoutData(gd);

		// subclasses manual creation
		this.__createPartControl();


		// repack the toolbars after eventual item adding by subclasses
		this.chartToolBar.pack();
		this.advancedChartToolBar.pack();
	}


	@Override
	public void setDirty(boolean dirty) {
		super.setDirty(dirty);

		// FIXME: SJ: enable/disable the buttons according to the computed state of the chart
		//this.exportChartEditorViewButton.setEnabled(this.getResult().hasBeenComputedOnce());
		//this.copyChartViewToClipboardButton.setEnabled(this.getResult().hasBeenComputedOnce());
	}

	/**
	 * Opens an editor where id is the specified result class name, computes the result then refreshes the editor components.
	 *
	 * @param result
	 */
	public static ChartEditor<ChartResult> openEditor(ChartResult result) {
		return openEditor(result, false);
	}

	/**
	 *
	 * @param result
	 * @param alwaysRecreateEditor
	 * @return
	 */
	public static ChartEditor<ChartResult> openEditor(ChartResult result, boolean alwaysRecreateEditor) {
		return openEditor(result, result.getClass().getName(), alwaysRecreateEditor);
	}

	/**
	 *
	 * @param result
	 * @param editorPartId
	 * @return
	 */
	public static ChartEditor<ChartResult> openEditor(ChartResult result, String editorPartId) {
		return openEditor(result, editorPartId, false);
	}

	/**
	 * Opens an editor specified by its id, computes the specified result then refreshes the editor components.
	 *
	 * @param result
	 * @param editorPartId
	 * @param alwaysRecreateEditor force editor creation
	 * @return
	 */
	public static ChartEditor<ChartResult> openEditor(ChartResult result, String editorPartId, boolean alwaysRecreateEditor) {

		ChartEditorInput<ChartResult> editorInput = new ChartEditorInput<>(result);
		editorInput.setAlwaysRecreateEditor(alwaysRecreateEditor);

		return (ChartEditor<ChartResult>) openEditor(editorInput, editorPartId);
	}


	@Override
	public void updateResultFromEditor() {
		// does nothing by default
	}

	/**
	 * Synchronizes the editor with the chart result.
	 * Dedicated to dynamically change the editor components values from the stored result data.
	 */
	public abstract void updateEditorFromChart(boolean update);


	@Override
	public final void updateEditorFromResult(boolean update) {

		// check that the charts engine used for the result matches the SWT components provider otherwise find a suitable components provider
		// and recreate new ChartComposite and ChartComponent
		if (this.getResult().getChartsEngine() != this.swtChartsComponentsProvider.getChartsEngine()) {

			Log.finest("ChartEditor.updateEditorFromResult(): result charts engine  = " + this.getResult().getChartsEngine().getName() + " / Editor charts engine = " + this.swtChartsComponentsProvider //$NON-NLS-1$ //$NON-NLS-2$
					.getChartsEngine().getName() + ".");  //$NON-NLS-1$

			this.swtChartsComponentsProvider = SWTChartsComponentsProvider.getComponentsProvider(this.getResult().getChartsEngine());
			this.swtChartsComponentsProvider.setChartsEngine(this.getResult().getChartsEngine());

			Log.finest("ChartEditor.updateEditorFromResult(): charts engine used to create the chart does not match the editor one. The SWT components provider has been changed to match it: " + this //$NON-NLS-1$
					.getSWTChartsComponentsProvider() + ".");  //$NON-NLS-1$

			// recreating the chart composite
			Composite parent = this.chartComposite.getParent();
			this.chartComposite.dispose();
			this.chartComposite = this.getSWTChartsComponentsProvider().createComposite(this, parent);

			GridData gd = new GridData(GridData.FILL_BOTH);
			this.chartComposite.setLayoutData(gd);
			parent.layout();
		}


		boolean needInit = (this.chartComposite.getChartComponent() == null) && (this.getResult().getChart() != null);

		// loading chart
		Log.fine(TXMCoreMessages.bind("Loading chart of type {0}...", this.getResult().getResultType())); //$NON-NLS-1$
		this.loadChart(needInit);

		// subclass updating
		this.updateEditorFromChart(update);

	}


	/**
	 * Method called if the editor needs initialization (if the chart component has not yet been created and if the chart is not null) and dedicated to be override by subclasses if needed to do some
	 * process after the initialization.
	 */
	public void onInit() {
		// nothing to do
	}

	/**
	 * Activates the chart editor context.
	 * The context is used, for example, to enable keyboard shortcut only for ChartEditor.
	 */
	public void activateContext() {
		IContextService contextService = getSite().getService(IContextService.class);
		if (contextService != null) {
			this.contextActivationToken = contextService.activateContext(CONTEXT_ID_CHART_EDITOR);

			Log.finest(TXMCoreMessages.bind("ChartEditor.activateContext(): context activated (token = {0}).", this.contextActivationToken)); //$NON-NLS-1$
		}
		else {
			Log.finest("ChartEditor.activateContext(): context activation falied."); //$NON-NLS-1$
		}
	}


	/**
	 * Deactivates the chart editor context.
	 * The context is used, for example, to enable keyboard shortcut only for ChartEditor.
	 */
	public void deactivateContext() {
		IContextService contextService = getSite().getService(IContextService.class);
		if (contextService != null) {
			contextService.deactivateContext(this.contextActivationToken);

			Log.finest(TXMCoreMessages.bind("ChartEditor.deactivateContext(): context deactivated (token = {0}).", this.contextActivationToken)); //$NON-NLS-1$
		}
		else {
			Log.finest("ChartEditor.activateContext(): context deactivation failed."); //$NON-NLS-1$
		}
	}


	// @Override
	// public void _setFocus() {
	//
	// // FIXME: SJ: old code, temporary disabled
	// // Debug
	// Log.finest("ChartEditor.setFocus(): giving focus to chart composite..."); //$NON-NLS-1$
	//
	// this.parent.setFocus();
	// // super.setFocus();
	// //
	// // this.chartComposite.setFocus();
	// //
	// //
	//
	// // SJ: new code
	// // super.setFocus();
	//
	// }



	public void onPartActivated() {
		Log.finest("+++++ ChartEditor.onPartActivated(): " + this.getResult()); //$NON-NLS-1$
		this.activateContext();
	}

	public void onPartDeactivated() {
		Log.finest("----- ChartEditor.onPartDeactivated(): " + this.getResult()); //$NON-NLS-1$
		this.deactivateContext();
	}


	/**
	 * Convenience method the get the casted linked editor input.
	 *
	 * @return the editorInput
	 */
	@Override
	public ChartEditorInput<T> getEditorInput() {
		return (ChartEditorInput<T>) super.getEditorInput();
	}


	/**
	 * Gets the chart drawing area composite.
	 *
	 * @return the composite
	 */
	public ChartComposite getComposite() {
		return this.chartComposite;
	}


	/**
	 * Resets the chart view (zoom, pan, rotation).
	 */
	public void resetView() {
		this.chartComposite.resetView();
	}

	/**
	 * Clears the selected items in chart.
	 */
	public void clearChartItemsSelection() {
		this.chartComposite.clearChartItemsSelection();
	}

	/**
	 * Zooms the chart view at x and y coordinates.
	 *
	 * @param x
	 * @param y
	 * @param zoomIn
	 */
	public void zoom(double x, double y, boolean zoomIn, boolean range, boolean domain) {
		this.chartComposite.zoom(x, y, zoomIn, range, domain);
	}

	/**
	 * Pans the chart view.
	 *
	 * @param srcX
	 * @param srcY
	 * @param dstX
	 * @param dstY
	 * @param panFactor
	 */
	public void pan(double srcX, double srcY, double dstX, double dstY, double panFactor) {
		this.chartComposite.pan(srcX, srcY, dstX, dstY, panFactor);
	}


	/**
	 * Copy the current chart view to clipboard.
	 */
	public void copyChartViewToClipboard() {
		try {
			this.chartComposite.copyChartViewToClipboard();
		}
		catch (Exception e) {
			Log.warning("ChartEditor.copyChartViewToClipboard() - error : "+e); //$NON-NLS-1$
			Log.printStackTrace(e);
		}
	}

	/**
	 * Loads the chart from the chart object stored into the composite.
	 *
	 * @param needInit
	 */
	public void loadChart(final boolean needInit) {


		Point size = chartComposite.getSize();
		if (size.x == 0 || size.y == 0) { // MD: using asyncExec should force the chartComposite to be displayed before loading the chart
			this.getSite().getShell().getDisplay().asyncExec(new Runnable() {

				@Override
				public void run() {
					chartComposite.loadChart(
							getResult().needsToResetView(),
							getResult().needsToClearItemsSelection(),
							getResult().needsToSquareOff());

					// FIXME: SJ: this code should be moved in ChartComposite.loadChart()?
					if (needInit) {
						onInit();
					}
				}
			});
		}
		else { // TODO MD: remove this duplicated code using lambdas
			this.getSite().getShell().getDisplay().syncExec(new Runnable() {

				@Override
				public void run() {
					chartComposite.loadChart(
							getResult().needsToResetView(),
							getResult().needsToClearItemsSelection(),
							getResult().needsToSquareOff());

					// FIXME: SJ: this code should be moved in ChartComposite.loadChart()?
					if (needInit) {
						onInit();
					}
				}
			});
		}

		// reset the states
		getResult().setNeedsToResetView(false);
		getResult().setNeedsToClearItemsSelection(false);

	}


	/**
	 * Disposes the editor.
	 * Implementations using a Composite to embed AWT/Swing components should manually call this.chartComposite.dispose() in the redefined method.
	 */
	@Override
	public void dispose() {
		if (this.chartComposite != null && !this.chartComposite.isDisposed()) {
			this.chartComposite.dispose();
		}
		super.dispose();
	}

	/**
	 * Returns the tool bar of this editor.
	 *
	 * @return the toolBar
	 */
	public ChartEditorToolBar getToolBar() {
		return chartToolBar;
	}

	/**
	 * Returns the advanced tool bar of this editor.
	 *
	 * @return
	 */
	public AdvancedChartEditorToolBar getAdvancedToolBar() {
		return this.advancedChartToolBar;
	}


	/**
	 * Gets the chart type. A same result can be drawn as multiple chart types (eg. pie, bar, etc.).
	 *
	 * @return
	 */
	public String getChartType() {
		return this.getResult().getChartType();
	}

	/**
	 * Returns the chart associated with the editor.
	 *
	 * @return
	 */
	public Object getChart() {
		return this.getResult().getChart();
	}

	/**
	 * Returns the chart components provider associated with the editor.
	 *
	 * @return
	 */
	public SWTChartsComponentsProvider getSWTChartsComponentsProvider() {
		return this.swtChartsComponentsProvider;
	}

	/**
	 * Gets the charts engine used by the result.
	 *
	 * @return
	 */
	public ChartsEngine getChartsEngine() {
		return this.getResult().getChartsEngine();
	}

	/**
	 * Exports the current view of the chart editor in the specified file.
	 *
	 * @param file
	 * @param fileFormat
	 * @return
	 */
	public File exportView(File file, String fileFormat) {
		return this.chartComposite.exportView(file, fileFormat);
	}


	/**
	 * Returns the export formats supported by the implementation of the <code>ChartComposite</code> for exporting the current the view.
	 *
	 * @return
	 */
	public ArrayList<String> getEditorSupportedExportFileFormats() {
		return this.chartComposite.getEditorSupportedExportFileFormats();
	}

	//	/**
	//	 * Gets the parent multipages editor part if exists.
	//	 *
	//	 * @return the parentMultiPagesEditor
	//	 */
	//	public MultiPageEditorPart getParentMultiPagesEditor() {
	//		return parentMultiPagesEditor;
	//	}
	//
	//	/**
	//	 * Sets the specified multipages editor part as parent of this one.
	//	 *
	//	 * @param parentMultiPagesEditor the parentMultiPagesEditor to set
	//	 */
	//	public void setParentMultiPagesEditor(TXMMultiPageEditor parentMultiPagesEditor) {
	//		this.parentMultiPagesEditor = parentMultiPagesEditor;
	//	}

	/**
	 * To check whether this editor was already opened when calling openEditor().
	 *
	 * @return the wasAlreadyOpened
	 */
	public boolean wasAlreadyOpened() {
		return wasAlreadyOpened;
	}

	/**
	 * To check whether this editor was already opened when calling openEditor().
	 *
	 * @param wasAlreadyOpened the wasAlreadyOpened to set
	 */
	public void setWasAlreadyOpened(boolean wasAlreadyOpened) {
		this.wasAlreadyOpened = wasAlreadyOpened;
	}

	/**
	 * Checks whatever the chart composite has the focus or not.
	 *
	 * @return <code>true</code> if the chart composite has the focus otherwise <code>false</code>
	 */
	public boolean hasFocus() {
		return this.chartComposite.hasFocus();
	}

	/**
	 * Gets the chart composite.
	 *
	 * @return the chartComposite
	 */
	public ChartComposite getChartComposite() {
		return chartComposite;
	}

	/**
	 * Gets the chart toolbar.
	 *
	 * @return the chartToolBar
	 */
	public ChartEditorToolBar getChartToolBar() {
		return chartToolBar;
	}

	@Override
	public T getResult() {
		return this.getEditorInput().getResult();
	}

	@Override
	public void close() {
		super.close();

		//		// FIXME: SJ: temporary work around to close a multipage editor from one of this editor page
		//		if (this.parentMultiPagesEditor != null) {
		//			this.getSite().getShell().getDisplay().syncExec(new Runnable() {
		//
		//				@Override
		//				public void run() {
		//
		//					getSite().getPage().closeEditor(parentMultiPagesEditor, false);
		//				}
		//			});
		//		}


	}

}
