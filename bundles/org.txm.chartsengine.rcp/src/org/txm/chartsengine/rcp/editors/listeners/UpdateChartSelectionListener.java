/**
 * 
 */
package org.txm.chartsengine.rcp.editors.listeners;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.txm.chartsengine.rcp.editors.ChartEditor;

/**
 * 
 * A selection listener that can be added to editors widgets and that synchronizes the editor parameters with the chart result parameters then updates the existing chart.
 * This listener is dedicated to update a chart without calling ChartResult.compute() or TXMEditor.compute().  
 * 
 * @author sjacquot
 *
 */
//TODO: SJ: see BaseAbstractComputeListener and mix some features if needed. Also may uses/shares an "autoUpdate/autoCompute" like variable to setChartDirty() and editor.setDirty() instead of instant updating the chart. 
public class UpdateChartSelectionListener implements SelectionListener, ISelectionChangedListener {


	/**
	 * Linked editor.
	 */
	protected ChartEditor<?> editor;


	/**
	 * 
	 * @param editor
	 * @param autoCompute
	 */
	public UpdateChartSelectionListener(ChartEditor<?> editor) {
		this.editor = editor;
	}

	
	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		this.editor.autoUpdateResultFromEditorParameters();
		this.editor.getResult().updateChart();
		//FIXME: SJ, 2024-09-25: useless?
		// => SJ: actually it calls the ChartEditor.onInit() so for example commenting that line break the CA squareOff if we don't call onInit manually
		//this.editor.updateEditorFromResult(false);
		this.editor.onInit();
		// endof FIXME
		
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		// TODO Auto-generated method stub
		
	}

}
