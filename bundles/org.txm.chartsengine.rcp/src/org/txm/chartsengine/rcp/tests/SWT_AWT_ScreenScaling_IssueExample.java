package org.txm.chartsengine.rcp.tests;


import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.TextArea;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;



/**
 * SJ: see: https://github.com/eclipse-platform/eclipse.platform.swt/issues/1020
 * 
 * Issue example of wrong AWT Frame size when using SWT_AWT bridge in Windows with screen scaling.
 * 
 * (-Dsun.java2d.uiScale=2) for VM argument test
 * 
 */
public class SWT_AWT_ScreenScaling_IssueExample {
    public static void main(String[] args) {
        System.setProperty("sun.java2d.uiScale", "2"); //$NON-NLS-1$ //$NON-NLS-2$

        Display display = new Display();
        Shell shell = new Shell(display);
        shell.setLayout(new FillLayout(SWT.VERTICAL));

        Composite embeddedComposite = new Composite(shell, SWT.EMBEDDED | SWT.BORDER_SOLID);

        TextArea textArea = new TextArea();
        textArea.setEditable(false);

        Frame frame = SWT_AWT.new_Frame(embeddedComposite);
        frame.add(textArea, BorderLayout.CENTER);
        frame.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                textArea.append(String.format("Resized to %d x %d\n", frame.getWidth(), frame.getHeight())); //$NON-NLS-1$
            }
        });

        shell.open();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
        display.dispose();
    }
}