package org.txm.chartsengine.rcp.tests;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.txm.chartsengine.core.Theme;
import org.txm.rcp.utils.IOClipboard;

public class ThemeColorsTester {

	static Theme theme = new Theme("0,0,0");
	static ArrayList<ColorLine> lines = new ArrayList<ThemeColorsTester.ColorLine>();

	public static void main (String [] args) {
		Display display = new Display ();
		Shell shell = new Shell(display);
		shell.setLayout(new GridLayout(3, false));
		
		ArrayList<java.awt.Color> colors = theme.getPalette17();
		List<String> names = theme.getColorNamesForPalette(colors);
		
		Button printButtons = new Button(shell, SWT.PUSH);
		printButtons.setText("Print&Copy");
		printButtons.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		printButtons.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				StringBuilder buffer = new StringBuilder();
				for (ColorLine line : lines) {
					buffer.append(line.toString()+"\n");
				}
				System.out.println(buffer);
				IOClipboard.write(buffer.toString());
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});

		Button defaultButtons = new Button(shell, SWT.PUSH);
		defaultButtons.setText("Palette17");
		defaultButtons.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				for (int i = 0 ; i < colors.size(); i++) {
					addColor(shell, theme, names.get(i), colors.get(i), lines.size());
				}
				shell.pack();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});
		Button clearButtons = new Button(shell, SWT.PUSH);
		clearButtons.setText("Clear");
		clearButtons.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				for (ColorLine line : lines) {
					line.dispose();
				}
				shell.pack();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});
		
		GridData gdata = new GridData(GridData.FILL, GridData.FILL, true, true);
		gdata.widthHint = 150;
		Label l = new Label(shell, SWT.NONE);
		l.setText("Color");
		l.setLayoutData(gdata);
		l = new Label(shell, SWT.NONE);
		l.setText("Name");
		l.setLayoutData(gdata);
		l = new Label(shell, SWT.NONE);
		l.setText("R,G,B");
		l.setLayoutData(gdata);
	
		for (int i = 0 ; i < colors.size(); i++) {
			addColor(shell, theme, names.get(i), colors.get(i), lines.size());
		}
		
		
		
		shell.pack();
		shell.open ();
		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) display.sleep ();
		}
		display.dispose ();
	}

	private static void addColor(Composite composite, Theme theme, String name, java.awt.Color color, int position) {
		
		ColorLine line = new ColorLine(composite, name, color);
		line.pack();
		
		lines.add(line);
	}
	
	public static class ColorLine extends Composite {
		
		String name;
		java.awt.Color color;
		private Text titleText;
		private Text helloWorldTest;
		
		public ColorLine(Composite parent, String name, java.awt.Color color) {
			super(parent, SWT.NONE);
			
			this.setLayout(new GridLayout(7, false));
			this.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 3, 1));
			
			this.name = name;
			this.color = color;
			
			GridData gdata = new GridData(GridData.FILL, GridData.FILL, true, true);
			gdata.widthHint = 150;
			
			Text l = new Text(this, SWT.NONE);
			l.setText("                          ");
			l.setBackground(new Color(color.getRed(), color.getGreen(),color.getBlue()));
			l.setLayoutData(gdata);
			
			titleText = new Text(this, SWT.NONE);
			titleText.setLayoutData(new GridData());
			titleText.setText(name);
			titleText.pack();
			titleText.setLayoutData(gdata);
			
			helloWorldTest = new Text(this, SWT.NONE);
			helloWorldTest.setLayoutData(new GridData());
			helloWorldTest.setText(color.getRed()+","+color.getGreen()+","+color.getBlue());
			helloWorldTest.setLayoutData(gdata);
			helloWorldTest.addModifyListener(new ModifyListener() {
				
				@Override
				public void modifyText(ModifyEvent e) {
					try {
						java.awt.Color c2 = theme.stringToColor(helloWorldTest.getText());
						Color c = new Color(c2.getRed(), c2.getGreen(),c2.getBlue());
						l.setBackground(c);
					}
					catch (Exception e2) {
						// TODO: handle exception
					}
				}
			});
			
//			Button upButton = new Button(this, SWT.PUSH);
//			upButton.setText("^");
//			upButton.addSelectionListener(new SelectionListener() {
//				
//				@Override
//				public void widgetSelected(SelectionEvent e) {
//					int i = lines.indexOf(ColorLine.this);
//					if (i > 0) {
//						Composite parent = ColorLine.this.getParent();
//						Control c = ColorLine.this.getParent().getChildren()[6+i-1];
//						ColorLine.this.getParent().getChildren()[6+i-1] = ColorLine.this;
//						ColorLine.this.getParent().getChildren()[6+i] = c;
//						lines.set(i, (ColorLine) c);
//						lines.set(i-1, ColorLine.this);
//						ColorLine.this.layout();
//						ColorLine.this.getParent().layout(true);
//						ColorLine.this.getParent().pack();
//					}
//				}
//				
//				@Override
//				public void widgetDefaultSelected(SelectionEvent e) { }
//			});
//			Button downButton = new Button(this, SWT.PUSH);
//			downButton.setText("v");
			Button addButton = new Button(this, SWT.PUSH);
			addButton.setText("+");
			addButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					addColor(ColorLine.this.getParent(), theme, "NEW", java.awt.Color.BLACK, lines.indexOf(ColorLine.this));
					ColorLine.this.getParent().pack();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) { }
			});
			Button removeButton = new Button(this, SWT.PUSH);
			removeButton.setText("-");
			removeButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					
					lines.remove(ColorLine.this);
					ColorLine.this.dispose();
					parent.pack();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) { }
			});
		
			helloWorldTest.pack();
		}
		
		public String toString() {
			return titleText.getText()+"\t"+helloWorldTest.getText();
		}
	}
}

