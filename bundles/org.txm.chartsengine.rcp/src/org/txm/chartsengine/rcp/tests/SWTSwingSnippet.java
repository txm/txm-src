package org.txm.chartsengine.rcp.tests;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.TextField;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * SJ: Focus bug in Linux minimal sample.
 * 1) if the AWT TextField has gained focus once, when clicking in the SWT Text widget, the key events are not transmitted
 * 2) the AWT Window never loses the focus
 *
 */
public class SWTSwingSnippet {

	public static void main(String[] args) {

		final Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());

		Composite composite = new Composite(shell, SWT.EMBEDDED);

		Frame frame = SWT_AWT.new_Frame(composite);
		Panel panel = new Panel(new BorderLayout());
		frame.add(panel);
		panel.add(new TextField());

		Text text = new Text(shell, SWT.BORDER);

		shell.setSize(200, 70);
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
