package org.txm.chartsengine.rcp;

import java.awt.event.MouseEvent;

import org.txm.chartsengine.rcp.editors.ChartEditor;

/**
 * Chart component interface.
 * A component dedicated to render chart, from a file, from an AWT object, etc. and to be added to an SWT ChartComposite.
 * 
 * @author sjacquot
 *
 */
public interface IChartComponent {



	/**
	 * Zooms the view at x and y coordinates.
	 * 
	 * @param x zoom center X position
	 * @param y zoom center Y position
	 * @param zoomIn true to zoom in ; false to zoom out
	 * @param range true to zoom in range axis
	 * @param domain true to zoom in range axis
	 */
	public abstract void zoom(double x, double y, boolean zoomIn, boolean range, boolean domain);
	
	/**
	 * Resets the view.
	 */
	public abstract void resetView();

	/**
	 * Pans the view.
	 * 
	 * @param srcX
	 * @param srcY
	 * @param dstX
	 * @param dstY
	 * @param panFactor
	 */
	public abstract void pan(double srcX, double srcY, double dstX, double dstY, double panFactor);

	/**
	 * Gets the linked editor.
	 * 
	 * @return
	 */
	public ChartEditor getChartEditor();

	/**
	 * Sets the linked editor.
	 * 
	 * @param editor
	 */
	public void setChartEditor(ChartEditor editor);

	/**
	 * Updates the mouse over item selection according to the specified mouse event.
	 * 
	 * @param event
	 */
	public void updateMouseOverItem(MouseEvent event);

	/**
	 * Checks if the component has the focus.
	 * 
	 * @return
	 */
	public boolean hasFocus();


	/**
	 * Constrains axes ticks to square aspect ratio.
	 */
	public void squareOff();


}
