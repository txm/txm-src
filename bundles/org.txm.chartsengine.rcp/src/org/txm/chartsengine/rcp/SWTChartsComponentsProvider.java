package org.txm.chartsengine.rcp;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.ChartsEnginesManager;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.messages.ChartsEngineUIMessages;
import org.txm.chartsengine.rcp.preferences.___KeyboardBindingProperties;
import org.txm.chartsengine.rcp.preferences.___MouseBindingProperties;
import org.txm.chartsengine.rcp.swt.AdvancedChartEditorToolBar;
import org.txm.chartsengine.rcp.swt.ChartComposite;
import org.txm.chartsengine.rcp.swt.ChartEditorToolBar;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.rcp.editors.TXMMultiPageEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.rcp.swt.widget.preferences.ComboFieldEditor;
import org.txm.utils.logger.Log;

/**
 * A class providing components such EditorPart and EditorInput according to the current charts engine.
 * 
 * @author sjacquot
 * 
 */
public abstract class SWTChartsComponentsProvider {



	// FIXME: should be done in an activator?
	static {
		// Initializes the SWT Charts Components provider according to the current charts engine and its output format
		SWTChartsComponentsProvider.createSWTChartsComponentsProviders();
		SWTChartsComponentsProvider.setCurrrentComponentsProvider(ChartsEngine.getCurrent());
	}


	/**
	 * The charts engine.
	 */
	protected ChartsEngine chartsEngine;


	/**
	 * The name of the provider.
	 */
	protected String name;

	/**
	 * The supported editor input formats.
	 */
	protected List<String> supportedInputFormats;


	/**
	 * Mouse control properties which will be shared by all implementations and chart editors.
	 */
	protected Properties mouseBindingProperties;


	/**
	 * Keyboard control properties which will be shared by all implementations and chart editors.
	 */
	protected Properties keyboardBindingProperties;


	/**
	 * Installed SWT charts components providers.
	 */
	protected static ArrayList<SWTChartsComponentsProvider> chartsComponentsProviders;

	/**
	 * Current SWT charts components provider index in the providers list.
	 */
	protected static int currentChartsComponentsProviderIndex;



	public SWTChartsComponentsProvider(ChartsEngine chartsEngine, Properties mouseBindingProperties, Properties keyboardBindingProperties) {
		this.chartsEngine = chartsEngine;

		this.mouseBindingProperties = mouseBindingProperties;
		this.keyboardBindingProperties = keyboardBindingProperties;

		// FIXME : SJ: see if we need to define the L&F as system for AWT/Swing components
		// If yes, we need to find a way to manage the GTK L&F/Linux bug, see ticket #761
		// System Look and feel
		// try {
		// UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		// }
		// catch(ClassNotFoundException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// catch(InstantiationException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// catch(IllegalAccessException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// catch(UnsupportedLookAndFeelException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

	}

	/**
	 * 
	 * @param chartsEngine
	 */
	public SWTChartsComponentsProvider(ChartsEngine chartsEngine) {
		this(chartsEngine, new ___MouseBindingProperties(), new ___KeyboardBindingProperties());
	}

	/**
	 * 
	 */
	public SWTChartsComponentsProvider() {
		this(null);
	}



	/**
	 * Creates and stores SWT charts components providers from installed extension contributions and defines the current one according to the specified charts engine output format and provider
	 * supported input format.
	 */
	public static void createSWTChartsComponentsProviders() {


		SWTChartsComponentsProvider.chartsComponentsProviders = new ArrayList<>();

		String extensionPointId = "org.txm.rcp.swtchartscomponentsprovider"; //$NON-NLS-1$
		IConfigurationElement[] contributions = Platform.getExtensionRegistry().getConfigurationElementsFor(extensionPointId);

		for (int i = 0; i < contributions.length; i++) {

			// FIXME: SJ: to manage more than one input format, Store them in the class rather than in the extension schema?
			String formatsStr = contributions[i].getAttribute("inputFormat").replace(" ", "").trim(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			String[] formats = formatsStr.split(","); //$NON-NLS-1$

			try {
				SWTChartsComponentsProvider swtChartsComponentsProvider = (SWTChartsComponentsProvider) contributions[i].createExecutableExtension("class");  //$NON-NLS-1$
				swtChartsComponentsProvider.setName(contributions[i].getAttribute("name")); //$NON-NLS-1$
				swtChartsComponentsProvider.setEditorSupportedInputFormat(Arrays.asList(formats));

				// Add the first charts engine that supports the input format
				ChartsEngine chartsEngine = null;
				for (int j = 0; j < formats.length; j++) {
					chartsEngine = ChartsEnginesManager.getInstance().getChartsEngine(formats[j]);
					if (chartsEngine != null) {
						break;
					}
				}
				swtChartsComponentsProvider.setChartsEngine(chartsEngine);

				SWTChartsComponentsProvider.chartsComponentsProviders.add(swtChartsComponentsProvider);

				Log.fine(TXMCoreMessages.bind("Starting SWT charts components provider: {0}...", swtChartsComponentsProvider)); //$NON-NLS-1$


			}
			catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


	/**
	 * Gets the current charts component provider.
	 * 
	 * @return the current charts engine
	 */
	public static SWTChartsComponentsProvider getCurrent() {
		return SWTChartsComponentsProvider.chartsComponentsProviders.get(SWTChartsComponentsProvider.currentChartsComponentsProviderIndex);
	}

	/**
	 * Sets the current charts component provider according to the specified charts engine output format and provider supported input format.
	 * 
	 * @param chartsEngine the charts engine to set as current
	 */
	public static void setCurrrentComponentsProvider(ChartsEngine chartsEngine) {
		for (int i = 0; i < SWTChartsComponentsProvider.chartsComponentsProviders.size(); i++) {
			if (SWTChartsComponentsProvider.chartsComponentsProviders.get(i).canCreateView(chartsEngine)) {
				// Set new charts engine
				SWTChartsComponentsProvider.chartsComponentsProviders.get(i).setChartsEngine(chartsEngine);

				SWTChartsComponentsProvider.currentChartsComponentsProviderIndex = i;
				Log.finest("Setting current SWT charts components provider to: " + SWTChartsComponentsProvider.getCurrent() + "."); //$NON-NLS-1$ //$NON-NLS-2$
				break;
			}
		}
	}


	/**
	 * Gets the first SWT charts components provider that supports the specified charts engine output format as input format.
	 * 
	 * @param chartsEngine
	 * @return the first components provider that manages the format of the specified charts engine
	 */
	public static SWTChartsComponentsProvider getComponentsProvider(ChartsEngine chartsEngine) {

		SWTChartsComponentsProvider chartsComponentsProvider = null;

		for (int i = 0; i < SWTChartsComponentsProvider.chartsComponentsProviders.size(); i++) {
			if (SWTChartsComponentsProvider.chartsComponentsProviders.get(i).canCreateView(chartsEngine)) {

				chartsComponentsProvider = SWTChartsComponentsProvider.chartsComponentsProviders.get(i);
				break;
			}
		}

		return chartsComponentsProvider;
	}


	/**
	 * Gets an installed charts components provider according to its class type.
	 * 
	 * @param type
	 * @return the installed charts components provider according to its class type if it exists otherwise null
	 */
	public static SWTChartsComponentsProvider getComponentsProvider(Class type) {

		SWTChartsComponentsProvider provider = null;

		for (int i = 0; i < SWTChartsComponentsProvider.chartsComponentsProviders.size(); i++) {
			if (SWTChartsComponentsProvider.chartsComponentsProviders.get(i).getClass().equals(type)) {
				provider = SWTChartsComponentsProvider.chartsComponentsProviders.get(i);
				break;
			}
		}

		return provider;
	}

	/**
	 * Checks if the components provider can create a composite to render a chart generated by the specified charts engine using its current output format.
	 * 
	 * @param chartsEngine
	 * @return
	 */
	protected boolean canCreateView(ChartsEngine chartsEngine) {
		return this.getEditorSupportedInputFormats().contains(chartsEngine.getOutputFormat());
	}


	/**
	 * Set the current charts engine.
	 * 
	 * @param chartsEngine the chartsEngine to set
	 */
	public void setChartsEngine(ChartsEngine chartsEngine) {
		this.chartsEngine = chartsEngine;
	}


	/**
	 * Gets the charts engine.
	 * 
	 * @return the chartsEngine
	 */
	public ChartsEngine getChartsEngine() {
		return chartsEngine;
	}


	/**
	 * Creates the chart drawing area composite.
	 * 
	 * @param parent
	 * @return the composite
	 */
	public abstract ChartComposite createComposite(ChartEditor<?> chartEditor, Composite parent);


	/**
	 * Returns the input formats supported by the implementation of the <code>EditorPart</code> for viewing.
	 * 
	 * @return
	 */
	public List<String> getEditorSupportedInputFormats() {
		return this.supportedInputFormats;
	}


	/**
	 * Sets the input formats supported by the implementation of the <code>EditorPart</code> for viewing.
	 * 
	 * @param supportedInputFormats
	 */
	public void setEditorSupportedInputFormat(List<String> supportedInputFormats) {
		this.supportedInputFormats = supportedInputFormats;
	}

	/**
	 * Returns the export formats supported by the implementation of the <code>ChartsEngine</code> for the total chart.
	 * 
	 * @return
	 */
	public abstract ArrayList<String> getChartsEngineSupportedExportFileFormats();


	/**
	 * Creates a SWT group for the charts engine default preferences area in preference pages.
	 * 
	 * @return
	 */
	public static Group createChartsRenderingPreferencesGroup(Composite parent) {

		Group chartsGroup = new Group(parent, SWT.NONE);

		chartsGroup.setText(ChartsEngineUIMessages.chartsRendering);

		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData.horizontalSpan = 2;
		gridData.verticalIndent = 10;
		chartsGroup.setLayoutData(gridData);

		return chartsGroup;
	}


	/**
	 * 
	 * @param parent
	 * @return
	 */
	public static Composite createChartsRenderingPreferencesTabFolderComposite(Composite parent) {
		return createChartsRenderingPreferencesTabFolderComposite(parent, ChartsEngineUIMessages.chartsRendering);
	}

	/**
	 * Creates a tab folder with a tab item for rendering preferences.
	 * 
	 * @param parent
	 * @return
	 */
	public static Composite createChartsRenderingPreferencesTabFolderComposite(Composite parent, String tabTitle) {


		TabFolder tabFolder = new TabFolder(parent, SWT.TOP);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.horizontalSpan = 2;
		tabFolder.setLayoutData(gridData);

		TabItem tabItem = new TabItem(tabFolder, SWT.NULL);
		tabItem.setText(tabTitle);
		Composite composite = new Composite(tabFolder, SWT.NULL);
		tabItem.setControl(composite);
		composite.setLayout(new GridLayout());

		return composite;
	}

	/**
	 * 
	 * @param page
	 * @param composite
	 */
	public static void createChartsRenderingPreferencesFields(TXMPreferencePage page, Composite composite) {

		Label separator = new Label(composite, SWT.HORIZONTAL | SWT.SEPARATOR);
		//GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1);
		gridData.horizontalSpan = 2;
		separator.setLayoutData(gridData);

		String commandNodeQualifier = ((TXMPreferenceStore) page.getPreferenceStore()).getNodeQualifier();

		// Create a group for columns management of mixed boolean fields and field/text input fields
		Group displayGroup = new Group(composite, SWT.NONE);
		displayGroup.setText(TXMUIMessages.display);
		GridData booleansGroupGridData = new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1);
		booleansGroupGridData.horizontalSpan = 2;
		displayGroup.setLayoutData(booleansGroupGridData);

		// Show title
		if (!TXMPreferences.isEmpty(commandNodeQualifier, ChartsEnginePreferences.SHOW_TITLE)) {
			page.addField(new BooleanFieldEditor(ChartsEnginePreferences.SHOW_TITLE, ChartsEngineUIMessages.title, displayGroup));
		}
		// Show legend
		if (!TXMPreferences.isEmpty(commandNodeQualifier, ChartsEnginePreferences.SHOW_LEGEND)) {
			page.addField(new BooleanFieldEditor(ChartsEnginePreferences.SHOW_LEGEND, ChartsEngineUIMessages.legend, displayGroup));
		}
		// Show grid
		if (!TXMPreferences.isEmpty(commandNodeQualifier, ChartsEnginePreferences.SHOW_GRID)) {
			page.addField(new BooleanFieldEditor(ChartsEnginePreferences.SHOW_GRID, ChartsEngineUIMessages.grid, displayGroup));
		}

		// Rendering colors mode selection
		if (!TXMPreferences.isEmpty(commandNodeQualifier, ChartsEnginePreferences.RENDERING_COLORS_MODE)) {
			String colorsModes[][] = new String[4][2];
			colorsModes[0][0] = ChartsEngineUIMessages.colors;
			colorsModes[0][1] = "0"; //$NON-NLS-1$
			colorsModes[1][0] = ChartsEngineUIMessages.grayscale;
			colorsModes[1][1] = "1"; //$NON-NLS-1$
			colorsModes[2][0] = ChartsEngineUIMessages.blackAndWhite;
			colorsModes[2][1] = "2"; //$NON-NLS-1$
			colorsModes[3][0] = ChartsEngineUIMessages.monochrome;
			colorsModes[3][1] = "3"; //$NON-NLS-1$
			ComboFieldEditor renderingModeComboField = new ComboFieldEditor(ChartsEnginePreferences.RENDERING_COLORS_MODE, ChartsEngineUIMessages.colorization, colorsModes, composite);
			renderingModeComboField.setToolTipText(ChartsEngineUIMessages.renderingColorsMode);
			page.addField(renderingModeComboField);
		}

		// Same lines stroke
		if (!TXMPreferences.isEmpty(commandNodeQualifier, ChartsEnginePreferences.MULTIPLE_LINE_STROKES)) {
			BooleanFieldEditor bf = new BooleanFieldEditor(ChartsEnginePreferences.MULTIPLE_LINE_STROKES, ChartsEngineUIMessages.patterns, composite);
			bf.setToolTipText(ChartsEngineUIMessages.multipleLineStrokeStyles);
			page.addField(bf);
		}
		
		try {
			((GridLayout) composite.getLayout()).marginWidth = 3;
			((GridLayout) composite.getLayout()).numColumns = 2;
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * Gets the current active chart editor managing the multi pages editor.
	 * 
	 * @param event
	 * @return
	 */
	public static ChartEditor<? extends ChartResult> getActiveChartEditor(ExecutionEvent event) {
		ChartEditor<? extends ChartResult> chartEditor = null;

		Widget widget = null;
		if (event != null) {
			Event eevent = (Event) event.getTrigger();
			if (eevent !=  null) {
				widget = eevent.widget;
			}
		}


		// Toolbars
		if (event != null && widget != null && widget instanceof ToolItem) {
			ToolItem to = (ToolItem) widget;
			ToolBar parent = to.getParent();
			if (to.getParent() instanceof ChartEditorToolBar) {
				chartEditor = ((ChartEditorToolBar) ((ToolItem) ((Event) event.getTrigger()).widget).getParent()).getEditorPart();
			}
			if (to.getParent() instanceof AdvancedChartEditorToolBar) {
				chartEditor = ((AdvancedChartEditorToolBar) ((ToolItem) ((Event) event.getTrigger()).widget).getParent()).getEditorPart();
			}
		}
		else {
			IWorkbenchPart part = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActivePart();

			// Context menu, multi pages editor
			if (part instanceof TXMMultiPageEditor) {
				TXMMultiPageEditor editor = (TXMMultiPageEditor) part;
				// Main editor
				if (editor.getMainEditorPart() instanceof ChartEditor
						&& ((ChartEditor<?>) editor.getMainEditorPart()).hasFocus()) {
					chartEditor = (ChartEditor<?>) editor.getMainEditorPart();
				}
				// FIXME: SJ: became useless?
				// else if(((MultiPageEditorPart) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor()).getSelectedPage() instanceof ChartEditor
				// && ((ChartEditor)((MultiPageEditorPart) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor()).getSelectedPage()).hasFocus() ) {
				// chartEditor = (ChartEditor)((MultiPageEditorPart) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor()).getSelectedPage();
				// }
			}
			// Context menu, Normal editor
			else if (part instanceof ChartEditor) {
				chartEditor = (ChartEditor<?>) part;
			}
		}
		return chartEditor;
	}



	@Override
	public String toString() {
		return String.format("%s (supported input formats: %s, Charts Engine: %s)", this.name, this.supportedInputFormats.toString().replaceAll("\\[|\\]", ""), this.chartsEngine); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}


	/**
	 * Sets the name of the provider.
	 * 
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}



}
