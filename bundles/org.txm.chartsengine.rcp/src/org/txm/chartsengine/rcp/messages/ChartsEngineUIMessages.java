package org.txm.chartsengine.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

public class ChartsEngineUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.chartsengine.rcp.messages.messages"; //$NON-NLS-1$

	public static String blackAndWhite;
	public static String canceledByUser;
	public static String cannotExportWithTheP0FormatPleaseUserOneOFP1;
	public static String canNotOpenChartPropertiesInterfaceForThecurrentChartEngine;
	public static String chartsRendering;
	public static String chartViewCopiedToClipboard;
	public static String colors;
	public static String currentEngine;
	public static String defaultExportFormat;
	public static String displayFormat;
	public static String displayOrPrintWidthInCentimeters;
	public static String ErrorP0IsNotAFile;
	public static String errorTheSelectedExportResolutionParametersLeadsToATooLargeImage;
	public static String ErrorWhileMmovingTotherightInP0P1;
	public static String ErrorWhileMovingDownInP0P1;
	public static String ErrorWhileMovingToTheLeftInP0P1;
	public static String ErrorWhileMovingUpInP0P1;
	public static String ErrorWhileResettingViewInP0P1;
	public static String ErrorWhileZoomingInP0P1;
	public static String ErrorWhileZoomingOutInP0P1;
	public static String ExportingChart;
	public static String ExportingCurrentChartViewToFileP0;
	public static String grayscale;
	public static String monochrome;
	public static String monochromeRenderingColor;
	public static String multipleLineStrokeStyles;
	public static String navigation;
	public static String navigationToolsToZoomPanEtcInTheView;
	
	public static String preferences_label_exportedChartsProperties;
	public static String preferences_label_exportedWidthPixels;
	public static String preferences_label_resolutionDPI;
	public static String preferences_label_widthCm;
	public static String preferences_tooltip_exportedImageWidthInPixels;
	public static String preferences_tooltip_outputResolutionInDPI;
	
	public static String recreatingChartsEngineAndSWTChartComponentsProvider;
	public static String rendering;
	public static String renderingColorsMode;
	public static String colorization;
	public static String patterns;
	public static String selectAchartTypeToRenderTheResult;
	public static String selectThefontFamilly;
	public static String selectTheFontSize;
	public static String selectThefontStyle;
	public static String selectTheSystemFontToUse;
	public static String showGrid;
	public static String showhideGrid;
	public static String showhideLegend;
	public static String showHideRenderingParameters;
	public static String showhideTitle;
	public static String showLegend;
	public static String showTitle;
	public static String legend;
	public static String title;
	public static String grid;
	public static String thesePreferencesWillApplyToNewChartsPreviouslyCreatedChartsWillNotBeAffected;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, ChartsEngineUIMessages.class);
	}

	private ChartsEngineUIMessages() {
	}
}
