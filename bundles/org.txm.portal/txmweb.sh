NO_ARGS=0 
E_OPTERROR=85
# txmweb.sh upgrade
#   Undeploy, deploy and copy HTML sources files in the webapps
# or 
# txmweb.sh install
#   create the portal configuration directory
# parameters

# PORTAL WEBAPP LOCATION
TOMCAT="/var/lib/tomcat6/webapps"
# PORTAL HTML (mostly) SOURCE FILES
SRCFILES="/var/lib/tomcat6/txm/data/projets/"
# PORTAL APPLICATION NAME
APPNAME="bbb"
# PORTAL AND ITS CORPUS CONFIGURATION FILES
TXMWEBHOME="/usr/share/tomcat6/txm/TXMWEB"
# LOCATION TO THE WAR FILE TO DEPLOY
WARPATH="/var/lib/tomcat6/txm/data/projets/bbb.war"
# TOMCAT USER
USER="txm"
# PORTAL ADDRESS
URL="zzz"

read -sp "Enter Tomcat Password: " PASSWORD

if [ $# -eq "$NO_ARGS" ]    # Script invoked with no command-line args?
then
  echo "Usage: sh txmweb.sh install|upgrade [-user xxx] [-url zzz] [-war aaa] -app bbb -repo ccc -tomcat ddd -txmhome eee"
  echo "Or sh txmweb.sh install|upgrade -u xxx -p yyy -l zzz -w aaa -a bbb -e ccc -t ddd -x eee"
  exit $E_OPTERROR
fi  

WHAT=$1
shift
echo "do $WHAT"

if ! options=$(getopt -o uplwaetx -l user,password,url,war,appname,repo,tomcat,txmhome -- "$@")
then
    # something went wrong, getopt will put out an error message for us
    exit 1
fi

while [ $# -gt 0 ]
do
  case "$1" in
    -u|--user     ) USER="$2";;
    -l|--url      ) URL="$2";;
    -w|--war      ) WARPATH="$2";;
    -a|--appname  ) APPNAME="$2";;
    -e|--repo     ) SRCFILES="$2";;
    -t|--tomcat   ) TOMCAT="$2";;
    -x|--txmhome  ) TXMWEBHOME="$2";;
    (--) shift; break;;
    (-*) echo "$0: error - unrecognized option $1" 1>&2; exit 1;;
    (*)  break;;
  esac
  shift
  shift
done

echo "user: $USER"
echo "url: $URL"
echo "manager url: $URL"
echo "tomcat: $TOMCAT"
echo "srcfiles: $SRCFILES"
echo "txmweb home: $TXMWEBHOME"
echo "war: $WARPATH"
echo "appname: $APPNAME"

if [ $WHAT = "upgrade" ]
then
	sh upgrade.sh "$USER" "$PASSWORD" "$URL" "$TOMCAT" "$SRCFILES" "$TXMWEBHOME" "$WARPATH" "$APPNAME"
else
	sh install.sh $USER "$USER" "$PASSWORD" "$URL" "$TOMCAT" "$SRCFILES" "$TXMWEBHOME" "$WARPATH" "$APPNAME"
fi;
