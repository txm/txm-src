#!/bin/bash

echo "** TXMWEB install: start."

# auth params
USER="$1"
PASSWORD="$2"
URL="$3"
# parameters
TOMCAT="$4"
SRCFILES="$5"
TXMWEBHOME="$6"
# application
WARPATH="$7"
APPNAME="$8"

MANAGERURL="http://$USER:$PASSWORD@$URL/manager/html/"

# check params
# Test if $WARPATH exists
if [ ! -f "$WARPATH" ]; then
	echo "** TXMWEB install: war zip is missing: $WARPATH. Can't continue the installation."
	exit 1;
fi

# Test $SRCFILES content
if [ ! -d "$SRCFILES/$APPNAME" ]; then
	echo "** TXMWEB install: $SRCFILES/$APPNAME is missing"
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/biblio" ]; then
	echo "** TXMWEB install: $SRCFILES/$APPNAME/biblio is missing. This folder contains the bibliographic records of texts"
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/css" ]; then
	echo "** TXMWEB install: $SRCFILES/$APPNAME/css is missing. This folder contains the css files"
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/html" ]; then
	echo "** TXMWEB install: $SRCFILES/$APPNAME/html is missing. This folder contains the HTML files"
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/images" ]; then
	echo "** TXMWEB install: $SRCFILES/$APPNAME/images is missing. This folder contains the png, jpg, ... files"
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/jsp" ]; then
	echo "** TXMWEB install: $SRCFILES/$APPNAME/jsp is missing. This folder contains the jsp files. Such as 404.jsp"
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/data" ]; then
	echo "** TXMWEB install: $SRCFILES/$APPNAME/data is missing. This folder contains the data to launch TXMWEB itself."
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/data/users" ]; then
	echo "** TXMWEB install: $SRCFILES/$APPNAME/data/users is missing. This folder contains the serialized users as XML files."
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/data/profiles" ]; then
	echo "** TXMWEB install: $SRCFILES/$APPNAME/data/profiles is missing. This folder contains the serialized profiles as XML files."
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/data/mails" ]; then
	echo "** TXMWEB install: $SRCFILES/$APPNAME/data/mails is missing. This folder contains the mails templates."
	exit 1;
fi

# test conf files
if [ ! -f "$SRCFILES/$APPNAME/txmweb.conf" ]; then
	echo "** TXMWEB install: $SRCFILES/$APPNAME/txmweb.conf file is missing. It describes the parameters of TXMWEB"
	exit 1;
fi

if [ ! -f "$SRCFILES/$APPNAME/textometrie.properties" ]; then
	echo "** TXMWEB install: $SRCFILES/$APPNAME/textometrie.properties file is missing. It describes the parameters of the Toolbox"
	exit 1;
fi

# creating dirs
# Try to create $TXMWEBHOME
mkdir "$TXMWEBHOME"
if [ ! -d "$TXMWEBHOME" ]; then
	echo "** TXMWEB install: src txmwebhome counld not been created $TXMWEBHOME. Can't continue the installation."
	exit 1;
fi

# Try to create $TXMWEBHOME APP DIR
mkdir "$TXMWEBHOME/$APPNAME"
if [ ! -d "$TXMWEBHOME/$APPNAME" ]; then
	echo "** TXMWEB install: src txmwebhome counld not been created $TXMWEBHOME/$APPNAME. Can't continue the installation."
	exit 1;
fi

# copy REPO files in home of $APPNAME
cp -rf "$SRCFILES/$APPNAME" "$TXMWEBHOME"

if [ $? != 0 ]; then
	echo "** TXMWEB install: Failed to copy files from repository $TXMWEBHOME/$APPNAME to TXMWEBHOME $TXMWEBHOME. Aborting."
	exit 1;
fi

# the end
echo "** TXMWEB install: done."
