#!/bin/bash

echo "** TXMWEB upgrade: start."

code=ERFIH3EZ2R5943U50A435JZ5R0ZEITERT
# auth params
USER="$1"
PASSWORD="$2"
URL="$3"
# parameters
TOMCAT="$4"
SRCFILES="$5"
TXMWEBHOME="$6"
# application
WARPATH="$7"
APPNAME="$8"

MANAGERURL="http://$USER:$PASSWORD@$URL/manager/html"
echo "** TXMWEB upgrade: checking directories..."
# check params
# Test if $TOMCAT exists
if [ ! -d "$TOMCAT" ]; then
	echo "** TXMWEB upgrade: $TOMCAT is missing. Can't continue the installation."
	exit 1;
fi

# Test if $WARPATH exists
if [ ! -f "$WARPATH" ]; then
	echo "** TXMWEB upgrade: war zip is missing: $WARPATH. Can't continue the installation."
	exit 1;
fi

# Test $SRCFILES content
if [ ! -d "$SRCFILES/$APPNAME/biblio" ]; then
	echo "** TXMWEB upgrade: $SRCFILES/$APPNAME/biblio is missing. This folder contains the bibliographic records of texts"
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/css" ]; then
	echo "** TXMWEB upgrade: $SRCFILES/$APPNAME/css is missing. This folder contains the css files"
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/html" ]; then
	echo "** TXMWEB upgrade: $SRCFILES/$APPNAME/html is missing. This folder contains the HTML files"
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/images" ]; then
	echo "** TXMWEB upgrade: $SRCFILES/$APPNAME/images is missing. This folder contains the png, jpg, ... files"
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/jsp" ]; then
	echo "** TXMWEB upgrade: $SRCFILES/$APPNAME/jsp is missing. This folder contains the jsp files. Such as 404.jsp"
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/data/mails" ]; then
	echo "** TXMWEB upgrade: $SRCFILES/$APPNAME/data/mails is missing. This folder contains the mails templates."
	exit 1;
fi

# set maintenance state
echo "** TXMWEB upgrade: set maintenance state..."
wget "$URL/$APPNAME/maintenance/?code=$code&state=true&message=The server is going to shutdown in 1 minute" -O _ -q
var=$(cat _)
echo $var
if [ "$var" = "false" ]; then
	echo "** TXMWEB upgrade: the server is not in maintenance state. Aborting"
	exit 1;
fi
echo "** TXMWEB upgrade: waiting 60s to let users see the message..."
#sleep 60

# undeploy and deploy $APPNAME
echo "$MANAGERURL/undeploy?path=/$APPNAME"
wget "$MANAGERURL/undeploy?path=/$APPNAME" -O – -q
if [ $? != 0 ]; then
	echo "** TXMWEB upgrade: Failed to undeploy $APPNAME with cmd '$MANAGERURL/undeploy?path=/$APPNAME'. Continuing."
fi
echo "$MANAGERURL/deploy?deployPath=/$APPNAME&deployConfig=&deployWar=file:$WARPATH"
wget "$MANAGERURL/deploy?deployPath=/$APPNAME&deployConfig=&deployWar=file:$WARPATH" -O – -q
if [ $? != 0 ]; then
	echo "** TXMWEB upgrade: Failed to deploy $APPNAME with cmd '$MANAGERURL/deploy?deployPath=/$APPNAME&deployConfig=&deployWar=file:$WARPATH'. Aborting."
	exit 1;
fi

# test if ant list contains $APPNAME
echo "** TXMWEB upgrade: check if the webapp is deployed..."
wget "$MANAGERURL/list" -O testwar -q
grep -q \/$APPNAME testwar
if [ $? != 0 ]; then
	echo "** TXMWEB upgrade: $APPNAME is not ready."
	exit 1;
fi
rm -rf testwar

sudo chmod -R g+w "$TOMCAT/$APPNAME"

mkdir "$SRCFILES/$APPNAME/biblio"
mkdir "$SRCFILES/$APPNAME/html"
mkdir "$SRCFILES/$APPNAME/images"
mkdir "$SRCFILES/$APPNAME/css"
mkdir "$SRCFILES/$APPNAME/jsp"
mkdir "$SRCFILES/$APPNAME/pdf"
mkdir "$SRCFILES/$APPNAME/catalog"
mkdir "$SRCFILES/$APPNAME/files"
mkdir "$SRCFILES/$APPNAME/js"

# copy biblio, html, images, css and jsp dir in dev webapps
echo "** TXMWEB upgrade: copying biblio, html, images, css and jsp directories in webapps dir..."
cp -rf "$SRCFILES/$APPNAME/biblio" "$TOMCAT/$APPNAME" &&
cp -rf "$SRCFILES/$APPNAME/html" "$TOMCAT/$APPNAME" &&
cp -f "$SRCFILES/$APPNAME/TXMWEB.jsp" "$TOMCAT/$APPNAME" &&
cp -f "$SRCFILES/$APPNAME/favicon.ico" "$TOMCAT/$APPNAME" &&
cp -f "$SRCFILES/$APPNAME/*.css" "$TOMCAT/$APPNAME" &&
cp -rf "$SRCFILES/$APPNAME/images" "$TOMCAT/$APPNAME" &&
cp -rf "$SRCFILES/$APPNAME/css" "$TOMCAT/$APPNAME" &&
cp -rf "$SRCFILES/$APPNAME/jsp" "$TOMCAT/$APPNAME" &&
cp -rf "$SRCFILES/$APPNAME/pdf" "$TOMCAT/$APPNAME" &&
cp -rf "$SRCFILES/$APPNAME/catalog" "$TOMCAT/$APPNAME" &&
cp -rf "$SRCFILES/$APPNAME/files" "$TOMCAT/$APPNAME" &&
cp -rf "$SRCFILES/$APPNAME/js" "$TOMCAT/$APPNAME"

if [ $? != 0 ]; then
	echo "** TXMWEB upgrade: Failed to copy biblio, html, images, css and jsp dir in webapp $WEBAPPS/$APPNAME."
	exit 1;
fi

# be sure that files are usable by tomcat6
sudo chown -R tomcat6:tomcat6 "$TOMCAT/$APPNAME"

# the end
echo "** TXMWEB deploy: done."
