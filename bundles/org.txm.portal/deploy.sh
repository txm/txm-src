#!/bin/bash

echo "** TXMWEB deploy: start."

# auth params
USER="$1"
PASSWORD="$2"
URL="$3"
# parameters
TOMCAT="$4"
SRCFILES="$5"
TXMWEBHOME="$6"
# application
WARPATH="$7"
APPNAME="$8"

MANAGERURL="http://$USER:$PASSWORD@$URL/manager/html"

# check params
# Test if $TOMCAT exists
if [ ! -d "$TOMCAT" ]; then
	echo "** TXMWEB deploy: $TOMCAT is missing. Can't continue the installation."
	exit 1;
fi

# Test if $WARPATH exists
if [ ! -f "$WARPATH" ]; then
	echo "** TXMWEB deploy: war zip is missing: $WARPATH. Can't continue the installation."
	exit 1;
fi

# Test $SRCFILES content
if [ ! -d "$SRCFILES/$APPNAME/biblio" ]; then
	echo "** TXMWEB deploy: $SRCFILES/$APPNAME/biblio is missing. This folder contains the bibliographic records of texts"
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/css" ]; then
	echo "** TXMWEB deploy: $SRCFILES/$APPNAME/css is missing. This folder contains the css files"
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/html" ]; then
	echo "** TXMWEB deploy: $SRCFILES/$APPNAME/html is missing. This folder contains the HTML files"
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/images" ]; then
	echo "** TXMWEB deploy: $SRCFILES/$APPNAME/images is missing. This folder contains the png, jpg, ... files"
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/jsp" ]; then
	echo "** TXMWEB deploy: $SRCFILES/$APPNAME/jsp is missing. This folder contains the jsp files. Such as 404.jsp"
	exit 1;
fi

if [ ! -d "$SRCFILES/$APPNAME/data/mails" ]; then
	echo "** TXMWEB deploy: $SRCFILES/$APPNAME/data/mails is missing. This folder contains the mails templates."
	exit 1;
fi

# undeploy and deploy $APPNAME
wget "$MANAGERURL/undeploy?path=/$APPNAME" -O – -q
if [ $? != 0 ]; then
	echo "** TXMWEB deploy: Failed to undeploy $APPNAME with cmd '$MANAGERURL/undeploy?path=/$APPNAME'. Continuing."
fi
wget "$MANAGERURL/deploy?deployPath=/$APPNAME&deployConfig=&deployWar=file:$WARPATH" -O – -q
echo "$MANAGERURL/deploy?deployPath=/$APPNAME&deployConfig=&deployWar=file:$WARPATH"
if [ $? != 0 ]; then
	echo "** TXMWEB deploy: Failed to deploy $APPNAME with cmd '$MANAGERURL/deploy?deployPath=/$APPNAME&deployConfig=&deployWar=file:$WARPATH'. Aborting."
	exit 1;
fi

# test if ant list contains $APPNAME
wget "$MANAGERURL/list" -O testwar -q
grep -q \/$APPNAME testwar
if [ $? != 0 ]; then
	echo "** TXMWEB deploy: $APPNAME is not ready."
	exit 1;
fi
rm -rf testwar

# copy biblio, html, images, css and jsp dir in dev webapps
cp -rf "$SRCFILES/$APPNAME/biblio" "$TOMCAT/$APPNAME" &&
cp -rf "$SRCFILES/$APPNAME/html" "$TOMCAT/$APPNAME" &&
cp -rf "$SRCFILES/$APPNAME/images" "$TOMCAT/$APPNAME" &&
cp -rf "$SRCFILES/$APPNAME/css" "$TOMCAT/$APPNAME" &&
cp -rf "$SRCFILES/$APPNAME/jsp" "$TOMCAT/$APPNAME"
if [ $? != 0 ]; then
	echo "** TXMWEB deploy: Failed to copy biblio, html, images, css and jsp dir in webapp $WEBAPPS/$APPNAME."
	exit 1;
fi

# the end
echo "** TXMWEB dev deploy: done."
