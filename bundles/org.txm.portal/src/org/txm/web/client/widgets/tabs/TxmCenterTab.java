package org.txm.web.client.widgets.tabs;

import java.util.Map;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.widgets.CenterPanel;
import org.txm.web.client.widgets.forms.TxmForm;
import org.txm.web.client.widgets.menus.TabContextMenu;

import com.smartgwt.client.widgets.tab.Tab;

/**
 * It's a smartGWT Tab overrided to set some default options on the tab.
 * 
 * @version 0.2.0
 * @author vchabani
 */
public abstract class TxmCenterTab extends Tab {
	
	public TxmForm getForm() {
		return null;
	}
	
	public void setParameters(Map<String, String> parameters) {
		if (getForm() != null) getForm().setParameters(parameters);
	}

	public TxmCenterTab() {
		super();
	}

	public TxmCenterTab(String title) {
		super(title);
	}

	public TxmCenterTab(String title, String img) {
		super(title, img);
	}

	/**
	 * Displays the tab into the center panel.
	 */
	public void addInView() {
		addInView(null, null, null);
	}

	/**
	 * Displays the tab into the center panel and the navigation tree.
	 * @param nodeName The name of the item into the navigation tree.
	 * @param nodePath The location of the item in the navigation tree (/BASE/CORPUS/).
	 */
	public void addInView(String nodeName, String ns,  String nodePath) {
		this.setCanClose(true);
		this.setContextMenu(new TabContextMenu());

		CenterPanel center = TXMWEB.getCenterPanel();
		
		if (TXMWEB.EXPO) {
			while (center.getNumTabs() > 0)
				center.removeTab(0);
		}
		center.addTab(this);
		center.selectTab(center.getTabs().length - 1);
		
		//To show tabs in the side nav tree uncomment the next code
		/*if (nodeName != null && nodePath != null) {
			Tree tree = TXM_RIA.getSideNavTree().getTree();
			TreeNode newChild = new TreeNode(nodeName);
			newChild.setAttribute("ns", ns);
			newChild.setAttribute("id", nodeName);
			newChild.setAttribute("tabInstance", this);
			tree.add(newChild, nodePath);
			tree.openFolder(tree.find(nodePath));
			TXM_RIA.getSideNavTree().refreshFields();

		}*/
	}

	public abstract void onClose();
}
