package org.txm.web.client.widgets.forms;

import java.util.Map;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.TxmHTMLPane;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.shared.UserData;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ScrolledEvent;
import com.smartgwt.client.widgets.events.ScrolledHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.layout.HLayout;

public class AccessForm extends TxmWindow {
	private String name = "";
	private String firstname = "";
	private String quality = "";
	private String address = "";
	private String mail = "";
	private String tel = "";
	private String access = "";

	
	TextItem nameField = new TextItem("Name");
	TextItem firstNameField = new TextItem("FirstName");
	TextItem qualityField = new TextItem("Quality");
	TextItem addressField = new TextItem("Address");
	TextItem mailField = new TextItem("Mail");
	TextItem telField = new TextItem("Phone");

	CheckboxItem acceptItem = new CheckboxItem();  

	IButton validateButton = new IButton(Services.getI18n().sendButton());
	IButton cancelButton = new IButton(Services.getI18n().cancelButton());

	public AccessForm() {
		super(true, "");
		
		initializeFields(null);
		//setParameters(parameters); // do it here because no Tab will do it
	}
	
	@Override
	public void _initializeFields(Map<String, String> parameters) {
		ProcessingWindow.show(Services.getI18n().openingAccessForm());
		this.setTitle(Services.getI18n().accessFormtitle(TXMWEB.PORTALLONGNAME));
		this.setModalMaskOpacity(20);
		this.setShowModalMask(true);

		validateButton.setPrompt(Services.getI18n().confirmTheAccessDemand());
		cancelButton.setPrompt(Services.getI18n().cancelAndcloseWindow());

		form = new DynamicForm();
		form.setPadding(10);
		form.setWidth("100%");
		form.setHeight("100%");
		form.setColWidths("20%","80%");

		acceptItem.setName("AcceptTerms");  
		acceptItem.setRequired(true);  

		nameField.setTitle(Services.getI18n().name());
		firstNameField.setTitle(Services.getI18n().firstName());
		qualityField.setTitle(Services.getI18n().quality());
		addressField.setTitle(Services.getI18n().institutionalAddress());
		mailField.setTitle(Services.getI18n().email());
		telField.setTitle(Services.getI18n().phone());
		acceptItem.setTitle(Services.getI18n().accessFormCertificate(TXMWEB.PORTALLONGNAME));

		addValidateHandlers();

		HLayout buttons = new HLayout(5);
		buttons.setMargin(7);
		buttons.setAutoHeight();
		buttons.setWidth100();
		buttons.setMembers(validateButton, cancelButton);
		form.setItems(nameField, firstNameField, qualityField, addressField, mailField, telField, acceptItem);

		final TxmHTMLPane htmlPane = new TxmHTMLPane();  
		//htmlPane.setShowEdges(true);
		htmlPane.setWidth(700);
		htmlPane.setContentsURL("CSU.html");  
		htmlPane.setContentsType(ContentsType.PAGE);  

		htmlPane.addScrolledHandler(new ScrolledHandler() {

			@Override
			public void onScrolled(ScrolledEvent event) {
				if (event.getY() >= 200)
					validateButton.enable();
			}
		});
		validateButton.disable();
		this.addItem(form);
		this.addItem(buttons);
		//RootPanel.get().add(this);
		this.centerInPage();
		this.show();

		nameField.focusInItem();
		ProcessingWindow.hide();
	}

	private void addValidateHandlers() {
		validateButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				validate();
			}
		});
		cancelButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				destroy();
			}
		});
		mailField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event != null && event.getKeyName() != null) {
					if (event.getKeyName().equals("Enter")) {
						validate();
					}
				}
			}
		});
		nameField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event != null && event.getKeyName() != null) {
					if (event.getKeyName().equals("Enter")) {
						validate();
					}
				}
			}
		});
		firstNameField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event != null && event.getKeyName() != null) {
					if (event.getKeyName().equals("Enter")) {
						validate();
					}
				}
			}
		});
		acceptItem.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event != null && event.getKeyName() != null) {
					if (event.getKeyName().equals("Enter")) {
						validate();
					}
				}
			}
		});
	}

	protected void validate() {
		name = form.getValueAsString("Name");
		firstname = form.getValueAsString("FirstName");
		quality = form.getValueAsString("Quality");
		address = form.getValueAsString("Address");
		mail = form.getValueAsString("Mail");
		tel = form.getValueAsString("Phone");
		access = form.getValueAsString("AcceptTerms");

		if (access == null) {
			TxmDialog.warning(Services.getI18n().accessFormWarning1());
			return;
		}

		if (name == null || firstname == null || quality == null || 
				address == null || mail == null || tel == null) {
			TxmDialog.warning(Services.getI18n().accessFormWarning2());
			return;
		}

		name = name.trim();
		firstname = firstname.trim();
		quality = quality.trim();
		address = address.trim();
		mail = mail.trim();
		tel = tel.trim();

		if (name.equals("") || firstname.equals("") || quality.equals("") || 
				address.equals("") || mail.equals("") || tel.equals("")) {
			TxmDialog.warning(Services.getI18n().accessFormWarning2());
			return;
		}

		if (!tel.matches(".*[0-8].*")) {
			TxmDialog.warning(Services.getI18n().nonValidPhone());
			return;
		}

		if (!UserData.checkMail(mail)) {
			TxmDialog.warning(Services.getI18n().nonValidMail());
			return;
		}

		boolean accessb = access.equals("true");
		ProcessingWindow.show(Services.getI18n().registering_account());
		Services.getAuthenticationService().askAccess(null, name, firstname, quality, address, mail, tel, accessb, new AsyncCallback<Boolean>() {
			public void onSuccess(Boolean result) {
				ProcessingWindow.hide();
				if (!result) {
					TxmDialog.warning(Services.getI18n().failed_to_send_confirmation());
				} else {
					TxmDialog.info(Services.getI18n().accessFormSent());
					TXMWEB.getConsole().addMessage(Services.getI18n().accessFormSent());
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				TxmDialog.warning(Services.getI18n().accessError());
			}
		});
	}
}
