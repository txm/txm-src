package org.txm.web.client.widgets.wizards;

import java.util.ArrayList;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.widgets.CenterPanel;
import org.txm.web.client.widgets.tabs.TxmCenterTab;

import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class TxmWizard extends TxmCenterTab{
	ArrayList<WizardPage> pages = new ArrayList<WizardPage>();
	IButton nextBtn, previousBtn, cancelBtn, doneBtn;
	VLayout pagesPanel;
	int currentPage;
	
	public void init()
	{
		VLayout mainPanel = new VLayout();
		mainPanel.setSize("100%", "100%");

		pagesPanel = new VLayout();

		ToolStrip btns = new ToolStrip();
		btns.setWidth100();
		btns.setHeight(25);

		cancelBtn = new IButton("Cancel");
		cancelBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				cancel();
			}
		});
		nextBtn = new IButton("Next");
		nextBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				next();
			}
		});
		previousBtn = new IButton("Previous");
		previousBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				previous();
			}
		});
		doneBtn = new IButton("Done");
		doneBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				done();
			}
		});
		btns.addFill();
		btns.addMember(cancelBtn);
		btns.addMember(previousBtn);
		btns.addMember(nextBtn);
		btns.addMember(doneBtn);

		mainPanel.setMembers(pagesPanel, btns); 
		
		if (pages.size() > 0)
		{
			pagesPanel.setMembers(pages.get(0));
			currentPage = 0;
		}
	}
	
	public void addPage(WizardPage page)
	{
		this.pages.add(page);
	}
	
	public void next()
	{
		if (currentPage >= pages.size()-1)
			return;
		currentPage++;
		pagesPanel.clear();
		pagesPanel.setMembers(pages.get(currentPage));
	}
	
	public void previous()
	{
		if (currentPage <= 0)
			return;
		currentPage--;
		pagesPanel.clear();
		pagesPanel.setMembers(pages.get(currentPage));
	}
	
	public void cancel()
	{
		CenterPanel center = TXMWEB.getCenterPanel();
		center.removeTab(this);
	}
	
	public void done()
	{
		//call service here
		CenterPanel center = TXMWEB.getCenterPanel();
		center.removeTab(this);
	}
	
	public void setPreviousEnable(boolean state)
	{
		if (state == true)
			previousBtn.enable();
		else
			previousBtn.disable();
	}
	
	public void setNextsEnable(boolean state)
	{
		if (state == true)
			nextBtn.enable();
		else
			nextBtn.disable();
	}
	
	public void setDoneEnable(boolean state)
	{
		if (state == true)
			doneBtn.enable();
		else
			doneBtn.disable();
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub
		
	}
}
