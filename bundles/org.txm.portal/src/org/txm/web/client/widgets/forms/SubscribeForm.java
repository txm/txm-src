package org.txm.web.client.widgets.forms;

import java.util.Map;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.CenterPanel;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.shared.Keys;
import org.txm.web.shared.LoginResult;
import org.txm.web.shared.UserData;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Image;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.SectionItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.validator.MatchesFieldValidator;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.VStack;

/**
 * Opens a popup wich allow to subscribe the user and ask access to the base
 * @author mdecorde, vchabanis
 *
 */
public class SubscribeForm extends TxmWindow {
	private String userlastname = "";
	private String userfirstname = "";
	private String mail = "";
	private String password = "";
	private String login = "";
	private String captchavalue = "";

	private String status = "";
	private String other = "";
	private String institution = "";
	private String tel = "";
	private String access = "";

	private Image captchaImage;

	DynamicForm cguForm; // certify button
	DynamicForm captchaForm; // captcha field

	IButton validateButton;
	IButton cancelButton;
	TextItem loginField;
	TextItem mailField;
	TextItem captchaField;
	PasswordItem passwordField;
	PasswordItem password2Field;

	SectionItem section1;
	SectionItem section2;
	
	private static int refreshme = (int)Math.random()*100;

	public SubscribeForm() {
		super(true, "");
		
		initializeFields(null);
		//setParameters(parameters); // do it here because no Tab will do it
	}
	
	@Override
	public void _initializeFields(Map<String, String> parameters) {
		
		this.setTitle(Services.getI18n().accessFormtitle(TXMWEB.PORTALLONGNAME));
		this.setModalMaskOpacity(20);
		this.setShowModalMask(true);
		
		this.setWidth("75%");
		this.setHeight("75%");
		
		Label intro = new Label();
		intro.setWidth100();
		intro.setMargin(10);
		intro.setContents(Services.getI18n().subscribeIntro(TXMWEB.PORTALLONGNAME));
		
		final SectionStack sectionStack = new SectionStack();  
		sectionStack.setWidth100();
		sectionStack.setHeight100();
		sectionStack.setVisibilityMode(VisibilityMode.MULTIPLE);
		
		SectionStackSection stackSection1 = new SectionStackSection("&nbsp;&nbsp;2. "+Services.getI18n().subscribeSection1(TXMWEB.PORTALLONGNAME));
		SectionStackSection stackSection2 = new SectionStackSection("&nbsp;&nbsp;1. "+Services.getI18n().subscribeSection2());
		SectionStackSection stackSection3 = new SectionStackSection("&nbsp;&nbsp;3. "+Services.getI18n().subscribeSection3());
		
		stackSection1.setCanCollapse(true);
		stackSection1.setExpanded(true);
		stackSection2.setCanCollapse(true);
		stackSection2.setExpanded(true);
		stackSection3.setCanCollapse(false);
		stackSection3.setExpanded(true);
		
		sectionStack.addSection(stackSection2);
		sectionStack.addSection(stackSection1); 
		sectionStack.addSection(stackSection3);
		
		//SECTION1
		VLayout section1Layout = new VLayout();
		section1Layout.setWidth100();
		section1Layout.setHeight(200);
		section1Layout.setPadding(10);
		
		final HTMLPane cguArea = new HTMLPane();
		cguArea.setPadding(5);
		cguArea.setBorder("2px solid black");
		cguArea.setWidth100();
		cguArea.setHeight("100%");
		cguArea.setContentsURL("html/CSU.html");
		
		Label version = new Label("<b>"+Services.getI18n().printCSU()+" <a href=\"html/CSU.pdf\" target=\"_blank\">PDF</a> / <a href=\"html/CSU.html\" target=\"_blank\">HTML</a></b>");
		version.setHeight("1%");
		version.setWidth100();
		version.setAlign(Alignment.RIGHT);
		cguForm = new DynamicForm();
		cguForm.setWidth("100%");
		cguForm.setHeight("1%");
		cguForm.setWrapItemTitles(true);
		cguForm.setBorder("0px black solid");
		
		final CheckboxItem acceptItem = new CheckboxItem("AcceptTerms");  
		//acceptItem.setTitle(Services.getI18n().accessFormCertificate(TXMWEB.PORTALLONGNAME));
		//acceptItem.setHint("*");
		acceptItem.setRequired(true);  
		acceptItem.setShowTitle(false);
		acceptItem.setShowLabel(false);
		//acceptItem.setWrapTitle(true);
		StaticTextItem sti = new StaticTextItem();
		sti.setValue(Services.getI18n().accessFormCertificate(TXMWEB.PORTALLONGNAME));
		sti.setShowTitle(false);

		cguForm.setItems(acceptItem, sti);
		cguForm.setNumCols(2);
		cguForm.setColWidths(20, "*");
		//cguForm.setAlign(Alignment.CENTER);
		section1Layout.setMembers(cguArea, version, cguForm);
		stackSection1.addItem(section1Layout);
		
		//SECTION2
		VStack section2Layout = new VStack();
		section2Layout.setWidth100();
		section2Layout.setHeight(300);
		section2Layout.setPadding(10);
		form = new DynamicForm();
		
		form.setWidth("100%");
		form.setColWidths("20%","*");

		TextItem lastNameField = new TextItem("LastName");
		TextItem firstNameField = new TextItem("FirstName");
		loginField = new TextItem(Keys.LOGIN);
		loginField.setIconPrompt(Services.getI18n().checkAvaibilityOfLogin());
		PickerIcon checkLoginPicker = new PickerIcon(PickerIcon.SEARCH, new FormItemClickHandler() {
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				String login = loginField.getValueAsString();
				if (login == null || login.trim().length() == 0)
					return;
				Services.getAuthenticationService().checkAvailability(login.trim(), new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						TxmDialog.severe(Services.getI18n().internalError());
					}

					@Override
					public void onSuccess(String result) {
						if (result.length() == 0) {
							TxmDialog.show(Services.getI18n().loginNotUsed());
						} else {
							if ("short".equals(result))
								TxmDialog.warning(Services.getI18n().loginTooShort());
							else if ("fail".equals(result)) {
								TxmDialog.warning(Services.getI18n().loginUsed());
							} else {
								TxmDialog.warning(result);
							}
						}
					}
				});
			}
		});
		loginField.setIcons(checkLoginPicker);
		passwordField = new PasswordItem("Password");
		password2Field = new PasswordItem("ConfirmPassword");
		final RadioGroupItem radioGroupItem = new RadioGroupItem("Statut");
		radioGroupItem.setWidth(300);
		final TextItem otherField = new TextItem("Other");
		TextItem addressField = new TextItem("Institution");
		mailField = new TextItem("Mail");
		mailField.setTooltip("xxx@yyy.zz");
		//TextItem telField = new TextItem("Phone");
		
		lastNameField.setRequired(true);
		firstNameField.setRequired(true);
		loginField.setRequired(true);
		passwordField.setRequired(true);
		password2Field.setRequired(true);
		radioGroupItem.setRequired(true);
		addressField.setRequired(true);
		mailField.setRequired(true);
		
		lastNameField.setHint("*");
		firstNameField.setHint("*");
		loginField.setHint("*");
		loginField.setTooltip(Services.getI18n().betweenMinAndMaxChar(UserData.MINLOGIN, UserData.MAXLOGIN));
		passwordField.setHint("*");
		passwordField.setTooltip(Services.getI18n().betweenMinAndMaxChar(UserData.MINPASSWORD, UserData.MAXPASSWORD));
		password2Field.setHint("*");
		radioGroupItem.setHint("*");
		addressField.setHint("*");
		mailField.setHint("*");
		otherField.setHint("*");
		
		lastNameField.setTitle(Services.getI18n().name());
		firstNameField.setTitle(Services.getI18n().firstName());
		loginField.setTitle(Services.getI18n().login());
		passwordField.setTitle(Services.getI18n().password());
		password2Field.setTitle(Services.getI18n().confirm_password());
		radioGroupItem.setTitle(Services.getI18n().status());  
		otherField.setTitle(Services.getI18n().otherPrecise());  
		addressField.setTitle(Services.getI18n().institutionalAddress());
		
		mailField.setTitle(Services.getI18n().email());
		//telField.setTitle(Services.getI18n().phone());
		Services.getI18n().teacherOrResearcher();
		Services.getI18n().studentOrPhD();
		Services.getI18n().engineer();
		Services.getI18n().otherPrecise();
		radioGroupItem.setValueMap(Services.getI18n().teacherOrResearcher(), 
				Services.getI18n().studentOrPhD(), 
				Services.getI18n().engineer(), 
				Services.getI18n().otherPrecise());
		radioGroupItem.setDefaultValue(Services.getI18n().teacherOrResearcher());
		radioGroupItem.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				if(radioGroupItem.getValueAsString().equals(Services.getI18n().otherPrecise())) {
					otherField.enable();
				} else {
					otherField.disable();
				}
			}
		});
		otherField.setLeft(10);
		form.setFields(lastNameField, firstNameField, addressField, radioGroupItem, otherField, mailField, loginField, passwordField, password2Field);//, fnameField, lnameField);
		Label etoiles = new Label(Services.getI18n().starsAreMandatory());
		etoiles.setHeight("1%");
		etoiles.setWidth100();
		
		section2Layout.setMembers(form);
		stackSection2.addItem(section2Layout);
		
		//SECTION3
		VLayout section3Layout = new VLayout();
		section3Layout.setWidth100();
		section3Layout.setPadding(10);
		
		captchaForm = new DynamicForm();
		captchaForm.setWidth("100%");
		captchaForm.setHeight("1%");
		captchaForm.setColWidths("20%","80%");
		
		captchaField = new TextItem("Captcha");
		captchaField.setRequired(true);
		captchaField.setTitle(Services.getI18n().captchatext());
		captchaField.setHint("*");
		
		HLayout captchaArea = new HLayout();
		captchaArea.setMembersMargin(5);
		captchaArea.setWidth100();
		
		captchaImage = new Image("/SimpleCaptcha.jpg");
		captchaImage.setHeight("100px");
		captchaImage.setWidth("200px");
		//captchaImage.setBorder("1px solid black");
		//captchaImage.setWidth("20%");

		IButton captchaRefresh = new IButton(Services.getI18n().captchaRefresh());
		captchaRefresh.setPrompt(Services.getI18n().captchaRefreshTooltip());
		captchaRefresh.setAutoFit(true);
		//captchaRefresh.setWidth("80%");
		captchaRefresh.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				captchaImage.setUrl("SimpleCaptcha.jpg?new+"+refreshme++); // simple counter to force refresh
			}
		});
		
		captchaArea.setAlign(Alignment.CENTER);
		captchaArea.addMember(captchaImage);
		captchaArea.addMember(captchaRefresh);
		
		captchaForm.setFields(captchaField);
		section3Layout.setMembers(captchaArea, captchaForm);
		stackSection3.addItem(section3Layout);
		
		//FINALIZE
		validateButton = new IButton(Services.getI18n().submitButton());
		validateButton.setPrompt(Services.getI18n().confirmSubcription());
		cancelButton = new IButton(Services.getI18n().cancelButton());
		cancelButton.setPrompt(Services.getI18n().cancelAndcloseWindow());
		
		HLayout buttons = new HLayout(2);
		buttons.setMargin(7);
		//buttons.setAutoHeight();
		buttons.setWidth100();
		buttons.setAlign(Alignment.CENTER);
		buttons.setMembers(validateButton, cancelButton);
		
		// set change handlers and initial settings
		addValidateHandlers();
		addChangeHandlers();
		otherField.setDisabled(true);
		validateButton.disable();
		
		intro.setHeight(20);
		sectionStack.setHeight(300);
		sectionStack.setOverflow(Overflow.VISIBLE);
		buttons.setHeight(20);
		
		this.addItem(intro);
		this.addItem(sectionStack);
		this.addItem(etoiles);
		this.addItem(buttons);
		
		//this.addItem(mainLayout);
		//RootPanel.get().add(this);
		this.show();
		captchaImage.setUrl("SimpleCaptcha.jpg?new+"+refreshme++); // simple counter to force refresh
	}

	private void addValidateHandlers()
	{
		validateButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				validate();
			}
		});
		cancelButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				destroy();
			}
		});
		
		KeyPressHandler keyPressValidateHandler = new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event.getKeyName().equals("Enter")) {
					validate();
				}
			}
		};
		loginField.addKeyPressHandler(keyPressValidateHandler);
		mailField.addKeyPressHandler(keyPressValidateHandler);
		passwordField.addKeyPressHandler(keyPressValidateHandler);
		password2Field.addKeyPressHandler(keyPressValidateHandler);
	}

	private void addChangeHandlers() {
		ChangedHandler changelistener = new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				if (loginField.getValue() != null  &&
						mailField.getValue() != null &&
						passwordField.getValue() != null  &&
						password2Field.getValue() != null  &&
						captchaField.getValue() != null)
					validateButton.enable();
				else
					validateButton.disable();
			}
		};

		loginField.addChangedHandler(changelistener);
		mailField.addChangedHandler(changelistener);
		passwordField.addChangedHandler(changelistener);
		password2Field.addChangedHandler(changelistener);
		captchaField.addChangedHandler(changelistener);
		
		// mail validator
		RegExpValidator regExpValidator = new RegExpValidator();  
		regExpValidator.setExpression("^([a-zA-Z0-9_.\\-+])+@(([a-zA-Z0-9\\-])+\\.)+[a-zA-Z0-9]{2,4}$");
		regExpValidator.setErrorMessage(Services.getI18n().mailNotValid());
		mailField.setValidators(regExpValidator);
		// password
		MatchesFieldValidator matchesValidator = new MatchesFieldValidator();  
		matchesValidator.setOtherField("password");  
		matchesValidator.setErrorMessage(Services.getI18n().passwordAndConfirmDiffers());
		password2Field.setValidators(matchesValidator);
	}
	
	private void showMissingFields()
	{
		captchaForm.validate(false);
		cguForm.validate(false);
		form.validate(false);
	}

	protected void validate() {
		// inscription fields
		userfirstname = form.getValueAsString("FirstName");
		if (userfirstname == null)
			userfirstname = "";
		userlastname = form.getValueAsString("LastName");
		if (userlastname == null)
			userlastname = "";
		login = form.getValueAsString(Keys.LOGIN);
		mail = form.getValueAsString("Mail");
		password = form.getValueAsString("Password");
		captchavalue = captchaForm.getValueAsString("Captcha");
		String password2 = form.getValueAsString("ConfirmPassword");
		// infos fields
		status = form.getValueAsString("Statut");
		other = form.getValueAsString("Other");
		institution = form.getValueAsString("Institution");
		tel = "";//form.getValueAsString("Phone");
		access = cguForm.getValueAsString("AcceptTerms");
		
		if (login == null || login.trim().equals("")) {
			TxmDialog.warning(Services.getI18n().loginNotSet());
			showMissingFields();
			return;
		}
		if (mail == null || mail.trim().equals("")) {
			TxmDialog.warning(Services.getI18n().mailNotSet());
			showMissingFields();
			return;
		}
		if (password == null || password.trim().equals("")) {
			TxmDialog.warning(Services.getI18n().passwordNotSet());
			showMissingFields();
			return;
		}
		if (password2 == null || password2.trim().equals("")) {
			TxmDialog.warning(Services.getI18n().passwordConfirmationNotSet());
			showMissingFields();
			return;
		}
		if (captchavalue == null || captchavalue.trim().equals("")) {
			TxmDialog.warning(Services.getI18n().captchaNotSet());
			showMissingFields();
			return;
		}
		
		if (userlastname == null || userlastname.trim().equals("")) {
			TxmDialog.warning(Services.getI18n().nameNotSet());
			showMissingFields();
			return;
		}
		if (userfirstname == null || userfirstname.trim().equals("")) {
			TxmDialog.warning(Services.getI18n().firstnameNotSet());
			showMissingFields();
			return;
		}
		if (status == null || status.trim().equals("")) {
			TxmDialog.warning(Services.getI18n().statusNotSet());
			showMissingFields();
			return;
		}
		if (institution == null || institution.trim().equals("")) {
			TxmDialog.warning(Services.getI18n().institutionNotSet());
			showMissingFields();
			return;
		}
		
		boolean accessb = false;
		if (access == null || access.trim().equals("") || access.trim().equals("false")) {
			TxmDialog.warning(Services.getI18n().accessNotSet());
			showMissingFields();
			return;
		} else {
			accessb = true;
		}
		
		// normalize values
		userlastname = userlastname.trim();
		userfirstname = userfirstname.trim();
		status = status.trim();
		mail = mail.trim();
		login = login.trim();
		password = password.trim();
		password2 = password2.trim();
		captchavalue = captchavalue.trim();		
		if(tel != null) // is not mandatory
			tel = tel.trim();
		else
			tel = "";
		
		if (status.equals(Services.getI18n().otherAndClarify())) {
			if(other == null || other.trim().length() == 0) {
				TxmDialog.warning(Services.getI18n().statusNotSet());
				showMissingFields();
				return;
			} else {
				status = other.trim();
			}
		}
		
		if (!password.equals(password2)) {
			TxmDialog.warning(Services.getI18n().passwordAndConfirmDiffers());
			showMissingFields();
			return;
		}

		if (!UserData.checkValidPassword(password)) {
			TxmDialog.warning(Services.getI18n().passwordTooWeak());
			showMissingFields();
			return;
		}

		if (!UserData.checkValidLogin(login)) {
			TxmDialog.warning(Services.getI18n().loginTooShort());
			showMissingFields();
			return;
		}

		if (!UserData.checkMail(mail)) {
			TxmDialog.warning(Services.getI18n().mailNotValid());
			showMissingFields();
			return;
		}

		UserData data = new UserData(login, password, "", mail, userfirstname, userlastname, captchavalue, status, institution, tel);
		System.out.println("Data: "+data);
		// first register the account
		ProcessingWindow.show(Services.getI18n().registering_account());
		Services.getAuthenticationService().askAccessAndSubscribe(data, accessb, new AsyncCallback<LoginResult>() {
			@Override
			public void onSuccess(LoginResult result) {
				ProcessingWindow.hide();
				if (!result.isSuccess()) {
					captchaImage.setUrl("SimpleCaptcha.jpg?new+"+refreshme++); // simple counter to force refresh
					String code = result.getHomePage();
					if (code.equals("Wrong captcha"))
						TxmDialog.warning(Services.getI18n().wrongCaptcha());
					else if (code.startsWith("login_already_used"))
						TxmDialog.warning(Services.getI18n().loginUsed());
					else if (code.startsWith("fail_send_inscription_mail"))
						TxmDialog.warning(Services.getI18n().mailFailed());
					else if (code.startsWith("MailUsed"))
						TxmDialog.warning(Services.getI18n().mailUsed());
					else
						TxmDialog.warning(Services.getI18n().loginFailed(result.getHomePage()));
				} else {
					if (TXMWEB.MAILENABLE) {
						if (result.getHomePage().equals(mail)) { // if the mail differs, there was a problem
							TxmDialog.info(Services.getI18n().confirmTheAccessDemand(), 400, 200,
									Services.getI18n().confirmationMailSent(mail, TXMWEB.PORTALSHORTNAME, TXMWEB.CONTACT));
							TXMWEB.getConsole().addMessage(Services.getI18n().accessFormSent());
							destroy();
						} else {
							TxmDialog.severe(Services.getI18n().internalError()+result.getHomePage());
						}
					} else {
						// user can login already, there is no activation, because there is no mail
						TXMWEB.getSideNavTree().updateTree();
						TXMWEB.getMenuBar().updateMenuBar();
						TXMWEB.getCenterPanel().updateUserPages();
						CenterPanel center = TXMWEB.getCenterPanel();
						if (center.getTabs().length > 0) {
							center.getUniqTab(Services.getI18n().homeTab());
							center.selectTab(0);
						}
							
						TXMWEB.getConsole().addMessage(Services.getI18n().connected());
						destroy();
					}				
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				TxmDialog.warning(Services.getI18n().signUpError());
			}
		});
	}

	private boolean sendAccesForm()
	{
		Services.getAuthenticationService().askAccess(login, userlastname, userfirstname, status, institution, mail, tel, true, new AsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				if (!result) {
					TxmDialog.warning(Services.getI18n().askAccessFailed());//"Sorry we were enable to send the access form. Please check your email, or contact the administrator. You can still ask access to the base using the button 'ask access'"
				} else {
					//TxmDialog.info(Services.getI18n().accessFormSent());
					TxmDialog.info(Services.getI18n().confirmationMailSent(mail, TXMWEB.PORTALSHORTNAME, TXMWEB.CONTACT));
					TXMWEB.getConsole().addMessage(Services.getI18n().accessFormSent());
					destroy();
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				//SubscribeForm.this.destroy();
				TxmDialog.warning(Services.getI18n().accessError());
			}
		});
		
		return true;
	}
}