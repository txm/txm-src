package org.txm.web.client.widgets;

import java.util.LinkedHashMap;

import org.txm.web.client.TXMWEB;

import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.user.client.Window.Location;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;

public class LanguageSelectionItem extends DynamicForm {
	
	public LanguageSelectionItem(String... languages) {
		super();
		
		LinkedHashMap<String, String> valueIcons = new LinkedHashMap<String, String>();
		SelectItem content = new SelectItem("lang selection");
		
		//content.setTitle();
		content.setShowTitle(false);
		content.setImageURLPrefix("flags/");
		content.setImageURLSuffix(".png");	
		content.setShowAllOptions(true);
		content.setValueMap(languages);
		
		for (String lang : languages) {
			valueIcons.put(lang, lang);
		}
		content.setValueIcons(valueIcons);
		content.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				UrlBuilder builder = Location.createUrlBuilder();
				String[] param = new String[] {(String)event.getValue()};
				builder.setParameter("locale", param);
				Location.assign(builder.buildString());
			}
		});
		content.setDefaultValue(TXMWEB.getLocale());
		
		this.setWidth(65);
		content.setWidth(65);
		this.setFields(content);
	}
}
