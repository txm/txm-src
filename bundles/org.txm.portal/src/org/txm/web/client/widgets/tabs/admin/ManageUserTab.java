package org.txm.web.client.widgets.tabs.admin;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.shared.UserData;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.MultipleAppearance;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.KeyPressEvent;
import com.smartgwt.client.widgets.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class ManageUserTab extends Tab {

	private Label userInfo = new Label();
	final SelectItem addProfilSelection = new SelectItem();
	final SelectItem removeProfileSelection = new SelectItem();
	final SelectItem removeUserSelection = new SelectItem();

	public String inputFormat = "yyyy-MM-dd";
	public DateTimeFormat datereader = DateTimeFormat.getFormat(inputFormat);
	private DynamicForm removeProfilePanel;
	private DynamicForm addPanel;
	private DynamicForm massPanel;
	private DynamicForm addUpdatePanel;
	private DynamicForm removePanel;
	private Layout allUsersPanel;
	private AdminTab adminTab;

	public ManageUserTab(AdminTab adminTab) {
		this.adminTab = adminTab;

		this.setTitle(Services.getI18n().manageUser());
		VLayout content = new VLayout();
		try {
			addPanel = add();
			
		} catch (Exception e){TxmDialog.severe("ManageUserTab:AddPanel:exception: "+e);}

		try {
			removeProfilePanel = removeProfileFromUser();
		} catch (Exception e) {TxmDialog.severe("ManageUserTab:ProfilePanel:exception: "+e);}
		
		HLayout vcontent = new HLayout(2);
		vcontent.setMembers(addPanel, removeProfilePanel);
		content.addMember(vcontent);
		
		try {
			massPanel = massManage();
			content.addMember(massPanel);
		}catch(Exception e) {TxmDialog.severe("ManageUserTab:massManage:exception: "+e);}

		try {
			addUpdatePanel = addUpdateUser();
		} catch (Exception e) {TxmDialog.severe("ManageUserTab:updatePanel:exception: "+e);}

		try {
			removePanel = removeUser();
		} catch (Exception e) {TxmDialog.severe("ManageUserTab:RemovePanel:exception: "+e);}
		
		vcontent = new HLayout(2);
		vcontent.setMembers(addUpdatePanel, removePanel);
		content.addMember(vcontent);
		
		try {
			allUsersPanel = listUsersAndDetails();
			content.addMember(allUsersPanel);
		} catch (Exception e) {TxmDialog.severe("ManageUserTab:TablePanel:exception: "+e);}
		
		try {
			refreshUsersLists(); // fill users lists
		} catch(Exception e) {
			TxmDialog.severe("Internal Error during initialization: "+e);
		}
		this.setPane(content);
	}

	SelectItem addProfiles = new SelectItem();
	private DynamicForm add() {
		final DynamicForm form = new DynamicForm();
		
		form.setIsGroup(true);
		form.setGroupTitle(Services.getI18n().adminAffectAProfile());
		form.setCanDragResize(false);

		addProfiles.setTitle(Services.getI18n().selectAProfileToApply());
		addProfiles.setName("profile");
		addProfiles.setShowAllOptions(true);
		refreshAddProfiles();
		addProfilSelection.setTitle(Services.getI18n().selectAUser());
		addProfilSelection.setName("user");
		addProfilSelection.setShowAllOptions(true);
		/*Services.getAdminService().listUsers(new AsyncCallback<List<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				SC.warn("No user available");
			}
			@Override
			public void onSuccess(List<String> result) {
				addProfilSelection.setValueMap(result.toArray(new String[result.size()]));
			}
		});*/
		addProfilSelection.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				updateAvailableProfiles((String)event.getValue());
			}
		});

		ButtonItem validate = new ButtonItem(Services.getI18n().adminValidate());
		validate.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ProcessingWindow.show("Adding profile to user...");
				Boolean sendMail = (Boolean) form.getValue("sendMail");
				if (sendMail == null)
					sendMail = false;

				Services.getAdminService().addProfileToUser(form.getValueAsString("user"), form.getValueAsString("profile"), sendMail, new AsyncCallback<Boolean>() {
					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						SC.say("Fail: "+caught);
					}
					@Override
					public void onSuccess(Boolean result) {

						ProcessingWindow.hide();
						if (result) {
							updateAvailableProfiles(form.getValueAsString("user"));
							SC.say("Success");
							refreshUsersGrid();
						} else {
							SC.warn("Error");
						}
					}
				});
			}
		});

		CheckboxItem checkboxItem = new CheckboxItem("sendMail", "Send a mail to the user");  
		checkboxItem.setValue(false);
		if (!TXMWEB.MAILENABLE)
			form.setItems(addProfilSelection, addProfiles, validate);
		else
			form.setItems(addProfilSelection, addProfiles, checkboxItem, validate);
		
		return form;
	}

	private void refreshAddProfiles() {
		Services.getAdminService().listProfiles(new AsyncCallback<List<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				SC.warn("No profile available: "+caught);
			}
			@Override
			public void onSuccess(List<String> result) {
				addProfiles.setValueMap(result.toArray(new String[result.size()]));
			}
		});
	}

	private void refreshUsersLists()
	{
		Services.getAdminService().listUsers(new AsyncCallback<List<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				SC.warn("No user available: "+caught);
			}
			@Override
			public void onSuccess(List<String> result) {
				Collections.sort(result);
				removeProfileSelection.setValueMap(result.toArray(new String[result.size()]));
				addProfilSelection.setValueMap(result.toArray(new String[result.size()]));
				removeUserSelection.setValueMap(result.toArray(new String[result.size()]));
			}
		});
	}

	private DynamicForm removeProfileFromUser() {
		final DynamicForm form = new DynamicForm();
		final SelectItem profile = new SelectItem();

		profile.setTitle(Services.getI18n().selectAProfileToDelete());
		profile.setName("profile");
		profile.setShowAllOptions(true);
		removeProfileSelection.setTitle(Services.getI18n().selectAUser());
		removeProfileSelection.setName("user");
		removeProfileSelection.setShowAllOptions(true);
		/*Services.getAdminService().listUsers(new AsyncCallback<List<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				SC.warn("No user available");
			}
			@Override
			public void onSuccess(List<String> result) {
				removeProfileSelection.setValueMap(result.toArray(new String[result.size()]));
			}
		});*/
		removeProfileSelection.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				Services.getAdminService().getUsersProfiles((String)event.getValue(), new AsyncCallback<List<String>>() {
					@Override
					public void onFailure(Throwable caught) {
						userInfo.setContents("No info: "+caught);
					}
					@Override
					public void onSuccess(List<String> result) {
						profile.setValueMap(result.toArray(new String[result.size()]));
						String content = Services.getI18n().usersProfile() + "<br />";
						for (String s : result)
							content += s + "<br />";
								userInfo.setContents(content);
					}
				});
			}
		});

		ButtonItem validate = new ButtonItem(Services.getI18n().delete());
		validate.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ProcessingWindow.show("Removing profile to user");
				Services.getAdminService().removeProfileFromUser(form.getValueAsString("user"), form.getValueAsString("profile"), new AsyncCallback<Boolean>() {
					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						SC.say("Fail: "+caught);
					}
					@Override
					public void onSuccess(Boolean result) {
						ProcessingWindow.hide();
						updateAvailableProfiles(form.getValueAsString("user"));
						SC.say("Success");
						refreshUsersGrid();
					}
				});
			}
		});

		form.setItems(removeProfileSelection, profile, validate);
		form.setGroupTitle(Services.getI18n().adminUserRemoveAProfile());
		form.setIsGroup(true);
		form.setCanDragResize(false);
		return form;
	}

	private DynamicForm removeUser() {
		final DynamicForm form = new DynamicForm();

		removeUserSelection.setTitle(Services.getI18n().selectAUser());
		removeUserSelection.setName("user");
		removeUserSelection.setShowAllOptions(true);
		removeUserSelection.setMultiple(true);
		removeUserSelection.setMultipleAppearance(MultipleAppearance.PICKLIST);

		ButtonItem validate = new ButtonItem(Services.getI18n().delete());
		validate.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				removeUserAction(form.getValueAsString("user").split(","));
			}
		});

		form.setItems(removeUserSelection, validate);
		form.setGroupTitle(Services.getI18n().removeAUser());
		form.setIsGroup(true);
		form.setCanDragResize(false);
		return form;
	}

	protected void removeUserAction(String[] userNames) {
		ProcessingWindow.show("Removing user");
		Services.getAdminService().removeUser(userNames, new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				SC.say("Fail: "+caught);
			}
			@Override
			public void onSuccess(Boolean result) {
				ProcessingWindow.hide();
				refreshUsersLists();
				SC.say("Success");
				refreshUsersGrid();
			}
		});
	}

	public DynamicForm addUpdateUser()
	{
		final DynamicForm form = new DynamicForm();
		form.setNumCols(3);
		
		TextItem loginField = new TextItem();
		loginField.setName("Login");
		PasswordItem passwordField = new PasswordItem();
		passwordField.setName("Password");
		TextItem mailField = new TextItem();
		mailField.setName("Mail");
		TextItem firstnameField = new TextItem();
		firstnameField.setName("FirstName");
		TextItem lastnameField = new TextItem();
		lastnameField.setName("LastName");

		passwordField.setRequired(true);
		loginField.setRequired(true);
		mailField.setRequired(true);

		ButtonItem validate = new ButtonItem("Add or Update");
		validate.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String login = form.getValueAsString("Login");
				String password = form.getValueAsString("Password");
				String mail = form.getValueAsString("Mail");
				String firstname = form.getValueAsString("FirstName");
				String lastname = form.getValueAsString("LastName");

				if (login == null || mail == null || password == null)
					return;

				UserData data = new UserData(login, password, "", mail, firstname, lastname, "", "","","");
				Services.getAdminService().addUpdateUser(data, new AsyncCallback<Boolean>() {
					@Override
					public void onFailure(Throwable caught) {
						SC.say("Fail: "+caught);
					}
					@Override
					public void onSuccess(Boolean result) {
						refreshUsersLists();
						refreshUsersGrid();
						SC.say("Success");
					}
				});
			}
		});
		form.setItems(loginField, passwordField, mailField, firstnameField, lastnameField, validate);
		form.setGroupTitle("Add or update user");
		form.setIsGroup(true);
		form.setCanDragResize(false);
		return form;
	}

	public DynamicForm massManage()
	{
		final TextAreaItem textEditor = new TextAreaItem("textEditor");
		final DynamicForm form = new DynamicForm();

		textEditor.setHeight("*");
		textEditor.setWidth("100%");
		textEditor.setTitle(Services.getI18n().content());
		textEditor.setRowSpan(3);

		ButtonItem validateimport = new ButtonItem("Import");
		validateimport.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String content = form.getValueAsString("textEditor");
				ProcessingWindow.show("Import users");
				Services.getAdminService().importUsers(content, new AsyncCallback<Boolean>() {
					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						SC.say("Fail: "+caught);
					}
					@Override
					public void onSuccess(Boolean result) {
						ProcessingWindow.hide();
						refreshUsersLists();
						SC.say("Success");
						refreshUsersGrid();
					}
				});
			}
		});

		ButtonItem validateexport = new ButtonItem("Export");
		validateexport.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ProcessingWindow.show("Exporting users");
				Services.getAdminService().exportUsers(new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						SC.say("Fail: "+caught);
					}
					@Override
					public void onSuccess(String result) {
						ProcessingWindow.hide();
						textEditor.setValue(result);
					}
				});
			}
		});
		form.setNumCols(3);
		
		form.setItems(textEditor, validateexport, validateimport);
		form.setGroupTitle("Mass export or import");
		form.setIsGroup(true);
		form.setCanDragResize(true);
		form.setHeight("300px");
		return form;
	}

	private void updateAvailableProfiles(String userName) {
		Services.getAdminService().getUsersProfiles(userName, new AsyncCallback<List<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				userInfo.setContents("No info: "+caught);
			}
			@Override
			public void onSuccess(List<String> result) {
				String content = Services.getI18n().usersProfile() + "<br />";
				for (String s : result) {
					content += s + "<br />";
						userInfo.setContents(content);
				}
				
			}
		});
	}
	ListGrid userTable;
	Label userTableTitle;
	private Layout listUsersAndDetails() {
		VLayout content = new VLayout();

		userTableTitle = new Label("User infos");
		userTableTitle.setAutoHeight();
		
		userTable = new ListGrid();  
		userTable.setWidth100();  
		userTable.setHeight(300);  
		userTable.setShowAllRecords(false); 
		userTable.setCanEdit(true);
		userTable.setAutoSaveEdits(false);
		userTable.setCanSelectText(true);
		userTable.addKeyPressHandler(new KeyPressHandler() {
			
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (!event.getKeyName().equals("Delete")) return;
					
				ListGridRecord[] records = userTable.getSelectedRecords();
				if (records == null || records.length == 0) return;
				
				final String[] users = new String[records.length];
				for (int i = 0 ; i < records.length ; i++) {
					ListGridRecord record = records[i];
					users[i] = record.getAttribute("login");
				}
				SC.ask("Remove users: "+Arrays.toString(users), new BooleanCallback() {
					
					@Override
					public void execute(Boolean value) {
						if (value) {
							removeUserAction(users);
							
						}
					}
				});
				
			}
		});
		
		// new ListGridField(NAME, TITLE)
		ListGridField loginField = new ListGridField("login", "Login");
		ListGridField mailField = new ListGridField("mail", "Mail");
		ListGridField profileField = new ListGridField("profil", "Profil");
		ListGridField nameField = new ListGridField("name", "Name");
		ListGridField firstnameField = new ListGridField("firstname", "Firstname");
		ListGridField statusField = new ListGridField("status", "Statut");
		ListGridField institutionField = new ListGridField("institution", "Institution");
		ListGridField phoneField = new ListGridField("phone", "Phone");
		ListGridField lastconnexionField = new ListGridField("connexion", "Last Connexion");
		lastconnexionField.setType(ListGridFieldType.DATE);
		
		lastconnexionField.setCellFormatter(new CellFormatter() {
			@Override
			public String format(Object value, ListGridRecord record, int rowNum,
					int colNum) {
				try	{
					return datereader.format((Date)value);
				}
				catch(Exception e){return "no date";}
			}
		});

		userTable.setFields(loginField, mailField, profileField, nameField, firstnameField, statusField, institutionField, phoneField, lastconnexionField);
		userTable.sort(0, SortDirection.ASCENDING); // sort by login

		TXMWEB.getConsole().addMessage("Getting user infos");
		refreshUsersGrid();

		Button sendMailUnactiveUsers = new Button("Send mail to inactive users");
		Button refreshUsers = new Button("Refresh");
		refreshUsers.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			
			@Override
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				refreshUsersGrid();
			}
		});

		content.setMembers(userTableTitle, userTable, refreshUsers);//, sendMailUnactiveUsers);
		content.setPadding(10);
		content.setBorder("1px solid #DDDDDD");
		content.setAutoHeight();
		return content;
	}
	
	protected void refreshUsersGrid() {
		Services.getAuthenticationService().listUserInfos(new AsyncCallback<List<UserData>>() {
			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.severe("Error while getting infos: "+caught);
			}
			@Override
			public void onSuccess(List<UserData> result) {
				ListGridRecord[] records = new ListGridRecord[result.size()];
				userTableTitle.setContents("User details ("+result.size()+")");
				int i = 0;
				for (UserData data : result) {
					ListGridRecord r = new ListGridRecord();
					r.setAttribute("login", data.login);
					r.setAttribute("mail", data.mail);
					r.setAttribute("profil", data.profil);
					r.setAttribute("name", data.lastname);
					r.setAttribute("firstname", data.firstname);
					r.setAttribute("status", data.status);
					r.setAttribute("institution", data.institution);
					r.setAttribute("phone", data.phone);
					r.setAttribute("connexion", data.lastconnection);
					records[i++] = r;
				}
				userTable.setData(records);
				TXMWEB.getConsole().addMessage("");
			}
		});
	}

	public void updateProfiles() {
		refreshAddProfiles();
	}
}
