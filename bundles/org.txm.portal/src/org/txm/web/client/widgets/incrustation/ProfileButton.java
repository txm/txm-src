package org.txm.web.client.widgets.incrustation;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.forms.ProfileForm;

import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.events.ClickEvent;

public class ProfileButton extends Button{
	public static String ID = "profile_target";

	public static Button getButton()
	{
		Button btn = new Button(Services.getI18n().profileButton());
		btn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				new ProfileForm();
			}
		});
		return btn;
	}
	
	public static Anchor getLink()
	{
		Anchor btn = new Anchor();
		btn.setHTML("<b>"+Services.getI18n().profileButton()+"</b>");
		btn.addClickHandler(new com.google.gwt.event.dom.client.ClickHandler() {
			
			@Override
			public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
				new ProfileForm();
			}
		});
		return btn;
	}
	
	public static boolean incrust()
	{
		RootPanel target = RootPanel.get(ID);
		if (target != null)
		{
			target.clear();
			String type = target.getElement().getAttribute("type");
			if(type != null && type.equals("link"))
				target.add(getLink());
			else
				target.add(getButton());
			return true;
		}
		return false;
	}
}
