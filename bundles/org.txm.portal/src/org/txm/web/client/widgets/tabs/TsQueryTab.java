package org.txm.web.client.widgets.tabs;

import java.util.Map;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.TxmHTMLPane;
import org.txm.web.client.widgets.forms.TIGERSearchForm;
import org.txm.web.client.widgets.forms.TxmForm;
import org.txm.web.shared.Keys;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.VLayout;

public class TsQueryTab extends TxmCenterTab {

	TIGERSearchForm form;
	public TxmForm getForm() {
		return this.form;
	}
	TxmHTMLPane svggraphpanel;
	HTMLPane htmlpanel;
	
	public void setTitle(String title) {
		super.setTitle(Canvas.imgHTML("icons/functions/TS.png", 16, 16)+"&nbsp;"+"<span style=\"vertical-align:-2px;\">"+title+"</span>");
	}
	
	public TsQueryTab(Map<String, String> parameters, ListGridRecord elem) {
			
		this.setName("TSQUERY");
		form = new TIGERSearchForm(elem, parameters, this);
		form.setHeight("30%");
		form.setShowResizeBar(true);
		
		svggraphpanel = new TxmHTMLPane();
		svggraphpanel.setHeight("64%");
		svggraphpanel.setWidth100();
		svggraphpanel.setContentsType(ContentsType.PAGE);
		svggraphpanel.setContentsURL("empty.html");
		
		htmlpanel = new HTMLPane();
		htmlpanel.setHeight("4%");
		
		VLayout layout = new VLayout();
		layout.setWidth100();  
		layout.setHeight100();  
		layout.setOverflow(Overflow.HIDDEN);
		
		//layout.setMembers(form, svggraphpanel, htmlpanel);
		layout.addMember(form);
		layout.addMember(svggraphpanel);
		layout.addMember(htmlpanel);

		refreshTitle(Services.getI18n().ts_title(parameters.get(Keys.PATH).substring(1)));
		this.setPane(layout);
		layout.draw(); 
		
		this.setParameters(parameters);
	}
	
	public void refreshGraph(String svg) {
		svggraphpanel.setContentsURL(svg);
	}
	
	public void refreshHTML(String html) {
		htmlpanel.setContents(html);
	}
	
	public void refreshTitle(String title) {
		this.setTitle(title);
	}

	@Override
	public void onClose() {
		Services.getTsQueryService().dropResult(form.getParams(), new AsyncCallback<Void>() {
			@Override
			public void onSuccess(Void arg0) { }
			
			@Override
			public void onFailure(Throwable arg0) { }
		});
	}
}
