package org.txm.web.client.widgets.tabs;

import java.util.HashMap;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.TxmHTMLPane;

import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.ContentsType;

/**
 * 
 * @author mdecorde
 *
 */
public class HTMLTab extends TxmCenterTab {

	TxmHTMLPane content;
	String name;
	public static HashMap<String, String> userpages = new HashMap<String, String>();
	static {
		userpages.put(Services.getI18n().contactTab(), "Contact");
		userpages.put(Services.getI18n().homeTab(), "Home");
		userpages.put(Services.getI18n().homeTab()+"_expo", "Home");
		userpages.put(Services.getI18n().helpTab(), "Help");
	}
	
	public HTMLTab(String title, String name) {
		this.name = name;
		this.setTitle(title);

		content = new TxmHTMLPane();
		content.setPadding(10);
		content.setContentsType(ContentsType.FRAGMENT);
		content.setHeight100();
		content.setWidth100();
		this.setPane(content);
	}
	
	public HTMLTab(String urlOrContent) {
		
		this.setTitle(urlOrContent);
		
		content = new TxmHTMLPane();
		content.setPadding(10);
		if (urlOrContent.startsWith("http://") || urlOrContent.startsWith("https://")) {
			content.setContentsType(ContentsType.PAGE);
			content.setContentsURL(urlOrContent);
		} else {	
			content.setContentsType(ContentsType.FRAGMENT);
		}
		this.setPane(content);
	}

	public void setContent(String text) {
		
		content.setContents(text);
	}

	public void loadProfilePage(String type) {
		
	}

	public void setContentUrl(String filepath) {
		
		content.setContentsURL(filepath);
	}


	public void update() {
		
		String locale = LocaleInfo.getCurrentLocale().getLocaleName();
		String id = userpages.get(name);
		if (id == null) {
			content.setContents(Services.getI18n().noid_for_userpage(name));
			return;
		}
		Services.getUIService().getUserPage(id, locale, 
				new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				content.setContents(Services.getI18n().errorWith(caught));
			}
			
			@Override
			public void onSuccess(String result) {
				if (result.trim().startsWith("404")){
					content.setContents("Your profile '"+name+"' page is not set. Please contact admin<br>"+result);
				} else {
					// System.out.println("User's profile page : "+result);
					content.setContentsURL(result);
				}
			}
		});
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub
	}
}
