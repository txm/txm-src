package org.txm.web.client.widgets.dialogs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.txm.web.client.services.Services;
import org.txm.web.shared.Keys;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class QueryAssistantDialog extends Window {

	protected String query = "";
	String itemPath;
	private VLayout wordsBar;
	private SelectItem structureComboBoxItem;
	private SpinnerItem contextSizeItem;
	private CheckboxItem contextCheckBox;
	private DynamicForm tmpForm;
	private String tmpFormKey;

	public QueryAssistantDialog(final String itemPath) {
		this.itemPath = itemPath;

		setWidth(600);
		setHeight(300);

		setTitle(Services.getI18n().queryAsssistantDialogTitle());
		setShowMinimizeButton(false);
		setIsModal(true);
		setShowModalMask(true);
		centerInPage();

		IButton newWordButton = new IButton(Services.getI18n().addAWord()); // Add a word
		newWordButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				addANewWord(itemPath);
			}
		});

		IButton okButton = new IButton(Services.getI18n().validateButton());
		okButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				QueryAssistantDialog.this.query = "";
				for (Canvas child : wordsBar.getChildren()) {
					if (child instanceof WordForm) {
						String tmp = ((WordForm) child).getCQL();
						//com.google.gwt.user.client.Window.alert("W CQL="+tmp);
						QueryAssistantDialog.this.query += tmp;
					}
				}

				if (contextCheckBox.getValueAsBoolean()) {
					String structure = structureComboBoxItem.getValueAsString();
					if (structure.equals(Keys.WORD)) {
						structure = "";
					} else {
						structure = " "+structure;
					}
					query += " within "+contextSizeItem.getValueAsString()+structure;
				}
				//com.google.gwt.user.client.Window.alert("CQL="+query);
				if (tmpForm != null && tmpFormKey != null && query.length() > 0) {
					tmpForm.setValue(tmpFormKey, query);
				}
				destroy();
			}
		});

		IButton cancelButton = new IButton(Services.getI18n().cancelButton());
		cancelButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				QueryAssistantDialog.this.query = null;
				destroy();
			}
		});

		wordsBar = new VLayout();
		wordsBar.setMargin(7);
		wordsBar.setWidth100();

		addANewWord(itemPath);

		contextCheckBox = new CheckboxItem();
		contextCheckBox.setName(Keys.CONTEXT);
		contextCheckBox.setTitle(Services.getI18n().withinAContextOf());
		contextCheckBox.setShowTitle(true);
		contextCheckBox.addChangedHandler(new ChangedHandler() {

			@Override
			public void onChanged(ChangedEvent event) {
				if (contextCheckBox.getValueAsBoolean()) {
					contextSizeItem.enable();
					structureComboBoxItem.enable();
				} else {
					contextSizeItem.disable();
					structureComboBoxItem.disable();
				}
			}
		});

		contextSizeItem = new SpinnerItem();
		contextSizeItem.setName("size");
		contextSizeItem.setShowTitle(false);
		//contextSizeItem.setTitle("within");
		contextSizeItem.setMin(1);
		contextSizeItem.setValue(1);
		contextSizeItem.setMax(200);
		contextSizeItem.disable();
		contextSizeItem.setWriteStackedIcons(false);

		structureComboBoxItem = new SelectItem();
		structureComboBoxItem.setName(Keys.STRUCTURE);
		structureComboBoxItem.setShowTitle(false);
		structureComboBoxItem.disable();

		Services.getCorpusActionService().getStructuralUnits(itemPath, new AsyncCallback<List<String>>() {

			@Override
			public void onFailure(Throwable arg0) {
				TxmDialog.severe("Error: "+arg0);
			}

			@Override
			public void onSuccess(List<String> structures) {
				ArrayList<String> values = new ArrayList<String>();
				values.add(Keys.WORD);
				values.addAll(structures);
				values.remove("txmcorpus");
				structureComboBoxItem.setValueMap(values.toArray(new String[values.size()]));
				structureComboBoxItem.setDefaultValue(values.get(0));
			}
		});

		DynamicForm contextBar = new DynamicForm();
		contextBar.setMargin(7);
		contextBar.setWidth100();
		contextBar.setNumCols(10);
		contextBar.setItems(contextCheckBox, contextSizeItem, structureComboBoxItem);

		HLayout validateBar = new HLayout();
		validateBar.setMargin(7);
		validateBar.setWidth100();
		validateBar.setMembers(newWordButton, okButton, cancelButton);

		Label l = new Label(Services.getI18n().iMLookingFor()); // "I'm looking for: "
		l.setWidth100();
		l.setHeight("1%");
		l.setMargin(7);
		
		this.addItem(l);
		this.addItem(wordsBar);
		this.addItem(contextBar);
		this.addItem(validateBar);
	}

	private void addANewWord(final String itemPath) {

		if (this.wordsBar.getChildren().length == 0) {
			WordForm form = new WordForm(itemPath, null);
			this.wordsBar.addMembers(form);
		} else {
			WordSeparatorForm separator = new WordSeparatorForm();
			WordForm form = new WordForm(itemPath, separator);
			this.wordsBar.addMembers(separator, form);
		}
	}

	static public class WordSeparatorForm extends DynamicForm {
		public WordSeparatorForm() {

			this.setWidth("100%");
			this.setHeight("100%");
			this.setNumCols(2);
			this.setColWidths("5%", "95%");

			SpacerItem tmp = new SpacerItem();

			SelectItem separatorItem = new SelectItem();
			separatorItem.setName("separator");
			separatorItem.setShowTitle(false);
			separatorItem.setValueMap(Services.getI18n().followedBy(), 
					Services.getI18n().separatedByAtLeastOneword(), 
					Services.getI18n().mayBeSeparatedBySomeWords());
			separatorItem.setDefaultValue(Services.getI18n().followedBy());

			this.setItems(tmp, separatorItem);
		}

		public String getSeparator() {
			return this.getValueAsString("separator");
		}

		public String getCQLSeparator() {
			String separator = this.getSeparator();
			if (Services.getI18n().followedBy().equals(separator)) {
				return " ";
			} else if (Services.getI18n().separatedByAtLeastOneword().equals(separator)) {
				return " []{1,10} ";
			} else if (Services.getI18n().mayBeSeparatedBySomeWords().equals(separator)) {
				return " []{0,10} ";
			} else {
				return " ";
			}
		}
	}

	public class WordForm extends DynamicForm {

		WordSeparatorForm separator;

		public WordForm(final String itemPath, WordSeparatorForm separator) {
			this.separator = separator;
			this.setWidth("100%");
			this.setHeight("100%");
			this.setNumCols(4);
			this.setColWidths("20%", "20%", "20%", "40%");

			final SelectItem comboPropertyItem = new SelectItem();
			comboPropertyItem.setName("property");
			comboPropertyItem.setTitle(Services.getI18n().aWordWith());
			comboPropertyItem.setDefaultValue(Keys.WORD);
			// ask possible properties and fill gui component with result
			Services.getCorpusActionService().getWordProperties(itemPath, new AsyncCallback<List<String>>() {

				@Override
				public void onFailure(Throwable caught) {
					SC.warn(Services.getI18n().failed_get_wordproperties(itemPath, caught.getMessage()));
				}

				@Override
				public void onSuccess(List<String> result) {
					ArrayList<String> values = new ArrayList<String>(result);
					values.remove(Keys.WORD);
					Collections.sort(values);
					values.add(0, Keys.WORD);
					//com.google.gwt.user.client.Window.alert("values="+values);
					comboPropertyItem.setValueMap(values.toArray(new String[values.size()]));
					comboPropertyItem.setDefaultValue(Keys.WORD);
				}
			});

			SelectItem testItem = new SelectItem();
			testItem.setName("test");
			testItem.setValueMap(Services.getI18n().equalsTo(), Services.getI18n().startingWith(), Services.getI18n().endingWith(), Services.getI18n().containing(), Services.getI18n().differentFrom());
			testItem.setDefaultValue(Services.getI18n().equalsTo());
			testItem.setShowTitle(false);

			final TextItem testValueItem = new TextItem();
			testValueItem.setName("testValue");
			testValueItem.setShowTitle(false);

			PickerIcon goPicker = new PickerIcon(PickerIcon.CLEAR, new FormItemClickHandler() {  
				@Override
				public void onFormItemClick(FormItemIconClickEvent event) {
//					com.google.gwt.user.client.Window.alert("children: "+wordsBar.getChildren().length);
					wordsBar.removeChild(WordForm.this);
					WordForm.this.destroy();
					if (WordForm.this.separator != null) {
						wordsBar.removeChild(WordForm.this.separator);
						WordForm.this.separator.destroy();
					} else if (wordsBar.getChildren().length > 0) { // it's the first word, we need to remove the next WordForm's separator
						Canvas sep = wordsBar.getChildren()[0];
						wordsBar.removeChild(sep);
						if (wordsBar.getChildren().length > 0) {
							WordForm wf = (WordForm)wordsBar.getChildren()[0];
							wf.separator = null;
						}
					}
				}
			});
			goPicker.setShowOver(true);
			testValueItem.setIcons(goPicker);

			WordForm.this.setItems(comboPropertyItem, testItem, testValueItem);
		}

		public String getSeparator() {
			return separator.getValueAsString(Keys.PROPERTY);
		}

		public String getProperty() {
			return this.getValueAsString(Keys.PROPERTY);
		}

		public String getTest() {
			return this.getValueAsString("test");
		}

		public String getTestValue() {
			return this.getValueAsString("testValue");
		}

		public String getCQLWord() {
			String cql = "["+getProperty();

			//comboPropertyItem.setValueMap("is", "startsWith", "endsWith", "contains", "is not");
			String test = getTest();
			String testValue = getTestValue();
			if (Services.getI18n().equalsTo().equals(test)) {
				cql += "=\""+testValue+"\"";
			} else if (Services.getI18n().startingWith().equals(test)) {
				cql += "=\""+testValue+".*\"";
			} else if (Services.getI18n().endingWith().equals(test)) {
				cql += "=\".*"+testValue+"\"";
			} else if (Services.getI18n().containing().equals(test)) {
				cql += "=\".*"+testValue+".*\"";
			} else if (Services.getI18n().differentFrom().equals(test)) {
				cql += "!=\""+testValue+"\"";
			} else {
				cql += "=\""+testValue+"\"";
			}

			return cql +"]";
		}

		public String getCQL() {
			String query = "";
			if (separator != null) {
				query += separator.getCQLSeparator();
			}
			query += getCQLWord();
			return query;
		}
	}

	public String getQuery() {
		return query;
	}

	public void show(DynamicForm queryForm, String key) {
		this.tmpForm = queryForm;
		this.tmpFormKey = key;
		super.show();

	}
}
