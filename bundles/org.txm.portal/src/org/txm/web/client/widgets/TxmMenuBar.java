package org.txm.web.client.widgets;

import org.txm.web.client.Commands;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.forms.AccessForm;
import org.txm.web.client.widgets.menus.FunctionsToolbar;
import org.txm.web.shared.MenuBarContent;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class TxmMenuBar extends ToolStrip {

	private static String welcome;
	public static boolean isUserGuest() {
		return welcome != null && welcome.equals("guest");
	}
	private ToolStripButton accessButton = createAccessButton();
	private ToolStripButton adminButton = createAdminButton();
	private ToolStripButton contactButton = createContactButton();
	private FunctionsToolbar functionBar = new FunctionsToolbar();
	private ToolStripButton helpButton = createHelpButton();
	private LanguageSelectionItem languageSelection = null;
	
	private ToolStripButton loginButton = createLoginButton();
	private ToolStripButton logoutButton = createLogoutButton();
	private ToolStripButton profileButton = createProfileButton();
	private Label selectionNameLabel = null;
	private ToolStripButton signUpButton = createSignUpButton();

	private Label welcomeLabel = new Label();

	public TxmMenuBar(String title) {
		
		this.setID("menuBar");
		this.setHeight(33);
		this.setWidth100();

		if (TXMWEB.EXPO) this.setBackgroundImage("expo_banner.jpg");

		//welcomeLabel.setID("MENUBAR_WELCOME"); // selenium
		welcomeLabel.setWidth(200);
		welcomeLabel.setAlign(Alignment.RIGHT);

		selectionNameLabel = new Label(title);
		selectionNameLabel.setContents("");
		selectionNameLabel.setWidth(24);
		selectionNameLabel.setID("selection_name_label");
		//titleName.setID("MENUBAR_ICON"); // selenium
		
		languageSelection = new LanguageSelectionItem("fr", "en");
		//languageSelection.setID("MENUBAR_LANGUAGESEL"); // selenium
		languageSelection.setHeight(16);

		this.addSpacer(6);
		this.addMember(createTitleButton());
		this.addMember(selectionNameLabel);
		functionBar.addButtonsTo(this);
		this.addFill();
		this.addMember(welcomeLabel);
		
//		ToolStripButton fontSizeButton = new ToolStripButton(null, "icons/font_add.png");
//		fontSizeButton.setTooltip("Increase the interface font size");
//		fontSizeButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
//			public void onClick(ClickEvent event) {
//				Element portalStyle = DOM.getElementById("portal-style");
//				Skinning
//			}
//		});
		this.loginButton.hide();
		this.accessButton.hide();
		this.logoutButton.hide();
		this.signUpButton.hide();
		this.profileButton.hide();
		this.adminButton.hide();
		this.helpButton.hide();
		this.contactButton.hide();

		//		this.loginButton.setID("MENUBAR_LOGIN"); // selenium
		//		this.accessButton.setID("MENUBAR_ACCESS"); // selenium
		//		this.logoutButton.setID("MENUBAR_LOGOUT"); // selenium
		//		this.signInButton.setID("MENUBAR_SIGNIN"); // selenium
		//		this.profileButton.setID("MENUBAR_PROFILE"); // selenium
		//		this.adminButton.setID("MENUBAR_ADMIN"); // selenium
		//		this.helpButton.setID("MENUBAR_HELP"); // selenium
		//		this.contactButton.setID("MENUBAR_CONTACT"); // selenium

		Services.getUIService().fillMenuBar(new AsyncCallback<MenuBarContent>() {
			@Override
			public void onFailure(Throwable caught) {
				SC.warn("Error on initialisation");
			}
			@Override
			public void onSuccess(MenuBarContent result) {
				_updateMenuBar(result);
				if (!TXMWEB.EXPO) {
					//addMember(consoleButton);
					addMember(adminButton);
					addMember(profileButton);
					//addMember(accessButton);
					addMember(signUpButton);

					addMember(logoutButton);
					addMember(loginButton);
					addSeparator();
				}
				addMember(helpButton);
				if (!TXMWEB.EXPO) addMember(contactButton);
				if (!TXMWEB.EXPO) {
					addMember(languageSelection);
				}
			}
		});
	}

	private void _updateMenuBar(MenuBarContent result) {
		if (!TXMWEB.EXPO) {
			updateUserName();
			loginButton.hide();
			logoutButton.hide();
			signUpButton.hide();
			profileButton.hide();
			adminButton.hide();
			//consoleButton.hide();
			accessButton.hide();
			if (result.isLogged()) {
				if (result.isAdmin()) // to test
					adminButton.show();
				//			if(!result.isMemberOf()) // if user has no profile
				//				accessButton.show();
				//consoleButton.show();
				profileButton.show();
				logoutButton.show();
			} else {
				signUpButton.show();
				loginButton.show();
			}
		}
		helpButton.show();
		if (!TXMWEB.EXPO) {
			contactButton.show();
		}
		if (!TXMWEB.EXPO) {
			languageSelection.show();
		}
	}

	private ToolStripButton createAccessButton() {
		ToolStripButton button = new ToolStripButton(Services.getI18n().accessButton());
		button.setStyleName("menuBarButton");
		//button.setShowTitle(!TXMWEB.EXPO);
		button.setTooltip(Services.getI18n().accessButtonTooltip());
		button.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				new AccessForm();
			}
		});
		return button;
	}

	private ToolStripButton createAdminButton() {
		ToolStripButton button = new ToolStripButton();
		if (TXMWEB.EXPO) {
			button.setIcon("icons/admin.png");
		}
		if (!TXMWEB.EXPO) {
			button.setTitle(Services.getI18n().adminButton());
		}
		button.setID("adminButton");
		int size = (TXMWEB.EXPO)?24:24;
		button.setIconHeight(size);
		button.setIconWidth(size);
		//button.setShowTitle(!TXMWEB.EXPO);
		button.setStyleName("menuBarButton");
		button.setTooltip(Services.getI18n().adminButtonTooltip());
		button.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				Commands.callAdmin();
			}
		});
		return button;
	}

	private ToolStripButton createConsoleButton() {
		ToolStripButton button = new ToolStripButton("Console");
		button.setStyleName("menuBarButton");
		button.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				if (TXMWEB.getConsole().isVisible()) {
					TXMWEB.getConsole().hide();
				} else {
					TXMWEB.getConsole().show();
				}
			}
		});
		return button;
	}

	private ToolStripButton createContactButton() {
		ToolStripButton button = new ToolStripButton();
		if (TXMWEB.EXPO) {
			button.setIcon("icons/contact.png");
		}
		if (!TXMWEB.EXPO) {
			button.setTitle(Services.getI18n().contactButton());
		}
		button.setTooltip(Services.getI18n().contactButtonTooltip());
		int size = (TXMWEB.EXPO)?24:24;
		button.setID("contactButton");
		button.setIconHeight(size);
		button.setIconWidth(size);
		//button.setShowTitle(!TXMWEB.EXPO);
		button.setStyleName("menuBarButton");
		button.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				Commands.callContact();
			}
		});
		return button;
	}

	private ToolStripButton createHelpButton() {
		ToolStripButton button = new ToolStripButton();
		button.setTooltip(Services.getI18n().helpButtonTooltip());
		if (TXMWEB.EXPO) {
			button.setIcon("icons/help.png");
		}
		if (!TXMWEB.EXPO) {
			button.setTitle(Services.getI18n().helpButton());
		}
		int size = (TXMWEB.EXPO)?24:24;
		button.setID("helpButton");
		button.setIconHeight(size);
		button.setIconWidth(size);
		//button.setShowTitle(!TXMWEB.EXPO);
		button.setStyleName("menuBarButton");
		button.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				Commands.callHelp();
			}
		});
		return button;
	}

	private ToolStripButton createLoginButton() {
		ToolStripButton button = new ToolStripButton();
		if (TXMWEB.EXPO) {
			button.setIcon("icons/login.png");
		}
		if (!TXMWEB.EXPO) {
			button.setTitle(Services.getI18n().loginButton());
		}
		button.setID("loginButton");
		button.setStyleName("menuBarButton");
		int size = (TXMWEB.EXPO)?24:24;
		button.setIconHeight(size);
		button.setIconWidth(size);
		//button.setShowTitle(!TXMWEB.EXPO);
		button.setTooltip(Services.getI18n().loginButtonTooltip());
		button.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				Commands.callLogin();
			}
		});
		return button;
	}

	private ToolStripButton createLogoutButton() {
		ToolStripButton button = new ToolStripButton();
		if (TXMWEB.EXPO) {
			button.setIcon("icons/logout.png");
		}
		if (!TXMWEB.EXPO) {
			button.setTitle(Services.getI18n().logoutButton());
		}
		button.setID("logoutButton");
		button.setStyleName("menuBarButton");
		int size = (TXMWEB.EXPO)?24:24;
		button.setIconHeight(size);
		button.setIconWidth(size);
		//button.setShowTitle(!TXMWEB.EXPO);
		button.setTooltip(Services.getI18n().logoutButtonTooltip());
		button.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				Commands.callLogout();
			}
		});
		return button;
	}

	private ToolStripButton createProfileButton() {
		ToolStripButton button = new ToolStripButton();
		if (TXMWEB.EXPO) button.setIcon("icons/user.png");
		if (!TXMWEB.EXPO) button.setTitle(Services.getI18n().profileButton());
		button.setStyleName("menuBarButton");
		int size = (TXMWEB.EXPO)?24:24;
		button.setID("profileButton");
		button.setIconHeight(size);
		button.setIconWidth(size);
		//button.setShowTitle(!TXMWEB.EXPO);
		button.setTooltip(Services.getI18n().profileButtonTooltip());
		button.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				Commands.callProfile(Commands.createParameters());
			}
		});
		return button;
	}

	private ToolStripButton createSignUpButton() {
		ToolStripButton button = new ToolStripButton();
		button.setStyleName("menuBarButton");
		String icon = "icons/signup.png";
		int home_icon_size = (TXMWEB.EXPO)?28:28;
		if (TXMWEB.EXPO) button.setIcon(icon);
		if (!TXMWEB.EXPO) button.setTitle(Services.getI18n().subscribeButton());
		button.setID("signupButton");
		button.setIconHeight(home_icon_size);
		button.setIconWidth(home_icon_size);
		//button.setShowTitle(!TXMWEB.EXPO);

		button.setTooltip(Services.getI18n().subscribeButtonTooltip());
		button.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				Commands.callSignUp();
			}
		});
		return button;
	}

	private ToolStripButton createTitleButton() {
		
		ToolStripButton button = new ToolStripButton();
		//titleName.setID("MENUBAR_ICON"); // selenium
		button.setTooltip(Services.getI18n().homeButtonTooltip());

		
		String home_icon = (TXMWEB.EXPO)?"icons/home_expo.png":"icons/TXMlogo.png";
		int home_icon_size = (TXMWEB.EXPO)?28:28;

		button.setIcon(home_icon);
		button.setIconHeight(home_icon_size);
		button.setIconWidth(home_icon_size);

		
		
		if (TXMWEB.EXPO)  {
			button.setTitle(TXMWEB.PORTALTITLE);
			//button.setShowTitle(true);
			//titleName.setWidth("100%");
		} else {
			button.setWidth(28);
		}

		button.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callHome();
			}
		});

		button.setID("menubar_title");
		//button.setStyleName("menubar_title");

		return button;
	}

	public FunctionsToolbar getFunctionBar() {
		return this.functionBar;
	}

	public void updateMenuBar() {
		Services.getUIService().fillMenuBar(new AsyncCallback<MenuBarContent>() {
			@Override
			public void onFailure(Throwable caught) {
				SC.warn("Error on initialisation");
			}
			@Override
			public void onSuccess(MenuBarContent result) {
				_updateMenuBar(result);
			}
		});
	}

	public void updateToolbarPrefix(String selectionName) {
		if (selectionName.length() > 10) {
			selectionName = selectionName.substring(0, 9)+"...";
		}
		selectionNameLabel.setContents(selectionName);
	}

	private void updateUserName() {
		Services.getUIService().getUserName(new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				welcomeLabel.setContents("<i>"+Services.getI18n().welcome() + " " + Services.getI18n().guest()+"</i>");
			}
			@Override
			public void onSuccess(String result) {
				welcome = result;
				if (result.equals("guest")) {
					result = (Services.getI18n().welcome());
				} else if (TXMWEB.MAINTENANCE) {
					result = "The site is under maintenance";
				} else if (result.equals("admin")) {
					result = (result+" - v."+TXMWEB.VERSION);
				}

				welcomeLabel.setContents("<i>"+result+"</i>");
			}
		});
	}
}