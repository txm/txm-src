package org.txm.web.client.widgets.tabs.textselection;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.EnhancedListGrid;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.tabs.HTMLTab;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SortNormalizer;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.grid.events.RowOverEvent;
import com.smartgwt.client.widgets.grid.events.RowOverHandler;
import com.smartgwt.client.widgets.layout.VLayout;

public class TextTable extends VLayout {
	CriteraPanel critera;
	protected TextSelectionTab textSelectionTab;
	ListGrid grid;
	ArrayList<ListGridField> fields = new ArrayList<ListGridField>();
	ListGridRecord[] records;
	ArrayList<String> seltexts;

	ListGridField keepField = new ListGridField("keep", "");
	ListGridField biblioField = new ListGridField("biblio", "");

	public TextTable(final TextSelectionTab textSelectionTab, CriteraPanel critera) {
		this.textSelectionTab = textSelectionTab;
		//seltexts = textSelectionTab.selectedTexts;
		this.critera = critera;
		this.setWidth("60%"); 
		//this.setOverflow(Overflow.VISIBLE);

		grid = new EnhancedListGrid();
		grid.setCanSelectText(true);
		grid.setSize("100%", "100%"); 
		grid.setCanMultiSort(true);  
		grid.setWrapCells(true);
		//grid.setAutoFitFieldText(true);
		grid.setFixedRecordHeights(false); 

		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {

				ListGridField col = grid.getField(event.getColNum());
				if (col.getName().equals(keepField.getName())) {
					ProcessingWindow.show(Services.getI18n().refreshingTextSelection());
					ListGridRecord record = event.getRecord();
					record.setAttribute("keep", !record.getAttributeAsBoolean("keep"));
					updateTextCounts();
					updateTextSelection();
					grid.refreshFields();
				} else if (col.getName().equals(biblioField.getName())) {
					String textid = event.getRecord().getAttribute("id");
					String biblio = textSelectionTab.biblios.get(textid);
					if (biblio != null && biblio.length() > 0) {
						if (TXMWEB.getCenterPanel().containsTab(textid) != null) {
							HTMLTab htmltab = new HTMLTab(textid, textid);
							htmltab.setContentUrl(biblio);
							htmltab.addInView();
						}
					} else {
						TXMWEB.log(Services.getI18n().textWithNoBiblio(textid));
					}
				}
				ProcessingWindow.hide();
			}
		});
		grid.addRowOverHandler(new RowOverHandler() {
			@Override
			public void onRowOver(RowOverEvent event) {
				String name = grid.getFieldName(event.getColNum());
				grid.setPrompt(event.getRecord().getAttribute(name));
			}
		});

		keepField.setTitle(" ");
		keepField.setType(ListGridFieldType.ICON); 
		keepField.setWidth(30);
		HashMap<String, String> valueicons= new HashMap<String, String>();
		valueicons.put("true", "icons/true.png");
		valueicons.put("false", "icons/coche.png");
		keepField.setValueIcons(valueicons);
		fields.add(keepField);

		biblioField.setTitle(Services.getI18n().biblioCol());
		biblioField.setPrompt(Services.getI18n().bibliosPrompt());
		biblioField.setType(ListGridFieldType.ICON); 
		biblioField.setWidth(40);
		valueicons = new HashMap<String, String>();
		valueicons.put("true", "icons/functions/Biblio.png");
		biblioField.setValueIcons(valueicons);
		fields.add(biblioField);

		ListGridField TField = new ListGridField("T", "T");
		TField.setWidth(50);
		TField.setType(ListGridFieldType.INTEGER);
		TField.setPrompt(Services.getI18n().TPrompt());

		//keepField.setCanEdit(true);
		fields.add(TField);
		//System.out.println("Props infos: "+textSelectionTab.propertiesInfos);
		//init property fields
		for (final String property : textSelectionTab.properties) {
			//if (!property.equals("base") && !property.equals("project") && !property.equals("id"))
			//System.out.println("prop: "+property);
			final String sortBy;
			String type = "String";
			String shortname = property;
			String longname = property;
			String output = "yyyy-MM-dd";
			int colwidth = 50;
			if (textSelectionTab.propertiesInfos.get(property) != null) {
				sortBy = textSelectionTab.propertiesInfos.get(property).extra.get("sortBy");
				type = textSelectionTab.propertiesInfos.get(property).type;
				shortname = textSelectionTab.propertiesInfos.get(property).shortname;
				longname = textSelectionTab.propertiesInfos.get(property).longname;
				output = textSelectionTab.propertiesInfos.get(property).extra.get("outputFormat");
				colwidth = textSelectionTab.propertiesInfos.get(property).colwidth;
			} else {
				System.out.println("No property infos for prop: "+property+" in corpus "+textSelectionTab.getItemPath());
				sortBy = null;
			}

			ListGridField field = new ListGridField(property, longname);
			field.setTitle(shortname);
			field.setPrompt(longname);
			field.setWidth(colwidth);

			//field.setWrap(true);

			if ("Integer".equals(type)) {
				field.setType(ListGridFieldType.INTEGER);
			} else if ("Date".equals(type)) {
				//System.out.println("field type: date");
				if (output == null || output.length() == 0) {	
					field.setCellFormatter(new CellFormatter() {
						@Override
						public String format(Object value, ListGridRecord record, int rowNum,
								int colNum) {
							//System.out.println("default formater: "+value+" class: "+value.getClass());
							try {return Selector.defaultdateformater.format((Date) value);}
							catch (Exception e){System.out.println(e);return value.toString();}
						}
					});
				} else {
					final DateTimeFormat formater = DateTimeFormat.getFormat(output);
					field.setCellFormatter(new CellFormatter() {
						@Override
						public String format(Object value, ListGridRecord record, int rowNum,
								int colNum) {
							//System.out.println("format: "+value+" class: "+value.getClass());
							try {return formater.format((Date)value);}
							catch (Exception e){return value.toString();}
						}
					});
				}
			} else {
				field.setType(ListGridFieldType.TEXT);
			}
			
			if (sortBy != null && sortBy.length() > 0) {
				if (textSelectionTab.propertiesInfos.get(sortBy).type.equals("Date")) {	
					field.setSortNormalizer(new SortNormalizer() {
						@Override
						public Object normalize(ListGridRecord record, String fieldName) {
							return record.getAttributeAsDate(sortBy);
						}
					});
				} else {
					field.setSortNormalizer(new SortNormalizer() {
						@Override
						public Object normalize(ListGridRecord record, String fieldName) {
							return record.getAttribute(sortBy);
						}
					});
				}
			}

			fields.add(field);

			if (!textSelectionTab.displayProperties.contains(property))
				field.setHidden(true);
		}
		//init group fields
		for (final String group : textSelectionTab.groups) {
			ListGridField field = new ListGridField(group, textSelectionTab.groupsInfos.get(group).longname);
			field.setTitle(group);
			field.setPrompt(textSelectionTab.groupsInfos.get(group).longname);
			field.setType(ListGridFieldType.TEXT);

			final String sortBy = textSelectionTab.groupsInfos.get(group).extra.get("sortBy");
			if (sortBy != null && sortBy.length() > 0) {
				if (textSelectionTab.propertiesInfos.get(sortBy).type.equals("Date")) {	
					field.setType(ListGridFieldType.DATE);
					field.setSortNormalizer(new SortNormalizer() {
						@Override
						public Object normalize(ListGridRecord record, String fieldName) {
							return record.getAttributeAsDate(sortBy);
						}
					});
				} else {
					field.setSortNormalizer(new SortNormalizer() {
						@Override
						public Object normalize(ListGridRecord record, String fieldName) {
							return record.getAttribute(sortBy);
						}
					});
				}
			}

			// get the position of the column and add it
			int position = Integer.parseInt(textSelectionTab.groupsInfos.get(group).extra.get("position"));
			position = Math.min(position, fields.size()); // in case the position is false
			fields.add(position, field);

			if (!textSelectionTab.displayGroups.contains(group))
				field.setHidden(true);
		}

		grid.setFields(fields.toArray(new ListGridField[]{}));

		records = new ListGridRecord[textSelectionTab.textids.size()];
		int i = 0;
		//init records
		for (String textid : textSelectionTab.textids) {
			ListGridRecord r = new ListGridRecord();
			r.setAttribute("keep", false);
			String biblio = textSelectionTab.biblios.get(textid);
			r.setAttribute("biblio",  biblio != null && biblio.length() > 0);
			r.setAttribute("id", textid);
			r.setAttribute("T", textSelectionTab.numberOfWordsPerText.get(textid));
			for (String property : textSelectionTab.properties) {
				String type = "String";
				String input = "yyyy-MM-dd";
				if (textSelectionTab.propertiesInfos.get(property) != null) {
					type = textSelectionTab.propertiesInfos.get(property).type;
					input = textSelectionTab.propertiesInfos.get(property).extra.get("inputFormat");
				}

				String value = textSelectionTab.valuesPerText.get(textid).get(property);
				if (type.equals("Date")) {
					try {
						Date date;
						//String formated;
						if (input == null || input.length() == 0 )
							date = Selector.defaultdateformater.parse(value);
						else
							date = DateTimeFormat.getFormat(input).parse(value);
						r.setAttribute(property, date);

					}catch (Exception e){System.out.println("error date : input="+input+" value="+value+" e="+e);r.setAttribute(property, value);};
				} else if(type.equals("Integer")) {
					try {r.setAttribute(property, Integer.parseInt(value));}
					catch (Exception e){r.setAttribute(property, value);};
				}
				else
					r.setAttribute(property, value);

			}

			for (String group : textSelectionTab.groups) {
				String type = textSelectionTab.groupsInfos.get(group).type;
				List<String> list = textSelectionTab.groupsProperties.get(group);
				if (list != null && list.size() == 2 && type.equals("MinMaxDate")) // concat min and max dates
				{
					final String property1 = list.get(0);
					final String property2 = list.get(1);
					String output1 = textSelectionTab.propertiesInfos.get(property1).extra.get("outputFormat");
					String output2 = textSelectionTab.propertiesInfos.get(property2).extra.get("outputFormat");

					if (output1 != null && output2 != null) {	
						final DateTimeFormat formater1 = DateTimeFormat.getFormat(output1);
						final DateTimeFormat formater2 = DateTimeFormat.getFormat(output2);

						String V1 = r.getAttributeAsString(property1);
						String V2 = r.getAttributeAsString(property2);
						//System.out.println("V1: "+V1+" V2: "+V2);
						String v = "";
						if (!V1.equals("N/A"))
							try {v += formater1.format(r.getAttributeAsDate(property1));}
						catch (Exception e){ System.out.println(e); v += V1.toString(); }
						else
							v += V1.toString();
						v += " - ";
						if (!V2.equals("N/A"))
							try { v += formater2.format(r.getAttributeAsDate(property2)); }
						catch (Exception e){ System.out.println(e);v += V2.toString(); }
						else
							v += V2.toString();

						r.setAttribute(group, v);
					}
				}
			}
			records[i++] = r;
		}

		this.grid.setData(records);
		this.grid.draw();
		//this.grid.refreshFields();

		//		HLayout layout = new HLayout();
		//		layout.setWidth("100%");
		//
		//		final Button allButton = new Button("Tout sélectionner");  
		//		allButton.setAutoFit(true);  
		//		allButton.addClickHandler(new ClickHandler() {
		//			@Override
		//			public void onClick(ClickEvent event) {
		//				//System.out.println("all");
		//				ProcessingWindow.show(Services.getI18n().refreshingTextSelection())
		//				for(ListGridRecord r : records)
		//				{
		//					r.setAttribute("keep", true);
		//				}
		//				grid.refreshFields();
		//				updateTextSelection();
		//				ProcessingWindow.hide();
		//			}
		//		});
		//
		//		final Button revertButton = new Button("Inverser toute la sélection");  
		//		revertButton.setAutoFit(true);
		//		revertButton.addClickHandler(new ClickHandler() {
		//			@Override
		//			public void onClick(ClickEvent event) {
		//				//System.out.println("all");
		//				ProcessingWindow.show(Services.getI18n().refreshingTextSelection())
		//				for(ListGridRecord r : records)
		//				{
		//					r.setAttribute("keep", !r.getAttributeAsBoolean("keep"));
		//				}
		//				grid.refreshFields();
		//				updateTextSelection();
		//				ProcessingWindow.hide();
		//			}
		//		});

		/*final Button keepButton = new Button("Ajouter");  
		keepButton.setAutoFit(true);
		keepButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ProcessingWindow.show(Services.getI18n().refreshingTextSelection())
				for(ListGridRecord r : grid.getSelection())
				{
					r.setAttribute("keep", true);
				}
				grid.refreshFields();
				updateTextSelection();
				ProcessingWindow.hide();
			}
		});

		final Button banButton = new Button("Retirer");  
		banButton.setAutoFit(true);
		banButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ProcessingWindow.show(Services.getI18n().refreshingTextSelection())
				for(ListGridRecord r : grid.getSelection())
				{
					r.setAttribute("keep", false);
				}
				grid.refreshFields();
				updateTextSelection();
				ProcessingWindow.hide();
			}
		});*/

		/*final Button biblioButton = new Button("Biblio");  
		biblioButton.setAutoFit(true);
		biblioButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {


			}
		});*/

		//layout.setMembers(allButton, revertButton);

		Label lt = new Label();
		int totalword = 0;

		lt.setTitle("N = "+textSelectionTab.selectedTexts.size() + "T = "+totalword);

		this.setMembers(grid);
		this.setShowResizeBar(true); 
	}


	public ArrayList<String> getVisibleCols() {
		ArrayList<String> cols = new ArrayList<String>();

		for (ListGridField field : this.grid.getFields()) {
			if (!field.getName().equals("keep"))
				cols.add(field.getName());					
		}
		//System.out.println("cols: "+cols);
		return cols;
	}

	/**
	 * Update the tables from the records
	 */
	public void updateTextSelection() {
		ArrayList<Record> textssel = textSelectionTab.selectedTexts;
		textssel.clear();
		for (ListGridRecord record : records) {
			if (record.getAttributeAsBoolean("keep")) {
				textssel.add(record); // add text to selection
			}
		}

		//refresh the other views
		if (textSelectionTab.isStatsEnable())
			textSelectionTab.statsPanel.refresh();
		textSelectionTab.critera.refreshSelectorsCounts();
	}

	/**
	 * Updates the counts from records data
	 */
	public void updateTextCounts() {
		HashMap<String, HashMap<String, Integer>> NbTextPerProp = textSelectionTab.numberOfSelectedTextPerProperty;
		HashMap<String, HashMap<String, Integer>> NbWPerTextPerProp = textSelectionTab.numberOfWordPerSelectedTextPerProperty;
		textSelectionTab.resetTextCounts();

		for (ListGridRecord record : records) {
			String id = record.getAttribute("id");
			int nb;
			if (record.getAttributeAsBoolean("keep")) {
				//System.out.println("record : "+record);
				for (String p : this.textSelectionTab.properties) {
					String str = record.getAttribute(p);
					//System.out.println("prop: "+p+" value: "+str);
					//update text count
					nb = NbTextPerProp.get(p).get(str);
					NbTextPerProp.get(p).put(record.getAttribute(p), nb+1);

					//update word count
					nb = NbWPerTextPerProp.get(p).get(str);
					NbWPerTextPerProp.get(p).put(record.getAttribute(p), nb+textSelectionTab.numberOfWordsPerText.get(id));	
				}
			}
		}
	}

	/**
	 * Update the table, but don't change the selection
	 */
	public void refresh() {
		//System.out.println("refresh table with text selection: "+textSelectionTab.selectedTexts);
		for (ListGridRecord record : records) {
			if (textSelectionTab.selectedTexts.contains(record))
				record.setAttribute("keep", true);
			else
				record.setAttribute("keep", false);
		}
		//System.out.println("Refresh text table : "+textSelectionTab.selectedTexts);
		grid.refreshFields();
	}

	String columnLongName= "";
	public String getColumnLongName() {
		return columnLongName;
	}
}
