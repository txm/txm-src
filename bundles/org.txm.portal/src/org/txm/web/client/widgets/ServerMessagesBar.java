package org.txm.web.client.widgets;

import org.txm.web.client.services.Services;

import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class ServerMessagesBar extends ToolStrip{
	Label message = new Label();
	ToolStripButton close = new ToolStripButton("∆ "+Services.getI18n().closeButton());
	
	public ServerMessagesBar()
	{
		message.setWidth100();
		this.setWidth100();
		this.setID("ServerMessageBar");

		this.addSpacer(20);
		this.addMember(message);
		this.addFill();
		this.addMember(close);
		
		close.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				ServerMessagesBar.this.hide();
			}
		});
	}
	
	public void setMessage(String mess)
	{
		message.setContents(mess);
		ServerMessagesBar.this.show();
	}
}
