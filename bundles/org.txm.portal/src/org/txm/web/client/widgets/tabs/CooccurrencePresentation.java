package org.txm.web.client.widgets.tabs;

import java.util.ArrayList;

import org.txm.web.shared.CooccurrenceResultLine;


public interface CooccurrencePresentation {
	
	public void fillList(ArrayList<CooccurrenceResultLine> result);
	public boolean isFilled();
}
