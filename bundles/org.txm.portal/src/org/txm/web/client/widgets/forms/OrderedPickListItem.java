package org.txm.web.client.widgets.forms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.EnhancedListGrid;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyDownEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyDownHandler;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;

public class OrderedPickListItem extends TextItem {
	
	/**
	 * if value contains "," then it is splitted and all values are selected
	 */
	final static String VALUE = "value";
	
	private boolean dialog = true;

	boolean first = true;
	PickerIcon goPicker;

	boolean isOpen = false;
	
	private boolean multiple = true;
	
	private String oldValue;
	
	ListGridRecord[] records;

	ArrayList<String> selected;

	ArrayList<String> values;

	EnhancedListGrid valuesGrid;

	Window winModal;

	private ListGridField valueField;

	private ListGridField keepField;

	public OrderedPickListItem(String name)
	{
		super(name);	
		init();
	}

	public OrderedPickListItem(String title, boolean b) {
		super(title);
		
		this.setTitle(title);
		this.dialog  = b;

		init();
	}

	/**
	 * add only one icon :)
	 * @param icon
	 */
	public void addIcon(PickerIcon icon)
	{
		PickerIcon[] icons = {goPicker,icon};
		super.setIcons(icons);
	}
	
	protected void init() {
		// build grid
		valuesGrid = new EnhancedListGrid();
		
		valuesGrid.setHeight(300);
		//valuesGrid.setWidth(200);
		valuesGrid.setHoverOpacity(75);
		valuesGrid.setCanReorderRecords(true);
		valuesGrid.setShowHeader(false);
		this.setCanSelectText(false);
		
		//		valuesGrid.addDragStopHandler(new DragStopHandler() {
		//			@Override
		//			public void onDragStop(DragStopEvent event) {
		//				updateText();
		//			}
		//		});

		keepField = new ListGridField("keep", "");
		keepField.setTitle(" ");
		keepField.setType(ListGridFieldType.BOOLEAN);
		keepField.addRecordClickHandler(new RecordClickHandler() {

			@Override
			public void onRecordClick(RecordClickEvent event) {
				ListGridRecord selectedRecord = valuesGrid.getSelectedRecord();
				boolean value = !selectedRecord.getAttributeAsBoolean("keep");
				
				if (!multiple) {
					if (!value) { // don't deselect record when the mode is not multiple
						return;
					}
					for (ListGridRecord record : valuesGrid.getRecords()) {
						record.setAttribute("keep", false);
					}
				}
				selectedRecord.setAttribute("keep", value);
				valuesGrid.refreshFields();
			}
		});

		valueField = new ListGridField("value", "");
		valueField.setType(ListGridFieldType.TEXT);
		valuesGrid.setFields(keepField, valueField);
		
		goPicker = new PickerIcon(PickerIcon.COMBO_BOX, new FormItemClickHandler() {  
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				showHideGrid();
			}
		});
		goPicker.setShowOver(true);
		this.setIcons(goPicker);
		this.setShowTitle(false);
		this.setCanFocus(false);
		this.setDisplayField("");

		// disable keys events
		this.addKeyDownHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				event.cancel();
			}
		});

		this.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				showHideGrid();
			}
		});

		winModal = new Window(); 
		winModal.setAutoSize(true);
		winModal.setCanDragResize(true); 
		winModal.setTitle(Services.getI18n().selectProperties()); 
		winModal.setShowMinimizeButton(false);  
		winModal.setIsModal(true);  
		winModal.setShowModalMask(false); 		
		winModal.addItem(valuesGrid);
		winModal.setAlign(Alignment.CENTER);

		//				winModal.addCloseClickHandler(new CloseClickHandler() {  
		//					public void onCloseClick(CloseClientEvent event) {
		//						if(!dialog)
		//							updateText();
		//						OnCancel();
		//						showHideGrid();
		//					}  
		//				}); 

		if (dialog) {
			//System.out.println("DIALOG MODE");
			IButton validateButton = new IButton(Services.getI18n().validateButton());
			IButton cancelButton = new IButton(Services.getI18n().cancelButton());
			HLayout buttons = new HLayout(2);
			buttons.setMargin(5);
			//buttons.setWidth100();
			buttons.setAlign(Alignment.CENTER);
			buttons.setMembers(cancelButton, validateButton);
			validateButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
				@Override
				public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
					updateText();
					if (!oldValue.equals(getValueAsString())) {
						OnValidate();
					}
					showHideGrid();
				}
			});

			cancelButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {

				@Override
				public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
					OnCancel();
					showHideGrid();
				}
			});

			winModal.addItem(buttons);
		}
	}

	/**
	 * Override this method to set the behavior of "CANCEL" button
	 */
	public void OnCancel(){

	}
	
	/**
	 * Override this method to set the behavior of "OK" button
	 */
	public void OnValidate(){

	}

	public FormItem setDefaultValue(String value)
	{
		super.setDefaultValue(value);
		if (records != null && selected != null) {
			selected.clear();
			String[] splited = value.split(",");
			if(splited != null) {
				for (String s : splited) {
					selected.add(s.trim());
				}
				System.out.println("ORDEREDPICKLIST selected="+selected);
				for (ListGridRecord r : records) {
					r.setAttribute("keep", selected.contains(r.getAttributeAsString(VALUE)));
				}
			}
		}
		return this;
	}

	public void setDefaultValues(List<String> values) {
		String value = "";
		for (String s : values) {
			value += ","+s;
		}
		if (value.length() > 0) value = value.substring(1); // remove ","
		super.setDefaultValue(value);

		if (records != null && selected != null) {
			selected.clear();
			for (String s : values) {
				selected.add(s.trim());
			}

			for (ListGridRecord r : records) {
				r.setAttribute("keep", selected.contains(r.getAttributeAsString(VALUE)));
			}
		}
	}

	public void setMultiple(boolean multiple) {
		this.multiple  = multiple;
		valueField.setMultiple(multiple);
		keepField.setMultiple(multiple);
		valuesGrid.setCanReorderRecords(multiple);
	}
	
	public void setValueMap(ArrayList<String> values)
	{
		this.values = values;
		this.selected = new ArrayList<String>();

		// set records
		records = new ListGridRecord[values.size()];
		for (int i = 0 ; i < values.size() ; i++)
		{
			records[i] = new ListGridRecord();
			records[i].setAttribute("value", values.get(i));
			records[i].setAttribute("keep", false);
		}
		valuesGrid.setData(records);
	}
	public void setValueMap(String[] values)
	{
		setValueMap(new ArrayList<String>(Arrays.asList(values)));
	}

	protected void showHideGrid() {
		if (isOpen) { // hide the grid
			winModal.hide();
			isOpen = false;
		} else { // show the grid
			oldValue = this.getValueAsString();
			winModal.centerInPage();
			winModal.show();
			winModal.focus();
			isOpen = true;
		}
	}

	protected void updateText() {
		if (valuesGrid.getSelection(true).length == 0) {
			for (ListGridRecord r : valuesGrid.getRecords()) {
				setValue(r.getAttribute("value"));
				break;
			}
		} else {	
			String str = "";
			for(ListGridRecord r : valuesGrid.getRecords())
				if(r.getAttributeAsBoolean("keep"))
					str += ", "+r.getAttribute("value");
			if(str.length() > 2)
				setValue(str.substring(2));
		}
	}
}
