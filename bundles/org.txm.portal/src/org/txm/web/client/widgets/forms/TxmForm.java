package org.txm.web.client.widgets.forms;

import java.util.HashMap;
import java.util.Map;

import org.txm.web.client.Parameter;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * It's a smartGWT window overridden to set some default options
 * 
 * @version 0.2.0
 * @author vchabani
 */
public abstract class TxmForm extends VLayout {

	protected DynamicForm form;
	protected Map<FormItem, Parameter> declaredParameters = new HashMap<FormItem, Parameter>();

	public TxmForm() {
		super();
	}
	
	

	public void setParameters(Map<String, String> parameters) {
		
		if (parameters == null) return;
		
		if (form == null) return;
		
		for (String property : parameters.keySet()) {
			setParameter(form, property, parameters.get(property));
		}
	}
	
	public static void setParameter(DynamicForm form, String property, String value) {
		if (form == null) return;
		FormItem item = form.getItem(property);
		if (item == null) return;

		item.setValue(value);
	}
	
	public abstract void _initializeFields(Map<String, String> parameters);
	
	private final void _initializeDeclaredParameters() {
		// TODO make this method abstract and register FormItems to the declaredParameters
	}
	
	/**
	 * MUST BE CALLED IN THE CONSTRUCTOR
	 */
	protected final void initializeFields(final Map<String, String> parameters) {
		_initializeFields(parameters);
		_initializeDeclaredParameters();
		setFormItemNamesFromParameterAnnotations();
	}
	
	private final void setFormItemNamesFromParameterAnnotations() {
		// TODO uncomment this when deldeclaredParameters will be used
//		for (FormItem item : declaredParameters.keySet()) {
//				FormItem item;
//				try {
//					Parameter p = declaredParameters.get(item);
//					item.setName(p.key);
//				} catch (IllegalArgumentException e) {
//					// TODO Bloc catch généré automatiquement
//					e.printStackTrace();
//				} catch (IllegalAccessException e) {
//					// TODO Bloc catch généré automatiquement
//					e.printStackTrace();
//				}
//		}
	}
}
