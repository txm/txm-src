package org.txm.web.client.widgets;

import org.txm.web.client.widgets.incrustation.CommandButton;
import org.txm.web.client.widgets.tabs.CMSTab;

import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.events.ContentLoadedEvent;
import com.smartgwt.client.widgets.events.ContentLoadedHandler;
/**
 * A HtmlPane that opens page from the directory pointed by the property htmlBase.
 * If the directory does not contain the page, it tries to open it normally
 * 
 * If a span has a special id, widget will be incrusted into the span
 * @author mdecorde
 *
 */
public class TxmHTMLPane extends HTMLPane {

	//HTMLPane htmlPanel = new HTMLPane();
	//Label header = new Label();
	ContentLoadedHandler contentHandler;
	private CMSTab cmsTab;
	/**
	 * add a ContentLoadedHandler
	 */
	public TxmHTMLPane(CMSTab tab) {
		this();
		this.cmsTab = tab;
	}
	
	public TxmHTMLPane() {
		//		header.setVisible(false);
		//		header.setAlign(Alignment.CENTER);  
		//		header.setBorder("1px solid #808080");  
		//		header.setBackgroundColor("#C3D9FF");  
		//		header.setHeight(20);
		//				
		//		this.addMember(header);
		
		//this.addMember(htmlPanel);
		contentHandler = new ContentLoadedHandler() {	
			@Override
			public void onContentLoaded(ContentLoadedEvent event) {
				//System.out.println("ADJUST FOR CONTENT");
				//if (htmlPanel != null) {
					adjustForContent(true);
					redraw();
//					if (cmsTab != null) {
//						NodeList<com.google.gwt.dom.client.Element> titles = getDOM().getElementsByTagName("title");
//						if (titles.getLength() > 0)
//							cmsTab.setTitle(titles.getItem(0).getInnerText());
//					}
					incrustWidgets();
			//	}
			}
		};
		//createNewHtmlPanel(ContentsType.FRAGMENT);
		this.setContentsType(ContentsType.PAGE);
		
		this.setEvalScriptBlocks(true);

		this.addContentLoadedHandler(contentHandler);
	}
//
//	public void srollToTop() {
//		htmlPanel.scrollToTop();
//	}

	public HTMLFlow setContentsURL(final String filepath) {
		
		if (filepath == null) {
			super.setContentsURL("empty.html");
			return this;
		}
		
		return TxmHTMLPane.super.setContentsURL(filepath);

//		//getHtmlContent
//		Services.getEditionService().getHtmlContent(filepath, new AsyncCallback<String>() {
//			@Override
//			public void onSuccess(String result) {
//				if (result != null) {
//					System.out.println("Loading a distant page: "+filepath);
//					setContents(result);
//				} else {
//					System.out.println("Loading a server public page: "+filepath);
//					
//				}	
//				//incrustWidgets();
//			}
//
//			@Override
//			public void onFailure(Throwable caught) {
//				TxmDialog.severe("Error: getHtmlContent failed: "+caught.getMessage());
//			}
//		});

		//htmlPanel.setContentsURL(filepath);
	}

	public HTMLFlow setContents(String htmlcode) {
		//htmlPanel.setContentsURL(null);
		super.setContents(htmlcode);
		this.markForRedraw();
		if (htmlcode.length() > 0) {
			incrustWidgets();
		}
		return this;
	}

	private void incrustWidgets() {
		com.google.gwt.dom.client.Element dom = this.getDOM();
		if (dom == null) return; // not loaded
//		ClientVariable.incrust();
//		AdminButton.incrust();
//		ConnexionButton.incrust();
//		ContactButton.incrust();
//		DeconnexionButton.incrust();
//		HelpButton.incrust();
//		InscriptionButton.incrust();
//		ProfileButton.incrust();
		CommandButton.incrust(this, dom); // all commands concordances, texts...
	}

	public void setContentsURL() {
		super.setContentsURL(null);
	}

	public CMSTab getCMSTab() {
		return cmsTab;
	}

//	public void setContentsType(ContentsType contentsType) {
//		//this.removeMember(htmlPanel);
//		createNewHtmlPanel(contentsType);
//		//htmlPanel.setContentsType(contentsType);
//	}

//	private void createNewHtmlPanel(ContentsType contentsType) {
//		htmlPanel.destroy();
//		htmlPanel = new HTMLPane();
//
//		//htmlPanel.setWidth100();
//		htmlPanel.setHeight100();
//
//		htmlPanel.setContentsType(contentsType);
//		htmlPanel.addContentLoadedHandler(contentHandler); // to refresh correctly the content and incrust widgets in DOM
//		this.addMember(htmlPanel);
//	}
}
