package org.txm.web.client.widgets.forms;

import java.util.Map;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.widgets.tabs.CMSTab;
import org.txm.web.shared.Keys;

public class PageForm {
	public static CMSTab validate(Map<String, String> parameters) {

		// String path, String profile, String locale, String title, String tooltip
		String path = parameters.get(Keys.PATH);
		if (path == null) return null;
		if (path.length() <= 1) return null;
		if (path.endsWith("/")) path = path.substring(0, path.length() -1);

		// try to find a tab with the page

		//SC.say("callPage: "+parameters);
		TXMWEB.log("Open page: "+path+" with "+parameters);
		CMSTab tab = new CMSTab(parameters); // path, profile, locale, title, tooltip
		tab.addInView();
		tab.update();
		return tab;
	}
}
