package org.txm.web.client.widgets.forms;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.CenterPanel;
import org.txm.web.client.widgets.SplashScreen;
import org.txm.web.client.widgets.tabs.HTMLTab;

public class Helpform {
	
	public static void validate() {
		if (TXMWEB.EXPO) {
			if (TXMWEB.getCenterPanel().getNumTabs() > 0) {
				if (TXMWEB.getCenterPanel().getTab(0).getTitle().startsWith(Services.getI18n().homeTab())) {
					new SplashScreen("html/Help_expo.jsp").show();
				} else {
					new SplashScreen("html/Help_expo_command.jsp").show();
				}
			}
		} else {
			CenterPanel center = TXMWEB.getCenterPanel();
			HTMLTab helpPage = center.getUniqTab(Services.getI18n().helpTab());
			if (helpPage == null)
				return;
			center.selectTab(helpPage);
		}
	}
}
