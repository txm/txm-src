package org.txm.web.client.widgets.forms;

import java.util.Map;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.tabs.BiblioTab;
import org.txm.web.shared.Keys;

import com.google.gwt.user.client.rpc.AsyncCallback;

public class BiblioForm {
	public static void validate(final Map<String, String> parameters) {
		
		final String path = parameters.get(Keys.PATH);
		final String textId = parameters.get(Keys.TEXTID);
		
		Services.getEditionService().getBiblio(path, textId, new AsyncCallback<String>() {

			@Override
			public void onSuccess(String result) {
				if (result != null) {
					if ("not_a_corpus".equals(result)) {
						TxmDialog.severe("Path is not a corpus: "+path);
					} else if ("corpus_not_found".equals(result)) {
						TxmDialog.severe("Could not find corpus with path: "+path);
					} else if ("text_not_found".equals(result)) {
						TxmDialog.severe("Could not find text with id: "+textId+" in corpus "+path);
					} else if ("permission".equals(result)) {
						TxmDialog.severe(Services.getI18n().noPermission());
					} else {
						//TxmDialog.severe("Opening record: "+result);
						BiblioTab tab =	new BiblioTab(result, textId);
						tab.addInView();
						tab.update();
					}
				} else { 
					TXMWEB.log("No record found for text="+textId+" in corpus="+path);
				}
			}

			@Override
			public void onFailure(Throwable caught) { }
		});
	}
}
