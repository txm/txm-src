package org.txm.web.client.widgets.forms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.txm.web.client.ExecTimer;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.tabs.SpecificitiesTab;
import org.txm.web.shared.Keys;
import org.txm.web.shared.SpecificitiesParam;
import org.txm.web.shared.SpecificitiesResultLine;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.HLayout;

public class SpecificityForm extends TxmForm {

	//	private DynamicForm options1Form = new DynamicForm();
	private DynamicForm options2Form = new DynamicForm();
	private SpecificitiesParam params = new SpecificitiesParam();
	private String itemPath;
	private SpecificitiesTab parentTab = null;
	private String mode = SpecificitiesParam.TOPN;
	private int defaultTopN = 10;
	private double defaultSmin = 2.0d;
	private HLayout section1, section3;

	
	public SpecificityForm(Map<String, String> parameters, SpecificitiesTab parent) {
		super();

		this.itemPath = parameters.get(Keys.PATH);
		this.parentTab = parent;

		initializeFields(parameters);
	}

	@Override
	public void _initializeFields(final Map<String, String> parameters) {

		section1 = new HLayout();
		section1.setHeight(25);

		this.form = new DynamicForm();

		final SelectItem propertyCombo = new SelectPropertyItem(Keys.PROPERTY); // propriété de mot à utiliser pour construire la table lexicale. Valeur par défaut : Keys.WORD
		propertyCombo.setTitle(Services.getI18n().property());
		propertyCombo.setDefaultValue(Keys.WORD);
		// ask possible properties and fill gui component with result
		
		final SpinnerItem fminSpinner = new SpinnerItem(Keys.FMIN); // fréquence minimale des mots
		fminSpinner.setTitle("Fmin");
		fminSpinner.setValue(2);
		fminSpinner.setWriteStackedIcons(false);

		final ComboBoxItem thresholdModeCombo = new ComboBoxItem(Keys.THRESHOLD); // mode de seuillages utilisés à choisir parmis : Keys.NONE, Keys.SMIN ou Keys.TOPN. Valeur par défaut : Keys.TOPN
		thresholdModeCombo.setTitle(Services.getI18n().thresholdMode());
		thresholdModeCombo.setValueMap(SpecificitiesParam.thresholdValues);
		
		final SpinnerItem threasholdParameterSpinner = new SpinnerItem(Keys.THRESHOLD_PARAMETER); // réglage du seuil : taille maximale de laliste de mots ou valeur minimale de l'indice de spécificité
		threasholdParameterSpinner.setTitle(Services.getI18n().threshold());

		
		StaticTextItem columnsLabelItem = new StaticTextItem("columns_labels", Services.getI18n().columns());
		
		CheckboxItem checkboxFreq = new CheckboxItem(Keys.FREQUENCE, Services.getI18n().frequencyField()); // afficher la colonne des fréquence du mot
		CheckboxItem checkboxCoFreq = new CheckboxItem("cofreq", Services.getI18n().coFrequencyField()); // afficher la colonne des fréquences de mots
		CheckboxItem checkboxSpecif = new CheckboxItem(Keys.SPECIFICITY, Services.getI18n().score()); // afficher la colonne des indices de spécificité des mots

		ChangedHandler thresholdValueHandler = new ChangedHandler() {

			@Override
			public void onChanged(ChangedEvent event) {
				params.setThreshold(Double.parseDouble(options2Form.getValueAsString(Keys.THRESHOLD)));
				params.setFmin(Integer.parseInt(options2Form.getValueAsString(Keys.FMIN)));
				//				params.setVmax(Integer.parseInt(options1Form.getValueAsString(Keys.VMAX)));
				params.setNbWords(Integer.parseInt(options2Form.getValueAsString(Keys.THRESHOLD_PARAMETER)));

				params.setMode(mode);
				ExecTimer.start();
				Services.getSpecificitiesService().getLines(
						params,
						new AsyncCallback<Map<String, ArrayList<SpecificitiesResultLine>>>() {

							@Override
							public void onFailure(Throwable caught) {
								ProcessingWindow.hide();
								TxmDialog.info(Services.getI18n().specificitiesError(caught.getMessage()));
							}

							@Override
							public void onSuccess(Map<String, ArrayList<SpecificitiesResultLine>> result) {
								ProcessingWindow.hide();
								Number n = null;
								if (mode.equals(SpecificitiesParam.TOPN)) {
									n = params.getNbWords();
								} else {
									n = params.getThreshold();
								}
								parentTab.fillList(result, mode, n);
							}
						});

			}
		};
		threasholdParameterSpinner.setWidth("60 px");
		threasholdParameterSpinner.addChangedHandler(thresholdValueHandler);
		
		threasholdParameterSpinner.setDefaultValue(defaultTopN);
		thresholdModeCombo.setDefaultValue(SpecificitiesParam.TOPN);
		thresholdModeCombo.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				String selection = thresholdModeCombo.getValueAsString();
				mode = SpecificitiesParam.ALL;

				if (selection.equals(SpecificitiesParam.SMIN)) {
					mode = SpecificitiesParam.SMIN;
					options2Form.setValue(Keys.THRESHOLD_PARAMETER, defaultSmin);
				} else if (selection.equals(SpecificitiesParam.TOPN)) {
					mode = SpecificitiesParam.TOPN;
					options2Form.setValue(Keys.THRESHOLD_PARAMETER, defaultTopN);
				}

				threasholdParameterSpinner.setDisabled(mode == SpecificitiesParam.ALL);
			}
		});

		IButton validationButton = new IButton("ok");
		validationButton.setPrompt(Services.getI18n().startSpecif());
		validationButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				validate();
			}
		});

		ChangedHandler changeHandler = new ChangedHandler() {

			@Override
			public void onChanged(ChangedEvent event) {
				boolean b = (Boolean) event.getValue();
				if (b) {
					parentTab.showField(event.getItem().getName());
				} else {
					parentTab.hideField(event.getItem().getName());
				}
			}
		};
		IButton optionsButton = new IButton(Services.getI18n().settingsButton());
		optionsButton.setPrompt(Services.getI18n().openSettings());
		optionsButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (section3.isVisible()) {
					section3.hide();
				} else {
					section3.show();
				}
			}
		});

		IButton exportButton = new IButton(Services.getI18n().concExportButton());
		exportButton.setPrompt(Services.getI18n().openSettings());
		exportButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (parentTab.isFilled()) {
					Services.getSpecificitiesService().exportSpecificities(
							params,
							new AsyncCallback<String>() {

								@Override
								public void onFailure(Throwable caught) {
									ProcessingWindow.hide();
									TxmDialog.info(Services.getI18n().specificitiesError(caught.getMessage()));
								}

								@Override
								public void onSuccess(String result) {
									ProcessingWindow.hide();
									if (result == null || result.startsWith("error")) {
										TxmDialog.info(Services.getI18n().specificitiesError(result));
									} else {
										String url = GWT.getHostPageBaseURL() + result;
										Window.open(url, "", "");
										TxmDialog.exportWarning();
									}
								}
							});
				}
			}
		});

		// section2.hide();
		checkboxFreq.setValue(true);
		checkboxCoFreq.setValue(true);
		checkboxSpecif.setValue(true);

		checkboxFreq.addChangedHandler(changeHandler);
		checkboxCoFreq.addChangedHandler(changeHandler);
		checkboxSpecif.addChangedHandler(changeHandler);

		form.setItems(propertyCombo);

		options2Form.setNumCols(14);
		options2Form.setWidth100();
		options2Form.setItems(fminSpinner, thresholdModeCombo, threasholdParameterSpinner, columnsLabelItem, checkboxFreq, checkboxCoFreq, checkboxSpecif);

		section1.setLayoutAlign(VerticalAlignment.CENTER);
		form.setWidth("150 px");
		section1.setWidth("100%");
		section1.setMembers(form, validationButton, optionsButton, exportButton);

		section3 = new HLayout();
		section3.setWidth("100%");
		section3.setLayoutAlign(VerticalAlignment.CENTER);
		section3.setMembers(options2Form);

		this.setMembers(section1, section3);
		section3.setVisible(false);

		Services.getCorpusActionService().getWordProperties(itemPath, new AsyncCallback<List<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.warning(Services.getI18n().errorGetProperties(itemPath));
			}

			@Override
			public void onSuccess(List<String> result) {
				Collections.sort(result);
				propertyCombo.setValueMap(result.toArray(new String[result.size()]));

				parentTab.setParameters(parameters);

				validate();
			}
		});

	}

	public void setParameters(Map<String, String> parameters) {
		for (String property : parameters.keySet()) {
			if (Keys.FMIN.equals(property) || Keys.THRESHOLD.equals(property)  || Keys.THRESHOLD_PARAMETER.equals(property) || Keys.FREQUENCE.equals(property) || Keys.SPECIFICITY.equals(property)) { // fminSpinner, comboLimitMode, limitSpinner, checkboxFreq, checkboxCoFreq, checkboxSpecif
				options2Form.getItem(property).setValue(parameters.get(property));
			} else {
				setParameter(form, property, parameters.get(property));
			}
		}
	}

	private void validate() {
		ProcessingWindow.show(Services.getI18n().computing_specificities(form.getValueAsString(Keys.PROPERTY)));

		params.setFmin(Integer.parseInt(options2Form.getValueAsString(Keys.FMIN)));
		//		params.setVmax(Integer.parseInt(options2Form.getValueAsString(Keys.VMAX)));
		params.setThreshold(Double.parseDouble(options2Form.getValueAsString(Keys.THRESHOLD_PARAMETER)));
		params.setNbWords(Integer.parseInt(options2Form.getValueAsString(Keys.THRESHOLD_PARAMETER)));
		params.setMode(mode);
		params.setProperty(form.getValueAsString(Keys.PROPERTY));
		params.setPath(itemPath);

		try {
			ExecTimer.start();
			Services.getSpecificitiesService().specificities(params, new AsyncCallback<Boolean>() {

				@Override
				public void onSuccess(Boolean result) {
					if (!result) {
						onFailure(null);
					} else {
						Services.getSpecificitiesService().getLines(
								params,
								new AsyncCallback<Map<String, ArrayList<SpecificitiesResultLine>>>() {

									@Override
									public void onFailure(Throwable caught) {
										ProcessingWindow.hide();
										TxmDialog.info(Services.getI18n().specificitiesError(caught.getMessage()));
									}

									@Override
									public void onSuccess(Map<String, ArrayList<SpecificitiesResultLine>> result) {
										ProcessingWindow.hide();
										Number n = null;
										if (mode.equals(SpecificitiesParam.TOPN)) {
											n = params.getNbWords();
										} else if (mode.equals(SpecificitiesParam.SMIN)) {
											n = params.getThreshold();
										}  else {
											n = 0;
										}
										parentTab.fillList(result, mode, n);

									}
								});
					}
				}

				@Override
				public void onFailure(Throwable caught) {
					ProcessingWindow.hide();
					TxmDialog.info(Services.getI18n().specificitiesError(caught.getMessage()));
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
