package org.txm.web.client.widgets.tabs;

import java.util.Map;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.TxmHTMLPane;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.forms.TxmForm;
import org.txm.web.shared.Keys;

import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

/**
 * 
 * @author mdecorde
 *
 */
public class CMSTab extends TxmCenterTab {

	ToolStrip bar;
	Label pathLabel;
	ToolStripButton goUpButton;
	
	TxmHTMLPane content;
	String path;
	String locale;
	String profile;
	String title = null;
	
	/**
	 * 
	 * @param name
	 * @param profile optional
	 * @param locale optional
	 * @param title Tab title, optional
	 * @param tooltip Tab tooltip, optional
	 */
	public CMSTab(Map<String, String> parameters) {
		
		String name = parameters.get(Keys.PATH);
		String profile = parameters.get(Keys.PROFILE);
		String locale = parameters.get(Keys.LOCALE);
		String title  = parameters.get(Keys.TITLE);
		String tooltip = parameters.get(Keys.TOOLTIP);
		Boolean navigation = "true".equals(parameters.get(Keys.NAVIGATION));
		
		//SC.say("name="+name+" profile="+profile+" locale="+locale+" title="+title+" tooltip="+tooltip);
		
		this.setName("CMS");
		this.path = name;
		if (!path.startsWith("/")) this.path = "/"+path;
		this.locale = locale;
		this.profile = profile;
		this.title = title;
		
		if (tooltip != null) {
			this.setPrompt(tooltip);
		}
		
		bar = new ToolStrip();
		bar.setWidth100();
		
		pathLabel = new Label("");
		pathLabel.setWidth100();
		pathLabel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				update(path);
			}
		});
		
		goUpButton = new ToolStripButton("<");
		goUpButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//TxmDialog.warning("CURRENT="+path);
				int idx = path.lastIndexOf("/");
				if (idx > 0) path = path.substring(0, idx);
				//TxmDialog.warning("NEW="+path);
				update();
			}
		});
		bar.addSpacer(5);
		bar.addMember(goUpButton);
		bar.addMember(pathLabel);
		
		content = new TxmHTMLPane(this);
		//content.setPadding(10);
		content.setContentsType(ContentsType.FRAGMENT);
		content.setHeight100();
		content.setWidth100();
		content.setMargin(5);
		VLayout mainPanel = new VLayout();
		if (navigation) {
			mainPanel.addMember(bar);
		}
		mainPanel.addMember(content);
		
		this.setPane(mainPanel);
		super.setTitle(title);
		super.setName(name);
	}
		
	public void setTitle(String newTitle) {
		this.title = newTitle;
	}
	
	public void refreshTitle() {
		
		if (title == null || title.length() == 0) {		
			int idx = path.lastIndexOf("/");
			if (idx >= 0) super.setTitle(path.substring(idx+1));
			else super.setTitle(path);
		} else {
			super.setTitle(title);
		}
	}
	
	public void setContentUrl(String filepath) {
		content.setContentsURL(filepath);
	}

	public void update(String path) {
		if (path != null && path.length() > 0) {
			if (path.endsWith("/")) path = path.substring(0, path.length() -1);
			this.path = path;
			update();
		}
	}
	
	public void update() {
		TXMWEB.log("Loading "+path+" ...");
		String locale = LocaleInfo.getCurrentLocale().getLocaleName();
		//System.out.println("get page for path="+path+" locale="+locale+" profile="+profile);
		Services.getUIService().getUserPage(path, locale, new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				content.setContents(Services.getI18n().errorWith(caught));
				TXMWEB.log("");
			}
			
			@Override
			public void onSuccess(String result) {
				TXMWEB.log("");
				if (result.trim().equals("404")) { // the page is stored in the public area, use HTTP protocole
					//System.out.println("PATH="+path);
					if (path.endsWith(".html") || path.endsWith(".jsp")) {
						content.setContentsURL(path); // result is a real HTTP path
					} else {
						content.setContentsURL("html"+path+".jsp");
					}
				} else if (result.trim().equals("500")) { // tomcat cannot access to the private "data/html" folder
					TxmDialog.severe("Error: server could not read private folder. Please contact admin.");
				} else if (result.trim().equals("profile")) {
					TxmDialog.severe("Error: user has no profile");
				} else { // the page is stored in the private area
					content.setContentsURL(result);
					//TxmDialog.severe("SET URL="+result);
				}
				pathLabel.setContents(path);
				refreshTitle();
			}
		});
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub
		
	}
	
	public TxmForm getForm() {
		return null;
	}
}
