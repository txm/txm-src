package org.txm.web.client.widgets.menus;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;

import com.smartgwt.client.core.KeyIdentifier;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.ClickHandler;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;

public class TabContextMenu extends Menu {

	public TabContextMenu() {
		this.setWidth(140);

		MenuItemIfFunction enableCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				int selectedTab = TXMWEB.getCenterPanel().getSelectedTabNumber();
				return selectedTab != 0;
			}
		};

		MenuItem closeItem = new MenuItem(Services.getI18n().tabContextMenuClose());
		closeItem.setEnableIfCondition(enableCondition);
		closeItem.setKeyTitle("Alt+C");
		KeyIdentifier closeKey = new KeyIdentifier();
		closeKey.setAltKey(true);
		closeKey.setKeyName("C");
		closeItem.setKeys(closeKey);
		closeItem.addClickHandler(new ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				int selectedTab = TXMWEB.getCenterPanel().getSelectedTabNumber();
				TXMWEB.getCenterPanel().removeTab(selectedTab);
				TXMWEB.getCenterPanel().selectTab(selectedTab - 1);
			}
		});

		MenuItem closeAllButCurrent = new MenuItem(Services.getI18n().tabContextMenuCloseAllButCurrent());
		closeAllButCurrent.setEnableIfCondition(enableCondition);
		closeAllButCurrent.addClickHandler(new ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				int selected = TXMWEB.getCenterPanel().getSelectedTabNumber();
				Tab[] tabs = TXMWEB.getCenterPanel().getTabs();
				int[] tabsToRemove = new int[tabs.length - 2];
				int cnt = 0;
				for (int i = 1; i < tabs.length; i++) {
					if (i != selected) {
						tabsToRemove[cnt] = i;
						cnt++;
					}
				}
				TXMWEB.getCenterPanel().removeTabs(tabsToRemove);
			}
		});

		MenuItem closeAll = new MenuItem(Services.getI18n().tabContextMenuCloseAll());
		closeAll.setEnableIfCondition(enableCondition);
		closeAll.addClickHandler(new ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				Tab[] tabs = TXMWEB.getCenterPanel().getTabs();
				int[] tabsToRemove = new int[tabs.length - 1];

				for (int i = 1; i < tabs.length; i++) {
					tabsToRemove[i - 1] = i;
				}
				TXMWEB.getCenterPanel().removeTabs(tabsToRemove);
				TXMWEB.getCenterPanel().selectTab(0);
			}
		});

		this.setItems(closeItem, closeAllButCurrent, closeAll);
	}
}
