package org.txm.web.client.widgets.tabs;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.widgets.TxmHTMLPane;

import com.smartgwt.client.widgets.layout.VLayout;

public class DoubleHtmlTab extends TxmCenterTab{
	TxmHTMLPane mainPanel;
	TxmHTMLPane secondPanel;

	public DoubleHtmlTab(String title)
	{
		super(title);
		VLayout layout = new VLayout();
		layout.setHeight("100%");

		if (TXMWEB.SHOWPUBLICPAGES){
			mainPanel = new TxmHTMLPane();
			layout.addMember(mainPanel);
		}

		if (TXMWEB.SHOWPRIVATEPAGES)
		{
			secondPanel = new TxmHTMLPane();
			layout.addMember(secondPanel);
		}
		
		if(TXMWEB.SHOWPRIVATEPAGES && TXMWEB.SHOWPUBLICPAGES)
			mainPanel.setShowResizeBar(true);

		this.setPane(layout);
	}

	public void updateSecondPage(String homePagePath) {
		if (TXMWEB.SHOWPRIVATEPAGES)
			secondPanel.setContentsURL(homePagePath);
	}

	public void updateMainPage(String homePagePath) {
		if (TXMWEB.SHOWPUBLICPAGES)
			mainPanel.setContentsURL(homePagePath);
	}

	public void setHeights(String first, String second)
	{
		if (TXMWEB.SHOWPUBLICPAGES)
			mainPanel.setHeight(first);
		if (TXMWEB.SHOWPRIVATEPAGES)
			secondPanel.setHeight(second);
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub
		
	}
}
