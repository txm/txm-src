package org.txm.web.client.widgets.forms;

import java.util.ArrayList;
import java.util.Map;

import org.txm.web.client.ExecTimer;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.tabs.IndexTab;
import org.txm.web.exceptions.PermissionException;
import org.txm.web.shared.Keys;
import org.txm.web.shared.VocabularyParam;
import org.txm.web.shared.VocabularyResult;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class LexiconForm extends IndexForm {

	public LexiconForm(Map<String, String> parameters, ListGridRecord elem, IndexTab parent) {
		super(parameters, elem, parent);

		propertyField.setMultiple(false);
		this.queryField.setVisible(false);
		this.assistantButton.setVisible(false);
		this.validationButton.setTitle(Services.getI18n().computeButton());
		this.validationButton.setPrompt(Services.getI18n().startLexicon());
	}

	public void validate() {

		params.setQuery("[]");
		params.setSort(form.getItem(Keys.SORT).getValue().toString());
		try {
			params.filterFmin = Integer.parseInt(form.getValueAsString(Keys.FMIN));
			params.filterFmax = Integer.parseInt(form.getValueAsString(Keys.FMAX));
			params.filterVmax = Integer.parseInt(form.getValueAsString(Keys.VMAX));
			params.filterNbPerPage = parentTab.getNbResultPerPage();
		}
		catch(Exception e){
			e.printStackTrace();
			TxmDialog.warning(Services.getI18n().indexErrorInteger());
		}
		params.startindex = parentTab.startindex;

		params.setProperties(new ArrayList<String>());
		String properties = form.getItem(Keys.PROPERTIES).getValue().toString();
		if (properties != null) {
			for (String s : properties.split(",")) {
				params.getProperties().add(s);
				break;
			}
		} else {
			params.getProperties().add(Keys.WORD);
		}

		ProcessingWindow.show(Services.getI18n().computing_lexicon(params.getProperties().toString()));
		ExecTimer.start();
		Services.getVocabularyService().index(params, new AsyncCallback<VocabularyResult>() {
			@Override
			public void onSuccess(VocabularyResult result) {
				ProcessingWindow.hide();
				parentTab.fillList(result);
			}
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				if (caught instanceof PermissionException) {
					TxmDialog.warning(Services.getI18n().indexPermissionException());
				} else {
					TxmDialog.warning(Services.getI18n().indexError());
				}
			}
		});

	}	

	public VocabularyParam getParams() {
		return params;
	}

	public void setParams(VocabularyParam params) {
		this.params = params;
	}
}
