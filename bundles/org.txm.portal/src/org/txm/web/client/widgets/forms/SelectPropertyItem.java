package org.txm.web.client.widgets.forms;

import com.smartgwt.client.widgets.form.fields.SelectItem;

public class SelectPropertyItem extends SelectItem {
	public SelectPropertyItem(String name) {super(name);}
	public SelectPropertyItem(String name, String label) {super(name, label);}
}
