package org.txm.web.client.widgets.menus;

import java.util.ArrayList;
import java.util.List;

import org.txm.web.client.Commands;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.TxmMenuBar;
import org.txm.web.shared.Actions;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
import com.smartgwt.client.widgets.toolbar.ToolStripSeparator;

/**
 * A set of buttons for the Top toolbar
 * 
 * @author mdecorde
 *
 */
public class FunctionsToolbar {
	
	private Label currentItem = new Label("");
	private ToolStripButton createSubcorpus = new ToolStripButton();
	private ToolStripButton createPartition = new ToolStripButton();
	private ToolStripButton documentation = new ToolStripButton();
	private ToolStripButton download = new ToolStripButton();
	private ToolStripButton properties = new ToolStripButton();
	private ToolStripButton index = new ToolStripButton();
	private ToolStripButton referencer = new ToolStripButton();
	private ToolStripButton lexicon = new ToolStripButton();
	private ToolStripButton concordances = new ToolStripButton();
	private ToolStripButton tsquery = new ToolStripButton();
	private ToolStripButton contexts = new ToolStripButton();
	private ToolStripButton specificities = new ToolStripButton();
	private ToolStripButton delete = new ToolStripButton();
	//private ToolStripButton edition = new ToolStripButton();
	private ToolStripButton texts = new ToolStripButton();
	private ToolStripButton textSelection = new ToolStripButton();
	private ToolStripButton cooccurrences = new ToolStripButton();
	private ToolStripButton afc = new ToolStripButton();

	private ToolStripSeparator separator1 = new ToolStripSeparator();
	private ToolStripSeparator separator2 = new ToolStripSeparator();
	private ToolStripSeparator separator3 = new ToolStripSeparator();
	private ToolStripSeparator separator4 = new ToolStripSeparator();

	String path = "";
	private ListGridRecord elem;

	public FunctionsToolbar() {
		init();
		this.createSubcorpus.setID("subcorpusButton");
		this.createPartition.setID("partitionButton");
		this.documentation.setID("documentationButton");
		this.download.setID("downloadButton");
		this.properties.setID("propertiesButton");
		this.index.setID("indexButton");
		this.referencer.setID("referencerButton");
		this.lexicon.setID("dictionnaryButton");
		this.concordances.setID("concordanceButton");
		this.tsquery.setID("tsqueryButton");
		this.contexts.setID("contextButton");
		this.specificities.setID("specificityButton");
		this.delete.setID("deleteButton");
		this.texts.setID("textsButton");
		this.textSelection.setID("textSelectionButton");
		this.cooccurrences.setID("cooccurrenceButton");
		this.afc.setID("afcButton");	
	}

	public void refreshEnables() {
		
		//ListGridRecord elem, String path, List<Actions> allowedActions
		
		String split[] = TXMWEB.getSideNavTree().getSelectedPaths().split("\"");
		System.out.println("Menubar refreshing with path="+TXMWEB.getSideNavTree().getSelectedPaths());
		if (split.length < 2) {
			System.out.println("No selection");
			ArrayList<Actions> result = new ArrayList<Actions>();
			TXMWEB.getMenuBar().updateToolbarPrefix("");
			refreshEnables(result, "", null);
			return;
		}
		path = split[1];
		
		String path2 = path;
		int idx = path2.lastIndexOf("/");
		if (idx >= 0) path2 = path2.substring(idx+1); // remove parent path
		idx = path2.lastIndexOf(":");
		if (idx >= 0) path2 = path2.substring(idx+1); // remove user namespace
		
		TXMWEB.getMenuBar().updateToolbarPrefix(path2);
		
		elem = TXMWEB.getSideNavTree().getSelectedRecord();

		Services.getUIService().getContextMenu(path, new AsyncCallback<List<Actions>>() {
			@Override
			public void onFailure(Throwable caught) {	}
			@Override
			public void onSuccess(List<Actions> result) {
				refreshEnables(result, path, elem);
			}
		});
	}

	public void refreshEnables(List<Actions> result, String path, ListGridRecord elem) {
		this.path = path;
		this.elem = elem;
		int i = 0; // count the number of shown elements, and show if necessary the separator
	
		if (!result.contains(Actions.DOCUMENTATION)) {
			if (documentation.isVisible()) documentation.hide();
		} else {
			if (!documentation.isVisible()) documentation.show();
			i++;
		}

		if (result.contains(Actions.TEXTS)) {
			//if (!texts.isVisible()) 
				texts.show();
				i++;
		} else {
			//if (texts.isVisible()) 
				texts.hide();
		}

		if (!result.contains(Actions.PROPERTIES)) {
			if (properties.isVisible()) properties.hide();
		} else {
			if (!properties.isVisible()) properties.show();
			i++;
		}
		
		//test to show first separator
		if (i > 0) {
			//if (!separator1.isVisible()) 
				separator1.show();
		} else {
			//if (separator1.isVisible()) 
				separator1.hide();
		}
		i = 0;
		

		if (!result.contains(Actions.DELETE)) {
			if (delete.isVisible()) delete.hide();
		} else	{
			if (!delete.isVisible())  delete.show();
			i++;
		}
		
		

		//test to show second separator
		if (i > 0) {
			//if (!separator2.isVisible()) 
				separator2.show();
		} else {
			//if (separator2.isVisible()) 
				separator2.hide();
		}
		i = 0;
		
		if (!result.contains(Actions.CREATESUBCORPUS)) { 
			if (createSubcorpus.isVisible()) createSubcorpus.hide();
		} else {
			if (!createSubcorpus.isVisible()) createSubcorpus.show();
			i++;
		}

		if (!result.contains(Actions.CREATEPARTITION)) {
			if (createPartition.isVisible()) createPartition.hide();
		} else {
			if (!createPartition.isVisible()) createPartition.show();
			i++;
		}

		if (!result.contains(Actions.TEXTSELECTION)) {
			if (textSelection.isVisible()) textSelection.hide();
		} else {
			if (!textSelection.isVisible()) textSelection.show();
			i++;
		}

		//test to show third separator
		if (i > 0) {
			//if (!separator3.isVisible()) 
				separator3.show();
		} else {
			//if (separator3.isVisible()) 
				separator3.hide();
		}
		i = 0;
		
		if (!result.contains(Actions.INDEX)) { 
			if (index.isVisible()) index.hide();
		} else	{
			if (!index.isVisible()) index.show();
			i++;
		}

		if (!result.contains(Actions.REFERENCER)) {
			if (referencer.isVisible()) referencer.hide();
		} else	{
			if (!referencer.isVisible()) referencer.show();
			i++;
		}

		if (!result.contains(Actions.LEXICON)) {
			if (lexicon.isVisible()) lexicon.hide();
		} else	{
			if (!lexicon.isVisible()) lexicon.show();
			i++;
		}

		if (!result.contains(Actions.CONCORDANCE)) {
			if (concordances.isVisible()) concordances.hide();
			if (contexts.isVisible()) contexts.hide();
		} else {
			if (!concordances.isVisible()) concordances.show();
			if (!contexts.isVisible()) contexts.show();
			i++;
		}

		if (!result.contains(Actions.TSQUERY)) {
			if (tsquery.isVisible()) tsquery.hide();
		} else {
			if (!tsquery.isVisible()) tsquery.show();
			i++;
		}

		if (!(result.contains(Actions.SPECIFICITIES) && TXMWEB.RENABLE )) {
			if (specificities.isVisible()) specificities.hide();
		} else {
			if (!specificities.isVisible()) specificities.show();
			i++;
		}

		if (!(result.contains(Actions.COOCCURRENCE) && TXMWEB.RENABLE)) {
			if (cooccurrences.isVisible()) cooccurrences.hide();
		} else {
			if (!cooccurrences.isVisible()) cooccurrences.show();
			i++;
		}

		if(!(result.contains(Actions.AFC) && TXMWEB.RENABLE)) {
			if (afc.isVisible()) afc.hide();
		} else	{
			if (!afc.isVisible()) afc.show();
			i++;
		}
		//test to show third separator
		if (i > 0) {
			//if (!separator3.isVisible()) 
				separator3.show();
		} else {
			//if (separator3.isVisible()) 
				separator3.hide();
		}
		i = 0;
		
		
		if (!result.contains(Actions.DOWNLOAD)) {
			if (download.isVisible()) download.hide();
		} else {
			if (!download.isVisible()) download.show();
			i++;
		}
	}

	public void init() {
		currentItem.setWidth(10);
		//set icons
		createSubcorpus.setIcon("icons/functions/SubCorpus.png");
		createPartition.setIcon("icons/functions/Partition.png");
		properties.setIcon("icons/functions/Diagnostique.png");
		documentation.setIcon("icons/house.png");
		download.setIcon("icons/download.png");
		index.setIcon("icons/functions/Vocabulary.png");
		referencer.setIcon("icons/functions/Referencer.png");
		lexicon.setIcon("icons/functions/VocabularyP.png");
		concordances.setIcon("icons/functions/Concordances.png");
		cooccurrences.setIcon("icons/functions/Cooccurrences.png");
		tsquery.setIcon("icons/functions/TS.png");
		contexts.setIcon("icons/functions/Contexts.png");
		specificities.setIcon("icons/functions/Specificities.png");
		afc.setIcon("icons/functions/CorrespondanceAnalysis.png");
		delete.setIcon("icons/functions/Delete.png");
		//edition.setIcon("icons/functions/Edition.png");
		texts.setIcon("icons/functions/Edition.png");
		textSelection.setIcon("icons/functions/Base.png");
		
		createSubcorpus.setTooltip(Services.getI18n().open_subcorpus_dialog());
		createPartition.setTooltip(Services.getI18n().open_partition_dialog());
		properties.setTooltip(Services.getI18n().properties());
		documentation.setTooltip(Services.getI18n().homeTab());
		download.setTooltip(Services.getI18n().download());
		index.setTooltip(Services.getI18n().open_index_tab());
		referencer.setTooltip(Services.getI18n().open_references_tab());
		lexicon.setTooltip(Services.getI18n().open_lexicon_tab());
		concordances.setTooltip(Services.getI18n().open_conc_tab());
		cooccurrences.setTooltip(Services.getI18n().coocurrence());
		tsquery.setTooltip(Services.getI18n().open_ts_tab());
		contexts.setTooltip(Services.getI18n().open_context_tab());
		specificities.setTooltip(Services.getI18n().specificity());
		afc.setTooltip(Services.getI18n().afc());
		delete.setTooltip(Services.getI18n().delete());
		//edition.setTooltip(Services.getI18n().open_edition_tab());
		texts.setTooltip(Services.getI18n().open_biblio_tab());
		textSelection.setTooltip(Services.getI18n().open_textsel_tab());
		
//		createSubcorpus.setID("MENUBAR_SUBCORPUS"); // selenium
//		createPartition.setID("MENUBAR_PARTITION"); // selenium
//		properties.setID("MENUBAR_PROPERTIES"); // selenium
//		documentation.setID("MENUBAR_DOCUMENTATION"); // selenium
//		index.setID("MENUBAR_INDEX"); // selenium
//		referencer.setID("MENUBAR_REFERENCER"); // selenium
//		dictionnary.setID("MENUBAR_LEXICON"); // selenium
//		concordance.setID("MENUBAR_CONCORDANCE"); // selenium
//		cooccurrence.setID("MENUBAR_COOCCURENCE"); // selenium
//		tsquery.setID("MENUBAR_TS"); // selenium
//		context.setID("MENUBAR_CONTEXT"); // selenium
//		specificity.setID("MENUBAR_SPECIFICITE"); // selenium
//		afc.setID("MENUBAR_AFC"); // selenium
//		delete.setID("MENUBAR_DELETE"); // selenium
//		edition.setID("MENUBAR_EDITION"); // selenium
//		texts.setID("MENUBAR_BIBLIO"); // selenium
//		textSelection.setID("MENUBAR_SELECTION"); // selenium

		//set handlers
		setDeleteHandler(delete);
		setCreateSubcorpusHandler(createSubcorpus);
		setCreatePartitionHandler(createPartition);
		setCreateConcordancesHandler(concordances);
		setCreateTsQueryHandler(tsquery);
		setCreatePropertiesHandler(properties);
		setCreateDocumentationHandler(documentation);
		setCreateDownloadHandler(download);
		setCreateReferencerHandler(referencer);
		setCreateIndexHandler(index);
		setCreateLexiconHandler(lexicon);
		setCreateContextsHandler(contexts);
		//setCreateEditionHandler(edition);
		setCreateTextsHandler(texts);
		setCreateTextSelectionHandler(textSelection);		
		setCreateSpecificitiesHandler(specificities);
		setCreateCooccurrencesHandler(cooccurrences);
		setCreateAFCHandler(afc);
	}

	public void addButtonsTo(TxmMenuBar bar)
	{
		bar.addMember(currentItem); // add space of 10px
		//bar.addMember(separator1);
		bar.addButton(documentation);
		bar.addButton(texts);
		//bar.addButton(edition);
		bar.addButton(properties);
		bar.addMember(separator1);
		
		bar.addButton(delete);
		bar.addMember(separator2);
		
		bar.addButton(textSelection);
		bar.addButton(createSubcorpus);
		bar.addButton(createPartition);	
		bar.addMember(separator3);
		
		bar.addButton(lexicon);
		bar.addButton(index);
		bar.addButton(concordances);
		bar.addButton(contexts);
		bar.addButton(cooccurrences);
		bar.addButton(referencer);
		bar.addButton(tsquery);
		bar.addButton(specificities);
		bar.addButton(afc);
		bar.addMember(separator4);
		bar.addButton(download);
		
		createSubcorpus.hide();
		createPartition.hide();
		documentation.hide();
		download.hide();
		properties.hide();
		index.hide();
		referencer.hide();
		lexicon.hide();
		concordances.hide();
		cooccurrences.hide();
		tsquery.hide();
		contexts.hide();
		specificities.hide();
		afc.hide();
		delete.hide();
		//edition.hide();
		texts.hide();
		textSelection.hide();
		separator1.hide();
		separator2.hide();
		separator3.hide();
		separator4.hide();
	}

	private void setDeleteHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				TXMWEB.log(Services.getI18n().delete()+path);
				Commands.callRemove(Commands.createParameters(path));
			}
		});
	}

	private void setCreateTsQueryHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callTS(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreatePropertiesHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callProperties(Commands.createParameters(path), elem);
			}
		});
	}
	
	private void setCreateDocumentationHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callDocumentation(Commands.createParameters(path));
			}
		});
	}
	
	private void setCreateDownloadHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callDownload(Commands.createParameters(path));
			}
		});
	}

	private void setCreateSubcorpusHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callSubCorpus(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreatePartitionHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callPartition(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreateConcordancesHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callConcordances(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreateContextsHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callContexts(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreateIndexHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callIndex(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreateLexiconHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callLexicon(Commands.createParameters(path),  elem);
			}
		});
	}

	private void setCreateEditionHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callEdition(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreateTextsHandler(ToolStripButton item) {
	
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callTexts(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreateTextSelectionHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callSelection(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreateReferencerHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callReferencer(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreateSpecificitiesHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callSpecificities(Commands.createParameters(path), elem);
			}
		});	
	}

	private void setCreateCooccurrencesHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callCooccurrences(Commands.createParameters(path), elem);
			}
		});	
	}
	
	private void setCreateAFCHandler(ToolStripButton item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commands.callAFC(Commands.createParameters(path), elem);
			}
		});	
	}
}