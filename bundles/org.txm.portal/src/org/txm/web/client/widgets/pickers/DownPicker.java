package org.txm.web.client.widgets.pickers;

import com.smartgwt.client.widgets.form.fields.FormItemIcon;

public class DownPicker extends FormItemIcon {

	public DownPicker() {
		super();
		
		setSrc("pickers/down.png");
		
		setShowOver(true);
	}
}
