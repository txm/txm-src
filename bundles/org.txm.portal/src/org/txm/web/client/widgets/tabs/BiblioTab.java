package org.txm.web.client.widgets.tabs;

import org.txm.web.client.Commands;
import org.txm.web.client.services.Services;
import org.txm.web.shared.Keys;

import com.smartgwt.client.widgets.Canvas;

public class BiblioTab extends CMSTab {

	public BiblioTab(String bibliopath, String textid) {
		super(Commands.createParameters(
				Keys.PATH, bibliopath, 
				Keys.PROFILE, null, 
				Keys.LOCALE, null, 
				Keys.TITLE, Canvas.imgHTML("icons/functions/Biblio.png", 16, 16) + "&nbsp;"+"<span style=\"vertical-align:-2px;\">" + textid + "</span>", 
				Keys.TOOLTIP, Services.getI18n().biblioShort(textid)));
		
		this.setName("BIBLIO");
	}
	
	public void update() { // TODO move the biblio files from "biblios/textid.html" to "html/CORPUSNAME/biblios/textid.jsp"
		//TxmDialog.severe("PATH="+path);
		this.setContentUrl(path);
	}
}
