package org.txm.web.client.widgets.forms;

import java.util.Map;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.shared.UserData;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;

public class ProfileForm extends TxmWindow {

	private String password = "";
	private String newpassword = "";
	
	private String oldmail = "";
	private String mail = "";
	private String phone = "";
	private String firstname = "";
	private String lastname = "";
	private String status = "";
	private String institution = "";
	
	DynamicForm passwordform;

	PasswordItem passwordField;
	PasswordItem newpasswordField1;
	PasswordItem newpasswordField2;
	
	TextItem mailField;
	TextItem firstnameField;
	TextItem lastnameField;
	TextItem statusField;
	TextItem institutionField;
	TextItem phoneField;
	
	public ProfileForm() {
		this(null);
	}
	
	public ProfileForm(Map<String, String> parameters) {
		super(true, "");
		
		initializeFields(parameters);
		setParameters(parameters); // do it here because no Tab will do it
		
		// fill the Profile fields with thos stored on server -> the suer can update them
		Services.getAuthenticationService().getUserData(new AsyncCallback<UserData>() {
			@Override
			public void onSuccess(UserData result) {
				
				mailField.setDefaultValue(result.mail);
				oldmail = result.mail;
				firstnameField.setDefaultValue(result.firstname);
				lastnameField.setDefaultValue(result.lastname);
				phoneField.setDefaultValue(result.phone);
				statusField.setDefaultValue(result.status);
				institutionField.setDefaultValue(result.institution);
			}
			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.severe(Services.getI18n().internalError()+caught);
			}
		});
	}
	
	@Override
	public void _initializeFields(Map<String, String> parameters) {
		this.setTitle(Services.getI18n().profileButton());
		
		passwordform = new DynamicForm();
		passwordform.setGroupTitle(Services.getI18n().changePassword());
		passwordform.setIsGroup(true); 
		passwordform.setColWidths("20%","80%");
		passwordform.setWidth100();
		passwordField = new PasswordItem();
		passwordField.setName("Password");
		newpasswordField1 = new PasswordItem();
		newpasswordField1.setName("NewPassword1");
		newpasswordField2 = new PasswordItem();
		newpasswordField2.setName("NewPassword2");
		
		form = new DynamicForm();
		form.setGroupTitle(Services.getI18n().changePersonalInfos());
		form.setIsGroup(true); 
		form.setColWidths("20%","80%");
		form.setWidth100();
		mailField = new TextItem();
		mailField.setName("Mail");
		firstnameField = new TextItem();
		firstnameField.setName("FirstName");
		lastnameField = new TextItem();
		lastnameField.setName("LastName");
		statusField = new TextItem();
		statusField.setName("Statut");
		institutionField = new TextItem();
		institutionField.setName("Institution");
		phoneField = new TextItem();
		phoneField.setName("Phone");

	    IButton validateButton = new IButton(Services.getI18n().save_changes());
	    validateButton.setAutoFit(true);
	    validateButton.setPrompt(Services.getI18n().updateUserProfile());
		IButton cancelButton = new IButton(Services.getI18n().closeButton());
		cancelButton.setPrompt(Services.getI18n().cancelAndcloseWindow());
		
		form.setAutoHeight();
		form.setPadding(10);
		passwordform.setAutoHeight();
		passwordform.setPadding(10);
		
		passwordField.setTitle(Services.getI18n().old_password());
		newpasswordField1.setTitle(Services.getI18n().new_password());
		newpasswordField2.setTitle(Services.getI18n().confirm_password());
		//loginField.setTitle(Services.getI18n().login());
		mailField.setTitle(Services.getI18n().email());
		firstnameField.setTitle(Services.getI18n().firstName());
		lastnameField.setTitle(Services.getI18n().lastName());
		statusField.setTitle(Services.getI18n().status());
		institutionField.setTitle(Services.getI18n().institution());
		phoneField.setTitle(Services.getI18n().phone());
		
		validateButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				validate();
			}
		});
		cancelButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				destroy();
			}
		});
//		this.addCloseClickHandler(new com.smartgwt.client.widgets.events.CloseClickHandler() {
//			@Override
//			public void onCloseClick(CloseClientEvent event) {
//				destroy();
//			}
//		});
		
		HLayout buttons = new HLayout(5);
		buttons.setMargin(7);
		buttons.setAutoHeight();
		buttons.setWidth100();
		buttons.setMembers(validateButton, cancelButton);
		
		passwordform.setFields(passwordField, newpasswordField1, newpasswordField2);
		form.setFields( mailField, firstnameField, lastnameField, statusField, institutionField, phoneField);
		//form.setDataSource(fields);
		this.addItem(passwordform);
		this.addItem(form);
		this.addItem(buttons);
		//RootPanel.get().add(this);
		this.centerInPage();
		this.show();
	}

	protected void validate() {
		//username = form.getValueAsString(Keys.LOGIN);
		password = passwordform.getValueAsString("Password");
		mail = form.getValueAsString("Mail");
		firstname = form.getValueAsString("FirstName");
		if (firstname == null)
			firstname = "";
		
		lastname = form.getValueAsString("LastName");
		if (lastname == null)
			lastname = "";
		
		phone = form.getValueAsString("Phone");
		if (phone == null)
			phone = "";
		
		status = form.getValueAsString("Statut");
		if (status == null)
			status = "";
		
		institution = form.getValueAsString("Institution");
		if (institution == null)
			institution = "";
		
		newpassword = null;
		String pass1 = passwordform.getValueAsString("NewPassword1");
		String pass2 = passwordform.getValueAsString("NewPassword2");
		boolean different = false;
		
		if (pass1 != null && pass2 != null && pass1.trim().length() > 0 && pass1.trim().length() > 0)
		{
			if (!pass1.equals(pass2)) {
				different = true;
			} else {
				newpassword = pass1.trim();
			}
		} else if (pass1 == null && pass2 == null) {
			different = false;
		} else {
			different = true;
		}
		
		UserData data = new UserData("", password, "", mail, firstname, lastname, "", status, institution, phone);
		data.newpassword = newpassword;
		//System.out.println("USERDATA: "+data);
				
		if (mail == null) {
			TxmDialog.warning(Services.getI18n().mail_mandatory());
			return;
		} else if (!UserData.checkMail(mail)) {
			TxmDialog.warning(Services.getI18n().mailNotValid());
			return;
		}
		
		//System.out.println("Password: "+password);
		if (password != null && password.trim().length() > 0) //user wants to change his password
		{
			if (different) {
				TxmDialog.warning(Services.getI18n().newPasswordDiffersFromConfirm());
				return;
			} else if (newpassword.length() < 8) {
				TxmDialog.warning(Services.getI18n().newPasswordTooWeak());
				return;
			}
		}

		Services.getAuthenticationService().changeUserData(data, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				if (result == null) {
					TxmDialog.warning(Services.getI18n().wrongPasswordChangeUserData());
				} else if(result.length() > 0) {
					if (result.startsWith("mail"))
						result = Services.getI18n().couldNotSendConfirmMail();
					else if (result.startsWith("wrongpassword"))
						result = Services.getI18n().wrongPasswordChangeUserData();
					else if (result.startsWith("expired"))
						result = Services.getI18n().youAreNotConnected();
					else if(result.startsWith("password"))
						result = Services.getI18n().passwordTooWeak();
					else if (result.startsWith("mailformat"))
						result = Services.getI18n().mailNotValid();
					else if ("user not saved".equals(result))
						result = "Error: could not save user data. Please contact admin.";
					TxmDialog.warning(result);
					TXMWEB.getConsole().addMessage(result);
				} else {
					passwordField.setDefaultValue("");
					newpasswordField1.setDefaultValue("");
					newpasswordField2.setDefaultValue("");
					TXMWEB.getMenuBar().updateMenuBar();
					TXMWEB.getConsole().addMessage(Services.getI18n().user_profile_updated());
					if (!oldmail.equals(mail) && TXMWEB.MAILENABLE)
						TxmDialog.info(Services.getI18n().mailchanged(mail));
					destroy();
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.severe(Services.getI18n().errorChangeUserData());
			}
		});
	}
}
