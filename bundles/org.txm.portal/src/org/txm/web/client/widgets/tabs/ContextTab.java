package org.txm.web.client.widgets.tabs;

import java.util.ArrayList;
import java.util.Map;

import org.txm.web.client.ExecTimer;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.EditionPanel;
import org.txm.web.client.widgets.EnhancedListGrid;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.forms.ConcordanceForm;
import org.txm.web.client.widgets.forms.NavigationToolStrip;
import org.txm.web.client.widgets.forms.TxmForm;
import org.txm.web.shared.ConcordanceResult;
import org.txm.web.shared.ConcordanceResultLine;
import org.txm.web.shared.Keys;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.grid.events.CellDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.CellDoubleClickHandler;
import com.smartgwt.client.widgets.grid.events.GridRowColEvent;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class ContextTab extends TxmCenterTab implements ConcordancePresentation {

	protected static boolean firstExportWarning = true;

	private VLayout concordanceResultContent = new VLayout();

	private int total = 0;
	//private Label totalLabel = new Label();
	NavigationToolStrip navigation;
	private EnhancedListGrid resultList = new EnhancedListGrid();

	private ConcordanceForm form = null;
	
	public TxmForm getForm() {
		return this.form;
	}

	private boolean firstWarning;

	private ListGridRecord elem;
	protected boolean computeAtOpening = false;
	
	public boolean getComputeAtOpening() {
		return computeAtOpening;
	}
	public void setTitle(String title) {
		super.setTitle(Canvas.imgHTML("icons/functions/Contexts.png", 16, 16)+"&nbsp;"+"<span style=\"vertical-align:-2px;\">"+title+"</span>");
	}
	
	public void setParameters(Map<String, String> parameters) {
		super.setParameters(parameters);
		
		// set additional parameters not in the main TxmForm
		if (parameters.get(Keys.NLINESPERPAGE) != null) this.setNLinesPerPage(parameters.get(Keys.NLINESPERPAGE));
	}
	
	public ContextTab(Map<String, String> parameters, ListGridRecord elem, boolean compute) {
		
		this.setName("CONTEXT");
		this.form = new ConcordanceForm(elem, parameters, this);
		this.form.setContextSizes("50", "50");
		this.elem = elem;
		this.setTitle(elem.getAttribute("path2") + ":" + form.getParams().getQuery());

		ListGridField referenceField = new ListGridField("REFERENCE", Services.getI18n().contFieldReference());
		referenceField.setWidth(150);
		referenceField.setFrozen(true);
		ListGridField contextField = new ListGridField("CONTEXT", Services.getI18n().contFieldContext());
		contextField.setFrozen(true);
		addBackToTextHandler(parameters.get(Keys.PATH));
		resultList.setShowEmptyMessage(false);
		resultList.setWrapCells(true);
		//resultList.setExpansionMode(ExpansionMode.DETAIL_FIELD);
		//resultList.setCanExpandRecords(true);
		//resultList.setDetailField("CONTEXT");
		resultList.setCanSort(false);
		resultList.setCanSelectText(true);
		resultList.setCanFreezeFields(false);
		resultList.setCanGroupBy(false);
		resultList.setCanAutoFitFields(false);
		resultList.setShowRowNumbers(true);
		resultList.setCanPickFields(false);
		resultList.setFixedRecordHeights(false);
		resultList.setFields(referenceField, contextField);
		
		EditionPanel edition = new EditionPanel(parameters.get(Keys.PATH), null);
		edition.setID("edition" + this.getID());
		edition.setShowResizeBar(false);
		edition.setHeight100();
		edition.hide();

		VLayout resultSection = new VLayout();
		resultSection.setID("concordance" + this.getID());
		resultSection.setSize("100%", "100%");

		VLayout section2 = new VLayout();
		section2.setHeight100();
		ToolStrip concordanceBar = createConcordanceBar();
		section2.setMembers(resultList, concordanceBar);

		resultSection.setMembers(form, section2); 

		concordanceResultContent.setSize("100%", "100%");
		concordanceResultContent.setMembers(edition, resultSection);
		
//		if (query != null) this.setQuery(query);
//		if (props != null) this.setViewProperties(props);
//		if (refs != null) this.setReferenceProperties(refs);
//		if (nLinesPerPage != null) this.setNLinesPerPage(nLinesPerPage);
//		if (contextsSize != null) this.setContextsSize(contextsSize);
		
		
		this.setPane(concordanceResultContent);
		this.addInView();
		this.setParameters(parameters);
		if (parameters.containsKey(Keys.QUERY) && parameters.get(Keys.QUERY).length() > 0) {
			this.compute();
		}
	}

	public void fillList(ConcordanceResult result) {
		resultList.setShowEmptyMessage(true);
		resultList.setCanSelectText(true);
		for (Record r : resultList.getRecords())
			resultList.removeData(r);
		for (ConcordanceResultLine entry : result) {
			ListGridRecord record = new ListGridRecord();
			String context = entry.getLeftContext() + " <font color='red'><b>" + entry.getKeyword() + "</b></font> " + entry.getRightContext();
			record.setAttribute("REFERENCE", "<i>" + entry.getReference() + "</i>");
			record.setAttribute("KEYWORD", entry.getKeyword());
			record.setAttribute("CONTEXT", context);
			record.setAttribute("WORDID", entry.getWordIds().toArray(new String[entry.getWordIds().size()]));
			record.setAttribute("TEXTID", entry.getTextId());
			resultList.addData(record);
			//resultList.expandRecord(record);
		}	
		this.setTitle(elem.getAttribute("path2") + ":" + form.getParams().getQuery());
		if (result.size() == 0)
			total = 0;
		else
			total = result.total;
		if (form.getParams().getEndIndex() > total)
			form.getParams().setEndIndex(total);
		navigation.updateLabel("<b>" + form.getParams().getStartIndex() + "-" + form.getParams().getEndIndex() + "</b>/" + total);
		
		String message = Services.getI18n().occurences(total);
		if (total == 0)
			message = Services.getI18n().occurrence(total);
		TXMWEB.getConsole().addMessage(message);
		navigation.updateInfos(message);
		
		if (result.size() > 0)
		if (result.get(0).getIsTruncated()) {
			if (firstWarning) {
				SC.say(Services.getI18n().sayTruncatedResults());
				firstWarning = false;
			}
			TXMWEB.getConsole().addMessage(Services.getI18n().sayTruncatedResults());
		}
	}


	public void selectRecord(int n) {
		if (resultList != null) resultList.selectRecord(n);
	}
	
	public void doBackToText(@SuppressWarnings("rawtypes") GridRowColEvent event) {
		ListGridRecord record;
		if (event == null) {
			if (resultList.getRecords().length == 0) return; // abort
			record = resultList.getRecord(0);
		} else {
			record = event.getRecord();
		}
		
		final String[] wordIds = record.getAttributeAsStringArray("WORDID");
		String textId = record.getAttribute("TEXTID");
		final String baseId = "/"+form.getParams().getCorpusPath().split("/")[1];
		EditionPanel edition = (EditionPanel)concordanceResultContent.getMember("edition" + getID());
		edition.backToText(baseId, textId, wordIds);
	}
	
	private void addBackToTextHandler(String itemPath) {
		
		if (TXMWEB.EXPO) {
			resultList.addCellClickHandler(new CellClickHandler() {
				@Override
				public void onCellClick(CellClickEvent event) {
					doBackToText(event);
				}
			});
		} else {
			resultList.addCellDoubleClickHandler(new CellDoubleClickHandler() {
				@Override
				public void onCellDoubleClick(CellDoubleClickEvent event) {
					doBackToText(event);
				}
			});
		}
	}

	private ToolStrip createConcordanceBar() {
		ToolStrip concordanceBar = new ToolStrip();
		navigation = new NavigationToolStrip();
		navigation.addExportClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ProcessingWindow.show(Services.getI18n().exporting());
				ExecTimer.start();
				Services.getConcordanceService().exportConcordance(form.getParams(), new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
					}
					@Override
					public void onSuccess(String result) {
						ProcessingWindow.hide();
						//new ExportTab("export " + form.getParams().getCorpusPath() + ":" + form.getParams().getQuery(), result).addInView();
						if (result == null) {
							TxmDialog.severe(Services.getI18n().internalError()+" result is null");
						} else if (result.startsWith("error")) {
							TxmDialog.severe(result);
						} else if (result.length() > 0) {
							Window.open(result, "", "");
							TxmDialog.exportWarning();
						}
					}
				});
			}
		});
		navigation.addPreviousClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int pageSize = form.getParams().getResultPerPage();
				int newStartIndex = form.getParams().getStartIndex() - pageSize;
				int newEndIndex = form.getParams().getEndIndex() - pageSize;
				form.getParams().setStartIndex(newStartIndex < 1 ? 1 : newStartIndex);
				form.getParams().setEndIndex(newEndIndex < pageSize ? pageSize : newEndIndex);
				ExecTimer.start();
				Services.getConcordanceService().getLines(form.getParams(), new AsyncCallback<ConcordanceResult>() {
					@Override
					public void onFailure(Throwable caught) {
						SC.say(Services.getI18n().concordanceError());
					}
					@Override
					public void onSuccess(ConcordanceResult result) {
						fillList(result);
					}
				});
			}
		});
		navigation.addFirstClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int pageSize = form.getParams().getResultPerPage();
				form.getParams().setStartIndex(1);
				form.getParams().setEndIndex(pageSize);
				ExecTimer.start();
				Services.getConcordanceService().getLines(form.getParams(), new AsyncCallback<ConcordanceResult>() {
					@Override
					public void onFailure(Throwable caught) {
						SC.say(Services.getI18n().concordanceError());
					}
					@Override
					public void onSuccess(ConcordanceResult result) {
						fillList(result);
					}
				});
			}
		});
		navigation.addLastClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int pageSize = form.getParams().getResultPerPage();
				form.getParams().setStartIndex(total - (total%pageSize));
				form.getParams().setEndIndex(total);
				ExecTimer.start();
				Services.getConcordanceService().getLines(form.getParams(), new AsyncCallback<ConcordanceResult>() {
					@Override
					public void onFailure(Throwable caught) {
						SC.say(Services.getI18n().concordanceError());
					}
					@Override
					public void onSuccess(ConcordanceResult result) {
						fillList(result);
					}
				});
			}
		});
		navigation.addNextClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int pageSize = form.getParams().getResultPerPage();
				int newStartIndex = form.getParams().getStartIndex() + pageSize;
				int newEndIndex = form.getParams().getEndIndex() + pageSize;
				form.getParams().setStartIndex(newStartIndex > total - pageSize ? total - (total%pageSize) : newStartIndex);
				form.getParams().setEndIndex(newEndIndex > total ? total : newEndIndex);
				ExecTimer.start();
				Services.getConcordanceService().getLines(form.getParams(), new AsyncCallback<ConcordanceResult>() {
					@Override
					public void onFailure(Throwable caught) {
						SC.say(Services.getI18n().concordanceError());
					}
					@Override
					public void onSuccess(ConcordanceResult result) {
						fillList(result);
					}
				});
			}
		});

		navigation.updateLabel("<b>" + form.getParams().getStartIndex() + "-" + form.getParams().getEndIndex() + "</b>/" + total);
		navigation.addToToolstrip(concordanceBar);
		
		concordanceBar.setWidth100();
		concordanceBar.setHeight(25);
		
		return concordanceBar;
	}

	@Override
	public int getResultPerPage() {
		return navigation.getPagination();
	}

	@Override
	public void setFirstWarning(boolean b) {
		firstWarning = b;
	}

	@Override
	public ArrayList<String> getKeywordSortProperties() {
		return form.getKeywordSortProperties();
	}

	@Override
	public ArrayList<String> getLeftSortProperties() {
		return form.getLeftSortProperties();
	}

	@Override
	public ArrayList<String> getRightSortProperties() {
		return form.getRightSortProperties();
	}

	public void setQuery(String query) {
		this.form.setQuery(query);
	}

	public void setViewProperties(String props) {
		this.form.setViewProperties(props);
	}
	
	private void setContextsSize(String contextsSize) {
		this.form.setContextSizes(contextsSize, contextsSize);
	}

	private void setNLinesPerPage(String nLinesPerPage) {
		this.navigation.setPagination(Integer.parseInt(nLinesPerPage));
	}
	
	private void setReferenceProperties(String refs) {
		this.form.setReferenceProperties(refs);
	}
	
	public void compute() {
		this.form.validate();
	}
	@Override
	public void onClose() {
		// TODO Auto-generated method stub
		
	}
}
