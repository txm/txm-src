package org.txm.web.client.widgets.forms;

import com.smartgwt.client.widgets.form.fields.SpinnerItem;

public class FloatItem extends SpinnerItem {
	public FloatItem(String name) {super(name);}
	public FloatItem(String name, String label) {super(name, label);}
}
