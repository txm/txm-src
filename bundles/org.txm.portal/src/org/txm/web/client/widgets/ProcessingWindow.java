package org.txm.web.client.widgets;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;

import com.smartgwt.client.types.Cursor;
import com.smartgwt.client.types.ImageStyle;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Window;

public class ProcessingWindow {
	
	private static Img img;
	private static Window window = new Window();
	
	static {
		String newsImage = "spinner.gif";  
        img = new Img(newsImage, 100, 100);  
        img.setImageType(ImageStyle.CENTER);
        img.setBorder("0px solid white");
        
		window.setShowModalMask(true);
		window.setTitle(Services.getI18n().taskRunning());
		window.setShowMinimizeButton(false);
		window.setShowCloseButton(false);
		window.setWidth(100);
		window.setHeight(100);
		window.setIsModal(true);
		window.setShowHeader(false);
		window.setShowFooter(false);
		window.setShowEdges(false);
		window.setCursor(Cursor.WAIT);
		//window.setOpacity(0);
		window.setBorder(null);
		//window.setBorder("0px solid white");

		//Label content = new Label("Please wait...");
		//content.setSize("100%", "100%");
		//window.setAlign(com.smartgwt.client.types.Alignment.CENTER);

		window.addChild(img);
        window.centerInPage();
		window.hide();
	}

	public static void show() {
		//System.out.println("Processing...");
		//window.setTitle(Services.getI18n().taskRunning());
		TXMWEB.log(Services.getI18n().taskRunning());
		window.show();
		TXMWEB.updateFavicon("images/icons/processing.png");
	}
	
	public static void show(String title) {
		System.out.println("Processing... "+title);
		//window.setTitle(title);
		TXMWEB.log(title);
		window.show();
		TXMWEB.updateFavicon("images/icons/processing.png");
	}
	
	public static void hide() {
		TXMWEB.log("");
		TXMWEB.updateFavicon("favicon.ico");
		window.hide();
	}
}
