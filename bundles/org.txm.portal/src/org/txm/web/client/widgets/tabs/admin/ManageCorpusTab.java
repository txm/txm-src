package org.txm.web.client.widgets.tabs.admin;

import java.util.ArrayList;
import java.util.List;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.shared.Keys;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class ManageCorpusTab extends Tab {

	private AdminTab adminTab;
	Label corporaInfo = new Label();

	SelectItem removeCorpusCombo;

	public ManageCorpusTab(AdminTab adminTab) {
		this.adminTab = adminTab;
		try{
			this.setTitle(Services.getI18n().manageCorpus());

			VLayout content = new VLayout();
			
			final DynamicForm form = new DynamicForm();
			form.setNumCols(3);
			form.setIsGroup(true);
			form.setGroupTitle(Services.getI18n().adminLoadACorpus());

			TextItem corpusPath = new TextItem("path", Services.getI18n().typeTheCorpusLocation());
			corpusPath.setWidth(300);

			ButtonItem validate = new ButtonItem(Services.getI18n().adminValidate());
			validate.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {

				@Override
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					String corpusPath = form.getValueAsString(Keys.PATH);
					if (corpusPath == null || corpusPath.length() == 0) return;

					ProcessingWindow.show(Services.getI18n().loadingCorpus(corpusPath));
					Services.getAdminService().loadBinaryCorpus(corpusPath, new AsyncCallback<Boolean>() {
						@Override
						public void onFailure(Throwable caught) {
							ProcessingWindow.hide();
							TxmDialog.warning("Fail : " + caught.getMessage());
						}
						@Override
						public void onSuccess(Boolean result) {
							ProcessingWindow.hide();
							if (!result) {
								onFailure(new Exception("Loading service returned false"));
							} else {
								TxmDialog.warning(Services.getI18n().loadedCorpus());
								TXMWEB.getSideNavTree().updateTree();
							}
						}
					});
				}
			});

			form.setItems(corpusPath, validate);

			final DynamicForm form2 = new DynamicForm();

			removeCorpusCombo = new SelectItem("removebase");
			removeCorpusCombo.setTitle(Services.getI18n().corpus());

			updateCorpusToRemove(removeCorpusCombo);

			ButtonItem validateRemove = new ButtonItem("Remove");
			validateRemove.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {

				@Override
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					String corpusName = form2.getValueAsString("removebase");
					if (corpusName == null || corpusName.length() == 0) return;

					ProcessingWindow.show(Services.getI18n().removingCorpus(corpusName));
					Services.getAdminService().removeBinaryCorpus(corpusName, new AsyncCallback<Boolean>() {
						@Override
						public void onFailure(Throwable caught) {
							ProcessingWindow.hide();
							TxmDialog.warning("Fail : " + caught.getMessage());
						}
						@Override
						public void onSuccess(Boolean result) {
							ProcessingWindow.hide();
							if (!result) {
								onFailure(new Exception("No such corpus"));
							} else {
								TxmDialog.warning(Services.getI18n().corpusRemoved());
								TXMWEB.getSideNavTree().updateTree();

								updateCorpusToRemove(removeCorpusCombo);
							}
						}
					});
				}
			});

			form2.setNumCols(3);
			form2.setFields(removeCorpusCombo, validateRemove);
			form2.setIsGroup(true);
			form2.setGroupTitle(Services.getI18n().removeCorpus());

			final DynamicForm form3 = new DynamicForm();
			form3.setWidth100();
			form3.setAutoHeight();
			form3.setIsGroup(true);
			form3.setGroupTitle("<b>Uploader puis charger un corpus</b>");
			
//			HLayout layout = new HLayout();
//			layout.setWidth100();
//			layout.setAutoHeight();
			//layout.setW
			
			final FormPanel formPanel = new FormPanel();
			formPanel.setAction(TXMWEB.BASEURL+"/upload_corpus");
			formPanel.setWidth("100%");

			// Because we're going to add a FileUpload widget, we'll need to set the
			// form to use the POST method, and multipart MIME encoding.
			formPanel.setEncoding(FormPanel.ENCODING_MULTIPART);

			formPanel.setMethod(FormPanel.METHOD_POST);
			formPanel.addSubmitCompleteHandler(new SubmitCompleteHandler() {
				@Override
				public void onSubmitComplete(SubmitCompleteEvent event) {
					String body = event.getResults();
					TxmDialog.warning("Result: "+body+". You can load the corpus.");
				}
			});

			HorizontalPanel panel = new HorizontalPanel();
			panel.getElement().getStyle().setPadding(10, Unit.PX);
			formPanel.setWidget(panel);
			
			//panel.setBorderWidth(1);

			com.google.gwt.user.client.ui.Label l = new com.google.gwt.user.client.ui.Label("Sélectionner un corpus : ");
			
			final FileUpload upload = new FileUpload();
			upload.setName("uploadFormElement");
			upload.setTitle("Upload a corpus");
			panel.add(l);
			panel.add(upload);

			Button submitButton = new Button("Uploader et charger");
			submitButton.addClickHandler(new com.google.gwt.event.dom.client.ClickHandler() {
				@Override
				public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
					formPanel.submit();
				}
			});

			formPanel.addSubmitHandler(new SubmitHandler() {

				@Override
				public void onSubmit(SubmitEvent arg0) {
					String s = upload.getFilename();
					if (s == null || s.length() == 0) {
						TxmDialog.warning("No file selected.");
						arg0.cancel();
					} else {
						TxmDialog.warning("Sending TXM file...");
					}	
				}
			});

			panel.add(submitButton);
			form3.addChild(formPanel);

			IButton showCorporaButton = new IButton(Services.getI18n().showCorporaContent());
			showCorporaButton.setWidth(200);
			showCorporaButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					ProcessingWindow.show(Services.getI18n().listingCorporaDirectory());
					Services.getAdminService().listCorporaDirectory(new AsyncCallback<ArrayList<String>>() {
						@Override
						public void onFailure(Throwable caught) {
							ProcessingWindow.hide();
							TxmDialog.warning("Fail : " + caught.getMessage());
						}
						@Override
						public void onSuccess(ArrayList<String> results) {
							ProcessingWindow.hide();
							if (results != null && results.size() > 0) {
								StringBuffer str = new StringBuffer();
								str.append("<br/><h3>"+Services.getI18n().paths()+"</h3>");
								str.append("<table>");
								for (String s : results) {
									String split[] = s.split(" ");
									String path = split[0];
									String corpusName = path.substring(path.lastIndexOf("/") + 1);
									String timestamp = split[1];
									str.append("<tr>");
									str.append("<td>"+path+"</td>");
									str.append("<td>"+corpusName+"</td>");
									str.append("<td>"+timestamp+"</td>");
									str.append("</tr>");
								}
								str.append("</table>");
								corporaInfo.setContents(str.toString());
							} else {
								TxmDialog.warning("An error occured. Result is: "+results);
							}
						}
					});
				}
			});

			corporaInfo.setWidth100();
			corporaInfo.setCanSelectText(true);
			//corporaInfo.setHeight100();

			HLayout hlayout = new HLayout();
			hlayout.setMembers(form, form2);
			hlayout.setHeight(100);
			
			HLayout hlayout2 = new HLayout();
			hlayout2.setMembers(form3);
			hlayout2.setHeight(100);
			
			content.setMembers(hlayout2, hlayout, showCorporaButton, corporaInfo);
			
			this.setPane(content);
		} catch(Exception e) {
			TxmDialog.severe("Internal Error: "+e);
		}
	}

	private void updateCorpusToRemove(final SelectItem combo) {
		Services.getCorpusActionService().getBases(new AsyncCallback<List<String>>() {
			@Override
			public void onSuccess(List<String> result) {
				combo.setValueMap(result.toArray(new String[result.size()]));
			}

			@Override
			public void onFailure(Throwable caught) {
				TXMWEB.error("Failed to get corpus list: "+caught);
			}
		});
	}
}
