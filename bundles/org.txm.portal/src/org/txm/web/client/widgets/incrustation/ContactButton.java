package org.txm.web.client.widgets.incrustation;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.CenterPanel;
import org.txm.web.client.widgets.tabs.HTMLTab;

import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.events.ClickEvent;

public class ContactButton extends Button{
	public static String ID = "contact_target";

	private static void call()
	{
		CenterPanel center = TXMWEB.getCenterPanel();
		HTMLTab contactPage = center.getUniqTab(Services.getI18n().contactTab());

		if(contactPage == null)
			return;
		
		contactPage.setCanClose(true);
		center.addTab(contactPage);
		center.selectTab(contactPage);
	}
	
	public static Button getButton()
	{
		Button btn = new Button(Services.getI18n().contactButton());
		btn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				call();
			}
		});
		return btn;
	}
	
	public static Anchor getLink()
	{
		Anchor btn = new Anchor();
		btn.setHTML(Services.getI18n().contactButton());
		btn.addClickHandler(new com.google.gwt.event.dom.client.ClickHandler() {
			
			@Override
			public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
				call();
			}
		});
		return btn;
	}
	
	public static boolean incrust()
	{
		RootPanel target = RootPanel.get(ID);
		if (target != null)
		{
			target.clear();
			String type = target.getElement().getAttribute("type");
			if(type != null && type.equals("link"))
				target.add(getLink());
			else
				target.add(getButton());
			return true;
		}
		return false;
	}
}
