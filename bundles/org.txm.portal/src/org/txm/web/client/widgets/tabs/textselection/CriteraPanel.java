package org.txm.web.client.widgets.tabs.textselection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.txm.web.shared.TextSelectionResult;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.events.SectionHeaderClickEvent;
import com.smartgwt.client.widgets.layout.events.SectionHeaderClickHandler;

public class CriteraPanel extends SectionStack{
	HashMap<String, ArrayList<String>> allproperties;
	ArrayList<Selector> selectors = new ArrayList<Selector>();

	//source data
	List<String> textids;
	HashMap<String, HashMap<String, String>> metadatas;
	HashMap<String, Integer> numberOfWords;
	TextSelectionTab textSelectionTab;

	public static boolean selectAllAtStartUp = false;

	public CriteraPanel(String itemPath, TextSelectionTab textSelectionTab, TextSelectionResult result) {
		this.textSelectionTab = textSelectionTab;
		this.textids = result.textids;
		this.metadatas = result.valuesPerText;
		this.numberOfWords = result.numberOfWordsPerText;
		allproperties = result.valuesPerProperties;

		//System.out.println("RESULT :\n"+result.toString());
		this.setVisibilityMode(VisibilityMode.MULTIPLE);  
		this.setCanResizeSections(true);
		this.setWidth("30%");
		this.setOverflow(Overflow.AUTO);
		this.setMinMemberSize(50);

		Selector selector = null;
		//System.out.println("** create criteria for properties: "+result.properties);
		for (String property : result.properties) {
			if (!property.equals("project") && !property.equals("base") && !property.equals("id"))
			{
				String type = "String";
				boolean open = false;
				boolean selection = true;
				if (textSelectionTab.propertiesInfos.get(property) != null) {
					type = textSelectionTab.propertiesInfos.get(property).type;
					open = textSelectionTab.propertiesInfos.get(property).open;
					selection = textSelectionTab.propertiesInfos.get(property).selection;
				}
				
				if (!selection) // skip critera since not in the selection
					continue;
				
				if (type != null && type.equals("Date")) {
					selector = new DateSelector(this, property, allproperties.get(property), type);
					selectors.add(selector);
				} else {
					selector = new Selector(this, property, allproperties.get(property), type, open);
					selectors.add(selector);
				}
			}		
		}
		//System.out.println("**groups infos: "+textSelectionTab.groupsInfos);
		//System.out.println("**groups properties: "+textSelectionTab.groupsProperties);
		for (String gp : result.groups) {
			String type = textSelectionTab.groupsInfos.get(gp).type;
			if (!textSelectionTab.groupsInfos.get(gp).selection)
				continue;
			if (type.equals("MinMaxDate")) {
				//System.out.println("**create MinMaxDate of group: "+gp);
				selector = new GroupedDateSelector(this, textSelectionTab.groupsInfos.get(gp), textSelectionTab.groupsProperties.get(gp));
				int position = Integer.parseInt(textSelectionTab.groupsInfos.get(gp).extra.get("position"));
				position = Math.min(position, selectors.size());
				selectors.add(position, selector);
			}
		}

		for (int i = 0 ; i < selectors.size() ; i++) {
			this.addSection(selectors.get(i));
			this.collapseSection(i);
		}
		
		this.addSectionHeaderClickHandler(new SectionHeaderClickHandler() {
			@Override
			public void onSectionHeaderClick(SectionHeaderClickEvent event) {
				//System.out.println("LOAD SELECTOR");
				SectionStackSection s = event.getSection();
				Selector sel = (Selector)s;
				sel.loadData();
			}
		});
		this.setShowResizeBar(true);
		updateSelectedTexts();

	}

	public void updateSelectedTexts(Selector s)
	{
		continueTextSelectionUpdate();
	}

	public void updateSelectedTexts()
	{
		//System.out.println("updateSeletedTexts");
		//clear selection
		textSelectionTab.selectedTexts.clear();
		boolean clearAll = true;
		for (Selector s : selectors)// test if a selector will reduce the selection
		{
			if (s.shouldTest()) {
				clearAll = false;
				break;
			}
		}
		//System.out.println("no text to select: "+clearAll);
		if (clearAll) // no selector is defined, no text is selected
		{
			//System.out.println("don't process further, there is no criteria selected");
			continueTextSelectionUpdate();
			return;
		}

		//add all text by default
		textSelectionTab.selectedTexts = new ArrayList<Record>();
		for (Record r : this.textSelectionTab.table.records)
			textSelectionTab.selectedTexts.add(r);
		
		//System.out.println("loop over selectors...");
		//then remove texts exclude by others Selector if they have a restriction defined
		for (Selector s : selectors) {
			if (s.isLoaded())
			if (textSelectionTab.selectedTexts.size() > 0) 
				s.filterTexts(textSelectionTab.selectedTexts);
			else // if there is no more removable texts, stop this loop
				break;
		}
		//System.out.println("Selected texts : "+textSelectionTab.selectedTexts);

		continueTextSelectionUpdate();
	}

	public ListGridRecord[] getAllRecords()
	{
		int size = 0;
		ListGridRecord[] allrecords;
		for (Selector s : selectors)
			size += s.getRecords().length;
		allrecords = new ListGridRecord[size];
		size = 0;
		for (Selector s : selectors) {
			System.arraycopy(s.getRecords(), 0, allrecords, size, s.getRecords().length);
			size += s.getRecords().length;
		}

		return allrecords;
	}

	/**
	 * update n and N of criteria panel
	 * update stats panel
	 */
	public void continueTextSelectionUpdate()
	{
		textSelectionTab.resetTextCounts(); // reset counts
		textSelectionTab.table.refresh(); // update the table (but don't change the selection)
		textSelectionTab.table.updateTextCounts(); // update the counts from the table
		
		refreshSelectorsCounts();

		if (textSelectionTab.isStatsEnable())
			textSelectionTab.statsPanel.refresh();
	}



	/**
	 * update columns n and t from the textSelection and text counts and word counts
	 */
	public void refreshSelectorsCounts() {
		HashMap<String, HashMap<String, Integer>> nbTextPerProp = textSelectionTab.numberOfSelectedTextPerProperty;
		HashMap<String, HashMap<String, Integer>> nbWPerTextPerProp = textSelectionTab.numberOfWordPerSelectedTextPerProperty;
		//update selectors' records
		for (Selector s : selectors) {
			//if (textSelectionTab.selectedTexts.size() > 0)
			s.updateCounts(nbTextPerProp.get(s.property), nbWPerTextPerProp.get(s.property));
		}
	}
	
	public void refreshSelectorCounts(Selector s) {
		s.updateCounts(textSelectionTab.numberOfSelectedTextPerProperty.get(s.property),
				textSelectionTab.numberOfWordPerSelectedTextPerProperty.get(s.property));
	}
}
