package org.txm.web.client.widgets.forms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.txm.web.client.ExecTimer;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.tabs.TsQueryTab;
import org.txm.web.shared.Keys;
import org.txm.web.shared.TsQueryParam;
import org.txm.web.shared.TsQueryResult;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class TIGERSearchForm extends TxmForm {

	protected static boolean firstExportWarning = true;
	protected ToolStrip tsqueryBar;
	protected int total = 0;
	protected Label graphLabel = new Label("");

	protected TsQueryParam params = new TsQueryParam();
	protected TsQueryResult tsresult = null;
	protected TsQueryTab parentTab;

	protected String html = "the html code";
	protected String svg = "ths svg code";

	protected List<String> ntfeatures;
	protected List<String> tfeatures;

	public TIGERSearchForm(ListGridRecord elem, Map<String, String> parameters, TsQueryTab tsQueryTab) {
		super();
		
		params.corpuspath = parameters.get(Keys.PATH);
		this.parentTab = tsQueryTab;
		
		initializeFields(parameters);
	}
	
	@Override
	public void _initializeFields(Map<String, String> parameters) {
		
		ProcessingWindow.show(Services.getI18n().openingTSForm());
		
		this.setWidth100();

		TSQueryItem queryField = new TSQueryItem(Keys.QUERY); // Requête au format TIGERSearch
		queryField.setTooltip(Services.getI18n().tsqueryTooltip());//"Write your TigerSearch query here");
		queryField.setColSpan(2);
		queryField.setShowTitle(false);
		queryField.setWidth("100%");
		queryField.setHeight("100%");

		tsqueryBar = createSubmitNavigationBar();
		refreshInfosAndButtons();
		//refreshTab();

		form = new DynamicForm();
		form.setWidth100();
		form.setHeight100();
		form.setItems(queryField);

		this.setMembers(form, tsqueryBar);
		
	
		ProcessingWindow.hide();
	}

	private void refreshTab() {

		if (tsresult != null) {
			parentTab.refreshHTML(tsresult.html);
			parentTab.refreshGraph(tsresult.svg);
		} else {
			parentTab.refreshHTML("empty.html");
			parentTab.refreshGraph("empty.html");
		}
	}

	private void validate() {
		ProcessingWindow.show(Services.getI18n().computing_TS());
		String corpuspath = params.corpuspath;
		String query = form.getValueAsString(Keys.QUERY);
		if (params.corpuspath.equals(corpuspath) && params.query.equals(query)) {
			//its an update
		}
		else {
			//params = new TsQueryParam(); // its a new query
			params.corpuspath = corpuspath;
			params.query = form.getValueAsString(Keys.QUERY);
		}
		
		params.tprops = new ArrayList<String>(Arrays.asList(tFeaturesItem.getValues()));
		params.ntprop = ntFeaturesItem.getValueAsString();
		
		ExecTimer.start();
		Services.getTsQueryService().tsquery(params, new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				TxmDialog.severe(Services.getI18n().tsqueryFailed());
			}
			@Override
			public void onSuccess(Boolean result) {
				//System.out.println("Submit query ok? : "+result);
				ProcessingWindow.hide();
				if (result) {
					ExecTimer.start();
					Services.getTsQueryService().getFirst(params, new AsyncCallback<TsQueryResult>() {
						@Override
						public void onFailure(Throwable caught) {
							TxmDialog.severe(Services.getI18n().tsqueryFailed());
						}
						@Override
						public void onSuccess(TsQueryResult result2) {
							if (result2 == null) {
								TxmDialog.severe("TIGERSearch result is null.");
								return;
							}
							tsresult = result2;
							//System.out.println("TsQuery result  : "+result2);
							refreshInfosAndButtons();
							refreshTab();
							TXMWEB.getConsole().addMessage("Number of sentences: "+result2.numberOfMatch+ExecTimer.stop());
						}
					});
				} else {
					tsresult = null;
					TxmDialog.warning(Services.getI18n().tsqueryNoResult());
				}
			}
		});
	}

	public void setParams(TsQueryParam params) {
		this.params = params;
	}

	public TsQueryParam getParams() {
		return params;
	}

	IButton export = new IButton(Services.getI18n().exportts());
	IButton exportforest = new IButton(Services.getI18n().concordance());
	IButton submit = new IButton(Services.getI18n().searchts());
	ToolStripButton previousGraph = new ToolStripButton();
	ToolStripButton nextGraph = new ToolStripButton();
	ToolStripButton firstGraph = new ToolStripButton();
	ToolStripButton lastGraph = new ToolStripButton();
	SpinnerItem currentGraphtextItem;
	Label currentGraphTitle = new Label("&nbsp;&nbsp;sent: ");

	ToolStripButton previousSubGraph = new ToolStripButton();
	ToolStripButton nextSubGraph = new ToolStripButton();
	Label subGraphLabel = new Label("");
	Label currentSubGraphTitle = new Label("sub: ");
	SpinnerItem currentSubGraphtextItem;
	
	SelectItem tFeaturesItem = new SelectItem();
	SelectItem ntFeaturesItem = new SelectItem();

	private ToolStrip createSubmitNavigationBar() {
		
		submit.setWidth(90);
		export.setWidth(90);
		exportforest.setWidth(100);
		
		ChangedHandler selectGraphHandler = new ChangedHandler() {	
			@Override
			public void onChanged(ChangedEvent event) {
				if (tsresult != null) {
					ExecTimer.start();
					int nograph = Integer.parseInt(currentGraphtextItem.getValueAsString()) - 1;
					int nosubgraph = Integer.parseInt(currentSubGraphtextItem.getValueAsString()) - 1;
					Services.getTsQueryService().getGraph(params,nograph ,nosubgraph , new AsyncCallback<TsQueryResult>() {
						@Override
						public void onFailure(Throwable caught) {
						}
						@Override
						public void onSuccess(TsQueryResult result2) {
							if (result2 == null) {
								TxmDialog.severe("TIGERSearch result is null.");
								return;
							}
							tsresult = result2;
							refreshInfosAndButtons();
							refreshTab();
						}
					});
				}
			}
		};

		currentSubGraphtextItem = new SpinnerItem();  
		currentSubGraphtextItem.setDefaultValue(0);  
		currentSubGraphtextItem.setMin(1);  
		currentSubGraphtextItem.setMax(0);  
		currentSubGraphtextItem.setStep(1); 
		currentSubGraphtextItem.setShowTitle(false);
		currentSubGraphtextItem.setWidth(70);
		currentSubGraphtextItem.addChangedHandler(selectGraphHandler);
		currentSubGraphtextItem.setWriteStackedIcons(false);
		
		currentGraphtextItem = new SpinnerItem();  
		currentGraphtextItem.setDefaultValue(0);  
		currentGraphtextItem.setMin(1);  
		currentGraphtextItem.setMax(0);  
		currentGraphtextItem.setStep(1); 
		currentGraphtextItem.setShowTitle(false);
		currentGraphtextItem.setWidth(70);
		currentGraphtextItem.addChangedHandler(selectGraphHandler);
		currentGraphtextItem.setWriteStackedIcons(false);

		ToolStrip tsBar = new ToolStrip();
		exportforest.setTooltip(Services.getI18n().exportts()+" Concordance");
		exportforest.setIcon("icons/functions/Export.png");
		exportforest.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (tsresult != null) {
					new TSExportForm(params, ntfeatures, tfeatures);
				}
			}
		});

		export.setTooltip(Services.getI18n().exportts());
		export.setIcon("icons/functions/ExportSVG.png");
		export.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (tsresult != null) {
					//open params
					Window.open(tsresult.svg, "", "");
					TxmDialog.exportWarning();
				}
			}
		});

		submit.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				validate();
			}
		});

		previousGraph.setIcon("icons/control_reverse.png");
		previousSubGraph.setTitle("<");
		nextGraph.setIcon("icons/control_play.png");
		nextSubGraph.setTitle(">");
		firstGraph.setIcon("icons/control_start.png");
		lastGraph.setIcon("icons/control_end.png");

		previousGraph.setTooltip(Services.getI18n().previousPage());
		nextGraph.setTooltip(Services.getI18n().nextPage());
		firstGraph.setTooltip(Services.getI18n().firstPage());
		lastGraph.setTooltip(Services.getI18n().lastPage());

		previousSubGraph.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (tsresult != null) {
					ExecTimer.start();
					params.tprops = new ArrayList<String>(Arrays.asList(tFeaturesItem.getValues()));
					params.ntprop = ntFeaturesItem.getValueAsString();
					ProcessingWindow.show("Previous sub match");
					Services.getTsQueryService().getPreviousSub(params, new AsyncCallback<TsQueryResult>() {
						@Override
						public void onFailure(Throwable caught) {
							ProcessingWindow.hide();
							TxmDialog.severe(Services.getI18n().internalError());
						}
						@Override
						public void onSuccess(TsQueryResult result2) {
							if (result2 == null) {
								TxmDialog.severe("TIGERSearch result is null.");
								return;
							}
							ProcessingWindow.hide();
							tsresult = result2;
							refreshInfosAndButtons();
							refreshTab();
						}
					});
				}
			}
		});
		nextSubGraph.setTooltip("Shows the next sub graph");
		nextSubGraph.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ProcessingWindow.show("Next sub match");
				if (tsresult != null) {
					ExecTimer.start();
					params.tprops = new ArrayList<String>(Arrays.asList(tFeaturesItem.getValues()));
					params.ntprop = ntFeaturesItem.getValueAsString();
					Services.getTsQueryService().getNextSub(params, new AsyncCallback<TsQueryResult>() {
						@Override
						public void onFailure(Throwable caught) {
							ProcessingWindow.hide();
						}
						@Override
						public void onSuccess(TsQueryResult result2) {
							if (result2 == null) {
								TxmDialog.severe("TIGERSearch result is null.");
								return;
							}
							ProcessingWindow.hide();
							tsresult = result2;
							refreshInfosAndButtons();
							refreshTab();
						}
					});
				}
			}
		});

		previousGraph.setTooltip("Shows the previous graph");
		previousGraph.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (tsresult != null) {
					ExecTimer.start();
					params.tprops = new ArrayList<String>(Arrays.asList(tFeaturesItem.getValues()));
					params.ntprop = ntFeaturesItem.getValueAsString();
					ProcessingWindow.show("Previous match");
					Services.getTsQueryService().getPrevious(params, new AsyncCallback<TsQueryResult>() {
						@Override
						public void onFailure(Throwable caught) {
							ProcessingWindow.hide();
						}
						@Override
						public void onSuccess(TsQueryResult result2) {
							if (result2 == null) {
								TxmDialog.severe("TIGERSearch result is null.");
								return;
							}
							ProcessingWindow.hide();
							tsresult = result2;
							//System.out.println("previous result  : "+result2);
							refreshInfosAndButtons();
							refreshTab();
						}
					});
				}
			}
		});
		firstGraph.setTooltip("Shows the first graph");
		firstGraph.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ProcessingWindow.show("First match");
				if (tsresult != null) {
					ExecTimer.start();
					params.tprops = new ArrayList<String>(Arrays.asList(tFeaturesItem.getValues()));
					params.ntprop = ntFeaturesItem.getValueAsString();
					Services.getTsQueryService().getFirst(params, new AsyncCallback<TsQueryResult>() {
						@Override
						public void onFailure(Throwable caught) {
							ProcessingWindow.hide();
						}
						@Override
						public void onSuccess(TsQueryResult result2) {
							if (result2 == null) {
								TxmDialog.severe("TIGERSearch result is null.");
								return;
							}
							ProcessingWindow.hide();
							tsresult = result2;
							//System.out.println("first result  : "+result2);
							refreshInfosAndButtons();
							refreshTab();
						}
					});
				}
			}
		});
		lastGraph.setTooltip("Shows the last graph");
		lastGraph.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ProcessingWindow.show("Last match");
				if (tsresult != null) {
					ExecTimer.start();
					params.tprops = new ArrayList<String>(Arrays.asList(tFeaturesItem.getValues()));
					params.ntprop = ntFeaturesItem.getValueAsString();
					Services.getTsQueryService().getLast(params, new AsyncCallback<TsQueryResult>() {
						@Override
						public void onFailure(Throwable caught) {
							ProcessingWindow.hide();
						}
						@Override
						public void onSuccess(TsQueryResult result2) {
							if (result2 == null) {
								TxmDialog.severe("TIGERSearch result is null.");
								return;
							}
							ProcessingWindow.hide();
							tsresult = result2;
							//System.out.println("last result  : "+result2);
							refreshInfosAndButtons();
							nextGraph.setDisabled(true);
							refreshTab();
						}
					});
				}
			}
		});
		nextGraph.setTooltip("Shows the next graph");
		nextGraph.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//System.out.println("Next Sentence");
				ProcessingWindow.show("Next match");
				if (tsresult != null) {
					ExecTimer.start();
					params.tprops = new ArrayList<String>(Arrays.asList(tFeaturesItem.getValues()));
					params.ntprop = ntFeaturesItem.getValueAsString();
					Services.getTsQueryService().getNext(params, new AsyncCallback<TsQueryResult>() {
						@Override
						public void onFailure(Throwable caught) {
							ProcessingWindow.hide();
						}
						@Override
						public void onSuccess(TsQueryResult result2) {
							if (result2 == null) {
								TxmDialog.severe("TIGERSearch result is null.");
								return;
							}
							ProcessingWindow.hide();
							tsresult = result2;
							//System.out.println("next result  : "+result2);
							refreshInfosAndButtons();
							refreshTab();
						}
					});
				}
			}
		});

		
		tFeaturesItem.setTitle("T");
		tFeaturesItem.setWidth(70);  
		tFeaturesItem.setMultiple(true);
		
		ntFeaturesItem.setTitle("NT");
		ntFeaturesItem.setWidth(70);
		ntFeaturesItem.setMultiple(false);
		
		ChangedHandler newViewProperties = new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				params.tprops = new ArrayList<String>(Arrays.asList(tFeaturesItem.getValues()));
				params.ntprop = ntFeaturesItem.getValueAsString();
				//System.out.println("tprops: "+Arrays.toString(tprops));
				//System.out.println("ntprop: "+ntprop);
				ProcessingWindow.show("Features changed");
				if (tsresult != null) {
					ExecTimer.start();
					Services.getTsQueryService().setFeatures(params, new AsyncCallback<TsQueryResult>() {
						@Override
						public void onFailure(Throwable caught) {
							ProcessingWindow.hide();
						}
						@Override
						public void onSuccess(TsQueryResult result2) {
							if (result2 == null) {
								TxmDialog.severe("TIGERSearch result is null.");
								return;
							}
							ProcessingWindow.hide();
							tsresult = result2;
							refreshInfosAndButtons();
							refreshTab();
						}
					});
				}
			}
		};
		tFeaturesItem.addChangedHandler(newViewProperties);
		ntFeaturesItem.addChangedHandler(newViewProperties);
		
		Services.getTsQueryService().getFeatures(params, new AsyncCallback<HashMap<String, List<String>>>() {
			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.severe("Error while fetching TS features list: "+caught.getMessage());
			}

			@Override
			public void onSuccess(HashMap<String, List<String>> result) {
				ntfeatures = result.get("nt");
				tfeatures = result.get("t");
				tFeaturesItem.setValueMap(tfeatures.toArray(new String[tfeatures.size()]));
				if (tfeatures.size() > 0) tFeaturesItem.setValue(tfeatures.get(0));
				ntFeaturesItem.setValueMap(ntfeatures.toArray(new String[ntfeatures.size()]));
				if (ntfeatures.size() > 0) ntFeaturesItem.setValue(ntfeatures.get(0));
			}
		});

		currentGraphTitle.setWidth(80);
		currentSubGraphTitle.setWidth(50);
		
		tsBar.setWidth100();
		tsBar.setHeight(25);
		//		tsBar.addMember(firstGraph);
		//tsBar.addMember(previousGraph);
		tsBar.addMember(currentGraphTitle);
		tsBar.addFormItem(currentGraphtextItem);
		tsBar.addMember(graphLabel);
		//tsBar.addMember(nextGraph);
		//		tsBar.addMember(lastGraph);
		//tsBar.addMember(previousSubGraph);
		tsBar.addMember(currentSubGraphTitle);
		tsBar.addFormItem(currentSubGraphtextItem);
		tsBar.addMember(subGraphLabel);
		//tsBar.addMember(nextSubGraph);
		tsBar.addFill();
		tsBar.addFormItem(tFeaturesItem);
		tsBar.addFormItem(ntFeaturesItem);
		tsBar.addFill();
		tsBar.addMember(submit);
		tsBar.addMember(export);
		tsBar.addMember(exportforest);
		return tsBar;
	}

	public void refreshInfosAndButtons() {
		if (tsresult == null) {
			currentGraphTitle.setContents("&nbsp;&nbsp;sent ");
			currentSubGraphTitle.setContents("sub ");
			graphLabel.setContents("<b> "+Services.getI18n().noresultts()+" </b>");
			subGraphLabel.setContents("<b> no sub graph </b>");
			firstGraph.setDisabled(true);
			previousGraph.setDisabled(true);
			nextGraph.setDisabled(true);
			lastGraph.setDisabled(true);
			export.setDisabled(true);
			exportforest.setDisabled(true);
		} else {
			currentGraphTitle.setContents("&nbsp;&nbsp;sent ("+(tsresult.nosent)+")");
			currentGraphtextItem.setValue(""+ (tsresult.nomatch + 1));
			currentGraphtextItem.setMax(tsresult.numberOfMatch);
			currentGraphtextItem.redraw();
			graphLabel.setContents("/" + tsresult.numberOfMatch);

			currentSubGraphTitle.setContents("sub "); 
			currentSubGraphtextItem.setMax(tsresult.numberOfSubGraph);
			currentSubGraphtextItem.setValue(""+(tsresult.nosubgraph+1)); 
			subGraphLabel.setContents("/ "+tsresult.numberOfSubGraph);
			
			firstGraph.setDisabled(false);
			lastGraph.setDisabled(false);
			export.setDisabled(false);
			exportforest.setDisabled(false);


			previousGraph.setDisabled(tsresult.nomatch == 0);
			nextGraph.setDisabled((tsresult.nomatch + 1) == tsresult.numberOfMatch);
			previousSubGraph.setDisabled(tsresult.nosubgraph == 0);
			nextSubGraph.setDisabled((tsresult.nosubgraph + 1) == (tsresult.numberOfSubGraph));

			TXMWEB.getConsole().addMessage(""+tsresult.numberOfMatch+" phrase"+((tsresult.numberOfMatch > 1 ? "s":"")));
		}
	}
}
