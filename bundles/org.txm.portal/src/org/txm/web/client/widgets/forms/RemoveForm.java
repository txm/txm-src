package org.txm.web.client.widgets.forms;

import java.util.Map;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.shared.Keys;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.tree.Tree;
import com.smartgwt.client.widgets.tree.TreeNode;

public class RemoveForm {
	
	public static void validate(Map<String, String> parameters, final ListGridRecord elem) {
		
		final String path = parameters.get(Keys.PATH);
		if (elem == null) {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(parameters.get(Keys.PATH)));
			return;
		}
		if (path == null || path.length() == 0) {
			TxmDialog.severe(Services.getI18n().errorMissingMandatoryParameter(Keys.PATH));
			return;
		}
		
		SC.confirm(Services.getI18n().validateDelete(elem.getAttribute(Keys.PATH)), new BooleanCallback() {
			@Override
			public void execute(Boolean value) {
				if (!value) {
					TXMWEB.log(Services.getI18n().failedToDelete());
					return;
				}
				Services.getCorpusActionService().delete(path, new AsyncCallback<Boolean>() {
					@Override
					public void onSuccess(Boolean result) {
						if (!result) {
							onFailure(null);
						} else {
							Tree tree = TXMWEB.getSideNavTree().getTree();
							tree.remove((TreeNode) elem);
							TXMWEB.getSideNavTree().refreshFields();
						}
					}
					@Override
					public void onFailure(Throwable caught) {
						TxmDialog.severe(Services.getI18n().sideNavTreeItemDeleteError());
					}
				});
			}
		});
	}
}
