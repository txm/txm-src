package org.txm.web.client.widgets.forms;

import java.util.Map;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.tabs.CMSTab;
import org.txm.web.client.widgets.tabs.EditionTab;
import org.txm.web.shared.Keys;

import com.smartgwt.client.widgets.grid.ListGridRecord;

public class EditionForm {
	public static EditionTab validate(Map<String, String> parameters, final ListGridRecord theRecord) {

		// String textid, String editions, String pageid, String wordids
		String path = parameters.get(Keys.PATH);
		String textid = parameters.get(Keys.TEXTID);
		String pageid = parameters.get(Keys.PAGEID);
		String wordids = parameters.get(Keys.WORDIDS);

		if (path == null || path.length() == 0) {
			TxmDialog.severe(Services.getI18n().errorMissingMandatoryParameter(Keys.PATH));
			return null;
		}
		if (pageid != null || wordids != null) {
			if (textid == null || textid.length() == 0) {
				TxmDialog.severe(Services.getI18n().errorMissingMandatoryParameter(Keys.TEXTID));
				return null;
			}
		}

		if (theRecord != null) {
			TXMWEB.log(Services.getI18n().open_edition_tab());
			EditionTab tab = new EditionTab(theRecord, parameters);
			//EditionPanel panel = tab.getEditionPanel();		
			tab.addInView();
			return tab;
		} else {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(path));
			return null;
		}
	}
}
