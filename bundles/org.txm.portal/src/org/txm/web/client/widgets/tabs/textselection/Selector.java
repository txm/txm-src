package org.txm.web.client.widgets.tabs.textselection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.EnhancedListGrid;
import org.txm.web.client.widgets.ProcessingWindow;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.events.KeyPressEvent;
import com.smartgwt.client.widgets.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyUpEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyUpHandler;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RowOverEvent;
import com.smartgwt.client.widgets.grid.events.RowOverHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;

public class Selector extends SectionStackSection
{
	public static int SHOWSEARCHFIELD = 10;
	CriteraPanel criterapanel;
	public static DateTimeFormat defaultdateformater = DateTimeFormat.getFormat("yyyy-MM-dd");
	//HLayout toolbarItem;
	HLayout toolbarItem2;

	TextItem searchText;
	IButton searchbtn;
	EnhancedListGrid valuesGrid;
	HLayout formlayout = new HLayout(); // searchform
	VLayout layout = new VLayout(); // table + searchform + btn
	ListGridRecord[] records;

	protected  String property;
	protected String longname;
	protected String shortname;
	protected List<String> values;
	protected String propname;
	protected int numberOfValues = 0;
	protected int numberOfSelectedValues = 0;

	/**
	 * The tooltip shown per text
	 */
	protected String prompt ="";
	private DynamicForm searchForm;
	private String type;
	private boolean open;
	protected boolean loaded = false;
	public ListGridRecord[] getRecords()
	{
		return records;
	}

	public void updateCounts(HashMap<String, Integer> wCounts, HashMap<String, Integer> tCounts) {
		if (!loaded) return;
		for(ListGridRecord record : records)
		{
			record.setAttribute("n", wCounts.get(record.getAttribute("value")));
			record.setAttribute("t", tCounts.get(record.getAttribute("value")));
		}
		this.valuesGrid.refreshFields();
	}

	public void filterTexts(ArrayList<Record> selectedTexts) {
		boolean process = false;
		System.out.println("RECORDS: "+records);
		for (ListGridRecord record : records) {
			if (record.getAttributeAsBoolean("keep")) {
				process = true;
				break;
			}
		}
		if (process) // at least one of the values has been selected
		{	
			//numberOfSelectedValues = numberOfValues;
			for(ListGridRecord record : records) {
				if (!record.getAttributeAsBoolean("keep")) {
					//numberOfSelectedValues--;
					for(int i = 0 ; i < selectedTexts.size() ; i++)
					{
						Record text = selectedTexts.get(i);
						String value = record.getAttribute("value");
						if (text.getAttribute(property).equals(value))
						{
							selectedTexts.remove(i);
							i--;
						}
					}
				}
			}
		}
	}		

	protected Selector()
	{

	}

	public Selector(CriteraPanel cpanel, final String property, List<String> values, String type, boolean open)
	{
		this.criterapanel = cpanel;
		this.property = property;
		this.longname = property;
		this.shortname = property;
		this.type = type;
		this.open = open;
		if (cpanel.textSelectionTab.propertiesInfos.get(property) != null) {
			this.longname = cpanel.textSelectionTab.propertiesInfos.get(property).longname;
			this.shortname = cpanel.textSelectionTab.propertiesInfos.get(property).shortname;
			//System.out.println("long: "+longname+" short: "+shortname);
		}
		this.values = values;
		this.numberOfValues = +values.size();
		this.setResizeable(true);
		updateTitle();

		this.propname = property;
		this.addItem(layout);  

		valuesGrid = new EnhancedListGrid();
		valuesGrid.setHoverOpacity(75);

		KeyPressHandler ctrlFHandler = new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event.isCtrlKeyDown() && event.getKeyName().equals("F"))
				{
					hideShowSearchTool();
					event.cancel();
				}
			}
		};
		valuesGrid.addKeyPressHandler(ctrlFHandler);
		valuesGrid.addRowOverHandler(new RowOverHandler() {
			@Override
			public void onRowOver(RowOverEvent event) {
				loadData();
				valuesGrid.setPrompt(event.getRecord().getAttribute("value"));
			}
		});

		ListGridField keepField = new ListGridField("keep", "");
		keepField.setTitle(" ");
		keepField.setType(ListGridFieldType.BOOLEAN);
		keepField.addRecordClickHandler(new RecordClickHandler() {

			@Override
			public void onRecordClick(RecordClickEvent event) {
				
				ProcessingWindow.show(Services.getI18n().refreshingTextSelection());
				ListGridRecord record = valuesGrid.getSelectedRecord();
				boolean value = !record.getAttributeAsBoolean("keep");
				record.setAttribute("keep", value);
				if (value)
					numberOfSelectedValues++;
				else
					numberOfSelectedValues--;
				criterapanel.updateSelectedTexts();
				valuesGrid.refreshFields();

				updateTitle();
				ProcessingWindow.hide();
			}
		});

		ListGridField valueField = new ListGridField("value", "valeur");
		valueField.setPrompt(Services.getI18n().valueForProperty());
		valueField.setWidth("40%");
		if (type.equals("Integer"))
			valueField.setType(ListGridFieldType.INTEGER);
		else if (type.equals("Date")) {
			valueField.setType(ListGridFieldType.DATE);
		}
		else
			valueField.setType(ListGridFieldType.TEXT);

		ListGridField nTextField = new ListGridField("n", "n");
		nTextField.setPrompt(Services.getI18n().nTextsSelected());
		nTextField.setType(ListGridFieldType.INTEGER);
		ListGridField NTextField = new ListGridField("N", "N");
		NTextField.setPrompt(Services.getI18n().NTexts());
		NTextField.setType(ListGridFieldType.INTEGER);
		ListGridField nWordField = new ListGridField("t", "t");
		nWordField.setType(ListGridFieldType.INTEGER);
		nWordField.setPrompt(Services.getI18n().tWordsselected());
		ListGridField NWordField = new ListGridField("T", "T");
		NWordField.setType(ListGridFieldType.INTEGER);
		NWordField.setPrompt(Services.getI18n().TWords());
		valuesGrid.setFields(keepField, valueField, nTextField, NTextField, nWordField, NWordField);

		valuesGrid.setSize("100%", "100%");
		valuesGrid.setShowEmptyMessage(false);
		valuesGrid.draw(); 	

		valuesGrid.addDoubleClickHandler(new DoubleClickHandler() {
			@Override
			public void onDoubleClick(DoubleClickEvent event) {
				ProcessingWindow.show(Services.getI18n().refreshingTextSelection());
				ListGridRecord record = valuesGrid.getSelectedRecord();
				record.setAttribute("keep", !record.getAttributeAsBoolean("keep"));
				criterapanel.updateSelectedTexts();
				valuesGrid.refreshFields();
				ProcessingWindow.hide();
			}
		});

		Button revertbtn = new Button("");
		revertbtn.setIcon("icons/revert.png");
		revertbtn.setWidth("50%");
		revertbtn.setTooltip("Invert all");
		revertbtn.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				//System.out.println("revert");
				ProcessingWindow.show(Services.getI18n().refreshingTextSelection());
				for (ListGridRecord r : records)
					r.setAttribute("keep", !r.getAttributeAsBoolean("keep"));
				//valuesGrid.setData(records);
				numberOfSelectedValues = numberOfValues - numberOfSelectedValues;
				criterapanel.updateSelectedTexts();
				valuesGrid.refreshFields();
				ProcessingWindow.hide();
				updateTitle();
			}
		});
		//toolbarItem.setMembers(); 

		toolbarItem2 = new HLayout();
		toolbarItem2.setWidth100();

		Button okbtn = new Button("");
		okbtn.setIcon("icons/add.png");
		okbtn.setTooltip("Add");
		okbtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//System.out.println("keep");
				ProcessingWindow.show(Services.getI18n().refreshingTextSelection());
				for (ListGridRecord r : valuesGrid.getSelection()) {
					if (!r.getAttributeAsBoolean("keep")) {
						numberOfSelectedValues++;
						r.setAttribute("keep", true);
					}
				}

				criterapanel.updateSelectedTexts();
				valuesGrid.refreshFields();
				ProcessingWindow.hide();
				updateTitle();
			}
		});
		okbtn.setWidth("50%");
		Button removebtn = new Button("");
		removebtn.setIcon("icons/delete.png");
		removebtn.setTooltip("Remove");
		removebtn.setWidth("50%");
		removebtn.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				//System.out.println("Remove");
				ProcessingWindow.show(Services.getI18n().refreshingTextSelection());
				for (ListGridRecord r : valuesGrid.getSelection()) {
					if (r.getAttributeAsBoolean("keep")) {
						r.setAttribute("keep", false);
						numberOfSelectedValues--;
					}
				}

				criterapanel.updateSelectedTexts();
				valuesGrid.refreshFields();
				ProcessingWindow.hide();
				updateTitle();
			}
		});
		toolbarItem2.setMembers(okbtn, removebtn, revertbtn); 

		formlayout.addKeyPressHandler(ctrlFHandler);
		searchForm = new DynamicForm();
		searchForm.setTitleOrientation(TitleOrientation.TOP);  
		searchForm.setWidth100();
		searchForm.setBackgroundColor("#DDDDDD");

		searchText = new TextItem();  
		searchText.setName(property+"text");
		searchText.setShowTitle(false);
		searchText.setColSpan(1);
		searchText.addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				//System.out.println("key event "+event.getKeyName());
				if (event == null)
					return;
				if (event.getKeyName() == null)
					return;
				if (!event.getKeyName().equals("Enter"))
					return;
				searchValues();
			}
		});
		searchForm.setNumCols(1);
		searchForm.setColWidths("*");
		searchForm.setFields(searchText);
		searchForm.addKeyPressHandler(ctrlFHandler);
		PickerIcon goPicker = new PickerIcon(PickerIcon.SEARCH, new FormItemClickHandler() {  
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {

				ProcessingWindow.show(Services.getI18n().refreshingTextSelection());
				searchValues();
				ProcessingWindow.hide();
				valuesGrid.focus();
			}
		});

		PickerIcon hidePicker = new PickerIcon(PickerIcon.CLEAR, new FormItemClickHandler() {  
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				hideShowSearchTool();
				valuesGrid.focus();
			}
		});
		searchText.setIcons(goPicker, hidePicker);
		searchText.setWidth("100%");
		searchForm.setColWidths("*");
		
		layout.setMembers(valuesGrid, toolbarItem2);
		if (open) {
			loadData();
		}
	}
	
	public boolean loadData() {
		if (loaded ) return true;
		this.loaded  = true;
		records = new ListGridRecord[values.size()];
		numberOfValues = values.size();
		for (int i = 0 ; i < values.size() ; i++) {	
			ListGridRecord r = new ListGridRecord();
			//r.setAttribute("pk",i);
			if (CriteraPanel.selectAllAtStartUp)
				r.setAttribute("keep",true);
			else
				r.setAttribute("keep",false);

			if ("Integer".equals(type)) {
				try {r.setAttribute("value", Integer.parseInt(values.get(i)));}
				catch (Exception e){r.setAttribute("value", values.get(i));}
			}
			else if ("Date".equals(type)) {
				try {r.setAttribute("value", defaultdateformater.parse(values.get(i)));}
				catch (Exception e){r.setAttribute("value", values.get(i));}
			}
			else
				r.setAttribute("value", values.get(i));
			
			//System.out.println(values.get(i)+" -> "+r.getAttribute("value"));
			r.setAttribute("n", 0);
			r.setAttribute("N",criterapanel.textSelectionTab.getTextNumber(property, values.get(i)));
			r.setAttribute("t", 0);
			r.setAttribute("T",criterapanel.textSelectionTab.getNumberOfWord(property, values.get(i)));
			records[i] = r;
		}
		valuesGrid.setData(records);
		valuesGrid.sort("value", SortDirection.ASCENDING);
		if(open && records.length >= SHOWSEARCHFIELD)
			layout.addMember(searchForm);
		//updateCounts(counts, counts2);
		criterapanel.refreshSelectorCounts(this);
		return true;
	}

	public boolean hideShowSearchTool() {
		if (layout.hasMember(searchForm))
			layout.removeChild(searchForm);
		else {
			layout.addMember(searchForm);
			searchText.focusInItem();
		}
		return formlayout.getParent() != null;
	}

	String title;
	protected void updateTitle() {
		if (title == null) // the first time
		{
			if (CriteraPanel.selectAllAtStartUp)
				title = (shortname+" ("+numberOfValues+"/"+numberOfValues+")");
			else
				title = (shortname+" (0/"+numberOfValues+")");
		} else {
			title = shortname+" ("+numberOfSelectedValues+"/"+numberOfValues+")";
			if (numberOfSelectedValues == 0)
				setTitle(title);
			else			
				title = ("<b>"+title+"</b>");
		}
		setTitle(title);
	}

	public void raz() {
		numberOfSelectedValues = 0;
		if (valuesGrid!= null) {
			for (ListGridRecord r : records) {
				r.setAttribute("keep", false);
				r.setAttribute("n", 0);
			}
			valuesGrid.refreshFields();
		}
		updateTitle();
	}

	public void all() {
		numberOfSelectedValues = numberOfValues;
		if (valuesGrid!= null) {
			for (ListGridRecord r : records) {
				r.setAttribute("keep", true);
				r.setAttribute("n", r.getAttribute("N"));
			}
			valuesGrid.refreshFields();
		}
		updateTitle();
	}

	public void updatePropertyCounts() {
		numberOfSelectedValues = 0;
		if (records != null)
			for (Record r : records)
				if (r.getAttributeAsBoolean("keep"))
					numberOfSelectedValues++;
	}

	/**
	 * Called when user press button "search", and select the values in the table
	 * @return
	 */
	private boolean searchValues() {
		Object value = searchText.getValue();
		if (value!= null) {
			ArrayList<ListGridRecord> tmprecords = new ArrayList<ListGridRecord>();
			//System.out.println("regex "+searchText.getValue());
			String regex = value.toString();
			regex = regex.replaceAll("([$^<>|\\\"{()}?+*.])","\\\\$1"); // process special chars
			regex = ".*"+regex+".*"; // add .*
			regex = regex.toLowerCase();
			valuesGrid.deselectAllRecords();
			for (ListGridRecord r : records) {
				if (r.getAttribute("value").toLowerCase().matches(regex))
					tmprecords.add(r);
			}
			if (tmprecords.size() > 0) {
				valuesGrid.selectRecords(tmprecords.toArray(new ListGridRecord[]{}));
				valuesGrid.scrollToRow(valuesGrid.getRecordIndex(tmprecords.get(0)));
			}
			if (tmprecords.size() > 1)
				TXMWEB.getConsole().addMessage(tmprecords.size()+ " valeurs sélectionnées");
			else
				TXMWEB.getConsole().addMessage(Services.getI18n().oneValueHasBeenSelected());
		}
		return true;
	}

	public ArrayList<String> getVisibleCols() {
		ArrayList<String> cols = new ArrayList<String>();
		//valuesGrid.getFi
		for (ListGridField field : this.valuesGrid.getFields()) {
			if (!field.getName().equals("keep") && !field.getName().equals("value"))
				cols.add(field.getName());					
		}
		//System.out.println("cols: "+cols);
		return cols;
	}

	public String getPropertyName() {
		return this.propname;
	}

	public boolean shouldTest() {
		if (numberOfSelectedValues > 0)
			return true;
		return false;
	}

	public ArrayList<String> getSelectedPropertyValues() {
		ArrayList<String> list = new ArrayList<String>();
		if (records == null)
			return list;
		for (Record r : this.records)
			if (r.getAttributeAsBoolean("keep")) {
				list.add(r.getAttribute("value"));
			}
		return list;
	}

	public boolean isLoaded() {
		return loaded;
	}
}