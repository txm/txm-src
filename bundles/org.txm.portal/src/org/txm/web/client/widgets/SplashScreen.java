package org.txm.web.client.widgets;

import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;

/**
 * Modal Window that :
 *   - take all the screen
 *   - can be closed with a simple click
 *   - content read from an HTML file splash.jsp 
 *   
 * 
 * @author mdecorde
 *
 */
public class SplashScreen extends Window {
	/**
	 * 
	 * @param path relative path : URL
	 */
	public SplashScreen(String path) {
		
		
		
		this.setShowHeader(false);
		this.setShowFooter(false);
		this.setShowHeaderBackground(false);
		this.setShowShadow(false);
		this.setShowEdges(false);
		this.setShowResizer(false);
		this.setCanDrag(false);
		this.setCanDragResize(false);
		this.setUseBackMask(false);
		this.setBodyStyle("none");
		this.setBackgroundImage("none");
		
		this.setWidth100();
        this.setHeight100();
        this.setBackgroundColor("transparent");
        
        this.setBorder("none");
        this.setBodyColor("transparent");
        this.setHiliteBodyColor("transparent");
        
        this.setIsModal(true);  
        this.setShowModalMask(false);  
        this.centerInPage();  
        
        this.addCloseClickHandler(new CloseClickHandler() {  
            public void onCloseClick(CloseClickEvent event) {  
                destroy();  
            }  
        });
        
        this.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				destroy(); 
			}
		});
        
        HTMLFlow frame = new HTMLFlow();
        frame.setBackgroundColor("transparent");
        
        frame.setHeight100();  
        frame.setWidth100();  
        frame.setContentsURL(path);
        
        this.addItem(frame);  
	}
}
