package org.txm.web.client.widgets.tabs.textselection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.EnhancedListGrid;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;

public class DateSelector extends Selector{

	DateSlider slider;
	String inputFormat = "yyyy-MM-dd";
	String outputFormat = "yyyy-MM-dd";
	public DateTimeFormat datereader = DateTimeFormat.getFormat(inputFormat);
	public DateTimeFormat dateformater = DateTimeFormat.getFormat(outputFormat);

	String[] strdates;
	ArrayList<Date> dates;

	public DateSelector(CriteraPanel cpanel, final String property,
			List<String> values, String type) {
		this.criterapanel = cpanel;
		this.property = property;
		this.longname = cpanel.textSelectionTab.propertiesInfos.get(property).longname;
		this.values = values;
		this.propname = property;

		this.setResizeable(true);
		
		this.inputFormat = cpanel.textSelectionTab.propertiesInfos.get(property).extra.get("inputFormat");
		this.outputFormat = cpanel.textSelectionTab.propertiesInfos.get(property).extra.get("outputFormat");
		if (inputFormat == null)
			inputFormat = "yyyy-MM-dd";
		if (outputFormat == null)
			outputFormat = "yyyy-MM-dd";
		datereader = DateTimeFormat.getFormat(inputFormat);
		dateformater = DateTimeFormat.getFormat(outputFormat);

		if (cpanel.textSelectionTab.propertiesInfos.get(property) != null) {
			this.longname = cpanel.textSelectionTab.propertiesInfos.get(property).longname;
			this.shortname = cpanel.textSelectionTab.propertiesInfos.get(property).shortname;
		}

		HashSet<Date> set = new HashSet<Date>();
		for (int i = 0 ; i < values.size() ; i++) {
			String value = values.get(i);
			try {set.add(datereader.parse(value));}
			catch (Exception e){ System.out.println("Malformed date: "+value); values.remove(i); i--; };
		}
		dates = new ArrayList<Date>(set);
		Collections.sort(dates);

		this.numberOfValues = dates.size();

		if (type == null)
			type= "";
		else
			type =" - "+type;

		if (CriteraPanel.selectAllAtStartUp)
			this.setTitle("<b>"+shortname+"</b>");

		VLayout layout = new VLayout();
		this.addItem(layout);  

		//init records
		records = new ListGridRecord[dates.size()];
		for (int i = 0 ; i < dates.size() ; i++) {	
			Date date = dates.get(i);
			//String value = dateformater.format(date);

			ListGridRecord r = new ListGridRecord();
			//r.setAttribute("pk",i);
			if (CriteraPanel.selectAllAtStartUp)
				r.setAttribute("keep",true);
			else
				r.setAttribute("keep",false);
			r.setAttribute("value", date);
			r.setAttribute("n", 0);
			r.setAttribute("N",criterapanel.textSelectionTab.getTextNumber(property, values.get(i)));
			r.setAttribute("t", 0);
			r.setAttribute("T",criterapanel.textSelectionTab.getNumberOfWord(property, values.get(i)));
			records[i] = r;
		}

		slider = new DateSlider(dates);// dates are sorted and uniq

		valuesGrid = new EnhancedListGrid();

		ListGridField keepField = new ListGridField("keep", "");
		keepField.setTitle(" ");
		keepField.setType(ListGridFieldType.BOOLEAN);
		keepField.addRecordClickHandler(new RecordClickHandler() {

			@Override
			public void onRecordClick(RecordClickEvent event) {
				ProcessingWindow.show(Services.getI18n().refreshingTextSelection());
				ListGridRecord record = valuesGrid.getSelectedRecord();
				record.setAttribute("keep", !record.getAttributeAsBoolean("keep"));
				criterapanel.updateSelectedTexts();
				valuesGrid.refreshFields();
				//this command does not refresh the ui...
				updateTitle();
				ProcessingWindow.hide();
			}
		});
		
		//keepField.setCanEdit(false);
		ListGridField valueField = new ListGridField("value", "valeur");
		valueField.setType(ListGridFieldType.DATE);
		valueField.setWidth("40%");
		valueField.setCellFormatter(new CellFormatter() {
			@Override
			public String format(Object value, ListGridRecord record, int rowNum,
					int colNum) {
				
				if (value == null) return "error";
				if (value instanceof Date)
					return dateformater.format((Date) value);
				return value.toString();
			}
		});
		
		ListGridField nTextField = new ListGridField("n", "n");
		nTextField.setPrompt(Services.getI18n().nTextsSelected());
		nTextField.setType(ListGridFieldType.INTEGER);
		ListGridField NTextField = new ListGridField("N", "N");
		NTextField.setPrompt(Services.getI18n().NTexts());
		NTextField.setType(ListGridFieldType.INTEGER);
		ListGridField nWordField = new ListGridField("t", "t");
		nWordField.setType(ListGridFieldType.INTEGER);
		nWordField.setPrompt(Services.getI18n().tWordsselected());
		ListGridField NWordField = new ListGridField("T", "T");
		NWordField.setType(ListGridFieldType.INTEGER);
		NWordField.setPrompt(Services.getI18n().TWords());
		valuesGrid.setFields(keepField, valueField, nTextField, NTextField, nWordField, NWordField);

		valuesGrid.setSize("100%", "100%");
		valuesGrid.setShowEmptyMessage(false);
		valuesGrid.setData(records);
		valuesGrid.draw(); 	

		layout.addMember(valuesGrid);
		layout.addMember(slider); 
		updateTitle();
		loaded = true;
	}

	public void raz() {
		super.raz();
		updateTitle();
	}

	public void all() {
		super.all();
		updateTitle();
	}

	public void updateSelectedCriteria()
	{
		String minvalue = slider.infDates.getValueAsString();
		String maxvalue = slider.supDates.getValueAsString();
		Date infdate = slider.hashmap.get(minvalue);
		Date supdate = slider.hashmap.get(maxvalue);
		if (infdate == null) {
			try {infdate = dateformater.parse(minvalue);}
			catch (Exception e){TxmDialog.severe(Services.getI18n().wrongDateFormat(outputFormat, minvalue));}
		}
		if (supdate == null) {
			try {supdate = dateformater.parse(maxvalue);}
			catch (Exception e){TxmDialog.severe(Services.getI18n().wrongDateFormat(outputFormat, maxvalue));}
		}

		numberOfSelectedValues = 0;
		if (infdate.compareTo(supdate) < 0) {
			for (ListGridRecord record : records) {
				Date date = record.getAttributeAsDate("value");
				boolean bool = infdate.compareTo(date) <= 0 & date.compareTo(supdate) <= 0;
				record.setAttribute("keep", bool);
				if (bool)
					numberOfSelectedValues++;
			}
		} else {
			for(ListGridRecord record : records)
				record.setAttribute("keep", false);
		}
		
		/*Date infdate = slider.getInfDate();
		Date supdate = slider.getSupDate();
		Date date;
		System.out.println("update criteria: "+infdate+" to "+supdate);

		for(ListGridRecord record : records)
		{
			date = record.getAttributeAsDate("date");
			System.out.println("comp date inf: "+date.compareTo(infdate));
			System.out.println("comp date sup: "+date.compareTo(supdate));
			boolean bool = date.compareTo(infdate) >= 0 & date.compareTo(supdate) <= 0;
			System.out.println("date "+date+" : "+bool);
			record.setAttribute("keep", bool);
		}*/
	}

	public void filterTexts(ArrayList<Record> selectedTexts) {
		//System.out.println("date selector filter");
		if (shouldTest()) {// at least one of the values has been selected
			numberOfSelectedValues = numberOfValues;
			for (ListGridRecord record : records) {
				if (!record.getAttributeAsBoolean("keep")) {
					Date date = record.getAttributeAsDate("value");
					numberOfSelectedValues--;
					for (int i = 0 ; i < selectedTexts.size() ; i++) {
						Record text = selectedTexts.get(i);
						try {
							Date textDate = text.getAttributeAsDate(property);
							if (date.equals(textDate)) {
								selectedTexts.remove(i);
								i--;
							}
						} catch(Exception e){System.out.println(e);selectedTexts.remove(i);i--;}
					}
				}
			}
		} else {
			numberOfSelectedValues = 0;
		}
	}		

	public ArrayList<String> getVisibleCols()
	{
		ArrayList<String> cols = new ArrayList<String>();
		return cols;
	}

	public void updateCounts(HashMap<String, Integer> counts, HashMap<String, Integer> counts2) {

	}

	public boolean shouldTest() {
		return numberOfSelectedValues > 0;			
	}

	public String getFrom() {
		return slider.infDates.getValueAsString();
	}
	
	public String getTo() {
		return slider.supDates.getValueAsString();
	}

	public class DateSlider extends VLayout{

		ComboBoxItem infDates;
		ComboBoxItem supDates;
		IButton okButton;

		public LinkedHashMap<String, Date> hashmap = new LinkedHashMap<String, Date>();

		/**
		 * @param dates sorted dates
		 */
		public DateSlider(ArrayList<Date> dates) {
			strdates = new String[dates.size()]; // for combos
			for (int i = 0 ; i < dates.size() ; i++) {
				strdates[i] = dateformater.format(dates.get(i));
				hashmap.put(strdates[i], dates.get(i));
			}

			final DynamicForm infform = new DynamicForm();  
			infform.setNumCols(5);
			infform.setColWidths("*", "*", "*", "*", 10);

			infDates = new ComboBoxItem(); 
			infDates.setTitle(Services.getI18n().fromSpinner());
			infDates.setType("comboBox"); 
			infDates.setValueMap(strdates);
			if (strdates.length > 0)
				infDates.setValue(strdates[0]);

			supDates = new ComboBoxItem(); 
			supDates.setTitle(Services.getI18n().toSpinner());
			supDates.setType("comboBox");  
			supDates.setValueMap(strdates);  
			if (strdates.length > 0) {
				if (CriteraPanel.selectAllAtStartUp)
					supDates.setValue(strdates[strdates.length -1]);
				else
					supDates.setValue(strdates[0]);
			}
			
			ButtonItem goPicker = new ButtonItem("OK", "OK");
			goPicker.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				
				@Override
				public void onClick(
						com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					ProcessingWindow.show(Services.getI18n().refreshingTextSelection());
					updateSelectedCriteria();
					criterapanel.updateSelectedTexts();
					valuesGrid.refreshFields();
					ProcessingWindow.hide();
					updateTitle();
				}
			});
			/*, new FormItemClickHandler() {  
						@Override
						public void onFormItemClick(FormItemIconClickEvent event) {
							
						}  
			         });  */
//			goPicker.setShowOver(true);
//			supDates.setIcons(goPicker);
			
			infform.setFields(infDates, supDates, goPicker);
			//infform.setColWidths("*", "*", 10);
			infform.setWidth("100%");

			this.addMember(infform);
		}

		public void updateTitle() {
			if (shouldTest())
				DateSelector.this.setTitle("<b>"+shortname+" ("+numberOfSelectedValues+"/"+numberOfValues+")"+"</b>");
			else
				DateSelector.this.setTitle(shortname+" ("+numberOfSelectedValues+"/"+numberOfValues+")");
		}

		public Date stringToDate(String str) {
			return dateformater.parse(str);
		}
	}

}
