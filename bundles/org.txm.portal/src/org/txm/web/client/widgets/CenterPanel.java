package org.txm.web.client.widgets;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.tabs.HTMLTab;
import org.txm.web.client.widgets.tabs.TxmCenterTab;

import com.smartgwt.client.types.TabBarControls;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.events.CloseClickHandler;
import com.smartgwt.client.widgets.tab.events.TabCloseClickEvent;

/**
 * Contains a tab panel.
 * by default contains the homepage of the user's profile.
 * @author vchabanis
 *
 */
public class CenterPanel extends TabSet {

	HashMap<String, HTMLTab> tabs = new HashMap<String, HTMLTab>();
	HashSet<String> uniqTabNames = new HashSet<String>();
	//	OMSVGDocument doc = OMSVGParser.createDocument();
	//	OMSVGSVGElement elem = OMSVGParser.parse("<?xml version='1.0' encoding='UTF-8' standalone='yes'?><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' ><path d='M 16.37319893494319 6.228651858192045 C 17.68203295396305 6.228651858192045 18.848041620877247 6.833011847152678 19.607352155717813 7.778415502222478 L 13.139393584795739 7.778415502222478 C 13.89862207788195 6.833011847152678 15.064415194099919 6.228651858192045 16.37319893494319 6.228651858192045 ZM 20.044973404694307 8.446327106383741 C 20.294568541382407 8.921450439612439 20.454828870370374 9.451162283913659 20.503119020673672 10.013136411145373 L 12.243626719839877 10.013136411145373 C 12.291910500441753 9.451162283913659 12.452206414828348 8.921450439612439 12.701772335819244 8.446327106383741 ZM 20.507293468199684 10.681048015306637 C 20.466355482069382 11.241826337257464 20.314283701516224 11.77144713729298 20.072803054867695 12.247857320068269 L 12.673942685645859 12.247857320068269 C 12.432490914977121 11.77144713729298 12.280384992824324 11.241826337257464 12.23945227231387 10.681048015306637 ZM 19.649096630977894 12.915768924229534 C 18.891161951645167 13.891506626214397 17.706396807126318 14.518756774216566 16.37319893494319 14.518756774216566 C 15.040088370134269 14.518756774216566 13.855503955277191 13.891463142386002 13.09764910953566 12.915768924229534 ZM 16.37319893494319 5.56074025403078 C 13.720072892303293 5.56074025403078 11.560408802775166 7.720760022378405 11.56040880808325 10.37387825151789 C 11.56040880808325 13.026995121787735 13.720073062161998 15.186668378377831 16.37319893494319 15.186668378377831 C 19.026401414000194 15.186668378377831 21.1863369324303 13.026996480657374 21.1863369324303 10.37387825151789 C 21.1863369324303 7.720758663508768 19.02640209343501 5.56074025403078 16.37319893494319 5.56074025403078 Z' style='fill:black cmyk(0, 0, 0, 100);stroke:none' /><path d='M 16.373372425496118 21.2910099483688 Q 19.79032868831984 21.2910099483688 23.620070267151824 22.942156520486396 Q 19.79032868831984 24.54743752668351 16.373372425496118 24.54743752668351 Q 12.979349741845319 24.54743752668351 9.12667538005309 22.942156520486396 Q 12.979349741845319 21.2910099483688 16.373372425496118 21.2910099483688 ZM 16.373372425496118 20.236111486749852 Q 12.15377645578652 20.236111486749852 6.6728883048369365 22.919223737526153 Q 12.15377645578652 25.57940453236335 16.373372425496118 25.57940453236335 Q 20.592968395205716 25.57940453236335 26.073856546155298 22.919223737526153 Q 20.61590144357018 20.236111486749852 16.373372425496118 20.236111486749852 Z' style='fill:black cmyk(0, 0, 0, 100);stroke:none' /><path d='M 28.298152864992254 16.117324441036295 L 28.298152864992254 25.579405499987537 L 35.70779722365628 25.579405499987537 L 35.70779722365628 24.466219493052098 L 29.411338871927693 24.466219493052098 L 29.411338871927693 17.230510447971735 L 47.987630362662856 17.230510447971735 L 47.987630362662856 24.466219493052098 L 42.352126202552185 24.466219493052098 L 42.352126202552185 25.579405499987537 L 49.10081636959829 25.579405499987537 L 49.10081636959829 16.117324441036295 Z' style='fill:black cmyk(0, 0, 0, 100);stroke:none' /></svg>");

	public CenterPanel(String id) {
		
		this.setID(id); // selenium
		uniqTabNames.addAll( Arrays.asList(Services.getI18n().homeTab(), Services.getI18n().homeTab()+"_expo", Services.getI18n().helpTab(), Services.getI18n().contactTab()));
		Layout paneContainerProperties = new Layout();
		paneContainerProperties.setLayoutMargin(0);
		paneContainerProperties.setPadding(0);
		paneContainerProperties.setMargin(0);
		
		this.setPaneContainerProperties(paneContainerProperties);
		this.setWidth100();
		this.setHeight100();
		this.setPadding(0);
		this.setMargin(0);
		this.setTabBarControls(TabBarControls.TAB_SCROLLER, TabBarControls.TAB_PICKER);
		
		this.addCloseClickHandler(new CloseClickHandler() {
			
			@Override
			public void onCloseClick(TabCloseClickEvent event) {
				
				Tab tab = event.getTab();
				if (tab instanceof TxmCenterTab) {
					((TxmCenterTab)tab).onClose();
				}
			}
		});
		
		/*OMSVGDocument doc = OMSVGParser.createDocument();
		OMSVGSVGElement svg = doc.createSVGSVGElement();
		svg.setWidth(OMSVGLength.SVG_LENGTHTYPE_PX, 300);
		svg.setHeight(OMSVGLength.SVG_LENGTHTYPE_PX, 300);
		OMSVGCircleElement circle = doc.createSVGCircleElement(150, 150, 100);
		circle.getStyle().setSVGProperty(SVGConstants.CSS_FILL_PROPERTY, "blue");
		svg.appendChild(circle);
		RootPanel.get().getElement().appendChild(svg.getElement());
		 */
		
		//		Tab testTab = new Tab("TEST ONLY");
		//		List<String> values = Arrays.asList(Keys.WORD, "pos", "lemme", "msd");
		//		DynamicForm form = new DynamicForm();
		//		form.setFields(new OrderedPickList());
		//		testTab.setPane(form);
		//		this.addTab(testTab);
	}

	public void updateUserPage(String page) {
		
		tabs.get(page).update();
	}

	public void updateUserPages() {
		
		for(Tab tab : this.getTabs())
			if(tab != null && tab instanceof HTMLTab)
				((HTMLTab)tab).update();
	}
	
	public HTMLTab containsTab(Tab tab2) {
		
		for(Tab tab : this.getTabs()) {
			if(tab.getTitle().equals(tab2.getTitle()))
				return (HTMLTab)tab;
		}
		return null;
	}
	
	public HTMLTab containsTab(String textid) {
		
		for(Tab tab : this.getTabs()) {
			if(tab.getTitle().equals(textid))
				return (HTMLTab)tab;
		}
		return null;
	}
	
	public HTMLTab getUniqTab(String key) {
		
		HTMLTab tab = containsTab(key);
		if (tab == null) {	
			tab = new HTMLTab(key, key);
			tab.addInView();
			//tabs.put(key, tab);
		}
		tab.update();
		return tab;
	}
}