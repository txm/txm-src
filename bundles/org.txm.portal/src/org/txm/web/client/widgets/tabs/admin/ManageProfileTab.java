package org.txm.web.client.widgets.tabs.admin;

import java.util.List;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.shared.Keys;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.MultipleAppearance;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class ManageProfileTab extends Tab {

	private SelectItem deleteProfileSelection = new SelectItem();
	private SelectItem editProfileSelection = new SelectItem();
	private AdminTab adminTab;
	
	public ManageProfileTab(AdminTab adminTab) {
		this.adminTab = adminTab;
		try {
		this.setTitle(Services.getI18n().manageProfile());
		
		VLayout content = new VLayout();
		content.setMembers(edit(), delete(), create(), infos());
		this.setPane(content);
		} catch (Exception e) {
			TxmDialog.severe("Internal Error: "+e);
		}
	}
	
	private Layout infos()
	{
		String permissionslist = 
			"<b>Liste des permissions</b><br/>" +
			"TxmAllPermission : tous les droits<br/>" +
			"DisplayPermission : l'objet est vue dans la vue de gauche<br/>" +
			"DocumentationPermission : on peut ouvrir l'accueil '/CORPUS/Documentation' d'un corpus<br/>" +
			"DownloadPermission : on peut télécharger le corpus binaire d'un corpus<br/>" +
			"TextsPermission : on peut ouvrir la liste des textes d'un corpus<br/>" +
			"BiblioPermission : on peut ouvrir la biblio d'un texte<br/>" +
			"ConcordancePermission : on peut faire des concordances<br/>" +
			"CooccurrencePermission : on peut faire des cooccurrences<br/>" +
			"CreatePartitionPermission : on peut créer des partitions<br/>" +
			"CreateSubcorpusPermission : on peut créer des sous-corpus (comprend la finalisation de la sélection)<br/>" +
			"DeletePermission : on peut supprimer<br/>" +
			"PropertiesPermission : on peut afficher les propriétés de corpus ou partition<br/>" +
			"EditionPermission : on peut voir l'édition<br/>" +
			"LexiconPermission : on peut faire des lexiques<br/>" +
			"IndexPermission : on peut calculer un index<br/>" +
			"ReferencerPermission : on peut rechercher les références<br/>" +
			"SpecificitiesPermission : on peut faire des spécificités<br/>" +
			"TextSelectionPermission : on peut ouvrir l'onglet de sélection<br/>" +
			"TsQueryPermission : on peut faire des requêtes TIGERSearch<br/>";
		HLayout content = new HLayout();
		content.setAutoHeight();
		Label title = new Label(permissionslist);
		title.setAutoHeight();
		title.setWidth100();
		title.setCanSelectText(true);
		content.setMembers(title);
		return content;
	}
	
	private DynamicForm create() {
		HLayout content = new HLayout();
		content.setAutoHeight();
		final DynamicForm form = new DynamicForm();
		final TextItem profile = new TextItem();
		profile.setName("name");
		profile.setShowTitle(false);
		
		ButtonItem create = new ButtonItem(Services.getI18n().adminCreate());
		create.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String name = form.getValueAsString(Keys.NAME);
				if (name == null) {
					TxmDialog.warning("Give a name to the new profile");
					return;
				}
				/*if (!name.endsWith(".xml")) {
					name += ".xml";
				}*/
				ProcessingWindow.show("Creating new profile: "+name);
				Services.getAdminService().createProfile(name, new AsyncCallback<Boolean>() {
					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						SC.warn("Fail to create new profile");
						
					}
					@Override
					public void onSuccess(Boolean result) {
						ProcessingWindow.hide();
						if (result == false) {
							onFailure(null);
							return ;
						}
						updateView();
						adminTab.getUserTab().updateProfiles();
						SC.say("Profile was correctly created");
					}
				});
			}
		});

		form.setItems(profile, create);
		form.setNumCols(2);
		form.setCanDragResize(true);
	    form.setIsGroup(true);
	    form.setGroupTitle(Services.getI18n().createProfile());

		return form;
	}
	
	private DynamicForm delete() {
//		HLayout content = new HLayout();
//		content.setAutoHeight();
		final DynamicForm form = new DynamicForm();
		deleteProfileSelection.setName("deleteProfileSelection");
		deleteProfileSelection.setShowTitle(false);
		deleteProfileSelection.setMultiple(true);
		deleteProfileSelection.setMultipleAppearance(MultipleAppearance.PICKLIST);
		deleteProfileSelection.setShowAllOptions(true);
		ProcessingWindow.show("Listing profiles");
		updateView();
		ButtonItem delete = new ButtonItem(Services.getI18n().delete());
		delete.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String value = form.getValueAsString("deleteProfileSelection");
				System.out.println("VALUE: "+value);
				if (value == null) return;
				String[] items = value.split(",");
				if (items == null)
					return;
				ProcessingWindow.show("Deleting profile");
				Services.getAdminService().deleteProfiles(items, new AsyncCallback<Boolean>() {
					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						SC.warn("Fail");
					}
					@Override
					public void onSuccess(Boolean result) {
						ProcessingWindow.hide();
						updateView();
						adminTab.getUserTab().updateProfiles();
						SC.say("Success");
					}
				});
			}
		});

		form.setNumCols(2);
		form.setItems(deleteProfileSelection, delete);
		form.setCanDragResize(true);
	    form.setIsGroup(true);
	    form.setGroupTitle(Services.getI18n().deleteProfile());
	    
//		content.setMembers(form, delete);
//		VLayout result = new VLayout();
//		result.setMembers(content);
//		result.setPadding(10);
//		result.setBorder("1px solid #DDDDDD");
//		result.setAutoHeight();
		return form;
	}
	
	private DynamicForm edit() {
		VLayout content = new VLayout();
		content.setAutoHeight();
		content.setCanDragResize(true);
		final TextAreaItem textEditor = new TextAreaItem("textEditor");
		final DynamicForm form = new DynamicForm();
		
		textEditor.setHeight("*");
		textEditor.setWidth("100%");
		
		editProfileSelection.setTitle(Services.getI18n().adminSelectAProfile());
		
		textEditor.setTitle(Services.getI18n().adminEditTheProfile());
		editProfileSelection.setName("editProfileSelection");
		editProfileSelection.setShowAllOptions(true);
		Services.getAdminService().listProfiles(new AsyncCallback<List<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				SC.warn("No profile available");
			}
			@Override
			public void onSuccess(List<String> result) {
				editProfileSelection.setValueMap(result.toArray(new String[result.size()]));
			}
		});
		editProfileSelection.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				Services.getAdminService().getProfileContent((String)event.getValue(), new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						SC.warn("Fail");
					}
					@Override
					public void onSuccess(String result) {
						textEditor.setValue(result);
					}
				});
			}
		});

		ButtonItem validate = new ButtonItem(Services.getI18n().adminValidate());

		validate.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String content = form.getValueAsString("textEditor");
				String filePath = form.getValueAsString("editProfileSelection");
				if (filePath == null || filePath.trim().length() == 0) {
					TxmDialog.severe("no profile selected");
					return;
				}
				if (content == null || content.trim().length() == 0) {
					TxmDialog.severe("no content in the profile definition");
					return;
				}
				ProcessingWindow.show("Updating profile content");
				Services.getAdminService().updateProfileContent(content, filePath, new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						SC.warn("Fail");
					}
					@Override
					public void onSuccess(String result) {
						ProcessingWindow.hide();
						if (result.length() == 0)
							SC.say("Success");
						else
							SC.warn(result);
						updateView();
						
					}
				});
			}
		});
		
		form.setItems(editProfileSelection, textEditor, validate);
		form.setHeight("300px");
		form.setCanDragResize(true);
	    form.setIsGroup(true);
	    form.setGroupTitle(Services.getI18n().editProfile());

		return form;
	}
	
	public void updateView() {
		Services.getAdminService().listProfiles(new AsyncCallback<List<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				SC.warn("No profile available");
			}
			@Override
			public void onSuccess(List<String> result) {
				editProfileSelection.clearValue();
				deleteProfileSelection.clearValue();
				editProfileSelection.show();
				deleteProfileSelection.show();
				editProfileSelection.setValueMap(result.toArray(new String[result.size()]));
				
				result.remove("Anonymous");//TODO: why remove Anonymous.xml of the profile list of adminPanel ?
				result.remove("Admin");//TODO: why remove Anonymous.xml of the profile list of adminPanel ?
				result.remove("Newbie");//TODO: why remove Anonymous.xml of the profile list of adminPanel ?
				deleteProfileSelection.setValueMap(result.toArray(new String[result.size()]));
				ProcessingWindow.hide();
			}
		});
	}
}
