package org.txm.web.client.widgets.tabs.textselection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.shared.MetadataInfos;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.layout.VLayout;

public class GroupedDateSelector extends Selector{

	public DateSlider slider;
	public String inputFormat = "yyyy-MM-dd";
	public String outputFormat = "yyyy";
	public DateTimeFormat datereader = DateTimeFormat.getFormat(inputFormat);
	public DateTimeFormat dateformater = DateTimeFormat.getFormat(outputFormat);

	String[] strmindates;
	String[] strmaxdates;
//	List<String> strmindateslist;
//	List<String> strmaxdateslist;

	ArrayList<Date> dates; // all dates
	ArrayList<ArrayList<Date>> datespervalue; // all date grouped by groupvalue
	int currentinfindex;
	int currentsupindex;
	List<String> values;

	List<String> dateProperties;
	public GroupedDateSelector(CriteraPanel criteraPanel, MetadataInfos infos, List<String> dateProperties) {
		this.criterapanel = criteraPanel;
		this.property = infos.name;
		this.longname = infos.longname;
		this.shortname = infos.shortname;
		this.propname = infos.name;
		this.dateProperties = dateProperties;
		assert(dateProperties != null);
		assert(dateProperties.size() == 2);

		// get dates intput and output formats
		inputFormat = infos.extra.get("inputFormat");
		outputFormat = infos.extra.get("outputFormat");
		if (inputFormat == null)
			inputFormat = "yyyy-MM-dd";
		if (outputFormat == null)
			outputFormat = "yyyy-MM-dd";
		datereader = DateTimeFormat.getFormat(inputFormat);
		dateformater = DateTimeFormat.getFormat(outputFormat);
		this.setResizeable(true);
		
		// Build dates
		HashSet<Date> minDatesSet = new HashSet<Date>(); // no duplicate
		HashSet<Date> maxDatesSet = new HashSet<Date>(); // no duplicate
		for (String str : criteraPanel.textSelectionTab.valuesPerProperty.get(dateProperties.get(0)))
		{
			try {minDatesSet.add(datereader.parse(str));}
			catch (Exception e) {} // malformed date or pattern
		}
		for (String str : criteraPanel.textSelectionTab.valuesPerProperty.get(dateProperties.get(1)))
		{
			try {maxDatesSet.add(datereader.parse(str));}
			catch (Exception e) {} // malformed date or pattern
		}

		ArrayList<Date> minDates = new ArrayList<Date>(minDatesSet);
		ArrayList<Date> maxDates = new ArrayList<Date>(maxDatesSet);
		Collections.sort(minDates);// sort dates
		Collections.sort(maxDates);// sort dates

		slider = new DateSlider(minDates, maxDates); // create the Date selection widget
		this.addItem(slider); 
		this.updateTitle();
		loaded = true;
	}
	
	public void updateTitle()
	{
		if (shouldTest()) {
			setTitle("<b>"+shortname+"</b>");
		} else {
			setTitle(shortname);
		}
	}

	public void raz()
	{
		slider.infDates.setValue(strmindates[0]);
		slider.supDates.setValue(strmaxdates[0]);
		updateTitle();
	}

	public void all()
	{
		slider.infDates.setValue(strmindates[0]);
		slider.supDates.setValue(strmaxdates[strmaxdates.length - 1]);
		updateTitle();
	}

	public boolean shouldTest()
	{
		if(slider.infDates.getValueAsString().equals(strmindates[0]) &&
				slider.supDates.getValueAsString().equals(strmaxdates[0]))
			return false;
		return true;		
	}

	public void filterTexts(ArrayList<Record> selectedTexts) {

		if (!shouldTest())
			return;

		Date supdate = slider.getSupDate();
		Date infdate = slider.getInfDate();
		//System.out.println("filter: inf: "+infdate+" sup: "+supdate);
		for (int i = 0 ; i < selectedTexts.size() ; i++) {
			Record text = selectedTexts.get(i);
			try {
				Date mindate = text.getAttributeAsDate(dateProperties.get(0));
				Date maxdate = text.getAttributeAsDate(dateProperties.get(1));

				if (mindate == null || maxdate == null || 
						mindate.compareTo(supdate) > 0 || maxdate.compareTo(infdate) < 0)
				{
					selectedTexts.remove(i);
					i--;
				}
			}
			catch(Exception e){System.out.println(e);selectedTexts.remove(i);i--;}
		}

	}		

	public ArrayList<String> getVisibleCols()
	{
		ArrayList<String> cols = new ArrayList<String>();
		return cols;
	}

	public void updateCounts(HashMap<String, Integer> counts, HashMap<String, Integer> counts2) {

	}

	public class DateSlider extends VLayout{

		ComboBoxItem infDates;
		ComboBoxItem supDates;

		ArrayList<Date> mindates;
		ArrayList<Date> maxdates;
		HashMap<String, Date> hashmap = new HashMap<String, Date>();

		public void clearSelection()
		{
			infDates.setValue(strmindates[0]);
			supDates.setValue(strmaxdates[0]);
		}

		/**
		 * mindates and maxdates are sorted and uniq
		 * @param mindates
		 * @param maxdates
		 */
		public DateSlider(ArrayList<Date> mindates, final ArrayList<Date> maxdates)
		{
			this.mindates = mindates;
			this.maxdates = maxdates;

			strmindates = new String[mindates.size()];
			strmaxdates = new String[maxdates.size()];
			for (int i = 0 ; i < mindates.size() ; i++) {
				strmindates[i] = dateformater.format(mindates.get(i));
				hashmap.put(strmindates[i], mindates.get(i));
			}
			for (int i = 0 ; i < maxdates.size() ; i++) {
				strmaxdates[i] = dateformater.format(maxdates.get(i));
				hashmap.put(strmaxdates[i], maxdates.get(i));
			}

			final DynamicForm infform = new DynamicForm();  
			infform.setNumCols(5);
			infform.setColWidths("*", "*", "*", "*", 10);
			
			infDates = new ComboBoxItem(); 
			infDates.setWidth(100);
			infDates.setTitle(Services.getI18n().fromSpinner());
			infDates.setType("comboBox");
			infDates.setValueMap(strmindates);
			if (strmindates.length > 0)
				infDates.setValue(strmindates[0]);

			supDates = new ComboBoxItem(); 
			supDates.setWidth(100);
			supDates.setTitle(Services.getI18n().toSpinner());
			supDates.setType("comboBox");  
			supDates.setValueMap(strmaxdates);  
			if (strmaxdates.length > 0) {
				if (CriteraPanel.selectAllAtStartUp)
					supDates.setValue(strmaxdates[strmaxdates.length -1]);
				else
					supDates.setValue(strmaxdates[0]);
			}
			
			ButtonItem goPicker = new ButtonItem("OK", "OK");
			goPicker.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				
				@Override
				public void onClick(
						com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					ProcessingWindow.show(Services.getI18n().refreshingTextSelection());
					criterapanel.updateSelectedTexts();
					ProcessingWindow.hide();
					updateTitle();
				}
			});
//			goPicker.setShowOver(true);
//			supDates.setIcons(goPicker);
			
			infform.setFields(infDates, supDates, goPicker);
			infform.setColWidths("*", "*", 10);
			infform.setWidth("100%");
			
			this.addMember(infform);
		}
		
		String minvalue;
		public Date getInfDate()
		{
			minvalue = infDates.getValueAsString();
			Date date = hashmap.get(minvalue);
			if (date == null) {
				try {date = dateformater.parse(minvalue);}
				catch (Exception e) {TxmDialog.severe(Services.getI18n().wrongDateFormat(outputFormat, minvalue));}
			}
			return date;
		}

		String maxvalue;
		public Date getSupDate()
		{
			maxvalue = supDates.getValueAsString();
			Date date = hashmap.get(maxvalue);
			if (date == null) {
				try {date = dateformater.parse(maxvalue);}
				catch (Exception e) {TxmDialog.severe(Services.getI18n().wrongDateFormat(outputFormat, maxvalue));}
			}
			return date;
		}
	}

	public String getFrom() {
		return slider.infDates.getValue().toString();
	}

	public String getTo() {
		return slider.supDates.getValue().toString();
	}
}