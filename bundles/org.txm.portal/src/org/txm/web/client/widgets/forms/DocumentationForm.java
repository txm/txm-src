package org.txm.web.client.widgets.forms;

import java.util.Map;

import org.txm.web.client.Commands;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.tabs.CMSTab;
import org.txm.web.shared.Keys;

import com.smartgwt.client.widgets.Canvas;

public class DocumentationForm {
	
	public static CMSTab validate(Map<String, String> parameters) {
		
		String path = parameters.get(Keys.PATH);
		String profile = parameters.get(Keys.PROFILE);
		if (profile == null) {
			profile = "";
		}
		String locale = parameters.get(Keys.LOCALE);
		if (locale == null) {
			locale = TXMWEB.getLocale();
		}
		
		if (path == null) return null;
		if (path.length() <= 1) return null;
		
		String title = path;
		if (title.startsWith("/")) title = title.substring(1);
		String tooltip = Services.getI18n().documentationPageTitle(title);
		title = Canvas.imgHTML("icons/house.png", 16, 16) + "&nbsp;"+"<span style=\"vertical-align:-2px;\">" + title + "</span>";
		
		parameters.put(Keys.PATH, parameters.get(Keys.PATH) + "/Documentation"); // chemin de la page .jsp à ouvrir
		parameters.put(Keys.TITLE, title); // titre de l'onglet de la page
		parameters.put(Keys.TOOLTIP, tooltip); // tooltip de l'onglet de la page
		parameters.put("navigation", "false");
		
		CMSTab tab = Commands.callPage(parameters);
		return tab;
	}
}
