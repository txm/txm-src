package org.txm.web.client.widgets.forms;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;

import com.google.gwt.user.client.rpc.AsyncCallback;

public class LogoutForm {
	
	public static void validate() {
		Services.getAuthenticationService().logout(new AsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				if (!result) {
					onFailure(null);
				} else {
					TXMWEB.logout();
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				//SC.say("Logout fail");
			}
		});
	}
}
