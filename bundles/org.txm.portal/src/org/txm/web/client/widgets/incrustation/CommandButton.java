package org.txm.web.client.widgets.incrustation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.txm.web.client.Commands;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.TxmHTMLPane;
import org.txm.web.client.widgets.tabs.CMSTab;
import org.txm.web.shared.Keys;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;

public class CommandButton {

	public static final String ID = "cmd_";
	public static final String INTERNAL = "internal";
	public static final String CLASS = "class";
	public static final String TYPE = "type";
	
	//	public static String commands[] = {
	//		"concordance", "context",
	//		"cooccurrence",
	//		"index", "lexicon", 
	//		"reference", "selection",
	//		"subcorpus", "partition",
	//		"texts", "tigersearch", 
	//		"description", "edition",
	//		"specificity", "afc",
	//		"record"};

	public static HashMap<String, ArrayList<String>> parameters;

	public static Canvas getButton(final String cmd, final Map<String, String> parameters) {

		if ((parameters.get(Keys.QUERY) != null && parameters.get(Keys.QUERY).length() > 0) && ("concordance".equals(cmd) || "context".equals(cmd))) {
			HLayout layout = new HLayout();

			final TextItem ti = new TextItem("query");
			ti.setTitle(Services.getI18n().queryField());
			DynamicForm f = new DynamicForm();
			f.setItems(ti);

			Button btn = new Button(parameters.get(Keys.QUERY));
			btn.setTooltip(cmd);
			btn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
				public void onClick(ClickEvent event) {
					Commands.execute(cmd, parameters);
				}
			});

			layout.addMembers(f, btn);
			return layout;
		} else {
			String label = parameters.get(Keys.LABEL);
			if (label == null) {
				label = "OK";
			}
			Button btn = new Button(label);
			btn.setTooltip(cmd);
			btn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
				public void onClick(ClickEvent event) {
					Commands.execute(cmd, parameters);
				}
			});
			return btn;
		}
	}

	public static Anchor getLink(final String cmd, final Map<String, String> parameters) {
		Anchor btn = new Anchor();
		btn.setTitle(cmd);
		btn.setHTML("<b>"+parameters.get(Keys.LABEL)+"</b>");
		btn.addClickHandler(new com.google.gwt.event.dom.client.ClickHandler() {
			@Override
			public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
				Commands.execute(cmd, parameters);
			}
		});
		return btn;
	}

	public static native JsArray<Node> getAttributes(Element elem) /*-{
	   return elem.attributes;
	}-*/;
	
	public static boolean incrust(TxmHTMLPane txmHTMLPane, Element dom) {
		//System.out.println("INCRUST BUTTONS");
		NodeList<Element> elements = dom.getElementsByTagName("a");
		//System.out.println("ELEMENTS: "+elements.getLength());
		for (int i = 0 ; i < elements.getLength() ; i++) {
			final com.google.gwt.dom.client.Element element = (com.google.gwt.dom.client.Element) elements.getItem(i);
			Anchor a = null;
			if (INTERNAL.equals(element.getAttribute(CLASS))) {
				final CMSTab tab = txmHTMLPane.getCMSTab();
				if (tab != null) {
					a = Anchor.wrap(element);
					a.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
							event.stopPropagation();
							if (element.getAttribute(Keys.TITLE) != null) {
								tab.setTitle(element.getAttribute(Keys.TITLE));
							}
							tab.update(element.getAttribute(Keys.PATH));
							
						}
					});
				}
			} else if (Keys.COMMAND.equals(element.getAttribute(CLASS))) {

				String type = element.getAttribute(TYPE);
				String title = element.getAttribute(Keys.TITLE);
				String cmd = element.getAttribute(Keys.COMMAND);
				
				//TODO: find out a better way to do this kind of hook
				if (type != null && type.length() > 0 && ("concordance".equals(cmd) || "context".equals(cmd))) {
					HorizontalPanel hpanel = new HorizontalPanel();
					hpanel.setWidth("100%");
					final TextBox tb = new TextBox();
					tb.setTitle("Saisir le mot ici");
					tb.setWidth("100%");

					Label b = new Label();
					if (TXMWEB.EXPO) {
						b.setIcon("icons/functions/Search.png");
						//b.setTitle("");
						b.setWidth(24);
						b.setHeight(24);
						b.setTooltip("Lancer la recherche ");
						b.setAlign(Alignment.CENTER);
						b.setBottom(20);
						b.setLeft(2);
						//b.setBorder("none");
						//b.setBackgroundColor("white");
						hpanel.setHeight("30px");
					} else {
						b.setTitle(Services.getI18n().searchButton());
						b.setWidth(60);
					}


					hpanel.add(tb);
					hpanel.add(b);
					element.getParentElement().replaceChild(hpanel.getElement(), element);

					b.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							callUsingValueInElement(element, tb);
						}
					});					
				} else {
					a = Anchor.wrap(element);
					JsArray<Node> attributes = getAttributes(element);
					String parameters = "";
					for (int iattribute = 0; iattribute < attributes.length(); iattribute++) {
						Node node = attributes.get(iattribute);
						if (iattribute > 0) parameters += "&";
						parameters += node.getNodeName()+"="+node.getNodeValue();
					}
					if (title != null && title.length() > 0) {
						a.setTitle(title);
					} else {
						//a.setTitle("Call '"+cmd +"' with the following parameters: "+parameters.replaceAll("&", "  "));
					}
					a.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
							String cmd = element.getAttribute(Keys.COMMAND);
							
							HashMap<String, String> parameters = new HashMap<String, String>();
							JsArray<Node> attributes = getAttributes(element);
							for (int i = 0; i < attributes.length(); i ++) {
							    final Node node = attributes.get(i);
							    String attributeName = node.getNodeName();
							    String attributeValue = node.getNodeValue();
							    parameters.put(attributeName, attributeValue);
							}
							Commands.execute(cmd, parameters);
						}
					});
					
//					SC.say("share = "+element.hasAttribute("share"));
					if (element.hasAttribute("share")) {
						
						Element shareLink = Document.get().createElement("a");
						shareLink.setInnerText(" ");
						Element imgElement = Document.get().createElement("img");
						imgElement.setAttribute("src", "images/icons/share-12x12.png");
						imgElement.setAttribute("class", "valign");
						
						shareLink.setAttribute("href", "./?"+parameters);
						shareLink.setAttribute("target", "_blank");
						shareLink.setTitle("copy link to share");	
						shareLink.insertAfter(imgElement, null);
												
						element.getParentNode().insertAfter(shareLink, element);
					}
				}
			}
			//			if (a != null) {
			//				a.getElement().getStyle().setCursor(Cursor.POINTER);
			//				a.getElement().getStyle().setColor("blue");
			//			}
		}
		//for (String cmd : commands) incrust(cmd);
		return true;
	}

	/**
	 * Call a command using the text value of 'element' dirty, must be moved
	 * @param element
	 * @param tb 
	 */
	public static void callUsingValueInElement(com.google.gwt.dom.client.Element element, TextBox tb) {

		if (tb.getText().trim().length() == 0) {
			return;
		}

		String cmd = element.getAttribute(Keys.COMMAND);
		
		HashMap<String, String> parameters = new HashMap<String, String>();
		JsArray<Node> attributes = getAttributes(element);
		for (int i = 0; i < attributes.length(); i ++) {
		    final Node node = attributes.get(i);
		    String attributeName = node.getNodeName();
		    String attributeValue = node.getNodeValue();
		    parameters.put(attributeName, attributeValue);
		}
		
		Commands.execute(cmd, parameters);
	}

	public static String fixQuery(String query) {
		if (query.contains("\"")) return query;
		if (query.contains("[") && query.contains("]") ) return query;

		query = "\""+query.replaceAll("\"", "\\\"");//.replaceAll("("+TokenizerClasses.regPunct+")", " $1 ");
		query = query.replaceAll(" +", "\"%c \"")+"\"";
		if (query.endsWith("\"")) query += "%c";
		return query;
	}

	@Deprecated
	public static boolean incrust(String cmd) {
		String id = ID+cmd;
		RootPanel target = RootPanel.get(id);
		//System.out.println(RootPanel.getBodyElement());
		//System.out.println("FOUND '"+id+"' ? "+target);
		if (target != null) {
			target.clear();
			String type = target.getElement().getAttribute("type");

			HashMap<String, String> parameters = new HashMap<String, String>();
			JsArray<Node> attributes = getAttributes(target.getElement());
			for (int i = 0; i < attributes.length(); i ++) {
			    final Node node = attributes.get(i);
			    String attributeName = node.getNodeName();
			    String attributeValue = node.getNodeValue();
			    parameters.put(attributeName, attributeValue);
			}

			//System.out.println("INCRUST TYPE: "+type);
			if (type != null && type.equals("link")) {
				target.add(getLink(cmd, parameters));
			} else {
				target.add(getButton(cmd, parameters));
			}
			return true;
		}
		return false;
	}
}

