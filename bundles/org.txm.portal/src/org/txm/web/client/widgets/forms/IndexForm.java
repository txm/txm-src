package org.txm.web.client.widgets.forms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.txm.web.client.Commands;
import org.txm.web.client.ExecTimer;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.QueryAssistantDialog;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.tabs.IndexTab;
import org.txm.web.exceptions.PermissionException;
import org.txm.web.shared.Keys;
import org.txm.web.shared.VocabularyParam;
import org.txm.web.shared.VocabularyResult;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.MultipleAppearance;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemValueFormatter;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;

public class IndexForm extends TxmForm {

	protected IndexTab parentTab = null;

	protected VocabularyParam params = new VocabularyParam();
	protected DynamicForm queryForm = new DynamicForm();

	QueryItem queryField;
	OrderedPickListItem propertyField;
	protected SpinnerItem vmaxField;
	protected SpinnerItem fmaxField;
	protected SpinnerItem fminField;

	protected IButton settingsButton;
	protected IButton validationButton;
	
	private SelectItem sortField;

	private ListGridRecord elem;

	protected IButton assistantButton;

	private String itemPath;

	public void setQuery(String query) {
		queryField.setValue(query);
	}
	
	public void setProperties(String properties) {
		form.setValue("properties", properties);
	}
	
	public void setParameters(Map<String, String> parameters) {
		//TxmDialog.severe("Indexform.setParameters: "+parameters);
		for (String property : parameters.keySet()) {
			
			if (Keys.QUERY.equals(property)) {
				setParameter(queryForm, property, parameters.get(property));
			} else {
				setParameter(form, property, parameters.get(property));
			}
		}
	}
	
	public IndexForm(Map<String, String> parameters, ListGridRecord elem, IndexTab parent) {
		super();
		
		this.parentTab = parent;
		this.elem = elem;
		this.itemPath = parameters.get(Keys.PATH);
		
		initializeFields(parameters);
	}
	
	@Override
	public void _initializeFields(Map<String, String> parameters) {
		
		ProcessingWindow.show(Services.getI18n().openingIndexForm());
		
		this.setWidth100();
		
		params.setCorpusPath(itemPath);

		this.form = new DynamicForm();
		
		HLayout section1 = new HLayout();
		final HLayout section2 = new HLayout();
		section1.setHeight(35);
		section1.setWidth100();
		section1.setAlign(Alignment.CENTER);
		section1.setAlign(VerticalAlignment.CENTER);
		section1.setMargin(3);
		section2.hide();
		section2.setBackgroundColor("#DDDDDD");

		assistantButton = new IButton("");
		assistantButton.setShowTitle(false);
		assistantButton.setWidth(24);
		assistantButton.setIcon("icons/functions/Queryassist.png");
		assistantButton.setPrompt(Services.getI18n().openTheQueryAssistant());
		assistantButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				QueryAssistantDialog assistantDialog = new QueryAssistantDialog(params.getCorpusPath());
				assistantDialog.show(queryForm, Keys.QUERY);
			}
		});
		
		queryField = new QueryItem(Keys.QUERY, Services.getI18n().queryField()); // requête CQL de sélection des occurrences
		queryField.setPrompt(Services.getI18n().queryPrompt());
		
		validationButton = new IButton(Services.getI18n().searchButton());
		validationButton.setPrompt(Services.getI18n().startIndex());
		
		settingsButton = new IButton(Services.getI18n().settingsButton());
		settingsButton.setPrompt(Services.getI18n().openSettings());

		propertyField = new OrderedPickListItem(Keys.PROPERTIES); // Propriétés de mots affichées. Valeur par défaut : Keys.WORD
		propertyField.setPrompt(Services.getI18n().propertyPrompt());
		propertyField.setTitle(Services.getI18n().indexFormProperties());
		propertyField.setShowTitle(true);

		Services.getCorpusActionService().getWordProperties(itemPath, new AsyncCallback<List<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.severe(Services.getI18n().failed_get_structuralunits(itemPath, caught.getMessage()));
			}
			@Override
			public void onSuccess(List<String> result) {
				Collections.sort(result);
				propertyField.setValueMap(result.toArray(new String[result.size()]));
				propertyField.setDefaultValue(Keys.WORD);
			}
		});
		//propertyField.setValueMap(elem.getAttributeAsStringArray("wordProperties"));
		propertyField.setDefaultValue(Keys.WORD);
		//propertyField.setWidth(80);
		
		sortField = new SelectItem(Keys.SORT); // colonne de tri à choisir entre Keys.FREQUENCE et Keys.ALPHANUMERIQUE. Valeur par défaut : Keys.FREQUENCE
		sortField.setPrompt(Services.getI18n().sortPrompt());
		sortField.setTitle(Services.getI18n().sort());
		sortField.setMultiple(false);
		sortField.setMultipleAppearance(MultipleAppearance.PICKLIST);
		sortField.setShowAllOptions(true);

		LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();
		values.put(Keys.FREQUENCE, "Freq");
		values.put(Keys.ALPHANUMERIC, "Alpha");
		sortField.setValueMap(values);	
		sortField.setDefaultValue(Keys.FREQUENCE);
		//sortField.setWidth(80);
		
		fminField = new SpinnerItem(Keys.FMIN); // nombre minimum d'occurrence pour faire partie du calcul
		fminField.setPrompt(Services.getI18n().fminPrompt());
		fminField.setTitle(Services.getI18n().Fmin());  
		fminField.setDefaultValue(1);  
		fminField.setMin(1);  
		fminField.setMax(1000000000);  
		fminField.setStep(1);
		fminField.setLength(10);
		//fminField.setWidth(100);
		fminField.setWriteStackedIcons(false);

		fmaxField = new SpinnerItem(Keys.FMAX); // nombre maximum d'occurrence pour faire partie du calcul
		fmaxField.setPrompt(Services.getI18n().fmaxPrompt());
		fmaxField.setTitle(Services.getI18n().Fmax());  
		fmaxField.setDefaultValue(1000000000);  
		fmaxField.setMin(2);  
		fmaxField.setMax(1000000000);  
		fmaxField.setStep(1);
		fmaxField.setLength(10);
		//fmaxField.setWidth(100);
		fmaxField.setWriteStackedIcons(false);
		
		vmaxField = new SpinnerItem(Keys.VMAX); // nombre maximum de lignes à afficher
		vmaxField.setPrompt(Services.getI18n().vmaxPrompt());
		vmaxField.setTitle(Services.getI18n().VMax());  
		vmaxField.setDefaultValue(1000000000);  
		vmaxField.setMin(1);  
		vmaxField.setMax(1000000000);  
		vmaxField.setStep(1);
		vmaxField.setLength(10);
		vmaxField.setWriteStackedIcons(false);
		
		final NumberFormat decimalformater = NumberFormat.getDecimalFormat();
		FormItemValueFormatter formatter = new FormItemValueFormatter() {
			@Override
			public String formatValue(Object value, Record record, DynamicForm form, FormItem item) {
				Integer v = (Integer) value;
				return decimalformater.format(v);
			}
		};
		
		fmaxField.setEditorValueFormatter(formatter);
		fminField.setEditorValueFormatter(formatter);
		vmaxField.setEditorValueFormatter(formatter);

		validationButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				validate();
			}
		});

		queryField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event != null && event.getKeyName() != null) {
					if (event.getKeyName().equals("Enter")) {
						validate();
					}
				}
			}
		});

		settingsButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (section2.isVisible()) {
					section2.hide();
				} else {
					section2.show();
				}
			}
		});

		queryField.setWidth("100%");
		queryForm.setWidth100();
		queryForm.setItems(queryField);
		//form.setWidth100();
		
		form.setItems(propertyField, sortField, fminField, fmaxField, vmaxField);
		form.setTitleOrientation(TitleOrientation.TOP); 
		form.setNumCols(6);
		
		section1.setMembers(assistantButton, queryForm, validationButton, settingsButton);
		section2.setMembers(form);
		this.setMembers(section1, section2);
		
		ProcessingWindow.hide();
	}

	public synchronized void validate() {
		
		if (queryForm.getValueAsString(Keys.QUERY) == null || queryForm.getValueAsString(Keys.QUERY).length() == 0) {
			//TxmDialog.severe("No query set");
			return;
		}
		
		ProcessingWindow.show(Services.getI18n().computing_index(params.getQuery()));

		params.setQuery(queryForm.getValueAsString(Keys.QUERY));
		params.setSort(form.getItem(Keys.SORT).getValue().toString());
		try {
			params.filterFmin = Integer.parseInt(form.getValueAsString(Keys.FMIN));
			params.filterFmax = Integer.parseInt(form.getValueAsString(Keys.FMAX));
			params.filterVmax = Integer.parseInt(form.getValueAsString(Keys.VMAX));
			params.filterNbPerPage = parentTab.getNbResultPerPage();// Integer.parseInt(form.getValueAsString("NbPerPage"));
		} catch(Exception e) {
			e.printStackTrace();
			ProcessingWindow.hide();
			TxmDialog.warning(Services.getI18n().indexErrorInteger());
			return;
		}
		params.startindex = parentTab.startindex;

		params.setProperties(new ArrayList<String>());
		String properties = form.getItem(Keys.PROPERTIES).getValue().toString();
		//System.out.println("'"+properties+"'");
		if (properties != null) {
			//properties = properties.substring(1, properties.length() - 1); // remove [ and ]
			for (String s : properties.split(", ")) {
				params.getProperties().add(s);
			}
		} else {
			params.getProperties().add(Keys.WORD);
		}

		if (params.getQuery() == null || params.getQuery().trim().length() == 0) {
			TxmDialog.warning(Services.getI18n().indexErrorNoQuery());
			ProcessingWindow.hide();
			return;
		}

		if (params.getQuery().matches(".*%(d|cd|dc).*")) {
			TxmDialog.warning(Services.getI18n().queryDError());
			ProcessingWindow.hide();
			return;
		}
		//TxmDialog.severe("RPC Params: "+params);
		ExecTimer.start();
		Services.getVocabularyService().index(params, new AsyncCallback<VocabularyResult>() {
			@Override
			public void onSuccess(VocabularyResult result) {
				ProcessingWindow.hide();
				if (result == null || result.error == null || result.error.length() > 0) {
					String error = result.error;
					if (error.equals("permission")) {
						TxmDialog.warning(Services.getI18n().noPermission());
					} else if (error.equals("session")) {
						TxmDialog.warning(Services.getI18n().sessionExpired());
					} else {
						TxmDialog.warning(Services.getI18n().querySyntaxError(error));
					}
				} else {
					parentTab.fillList(result);
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				if (caught instanceof PermissionException) {
					TxmDialog.warning(Services.getI18n().indexPermissionException());
				} else {
					TxmDialog.warning(Services.getI18n().indexError());
				}
			}
		});

	}	

	public VocabularyParam getParams() {
		return params;
	}

	public void setParams(VocabularyParam params) {
		this.params = params;
	}

	private static String chars = "^$<>|\\\"{()}?+*."; //$NON-NLS-1$
	public static String addBackSlash(String squery) {
		StringBuffer buff = new StringBuffer();
		//for(char c : chars.)
		for (int i = 0 ; i < squery.length() ; i++) {
			int j = chars.indexOf(squery.charAt(i));
			//System.out.println(""+i+" : "+squery.charAt(i)+ " >> "+j);
			
			if (j >= 0) {
				buff.append("\\"+squery.charAt(i)); //$NON-NLS-1$
			} else {
				buff.append(squery.charAt(i));
			}
		}
		return buff.toString();
	}
	
	@SuppressWarnings("unchecked")
	public void sendToConcordances(String itemPath, String value) {
		
		if ("partition".equals(elem.getAttributeAsString("type"))) {
			itemPath = itemPath.substring(0, itemPath.lastIndexOf("/"));
		}
			
		Object propvalue = form.getItem(Keys.PROPERTIES).getValue();
		//System.out.println("LINK TO CONC - properties : "+propvalue);
		ArrayList<String> props = new ArrayList<String>();
		if (propvalue != null) {
			if (propvalue instanceof ArrayList) {
				props = (ArrayList<String>)propvalue;
			} else {
				if (propvalue.toString().contains(",")) {
					String[] split = propvalue.toString().split(",");
					for (String v : split) {
						props.add(v.trim());
					}
				} else {
					props.add(propvalue.toString());
				}
			}
		} else {
			props = new ArrayList<String>();
			props.add(Keys.WORD);
		}
		//System.out.println("PROPS: "+props);
		//String props = form.getItem(Keys.PROPERTIES).getValue().toString();
		String query = addBackSlash(value);
		String[] tokens = value.split(" ");
		if (tokens.length > 1 || (props.size() >= 1 && !props.equals(Keys.WORD))) {
			//String[] propsSplit = props.split(",");
			query = "";
			for (String token : tokens) {
				String[] valueSplit = token.split("_");
				query += "[";
				for (int i = 0 ; i < props.size() ; i++) {
					query += props.get(i)+"=\""+addBackSlash(valueSplit[i])+"\" &";
				}
				if (valueSplit.length > 0) {
					query = query.substring(0, query.length()-1)+"]";
				} else {
					query += "]";
				}
			}
		}
		
		HashMap<String, String> p = Commands.createParameters(itemPath);
		p.put(Keys.QUERY, query);
		Commands.callConcordances(p);
	}

	public Map getValues() {
		return form.getValues();
	}
}