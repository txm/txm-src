package org.txm.web.client.widgets.forms;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class NavigationToolStrip {
	
	Label infoLabel = new Label("");
	
	ToolStripButton firstPage = new ToolStripButton();
	ToolStripButton previousPage = new ToolStripButton();
	Label navigationLabel = new Label("");
	ToolStripButton nextPage = new ToolStripButton();
	ToolStripButton lastPage = new ToolStripButton();

	Label paginationLabel = new Label(Services.getI18n().pagination());
	SelectItem resultPerPage = new SelectItem();
	
	//Label exportLabel = new Label(Services.getI18n().concExportButton());
	ToolStripButton exportResult = new ToolStripButton(Services.getI18n().concExportButton());
	
	public NavigationToolStrip()
	{
		infoLabel.setWidth(200);
		infoLabel.setCanSelectText(true);
		
		navigationLabel.setAlign(Alignment.CENTER);
		navigationLabel.setAutoWidth();
		navigationLabel.setCanSelectText(true);
		
		paginationLabel.setWidth(100);
		
		resultPerPage.setShowTitle(false);
		resultPerPage.setValueMap("10", "50", "100", "200", "500", "1000", "2000");  
		resultPerPage.setDefaultValue("100");
		resultPerPage.setTextAlign(Alignment.RIGHT);
		resultPerPage.setWidth(100);
		resultPerPage.setHeight(16);
		
		previousPage.setIcon("icons/control_reverse.png");
		nextPage.setIcon("icons/control_play.png");
		firstPage.setIcon("icons/control_start.png");
		lastPage.setIcon("icons/control_end.png");
		exportResult.setIcon("icons/functions/Export.png");
		
		previousPage.setTooltip(Services.getI18n().previousPage());
		nextPage.setTooltip(Services.getI18n().nextPage());
		firstPage.setTooltip(Services.getI18n().firstPage());
		lastPage.setTooltip(Services.getI18n().lastPage());
		exportResult.setTooltip(Services.getI18n().concExportButton());
		
		if (TXMWEB.EXPO) exportResult.hide();
	}
	
	public void updateLabel(String content)
	{
		navigationLabel.setContents(content);
	}
	
	public void addToToolstrip(ToolStrip bar)
	{
		bar.addSpacer(10);
		bar.addMember(infoLabel);
		bar.addFill();
		bar.addMember(firstPage);
		bar.addMember(previousPage);
		bar.addMember(navigationLabel);
		bar.addMember(nextPage);
		bar.addMember(lastPage);
		bar.addMember(paginationLabel);
		bar.addFormItem(resultPerPage);
		bar.addFill();		
		bar.addMember(exportResult);
		bar.addSpacer(10);
	}
	
	public void addPaginationChangedHandler(ChangedHandler handler)
	{
		resultPerPage.addChangedHandler(handler);
	}
	
	public void addInfosClickHandler(ClickHandler handler)
	{
		infoLabel.addClickHandler(handler);
	}
	
	public void addExportClickHandler(ClickHandler handler)
	{
		exportResult.addClickHandler(handler);
	}
	
	public void addPreviousClickHandler(ClickHandler handler)
	{
		previousPage.addClickHandler(handler);
	}

	public void addFirstClickHandler(ClickHandler handler)
	{
		firstPage.addClickHandler(handler);
	}

	public void addLastClickHandler(ClickHandler handler)
	{
		lastPage.addClickHandler(handler);
	}

	public void addNextClickHandler(ClickHandler handler)
	{
		nextPage.addClickHandler(handler);
	}
	
	public int getPagination()
	{
		return Integer.parseInt(resultPerPage.getValueAsString());
	}
	
	public void setPagination(int pagination)
	{
		if(pagination > 0)
			resultPerPage.setDefaultValue(pagination);
	}
	
	public void setInfos(String infos)
	{
		if(infos != null)
			this.infoLabel.setContents(infos);
	}

	public Label getPaginationLabel() {
		return navigationLabel;
	}

	public ToolStripButton getPreviousPage() {
		return previousPage;
	}

	public ToolStripButton getNextPage() {
		return nextPage;
	}

	public ToolStripButton getFirstPage() {
		return firstPage;
	}

	public ToolStripButton getLastPage() {
		return lastPage;
	}

	public ToolStripButton getExportResult() {
		return exportResult;
	}

	public Label getInfoLabel() {
		return infoLabel;
	}

	public SelectItem getResultPerPage() {
		return resultPerPage;
	}

	public void updateInfos(String message) {
		infoLabel.setContents(message);
	}
}
