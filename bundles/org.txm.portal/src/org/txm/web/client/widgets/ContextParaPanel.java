package org.txm.web.client.widgets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.dialogs.TxmDialog;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.Side;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
/**
 * ContextPara panel 
 * @author sgedzelm
 *
 */
public class ContextParaPanel extends VLayout {

	private String ID = this.getID();
	private String itemPath;
	SectionStack sectionStack;
	SectionStackSection querySection;
	SectionStackSection paramsSection;  

	String BLOCK = "block";
	String QUERY = "query";
	String LANG = "lang";

	private HLayout paramLayout;

	LinkedList<ParametersPanel> layoutByBlock = new LinkedList<ParametersPanel>();
	LinkedList<TextItem> langItems = new LinkedList<TextItem>();
	ListGridRecord elem;
	int nbLangs=1;
	String [] langs;
	String [] blocks = {"B1", "B2"};

	ListGridField blockField = new ListGridField(BLOCK, "Facettes");  
	ListGridField langField = new ListGridField(LANG, "Languages");  
	ListGridField queryLangField = new ListGridField(QUERY, "Query");  
	ListGridField queryBlockField = new ListGridField(QUERY, "Query");  

	private Tab parentTab;


	//QUERY
	HashMap<String, String> queries; //by Lang
	String struct;
	ArrayList<String> corpora;
	ArrayList<String> props; 
	ArrayList<String> refs; 
	ArrayList<Integer> CGs;
	ArrayList<Integer> CDs;
	ArrayList<Boolean> participates;

	private Tab tabLang = new Tab("Languages");  
	private ListGrid langGrid ;
	private ListGrid blockGrid ;
	private Tab tabBlock = new Tab("Facettes");  

	public ContextParaPanel(ListGridRecord elem, final String itemPath) {
		this.setSize("100%", "100%");
		this.itemPath = itemPath;
		System.out.println("ContextParaPanel - path ["+itemPath+"]");
		this.elem = elem;
		Services.getEditionService().getParallelLanguages(itemPath, new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.severe("Error: getParallelLanguages() failed: "+caught.toString());
			}
			@Override
			public void onSuccess(ArrayList<String> results) {
				System.out.println("Languages ? "+results);
				if(results!=null ){
					langs = new String[results.size()];
					int i = 0;
					for(String result : results){
						langs[i] = result;
						++i;
					}
				}			
				init();
			}
		});
	}

	private void init(){
		sectionStack = new SectionStack();  
		sectionStack.setVisibilityMode(VisibilityMode.MULTIPLE);  
		sectionStack.setWidth100();   
		sectionStack.setHeight100();

		initQuerySection();
		initParamsSection();
		sectionStack.setSections(querySection, paramsSection);
		this.setMembers(sectionStack);
	}

	private void initQuerySection(){    
		ToolStripButton navGo = new ToolStripButton("Go");
		navGo.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				queries = new HashMap<String, String>();
				corpora = new ArrayList<String>();
				props = new ArrayList<String>();
				refs = new ArrayList<String>();
				CGs = new ArrayList<Integer>();
				CDs = new ArrayList<Integer>();
				participates = new ArrayList<Boolean>();
				int num = 0;
				for (ParametersPanel pane : layoutByBlock) {
					corpora.add("/"+pane.getCorpus());
					refs.add(pane.getRef().replace(":", "_"));
					CGs.add(new Integer(pane.getLeftContext()));
					CDs.add(new Integer(pane.getRightContext()));
					props.add(pane.getPropsView());
					//props.add(pane.getPropsWord());
					participates.add(pane.getParticipate());
					struct = pane.getAlignStruct();
					++num;
				}
				ListGridRecord[] records = langGrid.getRecords();
				for(int i = 0 ; i < records.length ; ++i){
					ListGridRecord record = records[i];
					TextItem item = langItems.get(i);
					String lang = record.getAttribute(LANG);
					String queryLang = record.getAttribute(QUERY);
					if (queryLang == null || queryLang.equals("")){
						//"fro":'"..."'
						queryLang = "\"...*\"";
						queries.put(lang, queryLang);
						System.out.println("Lang : "+lang+" | Query : "+queryLang+" et ITEM ? "+item);
					}

					/*if(item!=null){
						String query = item.getValueAsString();
						if(query!=null){
							if(!query.equals("")){
								queries.put(lang, query);
							}else {
								System.out.println("QUERY WITHOUT TEXT !");
							}
						}else {
							System.out.println("QUERY NULL ! "+item.getValue());

						}
					}else {
						System.out.println("ITEM NULL !");
					}*/
				}


				Services.getEditionService().getParallelContexts(queries, struct, corpora, props, refs, CGs, CDs, participates, new AsyncCallback<HashMap<String, ArrayList<String>>>() {

					public void onFailure(Throwable caught) {
						TxmDialog.severe("Error: getParallelContexts failed: "+caught.toString());
					}

					public void onSuccess(HashMap<String, ArrayList<String>> results) {
						//int i = 0;
						System.out.println("Contextparapane - "+results.size()+" results ");
						if (results != null && results.size()>0){
							for (int iblock = 0 ; iblock < layoutByBlock.size() ; iblock++)
							{
								ParametersPanel pane = layoutByBlock.get(iblock);
								ArrayList<String> resultLines = new ArrayList<String>();
								for (String c : results.keySet()) { // seg
									ArrayList<String> rez = results.get(c);
									resultLines.add(rez.get(iblock));
								}
								pane.initGridData(resultLines);
							}	

						} else {
							SC.say("Pas de résultats à la recherche"); 
						}
					}

				});
			}
		});

		TabSet tabSet = new TabSet();  
		tabSet.setHeight(140);
		tabSet.setTabBarPosition(Side.TOP);  
		initTabLang();
		initTabBlock();
		tabSet.addTab(tabLang);  
		tabSet.addTab(tabBlock);  

		querySection = new SectionStackSection();
		querySection.setTitle("Requêtes");
		querySection.setExpanded(true);
		querySection.setControls(navGo);  
		querySection.addItem(tabSet);
	}

	private void initTabLang(){
		langGrid = initGrid(langField, queryLangField);
		langGrid.setData(setLanguageRecords(langs));
		tabLang.setPane(langGrid);
	}

	private void initTabBlock(){
		HLayout layout = new HLayout();
		layout.setHeight(150);
		blockGrid = initGrid(blockField, queryBlockField);
		blockGrid.setData(setBlockRecords(blocks));
		layout.addMember(blockGrid);//blocks Grid
		tabBlock.setPane(layout);
	}

	private void initParamsSection(){
		ToolStripButton addButt = new ToolStripButton("+");
		addButt.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ParametersPanel paramPane = new ParametersPanel(itemPath);
				layoutByBlock.add(paramPane);					
				paramLayout.addMember(paramPane);
			}
		});
		ToolStripButton removeButt = new ToolStripButton("-");
		removeButt.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//open pop-up to manage params blocks and eventually delete/move one or several
			}
		});

		paramLayout = new HLayout();
		ParametersPanel paramPane = new ParametersPanel(itemPath);
		layoutByBlock.add(paramPane);					
		paramLayout.addMember(paramPane);
		paramsSection = new SectionStackSection();
		//paramsSection.setShowHeader(false);
		paramsSection.setExpanded(true);
		paramsSection.setControls(addButt, removeButt);
		paramsSection.addItem(paramLayout);
	}


	private ListGrid initGrid(ListGridField f1, ListGridField f2){
		final ListGrid grid = new ListGrid() {  
			@Override  
			protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {  
				String fieldName = this.getFieldName(colNum);  
				if (fieldName != null && fieldName.equals(QUERY)) { 
					HLayout textLayout = new HLayout(1);  
					textLayout.setHeight(18);  
					textLayout.setWidth(65);  
					DynamicForm textForm = new DynamicForm();
					TextItem textItem = new TextItem();
					textItem.setShowTitle(false);
					/*textItem.addChangeHandler(new ChangeHandler() {
						@Override
						public void onChange(ChangeEvent event) {
							System.out.println("Text Item : "+record.getAttribute(LANG));
						}
					});*/
					langItems.add(textItem);
					System.out.println("Text Item initialized ! @ "+langItems.size());
					textForm.setFields(textItem);
					textLayout.addMember(textForm);
					return textLayout;
				} else {  
					return null;  
				}  
			}  
		};  
		grid.setShowRecordComponents(true);          
		grid.setShowRecordComponentsByCell(true);  
		grid.setWidth(300);  
		//grid.setHeight(100);  
		grid.setShowAllRecords(true);  
		grid.setFields(f1, f2);  
		return grid;
	}

	private RecordList setLanguageRecords(String [] langs){
		RecordList recordList =new RecordList();
		if (langs.length>0) {
			for (String lang : langs) {
				//lang = lang.trim();
				ListGridRecord gridRecord = new ListGridRecord();
				gridRecord.setAttribute(LANG, lang);		
				gridRecord.setAttribute(QUERY, "");		
				recordList.add(gridRecord);
			}
		}
		return recordList;
	}

	private RecordList setBlockRecords(String [] blocks){
		RecordList recordList =new RecordList();
		for (String block : blocks) {
			block = block.trim();
			ListGridRecord gridRecord = new ListGridRecord();			
			gridRecord.setAttribute(BLOCK, block);	
			gridRecord.setAttribute(QUERY, "");		
			recordList.add(gridRecord);
		}
		return recordList;
	}

	public void setTab(Tab editionTab) {
		this.parentTab = editionTab;
	}
}
