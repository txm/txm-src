package org.txm.web.client.widgets.forms;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.tabs.AFCTab;
import org.txm.web.shared.AFCParams;
import org.txm.web.shared.Keys;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.layout.HLayout;

public class AFCForm extends TxmWindow {

	String itemPath;

	public AFCForm(Map<String, String> parameters) {
		super(true, "");

		this.itemPath = parameters.get(Keys.PATH);

		initializeFields(parameters);
		//setParameters(parameters); // do it here because no Tab will do it
	}

	@Override
	public void _initializeFields(Map<String, String> parameterss) {

		setWidth(360);
		setHeight(200);

		setTitle(Services.getI18n().afcParameters());
		setShowMinimizeButton(false);
		setIsModal(true);
		setShowModalMask(true);
		centerInPage();

		//        addCloseClickHandler(new CloseClickHandler() {
		//
		//            public void onCloseClick(CloseClientEvent event) {
		//                destroy();
		//            }
		//        });

		form = new DynamicForm();
		form.setWidth("100%");
		form.setHeight("100%");
		form.setColWidths("20%", "80%");
		//form.setWidth100();
		//form.setPadding(5);
		//form.setLayoutAlign(VerticalAlignment.BOTTOM);

		final SelectItem comboPropertyItem = new SelectPropertyItem(Keys.PROPERTY); // propriété de mot à utiliser pour construire la table lexicale. Valeur par défaut : Keys.WORD
		comboPropertyItem.setTitle(Services.getI18n().property());
		// ask possible properties and fill gui component with result

		comboPropertyItem.setDefaultValue(Keys.WORD);


		SpinnerItem fMinSpinner = new SpinnerItem(Keys.FMIN); // fréquence minimale d'un mot
		fMinSpinner.setTitle(Services.getI18n().Fmin());
		fMinSpinner.setValue(2);
		fMinSpinner.setWriteStackedIcons(false);

		SpinnerItem vMaxSpinner = new SpinnerItem(Keys.VMAX); // taille maximale de la table lexicale
		vMaxSpinner.setTitle(Services.getI18n().VMax());
		vMaxSpinner.setValue(200);
		vMaxSpinner.setWriteStackedIcons(false);

		IButton okButton = new IButton("ok");
		okButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				AFCParams params = new AFCParams();
				params.setPath(itemPath);
				params.setProperty(form.getValueAsString(Keys.PROPERTY));
				params.setFmin(Integer.parseInt(form.getValueAsString(Keys.FMIN)));
				params.setVmax(Integer.parseInt(form.getValueAsString(Keys.VMAX)));
				params.setShowCol(true);
				params.setShowLine(false);
				params.setFirstDim(1);
				params.setSecondDim(2);

				new AFCTab(itemPath, params).addInView();
				destroy();
			}
		});

		IButton cancelButton = new IButton(Services.getI18n().cancelButton());
		cancelButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				destroy();
			}
		});

		HLayout buttons = new HLayout(5);
		buttons.setMargin(7);
		buttons.setWidth100();
		buttons.setMembers(okButton, cancelButton);

		form.setItems(comboPropertyItem, fMinSpinner, vMaxSpinner);

		//        setMembers(form, okButton);
		this.addItem(form);
		this.addItem(buttons);

		Services.getCorpusActionService().getWordProperties(itemPath,
				new AsyncCallback<List<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				SC.warn(Services.getI18n().failed_get_wordproperties(itemPath, caught.getMessage()));
			}

			@Override
			public void onSuccess(List<String> result) {
				Collections.sort(result);
				comboPropertyItem.setValueMap(result.toArray(new String[result.size()]));
			}
		});
	}
}
