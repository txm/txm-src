package org.txm.web.client.widgets.dialogs;

import org.txm.web.client.services.Services;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
/**
 * Don't works with smartGWT...
 * @author mdecorde
 *
 */
public class GWTMessageDialog extends DialogBox implements ClickHandler{
	public GWTMessageDialog(String title, String message, String iconfilename) {
		setText(title);

		Button closeButton = new Button(Services.getI18n().closeButton(), this);
		HTML msg = new HTML("<center>"+message+"</center>",true);

		DockPanel dock = new DockPanel();
		dock.setSpacing(4);

		dock.add(closeButton, DockPanel.SOUTH);
		dock.add(msg, DockPanel.NORTH);
		dock.add(new Image("images/icons/"+iconfilename), DockPanel.CENTER);

		dock.setCellHorizontalAlignment(closeButton, DockPanel.ALIGN_RIGHT);
		dock.setWidth("100%");
		setWidget(dock);
	}

	@Override
	public void onClick(ClickEvent event) {
		hide();
	}
}
