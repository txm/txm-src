package org.txm.web.client.widgets.forms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import org.txm.web.client.ExecTimer;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.QueryAssistantDialog;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.tabs.ConcordancePresentation;
import org.txm.web.shared.ConcordanceParam;
import org.txm.web.shared.ConcordanceResult;
import org.txm.web.shared.Keys;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.SelectionType;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class ConcordanceForm extends TxmForm {

	private ConcordancePresentation parentTab = null;
	private DynamicForm queryForm = new DynamicForm();
	private ConcordanceParam params = new ConcordanceParam();
	private ComplexSortForm sortForm = null;



	private ListGridRecord elem;
	private String itemPath;

	private QueryItem queryField;
	private OrderedPickListItem referenceViewField;
	private OrderedPickListItem referenceSortField;
	private OrderedPickListItem leftViewPropertyField;
	private OrderedPickListItem keywordViewPropertyField;
	private OrderedPickListItem rightViewPropertyField;
	private OrderedPickListItem leftSortPropertyField;
	private OrderedPickListItem keywordSortPropertyField;
	private OrderedPickListItem rightSortPropertyField;
	private TextItem rightContextField;
	private TextItem leftContextField;

	public ConcordanceForm(ListGridRecord elem, final Map<String, String> parameters, ConcordancePresentation parent) {
		super();

		params.setCorpusPath(parameters.get(Keys.PATH));
		this.parentTab = parent;
		this.elem = elem;
		this.itemPath = parameters.get(Keys.PATH);
		this.setWidth100();

		this.initializeFields(parameters);
	}

	@Override
	public void _initializeFields(final Map<String, String> parameters) {
		ProcessingWindow.show(Services.getI18n().openingConcordanceForm());

		this.form = new DynamicForm();
		this.sortForm = new ComplexSortForm(parentTab, params);

		HLayout section1 = new HLayout();
		final VLayout section2 = new VLayout();
		section1.setHeight(35);
		section1.setWidth100();
		section1.setAlign(Alignment.CENTER);
		section1.setAlign(VerticalAlignment.CENTER);
		section1.setMargin(3);
		section2.hide();
		section2.setBackgroundColor("#DDDDDD");

		IButton assistantButton = new IButton("");
		assistantButton.setShowTitle(false);
		assistantButton.setWidth(24);
		assistantButton.setIcon("icons/functions/Queryassist.png");
		assistantButton.setPrompt(Services.getI18n().openTheQueryAssistant());
		assistantButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				QueryAssistantDialog assistantDialog = new QueryAssistantDialog(params.getCorpusPath());
				assistantDialog.show(queryForm, Keys.QUERY);
			}
		});

		queryField = new QueryItem(Keys.QUERY, Services.getI18n().queryField()); // requête CQL de sélection du pivot
		queryField.setPrompt(Services.getI18n().queryPrompt());

		//view fields
		referenceViewField = new OrderedPickListItem(Keys.REFERENCES); // propriétés de mots et de structures à afficher dans la colonne référence
		referenceViewField.setShowTitle(false);
		referenceViewField.setPrompt(Services.getI18n().referenceViewPrompt());

		leftViewPropertyField = new OrderedPickListItem(Keys.LEFT_VIEW_PROPERTIES);  // propriétés de mots à afficher dans le contexte gauche. Valeur par défaut : Keys.WORD
		leftViewPropertyField.setShowTitle(false);
		leftViewPropertyField.setPrompt(Services.getI18n().leftSortPropertyPrompt());

		keywordViewPropertyField = new OrderedPickListItem(Keys.KEYWORD_VIEW_PROPERTIES);  // propriétés de mots à afficher dans la colonne pivot. Valeur par défaut : Keys.WORD
		keywordViewPropertyField.setShowTitle(false);
		keywordViewPropertyField.setPrompt(Services.getI18n().keywordViewPropertyPrompt());

		rightViewPropertyField = new OrderedPickListItem(Keys.RIGHT_VIEW_PROPERTIES);  //  propriétés de mots à afficher dans le contexte droit. Valeur par défaut : Keys.WORD
		rightViewPropertyField.setShowTitle(false);
		rightViewPropertyField.setPrompt(Services.getI18n().rightViewPropertyPrompt());


		// sort fields
		referenceSortField = new OrderedPickListItem(Keys.REFERENCE_SORT_PROPERTIES); // propriétés à utiliser pour trier par référence
		referenceSortField.setShowTitle(false);
		referenceSortField.setPrompt(Services.getI18n().referenceSortPrompt());

		leftSortPropertyField = new OrderedPickListItem(Keys.LEFT_SORT_PROPERTIES); // propriétés de mots à utiliser pour le tri par contexte gauche. Valeur par défaut : Keys.WORD
		leftSortPropertyField.setShowTitle(false);
		leftSortPropertyField.setPrompt(Services.getI18n().leftSortPropertyPrompt());

		keywordSortPropertyField = new OrderedPickListItem(Keys.KEYWORD_SORT_PROPERTIES);  // propriétés de mots à utiliser pour le tri par pivot. Valeur par défaut : Keys.WORD
		keywordSortPropertyField.setShowTitle(false);
		keywordSortPropertyField.setPrompt(Services.getI18n().keywordSortPropertyPrompt());

		rightSortPropertyField = new OrderedPickListItem(Keys.RIGHT_SORT_PROPERTIES); // propriétés de mots à utiliser pour le tri par contexte droit. Valeur par défaut : Keys.WORD
		rightSortPropertyField.setShowTitle(false);
		rightSortPropertyField.setPrompt(Services.getI18n().rightSortPropertyPrompt());

		leftContextField = new TextItem(Keys.LEFT_CONTEXT, Services.getI18n().leftContext()); // nombre de mots du contexte gauche
		leftContextField.setShowTitle(false);
		leftContextField.setPrompt(Services.getI18n().leftContextPrompt());

		rightContextField = new TextItem(Keys.RIGHT_CONTEXT, Services.getI18n().rightContext()); // nombre de mots du contexte gauche
		rightContextField.setShowTitle(false);
		rightContextField.setPrompt(Services.getI18n().rightContextPrompt());

		//referenceField.setValueMap(elem.getAttributeAsStringArray("structuralUnits"));

		IButton validationButton = new IButton(Services.getI18n().searchButton());
		validationButton.setPrompt(Services.getI18n().startConcordances());
		validationButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				validate();
			}
		});

		final IButton settingsButton = new IButton("∇ "+Services.getI18n().settingsButton());
		settingsButton.setPrompt(Services.getI18n().openSettings());
		settingsButton.setActionType(SelectionType.CHECKBOX); 
		settingsButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (section2.isVisible()) {
					section2.hide();
					settingsButton.setTitle("∇ "+Services.getI18n().settingsButton());
				} else {
					section2.show();
					settingsButton.setTitle("∆ "+Services.getI18n().settingsButton());
				}
			}
		});

		queryField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event != null && event.getKeyName() != null) {
					if (event.getKeyName().equals("Enter")) {
						validate();
					}
				}
			}
		});

		Services.getCorpusActionService().getConcordanceDefaultParameters(itemPath, new AsyncCallback<ConcordanceParam>() {

			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				TxmDialog.severe(Services.getI18n().failed_get_default_parameters(itemPath, caught.getMessage()));
			}

			@Override
			public void onSuccess(ConcordanceParam result) {
				if (result != null) {
					System.out.println("Loading concordance corpus default parameters: "+result);
					rightContextField.setValue(result.getRightContextSize());
					leftContextField.setValue(result.getLeftContextSize());

					String[] list = result.getViewReferences().toArray(new String[]{});
					referenceViewField.setValueMap(list);
					referenceSortField.setValueMap(list);
					String defaultRef = "text:id";
					for (String v : list) {
						if (v.equals("ref")) {
							defaultRef = "ref";
							break;
						}
					}
					referenceViewField.setDefaultValue(defaultRef);
					referenceSortField.setDefaultValue(defaultRef);

					list = result.getKeywordSortProperties().toArray(new String[]{});
					leftSortPropertyField.setValueMap(list);
					keywordSortPropertyField.setValueMap(list);	
					rightSortPropertyField.setValueMap(list);	
					if (list.length > 0) {
						leftSortPropertyField.setDefaultValue(list[0]);
						keywordSortPropertyField.setDefaultValue(list[0]);
						rightSortPropertyField.setDefaultValue(list[0]);
					}

					list = result.getKeywordViewProperties().toArray(new String[]{});
					leftViewPropertyField.setValueMap(list);
					keywordViewPropertyField.setValueMap(list);	
					rightViewPropertyField.setValueMap(list);	
					if (list.length > 0) {
						leftViewPropertyField.setDefaultValue(list[0]);
						keywordViewPropertyField.setDefaultValue(list[0]);
						rightViewPropertyField.setDefaultValue(list[0]);
					}

					setParameters(parameters);

					if (queryForm.getValueAsString(Keys.QUERY) != null && queryForm.getValueAsString(Keys.QUERY).length() > 0) validate();
				} else {
					TxmDialog.warning("Could not initialize parameters.");
				}
			}
		});



		queryField.setWidth("100%");

		StaticTextItem columnLabel = new StaticTextItem("lcol");
		columnLabel.setDefaultValue("");
		columnLabel.setShowTitle(false);
		StaticTextItem displayLabel = new StaticTextItem("ldis");
		displayLabel.setDefaultValue(Services.getI18n().display());
		displayLabel.setShowTitle(false);
		StaticTextItem sortLabel = new StaticTextItem("lsort");
		sortLabel.setDefaultValue(Services.getI18n().sort());
		sortLabel.setShowTitle(false);
		StaticTextItem sizeLabel = new StaticTextItem("lsize");
		sizeLabel.setDefaultValue(Services.getI18n().size());
		sizeLabel.setShowTitle(false);
		StaticTextItem leftText = new StaticTextItem("lleft");
		leftText.setDefaultValue(Services.getI18n().leftContext());
		leftText.setShowTitle(false);
		StaticTextItem keyText = new StaticTextItem("lkey", "Keyword");
		keyText.setDefaultValue(Services.getI18n().keyword());
		keyText.setShowTitle(false);
		StaticTextItem rightText = new StaticTextItem("lrig", "right");
		rightText.setDefaultValue(Services.getI18n().rightContext());
		rightText.setShowTitle(false);
		StaticTextItem refText = new StaticTextItem("lref", "Reference");
		refText.setDefaultValue(Services.getI18n().references());
		refText.setShowTitle(false);

		StaticTextItem temp = new StaticTextItem("pref");
		temp.setShowTitle(false);
		StaticTextItem temp2 = new StaticTextItem("skey");
		temp2.setShowTitle(false);
		StaticTextItem temp3 = new StaticTextItem("sref");
		temp3.setShowTitle(false);
		queryForm.setWidth100();
		queryForm.setItems(queryField);


		form.setNumCols(5);
		form.setColWidths(50, "*", "*", "*", "*");
		form.setWidth100();
		form.setItems(
				columnLabel, refText, leftText, keyText, rightText,
				displayLabel, referenceViewField, leftViewPropertyField, keywordViewPropertyField, rightViewPropertyField,
				sortLabel, referenceSortField, leftSortPropertyField, keywordSortPropertyField, rightSortPropertyField,
				sizeLabel, temp3, leftContextField, temp2, rightContextField);

		section1.setMembers(assistantButton, queryForm, validationButton, settingsButton);

		section2.setMembers(sortForm, form);
		if (TXMWEB.EXPO) { // don't show the settings button
			this.setMembers(section2);
		} else {
			this.setMembers(section1, section2);
		}
		ProcessingWindow.hide();
	}

	public void setParameters(Map<String, String> parameters) {

		//SC.say("ConcordanceForm: "+parameters);

		for (String property : parameters.keySet()) {
			if (Keys.QUERY.equals(property)) {
				setParameter(queryForm, property, parameters.get(property));
			}  else if (Keys.KEY1.equals(property) || Keys.KEY2.equals(property) || Keys.KEY3.equals(property) || Keys.KEY4.equals(property)) {
				setParameter(sortForm.form, property, parameters.get(property));
			} else {
				setParameter(form, property, parameters.get(property));
			}
		}

		if (parameters.containsKey(Keys.CONTEXTS)) {

			form.getItem(Keys.LEFT_CONTEXT).setValue(parameters.get(Keys.CONTEXTS));
			form.getItem(Keys.RIGHT_CONTEXT).setValue(parameters.get(Keys.CONTEXTS));
		}

		if (parameters.containsKey(Keys.PROPERTIES) && parameters.get(Keys.PROPERTIES).length() > 0) {
			form.getItem(Keys.KEYWORD_VIEW_PROPERTIES).setValue(parameters.get(Keys.PROPERTIES));
		}
	}

	public void setContextSizes(String left, String right) {
		rightContextField.setDefaultValue(right);
		leftContextField.setDefaultValue(left);
	}

	public String[] getSortProperties() {
		String[] props = new String[3];
		props[0] = form.getValueAsString(Keys.LEFT_SORT_PROPERTIES);
		props[1] = form.getValueAsString(Keys.KEYWORD_SORT_PROPERTIES);
		props[2] = form.getValueAsString(Keys.RIGHT_SORT_PROPERTIES);
		return props;
	}

	public void validate() {
		ProcessingWindow.show(Services.getI18n().computing_conc(params.getQuery()));
		params.setQuery(queryForm.getValueAsString(Keys.QUERY));
		try {
			//TxmDialog.warning("form: "+form.getValues());
			//TxmDialog.warning("form v1: "+form.getValueAsString(Keys.RIGHT_CONTEXT));
			params.setRightContextSize(Integer.parseInt(form.getValueAsString(Keys.RIGHT_CONTEXT)));
			//TxmDialog.warning("form v2: "+form.getValueAsString(Keys.LEFT_CONTEXT));
			params.setLeftContextSize(Integer.parseInt(form.getValueAsString(Keys.LEFT_CONTEXT)));
			//TxmDialog.warning("form v3: "+parentTab.getResultPerPage());
			params.setResultPerPage(parentTab.getResultPerPage());
		}
		catch(Exception e) {
			if (!TXMWEB.EXPO) TxmDialog.warning(Services.getI18n().concordanceWarning1()+e);
			ProcessingWindow.hide();
			return;
		}
		params.setStartIndex(1);
		params.setEndIndex(params.getResultPerPage());

		// set sort properties
		params.setLeftSortProperties(new ArrayList<String>());
		Object leftSortProp = form.getItem(Keys.LEFT_SORT_PROPERTIES).getValue();
		if (leftSortProp != null) {
			String sortProperties = leftSortProp.toString();
			for (String s : sortProperties.split(", ")) {
				params.getLeftSortProperties().add(s);
			}
		} else {
			params.getLeftSortProperties().add(Keys.WORD);
		}

		params.setKeywordSortProperties(new ArrayList<String>());
		Object keywordSortProp = form.getItem(Keys.KEYWORD_SORT_PROPERTIES).getValue();
		if (keywordSortProp != null) {
			String sortProperties = keywordSortProp.toString();
			for (String s : sortProperties.split(", ")) {
				params.getKeywordSortProperties().add(s);
			}
		} else {
			params.getKeywordSortProperties().add(Keys.WORD);
		}

		params.setRightSortProperties(new ArrayList<String>());
		Object rightSortProp = form.getItem(Keys.RIGHT_SORT_PROPERTIES).getValue();
		if (rightSortProp != null) {
			String sortProperties = rightSortProp.toString();
			for (String s : sortProperties.split(", ")) {
				params.getRightSortProperties().add(s);
			}
		} else {
			params.getRightSortProperties().add(Keys.WORD);
		}

		// set view properties
		params.setLeftViewProperties(new ArrayList<String>());
		Object leftViewProp = form.getItem(Keys.LEFT_VIEW_PROPERTIES).getValue();
		if (leftViewProp != null) {
			String viewProperties = leftViewProp.toString();
			for (String s : viewProperties.split(", ")) {
				params.getLeftViewProperties().add(s);
			}
		} else {
			params.getLeftViewProperties().add(Keys.WORD);
		}

		params.setKeywordViewProperties(new ArrayList<String>());
		Object keywordViewProp = form.getItem(Keys.KEYWORD_VIEW_PROPERTIES).getValue();
		if (keywordViewProp != null) {
			String viewProperties = keywordViewProp.toString();
			for (String s : viewProperties.split(", ")) {
				params.getKeywordViewProperties().add(s);
			}
		} else {
			params.getKeywordViewProperties().add(Keys.WORD);
		}

		params.setRightViewProperties(new ArrayList<String>());
		Object rightViewProp = form.getItem(Keys.RIGHT_VIEW_PROPERTIES).getValue();
		if (rightViewProp != null) {
			String viewProperties = rightViewProp.toString();
			for (String s : viewProperties.split(", ")) {
				params.getRightViewProperties().add(s);
			}
		} else {
			params.getRightViewProperties().add(Keys.WORD);
		}

		params.setViewReferences(new ArrayList<String>());
		Object ref = form.getItem(Keys.REFERENCES).getValue();
		if (ref != null) {
			String reference = ref.toString();
			for (String s : reference.split(", ")) {
				params.getViewReferences().add(s);
			}
		}

		params.setSortReferences(new ArrayList<String>());
		ref = form.getItem(Keys.REFERENCE_SORT_PROPERTIES).getValue();
		if (ref != null) {
			String reference = ref.toString();
			for (String s : reference.split(", ")) {
				params.getSortReferences().add(s);
			}
		}

		params.setSortKeys(new ArrayList<String>(Arrays.asList(sortForm.getValues())));

		if (params.getQuery() == null || params.getQuery().trim().equals("")) {
			if (!TXMWEB.EXPO) TxmDialog.warning(Services.getI18n().noQuery());
			ProcessingWindow.hide();
			return;
		}
		if (params.getQuery().matches(".*%(d|cd|dc).*")) {
			if (!TXMWEB.EXPO) TxmDialog.warning(Services.getI18n().queryDError());
			ProcessingWindow.hide();
			return;
		}
		ExecTimer.start();
		Services.getConcordanceService().concordance(params, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				ProcessingWindow.hide();
				if (result == null || result.length() > 0) {
					if (result.equals("permission")) {
						TxmDialog.warning(Services.getI18n().noPermission());
					}
					else if(result.equals("session")) {
						TxmDialog.warning(Services.getI18n().sessionExpired());
					}
					else
					{
						if (!TXMWEB.EXPO) TxmDialog.warning(Services.getI18n().querySyntaxError(result));
						parentTab.fillList(new ConcordanceResult());
					}
				}
				else {
					Services.getConcordanceService().getLines(params, new AsyncCallback<ConcordanceResult>() {
						@Override
						public void onFailure(Throwable caught) {
							ProcessingWindow.hide();
							if (!TXMWEB.EXPO) TxmDialog.warning(Services.getI18n().concordanceError());
						}
						@Override
						public void onSuccess(ConcordanceResult result) {
							ProcessingWindow.hide();
							parentTab.setFirstWarning(true);
							parentTab.fillList(result);

							if (TXMWEB.EXPO) { // automatically highlight the first line
								parentTab.selectRecord(0);
								parentTab.doBackToText(null);
							}

							//TXMWEB.getConsole().addMessage(""+result.size()+Services.getI18n().occurrence()+((result.() > 1) ? "s":""), true);
						}
					});
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				TxmDialog.warning(Services.getI18n().concordanceError());
			}
		});
	}

	public void setParams(ConcordanceParam params) {
		this.params = params;
	}

	public ConcordanceParam getParams() {
		return params;
	}

	public ArrayList<String> getLeftSortProperties() {
		ArrayList<String> ret = new ArrayList<String>();
		Object leftSortProp = form.getItem(Keys.LEFT_SORT_PROPERTIES).getValue();
		if (leftSortProp != null) {
			String sortProperties = leftSortProp.toString();
			for (String s : sortProperties.split(", ")) {
				ret.add(s);
			}
		} else {
			ret.add(Keys.WORD);
		}
		return ret;
	}

	public ArrayList<String> getRightSortProperties() {
		ArrayList<String> ret = new ArrayList<String>();
		Object rightSortProp = form.getItem(Keys.RIGHT_SORT_PROPERTIES).getValue();
		if (rightSortProp != null) {
			String sortProperties = rightSortProp.toString();
			for (String s : sortProperties.split(", ")) {
				ret.add(s);
			}
		} else {
			ret.add(Keys.WORD);
		}
		return ret;
	}

	public ArrayList<String> getKeywordSortProperties() {
		ArrayList<String> ret = new ArrayList<String>();
		Object keywordSortProp = form.getItem(Keys.KEYWORD_SORT_PROPERTIES).getValue();
		if (keywordSortProp != null) {
			String sortProperties = keywordSortProp.toString();
			for (String s : sortProperties.split(", ")) {
				ret.add(s);
			}
		} else {
			ret.add(Keys.WORD);
		}
		return ret;
	}

	public void setQuery(String query) {
		this.queryForm.setValue(Keys.QUERY, query);
	}

	public void setViewProperties(String props) {
		this.form.setValue(Keys.KEYWORD_VIEW_PROPERTIES, props);
	}

	public void setReferenceProperties(String refs) {
		this.form.setValue(Keys.REFERENCES, refs);
	}
}
