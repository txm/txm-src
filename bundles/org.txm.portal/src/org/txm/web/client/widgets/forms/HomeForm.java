package org.txm.web.client.widgets.forms;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.CenterPanel;
import org.txm.web.client.widgets.tabs.HTMLTab;

public class HomeForm {
	
	public static void validate() {
		String home_tab_name = Services.getI18n().homeTab();
		if (TXMWEB.EXPO) home_tab_name += "_expo";

		CenterPanel center = TXMWEB.getCenterPanel();
		HTMLTab homePage = center.getUniqTab(home_tab_name);
		if (homePage == null)
			return;
		homePage.setCanClose(false);
		center.selectTab(homePage);
	}
}
