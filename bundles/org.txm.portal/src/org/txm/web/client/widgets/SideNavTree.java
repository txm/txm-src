package org.txm.web.client.widgets;

import java.util.ArrayList;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.menus.TreeContextMenu;
import org.txm.web.shared.NavigationTree;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellContextClickEvent;
import com.smartgwt.client.widgets.grid.events.CellContextClickHandler;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.tree.Tree;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;
import com.smartgwt.client.widgets.tree.TreeNode;

public class SideNavTree extends TreeGrid {

	private Tree tree = new Tree();
	TreeContextMenu menu = new TreeContextMenu();
	
	public SideNavTree() {
		this.setID("sideNavTree");
		this.setPrompt(Services.getI18n().navTreeToolTip());
		this.setCanReorderFields(false);
		this.setCanPickFields(true);
		this.setCanMultiSort(true);
//		this.setHoverOpacity(75);
		this.setWidth100();
		this.setLeft(0);
		this.setHeight100();
		this.setBorder("0px solid white");
//		this.setEmptyMessage(Services.getI18n().sideNavTreeEmptyMessage());

		//menu popup
		this.addCellContextClickHandler(new CellContextClickHandler() {
			@Override
			public void onCellContextClick(CellContextClickEvent event) {
				event.cancel();
				if (tree.getLength() > 0) {
					menu.refreshEnables(true);
					RootPanel.get().add(menu, event.getX(), event.getY());
					//System.out.println("visible? "+menu.isVisible());
				}
			}
		});
	
		this.addSelectionChangedHandler(new SelectionChangedHandler() {
			@Override
			public void onSelectionChanged(SelectionEvent event) {
				if (tree.getLength() > 0) {
					TXMWEB.getMenuBar().getFunctionBar().refreshEnables();
				}
			}
		});
		
		ArrayList<TreeGridField> fields = new ArrayList<TreeGridField>();
		// name field
		TreeGridField field = new TreeGridField("Name");
		field.setTitle(Services.getI18n().sideNavTreeTitle());
		field.setName("path2");
//		field.setCellFormatter(new CellFormatter() {
//			@Override
//			public String format(Object value, ListGridRecord record, int rowNum,
//					int colNum) {
//				return ((String)value).substring(1);
//			}
//		});
		
		fields.add(field);
		
		// ns field
		TreeGridField nsfield = new TreeGridField("Ns");
		nsfield.setTitle("ns");
		nsfield.setName("ns");
		nsfield.setHidden(true);
		fields.add(nsfield);
		
		// path field
		TreeGridField pathfield = new TreeGridField("Path");
		pathfield.setTitle(Services.getI18n().path());
		pathfield.setName("path");
		pathfield.setHidden(true);
		pathfield.setCanHide(true);
		fields.add(pathfield);
		
		// path field
//		TreeGridField pathWithoutNsfield = new TreeGridField("PathWithoutNs");
//		pathWithoutNsfield.setTitle(Services.getI18n().path()+"2");
//		pathWithoutNsfield.setName("path2");
//		pathWithoutNsfield.setHidden(true);
//		pathWithoutNsfield.setCanHide(true);
//		fields.add(pathWithoutNsfield);
		
		this.setFields(fields.toArray(new ListGridField[]{}));

		//tree.setModelType(TreeModelType.PARENT);

		TXMWEB.log(Services.getI18n().loadingCorpora());
		Services.getUIService().fillSideNavTree(new AsyncCallback<NavigationTree>() {
			@Override
			public void onFailure(Throwable caught) {
				SC.say(Services.getI18n().sideNavTreeError());
			}
			@Override
			public void onSuccess(NavigationTree result) {
				tree.setData(result.toTreeNodeArray());
				setData(tree);
				TXMWEB.log("");
				sort();
				
				if (TXMWEB.sideNavTreeFirstLoad) {
					TXMWEB.testCommand();
					TXMWEB.sideNavTreeFirstLoad = false;
				}
			}
		});
	}

	public Tree getTree() {
		return tree;
	}

	public void updateTree() {
		TXMWEB.log(Services.getI18n().updateCorpora());
		Services.getUIService().fillSideNavTree(new AsyncCallback<NavigationTree>() {
			@Override
			public void onFailure(Throwable caught) {
				SC.say(Services.getI18n().sideNavTreeError());
			}
			@Override
			public void onSuccess(NavigationTree result) {
				tree.removeList(tree.getAllNodes());
				TreeNode[] tn = result.toTreeNodeArray();
				
				for (TreeNode node : tn)
					tree.add(node, "/");
				refreshFields();
				TXMWEB.log("");
				TXMWEB.getMenuBar().getFunctionBar().refreshEnables();
			}
		});
	}
}
