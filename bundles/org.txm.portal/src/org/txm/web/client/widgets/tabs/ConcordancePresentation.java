package org.txm.web.client.widgets.tabs;

import java.util.ArrayList;

import org.txm.web.shared.ConcordanceResult;

import com.smartgwt.client.widgets.grid.events.GridRowColEvent;

public interface ConcordancePresentation {
	public void fillList(ConcordanceResult result);
	public int getResultPerPage();
	public void setFirstWarning(boolean b);
	public ArrayList<String> getLeftSortProperties();
	public ArrayList<String> getKeywordSortProperties();
	public ArrayList<String> getRightSortProperties();

	public void selectRecord(int n);
	
	public void doBackToText(@SuppressWarnings("rawtypes") GridRowColEvent event);
	public boolean getComputeAtOpening();
}
