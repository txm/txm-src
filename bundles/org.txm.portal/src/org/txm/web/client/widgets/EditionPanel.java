package org.txm.web.client.widgets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.txm.web.client.Commands;
import org.txm.web.client.ExecTimer;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.forms.NavigationToolStrip;
import org.txm.web.client.widgets.forms.OrderedPickListItem;
import org.txm.web.exceptions.PermissionException;
import org.txm.web.shared.EditionResult;
import org.txm.web.shared.Keys;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
/**
 * Edition panel = html panel + navigation bar
 * @author vchabanis
 *
 */
public class EditionPanel extends VLayout {

	private String ID = this.getID();
	private TxmParaHTMLPane editionContents ;
	public ArrayList<EditionResult> currentResults = null;
	private ToolStrip editionBar = new ToolStrip();

	public final static String MOTIF = "motif";
	public final static String STRUCTURE = "structure";
	public final static String PAGE = "page";
	private String selection = PAGE;

	//	ToolStripButton navOKBtn = new ToolStripButton("OK");
	//	ToolStripButton navPreviousBtn = new ToolStripButton("previous");
	//	ToolStripButton navNextBtn = new ToolStripButton("next");
	//	ToolStripButton linkBtn = new ToolStripButton("link");
	OrderedPickListItem editionSel; // is dialog
	SectionStack sectionStack;
	SectionStackSection editionSection;
	SectionStackSection paramsSection ;  
	HLayout selectLayout = new HLayout();
	NavigationToolStrip navEditionBar;

	PaginationWidget paginationWidget = new PaginationWidget();
	Label openedEditionNames = new Label();
	IButton openbiblioBtn = new IButton(Services.getI18n().biblio());
	IButton openPDFBtn = new IButton("PDF");
	private Tab parentTab;
	private String itemPath;
	private ArrayList<String> availableEditionsValues = new ArrayList<String>();
	private ArrayList<String> selectedEditions = new ArrayList<String>();
	private String text;


	int pageInputErrorCount = 0; // stores the number of time the user tried and failed to select a page 

	/**
	 * true = display the edition when this widget is created
	 */
	private boolean autoload; 

	public EditionPanel(String path, final String text){
		this(path, text, null, null, null, false);
	}

	public EditionPanel(String path, final String text, final String editions, final String pageid, final String wordids, boolean autoload) {
		
		this.text = text;

		//	this.setSize(TXMWEB.DEFAULTEDITIONPANELSIZE, "100%");
		this.autoload = autoload;

		String maincorpuspath = path.substring(1);
		if (maincorpuspath.indexOf("/") > 0) {
			maincorpuspath = "/"+maincorpuspath.substring(0, maincorpuspath.indexOf("/"));
		} else {
			maincorpuspath = path;
		}
		this.itemPath = maincorpuspath;
	
		Services.getEditionService().getEditionNames(itemPath, new AsyncCallback<HashMap<String, ArrayList<String>>>() {
			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.severe("Error: getEditionNames failed: "+caught.toString());
			}
			@Override
			public void onSuccess(HashMap<String, ArrayList<String>> results) {
				if (results == null) { TxmDialog.severe("Error while getting editions of "+itemPath);return;}
				
				for (String corpusname : results.keySet()) { // initialize available editions
					for (String edition : results.get(corpusname)) {
						if (itemPath.startsWith("/"+corpusname)) {
							availableEditionsValues.add(edition.trim());
						} else {
							availableEditionsValues.add(corpusname.trim()+" - "+edition.trim());
						}
					}
				}
				
				if (availableEditionsValues.size() > 0) {
					if (editions != null) {
						//System.out.println("PARAMETER: "+editions);
						String[] split = editions.split(",");
						//selectedEditions = new String[split.length];
						for (int i = 0 ; i < split.length ; i++) {
							String edition = itemPath.substring(1)+" - "+split[i];
							if (availableEditionsValues.contains(edition)) { // the edition was given using the simple name
								selectedEditions.add(edition); 
							} else if (availableEditionsValues.contains(split[i])) { // the edition was given using the fullname
								selectedEditions.add(split[i]);
							} else {
								TxmDialog.warning("Missing edition: "+edition);
							}
						}
						if (selectedEditions.size() == 0) { // no edition was selected, select the first available
							selectedEditions.add(availableEditionsValues.get(0));
						}
						System.out.println("editions: "+editions);
						init(pageid, wordids);
					} else { // use corpus default edition (the first one)
						Services.getEditionService().getDefaultEditions(itemPath, new AsyncCallback<ArrayList<String>>() {
							@Override
							public void onFailure(Throwable caught) {
								TxmDialog.severe("Error: getEditionNames failed: "+caught.toString());
							}
							@Override
							public void onSuccess(ArrayList<String> results) {
								if (results.size() == 0) {
									results.add(availableEditionsValues.get(0));
								}
								selectedEditions.addAll(results);
								System.out.println("DEFAULT EDITIONS: "+selectedEditions.toString());
								init(pageid, wordids);
							}
						});
					}
					
				} else {
					TxmDialog.severe(itemPath+" has no edition");
				}
			}
		});
	}

	public void getEditionByName(String textid) {

		String[] textIds = new String[currentResults.size()];
		String[] currentPages = new String[currentResults.size()];
		for (int i = 0 ; i < currentResults.size() ; i++) {
			textIds[i] = currentResults.get(i).getTextId();
			currentPages[i] = currentResults.get(i).getCurrentPage();
		}		

		Services.getEditionService().getEditionByName(currentResults.get(0).getBaseId(), textid, selectedEditions, new AsyncCallback<ArrayList<EditionResult>>() {

			@Override
			public void onFailure(Throwable caught) {
				SC.warn(Services.getI18n().backToTextError());
				TXMWEB.error(Services.getI18n().backToTextError());
			}
			@Override
			public void onSuccess(ArrayList<EditionResult> results) {
				updateEditionsContent(results, selectedEditions);
				updateBar();
				TXMWEB.log("");
			}
		});
	}

	private void init(String initialPageid, String initialWordids) {
		
		sectionStack = new SectionStack();  
		sectionStack.setVisibilityMode(VisibilityMode.MULTIPLE);  
		sectionStack.setWidth100();   
		sectionStack.setHeight100();

		ToolStripButton previousText = new ToolStripButton();
		previousText.setIcon("icons/control_rewind.png");
		ToolStripButton nextText = new ToolStripButton();
		nextText.setIcon("icons/control_fastforward.png");
		ToolStripButton previousPage = new ToolStripButton();
		previousPage.setIcon("icons/control_reverse.png");
		ToolStripButton nextPage = new ToolStripButton();
		nextPage.setIcon("icons/control_play.png");
		ToolStripButton firstPage = new ToolStripButton();
		firstPage.setIcon("icons/control_start.png");
		ToolStripButton lastPage = new ToolStripButton();
		lastPage.setIcon("icons/control_end.png");

		previousPage.setPrompt(Services.getI18n().previousPage());
		nextPage.setPrompt(Services.getI18n().nextPage());
		firstPage.setPrompt(Services.getI18n().firstPage());
		lastPage.setPrompt(Services.getI18n().lastPage());
		previousText.setPrompt(Services.getI18n().previousText());
		nextText.setPrompt(Services.getI18n().nextText());

		openbiblioBtn.setAutoFit(true);
		openbiblioBtn.setTooltip("Open the bibliography of the current text");
		openbiblioBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				openBiblio();
				TXMWEB.log("");
			}
		});

		openPDFBtn.setAutoFit(true);
		openPDFBtn.setTooltip("Open the PDF of the current text");
		openPDFBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				openPDF();
				TXMWEB.log("");
			}
		});

		previousPage.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				TXMWEB.log(Services.getI18n().loadingPreviousPage());

				String[] textIds = new String[currentResults.size()];
				String[] currentPages = new String[currentResults.size()];
				for (int i = 0 ; i < currentResults.size() ; i++) {
					textIds[i] = currentResults.get(i).getTextId();
					currentPages[i] = currentResults.get(i).getCurrentPage();
				}
				ExecTimer.start();
				Services.getEditionService().getPreviousPage(currentResults.get(0).getBaseId(), textIds, selectedEditions, currentPages, new AsyncCallback<ArrayList<EditionResult>>() {
					@Override
					public void onFailure(Throwable caught) {
						SC.warn(Services.getI18n().backToTextError());
						TXMWEB.error(Services.getI18n().backToTextError());
					}
					@Override
					public void onSuccess(ArrayList<EditionResult> results) {
						updateEditionsContent(results, selectedEditions);
						updateBar();
						TXMWEB.log("");
					}
				});
			}
		});

		nextPage.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				TXMWEB.log(Services.getI18n().loadingNextPage());

				String[] textIds = new String[currentResults.size()];
				String[] currentPages = new String[currentResults.size()];
				for (int i = 0 ; i < currentResults.size() ; i++) {
					textIds[i] = currentResults.get(i).getTextId();
					currentPages[i] = currentResults.get(i).getCurrentPage();
				}
				ExecTimer.start();
				Services.getEditionService().getNextPage(currentResults.get(0).getBaseId(), textIds, selectedEditions, currentPages,  new AsyncCallback<ArrayList<EditionResult>>() {

					@Override
					public void onFailure(Throwable caught) {
						SC.warn(Services.getI18n().backToTextError());
						TXMWEB.error(Services.getI18n().backToTextError());
					}
					@Override
					public void onSuccess(ArrayList<EditionResult> results) {
						updateEditionsContent(results, selectedEditions);
						updateBar();
						TXMWEB.log("");
					}
				});
			}
		});

		firstPage.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				TXMWEB.log(Services.getI18n().loadingFirstPage());

				String[] textIds = new String[currentResults.size()];
				String[] currentPages = new String[currentResults.size()];
				for (int i = 0 ; i < currentResults.size() ; i++) {
					textIds[i] = currentResults.get(i).getTextId();
					currentPages[i] = currentResults.get(i).getCurrentPage();
				}
				ExecTimer.start();
				Services.getEditionService().getFirstPage(currentResults.get(0).getBaseId(), textIds, selectedEditions, currentPages,  new AsyncCallback<ArrayList<EditionResult>>() {

					@Override
					public void onFailure(Throwable caught) {
						SC.warn(Services.getI18n().backToTextError());
					}
					@Override
					public void onSuccess(ArrayList<EditionResult> results) {
						updateEditionsContent(results, selectedEditions);
						updateBar();
						TXMWEB.log("");
					}
				});
			}
		});

		lastPage.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				TXMWEB.log(Services.getI18n().loadingLastPage());

				String[] textIds = new String[currentResults.size()];
				String[] currentPages = new String[currentResults.size()];
				for (int i = 0 ; i < currentResults.size() ; i++) {
					textIds[i] = currentResults.get(i).getTextId();
					currentPages[i] = currentResults.get(i).getCurrentPage();
				}
				ExecTimer.start();
				Services.getEditionService().getLastPage(currentResults.get(0).getBaseId(), textIds, selectedEditions, currentPages, new AsyncCallback<ArrayList<EditionResult>>() {
					@Override
					public void onFailure(Throwable caught) {
						SC.warn(Services.getI18n().backToTextError());
					}
					@Override
					public void onSuccess(ArrayList<EditionResult> results) {
						updateEditionsContent(results, selectedEditions);
						updateBar();
						TXMWEB.log("");
					}
				});
			}
		});

		previousText.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				TXMWEB.log(Services.getI18n().loadingPreviousText());

				String[] textIds = new String[currentResults.size()];
				for (int i = 0 ; i < currentResults.size() ; i++) {
					textIds[i] = currentResults.get(i).getTextId();
				}
				ExecTimer.start();
				Services.getEditionService().getPreviousText(currentResults.get(0).getBaseId(), textIds, selectedEditions, new AsyncCallback<ArrayList<EditionResult>>() {
					@Override
					public void onFailure(Throwable caught) {
						if (caught instanceof PermissionException)
							SC.warn(Services.getI18n().editionPermissionException());
					}
					@Override
					public void onSuccess(ArrayList<EditionResult> results) {
						updateEditionsContent(results, selectedEditions);
						updateBar();
						TXMWEB.log("");
					}
				});
			}
		});

		nextText.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				TXMWEB.log(Services.getI18n().loadingNextText());

				String[] textIds = new String[currentResults.size()];
				for (int i = 0 ; i < currentResults.size() ; i++) {
					textIds[i] = currentResults.get(i).getTextId();
				}

				System.out.println("text ids: "+ Arrays.toString(textIds));
				ExecTimer.start();
				Services.getEditionService().getNextText(itemPath, textIds, selectedEditions, new AsyncCallback<ArrayList<EditionResult>>() {
					@Override
					public void onFailure(Throwable caught) {
						if (caught instanceof PermissionException)
							SC.warn(Services.getI18n().editionPermissionException());
					}
					@Override
					public void onSuccess(ArrayList<EditionResult> results) {
						updateEditionsContent(results, selectedEditions);
						updateBar();
						TXMWEB.log("");
					}
				});
			}
		});

		paginationWidget.setOnEnterEvent(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event != null && event.getKeyName() != null) {
					if (event.getKeyName().equals("Escape")) {
						updatePageNumber();
					} else if (event.getKeyName().equals("Enter")) {

						if (currentResults.size() == 0) return;
						TXMWEB.log("Loading page with ID="+paginationWidget.getPageID());

						final String text = currentResults.get(0).getTextId();
						final String pageid = paginationWidget.getPageID();

						Services.getEditionService().hasPageWithID(itemPath, text, selectedEditions.get(0), pageid, new AsyncCallback<Boolean>() {
							@Override
							public void onFailure(Throwable caught) {
								if (caught instanceof PermissionException)
									SC.warn(Services.getI18n().editionPermissionException());
							}
							@Override
							public void onSuccess(Boolean result) {
								if (result == null) {
									TxmDialog.severe(Services.getI18n().internalError());
								}
								if (!result) {
									paginationWidget.showWarning(true);
									String message = Services.getI18n().noPageWithID(pageid);
									TXMWEB.error(message);
									pageInputErrorCount++;
									if (pageInputErrorCount >= 5) {
										TxmDialog.severe(message);
										pageInputErrorCount = 0;
										updatePageNumber();										
									}
									return;
								}
								pageInputErrorCount = 0;
								paginationWidget.showWarning(false);
								String[] pages = new String[currentResults.size()];
								for (int i = 0 ; i < pages.length ; i++) {
									pages[i] = pageid;
								}
								
								ExecTimer.start();
								Services.getEditionService().getEditions(itemPath, text, selectedEditions, pages, new AsyncCallback<ArrayList<EditionResult>>() {
									@Override
									public void onFailure(Throwable caught) {
										if (caught instanceof PermissionException)
											SC.warn(Services.getI18n().editionPermissionException());
									}
									@Override
									public void onSuccess(ArrayList<EditionResult> results) {
										updateEditionsContent(results, selectedEditions);
										updateBar();
										TXMWEB.log("");
									}
								});
							}
						});
					}
				}
			}
		});

		//		linkBtn.setActionType(SelectionType.CHECKBOX);
		//		linkBtn.setState(State.STATE_DOWN);
		//		PickerIcon refreshEditions = new PickerIcon(PickerIcon.REFRESH);
		//		editionSel.addIcon(refreshEditions);
		//
		//		refreshEditions.addFormItemClickHandler(new FormItemClickHandler() {
		//			@Override
		//			public void onFormItemClick(FormItemIconClickEvent event) {				
		//				//Pattern splitter = Pattern.matches(regex, editionSel.getValueAsString());
		//				refreshEditionPanels();
		//			}
		//		});

		DynamicForm form = new DynamicForm();
		form.setHeight(1);
		form.setWidth(75);
		form.setNumCols(1);

		//TODO: fix navigation per motive (motif) then enable this combo
		//		SelectItem selectItem = new SelectItem();
		//		selectItem.setWidth(120);  
		//		selectItem.setShowTitle(false);  
		//		selectItem.setValueMap(EditionPanel.MOTIF, EditionPanel.PAGE);  // , EditionPanel.STRUCTURE
		//		selectItem.setDefaultValue(EditionPanel.PAGE);  
		//
		//		selectItem.addChangeHandler(new ChangeHandler() {
		//			@Override
		//			public void onChange(ChangeEvent event) {
		//				selection = (String)event.getValue();
		//				paramsSection.setTitle(selection);
		//				changeNavigationTool();
		//				paramsSection.setExpanded(true);
		//			}
		//		});
		//		form.setFields(selectItem);

		editionSel = new OrderedPickListItem("Editions", true) {
			@Override
			public void OnValidate() {
				refreshEditionPanels();
			}
		};
		DynamicForm form2 = new DynamicForm();
		if (!TXMWEB.EXPO) {
			form2.setItems(editionSel);
		}
		if (availableEditionsValues == null || availableEditionsValues.size() == 0) {
			TxmDialog.severe("No editions");
		} else {
			editionSel.setValueMap(availableEditionsValues);
			editionSel.setDefaultValues(selectedEditions);
			//TxmDialog.severe("Availables="+availableEditionsValues+" selected="+selectedEditions);
		}

		paramsSection = new SectionStackSection(selection);
		paramsSection.setShowHeader(!TXMWEB.EXPO); // no widget to show in EXPO mode
		//paramsSection.setControls(form, navOKBtn, navPreviousBtn, navNextBtn, linkBtn, form2);
		if (availableEditionsValues.size() > 1) {
			paramsSection.setControls(form, form2);
		} else {
			paramsSection.setControls(form);
		}

		paramsSection.addItem(selectLayout);

		changeNavigationTool();
		paramsSection.setExpanded(true);

		openedEditionNames.setWidth("50%");
		openedEditionNames.setID(ID + "textName");
		paginationWidget.setID(ID + "pagination");
		editionBar.setWidth100();
		editionBar.setHeight(25);
		if (!TXMWEB.EXPO) {
			editionBar.addMember(openedEditionNames);
		} else {
			editionBar.addFill();
		}
		editionBar.addMember(previousText);
		editionBar.addMember(firstPage);
		editionBar.addMember(previousPage);
		editionBar.addMember(paginationWidget);
		editionBar.addMember(nextPage);
		editionBar.addMember(lastPage);
		editionBar.addMember(nextText);
		editionBar.addFill();
		if (!TXMWEB.EXPO) {
			editionBar.addMember(openPDFBtn);
			editionBar.addMember(openbiblioBtn);
		}

		editionSection = new SectionStackSection();
		editionSection.setShowHeader(false);
		editionSection.setCanCollapse(false);
		editionSection.setExpanded(true);
		//System.out.println("CREATE TXMPARAHTMLPANE: "+availableEditionsValues);
		editionContents = new TxmParaHTMLPane(availableEditionsValues);
		editionSection.setItems(editionContents);

		sectionStack.setVisibilityMode(VisibilityMode.MULTIPLE);
		sectionStack.setSections(editionSection, paramsSection);
		paramsSection.setCanCollapse(!TXMWEB.EXPO);
		
		this.setMembers(sectionStack);
		if (autoload) {
			//String [] selected = {selectedEditions[0]};
			if (initialWordids != null && initialWordids.length() > 0) {
				this.backToText(itemPath, text, initialWordids.split(","));
			} else {
				this.getEditions(itemPath, text, initialPageid);
			}
		}
	}

	public void refreshEditionPanels() {

		String [] selections = editionSel.getValueAsString().split(", ");
		this.selectedEditions = new ArrayList<String>();
		for (int i = 0 ; i < selections.length ; ++i) {
			selectedEditions.add(selections[i].trim());
			//System.out.println("refresh edition ["+selectedEditions[i]+"]");
		}
		
		getEditions(itemPath, this.text, null);
		//		if (selectedEditions != null && selectedEditions.size() > 0) { // refresh edition choices of "motif"
		//			corpusSelectItem.setValueMap(availableEditionsValues.toArray(new String[availableEditionsValues.size()]));
		//			corpusSelectItem.setDefaultValue(selectedEditions);
		//		}
	}
	
	private void updatePageNumber() {
		
		if (currentResults.size() > 0) {
			EditionResult tmp = currentResults.get(0);
			currentPage = tmp.getCurrentPage();
			paginationWidget.update(currentPage, tmp.getTotalPage());
		}
	}

	protected void openBiblio() {
		
		ArrayList<EditionResult> results = this.currentResults;
		if (results.size() > 0) {
			EditionResult currentResult = results.get(0);
			if (currentResult.getBiblio() != null && currentResult.getBiblio().length() > 0) {
				//Commands.callRecord(itemPath, currentResult.getTextId(), currentResult.getBiblio());
				Commands.callBiblio(Commands.createParameters(Keys.PATH, itemPath, Keys.TEXTID, currentResult.getTextId()));
//				HTMLTab htmltab = new HTMLTab(currentResult.getTextId(), currentResult.getTextId());
//				htmltab.setContentUrl(currentResult.getBiblio());
//				htmltab.addInView();
			} else {
				TXMWEB.log(Services.getI18n().textWithNoBiblio(currentResult.getTextId()));
			}
		}
	}

	protected void openPDF() {
		
		ArrayList<EditionResult> results = this.currentResults;
		if (results.size() > 0) {
			final EditionResult currentResult = results.get(0);
			if (currentResult.getBiblio() != null && currentResult.getBiblio().length()  > 0) {
				Services.getCorpusActionService().reportActivity("PDF", itemPath+" "+currentResult.getTextId(), new AsyncCallback<Boolean>() {
					@Override
					public void onSuccess(Boolean result) {
						if (result) {
							// TODO: change this when PDFs will be stored in the private part of tomcat
							Window.open(TXMWEB.BASEURL+"/"+currentResult.getPDF(), "pdf_"+currentResult.getTextId()+"_"+Math.random()*10000, "");
						}							
					}

					@Override
					public void onFailure(Throwable caught) { }
				});
			} else {
				TXMWEB.log(Services.getI18n().textWithNoBiblio(currentResult.getTextId()));
			}
		}
	}

	ToolStrip queryBar = new ToolStrip();
	//ComboBoxItem corpusSelectItem = new ComboBoxItem("selectQueryEdition", "Chercher dans");
	TextItem searchItem = new TextItem("pickerControls", "Requête");
	ToolStripButton previousMatchButton = new ToolStripButton();
	ToolStripButton nextMatchButton = new ToolStripButton();
	protected String currentNavigationEdition;
	protected String currentNavigationQuery;
	private int[] startPositions;
	private int currentPosition = -1;
	private String currentPage;
	Element previousElem;
	private void initQueryBar() {
		
		//TODO: move to member zone if this functionnality is re-enable
		final SelectItem corpusSelectItem = new SelectItem("selectQueryEdition", "Chercher dans");
		queryBar.setWidth100();
		queryBar.setHeight(25);

		PickerIcon searchIcon = new PickerIcon(PickerIcon.SEARCH, new FormItemClickHandler() {  
			public void onFormItemClick(FormItemIconClickEvent event) {  
				String edition = corpusSelectItem.getValueAsString();
				String query = searchItem.getValueAsString();
				if (query != null && query.length() > 0 && edition != null && edition.length() > 0) {
					currentNavigationEdition = edition;
					currentNavigationQuery = query;

					Services.getEditionService().getPositions(itemPath, query, new AsyncCallback<int[]>() {
						@Override
						public void onFailure(Throwable caught) {
							TxmDialog.severe("Error: getPositions failed: "+caught.toString());
							nextMatchButton.setDisabled(true);
							previousMatchButton.setDisabled(true);
						}

						@Override
						public void onSuccess(int[] result) {
							startPositions = result;
							if (result == null || result.length == 0) {
								nextMatchButton.setDisabled(true);
								previousMatchButton.setDisabled(true);
								currentPosition = -1;
							} else {
								TXMWEB.getConsole().addMessage(Services.getI18n().occurences(startPositions.length));
								nextMatchButton.setDisabled(false);
								previousMatchButton.setDisabled(false);
								currentPosition = 0;
								updateEditionsFromCurrentPosition();
							}
						}
					});
				}
			}  
		});  

		searchItem.setIcons(searchIcon);
		corpusSelectItem.setType("comboBox");
		if (selectedEditions != null) {
			corpusSelectItem.setValueMap(selectedEditions.toArray(new String[selectedEditions.size()]));
			if (selectedEditions.size() > 0) {
				corpusSelectItem.setDefaultValue(selectedEditions.get(0));
			}
		}

		nextMatchButton.setIcon("icons/control_play.png");
		nextMatchButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				TXMWEB.getConsole().addMessage("next match of "+currentNavigationQuery+ " in "+currentNavigationEdition);

				if (currentPosition < startPositions.length -1) {
					currentPosition++;
					updateEditionsFromCurrentPosition();
				}
			}
		});

		previousMatchButton.setIcon("icons/control_reverse.png");
		previousMatchButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				TXMWEB.getConsole().addMessage("previous match of "+currentNavigationQuery+ " in "+currentNavigationEdition);
				if (currentPosition > 0) {
					currentPosition--;
					updateEditionsFromCurrentPosition();
				}
			}
		});

		nextMatchButton.setDisabled(true);
		previousMatchButton.setDisabled(true);
		//selectForm.setFields(corpusSelectItem, searchItem);
		queryBar.addFormItem(corpusSelectItem);
		queryBar.addFormItem(searchItem);
		queryBar.addButton(previousMatchButton);
		queryBar.addButton(nextMatchButton);
		queryBar.addFill();
	}

	public void updateEditionsFromCurrentPosition() {
		
		if (currentPosition >= 0) {
			//final String edition = corpusSelectItem.getValueAsString();
			final TxmHTMLPane panel = editionContents.getEditionPanels().get(currentNavigationEdition);
			if (currentPosition == 0) {
				nextMatchButton.setDisabled(false);
				previousMatchButton.setDisabled(true);
			} else if (currentPosition == startPositions.length -1) {
				nextMatchButton.setDisabled(true);
				previousMatchButton.setDisabled(false);
			} else {
				nextMatchButton.setDisabled(false);
				previousMatchButton.setDisabled(false);
			}

			String[] textIds = new String[currentResults.size()];
			for (int i = 0 ; i < currentResults.size() ; i++) {
				textIds[i] = currentResults.get(i).getTextId();
			}

			Services.getEditionService().getEditionsForPosition(itemPath, currentNavigationEdition, textIds, selectedEditions, startPositions[currentPosition], new AsyncCallback<EditionResult>() {
				@Override
				public void onFailure(Throwable caught) {
					TxmDialog.severe("Error: getEditionsForPosition failed: "+caught.toString());
				}

				@Override
				public void onSuccess(EditionResult result) {

					if (!result.getCurrentPage().equals(currentPage)) { 
						ArrayList<EditionResult> results = new ArrayList<EditionResult>();
						results.add(result);
						ArrayList<String> editionNames = new ArrayList<String>();
						editionNames.add(currentNavigationEdition);
						updateEditionsContent(results , editionNames);
					}
					currentPage = result.getCurrentPage();
					final String wordId = result.getWordId();
					//System.out.println("wordid: "+wordId);

					if (previousElem != null) {
						previousElem.getStyle().clearBackgroundColor();
						previousElem.getStyle().clearPaddingLeft();
						previousElem.getStyle().clearPaddingRight();
						previousElem.getStyle().clearPaddingTop();
						previousElem.getStyle().clearPaddingBottom();
					}
					Timer t = new Timer() {
						public void run() {
							Element elem = getElementByWordID(panel.getDOM(), wordId);
							previousElem = elem;
							//System.out.println("Elem: "+elem);
							if (elem != null && wordId != null) {
								elem.getStyle().setBackgroundColor("#F9A0A0");
								elem.getStyle().setPaddingLeft(3, Unit.PX);
								elem.getStyle().setPaddingRight(3, Unit.PX);
								elem.getStyle().setPaddingTop(1, Unit.PX);
								elem.getStyle().setPaddingBottom(1, Unit.PX);
								elem.scrollIntoView();

							}
						}
					};
					t.schedule(50);
				}
			});
		} else {
			nextMatchButton.setDisabled(false);
			previousMatchButton.setDisabled(false);
		}
	}

	public void changeNavigationTool() {
		
		if (selection.equals(EditionPanel.MOTIF)) {
			selectLayout.setHeight(40);
			selectLayout.setMembers(queryBar);
		} else if (selection.equals(EditionPanel.STRUCTURE)) {
			DynamicForm selectForm = new DynamicForm();
			PickerIcon searchPicker = new PickerIcon(PickerIcon.SEARCH, new FormItemClickHandler() {
				public void onFormItemClick(FormItemIconClickEvent event) {
					SC.say("Search Picker clicked");
				}  
			});  
			TextItem pickerControls = new TextItem("pickerControls", "Structure");
			pickerControls.setIcons(searchPicker);
			selectForm.setFields(pickerControls);
			selectForm.setOverflow(Overflow.AUTO);
			selectLayout.setHeight(30);
			selectLayout.setMembers(selectForm);
		} else if (selection.equals(EditionPanel.PAGE)) {
			selectLayout.setMembers(editionBar);
			selectLayout.setHeight(25);
		}
	}

	private ToolStrip createNavigationEditionBar() {
		
		navEditionBar = new NavigationToolStrip();
		navEditionBar.addExportClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {

			}
		});
		navEditionBar.addPreviousClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {

			}
		});
		navEditionBar.addFirstClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {

			}
		});
		navEditionBar.addLastClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {

			}
		});
		navEditionBar.addNextClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {

			}
		});

		ToolStrip navEditionToolBar = new ToolStrip();
		navEditionToolBar.setWidth100();
		navEditionToolBar.setHeight(25);

		navEditionBar.addToToolstrip(navEditionToolBar);
		return navEditionToolBar;
	}

	String URLParameters = "";
	private String[] wordIds; // stores the ids of the words to highlight
	public void backToText(String path, String textId, final String[] wordIds) {
		
		this.URLParameters = "path="+path+"&textid="+textId+"editions="+selectedEditions+"wordIds="+wordIds;
		//System.out.println("BTT: ids: "+Arrays.toString(wordIds)+" text: "+textId+" path: "+path);
		TXMWEB.log(Services.getI18n().openingEdition());
		ProcessingWindow.show(Services.getI18n().open_biblio_tab());
		ExecTimer.start();
		
		this.wordIds = wordIds;
		this.text = textId;
		
		Services.getEditionService().backToText(path, selectedEditions, textId, wordIds[0], new AsyncCallback<ArrayList<EditionResult>>() {	
			@Override
			public void onSuccess(ArrayList<EditionResult> results) {
				ProcessingWindow.hide();
				if (results == null || results.size() == 0) {
					SC.warn(Services.getI18n().backToTextError());
					return;
				}

				currentResults = results;
				LinkedHashMap<String, String> editionHtmlCodes = new LinkedHashMap<String, String>();
				for (int i = 0 ; i < selectedEditions.size() ; i++) {
					EditionResult result = results.get(i);
					String content = result.getContent();
					String editionName = selectedEditions.get(i);
					editionHtmlCodes.put(editionName, content);
				}
				editionContents.setContents(editionHtmlCodes);

				highlightWords();
				updateBar();
				if (!isVisible()) {
					setShowResizeBar(true);
					show();
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				hide();
				if (caught instanceof PermissionException)
					SC.warn(Services.getI18n().editionPermissionException());
				else
					SC.warn(Services.getI18n().backToTextError());
			}
		});
	}

	/**
	 * highlight&scrollto the words
	 */
	protected void highlightWords() {
		
		if (this.wordIds == null) return;
		if (this.wordIds.length == 0) return;
		
		for (int i = 0 ; i < selectedEditions.size() ; i++) {
			//EditionResult result = results.get(i);
			final String edition = selectedEditions.get(i);
			final TxmHTMLPane editionContent = editionContents.getEditionPanels().get(edition);
			//editionContent.setContents(result.getContent());
			Timer t = new Timer() {
				public void run() {
					boolean first = true;
					for (String wordId : wordIds) {
						Element elem = getElementByWordID(editionContent.getDOM(), wordId);
						//System.out.println("Elem: "+elem);
						if (elem != null && wordId != null) {
							elem.getStyle().setBackgroundColor("#F9A0A0");
							elem.getStyle().setPaddingLeft(3, Unit.PX);
							elem.getStyle().setPaddingRight(3, Unit.PX);
							elem.getStyle().setPaddingTop(1, Unit.PX);
							elem.getStyle().setPaddingBottom(1, Unit.PX);
							if (first) { // scroll only to the first word
								elem.scrollIntoView();
								first = false;
							}
						}
					}
				}
			};
			t.schedule(50);
		}
	}

	public Element getElementByWordID(Element elem, String id) {
		
		NodeList<Node> list = elem.getChildNodes();
		Node tmp;
		for (int i = 0 ; i < list.getLength() ; i++) {
			tmp = list.getItem(i);
			if (tmp.getNodeType() == Node.ELEMENT_NODE) {

				Element tmp2 = (Element)tmp;
				//System.out.println("node : "+tmp2.getInnerHTML());
				if (id.equals(tmp2.getAttribute("id"))) {
					return tmp2;
				} else {
					Element rez = getElementByWordID(tmp2, id);
					if (rez != null) {
						return rez;
					}
				}
			}
		}
		return null;
	}

	public void getEdition(String baseId) {
		
		this.itemPath = baseId;
		if (selectedEditions.size() == 0) {
			TXMWEB.getConsole().addWarning("No edition selected");
			return;
		}
		TXMWEB.log(Services.getI18n().openingEdition());
		ExecTimer.start();
		Services.getEditionService().getEdition(itemPath, this.selectedEditions.get(0), new AsyncCallback<EditionResult>() {
			@Override
			public void onFailure(Throwable caught) {
				if (caught instanceof PermissionException) {
					SC.warn(Services.getI18n().editionPermissionException());
				} else {
					SC.warn(Services.getI18n().backToTextError());
				}
				Tab tab = TXMWEB.getCenterPanel().getSelectedTab();
				TXMWEB.getCenterPanel().removeTab(tab);
				TXMWEB.log("");
			}
			@Override
			public void onSuccess(EditionResult result) {
				TXMWEB.log("");
				//currentResult = result;
				//editionContent.setContents(currentResult.getContent());
				updateBar();
			}
		});
	}

	public void getEditions(String baseId, String text, String pageid) {
		
		try {
			itemPath = "/"+baseId.split("/")[1];
		} catch (Exception e) { itemPath = "/"+baseId; }

		//this.itemPath = baseId;
		//this.selectedEditions = selectedEditions2;

		String[] currentPages = null;
		if (pageid != null && pageid.length() > 0) {
			System.out.println("USING PAGEID: "+pageid);
			currentPages = new String[selectedEditions.size()];
			for (int i = 0 ; i < selectedEditions.size() ; i++) {
				currentPages[i] = pageid;
			}
		} else if (currentResults != null) { // if adding edition facettes
			//System.out.println("Current Pages : "+currentResults);
			currentPages = new String[currentResults.size()];
			for (int i = 0 ; i < currentResults.size() ; i++) {
				currentPages[i] = currentResults.get(i).getCurrentPage();
			}
		}

		TXMWEB.log(Services.getI18n().openingEdition());
		ExecTimer.start();
		Services.getEditionService().getEditions(itemPath, text, selectedEditions, currentPages, new AsyncCallback<ArrayList<EditionResult>>() {
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				if (caught instanceof PermissionException) {
					//SC.warn(Services.getI18n().editionPermissionException());
					TxmDialog.warning(Services.getI18n().editionPermissionException());
					closeParent();
				} else {
					SC.warn(Services.getI18n().backToTextError());
				}
				System.out.println(caught);
			}

			public void onSuccess(ArrayList<EditionResult> results) {

				if (results == null || results.size() == 0) {
					TxmDialog.severe("Error while getting edition content");
					return;
				}
				TXMWEB.log("");
				
				//currentResults = results;
				//editionContent.setContents(currentResult.getContent());
				System.out.println("EditionPanel.onSuccess () - Nb results : "+results.size());
				updateEditionsContent(results, selectedEditions);
				updateBar();
			}
		});
	}

	private void closeParent() {
		if (parentTab != null) {
			TXMWEB.getCenterPanel().removeTab(parentTab);
		}
	}

	private void updateEditionsContent(ArrayList<EditionResult> results, ArrayList<String> selectedEditions2) {
		
		if (results.size() >0 ) {
			this.text = results.get(0).getTextId();
		}
		
		currentResults = results;
		LinkedHashMap<String, String> editionHtmlCodes = new LinkedHashMap<String, String>();
		for (int i = 0 ; i < selectedEditions2.size() ; i++) {
			EditionResult result = results.get(i);
			String content = result.getContent();
			String editionName = selectedEditions2.get(i);
			editionHtmlCodes.put(editionName, content);
		}
		editionContents.setContents(editionHtmlCodes);
		highlightWords();
	}

	private void updateBar() {
		if (currentResults != null && currentResults.size() > 0) {
			String s = "&nbsp;<b>";
			for (EditionResult tmp : currentResults) {
				s += " | " + tmp.getName();
			}
			openedEditionNames.setContents( s + " |</b>");

			EditionResult tmp = currentResults.get(0);
			currentPage = tmp.getCurrentPage();
			paginationWidget.update(currentPage, tmp.getTotalPage());
			if (tmp.getBiblio() != null && tmp.getBiblio().length()  > 0) {
				openbiblioBtn.enable();
			} else {
				openbiblioBtn.disable();
			}

			if (tmp.getPDF() != null && tmp.getPDF().length()  > 0) {
				openPDFBtn.enable();
			} else {
				openPDFBtn.disable();
			}

			if (this.parentTab != null) {
				this.parentTab.setTitle("Text: "+ tmp.getTextId());
			}
		}
	}

	/*public TxmHTMLPane getEditionContent() {
		return editionContent;
	}*/

	/*public void setEditionContent(TxmHTMLPane editionContent) {
		this.editionContent = editionContent;
	}*/

	public void setTab(Tab editionTab) {
		this.parentTab = editionTab;
	}

	public ArrayList<String> getSelectedEditions() {
		return selectedEditions;
	}
}
