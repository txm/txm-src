package org.txm.web.client.widgets;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class EnhancedListGrid extends ListGrid
{
	private JavaScriptObject jsGrid;
	ListGridField rowNum = new ListGridField("no", "   "); 
	boolean showLineNumber = false;
	public EnhancedListGrid(){
		super();
		 
		rowNum.setFrozen(true);
		rowNum.setWidth(30);
		rowNum.setCellFormatter(new CellFormatter() {  
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {  
				return ""+(getRowNumberStart()+rowNum);  
			}  
		}); 
	}
	
	/**
	 * Override to add rownum col if showLineNumber == true
	 */
	public ListGrid setFields(ListGridField... fields)
	{
		if (showLineNumber) {
			ListGridField[] nfields = new ListGridField[fields.length+1];
			nfields[0] = rowNum;
			for (int i = 0 ; i < fields.length ; i++) {
				nfields[i+1] = fields[i];
			}
			super.setFields(nfields);
		}
		else {
			super.setFields(fields);
		}
		return this;
	}
	
	/**
	 * This must be done before setting fields
	 * @param show
	 */
	public void setShowRowNumbers(boolean show)
	{
		showLineNumber = show;
	}
	
	@Override
	protected JavaScriptObject create()
	{
		jsGrid = super.create();
		return jsGrid;
	}

	public void autoSizeColumn(int col)
	{
		jsAutoSizeColumn(jsGrid, col);
	}

	public void disableAllDrag()
	{
		this.setCanDrag(false);
		this.setCanAcceptDroppedRecords(false);
		this.setCanDrop(false);
		this.setCanDropBefore(false);
		this.setCanDragSelect(false);
		this.setCanDragSelectText(false);
		this.setCanDragScroll(false);
	}
	
	/**
	 * super.setCanSelectText don't work, this is a work around
	 * @param val
	 */
	public void setCanSelectText(boolean val)
	{
		super.setCanSelectText(val);
		if (val == true) {
			disableAllDrag();
		}
		setListGridBodyCanSelectText(val);
	}
	
	public native void setListGridBodyCanSelectText(Boolean canSelectText)/*-{
    	var self = this.@com.smartgwt.client.widgets.BaseWidget::getOrCreateJsObj()();
        var body = self.body;
        if (body == null) return;

    	body.canSelectText = canSelectText;
    }-*/;

	private native void jsAutoSizeColumn(JavaScriptObject grid, int col)
	/*-{
            grid.autoSizeColumn(col);
        }-*/;
}