package org.txm.web.client.widgets.tabs;

import java.util.Map;

import org.txm.web.client.widgets.EditionPanel;
import org.txm.web.shared.Keys;

import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.grid.ListGridRecord;


public class EditionTab extends TxmCenterTab {
	
	private EditionPanel content;
	private ListGridRecord elem;
	
	public void setTitle(String title) {
		super.setTitle(Canvas.imgHTML("icons/functions/Edition.png", 16, 16)+"&nbsp;"+"<span style=\"vertical-align:-2px;\">"+title+"</span>");
	}
	
	public EditionTab(ListGridRecord elem, Map<String, String> parameters) {
		
		String path = parameters.get(Keys.PATH);
		String text = parameters.get(Keys.TEXTID);
		String editions = parameters.get(Keys.EDITIONS);
		String pageid = parameters.get(Keys.PAGEID);
		String wordids = parameters.get(Keys.WORDIDS);
		
		this.setName("EDITION");
		this.elem = elem;
		//String baseName = path.split("/")[1].toLowerCase();
		this.setTitle(elem.getAttribute("path2"));
		
		content = new EditionPanel(path, text, editions, pageid, wordids, true);
		content.setTab(this);
		this.setPane(content);
	}
	
	public EditionPanel getEditionPanel() {
		return content;
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub
		
	}
}
