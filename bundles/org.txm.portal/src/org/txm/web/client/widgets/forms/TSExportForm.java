package org.txm.web.client.widgets.forms;

import java.util.List;
import java.util.Map;

import org.txm.web.client.ExecTimer;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.shared.Keys;
import org.txm.web.shared.TsQueryParam;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.MultipleAppearance;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.layout.HLayout;

public class TSExportForm extends TxmWindow {

	protected static boolean firstExportWarning = true;
	SelectItem type = new SelectItem("type", Services.getI18n().type());
	SelectItem NTprops = new SelectItem("ntprops", Services.getI18n().nonTerminalFeatures());// "Non-terminal features"
	SelectItem Tprops = new SelectItem("tprops", Services.getI18n().terminalFeatures()); // "Terminal features"
	SpinnerItem contextSize = new SpinnerItem("context", Services.getI18n().contextSize()); // "Context size (in words)"  
	CheckboxItem addPunct = new CheckboxItem("punct", Services.getI18n().restorePunct()); // "Restore punctuation"

	private IButton validateButton;
	private IButton cancelButton;
	private List<String> ntfeatures;
	private List<String> tfeatures;
	private TsQueryParam tsparams;

	public TSExportForm(TsQueryParam tsparams, List<String> ntFeatures, List<String> tFeatures)
	{
		super(true, "");

		this.tsparams = tsparams;
		this.ntfeatures = ntFeatures;
		this.tfeatures = tFeatures;
		
		initializeFields(null);
		//setParameters(parameters); // do it here because no Tab will do it
	}
	
	@Override
	public void _initializeFields(Map<String, String> parameters) {

		this.setTitle(Services.getI18n().tsExportDialogTitle(tsparams.corpuspath));
		this.setModalMaskOpacity(20);
		this.setShowModalMask(true);
		this.setWidth(300);
		this.setHeight(100);
		this.setAutoHeight();		
		
		form = new DynamicForm();
		form.setPadding(10);
		form.setWidth("100%");
		form.setHeight("100%");
		form.setColWidths("20%", "80%");

		// type.setHint("<nobr>A combobox with styled entries</nobr>");  
		type.setValueMap("Basic concordance",  
				"Single word pivot concordance",
				"Pivot and block concordance");  
		type.setDefaultValue("Basic concordance");
		//		type.addChangedHandler(new ChangedHandler() {
		//			
		//			@Override
		//			public void onChanged(ChangedEvent event) {
		//				String value = type.getValueAsString();
		//				if("Pivot and block concordance".equals(value)) {
		//					NTprops.enable();
		//					Tprops.enable();
		//				} else {
		//					NTprops.disable();
		//					Tprops.disable();
		//				}
		//			}
		//		});

		// remove unused props
		this.tfeatures.remove(Keys.WORD);
		this.ntfeatures.remove("nodom");



		NTprops.setValueMap(ntfeatures.toArray(new String[ntfeatures.size()]));
		NTprops.setMultiple(true);  
		NTprops.setMultipleAppearance(MultipleAppearance.PICKLIST);  

		Tprops.setValueMap((String[]) tfeatures.toArray(new String[tfeatures.size()])); 
		Tprops.setMultiple(true);  
		Tprops.setMultipleAppearance(MultipleAppearance.PICKLIST); 

		contextSize.setDefaultValue(30);  
		contextSize.setMin(0);  
		contextSize.setMax(100);  
		contextSize.setWriteStackedIcons(false);

		//Finalize form
		validateButton = new IButton(Services.getI18n().validateButton());
		cancelButton = new IButton(Services.getI18n().cancelButton());
		HLayout buttons = new HLayout(2);
		buttons.setMargin(7);
		//buttons.setAutoHeight();
		buttons.setWidth100();
		buttons.setAlign(Alignment.CENTER);
		buttons.setMembers(validateButton, cancelButton);

		addValidateHandlers();

		StaticTextItem label = new StaticTextItem("infos");
		label.setValue(Services.getI18n().propertiesToDisplayInConc()); // "Properties to display in concordance"
		label.setShowTitle(false);
		label.setColSpan(2);

		addPunct.setValue(false); // default selected
		addPunct.setDisabled(true); // BUG: This fail right now
		form.setFields(type, contextSize, addPunct, label, NTprops, Tprops);

		this.addItem(form);
		this.addItem(buttons);

		//RootPanel.get().add(this);
		this.centerInPage();
		this.show();

		//		NTprops.disable();
		//		Tprops.disable();
	}

	private void addValidateHandlers() {
		validateButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				validate();
			}
		});
		cancelButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				destroy();
			}
		});
	}

	public void validate() {
		int csize = (Integer) contextSize.getValue();
		String method = type.getValueAsString();
		boolean punct = addPunct.getValueAsBoolean();
		String[] NT = NTprops.getValues();
		String[] T = Tprops.getValues();

		if (!tsparams.query.contains("#pivot")) {
			TxmDialog.warning("Query contains no #pivot variable");
			return;
		}

		if (method.equals("Pivot and block concordance"))
			if (!tsparams.query.contains("#block")) {
				TxmDialog.warning("Query contains no #blockX variable");
				return;
			}

		ProcessingWindow.show(Services.getI18n().exporting());
		ExecTimer.start();
		Services.getTsQueryService().exportForest(tsparams, csize, method, punct, NT, T, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				ProcessingWindow.hide();
				if (result.startsWith("failed")) {
					TxmDialog.severe("Sorry, the TS export failed, please contact admin: "+result);
				} else if (result.equals("wrong method")) {
					TxmDialog.severe("Sorry, the TS export failed, please contact admin: reason wrong method");
				}
				else {
					Window.open(result, "", "");
					TxmDialog.exportWarning();
				}
				TXMWEB.getConsole().addMessage(ExecTimer.stop());
			}

			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				TxmDialog.severe("Export failed: "+caught.getMessage());
			}
		});
	}
}
