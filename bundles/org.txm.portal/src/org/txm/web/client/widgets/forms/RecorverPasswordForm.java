package org.txm.web.client.widgets.forms;

import java.util.Map;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.shared.LoginResult;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.layout.HLayout;

public class RecorverPasswordForm extends TxmWindow {

	private String password = "";
	private String password2 = "";
	private String code;
	
	PasswordItem passwordField = new PasswordItem("Password");
	PasswordItem password2Field = new PasswordItem("Password2");
	IButton validateButton;

	public RecorverPasswordForm(String code) {
		super(true, "");
		
		this.code = code;
		
		initializeFields(null);
		//setParameters(parameters); // do it here because no Tab will do it
	}
	
	@Override
	public void _initializeFields(Map<String, String> parameters) {
		
		this.setTitle(Services.getI18n().passwordRecovery());
		this.setModalMaskOpacity(20);
		this.setShowModalMask(true);
		this.setIsModal(true);
		
		form = new DynamicForm();
		form.setPadding(10);
		form.setWidth("100%");
		form.setHeight("100%");
		form.setColWidths("20%","80%");
		
		validateButton = new IButton(Services.getI18n().validateButton());
		validateButton.setPrompt(Services.getI18n().confirmRecovery());
		IButton cancelButton = new IButton(Services.getI18n().cancelButton());
		cancelButton.setPrompt(Services.getI18n().cancelAndcloseWindow());
		
		passwordField.setTitle(Services.getI18n().password());
		passwordField.setPrompt(Services.getI18n().passwordPrompt());
		password2Field.setTitle(Services.getI18n().confirm_password());
		password2Field.setPrompt(Services.getI18n().confirmPasswordPrompt());

		validateButton.disable();
		validateButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				validate();
			}
		});
		cancelButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				destroy();
				onClose();
			}
		});
		
		KeyPressHandler handler = new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event != null && event.getKeyName() != null) {
					if (event.getKeyName().equals("Enter")) {
						validate();
						onClose();
					}
				}
			}
		};
		passwordField.addKeyPressHandler(handler);
		password2Field.addKeyPressHandler(handler);
		
		HLayout buttons = new HLayout(5);
		buttons.setMargin(7);
		buttons.setAutoHeight();
		buttons.setWidth100();
		buttons.setMembers(validateButton, cancelButton);
		form.setItems(passwordField, password2Field);
		
		this.addItem(form);
		this.addItem(buttons);
		//RootPanel.get().add(this);
		this.centerInPage();
		this.show();
		
		addChangeHandlers();
		passwordField.focusInItem();
	}
	
	private void onClose() {
		TXMWEB.restartWithoutParam("recovery");
	}
	
	 private void addChangeHandlers() {
		 ChangedHandler changelistener = new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				if (password2Field.getValue() != null  &&
						passwordField.getValue() != null)
					validateButton.enable();
				else
					validateButton.disable();
			}
		};
		
		passwordField.addChangedHandler(changelistener);
		password2Field.addChangedHandler(changelistener);
	}

	protected void validate() {
		password2 = form.getValueAsString("Password2");
		password = form.getValueAsString("Password");
		
		if (password == null || password.trim().length() == 0) {
			TxmDialog.warning(Services.getI18n().password_mandatory());
			return;
		}
		
		if (password2 == null || password2.trim().length() == 0) {
			TxmDialog.warning(Services.getI18n().confirm_password());
			return;
		}
		
		password = password.trim();
		password2 = password2.trim();
		
		if (!password.equals(password2)) {
			TxmDialog.warning(Services.getI18n().passwordAndConfirmDiffers());
			return;
		}
		
		Services.getAuthenticationService().recorverPassword(code, password, password2, new AsyncCallback<LoginResult>() {
			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.severe(Services.getI18n().errorPasswordRecovery(caught.toString()));
			}

			@Override
			public void onSuccess(LoginResult result) {
				if (result.isSuccess()) {
					TXMWEB.getSideNavTree().updateTree();
					TXMWEB.getMenuBar().updateMenuBar();
					TXMWEB.getCenterPanel().updateUserPages();
					destroy();
					TxmDialog.info(Services.getI18n().connectedAndPasswordSaved());
				} else {
					String mess = result.getHomePage();
					if ("only guest can recorver password".equals(mess)) {
						TxmDialog.warning(Services.getI18n().internalErrorPleaseRefresh());//"Erreur interne : veuillez rafraîchir la page."
					} else if ("Failed to update account password".equals(mess)) {
						TxmDialog.warning(Services.getI18n().FailToUpdatePassword());
					} else if ("Password and confirm password are differents".equals(mess)) {
						TxmDialog.warning(Services.getI18n().passwordAndConfirmDiffers());
					} else if ("Password is not enough strong".equals(mess)) {
						TxmDialog.warning(Services.getI18n().passwordTooWeak());
					} else if ("Recorvery delay has expired".equals(mess)) {
						TxmDialog.warning(Services.getI18n().recoveryTicketExpired());//"Le ticket de changement de mot de passe a expiré. Veuillez refaire une demande.");
					} else if ("No such recovery code".equals(mess)) {
						TxmDialog.warning(Services.getI18n().NoSuchRecoveryCode());//"Erreur : le code de récupération de mot de passe n'est pas valide.");
					} else {
						TxmDialog.warning(mess);
					}
				}
			}
		});
	}
}
