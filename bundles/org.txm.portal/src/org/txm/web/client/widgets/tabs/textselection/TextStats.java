package org.txm.web.client.widgets.tabs.textselection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.FloatProgressbar;
import org.txm.web.shared.TextSelectionResult;

import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.widgets.Progressbar;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.events.SectionHeaderClickEvent;
import com.smartgwt.client.widgets.layout.events.SectionHeaderClickHandler;

public class TextStats extends VLayout{
	TextTable table;
	private TextSelectionTab textSelectionTab;
	ArrayList<StatPanel> stats = new ArrayList<StatPanel>();
	private boolean countWords = true;

	SectionStack stack = new SectionStack();
	private RadioGroupItem radioGroupItem;

	private static String WORDS = Services.getI18n().words();
	private static String TEXTS = Services.getI18n().texts();

	public TextStats(TextTable table2, TextSelectionTab textSelectionTab, TextSelectionResult result) {
		this.setWidth("10%");

		this.textSelectionTab = textSelectionTab;

		final DynamicForm form = new DynamicForm();  
		form.setAutoWidth();
		radioGroupItem = new RadioGroupItem();  
		radioGroupItem.setTitle(Services.getI18n().countBy());  
		radioGroupItem.setValueMap(WORDS, TEXTS); 
		radioGroupItem.setValue(WORDS);
		radioGroupItem.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				refresh();
			}
		});
		form.setFields(radioGroupItem);

		System.out.println("selectors: "+textSelectionTab.critera.selectors);
		for (Selector selector : textSelectionTab.critera.selectors) {	
			System.out.println("Selector: "+selector);
			if (selector.property != null) { // && selector.records != null) {
				StatPanel stat = new StatPanel(selector);
				stats.add(stat);
			}
		}

		for (StatPanel s : stats) {
			stack.addSection(s); 
		}
		
		

		stack.addSectionHeaderClickHandler(new SectionHeaderClickHandler() {
			@Override
			public void onSectionHeaderClick(SectionHeaderClickEvent event) {
				//System.out.println("LOAD SELECTOR");
				SectionStackSection s = event.getSection();
				StatPanel sel = (StatPanel)s;
				sel.loadData();
				refresh();
			}
		});

		this.setShowResizeBar(true);
		stack.setCanResizeSections(true);
		stack.setVisibilityMode(VisibilityMode.MULTIPLE);
		this.setOverflow(Overflow.AUTO);
		this.setMinMemberSize(150);
		this.setOverflow(Overflow.AUTO);

		this.setMembers(form, stack);
		
		for (int i = 0 ; i < stats.size() ; i++)
			stack.collapseSection(i);
	}

	public class StatPanel extends SectionStackSection
	{
		ArrayList<FloatProgressbar> bars = new ArrayList<FloatProgressbar>();
		String property;
		ListGridRecord[] list;
		private VLayout horizontalBars;
		Selector selector;
		
		public StatPanel(Selector selector) {
			this.selector = selector;
			this.property = selector.property;
			this.list = selector.records;
			horizontalBars = new VLayout();  
			this.addItem(horizontalBars);
			this.setTitle(property);
		}

		public void loadData() {
			if (selector.records == null) {
				selector.loadData();
			}
			this.property = selector.property;
			this.list = selector.records;
			this.setTitle(property+" ("+this.list.length+")");
			for (ListGridRecord value : this.list) {
				FloatProgressbar bar = new FloatProgressbar(); 
				bar.setHeight(16);  
				bar.setVertical(false);  
				bar.setPercentDone(0);
				bar.setShowTitle(true);
				bar.setTitle(value.getAttribute("value"));
				bars.add(bar);
			}

			for (Progressbar bar : bars)
				horizontalBars.addMember(bar);
		}

		public void refresh() {
			if (this.property == null) return;
			HashMap<String, HashMap<String, Integer>> counts = textSelectionTab.numberOfSelectedTextPerProperty;
			if (counts.get(this.property) == null) return;
			
			this.refresh(counts.get(this.property), this.property);
		}
		
		public void refresh(HashMap<String, Integer> counts, String property) {
			int total = 0;
			for (int value : counts.values()) {
				total += value;
			}
			//TODO: GWT warning when trying to remove bars
			for (int i = 0 ; i < bars.size() ; i++) {
				FloatProgressbar bar = bars.get(i);
				String title = bar.getTitle();

				Integer n = counts.get(title);
				bar.setPercentDone(100.0f*((float)n/total));
				if (horizontalBars.hasMember(bar))
					horizontalBars.removeMember(bar);

				bar.setPrompt(n.toString());

			}

			Collections.sort(bars, new Comparator<FloatProgressbar>() {
				@Override
				public int compare(FloatProgressbar o1, FloatProgressbar o2) {
					return o2.getPercentDone() - o1.getPercentDone();
				}
			});

			for (FloatProgressbar bar : bars)
				if (bar.getFloatPercentDone() > 0)// don't show empty bars 
					horizontalBars.addMember(bar);
		}
	}

	public void refresh() {
		//System.out.println("refresh stats panel");
		HashMap<String, HashMap<String, Integer>> counts;
		if(radioGroupItem.getValueAsString() == null || radioGroupItem.getValueAsString().equals(WORDS))
			counts = textSelectionTab.numberOfWordPerSelectedTextPerProperty;
		else
			counts = textSelectionTab.numberOfSelectedTextPerProperty;
		for(StatPanel stat : stats)
			stat.refresh(counts.get(stat.property), stat.property);
	}
}
