package org.txm.web.client.widgets.tabs;

import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.layout.VLayout;


public class ExportTab extends TxmCenterTab {

	private VLayout content = new VLayout();

	public ExportTab(String title, String exportResult) {
		super(Canvas.imgHTML("icons/functions/Export.png", 16, 16)+"&nbsp;"+"<span style=\"vertical-align:-3px;\">"+title+"</span>");
		//this.setTitle();
		this.setName("EXPORT");

		DynamicForm form = new DynamicForm();
		TextAreaItem text = new TextAreaItem();
		content.setSize("100%", "100%");
		form.setSize("100%", "100%");
		text.setHeight("100%");
		text.setWidth("100%");
		text.setShowTitle(false);
		text.setColSpan(2);
		text.setValue(exportResult);
		form.setItems(text);
		content.addMember(form);
		this.setPane(content);
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub
		
	}
}
