package org.txm.web.client.widgets.dialogs;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;

import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.layout.HLayout;

/**
 * Simple model dialog (title, message and image)  with no return.
 * 
 * You can use TxmDialog.info/warning/error(String message)
 * to display a popup.
 * @author mdecorde
 *
 */
public class TxmDialog extends Window {
	static int imgsize = 40;
	private static boolean firstExportWarning = true;
	
	public TxmDialog(String title, String message) {
		this(title, message, null);
	}
	
	public TxmDialog(String title, String message, String imgfilename) {
		super();
		this.setWidth(360);
		this.setHeight(120);
		this.setTitle(title);
		this.setShowMinimizeButton(false);
		this.setIsModal(true);
		this.setShowModalMask(true);
		this.centerInPage();

		HLayout layout = new HLayout();  
		layout.setWidth100();  
		layout.setHeight100();  
		layout.setMembersMargin(10);
		layout.setLayoutMargin(10);

		if (imgfilename != null && !imgfilename.equals("")) {
			Img img = new Img(imgfilename, imgsize, imgsize);
			img.setAppImgDir("icons/");
			layout.addMember(img);
		}
		
		Label text = new Label(message);
		text.setHeight100();
		text.setWidth100();
		text.setCanSelectText(true);

		layout.addMember(text);
		this.addItem(layout);
	}
	
	public static void show(String title, String message) {
		TxmDialog dial = new TxmDialog(title, message,null);
		TXMWEB.getConsole().addMessage(message);
		dial.show();
	}
	
	public static void show(String message) {
		TxmDialog dial = new TxmDialog("", message,null);
		TXMWEB.getConsole().addMessage(message);
		dial.show();
	}
	

	public static void info(String title, int width, int height, String message) {
		TxmDialog dial = new TxmDialog(title, message,"info.png");
		dial.setHeight(height);
		dial.setWidth(width);
		TXMWEB.getConsole().addMessage(message);
		dial.show();
	}
	
	public static void info(String title, String message) {
		TxmDialog dial = new TxmDialog(title, message,"info.png");
		TXMWEB.getConsole().addMessage(message);
		dial.show();
	}
	
	public static void info(String message) {
		TxmDialog dial = new TxmDialog(Services.getI18n().info(), message,"info.png");
		TXMWEB.getConsole().addMessage(message);
		dial.show();
	}
	
	public static void info(String message, int width, int height) {
		TxmDialog dial = new TxmDialog(Services.getI18n().info(), message,"info.png");
		dial.setWidth(width);
		dial.setHeight(height);
		TXMWEB.getConsole().addMessage(message);
		dial.show();
	}
	
	public static void severe(String title, String message) {
		TxmDialog dial = new TxmDialog(title, message,"error.png");
		TXMWEB.getConsole().addError(message);
		dial.show();
	}
	
	public static void severe(String message) {
		TxmDialog dial = new TxmDialog(Services.getI18n().error(), message,"error.png");
		TXMWEB.getConsole().addError(message);
		dial.show();
	}
	
	public static void warning(String title, String message) {
		TxmDialog dial = new TxmDialog(title, message,"warning.png");
		TXMWEB.getConsole().addWarning(message);
		dial.show();
	}
	
	public static void warning(String message) {
		TxmDialog dial = new TxmDialog(Services.getI18n().warning(), message,"warning.png");
		TXMWEB.getConsole().addWarning(message);
		dial.show();
	}

	public static void exportWarning() {
		if (TxmDialog.firstExportWarning ) {
			TxmDialog.info(Services.getI18n().exportWarning(TXMWEB.BASEURL), 400, 200);
			TxmDialog.firstExportWarning = false;
		}
	}
}
