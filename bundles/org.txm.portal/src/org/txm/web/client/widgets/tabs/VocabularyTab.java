package org.txm.web.client.widgets.tabs;

import java.util.ArrayList;

import org.txm.web.shared.VocabularyParam;

import com.google.gwt.user.client.ui.FlexTable;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.VLayout;

public class VocabularyTab extends TxmCenterTab {
	public VocabularyTab(VocabularyParam params, ArrayList<String[]> result) {
		
		this.setName("VOCABULARY");
		this.setTitle("voc : " + params.getQuery()); 
		
		VLayout content = new VLayout();
		final FlexTable table = new FlexTable();
		table.setSize("100%","100%");
		int n = 1;
		Label l = new Label("SIGNATURE");
		table.setWidget(0,0,l);
		l = new Label("FREQUENCY");
		table.setWidget(0,1,l);
		
		for (String[] line : result) {
			for (int i = 0 ; i < line.length ; i ++) {
				table.setText(n, i, line[i]);
			}
			n++;
		}
		content.addMember(table);
		this.setPane(content);
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub
		
	}
}
