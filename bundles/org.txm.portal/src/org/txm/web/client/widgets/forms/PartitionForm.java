package org.txm.web.client.widgets.forms;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.txm.web.client.ExecTimer;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.shared.Keys;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.tree.Tree;
import com.smartgwt.client.widgets.tree.TreeNode;

public class PartitionForm extends TxmWindow {

//	private String name = "";
//	private String query = "";
	private String itemPath = "";
	ListGridRecord theRecord;

	public PartitionForm(Map<String, String> parameters, ListGridRecord theRecord) {
		super(true, "");
		
		this.itemPath = parameters.get(Keys.PATH); 
		this.theRecord = theRecord;
		
		initializeFields(parameters);
		setParameters(parameters); // do it here because no Tab will do it
	}
	
	@Override
	public void _initializeFields(Map<String, String> parameters) {
		
		this.form = new DynamicForm();
		this.setTitle(Services.getI18n().createPartition());

		TextItem nameField = new TextItem(Keys.NAME, Services.getI18n().partitionFormNameField()); // Nom de la partition à créer
		nameField.setPrompt(Services.getI18n().newNamePrompt());
		IButton validationButton = new IButton(Services.getI18n().createButton());
		validationButton.setPrompt(Services.getI18n().confirmPartition());
		IButton cancelButton = new IButton(Services.getI18n().cancelButton());
		cancelButton.setPrompt(Services.getI18n().cancelAndcloseWindow());

		final SelectItem structSelection = new SelectStructureItem(Keys.STRUCTURE, Services.getI18n().structure()); // Structure de sélection des parties. Valeur par défaut : Keys.TEXT
		structSelection.setPrompt(Services.getI18n().structurePrompt());
		
		final SelectItem propertySelection = new SelectPropertyItem(Keys.PROPERTY, Services.getI18n().property()); // Propriété de la structure de sélection des parties. Valeur par défaut : Keys.ID
		propertySelection.setPrompt(Services.getI18n().propertyPrompt());
		
		structSelection.setShowAllOptions(true);
		Services.getCorpusActionService().getStructuralUnits(itemPath, new AsyncCallback<List<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.warning(Services.getI18n().internalError());
			}
			@Override
			public void onSuccess(List<String> result) {
				Collections.sort(result);
				structSelection.setValueMap(result.toArray(new String[result.size()]));
			}
		});
		
		propertySelection.setShowAllOptions(true);
		structSelection.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				Services.getCorpusActionService().getStructuralUnitProperties(itemPath, (String) event.getValue(), new AsyncCallback<List<String>>() {
					@Override
					public void onFailure(Throwable caught) {
						TxmDialog.warning(Services.getI18n().internalError());
					}
					@Override
					public void onSuccess(List<String> result) {
						Collections.sort(result);
						propertySelection.setValueMap(result.toArray(new String[result.size()]));
					}
				});
			}
		});
		
		form.setHeight100();
		form.setWidth100();

		cancelButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				destroy();
			}
		});
		validationButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				validate();
			}
		});
		
		HLayout buttons = new HLayout(5);
		buttons.setMargin(7);
		buttons.setAutoHeight();
		buttons.setWidth100();
		buttons.setMembers(validationButton, cancelButton);

		form.setItems(nameField, structSelection, propertySelection);
		this.addItem(form);
		this.addItem(buttons);
		//RootPanel.get().add(this);
		this.centerInPage();
		this.show();
		
		nameField.focusInItem();
	}

	private void validate() {
		final String name = form.getValueAsString(Keys.NAME);
		String structName = form.getValueAsString(Keys.STRUCTURE);
		String structProp = form.getValueAsString(Keys.PROPERTY);
		
		if (name == null)
		{
			TxmDialog.warning(Services.getI18n().name_mandatory());
			return;
		}
		
		if (name.contains("/") || name.contains(":"))
		{
			TxmDialog.warning(Services.getI18n().errorNameChar());
			return;
		}
		ProcessingWindow.show(Services.getI18n().creatingPartition());
		Services.getCorpusActionService().createPartition(itemPath, name, structName, structProp, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				ProcessingWindow.hide();
				if (result == null) {
					TxmDialog.severe("Error: null partition result.");
				} else if ("user not saved".equals(result)) {
					TxmDialog.severe("Error: could not save user data. Please contact admin.");
				} else if ("workspace not saved".equals(result)) {
					TxmDialog.severe("Error: could not save workspace data. Please contact admin.");
				} else if(!result.endsWith(":"+name)) {
					if (result.equals("exist"))
						TxmDialog.severe(Services.getI18n().partitionExists());
					else
						TxmDialog.warning(Services.getI18n().errorWith(result));
				} else {
					String ns = result.substring(0, result.indexOf(":"));
					Tree tree = TXMWEB.getSideNavTree().getTree();
					TreeNode newChild;
					if (ns != null && ns.length() > 0)
						newChild = new TreeNode(ns+":"+name);
					else
						newChild = new TreeNode(name);
					String path = theRecord.getAttribute("path")+"/"+ns+":"+name;
					String path2 = name;
					newChild.setAttribute("path", path);
					newChild.setAttribute("path2", path2);
					newChild.setAttribute("type", "partition");
					newChild.setAttribute("ns", ns);
					newChild.setAttribute("id", name);

					newChild.setIcon("icons/partition.png");
					
					tree.add(newChild, itemPath); // append child to parent
					tree.openFolder(tree.find(itemPath)); // open parent Tree
					TXMWEB.getSideNavTree().refreshFields();
					destroy();
					TXMWEB.getConsole().addMessage(Services.getI18n().partition_created()+ExecTimer.stop());
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				TxmDialog.info(Services.getI18n().sideNavTreeCreatePartitionError());
			}
		});
	}
}
