package org.txm.web.client.widgets.forms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.txm.web.client.ExecTimer;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.QueryAssistantDialog;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.tabs.ReferencerTab;
import org.txm.web.exceptions.PermissionException;
import org.txm.web.shared.Keys;
import org.txm.web.shared.ReferencerParam;
import org.txm.web.shared.ReferencerResult;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.MultipleAppearance;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.layout.HLayout;

public class ReferenceForm extends TxmForm {

	protected ReferencerTab parentTab = null;

	protected ReferencerParam params = new ReferencerParam();
	protected DynamicForm queryForm = new DynamicForm();

	TextItem queryField;
	OrderedPickListItem propertiesField;

	protected SpinnerItem nbperpageField;

	private SelectItem sortField;

	private SelectItem propertyField;

	private String itemPath;

	public ReferenceForm(Map<String, String> parameters, ReferencerTab referencerTab) {
		super();
		
	
		params.setCorpusPath(itemPath);
		this.parentTab = referencerTab;
		this.itemPath = parameters.get(Keys.PATH);
		
		initializeFields(parameters);
	}
	
	@Override
	public void _initializeFields(final Map<String, String> parameters) {
		
		this.setWidth100();
		
		this.form = new DynamicForm();

		HLayout section1 = new HLayout();
		final HLayout section2 = new HLayout();
		section1.setHeight(35);
		section1.setWidth100();
		section1.setAlign(Alignment.CENTER);
		section1.setAlign(VerticalAlignment.CENTER);
		section1.setMargin(3);
		section2.hide();
		section2.setBackgroundColor("#DDDDDD");

		IButton assistantButton = new IButton("");
		assistantButton.setShowTitle(false);
		assistantButton.setWidth(24);
		assistantButton.setIcon("icons/functions/Queryassist.png");
		assistantButton.setPrompt(Services.getI18n().openTheQueryAssistant());
		assistantButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				QueryAssistantDialog assistantDialog = new QueryAssistantDialog(params.getCorpusPath());
				assistantDialog.show(queryForm, Keys.QUERY);
			}
		});
		
		queryField = new TextItem(Keys.QUERY, Services.getI18n().queryField()); // sélectionne les mots dont on cherche les références
		queryField.setPrompt(Services.getI18n().queryPrompt());
		queryField.setWidth("100%");
		propertyField = new SelectPropertyItem(Keys.PROPERTY, Services.getI18n().property()); // propriété de mot du pivot à afficher. Valeur par défaut : Keys.WORD
		propertyField.setPrompt(Services.getI18n().propertyPrompt());
		propertyField.setDefaultValue(Keys.WORD);
		
		IButton validationButton = new IButton(Services.getI18n().searchButton());
		IButton settingsButton = new IButton(Services.getI18n().settingsButton());

		propertiesField = new OrderedPickListItem(Keys.PROPERTIES); // propriétés de mot ou de structure à afficher comme référence
		propertiesField.setPrompt(Services.getI18n().referenceViewPrompt());
		propertiesField.setTitle(Services.getI18n().indexFormProperties());
		propertiesField.setMultiple(true);
		//propertiesField.setMultipleAppearance(MultipleAppearance.PICKLIST);
		//propertiesField.setShowAllOptions(true);

		validationButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				validate();
			}
		});

		queryField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event != null && event.getKeyName() != null) {
					if (event.getKeyName().equals("Enter")) {
						validate();
					}
				}
			}
		});

		settingsButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (section2.isVisible())
					section2.hide();
				else
					section2.show();
			}
		});

		queryForm.setWidth100();
		queryForm.setNumCols(2);
		queryForm.setColWidths("30", "*");
		queryForm.setItems(queryField);
		form.setWidth100();
		form.setItems(propertyField, propertiesField);
		form.setTitleOrientation(TitleOrientation.TOP); 
		form.setNumCols(8);

		section1.setMembers(assistantButton, queryForm, validationButton, settingsButton);
		section2.setMembers(form);
		this.setMembers(section1, section2);
		
		Services.getCorpusActionService().getWordProperties(itemPath, new AsyncCallback<List<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.severe(Services.getI18n().failed_get_structuralunits(itemPath, caught.getMessage()));
			}
			@Override
			public void onSuccess(List<String> result) {
				Collections.sort(result);
				propertyField.setValueMap(result.toArray(new String[result.size()]));
				propertyField.setDefaultValue(Keys.WORD);
				
				Services.getCorpusActionService().getStructuralUnitsProperties(itemPath, new AsyncCallback<List<String>>() {
					@Override
					public void onSuccess(List<String> result) {
						Collections.sort(result);
						propertiesField.setValueMap(result.toArray(new String[]{}));
						propertiesField.setDefaultValue("text_id");
						
						ReferenceForm.this.parentTab.setParameters(parameters);
						
						if (parameters.get(Keys.QUERY) != null && parameters.get(Keys.QUERY).length() > 0) {
							validate();
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						TxmDialog.severe(Services.getI18n().internalError());
					}
				});
			}
		});
		
		
		
	}

	public void setParameters(Map<String, String> parameters) {
		for (String property : parameters.keySet()) {
			if (Keys.QUERY.equals(property)) {
				queryForm.getItem(property).setValue(parameters.get(property));
			} else {
				if (form.getItem(property) != null) {
					form.getItem(property).setValue(parameters.get(property));
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void validate() {

		ProcessingWindow.show(Services.getI18n().computing_references(params.getQuery()));
		params.setQuery(queryForm.getValueAsString(Keys.QUERY));
		params.prop = form.getValueAsString(Keys.PROPERTY);
		try {
			params.filterNbPerPage = this.parentTab.getPageSize();
		} catch(Exception e) {
			ProcessingWindow.hide();
			TxmDialog.warning(Services.getI18n().FMinFMaxVMaxNPageError());
		}
		params.startindex = parentTab.startindex;

		//System.out.println("VALUE: "+form.getItem(Keys.PROPERTIES).getValue().getClass());
		Object value = form.getItem(Keys.PROPERTIES).getValue();
		ArrayList<String> properties;
		if (value == null) {
			properties = new ArrayList<String>();
			properties.add("text_id");
		} else if (value instanceof ArrayList){
			properties = (ArrayList<String>) value ;
		} else {
			TxmDialog.severe("Error: could not read properties");
			return;
		}
		params.setProperties(properties);
//		if (properties != null && properties.size() > 0) {
//			/*properties = properties.substring(1, properties.length() - 1); // remove [ and ]
//			for (String s : properties.split(",")) {
//				params.getProperties().add(s);
//			}*/
//			params.setProperties(properties);
//		} else {
//			params.setProperties(new ArrayList<String>());
//			params.getProperties().add(Keys.WORD);
//		}

		if (params.getQuery() == null || params.getQuery().trim().length() == 0) {
			TxmDialog.warning(Services.getI18n().query_mandatory());
			ProcessingWindow.hide();
			return;
		}
		if (params.getQuery().matches(".*%(d|cd|dc).*")) {
			TxmDialog.warning(Services.getI18n().queryDError());
			ProcessingWindow.hide();
			return;
		}
		System.out.println("PARAMS: "+params);
		ExecTimer.start();
		Services.getReferencesService().references(params, new AsyncCallback<ReferencerResult>() {
			@Override
			public void onSuccess(ReferencerResult result) {
				ProcessingWindow.hide();
				if (result == null || result.error == null || result.error.length() > 0) {
					String error = result.error;
					if (error.equals("permission")) {
						TxmDialog.warning(Services.getI18n().noPermission());
					} else if (error.equals("session")) {
						TxmDialog.warning(Services.getI18n().sessionExpired());
					} else {
						TxmDialog.warning(Services.getI18n().querySyntaxError(error));
					}
				}
				else {
					parentTab.fillList(result);
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				if (caught instanceof PermissionException)
					TxmDialog.warning(Services.getI18n().indexPermissionException());
				else
					TxmDialog.warning(Services.getI18n().indexError());
			}
		});
	}	

	public ReferencerParam getParams() {
		return params;
	}

	public void setParams(ReferencerParam params) {
		this.params = params;
	}
}
