package org.txm.web.client.widgets;

import com.smartgwt.client.widgets.Progressbar;

public class FloatProgressbar extends Progressbar{
	float f = 0f;
	
	public void setPercentDone(float v)
	{
		f = v;
		super.setPercentDone((int)v);
	}
	
	public float getFloatPercentDone()
	{
		return f;
	}
}
