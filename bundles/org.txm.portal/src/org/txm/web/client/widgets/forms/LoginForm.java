package org.txm.web.client.widgets.forms;

import java.util.Map;

import org.txm.web.client.Commands;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.CenterPanel;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.shared.Keys;
import org.txm.web.shared.LoginResult;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.layout.HLayout;

public class LoginForm extends TxmWindow {
	private String login = "";
	private String password = "";

	PasswordItem passwordField = new PasswordItem("Password");
	TextItem loginField = new TextItem(Keys.LOGIN); // identifiant de connexion
	IButton validateButton = new IButton(Services.getI18n().loginButton());
	LinkItem forgetLink = new LinkItem("forgot");

	public LoginForm(Map<String, String> parameters) {
		super(true, "");
		
		initializeFields(parameters);
		setParameters(parameters); // do it here because no Tab will do it
	}
	
	@Override
	public void _initializeFields(Map<String, String> parameters) {
		
		this.setAutoWidth();
		
		form = new DynamicForm();
		form.setPadding(10);
		form.setWidth("100%");
		form.setHeight("100%");
		form.setColWidths("20%","80%");

		IButton cancelButton = new IButton(Services.getI18n().cancelButton());
		cancelButton.setPrompt(Services.getI18n().cancelAndcloseWindow());
		this.setTitle(Services.getI18n().loginDialog(TXMWEB.PORTALSHORTNAME));
		this.setModalMaskOpacity(20);
		this.setShowModalMask(true);

		passwordField.setTitle(Services.getI18n().password());
		passwordField.setPrompt(Services.getI18n().passwordPrompt());
		loginField.setTitle(Services.getI18n().login());
		loginField.setPrompt(Services.getI18n().loginPrompt());
		forgetLink.setValue(Services.getI18n().forgotten());
		forgetLink.setShowTitle(false);
		forgetLink.setColSpan("2");
		validateButton.disable();
		validateButton.setPrompt(Services.getI18n().confirmConnexion());
		validateButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				validate();
			}
		});
		cancelButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				destroy();
			}
		});
		loginField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event != null && event.getKeyName() != null) {
					if (event.getKeyName().equals("Enter")) {
						validate();
					}
				}
			}
		});
		passwordField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event != null && event.getKeyName() != null) {
					if (event.getKeyName().equals("Enter")) {
						validate();
					}
				}
			}
		});
		forgetLink.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(
					com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				destroy();
				SC.askforValue(Services.getI18n().askEmail(), new ValueCallback() {

					@Override
					public void execute(final String mail) {
						System.out.println("recorver: mail: "+mail);
						if (mail == null || mail.trim().length() == 0)
							return;
						
						Services.getAuthenticationService().forgotPassword(mail, new AsyncCallback<Boolean>() {

							@Override
							public void onFailure(Throwable caught) {
								TxmDialog.severe(Services.getI18n().recoveryError(mail));
							}

							@Override
							public void onSuccess(Boolean result) {
								if (result)
									TxmDialog.info(Services.getI18n().mail_sent_to(mail, TXMWEB.PORTALSHORTNAME));
								else
									TxmDialog.warning(Services.getI18n().no_account_mail(mail, TXMWEB.PORTALSHORTNAME));
							}
						});
					}
				});
			}
		});

		HLayout buttons = new HLayout(5);
		buttons.setMargin(7);
		buttons.setAutoHeight();
		buttons.setWidth100();
		buttons.setMembers(validateButton, cancelButton);
		form.setItems(loginField, passwordField);
		if(TXMWEB.MAILENABLE)
			form.setItems(loginField, passwordField, forgetLink);
		else
			form.setItems(loginField, passwordField);

		this.addItem(form);
		this.addItem(buttons);
		
		//RootPanel.get().add(this);
		this.centerInPage();
		this.show();
		addChangeHandlers();
		loginField.focusInItem();
	}

	public LoginForm(String login) {
		this(Commands.createParameters(Keys.LOGIN, login));
	}

	private void addChangeHandlers() {
		ChangedHandler changelistener = new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				if (loginField.getValue() != null  &&
						passwordField.getValue() != null)
					validateButton.enable();
				else
					validateButton.disable();
			}
		};

		loginField.addChangedHandler(changelistener);
		passwordField.addChangedHandler(changelistener);
	}

	protected void validate() {
		login = form.getValueAsString(Keys.LOGIN);
		password = form.getValueAsString("Password");

		if (password == null || password.trim().length() == 0) {
			TxmDialog.warning(Services.getI18n().password_mandatory());
			return;
		}

		if (login == null || login.trim().length() == 0) {
			TxmDialog.warning(Services.getI18n().login_mandatory());
			return;
		}
		
		// trim values
		password = password.trim();
		login = login.trim();

		ProcessingWindow.show(Services.getI18n().login_in());
		Services.getAuthenticationService().login(login, password, new AsyncCallback<LoginResult>() {
			@Override
			public void onSuccess(LoginResult result) {
				ProcessingWindow.hide();
				if (!result.isSuccess()) {
					String code = result.getHomePage();
					if(code.equals("Wrong login or password"))
						TxmDialog.severe(Services.getI18n().wrongLoginPassword());
					else if(code.equals("toBeActivated"))
						TxmDialog.severe(Services.getI18n().notConfirmedYet());
					else
						TxmDialog.severe(result.getHomePage());
					TXMWEB.LOGGED = false;
				} else {
					TXMWEB.getSideNavTree().updateTree();
					TXMWEB.getMenuBar().updateMenuBar();
					TXMWEB.getCenterPanel().updateUserPages();
					TXMWEB.LOGGED = true;
					CenterPanel center = TXMWEB.getCenterPanel();
					center.updateUserPages();
					destroy();
					TXMWEB.getConsole().addMessage(Services.getI18n().connected());
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				TxmDialog.warning(Services.getI18n().loginError());
			}
		});
	}
}
