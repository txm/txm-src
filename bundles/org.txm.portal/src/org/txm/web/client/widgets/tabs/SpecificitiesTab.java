package org.txm.web.client.widgets.tabs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.EnhancedListGrid;
import org.txm.web.client.widgets.forms.SpecificityForm;
import org.txm.web.shared.Keys;
import org.txm.web.shared.SpecificitiesParam;
import org.txm.web.shared.SpecificitiesResultLine;

import com.smartgwt.client.types.GroupStartOpen;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.grid.GroupNode;
import com.smartgwt.client.widgets.grid.GroupTitleRenderer;
import com.smartgwt.client.widgets.grid.GroupValueFunction;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;

public class SpecificitiesTab extends TxmCenterTab implements SpecificitiesPresentation {

	public static final String WORD_FIELD = Keys.WORD;
	public static final String FREQ_FIELD = Keys.FREQUENCE;
	public static final String COFREQ_FIELD = "cofreq";
	public static final String SPECIF_FIELD = Keys.SPECIFICITY;
	public static final String INDEX_FIELD = "index";

	private SpecificityForm form = null;

	private VLayout specificitiesTabContent = new VLayout();
	private HLayout resultLayout;
	private ArrayList<EnhancedListGrid> resultLists;
	private ArrayList<SectionStack> sectionStacks;
	//private boolean isPartition;

	public void setTitle(String title) {
		
		super.setTitle(Canvas.imgHTML("icons/functions/Specificities.png", 16, 16)+"&nbsp;"+"<span style=\"vertical-align:-2px;\">"+title+"</span>");
	}

	public SpecificitiesTab(Map<String, String> parameters) {

		this.setName("SPECIFICITIES");
		setTitle(Services.getI18n().specificities_title(parameters.get(Keys.PATH)));
		this.form = new SpecificityForm(parameters, this);
		
		resultLists = new ArrayList<EnhancedListGrid>();
		sectionStacks = new ArrayList<SectionStack>();
		Canvas tablesCanvas = new Canvas();
		tablesCanvas.setWidth100();
		tablesCanvas.setHeight100();
		resultLayout = new HLayout();
		resultLayout.setWidth100();
		resultLayout.setHeight100();
		resultLayout.setMembersMargin(5);
		resultLayout.setAlign(VerticalAlignment.TOP);
		resultLayout.setOverflow(Overflow.CLIP_V);
		tablesCanvas.addChild(resultLayout);
		specificitiesTabContent.setMembers(form, tablesCanvas);
		this.setPane(specificitiesTabContent);
		
	}

	public SpecificityForm getForm() {
		return this.form;
	}
	
	public SpecificitiesTab(String title, String img) {
		
		super(title, img);
	}

	/**
	 * Displays the tab into the center panel.
	 */
	public void addInView() {
		
		addInView(null, null, null);
	}


	public void fillList(Map<String, ArrayList<SpecificitiesResultLine>> results, String mode, Number nbOrThreshold) {

		for (SectionStack ss : sectionStacks) {
			resultLayout.removeMember(ss);
		}
		
		ArrayList<String> keys = new ArrayList<String>(results.keySet());
		Collections.sort(keys);
		for (String s : keys) {
			
			ArrayList<SpecificitiesResultLine> specifitiesResults = getResultsToDisplay(results.get(s), mode, nbOrThreshold);

			EnhancedListGrid resultList = createTable(mode, nbOrThreshold);
			SectionStackSection section = new SectionStackSection(s);
			SectionStack sectionStack = new SectionStack();
			sectionStack.setAlign(VerticalAlignment.TOP);
			sectionStack.setWidth(300);
			sectionStack.setHeight100();
			sectionStack.setSections(section);
			sectionStack.setCanResizeSections(true);
			section.setCanCollapse(false);
			section.setItems(resultList);
			resultLayout.addMember(sectionStack);
			sectionStacks.add(sectionStack);
			resultList.setShowEmptyMessage(true);

			int nWord = 0;
			for (SpecificitiesResultLine entry : specifitiesResults) {
				ListGridRecord record = new ListGridRecord();
				record.setAttribute(WORD_FIELD, entry.getWord());
				record.setAttribute(FREQ_FIELD, entry.getFrequency());
				record.setAttribute(COFREQ_FIELD, entry.getCofrequency());
				record.setAttribute(SPECIF_FIELD, entry.getSpecificity());
				record.setAttribute(INDEX_FIELD, nWord);
				resultList.addData(record);
				nWord++;
			}

			//			if(!isPartition) // show only one is item is a subcorpus
			//			{
			//				sectionStack.setWidth100();
			//				break;
			//			}
		}
		hideField(INDEX_FIELD);
	}

	private ArrayList<SpecificitiesResultLine> getResultsToDisplay(ArrayList<SpecificitiesResultLine> fullList, String mode, Number nb) {

		ArrayList<SpecificitiesResultLine> resultsToDisplay = new ArrayList<SpecificitiesResultLine>();
		Collections.sort(fullList);
		Collections.reverse(fullList);

		resultsToDisplay.addAll(fullList);
		// preserve results according to threshold or nbWords
		if (mode.equals(SpecificitiesParam.TOPN)) {
			int nbWords = (Integer)nb;
			if (fullList.size() > nbWords * 2) {
				for (int n = resultsToDisplay.size() - 1 - nbWords; n >= nbWords; n--) {
					resultsToDisplay.remove(n);
				}
			}
		} else if (mode.equals(SpecificitiesParam.SMIN)) {
			double threshold = (Double)nb;
			if (threshold != 0) {
				for (int s = resultsToDisplay.size() - 1; s >= 0; s--) {
					SpecificitiesResultLine sp = resultsToDisplay.get(s);
					if (sp.getSpecificity() < threshold
							&& sp.getSpecificity() > (threshold * -1)) {
						resultsToDisplay.remove(s);
					}
				}

			}
		}
		return resultsToDisplay;
	}

	private EnhancedListGrid createTable(String mode, Number nbOrThreshold) {

		EnhancedListGrid resultList = new EnhancedListGrid();
		ListGridField wordField = new ListGridField(WORD_FIELD, Services.getI18n().wordRadio());
		ListGridField frequencyField = new ListGridField(FREQ_FIELD, Services.getI18n().frequencyField());
		ListGridField cofrequencyField = new ListGridField(COFREQ_FIELD, Services.getI18n().coFrequencyField());
		ListGridField specifField = new ListGridField(SPECIF_FIELD, Services.getI18n().score());
		specifField.setFormat("0.0");
		ListGridField indexField = new ListGridField(INDEX_FIELD, "index");
		
		// will not be visible (only for grouping)
		resultList.setFields(wordField, frequencyField, cofrequencyField, specifField, indexField);
		resultList.setShowRowNumbers(true);
		resultList.setSize("300px", "50%");
		resultList.setCanResizeFields(true);
		resultList.setCanSelectText(true);
		resultList.setCanFreezeFields(false);
		resultList.setCanGroupBy(false);
		resultList.setCanAutoFitFields(false);
		resultList.setCanPickFields(false);
		resultList.setShowAllRecords(true);
		
		if (mode != SpecificitiesParam.ALL) {
			resultList.setGroupStartOpen(GroupStartOpen.ALL);
			//resultList.setAutoFetchData(true);
			if (mode.equals(SpecificitiesParam.SMIN)) {
				groupBySpecif(resultList, specifField, (Double) nbOrThreshold);
			}
			else {
				groupByIndex(resultList, indexField, (Integer) nbOrThreshold);
			}
		}
		resultLists.add(resultList);
		return resultList;

	}

	private void groupByIndex(EnhancedListGrid resultList, ListGridField indexField, final int limit) {
		
		final int groupSmall = 1;
		final int groupLarge = 2;
		indexField.setType(ListGridFieldType.INTEGER);
		indexField.setGroupValueFunction(new GroupValueFunction() {
			public Object getGroupValue(Object value, ListGridRecord record,
					ListGridField field, String fieldName, ListGrid grid) {

				Number index = (Number) value;
				if (index.doubleValue() < limit) {
					return groupSmall;
				} else
					return groupLarge;
			}
		});

		indexField.setGroupTitleRenderer(new GroupTitleRenderer() {
			public String getGroupTitle(Object groupValue, GroupNode groupNode, ListGridField field, String fieldName, ListGrid grid) {
				
				final int groupType = (Integer) groupValue;
				String baseTitle = "";

				switch (groupType) {
				case groupSmall:
					baseTitle = Services.getI18n().positiveSpecificities();
					break;
				case groupLarge:
					baseTitle = Services.getI18n().negativeSpecificities();
					break;
				}
				baseTitle += " (" + groupNode.getGroupMembers().length +")";
				return baseTitle;
			}
		});
		resultList.setGroupByField(INDEX_FIELD);

	}

	private void groupBySpecif(EnhancedListGrid resultList, ListGridField specifField, final double threshold) {

		final int groupSmall = 1;
		final int groupLarge = 2;

		specifField.setType(ListGridFieldType.FLOAT);
		specifField.setGroupValueFunction(new GroupValueFunction() {
			public Object getGroupValue(Object value, ListGridRecord record,
					ListGridField field, String fieldName, ListGrid grid) {

				Number specif = (Number) value;
				if (specif.doubleValue() < threshold) {
					return groupSmall;
				} else
					return groupLarge;
			}
		});

		specifField.setGroupTitleRenderer(new GroupTitleRenderer() {
			public String getGroupTitle(Object groupValue, GroupNode groupNode,
					ListGridField field, String fieldName, ListGrid grid) {
				final int groupType = (Integer) groupValue;
				String baseTitle = "";

				switch (groupType) {
				case groupSmall:
					baseTitle = Services.getI18n().specificity() +" < " + threshold;
					break;
				case groupLarge:
					baseTitle = Services.getI18n().specificity() +" >= " + threshold;
					break;
				}
				baseTitle += " (" + groupNode.getGroupMembers().length +")";
				return baseTitle;
			}
		});
		resultList.setGroupByField(SPECIF_FIELD);
	}

	public void hideField(String colName) {
		
		for (EnhancedListGrid g : resultLists) {
			g.hideField(colName);
			g.refreshFields();
		}
	}

	public void showField(String colName) {
		
		for (EnhancedListGrid g : resultLists) {
			g.showField(colName);
			g.refreshFields();
		}
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean isFilled() {
		
		return resultLayout.getChildren().length > 0; // resultLayout is not empty when filled
	}
}
