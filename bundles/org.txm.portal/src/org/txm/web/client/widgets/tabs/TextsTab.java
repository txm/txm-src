package org.txm.web.client.widgets.tabs;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.txm.web.client.Commands;
import org.txm.web.client.ExecTimer;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.EnhancedListGrid;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.TxmHTMLPane;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.tabs.textselection.Selector;
import org.txm.web.shared.Keys;
import org.txm.web.shared.MetadataInfos;
import org.txm.web.shared.TextSelectionResult;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SortNormalizer;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;

public class TextsTab extends TxmCenterTab{

	private TxmHTMLPane content;
	ArrayList<ListGridField> fields = new ArrayList<ListGridField>();
	private String itemPath;
	EnhancedListGrid valuesGrid;
	private ListGridRecord elem;
	ListGridField textField;

	private static HashMap<String, HashMap<String, String>> valuesPerText = new HashMap<String, HashMap<String,String>>();
	public List<String> properties;
	public HashMap<String, MetadataInfos> propertiesInfos;
	public List<String> displayProperties;
	public HashMap<String, String> biblios;
	public HashMap<String, String> editions;
	public HashMap<String, String> pdfs;
	
	private List<String> textids;
	private ListGridRecord[] records;

	public TextsTab(ListGridRecord elem, Map<String, String> parameters) {
		super(Canvas.imgHTML("icons/functions/Edition.png", 16, 16)+"&nbsp;"+"<span style=\"vertical-align:-2px;\">"+parameters.get(Keys.PATH).substring(1)+"</span>");
		
		this.setName("TEXTS");
		this.itemPath = parameters.get(Keys.PATH);
		this.elem = elem;
		//System.out.println("item path = "+itemPath);

		//		this.setTitle();
		//		this.setIcon("icons/functions/Biblio.png", 16);
		final VLayout layout = new VLayout();
		layout.setWidth100();
		layout.setHeight100();

		//left panel : list
		valuesGrid = new EnhancedListGrid();
		valuesGrid.setWidth100();
		valuesGrid.setHeight("50%");
		valuesGrid.setShowResizeBar(true);

		valuesGrid.addDoubleClickHandler(new DoubleClickHandler() {
			
			@Override
			public void onDoubleClick(DoubleClickEvent event) {
				ListGridRecord r = valuesGrid.getSelectedRecord();
				String textid = r.getAttribute("id");
				String editionOK = r.getAttribute("edition");
				if ("true".equals(editionOK)) {
					
					HashMap<String, String> p = Commands.createParameters(itemPath);
					p.put(Keys.TEXTID, textid);
					Commands.callEdition(p);
				}
			}
		});
		
		valuesGrid.addRecordClickHandler(new RecordClickHandler() {
			@Override
			public void onRecordClick(RecordClickEvent event) {
				final String textid = event.getRecord().getAttribute("id");
				
				int ncol = event.getFieldNum();
				String colid = valuesGrid.getFieldName(ncol);
				if ("edition".equals(colid)) {
					String editionOK = event.getRecord().getAttribute("edition");
					if ("true".equals(editionOK)) {
						HashMap<String, String> p = Commands.createParameters(itemPath);
						p.put(Keys.TEXTID, textid);
						Commands.callEdition(p);
						
					}
				}  else if ("pdf".equals(colid)) {
					String pdfPath = event.getRecord().getAttribute("pdf");
					if (pdfPath != null && pdfPath.trim().length() > 0 && "true".equals(pdfPath)) {
						content.setContents("");
						
						Services.getCorpusActionService().reportActivity("PDF", itemPath+" "+textid, new AsyncCallback<Boolean>() {
							@Override
							public void onSuccess(Boolean result) {
								if (result) {
									// TODO: change this when PDFs will be stored in the private part of Tomcat
									Window.open(TXMWEB.BASEURL+"/"+pdfs.get(textid), "pdf", "");
								} 								
							}
							
							@Override
							public void onFailure(Throwable caught) { }
						});
					}
				} //else { // default is 
				// ALWAYS open biblio page
					//System.out.println(" BIBLIO: "+biblios.get(textid));
					String biblioPath = event.getRecord().getAttribute("biblio");
					if (biblioPath != null && biblioPath.trim().length() > 0 && "true".equals(biblioPath)) {
						//System.out.println("update content  with url: "+biblioPath);
						content.setContentsURL(biblios.get(textid));
					} else {
						content.setContents("");
					}
				//}
			}
		});

		content = new TxmHTMLPane();
		content.setContentsType(ContentsType.FRAGMENT);  
		content.setContents("&nbsp;"+Services.getI18n().noTextSelected());
		content.setMargin(10);

		layout.setMembers(valuesGrid, content);
		this.setPane(layout);

		ProcessingWindow.show(Services.getI18n().retrievingTextList());
		ExecTimer.start();
		Services.getTextSelectionService().getStuffForBiblios(itemPath, new AsyncCallback<TextSelectionResult>() {
			@Override
			public void onSuccess(TextSelectionResult result) {
				//System.out.println("text list : "+result);
				valuesPerText = result.valuesPerText;
				properties = result.properties;
				displayProperties = result.displayProperties;
				propertiesInfos = result.propertiesInfos;
				textids = result.textids;
				biblios = result.biblios;
				editions = result.editions;
				pdfs = result.pdfs;
				
				System.out.println("pdfs: "+pdfs);
				init();
				ProcessingWindow.hide();
			}

			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.severe(Services.getI18n().failed_retrieving_text_list(caught));
			}
		});
		
		this.setParameters(parameters);
	}

	private void init() {		
		System.out.println("BiblioTab: INIT: "+valuesPerText);
		records = new ListGridRecord[textids.size()];
		int i = 0;
		//init records
		for (String textid : textids) {
			ListGridRecord r = new ListGridRecord();
			r.setAttribute("keep", false);
			String biblio = biblios.get(textid);
			String edition = editions.get(textid);
			String pdf = pdfs.get(textid);
			r.setAttribute("edition",  edition != null && edition.length() > 0);
			r.setAttribute("biblio",  biblio != null && biblio.length() > 0);
			r.setAttribute("pdf",  pdf != null && pdf.length() > 0);
			
			r.setAttribute("id", textid);

			for (String property : properties) {
				String type = "String";
				String input = "yyyy-MM-dd";
				if (propertiesInfos.get(property) != null) {
					type = propertiesInfos.get(property).type;
					input = propertiesInfos.get(property).extra.get("inputFormat");
				}

				String value = valuesPerText.get(textid).get(property);
				if (type.equals("Date")) {
					try {
						Date date;
						//String formated;
						if(input == null || input.length() == 0 )
							date = Selector.defaultdateformater.parse(value);
						else
							date = DateTimeFormat.getFormat(input).parse(value);
						r.setAttribute(property, date);

					} catch(Exception e){System.out.println("error date : input="+input+" value="+value+" e="+e);r.setAttribute(property, value);};
				} else if(type.equals("Integer")) {
					try {r.setAttribute(property, Integer.parseInt(value));}
					catch (Exception e) {r.setAttribute(property, value);};
				}
				else
					r.setAttribute(property, value);

			}
			records[i++] = r;
		}

		initColumns();
		valuesGrid.setData(records);
	}

	ListGridField biblioField;
	ListGridField editionField;
	ListGridField pdfField;
	private void initColumns() {
		
		textField = new ListGridField("id", Services.getI18n().id());
		textField.setPrompt(Services.getI18n().id());
		textField.setWidth(150);
		textField.setType(ListGridFieldType.TEXT);
		fields.add(textField);

		editionField = new ListGridField("edition", Services.getI18n().edition());
		editionField.setPrompt(Services.getI18n().editionsPrompt());
		editionField.setType(ListGridFieldType.ICON); 
		editionField.setWidth(40);
		HashMap<String, String> valueicons= new HashMap<String, String>();
		valueicons.put("true", "icons/functions/Edition.png");
		editionField.setValueIcons(valueicons);
		fields.add(editionField);

		biblioField = new ListGridField("biblio", Services.getI18n().biblioCol());
		biblioField.setPrompt(Services.getI18n().bibliosPrompt());
		biblioField.setType(ListGridFieldType.ICON); 
		biblioField.setWidth(40);
		HashMap<String, String> valueicons2 = new HashMap<String, String>();
		valueicons2.put("true", "icons/functions/Biblio.png");
		biblioField.setValueIcons(valueicons2);
		fields.add(biblioField);
		
		pdfField = new ListGridField("pdf", "pdf");
		pdfField.setPrompt(Services.getI18n().pdfsPrompt());
		pdfField.setType(ListGridFieldType.ICON); 
		pdfField.setWidth(40);
		HashMap<String, String> valueicons3 = new HashMap<String, String>();
		valueicons3.put("true", "icons/PDF.png");
		pdfField.setValueIcons(valueicons3);
		fields.add(pdfField);

		//init property fields
		for (final String property : properties) {
			if (property.equals("base") || 
				property.equals("project") || 
				property.equals("id")) {
				continue;
				//System.out.println("prop: "+property);
			}
			final String sortBy;
			String type = "String";
			String shortname = property;
			String longname = property;
			String output = "yyyy-MM-dd";
			if (propertiesInfos.get(property) != null) {
				sortBy = propertiesInfos.get(property).extra.get("sortBy");
				type = propertiesInfos.get(property).type;
				shortname = propertiesInfos.get(property).shortname;
				longname = propertiesInfos.get(property).longname;
				output = propertiesInfos.get(property).extra.get("outputFormat");
			}
			else {
				sortBy = null;
			}
			ListGridField field = new ListGridField(property, longname);
			field.setTitle(shortname);
			field.setPrompt(longname);

			if (type.equals("Integer")) {
				field.setType(ListGridFieldType.INTEGER);
			} else if(type.equals("Date")) {
				if (output == null || output.length() == 0) {	
					field.setCellFormatter(new CellFormatter() {
						@Override
						public String format(Object value, ListGridRecord record, int rowNum,
								int colNum) {
							try{return Selector.defaultdateformater.format((Date) value);}
							catch(Exception e){return value.toString();}
						}
					});
				} else {
					final DateTimeFormat formater = DateTimeFormat.getFormat(output);
					field.setCellFormatter(new CellFormatter() {

						@Override
						public String format(Object value, ListGridRecord record, int rowNum,
								int colNum) {
							try {return formater.format((Date)value);}
							catch (Exception e) {return value.toString();}
						}
					});
				}
			}
			else
				field.setType(ListGridFieldType.TEXT);

			if (sortBy != null && sortBy.length() > 0) {
				if (propertiesInfos.get(sortBy).type.equals("Date")) {	
					field.setSortNormalizer(new SortNormalizer() {
						@Override
						public Object normalize(ListGridRecord record, String fieldName) {
							return record.getAttributeAsDate(sortBy);
						}
					});
				} else {
					field.setSortNormalizer(new SortNormalizer() {
						@Override
						public Object normalize(ListGridRecord record, String fieldName) {
							return record.getAttribute(sortBy);
						}
					});
				}
			}

			fields.add(field);

			if (!displayProperties.contains(property))
				field.setHidden(true);
		}

		valuesGrid.setFields(fields.toArray(new ListGridField[]{}));
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub
		
	}
}
