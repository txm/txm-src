package org.txm.web.client.widgets.pickers;

import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.FormItemIcon;

public class UpPicker extends FormItemIcon {

	private FormItem parent;
	
	public UpPicker() {
		super();
		
		setSrc("pickers/up.png");
		
		setShowOver(true);
	}

}
