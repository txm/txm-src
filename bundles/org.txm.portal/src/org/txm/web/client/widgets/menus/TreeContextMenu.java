package org.txm.web.client.widgets.menus;

import java.util.List;

import org.txm.web.client.Commands;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.shared.Actions;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemSeparator;
import com.smartgwt.client.widgets.menu.events.ClickHandler;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

/**
 * contextual menu of the NavSideTree
 * @author vchabanis
 *
 */
public class TreeContextMenu extends Menu {

	private String path = "";
	private ListGridRecord elem = null;

	private MenuItem subcorpusEntry = new MenuItem(Services.getI18n().createSubcorpus());
	Menu subcorpusMenu = new Menu();
	private MenuItem createSubcorpus = new MenuItem("Structures");
	private MenuItem textSelection = new MenuItem(Services.getI18n().texts());
	private MenuItem createPartition = new MenuItem(Services.getI18n().createPartition());
	private MenuItem properties = new MenuItem(Services.getI18n().properties());
	private MenuItem index = new MenuItem(Services.getI18n().index(""));
	private MenuItem referencer = new MenuItem(Services.getI18n().references());
	private MenuItem dictionnary = new MenuItem(Services.getI18n().dictionnary(""));
	private MenuItem concordance = new MenuItem(Services.getI18n().concordance());
	private MenuItem tsquery = new MenuItem(Services.getI18n().tsquery());
	private MenuItem context = new MenuItem(Services.getI18n().context());
	//private MenuItem contextPara = new MenuItem(Services.getI18n().contextParallel());
	private MenuItem specificity = new MenuItem(Services.getI18n().specificity());
	private MenuItem delete = new MenuItem(Services.getI18n().delete());
	//private MenuItem edition = new MenuItem(Services.getI18n().edition());
	private MenuItem documentation = new MenuItem(Services.getI18n().homeTab());
	private MenuItem download = new MenuItem(Services.getI18n().download());
	private MenuItem texts = new MenuItem(Services.getI18n().biblios());
	private MenuItem cooccurrence = new MenuItem(Services.getI18n().coocurrence());
	private MenuItem afc = new MenuItem(Services.getI18n().afc());
	
	//private MenuItemSeparator separator = ;
	
	public TreeContextMenu()
	{
		init();
	}
	
	public void init() {
		this.setID("TREEMENU"); // selenium
		
		//set icons
		createSubcorpus.setIcon("icons/functions/SubCorpus.png");
		createPartition.setIcon("icons/functions/Partition.png");
		properties.setIcon("icons/functions/Diagnostique.png");
		index.setIcon("icons/functions/Vocabulary.png");
		referencer.setIcon("icons/functions/Referencer.png");
		dictionnary.setIcon("icons/functions/VocabularyP.png");
		concordance.setIcon("icons/functions/Concordances.png");
		cooccurrence.setIcon("icons/functions/Cooccurrences.png");
		tsquery.setIcon("icons/functions/TS.png");
		context.setIcon("icons/functions/Contexts.png");
		//contextPara.setIcon("icons/functions/Contexts.png");
		specificity.setIcon("icons/functions/Specificities.png");
		afc.setIcon("icons/functions/CorrespondanceAnalysis.png");
		delete.setIcon("icons/functions/Delete.png");
		//edition.setIcon("icons/functions/Edition.png");
		documentation.setIcon("icons/house.png");
		download.setIcon("icons/download.png");
		texts.setIcon("icons/functions/Edition.png");
		textSelection.setIcon("icons/functions/Base.png");
		
		//set handlers
		setDeleteHandler(delete);
		setCreateSubcorpusHandler(createSubcorpus);
		setCreatePartitionHandler(createPartition);
		setCreateConcordanceHandler(concordance);
		setCreateTsQueryHandler(tsquery);
		setCreatePropertiesHandler(properties);
		setCreateReferencerHandler(referencer);
		setCreateIndexHandler(index);
		setCreateDictionnaryHandler(dictionnary);
		setCreateContextHandler(context);
		//setCreateContextParaHandler(contextPara);
		//setCreateEditionHandler(edition);
		setCreateDocumentationHandler(documentation);
		setCreateDownloadHandler(download);
		setCreateBiblioHandler(texts);
		setCreateTextSelectionHandler(textSelection);	
		setCreateSpecificitiesHandler(specificity);
		setCreateCooccurrenceHandler(cooccurrence);
		setCreateAFCHandler(afc);
		
		//addItem(edition);
		addItem(documentation);
		addItem(texts);
		addItem(properties);
		addItem(new MenuItemSeparator());
		
		addItem(delete);
		addItem(new MenuItemSeparator());
		
		addItem(subcorpusEntry);
		subcorpusEntry.setSubmenu(subcorpusMenu);
		subcorpusMenu.addItem(textSelection);
		subcorpusMenu.addItem(createSubcorpus);
		addItem(createPartition);	
		addItem(new MenuItemSeparator());
		
		addItem(dictionnary);
		addItem(index);
		addItem(concordance);
		addItem(context);
		addItem(cooccurrence);
		addItem(referencer);
		addItem(tsquery);
		addItem(specificity);
		addItem(afc);
		
		addItem(download);
	}
	
	public void refreshEnables(final boolean showMenu) {
		
		//ListGridRecord elem, String path, List<Actions> allowedActions
		String split[] = TXMWEB.getSideNavTree().getSelectedPaths().split("\"");
		if(split.length < 2)
			return;
		path = split[1];
		elem = TXMWEB.getSideNavTree().getSelectedRecord();
		
		System.out.println("Menu: Item path "+path);
		TXMWEB.log(Services.getI18n().showMenuFor(path));
		Services.getUIService().getContextMenu(path, new AsyncCallback<List<Actions>>() {
			@Override
			public void onFailure(Throwable caught) {
			}
			@Override
			public void onSuccess(List<Actions> result) {
				
				//this is done here to limit the server calls
				TXMWEB.getMenuBar().getFunctionBar().refreshEnables(result, path, elem);
				
				if (!showMenu) {
					return;
				}
				if (!result.contains(Actions.CREATESUBCORPUS)) 
					createSubcorpus.setEnabled(false);
				else
					createSubcorpus.setEnabled(true);
				
				if (!result.contains(Actions.CREATEPARTITION)) 
					createPartition.setEnabled(false);
				else
					createPartition.setEnabled(true);
				
				if (!result.contains(Actions.PROPERTIES)) 
					properties.setEnabled(false);
				else
					properties.setEnabled(true);
				if (!result.contains(Actions.INDEX)) 
					index.setEnabled(false);
				else
					index.setEnabled(true);
				
				if (!result.contains(Actions.REFERENCER))
					referencer.setEnabled(false);
				else
					referencer.setEnabled(true);
				
				if (!result.contains(Actions.LEXICON))
					dictionnary.setEnabled(false);
				else
					dictionnary.setEnabled(true);
				
				if (!result.contains(Actions.CONCORDANCE)) {
					concordance.setEnabled(false);
					context.setEnabled(false);
					//contextPara.setEnabled(false);
				} 
				else {
					concordance.setEnabled(true);
					context.setEnabled(true);
					//contextPara.setEnabled(true);
				}
				
				if (!result.contains(Actions.TSQUERY))
					tsquery.setEnabled(false);
				else
					tsquery.setEnabled(true);
				
				if (!(result.contains(Actions.SPECIFICITIES) && TXMWEB.RENABLE ))
					specificity.setEnabled(false);
				else
					specificity.setEnabled(true);
				
//				if (!result.contains(Actions.EDITION))
//					edition.setEnabled(false);
//				else
//					edition.setEnabled(true);
				
				if (result.contains(Actions.DOCUMENTATION))
					documentation.setEnabled(true);
				else
					documentation.setEnabled(false);
				
				if (result.contains(Actions.DOWNLOAD))
					download.setEnabled(true);
				else
					download.setEnabled(false);
				
				if (result.contains(Actions.TEXTS))
					texts.setEnabled(true);
				else
					texts.setEnabled(false);
				
				if (!result.contains(Actions.DELETE))
					delete.setEnabled(false);
				else
					delete.setEnabled(true);
				
				if (!result.contains(Actions.TEXTSELECTION))
					textSelection.setEnabled(false);
				else
					textSelection.setEnabled(true);
				
				if (!(result.contains(Actions.COOCCURRENCE) && TXMWEB.RENABLE))
					cooccurrence.setEnabled(false);
				else
					cooccurrence.setEnabled(true);
				
				if(!(result.contains(Actions.AFC) && TXMWEB.RENABLE))
					afc.setEnabled(false);
				else
					afc.setEnabled(true);

				if (showMenu) {
					show();
					refreshFields();
				}
			}
		});
	}

	private void setDeleteHandler(MenuItem item) {
		
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				//path = TXM_RIA.getSideNavTree().getSelectedPaths().split("\"")[1];
				
				
				TXMWEB.log(Services.getI18n().delete()+path);
				Commands.callRemove(Commands.createParameters(path));
			}
		});
	}

	private void setCreateTsQueryHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callTS(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreatePropertiesHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callProperties(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreateSubcorpusHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callSubCorpus(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreatePartitionHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callPartition(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreateConcordanceHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callConcordances(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreateContextHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callContexts(Commands.createParameters(path), elem);
			}
		});
	}
	

	private void setCreateContextParaHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callContextsPara(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreateIndexHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callIndex(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreateDictionnaryHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callLexicon(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreateEditionHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callEdition(Commands.createParameters(path), elem);
			}
		});
	}
	
	private void setCreateDocumentationHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callDocumentation(Commands.createParameters(path));
			}
		});
	}
	
	private void setCreateDownloadHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callDownload(Commands.createParameters(path));
			}
		});
	}
	
	private void setCreateBiblioHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callTexts(Commands.createParameters(path), elem);
			}
		});
	}

	private void setCreateTextSelectionHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callSelection(Commands.createParameters(path), elem);
			}
		});
	}
	

	private void setCreateReferencerHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callReferencer(Commands.createParameters(path), elem);
			}
		});
	}
	

	private void setCreateSpecificitiesHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callSpecificities(Commands.createParameters(path), elem);
			}
		});	
	}
	
	private void setCreateCooccurrenceHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callCooccurrences(Commands.createParameters(path), elem);
			}
		});	
	}
	
	private void setCreateAFCHandler(MenuItem item) {
		item.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(MenuItemClickEvent event) {
				Commands.callAFC(Commands.createParameters(path), elem);
			}
		});	
	}
}
