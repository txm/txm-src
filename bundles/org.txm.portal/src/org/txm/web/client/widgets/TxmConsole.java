package org.txm.web.client.widgets;

import com.google.gwt.user.client.Timer;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.VLayout;

public class TxmConsole extends VLayout{
	Label cbItem;
	int time = 15000;
	
	Timer timer = new Timer() {
		@Override
		public void run() {
			cbItem.setContents("");
		}
	};

	public TxmConsole(int max) {		
		this.setID("TXMConsole");
		cbItem = new Label();  
		cbItem.setCanSelectText(true);
		cbItem.setWidth("100%");
		cbItem.setHeight("25px");
		//cbItem.setLeft(20);
		this.addMember(cbItem);
	}
	
	public void addMessage(String message, boolean keep) {
		cbItem.setContents("<b>"+message+"</b>");
		timer.cancel();
	}

	public void addMessage(String message) {
		cbItem.setContents("<b>"+message+"</b>");
		timer.cancel();
		timer.schedule(time);
	}
	
	public void addError(String message, boolean keep) {
		cbItem.setContents("<b><font color=\"#FF0000\">"+message+"</font></b>");
		timer.cancel();
	}
	
	public void addError(String message) {
		cbItem.setContents("<b><font color=\"#FF0000\">"+message+"</font></b>");
		timer.cancel();
		timer.schedule(time);
	}
	
	public void addWarning(String message) {
		cbItem.setContents("<b><font color=\"#FFCC00\">"+message+"</font></b>");
		timer.cancel();
		timer.schedule(time);
	}
	
	public void addWarning(String message, boolean keep) {
		cbItem.setContents("<b><font color=\"#FFCC00\">"+message+"</font></b>");
		timer.cancel();
	}
}
