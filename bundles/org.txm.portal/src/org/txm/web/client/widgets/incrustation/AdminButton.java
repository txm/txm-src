package org.txm.web.client.widgets.incrustation;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.tabs.admin.AdminTab;

import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.events.ClickEvent;

public class AdminButton{
	public static String ID = "admin_target";

	public static Button getButton()
	{
		Button btn = new Button(Services.getI18n().adminButton());
		btn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				new AdminTab().addInView();
			}
		});
		return btn;
	}
	
	public static Anchor getLink()
	{
		Anchor btn = new Anchor();
		btn.setHTML(Services.getI18n().adminButton());
		btn.addClickHandler(new com.google.gwt.event.dom.client.ClickHandler() {
			
			@Override
			public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
				new AdminTab().addInView();
			}
		});
		return btn;
	}
	
	public static boolean incrust()
	{
		RootPanel target = RootPanel.get(ID);
		if (target != null)
		{
			target.clear();
			String type = target.getElement().getAttribute("type");
			if(type != null && type.equals("link"))
				target.add(getLink());
			else
				target.add(getButton());
			return true;
		}
		return false;
	}
}
