package org.txm.web.client.widgets.incrustation;

import org.txm.web.client.TXMWEB;

import com.google.gwt.user.client.ui.RootPanel;

public class ClientVariable {
	public static String ID = "variable_target";
	public static String availables[] = {"MAINTENANCE", "PORTALNAME", "VERSION", "INSCRIPTION", "MAIL", "CONTACT"};
	public static boolean incrust()
	{
		RootPanel target = RootPanel.get(ID);
		if (target != null) {
			target.clear();
			String type = target.getElement().getAttribute("type");
			if (type != null) {
				if (type.equals("MAINTENANCE"))
					target.getElement().setInnerText(""+TXMWEB.MAINTENANCE);
				else if (type.equals("PORTALNAME"))
					target.getElement().setInnerText(""+TXMWEB.PORTALSHORTNAME);
				else if (type.equals("VERSION"))
					target.getElement().setInnerText(""+TXMWEB.VERSION);
				else if (type.equals("INSCRIPTION"))
					target.getElement().setInnerText(""+TXMWEB.INSCRIPTIONENABLE);
				else if (type.equals("MAIL"))
					target.getElement().setInnerText(""+TXMWEB.MAILENABLE);
				else if (type.equals("CONTACT"))
					target.getElement().setInnerText(""+TXMWEB.CONTACT);
				return true;
			}
		}
		return false;
	}
}
