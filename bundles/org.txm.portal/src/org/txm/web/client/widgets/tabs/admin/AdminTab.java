package org.txm.web.client.widgets.tabs.admin;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.tabs.TxmCenterTab;

import com.smartgwt.client.types.Side;
import com.smartgwt.client.types.TabBarControls;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.TabSet;


public class AdminTab extends TxmCenterTab {

	TabSet tabs = new TabSet();
	private ControlPanelTab ctrlTab;
	private ManageUserTab userTab;
	private ManageCorpusTab baseTab;
	private ContactUser contactTab;
	private ManageProfileTab manageTab;
	private DumpDataTab dumpTab;
	
	public void setTitle(String title) {
		super.setTitle(Canvas.imgHTML("icons/warning.png", 16, 16)+"&nbsp;"+"<span style=\"vertical-align:-2px;\">"+title+"</span>");
	}
	
	public AdminTab() {
		this.setTitle(Services.getI18n().admin());
		VLayout content = new VLayout();
		
        Layout paneContainerProperties = new Layout();
        paneContainerProperties.setLayoutMargin(0);
        paneContainerProperties.setLayoutTopMargin(1);
        
        tabs.setPaneContainerProperties(paneContainerProperties);
        tabs.setWidth100();
        tabs.setHeight100();
        tabs.setTabBarControls(TabBarControls.TAB_SCROLLER, TabBarControls.TAB_PICKER);
        tabs.setTabBarPosition(Side.BOTTOM);
        
        ctrlTab = (new ControlPanelTab(this));
        manageTab = (new ManageProfileTab(this));
        userTab = (new ManageUserTab(this));
        baseTab = (new ManageCorpusTab(this));
        contactTab = (new ContactUser(this));
        dumpTab = new DumpDataTab(this);

        tabs.addTab(ctrlTab);
        tabs.addTab(manageTab);
        tabs.addTab(userTab);
        tabs.addTab(baseTab);
        tabs.addTab(contactTab);
        tabs.addTab(dumpTab);

		content.setMembers(tabs);
		this.setPane(content);
	}

	/**
	 * @return the tabs
	 */
	public TabSet getTabs() {
		return tabs;
	}

	/**
	 * @return the ctrlTab
	 */
	public ControlPanelTab getCtrlTab() {
		return ctrlTab;
	}

	/**
	 * @return the userTab
	 */
	public ManageUserTab getUserTab() {
		return userTab;
	}

	/**
	 * @return the baseTab
	 */
	public ManageCorpusTab getBaseTab() {
		return baseTab;
	}

	/**
	 * @return the contactTab
	 */
	public ContactUser getContactTab() {
		return contactTab;
	}

	/**
	 * @return the manageTab
	 */
	public ManageProfileTab getManageTab() {
		return manageTab;
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub
	}
}