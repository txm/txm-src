package org.txm.web.client.widgets.tabs;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.txm.web.client.ExecTimer;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.EnhancedListGrid;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.forms.TxmForm;
import org.txm.web.shared.AFCParams;
import org.txm.web.shared.ValeurPropreResultLine;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Frame;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.Side;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;

public class AFCTab extends TxmCenterTab{

	private HLayout AFCTabContent = new HLayout();
	private EnhancedListGrid valeursPropresList = new EnhancedListGrid();
	private EnhancedListGrid rowInfosList = new EnhancedListGrid();
	private EnhancedListGrid colInfosList = new EnhancedListGrid();
	private final static String FACTOR_COLNAME = "factor";
	private final static String VALUE_COLNAME = "value";
	private final static String PERCENT_COLNAME = "percent";
	VLayout svgLayout = new VLayout();
	private HTMLPane caSvgFrame;
	private TabSet rightTabSet;
	private DynamicForm form;

	public AFCTab(String itemPath, final AFCParams params) {
		super(Canvas.imgHTML("icons/functions/CorrespondanceAnalysis.png", 16, 16)+"&nbsp;"+"<span style=\"vertical-align:-2px;\">"+"AFC  - " + itemPath+"</span>");
		this.setName("AFC");
		
		//this.setTitle();
		//this.setIcon("icons/functions/CorrespondanceAnalysis.png", 16);
		AFCTabContent.setWidth100();
		AFCTabContent.setHeight100();
		caSvgFrame = new HTMLPane();
		caSvgFrame.setContentsType(ContentsType.PAGE);
		caSvgFrame.setWidth("100%");
		caSvgFrame.setHeight("100%");

		//right tabs
		rightTabSet = new TabSet();
		rightTabSet.setTabBarPosition(Side.TOP);
		rightTabSet.setWidth("30%");
		rightTabSet.setHeight("100%");
		rightTabSet.setVisible(false);

		//valeurs propres
		Tab tab1 = new Tab(Services.getI18n().singularValues());
		ListGridField factorList = new ListGridField(FACTOR_COLNAME, Services.getI18n().factor());
		ListGridField valuesList = new ListGridField(VALUE_COLNAME, Services.getI18n().singularValues());
		valuesList.setFormat("0.0000");
		ListGridField percentList = new ListGridField(PERCENT_COLNAME, "%");
		percentList.setFormat("0.00");

		tab1.setPane(valeursPropresList);
		valeursPropresList.setShowRowNumbers(true);
		valeursPropresList.setCanResizeFields(true);
		valeursPropresList.setCanSelectText(true);
		valeursPropresList.setCanFreezeFields(false);
		valeursPropresList.setCanGroupBy(false);
		valeursPropresList.setCanAutoFitFields(false);
		valeursPropresList.setCanPickFields(false);
		valeursPropresList.setShowAllRecords(true);
		valeursPropresList.setFields( factorList, valuesList, percentList);

		Tab tab2 = new Tab(Services.getI18n().infosLines());
		tab2.setPane(rowInfosList);
		Tab tab3 = new Tab(Services.getI18n().infoCols());
		tab3.setPane(colInfosList);
		rightTabSet.addTab(tab1);
		rightTabSet.addTab(tab2);
		rightTabSet.addTab(tab3);

		form = new DynamicForm();

		final ComboBoxItem comboFactorsItem = new ComboBoxItem();
		comboFactorsItem.setName("factors");
		comboFactorsItem.setTitle(Services.getI18n().factors());
		String[] factorValues = {"1,2", "1,3", "2,3"};
		comboFactorsItem.setValueMap(factorValues);
		comboFactorsItem.setDefaultValue("1,2");
		final CheckboxItem linesCheckboxItem = new CheckboxItem("lines", Services.getI18n().lines());
		final CheckboxItem columnsCheckboxItem = new CheckboxItem("columns", Services.getI18n().columns());
		columnsCheckboxItem.setDefaultValue(Boolean.TRUE);
		IButton refreshButton = new IButton(Services.getI18n().refreshButton());
		refreshButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				params.setShowCol(columnsCheckboxItem.getValueAsBoolean());
				params.setShowLine(linesCheckboxItem.getValueAsBoolean());
				String[] factors = comboFactorsItem.getValueAsString().split(",");
				params.setFirstDim(Integer.parseInt(factors[0]));
				params.setSecondDim(Integer.parseInt(factors[1]));
				ProcessingWindow.show(Services.getI18n().refreshing());
				//                for (int i = 0 ; i < svgLayout.getChildren().length ; i++) {
				//                	System.out.println("canvas: "+svgLayout.getChildren()[i]);
				//                }

				ExecTimer.start();
				Services.getAFCService().analysisComputing(params, new AsyncCallback<String>() {
					@Override
					public void onSuccess(String result) {
						ProcessingWindow.hide();
						caSvgFrame.setContentsURL(GWT.getHostPageBaseURL() + "data/svg/" + result);
						caSvgFrame.setHeight("100%");
						caSvgFrame.setWidth("100%");
					}

					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						SC.say(Services.getI18n().errorWith(caught.getMessage()));
						TXMWEB.getConsole().addError(Services.getI18n().errorWith(caught.getMessage()));
					}
				});
			}
		});

		IButton exportButton = new IButton(Services.getI18n().concExportButton());
		exportButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				params.setShowCol(columnsCheckboxItem.getValueAsBoolean());
				params.setShowCol(linesCheckboxItem.getValueAsBoolean());
				String[] factors = comboFactorsItem.getValueAsString().split(",");
				params.setFirstDim(Integer.parseInt(factors[0]));
				params.setSecondDim(Integer.parseInt(factors[1]));
				ProcessingWindow.show(Services.getI18n().refreshing());
				for (int i = 0 ; i < svgLayout.getChildren().length ; i++) {
					System.out.println("canvas: "+svgLayout.getChildren()[i]);
				}

				ExecTimer.start();
				Services.getAFCService().exportCA(params, new AsyncCallback<String>() {
					@Override
					public void onSuccess(String result) {
						ProcessingWindow.hide();
						String url = GWT.getHostPageBaseURL() + result;
						Window.open(url, "", "");
						TxmDialog.exportWarning();
					}

					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						SC.say(Services.getI18n().errorWith(caught.getMessage()));
						TXMWEB.getConsole().addError(Services.getI18n().errorWith(caught.getMessage()));
					}
				});
			}
		});

		form.setNumCols(7);
		form.setFields(comboFactorsItem, columnsCheckboxItem, linesCheckboxItem);

		HLayout h = new HLayout();
		h.setMembers(form, refreshButton, exportButton);
		h.setHeight(10);

		svgLayout.setWidth("70%");
		svgLayout.setHeight("100%");
		svgLayout.setOverflow(Overflow.AUTO);
		svgLayout.setShowResizeBar(Boolean.TRUE);

		for (int i = 0 ; i < svgLayout.getChildren().length ; i++)  {
			svgLayout.getChildren()[i].setHeight100();
		}

		rightTabSet.setShowResizeBar(true);
		rightTabSet.setOverflow(Overflow.HIDDEN);
		//rightTabSet.setShowResizeBar(true);
		svgLayout.setMembers(h, caSvgFrame);
		AFCTabContent.addMember(svgLayout);
		AFCTabContent.addMember(rightTabSet);

		this.setPane(AFCTabContent);

		ProcessingWindow.show(Services.getI18n().computing_AFC(params.getProperty()));
		Services.getAFCService().analysisComputing(params, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				ProcessingWindow.hide();
				initGUI(result, params);
			}

			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				SC.say(Services.getI18n().errorWith(caught.getMessage()));
				TXMWEB.getConsole().addError(Services.getI18n().errorWith(caught.getMessage()));
			}
		});
	}

	private void initGUI(String svgFile, final AFCParams params) {

		caSvgFrame.setContentsURL(GWT.getHostPageBaseURL() + "data/svg/" + svgFile);
		Services.getAFCService().getValeursPropres(params, new AsyncCallback<ArrayList<ValeurPropreResultLine>>() {
			@Override
			public void onSuccess(ArrayList<ValeurPropreResultLine> result) {
				for (ValeurPropreResultLine vprl : result) {
					ListGridRecord record = new ListGridRecord();
					record.setAttribute(FACTOR_COLNAME, vprl.getFactor());
					record.setAttribute(VALUE_COLNAME, vprl.getValue());
					record.setAttribute(PERCENT_COLNAME, vprl.getPercent());
					valeursPropresList.addData(record);
				}

				//                for (int i = 0 ; i < svgLayout.getChildren().length ; i++) {
				//                	//svgLayout.getChildren()[i].setHeight100();
				//                	System.out.println("canvas2: "+svgLayout.getChildren()[i]);
				//                }

				caSvgFrame.setHeight("100%");
				caSvgFrame.setVisible(true);
				rightTabSet.setVisible(true);
			}

			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				SC.say(Services.getI18n().errorWith(caught.getMessage()));
				TXMWEB.getConsole().addError(Services.getI18n().errorWith(caught.getMessage()));
			}
		});


		//get row titles at first, onsuccess: get row infos lines
		Services.getAFCService().getRowInfoTitles(params, new AsyncCallback<String[]>() {

			@Override
			public void onSuccess(final String[] result1) {
				ArrayList<ListGridField> rowFields = new ArrayList<ListGridField>();
				int i = 0;
								
				for (String s : result1) {

					if (s.length() > 1) {
						rowFields.add(new ListGridField(s, s));
						if (i > 0) {
							rowFields.get(i).setFormat("0.0000");
							rowFields.get(i).setType(ListGridFieldType.FLOAT);
							rowFields.get(i).setWidth(60);
						} else {
							rowFields.get(i).setWidth(100);
						}
						i++;
					}
				}
				
				rowInfosList.setShowRowNumbers(true);
				rowInfosList.setCanResizeFields(true);
				rowInfosList.setCanSelectText(true);
				rowInfosList.setCanFreezeFields(false);
				rowInfosList.setCanGroupBy(false);
				rowInfosList.setCanAutoFitFields(false);
				rowInfosList.setCanPickFields(false);
				rowInfosList.setShowAllRecords(true);
				//rowInfosList.setAutoSizeHeaderSpans(true);
				rowInfosList.setFields(rowFields.toArray(new ListGridField[rowFields.size()]));
				Services.getAFCService().getRowInfos(params, new AsyncCallback<LinkedHashMap<String, List<Double>>>() {

					@Override
					public void onSuccess(LinkedHashMap<String, List<Double>> result2) {
						for (String k : result2.keySet()) {
							List<Double> doubles= result2.get(k);
							
							ListGridRecord record = new ListGridRecord();
							record.setAttribute(rowInfosList.getFieldName(1), k); // 0=no 1=Lignes
							int i = 2;
							for (Object s : doubles) {
								record.setAttribute(rowInfosList.getFieldName(i), s);
								i++;
							}
							rowInfosList.addData(record);
						}

					}

					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						SC.say(Services.getI18n().errorWith(caught.getMessage()));
						TXMWEB.getConsole().addError(Services.getI18n().errorWith(caught.getMessage()));
					}
				});

			}

			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				SC.say(Services.getI18n().errorWith(caught.getMessage()));
				TXMWEB.getConsole().addError(Services.getI18n().errorWith(caught.getMessage()));
			}
		});


		//the same for cols
		Services.getAFCService().getColInfoTitles(params, new AsyncCallback<String[]>() {
			@Override
			public void onSuccess(final String[] result1) {
				ArrayList<ListGridField> colFields = new ArrayList<ListGridField>();
				int i = 0;
				for (String s : result1) {

					if (s.length() > 1) {
						colFields.add(new ListGridField(s, s));
						if (i > 0) {
							colFields.get(i).setFormat("0.0000");
							colFields.get(i).setType(ListGridFieldType.FLOAT);
							colFields.get(i).setWidth(60);
						} else {
							colFields.get(i).setWidth(100);
						}
						i++;
					}
				}
				colInfosList.setShowRowNumbers(true);
				colInfosList.setCanResizeFields(true);
				colInfosList.setCanSelectText(true);
				colInfosList.setCanFreezeFields(false);
				colInfosList.setCanGroupBy(false);
				colInfosList.setCanAutoFitFields(false);
				colInfosList.setCanPickFields(false);
				colInfosList.setShowAllRecords(true);

				colInfosList.setFields(colFields.toArray(new ListGridField[colFields.size()]));
				
				Services.getAFCService().getColInfos(params, new AsyncCallback<LinkedHashMap<String, List<Double>>>() {

					@Override
					public void onSuccess(LinkedHashMap<String, List<Double>> result2) {
						for (String k : result2.keySet()) {
							List<Double> doubles = result2.get(k);
							ListGridRecord record = new ListGridRecord();
							record.setAttribute(colInfosList.getFieldName(1), k); // 0=no 1=Colonnes
							int i = 2;
							for (Double d : doubles) {
								record.setAttribute(colInfosList.getFieldName(i), d);
								i++;
							}
							colInfosList.addData(record);
						}
					}

					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						SC.say(Services.getI18n().errorWith(caught.getMessage()));
						TXMWEB.getConsole().addError(Services.getI18n().errorWith(caught.getMessage()));
					}
				});
			}

			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				SC.say(Services.getI18n().errorWith(caught.getMessage()));
				TXMWEB.getConsole().addError(Services.getI18n().errorWith(caught.getMessage()));
			}
		});

		Services.getAFCService().singularValues(params, new AsyncCallback<String>() {

			@Override
			public void onSuccess(String result) {
				Tab tab = new Tab(Services.getI18n().histogram());
				rightTabSet.addTab(tab);
				Frame f = new Frame(GWT.getHostPageBaseURL() + "data/svg/" + result);
				f.setWidth("100%");
				f.setHeight("100%");
				HLayout hl = new HLayout();
				hl.setWidth100();
				hl.setHeight100();
				hl.addMember(f);
				tab.setPane(hl);
			}

			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
			}
		});
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub
	}
	
	public TxmForm getForm() {
		return null;
	}
}
