package org.txm.web.client.widgets.forms;

import java.util.Map;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.tabs.textselection.TextSelectionTab;
import org.txm.web.shared.Keys;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.layout.HLayout;

public class CreateSelection extends TxmWindow {

	TextSelectionTab sel;
	
	CheckboxItem acceptItem = new CheckboxItem("Recap");  
	TextItem nameField;
	CheckboxItem showN = new CheckboxItem("ShowN", Services.getI18n().showN());
	CheckboxItem showT = new CheckboxItem("ShowT", Services.getI18n().showT());
	CheckboxItem showHumanVersion = new CheckboxItem("ShowHuman", "Show human ");
	
	private IButton validateButton;
	private IButton cancelButton;
	
	public CreateSelection(TextSelectionTab sel) {
		super(true, "");
		
		this.sel = sel;
		
		initializeFields(null);
		//setParameters(parameters); // do it here because no Tab will do it
	}
	
	@Override
	public void _initializeFields(Map<String, String> parameters) {
		
		this.setTitle(Services.getI18n().finalizeSelection());
		this.setModalMaskOpacity(20);
		this.setShowModalMask(true);
		this.setWidth(300);
		this.setHeight(100);
		this.setAutoHeight();		
		
		form = new DynamicForm();
		form.setPadding(10);
		form.setWidth("100%");
		form.setHeight("100%");
		form.setColWidths("20%","80%");
		
		// check
		
		acceptItem.setPrompt(Services.getI18n().recapPrompt());
		acceptItem.setTitle(Services.getI18n().showSelectionSummary());
		//acceptItem.setHint(Services.getI18n().aNewTabWillOpen());
		acceptItem.setShowTitle(true);	
		acceptItem.setWidth("100");
		acceptItem.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				boolean shouldDisable = !acceptItem.getValueAsBoolean();
				nameField.setDisabled(shouldDisable);
			}
		});
		
		// textfield
		nameField = new TextItem(Keys.NAME);
		nameField.setPrompt(Services.getI18n().newNamePrompt());
		nameField.setTitle(Services.getI18n().selectionName());
		form.setItems(nameField, acceptItem, showN, showT);
		
		// ok // cancel
		validateButton = new IButton(Services.getI18n().validateButton());
		validateButton.disable();
		cancelButton = new IButton(Services.getI18n().cancelButton());
		HLayout buttons = new HLayout(2);
		buttons.setMargin(7);
		buttons.setWidth100();
		buttons.setAlign(Alignment.CENTER);
		buttons.setMembers(validateButton, cancelButton);
		
		addValidateHandlers();
		
		this.addItem(form);
		this.addItem(buttons);

		//RootPanel.get().add(this);
		this.centerInPage();this.show();
	}

	private void addValidateHandlers() {
		validateButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				validate();
			}
		});
		cancelButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				destroy();
			}
		});
		nameField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event.getKeyName().equals("Enter")) {
					validate();
				}
			}
		});
		ChangedHandler changelistener = new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				if (nameField.getValue() != null)
					validateButton.enable();
				else
					validateButton.disable();
			}
		};
		nameField.addChangedHandler(changelistener);
	}
	
	private void validate() {
		String value = form.getValueAsString(Keys.NAME);
		if (sel.finalizeSelection(value, acceptItem.getValueAsBoolean(), 
				showN.getValueAsBoolean(), 
				showT.getValueAsBoolean(), 
				showHumanVersion.getValueAsBoolean())) {
			this.destroy();
		}
	}
}
