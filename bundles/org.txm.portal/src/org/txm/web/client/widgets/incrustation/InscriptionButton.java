package org.txm.web.client.widgets.incrustation;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.forms.SubscribeForm;

import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.events.ClickEvent;

public class InscriptionButton extends Button{
	public static String ID = "inscription_target";

	public static Button getButton()
	{
		Button btn = new Button("<b>"+Services.getI18n().subscribeButton()+"</b>");
		btn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				new SubscribeForm();
			}
		});
		return btn;
	}
	
	public static Anchor getLink()
	{
		Anchor btn = new Anchor();
		btn.setHTML("<b>"+Services.getI18n().subscribeButton()+"</b>");
		btn.addClickHandler(new com.google.gwt.event.dom.client.ClickHandler() {
			@Override
			public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
				new SubscribeForm();
			}
		});
		return btn;
	}
	
	public static boolean incrust()
	{
		RootPanel target = RootPanel.get(ID);
		//System.out.println("target: "+target);
		if (target != null)
		{
			target.clear();
			String type = target.getElement().getAttribute("type");
			if(type != null && type.equals("link"))
				target.add(getLink());
			else
				target.add(getButton());
			return true;
		}
		return false;
	}
}
