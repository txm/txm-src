package org.txm.web.client.widgets.tabs.admin;

import java.util.List;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.shared.ServerInfosUpdater;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class ContactUser extends Tab {

	SelectItem userSelection = new SelectItem();
	SelectItem profileSelection = new SelectItem();
	String[] allusers;
	String[] allprofiles;

	Label notification = new Label();
	private AdminTab adminTab;
	
	public static void quickSendMessage(String message, final ButtonItem btn)
	{
		Services.getAdminService().quickSend(message, new AsyncCallback<Boolean>() {

			@Override
			public void onFailure(Throwable caught) { }

			@Override
			public void onSuccess(Boolean result) {
				Timer t = new Timer() {
					int time = ServerInfosUpdater.getLoopInSec();
					public void run() {
						time--;
						if(time > 0)
							btn.setHint("wait... "+time+"s");
						else
						{
							btn.setHint("OK");
							this.cancel();
							btn.enable();
						}
					}
				};
				t.scheduleRepeating(1000);
				btn.disable();
			}
		});
	}
	
	public ContactUser(AdminTab adminTab) {
		this.adminTab = adminTab;

		try {
			this.setTitle(Services.getI18n().sendEMail());

			final DynamicForm form = new DynamicForm();
			form.setHeight("100%");
			form.setWidth("100%");
			form.setNumCols(3);
			//form.setColWidths("50","*","100");
			
			final TextItem textItem = new TextItem();  
			textItem.setTitle("Quick message to all users");  
			textItem.setHint("If the message starts with '!!' then the message will be shown in a toolbar, with \"!!!\" in a popup"); 
			
			final ButtonItem quickSendBtn = new ButtonItem("sendQuick", "Send");  
			quickSendBtn.setStartRow(false);  
			quickSendBtn.setWidth(80);   
			quickSendBtn.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				@Override
				public void onClick(
						com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
							quickSendMessage(textItem.getValueAsString(), quickSendBtn);
				}
			});
			
			userSelection.setName("users");
			userSelection.setTitle(Services.getI18n().selectUsersToContact());
			userSelection.setMultiple(true); 

			ButtonItem selectAllUserBtn = new ButtonItem("selectAllUsers", "Select all users");  
			selectAllUserBtn.setStartRow(false);  
			selectAllUserBtn.setWidth(80);   
			selectAllUserBtn.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				@Override
				public void onClick(
						com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					if (allusers  != null)
						userSelection.setValues(allusers);
				}
			}); 

			profileSelection.setName("profiles");
			profileSelection.setTitle(Services.getI18n().selectProfilesToContact());
			profileSelection.setMultiple(true); 

			ButtonItem selectAllProfilesBtn = new ButtonItem("selectAllProfiles", "Select all profiles");  
			selectAllProfilesBtn.setStartRow(false);  
			selectAllProfilesBtn.setWidth(80);   
			selectAllProfilesBtn.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				@Override
				public void onClick(
						com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					if (allprofiles  != null)
						profileSelection.setValues(allprofiles);
				}
			}); 
			
			final TextItem objectField = new TextItem();
			objectField.setName("subject");
			objectField.setShowTitle(true);

			final TextAreaItem contentField = new TextAreaItem();
			contentField.setName("content");
			contentField.setShowTitle(true);
			contentField.setHeight("200px");
			contentField.setWidth("100%");

			IButton sendButton = new IButton("Send");
			sendButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {

					String users = form.getValueAsString("users");
					String profiles = form.getValueAsString("profiles");
					String subjectValue = form.getValueAsString("subject");
					String contentValue = form.getValueAsString("content");

					boolean nouser = false;
					boolean noprofile = false;
					if (users == null || users.trim().length() == 0) {
						nouser = true;
					}

					if (profiles == null || profiles.trim().length() == 0) {
						noprofile = true;
					}

					if (noprofile && nouser) {
						if (noprofile)
							TxmDialog.severe("No profile set");
						if (nouser)
							TxmDialog.severe("No user set");
						return;
					}

					if (subjectValue == null || subjectValue.trim().length() == 0) {
						TxmDialog.severe("Message object is not set");
						return;
					}

					if (contentValue == null || contentValue.trim().length() == 0) {
						TxmDialog.severe("Mail content is empty");
						return;
					}

					ProcessingWindow.show("Sending mail to: "+users+" and profiles: "+profiles);
					sendMail(users, profiles, subjectValue, contentValue);
				}
			});

			form.setItems(textItem, quickSendBtn, userSelection, selectAllUserBtn, profileSelection, selectAllProfilesBtn, objectField, contentField);

			VLayout content = new VLayout();
			content.setMembers(form, sendButton, notification);
			content.setHeight("400px");
			content.setWidth("100%");

			refreshUsersLists();
			refreshProfilesLists();
			this.setPane(content);
		} catch(Exception e) {
			TxmDialog.severe("Internal Error: "+e);
		}
	}

	private void sendMail(String users, String profiles, String obj, String content)
	{
		Services.getAdminService().sendMail(users, profiles, obj, content, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				ProcessingWindow.hide();
				TxmDialog.severe("Failed to send mail to users : "+caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				if (result.length() > 0) {
					TxmDialog.severe("Failed to send mail to users : "+result);
				}
				ProcessingWindow.hide();
			}
		});
	}

	private void refreshUsersLists()
	{
		Services.getAdminService().listUsers(new AsyncCallback<List<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.warning("No user available");
			}
			@Override
			public void onSuccess(List<String> result) {
				allusers = result.toArray(new String[result.size()]);
				userSelection.setValueMap(allusers);
			}
		});
	}

	private void refreshProfilesLists()
	{
		Services.getAdminService().listProfiles(new AsyncCallback<List<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				TxmDialog.warning("No profile available");
			}
			@Override
			public void onSuccess(List<String> result) {
				ProcessingWindow.hide();
				result.remove(result.indexOf("Anonymous"));
				allprofiles = result.toArray(new String[result.size()]);
				profileSelection.setValueMap(allprofiles);
			}
		});
	}
}
