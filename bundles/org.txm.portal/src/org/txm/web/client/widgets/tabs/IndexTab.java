package org.txm.web.client.widgets.tabs;

import java.util.ArrayList;
import java.util.Map;

import org.txm.web.client.ExecTimer;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.EnhancedListGrid;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.forms.IndexForm;
import org.txm.web.client.widgets.forms.LexiconForm;
import org.txm.web.client.widgets.forms.NavigationToolStrip;
import org.txm.web.client.widgets.forms.TxmForm;
import org.txm.web.shared.Keys;
import org.txm.web.shared.VocabularyResult;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.CellDoubleClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class IndexTab extends TxmCenterTab {

	protected IndexForm form = null;
	public TxmForm getForm() {
		return this.form;
	}
	protected VocabularyResult result = null;
	protected EnhancedListGrid resultList = new EnhancedListGrid();
	// protected Label infoLabel;
	// protected SpinnerItem nbperpageField;
	protected NavigationToolStrip navigation;
	public int startindex = 0;
	protected int endindex = 0;
	protected int totalpages = 0;
	protected ArrayList<String> colnames;
	protected ArrayList<ListGridField> listGridFields = new ArrayList<ListGridField>();
	// private VocabularyResult currentResult = null;
	VLayout resultSection;
	VLayout section2;
	ToolStrip indexBar;
	private boolean lexicon;
	private String itemPath;

	public void setTitle(String title) {
		if (lexicon) {
			super.setTitle(Canvas.imgHTML("icons/functions/VocabularyP.png", 16, 16)
					+ "&nbsp;" + "<span style=\"vertical-align:-2px;\">" + title
					+ "</span>");
		} else {
			super.setTitle(Canvas.imgHTML("icons/functions/Vocabulary.png", 16, 16)
					+ "&nbsp;" + "<span style=\"vertical-align:-2px;\">" + title
					+ "</span>");
		}
	}
	
	public void compute() {
		form.validate();
	}

	public IndexTab(Map<String, String> parameters, ListGridRecord elem, final boolean lexicon) {
		
		this.setName("INDEX");
		this.lexicon = lexicon;
		this.itemPath = parameters.get(Keys.PATH);
		if (lexicon) {
			this.form = new LexiconForm(parameters, elem, this);
		} else {
			this.form = new IndexForm(parameters, elem, this);
		}
		
		this.setTitle(form.getParams().getCorpusPath() + ": " + form.getParams().getQuery());

		resultSection = new VLayout();
		resultSection.setID("index" + this.getID());
		// resultSection.setSize("100%", "100%");
		resultSection.setHeight100();
		this.setPane(resultSection);

		VLayout section2 = new VLayout();
		section2.setHeight100();

		indexBar = new ToolStrip();
		indexBar.setWidth100();
		indexBar.setHeight(25);

		navigation = new NavigationToolStrip();
		//navigation.setInfos("t=<b>0</b>, v=<b>0</b>, fmin=<b>0</b>, fmax=<b>0</b>");
		navigation.addExportClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ProcessingWindow.show(Services.getI18n().exporting());
				ExecTimer.start();
				Services.getVocabularyService().export(form.getParams(), new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
					}

					@Override
					public void onSuccess(String result) {
						ProcessingWindow.hide();
						if (result == null) {
							TxmDialog.severe(Services.getI18n().internalError()+ " result is null");
						} else if (result.startsWith("error")) {
							TxmDialog.severe(result);
						} else if (result.length() > 0) {
							Window.open(result, "", "");
							TxmDialog.exportWarning();
						}
					}
				});
			}
		});
		navigation.addPreviousClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int oldstart = startindex;
				if (result == null) {
					return;
				}
				if (startindex <= 0) {
					return;
				}
				startindex = startindex - getNbResultPerPage();
				if (oldstart != startindex) {
					form.validate();
				}

			}
		});
		navigation.addFirstClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int oldstart = startindex;
				if (result == null) {
					return;
				}
				startindex = 0;
				if (oldstart != startindex) {
					form.validate();
				}

			}
		});
		navigation.addLastClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int oldstart = startindex;
				if (result == null) {
					return;
				}
				startindex = result.getTotalOccurrences() - getNbResultPerPage();
				if (startindex < 0) {
					startindex = 0;
				}
				if (oldstart != startindex) {
					form.validate();
				}

			}
		});
		navigation.addNextClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int oldstart = startindex;
				if (result == null) {
					return;
				}
				if (startindex >= (result.getTotalOccurrences() - getNbResultPerPage())) {
					return;
				}
				startindex = startindex + getNbResultPerPage();
				if (oldstart != startindex) {
					form.validate();
				}

			}
		});
		//navigation.updateLabel("<b>" + startindex + "-" + endindex + "</b>/" + totalpages);
		navigation.addToToolstrip(indexBar);

		section2.setMembers(resultList, indexBar);
		resultSection.setMembers(form, section2);
		
		this.setParameters(parameters);

		Services.getVocabularyService().getColumnTitles(itemPath, new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.severe(Services.getI18n().internalError());
			}

			@Override
			public void onSuccess(ArrayList<String> result) {
				if (result == null) {
					TxmDialog.severe(Services.getI18n().internalError());
				} else {
					colnames = result;
					initColumns();
					
					form.validate();
				}
			}
		});
		
	}

	ListGridField keywordField;

	protected void initColumns() {
		keywordField = new ListGridField("KEYWORD", Services.getI18n().indexKeyword());
		keywordField.setWidth(100);
		listGridFields.add(keywordField);
		if (colnames.size() > 1) {
			for (int i = 0; i < colnames.size(); i++) {
				ListGridField f = new ListGridField("F" + i, colnames.get(i));
				f.setWidth(45);
				listGridFields.add(f);
			}
		} else {
			ListGridField f = new ListGridField("F1", Services.getI18n().indexFrequency());
			f.setWidth(45);
			listGridFields.add(f);
		}

		resultList.setShowRowNumbers(true);
		resultList.setRowNumberStart(1);
		resultList.setFields(listGridFields.toArray(new ListGridField[] {}));
		resultList.setShowEmptyMessage(false);
		resultList.setCanSort(false);
		resultList.setCanSelectText(true);
		resultList.setCanFreezeFields(false);
		resultList.setCanGroupBy(false);
		resultList.setCanAutoFitFields(false);
		resultList.setCanPickFields(false);

		resultList.addCellDoubleClickHandler(new CellDoubleClickHandler() {
			@Override
			public void onCellDoubleClick(CellDoubleClickEvent event) {
				String value = event.getRecord().getAttributeAsString("KEYWORD");
				form.sendToConcordances(itemPath, value);
			}
		});
	}

	public void fillList(VocabularyResult result) {
		this.result = result;
		String title = "";
		for (String p : form.getParams().getProperties()) {
			title += ", "+p;
		}
		
		resultList.setShowEmptyMessage(true);
		resultList.setFieldTitle(keywordField.getName(), title.substring(2));
		for (Record r : resultList.getRecords()) {
			resultList.removeData(r);
		}
		for (String entry : result.getKeys()) {
			ListGridRecord record = new ListGridRecord();
			record.setAttribute("KEYWORD", entry);
			int[] freqs = result.get(entry);
			if (freqs.length == 1) {
				record.setAttribute("F1", freqs[0]);
			} else {
				for (int i = 0 ; i < freqs.length ; i++) {
					record.setAttribute("F"+i, freqs[i]);
				}
			}
			resultList.addData(record);
		}	

		if (lexicon) {
			this.setTitle(Services.getI18n().dictionnary(form.getParams().getCorpusPath()));
		} else {
			this.setTitle(Services.getI18n().index(form.getParams().getCorpusPath() + ":" + form.getParams().getQuery()));
		}

		int nbForm = result.getTotalOccurrences();
		int nbOcc = result.getSumFrequency();
		String message = Services.getI18n().formsAndOccs(nbForm, nbOcc);
		if (nbForm <= 1 && nbOcc <= 1) {
			message = Services.getI18n().formAndOcc(nbForm, nbOcc);
		} else if (nbForm <= 1) {
			message = Services.getI18n().formAndOccs(nbForm, nbOcc);
		}
		TXMWEB.getConsole().addMessage(message+ExecTimer.stop());
		navigation.updateInfos(message);
		
		startindex = result.getParams().startindex;
		totalpages = result.getTotalOccurrences();
		endindex = startindex+ result.values().size();//.getParams().filterNbPerPage;
		navigation.updateLabel("<b>" + (startindex+1) + "-" + (endindex) + "</b>/"+ (totalpages));
		
		resultList.setRowNumberStart(startindex+1);
		
		//resultList.autoFitFields(listGridFields.toArray(new ListGridField[]{}));
		//resultList.autoFitFields();
	}

	public int getNbResultPerPage() {
		return navigation.getPagination();
	}

	public void setQuery(String query) {
		this.form.setQuery(query);
	}
	

	public void setProperties(String properties) {
		this.form.setProperties(properties);
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub
	}
}
