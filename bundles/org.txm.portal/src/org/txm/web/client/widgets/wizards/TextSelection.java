package org.txm.web.client.widgets.wizards;

import org.txm.web.client.services.Services;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;

public class TextSelection extends TxmWizard{
	
	public TextSelection(String itemPath)
	{
		this.setTitle(Services.getI18n().textselection_title(itemPath.substring(1)));//remove the "/"
		//3-4 pages
		WizardPage createPage = createPage();
		WizardPage selectPage = selectPage();
		WizardPage finalizePage = finalizePage();
		
		this.addPage(createPage);
		this.addPage(selectPage);
		this.addPage(finalizePage);
		
		this.init();
	}

	private WizardPage finalizePage() {
		WizardPage page = new WizardPage();
		//show table
		return page;
	}

	private WizardPage selectPage() {
		WizardPage page = new WizardPage();
		//show critera
		//showtable
		return page;
	}

	private WizardPage createPage() {
		//show 3 creation methods
		
		WizardPage page = new WizardPage();
		
		final DynamicForm form = new DynamicForm();  
		form.setSize("100%", "100%");
		
		RadioGroupItem radioGroupItem = new RadioGroupItem("Creation");   
		radioGroupItem.setValueMap("Créer le corpus", "Créer le corpus à partir de", "Modif ier le corpus");
		radioGroupItem.addChangeHandler(new ChangeHandler(){
			@Override
			public void onChange(ChangeEvent event) {
				String creation = form.getValueAsString("Creation");
				System.out.println("creation method: "+creation);
			}
		});
		
		form.setFields(radioGroupItem);
		page.setMembers(form);
		
		return page;
	}
}
