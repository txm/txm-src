package org.txm.web.client.widgets.tabs.textselection;

import java.util.List;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.fields.DataSourceBooleanField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class StructuresDS extends DataSource {

	public StructuresDS(String id, List<String> values) {

		setID(id);

		DataSourceIntegerField pkField = new DataSourceIntegerField("pk");
		pkField.setHidden(true);
		pkField.setPrimaryKey(true);

		DataSourceBooleanField keepField = new DataSourceBooleanField("keep", " ");

		DataSourceTextField valueField = new DataSourceTextField("value", "valeur");

		DataSourceIntegerField ntextField = new DataSourceIntegerField("ntext");

		DataSourceIntegerField nwordField = new DataSourceIntegerField("nword");

		setFields(pkField, keepField, valueField, ntextField, nwordField);
		this.setClientOnly(true);
		fillData(values);
	}

	public void fillData(List<String> values)
	{
		for(int i = 0 ; i < values.size() ; i++)
		{	
			Record r = new Record();
			r.setAttribute("pk",i);
			r.setAttribute("keep",true);
			r.setAttribute("value",values.get(i));
			r.setAttribute("ntext",0);
			r.setAttribute("nword",0);

			this.addData(r);
		}
	}
}

