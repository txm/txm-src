package org.txm.web.client.widgets.tabs.textselection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.txm.web.client.ExecTimer;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.CenterPanel;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.forms.CreateSelection;
import org.txm.web.client.widgets.tabs.HTMLTab;
import org.txm.web.client.widgets.tabs.TxmCenterTab;
import org.txm.web.shared.Keys;
import org.txm.web.shared.MetadataInfos;
import org.txm.web.shared.SelectionInfos;
import org.txm.web.shared.TextSelectionResult;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.SelectionType;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.tree.Tree;
import com.smartgwt.client.widgets.tree.TreeNode;

public class TextSelectionTab extends TxmCenterTab{

	CriteraPanel critera;
	TextTable table;
	TextStats statsPanel;
	Button statsBtn;
	Button synthBtn;

	String subcorpusname = "";

	public ArrayList<String> textids;
	public HashMap<String, HashMap<String, String>> valuesPerText;
	public HashMap<String, ArrayList<String>> valuesPerProperty;
	public HashMap<String, Integer> numberOfWordsPerText;

	public ArrayList<String> properties;
	public HashMap<String, MetadataInfos> propertiesInfos;
	public ArrayList<String> displayProperties;
	public ArrayList<String> criteriaProperties;

	public ArrayList<String> groups;
	public HashMap<String, MetadataInfos> groupsInfos;
	public HashMap<String, ArrayList<String>> groupsProperties;
	protected ArrayList<String> displayGroups;
	protected ArrayList<String> criteriaGroups;

	public ArrayList<Record> selectedTexts = new ArrayList<Record>();
	public ArrayList<String> selectedTextsIds = new ArrayList<String>();
	public HashMap<String, HashMap<String, Integer>> numberOfSelectedTextPerProperty = new HashMap<String, HashMap<String, Integer>>();
	public HashMap<String, HashMap<String, Integer>> numberOfWordPerSelectedTextPerProperty = new HashMap<String, HashMap<String, Integer>>();

	private String itemPath;

	public HashMap<String, String> biblios;
	protected boolean getSelectionResult;
	private ListGridRecord theRecord;

	public void setTitle(String title) {
		super.setTitle(Canvas.imgHTML("icons/functions/Base.png", 16, 16)+"&nbsp;"+"<span style=\"vertical-align:-2px;\">"+title+"</span>");
	}

	/**
	 * 
	 * @param selectedItemPath
	 * @param theRecord
	 */
	public TextSelectionTab(Map<String, String> parameters, ListGridRecord theRecord) {
		// get the parent path. If the item is a Maincorpus, we get its path
		this.theRecord = theRecord;
		
		final String selectedItemPath = parameters.get(Keys.PATH);
		
		VLayout mainLayout = new VLayout();
		mainLayout.setSize("100%", "100%");

		ToolStrip btnBar = new ToolStrip();
		btnBar.setWidth100();
		btnBar.setHeight(25);

		final HLayout layout = new HLayout();
		layout.setSize("100%", "100%");

		mainLayout.setMembers(btnBar, layout);

		Button resetBtn = new Button(Services.getI18n().razButton());
		resetBtn.setPrompt(Services.getI18n().unselectAllCriteria());
		resetBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ProcessingWindow.show();
				//System.out.println("show");
				for (Selector s : critera.selectors) {
					s.raz();					
				}
				critera.updateSelectedTexts();
				ProcessingWindow.hide();
				//System.out.println("hide");
			}
		});

		/*Button allBtn = new Button("Tout");
		allBtn.setPrompt(Services.getI18n().selectAllCriteria());
		allBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ProcessingWindow.show();
				for (Selector s2 : critera.selectors) {
					s2.all();
				}
				critera.updateSelectedTexts();
				ProcessingWindow.hide();
			}
		});
		*/

		//		Button refreshBtn = new Button(Services.getI18n().refreshButton());
		//		refreshBtn.addClickHandler(new ClickHandler() {
		//			@Override
		//			public void onClick(ClickEvent event) {
		//				ProcessingWindow.show();
		//				critera.updateSelectedTexts();
		//				ProcessingWindow.hide();
		//			}
		//		});

		statsBtn = new Button("Stats");
		statsBtn.setPrompt(Services.getI18n().openStatsPanel());
		statsBtn.setActionType(SelectionType.CHECKBOX);
		statsBtn.setSelected(false);
		statsBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (statsBtn.getSelected()) {
					layout.showMember(statsPanel);
					statsPanel.refresh();
				} else
					layout.hideMember(statsPanel);
			}
		});

		Button buildBtn = new Button(Services.getI18n().validateButton());
		buildBtn.setPrompt(Services.getI18n().finalizeSelection());
		buildBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (selectedTexts.size() == 0) {
					TxmDialog.info(Services.getI18n().noTextSelected());
					return;
				}

				CreateSelection dialog = new CreateSelection(TextSelectionTab.this);
				dialog.show();
			}
		});

		//btnBar.addMember(resetBtn);
		//btnBar.addMember(allBtn);
		btnBar.addMember(statsBtn);
		btnBar.addFill();
		btnBar.addMember(buildBtn);

		// get the MainCorpus of the selected item
		int idx = selectedItemPath.substring(1).indexOf("/");
		String result = selectedItemPath;
		if (idx > 0) {
			result = selectedItemPath.substring(0, idx+1);
		}

		getSelectionResult = !result.equals(selectedItemPath);

		this.itemPath = result;
		Tree tree = TXMWEB.getSideNavTree().getTree();
		this.theRecord = tree.find(result);
		//System.out.println("Selection path: "+result+", record: "+this.theRecord.getAttribute("path2"));
		TextSelectionTab.this.setTitle(Services.getI18n().selection(itemPath.substring(1)));
		ExecTimer.start();
		Services.getTextSelectionService().getAllStuff(itemPath, new AsyncCallback<TextSelectionResult>() {
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
			}

			@Override
			public void onSuccess(TextSelectionResult result) {
				if (result == null) {
					TxmDialog.severe(Services.getI18n().internalError());
					return;
				}
				//result.dump();
				textids = result.textids;
				numberOfWordsPerText = result.numberOfWordsPerText;
				valuesPerText = result.valuesPerText;

				properties = result.properties;
				propertiesInfos = result.propertiesInfos;
				displayProperties = result.displayProperties;
				criteriaProperties = result.selectionProperties;
				valuesPerProperty = result.valuesPerProperties;

				groups = result.groups;
				groupsProperties = result.groupsProperties;
				groupsInfos = result.groupsInfos;
				displayGroups = result.displayGroups;
				criteriaGroups = result.selectionGroups;

				biblios = result.biblios;

				for (String prop : properties) {
					numberOfSelectedTextPerProperty.put(prop, new HashMap<String, Integer>());
					numberOfWordPerSelectedTextPerProperty.put(prop, new HashMap<String, Integer>());
				}

				for (String prop : groups) {
					numberOfSelectedTextPerProperty.put(prop, new HashMap<String, Integer>());
					numberOfWordPerSelectedTextPerProperty.put(prop, new HashMap<String, Integer>());
				}

				table = new TextTable(TextSelectionTab.this, critera);
				critera = new CriteraPanel(itemPath, TextSelectionTab.this, result);
				statsPanel = new TextStats(table, TextSelectionTab.this, result);
				statsPanel.setTitle(Services.getI18n().statPanel());

				resetTextCounts(); // init counts

				layout.setMembers(critera, table, statsPanel);
				layout.hideMember(statsPanel);

				if (getSelectionResult) {
					System.out.println("Get the selection result of "+selectedItemPath);
					Services.getCorpusActionService().getSelectionInfos(selectedItemPath, new AsyncCallback<SelectionInfos>() {

						@Override
						public void onFailure(Throwable caught) {
							ProcessingWindow.hide();
						}

						@Override
						public void onSuccess(SelectionInfos result) {
							if (result == null) {
								System.out.println("no infos for "+selectedItemPath);
								return;
							}

							System.out.println("set UI with: "+result.texts+result.critera);
							for (Selector selector : critera.selectors) {
								if (result.critera.containsKey(selector.property) && selector.records != null) {
									ArrayList<String> values = result.critera.get(selector.property);
									for (Record r : selector.records)
										if (values.contains(r.getAttribute("value")))
											r.setAttribute("keep", true);
								}
							}

							for (Record r : table.records) {
								if (result.texts.contains(r.getAttribute("id")))
									r.setAttribute("keep", true);
							}

							table.updateTextSelection();
							table.updateTextCounts();
							table.grid.refreshFields();

							critera.refreshSelectorsCounts();
							for (Selector selector : critera.selectors) {
								if (selector.records != null) {
									selector.updatePropertyCounts();
									selector.updateTitle();
								}
							}
						}
					});
				}

				ProcessingWindow.hide();
			}
		});

		this.setPane(mainLayout);
	}

	public boolean finalizeSelection(String value, final boolean showRecap, final Boolean showN, final Boolean showT, final Boolean showHuman) {
		selectedTextsIds.clear();
		for (Record r : selectedTexts)
			selectedTextsIds.add(r.getAttribute("id"));
				if (value.trim().length() > 0)
					subcorpusname = value.trim();
				if (subcorpusname.length() == 0) {
					TxmDialog.warning(Services.getI18n().corpusNameEmpty());
					return false;
				}

				if (subcorpusname.contains("/")) {
					TxmDialog.warning(Services.getI18n().corpusNameFormat(subcorpusname));
					return false;
				}

				if (TXMWEB.getSideNavTree().getTree().find(itemPath+"/"+subcorpusname) != null) {
					TxmDialog.warning(Services.getI18n().corpusNameUsed(subcorpusname));
					return false;
				}

				TXMWEB.getConsole().addMessage(Services.getI18n().createSubcorpus());

				//build the selection infos, will be used to build a clone selection
				SelectionInfos infos = new SelectionInfos();
				infos.texts = selectedTextsIds;
				infos.critera = new HashMap<String, ArrayList<String>>();
				for (Selector crit : this.critera.selectors) {
					ArrayList<String> list = crit.getSelectedPropertyValues();
					if (list.size() == 0) continue;
					infos.critera.put(crit.property, list);
				}

				Services.getCorpusActionService().createSubcorpus(itemPath, subcorpusname, infos, new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						if (caught.getMessage().equals("This subcorpus already exists"))
							TxmDialog.warning(Services.getI18n().corpusNameUsed(subcorpusname));
						else
							TxmDialog.severe(Services.getI18n().error_while_subcorpus_creation(caught));	
					}

					@Override
					public void onSuccess(String result) {
						ProcessingWindow.hide();
						if (result ==null) {
							TxmDialog.severe(Services.getI18n().internalError());
						}  else if ("user not saved".equals(result)) {
							TxmDialog.severe("Error: could not save user data. Please contact admin.");
						} else if ("workspace not saved".equals(result)) {
							TxmDialog.severe("Error: could not save workspace data. Please contact admin.");
						} else if (result.endsWith(":"+subcorpusname)) {
							String ns = result.substring(0, result.indexOf(":"));
							Tree tree = TXMWEB.getSideNavTree().getTree();
							TreeNode newChild;
							if (ns != null && ns.length() > 0)
								newChild = new TreeNode(ns+":"+subcorpusname);
							else
								newChild = new TreeNode(subcorpusname);

							String path = theRecord.getAttribute("path")+"/"+ns+":"+subcorpusname;
							String path2 = subcorpusname;
							newChild.setAttribute("path", path);
							newChild.setAttribute("path2", path2);

							newChild.setAttribute("ns", ns);
							newChild.setAttribute("id", subcorpusname);
							newChild.setAttribute("type", "subcorpus");
							newChild.setIcon("icons/subcorpus.png");
							tree.add(newChild, itemPath);
							TXMWEB.getSideNavTree().refreshFields();
							TXMWEB.getConsole().addMessage(Services.getI18n().corpusCreated());
							tree.openFolder(tree.find(itemPath));
							if (showRecap)
								doSelectionSynthese(subcorpusname, showN, showT, showHuman);
						} else {
							if (result.equals("exists"))
								TxmDialog.severe(Services.getI18n().subcorpusExists());
							else
								TxmDialog.severe(result);
						}
					}
				});
				return true;
	}

	private void doSelectionSynthese(String subcorpusname, Boolean showN, Boolean showT, Boolean showHuman) {
		ProcessingWindow.show(Services.getI18n().openingSelectionSynthese());
		StringBuffer toshow = new StringBuffer();

		// get critera
		toshow.append("<h1>"+Services.getI18n().summary(subcorpusname)+"</h1>");
		toshow.append("<h2>"+Services.getI18n().criteria()+"</h2>");
		for (int index = 0 ; index < this.critera.selectors.size() ; index++) {
			Selector selector = this.critera.selectors.get(index);
			selector.loadData();
			//if (selector.getSectionStack().sectionIsExpanded(index)) {
			if (selector instanceof GroupedDateSelector) {
				GroupedDateSelector dselector = (GroupedDateSelector) selector;
				toshow.append("<h3>"+Services.getI18n().property()+": "+dselector.getPropertyName()+"</h3>");
				toshow.append("<p>"+Services.getI18n().fromXtoY(dselector.getFrom(), dselector.getTo())+"</p>");
			} else if (selector instanceof DateSelector) {
				DateSelector dselector = (DateSelector) selector;
				toshow.append("<h3>"+Services.getI18n().property()+": "+dselector.getPropertyName()+"</h3>");
				toshow.append("<p>"+Services.getI18n().fromXtoY(dselector.getFrom(), dselector.getTo())+"</p>");
			} else {
				toshow.append("<table>");
				// header line
				toshow.append("<tr><th>"+Services.getI18n().property()+" "+selector.getPropertyName()+"</th>");

				if (showN) {
					toshow.append("<th>n</th>");
					toshow.append("<th>N</th>");
					toshow.append("<th>%n</th>");
				}
				if (showT) {
					toshow.append("<th>t</th>");
					toshow.append("<th>T</th>");
					toshow.append("<th>%t</th>");
				}
				toshow.append("</tr>");

				int wTotal = 0;
				int tTotal = 0;

				for (ListGridRecord r : selector.records) {
					tTotal += r.getAttributeAsInt("t");
					wTotal += r.getAttributeAsInt("n");
				}
				// values
				for (ListGridRecord r : selector.records) {
					toshow.append("<tr>");
					toshow.append("<td>"+r.getAttribute("value")+"</td>");

					int textCount = r.getAttributeAsInt("t");
					int wordCount = r.getAttributeAsInt("n");

					if (showN) {
						toshow.append("<td>"+wordCount+"</td>");
						toshow.append("<td>"+r.getAttributeAsInt("N")+"</td>");
						toshow.append("<td>"+((100*wordCount)/wTotal)+"</td>");
					}
					
					if (showT) {
						toshow.append("<td>"+textCount+"</td>");
						toshow.append("<td>"+r.getAttributeAsInt("T")+"</td>");
						toshow.append("<td>"+((100*textCount)/tTotal)+"</td>");
					}
					toshow.append("</tr>");
				}
				toshow.append("</table>");
			}
		}
		toshow.append("<br/>");

		//get text list
		toshow.append("<h2>"+Services.getI18n().selected_texts()+"</h2>");
		toshow.append("<table>");
		toshow.append("<tr>");
		toshow.append("<th>"+Services.getI18n().text()+"</th>");
		for (String colname : table.getVisibleCols()) {
			toshow.append("<th>"+colname+"</th>");
		}
		toshow.append("</tr>");

		int no = 1;
		for (ListGridRecord r : this.table.records) {
			toshow.append("<tr>");
			if (r.getAttribute("keep").equals("true")) {
				toshow.append("<td>"+(no++)+"</td>");
				for (String colname : table.getVisibleCols())
					toshow.append("<td>"+r.getAttribute(colname)+"</td>");
			}
			toshow.append("</tr>");
		}
		toshow.append("</table>");

		//get stats
		//		if (isStatsEnable() && false) {
		//			toshow.append("<h2>Statistics</h2>");
		//
		//			for (StatPanel statpanel : this.statsPanel.stats) {
		//				toshow.append(" "+statpanel.property+" : <br/>");
		//				for (Progressbar bar : statpanel.bars) {
		//					toshow.append("  "+bar.getTitle()+"="+bar.getPercentDone()+"%");
		//				}
		//				toshow.append("<br/>");
		//			}
		//		}

		//System.out.println(toshow);
		CenterPanel center = TXMWEB.getCenterPanel();
		HTMLTab newtab = new HTMLTab(Services.getI18n().summary(subcorpusname));
		newtab.setContent(toshow.toString());
		newtab.setCanClose(true);
		center.addTab(newtab);
		center.selectTab(center.getTabs().length - 1);	
		
		ProcessingWindow.hide();
	}

	/**
	 * 
	 * @param property
	 * @param value
	 * @return the number of words for a text property value
	 * @throws CqiClientException
	 */
	public int getNumberOfWord(String property, String value) {
		int nb = 0;
		for (String text : valuesPerText.keySet()) {
			if (valuesPerText.get(text).get(property).equals(value))
				nb+= numberOfWordsPerText.get(text);
		}
		return nb;
	}

	/**
	 * 
	 * @param property
	 * @param value
	 * @return
	 * @throws UnexpectedAnswerException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public int getTextNumber(String property, String value) {
		int nb = 0;
		for (String text : valuesPerText.keySet()) {
			if (valuesPerText.get(text).get(property).equals(value))
				nb++;
		}
		return nb;
	}

	public boolean isStatsEnable() {
		return statsBtn.isSelected();
	}

	public void resetTextCounts() {

		if (table != null)
			for (Record record : table.records) {
				for (String prop : properties) {
					String value = record.getAttribute(prop);
					numberOfSelectedTextPerProperty.get(prop).put(value, 0);
					numberOfWordPerSelectedTextPerProperty.get(prop).put(value, 0);
				}
			}
		/*for(String prop : properties)
		{
			List<String> values = valuesPerProperty.get(prop);
			for(String value : values)
			{
				numberOfSelectedTextPerProperty.get(prop).put(value, 0);
				numberOfWordPerSelectedTextPerProperty.get(prop).put(value, 0);
			}
		}*/
	}

	public String getItemPath() {
		return itemPath;
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub
		
	}
}
