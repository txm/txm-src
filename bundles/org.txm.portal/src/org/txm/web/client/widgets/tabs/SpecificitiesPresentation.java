package org.txm.web.client.widgets.tabs;

import java.util.ArrayList;
import java.util.Map;

import org.txm.web.shared.SpecificitiesResultLine;

public interface SpecificitiesPresentation {
	
	public void fillList(Map<String, ArrayList<SpecificitiesResultLine>> result, String mode, Number nbOrThreshold);
	
	public void hideField(String colName);
	
	public void showField(String colName);
	
	public boolean isFilled();
	
}
