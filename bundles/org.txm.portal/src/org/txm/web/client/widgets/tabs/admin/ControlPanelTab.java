package org.txm.web.client.widgets.tabs.admin;

import java.util.ArrayList;
import java.util.Arrays;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.EnhancedListGrid;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.shared.ServerState;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.FilterEditorSubmitEvent;
import com.smartgwt.client.widgets.grid.events.FilterEditorSubmitHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

/**
 * Allow admin to :
 * 	start/stop maintenance
 * 	restart toolbox
 * 	reload server options = edit Txmconfig file
 * @author mdecorde
 *
 */
public class ControlPanelTab extends Tab {

	ServerState serverState;

	DynamicForm content = new DynamicForm();

	ButtonItem maintenanceBtn;
	ButtonItem restartToolboxBtn;
	ButtonItem reloadServerParameters;
	CheckboxItem mailCheck = new CheckboxItem();  
	CheckboxItem inscriptionCheck = new CheckboxItem();
	TextAreaItem currentUsers = new TextAreaItem();

	EnhancedListGrid logsTable = new EnhancedListGrid();

	ListGridRecord[] srcrecords;

	private AdminTab adminTab;

	public ControlPanelTab(AdminTab adminTab) {
		this.adminTab = adminTab;

		this.setTitle(Services.getI18n().controlPanel());

		logsTable.setWidth100();  
		logsTable.setShowRowNumbers(true);
		logsTable.setHeight(300);  
		logsTable.setShowAllRecords(true); 
		logsTable.setShowFilterEditor(true); 
		Criteria criteria = new Criteria();
		criteria.addCriteria("action", "login|logout");
		criteria.addCriteria("login", "!admin");
		logsTable.setCriteria(criteria);

		logsTable.setFilterOnKeypress(false);
		logsTable.addFilterEditorSubmitHandler(new FilterEditorSubmitHandler() {

			@Override
			public void onFilterEditorSubmit(FilterEditorSubmitEvent event) {
				event.cancel();
				ProcessingWindow.show("Filtering records...");
				ArrayList<ListGridRecord> records = new ArrayList<ListGridRecord>(Arrays.asList(srcrecords));
				if (srcrecords == null || srcrecords.length == 0) {
					return;
				}

				String date = logsTable.getFilterEditorCriteria().getAttribute("date");
				boolean excluddate = false;
				if(date != null && date.startsWith("!")) {
					date = date.substring(1);
					excluddate = true;
				}
				String hour = logsTable.getFilterEditorCriteria().getAttribute("hour");
				boolean excludhour = false;
				if(hour != null && hour.startsWith("!")) {
					hour = hour.substring(1);
					excludhour = true;
				}
				String session = logsTable.getFilterEditorCriteria().getAttribute("session");
				boolean excludsession = false;
				if(session != null && session.startsWith("!")) {
					session = session.substring(1);
					excludsession = true;
				}
				String address = logsTable.getFilterEditorCriteria().getAttribute("address");
				boolean excludaddress = false;
				if(address != null && address.startsWith("!")) {
					address = address.substring(1);
					excludaddress = true;
				}
				String login = logsTable.getFilterEditorCriteria().getAttribute("login");
				boolean excludlogin = false;
				if(login != null && login.startsWith("!")) {
					login = login.substring(1);
					excludlogin = true;
				}
				String level = logsTable.getFilterEditorCriteria().getAttribute("level");
				boolean excludlevel = false;
				if(level != null && level.startsWith("!")) {
					level = level.substring(1);
					excludlevel = true;
				}
				String action = logsTable.getFilterEditorCriteria().getAttribute("action");
				boolean excludaction = false;
				if(action != null && action.startsWith("!")) {
					action = action.substring(1);
					excludaction = true;
				}
				String params = logsTable.getFilterEditorCriteria().getAttribute("params");
				boolean excludparams = false;
				if(params != null && params.startsWith("!")) {
					params = params.substring(1);
					excludparams = true;
				}
				for(ListGridRecord r : srcrecords)
				{
					if(date != null && (!r.getAttribute("date").matches(date) ^ excluddate) ){
						records.remove(r);
						continue;
					}

					if(hour != null && (!r.getAttribute("hour").matches(hour)^ excludhour)){
						records.remove(r);
						continue;
					}
					if(session != null && (!r.getAttribute("session").matches(session) ^ excludsession)){
						records.remove(r);
						continue;
					}

					if(address != null && (!r.getAttribute("address").matches(address) ^ excludaddress)){
						records.remove(r);
						continue;
					}

					if(login != null && (!r.getAttribute("login").matches(login) ^ excludlogin)){
						records.remove(r);
						continue;
					}

					if(level != null && (!r.getAttribute("level").matches(level) ^ excludlevel)){
						records.remove(r);
						continue;
					}

					if(action != null && (!r.getAttribute("action").matches(action) ^ excludaction)){
						records.remove(r);
						continue;
					}

					if(params != null && (!r.getAttribute("params").matches(params) ^ excludparams)){
						records.remove(r);
						continue;
					}
				}
				logsTable.setRecords(records.toArray(new ListGridRecord[records.size()]));
				ProcessingWindow.hide();
			}
		});
		ListGridField dateField = new ListGridField("date", "date");
		ListGridField hourField = new ListGridField("hour", "hour");
		ListGridField sessionField = new ListGridField("session", "session");
		ListGridField addressField = new ListGridField("address", "address");
		ListGridField loginField = new ListGridField("login", "login");
		ListGridField levelField = new ListGridField("level", "level");
		ListGridField actionField = new ListGridField("action", "action");
		ListGridField paramsField = new ListGridField("params", "params");

		 
		logsTable.setFields(dateField, hourField, sessionField, addressField, 
				loginField, levelField, actionField, paramsField);

		DynamicForm logsForm = new DynamicForm();
		logsForm.setWidth100();
		logsForm.setNumCols(4);
		ButtonItem btn = new ButtonItem("update", "Update logs");
		final SpinnerItem spinner = new SpinnerItem("n", "N last logs");
		spinner.setDefaultValue(1000);
		spinner.setMax(100000);
		spinner.setMin(100);
		logsForm.setFields(spinner, btn);
		btn.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			@Override
			public void onClick(
					com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				ProcessingWindow.show("getting logs");
				Services.getAdminService().getLastLogs(Integer.parseInt(spinner.getValueAsString()), new AsyncCallback<ArrayList<ArrayList<String>>>() {

					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						TxmDialog.severe("Error while fetching last logs: "+caught.getMessage());
					}

					@Override
					public void onSuccess(ArrayList<ArrayList<String>> result) {
						ProcessingWindow.hide();
						srcrecords = new ListGridRecord[result.size()];
						int i = 0;
						for (ArrayList<String> line : result) {
							ListGridRecord record = new ListGridRecord();
							record.setAttribute("date", line.get(0));
							record.setAttribute("hour", line.get(1));
							record.setAttribute("session", line.get(2));
							record.setAttribute("address", line.get(3));
							record.setAttribute("login", line.get(4));
							record.setAttribute("level", line.get(5));
							record.setAttribute("action", line.get(6));
							String params = line.get(7);
							for (int ii = 8 ; ii < line.size() ; ii++) {
								params += " "+line.get(ii);
							}
							record.setAttribute("params", params);
							srcrecords[i++] = record;
						}
						logsTable.setRecords(srcrecords);
					}
				});
			}
		});


		mailCheck.setTitle("Mail disable");
		mailCheck.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				System.out.println("set nomail to "+mailCheck.getValueAsBoolean());
				Services.getAdminService().setMailEnable(mailCheck.getValueAsBoolean(), new AsyncCallback<Boolean>() {
					@Override
					public void onFailure(Throwable caught) {
						TxmDialog.severe("Internal error: "+caught);
					}

					@Override
					public void onSuccess(Boolean result) {
						TxmDialog.info("Mail state is "+result);
					}
				});
			}
		});
		inscriptionCheck.setTitle("Inscription disable");
		inscriptionCheck.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				System.out.println("set noinscription to "+inscriptionCheck.getValueAsBoolean());
				Services.getAdminService().setInscriptionEnable(inscriptionCheck.getValueAsBoolean(), new AsyncCallback<Boolean>() {
					@Override
					public void onFailure(Throwable caught) {
						TxmDialog.severe("Internal error: "+caught);
					}

					@Override
					public void onSuccess(Boolean result) {
						TxmDialog.info("Inscription state is "+result);
					}
				});
			}
		});
		currentUsers.setTitle("Users logegd in");

		maintenanceBtn = new ButtonItem("maintenance", "Start maintenance");
		maintenanceBtn.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			@Override
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {

				System.out.println("press maintenance btn: newstate="+!serverState.maintenance);
				ProcessingWindow.show("Setting maintenance to "+!serverState.maintenance);
				if(!serverState.maintenance)
				{
					Timer t1 = new Timer() {
						@Override
						public void run() {
							// launch 2nd timer
							Timer t2 = new Timer(){
								@Override
								public void run() {
									switchMaintenance();
								}
							};
							t2.schedule(60000);
							ContactUser.quickSendMessage("!!TXMWEB is closing in 1min... Please disconnect.", maintenanceBtn);
							TXMWEB.getConsole().addMessage("2nd message sent");
							maintenanceBtn.disable();
						}
					};
					t1.schedule(60000);
					ContactUser.quickSendMessage("!!TXMWEB is closing in 2min... Please disconnect.", maintenanceBtn);
					TXMWEB.getConsole().addMessage("1st message sent");
				}
				else
				{
					switchMaintenance();
				}
			}
		});

		restartToolboxBtn = new ButtonItem("restart", "Restart toolbox");
		restartToolboxBtn.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			@Override
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				System.out.println("press restart btn");
				if(serverState.maintenance)
				{
					ProcessingWindow.show("restarting toolbox...");
					Services.getAdminService().restartToolbox(new AsyncCallback<Boolean>() {
						@Override
						public void onFailure(Throwable caught) {
							ProcessingWindow.hide();
							TxmDialog.severe("Failed to restart toolbox: "+caught);
						}

						@Override
						public void onSuccess(Boolean result) {
							ProcessingWindow.hide();
							if (result) {
								TxmDialog.info("The toolbox has been restarted");
							} else {
								TxmDialog.severe("The toolbox could not been restarted");
							}
						}
					});
				}
			}
		});

		reloadServerParameters = new ButtonItem("reload", "Reload txmweb.conf parameters");
		reloadServerParameters.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			@Override
			public void onClick(
					com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				ProcessingWindow.show("Reloading 'txmweb.conf' parameters...");
				Services.getAdminService().reloadServerParameters(new AsyncCallback<Boolean>(){
					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						TxmDialog.severe("Failed to restart toolbox: "+caught);
					}

					@Override
					public void onSuccess(Boolean result) {
						ProcessingWindow.hide();
						if (result) {
							TxmDialog.info("The parameters have been reloaded");
						} else {
							TxmDialog.severe("The parameters are NOT reloaded");
						}
					}
				});
			}
		});
		
		// EDIT TXMWEB.CONF SECTION
		
		final TextAreaItem editTXMWEBCONF = new TextAreaItem();
		editTXMWEBCONF.setTitle("txmweb.conf file content");
		editTXMWEBCONF.setWidth("100%");
		editTXMWEBCONF.setHeight(200);
		
		ButtonItem loadTXMWEBCONFButton = new ButtonItem("Loadtxmweb", "Display txmweb.conf");
		loadTXMWEBCONFButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Services.getAdminService().getTXMWEBCONF(new AsyncCallback<String>() {
					@Override
					public void onSuccess(String result) {
						if (result != null) {
							editTXMWEBCONF.setValue(result);
						} else {
							editTXMWEBCONF.setValue("Error: Failed to get txmweb.conf content");
						}
					}

					@Override
					public void onFailure(Throwable caught) {
						TxmDialog.severe("Failed to get txmweb.conf content: "+caught);
					}
				});
			}
		});
		ButtonItem updateTXMWEBCONFButton = new ButtonItem("Updatetxmweb", "Update txmweb.conf");
		updateTXMWEBCONFButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (editTXMWEBCONF.getValueAsString().trim().length() > 0)
				Services.getAdminService().saveTXMWEBCONF(editTXMWEBCONF.getValueAsString(), new AsyncCallback<Boolean>() {
					@Override
					public void onSuccess(Boolean result) {
						if (result != null && result) {
							TxmDialog.severe("txmweb.conf saved. Please reload values.");
						} else {
							TxmDialog.severe("Failed to save txmweb.conf content (return false or null");
						}
					}

					@Override
					public void onFailure(Throwable caught) {
						TxmDialog.severe("Failed to save txmweb.conf content: "+caught);
					}
				});
			}
		});
		
		Services.getAdminService().getServerState(new AsyncCallback<ServerState>() {
			@Override
			public void onSuccess(ServerState result) {
				serverState = result;
				System.out.println("current maintenance: "+result);
				updateMaintenanceBtnTitle();
			}

			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.severe("Failed to get maintenance state: "+caught);
			}
		});
		
		final DynamicForm form = new DynamicForm();
		form.setItems(mailCheck, inscriptionCheck, currentUsers);
		
		//VLayout layoutHTMLSEction = createUploadHTMLSection();

		content.setWidth100();
		content.setItems(maintenanceBtn, restartToolboxBtn, reloadServerParameters, loadTXMWEBCONFButton, updateTXMWEBCONFButton, editTXMWEBCONF);
		VLayout layout = new VLayout();
		layout.setHeight100();
		layout.setWidth100();

		layout.setMembers(content, logsForm, logsTable);
		this.setPane(layout);
	}

	private VLayout createUploadHTMLSection() {
		VLayout layout = new VLayout();
		layout.setWidth100();
		layout.setAutoHeight();

		final FormPanel formPanel = new FormPanel();
		formPanel.setAction("/FileUpload/HTMLFile.html");

		// Because we're going to add a FileUpload widget, we'll need to set the
		// form to use the POST method, and multipart MIME encoding.
		formPanel.setEncoding(FormPanel.ENCODING_MULTIPART);
		formPanel.setMethod(FormPanel.METHOD_POST);
		formPanel.addSubmitCompleteHandler(new SubmitCompleteHandler() {
			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {
				String body = event.getResults();
				TxmDialog.info("result: "+body);
			}
		});

		VerticalPanel panel = new VerticalPanel();
		formPanel.setWidget(panel);

		FileUpload upload = new FileUpload();
		upload.setName("uploadFormElement");
		upload.setTitle("Upload a HTML File");
		panel.add(upload);

		Button uploadButton = new Button("Submit");
		uploadButton.addClickHandler(new com.google.gwt.event.dom.client.ClickHandler() {
			@Override
			public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
				formPanel.submit();
			}
		});
		
		panel.add(uploadButton);
		layout.addChild(formPanel);
		
		return layout;
	}

	private void switchMaintenance()
	{
		Services.getAdminService().setMaintenance(!serverState.maintenance, new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				TxmDialog.severe("Failed to set maintenance state to "+!serverState.maintenance);
			}

			@Override
			public void onSuccess(Boolean result) {
				System.out.println("maintenance result: "+result);
				ProcessingWindow.hide();
				serverState.maintenance = result;
				updateMaintenanceBtnTitle();
			}
		});
	}

	private void updateMaintenanceBtnTitle()
	{
		System.out.println(serverState) ;
		if(serverState.maintenance)
		{
			maintenanceBtn.setTitle("Stop maintenance");
			restartToolboxBtn.enable();
			//			mailCheck.enable();
			//			inscriptionCheck.enable();
			//			currentUsers.enable();
		}
		else
		{
			maintenanceBtn.setTitle("Start maintenance");
			restartToolboxBtn.disable();
			//			mailCheck.disable();
			//			inscriptionCheck.disable();
			//			currentUsers.disable();
		}
		//		mailCheck.setValue(serverState.mail);
		//		inscriptionCheck.setValue(serverState.inscription);
		//		String content ="";
		//		for(String user : serverState.currentUsers)
		//			content += user + "\n";
		//				currentUsers.setValue(content);
	}
}
