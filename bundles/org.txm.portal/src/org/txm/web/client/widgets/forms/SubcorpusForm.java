package org.txm.web.client.widgets.forms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.txm.web.client.ExecTimer;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.shared.Keys;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.ToolbarItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.tree.Tree;
import com.smartgwt.client.widgets.tree.TreeNode;

public class SubcorpusForm extends TxmWindow {

	private String itemPath = "";
	private List<String> values;
	private ListGridRecord theRecord;

	public SubcorpusForm(Map<String, String> parameters, ListGridRecord theRecord) {
		super(true, "");
		
		this.itemPath = parameters.get(Keys.PATH); 
		this.theRecord = theRecord;
		
		initializeFields(parameters);
		setParameters(parameters); // do it here because no Tab will do it
	}
	
	@Override
	public void _initializeFields(Map<String, String> parameters) {
		
		this.form = new DynamicForm();
		this.setTitle(Services.getI18n().createSubcorpus());
		this.form.setNumCols(3);

		TextItem nameField = new TextItem(Keys.NAME, Services.getI18n().subcorpusFormNameField()); // nom du sous-corpus à créer
		IButton validationButton = new IButton(Services.getI18n().createButton());
		validationButton.setPrompt(Services.getI18n().confirmSubcorpus());
		IButton cancelButton = new IButton(Services.getI18n().cancelButton());
		cancelButton.setPrompt(Services.getI18n().cancelAndcloseWindow());

		final CheckboxItem checkboxItem = new CheckboxItem();  
		checkboxItem.setTitle(Services.getI18n().selectAll());
		nameField.setColSpan(3);

		final SelectItem structSelection = new SelectStructureItem(Keys.STRUCTURE, Services.getI18n().structure()); // structure du corpus à utiliser. Valeur par défaut : Keys.TEXT
		structSelection.setColSpan(3);
		final SelectItem propertySelection = new SelectPropertyItem(Keys.PROPERTY, Services.getI18n().property()); // propriété de la structure à utiliser. Valeur par défaut : Keys.ID
		propertySelection.setColSpan(3);
		final OrderedPickListItem valueSelection = new OrderedPickListItem(Keys.VALUE); // valeur de la propriété de la structure à conserver 
		valueSelection.setTitle(Services.getI18n().values());
		valueSelection.setShowTitle(true);

		structSelection.setColSpan(2);

		structSelection.setShowAllOptions(true);
		Services.getCorpusActionService().getStructuralUnits(itemPath, new AsyncCallback<List<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.severe(Services.getI18n().failed_get_structuralunits(SubcorpusForm.this.itemPath, caught.getMessage()));
			}
			@Override
			public void onSuccess(List<String> result) {
				Collections.sort(result);
				structSelection.setValueMap(result.toArray(new String[result.size()]));
			}
		});

		propertySelection.setShowAllOptions(true);
		structSelection.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				Services.getCorpusActionService().getStructuralUnitProperties(SubcorpusForm.this.itemPath, (String) event.getValue(), new AsyncCallback<List<String>>() {
					@Override
					public void onFailure(Throwable caught) {
						TxmDialog.severe(Services.getI18n().failed_get_structuralunits(SubcorpusForm.this.itemPath, caught.getMessage()));
					}
					@Override
					public void onSuccess(List<String> result) {
						Collections.sort(result);
						propertySelection.setValueMap(result.toArray(new String[result.size()]));
						propertySelection.setValues(new String[0]);
						valueSelection.setDefaultValues(new ArrayList<String>());
					}
				});
			}
		});

		//		valueSelection.setMultiple(true);
		//		valueSelection.setMultipleAppearance(MultipleAppearance.PICKLIST);
		propertySelection.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				String su = (String) structSelection.getValue();
				String prop = (String) event.getValue();
				Services.getCorpusActionService().getStructPropsValues(SubcorpusForm.this.itemPath, su, prop, new AsyncCallback<List<String>>() {

					@Override
					public void onFailure(Throwable caught) {
						TxmDialog.severe(Services.getI18n().failed_get_wordproperties(SubcorpusForm.this.itemPath, caught.getMessage()));
					}
					@Override
					public void onSuccess(List<String> result) {
						Collections.sort(result);
						values = result;
						valueSelection.setValueMap(result.toArray(new String[result.size()]));
						valueSelection.setDefaultValues(new ArrayList<String>());
					}
				});
			}
		});

		form.setHeight100();
		form.setWidth100();

		cancelButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				destroy();
			}
		});
		validationButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//				name = form.getValueAsString(Keys.NAME);
				//				query = form.getValueAsString(Keys.QUERY);
				validate();
			}
		});

		checkboxItem.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {

				if (!checkboxItem.getValueAsBoolean()) {
					valueSelection.setDefaultValues(new ArrayList<String>());
				} else if (values != null) {
					valueSelection.setDefaultValues(values);
				}
			}
		});

		ToolbarItem toolbarItem = new ToolbarItem();
		toolbarItem.setButtons(cancelButton, validationButton);

		form.setItems(nameField, structSelection, propertySelection, valueSelection,checkboxItem, toolbarItem);
		
		this.addItem(form);
		//RootPanel.get().add(this);
		this.centerInPage();
		this.show();
		
	}

	private void validate() {
		
		final String name = form.getValueAsString(Keys.NAME);
		if (name == null) {
			TxmDialog.warning(Services.getI18n().name_mandatory());
			return;
		}
		if (name.contains("/") && name.contains(":")) {
			TxmDialog.warning(Services.getI18n().errorNameChar());
			return;
		}
		String structName = form.getValueAsString(Keys.STRUCTURE);
		String structProp = form.getValueAsString(Keys.PROPERTY);
		List<String> values = new ArrayList<String>();
		for (String s : form.getValueAsString(Keys.VALUE).split(", ")) {
			values.add(s);
		}
		ProcessingWindow.show(Services.getI18n().creatingSubcorpus());
		Services.getCorpusActionService().createSubcorpus(itemPath, name, structName, structProp, values, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				ProcessingWindow.hide();
				if (result == null) {
					TxmDialog.severe(Services.getI18n().internalError());
				} else if ("user not saved".equals(result)) {
					TxmDialog.severe("Error: could not save user data. Please contact admin.");
				} else if ("workspace not saved".equals(result)) {
					TxmDialog.severe("Error: could not save workspace data. Please contact admin.");
				} else if(!result.endsWith(":"+name)) {
					if (result != null && result.equals("exists")) {
						TxmDialog.severe(Services.getI18n().subcorpusExists());
					} else {
						TxmDialog.severe(result);
					}
				} else {
					String ns = result.substring(0, result.indexOf(":"));
					Tree tree = TXMWEB.getSideNavTree().getTree();
					TreeNode newChild;
					if (ns != null && ns.length() > 0) {
						newChild = new TreeNode(ns+":"+name);
					} else {
						newChild = new TreeNode(name);
					}

					String path = theRecord.getAttribute(Keys.PATH)+"/"+ns+":"+name;
					String path2 = name;
					newChild.setAttribute(Keys.PATH, path);
					newChild.setAttribute("path2", path2);

					newChild.setAttribute("ns", ns);
					newChild.setAttribute("id", name);
					newChild.setAttribute("type", "subcorpus");
					newChild.setIcon("icons/subcorpus.png");
					tree.add(newChild, itemPath);
					tree.openFolder(tree.find(itemPath));
					TXMWEB.getSideNavTree().refreshFields();
					destroy();
					TXMWEB.getConsole().addMessage(Services.getI18n().subcorpus_created()+ExecTimer.stop());
					ProcessingWindow.hide();
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.severe(Services.getI18n().internalError()+caught);	
			}
		});
	}
}
