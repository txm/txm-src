package org.txm.web.client.widgets.incrustation;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.CenterPanel;
import org.txm.web.client.widgets.tabs.HTMLTab;

import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.events.ClickEvent;

public class HelpButton extends Button{
	public static String ID = "help_target";

	private static void call()
	{
		CenterPanel center = TXMWEB.getCenterPanel();
		HTMLTab helpPage = center.getUniqTab(Services.getI18n().helpTab());
		
		if(helpPage == null)
			return;
		
		helpPage.setCanClose(true);
		center.addTab(helpPage);
		center.selectTab(helpPage);
	}
	
	public static Button getButton()
	{
		Button btn = new Button(Services.getI18n().helpButton());
		btn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				call();
			}
		});
		return btn;
	}
	
	public static Anchor getLink()
	{
		Anchor btn = new Anchor();
		btn.setHTML(Services.getI18n().helpButton());
		btn.addClickHandler(new com.google.gwt.event.dom.client.ClickHandler() {
			
			@Override
			public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
				call();
			}
		});
		return btn;
	}
	
	public static boolean incrust()
	{
		RootPanel target = RootPanel.get(ID);
		if (target != null)
		{
			target.clear();
			String type = target.getElement().getAttribute("type");
			if(type != null && type.equals("link"))
				target.add(getLink());
			else
				target.add(getButton());
			return true;
		}
		return false;
	}
}
