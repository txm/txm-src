package org.txm.web.client.widgets.tabs;

import java.util.Map;

import org.txm.web.client.ExecTimer;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.shared.Keys;
import org.txm.web.shared.PropertiesResult;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.VLayout;

public class PropertiesTab extends TxmCenterTab {

	private String itemPath;
	private ListGridRecord elem;

	public static NumberFormat formatter = NumberFormat.getFormat("###,###,##0.##;(#)");

	public PropertiesTab(ListGridRecord record, Map<String, String> parameters) {

		super(Canvas.imgHTML("icons/functions/Diagnostique.png", 16, 16)+"&nbsp;"+"<span style=\"vertical-align:-2px;\">"+parameters.get(Keys.PATH).substring(1)+"</span>");

		this.setName("PROPERTIES");
		this.itemPath = parameters.get(Keys.PATH);
		this.elem = record;

		final String property = Keys.WORD;
		ExecTimer.start();

		//		final String url = TXMWEB.BASEURL+"/html"+path+"/Properties.jsp";
		//		RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, url);
		//		try {
		//			rb.sendRequest(null, new RequestCallback(){
		//				@Override
		//				public void onResponseReceived(Request request, Response response) {
		//					int code = response.getStatusCode();
		//					if (code == Response.SC_NOT_FOUND) {

		Services.getPropertiesService().getTV(itemPath, property, new AsyncCallback<PropertiesResult>() {
			@Override
			public void onSuccess(PropertiesResult result) {

				if (result == null) {
					onFailure(new IllegalStateException("no content to show"));
				} else {

					VLayout content = new VLayout();
					content.setCanSelectText(true);
					content.setPadding(10);
					setPane(content);

					if (result.htmlContent != null && result.htmlContent.length() > 0) {
						content.setContents(result.htmlContent);
						return;
					}
					String message = "<h4>"+Services.getI18n().propertiesOf(itemPath.substring(1))+"</h4><br/>";
					String console_message = "";

					if (result.nWords != null && result.nWords >= 0) {
						String messageNumberOfWords = Services.getI18n().numberOfWordsAndPuncts(formatter.format(result.nWords));
						message += "<p>"+messageNumberOfWords+"</p>";
						console_message += messageNumberOfWords +" ; ";
					}

					if (result.nTexts != null && result.nTexts >= 0) {
						String messageNumberOfTexts = Services.getI18n().numberOfText(result.nTexts)+"\n";
						message += "<p>"+messageNumberOfTexts+"</p>";
						console_message += messageNumberOfTexts;
					}

					if (result.nParts != null && result.nParts >= 0) {
						String messageNumberOfParts = Services.getI18n().numberOfPart(result.nParts)+"\n";
						message += "<p>"+messageNumberOfParts+"</p>";
						console_message += messageNumberOfParts;
					}
					if (result.partSizes != null && result.parts != null && result.partSizes.size() >= 0 
							&& result.parts.size() > 0 && result.partSizes.size() == result.parts.size()) {

						int total = 0;
						for (int i : result.partSizes) if (total < i) total = i;

						message += "<div style=\"height: 100%; width: 50%; overflow: auto;\"><table style=\"width: 100%;border-right:2px solid rgba(0,0,0,0.5);\"><tr><th>Name</th><th>Size</th><th></th></tr>";
						for (int i = 0 ; i < result.parts.size() ; i++) {
							message += "<tr><td style=\"padding: 5px;\">"+result.parts.get(i)+"</td><td style=\"padding: 5px;text-align:right;\">"+result.partSizes.get(i)+"</td><td style=\"width: 100%; padding: 5px;\"><div style=\"background-color: blue; width: "+(100.0*result.partSizes.get(i) / total)+"%;\">&nbsp;&nbsp;&nbsp;</div></td></tr>";
						}
						message += "</table></div>";
					}

					if (result.properties != null && result.properties.size() > 0) {
						String messageNumberOfProperties = Services.getI18n().numberOfProperties(result.properties.size())+"\n";
						message += "<p>";
						message += messageNumberOfProperties;
						message += "<ul>";
						for (String p : result.properties) {
							message += "<li>"+p+"</li>";
						}
						message += "</ul>";
						message += "</p>";
					}

					if (result.structures != null && result.structures.size() > 0) {
						String messageNumberOfStructures = Services.getI18n().numberOfStructures(result.structures.size())+"\n";
						message += "<p>";
						message += messageNumberOfStructures;
						message += "<ul>";
						for (String p : result.structures) {
							message += "<li>"+p+"</li>";
						}
						message += "</ul>";
						message += "</p>";
					}

					content.setContents(message);
					TXMWEB.getConsole().addMessage(console_message);
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.severe(Services.getI18n().propertiesError(caught.getMessage()));
			}
		});
		//					} else {
		//						TxmHTMLPane pane = new TxmHTMLPane();
		//						pane.setPadding(10);
		//						pane.setContentsType(ContentsType.FRAGMENT);
		//						pane.setHeight100();
		//						pane.setWidth100();
		//						pane.setContentsURL(url);
		//						setPane(pane);
		//					}
		//				}
		//
		//				@Override
		//				public void onError(Request request, Throwable exception) {
		//					
		//				}
		//			});
		//		} catch (RequestException e) {
		//			// TODO Bloc catch généré automatiquement
		//			e.printStackTrace();
		//		}

		this.setParameters(parameters);
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub

	}
}
