package org.txm.web.client.widgets.tabs;

import java.util.Map;

import org.txm.web.client.widgets.ContextParaPanel;
import org.txm.web.client.widgets.forms.TxmForm;
import org.txm.web.shared.Keys;

import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.grid.ListGridRecord;


public class ContextParaTab extends TxmCenterTab {
	
	private ContextParaPanel content;
	
	public void setTitle(String title)
	{
		super.setTitle(Canvas.imgHTML("icons/functions/Contexts.png", 16, 16)+"&nbsp;"+"<span style=\"vertical-align:-2px;\">"+title+"</span>");
	}
	
	public ContextParaTab(Map<String, String> parameters, ListGridRecord elem) {
		this.setTitle(elem.getAttribute(Keys.PATH));
		
		content = new ContextParaPanel(elem, parameters.get(Keys.PATH));
		content.setTab(this);
		this.setPane(content);
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub
		
	}
	
	public TxmForm getForm() {
		return null;
	}
}
