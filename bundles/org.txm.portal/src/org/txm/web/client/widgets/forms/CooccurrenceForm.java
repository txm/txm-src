package org.txm.web.client.widgets.forms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.txm.web.client.ExecTimer;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.tabs.CooccurrencePresentation;
import org.txm.web.shared.CooccurrenceParam;
import org.txm.web.shared.CooccurrenceResult;
import org.txm.web.shared.Keys;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyUpEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyUpHandler;
import com.smartgwt.client.widgets.layout.HLayout;

public class CooccurrenceForm extends TxmForm{

	String itemPath;
	
	private DynamicForm queryForm = new DynamicForm();
	private DynamicForm contextForm = new DynamicForm();
	private CooccurrenceParam params = new CooccurrenceParam();
	private CooccurrencePresentation parentTab;

	private SelectItem selectStructureItem;

	private RadioGroupItem radioGroupItem;

	private CheckboxItem checkboxItem;

	public CooccurrenceForm(Map<String, String> parameters, CooccurrencePresentation parent){
		super();
		parentTab = parent;
		itemPath = parameters.get(Keys.PATH);
		
		initializeFields(parameters);
	}
	
	@Override
	public void _initializeFields(Map<String, String> parameters) {
		this.form = new DynamicForm();
		
		ProcessingWindow.show(Services.getI18n().openingCooccurrenceForm());
		params.setCorpusPath(itemPath);
		
		QueryItem queryField = new QueryItem(Keys.QUERY, Services.getI18n().queryField()); // requete de sélection du pivot
		queryField.setPrompt(Services.getI18n().queryPrompt());
		queryField.setWidth("100%");
		queryField.addKeyUpHandler(new KeyUpHandler() {

			@Override
			public void onKeyUp(KeyUpEvent event) {
				if ("Enter".equals(event.getKeyName())) {
					validate();
				}
			}
		});

		IButton validationButton = new IButton(Services.getI18n().searchButton());
		validationButton.setPrompt(Services.getI18n().startCooccurrences());
		validationButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				validate();
			}
		});

		IButton exportButton = new IButton(Services.getI18n().concExportButton());
		exportButton.setPrompt(Services.getI18n().startCooccurrences());
		exportButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (parentTab.isFilled()) {
					export();
				}
			}
		});
		queryForm.setWidth100();
		queryForm.setItems(queryField);

		//query pane
		HLayout section1 = new HLayout();
		section1.setHeight(35);
		section1.setWidth100();
		section1.setAlign(Alignment.CENTER);
		section1.setAlign(VerticalAlignment.CENTER);
		section1.setMargin(3);		
		section1.setMembers(queryForm, validationButton, exportButton);

		//cooccur. properties pane
		HLayout section2 = new HLayout();
		section2.setHeight(35);
		section2.setWidth100();
		final OrderedPickListItem selectPropertyItem = new OrderedPickListItem(Keys.PROPERTIES); // propriété des cooccurrents à afficher Valeur par défaut : Keys.WORD
		selectPropertyItem.setPrompt(Services.getI18n().propertyPrompt());
		selectPropertyItem.setTitle(Services.getI18n().cooccurPropertyLabel());
		selectPropertyItem.setMultiple(true);
		//selectPropertyItem.setMultipleAppearance(MultipleAppearance.PICKLIST);
		//sselectPropertyItem.setShowAllOptions(true);
		selectPropertyItem.setDefaultValue(Keys.WORD);
		//ask possible properties and fill gui component with result
		Services.getCorpusActionService().getWordProperties(itemPath, new AsyncCallback<List<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.warning(Services.getI18n().errorGetProperties(itemPath));
			}
			@Override
			public void onSuccess(List<String> result) {
				Collections.sort(result);	
				selectPropertyItem.setValueMap(result.toArray(new String[result.size()]));	
			}
		});

		SpinnerItem fminSpinner = new SpinnerItem(Keys.FMIN, Services.getI18n().fminSpinner()); // fréquence minimale du cooccurrent dans le corpus
		fminSpinner.setPrompt(Services.getI18n().fminPrompt());
		fminSpinner.setWriteStackedIcons(false);
		fminSpinner.setMin(0.0d);
		SpinnerItem cminSpinner = new SpinnerItem(Keys.CMIN, Services.getI18n().cminSpinner()); // fréquence minimale de cooccurrence avec le pivot
		cminSpinner.setPrompt(Services.getI18n().cofreqPrompt());
		cminSpinner.setWriteStackedIcons(false);
		cminSpinner.setMin(0.0d);
		SpinnerItem sMinSpinner = new FloatItem(Keys.SMIN, Services.getI18n().sminSpinner()); // valeur minimale de l'indice de spécificité
		sMinSpinner.setPrompt(Services.getI18n().scorePrompt());
		sMinSpinner.setWriteStackedIcons(false);
		sMinSpinner.setDecimalPad(2);
		sMinSpinner.setMin(0.0d);

		form.setWidth100();
		form.setNumCols(10);
		form.setItems(selectPropertyItem, fminSpinner, cminSpinner, sMinSpinner);
		section2.setMembers(form);

		//context pane
		HLayout section3 = new HLayout();

		section3.setHeight(35);
		section3.setWidth100();
		radioGroupItem = new RadioGroupItem(Keys.CONTEXT); // choix du contexte en fenêtre de mots ou en structures Choisir entre Keys.WORD et Keys.STRUCTURE. Valeur par défaut : Keys.WORD
		radioGroupItem.setVertical(false);
		radioGroupItem.setTitle(Services.getI18n().contextLabel());
		LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();
		values.put(Keys.WORD, Services.getI18n().wordRadio());
		values.put(Keys.STRUCTURE, Services.getI18n().structRadio());
		radioGroupItem.setValueMap(values);
		//radioGroupItem.setValueMap(Services.getI18n().wordRadio(), Services.getI18n().structRadio());
		radioGroupItem.setDefaultValue(Keys.WORD);
		selectStructureItem = new SelectStructureItem(Keys.STRUCTURE); // choix de la structure si le contexte est en structure
		//selectStructureItem.setTitle("");
		selectStructureItem.setShowTitle(false);
		Services.getCorpusActionService().getStructuralUnits(itemPath, new AsyncCallback<List<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.warning(Services.getI18n().errorGetStructures(itemPath));
			}
			@Override
			public void onSuccess(List<String> result) {
				Collections.sort(result);	
				selectStructureItem.setValueMap(result.toArray(new String[result.size()]));	
				if (result.size() > 0) { // set default value if any
					selectStructureItem.setValue(result.get(0));
				}
			}
		});
		SpinnerItem fromSpinner = new SpinnerItem(Keys.LEFT_CONTEXT, Services.getI18n().fromSpinner()); // taille du contexte gauche
		fromSpinner.setWriteStackedIcons(false);
		fromSpinner.setMin(0.0d);
		SpinnerItem toSpinner = new SpinnerItem(Keys.MIN_LEFT_CONTEXT, Services.getI18n().toSpinner()); // distance du début du contexte gauche au pivot
		toSpinner.setWriteStackedIcons(false);
		toSpinner.setMin(0.0d);
		SpinnerItem andFromSpinner = new SpinnerItem(Keys.MIN_RIGHT_CONTEXT, Services.getI18n().andFromSpinner());  // distance du début du contexte droit au pivot
		andFromSpinner.setWriteStackedIcons(false);
		SpinnerItem andToSpinner = new SpinnerItem(Keys.RIGHT_CONTEXT, Services.getI18n().toSpinner()); // taille du contexte droit
		andToSpinner.setWriteStackedIcons(false);
		andToSpinner.setMin(0.0d);
		
		checkboxItem = new CheckboxItem(Keys.INCLUDE_PIVOT, Services.getI18n().pivotCheckbox()); // inclure la structure contenant le pivot dans les contextes
		radioGroupItem.addChangedHandler(new ChangedHandler() {			
			@Override
			public void onChanged(ChangedEvent event) {
				updateStructureListState();
			}
		});

		//default values
		fminSpinner.setValue(2);
		cminSpinner.setValue(2);
		sMinSpinner.setValue(2);
		fromSpinner.setValue(9);
		toSpinner.setValue(0);
		andFromSpinner.setValue(0);
		andToSpinner.setValue(9);
		checkboxItem.setDisabled(true);
		updateStructureListState();

		contextForm.setNumCols(15);
		contextForm.setWidth100();
		contextForm.setItems(radioGroupItem, selectStructureItem, fromSpinner, toSpinner, andFromSpinner, andToSpinner, checkboxItem);
		
		section3.setMembers(contextForm);

		this.setMembers(section1, section2, section3);

		ProcessingWindow.hide();
	}
	
	protected void updateStructureListState() {
		if (radioGroupItem == null) return;
		if (checkboxItem == null) return;
		if (selectStructureItem == null) return;
		
		boolean b = radioGroupItem.getValueAsString().equals(Keys.WORD);
		checkboxItem.setDisabled(b);
		selectStructureItem.setDisabled(b);
	}

	public void setParameters(Map<String, String> parameters) {
		for (String property : parameters.keySet()) {
			if (Keys.QUERY.equals(property)) {
				queryForm.getItem(property).setValue(parameters.get(property));
			} else if (Keys.CONTEXT.equals(property) ||
					Keys.STRUCTURE.equals(property) ||
					Keys.LEFT_CONTEXT.equals(property) ||
					Keys.MIN_LEFT_CONTEXT.equals(property) ||
					Keys.MIN_RIGHT_CONTEXT.equals(property) ||
					Keys.RIGHT_CONTEXT.equals(property) ||
					Keys.INCLUDE_PIVOT.equals(property)) {
				contextForm.getItem(property).setValue(parameters.get(property));
			} else {
				if (form.getItem(property) != null) {
					form.getItem(property).setValue(parameters.get(property));
				}
			}
			// contextForm.setItems(radioGroupItem, selectStructureItem, fromSpinner, toSpinner, andFromSpinner, andToSpinner, checkboxItem);
		}
		
		updateStructureListState();
	}

	public void validate() {
		
		//fill params
		params.setQuery(queryForm.getValueAsString(Keys.QUERY));

		if (params.getQuery() == null || params.getQuery().trim().equals("")) {
			TxmDialog.warning(Services.getI18n().noQuery());
			return;
		}
		if (params.getQuery().matches(".*%(d|cd|dc).*")) {
			TxmDialog.warning(Services.getI18n().queryDError());
			return;
		}
		
		try {
			//System.out.println("freq = " + Integer.parseInt(form.getValueAsString(Keys.FMIN)));
			params.setMinfreq(Integer.parseInt(form.getValueAsString(Keys.FMIN)));

			//System.out.println("cofreq = " + Integer.parseInt(form.getValueAsString(Keys.COFMIN)));
			params.setMinCofreq(Integer.parseInt(form.getValueAsString(Keys.CMIN)));

			//System.out.println("score = " + Integer.parseInt(form.getValueAsString(Keys.SMIN)));
			params.setMinscore(Integer.parseInt(form.getValueAsString(Keys.SMIN)));

			TXMWEB.getConsole().addMessage("BEFORE SETTING PARAMS CONTEXT");
			//System.out.println("maxL = " + Integer.parseInt(contextForm.getValueAsString(Keys.LEFT_CONTEXT)) +1);
			params.setLeftmax(Integer.parseInt(contextForm.getValueAsString(Keys.LEFT_CONTEXT)));

			//System.out.println("minL = " + Integer.parseInt(contextForm.getValueAsString(Keys.MIN_LEFT_CONTEXT)) +1);
			params.setLeftmin(Integer.parseInt(contextForm.getValueAsString(Keys.MIN_LEFT_CONTEXT)));

			//System.out.println("maxR = " + Integer.parseInt(contextForm.getValueAsString(Keys.RIGHT_CONTEXT)));
			params.setRightmax(Integer.parseInt(contextForm.getValueAsString(Keys.RIGHT_CONTEXT)) );

			//System.out.println("minR = " + Integer.parseInt(contextForm.getValueAsString(Keys.MIN_RIGHT_CONTEXT)));
			params.setRightmin(Integer.parseInt(contextForm.getValueAsString(Keys.MIN_RIGHT_CONTEXT)));

			//System.out.println("check = " + Boolean.parseBoolean(contextForm.getValueAsString(Keys.CHECK)));
			params.setPivot(Boolean.parseBoolean(contextForm.getValueAsString(Keys.INCLUDE_PIVOT)));

			//System.out.println("struct = " + contextForm.getValueAsString(Keys.STRUCTURE));
			if (Services.getI18n().structRadio().equals(contextForm.getValueAsString(Keys.STRUCTURE))) {
				params.setStructure(contextForm.getValueAsString("select"+Keys.STRUCTURE));
			} else {
				params.setStructure("");
			}
		} catch (Exception e) {
			TxmDialog.warning(Services.getI18n().failed_get_cooc_params(e.getMessage()));
			e.printStackTrace();
			return;
		}
		
		Object value = form.getItem(Keys.PROPERTIES).getValue();
		ArrayList<String> properties = new ArrayList<String>();
		if (value != null) {
			properties.addAll(Arrays.asList((""+value).split(",")));
		} else {
			properties.add(Keys.WORD);
		}
		params.setProperties(properties);
		
		//Call the cooccurrence Service
		ProcessingWindow.show(Services.getI18n().computing_cooc(params.getQuery()));
		//ExecTimer.start();
		Services.getCooccurrenceService().cooccurrence(params, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				ProcessingWindow.hide();
				if (result == null || result.length() > 0) {
					if (result.equals("permission")) {
						TxmDialog.warning(Services.getI18n().noPermission());
					} else if(result.equals("session")) {
						TxmDialog.warning(Services.getI18n().sessionExpired());
					} else {
						TxmDialog.warning(Services.getI18n().querySyntaxError(result));
					}
				}
				else {
					Services.getCooccurrenceService().getLines(params, new AsyncCallback<CooccurrenceResult>() {
						@Override
						public void onFailure(Throwable caught) {
							ProcessingWindow.hide();
							TxmDialog.warning(Services.getI18n().cooccurrenceError());
						}
						@Override
						public void onSuccess(CooccurrenceResult result) {
							ProcessingWindow.hide();
							parentTab.fillList(result);
							TXMWEB.getConsole().addMessage(Services.getI18n().results(result.size()));
						}
					});
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				TxmDialog.warning(Services.getI18n().cooccurrenceError());
			}
		});
	}

	public void export() {
		ProcessingWindow.show(Services.getI18n().computing_cooc(params.getQuery()));

		ExecTimer.start();
		Services.getCooccurrenceService().exportCooccurrence(params, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				ProcessingWindow.hide();
				if (result == null) {
				} else if (result.startsWith("permission")) {
					TxmDialog.warning(Services.getI18n().noPermission());
				} else if(result.startsWith("session")) {
					TxmDialog.warning(Services.getI18n().sessionExpired());
				} else if(result.startsWith("error")) {
					TxmDialog.warning(result);
				}  else {
					String url = GWT.getHostPageBaseURL() + result;
					Window.open(url, "", "");
					TxmDialog.exportWarning();
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				TxmDialog.warning(Services.getI18n().cooccurrenceError());
			}
		});
	}

	public void setQuery(String query) {
		this.queryForm.setValue(Keys.QUERY, query);
	}
}
