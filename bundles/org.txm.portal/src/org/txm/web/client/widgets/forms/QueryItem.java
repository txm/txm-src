package org.txm.web.client.widgets.forms;

import com.smartgwt.client.widgets.form.fields.TextItem;

public class QueryItem extends TextItem {
	public QueryItem(String name) {super(name);}
	public QueryItem(String name, String label) {super(name, label);}
}
