package org.txm.web.client.widgets.forms;

import java.util.Map;

import org.txm.web.client.Commands;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.tabs.EditionTab;
import org.txm.web.client.widgets.tabs.TextsTab;
import org.txm.web.shared.Keys;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class TextsForm {
	
	public static void validate(final Map<String, String> parameters, final ListGridRecord theRecord) {
		
		if (theRecord != null) {
			final String path = parameters.get(Keys.PATH);
			TXMWEB.log(Services.getI18n().open_biblio_tab());
			Services.getCorpusActionService().getNumberOfTexts(path, new AsyncCallback<Integer>() {
				@Override
				public void onSuccess(Integer result) {
					if (result == 1) {
						Services.getCorpusActionService().getFirstTextID(path, new AsyncCallback<String>() {
							@Override
							public void onSuccess(String result) {
								if ("session".equals(result)) {
									TxmDialog.severe(Services.getI18n().sessionExpired());
								} else if ("no text".equals(result)) {
									TxmDialog.severe("No texts.");
								} else {
									new EditionTab(theRecord, Commands.createParameters(path)).addInView();
								}
							}

							@Override
							public void onFailure(Throwable caught) {
								TxmDialog.severe(Services.getI18n().error()+caught);
							}
						});
					} else if (result > 1) {
						new TextsTab(Commands.getRecordByPath(path), parameters).addInView();
					} else {
						TxmDialog.severe("No text.");
					}
				}

				@Override
				public void onFailure(Throwable caught) {
					TxmDialog.severe(Services.getI18n().error()+caught);
				}
			});
		} else {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(parameters.get(Keys.PATH)));
		}
	}
}
