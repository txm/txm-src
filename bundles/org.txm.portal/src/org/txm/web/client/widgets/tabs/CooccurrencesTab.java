package org.txm.web.client.widgets.tabs;

import java.util.ArrayList;
import java.util.Map;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.EnhancedListGrid;
import org.txm.web.client.widgets.forms.CooccurrenceForm;
import org.txm.web.client.widgets.forms.TxmForm;
import org.txm.web.shared.CooccurrenceResultLine;
import org.txm.web.shared.Keys;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.VLayout;

public class CooccurrencesTab extends TxmCenterTab implements CooccurrencePresentation{
	
	private static final String COOCCURRENT_COLNAME ="COOCCURENT";
	private static final String FREQUENCY_COLNAME ="FREQ";
	private static final String COFREQUENCY_COLNAME ="COFREQ";
	private static final String SCORE_COLNAME ="SCORE";
	private static final String DISTANCE_COLNAME ="DIST";
	
	private VLayout cooccurrencesTabContent = new VLayout();
	private CooccurrenceForm form = null;
	public TxmForm getForm() {
		return this.form;
	}
	private EnhancedListGrid resultList = new EnhancedListGrid();
	private ListGridRecord elem;
	
	public void setTitle(String title) {
		
		super.setTitle(Canvas.imgHTML("icons/functions/Cooccurrences.png", 16, 16)+"&nbsp;"+"<span style=\"vertical-align:-2px;\">"+title+"</span>");
	}
	
	public CooccurrencesTab(ListGridRecord elem, Map<String, String> parameters) {
		
		this.setName("COOCCURRENCES");
		setTitle(elem.getAttribute("name"));
		this.elem = elem;
		this.form = new CooccurrenceForm(parameters, this);
		
		ListGridField cooccurField = new ListGridField(COOCCURRENT_COLNAME, Services.getI18n().cooccurrentField());
		ListGridField frequencyField = new ListGridField(FREQUENCY_COLNAME, Services.getI18n().frequencyField());
		ListGridField cofrequencyField = new ListGridField(COFREQUENCY_COLNAME, Services.getI18n().coFrequencyField());
		ListGridField scoreField = new ListGridField(SCORE_COLNAME, Services.getI18n().indexField());
		ListGridField distanceField = new ListGridField(DISTANCE_COLNAME, Services.getI18n().distanceField());
		
		frequencyField.setType(ListGridFieldType.INTEGER);
		cofrequencyField.setType(ListGridFieldType.INTEGER);
		scoreField.setType(ListGridFieldType.FLOAT);
		scoreField.setFormat("0.0");
		distanceField.setType(ListGridFieldType.FLOAT);
		distanceField.setFormat("0.0");

		resultList.setSize("100%", "100%");
		resultList.setShowEmptyMessage(false);
		resultList.setShowRowNumbers(true);
		resultList.setFields(cooccurField, frequencyField, cofrequencyField, scoreField, distanceField);
		resultList.setShowEmptyMessage(false);
		resultList.setCanSort(true);
		resultList.setCanFreezeFields(false);
		resultList.setCanGroupBy(false);
		resultList.setCanAutoFitFields(false);
		resultList.setCanPickFields(false);
		resultList.setCanSelectText(true);
		resultList.setEmptyMessage(Services.getI18n().noresultts());
		
		cooccurrencesTabContent.setMembers(form, resultList);
		this.setPane(cooccurrencesTabContent);
		
		this.setParameters(parameters);
		if (parameters.get(Keys.QUERY) != null && parameters.get(Keys.QUERY).length() > 0) {
			this.compute();
		}
	}
	
	/**
	 * Displays the tab into the center panel.
	 */
	public void addInView() {
		
		addInView(null, null, null);
	}

	@Override
	public void fillList(ArrayList<CooccurrenceResultLine> result) {
		
		resultList.setShowEmptyMessage(true);
		for (Record r : resultList.getRecords()) {
			resultList.removeData(r);
		}
		
		for (CooccurrenceResultLine entry : result) {
			ListGridRecord record = new ListGridRecord();
			record.setAttribute(COOCCURRENT_COLNAME, entry.getCooccurrent());
			record.setAttribute(COFREQUENCY_COLNAME, entry.getCoFrequency());
			record.setAttribute(FREQUENCY_COLNAME, entry.getFrequency());
			record.setAttribute(SCORE_COLNAME, entry.getScore());
			record.setAttribute(DISTANCE_COLNAME, entry.getAverageDistance());
			resultList.addData(record);
		}	
		
		resultList.sort(SCORE_COLNAME, SortDirection.DESCENDING);
	}
	
	@Override
	public boolean isFilled() {
		
		return resultList.getRecords().length > 0;
	}

	public void setQuery(String query) {
		
		this.form.setQuery(query);
	}

	public void compute() {
		this.form.validate();
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub
	}
}
