package org.txm.web.client.widgets;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;

public class PaginationWidget extends DynamicForm {
	
	//Label l = new Label();
	TextItem pageName = new TextItem();
	
	public PaginationWidget() {
		super();
		pageName.setPrompt("Current page. Change value for direct access");
		pageName.setShowTitle(false);
		pageName.setWidth(50);
		pageName.setLength(10);
		
		//pageName.setEnforceLength(false);
		this.setWidth(100);
		//this.setMembers(pageName, l);
		this.setItems(pageName);
	}
	
	public void update(String currentPage, int i) {
		pageName.setHint(" /&nbsp;"+i);
		pageName.setValue(currentPage);
	}
	
	public void setOnEnterEvent(KeyPressHandler handler) {
		pageName.addKeyPressHandler(handler);
	}

	public String getPageID() {
		return pageName.getValueAsString();
	}

	public void showWarning(boolean b) {

	}
}
