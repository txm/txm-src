package org.txm.web.client.widgets.tabs.admin;

import java.util.List;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.dialogs.TxmDialog;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class DumpDataTab extends Tab {

	private AdminTab adminTab;
	TextAreaItem contentField;
	
	public DumpDataTab(AdminTab adminTab) {
		this.setAdminTab(adminTab);

		try {
			this.setTitle(Services.getI18n().dumpPanel());

			final DynamicForm form = new DynamicForm();
			form.setHeight("100%");
			form.setWidth("100%");
			form.setNumCols(3);
			//form.setColWidths("50","*","100");

			final Button showUsersBtn = new Button("Show users");  
			showUsersBtn.setWidth(80);   
			showUsersBtn.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					Services.getAdminService().dumpUsers(new AsyncCallback<List<String>>() {

						@Override
						public void onFailure(Throwable caught) { }

						@Override
						public void onSuccess(List<String> result) {
							String content = "";
							for (String str : result)
								content += str+"\n\n";
							contentField.setValue(content);
						}
					});
				}
			});

			final Button showProfilesBtn = new Button("Show profiles");  
			showProfilesBtn.setWidth(80);   
			showProfilesBtn.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					Services.getAdminService().dumpProfiles(new AsyncCallback<List<String>>() {

						@Override
						public void onFailure(Throwable caught) { }

						@Override
						public void onSuccess(List<String> result) {
							String content = "";
							for (String str : result)
								content += str+"\n\n";
							contentField.setValue(content);
						}
					});
				}
			});

			contentField = new TextAreaItem();
			contentField.setName("content");
			contentField.setShowTitle(true);
			contentField.setHeight("300px");
			contentField.setWidth("100%");

			form.setItems(contentField);

			HLayout btns = new HLayout();
			btns.setWidth100();
			btns.setMembers(showUsersBtn, showProfilesBtn);
			
			VLayout content = new VLayout();
			content.setMembers(form, btns);
			content.setHeight100();
			content.setWidth100();

			this.setPane(content);
		} catch(Exception e) {
			TxmDialog.severe("Internal Error: "+e);
		}
	}

	/**
	 * @return the adminTab
	 */
	public AdminTab getAdminTab() {
		return adminTab;
	}

	/**
	 * @param adminTab the adminTab to set
	 */
	public void setAdminTab(AdminTab adminTab) {
		this.adminTab = adminTab;
	}
}
