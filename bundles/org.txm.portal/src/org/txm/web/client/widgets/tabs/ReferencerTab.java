package org.txm.web.client.widgets.tabs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.txm.web.client.ExecTimer;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.EnhancedListGrid;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.forms.NavigationToolStrip;
import org.txm.web.client.widgets.forms.ReferenceForm;
import org.txm.web.client.widgets.forms.TxmForm;
import org.txm.web.shared.Keys;
import org.txm.web.shared.ReferencerResult;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class ReferencerTab extends TxmCenterTab {

	protected static boolean firstExportWarning = true;
	protected ReferenceForm form = null;
	public TxmForm getForm() {
		return this.form;
	}
	protected ReferencerResult result = null;
	protected EnhancedListGrid resultList = new EnhancedListGrid();
	protected NavigationToolStrip navigation;
	public int startindex = 0;
	protected int endindex = 0;
	protected int totalpages = 0;
	protected ArrayList<String> colnames;
	protected ArrayList<ListGridField> listGridFields = new ArrayList<ListGridField>();
	//private VocabularyResult currentResult = null;
	VLayout resultSection;
	VLayout section2;
	ToolStrip indexBar;

	public void setTitle(String title) {
		super.setTitle(Canvas.imgHTML("icons/functions/Referencer.png", 16, 16)+"&nbsp;"+"<span style=\"vertical-align:-2px;\">"+title+"</span>");
	}
	
	public ReferencerTab(Map<String, String> parameters) {
		super();
		
		this.setName("REFERENCER");
		this.form = new ReferenceForm(parameters, this);
		this.setTitle(Services.getI18n().references_title(form.getParams().getCorpusPath(), form.getParams().getQuery()));
		
		resultSection = new VLayout();
		resultSection.setID("references" + this.getID());
		resultSection.setSize("100%", "100%");

		this.setPane(resultSection);
		
		VLayout section2 = new VLayout();
		section2.setHeight100();
		section2.setWidth100();
				
		indexBar = new ToolStrip();
		indexBar.setWidth100();
		indexBar.setHeight(25);
		
		navigation = new NavigationToolStrip();
		navigation.addExportClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ProcessingWindow.show(Services.getI18n().exporting());
				ExecTimer.start();
				Services.getReferencesService().export(form.getParams(), new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						TxmDialog.severe(Services.getI18n().internalError()+ caught);
					}
					@Override
					public void onSuccess(String result) {
						ProcessingWindow.hide();
						if (result == null) {
							TxmDialog.severe(Services.getI18n().internalError());
						} else if (result.startsWith("error")) {
							TxmDialog.severe(result);
						} else if (result.length() > 0) {
							Window.open(result, "", "");
							TxmDialog.exportWarning();
						}
					}
				});
			}
		});
		navigation.addPreviousClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int oldstart = startindex;
				if (result == null)
					return;
				if (startindex <= 0)
					return;
				startindex = startindex - getPageSize();
				if (oldstart != startindex)
					form.validate();
				//navigation.updateLabel("<b>" + startindex + "-" + endindex + "</b>/"+ totalpages);
			}
		});
		navigation.addFirstClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int oldstart = startindex;
				if (result == null)
					return;
				startindex = 0;
				if (oldstart != startindex)
					form.validate();
				//navigation.updateLabel("<b>" + startindex + "-" + endindex + "</b>/"+ totalpages);
			}
		});
		navigation.addLastClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int oldstart = startindex;
				if (result == null)
					return;
				startindex = result.total - getPageSize();
				if (startindex < 0)
					startindex = 0;
				if (oldstart != startindex)
					form.validate();
				//navigation.updateLabel("<b>" + startindex + "-" + endindex + "</b>/"+ totalpages);
			}
		});
		navigation.addNextClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int oldstart = startindex;
				if (result == null)
					return;
				if (startindex >=  (result.total - getPageSize()))
					return;
				startindex = startindex + getPageSize();
				if (oldstart != startindex)
					form.validate();
				//navigation.updateLabel("<b>" + startindex + "-" + endindex + "</b>/"+ totalpages);
			}
		});
		navigation.updateLabel("<b>" + startindex + "-" + endindex + "</b>/"+ totalpages);
		navigation.addToToolstrip(indexBar);
			
		section2.setMembers(resultList, indexBar);
		resultSection.setMembers(form, section2);
		initColumns();
		
		// setParameters doen in the Form
	}

	protected void initColumns() {
		ListGridField keywordField = new ListGridField("KEYWORD", Services.getI18n().indexKeyword());
		keywordField.setWidth(200);
		ListGridField refField = new ListGridField("ref", Services.getI18n().references());
		listGridFields.add(keywordField);
		listGridFields.add(refField);
		
		resultList.setShowRowNumbers(true);
		resultList.setFields(listGridFields.toArray(new ListGridField[]{}));
		resultList.setShowEmptyMessage(false);
		resultList.setCanSort(false);
		resultList.setCanSelectText(true);
		resultList.setCanFreezeFields(false);
		resultList.setCanGroupBy(false);
		resultList.setCanAutoFitFields(false);
		resultList.setCanPickFields(false);
		ProcessingWindow.hide();
	}

	public void fillList(ReferencerResult result) {
		
		this.result = result;
		resultList.setShowEmptyMessage(true);
		resultList.setCanSort(false);
		
		for (Record r : resultList.getRecords())
			resultList.removeData(r);
		
		for (String entry : result.refs.keySet()) {
			ListGridRecord record = new ListGridRecord();
			record.setAttribute("KEYWORD", entry);
			
			List<String> refs = result.refs.get(entry);
			HashMap<String, Integer> counts = result.counts.get(entry);
			StringBuffer str = new StringBuffer(2*refs.size());
			for (int i  = 0 ; i < refs.size() ; i++)
				str.append(refs.get(i)+"("+counts.get(refs.get(i))+")  ");
			
			record.setAttribute("ref", str.toString());
			
			resultList.addData(record);
		}	

		this.setTitle(Services.getI18n().references_title(form.getParams().getCorpusPath(), form.getParams().getQuery()));
		
		startindex = form.getParams().startindex;
		endindex = startindex+ result.refs.keySet().size();//.getParams().filterNbPerPage;
		totalpages = result.total;
		navigation.updateLabel("<b>" + (startindex+1) + "-" + (endindex) + "</b>/"+ (totalpages));
		if (totalpages > 1)
			TXMWEB.getConsole().addMessage(Services.getI18n().results(totalpages)+ExecTimer.stop());
		else
			TXMWEB.getConsole().addMessage(Services.getI18n().result()+ExecTimer.stop());
	}

	public int getPageSize() {
		return navigation.getPagination();
	}

	@Override
	public void onClose() {
		// TODO Auto-generated method stub
		
	}
}
