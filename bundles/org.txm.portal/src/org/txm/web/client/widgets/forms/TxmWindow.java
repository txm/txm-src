package org.txm.web.client.widgets.forms;

import java.util.Map;

import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;

/**
 * It's a smartGWT window overrided to set some default options
 * @version 0.2.0
 * @author vchabani
 */
public abstract class TxmWindow extends Window {

	protected DynamicForm form;

	public TxmWindow(boolean modal, String fix) {
		super();

		if (modal) {
			this.setAutoSize(true);
			this.setWidth("50%");
			this.setIsModal(true);
			this.setShowModalMask(true); 
			this.setShowMinimizeButton(false);
			//this.setKeepInParentRect(true);
			this.setOpacity(100);
			this.setBodyColor("white");
		} else {
			this.show();
		}
		//		this.addCloseClickHandler(new com.smartgwt.client.widgets.events.CloseClickHandler() {
		//			@Override
		//			public void onCloseClick(CloseClientEvent event) {
		//				destroy();
		//			}
		//		});
	}

	public void setParameters(Map<String, String> parameters) {

		if (parameters == null) return;

		if (form == null) return;

		for (String property : parameters.keySet()) {
			setParameter(property, parameters.get(property));
		}
	}

	public void setParameter(String property, String value) {
		if (form == null) return;
		if (form.getItem(property) == null) return;
		form.getItem(property).setValue(value);
	}

	public void _initializeFields(Map<String, String> parameters) {
		// see TxmForm._initializeFields
	}

	/**
	 * MUST BE CALLED IN THE CONSTRUCTOR
	 */
	protected void initializeFields(final Map<String, String> parameters) {
		_initializeFields(parameters);
		setFormItemNamesFromParameterAnnotations();
	}

	protected SelectItem createShareButton(final Map<String, String> parameters) {
		SelectItem shareItem = new SelectItem("share");
		shareItem.setTitle("<img src='images/icons/share.png'/>");
		shareItem.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				com.google.gwt.user.client.Window.open("./?"+parameters, "", "");
			}
		});
		return shareItem;
	}

	private void setFormItemNamesFromParameterAnnotations() {

		// This TxmForm.initializeFields
	}
}
