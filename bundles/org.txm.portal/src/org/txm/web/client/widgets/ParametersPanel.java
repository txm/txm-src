package org.txm.web.client.widgets;

import java.util.ArrayList;
import java.util.List;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.dialogs.TxmDialog;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;

public class ParametersPanel extends HLayout {

	private DynamicForm queryForm;
	private TextItem itemCG;
	private TextItem itemCD;
	//private SelectItem itemPropsWord;
	private SelectItem itemPropsView;
	private SelectItem itemCorpus;
	private SelectItem itemRef;

	private String[] listPropsWord;
	private String[] listPropsView;
	private List<String> refStructs;
	private ArrayList<String> availableParallelCorpora;
	private HLayout contextLayout;
	private HLayout paramsLayout;  
	private boolean checked = true; 
	private String corpus;
	//private String propsWord;
	private String propsView;
	private String ref;
	private String alignStruct;
	
	ListGridField field = new ListGridField("result", "Results");  
	ListGrid grid;
	String path;

	public ParametersPanel(String itemPath) {  
		this.path = itemPath;
		Services.getEditionService().getParallelCorpora(itemPath, new AsyncCallback<ArrayList<String>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				TxmDialog.severe("Error: getParallelCorpora failed: "+caught.toString());
			}
			@Override
			public void onSuccess(ArrayList<String> results) {
				System.out.println("availableParallelCorpora ? "+results);
				if(results!=null ){
					alignStruct = results.get(results.size()-1);
					results.remove(results.size()-1);
				}
				availableParallelCorpora = results;
				availableParallelCorpora.add(path.substring(1));
				System.out.println("NB availableParallelCorpora ? "+availableParallelCorpora.size());
				
				init();
			}
		});

		

	}  

	public void init(){
		//layout = new HLayout(20);
		this.setWidth(450);
		this.setHeight(300);

		DynamicForm paramsForm = new DynamicForm();
		paramsForm.setWidth(250);  

		//ToolStripButton participateButt = new ToolStripButton("participe");
		//(1)
		itemCorpus = new SelectItem();
		itemCorpus.setTitle("Corpora");
		itemCorpus.setWidth(120);  
		String [] corpora = null;
		if(availableParallelCorpora!=null){
			Object[] corporaTemp = availableParallelCorpora.toArray();
			corpora = new String[corporaTemp.length];
			for(int i=0;i<corporaTemp.length;i++)
				corpora[i] = (String) corporaTemp[i];
		}
		//System.out.println("Nb Corpora ? "+corpora.length);
		itemCorpus.setValueMap(corpora);  
		if(corpora.length>0){
			itemCorpus.setDefaultValue(corpora[0]);  
		}
		
		itemCorpus.setValueMap(corpora);  
		itemCorpus.addChangedHandler(new ChangedHandler() {
			
			@Override
			public void onChanged(ChangedEvent event) {
				// TODO Auto-generated method stub
				corpus = itemCorpus.getValueAsString();
				System.out.println("CORPUS selected is : "+corpus);
				
				Services.getCorpusActionService().getViewProperties("/"+corpus, new AsyncCallback<List<String>>() {
					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						TxmDialog.severe(Services.getI18n().failed_get_wordproperties("/"+corpus, caught.getMessage()));
					}
					@Override
					public void onSuccess(List<String> result) {
						ProcessingWindow.hide();
						listPropsView = result.toArray(new String[]{});
						if(listPropsView!=null){
							itemPropsView.setValueMap(listPropsView);  
							if(listPropsView.length>0){
								itemPropsView.setDefaultValue(listPropsView[0]);
							}
						}
					}
				});

				Services.getCorpusActionService().getRefProperties("/"+corpus, new AsyncCallback<List<String>>() {
					@Override
					public void onFailure(Throwable caught) {
						TxmDialog.severe("Error: getRefProperties failed: "+caught.toString());
					}
					@Override
					public void onSuccess(List<String> results) {
						refStructs = results;  
						if(refStructs!=null){
							Object[] refsTemp = refStructs.toArray();
							String [] refs = new String[refsTemp.length];
							for(int i=0;i<refsTemp.length;i++)
								refs[i] = (String) refsTemp[i];
							
							itemRef.setValueMap(refs);  
							if(refs.length>0){
								itemRef.setDefaultValue(refs[0]);
							}
						}
					}
				});
			}
		});
		

		//Corpus Properties Menu (2)
		itemPropsView = new SelectItem();
		itemPropsView.setTitle("Props views");
		if(itemPropsView!=null){
			itemPropsView.setValueMap(listPropsView);  
		}
	

		//References Properties Menu (3)
		itemRef = new SelectItem();
		itemRef.setTitle("Refs");
		itemRef.setWidth(120);  
		if(refStructs!=null){
			Object[] refsTemp = refStructs.toArray();
			String [] refs = new String[refsTemp.length];
			for(int i=0;i<refsTemp.length;i++)
				refs[i] = (String) refsTemp[i];
			itemRef.setValueMap(refs);  
		}
		
		//Participate checkbox (4)
		CheckboxItem cb = new CheckboxItem();
		cb.setTitle("Participe");
	
		cb.addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {
				checked = (Boolean) event.getValue();
			}
		});


		//LEFT and RIGHT Contexts
		DynamicForm contextsForm = new DynamicForm();

		itemCG = new TextItem("CG");
		itemCG.setWidth(40);  
		itemCG.setType("integer");  
		itemCG.setDefaultValue(5);
	
		itemCD = new TextItem("CD");
		itemCD.setWidth(40);  
		itemCD.setType("integer");  
		itemCD.setDefaultValue(2);
		contextsForm.setFields(itemCG,itemCD);


		FormItem [] paramsItems = new FormItem[4];
		paramsItems[0] = itemCorpus;	
		paramsItems[1] = itemPropsView;
		//paramsItems[2] = itemPropsWord;
		paramsItems[2] = itemRef;
		paramsItems[3] = cb;
		paramsForm.setFields(paramsItems);          


		
		SectionStack section = new SectionStack();
		section.setWidth100();   
		section.setHeight100();

		SectionStackSection paramSection = new SectionStackSection();
		paramsLayout = new HLayout();  
		//paramsLayout.setHeight(40);
		paramsLayout.setMembers(contextsForm, paramsForm);
		//paramSection.setExpanded(true);
		paramSection.setTitle("Paramètres d'affichage");
		paramSection.addItem(paramsLayout);

		SectionStackSection contextSection = new SectionStackSection();
		grid = new ListGrid();
		grid.setShowRecordComponents(true);          
		grid.setShowRecordComponentsByCell(true);  
		grid.setShowAllRecords(true);  
		grid.setFields(field);  
		ArrayList<String> gridData = new ArrayList<String>();
		initGridData(gridData);
		//grid.setHeight100();
		contextSection.setExpanded(true);
		contextSection.setShowHeader(false);
		contextSection.addItem(/*contextLayout*/grid);
		section.setSections(paramSection, contextSection);
		this.addMember(section);
	}
	
	public void initGridData(ArrayList<String> results){
		
		RecordList recordList =new RecordList();
		for(String result : results){
			ListGridRecord gridRecord = new ListGridRecord();
			gridRecord.setAttribute("result", result);		
			recordList.add(gridRecord);
		}
		grid.setData(recordList);
	}
	
	
	
	public String getCorpus() {
		return corpus;
	}

	/*public String getPropsWord() {
		return propsWord;
	}*/

	public String getPropsView() {
		return itemPropsView.getValueAsString();
	}

	public String getRef() {
		return itemRef.getValueAsString();
		//return ref;
	}

	public boolean getParticipate(){
		return checked;
	}
	
	public String getLeftContext(){
		return itemCG.getValueAsString();
	}
	
	public String getRightContext(){
		return itemCD.getValueAsString();
	}
	
	public String getAlignStruct(){
		return alignStruct;
	}

}
