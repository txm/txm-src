package org.txm.web.client.widgets.forms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import org.txm.web.client.ExecTimer;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.pickers.DownPicker;
import org.txm.web.client.widgets.pickers.UpPicker;
import org.txm.web.client.widgets.tabs.ConcordancePresentation;
import org.txm.web.shared.ConcordanceParam;
import org.txm.web.shared.ConcordanceResult;
import org.txm.web.shared.Keys;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;

public class ComplexSortForm extends TxmForm {

	private ConcordancePresentation parentTab = null;
	private ConcordanceParam params  = null;
	
	UpPicker upFirstPicker = new UpPicker();
	UpPicker upSecondPicker = new UpPicker();
	UpPicker upThirdPicker = new UpPicker();
	UpPicker upFourthPicker = new UpPicker();
	DownPicker downFirstPicker = new DownPicker();
	DownPicker downSecondPicker = new DownPicker();
	DownPicker downThirdPicker = new DownPicker();
	DownPicker downFourthPicker = new DownPicker();
	
	boolean[] dirs = {true, true, true, true};
	private SelectItem fourthKey;
	private SelectItem thirdKey;
	private SelectItem secondKey;
	private SelectItem firstKey;
	
	public ComplexSortForm(ConcordancePresentation parent, ConcordanceParam params) {
		super();
		this.parentTab = parent;
		this.params = params;
		
		initializeFields(null);
	}
	
	@Override
	public void _initializeFields(Map<String, String> parameters) {
		
		this.form = new DynamicForm();

		this.setWidth100();
		//this.setAlign(Alignment.CENTER);
		
		upFirstPicker.addFormItemClickHandler(new FormItemClickHandler() {
			
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				dirs[0] = true;
				upFirstPicker.setShowOver(true);
				firstKey.setTitle("#1 ^");
				firstKey.redraw();
			}
		});
		upSecondPicker.addFormItemClickHandler(new FormItemClickHandler() {
			
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				dirs[1] = true;
				upSecondPicker.setShowOver(true);
				secondKey.setTitle("#2 ^");
				secondKey.redraw();
			}
		});
		upThirdPicker.addFormItemClickHandler(new FormItemClickHandler() {
			
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				dirs[2] = true;
				upThirdPicker.setShowOver(true);
				thirdKey.setTitle("#3 ^");
				thirdKey.redraw();
			}
		});
		upFourthPicker.addFormItemClickHandler(new FormItemClickHandler() {
			
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				dirs[3] = true;
				upFourthPicker.setShowOver(true);
				fourthKey.setTitle("#4 ^");
				fourthKey.redraw();
			}
		});
		
		downFirstPicker.addFormItemClickHandler(new FormItemClickHandler() {
			
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				dirs[0] = false;
				downFirstPicker.setShowOver(true);
				firstKey.setTitle("#1 v");
				firstKey.redraw();
			}
		});
		downSecondPicker.addFormItemClickHandler(new FormItemClickHandler() {
			
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				dirs[1] = false;
				downSecondPicker.setShowOver(true);
				secondKey.setTitle("#2 v");
				secondKey.redraw();
			}
		});
		downThirdPicker.addFormItemClickHandler(new FormItemClickHandler() {
			
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				dirs[2] = false;
				downThirdPicker.setShowOver(true);
				thirdKey.setTitle("#3 v");
				thirdKey.redraw();
			}
		});
		downFourthPicker.addFormItemClickHandler(new FormItemClickHandler() {
			
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				dirs[3] = false;
				downFourthPicker.setShowOver(true);
				upFourthPicker.setShowOver(false);
				fourthKey.setTitle("#4 v");
				fourthKey.redraw();
			}
		});

		LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();
		values.put(Keys.REFERENCE, Services.getI18n().concFieldReference());
		values.put(Keys.LEFT_CONTEXT, Services.getI18n().concFieldLeftContext());
		values.put(Keys.RIGHT_CONTEXT, Services.getI18n().concFieldRightContext());
		values.put(Keys.KEYWORD, Services.getI18n().concFieldKeyword());
		values.put(Keys.NONE, Services.getI18n().none());

		firstKey = new SelectItem(Keys.KEY1); // 1ere clé de tri
		firstKey.setPrompt(Services.getI18n().firstKeyPrompt());
		firstKey.setTitle("#1 ^");
		firstKey.setShowAllOptions(true);
		firstKey.setValueMap(values);
		firstKey.setDefaultValues(Keys.NONE);
		firstKey.setIcons(upFirstPicker, downFirstPicker);
				
		secondKey = new SelectItem(Keys.KEY2); // 2eme clé de tri
		secondKey.setPrompt(Services.getI18n().secondKeyPrompt());
		secondKey.setTitle("#2 ^");
		secondKey.setShowAllOptions(true);
		secondKey.setValueMap(values);
		secondKey.setDefaultValues(Keys.NONE);
		secondKey.setIcons(upSecondPicker, downSecondPicker);

		thirdKey = new SelectItem(Keys.KEY3);  // 3eme clé de tri
		thirdKey.setPrompt(Services.getI18n().thirdKeyPrompt());
		thirdKey.setTitle("#3 ^");
		thirdKey.setShowAllOptions(true);
		thirdKey.setValueMap(values);
		thirdKey.setDefaultValues(Keys.NONE);
		thirdKey.setIcons(upThirdPicker, downThirdPicker);
		
		fourthKey = new SelectItem(Keys.KEY4);  // 4eme clé de tri
		fourthKey.setPrompt(Services.getI18n().fourthKeyPrompt());
		fourthKey.setTitle("#4 ^");
		fourthKey.setShowAllOptions(true);
		fourthKey.setValueMap(values);
		fourthKey.setDefaultValues(Keys.NONE);
		fourthKey.setIcons(upFourthPicker, downFourthPicker);
		
		StaticTextItem label = new StaticTextItem("lcol");
		label.setDefaultValue(Services.getI18n().sortKeys());
		label.setShowTitle(false);

		form.setWidth100();
		form.setItems(label, firstKey, secondKey, thirdKey, fourthKey);
		form.setNumCols(9);
		form.setColWidths("50", "10", "*", "10", "*", "10", "*", "10", "*");
		
		IButton validateSort = new IButton(Services.getI18n().sortButton());
		validateSort.setPrompt(Services.getI18n().sortConcordances());
		validateSort.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				sort();
			}
		});
		
		this.setAlign(VerticalAlignment.CENTER);
		this.setMembers(form, validateSort);
	}
	
	public void sort()
	{
		String[] keys = {
				form.getValueAsString(Keys.KEY1),
				form.getValueAsString(Keys.KEY2),
				form.getValueAsString(Keys.KEY3),
				form.getValueAsString(Keys.KEY4)
		};
		
		//System.out.println(Arrays.toString(dirs));

		ArrayList<String> leftProperties = parentTab.getLeftSortProperties();
		ArrayList<String> keywordProperties = parentTab.getKeywordSortProperties();
		ArrayList<String> rightProperties = parentTab.getRightSortProperties();
		sort(keys, dirs, leftProperties, keywordProperties, rightProperties);
	}
	private void sort(String[] keys, boolean[] dirs, ArrayList<String> leftProperties, ArrayList<String> keywordsProperties, ArrayList<String> rightProperties) {
		ProcessingWindow.show(Services.getI18n().sorting());
		params.setSortKeys(new ArrayList<String>(Arrays.asList(keys)));
		ArrayList<Boolean> dirsList = new ArrayList<Boolean>();
		for (boolean b : dirs) dirsList.add(b);
		params.setSortDirs(dirsList);
		params.setLeftSortProperties(leftProperties);
		params.setKeywordSortProperties(keywordsProperties);
		params.setRightSortProperties(rightProperties);
		ExecTimer.start();
		Services.getConcordanceService().sortResults(params, new AsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				ExecTimer.start();
				Services.getConcordanceService().getLines(params, new AsyncCallback<ConcordanceResult>() {
					@Override
					public void onFailure(Throwable caught) {
						ProcessingWindow.hide();
						TxmDialog.info(Services.getI18n().sortFailure());
					}
					@Override
					public void onSuccess(ConcordanceResult result) {
						ProcessingWindow.hide();
						parentTab.fillList(result);
						TXMWEB.getConsole().addMessage(Services.getI18n().sortSuccess()+ExecTimer.stop());
					}
				});
			}
			@Override
			public void onFailure(Throwable caught) {
				ProcessingWindow.hide();
				TxmDialog.info(Services.getI18n().sortFailure());
			}
		});
	}

	public String[] getValues() {
		String[] keys = {
				form.getValueAsString(Keys.KEY1),
				form.getValueAsString(Keys.KEY2),
				form.getValueAsString(Keys.KEY3),
				form.getValueAsString(Keys.KEY4)
		};
		return keys;
	}
}
