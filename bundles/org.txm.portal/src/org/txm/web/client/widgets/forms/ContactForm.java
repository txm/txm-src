package org.txm.web.client.widgets.forms;

import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.CenterPanel;
import org.txm.web.client.widgets.tabs.HTMLTab;

public class ContactForm {
	
	public static void validate() {
		CenterPanel center = TXMWEB.getCenterPanel();
		HTMLTab contactPage = center.getUniqTab(Services.getI18n().contactTab());
		if (contactPage == null)
			return;
		center.selectTab(contactPage);
	}
}
