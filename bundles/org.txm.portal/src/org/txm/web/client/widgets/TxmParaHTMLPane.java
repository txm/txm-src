package org.txm.web.client.widgets;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.txm.web.client.TXMWEB;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.layout.HLayout;
/**
 * A TxmParaHTMLPane that opens pages from the directories pointed by the property htmlBase.
 * 
 * @author sgedzelm
 *
 */
public class TxmParaHTMLPane extends HLayout {
	boolean onlyOnePanel = false;
	LinkedHashMap<String, TxmHTMLPane> editionContents = new LinkedHashMap<String,TxmHTMLPane>();

	public TxmParaHTMLPane(ArrayList<String> availableEditionsValues) {
		this.setSize("100%", "100%");
		this.setAlign(Alignment.CENTER);
		this.setOverflow(Overflow.AUTO);
		this.setBackgroundColor("#DDDDDD");

		onlyOnePanel = true;
		this.setPadding(10);
		this.setMembersMargin(5);

		System.out.println("set html panels: "+availableEditionsValues.size());

		for (String edition : availableEditionsValues) {
			TxmHTMLPane editionContent = new TxmHTMLPane();
			//editionContent.setShowResizeBar(true);
			editionContent.setOverflow(Overflow.AUTO);
			
			editionContent.setPadding(10);
			editionContent.setContentsType(ContentsType.FRAGMENT);
			editionContent.setBackgroundColor("#FFFFFF");
			editionContent.setHeight100();
			editionContent.setShowResizeBar(!TXMWEB.EXPO);
			if (onlyOnePanel) {
				//System.out.println("SET WIDHT: "+TXMWEB.DEFAULTEDITIONPANELSIZE);
				editionContent.setWidth(TXMWEB.DEFAULTEDITIONPANELSIZE);
			} else {
				editionContent.setWidth100();
			}

			this.addMember(editionContent);
			//editionContent.getElement().getStyle().setOverflowY(com.google.gwt.dom.client.Style.Overflow.AUTO);;
			editionContents.put(edition, editionContent);
		}
	}

	public LinkedHashMap<String, TxmHTMLPane> getEditionPanels() {
		return editionContents;
	}

	/**
	 * Set content for synoptic editions
	 * 
	 * @param editionHtmlCodes
	 */
	public void setContents(LinkedHashMap<String, String> editionHtmlCodes)
	{
		onlyOnePanel = editionHtmlCodes.size() == 1 || editionHtmlCodes.size() == 0;
		for (String edition : editionHtmlCodes.keySet()) {
			if (!editionContents.containsKey(edition)) {
				TxmHTMLPane editionContent = new TxmHTMLPane();
				editionContent.setOverflow(Overflow.VISIBLE);
				editionContent.scrollToTop();
				editionContent.setShowResizeBar(true);
				editionContent.setOverflow(Overflow.VISIBLE);
				editionContent.setPadding(10);
				editionContent.setContentsType(ContentsType.FRAGMENT);
				editionContent.setBackgroundColor("#FFFFFF");
				editionContent.setHeight100();

				//TODO: side effect with CMSTab ?
				if (onlyOnePanel) {
					editionContent.setWidth(TXMWEB.DEFAULTEDITIONPANELSIZE);
				} else {
					editionContent.setWidth100();
				}

				editionContents.put(edition, editionContent);
			}
			TxmHTMLPane panel = editionContents.get(edition);
			//panel.setHeaderTitle(edition);
			//panel.setShowHeader(true);

			if (onlyOnePanel) {
				panel.setWidth(TXMWEB.DEFAULTEDITIONPANELSIZE);
			} else {
				panel.setWidth100();
			}

			String htmlCode = editionHtmlCodes.get(edition);
			//System.out.println(htmlCode);
			panel.setContents(htmlCode); //"Hello: "+edition
		}

		this.removeMembers(this.getMembers());
		for (String edition : editionHtmlCodes.keySet()) {
			this.addMember(editionContents.get(edition));
		}
	}
}