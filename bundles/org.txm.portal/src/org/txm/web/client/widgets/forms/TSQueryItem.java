package org.txm.web.client.widgets.forms;

import com.smartgwt.client.widgets.form.fields.TextAreaItem;

public class TSQueryItem extends TextAreaItem {
	public TSQueryItem(String name) {super(name);}
	public TSQueryItem(String name, String label) {super(name, label);}
}
