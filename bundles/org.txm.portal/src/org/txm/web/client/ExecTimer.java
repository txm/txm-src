package org.txm.web.client;

import java.util.Date;

public class ExecTimer {
	static long startTime = 0, endTime = 0;
	static String message = "";
	public static void start() {
		startTime = new Date().getTime();
	}

	public static String stop() {
		endTime = new Date().getTime();
		return setMessage(endTime - startTime);
	}

	private static String setMessage(float t) {
		if (t < 1000) {
			message = " - In "+((int)t)+" msec";
		}
		else {
			t = t/1000;
			if (t < 3) {
				message = ""+t;
				message = " - In "+(message.substring(0, 3))+" sec";
			} else if (t < 60) 
				message = " - In "+((int)t)+" sec";
			else if (t < 3600) 
				message = " - In "+((int)t/60)+" min and "+((int)t%60)+" sec";
			else 
				message = " - In "+((int)t/3600)+"h, "+((int)(t%3600)/60)+" min and "+((int)(t%3600)%60)+ " sec";
		}
		return message;
	}

	public static String getMessage() {
		return message;
	}
}
