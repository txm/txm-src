package org.txm.web.client;

public class Parameter {
	public String key;
	public String description = "";
	public boolean mandatoryToCompute = false;
	
	public Parameter(String key) {
		this.key = key; 
	}
	
	public Parameter(String key, String description) {
		this.key = key; 
		this.description = description;
	}
	
	public Parameter(String key, String description, boolean mandatoryToCompute) {
		this.key = key; 
		this.description = description;
		this.mandatoryToCompute = mandatoryToCompute;
	}
	
	public Parameter(String key, boolean mandatoryToCompute) {
		this.key = key; 
		this.mandatoryToCompute = mandatoryToCompute;
	}
}
