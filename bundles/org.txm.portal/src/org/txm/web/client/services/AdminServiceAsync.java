package org.txm.web.client.services;

import java.util.ArrayList;
import java.util.List;

import org.txm.web.shared.ServerState;
import org.txm.web.shared.UserData;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface AdminServiceAsync {
	void loadBinaryCorpus(String baseDir, AsyncCallback<Boolean> callback);
	void removeBinaryCorpus(String basename, AsyncCallback<Boolean> asyncCallback);
	
	void listProfiles(AsyncCallback<List<String>> callback);
	void getProfileContent(String profileName, AsyncCallback<String> callback);
	void updateProfileContent(String content, String filePath,
			AsyncCallback<String> callback);
	void listUsers(AsyncCallback<List<String>> callback);
	void addProfileToUser(String userName, String profileName, Boolean sendMail, AsyncCallback<Boolean> callback);
	void deleteProfiles(String[] names, AsyncCallback<Boolean> callback);
	void createProfile(String name, AsyncCallback<Boolean> callback);
	void getUsersProfiles(String userName, AsyncCallback<List<String>> callback);
	void removeProfileFromUser(String userName, String profileName,  AsyncCallback<Boolean> callback);
	void removeUser(String[] userNames, AsyncCallback<Boolean> asyncCallback);
	void addUpdateUser(UserData data, AsyncCallback<Boolean> asyncCallback);
	void importUsers(String file, AsyncCallback<Boolean> asyncCallback);
	void exportUsers(AsyncCallback<String> asyncCallback);

	void sendMail(String users, String profiles, String obj, String content,
			 AsyncCallback<String> callback);
	
	void setMaintenance(boolean state, AsyncCallback<Boolean> callback);
	void getServerState(AsyncCallback<ServerState> callback);
	void restartToolbox(AsyncCallback<Boolean> callback);
	void setMailEnable(boolean state, AsyncCallback<Boolean> callback);
	void setInscriptionEnable(boolean state, AsyncCallback<Boolean> callback);
	void quickSend(String valueAsString, AsyncCallback<Boolean> asyncCallback);
	void getLastLogs(int size,
			AsyncCallback<ArrayList<ArrayList<String>>> asyncCallback);
	void setRemoteMaintenance(String code, boolean state, String message,
			AsyncCallback<Boolean> callback);
	void dumpUsers(AsyncCallback<List<String>> callback);
	void dumpProfiles(AsyncCallback<List<String>> callback);
	void reloadServerParameters(AsyncCallback<Boolean> asyncCallback);
	void listCorporaDirectory(AsyncCallback<ArrayList<String>> asyncCallback);
	void saveTXMWEBCONF(String content,AsyncCallback<Boolean> asyncCallback);
	void getTXMWEBCONF(AsyncCallback<String> asyncCallback);
}
