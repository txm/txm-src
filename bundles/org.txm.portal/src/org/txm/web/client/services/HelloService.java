package org.txm.web.client.services;

import org.txm.web.shared.ServerInfos;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Hello RPC services declarations
 * @author vchabanis
 *
 */
@RemoteServiceRelativePath("hello")
public interface HelloService extends RemoteService {
	String sayHello();
	ServerInfos startSession() throws Exception;
	ServerInfos getServerInfos();
}
