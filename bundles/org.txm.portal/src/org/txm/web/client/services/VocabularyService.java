package org.txm.web.client.services;

import java.util.ArrayList;

import org.txm.web.exceptions.PermissionException;
import org.txm.web.shared.VocabularyParam;
import org.txm.web.shared.VocabularyResult;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Vocabulary RPC services declarations
 * @author vchabanis
 *
 */
@RemoteServiceRelativePath("vocabulary")
public interface VocabularyService extends RemoteService {
	ArrayList<String[]> vocabulary(VocabularyParam params);
	VocabularyResult index(VocabularyParam params) throws PermissionException;
	ArrayList<String> getColumnTitles(String entity);
	String export(VocabularyParam params);
}
