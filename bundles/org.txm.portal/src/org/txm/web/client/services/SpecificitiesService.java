package org.txm.web.client.services;

import java.util.ArrayList;
import java.util.Map;

import org.txm.web.shared.SpecificitiesParam;
import org.txm.web.shared.SpecificitiesResultLine;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("specificities") 
public interface SpecificitiesService extends RemoteService{

	Map<String, ArrayList<SpecificitiesResultLine>> getLines(SpecificitiesParam params);
	boolean specificities(SpecificitiesParam params);
	String exportSpecificities(SpecificitiesParam params);
}
