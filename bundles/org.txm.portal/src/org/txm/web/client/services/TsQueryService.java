package org.txm.web.client.services;

import java.util.HashMap;
import java.util.List;

import org.txm.web.shared.TsQueryParam;
import org.txm.web.shared.TsQueryResult;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Concordance RPC services declarations
 * @author mdecorde
 *
 */
@RemoteServiceRelativePath("tsquery")
public interface TsQueryService extends RemoteService {
	/**
	 * compute the result matches
	 * @param params
	 * @return
	 * @throws Exception 
	 */
	boolean tsquery(TsQueryParam params) throws Exception;
	
	TsQueryResult getFirst(TsQueryParam params);
	TsQueryResult setFeatures(TsQueryParam params);
	TsQueryResult getPrevious(TsQueryParam params);
	TsQueryResult getNext(TsQueryParam params);
	TsQueryResult getPreviousSub(TsQueryParam params);
	TsQueryResult getNextSub(TsQueryParam params);
	TsQueryResult getLast(TsQueryParam params);
	TsQueryResult getGraph(TsQueryParam params, int nograph, int nosubgraph);

	HashMap<String, List<String>> getFeatures(TsQueryParam params);

	String exportForest(TsQueryParam params, int contextsize, String method,
			boolean punct, String[] ntprops, String[] tprops);

	void dropResult(TsQueryParam params);
}
