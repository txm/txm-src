package org.txm.web.client.services;

import javax.servlet.http.HttpSession;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class PortalService extends RemoteServiceServlet {
	
	private static final long serialVersionUID = 1L;
	
	protected HttpSession givenSession;
	
	public static int cleanResults(String id) {
		return 0;
	}

	protected String getSessionId() {
		
		return getSession().getId();
	}
	
	public String getRemoteAddr() {
		
		if (givenSession != null) return "0.0.0.0";
		
		return  this.getThreadLocalRequest().getRemoteAddr();
	}
	
	public void setSession(HttpSession session) {
		
		givenSession = session;
	}
	
	protected HttpSession getSession() {
		
		if (givenSession != null) return givenSession;
		
		return this.getThreadLocalRequest().getSession();
	}
}
