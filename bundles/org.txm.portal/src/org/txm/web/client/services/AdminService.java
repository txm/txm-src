package org.txm.web.client.services;

import java.util.ArrayList;
import java.util.List;

import org.txm.web.shared.ServerState;
import org.txm.web.shared.UserData;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Admin RPC services declarations
 * @author vchabanis
 *
 */
@RemoteServiceRelativePath("admin")
public interface AdminService extends RemoteService {
	boolean addProfileToUser(String userName, String profileName, Boolean sendMail);
	
	boolean addUpdateUser(UserData data);
	boolean createProfile(String name);
	boolean deleteProfiles(String[] names);
	List<String> dumpProfiles();
	List<String> dumpUsers();
	
	String exportUsers();
	ArrayList<ArrayList<String>> getLastLogs(int size);
	String getProfileContent(String profileName);
	ServerState getServerState();
	String getTXMWEBCONF();
	List<String> getUsersProfiles(String userName);
	boolean importUsers(String file);
	ArrayList<String> listCorporaDirectory();

	List<String> listProfiles();
	
	List<String> listUsers();
	Boolean loadBinaryCorpus(String baseDir);
	boolean quickSend(String valueAsString);
	boolean reloadServerParameters();
	
	boolean removeBinaryCorpus(String basename);
	boolean removeProfileFromUser(String userName, String profileName);
	boolean removeUser(String[] userNames);

	boolean restartToolbox();

	boolean saveTXMWEBCONF(String content);
	
	String sendMail(String users, String profiles, String obj, String content);
	boolean setInscriptionEnable(boolean state);

	boolean setMailEnable(boolean state);

	boolean setMaintenance(boolean state);

	boolean setRemoteMaintenance(String code, boolean state, String message);

	String updateProfileContent(String content, String filePath);
}
