package org.txm.web.client.services;

import java.util.HashMap;
import java.util.List;

import org.txm.web.shared.TsQueryParam;
import org.txm.web.shared.TsQueryResult;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface TsQueryServiceAsync {
	void tsquery(TsQueryParam params, AsyncCallback<Boolean> callback);
	void getFirst(TsQueryParam params, AsyncCallback<TsQueryResult> callback);
	void getPrevious(TsQueryParam params, AsyncCallback<TsQueryResult> callback);
	void getNext(TsQueryParam params, AsyncCallback<TsQueryResult> callback);
	void getLast(TsQueryParam params, AsyncCallback<TsQueryResult> callback);
	void getFeatures(TsQueryParam params, AsyncCallback<HashMap<String, List<String>>> asyncCallback);
	void exportForest(TsQueryParam params, int contextsize, String method, boolean punct, String[] ntprops, String[] tprops, AsyncCallback<String> callback);
	void getPreviousSub(TsQueryParam params, AsyncCallback<TsQueryResult> callback);
	void getNextSub(TsQueryParam params, AsyncCallback<TsQueryResult> callback);
	void getGraph(TsQueryParam params, int nograph, int nosubgraph, AsyncCallback<TsQueryResult> callback);
	void setFeatures(TsQueryParam params, AsyncCallback<TsQueryResult> callback);
	void dropResult(TsQueryParam params, AsyncCallback<Void> asyncCallback);
}