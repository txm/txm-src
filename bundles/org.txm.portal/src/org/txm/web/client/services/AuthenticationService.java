package org.txm.web.client.services;

import java.util.List;

import org.txm.web.shared.LoginResult;
import org.txm.web.shared.UserData;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Authetication RPC services declarations
 * @author vchabanis
 *
 */
@RemoteServiceRelativePath("authentication")
public interface AuthenticationService extends RemoteService {
	LoginResult subscribe(UserData data);
	LoginResult askAccessAndSubscribe(UserData data, Boolean accessb);
	LoginResult activateAccount(String codes);
	LoginResult login(String login, String password);
	Boolean logout();
	Boolean forgotPassword(String mail);
	LoginResult recorverPassword(String code, String newpass, String passconfirm);
	
	UserData getUserData();
	String changeUserData(UserData data);
	Boolean askAccess(String login, String name, String firstname, String quality, String address, String mail, String tel, Boolean accessb);
	
	boolean autologout(String login);
	List<UserData> listUserInfos();
	String updateUserMailEND(String code);
	String checkAvailability(String login);
}
