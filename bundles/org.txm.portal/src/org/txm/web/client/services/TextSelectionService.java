package org.txm.web.client.services;

import java.util.HashMap;

import org.txm.web.shared.TextSelectionResult;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Admin RPC services declarations
 * @author vchabanis
 *
 */
@RemoteServiceRelativePath("textselection")
public interface TextSelectionService extends RemoteService {

	/**
	 * 
	 * @return all necessary data to open the TextSelection tab :
	 * texts ids
	 * text metadatas
	 * structuralProperties
	 */
	TextSelectionResult getAllStuff(String itemPath);
	HashMap<String, HashMap<String, String>> getValuesPerText(String itemPath);
	TextSelectionResult getStuffForBiblios(String itemPath);
}
