package org.txm.web.client.services;

import java.util.List;

import org.txm.web.shared.Actions;
import org.txm.web.shared.MenuBarContent;
import org.txm.web.shared.NavigationTree;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface UIServiceAsync {
	void fillSideNavTree(AsyncCallback<NavigationTree> callback);
	void fillMenuBar(AsyncCallback<MenuBarContent> callback);
	void getContextMenu(String itemPath, AsyncCallback<List<Actions>> callback);
	void getUserName(AsyncCallback<String> callback);
	void getUserMail(AsyncCallback<String> callback);
//	void getUserHelpPage(AsyncCallback<String> asyncCallback);
//	void getUserContactPage(AsyncCallback<String> asyncCallback);
	void getUserPage(String name, String locale, AsyncCallback<String> callback);
	void getPage(String path, String locale, AsyncCallback<String> callback);
}
