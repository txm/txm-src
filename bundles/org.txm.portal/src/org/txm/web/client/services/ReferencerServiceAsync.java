package org.txm.web.client.services;

import org.txm.web.shared.ReferencerParam;
import org.txm.web.shared.ReferencerResult;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface ReferencerServiceAsync {
	void references(ReferencerParam params, AsyncCallback<ReferencerResult> callback);
	void export(ReferencerParam params, AsyncCallback<String> asyncCallback);

}
