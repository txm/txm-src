package org.txm.web.client.services;

import org.txm.web.shared.CooccurrenceParam;
import org.txm.web.shared.CooccurrenceResult;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface CooccurrenceServiceAsync {
	
	void getLines(CooccurrenceParam params, AsyncCallback<CooccurrenceResult> callback);
	void cooccurrence(CooccurrenceParam params, AsyncCallback<String> callback);
	void exportCooccurrence(CooccurrenceParam params, AsyncCallback<String> asyncCallback);

}
