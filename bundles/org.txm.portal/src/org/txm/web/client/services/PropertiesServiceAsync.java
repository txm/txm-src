package org.txm.web.client.services;

import org.txm.web.shared.PropertiesResult;

import com.google.gwt.user.client.rpc.AsyncCallback;
/**
 * 
 * @author mdecorde
 *
 */
public interface PropertiesServiceAsync {
	void getTV(String path, String property, AsyncCallback<PropertiesResult> callback);
}
