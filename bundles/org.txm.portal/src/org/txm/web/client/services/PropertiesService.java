package org.txm.web.client.services;

import org.txm.web.exceptions.PermissionException;
import org.txm.web.shared.PropertiesResult;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


/**
 * Allow to get informations on a corpus
 * @author mdecorde
 */
@RemoteServiceRelativePath("properties")
public interface PropertiesService extends RemoteService {
	/**
	 * 
	 * @return "T: xx V: xx"
	 * @throws PermissionException 
	 */
	PropertiesResult getTV(String path, String property) throws PermissionException;
}
