package org.txm.web.client.services;

import org.txm.web.shared.ConcordanceParam;
import org.txm.web.shared.ConcordanceResult;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface ConcordanceServiceAsync {
	void concordance(ConcordanceParam params, AsyncCallback<String> callback);
	void deleteConcordance(ConcordanceParam params, AsyncCallback<Void> callback);
	void getLines(ConcordanceParam params, AsyncCallback<ConcordanceResult> callback);
	void exportConcordance(ConcordanceParam params, AsyncCallback<String> callback);
	void exportContext(ConcordanceParam params, AsyncCallback<String> callback);
	void sortResults(ConcordanceParam params, AsyncCallback<Boolean> callback);
}
