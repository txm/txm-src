package org.txm.web.client.services;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.txm.web.shared.AFCParams;
import org.txm.web.shared.ValeurPropreResultLine;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("afc")
public interface AFCService extends RemoteService {

    String analysisComputing(AFCParams params);

    ArrayList<ValeurPropreResultLine> getValeursPropres(AFCParams params);

    String[] getRowInfoTitles(AFCParams params);

    LinkedHashMap<String, List<Double>> getRowInfos(AFCParams params);

    String[] getColInfoTitles(AFCParams params);
    
    /**
     * 
     * @param params
     * @return the result file name
     */
    String exportCA(AFCParams params);

    LinkedHashMap<String, List<Double>> getColInfos(AFCParams params);
    
    String singularValues(AFCParams params);



}
