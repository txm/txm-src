package org.txm.web.client.services;

import org.txm.web.shared.ServerInfos;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface HelloServiceAsync {
	void sayHello(AsyncCallback<String> message);
	void startSession(AsyncCallback<ServerInfos> callback);
	void getServerInfos(AsyncCallback<ServerInfos> asyncCallback);
}
