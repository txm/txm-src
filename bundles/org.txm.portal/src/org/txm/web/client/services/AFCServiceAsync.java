package org.txm.web.client.services;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.txm.web.shared.AFCParams;
import org.txm.web.shared.ValeurPropreResultLine;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface AFCServiceAsync {


	void analysisComputing(AFCParams params, AsyncCallback<String> callback);

	void getValeursPropres(AFCParams params,
			AsyncCallback<ArrayList<ValeurPropreResultLine>> callback);

	void getRowInfos(AFCParams params, AsyncCallback<LinkedHashMap<String, List<Double>>> callback);

	void getRowInfoTitles(AFCParams params, AsyncCallback<String[]> callback);

	void getColInfos(AFCParams params, AsyncCallback<LinkedHashMap<String, List<Double>>> callback);

	void getColInfoTitles(AFCParams params, AsyncCallback<String[]> callback);

	void singularValues(AFCParams params, AsyncCallback<String> callback);

	void exportCA(AFCParams params, AsyncCallback<String> callback);

}
