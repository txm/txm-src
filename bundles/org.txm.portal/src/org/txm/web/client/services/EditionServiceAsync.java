package org.txm.web.client.services;

import java.util.ArrayList;
import java.util.HashMap;

import org.txm.web.shared.EditionResult;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface EditionServiceAsync {
	void backToText(String corpusID, ArrayList<String> selectedEditions, String textId, String wordId, AsyncCallback<ArrayList<EditionResult>> callback);
	void getEditionByName(String corpusID, String textid, ArrayList<String> selectedEditions,
			AsyncCallback<ArrayList<EditionResult>> asyncCallback);
	void getNextPage(String corpusID, String[] textIds, ArrayList<String> selectedEditions, String[] currentPages, AsyncCallback<ArrayList<EditionResult>> callback);
	void getPreviousPage(String corpusID, String[] textIds, ArrayList<String> selectedEditions, String[] currentPages,
			AsyncCallback<ArrayList<EditionResult>> callback);
	void getLastPage(String corpusID, String[] textIds, ArrayList<String> selectedEditions,
			String[] currentPages, AsyncCallback<ArrayList<EditionResult>> callback);
	void getFirstPage(String corpusID, String[] textIds, ArrayList<String> selectedEditions,
			String[] currentPages,
			AsyncCallback<ArrayList<EditionResult>> callback);
	void getNextText(String corpusID, String[] textIds, ArrayList<String> selectedEditions,
			AsyncCallback<ArrayList<EditionResult>> callback);
	void getPreviousText(String corpusID, String[] textIds,
			ArrayList<String> selectedEditions,
			AsyncCallback<ArrayList<EditionResult>> callback);
	void getEditionsForPosition(String corpusID, String edition, String[] textIds, ArrayList<String> selectedEditions, int position,
			AsyncCallback<EditionResult> callback);
	
	void getEdition(String corpusID, String editionName, AsyncCallback<EditionResult> callback);
	void getEditions(String corpusID, String text, ArrayList<String> selectedEditions, String[] currentPages, AsyncCallback<ArrayList<EditionResult>> callback);
	
	void hasPageWithID(String corpusID, String textid, String editionName, String pageid, AsyncCallback<Boolean> callback);
	void isCanSeeEdition(String entityPath, AsyncCallback<Boolean> callback);
//	void getHtmlContent(String localfile, AsyncCallback<String> callback);
	void getBiblio(String corpusID, String textId, AsyncCallback<String> callback);
	void getBiblios(String corpusID, AsyncCallback<ArrayList<String>> callback);
	void getTextIds(String corpusID, AsyncCallback<ArrayList<String>> callback);
	void getEditionNames(String path, AsyncCallback<HashMap<String, ArrayList<String>>> asyncCallback);
	void getParallelCorpora(String path, AsyncCallback<ArrayList<String>> asyncCallback);
	void getParallelLanguages(String path, AsyncCallback<ArrayList<String>> asyncCallback);
	void getParallelContexts(HashMap<String,String> queriesByLang, String struct, ArrayList<String> corpora, 
			ArrayList<String> props, ArrayList<String> refs, 
			ArrayList<Integer> CGs, ArrayList<Integer> CDs,
			ArrayList<Boolean> participates, AsyncCallback<HashMap<String, ArrayList<String>>> asyncCallback);
	void getPositions(String itemPath, String query, AsyncCallback<int[]> callback);
	void getDefaultEditions(String corpusID, AsyncCallback<ArrayList<String>> callback);

}
