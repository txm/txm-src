package org.txm.web.client.services;

import java.util.HashMap;
import java.util.List;

import org.txm.web.shared.ConcordanceParam;
import org.txm.web.shared.SelectionInfos;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface CorpusActionServiceAsync {
	void delete(String path, AsyncCallback<Boolean> callback);
//	void createSubcorpus(String path, String name, String query, AsyncCallback<Boolean> callback);
	
	void getReferences(String path, AsyncCallback<List<String>> callback);
	void getWordProperties(String path, AsyncCallback<List<String>> callback);
	void getStructuralUnitProperties(String itemPath, String structName, AsyncCallback<List<String>> callback);
	void getStructuralUnits(String itemPath, AsyncCallback<List<String>> callback);
	void getStructPropsValues(String itemPath, String structName, String propName, AsyncCallback<List<String>> callback);
	void getStructuralUnitsProperties(String itemPath, String string, AsyncCallback<HashMap<String, List<String>>> asyncCallback);
	void getStructuralUnitsProperties(String itemPath, AsyncCallback<List<String>> asyncCallback);
	void createSubcorpus(String path, String name, String structName, String structProperty, List<String> values, AsyncCallback<String> callback);
	void createSubcorpus(String path, String name, SelectionInfos infos, AsyncCallback<String> callback);
	void createPartition(String path, String name, String structName, String structProperty, AsyncCallback<String> callback);
	void createPartition(String path, String name, List<String> queries, AsyncCallback<String> callback);
	
	void getBases(AsyncCallback<List<String>> callback);

	void getSelectionInfos(String path, AsyncCallback<SelectionInfos> callback);

	void getParentPath(String path, AsyncCallback<String> callback);

	void reportActivity(String cmd, String parameters, AsyncCallback<Boolean> callback);

	void getNumberOfTexts(String path, AsyncCallback<Integer> callback);
	void getFirstTextID(String path, AsyncCallback<String> callback);

	void getRightContextSize(String itemPath, AsyncCallback<Integer> asyncCallback);
	void getLeftContextSize(String itemPath, AsyncCallback<Integer> asyncCallback);
	void getViewProperties(String itemPath, AsyncCallback<List<String>> callback);
	void getSortProperties(String itemPath, AsyncCallback<List<String>> callback);
	void getRefProperties(String itemPath, AsyncCallback<List<String>> callback);
	void getConcordanceDefaultParameters(String itemPath, AsyncCallback<ConcordanceParam> asyncCallback);
}
