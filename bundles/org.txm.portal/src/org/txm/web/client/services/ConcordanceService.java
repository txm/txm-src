package org.txm.web.client.services;

import org.txm.web.shared.ConcordanceParam;
import org.txm.web.shared.ConcordanceResult;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Concordance RPC services declarations
 * @author vchabanis
 *
 */
@RemoteServiceRelativePath("concordance")
public interface ConcordanceService extends RemoteService {
	String concordance(ConcordanceParam params);
	void deleteConcordance(ConcordanceParam params);
	ConcordanceResult getLines(ConcordanceParam params);
	String exportConcordance(ConcordanceParam params);
	String exportContext(ConcordanceParam params);
	boolean sortResults(ConcordanceParam params);
}
