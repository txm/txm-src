package org.txm.web.client.services;

import org.txm.web.client.TxmI18N;
import org.txm.web.shared.ServerInfosUpdater;

import com.google.gwt.core.client.GWT;

/**
 * stores an instance of each service too simplify their access
 * @author vchabanis
 *
 */
public class Services {

	private static TxmI18N i18n = (TxmI18N)GWT.create(TxmI18N.class);
	private static UIServiceAsync UIService = GWT.create(UIService.class);
	private static HelloServiceAsync helloService = GWT.create(HelloService.class);
	private static CorpusActionServiceAsync corpusActionService = GWT.create(CorpusActionService.class);
	private static EditionServiceAsync editionService = GWT.create(EditionService.class);
	private static ConcordanceServiceAsync concordanceService = GWT.create(ConcordanceService.class);
	private static TsQueryServiceAsync tsqueryService = GWT.create(TsQueryService.class);
	private static VocabularyServiceAsync vocabularyService = GWT.create(VocabularyService.class);
	private static ReferencerServiceAsync referencerService = GWT.create(ReferencerService.class);
	private static AuthenticationServiceAsync authenticationService = GWT.create(AuthenticationService.class);
	private static AdminServiceAsync adminService = GWT.create(AdminService.class);
	private static PropertiesServiceAsync propertiesService = GWT.create(PropertiesService.class);
	private static TextSelectionServiceAsync selectionService = GWT.create(TextSelectionService.class);
	private static CooccurrenceServiceAsync cooccurrenceService = GWT.create(CooccurrenceService.class);
	private static SpecificitiesServiceAsync specificitiesService = GWT.create(SpecificitiesService.class);
	private static AFCServiceAsync afcService = GWT.create(AFCService.class);
	

	
	public static EditionServiceAsync getEditionService() {
		ServerInfosUpdater.alive();
		return editionService;
	}
	
	public static TsQueryServiceAsync getTsQueryService() {
		ServerInfosUpdater.alive();
		return tsqueryService;
	}
	
	public static TextSelectionServiceAsync getTextSelectionService() {
		ServerInfosUpdater.alive();
		return selectionService;
	}

	public static ConcordanceServiceAsync getConcordanceService() {
		ServerInfosUpdater.alive();
		return concordanceService;
	}
	
	public static CooccurrenceServiceAsync getCooccurrenceService() {
		ServerInfosUpdater.alive();
		return cooccurrenceService;
	}

	public static VocabularyServiceAsync getVocabularyService() {
		ServerInfosUpdater.alive();
		return vocabularyService;
	}

	public static TxmI18N getI18n() {
		return i18n;
	}

	public static UIServiceAsync getUIService() {
		ServerInfosUpdater.alive();
		return UIService;
	}

	public static HelloServiceAsync getHelloService() {
		ServerInfosUpdater.alive();
		return helloService;
	}

	public static CorpusActionServiceAsync getCorpusActionService() {
		ServerInfosUpdater.alive();
		return corpusActionService;
	}

	public static AuthenticationServiceAsync getAuthenticationService() {
		ServerInfosUpdater.alive();
		return authenticationService;
	}
	
	public static AdminServiceAsync getAdminService() {
		ServerInfosUpdater.alive();
		return adminService;
	}
	
	public static PropertiesServiceAsync getPropertiesService() {
		ServerInfosUpdater.alive();
		return propertiesService;
	}

	public static ReferencerServiceAsync getReferencesService() {
		ServerInfosUpdater.alive();
		return referencerService;
	}
	
	public static SpecificitiesServiceAsync getSpecificitiesService(){
		ServerInfosUpdater.alive();
		return specificitiesService;		
	}
	
	public static AFCServiceAsync getAFCService(){
		ServerInfosUpdater.alive();
		return afcService;
	}
}
