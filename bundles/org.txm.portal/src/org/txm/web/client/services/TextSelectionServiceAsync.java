package org.txm.web.client.services;

import java.util.HashMap;

import org.txm.web.shared.TextSelectionResult;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface TextSelectionServiceAsync {

	void getAllStuff(String itemPath, AsyncCallback<TextSelectionResult> callback);

	void getValuesPerText(String itemPath,
			AsyncCallback<HashMap<String, HashMap<String, String>>> callback);
	
	void getStuffForBiblios(String itemPath, AsyncCallback<TextSelectionResult> callback);
}
