package org.txm.web.client.services;

import java.util.List;

import org.txm.web.shared.LoginResult;
import org.txm.web.shared.UserData;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface AuthenticationServiceAsync {
	void subscribe(UserData data, AsyncCallback<LoginResult> callback);
	void activateAccount(String codes, AsyncCallback<LoginResult> callback);
	void login(String login, String password, AsyncCallback<LoginResult> callback);
	void logout(AsyncCallback<Boolean> asyncCallback);
	void forgotPassword(String mail, AsyncCallback<Boolean> callback);
	void recorverPassword(String code, String newpass, String passconfirm,
			AsyncCallback<LoginResult> callback);
	
	void autologout(String login, AsyncCallback<Boolean> asyncCallback);
	void getUserData(AsyncCallback<UserData> asyncCallback);
	void changeUserData(UserData data, AsyncCallback<String> asyncCallback);
	void askAccess(String login, String name, String firstname, String quality,
			String address, String mail, String tel,
			Boolean accessb, AsyncCallback<Boolean> callback);
	void listUserInfos(AsyncCallback<List<UserData>> callback);
	void askAccessAndSubscribe(UserData data, Boolean accessb,
			AsyncCallback<LoginResult> callback);
	void updateUserMailEND(String code, AsyncCallback<String> asyncCallback);
	void checkAvailability(String login, AsyncCallback<String> asyncCallback);
}
