package org.txm.web.client.services;

import java.util.ArrayList;
import java.util.Map;

import org.txm.web.shared.SpecificitiesParam;
import org.txm.web.shared.SpecificitiesResultLine;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface SpecificitiesServiceAsync {
	
	void specificities(SpecificitiesParam params, AsyncCallback<Boolean> callback);
	
	void getLines(SpecificitiesParam params, AsyncCallback<Map<String, ArrayList<SpecificitiesResultLine>>>  callback);

	void exportSpecificities(SpecificitiesParam params, AsyncCallback<String> asyncCallback);

}
