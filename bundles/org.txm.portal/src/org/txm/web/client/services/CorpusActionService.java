package org.txm.web.client.services;

import java.util.HashMap;
import java.util.List;

import org.txm.web.shared.ConcordanceParam;
import org.txm.web.shared.SelectionInfos;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Corpus RPC services declarations
 * @author vchabanis
 *
 */
@RemoteServiceRelativePath("corpusaction")
public interface CorpusActionService extends RemoteService {
	boolean delete(String path);
	
	List<String> getReferences(String path);
	List<String> getWordProperties(String path);
	List<String> getStructuralUnitProperties(String itemPath, String structName);
	List<String> getStructuralUnits(String itemPath);
	List<String> getStructPropsValues(String itemPath, String structName, String propName);
	HashMap<String, List<String>> getStructuralUnitsProperties(String itemPath, String string);
	String createSubcorpus(String path, String name, String structName, String structProperty, List<String> values);
	String createPartition(String path, String name, List<String> queries);
	String createPartition(String path, String name, String structName, String structProperty);
	SelectionInfos getSelectionInfos(String path);
	String getParentPath(String path);
	Integer getNumberOfTexts(String path);
	List<String> getBases();

	String createSubcorpus(String path, String name, SelectionInfos infos);

	List<String> getStructuralUnitsProperties(String itemPath);
	
	List<String> getViewProperties(String itemPath);
	List<String> getSortProperties(String itemPath);
	List<String> getRefProperties(String itemPath);
	
	boolean reportActivity(String cmd, String parameters);

	String getFirstTextID(String path);

	int getRightContextSize(String itemPath);
	int getLeftContextSize(String itemPath);

	ConcordanceParam getConcordanceDefaultParameters(String itemPath);
}
