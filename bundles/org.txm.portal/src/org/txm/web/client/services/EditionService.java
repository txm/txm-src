package org.txm.web.client.services;

import java.util.ArrayList;
import java.util.HashMap;

import org.txm.web.exceptions.PermissionException;
import org.txm.web.shared.EditionResult;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Edition RPC services declarations
 * @author vchabanis, mdecorde
 *
 */
@RemoteServiceRelativePath("edition")
public interface EditionService extends RemoteService {
	ArrayList<EditionResult> backToText(String corpusID,
			ArrayList<String> selectedEditions, String textId, String wordId) throws PermissionException;
	ArrayList<EditionResult> getEditionByName(String corpusID, String textid,
			ArrayList<String> selectedEditions) throws PermissionException;
	ArrayList<EditionResult> getNextPage(String corpusID, String[] textIds,
			ArrayList<String> selectedEditions, String[] currentPages) throws PermissionException;
	ArrayList<EditionResult> getPreviousPage(String corpusID, String[] textIds,
			ArrayList<String> selectedEditions, String[] currentPages) throws PermissionException;
	ArrayList<EditionResult> getLastPage(String corpusID, String[] textIds,
			ArrayList<String> selectedEditions, String[] currentPages);
	ArrayList<EditionResult> getFirstPage(String corpusID, String[] textIds,
			ArrayList<String> selectedEditions, String[] currentPages) throws PermissionException;
	ArrayList<EditionResult> getNextText(String corpusID, String[] textIds,
			ArrayList<String> selectedEditions) throws PermissionException;
	ArrayList<EditionResult> getPreviousText(String corpusID, String[] textIds,
			ArrayList<String> selectedEditions) throws PermissionException;
	EditionResult getEditionsForPosition(String itemPath, String edition,
			String[] textIds, ArrayList<String> selectedEditions, int position);
	
	ArrayList<String> getDefaultEditions(String corpusID) throws PermissionException;
	EditionResult getEdition(String corpusID, String editionName) throws PermissionException;
	ArrayList<EditionResult> getEditions(String corpusID, String text,
			ArrayList<String> selectedEditions2, String[] currentPages) throws PermissionException;
	String getBiblio(String corpusID, String textId);
	ArrayList<String> getBiblios(String corpusID);
	ArrayList<String> getTextIds(String corpusID);
	//String getHtmlContent(String localfile);
	boolean isCanSeeEdition(String entityPath);
	HashMap<String, ArrayList<String>> getEditionNames(String path);
	
	ArrayList<String> getParallelCorpora(String path);
	ArrayList<String> getParallelLanguages(String path);
	HashMap<String, ArrayList<String>> getParallelContexts(HashMap<String,String> queriesByLang, String struct, 
			ArrayList<String> corpora, 
			ArrayList<String> props, ArrayList<String> refs, 
			ArrayList<Integer> CGs, ArrayList<Integer> CDs,
			ArrayList<Boolean> participates);
	int[] getPositions(String itemPath, String query);
	Boolean hasPageWithID(String corpusID, String text, String edition,
			String page);


	
	// editionResult must contains the word Ids (to highlight words + create QueryNavigation{matches, wordids, textids, currentMatch}
	//ArrayList<EditionResult> startQueryNavigation(String path, String[] editionNames, String query, String edition) throws PermissionException;
	//ArrayList<EditionResult> nextMatch(String path, String[] editionNames) throws PermissionException;
	//ArrayList<EditionResult> previousMatch(String path, String[] editionNames) throws PermissionException;
}
