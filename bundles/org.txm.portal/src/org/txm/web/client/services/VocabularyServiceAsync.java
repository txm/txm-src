package org.txm.web.client.services;

import java.util.ArrayList;

import org.txm.web.shared.VocabularyParam;
import org.txm.web.shared.VocabularyResult;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface VocabularyServiceAsync {
	void vocabulary(VocabularyParam params, AsyncCallback<ArrayList<String[]>> callback);
	void index(VocabularyParam params, AsyncCallback<VocabularyResult> callback);
	void getColumnTitles(String entity,	AsyncCallback<ArrayList<String>> callback);
	void export(VocabularyParam params, AsyncCallback<String> asyncCallback);
}
