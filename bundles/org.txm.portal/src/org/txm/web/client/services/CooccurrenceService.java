package org.txm.web.client.services;

import org.txm.web.shared.CooccurrenceParam;
import org.txm.web.shared.CooccurrenceResult;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("cooccurrence")
public interface CooccurrenceService extends RemoteService{
	
	CooccurrenceResult getLines(CooccurrenceParam params);
	String cooccurrence(CooccurrenceParam params);
	String exportCooccurrence(CooccurrenceParam params);
}
