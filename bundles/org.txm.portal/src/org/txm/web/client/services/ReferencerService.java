package org.txm.web.client.services;

import org.txm.web.exceptions.PermissionException;
import org.txm.web.shared.ReferencerParam;
import org.txm.web.shared.ReferencerResult;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Referencer RPC services declarations
 * @author mdecorde
 *
 */
@RemoteServiceRelativePath("referencer")
public interface ReferencerService extends RemoteService {
	ReferencerResult references(ReferencerParam params) throws PermissionException;
	String export(ReferencerParam params);
}
