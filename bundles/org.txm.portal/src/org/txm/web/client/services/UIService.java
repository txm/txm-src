package org.txm.web.client.services;

import java.util.List;

import org.txm.web.shared.Actions;
import org.txm.web.shared.MenuBarContent;
import org.txm.web.shared.NavigationTree;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * UI RPC services declarations
 * @author vchabanis
 *
 */
@RemoteServiceRelativePath("UI")
public interface UIService extends RemoteService {
	/**
	 * @return the content of the SideNavTree
	 */
	NavigationTree fillSideNavTree() throws Exception ;
	/**
	 * @return the content of  the menubar
	 */
	MenuBarContent fillMenuBar();
	/**
	 * @return the Actions the user can do on an entity
	 */
	List<Actions> getContextMenu(String itemPath);
	/**
	 * @return the users name
	 */
	String getUserName();
	/**
	 * @return the user's first profile homepage
	 */
	String getUserPage(String name, String locale);
	
	String getPage(String path, String locale);
	
	/**
	 * return the user's mail
	 * @return
	 */
	String getUserMail();
	
	/**
	 * @return the relative url to the user's profile help page
	 */
//	String getUserHelpPage();
	
	/**
	 * @return the relative url to the user's profile help page
	 */
//	String getUserContactPage();

}