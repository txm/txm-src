package org.txm.web.client;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.CenterPanel;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.ServerMessagesBar;
import org.txm.web.client.widgets.SideNavTree;
import org.txm.web.client.widgets.TxmConsole;
import org.txm.web.client.widgets.TxmMenuBar;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.forms.RecorverPasswordForm;
import org.txm.web.client.widgets.tabs.CMSTab;
import org.txm.web.client.widgets.tabs.CorporaTab;
import org.txm.web.shared.Keys;
import org.txm.web.shared.LoginResult;
import org.txm.web.shared.ServerInfos;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.Location;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
/**
 * TXMWEB GWT's entrypoint
 * 
 * @author vchabanis, mdecorde
 *
 */
public class TXMWEB implements EntryPoint {

	public static String VERSION = "0.6.3";
	public static String PORTALTITLE = "";
	public static String PORTALSHORTNAME = "TXM";
	public static String PORTALLONGNAME = "TXM WEB";
	public static String CONTACT = "nocontact@nocontact.com";
	public static boolean RENABLE = false;
	public static boolean MAILENABLE = false;
	public static boolean INSCRIPTIONENABLE = false;
	public static boolean READY = false;
	public static boolean MAINTENANCE = false;
	public static boolean SHOWPUBLICPAGES = true;
	public static boolean SHOWPRIVATEPAGES = true;
	public static boolean EXPO = false; // if true the portal is in demonstration mode : simplified
	public static String DEFAULTEDITIONPANELSIZE = "100%";
	public static String BASEURL = "";

	public static boolean sideNavTreeFirstLoad = true;

	private static ServerMessagesBar messageBar = null;
	private static ToolStrip cookiesBar = null;
	private static TxmMenuBar menuBar = null;
	private static SideNavTree sideNav = null;
	private static CorporaTab corporaTab;
	private static CenterPanel centerPanel = null;
	private static CenterPanel leftPanel = null;
	private static String defaultLocale = "fr";
	private static TxmConsole console;
	//private static LinkedList<String> consoleContent = new LinkedList<String>();
	public static boolean LOGGED = false;
	
	DateTimeFormat f = DateTimeFormat.getFormat("yyyy-MM-dd");//new DateTimeFormat();
	Date lastYearDay = new Date();
	
	private VLayout mainLayout;
	
	public static native String getUserAgent() /*-{
		return navigator.userAgent.toLowerCase();
	}-*/;

	/**
	 * Called at the beginning :
	 * 	- Call the hello service
	 * 	- Build the UI. Each component has its own initialisation service
	 */
	public void onModuleLoad() {

		//		System.out.println("USER AGENT: "+getUserAgent());
		//		if (getUserAgent().contains("Safari")) {
		//			SC.warn(Services.getI18n().safariAlert());
		//		}

		//		String URL = Page.getSkinDir();
		//		//default: txmweb/sc/skins/Graphite/
		//		String URL2 = "txmweb/sc/skins/Enterprise/";
		//		System.out.println("SKIN DIR: "+URL);
		//		Page.setSkinDir(URL2);

		console = new TxmConsole(50);
		Window.addWindowClosingHandler(new Window.ClosingHandler() {
			public void onWindowClosing(Window.ClosingEvent closingEvent) {
				String code = Window.Location.getParameter("recovery");
				if (code != null) return;
				code = Window.Location.getParameter("updateMail");
				if (code != null) return;
				code = Window.Location.getParameter("confirmation");
				if (code != null) return;

				closingEvent.setMessage(Services.getI18n().close_warning());//"");
			}
		});

		String cookies = Cookies.getCookie("txm_cookies_informed");
		//		com.google.gwt.user.datepicker.client.CalendarUtil.
		//		com.google.gwt.user.datepicker.client.CalendarUtil oneYearOlder = Calendar.getInstance();
		
		//oneYearOlder.add(Calendar.MINUTE, 12);
		
		if (!("checked".equals(cookies))) {
			
			cookiesBar = new ToolStrip();
			cookiesBar.setID("cookiesBar");
			cookiesBar.setSeparatorSize(20);
			cookiesBar.setBorder("5px solid orange");
			
			Label l = new Label("<br><b>"+Services.getI18n().cookiesMessage("callTXMCommand('{&quot;command&quot;: &quot;page&quot;, &quot;path&quot;: &quot;Cookies&quot;}')")+"</b><br><br>");
			l.setID("cookiesBarMessage");
			l.setWidth100();
			l.setValign(VerticalAlignment.CENTER);
			l.setAlign(com.smartgwt.client.types.Alignment.CENTER);
			
			cookiesBar.addMember(new LayoutSpacer());
			cookiesBar.addMember(l);
			cookiesBar.addMember(new LayoutSpacer());
			
			ToolStripButton button = new ToolStripButton();
			button.setID("cookiesBarButton");
			button.setTitle("<b>"+Services.getI18n().closeButton()+"</b>");
			button.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
				public void onClick(ClickEvent event) {
					Date d = new Date();
					d.setYear(d.getYear() + 1);
					Cookies.setCookie("txm_cookies_informed", "checked", d);
					mainLayout.removeMember(cookiesBar);
				}
			});
			cookiesBar.addMember(button);
//			SC.say(Services.getI18n().cookiesMessage(), new BooleanCallback() {
//				public void execute(Boolean value) {
////					if (value != null && value) {
//							Cookies.setCookie("cookies", f.format(new Date()));
//							mainLayout.removeMember(cookiesBar);
////					} else {
////						//RootPanel.getBodyElement().removeAllChildren();
////						RootPanel.getBodyElement().setInnerHTML("Cookies were not autorized. Reload the page to change.");
////					}
//				}
//			});
		}
		// expose the Commands.call() method -> in JS use : $wnd.call(cmd, path, textid, query, editions, page, wordids, props, login)


		// Starting http session
		Services.getHelloService().startSession(new AsyncCallback<ServerInfos>() {

			public void onSuccess(ServerInfos result) {
				if (result == null) {
					TxmDialog.severe(Services.getI18n().internalError()+" unexpected error during initialization.");
					return;
				}
				if (result.error.length() > 0) {
					if (result.error.equals("TBX: initializing")) {
						TxmDialog.info(Services.getI18n().portal_restarting());
						Timer t1 = new Timer() {
							@Override
							public void run() {
								Window.Location.reload();
							}
						};
						t1.schedule(5*60000);
					} else {
						TxmDialog.severe(Services.getI18n().internalError()+" : "+result.error);
					}
					return;
				}
				PORTALTITLE = result.portaltitle;
				PORTALSHORTNAME = result.portalname;
				PORTALLONGNAME = result.portallongname;
				RENABLE = result.rEnable;
				MAILENABLE = result.mailEnable;
				INSCRIPTIONENABLE = result.inscriptionEnable;
				READY = result.ready;
				MAINTENANCE = result.maintenance;
				CONTACT = result.contact;
				SHOWPUBLICPAGES = result.showPublicPages;
				SHOWPRIVATEPAGES = result.showPrivatePages;
				DEFAULTEDITIONPANELSIZE = result.defaultEditionPanelSize;
				BASEURL = result.baseURL;
				if (BASEURL.endsWith("/")) { // remove a trailing "/" of the URL
					BASEURL = BASEURL.substring(0, BASEURL.length() - 1);
				}
				EXPO = false;//result.expo; // server configured as expo
				testExpo(); // and fetch the "expo" URL option
				if (EXPO) System.out.println("Mode expo enabled.");

				mainLayout = new VLayout();
				VLayout sideNavLayout = new VLayout();
				HLayout hLayout = new HLayout();
				VLayout centerLayout = new VLayout();

				menuBar = new TxmMenuBar(Services.getI18n().mainTitle());
				sideNav = new SideNavTree();
				messageBar = new ServerMessagesBar();
				centerPanel = new CenterPanel("CENTERPANEL");
				leftPanel = new CenterPanel("LEFTPANEL");
				leftPanel.setWidth100();
				console = new TxmConsole(50);
				console.setLayoutLeftMargin(10);

				//show homepage
				Commands.callHome();

				mainLayout.setWidth100();
				mainLayout.setHeight100();
				sideNavLayout.setHeight100();
				sideNavLayout.setWidth(150);
				sideNavLayout.setShowResizeBar(!TXMWEB.EXPO);
				centerLayout.setWidth100();
				centerLayout.setHeight100();

				corporaTab = new CorporaTab("Corpus");
				corporaTab.setPane(sideNav);
				corporaTab.setCanClose(false);

				leftPanel.addTab(corporaTab);

				log(Services.getI18n().startOK());

				sideNavLayout.setMembers(leftPanel);			
				centerLayout.setMembers(messageBar, centerPanel, console);

				hLayout.setMembers(sideNavLayout, centerLayout);

				//hLayout.setShowResizeBar(!TXMWEB.EXPO);
				hLayout.setCanDragResize(false);

				//hLayout.setBorder("1px solid #BBBBBB");

				if (cookiesBar != null) mainLayout.addMember(cookiesBar);
				mainLayout.addMember(menuBar);
				mainLayout.addMember(hLayout);

				mainLayout.draw();

				if (EXPO) {
					//					leftPanel.setShowResizeBar(false);
					//					sideNavLayout.setShowResizeBar(false);

					messageBar.disable();
					messageBar.hide();

					console.disable();
					console.hide();

					CMSTab queriesTab = new CMSTab(
					Commands.createParameters(
							Keys.PATH, "/LeftPanel_expo", 
							Keys.PROFILE, null, 
							Keys.LOCALE, null, 
							Keys.TITLE, "Queries", 
						Keys.TOOLTIP, "select a query in the following list."));
					queriesTab.update();
					//queriesTab.setTitle("test");
					leftPanel.removeTab(corporaTab);
					leftPanel.addTab(queriesTab);
					leftPanel.getTabBar().hide(); // hide tab pickers
					leftPanel.getTabBar().setHeight(0);
					centerPanel.getTabBar().hide();
					centerPanel.getTabBar().setHeight(0);
					centerLayout.setMembers(messageBar, centerPanel);
				}

				testMaintenance();
				testActivation();
				testRecovery();
				testLogin();
				testMailUpdate();
				//testCommand(); // done after side nav tree is ready

				DOM.removeChild(RootPanel.getBodyElement(), DOM.getElementById("SplashScreen"));
				messageBar.hide();
				console.addMessage(PORTALSHORTNAME+ " - "+VERSION);

				new Commands().exportCommands();
			}

			public void onFailure(Throwable caught) {
				SC.warn(Services.getI18n().sessionInitError()+"<br/>Error="+caught);
				caught.printStackTrace();
				error(Services.getI18n().internalError()+" Failed to start session. Try later.");
			}
		});		
	}

	public static CorporaTab getCorporaTab() {
		return corporaTab;
	}

	public static void updateFavicon(String src) {
		Element link = DOM.createElement("link");
		link.setAttribute("id", "dynamic-favicon");
		link.setAttribute("rel", "shortcut icon");
		link.setAttribute("type", "image/x-icon");
		link.setAttribute("href", src);

		Element old = DOM.getElementById("dynamic-favicon");

		if (old != null) {
			//System.out.println("Remove old link");
			com.google.gwt.dom.client.Element head = old.getParentElement();
			old.removeFromParent();
			//System.out.println("Append new link: "+src);
			head.appendChild(link);
		}	
	}

	public static native void changeFavicon(String src)/*-{
		document.head = document.head || document.getElementsByTagName('head')[0];

		var link = document.createElement('link'),
     	oldLink = document.getElementById('dynamic-favicon');
 		link.id = 'dynamic-favicon';
 		link.rel = 'shortcut icon';
 		link.type= 'image/x-icon';
 		link.href = src;
 		if (oldLink) {
  			document.head.removeChild(oldLink);
 		}
 		document.head.appendChild(link);  
	}-*/;

	public static void testCommand() {
		String cmd = Window.Location.getParameter(Keys.COMMAND);

		HashMap<String, String> parameters = new HashMap<String, String>();
		Set<String> keys = Window.Location.getParameterMap().keySet();
		//SC.say("HTML parameter keys="+keys);
		for (String k : keys) {
			parameters.put(k, Window.Location.getParameter(k));
		}
		//SC.say("HTML parameter keys="+keys+" built parameters="+parameters);
		// no need to test if parameters are empty or null
		
		if (cmd != null && cmd.trim().length() > 0) {
			Commands.execute(cmd, parameters);
		}
	}

	public static void logout()
	{
		if (getMenuBar() == null) {
			return; // the init failed
		}
		getMenuBar().updateMenuBar();
		getSideNavTree().updateTree();

		CenterPanel center = getCenterPanel();
		Tab[] tabs = center.getTabs();
		int[] tabsToRemove = new int[tabs.length - 1];
		int cnt = 0;
		for (int i = 1; i < tabs.length; i++) {
			tabsToRemove[cnt] = i;
			cnt++;
		}
		center.removeTabs(tabsToRemove);
		getCenterPanel().updateUserPages();
		getConsole().addMessage(Services.getI18n().disconnected());
	}

	private void testMaintenance() {

		if (MAINTENANCE) {
			String message = "Site off-line. "+PORTALSHORTNAME+
					" is currently under maintenance. We would like to thank you for your patience while we are working on essential upgrades for the system."+
					"To contact the site administrator in the meantime please email: "+CONTACT;
			TxmDialog.warning(message);
		}
	}

	public void testLogin() {
		//		String login = Window.Location.getParameter("login");
		//		Commands.callLogin(login);
	}

	public static UrlBuilder getUrlBuilder() {
		UrlBuilder urlBuilder = new UrlBuilder();

		try {urlBuilder.setHash(Window.Location.getHash());} catch(Exception e) {}
		try {urlBuilder.setHost(Window.Location.getHost());} catch(Exception e) {}
		try {urlBuilder.setPath(Window.Location.getPath());} catch(Exception e) {}
		try {urlBuilder.setPort(Integer.parseInt(Window.Location.getPort()));} catch(Exception e) {}
		try {urlBuilder.setProtocol(Window.Location.getProtocol());} catch(Exception e) {}
		Map<String, List<String>> params = Window.Location.getParameterMap();
		for (String key : params.keySet()) {
			urlBuilder.setParameter(key, params.get(key).toArray(new String[params.get(key).size()]));
		}
		return urlBuilder;
	}

	public static void restartWithoutParam(String param) {
		UrlBuilder urlB = TXMWEB.getUrlBuilder();
		urlB.removeParameter(param);
		Window.Location.replace(urlB.buildString());
	}

	private void testRecovery() {
		String code = Window.Location.getParameter("recovery");
		if (code != null && code.length() > 0) {
			RecorverPasswordForm popup = new RecorverPasswordForm(code);
			popup.show();
		}
	}

	private void testExpo() {
		String code = Window.Location.getParameter("mode");
		System.out.println("mode value: "+code);
		if ("expo".equals(code)) EXPO=true;
		if ("normal".equals(code)) EXPO=false;
	}

	private void testMailUpdate() {
		String code = Window.Location.getParameter("updateMail");
		if (code != null && code.length() > 0) {
			Services.getAuthenticationService().updateUserMailEND(code, new AsyncCallback<String>() {
				@Override
				public void onFailure(Throwable caught) {
					TxmDialog.severe(Services.getI18n().internalError()+" Error during mail update: "+caught);
					//caught.printStackTrace();
				}

				@Override
				public void onSuccess(String result) {
					if (result == null || result.length() > 0) {
						if (result == null)
							result = (Services.getI18n().internalError());
						else if (result.startsWith("code"))
							result = Services.getI18n().mail_update_code_does_not_exists();//"Ce code de mise à jour n'existe pas";
						else if (result.startsWith("delay"))
							result = Services.getI18n().mail_update_code_expired();//"Le delais de mise à jour de l'email est expiré";

						ProcessingWindow.hide();
						TxmDialog.severe(result);
						TXMWEB.getConsole().addWarning(result);
					} else {
						restartWithoutParam("updateMail");
						/*TXMWEB.getSideNavTree().updateTree();
						TXMWEB.getMenuBar().updateMenuBar();
						TXMWEB.getCenterPanel().updateUserPages();
						TXMWEB.getConsole().addMessage(Services.getI18n().emailUpdated());*/
					}
				}
			});
		}
	}

	private void testActivation() {
		String code = Window.Location.getParameter("confirmation");

		if (code != null && code.length() > 0) {
			Services.getAuthenticationService().activateAccount(code, new AsyncCallback<LoginResult>() {
				@Override
				public void onFailure(Throwable caught) {
					TxmDialog.severe(Services.getI18n().internalError()+" Error during account activation: "+caught);
					caught.printStackTrace();
				}

				@Override
				public void onSuccess(LoginResult result) {
					if (!result.isSuccess()) {
						ProcessingWindow.hide();
						TXMWEB.getConsole().addWarning(result.getHomePage());
					} else {
						restartWithoutParam("confirmation");
						/*TXMWEB.getSideNavTree().updateTree();
						TXMWEB.getMenuBar().updateMenuBar();
						TXMWEB.getCenterPanel().updateUserPages();
						TXMWEB.getConsole().addMessage(Services.getI18n().emailUpdated());*/
					}
				}
			});
		}
	}

	public static TxmMenuBar getMenuBar() {
		return menuBar;
	}

	public static SideNavTree getSideNavTree() {
		return sideNav;
	}

	public static CenterPanel getCenterPanel() {
		return centerPanel;
	}

	public static CenterPanel getLeftPanel() {
		return leftPanel;
	}


	public static TxmConsole getConsole() {
		return console;
	}

	public static ServerMessagesBar getMessagesBar()
	{
		return messageBar;
	}

	/**
	 * Log a message in the console
	 * @param message
	 */
	public static void log(String message) {
		if (console != null) {
			console.addMessage(message);
		}
	}

	/**
	 * Log a message in the console
	 * @param message
	 */
	public static void error(String message) {
		if (console != null)
			console.addError(message);
	}

	public static String getLocale() {
		String locale = Location.getParameter("locale");
		locale = (locale != null && !locale.equals("") ? locale : defaultLocale);
		return locale;
	}
}
