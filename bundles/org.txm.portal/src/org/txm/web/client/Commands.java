package org.txm.web.client;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.ProcessingWindow;
import org.txm.web.client.widgets.dialogs.TxmDialog;
import org.txm.web.client.widgets.forms.AFCForm;
import org.txm.web.client.widgets.forms.BiblioForm;
import org.txm.web.client.widgets.forms.ContactForm;
import org.txm.web.client.widgets.forms.DocumentationForm;
import org.txm.web.client.widgets.forms.EditionForm;
import org.txm.web.client.widgets.forms.Helpform;
import org.txm.web.client.widgets.forms.HomeForm;
import org.txm.web.client.widgets.forms.LoginForm;
import org.txm.web.client.widgets.forms.LogoutForm;
import org.txm.web.client.widgets.forms.PageForm;
import org.txm.web.client.widgets.forms.PartitionForm;
import org.txm.web.client.widgets.forms.ProfileForm;
import org.txm.web.client.widgets.forms.RemoveForm;
import org.txm.web.client.widgets.forms.SubcorpusForm;
import org.txm.web.client.widgets.forms.SubscribeForm;
import org.txm.web.client.widgets.forms.TextsForm;
import org.txm.web.client.widgets.tabs.CMSTab;
import org.txm.web.client.widgets.tabs.ConcordanceTab;
import org.txm.web.client.widgets.tabs.ContextParaTab;
import org.txm.web.client.widgets.tabs.ContextTab;
import org.txm.web.client.widgets.tabs.CooccurrencesTab;
import org.txm.web.client.widgets.tabs.IndexTab;
import org.txm.web.client.widgets.tabs.PropertiesTab;
import org.txm.web.client.widgets.tabs.ReferencerTab;
import org.txm.web.client.widgets.tabs.SpecificitiesTab;
import org.txm.web.client.widgets.tabs.TsQueryTab;
import org.txm.web.client.widgets.tabs.admin.AdminTab;
import org.txm.web.client.widgets.tabs.textselection.TextSelectionTab;
import org.txm.web.shared.Keys;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.Window;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.tree.TreeNode;

/**
 * Convenience class to open command editor or form
 * @author mdecorde
 *
 */
public class Commands {
	
	public static void callAdmin() {
		new AdminTab().addInView();
	}

	public static void callAFC(Map<String, String> parameters)
	{
		String path = parameters.get(Keys.PATH);
		if (path == null || path.length() == 0) {
			TxmDialog.severe(Services.getI18n().errorMissingMandatoryParameter(Keys.PATH));
			return;
		}
		ListGridRecord theRecord = getRecordByPath(path);
		callAFC(parameters, theRecord);
	}

	public static void callAFC(Map<String, String> parameters, ListGridRecord theRecord)
	{
		if (theRecord != null) {
			TXMWEB.log(Services.getI18n().openingAFC());
			AFCForm afcd = new AFCForm(parameters);
			afcd.show();
		} else {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(parameters.toString()));
		}
	}
	
	/**
	 * Open a biblio page
	 * @param parameters
	 */
	public static void callBiblio(Map<String, String> parameters) {
		
		BiblioForm.validate(parameters);
	}

	public static void callConcordances(Map<String, String> parameters)
	{
		ListGridRecord theRecord = getRecordByPath(parameters.get(Keys.PATH));
		callConcordances(parameters, theRecord);
	}

	public static void callConcordances(Map<String, String> parameters, ListGridRecord theRecord)
	{
		if (theRecord != null) {
			TXMWEB.log(Services.getI18n().open_conc_tab());
			boolean compute = parameters.get(Keys.QUERY) != null && parameters.get(Keys.QUERY).trim().length() > 0;
			ConcordanceTab tab = new ConcordanceTab(parameters, theRecord, compute);
			//tab.setParameters(parameters);
			//tab.addInView();
		} else {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(parameters.get(Keys.PATH)));
		}
	}

	public static void callContact() {
		
		ContactForm.validate();
	}

	public static void callContexts(Map<String, String> parameters)
	{
		ListGridRecord theRecord = getRecordByPath(parameters);
		callContexts(parameters, theRecord);
	}

	public static void callContexts(Map<String, String> parameters, ListGridRecord theRecord)
	{
		if (theRecord != null) {
			TXMWEB.log(Services.getI18n().open_conc_tab());
			String query = parameters.get(Keys.QUERY);
			boolean compute = query != null && query.trim().length() > 0;
			ContextTab tab = new ContextTab(parameters, theRecord, compute);
			//tab.addInView();
		} else {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(parameters.toString()));
		}
	}

	public static void callContextsPara(Map<String, String> parameters, ListGridRecord theRecord)
	{
		if (theRecord != null) {
			TXMWEB.log(Services.getI18n().open_context_tab());
			ContextParaTab tab = new ContextParaTab(parameters, theRecord);
			tab.addInView();
		} else {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(parameters.toString()));
		}
	}
	
	public static void callCooccurrences(Map<String, String> parameters)
	{
		String path = parameters.get(Keys.PATH);
		if (path == null || path.length() == 0) {
			TxmDialog.severe(Services.getI18n().errorMissingMandatoryParameter(Keys.PATH));
			return;
		}
		ListGridRecord theRecord = getRecordByPath(path);
		callCooccurrences(parameters, theRecord);
	}
	
	public static void callCooccurrences(Map<String, String> parameters, ListGridRecord theRecord)
	{
		if (theRecord != null) {
			TXMWEB.log(Services.getI18n().openingCooccurrenceForm());
			CooccurrencesTab tab = new CooccurrencesTab(theRecord, parameters);

			tab.addInView();
		} else {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(parameters.toString()));
		}
	}
	
	/**
	 * 
	 * @param path corpus path (not page path)
	 */
	public static void callDocumentation(Map<String, String> parameters)
	{
		DocumentationForm.validate(parameters);
	}

	public static void callDownload(Map<String, String> parameters)
	{
		String path = parameters.get(Keys.PATH);
		if (path == null || path.length() == 0) {
			TxmDialog.severe(Services.getI18n().errorMissingMandatoryParameter(Keys.PATH));
			return;
		}
		Window.open(TXMWEB.BASEURL+"/Download?path="+path+"&type=txm", path,"");
	}

	public static void callEdition(Map<String, String> parameters)
	{
		ListGridRecord theRecord = getRecordByPath(parameters);
		callEdition(parameters, theRecord);
	}

	public static void callEdition(Map<String, String> parameters, ListGridRecord theRecord)
	{
		EditionForm.validate(parameters, theRecord);
	}

	public static void callHelp() {
		
		Helpform.validate();
	}

	public static void callHome() {
		
		HomeForm.validate();

	}

	public static void callIndex(Map<String, String> parameters)
	{
		String path = parameters.get(Keys.PATH);
		if (path == null || path.length() == 0) {
			TxmDialog.severe(Services.getI18n().errorMissingMandatoryParameter(Keys.PATH));
			return;
		}
		ListGridRecord theRecord = getRecordByPath(path);
		callIndex(parameters, theRecord);
	}

	public static void callIndex(Map<String, String> parameters, ListGridRecord theRecord)
	{
		if (theRecord != null) {
			TXMWEB.log(Services.getI18n().open_index_tab());
			IndexTab tab = new IndexTab(parameters, theRecord, false);
			tab.addInView();
		} else {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(parameters.get(Keys.PATH)));
		}
	}

	public static void callLexicon(Map<String, String> parameters)
	{
		String path = parameters.get(Keys.PATH);
		if (path == null || path.length() == 0) {
			TxmDialog.severe(Services.getI18n().errorMissingMandatoryParameter(Keys.PATH));
			return;
		}
		ListGridRecord theRecord = getRecordByPath(path);
		callLexicon(parameters, theRecord);
	}

	public static void callLexicon(Map<String, String> parameters, ListGridRecord theRecord)
	{
		if (theRecord != null) {
			TXMWEB.log(Services.getI18n().open_lexicon_tab());
			
			IndexTab tab = new IndexTab(parameters, theRecord, true);
			tab.addInView();
		} else {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(parameters.toString()));
		}
	}

	public static void callLogin() {
		callLogin(createParameters());
	}

	public static void callLogin(Map<String, String> parameters) {
		
		if (parameters.get(Keys.LOGIN) != null && parameters.get(Keys.LOGIN).length() > 0) {
			if (!TXMWEB.LOGGED) {
				LoginForm form = new LoginForm(parameters);
				form.setParameters(parameters);
			}
		} else {
			new LoginForm(parameters);
		}
	}

	public static void callLogout() {

		LogoutForm.validate();
	}

	/**
	 * 
	 * @param path page path (not corpus path)
	 * @param profile optional profile parameter (NOT USED YET)
	 * @param locale optional locale parameter (NOT USED YET)
	 * @return 
	 */
	public static CMSTab callPage(Map<String, String> parameters)
	{
		return PageForm.validate(parameters);
	}

	public static void callPartition(Map<String, String> parameters) {
		
		String path = parameters.get(Keys.PATH);
		if (path == null || path.length() == 0) {
			TxmDialog.severe(Services.getI18n().errorMissingMandatoryParameter(Keys.PATH));
			return;
		}
		ListGridRecord theRecord = getRecordByPath(path);
		callPartition(parameters, theRecord);
	}

	public static void callPartition(Map<String, String> parameters, ListGridRecord theRecord)
	{
		if (theRecord != null) {
			TXMWEB.log(Services.getI18n().open_partition_dialog());
			new PartitionForm(parameters, theRecord);
		} else {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(parameters.toString()));
		}
	}

	public static void callProfile(Map<String, String> parameters) {
		ProfileForm f = new ProfileForm(parameters);
		
	}

	public static void callProperties(Map<String, String> parameters)
	{
		String path = parameters.get(Keys.PATH);
		if (path == null || path.length() == 0) {
			TxmDialog.severe(Services.getI18n().errorMissingMandatoryParameter(Keys.PATH));
			return;
		}
		ListGridRecord theRecord = getRecordByPath(path);
		callProperties(parameters, theRecord);
	}

	public static void callProperties(Map<String, String> parameters, final ListGridRecord theRecord)
	{
		if (theRecord != null) {
			new PropertiesTab(theRecord, parameters).addInView();
		} else {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(parameters.toString()));
		}
	}

	public static void callReferencer(Map<String, String> parameters)
	{
		String path = parameters.get(Keys.PATH);
		if (path == null || path.length() == 0) {
			TxmDialog.severe(Services.getI18n().errorMissingMandatoryParameter(Keys.PATH));
			return;
		}

		ListGridRecord theRecord = getRecordByPath(path);
		callReferencer(parameters, theRecord);
	}

	public static void callReferencer(Map<String, String> parameters, ListGridRecord theRecord)
	{
		if (theRecord != null) {
			ProcessingWindow.show(Services.getI18n().open_references_tab());
			ReferencerTab tab = new ReferencerTab(parameters);
			tab.addInView();
		} else {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(parameters.toString()));
		}
	}

	public static void callRemove(Map<String, String> parameters) {
		callRemove(parameters, getRecordByPath(parameters));
	}

	public static void callRemove(Map<String, String> parameters, final ListGridRecord elem) {
		
		RemoveForm.validate(parameters, elem);
	}

	public static void callSelection(Map<String, String> parameters)
	{
		String path = parameters.get(Keys.PATH);
		if (path == null || path.length() == 0) {
			TxmDialog.severe(Services.getI18n().errorMissingMandatoryParameter(Keys.PATH));
			return;
		}
		ListGridRecord theRecord = getRecordByPath(path);
		callSelection(parameters, theRecord);
	}

	public static void callSelection(Map<String, String> parameters, ListGridRecord theRecord)
	{
		if (theRecord != null) {
			ProcessingWindow.show(Services.getI18n().open_textsel_tab());
			new TextSelectionTab(parameters, theRecord).addInView();
		} else {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(parameters.toString()));
		}
	}

//	public static void callIndex(String path, String props, ListGridRecord theRecord)
//	{
//		callIndex(path, null, props, theRecord);
//	}

	public static void callSignUp() {
		if(TXMWEB.INSCRIPTIONENABLE) {
			new SubscribeForm();
		} else
			TxmDialog.info(Services.getI18n().subscriptionClose(TXMWEB.CONTACT));
	}
	
	public static void callSpecificities(Map<String, String> parameters)
	{
		String path = parameters.get(Keys.PATH);
		if (path == null || path.length() == 0) {
			TxmDialog.severe(Services.getI18n().errorMissingMandatoryParameter(Keys.PATH));
			return;
		}
		ListGridRecord theRecord = getRecordByPath(parameters);
		callSpecificities(parameters, theRecord);
	}

	public static void callSpecificities(Map<String, String> parameters, ListGridRecord theRecord)
	{
		if (theRecord != null) {
			TXMWEB.log(Services.getI18n().openingSpecificities());
			SpecificitiesTab tab = new SpecificitiesTab(parameters);
			tab.addInView();
		} else {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(parameters.toString()));
		}
	}

	public static void callSubCorpus(Map<String, String> parameters)
	{
		String path = parameters.get(Keys.PATH);
		if (path == null || path.length() == 0) {
			TxmDialog.severe(Services.getI18n().errorMissingMandatoryParameter(Keys.PATH));
			return;
		}
		ListGridRecord theRecord = getRecordByPath(path);
		callSubCorpus(parameters, theRecord);
	}
	
	public static void callSubCorpus(Map<String, String> parameters, ListGridRecord theRecord)
	{
		if (theRecord != null) {
			TXMWEB.log(Services.getI18n().open_subcorpus_dialog());
			new SubcorpusForm(parameters, theRecord);
		} else {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(parameters.toString()));
		}
	}
	
	public static void callTexts(Map<String, String> parameters)
	{
		String path = parameters.get(Keys.PATH);
		if (path == null || path.length() == 0) {
			TxmDialog.severe(Services.getI18n().errorMissingMandatoryParameter(Keys.PATH));
			return;
		}
		ListGridRecord theRecord = getRecordByPath(path);
		callTexts(parameters, theRecord);
	}

	/**
	 * Open the TextTab or the  EditionTab if corpus has only one corpus
	 * @param parameters
	 * @param theRecord
	 */
	public static void callTexts(final Map<String, String> parameters, final ListGridRecord theRecord)
	{
		TextsForm.validate(parameters, theRecord);
	}

	public static void callTS(Map<String, String> parameters)
	{
		String path = parameters.get(Keys.PATH);
		if (path == null || path.length() == 0) {
			TxmDialog.severe(Services.getI18n().errorMissingMandatoryParameter(Keys.PATH));
			return;
		}
		ListGridRecord theRecord = getRecordByPath(path);
		callTS(parameters, theRecord);
	}

	public static void callTS(Map<String, String> parameters, ListGridRecord theRecord)
	{
		if (theRecord != null) {
			TXMWEB.log(Services.getI18n().open_ts_tab());
			new TsQueryTab(parameters, theRecord).addInView();
		} else {
			TxmDialog.severe(Services.getI18n().cannot_find_item_for_path(parameters.toString()));
		}
	}

	public static final HashMap<String, String> createParameters() {
		return new  HashMap<String, String>();
	}


	public static final HashMap<String, String> createParameters(String... namesAndValues) {
		
		HashMap<String, String> p = new HashMap<String, String>();
		for (int i = 0 ; i + 1 < namesAndValues.length ; i = i + 2) {
			p.put(namesAndValues[i], namesAndValues[i+1]);
		}
		
		return p;
	}

	/**
	 * 
	 * @param path
	 * @return a pre-constructed parameters hash
	 */
	public static final HashMap<String, String> createParameters(String path) {
		HashMap<String, String> hash = new HashMap<String, String>();
		hash.put(Keys.PATH, path);
		return hash;
	}

	public static void execute(String cmd, Map<String, String> parameters) {
		try {
		// String path, String query, String textid, String editions, String page, String wordids, String properties, String refs, String nLinesPerPage, String contextsSize, String login
		if (parameters.get(Keys.PROPERTIES) != null) {
			parameters.put(Keys.PROPERTIES, Keys.reformat(parameters.get(Keys.PROPERTIES)));
		}
		if (parameters.get(Keys.WORDIDS) != null) {
			parameters.put(Keys.WORDIDS, Keys.reformat(parameters.get(Keys.WORDIDS)));
		}
		if (parameters.get(Keys.REFERENCES) != null) {
			parameters.put(Keys.REFERENCES, Keys.reformat(parameters.get(Keys.REFERENCES)));
		}

		//SC.say("execute: "+cmd+" path: "+path+ " query: "+query+" textid="+textid+" editions="+editions+" page="+page+" wordids="+wordids+" properties="+properties+" refs="+refs);

		cmd = cmd.toLowerCase();
		if (Keys.EDITION.equals(cmd)) // Textométrie // Ouvre l'édition d'un texte
			Commands.callEdition(parameters);
		else if (Keys.CONCORDANCE.equals(cmd)) // Textométrie // Calcule les concordances d'un pivot
			Commands.callConcordances(parameters);
		else if (Keys.CONTEXT.equals(cmd)) // Textométrie // Calcule les contextes d'un pivot
			Commands.callContexts(parameters);
		else if (Keys.TEXTS.equals(cmd) || Keys.METADATA.equals(cmd)) // Textométrie // Affiche la liste des textes d'un corpus
			Commands.callTexts(parameters);
		else if (Keys.INDEX.equals(cmd)) // Textométrie // Calcule la liste hiérarchique des mots correspondants à une requête CQL
			Commands.callIndex(parameters);
		else if (Keys.LEXICON.equals(cmd))  // Textométrie // Calcule la liste hiérarchique de tous les mots et leur fréquence
			Commands.callLexicon(parameters);
		else if (Keys.COOCCURRENCE.equals(cmd)) // Textométrie // Affiche les cooccurrents d'un pivot
			Commands.callCooccurrences(parameters);
		else if (Keys.SELECTION.equals(cmd)) // Textométrie // Assistant de construction d'un sous-corpus de textes à partir de leurs métadonnées
			Commands.callSelection(parameters);
		else if (Keys.SUBCORPUS.equals(cmd)) // Textométrie // Construit un sous-corpus à partir des valeurs d'une propriétés de structure
			Commands.callSubCorpus(parameters);
		else if (Keys.PARTITION.equals(cmd)) // Textométrie // Construit une partition à partir des valeurs d'une propriétés de structure
			Commands.callPartition(parameters);
		else if (Keys.REFERENCE.equals(cmd)) // Textométrie // Affiche les références des mots correspondants à une requête
			Commands.callReferencer(parameters);
		else if (Keys.BIBLIO.equals(cmd) || Keys.RECORD.equals(cmd)) // Textométrie // Affiche la fiche bibliographique d'un texte
			Commands.callBiblio(parameters);
		else if (Keys.TIGERSEARCH.equals(cmd)) // Textométrie // Affiche les phrases syntaxique correspondants à une requête TIGERSearch
			Commands.callTS(parameters);
		else if (Keys.AFC.equals(cmd)) // Textométrie // Calcule l'analyse factorielle des correspondances d'une partition
			Commands.callAFC(parameters);
		else if (Keys.SPECIFICITY.equals(cmd)) // Textométrie // Calcule les mots spécifiques d'une partition
			Commands.callSpecificities(parameters);
		else if (Keys.PROPERTIES.equals(cmd) || Keys.DESCRIPTION.equals(cmd)) // Textométrie // Affiche les informations générales d'un corpus
			Commands.callProperties(parameters);
		else if (Keys.ADMIN.equals(cmd)) // Administration // Ouvre l'interface d'administration du portail
			Commands.callAdmin();
		else if (Keys.HELP.equals(cmd)) // Aide // Ouvre l'onglet d'aide du portail
			Commands.callHelp();
		else if (Keys.HOME.equals(cmd)) // Textométrie // Ouvre la page d'accueil du portail
			Commands.callHome();
		else if (Keys.CONTACT.equals(cmd)) // Aide // Ouvre l'onglet de contact du portail
			Commands.callContact();
		else if (Keys.LOGIN.equals(cmd)) // Utilisateur // Ouvre la fenêtre de connexion au portail
			Commands.callLogin(parameters);
		else if (Keys.LOGOUT.equals(cmd)) // Utilisateur // Déconnecte l'utilisateur courant du portail
			Commands.callLogout();
		else if (Keys.SUBSCRIBE.equals(cmd)) // Utilisateur // Ouvre la fenêtre d'inscription au portail
			Commands.callSignUp();
		else if (Keys.PROFILE.equals(cmd)) // Utilisateur // Ouvre l'interface de réglage du profil de l'utilisateur courant
			Commands.callProfile(parameters);
		else if (Keys.PAGE.equals(cmd)) // Textométrie // Ouvre une page .jsp du portail dans un nouvel onglet
			Commands.callPage(parameters);
		else if (Keys.DOCUMENTATION.equals(cmd)) // Textométrie // Ouvre la page de documentation d'un corpus
			Commands.callDocumentation(parameters);
		else if (Keys.DOWNLOAD.equals(cmd)) // Textométrie // Télécharge le corpus binaire d'un corpus
			Commands.callDownload(parameters);
		else if (Keys.REMOVE.equals(cmd)) // Textométrie // Supprimer l'objet sélectionné
			Commands.callRemove(parameters);
		else {
			TXMWEB.error(Services.getI18n().unknownCommandID(cmd));
		}
		} catch(Exception e) {
			TxmDialog.severe("Error", e.getMessage()+"\n"+Arrays.asList(e.getStackTrace()));
		}
	}

	public static ListGridRecord getRecordByPath(Map<String, String> parameters) {
		return getRecordByPath(parameters.get(Keys.PATH));
	}

	public static ListGridRecord getRecordByPath(String path) {

		//String[] split = path.split("/");
		TreeNode found = TXMWEB.getSideNavTree().getTree().find(Keys.PATH, path);
		if (found != null) {
			return found;
		}

		return null;
	}

	public void call(String jsonparameters) {

		try {
			//@SuppressWarnings("deprecation")!!!! kc :'('
			JSONValue json = JSONParser.parseStrict(jsonparameters);
			if (json == null) {
				TxmDialog.severe("Error: json string was: "+jsonparameters);
				return;
			}
			JSONObject o = json.isObject();
			if (o == null) {
				TxmDialog.severe("Error: json was: "+json);
				return;
			}
			String cmd = null;
			if (o.get(Keys.COMMAND) != null && o.get(Keys.COMMAND).isString() != null) {
				cmd = o.get(Keys.COMMAND).isString().stringValue(); // o.get(Keys.COMMAND).isString().stringValue();
			} else {
				TxmDialog.severe("Error: 'command' parameter not set: "+json);
				return;
			}
			
			HashMap<String, String> parameters = new HashMap<String, String>();
			for (String p : o.keySet()) {
				if (o.get(p).isString() != null) {
					parameters.put(p, o.get(p).isString().stringValue());
				}
			}

			Commands.execute(cmd, parameters);
		} catch (Exception e) {
			TxmDialog.severe("Error: "+e);
		}
	}
	
	/**
	 * expose Commands through the call method -> in JS use : $wnd.call(cmd, path, textid, query, editions, page, wordids, props, login);
	 */
	public native void exportCommands() /*-{

    	var that = this;
    		$doc.callTXMCommand = $wnd.callTXMCommand = $entry(function(json) {
      		that.@org.txm.web.client.Commands::call(Ljava/lang/String;)(json);
    	});

	}-*/;
}
