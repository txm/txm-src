package org.txm.web.shared;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

public class PropertiesResult implements IsSerializable{
	
	public String htmlContent;
	public Integer nWords;
	public Integer nTexts;
	public Integer nParts;
	public ArrayList<String> properties;
	public ArrayList<String> structures;
	public ArrayList<Integer> partSizes;
	public ArrayList<String> parts;
	
	public String toString() {
		if (htmlContent != null && htmlContent.length() > 0) return htmlContent;
		
		StringBuffer buffer = new StringBuffer();
		if (nWords != null) buffer.append("\nwords="+nWords);
		if (nTexts != null) buffer.append("\nnTexts="+nTexts);
		if (nParts != null) buffer.append("\nnParts="+nParts);
		if (properties != null) buffer.append("\nproperties="+properties);
		if (structures != null) buffer.append("\nstructures="+structures);
		if (partSizes != null) buffer.append("\npartSizes="+partSizes);
		if (parts != null) buffer.append("\nparts="+parts);
		
		return buffer.toString();
	}
}
