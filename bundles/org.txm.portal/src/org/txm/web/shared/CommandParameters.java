package org.txm.web.shared;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public abstract class CommandParameters {
	
	public abstract void setParameters(Map<String, String> parameters);
	
	public static Integer toInt(String v) {
		
		try {
			return Integer.parseInt(v);
		} catch(Exception e) {
			return 0;
		}
	}
	
	public static Double toDouble(String v) {
		
		try {
			return Double.parseDouble(v);
		} catch(Exception e) {
			return 0.0;
		}
	}
	
	public static Boolean toBoolean(String v) {
		
		return "true".equals(v);
	}
	
	public static String fromBoolean(Boolean b) {
		
		return Boolean.toString(b);
	}
	
	public static ArrayList<Boolean> toBooleanArrayList(String v) {
		
		ArrayList<Boolean> list = new ArrayList<Boolean>();
		try {
			for (String s : v.split(",")) {
				list.add(toBoolean(s));
			}
		} catch(Exception e) { }
		
		return list;
	}
	
	public static String fromBooleanArrayList(ArrayList<Boolean> list) {
		
		return fromArrayList(list);
	}
	
	public static ArrayList<String> toStringArrayList(String v) {
		
		try {
			return new ArrayList<String>(Arrays.asList(v.split(",")));
		} catch(Exception e) {
			return new ArrayList<String>();
		}
	}
	
	public static String[] toStrings(String v) {
			if (v == null) return new String[0];
			return v.split(",");
	}
	
	public static String fromInt(Integer i) {
		
		if (i == null) return "0";
		try {
			return Integer.toString(i);
		} catch(Exception e) {
			return "0";
		}
	}
	
	public static String fromStringArrayList(ArrayList<String> list) {
		
		return fromArrayList(list);
	}
	
	public static String fromArrayList(ArrayList<? extends Object> list) {
		
		StringBuffer buffer = new StringBuffer();
		try {
			boolean next = false;
			for (Object v : list) {
				
				if (next) buffer.append(",");
				buffer.append(v);
				
				next = true;
			}
		} catch(Exception e) { }
		
		return buffer.toString();
	}
}
