package org.txm.web.shared;

import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ServerInfos implements IsSerializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6991819765246896712L;
	public String portaltitle = "";
	public String portalname = "";
	public String portallongname = "";
	public String baseURL = "";
	public boolean mailEnable = true;
	public boolean inscriptionEnable = true;
	public boolean maintenance = false;
	public boolean ready = false;
	public String contact = "nocontact";
	public boolean showPublicPages = true;
	public boolean showPrivatePages = false;
	public boolean rEnable = false;
	public String login = "guest";
	public String news= "";
	public Date lastNews = null;
	public String error = "";
	public String defaultEditionPanelSize;
	public boolean expo = false;
}
