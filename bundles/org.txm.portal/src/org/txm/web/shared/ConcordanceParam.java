package org.txm.web.shared;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Concordance parameters container
 * @author vchabanis
 *
 */
public class ConcordanceParam extends CommandParameters implements IsSerializable {

	private int ID = new java.util.Random().nextInt();
	private String corpusPath = "";
	private String query = ""; 
	private ArrayList<String> leftSortProperties = new ArrayList<String>();
	private ArrayList<String> keywordSortProperties = new ArrayList<String>();
	private ArrayList<String> rightSortProperties = new ArrayList<String>();
	private ArrayList<String> leftViewProperties = new ArrayList<String>();
	private ArrayList<String> keywordViewProperties = new ArrayList<String>();
	private ArrayList<String> rightViewProperties = new ArrayList<String>();
	private ArrayList<String> viewReferences = new ArrayList<String>();
	private Integer leftContextSize = 0;
	private Integer rightContextSize = 0;
	private Integer startIndex = 0;
	private Integer endIndex = 0;
	private Integer resultPerPage = 1;
	private ArrayList<String> sortKeys = new ArrayList<String>(Arrays.asList(Keys.NONE, Keys.NONE, Keys.NONE, Keys.NONE));
	private ArrayList<Boolean> sortdirs;
	private ArrayList<String> sortReferences;

	public String getCorpusPath() {
		return corpusPath;
	}

	public void setCorpusPath(String corpusPath) {
		this.corpusPath = corpusPath;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public ArrayList<String> getViewReferences() {
		return viewReferences;
	}

	public void setViewReferences(ArrayList<String> references) {
		this.viewReferences = references;
	}
	
	public ArrayList<String> getSortReferences() {
		return sortReferences;
	}

	public void setSortReferences(ArrayList<String> references) {
		this.sortReferences = references;
	}

	public int getLeftContextSize() {
		return leftContextSize;
	}

	public void setLeftContextSize(int leftContextSize) {
		this.leftContextSize = leftContextSize;
	}

	public int getRightContextSize() {
		return rightContextSize;
	}

	public void setRightContextSize(int rightContextSize) {
		this.rightContextSize = rightContextSize;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public int getEndIndex() {
		return endIndex;
	}

	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}

	public ConcordanceParam() {
	}

	public ConcordanceParam(String corpus, 
			String query, 
			ArrayList<String> leftSortProps,
			ArrayList<String> keywordSortProps,
			ArrayList<String> rightSortProps,
			ArrayList<String> leftViewProps,
			ArrayList<String> keywordViewProps,
			ArrayList<String> rightViewProps,
			ArrayList<String> refs,
			int Lsize,
			int Rsize,
			int startIndex,
			int endIndex) {
		this.corpusPath = corpus;
		this.query = query;
		this.leftSortProperties = leftSortProps;
		this.keywordSortProperties = keywordSortProps;
		this.rightSortProperties = rightSortProps;
		this.leftViewProperties = leftViewProps;
		this.keywordViewProperties = keywordViewProps;
		this.rightViewProperties = rightViewProps;
		this.viewReferences = refs;
		this.leftContextSize = Lsize;
		this.rightContextSize = Rsize;
		this.startIndex = startIndex;
		this.endIndex = endIndex;
	}

	public String toString() {
		String result = "Param : \n";
		result += " corpusPath = " + corpusPath + "\n";
		result += " query = " + query + "\n";
		result += " leftSortProperties = " + leftSortProperties + "\n";
		result += " keywordSortProperties = " + keywordSortProperties + "\n";
		result += " rightSortProperties = " + rightSortProperties + "\n";
		result += " leftviewProperties = " + leftViewProperties + "\n";
		result += " keywordViewProperties = " + keywordViewProperties + "\n";
		result += " rightViewProperties = " + rightViewProperties + "\n";
		result += " references = " + viewReferences + "\n";
		result += " startIndex = " + startIndex + "\n";
		result += " endIndex = " + endIndex + "\n";
		result += " leftContextSize = " + leftContextSize + "\n";
		result += " rightContextSize = " + rightContextSize + "\n";
		return result;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public int getID() {
		return ID;
	}

	public void setResultPerPage(int resultPerPage) {
		this.resultPerPage = resultPerPage;
	}

	public int getResultPerPage() {
		return resultPerPage;
	}

	public void setSortKeys(ArrayList<String> sortKeys) {
		this.sortKeys = sortKeys;
	}

	public ArrayList<String> getSortKeys() {
		return sortKeys;
	}

	// sort properties getters
	public ArrayList<String> getLeftSortProperties() {
		return leftSortProperties;
	}
	public ArrayList<String> getRightSortProperties() {
		return rightSortProperties;
	}
	public ArrayList<String> getKeywordSortProperties() {
		return keywordSortProperties;
	}
	
	// view properties getters
	public ArrayList<String> getLeftViewProperties() {
		return leftViewProperties;
	}
	public ArrayList<String> getKeywordViewProperties() {
		return keywordViewProperties;
	}
	public ArrayList<String> getRightViewProperties() {
		return rightViewProperties;
	}
	
	// sort properties setters
	public void setLeftSortProperties(ArrayList<String> leftSortProperties) {
		this.leftSortProperties = leftSortProperties;
	}
	public void setKeywordSortProperties(ArrayList<String> keywordSortProperties) {
		this.keywordSortProperties = keywordSortProperties;
	}
	public void setRightSortProperties(ArrayList<String> rightSortProperties) {
		this.rightSortProperties = rightSortProperties;
	}
	
	// view properties setters
	public void setLeftViewProperties(ArrayList<String> leftViewProperties) {
		this.leftViewProperties = leftViewProperties;
	}
	public void setKeywordViewProperties(ArrayList<String> keywordViewProperties) {
		this.keywordViewProperties = keywordViewProperties;
	}
	public void setRightViewProperties(ArrayList<String> rightViewProperties) {
		this.rightViewProperties = rightViewProperties;
	}

	public void setSortDirs(ArrayList<Boolean> dirs) {
		this.sortdirs = dirs;
	}
	
	public ArrayList<Boolean> getSortDirs() {
		return this.sortdirs;
	}

	/*
	private int ID = new java.util.Random().nextInt();
	private String corpusPath = "";
	private String query = ""; 
	private ArrayList<String> leftSortProperties = new ArrayList<String>();
	private ArrayList<String> keywordSortProperties = new ArrayList<String>();
	private ArrayList<String> rightSortProperties = new ArrayList<String>();
	private ArrayList<String> leftViewProperties = new ArrayList<String>();
	private ArrayList<String> keywordViewProperties = new ArrayList<String>();
	private ArrayList<String> rightViewProperties = new ArrayList<String>();
	private ArrayList<String> viewReferences = new ArrayList<String>();
	private ArrayList<String> sortKeys = new ArrayList<String>(Arrays.asList(Keys.NONE, Keys.NONE, Keys.NONE, Keys.NONE));
	private ArrayList<String> sortProperties = new ArrayList<String>(Arrays.asList(Keys.WORD, Keys.WORD, Keys.WORD));
	private ArrayList<Boolean> sortdirs;
	private ArrayList<String> sortReferences;
	private Integer leftContextSize = 0;
	private Integer rightContextSize = 0;
	private Integer startIndex = 0;
	private Integer endIndex = 0;
	private Integer resultPerPage = 1;
	 */
	
	@Override
	public void setParameters(Map<String, String> parameters) {
		
		for (String p : parameters.keySet()) {
			if (Keys.QUERY.equals(p)) {
				query = parameters.get(p);
			} else if (Keys.PATH.equals(p)) {
				corpusPath = parameters.get(p);
			} else if (Keys.QUERY.equals(p)) {
				query = parameters.get(p);
			} else if (Keys.LEFT_CONTEXT.equals(p)) {
				leftContextSize = toInt(parameters.get(p));
			} else if (Keys.RIGHT_CONTEXT.equals(p)) {
				rightContextSize = toInt(parameters.get(p));
			} else if (Keys.INDEX.equals(p)) {
				startIndex = toInt(parameters.get(p));
			} else if (Keys.END_INDEX.equals(p)) {
				endIndex = toInt(parameters.get(p));
			} else if (Keys.SORT.equals(p)) {
				sortKeys = toStringArrayList(parameters.get(p));
			} else if (Keys.SORT_DIRECTION.equals(p)) {
				sortdirs = toBooleanArrayList(parameters.get(p));
			}else if (Keys.LEFT_SORT_PROPERTIES.equals(p)) {
				leftSortProperties = toStringArrayList(parameters.get(p));
			} else if (Keys.KEYWORD_SORT_PROPERTIES.equals(p)) {
				keywordSortProperties = toStringArrayList(parameters.get(p));
			} else if (Keys.RIGHT_SORT_PROPERTIES.equals(p)) {
				rightSortProperties = toStringArrayList(parameters.get(p));
			} else if (Keys.LEFT_VIEW_PROPERTIES.equals(p)) {
				leftViewProperties = toStringArrayList(parameters.get(p));
			} else if (Keys.KEYWORD_VIEW_PROPERTIES.equals(p)) {
				keywordViewProperties = toStringArrayList(parameters.get(p));
			} else if (Keys.RIGHT_VIEW_PROPERTIES.equals(p)) {
				rightViewProperties = toStringArrayList(parameters.get(p));
			} else if (Keys.REFERENCES.equals(p)) {
				viewReferences = toStringArrayList(parameters.get(p));
			} else if (Keys.REFERENCE_SORT_PROPERTIES.equals(p)) {
				sortReferences = toStringArrayList(parameters.get(p));
			}
		}
	}
}
