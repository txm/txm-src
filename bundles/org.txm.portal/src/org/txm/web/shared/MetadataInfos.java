package org.txm.web.shared;

import java.util.HashMap;

import com.google.gwt.user.client.rpc.IsSerializable;
public class MetadataInfos implements IsSerializable{

	private static final long serialVersionUID = -5126107621501360557L;
	public String name, shortname, longname, type;
	public boolean selection, partition, display, open;
	public int colwidth;
	public HashMap<String, String> extra;
	
	public String toString() {
		return name+"/"+shortname+"/"+longname+" "+type+" "+selection+" "+partition+" "+display+" "+open+" "+colwidth+" "+extra;
	}
}
