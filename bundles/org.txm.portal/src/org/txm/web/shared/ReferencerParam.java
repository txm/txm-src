package org.txm.web.shared;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import com.google.gwt.user.client.rpc.IsSerializable;
/**
 * 
 * @author vchabanis
 *
 */
public class ReferencerParam extends CommandParameters implements IsSerializable{

	private static final long serialVersionUID = 1796762903841101410L;
	
	private String corpusPath = "";
	private String query = ""; 
	private ArrayList<String> properties = new ArrayList<>(Arrays.asList("text:id"));
	public String prop = "word";
	public int filterNbPerPage = Integer.MAX_VALUE;
	public int startindex = 0;
	private String sort = "Unit";



	public ReferencerParam() {
	}

	public ReferencerParam(String corpus, 
			String query, 
			ArrayList<String> properties) {
		this.corpusPath = corpus;
		this.query = query;
		this.properties = properties;
	}
	
	public ArrayList<String> getProperties() {
		return properties;
	}

	public void setProperties(ArrayList<String> properties) {
		this.properties = properties;
	}

	public String getCorpusPath() {
		return corpusPath;
	}

	public void setCorpusPath(String corpusPath) {
		this.corpusPath = corpusPath;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String toString() {
		String result = "Param : \n";
		result += " corpusPath = " + corpusPath + "\n";
		result += " query = " + query + "\n";
		result += " property = "+ prop + "\n";
		result += " pattern = " + properties + "\n";
		return result;
	}

	public String toKey() {
		return corpusPath+query+prop+properties;//+filterFmax+""+filterFmin+""+filterVmax;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getSort() {
		return sort;
	}

	@Override
	public void setParameters(Map<String, String> parameters) {

		for (String p : parameters.keySet()) {
			if (Keys.QUERY.equals(p)) {
				query = parameters.get(p);
			} else if (Keys.PATH.equals(p)) {
				corpusPath = parameters.get(p);
			} else if (Keys.SORT.equals(p)) {
				sort = parameters.get(p);
			} else if (Keys.PROPERTIES.equals(p)) {
				properties = toStringArrayList(parameters.get(p));
			} else if (Keys.PROPERTY.equals(p)) {
				prop = parameters.get(p);
			} else if (Keys.NLINESPERPAGE.equals(p)) {
				filterNbPerPage = toInt(parameters.get(p));
			} else if (Keys.INDEX.equals(p)) {
				startindex = toInt(parameters.get(p));
			}
		}	
	}
}
