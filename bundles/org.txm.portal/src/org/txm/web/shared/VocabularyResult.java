package org.txm.web.shared;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * 
 * @author vchabanis
 *
 */
public class VocabularyResult extends HashMap<String, int[]> implements IsSerializable {

	private static final long serialVersionUID = 4493237026329018568L;
	
	VocabularyParam params; // contains the parameter, updated or not 
	
	ArrayList<String> keys = new ArrayList<String>();
	ArrayList<String> columns = new ArrayList<String>();

	private int minFrequency = 0;
	private int maxFrequency = 0;
	private int sumFrequency = 0;
	private int totalOccurrences = 0;
	public String error = "";
		
	public VocabularyParam getParams() {
		return params;
	}
	
	public ArrayList<String> getKeys()
	{
		return keys;
	}
	
	public int[] put(String key, int[] values)
	{
		if (!keys.contains(key))
			keys.add(key);
		//System.out.println("key: "+key+" values:"+values);
		return super.put(key, values);
	}
	
	public ArrayList<String> getColumns() {
		return columns;
	}
	public void setColumns(ArrayList<String> columns) {
		this.columns = columns;
	}
	
	public int getMinFrequency() {
		return minFrequency;
	}
	public void setMinFrequency(int minFrequency) {
		this.minFrequency = minFrequency;
	}
	public int getMaxFrequency() {
		return maxFrequency;
	}
	public void setMaxFrequency(int maxFrequency) {
		this.maxFrequency = maxFrequency;
	}
	public int getSumFrequency() {
		return sumFrequency;
	}
	public void setSumFrequency(int sumFrequency) {
		this.sumFrequency = sumFrequency;
	}
	public int getTotalOccurrences() {
		return totalOccurrences;
	}
	public void setTotalOccurrences(int totalOccurrences) {
		this.totalOccurrences = totalOccurrences;
	}
	
	public String toString() {
		
		StringBuffer buffer = new StringBuffer();
		buffer.append("Unit");
		for (String col : columns) buffer.append("\t"+col);
		buffer.append("\n");
		for (int i = 0 ; i < keys.size() ; i++) {
			if (i > 0) buffer.append("\n");
			String line = "";
			for (int j : this.get(keys.get(i))) {
				line += "\t"+j;
			}
			buffer.append(keys.get(i)+line);
		}
		
		return buffer.toString();
	}

	public void setParams(VocabularyParam params2) {
		this.params = params2;
	}
}
