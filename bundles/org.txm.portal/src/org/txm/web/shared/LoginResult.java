package org.txm.web.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class LoginResult implements IsSerializable {
	
	private static final long serialVersionUID = -1512544223571971183L;
	
	private boolean success = true;
	private String homePage = "";
	
	public LoginResult() {
	}
	
	public LoginResult(boolean success, String homePage) {
		this.success = success;
		this.homePage = homePage;
	}
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getHomePage() {
		return homePage;
	}
	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}
}
