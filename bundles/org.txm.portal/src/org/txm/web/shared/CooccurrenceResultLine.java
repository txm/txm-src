package org.txm.web.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class CooccurrenceResultLine implements IsSerializable {

	private String cooccurrent;
	private int frequency;
	private int coFrequency;
	private double score;
	private double averageDistance;
	
	public String getCooccurrent() {
		return cooccurrent;
	}
	public void setCooccurrent(String cooccurrent) {
		this.cooccurrent = cooccurrent;
	}
	public int getFrequency() {
		return frequency;
	}
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	public int getCoFrequency() {
		return coFrequency;
	}
	public void setCoFrequency(int coFrequency) {
		this.coFrequency = coFrequency;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public double getAverageDistance() {
		return averageDistance;
	}
	public void setAverageDistance(double averageDistance) {
		this.averageDistance = averageDistance;
	}
	
	public String toString() {
		return cooccurrent+"\t"+frequency+"\t"+coFrequency+"\t"+score+"\t"+averageDistance;
	}
}
