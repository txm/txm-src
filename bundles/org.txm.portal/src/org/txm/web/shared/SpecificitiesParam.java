package org.txm.web.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class SpecificitiesParam implements IsSerializable {

	public static String ALL = "Tout"/*Services.getI18n().all()*/;
	public static String SMIN = "Smin"/*Services.getI18n().threshold()*/;
	public static String TOPN = "TopN"/*Services.getI18n().nbWords()*/;
	public static final String[] thresholdValues = {ALL, SMIN, TOPN};
	
	public int ID = new java.util.Random().nextInt();
	
	public String property;
	public Integer nbWords;
	public String path;
	public Double threshold;
	public String mode;
	private Integer fmin;
	private Integer vmax;
	
	public Integer getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public Integer getNbWords() {
		return nbWords;
	}
	public void setNbWords(int nbWords) {
		this.nbWords = nbWords;
	}
	public void setPath(String itemPath) {
		this.path = itemPath;
	}
	public String getPath(){
		return this.path;
	}
	
	public void setThreshold(double t){
		this.threshold = t;
	}
	
	public Double getThreshold(){
		return this.threshold;	
	}
	
	public void setMode(String m){
		this.mode = m;
	}
	
	public String getMode(){
		return this.mode;
	}
	
	public String toString(){
		return "path= "+path+" id="+this.ID+" prop: "+property+" fmin: "+fmin+" nbresult: "+nbWords;
	}
	
	public void setFmin(Integer fmin) {
		this.fmin = fmin;
	}
	
	public Integer getFmin() {
		return fmin;
	}
	
	public void setVmax(Integer vmax) {
		this.vmax = vmax;
	}
	
	public Integer getVmax() {
		return vmax;
	}
}
