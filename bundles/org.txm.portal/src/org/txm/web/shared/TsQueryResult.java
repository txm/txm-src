package org.txm.web.shared;

import com.google.gwt.user.client.rpc.IsSerializable;
/**
 * tsquery results.
 * 
 * @author mdecorde
 *
 */
public class TsQueryResult implements IsSerializable {
	
	private static final long serialVersionUID = -5215893496882483492L;

	public String corpusid = "";
	public Integer nomatch = 0;
	public Integer nosubgraph = 0;
	public Integer nosent = 0;
	public Integer numberOfMatch = 0;
	public Integer numberOfSubGraph = 0;
	public String html = "the html";
	public String svg = "thx svg";
}
