package org.txm.web.shared;

import java.util.Arrays;
import java.util.LinkedHashSet;


public class Keys {
	

	
	public static final String ADMIN = "admin";
	public static final String AFC = "afc";
	public static final String CONTACT = "contact";
	public static final String COOCCURRENCE = "cooccurrence";
	public static final String DOWNLOAD = "download";
	public static final String REMOVE = "remove";
	public static final String DOCUMENTATION = "documentation";
	public static final String SUBSCRIBE = "subscribe";
	public static final String TIGERSEARCH = "tigersearch";
	public static final String PAGE = "page";
	public static final String LOGOUT = "logout";
	public static final String HELP = "help";
	public static final String LEXICON = "lexicon";
	public static final String SPECIFICITY = "specificity";
	public static final String DESCRIPTION = "description";
	public static final String EDITION = "edition";
	public static final String CONCORDANCE = "concordance";
	public static final String INDEX = "index";
	public static final String END_INDEX = "end_index";
	public static final String SUBCORPUS = "subcorpus";
	public static final String PARTITION = "partition";
	public static final String SELECTION = "selection";
	public static final String RECORD = "record";
	public static final String BIBLIO = "biblio";
	public static final String TEXTS = "texts";
	public static final String METADATA = "metadata";
	public static final String CONTEXT = "context";
	public static final String REFERENCE = "reference";
	public static final String HOME = "home";
	
	public static final String PROFILE = "profile";
	public static final String LOCALE = "locale";
	public static final String PATH = "path";
	public static final String QUERY = "query";
	public static final String TEXTID = "textid";
	public static final String EDITIONS = "editions";
	public static final String PAGEID = "pageid";
	public static final String WORDIDS = "wordids";
	public static final String LOGIN = "login";
	public static final String COMMAND = "command";
	public static final String REFERENCES = "references";
	public static final String NLINESPERPAGE = "n-lines-per-page";
	
	public static final String CONTEXTS = "contexts";
	public static final String RIGHT_CONTEXT = "right-context";
	public static final String LEFT_CONTEXT = "left-context";
	public static final String MIN_RIGHT_CONTEXT = "min-right-context";
	public static final String MIN_LEFT_CONTEXT = "min-left-context";
	public static final String INCLUDE_PIVOT = "include-pivot";
	
	public static final String WORD = "word";
	public static final String ID = "id";
	public static final String STRUCTURE = "structure";
	public static final String TEXT = "text";
	public static final String PROPERTY = "property";
	public static final String PROPERTIES = "properties";
	public static final String LEFT_VIEW_PROPERTIES = "left-view-properties";
	public static final String KEYWORD_VIEW_PROPERTIES = "keyword-view-properties";
	public static final String RIGHT_VIEW_PROPERTIES = "right-view-properties";
	public static final String FIRSTDIM = "first-dimension";
	public static final String SECONDDIM = "second-dimension";
	public static final String SHOWCOLS = "show-cols";
	public static final String SHOWLINES = "show-lines";
	public static final String NAVIGATION = "navigation";
	
	public static final String LEFT_SORT_PROPERTIES = "left-sort-properties";
	public static final String KEYWORD_SORT_PROPERTIES = "keyword-sort-properties";
	public static final String RIGHT_SORT_PROPERTIES = "right-sort-properties";
	public static final String LABEL = "label";
	
	public static final String ALPHANUMERIC = "alpha";
	public static final String FREQUENCE = "frequence";
	public static final String FMIN = "fmin";
	public static final String CMIN = "cmin";
	public static final String SMIN = "smin";
	public static final String FMAX = "fmax";
	public static final String VMAX = "vmax";
	public static final String VMIN = "vmin";
	
	public static final String VALUE = "value";
	public static final String NAME = "name";
	public static final String SCORE = "score";
	public static final String SORT = "sort";
	public static final String SORT_DIRECTION = "sort";
	public static final String REFERENCE_SORT_PROPERTIES = "reference-sort-properties";
	public static final String TOPN = "topn";
	public static final String TITLE = "title";
	public static final String TOOLTIP = "tooltip";
	
	public static final String KEY1 = "sort-key1";
	public static final String KEY2 = "sort-key2";
	public static final String KEY3 = "sort-key3";
	public static final String KEY4 = "sort-key4";
	
	public static final String NONE = "none";
	public static final String KEYWORD = "keyword";
	public static final String MODE = "mode";
	public static final String MODE_PARAMETER = "mode-parameter";
	public static final String THRESHOLD = "threshold";
	public static final String THRESHOLD_PARAMETER = "threshold_parameter";
	
	public static final String USER = "user";
	public static final String PASSWORD = "password";
	
	public static final LinkedHashSet<String> COMMANDS = new LinkedHashSet<String>(Arrays.asList(EDITION,CONCORDANCE,CONTEXT,TEXTS,INDEX,LEXICON,COOCCURRENCE,SELECTION,SUBCORPUS,PARTITION,REFERENCE,BIBLIO,TIGERSEARCH,AFC,SPECIFICITY,PROPERTIES,ADMIN,HELP,HOME,CONTACT,LOGIN,LOGOUT,SUBSCRIBE,PROFILE,PAGE,DOCUMENTATION,DOWNLOAD,REMOVE));
	

	
	public static String reformat(String commaSeparatedValues) {
		
		if (commaSeparatedValues != null) { // reformat properties value
			String split[] = commaSeparatedValues.split(",");	
			commaSeparatedValues = "";
			for (int i = 0 ; i < split.length ; i++) {
				if (i > 0) commaSeparatedValues +=  ", ";
				split[i] = split[i].trim();
				if (split[i].length() > 0) {
					commaSeparatedValues +=  split[i].trim();
				}
			}
		}
		return commaSeparatedValues;
	}
}
