package org.txm.web.shared;

import java.util.Date;

import org.txm.web.client.Commands;
import org.txm.web.client.TXMWEB;
import org.txm.web.client.services.Services;
import org.txm.web.client.widgets.dialogs.TxmDialog;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class ServerInfosUpdater {
	static public int DELTA_INFOS_REFRESH = 5*60000;
	static public int DELTA_EXPO_REFRESH = 3*60000;
	static Timer t;
	static Date lastNews = new Date();
	static Date lastActivity = new Date();
	static String lastlogin = "guest";

	public static int getLoopInSec() {
		return DELTA_INFOS_REFRESH/1000;
	}
	
	public static void updateLastActivityTime() {
		lastActivity = new Date();
	}
	
	public static void start() {
		if (TXMWEB.EXPO) { // no need to fetch server infos
			
		}
		t = new Timer() {
			public void run() {
				//System.out.println("FETCH NEW INFOS...");
				if (TXMWEB.EXPO) { // check last activity time and show Home tab if there is no activity
					//System.out.println("EXPO MODE...");
					Date now = new Date();
					if (now.getTime() - lastActivity.getTime() > DELTA_EXPO_REFRESH) { // must open Home Tab
						//System.out.println("NO ACTIVITY DETECTED: opening home tab...");
						Commands.callHome();
						ServerInfosUpdater.updateLastActivityTime();
					}
					return; // no need to fetch server infos
				}
				
				Services.getHelloService().getServerInfos(new AsyncCallback<ServerInfos>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
					}

					@Override
					public void onSuccess(ServerInfos result) {
						
						if(result.lastNews != null && result.news != null && result.news.length() > 0) {
							if(lastNews.before(result.lastNews)) {
								if(result.news.startsWith("!!!"))
									TxmDialog.warning(result.news.substring(3));
								else if(result.news.startsWith("!!"))
									TXMWEB.getMessagesBar().setMessage(result.news.substring(2));
								else
									TXMWEB.getConsole().addMessage(result.news);
							}
						}
						if(result.lastNews != null)
							lastNews = result.lastNews;
						//System.out.println("last login="+lastlogin);
						//System.out.println("result login="+result.login);
						// lastlogin != "guest && result.login == "guest"
						if(!lastlogin.equals("guest") && result.login.equals("guest")) {
							TXMWEB.logout();
						}
						lastlogin = result.login;
					}
				});
			}
		};
		t.scheduleRepeating(DELTA_INFOS_REFRESH);
	}

	public static void alive() {
		if(t == null)
			start();
		ServerInfosUpdater.updateLastActivityTime();
	}
}
