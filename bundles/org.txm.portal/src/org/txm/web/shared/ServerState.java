package org.txm.web.shared;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ServerState implements IsSerializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5413966140434456217L;
	public boolean toolbox;
	public boolean maintenance;
	public boolean mail;
	public boolean inscription;
	public ArrayList<String> currentUsers;
	public String contact;
	
	public String toString()
	{
		return "toolbox: "+toolbox+"\n"+
				"maintenance: "+maintenance+"\n"+
				"contact: "+contact+"\n"+
				"mail: "+mail+"\n"+
				"inscription: "+inscription+"\n"+
				"users: "+currentUsers;
				
	}
}
