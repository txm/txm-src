package org.txm.web.shared;

import com.google.gwt.user.client.rpc.IsSerializable;
/**
 * MenuBarContent result
 * @author vchabanis
 *
 */
public class MenuBarContent implements IsSerializable {
	
	private static final long serialVersionUID = -539888759122910099L;
	
	private boolean isLogged = false;
	private boolean isAdmin = false;
	private boolean isMemberOf = false;
	private String userName = "";

	public MenuBarContent() {
		this("", false, false, false);
	}
	
	public MenuBarContent(String userName) {
		this(userName, false, false, false);
	}

	public MenuBarContent(String userName, boolean islogged, boolean isAdmin, boolean ismemberof) {
		this.userName = userName;
		this.isLogged = islogged;
		this.isAdmin = isAdmin;
		this.isMemberOf = ismemberof;
	}
	
	public boolean isLogged() {
		return isLogged;
	}	
	
	public boolean isAdmin() {
		return isAdmin;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isMemberOf() {
		return isMemberOf;
	}
}
