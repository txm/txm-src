package org.txm.web.shared;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

public class CooccurrenceResult extends ArrayList<CooccurrenceResultLine> implements IsSerializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7558200003362728918L;

	public String toString() {
		
		StringBuffer buffer = new StringBuffer();
		buffer.append("cooccurrent\tfrequency\tcoFrequency\tscore\taverageDistance");
		for (CooccurrenceResultLine line : this) {
			buffer.append("\n"+line.toString());
		}
		return buffer.toString();
	}
}
