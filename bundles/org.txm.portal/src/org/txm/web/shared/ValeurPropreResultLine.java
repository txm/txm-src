package org.txm.web.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ValeurPropreResultLine implements IsSerializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4404201310310416112L;
	private int factor;
	private double value;
	private double percent;
	
//	public ValeurPropreResultLine(int f, double val, double percent){
//		this.factor = f;
//		this.value = val;
//		this.percent = percent;
//		
//	}
	
	public int getFactor() {
		return factor;
	}
	public void setFactor(int factor) {
		this.factor = factor;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public double getPercent() {
		return percent;
	}
	public void setPercent(double percent) {
		this.percent = percent;
	}
	
	

}
