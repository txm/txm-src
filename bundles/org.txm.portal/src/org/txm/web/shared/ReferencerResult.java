package org.txm.web.shared;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import com.google.gwt.user.client.rpc.IsSerializable;
/**
 * 
 * @author mdecorde
 *
 */
public class ReferencerResult implements IsSerializable {

	private static final long serialVersionUID = 7401342570791538336L;
	
	public int total = 0;
	public String error = "";
	public LinkedHashMap<String, ArrayList<String>> refs = new LinkedHashMap<String, ArrayList<String>>();
	public LinkedHashMap<String, HashMap<String, Integer>> counts = new LinkedHashMap<String, HashMap<String, Integer>>();
	
	public String toString() {
		
		StringBuffer buffer = new StringBuffer();
		buffer.append("Reference\tCounts");
		for (String k: refs.keySet()) {
			buffer.append("\n"+k+"\t"+counts.get(k));
		}
		
		return buffer.toString();
	}
}
