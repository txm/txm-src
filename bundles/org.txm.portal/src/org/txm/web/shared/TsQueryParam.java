package org.txm.web.shared;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * TsQuery parameters container
 * @author mdecorde
 *
 */
public class TsQueryParam implements IsSerializable {

	private static final long serialVersionUID = 592895085449516969L;
	public int ID = new java.util.Random().nextInt();
	public String corpuspath = "";
	public String query = "";
	public ArrayList<String> tprops;
	public String ntprop;
}


