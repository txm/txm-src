package org.txm.web.shared;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.user.client.rpc.IsSerializable;

public class SelectionInfos implements IsSerializable{

	private static final long serialVersionUID = -7692141595699025959L;
	public ArrayList<String> texts;
	public HashMap<String, ArrayList<String>> critera;
	
	public SelectionInfos()
	{
		
	}
}
