package org.txm.web.shared;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;
/**
 * concordance result line.
 * @author vchabanis
 *
 */
public class ConcordanceResultLine implements IsSerializable {
	
	private String textId = "";
	private ArrayList<String> wordIds = new ArrayList<String>();
	private String reference = "";
	private String rightContext = "";
	private String leftContext = "";
	private String keyword = "";
	private boolean isTruncated = false;
	
	public String toString() {
		return ""+reference+"\t"+leftContext+"\t"+keyword+"\t"+rightContext+"\t"+isTruncated+"\t"+textId+"\t"+wordIds;
	}
	
	public String toStringContext() {
		return ""+reference+"\t"+leftContext+" ["+keyword+"] "+rightContext+"\t"+isTruncated+"\t"+textId+"\t"+wordIds;
	}
	
	public String getTextId() {
		return textId;
	}
	public void setTextId(String textId) {
		this.textId = textId;
	}
	public ArrayList<String> getWordIds() {
		return wordIds;
	}
	public void setWordIds(ArrayList<String> wordIds) {
		this.wordIds = wordIds;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getRightContext() {
		return rightContext;
	}
	public void setRightContext(String rightContext) {
		this.rightContext = rightContext;
	}
	public String getLeftContext() {
		return leftContext;
	}
	public void setLeftContext(String leftContext) {
		this.leftContext = leftContext;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String pivot) {
		this.keyword = pivot;
	}

	public void setIsTruncated(boolean isTruncated) {
		this.isTruncated = isTruncated;
	}
	public boolean getIsTruncated() {
		return isTruncated;
	}
	
}
