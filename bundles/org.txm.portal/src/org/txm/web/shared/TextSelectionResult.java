package org.txm.web.shared;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.user.client.rpc.IsSerializable;

public class TextSelectionResult implements IsSerializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5994483227286166532L;
	
	/**list of corpus texts**/
	public ArrayList<String> textids;
	public HashMap<String, Integer> numberOfWordsPerText;
	public HashMap<String, HashMap<String, String>> valuesPerText;
	
	public ArrayList<String> properties;
	public HashMap<String, MetadataInfos> propertiesInfos;
	public ArrayList<String> displayProperties;
	public ArrayList<String> selectionProperties;
	public HashMap<String, ArrayList<String>> valuesPerProperties;

	public ArrayList<String> groups;
	public HashMap<String, ArrayList<String>> groupsProperties;
	public HashMap<String, MetadataInfos> groupsInfos;
	public ArrayList<String> displayGroups;
	public ArrayList<String> selectionGroups;

	public HashMap<String, String> biblios;
	public HashMap<String, String> pdfs;
	public HashMap<String, String> editions;

	public void dump() {
		System.out.println("textids : "+textids);
		System.out.println("no of words : "+numberOfWordsPerText);

		System.out.println("properties : "+properties);
		System.out.println("properties infos : "+propertiesInfos);
		System.out.println("valuesPerProperties : "+valuesPerProperties);
		System.out.println("valuesPerTexts : "+valuesPerText);
		
		System.out.println("groups: "+groups);
		System.out.println("group infos : "+groupsInfos);
		System.out.println("groups properties: "+groupsProperties);
		
		System.out.println("biblios : "+biblios);
		System.out.println("editions available : "+editions);
		System.out.println("pdfs : "+pdfs);
		
	}
	
	public String toString() {
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("\ntextids : "+textids);
		buffer.append("\nno of words : "+numberOfWordsPerText);

		buffer.append("\nproperties : "+properties);
		buffer.append("\nproperties infos : "+propertiesInfos);
		buffer.append("\nvaluesPerProperties : "+valuesPerProperties);
		buffer.append("\nvaluesPerTexts : "+valuesPerText);
		
		buffer.append("\ngroups: "+groups);
		buffer.append("\ngroup infos : "+groupsInfos);
		buffer.append("\ngroups properties: "+groupsProperties);
		
		buffer.append("\nbiblios : "+biblios);
		buffer.append("\neditions available : "+editions);
		buffer.append("\npdfs : "+pdfs);
		
		return buffer.toString();
	}
}
