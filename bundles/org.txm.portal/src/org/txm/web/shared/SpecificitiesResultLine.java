package org.txm.web.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class SpecificitiesResultLine implements IsSerializable, Comparable<SpecificitiesResultLine>{

	private static final long serialVersionUID = 1719311347481958566L;
	
	private String word;
	private double frequency;
	private double cofrequency;
	private double specificity;
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public double getFrequency() {
		return frequency;
	}
	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}
	public double getCofrequency() {
		return cofrequency;
	}
	public void setCofrequency(double cofrequency) {
		this.cofrequency = cofrequency;
	}
	public double getSpecificity() {
		return specificity;
	}
	public void setSpecificity(double specificity) {
		this.specificity = specificity;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public int compareTo(SpecificitiesResultLine sr) {
		return new Double(specificity).compareTo(new Double(sr.getSpecificity()));
	}

}
