package org.txm.web.shared;

import java.util.Map;

import com.google.gwt.user.client.rpc.IsSerializable;

public class AFCParams extends CommandParameters implements IsSerializable {

    private String path;
    private String property;
    private Boolean showCol;
    private Boolean showLine;
    private Integer firstDim;
    private Integer secondDim;
    private Integer fMin;
    private Integer vMax;
    private Integer ID = new java.util.Random().nextInt();

//	public AFCParams(String path, String prop, int ml, int mf){
//		this.path = path;
//		this.property = prop;
//		this.maxLines = ml;
//		this.minFreq = mf;
//		
//	}
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Integer getID() {
        return ID;
    }

    public Boolean isShowCol() {
        return showCol;
    }

    /**
     * @param showCol the showCol to set
     */
    public void setShowCol(Boolean showCol) {
        this.showCol = showCol;
    }

    /**
     * @return the showLine
     */
    public Boolean isShowLine() {
        return showLine;
    }

    /**
     * @param showLine the showLine to set
     */
    public void setShowLine(Boolean showLine) {
        this.showLine = showLine;
    }

    /**
     * @return the firstDim
     */
    public Integer getFirstDim() {
        return firstDim;
    }

    /**
     * @param firstDim the firstDim to set
     */
    public void setFirstDim(Integer firstDim) {
        this.firstDim = firstDim;
    }

    /**
     * @return the secondDim
     */
    public Integer getSecondDim() {
        return secondDim;
    }

    /**
     * @param secondDim the secondDim to set
     */
    public void setSecondDim(int secondDim) {
        this.secondDim = secondDim;
    }

	public Integer getFmin() {
		return fMin;
	}
	
	public Integer getVmax() {
		return vMax;
	}
	
	public Integer setFmin(Integer fMin) {
		this.fMin = fMin;
		return fMin;
	}
	
	public Integer setVmax(Integer vMax) {
		this.vMax = vMax;
		return vMax;
	}

	@Override
	public void setParameters(Map<String, String> parameters) {
		/*
		 *     
    private String path;
    private String property;
    private Boolean showCol;
    private Boolean showLine;
    private Integer firstDim;
    private Integer secondDim;
    private Integer fMin;
    private Integer vMax;
		 */
		for (String p : parameters.keySet()) {
			if (Keys.PATH.equals(p)) {
				path = parameters.get(p);
			} else if (Keys.PROPERTY.equals(p)) {
				property = parameters.get(p);
			} else if (Keys.FMIN.equals(p)) {
				fMin = toInt(parameters.get(p));
			} else if (Keys.VMAX.equals(p)) {
				vMax = toInt(parameters.get(p));
			} else if (Keys.FIRSTDIM.equals(p)) {
				firstDim = toInt(parameters.get(p));
			}  else if (Keys.SECONDDIM.equals(p)) {
				secondDim = toInt(parameters.get(p));
			} else if (Keys.SHOWCOLS.equals(p)) {
				showCol = toBoolean(parameters.get(p));
			}  else if (Keys.SHOWLINES.equals(p)) {
				showLine = toBoolean(parameters.get(p));
			}
		}	
		
	}
}
