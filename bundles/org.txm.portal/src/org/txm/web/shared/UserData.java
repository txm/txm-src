package org.txm.web.shared;

import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;
/**
 * container for user informations
 * @author mdecorde
 *
 */
public class UserData implements IsSerializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4398414181239021591L;
	public String login ="";
	public String password="";
	public String profil="";
	public String mail="";
	public String firstname="";
	public String lastname="";
	public String newpassword="";
	public String captcha="";
	
	public String status="";
	public String institution="";
	public String phone="";

	public Date lastconnection= new Date();
	
	public UserData()
	{
		
	}
	
	public UserData(String login, String password, String profil, String mail, String firsname, String lastname, String captcha, String status, String institution, String phone)
	{
		this.login = login;
		this.password = password;
		this.profil = profil;
		this.mail = mail;
		this.firstname = firsname;
		this.lastname = lastname;
		this.captcha = captcha;
		this.status = status;
		this.institution = institution;
		this.phone = phone;
	}
	
	public String toString()
	{
		return 	" login: "+login+"\n mail: "+mail+"\n profil: "+profil+
				"\n newpassword: "+newpassword+"\n password: "+password+
				"\n firstname: "+firstname+"\n lastname: "+lastname+
				"\n phone: "+phone+"\n status: "+status+
				"\n institution : "+institution;
	}
	
	public static boolean checkMail(String mail2) {
		if (mail2 == null)
			return false;
		if (!mail2.contains("@") || !mail2.contains("."))
			return false;
		return true;
	}

	public static int MINPASSWORD = 8;
	public static int MAXPASSWORD = 20;
	public static boolean checkValidPassword(String newpassword2) {
		if (newpassword2 == null)
			return false;
		if (!newpassword2.matches(".{"+MINPASSWORD+","+MAXPASSWORD+"}"))
			return false;
		
		return true;
	}
	
	public static int MINLOGIN = 6;
	public static int MAXLOGIN = 20;
	public static boolean checkValidLogin(String login2) {
		if (login2 == null)
			return false;
		if (!login2.matches("\\w{"+MINLOGIN+","+MAXLOGIN+"}"))
			return false;
		return true;
	}
	
	/**
	 * test if infos for subscription are Ok and normalize them
	 * @return
	 */
	public boolean testSubscriptionInfos()
	{
		if (captcha == null || password == null || lastname == null || firstname == null ||
				status == null || mail == null)
		{
			return false;
		}
		
		lastname = lastname.trim();
		password = password.trim();
		captcha = captcha.trim();
		firstname = firstname.trim();
		status = status.trim();
		institution = institution.trim();
		mail = mail.trim();
		if(phone != null)
			phone = phone.trim();
		else
			phone = "";
		
		if (password.length() == 0 || lastname.length() == 0 || captcha.length() == 0 ||
				firstname.length() == 0 || status.length() == 0 || 
				mail.length() == 0) {
			return false;
		}
		return true;
	}
}
