package org.txm.web.shared;

/**
 * All the actions that a user can do.
 * To one action there is one and only one Permission that defines the action
 * @author vchabanis
 *
 */
public enum Actions {
	ALL,
	DISPLAY, 
	DELETE,
	CONCORDANCE,
	PARALLEL,
	SPECIFICITIES,
	CREATESUBCORPUS, 
	CREATEPARTITION,
	INDEX,
	LEXICON,
	EDITION,
	PROPERTIES,
	TEXTSELECTION, 
	TSQUERY, 
	BIBLIO,
	REFERENCER,
	COOCCURRENCE,
	AFC, 
	CLASSIFICATION,
	TEXTS,
	DOCUMENTATION, 
	DOWNLOAD
}
