package org.txm.web.shared;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;
/**
 * concordance results.
 * @author mdecorde
 *
 */
public class ConcordanceResult extends ArrayList<ConcordanceResultLine> implements IsSerializable {
	
	public ConcordanceResult(String error) {
		this.error = error;
	}
	
	public ConcordanceResult() { }
	private static final long serialVersionUID = 7516714651562534527L;
	
	public String error = null;
	public Integer total = 0;
	public boolean isTruncated = false;
	
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Total lines: "+total+"\n");
		buffer.append("Truncated: "+isTruncated+"\n\n");
		
		buffer.append("reference\tleftContext\tkeyword\trightContext\tisTruncated\ttextId\twordIds\n");
		boolean next = false;
		for (ConcordanceResultLine line : this) {
			if (next) buffer.append("\n");
			buffer.append(line);
		}
		
		return buffer.toString();
	}
	
	public String toStringContexts() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Total lines: "+total+"\n");
		buffer.append("Truncated: "+isTruncated+"\n\n");
		
		buffer.append("reference\tContext\tisTruncated\ttextId\twordIds\n");
		boolean next = false;
		for (ConcordanceResultLine line : this) {
			if (next) buffer.append("\n");
			buffer.append(line.toStringContext());
		}
		
		return buffer.toString();
	}
}
