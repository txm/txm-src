package org.txm.web.shared;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.smartgwt.client.widgets.tree.TreeNode;
/**
 * 
 * @author vchabanis
 *
 */
public class NavigationTree extends ArrayList<NavigationTree> implements IsSerializable {

	private static final long serialVersionUID = -7185618952775244301L;

	private boolean isLeaf = true;
	private NavigationTree parent = null;

	private String ID = null;
	private String fullname = null;
	private String name = null;
	private String ns = null;
	private String path = "";
	private String pathWithoutNs = "";
	private Type type = Type.other;

	private HashMap<String, String[]> structuralUnits = new HashMap<String, String[]>();
	private ArrayList<String> wordProperties = new ArrayList<String>();

	public enum Type {corpus, subcorpus, partition, other};

	public NavigationTree(String name, String ns, Type type, String ID) {
		this.name = name;
		this.fullname = name;
		this.type = type;
		this.ID = ID;
		this.ns = ns;
		if(ns != null && ns.length() > 0)
			this.fullname = ns+":"+name;
	}

	public NavigationTree(String name, String ns, Type type) {
		this.name = name;
		this.fullname = name;
		this.type = type;
		this.ns = ns;
		if(ns != null && ns.length() > 0)
			this.fullname = ns+":"+name;
	}
	
	public NavigationTree(String name, String ns) {
		this.name = name;
		this.fullname = name;
		this.ns = ns;
		if(ns != null && ns.length() > 0)
			this.fullname = ns+":"+name;
	}

	public NavigationTree(String name) {
		this.name = name;
		this.fullname = name;
		this.ns = "";
	}

	public NavigationTree() {
		super();
	}

	public boolean isLeaf() {
		return isLeaf;
	}

	public void setParent(NavigationTree parent) {
		this.parent = parent;
	}

	public NavigationTree getParent() {
		return parent;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Type getType() {
		return type;
	}
	
	public void setNS(String ns) {
		this.ns = ns;
	}

	public String getNS() {
		return ns;
	}

	public void setID(String ID) {
		this.ID = ID;
	}

	public String getID() {
		return ID;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setPath(String newPath) {
		this.path = newPath;
	}

	public String getPath() {
		return path;
	}

	public void setStructuralUnits(HashMap<String, String[]> structuralUnits) {
		this.structuralUnits = structuralUnits;
	}

	public HashMap<String, String[]> getStructuralUnits() {
		return structuralUnits;
	}

	public void setWordProperties(ArrayList<String> wordProperties) {
		this.wordProperties = wordProperties;
	}

	public boolean isBaseCorpus() {
		if ((this.type == Type.corpus) || (this.type == Type.subcorpus))
			return true;
		return false;
	}

	public ArrayList<String> getWordProperties() {
		return wordProperties;
	}

	public ArrayList<String> getReferences()
	{
		ArrayList<String> refs = new ArrayList<String>();
		for(String struct : structuralUnits.keySet())
			for(String p : structuralUnits.get(struct))
				refs.add(struct+":"+p);
		return refs;
	}

	public boolean add(NavigationTree elem) {
		isLeaf = false;
		elem.setParent(this);
		path.concat("/" + ns+":"+name);
		return super.add(elem);
	}

	public boolean remove(NavigationTree elem) {
		isLeaf = true;
		return super.remove(elem);
	}

	public NavigationTree get(String item) {
		for (NavigationTree child : this) {
			if (child.getFullName().equals(item))
				return child;
		}
		return null;
	}

	public String getFullName() {
		return fullname;
	}
	
	private void setFullName(String f) {
		fullname = f;
	}

	public TreeNode[] toTreeNodeArray() {
		List<TreeNode> childList = new ArrayList<TreeNode>();
		for (NavigationTree child : this) 
		{
			childList.add(child.toTreeNode());
		}
		return childList.toArray(new TreeNode[childList.size()]);
	}

	/**
	 * convert this instance into TreeNode
	 * @return
	 */
	public TreeNode toTreeNode() {
		TreeNode children;
		if(ns != null && ns.length() > 0)
			children = new TreeNode(this.getNS()+":"+this.getName());
		else
			children = new TreeNode(this.getName()); // ex : Maincorpus DISCOURS has no NS

		children.setAttribute("ns", this.getNS());
		//children.setAttribute("id", this.getName());
		children.setAttribute("path", this.getPath());
		children.setAttribute("path2", this.getName());
		children.setID(this.getPath()); // selenium
		
		switch (this.getType()) {
		case subcorpus :
			children.setAttribute("type", "subcorpus");
			children.setIcon("icons/subcorpus.png");
			break;
		case partition :
			children.setAttribute("type", "partition");
			children.setIcon("icons/partition.png");
			break;
		case corpus :
			children.setAttribute("type", "corpus");
			children.setIcon("icons/corpus.png");
			break;
		default :
			children.setAttribute("type", "other");
			children.setIcon("icons/books.png");
			break;
		}

		// process its children
		if (!this.isEmpty()) {
			// TODO add children in view (just uncomment)
			List<TreeNode> childList = new ArrayList<TreeNode>();
			for (NavigationTree child : this) {
				TreeNode treeChild = child.toTreeNode();
				treeChild.setAttribute("path", child.getPath());
				childList.add(treeChild);
			}
			children.setChildren(childList.toArray(new TreeNode[childList.size()]));
		}
		return children;
	}

	public String toString() {
		String result = this.ns+":"+this.name+"-"+fullname + ",path="+this.path+", path2="+pathWithoutNs+" -> " + this.type + "\n"+super.toString();
		return result;
	}

	public String getPathWithoutNs() {
		return pathWithoutNs;
	}
	
	public void setPathWithoutNs(String path) {
		pathWithoutNs = path;
	}
}
