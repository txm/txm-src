package org.txm.web.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Edition result container
 * @author vchabanis
 *
 */
public class EditionResult implements IsSerializable {

	private static final long serialVersionUID = 4625603615956971367L;
	
	private String content = "";
	private String currentPage = "";
	private int totalPage = 1;
	private String name = "";
	private String baseId = "";
	private String textId = "";
	private String biblio = "";
	private String pdf = "";
	private String edition = "";
	private String wordid;
	
	public String toString() {
		return "content.length="+content.length()+" page="+currentPage+
			" totalPage="+totalPage+" name="+name+" textID="+textId+" baseId="+baseId+
			" biblio="+biblio+" pdf="+pdf+" edition="+edition+" wordid="+wordid;
	}
	
	public String getBaseId() {
		return baseId;
	}
	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	public String getName() {
		return name;
	}
	public String getEdition(){
		return edition;
	}
	public void setEdition(String ed){
		edition = ed;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setTextId(String textId) {
		this.textId = textId;
	}
	public String getTextId() {
		return textId;
	}
	public String getBiblio() {
		return biblio;
	}
	public void setBiblio(String biblio2) {
		this.biblio = biblio2;
	}
	public String getPDF() {
		return pdf;
	}
	public void setPDF(String pdf2) {
		pdf = pdf2;
	}
	public String getWordId() {
		return this.wordid;
	}
	public void setWordId(String id) {
		this.wordid = id;;
	}
}
