package org.txm.web.shared;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import com.google.gwt.user.client.rpc.IsSerializable;

public class CooccurrenceParam extends CommandParameters implements IsSerializable {

	private static final long serialVersionUID = -5293197006568492303L;
	
	private int ID = new java.util.Random().nextInt();
	
	private String corpusPath;
	private String query;
	private ArrayList<String> properties = new ArrayList<>(Arrays.asList("word"));
	private String structure;
	private int leftmin = 0;
	private int leftmax = 10;
	private int rightmin = 0;
	private int rightmax = 10;
	private int minfreq = 0;
	private int mincofreq = 0;
	private double minscore = 0.0;
	private boolean pivot = true;
	
	public String getCorpusPath() {
		return corpusPath;
	}
	public void setCorpusPath(String corpusPath) {
		this.corpusPath = corpusPath;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public ArrayList<String> getProperties() {
		return properties;
	}
	public void setProperties(ArrayList<String> properties) {
		this.properties = properties;
	}
	public String getStructure() {
		return structure;
	}
	public void setStructure(String structure) {
		this.structure = structure;
	}
	public int getLeftmin() {
		return leftmin;
	}
	public void setLeftmin(int leftmin) {
		this.leftmin = leftmin;
	}
	public int getLeftmax() {
		return leftmax;
	}
	public void setLeftmax(int leftmax) {
		this.leftmax = leftmax;
	}
	public int getRightmin() {
		return rightmin;
	}
	public void setRightmin(int rightmin) {
		this.rightmin = rightmin;
	}
	public int getRightmax() {
		return rightmax;
	}
	public void setRightmax(int rightmax) {
		this.rightmax = rightmax;
	}
	public int getMinfreq() {
		return minfreq;
	}
	public void setMinfreq(int minfreq) {
		this.minfreq = minfreq;
	}
	public int getMinCofreq() {
		return mincofreq;
	}
	public void setMinCofreq(int mincofreq) {
		this.mincofreq = mincofreq;
	}
	public double getMinscore() {
		return minscore;
	}
	public void setMinscore(double minscore) {
		this.minscore = minscore;
	}
	public void setPivot(boolean pivot){
		this.pivot = pivot;
	}
	public boolean getPivot(){
		return pivot;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public int getID() {
		return ID;
	}
	@Override
	public void setParameters(Map<String, String> parameters) {
		/*
		 * 	private String corpusPath;
	private String query;
	private ArrayList<String> properties;
	private String structure;
	private int leftmin;
	private int leftmax;
	private int rightmin;
	private int rightmax;
	private int minfreq;
	private int mincofreq;
	private double minscore;
	private boolean pivot;
		 */
		for (String p : parameters.keySet()) {
			if (Keys.QUERY.equals(p)) {
				query = parameters.get(p);
			} else if (Keys.PATH.equals(p)) {
				corpusPath = parameters.get(p);
			} else if (Keys.PROPERTIES.equals(p)) {
				properties = toStringArrayList(parameters.get(p));
			} else if (Keys.STRUCTURE.equals(p)) {
				structure = parameters.get(p);
			} else if (Keys.FMAX.equals(p)) {
				leftmin = toInt(parameters.get(p));
			} else if (Keys.NLINESPERPAGE.equals(p)) {
				leftmax = toInt(parameters.get(p));
			} else if (Keys.VMAX.equals(p)) {
				rightmin = toInt(parameters.get(p));
			} else if (Keys.INDEX.equals(p)) {
				rightmax = toInt(parameters.get(p));
			} else if (Keys.VMAX.equals(p)) {
				minfreq = toInt(parameters.get(p));
			} else if (Keys.INDEX.equals(p)) {
				mincofreq = toInt(parameters.get(p));
			} else if (Keys.INDEX.equals(p)) {
				minscore = toDouble(parameters.get(p));
			}  else if (Keys.INCLUDE_PIVOT.equals(p)) {
				pivot = toBoolean(parameters.get(p));
			}
		}	
	}

			
}
