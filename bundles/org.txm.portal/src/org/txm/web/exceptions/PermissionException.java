package org.txm.web.exceptions;

public class PermissionException extends Exception {

	private static final long serialVersionUID = 3040048763579387273L;

	public PermissionException(String msg) {
		super(msg);
	}
	
	public PermissionException() {
		super();
	}
}
