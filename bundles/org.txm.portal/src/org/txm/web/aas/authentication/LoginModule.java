package org.txm.web.aas.authentication;

/**
 * The interface of a login module.
 * Kind of CRUD
 * 
 * @author vchabanis
 *
 */
public interface LoginModule {
	/**
	 * test a user's password
	 * @param userName
	 * @param password
	 * @return
	 */
	public boolean login(String userName, String password);
	
	/**
	 * test if user exists
	 * @param userName
	 * @param password
	 * @return
	 */
	public boolean exists(String login);
	/**
	 * add a user and its password 
	 * @param userName
	 * @param password
	 * @return
	 */
	public boolean create(String userName, String password);
	/**
	 * update the user's password 
	 * @param userName
	 * @param password
	 * @return
	 */
	public boolean update(String userName, String password);
	/**
	 * update
	 * @return
	 */
	public boolean update();
	/**
	 * remove a user
	 * @param userName
	 * @return
	 */
	public boolean delete(String userName);

	public String getMD5Login(String login);

	public boolean setMD5Password(String login, String md5);
}
