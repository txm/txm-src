package org.txm.web.aas.authentication;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.txm.utils.logger.Log;
import org.txm.web.server.TxmUser;

/**
 * The login module permits only to verif y if a login/password exists and change or add it
 * data is stored with md5 encoding in a file which path is defined in the txm_ria.conf file, by the key loginBase
 * 
 * @see TxmUser
 * @author vchabani
 * @version 0.2.0
 */
public class TxmLoginModule implements LoginModule {

	public static String LOGINBASE = "";//TxmConfig.getProperty("loginBase");
	/**
	 * associate a to user to a md5 password
	 */
	private Map<String, String> loginMap = Collections.synchronizedMap(new HashMap<String, String>());

	/**
	 * Opens an load the login base from the specified file. The login base file
	 * is actually written in the source code, but soon, it'll be in the System.properties
	 * or in a configuration file.
	 */
	public TxmLoginModule() {	
//		load();
	}
	
	public boolean load()
	{
		try {
		String line = null;
		FileReader reader = new FileReader(LOGINBASE);
		BufferedReader br = new BufferedReader(reader);
		while ((line = br.readLine()) != null) {
			if (line.length() > 0)
			{
				String[] logs = line.split(" ");
				loginMap.put(logs[0], logs[1]);
			}
		}
		reader.close();
	} catch (Exception e) {
		Log.severe("[TxmLoginModule.constructor] "+Log.toString(e));
	}
		return true;
	}

	/**
	 * Check if the identity exists in the login base.
	 * 
	 * @param userName The login
	 * @param password The password
	 * @return true if the identity exists, false otherwise
	 */
	@Override
	public synchronized boolean login(String userName, String password) {
		String digestPass = md5(password);
		if (loginMap.get(userName) == null)
			return false;
		if (loginMap.get(userName).equals(digestPass))
			return true;
		return false;
	}

	/**
	 * @param userName The login
	 * @param password The password
	 * @return true if success
	 */
	@Override
	public synchronized boolean create(String userName, String password) {
		if (loginMap.containsKey(userName))
		{
			Log.warning("[TxmLoginModule.create] User "+userName+" already exists");
			return false;
		}
		try {
			String digestPass = md5(password);
			FileWriter writer = new FileWriter(LOGINBASE, true);
			BufferedWriter bw = new BufferedWriter(writer);
			bw.write(userName + " " + digestPass + "\n");
			bw.flush();
			writer.close();
			loginMap.put(userName, digestPass);
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	@Override
	/**
	 * not implemented
	 */
	public synchronized boolean delete(String userName) {
		if (userName.contains(".usr"))
			userName = userName.replaceAll(".usr", "");
		loginMap.remove(userName);
		update();
		return true;
	}

	@Override
	/**
	 * update the password of a user
	 */
	public synchronized boolean update(String userName, String password) 
	{
		String digestPass = md5(password);
		loginMap.put(userName, digestPass);//update the map
		return update();
	}

	/**
	 * update the loginbase file
	 */
	public synchronized boolean update() 
	{
		try {
			FileWriter writer = new FileWriter(LOGINBASE);//rewrite all loginBase
			BufferedWriter bw = new BufferedWriter(writer);
			for (String key : loginMap.keySet()) {
				bw.write(key + " " + loginMap.get(key) + "\n");
			}
			bw.flush();
			writer.close();
			return true;
		} catch (IOException e) {
			return false;
		}
	}
	
	/**
	 * md5 encoder
	 * @param data the string to encode
	 * @return the encoded value as a String
	 */
	public static String md5(String data) {
		byte[] toHash = data.getBytes();
		byte[] MD5Digest = null;
		StringBuilder hashString = new StringBuilder();

		MessageDigest algo = null;
		try {
			algo = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		algo.reset();
		algo.update(toHash);
		MD5Digest = algo.digest();

		for (int i = 0; i < MD5Digest.length; i++) {
			String hex = Integer.toHexString(MD5Digest[i]);
			if (hex.length() == 1) {
				hashString.append('0');
				hashString.append(hex.charAt(hex.length() - 1));
			} else {
				hashString.append(hex.substring(hex.length() - 2));
			}
		}
		return hashString.toString();
	}

	@Override
	public boolean exists(String login) {
		return loginMap.containsKey(login);
	}

	@Override
	public String getMD5Login(String login) {
		return loginMap.get(login);
	}

	@Override
	public boolean setMD5Password(String login, String md5) {
		loginMap.put(login, md5);
		return update();
	}
	
	public static void main(String[] args)
	{
		System.out.println(md5("testme"));
	}
}
