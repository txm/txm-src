package org.txm.web.aas.authorization.permissions;

import org.txm.web.aas.authorization.TxmPermission;
import org.txm.web.shared.Actions;

/**
 * The Permission of the Lexicon action
 * No parameters
 * @author vchabanis
 *
 */
public class LexiconPermission extends TxmPermission {
	
	private static final long serialVersionUID = -2035181452663153851L;

	public LexiconPermission(String name) {
		super(name);
	}
	
	public LexiconPermission() {
		super("dictionnary");
	}

	@Override
	public Actions getAction() {
		return Actions.LEXICON;
	}
}
