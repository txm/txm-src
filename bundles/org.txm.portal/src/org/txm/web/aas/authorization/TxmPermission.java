package org.txm.web.aas.authorization;

import java.lang.reflect.Constructor;
import java.security.BasicPermission;
import java.security.Permission;

import org.txm.utils.logger.Log;
import org.txm.web.shared.Actions;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public abstract class TxmPermission extends BasicPermission {

	private static final long serialVersionUID = -157257317578082506L;
	protected boolean anti = false;
	public TxmPermission(String name) {
		super(name);
	}

	public String toXml() {
		return "<permission value=\""+this.getClass().getSimpleName()+"\" anti=\""+anti+"\"/>";
	}

	public void toXml(Element permElem)
	{
		permElem.setAttribute("value", this.getClass().getSimpleName());
		permElem.setAttribute("anti", ""+anti);
	}

	/**
	 * TODO: use <prop> tag to store permission parameters (concordances...)
	 * 
	 * @param attributes a xml attribute
	 * @return an instance of permission
	 */
	public static TxmPermission permissionFactory(NamedNodeMap attributes) {
		String namespace = "org.txm.web.aas.authorization.permissions.";
		String value = attributes.getNamedItem("value").getNodeValue();
		Node node = attributes.getNamedItem("not");
		String santi = null;
		if (node != null)
			santi = node.getNodeValue();

		//if (value.equals("AllPermission"))
		//	namespace = "java.security.";
		if (value.equals("AllPermission"))
			value = "TxmAllPermission";
		else if (value.equals("DictionnaryPermission")) 
			value = "LexiconPermission"; // Temporary transition fix from DictionnaryPermission to LexiconPermission
		TxmPermission permission = null;
		try {
			Class<?> c = Class.forName(namespace + value);

			if (value.equals("ConcordancePermission")) {
				//System.out.println("CREATE CONCORDANCEPERMISSION: "+attributes.getNamedItem("rightContext")+" "+attributes.getNamedItem("leftContext"));
				int rightContext = 10;
				int leftContext = 10;
				try {
					rightContext = Integer.parseInt(attributes.getNamedItem("rightContext").getNodeValue());
					leftContext = Integer.parseInt(attributes.getNamedItem("leftContext").getNodeValue());
				} catch(Exception e2) {
					Log.severe("[TxmPermission.permissionFactory] Concordance permission node is malformed: "+e2);
					return null;
				}

				for (Constructor<?> constructor : c.getConstructors()) {
					try {
						permission = (TxmPermission) constructor.newInstance(leftContext, rightContext);
						break;
					} catch (Exception e) { }	
				}
				if (permission == null) {
					Log.severe("[TxmPermission.permissionFactory] could not find constructor for 'ConcordancePermission'"); 
				}
			}
			//			else if (value.equals("CooccurrencePermission")) {
			//				int rightContext = 10;
			//				int leftContext = 10;
			//				try
			//				{
			//					rightContext = Integer.parseInt(attributes.getNamedItem("rightContext").getNodeValue());
			//					leftContext = Integer.parseInt(attributes.getNamedItem("leftContext").getNodeValue());
			//				}catch(Exception e2)
			//				{
			//					Log.severe("[TxmPermission.permissionFactory] Cooccurrence permission node is malformed");
			//					return null;
			//				}
			//				try {
			//					for (Constructor<?> constructor : c.getConstructors()) {
			//						permission = (TxmPermission) constructor.newInstance((Integer)leftContext, (Integer)rightContext);
			//						break;
			//					}
			//				} catch (Exception e) {
			//					System.out.println(e.getMessage());
			//				}
			//			}
			else {
				Constructor<?> constructor = c.getConstructor(new Class[] {});
				if (constructor != null) {
					permission = (TxmPermission) constructor.newInstance();
				} else {
					Log.severe("Error while reading permissions declaration in element with attributes: "+attributes );
					return null;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		if (santi != null) {
			permission.setAnti(Boolean.valueOf(santi));
			//System.out.println("negativ perm: "+permission.isAnti());
		}
		//System.out.println("build perm: ("+attributes+") "+permission);
		return permission;
	}

	/**
	 * If permission is set to anti, the method implies() will return !super.implies()
	 * @param state
	 */
	public void setAnti(boolean state)
	{
		anti = state;
	}

	public boolean isAnti()
	{
		return anti;
	}

	public boolean implies(Permission p) {
		if (anti)
			return !super.implies(p);
		else
			return super.implies(p);
	}

	public abstract Actions getAction();
}
