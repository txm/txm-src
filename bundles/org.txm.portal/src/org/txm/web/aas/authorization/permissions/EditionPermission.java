package org.txm.web.aas.authorization.permissions;

import org.txm.web.aas.authorization.TxmPermission;
import org.txm.web.shared.Actions;

/**
 * This Permission defines if the edition of an object can be displayed
 * No parameters
 * @author vchabanis
 *
 */
public class EditionPermission extends TxmPermission {

	private static final long serialVersionUID = -2382092575495828808L;

	public EditionPermission(String name) {
		super(name);
	}
	
	public EditionPermission() {
		super("edition");
	}

	@Override
	public Actions getAction() {
		return Actions.EDITION;
	}
}
