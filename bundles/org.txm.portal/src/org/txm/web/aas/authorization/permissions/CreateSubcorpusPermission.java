package org.txm.web.aas.authorization.permissions;

import org.txm.web.aas.authorization.TxmPermission;
import org.txm.web.shared.Actions;

/**
 * the Permission which allow to create sub corpus
 * No parameters
 * @author vchabanis
 *
 */
public class CreateSubcorpusPermission extends TxmPermission {

	private static final long serialVersionUID = -3794331967875957480L;

	public CreateSubcorpusPermission(String name) {
		super(name);
	}
	
	public CreateSubcorpusPermission() {
		super("createSubcorpus");
	}
	
	@Override
	public Actions getAction() {
		return Actions.CREATESUBCORPUS;
	}
}
