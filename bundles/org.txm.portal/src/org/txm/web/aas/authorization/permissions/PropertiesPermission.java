package org.txm.web.aas.authorization.permissions;

import org.txm.web.aas.authorization.TxmPermission;
import org.txm.web.shared.Actions;

/**
 * The permission to see the properties of a corpus or partition
 * @author mdecorde
 *
 */
public class PropertiesPermission extends TxmPermission {
	
	private static final long serialVersionUID = -7724046703005820858L;

	public PropertiesPermission(String name) {
		super(name);
	}
	
	public PropertiesPermission() {
		super("properties");
	}

	@Override
	public Actions getAction() {
		return Actions.PROPERTIES;
	}
}
