package org.txm.web.aas.authorization;

import java.io.File;
import java.io.Serializable;
import java.security.Permission;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.txm.objects.Text;
import org.txm.utils.logger.Log;
import org.txm.web.server.statics.Corpora;
import org.txm.web.server.statics.TxmConfig;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

/**
 * A profile is a container of permissions described in a profile.xml. It allows
 * a user to do the specif ied actions
 * 
 * @author vchabani
 * @version 0.2.0
 */
public class Profile extends HashMap<String, HashSet<TxmPermission>> implements Serializable {

	private static final long serialVersionUID = 3732393487483474627L;

	private String name = "";
	
	private String homePage; // = TxmConfig.getProperty("homePageDir");
	private String helpPage; // = TxmConfig.getProperty("helpPageDir");
	private String contactPage; // = TxmConfig.getProperty("contactPageDir");
	
	public String assignMail = "This is an automatically generated message.\n\n" +
			"The administrator has assigned the profile '%2$s' to your account '%1$s'.\n" +
			"Follow this link to connect to TXM WEB: %3$s";

	public String assignSubject = "TXM WEB - profile assignement";

	/**
	 * create an empty profile
	 */
	public Profile() {
		
	}

	/**
	 * creates a profile defined in a file
	 * @param configFileName
	 */
	public Profile(String configFileName) {
		load(configFileName);
	}

	public String toString() {
		String mailAbstract = this.assignMail;
		if (mailAbstract.length() > 10)
			mailAbstract = mailAbstract.substring(0, 10);
		return name+", message("+this.assignSubject+")="+mailAbstract+"\n"+super.toString();
	}
	
	/**
	 * Loads the permissions of a profile, given a profile xml file
	 *  
	 * @param configFileName see dev manual for the format of this file
	 * @return true if success
	 */
	@SuppressWarnings("unchecked")
	public boolean load(String configFileName) {
		//configFileName = 
		this.clear();
		Log.info("[Profile.load] " + configFileName);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		File configFile = new File(TxmConfig.getProperty("profileBase"), configFileName);
		//TODO: configFileName is the file name
		if (!configFile.exists()) {
			configFile = new File(configFileName);
			if (!configFile.exists()) {
				Log.severe("[Profile.load] " + configFileName + " not found.");
				return false;
			}
		}
		Document dom;//read the xml file
		this.clear();
		try {
			db = dbf.newDocumentBuilder();
			dom = db.parse(configFile);
			Element selfElement = dom.getDocumentElement();
			this.setName(selfElement.getAttribute("name"));
			this.setHomePage(selfElement.getAttribute("homepage"));
			this.setHelpPage(selfElement.getAttribute("helppage"));
			this.setContactPage(selfElement.getAttribute("contactpage"));
			
			//init assignement messages
			NodeList messagesList = selfElement.getElementsByTagName("messages");// get all entity elements
			for (int iMessages = 0 ; iMessages < messagesList.getLength() ; iMessages++)
			{
				Element messages = (Element) messagesList.item(iMessages);
				NodeList messageList = messages.getElementsByTagName("message");// get all entity elements
				for (int iMessage = 0 ; iMessage < messageList.getLength() ; iMessage++) {
					Element message = (Element) messageList.item(iMessage);
					this.assignMail = message.getTextContent();
					this.assignSubject = message.getAttribute("subject");
				}
			}
			
			//initialize permissions
			NodeList entityList = selfElement.getElementsByTagName("entity");// get all entity elements
			for (int iEntity = 0 ; iEntity < entityList.getLength() ; iEntity++) {
				Element entity = (Element) entityList.item(iEntity);
				String path = entity.getAttribute("path");
				if(containsKey(path)) // the path may be already containing TxmAllPermission
					remove(path); // since the entity is here, it's declaring its own permissions
				NodeList permissionList = entity.getElementsByTagName("permission");// get all the perm associated to this entity
				HashSet<TxmPermission> permissions = new HashSet<TxmPermission>();
				for(int iPermission = 0 ; iPermission < permissionList.getLength() ; iPermission++) {
					Element permission = (Element) permissionList.item(iPermission);
					NamedNodeMap attributes = permission.getAttributes();
					TxmPermission perm = TxmPermission.permissionFactory(attributes);
					if(perm != null) {
						permissions.add(perm);
					} else {
						StringBuffer s = new StringBuffer();
						for(int i = 0 ; i < attributes.getLength() ; i++) {
							s.append(attributes.item(i)+"\n");
						}
						Log.severe("Error: can't add permission declared in XML file for profile '"+this.name+"' for attributes: "+s.toString());
					}
				}


				if (Corpora.containsKey(path)) {
					// UPDATE the equal permissions 
					if (!this.containsKey(path))
						this.put(path, permissions);
					else {
						List<Permission> toRemove = new ArrayList<Permission>();
						for (Permission p : permissions) {
							for (Permission oldP : this.get(path)) {
								if (p.getName().equals(oldP.getName())) {
									toRemove.add(oldP);
								}
							}
						}
						for (Permission p : toRemove)
							this.get(path).remove(p);
						this.get(path).addAll(permissions);
					}

					// give the profile's perms to the children of the entity
					for (String child : Corpora.getChildPaths(path))
						if (!this.containsKey(child))
							if(Corpora.get(child) instanceof Text)
								this.put(child, (HashSet<TxmPermission>) permissions.clone());
				}
				else
					Log.warning("[Profile.load] Unknown path : " + path);
			}

			return true;
		} catch (Exception e) {
			Log.warning("[Profile.load] Error while loading : " + configFileName + ": "+Log.toString(e));
			this.clear();
			return false;
		}
	}

	/**
	 * @param entityPath the Object path in Corpora
	 * @param permission the permission of the asked action
	 * @return true if the action is allowed
	 */
	public boolean implies(String entityPath, Permission permission) {
		HashSet<TxmPermission> permissions = this.get(entityPath);
		if (permissions == null || permissions.isEmpty())
			return false;
		for (TxmPermission p : permissions) {
			if (p.implies(permission))
				return true;
		}
		return false;
	}

	/**
	 * @param entityPath the Object path in Corpora
	 * @return the list of Permission of this profile with the entity
	 */
	public HashSet<TxmPermission> getPermissions(String entityPath) {
		return this.get(entityPath);
	}

	/**
	 * set the name of the Profile
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the name of the profile
	 */
	public String getName() {
		return name;
	}

	/**
	 * set the homepage of this profile
	 * @param homePage's url as String
	 */
	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}


	/**
	 * set the helpPage of this profile
	 * @param helpPage url as String
	 */
	public void setHelpPage(String helpPage) {
		this.helpPage = helpPage;
	}

	/**
	 * set the contactPage of this profile
	 * @param contactPage
	 */
	private void setContactPage(String contactPage) {
		this.contactPage = contactPage;
	}

	public String getAssignMail() {
		return assignMail;
	}
	
	public String getAssignSubject() {
		return assignSubject;
	}
	
	/**
	 * @return the homepage of this profile
	 */
	public String getHomePage() {
		return homePage;
	}

	public String getHelpPage() {
		return helpPage;
	}

	public String getContactPage() {
		return contactPage;
	}

	public String getNameWithoutExtension() {
		int idx = name.indexOf(".");
		if(idx > 0)
			return name.substring(0, idx);
		return name;
	}
}
