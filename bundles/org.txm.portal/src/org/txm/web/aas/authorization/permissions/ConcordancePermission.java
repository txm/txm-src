package org.txm.web.aas.authorization.permissions;

import java.security.AllPermission;
import java.security.Permission;

import org.txm.web.aas.authorization.TxmPermission;
import org.txm.web.shared.Actions;
import org.w3c.dom.Element;

/**
 * The permission to cmpute a concordance.
 * right and left contexts size define the implies method 
 * @author vchabanis
 *
 */
public class ConcordancePermission extends TxmPermission {

	private static final long serialVersionUID = -6286561198924409285L;

	private int rightContextSize = 0;
	private int leftContextSize = 0;

	public ConcordancePermission(int leftContext, int rightContext) {
		super("concordance");
		this.leftContextSize = Math.max(0, leftContext);
		this.rightContextSize = Math.max(0, rightContext);
	}

	public ConcordancePermission(String name, int leftContext, int rightContext) {
		super(name);
	}

	public ConcordancePermission(int contextSize) {
		super("concordance");
		if (contextSize < 0) {
			this.leftContextSize = -1;
			this.rightContextSize = -1;
		} else {
			this.leftContextSize = contextSize / 2;
			this.rightContextSize = contextSize / 2;
		}
	}

	public ConcordancePermission(String leftContext, String rightContext) {
		super("concordance");
		this.leftContextSize = Integer.parseInt(leftContext);
		this.rightContextSize = Integer.parseInt(rightContext);
	}

	public ConcordancePermission() {
		super("concordance");
	}

	/**
	 * @param p the permission of the action asked
	 * @return true if the action is allowed
	 */
	@Override
	public boolean implies(Permission p) {
		if (!super.implies(p))
			return false;
		if (p instanceof ConcordancePermission) {
			if ((((ConcordancePermission)p).getLeftContext() <= this.getLeftContext() || 
					this.getLeftContext() < 0) &&
					(((ConcordancePermission)p).getRightContext() <= this.getRightContext() || 
							this.getRightContext() < 0)) {
				return true;
			}
		}
		if (p instanceof CooccurrencePermission) {
			if ((((CooccurrencePermission)p).getLeftContext() <= this.getLeftContext() || 
					this.getLeftContext() < 0) &&
					(((CooccurrencePermission)p).getRightContext() <= this.getRightContext() || 
							this.getRightContext() < 0)) {
				return true;
			}
		}
		if (p instanceof AllPermission)
			return true;
		return false;

	}

	public int getTotalContext() {
		return rightContextSize + leftContextSize;
	}

	public void setRightContext(int rightContextSize) {
		this.rightContextSize = rightContextSize;
	}

	public int getRightContext() {
		return rightContextSize;
	}

	public void setLeftContext(int leftContextSize) {
		this.leftContextSize = leftContextSize;
	}

	public int getLeftContext() {
		return leftContextSize;
	}

	public String toString() {
		String result = super.toString();
		result = result.substring(0, result.length() - 1);
		result += " rightContext:"; 
		result += rightContextSize;
		result += " leftContext:";
		result += leftContextSize;
		result += ")";
		return result;
	}

	@Override
	public Actions getAction() {
		return Actions.CONCORDANCE;
	}
	
	@Override
	public String toXml() {
		return "<permission value=\""+this.getClass().getSimpleName()+"\" anti=\""+anti+"\" leftContext=\""+this.leftContextSize+"\" rightContext=\""+this.rightContextSize+"\"/>";
	}
	
	public void toXml(Element permElem)
	{
		super.toXml(permElem);
		permElem.setAttribute("rightContext", ""+rightContextSize);
		permElem.setAttribute("leftContext", ""+leftContextSize);
	}
}
