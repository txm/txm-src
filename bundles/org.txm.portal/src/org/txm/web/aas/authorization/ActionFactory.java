package org.txm.web.aas.authorization;

import java.security.Permission;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.txm.Toolbox;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.utils.logger.Log;
import org.txm.web.aas.authorization.permissions.AFCPermission;
import org.txm.web.aas.authorization.permissions.BiblioPermission;
import org.txm.web.aas.authorization.permissions.ClassificationPermission;
import org.txm.web.aas.authorization.permissions.ConcordancePermission;
import org.txm.web.aas.authorization.permissions.CooccurrencePermission;
import org.txm.web.aas.authorization.permissions.CreatePartitionPermission;
import org.txm.web.aas.authorization.permissions.CreateSubcorpusPermission;
import org.txm.web.aas.authorization.permissions.DeletePermission;
import org.txm.web.aas.authorization.permissions.DisplayPermission;
import org.txm.web.aas.authorization.permissions.DocumentationPermission;
import org.txm.web.aas.authorization.permissions.DownloadPermission;
import org.txm.web.aas.authorization.permissions.EditionPermission;
import org.txm.web.aas.authorization.permissions.IndexPermission;
import org.txm.web.aas.authorization.permissions.LexiconPermission;
import org.txm.web.aas.authorization.permissions.PropertiesPermission;
import org.txm.web.aas.authorization.permissions.ReferencerPermission;
import org.txm.web.aas.authorization.permissions.SpecificitiesPermission;
import org.txm.web.aas.authorization.permissions.TextSelectionPermission;
import org.txm.web.aas.authorization.permissions.TextsPermission;
import org.txm.web.aas.authorization.permissions.TsQueryPermission;
import org.txm.web.aas.authorization.permissions.TxmAllPermission;
import org.txm.web.server.TxmUser;
import org.txm.web.server.statics.Corpora;
import org.txm.web.shared.Actions;

/**
 * A class that contains instances of permissions associated to an Action (a shared object)
 * give a user's permission
 * give the possibles actions of an object 
 * @author vchabanis, mdecorde
 *
 */
public class ActionFactory {

	private static HashMap<Actions, Permission> permissionMap = new HashMap<Actions, Permission>();

	static {
		permissionMap.put(Actions.ALL, new TxmAllPermission());
		permissionMap.put(Actions.DISPLAY, new DisplayPermission());
		permissionMap.put(Actions.DELETE, new DeletePermission());
		permissionMap.put(Actions.CONCORDANCE, new ConcordancePermission());
		permissionMap.put(Actions.TSQUERY, new TsQueryPermission());
		permissionMap.put(Actions.PROPERTIES, new PropertiesPermission());
		permissionMap.put(Actions.REFERENCER, new ReferencerPermission());
		permissionMap.put(Actions.INDEX, new IndexPermission());
		permissionMap.put(Actions.LEXICON, new LexiconPermission());
		permissionMap.put(Actions.CREATESUBCORPUS, new CreateSubcorpusPermission());
		permissionMap.put(Actions.CREATEPARTITION, new CreatePartitionPermission());
		permissionMap.put(Actions.TEXTSELECTION, new TextSelectionPermission());
		permissionMap.put(Actions.BIBLIO, new BiblioPermission());
		permissionMap.put(Actions.EDITION, new EditionPermission());
		permissionMap.put(Actions.COOCCURRENCE, new CooccurrencePermission());
		permissionMap.put(Actions.SPECIFICITIES, new SpecificitiesPermission());
		permissionMap.put(Actions.AFC, new AFCPermission());
		permissionMap.put(Actions.CLASSIFICATION, new ClassificationPermission());
		permissionMap.put(Actions.TEXTS, new TextsPermission());
		permissionMap.put(Actions.DOCUMENTATION, new DocumentationPermission());
		permissionMap.put(Actions.DOWNLOAD, new DownloadPermission());
	}

	/**
	 * 
	 * @param action the name of the permission
	 * @return an instance of permission
	 * @throws IllegalArgumentException
	 */
	public static Permission create(Actions action) throws IllegalArgumentException {
		if (permissionMap.containsKey(action))
			return permissionMap.get(action);
		throw new IllegalArgumentException("Action " + action.toString() + " does not exist.");
	}

	/**
	 * 
	 * @param user
	 * @param entityPath
	 * @return the action that a user can do on a specif ic object
	 */
	public static List<Actions> getUserActionList(TxmUser user, String entityPath) {
		if(user.isAdmin()) // admin bypass all tests
			return setAllActions(Corpora.get(entityPath));

		HashSet<Actions> actionList = new HashSet<Actions>();
		HashSet<Actions> antipermList = new HashSet<Actions>();
		//System.out.println("get actions");
		for (Permission p : user.getPermissions(entityPath)) {
			//System.out.println("perm: "+p);
			TxmPermission txmp = (TxmPermission) p;
			if (txmp instanceof TxmAllPermission) {
				//System.out.println(" txm all perm");
				if (txmp.isAnti()) {
					//System.out.println("  negative");
					antipermList.addAll(setAllActions(Corpora.get(entityPath)));
				} else {
					//System.out.println("  positive");
					actionList.addAll(setAllActions(Corpora.get(entityPath)));
				}
			} else if (p instanceof TxmPermission) {
				//System.out.println(" txm perm");
				//TxmPermission txmp = (TxmPermission) p;
				if (txmp.isAnti()) {
					// add all perms
					//actionList.addAll(permissionMap.keySet());

					//add the anti perm to antiperm list
					//System.out.println("  negative");
					antipermList.add(txmp.getAction());
				} else {
					//System.out.println("  positive");
					if((p instanceof SpecificitiesPermission ||
							p instanceof AFCPermission ||
							p instanceof ClassificationPermission) ) {
						if("false".equals(Toolbox.getParam(Toolbox.R_DISABLE)))
							actionList.add(txmp.getAction());
					} else {
						actionList.add(txmp.getAction());
					}
				}
			}
		}
		Log.info("Action list: "+actionList);
		actionList.removeAll(antipermList);
		Log.info("Action list without negativ perm: "+actionList);
		return new ArrayList<Actions>(actionList);
	}

	private static ArrayList<Actions> setAllActions(Object entity) {
		ArrayList<Actions> actionList = new ArrayList<Actions>();
		actionList.clear();
		boolean renable = "false".equals(Toolbox.getParam(Toolbox.R_DISABLE));
		
		if (entity instanceof Subcorpus) {
			//actionList.add(Actions.TEXTSELECTION); // removed for now
			actionList.add(Actions.CREATESUBCORPUS);
			actionList.add(Actions.CREATEPARTITION);
			actionList.add(Actions.PROPERTIES);
			actionList.add(Actions.REFERENCER);
			actionList.add(Actions.INDEX);
			if (renable)
				actionList.add(Actions.SPECIFICITIES);
			actionList.add(Actions.LEXICON);
			actionList.add(Actions.CONCORDANCE);
			if (renable)
				actionList.add(Actions.COOCCURRENCE);
			actionList.add(Actions.DELETE);
			//actionList.add(Actions.EDITION);
		} else if (entity instanceof MainCorpus) {
			actionList.add(Actions.DOWNLOAD);
			actionList.add(Actions.TEXTSELECTION);
			actionList.add(Actions.TEXTS);
			actionList.add(Actions.CREATESUBCORPUS);
			actionList.add(Actions.CREATEPARTITION);
			actionList.add(Actions.DOCUMENTATION);
			actionList.add(Actions.PROPERTIES);
			actionList.add(Actions.REFERENCER);
			actionList.add(Actions.INDEX);
			actionList.add(Actions.LEXICON);
			actionList.add(Actions.CONCORDANCE);
			if (renable)
				actionList.add(Actions.COOCCURRENCE);
			actionList.add(Actions.TSQUERY);
			actionList.add(Actions.EDITION);
			actionList.add(Actions.BIBLIO);
		} else if (entity instanceof Partition) {
			actionList.add(Actions.PROPERTIES);
			actionList.add(Actions.INDEX);
			actionList.add(Actions.LEXICON);
			if (renable) {
				actionList.add(Actions.SPECIFICITIES);
				actionList.add(Actions.AFC);
				actionList.add(Actions.CLASSIFICATION);
			}
			actionList.add(Actions.DELETE);
		}
		return actionList;
	}

	public static void removeNonConsistentPermissions(
			HashSet<TxmPermission> newPermissions, Object entity) {
		
		boolean renable = "false".equals(Toolbox.getParam(Toolbox.R_DISABLE));
		
		TxmPermission perm;
		if (entity instanceof Subcorpus) {
			Iterator<TxmPermission> itr = newPermissions.iterator();
			while(itr.hasNext()){
				perm = itr.next();
				if (perm instanceof BiblioPermission || 
						perm instanceof TsQueryPermission ||
						perm instanceof TextSelectionPermission || 
						perm instanceof TextsPermission ||
						perm instanceof DocumentationPermission ||
						perm instanceof DownloadPermission)
					itr.remove();
			}
		} else if (entity instanceof MainCorpus) {
			Iterator<TxmPermission> itr = newPermissions.iterator();
			while(itr.hasNext()){
				perm = itr.next();
				if (perm instanceof SpecificitiesPermission || 
						perm instanceof AFCPermission ||
						perm instanceof ClassificationPermission)
					itr.remove();
			}
		} else if (entity instanceof Partition) {
			Iterator<TxmPermission> itr = newPermissions.iterator();
			while(itr.hasNext()){
				perm = itr.next();
				if (perm instanceof BiblioPermission || 
						perm instanceof TsQueryPermission ||
						perm instanceof ConcordancePermission ||
						perm instanceof CooccurrencePermission ||
						perm instanceof TextSelectionPermission || 
						perm instanceof EditionPermission ||
						perm instanceof CreatePartitionPermission ||
						perm instanceof CreateSubcorpusPermission ||
						perm instanceof ReferencerPermission ||
						perm instanceof TsQueryPermission || 
						perm instanceof TextsPermission ||
						perm instanceof DocumentationPermission ||
						perm instanceof DownloadPermission || 
						(perm instanceof CooccurrencePermission && !renable) ||
						(perm instanceof SpecificitiesPermission && !renable) ||
						(perm instanceof SpecificitiesPermission && !renable))
					itr.remove();
			}
		}
	}
}
