package org.txm.web.aas.authorization.permissions;

import org.txm.web.aas.authorization.TxmPermission;
import org.txm.web.shared.Actions;

/**
 * The permission to compute a tsquery.
 * @author mdecorde
 *
 */
public class TsQueryPermission extends TxmPermission {

	private static final long serialVersionUID = -6286561198924409285L;


	public TsQueryPermission(String name) {
		super(name);
	}

	public TsQueryPermission() {
		super("tsquery");
	}

	@Override
	public Actions getAction() {
		return Actions.TSQUERY;
	}
}
