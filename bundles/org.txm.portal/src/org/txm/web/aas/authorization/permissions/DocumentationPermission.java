package org.txm.web.aas.authorization.permissions;

import org.txm.web.aas.authorization.TxmPermission;
import org.txm.web.shared.Actions;

/**
 * The permission to see the documentation of a corpus
 * @author mdecorde
 *
 */
public class DocumentationPermission extends TxmPermission {
	
	private static final long serialVersionUID = -7724046703005820858L;

	public DocumentationPermission(String name) {
		super(name);
	}
	
	public DocumentationPermission() {
		super("documentation");
	}

	@Override
	public Actions getAction() {
		return Actions.DOCUMENTATION	;
	}
}
