package org.txm.web.aas.authorization.permissions;

import org.txm.web.aas.authorization.TxmPermission;
import org.txm.web.shared.Actions;

/**
 * the permission which allow to create a partition
 * No parameters
 * @author vchabanis
 *
 */
public class CreatePartitionPermission extends TxmPermission {
	
	private static final long serialVersionUID = 983885772312011769L;

	public CreatePartitionPermission(String name) {
		super(name);
	}
	
	public CreatePartitionPermission() {
		super("createPartition");
	}

	@Override
	public Actions getAction() {
		return Actions.CREATEPARTITION;
	}
}
