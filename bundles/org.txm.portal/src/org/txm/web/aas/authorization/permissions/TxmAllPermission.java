package org.txm.web.aas.authorization.permissions;

import java.security.Permission;

import org.txm.web.aas.authorization.TxmPermission;
import org.txm.web.shared.Actions;

public class TxmAllPermission extends TxmPermission{

	public TxmAllPermission() {
		super("allpermission");
	}

	@Override
	public Actions getAction() {
		return Actions.ALL;
	}

	public boolean implies(Permission p)
	{
		return !anti;
	}
}
