package org.txm.web.aas.authorization.permissions;

import org.txm.web.aas.authorization.TxmPermission;
import org.txm.web.shared.Actions;

/**
 * This Permission defines if an object must be shown in the SideNavTree
 * No parameters
 * @author vchabanis
 *
 */
public class DisplayPermission extends TxmPermission {

	private static final long serialVersionUID = 7870439564567192089L;

	public DisplayPermission(String name) {
		super(name);
	}
	
	public DisplayPermission() {
		super("display");
	}

	@Override
	public Actions getAction() {
		return Actions.DISPLAY;
	}
}
