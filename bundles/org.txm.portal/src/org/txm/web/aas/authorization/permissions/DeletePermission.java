package org.txm.web.aas.authorization.permissions;

import org.txm.web.aas.authorization.TxmPermission;
import org.txm.web.shared.Actions;

/**
 * the permissio to delete objects
 * No parameters
 * @author vchabanis
 *
 */
public class DeletePermission extends TxmPermission {

	private static final long serialVersionUID = 2241360652784822890L;

	public DeletePermission(String name) {
		super(name);
	}
	
	public DeletePermission() {
		super("delete");
	}

	@Override
	public Actions getAction() {
		return Actions.DELETE;
	}
}
