package org.txm.web.aas.authorization.permissions;

import org.txm.web.aas.authorization.TxmPermission;
import org.txm.web.shared.Actions;

/**
 * This Permission defines if the edition of an object can be displayed
 * No parameters
 * @author mdecorde
 *
 */
public class TextsPermission extends TxmPermission {

	private static final long serialVersionUID = -2382092575495828808L;

	public TextsPermission(String name) {
		super(name);
	}
	
	public TextsPermission() {
		super("texts");
	}

	@Override
	public Actions getAction() {
		return Actions.TEXTS;
	}
}
