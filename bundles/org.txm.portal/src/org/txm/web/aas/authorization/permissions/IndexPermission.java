package org.txm.web.aas.authorization.permissions;

import org.txm.web.aas.authorization.TxmPermission;
import org.txm.web.shared.Actions;

/**
 * The Permission to compute an index
 * No parameters
 * @author vchabanis
 *
 */
public class IndexPermission extends TxmPermission {

	private static final long serialVersionUID = 7819221842532520285L;

	public IndexPermission(String name) {
		super(name);
	}
	
	public IndexPermission() {
		super("index");
	}

	@Override
	public Actions getAction() {
		return Actions.INDEX;
	}
}
