package org.txm.web.aas;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import org.txm.utils.logger.Log;
import org.txm.web.server.TxmUser;

/**
 * The CRUD (Create Read Update Delete) is the class containing the four 
 * basic functions of persistent storage.
 * 
 * 
 * @see TxmUser
 * @author vchabani
 * @version 0.2.0
 */
public class CRUD {
	public static String rootDir = "";//TxmConfig.getProperty("userBase");
	private static String userSufix = ".usr";
	// private static String groupSufix = ".grp";

	public enum Type {USER, GROUP}

	public synchronized static void configure() {
		//TODO a config loader
	}

	/**
	 * Store an instance of a TxmUser
	 * 
	 * @param item The object to store, must be an instance of TxmUser or TxmGroup
	 * @return true if the item was correctly written, false otherwise
	 */
	public synchronized static boolean create(Object item) {
		String itemName = null;
		if (item instanceof TxmUser) {
			itemName = ((TxmUser)item).getLogin() + userSufix;
		//		else if (item instanceof TxmGroup)
		//			itemPath = rootDir + ((TxmGroup)item).getName() + groupSufix;
		} else {
			return false;
		}
		if (new File(rootDir, itemName).exists())
			return false;
		return writeObject(item, rootDir, itemName);
	}

	/**
	 * Load the specif ied item
	 * 
	 * @param itemName The name of the item
	 * @param type The type of the item
	 * @return true if the item was correctly loaded, false otherwise
	 * @see CRUD.Type
	 */
	public synchronized static Object read(String itemName, Type type) {
		//TODO actually can read only users
		String itemPath = null;
		switch (type) {
		case USER:
			itemPath = itemName + userSufix;
			break;
		case GROUP:
			return null;
		default:
			return null;
		}
		return readObject(rootDir, itemPath);
	}

	/**
	 * Update the specified item.
	 * 
	 * @param itemName The object to update, must be an instance of TxmUser or TxmGroup
	 * @return true if the item was correctly updated, false otherwise
	 */
	public synchronized static boolean update(Object item) {
		String itemName = null;
		if (item instanceof TxmUser)
			itemName = ((TxmUser)item).getLogin() + userSufix;
		//		else if (item instanceof TxmGroup)
		//			itemPath = rootDir + ((TxmGroup)item).getName() + groupSufix;
		else
			return false;
		//if (!new File(itemName).exists())
			//return false;
		return writeObject(item, rootDir, itemName);
	}
	
	/**
	 * Remove the specif ied item
	 * 
	 * @param itemName The object to delete, must be an instance of TxmUser or TxmGroup
	 * @return true if the item was correctly deleted, false otherwise
	 */
	public synchronized static boolean delete(Object item) {
		String itemPath = null;
		if (item instanceof TxmUser)
			itemPath = ((TxmUser)item).getLogin() + userSufix;
		//		else if (item instanceof TxmGroup)
		//			itemPath = rootDir + ((TxmGroup)item).getName() + groupSufix;
		else
			return false;
		return (new File(rootDir, itemPath).delete());
	}
	
	/**
	 * Write the object 
	 * 
	 * @param item the Object to write
	 * @param itemPath the file path
	 * @return
	 */
	private static boolean writeObject(Object item, String itemPath, String itemName) {
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		try	{
			fos = new FileOutputStream(new File(itemPath, itemName));
			out = new ObjectOutputStream(fos);
			out.writeObject(item);
			out.close();
			fos.close();
		}
		catch(IOException ex) {
			Log.severe(Log.toString(ex));
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * @param itemPath the file where the Object as been serialized
	 * @return an Object
	 */
	private static Object readObject(String itemPath, String itemName) {
		if (!new File(itemPath, itemName).exists())
			return null;
		
		Object out = null;
		FileInputStream fis = null;
		ObjectInputStream in = null;
		try	{
			fis = new FileInputStream(new File(itemPath, itemName));
			in = new ObjectInputStream(fis);
			out = in.readObject();
			in.close();
		}
		catch(Exception ex) {
			return null;
		}
		return out;
	}
	
	public static ArrayList<String> getUsersLogin()
	{
		ArrayList<String> logins = new ArrayList<String>();
		File[] files = new File(rootDir).listFiles(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				
				return name.endsWith(".usr");
			}
		});
		for(File f : files)
			logins.add(f.getName().substring(0, f.getName().indexOf(".")));
		return logins;
	}
}
