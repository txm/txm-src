package org.txm.web.server;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.txm.web.server.statics.TxmConfig;

/**
 * Static class to send mail 
 * 
 * @author mdecorde
 */
public final class Emailer {

	public static boolean sendMail(String to, String subject, String content){
		if("true".equals(TxmConfig.getProperty("nomail")))
			return true;
		
		String protocol = TxmConfig.getProperty("mail.protocol", "SMTP");
		String host = TxmConfig.getProperty("mail.host", "127.0.0.1");
		String user = TxmConfig.getProperty("mail.user");
		String password = TxmConfig.getProperty("mail.password", "");
		String from = TxmConfig.getProperty("mail.default.from", "no_mail_set@none.fr");
		String reply = TxmConfig.getProperty("mail.default.reply", "no_repl_set@none.fr");
		
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", protocol);
		props.setProperty("mail.host", host);
		props.setProperty("mail.user", user);
		props.setProperty("mail.password", password);
		props.setProperty("mail.from", from);
		props.setProperty("mail.mime.charset", "UTF-8");

		try {
			Session mailSession = Session.getDefaultInstance(props, null);
			Transport transport = mailSession.getTransport();

			MimeMessage message = new MimeMessage(mailSession);
			message.setSubject(subject);
			//message.setContent(content, "text/plain");
			message.setText(content, "UTF-8");
			message.setFrom(new InternetAddress(from));
			
			Address[] addresses = { new InternetAddress(reply) };
			message.setReplyTo(addresses);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			
			transport.connect();
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
} 

