package org.txm.web.server.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.txm.Toolbox;
import org.txm.functions.selection.SelectionResult;
import org.txm.objects.Project;
import org.txm.objects.Text;
import org.txm.objects.TxmObject;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.corpus.query.Query;
import org.txm.utils.logger.Log;
import org.txm.web.aas.authorization.permissions.AFCPermission;
import org.txm.web.aas.authorization.permissions.DeletePermission;
import org.txm.web.aas.authorization.permissions.SpecificitiesPermission;
import org.txm.web.aas.authorization.permissions.TxmAllPermission;
import org.txm.web.client.services.CorpusActionService;
import org.txm.web.client.services.PortalService;
import org.txm.web.server.TxmUser;
import org.txm.web.server.statics.ActivityLog;
import org.txm.web.server.statics.Corpora;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.shared.Actions;
import org.txm.web.shared.ConcordanceParam;
import org.txm.web.shared.SelectionInfos;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The corpus service implementation. 
 * Helps to get informations about a corpus, such as StructuralUnits, properties...
 * @author vchabanis
 *
 */
public class CorpusActionServiceImpl extends PortalService implements CorpusActionService {

	private static final long serialVersionUID = 4623869090068043334L;

	@Override
	/**
	 * Service:
	 * @param itempath the CQP object
	 * @param structName the structural unit
	 * @param propName the name of the structural unit property
	 * @return the values of a structuralUnit property
	 */
	public List<String> getStructPropsValues(String itemPath, String structName, String propName) {
		Log.info("[CorpusActionServiceImpl.getStructPropsValues] start. itemPath="+itemPath+", structName="+structName+", propName="+propName);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getStructPropsValues", itemPath);
			Log.info("[CorpusActionServiceImpl.getStructPropsValues] end. ret=empty list");
			return new ArrayList<String>(); 
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getStructPropsValues", itemPath);

		List<String> ret = Corpora.getStructuralUnitPropertyValues(itemPath, structName, propName);
		Log.info("[CorpusActionServiceImpl.getStructPropsValues] end. ret="+ret);
		return ret;
	}

	@Override
	/**
	 * @return the structuralunits names of a CQP object.
	 */
	public List<String> getStructuralUnits(String itemPath) {
		Log.info("[CorpusActionServiceImpl.getStructuralUnits] start. itemPath="+itemPath);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if(user == null)
		{
			Log.severe("Session "+getSessionId()+" has expired");
			Log.info("[CorpusActionServiceImpl.getStructuralUnits] start. ret=empty list");
			return new ArrayList<String>(); 
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getStructuralUnits", itemPath);

		List<String> ret = Corpora.getStructuralUnits(itemPath);
		Log.info("[CorpusActionServiceImpl.getStructuralUnits] start. ret="+ret);
		return ret;
	}

	@Override
	/**
	 * @return the property names of a structural unit of a CWP object
	 */
	public List<String> getStructuralUnitProperties(String path, String structName) {
		Log.info("[CorpusActionServiceImpl.getStructuralUnitProperties] start. itemPath="+path+", structName="+structName);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if(user == null)
		{
			Log.severe("Session "+getSessionId()+" has expired");
			Log.info("[CorpusActionServiceImpl.getStructuralUnitProperties] end ret=empty list");
			return new ArrayList<String>(); 
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getWordProperties", path, structName);

		List<String> ret = Corpora.getStructuralUnitsProperties(path, structName);
		Log.info("[CorpusActionServiceImpl.getStructuralUnitProperties] end ret="+ret);
		return ret;
	}

	@Override
	/**
	 * @return the references that can be used with a CQP Object.
	 * @Example : text:id, the id of the text structure
	 */
	public ArrayList<String> getReferences(String path) {
		Log.info("[CorpusActionServiceImpl.getReferencess] start. itemPath="+path);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if(user == null)
		{
			Log.severe("Session "+getSessionId()+" has expired");
			Log.info("[CorpusActionServiceImpl.getReferencess] end. ret=empty list");
			return new ArrayList<String>();
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getReferences", path);

		ArrayList<String> ret = new ArrayList<String>(Arrays.asList(Corpora.getReferences(path)));
		Log.info("[CorpusActionServiceImpl.getReferencess] end. ret="+ret);
		return ret;
	}

	@Override
	/**
	 * @return the lexical properties of a CQP Object
	 */
	public ArrayList<String> getWordProperties(String path) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		Log.info("[CorpusActionServiceImpl.getWordProperties] start. itemPath="+path);
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return new ArrayList<String>();
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getWordProperties", path);

		ArrayList<String> ret = new ArrayList<String>(Arrays.asList(Corpora.getWordProperties(path)));
		Collections.sort(ret);
		int idx = ret.indexOf("word");
		if (idx >= 0) {
			String tmp = ret.get(idx);
			ret.set(idx, ret.get(0));
			ret.set(0, tmp);
		}
		Log.info("[CorpusActionServiceImpl.getWordProperties] end="+ret);
		return ret;
	}

	@Override
	/**
	 * creates a sub-corpus given a list of values of a structural unit property.
	 * @return true if success
	 */
	public String createSubcorpus(String path, String name, String structName, String structProperty, List<String> values) {
		Log.info("[CorpusActionServiceImpl.createSubcorpus] start. path="+path+", structName="+structName+", structProperty="+structProperty+", values="+values);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return "User session has expired"; 
		}
		Corpus corpus = (Corpus)user.askEntity(path, Actions.CREATESUBCORPUS);
		name = user.getLogin()+":"+name; // append namespace

		if (Corpora.containsKey(path + "/" + name)) {
			Log.info("[CorpusActionServiceImpl.createSubcorpus] User : " + user.getLogin() + " -> Fail, subcorpus already exists");
			return "exists";
		}
		if (corpus == null) {
			Log.info("[CorpusActionServiceImpl.createSubcorpus] User : " + user.getLogin() + " -> Fail, invalid path");
			return "Internal error: the corpus does not exists";
		}

		String childPath = path + "/" + name; // the subcorpus path

		try {
			// get the structural unit and property
			StructuralUnit struct = corpus.getStructuralUnit(structName);
			StructuralUnitProperty prop = struct.getProperty(structProperty);

			Subcorpus newSubcorpus = corpus.createSubcorpus(struct, prop, values, name);

			Corpora.put(childPath, newSubcorpus); // register the subcorpus in the Corpora
			newSubcorpus.registerToParent(); // TODO: should be done in the TBX

			// add permission for the new subcorpus
			user.inheritPermissions(childPath, newSubcorpus);
			user.addPermission(childPath, new DeletePermission());
			//if ("false".equals(Toolbox.getParam(Toolbox.R_DISABLE)))
			user.addPermission(childPath, new SpecificitiesPermission());
			if (!user.save()) {
				return "user not saved";
			}

			if (!Toolbox.workspace.save()) {
				return "workspace not saved";
			}
			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "createSubcorpus", childPath, prop.getQualifiedName());
			Log.info("[CorpusActionServiceImpl.createSubcorpus] User : " + user.getLogin() + " -> Success");
			return name;
		} catch (Exception e) {
			Log.info("[CorpusActionServiceImpl.createSubcorpus] User : " + user.getLogin() + " -> Fail, " + e.getMessage());
			ActivityLog.warning(getSessionId(), getRemoteAddr(),user.getLogin(), "createSubcorpus", childPath);
			return "Internal error: the corpus could not been created";
		}
	}

	@Override
	/**
	 * creates a partition given a structural unit property. 
	 * @return true if success
	 */
	public String createPartition(String path, String name, String structName, String structProperty) {
		Log.info("[CorpusActionServiceImpl.createPartition] start. path="+path+", structName="+structName+", structProperty="+structProperty);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return "User session has expired"; 
		}
		Corpus corpus = (Corpus)user.askEntity(path, Actions.CREATESUBCORPUS);
		name = user.getLogin()+":"+name;
		Partition newPartition = null;

		if (Corpora.containsKey(path + "/" + name)) {
			Log.info("[CorpusActionServiceImpl.createPartition] User : " + user.getLogin() + " -> Fail, partition already exists");
			return "This partition already exists";
		}

		if (corpus == null) {
			Log.info("[CorpusActionServiceImpl.createPartition] User : " + user.getLogin() + " -> Fail, invalid path");
			return "Internal error: the corpus does not exists";
		}

		String childPath = path + "/" + name; // partition path
		try {
			StructuralUnit struct = corpus.getStructuralUnit(structName);
			StructuralUnitProperty prop = struct.getProperty(structProperty);

			newPartition = corpus.createPartition(struct, prop);//(struct, prop, values, name);

			Corpora.put(childPath, newPartition); // register the partition in the corpora
			//newPartition.registerToParent();
			newPartition.setName(name);

			//add User new perms
			user.inheritPermissions(childPath, newPartition);
			user.addPermission(childPath, new DeletePermission());
			//if ("false".equals(Toolbox.getParam(Toolbox.R_DISABLE))) {
			user.addPermission(childPath, new SpecificitiesPermission());
			user.addPermission(childPath, new AFCPermission());
			//}
			//HashSet<TxmPermission> perms = user.getPermissions(childPath);
			if (!user.save()) {
				return "user not saved";
			}

			Toolbox.workspace.save(); // important
			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "createPartition", childPath, prop.getQualifiedName());
			Log.info("[CorpusActionServiceImpl.createPartition] User : " + user.getLogin() + " -> Success");
			return name;
		} catch (Exception e) {
			Log.info("[CorpusActionServiceImpl.createPartition] User : " + user.getLogin() + " -> Fail, " + e.getMessage());
			ActivityLog.warning(getSessionId(), getRemoteAddr(),user.getLogin(), "createPartition", childPath);
			return "Internal Error: could not create the corpus";
		}
	}

	@Override
	/**
	 * creates a partition given a name and queries
	 * @return true if success
	 */
	public String createPartition(String path, String name, List<String> queries) {
		Log.info("[CorpusActionServiceImpl.createPartition] start. path="+path+", name="+name+", queries="+queries);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return "User session has expired"; 
		}
		Corpus corpus = (Corpus)user.askEntity(path, Actions.CREATEPARTITION);

		if (Corpora.containsKey(path + "/" + name)) {
			Log.info("[createPartition] User : " + user.getLogin() + " -> Fail, subcorpus already exists");
			return "This partition already exists";
		}

		if (corpus == null) {
			Log.info("[createPartition] User : " + user.getLogin() + " -> Fail, invalid path");
			return "Internal error: the corpus does not exist";
		}

		String childPath = path + "/" + name; // partition path

		try {
			Partition newPartition = corpus.createPartition(name, queries);
			Corpora.put(childPath, newPartition); // register the partition in the corpora
			// newPartition.registerToParent();

			user.inheritPermissions(childPath, newPartition);
			user.addPermission(childPath, new TxmAllPermission());
			//if ("false".equals(Toolbox.getParam(Toolbox.R_DISABLE))) {
			user.addPermission(childPath, new SpecificitiesPermission());
			user.addPermission(childPath, new AFCPermission());
			//}
			if (!user.save()) {
				return "user not saved";
			}

			Toolbox.workspace.save(); // important
			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "createPartition", childPath, queries);
			Log.info("[createPartition] User : " + user.getLogin() + " -> Success");
			return "";
		} catch (CqiClientException e) {
			Log.info("[createPartition] User : " + user.getLogin() + " -> Fail, " + e.getMessage());
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "createPartition", childPath);
			return "Internal error: could not create the subcorpus";
		}
	}

	@Override
	/**
	 * Delete an object of Corpora
	 * @return true if success
	 */
	public boolean delete(String path) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return false; 
		}
		if (!user.can(path, new DeletePermission())) {
			Log.severe("no permission");
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "delete", path);
			return false; 
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "delete", path);
		Log.info("[CorpusActionServiceImpl.delete] start. path="+path);
		Object object = Corpora.get(path);
		if (object instanceof Subcorpus)
			return deleteSubcorpus(path);
		else if (object instanceof Partition)
			return deletePartition(path);
		else if (object instanceof MainCorpus)
			return deleteCorpus(path);
		else {
			ActivityLog.warning(getSessionId(), getRemoteAddr(),user.getLogin(), "delete", path);
			return false;
		}
	}

	/**
	 * Deletes a partition:
	 * 	- drops the partition in CWB
	 *  - removes the partition from Corpora
	 * @param path the partition's path
	 * @return true if success
	 */
	private boolean deletePartition(String path) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return false; 
		}

		try {
			Partition partition = (Partition)user.askEntity(path, Actions.DELETE);

			if ((partition == null) || (!Corpora.containsKey(path))) {
				Log.info("[deletePartition] User : " + user.getLogin() + " -> Fail, " + path + " does not exists");
				return false;
			}

			if (!partition.delete()) {
				Log.severe("[deletePartition] User : " + user.getLogin() + " -> fail");
				return false;
			}
			Toolbox.workspace.save();
			Corpora.remove(path);
			user.removeEntityFromUser(path);
			if (!user.save()) {
				Log.severe("Error: user not saved");
				return false;
			}
		} catch (Exception e) {
			Log.severe("[deletePartition] User : " + user.getLogin() + " -> Fail, " + e.getMessage());
			return false;
		}
		Corpora.remove(path);
		Log.info("[deletePartition] User : " + user.getLogin() + " -> Success");
		return true;
	}

	/**
	 * Deletes a sub-corpus:
	 * 	- drops the subcorpus in CWB
	 *  - removes the subcorpus from Corpora
	 * @param path
	 * @return
	 */
	private boolean deleteSubcorpus(String path) {

		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return false; 
		}
		try {
			Subcorpus subcorpus = (Subcorpus)user.askEntity(path, Actions.DELETE);
			if (subcorpus == null) {
				Log.severe("[deleteSubcorpus] User : " + user.getLogin() + " -> fail. No such element in corpora: "+path);
				return false;
			}
			boolean ret = true;
			if (!subcorpus.delete()) {
				Log.severe("[deleteSubcorpus] User : " + user.getLogin() + " -> fail");
				return false;
			}

			Toolbox.workspace.save();
			Corpora.remove(path);
			user.removeEntityFromUser(path);
			if (!user.save()) {
				Log.severe("Error: user not saved");
				return false;
			}

			Log.info("[deleteSubcorpus] User : " + user.getLogin() + " -> Success");
			return ret;
		} catch (Exception e){
			Log.severe("[deleteSubcorpus] User : " + user.getLogin() + " -> Fail, " + e.getMessage());
			return false;
		}
	}

	private boolean deleteCorpus(String path) {

		return false;
	}

	@Override
	public HashMap<String, List<String>> getStructuralUnitsProperties(
			String itemPath, String structName) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null)
		{
			Log.severe("Session "+getSessionId()+" has expired");
			return new HashMap<String, List<String>>(); 
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getStructuralUnitsProperties", itemPath);
		return Corpora.getStructuralUnitPropertyValues(itemPath, structName);
	}

	@Override
	public List<String> getBases() {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return new ArrayList<String>(); 
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getBases");
		return _getBases();
	}

	public static List<String> _getBases() {
		Project p = Toolbox.workspace.getProject("default");
		if (p == null) {
			return new ArrayList<String>();
		}
		ArrayList<String> bases = new ArrayList<String>(p.getBaseNames());
		Collections.sort(bases);
		return bases;
	}

	@Override
	public SelectionInfos getSelectionInfos(String path) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return new SelectionInfos(); 
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getSelectionInfos", path);

		TxmObject obj = Corpora.getCorporaMap().get(path);
		if (obj instanceof Subcorpus) {
			Subcorpus sub = (Subcorpus) obj;
			SelectionResult sel = sub.getSelectionResult();
			System.out.println("sel: "+sel);
			if (sel == null || sel.size() == 0) {
				sel = new SelectionResult();
				try {
					Query query = new Query("<text>[]"); //$NON-NLS-1$
					QueryResult result = sub.query(query, "selection", false); //$NON-NLS-1$
					List<Match> matches = result.getMatches();
					//System.out.println("Nb of texts: "+matches.size());
					int[] positions = new int[matches.size()]; // starting positions of texts in the corpus
					for(int i = 0 ; i < matches.size() ; i++)
						positions[i] = matches.get(i).getStart();

					StructuralUnit text_su = sub.getStructuralUnit("text");
					StructuralUnitProperty sup = text_su.getProperty("id"); //$NON-NLS-1$
					int[] idx = CorpusManager.getCorpusManager().getCqiClient().cpos2Struc(sup.getQualifiedName(), positions);
					String[] textsIdArray = CorpusManager.getCorpusManager().getCqiClient().struc2Str(sup.getQualifiedName(), idx);
					sel.addAll(Arrays.asList(textsIdArray));
				}
				catch(Exception e){e.printStackTrace();return null;}
			}

			SelectionInfos infos = new SelectionInfos();
			infos.texts = new ArrayList<String>(sel);
			infos.critera = sel.critera;

			return infos;
		}
		return null;
	}

	/**
	 * returns the parent's path of a corpus ; null is the path does not belong to a Corpus 
	 */
	@Override
	public String getParentPath(String path) {
		TxmObject obj = Corpora.get(path);
		if(obj instanceof Corpus)
			return Corpora.getParentPath(path);
		return null;
	}

	@Override
	public String createSubcorpus(String path, String name, SelectionInfos infos) {
		Log.info("[CorpusActionServiceImpl.createSubcorpus] start. path="+path+", infos="+infos);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return "User session has expired"; 
		}
		Corpus corpus = (Corpus)user.askEntity(path, Actions.CREATESUBCORPUS);
		name = user.getLogin()+":"+name; // append namespace
		Subcorpus newSubcorpus = null;

		if (Corpora.containsKey(path + "/" + name)) {
			Log.info("[CorpusActionServiceImpl.createSubcorpus] User : " + user.getLogin() + " -> Fail, subcorpus already exists");
			return "exists";
		}
		if (corpus == null) {
			Log.info("[CorpusActionServiceImpl.createSubcorpus] User : " + user.getLogin() + " -> Fail, invalid path");
			return "Internal error: the corpus does not exists";
		}
		try {
			SelectionResult rez = new SelectionResult();
			rez.addAll(infos.texts);
			rez.critera = infos.critera;

			newSubcorpus = corpus.createSubcorpus(name, rez);

			String childPath = path + "/" + name;
			Corpora.put(childPath, newSubcorpus);
			newSubcorpus.registerToParent(); // TODO: should be done in the TBX

			// add permission for the new subcorpus
			user.inheritPermissions(childPath, newSubcorpus);
			//if ("false".equals(Toolbox.getParam(Toolbox.R_DISABLE)))
			user.addPermission(childPath, new SpecificitiesPermission());
			user.addPermission(childPath, new DeletePermission());
			if (!user.save()) {
				Log.severe("Error: user not saved");
				return "user not saved";
			}
			Toolbox.workspace.save();

			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "createSubcorpusFromSelection", childPath, infos.texts.size());
			Log.info("[CorpusActionServiceImpl.createSubcorpus] User : " + user.getLogin() + " -> Success");
			return name;
		} catch (CqiClientException e) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "createSubcorpusFromSelection", e.toString(), infos.texts.size());
			Log.severe("[CorpusActionServiceImpl.createSubcorpus] User : " + user.getLogin() + " from Selection -> Fail, " + e.getMessage());
			return "Internal error: the corpus could not been created";
		}
	}

	@Override
	public List<String> getStructuralUnitsProperties(String itemPath) {
		return Corpora.getStructuralUnitsProperties(itemPath);
	}

	@Override
	public List<String> getViewProperties(String path) {
		Log.info("[CorpusActionServiceImpl.getViewProperties] start: path="+path);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null)
		{
			Log.severe("Session "+getSessionId()+" has expired");
			Log.info("[CorpusActionServiceImpl.getViewProperties] end: ret=empty list (not in session)");
			return new ArrayList<String>(); 
		}
		TxmObject obj = Corpora.getCorporaMap().get(path);
		if (obj instanceof Corpus) {
			Corpus parent = ((Corpus)obj);
			ArrayList<String> props = parent.getViewProperties();
			if (props == null || props.size() == 0) {
				Log.info("[CorpusActionServiceImpl.getViewProperties] end: path properties ret="+props);
				return getWordProperties(path);
			}
			Log.info("[CorpusActionServiceImpl.getViewProperties] end: parents properties ret="+props);
			return props;
		}
		Log.info("[CorpusActionServiceImpl.getViewProperties] end: ret=empty list (not a corpus)");
		return new ArrayList<String>();
	}

	@Override
	public List<String> getSortProperties(String path) {
		Log.info("[CorpusActionServiceImpl.getSortProperties] start: path="+path);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			Log.info("[CorpusActionServiceImpl.getSortProperties] end: ret=empty list (not in session)");
			return new ArrayList<String>(); 
		}
		TxmObject obj = Corpora.getCorporaMap().get(path);
		if (obj instanceof Corpus) {
			Corpus parent = ((Corpus)obj);
			ArrayList<String> props = parent.getSortProperties();
			if(props == null || props.size() == 0) {
				Log.info("[CorpusActionServiceImpl.getSortProperties] end: path properties ret="+props);
				return getWordProperties(path);
			}
			Log.info("[CorpusActionServiceImpl.getSortProperties] end: parents properties ret="+props);
			return props;
		}
		Log.info("[CorpusActionServiceImpl.getSortProperties] end: ret=empty list (not a corpus)");
		return new ArrayList<String>();
	}

	@Override
	public List<String> getRefProperties(String path) {
		Log.info("[CorpusActionServiceImpl.getRefProperties] start: path="+path);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null)
		{
			Log.severe("Session "+getSessionId()+" has expired");
			Log.info("[CorpusActionServiceImpl.getRefProperties] end: ret=empty list (not in session)");
			return new ArrayList<String>(); 
		}
		TxmObject obj = Corpora.getCorporaMap().get(path);
		if(obj instanceof Corpus)
		{
			Corpus parent = ((Corpus)obj);
			ArrayList<String> props = parent.getRefProperties();
			if (props == null || props.size() == 0) {
				Log.info("[CorpusActionServiceImpl.getRefProperties] end: path properties ret="+props);
				return getReferences(path);
			}
			Log.info("[CorpusActionServiceImpl.getRefProperties] end: parents properties ret="+props);
			return props;
		}
		
		Log.info("[CorpusActionServiceImpl.getRefProperties] end: ret=empty list (not a corpus)");
		return new ArrayList<String>();
	}

	@Override
	public boolean reportActivity(String cmd, String parameters) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return false;
		}
		System.out.println("REPORT ACTIVITY: CMD="+cmd+" PARAMS="+parameters);
		ActivityLog.add(getSessionId(), getRemoteAddr(), user.getLogin(), cmd, parameters);
		return true;
	}

	@Override
	public Integer getNumberOfTexts(String path) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return -1;
		}

		TxmObject obj = Corpora.getCorporaMap().get(path);
		if (obj instanceof Corpus) {
			Corpus corpus = ((Corpus)obj);
			return corpus.getTexts().size();
		}

		return 0;
	}

	@Override
	public String getFirstTextID(String path) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return "session";
		}

		TxmObject obj = Corpora.getCorporaMap().get(path);
		if (obj instanceof Corpus) {
			Corpus corpus = ((Corpus)obj);
			for (Text text :  corpus.getTexts()) {
				return text.getName();
			}
		}

		return "no text";
	}

	@Override
	public int getRightContextSize(String path) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return 12;
		}

		TxmObject obj = Corpora.getCorporaMap().get(path);
		if (obj instanceof Corpus) {
			Corpus corpus = ((Corpus)obj);
			return corpus.getDefaultConcordanceRightContextSize();
		}

		return 12;
	}

	@Override
	public int getLeftContextSize(String path) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return 8;
		}

		TxmObject obj = Corpora.getCorporaMap().get(path);
		if (obj instanceof Corpus) {
			Corpus corpus = ((Corpus)obj);
			return corpus.getDefaultConcordanceLeftContextSize();
		}
		return 8;
	}

	@Override
	public ConcordanceParam getConcordanceDefaultParameters(String path) {
		ConcordanceParam params = null;
		Log.info("[CorpusActionServiceImpl.getConcordanceDefaultParameters] start. path="+path);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return params;
		}

		TxmObject obj = Corpora.getCorporaMap().get(path);
		if (obj instanceof Corpus) {
			Corpus corpus = ((Corpus)obj);
			params = new ConcordanceParam();
			params.setLeftContextSize(corpus.getDefaultConcordanceLeftContextSize());
			params.setRightContextSize(corpus.getDefaultConcordanceRightContextSize());
			ArrayList<String> sortProperties = corpus.getSortProperties();
			if (sortProperties.size() == 0) sortProperties = getWordProperties(path); // if no sort properties set
			params.setKeywordSortProperties(sortProperties);
			ArrayList<String> viewProperties = corpus.getViewProperties();
			if (viewProperties.size() == 0) viewProperties = getWordProperties(path); // if no view properties set
			params.setKeywordViewProperties(viewProperties);
			ArrayList<String> refProperties = corpus.getRefProperties();
			if (refProperties.size() == 0) refProperties = getReferences(path); // if no ref properties set
			params.setSortReferences(refProperties);
			params.setViewReferences(refProperties);
		}
		Log.info("[CorpusActionServiceImpl.getConcordanceDefaultParameters] end. params="+params);
		return params;
	}
}