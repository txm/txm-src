package org.txm.web.server.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.txm.Toolbox;
import org.txm.functions.referencer.Referencer;
import org.txm.functions.referencer.Referencer.Line;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.query.Query;
import org.txm.utils.logger.Log;
import org.txm.web.client.services.PortalService;
import org.txm.web.client.services.ReferencerService;
import org.txm.web.exceptions.PermissionException;
import org.txm.web.server.TxmUser;
import org.txm.web.server.statics.ActivityLog;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.shared.Actions;
import org.txm.web.shared.ReferencerParam;
import org.txm.web.shared.ReferencerResult;

public class ReferencerServiceImpl extends PortalService implements ReferencerService{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4442692097966842650L;
	private static HashMap<String, Referencer> results = new HashMap<String, Referencer>();
	
	public static int cleanResults(String id) {
		int n = 0;
		HashSet<String> keys = new HashSet<String>();
		keys.addAll(results.keySet());
		for (String k : keys) {
			if (k.startsWith(id)) {
				results.remove(k);
				n++;
			}
		}
		return n;
	}

	@Override
	public ReferencerResult references(ReferencerParam params)
			throws PermissionException {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		ReferencerResult result = new ReferencerResult();
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "references", params.getCorpusPath(), params.getQuery());
			result.error = "session";
			return result; 
		}

		Corpus corpus = (Corpus)user.askEntity(params.getCorpusPath(), Actions.REFERENCER);
		if (corpus == null) {
			Log.severe("no permission");
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "references", params.getCorpusPath(), params.getQuery());
			result.error = "permission";
			return result; 
		}
		
		Query query = new Query(Query.fixQuery(params.getQuery()));
		ArrayList<Property> props = new ArrayList<Property>();
		Property property;
		try {
			property = corpus.getProperty(params.prop.trim());
			for (String prop : params.getProperties()) {
				int idx = prop.indexOf(":");
				if (idx > 0) {
					String su = prop.substring(0, idx);
					String name = prop.substring(idx+1);
					props.add(corpus.getStructuralUnit(su).getProperty(name));
				}
			}
		} catch (CqiClientException e) {
			Log.info("[vocabulary] User : " + user.getLogin() + " -> Fail, " + e.getMessage());
			e.printStackTrace();
			result.error = Toolbox.getCqiClient().getLastError();
			return result;
		}
		
		Referencer ref = results.get(params.toKey());
		if(ref == null)
		{
			ref = new Referencer(corpus, query, property, props, true);
			results.put(params.toKey(), ref);
		}

		List<Line> lines;
		try {
			if(!ref.compute())
			{
				ActivityLog.warning(getSessionId(), getRemoteAddr(),user.getLogin(), "references", params.getCorpusPath(), params.getQuery());
				return result;
			}
			
			lines = ref.getLines(params.startindex, params.startindex + params.filterNbPerPage);
			for(Line l : lines)
			{
				result.refs.put(l.getPropValue(), new ArrayList<String>(l.getReferences()));
				result.counts.put(l.getPropValue(), l.getCounts());
			}	
			result.total = ref.getNLines();
			
		} catch (Exception e1) {
			ActivityLog.warning(getSessionId(), getRemoteAddr(),user.getLogin(), "references", params.getCorpusPath(), params.getQuery());
			e1.printStackTrace();
			result.error = Toolbox.getCqiClient().getLastError();
			if(result.error.startsWith("No error"));
				result.error = e1.getMessage();
			return result;
		} 
		
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "references", params.getCorpusPath(), params.getQuery());
		Log.info("[Referencer] User : " + user.getLogin() + " -> Success");
		return result;
	}
	
	@Override
	public String export(ReferencerParam params) {
		try {	
			params.startindex = 0;
			params.filterNbPerPage = Integer.MAX_VALUE;
			ReferencerResult result = this.references(params); // do the permissions check
			if (result == null) {
				Log.warning("[VocabularyServiceImpl.export]: failed to get index result");
				return "error: failed to get index result";
			}
			
			StringBuffer buffer = new StringBuffer(result.counts.size()*2);
			buffer.append(params.prop+";"+params.getProperties()); //header
			
			for (String key : result.refs.keySet()) {
				buffer.append("\n"+key+"\t"); // prop values
				for (int i  = 0 ; i < result.refs.get(key).size() && i < result.counts.get(key).size() ; i++) // freqs
				{
					String ref = result.refs.get(key).get(i);
					String col = ref+"("+result.counts.get(key).get(ref)+") ";
					//buffer.append(col.replace("\"", "\"\""));	
				}
				buffer.append("\"");
			}
			
			System.out.println("EXPORT:"+buffer.toString());
			String ID = ""+new java.util.Random().nextInt(999999999);
			File appdir = new File(this.getServletContext().getRealPath("."));
			File csvFile = new File(appdir, "csv/"+ID+".txt");
			try {
				Writer writer = new OutputStreamWriter(new FileOutputStream(csvFile), "UTF-8");
				writer.write(buffer.toString());
				writer.close();
				return "csv/"+ID+".txt";
			} catch (IOException e) { e.printStackTrace(); return "error: "+e;}
		} catch (PermissionException e) {
			Log.warning("[VocabularyServiceImpl.export]: failed to build String version of the result");
			e.printStackTrace();
			return "error: "+e;
		}
	}
	
	public static void freeCaches(String corpusname) {
		HashSet<String> keys = new HashSet<String>();
		keys.addAll(results.keySet());
		for(String key : keys) {
			Corpus corpus = results.get(key).getCorpus();
			if(corpus != null && corpus.getName().equals(corpusname))
				results.remove(key);
		}
	}
}
