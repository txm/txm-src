package org.txm.web.server.services;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.TransformerFactory;

import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.ts.TSCorpus;
import org.txm.searchengine.ts.TSCorpusManager;
import org.txm.searchengine.ts.TSMatch;
import org.txm.searchengine.ts.TSResult;
import org.txm.utils.logger.Log;
import org.txm.web.client.services.PortalService;
import org.txm.web.client.services.TsQueryService;
import org.txm.web.server.TxmUser;
import org.txm.web.server.statics.ActivityLog;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.server.statics.TxmConfig;
import org.txm.web.shared.Actions;
import org.txm.web.shared.TsQueryParam;
import org.txm.web.shared.TsQueryResult;

/**
 * TsQuery service implementation. Computes tsquery
 * 
 * @author mdecorde
 *
 */
public class TsQueryServiceImpl extends PortalService implements TsQueryService {

	private static final long serialVersionUID = -6544593026652231206L;

	private static HashMap<String, TsQueryResult> tsqueryResults = new HashMap<String, TsQueryResult>();
	private static HashMap<String, TSResult> tsResults = new HashMap<String, TSResult>();
	private static HashMap<String, HashMap<Integer, File>> svgFiles = new HashMap<String, HashMap<Integer, File>>();
	private static HashMap<String, TSCorpusManager> managers = new HashMap<String, TSCorpusManager>();

	@Override
	public boolean tsquery(TsQueryParam params) throws Exception {
		Log.info("[TsQueryServiceImpl.tsquery] start. params="+params);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "tsquery", params.corpuspath);
			return false; 
		}

		Corpus corpus = (Corpus)user.askEntity(params.corpuspath, Actions.TSQUERY);
		if (corpus == null) {
			Log.severe("no permission");
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "tsquery", params.corpuspath);
			return false;
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "tsquery", params.corpuspath);

		String corpusid = corpus.getName();
		//System.out.println("Corpus id = Ts corpus id = "+corpusid);

		try {
			TSCorpusManager manager = getManager(corpus, params);

			if (manager != null && manager.isInitialized()) {
				String resultid = this.getSessionId()+params.ID;
				tsqueryResults.remove(resultid);
				tsResults.remove(resultid);
				svgFiles.remove(resultid);
				
				TSCorpus tscorpus = manager.getCorpus(corpusid);
				TSResult tsresult = tscorpus.query(params.query);
				tsresult.setDisplayProperties(params.tprops, params.ntprop);
				
				TSMatch currentMatch = tsresult.getFirst();
				
				TsQueryResult result = new TsQueryResult();
				result.corpusid = corpusid;
				result.html = "";
				result.svg = "";
				result.nomatch = 0;//tsresult.getNumberOfMatch();
				result.nosubgraph = 0;//tsresult.getNumberOfMatch();
				result.nosent = -1;
				result.numberOfMatch = tsresult.getNumberOfMatch();
				result.numberOfSubGraph = currentMatch.getNumberOfSubGraph();
				if (result.numberOfMatch == 0)
					return false;
				
				tsqueryResults.put(resultid, result);
				tsResults.put(resultid, tsresult);
				svgFiles.put(resultid, new HashMap<Integer, File>());
			} else {
				System.out.println("Error: failed to get TSCorpusManager");
			}
		} catch (ims.tiger.gui.tigergraphviewer.forest.ForestException ef) {
			Log.info("[tsquery] User : " + user.getLogin() + " -> Fail, " + ef.getMessage());
			if (ef.getMessage().contains("Empty forest")) {
				return false;
			} else {
				throw ef;
			}
		} catch (Exception e) {
			Log.info("[tsquery] User : " + user.getLogin() + " -> Fail, " + e.getMessage());
			tsqueryResults.remove(getSessionId() + params.corpuspath);
			e.printStackTrace();
			throw e;
			//return false;
		}
		Log.info("[tsquery] User : " + user.getLogin() + " -> Success");
		return true;
	}

	private TSCorpusManager getManager(Corpus corpus, TsQueryParam params) {
		TSCorpusManager manager = managers.get(this.getSessionId()+params.ID);
		if (manager == null || !manager.isInitialized()) {

			String corpusid = corpus.getName();
			File corpusDir = corpus.getBaseDirectory();
			File configdir = new File(corpusDir,"tiger/tigersearch.logprop");
			File registrydir = new File(corpusDir,"tiger");

			if (!registrydir.exists()) {
				Log.severe("Error: Can't find registry dir : "+registrydir);
				return null;
			}

			if (!configdir.exists()) {
				Log.severe("Error: Can't find config dir : "+configdir);
				return null;
			}

			if (!new File(registrydir, corpusid).exists()) {
				Log.severe("Error: Can't find corpus TS binaries : "+new File(registrydir, corpusid));
				return null;
			}

			manager = new TSCorpusManager(registrydir, configdir);
			String resultid = this.getSessionId()+params.ID;
			managers.remove(resultid);
			managers.put(resultid, manager);
		}
		return manager;
	}

	@Override
	public TsQueryResult getFirst(TsQueryParam params) {
		TsQueryResult result = tsqueryResults.get(this.getSessionId()+params.ID);
		if (result == null || result.numberOfMatch == 0)
			return null;

		try {
			File appdir = new File(this.getServletContext().getRealPath("."));

			TSResult tsresult = tsResults.get(this.getSessionId()+params.ID);
			tsresult.setDisplayProperties(params.tprops, params.ntprop);

			TSMatch currentMatch = tsresult.getFirst();
			int idx = tsresult.getCurrentSentenceNo();

			File svgfile = svgFiles.get(this.getSessionId()+params.ID).get(idx);
			if (svgfile != null) {
				svgfile.delete();
			}
			new File(appdir, "svg").mkdir();
			svgfile = File.createTempFile("result", ".svg",new File(appdir, "svg"));
			System.out.println("Create svg : " + svgfile);
			currentMatch.toSVGFile(svgfile);
			svgFiles.get(this.getSessionId()+params.ID).put(idx, svgfile);
			result.html = currentMatch.toHTML();
			result.nosent = idx;
			result.nomatch = tsresult.getCurrentMatchNo();
			result.nosubgraph = currentMatch.getCurrentSubMatchNo();
			result.numberOfMatch = tsresult.getNumberOfMatch();
			result.numberOfSubGraph = currentMatch.getNumberOfSubGraph();
			result.svg = "svg/"+svgfile.getName();

			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.severe("TSQueryService: getFirst: "+e);
		}

		return result;
	}

	@Override
	public TsQueryResult getPrevious(TsQueryParam params) {
		TsQueryResult result = tsqueryResults.get(this.getSessionId()+params.ID);
		if (result == null || result.numberOfMatch == 0)
			return null;

		try {
			File appdir = new File(this.getServletContext().getRealPath("."));	
			TSResult tsresult = tsResults.get(this.getSessionId()+params.ID);
			tsresult.setDisplayProperties(params.tprops, params.ntprop);

			TSMatch currentMatch = tsresult.getPrevious();
			int idx = tsresult.getCurrentSentenceNo();

			File svgfile= svgFiles.get(this.getSessionId()+params.ID).get(idx); // First -> 0
			if (svgfile != null) {
				svgfile.delete();
			}
			new File(appdir, "svg").mkdir();
			svgfile = File.createTempFile("result", ".svg",new File(appdir, "svg"));
			System.out.println("Create svg : " + svgfile);
			currentMatch.toSVGFile(svgfile);
			svgFiles.get(this.getSessionId()+params.ID).put(idx, svgfile);

			result.html = currentMatch.toHTML();
			result.nosent = idx;
			result.nomatch = tsresult.getCurrentMatchNo();
			result.nosubgraph = 0;
			result.numberOfMatch = tsresult.getNumberOfMatch();
			result.numberOfSubGraph = currentMatch.getNumberOfSubGraph();

			result.svg = "svg/"+svgfile.getName();
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.severe("TSQueryService: getPrevious: "+e);
		}
		return result;
	}

	@Override
	public TsQueryResult getNext(TsQueryParam params) {
		TsQueryResult result = tsqueryResults.get(this.getSessionId()+params.ID);
		if (result == null || result.numberOfMatch == 0)
			return null;

		try {
			File appdir = new File(this.getServletContext().getRealPath("."));	
			TSResult tsresult = tsResults.get(this.getSessionId()+params.ID);
			tsresult.setDisplayProperties(params.tprops, params.ntprop);

			TSMatch currentMatch = tsresult.getNext();
			int idx = tsresult.getCurrentSentenceNo();

			File svgfile = svgFiles.get(this.getSessionId()+params.ID).get(idx);
			if (svgfile != null) {
				svgfile.delete();
			}
			new File(appdir, "svg").mkdir();
			svgfile = File.createTempFile("result", ".svg",new File(appdir, "svg"));
			System.out.println("Create svg : " + svgfile);
			currentMatch.toSVGFile(svgfile);
			svgFiles.get(this.getSessionId()+params.ID).put(idx, svgfile);

			result.html = currentMatch.toHTML();
			result.nosent = idx;
			result.nosubgraph = 0;
			result.numberOfMatch = tsresult.getNumberOfMatch();
			result.numberOfSubGraph = currentMatch.getNumberOfSubGraph();
			result.nomatch = tsresult.getCurrentMatchNo();
			result.svg = "svg/"+svgfile.getName();
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.severe("TSQueryService: getNext: "+e);
		}
		return result;
	}

	@Override
	public TsQueryResult getLast(TsQueryParam params) {
		TsQueryResult result = tsqueryResults.get(this.getSessionId()+params.ID);
		if (result == null || result.numberOfMatch == 0)
			return null;

		try {
			File appdir = new File(this.getServletContext().getRealPath("."));	
			TSResult tsresult = tsResults.get(this.getSessionId()+params.ID);
			tsresult.setDisplayProperties(params.tprops, params.ntprop);

			TSMatch currentMatch = tsresult.getLast();
			int idx = tsresult.getCurrentSentenceNo();

			File svgfile = svgFiles.get(this.getSessionId()+params.ID).get(idx);
			if (svgfile != null) {
				svgfile.delete();
			}
			new File(appdir, "svg").mkdir();
			svgfile = File.createTempFile("result", ".svg",new File(appdir, "svg"));
			System.out.println("Create svg : " + svgfile);
			currentMatch.toSVGFile(svgfile);
			svgFiles.get(this.getSessionId()+params.ID).put(idx, svgfile);

			result.html = currentMatch.toHTML();
			result.nosent = idx;
			result.nosubgraph = 0;
			result.numberOfMatch = tsresult.getNumberOfMatch();
			result.numberOfSubGraph = currentMatch.getNumberOfSubGraph();
			result.nomatch = tsresult.getCurrentMatchNo();
			result.svg = "svg/"+svgfile.getName();
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.severe("TSQueryService: getLast: "+e);
		}
		return result;
	}

	public static String testhtml(String query) {
		return "<span>The full sentence with <b>query="+query.replace("<", "&lt;")+"</b></span>";
	}

	public static String testsvg(String query) {
		//return "<?xml version=\"1.0\" standalone=\"no\"?><!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.0//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"3662\" height=\"863\" viewBox=\"0 0 3662 863\"><rect x=\"50\" y=\"50\" rx=\"5\" ry=\"5\" width=\"200\" height=\"100\" style=\"fill:#CCCCFF;stroke:#000099\"/><text x=\"55\" y=\"90\" style=\"stroke:#000099;fill:#000099;font-size:24;\"> test </text></svg>";
		return "svg/test.svg\"";
	}

	@Override
	public HashMap<String, List<String>> getFeatures(TsQueryParam params) {
		Log.info("[TsQueryServiceImpl.tsquery] start. params="+params);
		String corpuspath = params.corpuspath;
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "tsexport", corpuspath);
			return null; 
		}
		Corpus corpus = (Corpus)user.askEntity(corpuspath, Actions.TSQUERY);
		if (corpus == null) {
			Log.severe("no permission");
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "tsexport", corpuspath);
			return null;
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "tsexport", corpuspath);

		String corpusid = corpus.getName();

		TSCorpusManager manager = getManager(corpus, params);
		if (manager != null && manager.isInitialized()) {
			TSCorpus tscorpus = manager.getCorpus(corpusid);
			HashMap<String, List<String>> features = new HashMap<String, List<String>>();
			List<String> tfeatures = tscorpus.getTFeatures();

			features.put("nt", new ArrayList<String>(tscorpus.getNTFeatures()));
			features.put("t", new ArrayList<String>(tfeatures));
			return features;
		} else {
			System.out.println("Error: TsQueryServiceImpl: getFeature: could not retrieve TsCorpusManager");
		}
		return null;
	}

	@Override
	public String exportForest(TsQueryParam params, int contextsize,
			String method, boolean punct, String[] ntprops, String[] tprops) {

		TransformerFactory fabrique = TransformerFactory.newInstance();
		System.out.println("Default TransformerFactory: "+fabrique.getClass());
		SAXParserFactory factory = SAXParserFactory.newInstance();
		System.out.println("Default SAXParserFactory: "+factory.getClass());
		System.out.println("Xerces version: "+com.sun.org.apache.xerces.internal.impl.Version.getVersion());

		if(method.equals("Basic concordance")) {
			method = TSResult.CONCSIMPLE;
		} else if(method.equals("Single word pivot concordance")) {
			method = TSResult.CONCMOTPIVOT;
		} else if(method.equals("Pivot and block concordance")) {
			method = TSResult.CONCBLOCKS;
		} else
			return "wrong method";

		TSResult result = TsQueryServiceImpl.tsResults.get(this.getSessionId()+params.ID);
		File appdir = new File(this.getServletContext().getRealPath("."));
		new File(appdir, "csv").mkdir();
		File csvFile = new File(appdir, "csv/"+(params.ID*params.ID)+".txt"); // not negative 
		System.out.println("csvFile: "+csvFile);
		System.out.println("method: "+method);
		System.out.println("cx: "+contextsize);
		System.out.println("punct: "+punct);
		System.out.println("nt: "+Arrays.asList(ntprops));
		System.out.println("t: "+Arrays.asList(tprops));

		try {
			if (result.toConcordance(csvFile, method, contextsize, 
					new ArrayList<String>(Arrays.asList(ntprops)), 
					new ArrayList<String>(Arrays.asList(tprops)), punct)) {
				String portal_address = TxmConfig.getProperty("portal_address");
				if (portal_address == null) {
					Log.severe("Error while exporting TIGERSearcg SVG result: 'portal_address' is not set");
					return "failed: 'portal_address' is not set";
				}
				return portal_address+"/csv/"+csvFile.getName();

			} else {
				return "failed: error while creating concordance";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.severe("TSQueryService: export forest: "+e);
			return "failed: "+e;
		}
	}

	public TsQueryResult getGraph(TsQueryParam params, int graphNo, int subGraphNo) {
		TsQueryResult result = tsqueryResults.get(this.getSessionId()+params.ID);
		if (result == null || result.numberOfMatch == 0)
			return null;

		try {
			File appdir = new File(this.getServletContext().getRealPath("."));	
			TSResult tsresult = tsResults.get(this.getSessionId()+params.ID);
			tsresult.setDisplayProperties(params.tprops, params.ntprop);

			TSMatch currentMatch = tsresult.setCurrentMatch(graphNo);
			currentMatch.setSubGraph(subGraphNo);
			int idx = tsresult.getCurrentSentenceNo();

			File svgfile = svgFiles.get(this.getSessionId()+params.ID).get(idx);
			if (svgfile != null) {
				svgfile.delete();
			}
			new File(appdir, "svg").mkdir();
			svgfile = File.createTempFile("result", ".svg",new File(appdir, "svg"));
			System.out.println("Create svg file: " + svgfile);
			currentMatch.toSVGFile(svgfile);
			svgFiles.get(this.getSessionId()+params.ID).put(idx, svgfile);

			result.html = currentMatch.toHTML();
			result.nosent = idx;
			result.nomatch = tsresult.getCurrentMatchNo();
			result.nosubgraph = currentMatch.getCurrentSubMatchNo();
			result.numberOfMatch = tsresult.getNumberOfMatch();
			result.numberOfSubGraph = currentMatch.getNumberOfSubGraph();
			String portal_address = TxmConfig.getProperty("portal_address");
			if (portal_address == null) {
				Log.severe("Error while exporting TIGERSearcg SVG result: 'portal_address' is not set");
				return null;
			}
			result.svg = portal_address+"/svg/"+svgfile.getName();
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public TsQueryResult getPreviousSub(TsQueryParam params) {
		TsQueryResult result = tsqueryResults.get(this.getSessionId()+params.ID);
		if (result == null || result.numberOfMatch == 0)
			return null;

		try {
			File appdir = new File(this.getServletContext().getRealPath("."));	
			TSResult tsresult = tsResults.get(this.getSessionId()+params.ID);
			tsresult.setDisplayProperties(params.tprops, params.ntprop);

			TSMatch currentMatch = tsresult.getCurrentMatch();
			currentMatch.previousSubGraph();
			int idx = tsresult.getCurrentSentenceNo();

			File svgfile = svgFiles.get(this.getSessionId()+params.ID).get(idx);
			if (svgfile != null) {
				svgfile.delete();
			}
			new File(appdir, "svg").mkdir();
			svgfile = File.createTempFile("result", ".svg",new File(appdir, "svg"));
			System.out.println("Create svg : " + svgfile);
			currentMatch.toSVGFile(svgfile);
			svgFiles.get(this.getSessionId()+params.ID).put(idx, svgfile);

			result.html = currentMatch.toHTML();
			result.nosent = idx;
			result.nomatch = tsresult.getCurrentMatchNo();
			result.nosubgraph = currentMatch.getCurrentSubMatchNo();
			result.numberOfMatch = tsresult.getNumberOfMatch();
			result.numberOfSubGraph = currentMatch.getNumberOfSubGraph();
			result.svg = "svg/"+svgfile.getName();
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public TsQueryResult getNextSub(TsQueryParam params) {
		TsQueryResult result = tsqueryResults.get(this.getSessionId()+params.ID);
		if (result == null || result.numberOfMatch == 0)
			return null;

		try {
			File appdir = new File(this.getServletContext().getRealPath("."));	
			TSResult tsresult = tsResults.get(this.getSessionId()+params.ID);
			tsresult.setDisplayProperties(params.tprops, params.ntprop);
			TSMatch currentMatch = tsresult.getCurrentMatch();
			currentMatch.nextSubGraph();
			int idx = tsresult.getCurrentSentenceNo();

			File svgfile = svgFiles.get(this.getSessionId()+params.ID).get(idx);

			if (svgfile != null) {
				svgfile.delete();
			}
			new File(appdir, "svg").mkdir();
			svgfile = File.createTempFile("result", ".svg",new File(appdir, "svg"));
			System.out.println("Create svg : " + svgfile);
			currentMatch.toSVGFile(svgfile);
			svgFiles.get(this.getSessionId()+params.ID).put(idx, svgfile);

			result.html = currentMatch.toHTML();
			result.nosent = idx;
			result.nomatch = tsresult.getCurrentMatchNo();
			result.nosubgraph = currentMatch.getCurrentSubMatchNo();
			result.numberOfMatch = tsresult.getNumberOfMatch();
			result.numberOfSubGraph = currentMatch.getNumberOfSubGraph();
			result.svg = "svg/"+svgfile.getName();
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public TsQueryResult setFeatures(TsQueryParam params) {

		String resultid = this.getSessionId()+params.ID;
		List<String> tprops = params.tprops;
		String ntprop = params.ntprop;
		TsQueryResult result = tsqueryResults.get(resultid);
		if (result == null || result.numberOfMatch == 0)
			return null;

		try {
			File appdir = new File(this.getServletContext().getRealPath("."));	
			TSResult tsresult = tsResults.get(resultid);
			TSMatch currentMatch = tsresult.getCurrentMatch();
			int idx = tsresult.getCurrentSentenceNo();

			tsresult.setDisplayProperties(tprops, ntprop);

			File svgfile = svgFiles.get(resultid).get(idx);
			if (svgfile != null) {
				svgfile.delete();
			}
			new File(appdir, "svg").mkdir();
			svgfile = File.createTempFile("result", ".svg",new File(appdir, "svg"));
			System.out.println("Create svg : " + svgfile);
			currentMatch.toSVGFile(svgfile);
			svgFiles.get(resultid).put(idx, svgfile);

			result.html = currentMatch.toHTML();
			result.nosent = idx;
			result.nomatch = tsresult.getCurrentMatchNo();
			result.nosubgraph = currentMatch.getCurrentSubMatchNo();
			result.numberOfMatch = tsresult.getNumberOfMatch();
			result.numberOfSubGraph = currentMatch.getNumberOfSubGraph();
			result.svg = "svg/"+svgfile.getName();
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public void dropResult(TsQueryParam params) {
		String s = this.getSessionId()+params.ID;
		tsResults.remove(s);
		svgFiles.remove(s);
		tsqueryResults.remove(s);
		managers.remove(s);
	}

	public static int cleanResults(String id) {
		int n = 0;
		HashSet<String> keys = new HashSet<String>();
		keys.addAll(tsResults.keySet());
		for (String k : keys) {
			if (k.startsWith(id)) {
				tsResults.remove(k);
				svgFiles.remove(k);
				tsqueryResults.remove(k);
				managers.remove(k);
				n++;
			}
		}
		return n;
	}
}
