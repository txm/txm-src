package org.txm.web.server.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.txm.functions.selection.Selection;
import org.txm.metadatas.DateMetadata;
import org.txm.metadatas.Metadata;
import org.txm.metadatas.MetadataGroup;
import org.txm.metadatas.MinMaxDateGroup;
import org.txm.objects.Text;
import org.txm.objects.TxmObject;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;
import org.txm.web.aas.authorization.permissions.EditionPermission;
import org.txm.web.client.services.PortalService;
import org.txm.web.client.services.TextSelectionService;
import org.txm.web.server.TxmUser;
import org.txm.web.server.statics.ActivityLog;
import org.txm.web.server.statics.Corpora;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.shared.Actions;
import org.txm.web.shared.MetadataInfos;
import org.txm.web.shared.TextSelectionResult;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * the services for the text selection tools
 * @author mdecorde
 *
 */
public class TextSelectionServiceImpl extends PortalService implements TextSelectionService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8379756554397958314L;
	/**
	 * the Selection are store by corpus path
	 */
	static HashMap<String, Selection> selections = new HashMap<String, Selection>();
	HashMap<String, String> metadataGroups;

	private HashMap<String, Integer> getNumberOfWordsPerText(String itemPath) {
		Selection s = getSelection(itemPath);
		return s.getNumberOfWordsPerText();
	}

	private ArrayList<String> getTextIds(String itemPath) {
		Selection s = getSelection(itemPath);
		return new ArrayList<String>(s.getTextIds());
	}

	private HashMap<String, ArrayList<String>> getValuesPerProperty(String itemPath) {
		Selection s = getSelection(itemPath);
		HashMap<String, ArrayList<String>> result = new HashMap<String, ArrayList<String>>();
		HashMap<String, List<String>> tmp = s.getStructuralUnitPropertyValues();
		for (String k : tmp.keySet()) {
			result.put(k, new ArrayList<String>(tmp.get(k)));
		}
		
		return result;
	}

	/**
	 * return metadata names and group names
	 * @param itemPath
	 * @return
	 */
	private ArrayList<String> getSelectionProperties(String itemPath) {
		Selection s = getSelection(itemPath);
		return s.getSelectionPropertyNames();
	}

	/**
	 * return the metadata names which can be displayed
	 * @param itemPath
	 * @return
	 */
	private List<String> getDisplayProperties(String itemPath) {
		Selection s = getSelection(itemPath);
		return s.getDisplayPropertyNames();
	}

	/**
	 * Get the metadata values per Text
	 * @param itemPath
	 * @return
	 */
	public HashMap<String, HashMap<String, String>> getValuesPerText(String itemPath) {
		Selection s = getSelection(itemPath);
		try {
			s.fullDump();
		} catch (CqiClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HashMap<String, HashMap<String, String>> hash = s.getTextMetadatas();
		// metadatas is a copy of hash
		HashMap<String, HashMap<String, String>> metadatas = new HashMap<String, HashMap<String, String>>();
		for(String textid : hash.keySet())
		{	
			HashMap<String, String> newhash = new HashMap<String, String>();
			for(String sup : hash.get(textid).keySet())
			{
				newhash.put(sup, hash.get(textid).get(sup));
			}
			metadatas.put(textid, newhash);
		}

		return metadatas;
	}

	/**
	 * get stored selection
	 * @param itemPath
	 * @return
	 */
	private Selection getSelection(String itemPath)
	{
		Object o = Corpora.get(itemPath);
		if (o == null)
			return null;
		if (!(o instanceof MainCorpus))
			return null;
		MainCorpus corpus = (MainCorpus)o;

		if (selections.containsKey(itemPath))
			return selections.get(itemPath);

		Selection s = new Selection(corpus);
		selections.put(itemPath, s);
		return s;
	}

	@Override
	public TextSelectionResult getAllStuff(String itemPath) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		TextSelectionResult result = new TextSelectionResult();	
		if (user == null) {
			Log.severe("Session " + getSessionId() + " has expired");
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getAllStuff", itemPath);
			return result;
		}

		Object object = user.askEntity(itemPath, Actions.TEXTSELECTION);
		if (object == null && !(object instanceof Corpus)) {
			Log.severe("no permission");
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getAllStuff", itemPath);
			return result;
		}
		
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getAllStuff", itemPath);
		Selection s = getSelection(itemPath);
		if (s == null)
			return null;
		
		result.properties = getSelectionProperties(itemPath);
		result.valuesPerProperties = getValuesPerProperty(itemPath);
		result.valuesPerText = getValuesPerText(itemPath);
		result.propertiesInfos = new HashMap<String, MetadataInfos>();
		for (Metadata m : getSelectionPropertyInfos(itemPath)) {
			//System.out.println("metadata class: "+m.getClass());
			MetadataInfos infos = new MetadataInfos();
			infos.name = m.id;
			infos.type = m.type;
			infos.colwidth = m.colwidth;
			infos.shortname = m.shortname;
			infos.longname = m.longname;
			infos.selection = m.selection;
			infos.partition = m.partition;
			infos.display = m.display;
			infos.open = m.open;
			
			HashMap<String, String> extras = new HashMap<String, String>();
			if (m.sortBy != null)
				extras.put("sortBy", m.sortBy);
			if (m instanceof DateMetadata) {
				extras.put("inputFormat", ((DateMetadata) m).inputFormat);
				extras.put("outputFormat", ((DateMetadata) m).outputFormat);
			}
			infos.extra = extras;
			
			result.propertiesInfos.put(m.id, infos);
		}
	
		result.groups = new ArrayList<String>();
		result.groupsInfos = new HashMap<String, MetadataInfos>();
		
		result.groupsProperties = new HashMap<String, ArrayList<String>>();
		HashMap<String, List<String>> tmp = s.getMetadataGroupNames();
		for (String k : tmp.keySet()) {
			result.groupsProperties.put(k, new ArrayList<String>(tmp.get(k)));
		}
		
		System.out.println("**get infos of groups: "+s.getMetadatagroups());
		for (MetadataGroup gp : s.getMetadatagroups()) {
			MetadataInfos infos = new MetadataInfos();
			infos.name = gp.id;
			infos.shortname = gp.shortname;
			infos.longname = gp.longname;
			infos.type = gp.type;
			infos.selection = gp.selection;
			infos.partition = gp.partition;
			infos.display = gp.display;
			infos.open = gp.open;
			
			HashMap<String, String> extras = new HashMap<String, String>();
			extras.put("position", ""+gp.position);
			if (gp.sortBy != null)
				extras.put("sortBy", gp.sortBy);
			if (gp instanceof MinMaxDateGroup) {
				extras.put("inputFormat", ((MinMaxDateGroup)gp).getInputFormat());
				extras.put("outputFormat", ((MinMaxDateGroup)gp).getOutputFormat());
			}
			System.out.println("extras "+gp+": "+extras);
			infos.extra = extras;
			result.groups.add(gp.id);
			result.groupsInfos.put(gp.id, infos);
		}
		
		result.displayGroups = s.getDisplayGroupNames();
		result.displayProperties = s.getDisplayPropertyNames();
		result.selectionGroups = s.getSelectionGroupNames();
		result.selectionProperties = s.getSelectionPropertyNames();

		result.textids = getTextIds(itemPath);
		result.numberOfWordsPerText = getNumberOfWordsPerText(itemPath);
		
		result.biblios = getBiblios(itemPath);

		//result.dump();
		Corpus corpus = (Corpus) Corpora.get(itemPath);
		for (Metadata m : corpus.getImportMetadata())
			System.out.println(m);
		return result;
	}
	
	private ArrayList<Metadata> getSelectionPropertyInfos(
			String itemPath) {
		Selection s = getSelection(itemPath);
		return s.getMetadatas();
	}

	public HashMap<String, String> getBiblios(String itempath) {
		HashMap<String, String> result = new HashMap<String, String>();
		TxmObject obj = Corpora.get(itempath);
		if (obj == null) {
			//System.out.println("EditionServiceImpl: getTextsIds: corpus "+itempath+" not found");
			return result;
		}
		
		Corpus corpus = null;
		if (obj instanceof Corpus)
			corpus = ((Corpus)obj);
		else
			return result;
				
		for (Text text : corpus.getTexts()) {
			result.put(text.getName(), text.getBiblioPath().toString());
		}
		
		return result;
	}

	public static void freeCaches(String corpusname) {
		HashSet<String> keys = new HashSet<String>();
		keys.addAll(selections.keySet());
		for (String key : keys) {
			Corpus corpus = selections.get(key).getCorpus();
			if (corpus.getName().equals(corpusname))
				selections.remove(key);
		}
	}

	@Override
	public TextSelectionResult getStuffForBiblios(String itemPath) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		TextSelectionResult result = new TextSelectionResult();	
		if (user == null) {
			Log.severe("Session " + getSessionId() + " has expired");
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getAllStuff", itemPath);
			return result;
		}
		
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getAllBiblioStuff", itemPath);
		Selection s = getSelection(itemPath);
		if(s == null)
			return null;
		
		result.properties = getSelectionProperties(itemPath);
		result.valuesPerText = getValuesPerText(itemPath);
		result.propertiesInfos = new HashMap<String, MetadataInfos>();
		for (Metadata m : getSelectionPropertyInfos(itemPath)) {
			//System.out.println("metadata class: "+m.getClass());
			MetadataInfos infos = new MetadataInfos();
			infos.name = m.id;
			infos.type = m.type;
			infos.shortname = m.shortname;
			infos.longname = m.longname;
			infos.selection = m.selection;
			infos.partition = m.partition;
			infos.display = m.display;
			infos.open = m.open;
			
			HashMap<String, String> extras = new HashMap<String, String>();
			if (m.sortBy != null)
				extras.put("sortBy", m.sortBy);
			if (m instanceof DateMetadata) {
				extras.put("inputFormat", ((DateMetadata) m).inputFormat);
				extras.put("outputFormat", ((DateMetadata) m).outputFormat);
			}
			infos.extra = extras;
			
			result.propertiesInfos.put(m.id, infos);
		}
		
		result.displayProperties = s.getDisplayPropertyNames();
		result.textids = getTextIds(itemPath);
		result.biblios = getBiblios(itemPath);
		result.pdfs = getPDFs(user, itemPath);
		result.editions = getEditionsAvaibilities(user, itemPath);
		return result;
	}

	private HashMap<String, String> getPDFs(TxmUser user, String itemPath) {
		HashMap<String, String> result = new HashMap<String, String>();
		TxmObject obj = Corpora.get(itemPath);
		if (obj == null) {
			//System.out.println("EditionServiceImpl: getTextsIds: corpus "+itempath+" not found");
			return result;
		}
		
		Corpus corpus = null;
		if (obj instanceof Corpus)
			corpus = ((Corpus)obj);
		else
			return result;

		for (Text text : corpus.getTexts()) {
			String path = "/" + corpus.getName() + "/" + text.getName();
			if (user.can(path, new EditionPermission()))
				result.put(text.getName(), text.getPDFPath());
		}
		
		return result;
	}
	
	private HashMap<String, String> getEditionsAvaibilities(TxmUser user, String itemPath) {
		HashMap<String, String> result = new HashMap<String, String>();
		TxmObject obj = Corpora.get(itemPath);
		if (obj == null) {
			//System.out.println("EditionServiceImpl: getTextsIds: corpus "+itempath+" not found");
			return result;
		}
		
		Corpus corpus = null;
		if (obj instanceof Corpus)
			corpus = ((Corpus)obj);
		else
			return result;
				
		String ok = "true";
		for (Text text : corpus.getTexts()) {
			String path = "/" + corpus.getName() + "/" + text.getName();
			if (user.can(path, new EditionPermission()))
				result.put(text.getName(), ok);
		}
		
		return result;
	}
}