package org.txm.web.server.services;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.txm.Toolbox;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.query.Query;
import org.txm.utils.logger.Log;
import org.txm.web.aas.authorization.permissions.CooccurrencePermission;
import org.txm.web.client.services.CooccurrenceService;
import org.txm.web.client.services.PortalService;
import org.txm.web.server.TxmUser;
import org.txm.web.server.services.Cooccurrence.CLine;
import org.txm.web.server.statics.ActivityLog;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.shared.Actions;
import org.txm.web.shared.CooccurrenceParam;
import org.txm.web.shared.CooccurrenceResult;
import org.txm.web.shared.CooccurrenceResultLine;

public class CooccurrenceServiceImpl extends PortalService implements CooccurrenceService{


	public static HashMap<String, Cooccurrence> results = new HashMap<String, Cooccurrence>();
	private static final long serialVersionUID = -8104493827443151567L;

	public static int cleanResults(String id) {
		int n = 0;
		HashSet<String> keys = new HashSet<String>();
		keys.addAll(results.keySet());
		for (String k : keys) {
			if (k.startsWith(id)) {
				results.remove(k);
				n++;
			}
		}
		return n;
	}
	
	@Override
	public CooccurrenceResult getLines(CooccurrenceParam params) {
		Log.info("[CooccurrenceServiceImpl.getLines()] start. params="+params);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return new CooccurrenceResult(); 
		}
		if (!user.can(params.getCorpusPath(), new CooccurrencePermission())) {
			Log.severe("no permission");
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "cooc.getLines", params.getID(), params.getCorpusPath(), params.getQuery());
			return new CooccurrenceResult(); 
		}
		
		CooccurrenceResult result = new CooccurrenceResult();
		Cooccurrence cooccurrence = results.get(getSessionId() + params.getID());
		if (cooccurrence == null) {
			Log.info("[CooccurrenceServiceImpl.getLines()] cooccurrence null for " + getSessionId() + params.getID());
			cooccurrence(params);
			cooccurrence = results.get(getSessionId() + params.getID());
		}
		Log.info("[CooccurrenceServiceImpl.getLines()] cooccurence =  " + cooccurrence);
		for (CLine line : cooccurrence.getLines()) {
			if (line.score < 0) continue; // don't show line with negative scores
			
			CooccurrenceResultLine entry = new CooccurrenceResultLine();
			entry.setCooccurrent(line.occ);
			entry.setScore(line.score);
			entry.setCoFrequency(line.nbocc);
			entry.setFrequency(line.freq);
			entry.setAverageDistance(line.distmoyenne);
			result.add(entry);
		}		
		
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "cooc.getLines", params.getID(), params.getCorpusPath(), params.getQuery());
		return result;
	}

	@Override
	public String cooccurrence(CooccurrenceParam params) {
		Log.info("[CooccurrenceServiceImpl.cooccurrence] start. params="+params);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return "session"; 
		}
		
		if (!Toolbox.isStatEngineInitialized()) {
			Log.severe("[CooccurrenceServiceImpl.cooccurrence] no stat engine");
			return "stat";
		}
		if (!user.can(params.getCorpusPath(), new CooccurrencePermission())) {
			Log.severe("no permission");
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "cooccurrence", params.getID(), params.getCorpusPath(), params.getQuery());
			return "permission"; 
		}
		
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "cooccurrence", params.getID(), params.getCorpusPath(), params.getQuery());
		
		Corpus corpus = (Corpus)user.askEntity(params.getCorpusPath(), Actions.COOCCURRENCE);
		Query CWBquery = new Query(Query.fixQuery(params.getQuery()));
		List<Property>	properties = new ArrayList<Property>();
		
		try {
			for (String prop : params.getProperties())
				properties.add(corpus.getProperty(prop.trim()));
			
			StructuralUnit struct = corpus.getStructuralUnit(params.getStructure());
			
			Cooccurrence cooc = new Cooccurrence(corpus, 
														CWBquery, 
														properties, 
														struct,
														params.getLeftmax()+1,
														params.getLeftmin()+1,
														params.getRightmin()+1,
														params.getRightmax()+1,
														params.getMinfreq(),
														params.getMinCofreq(),
														params.getMinscore(),
														params.getPivot(), false);
	
			cooc.stepQueryLimits();

			cooc.stepGetMatches(); 
			
			cooc.stepBuildSignatures();

			cooc.stepCount();

			cooc.stepBuildLexicalTable();

			cooc.stepGetScores();

			//Save results
			results.put(getSessionId() + params.getID(), cooc);
		} catch (Exception e) {
			Log.warning("[cooccurrence] User : " + user.getLogin() + " -> Fail, " + e);
			e.printStackTrace();
			String error = Toolbox.getCqiClient().getLastError();
			ActivityLog.warning(getSessionId(), getRemoteAddr(),user.getLogin(), "cooccurrence", params.getID(), params.getCorpusPath(), params.getQuery());
			results.remove(user.getLogin() + params.getID());
			//e.printStackTrace();
			if(error != null && error.length() > 0)
				return error;
			else
				return "query error";
		}
		

		Log.info("[cooccurrence] User : " + user.getLogin() + " -> ended with success ");
		return "";
	}

	public static void freeCaches(String corpusname) {
		HashSet<String> keys = new HashSet<String>();
		keys.addAll(results.keySet());
		for (String key : keys) {
			Cooccurrence cooccurrence = results.get(key);
			if (cooccurrence.getCorpus().getName().equals(corpusname)) {
				System.out.println("remove conc of session: "+key);
				results.remove(key);
			}
		}
	}

	@Override
	public String exportCooccurrence(CooccurrenceParam params) {
			Log.info("[CooccurrenceServiceImpl.exportCooccurrence()] start. params="+params);
			TxmUser user = SessionManager.getUser(this.getSessionId());
			if (user == null) {
				Log.severe("Session "+getSessionId()+" has expired");
				return "session"; 
			}
			if (!user.can(params.getCorpusPath(), new CooccurrencePermission())) {
				Log.severe("no permission");
				ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "cooc.exportCooccurrence", params.getID(), params.getCorpusPath(), params.getQuery());
				return "permission"; 
			}
			
			Cooccurrence cooccurrence = results.get(getSessionId() + params.getID());
			Log.info("[CooccurrenceServiceImpl.exportCooccurrence()] cooccurence =  " + cooccurrence);
						
			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "cooc.exportCooccurrence", params.getID(), params.getCorpusPath(), params.getQuery());
			
			String ID = ""+new java.util.Random().nextInt(999999999);
			File appdir = new File(this.getServletContext().getRealPath("."));

			File txtFile = new File(appdir, "csv/cooccurences_"+ID+".txt");
			txtFile.getParentFile().mkdirs();

			cooccurrence.toTxt(txtFile, "UTF-8", "\t", "");
			
			return "csv/"+txtFile.getName();
	}
}
