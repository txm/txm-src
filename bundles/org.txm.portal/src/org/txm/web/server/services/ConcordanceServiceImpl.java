package org.txm.web.server.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.security.Permission;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.txm.Toolbox;
import org.txm.functions.ReferencePattern;
import org.txm.functions.concordances.Concordance;
import org.txm.functions.concordances.Concordance.Format;
import org.txm.functions.concordances.Line;
import org.txm.functions.concordances.comparators.CompositeComparator;
import org.txm.functions.concordances.comparators.LexicographicKeywordComparator;
import org.txm.functions.concordances.comparators.LexicographicLeftContextComparator;
import org.txm.functions.concordances.comparators.LexicographicRightContextComparator;
import org.txm.functions.concordances.comparators.LineComparator;
import org.txm.functions.concordances.comparators.NullComparator;
import org.txm.functions.concordances.comparators.PropertiesReferenceComparator;
import org.txm.functions.concordances.comparators.ReverseComparator;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.query.Query;
import org.txm.utils.logger.Log;
import org.txm.web.aas.authorization.permissions.ConcordancePermission;
import org.txm.web.client.services.ConcordanceService;
import org.txm.web.client.services.PortalService;
import org.txm.web.server.TxmUser;
import org.txm.web.server.statics.ActivityLog;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.server.statics.TxmConfig;
import org.txm.web.shared.ConcordanceParam;
import org.txm.web.shared.ConcordanceResult;
import org.txm.web.shared.ConcordanceResultLine;
import org.txm.web.shared.Keys;

/**
 * Concordance service implementation. Computes concordances
 * 
 * @author vchabanis
 *
 */
public class ConcordanceServiceImpl extends PortalService implements ConcordanceService {

	private static final long serialVersionUID = -6544593026652231206L;

	public static HashMap<String, Concordance> results = new HashMap<String, Concordance>();
	public static HashMap<String, LineComparator> standardComparators = new HashMap<String, LineComparator>(); // the concordances sorters
	static {
		standardComparators.put(Keys.NONE, new NullComparator());
		standardComparators.put(Keys.KEYWORD, new LexicographicKeywordComparator()); 
		standardComparators.put(Keys.LEFT_CONTEXT, new LexicographicLeftContextComparator()); 
		standardComparators.put(Keys.RIGHT_CONTEXT, new LexicographicRightContextComparator()); 
		standardComparators.put(Keys.REFERENCE, new PropertiesReferenceComparator());
	};
	
	public static int cleanResults(String id) {
		int n = 0;
		HashSet<String> keys = new HashSet<String>();
		keys.addAll(results.keySet());
		for (String k : keys) {
			if (k.startsWith(id)) {
				results.remove(k);
				n++;
			}
		}
		return n;
	}

	@Override
	/**
	 * Compute a concordance given the parameters
	 * @return true if success
	 */
	public String concordance(ConcordanceParam params) {
		Log.info("[ConcordanceServiceImpl.concordance] start. params="+params);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return "session"; 
		}

		if (!user.can(params.getCorpusPath(), new ConcordancePermission())) {
			Log.severe("no permission");
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "concordance", params.getID(), params.getCorpusPath(), params.getQuery());
			return "permission"; 
		}

		Corpus corpus = (Corpus)user.getEntity(params.getCorpusPath());

		try {
			Query CWBquery = new Query(Query.fixQuery(params.getQuery()));

			// get sort properties
			List<Property>	sleftProp = new ArrayList<Property>();
			for (String prop : params.getLeftSortProperties()) {
				sleftProp.add(corpus.getProperty(prop.trim()));
			}

			List<Property>	skeywordProp = new ArrayList<Property>();
			for (String prop : params.getKeywordSortProperties()) {
				skeywordProp.add(corpus.getProperty(prop.trim()));
			}

			List<Property>	srightProp = new ArrayList<Property>();
			for (String prop : params.getRightSortProperties()) {
				srightProp.add(corpus.getProperty(prop.trim()));
			}

			//get view properties
			List<Property>	vleftProp = new ArrayList<Property>();
			for (String prop : params.getLeftViewProperties()) {
				vleftProp.add(corpus.getProperty(prop.trim()));
			}

			List<Property>	vkeywordProp = new ArrayList<Property>();
			for (String prop : params.getKeywordViewProperties()) {
				vkeywordProp.add(corpus.getProperty(prop.trim()));
			}

			List<Property>	vrightProp = new ArrayList<Property>();
			for (String prop : params.getRightViewProperties()) {
				vrightProp.add(corpus.getProperty(prop.trim()));
			}

			ReferencePattern viewReference = new ReferencePattern();
			for (String ref : params.getViewReferences()) {
				String[] struc_prop = ref.split(":");
				if (struc_prop.length == 2) {
					StructuralUnit p1 = corpus.getStructuralUnit(struc_prop[0]);
					Property pp = p1.getProperty(struc_prop[1]);
					viewReference.addProperty(pp);
				} else {
					Property p1 = corpus.getProperty(struc_prop[0].trim());
					viewReference.addProperty(p1);
				}
			}

			if (viewReference.getProperties().size() == 0) {
				if (corpus.getProperty("ref") != null) {
					viewReference.addProperty(corpus.getProperty("ref"));
				} else if (corpus.getStructuralUnit("text") != null) {
					if (corpus.getStructuralUnit("text").getProperty("id") != null)
						viewReference.addProperty(corpus.getStructuralUnit("text").getProperty("id"));
				}
			}

			ReferencePattern sortReference = new ReferencePattern();
			for (String ref : params.getSortReferences()) {
				String[] struc_prop = ref.split(":");
				if (struc_prop.length == 2) {
					StructuralUnit p1 = corpus.getStructuralUnit(struc_prop[0]);
					Property pp = p1.getProperty(struc_prop[1]);
					sortReference.addProperty(pp);
				} else {
					Property p1 = corpus.getProperty(struc_prop[0].trim());
					sortReference.addProperty(p1);
				}
			}

			if (sortReference.getProperties().size() == 0) {
				if (corpus.getProperty("ref") != null) {
					sortReference.addProperty(corpus.getProperty("ref"));
				} else if (corpus.getStructuralUnit("text") != null) {
					if (corpus.getStructuralUnit("text").getProperty("id") != null)
						sortReference.addProperty(corpus.getStructuralUnit("text").getProperty("id"));
				}
			}

			Concordance conc = new Concordance(corpus, CWBquery, sleftProp, srightProp, skeywordProp, vleftProp, vrightProp, vkeywordProp, viewReference, sortReference, params.getLeftContextSize(), params.getRightContextSize());
			System.out.println("SET CQL LIMIT TO: "+corpus.getCQLLimitQuery());
			conc.setCQLSeparator(corpus.getCQLLimitQuery());
			
			// Sort results
			List<LineComparator> comparators = new ArrayList<LineComparator>();
			for (String key : params.getSortKeys()) {
				comparators.add(standardComparators.get(key));
			}

			//Save results, they are shared between session
			results.put(getSessionId() + params.getID(), conc);
			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "concordance", params.getID(), params.getCorpusPath(), params.getQuery());
		} catch (CqiClientException e) {
			Log.warning("[concordance] User : " + user.getLogin() + " -> Fail, " + e.getMessage());
			String error ="query error";
			try {
				error = Toolbox.getCqiClient().getLastCQPError();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ActivityLog.warning(getSessionId(), getRemoteAddr(),user.getLogin(), "concordance", params.getID(), params.getCorpusPath(), params.getQuery());
			results.remove(user.getLogin() + params.getID());
			//e.printStackTrace();
			if(error != null && error.length() > 0)
				return error;
			else
				return "Error: failed to get last error ";
		}
		Log.info("[concordance] User : " + user.getLogin() + " -> Success");
		return "";
	}

	@Override
	/**
	 * @return the results of the concordances given the concordances parameters
	 */
	public ConcordanceResult getLines(ConcordanceParam params) {	
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return new ConcordanceResult("session"); 
		}
		if (!user.can(params.getCorpusPath(), new ConcordancePermission())) {
			Log.severe("[Concordance.getLines] no permission");
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "conc.getLines", params.getID());
			return new ConcordanceResult("permission"); 
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "conc.getLines", params.getID());
		ConcordanceResult result = new ConcordanceResult(null);
		Permission askedPermission = new ConcordancePermission(params.getLeftContextSize(), params.getRightContextSize());
		Concordance conc = results.get(getSessionId() + params.getID());

		if (conc == null) {
			concordance(params);
			conc = results.get(getSessionId() + params.getID());
		}
		int totalLines = conc.getNLines() - 1;

		//System.out.println("TEST CONCORDANCE CONTEXT. Asked: "+askedPermission);
		List<Line> lines = null;
		try {
			if (conc.getNLines() == 0)
				return new ConcordanceResult("no line");

			int from = params.getStartIndex() - 1;
			int to = params.getEndIndex() - 1;

			if (from > totalLines)
				from = totalLines;
			if (to > totalLines)
				to = totalLines;
			if (from < 0)
				from = 0;
			if (to < 0)
				to = 0;

			lines = conc.getLines(from, to);
			Log.info("[ConcordanceServiceImpl.getLines] "+lines.size()+" results");
		} catch (Exception e) {
			Log.info("[ConcordanceServiceImpl.getLines] User : " + user.getLogin() + " -> Fail, " + e.getMessage());
			e.printStackTrace();
			return new ConcordanceResult(e.getMessage());
		}

		// iterates over lines to truncate them because of permissions
		boolean oneLineHasBeenTruncated = false;
		for(Line l : lines) {
			//System.out.println("Line "+l);

			String root = params.getCorpusPath().substring(1); // remove first "/"
			int idx = root.indexOf("/"); // check for next "/" if any
			if(idx > 0)
				root = root.substring(0, idx); // keep head
			root = "/"+root;
			String textPath = root + "/" + (String)l.getTextId();

			ConcordanceResultLine entry = new ConcordanceResultLine();
			ConcordancePermission perm  = user.getConcordancePermissionForPath(textPath);
			if (perm == null) {
				perm = user.getConcordancePermissionForPath(params.getCorpusPath());
				//System.out.println("TEST CONCORDANCE CONTEXT. Corpus Can: "+perm);
			}

			if (perm != null) {
				boolean truncated = false;
				int rightContext = perm.getRightContext();
				int leftContext = perm.getLeftContext();
				if (rightContext < l.getRightContextSize()) // the right context is too big
				{   // cut the line right context
					for(Property prop : l.getRightCtxViewProperties().keySet())
					{   // keep the start of values
						List<String> values = l.getRightCtxViewProperties().get(prop);
						l.getRightCtxViewProperties().put(prop, values.subList(0, rightContext));
					}
					truncated = true;
					oneLineHasBeenTruncated = true;
				}
				if (leftContext < l.getLeftContextSize())
				{   // cut the line left context
					for (Property prop : l.getLeftCtxViewProperties().keySet()) {
						// keep the end of values
						List<String> values = l.getLeftCtxViewProperties().get(prop);
						int size = values.size();
						l.getLeftCtxViewProperties().put(prop, values.subList(size - rightContext, size));
					}
					truncated = true;
					oneLineHasBeenTruncated = true;
				}
				entry.setIsTruncated(truncated);
			} else if (user.can(textPath, askedPermission) || user.isAdmin()) {

			} else {
				// REALLY no luck, the user really has no permission for this text !
				oneLineHasBeenTruncated = true;
				continue; // jump to next line
			}

			entry.setTextId((String)l.getTextId());
			entry.setWordIds(new ArrayList<String>(l.getKeywordIds()));
			entry.setReference(l.getViewRef().toString());
			entry.setLeftContext(l.leftContextToString());
			entry.setKeyword(l.keywordToString());
			entry.setRightContext(l.rightContextToString());

			//System.out.println("Entry: "+entry);
			result.add(entry);
		}
		
		result.total = totalLines + 1;
		result.isTruncated = oneLineHasBeenTruncated;
		
		if(oneLineHasBeenTruncated)
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "conc.getLines.truncated", params.getID());
		return result;
	}


	@Override
	/**
	 * Export a concordance as a String
	 */
	public String exportConcordance(ConcordanceParam params) {	
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return "error: not logged"; 
		}
		if (!user.can(params.getCorpusPath(), new ConcordancePermission())) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "exportConcordance", params.getID());
			Log.severe("[Concordance.getLines] no permission");
			return "error: no permission"; 
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "exportConcordance", params.getID());
		Concordance conc = results.get(getSessionId() + params.getID());

		String ID = ""+new java.util.Random().nextInt(999999999);
		File appdir = new File(this.getServletContext().getRealPath("."));
		File csvFile = new File(appdir, "csv/"+ID+".txt");

		try {
			Writer writer = new OutputStreamWriter(new FileOutputStream(csvFile), "UTF-8");
			conc.toTxt(writer, 0, conc.getNLines() - 1, "\t", "");
		} catch (Exception e) {
			e.printStackTrace();
			return "error: "+e;
		}
		//result.write("</body></html>");
		String portal_address = TxmConfig.getProperty("portal_address");
		if (portal_address == null)
			return "error: portal_address is not set";
		return portal_address+"/csv/"+ID+".txt";
	}	

	@Override
	/**
	 * Export the concordance as a String and format it like the context function
	 */
	public String exportContext(ConcordanceParam params) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return ""; 
		}
		if (!user.can(params.getCorpusPath(), new ConcordancePermission())) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "exportContext", params.getID());
			Log.severe("[Concordance.getLines] no permission");
			return ""; 
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "exportContext", params.getID());
		Concordance conc = results.get(getSessionId() + params.getID());

		StringWriter result = new StringWriter();
		try {
			conc.toTxt(result, 0, conc.getNLines() - 1, Format.CONTEXT, "\t","");
		} catch (CqiClientException e) {
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return result.toString();
	}

	@Override
	/**
	 * sort the concordance
	 */
	public boolean sortResults(ConcordanceParam params){
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return false; 
		}
		if (!user.can(params.getCorpusPath(), new ConcordancePermission())) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "sortResults", params.getID());
			Log.severe("[Concordance.getLines] no permission");
			return false; 
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "sortResults", params.getID());
		Concordance conc = results.get(getSessionId() + params.getID());
		if (conc == null)
			return false;
		Corpus corpus = conc.getCorpus();
		List<String> keys = params.getSortKeys();
		List<Boolean> dirs = params.getSortDirs();
		List<String> leftProps = params.getLeftSortProperties();
		List<String> keywordsProps = params.getKeywordSortProperties();
		List<String> rightProps = params.getRightSortProperties();

		List<LineComparator> comparators = new ArrayList<LineComparator>();
		for (int i = 0 ; i < keys.size() && i < dirs.size() ; i++) {
			String key = keys.get(i);
			boolean dir = dirs.get(i);
			LineComparator comp = standardComparators.get(key);
			if (!dir) // down arrow, then reverse
				comp = new ReverseComparator(standardComparators.get(key));
			comparators.add(comp);
		}
		CompositeComparator sorter = new CompositeComparator("default", comparators);
		try {
			ArrayList<Property> leftSortProperties = new ArrayList<Property>();
			ArrayList<Property> keywordSortProperties = new ArrayList<Property>();
			ArrayList<Property> rightSortProperties = new ArrayList<Property>();
			for (String s : leftProps) {
				leftSortProperties.add(corpus.getProperty(s.trim()));
			}
			for (String s : keywordsProps) {
				keywordSortProperties.add(corpus.getProperty(s.trim()));
			}
			for (String s : rightProps) {
				rightSortProperties.add(corpus.getProperty(s.trim()));
			}
			conc.setLeftAnalysisProperties(leftSortProperties);
			conc.setKeywordAnalysisProperties(keywordSortProperties);
			conc.setRightAnalysisProperties(rightSortProperties);
			conc.sort(sorter);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	// TODO use this service when a concordance is deleted
	@Override
	public void deleteConcordance(ConcordanceParam params) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			ActivityLog.severe(getSessionId(), getRemoteAddr(), "no user", "deleteConcordance", params.getID());
			return; 
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "deleteConcordance", params.getID());
		results.remove(getSessionId() + params.getID());
	}

	public static void freeCaches(String corpusname) {
		HashSet<String> keys = new HashSet<String>();
		keys.addAll(results.keySet());
		for (String key : keys) {
			Concordance concordance = results.get(key);
			if (concordance.getCorpus().getName().equals(corpusname)) {
				results.remove(key);
			}
		}
	}
}