package org.txm.web.server.services;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.txm.Toolbox;
import org.txm.functions.specificities.Specificites;
import org.txm.functions.specificities.SpecificitesResult;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.Part;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.utils.IOUtils;
import org.txm.utils.logger.Log;
import org.txm.web.aas.authorization.permissions.AFCPermission;
import org.txm.web.client.services.PortalService;
import org.txm.web.client.services.SpecificitiesService;
import org.txm.web.server.TxmUser;
import org.txm.web.server.statics.ActivityLog;
import org.txm.web.server.statics.Corpora;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.shared.Actions;
import org.txm.web.shared.SpecificitiesParam;
import org.txm.web.shared.SpecificitiesResultLine;

public class SpecificitiesServiceImpl extends PortalService implements SpecificitiesService {

	private static final long serialVersionUID = -4624170019213351527L;

	private static HashMap<String, Map<String, ArrayList<SpecificitiesResultLine>>> results = new HashMap<String, Map<String, ArrayList<SpecificitiesResultLine>>>();

	public static int cleanResults(String id) {
		
		int n = 0;
		HashSet<String> keys = new HashSet<String>();
		keys.addAll(results.keySet());
		for (String k : keys) {
			if (k.startsWith(id)) {
				results.remove(k);
				n++;
			}
		}
		return n;
	}

	@Override
	public boolean specificities(SpecificitiesParam params) {
		
		Log.info("[SpecificitiesServiceImpl.specificities] start. params="+ params);
		try {
			TxmUser user = SessionManager.getUser(this.getSessionId());
			SpecificitesResult res;
			if (user == null) {
				Log.severe("Session " + getSessionId() + " has expired");
				ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "specificities", params.getPath(), params.getProperty());
				return false;
			}

			if (!Toolbox.isStatEngineInitialized()) {
				Log.severe("[SpecificitiesServiceImpl.specificities]: no stat engine");
				return false;
			}

			Object object = user.askEntity(params.getPath(), Actions.SPECIFICITIES);
			if (object == null) {
				Log.severe("no permission");
				ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "specificities", params.getPath(), params.getProperty());
				return false;
			}
			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "specificities", params.getPath(), params.getProperty());
			Property prop = null;
			if (object instanceof Partition) {
				Partition partition = (Partition) Corpora.get(params.getPath());

				for (Property p : ((Corpus) partition.getCorpus()).getOrderedProperties()) {
					if (p.getName().equals(params.getProperty())) {
						prop = p;
						break;
					}
				}
				if (prop == null) {
					Log.severe("[SpecificitiesServiceImpl.specificities] the property has not been found: "+params.getProperty());
					return false;
				}
				res = Specificites.specificites(partition, prop, new ArrayList<String>(), new ArrayList<Part>(), params.getFmin(), 1000);
			} else { // subcorpus

				Subcorpus subCorpus = (Subcorpus) Corpora.get(params.getPath());
				for (Property p : subCorpus.getProperties()) {
					if (p.getName().equals(params.getProperty()))
					{
						prop = p;
						break;
					}
				}

				if (prop == null) {
					Log.severe("[SpecificitiesServiceImpl.specificities] the property has not been found: "+params.getProperty());
					return false;
				}
				res = Specificites.specificites(subCorpus.getMainCorpus(), subCorpus, prop, 1000);
			}
			//System.out.println("finalize specif result");
			Map<String, ArrayList<SpecificitiesResultLine>> result = new HashMap<String, ArrayList<SpecificitiesResultLine>>();
			//System.out.println("for each part name...");
			String[] partShortNames = res.getPartShortNames();
			for (String s : partShortNames) {
				//System.out.println(" add container for: "+s);
				result.put(s, new ArrayList<SpecificitiesResultLine>());
			}
			//System.out.println("get form freqs");
			int[] formFreq = res.getFormFrequencies();
			//System.out.println("get form part freqs");
			int[][] formPartFreq = res.getFrequency();
			//System.out.println("get form part indexes");
			double[][] indexes = res.getSpecificitesIndex();
			//System.out.println("get forms");
			String[] forms = res.getTypeNames();
			//System.out.println("get nb part");
			int nbparts = res.getNbrPart();
			//System.out.println("for each form freq");
			for (int i = 0 ; i < formPartFreq.length; i ++) {
				String form = forms[i];
				for (int j = 0; j < nbparts; j++) {
					SpecificitiesResultLine r = new SpecificitiesResultLine();
					r.setWord(form);
					r.setFrequency(formFreq[i]);
					r.setCofrequency(formPartFreq[i][j]);
					r.setSpecificity(indexes[i][j]);
					result.get(partShortNames[j]).add(r);
				}
			}
			//System.out.println("save result");
			// Save results
			results.put(getSessionId() + params.getID(), result);
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public Map<String, ArrayList<SpecificitiesResultLine>> getLines(SpecificitiesParam params) {

		Log.info("[SpecificitiesServiceImpl.getLines()] start. params=" + params);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session " + getSessionId() + " has expired");
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getLines", params.getPath(), params.getProperty());
			return new HashMap<String, ArrayList<SpecificitiesResultLine>>();
		}
		Object object = user.askEntity(params.getPath(), Actions.SPECIFICITIES);
		if (object == null) {
			Log.severe("no permission");
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getLines", params.getPath(), params.getProperty());
			return new HashMap<String, ArrayList<SpecificitiesResultLine>>();
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getLines", params.getPath(), params.getProperty());
		String mode = params.getMode();
		double threshold = params.getThreshold();
		int nbWords = params.getNbWords();
		//Map<String, ArrayList<SpecificitiesResultLine>> result = new HashMap<String, ArrayList<SpecificitiesResultLine>>();
		Map<String, ArrayList<SpecificitiesResultLine>> linesByPart = results.get(getSessionId() + params.getID());
		if (linesByPart == null) {
			Log.warning("[SpecificitiesServiceImpl.getLines()] specificities null for " + getSessionId() + params.getID());
			specificities(params);
			linesByPart = results.get(getSessionId() + params.getID());
		}
		//
		//			// preserve results according to threshold or nbWords
		//			if (mode == SpecificitiesParam.Mode.NB_WORD) {
		//				for (ArrayList<SpecificitiesResultLine> list : linesByPart.values()) {
		//					// sort
		//					Collections.sort(list);
		//					Collections.reverse(list);
		//					if (list.size() > nbWords * 2) {
		//						for (int n = list.size() - 1 - nbWords; n >= nbWords; n--) {
		//							list.remove(n);
		//						}
		//					}
		//				}
		//			} else if (mode == SpecificitiesParam.Mode.THRESHOLD) {
		//				if (threshold != 0) {
		//					for (ArrayList<SpecificitiesResultLine> list : linesByPart.values()) {
		//						for (int s = list.size() - 1; s >= 0; s--) {
		//							SpecificitiesResultLine sp = list.get(s);
		//							if (sp.getSpecificity() < threshold
		//									&& sp.getSpecificity() > (threshold * -1)) {
		//								list.remove(s);
		//							}
		//						}
		//					}
		//				}
		//			}
		HashMap<String, ArrayList<SpecificitiesResultLine>> result = new HashMap<String, ArrayList<SpecificitiesResultLine>>();
		for (String part : linesByPart.keySet()) {
			
			ArrayList<SpecificitiesResultLine> words = new ArrayList<SpecificitiesResultLine>();
			ArrayList<SpecificitiesResultLine> allwords = linesByPart.get(part);
			//sort all words
			Collections.sort(allwords, new Comparator<SpecificitiesResultLine>() {
				@Override
				public int compare(SpecificitiesResultLine o1,
						SpecificitiesResultLine o2) {
					//System.out.println("comp : o1 <=> o2 = "+o1.getSpecificity()+" <=> "+o2.getSpecificity() +" = "+(int) (o1.getSpecificity() - o2.getSpecificity()));
					return Double.compare(o1.getSpecificity(), o2.getSpecificity());
				}
			});

			//get the 2x nbwords
			for (int i = 0 ; i < nbWords ; i++) {
				words.add(allwords.get(i));
			}
			int size = allwords.size() - 1;
			for(int i = 0 ; i < nbWords ; i++) {
				words.add(allwords.get(size - i));
			}

			//store
			result.put(part, words);
		}
		return result;
	}

	@Override
	public String exportSpecificities(SpecificitiesParam params) {
		
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session " + getSessionId() + " has expired");
			return "empty.html";
		}

		if (!user.can(params.getPath(), new AFCPermission())) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "exportSpecif", params.getPath(), params.getID());
			return "empty.html";
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "exportSpecif", params.getPath(), params.getID());

		String ID = ""+new java.util.Random().nextInt(999999999);
		File appdir = new File(this.getServletContext().getRealPath("."));

		File txtFile = new File(appdir, "csv/Specificities_"+ID+".txt");
		txtFile.getParentFile().mkdirs();

		Map<String, ArrayList<SpecificitiesResultLine>> specifs = results.get(getSessionId() + params.getID());

		HashSet<String> unitsSet = new HashSet<String>();
		String columns = "Unit\tF";
		for (String specif : specifs.keySet()) {
			for (SpecificitiesResultLine line : specifs.get(specif)) {
				unitsSet.add(line.getWord());
			}
			columns +="\t"+specif+"\tscore";
		}
		ArrayList<String> units = new ArrayList<String>(unitsSet);
		Collections.sort(units);
		
		HashMap<String, ArrayList<SpecificitiesResultLine>> allLines = new HashMap<String, ArrayList<SpecificitiesResultLine>>();
		for (String unit : units) {
			allLines.put(unit, new ArrayList<SpecificitiesResultLine>());
		}

		for (String specif : specifs.keySet()) {
			for (SpecificitiesResultLine line : specifs.get(specif)) {
				allLines.get(line.getWord()).add(line);
			}
		}

		PrintWriter writer;
		try {
			writer = IOUtils.getWriter(txtFile, "UTF-8");

			writer.println(columns);

			for (String unit : units) {
				int F = 0;
				String fAndIndexes = "";
				for (SpecificitiesResultLine line : allLines.get(unit)) {
					fAndIndexes += "\t"+line.getCofrequency()+"\t"+line.getSpecificity();
					F = (int)line.getFrequency();
				}
				writer.println(unit+"\t"+F+fAndIndexes);
			}
			writer.println();

			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
			return "error: "+e.getMessage();
		}

		return "csv/"+txtFile.getName();
	}

}
