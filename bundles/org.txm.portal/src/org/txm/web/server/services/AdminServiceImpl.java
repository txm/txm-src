package org.txm.web.server.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.txm.Toolbox;
import org.txm.objects.Base;
import org.txm.objects.Project;
import org.txm.objects.Workspace;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.DeleteDir;
import org.txm.utils.logger.Log;
import org.txm.web.aas.authorization.Profile;
import org.txm.web.client.services.AdminService;
import org.txm.web.client.services.PortalService;
import org.txm.web.server.Emailer;
import org.txm.web.server.TxmUser;
import org.txm.web.server.statics.ActivityLog;
import org.txm.web.server.statics.Corpora;
import org.txm.web.server.statics.Profiles;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.server.statics.StringFormat;
import org.txm.web.server.statics.TxmConfig;
import org.txm.web.server.statics.UserManager;
import org.txm.web.server.statics.UserTools;
import org.txm.web.shared.ServerState;
import org.txm.web.shared.UserData;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * the services for the admin's tools
 * @author vchabanis, mdecorde
 *
 */
public class AdminServiceImpl extends PortalService implements AdminService {

	private static final long serialVersionUID = 5376447030048122162L;
	public static boolean maintenance = false;

	public static boolean isAdmin(TxmUser user)
	{
		if (!user.getLogin().equals("admin")) {
			Log.severe("[AdminServiceImpl:isAdmin] User is not admin");
			return false; 
		}
		return true;
	}

	/**
	 * Loads a binary corpus in the toolbox.
	 */
	public static Boolean _loadBinaryCorpus(TxmUser user, String corpusDir) {
		final File corpusDirFile = new File(corpusDir);
		try	{
			if (!(corpusDirFile.exists() && corpusDirFile.isDirectory())) {
				Log.severe("[AdminService.loadBinaryCorpus] BaseDir : " + corpusDir + " -> Fail, invalid path");
				return false;
			}
			//String registrypath = Toolbox.getParam(Toolbox.CQI_SERVER_PATH_TO_REGISTRY);
			if (Toolbox.workspace == null || Toolbox.workspace.getProject("default") == null) {
				Log.info("[AdminService.loadBinaryCorpus] No workspace found, creating one in : " + Toolbox.getParam(Toolbox.WORKSPACE_LOCATION));
				Toolbox.workspace = Workspace.createEmptyWorkspace(new File(Toolbox.getParam(Toolbox.WORKSPACE_LOCATION)));
				//	Toolbox.workspace.save();
			}
			Workspace w = Toolbox.workspace;
			Project p = w.getProject("default");
			if (p == null) {
				Log.severe("[AdminService.loadBinaryCorpus] project is null");
				return false;
			}
			Base base = p.scanDirectory(corpusDirFile);
			if (base == null) {
				Log.warning("[AdminService.loadBinaryCorpus] BaseDir : " + corpusDir + " -> Fail, invalid directory format");
				throw new Exception("BaseDir : " + corpusDir + " -> Fail, invalid directory format");
			}
			//			File regDir = new File(basedir,"registry");
			//			File[] regfiles = regDir.listFiles();
			//			if(regfiles == null || regfiles.length == 0)
			//			{
			//				Log.severe("[AdminService.loadBase] registry dir is empty: "+regDir);
			//				return false;
			//			}
			//			File registryfiletopatch = regfiles[0];
			//			File cwbindex = new File(basedir,"data");
			//
			//			PatchCwbRegistry.patch(registryfiletopatch, cwbindex);
			//			FileCopy.copy(registryfiletopatch, new File(registrypath, registryfiletopatch.getName()));
			//
			//			if(!base.loadImportInfos(new File(basedir, "import.xml")))
			//			{
			//				Log.severe("[AdminService.loadBase] failed to load infos from: "+new File(basedir, "import.xml"));
			//			}

			if (!Toolbox.workspace.save()) {
				Log.severe("[AdminService.loadBinaryCorpus] failed to save workspace");
				return false;
			}
			Toolbox.shutdownSearchEngine();
			Thread.sleep(100); // test
			Toolbox.restartSearchEngine();
			Toolbox.restartWorkspace();
			Corpora.restart();
			if (!Toolbox.isInitialized()) {
				Log.severe("[AdminService.loadBinaryCorpus] Toolbox is not ready");
				return false;
			}

			//free All caches
			for (String corpusname : base.getCorpora().keySet()) {
				Log.info("[AdminService.loadBinaryCorpus]free caches with corpus "+corpusname);
				ConcordanceServiceImpl.freeCaches(corpusname);
				CooccurrenceServiceImpl.freeCaches(corpusname);
				VocabularyServiceImpl.freeCaches(corpusname);
				TextSelectionServiceImpl.freeCaches(corpusname);
				ReferencerServiceImpl.freeCaches(corpusname);
			}
			if (user.isAdmin())
				user.setAllPermission();
			return true;
		} catch(Exception e) {
			Log.severe("[AdminService.loadBinaryCorpus] error: "+e);
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Service: Loads a binary corpus in the toolbox.
	 * @param the binary corpus absolute path on the server
	 * @return true if success
	 * TODO: check if the RCP do the same things
	 */
	@Override
	public Boolean loadBinaryCorpus(String corpusDir) {
		Log.info("[AdminService.loadBinaryCorpus] start. corpusDir="+corpusDir);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return false; 
		}
		if (!isAdmin(user))
			return false;

		return _loadBinaryCorpus(user, corpusDir);
	}

	@Override
	/**
	 * Service:
	 * @return all the profiles names
	 */
	public List<String> listProfiles() {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return new ArrayList<String>(); 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "listProfiles");
			return new ArrayList<String>(); 
		}
		Log.info("[AdminService.listProfiles] start");
		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "listProfiles");
		//		try {
		//			return listDir(TxmConfig.getProperty("profileBase"), ".xml");
		//		} catch (Exception e){
		//			Log.severe("[AdminService.listProfiles] error: "+e);
		//			return new ArrayList<String>(); 
		//		}
		ArrayList<String> profiles = new ArrayList<String>();
		profiles.addAll(Profiles.getProfiles().keySet());
		return profiles;
	}

	/**
	 * Service:
	 * @return the content of the profile description as a String
	 */
	@Override
	public String getProfileContent(String profileName){
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return ""; 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getProfileContent");
			return "";
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "getProfileContent");
		Log.info("[AdminService.getProfileContent] start. profileName="+profileName);
		try {
			return getFileContent(TxmConfig.getProperty("profileBase", "profile_base_not_set"), profileName+".xml");
		} catch (Exception e) {
			Log.severe("[AdminService.getProfileContent] error: "+e);
			return ""; 
		}
	}

	/**
	 * Service: update the content of the profile description
	 * return true if success
	 */
	@Override
	public String updateProfileContent(String content, String profileName){
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return "No session, reload your page"; 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "updateProfileContent");
			return "User is not admin";
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "updateProfileContent");
		Log.info("[AdminService.updateProfileContent] start. filePath="+profileName+", content="+content);

		String error = Profiles.validateProfile(content);
		if (error != null) {
			Log.severe("[AdminService.updateProfileContent] XML profile is not valid.");
			return "XML content is not well formed: "+error;
		}

		try {
			if (setFileContent(content, new File(TxmConfig.getProperty("profileBase"), profileName+".xml"), false)) {
				Profile profile = Profiles.getProfile(profileName);
				if (profile == null) {
					profile = Profiles.addProfile(profileName, new Profile());
					profile.setName(profileName);
				}
				// reload profile from new content
				if (!profile.load(profileName+".xml")) {
					Log.severe("[AdminService.updateProfileContent] profile error while loading");
					return "Profile loading error";
				}
				return "";
			} else {
				Log.severe("[AdminService.updateProfileContent] failed to replace xml content");
				return "Error: Could not save content on server";
			}
		} catch (IOException e) {
			Log.severe("[AdminService.updateProfileContent] error: "+e);
			e.printStackTrace();
			return "Execution error: "+e;
		}
	}

	@Override
	/**
	 * @return the names of the users description files
	 */
	public List<String> listUsers() {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return new ArrayList<String>(); 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "listUsers");
			return new ArrayList<String>();
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "listUsers");
		Log.info("[AdminService.listUsers] start");
		//return listDir(TxmConfig.getProperty("userBase"), ".usr");
		try {
			return UserManager.getAllUsers();
		} catch (Exception e) {
			Log.severe("[AdminService.listUsers] error: "+e);
			return new ArrayList<String>();
		}
	}

	@Override
	public boolean addProfileToUser(String userName, String profileName, Boolean sendmail) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "addProfileToUser");
			Log.severe("[AdminService.addProfileToUser] Session "+this.getSessionId()+" has expired");
			return false; 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "addProfileToUser");
			return false;
		}

		if (userName == null || userName.length() == 0) {
			ActivityLog.warning(getSessionId(), getRemoteAddr(), "admin", "addProfileToUser");
			Log.severe("[AdminService.addProfileToUser]: username is empty");
			return false;
		}

		if (profileName == null || profileName.length() == 0) {
			ActivityLog.warning(getSessionId(), getRemoteAddr(), "admin", "addProfileToUser");
			Log.severe("[AdminService.addProfileToUser]: profile is empty");
			return false;
		}

		if (sendmail == null)
			sendmail = false;

		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "addProfileToUser", userName);
		Log.info("[AdminService.addProfileToUser] start. userName="+userName+", profileName="+profileName);
		TxmUser userToUpdate = UserManager.getUser(userName);
		Profile profile = Profiles.getProfile(profileName);
		System.out.println("Profile: "+profile);
		if (profile == null) {
			ActivityLog.warning(getSessionId(), getRemoteAddr(), "admin", "addProfileToUser");
			return false;
		}
		if (userToUpdate.setProfiles(profileName) && userToUpdate.save()) {
			if (sendmail)
				return sendProfileChangeMail(userToUpdate, profile);
			else
				return true;
		} else {
			ActivityLog.warning(getSessionId(), getRemoteAddr(), "admin", "addProfileToUser");
			return false;
		}
	}

	private boolean sendProfileChangeMail(TxmUser user, Profile profile)
	{
		Log.info("[AdminService.sendConfirmationEmail] start");
		String portal = TxmConfig.getProperty("portal_name");
//		String host = this.getThreadLocalRequest().getRequestURL().toString();
//		if (host == null  || host.length() == 0) {
//			Log.severe("[sendConfirmationEmail] no host found !");
//			return false;
//		} else {
//			int idx = host.indexOf("?");
//			if (idx > 0)
//				host = host.substring(0, idx); // remove http parameters
//			host = host.substring(0, host.length()-13);//remove service name
//		}
		String host = TxmConfig.getProperty("portal_address");
		
		String link = host+"?login="+user.getLogin();
		String content = StringFormat.format(profile.getAssignMail(), user.getLogin(), profile.getNameWithoutExtension(), link);
		System.out.println("profil = "+profile.getName()+ " - "+profile.getNameWithoutExtension());
		System.out.println("subject = "+profile.getAssignSubject());
		System.out.println("content = "+profile.getAssignMail());
		Log.info("[AdminService.sendConfirmationEmail] assign profile '"+profile.getName()+"' sending link: "+link);
		return Emailer.sendMail(user.getMail(), profile.getAssignSubject(), content);
	}

	@Override
	/**
	 * Service: remove a profile from an user description file
	 */
	public boolean removeProfileFromUser(String userName, String profileName) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return false; 
		}
		if (!isAdmin(user))
			return false;

		Log.info("[AdminService.removeProfileFromUser] start. userName="+userName+", profileName="+profileName);
		TxmUser usertoUpdate = UserManager.getUser(userName);
		Profile profile = Profiles.getProfile(profileName);
		if (profile == null) {
			Log.severe("[AdminService.removeProfileFromUser] profile not found: "+profileName);
			return false;
		}
		try {
			usertoUpdate.removeProfile(profileName);
			usertoUpdate.save();
			return true;
		} catch (Exception e){
			Log.severe("[AdminService.removeProfileFromUser] error: "+e);
			return false;
		}
	}

	@Override
	public boolean deleteProfiles(String[] names) {
		Log.info("[AdminService.deleteProfiles] start. names="+Arrays.asList(names));
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return false; 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "deleteProfiles");
			return false;
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "deleteProfiles", names);

		for (String name : names) {
			File profile = new File(TxmConfig.getProperty("profileBase", "profile_base_not_set"), name+".xml");
			profile.delete();
			Profiles.remove(name);
		}
		return true;
	}

	@Override
	/**
	 * Service: creates an empty profile description
	 * return true if succes
	 */
	public boolean createProfile(String name) {
		Log.info("[AdminService.createProfile] start. name="+name);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return false; 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "createProfile");
			return false;
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "createProfile", name);

		File profile = new File(TxmConfig.getProperty("profileBase", "profile_base_not_set"), name+".xml");
		if (!profile.getParentFile().exists()) {
			Log.severe("[AdminService.createProfile] Could not create profile : parent dir does not exists :"+profile.getParent());
			return false;
		}
		if (profile.exists()) {
			Log.severe("[AdminService.createProfile] Could not create profile : profile already exists :"+profile);
			return false;
		}
		try {
			Profiles.addProfile(name, new Profile());

			//write minimal profile definition
			FileWriter writer = new FileWriter(profile);
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?><profile name=\""+name+"\" homepage=\"\" helppage=\"\" contactpage=\"\"><messages><message subject=\"\"></message></messages></profile>");
			writer.close();

			return true;
		} catch (Exception e) {
			Log.severe("[AdminService.createProfile] error: "+e);
			e.printStackTrace();
		}
		return false;
	}

	@Override
	/**
	 * Service:
	 * @return the profiles of a user
	 */
	public List<String> getUsersProfiles(String userName) {
		Log.info("[AdminService.getUsersProfiles] start. userName="+userName);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("[AdminService.getUsersProfiles] Session "+this.getSessionId()+" has expired");
			return new ArrayList<String>(); 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(), user.getLogin(), "getUsersProfiles");
			return new ArrayList<String>();
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "getUsersProfiles", userName);
		try {
			TxmUser user2 = UserManager.getUser(userName);
			if (user2 == null) {
				Log.severe("[AdminService.getUsersProfiles] getUsersProfiles failed : Can't find user "+userName);
				return new ArrayList<String>();
			}
			List<String> result = new ArrayList<String>();
			result.addAll(user2.getProfiles());
			return result;
		} catch (Exception e){
			Log.severe("[AdminService.getUsersProfiles] error: "+e);
			return new ArrayList<String>();
		}
	}

	@Override
	/**
	 * Service: deletes users description file
	 * @return true if succes
	 */
	public boolean removeUser(String[] userNames) {
		Log.info("[AdminService.removeUser] start. userNames="+Arrays.asList(userNames));
		boolean ret = true;
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return false; 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "removeUser");
			return false;
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "removeUser", userNames);
		for (String userName : userNames) {
			TxmUser userToRemove = UserManager.getUser(userName);
			AuthenticationServiceImpl.getLoginModule().delete(userName);

			if(!UserManager.remove(userToRemove))
				ret = false;
		}
		return ret;
	}

	private List<String> listDir(String dirPath, String extension) {
		ArrayList<String> result = new ArrayList<String>();
		try{
			File profileDir = new File(dirPath);
			for (String file : profileDir.list()) {
				if (file.endsWith(extension))
					result.add(file);
			}
		}catch(Exception e){Log.severe("Error while listing "+dirPath+": "+e);}
		return result;
	}

	/**
	 * 
	 * @param filePath the file to read
	 * @return the content of the pointed file
	 */
	protected String getFileContent(String filePath, String filename) {
		File file = new File(filePath, filename);
		System.out.println("AdminService.getFileContent: "+file);
		
		if (!file.exists())
			return null;
		String result = null;
		InputStream is = null;
		byte[] buffer = new byte[(int)file.length()];
		try {
			is = new FileInputStream(file);
			is.read(buffer);
			is.close();
			result = new String(buffer, "UTF-8");
		} catch (IOException e) {
			Log.severe("Error while getting fiel content "+filePath+": "+e);
			e.printStackTrace();
		}
		return result;
	}

	private boolean setFileContent(String content, File file, boolean create) throws IOException {
		System.out.println("Updating content of "+file+" ("+content.length()+" chars).");
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return false; 
		}
		if (!isAdmin(user)) {
			Log.severe("Session "+this.getSessionId()+" is not admin");
			return false;
		}

		//File file = new File(filePath, filename);
		if (!file.exists()) {
			if (create)
				file.createNewFile();
			else
				return false;
		}
		
		Writer out = new FileWriter(file);
		out.write(content);
		out.close();
		return true;
	}

	@Override
	/**
	 * Admin tool to create or update user's data
	 * @return true if success
	 */
	public boolean addUpdateUser(UserData data) {
		Log.info("[AdminService.addUpdateUser] start");
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null)
		{
			Log.severe("[AdminService.addUpdateUser] Session "+this.getSessionId()+" has expired");
			return false; 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "addUpdateUser");
			return false;
		}

		// test mandatory fields
		if (data.login == null || data.mail == null || data.password == null) {
			ActivityLog.warning(getSessionId(), getRemoteAddr(),user.getLogin(), "addUpdateUser");
			Log.severe("[AdminService.addUpdateUser] Can't update user with login="+data.login+" mail="+data.mail+" passw null? "+(data.password == null));
			return false;
		}

		// set default values if not set
		if (data.firstname == null)
			data.firstname = "";
		if (data.lastname == null)
			data.lastname = "";
		if (data.phone == null)
			data.phone = "";
		if (data.institution == null)
			data.institution = "";
		if (data.status == null)
			data.status = "";

		TxmUser userToUpdate = null;

		for (TxmUser tmpUser : AuthenticationServiceImpl.toBeActivated.keySet()) {
			if (tmpUser.getLogin().equals(data.login)) {
				userToUpdate = tmpUser;
				break;
			}
		}

		if (userToUpdate != null) { // this use is not activated, return fail
			Log.warning("[AdminService.addUpdateUser] cannot update non-activated user : " + data.login);
			ActivityLog.warning(getSessionId(), getRemoteAddr(),user.getLogin(), "addUpdateUser", "non-activated user");
			return false;
		}

		userToUpdate = (TxmUser) UserManager.getUser(data.login);
		if (userToUpdate == null) { // create user if it doesn't exists
			userToUpdate = new TxmUser();
			userToUpdate.setLogin(data.login);	
			Log.info("[AdminService.addUpdateUser] create user : " + data.login);
		} else {
			Log.info("[AdminService.addUpdateUser] update user : " + data.login);
		}

		userToUpdate.setMail(data.mail);
		userToUpdate.setNames(data.firstname, data.lastname);
		userToUpdate.setInstitution(data.institution);
		userToUpdate.setStatus(data.status);
		userToUpdate.setPassword(data.password);
		userToUpdate.setPhone(data.phone);

		if (!UserManager.create(userToUpdate)) // save user and add in Users set
		{
			ActivityLog.warning(getSessionId(), getRemoteAddr(),user.getLogin(), "addUpdateUser");
			Log.info("[AdminService.addUpdateUser] failed to create user : " + userToUpdate);
			return false;
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "addUpdateUser", data.login, data.mail);
		return true;
	}

	@Override
	/**
	 * Service: export the users as XML
	 */
	public String exportUsers() {
		Log.info("[AdminService.exportUsers] start");
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return ""; 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "exportUsers");
			return "";
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "exportUsers");
		try {
			return UserTools.exportUsers();
		} catch (Exception e){
			Log.severe("[AdminService.exportUsers] error: "+e);
			return "";
		}
	}

	@Override
	/**
	 * Service: import users from XML data
	 * @return true if successful
	 */
	public boolean importUsers(String xmlcontent) {
		Log.info("[AdminService.importUsers] start. xmlcontent="+xmlcontent);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return false; 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "importUsers");
			return false;
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "importUsers");
		try {
			return UserTools.importUsers(xmlcontent);
		} catch (Exception e){
			Log.severe("[AdminService.importUsers] error: "+e);
			return false;
		}
	}

	@Override
	public boolean removeBinaryCorpus(String basename) {
		Log.info("[AdminService.removeBinaryCorpus] start: "+basename);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return false; 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "removeBinaryCorpus");
			return false;
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "removeBinaryCorpus", basename);
		
		Project p = Toolbox.workspace.getProject("default");
		if (p == null)
			return false;

		if (!p.getBaseNames().contains(basename))
			return false;

		Base base = p.getBase(basename);
		if (base == null)
			return false;

		try {
			System.out.println("remove base "+base+" from project "+p);
			base.getProject().removeBase(base.getName());
			
			//remove Maincorpus as well
			System.out.println("delete its corpora");
			for (MainCorpus c : base.getCorpora().values()) {
				System.out.println("remove "+c+" from CorpusManager");
				CorpusManager.getCorpusManager().getCorpora().remove(c);
				
				System.out.println("remove /"+c.getName()+" from Corpora");
				Corpora.remove("/"+c.getName());
				
				String regfile = c.getName().toLowerCase(); //$NON-NLS-1$ //$NON-NLS-2$
				String registryDirectory = Toolbox.getParam(Toolbox.CQI_SERVER_PATH_TO_REGISTRY);
				new File(registryDirectory, regfile).delete();
				System.out.println("delete registry "+new File(registryDirectory, regfile));
				if (new File(registryDirectory, regfile).exists()) {
					return false;
				}
			}
			
			Project project = Toolbox.workspace.getProject("default");
			project.removeBase(base.getName());
			project.removeChildren(base);
			
			DeleteDir.deleteDirectory(base.getBaseDirectory());
			
		} catch (Exception e) {
			Log.severe("[AdminService.removeBinaryCorpus] error: "+e);
			e.printStackTrace();
			return false;
		}

		if (!Toolbox.workspace.save())
			return false;

		Toolbox.restartSearchEngine();
		Toolbox.restartWorkspace();
		Corpora.restart();

		return true;
	}

	@Override
	public boolean setMaintenance(boolean state) {
		Log.info("[AdminService.setMaintenance] set maintenance: "+state);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return false; 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "setMaintenance");
			return false;
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),getRemoteAddr(), "admin", "setMaintenance", state);
		AdminServiceImpl.maintenance = state;
		return state;
	}

	public static boolean isMaintenance() {
		return AdminServiceImpl.maintenance;
	}

	@Override
	public String sendMail(String users, String profiles, String subject, String content) {
		Log.info("[AdminService.sendMail] send mail: "+subject);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return ""; 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "sendMail");
			return "";
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "sendMail", subject, users, profiles);

		List<String> userlist = new ArrayList<String>();
		List<String> profilelist = new ArrayList<String>();
		if (users != null)
			userlist = Arrays.asList(users.split(","));
		if (profiles != null)
			profilelist = Arrays.asList(profiles.split(","));

		String failedmail = "";

		//for(String user : listDir(TxmConfig.getProperty("userBase"), ".usr"))
		for(String userToContact : UserManager.getAllUsers()) {
			boolean doit = false;
			TxmUser txmuser = UserManager.getUser(userToContact);
			if (txmuser == null) {
				System.out.println("sendMail: Error: can't find user : "+userToContact);
				continue;
			}
			if (userlist.contains(userToContact))
				doit = true;

			if (!doit)
				for (String profile : txmuser.getProfiles()) {
					if (profilelist.contains(profile)) {
						doit = true;
						break;
					}
				}
			if (doit) {
				String to = txmuser.getMail();
				//System.out.println("send mail to "+txmuser.getLogin());
				if (!Emailer.sendMail(to, subject, content))
					failedmail += to+"<br/>";
			}
		}
		return failedmail;
	}

	@Override
	public boolean restartToolbox() {
		Log.info("[AdminService.restartToolbox] start");
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return false; 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "restartToolbox");
			return false;
		}

		if(AdminServiceImpl.maintenance) {
			try {
				ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "restartToolbox");
				Toolbox.shutdown();
				return HelloServiceImpl.startToolbox();
			} catch (Exception e) {
				Log.severe("[AdminService.restartToolbox] error: "+e);
				return false;
			}
		}
		else
			return false;
	}

	@Override
	public ServerState getServerState() {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return null; 
		}
		if (!isAdmin(user)) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getServerState");
			return null;
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "getServerState", maintenance);
		ServerState infos = new ServerState();
		infos.maintenance = maintenance;
		infos.mail = "true".equals(TxmConfig.getProperty("nomail"));
		infos.inscription = "true".equals(TxmConfig.getProperty("noinscription"));
		infos.contact = TxmConfig.getProperty("mail.contact", "no_contact_mail");	
		ArrayList<String> users = new ArrayList<String>();
		for (TxmUser loggeduser : SessionManager.getUserMap().values())
			users.add(loggeduser.getLogin());
				infos.currentUsers = users;
				infos.toolbox = Toolbox.isInitialized();

				return infos;
	}

	@Override
	public boolean setInscriptionEnable(boolean state) {
		TxmConfig.setProperty("noinscription", ""+state);
		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "setInscriptionEnable", state);
		return "true".equals(TxmConfig.getProperty("noinscription"));

	}

	@Override
	public boolean setMailEnable(boolean state) {
		TxmConfig.setProperty("nomail", ""+state);
		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "setMailEnable", state);
		return "true".equals(TxmConfig.getProperty("nomail"));
	}

	@Override
	public boolean quickSend(String message) {
		HelloServiceImpl.updateNews(message);
		return true;
	}

	@Override
	public ArrayList<ArrayList<String>> getLastLogs(int size) {
		Log.info("[AdminService.getLastLogs] size: "+size);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return new ArrayList<ArrayList<String>>(); 
		}
		if(!isAdmin(user)){
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getServerState");
			return new ArrayList<ArrayList<String>>(); 
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),"admin", "getLogs");
		
		if ("true".equals(TxmConfig.getProperty("log_activity"))) {
			return ActivityLog.getLastLogs(size);
		} else {
			ArrayList<ArrayList<String>> rez = new ArrayList<ArrayList<String>>();
			rez.add(new ArrayList<String>(Arrays.asList("log_activity","portal","parameter","is","not","set", "in", "txmweb.conf")));
			return rez;
		}
	}

	@Override
	public boolean setRemoteMaintenance(String code, boolean state, String message) {
		Log.info("[AdminService.setRemoteMaintenance] set maintenance: "+state);
		ActivityLog.add(getSessionId(), getRemoteAddr(),getRemoteAddr(), "admin", "setMaintenance", state);
		AdminServiceImpl.maintenance = state;
		quickSend(message);
		return state;
	}

	@Override
	public List<String> dumpUsers() {
		Log.info("[AdminService.dumpUsers]");
		ArrayList<String> usersStr = new ArrayList<String>();
		for (TxmUser user : UserManager.getUsers()) {
			usersStr.add(user.toString());
		}
		return usersStr;
	}

	@Override
	public List<String> dumpProfiles() {
		Log.info("[AdminService.dumpProfiles]");
		ArrayList<String> profilsStr = new ArrayList<String>();
		for (Profile profil : Profiles.getProfiles().values()) {
			profilsStr.add(profil.toString());
		}
		return profilsStr;
	}

	@Override
	public boolean reloadServerParameters() {
		Log.info("[AdminService.reloadServerParameters]");
		return TxmConfig.reset();
	}

	@Override
	public ArrayList<String> listCorporaDirectory() {
		Log.info("[AdminService.listCorporaDirectory]");
		ArrayList<String> result = new ArrayList<String>();
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			result.add("session expired");
			return result; 
		}
		if (!isAdmin(user)){
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getServerState");
			result.add("you are not administrator");
			return result;
		}
		
		String txmhome = Toolbox.getParam(Toolbox.USER_TXM_HOME);
		File corporaDirectory = new File(txmhome, "corpora");
		if (corporaDirectory.exists()) {
			for (File f : corporaDirectory.listFiles()) {
				if (f.isDirectory()) {
					File[] files = f.listFiles();
					if (files != null && files.length > 0)
					for (File sf : files) {
						if ("import.xml".equals(sf.getName())) {
							result.add(f.getAbsolutePath() + " timestamp="+f.lastModified());
							break;
						}
					}
				}
			}
		}
		
		Collections.sort(result);
		return result;
	}

	@Override
	public String getTXMWEBCONF() {
		Log.info("[AdminService.getTXMWEBCONF]");
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("AdminService.getTXMWEBCONF: Session "+this.getSessionId()+" has expired");
			return null; 
		}
		if(!isAdmin(user)){
			Log.severe("AdminService.getTXMWEBCONF: User is not admin");
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getServerState");
			return null;
		}
		
		File txmwebconf = TxmConfig.getConfigFile();
		return getFileContent(txmwebconf.getParent(), txmwebconf.getName());
	}

	@Override
	public boolean saveTXMWEBCONF(String content) {
		Log.info("[AdminService.saveTXMWEBCONF] "+content);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+this.getSessionId()+" has expired");
			return false; 
		}
		if(!isAdmin(user)){
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getServerState");
			return false;
		}
		
		File txmwebconf = TxmConfig.getConfigFile();
		try {
			return setFileContent(content, txmwebconf, false);
		} catch (IOException e) {
			Log.printStackTrace(e);
			return false;
		}
	}
}
