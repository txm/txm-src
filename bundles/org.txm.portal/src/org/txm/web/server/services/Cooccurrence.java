// Copyright � 2010-2013 ENS de Lyon.
package org.txm.web.server.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.rosuda.REngine.REXPMismatchException;
import org.txm.Messages;
import org.txm.functions.Function;
import org.txm.functions.TXMResult;
import org.txm.functions.concordances.Concordance;
import org.txm.functions.concordances.Line;
import org.txm.functions.cooccurrences.comparators.CLineComparator;
import org.txm.functions.specificities.Specificites;
import org.txm.functions.specificities.SpecificitesResult;
import org.txm.functions.vocabulary.Vocabulary;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.corpus.query.Query;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.stat.StatException;
import org.txm.stat.data.LexicalTable;
import org.txm.stat.engine.r.RWorkspace;
import org.txm.stat.engine.r.RWorkspaceException;
import org.txm.stat.engine.r.data.LexicalTableImpl;
import org.txm.utils.logger.Log;

/**
 * Compute a coocurrence from a concordance.
 *
 * @author mdecorde
 */
public class Cooccurrence extends Function implements TXMResult {

	/**
	 * The Class CLine.
	 */
	public class CLine implements Comparable<CLine> {

		/** The cooc. */
		Cooccurrence cooc;

		/** The occ. */
		public String occ;

		/** The freq. */
		public int freq;

		/** The nbocc. */
		public int nbocc;

		/** The score. */
		public double score;

		/** The distmoyenne. */
		public float distmoyenne;

		/** The mode. */
		public long mode;

		/** The props. */
		public List<String> props;

		/** The id. */
		public int id;

		/**
		 * Instantiates a new c line.
		 *
		 * @param cooc the cooc
		 * @param occ the occ
		 * @param props the props
		 * @param nbocc the nbocc
		 * @param freq the freq
		 * @param score the score
		 * @param distmoyenne the distmoyenne
		 * @param mode the mode
		 */
		public CLine(Cooccurrence cooc, String occ, List<String> props,
				int nbocc, int freq, double score, Float distmoyenne, long mode) {
			this.occ = occ;
			this.props = props;
			this.freq = freq;
			this.nbocc = nbocc;
			this.score = score;
			this.distmoyenne = distmoyenne;
			this.mode = mode;
			this.cooc = cooc;
		}

		/**
		 * Adds the txt sep.
		 *
		 * @param str the str
		 * @param sep the sep
		 * @return the string
		 */
		private String addTxtSep(String str, String sep) {
			return sep + str.replace(sep, sep + sep) + sep;
		}

		@Override
		public int compareTo(CLine o) {
			if (o == null)
				return 1;
			return (int) (100.0 * this.score - o.score);
		}

		/**
		 * Gets the cooc.
		 *
		 * @return the cooc
		 */
		public Cooccurrence getCooc() {
			return cooc;
		}

		/**
		 * Resume.
		 *
		 * @param colseparator the colseparator
		 * @param txtseparator the txtseparator
		 * @return the string
		 */
		public String resume(String colseparator, String txtseparator) {
			return addTxtSep("" + occ, txtseparator) //$NON-NLS-1$
					+ colseparator + freq + colseparator + nbocc + colseparator + score + colseparator + distmoyenne;
		}

		/**
		 * Sets the count and dist.
		 *
		 * @param count the count
		 * @param dist the dist
		 */
		public void setCountAndDist(int count, int dist) {
			this.nbocc = count;
			this.distmoyenne = dist;
		}

		/**
		 * Sets the freq.
		 *
		 * @param freq the new freq
		 */
		public void setFreq(int freq) {
			this.freq = freq;
		}

		/**
		 * Sets the score.
		 */
		public void setScore() {
			this.score = (this.nbocc + this.freq + this.distmoyenne);
		}

		@Override
		public String toString() {
			return occ + Messages.Cooccurrence_4 + freq
					+ Messages.Cooccurrence_7 + nbocc + Messages.Cooccurrence_8 + score + Messages.Cooccurrence_9 + distmoyenne + Messages.Cooccurrence_10 + props;
		}
	}

	/** The nocooc. */
	protected static int nocooc = 1;

	/** The prefix r. */
	protected static String prefixR = "Cooccurrences"; //$NON-NLS-1$



	/** The voc. */
	Vocabulary voc;

	/** The conc. */
	Concordance conc;

	/**
	 * The reference corpus to use = the R symbol that point to a matrix
	 * WordxFreqs
	 */
	String referenceCorpus;

	/** The conclines. */
	List<org.txm.functions.concordances.Line> conclines;

	/** The corpus. */
	Corpus corpus;

	/** The query. */
	Query pQuery;

	String query_occ = "[]"; //$NON-NLS-1$

	/** The properties. */
	List<Property> pProperties;

	/** The limit. */
	StructuralUnit pStructuralUnitLimit;

	/** The maxleft. */
	int pMaxLeftContextSize;

	/** The maxright. */
	int pMaxRightContextSize;

	/** The minleft. */
	int pMinLeftContextSize = 1;

	/** The minright. */
	int pMinRightContextSize = 1;

	/** The lines. */
	ArrayList<CLine> lines = new ArrayList<CLine>();

	/** The occproperties. */
	HashMap<String, List<String>> occproperties;

	/** The count. */
	HashMap<String, Integer> count;

	/** The dist. */
	HashMap<String, Float> dist;

	/** The freq. */
	HashMap<String, Integer> freq;

	/** The scores. */
	HashMap<String, Double> scores;

	/** The FA. */
	int FA = -1;
	/** The P. */
	int P = -1;
	/** The number of keyword. */
	int numberOfKeyword = 0;

	/** The seuil_freq. */
	int seuil_freq = -1;
	/** The seuil_count. */
	int seuil_count = -1;

	/** The seuil_score. */
	double seuil_score = -1;
	/** The contextquery. */
	private Query contextQuery;

	/** The minf. */
	private int pFminFilter;

	/** The minscore. */
	private double pScoreMinFilter;

	/** The mincof. */
	private int pFCoocFilter;

	/** The anticontextquery. */
	private Query anticontextquery;

	/** The include xpivot. */
	private boolean pIncludeXpivot;

	// System.out.println("Matches: focus: "+m1.size()+" full: "+m2.size()+" anti: "+m3.size());
	//System.out.println("T matches : "+(System.currentTimeMillis()- time)); //$NON-NLS-1$
	/** The distances. */
	HashMap<String, Double> distances = new HashMap<String, Double>();

	// contains the sum of distances
	/** The distancescounts. */
	HashMap<String, Integer> distancescounts = new HashMap<String, Integer>();

	// contains the sum of distances
	/** The counts. */
	HashMap<String, Integer> counts = new HashMap<String, Integer>();

	/** The indexfreqs. */
	HashMap<String, Integer> indexfreqs = new HashMap<String, Integer>();

	// contains the number of encounter
	/** The counted. */
	HashMap<Integer, Integer> counted = new HashMap<Integer, Integer>();

	/** The m1. */
	private List<Match> m1;

	// contains the list of positions already counteds
	/** The m2. */
	private List<Match> m2;

	/** The m3. */
	private List<Match> m3;

	/** The allsignaturesstr. */
	private HashMap<Integer, String> allsignaturesstr;

	/** The lt. */
	private LexicalTable lt;

	/** The keys to string. */
	private HashMap<String, String> keysToString;

	/** The symbol. */
	private String symbol;

	/** The writer. */
	private OutputStreamWriter writer;

	private boolean buildLexicalTableWithCooccurrents;

	int numberOfCooccurrents = -1;

	/**
	 * the cooc will be computed on all lines.
	 *
	 * @param conc
	 *            the conc
	 */
	public Cooccurrence(Concordance conc) {
		initConcInfos(conc);
	}

	//
	// /**
	// * build the conc and compute on all lines.
	// *
	// * @param corpus the corpus
	// * @param query the query
	// * @param properties the properties
	// * @param limit the limit
	// * @param leftlimit the leftlimit
	// * @param rightlimit the rightlimit
	// */
	// public Cooccurrence(Corpus corpus, Query query, List<Property>
	// properties,
	// StructuralUnit limit, int leftlimit, int rightlimit) {
	// try {
	// Concordance conc = new Concordance(corpus, query,
	// properties.get(0), properties,
	// new ReferencePattern(), new ReferencePattern(),
	// leftlimit, rightlimit);
	// // corpus.storeResult(conc);
	// initConcInfos(conc);
	// } catch (CqiClientException e) {
	// // TODO Auto-generated catch block
	// org.txm.utils.logger.Log.printStackTrace(e);
	// }
	//
	// }

	/**
	 * Instantiates a new cooccurrence.
	 *
	 * @param corpus
	 *            the corpus
	 * @param query
	 *            the query
	 * @param properties
	 *            the properties
	 * @param limit
	 *            the limit
	 * @param maxleft
	 *            the maxleft
	 * @param minleft
	 *            the minleft
	 * @param minright
	 *            the minright
	 * @param maxright
	 *            the maxright
	 * @param minf
	 *            the minf
	 * @param mincof
	 *            the mincof
	 * @param minscore
	 *            the minscore
	 * @param includeXpivot
	 *            the include xpivot
	 * @throws Exception
	 *             the exception
	 */
	public Cooccurrence(Corpus corpus, Query query, List<Property> properties,
			StructuralUnit limit, int maxleft, int minleft, int minright,
			int maxright, int minf, int mincof, double minscore,
			boolean includeXpivot, boolean buildLexicalTableWithCooccurrents)
			throws Exception {
		long time = System.currentTimeMillis();

		// System.out.println(Messages.Cooccurrence_0);
		this.corpus = corpus;
		this.pQuery = query;

		// System.out.println("cooc params= corpus: "+corpus+" query: "+query+" props: "+properties+" limit: "+limit+" maxleft: "+maxleft+" minleft: "+minleft+" minright: "+minright+" maxright: "+maxright);

		this.pProperties = properties;
		this.pStructuralUnitLimit = limit;
		this.pMinLeftContextSize = minleft;
		this.pMaxLeftContextSize = maxleft;
		this.pMinRightContextSize = minright;
		this.pMaxRightContextSize = maxright;
		this.pFminFilter = minf;
		this.pScoreMinFilter = minscore;
		this.pFCoocFilter = mincof;
		this.pIncludeXpivot = includeXpivot;
		this.buildLexicalTableWithCooccurrents = buildLexicalTableWithCooccurrents;

		//System.out.println("-- Done"); //$NON-NLS-1$
	}

	/**
	 * specif y exactly on which concordance lines compute the cooc.
	 *
	 * @param conclines
	 *            the conclines
	 */
	public Cooccurrence(List<Line> conclines) {
		if (conclines.size() > 0) {
			initConcInfos(conclines.get(0).getConcordance(), conclines);
		}
	}

	/**
	 * As r matrix.
	 *
	 * @return the string
	 * @throws RWorkspaceException
	 *             the r workspace exception
	 */
	public String asRMatrix() throws RWorkspaceException {
		symbol = prefixR + nocooc;

		String[] occ = new String[this.lines.size()];
		int[] freq = new int[this.lines.size()];
		int[] cofreq = new int[this.lines.size()];
		double[] score = new double[this.lines.size()];
		double[] dist = new double[this.lines.size()];

		int i = 0;
		for (CLine line : this.lines) {
			occ[i] = line.occ;
			freq[i] = line.freq;
			cofreq[i] = line.nbocc;
			score[i] = line.score;
			dist[i] = line.distmoyenne;
			i++;
		}

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		rw.addVectorToWorkspace("coococc", occ); //$NON-NLS-1$
		rw.addVectorToWorkspace("coocfreq", freq); //$NON-NLS-1$
		rw.addVectorToWorkspace("cooccofreq", cofreq); //$NON-NLS-1$
		rw.addVectorToWorkspace("coocscore", score); //$NON-NLS-1$
		rw.addVectorToWorkspace("coocmeandist", dist); //$NON-NLS-1$

		rw.eval(symbol
				+ "<- matrix(data = c(coocfreq, cooccofreq, coocscore, coocmeandist), nrow = " + this.lines.size() + ", ncol = 4)"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval("rownames(" + symbol + " ) <- coococc"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval("colnames(" + symbol + " ) <- c('freq', 'cofreq', 'score', 'dist')"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval(symbol + "<- list(data=" + symbol //$NON-NLS-1$
				// + ", leftcontext="+this.leftContextSize
				// + ", rightcontext="+this.rightContextSize
				// + ", query=\""+this.query.getQueryString()+"\""
				+ ")"); //$NON-NLS-1$

		nocooc++;
		return symbol;
	}

	/**
	 * Calcmode.
	 *
	 * @param inf
	 *            the inf
	 * @param ing
	 *            the ing
	 * @param s
	 *            the s
	 * @param cf
	 *            the cf
	 * @return the long
	 */
	long calcmode(long inf, long ing, long s, long cf) {
		return (long) Math.floor((double) ((inf + 1) * (ing + 1))
				/ (inf + ing + s + 2));
	}

	@Override
	public void clean() {
		try {
			this.writer.flush();
			this.writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	public void clearMemory() {
		distances = null;
		distancescounts = null;
		counts = null;
		indexfreqs = null;
		counted = null;
		m1 = null;
		m2 = null;
		m3 = null;
		allsignaturesstr = null;
		lt = null;
		keysToString = null;
		occproperties = null;
		count = null;
		dist = null;
		freq = null;
		scores = null;
	}

	/**
	 * Count occ.
	 *
	 * @param propsvalue
	 *            the propsvalue
	 * @param rightcontext
	 *            the rightcontext
	 */
	public void countOcc(Map<Property, List<String>> propsvalue,
			boolean rightcontext) {
		// System.out.println("countOcc (R="+rightcontext+") "+propsvalue);
		Property key = pProperties.get(0);
		List<String> iterationlist = propsvalue.get(key);
		for (int i = 0; i < iterationlist.size(); i++)// pr chq mot
		{
			// build occ
			String occ = ""; //$NON-NLS-1$
			for (Property p : pProperties)
				occ += propsvalue.get(p).get(i) + "_"; //$NON-NLS-1$
			occ = occ.substring(0, occ.length() - 1);

			if (occproperties.get(occ) == null) {
				ArrayList<String> values = new ArrayList<String>();
				for (Property p : pProperties)
					values.add(propsvalue.get(p).get(i));
				occproperties.put(occ, values);
			}

			// System.out.println("occ '"+occ+"'");
			// update nbocc
			if (count.get(occ) == null)
				count.put(occ, 0);
			count.put(occ, count.get(occ) + 1);

			// update dist
			if (dist.get(occ) == null)
				dist.put(occ, 0.0f);
			if (rightcontext) {
				dist.put(occ, dist.get(occ) + i + 1);
			} else {
				dist.put(occ, dist.get(occ) + iterationlist.size() - i);
			}

			// update freq
			if (freq.get(occ) == null) {
				// System.out.println("compute freq of "+occ);
				int occfreq = -1; // calcul avec l'index
				for (org.txm.functions.vocabulary.Line l : voc.getLines(0,
						voc.getV())) {
					if (l.toString().equals(occ)) {
						// System.out.println("FOUND "+occ);
						occfreq = l.getFrequency();
						break;
					}
				}
				freq.put(occ, occfreq);
			}
		}
	}

	@Override
	public boolean delete() {
		return this.corpus.removeResult(this);
	}

	/**
	 * Gets the corpus.
	 *
	 * @return the corpus
	 */
	public Corpus getCorpus() {
		return corpus;
	}

	/**
	 * Gets the fA.
	 *
	 * @return the fA
	 */
	public int getFA() {
		return this.FA;
	}

	public boolean getIncludeXPivot() {
		return pIncludeXpivot;
	}

	/**
	 * Gets the lines.
	 *
	 * @return the lines
	 */
	public List<CLine> getLines() {
		return lines;
	}

	/**
	 * Gets the max left.
	 *
	 * @return the max left
	 */
	public int getMaxLeft() {
		return pMaxLeftContextSize;
	}

	/**
	 * Gets the max right.
	 *
	 * @return the max right
	 */
	public int getMaxRight() {
		return pMaxRightContextSize;
	}

	/**
	 * Gets the min left.
	 *
	 * @return the min left
	 */
	public int getMinLeft() {
		return pMinLeftContextSize;
	}

	/**
	 * Gets the min right.
	 *
	 * @return the min right
	 */
	public int getMinRight() {
		return pMinRightContextSize;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return pQuery + Messages.Cooccurrence_3 + (pMaxLeftContextSize - 1)
				+ ", " + (pMaxRightContextSize - 1) + ")"; //$NON-NLS-1$ //$NON-NLS-2$ 
	}

	/**
	 * Gets the lines.
	 *
	 * @return the lines
	 */
	public int getNumberOfCooccurrents() {
		if (numberOfCooccurrents == -1) {
			numberOfCooccurrents = 0;
			for (CLine line : lines)
				numberOfCooccurrents += line.nbocc;
		}
		return numberOfCooccurrents;
	}

	/**
	 * Gets the lines.
	 *
	 * @return the lines
	 */
	public int getNumberOfDifferentCooccurrents() {
		if (lines != null)
			return lines.size();
		return 0;
	}

	/**
	 * Gets the number of keyword.
	 *
	 * @return the number of keyword
	 */
	public int getNumberOfKeyword() {
		return numberOfKeyword;
	}

	/**
	 * Gets the p.
	 *
	 * @return the p
	 */
	public int getP() {
		return P;
	}

	@Override
	public Object getParent() {
		return corpus;
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public List<Property> getProperties() {
		return pProperties;
	}

	/**
	 * Gets the query.
	 *
	 * @return the query
	 */
	public Query getQuery() {
		return pQuery;
	}

	/**
	 * Gets the structure.
	 *
	 * @return the structure
	 */
	public StructuralUnit getStructure() {
		return this.pStructuralUnitLimit;
	}

	/**
	 * Gets the symbol.
	 *
	 * @return the symbol
	 */
	public String getSymbol() {
		return this.symbol;
	}

	/**
	 * Inits the conc infos.
	 *
	 * @param conc
	 *            the conc
	 * @return true, if successful
	 */
	private boolean initConcInfos(Concordance conc) {
		int from = 0;
		int to = conc.getNLines() - 1;
		return initConcInfos(conc, from, to);
	}

	/**
	 * Inits the conc infos.
	 *
	 * @param conc
	 *            the conc
	 * @param from
	 *            the from
	 * @param to
	 *            the to
	 * @return true, if successful
	 */
	private boolean initConcInfos(Concordance conc, int from, int to) {
		try {
			this.conc = conc;
			conclines = conc.getLines(from, to);
			return initConcInfos(conc, conclines);
		} catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return false;
	}

	/**
	 * Inits the conc infos.
	 *
	 * @param conc
	 *            the conc
	 * @param conclines
	 *            the conclines
	 * @return true, if successful
	 */
	private boolean initConcInfos(Concordance conc, List<Line> conclines) {
		try {
			this.conc = conc;
			corpus = conc.getCorpus();
			this.P = corpus.getSize()
					/ (conc.getLeftContextSize() + conc.getRightContextSize());
			this.FA = conc.getNLines();
			corpus = conc.getCorpus();
			pQuery = conc.getQuery();
			// System.out.println("CONC PROPS :"+conc.getViewProperties());
			pProperties = conc.getViewProperties();
			pStructuralUnitLimit = null;
			pMaxLeftContextSize = conc.getLeftContextSize();
			pMaxRightContextSize = conc.getRightContextSize();
			this.conclines = conclines;
			return true;
		} catch (CqiClientException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return false;
	}

	/**
	 * Prints the.
	 */
	public void print() {
		// System.out.println("Lines "+lines.size());
		System.out.println("FA : " + FA); //$NON-NLS-1$
		System.out.println("P : " + P); //$NON-NLS-1$
		System.out.println(Messages.Cooccurrence_6);
		for (CLine line : lines)
			System.out.println(line.resume("\t", "")); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Process.
	 *
	 * @throws CqiClientException
	 *             the cqi client exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws CqiServerError
	 *             the cqi server error
	 * @throws StatException
	 *             the stat exception
	 */
	public void process() throws CqiClientException, IOException,
			CqiServerError, StatException {
		stepQueryLimits();
		stepGetMatches();
		stepBuildSignatures();
		stepCount();
		stepBuildLexicalTable();
		stepGetScores();
		clearMemory();
		getLines();
	}

	public void setCoocQuery(String q) {
		query_occ = q;
	}

	/**
	 * Sets the max left.
	 *
	 * @param maxleft
	 *            the new max left
	 */
	public void setMaxLeft(int maxleft) {
		this.pMaxLeftContextSize = maxleft;
	}

	/**
	 * Sets the max right.
	 *
	 * @param maxright
	 *            the new max right
	 */
	public void setMaxRight(int maxright) {
		this.pMaxRightContextSize = maxright;
	}

	/**
	 * Sets the min left.
	 *
	 * @param minleft
	 *            the new min left
	 */
	public void setMinLeft(int minleft) {
		this.pMinLeftContextSize = minleft;
	}

	/**
	 * Sets the min right.
	 *
	 * @param minright
	 *            the new min right
	 */
	public void setMinRight(int minright) {
		this.pMinRightContextSize = minright;
	}

	public void setReferenceCorpus(String symbol) {
		referenceCorpus = symbol;
	}

	/**
	 * Sets the structure.
	 *
	 * @param su
	 *            the new structure
	 */
	public void setStructure(StructuralUnit su) {
		pStructuralUnitLimit = su;
	}

	/**
	 * Sets the thresfold.
	 *
	 * @param freq
	 *            the freq
	 * @param count
	 *            the count
	 * @param score
	 *            the score
	 */
	public void setThresfold(int freq, int count, double score) {
		seuil_freq = freq;
		seuil_count = count;
		seuil_score = score;
	}

	/**
	 * Sort.
	 *
	 * @param comparator
	 *            the comparator
	 * @throws CqiClientException
	 *             the cqi client exception
	 */
	public void sort(CLineComparator comparator) throws CqiClientException {
		comparator.initialize(corpus);
		if (lines.size() > 0) {
			Collections.sort(lines);
		}
	}

	/**
	 * Step build lexical table.
	 *
	 * @return true, if successful
	 * @throws RWorkspaceException
	 *             the r workspace exception
	 */
	public boolean stepBuildLexicalTable() throws RWorkspaceException {
		String[] colnames = {
				corpus.getName() + "-" + pQuery.getQueryString(), pQuery.getQueryString() }; //$NON-NLS-1$
		keysToString = new HashMap<String, String>();

		// time = System.currentTimeMillis();
		//System.out.println("VOCABULARY"); //$NON-NLS-1$
		Vocabulary index = null;
		for (Object rez : corpus.getResults()) {// TODO: fix usages of index for
												// cooc
			if (rez instanceof Vocabulary) {
				Vocabulary rezvoc = (Vocabulary) rez;
				if (rezvoc.getProperties().equals(pProperties)) {
					if (rezvoc.getQuery()
							.equals(new Query("" + query_occ + ""))) { //$NON-NLS-1$ //$NON-NLS-2$
						if (!rezvoc.isAltered()) {
							index = rezvoc;
							break;
						}
					}
				}
			}
		}
		if (Thread.interrupted())
			return false; // stop if interrupted by user

		if (index == null) {
			if (pProperties.size() == 1 && "[]".equals(query_occ)) { //$NON-NLS-1$
				// System.out.println("build lexicon of props: "+properties);
				index = new Vocabulary(corpus, pProperties.get(0));
			} else {
				// System.out.println("build index of props: "+properties);
				try {
					index = new Vocabulary(corpus, new Query(query_occ),
							pProperties);
				} catch (Exception e) {
					Log.severe(Log.toString(e));
					return false;
				}
			}
			corpus.storeResult(index);
		}
		if (Thread.interrupted())
			return false; // stop if interrupted by user

		// ALTER THE INDEX IF A REFERENCE CORPUS IS SET
		if (referenceCorpus != null && referenceCorpus.length() > 0) {
			// voc.toTxt(new File("/home/mdecorde/TEMP/before.tsv"), "UTF-8",
			// "\t", "");
			try {
				index.setIsAltered(true);
				String[] ref_forms = RWorkspace.getRWorkspaceInstance()
						.eval("rownames(" + referenceCorpus + ")").asStrings(); //$NON-NLS-1$ //$NON-NLS-2$
				int[] ref_freqs = RWorkspace.getRWorkspaceInstance()
						.eval(referenceCorpus + "[,1]").asIntegers(); //$NON-NLS-1$
				if (ref_forms.length != ref_freqs.length) {
					System.out.println(Messages.Cooccurrence_22);
					return false;
				}
				HashMap<String, Integer> ref_counts = new HashMap<String, Integer>();
				for (int i = 0; i < ref_forms.length; i++)
					ref_counts.put(ref_forms[i], ref_freqs[i]);

				for (org.txm.functions.vocabulary.Line l : index.getAllLines()) {
					String key = l.toString();
					if (ref_counts.containsKey(key)) {
						int[] f = { ref_counts.get(key) };
						l.setCounts(f, 0);
					}
				}
				// voc.toTxt(new File("/home/mdecorde/TEMP/after.tsv"), "UTF-8",
				// "\t", "");
			} catch (REXPMismatchException e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
				return false;
			}
		}
		if (Thread.interrupted())
			return false; // stop if interrupted by user

		List<org.txm.functions.vocabulary.Line> vocLines = index.getAllLines();
		int[][] freqs;
		String[] rownames;
		if (buildLexicalTableWithCooccurrents) {
			freqs = new int[counts.keySet().size()][2];
			rownames = new String[counts.keySet().size()];
		} else { // all words
			freqs = new int[vocLines.size()][2];
			rownames = new String[vocLines.size()];
		}

		int i = 0;
		//System.out.println("T voc : "+(System.currentTimeMillis()- time)); //$NON-NLS-1$
		// System.out.println("nb lines voc "+voclines.size());
		// System.out.println("counts keys: "+counts.keySet());
		for (org.txm.functions.vocabulary.Line l : vocLines) {
			// System.out.println("L sign '"+l.getSignature()+"'");
			if (counts.keySet().contains(l.getSignature())) {
				keysToString.put(l.toString(), l.getSignature());
				rownames[i] = l.toString();
				// System.out.println("set rowname: "+l.toString());
				// System.out.println("("+l.getSignature()+", "+l.toString()+") : "+l.getFrequency()+" - "+counts.get(l.getSignature()));
				int count = counts.get(l.getSignature());
				int tot = l.getFrequency();
				indexfreqs.put(l.toString(), tot);

				freqs[i][0] = tot - count;
				freqs[i][1] = count;
				i++;
			} else if (!buildLexicalTableWithCooccurrents) {
				keysToString.put(l.toString(), l.getSignature());
				rownames[i] = l.toString();
				// System.out.println("set rowname: "+l.toString());
				// System.out.println("("+l.getSignature()+", "+l.toString()+") : "+l.getFrequency()+" - "+counts.get(l.getSignature()));

				int tot = l.getFrequency();
				indexfreqs.put(l.toString(), tot);

				freqs[i][0] = tot;
				freqs[i][1] = 0;
				i++;
			}
		}
		if (Thread.interrupted())
			return false; // stop if interrupted by user

		// time = System.currentTimeMillis();
		if (freqs.length == 0) {
			System.out.println(Messages.Cooccurrence_23);
		}
		// try {
		// PrintWriter writer =
		// IOUtils.getWriter("/home/mdecorde/test_cooc.txt");
		// //writer.println("Build LT: ");
		// //writer.println("freqs: "+Arrays.toString(freqs));
		// //writer.println("Rows: "+);
		// //String rows = Arrays.toString();
		// int nrow = rownames.length;
		// int ncol = colnames.length;
		// for (int ii = 0 ; ii < nrow ; ii++) {
		// writer.write(rownames[ii]);
		// for (int j = 0 ; j < ncol ; j++) {
		// writer.write("\t"+freqs[ii][j]);
		// }
		// writer.write("\n");
		// }
		// writer.close();
		// //writer.println("Cols: "+Arrays.toString(colnames));
		// } catch(Exception e) {e.printStackTrace();}

		lt = LexicalTableImpl.createLexicalTable(freqs, pProperties.get(0),
				rownames, colnames, 1);
		lt.setCorpus(corpus);

		// if(referenceCorpus != null && referenceCorpus.length() > 0) {
		// //lt.removeCol(0, false);
		// lt.setReference(referenceCorpus);
		// lt.exchangeColumns(1,2);
		// }
		return true;
	}

	/**
	 * Step build signatures.
	 *
	 * @return true, if successful
	 * @throws UnexpectedAnswerException
	 *             the unexpected answer exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws CqiServerError
	 *             the cqi server error
	 */
	public boolean stepBuildSignatures() throws UnexpectedAnswerException, IOException, CqiServerError {
		allsignaturesstr = new HashMap<Integer, String>();
		Set<Integer> allpositions = new HashSet<Integer>(); // no duplicates
		for (Match n : m2) {
			for (int i = n.getStart(); i <= n.getEnd(); i++)
				allpositions.add(i);
		}
		// System.out.println("Position set: "+allpositions.size());

		int[] allpositionsarray = new int[allpositions.size()];
		int pcount = 0;
		for (int p : allpositions) {
			allpositionsarray[pcount++] = p;
		}

		HashMap<Property, int[]> propsId = new HashMap<Property, int[]>();
		// HashMap<Property, String[]> propsValues = new HashMap<Property,
		// String[]>();
		for (Property property : pProperties) {
			int[] indices = CorpusManager.getCorpusManager().getCqiClient()
					.cpos2Id(property.getQualifiedName(), allpositionsarray);
			// String[] values =
			// CorpusManager.getCorpusManager().getCqiClient().cpos2Str(property.getQualifiedName(),allpositionsarray);
			propsId.put(property, indices);
			// propsValues.put(property, values);
			// System.out.println("all "+property+" indices: "+propsId.get(property).length);
		}
		if (Thread.interrupted())
			return false; // stop if interrupted by user

		//System.out.println("T values + ids: "+(System.currentTimeMillis()- time)); //$NON-NLS-1$

		pcount = 0;
		for (int position : allpositionsarray) {
			//String sign = ""; //$NON-NLS-1$
			String signstr = ""; //$NON-NLS-1$
			for (Property property : pProperties) {
				signstr += "[" + propsId.get(property)[pcount] + "]"; //$NON-NLS-1$ //$NON-NLS-2$
				//signstr+="["+propsValues.get(property)[pcount]+"]"; //$NON-NLS-1$ //$NON-NLS-2$
			}
			// allsignatures.put(position, sign);
			allsignaturesstr.put(position, signstr);
			pcount++;

		}
		return true;
	}

	/**
	 * Step count.
	 *
	 * @return true, if successful
	 */
	public boolean stepCount() {
		// ArrayList<Integer> keepedPosition = new ArrayList<>();

		int startsearchM2 = 0; // optimisation: m2 is ordered
		int startsearchM3 = 0; // optimisation: m3 is ordered
		// time = System.currentTimeMillis();

		HashMap<Integer, Integer> positionsDistances = new HashMap<Integer, Integer>();

		for (Match m : m1) { // for each match = for each focus

//			if (m.getTarget() >= 0) { // if target is set focus on target position
//				m.setStart(m.getTarget());
//				m.setEnd(m.getTarget());
//			}
			// System.out.println("for match m: "+m);
			Match n = null; // the match which contains the context
			Match o = null; // the match which does not contain the context
			boolean matchFound = false;
			for (int i = startsearchM2; i < m2.size(); i++) { // find n
				n = m2.get(i);
				if (n.getStart() <= m.getStart() && m.getEnd() <= n.getEnd()) {
					startsearchM2 = i;
					matchFound = true;
					break;
				}
			}
			if (Thread.interrupted()) return false; // stop if interrupted by user
			// System.out.println("found n: "+n);

			for (int i = startsearchM3; i < m3.size(); i++) { // find next match m3 contained by m2

				o = m3.get(i);
				if (o.getStart() <= m.getStart() && m.getEnd() <= o.getEnd()) {
					startsearchM3 = i;
					matchFound = matchFound && true;
					break;
				}
			}
			// System.out.println("found o: "+o);

			if (!matchFound) {
				continue;
			}

			int start = n.getStart();
			int size = n.getEnd() - start + 1;
			// if (size > 0)
			// size++;
			// System.out.println("Process focus:"+m+" with maxcontext:"+n+" and anticontext:"+o);
			// System.out.println("NbOccs "+(size));
			int[] positions = new int[size];
			int noOcc = 0;

			// System.out.println("positions");
			// System.out.println("start: "+(start)+" end:"+n.getEnd());
			for (int position = start; position <= n.getEnd(); position++) {

				if (o.getStart() <= position && position <= o.getEnd()) {
					// ignore positions in the anticontext positions
					continue;
				}

				int dist;
				if (position < m.getStart()) {
					dist = m.getStart() - position - 1;
				}
				else if (m.getEnd() < position) {
					dist = position - m.getEnd() - 1;
				}
				else { // the n match is in the m match !?
					System.out.println("Warning: the n match is in the m match ? " + n + " " + m);
					dist = 0;
				}
				if (!positionsDistances.containsKey(position) || positionsDistances.get(position) > dist) {
					positionsDistances.put(position, dist);
				}
			}

			// System.out.println("nb Occ ignored: "+ignore);
			// System.out.println("nb Occ chevauche: "+chevauche);
		}

		// store and count distances for each position signature
		int noOcc = 0;
		for (int position : positionsDistances.keySet()) { // cooccurrent words positions
			// String signature = allsignatures.get(position);
			String signaturestr = allsignaturesstr.get(position);

			int dist = positionsDistances.get(position);
			if (distances.containsKey(signaturestr)) {
				distances.put(signaturestr, (distances.get(signaturestr)) + dist);
			}
			else {
				distances.put(signaturestr, 0.0);
			}

			if (counts.containsKey(signaturestr)) {
				counts.put(signaturestr, (counts.get(signaturestr)) + 1);
			}
			else {
				counts.put(signaturestr, 1);
			}

			// if ("[1599]".equals(signaturestr)) {
			// System.out.println("p=" + position + " d=" + dist + " total(d)=" + distances.get(signaturestr) + " c=" + counts.get(signaturestr));
			// }
			// }

			noOcc++;
		}
		// System.out.println("T counts : "+(System.currentTimeMillis()- time)); //$NON-NLS-1$

		allsignaturesstr = null; // no more need
		// counted = null;
		return true;
	}

	/**
	 * Step get matches.
	 *
	 * @return true, if successful
	 * @throws CqiClientException
	 */
	public boolean stepGetMatches() throws CqiClientException {
		Corpus corpus = this.getCorpus();
		QueryResult r1 = corpus.query(pQuery, "CoocFocusQuery", false); // keywords positions //$NON-NLS-1$
		QueryResult r2 = null;
		if (contextQuery.equals(Query.fixQuery(pQuery.getQueryString()))) {
			r2 = r1;
		}
		else {
			r2 = corpus.query(contextQuery, "CoocContextFocusQuery", false); // max context //$NON-NLS-1$
		}
		QueryResult r3 = null;
		if (anticontextquery.equals(Query.fixQuery(pQuery.getQueryString()))) {
			r3 = r1;
		}
		else if (contextQuery.equals(anticontextquery)) {
			r3 = r2;
		}
		else {
			r3 = corpus.query(anticontextquery, "CoocAntiContextFocusQuery", false); // no context //$NON-NLS-1$
		}


		m1 = r1.getMatches();
		numberOfKeyword = m1.size();
		m2 = r2.getMatches();
		m3 = r3.getMatches();

		Log.finest("R1 size=" + m1.size());
		Log.finest("R2 size=" + m2.size());
		Log.finest("R3 size=" + m3.size());

		// System.out.println(query+" M1 size: "+m1.size());
		// System.out.println(contextquery+" M2 size: "+m2.size());
		// System.out.println(anticontextquery+" M3 size: "+m3.size());
		r1.drop();

		if (r2 != r1) {
			r2.drop();
		}
		if (r3 != r1 && r2 != r3) {
			r3.drop();
		}
		return true;
	}

	/**
	 * Step get scores.
	 *
	 * @return true, if successful
	 * @throws CqiClientException
	 *             the cqi client exception
	 * @throws StatException
	 *             the stat exception
	 */
	public boolean stepGetScores() throws CqiClientException, StatException {
		SpecificitesResult specif = Specificites.specificites(lt, 1000);
		// System.out.println("Specif N part: "+specif.getNbrPart()); //$NON-NLS-1$
		// System.out.println("Specif N lines number: "+specif.getSpecificitesIndex().length); //$NON-NLS-1$
		// System.out.println("T specif e: "+(System.currentTimeMillis()- time)); //$NON-NLS-1$
		// specif.toTxt(new File("~/Bureau/coocresults/specif Cooc")); //$NON-NLS-1$
		String[] specifrownames = specif.getTypeNames();
		double[][] scores = specif.getSpecificitesIndex();
		// System.out.println("Nb specif result: "+specif.getSpecificitesIndex().length);

		int iimax = Math.min(specifrownames.length, scores.length);
		for (int ii = 0; ii < iimax; ii++) { // counts.keySet())
			String signaturestr = keysToString.get(specifrownames[ii]);

			ArrayList<String> props = new ArrayList<String>();
			if (pProperties.size() > 1) {
				String[] splited = specifrownames[ii].split("_", pProperties.size()); //$NON-NLS-1$

				for (int p = 0; p < pProperties.size(); p++) {
					props.add(splited[p]);
				}
			}
			else {
				props.add(specifrownames[ii]);
			}
			// if(specifrownames[ii].equals("(") || specifrownames[ii].equals(")"))
			// {
			// System.out.println("rowname: "+specifrownames[ii]);
			// System.out.println("props: "+props);
			// System.out.println("counts: "+counts.get(signaturestr));
			// System.out.println("speciffreq: "+indexfreqs.get(specifrownames[ii]));
			// System.out.println("specif score: "+scores[ii][1]);
			// System.out.println("distance: "+distances.get(signaturestr));
			// System.out.println("distance count: "+distancescounts.get(signaturestr));
			// }
			if (counts.containsKey(signaturestr)) {
				CLine cline = new CLine(this, specifrownames[ii], props,
						counts.get(signaturestr), // cofreq
						indexfreqs.get(specifrownames[ii]), scores[ii][1], // freq
						((float) (distances.get(signaturestr) / counts.get(signaturestr))), // mean distance
						-1);

				// select the line
				if (cline.freq >= this.pFminFilter && cline.nbocc >= this.pFCoocFilter && cline.score >= 0 && cline.score >= this.pScoreMinFilter) {
					if (cline.score >= Integer.MAX_VALUE - 5) {
						cline.score = Float.MAX_EXPONENT;
					}
					lines.add(cline);
				}
			}
			// System.out.println(signaturestr+"\t"+voclines.get(ii).getFrequency()+"\t"+counts.get(signaturestr)+"\t"+(distances.get(signaturestr)/distancescounts.get(signaturestr))+"\t"+scores[ii][1]);
			// //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		}
		return true;
	}

	/**
	 * Step query limits.
	 *
	 * @return true, if successful
	 */
	public boolean stepQueryLimits() {
		
		String fixedQuery = Query.fixQuery(pQuery.getQueryString());

		if (pStructuralUnitLimit != null) {
			String tempquery = ""; //$NON-NLS-1$
			String lname = pStructuralUnitLimit.getName();

			// test if there is a left context
			if (pMinLeftContextSize > 0) {
				tempquery += "(<" + lname + ">[]* </" + lname + ">){" + (pMaxLeftContextSize) + "," + (pMaxLeftContextSize) + "}"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			}
			// (<p>[]*</p>){0, 50} "je" (<p>[]*</p>){0, 50}
			tempquery += " <" + lname + ">[]* " + fixedQuery + " []* </" + lname + "> "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

			// test if there is a right context
			if (pMinRightContextSize > 0) {
				tempquery += "(<" + lname + ">[]* </" + lname + ">){" + (pMaxRightContextSize) + "," + (pMaxRightContextSize) + "}";  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			}
			this.contextQuery = new Query(tempquery);

			if (pIncludeXpivot) {
				String anticontextquerystring = ""; //$NON-NLS-1$
				// minleft = 2..N
				if (pMinLeftContextSize > 1) {
					anticontextquerystring += "(<" + lname + ">[]* </" + lname + ">){" + (pMinLeftContextSize - 1) + "," + (pMinLeftContextSize - 1) + "} <" + lname + ">[]* "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
				}
				anticontextquerystring += fixedQuery;
				// minright = 2..N
				if (pMinRightContextSize > 1) {
					anticontextquerystring += " []* </" + lname + "> (<" + lname + ">[]* </" + lname + ">){" + (pMinRightContextSize - 1) + "," + (pMinRightContextSize - 1) + "}"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
				}
				this.anticontextquery = new Query(anticontextquerystring);
			}
			else {
				String anticontextquerystring = ""; //$NON-NLS-1$
				// minleft = 2..N
				if (pMinLeftContextSize > 0) {
					anticontextquerystring += "(<" + lname + ">[]* </" + lname + ">){" + (pMinLeftContextSize - 1) + "," + (pMinLeftContextSize - 1) + "} <" + lname + ">[]* "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
				}
				anticontextquerystring += fixedQuery;
				// minright = 2..N
				if (pMinRightContextSize > 0) {
					anticontextquerystring += " []* </" + lname + "> (<" + lname + ">[]* </" + lname + ">){" + (pMinRightContextSize - 1) + "," + (pMinRightContextSize - 1) + "}"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
				}
				this.anticontextquery = new Query(anticontextquerystring);
			}
		}
		else { // word context

			String tempquery = ""; //$NON-NLS-1$
			if (pMinLeftContextSize > 0) { // test if there is a left context
				tempquery += "[]{" + pMaxLeftContextSize + "," + pMaxLeftContextSize + "} "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			tempquery += fixedQuery;

			if (pMinRightContextSize > 0) { // test if there is a right context
				tempquery += " []{" + pMaxRightContextSize + "," + pMaxRightContextSize + "} "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			this.contextQuery = new Query(tempquery);
			String anticontextquerystring = ""; //$NON-NLS-1$
			if (pMinLeftContextSize > 1) {
				anticontextquerystring += "[]{" + (pMinLeftContextSize - 1) + ", " + (pMinLeftContextSize - 1) + "}"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			anticontextquerystring += fixedQuery;
			if (pMinRightContextSize > 1) {
				anticontextquerystring += "[]{" + (pMinRightContextSize - 1) + "," + (pMinRightContextSize - 1) + "} "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			this.anticontextquery = new Query(anticontextquerystring);
		}
		return true;
	}

	/**
	 * To infos.
	 *
	 * @return the string
	 */
	public String toInfos() {
		StringBuffer buf = new StringBuffer(60);
		buf.append("Cooc: \n"); //$NON-NLS-1$
		buf.append(" Corpus: " + this.corpus + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buf.append(" Query: " + this.pQuery + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buf.append(" Properties: " + this.pProperties + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buf.append(" Structure: " + this.pStructuralUnitLimit + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buf.append(" Empans: " + (this.pMinLeftContextSize - 1) + "<left<" + (this.pMaxLeftContextSize - 1) + " ; " + (this.pMinRightContextSize - 1) + "<right<" + (this.pMaxRightContextSize - 1) + "\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		buf.append(" Filters: freq>" + this.seuil_freq + " ; count>" + seuil_count + " ; score>" + seuil_score + "\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		return buf.toString();
	}

	/**
	 * To txt.
	 *
	 * @param outfile
	 *            the outfile
	 * @param encoding
	 *            the encoding
	 * @return true, if successful
	 */
	public boolean toTxt(File outfile, String encoding) {
		return toTxt(outfile, encoding, "\t", ""); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * To txt.
	 *
	 * @param outfile
	 *            the outfile
	 * @param encoding
	 *            the encoding
	 * @param colseparator
	 *            the colseparator
	 * @param txtseparator
	 *            the txtseparator
	 * @return true, if successful
	 */
	public boolean toTxt(File outfile, String encoding, String colseparator,
			String txtseparator) {
		try {
			// NK: writer declared as class attribute to perform a clean if the
			// operation is interrupted
			this.writer = new OutputStreamWriter(new FileOutputStream(outfile),
					encoding);
			// if ("UTF-8".equals(encoding)) writer.write('\ufeff'); // UTF-8
			// BOM
			toTxt(writer, colseparator, txtseparator);
		} catch (Exception e) {
			Log.severe(Messages.Cooccurrence_107 + e);
			return false;
		}
		return true;
	}

	/**
	 * To txt.
	 *
	 * @param writer
	 *            the writer
	 * @param colseparator
	 *            the colseparator
	 * @param txtseparator
	 *            the txtseparator
	 * @return true, if successful
	 */
	public boolean toTxt(Writer writer, String colseparator, String txtseparator) {
		try {
			// Occ Freq CoFreq Score MeanDist
			writer.write(txtseparator + Messages.Cooccurrence_11 + txtseparator
					+ colseparator + txtseparator + Messages.Cooccurrence_12
					+ txtseparator + colseparator + txtseparator
					+ Messages.Cooccurrence_13 + txtseparator + colseparator
					+ txtseparator + Messages.Cooccurrence_14 + txtseparator
					+ colseparator + txtseparator + Messages.Cooccurrence_15
					+ txtseparator + "\n");//colseparator+ //$NON-NLS-1$
			//txtseparator+Messages.Cooccurrence_16+txtseparator+"\n"); //$NON-NLS-1$
			for (CLine line : lines)
				writer.write(line.resume(colseparator, txtseparator) + "\n"); //$NON-NLS-1$
			writer.close();
		} catch (IOException e) {
			Log.severe(Messages.Cooccurrence_107 + e);
			return false;
		}

		return true;
	}

}
