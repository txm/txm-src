package org.txm.web.server.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.txm.Toolbox;
import org.txm.functions.parallelcontexts.ParaCell;
import org.txm.functions.parallelcontexts.ParallelContexts;
import org.txm.objects.Base;
import org.txm.objects.Edition;
import org.txm.objects.Page;
import org.txm.objects.Text;
import org.txm.objects.TxmObject;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Alignement;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.query.Query;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.logger.Log;
import org.txm.web.aas.authorization.permissions.EditionPermission;
import org.txm.web.client.services.EditionService;
import org.txm.web.client.services.PortalService;
import org.txm.web.exceptions.PermissionException;
import org.txm.web.server.TxmUser;
import org.txm.web.server.statics.ActivityLog;
import org.txm.web.server.statics.Corpora;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.server.statics.TxmConfig;
import org.txm.web.shared.Actions;
import org.txm.web.shared.EditionResult;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The implementation of Edition service
 * @author vchabanis
 *
 */
public class EditionServiceImpl extends PortalService implements EditionService {

	private static final long serialVersionUID = 4069453482203008052L;
	private String htmldir = TxmConfig.getProperty("htmlBase");

	@Override
	/**
	 * @return the edition of a Base given its ID
	 */
	public ArrayList<EditionResult> getEditions(String corpusPath, String textid, ArrayList<String> editionNames, String[] currentPages) throws PermissionException {
		Log.info("[EditionServiceImpl.getEditions] start. path="+corpusPath+" editionName="+editionNames.size());
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return null; 
		}
		ArrayList<EditionResult> results = new ArrayList<EditionResult>();
		
		String wordid = null; // trying to get current page first wordid, will be used to get other facette pages
		if (currentPages != null && currentPages.length > 0 && editionNames.size() > 0) {
			String editionName;
			String tmpcorpuspath = corpusPath;
			String[] split = editionNames.get(0).split(" - ");
			if (split.length == 2) {
				tmpcorpuspath = "/"+split[0];
				editionName = split[1];
			} else {
				editionName = editionNames.get(0);
			}
			
			Object o = Corpora.get(tmpcorpuspath);
			MainCorpus corpus = null;
			if (o instanceof MainCorpus) {
				corpus = (MainCorpus)o;
			} else if (o instanceof Subcorpus) {
				corpus = ((Subcorpus)o).getMainCorpus();
			} else {
				Log.severe("Error: path is not a MainCorpus or SubCorpus: "+corpusPath);
				return null;
			}

			Text text = corpus.getFirstText();
			if (textid != null && textid.length() > 0) { // select text if textid is specified
				Text t = corpus.getText(textid);
				if (t != null)	text = t;
			}
			//System.out.println("Editions: "+text.getEditions());
			Edition edition = text.getEdition(editionName);
			if (edition == null) {
				Log.severe("Error: edition not found: "+editionName);
				return null;
			}
			
			Page page = edition.get(currentPages[0]);
			if (page != null)
				wordid = page.getWordId();
		}
		
		for (String couple : editionNames) {
			String[] split = couple.split(" - ");
			String tmpcorpuspath = corpusPath;
			String editionName;
			if (split.length == 2) {
				tmpcorpuspath = "/"+split[0];
				editionName = split[1];
			} else {
				editionName = couple;
			}
			
			System.out.println("get HTML for couple "+tmpcorpuspath+" - "+editionName);

			Object o = Corpora.get(tmpcorpuspath);
			MainCorpus corpus = null;
			if (o instanceof MainCorpus) {
				corpus = (MainCorpus)o;
			} else if (o instanceof Subcorpus) {
				corpus = ((Subcorpus)o).getMainCorpus();
			} else {
				Log.severe("Error: path is not a MainCorpus or SubCorpus: "+corpusPath);
				return null;
			}

			Text text = corpus.getFirstText();
			if (textid != null && textid.length() > 0) { // select text if textid is specified
				Text t = corpus.getText(textid);
				if (t != null)	text = t;
			}
			//System.out.println("Editions: "+text.getEditions());
			Edition edition = text.getEdition(editionName);
			if (edition == null) {
				Log.severe("Error: edition not found: "+editionName);
				return null;
			}
			
			Page page = edition.getFirstPage();
			if (wordid != null) {
				 page = edition.getPageForWordId(wordid); // TODO: currentPage e [1:N]
				 if (page == null) page = edition.getFirstPage();
			}	

			String path = "/" + corpus.getName() + "/" + text.getName();
			System.out.println(user.getPermissions(path));
			if (!user.can(path, new EditionPermission())) {
				ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getEdition", path);
				Log.severe("[EditionServiceImpl.getEdition] Insufficient permissions to get this edition");
				throw new PermissionException("Insufficient permissions to get this edition.");
				/*
				Text next = corpus.getNextText(text);
				if(next == null || next.equals(text)) {	
					ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getEdition", path);
					Log.severe("[EditionServiceImpl.getEdition] Insufficient permissions to get this edition");
					throw new PermissionException("Insufficient permissions to get this edition.");
				}
				text = next;
				edition = text.getEdition(editionName);
				page = edition.getFirstPage();
				path = "/" + corpus.getName() + "/" + text.getName();
				*/
			}
			
			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getEditions", corpusPath);

			EditionResult result = new EditionResult();
			result.setContent(getStringValue(page));					
			result.setBaseId(corpusPath);
			result.setTextId(text.getName());
			result.setCurrentPage(page.getName());
			result.setTotalPage(edition.getNumPages());
			result.setName(edition.getName());
			result.setBiblio(text.getBiblioPath());
			result.setPDF(text.getPDFPath());
			results.add(result);
		}
		return results;
	}

	public static String getStringValue(Page page) {
		return getStringValue(page.getFile());
	}
	
	public static String getStringValue(File file) {
		if (!file.exists()) return "File not found: "+file.getName();
		InputStream is = null;
		byte[] buffer = new byte[(int)file.length()];
		try {
			is = new FileInputStream(file);
			is.read(buffer);
			is.close();
			return new String(buffer, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (is != null)
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return null;
	}

	@Override
	/**
	 * @return the edition of a Base given its ID
	 */
	public EditionResult getEdition(String corpusPath, String editionName) throws PermissionException {
		Log.info("[EditionServiceImpl.getEdition] start. baseId="+corpusPath);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return null; 
		}

		Object o = Corpora.get(corpusPath);
		MainCorpus corpus = null;
		if (o instanceof MainCorpus) {
			corpus = (MainCorpus)o;
		} else if (o instanceof Subcorpus) {
			corpus = ((Subcorpus)o).getMainCorpus();
		} else {
			Log.severe("Error: path is not a MainCorpus or SubCorpus: "+corpusPath);
			return null;
		}
		Text text = corpus.getFirstText();
		if (editionName == null) editionName = "default";
		Edition edition = text.getEdition(editionName);
		Page page = edition.getFirstPage();

		String path = "/" + corpus.getName() + "/" + text.getName();
		if (!user.can(path, new EditionPermission())) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getEdition", corpusPath);
			Log.severe("[EditionServiceImpl.getEdition] Insufficient permissions to get this edition");
			throw new PermissionException("Insufficient permissions to get this edition: "+path);
			
//			Text next = corpus.getNextText(text);
//			if (next == null || next.equals(text)) {	
//				ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getEdition", corpusPath);
//				Log.severe("[EditionServiceImpl.getEdition] Insufficient permissions to get this edition");
//				throw new PermissionException("Insufficient permissions to get this edition: "+path);
//			}
//			text = next;
//			edition = text.getEdition("default");
//			page = edition.getFirstPage();
//			path = "/" + corpus.getName() + "/" + text.getName();
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getEdition", corpusPath);

		EditionResult result = new EditionResult();
		result.setContent(getStringValue(page));
		result.setBaseId(corpusPath);
		result.setTextId(text.getName());
		result.setCurrentPage(page.getName());
		result.setTotalPage(edition.getNumPages());
		result.setName(edition.getName());
		result.setBiblio(text.getBiblioPath());
		result.setPDF(text.getPDFPath());
		return result;
	}

	@Override
	/**
	 * retrieve a page from a base id, text id and word id
	 * TODO: check how the RCP do it
	 */
	public ArrayList<EditionResult> backToText(String corpusPath, ArrayList<String> editions, String textId, String wordId) throws PermissionException {
		Log.info("[EditionServiceImpl.backToText] start. baseId="+corpusPath+", textId="+textId+", wordId="+wordId);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return null; 
		}

		Object o = Corpora.get(corpusPath);
		MainCorpus srccorpus = null;
		if (o instanceof MainCorpus) {
			srccorpus = (MainCorpus)o;
		} else if (o instanceof Subcorpus) {
			srccorpus = ((Subcorpus)o).getMainCorpus();
		} else {
			Log.severe("Error: path is not a MainCorpus or SubCorpus: "+corpusPath);
			return null;
		}

		ArrayList<EditionResult> results = new ArrayList<EditionResult>();
		// int i = 0;
		for (String couple : editions) {
			String[] split = couple.split(" - ");
			//String tmpcorpuspath = corpusPath;
			String editionName;
			if (split.length == 2) {
				//tmpcorpuspath = "/"+split[0];
				editionName = split[1];
			} else
				editionName = couple;

			//System.out.println("get HTML for couple "+tmpcorpuspath+" - "+editionName);
			Page page;
			Text text;
			Edition edition;
			String path;
			//if (corpusPath.equals(tmpcorpuspath)) {
				text = srccorpus.getText(textId);
				edition = text.getEdition(editionName);
				page = edition.getPageForWordId(wordId);
				path = "/" + srccorpus.getName() + "/" + text.getName();
			/*}
			else { // Aligned corpus
				o = Corpora.get(tmpcorpuspath); // the aligned corpus
				if(!(o instanceof MainCorpus)) {
					Log.severe("Error: path is not a MainCorpus: "+corpusPath);
					return null;
				}
				Corpus corpus = (MainCorpus)o;

				Alignement align = srccorpus.getBase().getLink(srccorpus.getName(), corpus.getName());
				List<String> rez;
				try {
					rez = align.resolve(wordId, textId);
					page = align.getPage(editionName, rez.get(0), rez.get(1));
				} catch (Exception e) {
					Log.severe("Failed to get alignement from "+srccorpus+" to "+corpus+" wid "+wordId+" text "+textId);
					e.printStackTrace();
					return null;
				} 

				text = corpus.getText(rez.get(1));
				edition = text.getEdition(editionName);
				path = "/" + corpus.getName() + "/" + text.getName();
			}*/

			if (!user.can(path, new EditionPermission())) {
				ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "backToText", corpusPath, textId, wordId);
				throw new PermissionException("Insufficient permissions to get this edition.");
			}
			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "backToText", corpusPath, textId, wordId);

			EditionResult result = new EditionResult();
			result.setContent(getStringValue(page));
			result.setBaseId(corpusPath);
			result.setTextId(text.getName());
			result.setCurrentPage(page.getName());
			result.setTotalPage(edition.getNumPages());
			result.setName(edition.getName());
			result.setBiblio(text.getBiblioPath());
			result.setPDF(text.getPDFPath());
			results.add(result);
		}
		return results;
	}

	@Override
	/**
	 * retrieve the next page given a base id, text id and a current page id
	 * @param baseId the name of the Base
	 * @param textId the name of the Text
	 * @param editionNames a list of edition names
	 * @param currentPage the index of the current page
	 * @return ArrayList<EditionResult>
	 */
	public ArrayList<EditionResult> getNextPage(String corpusPath, String[] textIds, ArrayList<String> editionNames, String[] currentPages) throws PermissionException {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return null; 
		}

		Log.info("[EditionServiceImpl.getNextPage] start. baseId="+corpusPath+", textId="+Arrays.toString(textIds)+", currentPage="+Arrays.toString(currentPages));
		ArrayList<EditionResult> results = new ArrayList<EditionResult>();
		int i = 0;
		for (String couple :  editionNames) {
			String[] split = couple.split(" - ");
			String tmpcorpuspath = corpusPath;
			String editionName;
			if (split.length == 2) {
				tmpcorpuspath = "/"+split[0];
				editionName = split[1];
			} else
				editionName = couple;

			Object o = Corpora.get(tmpcorpuspath);
			MainCorpus corpus = null;
			if (o instanceof MainCorpus) {
				corpus = (MainCorpus)o;
			} else if (o instanceof Subcorpus) {
				corpus = ((Subcorpus)o).getMainCorpus();
			} else {
				Log.severe("Error: path is not a MainCorpus or SubCorpus: "+corpusPath);
				return null;
			}

			String textId = textIds[i];
			String currentPage = currentPages[i++];

			Text text = corpus.getText(textId);
			Edition edition = text.getEdition(editionName);
			Page page = edition.get(currentPage); // TODO: currentPage e [1:N]
			Page targetPage = edition.getNextPage(page);
			if (targetPage == null)
				targetPage = page;

			String path = "/" + corpus.getName() + "/" + text.getName();
			if (!user.can(path, new EditionPermission())) {
				ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getNextPage", corpusPath, textId, currentPage);
				throw new PermissionException("Insufficient permissions to get this edition.");
			}
			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getNextPage", corpusPath, textId, targetPage.getName());

			EditionResult result = new EditionResult();
			result.setContent(getStringValue(targetPage));
			result.setBaseId(corpusPath);
			result.setTextId(text.getName());
			result.setCurrentPage(targetPage.getName());
			result.setTotalPage(edition.getNumPages());
			result.setName(edition.getName());
			result.setBiblio(text.getBiblioPath());
			result.setPDF(text.getPDFPath());
			results.add(result);
		}
		return results;
	}

	@Override
	/**
	 * retrieve the previous page given a base id, text id and a current page id
	 * @param baseId the name of the Base
	 * @param textId the name of the Text
	 * @param editionNames a list of edition names
	 * @param currentPage the index of the current page
	 * @return ArrayList<EditionResult>
	 */
	public ArrayList<EditionResult> getPreviousPage(String corpusPath, String[] textIds, ArrayList<String> editionNames, String[] currentPages) throws PermissionException {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return null; 
		}

		Log.info("[EditionServiceImpl.getPerviousPage] start. baseId="+corpusPath+", textId="+Arrays.toString(textIds)+", currentPage="+Arrays.toString(currentPages));

		ArrayList<EditionResult> results = new ArrayList<EditionResult>();
		int i = 0 ;
		for (String couple :  editionNames) {

			String[] split = couple.split(" - ");
			String tmpcorpuspath = corpusPath;
			String editionName;
			if (split.length == 2) {
				tmpcorpuspath = "/"+split[0];
				editionName = split[1];
			} else {
				editionName = couple;
			}

			Object o = Corpora.get(tmpcorpuspath);
			MainCorpus corpus = null;
			if (o instanceof MainCorpus) {
				corpus = (MainCorpus)o;
			} else if (o instanceof Subcorpus) {
				corpus = ((Subcorpus)o).getMainCorpus();
			} else {
				Log.severe("Error: path is not a MainCorpus or SubCorpus: "+corpusPath);
				return null;
			}

			String textId = textIds[i];
			String currentPage = currentPages[i++];

			Text text = corpus.getText(textId);
			Edition edition = text.getEdition(editionName);
			Page page = edition.get(currentPage);
			Page targetPage = edition.getPreviousPage(page);
			if (targetPage == null)
				targetPage = page;

			String path = "/" + corpus.getName() + "/" + text.getName();
			if (!user.can(path, new EditionPermission())) {
				ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getPreviousPage", corpusPath, textId, currentPage);
				throw new PermissionException("Insufficient permissions to get this edition.");
			}
			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getPreviousPage", corpusPath, textId, targetPage.getName());

			EditionResult result = new EditionResult();
			result.setContent(getStringValue(targetPage));
			result.setBaseId(corpusPath);
			result.setTextId(text.getName());
			result.setCurrentPage(targetPage.getName());
			result.setTotalPage(edition.getNumPages());
			result.setName(edition.getName());
			result.setBiblio(text.getBiblioPath());
			result.setPDF(text.getPDFPath());
			results.add(result);
		}
		return results;
	}

	@Override
	/**
	 * retrieve the last page of an edition
	 * @param baseId the name of the Base
	 * @param textId the name of the Text
	 * @param editionNames a list of edition names
	 * @param currentPage the index of the current page
	 * @return ArrayList<EditionResult>
	 */
	public ArrayList<EditionResult> getLastPage(String corpusPath, String[] textIds, ArrayList<String> editionNames, String[] currentPages){
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return null; 
		}

		Log.info("[EditionServiceImpl.getLastPage] start. baseId="+corpusPath+", textId="+textIds+", currentPage="+currentPages);
		ArrayList<EditionResult> results = new ArrayList<EditionResult>();
		int i = 0 ;
		for (String couple : editionNames) {
			String[] split = couple.split(" - ");
			String tmpcorpuspath = corpusPath;
			String editionName;
			if (split.length == 2) {
				tmpcorpuspath = "/"+split[0];
				editionName = split[1];
			} else
				editionName = couple;

			Object o = Corpora.get(tmpcorpuspath);
			MainCorpus corpus = null;
			if (o instanceof MainCorpus) {
				corpus = (MainCorpus)o;
			} else if (o instanceof Subcorpus) {
				corpus = ((Subcorpus)o).getMainCorpus();
			} else {
				Log.severe("Error: path is not a MainCorpus or SubCorpus: "+corpusPath);
				return null;
			}

			String textId = textIds[i];
			String currentPage = currentPages[i++];

			Text text = corpus.getText(textId);
			Edition edition = text.getEdition(editionName);
			Page page = edition.getLastPage();

			String path = "/" + corpus.getName() + "/" + text.getName();
			if (!user.can(path, new EditionPermission())) {
				ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getLastPage", corpusPath, textId, currentPage);
				return null;
			}
			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getLastPage", corpusPath, textId, currentPage);

			EditionResult result = new EditionResult();
			result.setContent(getStringValue(page));
			result.setBaseId(corpusPath);
			result.setTextId(text.getName());
			result.setCurrentPage(page.getName());
			result.setTotalPage(edition.getNumPages());
			result.setName(edition.getName());
			result.setBiblio(text.getBiblioPath());
			result.setPDF(text.getPDFPath());
			results.add(result);
		}
		return results;
	}

	@Override
	/**
	 * retrieve the first page of an edition
	 * @param baseId the name of the Base
	 * @param textId the name of the Text
	 * @param editionNames a list of edition names
	 * @param currentPage the index of the current page
	 * @return ArrayList<EditionResult>
	 */
	public ArrayList<EditionResult> getFirstPage(String corpusPath, String[] textIds, ArrayList<String> editionNames, String[] currentPages) throws PermissionException {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return null; 
		}

		Log.info("[EditionServiceImpl.getFirstPage] start. baseId="+corpusPath+", textId="+textIds+", currentPage="+currentPages);

		ArrayList<EditionResult> results = new ArrayList<EditionResult>();
		int i = 0 ;
		for (String couple :  editionNames) {
			String[] split = couple.split(" - ");
			String tmpcorpuspath = corpusPath;
			String editionName;
			if (split.length == 2) {
				tmpcorpuspath = "/"+split[0];
				editionName = split[1];
			}
			else
				editionName = couple;

			Object o = Corpora.get(tmpcorpuspath);
			MainCorpus corpus = null;
			if (o instanceof MainCorpus) {
				corpus = (MainCorpus)o;
			} else if (o instanceof Subcorpus) {
				corpus = ((Subcorpus)o).getMainCorpus();
			} else {
				Log.severe("Error: path is not a MainCorpus or SubCorpus: "+corpusPath);
				return null;
			}

			String textId = textIds[i];
			String currentPage = currentPages[i++];
			Text text = corpus.getText(textId);
			Edition edition = text.getEdition(editionName);
			Page page = edition.getFirstPage();

			String path = "/" + corpus.getName() + "/" + text.getName();
			if (!user.can(path, new EditionPermission())) {
				ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getFirstPage", corpusPath, textId, currentPage);
				throw new PermissionException("Insufficient permissions to get this edition.");
			}
			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getFirstPage", corpusPath, textId, currentPage);

			EditionResult result = new EditionResult();
			result.setContent(getStringValue(page));
			result.setBaseId(corpusPath);
			result.setTextId(text.getName());
			result.setCurrentPage(page.getName());
			result.setTotalPage(edition.getNumPages());
			result.setName(edition.getName());
			result.setBiblio(text.getBiblioPath());
			result.setPDF(text.getPDFPath());
			results.add(result);
		}
		return results;
	}

	@Override
	public ArrayList<EditionResult> getNextText(String corpusPath, String[] textIds, ArrayList<String> editionNames) throws PermissionException {
		Log.info("[EditionServiceImpl.getNextText] start. baseId="+corpusPath+", textId="+textIds);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return null; 
		}

		ArrayList<EditionResult> results = new ArrayList<EditionResult>();
		int i = 0;
		for (String couple : editionNames) {
			//System.out.println("get Edition result for: "+couple);
			String[] split = couple.split(" - ");
			String tmpcorpuspath = corpusPath;
			String editionName;
			if (split.length == 2) {
				tmpcorpuspath = "/"+split[0];
				editionName = split[1];
			} else
				editionName = couple;


			Object o = Corpora.get(tmpcorpuspath);
			MainCorpus corpus = null;
			if (o instanceof MainCorpus) {
				corpus = (MainCorpus)o;
			} else if (o instanceof Subcorpus) {
				corpus = ((Subcorpus)o).getMainCorpus();
			} else {
				Log.severe("Error: path is not a MainCorpus or SubCorpus: "+corpusPath);
				return null;
			}

			String textId = textIds[i++];
			Text text = corpus.getText(textId);
			//System.out.println("** "+corpus.getName()+", text for: "+textId);
			Text nextText = corpus.getNextText(text);
			if (nextText == null)
				nextText = text;

			String path = "/" + corpus.getName() + "/" + nextText.getName();
			//System.out.println("TEST CAN EDITION PATH: "+path+" "+user.can(path, new EditionPermission()));
			while (!user.can(path, new EditionPermission())) {
				nextText = corpus.getNextText(nextText);
				if (nextText == null) { // no more texts
					ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getNextText", corpusPath, textId);
					throw new PermissionException("Insufficient permissions to get this edition.");
				}
				path = "/" + corpus.getName() + "/" + nextText.getName();
			}

			Edition edition = nextText.getEdition(editionName);
			Page page = edition.getFirstPage();
			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getNextText", corpusPath, textId);

			EditionResult result = new EditionResult();
			result.setContent(EditionServiceImpl.getStringValue(page));
			result.setBaseId(corpusPath);
			result.setTextId(nextText.getName());
			result.setCurrentPage(page.getName());
			result.setTotalPage(edition.getNumPages());
			result.setName(edition.getName());
			result.setBiblio(nextText.getBiblioPath());
			result.setPDF(text.getPDFPath());
			results.add(result);
		}
		return results;
	}

	@Override
	public ArrayList<EditionResult> getPreviousText(String corpusPath, String[] textIds, ArrayList<String> editionNames) throws PermissionException {
		Log.info("[EditionServiceImpl.getPreviousText] start. baseId="+corpusPath+", textId="+textIds);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return null; 
		}

		ArrayList<EditionResult> results = new ArrayList<EditionResult>();
		int i = 0;
		for (String couple : editionNames) {
			String[] split = couple.split(" - ");
			String tmpcorpuspath = corpusPath;
			String editionName;
			if (split.length == 2) {
				tmpcorpuspath = "/"+split[0];
				editionName = split[1];
			} else
				editionName = couple;

			EditionResult result = new EditionResult();
			Object o = Corpora.get(tmpcorpuspath);
			MainCorpus corpus = null;
			if (o instanceof MainCorpus) {
				corpus = (MainCorpus)o;
			} else if (o instanceof Subcorpus) {
				corpus = ((Subcorpus)o).getMainCorpus();
			} else {
				Log.severe("Error: path is not a MainCorpus or SubCorpus: "+corpusPath);
				return null;
			}

			String textId = textIds[i++];
			Text text = corpus.getText(textId);
			Text previousText = corpus.getPreviousText(text);
			if (previousText == null)
				previousText = text;

			String path = "/" + corpus.getName() + "/" + previousText.getName();
			while (!user.can(path, new EditionPermission())) {
				previousText = corpus.getPreviousText(previousText);
				if (previousText == null) {
					ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getPreviousText", corpusPath, textId);
					throw new PermissionException("Insufficient permissions to get this edition: "+path);
				}
				path = "/" + corpus.getName() + "/" + previousText.getName();
			}
			Edition edition = previousText.getEdition(editionName);
			Page page = edition.getFirstPage();
			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getPreviousText", corpusPath, textId);

			result.setContent(EditionServiceImpl.getStringValue(page));
			result.setBaseId(corpusPath);
			result.setTextId(previousText.getName());
			result.setCurrentPage(page.getName());
			result.setTotalPage(edition.getNumPages());
			result.setName(edition.getName());
			result.setBiblio(previousText.getBiblioPath());
			result.setPDF(previousText.getPDFPath());
			results.add(result);
		}
		return results;
	}

	@Override
	/**
	 * @return true if the user has the permission to see the edition of this text
	 */
	public boolean isCanSeeEdition(String entityPath) {
		Log.info("[EditionServiceImpl.isCanSeeEdition] start. entityPath="+entityPath);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return false; 
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "isCanSeeEdition", entityPath);
		return user.can(entityPath, new EditionPermission());
	}

//	/**
//	 * Try reading content from the htmlBase directory in /TXMWEB/<app>/data/html
//	 */
//	@Override
//	public String getHtmlContent(String localfile) {
//		Log.info("[EditionServiceImpl.isCanSeeEdition] start. file="+localfile);
//		TxmUser user = SessionManager.getUser(this.getSessionId());
//		if (user == null) {
//			Log.severe("Session "+getSessionId()+" has expired");
//			return ""; 
//		}
//		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getHtmlContent", localfile);
//		localfile = localfile.replace("..", ""); // simple protection
//		File file = new File(htmldir, localfile);
//		/*try {
//			if (!file.getCanonicalPath().startsWith(htmldir)) { 
//				System.out.println("ERROR GETTING CONTENT OF "+file);return null; }
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//			return "error: "+e1;
//		}*/
//		System.out.println("Try reading "+file);
//		if (!file.exists())
//			return null;
//		if (!file.canRead())
//			return null;
//
//		InputStream is = null;
//		byte[] buffer = new byte[(int)file.length()];
//		try {
//			is = new FileInputStream(file);
//			is.read(buffer);
//			is.close();
//			return new String(buffer, "UTF-8");
//		} catch (IOException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}

	public String getBiblio(String itempath, String textId) {
		Log.info("[EditionServiceImpl.getBiblio] start. entityPath="+itempath);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return ""; 
		}
		if (user.askEntity(itempath, Actions.BIBLIO) == null) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getBiblio", itempath, textId);
			Log.severe("no permission");
			return "permission"; 
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getBiblio", itempath, textId);

		Object o = Corpora.get(itempath);

		if (o == null) {
			System.out.println("EditionServiceImpl: getTextsIds: corpus "+itempath+" not found");
			return "corpus_not_found";
		}

		MainCorpus corpus = null;
		if (o instanceof MainCorpus) {
			corpus = (MainCorpus)o;
		} else if (o instanceof Subcorpus) {
			corpus = ((Subcorpus)o).getMainCorpus();
		} else {
			Log.severe("Error: path is not a MainCorpus or SubCorpus: "+itempath);
			return "not_a_corpus";
		}

		Text text = corpus.getText(textId);
		if (text == null)
			return "text_not_found";
		if (text.getBiblioPath() == null)
			return null;

		String s = text.getBiblioPath().toString(); 
		return s;
	}

	public ArrayList<String> getBiblios(String itempath) {
		Log.info("[EditionServiceImpl.getBiblios] start. entityPath="+itempath);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return new ArrayList<String>(); 
		}
		if (user.askEntity(itempath, Actions.BIBLIO) == null) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getBiblios", itempath);
			Log.severe("no permission");
			return new ArrayList<String>(); 
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getBiblios", itempath);

		ArrayList<String> result = new ArrayList<String>();
		TxmObject o = Corpora.get(itempath);
		if (o == null) {
			System.out.println("EditionServiceImpl: getTextsIds: corpus "+itempath+" not found");
			return result;
		}

		MainCorpus corpus = null;
		if (o instanceof MainCorpus) {
			corpus = (MainCorpus)o;
		} else if (o instanceof Subcorpus) {
			corpus = ((Subcorpus)o).getMainCorpus();
		} else {
			Log.severe("Error: path is not a MainCorpus or SubCorpus: "+itempath);
			return null;
		}

		for (Text text : corpus.getTexts()) {
			result.add(text.getBiblioPath().toString());
		}

		return result;
	}

	@Override
	public ArrayList<String> getTextIds(String itempath) {
		Log.info("[EditionServiceImpl.getTextIds] start. entityPath="+itempath);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return new ArrayList<String>(); 
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getTextIds", itempath);

		TxmObject obj = Corpora.get(itempath);
		if (obj instanceof Corpus) {
			return new ArrayList<String>(((Corpus)obj).getTextsID());
		}
		else {
			System.out.println("EditionServiceImpl: getTextsIds: corpus "+itempath+" as no base");
			return new ArrayList<String>();
		}


	}

	@Override
	public HashMap<String, ArrayList<String>> getEditionNames(String itempath) {
		//return HashMap<String, ArrayList<String>>
		HashMap<String, ArrayList<String>> rez = new HashMap<String, ArrayList<String>>();
		Log.info("[EditionServiceImpl.getEditionNames] start. entityPath="+itempath);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return new HashMap<String, ArrayList<String>>(); 
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getEditionNames", itempath);

		TxmObject obj = Corpora.get(itempath);
		if (obj == null) {
			Log.severe("Can't find item for path: "+itempath);
			return new HashMap<String, ArrayList<String>>(); 
		}
		MainCorpus corpus = null;
		if (obj instanceof MainCorpus) {
			corpus = (MainCorpus)obj;
		} else if (obj instanceof Subcorpus) {
			corpus = ((Subcorpus)obj).getMainCorpus();
		} else {
			Log.severe("Error: path is not a MainCorpus or SubCorpus: "+itempath);
			return new HashMap<String, ArrayList<String>>(); 
		}

		System.out.println("Corpus : "+corpus.getName()+" nb Texts ? "+corpus.getTexts().size());

		String[] editions = new String[corpus.getEditionNames().size()];
		
		int i = 0;
		for (String edition : corpus.getEditionNames()) {
			editions[i++] = edition;
		}
		rez.put(corpus.getName(), new ArrayList<String>(Arrays.asList(editions)));
		
		Base base = corpus.getBase();
		//		System.out.println("from: "+base.getLinksFrom(corpus.getName()));
		//		System.out.println("to: "+base.getLinksTo(corpus.getName()));
		//		System.out.println("all: "+base.getLinks());
		for (Alignement align : base.getLinksFrom(corpus.getName())) {
			String paracorpusname = align.getTo();
			try {
				MainCorpus paracorpus = CorpusManager.getCorpusManager().getCorpus(paracorpusname);
				editions = new String[paracorpus.getEditionNames().size()];
				rez.put(corpus.getName(), new ArrayList<String>(Arrays.asList(editions)));

				i = 0;
				for (String edition : paracorpus.getEditionNames())
					editions[i++] = edition;
			} catch (Exception e) {
				Log.severe("Alignement error with corpus: "+paracorpusname+" : "+e);
			}
		}
		return rez;
	}

	@Override
	public ArrayList<String> getParallelCorpora(String itempath) {
		ArrayList<String> rez = new ArrayList<String>();
		TxmObject o = Corpora.get(itempath);
		if (o == null) {
			Log.severe("Can't find item for path: "+itempath);
			return null; 
		}
		MainCorpus corpus = null;
		if (o instanceof MainCorpus) {
			corpus = (MainCorpus)o;
		} else if (o instanceof Subcorpus) {
			corpus = ((Subcorpus)o).getMainCorpus();
		} else {
			Log.severe("Error: path is not a MainCorpus or SubCorpus: "+itempath);
			return null;
		}

		System.out.println("Corpus : "+corpus.getName()+" nb Texts ? "+corpus.getTexts().size());
		Base base = corpus.getBase();
		String str = null;
		for (Alignement align : base.getLinksFrom(corpus.getName())) {
			str = align.getStruct();
			String paracorpusname = align.getTo();
			try {
				MainCorpus paracorpus = CorpusManager.getCorpusManager().getCorpus(paracorpusname);
				rez.add(paracorpus.getName());
			} catch (Exception e) {
				Log.severe("Alignement error with corpus: "+paracorpusname+" : "+e);
			}
		}
		if (str!=null){
			rez.add(str);
		}
		return rez;
	}

	@Override
	public ArrayList<String> getParallelLanguages(String itempath) {
		ArrayList<String> rez = new ArrayList<String>();
		TxmObject o = Corpora.get(itempath);
		if (o == null) {
			Log.severe("Can't find item for path: "+itempath);
			return null; 
		}
		MainCorpus corpus = null;
		if (o instanceof MainCorpus) {
			corpus = (MainCorpus)o;
		} else if (o instanceof Subcorpus) {
			corpus = ((Subcorpus)o).getMainCorpus();
		} else {
			Log.severe("Error: path is not a MainCorpus or SubCorpus: "+itempath);
			return null;
		}

		System.out.println("Corpus : "+corpus.getName()+" nb Texts ? "+corpus.getTexts().size());
		Base base = corpus.getBase();
		rez.add(corpus.getLang());
		for (Alignement align : base.getLinksFrom(corpus.getName())) {
			String paracorpusname = align.getTo();
			try {
				MainCorpus paracorpus = CorpusManager.getCorpusManager().getCorpus(paracorpusname);
				String lang = paracorpus.getLang();
				if (lang !=null && !rez.contains(lang)){
					rez.add(lang);
				}
			} catch (Exception e) {
				Log.severe("Alignement error with corpus: "+paracorpusname+" : "+e);
			}
		}		
		return rez;
	}

	@Override
	public HashMap<String, ArrayList<String>> getParallelContexts(HashMap<String,String> queriesByLang, String struct, ArrayList<String> corporaStr, 
			ArrayList<String> props, ArrayList<String> refs, 
			ArrayList<Integer> CGs, ArrayList<Integer> CDs,
			ArrayList<Boolean> participates) {
		System.out.println("queries " +queriesByLang +" struct "+ struct +" corpora " +corporaStr +" props " +props +" refs " +refs+ 
				" CGS " +CGs +" CDS " +CDs +" participates " +participates);
		//récupérer les corpus à partir de leur nom
		ArrayList<Corpus> corpora = new ArrayList<Corpus>();
		ArrayList<String> queries = new ArrayList<String>();
		for (String c : corporaStr) {
			TxmObject obj = Corpora.get(c);
			if (obj == null) {
				Log.severe("Can't find item for path: "+c);
				return null; 
			}
			Corpus corpus = null;
			if (obj instanceof Corpus) {
				corpus = ((Corpus)obj);
				corpora.add(corpus);
				String lang = corpus.getLang();
				String query = queriesByLang.get(lang);
				if (query!=null){
					queries.add(query);
					System.out.println("Query for corpus : "+corpus.getName()+" with lang "+lang+" is : "+query);
				} else {
					System.out.println("No query for corpus : "+corpus.getName()+" with lang "+lang);
				}
			} else {
				return null;
			}
			System.out.println("Corpus : "+corpus.getName()+" nb Texts ? "+corpus.getTexts().size());
		}
		HashMap<String, ArrayList<String>> results = new HashMap<String, ArrayList<String>>();
		try {
			ParallelContexts paraContexts = new ParallelContexts(queries, struct, "id", corpora, props, refs, CGs, CDs, participates);	
			HashMap<String, ParaCell[]> ks = paraContexts.getKeywordsStrings();
			for (String k : ks.keySet()) {
				//System.out.println("Keywords ["+k+"]");
				ParaCell[] cells = ks.get(k);
				ArrayList<String> cellsToString = new ArrayList<String>();
				for (ParaCell cell : cells) {
					cellsToString.add(cell.toString());
				}
				results.put(k, cellsToString);
			}
		} catch (CqiClientException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (CqiServerError e) {
			e.printStackTrace();
		}
		System.out.println("REZ: "+results);
		return results;
	}

	private String[] getWordIdAndTextId(Corpus corpus, int position) throws CqiClientException, IOException, CqiServerError {
		int[] cpos = {position};
		Property idprop = corpus.getProperty("id");
		StructuralUnitProperty idtext = corpus.getStructuralUnit("text").getProperty("id");
		int[] struct_align_pos = Toolbox.getCqiClient().cpos2Struc(idtext.getQualifiedName(), cpos);
		String[] struct_align_str = Toolbox.getCqiClient().struc2Str(idtext.getQualifiedName(), struct_align_pos);

		String rez[] = {Toolbox.getCqiClient().cpos2Str(idprop.getQualifiedName(), cpos)[0], 
				struct_align_str[0]};

		return rez;
	}

	@Override
	public int[] getPositions(String itemPath, String query) {
		Object o = Corpora.get(itemPath);
		if (!(o instanceof Corpus)) {
			Log.severe("Error: path is not a Corpus: "+itemPath);
			return null;
		}
		Corpus corpus = null;
		if (o instanceof MainCorpus) {
			corpus = (MainCorpus)o;
		} else if (o instanceof Subcorpus) {
			corpus = ((Subcorpus)o);
		} else {
			Log.severe("Error: path is not a MainCorpus or SubCorpus: "+itemPath);
			return null;
		}

		QueryResult result;
		try {
			result = corpus.query(new Query(Query.fixQuery(query)), "getPositions", false);
			int[] starts = result.getStarts();
			result.drop();
			return starts;
		} catch (Exception e) {
			Log.severe(Log.toString(e));
		}
		return new int[0];
	}

	@Override
	public EditionResult getEditionsForPosition(String itemPath, String editionName, String[] textIds, ArrayList<String> editionNames, int position) {

		String[] split = editionName.split(" - ");
		if (split.length == 2) {
			editionName = split[1];
		}

		Object o = Corpora.get(itemPath);
		MainCorpus corpus = null;
		if (o instanceof MainCorpus) {
			corpus = (MainCorpus)o;
		} else if (o instanceof Subcorpus) {
			corpus = ((Subcorpus)o).getMainCorpus();
		} else {
			Log.severe("Error: path is not a MainCorpus or SubCorpus: "+itemPath);
			return null;
		}

		String[] ids;
		try {
			ids = getWordIdAndTextId(corpus, position);
		} catch (Exception e) {
			Log.severe(Log.toString(e));
			return null;
		} 
		Text text = corpus.getText(ids[1]);
		Edition edition = text.getEdition(editionName);
		Page page = edition.getPageForWordId(ids[0]);

		EditionResult result = new EditionResult();
		result.setWordId(ids[0]);
		result.setContent(getStringValue(page));					
		result.setBaseId(itemPath);
		result.setTextId(ids[1]);
		result.setCurrentPage(page.getName());
		result.setTotalPage(edition.getNumPages());
		result.setName(edition.getName());
		result.setBiblio(text.getBiblioPath());
		result.setPDF(text.getPDFPath());
		return result;
	}

	@Override
	public ArrayList<EditionResult> getEditionByName(String baseId,
			String textid, ArrayList<String> editions) throws PermissionException {
		Log.info("[EditionServiceImpl.getEditionByName] start. baseId="+baseId+", textId="+textid);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return null; 
		}

		Object o = Corpora.get(baseId);
		MainCorpus srccorpus = null;
		if (o instanceof MainCorpus) {
			srccorpus = (MainCorpus)o;
		} else if (o instanceof Subcorpus) {
			srccorpus = ((Subcorpus)o).getMainCorpus();
		} else {
			Log.severe("Error: path is not a MainCorpus or SubCorpus: "+baseId);
			return null;
		}

		ArrayList<EditionResult> results = new ArrayList<EditionResult>();
		// int i = 0;
		for (String couple : editions) {
			String[] split = couple.split(" - ");
			String tmpcorpuspath = baseId;
			String editionName;
			if (split.length == 2) {
				tmpcorpuspath = "/"+split[0];
				editionName = split[1];
			}
			else
				editionName = couple;

			//System.out.println("get HTML for couple "+tmpcorpuspath+" - "+editionName);
			o = Corpora.get(tmpcorpuspath);
			if(!(o instanceof MainCorpus)) {
				Log.severe("Error: path is not a MainCorpus: "+tmpcorpuspath);
				return null;
			}
			Corpus corpus = (MainCorpus)o;

			Page page;
			Text text;
			Edition edition;
			String path;

			text = srccorpus.getText(textid);
			edition = text.getEdition(editionName);
			page = edition.getFirstPage();
			path = "/" + srccorpus.getName() + "/" + text.getName();

			if (!user.can(path, new EditionPermission())) {
				ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "backToText", baseId, textid);
				throw new PermissionException("Insufficient permissions to get this edition.");
			}
			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "backToText", baseId, textid);

			EditionResult result = new EditionResult();
			result.setContent(getStringValue(page));
			result.setBaseId(baseId);
			result.setTextId(text.getName());
			result.setCurrentPage(page.getName());
			result.setTotalPage(edition.getNumPages());
			result.setName(edition.getName());
			result.setBiblio(text.getBiblioPath());
			result.setPDF(text.getPDFPath());
			results.add(result);
		}
		return results;
	}

	@Override
	public Boolean hasPageWithID(String baseId, String textid, String editionName,
			String pageid) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return null; 
		}
		
		Object o = Corpora.get(baseId);
		MainCorpus corpus = null;
		if (o instanceof MainCorpus) {
			corpus = (MainCorpus)o;
		} else if (o instanceof Subcorpus) {
			corpus = ((Subcorpus)o).getMainCorpus();
		} else {
			Log.severe("Error: path is not a MainCorpus nor a SubCorpus: "+baseId);
			return null;
		}

		Text text = corpus.getFirstText();
		if (textid != null && textid.length() > 0) { // select text if textid is specified
			Text t = corpus.getText(textid);
			if (t != null)	text = t;
		}
		
		String[] split = editionName.split(" - ");
		if (split.length == 2) {
			editionName = split[1];
		}

		Edition edition = text.getEdition(editionName);
		if (edition == null) {
			Log.severe("Error: edition not found: "+editionName);
			return null;
		}
		
		return edition.get(pageid) != null;
	}

	@Override
	public ArrayList<String> getDefaultEditions(String corpusID) throws PermissionException {
		
		Log.info("[EditionServiceImpl.getDefaultEditions] start. corpusID="+corpusID);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return null; 
		}

		Object o = Corpora.get(corpusID);
		MainCorpus srccorpus = null;
		
		if (o instanceof MainCorpus) {
			srccorpus = (MainCorpus)o;
		} else if (o instanceof Subcorpus) {
			srccorpus = ((Subcorpus)o).getMainCorpus();
		} else {
			Log.severe("Error: path is not a MainCorpus or SubCorpus: "+corpusID);
			return null;
		}

		Base base = srccorpus.getBase();
		Node corpusElem = srccorpus.getSelfElement();
		Element editionsElem = base.getBaseParameters().getEditionsElement((Element) corpusElem);
		String defaultEditions = editionsElem.getAttribute("default");
		String[] split = defaultEditions.split(",");
		ArrayList<String> editions = new ArrayList<String>();
		for (String s : split) {
			editions.add(s);
		}
		
		return editions;
	}
}