package org.txm.web.server.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.txm.Toolbox;
import org.txm.functions.vocabulary.LineComparator.SortMode;
import org.txm.functions.vocabulary.Vocabulary;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.Part;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.query.Query;
import org.txm.utils.logger.Log;
import org.txm.web.aas.authorization.permissions.IndexPermission;
import org.txm.web.client.services.PortalService;
import org.txm.web.client.services.VocabularyService;
import org.txm.web.exceptions.PermissionException;
import org.txm.web.server.TxmUser;
import org.txm.web.server.statics.ActivityLog;
import org.txm.web.server.statics.Corpora;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.server.statics.TxmConfig;
import org.txm.web.shared.Actions;
import org.txm.web.shared.Keys;
import org.txm.web.shared.VocabularyParam;
import org.txm.web.shared.VocabularyResult;

/**
 * @author vchabanis, mdecorde
 *
 */
public class VocabularyServiceImpl extends PortalService implements VocabularyService {

	private static final long serialVersionUID = -5502130451900747951L;

	private static HashMap<String, Vocabulary> results = new HashMap<String, Vocabulary>();

	public static int cleanResults(String id) {
		int n = 0;
		HashSet<String> keys = new HashSet<String>();
		keys.addAll(results.keySet());
		for (String k : keys) {
			if (k.startsWith(id)) {
				results.remove(k);
				n++;
			}
		}
		return n;
	}

	//TODO remove it
	@Deprecated
	@Override
	public ArrayList<String[]> vocabulary(VocabularyParam params) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "vocabulary", params.getCorpusPath(), params.getQuery());
			return new ArrayList<String[]>(); 
		}
		ArrayList<String[]> result = new ArrayList<String[]>();

		Corpus corpus = (Corpus)user.askEntity(params.getCorpusPath(), Actions.INDEX);
		if(corpus == null)
		{
			Log.severe("no permission");
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "vocabulary", params.getCorpusPath(), params.getQuery());
			return new ArrayList<String[]>(); 
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "vocabulary", params.getCorpusPath(), params.getQuery());
		Query query = new Query(Query.fixQuery(params.getQuery()));
		ArrayList<Property> props = new ArrayList<Property>();
		try {
			for (String prop : params.getProperties())
				props.add(corpus.getProperty(prop.trim()));
		} catch (CqiClientException e) {
			Log.info("[vocabulary] User : " + user.getLogin() + " -> Fail, " + e.getMessage());
			e.printStackTrace();
			return new ArrayList<String[]>();
		}
		Vocabulary vocabulary;
		try{
			vocabulary = new Vocabulary(corpus, query, props);
		} catch(Exception e){
			Log.severe(Log.toString(e));
			return result;
		}
		vocabulary.sortLines(SortMode.FREQUNIT, true);
		List<org.txm.functions.vocabulary.Line> lines = vocabulary.getLines(0, vocabulary.getV());
		for (org.txm.functions.vocabulary.Line l : lines) {
			String[] t = {l.getSignature(), 
					String.valueOf(l.getFrequency())};
			result.add(t);
		}	
		Log.info("[vocabulary] User : " + user.getLogin() + " -> Success");
		return result;
	}

	@Override
	/**
	 * call the index functionnality
	 */
	public VocabularyResult index(VocabularyParam params) throws PermissionException {
		VocabularyResult result = new VocabularyResult();
		result.setParams(params);
		Log.info("[VocabularyServiceImpl.VocabularyResult] start. params="+params);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "index", params.getCorpusPath(), params.getQuery(), params.getProperties());
			result.error = "session";
			return result; 
		}

		int fmin = params.filterFmin;
		int fmax = params.filterFmax;
		int vmax = params.filterVmax;
		int nbperpage = params.filterNbPerPage;

		Object entity = user.askEntity(params.getCorpusPath(), new IndexPermission());
		if (entity == null) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "index", params.getCorpusPath(), params.getQuery());
			result.error = "permission";
			throw new PermissionException("Insufficient permissions to use Index with "+params.getCorpusPath());
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "index", params.getCorpusPath(), params.getQuery(), params.getProperties());
		Corpus corpus;
		if (entity instanceof Partition)
			corpus = ((Partition)entity).getCorpus();
		else
			corpus = ((Corpus)entity);

		Query query = new Query(Query.fixQuery(params.getQuery()));
		ArrayList<Property> props = new ArrayList<Property>();
		try {
			for (String prop : params.getProperties()) {
				//System.out.println("** Create property: '"+prop+"'");
				props.add(corpus.getProperty(prop.trim()));
			}
		} catch (CqiClientException e) {
			Log.info("[index] User : " + user.getLogin() + " -> Fail, " + e.getMessage());
			e.printStackTrace();
			result.error = "Failed to get properties";
			return result;
		}
		String key = params.toKey();
		Vocabulary vocabulary;
		if (results.containsKey(key)) {
			vocabulary = results.get(key);
		} else {
			params.startindex = 0;
			try{
				if (entity instanceof Partition)
					vocabulary = new Vocabulary((Partition)entity, query, props);
				else
					vocabulary = new Vocabulary(corpus, query, props);
			} catch(Exception e){
				Log.severe(Log.toString(e));
				result.error = Toolbox.getCqiClient().getLastError();
				if(result.error.startsWith("No error"));
				result.error = e.getMessage();
				return result;
			}
			results.put(key, vocabulary);
		}

		vocabulary.filterLines(fmin, fmax);
		vocabulary.cut(vmax);

		if (params.getSort().equals(Keys.FREQUENCE))
			vocabulary.sortLines(SortMode.FREQUNIT, true);
		else
			vocabulary.sortLines(SortMode.UNIT, false);

		if (params.startindex + params.filterNbPerPage > vocabulary.getV()) {
			params.startindex = 0;
		}
		
		int from = params.startindex;
		int to = params.startindex+params.filterNbPerPage;

		if (from < 0)
			from = 0;
		if (to < 0)
			to = 0;

		List<org.txm.functions.vocabulary.Line> lines = vocabulary.getLines(from, to);
		if (lines == null) {
			return result;
		}
		for (org.txm.functions.vocabulary.Line l : lines) {
			//			if(params.render)
			//				result.put(vocabulary.render(l), l.getFrequencies());
			//			else
			//			{
			result.put(l.toString(), l.getFrequencies());
			//			}
		}

		result.setColumns(new ArrayList<>(vocabulary.getPartnames()));
		result.setMaxFrequency(vocabulary.getFmax());
		result.setMinFrequency(vocabulary.getFmin());
		result.setSumFrequency(vocabulary.getT());
		result.setTotalOccurrences(vocabulary.getV());
//		ArrayList<String> columns = new ArrayList<String>();
//		if (entity instanceof Partition) {
//			Partition partition = (Partition)entity;
//			for (Part p : partition.getParts()) {
//				try {
//					columns.add(p.getName()+" ("+0+"/"+p.getSize()+")");
//				} catch (CqiClientException e) {
//					e.printStackTrace();
//					columns.add(p.getName());
//				}
//			}
//		} else {
//			try {
//				columns.add(corpus.getName()+" ("+0+"/"+corpus.getSize()+")");
//			} catch (CqiClientException e) {
//				e.printStackTrace();
//				columns.add(corpus.getName());
//			}
//		}
		Log.info("[index] User : " + user.getLogin() + " -> Success");
		return result;
	}

	@Override
	public ArrayList<String> getColumnTitles(String entity) {
		Log.info("[VocabularyServiceImpl.getColumnTitles] start. entity="+entity);

		ArrayList<String> columns = new ArrayList<String>();

		if (!Corpora.containsKey(entity)) {
			return null;
		}
		
		if (Corpora.get(entity) instanceof Partition) {
			Partition partition = (Partition)Corpora.get(entity);
			for (Part p : partition.getParts()) {
				try {
					columns.add(p.getName()+" ("+p.getSize()+")");
				} catch (CqiClientException e) {
					e.printStackTrace();
					columns.add(p.getName());
				}
			}
		} else {
			columns.add("F");
		}
		return columns;
	}

	public static void freeCaches(String corpusname) {
		HashSet<String> keys = new HashSet<String>();
		keys.addAll(results.keySet());
		for (String key : keys) {
			Corpus corpus = results.get(key).getCorpus();
			if (corpus != null && corpus.getName().equals(corpusname))
				results.remove(key);
		}
	}

	@Override
	public String export(VocabularyParam params) {
		String ID = ""+new java.util.Random().nextInt(999999999);
		try {	
			params.startindex = 0;
			params.filterNbPerPage = 999999999;
			VocabularyResult result = index(params); // do the permissions check
			if (result == null) {
				Log.warning("[VocabularyServiceImpl.export]: failed to get index result");
				return "Error: index result not found";
			}

			// get frequency columns
			ArrayList<String> cols = getColumnTitles(params.getCorpusPath());

			// header line
			StringBuffer buffer = new StringBuffer(result.size()*2);
			buffer.append(params.getProperties().toString()); // header
			for (String col : cols) {// frequency cols
				buffer.append("\t"+col);
			}

			// result lines
			for (String key : result.getKeys()) {
				buffer.append("\n");
				buffer.append(key); // prop values
				for (int f : result.get(key)) // freqs
					buffer.append("\t"+f);	
			}

			//return buffer.toString();
			File appdir = new File(this.getServletContext().getRealPath("."));
			File csvFile = new File(appdir, "csv/"+ID+".txt");	
			try {
				Writer writer = new OutputStreamWriter(new FileOutputStream(csvFile), "UTF-8");
				writer.write(buffer.toString());
				writer.close();
			} catch (IOException e) { e.printStackTrace(); return "error: "+e;}
		} catch (PermissionException e) {
			Log.warning("[VocabularyServiceImpl.export]: failed to build String version of the result");
			e.printStackTrace();
			return "error: "+e;
		}

		String portal_address = TxmConfig.getProperty("portal_address");
		if (portal_address == null)
			return "error: portal_address is not set";
		return portal_address+"/csv/"+ID+".txt";
	}
}