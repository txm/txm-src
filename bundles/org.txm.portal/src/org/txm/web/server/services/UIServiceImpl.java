package org.txm.web.server.services;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.txm.objects.Text;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.utils.logger.Log;
import org.txm.web.aas.authorization.ActionFactory;
import org.txm.web.aas.authorization.Profile;
import org.txm.web.client.services.PortalService;
import org.txm.web.client.services.UIService;
import org.txm.web.server.TxmUser;
import org.txm.web.server.statics.ActivityLog;
import org.txm.web.server.statics.Corpora;
import org.txm.web.server.statics.Profiles;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.server.statics.TxmConfig;
import org.txm.web.shared.Actions;
import org.txm.web.shared.MenuBarContent;
import org.txm.web.shared.NavigationTree;
import org.txm.web.shared.NavigationTree.Type;
/**
 * Implementation of the Ui Service. Will be used to show the rigth things in the User's interface
 * @author vchabanis
 *
 */
public class UIServiceImpl extends PortalService implements UIService {

	private static final long serialVersionUID = 2646783684238025050L;
	public static String DEFAULT_LOCALE;
	
	@Override
	/**
	 * Checks if the session : 
	 *   - is associated to a user
	 *   - is associated to a logged user
	 *   - is associated to an admin account
	 *   
	 * @return the content of the menubar
	 */
	public MenuBarContent fillMenuBar() {
		Log.info("[UIServiceImpl.fillMenuBar] start.");
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "fillMenuBar");
			return null; 
		}
		// ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "fillMenuBar");
		Log.info("[fillMenuBar] User : " + user.getLogin() + " -> Success");
		//if (user.getLogin().equals("Guest") || user.getLogin().equals("Admin"))
		boolean ismemberof = true;
		for (String pname : user.getProfiles()) {
			System.out.println("test memberof: "+ user.getProfiles());
			if (pname.equals("Newbie")) {
				ismemberof = false;
				break;
			}
			else if(pname.equals("Anonymous")) {
				ismemberof = false;
				break;
			}
				
		}
		return new MenuBarContent(user.getLogin(), user.isLogged(), user.isAdmin(), ismemberof);
		//else
		//return new MenuBarContent(user.getformatedName(), user.isLogged(), user.isAdmin());
	}

	@Override
	/**
	 * return the Corpora objects that the users permission allow to see
	 */
	public NavigationTree fillSideNavTree() {
		Log.info("[UIServiceImpl.fillSideNavTree] start.");
		try {
			TxmUser user = SessionManager.getUser(this.getSessionId());
			if (user == null) {
				Log.severe("Session "+getSessionId()+" has expired");
				ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "fillSideNavTree");
				return null; 
			}
			
			// ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "fillSideNavTree");
			NavigationTree treeContent = new NavigationTree(); // root
			List<String> availableItems;
			if (user.getLogin().equals("admin")) { // admin bypass all permissions and get all corpus
				availableItems = new ArrayList<String>(Corpora.getCorporaList());
			} else {
				availableItems = user.getCorporaList();
			}
			
			for (String path : availableItems) { // ex : /DISCOURS/user:test
				if (Corpora.containsKey(path)) { // the corpus exists
					path = path.substring(1); // remove first "/"
					NavigationTree tmp = treeContent; //root
					String currentPath = "";
					for (String item : path.split("/")) { // gives : DISCOURS and user:test
						// this loop ensure thats all parents will be shown and created in the NavigationTree
						currentPath += "/" + item;
						
						int idx = item.indexOf(":");
						String ns = "";
						String name = item;
						if (idx > 0) {
							ns = item.substring(0, idx);
							name = item.substring(idx+1);
						}
						
						NavigationTree child = tmp.get(item); // try to get the node
						//System.out.println("try to get "+item+" from "+tmp);
						if (child == null) { // we must create it
							Object o = user.askEntity(currentPath, Actions.DISPLAY);
							//System.out.println("itempath: "+currentPath+" returned: "+o);
							
							if (o != null && !(o instanceof Text)) { 
								// only show corpus, subcorpus and partition and don't show items not in Corpora
								child = addNavTreeItem(tmp, name, ns, o); // append the child, the loop is done
							}
						}
						
						if (child != null) { // the child has been created or was already in the tree
							tmp = child;
						} else {
							// the treenode was not create because the user don't have the permission
							// or the item (corpus, ...) does not exists in Corpora
							// we stop the loop
							break;
						}
					}
				} else { // the corpus does not exists
					System.out.println("UIService: fillSideNavTree: the entity "+path+" exists no more in corpora, we delete !");
					user.removeEntityFromUser(path);
				}
			}
			Log.info("[fillSideNavTree] User : " + user.getLogin() + " -> Success");
			//System.out.println("Tree: "+treeContent.toString());
			
			return treeContent;
		} catch (Exception e) {
			e.printStackTrace();
			throw new NullPointerException("Could not fill the navigation tree.");
		}
	}

	@Override
	/**
	 * get the context menu that the user can see for an object of the SideNavTree
	 */
	public List<Actions> getContextMenu(String itemPath) {
		Log.info("[UIServiceImpl.getContextMenu] start. itemPath="+itemPath);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getContextMenu");
			return null; 
		}
		// ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getContextMenu");
		Log.info("[getContextMenu] User : " + user.getLogin() + " -> Success");
		return (ActionFactory.getUserActionList(user, itemPath));
	}

	@Override
	/**
	 * @return the user name associated to this session
	 */
	public String getUserName() {
		Log.info("[UIServiceImpl.getUserName] start.");
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null)
		{
			Log.severe("Session "+getSessionId()+" has expired");
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getUserName");
			return ""; 
		}
		//  ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getUserName");
		if (user.getLogin().equals("guest") || user.getLogin().equals("admin"))
			return user.getLogin();
		return user.getFormatedName();
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.web.client.services.UIService#getUserHomePage()
	 */
	public String getUserHomePage() {
		Log.info("[UIServiceImpl.getUserHomePage] start.");
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null)
		{
			Log.severe("Session "+getSessionId()+" has expired");
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getUserHomePage");
			return ""; 
		}
		if (user.getProfiles().size() > 0)
		{
			// ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getUserHomePage");
			Profile profile = Profiles.getProfile(user.getProfiles().get(0));
			return profile.getHomePage();
		}
		else
		{
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getUserHomePage");
			return "";
		}
	}

	/**
	 * Add an object to the sideNavTree
	 * @param node the parent node
	 * @param itemName the label to show
	 * @param ns the item namespace ~ the user who created the item
	 * @param itemInstance the associated object
	 * @return
	 */
	private NavigationTree addNavTreeItem(NavigationTree node, String itemName, String ns, Object itemInstance) {
		System.out.println("Append "+itemName+" ("+ns+") to "+node.getPath() + " = "+node.getPathWithoutNs());
		NavigationTree newItem;

		if(ns != null && ns.length() > 0){
			newItem = new NavigationTree(ns+":"+itemName);
			newItem.setPath(node.getPath() + "/" + ns+":"+itemName); // set full path
		}
		else{
			newItem = new NavigationTree(itemName);
			newItem.setPath(node.getPath() + "/" + itemName); // set full path
		}
		
		newItem.setNS(ns); // set namespace
		newItem.setName(itemName); // set name
		newItem.setPathWithoutNs(node.getPathWithoutNs()+"/"+itemName);
		
		if (itemInstance instanceof MainCorpus) {
			newItem.setType(Type.corpus);
		} else if (itemInstance instanceof Subcorpus) {
			newItem.setType(Type.subcorpus);
		} else if (itemInstance instanceof Partition) {
			newItem.setType(Type.partition);
		} else {
			newItem.setType(Type.other);
		}
		
		node.add(newItem);
		return newItem;
	}	

	@Override
	public String getUserMail() {
		Log.info("[UIServiceImpl.getUserMail] start.");
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null)
		{
			Log.severe("Session "+getSessionId()+" has expired");
			// ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getUserMail");
			return ""; 
		}
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getUserMail");
		return user.getMail();
	}

//	@Override
//	public String getUserHelpPage() {
//		Log.info("[UIServiceImpl.getUserHelpPage] start.");
//		TxmUser user = SessionManager.getUser(this.getSessionId());
//		if (user == null)
//		{
//			Log.severe("Session "+getSessionId()+" has expired");
//			// ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getUserHelpPage");
//			return ""; 
//		}
//		if (user.getProfiles().size() > 0)
//		{
//			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getUserHelpPage");
//			Profile profile = Profiles.getProfile(user.getProfiles().get(0));
//			return profile.getHelpPage();
//		}
//		else
//		{
//			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getUserHelpPage");
//			return "";
//		}
//	}
	
//	@Override
//	public String getUserContactPage() {
//		Log.info("[UIServiceImpl.getUserContactPage] start.");
//		TxmUser user = SessionManager.getUser(this.getSessionId());
//		if (user == null) {
//			Log.severe("Session "+getSessionId()+" has expired");
//			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getUserContactPage");
//			return ""; 
//		}
//		if (user.getProfiles().size() > 0) {
//			Profile profile = Profiles.getProfile(user.getProfiles().get(0));
//			// ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getUserContactPage");
//			return profile.getContactPage();
//		}
//		else {
//			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getUserContactPage");
//			return "";
//		}
//	}

	@Override
	public String getUserPage(String name, String locale) {
		
		if (DEFAULT_LOCALE == null) {
			DEFAULT_LOCALE = TxmConfig.getProperty("default_locale");
			if (DEFAULT_LOCALE == null) {
				DEFAULT_LOCALE = "en";
				Log.warning("No default locale set");
			}
		}
		
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getUserHomePage");
			return ""; 
		}
		
		if (user.getProfiles().size() > 0) {
			if (locale == null || locale.equals("default") || locale.length() < 2) // gwt value for default locale
			{
				try {
					locale = this.getThreadLocalRequest().getLocale().toString().substring(0, 2).toLowerCase();
				} catch(Exception e) {
					Log.warning(Log.toString(e));
					locale = DEFAULT_LOCALE;
				}
			}
			
			if (locale.equals(DEFAULT_LOCALE))
				locale = "";
			else {
				if(locale.length() > 2)
					locale = locale.substring(0, 2); // keep only the country
				locale = "_"+locale;
			}
			
			Profile profile = Profiles.getProfile(user.getProfiles().get(0));
			String profilename;
			if (profile != null)
				profilename = profile.getNameWithoutExtension();
			else
				profilename = "Anonymous"; // default
			
			String realPath = this.getServletContext().getRealPath(".");
			boolean checkExistance = new File(realPath).canExecute();
			Log.info("Can test existance of pages in "+new File(realPath).getAbsolutePath()+": "+checkExistance);
			
			// test name+profile+lang
			String url = "html/"+name+"_"+profilename+locale+".jsp";
			File page = new File(realPath, url);
			if(page.exists() && page.canRead())
				return url;
			
			if (!checkExistance) // I can't check if the HTML file exists
				return url;
			
			// test name+profile
			url = "html/"+name+"_"+profilename+".jsp";
			page = new File(realPath, url);
			if(page.exists() && page.canRead())
				return url;
			
			// test name+locale
			url = "html/"+name+locale+".jsp";
			page = new File(realPath, url);
			if(page.exists() && page.canRead())
				return url;
			
			// test name
			url = "html/"+name+".jsp";
			page = new File(realPath, url);
			if (page.exists() && page.canRead())
				return url;
			else {
				Log.severe("Could not find page with : name="+name+", profile="+profilename+" locale="+locale+" in "+page.getAbsolutePath());
				return "404: "+url;
			}
		}
		else {
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getUserContactPage");
			return "";
		}
	}

	String s404 = "404";
	String s500 = "500";
	@Override
	public String getPage(String path, String locale) {
		ActivityLog.add(getSessionId(), getRemoteAddr(),"null", "getPage");
		if(DEFAULT_LOCALE == null) {
			DEFAULT_LOCALE = TxmConfig.getProperty("default_locale");
			if(DEFAULT_LOCALE == null) {
				DEFAULT_LOCALE = "en";
				Log.warning("No default locale set");
			}
		}
		
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getPage");
			return "session"; 
		}
		
		if (user.getProfiles().size() > 0) {
			if (locale == null || locale.equals("default") || locale.length() < 2) { // gwt value for default locale
				try {
					locale = this.getThreadLocalRequest().getLocale().toString().substring(0, 2).toLowerCase();
				} catch(Exception e) {
					Log.warning(Log.toString(e));
					locale = DEFAULT_LOCALE;
				}
			}
			
			if (locale.equals(DEFAULT_LOCALE)) {
				locale = "";
			} else {
				if(locale.length() > 2)
					locale = locale.substring(0, 2); // keep only the country
				locale = "_"+locale;
			}
			System.out.println("GET PAGE "+path);
			Profile profile = Profiles.getProfile(user.getProfiles().get(0));
			String profilename;
			if(profile != null)
				profilename = profile.getNameWithoutExtension();
			else
				profilename = "Anonymous"; // default
			
			String realPath = TxmConfig.getProperty("htmlBase");
			boolean checkExistance = new File(realPath).canExecute(); // directory
			Log.severe("Can test existance of pages in "+new File(realPath).getAbsolutePath()+": "+checkExistance);
			if (!checkExistance) // I can't check if the HTML file exists
				return s500;
			
			// test name+profile+lang
			String url = path+"_"+profilename+locale+".html";
			File page = new File(realPath, url);
			if (page.exists() && page.canRead())
				return EditionServiceImpl.getStringValue(page);
			

			
			// test name+profile
			url = path+"_"+profilename+".html";
			page = new File(realPath, url);
			if(page.exists() && page.canRead())
				return EditionServiceImpl.getStringValue(page);
			
			// test name+locale
			url = path+locale+".html";
			page = new File(realPath, url);
			if (page.exists() && page.canRead())
				return EditionServiceImpl.getStringValue(page);
			
			// test name
			url = path+".html";
			page = new File(realPath, url);
			if (page.exists() && page.canRead())
				return EditionServiceImpl.getStringValue(page);
			else {
				Log.severe("Could not find page with parameters: name="+path+", profile="+profilename+" locale="+locale+" in "+page.getAbsolutePath());
				return s404;
			}
		} else {
			ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "getUserContactPage");
			return "profile";
		}
	}
}
