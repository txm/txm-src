package org.txm.web.server.services;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import org.txm.Toolbox;
import org.txm.functions.ca.CA;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.stat.StatException;
import org.txm.stat.data.LexicalTable;
import org.txm.utils.logger.Log;
import org.txm.web.aas.authorization.permissions.AFCPermission;
import org.txm.web.client.services.AFCService;
import org.txm.web.client.services.PortalService;
import org.txm.web.server.TxmUser;
import org.txm.web.server.statics.ActivityLog;
import org.txm.web.server.statics.Corpora;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.shared.AFCParams;
import org.txm.web.shared.ValeurPropreResultLine;

public class AFCServiceImpl extends PortalService implements AFCService {

	/**
	 *
	 */
	private static final long serialVersionUID = 4919664239919764796L;
	private static HashMap<String, CA> results = new HashMap<String, CA>();

	public static int cleanResults(String id) {
		int n = 0;
		HashSet<String> keys = new HashSet<String>();
		keys.addAll(results.keySet());
		for (String k : keys) {
			if (k.startsWith(id)) {
				results.remove(k);
				n++;
			}
		}
		return n;
	}

	@Override
	public String analysisComputing(AFCParams params) {
		try {
			Log.info("[AFCServiceImpl.analysisComputing] start. params="
					+ params);
			TxmUser user = SessionManager.getUser(this.getSessionId());
			if (user == null) {
				ActivityLog.warning(getSessionId(), getRemoteAddr(),"null", "analysisComputing", params.getPath(), params.getID());
				Log.severe("Session " + getSessionId() + " has expired");
				return "";
			}

			if(!Toolbox.isStatEngineInitialized())
			{
				Log.severe("[AFCServiceImpl.analysisComputing]: no stat engine");
				return "no stat engine";
			}

			if(!user.can(params.getPath(), new AFCPermission()))
			{
				ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "analysisComputing", params.getPath(), params.getID());
				return "no permission";
			}

			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "analysisComputing", params.getPath(), params.getID());

			Partition partition = (Partition) Corpora.get(params.getPath());
			Property prop = null;

			for (Property p : ((Corpus) partition.getCorpus()).getOrderedProperties()) {
				if (p.getName().equals(params.getProperty())) {
					prop = p;
				}
			}
			int fmin = params.getFmin();
			int vmax = params.getVmax();

			LexicalTable table = partition.getLexicalTable(prop, fmin);
			int nrows = table.getNRows();
			if (vmax > 4 && vmax < nrows) {
				table.sort(fmin, true);
				
				int[] lines = new int[nrows-vmax];
				int n = 0;
				for (int i = vmax ; i < nrows ; i++) lines[n++] = i;
				table.removeRows(lines);
				//table.cut(vmax);
			}
			partition.storeResult(table);
			
			CA ca = new CA(table);
			// Save results
			results.put(getSessionId() + params.getID(), ca);
			//String repository = System.getProperty("user.dir");
			String fileName = partition.getName() + (new Date()).getTime() + ".svg";

			//            Log.info("WAR PATH = " + TxmConfig.warpath);
			//            Log.info("INFO = " + getServletInfo());
			//            Log.info("CONFIG = " + getServletConfig().getServletContext().getContextPath());
			Log.info("CONFIG = " + getServletConfig().getServletContext().getRealPath(""));

			String path = getServletConfig().getServletContext().getRealPath("");
			//File svgDir = new File(TxmConfig.warpath + "/data/svg/");
			File svgDir = new File(path + "/data/svg/");
			if (!svgDir.exists()){
				svgDir.mkdir();
			}
			File userSvgDir = new File(path + "/data/svg/" + getSessionId() + "/");
			if (!userSvgDir.exists()) {
				userSvgDir.mkdir();
			}
			File file = new File(path+ "/data/svg/" + getSessionId()+"/"+fileName);

			ca.toSVGFactorialMap(file,params.isShowCol(), params.isShowLine(), params.getFirstDim(), params.getSecondDim());
			//perThreadRequest.get().get
			//System.out.println("SERVEUR NAME = " + perThreadRequest.get().getServerName());
			//System.out.println("CONTEXT PATH = " + perThreadRequest.get().getContextPath());
			//System.out.println("CONTEXT PATH = " + getServletContext().getContextPath());
			//System.out.println("PATH " + perThreadRequest);
			return getSessionId()+"/"+fileName;
		} catch (StatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CqiClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ArrayList<ValeurPropreResultLine> getValeursPropres(AFCParams params) {

		Log.info("[AFCServiceImpl.getValeursPropres] start. params="
				+ params);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session " + getSessionId() + " has expired");
			return new ArrayList<ValeurPropreResultLine>();
		}

		if(!user.can(params.getPath(), new AFCPermission()))
		{
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getValeursPropres", params.getPath(), params.getID());
			return new ArrayList<ValeurPropreResultLine>();
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getValeursPropres", params.getPath(), params.getID());

		ArrayList<ValeurPropreResultLine> result = new ArrayList<ValeurPropreResultLine>();
		CA ca = results.get(getSessionId() + params.getID());
		if (ca == null) {
			Log.info("[AFCServiceImpl.getValeursPropres] ca null for " + getSessionId() + params.getID());
			analysisComputing(params);
			ca = results.get(getSessionId() + params.getID());
			ca.getRowInfos();
		}

		try {
			double[] vpropres = ca.getValeursPropres();
			double sum = ca.getValeursPropresSum();
			for (int i = 0; i < vpropres.length; i++) {
				double vp = vpropres[i];
				double percent = vp / sum * 100;
				ValeurPropreResultLine vprl = new ValeurPropreResultLine();
				vprl.setFactor(i + 1);
				vprl.setValue(vp);
				vprl.setPercent(percent);
				result.add(vprl);
			}
		} catch (StatException e) {
			ActivityLog.warning(getSessionId(), getRemoteAddr(),user.getLogin(), "getValeursPropres", params.getPath(), params.getID());
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public LinkedHashMap<String, List<Double>> getRowInfos(AFCParams params) {
		Log.info("[AFCServiceImpl.getRowInfos] start. params=" + params);
		LinkedHashMap<String, List<Double>> list = new LinkedHashMap<String, List<Double>>();
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session " + getSessionId() + " has expired");
			return list;
		}

		if (!user.can(params.getPath(), new AFCPermission())) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getRowInfos", params.getPath(), params.getID());
			return list;
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getRowInfos", params.getPath(), params.getID());

		CA ca = results.get(getSessionId() + params.getID());
		Object[] rowInfos = ca.getRowInfos();
		for (Object o : rowInfos) {
			ArrayList<Object> row = (ArrayList<Object>) o;
			List<Double> l = new ArrayList<Double>();
			list.put(row.get(0).toString(), l);
			for (int i = 1 ; i < row.size() ; i++) {
				Object o2 = row.get(i);
				if (!(o2 instanceof String)) {
					l.add((Double)o2);
				}
			}
		}

		return list;
	}

	@Override
	public String[] getRowInfoTitles(AFCParams params) {
		Log.info("[AFCServiceImpl.getRowInfoTitles] start. params=" + params);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session " + getSessionId() + " has expired");
			return new String[0];
		}

		if (!user.can(params.getPath(), new AFCPermission())) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getRowInfoTitles", params.getPath(), params.getID());
			return new String[0];
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getRowInfoTitles", params.getPath(), params.getID());

		CA ca = results.get(getSessionId() + params.getID());
		return ca.getRowInfosTitles();
	}

	public String[] getColInfoTitles(AFCParams params) {
		Log.info("[AFCServiceImpl.getColInfoTitles] start. params=" + params);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session " + getSessionId() + " has expired");
			return new String[0];
		}

		if (!user.can(params.getPath(), new AFCPermission())) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getColInfoTitles", params.getPath(), params.getID());
			return new String[0];
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getColInfoTitles", params.getPath(), params.getID());

		CA ca = results.get(getSessionId() + params.getID());
		return ca.getColInfosTitles();
	}

	public LinkedHashMap<String, List<Double>> getColInfos(AFCParams params) {
		Log.info("[AFCServiceImpl.getColInfos] start. params=" + params);
		LinkedHashMap<String, List<Double>> list = new LinkedHashMap<String, List<Double>>();
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session " + getSessionId() + " has expired");
			return list;
		}

		if (!user.can(params.getPath(), new AFCPermission())) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getColInfos", params.getPath(), params.getID());
			return list;
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getColInfos", params.getPath(), params.getID());

		CA ca = results.get(getSessionId() + params.getID());
		Object[] rowInfos = ca.getColInfos();
		for (Object o : rowInfos) {
			ArrayList<Object> row = (ArrayList<Object>) o;
			List<Double> l = new ArrayList<Double>();
			list.put(row.get(0).toString(), l);
			for (int i = 1 ; i < row.size() ; i++) {
				Object o2 = row.get(i);
				if (!(o2 instanceof String)) {
					l.add((Double)o2);
				}
			}
		}

		return list;
	}

	public String singularValues(AFCParams params) {
		try {

			TxmUser user = SessionManager.getUser(this.getSessionId());
			if (user == null) {
				Log.severe("Session " + getSessionId() + " has expired");
				return "empty.html";
			}

			if(!user.can(params.getPath(), new AFCPermission()))
			{
				ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getColInfos", params.getPath(), params.getID());
				return "empty.html";
			}

			ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "getColInfos", params.getPath(), params.getID());

			CA ca = results.get(getSessionId() + params.getID());
			Partition partition = (Partition) Corpora.get(params.getPath());
			String fileName = partition.getName() + (new Date()).getTime()
					+ ".svg";

			String path = getServletConfig().getServletContext().getRealPath("");
			File file = new File(path+ "/data/svg/" + getSessionId()+"/" + fileName);
			ca.toSVGSingularValues(file);


			return getSessionId()+"/"+fileName;
		} catch (StatException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String exportCA(AFCParams params) {
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session " + getSessionId() + " has expired");
			return "empty.html";
		}

		if (!user.can(params.getPath(), new AFCPermission())) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "exportCA", params.getPath(), params.getID());
			return "empty.html";
		}

		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "exportCA", params.getPath(), params.getID());

		String ID = ""+new java.util.Random().nextInt(999999999);
		File appdir = new File(this.getServletContext().getRealPath("."));

		File txtFile = new File(appdir, "csv/CA_"+ID+".txt");
		txtFile.getParentFile().mkdirs();

		CA ca = results.get(getSessionId() + params.getID());
		ca.toTxt(txtFile.getAbsoluteFile(), "UTF-8", "\t", "");

		return "csv/"+txtFile.getName();
	}
}
