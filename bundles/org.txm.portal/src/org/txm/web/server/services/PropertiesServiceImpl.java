package org.txm.web.server.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Part;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.query.Query;
import org.txm.utils.IOUtils;
import org.txm.utils.logger.Log;
import org.txm.web.aas.authorization.permissions.PropertiesPermission;
import org.txm.web.client.services.PortalService;
import org.txm.web.client.services.PropertiesService;
import org.txm.web.exceptions.PermissionException;
import org.txm.web.server.TxmUser;
import org.txm.web.server.statics.ActivityLog;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.shared.PropertiesResult;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The services for the properties function
 * @author mdecorde
 *
 */
public class PropertiesServiceImpl extends PortalService implements PropertiesService {

	private static final long serialVersionUID = 5376447030048122162L;

	@Override
	public PropertiesResult getTV(String path, String property) throws PermissionException {
		Log.info("[PropertiesServiceImpl.getTV] start. path="+path+", property: "+property);
		TxmUser user = SessionManager.getUser(this.getSessionId());
		if (user == null) {
			Log.severe("Session "+getSessionId()+" has expired");
			return null;
		}
		
		Object obj = user.askEntity(path, new PropertiesPermission());
		if (obj == null) {
			ActivityLog.severe(getSessionId(), getRemoteAddr(),user.getLogin(), "getTV", path, property);
			throw new PermissionException("Insufficient permissions to use Properties with "+path);
		}
		
		ActivityLog.add(getSessionId(), getRemoteAddr(),user.getLogin(), "properties", path, property);
		
		PropertiesResult dr = new PropertiesResult();
		
		if (obj instanceof MainCorpus) {
			Corpus corpus = (Corpus)user.askEntity(path, new PropertiesPermission());
			//System.out.println("** getTV user "+user.getLogin()+" corpus "+corpus+" property "+property);
			try {
				File infoHTMLFile = new File(corpus.getBaseDirectory(), "Properties.html");
				if (infoHTMLFile.exists()) {
					try {
						BufferedReader reader = IOUtils.getReader(infoHTMLFile, "UTF-8");
						dr.htmlContent = "";
						String line = reader.readLine();
						while (line != null) {
							dr.htmlContent += line+"\n";
							line = reader.readLine();
						}
						reader.close();
						return dr;
					} catch (MalformedURLException e) {
						// TODO Bloc catch généré automatiquement
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Bloc catch généré automatiquement
						e.printStackTrace();
					}
				}
				dr.nWords = corpus.getSize();
				dr.nTexts = corpus.getTexts().size();
				dr.properties = new ArrayList<String>();
				for (Property p : corpus.getProperties()) dr.properties.add(p.getName());
				dr.structures = new ArrayList<String>();
				for (StructuralUnit s : corpus.getStructuralUnits()) dr.structures.add(s.getName());
				return dr;
			} catch (CqiClientException e) {
				Log.severe(Log.toString(e));
			}
			return null;
		} else if (obj instanceof Subcorpus) {
			Corpus corpus = (Corpus)user.askEntity(path, new PropertiesPermission());
			//System.out.println("** getTV user "+user.getLogin()+" corpus "+corpus+" property "+property);
			try {
				dr.nWords = corpus.getSize();
				//dr.nTexts = corpus.getTexts().size();
				
				QueryResult queryResult = corpus.query(new Query("<text>[]|[]</text>"), "TMP", false);
				dr.nTexts = queryResult.getNMatch();
				queryResult.drop();
				
				dr.properties = new ArrayList<String>();
				for (Property p : corpus.getProperties()) dr.properties.add(p.getName());
				dr.structures = new ArrayList<String>();
				for (StructuralUnit s : corpus.getStructuralUnits()) {
					dr.structures.add(s.getName());
				}
				return dr;
			} catch (CqiClientException e) {
				Log.severe(Log.toString(e));
			}
			return null;
		} else if (obj instanceof Partition) {
			Partition partition = (Partition) obj;
			try {
				dr.nWords = 0;
				List<Part> parts = partition.getParts();
				Collections.sort(parts, new Comparator<Part>() {

					@Override
					public int compare(Part arg0, Part arg1) {
						return arg0.getName().compareTo(arg1.getName());
					}
				});
				
				dr.parts = new ArrayList<String>();
				dr.partSizes = new ArrayList<Integer>();
				for (Part part : parts) {
					dr.nWords += part.getSize();
					dr.parts.add(part.getName());
					dr.partSizes.add(part.getSize());
				}
				dr.nTexts = -1;
				dr.nParts = partition.getParts().size();
				dr.properties = new ArrayList<String>();
				dr.structures = new ArrayList<String>();
				
				return dr;
			} catch (Exception e) {
				Log.severe(Log.toString(e));
			}
			return null;
		} else {
			return null;
		}
	}
}
