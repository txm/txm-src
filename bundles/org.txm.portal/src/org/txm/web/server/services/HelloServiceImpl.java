package org.txm.web.server.services;

import java.io.File;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.txm.Toolbox;
import org.txm.utils.logger.Log;
import org.txm.web.client.services.HelloService;
import org.txm.web.client.services.PortalService;
import org.txm.web.server.TxmUser;
import org.txm.web.server.statics.ActivityLog;
import org.txm.web.server.statics.Corpora;
import org.txm.web.server.statics.MailContent;
import org.txm.web.server.statics.Profiles;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.server.statics.TxmConfig;
import org.txm.web.shared.ServerInfos;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * Implementation of the HelloService
 * @author vchabanis
 *
 */
public class HelloServiceImpl extends PortalService implements HelloService {

	private static final long serialVersionUID = -5718230915866739944L;

	private boolean started = false;

	@Override
	public String sayHello() {
		return "Hello world";
	}

	@Override
	/**
	 * Initialize the toolbox if not
	 * Fill the Corpora
	 * Fill the profiles list
	 */
	public ServerInfos startSession() throws Exception {
		Log.info("[HelloServiceImpl.startSession] start.");
		
		// Load the framwork factory
//	    ServiceLoader loader = ServiceLoader.load( FrameworkFactory.class );
//
//	    FrameworkFactory factory = ( FrameworkFactory ) loader.iterator( ).next( );
//	    
//	    // Create a new instance of the framework
//	    Framework framework = factory.newFramework( null );
//	    framework.init( );	    
//	    framework.start( );
//      BundleContext bc = framework.getBundleContext();
//        
//	    ServiceTracker serviceTaskTracker = new ServiceTracker(bc, 
//	    		bc.createFilter("(objectClass=*ServiceTasks)"), null);
//        
//	    InternalPlatform.getDefault().start(bc);
//		System.out.println("CONTEXT: "+bc);
		//Activator.getDefault().start(bc);
		
		initWarPath();
		ServerInfos infos = new ServerInfos();
		if (Toolbox.isInitializing()) {
			Log.severe("[startSession] Starting session : " + this.getSession().getId() + " -> Fail : Toolbox is initializing");
			infos.error = "TBX: initializing";
			return infos;
		}
		
		if (!startToolbox()) {
			Log.severe("[startSession] Starting session : " + this.getSession().getId() + " -> Fail : Toolbox not initialized");
			infos.error = "TBX: not initialized";
			return infos;
		}
		if (Toolbox.workspace == null) {
			Log.severe("[startSession] Starting session : " + this.getSession().getId() + " -> Fail : Workspace is null");
			infos.error = "TBX: workspace not initialized";
			return infos;
		}
		Log.info("[startSession] ToolBox started.");
		
		Log.info("[startSession] Starting session : " + this.getSessionId());
		TxmUser user = SessionManager.createUser(getSessionId());
		
		Log.info("[startSession] load mail messages : " + this.getSessionId());
		MailContent.load();
		
		Log.info("[startSession] loading profiles..." );
		if (!Profiles.start()) {
			Log.severe("[startSession] could not load profiles.");
			started = false;
		} else {
			started = true;
			Log.info("[startSession] Profiles loaded.");
		}

		Log.info("[startSession] getting server infos ");
		infos.rEnable = Toolbox.isStatEngineInitialized();
		infos.portaltitle = TxmConfig.getProperty("portal_title", "");
		infos.portalname = TxmConfig.getProperty("portal_name");
		infos.portallongname = TxmConfig.getProperty("portal_longname");
		infos.maintenance = AdminServiceImpl.isMaintenance();
		infos.mailEnable = !TxmConfig.getProperty("nomail").equals("true");
		infos.inscriptionEnable = !TxmConfig.getProperty("noinscription").equals("true");
		infos.ready = started;
		infos.contact = TxmConfig.getProperty("mail.contact");
		infos.showPrivatePages = "true".equals(TxmConfig.getProperty("showPrivatePages"));
		infos.showPublicPages = "true".equals(TxmConfig.getProperty("showPublicPages"));
		infos.defaultEditionPanelSize = TxmConfig.getProperty("default_edition_page_width");
		infos.baseURL = TxmConfig.getProperty("portal_address");
		infos.expo = "true".equals(TxmConfig.getProperty("mode_expo"));
		
		if (user != null) {
			infos.login = user.getLogin();
		} else {
			infos.login = "guest";
		}
		
		if (started)
			ActivityLog.add(getSessionId(), getRemoteAddr(),"null", "Session started");
		else
			ActivityLog.severe(getSessionId(), getRemoteAddr(),"null", "startSession");

		return infos;
	}

	public static boolean startToolbox() {
		Log.info("[startToolbox] Starting toolbox...");
		try{
			String txmpropertiesfile = TxmConfig.getProperty("txm.properties");
			if (txmpropertiesfile == null || txmpropertiesfile.trim().length() == 0)
			{//was not set in txmweb.conf
				Log.warning("[startSession] 'txm.properties' is not set in txmweb.conf file using: "+TxmConfig.getDefaultToolboxPropertiesFile());
				txmpropertiesfile = TxmConfig.getDefaultToolboxPropertiesFile();
			}

			if (!Toolbox.isInitialized()) {
				Log.info("[startSession] Init toolbox with file: " + txmpropertiesfile);
				Toolbox.initialize(new File(txmpropertiesfile));
			}

			if (!Toolbox.isInitialized()){	
				Log.severe("[startSession] Toolbox could not be started, exiting.");
				throw new Exception("Toolbox init error.");
			}

			Corpora.start();
		} catch (Exception e) {
			Log.severe("[startSession] Starting toolbox -> Fail : " + e.getMessage());
			e.printStackTrace();
			return false;
		}			
		return Toolbox.isInitialized();
	}

	public void initWarPath() {
		if (TxmConfig.warpath == null || TxmConfig.warpath.trim().length() == 0) {
			Log.info("[startSession] Initializing warpath...");
			TxmConfig.warpath = this.getServletContext().getRealPath(".");
			
			//Log.info("Current dir: "+new File(".").getAbsolutePath());
			//for(Object p : System.getProperties().keySet())
			//Log.info(p+": "+System.getProperty((String)p));

			if (new File(TxmConfig.warpath).getName().equals("."))
				TxmConfig.warpath = new File(TxmConfig.warpath).getParentFile().getName();
			Log.info("[startSession] warpath="+TxmConfig.warpath);
		}
	}

	private static String news = "";
	private static Date lastNews = new Date();
	@Override
	public ServerInfos getServerInfos() {

		SessionManager.autoDisconnectInactiveUsers(); // filter inactive users
		TxmUser user = SessionManager.getUser(getSessionId()); // 
		
		ServerInfos infos = new ServerInfos();
		infos.rEnable = Toolbox.isStatEngineInitialized();
		infos.portaltitle = TxmConfig.getProperty("portal_title", "");
		infos.portalname = TxmConfig.getProperty("portal_name", "no_portal_name_set");
		infos.portallongname = TxmConfig.getProperty("portal_longname", "no_portal_longname_set");
		infos.maintenance = AdminServiceImpl.isMaintenance();
		infos.mailEnable = !"true".equals(TxmConfig.getProperty("nomail", "true"));
		infos.inscriptionEnable = !"true".equals(TxmConfig.getProperty("noinscription", "true"));
		infos.ready = started;
		infos.contact = TxmConfig.getProperty("mail.contact");
		infos.showPrivatePages = "true".equals(TxmConfig.getProperty("showPrivatePages"));
		infos.showPublicPages = "true".equals(TxmConfig.getProperty("showPublicPages"));
		infos.defaultEditionPanelSize = TxmConfig.getProperty("default_edition_page_width");
		infos.baseURL = TxmConfig.getProperty("portal_address");
		infos.expo = "true".equals(TxmConfig.getProperty("mode_expo"));
		//Log.warning("BASE URL: "+infos.baseURL);
		
		if (user != null) {
			infos.login = user.getLogin();
		} else {
			infos.login = "guest";
			// SessionManager.createUser(getSessionId()); // assign the guest use to the new session
		}
		infos.news = news;
		infos.lastNews = lastNews;
		
		return infos;
	}
	
	public static void updateNews(String message)
	{
		news = message;
		lastNews = new Date();
	}
}
