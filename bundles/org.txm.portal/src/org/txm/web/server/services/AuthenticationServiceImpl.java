package org.txm.web.server.services;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import nl.captcha.Captcha;

import org.txm.utils.logger.Log;
import org.txm.web.aas.authentication.LoginModule;
import org.txm.web.aas.authentication.TxmLoginModule;
import org.txm.web.aas.authorization.Profile;
import org.txm.web.client.services.AuthenticationService;
import org.txm.web.client.services.PortalService;
import org.txm.web.server.Emailer;
import org.txm.web.server.TxmUser;
import org.txm.web.server.statics.ActivityLog;
import org.txm.web.server.statics.MailContent;
import org.txm.web.server.statics.PasswordCheck;
import org.txm.web.server.statics.Profiles;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.server.statics.StringFormat;
import org.txm.web.server.statics.Tools;
import org.txm.web.server.statics.TxmConfig;
import org.txm.web.server.statics.TxmSessionListener;
import org.txm.web.server.statics.UserManager;
import org.txm.web.shared.LoginResult;
import org.txm.web.shared.UserData;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class AuthenticationServiceImpl extends PortalService implements AuthenticationService {

	private static final long serialVersionUID = -5718230915866739944L;

	public static LoginModule loginModule = new TxmLoginModule();

	private static HashMap<String, TxmUser> sentActivationCodes = new HashMap<String, TxmUser>();
	public static HashMap<TxmUser, Date> toBeActivated = new HashMap<TxmUser, Date>();

	private static HashMap<String, TxmUser> sentRecorveryCodes = new HashMap<String, TxmUser>();
	private static HashMap<TxmUser, Date> toBeRecorvered = new HashMap<TxmUser, Date>();

	private static HashMap<TxmUser, String> tempSavedPassword = new HashMap<TxmUser, String>();
	private static HashMap<String, TxmUser> sentUpdateMailCodes = new HashMap<String, TxmUser>();

	private static HashMap<TxmUser, Date> toBeMailUpdated = new HashMap<TxmUser, Date>();
	private static HashMap<TxmUser, String> tempSavedMail = new HashMap<TxmUser, String>();

	public static void stop() {
		sentActivationCodes.clear();
		toBeActivated.clear();

		sentRecorveryCodes.clear();
		toBeRecorvered.clear();

		tempSavedPassword.clear();
		sentUpdateMailCodes.clear();

		toBeMailUpdated.clear();
		tempSavedMail.clear();
	}

	@Override
	public LoginResult login(String login, String password) {
		Log.info("[AuthenticationServiceImpl.login] start. password=??, login="+login);

		TxmUser user = UserManager.getUser(login);
		if (user == null && login.equals("admin")) {
			Log.info("[login] Create default admin account");
			String default_pass = TxmConfig.getProperty("default_admin_pass");
			if (default_pass == null)
				return new LoginResult(false, "InternalError: failed to create admin user: no default admin password set");

			// create user "admin"
			TxmUser adminuser = new TxmUser();
			adminuser.setLogin("admin");
			adminuser.setNames("admin", "admin");
			adminuser.setMail(TxmConfig.getProperty("mail.contact"));
			adminuser.setAllPermission();
			adminuser.setProfile("Admin");
			adminuser.setMD5(default_pass);
			adminuser.setInstitution(TxmConfig.getProperty("portal_name"));
			adminuser.setStatus("administrator");

			if (!UserManager.create(adminuser)) {
				ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "login", login);
				Log.info("[login] failed to create admin user");
				return new LoginResult(false, "InternalError: failed to create admin user");
			}

			user = adminuser;
		} else if(user == null) {
			System.out.println("test if user is in : "+toBeActivated.keySet());
			for (TxmUser testuser : toBeActivated.keySet())
				if (testuser.getLogin().equals(login)) {
					ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "login", login);
					Log.severe("[login] User login : " + login + " -> Fail: to be activated");
					return new LoginResult(false, "toBeActivated");
				}
			Log.severe("[login] User login : " + login + " -> Fail: no such user");
			return new LoginResult(false, "Wrong login or password");
		}
		System.out.println("Login with user: "+user.getLogin());

		if (AdminServiceImpl.isMaintenance() && !user.getLogin().equals("admin")) {
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "login", login);
			return new LoginResult(false, "Site under maintenance. Please come back later");
		}

		if (!user.testPassword(password)) {
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "login", login);
			Log.severe("[login] User login : " + login + " -> Fail: password!=givenpassword");
			return new LoginResult(false, "Wrong login or password");
		}
		////////
		user.setIsLogged(true);
		user.updateCorpora();
		SessionManager.addUser(getSession(), user);
		Log.info("[login] User : " + login + ", profile: "+user.getProfiles()+" -> Success");
		Profile profile = Profiles.getProfile(user.getProfiles().get(0));
		if (profile == null) {
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "login", login);
			Log.info("[login] User " + login + " has no profile -> Fail");
			return new LoginResult(false, "No profile set, contact admin.");
		}
		user.setLastConnexion(Calendar.getInstance().getTime());
		LoginResult result = new LoginResult(true, profile.getHomePage());
		System.out.println("[login] return: "+result);
		ActivityLog.add(getSession().getId(), getRemoteAddr(), login, "login");
		return result;
	}
	
	@Override
	public Boolean logout() {
		Log.info("[AuthenticationServiceImpl.logout] start.");

		TxmUser user = SessionManager.getUser(getSession().getId());

		try {
			String path = getServletConfig().getServletContext().getRealPath("") + "/data/svg/"+getSession().getId();
			Tools.deleteDirectory(new File(path));
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (user == null) {
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "no user", "logout");
			Log.severe("Session "+getSession()+" has expired");
			return false; 
		}
		Log.info("[logout] User : " + user.getLogin());
		user.setIsLogged(false);
		if (!user.save()) {
			Log.severe("Could not save user");
			return false;
		}

		cleanResults(getSession().getId());
		
		//now the session is attached to the anonymous user
		SessionManager.addUser(getSession(), UserManager.getGuestUser());
		ActivityLog.add(getSession().getId(), getRemoteAddr(), user.getLogin(), "logout");
		return true;
	}
	
	public static int cleanResults(String id) {
//		Runtime runtime = Runtime.getRuntime();
//		int mb = 1024*1024;
//		System.gc();
//		long membefore = (runtime.totalMemory() - runtime.freeMemory()) / mb;
		int n = 0;
		n += ConcordanceServiceImpl.cleanResults(id);
		n += CooccurrenceServiceImpl.cleanResults(id);
		n += AFCServiceImpl.cleanResults(id);
		n += ReferencerServiceImpl.cleanResults(id);
		n += SpecificitiesServiceImpl.cleanResults(id);
		n += TsQueryServiceImpl.cleanResults(id);
		n += VocabularyServiceImpl.cleanResults(id);
		System.out.println("Number of results released: "+n);
		if (n > 0) System.gc();
//		long memafter = (runtime.totalMemory() - runtime.freeMemory()) / mb;
//		System.out.println("Cleaned "+(membefore-memafter)+" mb with session id="+id);
		return n;
	}

	@Override
	public LoginResult subscribe(UserData data) {
		Log.info("[AuthenticationServiceImpl.subscribe] start. data="+data);

		if ("true".equals(TxmConfig.getProperty("noinscription"))) {
			return new LoginResult(false, "Inscription is not enable on this portal.");
		}
		
		String firstname = data.firstname;
		String lastname = data.lastname;
		String login = data.login;
		String password = data.password;
		String mail = data.mail;
		String captcha = data.captcha;

		if (UserManager.getUser(login) == null) {
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "subscribe", login, mail);
			return new LoginResult(false, "login_already_used");
		}

		if (toBeActivated.containsKey(login)) {
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "subscribe", login, mail);
			return new LoginResult(false, "Sorry, this login is already used, a confirmation email has already been sent");
		}

		// test captcha
		HttpServletRequest request = getThreadLocalRequest();
		HttpSession session = request.getSession();
		Captcha captcha_ = (Captcha) session.getAttribute(Captcha.NAME);
		if (captcha_.isCorrect(captcha)) {
			Log.info("[subscribe] User : captcha validation ok");
		} else {
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "subscribe", login, mail);
			Log.info("[subscribe] User : Wrong captcha");
			return new LoginResult(false, "Wrong captcha");
		}	

		//System.out.println("fi: "+firstname+" la: "+lastname+" login: "+userName+" pass: "+password+" mail: "+mail);

		if (!UserData.checkMail(mail)) {
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "subscribe", login, mail);
			Log.info("[subscribe] User : " + login + " -> mail not valid");
			return new LoginResult(false, "The given mail is not valid (xxx@yyy.zz)");
		}

		if (!PasswordCheck.CheckPasswordStrength(password)) {
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "subscribe", login, mail);
			Log.info("[subscribe] User : " + login + " -> password not enough strong");
			return new LoginResult(false, "The given password not enough strong");
		}

		if (!UserData.checkValidLogin(login)) {
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "subscribe", login, mail);
			Log.info("[subscribe] User : " + login + " -> login not valid");
			return new LoginResult(false, "Login not valid");
		}

		boolean allowMultipleAccount = TxmConfig.getProperty("allow_multiple_account_per_mail").equals("true");
		if (!allowMultipleAccount) {
			for (TxmUser testuser : UserManager.getUsers()) {
				if (testuser.getMail().equals(mail)) {
					ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "subscribe", login, mail);
					Log.severe("[subscribe] User : fail: mail already used" + mail);
					return new LoginResult(false, "MailUsed");
				}	
			}
		}

		ActivityLog.add(getSession().getId(), getRemoteAddr(), "null", "subscribe", login, mail);
		//create the user
		TxmUser user = new TxmUser();
		user.setLogin(login);
		user.setNames(firstname, lastname);
		user.setMail(mail);

		//try sending the email
		if (!sendConfirmationEmail(user, password)) {
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "subscribe", login, mail);
			Log.severe("[subscribe] User : failed to send email to" + mail);
			return new LoginResult(false, "Failed to send inscription confirmation to "+mail);
		} else {
			if (TxmConfig.getProperty("nomail").equals("true")) {
				// there is no activation mail
				System.out.println("no activation per mail");
				return activateAccount(user);
			} else {
				System.out.println("activation per mail");
				ActivityLog.add(getSession().getId(), getRemoteAddr(), "null", "subscribe-mail-sent", login, mail);
				return new LoginResult(true, mail);
			}
		}
		//return createUser(user, password);
	}

	/**
	 * Send a mail to a user with a link to activate his account
	 * 
	 * @param user
	 * @param password
	 * @return
	 */
	private boolean sendConfirmationEmail(TxmUser user, String password)
	{
		String code = UUID.randomUUID().toString();
		//		String host = this.getThreadLocalRequest().getRequestURL().toString();
		//		if (host == null  || host.length() == 0) {
		//			Log.severe("[sendConfirmationEmail] no host found !");
		//			return false;
		//		} else {
		//			int idx = host.indexOf("?");
		//			if (idx > 0)
		//				host = host.substring(0, idx); // remove http parameters
		//			host = host.substring(0, host.length()-22);//remove service name
		//			System.out.println("host: "+host);
		//		}
		String host = TxmConfig.getProperty("portal_address");

		String portail = TxmConfig.getProperty("portal_name");
		String adminmail = TxmConfig.getProperty("mail.contact");
		String link = host +"?confirmation="+code;

		String content_en = StringFormat.format(MailContent.getConfirmation("en"), user.getLogin(), user.getMail(), link);
		String content_fr = StringFormat.format(MailContent.getConfirmation("fr"), user.getLogin(), user.getMail(), link, portail, adminmail);
		boolean doValidation = TxmConfig.getProperty("send_askaccess_with_inscription").equals("true");

		if (Emailer.sendMail(user.getMail(), MailContent.getConfirmationSubjects("fr"), content_fr)) {
			sentActivationCodes.put(code, user);
			Date date = Calendar.getInstance().getTime();
			date.setTime(date.getTime()+100000000);
			System.out.println("toBeActivated: "+user+" date: "+date);
			toBeActivated.put(user, date);
			tempSavedPassword.put(user, password);
			return true;
		} else
			return false;
	}

	/**
	 * Send a mail to the user to activate his account and confirm the access demand
	 * @param user
	 * @param password
	 * @return
	 */
	private boolean sendConfirmationAndAccessEmail(TxmUser user, String password)
	{
		String code = UUID.randomUUID().toString();
		//		String host = this.getThreadLocalRequest().getRequestURL().toString();
		//		if (host == null  || host.length() == 0) {
		//			Log.severe("[sendConfirmationEmail] no host found !");
		//			return false;
		//		} else {
		//			int idx = host.indexOf("?");
		//			if (idx > 0)
		//				host = host.substring(0, idx); // remove http parameters
		//			host = host.substring(0, host.length()-22);//remove service name
		//			System.out.println("host: "+host);
		//		}
		String host = TxmConfig.getProperty("portal_address");

		String portail = TxmConfig.getProperty("portal_name", "no_portal_name_set");
		String adminmail = TxmConfig.getProperty("mail.contact", "no_admin_contact_set");
		String link = host +"?confirmation="+code;

		//		String content_en = "Your account has been created with the login '"+user.getLogin()+"' et the email '"+user.getMail()+"' \n" +
		//		"To finish the inscription please follow this link: "+link;

		//		String content1_fr = "Une demande d'accès à la Base de Français Médiéval a été formulée sur le portail "+
		//		portail+" \n("+host+") avec l'adresse mail "+user.getMail()+".\n\n" +
		//		
		//		"Les informations associées à cette demande sont les suivantes :\n\n";
		//
		//		content1_fr += (" - Nom : "+user.getLastName())+"\n";
		//		content1_fr += (" - Prénom : "+user.getFirstName())+"\n";
		//		content1_fr += (" - Institution : "+user.getInstitution())+"\n";
		//		content1_fr += (" - Tél : "+user.getPhone())+"\n";
		//		content1_fr += (" - Statut : "+user.getStatus())+"\n";
		//		content1_fr += (" - Identifiant : "+user.getLogin()+"\n");
		//		content1_fr += (" - Mail : "+user.getMail())+"\n\n";
		//		
		//		content1_fr +=  "Si vous souhaitez modifier certaines de ces informations, merci de contacter " +
		//		"l'administrateur du portail \n("+adminmail+") pour lui indiquer les corrections à effectuer.\n" +
		//		"Par la suite, vous pourrez également corriger certaines informations dans votre espace " +
		//		"\n'informations personnelles' du portail un fois connecté.\n\n";
		//
		//		String content2_fr = "Cette demande d'accès est associée à la création du compte utilisateur '"+user.getLogin()+"'.\n" +
		//		"Pour pouvoir valider la création de ce compte, merci de cliquer sur le lien suivant "+link+
		//		" (ce lien est valable 24h) pour associer définitivement cette adresse mail avec ce compte.\n" +
		//		"Une fois cette association établie, vous pourrez vous connecter et l'administrateur pourra alors valider le compte.\n" +
		//		"L'accès à la base vous sera alors signifié par un mail de l'administrateur.\n\n" +
		//		
		//		"Si vous n'êtes pas à l'origine de cette demande d'accès à la BFM, merci d'avertir " +
		//		"l'administrateur du portail \n"+portail+" ("+adminmail+") pour lui indiquer un abus de l'usage de votre mail " +
		//		"de sorte à ce qu'il puisse annuler cette inscription. \nDans ce cas, merci de ne pas " +
		//		"cliquer sur le lien de confirmation de la demande de création de compte.\n\n" +
		//		
		//		"Cordialement, \n" +
		//		"l'équipe "+portail+"\n";
		String content_fr = StringFormat.format(MailContent.getAccessAndConfirmation("fr"), 
				host, user.getMail(), user.getLastName(), user.getFirstName(), user.getInstitution(), 
				user.getPhone(), user.getStatus(), user.getLogin(), adminmail, link, portail );
		String content_en = StringFormat.format(MailContent.getAccessAndConfirmation("fr"), 
				host, user.getMail(), user.getLastName(), user.getFirstName(), user.getInstitution(), 
				user.getPhone(), user.getStatus(), user.getLogin(), adminmail, link, portail );
		// subject : "équipe "+portail+ " - Confirmation de la demande d'inscription au portail "+portail
		if (Emailer.sendMail(user.getMail(), MailContent.getAccessAndConfirmationSubjects("fr"), content_fr))
		{
			sentActivationCodes.put(code, user);
			Date date = Calendar.getInstance().getTime();
			date.setTime(date.getTime()+100000000);
			System.out.println("toBeActivated: "+user+" date: "+date);
			toBeActivated.put(user, date);
			tempSavedPassword.put(user, password);
			return Emailer.sendMail(adminmail, "Copy: "+MailContent.getAccessAndConfirmationSubjects("fr"), "This is a copy of the mail sent to the user.\n\n"+content_fr);
		} else {
			return false;
		}
	}

	/**
	 * set user's password, default profile and register it in the UserManager
	 * @param user
	 * @param password
	 * @return
	 */
	public LoginResult createUser(TxmUser user, String password)
	{
		user.setPassword(password);
		if (!UserManager.create(user))
			return new LoginResult(false, "Failed to create a new user");
		Log.info("[subscribe] User : " + user.getLogin() + " -> Success");

		Profile profile = Profiles.getNewbieProfile();
		if (profile == null) {
			new LoginResult(false, "Could not find the 'Newbie' profile.");
		}
		user.setProfile(profile.getName());
		LoginResult result = new LoginResult(true, profile.getHomePage());

		return result;
	}

	public static LoginModule getLoginModule() {
		return loginModule;
	}

	@Override
	/**
	 * Service: Update the session user's data
	 * @return true if succes
	 */
	public String changeUserData(UserData data) {
		Log.info("[AuthenticationServiceImpl.changeUserData] start. login="+data.login);
		TxmUser user = SessionManager.getUser(getSession().getId());

		if (user == null || user.isGuest()) {
			Log.severe("Session "+getSession()+" has expired");
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "changeUserData", data.login);
			return "expired"; 
		}

		ActivityLog.add(getSession().getId(), getRemoteAddr(), user.getLogin(), "changeUserData", data.login);
		if (data.newpassword!= null && data.newpassword.length() > 0) {
			if (UserData.checkValidPassword(data.newpassword) && PasswordCheck.CheckPasswordStrength(data.newpassword))
			{
				if (!user.testPassword(data.password)) {
					Log.info("[changeUserData] User : " + data.login + " -> wrong password");
					return "wrongpassword";
				} else {
					Log.info("[changeUserData] User : " + data.login + " -> update password");
					user.setPassword(data.newpassword);
					if (!user.save()) {
						Log.severe("Could not save user");
						return "Error: User not saved";
					}
				}
			} else {
				Log.info("[changeUserData] User : " + data.login + " -> wrong new password format");
				return "password";
			}
		}

		if (UserData.checkMail(data.mail) ) {
			// try sending mail
			if (!data.mail.equals(user.getMail())) {
				if ("true".equals(TxmConfig.getProperty("nomail"))) { // no need to send mail
					user.setMail(data.mail);
				} else {
					if (!sendUpdateMail(user, data.mail)) { // try to send mail
						Log.info("[changeUserData] User : " + data.login + " -> can't send email");
						return "mail";
					}
				}
			}
		} else {
			Log.info("[changeUserData] User : " + data.login + " -> wrong mail format");
			System.out.println("UserData.checkMail(data.mail)= "+UserData.checkMail(data.mail));
			System.out.println("!(data.mail.equals(user.getMail()))="+!(data.mail.equals(user.getMail())));
			System.out.println("data.mail="+data.mail);
			System.out.println("user.getMail()="+user.getMail());
			return "mailformat";
		}

		user.setNames(data.firstname, data.lastname);
		user.setInstitution(data.institution);
		user.setPhone(data.phone);
		user.setStatus(data.status);
		if (!user.save()) {
			return "user not saved";
		}

		return "";
	}

	@Override
	/**
	 * Service:
	 * @return the session user's data
	 */
	public UserData getUserData() {
		TxmUser user = SessionManager.getUser(getSession().getId());

		if (user == null) {
			Log.severe("Session "+getSession()+" has expired");
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "getUserData");
			return null; 
		}
		ActivityLog.add(getSession().getId(), getRemoteAddr(), user.getLogin(), "getUserData");
		return new UserData(user.getLogin(), "", user.getProfile().getNameWithoutExtension(), user.getMail(),user.getFirstName(), user.getLastName(), "", user.getStatus(), user.getInstitution(), user.getPhone());
	}

	public LoginResult askAccessAndSubscribe(UserData data, Boolean accessb)
	{
		if (accessb == null || !accessb) {
			Log.severe("askAccess: certify button is not checked");
			return new LoginResult(false, "Certify button is not checked.");
		}

		Log.info("[AuthenticationServiceImpl.subscribe] start. login="+data.login);

		// test if inscription is enable
		if ("true".equals(TxmConfig.getProperty("noinscription"))) {
			return new LoginResult(false, "Inscription is not enable on this portal.");
		}
		
		data.testSubscriptionInfos(); // test data and normalize it

		//String firstname = data.firstname;
		//String lastname = data.lastname;
		String login = data.login;
		String password = data.password;
		String mail = data.mail;
		String captcha = data.captcha;
		//String status = data.status;
		//String institution = data.institution;
		//String tel = data.phone;

		ActivityLog.add(getSession().getId(), getRemoteAddr(), "null", "askAccessAndSubscribe" , login, mail);

		// test captcha
		HttpServletRequest request = getThreadLocalRequest();
		HttpSession session = request.getSession();
		Captcha captcha_ = (Captcha) session.getAttribute(Captcha.NAME);
		if (captcha_.isCorrect(captcha)) {
			Log.info("[subscribe] User : captcha validation ok");
		} else {
			Log.info("[subscribe] User : Wrong captcha");
			return new LoginResult(false, "Wrong captcha");
		}	

		// test mail
		if (!UserData.checkMail(mail)) {
			Log.severe("askAccess: mail not valid: "+mail);
			return new LoginResult(false, "The given mail is not valid");
		}

		// test password
		if (!PasswordCheck.CheckPasswordStrength(password)) {
			Log.info("[subscribe] User : " + login + " -> password not enough strong");
			return new LoginResult(false, "The given password not enough strong");
		}

		//test login
		if (!UserData.checkValidLogin(login)) {
			Log.info("[subscribe] User : " + login + " -> login not valid");
			return new LoginResult(false, "Login not valid");
		}

		//test login availability
		if (UserManager.getUser(login) != null)
			return new LoginResult(false, "login_already_used");

		for (TxmUser testuser : toBeActivated.keySet()) {
			if (testuser.getLogin().equals(login))
				return new LoginResult(false, "Sorry, this login is already used, a confirmation email has already been sent");
		}

		boolean allowMultipleAccount = TxmConfig.getProperty("allow_multiple_account_per_mail").equals("true");
		if (!allowMultipleAccount)
			for (TxmUser testuser : UserManager.getUsers())
				if (testuser.getMail().equals(mail)) {
					Log.severe("[subscribe] User : fail: mail already used" + mail);
					return new LoginResult(false, "MailUsed");
				}	

		//create the user
		TxmUser user = new TxmUser(data);

		//try sending the email
		if (!sendConfirmationAndAccessEmail(user, password)) {
			Log.severe("[subscribe] User : failed to send email to" + mail);
			ActivityLog.severe(getSession().getId(), getRemoteAddr(), "null", "askAccessAndSubscribe", login, mail, "mail error");
			return new LoginResult(false, "Failed to send inscription confirmation to "+mail);
		} else {
			if (TxmConfig.getProperty("nomail").equals("true")) {
				// there is no activation mail, so we activate the new account
				System.out.println("no activation per mail");
				return activateAccount(user);
			} else {
				System.out.println("activation per mail");
				return new LoginResult(true, mail);
			}
		}		
	}

	@Override
	/**
	 * Service: send mail to admin to register a user to a base
	 * @return true if success
	 */
	public Boolean askAccess(String login, String name, String firstname, String status,
			String institution, String mail, String tel, Boolean accessb) {
		Log.info("[AuthenticationServiceImpl.askAccess] start. name="+name);

		if (!accessb) {
			Log.severe("askAccess: certify button is not checked");
			return false;
		}

		TxmUser user = null;
		if (login == null || login.trim().length() == 0) {
			user = SessionManager.getUser(getSession().getId());
			if (!user.isLogged()) {
				Log.warning("[askAccess] logged mode: the user "+user.getLogin()+" is not logged in");
				return false;
			}
		} else {
			Log.warning("[askAccess] login mode, login="+login);
			user = UserManager.getUser(login);
			if (user == null) { // the accound is not yet activated
				Set<TxmUser> users = toBeActivated.keySet();
				for (TxmUser tobeuser : users) {
					if (tobeuser.getLogin().equals(login)) {
						user = tobeuser;
						break;
					}
				}
			}
			if (user == null) {
				Log.warning("[askAccess] login mode: the user "+login+" cannot be found in internal data");
				return false;
			}
		}
		if (user == null || user.isGuest()) {
			Log.severe("Session "+getSession()+" has expired");
			return false; 
		}

		ActivityLog.add(getSession().getId(), user.getLogin(), "askAccess", login, firstname, name, mail);

		if (name == null || firstname == null || status == null || 
				institution == null || mail == null) {
			Log.severe("askAccess: madatory fields are no set (null)");
			return false;
		}

		name = name.trim();
		firstname = firstname.trim();
		status = status.trim();
		institution = institution.trim();
		mail = mail.trim();
		if (tel != null)
			tel = tel.trim();
		else
			tel = "";

		if (name.equals("") || firstname.equals("") || status.equals("") || institution.equals("") || mail.equals("")) {
			Log.severe("askAccess: madatory fields are no set (\"\")");
			return false;
		}

		if (!UserData.checkMail(mail)) {
			Log.severe("askAccess: mail not valid: "+mail);
			return false;
		}

		System.out.println(" User "+user.getFormatedName()+" ask for base access");
		System.out.println(" Login: "+user.getLogin());
		System.out.println(" Name: "+name);
		System.out.println(" First name: "+firstname);
		System.out.println(" Statut: "+status);
		System.out.println(" Institution: "+institution);
		System.out.println(" Mail: "+mail);
		System.out.println(" Tel: "+tel);

		// update user infos
		user.setStatus(status);
		user.setInstitution(institution);
		user.setPhone(tel);
		user.setNames(firstname, name);
		user.setMail(mail);
		if (!user.save()) {
			Log.severe("Could not save user");
			return false;
		}

		String portail = TxmConfig.getProperty("portal_name", "portal_name_not_set");
		String host = this.getThreadLocalRequest().getLocalAddr();
		String subject = portail+" - Formulaire d'accès à la base";
		String adminmail = TxmConfig.getProperty("mail.contact", "no_admin_contact_set");
		String content = "Une demande d'accès à la base de Français Médiéval a été formulée sur le portail "+
				portail+" ("+host+") avec l'adresse mail "+mail+".\n" +
				"Les informations associés à cette demande sont les suivantes :\n";

		content += (" - Nom: "+name)+"\n";
		content += (" - Prénom: "+firstname)+"\n";
		content += (" - Institution: "+institution)+"\n";
		content += (" - Tel: "+tel)+"\n";
		content += (" - Statut: "+status)+"\n";
		content += (" - Identifiant "+user.getLogin()+"\n");
		content += (" - Mail: "+mail)+"\n";

		content +=  "Si vous souhaitez modifier certaines de ces informations, merci de contacter " +
				"l'administrateur du portail ("+adminmail+") pour lui indiquer les corrections à effectuer.\n" +
				"Vous pourrez également corriger certaines informations dans votre espace " +
				"'informations personnelles' du portail une fois connecté.";

		content +=	"L'administrateur va traiter votre demande et vous préviendra par mail quand il aura validé votre demande.\n" ;

		if (Emailer.sendMail(mail, subject,  content) && Emailer.sendMail(adminmail, "Copy: "+subject,  content))
			return true;
		else
			return false;
	}

	@Override
	/**
	 * @return true if the user has been autologed out
	 */
	public boolean autologout(String login) {
		TxmUser user = SessionManager.getUser(getSession().getId());

		//System.out.println("auto logout "+login+", user="+user);
		if (user == null) {
			return true;
		} else {
			if(user.getLogin().equals("guest"))
				return false; // no need to log out
			SessionManager.deleteSession(getSession().getId());
			return true;
		}
	}

	@Override
	public List<UserData> listUserInfos() {
		Log.info("[listUserInfos] start");
		TxmUser u = SessionManager.getUser(getSession().getId());
		if (!u.isAdmin()) {
			ActivityLog.add(getSession().getId(), getRemoteAddr(), u.getLogin(), "listUserInfos");
			Log.severe("[listUserInfos] only admin can get user infos");
			return new ArrayList<UserData>();
		}
		ActivityLog.add(getSession().getId(), getRemoteAddr(), "admin", "listUserInfos");
		ArrayList<UserData> data = new ArrayList<UserData>();
		for (String login : UserManager.getAllUsers()) {
			TxmUser user = UserManager.getUser(login);
			if (user != null) {
				Profile p = user.getProfile();
				if (p != null) {
					UserData userdata = new UserData(user.getLogin(), "", p.getNameWithoutExtension(), user.getMail(),user.getFirstName(), user.getLastName(), "", user.getStatus(), user.getInstitution(), user.getPhone());
					userdata.lastconnection = user.getLastConnexion();
					data.add(userdata);
				} else {
					Log.severe("UserManager returned a User with no profile. login="+login);
				}
			} else {
				Log.severe("UserManager returned null User for login="+login);
			}
		}
		for (TxmUser user : toBeActivated.keySet()) {
			UserData userdata = new UserData(user.getLogin()+"*", "", user.getProfile().getNameWithoutExtension(), user.getMail(),user.getFirstName(), user.getLastName(), "", user.getStatus(), user.getInstitution(), user.getPhone());
			userdata.lastconnection = toBeActivated.get(user);
			data.add(userdata);
		}
		Log.info("[listUserInfos] end");
		return data;
	}

	/**
	 * Store the user in a temporary hashmap
	 * @param user
	 * @return
	 */
	protected LoginResult activateAccount(TxmUser user) {
		Date date = toBeActivated.get(user);
		if (date == null) {
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "activateAccount", user.getLogin(), user.getMail(), date);
			return new LoginResult(false, "Internal error: Failed to get inscription date of user "+user);
		}
		Date today = Calendar.getInstance().getTime();
		if (date.after(today)) {
			ActivityLog.add(getSession().getId(), getRemoteAddr(), "null", "activateAccount", user.getLogin(), user.getMail(), date);

			System.out.println("Activate "+user);
			String password = tempSavedPassword.get(user);
			LoginResult rez = createUser(user, password);
			if (rez.isSuccess()) {
				toBeActivated.remove(user);
				tempSavedPassword.remove(user);
				System.out.println("User: "+user+" profile: "+user.getProfiles());
				ActivityLog.add(getSession().getId(), getRemoteAddr(), "null", "activateAccount-complete", user.getLogin(), user.getMail());
				return login(user.getLogin(), password);
			} else {
				ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "activateAccount", user.getLogin(), user.getMail(), date);
				System.out.println("accound finalization failed");
				return rez;
			}
		} else {
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "activateAccount", user.getLogin(), user.getMail(), date, "too late");
			System.out.println("Activation date of "+user+" has expired");
			return new LoginResult(false, "Activation date of "+user+" has expired");
		}
	}

	/**
	 * Check the code, if the date is not expired and the code is valid, the user is created and logged in
	 * @param code
	 * @return
	 */
	@Override
	public LoginResult activateAccount(String code) {
		if (sentActivationCodes.containsKey(code)) {
			TxmUser user = sentActivationCodes.get(code);
			LoginResult rez = activateAccount(user);
			if (rez.isSuccess()) {
				sentActivationCodes.remove(code);
			}
			return rez;
		} else {
			System.out.println("Wrong activation code: "+code+" from session "+getSession().getId());
			return new LoginResult(false,"Wrong activation code: "+code);
		}
	}

	@Override
	public Boolean forgotPassword(String mail) {
		boolean ok = false;
		TxmUser u = SessionManager.getUser(getSession().getId());
		if (u != null && !u.isGuest()) {
			ActivityLog.severe(getSession().getId(), getRemoteAddr(), u.getLogin(), "forgotPassword", mail);
			Log.severe("[forgotPassword] only guest can get recorver a password");
			return false;
		}
		ActivityLog.add(getSession().getId(), getRemoteAddr(), "null", "forgotPassword", mail);
		for (TxmUser user : UserManager.getUsers()) {
			if (user.getMail().equals(mail)) {
				sendPasswordRecorvery(user);
				ok = true;
			}
		}
		return ok;
	}

	private String buildCodedLink()
	{
		String code = UUID.randomUUID().toString();
		String host = this.getThreadLocalRequest().getLocalAddr();
		int port = this.getThreadLocalRequest().getLocalPort();
		return "http://"+host + ":"+port+"/TXMWEB.html?confirmation="+code;
	}

	private boolean sendUpdateMail(TxmUser user, String mail)
	{
		Log.info("[sendUpdateMail] Start: "+user);
		// test if mail is activated?
		String code = UUID.randomUUID().toString();
		//		String host = this.getThreadLocalRequest().getRequestURL().toString();
		//		if (host == null  || host.length() == 0) {
		//			Log.severe("[sendUpdateMail] no host found !");
		//			return false;
		//		} else {
		//			host = host.substring(0, host.length()-22);
		//			System.out.println("host: "+host);
		//		}
		String host = TxmConfig.getProperty("portal_address");

		String portal = TxmConfig.getProperty("portal_name");
		String adminmail = TxmConfig.getProperty("mail.contact");
		String link = host+"?updateMail="+code;

		String content_fr = StringFormat.format(MailContent.getUpdateMail("fr"), user.getLogin(), portal, link, adminmail);
		Log.info("[sendUpdateMail] Try sending password updateMail mail to: "+mail);
		if (Emailer.sendMail(mail, MailContent.getUpdateMailSubjects("fr"), content_fr)) {
			sentUpdateMailCodes.put(code, user);
			Date date = Calendar.getInstance().getTime();
			date.setTime(date.getTime()+100000000);
			toBeMailUpdated.put(user, date);
			tempSavedMail.put(user, mail);
			Log.info("[sendUpdateMail] store code: "+code);
			return true;
		} else {
			Log.warning("[sendUpdateMail] mail failed");
			return false;
		}
	}

	private boolean sendPasswordRecorvery(TxmUser user)
	{
		Log.info("[sendPasswordRecorvery] Start: "+user);
		// test if mail is activated?
		String code = UUID.randomUUID().toString();
		//		String host = this.getThreadLocalRequest().getRequestURL().toString();
		//		if (host == null  || host.length() == 0) {
		//			Log.severe("[sendConfirmationEmail] no host found !");
		//			return false;
		//		} else {
		//			host = host.substring(0, host.length()-22);
		//			System.out.println("host: "+host);
		//		}
		String host = TxmConfig.getProperty("portal_address");

		String portal = TxmConfig.getProperty("portal_name");
		String adminmail = TxmConfig.getProperty("mail.contact");
		String link = host+"?recovery="+code;

		String content_fr = StringFormat.format(MailContent.getLostPassword("fr"), user.getLogin(), portal, link, adminmail);
		String content_en = StringFormat.format(MailContent.getLostPassword("fr"), user.getLogin(), portal, link, adminmail);
		Log.info("[sendPasswordRecorvery] Try sending password recovery mail to: "+user.getMail());
		//subject : TxmConfig.getProperty("portal_name")+ " - Récupération de mot de passe"
		if (Emailer.sendMail(user.getMail(), MailContent.getLostPasswordSubjects("fr"), content_fr)) {
			sentRecorveryCodes.put(code, user);
			Date date = Calendar.getInstance().getTime();
			date.setTime(date.getTime()+100000000);
			toBeRecorvered.put(user, date);
			Log.info("[sendPasswordRecorvery] store code: "+code);
			return true;
		} else {
			Log.warning("[sendPasswordRecorvery] mail failed");
			return false;
		}
	}

	@Override
	public LoginResult recorverPassword(String code, String newpass, String passconfirm) {
		Log.info("[recorverPassword] Start");
		TxmUser u = SessionManager.getUser(getSession().getId());
		if (u != null && !u.isGuest()) {
			ActivityLog.severe(getSession().getId(), getRemoteAddr(), u.getLogin(), "recorverPassword");
			Log.info("[recorverPassword] only guest can recorver password");
			return new LoginResult(false, "only guest can recorver password");
		}

		if (sentRecorveryCodes.containsKey(code)) {
			TxmUser user = sentRecorveryCodes.get(code);
			ActivityLog.add(getSession().getId(), getRemoteAddr(), "null", "recoverPassword", code, user.getLogin());
			Date date = toBeRecorvered.get(user);
			Date today = Calendar.getInstance().getTime();
			if (date.after(today)) {
				System.out.println("Restore "+user);
				if (UserData.checkValidPassword(newpass) 
						&& PasswordCheck.CheckPasswordStrength(newpass)) {
					Log.info("[recorverPassword] User : " + user.getLogin() + " -> update password");
					if (newpass.equals(passconfirm)) {
						if (user.setPassword(newpass)) {
							sentRecorveryCodes.remove(code);
							toBeRecorvered.remove(user);

							return login(user.getLogin(), newpass);
						} else {
							Log.info("[recorverPassword] Failed to update account password");
							return new LoginResult(false, "Failed to update account password");
						}
					} else {
						Log.info("[recorverPassword] password and confirm password are differents");
						return new LoginResult(false, "Password and confirm password are differents");
					}
				} else {
					Log.info("[recorverPassword] password not enough strong");
					return new LoginResult(false, "Password is not enough strong");
				}
			} else {
				Log.info("[recorverPassword] Date < today = "+date+" < " + today);
				return new LoginResult(false, "Recorvery delay has expired");
			}

		}
		return new LoginResult(false, "No such recovery code");
	}

	@Override
	public String updateUserMailEND(String code) {
		TxmUser user = sentUpdateMailCodes.get(code); // get user from code
		sentUpdateMailCodes.remove(code);
		if (user == null) {
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "updateUserMail", code);
			return "code";
		}

		Date date = toBeMailUpdated.get(user); // get update date
		Date today = Calendar.getInstance().getTime(); // get today time
		String mail = tempSavedMail.get(user); // get saved mail
		ActivityLog.add(getSession().getId(), getRemoteAddr(), "null", "updateUserMail", code, user.getLogin(), mail);
		toBeMailUpdated.remove(user);
		tempSavedMail.remove(user);

		if (date.after(today)) {
			System.out.println("[updateUserMailEND] Update mail "+user+" with "+mail);
			user.setMail(mail); // update the account mail
		} else {
			ActivityLog.warning(getSession().getId(), getRemoteAddr(), "null", "updateUserMail", code);
			return "delay";
		}
		return "";
	}

	@Override
	public String checkAvailability(String login) {
		Log.info("[checkAvailability] start: "+login);
		if (!UserData.checkValidLogin(login)) {
			return "short";
		} else if (UserManager.getUser(login) == null) {
			return "";
		} else {
			return "fail";
		}
	}
}
