package org.txm.web.server.statics;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.txm.utils.logger.Log;
import org.txm.web.server.services.UIServiceImpl;

/**
 * It's the loader of the configuration file of the application. The file is located 
 * in ~/TXMWEB/<app>/txmweb.conf
 * 
 * @author vchabani
 * @version 0.2.0
 */
public class TxmConfig {

	private static HashMap<String, String> properties = new HashMap<String, String>();
	private static boolean isLoaded = false;
	
	public static String appname;
	public static String warpath;
	public static File configFile,configDir;

	public static String getDefaultToolboxPropertiesFile()
	{
		appname = new File(warpath).getName();
		String userhome = System.getProperty("user.home");
		File configDir = new File(userhome, "TXMWEB/"+appname);
		File configFile = new File(configDir, "textometrie.properties");
		return configFile.getAbsolutePath();
	}

	/**
	 * Reset properties content, mails content, default locale and email configuration
	 * @return
	 */
	public static boolean reset()
	{
		stop();
		return load();
	}
	
	/**
	 * Read the configuration file and store it into a map.
	 * @return true if success
	 */
	private static boolean load() {

		File configFile =  getConfigFile();
		System.out.println("Config file path "+configFile.getAbsolutePath());

		if (!configFile.exists()) {
			Log.severe("[TxmConfig.start] " + configFile.getAbsolutePath() + " not found.");
			return false;
		}
		try {
			String line = null;
			System.getProperty("user.home");
			FileReader reader = new FileReader(configFile);
			BufferedReader br = new BufferedReader(reader);
			while ((line = br.readLine()) != null) {
				if (!line.equals("")) {
					if (line.charAt(0) != '#' && !line.equals("")) {
						String[] entry = line.split("=");
						if(entry.length == 2)
							properties.put(entry[0].trim(), entry[1].trim());
						else if (entry.length == 1)
							properties.put(entry[0].trim(), "");
						else
							Log.warning("[TxmConfig.load] entry not well formed: "+line);
					}
				}
			}
			reader.close();
			System.out.println("TXMWEB.CONF properties: "+properties);
			isLoaded = true;
		} catch (FileNotFoundException e) {
			Log.severe("[TxmConfig.start] " + configFile.getAbsolutePath() + " not found.");
			return false;
		} catch (IOException e) {
			Log.severe("[TxmConfig.start] " + configFile.getAbsolutePath() + " could not be read.");
			return false;
		}	

		if (properties.get("userBase") == null)
			properties.put("userBase", new File(configDir, "data/users").getAbsolutePath());
		if (properties.get("profileBase") == null)
			properties.put("profileBase", new File(configDir, "data/profiles").getAbsolutePath());
		if (properties.get("htmlBase") == null)
			properties.put("htmlBase", new File(configDir, "data/html").getAbsolutePath());
		if (properties.get("mailBase") == null)
			properties.put("mailBase", new File(configDir, "data/mails").getAbsolutePath());
		if (properties.get("portal_address") == null) {
			Log.severe("portal_address is not set. Exports won't work");
			return false;
		} else {
			if (!(properties.get("portal_address").startsWith("http://") ||
				properties.get("portal_address").startsWith("https://"))) {
				properties.put("portal_address", "http://"+properties.get("portal_address"));
				Log.severe("portal_address is not well set. It must starts with http:// or https://");
				return false;
			}
		}
		Log.info("[TxmConfig.start] " + configFile.getAbsolutePath() + " was correctly loaded.");
		Log.info("[TxmConfig.start] properties : "+properties);
		return true;
	}

	public static String getProperty(String propertyName) {
		return getProperty(propertyName, null);
	}
	
	/**
	 * @return The value of the given property name.
	 */
	public static String getProperty(String propertyName, String default_return) {
		if (!isLoaded) {
			load();
		}
		
		String result = properties.get(propertyName);
		if (result == null) {
			Log.severe("[TxmConfig.getProperty] " + propertyName + " was not found in txmweb.conf, please fix it.");
			return default_return;
		}
		Log.fine("[TxmConfig.getProperty] Asking for property: " + propertyName + " -> " + result);
		return result;
	}

	public static File getConfigFile() {
		if (configFile == null) {
			appname = new File(warpath).getName();
			String userhome = System.getProperty("user.home");
			configDir = new File(userhome, "TXMWEB/"+appname);
			configFile = new File(configDir, "txmweb.conf");
		}
		return configFile;
	}

	/**
	 * @return The value of the given property name.
	 */
	public static boolean setProperty(String name, String value) {
		Log.info("[TxmConfig]: set property '"+name+"' to '"+value+"'");
		properties.put(name, value);
//		File configFile = getConfigFile();
//		System.out.println("Config file path "+configFile.getAbsolutePath());
//
//		if (!configFile.exists())
//		{
//			Log.severe("[TxmConfig.start] " + configFile.getAbsolutePath() + " not found.");
//			return false;
//		}
//		try {
//			String line = null;
//			FileReader reader = new FileReader(configFile);
//			BufferedReader br = new BufferedReader(reader);
//			while ((line = br.readLine()) != null) {
//				if (!line.equals("")) {
//					if (line.charAt(0) != '#' && !line.equals("")) {
//						String[] entry = line.split("=");
//						properties.put(entry[0].trim(), entry[1].trim());
//					}
//				}
//			}
//			reader.close();
//
//		} catch (FileNotFoundException e) {
//			Log.severe("[TxmConfig.start] " + configFile.getAbsolutePath() + " not found.");
//			return false;
//		} catch (IOException e) {
//			Log.severe("[TxmConfig.start] " + configFile.getAbsolutePath() + " could not be read.");
//			return false;
//		}	
		return true;
	}

	public static void stop() {
		MailContent.isLoaded = false; // to reset mail contents
		UIServiceImpl.DEFAULT_LOCALE = null; // reset  default locale
		properties.clear();
		isLoaded = false;
	}
}
