package org.txm.web.server.statics;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;

public class ActivityLog {
	private static boolean on = true;
	private static boolean init = false;
	private static Logger txm;
	private static FileHandler fh;
	public static SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd¤HH:mm:ss");

	private static File logdir;
	private static File logfile;

	public static void init() {
		if (init) return;
		txm = Logger.getLogger("TXMWEBActivity"); //$NON-NLS-1$
		for (int i = 0 ; i < txm.getHandlers().length ; i++) {
			txm.removeHandler(txm.getHandlers()[i]);
			i--;
		}
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
		try {
			logdir = new File(System.getProperty("user.home")+"/TXMWEB/"+TxmConfig.appname+"/activity");
			logdir.mkdirs();
			logfile = new File(logdir, "TXMWEB-"+f.format(new Date())+".activity");
			System.out.println("LOGFILE: "+logfile.getAbsolutePath());
			fh = new FileHandler(logfile.getAbsolutePath(), false);
			fh.setEncoding("UTF-8");
			fh.setFormatter(new Formatter() {
				@Override
				public String format(LogRecord record) {
					return record.getMessage()+"\n";
				}
			});
			fh.setEncoding("UTF-8"); //$NON-NLS-1$
			txm.addHandler(fh);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		init = true;
		on = "true".equals(TxmConfig.getProperty("log_activity"));
	}

	public static void add(Object session, String ip, String login, String command, Object... var)
	{
		init();
		if (on) txm.info(f.format(new Date())+"¤"+session+"¤"+ip+"¤"+login+"¤0¤"+command+"¤"+StringUtils.join(var, "¤"));
	}

	public static void warning(Object session, String ip, String login, String command, Object... var)
	{
		init();
		if (on) txm.warning(f.format(new Date())+"¤"+session+"¤"+ip+"¤"+login+"¤1¤"+command+"¤"+StringUtils.join(var, "¤"));
	}

	public static void severe(Object session, String ip, String login, String command, Object... var)
	{
		init();
		if (on) txm.severe(f.format(new Date())+"¤"+session+"¤"+ip+"¤"+login+"¤2¤"+command+"¤"+StringUtils.join(var, "¤"));
	}

	/**
	 * @param size
	 * @return
	 */
	public static ArrayList<ArrayList<String>> getLastLogs(int size) {

		ArrayList<ArrayList<String>> logs = new ArrayList<ArrayList<String>>();

		// most recent file
		File file = lastActivityFileModified(logdir);
		if(file == null)
			return logs;
		try {
			RandomAccessFile raf = new RandomAccessFile(file, "r");
			long position = file.length();
			int code;

			while (--position > 0) {
				raf.seek(position - 1);
				code = raf.readByte();
				// Teste si on a un saut à la ligne.
				if ((code == 13) || (code == 10)) {
					String ligne = raf.readLine();
					ligne = ligne.replace("Â", "");
					String split[] = ligne.split("¤");
					if(split.length >= 7)
					{
						ArrayList<String> line = new ArrayList<String>();
						//System.out.println(ligne);
						line.add(split[0]);
						line.add(split[1]);
						line.add(split[2]);
						line.add(split[3]);
						line.add(split[4]);
						line.add(split[5]);
						line.add(split[6]);

						String params = "";
						for(int i = 7 ; i < split.length ; i++)
							params+=split[i]+"\t";
						line.add(params);
						logs.add(line);
						if(logs.size() >= size)
							break;
					}

				}
			}
			raf.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return logs;
	}

	public static File lastActivityFileModified(File fl) {
		File[] files = fl.listFiles(new FileFilter() {                  
			public boolean accept(File file) {
				return file.isFile() && file.getName().endsWith(".activity");
			}
		});
		long lastMod = Long.MIN_VALUE;
		File choise = null;
		for (File file : files) {
			if (file.lastModified() > lastMod) {
				choise = file;
				lastMod = file.lastModified();
			}
		}
		return choise;
	}

}
