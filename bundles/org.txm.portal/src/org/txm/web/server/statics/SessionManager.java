package org.txm.web.server.statics;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.txm.utils.logger.Log;
import org.txm.web.server.TxmUser;

/**
 * It's the container of the sessions. Each session ID is linked with a TxmUser.
 * 
 * @see TxmSessionListener
 * @see TxmUser
 * @author vchabani
 * @version 0.2.0
 */
public class SessionManager {

	// list of users per session
	private static Map<String, TxmUser> userMap = Collections.synchronizedMap(new HashMap<String,TxmUser>());
	// last activity date per session
	private static Map<String, Date> sessionActivity = Collections.synchronizedMap(new HashMap<String,Date>());
	public static int AUTODECONN_TIMEOUT = 6000000; // ms, 100min 
	
	/**
	 * If the session is already stored. The user linked to the session will update his corpora view.
	 * If the session doesn't exist, an Anonymous user will be created
	 * @param session The HttpSession
	 */
	public static void startSession(HttpSession session) {
		if (userMap.containsKey(session.getId())) {
			userMap.get(session.getId()).updateCorpora();
		}
	}
	
	public static void shutdown() {
		userMap.clear();
		sessionActivity.clear();
	}

	/**
	 * Link a user to a session
	 * @param session The HttpSession
	 * @param user the user to link with the session
	 */
	public static void addUser(HttpSession session, TxmUser user) {
		Log.info("[SessionManager.addUser] Binding user : " + user.getLogin() + " with session : " + session.getId());
		userMap.put(session.getId(), user);
		sessionActivity.put(session.getId(), new Date());
	}
	
	public static void setIsAlive(String session) {
		sessionActivity.put(session, new Date());
	}
	
	public static void autoDisconnectInactiveUsers()
	{
		Date now = new Date();
		now.setTime(now.getTime() - AUTODECONN_TIMEOUT); // set limit date
		//System.out.println("Filter inactive users for time: "+now);
		for (String session : sessionActivity.keySet()) {
			//System.out.println(userMap.get(session).getLogin()+" > date : "+sessionActivity.get(session));
			TxmUser user = userMap.get(session);
			if (user != null && sessionActivity.get(session) != null)
			if (sessionActivity.get(session).before(now) && !user.isGuest()) {
				Log.info("Auto-deco : "+session+" = "+user.getLogin());
				ActivityLog.add(session, "no-ip", "xxx", user.getLogin(), "auto deconnexion", "");
				userMap.remove(session);
				createUser(session); // assign the guest use to the new session
			}
		}
	}

	/**
	 * @return Returns all the session map.
	 */
	public static Map<String, TxmUser> getUserMap() {
		return userMap;
	}

	/**
	 * @param sessionId The id of the session linked to the wanted user.
	 * @return The user linked to the given session id. May return a null user
	 */
	public static TxmUser getUser(String sessionId) {
		Log.fine("[SessionManager.getUser] Asking user linked to session : " + sessionId);
		setIsAlive(sessionId);
		if (userMap.containsKey(sessionId))
			return userMap.get(sessionId);
		else
			return null;//createUser(sessionId);
	}
	
	/**
	 * 
	 * @param sessionId
	 * @return an anonymous user linked to this session
	 */
	public static TxmUser createUser(String sessionId) {
		Log.fine("[SessionManager.getUser] Asking user linked to session : " + sessionId);
		if (userMap.containsKey(sessionId))
			return userMap.get(sessionId);
		return userMap.put(sessionId, UserManager.getGuestUser());
	}

	/**
	 * Remove an entry of the sessions map.
	 * @param sessionId The id of the session linked to the wanted user.
	 * @return True on success, false otherwise.
	 */
	public static boolean deleteSession(String sessionId) {
		Log.fine("[SessionManager.deleteSession] Session : " + sessionId);
		if (userMap.remove(sessionId) == null)
			return false;
		return true;
	}
}
