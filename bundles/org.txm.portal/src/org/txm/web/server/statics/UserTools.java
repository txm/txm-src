package org.txm.web.server.statics;

import java.io.StringReader;
import java.security.Permission;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.txm.utils.logger.Log;
import org.txm.web.aas.authorization.TxmPermission;
import org.txm.web.server.TxmUser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class UserTools {
	public static String toXml(TxmUser user)
	{
		if(user == null)
			return "";
		// TODO: serialize TxmUser.doc instead
		StringBuffer str = new StringBuffer();
		str.append("\t<user ");
		str.append(" login=\""+user.getLogin()+"\"");
		str.append(" mail=\""+user.getMail()+"\"");
		str.append(" md5=\""+user.getMD5Password()+"\"");
		str.append(" lastname=\""+user.getLastName()+"\"");
		str.append(" firstname=\""+user.getFirstName()+"\"");
		str.append(" phone=\""+user.getPhone()+"\"");
		str.append(" institution=\""+user.getInstitution()+"\"");
		str.append(" status=\""+user.getStatus()+"\"");
		str.append(">\n");
		str.append("\t\t<profiles>\n");
		if(user.getProfiles() != null)
		for(String profilename : user.getProfiles())
			str.append("\t\t\t<profile name=\""+profilename+"\"/>\n");
		str.append("\t\t</profiles>\n");
		str.append("\t\t<permissions>\n");
		if( user.getCorporaPermissions() != null)
		for(String entity : user.getCorporaPermissions().keySet())
		{
			str.append("\t\t\t<entity path=\""+entity+"\">\n");
			for(Permission perm : user.getCorporaPermissions().get(entity))
			{
				if (perm != null)
				if (perm instanceof TxmPermission)
					str.append("\t\t\t\t"+((TxmPermission)perm).toXml()+"\n");
				else// in this case it must be the AllPermission
					str.append("\t\t\t\t<permission value=\""+perm.getClass().getSimpleName()+"\"/>\n");
			}
			str.append("\t\t\t</entity>\n");
		}
		str.append("\t\t</permissions>\n");
		str.append("\t</user>\n");
		return str.toString();
	}

	public static TxmUser fromXml(Element userelem)
	{
		String login = userelem.getAttribute("login");
		if (login == null)
		{
			Log.severe("[UserTools.fromXml] user element has no attribute login");
			return null;
		}

		TxmUser user = UserManager.getUser(login);
		if (user == null) // it's a new user !
		{
			System.out.println("UserTools: fromXml: new user");
			user = new TxmUser();
		}
		else // it's an update
		{
			System.out.println("UserTools: fromXml: update user "+user);
		}

		if(user.load(userelem)) // try loading infos
		{
			if (!UserManager.create(user)) // try saving infos
			{
				System.out.println("UserTools: fromXml: Usermanager failed to create new user");
				return null;
			}
		}
		else
		{
			System.out.println("UserTools: fromXml: Usermanager failed to load user from eelement: "+userelem);
		}
		
		return user;
	}

	public static boolean importUsers(String usersxml)
	{
		Log.info("Import users: ");
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		Document dom;

		try {
			db = dbf.newDocumentBuilder();
			InputSource is = new InputSource( new StringReader( usersxml ) );
			dom = db.parse(is);
			Element root = dom.getDocumentElement();

			NodeList entityList = root.getElementsByTagName("user");// get all entity elements
			for (int iEntity = 0 ; iEntity < entityList.getLength() ; iEntity++) {
				Element userelem = (Element) entityList.item(iEntity);
				TxmUser user = fromXml(userelem);
				if (user == null) {
					System.out.println("Failed to load user from "+userelem);
					return false;
				} else {
					if (!user.save()) {
						Log.severe("Error: user not saved");
						return false;
					} else {
						Log.info("Create user successful: "+user.getLogin());
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static String exportUsers() {
		StringBuffer str = new StringBuffer();
		str.append("<users>\n");
		for (TxmUser user : UserManager.getUsers()) {
			if (user != null)
				str.append(toXml(user));
		}
		str.append("</users>");
		return str.toString();
	}

	public static Element toXml(TxmUser user, Document doc) {
		if (user == null || doc == null)
			return null;
		
		Element newElement = doc.createElement("user");
		newElement.setAttribute("login", user.getLogin());
		newElement.setAttribute("mail", user.getMail());
		newElement.setAttribute("md5", user.getMD5Password());
		newElement.setAttribute("lastname", user.getLastName());
		newElement.setAttribute("firstname", user.getFirstName());
		
		Element profiles = doc.createElement("profiles");
		newElement.appendChild(profiles);
		if (user.getProfiles() != null)
		for (String profilename : user.getProfiles()) {
			Element profile = doc.createElement("profile");
			profile.setAttribute("name", profilename);
			profiles.appendChild(profile);
		}
		
		Element permissions = doc.createElement("permissions");
		permissions.appendChild(profiles);
		if (user.getCorporaPermissions() != null)
		for (String entity : user.getCorporaPermissions().keySet()) {
			Element entityElem = doc.createElement("entity");
			permissions.appendChild(entityElem);
			entityElem.setAttribute("path", entity);

			for (Permission perm : user.getCorporaPermissions().get(entity)) {
				Element permElem = doc.createElement("permission");
				entityElem.appendChild(permElem);
				if (perm != null)
				if (perm instanceof TxmPermission)
					((TxmPermission)perm).toXml(permElem);
				
			}
		}
		return newElement;
	}

}
