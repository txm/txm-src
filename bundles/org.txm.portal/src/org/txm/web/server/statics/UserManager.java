package org.txm.web.server.statics;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.txm.utils.logger.Log;
import org.txm.web.server.TxmUser;

public class UserManager {
	private HashMap<String, TxmUser> txmusers = new HashMap<String, TxmUser>();
	private File xmlFileDir = new File(TxmConfig.getProperty("userBase"));

	private boolean initialized = false;
	protected static UserManager singleton;

	private UserManager()
	{
		singleton = this;
		_reloadAll();

		Runtime.getRuntime().addShutdownHook(new Thread(){
			public void run() {
		        try {
		        	System.out.println("Finalize");
		        	if (singleton == null) return;
		        	if (!singleton._saveAll())
		        		Log.severe("Singleton.shutdown error.");
		        } catch (Exception e) {
					Log.severe("Singleton.shutdown error: "+e);
				}
		      }
		});
		//System.out.println("USERS : "+txmusers);
	}
	
	private void _reloadAll()
	{
		Log.info("INIT USERS");
		txmusers.clear(); // remove All users

		if (!(xmlFileDir.exists() && xmlFileDir.isDirectory())) {
			Log.severe("User directory does not exists: "+xmlFileDir);
			return;
		}
		// load from xmlfile userX1Y2Z3.xml files
		for (File xmlFile : xmlFileDir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {return name.startsWith("user") && name.endsWith(".xml");}
		}))
		{
			System.out.println("Loading user from: "+xmlFile);
			try {
				TxmUser user = new TxmUser();
				if (user.load(xmlFile)) {
					txmusers.put(user.getLogin(), user); // store user
				} else
					Log.severe("Failed to load user from: "+xmlFile);
			} catch (Exception e) {
				Log.severe("Failed to load user from: "+xmlFile+" : "+e);
				e.printStackTrace();
			}
		}
		initialized = true;
	}

	public static void init()
	{
		if (singleton == null || !singleton.initialized) {
			Log.info("[UserManager] Starting up");
			singleton = new UserManager();
		}
	}

	/**
	 * Initialize and return the guest user
	 */
	public static TxmUser getGuestUser()
	{
		init();
		if (!singleton.txmusers.containsKey("guest")) {
			TxmUser guest = new TxmUser();
			guest.setLogin("guest");
			guest.setMail("nomail@nomail.fr");
			guest.save();
			singleton.txmusers.put("guest", guest);
		}
		//System.out.println("USERS: "+singleton.txmusers);
		return singleton.txmusers.get("guest");
	}

	public static TxmUser getUser(String login)
	{
		init();
		return singleton._getUser(login);
	}


	public TxmUser _getUser(String login)
	{
		//System.out.println("get user: "+login);
		if (!txmusers.containsKey(login)) {
			return null;
		}

		return txmusers.get(login);
	}

	/**
	 * save Xml document of the TxmUsers
	 * @return
	 */
	public boolean _saveAll()
	{
		for (TxmUser user : txmusers.values()) {
			if (!user.save())
				return false;
		}
		return true;
	}

	/**
	 * Serialize all user stored
	 */
	public static boolean saveAll() {
		init();
		return singleton._saveAll();
	}

	public static void reloadAll() {
		init();
		singleton._reloadAll();
	}

	public static boolean create(TxmUser user)
	{
		init();
		return singleton._create(user);
	}

	public boolean _create(TxmUser user)
	{
		txmusers.put(user.getLogin(), user);
		return user.save();
	}

	public static boolean remove(TxmUser user)
	{
		init();
		return singleton._remove(user);
	}

	public boolean _remove(TxmUser user)
	{
		String login = user.getLogin();
		if (txmusers.containsKey(login)) {
			txmusers.remove(login);
			return user.delete();
		}
		return false;
	}
	
	ArrayList<String> usersLogin;
	public static List<String> getAllUsers() {
		init();

		singleton.usersLogin = new ArrayList<String>();
		for (TxmUser user : singleton.txmusers.values())
			singleton.usersLogin.add(user.getLogin());

				return singleton.usersLogin;
	}
	
	public static Collection<TxmUser> getUsers() {
		init();
		return singleton.txmusers.values();
	}
	
	public static void stop() {
		if (singleton != null) {
			singleton.txmusers.clear();
		}
	}
}
