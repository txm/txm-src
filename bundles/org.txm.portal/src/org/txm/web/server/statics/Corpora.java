package org.txm.web.server.statics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.txm.Toolbox;
import org.txm.metadatas.Metadata;
import org.txm.objects.Base;
import org.txm.objects.Project;
import org.txm.objects.Text;
import org.txm.objects.TxmObject;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.CqpObject;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.utils.logger.Log;

/**
 * It's the container of the CqpObjects. Each CqpObject is linked with a path.
 * 
 * @see CqpObject
 * @author vchabani
 * @version 0.2.0
 */
public class Corpora {

	private static final long serialVersionUID = -1226960724634599338L;

	private static Map<String, TxmObject> corporaMap = new HashMap<String, TxmObject>();
	private static boolean isStarted = false;

	/**
	 * Fills the corpora map from the Toolbox.
	 */
	public static void start() throws IllegalArgumentException {
		if (!isStarted) {
			isStarted = true;
			fillCorporaMap();
			Log.fine("[Corpora.start] Started, content ->\n" + dump());
		}
	}	

	/**
	 * Fills the corpora map from the Toolbox.
	 */
	public static void restart() throws IllegalArgumentException {
		fillCorporaMap();
		Log.fine("[Corpora.restart] Restarted, content ->\n" + dump());
	}

	/**
	 * @return All the corpora map.
	 */
	public static Map<String, TxmObject> getCorporaMap() {
		return corporaMap;
	}

	/**
	 * Returns the CqpObject linked to the given key.
	 * @param key The absolute path to the CqpObject
	 */
	public static TxmObject get(String key) {
		return (corporaMap.get(key));
	}

	/**
	 * Adds the CqpObject in the corpora map to the given path
	 * @param key The absolute path to the CqpObject
	 * @param value The CqpObject
	 */
	public static TxmObject put(String key, TxmObject value) {
		return (corporaMap.put(key, value));
	}

	/**
	 * Verif y if the given key exists.
	 * @param key The absolute path to the CqpObject
	 * @return True if the key exists, false otherwise.
	 */
	public static boolean containsKey(String key) {
		return corporaMap.containsKey(key);
	}

	/**
	 * Delete the entry from the corpora map.
	 * @param key The absolute path to the CqpObject
	 * @return The previous value associated with key, or null if there was no mapping for key.
	 */
	public static Object remove(String key) {
		ArrayList<String> keys = new ArrayList<String>(corporaMap.keySet());
		Object o = corporaMap.get(key);
		for(String k : keys) {
			if (k.startsWith(key)) {
				corporaMap.remove(k);
			}
		}
		return o;		
	}

	/**
	 * @return The list of the paths of all CqpObjects.
	 */
	public static Set<String> getCorporaList() {
		return corporaMap.keySet();
	}

	/**
	 * Returns the list of the entities contained by the specif ied path.
	 * @param parent The path of an entity.
	 * @return
	 */
	public static List<String> getChildPaths(String parent) {
		ArrayList<String> result = new ArrayList<String>();
		for (String entry : corporaMap.keySet()) {
			if (entry.startsWith(parent) && !entry.equals(parent))
				result.add(entry);
		}
		return result;
	}

	/**
	 * Builds the corpora map using the Bases declared in the workspace
	 */
	private static void fillCorporaMap() {
		Project project = Toolbox.workspace.getProject("default");
		for (Base base : project.getBases()) {
			HashMap<String, MainCorpus> subCorpora = base.getCorpora();
			for(String corpusname : base.getCorpora().keySet())
			{
				String path = "/" + corpusname;
				MainCorpus corpus = subCorpora.get(corpusname);
				getChildren(corpus, path); // register the corpus and its children in Corpora
			}
		}
	}

	/**
	 * Recursive method to build the Corpora paths.
	 * 
	 */
	private static void getChildren(Corpus corpus, String path) {	
		corporaMap.put(path, corpus);
		for (Subcorpus subcorpus : corpus.getSubcorpora()) {
			corporaMap.put(path + "/" + subcorpus.getName(), subcorpus);
			getChildren(subcorpus, path + "/" + subcorpus.getName());
		}
		for (Partition partition : corpus.getPartitions()) {
			corporaMap.put(path + "/" + partition.getName(), partition);
		}
		for (Text text : corpus.getTexts()) {
			corporaMap.put(path + "/" + text.getName(), text);
		}
	}

	/**
	 * @return A string representation of the corpora map.
	 */
	public static String dump() {
		String result = "[\nCorpora\n";
		for (String entry : corporaMap.keySet()) {
			result += "\t" + entry + "\n";
		}
		result += "]\n";
		return result;
	}

	public static List<String> getStructuralUnitPropertyValues(String itemPath, String structName, String propName) {
		Corpus corpus;
		if (Corpora.get(itemPath) instanceof Partition)
			corpus = ((Partition)Corpora.get(itemPath)).getCorpus();
		else if (Corpora.get(itemPath) instanceof Corpus)
			corpus = (Corpus)Corpora.get(itemPath);
		else
			return null;
		List<StructuralUnit> structuralUnitList = null;
		try { 
			structuralUnitList = corpus.getStructuralUnits();
		} catch (CqiClientException e) {
			e.printStackTrace();
		}
		StructuralUnit su = null;
		for(StructuralUnit struct : structuralUnitList)	{
			if (struct.getName().equals(structName)) {
				su = struct;
				break;
			}
		}
		StructuralUnitProperty prop = su.getProperty(propName);
		try {
			return prop.getValues();
		} catch (CqiClientException e) {
			e.printStackTrace();
			return new ArrayList<String>();
		}
	}

	public static List<String> getStructuralUnits(String itemPath) {
		Corpus corpus;
		if (Corpora.get(itemPath) instanceof Partition)
			corpus = ((Partition)Corpora.get(itemPath)).getCorpus();
		else if (Corpora.get(itemPath) instanceof Corpus)
			corpus = (Corpus)Corpora.get(itemPath);
		else
			return null;
		List<StructuralUnit> structuralUnitList = null;
		try { 
			structuralUnitList = corpus.getStructuralUnits();
		} catch (CqiClientException e) {
			e.printStackTrace();
		}
		ArrayList<String> result = new ArrayList<String>();
		for(StructuralUnit su : structuralUnitList)	{
			result.add(su.getName());
		}
		return result;
	}

	public static List<String> getStructuralUnitsProperties(String itemPath,
			String structName) {
		Corpus corpus;
		if (Corpora.get(itemPath) instanceof Partition)
			corpus = ((Partition)Corpora.get(itemPath)).getCorpus();
		else if (Corpora.get(itemPath) instanceof Corpus)
			corpus = (Corpus)Corpora.get(itemPath);
		else
			return null;
		List<StructuralUnit> structuralUnitList = null;
		try { 
			structuralUnitList = corpus.getStructuralUnits();
		} catch (CqiClientException e) {
			e.printStackTrace();
		}
		ArrayList<String> result = new ArrayList<String>();
		for (StructuralUnit su : structuralUnitList) {
			if (su.getName().equals(structName)) {
				for(Property p : su.getProperties())
					result.add(p.getName());
						break;
			}
		}

		return result;
	}

	public static String[] getReferences(String itemPath) {
		Corpus corpus = (Corpus)Corpora.get(itemPath);
		List<StructuralUnit> structuralUnitList = null;
		try { 
			structuralUnitList = corpus.getStructuralUnits();
		} catch (CqiClientException e) {
			e.printStackTrace();
		}
		ArrayList<String> unitProperties = new ArrayList<String>();
		for(StructuralUnit su : structuralUnitList)	{
			for(Property p : su.getProperties())
				unitProperties.add(su.getName() + ":" + p.getName());
		}
		try {
			for (Property p : corpus.getProperties()) {
				unitProperties.add(p.getName());
			}
		} catch (CqiClientException e) {
			e.printStackTrace();
		}
		return unitProperties.toArray(new String[unitProperties.size()]);
	}

	public static String[] getWordProperties(String itemPath) {
		Corpus corpus;
		if (Corpora.get(itemPath) instanceof Partition)
			corpus = ((Partition)Corpora.get(itemPath)).getCorpus();
		else if (Corpora.get(itemPath) instanceof Corpus)
			corpus = (Corpus)Corpora.get(itemPath);
		else
			return null;

		List<Property> propertyList = null;
		try {
			propertyList = corpus.getProperties();
		} catch (CqiClientException e) {
			e.printStackTrace();
			return null;
		}
		ArrayList<String> wordProperties = new ArrayList<String>();
		for (Property p : propertyList) {
			wordProperties.add(p.getName());
		}
		return wordProperties.toArray(new String[wordProperties.size()]);
	}

	public static HashMap<String, List<String>> getStructuralUnitPropertyValues(String itemPath,
			String structName) {
		HashMap<String, List<String>> result = new HashMap<String, List<String>>();
		TxmObject obj = Corpora.get(itemPath);
		if (obj instanceof Corpus)
		{
			Corpus corpus = (Corpus)obj;
			StructuralUnit su;
			try {
				su = corpus.getStructuralUnit(structName);
			} catch (CqiClientException e) {
				e.printStackTrace();
				return result;
			}

			for(StructuralUnitProperty sup : su.getProperties())
			{
				try {
					result.put(sup.getName(), sup.getValues());
				} catch (CqiClientException e) {
					e.printStackTrace();
					return result;
				}
			}
		}
		return result;
	}

	public static HashMap<String, String> getMetadataTypes(String itemPath) {
		HashMap<String, String> result = new HashMap<String, String>();

		TxmObject obj = Corpora.get(itemPath);
		if (obj instanceof Corpus)
		{
			Corpus corpus = (Corpus) obj;

			for(Metadata m : corpus.getImportMetadata())
			{
				result.put(m.id, m.type);
			}
		}
		return result;
	}

	public static String getParentPath(String path) {
		if(path.lastIndexOf("/") > 0)
			return path.substring(0, path.lastIndexOf("/"));
		return path;
	}

	public static List<String> getStructuralUnitsProperties(String itemPath) {
		Corpus corpus;
		if (Corpora.get(itemPath) instanceof Partition)
			corpus = ((Partition)Corpora.get(itemPath)).getCorpus();
		else if (Corpora.get(itemPath) instanceof Corpus)
			corpus = (Corpus)Corpora.get(itemPath);
		else
			return null;
		List<StructuralUnit> structuralUnitList = null;
		try { 
			structuralUnitList = corpus.getStructuralUnits();
		} catch (CqiClientException e) {
			e.printStackTrace();
		}
		ArrayList<String> result = new ArrayList<String>();
		for (StructuralUnit su : structuralUnitList) 
			for(Property p : su.getProperties())
				result.add(su.getName()+"_"+p.getName());

					return result;
	}

	public static void stop() {
		corporaMap.clear();
	}
}
