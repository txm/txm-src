package org.txm.web.server.statics;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Tools {
	public static List<String> listDir(String dirPath, String extension) {
		ArrayList<String> result = new ArrayList<String>();
		File profileDir = new File(dirPath);
		for (String file : profileDir.list()) {
			if (file.endsWith(extension))
				result.add(file);
		}
		return result;
	}

	public static boolean deleteDirectory(File path) {
		if( path.exists() ) {
			File[] files = path.listFiles();
			for(int i=0; i<files.length; i++) {
				if(files[i].isDirectory()) {
					deleteDirectory(files[i]);
				}
				else {
					files[i].delete();
				}
			}
		}
		return( path.delete() );
	}

}
