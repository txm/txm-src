package org.txm.web.server.statics;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

import org.txm.importer.ValidateXml;
import org.txm.web.aas.CRUD;
import org.txm.web.aas.CRUD.Type;
import org.txm.web.aas.authentication.TxmLoginModule;
import org.txm.web.aas.authorization.TxmPermission;
import org.txm.web.server.TxmUser;

public class RecupUserOldFormat {
	public static void main(String[] args) throws Exception
	{
		int count = 0;
		File dir = new File("/home/mdecorde/Bureau/Experiences/users");
		File outdir = new File("/home/mdecorde/Bureau/Experiences/users/xml");
		outdir.mkdir();
		TxmLoginModule m = new TxmLoginModule();
		TxmLoginModule.LOGINBASE = "/home/mdecorde/Bureau/Experiences/users/loginBase.txt";
		m.load();	
		TxmConfig.configFile = new File("/home/mdecorde/TXMWEB/repo/bfm/txmweb.conf");
		System.out.println("Profiles ready ? "+Profiles.start());
		
		ArrayList<String> errors = new ArrayList<String>();
		
		for(File f : dir.listFiles())
		{
			if(f.canRead() && f.getName().endsWith(".usr"))
			{
				CRUD.rootDir = dir.getAbsolutePath();
				String name = f.getName().substring(0, f.getName().length()-4);
				File outfile = new File(outdir, "user_"+name+".xml");
				PrintWriter writer = new PrintWriter(new FileWriter(outfile));
				Object o = CRUD.read(name, Type.USER);
				if(o != null)
				{
					try
					{
						TxmUser user = (TxmUser) o;
						count++;
						writer.print("<user");
						writer.print(" login=\""+user.getLogin()+"\"");
						writer.print(" firstname=\""+user.getFirstName()+"\"");
						writer.print(" lastname=\""+user.getLastName()+"\"");
						writer.print(" mail=\""+user.getMail()+"\"");
						writer.print(" status=\""+user.getStatus()+"\"");
						writer.print(" institution=\""+user.getInstitution()+"\"");
						writer.print(" phone=\""+user.getPhone()+"\"");
						writer.print(" md5=\""+m.getMD5Login(user.getLogin())+"\"");
						writer.println(">");
						
						writer.println(" <profiles>");
						for(String profile : user.getProfiles())
							writer.println("  <profile name=\""+profile+"\"/>");
						writer.println(" </profiles>");
						
						writer.println(" <permissions>");
						for(String entity : user.getCorporaList())
						{
							writer.println("  <entity path=\""+entity+"\">");
							for(TxmPermission perm : user.getPermissions(entity))
								if(perm != null)
								{
									writer.println(perm.toXml());
								}
								else
									errors.add(user.getLogin()+" failed to get perm for entity: "+entity);
							writer.println("  </entity>");
						}
						writer.println(" </permissions>");
						writer.println("</user>");

					}catch(Exception e ){e.printStackTrace();}
				}
				else
				{
					System.out.println("cant load from: "+f);
				}
				writer.close();
				
				if(!ValidateXml.test(outfile))
					outfile.delete();
			}
		}
		
		System.out.println("Nb users: "+count);
		for(String error : errors)
			System.out.println(error);
	}
}
