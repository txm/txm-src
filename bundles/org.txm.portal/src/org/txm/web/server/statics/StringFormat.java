package org.txm.web.server.statics;


public class StringFormat {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String test = "1: {0} 2: {1} 3: {2} 1bis: {0}";
		String test2 = "1: %1$s 2: %2$s 3: %3$s 1bis: %1$s";
		System.out.println(StringFormat.bind(test, "1", "2", "3"));
		System.out.println(StringFormat.format(test2, "1", "2", "3"));
	}
	
	public static String bind(String str, String... values)
	{
		if(values != null)
		for(int i = 0 ; i < values.length ; i++) {
			String s = values[i];
			//System.out.println("replace {"+i+"} by "+s);
			if (s == null)
				str = str.replace("{"+i+"}", "");
			else
				str = str.replace("{"+i+"}", s);
		}
		return str;
	}
	
	public static String format(String str, String... values)
	{
		if(values != null)
		for (int i = 1 ; i <= values.length ; i++) {
			String s = values[i-1];
			//System.out.println("replace %"+i+"$s by "+s);
			if (s == null)
				str = str.replace("%"+i+"$s", "");
			else
				str = str.replace("%"+i+"$s", s);
		}
		return str;
	}
}
