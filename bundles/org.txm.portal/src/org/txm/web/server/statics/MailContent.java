package org.txm.web.server.statics;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;

import org.txm.utils.Pair;
import org.txm.utils.logger.Log;

/**
 * Get the content of mails of type (inscription, confirmation, lost_password and profile_assignement
 * @author mdecorde
 *
 */
public class MailContent {
	
	static boolean isLoaded = false;
	
	private static String defaultInscription="This is the default inscription mail";
	private static String defaultConfirmation="This is the default confirmation mail";
	private static String defaultAccessAndConfirmation="This is the default accessAndConfirmation mail";
	private static String defaultLostPassword="This is the default lostPassword mail";
	private static String defaultUpdateMail="This is the default updateMail mail";
	
	private static String defaultInscriptionSubject="This is the default inscription mail subject";
	private static String defaultConfirmationSubject="This is the default confirmation mail subject";
	private static String defaultAccessAndConfirmationSubject="This is the default accessAndConfirmation mail subject";
	private static String defaultLostPasswordSubject="This is the default lostPassword mail subject";
	private static String defaultUpdateMailSubject="This is the default updateMail mail subject";

	static HashMap<String, String> inscription;
	static HashMap<String, String> confirmation;
	static HashMap<String, String> accessAndConfirmation;
	static HashMap<String, String> lostPassword;
	static HashMap<String, String> updateMail;
	
	static HashMap<String, String> inscriptionSubjects;
	static HashMap<String, String> confirmationSubjects;
	static HashMap<String, String> accessAndConfirmationSubjects;
	static HashMap<String, String> lostPasswordSubjects;
	static HashMap<String, String> updateMailSubjects;
	
	public static String MAILBASE;// = TxmConfig.getProperty("mailBase");
	
	public static boolean load() {
		
		if (isLoaded) {
			return true;
		}
		
		MAILBASE = TxmConfig.getProperty("mailBase");
		if (MAILBASE == null) {
			MAILBASE = new File(TxmConfig.getConfigFile().getParentFile(), "data/mails").getAbsolutePath();
		}
		
		File mailDir = new File(MAILBASE);
		
		if (!mailDir.exists()) {
			Log.severe("[MailContent.start] Profiles directory does not exists: "+mailDir.getAbsolutePath());
			isLoaded = false;
			return false;
		}
		
		if (!mailDir.isDirectory()) {
			Log.severe("[MailContent.start] profileBase path is not a directory: "+mailDir.getAbsolutePath());
			isLoaded = false;
			return false;
		}

		inscription = new HashMap<String, String>();
		confirmation = new HashMap<String, String>();
		accessAndConfirmation = new HashMap<String, String>();
		lostPassword = new HashMap<String, String>();
		updateMail = new HashMap<String, String>();
		
		inscriptionSubjects = new HashMap<String, String>();
		confirmationSubjects = new HashMap<String, String>();
		accessAndConfirmationSubjects = new HashMap<String, String>();
		lostPasswordSubjects = new HashMap<String, String>();
		updateMailSubjects = new HashMap<String, String>();
		
		Log.info("[MailContent.start] Load mails content from directory: "+mailDir.getAbsolutePath());
		// initialize commons messages
		for (File f : mailDir.listFiles()) {
			if (!(f.canRead() && f.isFile() && !f.isHidden() && f.getName().endsWith(".txt")))
				continue;
			Log.info("read file: "+f);
			String filename = f.getName();
			String lang = filename.substring(0, 2);
			Pair<String, String> pair = getContent(f);
			if (filename.contains("_accessAndConfirmation.txt")) {
				accessAndConfirmation.put(lang, pair.getFirst());
				accessAndConfirmationSubjects.put(lang, pair.getSecond());
			} else if (filename.contains("_inscription.txt")) {
				inscription.put(lang, pair.getFirst());
				inscriptionSubjects.put(lang, pair.getSecond());
			} else if (filename.contains("_confirmation.txt")) {
				confirmation.put(lang, pair.getFirst());
				confirmationSubjects.put(lang, pair.getSecond());
			} else if (filename.contains("_lostPassword.txt")) {
				lostPassword.put(lang, pair.getFirst());
				lostPasswordSubjects.put(lang, pair.getSecond());
			} else if (filename.contains("_updateMail.txt")) {
				updateMail.put(lang, pair.getFirst());
				updateMailSubjects.put(lang, pair.getSecond());
			}
		}
		
		Log.info("[MailContent.start] Started, inscription messages : "+inscription);
		Log.info("[MailContent.start] Started, accessAndConfirmation messages : "+accessAndConfirmation);
		Log.info("[MailContent.start] Started, confirmation messages : "+confirmation);
		Log.info("[MailContent.start] Started, lostPassword messages : "+lostPassword);
		Log.info("[MailContent.start] Started, updateMail messages : "+lostPassword);
		isLoaded = true;
		return true;
	}
		
	public static org.txm.utils.Pair<String, String> getContent(File f)
	{
		String subject = "";
		StringBuffer rez = new StringBuffer();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(f));
			subject = reader.readLine(); //subject line
			String line = reader.readLine();
			while (line != null) {
				rez.append(line+"\n");
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new Pair<String, String>("", "");
		}
		return new Pair<String, String>(rez.toString(), subject);
	}
	
	public static String getInscription(String lang)
	{
		if (!isLoaded)
			load();
		String content = inscription.get(lang);
//		if(content == null)
//			content = defaultInscription;
		return content;
	}
	
	public static String getConfirmation(String lang)
	{
		if (!isLoaded)
			load();
		String content = confirmation.get(lang);
//		if(content == null)
//			content = defaultConfirmation;
		return content;
	}
	
	public static String getAccessAndConfirmation(String lang)
	{
		if (!isLoaded)
			load();
		String content = accessAndConfirmation.get(lang);
//		if(content == null)
//			content = defaultAccessAndConfirmation;
		return content;
	}
	
	public static String getLostPassword(String lang)
	{
		if (!isLoaded)
			load();
		String content = lostPassword.get(lang);
//		if(content == null)
//			content = defaultLostPassword;
		return content;
	}
	
	public static String getUpdateMail(String lang)
	{
		if (!isLoaded)
			load();
		String content = updateMail.get(lang);
//		if(content == null)
//			content = defaultUpdateMail;
		return content;
	}
	
	public static String getInscriptionSubjects(String lang)
	{
		if (!isLoaded)
			load();
		String subject = inscriptionSubjects.get(lang);
//		if(subject == null)
//			subject = defaultInscriptionSubject;
		return subject;
	}
	
	public static String getConfirmationSubjects(String lang)
	{
		if (!isLoaded)
			load();
		String subject = confirmationSubjects.get(lang);
//		if(subject == null)
//			subject = defaultConfirmationSubject;
		return subject;
	}
	
	public static String getAccessAndConfirmationSubjects(String lang)
	{
		if (!isLoaded)
			load();
		String subject = accessAndConfirmationSubjects.get(lang);
//		if(subject == null)
//			subject = defaultAccessAndConfirmationSubject;
		return subject;
	}
	
	public static String getLostPasswordSubjects(String lang)
	{
		if (!isLoaded)
			load();
		String subject = lostPasswordSubjects.get(lang);
//		if(subject == null)
//			subject = defaultLostPasswordSubject;
		return subject;
	}
		
	public static String getUpdateMailSubjects(String lang)
	{
		if (!isLoaded)
			load();
		String subject = updateMailSubjects.get(lang);
//		if(subject == null)
//			subject = defaultUpdateMailSubject;
		return subject;
	}
}
