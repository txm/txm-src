package org.txm.web.server.statics;

import java.io.File;
import java.util.HashSet;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.txm.utils.logger.Log;
import org.txm.web.server.services.AuthenticationServiceImpl;


/**
 * This listener allow the application to store the new sessions in the SessionManager, 
 * and remove them when they are time out.
 * This class have to be declared in the web.xml.
 * 
 * @see SessionManager
 * @author vchabani
 * @version 0.2.0
 */
public class TxmSessionListener implements HttpSessionListener {
	
	/** This is the maximal duration of a session in seconds*/
	private static int TIMEOUT = 72000 ;
	public static HashSet<HttpSession> sessions;
	@Override
	public void sessionCreated(HttpSessionEvent event) {
		if (sessions == null)
			sessions = new HashSet<HttpSession>();
		Log.info("[TxmSessionListener.sessionCreated] Got a new HTTP session : " + event.getSession().getId());
		HttpSession currentSession = event.getSession();
		sessions.add(currentSession);
		currentSession.setMaxInactiveInterval(TIMEOUT);
		SessionManager.startSession(currentSession);
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		Log.info("[TxmSessionListener.sessionDestroyed] Destroying session : " + event.getSession().getId());
		HttpSession currentSession = event.getSession();
		sessions.remove(currentSession);
		
		AuthenticationServiceImpl.cleanResults(currentSession.getId());
		
		SessionManager.deleteSession(currentSession.getId());
		String path = event.getSession().getServletContext().getRealPath("") + "/data/svg/"+currentSession.getId();
		Tools.deleteDirectory(new File(path));
	}
	
	public static int getTIMEOUT() {
		return TIMEOUT;
	}
}
