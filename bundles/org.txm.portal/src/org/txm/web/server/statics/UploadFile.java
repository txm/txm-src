package org.txm.web.server.statics;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Utility class to upload a file using a Servlet
 * 
 * @author mdecorde
 *
 */
public class UploadFile {

	/**
	 * 
	 * @param outDirectory
	 * @param req
	 * @param res
	 * @return the created file
	 */
	public static File doIt(File outDirectory, HttpServletRequest req, HttpServletResponse res) {
		System.out.println("UploadFile.doIt: uploading into: "+outDirectory);
		PrintWriter out;
		try {
			out = res.getWriter();
		} catch (IOException e1) {
			e1.printStackTrace();
			return null;
		}

		boolean isMultipart = ServletFileUpload.isMultipartContent(req);
		if (!isMultipart) {
			System.out.println("Request is not a multipart request.");
			out.println("this is not a multipart POST");
			return null;
		}

		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();

		ServletFileUpload servletFileUpload = new ServletFileUpload(factory);
		//out.println("Processing request...");
		// Parse the request
		try {
			List<FileItem> items = servletFileUpload.parseRequest(req);

			for (FileItem item : items) {
				
				if (item.isFormField()) {
					continue;
				}

				//System.out.println("UploadFile.doIt: uploading into: file name: "+item.getName());
				File newFile = new File(outDirectory, item.getName());
				//if (item instanceof FileItemStream) {
				//out.println("<br>Copying file "+item.getName()+" to "+f+"...");
				System.out.println("UploadFile.doIt: Copying file "+item.getName()+" to "+newFile+"...");
				//FileItemStream fitem = (FileItemStream)item;

				FileOutputStream fos = new FileOutputStream(newFile);
				InputStream fis = item.getInputStream();

				byte[] b = new byte[1000];
				while (fis.read(b) > 0) {
					fos.write(b);
				}
				fos.close();
				fis.close();
				
				out.println("File copied. ");
				return newFile;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			out.println("Internal exception: "+e);
			return null;
		}
		
		return null;
	}
}
