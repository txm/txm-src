package org.txm.web.server.statics;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.txm.utils.IOUtils;
import org.txm.utils.logger.Log;
import org.txm.web.aas.authorization.Profile;

/**
 * Contains the profiles of TXMWEB.
 * One profile is associated to one and only one name
 * @author vchabanis
 *
 */
public class Profiles {

	private static Map<String, Profile> profiles = new HashMap<String, Profile>();
	//public static final String PROFILEBASE = ;
	private static boolean isLoaded = false;

	public static boolean reload() {
		isLoaded = false;
		return start();
	}

	public static void stop() {
		profiles.clear();
	}

	public static boolean start() {
		if (isLoaded) {
			Log.info("[Profiles.start] Profiles are already loaded");
			return true;
		}
		File profileDir = new File(TxmConfig.getProperty("profileBase"));

		if (!profileDir.exists()) {
			Log.severe("[Profiles.start] Profiles directory does not exists: "+profileDir.getAbsolutePath());
			return false;
		}

		if (!profileDir.isDirectory()) {
			Log.severe("[Profiles.start] profileBase path is not a directory: "+profileDir.getAbsolutePath());
			return false;
		}

		Log.info("[Profiles.start] Load profiles from directory: "+profileDir.getAbsolutePath());

		for (String profileFilename : listProfileDir()) {
			Profile p = new Profile();
			/*int idx = profileName.lastIndexOf(".");
			if(idx > 0)
				profileName = profileName.substring(0, idx);*/
			if (p.load(profileFilename)) {
				Log.info("[Profiles.start] loading profile: " + p.getName());
				profiles.put(p.getName(), p);
			}
			else {
				Log.severe("[Profiles.start] Error while loading profile: " + profileFilename);
			}
		}
		Log.fine("[Profiles.start] Started, content ->\n" + profiles.keySet());

		if (!profiles.containsKey("Newbie")) {
			Log.severe("[Profiles.start] Warning no 'Newbie' profile found in " + profileDir);
		}
		if (!profiles.containsKey("Anonymous")) {
			Log.severe("[Profiles.start] Warning no 'Anonymous' profile found in " + profileDir);
		}
		if (!profiles.containsKey("Admin")) {
			Log.severe("[Profiles.start] Warning no 'Admin' profile found in " + profileDir);
		}
		isLoaded = true;
		return true;
	}


	/**
	 * 
	 * @param xmlFile
	 * @param xsdFile
	 * @return error message if any ; null if validation is successful
	 */
	public static String validateXMLSchema(String xmlContent, File xsdFile){
		File tmp = null;
		try {
			tmp = File.createTempFile("validate", ".xml");
			PrintWriter writer = IOUtils.getWriter(tmp, "UTF-8");
			writer.write(xmlContent);
			writer.close();
			
			return validateXMLSchema(tmp, xsdFile);
			
		} catch (Exception e) {
			return e.getMessage();
		} finally {
			if (tmp != null) tmp.delete();
		}
	}
	
	/**
	 * 
	 * @param xmlFile
	 * @param xsdFile
	 * @return error message if any ; null if validation is successful
	 */
	public static String validateXMLSchema(File xmlFile, File xsdFile){

		try {
			SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = factory.newSchema(xsdFile);
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(xmlFile));

		} catch (Exception e) {
			return e.getMessage();
		}

		return null;
	}

	/**
	 * 
	 * @return the list of profile names
	 */
	public static List<String> listProfileDir() {
		ArrayList<String> result = new ArrayList<String>();
		File profileDir = new File(TxmConfig.getProperty("profileBase"));
		for (String file : profileDir.list()) {
			if (file.endsWith(".xml"))
				result.add(file);
		}
		return result;
	}

	public static Profile getAnonymousProfile() {

		Profile anonymousProfile = null;
		if (profiles != null) {
			anonymousProfile = profiles.get("Anonymous");
		}
		if (anonymousProfile == null) {
			anonymousProfile = new Profile();
			anonymousProfile.setName("Anonymous");
		}
		return anonymousProfile;
	}

	public static Profile getAdminProfile() {
		Profile adminProfile = null;
		if (profiles != null) {
			adminProfile = profiles.get("Admin");
		}
		if (adminProfile == null) {
			adminProfile = new Profile();
			adminProfile.setName("Admin");
		}
		return adminProfile;
	}

	public static Profile getNewbieProfile() {
		Profile newbieProfile = null;
		if (profiles != null) {
			newbieProfile =  profiles.get("Newbie");
		}
		if (newbieProfile == null) {
			newbieProfile = new Profile();
			newbieProfile.setName("Newbie");
		}
		return newbieProfile;
	}

	public static Set<String> getProfileNames() {
		return profiles.keySet();
	}

	public static Map<String, Profile> getProfiles() {
		return profiles;
	}

	/**
	 * 
	 * @param profileName
	 * @return the profile associated to the profile name
	 */
	public static Profile getProfile(String profileName) {
		if (profiles.containsKey(profileName)) {
			return profiles.get(profileName);
		} else {
			Log.warning("[Profiles.getProfile] profiles does not contain "+profileName);
			Profile profile = new Profile();
			profile.setName(profileName+" not set in files");
			return profile;
		}
	}

	/**
	 * Add a profile
	 * @param name
	 * @param profile
	 * @return the added profile
	 */
	public static Profile addProfile(String name, Profile profile) {
		return profiles.put(name, profile);
	}

	public static void remove(String name) {
		profiles.remove(name);
	}

	public static void main(String[] args) {
		File xsd = new File(System.getProperty("user.home"), "/TXMWEB/txm/data/profiles/Profile.xsd"); 
		String error = "OK";
		if (xsd.exists()) {
			error = validateXMLSchema(new File(System.getProperty("user.home"), "/TXMWEB/txm/data/profiles/Newbie.xml"), xsd);
		}
		
		if (error == null) { //$NON-NLS-1$
			System.out.println("OK"); //$NON-NLS-1$
		} else {
			System.out.println("ERROR"); //$NON-NLS-1$
		}
	}

	public static String validateProfile(String content) {
		File profileDir = new File(TxmConfig.getProperty("profileBase"));
		File xsdFile = new File(profileDir, "Profile.xsd");
		
		String error = "OK";
		if (xsdFile.exists()) {
			error = validateXMLSchema(content, xsdFile);
		}
		return error;
		
	}
}
