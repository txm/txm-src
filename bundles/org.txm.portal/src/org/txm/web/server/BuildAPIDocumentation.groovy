package org.txm.web.server;

// Copyright © 2022 MY_INSTITUTION
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author mdecorde

// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

@Field @Option(name="sdk", usage="an example folder", widget="Folder", required=false, def="C:/Temp")
		def sdk

if (!ParametersDialog.open(this)) return

	if (!sdk.exists()) {
		println "sdk does not exists: "+sdk
		return;
	}

File keysClass = new File(sdk, "TXMSDK-workspace/TXM WEB/src/org/txm/web/shared/Keys.java")
String start = "public static final String "
def keys = [:]
keysClass.eachLine() { line ->
	line = line.trim()

	if (line.startsWith(start)) {
		line = line.substring(start.length())
		int idx = line.indexOf(" = ")
		keys[line.substring(0, idx)] = line.substring(idx+4, line.length() - 2)
	}
}

keys.sort()
println "Annexe : Liste complète des mots-clés de commande et paramètres \n  * "+keys.values().sort().join("\n  * ")

File commandsClass = new File(sdk, "TXMSDK-workspace/TXM WEB/src/org/txm/web/client/Commands.java")
String startCommand = "Keys."
String endCommand = ".equals(cmd)"
def commands = new LinkedHashMap<String, String>()
commandsClass.eachLine() { line ->
	line = line.trim()
	String command = ""
	while (line.contains(startCommand) && line.contains(endCommand) && line.indexOf(startCommand) < line.indexOf(endCommand)) {

		//println "LINE = $line"
		line = line.substring(line.indexOf(startCommand) + startCommand.length())
		//println "LINE2 = $line"
		int idx = line.indexOf(endCommand)
		//println "IDX = $idx"
		def s  = line.substring(0, idx)
		//println "S=$s"

		if (keys.containsKey(s)) {
			
			if (command.length() > 0) {
				commands.put(keys.get(s), ["*", "Voir la commande "+command]);	
			} else {
				command = keys.get(s);
				commands.put(command, ["", ""]);	
				if (line.indexOf("//") > 0) {
					def comment = line.substring(line.indexOf("//")+2).trim()
					def type = "NAVIGATION"
					if (comment.indexOf("//") > 0) {
						type = comment.substring(0, comment.indexOf("//")).trim()
						comment = comment.substring(comment.indexOf("//")+2).trim()
					}
					commands.put(command, [type, comment]);	
				}
			}
		} else {
			println "** NOT COMMAND KEY: $line"
		}
		
		line = line.substring(idx + endCommand.length())

	}
}
commands.sort()
println "Liste complète des commandes :\n"
def lines = []
for (def cmd : commands.keySet().sort()) {
	lines << "  * "+commands.get(cmd)[0]+" : "+cmd+ " : "+commands.get(cmd)[1]
}
lines.sort()
println lines.join("\n")

def markers = ["parameters.get(Keys.":"", ".setName(Keys.": "type non défini", "new ComboBoxItem(Keys.":"un choix parmis plusieurs", "new CheckboxItem(Keys.":"un booléen", "new RadioGroupItem(Keys.": "un choix alternatif", "new TSQueryItem(Keys.":"requête TIGERSearch",  "new QueryItem(Keys.":"requête CQL", "new SelectPropertyItem(Keys.":"nom d'une propriété de mot", "new SelectStructureItem(Keys.":"nom d'une structure de mot", "new SelectItem(Keys.":"liste de valeurs séparées par des virgules", "new FloatItem(Keys.":"nombre réel", "new SpinnerItem(Keys.":"nombre entier", "new OrderedPickListItem(Keys.":"liste de valeurs séparées par des virgules et ordonées", "new TextItem(Keys.":"chaîne de caractères"]

def forms = new LinkedHashMap<String, String>()
def parameters = [:]
for (def cmd : commands.keySet().sort()) {
	forms.put(cmd, "    * pas de paramètre");
	parameters.put(cmd, []);
}

File formsPackage = new File(sdk, "TXMSDK-workspace/TXM WEB/src/org/txm/web/client/widgets/forms")
for (File formClass : formsPackage.listFiles().sort()) {

	if (!formClass.getName().endsWith("Form.java")) continue

	StringWriter writer = new StringWriter()
	def name = formClass.getName()

	name = name.substring(0, name.indexOf("Form.")).toLowerCase()
	tmp = new LinkedHashSet()
	formClass.eachLine() { line ->
		line = line.trim()
		if (line.startsWith("//")) {
			// nothing
		} else {
			for (def marker : markers.keySet()) {
				if (line.contains(marker)) {
					def s = line.substring(line.indexOf(marker)+marker.length())
					if (s.indexOf(")") > 0) s = s.substring(0, s.indexOf(")"))
					if (s.indexOf(",") > 0) s = s.substring(0, s.indexOf(","))
					
					
					if (tmp.contains(keys.get(s))) {
						// Parameter already seen
					} else if (keys.containsKey(s)) {
					
						tmp << keys.get(s)
					
						writer.print "      * "+keys.get(s)
						if (keys.get(s).equals("path") || keys.get(s).equals("query")) writer.print "*"
						
						if (markers.get(marker).length() > 0) {
							writer.print " ("+markers.get(marker)+")"
						}
						
						if (line.contains("//")) {
							s = line.substring(line.indexOf("//") + 2).trim()
							if (s.startsWith("*")) {
								writer.print "*"
								s = s.substring(1)
							}
							writer.println " : "+s
						} else {
							writer.println ""
						}
						
					} else {
						println "** PARAMETER NOT IN KEYS: $line"
					}
					break
				}
			}
		}
		if (writer.toString().size() > 0) {
			forms.put(name, writer.toString())
			parameters.put(name, tmp)
		}
	}
}

println "Liste complète des paramètres par commande : "
forms.sort()
for (def cmd : forms.keySet().sort()) {
	if (parameters.get(cmd).size() > 0) {
		println "\n  * **$cmd**"
		println "    * Paramètres"
		println forms.get(cmd)
		println "    * API"
		println "      * externe : %%PORTALURL?command="+cmd+"&"+parameters.get(cmd).join("=FILLME&")+"=FILLME%%"
		println "      * interne : %%<a class=\"command\" share command=\"$cmd\" "+parameters.get(cmd).join("=\"FILLME\" ")+"=\"FILLME\">$cmd</a>%%"
	  println """      * dynamique :  %%callTXMCommand(JSON.stringify({"command": "$cmd", """+parameters.get(cmd).join("\": \"FILLME\", ")+"""\": \"FILLME\"}, null, 2));%%"""
	} else {
		println "  * **"+cmd + "**"
		println forms.get(cmd)
		println "    * API"
		println "      * externe : %%PORTALURL?command=$cmd%%"
		println "      * interne : %%<a class=\"command\" share command=\"$cmd\">$cmd</a>%%"
	  println """      * dynamique :  %%callTXMCommand(JSON.stringify({"command": "$cmd"}, null, 2));%%"""
	}
	
}