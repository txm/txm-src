package org.txm.web.server;

import java.io.File;

import org.txm.Toolbox;
import org.txm.utils.logger.Log;
import org.txm.web.server.statics.TxmConfig;
import org.txm.web.server.statics.UserManager;
/**
 * 
 * @author vchabanis
 *
 */
@Deprecated
public class WebCore {

	public static CoreStatus status = CoreStatus.uninitialised;
	public static TxmConfig config = new TxmConfig();
	public static UserManager userManager;// = new UserManager();
	public enum CoreStatus {uninitialised, initialised, processing, maintenance}

	public static void initCore() throws Exception {
		if (status != CoreStatus.initialised) {
			setStatus(CoreStatus.processing);
			Log.fine("[WebCore.initCore] Initialising toolbox with the config file : " + TxmConfig.getProperty("txm.properties"));
			Toolbox.initialize(new File(TxmConfig.getProperty("txm.properties")));
			setStatus(CoreStatus.initialised);
		}
	}

	public static void setStatus(CoreStatus newStatus) {
		status = newStatus;
	}

	public static CoreStatus getStatus() {
		return status;
	}
}
