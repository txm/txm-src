package org.txm.web.server.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.captcha.servlet.SimpleCaptchaServlet;

import org.txm.utils.logger.Log;
import org.txm.web.exceptions.PermissionException;
import org.txm.web.server.TxmUser;
import org.txm.web.server.services.AFCServiceImpl;
import org.txm.web.server.services.AuthenticationServiceImpl;
import org.txm.web.server.services.ConcordanceServiceImpl;
import org.txm.web.server.services.CooccurrenceServiceImpl;
import org.txm.web.server.services.CorpusActionServiceImpl;
import org.txm.web.server.services.EditionServiceImpl;
import org.txm.web.server.services.HelloServiceImpl;
import org.txm.web.server.services.PropertiesServiceImpl;
import org.txm.web.server.services.ReferencerServiceImpl;
import org.txm.web.server.services.TextSelectionServiceImpl;
import org.txm.web.server.services.UIServiceImpl;
import org.txm.web.server.services.VocabularyServiceImpl;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.shared.AFCParams;
import org.txm.web.shared.CommandParameters;
import org.txm.web.shared.ConcordanceParam;
import org.txm.web.shared.CooccurrenceParam;
import org.txm.web.shared.EditionResult;
import org.txm.web.shared.Keys;
import org.txm.web.shared.LoginResult;
import org.txm.web.shared.PropertiesResult;
import org.txm.web.shared.ReferencerParam;
import org.txm.web.shared.ReferencerResult;
import org.txm.web.shared.ServerInfos;
import org.txm.web.shared.TextSelectionResult;
import org.txm.web.shared.VocabularyParam;
import org.txm.web.shared.VocabularyResult;

public class APIServlet extends SimpleCaptchaServlet {

	private static final long serialVersionUID = 6560171562324177699L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String id = req.getSession().getId();

		Map<String, String[]> parameters = req.getParameterMap();
		String cmd = req.getParameter("command");
		if (!Keys.COMMANDS.contains(cmd)) {
			Log.severe("Show help");

			resp.getWriter().print("<p>No command founnd for parameters: "+parameters+"</p>");

			resp.getWriter().print("<p>API documentation... command=login|concordance</p>");
			return;
		}
		System.out.println(cmd);

		HashMap<String, String> parametersHash = new HashMap<>();
		for (String key : parameters.keySet()) {
			parametersHash.put(key, parameters.get(key)[0]);
		}

		TxmUser user = SessionManager.getUser(id);
		if (user == null) {
			
			try {
				HelloServiceImpl s = new HelloServiceImpl();
				s.setSession(req.getSession());
				ServerInfos infos = s.startSession();
				if (!infos.ready) {
					resp.getWriter().print("Error: portal is not ready");
					return;
				}
				user = SessionManager.getUser(id); // the user should be created now
			} catch (Exception e) {
				e.printStackTrace();
				resp.getWriter().print("Error: "+e);
			}
		}
		
		if (user == null) {
			System.out.println("Error: could no start user session");
			resp.getWriter().print("Error: could no start user session");
			return;
		}
		
		try {
			String result = execute(req, cmd, parametersHash, user);
			System.out.println("COMMAND RESULT: "+result);
			if (result == null) {
				resp.getWriter().print(""+parameters+" not implemented.");
			} else {
				resp.getWriter().print(result);
			}
			System.out.println("END");
		} catch(Exception e) {
			resp.getWriter().print("Error: "+e);
			e.printStackTrace();
		}
	}

	public synchronized static String execute(HttpServletRequest req, String cmd, Map<String, String> parameters, TxmUser user) {

		String content = null;

		try {
			// String path, String query, String textid, String editions, String page, String wordids, String properties, String refs, String nLinesPerPage, String contextsSize, String login
			if (parameters.get(Keys.PROPERTIES) != null) {
				parameters.put(Keys.PROPERTIES, Keys.reformat(parameters.get(Keys.PROPERTIES)));
			}
			if (parameters.get(Keys.WORDIDS) != null) {
				parameters.put(Keys.WORDIDS, Keys.reformat(parameters.get(Keys.WORDIDS)));
			}
			if (parameters.get(Keys.REFERENCES) != null) {
				parameters.put(Keys.REFERENCES, Keys.reformat(parameters.get(Keys.REFERENCES)));
			}

			//SC.say("execute: "+cmd+" path: "+path+ " query: "+query+" textid="+textid+" editions="+editions+" page="+page+" wordids="+wordids+" properties="+properties+" refs="+refs);

			cmd = cmd.toLowerCase();

			if (Keys.EDITION.equals(cmd)) { // Textométrie // Ouvre l'édition d'un texte
				content = callEdition(req, parameters);
			} else if (Keys.CONCORDANCE.equals(cmd)) { // Textométrie // Calcule les concordances d'un pivot
				content = callConcordances(req, parameters);
			} else if (Keys.CONTEXT.equals(cmd)) { // Textométrie // Calcule les contextes d'un pivot
				parameters.put(Keys.CONTEXT, "true");
				content = callConcordances(req, parameters);
			} else if (Keys.TEXTS.equals(cmd) || Keys.METADATA.equals(cmd)) { // Textométrie // Affiche la liste des textes d'un corpus
				content = callTexts(req, parameters);
			} else if (Keys.INDEX.equals(cmd)) { // Textométrie // Calcule la liste hiérarchique des mots correspondants à une requête CQL
				content = callIndex(req, parameters);
			} else if (Keys.LEXICON.equals(cmd)) {  // Textométrie // Calcule la liste hiérarchique de tous les mots et leur fréquence
				parameters.put(Keys.QUERY, "[]");
				content = callIndex(req, parameters);
			} else if (Keys.COOCCURRENCE.equals(cmd)) { // Textométrie // Affiche les cooccurrents d'un pivot
				content = callCooccurrences(req, parameters);
			} else if (Keys.SELECTION.equals(cmd)) { // Textométrie // Assistant de construction d'un sous-corpus de textes à partir de leurs métadonnées
				//content = callSelection(parameters);
			} else if (Keys.SUBCORPUS.equals(cmd)) { // Textométrie // Construit un sous-corpus à partir des valeurs d'une propriétés de structure
				//content = callSubCorpus(parameters);
			} else if (Keys.PARTITION.equals(cmd)) { // Textométrie // Construit une partition à partir des valeurs d'une propriétés de structure
				//content = callPartition(parameters);
			} else if (Keys.REFERENCE.equals(cmd)) { // Textométrie // Affiche les références des mots correspondants à une requête
				content = callReferencer(req, parameters);
			} else if (Keys.BIBLIO.equals(cmd) || Keys.RECORD.equals(cmd)) { // Textométrie // Affiche la fiche bibliographique d'un texte
				content = callBiblio(req, parameters);
			} else if (Keys.TIGERSEARCH.equals(cmd)) { // Textométrie // Affiche les phrases syntaxique correspondants à une requête TIGERSearch
				//content = callTS(parameters);
			} else if (Keys.AFC.equals(cmd)) { // Textométrie // Calcule l'analyse factorielle des correspondances d'une partition
				content = callAFC(req, parameters);
			} else if (Keys.SPECIFICITY.equals(cmd)) { // Textométrie // Calcule les mots spécifiques d'une partition
				content = callSpecificities(req, parameters);
			} else if (Keys.PROPERTIES.equals(cmd) || Keys.DESCRIPTION.equals(cmd)) { // Textométrie // Affiche les informations générales d'un corpus
				content = callProperties(req, parameters);
			} else if (Keys.ADMIN.equals(cmd)) { // Administration // Ouvre l'interface d'administration du portail
				//content = callAdmin();
			} else if (Keys.HELP.equals(cmd)) { // Aide // Ouvre l'onglet d'aide du portail
				//content = callHelp();
			} else if (Keys.HOME.equals(cmd)) { // Textométrie // Ouvre la page d'accueil du portail
				//content = callHome();
			} else if (Keys.CONTACT.equals(cmd)) { // Aide // Ouvre l'onglet de contact du portail
				//content = callContact();
			} else if (Keys.LOGIN.equals(cmd)) { // Utilisateur // Ouvre la fenêtre de connexion au portail
				content = callLogin(req, parameters);
			} else if (Keys.LOGOUT.equals(cmd)) { // Utilisateur // Déconnecte l'utilisateur courant du portail
				content = callLogout(req, parameters);
			} else if (Keys.SUBSCRIBE.equals(cmd)) { // Utilisateur // Ouvre la fenêtre d'inscription au portail
				//content = callSignUp();
			} else if (Keys.PROFILE.equals(cmd)) { // Utilisateur // Ouvre l'interface de réglage du profil de l'utilisateur courant
				content = callProfile(req, user);
			} else if (Keys.PAGE.equals(cmd)) { // Textométrie // Ouvre une page .jsp du portail dans un nouvel onglet
				//content = callPage(req, parameters);
			} else if (Keys.DOCUMENTATION.equals(cmd)) { // Textométrie // Ouvre la page de documentation d'un corpus
				content = callDocumentation(req, parameters);
			} else if (Keys.DOWNLOAD.equals(cmd)) { // Textométrie // Télécharge le corpus binaire d'un corpus
				//content = callDownload(parameters);
			} else if (Keys.REMOVE.equals(cmd)) { // Textométrie // Supprimer l'objet sélectionné
				//content = callRemove(parameters);
			} else {
				content = "Error: command no found: "+parameters;
			}
		} catch(Exception e) {
			content = "Error: "+ e.getMessage();
			e.printStackTrace();
		}

		return content;
	}
	
	private static String callProfile(HttpServletRequest req, TxmUser user) throws PermissionException {

		return user.toString();
	}

	private static String callProperties(HttpServletRequest req, Map<String, String> parameters) throws PermissionException {
		PropertiesServiceImpl s = new PropertiesServiceImpl();
		s.setSession(req.getSession());
		PropertiesResult rez = s.getTV(parameters.get(Keys.PATH), null);

		return rez.toString();
	}

	private static String callDocumentation(HttpServletRequest req, Map<String, String> parameters) {
		UIServiceImpl s = new UIServiceImpl();
		s.setSession(req.getSession());

		String infos = s.getUserPage(parameters.get(Keys.PATH), parameters.get(Keys.LOCALE));

		return infos;
	}

	private static String callTexts(HttpServletRequest req, Map<String, String> parameters) {
		TextSelectionServiceImpl s = new TextSelectionServiceImpl();
		s.setSession(req.getSession());

		TextSelectionResult infos = s.getStuffForBiblios(parameters.get(Keys.PATH));
		return infos.toString();

	}

	private static String callAFC(HttpServletRequest req, Map<String, String> parameters) {
		AFCServiceImpl s = new AFCServiceImpl();
		s.setSession(req.getSession());

		AFCParams params = new AFCParams();
		params.setParameters(parameters);
		String infos = s.analysisComputing(params);

		return infos;
	}

	private static String callSpecificities(HttpServletRequest req, Map<String, String> parameters) {
		TextSelectionServiceImpl s = new TextSelectionServiceImpl();
		s.setSession(req.getSession());

		TextSelectionResult infos = s.getStuffForBiblios(parameters.get(Keys.PATH));

		return infos.toString();
	}

	private static String callBiblio(HttpServletRequest req, Map<String, String> parameters) {
		EditionServiceImpl s = new EditionServiceImpl();
		s.setSession(req.getSession());

		return s.getBiblio(Keys.PATH, Keys.TEXTID);
	}

	private static String callIndex(HttpServletRequest req, Map<String, String> parameters) throws PermissionException {
		VocabularyServiceImpl s = new VocabularyServiceImpl();
		s.setSession(req.getSession());
		VocabularyParam params = new VocabularyParam();
		params.setParameters(parameters);

		VocabularyResult rez = s.index(params);
		return rez.toString();
	}

	private static String callReferencer(HttpServletRequest req, Map<String, String> parameters) throws PermissionException {

		ReferencerServiceImpl s = new ReferencerServiceImpl();
		s.setSession(req.getSession());
		ReferencerParam params = new ReferencerParam();
		params.setParameters(parameters);

		ReferencerResult rez = s.references(params);
		return rez.toString();
	}

	private static String callCooccurrences(HttpServletRequest req, Map<String, String> parameters) {

		CooccurrenceServiceImpl s = new CooccurrenceServiceImpl();
		s.setSession(req.getSession());
		CooccurrenceParam params = new CooccurrenceParam();
		params.setParameters(parameters);

		String rez = s.cooccurrence(params);
		if (rez.length() > 0) {
			return rez;
		} else {
			return s.getLines(params).toString();
		}
	}

	private static String callEdition(HttpServletRequest req, Map<String, String> parameters) throws PermissionException {
		EditionServiceImpl s = new EditionServiceImpl();
		s.setSession(req.getSession());

		if (parameters.get(Keys.EDITIONS) == null) parameters.put(Keys.EDITIONS, "default");
		//if (parameters.get(Keys.PAGE) == null) parameters.put(Keys.PAGE,"default");

		ArrayList<EditionResult> rez = s.getEditions(parameters.get(Keys.PATH), parameters.get(Keys.TEXTID), 
				CommandParameters.toStringArrayList(parameters.get(Keys.EDITIONS)), CommandParameters.toStrings(parameters.get(Keys.PAGE)));
		StringBuffer buffer = new StringBuffer();
		for (EditionResult p : rez) {
			buffer.append(p.getContent());
		}

		return buffer.toString();
	}

	private static String callLogin(HttpServletRequest req, Map<String, String> parameters) {

		AuthenticationServiceImpl s = new AuthenticationServiceImpl();
		s.setSession(req.getSession());
		LoginResult result = s.login(parameters.get(Keys.USER), parameters.get(Keys.PASSWORD));
		return ""+result.isSuccess();
	}

	private static String callLogout(HttpServletRequest req, Map<String, String> parameters) {

		AuthenticationServiceImpl s = new AuthenticationServiceImpl();
		s.setSession(req.getSession());
		Boolean result = s.logout();
		return ""+result;
	}

	private static String callConcordances(HttpServletRequest req, Map<String, String> parameters) {
		ConcordanceServiceImpl s = new ConcordanceServiceImpl();
		s.setSession(req.getSession());

		CorpusActionServiceImpl s2 = new CorpusActionServiceImpl();
		s2.setSession(req.getSession());
		ConcordanceParam params = s2.getConcordanceDefaultParameters(parameters.get(Keys.PATH));

		params.setCorpusPath(parameters.get(Keys.PATH));
		params.setQuery(parameters.get(Keys.QUERY));
		ArrayList<String> words = new ArrayList<>(Arrays.asList("word"));
		ArrayList<String> texts = new ArrayList<>(Arrays.asList("text:id"));
		params.setLeftSortProperties(words);
		params.setKeywordSortProperties(words);
		params.setRightSortProperties(words);
		params.setLeftViewProperties(words);
		params.setKeywordViewProperties(words);
		params.setRightViewProperties(words);
		params.setStartIndex(0);
		params.setEndIndex(Integer.MAX_VALUE);
		params.setLeftContextSize(10);
		params.setRightContextSize(10);
		params.setSortReferences(texts);
		params.setViewReferences(texts);

		params.setParameters(parameters);

		String c = s.concordance(params);
		System.out.println("NEW CONCORDANCE: "+c);
		if (c.length() == 0) {
			if ("true".equals(parameters.get(Keys.CONTEXT))) {
				return s.getLines(params).toStringContexts();
			} else {
				return s.getLines(params).toString(); //StringUtils.join(, "\n");
			}
		} else {
			return "Error: "+c;
		}
	}
}
