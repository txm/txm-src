package org.txm.web.server.servlets;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.captcha.servlet.SimpleCaptchaServlet;

import org.txm.web.server.statics.TxmSessionListener;

public class PrintSessionsServlet extends SimpleCaptchaServlet {

	private static final long serialVersionUID = 6560171562324177699L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (TxmSessionListener.sessions == null) {
			System.out.println("Error: no session");
			return;
		}
		for (HttpSession session : TxmSessionListener.sessions) {
			System.out.println("Client IP : "+getClientIpAddr(req));
			System.out.println("session : "+session);
			System.out.println(" ID: "+session.getId());
			System.out.println(" created at: "+session.getCreationTime());
			System.out.println(" servlet: "+session.getServletContext());
			System.out.println("  context_path: "+session.getServletContext().getContextPath());
			System.out.println("  context_name: "+session.getServletContext().getServletContextName());
			System.out.println("  params_name: "+session.getServletContext().getInitParameterNames());
			Enumeration<?> params = session.getServletContext().getInitParameterNames();
			while (params.hasMoreElements()) {
				String o=(String) params.nextElement();
				System.out.println("   param: "+o+" = "+session.getServletContext().getInitParameter(o));
			}
			
			System.out.println("  attributes_name: "+session.getServletContext().getAttributeNames());
			Enumeration<?> attributes = session.getServletContext().getAttributeNames();
			while(attributes.hasMoreElements()) {
				String o=(String) attributes.nextElement();
				System.out.println("   attribute: "+o+" = "+session.getServletContext().getAttribute(o));
			}
		}
	}
	
	public static String getClientIpAddr(HttpServletRequest request) {  
        String ip = request.getHeader("X-Forwarded-For");  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("WL-Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_CLIENT_IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getRemoteAddr();  
        }  
        return ip;  
    }  
}
