package org.txm.web.server.servlets;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSession;

import org.txm.Toolbox;
import org.txm.web.server.services.AuthenticationServiceImpl;
import org.txm.web.server.statics.Corpora;
import org.txm.web.server.statics.Profiles;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.server.statics.TxmSessionListener;
import org.txm.web.server.statics.UserManager;

public class StartStopContextListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		System.out.println("Starting up the TXM portal webapp!");
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		System.out.println("Shutting down the TXM portal webapp!");
		try {
			System.out.println("Closing HttpSession's...");
			for (HttpSession session : TxmSessionListener.sessions) {
				session.invalidate();
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			System.out.println("Stopping TXM session manager...");
			SessionManager.shutdown();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			System.out.println("Stopping TXM profiles...");
			Profiles.stop();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			System.out.println("Stopping TXM users...");
			UserManager.saveAll();
			UserManager.stop();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			System.out.println("Stopping TXM corpora...");
			Corpora.stop();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			System.out.println("Stopping Toolbox...");
			Toolbox.shutdoweb();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			System.out.println("Stopping Portal ...");
			AuthenticationServiceImpl.stop();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}