package org.txm.web.server.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.captcha.servlet.SimpleCaptchaServlet;

import org.txm.utils.logger.Log;
import org.txm.web.server.services.AdminServiceImpl;
import org.txm.web.server.services.HelloServiceImpl;
import org.txm.web.server.statics.TxmConfig;

public class SetMaintenanceServlet extends SimpleCaptchaServlet {
    
    private static final long serialVersionUID = 6560171562324177699L;
     
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
    	
    	if(req.getParameter("state") == null) // mandatory
    	{
    		PrintWriter out = resp.getWriter();
    	    out.print("error_state");
    	    out.close();
    		return;
    	}
    	if(req.getParameter("code") == null) // mandatory
    	{
    		PrintWriter out = resp.getWriter();
    	    out.print("error_code");
    	    out.close();
    		return;
    	}
    	if(req.getParameter("message") == null) // mandatory
    	{
    		PrintWriter out = resp.getWriter();
    	    out.print("error_message");
    	    out.close();
    		return;
    	}
    	String code = req.getParameter("code");
    	boolean state = false;
    	try{
    		state = Boolean.parseBoolean(req.getParameter("state"));
    	}catch (Exception e) {
    		PrintWriter out = resp.getWriter();
    	    out.print("error_code");
    	    out.close();
    		return;
		}
        String message = req.getParameter("message");
        
        if (!code.equals(TxmConfig.getProperty("REMOTECTRLCODE", "remote_ctrl_code_not_set_"+UUID.randomUUID()))) {// test ctrl code
        	PrintWriter out = resp.getWriter();
    	    out.print("error_code");
    	    out.close();
    		return;
        }
        
    	Log.info("[Servlet.SetRemoteMaintenance] set maintenance: "+state);
		//ActivityLog.add(getSessionId(), this.getThreadLocalRequest().getRemoteAddr(),this.getThreadLocalRequest().getRemoteAddr(), "admin", "setMaintenance", state);
		AdminServiceImpl.maintenance = state;
		HelloServiceImpl.updateNews(message);
		
		// send true|false
		PrintWriter out = resp.getWriter();
	    out.print(""+AdminServiceImpl.isMaintenance());
	    out.close();

		return;
    }
}
