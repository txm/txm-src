package org.txm.web.server.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.utils.logger.Log;
import org.txm.web.aas.authorization.permissions.DownloadPermission;
import org.txm.web.server.TxmUser;
import org.txm.web.server.statics.ActivityLog;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.server.statics.TxmConfig;
import org.txm.web.shared.Actions;

public class DownloadAttachmentServlet extends HttpServlet {

	private static final long serialVersionUID = 1455601817408067252L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.doGet(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		HttpSession session = req.getSession();
		String type = (String) req.getParameter("type");
		String itemPath = (String) req.getParameter("path");
		//System.out.println("DOWNLOAD SERVLET path="+itemPath+" type="+type);
		
		TxmUser user = SessionManager.getUser(session.getId());
		if (user == null) {
			Log.severe("Session "+session.getId()+" has expired");
			resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
			OutputStream out = resp.getOutputStream();
			out.write("User permission expired".getBytes());
			out.flush();
			resp.getOutputStream().close();
			return; 
		}

		if (!user.can(itemPath, new DownloadPermission())) {
			Log.severe("Error: no permission");
			ActivityLog.severe(session.getId(), PrintSessionsServlet.getClientIpAddr(req),user.getLogin(), "download.500", itemPath, type);
			resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
			OutputStream out = resp.getOutputStream();
			out.write("No permission".getBytes());
			out.flush();
			resp.getOutputStream().close();
			return; 
		}

		Object obj = user.askEntity(itemPath, Actions.DOWNLOAD);
		if (!(obj instanceof Corpus) || obj == null) {
			Log.severe("Error: no object found: "+obj);
			ActivityLog.severe(session.getId(), PrintSessionsServlet.getClientIpAddr(req),user.getLogin(), "download.404", itemPath, type);
			resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
			OutputStream out = resp.getOutputStream();
			out.write("File object found".getBytes());
			out.flush();
			resp.getOutputStream().close();
			return; 
		}

		Corpus corpus = (Corpus)obj;

		if ("txm".equals(type)) {
			String zip_directory = TxmConfig.getProperty("zip_directory");
			if (zip_directory == null || zip_directory.length() == 0) {
				zip_directory = corpus.getBaseDirectory().getParent();
			}
			File zipDirectory = new File(zip_directory);
			zipDirectory.mkdir();
			
			File zipFile = new File(zipDirectory, corpus.getName().replace("/", "")+".txm");

			if (!zipFile.exists()) {
				Log.severe("Error: binary corpus file not present: "+zipFile);
				ActivityLog.severe(session.getId(), PrintSessionsServlet.getClientIpAddr(req),user.getLogin(), "download.404: "+zipFile, itemPath, type);
				resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
				
				OutputStream out = resp.getOutputStream();
				out.write("File not found".getBytes());
				out.flush();
				resp.getOutputStream().close();
				return; 
			}
			
			if (!zipFile.canRead()) {
				Log.severe("Error: binary corpus file not readable: "+zipFile);
				ActivityLog.severe(session.getId(), PrintSessionsServlet.getClientIpAddr(req),user.getLogin(), "download.404: "+zipFile, itemPath, type);
				resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
				
				OutputStream out = resp.getOutputStream();

				out.write("File not readable".getBytes());
				out.flush();
				
				resp.getOutputStream().close();
				return; 
			}

			resp.setHeader("Content-Disposition", "attachment; filename=\"" + zipFile.getName() + "\"");
			//resp.setHeader("Content-Type", "application/zip");

			OutputStream out = resp.getOutputStream();

			FileInputStream fIn = new FileInputStream(zipFile);
			byte[] buffer = new byte[4096];
			int length;
			while ((length = fIn.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}
			fIn.close();
			out.flush();
		} else {
			Log.severe("wrong download type="+type);
			ActivityLog.severe(session.getId(), PrintSessionsServlet.getClientIpAddr(req),user.getLogin(), "download.500", itemPath, type);
			resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
			resp.getOutputStream().close();
			return; 
		}
	}
}