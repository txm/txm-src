package org.txm.web.server.servlets;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.txm.Toolbox;
import org.txm.doc.Zip;
import org.txm.utils.DeleteDir;
import org.txm.utils.logger.Log;
import org.txm.web.server.TxmUser;
import org.txm.web.server.services.AdminServiceImpl;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.server.statics.UploadFile;

//@WebServlet("/FileUpload/Corpus.html")
public class CorpusUploadServlet extends HttpServlet {

	private static final long serialVersionUID = 7025732237294540112L;

	public void doPost(HttpServletRequest req, HttpServletResponse res) {

		String id = req.getSession().getId();
		TxmUser user = SessionManager.getUser(id);
		if (user == null) {
			Log.severe("Session "+id+" has expired");
			return; 
		}
		if (!AdminServiceImpl.isAdmin(user)) {
			Log.severe("User is not admin.");
			return;
		}

		String txmhome = Toolbox.getParam(Toolbox.USER_TXM_HOME);
		File corporaDirectory = new File(txmhome, "corpora");
		corporaDirectory.mkdir();

		File f = UploadFile.doIt(corporaDirectory, req, res);

		if (f != null) {
			try {
				if (f.getName().endsWith(".zip") || f.getName().endsWith(".txm")) {

					String name = Zip.getRoot(f);
					File dir = new File(corporaDirectory, name);
					if (dir.exists()) {
						DeleteDir.deleteDirectory(dir);
					}
					if (dir.exists()) {
						System.out.println("CORPUSUPLOAD: warning could not remove previous directory "+dir);
					}

					Zip.decompress(f);

					if (dir.exists()) {
						
						System.out.println("UNZIP: success");
						if (AdminServiceImpl._loadBinaryCorpus(user, dir.getAbsolutePath())) {
							System.out.println("CORPUSUPLOAD: success.");
						} else {
							System.out.println("CORPUSUPLOAD: fail.");
						}
					}

				} else {
					System.out.println("CORPUSUPLOAD: fail.");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}