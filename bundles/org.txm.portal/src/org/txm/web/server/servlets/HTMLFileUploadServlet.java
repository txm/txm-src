package org.txm.web.server.servlets;

import java.io.File;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.txm.utils.logger.Log;
import org.txm.web.server.TxmUser;
import org.txm.web.server.services.AdminServiceImpl;
import org.txm.web.server.statics.SessionManager;
import org.txm.web.server.statics.UploadFile;

public class HTMLFileUploadServlet extends HttpServlet {

	private static final long serialVersionUID = 7025732237294540112L;

	public void doPost(HttpServletRequest req, HttpServletResponse res) {

		String id = req.getSession().getId();
		TxmUser user = SessionManager.getUser(id);
		if (user == null) {
			Log.severe("Session "+id+" has expired");
			return; 
		}
		if (!AdminServiceImpl.isAdmin(user)) {
			Log.severe("User is not admin.");
			return;
		}
		
		//String txmhome = Toolbox.getParam(Toolbox.USER_TXM_HOME);
		File htmlDirectory = new File(".");
		htmlDirectory.mkdir();
		
		UploadFile.doIt(htmlDirectory, req, res);
	}
}