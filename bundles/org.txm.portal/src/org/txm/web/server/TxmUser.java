package org.txm.web.server;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.security.Permission;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.txm.importer.DomUtils;
import org.txm.utils.logger.Log;
import org.txm.web.aas.authentication.TxmLoginModule;
import org.txm.web.aas.authorization.ActionFactory;
import org.txm.web.aas.authorization.Profile;
import org.txm.web.aas.authorization.TxmPermission;
import org.txm.web.aas.authorization.permissions.ConcordancePermission;
import org.txm.web.aas.authorization.permissions.TxmAllPermission;
import org.txm.web.server.statics.Corpora;
import org.txm.web.server.statics.Profiles;
import org.txm.web.server.statics.TxmConfig;
import org.txm.web.shared.Actions;
import org.txm.web.shared.UserData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

/**
 * It's the representation of a user. It contains all the informations about the user.
 * and its personnal permissions
 * 
 * @author vchabani, mdecorde
 * @version 0.2.0
 */
public class TxmUser implements Serializable {

	private static final long serialVersionUID = 6253834775536332126L; // the serial number to load .usr files
	//private static final long serialVersionUID = -7803956843356083826L;

	private HashMap<String, HashSet<TxmPermission>> corporaPermissions = new HashMap<String, HashSet<TxmPermission>>();
	//TODO: add profilesPermissions, bug: permissions stay if a profile is removed

	private List<String> profiles = Collections.synchronizedList(new ArrayList<String>());

	protected File xmlFile;
	protected Document doc; 

	private String login = "guest";
	private String mail = "";
	private String firstname = "";
	private String lastname = "";
	private String status = "";
	private String phone = "";
	private String institution = "";
	private String md5 = "";
	private boolean isLogged = false;

	static SimpleDateFormat format = new java.text.SimpleDateFormat();
	private Date lastConnexion = null;

	/**
	 * create an empty user and gives him the anonymous profile
	 */
	public TxmUser() {
		profiles.add("Anonymous");
	}

	public boolean load()
	{
		if(doc == null || xmlFile == null)
			return false;
		Element userelem = doc.getDocumentElement();

		return load(userelem);
	}

	public boolean setPassword(String password)
	{
		md5 = TxmLoginModule.md5(password);
		return this.save();
	}

	public boolean setMD5(String md5)
	{
		this.md5 = md5;
		return this.save();
	}

	public boolean update()
	{
		if (doc == null || xmlFile == null) {
			return false;
		}
		
		Element root = doc.getDocumentElement();
		root.setAttribute("login", login);
		root.setAttribute("mail", mail);
		root.setAttribute("md5", md5);
		root.setAttribute("lastname", lastname);
		root.setAttribute("firstname", firstname);
		root.setAttribute("status", status);
		root.setAttribute("phone", phone);
		root.setAttribute("institution", institution);
		if (lastConnexion != null) {
			root.setAttribute("lastconnexion", format.format(lastConnexion));
		}
		
		NodeList profilesList = root.getElementsByTagName("profiles");
		for(int i = 0 ; i < profilesList.getLength() ; i++) { // only 1
			root.removeChild(profilesList.item(i));
		}
		Element profiles = doc.createElement("profiles");
		root.appendChild(profiles);

		if (getProfiles() != null) {
			for (String profilename : getProfiles()) {
				Element profile = doc.createElement("profile");
				profile.setAttribute("name", profilename);
				profiles.appendChild(profile);
			}
		}

		NodeList permissionsList = root.getElementsByTagName("permissions");
		for(int i = 0 ; i < permissionsList.getLength() ; i++) { // only 1
			root.removeChild(permissionsList.item(i));
		}
		Element permissions = doc.createElement("permissions");
		root.appendChild(permissions);

		if (getCorporaPermissions() != null) {
			for (String entity : getCorporaPermissions().keySet()) {
				Element entityElem = doc.createElement("entity");
				permissions.appendChild(entityElem);
				entityElem.setAttribute("path", entity);

				for (Permission perm : getCorporaPermissions().get(entity)) {
					Element permElem = doc.createElement("permission");
					entityElem.appendChild(permElem);
					if (perm != null) {
						if (perm instanceof TxmPermission) {
							((TxmPermission)perm).toXml(permElem);
						}
					}
				}
			}
		}
		return true;
	}

	public boolean save()
	{
		if (xmlFile == null) {
			try {
				xmlFile = File.createTempFile("user", ".xml", new File(TxmConfig.getProperty("userBase")));
				if(xmlFile == null) { // 2nd try
					xmlFile = File.createTempFile("user", ".xml", new File(TxmConfig.getProperty("userBase")));
				}
			} catch (IOException e) {
				Log.severe("Error while saving user: "+e);
				e.printStackTrace();
				return false;
			}
		}

		if (doc == null) {
			try {
				DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				doc = builder.newDocument();
				doc.appendChild(doc.createElement("user"));
			} catch (ParserConfigurationException e) {
				Log.severe("Error while saving user: "+e);
				e.printStackTrace();
				return false;
			}
		}
		this.update();
		try {
			return DomUtils.save(doc, xmlFile);
		} catch (Exception e) {
			Log.severe(Log.toString(e));
		}
		return false;
	}

	/**
	 * Create a user with the informations of the data
	 * @param data
	 */
	public TxmUser(UserData data) {
		this();
		login = (data.login);
		this.setNames(data.firstname, data.lastname);
		mail = (data.mail);
		phone = (data.phone);
		institution = (data.institution);
		status = (data.status);
	}

	/**
	 * Checks if the current user is authenticated.
	 * @return True if the user is authenticated, false otherwise.
	 */
	public synchronized boolean isLogged() {
		return isLogged;
	}

	public boolean isGuest()
	{
		return login == null || login.equals("guest");
	}

	/**
	 * Sets if the current user is authenticated.
	 * @param isLogged The user's status.
	 */
	public synchronized void setIsLogged(boolean isLogged) {
		this.isLogged = isLogged;
		if (this.isLogged) {
			this.save();
		}
	}

	/**
	 * Checks if the current user is administrator, it allows to the UI to display the "Admin"
	 * button or not.
	 * @return True if the user is administrator, false otherwise.
	 */
	public synchronized boolean isAdmin() {
		return "admin".equals(login);
	}

	/**
	 * Sets the name of the user.
	 * @param name
	 */
	public synchronized void setLogin(String login) {
		this.login = login;
		if (this.isLogged) {
			this.save();
		}
	}

	/**
	 * @return the name of the user.
	 */
	public synchronized String getLogin() {
		return login;
	}
	/**
	 * 
	 * 	@return
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		if (status != null) {
			this.status = status;
		}
	}

	/**
	 * 
	 * @return
	 */
	public String getPhone() {
		return phone;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		if (institution != null)
			this.institution = institution;
	}

	public void setPhone(String phone) {
		if (phone != null)
			this.phone = phone;
	}

	public void setProfiles(List<String> profiles) {
		this.profiles = profiles;
	}

	/**
	 * @return the firstname and lastname formated as firstname, lastname
	 * If they are not defined it returns the login
	 */
	public synchronized String getFormatedName() {
		if (firstname != null && lastname != null)
			if (!firstname.equals("") || !lastname.equals(""))
				return firstname+" "+lastname;
		return login;
	}

	/**
	 * @return the list of the user's item in its permissions.
	 */
	public synchronized List<String> getCorporaList() {
		ArrayList<String> result = new ArrayList<String>();
		//add its personal permissions
		result.addAll(corporaPermissions.keySet());
		Log.info("User perm: "+result);
		//add profiles permissions
		for (String profile : profiles) {
			if (Profiles.getProfile(profile) != null) {
				Log.info("Profile perm: "+Profiles.getProfile(profile).keySet());
				for (String corpus : Profiles.getProfile(profile).keySet()) {
					if (!result.contains(corpus)) {
						result.add(corpus);
					}
				}
			}
		}
		return result;
	}

	/**
	 * Checks if the user has enough permission to do the action on the entity.
	 * 
	 * @param key The absolute path to the wanted entity.
	 * @param action The action that the user want to do on this object. 
	 * @return The asked object if the user's permissions are sufficient, null otherwise.
	 * @see Actions
	 */
	public synchronized Object askEntity(String key, Actions action) {
		Permission actionPermission = ActionFactory.create(action);
		return askEntity(key, actionPermission);
	}	

	/**
	 * Checks if the user has enough permission with the action's permission on the entity.
	 * 
	 * @param key The absolute path to the wanted entity.
	 * @param action The action that the user want to do on this object. 
	 * @return The asked object if the user's permissions are sufficient, null otherwise.
	 * @see Actions
	 */
	public synchronized Object askEntity(String key, Permission action) {
		Log.fine("[TxmUser.askEntity] User : " + this.login + ", asking : " + key + " to do : " + action);
		if (this.login.equals("admin") || this.can(key, action)) {
			return Corpora.get(key);
		}
		return null;
	}

	/**
	 * Checks if the user has enough permission to do an action on a CqpObject.
	 * @param key The absolute path to the wanted CqpObject (ex: /THECORPUS/subcorpus).
	 * @param action The action that the user want to do on this object. 
	 * @return True if the user's permissions are sufficient, false otherwise.
	 * @see Actions
	 */
	public synchronized boolean can(String key, Permission action) {
		Log.fine("[TxmUser.can] User : " + this.login + ", asking : " + key + " to do : " + action);
		if (key == null) return false;
		if(isAdmin()) return true;

		HashSet<TxmPermission> myPermissions = getPermissions(key); // get the permission for the key
		if (myPermissions != null) {
			for (TxmPermission perm : myPermissions) {
				if (perm.implies(action)) {
					return true;
				}
			}
		}
		for (String profileName : profiles) {
			if (getProfile(profileName).implies(key, action)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gives the wanted object without permission check.
	 * @param key The absolute path to the wanted object.
	 * @return The wanted object.
	 */
	public synchronized Object getEntity(String key) {
		Log.fine("[TxmUser.getEntity] User : " + this.login + ", asking : " + key);
		return Corpora.get(key);
	}

	/**
	 * Updates the list of the user's corpora, if an item was removed by another user, it will
	 * also be removed for the current user.
	 */
	public synchronized void updateCorpora() {

	}

	/**
	 * Gives all permissions to the current user.
	 */
	public synchronized void setAllPermission() {
		Log.fine("[TxmUser.setAllPermission] User : " + this.login);
		HashSet<TxmPermission> permissions = new HashSet<TxmPermission>();
		permissions.add(new TxmAllPermission());
		for (String txmEntity : Corpora.getCorporaList()) {
			corporaPermissions.put(txmEntity, permissions);
		}
		if (this.isLogged) {
			this.save();
		}
	}

	/**
	 * Adds a permission on an object to the current user.
	 * @param entityPath The absolute path to the object.
	 * @param permission The permission to give to the user.
	 * @return True if the permission was correctly added, false otherwise.
	 */
	public synchronized boolean addPermission(String entityPath, TxmPermission permission) {
		HashSet<TxmPermission> entity = corporaPermissions.get(entityPath);
		if (entity == null) { // register the entity if not
			corporaPermissions.put(entityPath, new HashSet<TxmPermission>());
		}

		boolean result = corporaPermissions.get(entityPath).add(permission);
		//Log.fine("[TxmUser.addPermission] User : " + this.login + " have now : " + permission);
		return result;
	}

	/**
	 * Gives the list of the permission owned by the current user and its profile on the specif ied object.
	 * @param entityPath The absolute path to the object.
	 * @return The list of the permission owned by the current user.
	 */
	public synchronized HashSet<TxmPermission> getPermissions(String entityPath) {
		HashSet<TxmPermission> result = new HashSet<TxmPermission>();
		HashSet<TxmPermission> tmp = corporaPermissions.get(entityPath);
		if (tmp != null) {
			result.addAll(tmp);
		}

		for (String profileName : profiles) {
			Profile profile = Profiles.getProfile(profileName);
			tmp = profile.getPermissions(entityPath);

			if (tmp != null) {
				result.addAll(tmp);
			}
		}
		return result;
	}

	/**
	 * Copy all the permissions of the parent object to the children object.
	 * @param childPath The absolute path to the child object.
	 * @param newPartition 
	 * @return True.
	 */
	public synchronized boolean inheritPermissions(String childPath, Object item) {
		String parent = childPath.substring(0, childPath.lastIndexOf("/"));

		HashSet<TxmPermission> newPermissions = corporaPermissions.get(parent);
		if (newPermissions == null)
			newPermissions = new HashSet<TxmPermission>();
		else
			newPermissions = new HashSet<TxmPermission>(newPermissions); // copy

		for (String profileName : profiles) {
			HashSet<TxmPermission> permsFromProfile = getProfile(profileName).getPermissions(parent);
			if (permsFromProfile != null) {
				//				for (Permission p : permsFromProfile) {
				//					if (p instanceof ConcordancePermission) {
				//						for (Permission concPerm : newPermissions) {
				//							if (concPerm instanceof ConcordancePermission && !concPerm.implies(p))
				//								newPermissions.add(p);
				//						}
				//					}
				//					else if (!permsFromProfile.contains(p))
				//						newPermissions.add(p);
				//				}
				newPermissions.addAll(permsFromProfile);
			}
		}

		ActionFactory.removeNonConsistentPermissions(newPermissions, item);
		corporaPermissions.put(childPath, newPermissions);
		return true;
	}

	/**
	 * Returns the asked profile if the user have this one. Returns an empty profile otherwise.
	 * @param profileName The name of the profile.
	 * @return
	 */
	public synchronized Profile getProfile(String profileName) {
		if (profiles.contains(profileName))
			return Profiles.getProfile(profileName);
		else
			return new Profile();
	}

	/**
	 * Returns the default profile.
	 * @param profileName The name of the profile.
	 * @return
	 */
	public synchronized Profile getProfile() {
		if (profiles.size() == 0) { // error, set default profile
			profiles.add(Profiles.getAnonymousProfile().getName());
		}
		return getProfile(profiles.get(0));
	}

	public synchronized boolean addProfile(String newProfile) {
		return profiles.add(newProfile);
	}

	public boolean setProfiles(String... newProfiles) {
		profiles.clear();
		for (String profile : newProfiles) {
			profiles.add(profile);
		}
		return true;
	}

	public synchronized boolean setProfile(String newProfile) {
		profiles.clear();
		profiles.add(newProfile);
		return true;
	}

	public boolean removeProfile(String profileName) {
		return profiles.remove(profileName);
	}

	public synchronized List<String> getProfiles() {
		return profiles;
	}

	public synchronized ConcordancePermission getConcordancePermissionForPath(String path) {
		HashSet<TxmPermission> list = corporaPermissions.get(path);
		if (list != null) {
			for (Permission p : list) 
				if (p instanceof ConcordancePermission) {
					return (ConcordancePermission) p;
				}
		}
		for (String profileName : profiles) {
			Profile profile = Profiles.getProfile(profileName);
			list = profile.get(path);
			if (list != null) {
				for (Permission p : list) {
					if (p instanceof ConcordancePermission) {
						return (ConcordancePermission) p;
					}
				}
			}
		}
		return null;
	}

	public synchronized Map<String, HashSet<TxmPermission>> getCorporaPermissions() {
		return corporaPermissions;
	}

	public synchronized void removeEntityFromUser(String entityPath) {
		corporaPermissions.remove(entityPath);
	}

	/**
	 * @return A string representation of the current user.
	 */
	public synchronized String toString() {
		String result = "[\nUser : " + login + " Profile: "+this.profiles+"\n";
		result += "mail: "+mail+" firstname: "+firstname+" lastname: "+lastname+" status: "+status+" phone: "+phone+" institution: "+institution+" lastConnexion: "+lastConnexion;
		for (String entry : corporaPermissions.keySet()) {
			result += "\t" + entry + " -> ";
			for (Permission p : corporaPermissions.get(entry)) {
				result += p + ", ";
			}
			result += "\n";
		}
		result += "]\n";
		return result;
	}

	public synchronized void setNames(String firstname, String lastname) {
		this.firstname = firstname;
		this.lastname = lastname;
	}

	public synchronized void setMail(String mail)
	{
		this.mail = mail;
	}

	public synchronized String getMail(){
		return mail;
	}

	public synchronized String getFirstName() {
		return firstname;
	}

	public synchronized String getLastName() {
		return lastname;
	}

	public synchronized void setFirstName(String string) {
		this.firstname = string;
	}

	public synchronized void setLastName(String string) {
		this.lastname = string;
	}

	public synchronized void removeAllProfiles() {
		this.profiles.clear();
	}

	public synchronized void removeAllPermissions() {
		this.corporaPermissions.clear();
	}

	public String getMD5Password() {
		return md5;
	}

	/**
	 * @return the lastConnexion
	 */
	public Date getLastConnexion() {
		return lastConnexion;
	}

	/**
	 * @param lastConnexion the lastConnexion to set
	 */
	public void setLastConnexion(Date lastConnexion) {
		this.lastConnexion = lastConnexion;
	}

	public boolean load(File xmlFile) {
		this.xmlFile = xmlFile;
		if (xmlFile.isFile() && xmlFile.canWrite()) {
			try {
				doc = DomUtils.load(xmlFile);
			} catch (Exception e) {
				Log.severe(Log.toString(e));
				return false;
			}
			return load();
		} else {
			return false;
		}
	}

	public boolean delete() {
		if(xmlFile != null)
		{
			xmlFile.delete();
			return !xmlFile.exists();
		}
		return false;
	}

	public boolean testPassword(String password) {
		return md5.equals(TxmLoginModule.md5(password));
	}

	public boolean load(Element userelem) {
		firstname = userelem.getAttribute("firstname");
		lastname = userelem.getAttribute("lastname");
		login = userelem.getAttribute("login");
		mail = userelem.getAttribute("mail");
		institution = userelem.getAttribute("institution");
		phone = userelem.getAttribute("phone");
		status = userelem.getAttribute("status");
		md5 = userelem.getAttribute("md5");
		try {
			String sd = userelem.getAttribute("lastconnexion");
			if (sd != null && sd.length() > 0) { // prevent null from old User XML file
				lastConnexion = format.parse(sd);
			}
		} catch (ParseException e) {
			Log.warning("Failed to parse 'lastconnexion' time for User: "+login+". Reason: "+e);
			//e.printStackTrace();
		}

		if (md5 == null) {
			String password = userelem.getAttribute("password");
			if (password == null) {
				Log.severe("Failed to load user: no password");
				return false;
			}
			md5 = TxmLoginModule.md5(password);
		}

		if (login == null) {
			Log.severe("Failed to load user: no login");
			return false;
		}
		if (md5 == null) {
			Log.severe("Failed to load user: no md5");
			return false;
		}
		if (mail == null) {
			Log.severe("Failed to load user: no mail");
			return false;
		}

		//set profiles
		removeAllProfiles(); // default user has Anonymous profile
		NodeList profileList = userelem.getElementsByTagName("profile");// get all entity elements
		for (int iProfile = 0 ; iProfile < profileList.getLength() ; iProfile++) {
			Element profile = (Element) profileList.item(iProfile);
			if (!getProfiles().contains(profile.getAttribute("name"))) {
				addProfile(profile.getAttribute("name"));
			}
		}

		//set entities
		NodeList entityList = userelem.getElementsByTagName("entity");// get all entity elements
		//System.out.println("NB entities: "+entityList.getLength());
		removeAllPermissions(); // clear permissions hash
		for (int iEntity = 0 ; iEntity < entityList.getLength() ; iEntity++) {
			Element entity = (Element) entityList.item(iEntity);
			String path = entity.getAttribute("path");
			//System.out.println("entity: "+path);
			if (corporaPermissions.containsKey(path)) { // the path may be already containing TxmAllPermission
				corporaPermissions.remove(path); // since the entity is here, it's declaring its own permissions
			}
			//get permissions
			NodeList permissionList = entity.getElementsByTagName("permission");// get all entity elements
			for (int i = 0 ; i < permissionList.getLength() ; i++) {
				Element permission = (Element) permissionList.item(i);
				NamedNodeMap attributes = permission.getAttributes();
				TxmPermission p = TxmPermission.permissionFactory(attributes);
				if (p != null) {
					addPermission(path, p);
				} else {
					Log.severe("Error: can't add permission declared in XML file for user '"+this.getLogin()+"'");
				}
			}
		}

		return true;			
	}
}
