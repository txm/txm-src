<?xml version="1.0" encoding="UTF-8"?>
<jsp:directive.page contentType="text/html;charset=UTF-8" />
<html>
<head>
	<meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta name="gwt:property" content="locale=fr_FR">
	
	<!-- Scripts to include Simple_Viewer library-->
	<script type="text/javascript" src="js/viewer/Simple_Viewer_beta_1.1-min.js"></script>
	<script type="text/javascript" src="js/viewer/toolbar-ext.js"></script>
	<link rel="stylesheet" type="text/css" href="js/viewer/toolbar-ext.css" />
	<!-- sample code to add to 'img' tags to use the Simple_Viewer
	<img src="imagepath" style="width:100%;height:100%" onLoad="viewer.toolbarImages='images';viewer.onload=viewer.toolbar;new viewer({
	        image: this, frame: ['100%','100%']});" />
	-->
	
	<!-- GWT related CSS-->
    <link id="portal-ria-style" type="text/css" rel="stylesheet" href="TXM_RIA.css"/>
    <!-- TXM related CSS-->
    <link id="portal-style" type="text/css" rel="stylesheet" href="css/TXM WEB.css"/>
    <!-- Portal favicon-->
	<link id="dynamic-favicon" rel="shortcut icon" href="images/icons/TXMlogo.png" type="image/x-icon" />
    
    <!-- Portal tab name-->
    <title>TXM</title>
</head>

  <body>
  	<!-- Splash screen content that is hidden when the TXM portal is ready-->
    <div id="SplashScreen">
  	<h4 id="bienvenue">Bienvenue sur le portail TXM</h4>
  	<p>
  		Veuillez attendre la fin du chargement...
  	</p>
  	<p>
  		<img id="spinningImgSplashscreen" src="images/spinner.gif"/>
  	</p>
  	
  	Si cet écran s'affiche pendant plus de 2 minutes, cliquez sur le bouton "Actualiser" de votre navigateur.<br/>
Si le message "La session n'a pu être démarrée" s'affiche, appuyez simultanément sur les touches Ctrl et F5 pour actualiser la page et purger le cache du navigateur.<br/>
Si le probl&egrave;me persiste, veuillez contacter l'administrateur du portail.<br/> 

	</div>
	
    <!-- OPTIONAL: include this if you want history support : not functional yet in TXM-->
    <iframe src="javascript:''" id="__gwt_historyFrame" tabIndex='-1' style="position:absolute;width:0;height:0;border:0"></iframe>
   
    <!-- RECOMMENDED if your web app will not function without JavaScript enabled -->
    <noscript>
      <div style="width: 22em; position: absolute; left: 50%; margin-left: -11em; color: red; background-color: white; border: 1px solid red; padding: 4px; font-family: sans-serif">
        <p>
        	Your web browser must have JavaScript enabled in order for this portal to run.
        </p>
        <p>
        	Vous devez activer Javascript dans votre navigateur pour que ce portail puisse fonctionner.
        </p>
      </div>
    </noscript>
    
    <script type="text/javascript">
function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

// Determine what skin file to load
var currentSkin = readCookie('skin_name_2_4');
if (currentSkin == null) currentSkin = "BlackOps";
</script>

<!--load skin-->
<script type="text/javascript">document.getElementById('loadingMsg').innerHTML('Loading skin...');</script>

<script type="text/javascript">
document.write("<"+"script src=txmweb/sc/skins/" + currentSkin + "/load_skin.js?isc_version=9.1.js><"+"/script>");
</script>
    
    
    <!-- This script loads your compiled module.   -->
    <!-- If you add any GWT meta tags, they must   -->
    <!-- be added before this line.                -->
    <!-- I've put this code here to allow the splash display first-->
    <script type="text/javascript" language="javascript" src="txmweb/txmweb.nocache.js"></script>
  </body>
</html>