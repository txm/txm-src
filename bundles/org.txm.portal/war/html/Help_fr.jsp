<jsp:directive.page contentType="text/html;charset=UTF-8" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<title>Aide &amp; Liens</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="css/TXM WEB.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="css/TXM WEB PRINT.css" rel="stylesheet" type="text/css" media="print" />
	<meta content="Serge Heiden" name="author" />
</head>
<body>
<div class="textzone">
<h1 style="font-family: Helvetica,Arial,sans-serif;">Aide &amp; Liens</h1>

Il n'y a pas encore de manuel pour TXM GWT. <br />
Néanmoins, l'interface est assez proche de celle de TXM RCP pour vous permettre d'utiliser son manuel.<br />

<ul>
  <li><a target="HelpTab" href="http://textometrie.sourceforge.net/doc/refman/ManueldeReferenceTXM0.5_FR.xhtml">Manuel de référence de TXM 0.5</a> (En ligne)</li><br />
  <ul>
    <li><a target="HelpTab" href="http://textometrie.sourceforge.net/doc/refman/ManueldeReferenceTXM0.5_FR17.xhtml#toc52">Créer un sous-corpus</a></li>
    <li><a target="HelpTab" href="http://textometrie.sourceforge.net/doc/refman/ManueldeReferenceTXM0.5_FR18.xhtml#toc56">Créer une partition</a></li>
    <li>Sélection de texte : nouvelle interface de création de sous-corpus (pas encore documentée)</li><br />
    <li>Propriétés : retourne la taille du corpus : le nombre de mots et le nombres de formes différentes</li>
    <li>References : liste les références bibliographiques des matches d'une requête CQL</li>
    <li><a target="HelpTab" href="http://textometrie.sourceforge.net/doc/refman/ManueldeReferenceTXM0.5_FR21.xhtml#toc71">Index</a></li>
    <li><a target="HelpTab" href="http://textometrie.sourceforge.net/doc/refman/ManueldeReferenceTXM0.5_FR21.xhtml#toc70">Lexique</a></li>
    <li><a target="HelpTab" href="http://textometrie.sourceforge.net/doc/refman/ManueldeReferenceTXM0.5_FR19.xhtml#toc60">Concordance</a></li>
    <li>Contextes : comme la commande 'Concordances' mais sans alignement vertical des pivots</li>
    <li>Tiger Search : Outil de requête syntaxiques, disponible seulement si les annotations syntaxiques ont été chargées avec le corpus</li><br />
    <li><a target="HelpTab" href="http://textometrie.sourceforge.net/doc/refman/ManueldeReferenceTXM0.5_FR16.xhtml#toc49">Edition des textes</a></li>
    <li>Fiches bibliographiques : disponible seulement si le corpus a été chargé avec les paramètres bibliographiques</li>
    <li>Suprimer : pour supprimer un sous-corpus ou une partition</li>
  </ul>
<br />
  <li><a target="HelpTab" href="http://textometrie.sourceforge.net/doc/refman/ManueldeReferenceTXM0.5_EN.xhtml">TXM 0.5 Reference Manual</a> (Online)</li>
  <li><a target="HelpTab" href="http://sourceforge.net/projects/textometrie/files/documentation/TXM%20Reference%20Manual%200.5_EN.pdf/download" title="Click to download TXM Reference Manual 0.5_EN.pdf">TXM Reference Manual 0.5</a> (PDF for printing)
  <li><a target="HelpTab" href="http://sourceforge.net/projects/textometrie/files/documentation/Manuel%20de%20Reference%20TXM%200.5_FR.pdf/download">Manuel de référence TXM 0.5</a> (PDF pour impression)</li>
<br />
  <li><a target="HelpTab" href="https://sourceforge.net/projects/textometrie/files/documentation">Autres manuels</a></li>
  <li><a target="HelpTab" href="http://textometrie.ens-lyon.fr/spip.php?article96">Documentation du projet Textométrie</a></li>
  <li><a target="HelpTab" href="http://textometrie.ens-lsh.fr/IMG/html/intro-discours.htm">Tutoriel vidéo introductif</a> (TXM RCP version 0.4.6)</li>
  <li><a target="HelpTab" href="https://listes.cru.fr/wiki/txm-users">Wiki des utilisateurs de TXM</a></li>
  <li>Liste de diffusion francophone&nbsp;: <a target="HelpTab" href="https://listes.cru.fr/sympa/subscribe/txm-users">txm-users AT cru.fr</a></li>
  <li>International mailing list : <a target="HelpTab" href="http://lists.sourceforge.net/mailman/listinfo/textometrie-open">textometrie-open AT lists.sourceforge.net</a></li>
  </li>
<br />
  <li><a target="HelpTab" href="http://sourceforge.net/apps/mediawiki/textometrie">Wiki des développeurs de TXM</a></li>
  <li><a target="HelpTab" href="http://textometrie.sourceforge.net/javadoc/index.html">Javadoc des sources de TXM</a>  </li>
<br />
  <li><a target="HelpTab" href="http://textometrie.ens-lyon.fr/spip.php?article62">Publications du projet Textométrie</a></li>
</ul>

</div>
</body></html>
