<jsp:directive.page	contentType="text/html;charset=UTF-8" />
<div class="textzone">
<h2>RECETTE</h2>

		<p>Interface<br>
		<br>
		<a class="command" share command="login" login="admin">login as "admin"</a></br>
		<a class="command" share command="subscribe" login="admin">SUBSCRIBE</a></br>
		<a class="command" share command="profile" login="admin">PROFILE</a></br>
		<a class="command" share command="contact">CONTACT</a></br>
		<a class="command" share command="logout">LOGOUT</a></br>
		</br>
		<a class="command" share command="home">HOME</a></br>
		<a class="command" share command="help">HELP</a></br>
		<a class="command" share command="documentation" path="VOEUX">Documentation de VOEUX</a></br>
		<a class="command" share command="page" title="Page CMS de test" path="/VOEUX/CMS/test">PAGE</a></br>
		</p>
		
		<p>Commands<br>
		</br>
		<a class="command" share command="texts" path="/VOEUX" query="souhait.*">textes.*</a></br>
			<a class="command" share command="edition" path="/GRAAL" editions="facs,dipl" >edition GRAAL facs et dipl</a></br>
			<a class="command" share command="edition" path="/VOEUX" textid="0009" >edition VOEUX/0009</a></br>
			<a class="command" share command="edition" path="/VOEUX" textid="0009" pageid="2">edition VOEUX/0009/2</a></br>
			<a class="command" share command="edition" path="/VOEUX" textid="0009" wordids="w_0009_20">edition VOEUX/0009 focus "là"</a></br>
			<a class="command" share command="concordance" key1="REFERENCE" path="/VOEUX" query="souhait.*" contexts="5" properties="word,frlemma">concordance souhait. contexts=10 properties word/frlemma*</a></br>
			<a class="command" share command="context" path="/VOEUX" query="souhait.*">context souhait.*</a></br>
			<a class="command" share command="cooccurrence" path="/VOEUX" query="souhait.*">cooccurrence souhait.*</a></br>
			<a class="command" share command="reference" path="/VOEUX" query="souhait.*">reference souhait.*</a></br>
			<a class="command" share command="index" path="/VOEUX" query="souhait.*">index souhait.*</a></br>
		</p>
		
		<p>Subcorpus<br>
		</br>
			<a class="command" share command="subcorpus" path="/VOEUX" structure="text" property="id" value="0001" name="0001">create subcorpus 0001</a></br>
			<a class="command" share command="index" path="/VOEUX/guest:0001" query="souhait.*">index subcorpus 0001</a></br>
			<a class="command" share command="specificity" path="/VOEUX/guest:0001">specificity subcorpus 0001</a></br>
			<a class="command" share command="concordance" path="/VOEUX/guest:0001" query="souhait.*">concordance subcorpus 0001</a></br>
			<a class="command" share command="remove" path="/VOEUX/guest:0001">remove subcorpus 0001</a></br>
		</p>
		
		<p>Partition<br>
		</br>
			<a class="command" share command="partition" path="/VOEUX" structure="text" property="id" value="0001" name="texts">create partition texts</a></br>
			<a class="command" share command="index" path="/VOEUX/guest:texts" query="souhait.*">index partition texts</a></br>
			<a class="command" share command="afc" path="/VOEUX/guest:texts" >afc partition texts</a></br>
			<a class="command" share command="specificity" path="/VOEUX/guest:texts" >specificity partition texts</a></br>
			<a class="command" share command="remove" path="/VOEUX/guest:texts" >remove partition texts</a></br>
			</p>
			</div>