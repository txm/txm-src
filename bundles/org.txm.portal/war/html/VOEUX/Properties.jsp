<jsp:directive.page
        contentType="text/html;charset=UTF-8" />
<? version="1.0" encoding="UTF-8"?>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/></head><body>
<h2 style'font-family:"Arial";'>Propriétés de VOEUX (CQP ID=VOEUX)</h2>
<p><h3>Description</h3><pre><br/>mdecorde<br/>29 mai 2019, 12h15</pre></p><h3 style'font-family:"Arial";'>Statistiques Générales</h3>
<ul>
<li>Nombre de mots : 61 197</li>
<li>Nombre de propriétés de mot 7 (word, lbn, pn, sn, n, frpos, frlemma)</li>
<li>Nombre d'unités de structure 4 (lb, p, s, text)</li>
</ul>
<h3 style'font-family:"Arial";'>Propriétés des unités lexicales (max 100 valeurs)</h3>
<ul>
<li> frlemma : pour, le, métropole, français, ,, Algérie, communauté, je, former, du, vœu, ardent, et, confiant, au, premier, jour, de, @card@, ., suivre|être, remplir, espoir, que, ce, année, nous, être, propice, parce, avoir, faire, beaucoup, cour|cours, celui, qui, finir, en, France, même, notre, institution, assurer, à, Etat, efficacité, autorité, lui, permettre, agir, il, ainsi, désormais, finance, équilibre, rester, demain, point, vue, ressource, technique, </li>
<li> frpos : PRP, DET:ART, NOM, ADJ, PUN, NAM, PRO:PER, VER:pres, PRP:det, KON, NUM, SENT, VER:pper, PRO:REL, PRO:DEM, VER:futu, ADV, DET:POS, VER:infi, </li>
<li> lbn : 1, 2, </li>
<li> n : 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, ...</li>
<li> pn : 1, </li>
<li> sn : 1, 2, 3, 4, 5, 6, </li>
<li> word : Pour, la, métropole, française, ,, pour, l', Algérie, communauté, je, forme, des, voeux, ardents, et, confiants, au, premier, jour, de, 1960, ., Je, suis, rempli, espoir, que, cette, année, nous, sera, propice, parce, avons, fait, beaucoup, cours, celle, qui, finit, En, France, même, nos, institutions, assurent, à, Etat, efficacité, autorité, lui, permettent, d', agir, Il, en, ainsi, désormais, Nos, finances, sont, équilibre, le, resteront, demain, Au, point, vue, ressources, technique, </li>
</ul>
<h3 style'font-family:"Arial";'>Propriétés des structures (max 100 valeurs)</h3>
<ul>
<li> p
<ul>
<li> n (16) = 1, 10, 11, 12, 13, 14, 15, 16, 2, 3, 4, 5, 6, 7, 8, 9.</li>
</ul>
</li>
<li> s
<ul>
<li> n (95) = 1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 2, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 3, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 4, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 5, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 6, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 7, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 8, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 9, 90, 91, 92, 93, 94, 95.</li>
</ul>
</li>
<li> text
<ul>
<li> annee (54) = 1959, 1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971, 1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981, 1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012.</li>
<li> id (54) = 0001, 0002, 0003, 0004, 0005, 0006, 0007, 0008, 0009, 0010, 0011, 0012, 0013, 0014, 0015, 0016, 0017, 0018, 0019, 0020, 0021, 0022, 0023, 0024, 0025, 0026, 0027, 0028, 0029, 0030, 0031, 0032, 0033, 0034, 0035, 0036, 0037, 0038, 0039, 0040, 0041, 0042, 0043, 0044, 0045, 0046, 0047, 0048, 0049, 0050, 0051, 0052, 0053, 0054.</li>
<li> loc (7) = chirac, dg, giscard, hollande, mitterrand, pompidou, sarkozy.</li>
</ul>
</li>
</ul>
</body>
</html>
