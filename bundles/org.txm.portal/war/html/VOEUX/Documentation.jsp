<jsp:directive.page contentType="text/html;charset=UTF-8" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" lang="fr" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>Bienvenue à la page d'accueil du corpus VOEUX</title>
    <meta name="generator" content="Bluefish 2.2.2" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta content="Serge Heiden" name="author" />
		
    <script type="text/javascript">
function callTXMSearch()
{
  var corpus = document.getElementById('corpus').value;
  var query = document.getElementById('mot').value;
  var args = {
    "command": "concordance",
    "path": "/"+corpus,
    "query": "[frlemma='"+query+"']",
    "props": "word"
  };
  console.log("executing 'callTXMSearch' script");
  // console.log(JSON.stringify(args, null, 2));
  callTXMCommand(JSON.stringify(args, null, 2));
};
    </script>
    
    <script defer>
    console.log("executing 'defer' script");
    console.log(document.getElementById("mot"));
    $(".mot").on('keydown', function (e) {
    	console.log("executing 'keyup' listener");
    	if (e.key === 'Enter' || e.keyCode === 13) {
        	callTXMSearch();
    	}
    });
/*
    node = document.getElementById("mot");
    console.log(node);
    node.addEventListener("keypress", function(e) {
      console.log("executing 'keyup' listener");
      if (e.key === 'Enter' || e.keyCode === 13) {
        callTXMSearch();
      };
    });
*/
    </script>

<script defer type="text/javascript">alert("ok");</script>

  <div class="textzone" style="background: no-repeat right 5% top 200px url('https://media.lesechos.com/api/v1/images/view/5c2a277d3e454604946a347b/1280x720-webp/021588441295-web.webp'); background-size:  40%;">
   <h1>Bienvenue à la page d'accueil du corpus VOEUX</h1> 
   <p>Le corpus VOEUX est composé de 54 discours de voeux, soit 61&nbsp;143 mots, de présidents français de 1959 à 2012.</p> 
   <p> En cliquant sur les liens ci-dessous, vous pouvez facilement : <br> </p>
   <p><b>Propriétés</b></p>
   <ul> 
    <li> <p> calculer les <a class="command" command="description" path="/VOEUX">dimensions</a> du corpus ; </p> </li>
   </ul>
   <p><b>Textes</b></p>
   <ul>
    <li> <p> afficher la <a class="command" command="metadata" path="/VOEUX">liste des discours</a> (cliquer sur les icones de livres pour lire un discours particulier) ; </p> </li> 
    <li> <p> lire directement le début du <a class="command" command="edition" path="/VOEUX" textid="0010" editions="default" pageid="1">discours de 1968</a> de De Gaulle ; </p> </li>
   </ul>
   <p><b>Lexique</b> (double-cliquer sur une ligne pour calculer la concordance d'un mot)</p>
   <ul>
    <li> <p> calculer le <a class="command" command="lexicon" path="/VOEUX">lexique</a> hiérarchique complet (trié par fréquence décroissante) ; </p> </li> 
    <li> <p> calculer le <a class="command" command="index" path="/VOEUX" query="[frpos=&quot;NOM&quot;]">lexique des noms communs</a> (tous les lexiques suivants sont calculés à partir de la commande Index) ; </p> </li> 
    <li> <p> calculer le <a class="command" command="index" path="/VOEUX" query="[frpos=&quot;NAM&quot;]">lexique des noms propres</a> ; </p> </li>
   </ul>
   <p><b>Concordance</b></p>
   <ul>
    <li> <p>calculer la concordance :</p> 
     <ul> 
      <li> <p> d'un mot : «&nbsp; <a class="command" command="concordance" path="/VOEUX" query="temps">temps&nbsp;»</a> (double-cliquer sur une ligne de concordance pour lire l'occurrence du pivot en plein texte dans l'édition) ; </p> </li> 
      <li> <p> d'un préfixe : «&nbsp; <a class="command" command="concordance" path="/VOEUX" query="souhait.*">souhait.*</a> &nbsp;» ; </p> </li> 
      <li> <p> d'une disjonction (mot A OU mot B) : «&nbsp; <a class="command" command="concordance" path="/VOEUX" query="européen.*|Europe">européen.*|Europe</a> &nbsp;» ; </p> </li> 
      <li> <p> d'un lemme particulier : «&nbsp; <a class="command" command="concordance" path="/VOEUX" query="[frlemma=&quot;souhaiter&quot;]">[frlemma="souhaiter"]</a> &nbsp;» ; </p> </li> 
      <li> <p> d'une combinaison de lemme et de partie du discours : «&nbsp; <a class="command" command="concordance" path="/VOEUX" query="[frlemma=&quot;pouvoir&quot; &amp; frpos=&quot;VER.*&quot;]">[frlemma="pouvoir" &amp; frpos="VER.*"]</a> &nbsp;» ; </p> </li>
     </ul>
    </li>
   </ul>
   <ul>
    <li> <p>formulaire de recherche :</p>
     <ul>
      <li> <p> choisir le corpus : <select id="corpus" name="corpus" size="1"> <option value="VOEUX">VOEUX</option> <option value="VOEUX/admin:DeGaulle">De Gaulle</option> </select> </p> </li>
      <li> <p> le mot : <input id="mot" value="jeune"> (vous pouvez utiliser une expression régulière) </p> </li>
      <li> <p> lancer la <button onclick="callTXMSearch();">Recherche</button> </p></li>
     </ul>
    </li>
   </ul>
   <p><b>Index</b></p>
   <ul>
    <li> <p> calculer la fréquence du mot «&nbsp; <a class="command" command="index" path="/VOEUX" query="bonheur">bonheur</a> &nbsp;» ; </p> </li> 
    <li> <p> lister les réalisations du radical <a class="command" command="index" path="/VOEUX" query=".*patri.*">.*patri.*</a> ; </p> </li> 
    <li> <p> lister <a class="command" command="index" path="/VOEUX" query="[word=&quot;pays|nation&quot;] [frpos=&quot;ADJ&quot;]">comment est qualifié le pays</a> (index de [word="pays|nation"] [frpos="ADJ"]) ; </p> </li> 
    <li> <p> lister <a class="command" command="index" path="/VOEUX" query="[frpos=&quot;NOM&quot;] [word=&quot;français.*&quot;]">ce qui est français</a> (index de [frpos="NOM"] [word="français.*"]) ; </p> </li> 
    <li> <p> lister les <a class="command" command="index" path="/VOEUX" query="[frlemma=&quot;politique&quot; &amp; frpos=&quot;NOM&quot;] [frpos=&quot;ADJ&quot;]">différentes politiques</a> (index de [frlemma="politique" &amp; frpos="NOM"] [frpos="ADJ"]) ; </p> </li> 
    <li> <p> lister sur <a class="command" command="index" path="/VOEUX" query="[frlemma=&quot;ne&quot;] [frpos=&quot;VER.*&quot; &amp; frlemma!=&quot;avoir|être&quot;]">quels verbes portent principalement les négations</a> (index de [frlemma="ne"] [frpos="VER.*"]) ; </p> </li> 
    <li> <p> lister des séquences de mots comprenant <a class="command" command="index" path="/VOEUX" query="[frlemma=&quot;ne&quot;][frlemma!=&quot;pas&quot;]{1,3}[frlemma=&quot;pas&quot;]">des insertions ou des discontinuités</a> (index de [frlemma="ne"][frlemma!="pas"]{1,3}[frlemma="pas"]) ;</p> </li>
   </ul>
  <p><b>Sous-corpus</b> </p>
   <ul>
    <li> <p> lister les <a class="command" command="index" path="/VOEUX/admin:DeGaulle" query="[frpos=&quot;NOM&quot;]">substantifs</a> utilisés par De Gaulle ; </p> </li> 
    <li> <p> lister les <a class="command" command="index" path="/VOEUX/admin:DeGaulle" query="[frpos=&quot;ADJ&quot;]">adjectifs</a> utilisés par De Gaulle ; </p> </li>
   </ul>
   <p><b>Téléchargement</b></p>
   <ul>
    <li> <p> télécharger le <a class="command" command="download" path="/VOEUX">corpus binaire</a> à utiliser avec l'application TXM pour poste </p> </li> 
   </ul> 
   <hr> 
   <p>Sources du corpus Copyright © 2010 Jean-Marc Leblanc. Licensed under Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported</p> 
   <p>Corpus binaire TXM sous licence CC BY-NC-SA 3.0</p> 
   <p> Veuillez nous <a href="mailto:textometrie@groupes.renater.fr?subject=[txm%20demo%20portal]%20VOEUX%20corpus&amp;body=%20">contacter</a> pour plus d’informations. </p> 
  </div>   
 </body>
</html>

