<jsp:directive.page contentType="text/html;charset=UTF-8" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<title>Aide &amp; Liens</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="css/TXM WEB.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="css/TXM WEB PRINT.css" rel="stylesheet" type="text/css" media="print" />
	<meta content="Serge Heiden" name="author" />
</head>
<body>
<h3>Words</h3>
<ul>
<li><a class="command" command="concordance" path="/GRAAL" query="je">je</a></li>
<li><a class="command" command="concordance" path="/GRAAL" query="tu">tu</a></li>
<li><a class="command" command="concordance" path="/GRAAL" query="il">il</a></li>
<li><a class="command" command="concordance" path="/GRAAL" query="elle">elle</a></li>
</ul>
<p>
Saisir un mot à chercher :<br>
<a class="command" type="button" command="concordance" path="/GRAAL">elle</a><a/>
</p>

<h3>Themes</h3>

<ul>
<li><a class="command" command="concordance" path="/GRAAL" query="je">je</a></li>
<li><a class="command" command="concordance" path="/GRAAL" query="tu">tu</a></li>
<li><a class="command" command="concordance" path="/GRAAL" query="il|ils">il(s)</a></li>
<li><a class="command" command="concordance" path="/GRAAL" query="elle|elles">elle(s)</a></li>
</ul>
</body></html>