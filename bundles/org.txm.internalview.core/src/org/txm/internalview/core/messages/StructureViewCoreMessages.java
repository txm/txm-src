package org.txm.internalview.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Internal view core messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class StructureViewCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.internalview.core.messages.messages"; //$NON-NLS-1$

	public static String RESULT_TYPE;


	public static String structureInformationsColon;

	public static String internalViewColonFailedToRetrieveStructPropertiesValuesColonP0;

	public static String noStructureInformations;


	public static String info_browsingP0;


	static {
		Utf8NLS.initializeMessages(BUNDLE_NAME, StructureViewCoreMessages.class);
	}
}
