package org.txm.internalview.core.functions;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.internalview.core.messages.StructureViewCoreMessages;
import org.txm.internalview.core.preferences.StructureViewPreferences;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

/**
 * Internal view.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class StructureView extends TXMResult {

	protected List<Match> matches = new ArrayList<>();

	protected int nmatches = 0;

	/**
	 * Current page.
	 */
	@Parameter(key = StructureViewPreferences.CURRENT_PAGE)
	protected int pCurrentPage;

	/**
	 * Structural unit to explore.
	 */
	@Parameter(key = TXMPreferences.STRUCTURAL_UNIT)
	protected StructuralUnit pStructuralUnit;

	/**
	 * Word properties to display.
	 */
	@Parameter(key = TXMPreferences.UNIT_PROPERTIES)
	protected List<WordProperty> pWordProperties;

	/**
	 * Word properties to display.
	 */
	@Parameter(key = StructureViewPreferences.CUT_STRUCTURE_CONTENT)
	protected Integer pNumberOfLines;

	/**
	 * Structural units to display.
	 */
	@Parameter(key = TXMPreferences.STRUCTURAL_UNIT_PROPERTIES)
	protected List<StructuralUnitProperty> pStructuralUnitsProperties;

	/**
	 * The number of the structural unit to show
	 */
	@Parameter(key = TXMPreferences.INDEX, type = Parameter.RENDERING)
	protected Integer pIndex;

	/**
	 * 
	 * @param parent
	 */
	public StructureView(CQPCorpus parent) {
		super(parent);
	}

	/**
	 * 
	 * @param parametersNodePath
	 */
	public StructureView(String parametersNodePath) {
		super(parametersNodePath);
	}



	@Override
	public boolean loadParameters() throws Exception {
		try {
			String str = this.getStringParameterValue(TXMPreferences.STRUCTURAL_UNIT);
			if (str.length() == 0) {
				try {
					StructuralUnit struct = this.getCorpus().getStructuralUnit("text"); //$NON-NLS-1$
					if (struct != null) {
						this.pStructuralUnit = struct;
					}
					struct = this.getCorpus().getStructuralUnit("div"); //$NON-NLS-1$
					if (struct != null) {
						this.pStructuralUnit = struct;
					}
					struct = this.getCorpus().getStructuralUnit("p"); //$NON-NLS-1$
					if (struct != null) {
						this.pStructuralUnit = struct;
					}
				}
				catch (CqiClientException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			else {
				this.pStructuralUnit = this.getCorpus().getStructuralUnit(str);
			}

		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
		try {
			String str = this.getStringParameterValue(TXMPreferences.UNIT_PROPERTIES);
			if ("*".equals(str)) {
				this.pWordProperties = new ArrayList<WordProperty>();
				pWordProperties.add(this.getCorpus().getWordProperty());

				for (WordProperty p : this.getCorpus().getOrderedProperties()) {
					if (!pWordProperties.contains(p) && (p.getName().endsWith("lemma") || p.getName().endsWith("pos"))) {
						pWordProperties.add(p);
					}
				}
			}
			else {
				this.pWordProperties = (List<WordProperty>) WordProperty.stringToProperties(this.getCorpus(), str);
			}
		}
		catch (Exception e2) {
			Log.printStackTrace(e2);
		}
		try {
			String str = this.getStringParameterValue(TXMPreferences.STRUCTURAL_UNIT_PROPERTIES);
			if (str.length() == 0) {
				StructuralUnitProperty structP = pStructuralUnit.getProperty("id");
				if (structP != null) {
					this.pStructuralUnitsProperties = Arrays.asList(structP);
				}
				structP = pStructuralUnit.getProperty("n");
				if (structP != null) {
					this.pStructuralUnitsProperties = Arrays.asList(structP);
				}
			}
			else {
				this.pStructuralUnitsProperties = StructuralUnitProperty.stringToProperties(this.getCorpus(), str);
			}
		}
		catch (Exception e3) {
			Log.printStackTrace(e3);
		}
		return true;
	}


	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		if (this.hasParameterChanged(StructureViewPreferences.STRUCTURAL_UNIT) || this.hasParameterChanged(StructureViewPreferences.NB_OF_ELEMENTS_PER_PAGE)) {

			// reset the current page (only if the result has been computed at least once otherwise it breaks the deserialization)
			if (this.hasBeenComputedOnce()) {
				this.pCurrentPage = 0;
			}

			// find struct start-end of the current corpus
			CQLQuery query = new CQLQuery("<" + pStructuralUnit.getName() + "> [] expand to " + pStructuralUnit); //$NON-NLS-1$ //$NON-NLS-2$
			// System.out.println(query);
			QueryResult result = getCorpus().query(query, "test", false); //$NON-NLS-1$
			nmatches = result.getNMatch();
			if (nmatches > 0) {
				matches = result.getMatches();
			}
			result.drop();
			// System.out.println(matches);
			return (nmatches > 0);
		}
		return true;
	}



	@Override
	public boolean canCompute() {

		return getCorpus() != null &&
				pStructuralUnit != null &&
				pWordProperties != null &&
				pWordProperties.size() > 0 &&
				pStructuralUnitsProperties != null &&
				pStructuralUnitsProperties.size() > 0 &&
				pNumberOfLines != null &&
				pNumberOfLines > 0;
	}

	/**
	 * get the ith page and set the current page number to i
	 * 
	 * @param i
	 * @return
	 * @throws CqiClientException
	 */
	protected LinkedHashMap<Property, List<String>> getPageContent(int i) throws CqiClientException {

		LinkedHashMap<Property, List<String>> rez = new LinkedHashMap<>();

		if (i >= 0 && i < nmatches) {
			Match m = matches.get(i);
			Collection<? extends Integer> positions = m.getRange();
			int max = Math.min(positions.size(), pNumberOfLines);
			int[] p = new int[max];
			int c = 0;
			for (int v : positions) {
				p[c++] = v;
				if (c == max) break;
			}
			for (Property prop : pWordProperties) {
				rez.put(prop, Match.getValuesForProperty(prop, p));
			}
		}
		return rez;
	}


	/**
	 * 
	 * @return the current page content map
	 * @throws CqiClientException
	 */
	public HashMap<Property, List<String>> getCurrentPageContent() throws CqiClientException {

		HashMap<Property, List<String>> current = this.getPageContent(pCurrentPage);
		return current;
	}



	/**
	 * Gets the current page number.
	 * 
	 * @return the current page number
	 */
	public int getCurrentPage() {

		return pCurrentPage;
	}

	@Override
	public boolean saveParameters() {

		if (pStructuralUnit != null) {
			this.saveParameter(StructureViewPreferences.STRUCTURAL_UNIT, pStructuralUnit.getName());
		}
		if (pStructuralUnitsProperties != null) {
			String str = StructuralUnitProperty.propertiesToString(pStructuralUnitsProperties);
			this.saveParameter(StructureViewPreferences.STRUCTURAL_UNIT_PROPERTIES, str);
		}
		if (pWordProperties != null) {
			this.saveParameter(StructureViewPreferences.UNIT_PROPERTIES, WordProperty.propertiesToString(pWordProperties));
		}

		return true;
	}


	@Override
	public String getComputingStartMessage() {

		if (this.hasBeenComputedOnce()) {
			return TXMCoreMessages.bind("Updating the browser of {0}", this.getParent().getSimpleName());
		}
		else {
			return TXMCoreMessages.bind(StructureViewCoreMessages.info_browsingP0, this.getParent().getSimpleName());
		}
	}


	@Override
	public void clean() {
		// nothing to do
	}


	/**
	 * Gets the linked corpus.
	 * 
	 * @return
	 */
	public CQPCorpus getCorpus() {

		return (CQPCorpus) getParent();
	}


	@Override
	public String getDetails() {

		return getCorpus().getName() + " " + this.pStructuralUnit.getName() + " " + (this.pCurrentPage + 1); //$NON-NLS-1$ //$NON-NLS-2$
	}


	@Override
	public String getSimpleName() {
		if (!this.hasBeenComputedOnce()) {
			return null;
		}
		else {
			try {
				return this.pStructuralUnit.getName() + " (" + (this.pCurrentPage + 1) + " / " + this.nmatches + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			catch (Exception e) {
				return null;
			}
		}
	}


	@Override
	public String getName() {

		return this.getCorpus().getName() + TXMPreferences.PARENT_NAME_SEPARATOR + this.getSimpleName();
	}


	/**
	 * Gets the number of matched segments.
	 * 
	 * @return
	 */
	public int getSegmentsCount() {
		return nmatches;
	}


	/**
	 * Gets lexical properties to display.
	 * 
	 * @return
	 */
	public List<WordProperty> getProperties() {
		return pWordProperties;
	}

	/**
	 * 
	 * @return
	 */
	public int getSegmentLength() {
		if (matches.size() == 0) {
			return 0;
		}
		if (matches.size() < pCurrentPage) {
			return 0;
		}

		Match m = matches.get(pCurrentPage);
		return m.size();
	}

	/**
	 * 
	 * @return
	 */
	public String getStructInfos() {
		return getStructInfos(getCurrentPage());
	}
	
	/**
	 * 
	 * @return
	 */
	public String getStructInfos(int ipage) {

		try {
			if (pStructuralUnitsProperties.size() > 0) {
				String str = "";
				for (Property sup : pStructuralUnitsProperties) {
					int[] array = { ipage };
					str += " " + sup.getName() + "=" + CQPSearchEngine.getCqiClient().struc2Str(sup.getQualifiedName(), array)[0]; //$NON-NLS-1$ //$NON-NLS-2$
				}
				return str;
			}
		}
		catch (Exception e) {
			System.out.println(NLS.bind(StructureViewCoreMessages.internalViewColonFailedToRetrieveStructPropertiesValuesColonP0, e.getMessage()));
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return StructureViewCoreMessages.noStructureInformations;
	}

	/**
	 * Gets the structural unit to explore.
	 * 
	 * @return
	 */
	public StructuralUnit getStructuralUnit() {
		return pStructuralUnit;
	}


	/**
	 * Dumps the specified segment in the console.
	 * 
	 * @param seg
	 */
	public void printSeg(HashMap<Property, List<String>> seg) {
		Collection<List<String>> values = seg.values();
		if (values.size() == 0) {
			return;
		}

		for (List<String> list : values) {
			for (String value : list) {
				System.out.print("\t" + value); //$NON-NLS-1$
			}
			System.out.println();
		}
		System.out.println();
	}


	public void setParameters(List<WordProperty> properties, StructuralUnit struct, List<StructuralUnitProperty> structProperties, Integer currentStructure) {
		if (properties != null) this.pWordProperties = properties;
		if (struct != null) this.pStructuralUnit = struct;
		if (structProperties != null) this.pStructuralUnitsProperties = structProperties;
		if (currentStructure != null) this.pCurrentPage = currentStructure;

		this.setDirty();
	}

	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		
		PrintWriter writer = IOUtils.getWriter(outfile, encoding);
		StringBuilder buffer = new StringBuilder();
		
		buffer.setLength(0);
		buffer.append(this.pStructuralUnit.getName());
		for (Property p : pWordProperties) {
			if (buffer.length() > 0) buffer.append("\t");
			buffer.append(p.getName());
		}
		writer.println(buffer.toString()); //$NON-NLS-1$
		
		for (int ipage = 0; ipage < nmatches; ipage++) { // run through all pages
			
			writer.println(getStructInfos(ipage).trim());
			
			LinkedHashMap<Property, List<String>> page = getPageContent(ipage);
			int size = page.get(pWordProperties.get(0)).size();
			
			for (int i = 0 ; i < size ; i++) {
				
				buffer.setLength(0);
				for (Property p : pWordProperties) {
					buffer.append("\t");
					buffer.append(page.get(p).get(i));
				}
				writer.println(buffer.toString()); //$NON-NLS-1$
			}
		}
		writer.close();
		return false;
	}


	/**
	 * Sets the structural unit to explore.
	 * 
	 * @param structuralUnit
	 */
	public void setStructuralUnit(StructuralUnit structuralUnit) {
		this.pStructuralUnit = structuralUnit;
	}


	/**
	 * Sets the word properties to display.
	 * 
	 * @param properties
	 */
	public void setWordProperties(List<WordProperty> properties) {
		this.pWordProperties = properties;
	}

	/**
	 * Sets the structural unit properties to display.
	 * 
	 * @param structuralUnitProperties
	 */
	public void setStructuralUnitProperties(List<StructuralUnitProperty> structuralUnitProperties) {
		this.pStructuralUnitsProperties = structuralUnitProperties;
	}


	/**
	 * Sets the current page number.
	 * 
	 * @param currentPage the currentPage to set
	 */
	public void setCurrentPage(int currentPage) {
		this.pCurrentPage = currentPage;

		if (this.pCurrentPage < 0 || this.pCurrentPage > (this.nmatches - 1)) {
			System.err.println("StructureView.setCurrentPage(): current page (" + this.pCurrentPage + ") is outside matches range (" + this.nmatches + ").");
			this.pCurrentPage = 0;
		}
	}

	@Override
	public String getResultType() {
		return StructureViewCoreMessages.RESULT_TYPE;
	}

	/**
	 * its not exactly the number per page since there is no page :D
	 * 
	 * @return
	 */
	public int getNLinesPerPage() {
		return pNumberOfLines;
	}

}
