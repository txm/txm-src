package org.txm.internalview.core.preferences;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Default preferences initializer.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class StructureViewPreferences extends TXMPreferences {

	public static final String CUT_STRUCTURE_CONTENT = "cut_structure_content";

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(StructureViewPreferences.class)) {
			new StructureViewPreferences();
		}
		return TXMPreferences.instances.get(StructureViewPreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.put(STRUCTURAL_UNIT, "");
		preferences.put(UNIT_PROPERTIES, "*");
		preferences.put(STRUCTURAL_UNIT_PROPERTIES, "");
		preferences.putInt(CURRENT_PAGE, 0);
		preferences.putInt(CUT_STRUCTURE_CONTENT, 100);
	}
}
