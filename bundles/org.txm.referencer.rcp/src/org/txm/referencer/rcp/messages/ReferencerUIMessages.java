package org.txm.referencer.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Referencer UI messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ReferencerUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.referencer.rcp.messages.messages"; //$NON-NLS-1$

	public static String references;

	public static String showingFirstResultPage;

	public static String showingLastResultPage;

	public static String showingNextResultPage;

	public static String showingPreviousResultPage;

	public static String referencePattern;

	public static String referencer;

	public static String orderReferencesByFrequency;

	public static String showCounts;

	public static String orderReferencesByFrequencyInsteadOfNames;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, ReferencerUIMessages.class);
	}

	private ReferencerUIMessages() {
	}
}
