// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.referencer.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.referencer.core.functions.Referencer;
import org.txm.referencer.rcp.editors.ReferencerEditor;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

/**
 * Opens the Referencer editor with a corpus or a Referencer result .
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class ComputeReferencer extends BaseAbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		if (!this.checkCorpusEngine()) {
			return false;
		}

		Object selection = this.getCorporaViewSelectedObject(event);
		Referencer referencer = null;

		// Creating from Corpus
		if (selection instanceof CQPCorpus) {
			referencer = new Referencer((CQPCorpus) selection);
		}
		// Reopening from existing result
		else if (selection instanceof Referencer) {
			referencer = (Referencer) selection;
		}
		// Error
		else {
			return super.logCanNotExecuteCommand(selection);
		}

		TXMEditor.openEditor(referencer, ReferencerEditor.class.getName());

		return null;
	}

}
