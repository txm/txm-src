// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.referencer.rcp.handlers;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.rcp.editors.TXMResultEditorInput;
import org.txm.referencer.core.functions.Referencer;
import org.txm.referencer.core.functions.Referencer.Line;
import org.txm.referencer.rcp.editors.ReferencerEditor;
import org.txm.searchengine.cqp.ReferencePattern;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;

// TODO: Auto-generated Javadoc
/**
 * The Class ReferencerToConc.
 */
@Deprecated
public class ___ReferencerToConc extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IStructuredSelection selection = (IStructuredSelection) HandlerUtil
				.getActiveWorkbenchWindow(event).getActivePage().getSelection();

		ReferencerEditor refeditor = (ReferencerEditor) HandlerUtil
				.getActiveWorkbenchWindow(event).getActivePage()
				.getActivePart();

		return link(refeditor, selection);
	}

	/**
	 * Link.
	 *
	 * @param refeditor the refeditor
	 * @param iSelection the selection
	 * @return the object
	 */
	public static Object link(ReferencerEditor refeditor, ISelection iSelection) {
		IStructuredSelection selection = (IStructuredSelection) iSelection;
		String query = getQuery(refeditor.getReferencer(), selection);
		if (query.length() == 0) {
			return null;
		}
		CQPCorpus corpus = refeditor.getCorpus();
		Concordance concordance = new Concordance(corpus);

		ReferencePattern rp = new ReferencePattern();
		for (Property p : refeditor.getReferencer().getPattern()) {
			rp.addProperty(p);
		}

		concordance.setParameters(new CQLQuery(query), null, null, null, null, null, null, rp, rp, null, null);
		TXMResultEditorInput editorInput = new TXMResultEditorInput(concordance);

		IWorkbenchPage page = refeditor.getEditorSite().getWorkbenchWindow().getActivePage();
		try {
			ConcordanceEditor conceditor = (ConcordanceEditor) page.openEditor(editorInput,
					ConcordanceEditor.class.getName());
		}
		catch (PartInitException e) {
			System.err.println("Error: " + e.getLocalizedMessage()); //$NON-NLS-1$
		}

		return null;
	}

	/**
	 * Gets the query.
	 *
	 * @param selection the selection
	 * @return the query
	 */
	private static String getQuery(Referencer referencer, IStructuredSelection selection) {
		String query = ""; //$NON-NLS-1$

		Line fline = (Line) selection.getFirstElement();
		int nbToken = 1;

		List<Line> lines = selection.toList();

		Property prop = referencer.getProperty();
		for (int t = 0; t < nbToken; t++) {
			query += "[" + prop + "=\""; //$NON-NLS-1$ //$NON-NLS-2$

			for (Line line : lines) {
				String s = line.getPropValue();
				s = CQLQuery.addBackSlash(s);
				query += s + "|"; //$NON-NLS-1$
			}
			query = query.substring(0, query.length() - 1);
			query += "\"] "; //$NON-NLS-1$
		}
		query = query.substring(0, query.length() - 1);
		return query;
	}
}
