// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.referencer.rcp.preferences;

import org.eclipse.ui.IWorkbench;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.referencer.core.preferences.ReferencerPreferences;
import org.txm.referencer.rcp.messages.ReferencerUIMessages;


/**
 * Referencer preference page.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ReferencerPreferencePage extends TXMPreferencePage {



	@Override
	public void createFieldEditors() {
		
		BooleanFieldEditor orderfield = new BooleanFieldEditor(ReferencerPreferences.SORT_BY_FREQUENCIES, ReferencerUIMessages.orderReferencesByFrequency, this.getFieldEditorParent());
		orderfield.setToolTipText(ReferencerUIMessages.orderReferencesByFrequencyInsteadOfNames);
		this.addField(orderfield);
	}

	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(ReferencerPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setTitle(ReferencerUIMessages.referencer);
	}



}
