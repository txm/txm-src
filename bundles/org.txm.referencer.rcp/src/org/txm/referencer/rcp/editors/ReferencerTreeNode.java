package org.txm.referencer.rcp.editors;

import java.util.ArrayList;

import org.eclipse.jface.viewers.TreeNode;
import org.txm.referencer.core.functions.Referencer;


public class ReferencerTreeNode extends TreeNode {

	public ReferencerTreeNode(Referencer.Line line) {

		super(line);
	}

	public static ArrayList<ReferencerTreeNode> buildTree(ArrayList<ReferencerTreeNode> nodes) {
		return nodes;
	}

}
