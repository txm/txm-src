// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.referencer.rcp.editors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableColumn;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.Parameter;
import org.txm.rcp.StatusLine;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TableKeyListener;
import org.txm.rcp.editors.TableResultEditor;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.editors.listeners.ComputeKeyListener;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.TXMFontRegistry;
import org.txm.rcp.swt.jface.TableViewerSeparatorColumn;
import org.txm.rcp.swt.widget.AssistedChoiceQueryWidget;
import org.txm.rcp.swt.widget.NavigationWidget;
import org.txm.rcp.swt.widget.PropertiesSelector;
import org.txm.referencer.core.functions.Referencer;
import org.txm.referencer.core.functions.Referencer.Line;
import org.txm.referencer.core.preferences.ReferencerPreferences;
import org.txm.referencer.rcp.messages.ReferencerUIMessages;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;

/**
 * The Class ReferencerEditor display&update a Referencer TXMResult
 */
public class ReferencerEditor extends TXMEditor<Referencer> implements TableResultEditor {

	/** The Constant ID. */
	public static final String ID = ReferencerEditor.class.getName();

	/** The corpus. */
	CQPCorpus corpus;

	/** The referencer. */
	Referencer referencer;


	/** The navigation area. */
	NavigationWidget navigationArea;

	/** The line table viewer. */
	protected TableViewer viewer;

	/** The unit column. */
	protected TableColumn unitColumn;

	/** The freq column. */
	protected TableColumn freqColumn;

	/** The lineperpage. */
	int lineperpage = 100;

	/** The top line. */
	int topLine = 0;

	/** The bottom line. */
	int bottomLine;



	/** The mess. */
	String mess;



	/** The querywidget. */
	@Parameter(key = ReferencerPreferences.QUERY)
	protected AssistedChoiceQueryWidget querywidget;

	/**
	 * Word property selector.
	 */
	@Parameter(key = ReferencerPreferences.UNIT_PROPERTY)
	protected PropertiesSelector<Property> propertiesSelector;

	/**
	 * Show counts button
	 */
	@Parameter(key = ReferencerPreferences.SHOW_COUNTS)
	protected Button showCountsButton;

	/** The pattern area. */
	@Parameter(key = ReferencerPreferences.PATTERN)
	protected PropertiesSelector<StructuralUnitProperty> patternArea;

	private TableColumn nColumn;

	private TableKeyListener tableKeyListener;



	@Override
	public void _createPartControl() {

		this.referencer = (this.getResult());
		this.corpus = referencer.getCorpus();

		// Computing listener
		ComputeSelectionListener computeSelectionListener = new ComputeSelectionListener(this);
		ComputeKeyListener computeKeyListener = new ComputeKeyListener(this);

		Composite paramArea = getExtendedParametersGroup();
		paramArea.setLayout(new GridLayout(3, false));

		getMainParametersComposite().getLayout().numColumns = 3;
		getMainParametersComposite().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));

		Label queryLabel = new Label(getMainParametersComposite(), SWT.NONE);
		queryLabel.setText(TXMUIMessages.ampQuery);
		queryLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

		querywidget = new AssistedChoiceQueryWidget(getMainParametersComposite(), SWT.None, this.corpus);
		querywidget.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		querywidget.addKeyListener(computeKeyListener);

		// [Edit]
		propertiesSelector = new PropertiesSelector<>(getMainParametersComposite(), SWT.NONE);
		propertiesSelector.setMaxPropertyNumber(1);
		propertiesSelector.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		propertiesSelector.setLayout(new GridLayout(3, false));
		propertiesSelector.addSelectionListener(computeSelectionListener);

		// [Search]
		// Button go = new Button(paramArea, SWT.PUSH);
		// go.setText(TXMUIMessages.SEARCH);
		// go.setLayoutData(new GridData(GridData.FILL, GridData.FILL,
		// false, false));
		// Font f = go.getFont();
		// FontData defaultFont = f.getFontData()[0];
		// defaultFont.setStyle(SWT.BOLD);
		// Font newf = new Font(go.getDisplay(), defaultFont);
		// go.setFont(newf);
		//
		// go.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		// go.addSelectionListener(computeSelectionListener);

		// [pattern : ... ]
		patternArea = new PropertiesSelector<>(paramArea, SWT.NONE);
		patternArea.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false, 3, 1));
		patternArea.setLayout(new GridLayout(3, false));
		patternArea.setTitle(ReferencerUIMessages.referencePattern);
		patternArea.addSelectionListener(computeSelectionListener);

		// [show counts]
		showCountsButton = new Button(paramArea, SWT.CHECK);
		showCountsButton.setText(ReferencerUIMessages.showCounts);
		showCountsButton.setSelection(referencer.getShowCounts());
		showCountsButton.setToolTipText("Display the number of matches for each reference");

		// pagination
		GLComposite navigationComposite = getBottomToolbar().installGLComposite("navigation", 1, false); //$NON-NLS-1$
		navigationComposite.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));

		navigationArea = new NavigationWidget(navigationComposite, SWT.NONE);
		navigationArea.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, true, false));

		navigationArea.addFirstListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				StatusLine.setMessage(ReferencerUIMessages.showingFirstResultPage);
				fillDisplayArea(0, lineperpage);
				viewer.getTable().select(0);
				viewer.getTable().showSelection();
				StatusLine.setMessage(""); //$NON-NLS-1$
			}
		});
		navigationArea.addLastListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				StatusLine.setMessage(ReferencerUIMessages.showingLastResultPage);
				fillDisplayArea(referencer.getNLines()
						- lineperpage, referencer.getNLines());
				viewer.getTable().select(0);
				viewer.getTable().showSelection();
				StatusLine.setMessage(""); //$NON-NLS-1$
			}
		});
		navigationArea.addNextListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				StatusLine.setMessage(ReferencerUIMessages.showingNextResultPage);
				fillDisplayArea(topLine + lineperpage,
						bottomLine + lineperpage);
				viewer.getTable().select(0);
				viewer.getTable().showSelection();
				StatusLine.setMessage(""); //$NON-NLS-1$
			}
		});
		navigationArea.addPreviousListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				StatusLine.setMessage(ReferencerUIMessages.showingPreviousResultPage);
				fillDisplayArea(topLine - lineperpage,
						bottomLine - lineperpage);
				viewer.getTable().select(0);
				viewer.getTable().showSelection();
				StatusLine.setMessage(""); //$NON-NLS-1$
			}
		});

		// result

		Composite resultArea = getResultArea();

		viewer = new TableViewer(resultArea, SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER | SWT.VIRTUAL);
		tableKeyListener = TableUtils.installCopyAndSearchKeyEvents(viewer);
		tableKeyListener.installSearchGroup(this);
		tableKeyListener.installMenuContributions(this);
		TableUtils.installColumnResizeMouseWheelEvents(viewer);
		TableUtils.installTooltipsOnSelectionChangedEvent(viewer);
		if (corpus != null && corpus.getFont() != null && corpus.getFont().length() > 0) {
			Font old = viewer.getTable().getFont();
			FontData fD = old.getFontData()[0];
			// Font f = new Font(old.getDevice(), corpus.getFont(), fD.getHeight(), fD.getStyle());
			Font font = TXMFontRegistry.getFont(Display.getCurrent(), corpus.getFont(), fD.getHeight(), fD.getStyle());
			viewer.getTable().setFont(font);
		}

		viewer.getTable().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		viewer.getTable().setLinesVisible(true);
		viewer.getTable().setHeaderVisible(true);

		viewer.setLabelProvider(new LineLabelProvider(this.referencer));
		viewer.setContentProvider(new LineContentProvider());
		viewer.setInput(new ArrayList<Property>());

		nColumn = TableViewerSeparatorColumn.newSeparator(viewer).getColumn();

		unitColumn = new TableColumn(viewer.getTable(), SWT.LEFT);
		unitColumn.setText(TXMCoreMessages.common_units);
		unitColumn.setToolTipText(TXMCoreMessages.common_units);
		unitColumn.setWidth(200);

		freqColumn = new TableColumn(viewer.getTable(), SWT.LEFT);
		freqColumn.setText(ReferencerUIMessages.references);
		freqColumn.setToolTipText(ReferencerUIMessages.references);
		freqColumn.setWidth(100);

		initializeFields();

		// Add double click, "Send to" command
		TableUtils.addDoubleClickCommandListener(viewer.getTable(), "org.txm.concordance.rcp.handlers.ComputeConcordance"); //$NON-NLS-1$

		// Register the context menu
		TXMEditor.initContextMenu(this.viewer.getTable(), this.getSite(), this.viewer);


	}

	@Override
	public void updateEditorFromResult(boolean update) {

		fillDisplayArea(0, lineperpage);

		if (!querywidget.isDisposed()) {
			querywidget.memorize();
		}

		viewer.getTable().setFocus();
	}



	/**
	 * Fill display area.
	 *
	 * @param from the from
	 * @param to the to
	 */
	public void fillDisplayArea(int from, int to) {
		from = Math.max(from, 0);
		to = Math.min(to, referencer.getNLines());
		List<Line> lines = null;
		if (referencer.getNLines() > 0) {
			try {
				lines = referencer.getLines(from, to);
			}
			catch (Exception e) {
				org.txm.utils.logger.Log.printStackTrace(e);
				lines = new ArrayList<>();
			}
		}
		else {
			lines = new ArrayList<>();
		}

		navigationArea.setInfoLineText("" + (from + 1), //$NON-NLS-1$
				"-" + (to) + " / " + referencer.getNLines()); //$NON-NLS-1$ //$NON-NLS-2$
		navigationArea.setPreviousEnabled(from > 0);
		navigationArea.setFirstEnabled(from > 0);
		navigationArea.setNextEnabled(to < referencer.getNLines() - 1);
		navigationArea.setLastEnabled(to < referencer.getNLines() - 1);

		unitColumn.setText(referencer.getProperty().getName());

		String unitColumnHeader = ""; //$NON-NLS-1$
		for (Property p : referencer.getPattern())
			unitColumnHeader += p.getName() + ", "; //$NON-NLS-1$
		if (unitColumnHeader.length() > 0)
			unitColumnHeader = unitColumnHeader.substring(0, unitColumnHeader
					.length() - 2);
		freqColumn.setText(unitColumnHeader);

		viewer.setInput(lines);
		viewer.refresh();
		// System.out.println("table refreshed from "+from+" to "+to+" with : "+lines);
		topLine = from;
		bottomLine = to;
		for (TableColumn col : viewer.getTable().getColumns()) {
			col.pack();
		}
	}

	/**
	 * Gets the referencer.
	 *
	 * @return the referencer
	 */
	public Referencer getReferencer() {
		return this.referencer;
	}

	/**
	 * Gets the corpus.
	 *
	 * @return the corpus
	 */
	public CQPCorpus getCorpus() {
		return this.corpus;
	}

	/**
	 * 
	 */
	protected void initializeFields() {
		// word properties
		try {
			ArrayList<Property> selectedProps = new ArrayList<>();
			ArrayList<Property> availables = new ArrayList<>();
			availables.addAll(corpus.getOrderedProperties());
			availables.addAll(corpus.getVirtualProperties());

			if (referencer.getProperty() != null) {
				selectedProps.add(referencer.getProperty());
				this.propertiesSelector.setProperties(availables, selectedProps);
			}
			else {
				this.propertiesSelector.setCorpus(corpus);
			}

		}
		catch (CqiClientException e1) {
			org.txm.utils.logger.Log.printStackTrace(e1);
		}

		// references

		try {
			ArrayList<StructuralUnitProperty> availables2 = new ArrayList<>();
			availables2.addAll(corpus.getStructuralUnitProperties());
			Collections.sort(availables2);

			ArrayList<StructuralUnitProperty> selectedProps = new ArrayList<>();
			if (referencer.getPattern() != null) {
				for (StructuralUnitProperty p : referencer.getPattern()) {
					selectedProps.add(p);
				}
			}

			this.patternArea.setProperties(availables2, selectedProps);
		}
		catch (CqiClientException e1) {
			org.txm.utils.logger.Log.printStackTrace(e1);
			return;
		}
	}

	@Override
	public void updateResultFromEditor() {
		// nothing to do
	}

	@Override
	public TableViewer[] getTableViewers() {

		return new TableViewer[] { viewer };
	}

	@Override
	public TableKeyListener getTableKeyListener() {

		return tableKeyListener;
	}
}
