package org.txm.groovy.rcp;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.txm.groovy.core.GroovyPreferences;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;

public class GroovyPreferencePage extends TXMPreferencePage {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 */
	@Override
	protected void createFieldEditors() {
		//this.addField(new StringFieldEditor(GroovyPreferences.ADDITIONAL_ROOT_DIRECTORY, "Additional root directory", this.getFieldEditorParent()));

		this.addField(new BooleanFieldEditor(GroovyPreferences.FAST_PACKAGE_INDEX, Messages.UseTheFastPackageIndexMode, this.getFieldEditorParent()));
	}

	@Override
	public void init(IWorkbench workbench) {

		this.setPreferenceStore(new TXMPreferenceStore(GroovyPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setTitle("Groovy"); //$NON-NLS-1$
		this.setImageDescriptor(IImageKeys.getImageDescriptor("org.txm.groovy.rcp", "icon/groovy.png")); //$NON-NLS-1$ //$NON-NLS-2$
	}
}
