package org.txm.scripts.importer.tigersearch

import org.txm.Toolbox
import org.txm.core.preferences.TBXPreferences
import org.txm.importer.ApplyXsl2;
import org.txm.importer.xtz.*
import org.txm.metadatas.Metadatas
import org.txm.scripts.importer.xtz.*
import org.txm.tigersearch.preferences.TigerSearchPreferences
import org.txm.utils.BundleUtils
import org.txm.utils.FileUtils
import org.txm.utils.io.FileCopy
import org.txm.utils.io.IOUtils
import groovy.xml.XmlSlurper

/**
 * Only build the Metadatas object since all XML-TXM files already exists.
 * Metadatas is used to build text order.
 * 
 * 
 * @author mdecorde
 *
 */
class TSImporter extends XTZImporter {

	public TSImporter(ImportModule module) {
		super(module);
	}

	@Override
	public void process() {
		
		//prepare metadata if any
		File allMetadataFile = Metadatas.findMetadataFile(inputDirectory);
		if (allMetadataFile != null && allMetadataFile.exists()) {
			File copy = new File(module.getBinaryDirectory(), allMetadataFile.getName())
			if (!FileCopy.copy(allMetadataFile, copy)) {
				println "Error: could not create a copy of the metadata file "+allMetadataFile.getAbsoluteFile();
				return;
			}
			metadata = new Metadatas(copy,
			Toolbox.getPreference(TBXPreferences.METADATA_ENCODING),
			Toolbox.getPreference(TBXPreferences.METADATA_COLSEPARATOR),
			Toolbox.getPreference(TBXPreferences.METADATA_TXTSEPARATOR), 1)
		}

		File sourceDirectory = inputDirectory
		File binaryDirectory = module.getBinaryDirectory()
		
		String driverFilename = TigerSearchPreferences.getInstance().getString(TigerSearchPreferences.DRIVER_FILENAME);
		String headerFilename = TigerSearchPreferences.getInstance().getString(TigerSearchPreferences.HEADER_FILENAME);
		
		File master = new File(sourceDirectory, driverFilename)
		
		def xmlFiles = [] // the TIGER XML files
		if (true || !master.exists() ) { // Managing the main.xml file is a pain, we'll do it later
			//println "No main.xml ($master) file found, trying to find a proper TIGER XML file."
//			master = new File(sourceDirectory, "main.xml")
//			String subcorpora = "";
			xmlFiles = sourceDirectory.listFiles(new FileFilter() {
						boolean accept(File file) {
							if (file.isDirectory()) return false;
							if (file.isHidden()) return false;
							String filename = file.getName()
							if (filename.equals("import.xml")) return false;
							if (!filename.endsWith(".xml")) return false;
							if (filename.equals(driverFilename)) return false;
							if (filename.equals(headerFilename)) return false;
							
							return true;
						}
					});
			
			println "Using ${xmlFiles} as TIGER XML source files."
			
		} else { // parse the master file
			println "Using the TIGER MAIN file: $master"
			
			
			for (def s : new XmlSlurper().parse(master).body.subcorpus) {
				String name = ""+s.@external
				if (name.startsWith("file:")) {
					File f = new File(sourceDirectory, name.substring(5));
					if (f.exists()) {
						xmlFiles << f
					} else {
						println "Warning: $name referenced file was nto found: $f"
					}
				}
			}
			
			if (xmlFiles.size() == 0) {
				println "Error no XML file found in $master file"
				isSuccessFul = false;
				return;
			}
		}
		
		// get the last version of the TIGER XML -> XML-TXM XSL
		File tsXSLFile = new File(Toolbox.getTxmHomePath(), "xsl/ts.xsl");
		BundleUtils.copyFiles("org.txm.tigersearch.rcp", "groovy", "org/txm/scripts/importer/tigersearch", "ts.xsl", tsXSLFile.getParentFile());

		File tigerXmlSrcDir = new File(binaryDirectory, "src"); // output directory of the TS XSL transformation
		tigerXmlSrcDir.deleteDir()
		tigerXmlSrcDir.mkdirs();
		
		println "TIGER-XML files: $xmlFiles"
		for (File xmlTigerFile : xmlFiles) {
			FileCopy.copy(xmlTigerFile, new File(tigerXmlSrcDir, xmlTigerFile.getName()));
		}

		File tokenizedDir = new File(module.getBinaryDirectory(), "tokenized");
		
		if (!ApplyXsl2.processImportSources(tsXSLFile, tigerXmlSrcDir, tokenizedDir)) {
			println "Error while applying TS XSL file to $tigerXmlSrcDir"
			isSuccessFul = false;
			return;
		}

		File[] files = FileUtils.listFiles(tokenizedDir);
		if (files == null || files.length == 0) {
			println "Error while applying TS XSL file to $tigerXmlSrcDir is empty"
			isSuccessFul = false;
			return;
		}

		if (!doToXMLTXMStep()) return; // build the XML-TXM files
		
		if (!doInjectMetadataStep()) return; // inject the metadata in the XML-TXM files

		isSuccessFul = FileUtils.listFiles(outputDirectory).length > 0
		
		String cleanDirectories = project.getCleanAfterBuild();
		if ("true".equals(cleanDirectories)) {
			new File(module.getBinaryDirectory(), "tokenized").deleteDir()
			new File(module.getBinaryDirectory(), "src").deleteDir()
			new File(module.getBinaryDirectory(), "split").deleteDir()
		}
	}
}
