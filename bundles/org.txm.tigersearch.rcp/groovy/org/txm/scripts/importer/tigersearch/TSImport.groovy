package org.txm.scripts.importer.tigersearch;

import java.io.File;
import java.util.ArrayList;

import ims.tiger.index.writer.*
import ims.tiger.system.*

import org.txm.Toolbox;
import org.txm.importer.ApplyXsl2;
import org.txm.importer.xtz.*
import org.txm.objects.Project
import org.txm.utils.BundleUtils;
import org.txm.utils.FileUtils
import org.txm.utils.io.FileCopy;
import org.txm.utils.io.IOUtils
import org.apache.log4j.BasicConfigurator;
import org.txm.importer.xtz.*
import org.txm.scripts.importer.xtz.*
import org.txm.searchengine.ts.TIGERSearchEngine
import org.txm.tigersearch.preferences.TigerSearchPreferences

import groovy.xml.XmlSlurper

class TSImport extends XTZImport {
	
	public TSImport(Project params) {
		super(params);
	}
	
	@Override
	public void init(Project p) {
		super.init(p);
		
		importer = new TSImporter(this); // select TIGER XML files then do XTZImporter step
		compiler = new XTZCompiler(this)
		annotater = null; // no annotater step to do
		pager = new XTZPager(this)
	}
	
	/**
	 * Do a XTZ Import then build the TIGERSearch indexes in the binary corpus "tiger" directory
	 */
	@Override
	public void start() throws InterruptedException {
		
		super.start(); // call the usual XTZ import
		
		if (isSuccessful) {
			
			File tigerXmlSrcDir = new File(binaryDirectory, "src")
			tigerXmlSrcDir.mkdirs()
			String driverFilename = TigerSearchPreferences.getInstance().getString(TigerSearchPreferences.DRIVER_FILENAME);
			
			File master = new File(tigerXmlSrcDir, driverFilename) // copy of the master file for later use if the corpus is updated or to get the text order
			
			File srcmaster = new File(sourceDirectory, driverFilename)
			if (srcmaster.exists()) {
				println "Using source TIGER driver file: $srcmaster -> $master"
				FileCopy.copy(srcmaster, master)
			} else {
				println "Building TIGER driver file: $master..."
				String headerFilename = TigerSearchPreferences.getInstance().getString(TigerSearchPreferences.HEADER_FILENAME);
				File header = new File(sourceDirectory, headerFilename)
				if (!header.exists()) {
					// parse TIGER-XML files to build a default header file
				}
				
				//build the master file using the TIGER-XML files of sourceDirectory
				// if necesary converts a driver (with <corpus>) file into a subcorpus (with <subcorpus>file
				def xmlFiles = FileUtils.listFiles(sourceDirectory)
				
				def featuresToSkips = TigerSearchPreferences.getInstance().
					getProjectPreferenceValue(project, TigerSearchPreferences.FEATURE_VALUES_TO_IGNORE_IN_HEADER, 
						TigerSearchPreferences.getInstance().getString(TigerSearchPreferences.FEATURE_VALUES_TO_IGNORE_IN_HEADER)).split(",") as Set;
				
				if (featuresToSkips.size() > 0) {
					println "Feature values to skip when indexing the corpus: $featuresToSkips"
				}
				
				HashMap<String, HashSet<String>> tfeatures = new HashMap<String, HashSet<String>>()
				HashMap<String, HashSet<String>> ntfeatures = new HashMap<String, HashSet<String>>()
				HashSet<String> edges = new HashSet<String>()
				HashSet<String> secedges = new HashSet<String>()
				for (def xmlFile : xmlFiles) {
					//println xmlFile
					def doc = new XmlSlurper().parse(xmlFile)
					def terminals = null
					
					if (doc.name() == "corpus") terminals = doc.body.s.graph.terminals
					else terminals = doc.s.graph.terminals
					
					for (def terminal : doc.s.graph.terminals) {
						
						for (def t : terminal.t) {
							def attributes = t.attributes()
							for (def a : attributes.keySet()) {
								if (a.toString() == "id") continue;
								
								if (!tfeatures.containsKey(a)) {
									tfeatures[a] = new HashSet<String>();
								}
								
								if (featuresToSkips.contains(a.toString())) continue;
								tfeatures[a].add(""+attributes[a])
							}
						}
					}
					
					def nonterminals = null
					if (doc.name() == "corpus") nonterminals = doc.body.s.graph.nonterminals
					else nonterminals = doc.s.graph.nonterminals
					
					for (def nterminal : nonterminals) {
						
						for (def nt : nterminal.nt) {
							def attributes = nt.attributes()
							for (def a : attributes.keySet()) {
								if (a.toString() == "id") continue;
								
								if (!ntfeatures.containsKey(a)) {
									ntfeatures[a] = new HashSet<String>();
								}
								
								if (featuresToSkips.contains(a.toString())) continue;
								ntfeatures[a].add(""+attributes[a])
							}
							for (def edge : nt.edge) {
								edges.add(""+edge.@label)
							}
							for (def secedge : nt.secedge) {
								secedges.add(""+secedge.@label)
							}
						}
					}
					
					if (doc.name() == "corpus") { // convert file to a subcorpus file
						doc.name = "subcorpus"
						doc.head = {}
					}
				}
				
				def orderedFiles = getTXMFilesOrder();
				
//				def defaultTFeature = TigerSearchPreferences.getInstance().
//				getProjectPreferenceValue(project, TigerSearchPreferences.DEFAULT_TFEATURE,
//					TigerSearchPreferences.getInstance().getString(TigerSearchPreferences.DEFAULT_NFEATURE)).split(",");
//				
//				def defaultNTFeature = TigerSearchPreferences.getInstance().
//				getProjectPreferenceValue(project, TigerSearchPreferences.DEFAULT_NTFEATURE,
//					TigerSearchPreferences.getInstance().getString(TigerSearchPreferences.DEFAULT_NTFEATURE)).split(",");
				
				def masterwriter = IOUtils.getWriter(master, "UTF-8")
				masterwriter.println("""<?xml version="1.0" encoding="utf-8"?>
<corpus id="$corpusName">
 <head>
  <meta>
   <name>$corpusName</name>
   <author>TXM</author>
   <date></date>
   <description>default master file generated by TXM for the TIGER import module</description>
   <format>TIGER-XML</format>
   <history></history>
  </meta>
  <annotation>""")
				for (def f : tfeatures.keySet()) {
					if (tfeatures[f].size() > 0) {
						masterwriter.println("""   <feature domain="T" name="$f">""")
						for (def v : tfeatures[f]) {
							v = v.replace("<", "&lt;").replace(">", "&gt;")
							masterwriter.println("""<value name="$v"/>""")
						}
						masterwriter.println("""   </feature>""")
					} else {
						masterwriter.println("""   <feature domain="T" name="$f"/>""")
					}
				}
				masterwriter.flush()
				for (def f : ntfeatures.keySet()) {
					if (ntfeatures[f].size() > 0) {
						masterwriter.println("""   <feature domain="NT" name="$f">""")
						for (def v : ntfeatures[f]) {
							v = v.replace("<", "&lt;").replace(">", "&gt;")
							masterwriter.println("""    <value name="$v"/>""")
						}
						masterwriter.println("""   </feature>""")
					} else {
						masterwriter.println("""   <feature domain="NT" name="$f"/>""")
					}
				}
				masterwriter.flush()
				
				if (edges.size() > 0) {
					masterwriter.println("""   <edgelabel>""")
					for (def v : edges) {
						v = v.replace("<", "&lt;").replace(">", "&gt;")
						masterwriter.println("""    <value name="$v"/>""")
					}
					masterwriter.println("""   </edgelabel>""")
				} else {
					masterwriter.println("""   <edgelabel/>""")
				}
				masterwriter.flush()
				if (secedges.size() > 0) {
					masterwriter.println("""   <secedgelabel>""")
					for (def v : secedges) {
						v = v.replace("<", "&lt;").replace(">", "&gt;")
						masterwriter.println("""    <value name="$v"/>""")
					}
					masterwriter.println("""   </secedgelabel>""")
				} else {
					masterwriter.println("""   <secedgelabel/>""")
				}
				masterwriter.flush()
				masterwriter.println("""
  </annotation>
 </head>
 <body>""")
				for (def xmlFileName : orderedFiles) {
					masterwriter.println("""  <subcorpus external="file:${xmlFileName}.xml" name="${xmlFileName}"/>\n""")
				}
				masterwriter.flush()
				masterwriter.println(""" </body>
</corpus>
""")
				masterwriter.flush()
				masterwriter.close()
			}
			
			TIGERSearchEngine.buildTIGERCorpus(sourceDirectory, this.binaryDirectory, corpusName);
		}
	}
	
	//	ArrayList<String> orderedFiles = null;
	protected ArrayList<String> getTXMFilesOrder() {
		
		String driverFilename = TigerSearchPreferences.getInstance().getString(TigerSearchPreferences.DRIVER_FILENAME);
		File srcDriverFile = new File(sourceDirectory, driverFilename)
		File binDriverFile = new File(binaryDirectory, "src/"+driverFilename)
		if (binDriverFile.exists()) {
			ArrayList<String> orderedFiles = new ArrayList<String>();
			for (def s : new XmlSlurper().parse(binDriverFile).body.subcorpus) {
				String name = ""+s.@external
				if (name.startsWith("file:")) {
					orderedFiles << name.substring(5, name.length() - 4);
				}
			}
			return orderedFiles
		} else if (srcDriverFile.exists()) {
			ArrayList<String> orderedFiles = new ArrayList<String>();
			for (def s : new XmlSlurper().parse(srcDriverFile).body.subcorpus) {
				String name = ""+s.@external
				if (name.startsWith("file:")) {
					orderedFiles << name.substring(5, name.length() - 4);
				}
			}
			return orderedFiles
		} else {
			return super.getTXMFilesOrder();
		}
	}
}