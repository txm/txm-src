<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:edate="http://exslt.org/dates-and-times"
	exclude-result-prefixes="edate xd">

	<xd:doc type="stylesheet">

		<xd:short>
			Feuille de transformation du format TIGER-XML vers le format XML-TXM
		</xd:short>

		<xd:detail>
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.

			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
			Lesser General Public License for more details.

			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>

		<xd:author>Matthieu Decorde, matthieu.decorde AT ens-lyon.fr</xd:author>
		<xd:author>Serge Heiden, slh AT ens-lyon.fr</xd:author>
		<xd:author>Alexey Lavrentev, alexei.lavrentev AT ens-lyon.fr></xd:author>

		<xd:copyright>2016, ENS de Lyon/CNRS (UMR IHRIM Cactus)</xd:copyright>

	</xd:doc>

	<xsl:output method="xml" encoding="UTF-8" indent="yes" />

	<xsl:template match="corpus">
		<corpus>
			<xsl:choose>
				<xsl:when test="subcorpus">
					<xsl:apply-templates select="subcorpus" />
				</xsl:when>
				<xsl:otherwise>
					<text>
						<xsl:apply-templates select="s" />
					</text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates />
		</corpus>
	</xsl:template>

	<xsl:template match="subcorpus">
		<text>
			<xsl:attribute name="name"><xsl:value-of
				select="@name" /></xsl:attribute>
			<xsl:apply-templates select="s" />
		</text>
	</xsl:template>

	<xsl:template match="s">
		<p>
			<s>
				<xsl:attribute name="id"><xsl:value-of
					select="@id" /></xsl:attribute>
				<xsl:apply-templates select="graph/terminals/t" />
			</s>
		</p>
	</xsl:template>

	<xsl:template match="t">
		<w>
			<xsl:for-each select="@*[not(name()='word')]">
				<xsl:copy />
			</xsl:for-each>
			<xsl:value-of select="@word" />
		</w>
	</xsl:template>

</xsl:stylesheet>
