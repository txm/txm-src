/*
Combines subcorpus files to give a single main file.
 */
// TXM package statement
package org.txm.scripts.importer.srcmf

import groovy.xml.XmlParser

// Command-line entry point

def cli = new CliBuilder(
    usage:'TigerSubcorpus2Main.groovy [options] tiger_master.xml'
)
cli.h(longOpt:'help', 'Prints this message.')
cli.o(longOpt:'output', args:1, argName:'outputfile.xml', 'Output to given file.')
options = cli.parse(args)
if (options.arguments().size() != 1) {
    println 'Incorrect number of command line arguments... exiting'
    println cli.usage()
    System.exit(2)
}
def tigerMaster = new File(options.arguments()[0])
def outputFile = new File('outputfile.xml')
if (options.o) {
    outputFile = new File(options.o)
}
script(tigerMaster, outputFile)

def script (File tigerMaster, File outputFile) {
    def masterDoc = new XmlParser().parse(tigerMaster)
    for (def body : masterDoc.body) {
	for (def subNode : body.subcorpus) {
		File subCorpusFile = new File(tigerMaster.getParentFile(), (""+subNode.@external).substring(5))
		def subDoc = new XmlParser().parse(subCorpusFile)
		body.remove(subNode)
		for (def child : subDoc.children())
			body.append(child)
	}
    }
    def strWriter = new StringWriter()
    new groovy.xml.XmlNodePrinter(new PrintWriter(strWriter)).print(masterDoc)
    def rez = strWriter.toString()
    outputFile.withWriter("UTF-8") { writer ->
	writer.println('<?xml version="1.0" encoding="UTF-8"?>')
	writer.print(rez)
    }
}
