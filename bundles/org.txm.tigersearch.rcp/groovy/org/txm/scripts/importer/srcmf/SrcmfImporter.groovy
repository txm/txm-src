/*
 * Calls all import scripts
 */

/**
 *
 * @author tmr
 */

// Command line form of import statements:
// import MasterReheader
// import PunctInjectImport
// import SubcorpusDataInject
// import TigerSubcorpus2Main
// import java.util.logging.FileHandler
// import javax.xml.parsers.DocumentBuilderFactory

// TXM package statement
package org.txm.scripts.importer.srcmf

import java.util.logging.*;

import org.txm.utils.FileUtils
import org.txm.utils.io.IOUtils


// Command line entry point
def cli = new CliBuilder(
    usage:'SrcmfImport.groovy [options] tiger_master.xml xml_txm.xml header_file.xml'
)
cli.h(longOpt:'help', 'Prints this message.')
options = cli.parse(args)
if (options.arguments().size() != 3) {
    println 'Incorrect number of command line arguments... exiting'
    println cli.usage()
    System.exit(2)
}

def tigerFile = new File(options.arguments()[0])
def txmFile = new File(options.arguments()[1])
def headerFile = new File(options.arguments()[2])
def tigerXmlAll = doAllButPnc(
    tigerFile, 
    txmFile, 
    headerFile,
    txmFile.getAbsoluteFile().getParentFile().getParentFile()
)
doPnc(tigerXmlAll, txmFile)
tigerXmlAll.delete()

def doAllButPnc(File tigerFile, File txmFile, File headerFile, File binDir) {
    // Run pos injection script
    File txmSrcDir = txmFile.getAbsoluteFile().getParentFile()
    File tigerDir = new File(binDir, "tiger")
    tigerDir.mkdir()
    File masterpos = new File(tigerDir, "master_pos.xml")
    File xmltxm = FileUtils.listFiles(txmSrcDir)[0]
    File logFile = new File(binDir, "tiger.log")
    def sdi = new SubcorpusDataInject(
        xmltxm, 
        new FileHandler(logFile.getAbsolutePath()), "vers"
    )
    sdi.processMaster(tigerFile, masterpos)
    // Run reheader script
    def reheader = new MasterReheader()
    File tmp = File.createTempFile("tmp", ".xml",tigerDir)
    def feats = ['nt':['cat', 'type', 'coord'], 't':['pos', 'form', 'q']]
    def firstFeat = ['nt':'cat', 't':'word']
    reheader.script(masterpos, headerFile, tmp, feats, firstFeat)
    if (!tmp.exists()) {
    	println "Error: reheader failed"
    }
    masterpos.delete()
    tmp.renameTo(masterpos)
    // Run merge master & subcorpus script
    def tigerXmlAll = new File(masterpos.getParentFile(), "TigerAll.xml")
    def mergescript = new TigerSubcorpus2Main()
    mergescript.script(masterpos, tigerXmlAll)
    return tigerXmlAll
}

def doPnc(File tigerXmlAll, File txmFile) {
    injector = new PunctInjectImport(tigerXmlAll, txmFile)
    injector.outputFile = new File(tigerXmlAll.getParentFile(), "TigerPnc.xml")
    injector.process()
}