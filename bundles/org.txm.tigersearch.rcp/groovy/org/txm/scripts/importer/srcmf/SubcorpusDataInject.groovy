#! /usr/bin/groovy
package org.txm.scripts.importer.srcmf;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.logging.Logger
import java.util.logging.FileHandler
import java.util.logging.SimpleFormatter
import java.util.logging.Level
import groovy.xml.XmlParser

class SubcorpusDataInject {
	// Globals
	def textForm = 'prose'
	def TEI = null
	def wNodes = []
	def wNodesInQ = []
	// Set up namespaces
	static nsXml = new groovy.xml.Namespace('http://www.w3.org/XML/1998/namespace', 'xml')
	static nsTxm = new groovy.xml.Namespace('http://textometrie.org/1.0', 'txm')
	// Set up a logger
	static logger = Logger.getLogger('subcorpusDataInjectLog')
	// END GLOBALS

	SubcorpusDataInject (
	File xmlTxmFile,
	FileHandler fh,
	String newTextForm
	) {
		// Reset global values
		if (newTextForm) {
			textForm = newTextForm
		}
		// Set up logger
		def formatter = new SimpleFormatter()
		logger.addHandler(fh)
		logger.setLevel(Level.ALL)
		fh.setFormatter(formatter)
		// Parse the XML-TXM file
		logger.log(Level.INFO, "Parsing XML-TXM file (may take a while).")
		TEI = new XmlParser().parse(xmlTxmFile)
		// Get text form from TEI header (if expressed)
		logger.log(Level.INFO, "Reading TEI header to detect text form.")
		if (TEI.teiHeader.profileDesc.textClass.catRef) {
			def catId = TEI.teiHeader.profileDesc.textClass.catRef.'@target'[0][1..-1]
			textForm = TEI.teiHeader.encodingDesc.'**'.category.find {
				it.'@id' == catId || it.attribute(nsXml.id) == catId
			}.catDesc[0].text()
			logger.log(Level.INFO, "Found text form '$textForm' in TEI header.")
		} else {
			logger.log(Level.INFO, "No text form in TEI header, assuming '$textForm'.")
		}
		// Convert Lbs to Property
		if (textForm == 'vers' || textForm == 'mixte') {
			logger.log(Level.INFO, "Converting <lb/> tags to a word property for later processing.")
			def undone = TEI.text.body
			def lastLb = false
			def lastWord = null
			while (undone) {
				def aNode = undone.remove(0)
				if (aNode.name().getLocalPart() == 'lb') {
					lastLb = true
					if (lastWord) {
						lastWord.'@lb' = 'end'
					}
				} else if (aNode.name().getLocalPart() == 'w') {
					if (!punctTest(aNode)) {
						if (lastLb) {
							aNode.'@lb' = 'start'
						}
						lastWord = aNode
						lastLb = false
					}
				} else {
					undone.addAll(0, aNode.children().findAll { it.getClass() == Node })
				}
			}
		}
		// Build node lists
		logger.log(Level.INFO, "Building node lists.")
		wNodes = TEI.text.'**'.findAll { it.name().getLocalPart() == 'w' }
		wNodesInQ = TEI.text.'**'.q.'**'.findAll { it.name().getLocalPart() == 'w' }
	}

	static main(def args) {
		// Parse command line args
		def outputFile = 'test-out.xml'
		def logName = 'subcorpusDataInjectLog.log'
		def cli = new CliBuilder(
				usage:'SubcorpusDataInject.groovy [options] TXM_file.xml TS_file.xml'
				)
		cli.h(longOpt:'help', 'Prints this message')
		cli.m(longOpt:'master', 'Specifies input file is a TS master file.')
		cli.v(longOpt:'verse', 'Sets default text form to verse. Overridden by TEI header.')
		cli.l(longOpt:'logfile', args:1, argName:'logfile.log', 'Use given file for log.')
		cli.o(longOpt:'output', args:1, argName:'outputfile.xml', 'Output to given file')
		println args
		def options = cli.parse(args)
		// Call XML TXM loader
		if (options.arguments().size() == 2) {
			def arglist = [new File (options.arguments()[0])]
			if (options.logfile) {
				arglist.add(new FileHandler(options.logfile))
			} else {
				arglist.add(new FileHandler(logName))
			}
			if (options.v) {
				arglist.add('vers')
			} else {
				arglist.add('')
			}
			def sdi = new SubcorpusDataInject(arglist[0], arglist[1], arglist[2])
			// Call process master or process subcorpus
			arglist = [new File(options.arguments()[1]).getCanonicalFile()]
			if (options.output) {
				arglist.add(new File (options.output))
			} else {
				arglist.add(new File (outputFile))
			}
			if (options.m) {
				sdi.processMaster(arglist[0], arglist[1])
			} else {
				sdi.processSubcorpus(arglist[0], arglist[1])
			}
		} else {
			println "Wrong number of arguments."
			println cli.usage()
		}
	}

	def processMaster (File masterFile, File outputFile) {
		// Runs the transform on each subcorpus listed in a master file.
		// Resaves the master file.
		logger.log(Level.INFO, "Loading corpus master file")
		def masterFolder = ''
		def outputFolder = ''
		if (masterFile.getParent()) {
			// If not cwd
			masterFolder = masterFile.getParent()
		}
		if (outputFile.getParent()) {
			// If not cwd
			outputFolder = outputFile.getParent()
		}
		def master = new XmlParser().parse(masterFile)
		// Process each subcorpus file
		master.body.subcorpus.each {
			processSubcorpus(
					new File (masterFolder, it.'@external'[5..-1]),
					new File (outputFolder, "${it.'@external'[5..-5]}_pos.xml")
					)
			it.'@external' = "${it.'@external'[0..-5]}_pos.xml"
		}
		logger.log(Level.INFO, "Saving new master file ${outputFile.getName()}")

		outputFile.withWriter { writer ->
			writer << groovy.xml.XmlUtil.serialize(master)
		}
		logger.log(Level.INFO, "Run reheader on this file!")
	}

	def processSubcorpus (File subcorpusFile, File outputFile) {
		// Parse Subcorpus file
		logger.log(Level.INFO, "Loading subcorpus file ${subcorpusFile.getName()}")
		def TS = new XmlParser().parse(subcorpusFile)
		// This is used as a stack
		def tNodes = TS.s.graph.terminals.t
		// This isn't
		def tNodesFixed = TS.s.graph.terminals.t

		// Build duplicata correspondance list
		logger.log(Level.INFO, "Building duplicata correspondance list")
		def duplIdMatches = tNodes.findAll {it.'@word' == '*'}.collect { dupl ->
			def duplId = dupl.'@id'
			def realId = dupl.parent().parent().nonterminals.nt.edge.find {
				it.'@idref' == duplId
			}.parent().secedge.find {
				it.'@label' == 'dupl'
			}.'@idref'
			[duplId, realId]
		}
		logger.log(Level.INFO, "${duplIdMatches.size()} duplicatas found.")

		// Add pos & form tags
		logger.log(Level.INFO, "Adding PoS and form tags to each t-node")
		// Get first wNode corresponding to a tNode
		def i = 0
		def wIx = null
		while (i < tNodes.size()) {
			// Avoid trying to find "fake" first words, it's very inefficient
			if (tNodes[i].'@word' != '#') {
				wIx = wNodes.indexOf(wNodes.find { idmatch(tNodes[i], it) })
			}
			if (wIx) {
				break
			} else {
				i++
			}
		}
		assert wIx, "$i"
		/*
		 * MAIN ITERATOR BEGINS HERE FOR POS INJECTION
		 */
		// Iterate over tNodes.
		while (tNodes) {
			def thisTNode = tNodes.remove(0)
			def thisTNodeId = thisTNode.'@editionId'
			def wNode = null
			def q = false
			// Define the PoS / q / form injecting closure
			def injectPos = {
				// Q tag
				if (q) {
					thisTNode.'@q' = 'y'
				} else {
					thisTNode.'@q' = 'n'
				}
				// Form tag
				if (textForm == 'prose') {
					thisTNode.'@form' = 'prose'
				} else if (
				textForm == 'vers' ||
				(
				textForm == 'mixte'
				&& wNode.parent().parent().name().getLocalPart() == 'ab'
				)
				) {
					if (! wNode.'@lb') {
						thisTNode.'@form' = 'vers'
					} else if (wNode.'@lb' == 'start') {
						thisTNode.'@form' = 'vers_debut'
					} else if (wNode.'@lb' == 'end')  {
						thisTNode.'@form' = 'vers_fin'
					}
				}
				// DEBUG TMR 22/08/2012
				else {
					thisTNode.'@form' = 'prose'
				}
				// END DEBUG TMR 22/08/2012
				// Find PoS tag in BFM file
				// Try txm:ana tags
				def txmAnaList = wNode[nsTxm.ana]
				// Then interp tags
				if (! txmAnaList) {
					txmAnaList = wNode.interp
				}
				// Try type="#pos_syn"
				if (txmAnaList.find {it.'@type' == '#pos_syn'}) {
					thisTNode.'@pos' = txmAnaList.find {it.'@type' == '#pos_syn'}.text()
					// type attr of wNode
				} else if (! txmAnaList && wNode.'@type') {
					thisTNode.'@pos' = wNode.'@type'
					// type="#pos"
				} else if (txmAnaList.find { it.'@type' =  '#pos' }) {
					thisTNode.'@pos' = txmAnaList.find {it.'@type' == '#pos'}.text()
					// type="#fropos"
				} else if (txmAnaList.find { it.'@type' =  '#fropos' }) {
					thisTNode.'@pos' = txmAnaList.find {it.'@type' == '#fropos'}.text()
				} else {
					thisTNode.'@pos' = '--'
				}
			}
			// End of injecting closure.
			// Check whether wNode matches tNode
			if (thisTNode.'@word' == '#') {
				// Not expecting to find these in BFM file, all OK.
				thisTNode.'@pos' = '--'
				thisTNode.'@form' = '--'
				thisTNode.'@q' = '--'
			} else if ( idmatch(thisTNode, wNodes[wIx]) ) {
				// Perfect match
				wNode = wNodes[wIx]
				q = wNodesInQ.contains(wNode)
				injectPos.call()
				wIx++
			} else if (thisTNode.'@word' == '*') {
				// Duplicata, use dupl match list to find correct wNode
				def duplIdMatch = duplIdMatches.find { it[0] == thisTNode.'@id' }
				if (duplIdMatch) {
					def matchTNode = tNodesFixed.find { it.'@id' == duplIdMatch[1] }
					// To enhance performance, I assume initially that the wNode matching the
					// duplicata is within +- 10 words of the current position in the
					// list.  The wNodes list is very long, and searches over the entire
					// list are avoided.
					assert matchTNode, "$duplIdMatch"
					if (wIx + 10 < wNodes.size() && wIx - 10 > 0) {
						wNode = wNodes[wIx-10 .. wIx+10].find {
							idmatch(matchTNode, it)
						}
					}
					// Use whole list if it failed.
					if (! wNode) {
						wNode = wNodes.find {
							idmatch(matchTNode, it)
						}
					}
					q = wNodesInQ.contains(wNode)
					injectPos.call()
					// Don't update wIx value!
				} else {
					thisTNode.'@pos' = '--'
					thisTNode.'@form' = '--'
					thisTNode.'@q' = '--'
					logger.log(
							Level.WARNING,
							"<w/> node matching TS duplicata $thisTNodeId not found."
							)
				}
			}  else if (punctTest(wNodes[wIx])) {
				// not expecting to find these in TS file; try same tNode again.
				tNodes.add(0, thisTNode)
				wIx++
			} else if (!
			wNodes.find {
				idmatch (thisTNode, it)
			}
			) {
				// Check the SRCMF corpus hasn't acquired an extra word
				logger.log(
						Level.WARNING,
						"TS word $thisTNodeId (${thisTNode.'@word'}) is " +
						"absent from BFM file."
						)
				thisTNode.'@pos' = '--'
				thisTNode.'@form' = '--'
				thisTNode.'@q' = '--'
			} else {
				// it's doesn't match, it's not a duplicata,
				// it's not BFM punctuation, but it IS found somewhere in the BFM
				// corpus.
				logger.log(
						Level.WARNING,
						"Discontinuity in TS corpus before word $thisTNodeId " +
						"(${thisTNode.'@word'})."
						)
				wNode = wNodes.find {
					idmatch(thisTNode, it)
				}
				q = wNodesInQ.contains(wNode)
				injectPos.call()
				wIx = wNodes.indexOf(wNode) + 1
			}
		}
		/*
		 * End Main iteration
		 */
		/*******************
		 * Recalculate headpos 
		 */
		logger.log(Level.INFO, "Recalculating headpos.")
		TS.s.graph.nonterminals.nt.each { nt ->
			def a = nt.edge.findAll { it.'@label' == 'L' }
			if (a.size() == 0) {
				nt.'@headpos' = '--'
			} else if (a.size() == 1) {
				nt.'@headpos' = nt.parent().parent().terminals.t.find {
					it.'@id' == a[0].'@idref'
				}.'@pos'
			} else {
				for (def headpos : ['NOM', 'PRO', 'ADJ', 'VER', 'ADV', 'CON', 'PRE', 'DET']) {
					if (
					a.each { edge ->
						nt.parent().parent().terminals.t.find {
							it.'@id' == edge.'@idref'
						}.'@pos'
					}.contains(headpos)
					) {
						nt.'@headpos' = headpos + '?'
						break
					}
				}
			}
			assert nt.'@headpos'
		}
		logger.log(Level.INFO, "Saving new subcorpus file ${outputFile.getName()}")

		outputFile.withWriter { writer ->
			writer << groovy.xml.XmlUtil.serialize(TS)
		}
	}
	// Subroutine self-test to establish whether a Groovy node is punctuation
	private punctTest(gNode) {
		if (gNode.'@type') {
			gNode.'@type'.toLowerCase().startsWith('pon')
		} else {
			if (gNode[nsTxm.ana]) {
				return gNode[nsTxm.ana].find {it.text().toLowerCase().startsWith('pon')} != null
			} else if (gNode.interp) {
				return gNode.interp.find {it.text().toLowerCase().startsWith('pon')} != null
			} else return false
		}
	}

	private idmatch (tNode, wNode) {
		// Matches a TS node with a <w/> node
		def idMatch = false
		def wNodeId = wNode.'@id'
		if (! wNodeId) {
			wNodeId = wNode.attribute(nsXml.id)
		}
		def tNodeId = tNode.'@editionId'
		def regex = /(\d+)([_a-z]+)?$/
		def m1 = (tNodeId =~ regex)
		def m2 = (wNodeId =~ regex)
		if ( m1.find() && m2.find() ) {
			idMatch = (m1.group(1).toInteger() == m2.group(1).toInteger())
		} else {
			logger.log(
					Level.WARNING,
					"Idmatch routine can't find integer part of one or both of the" +
					"paired Ids for nodes $tNode, $wNode"
					)
		}
		if (! idMatch) {
			return false
		}
		
		//def wNodeForm = wNode.text() // Enable if treating old-style XML-BFM
		def wNodeForm = "";
		if (! wNodeForm) {
			wNodeForm = getDeepText(wNode[nsTxm.form][0])
		}
		def tNodeForm = tNode.'@word'
		def formMatch = (tNodeForm == wNodeForm)
		if (idMatch && formMatch) {
			return true
		} else {
			logger.log(Level.WARNING,
					"Matching IDs ($tNodeId) but unmatched forms: TS form '$tNodeForm', BFM form '$wNodeForm'.")
			return true
		}
	}
	
	def getDeepText(def node) {
		def str = ""
		node.children().each { child ->
			if (child instanceof Node) {
				str += getDeepText(child)
			} else if (child instanceof String) {
				str += child
			}
		}
		return str
	}
}