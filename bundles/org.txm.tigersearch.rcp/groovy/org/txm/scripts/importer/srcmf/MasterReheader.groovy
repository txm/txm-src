#! /usr/bin/groovy
package org.txm.scripts.importer.srcmf;

import groovy.xml.XmlParser

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

// Set up globals
// def masterFile = new File('/home/tomr/Documents/Work/lyon12/srcmf/groovy/SrcmfImport/aucassin_surface/master_pos.xml')
// def outputFile = new File('/home/tomr/Documents/Work/lyon12/srcmf/groovy/SrcmfImport/aucassin_surface/master_pos2.xml')
// def headerFile = new File('/home/tomr/Documents/Work/SRCMF/srcmf_ts/header_srcmf.xml')
// def feats = [nt:['cat', 'type', 'coord'], t:['pos', 'form', 'q']]
// def firstFeat = [t:'word', nt:'cat']

// Command-line entry point
def cli = new CliBuilder(
    usage:'MasterReheader.groovy [options] master_file.xml header_file.xml'
)
cli.h(longOpt:'help', 'Prints this message.')
cli.o(longOpt:'output', args:1, argName:'outputfile.xml', 'Output to given file.')
cli.nt(longOpt:'ntfeats', args:1, argName:'ntfeats', 'NT features for which to provide value node.')
cli.t(longOpt:'tfeats', args:1, argName:'tfeats', 'T features for which  to provide value node.')
cli.nt1(longOpt:'ntfeat1st', args:1, argName:'1st ntfeat', 'First NT feature listed in header.')
cli.t1(longOpt:'tfeat1st', args:1, argName:'1st tfeat', 'First T feature listed in header.')
options = cli.parse(args)
if (options.arguments().size() == 2) {
    def masterFile = new File(options.arguments()[0])
    def headerFile = new File(options.arguments()[1])
    def masterFolder = masterFile.getCanonicalFile().getParent()
    def outputFile = null
    if (options.o) {
        outputFile = new File(options.o)
    } else {
        outputFile = new File(masterFolder, 'MasterReheader_out.xml')
    }
    def ntfirst = 'cat'
    if (options.nt1) {
        ntfirst = options.nt1
    }
    def tfirst = 'word'
    if (options.t1) {
        tfirst = options.t1
    }
    script(
        masterFile, headerFile, outputFile, 
        ['nt':options.nts, 't':options.ts],
        ['nt':options.nt1, 't':options.t1]
    )    
} else {
    println 'Incorrect number of command line arguments... exiting'
    println cli.usage()
}

def script(
    File masterFile, File headerFile, File outputFile, HashMap feats, HashMap firstFeat
) {
    // Load master and header files
    def master = new XmlParser().parse(masterFile)
    def header = new XmlParser().parse(headerFile)
    def masterFolder = masterFile.getCanonicalFile().getParent()

    // Set up locals
    def attrVal = [nt:[:], t:[:]]

    // Scan subcorpus files and build attribute lists.
    master.body.subcorpus.each { 
        def subcorpusFile = new File (masterFolder, it.'@external'[5..-1])
        def subcorpus = new XmlParser().parse(subcorpusFile)
        // Closure for t & nt nodes processing.
        def getvals = { node, type ->
            node.attributes().each { mEntry ->
                if (! attrVal[type].keySet().contains(mEntry.getKey())) {
                    attrVal[type][mEntry.getKey()] = new HashSet()
                }
                attrVal[type][mEntry.getKey()].add(mEntry.getValue())
            }
        }
        subcorpus.s.graph.terminals.t.each { getvals.call(it, 't') }
        subcorpus.s.graph.nonterminals.nt.each { getvals.call(it, 'nt') }
    }
    // Id isn't an attribute in the header.
    attrVal['t'].remove('id')
    attrVal['nt'].remove('id')
    // Remove old feature nodes in master file
    def oldFeatureNodes = master.head.annotation.feature
    while (oldFeatureNodes) {
        node = oldFeatureNodes.pop()
        node.parent().remove(node)
    }
    assert (! master.head.annotation.feature)
    // Check firstFeat was relevant
    ['t', 'nt'].each { type ->
        if (! (attrVal[type].keySet().contains(firstFeat[type]))) {
            firstFeat[type] = attrVal[type].keySet().sort()[0]
        }
    }
    assert attrVal['t'].keySet().contains(firstFeat['t'])
    assert attrVal['nt'].keySet().contains(firstFeat['nt'])
    def featList = [:]
    ['t', 'nt'].each { type ->
        featList[type] = [firstFeat[type]]
        featList[type].addAll(attrVal[type].keySet().findAll { it != firstFeat[type] })
    }
    // Add new feature and value nodes
    ['t', 'nt'].each { type ->
        featList[type].each { feat ->
            def fNode = new Node(master.head.annotation[0], 'feature', 
                ['domain':type.toUpperCase(), 'name':feat]
            )
            // Add value node if the node value is given in 'feats'
            if (feats[type].contains(feat)) {
                attrVal[type][feat].each { value ->
                    assert header.'**'.feature
                    assert header.'**'.feature[0].'@name'
                    assert header.'**'.feature[0].'@domain'
                    assert ['NT', 'T'].contains(header.'**'.feature[0].'@domain')
                    def hFNode = header.'**'.feature.find {
                        it.'@name' == feat && (
                            it.'@domain' == type.toUpperCase() || it.'@domain' == 'FREC'
                        )
                    }
                    def vText = '[unknown]'
                    if (hFNode && hFNode.value.find { it.'@name' == value }) {
                        vText = hFNode.value.find { it.'@name' ==  value }.text()
                    }
                    new Node(fNode, 'value', ['name':value], vText)
                }
            }
        }    
    }

    // Save to output_file
    outputFile.withWriter { writer ->
        writer << groovy.xml.XmlUtil.serialize(master)
    }
}

