// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.tigersearch.editors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.txm.rcp.swt.widget.AssistedQueryWidget;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

/**
 * the QueryWidget plus the button to open the QueryAssisDialog
 * 
 * @author mdecorde
 * 
 */
public class TSAssistedQueryWidget extends AssistedQueryWidget {

	/**
	 * Instantiates a new assisted query widget.
	 *
	 * @param parent the parent
	 * @param style the style
	 * @param corpus the corpus
	 */
	public TSAssistedQueryWidget(Composite parent, int style, final CQPCorpus corpus) {
		super(parent, SWT.NONE, corpus);
	}
}
