package org.txm.tigersearch.editors;

import java.util.HashSet;
import java.util.Set;

import org.txm.core.results.TXMResult;
import org.txm.function.tigersearch.TIGERSearch;
import org.txm.index.core.functions.Index;
import org.txm.rcp.editors.TXMEditorExtension;

public class IndexExtension extends TXMEditorExtension<Index> {

	@Override
	public String getName() {
		return "TIGERSearch"; //$NON-NLS-1$
	}

	@Override
	public void notifyStartOfCreatePartControl() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void notifyEndOfCreatePartControl() throws Exception {
		// add TS query engine selector

	}

	@Override
	public void notifyStartOfCompute() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void notifyEndOfCompute() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void notifyStartOfRefresh() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void notifyEndOfRefresh() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isDirty() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void notifyDoSave() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void notifyDispose() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public Set<Class<? extends TXMResult>> getTXMResultValidClasses() {
		// TODO Auto-generated method stub
		HashSet<Class<? extends TXMResult>> h = new HashSet<Class<? extends TXMResult>>();
		h.add(TIGERSearch.class);
		return h;
	}

	@Override
	public boolean isSaveOnCloseNeeded() throws Exception {
		return true;
	}

}
