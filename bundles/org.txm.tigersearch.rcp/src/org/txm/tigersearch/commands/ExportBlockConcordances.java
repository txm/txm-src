// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.tigersearch.commands;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.kohsuke.args4j.Option;
import org.txm.function.tigersearch.TIGERSearch;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.utils.logger.Log;

/**
 * Computes blocks concordances from a TIGERSearch result
 * 
 * @author mdecorde.
 */
public class ExportBlockConcordances extends AbstractHandler {

	@Option(name = "csvFile", widget = "CreateFile", usage = "CSV resutl file", def = "result.csv")
	File csvFile;

	@Option(name = "method", widget = "StringArray", metaVar = "concordance_simple	concordance_mot-pivot	concordance_blocks", usage = "Concordance method", def = "concordance_simple")
	String method;

	@Option(name = "ntTypes", widget = "StringArrayMultiple", metaVar = "cat	coord	dom	type	vform	vlemma	note	snr", usage = "NT properties to use", def = "cat")
	String ntTypes;

	@Option(name = "tTypes", widget = "StringArrayMultiple", metaVar = "word	pos	mor	lemma", usage = "T properties to use", def = "word")
	String tTypes;

	@Option(name = "cx", widget = "Integer", usage = "Contexts size", def = "10")
	Integer cx = 0;

	@Option(name = "injectionPunctuations", widget = "Boolean", usage = "Reinject punctuation from the TIGERXML source file", def = "false")
	Boolean injectionPunctuations;

	public static final String ID = ExportBlockConcordances.class.getName(); //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {

		if (CorporaView.getFirstSelectedObject() instanceof TIGERSearch tsResult) {
			export(this, tsResult);
		}
		else {
			Log.warning(NLS.bind("Selection is not a TIGERSearch result: {0}. Aborting.", CorporaView.getFirstSelectedObject()));
		}
		return null;
	}

	public static void export(ExportBlockConcordances bean, TIGERSearch tsResult) {

		if (ParametersDialog.open(bean)) {

			File csvFile = bean.csvFile;
			String method = bean.method;
			List<String> ntTypes = Arrays.asList(bean.ntTypes.split("\t"));
			List<String> tTypes = Arrays.asList(bean.tTypes.split("\t"));
			int cx = bean.cx;
			boolean injectionPunctuations = bean.injectionPunctuations;

			try {
				tsResult.getTSResult().toConcordance(csvFile, method, cx, ntTypes, tTypes, injectionPunctuations);
				Log.info("Done. See: " + csvFile.getAbsolutePath());
			}
			catch (Exception e) {
				Log.severe("Error: " + e);
				e.printStackTrace();
			}
		}
		else {
			Log.info("Aborted by user.");
		}
	}

}
