// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.tigersearch.commands;

import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.function.tigersearch.TSIndex;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.serverException.CqiServerError;


// TODO: Auto-generated Javadoc
/**
 * open the TIGERSearch Editor
 * 
 * @author mdecorde.
 */
public class ComputeRecette1 extends AbstractHandler {

	public static final String ID = "org.txm.rcp.commands.function.ComputeTSIndex"; //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {

		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);

		Object s = selection.getFirstElement();
		if (s instanceof CQPCorpus) {
			CQPCorpus corpus = (CQPCorpus) s;
			try {
				compute(corpus);
			}
			catch (CqiClientException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (CqiServerError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public static void compute(CQPCorpus corpus) throws CqiClientException, IOException, CqiServerError {
		System.out.println("****** RECETTE 1 ******");

		CQLQuery query = new CQLQuery("#pivot:[word=/[Cc][ou]m?me?/ & pos=\"CONsub\"]");
		ArrayList<WordProperty> props = new ArrayList<WordProperty>();
		props.add(corpus.getProperty("word")); //$NON-NLS-1$

		TSIndex tsi = null;

		System.out.println("TSIndex with " + query + " and " + corpus);
		tsi = new TSIndex(corpus, query, props);
		System.out.println("T=" + tsi.getT()); //$NON-NLS-1$
		System.out.println("V=" + tsi.getV()); //$NON-NLS-1$
		System.out.println("5 first lines: " + tsi.getLines(0, 5));

		CQLQuery query2 = new CQLQuery("[headpos=\"VERcjg\" & cat=\"Circ\"] >R #n:[cat=\"RelNC\"] & #n >L #pivot");
		System.out.println("TSIndex with " + query2 + " and " + corpus);
		tsi = new TSIndex(corpus, query2, props);
		System.out.println("T=" + tsi.getT()); //$NON-NLS-1$
		System.out.println("V=" + tsi.getV()); //$NON-NLS-1$
		System.out.println("5 first lines: " + tsi.getLines(0, 5));

		Subcorpus sub = corpus.createSubcorpus(new CQLQuery("[q] expand to q"), "Q", true); //$NON-NLS-1$

		System.out.println("TSIndex with " + query + " and " + sub);
		tsi = new TSIndex(sub, query, props);
		System.out.println("T=" + tsi.getT()); //$NON-NLS-1$
		System.out.println("V=" + tsi.getV()); //$NON-NLS-1$
		System.out.println("5 first lines: " + tsi.getLines(0, 5));

		System.out.println("TSIndex with " + query2 + " and " + sub);
		tsi = new TSIndex(sub, query2, props);
		System.out.println("T=" + tsi.getT()); //$NON-NLS-1$
		System.out.println("V=" + tsi.getV()); //$NON-NLS-1$
		System.out.println("5 first lines: " + tsi.getLines(0, 5));

		Subcorpus sub2 = corpus.createSubcorpus(new CQLQuery("[!q]"), "NOTQ", true); //$NON-NLS-1$

		System.out.println("TSIndex with " + query + " and " + sub2);
		tsi = new TSIndex(sub2, query, props);
		System.out.println("T=" + tsi.getT());
		System.out.println("V=" + tsi.getV());
		System.out.println("5 first lines: " + tsi.getLines(0, 5));

		System.out.println("TSIndex with " + query2 + " and " + sub2);
		tsi = new TSIndex(sub2, query2, props);
		System.out.println("T=" + tsi.getT());
		System.out.println("V=" + tsi.getV());
		System.out.println("5 first lines: " + tsi.getLines(0, 5));

		System.out.println("****** RECETTE 1 - END ******"); //$NON-NLS-1$
	}
}
