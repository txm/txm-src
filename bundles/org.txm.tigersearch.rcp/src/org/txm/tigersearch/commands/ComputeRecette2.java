// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.tigersearch.commands;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.function.tigersearch.TSIndex;
import org.txm.index.core.functions.Line;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.logger.Log;


// TODO: Auto-generated Javadoc
/**
 * open the TIGERSearch Editor
 * 
 * @author mdecorde.
 */
public class ComputeRecette2 extends AbstractHandler {

	public static final String ID = "org.txm.rcp.commands.function.ComputeTSIndex"; //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {

		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);

		List<?> list = selection.toList();

		if (list.size() != 2) {
			System.out.println("Error, you must select 2 TS ready corpus.");
			return false;
		}

		Object o1 = list.get(0);
		Object o2 = list.get(1);

		if (!(o1 instanceof CQPCorpus)) {
			System.out.println("Error, first item is not a corpus: " + o1);
			return false;
		}

		if (!(o2 instanceof CQPCorpus)) {
			System.out.println("Error, second item is not a corpus: " + o2);
			return false;
		}

		try {
			compute((CQPCorpus) o1, (CQPCorpus) o2);
		}
		catch (Exception e) {
			System.out.println("Recette 2 failed with error: " + e);
			Log.printStackTrace(e);
		}

		return null;
	}

	public static void compare(String C1, String C2, List<Line> lines1, List<Line> lines2) {
		System.out.println(C1 + "\t" + C2);
		Collections.sort(lines1, new Comparator<Line>() {

			@Override
			public int compare(Line o1, Line o2) {
				return o1.getUnitsProperties().toString().compareTo(o2.getUnitsProperties().toString());
			}
		});
		Collections.sort(lines2, new Comparator<Line>() {

			@Override
			public int compare(Line o1, Line o2) {
				return o1.getUnitsProperties().toString().compareTo(o2.getUnitsProperties().toString());
			}
		});

		int j = 0;
		int i = 0;
		while (i < lines1.size() && j < lines2.size()) {
			Line l1 = lines1.get(i);
			Line l2 = lines2.get(j);
			String s1 = l1.getUnitsProperties().toString();
			String s2 = l2.getUnitsProperties().toString();
			//System.out.println(s1+"\t"+s2);

			if (s1.equals(s2)) {
				System.out.println(s1 + "\t" + lines1.get(i).getFrequency() + "\t" + lines2.get(j).getFrequency());
				i++;
				j++;
			}
			else if (s1.compareTo(s2) > 0) {
				System.out.println(s1 + "\t" + lines1.get(i).getFrequency() + "\t0");
				i++;
			}
			else {
				System.out.println(s2 + "\t0\t" + lines2.get(j).getFrequency());
				j++;
			}
		}
		for (int c = i; c < lines1.size(); c++) {
			System.out.println(lines1.get(c).getUnitsProperties() + "\t" + lines1.get(c).getFrequency() + "\t0");
		}
		for (int c = j; c < lines2.size(); c++) {
			System.out.println(lines2.get(c).getUnitsProperties() + "\t0\t" + lines2.get(c).getFrequency());
		}
	}

	public static void compute(CQPCorpus corpus1, CQPCorpus corpus2) throws CqiClientException, IOException, CqiServerError {
		System.out.println("****** RECETTE 2 ******");

		CQLQuery query = new CQLQuery("#pivot:[word=/[Cc][ou]m?me?/ & pos=\"CONsub\"]");
		ArrayList<WordProperty> props = new ArrayList<WordProperty>();
		props.add(corpus1.getProperty("id"));

		TSIndex tsi = null, tsi2 = null;

		System.out.println("TSIndex with " + query + " and " + corpus1);
		tsi = new TSIndex(corpus1, query, props);
		System.out.println("T=" + tsi.getT());
		System.out.println("V=" + tsi.getV());
		System.out.println("5 first lines: " + tsi.getAllLines());

		System.out.println("TSIndex with " + query + " and " + corpus2);
		props = new ArrayList<WordProperty>();
		props.add(corpus2.getProperty("id"));
		tsi2 = new TSIndex(corpus2, query, props);
		System.out.println("T=" + tsi2.getT());
		System.out.println("V=" + tsi2.getV());
		System.out.println("5 first lines: " + tsi2.getAllLines());

		compare(corpus1.getName(), corpus2.getName(), tsi.getAllLines(), tsi2.getAllLines());

		System.out.println("****** RECETTE 2 - END ******");
	}
}
