// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.tigersearch.commands;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.BasicConfigurator;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.searchengine.ts.TIGERSearchEngine;
import org.txm.searchengine.ts.TSCorpus;
import org.txm.tigersearch.preferences.TigerSearchPreferences;
import org.txm.utils.DeleteDir;
import org.txm.utils.OSDetector;
import org.txm.utils.io.FileCopy;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

import ims.tiger.index.reader.IndexException;
import ims.tiger.index.writer.IndexBuilderErrorHandler;
import ims.tiger.index.writer.SimpleErrorHandler;
import ims.tiger.index.writer.XMLIndexing;
import ims.tiger.query.api.QueryIndexException;

/**
 * Import TIGERSearch annotations into a TXM corpus
 * 
 * IF the corpus alreasy wontains TIGER annotations, they are replaced
 * 
 * The annotations are given using a TIGERSEarch binary corpus OR a TIGER source directory using a "main.xml" file
 * 
 * @author mdecorde.
 */
public class ImportTIGERAnnotations extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {

		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);

		Object s = selection.getFirstElement();
		if (s instanceof MainCorpus) {
			CQPCorpus corpus = (CQPCorpus) s;
			MainCorpus mainCorpus = corpus.getMainCorpus();

			File tigerCorpusDirectory = null;
			DirectoryDialog dialog = new DirectoryDialog(HandlerUtil.getActiveShell(event), SWT.OPEN);
			String path = dialog.open();
			if (path == null) {
				Log.warning("Aborting annotation importation.");
				return null;
			}
			else {
				tigerCorpusDirectory = new File(path);
			}

			File tigerDirectory = new File(mainCorpus.getProjectDirectory(), "tiger"); //$NON-NLS-1$
			File tigerCorpusExistingDirectory = new File(tigerDirectory, tigerCorpusDirectory.getName());
			if (tigerCorpusExistingDirectory.exists()) {
				boolean doIt = MessageDialog.openConfirm(HandlerUtil.getActiveShell(event), "Replace existing annotations", "TIGERSearch annotations already exists, replace them ?");
				if (!doIt) {
					Log.warning("Aborting annotation importation.");
					return null;
				}
			}

			if (new File(tigerCorpusDirectory, "word.lexicon").exists() && new File(tigerCorpusDirectory, "corpus_config.xml").exists()) { //$NON-NLS-1$
				// ok this is a TIGERSearch binary corpus, use it
			}
			else {
				// need to build a TIGERSearch binary corpus
				File tigerBinaryCorpusDirectory = new File(tigerCorpusDirectory, "tiger"); //$NON-NLS-1$
				if (!buildTIGERBinaryCorpus(mainCorpus, tigerCorpusDirectory, tigerBinaryCorpusDirectory)) {
					Log.warning("Aborting annotation importation.");
					return null;
				}
				tigerCorpusDirectory = new File(tigerBinaryCorpusDirectory, corpus.getName());
			}

			try {
				return importTIGERBinaryCorpus(mainCorpus, tigerCorpusDirectory, "editionId"); //$NON-NLS-1$
			}
			catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		else {
			Log.warning("Selection is not a corpus. Aborting.");
			return null;
		}
	}

	public static boolean buildTIGERBinaryCorpus(MainCorpus corpus, File sourceDirectory, File tigerDir) {

		tigerDir.mkdirs();

		File configfile = new File(tigerDir, "tigersearch.logprop"); //$NON-NLS-1$
		if (!configfile.exists()) {
			TSCorpus.createLogPropFile(tigerDir);
		}

		BasicConfigurator.configure();
		File master = new File(sourceDirectory, TigerSearchPreferences.DRIVER_FILENAME);

		if (!master.exists()) {
			Log.warning("Error: Can't create TIGERSearch corpus: driver file found: " + master);
			return false;
		}
		else {
			Log.warning("Using the driver file found: " + master + ". Be sure text order is the same as the CQP corpus!");
		}

		String uri = master.getAbsolutePath(); // TIGER corpus source root file
		File tigerBinDir = new File(tigerDir, corpus.getName());
		tigerBinDir.mkdirs();
		try {
			IndexBuilderErrorHandler handler = new SimpleErrorHandler(tigerBinDir.getAbsolutePath()) {

				@Override
				public void setMessage(String message) {
				}

				@Override
				public void setNumberOfSentences(int number) {
				}

				@Override
				public void setProgressBar(int value) {
				}
			};

			XMLIndexing indexing = new XMLIndexing(corpus.getName(), uri, tigerBinDir.getAbsolutePath(), handler, false);

			indexing.startIndexing();

			File logs = new File(tigerBinDir, "indexing.log"); //$NON-NLS-1$

			String txt = IOUtils.getText(logs);
			if (txt.contains("Error in corpus graph ")) {
				Log.warning("Error while importing TIGER corpus: " + txt);
				return false;
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * 
	 * if aTIGER corpus with the same name already exists, it is replaced
	 * 
	 * @param corpus
	 * @param tigerCorpusDirectory
	 * @return the number of imported annotations
	 * @throws IndexException
	 * @throws QueryIndexException
	 * @throws CqiClientException
	 * @throws CqiServerError
	 * @throws IOException
	 * @throws UnexpectedAnswerException
	 */
	public static int importTIGERBinaryCorpus(MainCorpus corpus, File tigerCorpusDirectory, String wordIdAttribute) throws IndexException, QueryIndexException, UnexpectedAnswerException, IOException,
			CqiServerError,
			CqiClientException {

		File tigerDirectory = new File(corpus.getProjectDirectory(), "tiger"); //$NON-NLS-1$
		File tigerCorpusExistingDirectory = new File(tigerDirectory, corpus.getName());
		DeleteDir.deleteDirectory(tigerCorpusExistingDirectory);
		tigerCorpusExistingDirectory.mkdirs();

		File configfile = new File(tigerDirectory, "tigersearch.logprop"); //$NON-NLS-1$
		if (!configfile.exists()) {
			TSCorpus.createLogPropFile(tigerDirectory);
		}

		int numberOfWordsAnnotated = TIGERSearchEngine.writeOffsetDataFiles(corpus, wordIdAttribute, tigerCorpusDirectory, tigerDirectory, tigerCorpusExistingDirectory);

		Log.info("Finalizing TIGERSearch corpus");
		if (numberOfWordsAnnotated > 0) { // copy the TIGERcorpus to import
			FileCopy.copyFiles(tigerCorpusDirectory, tigerCorpusExistingDirectory);

			corpus.getProject().appendToLogs("TIGER Annotations imported from " + tigerDirectory + " by " + OSDetector.getUserAndOSInfos());

			Log.info("Done. " + numberOfWordsAnnotated + " words annotated.");
		}
		else {
			Log.warning("Warning: no words could be aligned with the CQP corpus. Aborting");
		}

		return numberOfWordsAnnotated;
	}

}
