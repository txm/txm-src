package org.txm.tigersearch.rcp.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.tigersearch.preferences.TigerSearchTreePreferences;

/**
 * TIGERSearch preferences page
 * 
 * @author mdecorde
 *
 */
public class TigerSearchTreePreferencePage extends TXMPreferencePage {

	@Override
	public void createFieldEditors() {

		this.addField(new StringFieldEditor(TigerSearchTreePreferences.TFEATURE, "Default T feature to show ", this.getFieldEditorParent()));

		this.addField(new StringFieldEditor(TigerSearchTreePreferences.NTFEATURE, "Default NT feature to show", this.getFieldEditorParent()));

		this.addField(new BooleanFieldEditor(TigerSearchTreePreferences.USESUBMATCHES, "Use sub matches", this.getFieldEditorParent()));
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {

		this.setPreferenceStore(new TXMPreferenceStore(TigerSearchTreePreferences.getInstance().getPreferencesNodeQualifier()));
		this.setImageDescriptor(IImageKeys.getImageDescriptor(this.getClass(), "icons/functions/TS.png")); //$NON-NLS-1$
	}
}
