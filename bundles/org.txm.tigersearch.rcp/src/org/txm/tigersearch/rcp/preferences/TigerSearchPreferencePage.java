package org.txm.tigersearch.rcp.preferences;

import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.tigersearch.preferences.TigerSearchPreferences;

/**
 * TIGERSearch preferences page
 * 
 * @author mdecorde
 *
 */
public class TigerSearchPreferencePage extends TXMPreferencePage {

	@Override
	public void createFieldEditors() {

		this.addField(new StringFieldEditor(TigerSearchPreferences.DRIVER_FILENAME, "TIGER-XML driver filename during import", this.getFieldEditorParent()));

		this.addField(new StringFieldEditor(TigerSearchPreferences.FEATURE_VALUES_TO_IGNORE_IN_HEADER, "Comma separated list of feature values to not declare in the TIGER header during import",
				this.getFieldEditorParent()));
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {

		this.setPreferenceStore(new TXMPreferenceStore(TigerSearchPreferences.getInstance().getPreferencesNodeQualifier()));

		this.setImageDescriptor(IImageKeys.getImageDescriptor(this.getClass(), "icons/functions/TS.png")); //$NON-NLS-1$
	}
}
