package org.txm.tigersearch.rcp;

import java.io.File;
import java.util.Calendar;

import org.txm.PostTXMHOMEInstallationStep;
import org.txm.Toolbox;
import org.txm.groovy.core.InstallGroovyFiles;
import org.txm.objects.Workspace;
import org.txm.utils.BundleUtils;
import org.txm.utils.DeleteDir;

public class InstallGroovyTIGERFiles extends PostTXMHOMEInstallationStep {

	final static String createfolders[] = { "scripts/groovy/lib", "scripts/groovy/user", "scripts/groovy/system" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	@Override
	public boolean do_install(Workspace workspace) {
		File txmhomedir = new File(Toolbox.getTxmHomePath());

		for (String folder : createfolders) {
			new File(txmhomedir, folder).mkdirs();
		}
		File scriptsDirectory = new File(txmhomedir, "scripts/groovy"); //$NON-NLS-1$
		File userDirectory = new File(scriptsDirectory, "user"); //$NON-NLS-1$
		File systemDirectory = new File(scriptsDirectory, "system"); //$NON-NLS-1$

		String bundle_id = "org.txm.tigersearch.rcp"; //$NON-NLS-1$

		// IMPORT MODULE SCRIPTS

		File scriptsPackageDirectory = new File(userDirectory, "org/txm/scripts/importer"); //$NON-NLS-1$
		File scriptsPackageDirectory2 = new File(systemDirectory, "org/txm/scripts/importer"); //$NON-NLS-1$

		File tigerDirectory = new File(scriptsPackageDirectory, "tigersearch"); //$NON-NLS-1$
		if (tigerDirectory.exists()) {
			File backDirectory = new File(tigerDirectory.getParentFile(), "tigersearch-" + Toolbox.dateformat.format(Calendar.getInstance().getTime())); //$NON-NLS-1$
			//System.out.println("Making a copy of previous TIGER import scripts directory: "+tigerDirectory+" to "+backDirectory);
			tigerDirectory.renameTo(backDirectory);
			DeleteDir.deleteDirectory(tigerDirectory);
		}

		scriptsPackageDirectory.mkdirs();
		scriptsPackageDirectory2.mkdirs();
		BundleUtils.copyFiles(bundle_id, "groovy", "org/txm/scripts/importer", "", scriptsPackageDirectory, true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		BundleUtils.copyFiles(bundle_id, "groovy", "org/txm/scripts/importer", "", scriptsPackageDirectory2, true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		// MACROS
		File macrosPackageDirectory = new File(userDirectory, "org/txm/macro/tiger"); //$NON-NLS-1$
		File macrosPackageDirectory2 = new File(systemDirectory, "org/txm/macro/tiger"); //$NON-NLS-1$

		if (macrosPackageDirectory.exists()) {
			//			File backDirectory = new File(scriptsPackageDirectory.getParentFile(), "tiger-"+Toolbox.dateformat.format(Calendar.getInstance().getTime()));
			//			Log.info("Making a copy of previous TIGER import scripts directory: "+tigerDirectory+" to "+backDirectory);
			//			scriptsPackageDirectory.renameTo(backDirectory);

			InstallGroovyFiles.backupFiles(macrosPackageDirectory, macrosPackageDirectory2, userDirectory.getParentFile());

			DeleteDir.deleteDirectory(macrosPackageDirectory);
		}

		macrosPackageDirectory.mkdirs();
		macrosPackageDirectory2.mkdirs();
		BundleUtils.copyFiles(bundle_id, "groovy", "org/txm/macro/tiger", "", macrosPackageDirectory, true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		BundleUtils.copyFiles(bundle_id, "groovy", "org/txm/macro/tiger", "", macrosPackageDirectory2, true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		return macrosPackageDirectory.exists();
	}

	@Override
	public String getName() {
		return "TIGERSearch (org.txm.tigersearch.rcp)"; //$NON-NLS-1$
	}

	@Override
	public boolean needsReinstall(Workspace workspace) {
		File txmhomedir = new File(Toolbox.getTxmHomePath());
		for (String folder : createfolders) {
			if (!new File(txmhomedir, folder).exists()) {
				return true;
			}
		}
		return false;
	}
}
