package org.txm.tigersearch.rcp;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.tigersearch.rcp.messages"; //$NON-NLS-1$

	public static String ErrorCantFindCorpusDirectoryP0;

	public static String ErrorInTSIndexNoLabelEtc;

	public static String ErrorInTSIndexTooManyLabelsEtc;

	public static String ErrorWhilePropertiesProjectionP0;

	public static String ErrorWhileQueingTIGERSearchP0;

	public static String FailToRenderSVGMatchForSentEqualsP0AndSubEqualsP1;

	public static String TIGERSearchCorpusNotFoundInP0;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
