package org.txm.tigersearch.rcp;

import java.io.File;

import org.txm.Toolbox;
import org.txm.rcp.menu.MacrosMenuContribution;

public class TSMacrosMenuContribution extends MacrosMenuContribution {

	public File getMacroDirectory() {
		//create the menu item
		String w = Toolbox.getTxmHomePath();
		if (w == null || w.length() == 0) return null;

		return new File(w, "scripts/groovy/user/org/txm/macro/tiger"); //$NON-NLS-1$
	}
}
