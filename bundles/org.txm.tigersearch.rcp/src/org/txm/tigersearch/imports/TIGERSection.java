package org.txm.tigersearch.imports;


import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.osgi.service.prefs.Preferences;
import org.txm.objects.Project;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.txm.rcp.editors.imports.sections.ImportEditorSection;

public class TIGERSection extends ImportEditorSection {

	String ID = TIGERSection.class.getSimpleName();

	private static final int SECTION_SIZE = 1;

	/**
	 * 
	 * @param toolkit2
	 * @param form2
	 * @param parent
	 */
	public TIGERSection(ImportFormEditor editor, FormToolkit toolkit2, ScrolledForm form2, Composite parent, int style) {

		super(editor, toolkit2, form2, parent, style, "TIGERSearch"); //$NON-NLS-1$

		TableWrapLayout layout = new TableWrapLayout();
		layout.makeColumnsEqualWidth = true;
		layout.numColumns = 1;
		this.section.setLayout(layout);
		this.section.setLayoutData(getSectionGridData(SECTION_SIZE));
		this.section.setEnabled(false);

		this.section.addExpansionListener(new ExpansionAdapter() {

			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.layout(true);
			}
		});

		// filesection.setDescription("Select how to find source files");
		Composite sectionClient = toolkit.createComposite(this.section);
		TableWrapLayout slayout = new TableWrapLayout();
		slayout.numColumns = 4;
		sectionClient.setLayout(slayout);
		this.section.setClient(sectionClient);

		//		headPropertiesText = toolkit.createText(sectionClient, "UD head properties to project (comma separated list)", SWT.CHECK);
		//		gdata2 = getButtonLayoutData();
		//		gdata2.colspan = 4; // one line
		//		headPropertiesText.setLayoutData(gdata2);
		//		
		//		// build text edition or not button
		//		depsPropertiesText = toolkit.createText(sectionClient, "UD deps properties to project (comma separated list)", SWT.CHECK);
		//		gdata2 = getButtonLayoutData();
		//		gdata2.colspan = 4; // one line
		//		depsPropertiesText.setLayoutData(gdata2);
	}

	@Override
	public void updateFields(Project project) {
		if (this.section.isDisposed()) return;
		if (project == null) return;

		Preferences customNode = project.getImportParameters().node("tigersearch"); //$NON-NLS-1$
		//		createPagesForSectionsButton.setSelection(customNode.getBoolean("create_section_pages", true)); //$NON-NLS-1$
		//		indexTranscriberMetadataButton.setSelection(customNode.getBoolean("ignoreTranscriberMetadata", true)); //$NON-NLS-1$
	}

	@Override
	public boolean saveFields(Project project) {
		if (this.section != null && !this.section.isDisposed()) {

			Preferences customNode = project.getImportParameters().node("tigersearch"); //$NON-NLS-1$
			//			customNode.putBoolean("create_section_pages", createPagesForSectionsButton.getSelection()); //$NON-NLS-1$
			//			customNode.putBoolean("ignoreTranscriberMetadata", indexTranscriberMetadataButton.getSelection()); //$NON-NLS-1$
		}
		return true;
	}

	@Override
	public boolean checkFields() {
		return true;
	}

	@Override
	public int getSectionSize() {
		return SECTION_SIZE;
	}
}
