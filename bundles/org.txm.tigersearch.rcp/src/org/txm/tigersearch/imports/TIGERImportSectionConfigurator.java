package org.txm.tigersearch.imports;


import org.txm.rcp.editors.imports.ImportEditorSectionConfigurator;
import org.txm.rcp.editors.imports.ImportModuleCustomization;


public class TIGERImportSectionConfigurator extends ImportEditorSectionConfigurator {

	@Override
	public void installSections() {

		ImportModuleCustomization.addAdditionalSections("tigersearch", TIGERSection.class); //$NON-NLS-1$
	}
}
