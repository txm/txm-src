package org.txm.test;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import org.txm.searchengine.ts.TSCorpus;
import org.txm.searchengine.ts.TSCorpusManager;
import org.txm.searchengine.ts.TSMatch;
import org.txm.searchengine.ts.TSResult;
import org.txm.utils.ExecTimer;

public class DrawTSSVG {

	String userhome = System.getProperty("user.home"); //$NON-NLS-1$

	File configdir = new File(userhome, "TXM/corpora/graal/tiger/tigersearch.logprop"); //$NON-NLS-1$

	File registrydir = new File(userhome, "TXM/corpora/graal/tiger"); //$NON-NLS-1$

	File svgfile = new File(registrydir, "result.svg"); //$NON-NLS-1$

	String id = "GRAAL"; //$NON-NLS-1$

	String query = "#n:[cat = \"Obj\"] >* #m & arity(#n, 2, 10)"; //$NON-NLS-1$
	//String query = "[]";

	public void test() throws Exception {
		TSCorpusManager manager = new TSCorpusManager(registrydir, configdir);

		if (manager.isInitialized()) {

			ExecTimer.start();
			TSCorpus corpus = manager.getCorpus(id);

			System.out.println("T features: " + corpus.getTFeatures());
			System.out.println("NT features: " + corpus.getNTFeatures());

			TSResult result = corpus.query(query);

			result.setDisplayProperties(new ArrayList<>(Arrays.asList("word", "pos", "form")), "type"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

			TSMatch first = result.getFirst();
			System.out.println("First: nb of sub graph: " + first.getNumberOfSubGraph()); //$NON-NLS-1$
			first.firstSubGraph();
			first.toSVGFile(new File(registrydir, "result_1_1.svg")); //$NON-NLS-1$
			System.out.println(first.toHTML() + "<br>"); //$NON-NLS-1$
			first.nextSubGraph();
			first.toSVGFile(new File(registrydir, "result_1_2.svg")); //$NON-NLS-1$
			System.out.println(first.toHTML() + "<br>"); //$NON-NLS-1$
			first.nextSubGraph();
			first.toSVGFile(new File(registrydir, "result_1_3.svg")); //$NON-NLS-1$
			System.out.println(first.toHTML() + "<br>"); //$NON-NLS-1$

			//			TSMatch next = result.getNext();
			//			System.out.println( "Next: nb of sub graph: "+next.getNumberOfSubGraph());
			//			next.firstSubGraph();
			//			next.toSVGFile(new File(userhome,"TXM/Tiger/result_2_1.svg"));
			//			System.out.println( next.toHTML() + "<br>");
			//			next.nextSubGraph();
			//			next.toSVGFile(new File(userhome,"TXM/Tiger/result_2_2.svg"));
			//			System.out.println( next.toHTML() + "<br>");
			//			next.nextSubGraph();
			//			next.toSVGFile(new File(userhome,"TXM/Tiger/result_2_3.svg"));
			//			System.out.println( next.toHTML() + "<br>");
			//			next.previousSubGraph();
			//			next.toSVGFile(new File(userhome,"TXM/Tiger/result_2_2bis.svg"));
			//			System.out.println( next.toHTML() + "<br>");

			//	System.out.println( "make some room :)"
			//	for(File f : new File("/home/mdecorde/Bureau/tigerexports/").listFiles(IOUtils.HIDDENFILE_FILTER))
			//		f.delete()
			//

			//	System.out.println( "SAVE XML"
			//	result.toXml(new File("/home/mdecorde/Bureau/tigerexports/result.xml"), false, true)
			//
			//	System.out.println( "SAVE AS SVG"
			//	for (int i = 0 ; i < result.getNumberOfMatch() && i < 10; i++) {
			//		result.getMatch(i).toSVGFile(new File("/home/mdecorde/Bureau/tigerexports/match_"+i+".svg"))
			//	}
			//
			//				System.out.println("SIMPLE NO PNC");
			//				System.out.println(result.toConcordance(new File(registrydir, "export1.csv"), "concordance_simple", 30, Arrays.asList("cat"), Arrays.asList("pos"), false));
			//				System.out.println("MOT-PIVOT NO PNC");
			//				System.out.println(result.toConcordance(new File(registrydir, "export2.csv"), "concordance_mot-pivot", 30, Arrays.asList("cat"), Arrays.asList("pos"), false));
			//				System.out.println("BLOCKS NO PNC");
			//				System.out.println(result.toConcordance(new File(registrydir, "export3.csv"), "concordance_blocks", 30, Arrays.asList("cat"), Arrays.asList("pos"), false));

			//	System.out.println( "SIMPLE + PNC"
			//	System.out.println( result.toConcordance(new File("/home/mdecorde/Bureau/tigerexports/export12.csv"), "concordance_simple", 30, ["cat"], ["pos"], true);
			//	System.out.println( "MOT-PIVOT + PNC"
			//	System.out.println( result.toConcordance(new File("/home/mdecorde/Bureau/tigerexports/export22.csv"), "concordance_mot-pivot", 30, ["cat"], ["pos"], true);
			//	System.out.println( "BLOCKS + PNC"
			//	System.out.println( result.toConcordance(new File("/home/mdecorde/Bureau/tigerexports/export32.csv"), "concordance_blocks", 30, ["cat"], ["pos"], true);
			System.out.println(ExecTimer.stop());
		}
	}

	public static void main(String[] args) {
		DrawTSSVG d = new DrawTSSVG();
		try {
			d.test();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
