package org.txm.para.core.functions;

public class ParaCell {

	public String CCG, CCD, CG, keyword, CD, ref;

	public ParaCell(String CCG, String CG, String keyword, String CD, String CCD, String ref) {
		this.CCG = CCG;
		this.CG = CG;
		this.CD = CD;
		this.CCD = CCD;
		this.keyword = keyword;
		this.ref = ref;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ref + ": " + CCG + ">" + CG + "|" + keyword + "|" + CD + "<" + CCD; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
	}
}
