package org.txm.para.core.functions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.serverException.CqiServerError;

public class ParallelContexts {

	ArrayList<String> queries;

	String struct;

	ArrayList<CQPCorpus> corpora;

	ArrayList<String> props;

	ArrayList<String> refs;

	ArrayList<Integer> CGs;

	ArrayList<Integer> CDs;

	ArrayList<Boolean> participates;

	// TODO : manage 
	HashMap<String, Match[]> keywordsStartsEnds = new HashMap<String, Match[]>();

	HashMap<String, ParaCell[]> keywordsStrings = new HashMap<String, ParaCell[]>();

	ArrayList<String> segKeys;

	private String structprop;

	public ParallelContexts(ArrayList<String> queries, String struct, String structprop, ArrayList<CQPCorpus> corpora,
			ArrayList<String> props, ArrayList<String> refs,
			ArrayList<Integer> CGs, ArrayList<Integer> CDs,
			ArrayList<Boolean> participates) throws CqiClientException, IOException, CqiServerError {
		this.queries = queries;
		this.corpora = corpora;
		this.struct = struct;
		this.structprop = structprop;
		this.props = props;
		this.refs = refs;
		this.CGs = CGs;
		this.CDs = CDs;
		this.participates = participates;

		for (int num = 0; num < corpora.size(); num++) {
			if (!participates.get(num))
				continue;
			CQPCorpus corpus = corpora.get(num);
			//String lang = corpus.getLang();
			String query = queries.get(num);
			System.out.println(corpus.getName() + " query: " + query);
			QueryResult result = corpus.query(new CQLQuery(query), "TMP", true);

			try {
				List<Match> matches = result.getMatches();
				int[] positions = result.getStarts();

				StructuralUnitProperty align_id = corpus.getStructuralUnit(struct).getProperty(structprop);
				System.out.println("struct: " + struct + " prop: " + structprop + " -> " + align_id);
				int[] struct_pos = CQPSearchEngine.getCqiClient().cpos2Struc(align_id.getQualifiedName(), positions);
				String[] struct_ids = CQPSearchEngine.getCqiClient().struc2Str(align_id.getQualifiedName(), struct_pos);

				// fill allsegments
				for (int i = 0; i < struct_ids.length; i++) {
					String id = struct_ids[i];
					if (!keywordsStartsEnds.containsKey(id)) {
						keywordsStartsEnds.put(id, new Match[corpora.size()]);
						keywordsStrings.put(id, new ParaCell[corpora.size()]);
					}
					keywordsStartsEnds.get(id)[num] = matches.get(i); // take the first one
				}
			}
			catch (Exception e) {
				System.out.println("Result is NULL ! " + e.toString());
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}

		//sort allsegments
		segKeys = new ArrayList<String>(keywordsStartsEnds.keySet());
		Collections.sort(segKeys);
		System.out.println("segKeys : " + segKeys.size());
	}

	/**
	 * @return the keywordsStartsEnds
	 */
	public HashMap<String, Match[]> getKeywordsStartsEnds() {
		return keywordsStartsEnds;
	}

	/**
	 * @return the keywordsStrings
	 * @throws CqiServerError
	 * @throws IOException
	 * @throws CqiClientException
	 */
	public HashMap<String, ParaCell[]> getKeywordsStrings() throws CqiClientException, IOException, CqiServerError {
		return getKeywordsStrings(0, segKeys.size());
	}

	/**
	 * @return the keywordsStrings
	 * @throws CqiClientException
	 * @throws CqiServerError
	 * @throws IOException
	 */
	public LinkedHashMap<String, ParaCell[]> getKeywordsStrings(int from, int to) throws CqiClientException, IOException, CqiServerError {
		if (from < 0) from = 0;
		if (to > segKeys.size()) to = segKeys.size();

		LinkedHashMap<String, ParaCell[]> rez = new LinkedHashMap<String, ParaCell[]>();
		getMissingValues(from, to);
		if (segKeys.size() > 0) {
			for (int i = from; i < to; i++)
				rez.put(segKeys.get(i), keywordsStrings.get(segKeys.get(i)));
			//System.out.println("getKeywordsStrings [REZ="+rez+"]");
		}
		else {
			System.out.println("getKeywordsStrings - NO SEG KEYS !!");
		}

		return rez;
	}

	private void getMissingValues(int from, int to) throws CqiClientException, IOException, CqiServerError {
		AbstractCqiClient cqiClient = CorpusManager.getCorpusManager().getCqiClient();
		if (segKeys.size() > 0) {
			String segquery = "<" + struct + "_" + structprop + "=\""; //\"]";
			for (int i = from; i < to; i++) {
				segquery += segKeys.get(i) + "|";
			}
			segquery = segquery.substring(0, segquery.length() - 1);
			segquery += "\">[]";
			System.out.println("align_seg_query : " + segquery);

			// loop over corpus to minimize the number of queries
			for (int nocorpus = 0; nocorpus < corpora.size(); nocorpus++) {
				CQPCorpus corpus = corpora.get(nocorpus);
				int MAXPOSITION = corpus.getSize();
				StructuralUnitProperty align_id = corpus.getStructuralUnit(struct).getProperty(structprop);
				Property prop = corpus.getProperty(props.get(nocorpus));
				//PropertyHashMap<CqpDataProxy> proxies = CorpusManager.getCorpusManager().getCorpusProxies(corpus);
				int iCG = CGs.get(nocorpus);
				int iCD = CDs.get(nocorpus);
				//System.out.println("iCG "+iCG+" iCD "+iCD);
				//CqpDataProxy wordp = cqiClient.get(prop);
				String[] split = refs.get(nocorpus).split("_", 2);
				//System.out.println("refs : "+refs);
				StructuralUnitProperty ref = null;
				if (split.length == 2)
					ref = corpus.getStructuralUnit(split[0]).getProperty(split[1]);

				// get the seg that actually are in the corpus
				QueryResult resul2t = corpus.query(new CQLQuery(segquery), "TMP", true);
				int[] struct_starts = resul2t.getStarts();
				int[] struct_pos = CQPSearchEngine.getCqiClient().cpos2Struc(align_id.getQualifiedName(), struct_starts);
				String[] struct_ids = CQPSearchEngine.getCqiClient().struc2Str(align_id.getQualifiedName(), struct_pos);

				//System.out.println("segs: "+Arrays.toString(struct_ids));

				// get ref if asked
				String[] ref_values = null;
				if (ref != null) {
					int[] struct_pos2 = CQPSearchEngine.getCqiClient().cpos2Struc(ref.getQualifiedName(), struct_starts);
					ref_values = CQPSearchEngine.getCqiClient().struc2Str(ref.getQualifiedName(), struct_pos2);
				}
				//System.out.println("ref values: "+Arrays.toString(ref_values));

				// get the words values of segs
				QueryResult result = corpus.query(new CQLQuery(segquery + "+</" + struct + ">"), "TMP", false);
				List<Match> matches = result.getMatches();
				//System.out.println("seg matches: "+matches);
				ArrayList<Integer> positions = new ArrayList<Integer>();
				for (Match m : matches) {
					List<Integer> range = m.getRange();

					int minpos = range.get(0);
					int maxpos = range.get(range.size() - 1);
					//System.out.println("range size "+range.size()+" minpos "+minpos+" maxpos "+maxpos);
					//int n = 0;
					for (int i = iCG; i > 0; i--) {
						if (minpos - i >= 0) {
							positions.add(minpos - i);
							//System.out.println("CG add "+(minpos-i));
						}
					}
					positions.addAll(range);
					//System.out.println("add "+range);
					for (int i = 1; i <= iCD; i++) {
						if (maxpos + i < MAXPOSITION) {
							positions.add(maxpos + i);
							//System.out.println("CD add "+(maxpos+i));
						}
					}
				}

				int[] cpos = new int[positions.size()];
				for (int i = 0; i < positions.size(); i++)
					cpos[i] = positions.get(i);

				List<String> words = cqiClient.getSingleData(prop, cpos);
				//System.out.println("words: "+(words));

				int wordIterator = 0;
				//System.out.println("loop");
				for (int i = 0; i < struct_ids.length; i++) // for each seg actually in the corpus
				{
					if (keywordsStrings.get(struct_ids[i])[nocorpus] != null)
						continue; // this cell has already been computed
					if (wordIterator >= words.size())
						break; // no more words to process
					Match m = matches.get(i);

					Match keyWordMatch = keywordsStartsEnds.get(struct_ids[i])[nocorpus];
					//System.out.println("wordIterator "+wordIterator+" str "+words.get(wordIterator)+ " str2 "+words.get(wordIterator+1)+" pos "+cpos[wordIterator]+" seg no "+i+" match "+m+" keyword "+keyWordMatch);
					//if ((m.getStart() - cpos[wordIterator]) < iCG) System.out.println("WARNING CG");
					String CCG = "", CG = "", keyword = "", CD = "", CCD = "", refValue = "";
					int state = 1; // 0 CCG, 1 CG 2 keyword 3 CD 4 CCD
					int previouspos = -1;
					while (wordIterator < cpos.length
							&& cpos[wordIterator] <= m.getEnd() + iCD
							&& previouspos <= cpos[wordIterator]) { // concat word value
						int pos = cpos[wordIterator];
						previouspos = pos;
						//if (keyWordMatch != null) {
						if (pos < m.getStart())
							state = 0;
						else if (keyWordMatch != null && pos < keyWordMatch.getStart())
							state = 1;
						else if (keyWordMatch != null && pos <= keyWordMatch.getEnd())
							state = 2;
						else if (pos <= m.getEnd())
							state = 3;
						else
							state = 4;
						//}	
						switch (state) {
							case 0:
								CCG += words.get(wordIterator) + " ";
								break;
							case 1:
								CG += words.get(wordIterator) + " ";
								break;
							case 2:
								keyword += words.get(wordIterator) + " ";
								break;
							case 3:
								CD += words.get(wordIterator) + " ";
								break;
							case 4:
								CCD += words.get(wordIterator) + " ";
								break;
						}
						wordIterator++;
					}

					if (ref_values != null) {
						refValue = ref_values[i];
						//System.out.println("ref: "+refValue);
					}

					// set ParaCell
					ParaCell cell = new ParaCell(CCG, CG, keyword, CD, CCD, refValue);
					keywordsStrings.get(struct_ids[i])[nocorpus] = cell;
				}
			}
		}
	}

	/**
	 * @return the segKeys
	 */
	public ArrayList<String> getSegKeys() {
		return segKeys;
	}

	/**
	 * @return the queries
	 */
	public ArrayList<String> getQueries() {
		return queries;
	}

	/**
	 * @return the struct
	 */
	public String getStruct() {
		return struct;
	}

	/**
	 * @return the corpora
	 */
	public ArrayList<CQPCorpus> getCorpora() {
		return corpora;
	}

	/**
	 * @return the props
	 */
	public ArrayList<String> getProps() {
		return props;
	}

	/**
	 * @return the refs
	 */
	public ArrayList<String> getRefs() {
		return refs;
	}

	/**
	 * @return the cGs
	 */
	public ArrayList<Integer> getCGs() {
		return CGs;
	}

	/**
	 * @return the cDs
	 */
	public ArrayList<Integer> getCDs() {
		return CDs;
	}

	/**
	 * @return the participates
	 */
	public ArrayList<Boolean> getParticipates() {
		return participates;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((CDs == null) ? 0 : CDs.hashCode());
		result = prime * result + ((CGs == null) ? 0 : CGs.hashCode());
		result = prime * result + ((corpora == null) ? 0 : corpora.hashCode());
		result = prime * result
				+ ((participates == null) ? 0 : participates.hashCode());
		result = prime * result + ((props == null) ? 0 : props.hashCode());
		result = prime * result + ((queries == null) ? 0 : queries.hashCode());
		result = prime * result + ((refs == null) ? 0 : refs.hashCode());
		result = prime * result + ((struct == null) ? 0 : struct.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParallelContexts other = (ParallelContexts) obj;
		if (CDs == null) {
			if (other.CDs != null)
				return false;
		}
		else if (!CDs.equals(other.CDs))
			return false;
		if (CGs == null) {
			if (other.CGs != null)
				return false;
		}
		else if (!CGs.equals(other.CGs))
			return false;
		if (corpora == null) {
			if (other.corpora != null)
				return false;
		}
		else if (!corpora.equals(other.corpora))
			return false;
		if (participates == null) {
			if (other.participates != null)
				return false;
		}
		else if (!participates.equals(other.participates))
			return false;
		if (props == null) {
			if (other.props != null)
				return false;
		}
		else if (!props.equals(other.props))
			return false;
		if (queries == null) {
			if (other.queries != null)
				return false;
		}
		else if (!queries.equals(other.queries))
			return false;
		if (refs == null) {
			if (other.refs != null)
				return false;
		}
		else if (!refs.equals(other.refs))
			return false;
		if (struct == null) {
			if (other.struct != null)
				return false;
		}
		else if (!struct.equals(other.struct))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ParallelContexts [queries=" + queries + ", struct=" + struct
				+ ", corpora=" + corpora + ", props=" + props + ", refs="
				+ refs + ", CGs=" + CGs + ", CDs=" + CDs + ", participates="
				+ participates + "]";
	}
}
