package org.txm.para.core.functions;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.txm.core.results.TXMResult;
import org.txm.searchengine.cqp.CqpDataProxy;
import org.txm.searchengine.cqp.ReferencePattern;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.corpus.query.QueryPart;
import org.txm.utils.Pair;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.i18n.LangFormater;

public class ParaBrowser extends TXMResult {


	/**
	 * 
	 * @param parent
	 */
	public ParaBrowser(TXMResult parent) {
		super(parent);
	}


	/**
	 * Gets the next or previous.
	 *
	 * @param unit the unit
	 * @param property the property
	 * @param value the value
	 * @param isNext the is next
	 * @return the nextor previous
	 */
	public String getNextOrPrevious(MainCorpus corpus, StructuralUnit unit,
			StructuralUnitProperty property, String value, boolean isNext) {
		String next = "";
		CQLQuery query = null;
		System.out.println("GET NEXT or PREVIOUS : " + value + " unit " + unit
				+ " property " + property + " next? " + isNext);
		if (isNext) {
			query = new CQLQuery("<" + unit.getName() + "_id=\"" + value
					+ "\">[]+</" + unit.getName() + ">[]");
		}
		else {
			query = new CQLQuery("<" + unit.getName() + ">[]+" + "</"
					+ unit.getName() + "><" + unit.getName() + "_id=\"" + value
					+ "\">[]+");
		}
		Property text_id = unit.getProperty("id"); //$NON-NLS-1$
		// System.out.println("text_id = unit "+unit+" get props de id");
		// System.out.println("Query : "+query);
		if (property != null) {
			// List<Line> lines;
			int nLines;

			ReferencePattern referencePattern = new ReferencePattern();
			referencePattern.addProperty(text_id);

			QueryResult queryResult;
			try {
				Map<Property, List<List<String>>> wordPropValues = new HashMap<>();

				queryResult = corpus.query(query, query.getQueryString().replace(
						" ", "_")
						+ "_parallel", false);
				nLines = queryResult.getNMatch(); // get number of tokens
				// System.out.println("nLines : "+nLines);
				// lines = new ArrayList<Line>();

				List<Match> matches = null;
				if (nLines > 0)
					matches = queryResult.getMatches(0, nLines - 1); // get the
				// indexes
				// sequences
				// of
				// result's
				// tokens
				else
					matches = new ArrayList<>();

				List<Integer> beginingOfKeywordsPositions = new ArrayList<>();
				List<Integer> lengthOfKeywords = new ArrayList<>();
				// keywordsViewPropValues = new HashMap<Property,
				// List<List<String>>>();
				int nb = 0;
				for (int j = 0; j < nLines; j++) {
					Match match = matches.get(j);
					beginingOfKeywordsPositions.add(match.getStart()); // get
					// the first
					// index
					lengthOfKeywords.add(match.getEnd() - match.getStart() + 1);// get
					// the last index
					nb = match.getEnd() - match.getStart() + 1;
				}

				if (!isNext) {
					nb = 1;
				}

				// get all reference values of all lines
				for (Property prop : referencePattern) {
					// List<List<String>> propVals = cache.get(prop).getData(
					// beginingOfKeywordsPositions,
					// Collections.nCopies(beginingOfKeywordsPositions
					// .size(), nb));
					// // for the first word = 1
					// // for the last word = nb
					//
					// for (List<String> propVal : propVals) {
					// for (String str : propVal) {
					// // System.out.println("Next = "+str);
					// next = str;
					// }
					// }
				}

			}
			catch (CqiClientException e1) {
				org.txm.utils.logger.Log.printStackTrace(e1);
			}
		}
		return next;
	}


	/**
	 * Gets the text region.
	 *
	 * @param unit the unit
	 * @param property the property
	 * @param value the value
	 * @return the text region
	 */
	public Pair<String, QueryResult> getTextRegion(MainCorpus corpus, StructuralUnit unit,
			StructuralUnitProperty property, String value) {
		CQLQuery query = new QueryPart(unit, property, value);
		// System.out.println("GET REGION : " + value);

		String text = "";

		if (property != null) {
			LinkedHashMap<String, Integer> counts;
			// List<Line> lines;
			int nLines;

			CqpDataProxy cache;
			try {
				cache = new CqpDataProxy(null, corpus.getProperty("word")); // TODO: not null
			}
			catch (CqiClientException e1) {
				org.txm.utils.logger.Log.printStackTrace(e1);
				return new Pair<>(
						"error can't get word property", null);
			}

			// get the cqp result of the query
			QueryResult result;
			try {
				result = corpus.query(query, query.getQueryString().replace(" ",
						"_")
						+ "_parallel", false);
				nLines = result.getNMatch(); // get number of tokens
				System.out.println("nMatchs : " + nLines);
				// lines = new ArrayList<Line>();

				List<Match> matches = null;
				if (nLines > 0)
					matches = result.getMatches(0, nLines - 1); // get the
				// indexes
				// sequences of
				// result's
				// tokens
				else
					matches = new ArrayList<>();

				List<Integer> beginingOfKeywordsPositions = new ArrayList<>();
				List<Integer> lengthOfKeywords = new ArrayList<>();
				// keywordsViewPropValues = new HashMap<Property,
				// List<List<String>>>();

				for (int j = 0; j < nLines; j++) {
					Match match = matches.get(j);
					beginingOfKeywordsPositions.add(match.getStart()); // get
					// the
					// first
					// index
					lengthOfKeywords.add(match.getEnd() - match.getStart() + 1);// get
					// the
					// last
					// index
				}

				List<List<String>> propVals = cache.getData(
						beginingOfKeywordsPositions, lengthOfKeywords);
				for (List<String> prop : propVals) {
					text += "\n\n";
					for (String str : prop) {
						text += str + " ";
						// System.out.println("Region word = "+str);
					}
				}
			}
			catch (CqiClientException e) {
				org.txm.utils.logger.Log.printStackTrace(e);
				return new Pair<>("error", null);
			}
			return new Pair<>(LangFormater.format(text, corpus
					.getLang()), result);
		}
		// return keywordsViewPropValues;
		return new Pair<>("error no property", null);
	}


	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getSimpleName() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getDetails() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void clean() {
		// TODO Auto-generated method stub

	}


	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveParameters() throws Exception {
		// FIXME: not yet computed
		System.err.println("ParaBrowser.saveParameters() not implemented");
		return false;
	}


	@Override
	public boolean loadParameters() throws Exception {
		// FIXME: not yet computed
		System.err.println("ParaBrowser.loadParameters() not implemented");
		return false;
	}


	@Override
	public boolean canCompute() throws Exception {
		// FIXME: not yet computed
		System.err.println("ParaBrowser.canCompute() not implemented");
		return false;
	}


	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {
		// FIXME: not yet computed
		System.err.println("ParaBrowser._compute() not implemented");
		return false;
	}


	@Override
	public String getResultType() {
		return "Parallel browser";
	}
}
