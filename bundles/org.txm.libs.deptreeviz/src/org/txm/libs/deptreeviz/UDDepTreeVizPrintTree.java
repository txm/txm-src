package org.txm.libs.deptreeviz;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.gitlab.nats.deptreeviz.DepTree;
import io.gitlab.nats.deptreeviz.SimpleParse;
import io.gitlab.nats.deptreeviz.SimpleWord;

public class UDDepTreeVizPrintTree {
	
	public static File print(File file, List<String> conll, String[] Tvalues, String[] NTvalues) {
		
		try {
			PrintWriter pw = new PrintWriter(file);
				
			
			
			ArrayList<String> conll2 = new ArrayList<>();
			for (int i = 0 ; i < conll.size() ; i++) {
				String l = conll.get(i);
				String split[] = l.split("\t");
//				if (split[6].equals("_") && split[0].contains("-")) {
//					if (conll.size() > i + 1) {
//						String splitNext[] = conll.get(i+1).split("\t");
//						if (split[0].startsWith(splitNext[0]+ "-")) {
//							split[6] = splitNext[6];
//						}
//					}
//				}
//				
				if (split[6].equals("_")) {
					//split[6] = "0";
					continue;
				}
				conll2.add(l);
//				l = "";
//				for (String s : split) l += s+"\t";
//				
//				conll2.add(l.substring(0, l.length() - 1));
				
			}
			for (String l : conll2) System.out.println(l);
			
			SimpleParse p = SimpleParse.fromConll(conll2);
			List<SimpleWord> words = p.getWords();
			List<String> labels = p.getVerticesLabels().get("SYN");
			
			if (Tvalues != null && Tvalues.length == words.size()) {
				
				for (int i = 0; i < words.size(); i++) {
					SimpleWord w = words.get(i);
					
					Field fword = SimpleWord.class.getDeclaredField("word");
					fword.setAccessible(true);
										
					if (Tvalues[i] != null) {
						fword.set(w, w.getWord() + " / " + Tvalues[i]);
					} else {
						fword.set(w, w.getWord());
					}
				}
			}
			
			if (NTvalues != null && NTvalues.length == words.size()) {
				List<String> tmp = labels;
				for (int i = 0; i < words.size(); i++) {
					if (NTvalues[i] != null) {
						tmp.set(i, tmp.get(i) + " / " + NTvalues[i]);
					}
				}
				p.getVerticesLabels().put("SYN", tmp);
			}
			DepTree<SimpleParse, SimpleWord> dt = new DepTree<SimpleParse, SimpleWord>(p);
			// dt.setShowingReferent(true);
			// dt.setShowingCat(true);
			dt.writeTree(pw);
			// w.close();
			return file;
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static void main(String[] args) throws IOException {
		
		File file = new File("/home/mdecorde/deptreeviz.svg");
		
		String all = """
1	[Pour]	pour	ADP	_	_	3	case	_	start_char=4|end_char=8|ner=O|XmlId=w_1959_1
2	la	le	DET	_	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	start_char=9|end_char=11|ner=O|XmlId=w_1959_3
3	métropole	métropole	NOUN	_	Gender=Fem|Number=Sing	15	obl:mod	_	start_char=12|end_char=21|ner=O|XmlId=w_1959_4
4	française	français	ADJ	_	Gender=Fem|Number=Sing	3	amod	_	start_char=22|end_char=31|ner=O|XmlId=w_1959_5
5	,	,	PUNCT	_	_	3	punct	_	start_char=31|end_char=32|ner=O|XmlId=w_1959_6
6	pour	pour	ADP	_	_	8	case	_	start_char=33|end_char=37|ner=O|XmlId=w_1959_7
7	l'	le	DET	_	Definite=Def|Number=Sing|PronType=Art	8	det	_	start_char=38|end_char=40|ner=O|XmlId=w_1959_8
8	Algérie	Algérie	PROPN	_	Number=Sing	15	obl:mod	_	start_char=40|end_char=47|ner=S-LOC|XmlId=w_1959_9
9	,	,	PUNCT	_	_	8	punct	_	start_char=47|end_char=48|ner=O|XmlId=w_1959_10
10	pour	pour	ADP	_	_	12	case	_	start_char=49|end_char=53|ner=O|XmlId=w_1959_11
11	la	le	DET	_	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	12	det	_	start_char=54|end_char=56|ner=O|XmlId=w_1959_12
12	communauté	communauté	NOUN	_	Gender=Fem|Number=Sing	15	obl:mod	_	start_char=57|end_char=67|ner=O|XmlId=w_1959_13
13	,	,	PUNCT	_	_	12	punct	_	start_char=67|end_char=68|ner=O|XmlId=w_1959_14
14	je	il	PRON	_	Number=Sing|Person=1|PronType=Prs	15	nsubj	_	start_char=69|end_char=71|ner=O|XmlId=w_1959_15
15	forme	former	VERB	_	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	0	root	_	start_char=72|end_char=77|ner=O|XmlId=w_1959_16
16	des	un	DET	_	Definite=Ind|Number=Plur|PronType=Art	17	det	_	start_char=78|end_char=81|ner=O|XmlId=w_1959_17
17	voeux	vœu	NOUN	_	Gender=Masc|Number=Plur	15	obj	_	start_char=83|end_char=88|ner=O|XmlId=w_1959_18
18	ardents	ardent	ADJ	_	Gender=Masc|Number=Plur	17	amod	_	start_char=90|end_char=97|ner=O|XmlId=w_1959_19
19	et	et	CCONJ	_	_	20	cc	_	start_char=98|end_char=100|ner=O|XmlId=w_1959_20
20	confiants	confiant	ADJ	_	Gender=Masc|Number=Plur	18	conj	_	start_char=101|end_char=110|ner=O|XmlId=w_1959_21
21-22	au	_	_	_	_	_	_	_	start_char=111|end_char=113|ner=O|XmlId=w_1959_22
21	à	à	ADP	_	_	24	case	_	XmlId=w_1959_23
22	le	le	DET	_	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	24	det	_	XmlId=w_1959_24
23	premier	premier	ADJ	_	Gender=Masc|Number=Sing	24	amod	_	start_char=114|end_char=121|ner=O|XmlId=w_1959_25
24	jour	jour	NOUN	_	Gender=Masc|Number=Sing	15	obl:mod	_	start_char=122|end_char=126|ner=O|XmlId=w_1959_26
25	de	de	ADP	_	_	26	case	_	start_char=127|end_char=129|ner=O|XmlId=w_1959_27
26	1960	1960	NUM	_	Number=Plur	24	nmod	_	start_char=130|end_char=134|ner=O|XmlId=w_1959_28
27	.	.	PUNCT	_	_	15	punct	_	start_char=134|end_char=135|ner=O|XmlId=w_1959_29
""";
		
		
		List<String> conll = Arrays.asList(all.split("\n"));
		
		
		System.out.println("FILE: " + print(file, conll, null, null));
	}
	
}
