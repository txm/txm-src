// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.backtomedia.preferences;

import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbench;
import org.txm.rcp.jface.ComboFieldEditor;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;

import vlcplayerrcp.MessagesMP;

/**
 * This class represents a preference page that is contributed to the
 * Preferences dialog. By subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows us to create a page
 * that is small and knows how to save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They are stored in the
 * preference store that belongs to the main plug-in class. That way,
 * preferences can be accessed directly via the preference store.
 */

public class BackToMediaPreferencePage extends TXMPreferencePage {

	/** The Constant ID. */
	public static final String ID = "org.txm.backtomedia.preferences.BackToMediaPreferencePage"; //$NON-NLS-1$

	/**
	 * The display parts count in chart title state.
	 */
	private BooleanFieldEditor loopProperty;

	private ComboFieldEditor backtotext_position;

	private ComboFieldEditor useWordProperty;

	private StringFieldEditor defaultStructure;

	private StringFieldEditor defaultStructureStartProperty;

	private StringFieldEditor defaultStructureEndProperty;

	private StringFieldEditor defaultWordTimeProperty;

	private IntegerFieldEditor wordLeftContextSizeProperty;

	private IntegerFieldEditor wordRightContextSizeProperty;

	private Group spanGroup;

	private ComboFieldEditor playerProperty;

	/**
	 * Instantiates a new diagnostic preference page.
	 */
	public BackToMediaPreferencePage() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(new BackToMediaPreferences().getPreferencesNodeQualifier()));
		// this.setImageDescriptor(WordCloudAdapterFactory.ICON);
		this.setTitle(MessagesMP.mediaPlayer);
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		loopProperty = new BooleanFieldEditor(BackToMediaPreferences.REPEAT, MessagesMP.BackToMediaPreferencePage_1, getFieldEditorParent());
		addField(loopProperty);

		String[][] values = {
				{ "OVER", "-1" },  //$NON-NLS-1$ //$NON-NLS-2$
				{ "RATIO", "-2" },  //$NON-NLS-1$ //$NON-NLS-2$
				{ "ABOVE", "" + EModelService.ABOVE },  //$NON-NLS-1$ //$NON-NLS-2$
				{ "BELOW", "" + EModelService.BELOW },  //$NON-NLS-1$ //$NON-NLS-2$
				{ "LEFT", "" + EModelService.LEFT_OF },  //$NON-NLS-1$ //$NON-NLS-2$
				{ "RIGHT", "" + EModelService.RIGHT_OF } }; //$NON-NLS-1$ //$NON-NLS-2$

		backtotext_position = new ComboFieldEditor(BackToMediaPreferences.BACKTOMEDIA_POSITION, MessagesMP.BackToMediaPosition, values, getFieldEditorParent());
		addField(backtotext_position);

		String[][] choices = { { BackToMediaPreferences.WORDMODE, BackToMediaPreferences.WORDMODE },
				{ BackToMediaPreferences.STRUCTUREMODE, BackToMediaPreferences.STRUCTUREMODE },
				{ BackToMediaPreferences.MILESTONEMODE, BackToMediaPreferences.MILESTONEMODE }
		};
		useWordProperty = new ComboFieldEditor(BackToMediaPreferences.SYNCMODE, MessagesMP.BackToMediaPreferencePage_2, choices, getFieldEditorParent());

		addField(useWordProperty);

		spanGroup = new Group(getFieldEditorParent(), SWT.NONE);
		spanGroup.setText(MessagesMP.BackToMediaPreferencePage_3);
		GridData gridData2 = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData2.horizontalSpan = 3;
		gridData2.verticalIndent = 10;
		spanGroup.setLayoutData(gridData2);
		RowLayout rlayout = new RowLayout(SWT.VERTICAL);
		rlayout.marginHeight = 5;
		spanGroup.setLayout(rlayout);

		defaultWordTimeProperty = new StringFieldEditor(BackToMediaPreferences.WORD_PROPERTY, MessagesMP.BackToMediaPreferencePage_4, spanGroup);
		addField(defaultWordTimeProperty);

		wordLeftContextSizeProperty = new IntegerFieldEditor(BackToMediaPreferences.WORD_CONTEXT_LEFT_DISTANCE, MessagesMP.LeftLength, spanGroup);
		addField(wordLeftContextSizeProperty);

		wordRightContextSizeProperty = new IntegerFieldEditor(BackToMediaPreferences.WORD_CONTEXT_RIGHT_DISTANCE, MessagesMP.RightLength, spanGroup);
		addField(wordRightContextSizeProperty);

		defaultStructure = new StringFieldEditor(BackToMediaPreferences.STRUCTURE, MessagesMP.BackToMediaPreferencePage_5, spanGroup);
		addField(defaultStructure);

		defaultStructureStartProperty = new StringFieldEditor(BackToMediaPreferences.STRUCTURE_START_PROPERTY, MessagesMP.BackToMediaPreferencePage_6, spanGroup);
		addField(defaultStructureStartProperty);

		defaultStructureEndProperty = new StringFieldEditor(BackToMediaPreferences.STRUCTURE_END_PROPERTY, MessagesMP.BackToMediaPreferencePage_7, spanGroup);
		addField(defaultStructureEndProperty);

		updateEnables(BackToMediaPreferences.getInstance().getString(BackToMediaPreferences.SYNCMODE));

		String[][] players = { { "HTML", "HTML" }, { "JFX", "JFX" }, { "VLC", "VLC" } }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
		playerProperty = new ComboFieldEditor(BackToMediaPreferences.PLAYER, MessagesMP.EmbeddedPlayer, players, getFieldEditorParent());
		addField(playerProperty);
	}

	/**
	 * enable/disable widget depending on the selected media synchronization mode
	 * 
	 * @param sync_mode
	 */
	public void updateEnables(String sync_mode) {

		if (BackToMediaPreferences.WORDMODE.equals(sync_mode)) {
			defaultWordTimeProperty.setEnabled(true, spanGroup);
			wordLeftContextSizeProperty.setEnabled(true, spanGroup);
			wordRightContextSizeProperty.setEnabled(true, spanGroup);

			defaultStructure.setEnabled(false, spanGroup);
			defaultStructureStartProperty.setEnabled(false, spanGroup);
			defaultStructureEndProperty.setEnabled(false, spanGroup);
		}
		else if (BackToMediaPreferences.STRUCTUREMODE.equals(sync_mode)) {
			defaultWordTimeProperty.setEnabled(false, spanGroup);
			wordLeftContextSizeProperty.setEnabled(false, spanGroup);
			wordRightContextSizeProperty.setEnabled(false, spanGroup);

			defaultStructure.setEnabled(true, spanGroup);
			defaultStructureStartProperty.setEnabled(true, spanGroup);
			defaultStructureEndProperty.setEnabled(true, spanGroup);
		}
		else { // milestones
			defaultWordTimeProperty.setEnabled(false, spanGroup);
			wordLeftContextSizeProperty.setEnabled(false, spanGroup);
			wordRightContextSizeProperty.setEnabled(false, spanGroup);

			defaultStructure.setEnabled(true, spanGroup);
			defaultStructureStartProperty.setEnabled(true, spanGroup);
			defaultStructureEndProperty.setEnabled(false, spanGroup);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		String prefName = ((FieldEditor) event.getSource()).getPreferenceName();
		if (prefName.equals(BackToMediaPreferences.SYNCMODE)) {
			updateEnables(useWordProperty.getStringValue());
		}
	}
}
