package org.txm.backtomedia.preferences;

import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

public class BackToMediaPreferences extends TXMPreferences {

	// auto populate the preference node qualifier from the current bundle id
	// public static final String PREFERENCES_NODE = FrameworkUtil.getBundle(BackToMediaPreferences.class).getSymbolicName();

	public static final String PREFERENCES_PREFIX = "backtomedia_"; //$NON-NLS-1$

	public static final String REPEAT = "backtomedia_loop"; //$NON-NLS-1$

	public static final String SYNCMODE = "sync_mode"; //$NON-NLS-1$

	public static final String STRUCTURE = "backtomedia_structure"; //$NON-NLS-1$

	public static final String STRUCTURE_START_PROPERTY = "backtomedia_startproperty"; //$NON-NLS-1$

	public static final String STRUCTURE_END_PROPERTY = "backtomedia_endproperty"; //$NON-NLS-1$

	public static final String WORD_PROPERTY = "backtomedia_wordtime_property"; //$NON-NLS-1$

	public static final String WORD_CONTEXT_LEFT_DISTANCE = "backtomedia_word_context_left_distance"; //$NON-NLS-1$

	public static final String WORD_CONTEXT_RIGHT_DISTANCE = "backtomedia_word_context_right_distance"; //$NON-NLS-1$

	// the sync modes
	public static final String WORDMODE = "Word"; //$NON-NLS-1$

	public static final String STRUCTUREMODE = "Structure"; //$NON-NLS-1$

	public static final String MILESTONEMODE = "Milestone"; //$NON-NLS-1$

	public static final String MEDIA_PATH_PREFIX = "media_path_prefix"; //$NON-NLS-1$

	public static final String MEDIA_EXTENSION = "media_extension"; //$NON-NLS-1$

	public static final String MEDIA_AUTH = "media_auth"; //$NON-NLS-1$

	public static final String MEDIA_AUTH_LOGIN = "media_auth_login"; //$NON-NLS-1$

	public static final String MEDIA_AUTH_PASSWORD = "media_auth_password"; //$NON-NLS-1$

	/**
	 * preferences node to media file access paths
	 */
	public static final String MEDIA_INDEX_NODE = "media_index_node"; //$NON-NLS-1$

	/**
	 * one of the EModelService position constant to use when opening editor: ABOVE = 0 BELOW = 1 LEFT = 2 RIGHT = 3
	 */
	public static final String BACKTOMEDIA_POSITION = "backtomedia_position"; //$NON-NLS-1$

	public static final String PLAYER = "player"; //$NON-NLS-1$

	public BackToMediaPreferences() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.put(PLAYER, "HTML"); //$NON-NLS-1$
		preferences.putBoolean(REPEAT, true);
		preferences.put(SYNCMODE, MILESTONEMODE);
		preferences.put(STRUCTURE, "u"); //$NON-NLS-1$
		preferences.put(STRUCTURE_START_PROPERTY, "start"); //$NON-NLS-1$
		preferences.put(STRUCTURE_END_PROPERTY, "end"); //$NON-NLS-1$
		preferences.put(WORD_PROPERTY, "time"); //$NON-NLS-1$
		preferences.putInt(WORD_CONTEXT_LEFT_DISTANCE, 10); // conc + 2
		preferences.putInt(WORD_CONTEXT_RIGHT_DISTANCE, 15); // conc + 2
		preferences.putInt(BACKTOMEDIA_POSITION, EModelService.ABOVE);
	}

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(BackToMediaPreferences.class)) {
			new BackToMediaPreferences();
		}
		return TXMPreferences.instances.get(BackToMediaPreferences.class);
	}
}

