// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.backtomedia.editors.input;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.txm.rcp.Application;
import org.txm.rcp.IImageKeys;

/**
 * Media player editor input
 *
 * @author mdecorde
 */
public class MediaPlayerEditorInput implements IEditorInput {

	String mrl;

	int startTime = 0, endTime = -1;

	private String sStartTime;

	private String sEndTime;

	private boolean loop;

	private String mdp;

	private String user;

	private boolean lightweight = false;

	/**
	 * Instantiates a new Media player editor input.
	 *
	 */
	public MediaPlayerEditorInput() {
		super();
	}

	public MediaPlayerEditorInput(String mrl, int startTime) {

		this.mrl = mrl;
		this.startTime = startTime;
	}

	public MediaPlayerEditorInput(String mrl, int startTime, int endTime) {

		this.mrl = mrl;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public MediaPlayerEditorInput(String mrl, String sStartTime, String sEndTime) {

		this.mrl = mrl;
		this.sStartTime = sStartTime;
		this.sEndTime = sEndTime;
	}

	public String getURL() {
		return mrl;
	}

	public int getStartTime() {
		return startTime;
	}

	public int getEndTime() {
		return endTime;
	}

	public String getStringStartTime() {
		return sStartTime;
	}

	public String getStringEndTime() {
		return sEndTime;
	}

	/**
	 * Exists.
	 *
	 * @return true, if successful
	 * @see org.eclipse.ui.IEditorInput#exists()
	 */
	@Override
	public boolean exists() {
		return false;
	}

	/**
	 * Gets the image descriptor.
	 *
	 * @return the image descriptor
	 * @see org.eclipse.ui.IEditorInput#getImageDescriptor()
	 */
	@Override
	public ImageDescriptor getImageDescriptor() {
		return AbstractUIPlugin.imageDescriptorFromPlugin(
				Application.PLUGIN_ID,
				IImageKeys.ACTION_WORDCLOUD);
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 * @see org.eclipse.ui.IEditorInput#getName()
	 */
	@Override
	public String getName() {
		return "Media Player"; //$NON-NLS-1$
	}

	/**
	 * Gets the persistable.
	 *
	 * @return the persistable
	 * @see org.eclipse.ui.IEditorInput#getPersistable()
	 */
	@Override
	public IPersistableElement getPersistable() {
		return null;
	}

	/**
	 * Gets the tool tip text.
	 *
	 * @return the tool tip text
	 * @see org.eclipse.ui.IEditorInput#getToolTipText()
	 */
	@Override
	public String getToolTipText() {
		return getName();
	}

	/**
	 * Gets the adapter.
	 *
	 * @param arg0 the arg0
	 * @return the adapter
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Object getAdapter(Class arg0) {
		return null;
	}

	public void setLoop(boolean loop) {

		this.loop = loop;
	}

	public boolean getLoop() {

		return loop;
	}

	public void setCrendentials(String user, String mdp) {

		this.user = user;
		this.mdp = mdp;
	}

	public String getCredentialUser() {

		return user;
	}

	public String getCredentialPassword() {

		return mdp;
	}

	public void setLightWeightInterface(boolean lightweight) {

		this.lightweight = lightweight;
	}

	public boolean getLightWeightInterface() {

		return lightweight;
	}
}
