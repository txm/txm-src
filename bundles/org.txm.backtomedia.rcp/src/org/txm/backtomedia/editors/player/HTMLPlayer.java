package org.txm.backtomedia.editors.player;

import java.io.File;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Scale;
import org.txm.backtomedia.commands.function.OpenMediaPlayer;
import org.txm.backtomedia.commands.function.SimpleFileDialog;
import org.txm.backtomedia.preferences.BackToMediaPreferences;
import org.txm.rcp.editors.TXMBrowserEditor;
import org.txm.rcp.swt.GLComposite;

import vlcplayerrcp.MessagesMP;

public class HTMLPlayer extends Composite implements IPlayer {

	Browser browser;

	String mrl = null;

	private String login;

	private String mdp;

	private Button stopButton;

	String initMrl;

	int initStartTime = 0;

	int initEndTime = -1;

	String initStringStartTime = null;

	String initStringEndTime = null;

	boolean initRepeat = false;

	private Label volumeLabel;

	private Label rateLabel;

	public HTMLPlayer(Composite parent, int style, boolean lightweight) {

		super(parent, style);

		this.setLayout(new GridLayout(1, true));

		this.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {

				dispose();

			}
		});

		browser = new Browser(this, SWT.NONE);
		browser.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		browser.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseScrolled(MouseEvent e) {

				if ((e.stateMask & SWT.CTRL) == SWT.CTRL) {
					if (e.count > 0) {
						TXMBrowserEditor.zoomIn(browser);
					}
					else {
						TXMBrowserEditor.zoomOut(browser);
					}
				}
			}
		});

		// use flex  to center the video
		// use ondataload fix the video size depending on its ratio and the viewport height
		// diable the fullscreen button, use a CSS special rule for webkit
		String html = "<!DOCTYPE html><html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">\n" +  //$NON-NLS-1$
				"<style>video::-webkit-media-controls-fullscreen-button {display: none;</style></head>\n" +  //$NON-NLS-1$
				"<body><div style=\"display:flex;align-items: center;justify-content: center;\">\n" +  //$NON-NLS-1$
				"<video id=\"video\" controls controlsList=\"nofullscreen nodownload\" onloadeddata=\"v = document.getElementById('video');r = v.videoWidth/v.videoHeight; v.style.width=(95*r)+'vh';\"></video></div></body></html>"; //$NON-NLS-1$

		browser.setText(html); // <source> is set later

		browser.addProgressListener(new ProgressListener() {

			@Override
			public void completed(ProgressEvent event) {
				//System.out.println("LOADED");
				HTMLPlayer.this.loaded = true;
				if (initMrl != null) {
					if (initStringStartTime != null) {
						HTMLPlayer.this.play(initMrl, initStringStartTime, initStringEndTime);
					}
					else {
						HTMLPlayer.this.play(initMrl, initStartTime, initEndTime);
					}
				}
				setRepeat(initRepeat);
			}

			@Override
			public void changed(ProgressEvent event) {
			}
		});

		browser.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {

				//System.out.println("KEY:"+e.keyCode);
				if (e.keyCode == SWT.SPACE) {
					pause();
				}
				else if (e.keyCode == SWT.ARROW_LEFT) {
					minus10s();
				}
				else if (e.keyCode == SWT.ARROW_RIGHT) {
					plus10s();
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		Menu menu = new Menu(browser);
		MenuItem playStopItem = new MenuItem(menu, SWT.PUSH);
		playStopItem.setText(MessagesMP.Play);
		playStopItem.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				resume();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		MenuItem pauseItem = new MenuItem(menu, SWT.PUSH);
		pauseItem.setText(MessagesMP.Pause);
		pauseItem.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				pause();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		MenuItem minus10Item = new MenuItem(menu, SWT.PUSH);
		minus10Item.setText(MessagesMP.Minus10S);
		minus10Item.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				minus10s();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		MenuItem plus10Item = new MenuItem(menu, SWT.PUSH);
		plus10Item.setText(MessagesMP.Plus10S);
		plus10Item.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				plus10s();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		browser.setMenu(menu);

		GLComposite controls = new GLComposite(this, SWT.NONE, "controls"); //$NON-NLS-1$
		controls.getLayout().numColumns = 20;
		controls.getLayout().horizontalSpacing = 2;

		if (!lightweight) {
			Button openButton = new Button(controls, SWT.PUSH);
			stopButton.setToolTipText(MessagesMP.openANewMedia);
			openButton.setText(MessagesMP.Opennnn);
			openButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent event) {

					SimpleFileDialog fd = new SimpleFileDialog(HTMLPlayer.this.getDisplay().getActiveShell(), MessagesMP.OpeningMediaFile, MessagesMP.EnterFilePathOrURL,
							BackToMediaPreferences.getInstance().getString(OpenMediaPlayer.ID), null);
					if (fd.open() == SimpleFileDialog.OK) {
						String mrl = fd.getValue();

						if (mrl != null) {
							File f = new File(mrl);
							if (f.exists()) {
								mrl = new File(mrl).toURI().toString();
							}
							else {

							}
							BackToMediaPreferences.getInstance().put(OpenMediaPlayer.ID, mrl);
							play(mrl, 0);
						}
					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			stopButton = new Button(controls, SWT.PUSH);
			stopButton.setText(MessagesMP.Stop);
			stopButton.setToolTipText(MessagesMP.stopPlayingTheCurrentMedia);
			stopButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent event) {
					stop();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});


			Button minus10Button = new Button(controls, SWT.PUSH);
			minus10Button.setText(MessagesMP.Minus10S);
			minus10Button.setToolTipText(MessagesMP.goBackwardOf10s);
			minus10Button.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent event) {
					minus10s();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			Button plus10Button = new Button(controls, SWT.PUSH);
			plus10Button.setText(MessagesMP.Plus10S);
			minus10Button.setToolTipText(MessagesMP.goForwardOf10s);
			plus10Button.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent event) {
					plus10s();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			//			Button playpauseButton = new Button(controls, SWT.TOGGLE);
			//			playpauseButton.setText("Pause");
			//			playpauseButton.setToolTipText("Play/Pause the media");
			//			playpauseButton.setSelection(true);
			//			playpauseButton.addSelectionListener(new SelectionListener() {
			//				
			//				@Override
			//				public void widgetSelected(SelectionEvent event) {
			//					if (playpauseButton.getSelection()) {
			//						resume();
			//						playpauseButton.setText("Pause");
			//					} else {
			//						pause();
			//						playpauseButton.setText("Play");
			//					}
			//				}
			//				
			//				@Override
			//				public void widgetDefaultSelected(SelectionEvent e) { }
			//			});
			//			
			//			Button replayButton = new Button(controls, SWT.PUSH);
			//			replayButton.setText("Re-play");
			//			replayButton.setToolTipText("Restart playing the media");
			//			replayButton.addSelectionListener(new SelectionListener() {
			//				
			//				@Override
			//				public void widgetSelected(SelectionEvent event) {
			//					replay();
			//				}
			//				
			//				@Override
			//				public void widgetDefaultSelected(SelectionEvent e) { }
			//			});
			//			
			//			Button loopButton = new Button(controls, SWT.TOGGLE);
			//			loopButton.setText("Loop");
			// 			loopButton.setToolTipText("Restart playing the media when end is reached");
			//			loopButton.setSelection(initRepeat);
			//			loopButton.addSelectionListener(new SelectionListener() {
			//				
			//				@Override
			//				public void widgetSelected(SelectionEvent event) {
			//					setRepeat(loopButton.getSelection());
			//				}
			//				
			//				@Override
			//				public void widgetDefaultSelected(SelectionEvent e) { }
			//			});
			//			
			//			volumeLabel = new Label(controls, SWT.NONE);
			//			volumeLabel.setText("Volume (100%)");
			//			
			//			Scale volumneSpinner = new Scale(controls, SWT.NONE);
			//			volumneSpinner.setToolTipText("Set the volume");
			//			volumneSpinner.setMinimum(0);
			//			volumneSpinner.setMaximum(100);
			//			volumneSpinner.setSelection(100);
			//			volumneSpinner.addSelectionListener(new SelectionListener() {
			//				
			//				@Override
			//				public void widgetSelected(SelectionEvent e) {
			//					setVolume(volumneSpinner.getSelection());
			//				}
			//				
			//				@Override
			//				public void widgetDefaultSelected(SelectionEvent e) { }
			//			});

			//			GridData gdata = new GridData(GridData.CENTER, GridData.CENTER, false, false);
			//			gdata.minimumWidth = gdata.widthHint = 150;
			//			volumneSpinner.setLayoutData(gdata);

			rateLabel = new Label(controls, SWT.NONE);
			rateLabel.setText(MessagesMP.Rate10);

			Scale rateSpinner = new Scale(controls, SWT.NONE);
			rateSpinner.setToolTipText(MessagesMP.changeThePlayRate);
			rateSpinner.setMinimum(10);
			rateSpinner.setMaximum(300);
			rateSpinner.setSelection(100);
			rateSpinner.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					setRate(((float) rateSpinner.getSelection()) / 100.0f);
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
			GridData gdata2 = new GridData(GridData.CENTER, GridData.CENTER, false, false);
			gdata2.minimumWidth = gdata2.widthHint = 150;
			rateSpinner.setLayoutData(gdata2);
		}



		//		new Label(controls, SWT.NONE).setText("Progression");
		//		
		//		Scale progressionSpinner = new Scale(controls, SWT.NONE);
		//		progressionSpinner.setToolTiptext("Set the play position of the media");
		//		progressionSpinner.setMinimum(0);
		//		progressionSpinner.setMaximum(300);
		//		progressionSpinner.setSelection(0);
		//		progressionSpinner.addSelectionListener(new SelectionListener() {
		//			
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				seek(((float)progressionSpinner.getSelection()));
		//			}
		//			
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) { }
		//		});
		//		GridData gdata3 = new GridData(GridData.FILL, GridData.CENTER, true, false);
		//		progressionSpinner.setLayoutData(gdata3);



	}

	protected float getTime() {

		Object time = browser.evaluate("return document.getElementById('video').currentTime;"); //$NON-NLS-1$
		if (time != null) {
			return Float.valueOf(time.toString());
		}
		else {
			return 0f;
		}
	}

	@Override
	public void setCredentials(String login, String mdp) {

		//browser.evaluate(TXMCoreMessages.bind("document.getElementById('video').;", login, mdp));
		this.login = login;
		this.mdp = mdp;
	}

	public void setInitialMedia(String mrl, int start, int end) {
		this.initMrl = mrl;
		this.initStartTime = start;
		this.initEndTime = end;
	}

	public void setInitialMedia(String mrl, String start, String end) {
		this.initMrl = mrl;
		this.initStringStartTime = start;
		this.initStringEndTime = end;
	}

	public void hideStopButton() {
		if (this.stopButton != null && !this.stopButton.isDisposed()) {
			this.stopButton.dispose();
		}
	}

	@Override
	public void dispose() {
		stop();
		super.dispose();
	}

	DateTimeFormatter hhmmssFormatter = DateTimeFormatter.ISO_LOCAL_TIME;

	protected boolean loaded;

	@Override
	public boolean play(String mrl, String startTime, String endTime) {

		int start = 0, end = 0;
		if (startTime.matches("[0-9]+.[0-9]+")) { //$NON-NLS-1$
			start = (int) (Float.parseFloat(startTime));
		}
		else if (startTime.matches("[0-9]+:[0-9]+:[0-9]+")) { //$NON-NLS-1$
			if (startTime.indexOf(":") == 1) { //$NON-NLS-1$
				startTime = "0" + startTime; //$NON-NLS-1$
			}
			LocalTime time1 = LocalTime.parse(startTime, hhmmssFormatter);
			start = ((time1.getHour() * 60 * 60) + (time1.getMinute() * 60) + time1.getSecond());
		}

		if (endTime != null) {
			if (endTime.matches("[0-9]+.[0-9]+")) { //$NON-NLS-1$
				end = (int) (Float.parseFloat(endTime));
			}
			else if (endTime.matches("[0-9]+:[0-9]+:[0-9]+")) { //$NON-NLS-1$
				if (endTime.indexOf(":") == 1) endTime = "0" + endTime; //$NON-NLS-1$ //$NON-NLS-2$
				LocalTime time1 = LocalTime.parse(endTime, hhmmssFormatter);
				end = ((time1.getHour() * 60 * 60) + (time1.getMinute() * 60) + time1.getSecond());
			}
		}
		return play(mrl, start, end);

	}

	@Override
	public boolean play(String mrl2, int start, int end) {

		//		browser.evaluate("document;");
		//		browser.evaluate("document.getElementById('video');");
		//		
		if (login != null && mdp != null) {
			mrl2 = mrl2.replace("{0}", login).replace("{1}", mdp); //$NON-NLS-1$ //$NON-NLS-2$
		}
		//System.out.println("MEDIA URL: "+mrl2);
		String script = ""; //$NON-NLS-1$

		if (HTMLPlayer.this.mrl == null || !HTMLPlayer.this.mrl.equals(mrl2)) {
			script += "document.getElementById('video').setAttribute('src', '" + mrl2 + "');\n";  //$NON-NLS-1$ //$NON-NLS-2$

			script += "document.getElementById('video').play();\n" + //$NON-NLS-1$
					"document.getElementById('video').onloadedmetadata = function() {document.getElementById('video').currentTime = " + start + ";};\n"; //$NON-NLS-1$ //$NON-NLS-2$
		}
		else {
			script += "document.getElementById('video').play();\n" + //$NON-NLS-1$
					"document.getElementById('video').currentTime=" + start + ";\n"; //$NON-NLS-1$ //$NON-NLS-2$
		}

		//System.out.println("EVAL: "+script);
		browser.evaluate(script);

		this.mrl = mrl2;
		return true;
	}

	@Override
	public boolean play(String mrl, int start) {

		return play(mrl, start, -1);

	}

	@Override
	public void stop() {

		if (browser.isDisposed()) return;

		mrl = null;
		browser.evaluate("document.getElementById('video').pause();"); //$NON-NLS-1$
		browser.evaluate("document.getElementById('video').removeAttribute('src');"); //$NON-NLS-1$

	}

	public void togglePlayPause() {
		if (isPlaying()) {
			pause();
		}
		else {
			resume();
		}
	}

	@Override
	public boolean isMediaLoaded() {

		return mrl != null && mrl.length() > 0;

	}

	public void setInitialRepeat(boolean b) {

		initRepeat = b;

	}

	@Override
	public void setRepeat(boolean b) {

		browser.evaluate("document.getElementById('video').loop=" + b + ";"); //$NON-NLS-1$ //$NON-NLS-2$

	}
	//
	//	@Override
	//	public boolean isDisposed() {
	//		
	//		return browser == null || browser.isDisposed();
	//		
	//	}

	@Override
	public void seek(float time) {

		browser.evaluate("document.getElementById('video').currentTime=" + time + ";"); //$NON-NLS-1$ //$NON-NLS-2$

	}

	public void minus10s() {
		float time = getTime() - 10;
		if (time < 0) time = 0f;
		seek(time);
	}

	public void plus10s() {
		float time = getTime() + 10;
		seek(time);
	}

	@Override
	public void pause() {

		browser.evaluate("document.getElementById('video').pause();"); //$NON-NLS-1$

	}

	@Override
	public void replay() {

		browser.evaluate("document.getElementById('video').currentTime=0;"); //$NON-NLS-1$

	}

	@Override
	public void resume() {

		browser.evaluate("document.getElementById('video').play();"); //$NON-NLS-1$

	}

	@Override
	public void setVolume(double volume) {

		browser.evaluate("document.getElementById('video').volume=" + volume / 100.0d + ";"); //$NON-NLS-1$ //$NON-NLS-2$
		if (volumeLabel != null && !volumeLabel.isDisposed()) volumeLabel.setText(MessagesMP.VolumeP0 + (int) volume + "%)"); //$NON-NLS-2$ //$NON-NLS-1$
	}

	@Override
	public void setRate(float rate) {

		browser.evaluate("document.getElementById('video').playbackRate=" + rate + ";"); //$NON-NLS-1$ //$NON-NLS-2$
		if (rateLabel != null && !rateLabel.isDisposed()) rateLabel.setText(String.format(MessagesMP.Rate1f, rate));

	}

	@Override
	public boolean isPlaying() {
		Object o = browser.evaluate("return !document.getElementById('video').playing;"); //$NON-NLS-1$
		if (o == null) return false;
		if (o instanceof Boolean) {
			return (Boolean) o;
		}
		return false;

	}

	@Override
	public void setStartTime(float seconds) {

		//browser.evaluate(TXMCoreMessages.bind("alert(''set start time {0}'');", seconds));

	}

	@Override
	public void setStopTime(float seconds) {

		//browser.evaluate(TXMCoreMessages.bind("alert(''set stop time {0}'');", seconds));

	}
}
