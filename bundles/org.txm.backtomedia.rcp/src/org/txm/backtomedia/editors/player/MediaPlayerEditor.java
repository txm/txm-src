package org.txm.backtomedia.editors.player;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
import org.txm.backtomedia.editors.input.MediaPlayerEditorInput;
import org.txm.backtomedia.preferences.BackToMediaPreferences;

public class MediaPlayerEditor extends EditorPart {

	/** The Constant ID. */
	public static final String ID = MediaPlayerEditor.class.getCanonicalName(); // $NON-NLS-1$

	IPlayer player;

	@Override
	public void createPartControl(Composite parent) {
		if ("VLC".equals(BackToMediaPreferences.getInstance().getString(BackToMediaPreferences.PLAYER))) { //$NON-NLS-1$
			player = new VLCPlayer(parent, SWT.BORDER);
		}
		else {
			HTMLPlayer hplayer = new HTMLPlayer(parent, SWT.BORDER, this.getEditorInput().getLightWeightInterface());

			hplayer.setInitialMedia(this.getEditorInput().getURL(), this.getEditorInput().getStartTime(), this.getEditorInput().getEndTime());
			hplayer.setInitialMedia(this.getEditorInput().getURL(), this.getEditorInput().getStringStartTime(), this.getEditorInput().getStringEndTime());
			hplayer.setCredentials(this.getEditorInput().getCredentialUser(), this.getEditorInput().getCredentialPassword());
			hplayer.setInitialRepeat(this.getEditorInput().getLoop());

			player = hplayer;

			//		} else if ("JFX".equals(BackToMediaPreferences.getInstance().getString(BackToMediaPreferences.PLAYER))) {
			//			player = new JFXPlayer(parent, SWT.NONE);
		}
	}

	public MediaPlayerEditorInput getEditorInput() {
		return (MediaPlayerEditorInput) super.getEditorInput();
	}

	public void init() {

	}

	/*
	 * Returns whether the contents of this editor should be saved when the editor
	 * is closed.
	 * <p>
	 * This method returns <code>true</code> if and only if the editor is dirty
	 * (<code>isDirty</code>).
	 * </p>
	 */
	@Override
	public boolean isSaveOnCloseNeeded() {
		player.stop();
		return false;
	}

	@Override
	public void setFocus() {

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		player.stop();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#doSaveAs()
	 */
	@Override
	public void doSaveAs() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#init(org.eclipse.ui.IEditorSite, org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setSite(site);
		setInput(input);

		// VLCPlayerEditorInput ii = (VLCPlayerEditorInput) input;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return true;
	}

	/**
	 * ensure the player is displayed and ready
	 * 
	 * @return
	 */
	public IPlayer getPlayer() {
		return player;
	}

	@Override
	public void setPartName(String partName) {
		super.setPartName(partName);
	}

	@Override
	public boolean isDirty() {
		return false; // player.isMediaLoaded();
	}

	/**
	 * for more test use "player.getEmbeddedMediaPlayer()"
	 * 
	 * @return false if the media could not be loaded (file not found, access error...)
	 */
	public boolean isMediaLoaded() {
		return player.isMediaLoaded();
	}
}
