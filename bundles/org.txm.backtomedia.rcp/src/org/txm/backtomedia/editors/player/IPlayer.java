package org.txm.backtomedia.editors.player;

public interface IPlayer {

	public void setCredentials(String login, String mdp);

	public boolean play(String mrl, String start, String end);

	public boolean play(String mrl, int start, int end);

	public boolean play(String mrl, int start);

	/**
	 * unload the media
	 */
	public void stop();

	/**
	 * 
	 * @param time seconds with decimals
	 */
	public void seek(float time);

	public void pause();

	public void replay();

	public void resume();

	public boolean isMediaLoaded();

	public void setRepeat(boolean b);

	public boolean isDisposed();

	/**
	 * 
	 * @param volume from 0 to 100
	 */
	public void setVolume(double volume);

	/**
	 * 
	 * @param rate from 0.1 to 1.0
	 */
	public void setRate(float rate);

	public boolean isPlaying();

	public void setStartTime(float seconds);

	public void setStopTime(float seconds);
}
