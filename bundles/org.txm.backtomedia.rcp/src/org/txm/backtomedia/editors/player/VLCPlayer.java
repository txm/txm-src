package org.txm.backtomedia.editors.player;

import static uk.co.caprica.vlcj.binding.LibVlc.libvlc_new;
import static uk.co.caprica.vlcj.binding.LibVlc.libvlc_release;

import java.io.File;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Semaphore;

import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Scale;
import org.txm.backtomedia.commands.function.TripleRangeSlider;
import org.txm.backtomedia.preferences.BackToMediaPreferences;
import org.txm.utils.logger.Log;

import com.sun.jna.StringArray;

import uk.co.caprica.vlcj.binding.internal.libvlc_instance_t;
import uk.co.caprica.vlcj.factory.discovery.NativeDiscovery;
import uk.co.caprica.vlcj.factory.discovery.strategy.NativeDiscoveryStrategy;
import uk.co.caprica.vlcj.factory.swt.SwtMediaPlayerFactory;
import uk.co.caprica.vlcj.player.base.MediaPlayer;
import uk.co.caprica.vlcj.player.base.MediaPlayerEventAdapter;
import uk.co.caprica.vlcj.player.component.EmbeddedMediaPlayerComponent;
import uk.co.caprica.vlcj.player.embedded.EmbeddedMediaPlayer;
import uk.co.caprica.vlcj.player.embedded.videosurface.swt.CompositeVideoSurface;
import uk.co.caprica.vlcj.support.version.LibVlcVersion;
import vlcplayerrcp.MessagesMP;

public class VLCPlayer extends Composite implements IPlayer {

	static { // force native libs discovery -> fix Mac OS X init
		NativeDiscovery discovery = new NativeDiscovery() {

			@Override
			protected void onFound(String path, NativeDiscoveryStrategy strategy) {
				Log.finer("VLC NativeDiscovery: onFound " + path + " " + strategy); //$NON-NLS-1$ //$NON-NLS-2$
			}

			@Override
			protected void onNotFound() {
				Log.finer("VLC NativeDiscovery: Not found."); //$NON-NLS-1$
			}
		};

		boolean found = discovery.discover();
		Log.finer("VLC NativeDiscovery: the discovery was succesfull ? " + found); //$NON-NLS-1$
		if (found) {
			libvlc_instance_t instance = libvlc_new(0, new StringArray(new String[0]));
			Log.finer("VLC NativeDiscovery: VLC instance is " + instance); //$NON-NLS-1$
			if (instance != null) {
				libvlc_release(instance);
			}
			else {
				Log.warning(NLS.bind(MessagesMP.ImpossibleToUseYourVLCP0, instance));
			}
			Log.finer("VLC NativeDiscovery: VLC version is " + new LibVlcVersion().getVersion()); //$NON-NLS-1$
		}
		else {
			Log.warning(MessagesMP.ImpossibleToFindVLC);
		}
	}

	protected static final String NOMEDIA = ""; //$NON-NLS-1$

	private EmbeddedMediaPlayer vlcPlayer;

	private Composite videoComposite;

	private CompositeVideoSurface playerCanvas;

	private Scale rateField;

	private Label rateValueLabel;

	private Scale volumeField;

	private Label volumeValueLabel;

	private Button playButton;

	private Button repeatButton;

	protected String currentlyPlayed = ""; //$NON-NLS-1$

	// private String startTime, endTime;
	private int start, end;

	protected boolean hasEnded = false;

	private String previouslyPlayed = ""; //$NON-NLS-1$

	private boolean repeat = false;

	protected int volume = 100;

	private Button stopButton;

	Label timeLabel;

	TripleRangeSlider timeRange;

	boolean firstLengthEvent = true;

	long previous = 0;

	long time, mins, secs;

	private SwtMediaPlayerFactory factory;


	static {
		// uncomment to enable VLC logs
		// Logger.setLevel(Level.TRACE);
	}

	public EmbeddedMediaPlayer getEmbeddedMediaPlayer() {
		return vlcPlayer;
	}

	Semaphore s = new Semaphore(1);

	public VLCPlayer(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(11, false));

		// THE PLAYER
		// if (RuntimeUtil.isMac()) {
		// try {
		// LibC.INSTANCE.setenv("VLC_PLUGIN_PATH", "/Applications/VLC.app/Contents/MacOS/plugins", 1);
		// }
		// catch (Exception ex) {
		// ex.printStackTrace();
		// }
		// }
		videoComposite = new Composite(this, SWT.EMBEDDED | SWT.NO_BACKGROUND);
		GridData gdata = new GridData(SWT.FILL, SWT.FILL, true, true);
		gdata.horizontalSpan = 11;
		videoComposite.setLayoutData(gdata);

		factory = new SwtMediaPlayerFactory();
		playerCanvas = factory.swt().newCompositeVideoSurface(videoComposite);

		EmbeddedMediaPlayerComponent e = new EmbeddedMediaPlayerComponent();
		vlcPlayer = e.mediaPlayer();
		vlcPlayer.videoSurface().set(playerCanvas);

		videoComposite.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {


				// vlcPlayer.submit(new Runnable() {
				//
				// @Override
				// public void run() {
				//
				// }
				// });

				// System.out.println("VLC STOP");
				// vlcPlayer.controls().stop();
				vlcPlayer.release();
				factory.release();
				// s.release();
			}
		});

		// THE CONTROL BUTTONS
		playButton = new Button(this, SWT.PUSH);
		GridData playLayoutData = new GridData(SWT.FILL, SWT.CENTER, false, false);
		playButton.setLayoutData(playLayoutData);
		playButton.setText(MessagesMP.play);
		playButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				vlcPlayer.submit(new Runnable() {

					@Override
					public void run() {
						if (currentlyPlayed.length() == 0) {
							selectMedia();
							playButton.setText(MessagesMP.pause);
						}
						else if (vlcPlayer.status().isPlaying()) {
							vlcPlayer.controls().pause();
							playButton.setText(MessagesMP.resume);
						}
						else if (hasEnded) {
							replay();
						}
						else {
							vlcPlayer.controls().play();
							playButton.setText(MessagesMP.pause);
						}
					}
				});
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// Button browseButton = new Button(this,SWT.PUSH);
		// browseButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
		// browseButton.setText("Open...");
		// browseButton.setToolTipText("Open a new media");// 
		// browseButton.addSelectionListener(new SelectionListener() {
		// @Override
		// public void widgetSelected(SelectionEvent e) {
		// selectMedia();
		// }
		//
		// @Override
		// public void widgetDefaultSelected(SelectionEvent e) {}
		// });

		stopButton = new Button(this, SWT.PUSH);
		stopButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		stopButton.setText(MessagesMP.stop);
		stopButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				stop();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		timeLabel = new Label(this, SWT.NONE);
		timeLabel.setText("00:00"); //$NON-NLS-1$

		timeRange = new TripleRangeSlider(this, SWT.None);
		timeRange.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		timeRange.setToolTipText(MessagesMP.time_range);
		timeRange.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				TripleRangeSlider.SELECTED_KNOB sk = timeRange.getLastSelectedKnob();
				switch (sk) {
					case UPPER:
						setStopTime(timeRange.getUpperValue());
						break;
					case LOWER:
						setStartTime(timeRange.getLowerValue());

						break;
					case MIDDLE:

						seek(timeRange.getMiddleValue());

						break;
					default:
						// nothing
				}
				// System.out.println("time range: "+start+" -> "+end+" time="+time);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		repeatButton = new Button(this, SWT.CHECK);
		repeatButton.setText(MessagesMP.repeat);
		repeat = BackToMediaPreferences.getInstance().getBoolean(BackToMediaPreferences.REPEAT);
		repeatButton.setSelection(repeat);
		repeatButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				repeat = repeatButton.getSelection();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Label l = new Label(this, SWT.NONE);
		l.setText(MessagesMP.rate);

		rateField = new Scale(this, SWT.BORDER);
		GridData gdata4 = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gdata4.widthHint = 100;
		rateField.setLayoutData(gdata4);
		rateField.setMaximum(140);
		rateField.setMinimum(70);
		rateField.setSelection(100);
		rateField.setPageIncrement(5);

		rateField.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				float rate = rateField.getSelection() / 100.0f;
				vlcPlayer.controls().setRate(rate);
				rateValueLabel.setText("" + rateField.getSelection() + "%"); //$NON-NLS-1$ //$NON-NLS-2$
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		rateValueLabel = new Label(this, SWT.NONE);
		rateValueLabel.setText("100%");//$NON-NLS-1$

		l = new Label(this, SWT.NONE);
		l.setText(MessagesMP.volume);

		volumeField = new Scale(this, SWT.BORDER);
		gdata4 = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gdata4.widthHint = 100;
		volumeField.setLayoutData(gdata4);
		volumeField.setMinimum(0);
		volumeField.setMaximum(100);
		volumeField.setSelection(volume);
		volumeField.setPageIncrement(5);
		volumeField.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				vlcPlayer.audio().setVolume(volumeField.getSelection());
				volume = volumeField.getSelection();
				volumeValueLabel.setText("" + volume + "%"); //$NON-NLS-1$ //$NON-NLS-2$
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		volumeValueLabel = new Label(this, SWT.NONE);
		volumeValueLabel.setText("100%"); //$NON-NLS-1$

		vlcPlayer.events().addMediaPlayerEventListener(new MediaPlayerEventAdapter() {

			@Override
			public void opening(MediaPlayer mediaPlayer) {
				if (videoComposite.isDisposed()) return;
				Log.finer("Opening media..."); //$NON-NLS-1$
			}

			@Override
			public void finished(MediaPlayer mediaPlayer) {
				if (videoComposite.isDisposed()) return;
				Log.finer("Finished playing media..."); //$NON-NLS-1$
				mediaPlayer.submit(new Runnable() {

					@Override
					public void run() {
						if (repeat) {
							replay();
						}
						else {
							hasEnded = true;
						}
					}
				});
			}
		});

		vlcPlayer.events().addMediaPlayerEventListener(new MediaPlayerEventAdapter() {

			@Override
			public void timeChanged(MediaPlayer mediaPlayer, final long arg1) {
				if (videoComposite.isDisposed()) return;

				mediaPlayer.submit(new Runnable() {

					@Override
					public void run() {
						time = arg1;
						if (previous == time) {
							return;
						}
						previous = time;

						timeLabel.getDisplay().syncExec(new Runnable() {

							@Override
							public void run() {
								if (timeRange.isDisposed()) {
									return;
								}
								if (!timeRange.isDragMiddleKnob()) {
									timeRange.setMiddleValue((int) time);
								}
								updateTimeLabel();

								if (arg1 > end && end != start) {
									// System.out.println("Time > end :"+arg1 +" > "+end);
									if (repeat) {
										vlcPlayer.controls().setTime(start);
									}
									else {
										vlcPlayer.controls().stop();
									}
								}
							}
						});
					}
				});
			}

			@Override
			public void lengthChanged(MediaPlayer mediaPlayer, final long arg1) {
				if (videoComposite.isDisposed()) return;

				mediaPlayer.submit(new Runnable() {

					@Override
					public void run() {
						if (firstLengthEvent) {
							firstLengthEvent = false;

							// initialize time range widget limits
							timeRange.getDisplay().syncExec(new Runnable() {

								@Override
								public void run() {
									if (timeRange.isDisposed()) return;
									timeRange.setMaximum((int) arg1);
									// if (start == end) end = (int)arg1;


									if (end > 0 && start != end) {
										timeRange.setUpperValue(end);
									}
									else {
										timeRange.setUpperValue((int) arg1);
									}

									timeRange.setLowerValue(start);
									// System.out.println("Range: "+start+" -> "+end+" song length "+arg1);
								}
							});
						}
					}
				});
			}
		});
	}

	private void updateTimeLabel() {
		mins = time / 60000;
		secs = (time / 1000) % 60;
		timeLabel.setText(String.format("%02d:%02d", mins, secs)); //$NON-NLS-1$
		timeLabel.update();
	}

	public void replay() {
		if (currentlyPlayed.length() > 0) {
			// this.play(currentlyPlayed, startTime, endTime);
			vlcPlayer.submit(new Runnable() {

				@Override
				public void run() {
					vlcPlayer.controls().setTime(start);
				}
			});

			playButton.setText(MessagesMP.pause);
		}
	}

	protected void selectMedia() {
		Log.fine(MessagesMP.select_file);

		FileDialog fd = new FileDialog(VLCPlayer.this.getShell(), SWT.OPEN);
		fd.setText(MessagesMP.select_file_title);
		File f = new File(previouslyPlayed);
		if (f.isDirectory()) fd.setFilterPath(f.getPath());
		else fd.setFilterPath(f.getParent());

		String[] filterExt = { "*.*", "*.mp3", "*.mp4", "*.avi" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		fd.setFilterExtensions(filterExt);
		String selected = fd.open();
		if (selected == null) {
			System.out.println(MessagesMP.cancel);
			return;
		}

		currentlyPlayed = selected;
		previouslyPlayed = selected;
		Log.fine(MessagesMP.opening + currentlyPlayed);
		play(currentlyPlayed, 0, 0);
	}

	/**
	 * 
	 * @param mrl
	 * @param time msec start time
	 * @param endtime msec end time
	 */
	@Override
	public boolean play(String mrl, int time, int endtime) {
		Log.fine(MessagesMP.bind(MessagesMP.playing, new Object[] { mrl, time, endtime }));
		return play(mrl, "" + time / 1000.0f, "" + endtime / 1000.0f); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Override
	public boolean play(String mrl, int time) {
		return play(mrl, time, time);
	}

	public void hideStopButton() {
		if (this.stopButton != null && !this.stopButton.isDisposed()) {
			this.stopButton.dispose();
		}
	}

	DateTimeFormatter hhmmssFormatter = DateTimeFormatter.ISO_LOCAL_TIME;

	private boolean authenticatedURLMode = false;

	/**
	 * 
	 * @param mrl
	 * @param startTime "0.0" or ""hh:mm:ss" format
	 * @param endTime "0.0" or ""hh:mm:ss" format
	 */
	@Override
	public boolean play(String mrl, String startTime, String endTime) {

		if (startTime.matches("[0-9]+.[0-9]+")) { //$NON-NLS-1$
			start = (int) (1000 * Float.parseFloat(startTime));
		}
		else if (startTime.matches("[0-9]+:[0-9]+:[0-9]+")) { //$NON-NLS-1$
			if (startTime.indexOf(":") == 1) { //$NON-NLS-1$
				startTime = "0" + startTime; //$NON-NLS-1$
			}
			LocalTime time1 = LocalTime.parse(startTime, hhmmssFormatter);
			start = 1000 * ((time1.getHour() * 60 * 60) + (time1.getMinute() * 60) + time1.getSecond());
		}

		if (endTime.matches("[0-9]+.[0-9]+")) { //$NON-NLS-1$
			end = (int) (1000 * Float.parseFloat(endTime));
		}
		else if (endTime.matches("[0-9]+:[0-9]+:[0-9]+")) { //$NON-NLS-1$
			if (endTime.indexOf(":") == 1) endTime = "0" + endTime; //$NON-NLS-1$ //$NON-NLS-2$
			LocalTime time1 = LocalTime.parse(endTime, hhmmssFormatter);
			end = 1000 * ((time1.getHour() * 60 * 60) + (time1.getMinute() * 60) + time1.getSecond());
		}

		// if (start == end) end = -1;

		Log.finer(MessagesMP.bind(MessagesMP.playing, new Object[] { mrl, startTime, endTime }));

		String[] options = { " :live-caching=200" }; // reduce video stream cache duration to 200ms //$NON-NLS-1$

		Log.finer("Preparing media..."); //$NON-NLS-1$

		// inserting login+mdp
		String tmp = null;
		if (authenticatedURLMode && mrl.contains("{0}") && mrl.contains("{1}") //$NON-NLS-1$ //$NON-NLS-2$
				&& System.getProperty(BackToMediaPreferences.MEDIA_AUTH_LOGIN) != null && System.getProperty(BackToMediaPreferences.MEDIA_AUTH_PASSWORD) != null) {
			tmp = NLS.bind(mrl, System.getProperty(BackToMediaPreferences.MEDIA_AUTH_LOGIN), System.getProperty(BackToMediaPreferences.MEDIA_AUTH_PASSWORD));
		}

		// use the patched URL if authenticatedURL is true
		if (authenticatedURLMode && tmp != null) {
			if (!vlcPlayer.media().prepare(tmp, options)) {
				Log.finer("** Impossible to prepare media."); //$NON-NLS-1$
				return false;
			}
		}
		else {
			if (!vlcPlayer.media().prepare(mrl, options)) {
				Log.finer("** Impossible to prepare media."); //$NON-NLS-1$
				return false;
			}
		}

		Log.finer("Is media valid ?"); //$NON-NLS-1$
		if (!vlcPlayer.media().isValid()) {
			Log.finer("** Invalid media."); //$NON-NLS-1$
			return false;
		}

		Log.info(MessagesMP.PlayingMedia);
		vlcPlayer.controls().play();
		vlcPlayer.audio().setVolume(volume);
		vlcPlayer.controls().setTime(start);

		if (new File(mrl + ".srt").exists()) { //$NON-NLS-1$
			vlcPlayer.subpictures().setSubTitleFile(mrl + ".srt"); //$NON-NLS-1$
		}
		currentlyPlayed = mrl;
		previouslyPlayed = mrl;

		if (!playButton.isDisposed()) {
			playButton.setText(MessagesMP.pause);
		}
		firstLengthEvent = true;

		return true;
	}

	/**
	 * for more test use "player.getEmbeddedMediaPlayer()"
	 * 
	 * @return false if the media could not be loaded (file not found, access error...)
	 */
	@Override
	public boolean isMediaLoaded() {
		return getEmbeddedMediaPlayer().status().isPlayable();
	}

	@Override
	public void setRepeat(boolean repeat) {
		this.repeat = repeat;
		repeatButton.setSelection(repeat);
	}

	@Override
	public void stop() {
		vlcPlayer.controls().stop();
		currentlyPlayed = NOMEDIA;
		if (!playButton.isDisposed()) {
			playButton.setText(MessagesMP.play);
		}
	}

	@Override
	public final void setCredentials(String login, String mdp) {
		if (login != null && mdp != null) {
			authenticatedURLMode = true;
		}
		else {
			authenticatedURLMode = false;
		}
	}

	@Override
	public void seek(float time) {

		//		vlcPlayer.controls().setPosition(time);
		time = timeRange.getMiddleValue();
		// System.out.println("Middle changed: fix time: "+time);
		vlcPlayer.controls().setPosition(time);
	}

	@Override
	public void pause() {

		vlcPlayer.controls().pause();

	}

	@Override
	public void resume() {

		vlcPlayer.controls().play();

	}

	@Override
	public void setVolume(double volume) {

		vlcPlayer.audio().setVolume(volumeField.getSelection());
	}

	@Override
	public void setRate(float rate) {

		vlcPlayer.controls().setRate(rate);

	}

	@Override
	public boolean isPlaying() {

		return vlcPlayer.status().isPlaying();
	}

	@Override
	public void setStartTime(float seconds) {

		start = timeRange.getLowerValue();
		if (start > time) {
			// System.out.println("Lower changed: fix time");
			time = start;
			vlcPlayer.controls().setTime(time);
		}
	}

	@Override
	public void setStopTime(float seconds) {
		end = timeRange.getUpperValue();

		if (end < time) {
			// System.out.println("Upper changed: fix time");
			time = end;
			vlcPlayer.controls().setTime(time);
		}

	}
}
