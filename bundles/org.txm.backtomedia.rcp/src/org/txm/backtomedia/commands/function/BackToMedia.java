// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 1
//
package org.txm.backtomedia.commands.function;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.part.EditorPart;
import org.txm.backtomedia.editors.player.MediaPlayerEditor;
import org.txm.backtomedia.preferences.BackToMediaPreferences;
import org.txm.concordance.core.functions.Line;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.TXMResult;
import org.txm.edition.rcp.editors.EditionPanel;
import org.txm.edition.rcp.editors.SynopticEditionEditor;
import org.txm.objects.CorpusCommandPreferences;
import org.txm.objects.Project;
import org.txm.objects.Text;
import org.txm.rcp.editors.ITXMResultEditor;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.UsernamePasswordDialog;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.logger.Log;

import vlcplayerrcp.MessagesMP;

/**
 * open the VLCPlayer Editor and go back to time
 * 
 * @author mdecorde.
 */
public class BackToMedia extends AbstractHandler {

	/** The ID. */
	public static String ID = "org.txm.rcp.commands.BackToMedia"; //$NON-NLS-1$

	/** The selection. */
	private IStructuredSelection selection;

	private static HashMap<IWorkbenchPart, MediaPlayerEditor> associatedEditors = new HashMap<>();

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.
	 * ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IWorkbenchPart page = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();

		if (event.getParameter("corpus") != null && event.getParameter("text") != null && (event.getParameter("word") != null || event.getParameter("time") != null)) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

			String corpus_parameter = event.getParameter("corpus"); //$NON-NLS-1$
			String text_parameter = event.getParameter("text"); //$NON-NLS-1$
			String word_parameter = event.getParameter("word"); //$NON-NLS-1$
			String time_parameter = event.getParameter("time"); //$NON-NLS-1$
			String start_time_parameter = event.getParameter("start_time"); //$NON-NLS-1$
			String end_time_parameter = event.getParameter("end_time"); //$NON-NLS-1$
			String sync_parameter = event.getParameter("sync"); //$NON-NLS-1$ Word, Structure, MileStone, etc.

			try {
				CQPCorpus cqpCorpus = CorpusManager.getCorpusManager().getCorpus(corpus_parameter);
				if (cqpCorpus == null) {
					Log.warning(NLS.bind(MessagesMP.ErrorCorpusWithIDP0NotFound, corpus_parameter));
					return null;
				}

				IWorkbenchPart editor = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();
				TXMEditor<TXMResult> txmEditor = (TXMEditor) editor;

				if (start_time_parameter != null && end_time_parameter != null) {
					return backToMedia(cqpCorpus, text_parameter, start_time_parameter, end_time_parameter, txmEditor, HandlerUtil.getActiveWorkbenchWindow(event).getShell());
				}
				else if (time_parameter != null) {
					return backToMedia(cqpCorpus, text_parameter, time_parameter, time_parameter, txmEditor, HandlerUtil.getActiveWorkbenchWindow(event).getShell());
				}
				else {
					return backtoMediaFromWordSelection(cqpCorpus, text_parameter, new String[] { word_parameter, null }, txmEditor, HandlerUtil.getActiveWorkbenchWindow(event).getShell());
				}
			}
			catch (Exception e) {
				Log.warning(TXMCoreMessages.bind(MessagesMP.FailToOpenMediaForParametersCorpusP0TextP1WordP2P3, corpus_parameter, text_parameter, word_parameter, e));
				Log.printStackTrace(e);
			}
			return null;
		}
		else if (page instanceof ConcordanceEditor) {
			Object o = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
			if (!(o instanceof IStructuredSelection)) {
				Log.fine(MessagesMP.BackToMedia_0);
				return null;
			}

			ConcordanceEditor ce = (ConcordanceEditor) page;
			selection = (IStructuredSelection) o;

			Object s = selection.getFirstElement();

			// System.out.println("Selected object: "+s);
			if (!(s instanceof Line)) {
				return null;
			}

			Line line = (Line) s;
			org.txm.objects.Match m = line.getMatch();
			String textid = line.getTextId();// line.getMatch().getValueForProperty(textP);// get text via text struc property id
			CQPCorpus corpus = line.getConcordance().getCorpus();

			try {
				return backtoMediaFromMatch(corpus, textid, m.getStart(), m.getEnd(), ce, ce.getContainer().getShell());
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
		else if (page instanceof SynopticEditionEditor) {
			SynopticEditionEditor editor = (SynopticEditionEditor) page;
			Text text = editor.getResult();
			Project project = text.getProject();
			MainCorpus cqpCorpus = (MainCorpus) project.getCorpusBuild(null, MainCorpus.class);
			EditionPanel panel = editor.getEditionPanel(0);
			String[] ids = panel.getWordSelection();
			// System.out.println("Words: "+Arrays.toString(ids));
			if (ids == null || ids[0] == null) {
				return backToMedia(cqpCorpus, text.getName(), "0.0", null, editor, HandlerUtil.getActiveWorkbenchWindow(event).getShell()); //$NON-NLS-1$
			}
			else if (ids[1] == null) { // only one word is selected -> using sync_mode preferences
				try {
					return backtoMediaFromWordSelection(cqpCorpus, text.getName(), ids, editor, HandlerUtil.getActiveWorkbenchWindow(event).getShell());
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else { // play the current word selection
				try {
					CQLQuery query = new CQLQuery(NLS.bind("[id=\"{0}\" & _.text_id=\"{1}\"]", ids[0] + "|" + ids[ids.length - 1], CQLQuery.addBackSlash(text.getName()))); //$NON-NLS-1$ //$NON-NLS-2$
					QueryResult rez = cqpCorpus.query(query, "TMP", false); //$NON-NLS-1$
					if (rez.getNMatch() == 2) {
						Match m = rez.getMatch(0);
						Match n = rez.getMatch(1);

						TXMPreferences alternative = BackToMediaPreferences.getInstance();
						// try reading project specific preferences
						// TODO see with SJ how to implement the "command > project > preferences >
						// default preferences" preference order
						CorpusCommandPreferences commandPreferences = project.getCommandPreferences("backtomedia"); //$NON-NLS-1$
						String pTimeName = commandPreferences.get(BackToMediaPreferences.WORD_PROPERTY, alternative);

						Property timeProperty = cqpCorpus.getProperty(pTimeName);
						if (timeProperty == null) {
							Log.warning(NLS.bind(MessagesMP.ErrorNoWordPropertyP0FoundInCorpusP1, pTimeName, cqpCorpus));
							return null;
						}
						String start = m.getValueForProperty(timeProperty);
						String end = n.getValueForProperty(timeProperty);
						return backToMedia(cqpCorpus, text.getName(), start, end, editor, HandlerUtil.getActiveWorkbenchWindow(event).getShell());
					}
					else {
						Log.warning(NLS.bind(MessagesMP.FailToFindWordsWithIdsP0InTextWithIdP1, ids[0] + "|" + ids[ids.length - 1], text.getName())); //$NON-NLS-2$ //$NON-NLS-1$
						return null;
					}
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		// no suitable parameters or selection or editor
		return null;
	}



	/**
	 * Uses word ids + text + corpus to build a start-end word positions array and finally uses Media preferences
	 * 
	 * @param corpus
	 * @param textid
	 * @param wordids
	 * @param editor
	 * @param shell
	 * @return
	 * @throws CqiClientException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public static Object backtoMediaFromWordSelection(CQPCorpus corpus, String textid, String[] wordids, TXMEditor<? extends TXMResult> editor, Shell shell)
			throws CqiClientException, IOException, CqiServerError {
		if (wordids == null) return null;
		if (wordids.length != 2) return null;

		Match m = null;
		Match n = null;

		if (wordids[1] == null) {
			CQLQuery query = new CQLQuery(NLS.bind("[id=\"{0}\" & _.text_id=\"{1}\"]", wordids[0], CQLQuery.addBackSlash(textid))); //$NON-NLS-1$
			QueryResult rez = corpus.query(query, "TMP", false); //$NON-NLS-1$

			if (rez.getNMatch() == 0) {
				Log.warning(NLS.bind(MessagesMP.WarningFoundNoWordForIdP0InP1, wordids[0], textid));
				return null;
			}
			else if (rez.getNMatch() > 1) {
				Log.warning(NLS.bind(MessagesMP.WarningFoundMoreThanOneWordForIdP0InP1, wordids[0], textid));
			}

			m = rez.getMatch(0);
			n = rez.getMatch(0);
		}
		else if (wordids[0] != null && wordids[1] != null) {
			CQLQuery query = new CQLQuery(NLS.bind("[id=\"{0}\" & _.text_id=\"{1}\"]", wordids[0] + "|" + wordids[1], CQLQuery.addBackSlash(textid))); //$NON-NLS-1$ //$NON-NLS-2$
			QueryResult rez = corpus.query(query, "TMP", false); //$NON-NLS-1$

			if (rez.getNMatch() != 2) {
				Log.warning(NLS.bind(MessagesMP.WarningFoundMoreThanOrNoWordForIdP0P1InP2, wordids[0] + ", " + wordids[1], textid)); //$NON-NLS-2$ //$NON-NLS-1$
				return null;
			}

			m = rez.getMatch(0);
			n = rez.getMatch(1);
		}

		if (m == null || n == null) {
			Log.warning(NLS.bind(MessagesMP.FailToFindWordWithIdP0InTextWithIdP1, Arrays.toString(wordids), textid));
			return null;
		}
		else {
			return backtoMediaFromMatch(corpus, textid, m.getStart(), n.getEnd(), editor, shell);
		}
	}


	/**
	 * given a start-end word position build a segment to play using Media preferences of corpus or TXM preferences
	 * 
	 * @param corpus
	 * @param textid
	 * @param keywordPosition
	 * @param keywordEndPosition
	 * @param editor
	 * @param shell
	 * @return
	 * @throws CqiClientException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public static Object backtoMediaFromMatch(CQPCorpus corpus, String textid, int keywordPosition, int keywordEndPosition, TXMEditor<? extends TXMResult> editor, Shell shell)
			throws CqiClientException, IOException,
			CqiServerError {

		TXMPreferences alternative = BackToMediaPreferences.getInstance();
		String sStartTime, sEndTime;

		// try reading project specific preferences
		// TODO see with SJ how to implement the "command > project > preferences >
		// default preferences" preference order
		CorpusCommandPreferences commandPreferences = corpus.getProject().getCommandPreferences("backtomedia"); //$NON-NLS-1$

		String structurePropertyName = commandPreferences.get(BackToMediaPreferences.STRUCTURE, alternative);
		String startPropertyName = commandPreferences.get(BackToMediaPreferences.STRUCTURE_START_PROPERTY, alternative);
		String endPropertyName = commandPreferences.get(BackToMediaPreferences.STRUCTURE_END_PROPERTY, alternative);
		String sync_mode = commandPreferences.get(BackToMediaPreferences.SYNCMODE, alternative);
		String pTimeName = commandPreferences.get(BackToMediaPreferences.WORD_PROPERTY, alternative);
		int leftContext = commandPreferences.getInt(BackToMediaPreferences.WORD_CONTEXT_LEFT_DISTANCE, alternative);
		int rightContext = commandPreferences.getInt(BackToMediaPreferences.WORD_CONTEXT_RIGHT_DISTANCE, alternative);

		if (BackToMediaPreferences.WORDMODE.equals(sync_mode)) {

			if ("".equals(pTimeName)) pTimeName = "time"; //$NON-NLS-1$ //$NON-NLS-2$

			Property timeP = corpus.getProperty(pTimeName);
			if (timeP == null) {
				System.out.println(MessagesMP.BackToMedia_11 + pTimeName);
				return false;
			}

			int leftPosition = Math.max(keywordPosition - leftContext, 0);
			int rightPosition = Math.min(keywordPosition + rightContext, corpus.getSize());
			int[] positions = { leftPosition, rightPosition };
			String[] times = CQPSearchEngine.getCqiClient().cpos2Str(timeP.getQualifiedName(), positions);
			sStartTime = times[0];
			sEndTime = times[1];
		}
		else if (BackToMediaPreferences.STRUCTUREMODE.equals(sync_mode)) {
			StructuralUnit structure = corpus.getStructuralUnit(structurePropertyName);
			if (structure == null) {
				System.out.println(MessagesMP.bind(MessagesMP.BackToMedia_12, structurePropertyName));
				return null;
			}
			Property startProperty = structure.getProperty(startPropertyName);
			if (startProperty == null) {
				System.out.println(MessagesMP.bind(MessagesMP.BackToMedia_14, startPropertyName, structure.getName()));
				return null;
			}

			sStartTime = Match.getValueForProperty(startProperty, keywordPosition);
			Log.fine(MessagesMP.BackToMedia_16 + sStartTime);

			Property endProperty = structure.getProperty(endPropertyName);
			if (endProperty == null) {
				System.out.println(MessagesMP.bind(MessagesMP.BackToMedia_17, endPropertyName, structure.getName()));
				return null;
			}
			sEndTime = Match.getValueForProperty(endProperty, keywordEndPosition);
			Log.fine(MessagesMP.BackToMedia_19 + sEndTime);
		}
		else { // MILESTONE MODE
			StructuralUnit structure = corpus.getStructuralUnit(structurePropertyName);
			if (structure == null) {
				System.out.println(MessagesMP.bind(MessagesMP.BackToMedia_12, structurePropertyName));
				return null;
			}
			StructuralUnitProperty startProperty = structure.getProperty(startPropertyName);
			if (startProperty == null) {
				System.out.println(MessagesMP.bind(MessagesMP.BackToMedia_14, startPropertyName, structure.getName()));
				return null;
			}

			AbstractCqiClient cqiClient = CorpusManager.getCorpusManager().getCqiClient();
			int[] struc = cqiClient.cpos2Struc(startProperty.getQualifiedName(), new int[] { keywordPosition, keywordEndPosition });
			int[] struc2 = new int[] { struc[0], struc[1], struc[1] + 1 };
			String[] times = cqiClient.struc2Str(startProperty.getQualifiedName(), struc2);
			sStartTime = times[0];
			if (times[2] != null) {
				sEndTime = times[2];
			}
			else {
				sEndTime = times[1];
			}
		}

		return backToMedia(corpus, textid, sStartTime, sEndTime, editor, shell);
	}

	/**
	 * Play the text associated media from a start-end time position
	 * 
	 * @param corpus
	 * @param textid
	 * @param sStartTime String in "sec.msec" or "hh:mm:ss" format
	 * @param sEndTime String in "sec.msec" or "hh:mm:ss" format
	 * @param editor
	 * @param shell
	 * @return the media editor or null if an error occurred
	 */
	public static Object backToMedia(CQPCorpus corpus, String textid, String sStartTime, String sEndTime, TXMEditor<? extends TXMResult> editor, Shell shell) {

		TXMPreferences alternative = BackToMediaPreferences.getInstance();

		// try reading project specific preferences
		// TODO see with SJ how to implement the "command > project > preferences >
		// default preferences" preference order
		CorpusCommandPreferences commandPreferences = corpus.getProject().getCommandPreferences("backtomedia"); //$NON-NLS-1$

		String media_directory = commandPreferences.get(BackToMediaPreferences.MEDIA_PATH_PREFIX, alternative);
		boolean media_auth = "true".equals(commandPreferences.get(BackToMediaPreferences.MEDIA_AUTH, alternative)); //$NON-NLS-1$
		String secured_media_login = commandPreferences.get(BackToMediaPreferences.MEDIA_AUTH_LOGIN, alternative);
		String media_format = commandPreferences.get(BackToMediaPreferences.MEDIA_EXTENSION, alternative);
		String media_index = commandPreferences.get(BackToMediaPreferences.MEDIA_INDEX_NODE, alternative);

		try {
			Log.fine(MessagesMP.BackToMedia_7 + textid);

			File binDir = corpus.getProject().getProjectDirectory();
			String path = null;
			if (media_index != null && media_index.length() > 0) {
				IEclipsePreferences node = corpus.getProject().getPreferencesScope().getNode(media_index);
				String path2 = node.get(textid, null);
				File audioFile = new File(path2);
				if (path2.startsWith("http:/")) { //$NON-NLS-1$
					try {
						InputStream conn = audioFile.toURI().toURL().openStream();
						conn.close();
						path = path2;
					}
					catch (Exception e) {
						Log.info(MessagesMP.bind(MessagesMP.CouldNotOpenMediaFileP0, path2));
					}
				}
				else if (!audioFile.exists()) {
					Log.info(MessagesMP.bind(MessagesMP.MediaFileRegisterInTheIndexNotFoundP0, audioFile));
					return null;
				}
			}
			else if (((media_directory != null && media_directory.length() > 0) || (media_auth))
					&& media_format != null && media_format.length() > 0) {

				String path_prefix = media_directory.trim();
				media_format = media_format.trim();
				
				if (media_auth) {

					if (System.getProperty(BackToMediaPreferences.MEDIA_AUTH_PASSWORD) == null) {
						URL url = new URL(media_directory);

						UsernamePasswordDialog dialog = new UsernamePasswordDialog(shell, new boolean[] { true, true }, url.getHost());
						dialog.setUsername(secured_media_login);
						dialog.setTitle(MessagesMP.YouMustAuthenticateToAccessTheMediaFile);
						dialog.setDetails(NLS.bind(TXMUIMessages.loginToP0, new URL(media_directory).getHost()));
						if (dialog.open() == Window.OK) {
							System.setProperty(BackToMediaPreferences.MEDIA_AUTH_LOGIN, dialog.getUser());
							System.setProperty(BackToMediaPreferences.MEDIA_AUTH_PASSWORD, dialog.getPassword());
						}
						else {
							// System.setProperty(BackToMediaPreferences.MEDIA_AUTH_LOGIN, "");
							// System.setProperty(BackToMediaPreferences.MEDIA_AUTH_PASSWORD, "");
							return null;
						}
					}
				}

				File audioFile = new File(path_prefix, textid + "." + media_format); //$NON-NLS-1$
				if (path_prefix.startsWith("http")) { //$NON-NLS-1$
					URL url = null;
					try {
						url = new URL(path_prefix + textid + "." + media_format); //$NON-NLS-1$
						if (!(media_auth)) {
							InputStream conn = url.openStream();
							conn.close();
						}
						path = url.toString();
					}
					catch (Exception e) {
						Log.info(MessagesMP.bind(MessagesMP.CouldNotOpenMediaFileP0, url));
					}
				}
				else { // local file
					if (!new File(path_prefix).exists()) {
						Log.info(MessagesMP.bind(MessagesMP.MediaDirectoryNotFoundP0, path_prefix));
						return null;
					}
					else if (!audioFile.exists()) {
						Log.info(MessagesMP.bind(MessagesMP.MediaFileNotFoundP0, audioFile));
						return null;
					}
					path = audioFile.toURI().toString();
				}
			}
			else {
				File mediaDir = new File(binDir, "media"); //$NON-NLS-1$
				File audioFile = new File(mediaDir, textid + ".mp3"); //$NON-NLS-1$
				if (!audioFile.exists()) audioFile = new File(mediaDir, textid + ".ogg"); //$NON-NLS-1$
				if (!audioFile.exists()) audioFile = new File(mediaDir, textid + ".wav"); //$NON-NLS-1$
				if (!audioFile.exists()) audioFile = new File(mediaDir, textid + ".mp4"); //$NON-NLS-1$
				if (!audioFile.exists()) audioFile = new File(mediaDir, textid + ".avi"); //$NON-NLS-1$
				if (!audioFile.exists()) audioFile = new File(mediaDir, textid + ".mov"); //$NON-NLS-1$
				if (!audioFile.exists()) {
					System.out.println(NLS.bind(MessagesMP.BackToMedia_26, textid));
					System.out.println(NLS.bind(MessagesMP.BackToMedia_27, mediaDir));
					System.out.println(MessagesMP.BackToMedia_29);
					return null;
				}
				path = audioFile.toURI().toString();
			}

			Log.fine(MessagesMP.BackToMedia_25 + path);

			// System.out.println("Linked editors: "+associatedEditors);
			boolean alreadyOpened = false;
			MediaPlayerEditor mediaPlayerEditor = null;
			if (editor != null) {
				mediaPlayerEditor = associatedEditors.get(editor);
				if (mediaPlayerEditor == null || mediaPlayerEditor.getPlayer() == null || mediaPlayerEditor.getPlayer().isDisposed()) { // if there is no linked editor, try with the 
					mediaPlayerEditor = null; // nope this linked editor is not usable 
				}
				if (mediaPlayerEditor == null) { // if there is no linked editor, try with the 
					for (ITXMResultEditor<TXMResult> linkedEditor : editor.getLinkedEditors()) {
						if (associatedEditors.get(linkedEditor) != null && associatedEditors.get(linkedEditor).getPlayer() != null && !associatedEditors.get(linkedEditor).getPlayer().isDisposed()) {
							mediaPlayerEditor = associatedEditors.get(linkedEditor);
							break;
						}
					}
				}
				alreadyOpened = mediaPlayerEditor != null;
			}

			if (!alreadyOpened) {
				// System.out.println("new editor linked to "+ce);
				String user = null, mdp = null;
				if (media_auth) {
					// path_prefix = NLS.bind(media_directory, secured_media_login, secured_media_password);
					user = System.getProperty(BackToMediaPreferences.MEDIA_AUTH_LOGIN);
					mdp = System.getProperty(BackToMediaPreferences.MEDIA_AUTH_PASSWORD);
				}
				mediaPlayerEditor = OpenMediaPlayer.openEditor(path, sStartTime, sEndTime, true, user, mdp, true);

				// editor.getPlayer().setRepeat(true);
				if (mediaPlayerEditor == null) {
					Log.warning(MessagesMP.ErrorCouldNotOpenTheMediaPlayer);
					return null;
				}

				// move the editor in the window
				int position = BackToMediaPreferences.getInstance().getInt(BackToMediaPreferences.BACKTOMEDIA_POSITION);
				if (position == -2) {
					Point s = editor.getParent().getSize();
					if (s.x < s.y) {
						position = EModelService.ABOVE;
					}
					else {
						position = EModelService.RIGHT_OF;
					}
				}
				if (editor != null && position >= 0) {
					SWTEditorsUtils.moveEditor((EditorPart) editor, mediaPlayerEditor, position);
				}

				if (mediaPlayerEditor != null) {
					associatedEditors.put(editor, mediaPlayerEditor);
				}
			}
			else {
				mediaPlayerEditor.getPlayer().play(path, sStartTime, sEndTime);
			}

			if (path != null) {
				mediaPlayerEditor.setPartName(textid);
			}
			return mediaPlayerEditor;
		}
		catch (Exception e2) {
			System.out.println(MessagesMP.BackToMedia_30 + e2);
			Log.printStackTrace(e2);
			return null;
		}
	}
}
