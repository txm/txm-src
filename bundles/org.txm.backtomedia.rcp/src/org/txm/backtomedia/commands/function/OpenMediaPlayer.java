// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.backtomedia.commands.function;

import java.io.File;
import java.net.MalformedURLException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.backtomedia.editors.input.MediaPlayerEditorInput;
import org.txm.backtomedia.editors.player.MediaPlayerEditor;
import org.txm.backtomedia.preferences.BackToMediaPreferences;
import org.txm.rcp.StatusLine;
import org.txm.rcp.TXMWindows;
import org.txm.rcp.utils.SWTEditorsUtils;

import vlcplayerrcp.MessagesMP;

/**
 * open the Media player Editor with a selected media file. Path or URL
 * 
 * @author mdecorde.
 */
public class OpenMediaPlayer extends AbstractHandler {

	/** The ID. */
	public static String ID = OpenMediaPlayer.class.getSimpleName(); //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object o = HandlerUtil.getCurrentSelection(event);
		IStructuredSelection selection;
		if (o instanceof IStructuredSelection) {
			selection = (IStructuredSelection) o;
			Object s = selection.getFirstElement();
			if (s instanceof File && !((File) s).isDirectory() && !((File) s).getName().endsWith(".groovy")) { //$NON-NLS-1$

				File f = (File) s;

				try {
					return openEditor(f.toURI().toURL().toString(), 0);
				}
				catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		Shell sh = HandlerUtil.getActiveShell(event);
		SimpleFileDialog fd = new SimpleFileDialog(sh, MessagesMP.OpeningMediaFile, MessagesMP.EnterFilePathOrURL, BackToMediaPreferences.getInstance().getString(ID), null);
		// FileDialog fd = new FileDialog(new Shell(), SWT.SAVE);
		// fd.setText(MessagesMP.select_file);

		// String[] filterExt = { "*.*", "*.mp3", "*.ogg", "*.avi", "*.mp4", "*.mov"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		// fd.setFilterExtensions(filterExt);
		if (fd.open() == fd.OK) {
			String mrl = fd.getValue();

			if (mrl != null) {
				File f = new File(mrl);
				if (f.exists()) {
					mrl = new File(mrl).toURI().toString();
				}
				else {

				}
				BackToMediaPreferences.getInstance().put(ID, mrl);
				return openEditor(mrl, 0);
			}
		}

		return null;
	}

	public static MediaPlayerEditor openEditor() {
		IWorkbenchPage page = TXMWindows.getActiveWindow().getActivePage();
		MediaPlayerEditorInput editorInput = new MediaPlayerEditorInput();
		StatusLine.setMessage(MessagesMP.opening_media);
		try {
			IEditorPart editor = SWTEditorsUtils.openEditor(editorInput, MediaPlayerEditor.ID);
			if (editor != null && editor instanceof MediaPlayerEditor) {
				return (MediaPlayerEditor) editor;
			}
			else {
				return null;
			}
		}
		catch (PartInitException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static MediaPlayerEditor openEditor(String mrl, int time) {

		return openEditor(mrl, time, -1, false, null, null);
	}

	public static MediaPlayerEditor openEditor(String mrl, int time, int endtime) {

		return openEditor(mrl, time, endtime, false, null, null);
	}

	public static MediaPlayerEditor openEditor(String path, int startTime, int endTime, boolean loop, String user, String mdp) {
		return openEditor(path, startTime, endTime, loop, user, mdp, false);
	}

	public static MediaPlayerEditor openEditor(String mrl, int time, int endtime, boolean loop, String user, String mdp, boolean lightInterface) {

		IWorkbenchPage page = TXMWindows.getActiveWindow().getActivePage();
		MediaPlayerEditorInput editorInput = new MediaPlayerEditorInput(mrl, time, endtime);
		editorInput.setLoop(loop);
		editorInput.setLightWeightInterface(lightInterface);
		editorInput.setCrendentials(user, mdp);

		StatusLine.setMessage(MessagesMP.opening_media);
		try {
			MediaPlayerEditor player = (MediaPlayerEditor) SWTEditorsUtils.openEditor(editorInput, MediaPlayerEditor.ID);
			return player;
		}
		catch (PartInitException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static MediaPlayerEditor openEditor(String path, String sStartTime, String sEndTime) {
		return openEditor(path, sStartTime, sEndTime, false, null, null);
	}

	public static MediaPlayerEditor openEditor(String path, String sStartTime, String sEndTime, boolean loop, String user, String mdp) {
		return openEditor(path, sStartTime, sEndTime, loop, user, mdp, false);
	}

	public static MediaPlayerEditor openEditor(String path, String sStartTime, String sEndTime, boolean loop, String user, String mdp, boolean lightInterface) {

		IWorkbenchPage page = TXMWindows.getActiveWindow().getActivePage();
		MediaPlayerEditorInput editorInput = new MediaPlayerEditorInput(path, sStartTime, sEndTime);
		editorInput.setLoop(loop);
		editorInput.setLightWeightInterface(lightInterface);
		editorInput.setCrendentials(user, mdp);

		StatusLine.setMessage(MessagesMP.opening_media);
		try {
			IEditorPart editor = SWTEditorsUtils.openEditor(editorInput, MediaPlayerEditor.ID);
			if (editor == null) {
				return null;
			}

			return (MediaPlayerEditor) editor;
		}
		catch (PartInitException e) {
			e.printStackTrace();
		}

		return null;
	}
}
