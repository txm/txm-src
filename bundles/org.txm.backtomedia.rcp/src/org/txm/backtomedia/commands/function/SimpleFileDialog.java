package org.txm.backtomedia.commands.function;

import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

public class SimpleFileDialog extends InputDialog {

	Button selectButton;

	String defaultPath = null;

	public SimpleFileDialog(Shell parentShell, String dialogTitle, String dialogMessage, String initialValue, IInputValidator validator) {
		super(parentShell, dialogTitle, dialogMessage, initialValue, validator);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite p = (Composite) super.createDialogArea(parent);

		GridLayout layout = (GridLayout) p.getLayout();
		layout.numColumns = layout.numColumns + 1;

		getText().setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false, 2, 1));
		if (defaultPath != null) {
			getText().setText(defaultPath);
		}
		setErrorMessage(null);
		selectButton = new Button(p, SWT.PUSH);
		selectButton.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, true));
		selectButton.setText("..."); //$NON-NLS-1$
		selectButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(e.display.getActiveShell());
				String path = dialog.open();
				if (path != null) {
					SimpleFileDialog.this.getText().setText(path);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		return p;
	}
}
