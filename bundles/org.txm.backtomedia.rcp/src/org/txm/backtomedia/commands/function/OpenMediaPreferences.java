package org.txm.backtomedia.commands.function;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.osgi.service.prefs.BackingStoreException;
import org.txm.objects.CorpusCommandPreferences;
import org.txm.searchengine.cqp.corpus.MainCorpus;

import vlcplayerrcp.MessagesMP;

public class OpenMediaPreferences extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection isel = HandlerUtil.getCurrentSelection(event);
		if (isel instanceof StructuredSelection) {
			StructuredSelection sel = (StructuredSelection) isel;
			Object o = sel.getFirstElement();
			if (o instanceof MainCorpus) {
				openPreferences((MainCorpus) o);
			}
		}

		return null;
	}

	public static boolean openPreferences(MainCorpus corpus) {
		Display d = Display.getCurrent();
		if (d == null) {
			return false;
		}
		Shell s = d.getActiveShell();
		MediaPreferencesDialog dialog = new MediaPreferencesDialog(s, corpus);
		if (dialog.open() == MediaPreferencesDialog.OK) {
			CorpusCommandPreferences commandPreferences = corpus.getProject().getCommandPreferences("backtomedia");//$NON-NLS-1$
			for (String p : MediaPreferencesDialog.properties) {
				commandPreferences.set(p, dialog.getProperty(p));
			}
			try {
				commandPreferences.getNode().flush();
			}
			catch (BackingStoreException e) {
				e.printStackTrace();
				return false;
			}

			System.out.println(MessagesMP.MediaPreferencesUpdatedWithP0 + dialog.getProperties());
			return true;
		}
		else {
			System.out.println(MessagesMP.Canceled);
			return false;
		}
	}
}
