package org.txm.backtomedia.commands.function;

import java.util.HashMap;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.txm.backtomedia.preferences.BackToMediaPreferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.objects.CorpusCommandPreferences;
import org.txm.searchengine.cqp.corpus.MainCorpus;

import vlcplayerrcp.MessagesMP;

public class MediaPreferencesDialog extends Dialog {

	public static String[] properties = { BackToMediaPreferences.REPEAT,
			BackToMediaPreferences.STRUCTURE_START_PROPERTY,
			BackToMediaPreferences.STRUCTURE_END_PROPERTY,
			BackToMediaPreferences.STRUCTURE,
			BackToMediaPreferences.WORD_PROPERTY,
			BackToMediaPreferences.WORD_CONTEXT_LEFT_DISTANCE,
			BackToMediaPreferences.WORD_CONTEXT_RIGHT_DISTANCE,
			BackToMediaPreferences.SYNCMODE,
			BackToMediaPreferences.MEDIA_PATH_PREFIX,
			BackToMediaPreferences.MEDIA_EXTENSION,
			BackToMediaPreferences.MEDIA_AUTH,
			BackToMediaPreferences.MEDIA_AUTH_LOGIN };

	HashMap<String, Text> fields = new HashMap<String, Text>();

	HashMap<String, String> values = new HashMap<String, String>();

	MainCorpus corpus;

	protected MediaPreferencesDialog(Shell parentShell, MainCorpus corpus) {
		super(parentShell);
		this.corpus = corpus;
	}

	public HashMap<String, String> getProperties() {
		return values;
	}

	public String getProperty(String p) {
		return values.get(p);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		Composite c = new Composite(container, SWT.NONE);
		c.setLayout(new GridLayout(3, false));

		CorpusCommandPreferences commandPreferences = corpus.getProject().getCommandPreferences("backtomedia");//$NON-NLS-1$
		TXMPreferences alternative = BackToMediaPreferences.getInstance();

		for (String p : properties) {
			new Label(c, SWT.NONE).setText(p);
			final Text t = new Text(c, SWT.BORDER);

			GridData gd = new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1);
			gd.minimumWidth = 200;
			t.setLayoutData(gd);
			t.setText(commandPreferences.get(p, alternative));
			t.setToolTipText(NLS.bind(MessagesMP.valueOfP0, p));
			if (p.contains("directory")) { //$NON-NLS-1$
				t.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 1, 1));
				Button b = new Button(c, SWT.PUSH);
				b.setText("..."); //$NON-NLS-1$
				b.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						DirectoryDialog fd = new DirectoryDialog(MediaPreferencesDialog.this.getDialogArea().getShell(), SWT.OPEN);
						fd.setFilterPath(t.getText());
						String path = fd.open();
						if (path != null) {
							t.setText(path);
						}
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});
			}
			fields.put(p, t);
		}
		return container;
	}

	// overriding this methods allows you to set the
	// title of the custom dialog
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(MessagesMP.MediaPreferences);
	}

	// @Override
	// protected Point getInitialSize() {
	// return new Point(450, 300);
	// }

	@Override
	protected void okPressed() {
		for (String p : properties) {
			values.put(p, fields.get(p).getText());
		}
		super.okPressed();
	}
}
