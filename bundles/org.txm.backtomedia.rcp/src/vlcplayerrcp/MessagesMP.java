package vlcplayerrcp;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

public class MessagesMP extends NLS {

	private static final String BUNDLE_NAME = "vlcplayerrcp.messages"; //$NON-NLS-1$

	public static String Canceled;

	public static String MediaPreferences;

	public static String MediaPreferencesUpdatedWithP0;

	public static String BackToMedia_0;

	public static String BackToMedia_1;

	public static String BackToMedia_11;

	public static String BackToMedia_12;

	public static String BackToMedia_14;

	public static String BackToMedia_16;

	public static String BackToMedia_17;

	public static String BackToMedia_19;

	public static String BackToMedia_25;

	public static String BackToMedia_26;

	public static String BackToMedia_27;

	public static String BackToMedia_29;

	public static String BackToMedia_30;

	public static String BackToMedia_7;

	public static String BackToMediaPosition;

	public static String BackToMediaPreferencePage_1;

	public static String BackToMediaPreferencePage_2;

	public static String BackToMediaPreferencePage_3;

	public static String BackToMediaPreferencePage_4;

	public static String BackToMediaPreferencePage_5;

	public static String BackToMediaPreferencePage_6;

	public static String BackToMediaPreferencePage_7;

	public static String cancel;

	public static String changeThePlayRate;

	public static String CouldNotOpenMediaFileP0;

	public static String EmbeddedPlayer;

	public static String EnterFilePathOrURL;

	public static String ErrorCorpusWithIDP0NotFound;

	public static String ErrorCouldNotOpenTheMediaPlayer;

	public static String ErrorNoWordPropertyP0FoundInCorpusP1;

	public static String error_native_libs;

	public static String error_open_media;

	public static String FailToFindWordsWithIdsP0InTextWithIdP1;

	public static String FailToFindWordWithIdP0InTextWithIdP1;

	public static String FailToOpenMediaForParametersCorpusP0TextP1WordP2P3;

	public static String goBackwardOf10s;

	public static String goForwardOf10s;

	public static String ImpossibleToFindVLC;

	public static String ImpossibleToUseYourVLCP0;

	public static String LeftLength;

	public static String loginToP0;

	public static String MediaDirectoryNotFoundP0;

	public static String MediaFileNotFoundP0;

	public static String MediaFileRegisterInTheIndexNotFoundP0;

	public static String Minus10S;

	public static String openANewMedia;

	public static String opening;

	public static String opening_media;

	public static String OpeningMediaFile;

	public static String Opennnn;

	public static String pause;

	public static String play;

	public static String Pause;

	public static String Play;

	public static String PlayingMedia;

	public static String Plus10S;

	public static String playing;

	public static String rate;

	public static String repeat;

	public static String resume;

	public static String Rate10;

	public static String Rate1f;

	public static String RightLength;

	public static String select_file;

	public static String select_file_title;

	public static String select_object;

	public static String stop;

	public static String stopPlayingTheCurrentMedia;

	public static String Stop;

	public static String time_range;

	public static String volume;

	public static String VolumeP0;

	public static String WarningFoundMoreThanOneWordForIdP0InP1;

	public static String WarningFoundMoreThanOrNoWordForIdP0P1InP2;

	public static String WarningFoundNoWordForIdP0InP1;

	public static String YouMustAuthenticateToAccessTheMediaFile;

	public static String valueOfP0;

	public static String mediaPlayer;
	public static String positionOfPlayerOpenedByReturnToMedia;


	static {
		Utf8NLS.initializeMessages(BUNDLE_NAME, MessagesMP.class);
	}

}
