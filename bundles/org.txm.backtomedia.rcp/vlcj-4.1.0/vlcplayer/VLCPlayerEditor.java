package org.txm.backtomedia.editors.vlcplayer;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

public class VLCPlayerEditor extends EditorPart {
	
	/** The Constant ID. */
	public static final String ID = VLCPlayerEditor.class.getCanonicalName(); // $NON-NLS-1$
	
	VLCPlayer player;
	
	@Override
	public void createPartControl(Composite parent) {
		player = new VLCPlayer(parent, SWT.NONE);
	}
	
	public void init() {
		
	}
	
	@Override
	public void setFocus() {
		
	}
	
	@Override
	public void dispose() {
		try {
			if (player != null) player.stop();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		super.dispose();
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#doSaveAs()
	 */
	@Override
	public void doSaveAs() {}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#init(org.eclipse.ui.IEditorSite, org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setSite(site);
		setInput(input);
		// VLCPlayerEditorInput ii = (VLCPlayerEditorInput) input;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}
	
	public VLCPlayer getPlayer() {
		return player;
	}
	
	public void setPartName(String partName) {
		super.setPartName(partName);
	}
	
	@Override
	public boolean isDirty() {
		return false;
	}
	
	/**
	 * for more test use "player.getEmbeddedMediaPlayer()"
	 * 
	 * @return false if the media could not be loaded (file not found, access error...)
	 */
	public boolean isMediaLoaded() {
		return player.getEmbeddedMediaPlayer().status().isPlayable();
	}
}
