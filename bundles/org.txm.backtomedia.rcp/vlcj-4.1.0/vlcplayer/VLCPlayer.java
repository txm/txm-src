package org.txm.backtomedia.editors.vlcplayer;

import static uk.co.caprica.vlcj.binding.LibVlc.libvlc_new;
import static uk.co.caprica.vlcj.binding.LibVlc.libvlc_release;

import java.io.File;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Scale;
import org.txm.backtomedia.commands.function.TripleRangeSlider;
import org.txm.backtomedia.preferences.BackToMediaPreferences;
import org.txm.utils.logger.Log;

import com.sun.jna.StringArray;

import uk.co.caprica.vlcj.binding.LibC;
import uk.co.caprica.vlcj.binding.RuntimeUtil;
import uk.co.caprica.vlcj.binding.internal.libvlc_instance_t;
import uk.co.caprica.vlcj.factory.discovery.NativeDiscovery;
import uk.co.caprica.vlcj.factory.discovery.strategy.NativeDiscoveryStrategy;
import uk.co.caprica.vlcj.factory.swt.SwtMediaPlayerFactory;
import uk.co.caprica.vlcj.media.MediaRef;
import uk.co.caprica.vlcj.player.base.MediaPlayer;
import uk.co.caprica.vlcj.player.base.MediaPlayerEventAdapter;
import uk.co.caprica.vlcj.player.component.EmbeddedMediaPlayerComponent;
import uk.co.caprica.vlcj.player.embedded.EmbeddedMediaPlayer;
import uk.co.caprica.vlcj.player.embedded.videosurface.swt.CompositeVideoSurface;
import uk.co.caprica.vlcj.support.version.LibVlcVersion;
import vlcplayerrcp.MessagesMP;

public class VLCPlayer extends Composite {
	
	static { // force native libs discovery -> fix Mac OS X init
		NativeDiscovery discovery = new NativeDiscovery() {
			
			@Override
			protected void onFound(String path, NativeDiscoveryStrategy strategy) {
				Log.finer("VLC NativeDiscovery: onFound " + path + " " + strategy);
			}
			
			@Override
			protected void onNotFound() {
				Log.finer("VLC NativeDiscovery: Not found.");
			}
		};
		boolean found = discovery.discover();
		Log.finer("VLC NativeDiscovery: the discovery was succesfull ? " + found);
		if (found) {
			libvlc_instance_t instance = libvlc_new(0, new StringArray(new String[0]));
			Log.finer("VLC NativeDiscovery: VLC instance is " + instance);
			if (instance != null) {
				libvlc_release(instance);
			}
			else {
				Log.warning(NLS.bind("** Impossible to use your VLC [{0}] (please check that you have at least VLC version 3.0).", instance));
			}
			Log.finer("VLC NativeDiscovery: VLC version is " + new LibVlcVersion().getVersion());
		}
		else {
			Log.warning("** Impossible to find VLC (please check that you have at least VLC version 3.0).");
		}
	}
	
	protected static final String NOMEDIA = ""; //$NON-NLS-1$
	
	private EmbeddedMediaPlayer vlcPlayer;
	
	private Composite videoComposite;
	
	private CompositeVideoSurface playerCanvas;
	
	private Scale rateField;
	
	private Label rateValueLabel;
	
	private Scale volumeField;
	
	private Label volumeValueLabel;
	
	private Button playButton;
	
	private Button repeatButton;
	
	protected String currentlyPlayed = ""; //$NON-NLS-1$
	
	// private String startTime, endTime;
	private int start, end;
	
	protected boolean hasEnded = false;
	
	private String previouslyPlayed = ""; //$NON-NLS-1$
	
	private boolean repeat = false;
	
	protected int volume = 100;
	
	private Button stopButton;
	
	Label timeLabel;
	
	TripleRangeSlider timeRange;
	
	boolean firstLengthEvent = true;
	
	long previous = 0;
	
	long time, mins, secs;
	
	private SwtMediaPlayerFactory factory;
	
	
	static {
		// uncomment to enable VLC logs
		// Logger.setLevel(Level.TRACE);
	}
	
	public EmbeddedMediaPlayer getEmbeddedMediaPlayer() {
		return vlcPlayer;
	}
	
	public VLCPlayer(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(11, false));
		
		// THE PLAYER
		// if (RuntimeUtil.isMac()) {
		// try {
		// LibC.INSTANCE.setenv("VLC_PLUGIN_PATH", "/Applications/VLC.app/Contents/MacOS/plugins", 1);
		// }
		// catch (Exception ex) {
		// ex.printStackTrace();
		// }
		// }
		videoComposite = new Composite(this, SWT.EMBEDDED | SWT.NO_BACKGROUND);
		GridData gdata = new GridData(SWT.FILL, SWT.FILL, true, true);
		gdata.horizontalSpan = 11;
		videoComposite.setLayoutData(gdata);
		
		factory = new SwtMediaPlayerFactory();
		playerCanvas = factory.swt().newCompositeVideoSurface(videoComposite);
		
		EmbeddedMediaPlayerComponent e = new EmbeddedMediaPlayerComponent();
		vlcPlayer = e.mediaPlayer();
		vlcPlayer.videoSurface().set(playerCanvas);
		
		videoComposite.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				// vlcPlayer.submit(new Runnable() {
				//
				// @Override
				// public void run() {
				// // vlcPlayer.controls().stop();
				// }
				// });
				vlcPlayer.controls().stop();
				vlcPlayer.release();
				factory.release();
			}
		});
		
		// THE CONTROL BUTTONS
		playButton = new Button(this, SWT.PUSH);
		GridData playLayoutData = new GridData(SWT.FILL, SWT.CENTER, false, false);
		playButton.setLayoutData(playLayoutData);
		playButton.setText(MessagesMP.play);
		playButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				vlcPlayer.submit(new Runnable() {
					
					@Override
					public void run() {
						if (currentlyPlayed.length() == 0) {
							selectMedia();
							playButton.setText(MessagesMP.pause);
						}
						else if (vlcPlayer.status().isPlaying()) {
							vlcPlayer.controls().pause();
							playButton.setText(MessagesMP.resume);
						}
						else if (hasEnded) {
							replay();
						}
						else {
							vlcPlayer.controls().play();
							playButton.setText(MessagesMP.pause);
						}
					}
				});
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		// Button browseButton = new Button(this,SWT.PUSH);
		// browseButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
		// browseButton.setText("Open...");
		// browseButton.addSelectionListener(new SelectionListener() {
		// @Override
		// public void widgetSelected(SelectionEvent e) {
		// selectMedia();
		// }
		//
		// @Override
		// public void widgetDefaultSelected(SelectionEvent e) {}
		// });
		
		stopButton = new Button(this, SWT.PUSH);
		stopButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		stopButton.setText(MessagesMP.stop);
		stopButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				stop();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		timeLabel = new Label(this, SWT.NONE);
		timeLabel.setText("00:00"); //$NON-NLS-1$
		
		timeRange = new TripleRangeSlider(this, SWT.None);
		timeRange.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		timeRange.setToolTipText(MessagesMP.time_range);
		timeRange.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				TripleRangeSlider.SELECTED_KNOB sk = timeRange.getLastSelectedKnob();
				switch (sk) {
					case UPPER:
						end = timeRange.getUpperValue();
						
						if (end < time) {
							// System.out.println("Upper changed: fix time");
							time = end;
							vlcPlayer.controls().setTime(time);
						}
						break;
					case LOWER:
						start = timeRange.getLowerValue();
						if (start > time) {
							// System.out.println("Lower changed: fix time");
							time = start;
							vlcPlayer.controls().setTime(time);
						}
						break;
					case MIDDLE:
						
						time = timeRange.getMiddleValue();
						// System.out.println("Middle changed: fix time: "+time);
						vlcPlayer.controls().setTime(time);
						break;
					default:
						// nothing
				}
				// System.out.println("time range: "+start+" -> "+end+" time="+time);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		repeatButton = new Button(this, SWT.CHECK);
		repeatButton.setText(MessagesMP.repeat);
		repeat = BackToMediaPreferences.getInstance().getBoolean(BackToMediaPreferences.REPEAT);
		repeatButton.setSelection(repeat);
		repeatButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				repeat = repeatButton.getSelection();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		Label l = new Label(this, SWT.NONE);
		l.setText(MessagesMP.rate);
		
		rateField = new Scale(this, SWT.BORDER);
		GridData gdata4 = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gdata4.widthHint = 100;
		rateField.setLayoutData(gdata4);
		rateField.setMaximum(140);
		rateField.setMinimum(70);
		rateField.setSelection(100);
		rateField.setPageIncrement(5);
		
		rateField.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				float rate = rateField.getSelection() / 100.0f;
				vlcPlayer.controls().setRate(rate);
				rateValueLabel.setText("" + rateField.getSelection() + "%");
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		rateValueLabel = new Label(this, SWT.NONE);
		rateValueLabel.setText("100%");//$NON-NLS-1$
		
		l = new Label(this, SWT.NONE);
		l.setText(MessagesMP.volume);
		
		volumeField = new Scale(this, SWT.BORDER);
		gdata4 = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gdata4.widthHint = 100;
		volumeField.setLayoutData(gdata4);
		volumeField.setMinimum(0);
		volumeField.setMaximum(100);
		volumeField.setSelection(volume);
		volumeField.setPageIncrement(5);
		volumeField.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				vlcPlayer.audio().setVolume(volumeField.getSelection());
				volume = volumeField.getSelection();
				volumeValueLabel.setText("" + volume + "%");
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		volumeValueLabel = new Label(this, SWT.NONE);
		volumeValueLabel.setText("100%");
		
		vlcPlayer.events().addMediaPlayerEventListener(new MediaPlayerEventAdapter() {
			
			@Override
			public void opening(MediaPlayer mediaPlayer) {
				Log.finer("Opening media...");
			}
			
			@Override
			public void finished(MediaPlayer mediaPlayer) {
				Log.finer("Finished playing media...");
				mediaPlayer.submit(new Runnable() {
					
					@Override
					public void run() {
						if (repeat) {
							replay();
						}
						else {
							hasEnded = true;
						}
					}
				});
			}
		});
		
		vlcPlayer.events().addMediaPlayerEventListener(new MediaPlayerEventAdapter() {
			
			@Override
			public void timeChanged(MediaPlayer mediaPlayer, final long arg1) {
				
				mediaPlayer.submit(new Runnable() {
					
					@Override
					public void run() {
						time = arg1;
						if (previous == time) {
							return;
						}
						previous = time;
						
						timeLabel.getDisplay().syncExec(new Runnable() {
							
							@Override
							public void run() {
								if (timeRange.isDisposed()) {
									return;
								}
								if (!timeRange.isDragMiddleKnob()) {
									timeRange.setMiddleValue((int) time);
								}
								updateTimeLabel();
								
								if (arg1 > end && end != start) {
									// System.out.println("Time > end :"+arg1 +" > "+end);
									if (repeat) {
										vlcPlayer.controls().setTime(start);
									}
									else {
										vlcPlayer.controls().stop();
									}
								}
							}
						});
					}
				});
			}
			
			@Override
			public void lengthChanged(MediaPlayer mediaPlayer, final long arg1) {
				
				mediaPlayer.submit(new Runnable() {
					
					@Override
					public void run() {
						if (firstLengthEvent) {
							firstLengthEvent = false;
							
							// initialize time range widget limits
							timeRange.getDisplay().syncExec(new Runnable() {
								
								@Override
								public void run() {
									if (timeRange.isDisposed()) return;
									timeRange.setMaximum((int) arg1);
									// if (start == end) end = (int)arg1;
									
									
									if (end > 0 && start != end) {
										timeRange.setUpperValue(end);
									}
									else {
										timeRange.setUpperValue((int) arg1);
									}
									
									timeRange.setLowerValue(start);
									// System.out.println("Range: "+start+" -> "+end+" song length "+arg1);
								}
							});
						}
					}
				});
			}
		});
	}
	
	private void updateTimeLabel() {
		mins = time / 60000;
		secs = (time / 1000) % 60;
		timeLabel.setText(String.format("%02d:%02d", mins, secs)); //$NON-NLS-1$
		timeLabel.update();
	}
	
	protected void replay() {
		if (currentlyPlayed.length() > 0) {
			// this.play(currentlyPlayed, startTime, endTime);
			vlcPlayer.submit(new Runnable() {
				
				@Override
				public void run() {
					vlcPlayer.controls().setTime(start);
				}
			});
			
			playButton.setText(MessagesMP.pause);
		}
	}
	
	protected void selectMedia() {
		Log.fine(MessagesMP.select_file);
		
		FileDialog fd = new FileDialog(VLCPlayer.this.getShell(), SWT.OPEN);
		fd.setText(MessagesMP.select_file_title);
		File f = new File(previouslyPlayed);
		if (f.isDirectory()) fd.setFilterPath(f.getPath());
		else fd.setFilterPath(f.getParent());
		
		String[] filterExt = { "*.*", "*.mp3", "*.mp4", "*.avi" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		fd.setFilterExtensions(filterExt);
		String selected = fd.open();
		if (selected == null) {
			System.out.println(MessagesMP.cancel);
			return;
		}
		
		currentlyPlayed = selected;
		previouslyPlayed = selected;
		Log.fine(MessagesMP.opening + currentlyPlayed);
		play(currentlyPlayed, 0, 0);
	}
	
	/**
	 * 
	 * @param mrl
	 * @param time msec start time
	 * @param endtime msec end time
	 */
	public void play(String mrl, int time, int endtime) {
		Log.fine(MessagesMP.bind(MessagesMP.playing, new Object[] { mrl, time, endtime }));
		play(mrl, "" + time / 1000.0f, "" + endtime / 1000.0f); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	public void play(String mrl, int time) {
		play(mrl, time, time);
	}
	
	public void hideStopButton() {
		if (this.stopButton != null && !this.stopButton.isDisposed()) {
			this.stopButton.dispose();
		}
	}
	
	DateTimeFormatter hhmmssFormatter = DateTimeFormatter.ISO_LOCAL_TIME;
	
	/**
	 * 
	 * @param mrl
	 * @param startTime "0.0" or ""hh:mm:ss" format
	 * @param endTime "0.0" or ""hh:mm:ss" format
	 */
	public boolean play(String mrl, String startTime, String endTime) {
		
		if (startTime.matches("[0-9]+.[0-9]+")) {
			start = (int) (1000 * Float.parseFloat(startTime));
		}
		else if (startTime.matches("[0-9]+:[0-9]+:[0-9]+")) {
			if (startTime.indexOf(":") == 1) {
				startTime = "0" + startTime;
			}
			LocalTime time1 = LocalTime.parse(startTime, hhmmssFormatter);
			start = 1000 * ((time1.getHour() * 60 * 60) + (time1.getMinute() * 60) + time1.getSecond());
		}
		
		if (endTime.matches("[0-9]+.[0-9]+")) {
			end = (int) (1000 * Float.parseFloat(endTime));
		}
		else if (endTime.matches("[0-9]+:[0-9]+:[0-9]+")) {
			if (endTime.indexOf(":") == 1) endTime = "0" + endTime;
			LocalTime time1 = LocalTime.parse(endTime, hhmmssFormatter);
			end = 1000 * ((time1.getHour() * 60 * 60) + (time1.getMinute() * 60) + time1.getSecond());
		}
		
		// if (start == end) end = -1;
		
		Log.finer(MessagesMP.bind(MessagesMP.playing, new Object[] { mrl, startTime, endTime }));
		// vlcPlayer.submit(new Runnable() {
		//
		// @Override
		// public void run() {
		String[] options = { " :live-caching=200" }; // reduce video stream cache duration to 200ms
		
		Log.finer("Preparing media...");
		if (!vlcPlayer.media().prepare(mrl, options)) {
			Log.finer("** Impossible to prepare media.");
			return false;
		}
		
		Log.finer("Is media valid ?");
		if (!vlcPlayer.media().isValid()) {
			Log.finer("** Invalid media.");
			return false;
		}
		
		Log.info("Playing media...");
		vlcPlayer.controls().play();
		vlcPlayer.audio().setVolume(volume);
		vlcPlayer.controls().setTime(start);
		
		if (new File(mrl + ".srt").exists()) { //$NON-NLS-1$
			vlcPlayer.subpictures().setSubTitleFile(mrl + ".srt"); //$NON-NLS-1$
		}
		currentlyPlayed = mrl;
		previouslyPlayed = mrl;
		
		if (!playButton.isDisposed()) {
			playButton.setText(MessagesMP.pause);
		}
		firstLengthEvent = true;
		// }
		// });
		return true;
	}
	
	public void setRepeat(boolean repeat) {
		this.repeat = repeat;
		repeatButton.setSelection(repeat);
	}
	
	public void stop() {
		vlcPlayer.controls().stop();
		currentlyPlayed = NOMEDIA;
		if (!playButton.isDisposed()) {
			playButton.setText(MessagesMP.play);
		}
	}
}
