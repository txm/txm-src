package org.txm.partition.rcp.chartsengine.events;

import java.awt.AWTEvent;
import java.awt.event.MouseEvent;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.txm.chartsengine.rcp.events.EventCallBack;
import org.txm.edition.rcp.handlers.OpenEdition;
import org.txm.partition.core.functions.PartitionDimensions;
import org.txm.partition.rcp.editors.PartitionDimensionsEditor;
import org.txm.searchengine.cqp.corpus.Part;


public class PartitionDimensionsCallback extends EventCallBack<PartitionDimensionsEditor> {

	public PartitionDimensionsCallback() {
		// TODO Auto-generated constructor stub
	}

	public void processEvent(final Object event, final int eventArea, final Object o) {

		// Need to run this in the SWT UI thread because it's called from the AWT UI thread in TBX charts engine layer
		chartEditor.getComposite().getDisplay().asyncExec(new Runnable() {

			@Override
			public void run() {

				AWTEvent awtEvent = (AWTEvent) event;

				// Mouse event
				if (event instanceof MouseEvent) {

					MouseEvent mouseEvent = (MouseEvent) event;

					// return if it's not the left mouse button
					if (mouseEvent.getButton() != MouseEvent.BUTTON1) {
						return;
					}


					ChartMouseEvent chartEvent = (ChartMouseEvent) o;
					ChartEntity entity = chartEvent.getEntity();

					if (awtEvent.getID() == MouseEvent.MOUSE_CLICKED && mouseEvent.getClickCount() == 2 && eventArea == EventCallBack.AREA_ITEM) {

						if (entity instanceof XYItemEntity xyentity) {
							int item = xyentity.getItem();
							//int series = xyentity.getSeriesIndex();
							try {
								PartitionDimensions pd = chartEditor.getResult();
								Part p = pd.getParts(pd.isSortingBySize()).get(item);
								OpenEdition.openEdition(p);
							}
							catch (Exception e) {
								e.printStackTrace();
							}
						}

					}
				}

			}
		});
	}

}
