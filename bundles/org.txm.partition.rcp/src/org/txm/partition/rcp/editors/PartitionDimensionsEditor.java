package org.txm.partition.rcp.editors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.ToolItem;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.core.results.Parameter;
import org.txm.partition.core.functions.PartitionDimensions;
import org.txm.partition.core.preferences.PartitionDimensionsPreferences;
import org.txm.partition.rcp.messages.PartitionUIMessages;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;

/**
 * Partition dimensions chart editor.
 * 
 * @author sjacquot
 *
 */
public class PartitionDimensionsEditor extends ChartEditor<PartitionDimensions> {


	/**
	 * To sort or not the parts by size.
	 */
	@Parameter(key = PartitionDimensionsPreferences.CHART_DIMENSIONS_SORT_BY_SIZE)
	protected ToolItem sortByPartSize;



	@Override
	public void __createPartControl() {

		// remove the compute button
		//this.removeComputeButton();

		// extend the tool bar
		new ToolItem(this.getToolBar(), SWT.SEPARATOR);
		this.sortByPartSize = new ToolItem(this.getToolBar(), SWT.CHECK);
		this.sortByPartSize.setImage(IImageKeys.getImage(this.getClass(), "icons/silk_sort_by_size.png")); //$NON-NLS-1$
		this.sortByPartSize.setToolTipText(PartitionUIMessages.sortByPartSize);

		this.sortByPartSize.addSelectionListener(new ComputeSelectionListener(this, true));


	}


	@Override
	public void updateEditorFromChart(boolean update) {
		// nothing to do
	}


}
