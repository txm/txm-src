// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.partition.rcp.adapters;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.FrameworkUtil;
import org.txm.partition.core.functions.PartitionDimensions;
import org.txm.rcp.adapters.TXMResultAdapter;
import org.txm.rcp.adapters.TXMResultAdapterFactory;


/**
 * A factory for creating Adapter objects.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class PartitionDimensionsAdapterFactory extends TXMResultAdapterFactory {


	public static final ImageDescriptor ICON = AbstractUIPlugin.imageDescriptorFromPlugin(FrameworkUtil.getBundle(PartitionDimensionsAdapterFactory.class).getSymbolicName(),
			"platform:/plugin/" + FrameworkUtil.getBundle(PartitionDimensionsAdapterFactory.class).getSymbolicName() + "/icons/dimensions.png"); //$NON-NLS-1$ //$NON-NLS-2$


	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (this.canAdapt(adapterType) && adaptableObject instanceof PartitionDimensions) {
			return new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return ICON;
				}
			};
		}
		return null;
	}


}
