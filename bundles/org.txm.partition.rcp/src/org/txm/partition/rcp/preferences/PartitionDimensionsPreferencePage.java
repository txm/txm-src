// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.partition.rcp.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.partition.core.messages.PartitionCoreMessages;
import org.txm.partition.core.preferences.PartitionDimensionsPreferences;
import org.txm.partition.rcp.adapters.PartitionDimensionsAdapterFactory;
import org.txm.partition.rcp.messages.PartitionUIMessages;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;

/**
 * Partition preference page.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class PartitionDimensionsPreferencePage extends TXMPreferencePage {

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		// Charts rendering
		Composite chartsTab = SWTChartsComponentsProvider.createChartsRenderingPreferencesTabFolderComposite(this.getFieldEditorParent(), PartitionUIMessages.dimensionsChartRendering);

		this.addField(new BooleanFieldEditor(PartitionDimensionsPreferences.CHART_DIMENSIONS_SORT_BY_SIZE, PartitionUIMessages.sortByPartSize, chartsTab));
		this.addField(new BooleanFieldEditor(PartitionDimensionsPreferences.CHART_DIMENSIONS_DISPLAY_PARTS_COUNT_IN_TITLE, PartitionUIMessages.displayPartsCountInChartTitle, chartsTab));

		// other shared preferences
		SWTChartsComponentsProvider.createChartsRenderingPreferencesFields(this, chartsTab);

	}

	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(PartitionDimensionsPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setTitle(PartitionCoreMessages.RESULT_TYPE);
		this.setImageDescriptor(PartitionDimensionsAdapterFactory.ICON);


		//		//FIXME: description
		//		//setDescription("Progression");
		//		this.setTitle(PartitionDimensions.RESULT_TYPE);
		//
		//
		//		//FIXME: description
		//		//setDescription("Progression");
		//		this.setTitle(ProgressionCoreMessages.RESULT_TYPE);
		//		this.setImageDescriptor(ProgressionAdapterFactory.ICON);
		//


	}
}
