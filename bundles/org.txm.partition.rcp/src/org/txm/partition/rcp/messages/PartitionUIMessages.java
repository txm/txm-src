package org.txm.partition.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;


/**
 * Partition UI Messages.
 * 
 * @author sjacquot
 *
 */
public class PartitionUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.partition.rcp.messages.messages"; //$NON-NLS-1$

	public static String displayPartsCountInChartTitle;

	public static String sortByPartSize;

	public static String dimensionsChartRendering; // "Dimensions chart rendering"

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, PartitionUIMessages.class);
	}

	private PartitionUIMessages() {
	}

}
