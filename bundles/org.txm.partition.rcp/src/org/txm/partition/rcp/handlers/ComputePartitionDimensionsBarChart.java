// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.partition.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.partition.core.functions.PartitionDimensions;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.searchengine.cqp.corpus.Partition;


/**
 * Creates and opens a bar chart editor with the dimensions of the selected partition.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class ComputePartitionDimensionsBarChart extends BaseAbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkCorpusEngine()) {
			return false;
		}

		if (!this.checkStatsEngine()) {
			return false;
		}


		Object selection = this.getCorporaViewSelectedObject(event);
		PartitionDimensions partitionDimensions = null;

		// Creating from Partition
		if (selection instanceof Partition) {
			partitionDimensions = new PartitionDimensions((Partition) selection);
		}
		// Reopening from existing result
		else if (selection instanceof PartitionDimensions) {
			partitionDimensions = (PartitionDimensions) selection;
		}
		// Error
		else {
			return super.logCanNotExecuteCommand(selection);
		}

		ChartEditor.openEditor(partitionDimensions);



		// FIXME: tests to Load Java embedded fonts from JRE to SWT		
		//		Display.getDefault().syncExec(new Runnable() {
		//		    public void run() {
		//
		//				File javaFontsPath = new File(System.getProperty("java.home")
		//						+ System.getProperty("file.separator") + "lib"
		//						+ System.getProperty("file.separator") + "fonts"
		//						+ System.getProperty("file.separator"));
		//
		//				String[] fontFiles = javaFontsPath.list(); 
		//				
		//				for (int i = 0; i < fontFiles.length; i++) {
		//					
		//					if(fontFiles[i].endsWith(".ttf"))	{ //$NON-NLS-1$
		//						
		//						File fontFile = new File(javaFontsPath, fontFiles[i]);
		//						
		//						Log.finest("Loading Java font to SWT Device from file " + fontFile + "...");
		//					
		//						// Load the font in SWT
		//						
		//						Display.getCurrent().loadFont(fontFile.getAbsolutePath());
		//					}
		//					
		//				}
		//		    }
		//		});

		return null;
	}

}
