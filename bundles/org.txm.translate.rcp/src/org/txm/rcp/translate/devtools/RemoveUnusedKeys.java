package org.txm.rcp.translate.devtools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.TreeMap;

import org.txm.rcp.translate.i18n.PluginMessagesManager;
import org.txm.rcp.translate.i18n.WorkspaceMessagesManager;

/**
 * Checks that all key defined in Messages.java class of a bundle are used in the source files AND removed the unused keys
 * 
 * @author sjacquot
 *
 */
public class RemoveUnusedKeys {
	
	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		WorkspaceMessagesManager wmm = new WorkspaceMessagesManager();
		
		TreeMap<String, LinkedHashSet<File>> usedKeys = wmm.getUsedKeysFilesIndex();
		System.out.println("all keys: "+usedKeys.keySet()); //$NON-NLS-1$
		int total = 0;
		for (PluginMessagesManager pm : wmm.getPluginMessages().values()) {
			//System.out.println("pm keys: "+pm.getProjectDirectory()+" "+pm.getUsedKeysFilesIndex().keySet());
			int n = 0;
			ArrayList<String> keys = new ArrayList<String>(pm.getMessageKeys());
//			System.out.println("N keys="+keys.size());
			ArrayList<String> removedKeys = new ArrayList<String>();
			
			for (String key : keys) {
				if (!usedKeys.containsKey(pm.getKeyFullName(key))) {
					pm.removeKey(key);
					removedKeys.add(key);
					n++;
				}
			}
			
			if (n > 0) {
				System.out.println(pm.getMessageFullName()+" "+n+" key removed: "+removedKeys); //$NON-NLS-1$
				pm.saveChanges(true);
				total += n;
			}
		}
		
		if (total > 0) {
			System.out.println("Done: "+total+ " changes.");
		} else {
			System.out.println("No change.");
		}
	}
}
