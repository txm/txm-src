package org.txm.rcp.translate.devtools;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Properties;
import java.util.TreeSet;

import org.odftoolkit.odfdom.doc.OdfDocument;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.odftoolkit.odfdom.doc.table.OdfTable;
import org.txm.libs.office.WriteODS;
import org.txm.rcp.translate.i18n.PluginMessagesManager;
import org.txm.utils.io.IOUtils;

public class CreateMessagesTable {
	
	File dir;
	String lang;
	
	public CreateMessagesTable(String lang) {
		this(new File(System.getProperty("user.dir")), lang);
	}
	
	public CreateMessagesTable(File dir, String lang) {
		this.dir = dir;
		this.lang = lang;
	}
	
	public boolean createODT(String prefix, File output) throws Exception {
		
		if (!dir.exists()) return false;
		File defaultPropertiesFile = new File(dir, prefix+".properties");
		if (!defaultPropertiesFile.exists()) return false;
		
		File langPropertiesFile = new File(dir, prefix+"_"+lang+".properties");
		
		System.out.println("Building table...");
		LinkedHashMap<String, String> master = new LinkedHashMap<String, String>();
		String encoding = "UTF-8";
		if (prefix.startsWith("bundle")) {
			encoding = "iso-8859-1";
		}
		master = PluginMessagesManager.loadFromProperties(IOUtils.getReader(defaultPropertiesFile, encoding));
		
		LinkedHashMap<String, String> slave = new LinkedHashMap<String, String>();
		slave = PluginMessagesManager.loadFromProperties(IOUtils.getReader(langPropertiesFile, encoding));
		
		OdfTextDocument doc = OdfTextDocument.newTextDocument();
		
		TreeSet<Object> keys = new TreeSet<Object>(PluginMessagesManager.comp);
		keys.addAll(master.keySet());
		
		OdfTable table = OdfTable.newTable(doc, keys.size()+1,4);// doc.addTable(keys.size()+1,4);
		editTable(doc, table, keys, master, slave);
		
		System.out.println("Saving...");
		WriteODS.save(doc, output);
		System.out.println("Done.");
		
		return true;
	}
	
	private void editTable(OdfDocument doc, OdfTable table, TreeSet<Object> keys, LinkedHashMap<String, String> master, LinkedHashMap<String, String> slave ) {
		
		table.getCellByPosition(0, 0).setStringValue("N");
		table.getCellByPosition(1, 0).setStringValue("KEY");
		table.getCellByPosition(2, 0).setStringValue("DEFAULT");
		table.getCellByPosition(3, 0).setStringValue(lang.toUpperCase());
		
		int n = 1;
		for (Object key : keys) {
			table.getCellByPosition(0, n).setStringValue(""+n);
			table.getCellByPosition(1, n).setStringValue(key.toString());
			table.getCellByPosition(2, n).setStringValue(""+master.get(key));
			table.getCellByPosition(3, n).setStringValue(""+slave.get(key));
			n++;
		}
	}

	public boolean createODS(String prefix, File output) throws Exception {
		if (!dir.exists()) return false;
		File defaultPropertiesFile = new File(dir, prefix+".properties");
		if (!defaultPropertiesFile.exists()) return false;
		
		File langPropertiesFile = new File(dir, prefix+"_"+lang+".properties");
		
		String encoding = "UTF-8";
		if (prefix.startsWith("bundle")) {
			encoding = "iso-8859-1";
		}
		
		System.out.println("Building table...");
		LinkedHashMap<String, String> master = new LinkedHashMap<String, String>();
		master = PluginMessagesManager.loadFromProperties(IOUtils.getReader(defaultPropertiesFile, encoding));
		
		LinkedHashMap<String, String> slave = new LinkedHashMap<String, String>();
		slave = PluginMessagesManager.loadFromProperties(IOUtils.getReader(langPropertiesFile, encoding));
		
		OdfSpreadsheetDocument spreadsheet = OdfSpreadsheetDocument.newSpreadsheetDocument();
		
		TreeSet<Object> keys = new TreeSet<Object>(PluginMessagesManager.comp);
		keys.addAll(master.keySet());
		
		//Cell cell = table.getCellByPosition(0, 0);
//        cell.setStringValue("Hello World!");
		
		OdfTable table = spreadsheet.getTableList().get(0);
		table.appendRows(keys.size()+1);
		table.appendColumns(4);
		editTable(spreadsheet, table, keys, master, slave);

		System.out.println("Saving...");
		spreadsheet.save(output);
		System.out.println("Done.");
		return true;
	}
	
	public static void main(String[] args) throws Exception {
		String prefix = "bundle"; // bundle messages
		File odt = new File(prefix+"_fr.odt");
		new CreateMessagesTable("fr").createODT(prefix, odt);
		System.out.println("ODT="+odt.getAbsolutePath());
	}
}
