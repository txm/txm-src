package org.txm.rcp.translate.devtools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;

import org.txm.rcp.translate.i18n.PluginMessagesManager;
import org.txm.rcp.translate.i18n.WorkspaceMessagesManager;

/**
 * Test properties files :
 * - no duplicated key
 * - no empty message
 * 
 * @author mdecorde
 *
 */
public class FindBugsInPropertiesFiles {
	
	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		WorkspaceMessagesManager wmm = new WorkspaceMessagesManager();
		LinkedHashMap<File, PluginMessagesManager> h = wmm.getPluginMessages();
		
		String langs[] = {"", "_fr", "_ru"};
		
		System.out.println("Fetching messages...");
		for (File project : h.keySet()) {
			
			PluginMessagesManager messages = h.get(project);
			
			for (String lang : langs) {
				File propFile = messages.getFile2lang().getKey("messages"+lang);
				if (propFile == null) continue;
				
			}
		}
	}
}
