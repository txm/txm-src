package org.txm.rcp.translate.devtools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.Properties;

import org.txm.rcp.translate.i18n.PluginMessagesManager;
import org.txm.rcp.translate.i18n.WorkspaceMessagesManager;
import org.txm.utils.io.IOUtils;

/**
 * Test properties files :
 * - no duplicated key
 * - no empty message
 * 
 * @author mdecorde
 *
 */
public class FixEncodingDeclarationInMessagesPropertiesFiles {
	
	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		WorkspaceMessagesManager wmm = new WorkspaceMessagesManager();
		LinkedHashMap<File, PluginMessagesManager> h = wmm.getPluginMessages();
		
		String langs[] = {"", "_fr", "_ru"};
		
		System.out.println("Fetching messages...");
		for (File project : h.keySet()) {
			
			File settingsDir = new File(project, ".settings");
			File ressourcePrefFile = new File(settingsDir, "org.eclipse.core.resources.prefs");
			LinkedHashMap<String, String> content = new LinkedHashMap<String, String>();
			if (ressourcePrefFile.exists()) {
				content = PluginMessagesManager.loadFromProperties(IOUtils.getReader(ressourcePrefFile));
			}
			PluginMessagesManager messages = h.get(project);
			//System.out.println(" "+messages.getMessageFullName()+" -> "+messages.getMessageKeys().size());
			
			//			System.out.println("M"+project+"? "+messages.getMessageFile());
			
			boolean modified = false;
			
			for (String lang : langs) {
				File propFile = messages.getFile2lang().getKey("messages"+lang);
				if (propFile == null) continue;
				//System.out.println("M="+propFile);
				String relPath = propFile.getAbsolutePath().substring(project.getAbsolutePath().length());
				if (content.containsKey("encoding/"+relPath) && content.get("encoding/"+relPath).equals("UTF-8")) {
					System.out.println("OK UTF8: "+propFile);
				} else {
					System.out.println("KO UTF8: "+propFile);
					content.put("encoding/"+relPath, "UTF-8");
					modified = true;
				}
			}
			
			if (modified) {
				System.out.println("Fixing pref file");
				PluginMessagesManager.saveToProperties(content, IOUtils.getWriter(ressourcePrefFile));
			}
		}
	}
}
