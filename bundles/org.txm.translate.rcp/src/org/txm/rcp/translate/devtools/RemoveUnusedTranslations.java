package org.txm.rcp.translate.devtools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.TreeSet;

import org.txm.rcp.translate.i18n.PluginMessagesManager;
import org.txm.rcp.translate.i18n.WorkspaceMessagesManager;

public class RemoveUnusedTranslations {
	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		WorkspaceMessagesManager wmm = new WorkspaceMessagesManager();
		LinkedHashMap<File, PluginMessagesManager> h = wmm.getPluginMessages();

		String langs[] = {"", "_fr", "_ru"};
		int total = 0;
		for (File project : h.keySet()) {

			PluginMessagesManager messages = h.get(project);
			LinkedHashSet<String> keys = messages.getMessageKeys();
			
			for (String lang : langs) {
				
				int n = 0;
				
				HashMap<String, String> lmessages = messages.getMessagesForLang(lang);
				if (lmessages == null) continue;
				
				ArrayList<String> propskeys = new ArrayList<String>(lmessages.keySet());
				ArrayList<String> removed = new ArrayList<String>();
				
				for (String k : propskeys) {
					if (!keys.contains(k)) {
						//System.out.println(" Remove key="+k);
						lmessages.remove(k);
						removed.add(k);
						n++;
					}
				}
				
				if (n > 0) {
					System.out.println(messages.getMessageFullClassName()+" "+n+" "+lang+" keys removed: "+removed);
					//System.out.println("all keys:" + keys);
					messages.saveChanges(true);
					total += n;
				}
			}
		}
		
		if (total > 0) {
			System.out.println("Done: "+total+" keys removed."); 
		} else {
			System.out.println("Done: no change.");
		}
	}
}