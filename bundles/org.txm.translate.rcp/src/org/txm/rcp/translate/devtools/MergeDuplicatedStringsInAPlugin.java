package org.txm.rcp.translate.devtools;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.TreeSet;

import org.txm.rcp.translate.i18n.PluginMessagesManager;
import org.txm.rcp.translate.i18n.WorkspaceMessagesManager;

/**
 * 
 * Finds duplicated strings in a plug-in, merges them in one key and removes duplicated.
 *
 */
public class MergeDuplicatedStringsInAPlugin {
	
	boolean debug = false;
	
	/**
	 * 
	 * @param debug
	 */
	public MergeDuplicatedStringsInAPlugin(boolean debug) {
		this.debug = debug;
	}
	
	public int merge(PluginMessagesManager pm) {
		HashMap<String, String> defaults = pm.getMessagesForDefaultLang();
		TreeMap<String, String> value2Key = new TreeMap<>();
		
		TreeMap<String, TreeSet<String>> merges = new TreeMap<>();
		TreeSet<String> keys = new TreeSet<>(defaults.keySet());
		for (String key : keys) {
			String value = defaults.get(key);
			if (value2Key.containsKey(value)) { // duplicated message
				pm.renameKey(key, value2Key.get(value));
				merges.get(value2Key.get(value)).add(key);
			}
			else {
				value2Key.put(value, key);
				merges.put(key, new TreeSet<String>());
			}
		}
		
		for (String key : keys) {
			if (merges.get(key) != null && merges.get(key).size() == 0) {
				merges.remove(key);
			}
		}
		
		if (merges.size() > 0) {
			System.out.println(" Done: " + pm.getMessageFullName() + " " + merges.size() + " merges: " + merges);
		}
		else {
			// System.out.println(" Done: no merge");
		}
		
		return merges.size();
	}
	
	/**
	 * @param args
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		
		MergeDuplicatedStringsInAPlugin merger = new MergeDuplicatedStringsInAPlugin(false);
		
		WorkspaceMessagesManager wmm = new WorkspaceMessagesManager();
		int total = 0;
		for (PluginMessagesManager pm : wmm.getPluginMessages().values()) {
			total += merger.merge(pm);
			
			pm.saveChanges(true);
		}
		
		wmm.saveKeyModificationsInSources();
		
		if (total > 0) {
			System.out.println("Done: " + total);
		}
		else {
			System.out.println("Done: no merge");
		}
	}
}
