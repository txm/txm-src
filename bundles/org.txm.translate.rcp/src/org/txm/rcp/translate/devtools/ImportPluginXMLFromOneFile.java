package org.txm.rcp.translate.devtools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.Properties;

import org.txm.rcp.translate.i18n.PluginMessagesManager;
import org.txm.rcp.translate.i18n.WorkspaceMessagesManager;
import org.txm.utils.io.IOUtils;

/**
 * 
 * @author mdecorde
 *
 *         Reads one properties file per lang and uses the key to dispatch the
 *         strings in the right files
 *
 */
public class ImportPluginXMLFromOneFile {
	
	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		
		WorkspaceMessagesManager wmm = new WorkspaceMessagesManager();
		LinkedHashMap<File, PluginMessagesManager> h = wmm.getPluginMessages();
		
		String langs[] = {"", "_fr"}; // "",  "_ru" }; //$NON-NLS-1$
		
		System.out.println("Importing bundle messages...");
		int total_update = 0;
		for (String lang : langs) {
			System.out.println(" For lang='" + lang + "'...");
			
			File mergedPropertiesFile = new File(wmm.getWorkspaceLocation(), "bundles/org.txm.translate.rcp/bundle" + lang + ".properties");
			LinkedHashMap<String, String> mergedProperties = new LinkedHashMap<String, String>();
			mergedProperties = PluginMessagesManager.loadFromProperties(IOUtils.getReader(mergedPropertiesFile, "iso-8859-1")); //$NON-NLS-1$
			
			if (mergedProperties.size() == 0) {
				System.out.println(" No messages to update");
				return;
			}
			
			int update = 0;
			
			for (File project : h.keySet()) {
				
				int projectUpdate = 0;
				PluginMessagesManager messages = h.get(project);
				
				// messages.put(mergedLang, key, value);
				for (String key : messages.getOsgiKeys()) {
					
					String merged_key = messages.getMessageFullName() + "." + key; //$NON-NLS-1$
					//System.out.println(merged_key);
					String newMessage = mergedProperties.get(merged_key);
					String oldMessage = messages.getOsgiMessageForLang(lang, key);
					if (newMessage != null 
							&& !newMessage.equals(oldMessage)) {
						messages.putOSGI(lang, key, mergedProperties.get(merged_key).toString());
						System.out.println("UPDATE:" +lang+" "+key+" "+oldMessage+ " -> "+newMessage);
						projectUpdate++;
					}
				}
				
				update += projectUpdate;
				if (projectUpdate > 0) {
					System.out.println("Updates in "+project+": "+projectUpdate);
				}
			}
			
			if (update == 0) {
				System.out.println(" Done, no update done.");
			}
			else {
				System.out.println(" Done, " + update + " update(s) done.");
			}
			total_update += update;
		}
		
		for (PluginMessagesManager messages : h.values()) {
			messages.saveChanges(true, true, false); // only save OSGI messages
		}
		
		if (total_update == 0) {
			System.out.println("Done, no update done.");
		}
		else {
			System.out.println("Done, " + total_update + " update(s) done.");
		}
	}
}
