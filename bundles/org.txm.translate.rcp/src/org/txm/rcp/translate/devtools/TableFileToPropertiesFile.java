package org.txm.rcp.translate.devtools;

import java.io.File;
import java.io.PrintWriter;
import java.util.LinkedHashMap;

import org.txm.rcp.translate.i18n.PluginMessagesManager;
import org.txm.rcp.translate.i18n.WorkspaceMessagesManager;
import org.txm.utils.io.IOUtils;

public class TableFileToPropertiesFile {
	
	File tableFile;
	File propertiesFile;
	String lang;
	
	public TableFileToPropertiesFile(File tableFile, File propertiesFile, String lang) {
		this.tableFile = tableFile;
		this.propertiesFile = propertiesFile;
		this.lang = lang;
	}
	
	public boolean convert() throws Exception {
		
		LinkedHashMap<String, String> slave = new TableToProperties(tableFile, lang).convert();
		
		String encoding = "UTF-8"; //$NON-NLS-1$
		if (tableFile.getName().startsWith("bundle") && !tableFile.getName().contains("_ru")) { //$NON-NLS-1$
			encoding = "iso-8859-1"; //$NON-NLS-1$
		}
		
		PrintWriter writer = IOUtils.getWriter(propertiesFile, encoding);
		PluginMessagesManager.saveToProperties(slave, writer);
		writer.close();
		
		if (propertiesFile.getName().contains("_ru")) { //$NON-NLS-1$
			System.out.println("native2ascii \""+propertiesFile.getAbsolutePath() +"\" \""+propertiesFile.getAbsolutePath()+".tmp\""); //$NON-NLS-1$
			System.out.println("rm \""+propertiesFile.getAbsolutePath()+"\""); //$NON-NLS-1$
			System.out.println("mv \""+propertiesFile.getAbsolutePath() +".tmp\" \""+propertiesFile.getAbsolutePath()+"\""); //$NON-NLS-1$
		}
		
		return true;
	}
	
	public static void main(String args[]) throws Exception {
		
		WorkspaceMessagesManager wmm = new WorkspaceMessagesManager();
		File translatePlugindir = new File(wmm.getWorkspaceLocation(), "bundles/org.txm.translate.rcp"); //$NON-NLS-1$
		
//		boolean b = new TableFileToPropertiesFile(new File(translatePlugindir, "bundles_fr.odt"), new File("orig.properties"), "fr").convert();
//		System.out.println("RESULT: "+b);
//		
//		boolean b2 = new TableFileToPropertiesFile(new File(translatePlugindir, "bundle_fr.odt"), new File("slh.properties"), "fr").convert();
//		System.out.println("RESULT: "+b2);
		
		boolean b3 = new TableFileToPropertiesFile(new File(translatePlugindir, "messages_ru.odt"), new File(translatePlugindir, "messages_ru.properties"), "ru").convert(); //$NON-NLS-1$
		System.out.println("RESULT: "+b3);
	}
}
