package org.txm.rcp.translate.devtools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.txm.rcp.translate.i18n.PluginMessagesManager;
import org.txm.rcp.translate.i18n.WorkspaceMessagesManager;
import org.txm.utils.DeleteDir;
import org.txm.utils.io.IOUtils;

/**
 * 
 * Finds unbounded messages and saves them to a file.
 * E.g. Line of code containing Strings that are concatenated.
 *
 */
public class FindMessagesToBind {
	
	File report;
	
	PrintWriter writer;
	
	boolean searchOnlyExternalized = false;

	private boolean printJavaPath;
	
	public FindMessagesToBind(File report, boolean printJavaPath) {
		this.report = report;
		this.printJavaPath = printJavaPath;
	}
	
	private int processSourceFile(File project, File sourceFile, boolean searchOnlyExternalized) {
		
		// BufferedReader reader = IOUtils.getReader(sourceFile)
		// String line = reader.readLine();
		this.searchOnlyExternalized = searchOnlyExternalized;
		boolean ignore = false;
		ArrayList<String> lines = IOUtils.getLines(sourceFile, "UTF-8"); //$NON-NLS-1$
		ArrayList<String> toBind = new ArrayList<>();
		ArrayList<String> toExternalize = new ArrayList<>();
		
		if (sourceFile.getAbsolutePath().contains("visuAnalec")) { //$NON-NLS-1$
			return 0;
		}
		if (sourceFile.getAbsolutePath().contains("JamaPlus")) { //$NON-NLS-1$
			return 0;
		}
		if (sourceFile.getAbsolutePath().contains("org.txm.portal")) {  //$NON-NLS-1$
			return 0;
		}
		if (sourceFile.getAbsolutePath().contains("org.txm.libs.tidy")) {  //$NON-NLS-1$
			return 0;
		}
		if (sourceFile.getAbsolutePath().contains("org.txm.practically")) {  //$NON-NLS-1$
			return 0;
		}
		if (sourceFile.getAbsolutePath().contains("org.txm.udpipe")) {  //$NON-NLS-1$
			return 0;
		}
		if (sourceFile.getAbsolutePath().contains("org.txm.rcp.translate")) {  //$NON-NLS-1$
			return 0;
		}
		if (sourceFile.getAbsolutePath().contains("org.txm.translate.rcp")) {  //$NON-NLS-1$
			return 0;
		}
		if (sourceFile.getAbsolutePath().contains("org.txm.tmp.rcp")) {  //$NON-NLS-1$
			return 0;
		}
		String basePath = project.getAbsolutePath()+"/src/"; //$NON-NLS-1$
		
		int nLine = 0;
		for (String line : lines) {
			nLine++;
			// println ""+line
			String l = line.trim();
			
			if (ignore) {
				if (l.startsWith("*/") || l.endsWith("*/")) { //$NON-NLS-1$ //$NON-NLS-2$
					ignore = false;
				}
			}
			else {
				if (l.startsWith("//") || l.startsWith("@")) { //$NON-NLS-1$ //$NON-NLS-2$
					// ignore
				}
				else if (l.startsWith("/*")) { //$NON-NLS-1$
					ignore = true;
					if (l.endsWith("*/")) ignore = false; //$NON-NLS-1$
				}
				else {
					
					Matcher m = p1.matcher(l);
					if (m.find()) {
						toBind.add("" + nLine + "\t" + l); //$NON-NLS-1$ //$NON-NLS-2$
						printInConsole(basePath, sourceFile, line, nLine);
					}
					else {
						m = p2.matcher(l);
						if (m.find()) {
							toBind.add("" + nLine + "\t" + l); //$NON-NLS-1$ //$NON-NLS-2$
							printInConsole(basePath, sourceFile, line, nLine);
						}
						else if (!l.contains("$NON-NLS-") && !searchOnlyExternalized) { //$NON-NLS-1$
							m = p3.matcher(l);
							if (m.find()) {
								toExternalize.add("" + nLine + "\t" + l); //$NON-NLS-1$ //$NON-NLS-2$
								printInConsole(basePath, sourceFile, line, nLine);
							}
							else {
								m = p4.matcher(l);
								if (m.find()) {
									toExternalize.add("" + nLine + "\t" + l); //$NON-NLS-1$ //$NON-NLS-2$
									printInConsole(basePath, sourceFile, line, nLine);
								}
								else {
									m = p5.matcher(l);
									if (m.find()) {
										toExternalize.add("" + nLine + "\t" + l); //$NON-NLS-1$ //$NON-NLS-2$
										printInConsole(basePath, sourceFile, line, nLine);
									}
									else {
										
									}
								}
							}
						}
					}
				}
			}
		}
		
		if (toBind.size() > 0 || toExternalize.size() > 0) {
			writer.println(sourceFile.getAbsolutePath());
			
			if (toBind.size() > 0) writer.println("TO BIND"); //$NON-NLS-1$
			for (String l : toBind) {
				if (l.contains("Log.fine")) continue; //$NON-NLS-1$
				writer.println("\t" + l); //$NON-NLS-1$
			}
			if (toExternalize.size() > 0) writer.println("TO EXTERNALIZE"); //$NON-NLS-1$
			for (String l : toExternalize) {
				if (l.contains("Log.fine")) continue; //$NON-NLS-1$
				writer.println("\t" + l); //$NON-NLS-1$
			}
		}
		
		return toBind.size() + toExternalize.size();
	}
	
	public void printInConsole(String basePath, File sourceFile, String line, int iline) {
		
		if (printJavaPath) {
			String path = sourceFile.getAbsolutePath();
			path = path.substring(basePath.length(), path.length()).replace(File.separatorChar, '.'); //$NON-NLS-1$
			System.out.println("("+path+":"+iline+")	"+line.trim());
		}
	}
	
	// a key + 
	Pattern p1 = Pattern.compile("(" + WorkspaceMessagesManager.KEY_REGEX.pattern() + ")( ?\\+)"); //$NON-NLS-1$ //$NON-NLS-2$
	
	// '+' + key + '+'
	Pattern p2 = Pattern.compile("( ?\\+)(" + WorkspaceMessagesManager.KEY_REGEX.pattern() + ")"); //$NON-NLS-1$ //$NON-NLS-2$
	
	// "chars" + '+'
	Pattern p3 = Pattern.compile("(" + WorkspaceMessagesManager.STRING_REGEX.pattern() + " ?\\+)"); //$NON-NLS-1$ //$NON-NLS-2$
	
	// '+' + "chars" 
	Pattern p4 = Pattern.compile("( ?\\\\+)(" + WorkspaceMessagesManager.STRING_REGEX.pattern() + " ?\\+)"); //$NON-NLS-1$ //$NON-NLS-2$
	
	// any chars
	Pattern p5 = Pattern.compile(WorkspaceMessagesManager.STRING_REGEX.pattern());

	private WorkspaceMessagesManager wmm;
	
	private ArrayList<String> getLineStrings(String l) {
		ArrayList<String> strings = new ArrayList<>();
		boolean inS = false;
		int start = 0;
		int comment = 0;
		for (int i = 0; i < l.length(); i++) {
			String c = "" + l.charAt(i);
			if (inS) {
				comment = 0;
				if (c.equals("\\")) { //$NON-NLS-1$
					i++;
				}
				else if (c.equals("\"")) { //$NON-NLS-1$
					inS = false;
					strings.add(l.substring(start, i + 1));
				}
			}
			else {
				if (c.equals("\"")) { //$NON-NLS-1$
					inS = true;
					start = i;
					comment = 0;
				}
				else if (c.equals("/")) { //$NON-NLS-1$
					comment++;
					if (comment == 2) break;
				}
				else {
					comment = 0;
				}
			}
		}
		return strings;
	}
	
	public void run() throws UnsupportedEncodingException, FileNotFoundException, IOException {
		wmm = new WorkspaceMessagesManager();
		LinkedHashMap<File, PluginMessagesManager> h = wmm.getPluginMessages();
		
		writer = IOUtils.getWriter(report);
		//FIXME: SJ: to test to print to Console with link to line in classes and bundle
		// A link can be create in console using this pattern: (java.util.List.java:100)
		////writer = new PrintWriter(System.out);
		
		int total = 0;
		
		TreeSet<File> projects = new TreeSet(h.keySet());
		for (File project : projects) {
			
			// System.out.println("Project: "+project);
			
			File srcDir = new File(project, "src/java"); //$NON-NLS-1$
			if (!srcDir.exists()) srcDir = new File(project, "src"); //$NON-NLS-1$
			if (!srcDir.exists()) continue; // no Java sources
			
			ArrayList<File> files = DeleteDir.scanDirectory(srcDir, true, true);
			Collections.sort(files, new Comparator<File>() {
				
				@Override
				public int compare(File arg0, File arg1) {
					return arg0.getAbsolutePath().compareTo(arg1.getAbsolutePath());
				}
			});
			for (File javaFile : files) {
				
				if (!javaFile.getName().endsWith(".java")) continue; //$NON-NLS-1$
				if (javaFile.getName().endsWith("Messages.java")) continue; //$NON-NLS-1$
				
				// System.out.println(" File: "+javaFile);
				total += processSourceFile(project, javaFile, false);
			}
			
			PluginMessagesManager messages = h.get(project);
			for (File langFile : messages.getLangs().keySet()) {
				String lang = messages.getFile2lang().get(langFile);
				HashMap<String, String> lmessages = messages.getMessagesForLang(langFile);
				for (String key : messages.getMessageKeys()) {
					String message = lmessages.get(key);
					if (message == null) continue;
					if (key.contains("P0")  && !message.contains("{0}")) { //$NON-NLS-1$ //$NON-NLS-2$
						writer.println("\tMissing brakets: lang=" + lang+"->"+key+"="+message); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						total++;
					} else if (key.contains("P1")  && !message.contains("{1}")) { //$NON-NLS-1$ //$NON-NLS-2$
						writer.println("\tMissing brakets: lang=" + lang+"->"+key+"="+message); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						total++;
					} else if (key.contains("P2")  && !message.contains("{2}")) { //$NON-NLS-1$ //$NON-NLS-2$
						writer.println("\tMissing brakets: lang=" + lang+"->"+key+"="+message); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						total++;
					} else if (key.contains("P3")  && !message.contains("{3}")) { //$NON-NLS-1$ //$NON-NLS-2$
						writer.println("\tMissing brakets: lang=" + lang+"->"+key+"="+message); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						total++;
					}
					
					if (!key.contains("P0")  && message.contains("{0}")) { //$NON-NLS-1$ //$NON-NLS-2$
						writer.println("\tMissing PN: lang=" + lang+"->"+key+"="+message); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						total++;
					} else if (!key.contains("P1")  && message.contains("{1}")) { //$NON-NLS-1$ //$NON-NLS-2$
						writer.println("\tMissing PN: lang=" + lang+"->"+key+"="+message); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						total++;
					} else if (!key.contains("P2")  && message.contains("{2}")) { //$NON-NLS-1$ //$NON-NLS-2$
						writer.println("\tMissing PN: lang=" + lang+"->"+key+"="+message); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						total++;
					} else if (!key.contains("P3")  && message.contains("{3}")) { //$NON-NLS-1$ //$NON-NLS-2$
						writer.println("\tMissing PN: lang=" + lang+"->"+key+"="+message); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						total++;
					}

				}
			}
		}
		
		writer.close();
		
		if (total > 0) {
			System.out.println("" + total + " potential bindinds or externalizes."); //$NON-NLS-1$ //$NON-NLS-2$
		}
		else {
			System.out.println("You are the very best of the very best: nothing to do!"); //$NON-NLS-1$
		}
	}
	
	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		File report = new File("report.txt"); //$NON-NLS-1$
		new FindMessagesToBind(report, true).run();
		System.out.println("Report: " + report.getAbsolutePath()); //$NON-NLS-1$
	}
}
