package org.txm.rcp.translate.devtools;

import java.io.File;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Properties;
import java.util.TreeSet;

import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.odftoolkit.odfdom.doc.table.OdfTable;
import org.odftoolkit.odfdom.doc.table.OdfTableCell;
import org.odftoolkit.odfdom.doc.table.OdfTableRow;
import org.txm.rcp.translate.i18n.PluginMessagesManager;
import org.txm.rcp.translate.i18n.WorkspaceMessagesManager;
import org.txm.utils.io.IOUtils;

public class ImportMessagesTable {
	
	File dir;
	
	String lang;
	
	public ImportMessagesTable(String lang) {
		this(new File(System.getProperty("user.dir")), lang);
	}
	
	public ImportMessagesTable(File dir, String lang) {
		this.dir = dir;
		this.lang = lang;
	}
	
	public boolean run(String prefix, File tableFile) throws Exception {
		if (!dir.exists()) {
			System.out.println("dir not found: " + dir);
			return false;
		}
		
		if (!tableFile.exists()) {
			System.out.println("table file not found: " + tableFile);
			return false;
		}
		
		File langPropertiesFile = new File(dir, prefix + "_" + lang + ".properties");
		LinkedHashMap<String, String> slave = new LinkedHashMap<String, String>();
		File defaultPropertiesFile = new File(dir, prefix + ".properties");
		LinkedHashMap<String, String> master = new LinkedHashMap<String, String>();
		
		String encoding = "UTF-8";
		if (prefix.startsWith("bundle")) {
			encoding = "iso-8859-1";
		}
		
		slave = PluginMessagesManager.loadFromProperties(IOUtils.getReader(langPropertiesFile, encoding));
		
		OdfTextDocument doc = OdfTextDocument.loadDocument(tableFile);
		OdfTable table = doc.getTableList(false).get(0);
		
		OdfTableRow headerRow = table.getRowList().get(0);
		int ncol = headerRow.getCellCount();
		int keyIndex = -1;
		int langIndex = -1;
		int defaultIndex = -1;
		for (int c = 0; c < ncol; c++) {
			OdfTableCell cell = headerRow.getCellByIndex(c);
			if ("KEY".equals(cell.getDisplayText())) {
				keyIndex = c;
			}
			else if (lang.equals(cell.getDisplayText().toLowerCase())) {
				langIndex = c;
			}
			else if ("DEFAULT".equals(cell.getDisplayText())) {
				defaultIndex = c;
			}
		}
		
		if (keyIndex == -1) {
			System.out.println("KEY columns not found in " + headerRow);
			return false;
		}
		if (langIndex == -1) {
			System.out.println(lang + " columns not found in " + headerRow);
			return false;
		}
		if (defaultIndex == -1) {
			System.out.println("DEFAULT columns not found in " + headerRow);
			return false;
		}
		
		for (OdfTableRow h : table.getRowList()) {
			
			String key = h.getCellByIndex(keyIndex).getDisplayText();
			if (key != null) {
				String value = h.getCellByIndex(langIndex).getDisplayText();
				
				if (value != null) {
					String value2 = h.getCellByIndex(defaultIndex).getDisplayText();
					
					// fix value
					if (value.contains("{0}")) {
						value = value.replaceAll("([^'])'([^'])", "$1''$2");
						value = value.replaceAll("’", "'");
						value = value.replace("''''", "''");
						value = value.replace("'''", "''");
						value2 = value2.replaceAll("([^'])'([^'])", "$1''$2");
						value2 = value2.replaceAll("’", "'");
						value2 = value2.replace("''''", "''");
						value2 = value2.replace("'''", "''");
					} else {
						value = value.replace("'''", "'");
						value = value.replace("''", "'");
						value2 = value2.replace("'''", "'");
						value2 = value2.replace("''", "'");
					}
					
					slave.put(key, value);
					master.put(key, value2);
				}
			}
		}
		
		PluginMessagesManager.saveToProperties(slave, IOUtils.getWriter(langPropertiesFile, encoding));
		PluginMessagesManager.saveToProperties(master, IOUtils.getWriter(defaultPropertiesFile, encoding));
		
		System.out.println("Import saved in master=" + defaultPropertiesFile);
		System.out.println("Import saved in slave=" + langPropertiesFile);
		
		return true;
	}
	
	public static void main(String[] args) throws Exception {
		
		WorkspaceMessagesManager wmm = new WorkspaceMessagesManager();
		
//		String prefix = "messages"; // messages, bundle
//		System.out.println(new ImportMessagesTable(new File(wmm.getWorkspaceLocation(), "bundles/org.txm.translate.rcp"), "ru").run(prefix, new File(prefix + "_ru.odt")));
		
		String prefix = "bundle"; // messages, bundle
		System.out.println(new ImportMessagesTable(new File(wmm.getWorkspaceLocation(), "bundles/org.txm.translate.rcp"), "fr").run(prefix, new File(prefix + "_fr-slh.odt")));
	}
}
