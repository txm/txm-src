package org.txm.rcp.translate.devtools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Properties;
import java.util.TreeSet;

import org.txm.rcp.translate.i18n.PluginMessagesManager;
import org.txm.rcp.translate.i18n.WorkspaceMessagesManager;
import org.txm.utils.io.IOUtils;

/**
 * Creates one properties files containing all TXM externalized and translated Strings
 * 
 * The keys are prefixed with the Messages.java class full path
 * 
 * @author mdecorde
 *
 */
public class ExportOSGiXMLToOneFile {

	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		WorkspaceMessagesManager wmm = new WorkspaceMessagesManager();
		LinkedHashMap<File, PluginMessagesManager> h = wmm.getPluginMessages();

		HashMap<String, LinkedHashMap<String, String>> mergedProps = new HashMap<String, LinkedHashMap<String, String>>();
		String langs[] = {"", "_fr", "_ru"};

		for (String lang : langs) {
			mergedProps.put(lang, new LinkedHashMap<String, String>());
		}
		
		System.out.println("Fetching messages...");
		for (File project : h.keySet()) {
			
			PluginMessagesManager messages = h.get(project);
			//System.out.println(" "+messages.getMessageFullName()+" -> "+messages.getMessageKeys().size());
			
			File osgiInf = new File(messages.getProjectDirectory(), "OSGI-INF/l10n");
			for (String lang : langs) {
				File propFile = new File(osgiInf, "bundle"+lang+".properties");
				
				if (propFile != null) {
					HashMap<String, String> lmessages = messages.getOsgiLangs().get(propFile);
					
					if (lmessages == null) continue; // no OSGI for this lang
					
					for (String key : messages.getOsgiKeys()) {
						
						if (lmessages.get(key) == null) continue;
						
						String merged_key = messages.getMessageFullName()+"."+key;
						if (mergedProps.get(lang).containsKey(merged_key)) {
							System.out.println(" WARNING: duplicated key ? "+merged_key);
						} else {
							mergedProps.get(lang).put(merged_key, lmessages.get(key));
						}
					}
				}
			}
		}
		
		System.out.println("Exporting messages to "+wmm.getWorkspaceLocation()+ "bundles/org.txm.translate.rcp");
		for (String lang : langs) {
			System.out.println(lang+" "+mergedProps.get(lang).size());
			File mergedPropFile = new File(wmm.getWorkspaceLocation(), "bundles/org.txm.translate.rcp/bundle"+lang+".properties");
			try {
				PluginMessagesManager.saveToProperties(mergedProps.get(lang), IOUtils.getWriter(mergedPropFile, "iso-8859-1"));
				System.out.println("See: "+mergedPropFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
