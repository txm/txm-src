package org.txm.rcp.translate.devtools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import org.txm.rcp.translate.i18n.PluginMessagesManager;
import org.txm.rcp.translate.i18n.WorkspaceMessagesManager;

/**
 * FixDoubleQuotes
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class FixDoubleQuotes {

	/**
	 * Debug state.
	 */
	protected static boolean debug = false;

	/**
	 * 
	 * @param debug show lot of debug messages
	 */
	public FixDoubleQuotes(boolean debug) {
		FixDoubleQuotes.debug = debug;
	}

	/**
	 * fix double quotes in messages
	 * 
	 * @param pmManager the PluginMessages to update
	 * @return the number of modifications done
	 */
	public static int fix(PluginMessagesManager pmManager)	{

		if(debug)	{
			//System.out.println("FixDoubleQuotes.fix(): "+pmManager.getProjectDirectory().getName());
		}
		int update = 0;
		String[] langs = {"", "_fr", "_ru"};
		for (String lang : langs) {
			HashMap<String, String> messages = pmManager.getMessagesForLang(lang);
			if (messages != null)
			for (String key : messages.keySet()) {
				String v = messages.get(key);
				v = v.replaceAll("’", "'");
				if (v.contains("{") && v.contains("}")) {
					System.out.println("v="+v);
					v = v.replaceAll("([^'])'([^'])", "$1'$2");
				} else {
					v = v.replace("''", "'");
				}

				if (!messages.get(key).equals(v)) {
					if (debug) {
						System.out.println(key+"= "+messages.get(key)+" -> "+v);
					}
						
					messages.put(key, v);
					update++;
				}

			}
		}
		
		return update;
	}

	/**
	 * 
	 * @param args
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {

		WorkspaceMessagesManager wmm = new WorkspaceMessagesManager(false);
		FixDoubleQuotes fixer = new FixDoubleQuotes(true);
		for (PluginMessagesManager pmManager : wmm.getPluginMessages().values()) {
			File projectFile = pmManager.getProjectDirectory();

			if (fixer.fix(pmManager) > 0) {
				pmManager.saveChanges(true); // mode homme
			}
		}
		
	}
}
