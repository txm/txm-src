package org.txm.rcp.translate.devtools;

import java.io.BufferedReader;
import java.io.File;
import java.io.PrintWriter;
import java.util.LinkedHashMap;

import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.odftoolkit.odfdom.doc.table.OdfTable;
import org.odftoolkit.odfdom.doc.table.OdfTableCell;
import org.odftoolkit.odfdom.doc.table.OdfTableRow;
import org.txm.rcp.translate.i18n.PluginMessagesManager;
import org.txm.utils.io.IOUtils;

public class TableToProperties {
	
	File tableFile;
	String lang;
	
	public TableToProperties(File tableFile, String lang) {
		
		this.tableFile = tableFile;
		this.lang = lang;
	}
	
	public LinkedHashMap<String, String> convert() throws Exception {
		
		LinkedHashMap<String, String> slave = new LinkedHashMap<String, String>();
		
		OdfTextDocument doc = OdfTextDocument.loadDocument(tableFile);
		OdfTable table = doc.getTableList(false).get(0);
		
		OdfTableRow headerRow = table.getRowList().get(0);
		int ncol = headerRow.getCellCount();
		int keyIndex = -1;
		int langIndex = -1;
		int defaultIndex = -1;
		String str = "";
		for (int c = 0; c < ncol; c++) {
			OdfTableCell cell = headerRow.getCellByIndex(c);
			String s = cell.getDisplayText();
			if ("KEY".equals(s)) {
				keyIndex = c;
			}
			if (lang.equals(s.toLowerCase())) {
				langIndex = c;
			}
			if ("DEFAULT".equals(s)) {
				defaultIndex = c;
			}
			str += "\t"+cell.getDisplayText();
		}
		
		if (keyIndex == -1) {
			System.out.println("KEY columns not found in " + str);
			return null;
		}
		if (langIndex == -1) {
			System.out.println(lang + " columns not found in " + str);
			return null;
		}
		if (defaultIndex == -1) {
			System.out.println("DEFAULT columns not found in " + str);
			return null;
		}
		
		for (OdfTableRow h : table.getRowList()) {
			
			String key = h.getCellByIndex(keyIndex).getDisplayText();
			if (key != null) {
				String value = h.getCellByIndex(langIndex).getDisplayText();
				if (value != null) {
					slave.put(key, value);
				}
			}
		}
		
		return slave;
	}
	
	public static void main(String args[]) throws Exception {
		
//		Properties b = new TableToProperties(new File("/home/mdecorde/workspace047/org.txm.translate.rcp/bundles_fr.odt"), "default").convert();
//		
//		System.out.println("RESULT: "+b);
//		
//		Properties b2 = new TableToProperties(new File("/home/mdecorde/workspace047/org.txm.translate.rcp/bundle_fr.odt"), "default").convert();
//		
//		System.out.println("RESULT: "+b2);
//		
//		Properties newProperties = new LinkedHashMap<String, String>();
//		
//		for (Object k : b.keySet()) {
//			if (!(b.get(k).equals(b2.get(k)))) {
//				newProperties.put(k.toString(), b2.get(k).toString());
//			}
//		}
//		
//		System.out.println("NEWPROPERTIES: "+newProperties);
//		
//		PrintWriter writer = IOUtils.getWriter(new File("/home/mdecorde/TEMP/newvalues_default.properties"), "UTF-8");
//		newPropertiesPluginMessagesManager.saveToProperties(writer, "");
//		writer.close();
		
		
		LinkedHashMap<String, String> previousOKProperties = new TableToProperties(new File("/home/mdecorde/TEMP/messagespaskc.odt"), "default").convert();
		
		File newValuesPropertiesFile = new File("/home/mdecorde/TEMP/newvalues_default.properties");
		BufferedReader reader = IOUtils.getReader(newValuesPropertiesFile, "UTF-8");
		LinkedHashMap<String, String> newProperties2 = new LinkedHashMap<String, String>();
		newProperties2 = PluginMessagesManager.loadFromProperties(reader);
		reader.close();
		
		for (Object k : newProperties2.keySet()) {
			previousOKProperties.put(k.toString(), newProperties2.get(k.toString()));
		}
		
		PrintWriter writer = IOUtils.getWriter(new File("/home/mdecorde/TEMP/bundle.properties"), "iso-8859-1");
		PluginMessagesManager.saveToProperties(previousOKProperties, writer);
		writer.close();
	}
}
