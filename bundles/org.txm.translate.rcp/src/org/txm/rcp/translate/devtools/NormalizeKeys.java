package org.txm.rcp.translate.devtools;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

import org.txm.rcp.translate.i18n.PluginMessagesManager;
import org.txm.rcp.translate.i18n.WorkspaceMessagesManager;

/**
 * Normalizes the keys using the words of the message sentence.
 * eg. : "Error while computing {0}." => errorWhileComputing0
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class NormalizeKeys {

	/**
	 * Debug state.
	 */
	protected static boolean debug = false;

	/**
	 * Keys matching this REGEX will be preserved.
	 */
	public static String preserve = "^(common_|error_|info_|export_|charts_|RESULT_TYPE).*$"; //$NON-NLS-1$

	/**
	 * Java keywords that can not be used as variable name.
	 */
	public static String javaKeyWords = "`abstract|assert|boolean|break|byte|case|catch|char|class|const|continue|default|do|double|else|enum|extends|false|final|finally|float|for|goto|if|implements|import|instanceof|int|interface|long|native|new|null|package|private|protected|public|return|short|static|strictfp|super|switch|synchronized|this|throw|throws|transient|true|try|void|volatile|while`i"; //$NON-NLS-1$

	/**
	 * 
	 * @param debug show lot of debug messages
	 */
	public NormalizeKeys(boolean debug) {
		NormalizeKeys.debug = debug;
	}

	/**
	 * Normalize the messages keys in Messages.java and messages.properties files
	 * 
	 * Warning: the sources files are not updated use WorkspaceMessagesManager.saveKeyModificationsInSources() when all work is done
	 * 
	 * @param pmManager the PluginMessages to update
	 * @return the number of modifications done
	 */
	public int normalize(PluginMessagesManager pmManager)	{

		if(debug)	{
			System.out.println("NormalizeKeys.normalize(): ****************************************************************************************"); //$NON-NLS-1$
			System.out.println("NormalizeKeys.normalize(): normalizating messages keys of project " + pmManager.getProjectDirectory() + "..."); //$NON-NLS-1$
		}

		HashMap<String, String> messages = pmManager.getMessagesForDefaultLang();

		ArrayList<String> keys = new ArrayList<String>(pmManager.getMessageKeys());
		ArrayList<String> normalized = new ArrayList<String>();

		for (String key : keys) {
			String value = messages.get(key);
			if (value == null) {
				System.out.println("** NormalizeKeys.normalize(): warning: missing message for key="+key);
				continue;
			}
			// already formatted message
			if(key.matches(preserve))	{
				if (debug) {
					System.out.println("** NormalizeKeys.normalize(): warning: skipped: " + key + "=" + value);
				}
				continue;
			}
			// empty message
			else if(value.length() == 0)	{
				if (debug) {
					System.out.println("** NormalizeKeys.normalize(): warning: empty string for key: " + key); 
				}
				continue;
			}

			if (debug) System.out.print("K="+key+" "); //$NON-NLS-1$
			if (key.equals("ComplexSort_4")) { //$NON-NLS-1$
				System.out.println();
			}
			String newKeyOrig = normalize(messages.get(key));
			if (newKeyOrig.length() == 0) {
				//newKeyOrig = "EMPTY";
				continue;
			}

			if (key.equals(newKeyOrig)) { // nothing to do
				continue;
			}

			//			if (value.equals(messages.get(newKeyOrig))) { // the newKey Value == the value
			//				//A=hello world
			//				//B=hello world
			//				// rename A -> helloWorld
			//				//B -> helloWorld SAUF QUE helloWorld existe ET meme valeur que valeur(B) valeur(helloWorld)
			//
			//				pmManager.removeKey(key); // remove the old key
			//			} else {
			// A=hello world
			// B=hello world!
			// A et B n'ont pas la meme clé

			String newKey = ""+newKeyOrig; //$NON-NLS-1$
			int c = 2;
			while (pmManager.getMessageKeys().contains(newKey)) {
				newKey = newKeyOrig+"_"+(c++); //$NON-NLS-1$
			}

			if (key.equals(newKeyOrig)) { // finally nothing to do
				continue;
			}
			
			pmManager.renameKey(key, newKey);
			normalized.add(key + " => " + newKey); //$NON-NLS-1$
			//			}
		}

		if(debug)	{
			System.out.println("NormalizeKeys.normalize(): normalization done for project " + pmManager.getProjectDirectory() + "."); //$NON-NLS-1$
			System.out.println("NormalizeKeys.normalize(): number of keys: " + pmManager.getMessageKeys().size() + "."); //$NON-NLS-1$
			System.out.println("NormalizeKeys.normalize(): number of replacements to do: " + pmManager.getKeyModifications().size() + "."); //$NON-NLS-1$
		}

		// Log
		if (normalized.size() > 0) {
			System.out.println(" Done: "+pmManager.getMessageName() + " " + normalized.size() + " changes: ");
			for (int i = 0; i < normalized.size(); i++) {
				System.out.println("   " + normalized.get(i)); //$NON-NLS-1$
			}
		}
		return normalized.size();
	}

	/**
	 * Normalizes the specified string.
	 * @param str
	 */
	public static String normalize(String str)	{
		
		// log
		if(debug)	{
			System.out.println("NormalizeKeys.normalize(): normalizing: " + str); //$NON-NLS-1$
		}

		str = str.trim();
		
		if (str.length() == 0) return "EMPTY"; //$NON-NLS-1$

		// replace the parameters binding
		str = str.replaceAll("(\\{[0-9]})", "P$1"); //$NON-NLS-1$

		// special replacements
		str = str.replace("%", "Percent"); //$NON-NLS-1$
		str = str.replace("~", "Tild"); //$NON-NLS-1$
		str = str.replace("=", "Equals"); //$NON-NLS-1$
		str = str.replace("@", "At"); //$NON-NLS-1$
		str = str.replace("&", "Amp"); //$NON-NLS-1$
		str = str.replace("<", "Inf"); //$NON-NLS-1$
		str = str.replace(">", "Sup"); //$NON-NLS-1$
		str = str.replace("+", "Plus"); //$NON-NLS-1$
		str = str.replace("|", "Pipe"); //$NON-NLS-1$
		str = str.replace(":", "Colon"); //$NON-NLS-1$
		str = str.replace("...", "3Dot"); //$NON-NLS-1$

		// remove all special chars
		str = str.replaceAll("[^a-zA-Z0-9 ]", "").trim(); //$NON-NLS-1$

		// the message contains only special chars
		if(str.length() == 0)	{
			//System.err.println("NormalizeKeys.normalize(): warning: empty string.");
			return ""; //$NON-NLS-1$
		}
		else	{

			String[] words = str.split(" "); //$NON-NLS-1$
			str = ""; //$NON-NLS-1$
			for (int i = 0; i < words.length; i++) {
				if(words[i].length() == 0)	{
					continue;
				}
				try {
					str += words[i].substring(0, 1).toUpperCase();
				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					str += words[i].substring(1);
				}
				catch (Exception e) {
					// nothing to do
				}
			}

			// uncapitalize first letter
			String tmpString = str.substring(0, 1).toLowerCase();
			try {
				tmpString += str.substring(1);
			}
			catch (Exception e) {
				// nothing to do
			}
			str = tmpString;
		}

		// java variable can not start by a number or be a java keyword 
		
		if(str.matches(javaKeyWords) 
				|| str.matches("^[0-9].*$"))	{ //$NON-NLS-1$
			str = "_" + str; //$NON-NLS-1$
		}

		// log
		if(debug)	{
			System.out.println("   => " + str); //$NON-NLS-1$
		}

		return str;
	}

	/**
	 * 
	 * @param args
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {

		//		try {
		//			System.out.println("NormalizeKeys.main(): starting process...");
		//			
		//			File projectFile = new File(new File(System.getProperty("user.dir")).getParentFile().getAbsolutePath() + "/org.txm.core");
		//			File messageFile = new File(projectFile, "src/java/org/txm/core/messages/TXMCoreMessages.java");
		//			
		//			PluginMessages pmManager = new PluginMessages(projectFile, messageFile, false);
		//			NormalizeKeys keysNormalizer = new NormalizeKeys(true);
		//			
		//			// tests
		//			keysNormalizer.normalize(pmManager);
		//
		//			//pmManager.dump(pmManager.getMessagesForMainLang());
		//			pmManager.dumpKeysReplacements();
		//			
		//			System.out.println("NormalizeKeys.main(): number of keys: " + pmManager.getMessageKeys().size() + ".");
		//			System.out.println("NormalizeKeys.main(): number of replacements to do: " + pmManager.getKeyModifications().size() + ".");
		//
		//			System.out.println("NormalizeKeys.main(): terminated.");
		//			
		//		}
		//		catch (UnsupportedEncodingException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		//		catch (FileNotFoundException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		//		catch (IOException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}

		NormalizeKeys keysNormalizer = new NormalizeKeys(true);

		WorkspaceMessagesManager wmm = new WorkspaceMessagesManager(false);
		int total = 0;
		for (PluginMessagesManager pm : wmm.getPluginMessages().values()) {

			//if (!pm.getProjectDirectory().getName().equals("org.txm.ca.rcp")) continue;

			total += keysNormalizer.normalize(pm);

			pm.saveChanges(true); // mode homme
			//pm.saveChanges(false);

			if (pm.getProjectDirectory().getName().equals("org.txm.ca.rcp")) { //$NON-NLS-1$
				System.out.println("KEYS2="+pm.getMessageKeys()); //$NON-NLS-1$
			}
		}

		if (total > 0) {
			wmm.saveKeyModificationsInSources();
			System.out.println("Done: "+total+" changes.");
		} else {
			System.out.println("Done: no change.");
		}		

		//		WorkspaceMessagesManager wmm = new WorkspaceMessagesManager(false);
		//		
		//		for (PluginMessagesManager pmManager : wmm.getPluginMessages().values()) {
		//			File projectFile = pmManager.getProjectDirectory();
		//			if (!projectFile.getName().equals("org.txm.ahc.core")) continue;
		//			
		//			keysNormalizer.normalize(pmManager);
		//				
		//			pmManager.saveChanges(true); // mode homme
		//			//pmManager.saveChanges(false);
		//		}
		//		
		//		wmm.saveKeyModificationsInSources();


		//		WorkspaceMessagesManager wmm = new WorkspaceMessagesManager(false);
		//		
		//		for (PluginMessagesManager pmManager : wmm.getPluginMessages().values()) {
		//			//System.out.println(pmManager.getProjectDirectory().getName());
		//			if (pmManager.getProjectDirectory().getName().equals("org.txm.chartsengine.jfreechart.core")) {
		//				System.out.println("INDEX jfc core="+pmManager.getUsedKeysFilesIndex());
		//			}
		//			if (!pmManager.getProjectDirectory().getName().equals("org.txm.chartsengine.core")) continue;
		//
		//
		////			System.out.println("KEYS="+pmManager.getMessageKeys());
		//			keysNormalizer.normalize(pmManager);
		////			System.out.println("KEYS2="+pmManager.getMessageKeys());
		////			System.out.println("MODIFS="+pmManager.getKeyModifications());
		//			//pmManager.saveChanges(true); // mode homme
		//
		//			//pmManager.saveChanges(false);
		//			
		//			System.out.println("INDEX="+pmManager.getUsedKeysFilesIndex());
		//		}
		//
		//		//System.out.println("FULLINDEX="+wmm.getUsedKeysFilesIndex().);
		//		//wmm.saveKeyModificationsInSources();
	}
}
