package org.txm.rcp.translate.preferences;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;

import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.io.IOUtils;

/**
 * Manages the messages of the TXM projects of an RCP workspace
 * 
 * @author mdecorde
 *
 */
public class WorkspacePreferencesManager {

	/**
	 * Debug state.
	 */
	protected boolean debug = false;

	/**
	 * the plugins discovered and their messages
	 */
	LinkedHashMap<File, PluginPreferencesManager> pluginsPreferencesPerProject = new LinkedHashMap<File, PluginPreferencesManager>();

	/**
	 * the Eclipse RCP workspace location containing the projects
	 */
	File workspaceLocation;

	/**
	 * Index of the keys used in all plugins sources files.
	 */
	protected TreeMap<String, TreeSet<File>> usedKeysFilesIndex;

	/**
	 * Generic Regex to find keys in source files 
	 */
	public static final Pattern KEY_REGEX = Pattern.compile("(?:[A-Z][a-zA-Z0-9]+)?Preferences\\.[a-zA-Z0-9_]+");

	/**
	 * Generic Regex to find Strings in source files. Warning the "\"" strings are not found. 
	 */
	public static final Pattern STRING_REGEX = Pattern.compile("\"[^\"]+\"");

	/**
	 * 
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public WorkspacePreferencesManager() throws UnsupportedEncodingException, FileNotFoundException, IOException {
		this(false);
	}

	/**
	 * Creates the manager using the "user.dir" parent directory -> works when run from Eclipse
	 * 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public WorkspacePreferencesManager(boolean debug) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		this(new File(System.getProperty("user.dir")).getParentFile(), debug);
	}

	/**
	 * Build a WorkspacePreferencesManager for a certain workspace directory
	 * 
	 * @param workspaceLocation
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public WorkspacePreferencesManager(File workspaceLocation, boolean debug) throws UnsupportedEncodingException, FileNotFoundException, IOException {

		if (!new File(workspaceLocation, ".metadata").exists()) {
			throw new IllegalArgumentException("Workspace directory is not a RCP workspace: "+workspaceLocation);
		}

		this.workspaceLocation = workspaceLocation;
		this.debug = debug;

		if (debug) {
			System.out.println("WorkspacePreferencesManager.WorkspacePreferencesManager(): workspace location = " + this.workspaceLocation);
		}

		if (!workspaceLocation.exists()) return;
		if (!workspaceLocation.isDirectory()) {
			System.out.println("Error: workspace not found in: "+workspaceLocation);
			return;
		}

		System.out.println("Initializing project preferences...");
		ArrayList<File> files = new ArrayList<File>();
		for (File project : workspaceLocation.listFiles(IOUtils.HIDDENFILE_FILTER)) {
			if (!project.isDirectory()) continue;
			if (project.isHidden()) continue;
			if (!project.getName().startsWith("org.txm")) continue;
			if (project.getName().endsWith(".feature")) continue;
			files.add(project);
		}
		ConsoleProgressBar cpb = new ConsoleProgressBar(files.size());
		for (File project : files) {
			cpb.tick();

			//			File messagesPackageDir = new File(project, "src/"+project.getName().replaceAll("\\.", "/")+"/messages");
			//			if (!messagesPackageDir.exists()) {
			//				messagesPackageDir = new File(project, "src/java/"+project.getName().replaceAll("\\.", "/")+"/messages");
			//			}
			//			//System.out.println(messagesPackageDir.getAbsolutePath());
			//
			//			File messagesJavaFile = PluginPreferences.findPreferenceFile(project);

			//			if (messagesJavaFile != null && messagesJavaFile.exists()) {

			PluginPreferencesManager preferences = new PluginPreferencesManager(project, debug);

			pluginsPreferencesPerProject.put(project, preferences);

			if (debug) {
				System.out.println(project + "=" + preferences.getPreferenceKeys().size() + " preferences.");
			}
			//			}
		}
		cpb.done();

		this.createUsedKeysFilesIndex();

		// Log summary
		if (debug) {

			this.dumpUsedKeysFilesIndex();

			System.out.println("WorkspacePreferencesManager.WorkspacePreferencesManager(): ------------------ process summary --------------------------------------------------------------");
			System.out.println("WorkspacePreferencesManager.WorkspacePreferencesManager(): numbers of projects: " + this.pluginsPreferencesPerProject.size() + ".");
			System.out.println("WorkspacePreferencesManager.WorkspacePreferencesManager(): numbers of used keys: " + this.usedKeysFilesIndex.size() + ".");
			System.out.println("WorkspacePreferencesManager.WorkspacePreferencesManager(): done.");
		}
	}

	public TreeMap<String, TreeSet<File>> getUsedKeysFilesIndex() {
		return usedKeysFilesIndex;
	}

	/**
	 *  Dumps the index of the keys used and their associated files.
	 */
	protected void dumpUsedKeysFilesIndex() {
		System.out.println("WorkspacePreferencesManager.dumpUsedKeysIndex(): dumping used keys files index..."); //$NON-NLS-1$

		for (Map.Entry<String, TreeSet<File>> entry : this.usedKeysFilesIndex.entrySet()) {
			System.out.println(entry.getKey());
			if(entry.getValue().isEmpty())	{
				System.out.println("\t-not used in any file of the workspace-"); //$NON-NLS-1$
			}
			else	{
				for(File file : entry.getValue())	{
					System.out.println("\t" + file); //$NON-NLS-1$
				}
			}
		}

		System.out.println("WorkspacePreferencesManager.dumpUsedKeysFilesIndex(): number of used keys: " + this.usedKeysFilesIndex.size()); //$NON-NLS-1$
	}

	/**
	 * Save modifications of keys of all known PluginPreferences in all known source files
	 * @throws IOException 
	 */
	public void saveKeyModificationsInSources() throws IOException {
//		HashMap<String, String> modifications = new HashMap<String, String>();
//
//		// stores all modifications
//		ArrayList<PluginPreferencesManager> plugins = new ArrayList<PluginPreferencesManager>(pluginsPreferencesPerProject.values());
//
//		// sort plugins by name length. Needed to not erase a replacement: for messages BPreferences.xx and ABPreferences.xx, ABPreferences.xx should be replaced first
//		Collections.sort(plugins, new Comparator<PluginPreferencesManager>() {
//			@Override
//			public int compare(PluginPreferencesManager arg0, PluginPreferencesManager arg1) {
//				return arg1.getPreferenceName().length() - arg0.getPreferenceName().length();
//			}
//
//		});
//
//		// build the global keys used in sources
//		for (PluginPreferencesManager pm : plugins) {
//			HashMap<String, String> modifs = pm.getKeyModifications();
//			String name = pm.getPreferenceClassName();
//
//			for (String old : modifs.keySet()) {
//				modifications.put(pm.getKeyFullName(old), pm.getKeyFullName(modifs.get(old))); // prefix the key with class name: Key -> XXXPreferences.Key used in sources
//			}
//		}
//
//		// update source files
//
//		// sort keys by name length. Needed to not erase a replacement: for messages BPreferences.xx and ABPreferences.xx, ABPreferences.xx should be replaced first
//		ArrayList<String> keys = new ArrayList<String>(modifications.keySet());
//		Collections.sort(keys, new Comparator<String>() {
//			@Override
//			public int compare(String arg0, String arg1) {
//				return arg1.length() - arg0.length();
//			}
//		});
//
//		System.out.println("Keys to update: "+keys);
//		ArrayList<String> updated = new ArrayList<String>();
//		//		for (PluginPreferencesManager pm : pluginsPreferencesPerProject.values()) {
//		TreeMap<String, TreeSet<File>> index = getUsedKeysFilesIndex();
//
//		for (String oldKey : keys) {
//			if (index.containsKey(oldKey)) {
//				TreeSet<File> files = index.get(oldKey);
//				if (files.size() > 0) {
//					Pattern p = Pattern.compile(oldKey+"([^_0-9a-zA-Z$])");
//					System.out.println("Replace KEY="+p+" BY KEY="+modifications.get(oldKey));
//					System.out.println("Files: "+files);
//					for (File srcFile : files) {
//						//FIXME AAAAA remplace "AAAAA" et "AAAAA1"
//						IOUtils.replaceAll(srcFile, p, modifications.get(oldKey)+"$1");
//					}
//					updated.add(oldKey);
//				}
//			}
//		}
		//		}
	}

	/**
	 * @return the Project directory associated with their PluginPreferences
	 */
	public LinkedHashMap<File, PluginPreferencesManager> getPluginPreferences() {
		return pluginsPreferencesPerProject;
	}

	public File getWorkspaceLocation() {
		return workspaceLocation;
	}

	/**
	 *  dedicated to get a manager that use the specified key in the project source files for example to move an unused message to another plugin (move dedicated string from core/rcp to dedicated plugin)
	 * @param key local key (without the 'XXXPreferences.' part)
	 * @return the PluginPreferences that uses the key in source files
	 */
	public ArrayList<PluginPreferencesManager> getPluginPreferencesManagersUsingKey(String key)	{
		ArrayList<PluginPreferencesManager> pms = new ArrayList<>();

		for (PluginPreferencesManager pm : this.getPluginPreferences().values()) {
			if (pm.getUsedKeysFilesIndex().containsKey(key)) {
				pms.add(pm);
			}
		}

		return pms;
	}


	/**
	 * Creates the index of the keys used and their associated files.
	 */
	public void createUsedKeysFilesIndex()	{
		this.usedKeysFilesIndex = new TreeMap<String, TreeSet<File>>();

		for (File p : this.pluginsPreferencesPerProject.keySet()) {
			PluginPreferencesManager pmManager = this.pluginsPreferencesPerProject.get(p);

			TreeMap<String, TreeSet<File>> index = pmManager.getUsedKeysFilesIndex();

			if(index == null) {
				System.err.println("WorkspacePreferencesManager.createUsedKeysFilesIndex(): project " + pmManager.getProjectDirectory() + " have no used keys.");
				continue;
			}

			for (Map.Entry<String, TreeSet<File>> entry :index.entrySet()) {

				if (debug) {
					System.out.println("WorkspacePreferencesManager.createUsedKeysFilesIndex(): key added: " + entry.getKey() + ".");
				}

				if(this.usedKeysFilesIndex.containsKey(entry.getKey()))	{
					this.usedKeysFilesIndex.get(entry.getKey()).addAll(entry.getValue());
				}
				else	{
					this.usedKeysFilesIndex.put(entry.getKey(), entry.getValue());
				}
			}
		}
	}


	/**
	 * 
	 * @param args
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		WorkspacePreferencesManager wmm = new WorkspacePreferencesManager();
		wmm.summary();
		//wmm.saveKeyModificationsInSources();
	}

	private void summary() {
		System.out.println("CLASS\tVARIABLE\tIDENTIFIER\tDEFAULT VALUE");
		ArrayList<File> files = new ArrayList<File>(this.getPluginPreferences().keySet());
		Collections.sort(files, new Comparator<File>() {

			@Override
			public int compare(File o1, File o2) {
				// TODO Auto-generated method stub
				return o1.getName().compareTo(o2.getName());
			}
		});
		for (File p : files) {
			PluginPreferencesManager preferences = this.getPluginPreferences().get(p);
			preferences.summary();
		}
	}
}
