package org.txm.rcp.translate.i18n;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.LinkedHashSet;
import java.util.regex.Pattern;

import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.io.IOUtils;

/**
 * Manages the messages of the TXM projects of an RCP workspace
 * 
 * @author mdecorde
 *
 */
public class WorkspaceMessagesManager {

	/**
	 * Debug state.
	 */
	protected boolean debug = false;

	/**
	 * the plugins discovered and their messages
	 */
	LinkedHashMap<File, PluginMessagesManager> pluginsMessagesPerProject = new LinkedHashMap<File, PluginMessagesManager>();

	/**
	 * the Eclipse RCP workspace location containing the projects
	 */
	File workspaceLocation;

	/**
	 * Index of the keys used in all plugins sources files.
	 */
	protected TreeMap<String, LinkedHashSet<File>> usedKeysFilesIndex;

	/**
	 * Generic Regex to find keys in source files 
	 */
	public static final Pattern KEY_REGEX = Pattern.compile("(?:[A-Z][a-zA-Z0-9]+)?Messages\\.[a-zA-Z0-9_]+");

	/**
	 * Generic Regex to find Strings in source files. Warning the "\"" strings are not found. 
	 */
	public static final Pattern STRING_REGEX = Pattern.compile("\"[^\"]+\"");

	/**
	 * 
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public WorkspaceMessagesManager() throws UnsupportedEncodingException, FileNotFoundException, IOException {
		this(false);
	}

	/**
	 * Creates the manager using the "user.dir" parent directory -> works when run from Eclipse
	 * 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public WorkspaceMessagesManager(boolean debug) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		this(new File(System.getProperty("user.dir")).getParentFile().getParentFile(), debug);
	}

	/**
	 * Build a WorkspaceMessagesManager for a certain workspace directory
	 * 
	 * @param workspaceLocation
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public WorkspaceMessagesManager(File workspaceLocation, boolean debug) throws UnsupportedEncodingException, FileNotFoundException, IOException {

		if (!new File(workspaceLocation, "bundles").exists()) {
			throw new IllegalArgumentException("Workspace directory is not a RCP workspace: "+workspaceLocation);
		}

		this.workspaceLocation = workspaceLocation;
		this.debug = debug;

		if (debug) {
			System.out.println("WorkspaceMessagesManager.WorkspaceMessagesManager(): workspace location = " + this.workspaceLocation);
		}

		if (!workspaceLocation.exists()) return;
		if (!workspaceLocation.isDirectory()) {
			System.out.println("Error: workspace not found in: "+workspaceLocation);
			return;
		}

		System.out.println("Initializing project messages...");
		ArrayList<File> files = new ArrayList<File>();
		for (File project : new File(workspaceLocation, "bundles").listFiles(IOUtils.HIDDENFILE_FILTER)) {
			if (!project.isDirectory()) continue;
			if (project.isHidden()) continue;
			if (!project.getName().startsWith("org.txm")) continue;
			if (project.getName().endsWith(".feature")) continue;
			files.add(project);
		}
		ConsoleProgressBar cpb = new ConsoleProgressBar(files.size());
		for (File project : files) {
			cpb.tick();

			//			File messagesPackageDir = new File(project, "src/"+project.getName().replaceAll("\\.", "/")+"/messages");
			//			if (!messagesPackageDir.exists()) {
			//				messagesPackageDir = new File(project, "src/java/"+project.getName().replaceAll("\\.", "/")+"/messages");
			//			}
			//			//System.out.println(messagesPackageDir.getAbsolutePath());
			//
			//			File messagesJavaFile = PluginMessages.findMessageFile(project);

			//			if (messagesJavaFile != null && messagesJavaFile.exists()) {

			PluginMessagesManager messages = new PluginMessagesManager(project, debug);

			pluginsMessagesPerProject.put(project, messages);

			if (debug) {
				System.out.println(project + "=" + messages.getLangs().size() + " langs.");
			}
			//			}
		}
		cpb.done();

		this.createUsedKeysFilesIndex();

		// Log summary
		if (debug) {

			this.dumpUsedKeysFilesIndex();

			System.out.println("WorkspaceMessagesManager.WorkspaceMessagesManager(): ------------------ process summary --------------------------------------------------------------");
			System.out.println("WorkspaceMessagesManager.WorkspaceMessagesManager(): numbers of projects: " + this.pluginsMessagesPerProject.size() + ".");
			System.out.println("WorkspaceMessagesManager.WorkspaceMessagesManager(): numbers of used keys: " + this.usedKeysFilesIndex.size() + ".");
			System.out.println("WorkspaceMessagesManager.WorkspaceMessagesManager(): numbers of unused keys: " + PluginMessagesManager.getUnusedKeys(this.usedKeysFilesIndex, false).size() + ".");
			System.out.println("WorkspaceMessagesManager.WorkspaceMessagesManager(): done.");
		}
	}

	public TreeMap<String, LinkedHashSet<File>> getUsedKeysFilesIndex() {
		return usedKeysFilesIndex;
	}

	/**
	 *  Dumps the index of the keys used and their associated files.
	 */
	protected void dumpUsedKeysFilesIndex() {
		System.out.println("WorkspaceMessagesManager.dumpUsedKeysIndex(): dumping used keys files index...");

		for (Map.Entry<String, LinkedHashSet<File>> entry : this.usedKeysFilesIndex.entrySet()) {
			System.out.println(entry.getKey());
			if(entry.getValue().isEmpty())	{
				System.out.println("\t-not used in any file of the workspace-");
			}
			else	{
				for(File file : entry.getValue())	{
					System.out.println("\t" + file);
				}
			}
		}

		System.out.println("WorkspaceMessagesManager.dumpUsedKeysFilesIndex(): number of used keys: " + this.usedKeysFilesIndex.size());
	}

	/**
	 * Save modifications of keys of all known PluginMessages in all known source files
	 * @throws IOException 
	 */
	public void saveKeyModificationsInSources() throws IOException {
		HashMap<String, String> modifications = new HashMap<String, String>();

		// stores all modifications
		ArrayList<PluginMessagesManager> plugins = new ArrayList<PluginMessagesManager>(pluginsMessagesPerProject.values());

		// sort plugins by name length. Needed to not erase a replacement: for messages BMessages.xx and ABMessages.xx, ABMessages.xx should be replaced first
		Collections.sort(plugins, new Comparator<PluginMessagesManager>() {
			@Override
			public int compare(PluginMessagesManager arg0, PluginMessagesManager arg1) {
				return arg1.getMessageName().length() - arg0.getMessageName().length();
			}

		});

		// build the global keys used in sources
		for (PluginMessagesManager pm : plugins) {
			HashMap<String, String> modifs = pm.getKeyModifications();
			String name = pm.getMessageClassName();

			for (String old : modifs.keySet()) {
				modifications.put(pm.getKeyFullName(old), pm.getKeyFullName(modifs.get(old))); // prefix the key with class name: Key -> XXXMessages.Key used in sources
			}
		}

		// update source files

		// sort keys by name length. Needed to not erase a replacement: for messages BMessages.xx and ABMessages.xx, ABMessages.xx should be replaced first
		ArrayList<String> keys = new ArrayList<String>(modifications.keySet());
//		Collections.sort(keys, new Comparator<String>() {
//			@Override
//			public int compare(String arg0, String arg1) {
//				return arg1.length() - arg0.length();
//			}
//		});

		System.out.println("Keys to update: "+keys);
		ArrayList<String> updated = new ArrayList<String>();
		//		for (PluginMessagesManager pm : pluginsMessagesPerProject.values()) {
		TreeMap<String, LinkedHashSet<File>> index = getUsedKeysFilesIndex();

		for (String oldKey : keys) {
			if (index.containsKey(oldKey)) {
				LinkedHashSet<File> files = index.get(oldKey);
				if (files.size() > 0) {
					Pattern p = Pattern.compile(oldKey+"([^_0-9a-zA-Z$])");
					System.out.println("Replace KEY="+p+" BY KEY="+modifications.get(oldKey));
					System.out.println("Files: "+files);
					for (File srcFile : files) {
						//FIXME AAAAA remplace "AAAAA" et "AAAAA1"
						IOUtils.replaceAll(srcFile, p, modifications.get(oldKey)+"$1");
					}
					updated.add(oldKey);
				}
			}
		}
		//		}
	}

	/**
	 * @return the Project directory associated with their PluginMessages
	 */
	public LinkedHashMap<File, PluginMessagesManager> getPluginMessages() {
		return pluginsMessagesPerProject;
	}

	public File getWorkspaceLocation() {
		return workspaceLocation;
	}

	/**
	 *  dedicated to get a manager that use the specified key in the project source files for example to move an unused message to another plugin (move dedicated string from core/rcp to dedicated plugin)
	 * @param key local key (without the 'XXXMessages.' part)
	 * @return the PluginMessages that uses the key in source files
	 */
	public ArrayList<PluginMessagesManager> getPluginMessagesManagersUsingKey(String key)	{
		ArrayList<PluginMessagesManager> pms = new ArrayList<>();

		for (PluginMessagesManager pm : this.getPluginMessages().values()) {
			if (pm.getUsedKeysFilesIndex().containsKey(key)) {
				pms.add(pm);
			}
		}

		return pms;
	}


	/**
	 * Creates the index of the keys used and their associated files.
	 */
	public void createUsedKeysFilesIndex()	{
		this.usedKeysFilesIndex = new TreeMap<String, LinkedHashSet<File>>();

		for (File p : this.pluginsMessagesPerProject.keySet()) {
			PluginMessagesManager pmManager = this.pluginsMessagesPerProject.get(p);

			TreeMap<String, LinkedHashSet<File>> index = pmManager.getUsedKeysFilesIndex();

			if(index == null) {
				System.err.println("WorkspaceMessagesManager.createUsedKeysFilesIndex(): project " + pmManager.getProjectDirectory() + " have no used keys.");
				continue;
			}

			for (Map.Entry<String, LinkedHashSet<File>> entry :index.entrySet()) {

				//String fullKeyName = pmManager.getKeyFullName(entry.getKey());

				if (debug) {
					System.out.println("WorkspaceMessagesManager.createUsedKeysFilesIndex(): key added: " + entry.getKey() + ".");
				}

				if(this.usedKeysFilesIndex.containsKey(entry.getKey()))	{
					this.usedKeysFilesIndex.get(entry.getKey()).addAll(entry.getValue());
				}
				else	{
					this.usedKeysFilesIndex.put(entry.getKey(), entry.getValue());
				}
			}
		}
	}


	/**
	 * 
	 * @param args
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		
		WorkspaceMessagesManager wmm = new WorkspaceMessagesManager();
		LinkedHashMap<File, PluginMessagesManager> pms = wmm.getPluginMessages();
		ArrayList<File> keys = new ArrayList<File>(pms.keySet());
		Collections.sort(keys);
		System.out.println("pms keys size:"+keys.size());
		for (File pmk : keys) {
			PluginMessagesManager pm = pms.get(pmk);
			System.out.println("PMK="+pmk+" messages="+pm.getMessageKeys().size()+" osgi="+pm.getOsgiKeys().size());
			System.out.println(pm.getMessageKeys());
			System.out.println(pm.getOsgiKeys());
		}
		
		//wmm.dumpUsedKeysFilesIndex();
		//wmm.saveKeyModificationsInSources();
	}
}
