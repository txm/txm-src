package org.txm.rcp.translate.i18n;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class FindUntranslatedMessages {
	public static void main(String[] args) throws IOException {
		String userdir = System.getProperty("user.home");
		File workspace = new File(userdir, "workspace047");
		List<String> langs = Arrays.asList("fr", "ru");

		ExternalizationFilesUpdater scanner = new ExternalizationFilesUpdater();
		scanner.scanDirectory(new File(workspace, "org.txm.rcp"));
		for (String lang : langs) {
			scanner.createMissingFiles(lang);
		}
		scanner.processFoundDirectories();
		scanner.updateFiles();
	}
}
