// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2013-07-25 16:24:13 +0200 (jeu. 25 juil. 2013) $
// $LastChangedRevision: 2490 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.rcp.translate.i18n;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.txm.utils.io.IOUtils;

/**
 * Tool to update the key of externalization files xxxx_fr.properties
 * adds _NA to missing keys
 * 
 * @author mdecorde
 * 
 */
public class ExternalizationFilesUpdater {

	/** The dirfiles. */
	LinkedList<File> dirfiles = new LinkedList<File>();
	
	/** The propertyfiles. */
	HashMap<File, List<File>> propertyfiles = new HashMap<File, List<File>>();
	
	/** The fileentries. */
	HashMap<File, List<String>> fileentries = new HashMap<File, List<String>>();
	
	/** The fileentriesvalues. */
	HashMap<File, HashMap<String, String>> fileentriesvalues = new HashMap<File, HashMap<String, String>>();

	/**
	 * Gets the entries.
	 *
	 * @param file the file
	 * @return the entries
	 * @throws IOException 
	 */
	public List<String> getEntries(File file) throws IOException
	{
		List<String> entries = new ArrayList<>();
		
		String encoding = "UTF-8"; //$NON-NLS-1$
		if (file.getName().startsWith("bundle")) { //$NON-NLS-1$
			encoding = "iso-8859-1"; //$NON-NLS-1$
		}
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file) , encoding));
		String line = reader.readLine();
		while (line != null) {
			String[] split = line.split("=", 2); //$NON-NLS-1$
			if (split.length > 0) {
				String key = split[0];
				entries.add(key);
			}
			line = reader.readLine();
		}
		reader.close();
		return entries;
	}

	/**
	 * Gets the values.
	 *
	 * @param file the file
	 * @return the values
	 * @throws IOException 
	 */
	public HashMap<String, String> getValues(File file) throws IOException {
		HashMap<String, String> values = new HashMap<String, String>();
		
		String encoding = "UTF-8"; //$NON-NLS-1$
		if (file.getName().startsWith("bundle")) { //$NON-NLS-1$
			encoding = "iso-8859-1"; //$NON-NLS-1$
		}
		
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));
		String line = reader.readLine();
		while (line != null) {
			
			String[] split = line.split("="); //$NON-NLS-1$
			if (split.length > 0) {
				String key = split[0];
				String concat = ""; //$NON-NLS-1$
				for (int i = 1; i < split.length; i++) {
					concat += split[i];
				}
				values.put(key, concat);
			}
			line = reader.readLine();
		}
		return values;
	}

	/**
	 * Process found directories.
	 * @throws IOException 
	 */
	public void processFoundDirectories() throws IOException
	{
		for (File dirfile : dirfiles) {
			
			System.out.println("DIRECTORY : "+dirfile); //$NON-NLS-1$
			List<File> files = propertyfiles.get(dirfile);
			for (File f : files) {
				this.fileentries.put(f, getEntries(f));
				this.fileentriesvalues.put(f, getValues(f));
			}
			File reference = null;
			if (files.get(0).getName().startsWith("m")) { //$NON-NLS-1$
				reference = new File(dirfile,"messages.properties"); //$NON-NLS-1$
			}
			else if (files.get(0).getName().startsWith("b"))
				reference = new File(dirfile,"bundle.properties"); //$NON-NLS-1$
			
			if (reference != null && reference.exists()) {
				for (File f : files) {
					if (f != reference) {
						List<String> refentries = new ArrayList<>(fileentries.get(reference));
						List<String> tmp1 = new ArrayList<>(this.fileentries.get(f));
						tmp1.removeAll(refentries);
						
						refentries = new ArrayList<>(this.fileentries.get(reference));
						List<String> tmp = this.fileentries.get(f);
						refentries.removeAll(tmp);
						for (String missing : refentries) {
							this.fileentriesvalues.get(f).put(missing, "N/A_"+this.fileentriesvalues.get(reference).get(missing)); // put entry's value $NON-NLS-1$
						}
						
						this.fileentries.put(f, this.fileentries.get(reference)); // update file entries
						
						if (tmp1.size() > 0 || refentries.size() > 0) {
							System.out.println(" "+f.getName()); //$NON-NLS-1$
						}
						if (tmp1.size() > 0) {
							System.out.println("  Removed keys : "+tmp1); //$NON-NLS-1$
						}
						if (refentries.size() > 0) {
							System.out.println("  Added keys : "+refentries); //$NON-NLS-1$
						}
					}
				}
			}
		}
	}

	/**
	 * Update files.
	 * @throws IOException 
	 */
	public void updateFiles() throws IOException {
		
		for (File dirfile : dirfiles) {
			
			List<File> files = propertyfiles.get(dirfile);
			for (File f : files) {
				
				List<String> entries =  this.fileentries.get(f);
				HashMap<String, String> values = this.fileentriesvalues.get(f);
				
				String encoding = "UTF-8"; //$NON-NLS-1$
				if (f.getName().startsWith("bundle")) { //$NON-NLS-1$
					encoding = "iso-8859-1"; //$NON-NLS-1$
				}
				
				Writer writer = new OutputStreamWriter(new FileOutputStream(f) , encoding);
				for (String entry : entries) {
					writer.write(entry+"="+values.get(entry)+"\n"); //$NON-NLS-1$
				}
				writer.close();
			}
		}
	}

	/**
	 * Creates the missing files.
	 *
	 * @param suffix the suffix
	 * @throws IOException 
	 */
	public void createMissingFiles(String suffix) throws IOException {
		
		System.out.println("Looking for missing messages files "+ suffix);
		for (File dirfile : dirfiles) {
			//println "DIRECTORY : "+dirfile
			File reference = null;
			String lookingname = ""; //$NON-NLS-1$
			List<File> files = propertyfiles.get(dirfile);
			if (files.get(0).getName().startsWith("m")) { //$NON-NLS-1$
				
				reference = new File(dirfile, "messages.properties"); //$NON-NLS-1$
				lookingname = "messages_"+suffix+".properties"; //$NON-NLS-1$
			}
			else if (files.get(0).getName().startsWith("b")) { //$NON-NLS-1$
				
				reference = new File(dirfile, "bundle.properties"); //$NON-NLS-1$
				lookingname = "bundle_"+suffix+".properties"; //$NON-NLS-1$
			}
			
			boolean create = true;
			if (reference != null && reference.exists()) {
				for (File f : files) {
					if(f.getName() == lookingname)
						create = false;
				}
			}
			
			if (create) {
				new File(dirfile,lookingname).createNewFile();
				System.out.println("Create file " + new File(dirfile,lookingname)); //$NON-NLS-1$
			}
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		
		String rez = ""; //$NON-NLS-1$
		for (File dirfile : dirfiles) {
			rez += dirfile+"\n"; //$NON-NLS-1$
			for (File f : propertyfiles.get(dirfile)) {
				rez += "  "+f.getName()+"\n"; //$NON-NLS-1$
			}
		}
		return rez;
	}

	/**
	 * Scan directory.
	 *
	 * @param directory the directory
	 */
	public void scanDirectory(File directory) {
		
		if (!directory.exists()) {
			System.out.println("directory '$directory' does not exists"); //$NON-NLS-1$
			return;
		}
		
		System.out.println("Scan directory : "+directory.getAbsolutePath()); //$NON-NLS-1$
		LinkedList<File> files = new LinkedList<File>();
		files.add(directory);
		
		while (!files.isEmpty()) {
			
			File current = files.removeFirst();
			if (current.isDirectory()) {
				List<File> currentpfiles = new ArrayList<>();
				for (File sfile : current.listFiles(IOUtils.HIDDENFILE_FILTER)) {
					
					if (sfile.isDirectory()) {
						files.add(sfile);
					} else if(sfile.getName().endsWith(".properties") && (sfile.getName().startsWith("messages") || sfile.getName().startsWith("bundle")) ) { //$NON-NLS-1$
						currentpfiles.add(sfile);
					}
				}
				if (currentpfiles.size() > 0) {
					dirfiles.add(current);
					propertyfiles.put(current, currentpfiles);
				}
			}
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		String userdir = System.getProperty("user.home"); //$NON-NLS-1$

		System.out.println("\nRCP\n"); //$NON-NLS-1$
		
		ExternalizationFilesUpdater scanner = new ExternalizationFilesUpdater();
//		scanner.scanDirectory(new File(userdir, "workspace37/org.txm.rcp")); // find directories with a messages.properties file
//		scanner.createMissingFiles("fr"); // create messages_fr.properties files when a messages.properties is found
//		scanner.processFoundDirectories(); // find missing and obsolets keys
//		scanner.updateFiles(); // update messages files content
		
		System.out.println("\nTOOLBOX\n"); //$NON-NLS-1$
		
		scanner = new ExternalizationFilesUpdater();
		scanner.scanDirectory(new File(userdir, "workspace047/org.txm.rcp/src/main/java")); //$NON-NLS-1$
		scanner.createMissingFiles("fr"); //$NON-NLS-1$
		scanner.createMissingFiles("ru"); //$NON-NLS-1$
		scanner.processFoundDirectories();
		scanner.updateFiles();
	}
}
