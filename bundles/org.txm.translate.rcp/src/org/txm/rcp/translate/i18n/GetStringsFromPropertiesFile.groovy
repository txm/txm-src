package org.txm.rcp.translate.i18n;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;

import org.txm.utils.io.IOUtils;

File newProjectMessagesPackage= new File(System.getProperty("user.home"), "workspace079/org.txm.concordance.core/src/org/txm/concordances/messages");
File oldProjectMessagesPackage= new File(System.getProperty("user.home"), "workspace079/org.txm.core/src/java/org/txm/core/messages")
File newNLSClassFile = new File(newProjectMessagesPackage, "ConcordanceMessages.java");
File oldNLSClassFile = new File(oldProjectMessagesPackage, "TBXMessages.java");
def langs = ["_fr", "", "_ru"]
for (def lang : langs) {
	
	println "LANG=$lang"
	Properties oldProperties = new Properties();
	File oldMessageFile = new File(oldProjectMessagesPackage, "messages${lang}.properties")
	oldProperties.load(IOUtils.getReader(oldMessageFile, "UTF-8"));
	Properties newProperties = new Properties();
	File newMessageFile = new File(newProjectMessagesPackage, "messages${lang}.properties")
	if (newMessageFile.exists()) {
		newProperties.load(IOUtils.getReader(newMessageFile, "UTF-8"));
	}
	
	newNLSClassFile.getText("UTF-8").findAll("public static String [^;]+;") { key ->
		key = key.substring(21, key.length()-1)
		
		if (oldProperties.get(key) == null) {
			println "Key $key is missing from old messages file: "+oldMessageFile.getName()
			newProperties.put(key, "MISSING")
		} else {
			if (newProperties.get(key) != null) {
				println "update $key = "+oldProperties.get(key)
			} else {
				println "add $key = "+oldProperties.get(key)
			}
			newProperties.put(key, oldProperties.get(key))
		}
	}
	newProperties.store(IOUtils.getWriter(newMessageFile, "UTF-8"), "created/updated with GetStringsFromPropertiesFile.groovy")
	println "Done"		
}