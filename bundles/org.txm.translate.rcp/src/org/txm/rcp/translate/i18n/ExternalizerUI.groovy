package org.txm.rcp.translate.i18n;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*
import java.awt.GridLayout
import java.awt.event.*

import org.txm.utils.LS;
/**
 * This replace the eclipse externalisation interface
 * Scan the java files for not translated Strings and update the messages.properties file
 * 
 * uses ExternalizeStringFile
 * @author mdecorde
 *
 */
class ExternalizeUI extends JPanel {
	ExternalizeStringFile externalizer;

	ExternalizeUI() {
		super(new FlowLayout());
		File userDir = new File(System.getProperty("user.home"))
		File srcDir = new File(userDir, "workspace047/org.txm.rcp/src/main/java")
		File propFile = new File(srcDir, "org/txm/rcp/messages/messages.properties")
		File messageFile = new File(srcDir, "org/txm/rcp/messages/TXMUIMessages.java")

		println userDir
		println srcDir
		println propFile
		println messageFile

		String[] columnNames = ["Files in "+srcDir.getPath(), "Nb of changes"];


		def files = LS.list(srcDir, true, false);
		files = files.findAll { file -> file.getName().endsWith(".java") }
		files = files.sort()
		println files

		externalizer = new ExternalizeStringFile(propFile, messageFile);
		println "externalizer ready"
		def data = []
		def selectedFiles = []
		for (File file : files) {
			int count = externalizer.getNbOfChanges(file)
			if (count != 0)	{
				selectedFiles << file
				data << [file.getPath().substring(srcDir.getPath().length()), count]
			}
		}
		Object[][] arrayData = data;
		JTable table = new JTable(arrayData, columnNames);
		table.getColumnModel().getColumn(0).setPreferredWidth(600);
		table.getColumnModel().getColumn(1).setPreferredWidth(100);

		table.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 1) {
							int selectedRowIndex = table.getSelectedRow();
							int c = externalizer.process(selectedFiles[selectedRowIndex])
							println ""+c
							int after = table.getValueAt(table.getSelectedRow(), 1);
							table.setValueAt(after - c, table.getSelectedRow(), 1);
							//show your dialog with the selected row's contents
						}
					}
				});

		JScrollPane scrollPane = new JScrollPane(table);
		table.setFillsViewportHeight(true);

		add(scrollPane)

		JButton cancelButton = new JButton("CANCEL");
		JButton okButton = new JButton("OK + SAVE");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				ExternalizeUI.this.doSave();
				System.exit(0);
			}
		});
		
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				println "Changes not saved: "
				System.exit(0);
			}
		});
		cancelButton.setSize(100, 20);
		okButton.setSize(100, 20);
		add(cancelButton)
		add(okButton)

		println "end of constructor"
	}

	public void doSave() {
		if (externalizer != null && externalizer.saveChanges()) {
			JOptionPane.showMessageDialog(this, "Changes saved");
		}
	}

	/**
	 * Create the GUI and show it.  For thread safety,
	 * this method should be invoked from the
	 * event-dispatching thread.
	 */
	private static void createAndShowGUI() {
		//Create and set up the window.
		JFrame frame = new JFrame("ExternalizeUI");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Create and set up the content pane.
		ExternalizeUI newContentPane = new ExternalizeUI();
		newContentPane.setOpaque(true); //content panes must be opaque

		//		frame.addWindowListener(new java.awt.event.WindowAdapter() {
		//			public void windowClosing(WindowEvent winEvt) {
		//				newContentPane.doSave();
		//				System.exit(0);
		//			}
		//		});
		frame.setContentPane(newContentPane);

		println "display"
		//Display the window.
		frame.pack();
		frame.setVisible(true);
	}


	public static void main(String[] args) {
		//Schedule a job for the event-dispatching thread:
		//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						createAndShowGUI();
					}
				});
	}
}