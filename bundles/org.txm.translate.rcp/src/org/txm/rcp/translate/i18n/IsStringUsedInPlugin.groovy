package org.txm.rcp.translate.i18n

/**
 * Display the String and key not used in the plugin messages
 * @author mdecorde
 *
 */
class IsStringUsedInPlugin {
	Properties props;

	public boolean scan(File propertyFile, File pluginFile) {
		if(!(propertyFile.exists() && propertyFile.canRead() && propertyFile.canWrite() && propertyFile.isFile()))
		{
			println "error file : "+propertyFile
			return false;
		}

		props = new Properties();
		
		String encoding = "UTF-8";
		if (propertyFile.getName().startsWith("bundle")) {
			encoding = "iso-8859-1";
		}
		
		propertyFile.withReader(encoding) { input->
			props.load(input);
			input.close()
		}

		String content = pluginFile.getText();
		def toRemove = [];
		for(def key : props.keySet()) {
			if (!content.contains("\"%"+key+"\"")) {
				println "not used $key "+props.get(key);
				toRemove << key
			}
		}

		for(String key : toRemove) {
			props.remove(key);
		}


		if (toRemove == 0) {
			println "nothing to do"
			return false;
		}
		return true
	}

	public boolean writeFixes(File propertyFile) {
		if (props == null) return false;
		
		String encoding = "UTF-8";
		if (propertyFile.getName().startsWith("bundle")) {
			encoding = "iso-8859-1";
		}
		
		propertyFile.withWriter(encoding) { output ->
			props.store(output, "")
		}
		return true;
	}

	public static void main(String[] args) {
		String userhome = System.getProperty("user.home")
		File propertyFile = new File(userhome, "workspace047/org.txm.rcp/OSGI-INF/l10n/bundle.properties");
		File pluginFile = new File(userhome, "workspace047/org.txm.rcp/plugin.xml");
		IsStringUsedInPlugin fixer = new IsStringUsedInPlugin();
		//uncomment to remove unused Strings
		if (fixer.scan(propertyFile, pluginFile)) {
			fixer.writeFixes(propertyFile)
		}
	}
}
