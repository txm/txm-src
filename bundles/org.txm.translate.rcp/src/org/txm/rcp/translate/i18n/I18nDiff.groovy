package org.txm.rcp.translate.i18n

import java.io.File;
import java.util.HashMap;
/**
 * Display a diff of 2 message files
 * 
 * @author mdecorde
 *
 */
class I18nDiff {
	Properties values1, values2;
	File file1;
	File file2;
	
	def notInValues1, notInValues2
	I18nDiff(File file1, File file2) {
		this.file1 = file1
		this.file2 = file2
		values1 = getValues(file1)
		values2 = getValues(file2)
		
		def inter = values1.keySet().intersect(values2.keySet());
		notInValues1 = values2.keySet().minus(values1.keySet());
		notInValues2 = values1.keySet().minus(values2.keySet());
		
		println "not in $file1"
		for(String key : notInValues1)
			if (key.trim().length() > 0)
				println key+" = "+values2.get(key)
		
		println "not in $file2"
		for(String key : notInValues2)
			if (key.trim().length() > 0)
				println key+" = "+values1.get(key)
		
		//		println "values diff"
		//
		//		for(String key : inter ) {
		//			if(values1.get(key) != values2.get(key))
		//				println key+" = "+values1.get(key)+" != "+values2.get(key)
		//		}
	}
	
	public void synchronizeFromF1toF2() {
		
		for (String key : notInValues2) {
			if (key.trim().length() > 0)
				values2.setProperty(key, "NA_"+values1.get(key));
		}
		
		for (String key : notInValues1) {
			if (key.trim().length() > 0)
				values2.remove(key)
		}
		
		String encoding = "UTF-8";
		if (file2.getName().startsWith("bundle")) {
			encoding = "iso-8859-1";
		}
		
		file2.withWriter(encoding) { writer ->
			values2.store(writer, "")
		}
	}
	
	/**
	 * Gets the values.
	 *
	 * @param file the file
	 * @return the values
	 */
	public Properties getValues(File file) {
		
		Properties props = new Properties();
		
		String encoding = "UTF-8";
		if (file.getName().startsWith("bundle")) {
			encoding = "iso-8859-1";
		}
		
		file.withReader(encoding) { input->
			props.load(input);
			input.close()
		}
		return props;
	}
	
	public static void main(String[] args) {
		
		String userhome = System.getProperty("user.home");
		File f1 = new File(userhome, "workspace047/org.txm.rcp/OSGI-INF/l10n/bundle.properties")
		File f2 = new File(userhome, "workspace047/org.txm.rcp/OSGI-INF/l10n/bundle_fr.properties")
		
		I18nDiff diff = new I18nDiff(f1, f2);
		diff.synchronizeFromF1toF2()
	}
}
