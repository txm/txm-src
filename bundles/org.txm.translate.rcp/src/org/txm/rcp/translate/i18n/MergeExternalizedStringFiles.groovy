package org.txm.rcp.translate.i18n

import java.io.File;
import org.eclipse.osgi.util.NLS;
import org.txm.utils.DeleteDir;
import org.txm.utils.io.IOUtils

/**
 * Scan for *.properties files and merge them into one
 * @author mdecorde
 *
 */
class MergeExternalizedStringFiles {
	
	ArrayList<File> alldirs;
	LinkedHashMap<File, ArrayList> data = [:];
	
	public MergeExternalizedStringFiles(File projectDir)
	{
		alldirs = scanDirectory(projectDir);
		alldirs.sort{it.getAbsolutePath();}
		//System.out.println("All dirs size: "+alldirs.size);
		findMessageClasses();
		mergeAll()
	}
	
	public findMessageClasses()
	{
		File messageClass;
		ArrayList<File> externalizeMessages;
		for(int i = 0 ; i < alldirs.size() ; i++) {
			File dir = alldirs.get(i)
			if (!dir.isDirectory())
				continue;
			//System.out.println("Dir: "+dir);
			externalizeMessages = new ArrayList<File>();
			for (File f : dir.listFiles(IOUtils.HIDDENFILE_FILTER)) {
				if (f.getName() == "Messages.java")
					messageClass = f
				if (f.getName() =~ "messages(_..)?\\.properties")
					externalizeMessages << f
			}
			
			if (messageClass != null) {
				String bundle = findBundle(messageClass);
				data.put(dir, [messageClass, bundle, externalizeMessages])
			} else {
				alldirs.remove(i);
				i--;
			}
		}
		println "all dirs size: "+alldirs.size()
		//		for(def f : alldirs)
		//			println f
	}
	
	public String findBundle(File message)
	{
		String ret;
		
		String encoding = "UTF-8";
		if (message.getName().startsWith("bundle")) {
			encoding = "iso-8859-1";
		}
		
		message.eachLine(encoding) {
			if (it.contains("private static final String BUNDLE_NAME = ")) {
				int start = it.indexOf("\"")
				int end = it.lastIndexOf("\"")
				ret = it.substring(start+1, end)
			}
		}
		return ret;
	}
	
	/**
	 * Scan directory.
	 *
	 * @param directory the directory
	 */
	public ArrayList<File> scanDirectory(File directory)
	{
		ArrayList<File> dirfiles = [];
		
		if (!directory.exists()) {
			println "directory '$directory' does not exists"
			return;
		}
		
		println "scan directory : "+directory.getAbsolutePath();
		LinkedList<File> files = new LinkedList<File>();
		files.add(directory);
		
		while (!files.isEmpty()) {
			File current = files.removeFirst();
			if (current.isDirectory() && !current.getName().startsWith(".")) {
				//println "add dir: "+current
				List<String> currentpfiles = [];
				for (File sfile : current.listFiles(IOUtils.HIDDENFILE_FILTER)) {
					if (sfile.isDirectory())
						files.add(sfile);
				}
				
				dirfiles.add(current);
			}
		}
		return dirfiles;
	}
	
	public void mergeAll()
	{
		def files = new ArrayList<File>(data.keySet());
		println files
		if (files.size() < 1)
			return;
		
		File refDir = files.get(0);
		File refClass = data.get(refDir)[0];
		String refBundle = data.get(refDir)[1];
		def externalized = data.get(refDir)[2];
		
		System.out.println("ref dir: "+refDir);
		System.out.println("ref class: "+refClass);
		System.out.println("ref bundle: "+refBundle);
		System.out.println("ref files: "+externalized);
		
		// get externalized ref strings
		//      Lang            Key     externalized string 
		HashMap<String, HashMap<String, String>> externalizedLines = [:];
		for (File source : externalized) {
			
			def lines = new HashMap<String, String>();
			externalizedLines.put(source.getName(), lines);
			source.eachLine("UTF-8") {
				
				if (it =~ ".+=.+") {
					int idx = it.indexOf("=");
					lines.put(it.substring(0, idx), it.substring(idx+1))
				} else if (it =~ ".+=\$") {
					int idx = it.indexOf("=")
					String key = it.substring(0, idx)
					String value = ""
					lines.put(key, value)
				}
			}
		}
		
		// get references keys
		HashSet<String> refKeys = new HashSet<String>();
		refClass.eachLine("UTF-8") {
			if (it =~ ".+public static String .+")
				refKeys << it.trim()
		}
		
		println "BEFORE"
		println "refKeys size: "+refKeys.size();
		println "fr externalizedLines size: "+externalizedLines.get("messages_fr.properties").size();
		println "en externalizedLines size: "+externalizedLines.get("messages.properties").size();
		
		// for all sub dir
		for (int i = 1 ; i < files.size() ; i++) {
			File srcDir = files.get(i);
			File srcClass = data.get(srcDir)[0];
			String srcBundle = data.get(srcDir)[1];
			
			def srcExternalized = data.get(srcDir)[2];
			// merge externalized strings
			for (File source : srcExternalized) {
				def lines = externalizedLines.get(source.getName());
				source.eachLine("UTF-8") {
					if (it =~ ".+=.+") {
						int idx = it.indexOf("=")
						String key = it.substring(0, idx)
						String value = it.substring(idx+1)
						if (lines.containsKey(key) && lines.get(key) != value)
							println "merged message of "+source.getName()+": "+it+ " >> "+lines.get(key)
						lines.put(key, value)
					} else if(it =~ ".+=\$") {
						int idx = it.indexOf("=")
						String key = it.substring(0, idx)
						String value = ""
						lines.put(key, value)
					}
				}
				source.delete(); // bye bye *.properties files
			}
						
			// merge static String
			if(!srcClass.exists())
			{
				println "file removed: "+srcClass
				println  srcDir
				println  srcClass
				println  srcBundle
				println  srcExternalized
				continue;
			}
			srcClass.eachLine("UTF-8"){
				if(it =~ ".+public static String .+")
				{
//					if(refKeys.contains(it.trim()))
//						println "Externalized key merged: "+it
					refKeys << it.trim()
				}
			}
			srcClass.delete(); // bye bye Messages.java
		}
		
		println "AFTER"
		println "refKeys size: "+refKeys.size();
		println "fr externalizedLines size: "+externalizedLines.get("messages_fr.properties").size();
		println "en externalizedLines size: "+externalizedLines.get("messages.properties").size();
		
		//check unused externalized strings
		def keysDeclared = refKeys.collect{it.substring(21, it.length() -1) };
		System.out.println("number of keys declared in Message.java: "+keysDeclared.size());
		def missingDeclarations = new HashMap<String, HashSet<String>>();
		def missingExternalizations = new HashMap<String, HashSet<String>>();
		for(def lang : externalizedLines.keySet())
		{
			def keys = externalizedLines.get(lang).keySet(); // keys  in propeties files
			missingExternalizations.put(lang, new HashSet<String>());
			missingDeclarations.put(lang, new HashSet<String>());
			for(String key : keys)
				if(!keysDeclared.contains(key))
					missingDeclarations.get(lang) << key
					
			for(String key : keysDeclared)
				if(!keys.contains(key))
					missingExternalizations.get(lang) << key
		}
		
		println "fix missing key declaration and externalization"
		for(String lang: missingExternalizations.keySet())
		{
			println "Lang: "+lang
			println " missingDeclarations: "+missingDeclarations.get(lang)
			for(String key : missingDeclarations.get(lang))
				refKeys << "public static String "+key+";"
			println " missingExternalizations: "+missingExternalizations.get(lang)
			for(String key : missingExternalizations.get(lang))
				externalizedLines.get(lang).put(key, "N/A")
		}
		
		println "AFTER FIX"
		println "refKeys size: "+refKeys.size();
		println "fr externalizedLines size: "+externalizedLines.get("messages_fr.properties").size();
		println "en externalizedLines size: "+externalizedLines.get("messages.properties").size();
		
	
		// write externalized lines
		for(File source : externalized)
		{
			source.withWriter("UTF-8") { out ->
				def hash = externalizedLines.get(source.getName())
				def keys = hash.keySet();
				keys = keys.sort()
				keys.each() { 
					out.writeLine(it+"="+hash.get(it))
				}
			}
		}

		// write classRef File
		refClass.withWriter("UTF-8") { out ->			
			// write start
			out.writeLine('''
package org.txm.scripts.;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

	// The Constant BUNDLE_NAME. 
	private static final String BUNDLE_NAME = "org.txm.messages"; //$NON-NLS-1$
	
				''')
			
			//write keys
			for(String key : refKeys.sort())
				out.writeLine("\t"+key);
			
			// write end
			out.writeLine('''
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
				''')
		}
	}
	
	public static void main(String[] args)
	{
		File projectdir = new File("/home/mdecorde/workspace37/org.txm.core/src/java")
		def rez = new MergeExternalizedStringFiles(projectdir);
	}
}
