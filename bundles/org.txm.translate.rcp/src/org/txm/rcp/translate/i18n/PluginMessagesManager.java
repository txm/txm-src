package org.txm.rcp.translate.i18n;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.txm.rcp.translate.devtools.NormalizeKeys;
import org.txm.utils.BiHashMap;
import org.txm.utils.FileUtils;
import org.txm.utils.PatternUtils;
import org.txm.utils.io.IOUtils;

/**
 * 
 * Check any missing key in both of messages.properties and Message.java files
 *
 * @author mdecorde
 * @author sjacquot
 *
 */
public class PluginMessagesManager {
	
	/**
	 * Files encoding.
	 */
	public static String UTF8 = "UTF-8";
	
	public final static String FILE_SEPARATOR = System.getProperty("file.separator");
	
	/**
	 * Debug state.
	 */
	protected boolean debug;
	
	/**
	 * Project root directory.
	 */
	protected File projectDirectory;
	
	/**
	 * Java message file, eg.: TXMCoreMessages.java, ConcordanceUIMessages.java, etc.
	 */
	protected File javaMessageFile;
	
	/**
	 * Source files of the project.
	 */
	protected ArrayList<File> srcFiles;
	
	/**
	 * Extensions to use when creating the source files list.
	 */
	protected String[] srcFilesExtensions = new String[] { "java", "groovy" };
	
	
	/**
	 * Global REGEX containing all keys to later use for search in files.
	 */
	protected Pattern keysGlobalREGEX;
	
	/**
	 * Index of the keys used and their associated files.
	 */
	protected TreeMap<String, LinkedHashSet<File>> usedKeysFilesIndex = new TreeMap<>();
	
	/**
	 * the XXXMessages.java messages keys
	 */
	LinkedHashSet<String> messageKeys = new LinkedHashSet<>();
	
	/**
	 * plugin.xml file -> contains eventually keys
	 */
	protected File pluginXMLFile;
	
	public File getPluginXMLFile() {
		return pluginXMLFile;
	}
	
	public LinkedHashSet<String> getOsgiKeys() {
		return osgiKeys;
	}
	
	public TreeMap<File, HashMap<String, String>> getOsgiLangs() {
		return osgiLangs;
	}
	
	/**
	 * the OSGI-INF/l10n/bundle[_lang].properties OSGI message keys
	 */
	LinkedHashSet<String> osgiKeys = new LinkedHashSet<>();
	
	/**
	 * The OSGI messages stored by properties file
	 */
	TreeMap<File, HashMap<String, String>> osgiLangs = new TreeMap<>();
	
	/**
	 * Stores the key modifications for further saves in the source files
	 */
	HashMap<String, String> keyModifications = new HashMap<>();
	
	/**
	 * The messages stored by properties file
	 */
	TreeMap<File, HashMap<String, String>> langs = new TreeMap<>();
	
	/**
	 * the properties files langs
	 */
	BiHashMap<File, String> file2lang = new BiHashMap<>();

	private File osgiInf;
	
	/**
	 * 
	 * @param projectDirectory
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public PluginMessagesManager(File projectDirectory) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		this(projectDirectory, false);
	}
	
	/**
	 * 
	 * @param projectDirectory
	 * @param debug
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public PluginMessagesManager(File projectDirectory, boolean debug) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		this(projectDirectory, findMessageFile(projectDirectory), debug);
	}
	
	
	
	/**
	 * 
	 * @param projectDirectory
	 * @param javaMessageFile
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public PluginMessagesManager(File projectDirectory, File javaMessageFile) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		this(projectDirectory, javaMessageFile, false);
	}
	
	
	
	/**
	 * 
	 * @param projectDirectory
	 * @param javaMessageFile
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public PluginMessagesManager(File projectDirectory, File javaMessageFile, boolean debug) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		
		this.javaMessageFile = javaMessageFile;
		this.projectDirectory = projectDirectory;
		this.pluginXMLFile = new File(projectDirectory, "plugin.xml");
		this.srcFiles = new ArrayList<>();
		this.debug = debug;
		
		if (debug) {
			System.out.println("********************************************************************************************************************");
			System.out.println("PluginMessagesManager.PluginMessages(): project root directory = " + this.projectDirectory);
			System.out.println("PluginMessagesManager.PluginMessages(): java message file = " + this.javaMessageFile);
		}
		
		// create the project source files list
		this.createSourceFilesList();
		
		// this.createKeysGlobalREGEX();
		this.createUsedKeysFilesIndex();
		
		
		if (this.javaMessageFile != null) {
			
			File[] propFiles = javaMessageFile.getParentFile().listFiles(IOUtils.HIDDENFILE_FILTER);
			for (File propFile : propFiles) {
				if (!FileUtils.isExactlyExtension(propFile, "properties")) continue;
				
				// messages.properties, messages_xx.properties
				String lang = FileUtils.stripExtension(propFile);
				langs.put(propFile, new HashMap<String, String>());
				file2lang.put(propFile, lang);
				
				HashMap<String, String> hash = langs.get(propFile);
				
				LinkedHashMap<String, String> props1 = new LinkedHashMap<String, String>();
				props1 = loadFromProperties(IOUtils.getReader(propFile, UTF8));
				for (Object k : props1.keySet()) {
					hash.put(k.toString(), props1.get(k).toString());
				}
			}
			
			BufferedReader reader2 = new BufferedReader(new InputStreamReader(new FileInputStream(javaMessageFile), UTF8));
			String line2 = reader2.readLine();
			while (line2 != null) {
				line2 = line2.trim();
				
				if (line2.startsWith("public static String")) {
					int idx = line2.indexOf("//");
					if (idx > 0 && line2.contains("\"")) { // remove comment from line
						// System.out.println("COMMENT DETECTED AFTER KEY: line="+line2+" IN "+javaMessageFile);
						line2 = line2.substring(0, idx).trim();
					}
					if (line2.endsWith(";") && !line2.contains("=") && !line2.contains("\"")) {
						line2 = line2.substring(21, line2.length() - 1);
						
						if (debug) {
							System.out.println("PluginMessagesManager.PluginMessagesManager(): messages key: " + line2 + " added.");
						}
						messageKeys.add(line2);
					}
				}
				
				line2 = reader2.readLine();
			}
			reader2.close();
			
			if (debug) {
				System.out.println("PluginMessagesManager.PluginMessagesManager(): number of keys: " + this.messageKeys.size() + ".");
			}
			
			// try loading OSGI messages
			if (pluginXMLFile.exists()) {
				ArrayList<String> result = IOUtils.findWithGroup(pluginXMLFile, "\"%([^\"]+)\"");
				osgiKeys.addAll(result);
				
				osgiInf = new File(projectDirectory, "OSGI-INF/l10n");
				if (osgiInf.exists()) {
					for (File propFile : osgiInf.listFiles(IOUtils.HIDDENFILE_FILTER)) {
						if (propFile.getName().startsWith("bundle") && propFile.getName().endsWith(".properties")) {
							LinkedHashMap<String, String> props = new LinkedHashMap<String, String>();
							props = loadFromProperties(IOUtils.getReader(propFile, "iso-8859-1"));
							HashMap<String, String> h = new HashMap<>();
							for (Object k : props.keySet())
								h.put(k.toString(), props.get(k.toString()));
							osgiLangs.put(propFile, h);
						}
					}
				}
				// System.out.println(osgiKeys);
				// System.out.println(osgiLangs);
			}
			
			if (debug) {
				this.dumpUsedKeysFilesIndex();
			}
		}
		else {
			if (debug) {
				System.err.println("PluginMessagesManager.PluginMessagesManager(): skipped, java message file not found for project " + this.projectDirectory + ".");
			}
		}
	}
	
	public static LinkedHashMap<String, String> loadFromProperties(BufferedReader reader) {
		
		LinkedHashMap<String, String> props = new LinkedHashMap<String, String>();
		
		try {
			String line = reader.readLine();
			while (line != null) {
				if (!line.startsWith("#") && line.contains("=")) {
					String[] split = line.split("=", 2);
					props.put(split[0].trim(), PatternUtils.unquote(split[1], specialChars));
				}
				line = reader.readLine();
			}
			reader.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return props;
	}
	public static final String specialChars = "#:=!";
	public static boolean saveToProperties(LinkedHashMap<String, String> props, File file, String encoding) {
		
		try {
			PrintWriter writer = IOUtils.getWriter(file, encoding);
			return saveToProperties(props, writer);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static boolean saveToProperties(LinkedHashMap<String, String> props, PrintWriter writer) {
		
		try {
			
			for (String key : props.keySet()) {
				writer.println(key.trim()+"="+PatternUtils.quote(props.get(key), specialChars));
			}
			writer.close();
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	// /**
	// * Creates a global REGEX containing all keys to later search in files.
	// */
	// public void createKeysGlobalREGEX() {
	//
	// if(messageKeys.size() == 0) {
	// return;
	// }
	//
	// StringBuffer buffer = new StringBuffer();
	// buffer.append(this.getMessageClassName() + "\\\\.(");
	//
	// int i = 0;
	// for (String key : messageKeys) {
	// if(i > 0) {
	// buffer.append("|" + key);
	// }
	// else {
	// buffer.append(key);
	// }
	// i++;
	// }
	//
	// // remove first pipe
	// //buffer.deleteCharAt(0);
	//
	// this.keysGlobalREGEX = Pattern.compile("(" + buffer + "))");
	//
	// if(debug) {
	// System.out.println("PluginMessagesManager.createKeysGlobalREGEX(): REGEX created: " + this.keysGlobalREGEX + ".");
	// }
	//
	// }
	
	
	/**
	 * Creates a global REGEX containing all keys to later search in files.
	 */
	public void createKeysGlobalREGEX() {
		StringBuffer buffer = new StringBuffer();
		for (String key : messageKeys) {
			buffer.append("|" + this.getKeyFullName(key).replaceAll("\\.", "\\\\."));
		}
		
		// remove first pipe
		buffer.deleteCharAt(0);
		
		this.keysGlobalREGEX = Pattern.compile("(" + buffer + ")");
		
		if (debug) {
			System.out.println("PluginMessagesManager.createKeysGlobalREGEX(): REGEX created: " + this.keysGlobalREGEX + ".");
		}
		
	}
	
	
	
	public static File findMessageFile(File projectDirectory2) {
		File messagesPackageDir = new File(projectDirectory2, "src/" + projectDirectory2.getName().replaceAll("\\.", "/") + "/messages");
		
		if (!messagesPackageDir.exists()) {
			messagesPackageDir = new File(projectDirectory2, "src/java/" + projectDirectory2.getName().replaceAll("\\.", "/") + "/messages");
		}
		
		if (!messagesPackageDir.exists()) {
			messagesPackageDir = new File(projectDirectory2, "src/main/java/" + projectDirectory2.getName().replaceAll("\\.", "/") + "/messages");
		}
		
		if (!messagesPackageDir.exists()) {
			return null;
		}
		
		for (File messagesJavaFile : messagesPackageDir.listFiles(IOUtils.HIDDENFILE_FILTER)) {
			if (messagesJavaFile.isDirectory()) continue;
			if (!messagesJavaFile.getName().endsWith("Messages.java")) continue;
			return messagesJavaFile;
		}
		return null;
	}
	
	// public static File findMessageFile(File projectDirectory2) {
	//// System.out.println("PluginMessagesManager.findMessageFile() " + FILE_SEPARATOR);
	// File messagesPackageDir = new File(projectDirectory2, "src" + FILE_SEPARATOR + projectDirectory2.getName().replaceAll("\\.", "\\" + FILE_SEPARATOR) + FILE_SEPARATOR + "messages");
	//
	// if (!messagesPackageDir.exists()) {
	// messagesPackageDir = new File(projectDirectory2, "src" + FILE_SEPARATOR + "java" + FILE_SEPARATOR + projectDirectory2.getName().replaceAll("\\.", "\\" + FILE_SEPARATOR) + FILE_SEPARATOR +
	// "messages");
	// }
	//
	// if (!messagesPackageDir.exists()) {
	// messagesPackageDir = new File(projectDirectory2, "src" + FILE_SEPARATOR + "main" + FILE_SEPARATOR + "java" + FILE_SEPARATOR + projectDirectory2.getName().replaceAll("\\.", "\\" +
	// FILE_SEPARATOR) + FILE_SEPARATOR + "messages");
	// }
	//
	// if (!messagesPackageDir.exists()) {
	// return null;
	// }
	//
	// for (File messagesJavaFile : messagesPackageDir.listFiles(IOUtils.HIDDENFILE_FILTER)) {
	// if (messagesJavaFile.isDirectory()) continue;
	// if (!messagesJavaFile.getName().endsWith("Messages.java")) continue;
	// return messagesJavaFile;
	// }
	// return null;
	// }
	
	
	
	/**
	 * Creates/updates the index of the keys used and their associated files.
	 * 
	 * @throws IOException
	 */
	public void createUsedKeysFilesIndex() throws IOException {
		this.usedKeysFilesIndex = new TreeMap<>();
		
		// Old version, based on real key name
		// for (File file : this.srcFiles) {
		// for (String key : messageKeys) {
		// if(debug) {
		// System.out.println("PluginMessagesManager.createUsedKeysIndex(): looking for key " + this.getKeyFullName(key) + " in project source files...");
		// }
		// String lines = IOUtils.getText(file, PluginMessagesManager.ENCODING);
		// if(lines.contains(this.getKeyFullName(key))) {
		// if (!this.usedKeysFilesIndex.containsKey(key)) {
		// this.usedKeysFilesIndex.put(key, new LinkedHashSet<File>());
		// }
		// this.usedKeysFilesIndex.get(key).add(file);
		// };
		// }
		// }
		
		// New version using REGEX
		for (File file : this.srcFiles) {
			if (!file.exists()) continue;
			String lines = IOUtils.getText(file, PluginMessagesManager.UTF8);
			
			Matcher m = WorkspaceMessagesManager.KEY_REGEX.matcher(lines);
			// Matcher m = this.keysGlobalREGEX.matcher(lines);
			while (m.find()) {
				// Get the matching string
				String key = m.group();
				
				if (!this.usedKeysFilesIndex.containsKey(key)) {
					this.usedKeysFilesIndex.put(key, new LinkedHashSet<File>());
				}
				this.usedKeysFilesIndex.get(key).add(file);
				
				if (debug) {
					System.out.println("PluginMessagesManager.createUsedKeysFilesIndex(): messages key: " + key + " added for file " + file + ".");
				}
				
			}
			
		}
	}
	
	
	/**
	 * Dumps in console the index of the keys used and their associated files.
	 */
	public void dumpUsedKeysFilesIndex() {
		
		System.out.println("PluginMessagesManager.dumpUsedKeysIndex(): dumping used keys files index...");
		
		if (this.usedKeysFilesIndex == null) {
			System.out.println("PluginMessagesManager.dumpUsedKeysIndex(): project have no used keys.");
			return;
		}
		
		for (Map.Entry<String, LinkedHashSet<File>> entry : this.usedKeysFilesIndex.entrySet()) {
			System.out.println("key: " + entry.getKey());
			if (entry.getValue().isEmpty()) {
				System.out.println("   file(s): -not used in any file of the project " + this.getProjectDirectory() + "-");
			}
			else {
				for (File file : entry.getValue()) {
					System.out.println("   file(s): " + file);
				}
			}
		}
		
		System.out.println("PluginMessagesManager.dumpUsedKeysIndex(): number of used keys: " + this.usedKeysFilesIndex.size() + ".");
	}
	
	/***
	 * @param key
	 * @return all source files using the specified message key (except the main Java message file).
	 */
	public LinkedHashSet<File> getFilesUsingKey(String key) {
		if (usedKeysFilesIndex.containsKey(key)) {
			return this.usedKeysFilesIndex.get(key);
		}
		else {
			return new LinkedHashSet<>();
		}
	}
	
	/***
	 * @param key local key name
	 * @return all source files using the specified local message key (except the main Java message file).
	 */
	public LinkedHashSet<File> getFilesUsingLocalKey(String key) {
		return getFilesUsingKey(getKeyFullName(key));
	}
	
	
	/**
	 * Returns the keys of all messages that are not used in the project itself.
	 * 
	 * @return a sorted list containing all unused keys
	 */
	public ArrayList<String> getUnusedKeys() {
		return getUnusedKeys(usedKeysFilesIndex, debug);
	}
	
	/**
	 * @param usedKeysFilesIndex
	 * 
	 * @return a sorted list containing all unused keys of all messages that are not used in the used key files index.
	 */
	public static ArrayList<String> getUnusedKeys(TreeMap<String, LinkedHashSet<File>> usedKeysFilesIndex, boolean debug) {
		ArrayList<String> unusedKeys = new ArrayList<>();
		for (Map.Entry<String, LinkedHashSet<File>> entry : usedKeysFilesIndex.entrySet()) {
			if (entry.getValue().isEmpty()) {
				
				if (debug) {
					System.out.println("PluginMessagesManager.getUnusedKeys(): unused key found: " + entry.getKey() + ".");
				}
				
				unusedKeys.add(entry.getKey());
			}
		}
		
		//Collections.sort(unusedKeys);
		return unusedKeys;
	}
	
	/**
	 * @param lang
	 * @return the map of the messages keys and values for the specified language.
	 */
	public HashMap<String, String> getMessagesForLang(String lang) {
		if (javaMessageFile != null) {
			File p = new File(javaMessageFile.getParentFile(), "messages" + lang + ".properties");
			return langs.get(p);
		}
		else {
			return new HashMap<>();
		}
	}
	
	/**
	 * @param lang
	 * @return the map of the messages keys and values for the specified language.
	 */
	public HashMap<String, String> getOsgiMessagesForLang(String lang) {
		File p = new File(osgiInf, "bundle" + lang + ".properties");
		HashMap<String, String> all = getOsgiLangs().get(p);
		return all;
	}
	
	/**
	 * @param lang
	 * @return the map of the messages keys and values for the specified language.
	 */
	public String getOsgiMessageForLang(String lang, String key) {
		File p = new File(osgiInf, "bundle" + lang + ".properties");
		HashMap<String, String> all = getOsgiLangs().get(p);
		if (all == null) return null;
		return all.get(key);
	}
	
	/**
	 * @param langFile
	 * @return the map of the messages keys and values for the specified language.
	 */
	public HashMap<String, String> getMessagesForLang(File langFile) {
		return langs.get(langFile);
	}
	
	/**
	 * @return the map of the messages keys and values for the main language.
	 */
	public HashMap<String, String> getMessagesForDefaultLang() {
		return this.getMessagesForLang(""); //$NON-NLS-1$
	}
	
	public void put(String lang, String key, String message) {
		messageKeys.add(key);
		
		File p = new File(javaMessageFile.getParentFile(), "messages" + lang + ".properties");
		
		if (!langs.containsKey(p)) {
			langs.put(p, new HashMap<String, String>());
			file2lang.put(p, lang);
		}
		
		if (message == null) {
			langs.get(p).remove(key);
		}
		else {
			langs.get(p).put(key, message);
		}
	}
	
	
	public void putOSGI(String lang, String key, String message) {
		osgiKeys.add(key);
		
		File p = new File(projectDirectory, "OSGI-INF/l10n/bundle" + lang + ".properties");
		
		if (!osgiLangs.containsKey(p)) {
			osgiLangs.put(p, new HashMap<String, String>());
		}
		
		if (message == null) {
			osgiLangs.get(p).remove(key);
		}
		else {
			osgiLangs.get(p).put(key, message);
		}
	}
	
	public String get(String lang, String key) {
		File p = new File(javaMessageFile.getParentFile(), "messages" + lang + ".properties");
		
		if (!file2lang.containsKey(p)) return null;
		
		return langs.get(p).get(key);
	}
	
	public LinkedHashSet<String> getMessageKeys() {
		return messageKeys;
	}
	
	public File getMessageFile() {
		return javaMessageFile;
	}
	
	public String getMessageClassName() {
		if (javaMessageFile == null) return "";
		return javaMessageFile.getName().substring(0, javaMessageFile.getName().length() - 5);
	}
	
	public String getMessageFullClassName() {
		if (javaMessageFile == null) return "";
		return javaMessageFile.getAbsolutePath().substring(javaMessageFile.getAbsolutePath().lastIndexOf("org" + FILE_SEPARATOR + "txm" + FILE_SEPARATOR),
				javaMessageFile.getAbsolutePath().length() - 5).replace(FILE_SEPARATOR, ".");
	}
	
	/**
	 * @return the XXXMessages.java class name without the 'Messages' part
	 */
	public String getMessageName() {
		if (javaMessageFile == null) return "";
		return javaMessageFile.getName().substring(0, javaMessageFile.getName().length() - 5 - 8);
	}
	
	public String getMessageFullName() {
		if (javaMessageFile == null) return "";
		return javaMessageFile.getAbsolutePath().substring(javaMessageFile.getAbsolutePath().lastIndexOf("org" + FILE_SEPARATOR + "txm" + FILE_SEPARATOR) + 8,
				javaMessageFile.getAbsolutePath().length() - 5 - 8).replace(FILE_SEPARATOR, ".");
	}
	
	/**
	 * Returns the full name of the specified key including the Java message class name.
	 * 
	 * @param key
	 * @return
	 */
	public String getKeyFullName(String key) {
		return this.getMessageClassName() + "." + key;
	}
	
	public static Collator col = Collator.getInstance(Locale.FRANCE);
	
	public static Comparator comp = new Comparator<String>() {
		
		@Override
		public int compare(String arg0, String arg1) {
			return col.compare(arg0, arg1);
		}
	};
	static {
		col.setStrength(Collator.TERTIARY);
	}
	
	public static String convertToUnicode(String entry) {
		char[] chtb = entry.toCharArray();
		StringBuffer temp = new StringBuffer(6 * chtb.length);
		int BIT_MASK = (1 << 16);
		for (int i = 0; i < chtb.length; i++) {
			temp.append("\\u");
			temp.append(Integer.toHexString(chtb[i] | BIT_MASK).substring(1));
		}
		return temp.toString();
	}
	
	
	public void saveChanges(boolean replaceFiles) throws IOException {
		saveChanges(replaceFiles, true, true);
	}
	
	public void saveChanges(boolean replaceFiles, boolean doOsgi, boolean doMessages) throws IOException {
		
		if (javaMessageFile == null) return;
		
		// 1 Write bundle*.properties prop File
		if (doOsgi) {
			boolean utf8fixes = false;
			StringBuilder cmds = new StringBuilder();
			for (File propFile : osgiLangs.keySet()) {
				
				LinkedHashMap<String, String> props = new LinkedHashMap<String, String>();
				HashMap<String, String> h = osgiLangs.get(propFile);
//				ArrayList<String> sortedkeys = new ArrayList<String>();
//				sortedkeys.addAll(h.keySet());
//				Collections.sort(sortedkeys);
				for (String k : h.keySet()) {
					props.put(k, h.get(k));
				}
				
				File newPropFile = new File(propFile.getParentFile(), propFile.getName() + ".new");
				if (replaceFiles) {
					propFile.delete();
					newPropFile = propFile;
				}
				
				String encoding = "UTF-8";
				if (newPropFile.getName().startsWith("bundle") && !newPropFile.getName().contains("_ru")) {
					encoding = "iso-8859-1";
				}
				
				if (propFile.getName().contains("_ru")) {
					utf8fixes = true;
					cmds.append("native2ascii " + newPropFile + " > " + newPropFile + ".tmp\n");
					cmds.append("rm " + newPropFile + "\n");
					cmds.append("mv " + newPropFile + ".tmp " + newPropFile + "\n");
				}
				
				saveToProperties(props, newPropFile, encoding);
			}
			if (utf8fixes) {
				// System.out.println("WARNING: need to call: ");
				System.out.println(cmds.toString());
			}
		}
		
		// 2 Write messages*.properties prop File
		if (doMessages) {
			for (File propFile : langs.keySet()) {
				
				LinkedHashMap<String, String> props = new LinkedHashMap<String, String>();
				HashMap<String, String> h = langs.get(propFile);
				for (String k : h.keySet()) {
					props.put(k, h.get(k));
				}
				
				File newPropFile = new File(propFile.getParentFile(), propFile.getName() + ".new");
				if (replaceFiles) {
					propFile.delete();
					newPropFile = propFile;
				}
				saveToProperties(props, newPropFile, UTF8);
			}
			
			// 3 write Java message File
			File newJavaMessageFile = new File(javaMessageFile.getParentFile(), javaMessageFile.getName() + ".new");
			
			ArrayList<String> lines = IOUtils.getLines(javaMessageFile, UTF8);
			LinkedHashSet<String> oldKeys = new LinkedHashSet<>(IOUtils.findWithGroup(javaMessageFile, "public static String ([^\"=;]+);"));
			LinkedHashSet<String> newKeys = new LinkedHashSet<>(messageKeys);
			LinkedHashSet<String> writtenKeys = new LinkedHashSet<>();
			
			newKeys.removeAll(oldKeys);
			newKeys.removeAll(keyModifications.values()); // now contains only the very newly created keys
			
			if (replaceFiles) {
				newJavaMessageFile = javaMessageFile;
			}
			
			PrintWriter out = IOUtils.getWriter(newJavaMessageFile, UTF8);
			
			// update lines
			for (String line : lines) {
				String l = line.trim();// remove tabs
				String comments = "";
				int idx = l.indexOf("//"); // remove comments
				if (idx > 0) {
					comments = l.substring(idx).trim();
					l = l.substring(0, idx).trim();
				}
				
				if (l.startsWith("private static final String BUNDLE_NAME")) {
					out.println(line);
					
					// write the totally new keys
					if (newKeys.size() > 0) {
						out.println("\t");
					}
					for (String key : newKeys) {
						
						if (writtenKeys.contains(key)) continue; // key already written
						out.println("\tpublic static String " + key + ";");
						writtenKeys.add(key);
					}
				}
				else if (l.startsWith("public static String") && l.endsWith(";") && !l.contains("=") && !l.contains("\"")) {
					
					String key = l.substring(21, l.length() - 1);
					if (messageKeys.contains(key)) {
						if (writtenKeys.contains(key)) continue; // key already written
						out.println(line); // keep the line
						writtenKeys.add(key);
					}
					else if (keyModifications.containsKey(key)) {
						// the key has been renamed
						if (writtenKeys.contains(keyModifications.get(key))) continue; // key already written
						out.println("\tpublic static String " + keyModifications.get(key) + "; " + comments);
						writtenKeys.add(keyModifications.get(key));
					}
					else {
						// the key has been removed
					}
				}
				else {
					out.println(line);
				}
			}
			out.close();
		}
	}
	
	public void summary() {
		
		System.out.println(getMessageClassName());
		
		System.out.println("Messages keys");
		for (String key : messageKeys) {
			System.out.println(key);
		}
		
		System.out.println("Langs");
		for (File lang : langs.keySet()) {
			System.out.println("  ------------------------------------------------------------------------------------------------------------------------------");
			System.out.println("  " + lang);
			System.out.println("  ------------------------------------------------------------------------------------------------------------------------------");
			HashMap<String, String> hash = langs.get(lang);
			System.out.println(" keys=" + hash.keySet());
			for (String key : hash.keySet()) {
				System.out.println("  " + key + "=" + hash.get(key));
			}
		}
	}
	
	/**
	 * Parses the specified path and stores a list of the source files.
	 */
	private void createSourceFilesList(File path) {
		File[] list = path.listFiles(IOUtils.HIDDENFILE_FILTER);
		
		if (list == null) {
			return;
		}
		
		for (File file : list) {
			if (file.isDirectory()) {
				this.createSourceFilesList(file);
			}
			else if (!file.equals(this.javaMessageFile)
					&& Arrays.asList(this.srcFilesExtensions).contains(FilenameUtils.getExtension(file.getName()))) {
				
				if (debug) {
					System.out.println("PluginMessagesManager.createSourceFilesList(): adding source files " + file + ".");
				}
				else {
					// System.out.print(".");
				}
				
				this.srcFiles.add(file);
			}
		}
	}
	
	/**
	 * Parses the project and stores a list of the source files.
	 */
	public void createSourceFilesList() {
		
		if (debug) {
			System.out.println("PluginMessagesManager.loadSourceFilesList(): creating source files list for extensions [" + StringUtils.join(this.srcFilesExtensions, ", ") + "]...");
		}
		
		this.createSourceFilesList(this.projectDirectory);
		
		
		if (debug) {
			System.out.println("PluginMessagesManager.loadSourceFilesList(): done. Source files count = " + this.srcFiles.size() + ".");
		}
	}
	
	/**
	 * Rename a key and update in the properties file the keys
	 * 
	 * stores the modification to rename the keys later in the Workspace source files
	 * 
	 * @param oldName
	 * @param newName
	 */
	public void renameKey(String oldName, String newName) {
		
		if (!messageKeys.contains(oldName)) {
			System.err.println("PluginMessagesManager.renameKey(): key=" + oldName + " not found, aborting renaming to " + newName + ".");
			return;
		}
		
		// continue only if key name has changed
		if (oldName.equals(newName)) {
			if (debug) {
				System.out.println("PluginMessagesManager.renameKey(): no changes, skipped key: " + oldName + " = " + newName);
			}
			return;
		}
		
		messageKeys.remove(oldName);
		messageKeys.add(newName);
		
		for (File p : langs.keySet()) {
			HashMap<String, String> h = langs.get(p);
			if (h.containsKey(oldName)) {
				if (h.containsKey(newName)
						&& !h.get(newName).equals(h.get(oldName))) { // a value is already set for the new key
					System.err.println("PluginMessagesManager.renameKey(): WARNING new key=" + newName + " already set in " + p + " with value=" + h.get(newName) + " new value=" + h.get(oldName));
				}
				else {
					h.put(newName, h.get(oldName));
				}
				h.remove(oldName);
			}
		}
		
		// System.out.println("M+ "+oldName+" -> "+newName);
		keyModifications.put(oldName, newName);
	}
	
	/**
	 * Remove a messages from XXMessages.java and messages.properties files
	 * 
	 * @param key the local name of the key to remove
	 */
	public void removeKey(String key) {
		if (!messageKeys.contains(key)) {
			System.err.println("PluginMessagesManager.removeKey(): key=" + key + " not found, aborting removing.");
			return;
		}
		
		messageKeys.remove(key);
		
		for (File p : langs.keySet()) {
			HashMap<String, String> h = langs.get(p);
			if (h.containsKey(key)) {
				h.remove(key);
			}
		}
		
		keyModifications.remove(key); // if any modifications was done
	}
	
	public void newKey(String defaultMessage) {
		String key = NormalizeKeys.normalize(defaultMessage);
		String newKey = "" + key;
		int c = 1;
		while (messageKeys.contains(newKey)) {
			newKey = key + "_" + (c++);
		}
		
		messageKeys.add(newKey);
		getMessagesForDefaultLang().put(newKey, defaultMessage);
	}
	
	/**
	 * 
	 * @return the renamed keys
	 */
	public HashMap<String, String> getKeyModifications() {
		return keyModifications;
	}
	
	public BiHashMap<File, String> getFile2lang() {
		return file2lang;
	}
	
	public TreeMap<File, HashMap<String, String>> getLangs() {
		return langs;
	}
	
	/**
	 * Gets the project root directory.
	 * 
	 * @return the projectDirectory
	 */
	
	public File getProjectDirectory() {
		return projectDirectory;
	}
	
	
	/**
	 * Dumps the specified BiHashMap<String, String> to standard output.
	 */
	public void dump(BiHashMap<String, String> messages) {
		for (String key : messages.getKeys()) {
			System.out.println("PluginMessagesManager.dump(): " + key + "=" + messages.get(key));
		}
	}
	
	
	/**
	 * Dumps the keys replacements table.
	 */
	public void dumpKeysReplacements() {
		System.out.println("PluginMessagesManager.dumpKeysReplacements(): keys replacements table");
		for (String key : this.keyModifications.keySet()) {
			System.out.println("PluginMessagesManager.dumpKeysReplacements(): " + key + "=>" + this.keyModifications.get(key));
		}
	}
	
	/**
	 * @return the usedKeysIndex
	 */
	public TreeMap<String, LinkedHashSet<File>> getUsedKeysFilesIndex() {
		return usedKeysFilesIndex;
	}
	
	/**
	 * 
	 * @param args
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		
		long startTime = System.currentTimeMillis();
		
		// File projectFile = new File(new File(System.getProperty("user.dir")).getParentFile().getAbsolutePath() + "/org.txm.rcp");
		File projectFile = new File(new File(System.getProperty("user.dir")).getParentFile().getAbsolutePath() + "/org.txm.core");
		
		PluginMessagesManager pmManager = new PluginMessagesManager(projectFile, true);
		// test to find files using the specified key
		/*
		 * String key = "TXMCoreMessages.binaryDirectory";
		 * LinkedHashSet<File> files = pmManager.getFilesUsingKey(key);
		 * System.out.println("getFilesUsingKey: files using key: " + key);
		 * for(File file : files) {
		 * System.out.println("   file: " + file);
		 * }
		 * key = "TXMCoreMessages.common_frequency";
		 * files = pmManager.getFilesUsingKey(key);
		 * System.out.println("getFilesUsingKey: files using key: " + key);
		 * for(File file : files) {
		 * System.out.println("   file: " + file);
		 * }
		 * // test to find unused keys
		 * ArrayList<String> unusedKeys = pmManager.getUnusedKeys();
		 * for (int i = 0; i < unusedKeys.size(); i++) {
		 * System.out.println("findUnusedKeys: key " + unusedKeys.get(i) + " is unused in project " + pmManager.getProjectDirectory() + " (main language value = " +
		 * pmManager.getMessagesForLang("").get(unusedKeys.get(i)) + ")");
		 * }
		 */
		// dict.summary();
		
		//pmManager.saveChanges(false);
		
		System.out.println("dir=" + pmManager.getProjectDirectory());
		System.out.println("Elapsed time:" + ((double) (System.currentTimeMillis() - startTime) / 1000));
		
		System.out.println("PluginMessagesManager.main(): done.");
	}
}
