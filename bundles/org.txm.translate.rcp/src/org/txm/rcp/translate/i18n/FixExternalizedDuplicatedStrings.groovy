package org.txm.rcp.translate.i18n

import org.txm.utils.DeleteDir;

/**
 * Browse messages files and look for duplicated values
 * @author mdecorde
 *
 */
class FixExternalizedDuplicatedStrings {
	
	static main(args) {
		
//		File propertyFile = new File("/home/mdecorde/Bureau/Experiences/rcp/messages.properties");
//		File messageClass = new File("/home/mdecorde/Bureau/Experiences/rcp/Messages.java");
//		File srcdirectory = new File("/home/mdecorde/Bureau/Experiences/rcp");
		File propertyFile = new File("/home/mdecorde/workspace37/org.txm.rcp/src/main/java/org/txm/rcp/messages.properties");
		File messageClass = new File("/home/mdecorde/workspace37/org.txm.rcp/src/main/java/org/txm/rcp/Messages.java");
		File srcdirectory = new File("/home/mdecorde/workspace37/org.txm.rcp/src/main/java/org/txm/rcp");
		
		if (!(propertyFile.exists() && propertyFile.canRead() && propertyFile.canWrite() && propertyFile.isFile())) {
			println "error file : "+propertyFile
			return
		}
		
		// build hash of externalised Strings
		//						  key	  externalized string
		def entries = new HashMap<String, String>()
		
		String encoding = "UTF-8";
		if (propertyFile.getName().startsWith("bundle")) {
			encoding = "iso-8859-1";
		}
		
		propertyFile.eachLine(encoding){
			if (it =~ ".+=.+") {
				int idx = it.indexOf("=")
				String key = it.substring(0, idx)
				String value = it.substring(idx+1)
				entries.put(key, value)
			}
			else if (it =~ ".+=\$") {
				int idx = it.indexOf("=")
				String key = it.substring(0, idx)
				String value = ""
				entries.put(key, value)
			}
		}
		
		// invert hash
		def hash = entries
		def reverse = [:]
		hash.each{
			if (reverse[it.value] == null) {
				reverse[it.value] = []
			}
			reverse[it.value] << it.key
		};
		
		for(def key : reverse.keySet())
			if(reverse[key].size() > 1)
				println "doublons of $key: "+reverse[key]
		
		// translate dictionnary
		def dictionnary = [:]
		for (def value : reverse.keySet()) {
			
			def keys = reverse[value];
			def first = keys[0];
			for (int i = 1 ; i < keys.size() ; i++) // the first key will not be removed
			{
				def key = keys.get(i)
				if (key != first) {
					dictionnary.put(key , first)
					println key+ " >> "+first
				}
			}
		}
		
		// patch source files : iterates over all java files
		
		/*def files = DeleteDir.scanDirectory(srcdirectory, true, true);
		for(File f : files)
		{
			if(f.getName().endsWith(".java") && f.getName() != "Messages.java")
			{
				def lines = f.readLines("UTF-8")
				f.withWriter("UTF-8"){ writer ->
					
					for(String line : lines)
					{
						for(String key : dictionnary.keySet())
						{
							String old = line;
							//line = line.replaceAll(, "Messages."+dictionnary[key]);
							line = (line =~ /Messages\.$key([^0-9]|$)/).replaceAll("Messages."+dictionnary[key]+'$1')

							if(old != line)
								System.out.println(f.getAbsolutePath()+":"+old+": replaced "+"Messages\\."+key+"[^0-9] Messages."+dictionnary[key]);
						}
						writer.writeLine(line);
					}	
				}
			}
		}
		
		// remove key declaration from Messages.java
		String txt = messageClass.getText("UTF-8")
		String[] split = txt.split("\n");
		messageClass.withWriter "UTF-8", { writer -> 
			for(String line : split)
			{
				if(line.contains("public static String "))
				{
					for(String key : dictionnary.keySet())
					{
						if(line.contains("public static String "+key+";"))
						{
							line = line.trim()
							line = null
							println "remove key: "+key
							break;
						}
					}
					if(line != null)
						writer.writeLine(line);
				}
				else
					writer.writeLine(line);
			}
		}*/
		
		// rewrite messages.properties
		def keytoRemove = dictionnary.keySet();
		propertyFile.withWriter("UTF-8") { writer ->
			for(String key : entries.keySet().sort())
				if(!keytoRemove.contains(key))
					writer.writeLine(key+"="+entries[key])
		}
	}
}
