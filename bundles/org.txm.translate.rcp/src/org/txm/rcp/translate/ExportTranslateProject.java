package org.txm.rcp.translate;

import java.util.ArrayList;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.internal.wizards.datatransfer.ArchiveFileExportOperation;
import org.txm.Toolbox;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.utils.ConsoleProgressBar;

public class ExportTranslateProject extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IProject newProject = workspace.getRoot().getProject("translate");
		if (!newProject.exists()) {
			System.out.println("You must create the translate project before.");
			return null;
		}
		
		if (!newProject.isOpen()) {
			System.out.println("You must open the translate project before.");
			return null;
		}
		
		String path = Toolbox.getTxmHomePath()+"/translate.zip";
		ArrayList<IResource> propertiesFiles = new ArrayList<IResource>();
		try {
			findPropertiesFile(newProject.getFolder("plugins"), propertiesFiles);
		} catch (CoreException e1) {
			e1.printStackTrace();
			return null;
		}
		
		ArchiveFileExportOperation exporter = new ArchiveFileExportOperation(null, propertiesFiles, path);
		exporter.setUseCompression(true);
		exporter.setIncludeLinkedResources(true);
		ConsoleProgressBar cbp = new ConsoleProgressBar(100);
		try {
			System.out.println("Exporting translation to "+path);
			exporter.run(cbp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cbp.done();
		return null;
	}

	public static ArrayList<IResource> findPropertiesFile(IResource r, ArrayList<IResource> propertiesFiles) throws CoreException {
		if (r instanceof IFolder) {
			IFolder dir = (IFolder)r;
			
			for (IResource s : dir.members()) {
				findPropertiesFile(s, propertiesFiles);
			}
		} else if (r instanceof IFile) {
			IFile file = (IFile)r;
			String name = file.getName();
			if (name.endsWith(".properties") && (name.startsWith("bundle") || name.startsWith("messages"))) {
				propertiesFiles.add(file);
			}
		}
		return propertiesFiles;
	}
}
