package org.txm.rcp.translate;

import java.util.ArrayList;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.search.ui.ISearchQuery;
import org.eclipse.search.ui.NewSearchUI;
import org.eclipse.search.ui.text.TextSearchQueryProvider;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkingSet;
import org.txm.rcp.handlers.BaseAbstractHandler;

public class SearchStringsToTranslate extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IProject newProject = CreateTranslateProject.getOrCreateProject();

		// project should exists now, we can show it
		try {
			openSearchView(newProject);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static final String SEARCHVIEW_ID = "org.eclipse.search.ui.views.SearchView";
	public static void openSearchView(IProject newProject) throws CoreException {

		IViewPart view = CreateTranslateProject.openView();
		if (view != null) {
			CreateTranslateProject.openTranslateWorkingSet(view, newProject);

//			ResourceNavigator navigator = (ResourceNavigator)view;
//			IWorkingSet wk = navigator.getWorkingSet();
//			ArrayList<IResource> res = new ArrayList<IResource>();
//			for (IAdaptable a : wk.getElements()) {
//				if (a instanceof IResource) {
//					res.add((IResource) a);
//				}
//			}
			
//			TextSearchQueryProvider provider= TextSearchQueryProvider.getPreferred();
//			ISearchQuery query = provider.createQuery("NA_", res.toArray(new IResource[res.size()]));
//
//			NewSearchUI.runQueryInBackground(query);
		}
	}
}
