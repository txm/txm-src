package org.txm.rcp.translate;

import java.util.ArrayList;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.WorkingSet;
import org.eclipse.ui.views.framelist.FrameList;
import org.eclipse.ui.views.framelist.GoIntoAction;
import org.txm.Toolbox;
import org.txm.core.preferences.TXMPreferences;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.logger.Log;

public class CreateTranslateProject extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		

		IProject newProject = getOrCreateProject();

		// project should exists now, we can show it
		IViewPart view = openView();
		if (view != null) {
			openTranslateWorkingSet(view, newProject);
		}

		return null;
	}

	public static void openTranslateWorkingSet(IViewPart view, IProject newProject) {
		
		org.eclipse.ui.navigator.CommonNavigator navigator = (org.eclipse.ui.navigator.CommonNavigator)view;
//		try {
//			IFolder plugins = newProject.getFolder("plugins");
//
//			//focus on plugins in the plugins directory (link) 
//			navigator.getTreeViewer().setSelection(new StructuredSelection(plugins));
//			FrameList fm = navigator.getFrameList();
//			GoIntoAction a = new GoIntoAction(fm);
//			a.run();
//
//			// show only the properties files
//			ArrayList<IResource> propertiesFiles = new ArrayList<>();
//			ExportTranslateProject.findPropertiesFile(plugins, propertiesFiles);
//			IResource[] elements = propertiesFiles.toArray(new IResource[propertiesFiles.size()]);
//
//			WorkingSet wk = new WorkingSet("Translate TXM", "Translate TXM", elements);
//			navigator.setWorkingSet(wk);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	public static final String NAVIGATOR_ID = "org.eclipse.ui.views.ResourceNavigator";
	public static IViewPart openView() {
		// open Element View if not opened
		try {
			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			IViewPart view = page.findView(NAVIGATOR_ID);
			if (view == null) {
				view = page.showView(NAVIGATOR_ID);
			}

			view.getSite().getPage().activate(view);
			return view;
		} catch (PartInitException e1) {
			System.out.println("Part initialisation error: "+e1.getLocalizedMessage());
			Log.printStackTrace(e1);
		}
		return null;
	}
	
	public static IProject getOrCreateProject() {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IProject newProject = workspace.getRoot().getProject("translate");
		IScopeContext projectScope = new ProjectScope(newProject);

		// ensure the project exists
		if (!newProject.exists()) {
			IProjectDescription desc = workspace.newProjectDescription(newProject.getName());
			desc.setComment("TXT translate project");
			try {
				newProject.create(desc, null);

				if (!newProject.isOpen()) {
					ConsoleProgressBar cpb = new ConsoleProgressBar(2);
					newProject.open(cpb);

					cpb.done();

					// set global encoding preferences
					projectScope.getNode("org.eclipse.core.resources/encoding").put("<project>", "UTF-8");
					projectScope.getNode("org.eclipse.core.resources").flush();
					projectScope.getNode("org.eclipse.core.runtime").put("line.separator", "\n");
					projectScope.getNode("org.eclipse.core.runtime").flush();

					// link to TXM plugins files
					cpb = new ConsoleProgressBar(2);
					IFolder srcFolder = newProject.getFolder("plugins");
					IPath path = new Path(Toolbox.getInstallDirectory()+"/plugins");
					srcFolder.createLink(path, IResource.ALLOW_MISSING_LOCAL, cpb);

					// set the encoding bundle=UTF-8, messages=utf-8
					ArrayList<IResource> propertiesFiles = new ArrayList<IResource>();
					ExportTranslateProject.findPropertiesFile(srcFolder, propertiesFiles);
					for (IResource e : propertiesFiles) {
						IFile f = (IFile)e;
						if (f.getName().startsWith("messages")) {
							projectScope.getNode("org.eclipse.core.resources/encoding").put(f.getProjectRelativePath().toString(), "UTF-8");
						}
						projectScope.getNode("org.eclipse.core.resources/encoding").flush();
					}

					//TXMPreferences.put("org.eclipse.ui.workbench", "resourcetypes", "<?xml version\\=\"1.0\" encoding\\=\"UTF-8\"?>\\n<editors version\\=\"3.1\">\\n<info extension\\=\"shtml\" name\\=\"*\">\\n<editor id\\=\"org.eclipse.ui.browser.editorSupport\"/>\\n</info>\\n<info extension\\=\"htm\" name\\=\"*\">\\n<editor id\\=\"org.eclipse.ui.browser.editorSupport\"/>\\n</info>\\n<info extension\\=\"html\" name\\=\"*\">\\n<editor id\\=\"org.eclipse.ui.browser.editorSupport\"/>\\n</info>\\n<info extension\\=\"properties\" name\\=\"*\">\\n<editor id\\=\"com.essiembre.eclipse.rbe.ui.editor.ResourceBundleEditor\"/>\\n<editor id\\=\"org.eclipse.ui.DefaultTextEditor\"/>\\n<defaultEditor id\\=\"com.essiembre.eclipse.rbe.ui.editor.ResourceBundleEditor\"/>\\n<defaultEditor id\\=\"org.eclipse.ui.DefaultTextEditor\"/>\\n</info>\\n</editors>");
					//TXMPreferences.put("org.eclipse.ui.workbench", "editors", "");
					TXMPreferences.flush("org.eclipse.ui.workbench");
					cpb.done();
				}

			} catch (Exception e) {
				System.out.println("Fail to create translation project: "+e);
				e.printStackTrace();
			}
		} 
		
		return newProject;
	}
}
