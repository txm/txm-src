// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2013-07-25 16:24:13 +0200 (jeu. 25 juil. 2013) $
// $LastChangedRevision: 2490 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.rcp.translate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.txm.utils.io.IOUtils;

// TODO: Auto-generated Javadoc
/**
 * Tool to update the key of externalization files xxxx_fr.properties
 * adds _N/A to missing keys in 'fr' and 'ru' files
 * 
 * @author mdecorde
 * 
 */
public class ExternalizationFilesUpdater {
	
	static boolean debug = false; // activate the properties files content display
	
	/** The dirfiles to process containing "(messages|bundle)(_$lang)?.properties" files */
	LinkedList<File> dirfiles = new LinkedList<>();
	
	/** The propertyfiles. */
	HashMap<File, List<File>> propertyfiles = new HashMap<>();
	
	/** The fileentries: the messages keys */
	HashMap<File, List<String>> fileentries = new HashMap<>();
	
	/** The fileentriesvalues: the messages key<->value s. */
	HashMap<File, HashMap<String, String>> fileentriesvalues = new HashMap<>();
	
	/**
	 * Gets the entries.
	 *
	 * @param file the file
	 * @return the entries
	 * @throws IOException
	 */
	public static List<String> getEntries(File file) throws IOException {
		List<String> entries = new ArrayList<>();
		String encoding = "UTF-8";
		if (file.getName().startsWith("bundle")) { // TXM manages messages.properties encoded in UTF-8
			encoding = "iso-8859-1";
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));
		String line = reader.readLine();
		while (line != null) {
			String[] split = line.split("=", 2);
			if (line.length() > 0 && !line.startsWith("#") && split.length > 0) {
				entries.add(split[0].trim());
			}
			line = reader.readLine();
		}
		reader.close();
		if (debug) System.out.println(" ENTRIES " + file + " -> " + entries);
		return entries;
	}
	
	/**
	 * Gets the values.
	 *
	 * @param file the file
	 * @return the values
	 * @throws IOException
	 */
	public static HashMap<String, String> getValues(File file) throws IOException {
		HashMap<String, String> values = new HashMap<>();
		
		String encoding = "UTF-8";
		if (file.getName().startsWith("bundle")) { // TXM manages messages.properties encoded in UTF-8
			encoding = "iso-8859-1";
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));
		
		String line = reader.readLine();
		while (line != null) {
			String[] split = line.split("=", 2);
			if (line.length() > 0 && !line.startsWith("#") && split.length == 2) {
				values.put(split[0].trim(), split[1]);
			}
			line = reader.readLine();
		}
		reader.close();
		
		if (debug) System.out.println(" VALUES " + file + " -> " + values);
		return values;
	}
	
	/**
	 * Process found directories.
	 * 
	 * @throws IOException
	 */
	public void processFoundDirectories() throws IOException {
		for (File dirfile : dirfiles) {
			System.out.println("DIRECTORY : " + dirfile);
			List<File> files = propertyfiles.get(dirfile);
			for (File f : files) {
				this.fileentries.put(f, getEntries(f)); // ordered
				this.fileentriesvalues.put(f, getValues(f));
			}
			File reference = null;
			if (files.get(0).getName().startsWith("m")) {
				reference = new File(dirfile, "messages.properties");
			}
			else if (files.get(0).getName().startsWith("b")) {
				reference = new File(dirfile, "bundle.properties");
			}
			if (reference != null && reference.exists()) {
				for (File f : files) {
					if (!f.getName().equals(reference.getName())) {
						List<String> refentries = new ArrayList<>(this.fileentries.get(reference));
						List<String> tmp1 = new ArrayList<>(this.fileentries.get(f));
						tmp1.removeAll(refentries);
						
						refentries = this.fileentries.get(reference);
						List<String> tmp = this.fileentries.get(f);
						refentries.removeAll(tmp);
						for (String missing : refentries) {
							this.fileentriesvalues.get(f).put(missing, "N/A_" + this.fileentriesvalues.get(reference).get(missing)); // put entry's value
						}
						
						this.fileentries.put(f, this.fileentries.get(reference)); // update file entries
						
						if (tmp1.size() > 0 || refentries.size() > 0)
							System.out.println(" " + f.getName());
						if (tmp1.size() > 0)
							System.out.println("  Removed keys : " + tmp1);
						if (refentries.size() > 0)
							System.out.println("  Added keys : " + refentries);
					}
				}
			}
		}
	}
	
	/**
	 * Update files.
	 * 
	 * @throws IOException
	 */
	public void updateFiles() throws IOException {
		for (File dirfile : dirfiles) {
			List<File> files = propertyfiles.get(dirfile);
			for (File f : files) {
				
				List<String> entries = this.fileentries.get(f);
				if (debug) System.out.println(" U " + f + " <- " + entries);
				HashMap<String, String> values = this.fileentriesvalues.get(f);
				
				OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(f), "UTF-8");
				for (String entry : entries) {
					writer.write(entry + "=" + values.get(entry) + "\n");
				}
				writer.close();
			}
		}
	}
	
	/**
	 * Creates the missing files.
	 *
	 * @param lang the suffix
	 * @throws IOException
	 */
	public void createMissingFiles(String lang) throws IOException {
		System.out.println("Looking for missing messages files " + lang);
		for (File dirfile : dirfiles) {
			// println "DIRECTORY : "+dirfile
			File reference = null;
			String lookingname = "";
			List<File> files = propertyfiles.get(dirfile);
			if (files.get(0).getName().startsWith("m")) {
				reference = new File(dirfile, "messages.properties");
				lookingname = "messages_" + lang + ".properties";
			}
			else if (files.get(0).getName().startsWith("b")) {
				reference = new File(dirfile, "bundle.properties");
				lookingname = "bundle_" + lang + ".properties";
			}
			
			boolean create = true;
			if (reference != null && reference.exists()) {
				for (File f : files) {
					if (f.getName().equals(lookingname))
						create = false;
				}
			}
			
			if (create) {
				new File(dirfile, lookingname).createNewFile();
				System.out.println("Create file " + new File(dirfile, lookingname));
			}
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String rez = "";
		for (File dirfile : dirfiles) {
			rez += dirfile + "\n";
			for (File f : propertyfiles.get(dirfile)) {
				rez += "  " + f.getName() + "\n";
			}
		}
		return rez;
	}
	
	/**
	 * Scan directory.
	 *
	 * @param directory the directory
	 */
	public void scanDirectory(File directory) {
		if (!directory.exists()) {
			System.out.println("directory '" + directory + "' does not exists");
			return;
		}
		
		System.out.println("scan directory : " + directory.getAbsolutePath());
		LinkedList<File> files = new LinkedList<>();
		files.add(directory);
		
		while (!files.isEmpty()) {
			File current = files.removeFirst();
			if (current.isDirectory() && !current.getName().equals("bin")) {
				List<File> currentpfiles = new ArrayList<>();
				for (File sfile : current.listFiles(IOUtils.HIDDENFILE_FILTER)) {
					if (sfile.isDirectory())
						files.add(sfile);
					else if (sfile.getName().endsWith(".properties") && (sfile.getName().startsWith("messages") || sfile.getName().startsWith("bundle")))
						currentpfiles.add(sfile);
				}
				if (currentpfiles.size() > 0) {
					dirfiles.add(current);
					propertyfiles.put(current, currentpfiles);
				}
			}
		}
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws IOException
	 */
	public static void main(String[] args) throws Exception {
		String userdir = System.getProperty("user.home");
		File workspaceDir = new File(userdir, "workspace047/org.txm.annotation.urs.rcp");
		
		for (File project : workspaceDir.listFiles(IOUtils.HIDDENFILE_FILTER)) {
			if (!project.isDirectory()) continue;
			if (project.isHidden()) continue;
			// if (!project.getName().endsWith(".rcp")) continue;
			
			System.out.println("*** " + project + " ***");
			
			String[] langs = { "fr", "ru" };
			ExternalizationFilesUpdater scanner = new ExternalizationFilesUpdater();
			scanner.scanDirectory(project);
			for (String lang : langs) {
				scanner.createMissingFiles(lang);
			}
			scanner.processFoundDirectories();
			scanner.updateFiles();
		}
	}
}
