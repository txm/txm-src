package org.txm.texts.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.core.preferences.TXMPreferences;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.texts.core.functions.TextsView;
import org.txm.texts.rcp.editors.TextsViewEditor;

/**
 * Creates and opens a texts view editor.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ComputeTextsView extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkCorpusEngine()) {
			return false;
		}

		TextsView textsView = null;

		// From link: creating from parameter node
		String parametersNodePath = event.getParameter(TXMPreferences.RESULT_PARAMETERS_NODE_PATH);
		if (parametersNodePath != null && !parametersNodePath.isEmpty()) {
			textsView = new TextsView(parametersNodePath);
		}
		// From view result node
		else {
			Object selection = this.getCorporaViewSelectedObject(event);

			// Creating from Corpus
			if (selection instanceof CQPCorpus) {
				CQPCorpus corpus = (CQPCorpus) selection;
				textsView = corpus.getFirstChild(TextsView.class);
				if (textsView == null) {
					textsView = new TextsView(corpus);
				}
			}
			// Reopening existing result
			else if (selection instanceof TextsView) {
				textsView = (TextsView) selection;
			}
			// Error
			else {
				return this.logCanNotExecuteCommand(selection);
			}
		}

		openEditor(textsView);
		return null;
	}

	/**
	 * Opens an editor for the specified result.
	 * 
	 * @param view
	 * @return
	 */
	public static TXMEditor<TextsView> openEditor(TextsView view) {
		return TXMEditor.openEditor(view, TextsViewEditor.class.getName());
	}

}
