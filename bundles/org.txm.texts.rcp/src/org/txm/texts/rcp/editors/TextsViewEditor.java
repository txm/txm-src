package org.txm.texts.rcp.editors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableColumn;
import org.txm.Toolbox;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.Parameter;
import org.txm.edition.rcp.editors.SynopticEditionEditor;
import org.txm.edition.rcp.handlers.OpenEdition;
import org.txm.edition.rcp.preferences.SynopticEditionPreferences;
import org.txm.objects.Text;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TableKeyListener;
import org.txm.rcp.editors.TableLinesViewerComparator;
import org.txm.rcp.editors.TableResultEditor;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.HashMapWidget;
import org.txm.rcp.swt.widget.PropertiesSelector;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.SubcorpusCQLQuery;
import org.txm.texts.core.functions.TextsView;
import org.txm.texts.core.preferences.TextsViewPreferences;
import org.txm.texts.rcp.messages.TextsViewUIMessages;
import org.txm.utils.logger.Log;

public class TextsViewEditor extends TXMEditor<TextsView> implements TableResultEditor {

	@Parameter(key = TextsViewPreferences.COLUMNS)
	PropertiesSelector<StructuralUnitProperty> propertiesSelector;

	//@Parameter(key = TextsViewPreferences.USE_TOKENS_IN_STATS)
	Combo statsWithTokensButton;

	@Parameter(key = TextsViewPreferences.NEGATIVE_FILTERS)
	HashMapWidget negativeFiltersText;
	
	@Parameter(key = TextsViewPreferences.POSITIVE_FILTERS)
	HashMapWidget positiveFiltersText;

	@Parameter(key = TextsViewPreferences.SORT_BY_SIZE)
	Button sortButton;

	TableViewer viewer;

	private TableLinesViewerComparator viewerComparator;

	private StatsPanel statsPanel;

	private SelectionPanel selectionPanel;

	private TableKeyListener tableKeyListener;

	private ScrolledComposite sc1;

	private GLComposite left;

	private ScrolledComposite sc2;

	private GLComposite right;

	@Override
	public void _createPartControl() throws Exception {

		this.getParent().setLayout(new GridLayout(1, true));

		this.getMainParametersComposite().getLayout().numColumns = 10;

		ArrayList<StructuralUnitProperty> properties = new ArrayList<>(getResult().getCorpus().getStructuralUnitProperties("text")); //$NON-NLS-1$
		for (int i = 0; i < properties.size(); i++) {
			String name = properties.get(i).getName();
			if (name.equals("id") || name.equals("project") || name.equals("base")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				properties.remove(i--);
			}
		}
		Collections.sort(properties);
		
		ComputeSelectionListener computeSelectionListener = new ComputeSelectionListener(this, true);
		
		Label l = new Label(this.getMainParametersComposite(), SWT.NONE);
		l.setText(TextsViewUIMessages.balanceBy);
		
		statsWithTokensButton = new Combo(this.getMainParametersComposite(), SWT.READ_ONLY|SWT.SINGLE);
		statsWithTokensButton.setItems(TextsViewUIMessages.tokens, TextsViewUIMessages.texts);
		//statsWithTokensButton.addSelectionListener(computeSelectionListener);
		statsWithTokensButton.setToolTipText(TextsViewUIMessages.balanceOfTheSelectionIsCalculatedOnTheNumberOfWordsTokensOrtexts);
		
		sortButton = new Button(this.getMainParametersComposite(), SWT.TOGGLE);
		sortButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_HISTO));
		sortButton.setToolTipText("Sort by size");
		//sortButton.addSelectionListener(computeSelectionListener);
		
		Button subcorpusButton = new Button(this.getMainParametersComposite(), SWT.PUSH);
		subcorpusButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_SUBCORPUS));
		subcorpusButton.setToolTipText(TextsViewUIMessages.tooltip_createASubcorpusOfTheShownTexts);
		subcorpusButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				try {
					ArrayList<String> selectedValues = new ArrayList<String>();
					for (Object o : viewer.getStructuredSelection().toList()) {
						Text l = (Text) o;
						selectedValues.add(l.getName());
					}

					if (selectedValues.size() == 0) {
						Log.info(TextsViewUIMessages.info_noLineSelectedDotAborting);
						return;
					}

					InputDialog dialog = new InputDialog(getShell(), TXMCoreMessages.common_subcorpusName, TextsViewUIMessages.setTheSubcorpusNameToUse, TXMCoreMessages.common_subcorpus, null);
					
					if (dialog.open() == InputDialog.CANCEL) {
						return;
					}
					String name = dialog.getValue().trim();
					if (name.length() == 0) {
						name = TXMCoreMessages.common_unnamed;
					}
					Subcorpus subcorpus = new Subcorpus(getResult().getCorpus());
					subcorpus.setUserName(name);

					String regexp = ""; //$NON-NLS-1$
					for (String v : selectedValues) {
						regexp += CQLQuery.addBackSlash(v) + "|"; //$NON-NLS-1$
					}
					regexp = regexp.substring(0, regexp.length() - 1);

					StructuralUnit su = getResult().getCorpus().getTextStructuralUnit();

					StructuralUnitProperty sup = su.getProperty("id"); //$NON-NLS-1$
					SubcorpusCQLQuery query = new SubcorpusCQLQuery(su, sup, regexp, selectedValues);

					subcorpus.setQuery(query);

					subcorpus.compute();

					CorporaView.refreshObject(subcorpus);
				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		
		if (properties.size() > 0) {
			propertiesSelector = new PropertiesSelector<>(this.getExtendedParametersGroup(), TXMCoreMessages.common_metadataColon);
			propertiesSelector.setProperties(properties);
			propertiesSelector.addSelectionListener(computeSelectionListener);
			propertiesSelector.setShowSimpleNames();

			Button deleteButton = new Button(this.getExtendedParametersGroup(), SWT.PUSH);
			deleteButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_DELETE));
			deleteButton.setToolTipText(TextsViewUIMessages.tooltip_removeSelectedTextsFromTheView);
			deleteButton.addSelectionListener(new ComputeSelectionListener(this, true) {
				@Override
				public void widgetSelected(SelectionEvent e) {
					deleteSelection();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			LinkedHashMap<String, List<String>> availableValues = new LinkedHashMap<>();
			for (StructuralUnitProperty k : getResult().getColumns()) {
				availableValues.put(k.getName(), k.getOrderedValues());
			}

			new Label(this.getExtendedParametersGroup(), SWT.NONE).setText(TXMCoreMessages.common_filtersColon);
			
			positiveFiltersText = new HashMapWidget(this.getExtendedParametersGroup(), SWT.NONE, "+", availableValues);
			positiveFiltersText.addSelectionListener(computeSelectionListener);
			LinkedHashMap<String, String> defaultValues = new LinkedHashMap<String, String>();
			for (StructuralUnitProperty sup : properties) {
				if (sup.getStructuralUnit().getName().equals("txmcorpus")) {  //$NON-NLS-1$
					continue;
				}

				defaultValues.put(sup.getName(), ""); //$NON-NLS-1$
			}
			positiveFiltersText.setDefaultValues(defaultValues);
			
			negativeFiltersText = new HashMapWidget(this.getExtendedParametersGroup(), SWT.NONE, "-", availableValues);
			negativeFiltersText.addSelectionListener(computeSelectionListener);
			for (StructuralUnitProperty sup : properties) {
				if (sup.getStructuralUnit().getName().equals("txmcorpus")) {  //$NON-NLS-1$
					continue;
				}

				defaultValues.put(sup.getName(), ""); //$NON-NLS-1$
			}
			negativeFiltersText.setDefaultValues(defaultValues);
		}

		

//		Button partitionButton = new Button(this.getMainParametersComposite(), SWT.PUSH);
//
//		partitionButton.setImage(IImageKeys.getImage("platform:/plugin/org.txm.searchengine.cqp.rcp/icons/functions/partition.png"));
//		partitionButton.setToolTipText("Create a partition of the shown texts");
//		partitionButton.addSelectionListener(new SelectionListener() {
//
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				try {
//
//					ArrayList<String> selectedValues = new ArrayList<String>();
//					for (Object o : viewer.getStructuredSelection().toList()) {
//						Text l = (Text) o;
//						selectedValues.add(l.getName());
//					}
//
//					if (selectedValues.size() == 0) {
//						Log.info("No line selected. Aborting.");
//						return;
//					}
//
//					InputDialog dialog = new InputDialog(getShell(), "Partition name", "Set the partition name to use", "partition", null);
//					if (dialog.open() == InputDialog.CANCEL) {
//						Log.warning("Operation canceled by user.");
//						return;
//					}
//					String name = dialog.getValue().trim();
//					if (name.length() == 0) {
//						Log.warning("Operation canceled: no name provided.");
//						return;
//					}
//
//					StructuredPartition partition = new StructuredPartition(getResult().getCorpus());
//
//					StructuralUnit su = getResult().getCorpus().getStructuralUnit("text");
//					StructuralUnitProperty sup = su.getProperty("id");
//
//					partition.setParameters(name, sup, selectedValues);
//					partition.compute();
//					CorporaView.refreshObject(partition);
//				}
//				catch (Exception e1) {
//					e1.printStackTrace();
//				}
//			}
//
//			@Override
//			public void widgetDefaultSelected(SelectionEvent e) {
//			}
//		});

		SashForm sashForm = new SashForm(this.getResultArea(), SWT.HORIZONTAL);
		sashForm.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		// Selection left panel
		sc1 = new ScrolledComposite(sashForm, SWT.BORDER | SWT.V_SCROLL);
		sc1.setExpandHorizontal(true);
		sc1.setExpandVertical(true);
		
		left = new GLComposite(sc1, SWT.NONE);
		new Label(left, SWT.None).setText(TextsViewUIMessages.selectionByMetadata);
		selectionPanel = new SelectionPanel(left, SWT.BORDER, this);
		selectionPanel.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		sc1.setContent(left);
//		sc1.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		left.addListener(SWT.Resize, event -> {
			sc1.setMinSize(left.computeSize(sc1.getParent().getSize().x, SWT.DEFAULT));
		});
		
		
		// Texts list middle panel
		GLComposite middle = new GLComposite(sashForm, SWT.BORDER);

		new Label(middle, SWT.None).setText(TextsViewUIMessages.selectionByText);

		viewer = new TableViewer(middle, SWT.MULTI | SWT.VIRTUAL | SWT.FULL_SELECTION);
		viewer.getTable().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		viewer.getTable().setHeaderVisible(true);
		viewer.getTable().setLinesVisible(true);
		tableKeyListener = TableUtils.installCopyAndSearchKeyEvents(viewer);
		tableKeyListener.installSearchGroup(this);
		tableKeyListener.installMenuContributions(this);
		TableUtils.installColumnResizeMouseWheelEvents(viewer);
		TableUtils.installTooltipsOnSelectionChangedEvent(viewer);

		viewer.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				openEditionOfSelectedLine();
			}
		});

		viewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {

				statsPanel.selectionUpdated();
			}
		});

		viewer.getTable().addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {

				if (e.keyCode == SWT.DEL) {
					deleteSelection();
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		viewer.setContentProvider(new IStructuredContentProvider() {

			@SuppressWarnings("rawtypes")
			@Override
			public Object[] getElements(Object inputElement) {

				if (inputElement instanceof List) {
					return ((List) inputElement).toArray();
				}

				return null;
			}
		});

		
		// stats data right panel
		sc2 = new ScrolledComposite(sashForm, SWT.BORDER | SWT.V_SCROLL);
		sc2.setExpandHorizontal(true);
		sc2.setExpandVertical(true);

		right = new GLComposite(sc2, SWT.NONE, "left"); //$NON-NLS-1$
		new Label(right, SWT.None).setText(TextsViewUIMessages.balance);
		statsPanel = new StatsPanel(right, SWT.BORDER, this);
		statsPanel.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		
		sc2.setContent(right);
//		sc1.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		right.addListener(SWT.Resize, event -> {
			sc2.setMinSize(right.computeSize(sc2.getParent().getSize().x, SWT.DEFAULT));
		});
		
		sashForm.setWeights(20, 70, 30);
	}

	/**
	 * 
	 */
	protected void openEditionOfSelectedLine() {
		
		ISelection sel = viewer.getSelection();
		StructuredSelection ssel = (StructuredSelection) sel;
		Object o = ssel.getFirstElement();
		if (o != null && o instanceof Text) {
			SynopticEditionEditor editor = OpenEdition.openEdition(getResult().getCorpus(), new String[] { getResult().getProject().getDefaultEditionName() }, ((Text) o).getName());
			int position = SynopticEditionPreferences.getInstance().getInt(SynopticEditionPreferences.BACKTOTEXT_POSITION);
			if (position == -2) {
				Point s = editor.getParent().getSize();
				if (s.x < s.y) {
					position = EModelService.ABOVE;
				}
				else {
					position = EModelService.RIGHT_OF;
				}
			}
			if (editor != null && position >= 0) {
				SWTEditorsUtils.moveEditor(editor, TextsViewEditor.this, position);
			}
		}
	}

	/**
	 * 
	 */
	protected void deleteSelection() {

		try {
			if (viewer.getSelection().isEmpty()) {
				return;
			}

			TextsView result = getResult();
			for (Object o : viewer.getStructuredSelection().toList()) {
				Text t = (Text) o;
				result.getLines().remove(t.getName());
			}
			result.setAltered();
			result.setDirty();
			updateEditorFromResult(false);
		}
		catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	@Override
	public void updateResultFromEditor() {
		
		TextsView tv = getResult();
		tv.setIsBalanceWithTokens(this.statsWithTokensButton.getSelectionIndex() == 0); // the first item is tokens
	}

	@Override
	public void updateEditorFromResult(boolean update) throws Exception {

		TextsView tv = getResult();
		int currentDisplayedColumns = viewer.getTable().getColumns().length;
		int numberOfColumnsToDisplay = (tv.getColumns().size() + 3);

		List previousSelection = viewer.getStructuredSelection().toList();
		
		this.statsWithTokensButton.select(tv.isBalanceWithTokens()?0:1);  // the first item is tokens

		if (currentDisplayedColumns == 0 || currentDisplayedColumns != numberOfColumnsToDisplay) {

			if (viewer.getTable().getColumns().length > 0) { // get rid of current columns
				for (TableColumn column : viewer.getTable().getColumns()) {
					column.dispose();
				}
			}

			TableViewerColumn c = new TableViewerColumn(viewer, SWT.NONE);
			TableColumn selectionColumn = c.getColumn();
			selectionColumn.setText(" ");
			//selectionColumn.setToolTipText("is this text selected");
			selectionColumn.setWidth(0);
			c.setLabelProvider(new ColumnLabelProvider() {

				@Override
				public String getText(Object element) {
					return "";
				}

				@Override
				public Image getImage(Object element) {
					return null;
				}
			});

			c = new TableViewerColumn(viewer, SWT.NONE);
			TableColumn textColumn = c.getColumn();
			textColumn.setText(TextsViewUIMessages.common_text);
			textColumn.setToolTipText(TextsViewUIMessages.common_text);
			textColumn.setWidth(200);
			c.setLabelProvider(new ColumnLabelProvider() {

				@Override
				public String getText(Object element) {
					Text text = (Text) element;
					return text.getName();
				}
			});

			c = new TableViewerColumn(viewer, SWT.NONE);
			TableColumn editionColumn = c.getColumn();
			editionColumn.setText("  ");
			editionColumn.setToolTipText(TextsViewUIMessages.common_edition);
			editionColumn.setAlignment(SWT.CENTER);
			editionColumn.setWidth(30);
			c.setLabelProvider(new ColumnLabelProvider() {

				@Override
				public Image getImage(Object element) {
					Text text = (Text) element;
					TextsViewEditor.this.getResult().getLines().get(text.getName());
					if (text.getEditions().size() > 0) {
						return IImageKeys.getImage(IImageKeys.EDITION);
					}
					else {
						return null;
					}
				}

				@Override
				public String getText(Object element) {
					return null;
				}
			});

			TableUtils.addLineListener(viewer, editionColumn, new MouseAdapter() {
				@Override
				public void mouseUp(MouseEvent e) {
					openEditionOfSelectedLine();
				}
			});

			c = new TableViewerColumn(viewer, SWT.NONE);
			TableColumn separatorColumn = c.getColumn();
			separatorColumn.setText(""); //$NON-NLS-1$
			separatorColumn.setWidth(2);
			c.getColumn().setResizable(false);
			c.setLabelProvider(new ColumnLabelProvider() {

				@Override
				public String getText(Object element) {
					return "";
				}
			});
			//			viewerComparator.addSelectionAdapter(viewer, separatorColumn, 3);

			int nColumn = 0;
			for (StructuralUnitProperty col : this.getResult().getColumns()) {
				c = new TableViewerColumn(viewer, SWT.NONE);
				TableColumn newColumn = c.getColumn();
				newColumn.setText(col.getName());
				newColumn.setData(nColumn);
				newColumn.setToolTipText(col.getFullName());
				newColumn.setWidth(100);
				//				viewerComparator.addSelectionAdapter(viewer, newColumn, 4+nColumn);
				c.setLabelProvider(new TextsViewColumnLabelProvider(this, nColumn));
				nColumn++;
			}

			// must be done after installing the columns
			viewerComparator = new TableLinesViewerComparator(Toolbox.getCollator(this.getResult())) {

				@Override
				public int compare(Viewer viewer, Object e1, Object e2) {

					Text t1 = (Text) e1;
					Text t2 = (Text) e2;
					if (lastColumnIndex == 0) {
						return super.compare(viewer, t1.getName(), t2.getName()); // TODO
					}
					else if (lastColumnIndex == 1) {
						return super.compare(viewer, t1.getName(), t2.getName());
					}
					else if (lastColumnIndex == 2) {
						return super.compare(viewer, t1.getEditions().size(), t2.getEditions().size());
					}
					else if (lastColumnIndex == 3) {
						return 0;
					}
					else { // get the Text info to compare
						return super.compare(viewer,
								getResult().getLines().get(t1.getName()).get(lastColumnIndex - 4),
								getResult().getLines().get(t2.getName()).get(lastColumnIndex - 4));
					}
				}
			};
			viewer.setComparator(viewerComparator);
			viewerComparator.addSelectionAdapters(viewer); // must be done after the columns are added
			
			statsPanel.selectionUpdated();
		}
		//
		ArrayList<Text> texts = new ArrayList<>();
		for (Text text : getResult().getCorpus().getProject().getTexts()) {
			if (this.getResult().getLines().containsKey(text.getName())) {
				texts.add(text);
			}
		}
		viewer.setInput(texts);

		if (previousSelection != null) {
			viewer.setSelection(new StructuredSelection(previousSelection));
		}

		selectionPanel.updateFromResult(); // reset
		statsPanel.updateFromResult(); // reset
		
		sc1.setMinSize(left.computeSize(sc1.getParent().getSize().x, SWT.DEFAULT));
		sc2.setMinSize(right.computeSize(sc2.getParent().getSize().x, SWT.DEFAULT));
	}

	@SuppressWarnings("unchecked")
	public List<Text> getSelectedTexts() {
		return viewer.getStructuredSelection().toList();
	}

	/**
	 * 
	 */
	public void updateSelection() {

		ArrayList<Text> newSelection = new ArrayList<Text>();
		ArrayList<Text> texts = (ArrayList<Text>) viewer.getInput();
		LinkedHashMap<String, ArrayList<String>> lines = getResult().getLines();
		
		for (Text t : texts) {
			
			// int icolumn = getResult().getColumns().indexOf(col);
			boolean allPropertiesMatch = true;
			for (int icolumn = 0 ; icolumn < getResult().getColumns().size() ; icolumn++) {
				TableViewer tv = selectionPanel.itemViewers.get(icolumn);
				List selected = tv.getStructuredSelection().toList();
				if (selected.size() > 0) {
					allPropertiesMatch &= selected.contains(lines.get(t.getName()).get(icolumn));
				}
			}
			
			if (allPropertiesMatch) {
				newSelection.add(t);
			}
		}
		StructuredSelection sel = new StructuredSelection(newSelection);
		viewer.setSelection(sel);
	}

	@Override
	public TableViewer[] getTableViewers() {
		return new TableViewer[] { viewer };
	}

	@Override
	public TableKeyListener getTableKeyListener() {
		return tableKeyListener;
	}
}
