package org.txm.texts.rcp.editors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableItem;
import org.txm.objects.Text;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.swt.provider.SimpleLabelProvider;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;

/**
 * Shows stats depending on the editor main viewer selection.
 * 
 * @author mdecorde
 *
 */
public class StatsPanel extends LateralPanel {

	LinkedHashMap<StructuralUnitProperty, LinkedHashMap<String, Integer>> counts = new LinkedHashMap<>();

	LinkedHashMap<StructuralUnitProperty, Integer> totals = new LinkedHashMap<>();

	LinkedHashMap<StructuralUnitProperty, HashSet<String>> selectedValues = new LinkedHashMap<>();

	/**
	 * 
	 * @param parent
	 * @param style
	 * @param editor
	 */
	public StatsPanel(Composite parent, int style, TextsViewEditor editor) {

		super(parent, style, editor);
	}

	/**
	 * 
	 */
	public void selectionUpdated() {

		totals.clear();
		counts.clear();
		List<Text> selection = editor.getSelectedTexts();
		LinkedHashMap<String, Integer> textcounts = editor.getResult().getCounts();
		LinkedHashMap<String, ArrayList<String>> lines = editor.getResult().getLines();
		selectedValues.clear();
		List<StructuralUnitProperty> columns = editor.getResult().getColumns();
		for (int i = 0; i < columns.size(); i++) {

			if (itemViewers.size() <= i) break;

			StructuralUnitProperty col = columns.get(i);
			HashSet<String> selected = new HashSet<String>();
			selectedValues.put(col, selected);

			totals.put(col, 0);

			List<String> input = (List) itemViewers.get(i).getInput();
			LinkedHashMap<String, Integer> colcounts = new LinkedHashMap<String, Integer>();
			counts.put(col, colcounts);
			for (String value : input) {
				colcounts.put(value, 0);
			}

			for (Text t : selection) {
				String value = lines.get(t.getName()).get(i); // the value to select in the stats panel
				if (colcounts.containsKey(value)) {
					selected.add(value);
					int amount = textcounts.get(t.getName());
					colcounts.put(value, colcounts.get(value) + amount);
					totals.put(col, totals.get(col) + amount);
				}
			}

			if (editor.getResult().sortBySize()) {
				Collections.sort(input, new Comparator<String>() {

					@Override
					public int compare(String o1, String o2) {
						return colcounts.get(o2) - colcounts.get(o1);
					}
				});
			}

			// update header stats data
			itemViewers.get(i).getTable().getColumn(0).setText(col.getName() + ": " + selected.size() + "/" + input.size() + " T=" + totals.get(col));

			// refresh and pack the columns
			TableUtils.packColumns(this.itemViewers.get(i));
			this.itemViewers.get(i).getTable().deselectAll();
			
			// show the first value with count > 0
			for (String ii : input) {
				if (colcounts.get(ii) > 0) {
					itemViewers.get(i).reveal(ii);
					break;
				}
			}
			
		}
	}

	/**
	 * Fills the columns, events, etc.
	 * 
	 * @param itemViewer
	 * @param col
	 * @throws CqiClientException
	 */
	@Override
	protected void update(TableViewer itemViewer, StructuralUnitProperty col) throws CqiClientException {

		TableViewerColumn statcolumn = new TableViewerColumn(itemViewer, SWT.NONE);
		statcolumn.getColumn().setText(col.getName());
		statcolumn.getColumn().setWidth(100);
		// To draw the bar plot column
		itemViewer.getTable().addListener(SWT.EraseItem, new Listener() {

			@Override
			public void handleEvent(Event event) {

				if (event.index == 0) {
					if (selectedValues.size() == 0) return; // no selection yet

					TableItem item = (TableItem) event.item;
					String value = item.getText();

					if (selectedValues.get(col) == null) return;
					if (!selectedValues.get(col).contains(value)) return;
					if (totals.get(col) == 0) return;

					int p = event.width * (100 * counts.get(col).get(value)) / (100 * totals.get(col));

					//event.gc.setBackground(itemViewer.getTable().getDisplay().getSystemColor(SWT.COLOR_GRAY));
					event.gc.setBackground(itemViewer.getTable().getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));
					event.gc.fillRectangle(event.x, event.y + 1, p, event.height - 1);
				}
			}
		});

		itemViewer.getTable().pack();
		itemViewer.setContentProvider(new ArrayContentProvider());
		itemViewer.setLabelProvider(new SimpleLabelProvider());
		itemViewer.setInput(new ArrayList(editor.getResult().getColValues(col)));
	}

}
