package org.txm.texts.rcp.editors;

import java.util.LinkedHashMap;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.txm.rcp.swt.provider.SimpleLabelProvider;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;

/**
 * Update the selection of the editor main viewer -> StatsPanel is then updated too
 * 
 * @author mdecorde
 *
 */
public class SelectionPanel extends LateralPanel {

	public SelectionPanel(Composite parent, int style, TextsViewEditor editor) {

		super(parent, style, editor);
	}

	/**
	 * Fill the column, events, etc.
	 * 
	 * @param itemViewer
	 * @param col
	 * @throws CqiClientException
	 */
	protected void update(TableViewer itemViewer, StructuralUnitProperty col) throws CqiClientException {

		LinkedHashMap<String, Integer> counts = editor.getResult().getCounts();

		TableViewerColumn selectionColumn = new TableViewerColumn(itemViewer, SWT.NONE);
		selectionColumn.getColumn().setText("");
		selectionColumn.getColumn().setWidth(10);

		TableViewerColumn statcolumn = new TableViewerColumn(itemViewer, SWT.NONE);
		statcolumn.getColumn().setText(col.getName());
		statcolumn.getColumn().setWidth(100);

		itemViewer.getTable().pack();
		itemViewer.setContentProvider(new ArrayContentProvider());
		itemViewer.setLabelProvider(new SimpleLabelProvider() {

			/**
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
			 * 
			 * @param element
			 * @param columnIndex
			 */
			@Override
			public Image getColumnImage(Object element, int columnIndex) {
				if (columnIndex == 0) {
					return null;
				}
				else {
					return null;
				}
			}

			@Override
			public String getColumnText(Object element, int columnIndex) {
				if (columnIndex == 0) {
					return "";
				}
				else {
					return "" + element; //$NON-NLS-1$
				}

			}
		});
		itemViewer.setInput(editor.getResult().getColValues(col));

		itemViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {

				editor.updateSelection();
			}
		});
	}
}
