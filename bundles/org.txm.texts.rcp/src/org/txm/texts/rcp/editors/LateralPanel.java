package org.txm.texts.rcp.editors;

import java.util.ArrayList;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.txm.rcp.swt.GLComposite;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;

public abstract class LateralPanel extends GLComposite {

	TextsViewEditor editor;

	protected ArrayList<TableViewer> itemViewers = new ArrayList<TableViewer>();

	
	/**
	 * Creates a lateral panel.
	 * @param parent
	 * @param style
	 * @param editor
	 */
	public LateralPanel(Composite parent, int style, TextsViewEditor editor) {
		super(parent, style);
		this.editor = editor;
	}
	 
	/**
	 * Fill the column, events, etc.
	 * 
	 * @param itemViewer
	 * @param col
	 * @throws CqiClientException
	 */
	protected abstract void update(TableViewer itemViewer, StructuralUnitProperty col) throws CqiClientException;

	
	/**
	 * Updates the tables according to the result data.
	 */
	public void updateFromResult() {

		for (Control c : this.getChildren()) {
			c.dispose(); // clear
		}
		itemViewers.clear();

		for (StructuralUnitProperty col : editor.getResult().getColumns()) {

			TableViewer itemViewer = new TableViewer(this, SWT.MULTI | SWT.VIRTUAL | SWT.FULL_SELECTION | SWT.BORDER);
			itemViewer.getTable().setHeaderVisible(true);
			itemViewer.getTable().setLinesVisible(true);
			GridData gdata = new GridData(GridData.FILL, GridData.FILL, true, false);
			itemViewer.getTable().setLayoutData(gdata);
			gdata.heightHint = 250;
			itemViewers.add(itemViewer);

			try {
				update(itemViewer, col);
			}
			catch (CqiClientException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		this.layout();
	}

	@Override
	public void checkSubclass() {
		// allow subclassing
	}
}
