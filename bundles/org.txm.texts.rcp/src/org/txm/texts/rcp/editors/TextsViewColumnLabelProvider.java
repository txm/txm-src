package org.txm.texts.rcp.editors;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.txm.objects.Text;


public class TextsViewColumnLabelProvider extends ColumnLabelProvider {

	int n;

	TextsViewEditor editor;

	public TextsViewColumnLabelProvider(TextsViewEditor editor, int n) {
		this.n = n;
		this.editor = editor;
	}

	@Override
	public String getText(Object element) {

		try {
			Text text = (Text) element;
			return editor.getResult()
					.getLines()
					.get(text.getName())
					.get(n);
		}
		catch (Exception e) {
		}
		return ""; //$NON-NLS-1$
	}
}
