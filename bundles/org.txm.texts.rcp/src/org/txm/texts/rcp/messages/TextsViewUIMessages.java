package org.txm.texts.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;


/**
 * Texts view UI Messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class TextsViewUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.texts.rcp.messages.messages"; //$NON-NLS-1$


	public static String common_text;
	public static String common_edition;
	public static String balanceBy;
	public static String selectionByMetadata;
	public static String selectionByText;
	public static String info_noLineSelectedDotAborting;
	public static String useTokensInStats;
	public static String tokens;
	public static String texts;
	public static String balanceOfTheSelectionIsCalculatedOnTheNumberOfWordsTokensOrtexts;
	public static String balance;
	public static String tooltip_createASubcorpusOfTheShownTexts;
	public static String tooltip_removeSelectedTextsFromTheView;


	public static String setTheSubcorpusNameToUse;




	
	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, TextsViewUIMessages.class);
	}


}
