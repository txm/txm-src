// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.texts.rcp.adapters;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.FrameworkUtil;
import org.txm.rcp.adapters.TXMResultAdapter;
import org.txm.rcp.adapters.TXMResultAdapterFactory;
import org.txm.texts.core.functions.TextsView;


/**
 * A factory for creating Adapter objects.
 * 
 * @author mdecorde
 */
public class TextsViewAdapterFactory extends TXMResultAdapterFactory {


	public static final ImageDescriptor ICON = AbstractUIPlugin.imageDescriptorFromPlugin(FrameworkUtil.getBundle(TextsViewAdapterFactory.class).getSymbolicName(),
			"platform:/plugin/org.txm.rcp/icons/objects/books.png"); //$NON-NLS-1$

	@Override
	public <T> T getAdapter(Object adaptableObject, Class<T> adapterType) {
		if (this.canAdapt(adapterType) && adaptableObject instanceof TextsView) {
			return adapterType.cast(new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return ICON;
				}
			});
		}
		return null;
	}
}
