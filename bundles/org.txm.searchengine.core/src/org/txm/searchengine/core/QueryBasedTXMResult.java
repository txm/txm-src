package org.txm.searchengine.core;

import org.txm.core.results.TXMResult;

/**
 * abstract class of result built on query
 * 
 * @author mdecorde
 *
 */
public abstract class QueryBasedTXMResult extends TXMResult {


	public QueryBasedTXMResult(String parametersNodePath) {

		super(parametersNodePath);
	}

	public QueryBasedTXMResult(String parametersNodePath, TXMResult parent) {

		super(parametersNodePath, parent);
	}

	public QueryBasedTXMResult(TXMResult parent) {

		super(parent);
	}

	/**
	 * 
	 * @return a query based on the result parameters
	 */
	public abstract IQuery getQuery();
}
