package org.txm.searchengine.core;

import java.util.HashMap;
import java.util.Set;

/**
 * Represent a search engine query
 * 
 * @author mdecorde
 *
 */
public interface IQuery extends Comparable<IQuery> {

	public String getQueryString();

	public boolean isEmpty();

	/**
	 * 
	 * @return the Query SearchEngine that can resolve the query
	 */
	public SearchEngine getSearchEngine();

	/**
	 * update the query
	 * 
	 * @param rawQuery
	 * @return this
	 */
	public IQuery setQuery(String rawQuery);

	/**
	 * update the query name
	 * 
	 * @return this
	 */
	public String getName();

	/**
	 * update the query name for frienddier access
	 * 
	 * @param name
	 * @return this
	 */
	public IQuery setName(String name);

	/**
	 * Can be use to set a environment variable before doding something
	 * 
	 * @param option
	 */
	public void setOption(String name, String value);

	public String getOption(String name);

	/**
	 * 
	 * @return the set options. NOT NULL
	 */
	public HashMap<String, String> getOptions();

	public String removeOption(String name);

	public Set<String> getOptionNames();

	/**
	 * fix the query using sugar syntax rules
	 * 
	 * @return this
	 */
	public IQuery fixQuery(String lang);

	public boolean equals(IQuery q);

	public String asString();

	public boolean canBeMultiLine();

	//	public static String toPrefString(IQuery query) {
	//		return query.getSearchEngine().getName()+"\t"+query.getQueryString(); //$NON-NLS-1$
	//	}
	//	
	//	public static IQuery fromPrefString(String prefString) {
	//		
	//		String[] split = prefString.split("\t", 2); //$NON-NLS-1$
	//		
	//		String engineName = null;
	//		String queryString = null;
	//		
	//		if (split.length == 2) {
	//			engineName = split[0];
	//			queryString = split[1];
	//		} else {
	//			engineName = "CQP"; //$NON-NLS-1$
	//			queryString = split[0];
	//		}
	//		
	//		SearchEngine se = SearchEnginesManager.getSearchEngine(engineName);
	//		if (se == null) {
	//			Log.severe(NLS.bind(SearchEngineCoreMessages.NoSearchengineP0Found, engineName));
	//			return null;
	//		}
	//		IQuery query = se.newQuery();
	//		query.setQuery(queryString);
	//		return query;
	//	}
}
