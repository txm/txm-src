package org.txm.searchengine.core;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;


public class SearchEnginePreferences extends TXMPreferences {

	/**
	 * contains the last bundle version setting the TreeTagger models directory
	 */
	public static final String DEFAULT_SEARCH_ENGINE = "default_search_engine"; //$NON-NLS-1$

	/**
	 * if set, the available search engines are shown in TXM interfaces
	 */
	public static final String SHOW_SEARCH_ENGINES = "show_search_engines"; //$NON-NLS-1$

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(SearchEnginePreferences.class)) {
			new SearchEnginePreferences();
		}
		return TXMPreferences.instances.get(SearchEnginePreferences.class);
	}

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		// Default preferences if no org.txm.udpipe.core fragment is found
		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.put(DEFAULT_SEARCH_ENGINE, "CQP"); //$NON-NLS-1$
		preferences.putBoolean(SHOW_SEARCH_ENGINES, true);
	}
}
