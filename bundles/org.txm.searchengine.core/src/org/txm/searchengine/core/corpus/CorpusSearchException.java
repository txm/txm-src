package org.txm.searchengine.core.corpus;

public class CorpusSearchException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3518861723206508596L;

	public CorpusSearchException(String message) {
		super(message);
	}

	public CorpusSearchException(String message, Throwable cause) {
		super(message, cause);
	}
}
