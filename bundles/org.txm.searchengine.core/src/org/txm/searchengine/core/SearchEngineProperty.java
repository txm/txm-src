package org.txm.searchengine.core;

/**
 * A SearchEngine queryable property for match positions
 * 
 * @author mdecorde
 *
 */
public interface SearchEngineProperty {

	/**
	 * 
	 * @return the property uniq name in the queried corpus
	 */
	public String getName();

	/**
	 * 
	 * @return the property uniq name in the queried corpus
	 */
	public String getFullName();

}
