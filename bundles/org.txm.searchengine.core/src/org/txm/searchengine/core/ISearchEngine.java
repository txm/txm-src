package org.txm.searchengine.core;

import org.txm.core.engines.Engine;
import org.txm.objects.CorpusBuild;

public interface ISearchEngine extends Engine {

	public Selection query(CorpusBuild corpus, IQuery query, String name, boolean saveQuery) throws Exception;

	/**
	 * 
	 * @return an empty Query for the engine implementation
	 */
	public IQuery newQuery();

	/**
	 * 
	 * @param corpus
	 * @return true if the Engine can query the corpus
	 */
	public abstract boolean hasIndexes(CorpusBuild corpus);

	public String getDetails();
}
