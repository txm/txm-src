// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-05-13 09:46:56 +0200 (Wed, 13 May 2015) $
// $LastChangedRevision: 2967 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.searchengine.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.eclipse.osgi.util.NLS;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.txm.core.preferences.TXMPreferences;
import org.txm.searchengine.core.messages.SearchEngineCoreMessages;
import org.txm.utils.logger.Log;

/**
 * Implements a SearchEngine generic query with a query and a search engine.
 * 
 */

public class Query implements IQuery {

	public LinkedHashMap<String, String> options;

	public Query() {

	}

	public static String queriesToJSON(List<? extends IQuery> queries) {

		JSONArray all = new JSONArray();
		for (IQuery q : queries) {
			all.put(queryToJSONObject(q));
		}
		return all.toString();
	}

	public final static String queryToJSON(IQuery q) {

		return queryToJSONObject(q).toString();
	}

	public static JSONObject queryToJSONObject(IQuery q) {

		try {
			JSONObject queryJson = new JSONObject();
			queryJson.put("q", q.getQueryString()); //$NON-NLS-1$
			if (q.getSearchEngine() == null) {
				queryJson.put("e", "CQP"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			else {
				queryJson.put("e", q.getSearchEngine().getName()); //$NON-NLS-1$
			}

			JSONObject optionsJson = new JSONObject();
			for (String option : q.getOptionNames()) {
				optionsJson.put(option, q.getOption(option));
			}
			queryJson.putOpt("o", optionsJson); //$NON-NLS-2$

			return queryJson;
		}
		catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static List<IQuery> JSONtoQueries(String json) {

		if (json == null) return new ArrayList<>();
		
		List<IQuery> queries = new ArrayList<>();
		try {
			Object o = new JSONArray(json);

			if (o instanceof JSONArray all) {
				for (int i = 0; i < all.length(); i++) {
					JSONObject queryJson = all.getJSONObject(i);

					String eName = queryJson.getString("e"); //$NON-NLS-1$
					SearchEngine e = SearchEnginesManager.getSearchEngine(eName);

					Query q = e.newQuery();
					q.setQuery(queryJson.getString("q")); //$NON-NLS-1$

					JSONObject optionsJson = queryJson.getJSONObject("o"); //$NON-NLS-1$
					String[] names = JSONObject.getNames(optionsJson);
					if (names != null) {
						for (String option : names) {
							q.setOption(option, optionsJson.getString(option));
						}
					}

					queries.add(q);
				}
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
			
			queries.add(JSONtoQuery(json));
		}
		return queries;
	}

	public static IQuery JSONObjectToQuery(JSONObject queryJson) {

		try {
			String eName = queryJson.getString("e"); //$NON-NLS-1$
			if (eName == null) eName = "CQP"; // default //$NON-NLS-1$
			SearchEngine e = SearchEnginesManager.getSearchEngine(eName);

			IQuery q = e.newQuery();
			q.setQuery(queryJson.getString("q")); //$NON-NLS-1$

			if (!queryJson.isNull("o")) { //$NON-NLS-1$
				JSONObject optionsJson = queryJson.getJSONObject("o"); //$NON-NLS-1$
//				System.out.println("obj="+optionsJson);
//				Class<? extends JSONObject> c = optionsJson.getClass();
//				System.out.println(java.util.Arrays.toString(c.getDeclaredMethods()));
				if (optionsJson != null) {
					String[] names = JSONObject.getNames(optionsJson);
					if (names != null) {
						for (String option : names) {
							q.setOption(option, optionsJson.getString(option));
						}
					}
				}
			}

			return q;
		}
		catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public final static IQuery JSONtoQuery(String json) {
		try {
			return JSONObjectToQuery(new JSONObject(json));
		}
		catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Converts the specified list of Query to a string.
	 * Queries strings are separated by a tabulation.
	 * 
	 * @param collection
	 * @return
	 */
	public static String queriesToString(Collection<? extends IQuery> collection, String separator) {
		if (collection == null) return ""; //$NON-NLS-1$
		StringBuilder str = new StringBuilder();
		int i = 0;
		for (IQuery q : collection) {
			if (i > 0) {
				str.append(separator);
			}
			str.append(q.getQueryString());
			str.append(separator + q.getSearchEngine().getName());
			i++;
		}
		return str.toString();
	}

	/**
	 * Converts the specified list of Query to a string.
	 * Queries strings are separated by a tabulation.
	 * 
	 * @param collection
	 * @return
	 */
	public static String queriesToString(Collection<? extends IQuery> collection) {
		return queriesToString(collection, TXMPreferences.LIST_SEPARATOR);
	}

	/**
	 * Converts the specified Query to a string.
	 * contains the query string and the query engine
	 * 
	 * @param q the query to serialize
	 * @return
	 */
	public static String queryToString(IQuery q) {

		//return q.getQueryString() + TXMPreferences.LIST_SEPARATOR + q.getSearchEngine().getName();
		return queryToJSON(q);
	}

	@Override
	public int hashCode() {
		
		if (queryString != null && engine != null) {
			return queryString.hashCode() + engine.getName().hashCode();
		}
		return super.hashCode();
	}

	@Override
	public boolean canBeMultiLine() {
		return false;
	}

	/**
	 * Converts the specified string to a list of Query.
	 * Input string must contains queries separated by a tabulation.
	 * 
	 * RAW String Or JSON String
	 * 
	 * A RAW String query is composed of a <engine name + tabulation + query>
	 * 
	 * if the String list is odd, the import fails
	 * 
	 * @param str
	 * @return
	 */
	public static List<IQuery> stringToQueries(String str) {

		ArrayList<IQuery> queries = new ArrayList<>();

		if (str == null || str.length() == 0) {
			return queries;
		}
		if (str.startsWith("[{") && str.endsWith("}]")) { //$NON-NLS-1$ //$NON-NLS-2$
			return JSONtoQueries(str);
		} else if (str.startsWith("{") && str.endsWith("}")) { //$NON-NLS-1$ //$NON-NLS-2$
			ArrayList<IQuery> rez = new ArrayList<IQuery>();
			rez.add(JSONtoQuery(str));
			return rez;
		}

		String[] split = str.split(TXMPreferences.LIST_SEPARATOR);

		for (int i = 0; i + 1 < split.length; i = i + 2) {
			String s = split[i];
			String eName = split[i + 1];
			SearchEngine e = SearchEnginesManager.getSearchEngine(eName);
			if (e == null) {

				s = split[i + 1 ]; // retrocompatibility with old format of query serialisation
				eName = split[i];
				e = SearchEnginesManager.getSearchEngine(eName);

				if (e == null) {
					Log.warning(NLS.bind(SearchEngineCoreMessages.ErrorUnknownSearchEngineP0, Arrays.toString(split)));
					continue;
				}
			}

			Query q = e.newQuery();
			q.setQuery(s);
			queries.add(q);
		}
		return queries;
	}

	/**
	 * Converts the specified string to a Query.
	 * Input string must contains queries separated by a tabulation.
	 * 
	 * @param str
	 * @return
	 */
	public static IQuery stringToQuery(String str) {

		//if (str == null || str.length() == 0) return new Query();
		
		if (str.startsWith("{") && str.endsWith("}")) { //$NON-NLS-1$ //$NON-NLS-2$
			return JSONtoQuery(str);
		}

		String[] split = str.split(TXMPreferences.LIST_SEPARATOR, 2);
		String eName = "CQP"; //$NON-NLS-1$ //$NON-NLS-1$
		String qs = null;
		if (split.length == 2) {
			qs = split[0];
			eName = split[1];
		}
		else {
			qs = split[0];
		}

		SearchEngine e = SearchEnginesManager.getSearchEngine(eName);
		if (e == null) { // retro compatibility
			String tmp = eName;
			eName = qs;
			qs = tmp;
			e = SearchEnginesManager.getSearchEngine(eName);
			if (e == null) { // no luck
				e = SearchEnginesManager.getCQPSearchEngine();
				if (qs.length() == 0 && eName.length() > 0) {
					qs = eName;
				}
			}
		}

		IQuery q = e.newQuery();
		q.setQuery(qs);
		return q;
	}

	// old code from IQuery
	//public static IQuery fromPrefString(String prefString) {
	//	
	//	String[] split = prefString.split("\t", 2); //$NON-NLS-1$
	//	
	//	String engineName = null;
	//	String queryString = null;
	//	
	//	if (split.length == 2) {
	//		engineName = split[0];
	//		queryString = split[1];
	//	} else {
	//		engineName = "CQP"; //$NON-NLS-1$
	//		queryString = split[0];
	//	}
	//	
	//	SearchEngine se = SearchEnginesManager.getSearchEngine(engineName);
	//	if (se == null) {
	//		Log.severe(NLS.bind(SearchEngineCoreMessages.NoSearchengineP0Found, engineName));
	//		return null;
	//	}
	//	IQuery query = se.newQuery();
	//	query.setQuery(queryString);
	//	return query;
	//}


	/**
	 * The query string.
	 */
	protected SearchEngine engine = null;

	/**
	 * The query string.
	 */
	protected String queryString = ""; //$NON-NLS-1$

	/**
	 * The query friendly name.
	 */
	protected String friendlyName = ""; //$NON-NLS-1$

	/**
	 * Instantiates a new query.
	 * 
	 * @param queryString
	 *            the query string
	 * 
	 *            public Query(String queryString){
	 *            this.queryString=queryString.trim(); }
	 */
	public Query(String queryString, SearchEngine engine) {
		
		if (queryString != null) {
			this.queryString = queryString.trim();
		}
		else {
			this.queryString = ""; //$NON-NLS-1$
		}

		this.engine = engine;
	}

	@Override
	public Query setName(String name) {
		
		this.friendlyName = name;
		return this;
	}

	@Override
	public String getName() {
		
		if (friendlyName == null || friendlyName.length() == 0) return queryString; // if no name, the query is the name

		return friendlyName;
	}

	@Override
	public final boolean equals(Object o) {
		
		if (!(o instanceof IQuery q)) return false;
		
		return equals(q);
	}

//	@Override
//	public boolean equals(Object obj) {
//
//		if (obj == null) return false;
//
//		if (!(obj instanceof Query)) {
//			return false;
//		}
//		if (this.queryString == null) return false;
//		if (this.engine == null) return false;
//
//		Query q = ((Query) obj);
//		return this.queryString.equals(q.queryString)
//				&& this.engine.equals(q.engine)
//				&& this.getName().equals(q.getName());
//	}

	/**
	 * BY default don't fix the query
	 */
	@Override
	public IQuery fixQuery(String lang) {
		return this;
	}

	/**
	 * Gets the query string.
	 * 
	 * @return the query string
	 */
	@Override
	public final String getQueryString() {
		return queryString;
	}

	@Override
	public SearchEngine getSearchEngine() {
		return engine;
	}

	/**
	 * Checks whatever the query is empty (null, empty or contains only double quotes) or not.
	 * 
	 * @return true if empty otherwise false
	 */
	@Override
	public boolean isEmpty() {
		return queryString == null || queryString.length() == 0 || "\"\"".equals(queryString); //$NON-NLS-1$
	}

	@Override
	public IQuery setQuery(String rawQuery) {
		queryString = rawQuery;
		return this;
	}

	/**
	 * Creates a readable formated string from the query.
	 * 
	 * @return
	 */
	@Override
	public String asString() {
		return (friendlyName != null ? friendlyName : "") + "<" + this.queryString + ">"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}


	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return queryString;
	}

	public static Object toPrettyString(List<? extends IQuery> queries) {

		if (queries == null) return ""; //$NON-NLS-1$
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < queries.size(); i++) {
			if (i > 0) {
				str.append(TXMPreferences.LIST_SEPARATOR);
			}
			IQuery q = queries.get(i);
			str.append(q.getQueryString());
			//str.append(TXMPreferences.LIST_SEPARATOR + q.getSearchEngine().getName());
		}
		return str.toString();
	}

	@Override
	public int compareTo(IQuery o) {

		int c = this.getSearchEngine().getName().compareTo(o.getSearchEngine().getName());

		if (c == 0) {
			c = this.queryString.compareTo(o.getQueryString());
		}

		if (c == 0) {
			c = this.getName().compareTo(o.getName());
		}

		return c;
	}

	@Override
	public String removeOption(String name) {

		if (name == null) return null;
		if (options == null) return null;
		return options.remove(name);
	}

	@Override
	public void setOption(String name, String value) {

		if (name == null) return;
		if (options == null) options = new LinkedHashMap<>();
		options.put(name, value);
	}

	@Override
	public String getOption(String name) {

		if (name == null) return null;
		if (options == null) return null;
		return options.get(name);
	}


	@Override
	public HashMap<String, String> getOptions() {

		if (options == null) options = new LinkedHashMap<>();
		return options;

	}

	@Override
	public Set<String> getOptionNames() {

		if (options == null) return new HashSet<String>();
		return options.keySet();
	}

	@Override
	public boolean equals(IQuery q) {
		
		return this.engine.equals(q.getSearchEngine())  // same engine
				&& this.getQueryString().equals(q.getQueryString()) // same raw query
				&& this.getName().equals(q.getName()) // same name
				&& this.getOptions().toString().equals(q.getOptions().toString()) // same options values
				;
	}
}
