package org.txm.searchengine.core;

import java.util.ArrayList;
import java.util.List;

import org.txm.objects.Match;

public abstract class Selection {

	public abstract List<? extends Match> getMatches() throws Exception;

	public abstract List<? extends Match> getMatches(int from, int to) throws Exception;

	public abstract int getNMatch() throws Exception;

	public abstract IQuery getQuery();

	public abstract boolean isTargetUsed() throws Exception;

	public abstract void drop() throws Exception;

	public abstract boolean delete(int iMatch);

	public abstract boolean delete(Match match);

	public abstract boolean delete(ArrayList<Match> matchesToRemove);

	public int[] getStarts() throws Exception {

		List<? extends Match> matches = getMatches();
		int[] starts = new int[matches.size()];
		for (int i = 0; i < matches.size(); i++) {
			starts[i] = matches.get(i).getStart();
		}
		return null;
	}
}
