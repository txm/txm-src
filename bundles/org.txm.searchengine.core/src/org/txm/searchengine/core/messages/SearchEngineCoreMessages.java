package org.txm.searchengine.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;


/**
 * Search engine core messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class SearchEngineCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.searchengine.core.messages.messages"; //$NON-NLS-1$


	public static String deletingQueryResultP0;
	public static String ErrorUnknownSearchEngineP0;
	
	public static String info_computingCorpusP0;
	public static String info_computingPartP0WithQueryP1;
	public static String info_computingTheP0PartitionOfTheP1Corpus;
	public static String info_computingTheP0SubCorpusOfP1CommaQueryP2;
	public static String info_computingTheP0SubCorpusOfP1CommaStructureP2CommaPropertyP3ColonP4;
	public static String info_creatingNewPartitionP0OfP1;
	public static String info_deletingPartitionP0;
	public static String info_doneP0Parts;
	public static String info_partitionP0CreatedInP1Ms;
	public static String info_searchEngineStopped;
	public static String info_stoppingSearchEngine;
	
	public static String matchingStrategy;
	public static String NoSearchengineP0Found;
	
	public static String QueryHistory;
	public static String QueryHistoryOfP0;
	
	public static String sizeOfSubcorpusP0P1ComputedInP2Ms;
	public static String startingP0;
	public static String subcorpusP0CreatedInP1Ms;
	
	
	
	



	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, SearchEngineCoreMessages.class);
	}

	private SearchEngineCoreMessages() {
	}

}
