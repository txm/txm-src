package org.txm.searchengine.core;

import java.util.HashMap;
import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.txm.core.results.TXMResult;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Match;
import org.txm.searchengine.core.messages.SearchEngineCoreMessages;
import org.txm.utils.LogMonitor;

public abstract class SearchEngine implements ISearchEngine {

	public static final String EXTENSION_POINT_ID = SearchEngine.class.getCanonicalName();

	@Override
	public abstract String getName();

	public boolean start() throws Exception {
		return start(new LogMonitor(NLS.bind(SearchEngineCoreMessages.startingP0, getName())));
	}

	@Override
	public abstract Selection query(CorpusBuild corpus, IQuery query, String name, boolean saveQuery) throws Exception;

	@Override
	public abstract Query newQuery();

	/**
	 * 
	 * @param corpus
	 * @return true if the Engine can query the corpus
	 */
	@Override
	public abstract boolean hasIndexes(CorpusBuild corpus);

	@Override
	public String getDetails() {
		return this.getClass() + " " + this.toString(); //$NON-NLS-1$
	}

	/**
	 * 
	 * @return null OR an option name if this searchengine offer options for another engine
	 */
	public String getOptionForSearchengine() {
		return null;
	}

	/**
	 * 
	 * @return null OR an option name if thi searchengine uses the enginename engine
	 */
	public String getOptionNameForSearchengine() {
		return null;
	}

	/**
	 * 
	 * @return null OR an option description if thi searchengine uses the enginename engine
	 */
	public String getOptionDescriptionForSearchengine() {
		return null;
	}

	/**
	 * 
	 * @return all query option ID and possible values 
	 */
	public HashMap<String, List<String>> getAvailableQueryOptions() {
		return new HashMap<String, List<String>>();
	}

	public boolean equals(SearchEngine e) {

		if (e == null) return false;
		if (this.getName() == null) return false;

		return this.getName().equals(e.getName());
	}

	/**
	 * 
	 * @param match
	 * @param property
	 * @return the first position property value
	 */
	public abstract String getValueForProperty(Match match, SearchEngineProperty property);

	/**
	 * 
	 * @param match
	 * @param property
	 * @return the positions property value
	 */
	public abstract List<String> getValuesForProperty(Match match, SearchEngineProperty property);

	/**
	 * 
	 * @return indicates if the SearchEngine queries need mumltiple lines to be written
	 */
	public boolean hasMultiLineQueries() {
		return false;
	}

	@Override
	public String hasAdditionalDetailsForResult(TXMResult result) {
		return null;
	}

	@Override
	public String getAdditionalDetailsForResult(TXMResult result) {
		return null;
	}
}
