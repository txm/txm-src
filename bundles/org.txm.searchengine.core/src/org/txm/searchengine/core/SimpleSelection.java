package org.txm.searchengine.core;

import java.util.ArrayList;
import java.util.List;

import org.txm.objects.Match;

public class SimpleSelection<T extends Match> extends Selection {

	ArrayList<T> matches;

	IQuery query;

	public SimpleSelection(IQuery q, ArrayList<T> matcheList) {
		query = q;
		matches = matcheList;
	}

	@Override
	public List<T> getMatches() throws Exception {
		return matches;
	}

	@Override
	public List<T> getMatches(int from, int to) throws Exception {
		if (to > matches.size()) to = matches.size();
		return matches.subList(from, to + 1); // +1 because Match.getMatches is start-end inclusive and List.subList is start inclusif and end exclusif
	}

	@Override
	public int getNMatch() throws Exception {
		return matches.size();
	}

	@Override
	public IQuery getQuery() {
		return query;
	}

	@Override
	public boolean isTargetUsed() throws Exception {
		return false;
	}

	@Override
	public void drop() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean delete(int iMatch) {
		if (iMatch < 0) return false;
		if (iMatch >= matches.size()) return false;
		return matches.remove(iMatch) != null;
	}

	@Override
	public boolean delete(Match match) {
		return matches.remove(match);
	}

	@Override
	public boolean delete(ArrayList<Match> matchesToRemove) {
		return matches.removeAll(matchesToRemove);
	}
}
