package org.txm.searchengine.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashSet;

import org.eclipse.osgi.util.NLS;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.TXMResult;
import org.txm.objects.Project;
import org.txm.searchengine.core.messages.SearchEngineCoreMessages;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.io.IOUtils;

public class QueryHistory extends TXMResult {

	LinkedHashSet<IQuery> queries = new LinkedHashSet<IQuery>(); // first is the older and last is the newer

	boolean modified = false;

	public QueryHistory(String parametersNodePath, TXMResult parent) {
		super(parametersNodePath, parent);
	}

	public QueryHistory(String parametersNodePath) {
		super(parametersNodePath, null);
	}

	public QueryHistory(Project corpus) {
		super(null, corpus);
	}

	/**
	 * Gets the internal persistable state.
	 * 
	 * @return the persistable
	 */
	public boolean isInternalPersistable() {
		return true;
	}

	@Override
	public boolean saveParameters() throws Exception {

		return true;
	}

	@Override
	public boolean isVisible() {

		return true;
	}

	@Override
	public boolean loadParameters() throws Exception {

		return true;
	}

	public String getComputingDoneMessage() {
		return ""; //$NON-NLS-1$
	}

	public String getComputingStartMessage() {
		return ""; //$NON-NLS-1$
	}

	@Override
	public String getName() {

		return SearchEngineCoreMessages.QueryHistory;
	}

	@Override
	public String getSimpleName() {

		return SearchEngineCoreMessages.QueryHistory;
	}

	@Override
	public String getDetails() {

		return NLS.bind(SearchEngineCoreMessages.QueryHistoryOfP0, queries.size());
	}

	@Override
	public void clean() {

	}

	@Override
	public boolean canCompute() throws Exception {

		return true;
	}

	public Project getParent() {
		return (Project) super.getParent();
	}

	@Override
	public void onProjectClose() {
		saveQueriesToFile();
	}

	@Override
	public void onWorkspaceClose() {
		saveQueriesToFile();
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {
		queries.clear();

		loadQueriesFromFile();
		return true;
	}

	@SuppressWarnings("unchecked")
	public <Q extends IQuery> ArrayList<Q> getQueries(Class<Q> clazz) {
		ArrayList<Q> ret = new ArrayList<Q>();
		for (IQuery q : queries) {
			if (clazz == null || q.getClass().equals(clazz) || q.getClass().isInstance(clazz) || q.getClass().isAssignableFrom(clazz)) {
				ret.add((Q) q);
			}
		}
		return ret;

	}


	public boolean saveQueriesToFile() {

		if (modified) {
			File logFile = new File(this.getParent().getProjectDirectory(), "QUERIES"); //$NON-NLS-1$
			try {
				return _toTxt(logFile, "UTF-8", "", ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}
		return true;
	}

	public void loadQueriesFromFile() throws IOException {

		File logFile = new File(this.getParent().getProjectDirectory(), "QUERIES"); //$NON-NLS-1$
		if (logFile.exists() && logFile.canRead()) {
			int MAX = TBXPreferences.getInstance().getInt(TBXPreferences.QUERY_HISTORY_SIZE);

			BufferedReader reader = IOUtils.getReader(logFile);
			String line = reader.readLine();
			while (line != null) {
				line = line.trim();
				if (line.length() > 0 && !line.startsWith("#")) { //$NON-NLS-1$
					IQuery q = Query.stringToQuery(line);
					queries.add(q);
				}

				if (queries.size() > MAX) break;
				line = reader.readLine();
			}
			reader.close();
			modified = false;
		}
	}

	public void addQuery(IQuery query) {

		//int MAX = TBXPreferences.getInstance().getInt(TBXPreferences.QUERY_HISTORY_SIZE);
		int MAX = 5;
		reduceSize(MAX - 1); // ensure history size limit

		boolean removed = queries.remove(query);
		queries.add(query);

		modified = true;
	}

	public void removeQuery(IQuery query) {

		queries.remove(query);
		modified = true;
	}

	public void reduceSize(int MAX) {
		int diff = queries.size() - MAX; // number of queries to remove
		if (diff == 0) return;
		
		
		ArrayList<IQuery> toRemove = new ArrayList<>();
		for (IQuery q : queries) {
			if (diff == 0) return;
			
			toRemove.add(q);
			diff--;
		}
		queries.removeAll(toRemove);
		modified = true;
	}

	@Override
	protected boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {

		PrintWriter writer = IOUtils.getWriter(outfile, encoding);

		for (IQuery q : queries) {
			writer.println(Query.queryToString(q));
		}
		writer.close();

		return true;
	}

	@Override
	public String getResultType() {

		return "History"; //$NON-NLS-1$
	}

	public ArrayList<Query> getQueries() {
		return getQueries(null);
	}
}
