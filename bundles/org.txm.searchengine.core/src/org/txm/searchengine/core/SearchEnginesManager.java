package org.txm.searchengine.core;

import java.util.ArrayList;
import java.util.List;

import org.txm.Toolbox;
import org.txm.core.engines.EngineType;
import org.txm.core.engines.EnginesManager;
import org.txm.objects.CorpusBuild;

/**
 * Search engines manager.
 * 
 * @author mdecorde
 *
 */
// FIXME: useless class, the type and priority/order should be defined in org.txm.core.engines.EnginesManager extension point
public class SearchEnginesManager extends EnginesManager<SearchEngine> {

	private static final long serialVersionUID = 8476457980915443357L;

	/**
	 * 
	 * @return
	 */
	public static SearchEngine getCQPSearchEngine() {
		return (SearchEngine) Toolbox.getEngineManager(EngineType.SEARCH).getEngine("CQP"); //$NON-NLS-1$
	}

	/**
	 * 
	 * @return
	 */
	public static SearchEngine getTIGERSearchEngine() {
		return (SearchEngine) Toolbox.getEngineManager(EngineType.SEARCH).getEngine("TIGER"); //$NON-NLS-1$
	}

	/**
	 * 
	 * @return
	 */
	public static SearchEngine getSearchEngine(String name) {
		return (SearchEngine) Toolbox.getEngineManager(EngineType.SEARCH).getEngine(name);
	}

	@Override
	public boolean fetchEngines() {
		return this.fetchEngines(SearchEngine.EXTENSION_POINT_ID);
	}

	@Override
	public EngineType getEnginesType() {
		return EngineType.SEARCH;
	}

	public static List<SearchEngine> getAvailableEngines(CorpusBuild corpus) {
		List<SearchEngine> availables = new ArrayList<>();
		SearchEnginesManager em = (SearchEnginesManager) Toolbox.getEngineManager(EngineType.SEARCH);
		for (String name : em.getEngines().keySet()) {
			SearchEngine engine = em.getEngine(name);
			if (engine.hasIndexes(corpus)) {
				availables.add(engine);
			}
		}
		return availables;
	}
}
