package org.txm.searchengine.core;

import java.util.ArrayList;
import java.util.List;

import org.txm.objects.Match;

public class EmptySelection extends Selection {

	private static final List<? extends Match> EMPTY = new ArrayList<Match>();

	protected IQuery query;

	public EmptySelection(IQuery query) {
		this.query = query;
	}

	public List<? extends Match> getMatches() {
		return EMPTY;
	}

	public List<? extends Match> getMatches(int from, int to) {
		return EMPTY;
	}

	public int getNMatch() {
		return 0;
	}

	public IQuery getQuery() {
		return query;
	}

	@Override
	public boolean isTargetUsed() {
		return false;
	}

	@Override
	public void drop() throws Exception {
	}

	@Override
	public boolean delete(int iMatch) {
		// nothing to do
		return false;
	}

	@Override
	public boolean delete(Match match) {
		// nothing to do
		return false;
	}

	@Override
	public boolean delete(ArrayList<Match> matchesToRemove) {
		return false;
	}
}
