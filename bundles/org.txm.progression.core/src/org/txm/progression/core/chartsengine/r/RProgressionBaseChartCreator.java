/**
 * 
 */
package org.txm.progression.core.chartsengine.r;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.txm.chartsengine.r.core.RChartCreator;
import org.txm.chartsengine.r.core.themes.DefaultTheme;
import org.txm.progression.core.chartsengine.base.ProgressionChartCreator;
import org.txm.progression.core.chartsengine.base.Utils;
import org.txm.progression.core.functions.Progression;
import org.txm.stat.engine.r.RDevice;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.logger.Log;

/**
 * Base chart creator for R Progression charts.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public abstract class RProgressionBaseChartCreator extends RChartCreator<Progression> implements ProgressionChartCreator {



	/**
	 * Creates a progression chart file.
	 * 
	 * @param result
	 * @param file
	 * @param cumulative
	 * @return
	 */
	public File createChart(Progression result, File file, boolean cumulative) {

		try {

			Progression progression = result;

			// boolean cumulative = result.getBooleanParameterValue(ProgressionPreferences.CHART_CUMULATIVE);

			// parameters
			int renderingColorMode = progression.getRenderingColorsMode();

			RWorkspace rw = RWorkspace.getRWorkspaceInstance();
			rw.eval("library(textometry)"); //$NON-NLS-1$
			// System.out.println("create list of positions");


			List<int[]> allPositions = progression.getAllPositions();
			String listString = "list("; //$NON-NLS-1$
			for (int i = 0; i < allPositions.size(); i++) {
				int[] positions = allPositions.get(i);
				// System.out.println("add vector "+i);
				rw.eval("rm(x" + i + ")"); //$NON-NLS-1$ //$NON-NLS-2$
				rw.addVectorToWorkspace("x" + i, positions); //$NON-NLS-1$
				listString += "x" + i + ","; //$NON-NLS-1$ //$NON-NLS-2$
			}
			listString = listString.substring(0, listString.length() - 1) + ")"; //$NON-NLS-1$
			// System.out.println("create positions");
			rw.eval("rm(positions)"); //$NON-NLS-1$
			rw.eval("positions <- " + listString); //$NON-NLS-1$

			rw.eval("rm(structurepositions)"); //$NON-NLS-1$
			rw.addVectorToWorkspace("structurepositions", progression.getStructurePositions()); //$NON-NLS-1$
			
			rw.eval("rm(structurenames)"); //$NON-NLS-1$
			// if (structurenames != null) structurenames = new
			// String[structurepositions.length];
			for (int i = 0; i < progression.getStructureNames().length; i++) { // Fix empty attribute values
				if ("".equals(progression.getStructureNames()[i]) || progression.getStructureNames()[i] == null) {  //$NON-NLS-1$
					progression.getStructureNames()[i] = "--"; //$NON-NLS-1$
				}
			}
			rw.addVectorToWorkspace("structurenames", progression.getStructureNames()); //$NON-NLS-1$
			// System.out.println("create list positions");
			// rw.getConnection().assign("positions",Rpositions);

			this.getChartsEngine().setColors(renderingColorMode, progression.getQueries().size());

			// Multiple line styles or not
			rw.eval("rm(styles)"); //$NON-NLS-1$
			if (!progression.isMultipleLineStrokes()) {
				rw.addVectorToWorkspace("styles", DefaultTheme.monostyles); //$NON-NLS-1$
			}
			else {
				rw.addVectorToWorkspace("styles", DefaultTheme.styles); //$NON-NLS-1$
			}

			// System.out.println("create styles vector");
			rw.eval("rm(widths)"); //$NON-NLS-1$
			rw.addVectorToWorkspace("widths", DefaultTheme.monoWidths); //$NON-NLS-1$

			// System.out.println("create names vector");
			String[] queriesstring = new String[progression.getQueries().size()];
			for (int i = 0; i < progression.getQueries().size(); i++) {
				queriesstring[i] = progression.getQueries().get(i).getQueryString();
			}
			rw.eval("rm(names)"); //$NON-NLS-1$
			rw.addVectorToWorkspace("names", queriesstring); //$NON-NLS-1$

			String name = progression.getCorpus().getName();

			// System.out.println("BANDE="+bande);

			String cmd = "progression(positions, names, colors, styles, widths, \"" + name + "\"," + progression.getXminCorpus() + "," + progression.getXmaxCorpus() + ",\"" + cumulative //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					+ "\",structurepositions,structurenames, \"\", " + progression.getBande() + ");\n";  //$NON-NLS-1$ //$NON-NLS-2$

			this.getChartsEngine().plot(file, cmd, result, StringEscapeUtils.escapeJava(Utils.createProgressionChartTitle(progression, cumulative)), null, RDevice.getDeviceForFile(file));

			symbol = Progression.prefixR + Progression.norep;

			RWorkspace.getRWorkspaceInstance().eval(symbol + " <- list(positions=positions, names=names, xmin=" + progression.getXminCorpus() + ",xmax=" + progression //$NON-NLS-1$ //$NON-NLS-2$
					.getXmaxCorpus() + ", structpositions=structurepositions, structnames=structurenames)");  //$NON-NLS-1$
			Progression.norep++;
		}
		catch (RWorkspaceException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}

		return file;
	}


	@Override
	public ArrayList<Integer> getSelectedPointPositions(Object chart) {
		Log.severe(this.getClass() + ".getProgressionChartSelectedPointPositions(): Not yet implemented."); //$NON-NLS-1$
		return new ArrayList<>(0);
	}



	@Override
	public ArrayList<Integer> getSelectedSeries(Object chart) {
		Log.severe(this.getClass() + ".getProgressionChartSelectedSeries(): Not yet implemented."); //$NON-NLS-1$
		return new ArrayList<>(0);
	}


	@Override
	public ArrayList<Integer> getSelectedPointNumbers(Object chart) {
		Log.severe(this.getClass() + ".getProgressionChartSelectedPointNumber(): Not yet implemented."); //$NON-NLS-1$
		return new ArrayList<>(0);
	}


	@Override
	public int getLastSelectedSeries(Object chart) {
		Log.severe(this.getClass() + ".getProgressionChartLastSelectedSeries(): Not yet implemented."); //$NON-NLS-1$
		return -1;
	}

	@Override
	public ArrayList<ArrayList<Integer>> getAllPointPositionsBySeries(Object chart) {
		Log.severe(this.getClass() + ".getAllPointPositionsBySeries(): Not yet implemented."); //$NON-NLS-1$
		return null;
	}


	@Override
	public Class<Progression> getResultDataClass() {
		return Progression.class;
	}

}
