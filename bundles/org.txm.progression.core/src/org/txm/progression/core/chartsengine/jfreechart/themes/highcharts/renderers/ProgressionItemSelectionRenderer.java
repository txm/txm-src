package org.txm.progression.core.chartsengine.jfreechart.themes.highcharts.renderers;

import java.awt.Color;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeriesCollection;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.RendererUtils;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.renderers.ItemSelectionXYStepRenderer;
import org.txm.chartsengine.jfreechart.core.themes.highcharts.defaulttheme.swing.CustomHTMLToolTip;
import org.txm.progression.core.functions.Progression;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.query.Match;

/**
 * Renderer providing item selection system and drawing features for selected item and custom rendering for Progression charts.
 * 
 * @author sjacquot
 *
 */
public class ProgressionItemSelectionRenderer extends ItemSelectionXYStepRenderer {

	private static final long serialVersionUID = -2180907160906491676L;

	/**
	 * Needed to get the Graphics2D object to use in getItemShape() to compute the label text bounds and draw a background rectangle.
	 */
	protected ChartPanel chartPanel;

	/**
	 * Stack to store selected items to draw at last, over others items.
	 */
	protected ArrayList<?> itemsToDrawAtLast;

	/**
	 * additional series informations to show in tooltips
	 */
	protected List<List<String>> additionalInformations;

	/**
	 * alternative series shape to show in tooltips
	 */
	protected List<List<Double>> additionalShapesScale;

	/**
	 * Creates a renderer dedicated to Progression charts.
	 */
	public ProgressionItemSelectionRenderer() {
		super(true, true);
		// FIXME: simple selection mode
		//((MultipleItemsSelector) this.multipleItemsSelector).setSimpleSelectionMode(true);
	}



	/**
	 * Initializes the tool tip generator.
	 */
	public void initToolTipGenerator(final IRendererWithItemSelection renderer) {
		this.setDefaultToolTipGenerator(new XYToolTipGenerator() {

			@Override
			public String generateToolTip(XYDataset dataset, int series, int item) {

				// Hexadecimal color
				Color color = (Color) getSeriesPaint(series);
				// FIXME: this case should never happen but it sometimes happens with Progression
				if (color == null) {
					return ""; //$NON-NLS-1$
				}
				String hex = "#" + Integer.toHexString(color.getRGB()).substring(2); //$NON-NLS-1$
				XYSeriesCollection xyDataset = (XYSeriesCollection) dataset;

				//
				//				// FIXME: test affichage des concordances des occurrences directement dans le tooltip
				//				// Progression
				//				String concordanceStr = "";

				//				if(renderer.getChartType() == ChartsEngine.CHART_TYPE_PROGRESSION)	{
				//					try {
				//						Progression progression = ((Progression)renderer.getItemSelector().getResultData());
				//							//System.out.println("ItemSelectionXYStepRenderer.initToolTipGenerator(...).new XYToolTipGenerator() {...}.generateToolTip() query: " + progression.getQueries().get(xyDataset.getSeriesIndex(xyDataset.getSeries(series).getKey())));
				//						Property analysisProperty = progression.
				//								getCorpus().
				//								getProperty("word");
				//						List<Property> viewProperties = new ArrayList<Property>();
				//						viewProperties.add(progression.
				//								getCorpus().
				//								getProperty("word"));
				//						ReferencePattern referencePattern = new ReferencePattern();
				//						ReferencePattern refAnalysePattern = new ReferencePattern();
				//						Concordance concordance = new Concordance(progression.getCorpus(), progression.getQueries().get(xyDataset.getSeriesIndex(xyDataset.getSeries(series).getKey())), analysisProperty, viewProperties, referencePattern, refAnalysePattern, 10, 10);
				//
				//						System.err.println("ItemSelectionXYStepRenderer.initToolTipGenerator(...).new XYToolTipGenerator() {...}.generateToolTip() concordance nlines: " + concordance.getNLines());
				//						System.err.println("ItemSelectionXYStepRenderer.initToolTipGenerator(...).new XYToolTipGenerator() {...}.generateToolTip() item position: " + (xyDataset.getSeries(series).getY(item).intValue() - 1));
				//
				//
				//
				//						Line line = concordance.getLines(xyDataset.getSeries(series).getY(item).intValue() - 1, xyDataset.getSeries(series).getY(item).intValue() - 1).get(0);
				//						concordanceStr = "<p>" + line.leftContextToString() + " <b>" + line.keywordToString() + "</b> " + line.rightContextToString() + "</p>";
				//						//xyDataset.getSeriesIndex(xyDataset.getSeries(series).getKey())
				//					}
				//					catch(CqiClientException e) {
				//						// TODO Auto-generated catch block
				//						e.printStackTrace();
				//					}
				//					catch(IOException e) {
				//						// TODO Auto-generated catch block
				//						e.printStackTrace();
				//					}
				//					catch(CqiServerError e) {
				//						// TODO Auto-generated catch block
				//						e.printStackTrace();
				//					}
				//				}

				String token = xyDataset.getSeriesKey(series).toString();
				token = token.substring(0, token.lastIndexOf(" ")); //$NON-NLS-1$
				int tokenCount = xyDataset.getSeries(series).getItemCount() - 2; // -2 for the 0 point and the dummy last point

				return CustomHTMLToolTip.getDefaultHTMLBody(renderer, hex) + "<p><span style=\"color: " + hex + ";\">" + token + "</span> <b>(" //$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
						+ RendererUtils.valuesNumberFormat.format(xyDataset.getSeries(series).getY(item)) + " / " + tokenCount + ")</b>"    //$NON-NLS-1$ //$NON-NLS-2$
						+ "</p></body><html>"; //$NON-NLS-1$

				// FIXME: test contexts in tool tip
				//				return CustomHTMLToolTip.getDefaultHTMLBody(renderer, hex) + "<p><span style=\"color: " + hex + ";\">" + xyDataset.getSeriesKey(series)
				//						+ "</span></p><p><b>" + valuesNumberFormat.format(xyDataset.getSeries(series).getY(item)) + ChartsEngineMessages.ChartsEngine_LABEL_VALUE_SEPARATORS
				//						+ " </b><b>" + valuesNumberFormat.format(xyDataset.getSeries(series).getX(item)) + "</b>"
				//						+ concordanceStr
				//						+ "</p></body><html>";

			}
		});
	}



	@Override
	public Paint getItemPaint(int series, int item) {

		Color color = (Color) super.getItemPaint(series, item);

		// Skip the line pass
		if (!isLinePass) {

			// Change selected item shape color, reducing transparency
			if (this.multipleItemsSelector.isMouseOverItem(series, item)) {
				return new Color(color.getRed(), color.getGreen(), color.getBlue(), 195);
			}
			// Multiple item selection
			//			else if(this.itemSelector.isMultipleSelectedItem(series, item))	{
			//				// FIXME: rendering tests
			//				return new Color(130, 130, 130, 195);
			//				//return new Color(color.getRed(), color.getGreen(), color.getBlue(), 140);
			//			}

		}

		return color;
	}


	@Override
	public Shape getItemShape(int series, int item) {

		Shape s = super.getItemShape(series, item);
		AffineTransform t = new AffineTransform();

		// shapes are being patched and its not the first false point
		if (item != 0 && additionalShapesScale != null) {
			if (additionalShapesScale.size() > series) {
				List<Double> shapes = additionalShapesScale.get(series);
				int i = item;// - 1;
				if (shapes.size() > i) {
					Double f = shapes.get(i);
					if (f != null) {
						t.setToScale(f, f);
						return t.createTransformedShape(s);
					}
				}
			}
		}

		// TODO: find better way to not draw a shape
		// If not selected item, reduce the shape therefore it will be hidden by the lines chart
		if (!this.multipleItemsSelector.isMouseOverItem(series, item) && !this.multipleItemsSelector.isSelectedItem(series, item)) {
			t.setToScale(0.1, 0.1);
		}
		// FIXME: Multiple selection
		//		else if(this.itemSelector.isMultipleSelectedItem(series, item))	{
		////			
		//			// Add a background rectangle to the label selected item
		//			XYSeriesCollection dataset = (XYSeriesCollection) this.getPlot().getDataset();
		//					
		//			Graphics2D graphics = ((Graphics2D) this.chartPanel.getGraphics());
		//			 
		//			// FIXME: get graphics by creating an image insetad of passing the chart panel to this renderer
		////			BufferedImage img = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
		////			Graphics2D graphics = img.createGraphics();
		//			// FIXME: rendering quality tests
		////			RenderingHints rh = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		////			rh.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		////			rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		////			rh.put(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		////		    graphics.setRenderingHints(rh);
		//			
		//			Font font = this.getItemLabelFont(series, item);
		//			String label = valuesNumberFormat.format(dataset.getSeries(series).getY(item)) + " / " + (dataset.getSeries(series).getItemCount() - 2); //$NON-NLS-1$ // -2 for the 0 point and the dummy last point;
		//
		//			GlyphVector v = font.createGlyphVector(graphics.getFontMetrics(font).getFontRenderContext(), label);
		//	        Shape labelShape = v.getOutline();
		//	        //Rectangle2D labelBounds = v.getVisualBounds();
		//	        
		//			Rectangle2D labelBounds = font.getStringBounds(label, graphics.getFontRenderContext());
		//
		//			// FIXME : not rounded rectangle version
		////			Shape labelBackground = new Rectangle2D.Double(-bounds.getWidth() / 2, -s.getBounds2D().getHeight() / 2 + 1, bounds.getWidth() + 4, bounds.getHeight() / 2 + 8);
		//
		//			//Rectangle2D labelBounds = font.getStringBounds(label, graphics.getFontRenderContext());
		//			
		//	        //Rectangle labelBounds = labelShape.getBounds();
		//	        
		//			//Shape labelBackground = new RoundRectangle2D.Double(-(labelBounds.getWidth() + 4) / 2, -s.getBounds2D().getHeight() / 2 + 1, labelBounds.getWidth() + 8, labelBounds.getHeight() / 2 + 10, 5, 5);
		//			double width = labelBounds.getWidth() + 10;
		//			double height = labelBounds.getHeight() + 10;
		//			Shape labelBackground = new RoundRectangle2D.Double(-width  / 2, -height / 2 - 15, width, height, 5, 5);
		//			
		//			AffineTransform t2 = new AffineTransform();
		//			t2.setToScale(0.9, 0.9);
		//			Shape labelBackground2 = t2.createTransformedShape(labelBackground);
		//			
		//			
		//			Area a = new Area(s);
		//			a.add(new Area(labelBackground));
		//			//a.add(new Area(labelBackground2));
		//			//a.add(new Area(labelShape));
		//			
		//			s = a;
		//			//t.setToScale(1.5, 1.5);
		//
		//			//return a;
		//			
		//		}
		else {
			t.setToScale(1.5, 1.5);
		}
		s = t.createTransformedShape(s);
		return s;
	}

	public void setAdditionalShapeScales(List<List<Double>> shapeScales) {
		this.additionalShapesScale = shapeScales;
	}

	public void setAdditionalLabelInformation(List<List<String>> informations) {
		this.additionalInformations = informations;
	}

	/**
	 * @param chartPanel the chartPanel to set
	 */
	public void setChartPanel(ChartPanel chartPanel) {
		this.chartPanel = chartPanel;
	}


	@Override
	public String getSelectedItemLabelFromDataset(XYDataset dataset, int series, int item) {

		// manage the first and last dummy point
		if (item == 0) {
			item++;
		}
		else if (item == dataset.getItemCount(series) - 1) {
			item--;
		}

		String label = ""; //$NON-NLS-1$
		int globalPosition = (int) dataset.getXValue(series, item) + 1;

		try {

			Property prop;

			// Ref
			prop = ((Progression) this.multipleItemsSelector.getResult()).getCorpus().getProperty("ref"); //$NON-NLS-1$
			// Text id
			if (prop == null) {
				prop = ((Progression) this.multipleItemsSelector.getResult()).getCorpus().getStructuralUnit("text").getProperty("id"); //$NON-NLS-1$ //$NON-NLS-2$
			}

			Match selectedWordMatch = new Match(globalPosition, globalPosition);
			label = selectedWordMatch.getValueForProperty(prop);

		}
		catch (CqiClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// manage the first 0 dummy point
		if (item == 0) {
			item++;
		}

		int tokenCount = ((XYSeriesCollection) dataset).getSeries(series).getItemCount() - 2; // -2 for the 0 point and the dummy last point
		label += "\n" + globalPosition + " / " + RendererUtils.valuesNumberFormat.format(((XYSeriesCollection) dataset).getDomainUpperBound(false)); //$NON-NLS-1$ //$NON-NLS-2$ // -2 for the 0 point and the dummy last point

		label += "\n" + RendererUtils.valuesNumberFormat.format(((XYSeriesCollection) dataset).getSeries(series).getY(item)) + " / " //$NON-NLS-1$//$NON-NLS-2$
				+ (((XYSeriesCollection) dataset).getSeries(series).getItemCount() - 2);   // -2 for the 0 point and the dummy last point 

		if (additionalInformations != null) {
			if (additionalInformations.size() > series) {
				if (additionalInformations.get(series).size() > item) {
					label += "\n " + additionalInformations.get(series).get(item); //$NON-NLS-1$
				}
			}
		}

		return label;
	}



}
