package org.txm.progression.core.chartsengine.base;

import org.eclipse.osgi.util.NLS;
import org.txm.progression.core.functions.Progression;
import org.txm.progression.core.messages.ProgressionCoreMessages;

/**
 * Progression chart utility class.
 * 
 * @author sjacquot
 *
 */
public abstract class Utils {

	public Utils() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * Creates a string title, shared by charts engine implementations, for the progression chart from the specified result.
	 * 
	 * @param progression the progression result
	 * @param doCumulative
	 * @return the titles string
	 */
	public static String createProgressionChartTitle(Progression progression, boolean doCumulative) {

		String title = ProgressionCoreMessages.progressionOf;

		// Put queries in title
		for (int i = 0; i < progression.getQueries().size(); i++) {
			title += progression.getQueries().get(i).getQueryString();
			if (i < progression.getQueries().size() - 1) {
				title += ", "; //$NON-NLS-1$
			}
		}


		title += " " + NLS.bind(ProgressionCoreMessages.inP0, progression.getCorpus().getName()); //$NON-NLS-1$

		if (!doCumulative) {
			title += " " + ProgressionCoreMessages.density; //$NON-NLS-1$
		}

		if (progression.getStructuralUnit() != null) {
			title += "\n(" + ProgressionCoreMessages.structureColon + progression.getStructuralUnit().getName(); //$NON-NLS-1$
			if (progression.getStructuralUnitProperty() != null) {
				title += ProgressionCoreMessages.propertyColon + progression.getStructuralUnitProperty().getName();
				if (progression.getPropertyRegex() != null && progression.getPropertyRegex().length() > 0) {
					title += ProgressionCoreMessages.filteringREGEXColon + progression.getPropertyRegex();
				}
			}
			title += ")"; //$NON-NLS-1$
		}

		return title;
	}


}
