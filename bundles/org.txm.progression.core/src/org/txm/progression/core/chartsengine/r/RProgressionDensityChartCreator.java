package org.txm.progression.core.chartsengine.r;

import java.io.File;

import org.txm.progression.core.functions.Progression;

/**
 * R progression density chart creator.
 * 
 * @author sjacquot
 *
 */
public class RProgressionDensityChartCreator extends RProgressionBaseChartCreator {


	@Override
	public File createChart(Progression result, File file) {
		return super.createChart(result, file, false);
	}

}
