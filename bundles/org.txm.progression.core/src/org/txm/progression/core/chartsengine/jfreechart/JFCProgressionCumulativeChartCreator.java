package org.txm.progression.core.chartsengine.jfreechart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.ui.RectangleAnchor;
import org.jfree.chart.ui.TextAnchor;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.txm.chartsengine.jfreechart.core.JFCChartCreator;
import org.txm.chartsengine.jfreechart.core.renderers.MultipleItemsSelector;
import org.txm.chartsengine.jfreechart.core.renderers.interfaces.IRendererWithItemSelection;
import org.txm.chartsengine.jfreechart.core.themes.base.ExtendedNumberAxis;
import org.txm.progression.core.chartsengine.base.ProgressionChartCreator;
import org.txm.progression.core.chartsengine.base.Utils;
import org.txm.progression.core.chartsengine.jfreechart.themes.highcharts.renderers.ProgressionItemSelectionRenderer;
import org.txm.progression.core.functions.Progression;
import org.txm.progression.core.messages.ProgressionCoreMessages;
import org.txm.progression.core.preferences.ProgressionPreferences;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;

/**
 * JFC progression chart creator.
 * 
 * @author sjacquot
 *
 */
public class JFCProgressionCumulativeChartCreator extends JFCChartCreator<Progression> implements ProgressionChartCreator {


	@Override
	public JFreeChart createChart(Progression progression) {

		// parameters
		boolean cumulative = progression.getBooleanParameterValue(ProgressionPreferences.CHART_CUMULATIVE);


		JFreeChart chart = null;

		// Creates the empty dataset
		XYSeriesCollection dataset = new XYSeriesCollection();
		// Y axis label
		String yAxisLabel;


		// Cumulative
		if (cumulative) {

			// Create the chart
			yAxisLabel = ProgressionCoreMessages.occurrences;
			try {
				chart = this.getChartsEngine().createXYStepChart(dataset, Utils.createProgressionChartTitle(progression, cumulative),
						ProgressionCoreMessages.bind(ProgressionCoreMessages.tEqualsP0, progression.getCorpus().getSize()), yAxisLabel, true, true, true, true, true);
			}
			catch (CqiClientException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			// Custom renderer
			chart.getXYPlot().setRenderer(new ProgressionItemSelectionRenderer());

		}
		// FIXME: SJ: Density, to implement when textometry R package will store the result in the Progression class
		// FIXME: SJ: put that in a new ChartCreator dedicated to the density progression chart
		else {

			// FIXME : density not yet implemented
			System.err.println("JFCProgressionCumulativeChartCreator.createChart(): density mode not yet implemented."); //$NON-NLS-1$

			// for(int i = 0; i < progression.getAllpositions().size(); i++) {
			// int[] liste = positions.get(i);
			// Query query = queries.get(i);
			// XYSeries series = new XYSeries(query.toString() + " " + liste.length);
			//
			// for(int j = 0; j < liste.length; j++) {
			//
			// // FIXME : tests y
			// // y <- c( c(0), y , c(y[[length(x)]]) ) // R code
			// // FIXME : random Y value
			// double y = progression.getAllpositions().get(i)[j] * Math.random();
			// // double y = progression.getAllpositions().get(i)[j];
			// series.add(liste[j], y);
			// }
			// dataset.addSeries(series);
			// }
			//
			// // Create the chart
			// yAxisLabel = ProgressionCoreMessages.ChartsEngine_PROGRESSION_Y_AXIS_NOT_CUMULATIVE_LABEL;
			// chart = this.getChartsEngine().createXYLineChart(dataset, Utils.createProgressionChartTitle(progression, cumulative),
			// ProgressionCoreMessages.bind(ProgressionCoreMessages.tEqualsP0, progression.getXmaxCorpus()), yAxisLabel,
			// true, true, true, false, true, false, true, null);
		}



		((IRendererWithItemSelection) chart.getXYPlot().getRenderer()).getItemsSelector().setResult(progression);


		return chart;

	}


	@Override
	public void updateChart(Progression progression) {

		JFreeChart chart = (JFreeChart) progression.getChart();

		// freeze rendering while computing
		chart.setNotify(false);


		// Fill the data set from the result
		// FIXME: SJ: the tests on queries is not sufficient, it does not work when we switch from R chart to JFC chart because the queries didn't change
		// if(progression.hasParameterChanged(TXMPreferences.QUERIES)) {

		XYSeriesCollection dataset = (XYSeriesCollection) chart.getXYPlot().getDataset();
		dataset.removeAllSeries();

		List<int[]> positions = progression.getAllPositions();
		List<? extends IQuery> queries = progression.getQueries();
		for (int i = 0; i < positions.size(); i++) {
			int[] list = positions.get(i);
			IQuery query = queries.get(i);
			XYSeries series = new XYSeries(query.getName() + " " + list.length); //$NON-NLS-1$
			int c = 1;
			for (int j = 0; j < list.length; j++) {
				// add a first point to draw a vertical line
				if (j == 0) {
					series.add(list[j], 0);
				}
				series.add(list[j], c++);
			}

			// add a dummy point to draw horizontal line between last point and max X corpus
			series.add(progression.getXmaxCorpus(), --c);
			dataset.addSeries(series);
		}

		// Custom domain axis for ticks drawing options
		chart.getXYPlot().setDomainAxis(new ExtendedNumberAxis((NumberAxis) chart.getXYPlot().getDomainAxis(), true, true, progression.getXminCorpus(), progression.getXmaxCorpus()));

		// Custom range axis for ticks drawing options
		chart.getXYPlot().setRangeAxis(new ExtendedNumberAxis((NumberAxis) chart.getXYPlot().getRangeAxis(), false, true, 0, progression.getMaxY()));


		// chart.getXYPlot().getDomainAxis().setRange(progression.getXminCorpus(), progression.getXmaxCorpus());
		// System.out.println("JFCProgressionCumulativeChartCreator.updateChart()" + progression.getXminCorpus());

		// // Match the X-axis origin value to the min x corpus (for sub-corpus case)
		// chart.getXYPlot().getDomainAxis().setLowerBound(progression.getXminCorpus());
		//
		// // Match the X-axis max value to the max x corpus (for sub-corpus case)
		// chart.getXYPlot().getDomainAxis().setUpperBound(progression.getXmaxCorpus());


		((NumberAxis) chart.getXYPlot().getDomainAxis()).setAutoRangeIncludesZero(false);



		// } // ENDOF FIXME



		// removes all existing domain marker
		chart.getXYPlot().clearDomainMarkers();

		// Add the limit markers
		BasicStroke dashedStroke = new BasicStroke(0.5f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f, new float[] { 2f }, 0f);
		Marker marker = new ValueMarker(progression.getXminCorpus());
		marker.setPaint(Color.gray);
		marker.setStroke(dashedStroke);
		chart.getXYPlot().addDomainMarker(marker);
		marker = new ValueMarker(progression.getXmaxCorpus());
		marker.setPaint(Color.gray);
		marker.setStroke(dashedStroke);
		chart.getXYPlot().addDomainMarker(marker);


		// // FIXME: SJ: tests to add limits with annotations rather than markers (marker height is dynamically adapted to the real chart height.
		// Annotation have a fixed height)
		// XYLineAnnotation annotation = new XYLineAnnotation(0, 0, 0, dataset.getRangeUpperBound(true), dashedStroke, Color.gray);
		// chart.getXYPlot().addAnnotation(annotation, true);

		// Add the part markers
		String previousPartName = ""; //$NON-NLS-1$
		if (progression.getStructuralUnit() != null && progression.getStructurePositions().length > 0) {
			for (int i = 0; i < progression.getStructurePositions().length; i++) {
				if (progression.getStructurePositions()[i] != 0) {

					String currentPartName = progression.getStructureNames()[i];

					if (progression.isRepeatingValues() || !currentPartName.equals(previousPartName)) {

						marker = new ValueMarker(progression.getStructurePositions()[i]);
						marker.setPaint(Color.black);
						marker.setLabel(progression.getStructureNames()[i]);
						marker.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
						marker.setLabelTextAnchor(TextAnchor.HALF_ASCENT_LEFT);


						// FIXME: test of rotated annotations rather than using marker label. The problem with this is that the position of annotation is not adjusted when zooming/dezooming.
						// FIXME: the best approach is to redefine the drawing method of
						//// double start = marker.getStartValue();
						//// double end = TempIntervalMarker.getEndValue();
						//// double middle = (end - start)/2;
						// XYTextAnnotation updateLabel = new XYTextAnnotation("Update", progression.getStructurePositions()[i], 0);
						// updateLabel.setFont(new Font("Sans Serif", Font.BOLD, 10));
						// updateLabel.setRotationAnchor(TextAnchor.BASELINE_CENTER);
						// updateLabel.setTextAnchor(TextAnchor.BASELINE_CENTER);
						// updateLabel.setRotationAngle(3.14 / 2);
						// updateLabel.setPaint(Color.black);
						// chart.getXYPlot().addAnnotation(updateLabel);

						chart.getXYPlot().addDomainMarker(marker);
					}

					previousPartName = currentPartName;
				}
			}
		}

		chart.setTitle(Utils.createProgressionChartTitle(progression, true));

		super.updateChart(progression);

	}

	@Override
	public ArrayList<Integer> getSelectedPointPositions(Object chart) {

		MultipleItemsSelector selector = (MultipleItemsSelector) ((IRendererWithItemSelection) ((JFreeChart) chart).getXYPlot().getRenderer()).getItemsSelector();
		ArrayList<Integer> pointPositionsInCorpora = new ArrayList<>();


		// FIXME: returns all the point positions
		XYDataset dataset = ((JFreeChart) chart).getXYPlot().getDataset();
		for (int i = 0; i < selector.getSelectedSeries().length; i++) {
			int selectedSeries = selector.getSelectedSeries()[i];
			int[] selectedItemsInSeries = selector.getSelectedItems(selectedSeries);
			for (int j = 0; j < selectedItemsInSeries.length; j++) {
				// if it's the dummy last chart point, get the previous point position instead of ther last one
				if (selectedItemsInSeries[j] == dataset.getItemCount(selectedSeries) - 1) {
					pointPositionsInCorpora.add((int) (dataset.getXValue(selectedSeries, selectedItemsInSeries[j] - 1)));
				}
				else {
					pointPositionsInCorpora.add((int) (dataset.getXValue(selectedSeries, selectedItemsInSeries[j])));
				}
			}
		}

		// put the last selected point at the end of the list
		if (selector.getLastSelectedSeries() != -1 && selector.getLastSelectedItem() != -1) {
			int item = selector.getLastSelectedItem();
			// if it's the dummy last chart point, get the previous point position instead of ther last one
			if (item == dataset.getItemCount(selector.getLastSelectedSeries()) - 1) {
				item--;
			}

			Integer lastSelectedPointPosition = (int) (dataset.getXValue(selector.getLastSelectedSeries(), item));
			pointPositionsInCorpora.remove(lastSelectedPointPosition);
			pointPositionsInCorpora.add(lastSelectedPointPosition);
		}
		return pointPositionsInCorpora;
	}

	@Override
	public ArrayList<Integer> getSelectedPointNumbers(Object chart) {

		MultipleItemsSelector selector = (MultipleItemsSelector) ((IRendererWithItemSelection) ((JFreeChart) chart).getXYPlot().getRenderer()).getItemsSelector();
		ArrayList<Integer> pointNumbers = new ArrayList<>();
		XYDataset dataset = ((JFreeChart) chart).getXYPlot().getDataset();

		for (int i = 0; i < selector.getSelectedSeriesCount(); i++) {
			int series = selector.getSelectedSeries()[i];
			int[] items = selector.getSelectedItems(series);
			int lastDummyItem = dataset.getItemCount(series) - 1;
			for (int j = 0; j < items.length; j++) {
				int item = items[j];
				// manage the first dummy point
				if (item == 0) {
					item++;
				}
				// manage the last dummy point
				else if (item == lastDummyItem) {
					item--;
				}

				pointNumbers.add(item);
			}
		}

		return pointNumbers;
	}



	@Override
	public ArrayList<ArrayList<Integer>> getAllPointPositionsBySeries(Object chart) {

		ArrayList<ArrayList<Integer>> pointPositionsBySeries = new ArrayList<>();

		XYDataset dataset = ((JFreeChart) chart).getXYPlot().getDataset();

		for (int i = 0; i < dataset.getSeriesCount(); i++) {
			ArrayList<Integer> pointPositionsInCorpora = new ArrayList<>();
			for (int j = 0; j < dataset.getItemCount(i) - 1; j++) { // -1 to skip the last dummy point of the step chart
				pointPositionsInCorpora.add((int) dataset.getXValue(i, j));
			}
			pointPositionsBySeries.add(pointPositionsInCorpora);
		}

		return pointPositionsBySeries;
	}



	@Override
	public ArrayList<Integer> getSelectedSeries(Object chart) {
		ArrayList<Integer> selectedSeries = new ArrayList<>();
		MultipleItemsSelector selector = (MultipleItemsSelector) ((IRendererWithItemSelection) ((JFreeChart) chart).getXYPlot().getRenderer()).getItemsSelector();
		Integer[] multipleSelectedSeries = selector.getSelectedSeries();
		for (int i = 0; i < multipleSelectedSeries.length; i++) {
			selectedSeries.add(multipleSelectedSeries[i]);
		}
		return selectedSeries;
	}


	@Override
	public int getLastSelectedSeries(Object chart) {
		return ((MultipleItemsSelector) ((IRendererWithItemSelection) ((JFreeChart) chart).getXYPlot().getRenderer()).getItemsSelector()).getLastSelectedSeries();
	}



	@Override
	public Class<Progression> getResultDataClass() {
		return Progression.class;
	}



}
