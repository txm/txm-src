package org.txm.progression.core.chartsengine.r;

import java.io.File;

import org.txm.progression.core.functions.Progression;

// FIXME: SJ: this chart creator has been disabled in extension definition to force JFC mode for Cumulative chart and R mode for Density chart since the Density chart is not yet implemented in JFC charts engine  
/**
 * R progression cumulative chart creator.
 * 
 * @author sjacquot
 *
 */
public class RProgressionCumulativeChartCreator extends RProgressionBaseChartCreator {


	@Override
	public File createChart(Progression result, File file) {
		return super.createChart(result, file, true);
	}

}
