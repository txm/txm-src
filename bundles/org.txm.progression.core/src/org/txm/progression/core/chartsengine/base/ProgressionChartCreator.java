/**
 * 
 */
package org.txm.progression.core.chartsengine.base;

import java.util.ArrayList;

/**
 * Progression chart creator interface.
 * 
 * @author sjacquot
 *
 */
public interface ProgressionChartCreator {

	/**
	 * Gets the positions of the points currently selected in the Progression step chart.
	 * 
	 * @param chart
	 * @return
	 */
	public abstract ArrayList<Integer> getSelectedPointPositions(Object chart);

	/**
	 * Gets the numbers of item in series of the selected points.
	 * 
	 * @param chart
	 * @return
	 */
	public abstract ArrayList<Integer> getSelectedPointNumbers(Object chart);


	/**
	 * Gets the positions of all the points in the Progression step chart grouped by series.
	 * 
	 * @param chart
	 * @return
	 */
	public abstract ArrayList<ArrayList<Integer>> getAllPointPositionsBySeries(Object chart);


	/**
	 * Gets the selected series indexes.
	 * 
	 * @param chart
	 * @return
	 */
	public abstract ArrayList<Integer> getSelectedSeries(Object chart);

	/**
	 * Gets the last selected series.
	 * 
	 * @param chart
	 * @return
	 */
	public abstract int getLastSelectedSeries(Object chart);


}
