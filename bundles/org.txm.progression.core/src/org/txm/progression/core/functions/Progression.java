// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-09-19 10:31:00 +0200 (Mon, 19 Sep 2016) $
// $LastChangedRevision: 3298 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.progression.core.functions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang.StringUtils;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.r.core.themes.DefaultTheme;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.progression.core.messages.ProgressionCoreMessages;
import org.txm.progression.core.preferences.ProgressionPreferences;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.Selection;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * Progression similar to the progression of Weblex software.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class Progression extends ChartResult {


	/** The norep. */
	public static int norep = 1;

	/** The prefix r. */
	public static String prefixR = "Progression"; //$NON-NLS-1$

	public static final String DENSITY_CHART_TYPE = "density"; //$NON-NLS-1$

	/** The Xmin corpus. */
	protected int xMinCorpus;

	/** The Xmax corpus. */
	protected int xMaxCorpus;

	/** The writer. */
	private BufferedWriter writer;

	/** List of token position in the text. */
	protected List<int[]> allPositions;

	/** The structurepositions. */
	protected int[] structurePositions;

	/** The structurenames. */
	protected String[] structureNames;

	/** chart boundaries. */
	protected int maxY;

	/** The max x. */
	protected int maxX;


	/** The bande. */
	protected float bande = 0.0f;

	/**
	 * set this to skip the QueryResult build step
	 */
	protected List<QueryResult> queryResults;


	/**
	 * Queries.
	 */
	@Parameter(key = TXMPreferences.QUERIES)
	protected List<? extends IQuery> queries;

	/**
	 * Structural unit.
	 */
	@Parameter(key = TXMPreferences.STRUCTURAL_UNIT)
	protected StructuralUnit structuralUnit;

	/**
	 * Structural unit property.
	 */
	@Parameter(key = TXMPreferences.STRUCTURAL_UNIT_PROPERTY)
	protected StructuralUnitProperty structuralUnitProperty;

	/**
	 * Only used in Density mode.
	 */
	@Parameter(key = ProgressionPreferences.BANDE_MULTIPLIER)
	protected float bandeMultiplier;

	/**
	 * Regex to display only the property that match it.
	 */
	@Parameter(key = ProgressionPreferences.PROPERTY_REGEX)
	protected String propertyRegex;

	/**
	 * Repeats or not the same value when displaying the section markers.
	 */
	@Parameter(key = ProgressionPreferences.REPEAT_SAME_VALUES, type = Parameter.RENDERING)
	protected boolean repeatValues;



	/**
	 * Creates a not computed progression
	 * 
	 * @param parent
	 */
	public Progression(CQPCorpus parent) {
		this(null, parent);
	}

	/**
	 * Creates a not computed progression
	 * 
	 * @param parametersNodePath
	 */
	public Progression(String parametersNodePath) {
		super(parametersNodePath, null);
	}

	/**
	 * Creates a not computed progression.
	 * 
	 * @param parametersNodePath
	 * @param parent
	 */
	public Progression(String parametersNodePath, CQPCorpus parent) {
		super(parametersNodePath, parent);

		this.domainGridLinesVisible = false;
	}



	@Override
	public boolean loadParameters() {
		if (!this.getStringParameterValue(TXMPreferences.QUERIES).isEmpty()) {
			this.queries = Query.stringToQueries(this.getStringParameterValue(TXMPreferences.QUERIES));
		}

		if (!this.getStringParameterValue(TXMPreferences.STRUCTURAL_UNIT).isEmpty()) {
			try {
				this.structuralUnit = this.getCorpus().getStructuralUnit(this.getStringParameterValue(TXMPreferences.STRUCTURAL_UNIT));
				if (!this.getStringParameterValue(TXMPreferences.STRUCTURAL_UNIT_PROPERTY).isEmpty()) {
					this.structuralUnitProperty = this.structuralUnit.getProperty(this.getStringParameterValue(TXMPreferences.STRUCTURAL_UNIT_PROPERTY));
				}
			}
			catch (CqiClientException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// generate the chart type according to cumulative boolean preference
		if (!this.getBooleanParameterValue(ProgressionPreferences.CHART_CUMULATIVE)) {
			this.setChartType(Progression.DENSITY_CHART_TYPE);
		}

		return true;
	}


	@Override
	public boolean saveParameters() {

		if (this.queries != null && !this.queries.isEmpty()) {
			this.saveParameter(TXMPreferences.QUERIES, Query.queriesToJSON(this.queries));
		}

		if (this.structuralUnit != null) {
			this.saveParameter(TXMPreferences.STRUCTURAL_UNIT, this.structuralUnit.getName());
		}

		if (this.structuralUnitProperty != null) {
			this.saveParameter(TXMPreferences.STRUCTURAL_UNIT_PROPERTY, this.structuralUnitProperty.getName());
		}

		return true;
	}

	@Override
	public boolean canCompute() {

		// null or empty queries
		if (this.queries == null || this.queries.isEmpty()) {
			Log.fine("Progression.canCompute(): can not compute with no query."); //$NON-NLS-1$
			return false;
		}

		// wrong REGEX
		if (!this.propertyRegex.isEmpty()) {
			try {
				Pattern.compile(this.propertyRegex);
			}
			catch (PatternSyntaxException e) {
				Log.warning("Progression.canCompute(): wrong regular expression."); //$NON-NLS-1$
				Log.printStackTrace(e);
				return false;
			}
		}

		return true;
	}

	/**
	 * Sets the structural unit.
	 * 
	 * @param structuralUnit
	 */
	public void setStructuralUnit(StructuralUnit structuralUnit) {
		this.structuralUnit = structuralUnit;
		this.setStructuralUnitProperty(null);
	}

	/**
	 * Sets the structural unit property.
	 * 
	 * @param structuralUnitProperty
	 */
	public void setStructuralUnitProperty(StructuralUnitProperty structuralUnitProperty) {
		this.structuralUnitProperty = structuralUnitProperty;
	}

	public void setQueryResults(List<QueryResult> queryResults) {
		this.queryResults = queryResults;
	}

	/**
	 * 
	 * @param queries
	 * @param structuralUnit
	 * @param property
	 * @param propertyregex
	 * @param doCumulative
	 * @param lineWidth
	 * @param repeatValues
	 * @param bandeMultiplier
	 */
	// FIXME : SJ: all parameters related to rendering should be removed from this class and centralized in the charts engine system, eg. line width
	public void setParameters(List<Query> queries,
			StructuralUnit structuralUnit, StructuralUnitProperty property, String propertyregex,
			boolean doCumulative, int lineWidth, boolean repeatValues, float bandeMultiplier) {
		this.queries = queries; // the query of the selection
		this.structuralUnit = structuralUnit;
		this.structuralUnitProperty = property;
		this.propertyRegex = propertyregex;
		this.repeatValues = repeatValues;
		this.bandeMultiplier = bandeMultiplier;

		for (int i = 0; i < DefaultTheme.monoWidths.length; i++) {
			DefaultTheme.monoWidths[i] = lineWidth;
		}
	}

	/**
	 * Computes.
	 *
	 * @return true, if successful
	 * @throws CqiClientException the cqi client exception
	 * @throws CqiServerError
	 * @throws IOException
	 */
	@Override
	protected boolean __compute(TXMProgressMonitor monitor) throws Exception {

		// Queries
		if (this.hasParameterChanged(ProgressionPreferences.QUERIES)) {
			this.allPositions = new ArrayList<>();
			this.structurePositions = new int[0];
			this.structureNames = new String[0];

			monitor.setTask(ProgressionCoreMessages.ProcessingQueries);
			if (!this.stepQueries(monitor)) {
				return true;
			}

			if (monitor.isCanceled()) {
				return false;
			}
			monitor.worked(40);
		}

		// Structural units
		monitor.setTask(ProgressionCoreMessages.ProcessingStructuralUnits);
		if (!this.stepStructuralUnits(monitor)) {
			return false;
		}
		monitor.worked(40);


		// Finalization steps
		if (this.hasParameterChanged(ProgressionPreferences.QUERIES)) {
			monitor.setTask(ProgressionCoreMessages.Finalizing);
			if (!stepFinalize()) {
				return false;
			}
			monitor.worked(20);
		}

		return true;
	}

	/**
	 * Test discontinuity of a corpus. If the corpus is a subcorpus then test if the start-end of matches make no hole
	 * 
	 * @param corpus
	 * @return
	 */
	public static boolean canRunProgression(CQPCorpus corpus) {
		if (corpus.getMatches().size() == 1) {
			return true;
		}
		else {
			org.txm.objects.Match previousMatch = null;
			for (org.txm.objects.Match m : corpus.getMatches()) {
				if (previousMatch == null) {
					previousMatch = m;
					continue;
				}

				if (m.getStart() == previousMatch.getEnd() + 1) {
					previousMatch = m;
					continue;
				}
				else {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * compute using already resolved QueryResults
	 * 
	 * @param queryResults
	 * @return
	 * @throws CqiClientException
	 */
	public boolean stepQueries(List<QueryResult> queryResults) throws CqiClientException {
		if (queryResults.size() != queries.size()) return false;

		this.maxX = this.getCorpus().getSize();
		int npositions = 0;
		for (int iQuery = 0; iQuery < queries.size(); iQuery++) {
			QueryResult result = queryResults.get(iQuery);

			int nLines = result.getNMatch();
			if (this.maxY < nLines) {
				this.maxY = nLines;
			}

			List<Match> matches = null;
			if (nLines > 0) {
				matches = result.getMatches(0, nLines - 1); // get the indexes sequences of result's tokens
			}
			else {
				matches = new ArrayList<>();
			}
			// System.out.println("matches: "+matches);
			int[] positions = new int[matches.size()];
			// System.out.println("nb positions "+matches.size());
			// if (matches.size() > 0)
			this.allPositions.add(positions);
			int i = 0;
			for (Match m : matches) {
				positions[i++] = m.getStart();
				npositions++;
			}
		}
		return npositions > 0;
	}

	/**
	 * Step queries.
	 * 
	 * @throws Exception
	 */
	public boolean stepQueries(TXMProgressMonitor monitor) throws Exception {
		this.maxX = this.getCorpus().getSize();
		int npositions = 0;

		for (int iQuery = 0; iQuery < queries.size(); iQuery++) {
			Selection result = null;
			if (queryResults != null) {
				result = queryResults.get(iQuery);
			}
			else {
				IQuery query = queries.get(iQuery);
				// System.out.println("query "+query.getQueryString());
				result = query.getSearchEngine().query(getCorpus(), query, query.getQueryString().replace(" ", "_") + "_progression", true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				//result = getCorpus().query(query, query.getQueryString().replace(" ", "_") + "_progression", true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			int nLines = result.getNMatch();
			if (maxY < nLines) {
				maxY = nLines;
			}
			List<? extends org.txm.objects.Match> matches = null;
			if (nLines > 0) {
				matches = result.getMatches(0, nLines - 1); // get the indexes
				// sequences of result's tokens
			}
			else {
				matches = new ArrayList<>();
			}
			// System.out.println("matches: "+matches);
			int[] positions = new int[matches.size()];
			// System.out.println("nb positions "+matches.size());
			// if (matches.size() > 0)
			allPositions.add(positions);
			int i = 0;
			for (org.txm.objects.Match m : matches) {
				positions[i++] = m.getStart();
				npositions++;
				monitor.worked(1);
			}
		}
		return true;
	}

	/**
	 * Step structural units.
	 *
	 * @throws CqiClientException the cqi client exception
	 * @throws CqiServerError
	 * @throws IOException
	 */
	public boolean stepStructuralUnits(TXMProgressMonitor monitor) throws CqiClientException, IOException, CqiServerError {
		// structure query
		if (structuralUnit != null) {
			CQLQuery query = new CQLQuery("<" + structuralUnit.getName() + ">[]"); //$NON-NLS-1$ //$NON-NLS-2$
			if (structuralUnitProperty != null) {
				if (!propertyRegex.isEmpty()) {
					query = new CQLQuery("<" + structuralUnitProperty.getFullName() + "=\"" + propertyRegex + "\">[]"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				}
				else {
					query = new CQLQuery("<" + structuralUnitProperty.getFullName() + ">[]"); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}

			// System.out.println("Struct: "+ structure+" property: "+property);
			// System.out.println(query.getQueryString());
			QueryResult result = getCorpus().query(query, query.getQueryString().replace(" ", "_") + "_progression", true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			int[] starts = result.getStarts();
			result.drop();
			int nLines = starts.length;

			// List<Match> matches = new ArrayList<Match>();
			// if (nLines > 0)
			// matches = result.getMatches(0, nLines - 1); // get the indexes
			// sequences of result's tokens

			structurePositions = new int[nLines];
			structureNames = new String[nLines];



			Map<Property, List<String>> refValues = new HashMap<>();
			if (structuralUnitProperty != null) {
				List<String> values = CorpusManager.getCorpusManager().getCqiClient().getSingleData(structuralUnitProperty, starts);
				refValues.put(structuralUnitProperty, values);
			}

			int i = 0;
			String previousname = ""; //$NON-NLS-1$
			String currentname = ""; //$NON-NLS-1$
			int previousposition = -99999;
			int distmin = 999999;
			for (int m : starts) {
				if (structuralUnitProperty != null) {
					currentname = refValues.get(structuralUnitProperty).get(i);
				}
				else {
					currentname = structuralUnit.getName() + i;
				}

				// if (repeatValues) {
				structureNames[i] = currentname;
				structurePositions[i] = m;
				// }
				// else {
				// if (!previousname.equals(currentname)) {
				// structureNames[i] = currentname;
				// structurePositions[i] = m;
				// }
				// }
				if (i == 0) {
					structurePositions[i] += 1;
				}

				if (m - previousposition < distmin) {
					distmin = m - previousposition;
				}

				previousname = currentname;
				previousposition = m;

				i++;

				monitor.worked(1);
			}
			this.bande = distmin * this.bandeMultiplier;
		}
		return true;
	}

	/**
	 * Step finalize.
	 *
	 * @throws CqiClientException the cqi client exception
	 */
	public boolean stepFinalize() throws CqiClientException {
		if (this.getCorpus() instanceof Subcorpus) {
			List<? extends org.txm.objects.Match> matches = ((Subcorpus) getCorpus()).getMatches();
			if (matches.size() == 0) {
				Log.severe(ProgressionCoreMessages.errorColonSubcorpusWithSize0);
				return false;
			}

			xMinCorpus = matches.get(0).getStart();
			xMaxCorpus = matches.get(matches.size() - 1).getEnd();
		}
		else {
			xMinCorpus = 0;
			xMaxCorpus = getCorpus().getSize();
		}
		// System.out.println("min: "+XminCorpus+ "max: "+XmaxCorpus);

		if (bande <= 0) {
			bande = ((xMaxCorpus - xMinCorpus) / 100) * bandeMultiplier;
			if (bande == 0) {
				bande = 1.0f;
			}
		}
		return true;
	}



	/**
	 * Gets the max x.
	 *
	 * @return the max x
	 */
	public int getMaxX() {
		return maxX;
	}

	/**
	 * Gets the max y.
	 *
	 * @return the max y
	 */
	public int getMaxY() {
		return maxY;
	}

	/**
	 * Gets the corpus.
	 *
	 * @return the corpus
	 */
	public CQPCorpus getCorpus() {
		return (CQPCorpus) this.parent;
	}

	/**
	 * Gets the queries.
	 *
	 * @return the queries
	 */
	public List<? extends IQuery> getQueries() {
		return this.queries;
	}

	/**
	 * @return the allPositions
	 */
	public List<int[]> getAllPositions() {
		return allPositions;
	}


	/**
	 * Gets the positions counts.
	 *
	 * @return the positions counts
	 */
	public List<Integer> getPositionsCounts() {
		ArrayList<Integer> counts = new ArrayList<>();
		for (int[] list : allPositions) {
			counts.add(list.length);
		}
		return counts;
	}


	/**
	 * Checks if is repeatvalues.
	 *
	 * @return true, if is repeatvalues
	 */
	public boolean isRepeatingValues() {
		return repeatValues;
	}



	@Override
	public void clean() {
		try {
			this.writer.flush();
			this.writer.close();
		}
		catch (NullPointerException e) {
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	/**
	 * 
	 * @param outfile
	 * @param encoding
	 * @return
	 * @throws IOException
	 */
	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws IOException {

		// NK: writer declared as class attribute to perform a clean if the operation is interrupted
		tryAcquireSemaphore();
		this.writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outfile),
				encoding));
		for (int i = 0; i < queries.size(); i++) {
			writer.write("* " + queries.get(i) + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			for (int p : allPositions.get(i)) {
				writer.write("" + p + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		writer.close();
		releaseSemaphore();
		return true;
	}


	/**
	 * @return the structurePositions
	 */
	public int[] getStructurePositions() {
		return structurePositions;
	}

	/**
	 * @return the structureNames
	 */
	public String[] getStructureNames() {
		return structureNames;
	}


	/**
	 * @return the propertyRegex
	 */
	public String getPropertyRegex() {
		return propertyRegex;
	}

	/**
	 * @return the bande
	 */
	public float getBande() {
		return bande;
	}

	/**
	 * @return the xminCorpus
	 */
	public int getXminCorpus() {
		return xMinCorpus;
	}

	/**
	 * @return the xmaxCorpus
	 */
	public int getXmaxCorpus() {
		return xMaxCorpus;
	}


	@Override
	public String[] getExportTXTExtensions() {
		return new String[] { "*.txt" }; //$NON-NLS-1$
	}


	/**
	 * Computes and gets a query constructed from all the queries in alternative mode (with pipe |).
	 * 
	 * @return
	 */
	// FIXME: not used? even in synchronous editors (concordance and edition?)?
	public String getQueriesString() {
		ArrayList<Integer> allQueriesIndexes = new ArrayList<>(this.queries.size());
		for (int i = 0; i < this.queries.size(); i++) {
			allQueriesIndexes.add(i);
		}
		return this.getQueriesString(allQueriesIndexes);
	}

	/**
	 * Computes and gets a query constructed from all the queries specified by their index in alternative mode (with pipe |).
	 * 
	 * @param queriesIndexes
	 * @return
	 */
	public String getQueriesString(ArrayList<Integer> queriesIndexes) {

		String query = ""; //$NON-NLS-1$

		for (int i = 0; i < this.queries.size(); i++) {
			if (queriesIndexes.contains(i)) {
				String tmpQuery = this.queries.get(i).getQueryString();
				// Remove "[" and "]" only if it's a simple query as "[look]"
				if (tmpQuery.startsWith("[") && !tmpQuery.contains("=")) { //$NON-NLS-1$ //$NON-NLS-2$
					int length = tmpQuery.length();
					if (tmpQuery.endsWith("]")) { //$NON-NLS-1$
						length--;
					}
					tmpQuery = tmpQuery.substring(1, length);
				}
				if (query.length() > 0) {
					query += "|"; //$NON-NLS-1$
				}
				query += tmpQuery;
			}
		}

		return query;

	}


	@Override
	public String getDetails() {
		return this.getName();
	}

	@Override
	public String getName() {
		try {
			return this.parent.getSimpleName() + ": " + this.getSimpleName(); //$NON-NLS-1$
		}
		catch (Exception e) {
		}
		return ""; //$NON-NLS-1$
	}

	@Override
	public String getSimpleName() {
		try {
			if (this.queries == null || this.queries.isEmpty()) {
				return getEmptyName();
			}
			String output = StringUtils.join(this.queries, ","); //$NON-NLS-1$
			if (this.propertyRegex != null) {
				output += " " + this.propertyRegex; //$NON-NLS-1$
			}
			return output;
		}
		catch (Exception e) {
		}
		return this.getEmptyName();
	}


	@Override
	public String getComputingStartMessage() {
		return TXMCoreMessages.bind(ProgressionCoreMessages.progressionOfP0InTheP1Corpus, Query.toPrettyString(this.queries), this.getCorpus().getName());
	}


	@Override
	public String getComputingDoneMessage() {
		if (this.getAllPositions() == null || this.getAllPositions().isEmpty()) {
			return TXMCoreMessages.common_noResults;
		}
		else {
			String positions = String.valueOf(this.getAllPositions().get(0).length);
			for (int i = 1; i < this.getAllPositions().size(); i++) {
				positions += ", " + TXMCoreMessages.formatNumber(this.getAllPositions().get(i).length); //$NON-NLS-1$
			}
			return TXMCoreMessages.bind(TXMCoreMessages.common_P0Positions, positions);
		}
	}

	/**
	 * @return the bandeMultiplier
	 */
	public float getBandeMultiplier() {
		return bandeMultiplier;
	}

	/**
	 * @param bandeMultiplier the bandeMultiplier to set
	 */
	public void setBandeMultiplier(float bandeMultiplier) {
		this.bandeMultiplier = bandeMultiplier;
	}



	/**
	 * @param repeatValues the repeatValues to set
	 */
	public void setRepeatValues(boolean repeatValues) {
		this.repeatValues = repeatValues;
	}



	/**
	 * @return the structuralUnit
	 */
	public StructuralUnit getStructuralUnit() {
		return structuralUnit;
	}



	/**
	 * @return the structuralUnitProperty
	 */
	public StructuralUnitProperty getStructuralUnitProperty() {
		return structuralUnitProperty;
	}



	/**
	 * @param queries the queries to set
	 */
	public void setQueries(List<Query> queries) {
		this.queries = queries;
	}

	/**
	 * @param queries the queries to set
	 */
	public void setQueries(Query... queries) {
		this.queries = Arrays.asList(queries);
	}

	@Override
	public String getResultType() {
		return ProgressionCoreMessages.RESULT_TYPE;
	}


}
