package org.txm.progression.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Progression core messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ProgressionCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.progression.core.messages.messages"; //$NON-NLS-1$

	public static String RESULT_TYPE;

	public static String tEqualsP0;

	public static String occurrences;

	public static String progressionOf;

	public static String inP0;

	public static String density;

	public static String structureColon;

	public static String propertyColon;

	public static String filteringREGEXColon;

	public static String theProgressionCommandIsNotYetAvailableForDiscontinuousSubcorpora;


	public static String errorColonSubcorpusWithSize0;

	public static String Finalizing;

	public static String ProcessingQueries;

	public static String ProcessingStructuralUnits;

	public static String progressionOfP0InTheP1Corpus;


	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, ProgressionCoreMessages.class);
	}

	private ProgressionCoreMessages() {
	}
}
