package org.txm.progression.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Default preferences initializer.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ProgressionPreferences extends ChartsEnginePreferences {

	/**
	 * To repeat or not the same values of property area markers.
	 */
	public static final String REPEAT_SAME_VALUES = "repeat_same_values"; //$NON-NLS-1$

	/**
	 * REGEX used to filtering the property area markers.
	 */
	public static final String PROPERTY_REGEX = "property_regex"; //$NON-NLS-1$

	/** The Constant BANDE_MULTIPLIER. */
	public static final String BANDE_MULTIPLIER = "bande_multiplier"; //$NON-NLS-1$

	/**
	 * Type of chart: cumulative or density.
	 */
	public static final String CHART_CUMULATIVE = "cumulative_chart"; //$NON-NLS-1$



	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(ProgressionPreferences.class)) {
			new ProgressionPreferences();
		}
		return TXMPreferences.instances.get(ProgressionPreferences.class);
	}

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.putBoolean(CHART_CUMULATIVE, true);
		preferences.putBoolean(REPEAT_SAME_VALUES, false);
		preferences.putDouble(BANDE_MULTIPLIER, 1.0d);

		// FIXME: SJ: should find a way to disable functionality only for a specific charts engine, ie. here R  implementation doesn't manage the Show/Hide legend
		// disable unavailable functionalities
		//TXMPreferences.setEmpty(this.getPreferencesNodeQualifier(), SHOW_LEGEND);
	}
}
