package org.txm.progression.core.r;

import java.util.List;

import org.txm.chartsengine.core.ChartCreator;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.r.core.RChartsEngine;
import org.txm.progression.core.chartsengine.r.RProgressionBaseChartCreator;
import org.txm.progression.core.functions.Progression;
import org.txm.searchengine.core.IQuery;
import org.txm.statsengine.r.core.RTransformer;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

public class ProgressionRTransformer extends RTransformer<Progression> {

	/** The prefix r. */
	public static String prefixR = "Progression"; //$NON-NLS-1$

	@Override
	public Progression fromRtoTXM(String symbol) throws RWorkspaceException {
		return null;
	}

	@Override
	public String _fromTXMtoR(Progression o, String symbol) throws RWorkspaceException {

		ChartCreator<? extends ChartsEngine, ? extends ChartResult> cc = o.getChartCreator();
		if (cc.getChartsEngine() instanceof RChartsEngine) {
			RProgressionBaseChartCreator rpbcc = (RProgressionBaseChartCreator) cc;
			return rpbcc.getSymbol();
		}
		else {
			if (symbol == null || symbol.length() == 0) {
				symbol = Progression.prefixR + Progression.norep;
				Progression.norep++;
			}


			RWorkspace rw = RWorkspace.getRWorkspaceInstance();

			List<? extends IQuery> queries = o.getQueries();
			List<int[]> positions = o.getAllPositions();

			String[] names = new String[queries.size()];
			for (int i = 0; i < queries.size(); i++) {
				names[i] = queries.get(i).getQueryString();
			}

			rw.addVectorToWorkspace("tmpqueries", names); //$NON-NLS-1$
			rw.eval("tmppositions <- list();"); //$NON-NLS-1$
			//rw.eval(s+"$positions <- list();");
			for (int i = 0; i < queries.size(); i++) {
				rw.addVectorToWorkspace("TEMP", positions.get(i)); //$NON-NLS-1$
				rw.eval("tmppositions[[" + (i + 1) + "]] <- TEMP"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			rw.eval(symbol + " <- list(queries=tmpqueries, positions=tmppositions);"); //$NON-NLS-1$
			rw.eval("rm(tmpqueries, tmppositions);"); //$NON-NLS-1$
			//rw.addVectorToWorkspace(variableName, vector);
			return symbol;
		}

	}

}
