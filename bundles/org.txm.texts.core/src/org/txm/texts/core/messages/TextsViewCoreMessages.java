package org.txm.texts.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;


/**
 * Texts view UI Messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class TextsViewCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.texts.core.messages.messages"; //$NON-NLS-1$

	public static String RESULT_NAME;
	public static String RESULT_SIMPLE_NAME;

	public static String warning_theP0CorpusHasNoTextNamedP1;
	
	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, TextsViewCoreMessages.class);
	}


}
