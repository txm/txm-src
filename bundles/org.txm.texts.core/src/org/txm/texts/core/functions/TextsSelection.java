package org.txm.texts.core.functions;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.txm.core.results.Parameter;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.texts.core.preferences.TextsViewPreferences;
import org.txm.utils.TXMProgressMonitor;

/**
 * Structured subcorpus on the text structures
 * 
 * @author mdecorde
 *
 */
//FIXME: SJ, 2024-11-20: seems not used.
public class TextsSelection extends Subcorpus {

	@Parameter(key = TextsViewPreferences.CRITERII)
	HashMap<String, ArrayList<String>> criterii;

	@Parameter(key = TextsViewPreferences.ALL_PROPERTIES_MUST_MATCH)
	Boolean allPropertiesMustMatch;

	@Parameter(key = TextsViewPreferences.ALL_MUST_MATCH)
	HashMap<String, Boolean> allMustMatch;

	public TextsSelection(CQPCorpus corpus) {
		super(corpus);
	}

	/**
	 * 
	 * @param parametersNodePath
	 */
	public TextsSelection(String parametersNodePath) {
		super(parametersNodePath);
	}

	@Override
	public boolean canCompute() {
		return criterii.size() > 0;
	}

	@Override
	public boolean __compute(TXMProgressMonitor monitor) throws Exception {

		StringBuilder buffer = new StringBuilder();
		buffer.append("<text> ["); //$NON-NLS-1$
		int n = 0;
		
		for (String textprop : criterii.keySet()) {

			String operator = " && ";//$NON-NLS-1$
			if (allPropertiesMustMatch) operator = " || ";//$NON-NLS-1$

			if (n > 0) buffer.append(operator);

			String matchOperator = "&";//$NON-NLS-1$
			if (!allMustMatch.get(textprop)) matchOperator = "|";//$NON-NLS-1$

			buffer.append("_.text_" + textprop + "=\"" + StringUtils.join(criterii.get(textprop), matchOperator) + "\"");//$NON-NLS-1$ //$NON-NLS-2$// $NON-NLS-3$
		}
		buffer.append("] expand to text"); //$NON-NLS-1$

		this.pQuery = new CQLQuery(buffer.toString());

		return super.__compute(monitor);
	}
}
