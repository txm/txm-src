package org.txm.texts.core.functions;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.objects.CorpusBuild;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.InvalidCqpIdException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.texts.core.messages.TextsViewCoreMessages;
import org.txm.texts.core.preferences.TextsViewPreferences;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * 
 * @author mdecorde
 *
 */
public class TextsView extends TXMResult {

	@Parameter(key = TextsViewPreferences.COLUMNS)
	ArrayList<StructuralUnitProperty> pColumns;

	@Parameter(key = TextsViewPreferences.USE_TOKENS_IN_STATS)
	Boolean pStatsWithTokens;

	LinkedHashMap<String, Integer> counts;

	@Parameter(key = TextsViewPreferences.POSITIVE_FILTERS) //$NON-NLS-1$
	LinkedHashMap<String, String> pPositiveFilters;
	
	@Parameter(key = TextsViewPreferences.NEGATIVE_FILTERS) //$NON-NLS-1$
	LinkedHashMap<String, String> pNegativeFilters;
	
	@Parameter(key = TextsViewPreferences.SORT_BY_SIZE) //$NON-NLS-1$
	Boolean pSortBySize;

	private LinkedHashMap<String, ArrayList<String>> result;


	/**
	 * 
	 * @param corpus
	 */
	public TextsView(CQPCorpus corpus) {
		super(corpus);
		//hide the node and force the non-persistence since the object is not ready
		this.setVisible(false);
		this.setUserPersistable(false);
	}

	/**
	 * 
	 * @param parametersNodePath
	 */
	public TextsView(String parametersNodePath) {
		super(parametersNodePath);
		//hide the node and force the non-persistence since the object is not ready
		this.setVisible(false);
		this.setUserPersistable(false);
	}

	public boolean isBalanceWithTokens() {
		return pStatsWithTokens;
	}
	
	public void setIsBalanceWithTokens(boolean b) {
		pStatsWithTokens = b;
	}

	@Override
	public boolean saveParameters() throws Exception {

		String s = ""; //$NON-NLS-1$
		for (StructuralUnitProperty sup : pColumns) {
			if (s.length() > 0) s += "\t"; //$NON-NLS-1$
			s += sup.getFullName();
		}
		this.saveParameter(TextsViewPreferences.COLUMNS, s); //$NON-NLS-1$
		//		this.saveParameter("positiveFilters", StringUtils.join(positiveFilters, "\t"));

		Properties props = new Properties();
		props.putAll(pPositiveFilters);
		StringWriter sw = new StringWriter();
		props.store(sw, ""); //$NON-NLS-1$
		this.saveParameter(TextsViewPreferences.POSITIVE_FILTERS, sw.toString());
		
		Properties props2 = new Properties();
		props.putAll(pNegativeFilters);
		StringWriter sw2 = new StringWriter();
		props.store(sw2, ""); //$NON-NLS-1$
		this.saveParameter(TextsViewPreferences.NEGATIVE_FILTERS, sw2.toString());
		return true;
	}

	@Override
	public boolean loadParameters() throws Exception {

		String v = this.getStringParameterValue(TextsViewPreferences.COLUMNS).toString(); //$NON-NLS-1$
		if ("*".equals(v)) { //$NON-NLS-1$

			this.pColumns = new ArrayList<>(getCorpus().getTextStructuralUnit().getUserDefinedOrderedProperties()); //$NON-NLS-1$
			for (int i = 0; i < pColumns.size(); i++) {
				if (pColumns.get(i).getName().equals(CorpusBuild.ID)) { //$NON-NLS-1$
					pColumns.remove(i);
					break;
				}
			}
		}
		else {
			this.pColumns = new ArrayList<>(StructuralUnitProperty.stringToProperties(getCorpus(), v));
		}

		v = this.getStringParameterValue(TextsViewPreferences.POSITIVE_FILTERS).toString(); //$NON-NLS-1$
		this.pPositiveFilters = new LinkedHashMap<String, String>();
		if (v != null && v.length() > 0) {
			Properties props = new Properties();
			props.load(new StringReader(v));

			for (Object k : props.keySet())
				pPositiveFilters.put(k.toString(), props.get(k.toString()).toString());
		}
		
		v = this.getStringParameterValue(TextsViewPreferences.NEGATIVE_FILTERS).toString(); //$NON-NLS-1$
		this.pNegativeFilters = new LinkedHashMap<String, String>();
		if (v != null && v.length() > 0) {
			Properties props = new Properties();
			props.load(new StringReader(v));

			for (Object k : props.keySet())
				pNegativeFilters.put(k.toString(), props.get(k.toString()).toString());
		}

		return pColumns != null;
	}

	@Override
	public String getName() {
		return NLS.bind(TextsViewCoreMessages.RESULT_NAME, pColumns);
	}

	@Override
	public String getSimpleName() {
		return TextsViewCoreMessages.RESULT_SIMPLE_NAME;
	}

	@Override
	public String getDetails() {
		return getName();
	}

	@Override
	public void clean() {
		//nothing to do
	}

	@Override
	public boolean canCompute() throws Exception {
		return pColumns != null;
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		result = new LinkedHashMap<String, ArrayList<String>>();

		// preset arrays
		String[] textIds = getCorpus().getCorpusTextIdsList();

		for (String textId : textIds) {
			ArrayList<String> values = new ArrayList<String>();
			result.put(textId, values);

			if (this.getCorpus().getProject().getText(textId) == null) {
				Log.warning(NLS.bind(TextsViewCoreMessages.warning_theP0CorpusHasNoTextNamedP1, this.getCorpus(), textId));
				return false;
			}
		}

		// prepare data
		counts = new LinkedHashMap<>();

		for (String t : textIds) {
			counts.put(t, 0);
		}

		
		// optimization: get values property per property then set the Text property value arrays
		if (getCorpus() instanceof MainCorpus) {
			int[] idx = new int[textIds.length];
			for (int i = 0; i < textIds.length; i++) {
				idx[i] = i;
			}

			for (int icol = 0; icol < pColumns.size(); icol++) {
				StructuralUnitProperty col = pColumns.get(icol);

				String[] values = CQPSearchEngine.getCqiClient().struc2Str(col.getQualifiedName(), idx); // idx
				for (int i = 0; i < textIds.length; i++) {
					result.get(textIds[i]).add(values[i]);
				}
			}

		}
		else {
			int[] positions = getCorpus().getTextStartLimits();

			for (int icol = 0; icol < pColumns.size(); icol++) {
				StructuralUnitProperty col = pColumns.get(icol);

				int[] idx = CQPSearchEngine.getCqiClient().cpos2Struc(col.getQualifiedName(), positions);
				String[] values = CQPSearchEngine.getCqiClient().struc2Str(col.getQualifiedName(), idx);

				for (int i = 0; i < textIds.length; i++) {
					result.get(textIds[i]).add(values[i]);
				}

			}
		}

		computeCounts(textIds);

		computeColValues();

		if (pPositiveFilters.size() > 0 && pColumns.size() > 0) {

			ArrayList<String> columnNames = new ArrayList<>(pColumns.size());
			for (StructuralUnitProperty p : pColumns)
				columnNames.add(p.getName());

			ArrayList<String> keys = new ArrayList<>(result.keySet());
			for (String text : keys) {

				for (String positiveFilter : pPositiveFilters.keySet()) {
					if (columnNames.contains(positiveFilter)
							&& !result.get(text).get(columnNames.indexOf(positiveFilter)).matches(pPositiveFilters.get(positiveFilter))) {
						// remove this line if not matching with the positive
						result.remove(text);
						break;
					}
				}
			}
		}
		
		if (pNegativeFilters.size() > 0 && pColumns.size() > 0) {

			ArrayList<String> columnNames = new ArrayList<>(pColumns.size());
			for (StructuralUnitProperty p : pColumns)
				columnNames.add(p.getName());

			ArrayList<String> keys = new ArrayList<>(result.keySet());
			for (String text : keys) {

				for (String negativeFilter : pNegativeFilters.keySet()) {
					if (columnNames.contains(negativeFilter)
							&& result.get(text).get(columnNames.indexOf(negativeFilter)).matches(pNegativeFilters.get(negativeFilter))) {
						// remove this line if matching with the negative filter
						result.remove(text);
						break;
					}
				}
			}
		}
		
		return true;
	}

	LinkedHashMap<StructuralUnitProperty, ArrayList<String>> colValues = new LinkedHashMap<>();

	private void computeColValues() {

		colValues.clear();
		for (int i = 0; i < pColumns.size(); i++) {
			ArrayList<String> values = new ArrayList<String>();
			LinkedHashSet<String> added = new LinkedHashSet<String>();
			colValues.put(pColumns.get(i), values);

			for (ArrayList<String> line : getLines().values()) {
				added.add(line.get(i));
			}
			values.addAll(added);
		}
	}

	public ArrayList<String> getColValues(StructuralUnitProperty col) {
		return colValues.get(col);
	}

	
	public CQPCorpus getCorpus() {
		return (CQPCorpus) parent;
	}


	
	private void computeCounts(String[] texts) throws IOException, CqiServerError, CqiClientException, InvalidCqpIdException {

		int[] starts = getCorpus().getTextStartLimits();
		int[] ends = getCorpus().getTextEndLimits();

		for (int i = 0; i < texts.length; i++) {
			int amount = 1;
			if (pStatsWithTokens) {
				amount = ends[i] - starts[i];
			}

			counts.put(texts[i], amount);
		}

		//		LinkedHashMap<String, Integer> tmp = counts.get(col);
		//		ArrayList<String> percentsValues = new ArrayList<>(tmp.keySet());
		//		Collections.sort(percentsValues, new Comparator<String>() {
		//
		//			@Override
		//			public int compare(String o1, String o2) {
		//				return tmp.get(o2) - tmp.get(o1);
		//			}
		//		});
		//		LinkedHashMap<String, Integer> newTmp = new LinkedHashMap<>();
		//		for (String value : percentsValues) {
		//			newTmp.put(value, tmp.get(value));
		//		}
		//		counts.put(col, newTmp);
	}

	@Override
	protected boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {

		PrintWriter writer = org.txm.utils.io.IOUtils.getWriter(outfile, encoding);

		writer.println("text" + colseparator + StringUtils.join(pColumns, colseparator)); //$NON-NLS-1$

		for (String text : result.keySet()) {
			writer.println(text + colseparator + StringUtils.join(result.get(text), colseparator));
		}

		writer.close();
		return false;
	}

	@Override
	public String getResultType() {
		return "Texts View"; //$NON-NLS-1$
	}

	public LinkedHashMap<String, ArrayList<String>> getLines() {
		return result;
	}

	public List<StructuralUnitProperty> getColumns() {

		return pColumns;
	}

	public LinkedHashMap<String, Integer> getCounts() {
		return this.counts;
	}

	public boolean sortBySize() {
		return pSortBySize;
	}
}
