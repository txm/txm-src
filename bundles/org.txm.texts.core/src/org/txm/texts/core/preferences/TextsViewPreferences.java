package org.txm.texts.core.preferences;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * 
 * Default preferences initializer of the TextsView command.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class TextsViewPreferences extends TXMPreferences {

	public static final String COLUMNS = "columns"; //$NON-NLS-1$

	
	public static final String SORT_BY_SIZE = "sort_by_size"; //$NON-NLS-1$
	
	public static final String NEGATIVE_FILTERS = "negative_filters"; //$NON-NLS-1$
	
	public static final String POSITIVE_FILTERS = "positive_filters"; //$NON-NLS-1$
	
	public static final String CRITERII = "criterii"; //$NON-NLS-1$

	public static final String ALL_MUST_MATCH = "all_must_match"; //$NON-NLS-1$

	public static final String ALL_PROPERTIES_MUST_MATCH = "all_properties_must_match"; //$NON-NLS-1$

	public static final String USE_TOKENS_IN_STATS = "use_tokens_in_stats"; //$NON-NLS-1$
	
	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		
		if (!TXMPreferences.instances.containsKey(TextsViewPreferences.class)) {
			new TextsViewPreferences();
		}
		return TXMPreferences.instances.get(TextsViewPreferences.class);
	}
	
	@Override
	public void initializeDefaultPreferences() {
		
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.put(TextsViewPreferences.COLUMNS, "*"); //$NON-NLS-1$
		preferences.putBoolean(TextsViewPreferences.USE_TOKENS_IN_STATS, true);
		preferences.putBoolean(TextsViewPreferences.SORT_BY_SIZE, false);
	}
	
}
