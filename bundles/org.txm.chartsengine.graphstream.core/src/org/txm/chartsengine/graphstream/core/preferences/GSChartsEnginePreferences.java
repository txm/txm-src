package org.txm.chartsengine.graphstream.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.chartsengine.graphstream.core.GSChartsEngine;
import org.txm.core.preferences.TXMPreferences;

/**
 * Preferences initializer and manager.
 * 
 * @author sjacquot
 *
 */
public class GSChartsEnginePreferences extends TXMPreferences {


	public final static String PREFERENCES_PREFIX = GSChartsEngine.NAME + "_"; //$NON-NLS-1$

	public static final String OUTPUT_FORMAT = PREFERENCES_PREFIX + "output_format"; //$NON-NLS-1$


	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(GSChartsEnginePreferences.class)) {
			new GSChartsEnginePreferences();
		}
		return TXMPreferences.instances.get(GSChartsEnginePreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences defaultPreferences = this.getDefaultPreferencesNode();
		defaultPreferences.put(OUTPUT_FORMAT, GSChartsEngine.OUTPUT_FORMAT_GRAPHSTREAM);
	}

}
