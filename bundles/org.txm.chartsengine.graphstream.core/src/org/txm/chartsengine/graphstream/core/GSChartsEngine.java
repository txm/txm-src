/**
 * 
 */
package org.txm.chartsengine.graphstream.core;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.graphstream.core.preferences.GSChartsEnginePreferences;
import org.txm.utils.logger.Log;

/**
 * GraphStream charts engine.
 * 
 * @author sjacquot
 *
 */
public class GSChartsEngine extends ChartsEngine {


	/**
	 * Constants for extra output formats.
	 */
	public final static String OUTPUT_FORMAT_GRAPHSTREAM = "GraphStream/Java2D"; //$NON-NLS-1$


	/**
	 * The charts engine internal name.
	 */
	public final static String NAME = "graphstream_charts_engine"; //$NON-NLS-1$

	/**
	 * The charts engine description.
	 */
	public final static String DESCRIPTION = "GraphStream";



	/**
	 * Creates a GraphStream charts engine with output format from the preferences.
	 */
	public GSChartsEngine() {
		this(GSChartsEnginePreferences.getInstance().getString(GSChartsEnginePreferences.OUTPUT_FORMAT));
	}


	/**
	 * Creates a GraphStream charts engine with the specified output format.
	 * 
	 * @param outputFormat
	 */
	public GSChartsEngine(String outputFormat) {
		super(DESCRIPTION, outputFormat);
	}



	/*
	 * (non-Javadoc)
	 * @see org.txm.core.engines.Engine#getName()
	 */
	@Override
	public String getName() {
		return GSChartsEngine.NAME;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.core.engines.Engine#getState()
	 */
	@Override
	public boolean isRunning() {
		// TODO Auto-generated method stub
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.core.engines.Engine#start(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		// TODO Auto-generated method stub
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.core.engines.Engine#stop()
	 */
	@Override
	public boolean stop() throws Exception {
		// TODO Auto-generated method stub
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.chartsengine.core.ChartsEngine#exportChart(java.lang.Object, java.io.File, java.lang.String, int, int, int, int, int, int)
	 */
	@Override
	public File exportChartToFile(Object chart, File file, String outputFormat, int imageWidth, int imageHeight, int drawingAreaX, int drawingAreaY, int drawingAreaWidth, int drawingAreaHeight) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ArrayList<String> getSupportedOutputDisplayFormats() {
		ArrayList<String> formats = new ArrayList<>(1);

		formats.add(OUTPUT_FORMAT_GRAPHSTREAM);

		return formats;
	}


	@Override
	public ArrayList<String> getSupportedOutputFileFormats() {
		Log.fine("ECEChartsEngine.getSupportedOutputFileFormats(): not yet implemented.");
		ArrayList<String> formats = new ArrayList<>(1);
		formats.add(OUTPUT_FORMAT_NONE);
		return formats;

	}

	@Override
	public File exportChartResultToFile(ChartResult result, File file, int imageWidth, int imageHeight, String outputFormatSvg) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public File exportChartResultToFile(ChartResult result, int imageWidth, int imageHeight, String outputFormat) {
		// TODO Auto-generated method stub
		return null;
	}

}
