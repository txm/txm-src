package org.txm.chartsengine.graphstream.core;

import org.graphstream.graph.Graph;
import org.txm.chartsengine.core.ChartCreator;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.utils.logger.Log;

/**
 * GraphStream base chart creator.
 * 
 * All GSC charts creators should extend this class.
 * The updateChart(ChartResult result) method implementation of the subclasses must call super.update(result) to benefit to the shared system.
 * 
 * @author sjacquot
 *
 */
public abstract class GSChartCreator<R extends ChartResult> extends ChartCreator<GSChartsEngine, R> {

	@Override
	public void updateChart(R result) {
		// Java object
		if (result.getChart() instanceof Graph) {
			Graph graph = (Graph) result.getChart();


			// FIXME: Debug
			System.out.println("GSChartCreator.updateChart() style sheet dump: " + graph.getAttribute("ui.stylesheet")); //$NON-NLS-1$

			// Add the current shared preferences as Font size, etc.
			// graph.addAttribute("ui.stylesheet", graph.getAttribute("ui.stylesheet") + "node {"
			// + " text-size: " + result.getFont().split("\\|")[2] + ";"
			// + " text-font: \"" + result.getFont().split("\\|")[1] + "\";"
			//
			// + " }");

			// FIXME: Debug
			System.out.println("GSChartCreator.updateChart() style sheet dump: " + graph.getAttribute("ui.stylesheet")); //$NON-NLS-1$



			// // freeze rendering
			// chart.setNotify(false);
			//
			//
			// // applying full theme
			// if(applyTheme) {
			// this.getChartsEngine().getJFCTheme().applyThemeToChart(result);
			// }
			//
			//
			// // rendering color mode
			// this.getChartsEngine().getJFCTheme().applySeriesPaint(result);
			//
			// // multiple line strokes
			// this.getChartsEngine().getJFCTheme().applySeriesStrokes(result);
			//
			// // title visibility
			// if(chart.getTitle() != null && result.hasParameterChanged(ChartsEnginePreferences.SHOW_TITLE)) {
			// chart.getTitle().setVisible(result.isTitleVisible());
			// }
			// // legend visibility
			// if(chart.getLegend() != null && result.hasParameterChanged(ChartsEnginePreferences.SHOW_LEGEND)) {
			// chart.getLegend().setVisible(result.isLegendVisible());
			// }
			// // CategoryPlot
			// if(chart.getPlot() instanceof CategoryPlot) {
			// CategoryPlot plot = (CategoryPlot) chart.getPlot();
			// // grid visibility
			// plot.setDomainGridlinesVisible(result.isGridVisible());
			// plot.setRangeGridlinesVisible(result.isGridVisible());
			//
			// // force the renderer initialization to override theme settings
			// if(plot.getRenderer() instanceof IRendererWithItemSelection) {
			// ((IRendererWithItemSelection) plot.getRenderer()).init();
			// }
			// }
			// // XYPlot
			// else if(chart.getPlot() instanceof XYPlot) {
			// XYPlot plot = (XYPlot) chart.getPlot();
			// // grid visibility
			// plot.setRangeGridlinesVisible(result.isGridVisible());
			// plot.setDomainGridlinesVisible(result.isGridVisible());
			//
			// // force the renderer initialization to override theme settings
			// if(plot.getRenderer() instanceof IRendererWithItemSelection) {
			// ((IRendererWithItemSelection) plot.getRenderer()).init();
			// }
			// }
			//
			// chart.setNotify(true);
		}
		else {
			Log.severe("Error: GSChartCreator only manage Graph charts: " + result.getChart());
		}
		// // File
		// else if(result.getChart() instanceof File) {
		// // creates a new chart but using the same file
		// this.createChartFile(result, (File)result.getChart());
		//
		// // FIXME: using new file
		// //this.createChartFile(result, preferencesNode);
		//
		// }
	}

	@Override
	public Class<GSChartsEngine> getChartsEngineClass() {
		return GSChartsEngine.class;
	}
}
