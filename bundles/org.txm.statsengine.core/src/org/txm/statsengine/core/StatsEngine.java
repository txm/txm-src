package org.txm.statsengine.core;

import org.txm.core.engines.Engine;
import org.txm.core.results.TXMResult;

public abstract class StatsEngine implements Engine {

	public static final String EXTENSION_POINT_ID = "org.txm.statsengine.core.StatsEngine"; //$NON-NLS-1$

	@Override
	public abstract String getName();

	@Override
	public void notify(TXMResult r, String state) {
		return;
	}

	@Override
	public String getDetails() {
		return ""; //$NON-NLS-1$
	}

	public String hasAdditionalDetailsForResult(TXMResult result) {
		return null;
	}

	public String getAdditionalDetailsForResult(TXMResult result) {
		return null;
	}
}
