package org.txm.statsengine.core;

import org.txm.Toolbox;
import org.txm.core.engines.EngineType;
import org.txm.core.engines.EnginesManager;

/**
 * Statistics engines manager.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
//FIXME: useless class, the type and priority/order should be defined in org.txm.core.engines.EnginesManager extension point
public class StatsEnginesManager extends EnginesManager<StatsEngine> {

	private static final long serialVersionUID = 1768281732799460015L;

	public static StatsEngine getREngine() {
		return (StatsEngine) Toolbox.getEngineManager(EngineType.STATS).getEngine("R"); //$NON-NLS-1$
	}

	@Override
	public boolean fetchEngines() {
		return this.fetchEngines(StatsEngine.EXTENSION_POINT_ID);
	}

	@Override
	public EngineType getEnginesType() {
		return EngineType.STATS;
	}
}
