// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.core.data;

import java.util.List;

import org.txm.statsengine.core.StatException;

// TODO: Auto-generated Javadoc
/**
 * The interface Vector.
 * 
 * @author sloiseau
 */
public interface Vector extends QuantitativeDataStructure {

	/**
	 * Get the sub-vector given index.
	 * 
	 * @param index
	 *            0-based index.
	 * 
	 * @return a new vector, copy to the selected values (modif ication to the
	 *         vector returned does not modif y the vector).
	 * 
	 * @throws StatException
	 *             if anything goes wrong.
	 */
	public Vector get(int[] index) throws StatException;

	/**
	 * Convert the vector to a java array of <code>int</code>, if possible.
	 * 
	 * @return a java array of <code>int</code>.
	 * 
	 * @throws StatException
	 *             if anything goes wrong, ni particular if this Vector cannot
	 *             be converted into <code>int</code>.
	 */
	public int[] asIntArray() throws StatException;

	/**
	 * Convert the vector to a java array of <code>double</code>, if possible.
	 * 
	 * @return a java array of <code>double</code>.
	 * 
	 * @throws StatException
	 *             if anything goes wrong, ni particular if this Vector cannot
	 *             be converted into <code>double</code>.
	 */
	public double[] asDoubleArray() throws StatException;

	/**
	 * Convert the vector to a java array of <code>String</code>, if possible.
	 * 
	 * @return a java array of <code>String</code>.
	 * 
	 * @throws StatException
	 *             if anything goes wrong, ni particular if this Vector cannot
	 *             be converted into <code>String</code>.
	 */
	public String[] asStringsArray() throws StatException;

	/**
	 * Get the length of the vector.
	 * 
	 * @return the length.
	 * 
	 * @throws StatException
	 *             if anything goes wrong.
	 */
	public int getLength() throws StatException;

	/**
	 * Removes the.
	 *
	 * @param col the col
	 * @return true, if successful
	 */
	public boolean remove(int col);

	/**
	 * Sets the string.
	 *
	 * @param i the i
	 * @param value the value
	 */
	public void setString(int i, String value);

	/**
	 * Sets the int.
	 *
	 * @param i the i
	 * @param value the value
	 */
	public void setInt(int i, int value);

	/**
	 * Sets the double.
	 *
	 * @param i the i
	 * @param value the value
	 */
	public void setDouble(int i, double value);

	/**
	 * Sort.
	 *
	 * @param reverse the reverse
	 */
	public void sort(Boolean reverse);

	/**
	 * Sets the order.
	 *
	 * @param neworder the neworder
	 * @param reverse the reverse
	 */
	public void setOrder(List<Integer> neworder, Boolean reverse);

	/**
	 * Cut after n lines.
	 *
	 */
	public void cut(int before, int after);

	// public String get(int i);

}
