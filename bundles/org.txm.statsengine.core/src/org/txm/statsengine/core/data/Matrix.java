// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.core.data;

import org.txm.statsengine.core.StatException;

// TODO: Auto-generated Javadoc
/**
 * The Interface Matrix, root of all other matrix-like structure.
 * 
 * @author sloiseau
 */
public interface Matrix extends QuantitativeDataStructure {

	/**
	 * Get the value at the specif ied cell.
	 *
	 * @param row 0-based row index
	 * @param col 0-based column index
	 * @return the value as a <code>double</code>
	 * @throws StatException the stat exception
	 */
	public double get(int row, int col) throws StatException;

	/**
	 * The number of rows in the contingency table.
	 * 
	 * @return the number of raw
	 */
	public abstract int getNRows();

	/**
	 * Exchange 2 columns in the lexical table
	 * 
	 * @param c1 1..N+1
	 * @param c2 1..N+1
	 */
	public abstract void exchangeColumns(int c1, int c2);

	/**
	 * The number of columns in the contingency table.
	 * 
	 * @return the number of columns
	 */
	public abstract int getNColumns();

	/**
	 * Get the row names or null if no row names.
	 * 
	 * Row names are available if the
	 *
	 * @return the row names
	 */
	public abstract Vector getRowNames();

	/**
	 * Get the col names or null if no col names.
	 * 
	 * Col names are available if the
	 *
	 * @return the col names
	 */
	public abstract Vector getColNames();

	/**
	 * Gets a row vector as a Vector object.
	 *
	 * @param index 0-based row index.
	 * @return the row
	 * @throws StatException the stat exception
	 */
	public abstract Vector getRow(int index) throws StatException;

	/**
	 * Gets a column vector as a Vector object.
	 *
	 * @param index 0-based index column index.
	 * @return the row
	 * @throws StatException the stat exception
	 */
	public abstract Vector getCol(int index) throws StatException;

	/**
	 * Gets a column vector as a Vector object.
	 *
	 * @param column the column name
	 * @return the column vector
	 * @throws StatException the stat exception
	 */
	public abstract Vector getCol(String column) throws StatException;

	/**
	 * Gets a row vector as a Vector object.
	 *
	 * @param row the row name
	 * @return the row vector
	 * @throws StatException the stat exception
	 */
	public abstract Vector getRow(String row) throws StatException;

}
