// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.core.utils;

import org.txm.statsengine.core.messages.StatsEngineCoreMessages;


// TODO: Auto-generated Javadoc
/**
 * Static methods for dealing vectorization of Arrays.
 * 
 * @author sloiseau
 * 
 */
public final class VectorizeArray {

	/**
	 * Convert a <code>double * double</code> array in a <code>double</code>
	 * array. Each inner array of the argument is added one after the other into
	 * the result array.
	 * 
	 * <pre>
	 * [
	 * [1, 2, 3],
	 * [4, 5, 6]
	 * ]
	 * </pre>
	 * 
	 * result in :
	 * 
	 * <pre>
	 * [1, 2, 3, 4, 5, 6]
	 * </pre>
	 * 
	 * that is, "row first".
	 *
	 * @param matrix a <code>double * double</code> array where all inner arrays
	 *            have the same length.
	 * @return an array of <code>double</code> produced by vectorization of the
	 *         matrix.
	 */
	public final static double[] vectorizeByInner(double[][] matrix) {
		if (matrix == null || matrix.length == 0) {
			throw new IllegalArgumentException(StatsEngineCoreMessages.nothingToDoWithAnEmptyMatrix);
		}
		int inner_length = matrix[0].length;
		if (inner_length < 1) {
			throw new IllegalArgumentException(StatsEngineCoreMessages.innerArraysMustBeOfDimensionSup0);
		}
		double[] vector = new double[matrix.length * matrix[0].length];
		for (int i = 0; i < matrix.length; i++) {
			if (matrix[i] == null) {
				throw new IllegalArgumentException(StatsEngineCoreMessages.anInnerArrayCannotBeNull);
			}
			if (inner_length != matrix[i].length) {
				throw new IllegalArgumentException(StatsEngineCoreMessages.allTheInnerArrayDoNotHaveTheSameLength);
			}
			System.arraycopy(matrix[i], 0, vector, i * inner_length,
					inner_length);
		}
		return vector;
	}

	/**
	 * Convert a <code>int * int</code> array in a <code>double</code> array.
	 * Each inner array of the argument is added one after the other into the
	 * result array.
	 * 
	 * <pre>
	 * [
	 * [1, 2, 3],
	 * [4, 5, 6]
	 * ]
	 * </pre>
	 * 
	 * result in :
	 * 
	 * <pre>
	 * [1, 2, 3, 4, 5, 6]
	 * </pre>
	 * 
	 * that is, "row first".
	 *
	 * @param matrix a <code>int * int</code> array where all inner arrays have the
	 *            same length.
	 * @return an array of <code>int</code> produced by vectorization of the
	 *         matrix.
	 */
	public final static int[] vectorizeByInner(int[][] matrix) {
		if (matrix == null || matrix.length == 0) {
			//throw new IllegalArgumentException(StatsEngineCoreMessages.nothingToDoWithAnEmptyMatrix);
			return new int[0];
		}
		int inner_length = matrix[0].length;
		return vectorizeByInner(matrix, inner_length);
	}

	/**
	 * Convert a <code>int * int</code> array in a <code>double</code> array.
	 * Each inner array of the argument is added one after the other into the
	 * result array.
	 * 
	 * <pre>
	 * [
	 * [1, 2, 3],
	 * [4, 5, 6]
	 * ]
	 * </pre>
	 * 
	 * result in :
	 * 
	 * <pre>
	 * [1, 2, 3, 4, 5, 6]
	 * </pre>
	 * 
	 * that is, "row first".
	 *
	 * @param matrix a <code>int * int</code> array where all inner arrays have the
	 *            same length.
	 * @return an array of <code>int</code> produced by vectorization of the
	 *         matrix.
	 */
	public final static int[] vectorizeByInner(int[][] matrix, int inner_length) {

		if (inner_length < 1) {
			throw new IllegalArgumentException(StatsEngineCoreMessages.innerArraysMustBeOfDimensionSup0);
		}
		int[] vector = new int[matrix.length * inner_length];
		for (int i = 0; i < matrix.length; i++) {
			if (matrix[i] == null) {
				throw new IllegalArgumentException(StatsEngineCoreMessages.anInnerArrayCannotBeNull);
			}
			if (inner_length != matrix[i].length) {
				throw new IllegalArgumentException(StatsEngineCoreMessages.allTheInnerArrayDoNotHaveTheSameLength);
			}
			System.arraycopy(matrix[i], 0, vector, i * inner_length, inner_length);
		}
		return vector;
	}

	/**
	 * Convert a <code>double * double</code> array in a <code>double</code>
	 * array. The elements of the input array are assembled in the output array
	 * so that elements of same index in the outer array are consecutive. So :
	 * 
	 * <pre>
	 * [
	 * [1, 2, 3],
	 * [4, 5, 6]
	 * ]
	 * </pre>
	 * 
	 * result in :
	 * 
	 * <pre>
	 * [1, 4, 2, 5, 3, 6]
	 * </pre>
	 * 
	 * that is, "column first".
	 *
	 * @param matrix a <code>double * double</code> array where all inner arrays
	 *            have the same length.
	 * @return an array of <code>double</code> produced by vectorization of the
	 *         matrix.
	 */
	public final static double[] vectorizeByOuter(double[][] matrix) {
		if (matrix == null || matrix.length == 0) {
			//throw new IllegalArgumentException(StatsEngineCoreMessages.nothingToDoWithAnEmptyMatrix);
			return new double[0];
		}
		int inner_length = matrix[0].length;
		int outer_length = matrix.length;
		if (inner_length < 1) {
			throw new IllegalArgumentException(StatsEngineCoreMessages.innerArraysMustBeOfDimensionSup0);
		}
		double[] vector = new double[matrix.length * matrix[0].length];
		for (int i = 0; i < matrix.length; i++) {
			if (matrix[i] == null) {
				throw new IllegalArgumentException(StatsEngineCoreMessages.anInnerArrayCannotBeNull);
			}
			if (inner_length != matrix[i].length) {
				throw new IllegalArgumentException(StatsEngineCoreMessages.allTheInnerArrayDoNotHaveTheSameLength);
			}
			for (int j = 0; j < matrix[i].length; j++) {
				vector[(j * outer_length) + i] = matrix[i][j];
			}

		}
		return vector;
	}

}
