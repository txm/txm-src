// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.core.utils;

// TODO: Auto-generated Javadoc
/**
 * Various utility methods for checking consistency of matrix as java array of
 * arrays.
 * 
 * @author sloiseau
 * 
 */
public final class CheckArray {

	/**
	 * Check if an array is not null (or 0-length), does not include null inner
	 * array (or 0 length), and if all inner arrays have the same length.
	 *
	 * @param array the array
	 * @return <code>true</code> if the condition are satisfied,
	 *         <code>false</code> otherwise.
	 */
	public final static boolean checkMatrixRepresentation(int[][] array) {
		// same code as in #checkMatrixRepresentation(double[][])
		if (array == null)
			return false;
		int length = array.length;
		if (length < 1)
			return false;
		if (array[0] == null)
			return false;
		int firstElementLength = array[0].length;
		if (firstElementLength == 0)
			return false;
		for (int i = 0; i < array.length; i++) {
			if (array[i] == null)
				return false;
			if (array[i].length != firstElementLength)
				return false;
		}
		return true;
	}

	/**
	 * Check if an array is not null (or 0-length), does not include null inner
	 * array (or 0 length), and if all inner arrays have the same length.
	 *
	 * @param array the array
	 * @return <code>true</code> if the condition are satisfied,
	 *         <code>false</code> otherwise.
	 */
	public final static boolean checkMatrixRepresentation(double[][] array) {
		// same code as in #checkMatrixRepresentation(int[][])
		if (array == null)
			return false;
		int length = array.length;
		if (length < 1)
			return false;
		if (array[0] == null)
			return false;
		int firstElementLength = array[0].length;
		if (firstElementLength == 0)
			return false;
		for (int i = 0; i < array.length; i++) {
			if (array[i] == null)
				return false;
			if (array[i].length != firstElementLength)
				return false;
		}
		return true;
	}

}
