// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.core.utils;

import java.util.Arrays;

// TODO: Auto-generated Javadoc
/**
 * Utility methods for testing for equality of arrays.
 * 
 * @author sloiseau
 * 
 */
public final class ArrayEquals {

	/**
	 * Test for equality of both array. For consistency with Arrays.equals
	 * contract, the two arrays are equals if both are <code>null</code>.
	 *
	 * @param array1 the array1
	 * @param array2 the array2
	 * @return <code>true</code> if (1) both arrays are <code>null</code> or if
	 *         (2) both arrays have the same length and contain pair of arrays
	 *         equals to each other according to
	 *         {@link Arrays#equals(int[], int[])}.
	 */
	public static final boolean deepEquals(int[][] array1, int[][] array2) {
		if (array1 == null && array2 == null)
			return true;
		if (array1 == null || array2 == null)
			return false;
		if (array1.length != array2.length)
			return false;
		for (int i = 0; i < array2.length; i++) {
			if (!Arrays.equals(array1[i], array2[i]))
				return false;
		}
		return true;
	}

}
