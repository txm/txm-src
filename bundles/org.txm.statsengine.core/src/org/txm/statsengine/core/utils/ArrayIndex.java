// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.core.utils;

import java.util.HashMap;
import java.util.Map;

import org.txm.statsengine.core.messages.StatsEngineCoreMessages;

// TODO: Auto-generated Javadoc
/**
 * Utilities for managing index of vector.
 * 
 * @author sloiseau
 * 
 */
public final class ArrayIndex {

	/**
	 * Convert an array of zero-based index to an array of one-base index, that
	 * is, add <code>1</code> to all elements.
	 * 
	 * This method check every index (see exception thrown).
	 *
	 * @param zero_based the zero based index.
	 * @param length the number of elements that can be indexed.
	 * @return the one base index.
	 */
	public static final int[] zeroToOneBasedIndex(int[] zero_based, int length) {
		if (zero_based == null || zero_based.length == 0) {
			throw new IllegalArgumentException(StatsEngineCoreMessages.theIndexArrayCanNotBeNullOrEmpty);
		}
		int[] one_based = new int[zero_based.length];
		for (int i = 0; i < zero_based.length; i++) {
			if (zero_based[i] < 0 || zero_based[i] >= length) {
				throw new IllegalArgumentException(StatsEngineCoreMessages.aZerobasedIndexCannotBeInf0OrSupToTheNumberOfIndexedElements);
			}
			one_based[i] = zero_based[i] + 1;
		}
		return one_based;
	}

	/**
	 * Zero to one based index.
	 *
	 * @param zero_based the zero_based
	 * @return the int
	 */
	public static final int zeroToOneBasedIndex(int zero_based) {
		return zero_based + 1;
	}

	/**
	 * Get the index (0-based) of the element of subvector into vector. Return
	 * <code>-1</code> when no element in vector correspond to the element in
	 * subvector.
	 *
	 * @param vector the vector
	 * @param subvector the subvector
	 * @return the index
	 */
	public static int[] getIndex(String[] vector, String[] subvector) {
		int[] result = new int[subvector.length];
		Map<String, Integer> index = new HashMap<String, Integer>();
		for (int i = 0; i < subvector.length; i++) {
			index.put(subvector[i], i);
			result[i] = -1;
		}
		for (int i = 0; i < vector.length; i++) {
			if (index.containsKey(vector[i])) {
				result[index.get(vector[i])] = i;
			}
		}
		return result;
	}
}
