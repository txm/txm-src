// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.core.utils;

import java.util.Arrays;

// TODO: Auto-generated Javadoc
/**
 * Various utilities method for printing arrays.
 * 
 * @author sloiseau
 * 
 */
public final class PrintArray {

	/**
	 * Double array.
	 *
	 * @param matrix the matrix
	 * @return the string
	 */
	public final static String doubleArray(double[][] matrix) {
		// DoubleMatrix2D m = new SparseDoubleMatrix2D(matrix);
		// return m.toString();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < matrix.length; i++) {
			sb.append(Arrays.toString(matrix[i]));
		}
		return sb.toString();
	}

	/**
	 * Int array.
	 *
	 * @param matrix the matrix
	 * @return the string
	 */
	public final static String intArray(int[][] matrix) {
		double[][] doublematrix = new double[matrix.length][matrix[0].length];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < doublematrix[i].length; j++) {
				doublematrix[i][j] = matrix[i][j];
			}
		}
		return doubleArray(doublematrix);
	}

}
