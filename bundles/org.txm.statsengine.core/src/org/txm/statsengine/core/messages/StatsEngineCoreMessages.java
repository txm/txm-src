package org.txm.statsengine.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Statistics engine core messages.
 * 
 * @author sjacquot
 *
 */
public class StatsEngineCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.statsengine.core.messages.messages"; //$NON-NLS-1$

	public static String error_statsEngineNotReadyCancelingCommand;


	public static String nothingToDoWithAnEmptyMatrix;

	public static String innerArraysMustBeOfDimensionSup0;

	public static String anInnerArrayCannotBeNull;

	public static String allTheInnerArrayDoNotHaveTheSameLength;

	public static String theIndexArrayCanNotBeNullOrEmpty;

	public static String aZerobasedIndexCannotBeInf0OrSupToTheNumberOfIndexedElements;

	public static String notImplemented;

	public static String rowIndex;

	public static String colIndex;

	public static String rObjectEvaluatedToNull;

	public static String unknownTypeP0;

	public static String mode;

	public static String columnIndex;

	public static String rowNamesOfAContingencyTableCannotBeNull;

	public static String columnNamesOfAContingencyTableCannotBeNull;

	public static String matrixColonFailedToGetNrowP0;

	public static String failedToGetRowNames;

	public static String cannotDeleteAllLines;

	public static String cannotDeleteANonexistingObject;

	public static String CanNotExecuteTheP0CommandWithTheP1Selection;

	public static String CorpusEngineIsNotReadyCancelingCommand;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, StatsEngineCoreMessages.class);
	}

	private StatsEngineCoreMessages() {
	}
}
