package org.txm.searchengine.rcp.preferences;

import java.util.LinkedHashMap;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.txm.core.results.TXMResult;
import org.txm.objects.Workspace;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.MultipleValueDialog;
import org.txm.rcp.utils.IOClipboard;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.QueryHistory;

public class QueryView extends ViewPart {

	protected TreeViewer tv;

	protected IQuery copiedQuery;

	@Override
	public void createPartControl(Composite parent) {

		parent.setLayout(new GridLayout());

		Button go = new Button(parent, SWT.NONE);
		go.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false));
		go.setText("Refresh");
		go.setToolTipText(TXMUIMessages.common_refresh);
		go.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				tv.refresh();
			}
		});

		tv = new TreeViewer(parent, SWT.VIRTUAL);
		tv.getTree().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		final DragSource source = new DragSource (tv.getTree(), DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_LINK);
		source.setTransfer(new Transfer[] { LocalSelectionTransfer.getTransfer()});
		
		source.addDragListener (new DragSourceAdapter() {
			
			@Override
			public void dragSetData(DragSourceEvent event) {
				LocalSelectionTransfer transfer = LocalSelectionTransfer.getTransfer();
				if (transfer.isSupportedType(event.dataType)) {
					transfer.setSelection(tv.getStructuredSelection());
					transfer.setSelectionSetTime(event.time & 0xFFFF);
				}
			}
		});
		
		tv.setContentProvider(new ITreeContentProvider() {

			@Override
			public Object[] getElements(Object inputElement) {
				if (inputElement instanceof TXMResult) {
					return ((TXMResult) inputElement).getDeepChildren(QueryHistory.class).toArray();
				}
				else {
					return new Object[0];
				}
			}

			@Override
			public Object[] getChildren(Object parentElement) {
				if (parentElement instanceof QueryHistory) {
					return ((QueryHistory) parentElement).getQueries().toArray();
				}
				else {
					return new Object[0];
				}
			}

			@Override
			public Object getParent(Object element) {
				if (element instanceof TXMResult) {
					return ((TXMResult) element).getParent();
				}
				else {
					return null;
				}
			}

			@Override
			public boolean hasChildren(Object parentElement) {
				if (parentElement instanceof QueryHistory) {
					QueryHistory h = ((QueryHistory) parentElement);
					try {
						h.compute(false); // silent
					}
					catch (InterruptedException e) {
						e.printStackTrace();
					}

					return h.getQueries().size() > 0;
				}
				else {
					return false;
				}
			}

		});
		tv.setUseHashlookup(true);
		tv.setLabelProvider(new LabelProvider() {

			/*
			 * (non-Javadoc)
			 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
			 */
			@Override
			public String getText(Object element) {
				if (element instanceof QueryHistory) {
					return ((QueryHistory) element).getParent().getName();
				}
				else if (element instanceof IQuery) {
					IQuery q = (IQuery) element;
					return queryToLabel(q);
				}
				else {
					return "" + element; //$NON-NLS-1$
				}
			}

			@Override
			public final Image getImage(Object element) {
				if (element instanceof QueryHistory) {
					return IImageKeys.getImage(IImageKeys.CORPUS);
				}
				else {
					return null;
				}
			}
		});
		tv.setInput(Workspace.getInstance());

		tv.getTree().addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {

				if (e.keyCode == SWT.F5) {
					QueryView.this.tv.refresh();
					return;
				}

//				IStructuredSelection sel = tv.getStructuredSelection();
				Object o = tv.getStructuredSelection().getFirstElement();

				if (o instanceof IQuery) {
					IQuery q = (IQuery) o;

					if (e.keyCode == SWT.DEL) {

						TreeItem selectedItem = tv.getTree().getSelection()[0];
						TreeItem parentItem = selectedItem.getParentItem();
						Object parentObject = parentItem.getData();
						if (parentObject instanceof QueryHistory) {
							QueryHistory qh = (QueryHistory) parentObject;
							System.out.println("Delete query: " + o + " to " + qh.getParent().getName());
							qh.removeQuery(q);

							tv.refresh(qh);
						}

					}
					else if (e.keyCode == SWT.KEYPAD_CR || e.keyCode == SWT.CR) {

						//						System.out.println("Edit query: "+o);
						LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();
						values.put("name", q.getName()); //$NON-NLS-1$
						values.put("query", q.getQueryString()); //$NON-NLS-1$
						MultipleValueDialog mvd = new MultipleValueDialog(tv.getTree().getDisplay().getActiveShell(), "Edit Query", values);
						if (mvd.open() == MultipleValueDialog.OK) {
							if (!mvd.getValues().get("name").isEmpty()) q.setName(mvd.getValues().get("name")); //$NON-NLS-1$
							if (!mvd.getValues().get("query").isEmpty()) q.setQuery(mvd.getValues().get("query")); //$NON-NLS-1$

							tv.refresh(q);
						}
					}
					else {

						if ((char) e.keyCode == 'c' & ((e.stateMask & SWT.CTRL) != 0)) { //$NON-NLS-1$
							System.out.println("Copied query: " + queryToLabel(q));
							copiedQuery = q;
							IOClipboard.write(q.getQueryString());
						}
						//						else if ((char) e.keyCode == 'v' & ((e.stateMask & SWT.CTRL) != 0)) {
						//							System.out.println("Paste query: "+o);
						//						}

					}
				}
				else if (o instanceof QueryHistory) {
					if ((char) e.keyCode == 'v' & ((e.stateMask & SWT.CTRL) != 0) && copiedQuery != null) { //$NON-NLS-1$

						QueryHistory qh = (QueryHistory) o;
						System.out.println("Paste query: " + copiedQuery + " to " + qh);
						qh.addQuery(copiedQuery);
						tv.refresh(qh);
					}
					else if (e.keyCode == SWT.KEYPAD_CR || e.keyCode == SWT.CR) {

						QueryHistory qh = (QueryHistory) o;

						LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();
						values.put("engine", "CQP"); //$NON-NLS-1$
						values.put("name", ""); //$NON-NLS-1$
						values.put("query", ""); //$NON-NLS-1$

						MultipleValueDialog mvd = new MultipleValueDialog(tv.getTree().getDisplay().getActiveShell(), "Edit Query", values);

						if (mvd.open() == MultipleValueDialog.OK) {
							if (!mvd.getValues().get("query").isEmpty() && !mvd.getValues().get("engine").isEmpty()) { //$NON-NLS-1$

								IQuery newQuery = Query.stringToQuery(mvd.getValues().get("engine") + "\t" + mvd.getValues().get("query")); //$NON-NLS-1$
								if (!mvd.getValues().get("name").isEmpty()) newQuery.setName(mvd.getValues().get("name")); //$NON-NLS-1$
								System.out.println("New query: " + newQuery + " to " + qh);
								qh.addQuery(newQuery);
								tv.refresh(qh);
							}


						}


					}
				}
			}
		});

		MenuManager menuManager = new MenuManager();
		Menu menu = menuManager.createContextMenu(tv.getTree());

		// Set the MenuManager
		tv.getTree().setMenu(menu);
		getSite().registerContextMenu(menuManager, tv);
		// Make the selection available
		getSite().setSelectionProvider(tv);
	}

	public static String queryToLabel(IQuery q) {

		if (q == null) return "";

		if (q.getName().equals(q.getQueryString())) {
			return q.getSearchEngine().getName() + " = " + q.getQueryString()+(q.getOptions().size() > 0 ?" "+q.getOptions():"");
		}
		else {
			return q.getSearchEngine().getName() + " - " + q.getName() + " = " + q.getQueryString()+(q.getOptions().size() > 0 ?" "+q.getOptions():"");
		}
	}

	@Override
	public void setFocus() {
		if (tv != null && !tv.getTree().isDisposed()) {
			tv.getTree().setFocus();
		}
	}

	/**
	 * Refresh.
	 */
	public static void refresh() {
		QueryView v = (QueryView) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findView(
						QueryView.class.getName());

		if (v == null) return;

		v.tv.refresh();
	}

	/**
	 * Refresh.
	 */
	public static void refresh(Object f) {
		if (f == null) return;

		QueryView v = (QueryView) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findView(
						QueryView.class.getName());

		if (v == null) return;

		v.tv.refresh(f);
	}

	/**
	 * Opens the File Explorer
	 */
	public static void open() {
		open(null);
	}

	/**
	 * Opens the Fiel Explorer
	 */
	public static void open(Object f) {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();

		try {
			IViewPart v = page.showView(QueryView.class.getName());
			if (v instanceof QueryView) {
				QueryView e = (QueryView) v;
				e.tv.setSelection(new StructuredSelection(f));
			}
		}
		catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
