package org.txm.searchengine.rcp.preferences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import org.eclipse.ui.IWorkbench;
import org.txm.Toolbox;
import org.txm.core.engines.EngineType;
import org.txm.rcp.preferences.RCPPreferencesPage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.rcp.swt.widget.preferences.ComboFieldEditor;
import org.txm.searchengine.core.SearchEngine;
import org.txm.searchengine.core.SearchEnginePreferences;
import org.txm.searchengine.core.SearchEnginesManager;


public class SearchEnginePreferencesPage extends RCPPreferencesPage {

	@Override
	public void createFieldEditors() {

		SearchEnginesManager sem = (SearchEnginesManager) Toolbox.getEngineManager(EngineType.SEARCH);
		HashMap<String, SearchEngine> engines = sem.getEngines();
		ArrayList<String> keys = new ArrayList<>(engines.keySet());
		Collections.sort(keys);

		String[][] choices2 = new String[engines.keySet().size()][2];
		for (int i = 0; i < keys.size(); i++) {
			choices2[i][0] = choices2[i][1] = engines.get(keys.get(i)).getName();
		}
		addField(new ComboFieldEditor(SearchEnginePreferences.DEFAULT_SEARCH_ENGINE, Messages.seachEngine, choices2, getFieldEditorParent()));

		BooleanFieldEditor field = new BooleanFieldEditor(SearchEnginePreferences.SHOW_SEARCH_ENGINES, Messages.showAvailableSearchEngines, getFieldEditorParent());
		field.setToolTipText(Messages.showAvailableSearchEnginesEtc);
		addField(field);
	}

	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(SearchEnginePreferences.getInstance().getPreferencesNodeQualifier()));
	}
}
