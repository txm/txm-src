// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.statsengine.r.rcp.views;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.txm.Toolbox;
import org.txm.core.engines.Engine;
import org.txm.core.engines.EngineType;
import org.txm.core.engines.EnginesManager;
import org.txm.rcp.IImageKeys;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.statsengine.r.rcp.messages.RUIMessages;
import org.txm.utils.MessagePrinter;
import org.txm.utils.StreamHog;
import org.txm.utils.logger.Log;

/**
 * Display the R logs only.
 * 
 * @author mdecorde
 */
public class RConsole extends ViewPart implements MessagePrinter {

	/** The ID. */
	public static String ID = RConsole.class.getName();

	/** The text area. */
	private StyledText textArea;

	private Display display;

	boolean readyToDisplay = false;

	//	/** The out. */
	//	private static PrintStream out;
	//
	//	/** The err. */
	//	private static PrintStream err;
	//
	//	/** The quit. */
	//	private boolean quit;
	//
	//	/** The reader. */
	//	private Thread reader;
	//
	//	/** The reader2. */
	//	private Thread reader2;
	//
	//	/** The pin. */
	//	PipedInputStream pin = new PipedInputStream();
	//
	//	/** The pin2. */
	//	PipedInputStream pin2 = new PipedInputStream();
	//
	//	/** The output. */
	//	static PrintStream output = System.out;
	//
	//	/** The errput. */
	//	static PrintStream errput = System.err;
	//
	//	/** The blue. */
	//	static Color blue;
	//
	//	/** The red. */
	//	static Color red;

	/**
	 * Instantiates a new r console.
	 */
	public RConsole() {
		//		try {
		//			PipedOutputStream pout = new PipedOutputStream(pin);
		//			out = new PrintStream(pout, true);
		//
		//			new PipedOutputStream(pin2);
		//			err = new PrintStream(pout, true);
		//
		//		} catch (IOException e) {
		//			org.txm.utils.logger.Log.printStackTrace(e);
		//		}
		//
		//		reader = new Thread(this);
		//		reader.setDaemon(true);
		//		reader.start();
		//		reader2 = new Thread(this);
		//		reader2.setDaemon(true);
		//		reader2.start();
	}

	/**
	 * Redirect.
	 *
	 * @param b the b
	 */
	public static void redirect(boolean b) {
		//		if (b) {
		//			System.setOut(out);
		//			System.setErr(err);
		//		} else {
		//			System.setOut(output);
		//			System.setErr(errput);
		//		}
	}

	//	/**
	//	 * Lecture de la sortie standard.
	//	 *
	//	 * @param in the in
	//	 * @return the string
	//	 * @throws IOException Signals that an I/O exception has occurred.
	//	 */
	//	protected synchronized String readLine(PipedInputStream in)
	//			throws IOException {
	//		String input = ""; //$NON-NLS-1$
	//		do {
	//			int available = in.available();
	//			if (available == 0)
	//				break;
	//			byte b[] = new byte[available];
	//			in.read(b);
	//			input = input + new String(b, 0, b.length);
	//		} while (!input.endsWith("\n") && !input.endsWith("\r\n") && !quit); //$NON-NLS-1$ //$NON-NLS-2$
	//		return input;
	//	}
	//
	//	/* (non-Javadoc)
	//	 * @see java.lang.Runnable#run()
	//	 */
	//	@Override
	//	public synchronized void run() {
	//		try {
	//			while (Thread.currentThread() == reader) {
	//				try {
	//					this.wait(100);
	//				} catch (InterruptedException ie) {
	//				}
	//				if (pin.available() != 0) {
	//					String input = this.readLine(pin);
	//					textArea.append(input);
	//				}
	//				if (quit)
	//					return;
	//			}
	//
	//			while (Thread.currentThread() == reader2) {
	//				try {
	//					this.wait(100);
	//				} catch (InterruptedException ie) {
	//				}
	//				if (pin2.available() != 0) {
	//					String input = this.readLine(pin2);
	//					textArea.append(input);
	//				}
	//
	//				if (quit)
	//					return;
	//			}
	//		} catch (Exception e) {
	//			textArea.append(Messages.GroovyConsole_4);
	//			textArea.append(Messages.GroovyConsole_5 + e);
	//		}
	//	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		display = parent.getDisplay();

		//	blue = parent.getDisplay().getSystemColor(SWT.COLOR_BLUE);
		//	red = parent.getDisplay().getSystemColor(SWT.COLOR_RED);

		textArea = new StyledText(parent, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL
				| SWT.H_SCROLL);
		textArea.setEditable(false);

		textArea.setLayoutData(new GridData(GridData.FILL_BOTH
				| GridData.GRAB_HORIZONTAL | GridData.GRAB_VERTICAL));

		// Custom Action for the View's Menu  
		Action lCustomAction = new Action() {

			public void run() {
				clear();
			}
		};
		lCustomAction.setId("org.txm.rcp.views.RConsole.clear"); //$NON-NLS-1$
		//lCustomAction.setText("Clear");  
		lCustomAction.setImageDescriptor(IImageKeys.getImageDescriptor("platform:/plugin/org.eclipse.ui.console/icons/full/clcl16/clear_co.png"));
		getViewSite().getActionBars().getToolBarManager().add(lCustomAction);

		connectToRWorkspace();
	}

	public void reconnectToRWorkspace() {
		readyToDisplay = false;
		connectToRWorkspace();
	}

	public void connectToRWorkspace() {
		EnginesManager<? extends Engine> engine = Toolbox.getEngineManager(EngineType.STATS);
		if (!readyToDisplay && engine != null && engine.isCurrentEngineAvailable()) {
			RWorkspace rw = null;
			Log.fine(RUIMessages.connectingRConsoleToRWorkspaceStdoutAndStderr);

			try {
				rw = RWorkspace.getRWorkspaceInstance();
				if (rw == null) return;
				StreamHog errorLogger = rw.getErrorLogger();
				StreamHog inputLogger = rw.getInputLogger();
				//if (errorLogger.getMessagePrinter() == null) {
				//System.out.println("SET MESSAGEPRINTER OF THE RCONSOLE");
				if (errorLogger != null)
					errorLogger.registerMessagePrinter(this);
				if (inputLogger != null)
					inputLogger.registerMessagePrinter(this);
				readyToDisplay = true;
				//}
			}
			catch (RWorkspaceException e) {
				System.out.println(RUIMessages.errorWhileInitializingRConsoleColon + e);
				Log.printStackTrace(e);
			}
		}
	}


	String txtToPrint;

	/**
	 * Println.
	 *
	 * @param txt the txt
	 */
	public void println(String txt) {
		txtToPrint = txt;
		display.syncExec(new Runnable() {

			public void run() {
				if (textArea != null) {
					textArea.append(txtToPrint + "\n"); //$NON-NLS-1$
					textArea.setTopIndex(textArea.getLineCount());
				}
			}
		});
	}

	/**
	 * Prints the.
	 *
	 * @param txt the txt
	 */
	public void print(String txt) {
		txtToPrint = txt;
		display.syncExec(new Runnable() {

			public void run() {
				if (textArea != null) {
					textArea.append(txtToPrint); //$NON-NLS-1$
					textArea.setTopIndex(textArea.getLineCount());
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
	}

	public void clear() {
		if (textArea != null)
			textArea.setText(""); //$NON-NLS-1$
	}

	public static void reload() {
		for (IWorkbenchWindow window : PlatformUI.getWorkbench().getWorkbenchWindows()) {
			IViewPart v = window.getActivePage().findView(RConsole.ID);
			if (v == null) return;
			RConsole rc = (RConsole) v;
			rc.reconnectToRWorkspace();
		}
	}
}
