package org.txm.statsengine.r.rcp.handlers;

import java.util.HashMap;

import org.txm.core.results.TXMResult;

public class RAdaptor {

	protected HashMap<String, TXMResult> symbols = new HashMap<String, TXMResult>();

	protected HashMap<TXMResult, String> sended = new HashMap<TXMResult, String>();


	public static String sendToR(TXMResult result) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * Get the symbols created.
	 *
	 * @return the symbol
	 */
	public HashMap<String, TXMResult> getSymbols() {
		return symbols;
	}

	/**
	 * Gets the indexes who have a symbol.
	 *
	 * @return the symbol
	 */
	public HashMap<TXMResult, String> getSendedTXMResults() {
		return sended;
	}
}
