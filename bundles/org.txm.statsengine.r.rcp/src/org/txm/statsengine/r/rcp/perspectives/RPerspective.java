// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.statsengine.r.rcp.perspectives;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.console.IConsoleConstants;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.rcp.views.fileexplorer.Explorer;
import org.txm.statsengine.r.rcp.views.RConsole;
import org.txm.statsengine.r.rcp.views.RVariablesView;

// TODO: Auto-generated Javadoc
/**
 * The Class RPerspective.
 */
public class RPerspective implements IPerspectiveFactory {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui.IPageLayout)
	 */
	@Override
	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(true);
		layout.setFixed(false);

		IFolderLayout left = layout.createFolder("leftR", IPageLayout.LEFT, 0.25f, editorArea); //$NON-NLS-1$
		left.addView(RVariablesView.ID);
		left.addView(CorporaView.ID);
		left.addView(Explorer.ID);

		//if olderLayout bottom = layout.createFolder("bottomR", IPageLayout.BOTTOM, 0.66f, editorArea); //$NON-NLS-1$
		layout.addView(IConsoleConstants.ID_CONSOLE_VIEW, IPageLayout.BOTTOM, 0.8f, editorArea);
		layout.addView(RConsole.ID, IPageLayout.BOTTOM, 0.5f, editorArea);


		RVariablesView.reload();
	}
}
