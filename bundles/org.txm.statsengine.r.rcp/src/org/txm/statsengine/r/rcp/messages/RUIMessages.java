package org.txm.statsengine.r.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * R statistics engine UI messages.
 * 
 * @author sjacquot
 *
 */
public class RUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.statsengine.r.rcp.messages.messages"; //$NON-NLS-1$

	public static String runRServeInDebugMode;

	public static String portDefaultIs6311;

	public static String pathToRBinary;

	public static String remoteConnection;

	public static String serverAddress;

	public static String user;

	public static String password;

	public static String useFileBasedRDataTransfert;

	public static String rArgs;

	public static String rserveArgs;

	public static String tXMCouldNotLoadTheTextometryRPackageTheCooccurenceSpecificityAndProgressionCommandsWontBeAvailable;

	public static String tXMCouldNotLoadTheFactoMineRRPackageTheCorrespondanceAnalysisClassificationCommandsWontBeAvailable;

	public static String anErrorOccuredWhileGettingRWorkspaceInsanceColon;

	public static String rPackageDiagnostic;

	public static String installedSuccessfully;

	public static String errorWhileInstallingPackageColon;

	public static String theRStatEngineIsNotRunningTryingToReinstallThePackagesRServeFactoMineRTextometry;

	public static String tXMCouldNotReinstallRPackagesPleaseDoItManuallySeeManual;

	public static String necessaryPackagesAreReady;

	public static String eRRORColonRlistFirstElementIsNotREXPIntegerColon;

	public static String eRRORColonREvalListSizeIs0Colon;

	public static String eRRORColonREvalIsNotAListColon;

	public static String somePackagesAreMissingSeeConsoleForDetails;

	public static String checkingRPackages;

	public static String sendingToRWorkspaceColon;

	public static String refreshingViews;

	public static String connectingRConsoleToRWorkspaceStdoutAndStderr;

	public static String DefaultPackageRepository;

	public static String errorWhileInitializingRConsoleColon;

	public static String ErrorWhileInstallingRDependencyOfP0P1P2;

	public static String ErrorWhileTEstingPackageP0;

	public static String FailedToSendToR;

	public static String LogTheREvalCommandLines;

	public static String P0HasBeenCopiedInTheP1RVariable;

	public static String P0PackageNotInstalledErrorP1;

	public static String P0WasNotTransferedToTheRWorkspace;

	public static String refreshVariables;

	public static String showEvalLogs;

	public static String TheP0PackageIsNotInstalledTryingToInstallItNow;

	public static String TheP0PackageWasNotInstalledCorrecltyPleaseInstallItManuallyWithCmdP1;

	public static String TryingToInstallTheP0DependencyOfP1Now;

	public static String tXMNameP0SupSupRNameP1;

	public static String UpdateOfP0FailedCurrentVersionIsStillP1;

	public static String UpdatingTheP0P1RPackageUpToVersionP2;

	public static String distantR; // Rserve distante
	public static String localR; // Rserve locale

	public static String extraRLibraryDirectory; // "Extra R library directory"

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, RUIMessages.class);
	}

	private RUIMessages() {
	}
}
