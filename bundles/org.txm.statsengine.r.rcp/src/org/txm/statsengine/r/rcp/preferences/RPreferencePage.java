// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.statsengine.r.rcp.preferences;

import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbench;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.rcp.swt.widget.preferences.DirectoryFieldEditor;
import org.txm.rcp.swt.widget.preferences.FileFieldEditor;
import org.txm.rcp.swt.widget.preferences.IntegerFieldEditor;
import org.txm.rcp.swt.widget.preferences.StringFieldEditor;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.preferences.RPreferences;
import org.txm.statsengine.r.rcp.messages.RUIMessages;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * This class represents a preference page that is contributed to the
 * Preferences dialog. By subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows us to create a page
 * that is small and knows how to save, restore and apply itself.
 * <p>
 * This page is used to modif y preferences only. They are stored in the
 * preference store that belongs to the main plug-in class. That way,
 * preferences can be accessed directly via the preference store.
 */

public class RPreferencePage extends TXMPreferencePage {

	/** The r_path_to_executable. */
	private FileFieldEditor r_path_to_executable;

	/** The r_server_adress. */
	private StringFieldEditor r_server_adress;

	private IntegerFieldEditor r_port;

	/** The r_user. */
	private StringFieldEditor r_user;

	/** The r_password. */
	private StringFieldEditor r_password;

	/** The r_is_remote. */
	private BooleanFieldEditor r_is_remote;

	private BooleanFieldEditor r_debug;

	/** The r_use_file_transfert. */
	private BooleanFieldEditor r_use_file_transfert;

//	private StringFieldEditor r_svg_device;

	private StringFieldEditor r_rargs;

	private StringFieldEditor r_rserveargs;

	private StringFieldEditor r_repos;

	private DirectoryFieldEditor r_extra;



	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(RPreferences.getInstance().getPreferencesNodeQualifier()));
		// this.setTitle(RUIMessages.runRServeInDebugMode);
		// FIXME todo if needed
		// setDescription(Messages.RPreferencePage_1);
		// this.setImageDescriptor(SpecificitiesAdapterFactory.ICON);
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		r_is_remote = new BooleanFieldEditor(RPreferences.REMOTE, RUIMessages.remoteConnection, BooleanFieldEditor.DEFAULT, getFieldEditorParent());
		addField(r_is_remote);

		addField(new BooleanFieldEditor(RPreferences.SHOW_EVAL_LOGS, RUIMessages.LogTheREvalCommandLines, getFieldEditorParent()));

		Group localPreferences = new Group(getFieldEditorParent(), SWT.NONE);
		localPreferences.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1));
		localPreferences.setText(RUIMessages.localR);

		r_path_to_executable = new FileFieldEditor(RPreferences.PATH_TO_EXECUTABLE, RUIMessages.pathToRBinary, localPreferences);
		addField(r_path_to_executable);

		r_rargs = new StringFieldEditor(RPreferences.RARGS, RUIMessages.rArgs, localPreferences);
		addField(r_rargs);

		r_rserveargs = new StringFieldEditor(RPreferences.RSERVEARGS, RUIMessages.rserveArgs, localPreferences);
		addField(r_rserveargs);

		r_use_file_transfert = new BooleanFieldEditor(RPreferences.FILE_TRANSFERT, RUIMessages.useFileBasedRDataTransfert, BooleanFieldEditor.DEFAULT, localPreferences);
		addField(r_use_file_transfert);

		r_debug = new BooleanFieldEditor(RPreferences.DEBUG, RUIMessages.runRServeInDebugMode, BooleanFieldEditor.DEFAULT, localPreferences);
		addField(r_debug);

		r_repos = new StringFieldEditor(RPreferences.DEFAULT_REPOS, RUIMessages.DefaultPackageRepository, localPreferences);
		addField(r_repos);

		r_extra = new DirectoryFieldEditor(RPreferences.PACKAGES_PATH, RUIMessages.extraRLibraryDirectory, localPreferences);
		addField(r_extra);

		((GridLayout)localPreferences.getLayout()).marginWidth = 3;

		Group distantPreferences = new Group(getFieldEditorParent(), SWT.NONE);
		distantPreferences.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1));
		distantPreferences.setText(RUIMessages.distantR);

		r_server_adress = new StringFieldEditor(RPreferences.SERVER_ADDRESS, RUIMessages.serverAddress, distantPreferences);
		addField(r_server_adress);

		r_port = new IntegerFieldEditor(RPreferences.PORT, RUIMessages.portDefaultIs6311, distantPreferences);
		addField(r_port);

		r_user = new StringFieldEditor(RPreferences.USER, RUIMessages.user, distantPreferences);
		addField(r_user);

		r_password = new StringFieldEditor(RPreferences.PASSWORD, RUIMessages.password, distantPreferences);
		addField(r_password);

		((GridLayout)distantPreferences.getLayout()).marginWidth = 3;
		((GridLayout)distantPreferences.getLayout()).numColumns = 3;

		changeFieldStates(RPreferences.getInstance().getBoolean(RPreferences.REMOTE));
	}

	/**
	 * Enable/disable the fields according to the remote mode activation.
	 * 
	 * @param serverIsRemote
	 */
	public void changeFieldStates(boolean serverIsRemote) {

		r_server_adress.setEnabled(serverIsRemote);
		r_user.setEnabled(serverIsRemote);
		r_password.setEnabled(serverIsRemote);
		
		r_path_to_executable.setEnabled(!serverIsRemote);
		r_rargs.setEnabled(!serverIsRemote);
		r_rserveargs.setEnabled(!serverIsRemote);
		r_use_file_transfert.setEnabled(!serverIsRemote);
		r_repos.setEnabled(!serverIsRemote);
		r_extra.setEnabled(!serverIsRemote);
		r_debug.setEnabled(!serverIsRemote);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#propertyChange(org.eclipse.jface.util.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
		if (((FieldEditor) event.getSource()) == r_is_remote) {
			changeFieldStates((Boolean) event.getNewValue());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#performOk()
	 */
	@Override
	public boolean performOk() {
		try {
			if (!super.performOk()) {
				return false;
			}
			// Toolbox.updateProperties(ApplicationWorkbenchAdvisor.getProperties());

			RWorkspace rw = RWorkspace.getRWorkspaceInstance();
			if (rw != null) {
				RWorkspace.setUseFileCommunication(RPreferences.getInstance().getBoolean(RPreferences.FILE_TRANSFERT));
				rw.setLog(RWorkspace.isLoggingEvalCommandLines());
			}
		}
		catch (Exception e) {
			Log.warning(e);
			return false;
		}
		return true;
	}
}
