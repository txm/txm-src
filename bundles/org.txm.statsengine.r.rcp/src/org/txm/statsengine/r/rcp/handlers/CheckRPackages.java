package org.txm.statsengine.r.rcp.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.util.Util;
import org.eclipse.swt.widgets.Shell;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPInteger;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.RList;
import org.txm.Toolbox;
import org.txm.core.engines.EngineType;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.utils.SWTUtils;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.statsengine.r.rcp.messages.RUIMessages;
import org.txm.utils.logger.Log;

public class CheckRPackages extends BaseAbstractHandler {

	public static HashMap<String, String> packages;

	public static HashMap<String, String> errorMessages;

	public static HashMap<String, List<String>> depends;


	public static void initialize() {
		packages = new HashMap<>();
		errorMessages = new HashMap<>();
		depends = new HashMap<>();

		packages.put("textometry", "0.1.4"); //$NON-NLS-1$ //$NON-NLS-2$
		packages.put("FactoMineR", "1.24"); //$NON-NLS-1$ //$NON-NLS-2$

		depends.put("textometry", new ArrayList<String>()); //$NON-NLS-1$
		depends.put("FactoMineR", Arrays.asList("MASS", //$NON-NLS-1$ //$NON-NLS-2$
				"nnet",  //$NON-NLS-1$
				"car",  //$NON-NLS-1$
				"ellipse",  //$NON-NLS-1$
				"leaps",  //$NON-NLS-1$
				"lattice",  //$NON-NLS-1$
				"cluster",  //$NON-NLS-1$
				"scatterplot3d")); //$NON-NLS-1$

		errorMessages.put("textometry", RUIMessages.tXMCouldNotLoadTheTextometryRPackageTheCooccurenceSpecificityAndProgressionCommandsWontBeAvailable); //$NON-NLS-1$
		errorMessages.put("FactoMineR", RUIMessages.tXMCouldNotLoadTheFactoMineRRPackageTheCorrespondanceAnalysisClassificationCommandsWontBeAvailable); //$NON-NLS-1$
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!super.checkStatsEngine()) {
			return null;
		}

		Shell shell = SWTUtils.getShell(event);
		if (check()) {
			MessageDialog.openInformation(shell, RUIMessages.rPackageDiagnostic, RUIMessages.necessaryPackagesAreReady);
		}
		else {
			MessageDialog.openError(shell, RUIMessages.rPackageDiagnostic, RUIMessages.somePackagesAreMissingSeeConsoleForDetails);
		}
		return null;
	}

	public static String buildVersion(REXPInteger rExpInteger) throws REXPMismatchException {
		StringBuffer buff = new StringBuffer(""); //$NON-NLS-1$

		// RList list = rExpInteger.asList();
		int[] ints = rExpInteger.asIntegers();
		for (int i = 0; i < rExpInteger.length(); i++) {
			buff.append("." + ints[i]); //$NON-NLS-1$
		}
		if (buff.length() > 0)
			return buff.substring(1); // first '.'

		return ""; //$NON-NLS-1$
	}

	public static boolean check() {
		System.out.print(RUIMessages.checkingRPackages);

		initialize();
		// Log.info("Checking R packages...");

		String method = "internal"; // default method is internal //$NON-NLS-1$
		if (Util.isMac()) method = "curl"; //$NON-NLS-1$
		else if (Util.isLinux()) method = "wget"; //$NON-NLS-1$

		boolean ret = true;
		if (Toolbox.getEngineManager(EngineType.STATS).isCurrentEngineAvailable()) {
			RWorkspace rw = null;
			try {
				rw = RWorkspace.getRWorkspaceInstance();
			}
			catch (RWorkspaceException e) {
				Log.warning(TXMCoreMessages.bind(RUIMessages.anErrorOccuredWhileGettingRWorkspaceInsanceColon, e));
				Log.printStackTrace(e);
				return false;
			}

			for (String p : packages.keySet()) {
				// String v = packages.get(p);
				// String error = errorMessages.get(p);
				try {
					rw.eval("installed.packages(\"" + p + "\")"); //$NON-NLS-1$ //$NON-NLS-2$
				}
				catch (Exception e) {
					Log.printStackTrace(e);
					Log.severe(TXMCoreMessages.bind(RUIMessages.P0PackageNotInstalledErrorP1, p, e));
					ret = false;
				}

				if (ret) { // check the version and install if possible
					try {
						String currentVersion = getVersion(rw, p);
						ret = updateIfNecessary(rw, p, currentVersion);
					}
					catch (Exception e) {
						Log.printStackTrace(e);
						Log.warning(TXMCoreMessages.bind(RUIMessages.ErrorWhileTEstingPackageP0, e));
						ret = false;
					}
				}
				else { // package is not present, try installing it
					try {
						Log.info(TXMCoreMessages.bind(RUIMessages.TheP0PackageIsNotInstalledTryingToInstallItNow, p));

						try {
							rw.eval("detach(\"package:" + p + "\", character.only = TRUE)"); //$NON-NLS-1$ //$NON-NLS-2$
						}
						catch (Exception tmpE) {
						} // unload package to be able to reinstall it

						// install depends if any
						if (depends.containsKey(p)) {
							for (String dep : depends.get(p)) {
								try {
									Log.info(TXMCoreMessages.bind(RUIMessages.TryingToInstallTheP0DependencyOfP1Now, dep, p));
									rw.eval("install.packages(\"" + dep + "\", dependencies=TRUE, method=\"" + method + "\", repos=\"http://cran.rstudio.com\");"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
								}
								catch (Exception e) {
									Log.warning(TXMCoreMessages.bind(RUIMessages.ErrorWhileInstallingRDependencyOfP0P1P2, p, dep, e));
									Log.printStackTrace(e);
									ret = false;
								}
							}
						}

						// install the package itself
						rw.eval("install.packages(\"" + p + "\", dependencies=TRUE, method=\"" + method + "\", repos=\"http://cran.rstudio.com\");"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

						try { // now try loading the package
							rw.eval("library(\"" + p + "\")"); //$NON-NLS-1$ //$NON-NLS-2$
							Log.info(TXMCoreMessages.bind(RUIMessages.installedSuccessfully, p));
						}
						catch (Exception e) {
							Log.printStackTrace(e);
							Log.warning(TXMCoreMessages.bind(RUIMessages.TheP0PackageWasNotInstalledCorrecltyPleaseInstallItManuallyWithCmdP1, p, p));
							ret = false;
						}
					}
					catch (Exception e) {
						Log.printStackTrace(e);
						System.out.println(TXMCoreMessages.bind(RUIMessages.errorWhileInstallingPackageColon, e));
						ret = false;
					}
				}
			}
		}
		else {
			System.out.println(RUIMessages.theRStatEngineIsNotRunningTryingToReinstallThePackagesRServeFactoMineRTextometry);
			try {
				installRPAckagesViaRscript();
			}
			catch (Exception e) {
				System.out.println(RUIMessages.tXMCouldNotReinstallRPackagesPleaseDoItManuallySeeManual);
			}
			ret = false;
		}
		if (ret == true) {
			System.out.println(TXMCoreMessages.common_ok);
		}
		return ret;
	}

	public static boolean installRPAckagesViaRscript() throws IOException {
		String cmd = ""; //$NON-NLS-1$
		String installPath = Toolbox.getInstallDirectory();
		if (Util.isMac() || Util.isLinux()) {
			cmd = "R --no-save < \"" + installPath + "/R/rlibs.R\""; //$NON-NLS-1$ //$NON-NLS-2$
		}
		else if (Util.isWindows()) {
			return false;
		}
		else {
			return false;
		}
		Runtime.getRuntime().exec(cmd);
		return true;
	}

	private static String getVersion(RWorkspace rw, String p) throws REXPMismatchException, RWorkspaceException {
		REXP rRez = rw.eval("packageVersion(\"" + p + "\")"); //$NON-NLS-1$ //$NON-NLS-2$
		if (rRez.isList()) {
			RList rList = rRez.asList();
			if (rList.size() > 0) {
				Object elem = rList.get(0);
				if (elem instanceof REXPInteger) {
					REXPInteger rExpInt = (REXPInteger) elem;
					return buildVersion(rExpInt);
				}
				else {
					System.out.println(TXMCoreMessages.bind(RUIMessages.eRRORColonRlistFirstElementIsNotREXPIntegerColon, elem));
				}
			}
			else {
				System.out.println(TXMCoreMessages.bind(RUIMessages.eRRORColonREvalListSizeIs0Colon, rList));
			}
		}
		else {
			System.out.println(TXMCoreMessages.bind(RUIMessages.eRRORColonREvalIsNotAListColon, rRez));
		}
		return ""; //$NON-NLS-1$
	}

	/**
	 * 
	 * @param rw
	 * @param p
	 * @param currentVersion
	 * @return false if update failed
	 * 
	 * @throws RWorkspaceException
	 * @throws REXPMismatchException
	 */
	private static boolean updateIfNecessary(RWorkspace rw, String p, String currentVersion) throws RWorkspaceException, REXPMismatchException {
		// System.out.println("Current version of "+p+" is "+v+" and need "+packages.get(p));
		// System.out.println("comp "+(packages.get(p))+" to "+(v));
		// System.out.println(packages.get(p).compareTo(v));
		if (packages.get(p).compareTo(currentVersion) > 0) {
			System.out.println(TXMCoreMessages.bind(RUIMessages.UpdatingTheP0P1RPackageUpToVersionP2, p, currentVersion, packages.get(p)));

			try {
				rw.eval("detach(\"package:" + p + "\", character.only = TRUE)"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			catch (Exception tmpE) {
			} // unload package to be able to reinstall it

			// update.packages don't work ?
			// rw.eval("update.packages(\""+p+"\", dependencies=TRUE, ask=FALSE, repos=\"http://cran.rstudio.com\");"); //$NON-NLS-1$ //$NON-NLS-2$
			rw.eval("install.packages(\"" + p + "\", dependencies=TRUE, repos=\"http://cran.rstudio.com\");"); //$NON-NLS-1$ //$NON-NLS-2$

			try {
				rw.eval("library(\"" + p + "\")"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			catch (Exception tmpE) {
			} // reload it
			String newVersion = getVersion(rw, p);
			if (!newVersion.equals(packages.get(p))) {
				System.out.println(TXMCoreMessages.bind(RUIMessages.UpdateOfP0FailedCurrentVersionIsStillP1, p, newVersion));
				return false;
			}
		}
		return true;
	}
}
