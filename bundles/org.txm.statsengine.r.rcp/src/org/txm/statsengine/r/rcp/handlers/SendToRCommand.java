// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.statsengine.r.rcp.handlers;

import java.util.HashMap;
import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.results.TXMResult;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.statsengine.core.StatsEnginesManager;
import org.txm.statsengine.r.core.RResult;
import org.txm.statsengine.r.core.RStatsEngine;
import org.txm.statsengine.r.core.RTransformer;
import org.txm.statsengine.r.rcp.messages.RUIMessages;
import org.txm.statsengine.r.rcp.views.RVariablesView;
import org.txm.utils.logger.Log;

/**
 * Commands that creates a R object using a TXMResult.
 * 
 * 
 * 
 * @author mdecorde.
 */
public class SendToRCommand extends BaseAbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!super.checkStatsEngine()) {
			return null;
		}

		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		if (selection.isEmpty()) return false;

		final List<?> list = selection.toList();
		if (list.size() == 0) return false;

		JobHandler jobhandler = new JobHandler(RUIMessages.sendingToRWorkspaceColon + list) {

			@Override
			protected IStatus _run(SubMonitor monitor) throws Exception {

				monitor.beginTask(RUIMessages.sendingToRWorkspaceColon + list, 100);
				int delta = 100 / list.size();
				for (Object o : list) {

					if (o instanceof TXMResult) {
						try {
							((TXMResult) o).compute(false);
						}
						catch (InterruptedException e) {
							e.printStackTrace();
							continue;
						}
					}

					if (o instanceof RResult) {
						Log.info(NLS.bind("{0} R symbol is: {1}.", ((RResult) o).toString(), ((RResult) o).getRSymbol()));
					}
					else if (o instanceof TXMResult) {
						try {
							monitor.subTask(RUIMessages.sendingToRWorkspaceColon + o);
							this.acquireSemaphore();

							String previousSymbol = RTransformer.getTMPSymbol((TXMResult) o);

							String symbol = send((TXMResult) o);

							if (symbol == null) {
								Log.info(NLS.bind(RUIMessages.P0WasNotTransferedToTheRWorkspace, o));
								this.releaseSemaphore();
								return Status.CANCEL_STATUS;
							}
							else {
								Log.info(NLS.bind(RUIMessages.P0HasBeenCopiedInTheP1RVariable, o.toString(), symbol));
							}
							this.releaseSemaphore();

							monitor.worked(delta);

						}
						catch (Exception e) {
							Log.warning(RUIMessages.FailedToSendToR + o);
							throw e;
						}
					}
				}

				monitor.subTask(RUIMessages.refreshingViews);
				this.syncExec(new Runnable() {

					@Override
					public void run() {
						CorporaView.refresh();
						RVariablesView.reload();
					}
				});
				return Status.OK_STATUS;
			}
		};
		jobhandler.startJob();

		return null;
	}

	/**
	 * Static method to create a R object from a TXMResult
	 *
	 * @param o the o
	 * @throws Exception the exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String send(TXMResult o) throws Exception {

		RStatsEngine rengine = (RStatsEngine) StatsEnginesManager.getREngine();
		HashMap<String, RTransformer<? extends TXMResult>> transformers = rengine.getTransformers();
		if (transformers.containsKey(o.getClass().getName())) {
			RTransformer transformer = rengine.getTransformers().get(o.getClass().getName());
			if (transformer == null) {
				return null;
			}
			String s = transformer.fromTXMtoR(o);
			return s;
		}
		else {
			return null;
		}
	}
}
