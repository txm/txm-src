// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.statsengine.r.rcp.decorators;

import java.lang.reflect.Method;

import org.eclipse.jface.viewers.ILabelDecorator;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.FrameworkUtil;
import org.txm.core.results.TXMResult;
import org.txm.rcp.utils.OverlayImageIcon;
import org.txm.statsengine.r.core.RTransformer;
import org.txm.utils.logger.Log;

/**
 * The Class RDecorator adds a R icon to results that have a R symbol
 */
public class RDecorator extends LabelProvider implements ILabelDecorator {

	/**
	 * Instantiates a new r decorator.
	 */
	public RDecorator() {
		super();
	}

	// Method to decorate Image 
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ILabelDecorator#decorateImage(org.eclipse.swt.graphics.Image, java.lang.Object)
	 */
	@Override
	public Image decorateImage(Image image, Object object) {
		String symbol = null;
		try {
			Method getname = object.getClass().getMethod("getRSymbol"); //$NON-NLS-1$
			symbol = (String) getname.invoke(object);
		}
		catch (Exception e) {

		}

		if (symbol == null) {
			try {
				if (object instanceof TXMResult result) {
					symbol = RTransformer.getTMPSymbol(result);
					if (symbol != null && symbol.length() == 0) symbol = null;
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (symbol == null) {
			return null;
		}
		//System.out.println("decorate "+symbol);
		// Overlay custom image over base image 
		try {
			Image image2;
			OverlayImageIcon overlayIcon = new OverlayImageIcon(image, "platform:/plugin/" + FrameworkUtil.getBundle(RDecorator.class).getSymbolicName() + "/icons/decorators/R.png"); //$NON-NLS-1$ //$NON-NLS-2$
			image2 = overlayIcon.getImage();
			return image2;
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
		return null;
		// The image should be disposed when the plug-in is 
		// disabled or on shutdown 
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ILabelDecorator#decorateText(java.lang.String, java.lang.Object)
	 */
	@Override
	public String decorateText(String text, Object element) {
		// TODO Auto-generated method stub
		return null;
	}
}
