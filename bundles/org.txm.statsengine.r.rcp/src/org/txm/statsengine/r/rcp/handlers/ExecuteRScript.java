// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.statsengine.r.rcp.handlers;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.txm.Toolbox;
import org.txm.core.engines.ScriptEnginesManager;
import org.txm.rcp.editors.TxtEditor;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.statsengine.r.rcp.views.RConsole;

// TODO: Auto-generated Javadoc
/**
 * Handler: Execute a R script file in the same thread a the RCP.
 *
 * @author mdecorde
 */
public class ExecuteRScript extends AbstractHandler {

	private static final String ID = "org.txm.rcp.commands.ExecuteRScript"; //$NON-NLS-1$
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String result = ""; //$NON-NLS-1$
		IWorkbenchWindow aww = HandlerUtil.getActiveWorkbenchWindow(event);
		IWorkbenchPage page = aww.getActivePage();
		IViewPart view = page.findView(RConsole.ID);
		if (view != null && view instanceof RConsole) {
			((RConsole) view).connectToRWorkspace();
		}
		IWorkbenchPart part = page.getActivePart();

		ISelection selection = page.getSelection();
		if (part != null && part instanceof TxtEditor) {
			TxtEditor te = (TxtEditor) part;
			FileStoreEditorInput input = (FileStoreEditorInput) te
					.getEditorInput();

			File f = new File(input.getURI().getPath());
			result = f.getAbsolutePath();
		}
		else if (selection != null
				& selection instanceof IStructuredSelection) {
					File file = null;
					IStructuredSelection strucSelection = (IStructuredSelection) selection;
					for (Iterator<Object> iterator = strucSelection.iterator(); iterator
							.hasNext();) {
						file = (File) iterator.next();
						result = file.getAbsolutePath();
						break;
					}
				}

		if ("" == result) { //$NON-NLS-1$
			Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event)
					.getShell();
			FileDialog dialog = new FileDialog(shell, SWT.OPEN);
			if (LastOpened.getFile(ID) != null) {
				dialog.setFilterPath(LastOpened.getFolder(ID));
				dialog.setFileName(LastOpened.getFile(ID));
			}
			dialog.setFilterExtensions(new String[] { "*.R" }); //$NON-NLS-1$

			String scriptCurrentDir = Toolbox.getTxmHomePath() + "/scripts"; //$NON-NLS-1$

			dialog.setFilterPath(scriptCurrentDir);
			result = dialog.open();
			if (result == null) return null;
			LastOpened.set(ID, new File(result));
		}

		ScriptEnginesManager.getREngine().executeScript(new File(result), new HashMap<String, Object>());
		return null;
	}

	public static void executeScript(String absolutePath) {
		ScriptEnginesManager.getREngine().executeScript(new File(absolutePath));
	}
}
