// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.statsengine.r.rcp.handlers;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.Toolbox;
import org.txm.rcp.commands.OpenBrowser;
import org.txm.rcp.editors.TableResultEditor;
import org.txm.rcp.swt.dialog.IntegerDialog;
import org.txm.rcp.swt.dialog.ListSelectionDialog;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.utils.logger.Log;

/**
 * Allow the user to display an histogram of values of a column of a TableViewer of a TableResultEditor
 * 
 * @author mdecorde
 */
public class HistogramFromTableResultEditor extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		// ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();

		IWorkbenchPart editor = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();

		if (editor instanceof TableResultEditor tre) {
			try {
				histogram(tre);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return null;
	}

	public static void histogram(TableResultEditor editor) throws Exception {
		int n = 0;
		ArrayList<String> columns = new ArrayList<String>();
		for (TableViewer v : editor.getTableViewers()) {
			if (v.getTable().isDisposed()) continue;
			n += v.getTable().getColumnCount();
			for (TableColumn c : v.getTable().getColumns()) {
				if (c.getText().trim().length() > 0) {
					columns.add(c.getText());
				} else {
					columns.add("<empty>");
				}
			}
		}

		ListSelectionDialog<String> dialog = new ListSelectionDialog<String>(Display.getCurrent().getActiveShell(), "Which column to use", "Select the column containing the numeric values to use in the barplot.", columns, null);

		//InputDialog dialog = new InputDialog(Display.getCurrent().getActiveShell(), "Select column number to build the historgram", "Select a column number between 1 and "+n +" in columns\n"+StringUtils.join(columns, "\n"), "1", null);

		if (dialog.open() == Window.CANCEL) {
			System.out.println("Aborted.");
			return;
		}

		int iColName = dialog.getIndexSelectedValue();
		String colName = dialog.getSelectedValue();
		ArrayList<Double> values = new ArrayList<Double>();

		n = 0;
		for (TableViewer v : editor.getTableViewers()) {
			if (v.getTable().isDisposed()) continue;

			if (iColName > v.getTable().getColumnCount()) {
				iColName -= v.getTable().getColumnCount(); // go to the next table
			} else {

				for (TableItem item : v.getTable().getItems()) {
					try {
						values.add(Double.parseDouble(item.getText(iColName).replace(",", ".")));
					} catch (Exception e) {

					}
				}
				break; // done !!
			}
		}

		int ncluster = 10;
		IntegerDialog idialog = new IntegerDialog(Display.getCurrent().getActiveShell(), "N clusters", 10, 2, null);
		if (idialog.open() == Window.CANCEL) {
			System.out.println("Aborted.");
			return;
		}
		ncluster = idialog.getValue();

		if (colName != null && values.size() > 0) {

			// Création du fichier de sortie .svg
			new File(Toolbox.getTxmHomePath(), "results").mkdirs();
			File file = File.createTempFile("IndexHist", ".svg", new File(Toolbox.getTxmHomePath(), "results")); //$NON-NLS-1$

			// Récupération de R
			RWorkspace rw = RWorkspace.getRWorkspaceInstance();

			double[] values2 = new double[values.size()];
			for (int i = 0 ; i < values.size() ; i++) values2[i] = values.get(i);
			rw.addVectorToWorkspace("tmp", values2); //$NON-NLS-1$
			rw.voidEval("hc <- hclust(dist(tmp), method=\"single\")"); //$NON-NLS-1$
			rw.voidEval("cla <- cutree(hc, k="+ncluster+")"); //$NON-NLS-1$
			rw.voidEval("txm_color_palette_17 <- c(\"#FF0000\", \"#0000FF\", \"#00CD00\", \"#FFD700\", \"#FF1493\", \"#B22222\", \"#000080\", \"#008B00\", \"#FFB90F\", \"#800080\", \"#00BFFF\", \"#00FF00\", \"#FFFF00\", \"#FF00FF\", \"#00FFFF\", \"#ADFF2F\", \"#54FF9F\" )"); //$NON-NLS-1$
			rw.voidEval("par(2)"); //$NON-NLS-1$
			String script ="barplot(sort(tmp, decreasing=TRUE), col=txm_color_palette_17[cla], main=\""+ncluster+" clusters of "+colName+"\", xlab=\""+colName+"\")"; //$NON-NLS-1$
			rw.plot(file, script);
			System.out.println("Histogram saved in: "+file.getAbsolutePath()); //$NON-NLS-1$

			// Affichage du fichier de sortie .svg dans une nouvelle fenêtre de TXM
			OpenBrowser.openfile(file.getAbsolutePath());
		} else {
			Log.warning("No values found in column "+colName);
		}
	}
}
