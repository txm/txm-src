// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.statsengine.r.rcp.views;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.DecoratingLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.part.ViewPart;
import org.txm.Toolbox;
import org.txm.core.results.TXMResult;
import org.txm.objects.Project;
import org.txm.objects.Workspace;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.commands.R.CopySelection;
import org.txm.rcp.commands.R.DescribeSelection;
import org.txm.rcp.handlers.files.EditFile;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.statsengine.r.core.RResult;
import org.txm.statsengine.r.core.RTransformer;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.rcp.messages.RUIMessages;

/**
 * The Class RVariablesView: list the R variables linked to TXMResult that have a representation in the R workspace
 */
public class RVariablesView extends ViewPart {

	/** The tv. */
	TreeViewer tv;

	/** The ID. */
	public static String ID = RVariablesView.class.getName();

	/**
	 * Refresh.
	 */
	public static void refresh() {
		IWorkbenchWindow activeworksbench = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (activeworksbench == null)
			return;
		IWorkbenchPage activepage = activeworksbench.getActivePage();
		if (activepage == null)
			return;
		RVariablesView rview = (RVariablesView) activepage.findView(RVariablesView.ID);
		if (rview != null)
			rview.tv.refresh();
	}

	/**
	 * Reload.
	 */
	public static void reload() {

		IWorkbenchWindow activeworksbench = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (activeworksbench == null)
			return;
		IWorkbenchPage activepage = activeworksbench.getActivePage();
		if (activepage == null)
			return;
		RVariablesView rview = (RVariablesView) activepage.findView(RVariablesView.ID);
		if (rview != null) {
			rview._reload();
		}
	}

	/**
	 * _reload.
	 */
	public void _reload() {
		tv.setContentProvider(new RVariablesTreeProvider());
		tv.setLabelProvider(new RVariablesLabelProvider(new WorkbenchLabelProvider(), null));
		try {
			buildTreeContent();
			tv.refresh();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Builds the tree content.
	 *
	 */
	protected void buildTreeContent() {
		List<CQPCorpus> i = new ArrayList<CQPCorpus>();
		for (Project p : Workspace.getInstance().getProjects()) {
			i.addAll(p.getChildren(CQPCorpus.class));
		}
		tv.setInput(i);
	}

	/**
	 * Deep search.
	 *
	 * @param container the container
	 * @return true, if successful
	 */
	protected boolean deepSearch(TXMResult container) {
		boolean own = hasSymbol(container.getChildren());
		if (own) {
			return true;
		}
		else // continue deeper
		{

			for (TXMResult children : container.getChildren()) {
				boolean childrencontainer = deepSearch(children);
				if (childrencontainer)
					return true;
			}
		}
		return false;
	}

	/**
	 * Checks for symbol.
	 *
	 * @param results the results
	 * @return true, if successful
	 */
	private boolean hasSymbol(List<TXMResult> results) {
		for (Object rez : results) {
			if (rez instanceof RResult)
				return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		parent.setLayout(new GridLayout(2, false));
		Button refreshButton = new Button(parent, SWT.PUSH);
		refreshButton.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
		refreshButton.setToolTipText(RUIMessages.refreshVariables);
		refreshButton.setImage(IImageKeys.getImage(IImageKeys.REFRESH));
		refreshButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				//System.out.println("reload");
				_reload();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});

		Button logs = new Button(parent, SWT.PUSH);
		logs.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		logs.setText(RUIMessages.showEvalLogs);
		logs.setToolTipText("Show the R evaluation history");
		logs.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				//System.out.println("reload");

				try {
					File homedir = new File(Toolbox.getTxmHomePath());
					long time = System.currentTimeMillis();
					File tmp = new File(homedir, "REvalLogs.txt"); //$NON-NLS-1$
					RWorkspace.saveEvalLog(tmp);
					EditFile.openfile(tmp.getAbsolutePath());
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					org.txm.utils.logger.Log.printStackTrace(e);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});

		tv = new TreeViewer(parent);
		tv.getTree().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 2, 1));
		//_reload();

		tv.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
			}
		});

		tv.getTree().addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				/*
				 * if (e.keyCode == SWT.DEL) {
				 * DeleteObject.delete(tv.getSelection());
				 * }
				 * else if (e.keyCode == SWT.F5) {
				 * reload();
				 * }
				 */
				if ((char) e.keyCode == 'c' & ((e.stateMask & SWT.CTRL) != 0)) {
					TreeSelection selection = (TreeSelection) tv.getSelection();
					try {
						CopySelection.copyFirst(selection);
						tv.refresh();
					}
					catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} else if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					TreeSelection selection = (TreeSelection) tv.getSelection();
					DescribeSelection.describeFirst(selection);
					tv.refresh();
				} else if (e.keyCode == SWT.F5) {
					tv.refresh();
				}
			}
		});

		MenuManager menuManager = new MenuManager();
		Menu menu = menuManager.createContextMenu(tv.getTree());

		// Set the MenuManager
		tv.getTree().setMenu(menu);
		getSite().registerContextMenu(menuManager, tv);
		// Make the selection available
		getSite().setSelectionProvider(tv);
		
		_reload();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

	/**
	 * The Class RVariablesLabelProvider.
	 */
	public class RVariablesLabelProvider extends DecoratingLabelProvider {

		public RVariablesLabelProvider(WorkbenchLabelProvider workbenchLabelProvider, Object object) {
			super(workbenchLabelProvider, null);
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
		 */
		@Override
		public String getText(Object element) {

			if (element instanceof RResult) {
				if (element instanceof TXMResult tr) {
					if (tr.hasBeenComputedOnce()) {
						return NLS.bind(RUIMessages.tXMNameP0SupSupRNameP1, ((TXMResult) element).getName(), ((RResult) element).getRSymbol());
					} else {
						return NLS.bind(RUIMessages.tXMNameP0SupSupRNameP1, ((TXMResult) element).getName(), "<not computed yet>");
					}
				}
				else {
					return ((RResult) element).getRSymbol();
				}
			}
			else if (element instanceof TXMResult) {
				return ((TXMResult) element).getName();
			}
			else {
				return element.toString();
			}
		}

		//		/*
		//		 * (non-Javadoc)
		//		 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
		//		 */
		//		@Override
		//		public final Image getImage(Object element) {
		//			if (element instanceof TXMResult r)
		//				return r.getIc;
		//			else if (element instanceof CQPCorpus)
		//				return IImageKeys.getImage(IImageKeys.CORPUS);
		//			else if (element instanceof Partition)
		//				return IImageKeys.getImage(IImageKeys.PARTITION);
		//			return null;
		//		}
	}

	/**
	 * The Class RVariablesTreeProvider.
	 */
	public class RVariablesTreeProvider implements ITreeContentProvider {

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
		 */
		@Override
		public Object[] getChildren(Object element) {
			//System.out.println("get children of "+element);
			if (element instanceof TXMResult) {


				ArrayList<Object> rresults = new ArrayList<>();
				List<TXMResult> children = ((TXMResult) element).getChildren();

				for (TXMResult r : children) {
					if (r instanceof RResult) {
						rresults.add(r);
					} else if (RTransformer.hasRSymbol(r)) {
						rresults.add(r);
					}
					else if (deepSearch(r)) {
						rresults.add(r);
					}
				}
				//System.out.println(""+element+" > "+children);
				return rresults.toArray();
			}
			return new Object[0];
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
		 */
		@Override
		public Object getParent(Object element) {
			if (element instanceof TXMResult) {
				TXMResult r = (TXMResult) element;
				Partition p = r.getFirstParent(Partition.class);
				if (p != null) {
					return p;
				}
				CQPCorpus c = r.getFirstParent(CQPCorpus.class);
				if (c != null) {
					return c;
				}

				return null;
			}
			else {
				return null;
			}
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
		 */
		@Override
		public boolean hasChildren(Object element) {
			if (element instanceof TXMResult) {
				return getChildren(element).length > 0;
			}
			return false;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
		 */
		@Override
		public Object[] getElements(Object element) {

			Collection<Project> input = (Collection<Project>) element;
			Object[] objects = new Object[input.size()];
			Iterator<Project> it = input.iterator();
			int i = 0;
			while (it.hasNext()) {
				objects[i] = it.next();
				i++;
			}
			return objects;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
		 */
		@Override
		public void dispose() {
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
		 */
		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}
}
