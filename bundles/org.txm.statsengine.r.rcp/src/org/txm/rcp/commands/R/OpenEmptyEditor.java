// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands.R;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.txm.Toolbox;
import org.txm.rcp.handlers.files.EditFile;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.RCPPreferences;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class OpenEmptyEditor.
 */
public class OpenEmptyEditor extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		File txmhome = new File(Toolbox.getTxmHomePath());
		File scripts = new File(txmhome, "scripts"); //$NON-NLS-1$
		scripts.mkdir();
		if (!scripts.exists()) {
			System.out.println(NLS.bind(TXMUIMessages.couldNotFindDirectoryColonP0, scripts));
			return null;
		}

		File Rscripts = new File(scripts, "R"); //$NON-NLS-1$
		Rscripts.mkdir();
		if (!Rscripts.exists()) {
			System.out.println(NLS.bind(TXMUIMessages.couldNotFindDirectoryColonP0, Rscripts));
			return null;
		}

		int nosession = RCPPreferences.getInstance().getInt(RCPPreferences.NO_SESSION);

		File temp = new File(Rscripts, "session" + nosession + ".R"); //$NON-NLS-1$ //$NON-NLS-2$

		//create newt session no
		nosession++;
		RCPPreferences.getInstance().put(RCPPreferences.NO_SESSION, nosession);

		try {
			temp.createNewFile();
		}
		catch (IOException e) {
			System.out.println(Log.toString(e));
			return null;
		}

		EditFile.openfile(temp.getAbsolutePath());

		return null;
	}

}
