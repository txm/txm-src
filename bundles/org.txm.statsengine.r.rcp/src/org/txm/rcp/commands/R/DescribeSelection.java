// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands.R;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.rosuda.REngine.REXP;
import org.txm.core.results.TXMResult;
import org.txm.statsengine.r.core.RResult;
import org.txm.statsengine.r.core.RTransformer;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

import cern.colt.Arrays;
import jline.internal.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class CopySelection.
 */
public class DescribeSelection extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);

		describeFirst(selection);

		return null;
	}

	/**
	 * Copy first.
	 *
	 * @param selection the selection
	 */
	public static void describeFirst(IStructuredSelection selection) {
		Object selectedItem = selection.getFirstElement();
		String symbol = null;
		try {
			if (selectedItem instanceof TXMResult r) {
				r.compute(false);
				if (selectedItem instanceof RResult rr) {
					symbol = rr.getRSymbol();
				}
				else {
					String previousSymbol = RTransformer.getTMPSymbol(r);
					symbol = previousSymbol;
				}
			}
			if (symbol != null) {

				REXP rexp = RWorkspace.getRWorkspaceInstance().eval("capture.output(str("+symbol+"))");
				System.out.println(StringUtils.join(rexp.asStrings(), "\n"));

			} else {
				Log.info("Selection has not R symbol.");
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
