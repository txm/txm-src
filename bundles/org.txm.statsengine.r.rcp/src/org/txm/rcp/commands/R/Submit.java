// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.rcp.commands.R;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.txm.rcp.editors.TxtEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.utils.JobHandler;
import org.txm.statsengine.r.rcp.handlers.ExecuteRScript;
import org.txm.statsengine.r.rcp.handlers.ExecuteRText;

/**
 * The Class Submit. Deprecated due to ExecuteScript command
 */
@Deprecated
public class Submit extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {

		IWorkbenchWindow wb = HandlerUtil.getActiveWorkbenchWindow(event);
		IWorkbenchPage ap = wb.getActivePage();
		IWorkbenchPart editor = ap.getActivePart();

		if (editor == null)
			return null;

		if (editor instanceof TxtEditor) {
			final TxtEditor Teditor = (TxtEditor) editor;
			final ISelection selection = Teditor.getSelectionProvider().getSelection();
			JobHandler job = new JobHandler(TXMUIMessages.saveScript, true) {

				@Override
				protected IStatus _run(final SubMonitor monitor) {
					this.syncExec(new Runnable() {

						@Override
						public void run() {
							if (selection == null) {
								Teditor.doSave(monitor);
								File script = Teditor.getEditedFile();
								ExecuteRScript.executeScript(script.getAbsolutePath());
							}
							else {
								if (selection instanceof TextSelection) {
									TextSelection t = (TextSelection) selection;
									String result = t.getText();

									if (result.length() > 0) {
										ExecuteRText.executeText(result);
									}
									else {
										Teditor.doSave(monitor);
										FileStoreEditorInput input = (FileStoreEditorInput) Teditor.getEditorInput();
										File script = new File(input.getURI().getPath());
										ExecuteRScript.executeScript(script.getAbsolutePath());
									}
								}
							}
						}
					});
					return Status.OK_STATUS;
				}
			};
			job.schedule();

		}
		return event;
	}
}
