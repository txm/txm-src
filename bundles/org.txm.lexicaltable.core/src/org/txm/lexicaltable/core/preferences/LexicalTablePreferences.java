package org.txm.lexicaltable.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Default preferences initializer.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class LexicalTablePreferences extends TXMPreferences {


	/**
	 * If true, the table computed from an Index will use all the occurrences from the corpus to add the #RESTE# line composed of the others words
	 */
	public static final String USE_ALL_OCCURRENCES = "use_all_occurrences"; //$NON-NLS-1$

	/**
	 * If true, the table data has been manually set with lt.setData(lexicalTableImpl)
	 */
	public static final String EXTERN_DATA = "extern_data"; //$NON-NLS-1$

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(LexicalTablePreferences.class)) {
			new LexicalTablePreferences();
		}
		return TXMPreferences.instances.get(LexicalTablePreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.putInt(F_MIN, 2);
		preferences.putInt(F_MAX, Integer.MAX_VALUE);
		preferences.putInt(V_MAX, 200);
		preferences.putBoolean(USE_ALL_OCCURRENCES, true);
		preferences.putBoolean(EXTERN_DATA, false);
		preferences.put(UNIT_PROPERTY, DEFAULT_UNIT_PROPERTY);
	}

}
