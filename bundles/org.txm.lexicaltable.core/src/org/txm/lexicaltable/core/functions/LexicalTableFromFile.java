package org.txm.lexicaltable.core.functions;

import java.io.File;

import org.eclipse.osgi.util.NLS;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.Parameter;
import org.txm.lexicaltable.core.statsengine.r.data.LexicalTableImpl;
import org.txm.objects.Laboratory;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * Lexical Table computed using data stored in a Table file
 * 
 * @author mdecorde
 * 
 */
public class LexicalTableFromFile extends LexicalTable {

	@Parameter(key = "file")
	private File sourceFile;

	public LexicalTableFromFile(String node) {

		super(node);
	}

	public LexicalTableFromFile(Laboratory corpus) {

		super(corpus);
	}

	public boolean _compute(TXMProgressMonitor monitor) throws Exception {

		this.pExternData = true;
		File tmp = new File(this.getProject().getProjectDirectory(), sourceFile.getName());
		if (!tmp.exists()) {
			tmp = sourceFile;
		}
		if (!tmp.exists()) {
			Log.warning(NLS.bind("Lexical table source file not found in {0} nor in {1}.", sourceFile, tmp));
			return false;
		}

		String[] rowNames = {};
		String[] colNames = {};
		int[][] matrix = new int[0][0];
		this.statsData = new LexicalTableImpl(matrix, rowNames, colNames);

		String encoding = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_ENCODING);
		String colseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_COL_SEPARATOR);
		String txtseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_TXT_SEPARATOR);
		
		if (this.getData().importData(tmp, encoding, colseparator, txtseparator)) {
			return super._compute(monitor);
		}
		else {
			return false;
		}
	}

	public void setSourceFile(File file) {

		this.sourceFile = file;
	}

	@Override
	public String getComputingStartMessage() {

		return NLS.bind("Computing Lexical table on {0}", this.sourceFile);
	}
}
