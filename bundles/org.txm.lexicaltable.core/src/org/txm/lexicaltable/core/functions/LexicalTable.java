package org.txm.lexicaltable.core.functions;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.index.core.functions.Line;
import org.txm.index.core.functions.PartitionIndex;
import org.txm.lexicaltable.core.messages.LexicalTableCoreMessages;
import org.txm.lexicaltable.core.preferences.LexicalTablePreferences;
import org.txm.lexicaltable.core.statsengine.data.ILexicalTable;
import org.txm.lexicaltable.core.statsengine.r.data.LexicalTableImpl;
import org.txm.objects.Laboratory;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CQPLexicon;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.statsengine.core.data.Vector;
import org.txm.statsengine.r.core.RResult;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * Holds the word frequencies table of a {@link Partition} seen through a word property.
 * Until the LT is computed the internal R table is set to an empty matrix with rownames and colnames sets.
 *
 * @author mdecorde
 * @author sjacquot
 *
 */
public class LexicalTable extends TXMResult implements RResult {

    public static Integer LEXICALTABLE_COUNTER = new Integer(1);

    public static String SYMBOL_BASE = "LexicalTable_"; //$NON-NLS-1$

    /** stores the computed data */
    protected ILexicalTable statsData;

    /**
     * The word property.
     */
    @Parameter(key = TXMPreferences.UNIT_PROPERTY)
    private WordProperty property;

    /**
     * Minimum frequency.
     */
    @Parameter(key = TXMPreferences.F_MIN)
    private int fMinFilter;

    /**
     * Maximum frequency.
     */
    @Parameter(key = TXMPreferences.F_MAX)
    private int fMaxFilter;

    /**
     * Maximum number of lines spinner.
     */
    @Parameter(key = TXMPreferences.V_MAX)
    private int vMaxFilter;

    /**
     * if true and if built from a partition index, the #RESTE# line is added containing the cumulated frequencies of the occurrences not counted in the lexical table (corpus frequencies minus the
     * index frequencies)
     */
    @Parameter(key = LexicalTablePreferences.USE_ALL_OCCURRENCES)
    protected boolean useAllOccurrences;

    /**
     * true if the data has been set with lt.setData(...)
     */
    @Parameter(key = LexicalTablePreferences.EXTERN_DATA)
    protected boolean pExternData;

    /**
     * Build a LexicalTable with the Subcorpus words and Subcorpus parent remaining words.
     *
     * @param corpus
     * @throws Exception
     */
    public LexicalTable(Subcorpus corpus) throws Exception {
        super(corpus);
        try { // create empty LT to store infos
            String[] rowNames = {};
            String[] colNames = { corpus.getName(), NLS.bind("{0}-{1}", corpus.getCorpusParent().getName(), corpus.getName()) };
            int[][] matrix = new int[0][2];
            this.statsData = new LexicalTableImpl(matrix, rowNames, colNames);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<double[]> tableToColumnsList(LexicalTable table) throws Exception {
        List<double[]> cols = new ArrayList<>();

        int Ncols = table.getNColumns();
        for (int i = 0; i < Ncols; i++) {
            cols.add(table.getData().getCol(i).asDoubleArray());
        }
        return cols;
    }

    /**
     *
     * @param index
     */
    public LexicalTable(PartitionIndex index) {
        super(index);

        this.property = index.getProperties().get(0);
        this.useAllOccurrences = true;

        try { // create empty LT to store infos
            String[] rowNames = {};
            List<String> names = index.getPartition().getPartNames();
            String[] colNames = names.toArray(new String[names.size()]);
            int[][] matrix = new int[0][names.size()];
            this.statsData = new LexicalTableImpl(matrix, rowNames, colNames);
        }
        catch (Exception e) {
            Log.printStackTrace(e);
        }
    }

    /**
     *
     * @param partition
     */
    public LexicalTable(Partition partition) {
        super(partition);

        try { // create empty LT to store infos
            String[] rowNames = {};
            partition.compute(false);
            List<String> names = partition.getPartNames();
            String[] colNames = names.toArray(new String[names.size()]);
            int[][] matrix = new int[0][names.size()];
            this.statsData = new LexicalTableImpl(matrix, rowNames, colNames);
        }
        catch (Exception e) {
            Log.printStackTrace(e);
        }
    }

    public String[] getExportTXTExtensions() {
        return new String[] {
                "*.xlsx", "*.ods", "*.csv", "*.Rdata", "*.rds"
        };
    }

    public String[] getImportTXTExtensions() {
        return new String[] {
        		"*.xlsx", "*.ods", "*.csv", "*.Rdata", "*.rds"
        };
    }

    public LexicalTable(String parametersNodePath) {
        super(parametersNodePath);
    }

    public LexicalTable(CQPCorpus corpus) {
        super(corpus);

        try { // create empty LT to store infos
            String[] rowNames = {};
            String[] colNames = {};
            int[][] matrix = new int[0][0];
            this.statsData = new LexicalTableImpl(matrix, rowNames, colNames);
        }
        catch (Exception e) {
            Log.printStackTrace(e);
        }
    }

    public LexicalTable(Laboratory sandbox) {
        super(sandbox);
        try { // create empty LT to store infos
            String[] rowNames = {};
            String[] colNames = {};
            int[][] matrix = new int[0][0];
            this.statsData = new LexicalTableImpl(matrix, rowNames, colNames);
        }
        catch (Exception e) {
            Log.printStackTrace(e);
        }
    }

    @Override
    public boolean loadParameters() {
        try {
            if (this.getCorpus() != null) {
                this.property = this.getCorpus().getProperty(this.getStringParameterValue(TBXPreferences.UNIT_PROPERTY));
            }
        }
        catch (CqiClientException e) {
            Log.printStackTrace(e);
            return false;
        }
        return true;
    }

    @Override
    public boolean saveParameters() {
        this.saveParameter(TBXPreferences.UNIT_PROPERTY, this.property);
        return true;
    }

    /**
     *
     * @param partition
     * @throws Exception
     */
    protected LexicalTableImpl _computeFromPartition(Partition partition, TXMProgressMonitor monitor) throws Exception {

        // parts lexicons
        List<CQPLexicon> partsLexicons = new ArrayList<>();
        for (int i = 0; i < partition.getPartsCount(); i++) {
            partsLexicons.add(CQPLexicon.getLexicon(partition.getParts().get(i), partition.getParts().get(i).getProperty(property.getFullName()), monitor.createNewMonitor(1), false));
        }

        // Corpus global lexicon
        CQPLexicon corpusLexicon = CQPLexicon.getLexicon(partition.getParent(), this.property, monitor.createNewMonitor(10), false);

        ArrayList<String> filteredForms = new ArrayList<>();
        // create a copy and filter line with Fmin;
        for (int i = 0; i < corpusLexicon.getFreq().length; i++) {
            int f = corpusLexicon.getFreq()[i];
            if (this.fMinFilter <= f && f <= this.fMaxFilter) {
                filteredForms.add(corpusLexicon.getForms()[i]);
            }
        }
        Map<String, Integer> entries2index = new HashMap<>();
        for (int i = 0; i < filteredForms.size(); i++) {
            entries2index.put(filteredForms.get(i), i);
        }

        int[][] mat = new int[filteredForms.size()][partsLexicons.size()];

        Integer id = null;
        for (int i = 0; i < partsLexicons.size(); i++) {
            CQPLexicon l = partsLexicons.get(i);
            String[] ents = l.getForms();
            int[] freqs = l.getFreq();
            for (int j = 0; j < freqs.length; j++) {
                id = entries2index.get(ents[j]);
                if (id != null) {
                    mat[id][i] = freqs[j];
                }
            }
        }

        try { // clean R objet befor resetting it
            if (statsData != null) {
                RWorkspace.getRWorkspaceInstance().removeVariableFromWorkspace(statsData.getSymbol());
            }
        }
        catch (RWorkspaceException e) {
            e.printStackTrace();
            return null;
        }

        return new LexicalTableImpl(mat, filteredForms.toArray(new String[] {}), partition.getPartNames().toArray(new String[] {}));
    }

    /**
     * Creates the lexical table.
     *
     * @param indexes the vocabularies
     * @return the lexical table
     * @throws RWorkspaceException the r workspace exception
     */
    protected LexicalTableImpl _computeFromIndexes(List<PartitionIndex> indexes) throws RWorkspaceException {

        Log.fine(LexicalTableCoreMessages.bind(LexicalTableCoreMessages.info_buildingLexicalTableWithP0, indexes));

        PartitionIndex partIndex = indexes.get(0);// FRIGO
        Partition partition = partIndex.getPartition(); // the Index is computed on a partition

        if (!partIndex.isComputedWithPartition()) {
            throw new IllegalArgumentException("Index is not computed with a partition. Aborting."); //$NON-NLS-1$
        }

        if (!this.property.getFullName().equals(partIndex.getProperties().get(0).getFullName())) {
            Log.info(NLS.bind("The Lexical table @{0} word property has been ignored. The index {1} properties are used.", this.property, WordProperty.asString(partIndex.getProperties())));
        }

        if (partIndex.size() <= 1 && !this.useAllOccurrences) { // cant buid LT of one line. If the #REST line is added, we'll have 2 lines \o/
            Log.warning("Error: cannot build a lexical table with less than 2 lines");
            return null;
        }

        this.property = partIndex.getProperties().get(0);

        HashMap<String, Line> alllines = new HashMap<>();
        // merge lines of all indexes
        for (PartitionIndex voc : indexes) {
            for (Line l : voc.getAllLines()) {
                if (alllines.containsKey(l.getSignature())) {
                    Line ll = alllines.get(l.getSignature());
                    int[] c1 = ll.getFrequencies();
                    int[] c2 = l.getFrequencies();
                    for (int i = 0; i < c1.length; i++) {
                        c2[i] += c1[i];
                    }
                    ll.setCounts(c2, 0.0f);
                }
                else {
                    alllines.put(l.toString(), l);
                }
            }
        }

        List<String> colnames = partIndex.getPartNames();

        Collection<Line> lines = alllines.values();
        List<String> rownames = new ArrayList<>(lines.size());
        for (Line l : lines) {
            rownames.add(l.toString());
        }
        int extra = 0;
        if (this.useAllOccurrences) {
            extra = 1;
        }

        // String[] entries = new String[alllines.size() + extra];

        int[][] mat = new int[rownames.size() + extra][colnames.size()];
        int[] margins = new int[colnames.size()]; // compute margins
        int i = 0;
        for (Line l : lines) {
            for (int j = 0; j < colnames.size(); j++) {
                mat[i][j] = l.getFrequency(j);
                margins[j] += l.getFrequency(j);
            }
            // entries[i++] = l.toString();
            i++;
        }

        if (this.useAllOccurrences) {
            try {
                int[] partitionSizes = partition.getPartSizes();
                int[] reste = new int[partitionSizes.length];

                // System.out.println("margins : "+Arrays.toString(margins));
                // System.out.println("partsizes : "+Arrays.toString(partitionSizes));

                for (i = 0; i < reste.length; i++) {
                    reste[i] = partitionSizes[i] - margins[i];
                    if (reste[i] < 0) {
                        Log.severe(LexicalTableCoreMessages.error_marginIsHigherThanThePartitionSize);
                        throw new IllegalArgumentException(LexicalTableCoreMessages.error_marginIsHigherThanThePartitionSize);
                    }
                    mat[lines.size()][i] = reste[i];
                }
                // entries[lines.size()] = "#RESTE#"; //$NON-NLS-1$
                rownames.add("#RESTE#"); //$NON-NLS-1$
                // System.out.println("rownames: "+rownames);
                // System.out.println("reste : "+Arrays.toString(reste));
            }
            catch (CqiClientException e) {
                Log.printStackTrace(e);
                return null;
            }
        }
        // System.out.println("mat size : ["+(rownames.size() + extra)+"]["+colnames.size()+"]");
        // System.out.println("rownames size : "+rownames.size());
        // System.out.println("colnames size : "+colnames.size());
        return new LexicalTableImpl(mat, rownames.toArray(new String[] {}), colnames.toArray(new String[] {}));
    }

    @Override
    protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

        if (this.pExternData) {
            return this.statsData != null;
        }

        // update the F max filter maximum value to match the parent corpus size
        if(this.fMaxFilter == Integer.MAX_VALUE) {
            Partition partition = this.getPartition();
            if(partition != null) {
            	this.fMaxFilter = partition.getTotalSize();
            }
            //TODO: SJ: to also match the parent subcorpus or corpus size
//            else {
//            	CQPCorpus corpus = this.getCorpus();
//	            if (corpus != null) {
//	                this.fMaxFilter = corpus.getSize();
//	            }
//            }
        }

        if (this.parent instanceof Subcorpus) {
            Subcorpus subcorpus = (Subcorpus) this.parent;
            CQPCorpus parentCorpus = subcorpus.getCorpusParent();
            CQPLexicon l1 = CQPLexicon.getLexicon(parentCorpus, this.property, monitor.createNewMonitor(20), false);
            CQPLexicon l2 = CQPLexicon.getLexicon(subcorpus, this.property, monitor.createNewMonitor(20), false);
            this.statsData = new LexicalTableImpl(getNextName(), l1, l2);
        }
        else if (this.parent instanceof Partition) {
            this.statsData = this._computeFromPartition((Partition) this.parent, monitor);
        }
        else if (this.parent instanceof PartitionIndex) {
            this.statsData = this._computeFromIndexes(Arrays.asList((PartitionIndex) this.parent));
        }

        if (this.statsData == null) { // no statsData computed or given
            return false;
        }

        // apply filtering
        this.statsData.filter(this.vMaxFilter, this.fMinFilter, this.fMaxFilter);

        return true;
    }

    @Override
    public boolean canCompute() {
        // can be computed on partition or corpus
        if (this.parent == null) {
            return false;
        }
        if (this.property == null) {
            Log.finest("LexicalTable.canCompute(): can not compute with no unit property."); //$NON-NLS-1$
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    protected static String getNextName() {
        return SYMBOL_BASE + (LEXICALTABLE_COUNTER++);
    }

    @Override
    public void clean() {
        try {
            if (statsData != null) {
                RWorkspace.getRWorkspaceInstance().removeVariableFromWorkspace(statsData.getSymbol());
            }
        }
        catch (RWorkspaceException e) {
            Log.printStackTrace(e);
        }
    }

    @Override
    public String getRSymbol() {
        if (statsData == null) return null;

        return statsData.getSymbol();
    }

    public Vector getColMarginsVector() {
        try {
            return statsData.getColMarginsVector();
        }
        catch (Exception e) {
            return null;
        }
    }

    /**
     * Gets the column names.
     *
     * @return
     */
    public Vector getColNames() {
        try {
            return statsData.getColNames();
        }
        catch (Exception e) {
            Log.printStackTrace(e);
            return null;
        }
    }

    public ILexicalTable getData() {
        return statsData;
    }

    @Override
    public String getDetails() {
        if (hasBeenComputedOnce()) {
            try {
                return "fMin=" + this.getFMin() + " V=" + this.getNRows();
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "";
            }
        }
        else {
            return "fMinFilter=" + this.fMinFilter + (Integer.MAX_VALUE == fMaxFilter ? " fMaxFilter=" + fMaxFilter : "") + " vMaxFilter=" + vMaxFilter;
        }
    }

    /**
     * Gets the minimum frequency filter.
     *
     * @return
     */
    public int getFMin() {
        return statsData.getFMin();
    }

    /**
     * Gets the minimum frequency filter.
     *
     * @return the minimum frequency filter
     */
    public int getFMinFilter() {
        return this.fMinFilter;
    }

    /**
     * Gets the maximum frequency filter.
     *
     * @return the maximum frequency filter
     */
    public int getFMaxFilter() {
        return this.fMaxFilter;
    }

    @Override
    public String getSimpleName() {
        StringBuilder strb = new StringBuilder();
        if (this.property != null) {
            strb.append(this.property.asString());
            try {
                strb.append(TXMCoreMessages.formatMinFilter(this.fMinFilter));
                strb.append(TXMCoreMessages.formatMaxFilter(this.fMaxFilter));
                strb.append(TXMCoreMessages.formatVMaxFilter(this.vMaxFilter));
            }
            catch (Exception e) {
                Log.printStackTrace(e);
                return this.getEmptyName();
            }
            return strb.toString();
        }
        else {
            return this.getEmptyName();
        }
    }

    @Override
    public String getName() {
        try {
            return this.getParent().getName() + TXMPreferences.PARENT_NAME_SEPARATOR + this.getSimpleName();
        }
        catch (Exception e) {
            Log.printStackTrace(e);
        }
        return ""; //$NON-NLS-1$
    }

    @Override
    public String getComputingStartMessage() {
        // from partition index
        if (this.parent instanceof PartitionIndex) {
            return NLS.bind(LexicalTableCoreMessages.info_creatingLexicalTableWithTheP0PartitionIndex, this.parent.getName());
        }
        // from partition
        else if (this.getPartition() != null) {
            return NLS.bind(LexicalTableCoreMessages.info_creatingLexicalTableWithTheP0PartitionCommaPropertiesP1, this.getPartition().getName(), this.property.asString());
        }
        else if (this.parent instanceof Subcorpus) { // from subcorpus
            return NLS.bind(LexicalTableCoreMessages.info_creatingLexicalTableWithTheP0CorpusAndTheP1Subcorpus, this.parent.getParent().getName(), this.parent.getName());
        }
        // from main corpus
        else {
            return NLS.bind("Computing Lexical table on {0}", this.parent.getName());
        }
    }

    /**
     * Gets the columns count.
     *
     * @return
     * @throws Exception
     */
    public int getNColumns() throws Exception {
        if (statsData != null) {
            return statsData.getNColumns();
        }
        else if (parent instanceof Partition) {
            return ((Partition) parent).getPartsCount();
        }
        else if (parent instanceof PartitionIndex) {
            return ((PartitionIndex) parent).getPartition().getPartsCount();
        }
        return 1;
    }

    /**
     * Gets the rows count.
     *
     * @return
     * @throws Exception
     */
    public int getNRows() throws Exception {
        if (statsData != null) {
            return statsData.getNRows();
        }
        else {
            return 0;
        }
    }

    /**
     * Gets the corpus used by this lexical table.
     *
     * @return the corpus
     */
    public CQPCorpus getCorpus() {
        return CQPCorpus.getFirstParentCorpus(this);
    }

    /**
     * Gets the parent partition of this lexical table.
     *
     * @return the partition
     */
    public Partition getPartition() {
        return Partition.getFirstParentPartition(this);
    }

    /**
     * Get the property this lexical table is bound to.
     *
     * @return the property.
     */
    public WordProperty getProperty() {
        return property;
    }

    public Vector getRow(int i) throws Exception {
        return statsData.getRow(i);
    }

    public Vector getRowMarginsVector() throws Exception {
        return statsData.getRowMarginsVector();
    }

    public Vector getRowNames() throws Exception {
        if (statsData != null) {
            return statsData.getRowNames();
        }
        else {
            return null;
        }
    }

    /**
     * Gets the maximum number of lines filter.
     *
     * @return the maximum number of lines filter
     */
    public int getVMaxFilter() {
        return vMaxFilter;
    }


    /**
     * Sets the maximum number of lines filter
     *
     * @param vMax the maximum number of lines filter
     */
    public void setVMaxFilter(int vMax) {
        this.vMaxFilter = vMax;
    }

    /**
     * Sets the minimum frequency filter.
     *
     * @param fMin
     */
    public void setFMinFilter(int fMin) {
        this.fMinFilter = fMin;
    }

    /**
     * Sets the maximum frequency filter.
     *
     * @param fMax
     */
    public void setFMaxFilter(int fMax) {
        this.fMaxFilter = fMax;
    }

    /**
     * see LexicalTablePreferences.PROPERTY, LexicalTablePreferences.F_MIN, LexicalTablePreferences.V_MAX and LexicalTablePreferences.USEALLOCCURENCES
     */
    // public boolean setParameters(TXMParameters parameters) {
    // try {
    // if (parameters.get(LexicalTablePreferences.PROPERTY) != null) {
    // Object o = parameters.get(LexicalTablePreferences.PROPERTY);
    // if (o instanceof Property) this.pProperty = (Property) o;
    // else if (o instanceof String) this.pProperty = this.getCorpus().getProperty(o.toString());
    // }
    // if (parameters.get(LexicalTablePreferences.F_MIN) != null) {
    // Object o = parameters.get(LexicalTablePreferences.F_MIN);
    // if (o instanceof Integer) this.pFminFilter = (Integer)o;
    // else if (o instanceof String) this.pFminFilter = Integer.parseInt(o.toString());
    // }
    // if (parameters.get(LexicalTablePreferences.V_MAX) != null) {
    // Object o = parameters.get(LexicalTablePreferences.V_MAX);
    // if (o instanceof Integer) this.pVMaxFilter = (Integer)o;
    // else if (o instanceof String) this.pVMaxFilter = Integer.parseInt(o.toString());
    // }
    // if (parameters.get(LexicalTablePreferences.USEALLOCCURENCES) != null) {
    // Object o = parameters.get(LexicalTablePreferences.USEALLOCCURENCES);
    // if (o instanceof Boolean) this.pUseAllOccurrences = (Boolean)o;
    // else if (o instanceof Integer) this.pUseAllOccurrences = (Integer)o > 0;
    // else if (o instanceof String) this.pUseAllOccurrences = Boolean.parseBoolean(o.toString());
    // }
    //
    // dirty = true;
    // } catch (Exception e) {
    // System.out.println("Error while setting LexicalTable parameters: "+e.getLocalizedMessage());
    // Log.printStackTrace(e);
    // return false;
    // }
    // return true;
    // }

    @Override
    public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) {
        if (outfile.getName().endsWith(".Rdata") || outfile.getName().endsWith(".rds")) {
            try {
                return RResult.exportRdataOrRDS(this, outfile);
            }
            catch (RWorkspaceException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return false;
            }
        }
        else {
            return statsData.toTxt(outfile, encoding, colseparator, txtseparator);
        }
    }

    /**
     * Sets the unit property.
     *
     * @param property
     */
    public void setUnitProperty(WordProperty property) {
        this.property = property;
    }


    public void setParameters(WordProperty prop, Integer fmin, Integer fmax, Integer vmax, Boolean useAllOccurrences) {
        if (prop != null) {
            this.property = prop;
        }
        if (fmin != null) {
            this.fMinFilter = fmin;
        }
        if (fmax != null) {
            this.fMaxFilter = fmax;
        }
        if (vmax != null) {
            this.vMaxFilter = vmax;
        }
        if (useAllOccurrences != null) {
            this.useAllOccurrences = useAllOccurrences;
        }
        this.setDirty();
    }

    /**
     * @param useAllOccurrences the useAllOccurrences to set
     */
    public void setUseAllOccurrences(boolean useAllOccurrences) {
        this.useAllOccurrences = useAllOccurrences;
    }

    @Override
    public String getResultType() {
        return LexicalTableCoreMessages.RESULT_TYPE;
    }

    /**
     * Use this method to set manually data to the lexical table
     *
     * @param lexicalTableImpl external data to use. If null the table will be computed using its parent
     */
    public void setData(LexicalTableImpl lexicalTableImpl) {
        this.statsData = lexicalTableImpl;

        this.pExternData = statsData != null;
        setAltered();
    }

    /**
     * @return the minimum dimension size
     */
	public int getMinDimension() throws Exception {
		
		if (!hasBeenComputedOnce()) {
			return 0;
		}
		
		return Math.min(this.getNColumns(), this.getNRows());
	}
}
