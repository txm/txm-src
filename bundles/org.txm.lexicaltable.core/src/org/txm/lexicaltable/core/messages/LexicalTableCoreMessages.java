package org.txm.lexicaltable.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Lexical table core messages.
 * 
 * @author sjacquot
 *
 */
public class LexicalTableCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.lexicaltable.core.messages.messages"; //$NON-NLS-1$

	public static String RESULT_TYPE;

	public static String info_buildingLexicalTableWithP0;

	public static String info_creatingLexicalTableWithTheP0PartitionCommaPropertiesP1;

	public static String info_creatingLexicalTableWithTheP0CorpusAndTheP1Subcorpus;

	public static String info_creatingLexicalTableWithTheP0PartitionIndex;


	public static String error_marginIsHigherThanThePartitionSize;

	public static String error_notEnoughColumnsP0MinimumIs2;

	public static String error_lineP0DoesNotHaveP1olumns;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, LexicalTableCoreMessages.class);
	}
}
