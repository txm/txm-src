// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-09-19 10:31:00 +0200 (Mon, 19 Sep 2016) $
// $LastChangedRevision: 3298 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.lexicaltable.core.statsengine.r.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.rosuda.REngine.REXPMismatchException;
import org.txm.lexicaltable.core.messages.LexicalTableCoreMessages;
import org.txm.lexicaltable.core.statsengine.data.ILexicalTable;
import org.txm.searchengine.cqp.corpus.CQPLexicon;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.core.data.Vector;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.data.ContingencyTableImpl;
import org.txm.statsengine.r.core.data.VectorImpl;
import org.txm.statsengine.r.core.exceptions.RException;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix2D;



// TODO: Auto-generated Javadoc
/**
 * Implementation of the {@link ILexicalTable} interface, wrapping a R matrix.
 * 
 * A lexical table is a contingency table representing the frequencies of
 * linguistic types accross several sub-parts of a corpus.
 * 
 * Each column of the lexical table stand for a parts. Each row stand for a
 * linguistic type. Each cell give the frequency of the corresponding unit into
 * the corresponding part.
 * 
 * @author Sylvain Loiseau &lt;sloiseau@ens-lsh.fr&gt;
 * 
 */
public class LexicalTableImpl extends ContingencyTableImpl implements ILexicalTable {

	/** The sortedindex. */
	private int[] sortedindex;

	/**
	 * Instantiates a new lexical table impl.
	 *
	 * @param matrix the matrix
	 * @param formNames the form names
	 * @param partNames the part names
	 * @throws RWorkspaceException the r workspace exception
	 */
	private LexicalTableImpl(DoubleMatrix2D matrix, String[] formNames, String[] partNames) throws RWorkspaceException {
		super(matrix, formNames, partNames);
		initSortedIndex();
	}

	/**
	 * Instantiates a new lexical table impl.
	 *
	 * @param matrix the matrix
	 * @throws RWorkspaceException the r workspace exception
	 */
	public LexicalTableImpl(int[][] matrix, String[] rowNames, String[] colNames) throws RWorkspaceException {
		super(matrix, rowNames, colNames);
		initSortedIndex();
	}

	/**
	 * Instantiates a new lexical table impl.
	 *
	 * @param table the table
	 * @param symbol the symbol
	 * @throws RWorkspaceException the r workspace exception
	 */
	public LexicalTableImpl(LexicalTableImpl table, String symbol) throws RWorkspaceException {
		super(symbol);
		initSortedIndex();
	}


	/**
	 * Instantiates a new lexical table impl.
	 *
	 * @param symbol the symbol
	 * @throws RWorkspaceException the r workspace exception
	 */
	public LexicalTableImpl(String symbol) throws RWorkspaceException {
		super(symbol);
		initSortedIndex();
	}

	/** The nolex. */
	protected static int nolex = 1;

	/** The prefix r. */
	protected static String prefixR = "Lexicon_"; //$NON-NLS-1$

	/**
	 * Convert the Lexicon into a Vector object.
	 * TODO move this code somewhere
	 * 
	 * @return the vector
	 * @throws StatException the stat exception
	 */
	public static Vector asVector(CQPLexicon lex) throws StatException {
		String symbol = prefixR + (nolex++);
		VectorImpl v = new VectorImpl(lex.getFreq(), symbol);
		v.setRNames(lex.getForms());
		lex.setSymbol(v.getSymbol());
		return v;
	}


	/**
	 * 
	 * @param symbol
	 * @param corpusLexicon
	 * @param subcorpusLexicon
	 * @throws StatException
	 */
	public LexicalTableImpl(String symbol, CQPLexicon corpusLexicon, CQPLexicon subcorpusLexicon) throws StatException {
		super(symbol);
		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		Vector corpusLexiconV = asVector(corpusLexicon);
		Vector subcorpusLexiconV = asVector(subcorpusLexicon);

		// TODO: implement the R function : rw.callFunction("lexicons2LexicalTable", new QuantitativeDataStructure[] { corpusLexiconV, subcorpusLexiconV }, symbol); //$NON-NLS-1$

		rw.eval(symbol + " <- matrix(0, ncol = 2, nrow=length(" + corpusLexiconV.getSymbol() + "))");
		rw.eval("colnames(" + symbol + ") <- c(\"" + subcorpusLexicon.getParent().getName() + "\", \"" + corpusLexicon.getParent().getName() + "-" + subcorpusLexicon.getParent().getName() + "\")");
		// System.out.println("forms: "+Arrays.toString(corpusLexicon.getForms()));
		rw.addVectorToWorkspace("ltnames", corpusLexicon.getForms());
		rw.eval("rownames(" + symbol + ") <- ltnames");
		rw.eval(symbol + "[names(" + corpusLexiconV.getSymbol() + "),2] <- " + corpusLexiconV.getSymbol());
		rw.eval(symbol + "[names(" + subcorpusLexiconV.getSymbol() + "),2] <- " + symbol + "[names(" + subcorpusLexiconV.getSymbol() + "),2] - " + subcorpusLexiconV.getSymbol());
		rw.eval(symbol + "[names(" + subcorpusLexiconV.getSymbol() + "),1] <- " + subcorpusLexiconV.getSymbol());

		// try {
		// System.out.println("colnames: "+Arrays.toString(rw.eval("colnames("+symbol+")").asStrings()));
		// System.out.println("ltnames: "+Arrays.toString(rw.eval("ltnames").asStrings()));
		// System.out.println("length ltnames: "+rw.eval("length(ltnames)").asInteger());
		// System.out.println("ncol: "+rw.eval("ncol("+symbol+")").asInteger());
		// System.out.println("nrow: "+rw.eval("nrow("+symbol+")").asInteger());
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }


	}

	/**
	 * Inits the sorted index.
	 */
	public void initSortedIndex() {
		int ncol = this.getNColumns();
		sortedindex = new int[ncol];
		for (int i = 0; i < ncol; i++) {
			sortedindex[i] = i;
		}
	}

	// FIXME: should be moved to an importer RCP extension
	@Deprecated
	private static int getNline(File f) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(f));
			int i = 0;

			while (reader.readLine() != null) {
				i++;
			}
			return i;
		}
		catch (Exception e) {
			return 0;
		}
	}

	// /**
	// * Creates the lexical table impl.
	// *
	// * @param partindex the partindex
	// * @param symbol the symbol
	// * @return the lexical table
	// * @throws RWorkspaceException the r workspace exception
	// */
	// static public LexicalTable createLexicalTableImpl(QueryIndex partindex,
	// String symbol) throws RWorkspaceException {
	// List<QueryIndex> qindexes = new ArrayList<QueryIndex>();
	// qindexes.add(partindex);
	//
	// return createLexicalTableImpl(qindexes, symbol);
	// }

	// /**
	// * Creates the lexical table impl.
	// *
	// * @param query indexes
	// * @param symbol the symbol
	// * @param useAllOccurrences
	// * @return the lexical table
	// * @throws RWorkspaceException the r workspace exception
	// */
	// static public LexicalTable createLexicalTableImpl(
	// List<QueryIndex> qindexes, String symbol)
	// throws RWorkspaceException {
	//
	// System.out.println(Messages.LexicalTableImpl_1 + qindexes);
	// QueryIndex partindex = qindexes.get(0);// FRIGO
	// if (!partindex.isComputedWithPartition())
	// return null;
	//
	// Partition partition = partindex.getPartition();
	// Property property = null;
	// try {
	// property = partindex.getCorpus().getProperties().get(0);
	// } catch (CqiClientException e) {
	// // TODO Auto-generated catch block
	// org.txm.utils.logger.Log.printStackTrace(e);
	// }
	//
	// HashMap<String, QueryIndexLine> alllines = new HashMap<String, QueryIndexLine>();
	// // merge lines of all indexes
	// for (QueryIndex voc : qindexes) {
	// for (QueryIndexLine l : voc.getLines()) {
	// alllines.put(l.getName(), l);
	// }
	// }
	//
	// List<String> colnames = partindex.getPartnames();
	//
	// Collection<QueryIndexLine> lines = alllines.values();
	// List<String> rownames = new ArrayList<String>(lines.size());
	// for (QueryIndexLine l : lines) {
	// rownames.add(l.getName());
	// }
	//
	// String[] entries = new String[alllines.size()];
	//
	// int[][] mat = new int[rownames.size()][colnames.size()];
	// int[] margins = new int[colnames.size()]; // compute margins
	// int i = 0;
	// for (QueryIndexLine l : lines) {
	// for (int j = 0; j < colnames.size(); j++) {
	// mat[i][j] = l.getFrequency(j);
	// margins[j] += l.getFrequency(j);
	// }
	// entries[i++] = l.toString();
	// }
	//
	// //System.out.println("mat size : ["+(rownames.size() + extra)+"]["+colnames.size()+"]");
	// //System.out.println("rownames size : "+rownames.size());
	// //System.out.println("colnames size : "+colnames.size());
	// LexicalTableImpl table = new LexicalTableImpl(mat, partition, property,
	// rownames.toArray(new String[] {}), colnames
	// .toArray(new String[] {}));
	// table.constructor_fmin = qindexes.get(0).getFmin();
	// return table;
	// }


	static public ILexicalTable createLexicalTableImpl(File tsvFile) throws IOException, RWorkspaceException {

		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(tsvFile), "UTF-8")); //$NON-NLS-1$
		String line = reader.readLine();
		String[] split = line.split("\t"); //$NON-NLS-1$
		int ncol = split.length;
		if (ncol <= 1) {
			throw new IOException(LexicalTableCoreMessages.bind(LexicalTableCoreMessages.error_notEnoughColumnsP0MinimumIs2, ncol));
		}

		int nlines = getNline(tsvFile);
		String[] forms = new String[nlines];
		int[][] freqs = new int[nlines][ncol - 1];

		int i = 0;
		while (line != null) {
			split = line.split("\t"); //$NON-NLS-1$
			if (split.length == ncol) {
				forms[i] = split[0];
				for (int j = 1; j < ncol; j++)
					freqs[i][j - 1] = Integer.parseInt(split[j]);
			}
			else {
				throw new IOException(LexicalTableCoreMessages.bind(LexicalTableCoreMessages.error_lineP0DoesNotHaveP1olumns, i, ncol));
			}
			i++;
			line = reader.readLine();
		}

		String[] colnames = new String[ncol - 1];
		for (int j = 0; j < ncol - 1; j++)
			colnames[j] = "forms" + (j + 1); //$NON-NLS-1$

		ILexicalTable lt = LexicalTableImpl.createLexicalTable(freqs, null, forms, colnames, 1);

		return lt;
	}

	/**
	 * Creates the lexical table.
	 *
	 * @param freqs the freqs
	 * @param prop the prop
	 * @param rownames the rownames
	 * @param colnames the colnames
	 * @param Fmin the fmin
	 * @return the lexical table
	 * @throws RWorkspaceException the r workspace exception
	 */
	public static ILexicalTable createLexicalTable(int[][] freqs, Property prop,
			String[] rownames, String[] colnames, int Fmin) throws RWorkspaceException {

		ArrayList<Integer> idx = new ArrayList<Integer>();
		for (int j = 0; j < rownames.length; j++) {
			int sum = 0;
			for (int i = 0; i < colnames.length; i++) {
				sum += freqs[j][i];
			}
			if (sum >= Fmin) {
				idx.add(j);
			}
		}

		DoubleMatrix2D mat = DoubleFactory2D.sparse.make(idx.size(), colnames.length, 0);

		int countline = 0;
		for (int j : idx) {
			for (int i = 0; i < colnames.length; i++) {
				mat.setQuick(countline, i, freqs[j][i]);
			}
			countline++;
		}

		String[] filteredrownames = new String[idx.size()];
		for (int i = 0; i < idx.size(); i++) {
			filteredrownames[i] = rownames[idx.get(i)];
		}

		LexicalTableImpl table = new LexicalTableImpl(mat, filteredrownames, colnames);
		return table;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.LexicalTable#getFmax()
	 */
	@Override
	public int getFMax() {
		List<Integer> freqs = getFreqs();
		int max = 0;
		for (int i : freqs) {
			if (max < i) {
				max = i;
			}
		}
		return max;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.LexicalTable#getFmin()
	 */
	@Override
	public int getFMin() {
		//		List<Integer> freqs = getFreqs();
		//		int min = 999999;
		//		for (int i : freqs) {
		//			if (min > i) {
		//				min = i;
		//			}
		//		}
		//		return min;

		try {
			return rw.eval("min(rowSums(" + this.symbol + "))").asInteger();
		}
		catch (RWorkspaceException | REXPMismatchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}

	/*
	 * (non-Javadoc)
	 * TODO: = rowmargins ?
	 * @see org.txm.stat.data.LexicalTable#getFreqs()
	 */
	@Override
	public List<Integer> getFreqs() {
		ArrayList<Integer> freqs = new ArrayList<Integer>();
		ArrayList<double[]> cols = new ArrayList<double[]>();
		int Nrows = this.getNRows();
		int Ncols = this.getNColumns();
		for (int i = 0; i < Ncols; i++) {
			try {
				cols.add(this.getCol(i).asDoubleArray());
			}
			catch (RException e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
			}
			catch (RWorkspaceException e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
			}
			catch (StatException e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		int sum = 0;

		for (int i = 0; i < Nrows; i++) {
			sum = 0;
			for (int j = 0; j < Ncols; j++) {
				sum += (int) cols.get(j)[i];
			}
			freqs.add(sum);
		}
		return freqs;
	}

	public void setReference(String refSymbol) {
		try {
			RWorkspace rw = RWorkspace.getRWorkspaceInstance();
			rw.voidEval("missingrownames <- rownames(" + symbol + ")[!rownames(" + symbol + ")%in%rownames(" + refSymbol + ")]"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			rw.voidEval("missingrows <- matrix(ncol=1, nrow=length(missingrownames))"); //$NON-NLS-1$
			rw.voidEval("missingrows[,] <- 0"); //$NON-NLS-1$
			rw.voidEval("rownames(missingrows) <- missingrownames"); //$NON-NLS-1$
			rw.voidEval(refSymbol + " <- t(t(rbind(" + refSymbol + ", t(t(missingrows)))))"); //$NON-NLS-1$ //$NON-NLS-2$
			rw.voidEval("refLines <- t(t(" + refSymbol + "[rownames(" + symbol + "),]))"); // only keep the same lines //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			rw.voidEval("rmargins <-  t(t(margin.table(" + symbol + ", 1)))"); // //$NON-NLS-1$ //$NON-NLS-2$
			rw.voidEval("refmargin <- margin.table(refLines)"); //$NON-NLS-1$
			rw.voidEval(symbol + " <- cbind(" + symbol + ", abs(refLines - rmargins))"); // use abs if refLine does not contains a line from 'symbol' //$NON-NLS-1$ //$NON-NLS-2$
			rw.voidEval("colnames(" + symbol + ")[length(colnames(" + symbol + "))] <- \"##RESTE##\""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	@Override
	public int getRowsCount() {
		return this.getNRows();
	}

	@Override
	public int getColumnsCount() {
		return this.getNColumns();
	}

	@Override
	public ILexicalTable getCopy() {

		try {
			RWorkspace rw = RWorkspace.getRWorkspaceInstance();

			String newsymbol = this.getSymbol() + "_copy";
			while (rw.containsVariable(newsymbol)) {
				newsymbol += "_copy";
			}
			ILexicalTable copy = new LexicalTableImpl(newsymbol);
			rw.eval(newsymbol + " <- " + this.getSymbol());
			return copy;
		}
		catch (RWorkspaceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
