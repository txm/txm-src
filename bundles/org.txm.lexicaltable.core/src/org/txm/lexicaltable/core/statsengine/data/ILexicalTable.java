// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-09-19 10:31:00 +0200 (Mon, 19 Sep 2016) $
// $LastChangedRevision: 3298 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.lexicaltable.core.statsengine.data;

import java.io.File;
import java.util.List;

import org.rosuda.REngine.REXPMismatchException;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.core.data.ContingencyTable;
import org.txm.statsengine.core.data.Vector;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

// TODO: Auto-generated Javadoc
/**
 * A LexicalTable is a special kind of {@link ContingencyTable} extracted from a
 * corpora given a {@link Partition} (the columns) and a {@link Property} (the
 * rows).
 * 
 * Can be edited, rows can be deleted cols can be deleted
 * 
 * can be exported of imported from/to a file
 * 
 * @author sloiseau
 */
public interface ILexicalTable extends ContingencyTable {

	/**
	 * Insert a column representing the reference corpus
	 * 
	 * @param symbol
	 */
	public abstract void setReference(String symbol);

	/**
	 * Gets the copy.
	 *
	 * @return the copy
	 */
	public abstract ILexicalTable getCopy();

	/**
	 * Sort.
	 *
	 * @param colindex the colindex
	 * @param reverse the reverse
	 */
	public abstract void sort(int colindex, Boolean reverse);

	/**
	 * Sort row names.
	 *
	 * @param reverse the reverse
	 */
	public void sortRowNames(Boolean reverse);

	/**
	 * Removes the col.
	 *
	 * @param col the col
	 * @param checkEmptyLines the check empty lines
	 */
	public abstract void removeCol(int col, boolean checkEmptyLines) throws StatException;

	/**
	 * Removes the cols.
	 *
	 * @param cols the cols
	 */
	public abstract void removeCols(List<Integer> cols) throws StatException;

	/**
	 * Removes the cols.
	 *
	 * @param coltodelete the coltodelete
	 */
	public abstract void removeCols(int[] coltodelete) throws StatException;

	/**
	 * Removes the row.
	 *
	 * @param row the row
	 */
	public abstract void removeRow(int row) throws StatException;

	/**
	 * Removes the rows.
	 *
	 * @param row the row
	 */
	public abstract void removeRows(List<Integer> row) throws StatException;

	/**
	 * Removes the rows.
	 *
	 * @param selectionIndices the selection indices
	 */
	public abstract void removeRows(int[] selectionIndices) throws StatException;

	/**
	 * Sets the.
	 *
	 * @param row the row
	 * @param col the col
	 * @param value the value
	 */
	public abstract void set(int row, int col, double value);

	/**
	 * Gets the fmin.
	 *
	 * @return the fmin
	 */
	public abstract int getFMin();

	/**
	 * Gets the fmax.
	 *
	 * @return the fmax
	 */
	public abstract int getFMax();

	/**
	 * Removes the rows.
	 *
	 * @param i the i
	 * @param j the j
	 */
	public abstract void removeRows(int i, int j);

	/**
	 * Cut.
	 * TODO: must be done after calling copy(), check why.
	 *
	 */
	public abstract void cut(int before, int after);

	/**
	 * Filter.
	 *
	 * @param nlines the nlines
	 * @param fmin the fmin
	 */
	public abstract void filter(int nlines, int fmin, int fmax) throws Exception;

	/**
	 * Sets the order.
	 *
	 * @param neworder the neworder
	 * @param reverse the reverse
	 */
	abstract public void setOrder(List<Integer> neworder, Boolean reverse);

	/**
	 * Gets the freqs.
	 *
	 * @return the freqs
	 */
	public abstract List<Integer> getFreqs();

	public abstract boolean toTxt(File outfile, String encoding, String colseparator, String txtseparator);

	/**
	 * Import data.
	 *
	 * @param file the file
	 */
	public abstract boolean importData(File file, String encoding, String colseparator, String txtseparator) throws RWorkspaceException, REXPMismatchException;

	public abstract void sortByFreqs(boolean reverse) throws Exception;

	@Override
	public abstract Vector getRowMarginsVector() throws StatException;

	@Override
	public abstract Vector getColMarginsVector() throws StatException;

	@Override
	public abstract int[] getRowMargins() throws Exception;

	@Override
	public abstract int[] getColMargins() throws Exception;


	/**
	 * Gets the number of rows.
	 * 
	 * @return the number of rows
	 */
	public abstract int getRowsCount();


	/**
	 * Gets the number of columns.
	 * 
	 * @return the number of columns
	 */
	public abstract int getColumnsCount();


}
