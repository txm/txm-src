package org.txm.functions.intertextualdistance;

/**
 * @author mdecorde lvanni
 */

import org.rosuda.REngine.REXP;
import org.txm.lexicaltable.core.statsengine.data.ILexicalTable;
import org.txm.statsengine.r.core.RWorkspace;

public class InterTextDistance {

	ILexicalTable table;

	String pMethod = "euclidian"; //$NON-NLS-1$

	public InterTextDistance(ILexicalTable table) {
		this.table = table;
	}


	public boolean _compute() {
		try {
			// Corpus corpus = CorpusManager.getCorpusManager().getCorpus("VOEUX");
			// String method = "euclidean"; // "euclidean"’, ‘"maximum"’, ‘"manhattan"’, ‘"canberra"’, ‘"binary"’ or ‘"minkowski"’
			//
			// StructuralUnit text_su = corpus.getStructuralUnit("text");
			// StructuralUnitProperty text_prop = text_su.getProperty("loc");
			// Partition textes = corpus.createPartition(text_su, text_prop);
			//
			// Property prop = corpus.getProperty("word") ;
			// LexicalTable table = textes.getLexicalTable(prop, 2);

			System.out.println(table.getSymbol()); // LexicalTable41

			RWorkspace r = RWorkspace.getRWorkspaceInstance();	// get the R connection
			REXP rresult = r.eval("as.matrix(dist(t(" + table.getSymbol() + "), method=\"euclidian\"))"); //$NON-NLS-1$ //$NON-NLS-2$
			double[][] result = rresult.asDoubleMatrix();

			for (int i = 0; i < result.length; i++) {
				for (int j = 0; j < result[i].length; j++) {
					System.out.print(" " + result[i][j]); //$NON-NLS-1$
				}
				System.out.println();
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	public ILexicalTable getTable() {
		return table;
	}

	public String getLabel() {
		return pMethod;
	}
}
