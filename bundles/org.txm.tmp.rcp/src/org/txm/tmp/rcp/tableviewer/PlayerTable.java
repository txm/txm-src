package org.txm.tmp.rcp.tableviewer;

import java.util.ArrayList;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

/**
 * This class demonstrates TableViewers
 */
public class PlayerTable extends ApplicationWindow {

	ArrayList<TableColumn> columns = new ArrayList<TableColumn>();

	// The data model
	private PlayerTableModel model;

	// The table viewer
	private TableViewer rightTV;

	private Spinner noSpinner;

	private TableViewer leftTV;

	private Table rightTable, leftTable;

	private ScrollBar vBarLeft;

	private ScrollBar vBarRight;

	/**
	 * Constructs a PlayerTable
	 */
	public PlayerTable() {
		super(null);
		model = new PlayerTableModel();
	}

	/**
	 * Runs the application
	 */
	public void run() {
		// Don't return from open() until window closes
		setBlockOnOpen(true);

		// Open the main window
		open();

		// Dispose the display
		Display.getCurrent().dispose();
	}

	/**
	 * Configures the shell
	 * 
	 * @param shell
	 *            the shell
	 */
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setSize(400, 400);
	}

	/**
	 * Creates the main window's contents
	 * 
	 * @param parent
	 *            the main window
	 * @return Control
	 */
	protected Control createContents(Composite parent) {
		// Create the composite to hold the controls
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));

		noSpinner = new Spinner(composite, SWT.NONE);
		noSpinner.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		noSpinner.setMaximum(10);
		noSpinner.setMinimum(0);

		Button addColumn = new Button(composite, SWT.PUSH);
		addColumn.setText("add column");
		addColumn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		addColumn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				TableViewerColumn tc = new TableViewerColumn(rightTV, SWT.LEFT, 0);
				columns.add(0, tc.getColumn());
				tc.getColumn().setText("Col "+noSpinner.getSelection());
				tc.getColumn().setWidth(100);
				tc.setLabelProvider(new ColumnLabelProvider() {

					int i = noSpinner.getSelection();

					@Override
					public String getText(Object element) {
						return ""+element+element+element+element;
					}
				});

				tc.getColumn().addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent event) {
						((PlayerViewerSorter) rightTV.getSorter())
						.doSort(PlayerConst.COLUMN_LAST_NAME);
						rightTV.refresh();
					}
				});

				rightTV.refresh();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});

		Button killColumn = new Button(composite, SWT.PUSH);
		killColumn.setText("remove last column");
		killColumn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		killColumn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				TableColumn tc = columns.remove(columns.size()-1);
				tc.dispose();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});

		// Create the combo to hold the team names
		Combo combo = new Combo(composite, SWT.READ_ONLY);
		combo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));


		// Create the table viewer to display the players
		leftTable = new Table (composite, SWT.BORDER);
		leftTV = new TableViewer(leftTable);
		leftTable.setLayoutData(new GridData(GridData.FILL_BOTH));
		leftTV.setContentProvider(new PlayerContentProvider());
		leftTV.setSorter(new PlayerViewerSorter());

		// Add the team names to the combo
		for (int i = 0, n = model.teams.length; i < n; i++) {
			combo.add(model.teams[i].getName());
		}

		// Add a listener to change the tableviewer's input
		combo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				update(model.teams[((Combo) event.widget).getSelectionIndex()]);
			}
		});

		// Turn on the header and the lines
		leftTable.setHeaderVisible(true);
		leftTable.setLinesVisible(true);

		TableViewerColumn tc = new TableViewerColumn(leftTV, SWT.LEFT, 0);
		columns.add(0, tc.getColumn());
		tc.getColumn().setText("Col "+noSpinner.getSelection());
		tc.getColumn().setWidth(100);
		tc.setLabelProvider(new ColumnLabelProvider() {

			int i = noSpinner.getSelection();

			@Override
			public String getText(Object element) {
				String l = ""+element;
				if (l.length() < i) return "too long";

				else return l.substring(i);

			}
		});

		tc.getColumn().addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				((PlayerViewerSorter) leftTV.getSorter())
				.doSort(PlayerConst.COLUMN_LAST_NAME);
				leftTV.refresh();
			}
		});

		// Create the table viewer to display the players
		rightTable = new Table (composite, SWT.BORDER);
		rightTV = new TableViewer(rightTable);
		rightTable.setLayoutData(new GridData(GridData.FILL_BOTH));
		rightTV.setContentProvider(new PlayerContentProvider());
		rightTV.setSorter(new PlayerViewerSorter());


		// Add the team names to the combo
		for (int i = 0, n = model.teams.length; i < n; i++) {
			combo.add(model.teams[i].getName());
		}

		// Add a listener to change the tableviewer's input
		combo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				update(model.teams[((Combo) event.widget).getSelectionIndex()]);
			}
		});

		// Turn on the header and the lines
		rightTable.setHeaderVisible(true);
		rightTable.setLinesVisible(true);

		leftTable.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				rightTable.setSelection(leftTable.getSelectionIndex());
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});
		vBarLeft = leftTable.getVerticalBar();
		vBarLeft.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				vBarRight.setSelection(vBarLeft.getSelection());
			}
		});
		rightTable.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				leftTable.setSelection(rightTable.getSelectionIndex());
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});
		vBarRight = rightTable.getVerticalBar();
		vBarRight.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				vBarLeft.setSelection(vBarRight.getSelection());
			}
		});

		// Select the first item
		combo.select(0);
		update(model.teams[0]);

		return composite;
	}

	/**
	 * Updates the application with the selected team
	 * 
	 * @param team
	 *            the team
	 */
	private void update(Team team) {
		// Update the window's title bar with the new team
		getShell().setText(team.getYear() + " " + team.getName());

		// Set the table viewer's input to the team
		rightTV.setInput(team);
		leftTV.setInput(team);
	}

	/**
	 * The application entry point
	 * 
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		new PlayerTable().run();
	}
}
