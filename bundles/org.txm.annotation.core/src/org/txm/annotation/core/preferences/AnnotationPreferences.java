package org.txm.annotation.core.preferences;

import org.txm.core.preferences.TXMPreferences;

/**
 * Preferences initializer and manager.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class AnnotationPreferences extends TXMPreferences {

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(AnnotationPreferences.class)) {
			new AnnotationPreferences();
		}
		return TXMPreferences.instances.get(AnnotationPreferences.class);
	}

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		//Preferences preferences = this.getDefaultPreferencesNode();
	}
}
