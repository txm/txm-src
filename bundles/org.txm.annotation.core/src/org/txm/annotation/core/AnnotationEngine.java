package org.txm.annotation.core;

import org.txm.core.engines.Engine;
import org.txm.core.results.TXMResult;
import org.txm.objects.CorpusBuild;

public abstract class AnnotationEngine implements Engine {

	public static final String EXTENSION_POINT_ID = AnnotationEngine.class.getCanonicalName();

	@Override
	public abstract String getName();

	/**
	 * 
	 * @return true if the Annotation Engine has some annotations not saved (that will be lost if not saved)
	 */
	public abstract boolean hasAnnotationsToSave(CorpusBuild corpus);

	public String hasAdditionalDetailsForResult(TXMResult result) {
		return null;
	}

	public String getAdditionalDetailsForResult(TXMResult result) {
		return null;
	}
}
