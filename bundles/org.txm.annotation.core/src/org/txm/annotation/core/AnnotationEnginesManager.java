package org.txm.annotation.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.osgi.util.NLS;
import org.txm.Toolbox;
import org.txm.core.engines.EngineType;
import org.txm.core.engines.EnginesManager;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Project;
import org.txm.utils.logger.Log;


/**
 * 
 * @author mdecorde, sjacquot
 * 
 *         FIXME: SJ: useless class, the type and priority/order should be defined in org.txm.core.engines.EnginesManager extension point
 **/
public class AnnotationEnginesManager extends EnginesManager<AnnotationEngine> {

	private static final long serialVersionUID = -2692728355544468496L;

	public AnnotationEnginesManager() {

	}

	@Override
	public boolean startEngines(IProgressMonitor monitor) {

		//		for (AnnotationEngine e : values()) {
		//			
		// lazy mode

		// AnnotationEngine se = (AnnotationEngine)e;
		// if (monitor != null) monitor.subTask("Starting "+ se.getName()+" annotation engine.");
		// try {
		// se.start(monitor);
		// } catch (Exception ex) {
		// System.out.println("Error: failed to start annotation engine: "+se.getName()+": "+ex.getLocalizedMessage());
		// }
		//		}
		return true;
	}

	/**
	 * Check all AnnotationEngine s and call ae.hasAnnotationsToSave(corpus)
	 * 
	 * @param corpus
	 * @return true if any AnnotationEngine have annotations to save
	 */
	public boolean hasAnnotationsToSave(CorpusBuild corpus) {
		boolean todo = false;
		for (AnnotationEngine engine : this.getEngines().values()) {
			AnnotationEngine ae = engine;
			todo = todo || ae.hasAnnotationsToSave(corpus);
		}
		return todo;
	}

	@Override
	public boolean stopEngines() {
		for (AnnotationEngine e : values()) {
			// System.out.println(e);
			AnnotationEngine se = e;
			Log.fine("Stoping " + se.getName() + " annotation engine."); //$NON-NLS-1$ //$NON-NLS-2$
			try {
				se.stop();
			}
			catch (Exception ex) {
				Log.severe(NLS.bind(Messages.ErrorFailedToStopAnnotationEngineP0P1, se.getName(), ex.getLocalizedMessage()));
			}
		}
		return true;
	}

	@Override
	public EngineType getEnginesType() {
		return EngineType.ANNOTATION;
	}

	@Override
	public boolean fetchEngines() {

		IConfigurationElement[] contributions = Platform.getExtensionRegistry().getConfigurationElementsFor(AnnotationEngine.EXTENSION_POINT_ID);
		// System.out.println("search engine contributions: "+SearchEngine.EXTENSION_POINT_ID);
		for (int i = 0; i < contributions.length; i++) {
			try {
				AnnotationEngine e = (AnnotationEngine) contributions[i].createExecutableExtension("class"); //$NON-NLS-1$
				if (e.initialize()) {
					put(e.getName(), e);
				}
				else {
					Log.warning(NLS.bind(Messages.FailToInitializeTheP0AnnotationEngine, e.getName()));
				}
			}
			catch (Exception e) {
				Log.warning(NLS.bind(Messages.FailToInstanciateP0P1, contributions[i].getName(), e.getLocalizedMessage()));
				Log.printStackTrace(e);
			}
		}

		return size() > 0;
	}

	public boolean isSaveNeeded() {

		for (AnnotationEngine a : values()) {
			for (Project project : Toolbox.workspace.getProjects()) {
				for (CorpusBuild corpus : project.getCorpora()) {
					if (a.hasAnnotationsToSave(corpus)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public List<AnnotationEngine> getEnginesWithSaveNeeded() {

		List<AnnotationEngine> tmp = new ArrayList<AnnotationEngine>();
		for (AnnotationEngine a : values()) {
			for (Project project : Toolbox.workspace.getProjects()) {
				for (CorpusBuild corpus : project.getCorpora()) {
					if (a.hasAnnotationsToSave(corpus)) {
						tmp.add(a);
					}
				}
			}
		}
		return tmp;
	}

	public HashMap<CorpusBuild, ArrayList<AnnotationEngine>> getEnginesWithSaveNeededPerCorpus() {

		HashMap<CorpusBuild, ArrayList<AnnotationEngine>> tmp = new HashMap<CorpusBuild, ArrayList<AnnotationEngine>>();
		for (AnnotationEngine a : values()) {
			for (Project project : Toolbox.workspace.getProjects()) {
				for (CorpusBuild corpus : project.getCorpora()) {
					if (a.hasAnnotationsToSave(corpus)) {
						if (!tmp.containsKey(corpus)) tmp.put(corpus, new ArrayList<>());
						tmp.get(corpus).add(a);
					}
				}
			}
		}
		return tmp;
	}
}
