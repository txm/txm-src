package org.txm.annotation.core;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

public class Messages extends NLS {

	public static String ErrorFailedToStopAnnotationEngineP0P1;

	public static String FailToInitializeTheP0AnnotationEngine;

	public static String FailToInstanciateP0P1;

	public static String ErrorNoSuitableFileToProcessInP0;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(Messages.class);
	}

	private Messages() {
	}
}
