package org.txm.concordance.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.concordance.core.functions.Concordance;
import org.txm.core.preferences.TXMPreferences;

/**
 * Default preferences initializer.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ConcordancePreferences extends TXMPreferences {

	public static final String LEFT_CONTEXT_SIZE = "left_context_size"; //$NON-NLS-1$

	public static final String RIGHT_CONTEXT_SIZE = "right_context_size"; //$NON-NLS-1$

	public static final String LEFT_ANALYSIS_PROPERTIES = "left_analysis_properties"; //$NON-NLS-1$

	public static final String KEYWORD_ANALYSIS_PROPERTIES = "keyword_analysis_properties"; //$NON-NLS-1$

	public static final String RIGHT_ANALYSIS_PROPERTIES = "right_analysis_properties"; //$NON-NLS-1$

	public static final String LEFT_VIEW_PROPERTIES = "left_view_properties"; //$NON-NLS-1$

	public static final String KEYWORD_VIEW_PROPERTIES = "keyword_view_properties"; //$NON-NLS-1$

	public static final String RIGHT_VIEW_PROPERTIES = "right_view_properties"; //$NON-NLS-1$

	/** The default value is "*" -> The concordance will use ref or text_id */
	public static final String VIEW_REFERENCE_PATTERN = "view_reference_pattern"; //$NON-NLS-1$

	public static final String SORT_REFERENCE_PATTERN = "sort_reference_pattern"; //$NON-NLS-1$

	public static final String LIMIT_CQL = "limit_cql"; //$NON-NLS-1$

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(ConcordancePreferences.class)) {
			new ConcordancePreferences();
		}
		return TXMPreferences.instances.get(ConcordancePreferences.class);
	}

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.putInt(NB_OF_ELEMENTS_PER_PAGE, 100);
		preferences.putInt(INDEX, 0);
		preferences.putInt(LEFT_CONTEXT_SIZE, 8);
		preferences.putInt(RIGHT_CONTEXT_SIZE, 12);

		preferences.put(LEFT_ANALYSIS_PROPERTIES, TXMPreferences.DEFAULT_UNIT_PROPERTY);
		preferences.put(KEYWORD_ANALYSIS_PROPERTIES, TXMPreferences.DEFAULT_UNIT_PROPERTY);
		preferences.put(RIGHT_ANALYSIS_PROPERTIES, TXMPreferences.DEFAULT_UNIT_PROPERTY);
		preferences.put(LEFT_VIEW_PROPERTIES, TXMPreferences.DEFAULT_UNIT_PROPERTY);
		preferences.put(KEYWORD_VIEW_PROPERTIES, TXMPreferences.DEFAULT_UNIT_PROPERTY);
		preferences.put(RIGHT_VIEW_PROPERTIES, TXMPreferences.DEFAULT_UNIT_PROPERTY);

		preferences.put(VIEW_REFERENCE_PATTERN, "*"); //$NON-NLS-1$
		preferences.put(SORT_REFERENCE_PATTERN, "*"); //$NON-NLS-1$

		preferences.put(LIMIT_CQL, "<text>[]"); //$NON-NLS-1$

		preferences.put(TARGET_STRATEGY, Concordance.TARGET_SELECT); //$NON-NLS-1$
	}
}
