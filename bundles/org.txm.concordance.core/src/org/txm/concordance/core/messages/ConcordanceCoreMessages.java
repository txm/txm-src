package org.txm.concordance.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Concordance core messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ConcordanceCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.concordance.core.messages.messages"; //$NON-NLS-1$



	public static String RESULT_TYPE;

	public static String wordCorpusOrder;

	public static String failedToRetrieveTextidPropertyFromLines;

	public static String failedToExportConcordanceColonP0;

	public static String reference;

	public static String leftContext;

	public static String keyword;

	public static String rightContext;

	public static String context;

	public static String undefined;

	public static String resultsRetrievedIn;

	public static String retrievingP0Results;

	public static String failedToGetTextsLimitsColonP0;

	public static String compositeColon;

	public static String references;

	public static String leftContext_2;

	public static String rightContext_2;

	public static String concordanceOfP0InTheP1Corpus;



	public static String CQPErrorP0;



	public static String NoQuery;


	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, ConcordanceCoreMessages.class);
	}
}
