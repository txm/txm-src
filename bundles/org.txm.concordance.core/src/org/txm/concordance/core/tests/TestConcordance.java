package org.txm.concordance.core.tests;


import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.core.functions.comparators.LexicographicKeywordComparator;
import org.txm.concordance.core.functions.comparators.LexicographicLeftContextComparator;
import org.txm.concordance.core.functions.comparators.LexicographicRightContextComparator;
import org.txm.concordance.core.functions.comparators.LineComparator;
import org.txm.concordance.core.functions.comparators.PropertiesReferenceComparator;
import org.txm.concordance.core.functions.comparators.WordPositionComparator;
import org.txm.searchengine.cqp.ReferencePattern;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.LogMonitor;
import org.txm.utils.TXMProgressMonitor;

/**
 * Test class for the concordances
 * 
 * @author mdecorde
 *
 */
public class TestConcordance {

	/**
	 * the corpus name to use
	 */
	static String CORPUS = "DISCOURS"; //$NON-NLS-1$

	public static boolean allTests(File outdir) {
		TestConcordance tester = new TestConcordance();
		return tester.test1(outdir);
	}

	/**
	 * First test
	 * 
	 * @param outdir : the out folder
	 */
	public boolean test1(File outdir) {
		return test(new File(outdir, TestConcordance.class.getName() + "_test1.csv"), CORPUS, null, "je", //$NON-NLS-1$ //$NON-NLS-2$
				Arrays.asList("word"), Arrays.asList("word"), Arrays.asList("word"),  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				Arrays.asList("word"), Arrays.asList("word"), Arrays.asList("word"),  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				Arrays.asList("text_id", "text_loc"), Arrays.asList("text_id"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				10, 40, null);
	}

	private boolean test(File outfile, String corpusname, String subcorpusname, String queryString,
			List<String> leftCSortPropertiesString, List<String> rightCSortPropertiesString, List<String> keywordSortPropertiesString,
			List<String> leftCViewPropertiesString, List<String> rightCViewPropertiesString, List<String> keywordViewPropertiesString,
			List<String> refPatternsString, List<String> refAnalysePatternString, int leftContextSize, int rightContextSize, String sortBy) {
		try {
			CQPCorpus corpus = CorpusManager.getCorpusManager().getCorpus(corpusname);
			if (subcorpusname != null) {
				corpus = corpus.getSubcorpusByName(subcorpusname);
			}
			CQLQuery query = new CQLQuery(CQLQuery.fixQuery(queryString, "fr")); //$NON-NLS-1$

			List<WordProperty> leftCSortProperties = new ArrayList<>();
			for (String p : leftCSortPropertiesString)
				leftCSortProperties.add(corpus.getProperty(p));

			List<WordProperty> rightCSortProperties = new ArrayList<>();
			for (String p : rightCSortPropertiesString)
				rightCSortProperties.add(corpus.getProperty(p));

			List<WordProperty> keywordSortProperties = new ArrayList<>();
			for (String p : keywordSortPropertiesString)
				keywordSortProperties.add(corpus.getProperty(p));

			List<WordProperty> leftCViewProperties = new ArrayList<>();
			for (String p : leftCViewPropertiesString)
				leftCViewProperties.add(corpus.getProperty(p));

			List<WordProperty> rightCViewProperties = new ArrayList<>();
			for (String p : rightCViewPropertiesString)
				rightCViewProperties.add(corpus.getProperty(p));

			List<WordProperty> keywordViewProperties = new ArrayList<>();
			for (String p : keywordViewPropertiesString)
				keywordViewProperties.add(corpus.getProperty(p));

			ArrayList<Property> refPatterns = new ArrayList<>();
			for (String p : refPatternsString) {
				String[] split = p.split("_", 2); //$NON-NLS-1$
				if (split != null && split[0].length() > 0 && split[1].length() > 0) {
					refPatterns.add(corpus.getStructuralUnit(split[0]).getProperty(split[1]));
				}
				else {
					refPatterns.add(corpus.getProperty(p));
				}
			}
			ReferencePattern referencePattern = new ReferencePattern(refPatterns);

			refPatterns = new ArrayList<>();
			for (String p : refAnalysePatternString) {
				String[] split = p.split("_", 2); //$NON-NLS-1$
				if (split != null && split[0].length() > 0 && split[1].length() > 0) {
					refPatterns.add(corpus.getStructuralUnit(split[0]).getProperty(split[1]));
				}
				else {
					refPatterns.add(corpus.getProperty(p));
				}
			}
			ReferencePattern refAnalysePattern = new ReferencePattern(refPatterns);

			Concordance conc = new Concordance(corpus);
			conc.setParameters(query,
					leftCSortProperties, rightCSortProperties, keywordSortProperties,
					leftCViewProperties, rightCViewProperties, keywordViewProperties,
					referencePattern, refAnalysePattern,
					leftContextSize, rightContextSize);
			conc.compute();
			conc.getLines(0, conc.getNLines());

			if (sortBy != null) {
				LineComparator comparator;
				if ("null".equals(sortBy)) comparator = new WordPositionComparator(); //$NON-NLS-1$
				else if ("lexicographic".equals(sortBy)) comparator = new LexicographicKeywordComparator(); //$NON-NLS-1$
				else if ("leftcontext".equals(sortBy)) comparator = new LexicographicLeftContextComparator(); //$NON-NLS-1$
				else if ("rightcontext".equals(sortBy)) comparator = new LexicographicRightContextComparator(); //$NON-NLS-1$
				else if ("reference".equals(sortBy)) comparator = new PropertiesReferenceComparator(); //$NON-NLS-1$
				else comparator = new WordPositionComparator();
				conc.sort(comparator, new TXMProgressMonitor(new LogMonitor()));
			}
			conc.toTxt(outfile, "UTF-8", "\t", ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
		return true;
	}
}
