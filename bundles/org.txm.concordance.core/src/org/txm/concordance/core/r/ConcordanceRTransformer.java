package org.txm.concordance.core.r;

import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.core.functions.Line;
import org.txm.statsengine.r.core.RTransformer;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

public class ConcordanceRTransformer extends RTransformer<Concordance> {

	/** The noconc. */
	protected static int noconc = 1;

	/** The prefix r. */
	protected static String prefixR = "Concordances"; //$NON-NLS-1$

	@Override
	public Concordance fromRtoTXM(String symbol) throws RWorkspaceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String _fromTXMtoR(Concordance o, String symbol) throws RWorkspaceException {

		Concordance conc = (Concordance) o;

		if (symbol == null || symbol.length() == 0) {
			symbol = prefixR + noconc++;
		}
		String[] refs = new String[conc.getNLines()];
		String[] lefts = new String[conc.getNLines()];
		String[] keywords = new String[conc.getNLines()];
		String[] rights = new String[conc.getNLines()];

		int i = 0;
		try {
			for (Line line : conc.getLines(0, conc.getNLines())) {
				refs[i] = line.getViewRef().toString();
				lefts[i] = line.leftContextToString();
				keywords[i] = line.keywordToString();
				rights[i] = line.rightContextToString();
				i++;
			}
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return null; //$NON-NLS-1$
		}

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		rw.addVectorToWorkspace("concrefs", refs); //$NON-NLS-1$
		rw.addVectorToWorkspace("conclefts", lefts); //$NON-NLS-1$
		rw.addVectorToWorkspace("conckeywords", keywords); //$NON-NLS-1$
		rw.addVectorToWorkspace("concrights", rights); //$NON-NLS-1$

		rw.eval(symbol + "<- matrix(data = c(concrefs, conclefts, conckeywords, concrights), nrow = " + conc.getNLines() + ", ncol = 4)"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval("colnames(" + symbol + " ) <- c('refs', 'leftcontext', 'keyword', 'rightcontext')"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval(symbol + "<- list(data=" + symbol //$NON-NLS-1$
				+ ", leftcontext=" + conc.getLeftContextSize() //$NON-NLS-1$
				+ ", rightcontext=" + conc.getRightContextSize() //$NON-NLS-1$
				//+ ", query=\""+conc.query.getQueryString()+"\""
				+ ")"); //$NON-NLS-1$

		rw.eval("rm(concrefs, conclefts, conckeywords, concrights)"); //$NON-NLS-1$
		return symbol;
	}

}
