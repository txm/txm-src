// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.concordance.core.functions.comparators;

import org.txm.concordance.core.functions.Line;
import org.txm.concordance.core.messages.ConcordanceCoreMessages;
import org.txm.searchengine.cqp.Reference;
import org.txm.searchengine.cqp.corpus.Property;

// TODO: Auto-generated Javadoc
/**
 * Compare the references of concordance line @ author mdecorde.
 */
public class PropertiesReferenceComparator extends LocalizedLineComparator {

	/** The Constant NAME. */
	private static final String NAME = ConcordanceCoreMessages.references;

	//protected Collator collator = Collator.getInstance(new Locale(System.getProperty("user.language"))); //$NON-NLS-1$

	/**
	 * Instantiates a new properties reference comparator.
	 */
	public PropertiesReferenceComparator() {
		super(NAME);
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Line l1, Line l2) {
		if (l1 == null || l2 == null)
			return 0;

		Reference ref1 = l1.getAnaRef();
		Reference ref2 = l2.getAnaRef();
		assert (ref1.getReferencePattern().equals(ref2.getReferencePattern()));
		for (Property prop : ref1.getProperties()) {
			int c = 0;
			try {
				Integer i1 = Integer.parseInt(ref1.getValue(prop));
				Integer i2 = Integer.parseInt(ref2.getValue(prop));
				c = i1.compareTo(i2);
			}
			catch (NumberFormatException e) {
				//System.out.println("sort "+ref1+ " "+ ref2 +" with "+collator);
				c = collator.compare(ref1.getValue(prop), ref2.getValue(prop));
			}

			if (c != 0) {
				return c;
			}
		}
		return 0;
	}
}
