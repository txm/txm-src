// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-02-01 09:53:38 +0100 (Mon, 01 Feb 2016) $
// $LastChangedRevision: 3101 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.concordance.core.functions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.txm.objects.Match;
import org.txm.searchengine.cqp.Reference;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.utils.i18n.LangFormater;

// TODO: Auto-generated Javadoc
/**
 * The Class Line represents concordance line.
 *
 * @author jmague
 */
public class Line {

	/** The concordance. */
	private Concordance concordance;

	/** The left ctx view properties. */
	private Map<Property, List<String>> leftCtxViewPropertiesValues;

	/** The keywords view properties. */
	private Map<Property, List<String>> keywordsViewValues;

	/** The right ctx view properties. */
	private Map<Property, List<String>> rightCtxViewValues;

	/** The left ctx analysis property. */
	private HashMap<Property, List<String>> leftCtxAnalysisValues;

	/** The keywords analysis property. */
	private HashMap<Property, List<String>> keywordsAnalysisValues;

	/** The right ctx analysis property. */
	private HashMap<Property, List<String>> rightCtxAnalysisValues;

	/** The proerty values separator. */
	public static String PROP_SEPARTOR = "_"; //$NON-NLS-1$

	/** The references : view and sort */
	private Reference refView;

	private Reference refAna;

	/** The match. */
	private Match match;

	/** the line's text. */
	private String textId;

	/** the ids of the keywords. */
	private List<String> keywordsIdValues;

	/** The keywordpos. */
	private int keywordpos;

	/**
	 * the @target corpus position of the query
	 */
	private int targetpos;

	/**
	 * Instantiates a new line.
	 *
	 * @param concordance the concordance
	 * @param keywordsIdValues the keywords id values
	 * @param textId the text id
	 * @param leftCtxViewPropertiesValues the left context view property values
	 * @param keywordsViewValues the keywords view property values
	 * @param rightCtxViewValues the right context view property values
	 * @param leftCtxAnalysisValues the left context analysis property values
	 * @param keywordsAnalysisValues the keywords analysis property values
	 * @param rightCtxAnalysisValues the right context analysis property values
	 * @param refView the view references
	 * @param refAna the sort references
	 */
	public Line(Concordance concordance, List<String> keywordsIdValues,
			String textId, Map<Property, List<String>> leftCtxViewPropertiesValues,
			Map<Property, List<String>> keywordsViewValues,
			Map<Property, List<String>> rightCtxViewValues,
			HashMap<Property, List<String>> leftCtxAnalysisValues,
			HashMap<Property, List<String>> keywordsAnalysisValues,
			HashMap<Property, List<String>> rightCtxAnalysisValues,
			Reference refView, Reference refAna,
			Match match, int targetpos) {
		this.textId = textId;
		this.targetpos = targetpos;
		this.keywordsIdValues = keywordsIdValues;
		this.concordance = concordance;
		this.leftCtxViewPropertiesValues = leftCtxViewPropertiesValues;
		this.keywordsViewValues = keywordsViewValues;
		this.rightCtxViewValues = rightCtxViewValues;
		this.leftCtxAnalysisValues = leftCtxAnalysisValues;
		this.keywordsAnalysisValues = keywordsAnalysisValues;
		this.rightCtxAnalysisValues = rightCtxAnalysisValues;
		this.refView = refView;
		this.refAna = refAna;
		this.match = match;
		this.keywordpos = match.getStart();
	}

	/**
	 * Gets the keywords analysis property.
	 * 
	 * @return the keywords analysis property
	 */
	public HashMap<Property, List<String>> getKeywordsAnalysisProperty() {
		return keywordsAnalysisValues;
	}

	/**
	 * Gets the left context analysis property.
	 * 
	 * @return the left context analysis property
	 */
	public HashMap<Property, List<String>> getLeftAnalysisProperty() {
		return leftCtxAnalysisValues;
	}

	public boolean equals(Object l) {

		if (l instanceof Line) {
			return this.match.equals(((Line) l).getMatch());
		}
		return super.equals(l);
	}

	@Override
	public int hashCode() {
		return this.getMatch().hashCode();
	}

	/**
	 * Gets the right ctx analysis property.
	 * 
	 * @return the right ctx analysis property
	 */
	public HashMap<Property, List<String>> getRightAnalysisProperty() {
		return rightCtxAnalysisValues;
	}

	/**
	 * Gets the keywords view properties.
	 * 
	 * @return the keywords view properties
	 */
	public Map<Property, List<String>> getKeywordsViewProperties() {
		return keywordsViewValues;
	}

	/**
	 * Gets the left context view properties.
	 * 
	 * @return the left context view properties
	 */
	public Map<Property, List<String>> getLeftCtxViewProperties() {
		return leftCtxViewPropertiesValues;
	}

	/**
	 * Gets the right context view properties.
	 * 
	 * @return the right context view properties
	 */
	public Map<Property, List<String>> getRightCtxViewProperties() {
		return rightCtxViewValues;
	}

	/**
	 * Gets the reference.
	 * 
	 * @return the reference
	 */
	public Reference getViewRef() {
		return refView;
	}

	/**
	 * Gets the reference.
	 * 
	 * @return the reference
	 */
	public Reference getAnaRef() {
		return refAna;
	}


	/**
	 * Gets the Line left context size.
	 * 
	 * @return the left context size
	 */
	public int getLeftContextSize() {
		if (leftCtxViewPropertiesValues.values().size() == 0) return 0;
		return leftCtxViewPropertiesValues.values().iterator().next().size();
	}

	/**
	 * Gets the Line keywords size.
	 * 
	 * @return the keywords size
	 */
	public int getKeywordsSize() {
		if (keywordsViewValues.values().size() == 0) return 0;
		return keywordsViewValues.values().iterator().next().size();
	}

	/**
	 * Gets the Line right context size.
	 * 
	 * @return the right context size
	 */
	public int getRightContextSize() {
		if (rightCtxViewValues.values().size() == 0) return 0;
		return rightCtxViewValues.values().iterator().next().size();
	}

	/**
	 * Gets the match.
	 * 
	 * @return the match
	 */
	public Match getMatch() {
		return match;
	}

	/**
	 * Returns a string representing the left context.
	 *
	 * @param propertySeparator The string used to separate the dif ferent view properties for
	 *            each word
	 * @param wordSeparator The string used to separate the dif ferent words
	 * @param limit the limit
	 * @return The string representation
	 */
	public String leftContextToString(String propertySeparator,
			String wordSeparator, int limit) {
		return leftContextToString(propertySeparator, wordSeparator, limit, null);
	}


	@Override
	public String toString() {
		return match.toString() + "=" + keywordsViewValues; //$NON-NLS-1$
	}

	/**
	 * Returns a string representing the left context.
	 *
	 * @param propertySeparator The string used to separate the dif ferent view properties for
	 *            each word
	 * @param wordSeparator The string used to separate the dif ferent words
	 * @param limit the limit
	 * @return The string representation
	 */
	public String leftContextToString(String propertySeparator,
			String wordSeparator, int limit, LineDecorator decorator) {

		if (getLeftContextSize() == 0) return ""; //$NON-NLS-1$

		int currentPos = keywordpos - getLeftContextSize();

		List<String> words = new ArrayList<>();
		Map<Property, List<String>> values = getLeftCtxViewProperties();
		int max = (limit == -1) ? (values.get(concordance
				.getLeftViewProperties().get(0)).size()) : (limit);
		int start = this.getLeftContextSize() - max;
		for (int i = start; i < this.getLeftContextSize(); i++) {
			List<String> word = new ArrayList<>();
			for (Property property : values.keySet()) {
				List<String> ivalues = values.get(property);
				if (ivalues == null) {
					System.out.println("ERROR: null ivalues in " + values + " for property " + property); //$NON-NLS-1$ //$NON-NLS-2$
				}
				else {
					word.add(ivalues.get(i));
				}
			}
			words.add(StringUtils.join(word, propertySeparator));

			if (decorator != null) {
				words.set(i, decorator.decorate(this, currentPos, words.get(i)));
			}

			currentPos++; // next position
		}

		return LangFormater.format(StringUtils.join(words, wordSeparator),
				concordance.getCorpus().getLang());
	}

	/**
	 * Returns a string representing the keywords.
	 *
	 * @param propertySeparator The string used to separate the dif ferent view properties for
	 *            each word
	 * @param wordSeparator The string used to separate the dif ferent words
	 * @return The string representation
	 */
	public String keywordToString(String propertySeparator, String wordSeparator) {
		return keywordToString(propertySeparator, wordSeparator, null);
	}

	/**
	 * Returns a string representing the keywords.
	 *
	 * @param propertySeparator The string used to separate the dif ferent view properties for
	 *            each word
	 * @param wordSeparator The string used to separate the dif ferent words
	 * @return The string representation
	 */
	public String keywordToString(String propertySeparator, String wordSeparator, LineDecorator decorator) {

		int currentPos = keywordpos;

		List<String> words = new ArrayList<>();
		Map<Property, List<String>> values = getKeywordsViewProperties();
		int s = values.get(concordance.getKeywordViewProperties().get(0)).size();
		//int keywordEndPos = keywordpos + s - 1;
		// System.out.println("Build STR of currentpos="+currentPos+" length="+s);
		for (int i = 0; i < s; i++) {
			List<String> word = new ArrayList<>();
			for (Property property : values.keySet())
				word.add(values.get(property).get(i));

			if (currentPos == targetpos) { // highlight the @ target
				words.add("[" + StringUtils.join(word, propertySeparator) + "]"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			else {
				words.add(StringUtils.join(word, propertySeparator));
			}

			if (decorator != null) {
				words.set(i, decorator.decorate(this, currentPos, words.get(i)));
			}

			currentPos++; // next position
		}
		return LangFormater.format(StringUtils.join(words, wordSeparator),
				concordance.getCorpus().getLang());
	}

	/**
	 * Returns a string representing the right context.
	 *
	 * @param propertySeparator The string used to separate the dif ferent view properties for
	 *            each word
	 * @param wordSeparator The string used to separate the dif ferent words
	 * @param limit the limit
	 * @return The string representation
	 */
	public String rightContextToString(String propertySeparator,
			String wordSeparator, int limit) {
		return rightContextToString(propertySeparator, wordSeparator, limit, null);
	}

	/**
	 * Returns a string representing the right context.
	 *
	 * @param propertySeparator The string used to separate the dif ferent view properties for
	 *            each word
	 * @param wordSeparator The string used to separate the dif ferent words
	 * @param limit the limit
	 * @return The string representation
	 */
	public String rightContextToString(String propertySeparator,
			String wordSeparator, int limit, LineDecorator decorator) {
		int s = getKeywordsSize();
		if (s == 0) return ""; //$NON-NLS-1$

		List<String> words = new ArrayList<>();
		Map<Property, List<String>> values = getRightCtxViewProperties();
		int max = (limit == -1) ? (values.get(concordance.getRightViewProperties().get(0)).size()) : (limit);

		int currentPos = keywordpos + s;
		// System.out.println("Line "+keywordToString()+" right context, currentpos="+currentPos+" keywordpos="+keywordpos+" keywordlength="+s);
		for (int i = 0; i < max; i++) {
			List<String> word = new ArrayList<>();
			for (Property property : values.keySet())
				word.add(values.get(property).get(i));
			words.add(StringUtils.join(word, propertySeparator));

			if (decorator != null) {
				words.set(i, decorator.decorate(this, currentPos, words.get(i)));
			}
			currentPos++;
		}
		return LangFormater.format(StringUtils.join(words, wordSeparator),
				concordance.getCorpus().getLang());
	}

	/** The properties to process. */
	protected static HashSet<String> propertiesToProcess;

	/**
	 * Right context render.
	 *
	 * @param limit the limit
	 * @return the string
	 */
	public String rightContextRender(int limit) {
		// if(propertiesToProcess == null)
		// propertiesToProcess = TxmRenderer.getPropertyToRender(concordance, "jsesh"); //$NON-NLS-1$

		List<String> words = new ArrayList<>();
		Map<Property, List<String>> values = getRightCtxViewProperties();
		int max = (limit == -1) ? (values.get(concordance.getRightViewProperties().get(0)).size()) : (limit);

		for (int i = 0; i < max; i++) {
			List<String> word = new ArrayList<>();
			for (Property property : values.keySet()) {
				// if(propertiesToProcess.contains(property.getName()))
				// word.add(TxmRenderer.jseshrenderer.render(values.get(property).get(i)));
				// else
				word.add(values.get(property).get(i));
			}
			words.add(StringUtils.join(word, "_")); //$NON-NLS-1$
		}
		return LangFormater.format(StringUtils.join(words, " "), //$NON-NLS-1$
				concordance.getCorpus().getLang());
	}

	/**
	 * Keyword render.
	 *
	 * @param wordSeparator the word separator
	 * @return the string
	 */
	public String keywordRender(String wordSeparator) {
		// if(propertiesToProcess == null)
		// propertiesToProcess = TxmRenderer.getPropertyToRender(concordance, "jsesh"); //$NON-NLS-1$

		List<String> words = new ArrayList<>();
		Map<Property, List<String>> values = getKeywordsViewProperties();
		for (int i = 0; i < values.get(
				concordance.getKeywordViewProperties().get(0)).size(); i++) {
			List<String> word = new ArrayList<>();
			for (Property property : values.keySet()) {
				// if(propertiesToProcess.contains(property.getName()))
				// word.add(TxmRenderer.jseshrenderer.render(values.get(property).get(i)));
				// else
				word.add(values.get(property).get(i));
			}
			words.add(StringUtils.join(word, wordSeparator));
		}
		return LangFormater.format(StringUtils.join(words, wordSeparator),
				concordance.getCorpus().getLang());
	}

	/**
	 * Left context render.
	 *
	 * @param limit the limit
	 * @return the string
	 */
	public String leftContextRender(int limit) {
		List<String> words = new ArrayList<>();
		Map<Property, List<String>> values = getLeftCtxViewProperties();
		int max = (limit == -1) ? (values.get(concordance
				.getLeftViewProperties().get(0)).size()) : (limit);
		int start = this.getLeftContextSize() - max;
		for (int i = start; i < this.getLeftContextSize(); i++) {
			List<String> word = new ArrayList<>();
			for (Property property : values.keySet()) {
				// if(propertiesToProcess.contains(property.getName()))
				// word.add(TxmRenderer.jseshrenderer.render(values.get(property).get(i)));
				// else
				word.add(values.get(property).get(i));
			}
			words.add(StringUtils.join(word, "_")); //$NON-NLS-1$
		}

		return LangFormater.format(StringUtils.join(words, " "), //$NON-NLS-1$
				concordance.getCorpus().getLang());
	}

	/**
	 * Returns a string representing the left context. Equivalent to
	 * leftContextToString("/"," ")
	 * 
	 * @return The string representation
	 */
	public String leftContextToString() {
		return leftContextToString(PROP_SEPARTOR, " ", -1); //$NON-NLS-1$
	}

	/**
	 * Left context to string.
	 *
	 * @param limit the limit
	 * @return the string
	 */
	public String leftContextToString(int limit) {
		return leftContextToString(PROP_SEPARTOR, " ", limit); //$NON-NLS-1$
	}

	/**
	 * Returns a string representing the keywords. Equivalent to
	 * keywordToString("/"," ")
	 * 
	 * @return The string representation
	 */
	public String keywordToString() {
		return keywordToString(PROP_SEPARTOR, " "); //$NON-NLS-1$
	}

	/**
	 * Returns a string representing the right context. Equivalent to
	 * rightContextToString("/"," ")
	 * 
	 * @return The string representation
	 */
	public String rightContextToString() {
		return rightContextToString(PROP_SEPARTOR, " ", -1); //$NON-NLS-1$
	}

	/**
	 * Right context to string.
	 *
	 * @param limit the limit
	 * @return the string
	 */
	public String rightContextToString(int limit) {
		return rightContextToString(PROP_SEPARTOR, " ", limit); //$NON-NLS-1$
	}

	/**
	 * Match get start.
	 *
	 * @return the integer
	 */
	public Integer matchGetStart() {
		return match.getStart();
	}

	/**
	 * Match get end.
	 *
	 * @return the integer
	 */
	public Integer matchGetEnd() {
		return match.getEnd();
	}

	/**
	 * Match get col.
	 *
	 * @param corpus the corpus
	 * @return the string
	 * @throws CqiClientException the cqi client exception
	 */
	public String matchGetCol(CQPCorpus corpus) throws CqiClientException {
		return org.txm.searchengine.cqp.corpus.query.Match.getValueForProperty(corpus.getProperty("col"), match.getStart()); //$NON-NLS-1$
	}

	/**
	 * Match get id.
	 *
	 * @param corpus the corpus
	 * @return the list
	 * @throws CqiClientException the cqi client exception
	 */
	public List<String> matchGetId(CQPCorpus corpus) throws CqiClientException {

		return org.txm.searchengine.cqp.corpus.query.Match.getValuesForProperty(corpus.getProperty("id"), match); //$NON-NLS-1$
	}

	/**
	 * Match get id.
	 *
	 * @param corpus the corpus
	 * @param positions the positions
	 * @return the list
	 * @throws CqiClientException the cqi client exception
	 */
	public List<String> matchGetId(CQPCorpus corpus, int[] positions) throws CqiClientException {

		return org.txm.searchengine.cqp.corpus.query.Match.getValuesForProperty(corpus.getProperty("id"), positions); //$NON-NLS-1$
	}

	/**
	 * Match get properties.
	 *
	 * @param corpus the corpus
	 * @param positions the positions
	 * @return the list
	 * @throws CqiClientException the cqi client exception
	 */
	public List<String> matchGetPropertie(CQPCorpus corpus, String property, int[] positions) throws CqiClientException {

		return org.txm.searchengine.cqp.corpus.query.Match.getValuesForProperty(corpus.getProperty(property), positions);
	}

	/**
	 * Match get properties.
	 *
	 * @param corpus the corpus
	 * @param property the property
	 * @param positions the positions
	 * @return the list
	 * @throws CqiClientException the cqi client exception
	 */
	public List<String> matchGetProperties(CQPCorpus corpus, Property property, int[] positions) throws CqiClientException {

		return org.txm.searchengine.cqp.corpus.query.Match.getValuesForProperty(property, positions);
	}

	/**
	 * Gets the text id.
	 *
	 * @return the text id
	 */
	public String getTextId() {
		return this.textId;
	}

	/**
	 * Gets the concordance.
	 *
	 * @return the concordance
	 */
	public Concordance getConcordance() {
		return concordance;
	}

	/**
	 * Gets the keyword IDs.
	 *
	 * @return the keyword IDs
	 */
	public List<? extends String> getKeywordIds() {
		return this.keywordsIdValues;
	}

	/**
	 * Gets the keyword position.
	 *
	 * @return the keyword position
	 */
	public int getKeywordPosition() {
		return keywordpos;
	}
}
