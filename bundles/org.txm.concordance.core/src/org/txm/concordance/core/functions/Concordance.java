// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-11-29 16:47:07 +0100 (Tue, 29 Nov 2016) $
// $LastChangedRevision: 3349 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.concordance.core.functions;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.osgi.util.NLS;
import org.txm.concordance.core.functions.comparators.LineComparator;
import org.txm.concordance.core.messages.ConcordanceCoreMessages;
import org.txm.concordance.core.preferences.ConcordancePreferences;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.objects.Match;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.QueryBasedTXMResult;
import org.txm.searchengine.core.Selection;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.Reference;
import org.txm.searchengine.cqp.ReferencePattern;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * The Class Concordance computes concordance lines using a query
 * 
 * @author jmague, mdecorde
 */
public class Concordance extends QueryBasedTXMResult {

	public static final String TARGET_SELECT = "Select"; //$NON-NLS-1$

	public static final String TARGET_SHOW = "Show"; //$NON-NLS-1$

	public static final String TARGET_KEEPLEFT = "Keep left"; //$NON-NLS-1$

	public static final String TARGET_KEEPRIGHT = "Keep right"; //$NON-NLS-1$

	/** The noconc. */
	protected static int noconc = 1;;

	/** The prefix r. */
	protected static String prefixR = "Concordances"; //$NON-NLS-1$

	// int[] textStarts = null;
	int[] CQLLimitStarts = null;

	/** The lines. */
	protected List<Line> lines;

	/** The n lines. */
	protected int nLines;

	/** The query result. */
	private Selection queryResult;

	/** The symbol. */
	private String symbol;

	private ArrayList<WordProperty> availableKeywordViewProperties;

	private ArrayList<WordProperty> availableLeftViewProperties;

	private ArrayList<WordProperty> availableRightViewProperties;

	private ArrayList<WordProperty> availableLeftSortProperties;

	private ArrayList<WordProperty> availableKeywordSortProperties;

	private ArrayList<WordProperty> availableRightSortProperties;

	/** The query. */
	@Parameter(key = ConcordancePreferences.QUERY)
	protected IQuery pQuery;


	/** The keyword analysis properties */
	@Parameter(key = ConcordancePreferences.KEYWORD_ANALYSIS_PROPERTIES)
	protected List<WordProperty> pAnalysisKeywordProperties;

	/** The analysis properties */
	@Parameter(key = ConcordancePreferences.LEFT_ANALYSIS_PROPERTIES)
	protected List<WordProperty> pAnalysisLeftProperties;

	/** The ReferncePattern sort properties */
	@Parameter(key = ConcordancePreferences.SORT_REFERENCE_PATTERN)
	protected ReferencePattern pAnalysisRefPattern;

	/** The right c analysis property. */
	@Parameter(key = ConcordancePreferences.RIGHT_ANALYSIS_PROPERTIES)
	protected List<WordProperty> pAnalysisRightProperties;

	/** used to limit context to the matches of the CQL limit query */
	@Parameter(key = ConcordancePreferences.LIMIT_CQL)
	protected String pLimitCQL;

	/** The left context size. */
	@Parameter(key = ConcordancePreferences.LEFT_CONTEXT_SIZE)
	protected Integer pLeftContextSize;

	/** The right context size. */
	@Parameter(key = ConcordancePreferences.RIGHT_CONTEXT_SIZE)
	protected Integer pRightContextSize;

	/** The top line index shown and the number of lines to show per page */
	@Parameter(key = ConcordancePreferences.INDEX)
	protected Integer pTopIndex;

	@Parameter(key = ConcordancePreferences.NB_OF_ELEMENTS_PER_PAGE)
	protected Integer pNLinesPerPage;

	/** The keyword view properties. */
	@Parameter(key = ConcordancePreferences.KEYWORD_VIEW_PROPERTIES)
	protected List<WordProperty> pViewKeywordProperties;

	/** The left c view properties. */
	@Parameter(key = ConcordancePreferences.LEFT_VIEW_PROPERTIES)
	protected List<WordProperty> pViewLeftProperties;

	/** The reference pattern. */
	@Parameter(key = ConcordancePreferences.VIEW_REFERENCE_PATTERN)
	protected ReferencePattern pViewRefPattern;

	/** The right c view properties. */
	@Parameter(key = ConcordancePreferences.RIGHT_VIEW_PROPERTIES)
	protected List<WordProperty> pViewRightProperties;

	/** The right c view properties. */
	@Parameter(key = ConcordancePreferences.TARGET_STRATEGY)
	protected String pTargetStrategy;

	/**
	 * Optional parameterQuery result already resolved. If set the pQuery is optional
	 */
	@Parameter
	private QueryResult pQueryResult;

	public void setQueryResult(QueryResult pQueryResult) {
		this.pQueryResult = pQueryResult;
	}

	public QueryResult getQueryResultParameter() {
		return pQueryResult;
	}

	/**
	 * 
	 * @param parent
	 */
	public Concordance(CQPCorpus parent) {
		super(parent);
	}

	/**
	 * 
	 * @param parametersNodePath
	 */
	public Concordance(String parametersNodePath) {
		super(parametersNodePath);
	}

	@Override
	public CQPCorpus getParent() {
		return (CQPCorpus) super.getParent();
	}

	@Override
	public boolean loadParameters() {

		try {
			this.pQuery = Query.stringToQuery(this.getStringParameterValue(ConcordancePreferences.QUERY));

			this.setCQLSeparator(this.getParent().getCQLLimitQuery());

			// this.pTopIndex = this.getIntParameterValue(ConcordancePreferences.TOP_INDEX);
			// this.pNLinesPerPage = this.getIntParameterValue(ConcordancePreferences.N_LINE_PER_PAGE);
			//
			// this.pLeftContextSize = this.getIntParameterValue(ConcordancePreferences.LEFT_CONTEXT_SIZE);
			// this.pRightContextSize = this.getIntParameterValue(ConcordancePreferences.RIGHT_CONTEXT_SIZE);

			String propertyNames = this.getStringParameterValue(ConcordancePreferences.KEYWORD_VIEW_PROPERTIES);
			this.pViewKeywordProperties = WordProperty.stringToProperties(getCorpus(), propertyNames);
			this.availableKeywordViewProperties = new ArrayList<>(getCorpus().getOrderedProperties());
			this.availableKeywordViewProperties.removeAll(pViewKeywordProperties);

			propertyNames = this.getStringParameterValue(ConcordancePreferences.LEFT_VIEW_PROPERTIES);
			this.pViewLeftProperties = WordProperty.stringToProperties(getCorpus(), propertyNames);
			this.availableLeftViewProperties = new ArrayList<>(getCorpus().getOrderedProperties());
			this.availableLeftViewProperties.removeAll(pViewLeftProperties);

			propertyNames = this.getStringParameterValue(ConcordancePreferences.RIGHT_VIEW_PROPERTIES);
			this.pViewRightProperties = WordProperty.stringToProperties(getCorpus(), propertyNames);
			this.availableRightViewProperties = new ArrayList<>(getCorpus().getOrderedProperties());
			this.availableRightViewProperties.removeAll(pViewRightProperties);

			propertyNames = this.getStringParameterValue(ConcordancePreferences.KEYWORD_ANALYSIS_PROPERTIES);
			this.pAnalysisKeywordProperties = WordProperty.stringToProperties(getCorpus(), propertyNames);
			this.availableKeywordSortProperties = new ArrayList<>(getCorpus().getOrderedProperties());
			this.availableKeywordSortProperties.removeAll(pAnalysisKeywordProperties);

			propertyNames = this.getStringParameterValue(ConcordancePreferences.LEFT_ANALYSIS_PROPERTIES);
			this.pAnalysisLeftProperties = WordProperty.stringToProperties(getCorpus(), propertyNames);
			this.availableLeftSortProperties = new ArrayList<>(getCorpus().getOrderedProperties());
			this.availableLeftSortProperties.removeAll(pAnalysisLeftProperties);

			propertyNames = this.getStringParameterValue(ConcordancePreferences.RIGHT_ANALYSIS_PROPERTIES);
			this.pAnalysisRightProperties = WordProperty.stringToProperties(getCorpus(), propertyNames);
			this.availableRightSortProperties = new ArrayList<>(getCorpus().getOrderedProperties());
			this.availableRightSortProperties.removeAll(pAnalysisRightProperties);

			
			String refPropertyNames = this.getStringParameterValue(ConcordancePreferences.VIEW_REFERENCE_PATTERN);
			
			if (TXMPreferences.getNode(this.getParametersNodePath()).get(ConcordancePreferences.VIEW_REFERENCE_PATTERN, null) == null) { // if the result did not define the parameter use the corpus preference
				String refPropertyNames2 = this.getCorpus().getProject().getCommandPreferences("concordance").get(ConcordancePreferences.VIEW_REFERENCE_PATTERN); //$NON-NLS-1$
				if (refPropertyNames2 != null && refPropertyNames2.length() > 0) {
					ReferencePattern ref = ReferencePattern.stringToReferencePattern(this.getCorpus(), refPropertyNames2);
					if (ref.getProperties().size() > 0) { // ensure the ref properties is well set
						refPropertyNames = refPropertyNames2;
					}
				}
			}
			
			
			
			if ("*".equals(refPropertyNames)) { //$NON-NLS-1$
				Property refProperty = this.getCorpus().getProperty("ref");//$NON-NLS-1$
				if (refProperty == null) {
					StructuralUnitProperty sup_textid = this.getCorpus().getTextIdStructuralUnitProperty();
					this.pViewRefPattern = new ReferencePattern(sup_textid);
				}
				else {
					this.pViewRefPattern = new ReferencePattern(refProperty);
				}
			}
			else {
				this.pViewRefPattern = ReferencePattern.stringToReferencePattern(this.getCorpus(), refPropertyNames);
			}

			refPropertyNames = this.getStringParameterValue(ConcordancePreferences.SORT_REFERENCE_PATTERN);
			if (TXMPreferences.getNode(this.getParametersNodePath())
					.get(ConcordancePreferences.SORT_REFERENCE_PATTERN, null) == null) { // if the result did not define the parameter use the corpus preference
				String refPropertyNames2 = this.getCorpus().getProject().getCommandPreferences("concordance").get(ConcordancePreferences.SORT_REFERENCE_PATTERN); //$NON-NLS-1$
				if (refPropertyNames2 != null && refPropertyNames2.length() > 0) {
					ReferencePattern ref = ReferencePattern.stringToReferencePattern(this.getCorpus(), refPropertyNames2);
					if (ref.getProperties().size() > 0) { // ensure the ref properties is well set
						refPropertyNames = refPropertyNames2;
					}
				}
			}
			if ("*".equals(refPropertyNames)) { //$NON-NLS-1$
				Property refProperty = getCorpus().getProperty("ref");//$NON-NLS-1$
				if (refProperty == null) {
					StructuralUnitProperty sup_textid = this.getCorpus().getTextIdStructuralUnitProperty();
					this.pAnalysisRefPattern = new ReferencePattern(sup_textid);
				}
				else {
					this.pAnalysisRefPattern = new ReferencePattern(refProperty);
				}
			}
			else {
				this.pAnalysisRefPattern = ReferencePattern.stringToReferencePattern(getCorpus(), refPropertyNames);
			}
		}
		catch (CqiClientException e) {
			Log.printStackTrace(e);
			return false;
		}

		return true;
	}

	/**
	 * Checks if this concordance is equal to specified concordance.
	 * 
	 * @param other
	 * @return
	 */
	public boolean equals(Concordance other) {
		if (other == null) return false;

		if (!this.getCorpus().equals(other.getCorpus())) return false;

		IQuery q1 = other.getQuery();
		IQuery q2 = this.getQuery();
		if (q1 != null && q2 != null) return q1.equals(q2);

		return true;
	}

	/**
	 * Compute concordance.
	 *
	 * @return true, if successful
	 * @throws CqiClientException
	 */
	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {
		if (pQueryResult != null) {
			this.queryResult = pQueryResult;
		}
		else {
			try {
				this.queryResult = pQuery.getSearchEngine().query(this.getCorpus(), pQuery, pQuery.getQueryString().replace(" ", "_") + "_concordance", true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			catch (Exception e) {
				this.nLines = 0;
				this.pTopIndex = 0;
				this.lines = new ArrayList<>();
				Log.warning(NLS.bind(ConcordanceCoreMessages.CQPErrorP0, e.getMessage()));
				Log.printStackTrace(e);
				return false;
			}
			// this.queryResult = this.getCorpus().query(pQuery, pQuery.getQueryString().replace(" ", "_") + "_concordance", true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}

		this.pTopIndex = 0;

		this.nLines = queryResult.getNMatch();
		this.lines = new ArrayList<>(Collections.nCopies(nLines, (Line) null));// lines are lazily fetched; we force an

		return true;// lines.size() > 0;
	}

	/**
	 * Adds the txt sep.
	 *
	 * @param str the str
	 * @param separator the separator
	 * @return the string
	 */
	private String addTxtSep(String str, String separator) {
		return separator + (str.replace(separator, separator + separator)) + separator;
	}

	/**
	 * As r matrix.
	 *
	 * @return the string
	 * @throws RWorkspaceException the r workspace exception
	 * @throws CqiServerError
	 * @throws IOException
	 */
	public String asRMatrix() throws Exception {
		symbol = prefixR + noconc++;
		String[] refs = new String[this.getNLines()];
		String[] lefts = new String[this.getNLines()];
		String[] keywords = new String[this.getNLines()];
		String[] rights = new String[this.getNLines()];

		int i = 0;
		try {
			for (Line line : this.getLines(0, this.getNLines())) {
				refs[i] = line.getViewRef().toString();
				lefts[i] = line.leftContextToString();
				keywords[i] = line.keywordToString();
				rights[i] = line.rightContextToString();
				i++;
			}
		}
		catch (CqiClientException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return ""; //$NON-NLS-1$
		}

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		rw.addVectorToWorkspace("concrefs", refs); //$NON-NLS-1$
		rw.addVectorToWorkspace("conclefts", lefts); //$NON-NLS-1$
		rw.addVectorToWorkspace("conckeywords", keywords); //$NON-NLS-1$
		rw.addVectorToWorkspace("concrights", rights); //$NON-NLS-1$

		rw.eval(symbol + "<- matrix(data = c(concrefs, conclefts, conckeywords, concrights), nrow = " + this.getNLines() + ", ncol = 4)"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval("colnames(" + symbol + " ) <- c('refs', 'leftcontext', 'keyword', 'rightcontext')"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval(symbol + "<- list(data=" + symbol //$NON-NLS-1$
				+ ", leftcontext=" + this.pLeftContextSize //$NON-NLS-1$
				+ ", rightcontext=" + this.pRightContextSize //$NON-NLS-1$
				// + ", query=\""+this.query.getQueryString()+"\""
				+ ")"); //$NON-NLS-1$

		return symbol;
	}

	@Override
	public boolean canCompute() {
		boolean go = true;
		if (this.getCorpus() == null) {
			Log.finest("Concordance.canCompute(): Corpus is not set."); //$NON-NLS-1$
			go = false;
		}
		if (pQueryResult == null && (pQuery == null || pQuery.isEmpty())) {
			Log.finer("Concordance.canCompute(): Query is not set or empty."); //$NON-NLS-1$
			go = false;
		}
		return go;
	}

	@Override
	public void clean() {
		try {
			if (queryResult != null && CQPSearchEngine.isInitialized()) {
				queryResult.drop();
			}
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	/**
	 * Gets the analysis property.
	 *
	 * @return the analysisProperty
	 */
	public List<WordProperty> getAnalysisProperty() {
		return pAnalysisKeywordProperties;
	}

	/**
	 * Gets the corpus.
	 *
	 * @return the corpus
	 */
	public CQPCorpus getCorpus() {
		return (CQPCorpus) this.parent;
	}

	@Override
	public String getSimpleDetails() {
		return getComputingDoneMessage();
	}

	@Override
	public String getDetails() {

		if (isDirty()) {
			return parent.getName();
		}
		else {
			StringBuffer buf = new StringBuffer();
			buf.append("Concordance:\n"); //$NON-NLS-1$
			buf.append("\tCorpus: " + this.getCorpus() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			buf.append("\tQuery: " + this.getQuery() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			buf.append("\tLeft Context View property: " + this.getLeftViewProperties() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			buf.append("\tKeyword View property: " + this.getKeywordViewProperties() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			buf.append("\tRight Context View property: " + this.getRightViewProperties() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			buf.append("\tLeft Context Sort property: " + this.getLeftAnalysisProperties() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			buf.append("\tKeyword Sort property: " + this.getKeywordAnalysisProperties() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			buf.append("\tRight Context Sort property: " + this.getRightAnalysisProperties() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			buf.append("\tReference View Pattern: " + this.getRefViewPattern() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			buf.append("\tReference Sort Pattern: " + this.getRefAnalysePattern() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			return buf.toString();
		}
	}

	@Override
	public String[] getExportTXTExtensions() {
		return new String[] { "*.csv" }; //$NON-NLS-1$
	}

	/**
	 * Gets the keyword analysis property.
	 *
	 * @return the analysisProperty
	 */
	public List<WordProperty> getKeywordAnalysisProperties() {
		return pAnalysisKeywordProperties;
	}

	/**
	 * Gets the keywords id.
	 *
	 * @param text the text
	 * @param from the from
	 * @param to the to
	 * @return the keywords id
	 */
	public List<String> getKeywordsId(String text, int from, int to) {
		// System.out.println(Messages.Concordance_5+from+" "+to); //$NON-NLS-2$
		HashSet<String> allids = new HashSet<>();
		for (int i = from; i < to && i < lines.size(); i++) {
			if (i >= 0 && i < lines.size()) {
				Line line = lines.get(i);
				// System.out.println("line text "+line.getTextId());
				if (line.getTextId() != null && line.getTextId().equals(text)) {
					allids.addAll(line.getKeywordIds());
				}
			}
		}
		return new ArrayList<>(allids);
	}

	/**
	 * Gets the keyword view properties.
	 *
	 * @return the keywordViewProperties
	 */
	public List<WordProperty> getKeywordViewProperties() {
		return pViewKeywordProperties;
	}

	/**
	 * Gets the left c analysis property.
	 *
	 * @return the analysisProperty
	 */
	public List<WordProperty> getLeftAnalysisProperties() {
		return pAnalysisLeftProperties;
	}

	/**
	 * Gets the left context size.
	 *
	 * @return the leftContextSize
	 */
	public Integer getLeftContextSize() {
		return pLeftContextSize;
	}

	/**
	 * Gets the left c view properties.
	 *
	 * @return the leftCViewProperties
	 */
	public List<WordProperty> getLeftViewProperties() {
		return pViewLeftProperties;
	}

	/**
	 * Returns all lines.
	 * 
	 * @return
	 * @throws CqiClientException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public List<Line> getLines() throws Exception {
		return getLines(0, this.getNLines());
	}

	/**
	 * Returns the line between from and to.
	 * 
	 * @param from
	 *            the from
	 * @param to
	 *            the to
	 * 
	 * @return the lines
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 * @throws CqiServerError
	 * @throws IOException
	 */
	public List<Line> getLines(int from, int to) throws Exception {

		if (nLines == 0) {
			return new ArrayList<>();
		}

		Log.finest(NLS.bind(ConcordanceCoreMessages.retrievingP0Results, to - from + 1));

		AbstractCqiClient cqiClient = CorpusManager.getCorpusManager().getCqiClient();

		long start = System.currentTimeMillis();

		// check asked lines numbers
		if (from < 0) from = 0;
		if (to < 0) to = 0;
		if (from > to) from = to;
		if (to >= nLines) to = nLines - 1;

		List<? extends Match> matches = queryResult.getMatches(from, to);
		//		boolean hasTarget = false;
		//		for (Match m : matches) {
		//			if (m.getTarget() >= 0) {
		//				hasTarget = true;
		//				break;
		//			}
		//		}

		// System.out.println("cqllimit="+cql_limit);
		// System.out.println("CQLLimitStarts="+CQLLimitStarts);
		if (pLimitCQL != null && CQLLimitStarts == null) { // initialize text limits
			try {
				//				CorpusCommandPreferences prefs = getProject().getCommandPreferences("concordance");
				//				if (prefs != null && "list".equals(prefs.get("context_limits_type"))) {
				//					String str = prefs.get("context_limits");
				//					TreeSet<Integer> positions = new TreeSet<Integer>();
				//						String[] ss = str.split(",");
				//						
				//						for (String s : ss) {
				//							int[] tmp = this.getCorpus().getStartLimits("<" + s + "> []");
				//							for (int i = 0 ; i < tmp.length ; i++) positions.add(tmp[i]);
				//							tmp = this.getCorpus().getStartLimits("[] </" + s + ">");
				//							for (int i = 0 ; i < tmp.length ; i++) positions.add(tmp[i]);
				//						}
				//						
				//						CQLLimitStarts = new int[positions.size()];
				//						int i = 0;
				//						for (int p : positions) {
				//							CQLLimitStarts[i] = p;
				//							i++;
				//						}
				//				} else {
				Log.fine("Get CQL limit for query=" + pLimitCQL); //$NON-NLS-1$
				CQLLimitStarts = this.getCorpus().getStartLimits(pLimitCQL);
				if (CQLLimitStarts.length > 0 && CQLLimitStarts[0] != 0) {
					int[] tmp = new int[CQLLimitStarts.length + 1];
					System.arraycopy(CQLLimitStarts, 0, tmp, 1, CQLLimitStarts.length);
					CQLLimitStarts = tmp;
				}
				//				}
			}
			catch (Exception e) {
				org.txm.utils.logger.Log.printStackTrace(e);
				Log.severe(NLS.bind(ConcordanceCoreMessages.failedToGetTextsLimitsColonP0, e));
			}

		}

		int currentCQLLimit = 0;
		// System.out.println("texts: "+Arrays.toString(textStarts));
		// we create the blocks of lines not created yet that may occur between
		// from and to
		// int step = Math.max(100/(to - from), 1);
		for (int i = from; i <= to; i++) {
			int j;
			for (j = i; j <= to && lines.get(j) == null; j++)
				; // THIS IS MANDATORY
			// j is the index of the first next existing line
			if (i != j) { // i does point at the beginning of a block of non
				// existing lines. We have to create the lines between i and j

				// if (monitorIsCanceled()) return new ArrayList<>(); // check if user canceled the Job

				// First we get all the data we need : values for the view
				// properties, the analysis property and the reference
				List<Integer> beginingOfKeywordsPositions = new ArrayList<>(j - i);
				List<Integer> targetPositions = new ArrayList<>(j - i);
				List<Integer> lengthOfKeywords = new ArrayList<>(j - i);
				List<Integer> beginingOfRightCtxPositions = new ArrayList<>(j - i);
				List<Integer> lengthOfRightCtx = new ArrayList<>(j - i);
				List<Integer> beginingOfLeftCtxPositions = new ArrayList<>(j - i);
				List<Integer> lengthOfLeftCtx = new ArrayList<>(j - i);

				// get all first token and last token of all lines
				int currentKeywordPos, currentKeywordEndPos;// = matches.get(0).getStart();
				// currentText = previousPivotText;

				//ArrayList<Match> subsetMatch = new ArrayList<>();

				for (int k = 0; k < j - i; k++) {
					Match match = matches.get(i - from + k);
					//subsetMatch.add(match);
					// System.out.println("match: "+match);
					// 1) find current text using keyword position
					currentKeywordPos = match.getStart();
					currentKeywordEndPos = match.getEnd();

					//					if (hasTarget) {
					//						if (Concordance.TARGET_SELECT.equals(pTargetStrategy)) {
					//							currentKeywordPos = match.getTarget();
					//							currentKeywordEndPos = match.getTarget();
					//						} else if (Concordance.TARGET_KEEPLEFT.equals(pTargetStrategy)) {
					//							currentKeywordPos = match.getStart();
					//							currentKeywordEndPos = match.getTarget();
					//						} else if (Concordance.TARGET_KEEPRIGHT.equals(pTargetStrategy)) {
					//							currentKeywordPos = match.getTarget();
					//							currentKeywordEndPos = match.getEnd();
					//						} else if (Concordance.TARGET_SHOW.equals(pTargetStrategy)) {
					//							currentKeywordPos = match.getStart();
					//							currentKeywordEndPos = match.getEnd();
					//						}
					//					}

					if (CQLLimitStarts != null && currentCQLLimit < CQLLimitStarts.length) {
						while (currentCQLLimit < CQLLimitStarts.length && CQLLimitStarts[currentCQLLimit] <= currentKeywordPos) { // find the match's text
							currentCQLLimit++; // matches are ordered
						}
						if (currentCQLLimit > 0) {
							currentCQLLimit--; // get the previous text
						}
					}

					// System.out.println("text no: "+currentText+" text position: "+textStarts[currentText]);
					// LEFT CONTEXT LIMITS
					int ctxPosition = currentKeywordPos - pLeftContextSize; // left context start position
					if (CQLLimitStarts != null && currentCQLLimit < CQLLimitStarts.length) {
						if (CQLLimitStarts[currentCQLLimit] > ctxPosition) { // test if the end of left context is in the same text
							ctxPosition = CQLLimitStarts[currentCQLLimit]; // the context start is the start of the text
							// System.out.println("fix right "+currentText+" pos: "+ctxPosition);
						}
					}
					beginingOfLeftCtxPositions.add(ctxPosition);
					lengthOfLeftCtx.add(currentKeywordPos - ctxPosition); // distance between let context start position and keyword position
					// System.out.println("left: "+ctxPosition+ "len: "+(match.getStart() - ctxPosition));

					// KEYWORD LIMITS
					beginingOfKeywordsPositions.add(currentKeywordPos);
					lengthOfKeywords.add(currentKeywordEndPos - currentKeywordPos + 1);

					// TARGET POSITION


					//					if (Concordance.TARGET_SELECT.equals(pTargetStrategy) || Concordance.TARGET_KEEPLEFT.equals(pTargetStrategy) || Concordance.TARGET_KEEPRIGHT.equals(pTargetStrategy)) {
					//						targetPositions.add(match.getTarget());
					//					} else if (Concordance.TARGET_SHOW.equals(pTargetStrategy)) {
					targetPositions.add(match.getTarget());
					//					}

					// check if the end of keyword pass a text limit
					currentKeywordPos = currentKeywordEndPos;

					if (CQLLimitStarts != null)
						if (currentCQLLimit < CQLLimitStarts.length) {
							while (currentCQLLimit < CQLLimitStarts.length && CQLLimitStarts[currentCQLLimit] <= currentKeywordPos) { // find the match's text
								currentCQLLimit++; // matches are ordered
							}
							if (currentCQLLimit > 0) {
								currentCQLLimit--; // get the previous text
							}
						}
					// find the right limit
					ctxPosition = currentKeywordEndPos + pRightContextSize;

					if (CQLLimitStarts != null)
						if (currentCQLLimit + 1 < CQLLimitStarts.length) {
							if (CQLLimitStarts[currentCQLLimit + 1] < ctxPosition) { // test if the right context last position is in the same text as the pivot
								ctxPosition = CQLLimitStarts[currentCQLLimit + 1] - 1; // the context end is the next text start position
							}
						}

					// if (ctxPosition > corpus.getSize()) ctxPosition = corpus.getSize();

					beginingOfRightCtxPositions.add(currentKeywordEndPos + 1);
					lengthOfRightCtx.add(ctxPosition - currentKeywordEndPos);
					// System.out.println("right: "+ctxPosition+" len: "+(ctxPosition - match.getEnd() + 1)+"\n");
				}

				// System.out.println("RIGHT ctx lengths: "+lengthOfRightCtx);
				// get all view property values of all lines
				Map<Property, List<List<String>>> keywordsViewPropValues = new LinkedHashMap<>();
				Map<Property, List<List<String>>> leftCtxViewPropValues = new LinkedHashMap<>();
				Map<Property, List<List<String>>> rightCtxViewPropValues = new LinkedHashMap<>();
				for (Property property : pViewLeftProperties) {
					leftCtxViewPropValues.put(property, cqiClient.getData(property, beginingOfLeftCtxPositions,
							lengthOfLeftCtx));
				}
				for (Property property : pViewRightProperties) {
					rightCtxViewPropValues.put(property, cqiClient.getData(property, beginingOfRightCtxPositions, lengthOfRightCtx));
				}
				for (Property property : pViewKeywordProperties) {
					keywordsViewPropValues.put(property, cqiClient.getData(property, beginingOfKeywordsPositions, lengthOfKeywords));
				}

				// get all analysis property values of all lines
				HashMap<Property, List<List<String>>> leftCtxAnaPropValues = new LinkedHashMap<>();
				for (Property p : pAnalysisLeftProperties) {
					leftCtxAnaPropValues.put(p, cqiClient.getData(p, beginingOfLeftCtxPositions, lengthOfLeftCtx));
				}
				HashMap<Property, List<List<String>>> keywordsAnaPropValues = new LinkedHashMap<>();
				for (Property p : pAnalysisKeywordProperties) {
					keywordsAnaPropValues.put(p, cqiClient.getData(p, beginingOfKeywordsPositions, lengthOfKeywords));
				}
				HashMap<Property, List<List<String>>> rightCtxAnaPropValues = new LinkedHashMap<>();
				for (Property p : pAnalysisRightProperties) {
					rightCtxAnaPropValues.put(p, cqiClient.getData(p, beginingOfRightCtxPositions, lengthOfRightCtx));
				}
				// get all view reference values of all lines
				Map<Property, List<List<String>>> refValues = new LinkedHashMap<>();
				for (Property property : pViewRefPattern) {
					refValues.put(property, cqiClient.getData(property,
							beginingOfKeywordsPositions,
							Collections.nCopies(beginingOfKeywordsPositions.size(), 1)));
				}

				// get all sort reference values of all lines
				Map<Property, List<List<String>>> refSortValues = new LinkedHashMap<>();
				for (Property property : pAnalysisRefPattern) {
					refSortValues.put(property, cqiClient.getData(property, beginingOfKeywordsPositions,
							Collections.nCopies(beginingOfKeywordsPositions.size(), 1)));
				}

				// get all lines' tokens ids
				Property id = this.getCorpus().getProperty("id"); //$NON-NLS-1$
				StructuralUnit text = this.getCorpus().getStructuralUnit("text"); //$NON-NLS-1$
				Property text_id = text.getProperty("id"); //$NON-NLS-1$

				List<List<String>> keywordsIdValues;
				List<List<String>> keywordsTextValues;
				if (id != null && text_id != null) {
					/** The occurrences Ids&Text => BackToText */
					keywordsIdValues = cqiClient.getData(id, beginingOfKeywordsPositions, lengthOfKeywords);
					keywordsTextValues = cqiClient.getData(text_id, beginingOfKeywordsPositions,
							Collections.nCopies(beginingOfKeywordsPositions.size(), 1));
				}
				else {
					keywordsTextValues = null;
					keywordsIdValues = null;
					Log.warning(ConcordanceCoreMessages.failedToRetrieveTextidPropertyFromLines);
				}

				// Since we get the value only for the first word of the
				// keyword, we know that refValues maps each property to a list
				// of list of size==1 : refValue.get(p).get(i).size()==1

				// Second, we build the lines
				for (int k = 0; k < j - i; k++) {
					// worked(1);
					// get the kth line's analysis property values
					HashMap<Property, List<String>> lineLeftCtxAnaPropValue = new LinkedHashMap<>();
					for (Property p : pAnalysisLeftProperties) {
						lineLeftCtxAnaPropValue.put(p, leftCtxAnaPropValues.get(p).get(k));
					}
					HashMap<Property, List<String>> lineKeywordsAnaPropValue = new LinkedHashMap<>();
					for (Property p : pAnalysisKeywordProperties) {
						lineKeywordsAnaPropValue.put(p, keywordsAnaPropValues.get(p).get(k));
					}
					HashMap<Property, List<String>> lineRightCtxAnaPropValue = new LinkedHashMap<>();
					for (Property p : pAnalysisRightProperties) {
						lineRightCtxAnaPropValue.put(p, rightCtxAnaPropValues.get(p).get(k));
					}
					// get the kth line's view property values
					Map<Property, List<String>> lineLeftCtxViewPropValue = new LinkedHashMap<>();
					Map<Property, List<String>> lineKeywordsViewPropValue = new LinkedHashMap<>();
					Map<Property, List<String>> lineRightCtxViewPropValue = new LinkedHashMap<>();
					for (Property property : pViewLeftProperties) {
						lineLeftCtxViewPropValue.put(property, leftCtxViewPropValues.get(property).get(k));
					}
					for (Property property : pViewRightProperties) {
						lineRightCtxViewPropValue.put(property, rightCtxViewPropValues.get(property).get(k));
					}
					for (Property property : pViewKeywordProperties) {
						lineKeywordsViewPropValue.put(property, keywordsViewPropValues.get(property).get(k));
					}

					// get the kth line reference values
					LinkedHashMap<Property, String> ref = new LinkedHashMap<>();
					for (Property property : pViewRefPattern) {
						ref.put(property, refValues.get(property).get(k).get(0));
					}

					// get the kth line reference values
					LinkedHashMap<Property, String> refAna = new LinkedHashMap<>();
					for (Property property : pAnalysisRefPattern) {
						refAna.put(property, refSortValues.get(property).get(k).get(0));
					}

					// get Text id of the line
					String TextId;
					List<String> wordids;
					if (keywordsIdValues != null && keywordsTextValues != null) {
						TextId = keywordsTextValues.get(k).get(0);
						wordids = keywordsIdValues.get(k);
					}
					else {
						TextId = ConcordanceCoreMessages.undefined;
						wordids = new ArrayList<>();
					}

					int targetpos = targetPositions.get(k);

					Match match = matches.get(i - from + k);

					Line line = new Line(this, wordids, TextId,
							lineLeftCtxViewPropValue,
							lineKeywordsViewPropValue,
							lineRightCtxViewPropValue, lineLeftCtxAnaPropValue,
							lineKeywordsAnaPropValue, lineRightCtxAnaPropValue,
							new Reference(ref, pViewRefPattern),
							new Reference(refAna, pAnalysisRefPattern),
							match,
							targetpos);
					lines.set(i + k, line);
				}
			}
			i = j;
		}
		List<Line> res = lines.subList(from, to + 1);// our to is inclusive,
		// sublist's is exlusive
		Log.finest((to - from + 1) + ConcordanceCoreMessages.resultsRetrievedIn + (System.currentTimeMillis() - start) + "ms"); //$NON-NLS-1$

		return res;
	}

	/**
	 * Gets the query result matches.
	 * 
	 * @return
	 * @throws CqiClientException
	 */
	public List<? extends Match> getMatches() throws Exception {
		return queryResult.getMatches();
	}

	/**
	 * Gets the number of lines.
	 * 
	 * @return the n lines
	 */
	public Integer getNLines() {
		return nLines;
	}

	/**
	 * Returns the query used to build the concordance.
	 * 
	 * @return the query
	 */
	@Override
	public IQuery getQuery() {
		return pQuery;
	}

	public Selection getQueryResult() {
		return queryResult;
	}

	public ReferencePattern getRefAnalysePattern() {
		return pAnalysisRefPattern;
	}

	/**
	 * Gets the reference pattern.
	 *
	 * @return the referencePattern
	 */
	public ReferencePattern getRefViewPattern() {
		return pViewRefPattern;
	}

	/**
	 * Gets the right c analysis property.
	 *
	 * @return the analysisProperty
	 */
	public List<WordProperty> getRightAnalysisProperties() {
		return pAnalysisRightProperties;
	}

	/**
	 * Gets the right context size.
	 *
	 * @return the rightContextSize
	 */
	public Integer getRightContextSize() {
		return pRightContextSize;
	}

	/**
	 * Gets the right c view properties.
	 *
	 * @return the rightCViewProperties
	 */
	public List<WordProperty> getRightViewProperties() {
		return pViewRightProperties;
	}

	@Override
	public String getSimpleName() {
		if (this.pQuery != null && !this.pQuery.isEmpty()) {
			return this.pQuery.asString();
		}
		else {
			return this.getEmptyName();
		}
	}

	@Override
	public String getName() {
		return this.getCorpus().getName() + TXMPreferences.PARENT_NAME_SEPARATOR + this.getSimpleName();
	}

	@Override
	public String getComputingStartMessage() {
		if (this.pQuery == null) {
			return TXMCoreMessages.bind(ConcordanceCoreMessages.concordanceOfP0InTheP1Corpus, ConcordanceCoreMessages.NoQuery, this.getCorpus());
		}
		else {
			return TXMCoreMessages.bind(ConcordanceCoreMessages.concordanceOfP0InTheP1Corpus, this.pQuery.asString(), this.getCorpus());
		}
	}

	@Override
	public String getComputingDoneMessage() {
		if (this.getNLines() == 0) {
			return TXMCoreMessages.common_noResults;
		}
		else {
			return TXMCoreMessages.bind(TXMCoreMessages.common_P0Occurrences, TXMCoreMessages.formatNumber(this.getNLines()));
		}
	}


	/**
	 * Gets the symbol.
	 *
	 * @return the symbol
	 */
	public String getSymbol() {
		return symbol;
	}

	/**
	 * Gets the line index linked to the global token position in corpus according to the current concordance sorting.
	 * 
	 * @param globalPosition
	 * @return the index in the current result page
	 */
	public int indexOf(int globalPosition) {
		int index = -1;

		// FIXME: works but may be optimized (e.g. get range by 50 token until the token is found instead of getting all the lines)
		try {
			List<Line> allLines = this.getLines(0, this.nLines);

			for (int i = 0; i < allLines.size(); i++) {
				Line line = allLines.get(i);
				if (line != null && line.getMatch().getStart() == globalPosition) {
					index = i;
					break;
				}
			}
		}
		catch (Exception e) {
			System.out.println("Error: " + e); //$NON-NLS-1$
			Log.printStackTrace(e);
		}

		// FIXME: tests for optimization by range
		// try {
		// List<Line> lines;
		// int rangeLength = 20;
		// int rangeMax = 0;
		// for(int i = 0; i < this.nLines; i += rangeLength) {
		// rangeMax += rangeLength;
		// lines = this.getLines(i, rangeMax);
		// System.out.println("Concordance.indexOf(): getLines " + i + " / " + rangeMax);
		//
		// for(int j = 0; j < lines.size(); j++) {
		// Line line = lines.get(j);
		// if(line != null && line.getMatch().getStart() == globalPosition) {
		// index = i + j;
		// break;
		// }
		// }
		// if(index != -1) {
		// break;
		// }
		// }
		// }
		// catch(CqiClientException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// catch(IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// catch(CqiServerError e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }


		// FIXME: tests for optimization
		// Problem here is that we need the sorted index of
		// try {
		// List<Match> matches = queryResult.getMatches(0, this.nLines - 1);
		// Collections.sort(matches, );
		// for(int i = 0; i < matches.size(); i++) {
		// if(matches.get(i).getStart() == globalPosition) {
		// index = i;
		// break;
		// }
		// }
		// }
		// catch(CqiClientException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }



		return index;
	}

	/**
	 * reload a part of the concordance lines
	 * 
	 * @param topLine
	 * @param bottomLine
	 * @return
	 * @throws CqiClientException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public List<Line> reloadLines(int topLine, int bottomLine) throws Exception {
		if (bottomLine >= lines.size()) bottomLine = lines.size();
		for (int i = topLine; i < bottomLine; i++) {
			lines.set(i, null);
		}

		return getLines(topLine, bottomLine);
	}

	/**
	 * reload a part of the concordance lines
	 * 
	 * @return
	 * @throws CqiClientException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public List<Line> reloadCurrentLines() throws Exception {
		for (int i = this.pTopIndex; i < this.pTopIndex + this.pNLinesPerPage && i < lines.size(); i++) {
			lines.set(i, null);
		}

		return getLines(this.pTopIndex, this.pTopIndex + this.pNLinesPerPage);
	}


	/**
	 * Update the QueryResult removing the match of the given line.
	 * 
	 * It uses the "delete" command but needs to find the match number of all the matches of the queryresult (may be long depending on the queryresult size)
	 * 
	 * @param line the line to remove
	 * @return true if the line was successfully removed
	 * @throws CqiClientException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public boolean removeLine(Line line) throws CqiClientException, IOException, CqiServerError {

		if (queryResult.delete(line.getMatch())) {
			this.lines.remove(line);
			try {
				this.nLines = queryResult.getNMatch();
				this.setAltered();
				return true;
			}
			catch (Exception e) {
				Log.printStackTrace(e);
			}
		}

		return false;

		// AbstractCqiClient CQI = CorpusManager.getCorpusManager().getCqiClient();
		// if (CQI instanceof MemCqiClient) {
		// MemCqiClient MCQI = (MemCqiClient)CQI;
		//
		// Match m = line.getMatch();
		// Match m2;
		// List<? extends Match> allMatches = queryResult.getMatches();
		// int to = allMatches.size();
		// for (int i = 0 ; i < to; i++) {
		// m2 = allMatches.get(i);
		// if (m2.getStart() == m.getStart() && m2.getEnd() == m.getEnd()) {
		// String query = "delete "+this.queryResult.getQualifiedCqpId()+" "+i+";";
		// MCQI.query(query);
		//
		//
		// }
		// }
		// }
		// return false;
	}

	/**
	 * Update the QueryResult removing the match of the given line.
	 * 
	 * It uses the "delete" command but needs to find the match number of all the matches of the queryresult (may be long depending on the queryresult size)
	 * 
	 * @param linesToRemove the lines to remove
	 * @return
	 * @throws CqiClientException
	 * @throws IOException
	 * @throws CqiServerError
	 */
	public boolean removeLines(List<Line> linesToRemove) throws Exception {

		// reorder lines to start with the last line
		Collections.sort(linesToRemove, new Comparator<Line>() {

			@Override
			public int compare(Line o1, Line o2) {
				return o2.getKeywordPosition() - o1.getKeywordPosition();
			}
		});
		ArrayList<Match> matchesToRemove = new ArrayList<>();
		for (Line line : linesToRemove) {
			matchesToRemove.add(line.getMatch());
		}
		if (queryResult.delete(matchesToRemove)) {
			boolean ret = lines.removeAll(linesToRemove);

			this.nLines = queryResult.getNMatch();
			this.setAltered();
			return ret;
		}
		return false;
	}

	/**
	 * Brutal way to reset the concordance lines. they will be recomputed when getLines(...) is called
	 */
	public void resetLines() {
		lines = new ArrayList<>(Collections.nCopies(nLines, (Line) null)); // empty lines
	}

	/**
	 * Set the analysis property.
	 * 
	 * @param list the new analysis property
	 */
	public void setAnalysisProperty(List<WordProperty> list) {
		this.pAnalysisLeftProperties = list;
		this.pAnalysisRightProperties = list;
		this.pAnalysisKeywordProperties = list;
		resetLines();
	}

	/**
	 * Set the analysis property per column.
	 * 
	 * @param leftProps list the new analysis property
	 * @param keywordProps list the new analysis property
	 * @param rightProps list the new analysis property
	 */
	public void setAnalysisProperty(List<WordProperty> leftProps, List<WordProperty> keywordProps, List<WordProperty> rightProps) {
		this.pAnalysisLeftProperties = leftProps;
		this.pAnalysisRightProperties = keywordProps;
		this.pAnalysisKeywordProperties = rightProps;
		resetLines();
	}

	/**
	 * Set both left and right contexts size.
	 * 
	 * @param size the new context size
	 */
	public void setContextSize(int size) {
		setContextSize(size, size);
	}

	/**
	 * Set left and right contexts size.
	 *
	 * @param leftsize the leftsize
	 * @param rightsize the rightsize
	 */
	public void setContextSize(int leftsize, int rightsize) {
		this.pLeftContextSize = leftsize;
		this.pRightContextSize = rightsize;
		this.setDirty();
	}

	public void setCQLSeparator(String cql_limit) {
		this.pLimitCQL = cql_limit;
		// updateDirty(pLimitCQL, cql_limit);
	}

	/**
	 * Set the analysis propertyof the left context.
	 * 
	 * @param selectedKeywordSortProperty
	 *            the new analysis property
	 */
	public void setKeywordAnalysisProperties(List<WordProperty> selectedKeywordSortProperty) {
		// updateDirty(pAnalysisKeywordProperties, selectedKeywordSortProperty);
		this.pAnalysisKeywordProperties = selectedKeywordSortProperty;
		resetLines();
	}

	/**
	 * Set the analysis propertyof the left context.
	 * 
	 * @param selectedLeftSortProperty
	 *            the new analysis property
	 */
	public void setLeftAnalysisProperties(List<WordProperty> selectedLeftSortProperty) {
		// updateDirty(pAnalysisLeftProperties, selectedLeftSortProperty);
		this.pAnalysisLeftProperties = selectedLeftSortProperty;
		resetLines();
	}

	/**
	 * Set both left and right contexts size.
	 *
	 * @param size the new context size
	 */
	public void setLeftContextSize(int size) {
		// updateDirty(pLeftContextSize, size);
		this.pLeftContextSize = size;
		resetLines();
	}



	public void setParameters(CQLQuery query,
			List<WordProperty> selectedLeftCSortProperty,
			List<WordProperty> selectedRightCSortProperty,
			List<WordProperty> selectedKeywordSortProperty,
			List<WordProperty> selectedLeftCViewProperties,
			List<WordProperty> selectedRightCViewProperties,
			List<WordProperty> selectedKeywordViewProperties,
			ReferencePattern referencePattern,
			ReferencePattern refAnalysePattern,
			Integer leftContextSize,
			Integer rightContextSize) {

		this.pQuery = query;
		this.pAnalysisLeftProperties = selectedLeftCSortProperty;
		this.pAnalysisRightProperties = selectedRightCSortProperty;
		this.pAnalysisKeywordProperties = selectedKeywordSortProperty;
		this.pViewLeftProperties = selectedLeftCViewProperties;
		this.pViewRightProperties = selectedRightCViewProperties;
		this.pViewKeywordProperties = selectedKeywordViewProperties;
		this.pViewRefPattern = referencePattern;
		this.pAnalysisRefPattern = refAnalysePattern;
		this.pLeftContextSize = leftContextSize;
		this.pRightContextSize = rightContextSize;

		this.setDirty();
	}

	/**
	 * Set the reference Pattern.
	 * 
	 * @param referencePattern
	 *            the new reference Pattern
	 */
	public void setRefAnalysePattern(ReferencePattern referencePattern) {
		// updateDirty(pAnalysisRefPattern, referencePattern);
		this.pAnalysisRefPattern = referencePattern;
		resetLines();
	}

	/**
	 * Set the reference Pattern.
	 * 
	 * @param referencePattern
	 *            the new reference Pattern
	 */
	public void setRefViewPattern(ReferencePattern referencePattern) {
		// updateDirty(pViewRefPattern, referencePattern);
		this.pViewRefPattern = referencePattern;
		resetLines();
	}

	/**
	 * Set the analysis propertyof the left context.
	 * 
	 * @param selectedRightSortProperty
	 *            the new analysis property
	 */
	public void setRightAnalysisProperties(List<WordProperty> selectedRightSortProperty) {
		// updateDirty(pAnalysisRightProperties, selectedRightSortProperty);
		this.pAnalysisRightProperties = selectedRightSortProperty;
		resetLines();
	}

	/**
	 * Set both left and right contexts size.
	 * 
	 * @param size
	 *            the new context size
	 */
	public void setRightContextSize(int size) {
		// updateDirty(pRightContextSize, size);
		this.pRightContextSize = size;
		resetLines();
	}

	/**
	 * Set the same view properties for left context, right context and keyword.
	 * 
	 * @param viewProperties
	 *            the new view properties
	 */
	public void setViewProperties(List<WordProperty> viewProperties) {
		this.pViewLeftProperties = viewProperties;
		this.pViewRightProperties = viewProperties;
		this.pViewKeywordProperties = viewProperties;
		this.setDirty();
		resetLines();
	}

	/**
	 * Set specif ic view properties for left context, right context and keyword.
	 * if the List are null, the corresponding list will be reset
	 * 
	 * @param leftCViewProperties
	 *            left context view properties
	 * @param rightCViewProperties
	 *            right context view properties
	 * @param keywordViewProperties
	 *            keyword view properties
	 */
	public void setViewProperties(List<WordProperty> leftCViewProperties,
			List<WordProperty> rightCViewProperties,
			List<WordProperty> keywordViewProperties) {
		// reset list or use the given ones
		if (leftCViewProperties != null) {
			this.pViewLeftProperties = leftCViewProperties;
		}
		else {
			this.pViewLeftProperties = new ArrayList<>();
		}

		if (rightCViewProperties != null) {
			this.pViewRightProperties = rightCViewProperties;
		}
		else {
			this.pViewRightProperties = new ArrayList<>();
		}

		if (keywordViewProperties != null) {
			this.pViewKeywordProperties = keywordViewProperties;
		}
		else {
			this.pViewKeywordProperties = new ArrayList<>();
		}

		this.setDirty();
		resetLines();
	}

	/**
	 * Sort the lines of the concordance according a comparator.
	 * 
	 * @param comparator the comparator used to compare the lines
	 * 
	 * @throws CqiClientException the cqi client exception
	 * @throws CqiServerError
	 * @throws IOException
	 */
	public void sort(LineComparator comparator, TXMProgressMonitor monitor) throws Exception {

		//String[] previousSort = comparator.getAvailableComparators();
		System.currentTimeMillis();
		this.acquireSemaphore();
		comparator.initialize(this.getCorpus());
		this.releaseSemaphore();

		resetLines();

		if (getNLines() == 0) {
			return;
		}

		getLines(0, nLines - 1); // all lines are necessary to sort TODO: if fact only the sort properties are needed
		if (lines.size() > 0) {
			Collections.sort(lines, comparator);
		}
		System.currentTimeMillis();
	}


	/**
	 * To txt.
	 *
	 * @param outfile the outfile
	 * @param format the format
	 * @return true, if successful
	 */
	@Deprecated
	// FIXME: should be moved in a exporter RCP extension
	public boolean toTxt(File outfile, Format format) {
		try {
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(outfile), "UTF-8")); //$NON-NLS-1$
			toTxt(writer, 0, nLines - 1, format, "\t", ""); //$NON-NLS-1$ //$NON-NLS-2$
			writer.close();
		}
		catch (Exception e) {
			System.err.println(NLS.bind(ConcordanceCoreMessages.failedToExportConcordanceColonP0, e.getLocalizedMessage()));
			Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	/**
	 * Write all the lines on a writer.
	 *
	 * @param outfile the outfile
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @return true, if successful
	 * @throws IOException
	 */
	@Override
	@Deprecated
	// FIXME: should be moved in a exporter RCP extension
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws IOException {

		// NK: writer declared as class attribute to perform a clean if the operation is interrupted
		OutputStreamWriter writer = null;
		try {
			writer = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(outfile)), encoding);
			// if ("UTF-8".equals(encoding)) writer.write('\ufeff'); // UTF-8 BOM
			toTxt(writer, 0, nLines - 1, colseparator, txtseparator);
		}
		catch (Exception e) {
			System.err.println(NLS.bind(ConcordanceCoreMessages.failedToExportConcordanceColonP0, e.getLocalizedMessage()));
			Log.printStackTrace(e);
			return false;
		}
		finally {
			if (writer != null) {
				writer.close();
			}
		}

		return true;
	}

	/**
	 * Write the lines between from and to on a writer.
	 * 
	 * @param writer
	 *            Where to write the lines
	 * @param from
	 *            The first line to be written
	 * @param to
	 *            The last line to be writen
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws CqiServerError
	 */
	@Deprecated
	// FIXME: should be moved in a exporter RCP extension
	public void toTxt(Writer writer, int from, int to)
			throws Exception {

		toTxt(writer, from, to, Format.CONCORDANCE, "\t", ""); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * To txt.
	 * Write line per packet to avoid memory limits
	 *
	 * @param writer the writer
	 * @param from the from
	 * @param to the to
	 * @param format the format
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @throws CqiClientException the cqi client exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CqiServerError
	 */
	// FIXME: should be moved in a exporter RCP extension
	@Deprecated
	public void toTxt(Writer writer, int from, int to, Format format, String colseparator, String txtseparator)
			throws Exception {

		int packetSize = 5000;
		int nlines = to - from;
		if (nlines == 0) nlines = 1;
		int step = (100 * packetSize) / nlines;
		if (step == 0) step = 1;

		String keyColSeparator = colseparator; // the separator is "" if export in CONTEXT mode
		// write header and set keywordColumnSeparator
		if (format == Format.CONTEXT) {
			keyColSeparator = ""; //$NON-NLS-1$
			writer.write(
					txtseparator + ConcordanceCoreMessages.reference + txtseparator
							+ colseparator + txtseparator + ConcordanceCoreMessages.context + txtseparator
							+ "\n"); //$NON-NLS-1$
		}
		else {
			writer.write(
					addTxtSep(ConcordanceCoreMessages.reference, txtseparator) + colseparator
							+ addTxtSep(ConcordanceCoreMessages.leftContext, txtseparator) + colseparator
							+ addTxtSep(ConcordanceCoreMessages.keyword, txtseparator) + colseparator
							+ addTxtSep(ConcordanceCoreMessages.rightContext, txtseparator)
							+ "\n"); //$NON-NLS-1$
		}

		// write lines
		for (int i = from; i <= to; i += packetSize + 1) {
			// this.monitorSetWorked(step);
			// println "worked: $step"
			// println "packet $i -> "+(i+packetSize)
			int to2 = i + packetSize;
			if (to2 > to) to2 = to;
			// println "get lines: $i -> $to2"
			List<Line> sublines = getLines(i, to2);
			for (Line line : sublines) {
				writer.write(
						addTxtSep(line.getViewRef().toString(), txtseparator)
								+ colseparator + addTxtSep(line.leftContextToString(), txtseparator)
								+ keyColSeparator + addTxtSep(line.keywordToString(), txtseparator)
								+ keyColSeparator + addTxtSep(line.rightContextToString(), txtseparator)
								+ "\n"); //$NON-NLS-1$
				writer.flush();
			}
			// println "Lines1: "+lines
			// lines.addAll(i,Collections.nCopies(packetSize, (Line) null)); // delete created lines
			for (int j = i; j <= to2 && j < lines.size(); j++)
				lines.set(j, null);
			// println "Lines2: "+lines
		}

		writer.flush();
		writer.close();
	}

	/**
	 * Write the lines between from and to on a writer with custom col and txt separators.
	 * 
	 * @param writer
	 *            Where to write the lines
	 * @param from
	 *            The first line to be written
	 * @param to
	 *            The last line to be writen
	 * @param colseparator
	 *            The column separator
	 * @param txtseparator
	 *            Thx text separator, e.g txtseparator=' : "I'm" >> "I''m"
	 * 
	 * @throws CqiClientException
	 *             the cqi client exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws CqiServerError
	 */
	// FIXME: should be moved in a exporter RCP extension
	@Deprecated
	public void toTxt(Writer writer, int from, int to, String colseparator, String txtseparator)
			throws Exception {
		toTxt(writer, from, to, Format.CONCORDANCE, colseparator, txtseparator);
	}

	/**
	 * The Enum Format.
	 */
	public enum Format {
		/** The CONCORDANCE. */
		CONCORDANCE,
		/** The CONTEXT. */
		CONTEXT
	}

	public void setNLinePerPage(Integer nLinesPerPage) {
		this.pNLinesPerPage = nLinesPerPage;
	}

	public Integer getNLinePerPage() {
		return pNLinesPerPage;
	}

	public Integer getTopIndex() {
		return pTopIndex;
	}

	public List<WordProperty> getAvailableKeywordViewProperties() {
		return availableKeywordViewProperties;
	}

	public List<WordProperty> getAvailableKeywordSortProperties() {
		return availableKeywordSortProperties;
	}

	public String getLimitCQL() {
		return pLimitCQL;
	}

	public List<WordProperty> getAvailableLeftViewProperties() {
		return availableLeftViewProperties;
	}

	public List<WordProperty> getAvailableRightViewProperties() {
		return availableRightViewProperties;
	}

	public void setLeftViewProperties(List<WordProperty> selectedViewProperties) {
		// updateDirty(pViewLeftProperties, selectedViewProperties);
		this.pViewLeftProperties = selectedViewProperties;
	}

	public void setRightViewProperties(List<WordProperty> selectedViewProperties) {
		// updateDirty(pViewRightProperties, selectedViewProperties);
		this.pViewRightProperties = selectedViewProperties;
	}

	public void setKeywordViewProperties(List<WordProperty> selectedViewProperties) {
		// updateDirty(pViewKeywordProperties, selectedViewProperties);
		this.pViewKeywordProperties = selectedViewProperties;
	}

	public List<WordProperty> getAvailableLeftSortProperties() {
		return this.availableLeftSortProperties;
	}

	public List<WordProperty> getAvailableRightSortProperties() {
		return this.availableRightSortProperties;
	}

	/**
	 * Sets the query.
	 * 
	 * @param query the query to set
	 */
	public void setQuery(IQuery query) {
		this.pQuery = query;
	}


	/**
	 * Sets the query.
	 * 
	 * @param queryString
	 */
	public void setQuery(String queryString) {
		this.setQuery(new CQLQuery(queryString));
	}

	public void setTopIndex(int i) {
		pTopIndex = i;
	}

	@Override
	public boolean saveParameters() {

		if (pQuery != null) {
			this.saveParameter(ConcordancePreferences.QUERY, Query.queryToString(pQuery));
		}

		this.saveParameter(ConcordancePreferences.KEYWORD_VIEW_PROPERTIES, WordProperty.propertiesToString(this.pViewKeywordProperties));

		this.saveParameter(ConcordancePreferences.LEFT_VIEW_PROPERTIES, WordProperty.propertiesToString(this.pViewLeftProperties));

		this.saveParameter(ConcordancePreferences.RIGHT_VIEW_PROPERTIES, WordProperty.propertiesToString(this.pViewRightProperties));

		this.saveParameter(ConcordancePreferences.KEYWORD_ANALYSIS_PROPERTIES, WordProperty.propertiesToString(this.pAnalysisKeywordProperties));

		this.saveParameter(ConcordancePreferences.LEFT_ANALYSIS_PROPERTIES, WordProperty.propertiesToString(this.pAnalysisLeftProperties));

		this.saveParameter(ConcordancePreferences.RIGHT_ANALYSIS_PROPERTIES, WordProperty.propertiesToString(this.pAnalysisRightProperties));

		this.saveParameter(ConcordancePreferences.VIEW_REFERENCE_PATTERN, ReferencePattern.referenceToString(this.pViewRefPattern));

		this.saveParameter(ConcordancePreferences.SORT_REFERENCE_PATTERN, ReferencePattern.referenceToString(pAnalysisRefPattern));

		return true;
	}



	@Override
	public String getResultType() {
		return ConcordanceCoreMessages.RESULT_TYPE;
	}

	public void setKeywordAvailableViewProperties(List<WordProperty> availableProperties) {

		this.availableKeywordViewProperties = new ArrayList<>(availableProperties);
	}

	public void setLeftAvailableViewProperties(List<WordProperty> availableProperties) {

		this.availableLeftViewProperties = new ArrayList<>(availableProperties);
	}

	public void setRightAvailableViewProperties(List<WordProperty> availableProperties) {

		this.availableRightViewProperties = new ArrayList<>(availableProperties);
	}

	public void setKeywordAvailableAnalysisProperties(List<WordProperty> availableProperties) {

		this.availableKeywordSortProperties = new ArrayList<>(availableProperties);
	}

	public void setLeftAvailableAnalysisProperties(List<WordProperty> availableProperties) {

		this.availableLeftSortProperties = new ArrayList<>(availableProperties);
	}

	public void setRightAvailableAnalysisProperties(List<WordProperty> availableProperties) {

		this.availableRightSortProperties = new ArrayList<>(availableProperties);
	}
}
