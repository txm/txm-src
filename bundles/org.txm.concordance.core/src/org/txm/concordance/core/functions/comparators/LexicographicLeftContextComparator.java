// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2014-07-15 09:42:00 +0200 (mar., 15 juil. 2014) $
// $LastChangedRevision: 2820 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.concordance.core.functions.comparators;

import java.util.HashMap;
import java.util.List;

import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.core.functions.Line;
import org.txm.concordance.core.messages.ConcordanceCoreMessages;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.WordProperty;

// TODO: Auto-generated Javadoc
/**
 * Compare two values of left context of a concordance line @ author mdecorde.
 */
public class LexicographicLeftContextComparator extends
		StringListBasedComparator {

	/** The Constant NAME. */
	private static final String NAME = ConcordanceCoreMessages.leftContext_2;

	private Concordance conc;

	private List<WordProperty> props;

	/**
	 * Instantiates a new lexicographic left context comparator.
	 */
	public LexicographicLeftContextComparator() {
		super(NAME);
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Line l1, Line l2) {

		this.conc = l1.getConcordance();
		this.props = conc.getLeftAnalysisProperties();
		if (props.size() == 0)
			return 0;
		int rez = 0;
		int i = 0;

		HashMap<Property, List<String>> values1 = l1.getLeftAnalysisProperty();
		HashMap<Property, List<String>> values2 = l2.getLeftAnalysisProperty();
		int j1 = l1.getLeftAnalysisProperty().get(props.get(0)).size() - 1;
		int j2 = l2.getLeftAnalysisProperty().get(props.get(0)).size() - 1;

		String v1, v2;

		while (rez == 0) {

			if ((j1 < 0) && (j2 < 0))
				return 0;
			if (j1 < 0)
				return -1; // end of line 1
			if (j2 < 0)
				return 1; // end of line 2

			v1 = values1.get(props.get(i)).get(j1);
			v2 = values2.get(props.get(i)).get(j2);

			if (v1 == null) return -1;
			if (v2 == null) return 1;

			rez = collator.compare(v1, v2);
			if (++i >= props.size()) { // iterates over properties before tokens
				i = 0;
				j1--;
				j2--;
			}
		}
		return rez;
	}
}
