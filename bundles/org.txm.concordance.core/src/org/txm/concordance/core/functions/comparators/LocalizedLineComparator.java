// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2014-07-15 09:42:00 +0200 (mar., 15 juil. 2014) $
// $LastChangedRevision: 2820 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.concordance.core.functions.comparators;

import java.text.Collator;
import java.util.Locale;

import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * use the giver locale to compare concordance line, if locale is not defined,
 * use the system locale @ author mdecorde.
 */
public abstract class LocalizedLineComparator extends LineComparator {

	/** The locale. */
	private Locale locale = new Locale("en", "US"); //$NON-NLS-1$ //$NON-NLS-2$

	/** The collator. */
	protected Collator collator;

	/**
	 * Instantiates a new localized line comparator.
	 *
	 * @param name the name
	 */
	public LocalizedLineComparator(String name) {
		super(name);
	}

	/**
	 * Gets the locale.
	 *
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * Sets the locale.
	 *
	 * @param locale the new locale
	 */
	public void setLocale(Locale locale) {
		Log.fine("Set comparator locale to " + locale); //$NON-NLS-1$
		this.locale = locale;
		this.collator = Collator.getInstance(locale);
		this.collator.setStrength(Collator.TERTIARY);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.concordance.functionscomparators.LineComparator#initialize(org.txm.searchengine.cqp.corpus.Corpus)
	 */
	@Override
	public void initialize(CQPCorpus corpus) {
		setLocale(new Locale(corpus.getLang()));
	}
}
