// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.concordance.core.functions.comparators;

import java.util.List;

import org.txm.concordance.core.functions.Line;
import org.txm.concordance.core.messages.ConcordanceCoreMessages;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

// TODO: Auto-generated Javadoc
/**
 * The Class CompositeComparator.
 */
public class CompositeComparator extends LineComparator {

	/** The comparators. */
	private List<LineComparator> comparators;

	/**
	 * Instantiates a new composite comparator. A composite comparator C is made
	 * of a list compartors [C0,...,Cn]. C.compare(l1,l2) == 1 (resp. -1) if and
	 * only if their exists k such as Ci.compare(l1,l2)==0 for i<k and
	 * Ck.compare(l1,l2)=1 (resp. -1) C.compare(l1,l2) == 0 if and only if
	 * Ci.compare(l1,l2)==0 for all i
	 *
	 * @param name the name
	 * @param comparators the comparators
	 */
	public CompositeComparator(String name, List<LineComparator> comparators) {
		super(name);
		setComparators(comparators);
	}

	/*
	 * public CompositeComparator addComparator(LineComparator comparator) {
	 * this.comparators.add(comparator);
	 * return this;
	 * }
	 */

	/*
	 * (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Line l1, Line l2) {
		//System.out.println("compare: "+l1+" "+l2);
		for (LineComparator comparator : comparators) {
			int c = comparator.compare(l1, l2);
			//System.out.println("comparator: "+comparator+" = "+c);
			if (c != 0)
				return c;
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.concordance.functionscomparators.LineComparator#initialize(org.txm.searchengine.cqp.corpus.Corpus)
	 */
	@Override
	public void initialize(CQPCorpus corpus) {
		for (LineComparator comparator : comparators)
			comparator.initialize(corpus);
	}

	/**
	 * Gets the comparators.
	 *
	 * @return the comparators
	 */
	public List<LineComparator> getComparators() {
		return comparators;
	}

	/**
	 * Sets the comparators.
	 *
	 * @param comp the new comparators
	 */
	public void setComparators(List<LineComparator> comp) {
		this.comparators = comp;
		this.name = ConcordanceCoreMessages.compositeColon;
		for (LineComparator c : comp)
			this.name += c.getName() + " "; //$NON-NLS-1$
	}
}
