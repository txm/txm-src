// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.concordance.core.functions.comparators;

import java.util.Comparator;

import org.txm.concordance.core.functions.Line;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

// TODO: Auto-generated Javadoc
/**
 * Abstract comparator of concordance line @ author mdecorde.
 */
public abstract class LineComparator implements Comparator<Line> {

	/** The name. */
	protected String name;

	/** The available comparator. */
	private static String[] availableComparator = new String[] {
			"org.txm.concordance.functionscomparators.LexicographicKeywordComparator", //$NON-NLS-1$
			"org.txm.concordance.functionscomparators.LexicographicLeftContextComparator", //$NON-NLS-1$
			"org.txm.concordance.functionscomparators.LexicographicRightContextComparator", //$NON-NLS-1$
			"org.txm.concordance.functionscomparators.PropertiesReferenceComparator" //$NON-NLS-1$
	};

	/**
	 * Return the list of all the known comparators. Any non-abstract class
	 * extending LineComparator should be mentionned in the availableComparator
	 * list
	 *
	 * @return the available comparators
	 */
	static public String[] getAvailableComparators() {
		return availableComparator;
	}

	public boolean equals(LineComparator c2) {
		return this.getName().equals(c2.getName());
	}

	/**
	 * Instantiates a new line comparator.
	 *
	 * @param name the name
	 */
	public LineComparator(String name) {
		this.name = name;

	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "C:" + name; //$NON-NLS-1$
	}

	/**
	 * Initialize.
	 *
	 * @param corpus the corpus
	 */
	public abstract void initialize(CQPCorpus corpus);

}
