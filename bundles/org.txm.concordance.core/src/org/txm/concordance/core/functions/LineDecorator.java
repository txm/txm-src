package org.txm.concordance.core.functions;

/**
 * 
 * Extends this class to decorate concordance lines
 * 
 * @author mdecorde
 *
 */
public abstract class LineDecorator {

	/**
	 * 
	 * decorate the String str.
	 * 
	 * 
	 * @param i the relative position of 'str'
	 * @param currentPos the position of 'str' in the corpus
	 * @param str the String to decorate
	 * 
	 * @return the decorated String
	 */
	public abstract String decorate(Line i, int currentPos, String str);
}
